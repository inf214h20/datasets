The Revelator Collection
{{Infobox Film
| name = The Revelator Collection
| image = revelator.jpg
| caption =
| director = Mark Seliger
| producer = Gillian Welch & David Rawlings / Rhea Rupert
| eproducer =
| aproducer =
| writer =
| starring = Gillian Welch & David Rawlings
| music =
| cinematography =
| editing = Lee Tucker / Barney Miller Acony
| released = November 12, 2002
| runtime = 59 min.
| country = United States|U.S.
| awards = English
| budget =
| preceded_by =
| followed_by =
}}
 concert footage of singer-songwriter Gillian Welch and her musical partner David Rawlings. All of the video was filmed in black and white by still photographer Mark Seliger.

The first three tracks were recorded at RCA Studio B in Nashville, Tennessee and document the recording of these songs for Welchs 2001 album, Time (The Revelator).

The remaining songs are all filmed before live audiences. Tracks 4-8 were shot at the Cats Cradle in Carrboro, North Carolina on November 10, 2001, and Tracks 9-11 were filmed the following day at the Bijou Theater in Knoxville, Tennessee. These eight tracks include several songs from the Time (The Revelator) album, one previously unreleased Gillian Welch song, "Wichita", and several covers. The duo cover Bob Dylans "Billy", Neil Youngs "Pocahontas" and Townes Van Zandts "White Freight Liner Blues" as well as Bill Monroes "Im on My Way Back to the Old Home" with Rawlings singing lead. The clip of Welch and Rawlings performing "I Want to Sing that Rock and Roll" from the concert film, Down from the Mountain is added as a bonus.

Audio of all tracks have been released online and titled as "Songs from the Revelator Collection".

==Track listing==
# "Elvis Presley Blues" (David Rawlings|Rawlings-Gillian Welch|Welch)
# "My First Lover" (Rawlings-Welch)
# "Revelator" (Rawlings-Welch)
# "April the 14th" (Rawlings-Welch)
# "Wichita" (Welch)
# "Red Clay Halo" (Rawlings-Welch)
# "Billy" (Bob Dylan|Dylan)
# "Pocahontas (Neil Young song)|Pocahontas" (Neil Young|Young)
# "Revelator" (Rawlings-Welch)
# "Im on My Way Back to the Old Home" (Bill Monroe|Monroe) Van Zandt)
# "I Want to Sing that Rock and Roll" (Rawlings-Welch)

==Credits==
* Director: Mark Seliger
* Director of Photography: Michael Garofalo
* Sound: Terry Hillman
* Project Coordinator: Norm Parenteau
* Package Design: Frank Olinsky
* Cover Photograph: Mark Seliger
* DVD Editor: Lee Tucker, Ground Zero, Nashville, Tennessee
* DVD Authoring, Encoding, and Mastering: Jeff Stabenau, Blink Digital, New York, New York
* DVD Producers: Gillian Welch & David Rawlings
* Gillian Welch plays a 1956 Gibson J-50
* David Rawlings plays a 1935 Epiphone Olympic
* The Box was made by Barnett and Jaffe
===Tracks 1-3===
* Film producer: Rhea Rupert
* Editor: Barney Miller

==See also==
* Time (The Revelator) (related album)
* Down from the Mountain (concert film with some of the same footage)

==External links==
*  

 
 
 
 
 
 
 
 