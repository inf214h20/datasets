54 (film)
{{Infobox film
| name           = 54
| image          = 54 poster.jpg
| alt            = 
| caption        = Theatrical release poster Mark Christopher
| producer       = Ira Deutchman Richard N. Gladstein Dolly Hall
| writer         = Mark Christopher
| starring       = Ryan Phillippe Salma Hayek Neve Campbell Mike Myers Sela Ward Breckin Meyer
| music          = Marco Beltrami
| cinematography = Alexander Gruszynski
| editing        = Lee Percy
| distributor    = Miramax Films
| released       =  
| runtime        = 93 minutes 105 minutes  
| country        = United States
| language       = English
| budget         = $13 million 
| gross          = $16.8 million 
}} Mark Christopher, about Studio 54, a world-famous New York City disco club, the main setting of the film. It stars Ryan Phillippe, Salma Hayek Neve Campbell and Mike Myers as Steve Rubell, the co-founder of the club.

==Plot== Jersey man, handsome enough to become a bartender at Studio 54. There he befriends aspiring singer Anita (Salma Hayek) and her husband, Greg Randazzo (Breckin Meyer). Shane gets sucked into the hard-partying scene at Studio 54, as his life spirals downward, so does It.

==Cast==
* Ryan Phillippe as Shane OShea
* Salma Hayek as Anita Randazzo
* Neve Campbell as Julie Black
* Mike Myers as Steve Rubell
* Sela Ward as Billie Auster
* Breckin Meyer as Greg Randazzo
* Sherry Stringfield as Viv
* Cameron Mathison as Atlanta
* Heather Matarazzo as Grace OShea
* Skipp Sudduth as Harlan OShea
* Mark Ruffalo as Ricko
* Lauren Hutton as Liz Vangelder Michael York as Ambassador
* Ellen Albertini Dow as Disco Dottie
* Justin Bartha as Clubgoer (uncredited)

===Celebrity patrons===
{{columns-list|2|style=width:60em;|
* Thelma Houston
* Ron Jeremy Elio Fiorucci
* Sheryl Crow
* Georgina Grenville
* Cindy Crawford
* Heidi Klum
* Donald Trump
* Cecilie Thomsen
* Frederique van der Wal
* Veronica Webb
* Art Garfunkel
* Peter Bogdanovich
* Beverly Johnson
* Bruce Jay Friedman
* Lorna Luft
* Valerie Perrine
* Stars on 54 (Amber (performer)|Amber, Ultra Naté, and Jocelyn Enriquez)
}}

==Production== Mark Christopher persuaded Miramax Films to back a full-length feature about Studio 54. He had spent five years researching the club and the time period, as well as working on a screenplay. Miramax purchased a partial screenplay in 1995 and developed the script with the filmmaker for over a year. Christopher shot the film in Toronto over two months in the fall of 1997. During the production, a Miramax executive was often found on the set and studio head Harvey Weinstein flew up from New York to give his approval.

Expectations were high with the hopes that the film would become a big summer hit. Christopher finished his cut of the film and the studio scheduled the films release for July of the following year. After initial positive reaction within the company, early test screenings in the Long Island suburbs for the two-hour cut of the film were disappointing to the studio. Audiences found the characters unlikable and reacted negatively to the kiss between Shane and Greg. They also did not respond well to the happy ending for both of them and Anita. Christopher said via his publicist, “Our goal was to keep the audience sympathetic to the characters,   any material that was removed from the film was removed because it was too challenging for some members of the audience." {{cite news 
  | last = Ascher-Walsh 
  | first = Rebecca
  | coauthors = 
  | title = The 411 On 54
  | work = 
  | pages = 
  | language = persian
  | publisher = Entertainment Weekly
  | date = 1998-09-04
  | url = http://www.ew.com/ew/report/0,6115,284645_7%7C44806%7C%7C0_0_,00.html
  | accessdate = 2006-12-21}}  Miramax requested cuts be made and Christopher initially refused.

The studio forced Christopher to reshoot parts of his movie with only two months until its theatrical release, destroying the love triangle subplot between the three characters. Much of the cast was called back for two weeks of additional filming in New York without being told what they would be shooting. Meyer, for example, found out that his substantial part in the film had been cut down to a stereotypical best-friend role and a new scene was shot that portrayed his character as a thief. The kiss between Greg and Shane was replaced with a conversation. Ultimately, 45 minutes of the original film were deleted and replaced with 25 minutes of new scenes and voice-over.

Christopher initially complained to friends and colleagues about what the studio was doing to his movie but under pressure at the films release, he took a more politically advantageous stance. "We were both trying to make the best movie possible, and I think weve done that,"  he said at the time.

==Critical reaction==
54 opened at #4 in its opening weekend (8/28-30) with $6,611,532 behind Blade (film)|Blade, Theres Something About Mary, and Saving Private Ryan. 

The studio cut of the film received almost universally middling-to-poor reviews and was a box office disappointment, grossing $16 million on an estimated budget of $13 million. Mike Myers, in his first serious dramatic role, having garnered some of the films only positive word-of-mouth. That generated brief buzz that his performance would land him among those nominated for an Academy Award (though he ultimately was not nominated). Many critics were particularly disappointed with the films fictional characters and storyline, believing that Studio 54s notorious, real-life past should have been explored more in detail and with better realism. Critical response to the Directors cut, which has gained a fair amount of cult status, is positive. 

The film currently holds a 13% Rotten rating on Rotten Tomatoes, with the consensus "Poor plot development and slow pacing keep 54 from capturing the energy of its legendary namesake." 
 Razzie Awards, Worst Actor Worst Supporting Actress for Ellen Albertini Dow. 

==Home media== additional and alternate scenes that were not included in the theatrical release. This extended cut runs 100 minutes, 8 minutes of which are not in the studios 92 minute release. A 105 minute directors cut, which includes over 30 minutes of footage from the original shoot that has never been in any cut, and deleting all but a few seconds of the studio-dictated reshot footage, was screened in the Panorama section of the 65th Berlin International Film Festival in February 2015.   

==References==
 

==External links==
*  
*  
*  
*  


 
 
 
 
 
 
 
 
 
 
 
 
 
 