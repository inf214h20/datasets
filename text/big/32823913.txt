You Ain't Seen Nothin' Yet (film)
 You Aint Seen Nothin Yet}}
{{Infobox film
| name = You Aint Seen Nothin Yet!
| image = Vous navez encore rien vu.jpg
| caption = Theatrical release poster
| director = Alain Resnais
| producer = Jean-Louis Livi
| screenplay = Laurent Herbiet Alain Resnais
| based on =  
| starring = Mathieu Amalric Sabine Azéma Anne Consigny Lambert Wilson
| music = Mark Snow
| cinematography = Eric Gautier
| editing = Hervé de Luze
| studio = F Comme Film
| distributor = StudioCanal
| released =  
| runtime = 115 minutes
| country = France
| language = French
| budget = € 8 million
}}
You Aint Seen Nothin Yet! ( ) is a 2012 French-German film directed by Alain Resnais, and loosely based on two plays by Jean Anouilh. The film was shown in competition for the Palme dOr at the 2012 Cannes Film Festival.

==Plot==
From beyond the grave, celebrated playwright Antoine d’Anthac gathers together all his friends who have appeared over the years in his play “Eurydice.” These actors watch a recording of the work performed by a young acting company, La Compagnie de la Colombe. During the screening, Antoine’s friends are so overwhelmed by their memories of the play that they start performing it together, despite no longer being the appropriate age for their various roles.

==Cast==
The Actors:
* Mathieu Amalric
* Pierre Arditi 
* Sabine Azéma
* Jean-Noël Brouté
* Anne Consigny
* Anny Duperey 
* Hippolyte Girardot
* Gérard Lartigau
* Michel Piccoli
* Denis Podalydès, as Antoine
* Michel Robin 
* Andrzej Seweryn, as Marcellin
* Jean-Chrétien Sibertin-Blanc
* Michel Vuillermoz
* Lambert Wilson

La Compagnie de la Colombe:
* Vimala Pons, as Eurydice
* Sylvain Dieuaide, as Orphée
* Fulvia Collongues, as the Mother
* Vincent Chatraix, as the Father
* Jean-Christophe Folly, as Monsieur Henri
* Vladimir Consigny, as Mathias
* Laurent Ménoret, as Vincent
* Lyn Thibault, as the Young Girl and the Café Waiter
* Gabriel Dufay, as the Hotel Waiter

==Production==
 .]] Les Herbes folles. The film was produced through F Comme Film for a budget of eight million euro. It received co-production support from Christmas In July, the German company Alamode Film, and the television channel France 2 which invested 1.3 million euro.     It received 471,000 euro from the Île-de-France (region)|Île-de-France region, as well as funding from the National Center of Cinematography and the moving image|CNC, Canal+ and Ciné+. Principal photography took place at the Saint-Ouen Studios for two months and was completed on 1 April 2011. 

Resnais agreed with the writer Laurent Herbiet, with whom he had previously worked on Les Herbes folles, that they would use two plays by Jean Anouilh to form the basis of the film: Eurydice (Anouilh play)|Eurydice and Cher Antoine ou lAmour raté. Explaining his choice, Resnais recalled the impact which the original stage production of Eurydice in 1942 had on him, and added that in his films he was constantly looking for a theatrical kind of language which invited the actors to distance themselves from the realism of everyday life and to move towards a more unexpected or offbeat kind of performance. He assembled a cast of 15 actors, many of whom he had worked with in previous films but including also four newcomers whose work he had admired elsewhere. From an interview with François Thomas, in the Festival de Cannes press kit for  . Retrieved 2012-05-22. 

For the recording of a performance of the play which is featured within the film, Resnais asked Bruno Podalydès to direct it quite independently, with his own cast, crew, and style, so that, in keeping with the spirit of the film, it would be entirely different from something that Resnais would have done himself. The challenge of then integrating this work into his own ideas for the film introduced an unpredictability to the outcome of the project which Resnais admitted to finding one of the most stimulating aspects of it. 

Mark Snow wrote the music for the film, the third of Resnaiss films in succession that he had worked on.

Denying that the film should be seen as a testament, Resnais said at a press conference in Cannes, "This film is unlike any other. If Id thought of this film as a final statement, Id never have had the courage or energy to do it." 

==Release==
The film was shown in competition at the 2012 Cannes Film Festival on 21 May 2012.       It was released in France on 26 September 2012  and in the United States in June 2013. 

==References==
 

==External links==
*  

 

 
 
 
 
 