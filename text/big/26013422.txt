Arasu (2007 film)
 
 

{{Infobox film
| name = arasu
| image = Shivarasu.jpg
| caption = movie poster
| director = Mahesh babu
| writer =
| producer = Smt. Parvathamma Rajkumar
| starring = Puneeth Rajkumar, Ramya, Meera Jasmine
| music = Joshua Sridhar
| music on =
| language = Kannada
| country = India
| website =
| cinematography =
| editing        =
| distributor    =
| released       = 6 June 2007
| runtime        =
}}
Arasu is a Kannada film starring Puneeth Rajkumar, Ramya and Meera Jasmine. Popular south Indian actress Shriya Saran also make a cameo appearance in this film. This is Shriyas first Kannada language film. This film made huge expectation among the fans  and become a blockbuster. 

==Plot==
Shivaraj Arasu is residing abroad. He is extremely good person and opulence is very common thing for him. He is asked to come to India by his manager Ramanna (Srinivasamurthy) to take up the progress of various companies he heads. In India he is not able to adjust to the mediocre life led by people. He accidentally sees Shruthi (Ramya) in a bus stop and proposes for marriage saying ILY. Shruthi is not ready to accept this offer. Shivaraj Urs goes behind her and he finds that she is the daughter of his manager Ramanna. In her house Shruthi gives a caustic remark on the attitude of Shivaraj group of companies and ask him to earn Rs. 5000 in a month without using his influence. This striking remark startles Shivaraj Urs and he takes up the challenge.

Now on streets without a single pie in his pocket Shivaraj struggles hard. He keeps on telling that he would give in excess of what he takes to fill his hunger initially. The hotelier feels that he has lost mental balance. He finds an old lady selling bananas. He repeats the same and eats two bananas and promises Rs. 200,000 for the old lady after month. At this stage he finds an ordinary girl Aishwarya (Meera Jasmine). The muscle power of Shivaraj when the rowdies attack her earns good name. Shivaraj also gets a job as a sales representative in the saree shop where Aishwarya is working. He finds a place to live in the small room of Aishwarya. Aishwarya turns very sympathetic and Shivaraj decides to stay in the house for one month. Shivaraj sleeps on the floor and his wonderful behavior and lot of help for the people in his colony gives him a comfortable standing. He owes a lot for Sruthi for her remark that has changed his life style and he is extremely happy with Aishwarya for supporting him in knowing the hard realities in life.

Now the phase three of the film after the interval! Shivaraj is liked immensely by Aishwarya in this one month. On the other hand Sruthi is also in love with Shivaraj and ready to marry him. Sruthi and Aishwarya are of course very good friends. Shivaraj is not aware that Aishwarya is in love with him but Aishwarya declares to Shruthi that she is in love with Shivaraj that surprises her. Now Sruthi decides to give up his love for the sake of her friend Aishwarya. On the other hand Aishwarya knowing that Sruthi is ready for marriage with Shivaraj also comes forward for sacrifice of her love. Not knowing what to do the intelligent Shivaraj takes a bold step. Without revealing his decision he convinces both for the marriage with two of his good friends Darshn and Aditya. Both Sruthi and Aishwarya get deceived with the tactics of Shivaraj. This is where the audience also get baffled.

Two good friends Darshan and Aditya have taken acceptance to find a girl for Shivaraj. In comes Shreya saying ILY to Shivaraj.

==Cast==
*Puneet Rajkumar
*Ramya
*Meera Jasmine
*Srinivasa Murthy
*Shriya Saran
* Komal Darshan
*Adithya

==Sound Track==
{{Infobox album  
| Name        = Arasu
| Type        = Soundtrack
| Artist      = Joshua sridhar
| Cover       =
| Released    =  June 2007
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Baaro Baaro || Puneet Rajkumar, Suchitra ||
|-
| 2 || Eko Eno || Mahalakshmi Iyer ||
|- Karthik ||
|-
| 4 || Ninna Kandakshana  || Kunal Ganjawala ||
|- Ranjith ||
|-
| 6 || Preethi Preethi || K. S. Chitra, Karthik ||
|-
|}

==Awards==

===Filmfare===
*Best Actor for Puneet Rajkumar

==References==
 

==External links==
* 

 
 
 
 