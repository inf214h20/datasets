The Count of Monte Cristo (2002 film)
{{Infobox film
| name = The Count of Monte Cristo
| image = The Count of Monte Cristo film.jpg
| caption = Theatrical release poster Kevin Reynolds
| producer = Gary Barber Roger Birnbaum Jonathan Glickman
| screenplay = Jay Wolpert
| based on = The Count of Monte Cristo by Alexandre Dumas
| starring = Jim Caviezel Guy Pearce Richard Harris James Frain Dagmara Dominczyk Luis Guzmán
| music = Edward Shearmur Andrew Dunn
| editing = Stephen Semel
| studio = Touchstone Pictures Spyglass Entertainment Buena Vista Pictures Distribution
| released =  
| runtime = 131 minutes
| language = English
| country = United Kingdom United States Ireland
| budget = $35 million
| gross = $75.4 million   
}} Kevin Reynolds. book of the same name by Alexandre Dumas, père and stars Jim Caviezel, Guy Pearce, and Richard Harris.    It follows the general plot of the novel (the main storyline of imprisonment and revenge is preserved); but many aspects, including the relationships between major characters and the ending, have been changed, simplified, or removed; and action scenes have been added. The movie met with modest box office success.

==Plot==
 
 
In 1815,, Edmond Dantès, second mate of a French trading ship, and his friend Fernand Mondego, a representative of the shipping company, head to the isle of Elba to seek medical attention for their ailing captain. Dantès and Mondego are chased by British Dragoons who believe they are spies for the exiled Napoleon Bonaparte. Bonaparte himself comes to their aid, stating that they are not his agents. As payment for using his physician, Bonaparte asks Dantès to deliver a letter to a certain Monsieur Clarion in France. The captain dies, however, and they leave Elba. On returning to France, Dantès is reprimanded by the ships first mate, Danglars, for disobeying orders. However, the companys boss, Morrell, commends Dantès bravery, promoting him to captain over Danglars, who is left fuming. Mondego intercepts Dantès fiancée, Mercédès, and tries to seduce her. When he hears of Dantès promotion, Mondego realizes that Dantès and Mercédès would soon be married.

A bitter Mondego gets drunk and tells Danglars about the letter Napoleon gave Dantès. Danglars informs on Dantès, who is charged with treason and called before a magistrate, Villefort. Villefort believes Dantès to be innocent and is about to release him when he realizes that the addressed is his own father, a Bonapartist. Horrified, he burns the letter and has Dantès sent to the island prison, Château dIf. Dantès escapes en route and seeks Mondegos help, but Mondego betrays and wounds him so he cannot escape. Dantès is imprisoned in the Château dIf, which is controlled by the sadistic Armand Dorleac. Meanwhile, news spreads that Napoleon has escaped from Elba. Mondego, Mercédès, Morrell and Dantès father go to Villefort to plead Dantès innocence, but Villefort dismisses them. Unknown to the others, Mondego and Villefort are in league together and Villefort informs Mercédès that Dantès has been executed.

In prison, Dantès befriends Abbé Faria, an elderly priest and former soldier in Napoleons army. Faria was imprisoned for refusing to reveal the location of the deceased Count Spadas vast fortune. For thirteen years Faria educates Dantès, teaching him mathematics, literature, philosophy, economics, hand and sword combat and military strategy. They attempt to escape but their tunnel caves in, mortally wounding Faria, who gives Dantès the location of Spadas treasure before dying. When the guards put Faria into a body bag, Dantès changes places with the corpse and is thrown into the sea, taking Dorleac with him, whom he promptly drowns.

Dantès washes onto a desert island and encounters Luigi Vampa, a smuggler and thief, and his band of pirates. Dantès is made to fight Jacopo, a traitor whom they intend to bury alive. Dantès defeats Jacopo but convinces Vampa to spare his life; Jacopo vows to serve Dantès thereafter. Dantès joins the smugglers, leaving with Jacopo when they arrive at Marseille. He visits Morrell, who doesnt recognise him. Dantès learns that while he was in prison, his father committed suicide, Danglars took over Morrells shipping company after being made a partner, and Mercédès married Mondego. Dantès goes to the island of Monte Cristo, finds Spadas treasure and vows revenge on his betrayers, taking the persona of the mysterious "Count of Monte Cristo". In Rome, he hires Vampa to kidnap Mondegos son Albert and then "rescues" and befriends him, revealing selective information about Spadas treasure that he knows will be told to Alberts father. In return, Albert invites the count to his birthday party at their residence in Paris. Dantès meets with Villefort to discuss a shipment. Mondego and Villefort are now convinced that Monte Cristo has found Spadas lost treasure and plot to steal it.

At the party, Mercédès recognizes Dantès, with whom she is still in love. Jacopo allows her to hide in Monte Cristos carriage to speak with him, wanting his master to abandon his obsession with revenge and simply live his life. Dantès turns her away and denies being her former lover, but gives himself away when he accidentally uses Edmonds last name. As Danglars men are stealing Monte Cristos cargo for Mondego he is confronted by Dantès, with the police in tow. Danglars fights Dantès, who reveals his true identity before having Danglars arrested. Dantès gets Villefort to confess that he made a deal with Mondego to kill his father in return for telling Mercédès that Dantès was dead. Villefort is arrested, and realizes Monte Cristos true identity before being imprisoned.

Mercédès confronts Dantès, admits that she is still in love with him, and they make love. The next morning, Dantès decides to take Mercédès and her son and leave France. Dantès has Mondegos debts called in, bankrupting him. Mercédès confronts Mondego, telling him that Albert is Dantès son; she only married him to hide his true paternity. Mondego leaves for his family estate, where the stolen gold shipment was to be taken. He finds that the chests are filled with dirt and sand, and that Dantès has arrived to take his revenge. Dantès disarms Mondego, but Albert rushes to defend him. Mercédès also arrives and reveals that Dantès is Alberts father. Mondego shoots Mercédès, but only wounds her. Mondego has a chance to get away but, having lost everything, has his own desire for revenge. Mondego and Dantès fight; Dantès stabs Mondego through the heart, killing him. Dantès returns to Château dIf to pay homage to Faria and promises him that he will live a better life with those who love him. With his revenge complete, Dantès leaves the island with Mercédès, Albert and Jacopo.
 

==Cast==
 

* Jim Caviezel as Edmond Dantès
* Guy Pearce as Fernand Mondego
* James Frain as J.F. Villefort
* Dagmara Dominczyk as Mercedès
* Luis Guzmán as Jacopo
* Richard Harris as Abbe Faria
* Michael Wincott as Armand Dorleac
* Henry Cavill as Albert Mondego
* Albie Woodington as Danglars 
* JB Blanc as Luigi Vampa
* Alex Norton as Napoléon
* Patrick Godfrey as Morrel
* Freddie Jones as Colonel Villefort
* Helen McCrory as Valentina Villefort Christopher Adamson as Maurice

==Reception==
The Count of Monte Cristo was well received by critics with a rating of 73% based on 143 ratings at Rotten Tomatoes with critics conceding that it was an "entertaining tale of revenge reminiscent of those swashbuckling movies made in the 1940s."  At Metacritic, the film received a score of 61 out of 100, with generally favorable reviews. 

Roger Ebert gave the film 3 stars out of 4 writing, "The Count of Monte Cristo is a movie that incorporates piracy, Napoleon in exile, betrayal, solitary confinement, secret messages, escape tunnels, swashbuckling, comic relief, a treasure map, Parisian high society and sweet revenge, and brings it in at under two hours, with performances by good actors who are clearly having fun. This is the kind of adventure picture the studios churned out in the Golden Age--so traditional it almost feels new." 

==Soundtrack==
{{Infobox album  
| Name        = The Count of Monte Cristo OST
| Type        = Soundtrack
| Artist      = Edward Shearmur
| Cover       = The Count of Monte Cristo soundtrack.jpg
| Released    = January 25, 2002
| Recorded    = 2001
| Genre       = Soundtrack
| Length      = 53:03
| Label       = RCA
}}

The Count of Monte Cristo Official Soundtrack was composed and conducted by Edward Shearmur and performed by the London Metropolitan Orchestra. 

{{Album reviews rev1 = SoundtrackNet rev1score =    
}}

;Track listing
# "Introduction" – 1:56
# "Landing on Elba" – 3:33
# "Marseille" – 4:23
# "Betrayed" – 3:52
# "Chateau dIf" – 4:26
# "Abbe Faria" – 2:24
# "Edmonds Education" – 0:58
# "Training Montage" – 1:54
# "Escape from the Island" – 7:24
# "Finding the Treasure" – 2:52
# "Invitation to the Ball" – 2:12
# "Involving Albert" – 2:47
# "After the Party" – 3:06
# "Retribution" – 5:29
# "End Titles" – 5:47

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 