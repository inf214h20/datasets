Vegas, City of Dreams
{{Infobox film
| name           = Vegas, City of Dreams
| image          = Vegas-cod.jpg
| caption        =
| director       = Lorenzo Doumani
| producer       =
| writer         = Lorenzo Doumani
| narrator       =
| starring       = Joe Don Baker Daniel Benzali Carlos Bernard Erika Eleniak
| music          =
| cinematography =
| editing        =
| distributor    = DMG Entertainment
| released       = April 1, 2001
| runtime        = 93 minutes
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}}

Vegas, City of Dreams is a drama film released in 2001. The film stars Joe Don Baker, Daniel Benzali, Carlos Bernard, and Erika Eleniak.

==Film synopsis== John Taylor) Las Vegas underworld.

==Cast==
* Joe Don Baker as Dylan Garrett
* Daniel Benzali as Dr. Sigmund Stein
* Carlos Bernard as Chico Escovedo
* Erika Eleniak as Katherine Garrett
* Brenda Epperson Doumani as Brenda Garrett
* Monika Schnarre as Jessica Garrett
* Carrie Stevens as Gabrielle Garrett

==Release==
The film was released on April 1, 2001.

==External links==
* 
*  at Rotten Tomatoes

 