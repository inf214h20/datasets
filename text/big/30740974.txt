Once Upon a Time in Anatolia
{{Infobox film
| name           = Once Upon a Time in Anatolia
| image          = Once Upon a Time in Anatolia.jpg
| alt            =
| caption        = Original theatrical poster
| director       = Nuri Bilge Ceylan
| producer       = Zeynep Özbatur Atakan
| writer         = Nuri Bilge Ceylan Ercan Kesal Ebru Ceylan
| starring       = Muhammet Uzuner Yılmaz Erdoğan Taner Birsel
| music          = 
| cinematography = Gökhan Tiryaki
| editing        = Bora Gökşingöl Nuri Bilge Ceylan
| studio         = Zeyno Film
| distributor    = The Cinema Guild
| released       =  
| runtime        = 157 minutes
| country        = Turkey Bosnia and Herzegovina
| language       = Turkish
| budget         = 
| gross          = 
}}
 Grand Prix.

==Plot==
Through the night, three cars carry a small group of men police officers, a doctor, a prosecutor, grave diggers, gendarmerie forces, and two brothers, homicide suspects around in the rural surroundings of the Anatolian town Keskin, in search of a buried body. Kenan, one of the suspects, leads them from one water fountain to another; at the time of the crime he was drunk and he cannot recall where he and his mentally challenged brother buried the body. The darkness and visual indistinctness of the landscape do not help; each spot looks the same as the others.

Meanwhile the men discuss a variety of topics, such as yoghurt, lamb chops, urination, family, spouses, ex-wives, death, suicide, hierarchy, bureaucracy, ethics, and their jobs. Philosophy is also discussed, with one apparently central and particular idea/theme mentioned a couple of times throughout the film—the idea that children invariably pay for their parents mistakes.

The prosecutor tells the doctor about a particularly mysterious death where a woman correctly predicted to her husband the exact date of her own death, which was a short time after she had given birth to a child. The doctor asks about the cause of death and the prosecutor says it was natural, a heart attack. The doctor then asks whether an autopsy was performed, and the prosecutor replies that there was no need as the cause of death was obvious and unsuspicious. The doctor suggests that it may have been a self-induced heart attack with the use of drugs and therefore a suicide.

Before dawn the prosecutor gets hungry, and the group stops at a nearby village to eat. The electric power goes off, and the hosts daughter silently brings the men tea on a tray, with a lamp on the tray lighting her face. Several of the men, including Kenan, are struck by her beauty. After the meal Kenan reveals what happened the night of the killing while drunk he let slip the secret that the victims son was actually his, and then things got ugly.

Daylight breaks. The body is found and taken to the local hospital for autopsy. The mother and son (perhaps 12 years old) are waiting outside the hospital. The son throws a stone at Kenan hitting him between the eyes. Kenan cries.

At the hospital the prosecutor again discusses the woman who predicted her own death with the doctor. They further discuss the possibility of suicide, where it is established that a certain prescription drug could have been used to induce the heart attack. The prosecutor is familiar with the drug as his father-in-law took it for his heart problems. Possible reasons for suicide are also discussed, and the two come to a possible motive—her husbands confirmed infidelity. At the end of the discussion the prosecutors behavior suggests that the woman may have been his own wife.

The prosecutor invites the victims wife to identify the body in the hospital morgue, files the necessary paperwork, and departs, leaving the doctor to perform the autopsy. The autopsy reveals the presence of soil in the lungs, implying that the victim had been buried alive, but the doctor intentionally omits that from the report.

The movie ends with a shot from the doctors perspective of the mother and son in the distance walking away with the husbands belongings. The son sees that a football has been accidentally kicked far from a schoolyard and he runs and retrieves it and kicks it back to the children in the yard. He then runs back to his mother.

==Cast==
* Muhammet Uzuner as Doctor Cemal
* Yılmaz Erdoğan as Commissar Naci
* Taner Birsel as Prosecutor Nusret
* Ahmet Mümtaz Taylan as Chauffeur Arap Ali
* Fırat Tanış as Suspect Kenan
* Ercan Kesal as Mukhtar
* Cansu Demirci as Mukhtars Daughter
* Erol Eraslan as Murder Victim Yaşar
* Uğur Arslanoğlu as Courthouse Driver Tevfik
* Murat Kılıç as Police Officer İzzet
* Şafak Karali as Courthouse Clerk Abidin
* Emre Şen as Sergeant Önder
* Burhan Yıldız as Suspect Ramazan
* Nihan Okutucu as Yaşars wife Gülnaz

==Production==
Director Nuri Bilge Ceylan grew up in a small town similar to the one in the film in terms of mentality and hierarchy, and says he feels a close connection to the characters depicted. The story is based on real events. One of Ceylans co-writers was an actual doctor, and, in order to attain his license, had been required to work for two years in the town where the plot is set. The story in the film is based on very similar events the co-writer experienced during this period. The title of the film references Sergio Leones film Once Upon a Time in the West, and was something one of the drivers uttered during the actual events. When writing the screenplay, the filmmakers tried to be as realistic as possible, and the main aim was to portray the special atmosphere, which had left a strong impression on the doctor. A number of quotations from stories by Anton Chekhov were incorporated in the script.    Central Anatolia.  It was shot in the CinemaScope format. 

==Awards==
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! scope="col" | Award
! scope="col" | Date of ceremony
! scope="col" | Category
! scope="col" | Nominee(s)
! scope="col" | Result
|-
! scope="row" rowspan="5" | Asia Pacific Screen Award  
| rowspan="5" | 24 November 2011 Achievement in Cinematography
| Gökhan Tiryaki
|  
|- Achievement in Directing
| Nuri Bilge Ceylan
|  
|- Best Feature
| Zeynep Özbatur Atakan Mirsad Purivatra Eda Arikan İbrahim Şahin Müge Kolat Murat Akdilek Nuri Bilge Ceylan
|  
|- Best Screenplay
| Ercan Kesal Ebru Ceylan Nuri Bilge Ceylan
|  
|- Jury Grand Prize
|
|  
|-
! scope="row" rowspan="2" | Cannes Film Festival  22 May 2011 Grand Prix  
| rowspan="2" | Nuri Bilge Ceylan
|  
|-
| Palme dOr
|  
|-
! scope="row" | Chicago Film Critics Association 17 December 2012
| Best Foreign Language Film
|
|  
|-
! scope="row" | Cinemanila International Film Festival 
| 17 November 2011
| Main Competition - Best Director
| Nuri Bilge Ceylan
|  
|-
! scope="row" rowspan="2" | Dubai International Film Festival 
| rowspan="2" | 7–14 December 2011
| Muhr AsiaAfrica Feature: Best Cinematographer
| Gökhan Tiryaki
|  
|-
| Muhr AsiaAfrica Feature: Special Jury Prize
| Nuri Bilge Ceylan
|  
|-
! scope="row" rowspan="2" | European Film Awards   1 December 2012 Best Cinematographer
| Gökhan Tiryaki
|  
|- Best Director
| Nuri Bilge Ceylan
|  
|-
! scope="row" | Independent Spirit Awards 23 February 2013 Best Foreign Film
|
|  
|-
! scope="row" | Jameson Dublin International Film Festival
| 26 February 2012
| Best Director
| rowspan="3" | Nuri Bilge Ceylan
|  
|-
! scope="row" | Karlovy Vary International Film Festival 
| 9 July 2011
| NETPAC Award
|  
|-
! scope="row" rowspan="2" | London Film Critics Circle 20 January 2013 Director of the Year
|  
|-
| Foreign Language Film of the Year
| rowspan="2"|
|  
|-
! scope="row" | New York Film Critics Circle 3 December 2012 Best Foreign Language Film
|  
|- Olso Films from the South Festival
| 16 October 2011
| Best Feature
| rowspan="2" | Nuri Bilge Ceylan
|  
|-
! scope="row" | Philadelphia Film Festival 
| 29 October 2011
| Audience Award Honorable Mention Masters
|  
|}

==Reception==
The film has been met with critical acclaim. Review aggregation website Rotten Tomatoes gives the film a score of 94% based on 70 reviews, with an average rating of 8.2/10,  while Metacritic gives a weighted average rating of 82 based on reviews from 21 critics, indicating "universal acclaim."  

Dave Calhoun reviewed the film for   and Climates (film)|Climates. ... Beyond being chronological, the film follows no obvious storytelling pattern. Things happen when they do and at a natural rhythm. ... Ceylan invites us along for the ride – but only if were up for it." 
 Grand Prix, in a shared win with the film The Kid with a Bike by the Dardenne brothers. 

The film was selected as Turkeys official submission for the Academy Award for Best Foreign Language Film,       but did not make the shortlist. 

Sight & Sound film magazine listed the film at #8 on its list of best films of 2012.  Stephen Holden, film critic for The New York Times, called Once Upon a Time in Anatolia the sixth best movie of 2012 and "a searching reflection on the elusiveness of truth." 

==See also==
* 2011 in film
* Cinema of Turkey
* Turkish films of 2011
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Turkish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 