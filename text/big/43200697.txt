Treasure of the Silver Lake
{{Infobox film
| name           = Treasure of the Silver Lake
| image          = 
| alt            =  
| caption        = 
| director       = Harald Reinl
| producer       = Horst Wendlandt
| writer         = Karl May (novel) Harald G. Petersson (screenplay)
| starring       = Lex Barker Pierre Brice
| music          = 
| cinematography = Ernst W. Kalinke
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
Treasure of the Silver Lake ( ) is a 1962 German western film directed by Harald Reinl. It is based on the story by Karl May.

== Cast ==
*  
*  
* Götz George: Fred Engel
* Herbert Lom: Colonel Brinkley
* Karin Dor: Ellen Patterson
* Eddi Arent: Lord Castlepool
* Marianne Hoppe: Mrs. Butler
* Ralf Wolter: Sam Hawkens
* Mirko Boman: Gunstick Uncle

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 