The Fearless Hyena
 
 
{{Infobox film
| name           = The Fearless Hyena
| image          = Fearlesshyena.jpg
| caption        = Hong Kong DVD cover
| film name = {{Film name| traditional    = 笑拳怪招
 | simplified     = 笑拳怪招
 | pinyin         = Xiǎo Quán Guài Zhào
 | jyutping       = Siu3 Kyun4 Gwaai3 Ziu1}}
| director       = Jackie Chan   Kenneth Tsang
| producer       = Hsu Li Hwa
| writer         = Jackie Chan   Kenneth Tsang James Tien
| music          = Frankie Chan   Chen Hsua-Chi
| cinematography = Chen Yung Shu
| editing        = Liang Yung Tsan
| studio         = Goodyear Movie Company
| distributor    = Lung Cheung Company Limited
| released       =  
| runtime        = 97 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}}
   1979 Cinema Hong Kong kung fu film directed by and starring Jackie Chan. The film was co-directed by Kenneth Tsang.

The film has been released on several alternative titles internationally, including:
* Revenge of the Dragon (USA video title)
* Superfighter 3 (West Germany video title)
* The Shadowman (West Germany video title)

==Plot== James Tien). Lung does not take his training seriously enough, he gambles, and he gets into fights which lead him to display the skills his grandfather has told him he must keep secret.

Lung briefly finds employment selling coffins, working for an unscrupulous proprietor (Dean Shek), who even stoops to selling second-hand coffins.  Lung is fired when he accidentally traps his boss in one of the coffins.  After making his escape, he runs into three thugs hed beaten up earlier, who ask him to teach them kung fu.  Lung meets their sifu, Ti Cha (Lee Kwan), the unskilled leader of the Everything Clan.  Master Ti offers Lung a lucrative job training his students and fighting against the top fighters from rival schools.  This boosts the reputation of the school and of the scheming Master Ti.  However, Lung makes the mistake of naming the school under the Sien Yi clan name.  This comes to the attention of evil kung fu master Yen Ting Hua (Yam Sai-kwoon), who finds and kills Lungs grandfather.  But, Lung eventually takes revenge for his grandfathers murder after undergoing rigorous training from The Unicorn (Chan Wai-Lau).

==Cast==
*Jackie Chan - Shing Lung  James Tien - Grandfather
*Dean Shek - The Coffin Seller (as Shih Tien)
*Chen Hui Lou 
*Yen Shi-Kwan - Yen
*Lee Kwan - The Master
*Cheng Tien-Chi - "The Willow Knife" Bar Tar
*Chih-ping Chiang
*Shao Hua Chu
*Eagle Han-ying
*Hsing Nan Ho
*Hong Hsu
*Huang Kan-man
*Sae Ok Kim
*Kuo Nai-hua
*Chang Ma
*Kang Peng
*Wang Yao
*Wan Li-peng
*Chi Sang Wong

==Fight scenes== mentally retarded man, disguised as a woman, and using "Emotional Kung-Fu", a style that involves vividly displaying the emotions of anger, sorrow, joy and happiness to find the opponents weakness thus fighting whilst crying or laughing.

==Inspirations==
This film served as the inspiration behind Akira Toriyamas Dragon Ball manga. 

==Box office==
The film grossed a HK $5,445,535 at the Hong Kong box office. 

==See also==
* Jackie Chan filmography
* List of films in the public domain

==External links==
*   at Hong Kong Cinemagic
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 