Stage Door Canteen
{{Infobox film
| name          = Stage Door Canteen
| image         = Stage Door Canteen poster.jpg
| image size    = 
| caption       = Theatrical release poster
| director      = Frank Borzage
| producer      = Frank Borzage Sol Lesser
| writer        = Delmer Daves William Terry Marjorie Riordan Lon McCallister Margaret Early James Monaco Fred Rich Richard Rodgers Harry Wild
| distributor   = United Artists
| released      =  
| runtime       = 132 minutes
| country       = United States
| language      = English
| budget        =
| gross         = $4,350,000 (US/Canada rentals)   {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p189 
}}
Stage Door Canteen (1943) is a musical film produced by Sol Lesser Productions and distributed by United Artists. It was directed by Frank Borzage and features many cameo appearances by celebrities. The majority of the film is a filmed concert although there is also a storyline to the film. Black Producer Leonard Harper was hired to do the African-American casting in New York City.

Stage Door Canteen is in the public domain in North America and for this reason is widely available in many DVD and VHS releases of varying quality.

==Background and production==
The film, made in wartime, celebrates the work of the Stage Door Canteen, created in New York City as a recreational center for both American and Allied servicemen on leave to socialize with, be entertained or served by theatrical celebrities.  It was made under the auspices of The American Theatre Wing.

The real Stage Door Canteen was a basement club located in the 44th Street Theatre in New York City.  It could not be used for the filming as it was too busy receiving real servicemen. It was recreated in New York and at the RKO Radio Pictures studio in Culver City. There also was a Stage Door Canteen in the Academy of Music Building in Philadelphia.
 Hollywood Canteen. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 106 

==Plot==
The storyline of the film follows several women who volunteer for the Canteen and who must adhere to strict rules of conduct, the most important of which is that while their job is to provide friendly companionship to and be dance partners for the (often nervous) men who are soon to be sent into combat, no romantic fraternization is allowed. One volunteer who confesses to only becoming involved in the Canteen in order to be discovered by one of the Hollywood stars in attendance, ultimately finds herself falling in love with one of the soldiers. 

Star appearances range from momentary cameos, such as Johnny Weissmuller, seen working in the canteens kitchen, to more substantial roles such as Katharine Hepburn, who helps advance the plot.

Most of the cameos were filmed at the studio, but a number of spots -- Benny Goodmans, for example -- were filmed in New York City.

==Cameos (alphabetical order)==
 
 
 
(featured performer) means either they performed a number or a comedy bit+ or had extended dialogue in the plot++
*Judith Anderson
*Henry Armetta Kenny Baker (featured performer+)
*Tallulah Bankhead
*Ralph Bellamy
*Jack Benny
*Edgar Bergen (with Charlie McCarthy and Mortimer Snerd) (featured performer+)
*Ray Bolger (featured performer+)
*Helen Broderick
*Ina Claire
*Katharine Cornell (featured performer - her only film appearance++)
*Lloyd Corrigan
*Jane Darwell
*William Demarest
*Gracie Fields (featured performer+)
*Arlene Francis
*Virginia Grey
*Helen Hayes
*Katharine Hepburn (featured performer; Hepburns only co-starring effort in a musical, even though she doesnt sing++)
*Hugh Herbert
*Jean Hersholt Sam Jaffe
*Allen Jenkins George Jessel (featured performer+)
*Otto Kruger
*Gertrude Lawrence
*Gypsy Rose Lee (featured performer+)
*Alfred Lunt and Lynn Fontanne
*Bert Lytell
*Aline MacMahon
*Harpo Marx
*Elsa Maxwell
*Helen Menken
*Yehudi Menuhin (featured performer+)
*Ethel Merman (featured performer+)
*Peggy Moran
*Alan Mowbray
*Paul Muni
*Merle Oberon
*Franklin Pangborn
*Brock Pemberton
*George Raft
*Selena Royle (featured performer++)
*Lanny Ross (featured performer+)
*Martha Scott
*Cornelia Otis Skinner
*Ned Sparks
*Bill Stern
*Ethel Waters (featured performer+)
*Johnny Weissmuller
*Dame May Whitty
*Ed Wynn (featured performer+)
Orchestras: (all featured performers)
*Count Basie (with Ethel Waters as featured vocalist+) Lina Romay as featured vocalist+)
*Benny Goodman (with Peggy Lee as featured vocalist+)
*Kay Kyser +
*Guy Lombardo +
*Freddy Martin +

==Reception==
The film made an estimated profit of $2.5 million. MONOGRAM BIDS FOR THE BIG LEAGUE: Sets $500,000 Budget, a Studio Record, for One Film -- Other Items
By FRED STANLEYHOLLYWOOD.. New York Times (1923-Current file)   26 Sep 1943: X3 
==References==
 

==External links==
 
*  
*  
*  
*  

http://www.rhythmforsale.com/

http://www.amazon.com/Rhythm-Sale-Grant-Harper-Reid/dp/0615678289
 

 
 
 
 
 
 
 