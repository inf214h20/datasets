Unbecoming Age
{{Infobox Film
| name           = Unbecoming Age
| image          = The Magic Bubble DVD cover.jpg
| image_size     = 
| caption        = The Magic Bubble DVD cover
| director       = Alfredo Ringel Deborah Ringel
| producer       = Alfredo Ringel Deborah Ringel
| writer         = Meridith Baer Geof Prysirr
| narrator       = 
| starring       = Diane Salinger Colleen Camp George Clooney
| music          = Jeff Lass
| cinematography = Harry Mathias
| editing        = Alan James Geik
| distributor    = Castle Hill Productions (theatrical) Monarch Video (home media)
| released       = 1992
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Unbecoming Age, also known as The Magic Bubble, is a 1992 American comedy film starring George Clooney  and Diane Salinger. 
The film tells the story of woman who, for her 40th birthday, wishes to forget how old she is. A bottle of magic bubbles makes her wish come true, instantly transforming Julia into an ageless and happy woman.

==Cast==
* Diane Salinger &mdash; Julia John Calvin &mdash; Charles
* Colleen Camp &mdash; Deborah
* Priscilla Pointer &mdash; Grandma
* George Clooney &mdash; Mac
* Shera Danese &mdash; Letty
* Nicholas Guest &mdash; Dooley

==Critical reception==
On her The Movie Archive Review website, Marjorie Johns awarded the film a score of six out of ten, stating that the films "fantasy elements work well" and concluding that "its a sweet natured, unpretentious little movie and not Martin Scorsese|Scorsese". 

Film critic Todd McCarthy of Variety (magazine)|Variety based his negative review of the film on the fact that "the fantasy element...is exasperatingly tame", claiming that the screenwriters "have kept their imaginations too inhibited and domesticated where greater flights of fancy would have been welcome". McCarthy did agree that his "sympathy goes out to Salinger" for her ability to "remain somewhat likable" and that a "number of talented thesps brighten up the supporting cast" even though "few will count this among their more stellar credits". 

Critic Peter Rainer of Los Angeles Times summarized his opinion with the opening sentence of his review titled "Magic Bubbles Cant Help Un-Becoming". He stated: "The comic premise of Un-Becoming Age...doesnt make a whit of sense" 

==See also==
* 1992 in film
* United States
* List of American films of 1992

==Notes==
 

==External links==
* 

 
 
 
 


 