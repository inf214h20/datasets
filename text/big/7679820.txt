Andhrudu
{{Infobox film
|  name     = Andhrudu |
  image          = | Gopichand Gowri Pandit Pavan Malhotra K. Viswanath Sayaji Shinde |
  writer           = paruchuri murali  |
  director         = Paruchuri Murali |
  producer         = Paruchuri Murali |
  distributor      = |
  released   = 19 August 2005 |
  editing          = Marthand K. Venkatesh | Telugu |
  music            = Kalyani Malik |
  budget           = |
}} Telugu film released on 19 August 2005 and was directed by Paruchuri Murali. It starred T. Gopichand|Gopichand, Gowri Pandit, K. Viswanath, Sayaji Shinde, and many others. Kalyani Malik, brother of M.M. Keeravani, scored music for the film. The film was also dubbed into Tamil as "Ivanthanda Police."This film is also dubbed in hindi as "Loha-The Ironman".

== Plot ==

Surendra (T. Gopichand) is an honest and hot-blooded cop. Archana (Gowri Pandit) is the daughter of police commissioner. They meet accidentally and their relationship blossoms into love. Archanas father is of Bihar origin. Parents settle the match of Surendra and Archana. When they are about to get engaged, Surendra expresses his reservations due to Archana overhearing her fathers conversation with his long estranged sister on the subject of Archana and Sinha marrying. Surendra takes the blame for the destroyed relationship and Archana goes back to Bihar to wed her cousin Sinha (Salim Baig). The rest of the story is all about how Surendra goes to Bihar and wins Archana back.

== Cast == Gopichand ... Surendra
* Gowri Pandit ... Archana
* Sayaji Shinde ...
* Salim Baig ... Sinha
* K. Viswanath ... Father of Surendra
* Pawan Malhotra ... Father of Archana

==External links==
*  

 
 
 


 