Auditions (film)
{{Infobox film
| name           = Auditions
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Harry Hurwitz
| producer       = Charles Band
| writer         = Albert Band Charles Band
| narrator       =
| starring       = Linnea Quigley Greg Travis
| music          = Richard Band Joel Goldsmith
| cinematography =
| editing        = Emil Haviv
| studio         = Charles Band Productions
| distributor    = Wizard Distributors
| released       =
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Auditions is a 1978 film directed by Harry Hurwitz.

==Plot==

A pseudo-documentary about the processes involved in putting together and casting actors and actresses for a porn film. Although several actual porn stars are in the film, it is not a porn film itself.

==Cast==
* Linnea Quigley as Sally Webster
* Adore OHara as Adore OHara
* Rhonda Jo Petty as Patty Rhodes 
* Jennifer West as Melinda Sale 
* William Margold as Larry Krantz
* Ric Lutze as Ron Wilaon
* Cory Brandon as Van Scott
* Rick Cassidy as Charlie White
* Greg Travis as Unknown 
* Maria Lutra as Jenny Marino

==DVD release==

Full Moon re-released the film on DVD in 2011, as part of their "Full Moons Grindhouse" series of exploitation films on DVD.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 