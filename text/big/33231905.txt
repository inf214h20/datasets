China Doll (film)
{{Infobox film
| name           = China Doll
| image          = Chinadoll poster.jpg
| image_size     =
| caption        = Theatrical (lobby) poster
| director       = Frank Borzage
| producer       = Frank Borzage
| writer         = Kitty Buhler Thomas F. Kelly (story) James Benson Nablo (story)
| narrator       =
| starring       = Victor Mature Li Hua Li
| music          = Henry Vars
| cinematography = William H. Clothier	 Jack Murray	 
| studio         = Batjac Productions Romina Productions
| distributor    = United Artists (US)
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
China Doll (a.k.a. Time Is a Memory) is a 1958 romantic drama film set in the China Burma India Theater of World War II and starring Victor Mature and Li Hua Li. It represented a return to films for director Frank Borzage who had taken a 10-year hiatus before tackling this poignant, yet "offbeat" film.  

==Plot==
In 1943, Captain Cliff Brandon (Victor Mature) is a cargo aircraft pilot supplying the Allied troops fighting the Japanese in China. When he is not flying or training his new crew hard, he is usually drinking in the local bar.

One night, while stumbling home drunk, he encounters an old Chinese man who offers him a girl, his daughter Shu-Jen (Li Hua Li). Brandon pays him, but when he sees the young woman, he tells the old man to keep her. When he wakes up the next morning, he finds Shu-Jen there. After Father Cairns (Ward Bond), a longtime resident of China, expresses his disapproval, Brandon tries his best to get rid of her, assigning the task to Ellington, a young Chinese boy who speaks English well. Ellington tries to sell her into prostitution, but Father Cairns happens by and takes Shu-Jen back to Brandon.

The priest finds out that Shu-Jens father was a farmer, but he lost his land to the Japanese invaders. Destitute, he sold his daughters services for three months to feed the rest of his large family. Cairns tells Brandon that, if he were to send the girl back, the old man would return the desperately needed money. So, over Brandons protests, the priest gets him to keep the girl; Brandon tells her that she is there only as a housekeeper. He makes Ellington his live-in interpreter.

Over time, however, love blooms, and Shu-Jen becomes pregnant. They get married in a traditional Chinese ceremony. After he is transferred to another base, she gives birth to their daughter. Later, they are reunited.

While Brandon is flying a mission, the base is attacked. The returning flight is ordered to divert to a different airfield, but Brandon disobeys and lands his aircraft. When he cannot locate his family, he orders his crew to leave with the survivors. Then he finds Shu-Jen and Ellington both dead, but his daughter is alive. He puts his dog tag around her neck, then mans an anti-aircraft gun and shoots down one or two enemy aircraft before he is killed.

In 1957, his former crewmates and their wives anxiously await the arrival in the United States of Brandons daughter, found in an orphanage by Father Cairns, still with her fathers dog tag.

 
  transport aircraft was featured predominantly in the film.]]

==Cast==
As appearing in China Doll, (main roles and screen credits identified):   IMDb. Retrieved: May 22, 2012. 
* Victor Mature as Capt. Cliff Brandon
* Li Hua Li as Shu-Jen
* Ward Bond as Father Cairns
* Bob Mathias as Capt. Phil Gates
* Johnny Desmond as Sgt. Steve Hill
* Stuart Whitman as Lt. Dan ONeill, Navigator (as Stu Whitman)
* Elaine Devry as Alice Nichols (as Elaine Curtis)
* Ann McCrea as Mona Perkins
* Danny Chang as Ellington
* Denver Pyle as Col. Wiley, Brandons commanding officer
* Don "Red" Barry as MSgt. Hal Foster
* Tige Andrews as Cpl. Carlo Menotti
* Steve Mitchell as Dave Reisner
* Ken Perry as Sgt. Ernie Fleming
* Ann Paige as Sally
* Gregg Barton as Airman
* Bill White, Jr. as Forsyth, Flying Tiger

==Production== Seventh Heaven Bad Girl A Farewell to Arms (1932).  During the 1940s, his films were not as well received and after the film noir, Moonrise (film)|Moonrise (1948), Borzage had stopped directing. China Doll marked his return to Hollywood, although he only completed one more film, The Big Fisherman (1959), while his last effort, LAtlantide (1961), had to be finished by others due to his illness. 
 Kunming Airfield, documentary footage from World War II was incorporated. Although aerial action in China Doll took a secondary role compared to the melodrama that predominated, the following aircraft were featured: 
* Bell P-39 Airacobra fighters
* Consolidated B-24 Liberator bombers
* Curtiss P-40 Warhawk fighters
* Douglas C-47 Skytrain transports 
* Grumman F6F Hellcat fighters
* Grumman TBF Avenger bombers
* Kawasaki Ki-61 fighter
* Mitsubishi Ki-21 medium bombers
* Mitsubishi Ki-51 light bomber
* Northrop A-17 bomber
* Republic P-47 Thunderbolt fighter

 

==Reception==
Considered a modest but interesting film, China Doll received favorable critical reviews. Variety noted the film had, "the warmth and humor of a romance between a burly air corps captain and a fragile oriental beauty."  Howard Thompson, reviewer for The New York Times found it "(a) familiar war drama (that) has some winning aspects. ... Under Mr. Borzages leisurely, gentle staging, the love story dominates the picture."  Thompson, Howard.   The New York Times, December 4, 1958. 

More recent reviews have treated China Doll as one of Borzages best and a fitting penultimate testament to his career. Parish and Stanke 1980, p. 469.  A lengthy review by Dan Callahan laid out the tropes of his earlier works were present: "China Doll is a delicate, spare, old mans movie, with quiet attention to character detail (even Ward Bonds priest is sensitive and thoughtful). Theres a melancholy, pessimistic slant to the dialogue that isnt lingered over; the movements of the actors and the compositions are so stylized and presentational that it almost feels, at magical times, like a silent film. The ending is surprisingly violent, even brutal, but in a brief coda, Borzage observes the regeneration of beauty in the couples child, even as he has shown the lovers bond and their kindness viciously wiped out by war." 

==References==
;Notes
 
;Citations
 
;Bibliography
 
* Dumont, Hervé. Frank Borzage: The Life and Films of a Hollywood Romantic. Jefferson, North Carolina: McFarland, 2009, First edition 2006. ISBN 978-0-786440-986. 
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Herzogenrath, Bernd.    Lanham, Maryland: Scarecrow Press, 2009. ISBN 0-81086-700-1. 
* Parish, James Robert and Don E. Stanke. The Swashbucklers. Highland City, Florida: Rainbow Books, 1980. ISBN 978-0-895080-066.
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 