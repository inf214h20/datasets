The Fugitive from Chicago
{{Infobox film
| name           = The fugitive from Chicago
| image          = 
| image_size     = 
| caption        =  Johannes Meyer
| producer       = Atalanta Film company, Berlin   Bavaria Film company, Munich
| writer         = Max W. Kimmich, Hermann Oberländer and Hans Martin Cremer after a novel of Curt J. Braun
| narrator       = 
| starring       = Gustav Fröhlich   Hubert von Meyerinck   Luise Ulrich   Lil Dagover
| music          = 
| cinematography = 
| editing        = 
| studio         = Bavaria Studios, Geiselgasteig near Munich
| distributor    = Bavarian film company, Munich   Hugo Engel Film company, Vienna   Fr. Lepka, Prague
| released       =  
| runtime        = 
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Johannes Meyer. Bavaria (situated in Geiselgasteig near Munich, where most scenes were shot) and Atalanta (situated in Berlin). Some scenes were shot at the German motorcycle company, Zündapp. It passed censorship on January 23, 1934, and was presented to the public on January 31, 1934, in Munich and on February 22, 1934, in Berlin.

== Story ==
 
Werner Dux, heir of the big car company "Dux-Werke", is a gambler who has just been arrested in Chicago for shooting a cardsharper. While in jail, he learns from his friend Michael Nissen that his father has died recently. That means that the Dux-Werke are without a leader at the moment, for Werners cousin Steffie, who is his co-heiress, is still too young to run the firm. In this situation, Werner convinces his friend Michael, who is an engineer by profession, to take over the firm as long as he is arrested and to pose as him. Michael agrees and is able to bring the firm up again. He even arranges a reliability test for motorcycles via Konstantinopel, Rome and Barcelona, which creates a big Turkish order for the company. Therefore, Steffie is looking up to him (whom she considers to be Werner) respectful. 

When the real Werner escapes from jail and arrives in Germany, it looks as if the whole story is up to bust. But Werner needs money, so he convinces Michael to go on like before and hand over all profits of the firm to him. But meanwhile, Michael has become interested in Steffie and feels bad by betraying her. So he went on holiday in a secluded lodge, and after his return, he is somehow changed. Steffie is worried he might have met another woman and at the presentation of a new car model, she demands an explanation from him, but Michael is not able to give her one. Now Werner arrives and begins to court Steffie, who does not recognize him. Still unhappy about Michaels behaviour, she follows Werner into his villa, but when he tries to approach her, she turns him back. Deeply offended, he tells her about his real name, and now Steffie understands why Michael has moved back from her. 

One day, Werner is shot dead, and Michael is suspected to be his murderer. The only one who believes in his innocence is Steffie. Shortly before his condemnation, the real murderer can be arrested. After his release, Michael disappears without a trace. Steffie, who has looked for him in vain, now hopes to meet him at the reliability test race, where their motorcycles have good prospects. Driven by ambition, Michael really appears there, and Steffie can finally embrace him.

== Staff ==

{| class="wikitable" border="1"
|-
! actor
! role
!
|-
| Gustav Fröhlich 
| Michael Nissen, engineer
| 
|-
| Hubert von Meyerinck
| Werner Dux, his friend
|- 
| Luise Ulrich
| Steffie Dux, Werners cousin
|-
| Lil Dagover
| Eveline
|- 
| Otto Wernicke
| Wolke, foreman
|- 
| Adele Sandrock
| Miss von Zackenthin
|-  Paul Kemp
| August P. Lemke, bookkeeper
|- 
| Willy Dohm
| Billy
|-
| Ernst Dumcke
| general manager von Oetten
|- 
| Lilo Müller
| Ruth Müller
|-
| Armand Zaepfel
| Smith, detective superintendent
|- Fritz Greiner
|- Max Weydner
|}

== Reception ==

The film, which was rated as "not suitable for adolescents" by filmcheckers of the propaganda ministry, did nevertheless receive the attribute "artistically" by the same institution. The attribute meant that the distributors and cinemas had to pay less entertainment tax when showing this film. It was also shown to many other European countries; the following year, it made his debut in Denmark on August 5 and in Finland on August 25. Foreign-language versions were also produced for Italy (L´evaso di Chicago), Greece (O asotos yios) and Czechoslovakia (Uprchlik z Chicaga). To US cinema-goers, the film was first shown on March 15, 1936.

==References==
Klaus, Ulrich J. : German sound films. Encyclopedia of the full-length German speaking feature films (1929 - 1945), sorted chronologically after their debut dates in Germany. - Ulrich J. Klaus. - Berlin   : 1934.

 
 
 
 