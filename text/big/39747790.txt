Pawns of Passion
{{Infobox film
| name           = Pawns of Passion
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Bieganski   Carmine Gallone
| producer       = 
| writer         = Carmine Gallone   Norbert Falk
| narrator       = 
| starring       = Olga Tschechowa   Harry Frank   Hans Stüwe   Henri Baudin
| music          = Werner Schmidt-Boelcke   
| editing        = 
| cinematography = Victor Arménise   Mutz Greenbaum
| studio         = Erda-Film 
| distributor    = Deutsche Universal
| released       = 8 August 1928
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Wiktor Bieganski and Carmine Gallone and starring Olga Tschechowa, Harry Frank and Hans Stüwe. The film was released in the United States in 1929.  The film is also known by several other titles including Liebeshölle.

==Cast==
* Olga Tschechowa as Ala Suminska 
* Harry Frank as Ihr Mann, Offizier 
* Hans Stüwe as Bruno Bronek, Maler 
* Henri Baudin as Pierre, Offizier 
* Oreste Bilancia as Jean, Bildhauer 
* Helmuth Krauß as Viktor, Maler 
* Angelo Ferrari as Robert, Maler 
* Lola Josane as Lolotte, Modell
* Sofia Szuberla as Alas Kind 
* Max Maximilian as Ein Bauer 
* Sylvia Torf as Eine Bäuerin  
* Diana Karenne   
* Sidney Suberly

==References==
 

==Bibliography==
*Helker, Renata. Die Tschechows: Wege in die Moderne. Deutsches Theatermuseum München, 2005.
* Street, Sarah. Transatlantic Crossings: British Feature Films in the United States. Continuum, 2002.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 