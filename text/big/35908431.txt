Forbidden (1953 film)
{{Infobox film
| name           = Forbidden
| image          = Forbidden poster small.jpg
| image size     = 220px
| alt            =
| caption        = Theatrical release lobby card
| director       = Rudolph Maté
| producer       = Ted Richmond
| screenplay     = Gil Doud William Sackheim
| story          = William Sackheim starring        = Tony Curtis Joanne Dru
| music          = Frank Skinner
| cinematography = William H. Daniels
| editing        = Edward Curtiss
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} thriller film noir directed by Rudolph Mate, featuring Tony Curtis and Joanne Dru. 

==Plot==
A man is hired by Chicago mobster to find a woman he once knew.  He traces her to Macao where complications arise.

==Cast==
* Tony Curtis as Eddie Darrow
* Joanne Dru as Christine Lawrence Manard
* Lyle Bettger as Justin Keit
* Marvin Miller as Cliff Chalmer
* Victor Sen Yung as Allan Chung
* Alan Dexter as Bernard "Barney" Pendleton David Sharpe as Henchman Leon
* Peter Mamakos as Sam
* Howard Chuman as Hon-Fai
* Weaver Levy as Tang
* Harold Fong as Wong
* Mai Tai Sing as Soo Lee
* Mamie Van Doren as Singer

==References==
 

==External links==
*  
*  
*  
*  
*   (Mamie Van Doren performance, dubbed by Virginia Rees

 

 
 
 
 
 
 
 
 
 
 


 