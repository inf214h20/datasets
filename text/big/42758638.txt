The Swedish Nightingale (film)
{{Infobox film
| name =  The Swedish Nightingale 
| image =
| image_size =
| caption =
| director = Peter Paul Brauer
| producer =  Ernst Günter Techow
| writer =  Friedrich Forster-Burggraf (play)   Henry Lemarchand   Per Schwenzen   Gert von Klaß 
| narrator =
| starring = Ilse Werner   Karl Ludwig Diehl   Joachim Gottschalk   Aribert Wäscher
| music = Franz Grothe  
| cinematography = Ewald Daub  
| editing =  Alice Ludwig     
| studio = Terra Film
| distributor = Terra Film
| released = 9 April 1941  
| runtime = 97 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Swedish Nightingale (German:Die schwedische Nachtigall) is a 1941 German musical film directed by Peter Paul Brauer and starring Ilse Werner, Karl Ludwig Diehl and Joachim Gottschalk.  The film is based on a play by Friedrich Forster-Burggraf set in nineteenth century Copenhagen. It portrays a romance between the writer Hans Christian Andersen and the opera singer Jenny Lind the "Swedish Nightingale" of the title.

==Cast==
* Ilse Werner as Jenny Lind  
* Karl Ludwig Diehl as Count Rantzan  
* Joachim Gottschalk as Hans Christian Andersen  
* Aribert Wäscher as Peer Upän  
* Marianne Simson as Karin Nielsson  
* Hans Leibelt as Theatre Director  
* Emil Heß as Thorwaldsen  
* Hans Hermann Schaufuß as Orchestra Conductor  
* Volker von Collande as Olaf Larsson  
* Käte Kühl as Frl. Rindom, Sängerin  
* Ruth Lommel as Eine Debütantin  
* Elga Brink as Gräfin Ebba Douglas  
* Erich Dunskus as Postmeister  
* Angelo Ferrari as Italienischer Gastwirt  
* Werner Stock as Prinz Schweinehirt  
* Jakob Tiedtke as Kaiser 
* Wilfried Seyferth as Hofjunker  
* Alwin Lippisch as Leibarzt  
* Charlotte Schellhorn as Küchenmädchen 
* Ernst Sattler as Axel Lind  
* Jeanette Bethge as Frau Tostrup, Andersens Haushälterin  
* Siegfried von Geldern as Tenor  
* Erwin Hoffmann as Ballettmeister  
* Walter Bechmann as Theatersekretär  
* Franz Stein as Hofuhrmacher  
* Bernhard Goetzke as Tod  
* Erna Berger as Solostimme und Stimme der Nachtigall 
* Lillie Claus as Singer 
* France Clery as Singer  
* Karl Hellmer
* Rudolf Schündler  
* Ingeborg Albert 
* Curt Cappi  
* Elsa Andrä Beyer 
* Franz Arzdorf 
* Max Dietze Irene Fischer 
* Gustl Kreusch 
* Peter C. Leska 
*Willy Melas 
* Hans Reiners 
* Ernst Rotmund 
* Otto Sauter-Sarto Karl Wagner 
* Hanns Waschatko
* Bruno Ziener

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 