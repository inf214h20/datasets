Yes, But...
{{Infobox film
| name           = Yes, But...
| image          = Oui, mais.jpg
| image size     =
| caption        =
| director       = Yves Lavandier
| producer       = François Kraus  Denis Pineau-Valencienne
| writer         = Yves Lavandier
| narrator       =
| starring       = Gérard Jugnot  Émilie Dequenne
| music          = Philippe Rombi
| cinematography = Pascal Caubère
| editing        = Dominique Pétrot
| distributor    =
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         =
| preceded by    =
| followed by    =
}}
Yes, But... ( ) is a 2001 French comedy film written and directed by Yves Lavandier, dealing with brief therapy and teenage sexuality.

==Plot==
Attracted but also frightened by her sexuality, a teenage girl undergoes a brief therapy with a warm, humorous and competent psychotherapist.

==Cast==
*Émilie Dequenne as Eglantine Laville
*Gérard Jugnot as Erwann Moenner
*Alix de Konopka as Mme Laville
*Cyrille Thouvenin as Sébastien
*Patrick Bonnel as M. Laville
*Vanessa Jarry as Françoise

==Background==
Yes, But... shows a  , Gestalt therapy|gestalt, Ericksonian hypnosis, systemic therapy, paradoxical prescriptions, humor, Creative visualization|visualisation.
 Brief Therapy Center in Palo Alto, called Yes, But... "a gem". Psychotherapist Alan D. Entin says Yes, But... gives a very accurate portrayal of his occupation.

==External links==
*  
*  
*  
*  

 
 
 
 

 