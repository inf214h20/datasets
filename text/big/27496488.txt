Honey 2
{{Multiple issues|
 
 

}}
{{Infobox film
| name = Honey 2

| image = Honey two.jpg
| caption = Dutch theatrical poster
| director = Bille Woodruff
| producer = Paul Hellerman
| screenplay = Blayne Weaver Alyson Fouse
| starring = Katerina Graham Randy Wayne Seychelle Gabriel Alexis Jordan Audrina Patridge Mario Lopez
| music = Sam Retzer Tim Boland  David Klein
| editing = Paul Millspaugh
| distributor = Universal Studios
| released =  
| runtime = 110 minutes
| country = United States
| language = English
| gross = $8,775,693 (foreign)  
}}

Honey 2 is a dance film which is a sequel to the 2003 film Honey (2003 film)|Honey, directed by Bille Woodruff, who directed the original film. It stars Katerina Graham, Randy Wayne, Melissa Molinaro,  Lonette McKee (reprising her role from Honey) and Audrina Patridge. The film was released to cinemas in Britain on June 10, 2011 and direct to DVD in North America. 

==Plot== 
 
A talented but very troubled young dancer tries to turn her life around by focusing on her one true passion, Dancing and trains a  young crew to compete against one of the best dance crews in the country in this sequel from original Honey director Bille Woodruff. 17 year old Maria Bennett (Kat Graham) decides to return to the Bronx, and become a professional dancer, after she gets out of juvie. With the support of Honeys mother Connie (Lonette McKee), Maria is able to get a decent job, and begins training in the same studio where she learned how dance. Later a volunteer Brandon recognizes Marias talent, and recruits her to help get a promising young dance crew known as the HDs into shape. But temptation soon shows up in the form of Marias ex-boyfriend Luis, the leader of the  718 crew, and a fierce dancer who doesnt take competition lightly. Recognizing her one chance to break free of Luis influence and stay positive, Maria joins the HDs and prepares to go toe-to-toe with reigning champs the 718s on national television. If Maria and her new crew can just come out on top, shell finally be able to cut her ties from the past, and follow in the footsteps of her idol Honey.

==Cast==
* Katerina Graham as Maria Ramirez 
* Randy Wayne as Brandon
* Melissa Molinaro as Carla
* Audrina Patridge as Melinda 
* Lonette McKee as Connie Daniels
* Christopher War Martinez as Luis
* Brittany Perry-Russell as Lyric
* Seychelle Gabriel as Tina
* Tyler Nelson as Darnell
* Casper Smart as Ricky
* Kallie Loudon as Julissa
* Gerry Bednob as Mr. Kapoor 
* Brandon Molale as Officer Gordon 
* Brandon Gonzales as Officer Blaine
* Rosero McCoy as Jonas
* Laurieann Gibson as Katrina
* Mario Lopez as Himself
* Alexis Jordan as Herself

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 