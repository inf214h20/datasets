Isn't Life Wonderful
{{Infobox film
| name = Isnt Life Wonderful
| image =
| caption =
| director = D.W. Griffith
| producer = D.W. Griffith
| writer = D.W. Griffith Geoffrey Moss (novel) Neil Hamilton
| music = Louis Silvers Cesare Sodero
| cinematography = Hendrik Sartov Harold S. Sintzenich
| editing =
| distributor = United Artists
| released =  
| runtime =  120 minutes
| language = 
| country = United States
| budget =
}}

Isnt Life Wonderful? (1924) is a silent film directed by D. W. Griffith for his company D. W. Griffith Productions, and distributed by United Artists. It was based on the novel by Geoffrey Moss and it went under the alternative title Dawn.
 Neil Hamilton.  The film was a failure at the box office, and led to Griffith leaving United Artists shortly after its run in theaters. 

The film did receive some positive critical notices at the time, but its stock has risen considerably since; it has for some decades been considered one of Griffiths greatest films. 

The title of the film was spoofed in the Charley Chase comedy Isnt Life Terrible (1925).

==Synopsis==
 Great Inflation. Neil Hamilton). Weakened by poison gas, Paul begins to invest in Ingas future and he serves as their symbol of optimism.

== Cast ==
*Carol Dempster (Inga) Neil Hamilton (Paul)
*Erville Alderson (The professor)
*Helen Lowell (Grandmother)
*Marcia Harris (The aunt)
*Frank Puglia (Theodor)
*Hans Adalbert Schlettow (Leader of the Workers)
*Paul Rehkopf (Hungry Worker)
*Walter Plimmer (The American)
*Lupino Lane (Rudolph)
*Robert Scholtz
*Dick Sutherland
*Louis Wolheim

== References ==
 
 

== External links ==
* 
* 
*30 minute abridgement   available for free download @  

 

 
 
 
 
 
 
 
 
 


 