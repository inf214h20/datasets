Ik Kudi Punjab Di
 
 
{{Infobox film
| name = IK KUDI PUNJAB DI
| image = Ik_Kudi_Punjab_Di_official_film_poster.jpg
| image_size = caption        = Official poster  Manmohan Singh
| producer =  
| writer =  
| starring =  
| music = Sukshinder Shinda Manmohan Singh|
| editing = Bunty Nagi
| released =  
| language = Punjabi
| country = India gross  =  
}}
 Manmohan Singh Manmohan Singh Manmohan Singh and Ratan Bhatia and stars Amrinder Gill, Jaspinder Cheema, Aman Dhaliwal, Gugu Gill, Gurpreet Ghuggi, Rana Ranbir, and Kimi Verma.

Ik Kudi Punjab Di released on 17 September 2010 globally. It was keenly awaited due to reasons like its theme, lead actor Amrinder Gill and the director. The director/writer/producer has attempted a very new and unique concept of drama/theatre in Ik Kudi Punjab Di which is not done much in Punjabi cinema.

==Plot==
Ik Kudi Punjab Di tells a richly textured tale from a keenly female perspective set against the backdrop of male-dominated Punjabi society. It does so with a Shakespearean credo of “all the world’s a stage” and a lively cast.

SP Singh (Amrinder Gill) is a boy from a wealthy family who meets Navdeep (Jaspinder Cheema), the girl of his dreams,  at his college drama class. They quickly bond, much to the chagrin of bad-boy student Vicky (Aman Dhaliwal) who prizes Navdeep for himself.

The male-chauvinist Vicky has no chance with the progressive-minded Navdeep. Even Singh, the man who she admires enough to call a friend, is in for a shock. Navdeep doesn’t want to get married; she’s intent on being the guardian of her loving parents because the family lacks a male heir. Few scene were also filmed in Post Graduate College Sector 11 Chandigarh.

Singh tests his own view of women by agreeing to all of Navdeep’s demands, including moving in with her family after marriage. This is seen as both revolutionary (by her classmates) and an affront to Punjabi society and tradition. 

==Cast==
*Amrinder Gill ... SP Pal
*Jaspinder Cheema ... Navdeep Sidhu
*Surbhi Jyoti.........
*Gugu Gill ... Professor Gill
*Gurpreet Ghuggi ... Bawa/Laali Baba
*Rana Ranbir Kanwaljit Singh ... Singhs father
*Deep Dhillon ... Sukhdev Singh
*Navneet Nishan
*Kimi Verma ... Laali
*Aman Dhaliwalas Param Dhillon 
*Neeta Mahindra
*Balwinder Begowal
*Tarshinder Soni
*Sukhbir Razia
*Surinder Sharma
*niyamat kaur... Roshni

==Music==
{{Infobox album  
| Name        = Ik Kudi Punjab Di
| Type        = Soundtrack
| Released    =  
| Artist      = Sukshinder Shinda, Amrinder Gill
| Cover       = Ik_Kudi_Punjab_Di_Soundtrack_CD_Cover.jpg
| Caption     = Indian release CD cover  Bhangra 
| Label       = Speed Records (India)  Moviebox (UK)
| Producer    = Sukshinder Shinda
| Length      =
| Lyrics      = Amardeep Gill, Rana Ranbir,& Others
| Last album  = Munde U.K. De (2009)
| This album  = Ik Kudi Punjab Di (2010)
| Next album  = Ajj Nachna (2012)
}}

The music of Ik Kudi Punjab Di was praised by audiences and critics alike. Singh Speaks particularly praised it by calling this album very good. Punjabi Portal also praised the album saying that the great music further increases the expectation from the film.

The album was released on Speed Records in India and Moviebox in the UK. The UK album artwork featured the stars of the film,  Amrinder Gill and Jaspinder Cheema, instead of the support characters pictured on the Indian cover artwork.

In October 2010, Amrinder Gill scored a Top 30 hit in the UK on the official Asian Download Chart  with the lead track from the film, "Sochan Vich."

===Track listing===
{|class="wikitable"
! Track
! Song
! Singer(s)
! Lyrics
! Duration
|-
| 1
| Sochan Vich
| Amrinder Gill
| Amardeep Gill & Jitt Salala
| 4.23
|-
| 2
| Ik Kudi Punjab Di
| Sukshinder Shinda & Amrinder Gill
| Amardeep Gill
| 4.08
|-
| 3
| Pyar Lai Ke
| Amrinder Gill
| Amardeep Gill
| 4.47
|-
| 4
| Salaaman
| Amrinder Gill
| Jhalman Singh Dhanda & Rana Madojhandia
| 4.02
|-
| 5
| Tera Mera Na
| Amrinder Gill & Pamela Jain
| Nimma Loharka
| 5.13
|-
| 6
| Dhin Tak Dhin
| Amrinder Gill, Labh Janjua, Rana Ranbir, Arvinder Singh & Bobby Sandhu
| Rana Ranbir
| 4.18
|-
| 7
| Gal Teri
| Amrinder Gill
| Harjit Sidaki
| 3.55
|-
| 8
| Socha Vich Remix
| Amrinder Gill
| Amardeep Gill & Jitt Salala
| 3.33
|}

==References==
 

==External links==
*  

 
 