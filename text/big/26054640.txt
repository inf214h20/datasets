Margie (film)
{{Infobox film
| name           = Margie
| image          = Margie poster.jpg
| image_size     =
| caption        = Theatrical release poster Henry King
| producer       = Walter Morosco
| writer         = F. Hugh Herbert Ruth McKenney Richard Bransten
| narrator       = 
| starring       = Jeanne Crain Glenn Langan Lynn Bari Alfred Newman
| cinematography = Charles G. Clarke
| editing        = Barbara McLean Twentieth Century-Fox Film Corporation
| released       = October 16, 1946
| runtime        = 94 minutes United States
| language       = English
| budget         = $1,680,000
| gross          = $4.1 million (US/ Canada rentals)  
| preceded_by    = 
| followed_by    = 
}}
 1946 Cinema American romantic Henry King.

==Plot==
Starting in 1946, Margie (Jeanne Crain ) is a housewife who looks back to her teenage life in the 1920s. Back then, she was a joyful, high-spirited girl living with her dominant but good-hearted grandmother McSweeney (Esther Dale). She did not have many friends, except for her neighbor Marybelle Tenor (Barbara Lawrence), an outgoing teen who had a relationship with the handsome but dimwitted jock Johnny Green (Conrad Janis). Margie secretly had a small crush on Johnny as well, but she was more smitten with Prof. Ralph Fontayne (Glenn Langan), a popular professor who taught French at her high school. Meanwhile, fellow teen Roy Hornsdale (Alan Young) was in love with Margie and attempted to court her, without any luck. Her grandmother thought he was a suitable partner for her, but Margie had more interest in either John or Mr. Fontayne.

One day, Margie entered a debate competition. There, she was reunited with her father (Hobart Cavanaugh), who lived apart from her. Not much later, Margie went ice skating with her friends, when she suddenly fell. She was helped by Mr. Fontayne, who then realized how special Margie was. When the homecoming dance was finally nearing, Margies date Roy was not allowed to attend the dance. Margie was devastated, but her grandmother assured her that she had arranged a mysterious substitute. McSweeney contacted Margies father to attend the dance with his daughter. However, before he was able to arrive Mr. Fontayne stopped by, to tell Margie how well she did on her latest paper.

Margie, who did not know who was replacing Roy as her date, mistook Mr. Fontayne for being the substitute. When Fontayne explained he is merely stopping by for the compliment and that he is actually taking Miss Palmer (Lynn Bari), who works at the school library, to the dance, Margie burst out in tears. In the end, she decided to attend the dance anyway, escorted by her father. Fast forward to the present, it turns out Margie is married to Fontayne, who now is the principal at the same high school.

==Cast==
* Jeanne Crain as Marjorie Margie MacDuff (singing voice was dubbed by Louanne Hogan)
* Glenn Langan as Professor Ralph Fontayne
* Lynn Bari as Miss Isabel Palmer
* Alan Young as Roy Hornsdale
* Barbara Lawrence as Marybelle Tenor
* Conrad Janis as Johnny Johnikins Green
* Esther Dale as Grandma McSweeney
* Hobart Cavanaugh as Mr. Angus MacDuff
* Ann E. Todd as Joyce, Margies teenage daughter
* Hattie McDaniel as Cynthia

==Production==
In January 1945, 20th Century Fox paid $12,500 for a story written by Ruth McKenney and her husband Richard Bransten.    For the screenplay adaption, F. Hugh Herbert used elements from the film Girls Dormitory (1936). 

The male lead was initially offered to Cornel Wilde, but he refused it and was put on suspension by the studio.  Next, Richard Jaeckel was announced as the male lead, but he was eventually replaced by Glenn Langan. 

Set decorations include the 1794 Thomas Lawrence painting, Pinkie (Lawrence painting)|Pinkie, which can be seen in the home of Margie and her grandmother, located on the wall in the sitting room.

The film was shot in Reno, Nevada. The exteriors of "Central High" are actually the University of Nevada, Reno. In some shots, the snow covered Sierras can be seen. This film is considered an excellent example of the Technicolor film process.

IN the opening scene, one of the songs played on the old phonograph is, "At Sundown".

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 