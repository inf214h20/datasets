Immortally Yours
{{Infobox Film
| name           = Immortally Yours
| image          = 
| caption        = 
| director       = Joe Tornatore
| producer       = Frank D. Russo Katherine Hawkes Joe Tornatore
| writer         = Katherine Hawkes  Daniel Goddard Eric Etebari
| music          = Gordon McGhie
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
}}  Daniel Goddard and Eric Etebari.

==Plot==
During a night at the opera, Estelle Henderson is embarrassed by her drunken fiance, John, but also attracted by a dark and mysterious young man in the audience. After the performance, John attacks Estelles father, Dr. George Henderson, but is stopped by the young man, who introduces himself as Alex Stone. Alex and Estelle go out for dinner and fall in love. 

As it turns out, Alex is part of a vampire coven. Questioned by his fellow vampires, he reveals that he his tired of the emptiness of being a savage beast living in eternal darkness. Seeking help from Estelles father, a famous medical research scientist, Alex confides in Estelle, explaining that he is a vampire but would prefer a mortal life with her. When approached, Dr. Henderson is incredulous at first but then is very interested in working with a vampire as this could further his research into immortality. 

However, Hendersons research is funded by Victor Price, head of the Illuminati in popular culture|Illuminati, a multi-national crime syndicate, who hope to attain immortality through that research. Meanwhile, they are harvesting people for spare body parts. Through an informer, Price learns of Alexs case and allows Hendersons research to continue if Alex would in turn make him a vampire. Though Dr. Henderson unlocks the secret behind Alexs vampirism, Alex refuses to provide Price with immortality. He informs the Hendersons of Prices association with the Iluminati but then agrees to Estelles enigmatic suggestion to "take advantage" of the Iluminatis power.

Meanwhile, Alexs fellow vampires are lacking Alexs power to make the group disappear, have to contend with Marshall Pope, a vampire hunter working for Interpol who has been brought after the police discovered the existence of vampire attacks. With his help, the police manage to kill the other vampires, who are lacking Alexs power, though Pope is also killed. The police have also arrested one of Prices henchmen, Rex, in a failed drug trafficking operation. In a bargain, Rex reveals Prices role in the operation, prompting the police to put Price under surveillance.

At the same time, Price and his associates receive the promised treatment and return to their mansion, taking Estelle with them. One of Prices henchman, Steven Mills, tries to stake Alex but is overpowered. Henderson and Alex rush to the mansion, where Price is showing all his power and wealth to Estelle in order to seduce her. Alex confronts Price and, as the police storm in, uses his powers of disappearance to vanish together with the Iluminati-turned-vampires. As the bystanders are looking on in bewilderment, the film shows Alex leaving the Iluminati to spend their eternal lifer in outer space, as he returns to earth.
 charitable cause and now lives with non-vampiric Alex in a large house.

==Cast==
*Katherine Hawkes as Estelle Henderson Daniel Goddard as Alex Stone
*Eric Etebari as Victor Price
*Martin Kove as Steven Miles
*Costas Mandylor as	Rex
*Matthias Hues as Marshall Pope
*Gary Daniels as Sebastian
*Phil Fondacaro as Michael Bates
*Katie Rich as Alice Hammond
*Vince Jolivette as Pete Blake
*Nick Jameson as Dr. George Henderson
*Tom Hedrick as Larry Masters
*David Castro as Ricardo Ortega
*Andrew Bowen as John Evans
*Miranda Kwok as Sylvia

==External links==
*  
*  
*  

 
 
 
 
 
 