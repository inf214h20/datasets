Impressions de la Haute Mongolie
{{multiple issues|
 
 
}}

 
Impressions de la haute Mongolie was a 1976 surrealist film directed by Salvador Dalí and José Montes-Baquer, starring Salvador Dalí himself.  It is a false documentary about a hunt through Mongolia for a giant hallucinogenic mushroom.  The film is dedicated to Raymond Roussel, author of the similarly titled poem Homage to Impressions d’Afrique.

 
 
 


 