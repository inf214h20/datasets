The Edifying and Joyous Story of Colinot
{{Infobox film
| name           =The Edifying and Joyous Story of Colinot 
| image          = 
| image_size     = 
| caption        = 
| director       = Nina Companéez
| producer       = Mag Bodard
| writer         = Nina Companéez
| narrator       =
| starring       = Francis Huster Nathalie Delon Brigitte Bardot Bernadette Lafont
| music          = Guy Bontempelli    
| cinematography = Ghislain Cloquet
| editing        = Raymonde Guyot    
| distributor    = Warner Bros.
| released       =  :  17 May 1974
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 913,333 admissions (France) 
| preceded_by    = 
| followed_by    = 
}}
The Edifying and Joyous Story of Colinot ( ) is a 1973 French comedy film directed and written by Nina Companéez. Francis Huster stars as the title character, Colinot. It is notable as the final film appearance of Brigitte Bardot who retired from the entertainment industry when the film went into post-production.

==Plot==
Colinots (Huster) world is turned upside down when his fiancee is kidnapped. This leads him to dangerous chase around 15th century France only to find that she has found love in the arms of a nobleman. But his fortunes take a turn when he meets Arabelle (Bardot) who teaches him many life lessons. 

==Cast==
*Francis Huster as Colinot
*Brigitte Bardot as Arabelle
*Nathalie Delon as Bertrade
*Ottavia Piccolo as Bergamotte
*Francis Blanche as vagrant
*Bernadette Lafont as Rosemonde
*Alice Sapritch as Dame Blanche
*Muriel Catala as Blandine
*Jean-Claude Drouot as Masnil Plassac
*Julien Guiomar as Rosemondes husband
*Jean Le Poulain as Brother Albaret Paul Müller as Brother Hugo Rufus as Gagnepain
*Henri Tisot as Tournebeuf
*Guy Grosso as Lucas
*Catherine Lachens
*Marie-Georges Pascal
*Évelyne Buyle 
*Maurice Barrier
*Mike Marshall 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 