Don Peyote
{{Infobox film
| name           = Don Peyote
| image          =
| alt            =
| caption        =
| film name      =
| director       = {{plainlist|
* Dan Fogler
* Michael Canzoniero
}}
| producer       = {{plainlist|
* Thomas Michael Sullivan
* Luke Daniels
* Carlos Velazquez
* Stuart Braunstein
}}
| writer         = {{plainlist|
* Dan Fogler
* Michael Canzoniero
}}
| starring       = {{plainlist|
* Dan Fogler
* Kelly Hutchinson
* Jay Baruchel
* Josh Duhamel
* Annabella Sciorra
* Wallace Shawn
}} 
| music          = Ben Lovett
| cinematography = John Inwood
| editing        = Dan Bush
| studio         = {{plainlist|
* Highland Film Group
* Studio 13
* Wingman Productions
* Redwire Pictures
* Casadelic Pictures
}}
| distributor    = XLrator
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Don Peyote is a 2014 American comedy film written and directed by Dan Fogler and Michael Canzoniero.  It stars Fogler as a slacker who has a spiritual awakening and becomes obsessed with conspiracy theories.

== Plot ==
Warren, an unemployed pot-smoker, is engaged to be married to Karen.  As Warren becomes more obsessed with conspiracy theories, he is driven to find the truth and meets a large number of people, each of which have their own views.  Warren begins to film a documentary based on his experiences and search for answers.

== Cast ==
* Dan Fogler as Warren Allman
* Josh Duhamel
* Jay Baruchel as Bates
* Wallace Shawn as Psychotherapist
* Kelly Hutchinson as Karen
* Yang Miller as Balance
* Anne Hathaway as Agent of TRUTH
* Topher Grace as Glavin Culpepper
* Annabella Sciorra as Giulietta
* Abel Ferrara as Taxi cab driver
* Timothy Levitch
* Daniel Pinchbeck as himself

== Production ==
Fogler recruited the large cast of cameos in part by allowing them to co-write their characters and improvise.  The film was shot between 2010 and 2013. 

== Release ==
XLrator gave Don Peyote a limited release on May 16, 2014,  and released it on DVD on July 8, 2014. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 8% of thirteen surveyed critics gave the film a positive rating; the average rating was 3.5/10.   Metacritic rated it 14/100 based on eight reviews.   Sheri Linden of The Hollywood Reporter wrote, "To call Don Peyote a mess would be putting too fine a point on it. The hallucinatory odyssey of a conspiracy-theory-obsessed New Yorker is a bad trip, destination nowhere."   Daniel M. Gold of The New York Times called it "a cautionary tale of drug-fueled decline" that may not have been realized by its creators.   Gary Goldstein of the Los Angeles Times called it "a tedious, incoherent look at a paranoid stoners emotional and spiritual unraveling".   Calum Marsh of The Village Voice wrote that the film  becomes increasingly incomprehensible as time goes on.   Christopher Schobert of Indiewire rated it D and wrote, "Perhaps in the hands of a Charlie Kaufman or Michel Gondry, this story could move beyond the unexceptional, but in Foglers hands, Don Peyote is a slow-moving dirge."   Vadim Rizov of The Dissolve rated it 0/5 stars and wrote, "In practice, Dan Foglers sophomore directorial effort (co-directed/written by Michael Canzoniero) is merely execrable, segueing incoherently from one stand-alone fragment of a terrible movie to another."   Matt Donato of We Got This Covered rated it 2.5/5 stars and wrote, "Don Peyote is a delusional, hallucinogenic journey into the mind of an apocalypse obsessed lunatic – a jumbled puzzle of ideas missing a few crucial connections." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 