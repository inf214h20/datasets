Rekava
 
{{Infobox film
 | name = Rekava
 | image = Rekava.jpg
 | imagesize=150px
 | caption = A scene in film
 | director = Lester James Peries
 | producer = Lester James Peries
 | writer = Lester James Peries
 | starring = Ananda Weerakoon Somapala Dharmapriya D. R. Nanayakkara Myrtle Fernando
 | music = Sunil Shantha (songs)   B. S. Perera (music direction and background score) Sisira Senaratne (songs Olu Nelum Neliya Rangala) William Blake 
 | editing = Titus de Silva
 | distributor = 
 | country    = Sri Lanka 1956 
| runtime = 89 mins  Sinhala
 | budget = 
 | followed_by =
}} 1956 film based on village life and their mythical beliefs in Sri Lanka (then Ceylon). It is the first Sinhalese film which was fully shot in Ceylon and was the first in the country to be shot outdoors. It was also the first film which was free from Indian influence. Many Sinhalese films produced in that era were remakes of South Indian films which were not properly adapted to the Sri Lankan context. Even their dialogs were not natural.

It was the first feature length film by Sri Lankan director Lester James Peries. The film was well-received internationally. It was shown at the 1957 Cannes Film Festival and was included in the main competition.     It is still the only Sri-Lankan film to be nominated for the Palme dOr. The cinematography was by Wiliam Blake. Despite much acclaim the film was not a commercial success in Sri Lanka due to its defiance of the mainstream concept of what a film is (i.e. one of a romance between a girl and a boy, fights, comedy, songs with Hindi tunes). Since that time it has come to become one of the best known Sinhala movies and is considered as marking the birth of a uniquely Sri Lankan cinema.

==Synopsis==
The film starts with a tilt walker cum musician Miguel (Sesha Palihakkara) arriving in the village of Siriyalas with a monkey it performs various antics for the public. Two thieves of the village try to rob him and Sena (Somapala Dharapriya), a young boy prevents the robbery. The tilt walker, who is also a palm reader, read the boys palm and predict the future, said that Sena will become a famous healer and bring dignity to the village.

One day, when Sena and his friend Anula (Myrtle Fernando) were playing with a Kyte, Anula suddenly loses her eyesight. The native doctor of village was unable to restore Anulas sight but Sena touches her eyes, miraculously Anula begins to see. After that Sena got a reputation of a boy with a magical touch.

Senas father who is a notorious money lender of the village, used his talent to earn money. His father organize a healing campaign among the villagers. One day a rich land owner brought his son for the treatment, but after that boy died. The villagers are outraged say that it is Senas fault. To make the situation worse the village undergoes a huge drought. Later on the peace and tranquility return to the village of Siriyala with the blessings of Sena.

=== Cast ===
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Somapala Dharmapriya || Sena
|-
| Myrtle Fernando || Anula
|-
| Sesha Palihakkara || Miguel
|-
| Romulus de Silva || Village Headman
|-
| Mallika Pilapitiya || Premawathie
|- Iranganie Meedeniya || Kathrina
|-
| D. R. Nanayakkara || Sooty
|-
| N. R. Dias || Podi Mahaththaya
|-
| Ananda Weerakoon || Nimal
|-
| Winston Serasinghe || Kumatheris
|-
| Nona Subaida || Rosalin
|}

==Music==
The Music in this film was composed by late legendary Sunil Santha a pioneer in Sinhala Music and the lyrics were composed by late Rev. Fr. Merciline Jayakody. The music director of the film was B.S.Perera. The songs of the film are even heard and praised today. Lata Walpola, Indrani Wijebandara, Sisira Senaratne and Ivor Dennis contributed to the vocals. Sisira Senaratne sang the main song Olu Nelum Neliya Rangala.

==References==
 

==External links==
* 
* 
* 
* 
*  
* 
* 
* 
* 

 
 
 