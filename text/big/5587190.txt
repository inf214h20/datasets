Pinocchio (1992 film)
{{multiple issues|
 
 
}}

{{Infobox film|
  name           = Pinocchio |
  image          = Pinocchio 1993.jpg |
  director       = |
  producer       = Diane Eskenazi |
  writer         = Carlo Collodi (original author), Roger Scott Olsen |
  based on       =   | 
  studio         = Golden Films |
  distributor    = Trimark Pictures |
  country        = Japan United States |
  released       =   |
  runtime        =49 minutes |
  language       = English |
  budget         = |
  music          = |
}} Italian author American Film Investment Corporation II (later Golden Films) and was originally distributed by Trimark Pictures. In subsequent years, the distribution rights were transferred to GoodTimes Entertainment, which it held until Gaiam acquired its assets in 2005, after GoodTimes filed for bankruptcy. The film is currently available on DVD under the "Collectible Classics" label.

==Plot==
 

==Characters==
* Pinocchio (voiced by Jeannie Elias) - He is a little wooden puppet that Geppetto made using an enchanted piece of wood. Pinocchio instantly comes to life and immediately begins getting into mischief, luckily, hell see the guidance of his new friend, Cricket and that of the kind Blue Fairy. Pinocchio longs to be a real boy and do everything a little boy should do, but he soon learns that in order for his wish to come true, he must prove that he has what it takes to be a decent little boy.

*   who feels despair after people start buying and less and less of his toys, for this, he decides to make a toy for himself, a small wooden puppet. When his puppet comes to life, he gives him the name of "Pinocchio" and instantly, he becomes the toymakers most treasured thing in the world. Gepetto is heartbroken when Pinocchio disappears leaving no trace and will not hesitate to sail around the entire world if thats what itll take to find his little boy.

* Talking Cricket (voiced by Cam Clarke) - A small cricket (insect)|cricket, he is the first to meet Pinocchio after he becomes alive. They both soon become friends and Cricket becomes Pinocchios most faithful advisor, never leaving his side. Although a bit of a coward, Cricket never even thinks of abandoning Pinocchio when danger lurks by, such as when he meets the Wolf and the Cat.
 The Blue Fairy (voiced by Jeannie Elias) - Kind, soft-spoken and sweet, the Blue Fairy becomes a motherly figure over Pinocchio, guiding and looking after him at all times, even when shes not physically around. She takes Pinocchio into her home after he is lost in the woods and will not forget him in his times of peril. When the Blue Fairy believes that Pinocchio has been swallowed by a whale, her heart breaks and her life endangers but the good Pinocchio will know how to repay all her help and kindness.
 The Wolf and the Cat (both voiced by Cam Clarke) - Two wicked, greedy individuals are the perfect main antagonists for poor Pinocchio. When they learn about his five gold coins, they follow him around until they can get their hands on all five of them including a plan to make him grow a money tree in the "Field of Wishes". Though Pinocchios innocence and ignorance about the evil of the world will make their scheme much easier.

* Whiskers - Geppettos pet cat
 Puppet Master (voiced by Jim Cummings) - A kind old man who gives Pinocchio five gold coins.

* Candlewick (Pinocchio character)|Candlewick (voiced by Cam Clarke) - A foolish boy who leads Pinocchio into Land of Toys|Dunceland.

* The Ringmaster (circus)|Ringmaster (voiced by Jim Cummings) - A wicked man who buys Pinocchio after he is turned into a donkey.

* The Coachman - A man who gives all "stupid" boys a ride to Dunceland.

* The Swallow - A kind swallow who helps Pinocchio on his journey and informs him of the fate of the Blue Fairy.

==Music==
*Theme: "All for Me and All for Free", written and composed by Richard Hurwitz and John Arrias.

===Classical pieces===
(incomplete)
*" " - Camille Saint-Saëns Clair de lune" - Claude Debussy Spring Song" - Felix Mendelssohn
*" " - Camille Saint-Saëns
*"Gavotte" - François-Joseph Gossec Die Fledermaus - Overture" - Johann Strauss II Minuet in Christian Pezold Sleeping Beauty waltz" - Peter Tchaikovsky Minuet from the String Quintet Op.11 No.5" - Luigi Boccherini
*"  - Franz Schubert
*" " - Robert Schumann Barber of Seville - Overture" - Gioacchino Rossini
*" " - Peter Tchaikovsky Ave María" - Johann Sebastian Bach and Charles Gounod Piano Concerto in A minor, Op. 16" - Edvard Grieg The Flying Dutchman - Overture" - Richard Wagner
*"Ride of the Valkyries" - Richard Wagner
*" " - Pyotr Ilyich Tchaikovsky
*"Sylvia (ballet)|Sylvia" - Léo Delibes Symphony No. 40 - "Molto Allegro" - Wolfgang Amadeus Mozart Julius Fučík
*" " - Camille Saint-Saëns

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 