Dhruvam
{{Infobox film
| name           = Dhruvam
| image          = Dhruvam.jpg
| image size     =
| caption        = Cover of Druvam Video CD
| director       = Joshiy
| producer       = M. Mani
| story and dialogues  =A.K.  ]
| screenplay          = A.K. Sajan & S. N. Swamy Vikram
| music          = S.P. VenkateshTiger Prabhakar
| cinematography = Dinesh Babu
| editing        = K. Sankunny	 	
| distributor    =
| released       =    
| runtime        = 145 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          = 
}}
 Vikram also played other povited roles and its music was composed by S. P. Venkatesh. Produced on a large budget, the film was among the highest grossing Malayalam films of 1993.  

==Plot==
A hangman is brought to central jail to hang Hyder Marakkar (Tiger Prabhakar), a notorious criminal with terror links, but he is killed in a road accident. DIG Marar (Janardhanan (actor)|Janardhanan) smells foul play and arrives at Kamakshipuram along with Jose Nariman (Suresh Gopi), a smart young police officer, to meet Narasimha Mannadiar (Mammootty), a revered village lord and member of the royal family who had once ruled the hamlet. He is worshipped and revered by his villagers. Mannadiar is known for his generous and fearless attitude, and his thirst for justice and peace for his village has made him an enemy in the eyes of politicians and a certain group of police officers. 

Upon Narimans request, Viramani (Babu Namboothiri), the Mannadiars secretary shares with him a few stories from Mannadiars life that made him popular. One among them was shared with Veerasimha Mannadiar (Jayaram), his younger brother. Veeran liked Maya, and informed Narasimha Mannadiar about it. A  few days before the marriage, a young man, Bhadran(Chiyaan Vikram|Vikram) arrived at the house of Mannadiar introducing himself as Bhadran, a man in love with Maya. He also added that her parents had agreed to the marriage without Mayas consent. Mannadiar called off Veerans marriage and got Maya and Bhadran married. Bhadran who was a gang member of Marakkar who did not want to kill the DIG. Hence Bhadran was a target of the gang. Veerasimhan saves Bhadran and is appointed as Mannadiars driver. Veerasimhan plans to go to the UK for higher studies, but on the way is killed by Hyder Marakkar. From that day onwards, Narasimha Mannadiar waits for the chance to avenge for his brothers death. Upon hearing the story, Nariman decides to help Mannadiar. Although convicted by court for execution, Marakkar tries every possible way to escape. Mannadiars battle of revenge against Hayder Marakkar forms the rest of the story.

==Cast==
*Mammootty as Narasimha Mannadiyar, the main protagonist
*Suresh Gopi  as Sub Inspector Jose Nariman, another protagonist
*Jayaram as Veerasimha Mannadiyar, Narasimha Mannadiyars younger brother Vikram as Bhadran
*Gauthami as Mythili, Narasimha Mannadiyar wife
*P. K. Abraham as Chidambaram, Mythilis father
*Tiger Prabhakar as Hyder Marakkar, the antagonist. A mafia don and convict awaiting execution
*Shammi Thilakan as Ali, Hyder Marakkars brother Janardhanan as Marar, Deputy Inspector General of the Police
*T. G. Ravi as Kasi, the new hangman Santhosh as Hassan
*Appa Haja as Marars son
*Kollam Thulasi as Chekutty MLA Vijayaraghavan as Ramdas Jail Superintendent 
*Babu Namboothiri ... Rudra ...  Maya
*M.S. Thripunithura ...

==Trivia==
 
* This was the last combination of Mammootty and Sureshgopi before 20-twenty ,except a cameo role in The King.
* It was Chiyaan Vikram|Vikrams first Malayalam film.

* Dinesh Baboo served as the cinematographer replacing Joshiy|Joshiys frequent collaborator Jayanan Vincent.

* For the first time, Gouthami was paired with Mammootty. Later the same year they joined again in Jackpot and Sukrutham in 1994.

*This was the second script by S. N. Swamy for Joshiy. They had worked together in Naduvazhikal in 1989.

*The background score by S.P. Venkatesh was one of the main highlights of the movie.

==Music==
The songs and background score were composed by S.P. Venkatesh. 
===Track listing===
{| class="wikitable"
|-
! Song
! Duration
! Artist
! Lyricist
|-	
|Karuka Vayal 
||04:04 Chitra (singer)|K. S. Chitra  Shibu Chakravarthy 
|-	
|Thalirvettilayndo
||04:02 Chitra (singer)|K. S. Chitra, G. Venugopal
||M. D. Rajendran 
|-	
|Thumbippenne Va Va 
||05:09
||Yesudas, Sujatha Mohan
||M. D. Rajendran 
|-	
|Varavarnnini 
||01:22 Chitra (singer)|K. S. Chitra Shibu Chakravarthy
|}

==External links==
*  
* http://popcorn.oneindia.in/title/5728/dhruvam.html
 
 
 
 
 