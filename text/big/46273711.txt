Damascus Roof and Tales of Paradise
Damascus Roof and Tales of Paradise (2010) is a Syrian documentary film directed by Soudade Kaadan and produced by Anas Abdel Wahab.      

== Synopsis ==
Syrian traditional story-telling, folklore, tales, fictional mythological characters play an important role in their culture. This tradition is passed generation to the next generation — from grandparents to grandchildren. However, in the modern era these stories are being lost. This is happening in Damascus, the capital and the second largest city of Syria, too.

== Production ==
The film is directed by Soudade Kaadan. This was his second documentary film. Anas Abdel Wahab produced the film.    

== References ==
 

== External links ==
*  



 
 
 
 


 
 