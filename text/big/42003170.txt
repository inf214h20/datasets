Thottupaar
{{Infobox film
| name           = Thottupaar
| image          = 
| caption        =
| director       = K. V. Nandhu
| producer       = Janaki Sivakumar
| story          = 
| screenplay     =  Ramana Lakshana
| music          = Srikanth Deva
| cinematography = Rathinavelu
| editing        =
| studio         = Tholl Paavai Films
| distributor    = S. Thanu
| released       =  
| country        = India
| budget         =
| runtime        = 
| language       = Tamil
}} Tamil action Ramana and Lakshana in leading roles, released on 15 October 2010.

==Cast==
*Vidharth as Maharaja Ramana
*Lakshana
*Cochin Haneefa
*Jagan
*Dinesh Lamba
*Azhagam Perumal
*Anu Haasan
*Manobala
*Mayilsamy
*Ragasya in a special appearance

==Production==
Actor Vidharth played the lead role for the first time in his career, though the failure of the film meant that Mynaa (2010) is often noted as his debut film.    The films director Nandhu, had previously apprenticed under director Perarasu (director)|Perarasu.   

==Release==
The film opened to poor reviews, with a critic from Sify.com labelling the film as a "big bore", adding that "the film lacks a cohesive narration and screenplay".    Another reviewer cited that the film was a "cant watch".   

==Soundtrack==
Music is composed by Srikanth Deva.    Soundtrack contains 5 songs including a remix version of "Aadi Maasam Kaathadikka" from Rajinikanth starrer Paayum Puli.
*Oolavediye - Silambarasan, Suchithra
*Aadimaasam - Udit Narayan, Anuradha Sriram
*Seemasirikki - Jassie Gift, Anitha, Mamiboys
*Usurula - Srikanth Deva, Sangeetha
*Thottu Thottu - Sadhana Sargam

==References==
 

==External links==
*  

 
 
 
 


 