The Taming of the Shrew (1967 film)
{{Infobox film
| name           = The Taming of the Shrew
| image          = The Taming of the Shrew (1967 film) poster.jpg
| caption        = Original film poster
| alt            = 
| director       = Franco Zeffirelli|
| producer       =  Elizabeth Taylor, Richard McWhorter
| based on       =  
| screenplay     = Paul Dehn Suso Cecchi dAmico Franco Zeffirelli
| starring       = Elizabeth Taylor Richard Burton  Natasha Pyne Michael Hordern
| music          = Nino Rota
| cinematography = Oswald Morris Peter Taylor
| distributor    = Columbia Pictures
| released       =  
| runtime        = 122 minutes
| language       = English
| budget         = $4 million
| gross          = $8,000,000   $12,000,000  
}}
 1967 film the play of the same name by William Shakespeare about a courtship between two strong-willed people. The film was directed by Franco Zeffirelli and stars Elizabeth Taylor and Richard Burton as Shakespeares Kate and Petruchio.

==Plot and cast== Michael York), Alan Webb).

==Production details==
The film, made in English but shot in Italy, cuts much of the original dialogue, including much of the subplot of Lucentio and Bianca, and all of the Christopher Sly framing device.
 controversial speech 1929 film); however, her taming is apparently undercut by her quick exit from the banquet, which forces Burton’s Petruchio to chase after her amid jeers from the other men. Similar to Harold Bloom’s take on the play, Elizabeth Taylors Katherina is demonstrating that women may control men by appearing to obey them. 

The film was marketed quite misogyny|misogynistically, including the use of the taglines "A motion picture for every man who ever gave the back of his hand to his beloved... and for every woman who deserved it. Which takes in a lot of people!" and "In the war between the sexes, there always comes a time for unconditional surrender." 

The film was originally intended to be a vehicle for Sophia Loren and Marcello Mastroianni. Taylor and Burton put over a million dollars into the production and, instead of a salary, took a percentage of profits. The film made 12 million dollars worldwide and was generally liked by the critics.

==Awards== Best Costume Best Art Direction (Lorenzo Mongiardino, John DeCuir, Elven Webb, Giuseppe Mariani, Dario Simoni and Luigi Gervasi).   

==Reception==
===Box office performance=== theatrical rentals 25th highest grossing picture of 1967. The film grossed $12 million worldwide.   

===Critical reception===
The film received positive reviews from modern critics. Review aggregator Rotten Tomatoes reports that 85% of professional critics gave the film a positive review, with a rating average of 7.5 out of 10 and the sites consensus stating: "It may not be reverent enough for purists, but This Taming of the Shrew is too funny -- and fun -- for the rest of us to resist." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 