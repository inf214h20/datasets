Mr. Vampire IV
 
 
{{Infobox film name = Mr. Vampire IV image = Mr. Vampire IV.jpg alt =  caption = Original Hong Kong film poster traditional = 僵屍叔叔 simplified = 僵尸叔叔 pinyin = Jiāngshī Shūshū jyutping = Goeng1 Si1 Suk1 Suk1}} director = Ricky Lau producer = Sammo Hung Jessica Chan screenplay = Lo Wing-keung story = Ricky Lau Lo Wing-Keung Shut Mei-yee Sam Chi-Leung starring = Anthony Chan Wu Ma Chin Kar-lok Loletta Lee music = Stephen Shing cinematography = Abdul M. Rumjahn Tom Lau Bill Wong editing = Peter Cheung Keung Chuen-tak studio = Bo Ho Films Co., Ltd. Paragon Films Ltd. distributor = Golden Harvest released =   runtime = 96 minutes country = Hong Kong language = Cantonese budget =  gross = HK$14,038,901
}} jiangshi cinematic boom in Hong Kong during the 1980s.  The Chinese title of the film literally translates to Uncle Vampire.

==Plot==
In the rural countryside, a bespectacled Taoist priest called Four-eyed Taoist is always at odds with his neighbour, a Buddhist monk called Master Yat-yau. Their respective students, Kar-lok and Ching-ching, are friendly towards each other. The Taoist and Buddhist play pranks on each other.

One day, a convoy of soldiers escorting a coffin passes by their houses. Inside the convoy is Four-eyeds junior, Taoist Crane, and Cranes four disciples, as well as a young prince and his bodyguards. Four-eyed and Yat-yau learn from Crane that there is a jiangshi (Chinese "hopping" vampire) in the coffin, and the convoy is on its way to the capital to let the emperor inspect the jiangshi.

During a thunderstorm that night, rainwater washes away the magical charms on the coffin and the vampire breaks out, becoming more powerful after being struck by a bolt of lightning. The vampire starts attacking everyone in the convoy and infecting them with the "vampire virus", causing them to transform into vampires as well after a short time. Taoist Crane remains behind to hold off the vampire while the prince and his attendant flee towards the houses.

At this critical moment, Four-eyed and Yat-yau both decide to put aside their differences and focus on destroying the vampires.

==Cast== Anthony Chan as Four-eyed Taoist (四目道長), a Taoist priest
*Wu Ma as Master Yat-yau (一休大師), a Buddhist monk
*Chin Kar-lok as Kar-lok (家樂), Four-eyeds student
*Loletta Lee as JīngJīng (菁菁), Yat-yaus student
*Yuen Wah as Attendant Wu (烏侍長), the princes feminine personal attendant Vixen Spirit, a demoness who tries to seduce Four-eyed
*Chung Fat as Taoist Crane (千鶴道長), Four-eyeds junior
*Hoh Kin-wai as the 71st Prince
*Cheung Wing-cheung as the Vampire
*Chun Kwai-bo as one of the princes guards
*Chu Tau as one of the princes guards
*Pang Yun-cheung as one of the princes guards
*Ka Lee as one of Taoist Cranes students
*Chow Gam-kong as one of Taoist Cranes students
*Kwan Yung as one of Taoist Cranes students
*Lung Ying as a soldier

==Home media==

===VHS===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| United States 
|| Unknown
|| Rainbow Audio and Video Incorporation 
|| NTSC
|| Cantonese 
|| English
|| 
|| 
|}

===Laserdisc=== 
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Catalog No  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Japan
|| N/A
|| Pioneer LDC 
|| 
|| CLV / NTSC
|| Cantonese
|| Japanese 
|| Audio Mono
|| 
|-
|- style="text-align:center;"
|| 1989
|| Hong Kong
|| N/A
|| Unknown
|| SEL0133H89
|| CLV / NTSC
|| Cantonese
|| None
|| Audio: Stereo, 2 Sides
|| 
|}

===VCD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Hong Kong
|| N/A
|| Deltamac (HK)
|| NTSC
|| Cantonese, Mandarin
|| English, Chinese
|| 2VCDs
|| 
|-
|-
|- style="text-align:center;"
|| 1 April 2000
|| Hong Kong
|| N/A
|| Joy Sales (HK)
|| NTSC
|| Cantonese, Mandarin
|| English, Traditional Chinese
|| 2VCDs
|| 
|}

===DVD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Japan
|| N/A
|| Universal Pictures Japan
|| NTSC
|| 2
|| Cantonese, Japanese 
|| Dolby Digital Mono
|| Japanese 
|| Digitally Re-mastered Box-set
|| 
|-
|- style="text-align:center;"
|| 19 December 2002 
|| Hong Kong
|| N/A
|| Deltamac (HK) 
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Dolby Digital
|| English, Chinese
|| 
|| 
|- 
|- style="text-align:center;"
|| 19 February 2004
|| France
|| N/A
|| HK Video
|| PAL
|| 2
|| Cantonese
|| Dolby Digital 
|| French
|| Box-set 
|| 
|-
|- style="text-align:center;"
|| 15 March 2008
|| Hong Kong
|| N/A
|| Joy Sales (HK)
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Dolby Digital 2.0
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|}

==References==
 

==External links==
*  
*  
*   at Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 