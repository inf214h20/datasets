The Adventures of Werner Holt (film)
{{Infobox film
| name           =Die Abenteuer des Werner Holt
| image          =Bundesarchiv Bild 183-D0204-0021-001, Berlin, Kino "Kosmos", Nacht.jpg
| caption        =The premier of The Adventures of Werner Holt.
| director       =Joachim Kunert
| producer       =Hans Mahlich, Martin Sonnabend
| writer         =Dieter Noll, Claus Küchenmeister, Joachim Kunert
| starring       =Klaus-Peter Thiele
| music          =Gerhard Wohlgemuth
| cinematography =Rolf Sohre
| editing        =Christa Stritt
| studio    = DEFA
| distributor    =PROGRESS-Film Verleih
| released       =  
| runtime        =164 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}} East German drama film directed by Joachim Kunert.

==Plot==
Werner Holt, a young   and having a sexual encounter with an SS officers wife, which left him disgusted. After that, he realized that his fathers claims about millions of people being murdered in the concentration camps were true. 

As the Soviets attack, Wolzow orders his ill-equipped soldiers to hold to the last man. Holt flees, only to hear that his friend was himself accused of treason by an SS blocking detachment. He arrives in time to see Wolzow hanged. Enraged, Holt grabs a machine-gun and mows down the executioners. He then deserts. 

==Cast==    
*Klaus-Peter Thiele - Werner Holt
*Manfred Karge - Gilbert Wolzow
*Arno Wyzniewski - Sepp Gomulka
*Günter Junghans - Christian Vetter
*Peter Reusse - Peter Wiese
*Dietlinde Greiff - Marie Krüger
*Angelica Domröse - Uta Barnim
*Maria Alexander - Gertie Ziesche
*Monika Woytowicz - Gundel Thieß
*Wolfgang Langhoff - Professor Holt
*Wolf Kaiser - General Wolzow
*Erika Pelikowsky - Mrs. Wolzow
*Martin Flörchinger - Attorney Gomulka
*Helga Göring -	Mrs. Gomulka
*Ingeborg Ottmann - Mrs. Wiese
*Norbert Christian - Knaack
*Kurt Steingraf - director Maaß
*Hans-Joachim Hanisch -	Sergeant Gottesknecht
*Adolf Peter Hoffmann - Captain Kutschera
*Herbert Körbs - General

==Production== The Adventures East Germanys National Prize at 1963.  

==Reception==
The film sold more than three million tickets in East Germany alone,  and was well received in the Soviet Union.  It was one of the relatively few DEFA pictures to be released in West Germany, where it enjoyed considerable success, as well.  

Director Joachim Kunert, writer Claus Küchenmeister and cinematographer Rolf Sohre all won the National Prize of East Germany, 2nd Class, on 6 October 1965.  The film was also selected as the best film of the year by the readers of the magazine Junge Welt, and its producers were honored with the Erich Weinert Medal. Abroad, The Adventures of Werner Holt received the Prize for the Best Anti-Fascist Film and the Prize of the Soviet Peace Committee at the 4th Moscow International Film Festival,    as well as a Honorary Diplom in the 1965 Edinburgh Film Festival. In addition, It was granted an Honorary Medal in the 1966 Carthage Film Festival.  

On 6 February 1965, the National Zeitung columnist Hartmut Albrect wrote that the picture contained "extraordinary, well-made scenes that convey deeper messages than those immediately noticed." Günter Sobe from the Berliner Zeitung dubbed the picture as "remarkably authentic" that has "a powerful effect." Critic Ulrich Gregor praised Kunerts decision to split the plot into two storylines in order to deal with the chronological inconsistency of Nolls book.  The German International Film Lexicon described the picture as "one that causes shock... and warns against misguided ideals."  

Sabine Hake cited The Adventures of Werner Holt as one of the most notable films that, using a modernist style, challenged the traditional East German anti-Fascist narrative by introducing a more personal perspective to the theme.  Anke Pinkert, too, viewed it as a picture that dealt with the issue of in a more realistic manner than previous works. Anke Pinkert. Film and memory in East Germany. Indiana University Press (2008). ISBN 0-253-21967-1. Page 146.  James Chapman wrote that the "flashbacks and the stream-of-concioucenss techniques" employed by the director enabled Kunert to present "a fully rounded protagonist".  Daniela Berghan included the film among DEFAs Anti-Fascist classics.  Authors Antonin and Miera Liehm classified it as one the "army epics", a genre that used the setting of the German military to convey strong criticism of the countrys militaristic tradition. 

In 1996, The Adventured of Werner Holt was selected by a commission of historians and critics as one of the hundred most important German films ever made. 

==References==
 

==External links==
*  
*  on filmportal.de.

 
 
 
 
 
 
 
 