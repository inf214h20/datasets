Fun Is Beautiful
{{Infobox film
 | name = Fun Is Beautiful (Un sacco bello)
 | image =Fun Is Beautiful.jpg
 | caption =
 | director = Carlo Verdone
 | writer = Leonardo Benvenuti  Piero De Bernardi  Carlo Verdone 
 | starring = Carlo Verdone, Mario Brega, Renato Scarpa 
 | music = Ennio Morricone
 | cinematography = Ennio Guarnieri
 | editing =  Eugenio Alabiso
 | language = Italian 
 | country = Italy
 | runtime = 97 min
 | producer = Romano Caldarelli and Sergio Leone
 | released = 1980
 }}
 David di Donatello Awards and the Nastro dArgento for best new actor.   

== Plot ==
Carlo Verdone plays three roles in three episodes joined together. The Roman hick Enzo must  by car with a friend to reach a party of friends outside of Italy. The flower child Ruggero returns to Rome from his father (Mario Brega) who believes that the guy has psychological problems, and so he calls home this home a priest, a teacher and a very problematic nephew. Mr. Mario then is going to change the mentality of the guy with the help of these wise people, in the hope that Ruggero and his girlfriend (who is also a flower child ) find themselves the right way to live in a modern society. Finally the shy and awkward Leo find the love in a Spanish girl, but she gives him a lot of problems. In fact she is not alone and intends to recover an affair with her ex.

== Cast ==
*Carlo Verdone: Enzo/ Ruggero/ Leo/ Don Alfio/ Anselmo/ professor 
*Mario Brega: Mario  
*Renato Scarpa: Sergio
*Veronica Miriel: Marisol
*Isabella De Bernardi: Fiorenza
*Sandro Ghiani: Cristiano

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 