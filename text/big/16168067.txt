Devathayai Kanden
{{Infobox film
| name = Devathayai Kanden
| image =
| caption =
| director = Boopathy Pandian
| writer = Boopathy Pandian Kunal Karunas Sathyan Mayilsamy Nassar Thalaivasal Vijay
| producer =
| music = Deva
| cinematography = M.V. Pannerselvam Prasad Murella
| editor =
| released =  
| runtime =
| country = India
| language = Tamil
}}
Devathayai Kanden (  and Sridevi Vijaykumar. The film borrows its name from a hit song from another Dhanush-starrer Kadhal Kondein. The movie went on to become a critically acclaimed commercial success.

==Plot==
In this movie, Dhanush plays a struggling youngster who makes a living out of selling milk tea, carrying the business on his bicycle. He falls in love with a college girl played by Sridevi Vijaykumar. Gradually, the girl also reciprocates the boys feelings and things go smoothly for a while.

The girls parents find her a groom who is well educated and earns handsomely. The girl is in a dilemma on whom to choose as her life partner. She thinks of her future if she chooses the poor guy where she visualizes herself giving birth to a child in a government hospital, her husband unable to raise required money for her medical expenses and overall a very difficult life. Contrast this with a life where she is pampered by everyone, her every need taken care of almost instantly if she chooses to marry the groom selected by her parents.

While the poor lover guy is on a religious trip to Sabarimala, the girl decides to marry the groom (Kunal (actor)|Kunal) so that she can continue to lead a comfortable life. The lover is devastated when he hears this and heartbroken when the girl goes to the extent of slapping him when he accuses her parents of changing her mind.

In a unique way, the poor guy files a case against the girl for not holding the promise made to him of sharing a life together for a good 50 years. Though strange, the case soon gathers momentum and has the public discussing it everywhere. As the case progresses, several sacrifices made by the guy for the sake of the girl come into light through revelations made by people known to him and by his close friends. The girl has a change of heart and on the final day of the hearing, decides to re unite with the guy.

In a final twist, when the girl offers a rose to him, the hero refuses to accept her saying that the case was filed not to win her back but to teach a lesson not only her but to every girl with an attitude identical to her.
He justifies further by saying that yesterday she hated him because of his status but today she loves him and tomorrow she may again find him unattractive. Feeling light at heart, he leaves the court complex much to the surprise of the visibly embarrassed rich girl.

==Cast==

  
*Dhanush as Babu
*Sridevi Vijaykumar as Uma Kunal as Bala
*Karunas as Kaduppu Subramani Sathyan
*Mayilsamy
*Nassar
*Thalaivasal Vijay Rajeev as Umas father Ponnambalam
*Sreekanth
*Mohan V. Raman
 
*Fathima Babu as Umas mother
*Paravai Muniyamma
*T. P. Gajendran
*Suryakanth
*Bayilvan Ranganathan
*Nellai Siva
*Sivanarayana Murthy
*Crane Manohar
*Benjamin
*Cool Suresh
*Theni Kunjaramma
*Mumtaj
*Ragasya
 

==Soundtrack==
{| class="wikitable"
! # !! Title !! Singer(s)
|-
| 1 || "Azhage Brammanidam" || Harish Raghavendra, Chinmayi
|-
| 2 || "Ore Oru Thopula" || Sabash, Srilekha Parthasarathy
|-
| 3 || "Maama Paiya" || Ranjith (singer)| Ranjith
|-
| 4 || "Thundai Kaanom" || Dhanush, Anuradha Sriram
|-
| 5 || "Velakku Onnu" || Yugendran, Grace Karunas
|-
|}

==Production==
Dhanush signed the film, then titled Ennai Mattum Kadhal Pannu, soon after the success of Thulluvadho Ilamai and the venture was initially set to star Sherin, who appeared alongside him in that film. Since then, the title and the heroine went through a couple pof changes, briefly being referred to as Kadhal Sughamanathu and then finally as Devathayai Kanden, after the popular song of Dhanush-starrer Kadhal Kondein. Sridevi Vijayakumar replaced Sherin as heroine in the film. 

==Box office==
*The film was a commercial success grossing $15 million at the box office.

==References==
 

 
 
 
 
 
 