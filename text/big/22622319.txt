Invited People
{{Infobox film
| name           = Invited People
| image          = Invited People.jpg
| caption        = Theatrical poster for Invited People (1981)
| film name      = {{Film name
 | hangul         =     -    
 | hanja          =  받은  들 -  받은 사람들
 | rr             = Chodaebadeun seongungdeul - Chodaebadeun saramdeul
 | mr             = Ch‘odaebadŭn sŏngungdŭl - Ch‘odaebadŭn saramdŭl}}
| director       = Choi Ha-won 
| producer       = Choe Chun-ji
| writer         =
| starring       =
| music          = Choi Chang-kwon
| cinematography = Yang Yeong-gil
| editing        = Park Deok-yeol
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 135 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
}}
Invited People (hangul|초대받은 성웅들 - 초대받은 사람들 - Chodaebadeun seongungdeul or Chodaebadeun saramdeul) is a 1981 South Korean film directed by Choi Ha-won. It was chosen as Best Film at the Grand Bell Awards.     

== Plot ==
  
A religious drama about people studying and practising Catholicism in Korea during the mid 19th century despite oppression and persecution. 

== Cast ==
  
* 
* Yu In-chon 
* Kim Seong-su
* Yoon Yang-ha
* Kim Min-kyoung
* Kwak Eun-kyung
* Oh Young-hwa
* Kim Ae-kyung
* Moon Mi-bong
* Yoon Il-ju
* Lee In-ock

==Bibliography==
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 
 

 
 
 
 
 


 