Aik Aur Ghazi
{{Infobox film
| name           = Aik Aur Ghazi
| image          = 
| caption        = 
| director       = Syed Noor
| producer       = Danish Ghaffar
| writer         = Syed Noor
| starring       = Saima Safqat Cheema Heera Malik Haya Ali
| music          = Tafoo
| cinematography = Ali Jan
| editing        =
| released       =  
| runtime        =
| country        = Pakistan
| language       = Punjabi
| budget         =
| gross          =
}}
Aik Aur Ghazi  is a Punjabi language|Punjabi-language film directed by Syed Noor. It is based on a man named Yusuf Kazzab from Lahore who claimed to be a prophet. He was sentenced to death by a lower court of Pakistan and was murdered by a man named Tariq in jail. 

==Story==
Tariq (Heera Malik) lives a double life: gambler in reality, practicing Muslim in front of his father. His father, Haji Saab runs a small business and is widely respected in the neighborhood, is ignorant of Tariqs waywardness. Tariq is also seduced by Mohni (Saima) living next door. Tariq, over a gambling dispute, murders a rival. In revenge, rivals kill Tariqs younger brother. Therefore Tariq kills two more rivals to avenge his brothers murder. Tariqs father also dies of a heart-attack. He himself lands in jail. However, the killers are transformed when Yusuf Kazab (played by Shafqat Cheema) arrives. The jail inmates hurl abuses at him while Zulfi hatches a plan to murder Yusuf Kazab. Zulfi, through his uncle to smuggle in a pistol. However, Zulfi is transferred to another jail and he gives the pistol on to Tariq. Both want to decapitate the Kazab. 

==Background==
Muhemmed Yusuf Ali, dubbed Yusuf Kazzab (کذاب Kazzab means Great Liar) by a Pakistani lower court judge in an in camera trial was a Pakistani citizen who was murdered by a man named Tariq in 2002 while at the Kot Lakpat jail in Lahore, Pakistan. Tariq was on death row for committing murder. 

Muhemmed Yusuf Ali was charged of claiming prophethood. A case was registered against him by the Anjuman-e Tahaffuz-e Khatim-e Nabuwwat on 29 March 1997, a politically active Pakistani religious organization. He was accused of adultery, fraud, claiming prophethood, and blasphemy against Islam. He clarified his position through paid advertisements in the newspapers.  Based on the case, he was immediately detained.

A trial finally started in February 2000. Most of the hearings commenced at about 2:30pm and continued till as late as 7-8pm. The final arguments were heard on 28 July till 11:45pm, and the counsel for the defense were asked to conclude.

According to the trial records the complainant, office bearer of a local religious organization, alleged that Mr Yusuf Ali indirectly committed blasphemy by showing his resemblance with the Islamic prophet, Muhammad, on an unknown date, during a Friday sermon, more than 2 years before registration of the case.  

An aside: Kazab, or liar, is a term with religious connotations. It was daily Khabrain that not merely highlighted the case but also added the affix Kazab to the late Yusufs name, every time the case was reported in this rag. Bait (allegiance) at Mr. Yusufs hands later cost Zaid Hamid his jingoistic career as youth leader-cum-anchorperson and would-be-conqueror-of-India. 

==Reception==
The film released 10 June 2011. The film generated 100,000 PK rupees and lost money for the producer.

==References==
 

 

 
 
 
 