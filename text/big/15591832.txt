Jet Storm
 
{{Infobox film| name = Jet Storm
  | image = "Jet_Storm"_(1959).jpg
  | image size =
  | director = Cy Endfield
  | producer = Steven Pallos
  | writer = Cy Endfield Sigmund Miller
  | starring =Richard Attenborough Stanley Baker Hermione Baddeley Bernard Braden
  | music = Thomas Rajna
  | cinematography = Jack Hildyard
  | editing = Oswald Hafenrichter
  | studio = Pendennis Pictures
  | distributor = United Producers Releasing Organization
  | released =  
  | runtime = 88 min.
  | country = United Kingdom
  | language = English
  }}
Jet Storm (also known as Jetstream and Killing Urge) is a 1959 British thriller film directed and co-written by Cy Endfield. Richard Attenborough stars with Stanley Baker, Hermione Baddeley and Diane Cilento. The film has many of the characteristics of the later aviation disaster film genre such as Airport (1970 film)|Airport (1970).  

==Plot== George Rose), the man he believes is responsible for the accident and boards the same airliner on a transatlantic flight, flying from London to New York. 

Tilley threatens to blow himself up and everyone on board as an act of vengeance. When Captain Bardow (Stanley Baker) and the passengers realize that he is serious, and they cannot find the bomb (which Tilley had attached to the underside of the airliners left wing), they begin to panic. Some want to pressure him into revealing the location of the bomb, while others such as Doctor Bergstein (David Kossoff) try to reason with the now silent Tilley. Mulliner (Patrick Allen), a terrified passenger, attempts to kill Brock to get Tilley to not set off the bomb. 

Acting out of fear, Brock is killed when he smashes a window and is sucked out of the airliner. Tilley, coming to his senses when a young girl passenger soothes him, disconnects the remote control for the bomb, then commits suicide by poison. As the airliner approaches New York, the passengers realize that they will survive.

==Cast==
 
* Richard Attenborough as Ernest Tilley
* Stanley Baker as Capt. Bardow
* Hermione Baddeley as Mrs. Satterly
* Bernard Braden as Otis Randolf
* Diane Cilento as Agelica Como
* David Kossoff as Dr. Bergstein
* Virginia Maskell as Pam Leyton
* Harry Secombe as Binky Meadows
* Elizabeth Sellars as Inez Barrington
* Sybil Thorndike as Emma Morgan
* Mai Zetterling as Carol Tilley
* Marty Wilde as Billy Forrester
* Patrick Allen as Mulliner Paul Carpenter as George Towers
* Megs Jenkins as Rose Brock
* Jocelyn Lane as Clara Forrester
* Cec Linder as Col Coe Neil McCallum as Gil Gilbert
* Lana Morris as Jane Tracer George Rose as James Brock
* Peter Bayliss as Bentley
* Captain John Crewdson as Whitman
* Paul Eddington as Victor Tracer
* Glyn Houston as Michaels
* Peter Illing as Gelderen
* Jeremy Judge as Jeremy Tracer
* George Murcell as Saunders 
* Alun Owen as Green
* Irene Prador as Sophia Gelderen
 

==Production==
The type of aircraft depicted is a Soviet-built Tupolev Tu-104. Although the airline and its crew are clearly British, flying out of London and a BEA Vickers Viscount is also seen, the aircraft shown at the beginning is sporting the Soviet Unions flag on the tail. This twin-jet airliner was never flown by any airline outside the Soviet bloc. A medium-range airliner, the Tu-104 also could not have been used on transatlantic routes.  

==Reception==
In the Time Out review, John Pym saw Jet Storm as, "A British prototype for the Airport disaster movies of the 60s and 70s." He went on to note, "... like its later supersonic counterparts, Endfields film is naive and contrived, but not without interest as the alarmed passengers soon divide into groups: reactionary (advocating torture) and liberal (patience and persuasion)."  

The TV Guide critic wrote, "... thanks to an outstanding cast, this air-disaster film manages to limp to a landing with its thriller status intact."  The Radio Times applauded "... a star turn for Attenborough, who brings a convincing complexity to the role of bomber and bereft father." 

==References==
Notes
 

Citations
 
Bibliography
 
* Pym, John, ed. "Jet Storm." Time Out Film Guide. London: Time Out Guides Limited, 2004. ISBN 978-0-14101-354-1.
 

==External links==
*  
 

 
 
 
 
 
 