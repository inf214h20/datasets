A Girl of the Bush
 
{{Infobox film
| name           = A Girl of the Bush
| image          = File:Still_from_1921_Australian_silent_film_A_Girl_of_the_Bush.jpg
| image_size     =
| caption        = Still from film
| director       = Franklyn Barrett
| producer       = Franklyn Barrett
| writer         = Franklyn Barrett 
| narrator       =
| starring       = Stella Southern
| music          =
| cinematography = 
| editing        = 
| studio = Barretts Australian Productions
| distributor    = 
| released       = 26 March 1921
| runtime        = 6,000 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

A Girl of the Bush is a 1921 Australian silent film directed by Franklyn Barrett. It is one of the few films from Barrett to survive in its entirety today.

==Plot==
Lorna Denver manages Kangaroo Flat sheep station and is pursued by two men, evil Oswald and handsome young surveyor, Tom Wilson.

Lorna gives shelter to a baby that has survived an attack by aboriginals, but Tom thinks the baby is hers. This upsets Lorna who breaks it off with him.

Oswald is murdered and Tom is arrested. A Chinese cook reveals that the real killer was the father of a woman who had been seduced by Oswald.

==Cast==
*Vera James as Lorna Denver
*Jack Martin as Tom Wilson
*Herbert Linden as Oswald Keane
*Stella Southern as Grace Girton
*James Martin as John Burns
*Sam Warr as Sing Lee
*Emma Shea as Looe Toe
*D. L. Dalziel as James Keane
*Gerald Harcourt as Reg Howard
*Olga Broughton as Mary Burns
*Mick Huntsdale as Bill Tresle
*Tom Cosgrove as Bills mate

==Production== The Squatters On Our Selection. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 105. 

This was the first of three films Barrett made for his own company.  Shooting began in October 1920 at the Fremantle Station near Bathurst. 

==Reception==
The film was widely distributed and appears to have been a success at the box office. 

Vera James father bought the rights to distribute the film in New Zealand. 

==References==
 

==External links==
* 
*  at the National Film and Sound Archive
* 
 

 
 
 
 
 
 


 