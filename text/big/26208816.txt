Rahul's Arranged Marriage
{{Infobox Film
| name           = Rahuls Arranged Marriage
| image          = rahulsarrangedmarriage.jpg
| image_size     = 
| caption        = Official Movie Poster Anant Mathur Anant Mathur  Anant Mathur
| narrator       = 
| starring       = Devika Mathur Sumeet Mathur Amar Kumar Anurag Mathur Anita Mathur Jyothi Rana Abhishek Mathur
| music          =  Anant Mathur Anant Mathur
| distributor    = Lucky Break Entertainment 2005
| runtime        = 10 Minutes
| country        = Canada
| language       = Silent film
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2005 silent Canadian film directed and produced by Anant Mathur, and is a short film. 

==Plot==
The film revolves around a young man named Rahul whose father wants him to marry the daughter (Ishaa) of his old friend Mr. Kapoor. Rahul is quite modern and the concept of arranged marriage doesnt appeal to him. His father convinces Rahul to just meet Ishaa and if hes not interested hell call it off. But when Rahul meets Ishaa, sparks fly. 

==Cast==
* Sumeet Mathur - Rahul Shrivastava
* Devika Mathur - Ishaa
* Amar Kumar - Rahuls Grandfather
* Anurag Mathur - Mr. Shrivastava
* Anita Mathur - Mrs. Shrivastava
* Jyothi Rana - Mrs. Kapoor
* Abhishek Mathur - Mr. Kapoor
* Vineet Mathur - Ishaas Brother
* Arvind Mathur - Waiter

==Crew== Anant Mathur - Producer, Director, Writer
* Amar Kumar - Assistant Director
* Archana Johnson - Hair Stylist, Make-Up Artist Anant Mathur - Cinematographer
* Amar Kumar - Script Supervisor Anant Mathur - Film Editor Anant Mathur - Sound Editor
* Sumeet Mathur - Key Grip
* Vineet Mathur - Second Assistant Director
* Dayal Mathur - Property Master

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 
 