Thiranottam
{{Infobox film
| name           = Thiranottam
| image          = 
| image_size     = 
| caption        = 
| director       = Ashok Kumar
| producer       = Sasheendran
| writer         = 
| narrator       =  Chandra Mohan Maniyanpilla Raju Renu Chandra Mohanlal 
| music          = M. G. Radhakrishnan
| cinematography = S.Kumar ISC
| editing        = 
| studio         = Mrithyunjaya Films 
| released       = 2005  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Indian feature directed by Chandra Mohan, Ravikumar (actor)|Ravikumar, Maniyanpilla Raju, Mohanlal and Renu Chandra in their debut roles.   Mohanlal played Kuttappan, a mentally disabled servant in the movie. The film was completed and censored, but was not released immediately.   

==Plot==

Thiranottam is the tale of Kuttappan, a mentally disabled servant and the life around him.

==Trivia==

Mohanlals first shoot took place on 3 September 1978 at 11:30 AM on Kesavadev Road, in front of Mohanlals house in Mudavanmugal, Trivandrum.

The film was made by Mohanlal and his friends, Ashok Kumar (director), Late Sasheendran (Producer), Priyadarshan (Asst.director), Suresh Kumar (clap boy) and the cast were Ravi Kumar, Mohanlal, Renu Chandra, Chandra Mohan and others.

The film was completed and censored, but couldnt release then, eventhough a premier show was arranged. Years later it was released in a single theatre at Kollam.

Recently Mohanlal appealed through Facebook to trace the child actor who done the first shot along with him.  He mentioned,  There is a boy who shared screen space with me in it. However, I am unable to remember who he was. If any of you know his identity, do come forward and inform me. I really wish to meet him once again 

==Cast==
 Chandra Mohan
*Ravikumar
*Maniyanpilla Raju
*Mohanlal
*Renu Chandra

==References==
 

 
 
 


 