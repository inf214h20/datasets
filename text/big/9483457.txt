Three Girls Lost
{{Infobox film
| name           = Three Girls Lost
| image          = Three Girls Lost.jpg
| caption        = Theatrical release poster
| director       = Sidney Lanfield
| producer       = Sidney Lanfield
| screenplay     = Bradley King
| story          = Robert Hardy Andrews
| starring       = {{Plainlist|
* Loretta Young
* Lew Cody
* John Wayne
}}
| music          = R. H. Bassett
| cinematography = L. William OConnell
| editing        = Ralph Dietrich
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         =
}}
Three Girls Lost is a 1931 American film directed by Sidney Lanfield and starring Loretta Young, Lew Cody, and John Wayne. Based on a story by Robert Hardy Andrews, the film is about an architect who finds his neighbor locked out and flirts with her. When he is accused of murdering a rackateer, he becomes romantically involved with his neighbors roommate.

==Plot==
Architect Gordon Wales (Wayne) finds fellow apartment building resident Marcia Tallant (Marsh) locked out and flirts with her.

==Cast==
*Loretta Young as Norene McMann 
*Lew Cody as William (Jack) Marriott 
*John Wayne as Gordon Wales 
*Joan Marsh as Marcia Tallant 
*Joyce Compton as Edna Best
*George Beranger as Andre
*Ward Bond as Airline Steward
*Paul Fix as Tony Halcomb
*Willie Fung as Chinese Headwaiter
*Tenen Holtz as Photographer
*Hank Mann as Taxicab Driver
*Robert Emmett OConnor as Detective

==See also==
* John Wayne filmography

==External links==
* 

 

 
 
 
 
 
 
 


 