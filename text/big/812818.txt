In a Lonely Place
 
{{Infobox film
| name           = In a Lonely Place
| image          = In a lonely place 1950 poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Nicholas Ray
| producer       = Robert Lord
| based on       =  
| screenplay     = Edmund H. North Andrew Solt
| starring       = Humphrey Bogart Gloria Grahame
| music          = George Antheil
| cinematography = Burnett Guffey
| editing        = Viola Lawrence
| distributor    = Columbia Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
 In a Lonely Place by Dorothy B. Hughes. 
 Sunset Boulevard and Joseph Mankiewiczs All About Eve.

Although lesser known than his other work, Bogarts performance in this film is considered by many critics to be among his finest and the films reputation itself has grown over time along with Rays.   

The film is now considered a classic film noir, as evidenced by its inclusion on the Time magazine "All-Time 100 List"  as well as Slant Magazines 100 Essential Films.   In 2007, In a Lonely Place was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."

==Plot==
 
 Art Smith), Martha Stewart), is engrossed in reading it and asks if she can finish, since she only has a few pages left. Dix has a second violent outburst when a young director bad-mouths Dixs friend Charlie (Robert Warwick), a washed-up actor.

Dix claims to be too tired to read the novel, so he asks Mildred to go home with him, ostensibly to explain the plot. As they enter the courtyard of his apartment, they pass a new tenant, Laurel Gray (Gloria Grahame). As soon as Mildred is convinced that Dixs motives are honorable, she describes the story and confirms what he had suspected—the book is trash. He gives her cab fare to get home.

The next morning, he is awakened by an old army buddy, now a police detective, Brub Nicholai (Frank Lovejoy), who takes him downtown to be questioned by Captain Lochner (Carl Benton Reid). Mildred was murdered during the night and Dix is a suspect. Laurel is brought to the police station and confirms seeing the girl leave Dixs apartment alone, but Lochner is still deeply suspicious. Although Dix shows no overt sympathy for the dead victim, on the way home from the police station, he anonymously sends her two dozen white roses.

 
When he gets home, Dix checks up on Laurel. He finds she is an aspiring actress with only a few low-budget films to her credit. They begin to fall in love; this invigorates Dix into going back to work with a vengeance, with Laurel assisting him, much to his agents delight.

However, Dix behaves strangely. He says things that make his agent and Brubs wife Sylvia (Jeff Donnell) wonder if he did kill the girl. In addition, Lochner sows seeds of doubt in Laurels mind, pointing out Dixs lengthy record of violent behavior. Dix becomes furiously irrational when he learns of it. He drives at high speed, with Laurel a terrified passenger, until they sideswipe another car. Nobody is hurt; but, when the other driver accosts him, Dix beats him unconscious and is about to strike him with a large rock when Laurel stops him.

Laurel gets to the point where she cannot sleep without taking pills. Her distrust and fear of Dix are becoming too much for her. When he asks her to marry him, she accepts but only because she is too scared of what he might do if she refused. She makes a plane reservation and tells Mel she is leaving because she cannot take it anymore. Dix finds out and almost strangles her during a violent confrontation before he regains control.

Just then, the phone rings. It is Brub with good news: Mildreds boyfriend (named Henry Kesler, the same as the films associate producer) has confessed to her murder. Tragically, it is too late to salvage Dix and Laurels relationship.

==Cast==
* Humphrey Bogart as Dixon Steele
* Gloria Grahame as Laurel Gray
* Frank Lovejoy as Det. Sgt. Brub Nicolai
* Carl Benton Reid as Capt. Lochner Art Smith as Mel Lippman Martha Stewart as Mildred Atkinson
* Jeff Donnell as Sylvia Nicolai
* Robert Warwick as Charlie Waterman
* Morris Ankrum as Lloyd Barnes
* William Ching as Ted Barton
* Steven Geray as Paul, Headwaiter
* Hadda Brooks as Singer

==Background==
 

Louise Brooks wrote in her essay "Humphrey and Bogey" that she felt it was the role of Dixon Steele in this movie that came closest to the real Bogart she knew. "Before inertia set in, he played one fascinatingly complex character, craftily directed by Nicholas Ray, in a film whose title perfectly defined Humphreys own isolation among people. In a Lonely Place gave him a role that he could play with complexity because the film characters, the screenwriters, pride in his art, his selfishness, his drunkenness, his lack of energy stabbed with lightning strokes of violence, were shared equally by the real Bogart." 
 Art Smith and Gloria. And we improvised the ending as it is now. In the original ending we had ribbons so it was all tied up into a very neat package, with Frank Lovejoy coming in and arresting him as he was writing the last lines, having killed Gloria. Huh! And I thought, shit, I cant do it, I just cant do it! Romances dont have to end that way. Marriages dont have to end that way, they dont have to end in violence. Let the audience make up its own mind whats going to happen to Bogie when he goes outside the apartment." 

Lauren Bacall and Ginger Rogers were considered for the role of Laurel Gray. Bacall was a natural choice given her off-screen marriage to Bogart and their box-office appeal, but Warner Bros. refused to loan her out, a move often thought to be in reaction to Bogart having set up his own independent production company, the type of which Warner Bros. were afraid would jeopardize the future of the major studios. Rogers was the producers first choice but director Nicholas Ray believed that his wife Gloria Grahame was right for the part. Even though their marriage was troubled, he insisted that she be cast. Her performance today is unanimously considered to be among her finest.

Grahame and Rays marriage was starting to come apart during filming. Grahame was forced to sign a contract stipulating that "my husband   shall be entitled to direct, control, advise, instruct and even command my actions during the hours from 9 AM to 6 PM, every day except Sunday...I acknowledge that in every conceivable situation his will and judgment shall be considered superior to mine and shall prevail." Grahame was also forbidden to "nag, cajole, tease or in any other feminine fashion seek to distract or influence him." The two did separate during filming. Afraid that one of them would be replaced, Ray took to sleeping in a dressing room, lying and saying that he needed to work on the script. Grahame played along with the charade and nobody knew that they had separated. Though there was a brief reconciliation, the couple divorced in 1952, when Ray found Grahame in bed with his thirteen-year-old son.

The film was one of a number of Nicholas Ray films to be scored by avant garde classical composer George Antheil (1900–1959).

==Reception==

===Critical response===
At the time of its original release, the reviews were generally positive (in particular many critics praised Bogart and Grahames performances), but many questioned the marketability given the bleak ending.

The staff at Variety (magazine)|Variety magazine in May 1950 gave the film a good review and wrote, "In a Lonely Place Humphrey Bogart has a sympathetic role though cast as one always ready to mix it with his dukes. He favors the underdog; in one instance he virtually has a veteran, brandy-soaking character actor (out of work) on his very limited payroll...Director Nicholas Ray maintains nice suspense. Bogart is excellent. Gloria Grahame, as his romance, also rates kudos." 

Bosley Crowther lauded the film, especially Bogarts performance and the screenplay, writing, "Everybody should be happy this morning. Humphrey Bogart is in top form in his latest independently made production, In a Lonely Place, and the picture itself is a superior cut of melodrama. Playing a violent, quick-tempered Hollywood movie writer suspected of murder, Mr. Bogart looms large on the screen of the Paramount Theatre and he moves flawlessly through a script which is almost as flinty as the actor himself. Andrew Solt, who fashioned the screen play from a story by Dorothy B. Hughes and an adaptation by Edmund H. North, has had the good sense to resolve the story logically. Thus Dixon Steele remains as much of an enigma, an explosive, contradictory force at loose ends when the film ends as when it starts." 

The film was considered a box-office disappointment. Not unlike Nicholas Rays debut They Live by Night (1948), it was advertised as a straight thriller while the film is not as simply fit into one genre as the marketing shows. Rays films had a brief revival in the 1970s and Bogarts anti-hero stance gained a following in the 1960s, and the French Cahiers du cinéma critics during the 1950s praised Rays unique film making. Time (magazine)|Time magazine, which gave the film a negative review upon its initial release, called it one of the 100 best films of all time in their 2005 list.
 The Stranger, existentialist primers...Laurel and Dixon may love each other but its evident that theyre both entirely too victimized by their own selves to sustain this kind of happiness. In the end, their love resembles a rehearsal for the next and hopefully less complicated romance. This is the existential endgame of one of Rays smartest and most devastating masterpieces." 

Curtis Hanson is featured on the retrospective documentary of the DVD release, and has stated his admiration for the film, notably Rays direction, the dark depiction of Hollywood and Bogarts performance. This was one of the films which he showed to actors Russell Crowe and Guy Pearce in preparation for filming L.A. Confidential (film)|L.A. Confidential.  He said, "I wanted them to see the reality of that period and to see that emotion. This movie, and Im not saying its the greatest movie ever made, but it represents many things that I think are worth aspiring to, such as having character and emotion be the driving force, rather than the plot....When I first saw In a Lonely Place as a teenager, it frightened me and yet attracted me with an almost hypnotic power. Later, I came to understand why. Occasionally, very rarely, a movie feels so heartfelt, so emotional, so revealing that it seems as though both the actor and the director are standing naked before the audience. When that kind of marriage happens between actor and director, its breathtaking." 

In 2009, film critic Roger Ebert added In a Lonely Place to his "great movies" list. 

The review aggregator Rotten Tomatoes reported that 97% of critics gave the film positive reviews, based on thirty reviews. 

==Comparisons to novel==
In a Lonely Place was based on Dorothy B. Hughes 1947 novel of the same title. Some controversy exists between admirers of the film and admirers of the novel (who view the film as a watered down adaptation), as Edmund H. Norths script takes some elements of the novel, but is ultimately an entirely different story.

The strongest difference between the two works lies in the primary character: the films Dixon Steele is a screenwriter with an offbeat lifestyle, and a decent person with fatally poor impulse control and prone to wild overreaction when enraged. The novels Steele is a limited third-person view from Dixs perspective, reminiscent of the first-person in noir, à la The Killer Inside Me.  Steele is a charlatan who pretends to be a novelist while sponging money from his overbearing uncle. When this well dries up, he murders a wealthy young man and assumes his identity, in a manner similar to Patricia Highsmiths Tom Ripley. (It should be noted that Hughes novel predates Highsmiths and may have influenced it.) The film follows the question of whether Dix finally went too far in his anger and committed the murder under investigation to a tragic end: even though hes proven innocent, his rage at the cloud of suspicion has driven the woman he loves away for good. No question of Dixs innocence exists in the novel, which follows the investigation of a murder Dix plainly committed and his self-insertion into that investigation for his own ends.

Curtis Hanson, in the DVD featurette In A Lonely Place Revisited, further analyses the parallels and differences between the novel and the film. He notes that there is a parallel in the film between Dixs adaptation of a novel for film with the adaptation of In a Lonely Place for film, he also notes a key difference between Dix in the film and Dix in the novel is their respective treatment of women. In the novel Dix pursues women, the first chapter details his pursuit of a woman. In the film, Dix is pursued by women.

Hughes novel, out of print for decades, was re-released by The Feminist Press at CUNY in 2003, which edition is still in print as of April 2007. Penguin Books also published a paperback edition in the UK in 2010 as part of their Modern Classics imprint. The novel has been hailed lately as a stellar example of mid-twentieth hardboiled/noir fiction, both as a rare example of womens writing in that genre and for its quality and contributions to that genre. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   article at Sunset Gun by Kim Morgan
*   article at Bright Lights Film Journal by Scott Thill
*   article at Senses of Cinema by Serena Bramble
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 