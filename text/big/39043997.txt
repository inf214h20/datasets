John the Violent
 
{{Infobox film
| name           = John the Violent
| image          = 
| caption        = Theatrical poster
| director       = Tonia Marketaki 
| producer       = Tonia Marketaki
| writer         = Tonia Marketaki
| starring       = Manolis Logiadis, Mika Flora, Vangelis Kazan
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = September 1973 
| runtime        = 180 minutes
| country        = Greece
| language       = Greek
| budget         = 
}} Thessaloniki film festival upon its release in 1973 where it was awarded three of the top prizes.       The film is based on an actual murder which happened in Athens in the 1960s and created a media sensation at the time.      

== Plot ==
Ioannis Zachos is a psychopath who fantasises about killing beautiful women as a means to validate his manhood and to feel a sense of empowerment. He stalks the empty streets of Athens at night looking for victims. In one of those outings he enters a small sidestreet and at around midnight he sees a young woman, Eleni Chalkia, whom he attacks stabbing her to death. After the murder he disappears into the darkness. 

A murder investigation eventually leads to his arrest. During  interrogation he readily confesses his crime to the police to their great relief, since their investigation had come under fire for its perceived faults. In the ensuing trial, the psychopath freely admits his guilt but during his testimony he falls into contradictions. It becomes apparent that the murderer bases his testimony on reports he reads from the newspapers which cover his criminal trial.  

Zachos may be a deviant psychopath but he possesses eloquence, grace, charisma and above-average intelligence which make him attractive to the trial audience, the judges, the press and the psychiatrists. He soon becomes a "social icon" through the everyday reports of the press. 

He is articulate in describing his violent tendencies in the context of an indifferent, cold society and explains that he uses violence as a means to achieve catharsis.  He freely admits that the murder was senseless and served no purpose but he explains that he feels pressured and trapped within society and that he committed the murder to obtain a sense of relief. 
 hedonist and he feels pleasure for being guilty and then pleasure for admitting his guilt, an act which he considers his way of presenting his own "truth" and being honest. Finally, as soon as society accepts his guilt, he feels the pleasure of atonement. 

His appeal and articulate and sincere-sounding descriptions of his motives and actions within the confines and context of society, resonate with the youth of the era. He presents himself as a "trapped soul" seeking relief from the burdens of society. The youth believe that his descriptions express their own deeper needs and frustrations and they become enthralled with him. 
 reason of insanity. He is sentenced to life incarceration in a psychiatric hospital.     The film in the end leaves an open question as to who is the real guilty party, the individual or society.  The plot is based on the actual murder case of Maria Bavea ( ) in 1963.   

==Reception==
Angelos Rouvas in his book Greek Cinema mentions that Tonia Marketakis film explores the underlying reasons which cause alienated and lonely people living in large cities to become psychologically disturbed individuals who can then commit violent crimes.   
 In Cold the reconstruction murder scene and the psychiatric wards. 

He comments that in Marketakis film the truths are all over the place, but that the Truth is absent and compares it to Akira Kurosawas Rashomon.  According to Kyriakides, Marketakis technique is to present the elements of her film like a puzzle, where the pieces she supplies fit in a larger mosaic but comments that the picture of the mosaic is never revealed in its entirety. The film ends abruptly, as if it puts an end to its own life, in a final, violent act. 
 theatrical aspects absurd as applied to the Greek society of the 1960s.   

== Cast ==
*Manolis Logiadis as Ioannis Zachos
*Mika Flora as Eleni Chalkia    
*Vangelis Kazan
*Malaina Anousaki		
*Costas Arzoglou		
*Dimitris Bikiropoulos		
*Takis Doukatos (as T. Doukakos)
*Nikos Glavas		
*Minas Hatzisavvas		
*Costas Messaris		
*Mary Metaxa		
*Yorgos Partsalakis		
*Lida Protopsalti		
*Nikitas Tsakiroglou		
*Kostas Tsakonas		
*Thanassis Valtinos		
*Zozo Zarpa		
*Kostas Ziogas

==Awards==

The film was awarded the following prizes at the 14th Thessaloniki International Film Festival in 1973:     
*Direction
*Plot
*Male leading role
*Honourable distinction

== Release ==
The film was released in Greece in September 1973.

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 