Yaagagni
{{Infobox film
| name           = Yaagagni
| image          =
| caption        =
| director       = P Chandrakumar
| producer       =
| writer         =
| screenplay     = Rohini Captain Raju MG Soman
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Jay Arts
| distributor    = Jay Arts
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar. The film stars Suresh Gopi, Rohini (actress)|Rohini, Captain Raju and MG Soman in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Suresh Gopi Rohini
*Captain Raju
*MG Soman
*Philomina
*Sabitha Anand
*Saritha

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaalam Kalyaanakaalam || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 2 || Kaavile Murukanu || K. J. Yesudas, Chorus, R Usha || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 