Manners of Dying
{{Infobox film
| name           = Manners of Dying
| image          = 
| caption        = 
| director       = Jeremy Peter Allen
| producer       = Yves Fortin Gautam Hooja
| screenplay     = Jeremy Peter Allen
| story          = Yann Martel
| starring       = Roy Dupuis Serge Houde
| music          = Éric Pfalzgraf James Gray
| editing        = Jeremy Peter Allen
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Manners of Dying is a 2004 Canadian drama film based on the short story of the same name (1993) by Yann Martel, winner of the Man Booker Prize for his book, The Life of Pi.

== Plot ==
Kevin Barlow (Roy Dupuis) will die on schedule and according to regulations. Harry Parlington (Serge Houde), director of the Cantos execution facility, intends to make sure of it. However Barlow chooses to go, be it calmly or fighting to the end, Parlington feels confident that he and his team can deal with the situation. When Barlow makes an unusual final request, a strange duel ensues between the condemned man and the prison director. In this struggle there can be no winner or loser, only two men faced with doubts and difficult choices to make.

== Recognition ==
* 2006 Genie Award for Best Achievement in Editing - Jeremy Peter Allen - Nominated
* 2006 Genie Award for Best Achievement in Music - Original Score - Éric Pfalzgraf - Nominated

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 