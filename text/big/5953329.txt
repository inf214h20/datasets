Bus 44
{{Infobox film
| name           = Bus 44
| image          = 
| image_size     = 
| caption        = 
| director       = Dayyan Eng
| producer       = 
| writer         = Dayyan Eng
| narrator       = 
| starring       = Gong Beibi Wu Chao Li Yixiang
| music          = 
| cinematography = Sam Koa
| editing        = Dayyan Eng
| distributor    =  2001
| runtime        = Short: 11 min.
| country        = Hong Kong / U.S.A. Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Bus 44 ( ) written & directed by Chinese-American filmmaker Dayyan Eng in 2001, is a short film starring Chinese actress Gong Beibi and Wu Chao. The film won awards at Venice Film Festival, Sundance Film Festival, and was invited to Cannes Film Festival; the first time a Chinese short film won in all three festivals history.

Based on a true story, Bus 44 takes place on the outskirts of a small town and tells the story of a bus driver (Gong) and her passengers encounter with highway robbers. "Bus 44" carries a universal theme that travels across all boundaries and societies, trespassing the dark side and bright side of human behavior.

== Awards ==

*Special Jury Award - 2001 Venice Film Festival
*Jury Honorable Mention - 2002 Sundance Film Festival
*Directors Fortnight - 2002 Cannes Film Festival
*Grand Jury Award - 2002 Florida Film Festival
*Official Selection - 2003 New York Film Festival
*Official Selection - 2002 Clermont-Ferrand Internatinoal Short Film Festival
*Official Selection - 2002 Hong Kong International Film Festival
*Official Selection - 2002 Seattle International Film Festival
*Official Selection - 2002 Toronto Worldwide Short Film Festival
*Official Selection - 2002 Edinburgh Internatinoal Film Festival
*Official Selection - 2001 Pusan International Film Festival
*Official Selection - 2001 Gijon International Film Festival

== External links ==
*  
*  
*   
* 
* 
*   

 

 
 
 
 


 