Forced Entry (1973 film)
{{Infobox film
| name           = Forced Entry
| image          = ForcedEntry1973.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Shaun Costello
| producer       = Jerald Intrator   Shaun Costello   John Klugerman
| writer         = Shaun Costello
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Harry Reems   Laura Cannon
| music          = Santino DiOrici
| cinematography = Jayson Black
| editing        = Shaun Costello
| studio         = Boojum Productions
| distributor    = Variety Films
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $6,200      
| gross          = 
}}

Forced Entry is a 1973 pornographic film written and directed by Shaun Costello under the pseudonym Helmuth Richler. It stars Harry Reems as an unnamed and psychotic Vietnam War veteran who sexually assaults and kills random woman who stop at the filling station where he works as an attendant. Called "one of the most disturbing and unpleasant porn features ever made" the film utilizes actual footage of the war, predominantly in the rape and murder sequences.  

== Plot ==

After opening with newspaper articles, footage, and quotes relating to the Vietnam War and the psychological condition of the American soldiers who have returned from it, the authorities are shown investigating a crime scene in which a man was shot in the head.

Going back an unspecified amount of time, the man is revealed to be a gas station attendant, and is shown coercing a female customer into giving out her name and address by claiming they are needed for a credit card transaction. After the woman drives away, the attendant goes to her apartment building, and watches through a window as she has sex with her husband. When the husband leaves, the attendant breaks into the apartment and forces the woman to perform oral sex on him while he verbally abuses her, slitting her throat after he climaxes.
 sodomizes her, stabs her, and kisses her as she dies. Later, a ditzy female hippie picks up an equally vapid hitchhiker, and the two travel to the gas station. The attendant tricks the driver into giving out her address, and goes to her home, where she and her companion are doing drugs, and having sex. The attendant sneaks in and tries to force the girls to submit to him, but the pair are too intoxicated to take him seriously, frustrating him to the point of a psychotic break. As he screams "Stay away from me!" and has a series of flashbacks to both the war and the rapes and murders he has committed, the attendant shoots himself in the head. Returning to the intro, the mans body is covered by a sheet, and hauled away by the police.

== Cast ==

* Laura Cannon as Lost Driver
* Harry Reems as Gas Station Attendant
* Jutta David as Davids Wife
* Shaun Costello as David
* Ruby Runhouse as Hippie Girl
* Nina Fawcett as Hippie Girl

== Reception ==

In his autobiography, star Harry Reems wrote that Forced Entry was the one film he regretted doing.  

DVD Talk gave the film zero stars, calling it "morally repugnant".  

== Follow-ups ==
 a loose Forced Entry, was released by Extreme Associates in 2002.

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 