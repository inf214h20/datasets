Hum Kaun Hai?
{{Infobox film
| name           =Hum Kaun Hai?
| image          = 
| image_size     = 
| caption        = 
| director       =Ravi Sharma Shankar
| producer       =
| writer         =
| narrator       = 
| starring       =Dimple Kapadia Amitabh Bachchan Dharmendra
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2004
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 The Others starring Nicole Kidman.

==Plot Summary==

After the mysterious disappearance of her maid, butler, and gardener, Mrs. Sandra Williams, the wife of Major Frank, who lives in a palatial house with two children, Sara and David, writes a letter to the local employment agency to get replacements. Martha Pinto, a mute woman, and Edgar And Maria apply for these vacancies. Sandra hires her and also lets know that her childrens eyes suffer from a condition that could cause them to be damaged by sunlight, hence the curtains are to be drawn at all times. Also she gets migraines by any loud noise, hence everyone must remain quiet, there is no electricity and all work must be done during the daytime, and the entire house is candle-lit. The staff gets busy with their work, and it is then Sarah tells her mom that she has noticed other people in the house that are visible to her only, and she draws the picture of a male, a female (his wife), their son, Vicky, and an elder woman with magical powers. Sandra disbelieves her, but when she starts feeling someones presence in her house, as well as hearing noises, she decides to investigate. She finds out that the letter she had sent to the employment agency is still lying in the mailbox. She must now find out how Martha, Edgar and Maria came to know of the vacancies, and of the presence of an album with dead peoples photos. Watch as events beyond her control unfold, leading her, Sara and David face to face with the other unseen people in this household.

==Reception== The Tribune, however, called it an "enthralling suspense thriller", and further noted, "With no smiles, no songs it is a clean spooky affair. Go for it if you have the spunk." it was released in bengali dubbed version named ORA KARAat same time.

==See also==
*List of ghost films

==References==
 

==External links==
*  

 
 
 