Finding Vivian Maier
 
{{Infobox film
| name           = Finding Vivian Maier
| image          = 
| alt            = 
| caption        = 
| director       = {{Plainlist|
* John Maloof
* Charlie Siskel}}
| producer       = {{Plainlist|
* John Maloof
* Charlie Siskel}}
| screenplay     = {{Plainlist|
* John Maloof
* Charlie Siskel}}
| narrator       = John Maloof
| starring       = {{Plainlist|
* John Maloof
* Phil Donahue
* Mary Ellen Mark
* Joel Meyerowitz
* Tim Roth}}
| music          = J. Ralph
| cinematography = John Maloof
| editing        = Aaron Wickenden
| studio         = Ravine Pictures
| distributor    = IFC Films
| released       =  
| runtime        = 84 minutes  
| country        = United States
| language       = {{Plainlist|
* English
* French}}
| budget         = 
| gross          = $2.2 million 
}} directed and produced by John Maloof and Charlie Siskel.       Maiers photographic legacy was largely unknown during her lifetime. The film documents how Maloof discovered her work and, after her death, uncovered her life as a nanny and a photographer in Chicago through interviews with people who knew her.

The film had its world premiere at the 2013 Toronto International Film Festival on 9 September 2013. It was shown in cinemas, and was released on DVD in November 2014.  It won various awards, and was nominated for the Academy Award for Best Documentary Feature at the 87th Academy Awards. 
 executive producer; associate producers. 

==Selected cast==
* John Maloof
* Phil Donahue
* Mary Ellen Mark
* Joel Meyerowitz
* Tim Roth

==Accolades==
* 2014: Best Documentary Feature, Alaska Airlines Audience Award,  . Accessed 7 August 2014. 
* 2014: Best New Director Award, Alaska Airlines Audience Award, Portland International Film Festival. 
* 2014: Founders Prize for Best Documentary, Traverse City Film Festival. 
* 2014: Won (tied with  . Accessed 7 August 2014. 
* 2014: John Schlesinger Award for Outstanding First Feature, Palm Springs International Film Festival.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*   (36 minute video)

 
 
 
 
 
 
 
 
 
 
 
 