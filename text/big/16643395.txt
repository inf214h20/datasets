Vodkaa, komisario Palmu
{{Infobox Film
| name           = Vodkaa, komisario Palmu
| image          = 
| image_size     = 
| caption        = 
| director       = Matti Kassila
| producer       = Mauno Mäkelä Suomen Filmiteollisuus
| writer         = Matti Kassila (writer) Georg Korkman Viktor Klimenko
| music          = Rauno Lehtinen
| cinematography = Esko Nevalainen
| editing        = Juho Gartz
| distributor    = 
| released       =  
| runtime        = 
| country        = Finland Finnish
| budget         = 
| gross          = 
}}

Vodkaa, komisario Palmu (Vodka, Inspector Palmu) is a 1969 film, the fourth and final part of the Inspector Palmu series. The only part of the series to be filmed in color, the film is also the only one not to be based on a novel by Mika Waltari.

The film sees a now-retired and married Palmu helping the Finnish Broadcasting company discover the truth behind the murder of one of their reporters.

==Plot Synopsis==
An important agreement on a tunnel building project is being held between Finland and Soviet Union in secresy, over fears of their political effects. When the press catches wind that the meeting is held at the foreign ministers manor, the talks are hastily moved. A reporter for the Finnish Broadcasting Company, Yleisradio, is murdered on the grounds of the manor and his camera-man is caught by the guards.

The police and Yleisradio cant come to agreement over how to handle the investigation, as the police want to keep the details of the talks a secret. Yleisradio then turns to the retired Palmu to help YLE discover the truth behind the murder. This greatly upsets Inspector Virta, who asks Palmu to stay away from the case. Eventually, Palmu realises that the murder was politically motivated and that a far-right underground group was looking to silence the reporter and saboutage the talks between Finland and the Soviet Union.

== Production ==

Matti Kassila and Mika Waltari originally planned to follow Tähdet Kertovat, Komisario Palmu with a TV-series based on the character called Lepää Rauhassa, Komisario Palmu (Rest in Piece, Inspector Palmu). However, a country-wide writers strike put the plans on hold.

After a few years, Kassila decided to make another Palmu movie and set it in contemporary era (previous Palmu films and novels were set in pre-War Finland and the 1950s). Waltari had no direct involvement with the film. Kassila has later admitted that the film failed to live up to the prior movies, despite featuring elements of satire involving Finnish politics and Yleisradio.

==External links==
* 

 
 
 
 
 