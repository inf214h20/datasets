Baazigar
 
 
 
{{Infobox film
| name = Baazigar
| image = Baazigar_1993.jpg
| caption = 
| director = Abbas-Mustan
| writer = Robin Bhatt  Akash Khurana  Javed Siddiqui
| starring = Shah Rukh Khan Kajol Siddharth Ray Shilpa Shetty Raakhee
| producer = Ganesh Jain
| distributor = Eros Labs
| music = Anu Malik
| released = 12 November 1993
| runtime = 182 minutes
| country = India
| language = Hindi
| gross = 
|}} A Kiss Before Dying.  Initially Salman Khan was the first choice to play the lead in the film.   This was Shah Rukh Khans breakthrough role as the sole lead, Kajols first commercial success and Shilpa Shettys debut film. Baazigar was the first film Shah Rukh Khan played the role of an anti-Hero and also the first film which earned Khan a Filmfare Award for Best Actor. 
It was a major commercial success at the time of its release and the first film of the famous Shahrukh Khan-Kajol jodi.
 Rajasekhar and in Kannada as Nagarahavu_(2002 film)|Nagarahavu with Upendra.

==Synopsis==
After acquiring the possession of the company owned by Vishwanath Sharma (Anant Mahadevan) using trickery, Madan Chopra (Dalip Tahil) dismisses Vishwanath from the company to exact a revenge on the latter, leaving the Sharma household to undergo distress and illness which claims the lives of Vishwanath and his little daughter. A spell of lasting grief casts over Vishwanaths wife (Rakhee Gulzar), while his son Ajay (Shah Rukh Khan) determines to take vengeance on Madan for his familys tragedy. 15 years later, a grown-up Ajay courts Seema (Shilpa Shetty), one of Madans two daughters, while dating Priya (Kajol), the other daughter with a false individuality named Vicky Malhotra, keeping everyone ignorant of his dual-identity. The scary part of the film starts now, when a vindictive Ajay pushes Seema from a building to death making it, to all appearances, look like a suicide while Priya, with a strong belief of Seemas death to be a murder, determines to find out the killer with the help of her friend Inspector Karan (Siddharth Ray).
===Plot===

When reputed businessman Vishwanath Sharma (Anant Mahadevan) discovers that his manager Madan Chopra (Dalip Tahil) is embezzling money right under his nose, Sharma has him thrown in jail. After Chopra completes his jail term, he re-approaches Sharma and asks for forgiveness. Vishwanath rebukes him, but his wife Shobha (Rakhee) takes pity on Chopras daughters and pleads to her husband. Chopra is reappointed in the company. However, Chopra has come back to exact revenge on his former boss. Slowly, but surely, Chopra regains the confidence of Sharma.

One day, when Sharma has to go for a business tour, he hands over the power of attorney to Chopra. Chopra, who has been waiting for such chance, usurps Sharmas company and becomes its de facto owner. Sharma learns of this treachery, by which time his family is ejected out of their home. More tragedy strikes the Sharma household when his newborn daughter dies of fever and Sharma himself dies while trying to buy medicines for her. Devastated by the turn of events, Shobha goes insane, while her son Ajay decides to avenge the wrongdoings by making Chopra pay in the same coin.

Years later, a young Ajay (Shah Rukh Khan) meets Chopras daughter Seema (Shilpa Shetty). Ajay manages to cozy up to Seema, and she falls in love with him. Meanwhile, Ajay takes up the persona of Vicky Malhotra and creates a good impression in the minds of both Chopra and his younger daughter Priya (Kajol). Ajay begins a double game whilst keeping both the parties in dark.

Ajay manages to keep his affair with Seema a secret, while a believing Seema too doesnt let anybody know that she is in love. When Chopra decides to get Seema married, she decides to elope with Ajay. Ajay takes advantage of the opportunity and tricks her into writing a suicide letter. Later, on premise of getting married secretly, Ajay calls Seema to the registrar office. He takes her to the terrace and throws her down, using her suicide note to close the case as suicide. Chopra too hastily has the case closed to prevent any further embarrassment.

Ajay uses Seemas death to gets close to Priya and Chopra. Priya, however, is suspicious that her sister didnt commit suicide. With help of her college friend and Police Inspector Karan Saxena, she decides to investigate the matter secretly. Ravi, another friend of Seema, who had a crush on her, tells Priya about Seemas secret lover. Ravi finds a photo where Seema and Ajay are together at a birthday party, but is killed by Ajay. Ajay forces Ravi to sign a suicide note, making Priya and Karan believe that Ravi must have been Seemas lover and murderer.

Later, Priya and Vicky meet Seemas college friend, Anjali, who thinks that she recognizes Vicky. When she finds out the photo as well, she calls the Sharma household during Vickys and Priyas engagement party. Vicky intercepts the phone, impersonates Chopra, and arrives at her place. He strangles her, stuffs her body in a suitcase, and throws it in the river. Priya and Karan realize that the murderer is still alive. Meanwhile, history repeats itself, with Chopra handing over the power of attorney to "Vicky". Ajay decides to hasten up his plans on learning that Priya and Karan are still bent on finding the killer.

Ajays plan hits a glitch when he and Priya run into the real Vicky Malhotra, Ajays friend whose identity he had taken. Priya becomes suspicious and decides to contact this Vicky. After returning from his business trip, Chopra is shocked to find that the company is run by a Sharma group. Now, Ajay reveals the truth to Chopra and sends him out of the company after humiliating him.

Meanwhile, Priya too learns of Ajays true identity from Vicky, and rushes to Ajays home in Panvel. She is shocked to see a poster of Ajay and finds a marriage locket with the photos of him with her sister.  Ajay comes home, where she confronts him with his misdeeds. He tells her the whole story, and she is shocked at the revelation of what her father did to Ajays family.  Chopra arrives with his henchmen to kill Ajay and exact revenge. After seeing Ajay being beaten by Chopra and hearing Chopras name being repeated, Shobha regains her sanity and runs to her sons defense. Ajay starts bashing Chopras goons and overpowers Chopra. Despite the circumstances, Karan and Priya sympathize with Ajay. In a standoff with Chopra, Ajay decides to spare Chopras life, but then Chopra impales Ajay with a long iron bar, and laughs about his victory. Ajay begins to laugh maniacally as well and rams the bar into Chopras stomach, with both plummeting down from a high wall, killing Chopra, mortally wounding Ajay as well.

Ajay makes it back to his mother who finally recognizes her son. Ajay promises her that hes gotten revenge for their family misfortunes and reacquired everything that was meant to be theirs; now wanting only to rest peacefully in her embrace. Priya and Karan watch despondently as Ajay dies in his mothers arms, finally, in peace.

==Cast==
* Shah Rukh Khan as Ajay Sharma/Vicky Malhotra
* Kajol as Priya Chopra
* Shilpa Shetty as Seema Chopra
* Raakhee as Shobha Sharma {special appearance}
* Dalip Tahil as Madan Chopra
* Siddharth Ray as Inspector Karan Saxena
* Johnny Lever as Babu Lal
* Anant Mahadevan as Vishvanath Sharma
* Resham Tipnis as Anjali Sinha,Seemas Friend
* Dinesh Hingoo as Bajodia Seth
* Manmauji Taalia The Servant
* Adi Irani as Real Vicky Malhotra
* Master Sumeet as Young Ajay
* Sharad Sankla as Charlie (a guy in party)
* Raju Srivastav as a guy in party with Charlie
* Harpal as Motu the Cook.
* Amrit Patel as Seemas Driver

== Soundtrack ==
The music was composed by Anu Malik and won the Filmfare award for best music director. The song "Yeh Kaali Kaali Aankhein" was very popular which bagged singer Kumar Sanu his fourth consecutive Filmfare Award for Best Male Playback Singer after Aashiqui, Saajan and Deewana. Other Singers of this album are Asha Bhosle, Pankaj Udhas, Alka Yagnik, Vinod Rathod and Sonali Bajpai. The song titles are listed below. The soundtrack was released by Venus Music.
{{Infobox album 
| Name = Baazigar
| Type = soundtrack
| Artist = Anu Malik
| Cover =
| Released =
| Genre = Film soundtrack
| Length = 
| Producer    = 
| Label  = Venus Music
| Reviews =
| Last album = Phool Aur Angaar (1993)
| This album = Baazigar (1993) The Gentleman (1994)
}}
{| class="wikitable"
|-
! Song
! Singer
! Picturised on
! Songwriter
|-
| "Baazigar O Baazigar"
| Kumar Sanu & Alka Yagnik
| Shahrukh Khan & Kajol
| Nawab Arzoo
|-
| "Yeh Kaali Kaali Aankhein"
| Kumar Sanu & Anu Malik
| Shahrukh Khan & Kajol
| Dev Kohli
|-
| "Kitabein Bahut Si"
| Asha Bhosle & Vinod Rathod
| Shahrukh Khan & Shilpa Shetty
| Zafar Gorakhpuri
|-
| "Chhupana Bhi Nahi Aata"
| Vinod Rathod
| Siddharth & Kajol
| Rani Malik
|-
| "Chhupana Bhi Nahi Aata"
| Pankaj Udhas
| In soundtrack, excluded in the film
| Rani Malik
|-
| "Samajh Kar Chand Jis Ko"
| Alka Yagnik & Vinod Rathod
| In soundtrack, excluded in the film
| Zameer Kazmi

|-
| "Ae Mere Humsafar"
| Alka Yagnik & Vinod Rathod
| Shahrukh Khan & Shilpa Shetty
| Gauhar Kanpuri
|-
| "Tere Chehre Pe"
| Kumar Sanu & Sonali Bajpai
| In soundtrack, excluded in the film
| Rani Malik
|}

==Box office==
Baazigar was a commercial success and the fourth highest grossing Hindi film of 1993.   

== Filmfare Awards == Best Actor - Shah Rukh Khan Filmfare Best Male Singer - Kumar Sanu for "Yeh Kaali Kaali Aankhein" Best Music Direction - Anu Malik Best Screenplay -  Robin Bhatt, Javed Siddiqui, Akash Khurana

==References==
 

== External links ==
 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 