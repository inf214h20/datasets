The Beekeeper (film)
 
{{Infobox film
| name           = The Beekeeper
| image	         = The Beekeeper FilmPoster.jpeg
| caption        = Film poster
| director       = Theodoros Angelopoulos
| producer       = Theodoros Angelopoulos
| writer         = Theodoros Angelopoulos Tonino Guerra Dimitris Nollas
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Giorgos Arvanitis
| editing        = Takis Yannopoulos
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}
The Beekeeper ( , Transliteration|translit.&nbsp;O Melissokomos) is a 1986 Greek drama film directed by Theodoros Angelopoulos.   

==Cast==
* Marcello Mastroianni as Spyros
* Nadia Mourouzi as the girl
* Serge Reggiani as Sick Man
* Jenny Roussea as spyros wife
* Dinos Iliopoulos as Spyros friend
* Iakovos Panotas as soldier the girls boyfriend
* Vassia Panagopoulou
* Stamatis Gardelis
* Mihalis Giannatos
* Karyofyllia Karabeti
* Konstandinos Konstandopoulos
* Nikos Kouros Christoforos Nezer
* Stratos Pahis
* Dimitris Poulikakos
* Athinodoros Prousalis
* Costas Tymvios
* Dora Volanaki
* Giannis Zavradinos

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 