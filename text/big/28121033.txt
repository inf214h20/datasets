Caravan to Vaccarès (film)
{{Infobox film
| name           = Caravan to Vaccarès
| image          = Caravan to Vaccares poster.jpg
| caption        = Original cinema poster
| alt            = 
| writer         = Alistair MacLean (novel) Paul Wheeler (screenplay) Joseph Forest (adaptation)
| starring       = David Birney, Charlotte Rampling, Michael Lonsdale
| director       = Geoffrey Reeve 
| producer       = George Davis , John McNab, Richard Morris-Adams, Geoffrey Reeve
| distributor    = Rank Film Distributors Ltd (UK) 20th Century Fox (USA)
| released       = August 8, 1974 
| runtime        = 98 minutes (theatrical release)
| music          = Stanley Meyers
| cinematography = Fred Tammes
| editing        = Robert Morgan
| studio         = 
| language       = English 
| budget         = 
| gross          = 
}}
 a novel by Alistair MacLean.

==Plot introduction==
A wandering young American adventurer (David Birney) meets a pretty young British photographer (Charlotte Rampling) when she hitches a ride. By chance, they are hired by a French duke (Michel Lonsdale) to smuggle a Hungarian scientist (Michael Bryan) out of France to the United States. The scientist escaped the Iron Curtain by hiding with a caravan of gypsies, but is being pursued by an unscrupulous gang bent on capturing him for sale to the highest bidder.

==Cast==
* David Birney - Bowman 
* Charlotte Rampling - Lila 
* Michael Lonsdale - Duc de Croyter
* Marcel Bozzuffi - Czerda  Michael Bryant - Zuger 
* Serge Marquand - Ferenc 
* Marianne Eggerickx - Cecile 
* Françoise Brion - Stella 
* Vania Vilers - Vania 
* Manitas De Plata - Ricardo 
* Jean-Pierre Cargol - Jules 
* Jean-Pierre Castaldi - Pierre 
* Jean Michaux - Waiter  Alan Scott - Receptionist 
* Jean-Yves Gautier - Gendarme
* Graham Hill - helicopter pilot

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
   
   
 
 
 


 
 