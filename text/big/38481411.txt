The Awful Truth (1925 film)
{{infobox_film
| name           = The Awful Truth
| image          =
| imagesize      =
| caption        = Paul Powell Elmer Harris
| based on       =  
| writer         = Elmer Rice
| starring       = Agnes Ayres Warner Baxter
| music          =
| cinematography = Joseph A. DuBray
| editing        =
| distributor    = Producers Distributing Corporation
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels; 5,917
| country        = USA
| language       = Silent film (English intertitles)

}} silent film drama released by Producers Distributing Corporation. It is based on a 1922 play, The Awful Truth, by Arthur Richman. Agnes Ayres stars in this silent film version of the play.   
According to IMDb.com this film survives in the UCLA Film & Television.  

==Cast==
*Agnes Ayres - Lucy Satterlee
*Warner Baxter - Norman Satterlee
*Winifred Bryson - Josephine Trent
*Phillips Smalley - Rufus Kempster
*Carrie Clark Ward - Mrs. Julia Leeson
*Raymond Lowney - Danny Leeson William Worthington - Jonathan Sims

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 

 