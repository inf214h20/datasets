Remedial Reading Comprehension
{{Infobox film
| name           = Remedial Reading Comprehension
| image          =
| image size     =
| caption        =
| director       = Owen Land  (as George Landow) 
| producer       =
| writer         =
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| filmed         = 
| released       = 1970
| runtime        = 5 minutes
| country        = United States
| language       = English
| budget         =
}}
 experimental short film by Owen Land, produced in 1970 in film|1970.

==Description==
Remedial Reading Comprehension takes the form of an  , a mock television commercial about rice, text from a speed-reading manual, and footage of the director running, with the superimposed words, "This Is A Film About You... Not Its Maker." Robin Blaetz (2002) "Avant-garde Cinema of the Seventies," History of the American cinema, Volume 9, University of California Press, p473  The speed-reading text is taken from "Pupils into Students," an essay written by Jacques Barzun and published in Teacher in America (1945). Conner, J.D. (2009) "What Becomes of Things on Film on Film: Adaptation in Owen Land (George Landow)," Adaptation, 2(2), 161–76 

Remedial Reading Comprehension forms part of the   itself. Dixon, Wheeler W. (1995) It looks at you: the returned gaze of cinema, SUNY Press, p1-2, 56-7 

==Reception==
Remedial Reading Comprehension is considered a prominent and important work in the structural film movement.  Scholar Fred Camper wrote that the film “does not try to build up an illusion of reality, to combine the images together with the kind of spatial or rhythmic continuity that would suggest that one is watching “real” people or objects. It works rather toward the opposite end, to make one aware of the unreality, the created and mechanical nature, of film.”  Wheeler Dixon wrote that "the film acts upon us, addressing us, viewing us as we view it, until the film itself becomes a gaze, rather than an object to be gazed upon.”  J.D. Connor considered Remedial Reading Comprehension to be an "odd intervention" in the structural film movement, in that it retains "the Romantic interest in dreams and personae" away from which avant-garde cinema had been moving. 

==References==
 

==External links==
*  

 
 
 
 
 