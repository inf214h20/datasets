The Hound of the Baskervilles (1978 film)
{{Infobox film
| name           = The Hound of the Baskervilles
| image          =Poster of the movie The Hound of the Baskervilles.jpg
| image_size     =
| caption        =
| director       = Paul Morrissey
| producer       = John Goldstone
| writer         = Peter Cook, Dudley Moore & Paul Morrissey (screenplay) Arthur Conan Doyle (novel)
| narrator       =
| starring       = Peter Cook Dudley Moore
| music          = Dudley Moore
| cinematography = Dick Bush John Wilcox
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
The Hound of the Baskervilles is a 1978 British comedy film spoofing The Hound of the Baskervilles by Sir Arthur Conan Doyle. It starred Peter Cook as Sherlock Holmes and Dudley Moore as Doctor Watson|Dr. Watson. A number of other well-known British comedy actors appeared in the film including Terry-Thomas (in his final screen appearance), Kenneth Williams and Denholm Elliott.

==Plot==
The film begins in a theater, where a pianist (Moore) begins to play a piano accompaniment to the actual film being shown in the theater.
 Welsh eccentric.

Upon arriving at the station, Sir Henry, Dr. Mortimer, Watson and Perkins (their driver) are halted by a policeman (Spike Milligan), who warns them of a murderer stalking the moors, before sending the group on their way.
 chihuahua that proceeds to urinate in Watsons pocket and face.

Arriving at Merripit Hall, Watson meets the eccentric Mrs. Stapleton, who displays surreal symptoms suggesting demonic possession. Late at night, Sir Henry and Watson discover the Seldons and the escaped murderer, whom Watson recognizes as Mrs. Barrymores brother Ethel Seldon (Roy Kinnear), having a family dinner. Oddly enough, neither of the men seems to panic at this.

Afterward, Holmes arrives and examines the case so far. An invitation arrives for Sir Henry, asking him to dinner at Merripit Hall. Suspecting a trap, Watson goes along with Sir Henry while Holmes observes carefully. Mrs. Stapleton resumes her bizarre acts and begins to vomit pale-blue liquid over Sir Henry, whilst Mr. Stapletons chihuahua urinates in Watsons soup.

Ordered to leave in disgrace, the Stapletons, Dr. Mortimer, Mr. Frankland (Denholm Elliott), and his wife Mary (Dana Gillespie) follow Sir Henry and Watson to kill them, but become trapped in a quagmire. Holmes then proceeds to reveal that the Hound is no more than a large, rather friendly Irish wolfhound owned by the late Sir Charles Baskerville, whose excited barking was misinterpreted as a monstrous beast. He also states that the dog is the sole heir of Sir Charles. With the dog gone, the would-be murderers would have gained the Baskerville fortune and the estate.

The film ends on the pianist, who is then hit by vegetables from the audience.

==Cast==
* Peter Cook &ndash; Sherlock Holmes
* Dudley Moore &ndash; Doctor Watson / Mr. Spiggot / Mrs. Ada Holmes / Piano Player
* Denholm Elliott &ndash; Stapleton
* Joan Greenwood &ndash; Beryl Stapleton
* Hugh Griffith &ndash; Frankland
* Irene Handl &ndash; Mrs. Barrymore
* Terry-Thomas &ndash; Dr. Mortimer
* Max Wall &ndash; Arthur Barrymore
* Kenneth Williams &ndash; Sir Henry Baskerville
* Roy Kinnear &ndash; Ethel Seldon
* Dana Gillespie &ndash; Mary Frankland Lucy Griffiths &ndash; Iris
* Penelope Keith &ndash; Massage-Parlour Receptionist
* Jessie Matthews &ndash; Mrs. Tinsdale
* Prunella Scales &ndash; Glynis
* Josephine Tewson &ndash; Nun
* Rita Webb &ndash; Elder Masseuse
* Henry Woolf &ndash; Shopkeeper
* Spike Milligan &ndash; Policeman

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 