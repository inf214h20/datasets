Le Grand Voyage
{{Infobox film
| name           = Le Grand Voyage
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ismaël Ferroukhi
| producer       = Humbert Balsan
| writer         = Ismaël Ferroukhi
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Nicolas Cazalé, Mohamed Majd
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Pyramide Distribution
| released       =  
| runtime        = 108 minutes
| country        = France, Morocco, Bulgaria, Turkey French
| budget         = 
| gross          = 
}} 2004 film directed by 2004 Toronto and Venice International Film Festivals.

==Plot== pilgrimage to French to his father, who is seen speaking only Arabic for the majority of the film.  Later, when necessary, the father proves that he in fact speaks impeccable French; his choice to speak only Arabic to his son is therefore purposeful. 

Along the way, the two meet several interesting characters. The son learns about Islam and why his father thought it would be preferable to make the pilgrimage by car rather than by plane.

The route taken by the father and son goes from Provence, France through Italy, Slovenia, Croatia, Serbia, Bulgaria, Turkey, Syria, and Jordan before reaching Saudi Arabia.

==Cast==
*Nicolas Cazalé – Réda
*Mohamed Majd – The Father
*Jacky Nercessian – Mustapha
*Ghina Ognianova – The old woman
*Kamel Belghazi – Khalid
*Atik Mohamed – Le pélerin Ahmad

==Production==
Most scenes that were set in the Middle East were shot in Morocco. However, some scenes involving the two principal actors were shot in Mecca. While the Saudi Arabian government had previously permitted documentary crews to shoot in Mecca, this was the first fiction feature permitted to shoot during the Hajj. The films director, Ismaël Ferroukhi, said that while shooting in Mecca, "no one looked at the camera; people didnt even seem to see the crew – theyre in another world." 

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 

 