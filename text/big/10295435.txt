Test Tube Babies (film)
{{Infobox film
| name           = Test Tube Babies
| image          = Test Tube Babies (1948) - Title.jpg
| image_size     = 190px
| caption        = title card for film
| director       = W. Merle Connell George Weiss
| writer         = Richard S. McMahan
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio    = Screen Classics
| released       = 1948
| runtime        = 70 minutes
| country        = United States English
| budget         = 
| gross = 
|
}} 1948 exploitation George Weiss, it is a narrative about artificial insemination with scenes of nudity and sexual promiscuity included. One scene shows the male lead characters sperm viewed through a microscope.

The film is also known as Blessed Are They (American reissue title), Sins of Love (American reissue title) and The Pill (America reissue title, recut version).

==Plot==

A young married couple find themselves drifting apart. Wife Cathy Bennet (Dorothy Duke) finds temporary pleasure at swinging parties or in the arms of another man, Frank Grover (John Michael). Husband George Bennet (William Thomason) confronts his wife about the widening chasm between them; she tells him she feels they are somehow incomplete without children. She undergoes testing to see why she hasnt conceived. George, who has accompanied her, is asked to also undergo testing and is found to be the problem: he is Sterility (physiology)|sterile. Physician Dr. Wright (Timothy Farrell) suggests artificial insemination using a sperm donor. This proves successful and the Bennetts begin a new and happy phase of their marriage.

==Cast==

*Dorothy Duke as Cathy Bennett
*William Thomason as George Bennett
*Timothy Farrell as Dr. Wright
*John Michael as Frank Grover
*Margaret Roach (here as Peggy Roach) as Cathys mother
*Stacey Alexander as Don Williams
*Georgie Barton as Betty Williams
*Mary Lou Reckow as Dolores LaFleur
*Bebe Berto as Jerry
*Guy Gordon as Phil
*Helen Cogan as Grace
*Gine Franklin as Ralph
*Zona Siggins as Nurse Mason

==See also==
* List of films in the public domain

==References==
 
*  
*  

 
 
 
 
 
 
 


 
 