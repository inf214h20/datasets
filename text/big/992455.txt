Gothic (film)
 
 
{{Infobox film
| name            = Gothic
| image           = Gothic-1986-poster.png
| image_size      = 225px
| caption         = 1986 Virgin Films poster
| director        = Ken Russell
| story           = Lord Byron Percy Bysshe Shelley
| screenplay      = Stephen Volk
| starring        = Gabriel Byrne Julian Sands Natasha Richardson Myriam Cyr Timothy Spall
| producer        = Al Clark Robert Devereux 
| cinematographer = Mike Southon
| editing         = Michael Bradsell
| music           = Thomas Dolby
| runtime         = 88 minutes
| country         = United Kingdom
| language        = English
| released        =   
| budget          =$4.5 million 
| gross           = $916,172
}}
 British horror film directed by Ken Russell, starring Gabriel Byrne as Lord Byron, Julian Sands as Percy Bysshe Shelley, Natasha Richardson as Mary Shelley, Myriam Cyr as Claire Clairmont (Mary Shelleys stepsister) and Timothy Spall as Dr. John William Polidori. It features a soundtrack by Thomas Dolby, and marks Richardsons film debut.
 horror story, which ultimately led to Mary Shelley writing Frankenstein and John Polidori writing The Vampyre. The same event has also been portrayed in the films Bride of Frankenstein (1935) and Haunted Summer (1988), among others.

The films poster motif is based on Henry Fuselis painting The Nightmare, which is also referenced in the film.

==Plot==
 
Through her stepsister Claire Clairmont (Miriam Cyr), Mary Godwin (Natasha Richardson) and her future husband Percy Shelley (Julian Sands) came to know Lord Byron (Gabriel Byrne). During the summer of 1816, Lord Byron invited them to stay for a while at Villa Diodati in Switzerland. There they met Byrons physician friend, Dr. John Polidori (Timothy Spall). On June 16th, while a storm raged outside, the five of them amused themselves by telling ghost stories and revealing private skeletons. From Marys previous experience of miscarriage came the desire to raise her child from the dead, which led to the creation of the Frankenstein monster. From Polidoris homosexuality, suicidal thoughts, and fascination with vampires came the story "The Vampyre".

==Cast==
*Gabriel Byrne as Lord Byron
*Julian Sands as Percy Bysshe Shelley
*Natasha Richardson as Mary Shelley
*Myriam Cyr as Claire Clairmont
*Timothy Spall as John William Polidori|Dr. John William Polidori
*Alec Mango as Murray
*Andreas Wisniewski as Fletcher
*Dexter Fletcher as Rushton

==Reception==
 
The film earned mixed reviews from critics and holds a 50% rating on Rotten Tomatoes based on 12 reviews.  According to Dan Ireland, who later worked with Russell, the film was a big success on video. 

==Awards and honors==
Gothic was nominated for three 1987 International Fantasy Film Awards and won two: Gabriel Byrne won as Best Actor, both for his role as Lord Byron in this film and for his role in Defense of the Realm, and the film won for Best Special Effects. Director Ken Russell was nominated for Best Film but did not win. 

==References==
Notes
 

==External links==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 