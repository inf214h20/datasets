Ten Wanted Men
{{Infobox film name           =Ten Wanted Men image          =Tenwmpos.jpg border         = alt            = caption        =Original film poster director       =H. Bruce Humberstone producer       =Harry Joe Brown Randolph Scott writer         = screenplay     =Kenneth Gamet     story          =Irving Ravetch Harriet Frank Jr.    based on       =  narrator       = starring       =Randolph Scott Richard Boone music          =Paul Sawtell cinematography =Wilfred M. Cline editing        =Gene Havlick studio         =Columbia Pictures distributor    =Columbia Pictures released       =  runtime        =80 minutes country        =United States language       =English budget         = gross          =
}} Western film from Columbia Pictures directed by H. Bruce Humberstone. It stars Randolph Scott, Jocelyn Brando, Richard Boone and Alfonso Bedoya.   

==Plot==
Adam Stewart, a lawyer heading west with grown son Howie, is persuaded by brother John Stewart to settle down near him in Ocatilla, Arizona, where he has a ranch and romantic interest in a widow, Corinne Michaels.

The menacing rancher Wick Campbell has an attractive ward, Maria Segura, and also lusts for her, but she wants no part of that. Her interest in Howie strikes a jealous chord in Campbell, who hires gunfighters led by Frank Scavo to rid the region of the meddlesome Stewarts once and for all.

Campbells thugs kill a rancher and stampede cattle. One picks a fight with Howie, who surprisingly beats him to the draw in self-defense, only to be locked up by Sheriff Gibbons, falsely accused of murder. Howie busts out and flees with Maria.

Adam is killed in cold blood by Campbell, for which Howie blames himself while promising to get even. During a gun battle, the cowardly Campbell uses the sheriffs wife as a hostage. Scavo kills him, intending to take over the territory himself.

With assistance from the sheriff, John and Howie take on Scavos men and prevail. The town has law and order at last, while the Stewarts celebrate a double wedding.

==Cast==
*Randolph Scott as John Stewart
*Jocelyn Brando as Corinne Michaels
*Richard Boone as Wick Campbell
*Alfonso Bedoya as Hermando
*Donna Martell as Maria Segura
*Skip Homeier as Howie Stewart
*Clem Bevans as Tod Grinnel
*Leo Gordon as Frank Scavo
*Minor Watson as Jason Carr
*Lester Matthews as Adam Stewart
*Tom Powers as Henry Green
*Dennis Weaver as Sheriff Clyde Gibbons
*Lee Van Cleef as Al Drucker

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 