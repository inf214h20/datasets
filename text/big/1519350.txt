Not Another Teen Movie
 
 
{{Infobox film
| name            = Not Another Teen Movie
| image           = Not Another Teen Movie poster.jpg
| alt             = 
| caption         = Theatrical release poster
| director        = Joel Gallen
| producer        = {{Plainlist | 
* Neal H. Moritz
* Phil Beauman
* Mike Bender
* Buddy Johnson
}}
| writer          = {{Plainlist | 
* Mike Bender
* Adam Jay Epstein
* Andrew Jacobson
* Phil Beauman
* Buddy Johnson
}}
| starring        = {{Plainlist | 
* Chyler Leigh Chris Evans
* Jaime Pressly
* Eric Christian Olsen
* Eric Jungmann
* Mia Kirshner
* Deon Richmond
* Cody McMains
* Sam Huntington
* Samm Levine
* Cerina Vincent
* Ron Lester
* Lacey Chabert
* Riley Smith
}} Theodore Shapiro
| cinematography = Reynaldo Villalobos
| editing        = Steven Welch
| studio         = Original Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $66,468,332   
}} teen comedy parody of Bring It American Pie, American Beauty, Varsity Blues, Billboard Hot 100.

==Plot== John Hughes football star Chris Evans). retribution by turning Janey Briggs (Chyler Leigh), a "uniquely rebellious girl", the prom queen.
 sexually attracted to him; Janeys unnoticed admirer and best friend, Ricky Lipman (Eric Jungmann); and memories from his past football career. Catherine eventually assists her brother by slightly altering Janeys appearance (by simply removing her glasses and ponytail), instantly making her drop dead gorgeous.

Meanwhile, Janeys younger brother, Mitch (Cody McMains), and his friends, Ox (Sam Huntington) and Bruce (Samm Levine), make a pact to lose their virginity by graduation despite still being in their freshman year. Mitch tries to impress his longtime crush, the beautiful yet perverted Amanda Becker (Lacey Chabert) with a letter professing his love for her. Bruce says that he does not have a chance with her, mockingly stating, "Keep dreaming!"

As the prom draws near, Jake draws infamy among his peers after he fails to lead his football team to victory at the state championship game the year before. The situation is further worsened when Austin tricks Jake into telling Janey about his plan to spite Priscilla by pretending to whisper the secret bet in Janeys ear, causing her to immediately leave Jake. During prom night, Austin and Janey go together; a jealous Jake and Catherine have a dance-off with Austin and Janey, with Catherine dancing in a sexual manner. Janey runs off crying. Meanwhile, Mitch and his friends are having a lousy time at the prom until Amanda arrives and Mitch gives her the letter and Ox later hooks up with Catherine.

Jake is awarded prom king and the principal reads out that the votes for prom queen are tied. Everyone thinks that it is between Janey and Priscilla, but they are shocked to find that Kara and Sara Fratelli (Samaire Armstrong and Nectar Rose), twins conjoined at the head, win prom queen. During the traditional prom king and queen dance, Janey supposedly left with Austin to go to a hotel.

Jake goes to the hotel room where he finds Austin having wild sex with a girl but is shocked to find that it is Priscilla not Janey while the weird Les videotapes with his pants down supposedly having an erection, Austin tells Jake that Janey "ran home to her daddy". Jake angrily punches Austin and Priscilla for what they had done to Janey, then punches Les for "being really weird" (he also punches a plastic bag that happens to be floating next to Les); afterwards he runs to Janeys house only to learn from her father (Randy Quaid) that she is going to Paris for art school.
 American Pie, American Beauty, The Karate Kid, and she decides to stay with him.

==Cast==
 
* Chyler Leigh as Janey Briggs ("The Pretty Ugly Girl") Chris Evans as Jake Wyler ("The Popular Jock (athlete)|Jock")
* Jaime Pressly as Priscilla ("The Nasty Cheerleading|Cheerleader")
* Eric Christian Olsen as Austin ("The Cocky Blond Guy")
* Mia Kirshner as Catherine Wyler ("The Cruelest Girl") Token Black Guy")
* Eric Jungmann as Ricky Lipman ("The Obsessed Best Friend")
* Ron Lester as Reggie Ray ("The Stupid Fat Guy")
* Cody McMains as Mitch Briggs ("The Desperate Virgin")
* Sam Huntington as Ox ("The Sensitive Guy")
* Samm Levine as Bruce ("The Wannabe")
* Lacey Chabert as Amanda Becker ("The Perfect Girl")
* Cerina Vincent as Areola ("The Foreign Exchange Student")
* Riley Smith as Les ("The Beautiful Weirdo")
* Samaire Armstrong as Kara Fratelli
* Nectar Rose as Sara Fratelli
* Ed Lauter as The Coach
* Randy Quaid as Mr. Briggs
* Joanna García as Sandy Sue
* Beverly Polcyn as Sadie Agatha Johnson Robert Patrick Benedict as Preston Wasserstein
* Patrick St. Esprit as Austins father
* Josh Radnor as Tour Guide
* Paul Goebel as The Chef Who Ejaculated Into Mitchs French Toast
* George Wyner as Principal Cornish 
 

;Cameos
Many stars of teen movies, as well as teen films from the 1980s, make credited and uncredited appearances. These include:
* Molly Ringwald as "The Rude, Hot Flight Attendant"; Ringwald starred in many 80s teen movies, most significantly Pretty in Pink, Sixteen Candles, and The Breakfast Club.
* Mr. T as "The Wise Janitor"; The A-Team s opening sequence music is playing at the end of his speech.
* Kyle Cease as "The Slow Clap Guy"; Cease played Bogey Lowenstein in 10 Things I Hate About You.
* Melissa Joan Hart (uncredited) as "Slow Clappers Instructor"; Hart can also be seen in Cant Hardly Wait and Drive Me Crazy. The commentator at the football game praises Hart and Sabrina the Teenage Witch. Lyman Ward as Mr. Wyler; Ward played Ferris Buellers father in Ferris Buellers Day Off.
* Paul Gleason as Richard "Dick" Vernon; Gleason played Vernon in The Breakfast Club.
* Sean Patrick Thomas as "The Other Token Black Guy"; Thomas appeared in teen movies Cant Hardly Wait, Cruel Intentions and Save the Last Dance.
* Good Charlotte as the band playing at the prom.

==List of films parodied==
 
* 10 Things I Hate About You 
* Almost Famous American Beauty  American Pie Better Off Dead
* The Breakfast Club Bring It On
* Cant Hardly Wait 
* Cruel Intentions  Dazed and Confused Detroit Rock City
* Election (1999 film)|Election
* Fast Times at Ridgemont High
* Ferris Buellers Day Off
* Grease (film)|Grease
* Jawbreaker (film) |Jawbreaker The Karate Kid
* Lucas (film)|Lucas
* Never Been Kissed
* Pleasantville (film)|Pleasantville
* Pretty in Pink
* Risky Business Road Trip
* Rudy (film)|Rudy
* Shes All That  
* Sixteen Candles Varsity Blues 
 

==Extra footage==
;Alternate footage
* Three scenes that appear on the green-band trailer are not included in the film: a scene that spoofs Save the Last Dance where a girl is dancing at the big party, a scene that spoofs Never Been Kissed during the football game with Sadie standing on the football field with a microphone before the entire football team runs her down (the person she was waiting for - one of the school teachers - gets up out of the bleachers just before she is run down and then sits immediately after she is trampled), and a small scene with Areola asking the principal if her breasts are perky (this scene does appear in the unrated cut of the film).
* In the trailer for the film, Jake wears boxer shorts during the whipped cream bikini scene, whereas during that same scene in the film, he wears no shorts; his bare crotch is covered with whipped cream instead.

;Footage during credits
The film has three additional scenes Post-credits scene|during/after the credits:
* Mitch, Ox, and Bruce talking about what they learned from the whole experience (only in the unrated cut).
* Mr. Briggs, in a parody of a scene from American Pie, talks about a "three-way" while holding two pies (only in the rated cut).
* The albino folk singer sings about being blind, and her corneas being burned out by the sun (after all the credits have finished).

==Home media==
The R-rated version of the film was released on DVD on April 30, 2002 with the original 89-minute cut. The Unrated Extended Directors Cut was released July 26, 2005 with all of the original special features from the original DVD. The unrated version includes an added 11 minutes to the film, adding up to 100 minutes.

==Reception==
===Box office===
The film opened at number 3 at the US box office taking $12,615,116 in its opening weekend behind Vanilla Sky s opening weekend and Oceans Eleven (2001 film) s second weekend. 

===Critical response===
The film received generally negative reviews from critics. Rotten Tomatoes gives the film a score of 28% based reviews from 96, with an average score of 4 out of 10. The sites consensus states: "NATM has some funny moments, but the movie requires the audience to have familiarity with the movies being spoofed and a tolerance for toilet and sexual humor to be truly effective."  
Metacritic gives the film a score of 32/100 based on reviews from 22 critics, indicating "generally unfavorable reviews". 

Roger Ebert gives the film two stars out of a possible four, and admitted to laughing a few times but not as much as he did for American Pie or Scary Movie. Ebert also criticizes the scatological humor. He urges audiences to not waste their time on the film, when in the month of December there are 21 other promising films to choose from. http://www.rogerebert.com/reviews/not-another-teen-movie-2001 

Robin Rauzi of The Los Angeles Times calls it "a 90-minute exercise in redefining the word gratuitous" and suggests it is most likely to appeal to fourteen-year-olds -- "who of course is not supposed to be seeing this R-rated movie".    Dennis Harvey of Variety (magazine)|Variety criticizes the film for its "overall tendency to mistake mere bad taste for outrageousness, and plain referentiality for satire" 
and praises Evans, Pressly, and Olsen for giving performances better than the material. He notes the film follows the model of Scary Movie but lacking the comic finesse of Anna Faris.   
Mick LaSalle of the San Francisco Chronicle calls the film "a crass act" and points out the futility of trying to parody films that are already absurd. LaSalle complains that the film too closely copies Shes All That and calls it "pathetic" that Not Another Teen Movie is just another formulaic teen movie. {{cite news | title = A crass act. Gross-out teen flick imagines its a parody | author = Mick LaSalle | work = San Francisco Chronicle | url = http://www.sfgate.com/cgi-bin/article.cgi?f=/chronicle/archive/2001/12/14/DD138285.DTL&type=movies | archiveurl =  
http://web.archive.org/web/20020817185801/http://www.sfgate.com/cgi-bin/article.cgi?f=/chronicle/archive/2001/12/14/DD138285.DTL&type=movies | archivedate = 2002-08-17 | date=December 14, 2001}} 

==Music==
  
The soundtrack for the film features metal, punk and rock artists from the 1990s and 2000s, mostly covering songs from the 1980s, and this CD was released by Maverick Records in 2001.

  Tainted Love" Marilyn Manson
# "Never Let Me Down Again" (Depeche Mode) - The Smashing Pumpkins Blue Monday" Orgy
# The Metro" (Berlin (band)|Berlin) - System of a Down But Not Tonight" (Depeche Mode) - Scott Weiland The Pretenders) Saliva
# "Bizarre Love Triangle" (New Order) - Stabbing Westward Goldfinger
# Modern English) - Mest
# "If You Leave" (Orchestral Manoeuvres in the Dark|OMD) - Good Charlotte Muse
# "Somebodys Baby" (Jackson Browne) - Phantom Planet Bad Ronald
# "Prom Tonight" - Not Another Teen Movie cast Kiss Me"  (Sixpence None the Richer)

==See also==
* Scary Movie (film series)|Scary Movie series
* Epic Movie

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 