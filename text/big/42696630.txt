Suprabhatha
{{Infobox film	
| name = Suprabhatha
| image =
| caption =
| director = Dinesh Babu
| producer = Prabha Raj
| writer = Dinesh Babu
| narrator = Vishnuvardhan Suhasini Srividya
| music = Rajan-Nagendra
| cinematography = Dinesh Babu
| editing =  
| studio=  Suryaprabha Films
| released =  
| runtime = 121 minutes
| country = India Kannada
| budget =
| gross =
}}	
Suprabhatha ( ) is a 1988 Indian Kannada language romantic drama film directed and written by Dinesh Babu  making his first entry as a director. The film starred Vishnuvardhan (actor)|Vishnuvardhan,  Suhasini and Srividya in the lead roles.  The music was composed by Rajan-Nagendra and the dialogues & lyrics were written by Chi. Udaya Shankar. The film was presented by Charuhasan and produced by Prabha Raj.
 Best Cinematographer Karnataka State Filmfare Best Filmfare Best Actor award Karnataka State Film Awards also.

==Cast== Vishnuvardhan
* Suhasini
* Srividya
* Maanu
* Vijaya Kashi
* Anand Kumar
* Baby Mala
* Rajendra Singh

==Soundtrack==
All songs were composed by Rajan-Nagendra for the lyrics of Chi. Udaya Shankar.  The soundtrack was hugely successful upon release.

* Ee Hrudaya Haadide - S. P. Balasubramanyam
* Nanna Haadu Nannadu - S. P. Balasubramanyam
* Cheluve Nanna Cheluve - S. P. Balasubramanyam, K. S. Chithra
* Aralida Aase - S. P. Balasubramanyam, K. S. Chithra
* Ee Hrudaya Haadide - K. S. Chithra
* Cheluva Nanna Cheluva - K. S. Chithra

==Awards==

* Karnataka State Film Award for Best Cinematographer - Dinesh Babu
* Filmfare Award for Best Director - Kannada - Dinesh Babu Vishnuvardhan
* Suhasini
* Aragini Readers Award
* Cine Express Award
* Tarangini Berkley Award
* Indira Pratishtana National Award
* Kannada Film Fans Association Award
* Kaladevi Award(Chennai)
* It screened at 12th IFFI mainstream section.

==References==
 

== External links ==
*  
*  
 
 
 
 
 
 
 
 

 