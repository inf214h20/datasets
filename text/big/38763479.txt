Kit Carson (film)
{{Infobox film
| name           = Kit Carson
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = George B. Seitz John E. Burch (assistant)
| producer       = Edward Small
| writer         = Evelyn Wells 
| based on       = newspaper serial by Evelyn Wells
| screenplay     = George Bruce
| narrator       =  Jon Hall Lynn Bari Dana Andrews Edward Ward
| cinematography = John J. Mescall Robert Pittack
| editing        = William F. Claxton Fred R. Feitshans Jr.
| studio         = Edward Small Productions
| distributor    = United Artists
| released       =  
| runtime        = 97 min
| country        = United States English
| budget         = 
| gross          = 
}}
 1940 Western Western directed Jon Hall, Lynn Bari and Dana Andrews. This picture was filmed on location at Cayente (Kayenta, Arizona|Kayenta) AZ  and was one of the early films to use Monument Valley as a backdrop. The supporting cast features Ward Bond as a character named "Ape," future Lone Ranger Clayton Moore without his mask, and Raymond Hatton as Jim Bridger.

==Plot summary==
  Jon Hall) and his two saddle pals, Ape (Ward Bond) and Lopez (Harold Huber) are attacked by Indians. They manage to escape unscatched and make their way to Fort Bridger, where Captain John Fremont (Dana Andrews) hires Carson to guide a wagon train westward to California south along the Oregon Trail. Both Carson and Fremont fall in love with pretty Dolores Murphy (Lynn Bari), on her way to her fathers hacienda in Monterey, California|Monterey. Meanwhile, General Castro (C. Henry Gordon), the Mexican Governor General of California, arms the Shoshoni Indians in an effort to keep the Americans out of California. 

==Cast== Jon Hall as Kit Carson  
* Lynn Bari as Dolores Murphy  
* Dana Andrews as Captain John C. Fremont  
* Harold Huber as Lopez  
* Ward Bond as Ape  
* Renie Riano as Miss Pilchard  
* Clayton Moore as Paul Terry  
* Rowena Cook as Alice Terry  
* Raymond Hatton as Jim Bridger  
* Harry Strang as Sergeant Clanahan  
* C. Henry Gordon as General José Antonio Castro  
* Lew Merrill as General Mariano Guadalupe Vallejo   
* Stanley Andrews as Thomas O. Larkin  
* Edwin Maxwell as John Sutter   George Lynn as James King (as Peter Lynn)

==Production==
The movie was one of several Edward Small made for United Artists. Victor McLaglen was originally announced for the title role, SCREEN NEWS HERE AND IN HOLLYWOOD: Metro Will Make Picture of Escape, Novel About Actress Held in Concentration Camp JAMES STEWART FILM DUE Music Halls Mr. Smith Goes to Washington, Here Thursday --Another Opening Listed John Stahl Leaves Universal Role for Marjorie Rambeau
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 Oct 1939: 30.  and then Randolph Scott. SCREEN NEWS HERE AND IN HOLLYWOOD: Metro Will Produce Escape, With Norma Shearer and Robert Taylor in Leads TWO FILMS OPEN TODAY Dr. Cyclops in Local Premiere at Paramount--Criterion Bills One Was Beautiful Roosevelt Changes Plans Role for Humphrey Bogart
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   10 Apr 1940: 35.  Joel McCrea and Henry Fonda were also named. 32 FILMS ON LIST OF UNITED ARTISTS: Pictures by Eleven Producers to Be Released by Company During 1939-40 Season LEAD FOR JASCHA HEIFETZ Will Star in Goldwyns Music School--Rebecca and My Son, My Son! on Schedule Leslie Howard to Star "Of Mice and Men Listed"
New York Times (1923-Current file)   08 May 1939: 23.  

Jon Hall had just made South of Pago Pago for Edward Small and was borrowed from Sam Goldwyn. Lynn Bari was borrowed from 20th Century Fox. Filming started on March 10, 1940. SCREEN NEWS HERE AND IN HOLLYWOOD: Melvyn Douglas Gets Lead in Story of Refugee in Paris-- Wendy Barrie Signed NEW ITALIAN FILM TODAY Opens at Cine Roma--Helen Vinson Here After Playing in New Cagney Picture Of Local Origin
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   01 May 1940: 31.   It was shot on location in Arizona. Hawaiian Plays Indian in Film
Los Angeles Times (1923-Current File)   20 Sep 1940: A13 

The film was later remade as Frontier Uprising (1961).

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 