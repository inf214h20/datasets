Knights of the Teutonic Order (film)
{{Infobox Film
  | name = Knights of the Teutonic Order
  | director = Aleksander Ford
  | producer = Zygmunt Król
  | screenplay = Jerzy Stefan Stawiński Aleksander Ford Henryk Sienkiewicz (novel)
  | music = Kazimierz Serocki
  | cinematography = Mieczysław Jahoda
  | starring = 	Grażyna Staniszewska (actress)|Grażyna Staniszewska Urszula Modrzyńska Mieczysław Kalenik Aleksander Fogiel
  | studio         = Zespół Filmowy
  | distributor    =  
  | released = 15 July 1960
  | runtime = 166 minutes
  | country = Peoples Republic of Poland Polish
  | budget         = zł38,000,000  
  | gross          = zł100,000,000 (by March 1961) Maria Dąbrowska, Tadeusz Drewnowski. Dzienniki: 1958-1965. Czytelnik (1988). ISBN 9788307009742. p. 217. 
  }}
  portrayed by Stanisław Jasiukiewicz]]
 1960 Cinema Polish film novel of the same name by Henryk Sienkiewicz.
 Polish submission to the 33rd Academy Awards.

It was released on 15 July 1960, the 550th anniversary of the battle of Grunwald.

== Cast ==
* Grażyna Staniszewska (actress)|Grażyna Staniszewska – Danusia Jurandówna
* Urszula Modrzyńska – Jagienka ze Zgorzelic
* Mieczysław Kalenik – Zbyszko z Bogdańca
* Aleksander Fogiel – Maćko z Bogdańca
* Andrzej Szalawski – Jurand ze Spychowa
* Leon Niemczyk – Fulko de Lorche
* Henryk Borowski – Zygfryd de Löwe
* Mieczysław Voit – Kuno von Lichtenstein
* Tadeusz Białoszczyński – Duke Janusz I of Warsaw
* Stanisław Jasiukiewicz – Grand Master of the Teutonic Knights Ulrich von Jungingen
* Emil Karewicz – King Władysław II Jagiełło|Władysław Jagiełło Witold
* Janusz Strachocki – Grand Master of the Teutonic Knights Konrad von Jungingen Anna Danuta, wife of Janusz I of Warsaw Zawisza Czarny
* Jerzy Kozakiewicz – Cztan z Rogowa
* Janusz Paluszkiewicz – royal marshall
* Jerzy Pichelski - Powala of Taczew

==References==
 

== External links ==
*  
*   at FilmPolski.pl

 

 
 
 
 
 
 
 
 
 
 
 


 
 