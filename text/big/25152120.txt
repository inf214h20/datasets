Actresses (film)
 
{{Infobox film
| name           = Actresses
| image          = Actresses film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| hanja          =  들
| rr             = Yeobaeudeul
| mr             = Yŏpaeudŭl}}
| director       = E J-yong
| producer       = Seo Dong-hyeon E J-yong
| writer         = E J-yong Youn Yuh-jung Lee Mi-sook Go Hyun-jung Choi Ji-woo Kim Min-hee Kim Ok-bin
| starring       = Youn Yuh-jung Lee Mi-sook Go Hyun-jung Choi Ji-woo Kim Min-hee Kim Ok-bin
| music          = Jang Young-gyu Lee Byung-hoon
| cinematography = Hong Kyung-pyo
| editing        = Ham Sung-won Go Ah-mo
| studio         = Moongcle Pictures
| distributor    = Showbox  
| released       =  
| runtime        = 104 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} 2009 South Korean mockumentary-style drama film directed by E J-yong.     

== Plot ==
Six actresses — Youn Yuh-jung,    Lee Mi-sook, Go Hyun-jung, Choi Ji-woo, Kim Min-hee and Kim Ok-bin, each portraying themselves — come together for a Vogue (magazine)|Vogue Korea magazine photo shoot at a studio in Cheongdam-dong, Seoul on Christmas Eve, resulting in a clash of egos between individuals not used to sharing the limelight.

== Production ==
Director  ,  . Retrieved 2012-11-20. 

Actresses was made without a script, instead being filmed on a scene-by-scene basis with the actresses improvising their performances according to the given situation.  E noted: "I provided the basis for conflict and the actresses took it from there. The six women represent Korean actresses as a whole, and instead of creating something fictional I thought it would be interesting to feature each actress charms and show something real."  The film includes a real-life confrontation between Go and Choi, with each admitting that their on-set relationship was strained, and Go saying that she felt envious of Chois looks.     

== Release ==
Actresses was released in South Korea on December 10, 2009.  The film pulled in 508,243 admissions. 

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 