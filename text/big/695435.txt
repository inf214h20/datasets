Higher Learning
 
{{Infobox film
| name           = Higher Learning
| image          = Higher Learning (movie).jpg
| caption        = Theatrical release poster
| alt            =
| writer         = John Singleton
| starring       = Omar Epps Kristy Swanson Ice Cube Jennifer Connelly Laurence Fishburne Michael Rapaport Cole Hauser
| director       = John Singleton Paul Hall
| music          = Stanley Clarke
| cinematography =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 127 minutes
| awards         =
| budget         =
| gross          = $38,290,723
}}
Higher Learning is a 1995 American  ), a black track star who struggles with academics; Kristen Connor (Kristy Swanson), a shy and naive girl; and Remy (Michael Rapaport), a lonely and confused man seemingly out of place in his new environment. 

The film also featured Tyra Banks first performance in a theatrical film. Laurence Fishburne won an NAACP Image Award for "Outstanding Supporting Actor in a Motion Picture"; Ice Cube was also nominated for the award. This was the last film appearance of Dedrick D. Gobert, who was shot dead in 1994 prior to the films release.

The exterior shots and outdoor scenes were shot on the campus of University of California, Los Angeles (UCLA) while the interiors were shot at Sony Pictures Studios.

==Plot==
 
We meet Malik Williams (Omar Epps), Kristen Connor (Kristy Swanson), and Remy, (Michael Rapaport), attending the opening freshman pep rally. Malik and Kristen cross paths in the elevator and she shields her purse while Malik, both offended and amused, simply ignores her. Malik, a track star, goes to his first practice unprepared, and is surprised to be chastised by the black coach.

Kristen meets her black roommate, Monet (Regina King) before attending a fraternity rush party with her white girlfriends later that evening. Monet attends a party with other black students, including Malik, hosted by Fudge (Ice Cube), an Afrocentric senior.

Remy, Fudges roommate, is upset at the loud rap music. He calls campus police, who breaks up the party. Fudge asks the police why they arent telling the white students to turn off their "hillbilly music".

While walking home, Kristen meets Taryn (Jennifer Connelly), a lesbian, who warns Kristen about walking alone late at night and invites her to a student group. Maliks roommate, Wayne, is then introduced, with Malik complaining about his filthy side of the room.

The next day, Fudge points out the racially divided groups on campus. In Maliks first class, Professor Phipps (Laurence Fishburne) calls out Malik and Kristen and several others for tuition problems. They go to the financial aid office, where Kristen is told she needs a job and Malik learns that his scholarship is partial, not full. He talks to the coach, who agrees to help him if he keeps his grades up and works extra hard on the field.

Fudge returns to his room with his friend Dreads (Busta Rhymes) and blasts his music, disrupting Remys studying. When Remy complains, Fudge mocks him. Remy later moves out. Later, Remy rejects an offer of pool from Wayne and David Isaacs, Remys new Jewish roommate.

Kristen attends a party with a student named Billy. Billy and Kristen retreat to his room to make out and they begin to have sex. Kristen wants Billy to stop and put on a condom, but he refuses. Despite her pleas, Billy keeps going and what was consensual at first turns ugly - its now rape. She eventually fights him off and flees, with Billy giving chase.

Monet returns to the room and finds Kristen crying. When Billy calls, Monet refuses give the phone to Kristen and Billy insults her racially. Angered, she turns to Fudge, who recruits a group of black students to confront Billy at the frat house. Fudge and Dreads drag Billy outside and force him to apologize. Security arrives and the black students leave. Kristen begins attending Taryns student group on harmony between different student groups.

Malik borrows a book from Fudge and after Fudge asks how he became interested in history, Malik says hes only reading it for a class. Fudge then says that Malik needs to read the book for himself and not for a class, and puts him out. That evening, Remy is reading outside when he is approached by Scott Moss. Scott and his friends—Erik, James and Knocko—soon reveal themselves to be white supremacists.

Professor Phipps challenges his class to determine who they are for themselves and not let others categorize them. Kristen responds to this entreaty by telling Taryn about Billys assault. Taryn consoles her and encourages her to report it.

Malik confronts Phipps about his grade on a paper. When Phipps shows him the various spelling and grammatical errors, Malik then calls Phipps a sellout. Malik then participates in a relay race at a track meet and scores second place after losing ground to another racer. When one of his teammates confront him, he responds by downplaying his role. Sometime afterwards, he flirts with fellow runner Deja (Tyra Banks).

Remy and his new friends gather in Scotts room. The walls are decorated with Nazi paraphernalia. Scott expresses his racist beliefs and Remy is slowly convinced of his ideals. Deja helps Malik with his essay while Kristen realizes her attraction to Taryn.

Phipps then criticizes Kristens bland paper, telling her that her purpose at college is to learn to think for herself. Phipps also meets with Malik about his much improved paper. Remy is shown working in class, revealing his recently shaved head.

Wayne confronts Kristen when she says the anti-sexism group is for women. Kristen later attends a rape awareness rally with Taryn. The Neo-Nazis attack an interracial couple while Remy looks on. Kristen asks to spend the night with Taryn. Taryn rebuffs her, saying she wants Kristen to be sure. While stretching on the track, Malik touts his newfound black ideology to Deja. Deja rebuffs this, saying he has opportunities that people would do anything for and he should take them. A montage shows the two training and falling in love while Kristen begins dating Wayne.

Scott shows Remy his gun collection and Remy talks about his abusive, survivalist father. Kristen forms a deeper bond with Taryn, holding hands and taking walks together. It is later revealed that Kristen sleeps with both Wayne and Taryn. Remy approaches Malik and Deja, condemning Maliks Black Panther shirt and insults him racially. Malik goes to Remys room to confront him. Remy backs down and Malik insults him and leaves. Remy flies into a rage, trashes the room and preps his Glock 9mm. After David returns, he confronts Remy. Remy attacks him. Malik intervenes. Remy draws his weapon, threatening the duo and retreats. Malik runs after him, only to be stopped by security guards asking for ID. Resisting, he is arrested as David protests. Remy escapes.

Malik subsequently moves in with Fudge. One day, Malik, Fudge, and Phipps have a discussion about race relations. Phipps says that he must play a game of life to win, because society wants no excuses. Kristen and Taryn organize a peace festival to calm and unify the campus after Remys actions.

Meanwhile, Scott chastises Remy for dropping out of school, arguing that skinheads need more educated members. Knocko then shouts out a racist statement just as Fudge, Malik, Dreads, and another black student show up, leading to a fight between the Neo-Nazis and the black students. Deja then confronts Malik about fighting on campus, causing Malik to become more and more angry about racial issues and she confronts him about his plans to drop out of college. Scott says that Remys posturing means nothing and only actions do. He then shows Remy a sniper rifle and challenges him into action.

Malik, who plans to stay in school, and Deja are at the Peace Fest. Remy is on a rooftop with the sniper rifle. The Neo-Nazis attack a gay couple to distract security. An ambivalent Remy opens fire on the crowd below, shooting several students and striking Deja in the stomach. Malik pulls her to safety. Malik gives chase to Remy and beats him bloody in the stairwell, nearly choking him to death before security intervenes. Security begins beating Malik. Security chase down Remy. Remy apologizes to Malik and shoots himself in the mouth. Malik returns and finds that Deja has died and he collapses into Phipps arms, crying.

The Neo-Nazis are shown mourning Remys death when they see a TV news report of the university shooting. Knocko, Erik, and James then begin celebrating his actions while Scott sits back and smiles.

Malik and Phipps discuss a future away from the university, with Phipps saying he trusts Maliks judgment. Later, Malik and Kristen meet at the memorial. Kristen feels guilty about the deaths because she started the festival but Malik reassures her. They shake hands and part. The final montage shows Malik running, Fudge (and the senior class) graduating as the band plays the national anthem and Phipps leaving. He exits under the frame of the American flag and the words UNLEARN are then displayed on the screen.

==Cast==
* Omar Epps as Malik Williams, a track star who struggles with academics
* Kristy Swanson as Kristen Connor, a shy and naive girl
* Michael Rapaport as Remy, a lonely and confused student 
* Ice Cube as Fudge, an afrocentric senior
* Jennifer Connelly as Taryn, a junior and a lesbian 
* Tyra Banks as Deja, Maliks girlfriend and fellow track runner 
* Regina King as Monet, Kristens roommate 
* Jason Wiles as Wayne, Maliks roommate who begins dating Kristen 
* Cole Hauser as Scott Moss, the leader of a racist Neo-Nazi gang
* Busta Rhymes as Dreads, Fudges best friend 
* Laurence Fishburne as Professor Maurice Phipps, a political science teacher at Columbus University 
* Bradford English as Officer Bradley, an officer on the campus 
* Jay R. Ferguson as Billy
* Andrew Bryniarski as Knocko
* Trevor St. John as James
* Talbert Morton as Erik
* Adam Goldberg as David Isaacs, Remys Jewish roommate 
* J. Trevor Edmond as Eddie
* Bridgette Wilson as Nicole
* Kari Wührer as Claudi Colleen Fitzpatrick as Festival Singer
* Morris Chestnut as Track Anchor (Uncredited)
* Gwyneth Paltrow as Student (Uncredited)

==Reception   ==
According to Box Office Mojo, Higher Learning grossed $38,290,723 in the United States, with $20,200,000 in rentals.  It ranked #17 amongst highest grossing R-rated movies in 1995. 

 
Laurence Fishburne won an Image Award, and Ice Cube was nominated an Image Award in 1996.

===Critical response   ===
The film received mixed reviews.   wrote: "a stylish, intelligent film-maker, Singleton interweaves the threads of his demographic tapestry with assurance, passion and a welcome awareness of the complexities of the college communitys contradictory impulses towards integration and separatism." 

Reel Film Reviews wrote, "...Higher Learning is consistently entertaining and well-acted all around. While its not a perfect movie – Cubes character disappears for a 30-minute stretch and Singletons approach often veers into heavy-handedness – it is nevertheless an intriguing look at the differences between races and how such differences can clash." (3.5 stars out of 4) {{cite web
| title = The Films of John Singleton. Higher Learning.| url = http://www.reelfilm.com/sngleton.htm#higher| work = Reel Film| archiveurl = http://web.archive.org/web/20071013161733/http://reelfilm.com/sngleton.htm| archivedate = 2007-10-13}} 

Higher Learning currently holds a 50% rating on Rotten Tomatoes based on 34 reviews.

==Soundtrack   ==
 
The  Higher Learning (soundtrack)|soundtrack, containing hip hop, R&B, rock and jazz music was released on January 3, 1995 by Epic Records. It peaked at #39 on the Billboard 200|Billboard 200 and #9 on the Top R&B/Hip-Hop Albums. 
In addition to "Higher", performed by Ice Cube, the soundtrack includes original music by OutKast, Liz Phair, Tori Amos and Rage Against the Machine.

==References ==
 

== External links ==
*  
*  
*  
* http://boxofficemojo.com/movies/?id=higherlearning.htm

 

 
 
 
 
 
 
 
 
 