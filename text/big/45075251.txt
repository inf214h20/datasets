List of accolades received by Whiplash (2014 film)
 
Whiplash (2014 film)|Whiplash is a 2014 American drama film written and directed by Damien Chazelle, released on October 10, 2014 in the United States.
 , received many awards and nominations for his acting role in the film]]

==Accolades==
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; vertical-align:bottom;"
! colspan="5" style="background:LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award / Film Festival
! Category
! Recipient(s) and nominee(s)
! Result
! class="unsortable" |  }}  
|- style="border-top:2px solid gray;"
|-
| rowspan="4" | 4th AACTA International Awards Best Film
|
|  
| style="text-align:center;" rowspan="4" | 
|- Best Direction
| rowspan="2" | Damien Chazelle
|  
|- Best Screenplay
|  
|- Best Supporting Actor
| J. K. Simmons
|  
|-
| rowspan="5" | 87th Academy Awards Best Picture
|
|  
| style="text-align:center;" rowspan="5" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Adapted Screenplay
| Damien Chazelle
|  
|- Best Film Editing Tom Cross
|  
|- Best Sound Mixing
| Craig Mann   Ben Wilkins   Thomas Curley
|  
|- 12th African-American Film Critics Association Awards
| Best Supporting Actor Gone Girl) 
|  
| style="text-align:center;" rowspan="1" | 
|- 9th Alliance of Women Film Journalists Awards
| Best Supporting Actor
| J. K. Simmons
|  
| style="text-align:center;" rowspan="2" | 
|-
| Best Editing
| rowspan="2" | Tom Cross
|  
|- 65th American Cinema Editors Awards Best Edited Feature Film – Dramatic
|  
| style="text-align:center;" rowspan="1" | 
|- 15th American Film Institute Awards
| Top Ten Movies of the Year
|
|  
| style="text-align:center;" rowspan="1" | 
|- 30th Artios Awards
| Studio or Independent Drama
| Terri Taylor
|  
| style="text-align:center;" rowspan="1" | 
|- 10th Austin Film Critics Association Awards
| Best Film  (Top 10 Films) 
|
|  
| style="text-align:center;" rowspan="2" | 
|-
| Best Supporting Actor
| rowspan="2" | J. K. Simmons
|  
|- 35th Boston Society of Film Critics Awards Best Supporting Actor
|  
| style="text-align:center;" rowspan="1" | 
|-
| rowspan="5" | 68th British Academy Film Awards Best Direction
| rowspan="2" | Damien Chazelle
|  
| style="text-align:center;" rowspan="5" | 
|- Best Original Screenplay
|  
|- Best Actor in a Supporting Role
| J. K. Simmons
|  
|- Best Editing
| Tom Cross
|  
|- Best Sound
| Craig Mann   Ben Wilkins   Thomas Curley
|  
|- 20th Broadcast Film Critics Association Awards Best Picture
|
|  
| style="text-align:center;" rowspan="4" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Original Screenplay
| Damien Chazelle
|  
|- Best Editing
| Tom Cross
|  
|- 10th Calgary International Film Festival
| People’s Choice Award  (Best Narrative Feature) 
| rowspan="2" | Damien Chazelle
|  
| style="text-align:center;" rowspan="1" | 
|- 67th Cannes Film Festival Queer Palm Award
|  
| style="text-align:center;" rowspan="1" | 
|- 27th Chicago Film Critics Association Awards Best Picture
|
|  
| style="text-align:center;" rowspan="5" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|-
| Best Editing
| Tom Cross
|  
|- Best Original Screenplay
| rowspan="2" | Damien Chazelle
|  
|-
| Most Promising Filmmaker
|  
|- 21st Dallas–Fort Worth Film Critics Association Awards
|  ) 
|
|  
| style="text-align:center;" rowspan="2" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- 40th Deauville American Film Festival
| Audience Award
|
|  
| style="text-align:center;" rowspan="2" | 
|-
| Grand Special Prize
|
|  
|- 6th Denver Film Critics Society Awards
| Best Picture
|
|  
| style="text-align:center;" rowspan="3" | 
|-
| Best Supporting Actor
| J. K. Simmons
|  
|-
| Best Original Screenplay
| Damien Chazelle
|  
|- 8th Detroit Film Critics Society Awards
| Best Film
|
|  
| style="text-align:center;" rowspan="5" | 
|-
| Best Director
| Damien Chazelle
|  
|-
| Best Supporting Actor
| J. K. Simmons
|  
|-
| Best Screenplay
| rowspan="2" | Damien Chazelle
|  
|-
| Breakthrough Artist
|  
|- 30th Film Independent Spirit Awards Best Film
|
|  
| style="text-align:center;" rowspan="4" | 
|- Best Director
| Damien Chazelle
|  
|- Best Supporting Male
| J. K. Simmons
|  
|-
| Best Editing
| Tom Cross
|  
|- 19th Florida Film Critics Circle Awards Best Supporting Actor
| J. K. Simmons
|  
| style="text-align:center;" rowspan="2" | 
|-
| Pauline Kael Breakout Award
| Damien Chazelle
|  
|-
| 72nd Golden Globe Awards Best Supporting Actor – Motion Picture
|
|  
| style="text-align:center;" rowspan="1" | 
|- 24th Gotham Independent Film Awards 
| Best Actor
| Miles Teller
|  
|style="text-align:center;" rowspan="1" | 
|- 8th Houston Film Critics Society Awards
| Best Picture
|
|  
| style="text-align:center;" rowspan="4" | 
|-
| Best Director
| Damien Chazelle
|  
|-
| Best Supporting Actor
| J. K. Simmons
|  
|-
| Best Screenplay
| Damien Chazelle
|  
|- 22nd International Film Festival of the Art of Cinematography Camerimage
| Best Directorial Debut
| Damien Chazelle
|  
|  style="text-align:center;" rowspan="1" | 
|-  4th International Online Film Critics Poll
| Best Supporting Actor
| rowspan="2" | J. K. Simmons
|  
| style="text-align:center;" rowspan="1" |   
|- 12th Iowa Film Critics Awards
| Best Supporting Actor
|  
| style="text-align:center;" rowspan="1" | 
|- 35th London Film Critics Circle Awards Film of the Year
|
|  
| style="text-align:center;" rowspan="4" | 
|- Screenwriter of the Year
| Damien Chazelle
|  
|- Supporting Actor of the Year
| J. K. Simmons
|  
|- Technical Achievement of the Year
| Tom Cross
|  
|- 40th Los Angeles Film Critics Association Awards
| Best Supporting Actor
| J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|- 37th Mill Valley Film Festival
| Audience Award  (Best U.S. Feature Film) 
| Damien Chazelle
|  
| style="text-align:center;" rowspan="1" | 
|- 28th Motion Picture Sound Editors Golden Reel Awards
| Feature English Language – Dialogue/ADR
| Craig Mann   Ben Wilkins   Joe Schiff   Lauren Hadaway
|  
| style="text-align:center;" rowspan="2" | 
|-
| Feature Musical
| Craig Mann   Ben Wilkins   Richard Henderson
|  
|- 24th MTV Movie Awards Movie of the Year
| Damien Chazelle
|  
| style="text-align:center;" rowspan="5" | 
|- Best Male Performance
| rowspan="3" | Miles Teller
|  
|- Best Musical Moment
|  
|- Best WTF Moment
|  
|- Best Villain
| rowspan="3" | J. K. Simmons
|  
|- 80th New York Film Critics Circle Awards Best Supporting Actor
|  
| style="text-align:center;" rowspan="1" | 
|- 14th New York Film Critics Online Awards
| Best Supporting Actor
|  
| style="text-align:center;" rowspan="1" | 
|- 18th Online Film Critics Society Awards Best Picture
|
|  
| style="text-align:center;" rowspan="4" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Original Screenplay
| Damian Chazelle
|  
|- Best Editing
| Tom Cross
|  
|- 26th Palm Springs International Film Festival
| Spotlight Award
| J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|- 26th Producers Guild of America Awards Best Theatrical Motion Picture
| Jason Blum   David Lancaster   Helen Estabrook
|  
| style="text-align:center;" rowspan="1" | 
|- 19th San Diego Film Critics Society Awards Best Supporting Actor
| J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|- 13th San Francisco Film Critics Circle Awards Best Picture
|
|  
| style="text-align:center;" rowspan="4" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Original Screenplay
| Damien Chazelle
|  
|-
| Best Editing
| Tom Cross
|  
|- 30th Santa Barbara International Film Festival
| Virtuoso Award
| J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|-
| rowspan="5" | 19th Satellite Awards Best Film
|
|  
| style="text-align:center;" rowspan="5" | 
|- Best Director
| Damien Chazelle
|  
|- Best Actor – Motion Picture
| Miles Teller
|  
|- Best Supporting Actor – Motion Picture
| J. K. Simmons
|  
|- Best Sound (Editing and Mixing)
| Craig Mann   Ben Wilkins   Thomas Curley
|  
|-
| rowspan="3" | 41st Saturn Awards
| Best Independent Film
|
|  
| style="text-align:center;" rowspan="3" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Writing
| Damien Chazelle
|  
|-
| 21st Screen Actors Guild Awards Outstanding Performance by a Male Actor in a Supporting Role
| J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|- 11th St. Louis Gateway Film Critics Association Awards
| Best Arthouse Film
|
|  
| style="text-align:center;" rowspan="5" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Original Screenplay
| Damien Chazelle
|  
|-
| Best Soundtrack
|
|  
|-
| Special Merit (For best scene, cinematic technique or other memorable aspect, or moment)  (Finale Drum Solo) 
|
|  
|- 30th Sundance Film Festival
| Audience Award  (Dramatic) 
| rowspan="3" | Damien Chazelle
|  
| style="text-align:center;" rowspan="2" | 
|-
| Grand Jury Prize  (Dramatic) 
|  
|- 18th Tallinn Black Nights Film Festival
| Best North American Independent Film
|  
| style="text-align:center;" rowspan="1" | 
|- 18th Toronto Film Critics Association Awards Best Supporting Actor
| rowspan="2" | J. K. Simmons
|  
| style="text-align:center;" rowspan="1" | 
|-
| 13th Utah Film Critics Association Awards
| Best Supporting Actor
|  
| style="text-align:center;" rowspan="1" | 
|- 59th Valladolid International Film Festival
| Pilar Miró Award  (Best New Director) 
| rowspan="2" | Damien Chazelle
|  
| style="text-align:center;" rowspan="2" | 
|-
| Golden Spike  (Best Film) 
|  
|- 16th Village Voice Film Poll
| Best Film
|
|  
| style="text-align:center;" rowspan="2" | 
|- Best Supporting Actor
| J. K. Simmons
|  
|- 13th Washington D.C. Area Film Critics Association Awards Best Film
|
|  
| style="text-align:center;" rowspan="5" | 
|- Best Director
| Damien Chazelle
|  
|- Best Supporting Actor
| J. K. Simmons
|  
|- Best Original Screenplay
| Damien Chazelle
|  
|-
| Best Editing
| Tom Cross
|  
|- 67th Writers Guild of America Awards Best Original Screenplay
| Damien Chazelle
|  
| style="text-align:center;" rowspan="1" | 
|}

==References==
 

==External links==
*  at Internet Movie Database

 
 
 