Dancing in the Rain (film)
 
{{Infobox film
| name           = Ples v dežju
| image          =
| image_size     =
| caption        =
| director       = Boštjan Hladnik
| producer       =
| writer         = Dominik Smole, Boštjan Hladnik
| narrator       =
| starring       = Miha Baloh Rado Nakrst Duša Počkaj Ali Rener Jože Zupan
| music          = Bojan Adamič
| cinematography =
| editing        =
| distributor    =
| released       = 27 March 1961
| runtime        = 100 minutes
| country        =   Slovene
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 
 Slovene film directed by Boštjan Hladnik. Its international English title is Dance in the Rain. It is a love drama based on the novel Črni dnevi in beli dan  by Dominik Smole.

Ples v dežju is Hladniks first feature film after he returned from Paris, where he worked with Claude Chabrol, Robert Siodmak & Philippe de Broca. It is considered the first Slovenian film noir. 

On the occasion of the hundredth anniversary of Slovene film in 2005, Slovene film critics of various generations chose Ples v dežju as the best Slovene film. The Film Fund of the Republic of Slovenia thus funded its  , excerpts from the television show Povečava ("Magnification", 1991) directed by Franci Slak, and excerpts from the documentary portrait of the actor Miha Baloh (1998) directed by Slavko Hren. Apart from Slovene, it has subtitles in six languages: English, German, French, Italian, Serbian (Cyrillic) and Croatian. 

==Plot summary==
A love drama of Maruša, an actress and Peter, a painter. He lives in a dark tiny rented room and she lives in a small flat. They both spend most of their time in a pub where they meet. Their relationship is full of uncertainties and contrasts. Peter dreams of perfect beauty and Maruša yearns for lost youth. A bright window to the future shown throughout the film seems to be
moving further and further away.

==External links==
* 

==References==
 

 
 
 
 

 