Galaxina
 
{{Infobox film
| name           = Galaxina
| image          = Galaxina ver1.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = William Sachs
| producer       = Marilyn Jacobs Tenser
| writer         = William Sachs
| starring       = Stephen Macht Avery Schreiber James David Hinton Dorothy Stratten
| music          =
| cinematography = Dean Cundey
| editing        = George Berndt, George Bowers
| distributor    = Crown International Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         =
| gross          =
}}
 American comedy Playmate of Western movies. It won the Audience Award at the 1983 Brussels International Festival of Fantasy Film. 

A film viewed by characters within the film is a clip from the 1960 Eastern bloc movie First Spaceship on Venus.  The clip is used in the film because it was also a Crown International Picture.

==Plot== android servant, and Rock-Eater, a rock-eating alien prisoner confined to the brig.
 laser fire but the bird-ship gets away. After the encounter, Galaxina serves a dinner of chicken-flavored food pills to Capt. Butt, Thor and Buzz. The three men are stunned by her beauty, and Buzz receives an electric shock when he touches her buttocks. Tired of the pill-food, Capt. Butt decides to eat an alien egg confiscated from a prisoner. The egg sickens him, and he coughs up a baby alien creature that quickly scurries away.

Later, the crew receive orders to proceed to the prison planet Altair One to recover a priceless stolen gemstone called the Blue Star; every time the stone is mentioned, an invisible heavenly chorus is heard by the characters. The trip will take the Infinity 27 years to complete, requiring that the crew enter cryogenic sleep.  Before doing so, they make a quick stop at an asteroid brothel. 

Galaxina remains in charge of the ship while the crew are in stasis (fiction)|stasis.  While alone, she reprograms herself to become more human.  She learns to talk and disables her electrical defense mechanism. She visits Thors sleep chamber periodically, embracing it and telling the sleeping Thor that she loves him. Later, the baby alien visits Butts chamber and tampers with the controls. When the crew awaken at their destination, Butt emerges from his pod an old man with shaggy gray hair. 

Thor is seduced by Galaxina and he falls in love with her. Although she lacks the proper equipment to have sex, she assures Thor that these options can be ordered from the android catalog. Thor can only fantasize about Galaxina until they return home and get her upgrades.

The ship reaches Altair One and lands. Knowing that the local aliens are hostile to humans, Galaxina volunteers to go look for the Blue Star while the others stay on the ship. She walks into town and enters a "human restaurant", and discovers that this means the restaurant serves humans as food to alien creatures. There she finds Ordric, the masked figure the crew encountered earlier.  Ordric possesses the Blue Star, and Galaxina attacks him. Galaxina discovers Ordric is a robot when she smashes his head open. Ordric is deactivated and Galaxina takes the Star. 
 gang of bikers, descendants of the first settlers of Altair One. Their leader announces that he will sacrifice Galaxina to their deity "Harley Davidson|Harley-David-Son" and with the power of the Blue Star, he will take control of the universe. 

Thor and Buzz, who have been looking for Galaxina, rescue her from the bikers and return to the ship. Ordric attacks and boards the Infinity as soon as they reach space. He takes back the Blue Star and confines everyone in the brig. The baby alien, now fully grown, sneaks onto the bridge and attacks Ordric. The creature, believing Butt to be its mother, goes to the brig and gives Butt the keys to the cell door. 

The crew escapes the brig and rushes the bridge, finding that Ordric has been torn to pieces. While contemplating the reward they will receive for returning the Blue Star, they notice that Rock-Eater has eaten it.

==Cast==
  
* Stephen Macht as Sgt. Thor
* Avery Schreiber as Captain Cornelius Butt  
* James David Hinton as Buzz
* Dorothy Stratten as Galaxina  
* Lionel Mark Smith as Maurice  
* Tad Horino as Sam Wo
* Ronald J. Knight as Ordric 
 
* Percy Rodrigues as Ordrics voice
* Herb Kaplowitz as Rock Eater / Kitty / Ugly Alien Woman
* Nancy McCauley as Elexia
* Fred D. Scott as Commander Garrity 
* George Mather as Horn Man 
* Susan Kiger as Blue Girl
* Rhonda Shear as Mime / Robot
 

==Notes==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 