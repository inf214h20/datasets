Rokk í Reykjavík
{{Infobox film
| name        = Rokk í Reykjavík
| image       = Rokk í Reykjavík (VHS front cover).jpg
| director    = Friðrik Þór Friðriksson
| producer    = Hugrenningur
| writer      =
| starring    = Several Icelandic bands
| distributor = Íslenska kvikmyndasamsteypan
| released    =  
| runtime     = 83 min
| country     = Iceland Icelandic
| music       = Various
| budget      =
}}

Rokk í Reykjavík ( ) is a documentary directed by Icelandic Friðrik Þór Friðriksson during the Icelandic winter of 1981-1982 and released for the local television the same year.  new wave most important bands at that time taken from different concerts and accompanied by, some times, short interviews with musicians, and it portrays the lifestyle of the Icelandic youth faced to the establishment and advocated to anarchy, who were trying to find their own identity.

Rokk í Reykjavík is today considered as one of the most important documentaries about the Icelandic music culture and it included several important bands. For instance,  , today considered as one of the legendary Iceland bands of the early eighties, is featured here with their songs “Rúdolf” and “Killer Boogie”. 
It is also worth of mentioning, the presence of Einar Örn Benediktsson’s punk group Purrkur Pillnikk, which appeared with two tracks: “Ovænt” and “Gluggagægir”. 
Other important artists featured here are Bubbi Morthens with his band Egó, Fræbbblarnir, Grýlurnar, and the renowned Sveinbjörn Beinteinsson with his chanting poem “Rímur”, among others.

As this film was the first Icelandic work in Dolby Stereo, it brought innovation to the Icelandic film industry. With Íslenska kvikmyndasamsteypan as the distributor, Rokk í Reykjavík was released in VHS format. The soundtrack to this film was released as a double LP compilation by Hugrenningur in April 1982.

The image cover for this release depicts singer Björk from (at that moment) Tappi Tíkarrass on stage.

In July 2008 the movie was released on DVD by Sena (Iceland only)  .

==Songs==
{|  class="toccolours" border="1" cellpadding="4" style="border-collapse:collapse"
|- style="background-color:#e9e9e9" |
! Track !! Title !! Length !! Artist
|-
| 01 || Ó Reykjavík || 02:25 || Vonbrigði
|-
| 02 || Sieg Heil || 01:10 || Egó
|-
| 03 || Gotta Go || 01:45 || Fræbbblarnir
|-
| 04 || Óvænt || 01:07 || Purrkur Pillnikk
|-
| 05 || Rúdolf || 02:49 || Þeyr
|-
| 06 || Creeps || 01:48 || Q4U
|-
| 07 || Breyttir Tímar || 02:20 || Egó
|-
| 08 || Where are the Bodies || 05:08 || Bodies
|-
| 09 || Hrollur || 02:25 || Tappi Tíkarrass
|-
| 10 || Moving Up to a Motion || 03:15 || Baraflokkurinn
|-
| 11 || Talandi Höfuð || 02:55 || Spilafífl
|-
| 12 || Í Speglinum || 03:25 || Þursaflokkurinn
|-
| 13 || Í Kirkju || 02:42 || Friðryk
|-
| 14 || Lífið og Tilveran || 03:25 || Start
|-
| 15 || Gullúrið || 03:10 || Grýlurnar
|-
| 16 || Sat ég Inni á Kleppi || 03:42 || Egó
|-
| 17 || Gluggagægir || 03:00 || Purrkur Pillnikk
|-
| 18 || Dúkkulísur || 02:40 || Tappi Tíkarrass
|-
| 19 || Bereft || 03:18 || Mogo Homo
|-
| 20 || Hver er svo sekur? || 02:35 || Jonee Jonee
|-
| 21 || Killer Boogie || 02:45 || Þeyr
|-
| 22 || Kick Us Out of the Country || 01:55 || Bodies
|-
| 23 || Af Því Pabbi Vildi Það || 01:43 || Jonee Jonee
|-
| 24 || Í Nótt || 01:48 || Fræbbblarnir
|-
| 25 || Guðfræði || 03:00 || Vonbrigði
|-
| 26 || Stórir Strákar || 02:40 || Egó
|-
| 27 || Gonna Get You || 01:26 || Q4U
|-
| 28 || Toys || 01:57 || Q4U
|-
| 29 || Lollipops || 02:50 || Sjálfsfróun
|-
| 30 || Antichrist || 01:10 || Sjálfsfróun
|-
| 31 || Sjálfsfróun || 01:30 || Sjálfsfróun
|-
| 32 || Af Litlum Neista Verður Mikið Mál || 02:30 || Bruni BB
|-
| 33 || Rímur || 02:00 || Sveinbjörn Beinteinsson
|}

==Credits==

===Personnel===
Direction: Friðrik Þór Friðriksson. 
Cinematography and lightning: Ari Kristinsson. 
Other shooting personnel: Árni Páll Jóhannsson, Baldur Hrafnkell Jónsson, Friðrik Þór Friðriksson, Jón Karl Helgason, Magnús Magnússon, Peter Auspin, Richard Crowe, Sigurður Grímsson, Sigurður Snæberg Jónsson, Sigurjón Sighvatsson, Vilhjálmur Knudsen and Þorgeir Gunnarsson. 
Recording engineers from Þursabit recording studio: Júlíus Agnarsson, Tómas Magnús Tómasson, Þórður Árnason. 
Sound: Jón Karl Helgason. 
Edition: Ari Kristinsson, Kristín Pálsdóttir, Peter Auspin, Richard Crowe, Sigurður Grímsson, Sigurður Jón Ólafsson, Sigurður Snæberg Jónsson. 
Executive board: Þorgeir Gunnarsson. 
Mixing: Alan Snelling, Þórður Árnason. 
Sound production: Anvil Studios, Abbey Road, London. 
Video production: Johan Ankerstjerne a/s, Kaupmannahöfn. 
Distributor: Íslenska kvikmyndasamsteypan.
 Bad Taste.

==See also== Rokk í Reykjavík (Hugrenningur), the soundtrack.

*2005 Screaming Masterpiece documentary about Icelands music scene.

==External links==
*  

 

 
 
 
 
 