65 Redroses
{{Infobox film
| name           = 65_Redroses   
| image          = 65_redroses.jpg
| director       = Philip Lyall  Nimisha Mukerji  John Ritchie
| writer         = Nettie Wild Mike Rae
| starring       = Eva Markvoort
| music          = Adam Locke-Norton
| editing        = Justin Cousineau   Philip Lyall  Nimisha Mukerji
| studio         = Force Four Entertainment   in association with    CBC Newsworld   Dualogue Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
}}  lung transplant while blogging about her experiences.   

==Production==
The film began when Philip Lyall, a long-time friend of Markvoort, introduced her to his University of British Columbia film school partner Nimisha Mukerji. Lyall and Mukerji had been looking for a post-graduation project and decided to chronicle Markvoort’s wait for a double-lung transplant. They named the film 65_Redroses after Evas online identity, which she had chosen because, according to the Canadian cystic fibrosis community, "sixty-five roses" is how many young children with the disease mispronounce “cystic fibrosis”.    Eva added red because it was her favourite colour.   

When Lyall and Mukerji began shooting, Markvoort’s lungs were so clogged doctors said that without a transplant, she would not live to 2009.  She was getting ready to visit a pumpkin patch the week before Halloween in 2007, when her pager  went off, a signal to call the hospital transplant center.  A pair of lungs was available for transplant. This sequence, emotional for both Markvoort and the filmmakers, was instrumental in attracting the attention and backing of the Canadian Broadcasting Corporation. 

==Release==
65_Redroses premiered at the Hot Docs Canadian International Documentary Festival and won three awards at the Vancouver International Film Festival, including most popular Canadian film and documentary. It debuted on television on CBCs The Passionate Eye series and was acquired by the Public Broadcasting Service for international distribution. 
 Oprah Winfrey Network and would premiere in the United States in early 2011. 

Hello Cool World  is the distributor of the film in Canada.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 