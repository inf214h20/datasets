Hit and Run (2009 film)
 
{{Infobox film
| name = Hit and Run
| image = Hit and Run(2009)poster .jpg
| caption = Hit and Run poster
| director = Enda McCallion
| producer = Mark Morgan Andrew Weiner Braxton Pope Brent Emery Scott Reed Ron Singer Guy Oseary Benjamin Sitzer
| writer = Diane Doniol-Valcroze Arthur Flam
| starring = Laura Breckenridge Kevin Corrigan Christopher Shand
| cinematography = Olivier Cocaul
| editing = Miklos Wright
| music = Matt Messina
| distributor = MGM 20th Century Fox Home Entertainment
| released =  
| runtime = 84 minutes
| country = United States
| language = English
}}
Hit and Run (also known as Bumper or Hit and Run unrated) is a 2009 horror film from MGM, released only in an unrated version. The film tells the story of a young woman who tries to cover up a deadly hit and run accident, only to have the supposedly dead victim come back to terrorize her.

==Plot==
The film opens as Mary Murdock (Laura Breckenridge), a young student, leaves a lively party at a club in New Jersey one night. She hits a bad bump driving home drunk on a dark road. Later, hearing noises in the garage, she finds a bleeding man, mangled and impaled on her jeeps bumper. Not calling 911, she tries to help, but he suddenly attacks. Panicked, she hits him with a golf club. After that, she buries his body in a shallow grave in the woods off Clover Rd.

The next day, Mary covers up the crime (before parents return from a weekend trip); she scrubs off the blood but doesnt get the dent fixed (a suspicious cop at the auto shop scares her off, then she detours for her grandmas parrots at airport cargo). Mary starts coming apart. Irrational, she crashes on a tree to camouflage the small dent with a bigger dent.

Later the news reveals the missing man is kindergarten teacher Timothy Emser (Kevin Corrigan), bipolar and unstable when he vanished. Strange things occur in Marys house, making her feel toyed with by someone unseen; she descends into paranoia. It transpires that Rick (Christopher Shand), her boyfriend, gets involved in the cover up and goes to retrieve incriminatory evidence (a blanket) off the corpse, but is killed, replaced in Emsers grave.

It becomes clear Emser survived. Unhinged, he returns to make Mary live through the same nightmare she inflicted on him. He stalks Mary many ways. The parrots squawk strange phrases, which alert Mary. At one point, she falls down the stairs, impaling a screw driver in her thigh. Emser surprise attacks, biting and stabbing her, and she passes out. In a reversal, when Mary revives, Emser has strapped her to the bumper with electric cords and Christmas lights and takes her on a sadistic, all-night "revenge drive".

Along the way, after a struggle, Emser kills a gas attendant (who was about to call the cops) by pumping petrol down his throat and the attendant throws up blood after Emser departs. Emser parks back at his own house, leaving Mary trapped in his garage, as she did to him. He reunites with his worried family, but is ever more delusional, violent. Soon, Emser stabs his wife Jane in the back with hedge clippers when she accidentally stumbles on and tries to untie Mary.

It transpires that Emser goes to bury Mary in the woods with Ricks body, after detaching her and the bumper using a blowtorch and welding mask. However, in the grave, Mary is able to maim his eye with a plug from the cords and escapes in the jeep. When Emser blocks her path, Mary revs up and intentionally runs her tormentor over several times. She speeds off in the dark.

Next morning, Mary wakes on the roadside, numb and battered, and drives to a local auto shop. There, the mechanic and passersby ultimately discover, and pull out, Emsers body from under the jeep, where it got snagged. Seeing this, Mary has a mental breakdown, saying "I dont think I need that bumper anymore", laughing as the police sirens close in on her. The film ends moving closer to Emsers bloodied face on the pavement in the early rain, and it appears to be left uncertain whether he is actually dead or not.

Throughout the narrative, a radio DJ called Eddie the Spaz is periodically heard, hosting a weekend music marathon, the "Spazathon", which bookends the film.

==Cast==
* Laura Breckenridge as Mary Murdock
* Kevin Corrigan as Timothy Emser
* Christopher Shand as Rick
* Megan Anderson as Jane
* Michael Gell as Andy
* Nitin Adsul as Gas Attendant
* Eric Zuckerman as First Street Mechanic 
* Joe Hansard as Quarantine Dude
* Lance Lewman as Mechanic
* Rebecca Gebhard as News Anchor
* Al Twanmo as Newscaster
* Kara Quick as Reporter #1
* Katherine Schmoke as Reporter #2
* Mary Lechter as Teacher
* Robert Cait as Eddie the Spaz

* Larry Carter as Bar Patron (uncredited)
* Michael Cohen as Bar Patron (uncredited)
* Mike Dusi as Motorcycle Cop (uncredited)
* Blaze Foster as Making out couple outside bar (uncredited)
* Kiersten Hall as Shot Girl (uncredited)
* Jason Moffett as Mechanic (uncredited)
* Kevin E. Scott as Bar Patron (uncredited)
* Katelyn Linnell as Bar Patron, Stand-in for Laura Breckenridge (uncredited)

==Background== Chante Mallard of Fort Worth. 

==Release==
 to DVD on January 13, 2009 in North America (USA & Canada); February 9, 2009 in the UK; June 17, 2009 in Argentina and Finland; October 1, 2009 in Germany; February 3, 2010 in Spain; February 10, 2010 in Australia; April 30, 2010 in Belgium; and May 26, 2010 in the Netherlands.

==Soundtrack==
Songs in the film include:

*"Roller Coaster" (Rafelson) Performed by Erika Jayne Float On" (Gallucci, Judy, Brock) Performed by Modest Mouse
*"Noise" (Frost, Coates, Murphy) Performed by Bangkok 5
*"90 Miles An Hour" (Costner, Morgan, Chisolm) Performed by Kevin Costner and Modern West

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 