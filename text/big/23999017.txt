The Legacy (2009 film)
{{Infobox film
| name           = The Legacy
| image          = 20090604 donation actu-copie-1.jpg
| caption        = 
| director       = Bernard Émond
| producer       = Bernadette Payeur Marc Daigle
| writer         = Bernard Émond
| starring       = Élise Guilbault Jacques Godin
| music          = Robert Lepage
| cinematography = Sara Mishara
| editing        = Louise Côté
| distributor    = Seville Pictures
| released       =  
| runtime        = 96 minutes
| country        = Canada
| language       = French
| budget         = $CAN 4.2 million
| gross          = 
}} 2009 film directed by Bernard Émond. The film received the Special Grand Prize of Youth Jury and the Don Quixote Award of the Locarno International Film Festival.   It will also compete at the Toronto International Film Festival in September 2009. 

==Synopsis==
The film is the third of a trilogy started with La Neuvaine (The Novena) in 2005 and Contre toute espérance (Summit Circle) in 2007, all directed by Bernard Émond.
 Abitibi named Normétal, Quebec|Normétal to replace him for a few weeks, with no plans for an extended stay. When Dr. Rainville suddenly dies, Jeanne must decide if she’ll take over the job, and its inherent responsibilities, for the long-term.

The film also stars Eric Hoziel, Sylvain Marcel, Angèle Coutu, Michel Daigle and Danielle Filtault.

== See also ==
* Health care in Canada 
* Faith healing
* Secular theology

==References==
 

== External links ==
*  
*  
*   from OutNow!

 
 
 
 
 
 


 