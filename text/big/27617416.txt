Kaadhal Solla Vandhen
{{Infobox film
| name           = Kadhaal Solla Vandhen 
காதல் சொல்ல வந்தேன்
| image          = Kaadhal_Solla_Vandhen.jpg
| caption        = 
| director       = Boopathy Pandian
| producer       = S. Jayakumar Meena Jayakumar
| writer         = Boopathy Pandian
| starring       = Balaji Balakrishan Meghana Raj Karthik Sabesh
| music          = Yuvan Shankar Raja
| cinematography = Raana
| editing        = Praveen K. L. N. B. Srikanth
| studio         = S3 Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    =
}} Arya in a guest role.    The film, previously titled as Naanum En Sandhyavum and Naan Avalai Sandhitha Pozhudhu, is produced by S3 Films and features music scored by Yuvan Shankar Raja. It released on 13 August 2010 to mixed reviews and proved to be highly unsuccessful at the box office.     

== Cast == Arya as a doctor (Guest appearance)
* Balaji Balakrishnan as Nanu Prabhu
* Meghana Raj as Sandhya Panchacharam
* Karthik Sabesh as Petha Perumal
* R. Sundarrajan as Ashwin as Aravind Singh

== Production == Vedhika were considered.   Yuvan Shankar Raja was announced as the music director for film.  Following the announcement, however, there were no more news or any further details disclosed in the media and the film got shelved.

In early 2010 then, reports claimed that the director had completed a film titled Naan Avalai Sandhitha Pozhudhu (When I met her), starring newcomers and featuring Yuvan Shankar Rajas music.  Boopathy Pandian had restarted the projects, changing its title and replacing the lead couple by two relatively unknown artists;  Balaji Balakrishnan, who starred in the popular STAR Vijay television series Kana Kaanum Kaalangal and also appeared in the N. Linguswamy-produced 2009 film Pattalam (2009 film)|Pattalam, was signed as the male protagonist, whilst Meghana Raj, daughter of actors Sunderraj and Pramila,  who was also earlier signed by Kailasam Balachander|K. Balachander for his long-delayed production venture Krishnaleelai, was roped in to enact the role of Sandhya.   The title of the film was later changed again to Kaadhal Solla Vandhen as disclosed by Balaji on his Facebook site. The films shooting was primarily carried on in and around Perambalur, with major portions being filmed at the Dhanalakshmi Srinivasan Engineering College.

== Soundtrack ==
{{Infobox album|  
| Name = Kaadhal Solla Vandhen
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover =
| Released = 16 June 2010
| Recorded = 2010 Feature film soundtrack
| Length = 23:02
| Label = Think Music
| Producer = Yuvan Shankar Raja
| Reviews =  
| Last album = Baana Kaathadi (2010)
| This album = Kaadhal Solla Vandhen (2010)
| Next album = Thillalangadi (2010)
}}
 solo numbers Hindu priests (poosaris). The film did feature another song as part of the film score that was not included in the soundtrack.

{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 23:02
| all_lyrics      = Na. Muthukumar, except where noted
| title1          = Oh Shala
| lyrics1         = Saradhi
| extra1          = Yuvan Shankar Raja
| note1           = Saradhi
| length1         = 4:16
| title2          = Oru Vaanavillin Pakkathilae
| lyrics2         = Na. Muthukumar
| extra2          = Udit Narayan
| length2         = 4:48
| title3          = Enna Enna Aagiraen
| lyrics3         = Na. Muthukumar
| extra3          = Vijay Yesudas
| length3         = 4:38
| title4          = Saamy Varugudhu
| lyrics4         = Na. Muthukumar
| extra4          = Chidambaram Sivakumar Poosari & Chorus
| length4         = 3:38
| title5          = Anbulla Sandhya
| lyrics5         = Na. Muthukumar Karthik
| length5         = 5:42
}}

;Not in the soundtrack
Other music featured in the film include:

# Yedho Ondru Unnai, sung by Haricharan, composed by Yuvan Shankar Raja

== References ==
 
www

== External links ==
*  

 
 
 
 
 
 