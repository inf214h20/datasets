Duane Hopwood
{{Infobox Film
| name           = Duane Hopwood
| director       = Matt Mulhern
| producer       = David T. Friendly Melissa Marr Lemore Syvan Marc Turtletaub
| writer         = Matt Mulhern
| narrator       = 
| starring       = David Schwimmer Janeane Garofalo Judah Friedlander Dick Cavett Steve Schirripa John Krasinski
| music          = Michael Rohatyn
| cinematography = Mauricio Rubinstein
| editing        = Tom McArdle
| distributor    = IFC Films (USA)
| released       = January 20, 2005 (Sundance Film Festival) November 11, 2005 (limited)
| runtime        = 84 min
| country        = United States English
| budget         = 1 million (estimated)
| preceded_by    = 
| followed_by    = 
}}

Duane Hopwood is a 2005 film featured in the Sundance Film Festival starring David Schwimmer and Janeane Garofalo, with a supporting cast featuring Judah Friedlander, Dick Cavett, Susan Lynch, John Krasinski,  Rachel Covey, and Mia Dillon. The movie was written and directed by Matt Mulhern and released by IFC Films in November 2005.

The films plot centers on the titular character (Schwimmer), an alcoholic whose life is spiraling downward rapidly after his divorce from Linda (Garofalo).
 Kansas City. Showtime and The Movie Channel began the cable run in December 2006.

==Plot==
Duane Hopwood (David Schwimmer) is a father sharing custody of his two young daughters with his ex-wife Linda (Janeane Garofalo). 
 Caesars Palace casino in Atlantic City, New Jersey. The montage culminates when Duane is pulled over after continually swerving across the road. Duane is drunk, but the policeman, a close friend, offers to take him home. Duane agrees but his friends leniency changes after Duane tells him his daughter is in the back seat.

Duane is banned from driving and is forced to travel to the casino by a bicycle borrowed from his ex-wife Linda. Luckily, Duanes colleague, Anthony (Judah Friedlander), sees him and offers him a ride. At the casino Duane is forced to intervene in an altercation between a blackjack table dealer and a customer. Duane initially asks him to leave, but after the man asks for a second chance, Duane gives him a few quarters and asks him to play a few slots then leave. The man does so and wins the jackpot prize.

Duane meets his ex-wife Linda at the pier and she tells him that his oldest daughter, Mary, would like to talk to him and also informs him that her lawyer has suggested she revoke his visitation rights because of his DUI. In anger he chucks the bicycle into the sea.

Needing to call a cab, Duane returns to the casino, but gets asked to take a walk by his boss. Duane is taken into his bosss office to explain his part in a dispute between the slot machine winner and an old lady who says that it was "her machine". Duane confirms, falsely, that it was the mans own quarter which was used to win the jackpot. Duanes boss explains that the dispute is a "potential public relations nightmare".

At home, Duane is awoken by his neighbors posting a Thanksgiving invitation through his mailbox. Duane opens the door and receives the invitation personally, but is reluctant to confirm his attendance, saying he may be working on Thanksgiving, which his neighbor finds bemusing.

Duane takes his daughter on an impromptu bicycle ride to try and asks her what it was she wanted to talk to him about. She tells him her feelings about "jogging Bob", Lindas new fitness fanatic boyfriend, and says she wants to live with Duane because her sister called her fat and Bob (John Krasinski) was encouraging her to go running and lose weight.

Returning from a meal with Linda Bob and his two daughters, Duane explains what Mary said to him about her worries and goes on to confront Bob about his role in his daughters unhappiness. The confrontation escalates and Duane picks up a baseball bat and hits it against the veranda railing. Just as Duane moves towards Bob, his two daughters appear at the door, which causes Duane to stop in his tracks and eventually leave.

After work, Duane stops by his usual bar a begins to talk to the barmaid, Gina (Susan Lynch). She gives the now drunk Duane a lift home but her car will not restart and she is forced to stay the night at Duanes. Rejecting his drunken advances she sleeps in his bed while he sleeps on the sofa.

Duane is called to his bosss office and is shown a tape of him giving the quarters to the jackpot winner. Duanes boss, under pressure from his own boss, is forced to fire Duane. The now jobless Duane is seen drinking for the rest of the day. It jumps to Duane and Gina lying in bed after sex, where Duane tells her that he still loves Linda, causing Gina to leave.

Duane attends the court hearing on his visitation rights and explains to the judge the importance of him being able to see his kids. After this, Lindas lawyer produces the bat that Duane threatened Bob with previously. It then cuts to an impatient Duane and his lawyer waiting outside for the verdict. Duane goes to go talk with Linda but his lawyer convinces him not to.

He cycles home to find Anthony sitting in a turkey costume, a prop hes using for that nights stand-up performance.

Duanes lawyer, standing at his door, after a pause simply says "we need to talk" indicating the negative outcome of the court decision. This causes Duane to immediately rush upstairs, get his coat, and run to his car. Duane drives to Lindas house but finds the place empty. Distraught and drunk, Duane appears at Anthonys stand-up gig at the casino and proceeds to ruin it with his drunken behavior, before finally being restrained and then taken to the ground by the casino security guard, his former colleague.

The next morning. Duane goes downstairs and apologizes to Anthony for screwing up his big break. Linda surprisingly knocks and explains that she would never leave without saying goodbye, and that she had to move the furniture early. Linda explains that she, Bob and the two girls are moving to South Carolina, where Bob has a job co-running a gym with his brother. Duane accepts responsibility for the breakup of the family. Linda asks him to come and say goodbye before they move and promises to arrange for him to visit the children. The two reconcile and part amicably.

Linda, Bob and the two girls wait at the house for a time before giving up and drive off, only for Duane to appear cycling next to the car in the turkey costume. Hes able to give his daughters a good send off and watches them leave content. He enters his usual bar to the pleasant surprise of Gina. Duane, having finally moved on from Linda, is able to start a new relationship with Gina.

Duane and Anthony go to their neighbors thanksgiving meal and Gina also arrives. The film ends with a scene of a toast, with everyone then drinking wine, except Duane, who drinks water.

==External links==
* 
*  

 
 
 
 
 