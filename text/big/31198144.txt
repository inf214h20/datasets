Kabani Nadi Chuvannappol
{{Infobox film
| name           = Kabani Nadi Chuvannappol
| image          = Kabani Nadi Chuvannappol.jpg
| image size     = 
| alt            = 
| caption        = A screenshot from the film
| director       = P. A. Backer
| producer       = Pavithran
| writer         = P. A. Backer
| starring       = T. V. Chandran Raveendran J. Siddiqui Shalini
| music          = Devarajan
| cinematography = Vipindas
| editing        = Kalyanasundaram
| studio         = Saga Movie Makers
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Emergency period. Best Director Second Best Film at the Kerala State Film Awards.  Pavithran, who later directed many critically acclaimed Malayalam films produced the film.  T. V. Chandran, who also later went on to direct a bevy of award winning films in Malayalam and Tamil, played the lead role. {{cite web|url=http://www.cscsarchive.org:8081/MediaArchive/art.nsf/(docid)/99955C937985A13A6525694000620189|title=Soul on Fire  
|work=The Indian Express|publisher=cscsarchive.org|date=April 19, 1998|accessdate=March 16, 2011}}  {{cite web|url=http://www.cinemaofmalayalam.net/chandran.html|title=T.V.Chandran  
|publisher=Cinemaofmalayalam.net|accessdate=March 16, 2011}}  After certain post-production controversies, the film debuted in theatres on 16 July 1976.

==Plot==
The film is a love story between a young woman (Shalini) and a radical political activist (T. V. Chandran), who is declared to be Naxalite. The film ends with the police killing him and the woman learning about his death through the newspaper. {{cite web|url=http://www.cinemaofmalayalam.net/backer.html|title=P.A.Backer  
|publisher=Cinemaofmalayalam.net|accessdate=March 16, 2011}} 

==Cast== 
  
*TV Chandran 
*Salam Karassery
*J Sidhiq
*Laila
*Miss Don 
*Pailunni 
*Salini 
*K. Ravindran 
 

==Production and release== Emergency was declared in India. Strict warnings by the Government against any act that supported extremist activity left P. A. Backer, in two minds for the film had a Naxalite as its hero. Both Backer and Chandran sought Pavithrans opinion who had said, "Lets go ahead.".  

The film was screened at several film festivals in 1975. It was not given the censor certificate for the theme it dealt with for more than a year. It released in theatres during the Emergency period itself, on 16 July 1976. 

The English title of the film is When the River Kabani Turned Red. 

==Awards==
; Kerala State Film Awards  Second Best Film - P. A. Backer (director), Pavithran (producer) Best Director - P. A. Backer

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 
 
 