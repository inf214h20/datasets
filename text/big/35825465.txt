Caldera (film)
{{Infobox film
| name = Caldera
| image = Caldera_(film)_official_poster_artwork.jpg
| caption = Official poster
| director = Evan Viera
| writer = Chris Bishop Evan Viera
| producer = Chris Perry Evan Viera
| music = Evan Viera
| studio = Orchid Animation Bit Films Flicker Dreams Productions
| released =  
| runtime = 11 minutes
| country = United States
| language = English
}} computer animated short film released in 2012. It was directed by Evan Viera, co-written by Chris Bishop, co-produced by Chris Perry, and created in conjunction with Bit Films, the computer animation incubator program at Hampshire College in Amherst, Massachusetts.  

Caldera received a Prix Ars Electronica Award of Distinction in the Computer Animation category in 2012.

== Plot ==
Caldera is about a young girl who goes off her medication and leaves a bleak metropolis to immerse herself in a vibrant oceanic cove. Ultimately, the story is about the young girl’s impossible predicament, where she can not live in either the fantastical and haunting world of psychosis or in the marginalizing society that mandates her medication.

==Awards==
;2012
* Prix Ars Electronica – Award of Distinction (Computer Animation) 
* Seattle International Film Festival - Award of Innovation 
* Rome Independent Film Festival - Best Short Animation 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 