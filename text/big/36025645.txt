Vetri Selvan
{{Infobox film
| name           = Vetri Selvan
| image          = 
| alt            =  
| caption        = 
| director       = Rudhran
| producer       = 
| writer         = Rudhran
| starring       =  
| music          = Mani Sharma
| cinematography = Ramesh Kumar
| editing        = Kishore Te.
| studio         = Silicon studios &Srushti Cinemas
| distributor    = Silicon Studios
| released       =  
| runtime        = 138 mins
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil thriller drama film directed by Rudhran   featuring Ajmal Ameer and Radhika Apte in the lead roles.  Produced on Srushti Cinemas banner&Silicon Studios. The film has screening in 125 theatres. the film has singer Mano (singer)|Mano, Sherrif and Ganja Karuppu in pivotal roles.  It was the last film of child actress Taruni Sachdev. The original music and background score of the film were composed by Mani Sharma, cinematography was handled by Ramesh Kumar, while editing was by Kishore Te.  Vetri Selvan revolves around three youth who have been rejected by the society and how they try to reform it.  It was released on 19 June 2014. 

==Cast==
* Ajmal Ameer as Vetri Selvan
* Radhika Apte as Sujatha Mano as Ananthakrishnan
* Sherrif as Ganesh
* Ganja Karuppu
* Dinesh Lamba
* Thalaivasal Vijay as Bhasyam Aarthi as Aarthi
* Azhagam Perumal
* Swarna Thomas
* Taruni Sachdev as Abhi
* Sanjana Singh
* Karate Raja
* Ramdoss

==Production== Vijay in mind to play the lead role. Ajmal was signed for the lead role, that of a student, and singer Mano was reported to play a "prominent character", that of a professor in the film, acting in a film again after Singaravelan; Sherif, winner of a reality show, was given the role of a dancer. 

During the shooting, Ajmal met with an accident during the shoot of Vetri Selvan. Ajmal and the film’s lead actress Radhika Apte were travelling in an auto on the hills of Ooty as part of the film, when the auto rammed on a median and fell stumbling down on the road. The shoot came to a halt for a couple of hours giving time for Ajmal to recuperate. The director had to find another auto similar to the previous one to continue with the shooting as the initial one was heavily damaged during the accident.  Child actress Taruni Sachdev, who had played a supporting role as Aptes sister in the film, died in a plane crash in May 2012. She had completed most of her part however, with Rudhran stating that her footages would be retained and "as a remembrance of the prodigy" and that the rest of her part would be patched up during the post-production. 

==Soundtrack== Bala on Ameer and Seenu Ramasamy, producer G. Dhananjayan and writer Tamilachi Thangapandian were also present.  All songs were written by Madhan Karky. Behindwoods wrote:"all the album feels like vintage Mani Sharma, but somehow that can be a double edge sword as it does have a dated feel amidst the more trendy styles that the audience subscribes to today". 

* Megathile - Shreya Ghoshal Ranjith
* Enna Enna - Haricharan, Mahathi
* Vittu Vittu - Rahul Nambiar, Bhargavi Pillai
* Deivatha Pole - Hema Chandran

==Critical reception==
Baradwaj Rangan wrote, "A filmmaker sees something horrible. It gnaws at his soul. After nights spent writhing in torment, he decides that he needs to exorcise those feelings — and what better way than to create a work of art, with each scene, every line of dialogue a stinging whiplash on the aspect of society that reduced him to this state? And then he discovers it doesn’t quite work that way. There’s an audience out there, and they don’t give a rat’s behind about his suffering. They want entertainment — songs, action, comedy, romance. There are films that manage this balancing act well. Vetri Selvan isn’t one of them.".  The Times of India gave the film 2.5 stars out of 5 and wrote, "Even if we set aside the question of whether movies should really have a message, the sincerity in wanting to spread a message doesnt reflect in the inelegant filmmaking. The fault lies in the formulaic storytelling. The director chooses the tried and tested trope of a lighter first half with a mystery around the protagonist and a serious latter half where secrets are revealed and a point is made. But the problem here is that the initial set-up is very much non-existent". 

The New Indian Express wrote, "An insipid screenplay, lacklustre narration and some uninspiring performances make sure that Vetriselvan leaves no impact".  Sify wrote, "The intentions of director Rudharan is honourable but the way he has executed the film is shoddy and boring. The story and treatment is a long yawn. The director has tried to fit in as many commercial items and in the process the film loses its steam. Nothing much to recommend in the film which is a wasted and futile effort, though the message it tries to convey is topical".  Behindwoods.com gave it 1.25 stars out of 5 and wrote, "Rudran has genuinely attempted to pack the first half with comedy and finish the movie off on a sentimental note, however the lack of quality humour and an overdose of drama backstabs the engaging factor of the movie. Vetri Selvan comes out with a genuine social cause, but fails to impress", calling it "An interesting concept that isn’t presented appropriately".  Indiaglitz.com wrote, "the noble intentions have not been executed well and the end product awfully falls flat. The film ends up as a case study of how not to convey a good message". 

==References==
 

==External links==
* 

 
 
 