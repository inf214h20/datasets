The Elder Son
{{Infobox film
| name = The Elder Son
| image =
| caption =
| director = Marius Balchunas
| producer = Eric Koskin Damon Martin Scott Sturgeon
| starring = Shane West Leelee Sobieski Rade Šerbedžija Eric Balfour Regina Hall Reiley McClendon
| music = Yagmur Kaplan Mladen Milićević
| cinematography = Andrew Huebscher
| editing = David Dodson
| distributor = 2 Loop Films
| released = October 31, 2006
| runtime = 87 minutes
| country = United States
| language = English
| gross =
}} Scott Sturgeon.

==Plot==
Maxim Sarafanov (Rade Šerbedžija) has just been fired from the orchestra he played clarinet in, his son Nikita (Reiley McClendon) is in love with his schoolteacher Susan (Regina Hall) and his daughter Lolita (Leelee Sobieski) is getting married to a US pilot Greg (Brian Geraghty) and leaving home to go to Texas. But things get worse when a small time car thief Bo (Shane West) looking for a hideout from the police tells Maxim that he is his son from Maxims old girlfriend towards whom he still has feelings.

==Production notes==
The film is based on the screenplay The Elder Son written in 1968 by Russian writer Alexander Vampilov.

==Cast==
*Shane West as Bo
*Leelee Sobieski as Lolita Sarafanov
*Rade Šerbedžija as Maxim Sarafanov
*Eric Balfour as Skip
*Regina Hall as Susan
*Reiley McClendon as Nikita Sarafanov
*Brian Geraghty as Greg

==References==
 

==External links==
 
*  

 
 
 
 
 
 


 