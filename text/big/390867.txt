UHF (film)
{{Infobox film
| name = UHF
| image = UHFposter.jpg
| caption = Theatrical release poster
| director = Jay Levey
| producer = Gene Kirkwood John W. Hyde
| writer = "Weird Al" Yankovic Jay Levey
| starring= {{Plainlist | 
* "Weird Al" Yankovic Kevin McCarthy
* Michael Richards David Bowe
* Victoria Jackson
}}
| music = John Du Prez
| cinematography = David Lewis
| editing = Dennis M. OConnor
| studio = Cinecorp
| distributor = Orion Pictures 
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget = $5 million
| gross = $6.1 million   
}} David Bowe, Kevin McCarthy, Michael Richards, Gedde Watanabe, Billy Barty, Anthony Geary, Emo Philips and Trinidad Silva; the film is dedicated to Silva who died shortly after principal filming. The film was directed by Jay Levey, Yankovics manager, who also co-wrote the screenplay with him. It was released by Orion Pictures and presently distributed by MGM. 
 Ultra High Frequency (UHF) analog television broadcasting band on which such low-budget television stations often were placed in the United States. 
 straight man with a vivid imagination to support the inclusion of parodies within the film. They struggled with finding a film production company for financing the film, but were eventually able to get Orion Pictures support after stating they could keep the film costs under $5 million. Principle filming took place around Tulsa, Oklahoma, with many of the extras for the film from the Tulsa and nearby Dallas, Texas areas.

UHF earned mixed to poor critical reviews, and was further impacted by releasing in the middle of one of Hollywoods largest blockbuster summer periods. While only a modest success during its theatrical release, it became a cult film on home video. Shout! Factory released a special 25th Anniversary edition of UHF on November 11, 2014 on DVD and Blu-ray.

==Plot== David Bowe) UHF television Kevin McCarthy), chases him out angrily. On his way out of the station he encounters janitor Stanley Spadowski (Michael Richards), who was recently unjustly fired by Fletcher. George offers him a janitorial job at Channel 62.

Though George creates new shows, including the kid-friendly but poorly named "Uncle Nutsys Clubhouse", the workload and bad debt of the station get to him. Amid the stress he forgets his girlfriend Teris (Victoria Jackson) birthday, causing her to break up with him over the incident. A despondent George turns "Uncle Nutsys Clubhouse" over to Stanley so he and Bob can go out for a drink. Arriving at the bar, they find that all the patrons are excitedly watching Stanleys antics on Channel 62. Realizing they have a hit on their hands, George and Bob are revived and inspired. They come up with ideas for more bizarre, original shows in Channel 62s lineup, all spearheaded by the newly titled "Stanley Spadowskis Clubhouse". 
 duopoly rules. investment stock in Channel 62 through a telethon.
 penny he mockingly gave to a beggar earlier was a rare 1955 doubled-die cent worth a substantial fortune, which the beggar sold and used to purchase a substantial amount of Channel 62 stock. Fletcher also discovers that a slanderous conversation of his contempt for his stations viewers was secretly recorded and rebroadcast by Philo and that Channel 8 failed to file paperwork to renew its broadcast license with the FCC. The FCC revokes his license and takes the station off the air. As the film ends George and Teri rekindle their relationship, while the rest of the employees and fans of Channel 62 celebrate.
 Money for Nothing", and fake commercials for Plots R Us Mortuary Service, Gandhi II, Conan the Librarian and Spatula City are shown throughout the film.

==Cast==
* "Weird Al" Yankovic as George Newman David Bowe as Bob Steckler/Bobbo the Clown
* Fran Drescher as Pamela Finklestein
* Victoria Jackson as Teri Campbell
* Michael Richards as Stanley Spadowski
* Stanley Brock as Uncle Harvey Bilchik
* Sue Ane Langdon as Aunt Esther
* Anthony Geary as Philo
* Billy Barty as Noodles MacIntosh
* Trinidad Silva as Raul
* Gedde Watanabe as Kuni Vance Colvig Jr. as the bum Kevin McCarthy as R.J. Fletcher
* David Proval as Fletchers head goon
* John Paragon as R.J. Fletcher, Jr. Belinda Bauer as Mud Wrestler
* Dr. Demento as himself/Whipped Cream Eater
* Emo Philips as shop teacher Joe Earley
* The Kipper Kids as themselves

==Production==
Yankovic and his manager Jay Levey had discussed the idea of a movie for Yankovic around 1985, after his second major successful album; his popularity at that time led the two to thinking what other venues would work for the musician.    The story concept they created was based on Yankovics approach to his music videos, making parodies of other work. After sketching out a number of such parodies for a film, the concept of Yankovic being the owner of a small-time UHF station broadcasting this parodies as shows was born, as this would not require having any significant plot to string the parodies together, in a manner similar to Airplane!. 

The two attempted to shop the script around Hollywood film agencies for about three years without luck. They were surprised when one of their agents had shown the script to the founders of a new production company, Cinecorp, who were interested in the script and had given it to directors Gene Kirkwood and John W. Hyde; Kirkwood stated he has previously seen Yankovics videos and wanted to make a movie with him.  Kirkwood and Hyde had connections with Orion Pictures, who offered to fund the production as long as they could keep it under $5 million.  
 ultra high frequency broadcasts, which were typically known for quirkly, low-cost production shows, which the film spoofed.  However, at the same time, cable television was becoming popular and displacing UHF stations, and the meaning of UHF was being lost to the general public; by the time the film was released Internationally, they renamed it The Vidiot from UHF as to provide context to the title; Yankovic had wanted to just go with The Vidiot but the studio felt that the tie-in to the original release name was needed.   

===Locations=== The Outsiders in Oklahoma, and found the ease and the cost to film in the state to be favorable for the needs of UHF.  They found several favorable factors that made the city suitable for filming. At the time of filming, the Kensington Galleria (71st and Lewis) was being closed down to convert the mall into office space, allowing the production team to use it for both sound stage and interior scenes including those for both Channels 8 and 62; the mall was also situated near a hotel making it ideal for housing the cast and crew during filming.  The area and close proximity to Dallas allowed them to recruit additional local talent for some of the acts during the telethon scenes. 

The Burger World location was Hardens Hamburgers at 6835 East 15th Street in Tulsa, and Bowling for Burgers was filmed at Rose Bowl Lanes on East 11th Street. The bar location was Joeys House of the Blues at 2222 East 61st Street. The building used for Kunis Karate School belongs to the Tulsa Pump Company and is located at 114 West Archer in Tulsa, while "Crazy Eddies Used Car Emporium" was filmed on the lot of  . The "U-62" building was constructed around KGTO 1050s AM radio transmitter site (5400 West Edison Street); the real KGTO studios had been moved elsewhere in 1975. Just the tower itself remains at this location today.  The airport scenes were taken at Tulsa International Airport.

===Casting=== straight man The Secret Life of Walter Mitty.  As the focus of the film was to be on the parodies, George was not fleshed out beyond enough character development to drive the principle storyline.  The name "Newman" was selected as homage to Mad (magazine)|Mad magazines mascot, Alfred E. Newman, further referenced by the name of "Uncle Nutsys Clubhouse". 

The part of Stanley was written by Yankovic with Michael Richards in mind; at the time, Yankovic had been impressed with Richards stand-up comedy and performance in the show Fridays (TV series)|Fridays.  Yankovic had also considered that Stanleys part was influenced by Christopher Lloyds performance on Taxi (TV series)|Taxi, and had considered reaching out to Lloyd to offer him the part, but decided to stay with Richards due to their original premise.  Richards agents had told Yankovic that he wasnt interested in the part as Richards at the time was suffering a bout of Bells palsy, but when they reached out to Richards again, Richards came to the set, dropping right into the character of Stanley for the test read. 

Other principal roles were cast through normal auditions, with most of the choices based on how well the actor fit the role. For Georges girlfriend Teri, they didnt feel that they needed to spend a significant amount of time developing this side as they did not consider Yankovic to be the type of actor for a romantic lead. Although Jennifer Tilly and Ellen DeGeneres auditioned, they found Victoria Jacksons soft demeanor to be well-suited for the role.  

For R. J. Fletcher, they found that Kevin McCarthy was in a similar stage of his career as Leslie Nielsen, one of many "serious vintage actors who had crossed over into satire", according to Levey, and McCarthy had relished the role.  

Fran Drescher was selected for the role of Pamela, in part for her established comedy as well as her nasally voice that made for a humorous contrast for a news anchor.  

For Georges friend Bob, they looked for an actor that could be seen as a friend to George. At point they had considered Jerry Seinfeld for the role, but he had turned it down. David Bowe, who had been a long-time Yankovic fan, easily fit the part during auditions. 

Philos role was written with Joel Hodgson in mind, building on Hodgsons stand-up routines of using homemade inventions and deadpan comedy. Hodgson turned down the role feeling at the time that he wasnt a good actor.  At the time, Hodgson was also working on Mystery Science Theater 3000. They also had approached Crispin Glover for Philos role, but Glover said that he only wanted to play a used car salesman and no other part, turning down the offer.  As they sought other actors, their casting agent Cathy Henderson offered up Anthony Geary, who at the time had gained popularity due to his role on General Hospital. Geary wanted to perform the role both as a fan of Yankovic and seeing the role as completely opposite from his normal acting.  

Kuni was written with the intention of being performed by Gedde Watanabe from the start.  

Emo Philips was a close friend of Yankovic, and Yankovic felt that he had to include Philips in the movie, eventually creating the role of the clumsy shop teacher for him.  
 Cream had volunteered to audition for the part, but Yankovic and the production team found Colvig to be a better fit.  

Levey himself appears in the movie playing Mahatma Gandhi in the spoof segment Gandhi II.  

Trinidad Silva had performed his primary scenes, but he died in a car accident after returning home. Although they had planned on bringing back Silvas character for the telethon scene which had not yet been filmed, they were not comfortable with the use of body doubles and dropped this. The film was dedicated to Silva. 

==Reception==
UHF received mixed reviews. On review aggregation website Rotten Tomatoes, the film has a 63% rating based on 24 reviews, with an average score of 5.6/10.  On Metacritic, which assigns a weighted average based on selected critic reviews, the film has a score of 32 out of 100, based on 11 critics, indicating "generally unfavorable" reviews.  Critic Roger Ebert wrote in the Chicago Sun-Times that Yankovics approach to satire and parody works for the short-form music video, but does not work to fill out a full-length movie. Ebert also called to Yankovics lack of screen presence, creating a "dispirited vacuum at the center of many scenes"; he gave UHF one star out of four.  Chicago Tribune critic Gene Siskel wrote of the film, "Never has a comedy tried so hard and failed so often to be funny"; he gave it no stars.  Michael Wilmington of the Los Angeles Times believed that, as the entire film comprised parodies, it gave no structure for the larger plot to work, thus resulting in "not much of a movie". 
 summer blockbuster, hoping that it would pull them out of the water. However, critical response was negative,  and it was out of the theaters by the end of the month.  The film has been compared to Young Einstein, which similarly scored well with test audiences but failed to make a critical impression.  Yankovic has stated that it was not a "critic movie". As Yankovic states in his commentary of the movie, UHF was thought to be the movie that would "save the studio" for Orion. He was treated very well because of this. He states in the commentary: "Every morning I would wake up to fresh strawberries next to my bed. Then, when the movie bombed, I woke up and...no more strawberries!"

Within the month prior, and up to the release of UHF, studios released bigger movies like Indiana Jones and the Last Crusade, Ghostbusters II, Honey, I Shrunk the Kids, Lethal Weapon 2, Batman (1989 film)|Batman, Licence to Kill, When Harry Met Sally, and Weekend at Bernies.  The draw to these blockbuster movies is also attributable to the lower attendance at UHF  premiere; The A.V. Club, in a retrospective, called UHF "a sapling among the redwoods" and the type of film that Hollywood has since abandoned.    Yankovic and the creators of the film considered that the film had a strong audience with younger viewers which did well to fill midday matinees but did not succeed in helping to sell tickets for more lucrative evening and nighttime showings.  

The poor critical response of UHF left Yankovic in a slump  that lasted for three years, impacting the finalization of his next studio album; the slump was broken when the band Nirvana rose to wide popularity, giving him the inspiration to write "Smells Like Nirvana" and complete the album Off the Deep End. 

===Legacy=== music video of the movies theme song, a commentary track featuring director Jay Levey and Yankovic himself (with surprise guest appearances by costar Michael Richards and Emo Philips and a phoned-in appearance by Victoria Jackson), and a deleted scenes reel with Yankovics commentary. Shout! Factory released a special 25th Anniversary Edition of UHF on November 11, 2014 on Blu-ray.   

Though Yankovic has considered the possibility of a sequel, he has not actively pursued this. Yankovic noted that UHF is "a product of its era, and comedy has changed so much over the decades", but also considered that the type of comedy predated the nature of Internet phenomena  and viral videos on the Internet. 
 George Clinton, and others. The series was the brainchild of Zack Wolk, an intern for Tim and Eric Awesome Show, Great Job!

==Soundtrack==
Yankovic also released a quasi-soundtrack for the film in late 1989, titled UHF - Original Motion Picture Soundtrack and Other Stuff, which featured songs (and commercials) from the movie as well as his own new, unrelated studio material.



==See also==
* Pray TV

==References==
 

==External links==
 
*  
*  
*  
*  
*   at robohara.com
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 