Man of Vendetta
{{Infobox film
| name           = Man of Vendetta
| image          = File:Man_Of_Vendetta_poster.jpg
| director       = Woo Min-ho
| producer       = Jung Hoon-tak   Choi Jae-won   Chun Seung-chul 
| writer         = Woo Min-ho
| starring       = Kim Myung-min   Uhm Ki-joon 
| music          = Lee Jae-jin
| cinematography = Jo Yong-gyu   
| editing        = Kim Sun-min 
| distributor    = Synergy
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Man of Vendetta ( ) is a 2010 South Korean film about a pastor whose life changes after the abduction of his daughter.  
 47th Baeksang Arts Awards in 2011.

==Plot==
Joo Young-soo (Kim Myung-min) is a devoted Christian and well-respected pastor whose 5-year-old daughter, Hye-rin (Kim So-hyun), gets kidnapped. The story starts with Young-soo attempting to give the kidnapper the ransom money in exchange for his daughter in an ice hockey rink, but because of his wife, Min-kyungs (Park Joo-mi) interference by informing the police, the kidnapper doesnt show. Eight years later, Young-soo loses his faith in God and leaves the church while his wife is still desperately searching for their daughter. He opens up a business and mocks his wife for not losing hope. He soon receives a call from the kidnapper, saying his daughter is still alive and theyre asking for more ransom money, demanding that there be no police this time. Now given another chance to save his daughter, he takes matters into his own hands.  

==Cast==
*Kim Myung-min ... Joo Young-soo  
*Uhm Ki-joon ... Choi Byeong-chul  
*Kim So-hyun ... Joo Hye-rin 
*Park Joo-mi ... Park Min-kyung 
*Lee Byung-joon  ... Detective Koo 
*Oh Gwang-rok - GPS technician Kim Eung-soo - corrupt hospital chief Kim
*Min Bok-gi - delivery man
*Lee Ho-jae - Mr. No (audiophile selling expensive amp)
*Lee Jang-won - fat boss (Byeong-chuls boss)

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 