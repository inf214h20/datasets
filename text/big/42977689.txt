The Children from the Hotel America
{{Infobox film
| name           = The Children from the Hotel America
| image          = 
| image_size     =
| alt            = 
| caption        = 
| director       = Raimundas Banionis
| producer       = 
| writer         = Maciejus Drygas
| starring       = Augustas Šavelis 
Gabija Jaraminaitė 
Jūratė Onaitytė 
Jurga Kaščiukaitė 
Gediminas Karka 
Linas Paugis 
Rolandas Kazlas 
Giedrius Čaikauskas
| music          = Faustas Latėnas
| cinematography = 
| editing        = 
| studio         = 
| distributor    =
| released       = 1990
| runtime        = 88 minutes 
| country        = Lithuania
| language       = Lithuanian
| budget         = 
| gross          = 
}}
The Children from the Hotel America (Lithuanian language|Lithuanian: Vaikai iš „Amerikos“ viešbučio) is a 1990 Lithuanian drama film.

The film revolves around the lives of teenagers in Soviet Lithuania. The protagonists of the film are fans of rocknroll music which is banned in the USSR and are interested in the hippie movement, secretly listening to the Luxembourg radio. They all live in a house which was formerly a hotel, called "America". Events of 1972 unrest in Lithuania|Kaunas spring affect them as they are involved. After their attempt to send a letter to the Luxembourg radio, teenagers would become suspects by the KGB.

== See also ==
*1972 unrest in Kaunas
*Rock and roll and the fall of communism

== External links ==
* 

 
 
 

 