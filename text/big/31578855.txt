Unfinished Spaces
{{Infobox film
| name           = Unfinished Spaces
| image          = Unfinished_Spaces_film_website_banner_2011.jpg
| alt            = Unfinished Spaces Logo
| caption        = Unfinished Spaces Logo
| director       = Alysa Nahmias and Benjamin Murray
| producer       = Alysa Nahmias and Benjamin Murray
| starring       = Ricardo Porro, Vittorio Garatti, Roberto Gottardi
| music          = Giancarlo Vulcano
| cinematography = Benjamin Murray
| editing        = Kristen Nutile, Alex Minnick
| studio         = Ajna Films
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English Spanish
}}
Unfinished Spaces is a 2011 documentary film about the revolutionary design of the National Art Schools (Cuba). The film tells the dramatic story of the art schools from their founding by Fidel Castro and Che Guevara to their eventual abandonment and fall into ruin and recent efforts to restore them.

The three visionary architects Ricardo Porro, Roberto Gottardi, and Vittorio Garatti are interviewed on camera. They talk about the intense atmosphere of revolutionary Cuba and how they strove to create an entirely new language of architecture, one without precedent. They also speak about why their design fell into disfavor and how the complex was mostly abandoned, uncompleted.

As the film shows, parts of the schools are in ruins while other parts are used today by young dancers and artists. These schools are on the watch list of the World Monuments Fund, and efforts to restore the abandoned buildings are being explored. 

Unfinished Spaces (release date: 2011) was produced by Alysa Nahmias and Benjamin Murray, young filmmakers who became obsessed with the story of the schools rise and downfall. The film received several important grants  and had its World Premiere at the Los Angeles Film Festival  in June 2011. The film was favorably reviewed in Hollywood Reporter  and LA Weekly. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 