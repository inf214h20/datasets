New Faces of 1937
{{Infobox film
| name = New Faces of 1937
| image = New Faces of 1937.jpg
| image size  =
| caption = Lobby card to New Faces of 1937 James Anderson (assistant)
| producer = Edward Small
| writer = Story:   George Bradshaw   ("Shoestring")   Sketch:       Philip G. Epstein Irving Brecher William Brady Jerome Cowan Thelma Leeds Lorraine Krueger
| music = Roy Webb
| cinematography = J. Roy Hunt
| editing = George Crone
| studio = Edward Small Productions for RKO Radio Pictures
| distributor = RKO Radio Pictures (1937) (USA) (theatrical)   C&C Television Corporation (1955) (USA) (TV)   RKO Home Video (USA) (video) (laserdisc)
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget = $728,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57  
| gross = $775,000 
}}
 The Producers (1968).

==Plot==
A crooked theatrical producer deliberately sets about creating an unsuccessful show after selling more than 100% of it to investors.

==Cast==
*Joe Penner - Seymore Seymore
*Milton Berle - Wallington Wally Wedge
*Harry Parke - Parkyakarkus Parky (as Parkyakarkus)
*Harriet Hilliard - Patricia Pat Harrington William Brady - James Jimmy Thompson
*Jerome Cowan - Robert Hunt
*Thelma Leeds - Elaine Dorset
*Lorraine Krueger - Suzy
*Tommy Mack - Judge Hugo Straight, Conductor
*Bert Gordon - Count Mischa Moody
*Patricia Wilder - Pat, Hunts Secretary Richard Lane - Harry Barnes, Broker
*Dudley Clements - Plunkett, Stage Manager
*William Corson - Assistant Stage Manager
*George Rosener - Peter, Stage Doorman
*Dewey Robinson - Joe Guzzola Harry C. Bradley - Count Moodys Secretary

==Production==
Singer Rene Stone, who appears in the film, was discovered by Edward Small singing while cleaning dishes in a Manhattan restaurant. Around and About in Hollywood
Read, Kendall. Los Angeles Times (1923-Current File)   18 May 1937: 10. 

==Soundtrack==
* "New Faces"
:(1937)
:Music and Lyrics by Charles Henderson
:Played during the opening credits
:Sung and danced by showgirls (including The Brian Sisters) and showboys to open the final show
:Danced by Ann Miller
:Sung by Harriet Hilliard and showgirls
* "The Widow in Lace"
:(1937)
:Music by Harold Spina
:Lyrics by Walter Bullock
:Sung by Thelma Leeds and showgirls at rehearsal
:Played and danced by unidentified children, probably The Loria Brothers
* "Our Penthouse on Third Avenue"
:(1937)
:Music by Sammy Fain
:Lyrics by Lew Brown
:Played on piano by Harriet Hilliard and sung by her and William Brady
* "It Goes to Your Feet"
:(1937)
:Music by Sammy Fain
:Lyrics by Lew Brown
:Played and sung by Eddie Rio and Brothers Hite and Stanley act, with Lorraine Krueger
* "If I Didnt Have You"
:(1937)
:Music by Sammy Fain
:Lyrics by Lew Brown
:Sung by Harriet Hilliard and William Brady
* "Love Is Never Out of Season"
:(1937)
:Music by Sammy Fain
:Lyrics by Lew Brown
:Sung by William Brady and danced by Harriet Hilliard and male chorus
* "When the Berry Blossoms Bloom"
:(1937)
:Written by Joe Penner and Hal Raynor
:Sung and danced by Joe Penner in the show
* "Peckin"
:(1936)
:Music and Lyrics by Ben Pollack and Harry James
:Additional lyrics by Eddie Cherkose (1937)
:Sung and danced by The Three Chocolateers, The Four Playboys and chorus in the big finale in the show
* "Bridal Chorus (Here Comes the Bride)"
:(uncredited)
:from "Lohengrin"
:Music by Richard Wagner
:Swing version in the song "Peckin"
* "The Wedding March"
:(uncredited)
:from "A Midsummer Nights Dream, Op.61"
:Music by Felix Mendelssohn-Bartholdy
:Swing version in the song "Peckin"

==Reception==
The film recorded a loss of $258,000.  Reviews were mixed. THE THEATRE: Top Summer Fare
Wall Street Journal (1923 - Current file)   02 July 1937: 13.  THE SCREEN: A Suspicious Glance at New Faces of 1937, at the Music Hall-New Films at Rialto and Palace At the Rialto
By FRANK S. NUGENT. New York Times (1923-Current file)   02 July 1937: 25.  

The film was meant to be the first in a series of musical revues designed to introduce new RKO talent, but this did not eventuate. Film writers Richard B. Jewell and Vernon Harbin wrote that:
 Containing not a single memorable musical number or inspired comedy routine, this tedious mish-mash caused the studio embarrassment a-plenty. Theatre owners and audiences displayed such hostility towards the Edward Small production in general, and Penner and Parkyakaras in particular, that RKO cancelled plans to make a New Faces of 1938.  

==References==
 

==External links==
*  
*   at The New York Times
*   at Answers.com
*   at Flixster
*  
*  , by Craig Hodgkins
 

 
 
 