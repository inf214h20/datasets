A Day's Pleasure
{{Infobox film
 | name = A Days Pleasure
 | image = CC_Days_Pleasure_1919.jpg
 | caption = Theatrical poster to A Days Pleasure
 | director = Charles Chaplin
 | producer = Charles Chaplin
 | writer = Charles Chaplin Tom Wilson   Babe London   Henry Bergman   Loyal Underwood
 | music = Charles Chaplin (in 1959 re-release as part of The Chaplin Revue)
 | cinematography = Roland Totheroh
 | editing = Charles Chaplin (uncredited)
| studio  = Charles Chaplin Productions First National Pictures Inc.
 | released =  
 | runtime = 25 min. / 18 min. English (Original intertitles)
 | country = United States
 | budget =
 }} First National The Kid. It is about a day outing with his wife and the kids and things dont go smoothly. Edna Purviance plays Chaplins wife and Jackie Coogan one of the kids. The first scene shows the Chaplin Studio corner office in the background while Chaplin tries to get his car started.

==Reception== Ford car, and biff-bang slap-stick, with which he is little, if any, funnier than many other screen comedians."  

==Cast==
* Charles Chaplin - Father
* Edna Purviance - Mother
* Marion Feducha - Small Boy (uncredited)
* Bob Kelly - Small Boy (uncredited)
* Jackie Coogan - Smallest Boy (uncredited) Tom Wilson - Large Husband (uncredited)
* Babe London - His Seasick Wife (uncredited)
* Henry Bergman - Captain, Man in Car and Heavy Policeman (uncredited)
* Loyal Underwood - Angry Little Man in Street (uncredited)

== References ==
 

==External links==
*  
*   on  
*  

 

 
 
 
 
 
 
 
 
 


 