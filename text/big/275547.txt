Selena (film)
 
{{Infobox film
| name           = Selena
| image          = Selenathemovie.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Gregory Nava 
| producer       = Abraham Quintanilla, Jr. Moctesuma Esparza Robert Katz
| writer         = Gregory Nava
| starring       = Jennifer Lopez Edward James Olmos Jon Seda Constance Marie Jacob Vargas Lupe Ontiveros Jackie Guerra
| music          = Dave Grusin
| cinematography = Edward Lachman
| editing        = Nancy Richardson
| studio         = Q-Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 127 minutes 134 minutes  
| country        = United States
| language       = English Spanish
| budget         = $20 million
| gross          = $60,000,000   
}} biographical Musical musical drama film about the life and career of the late Tejano music star Selena, a recording artist well known in the Mexican-American and Hispanic communities in the United States and Mexico before she was murdered by Yolanda Saldívar, the president of her fan club at the age of 23.

The film stars Jennifer Lopez in her breakthrough role as Selena. Her father, Abraham Quintanilla, Jr., is played by Edward James Olmos and Constance Marie plays Marcella Quintanilla.

==Plot== Abraham Quintanilla Corpus Christi, Texas, to live with Abrahams brother.

Selena meets a guitarist, Chris Pérez (Jon Seda), who after joining the band, develops a friendship with her. Chris and Selenas friendship grows into love, but when Abraham catches them hugging on their tour bus, Big Bertha, he fires Chris from the band and harshly threatens a heartbroken Selena that if she continues seeing him, he will disband the group.

Selena and Chris continue seeing each other behind Abrahams back, but soon she becomes tired and tells Chris that she wants to get married right away. On April 2, 1992, Selena and Chris secretly elope, but their marriage soon makes radio headlines. She goes to see her family, who eventually accept Chris into the Quintanilla family and he returns to being the lead guitarist for Los Dinos.
 Best Mexican-American crossover album, and Saldívar gives her a friendship ring, which was really a gift from Selenas staff.

Later, Selena finds out from her father that Yolanda was stealing money from her fan club and a lot of business records have gone missing. Soon a month later (though in reality it was on March 9), Abraham, Selena and Suzette confront Saldívar about the evidence they found. Saldívar denies knowing anything about the missing records and says that if given time, she can explain everything. Days afterwards Selena performs at the Houston Astrodome in front of 65,000 people.
 murdered by Saldívar, who is later arrested after a nine-and-a-half-hour standoff while Selenas fans, friends, and family are left grieving over her death. A montage featuring the real Selena from her live performances, award show appearances, and music videos follows and the film ends with a freeze frame of a smiling Selena and a caption "Selena Quintanilla Perez 1971 - 1995".

==Cast==
* Jennifer Lopez as Selena
** Rebecca Lee Meza as Young Selena
* Edward James Olmos as Abraham Quintanilla, Jr.
** Panchito Gómez as Young Abraham
* Constance Marie as Marcela Quintanilla
* Jon Seda as Chris Pérez
* Lupe Ontiveros as Yolanda Saldívar Suzette Quintanilla
** Victoria Elena Flores as Young Suzette
* Jacob Vargas as A.B. Quintanilla
** Rafael Tamayo as Young A.B. Alexandra Meneses as Sara
* Ruben Gonzalez as Joe Ojeda
* Seidy López as Deborah
* Pete Astudillo as himself, Dinos 1990s
* Ricky Vela as himself, Dinos 1990s
* Don Shelton as himself, Dinos 1990s

==Production== El Norte executive produce the film. Abraham agreed to the film as "sort of a pre-emptive strike", and felt that it was better the film about Selenas life be made with him rather than someone else.  Abraham had personally chosen Nava, stating: "I chose Mr. Nava because I think hes a good director and scriptwriter".  The film was created with full participation from the Quintanilla family.  The biographical film was focused on Selenas life instead of her death, Nava said "I dont want to attend to  ", while her death is treated "at a distance". 

===Casting===
In June 1996, it was announced that   (1995).  The screen testing was described as "grueling", requiring "nine minutes of singing and dancing and eight pages of script."    On August 8, 1996, the Los Angeles Daily News announced that Jon Seda and Edward James Olmos had joined the cast as Chris Perez and Abraham Quintanilla. 
 
However, Lopezs casting was the subject to high criticism from fans of Selena, who werent pleased that Lopez, a   noted that "nothing could have prepared   for the hype attached to her million-dollar salary". Lopez perfected Selenas dialect while also "studying performance footage of the pop sensation" according to Nava. Lopez said "you need to do your homework on this gig" because Selena was "fresh in the publics mind".  After seeing Lopezs portrayal of Selena, protesters revised their opinions and were more accepting of Navas decision.  Filming Selena inspired Lopez to begin her own music career.  

===Filming===
  in   in Houston.  Nava said he wanted to capture the "magnificence, beauty and excitement" of the concert.  Abraham Quintanilla told Nava to remove scenes where Chris and Selena elope, because he didnt want to influence Selenas younger fans that eloping is right. However, Nava maintained that while this was true, the scene was inevitable because it was an important part to Selenas story. Abraham eventually agreed. 

===Music===
 
An original motion picture Selena (soundtrack)|Selena soundtrack was released by EMI Latin on March 11, 1997 debuting at number 20 on the US Billboard 200|Billboard 200.  The CD contains twelve tracks including Selena singing songs heard in the film. The only songs performed by Selena that were not heard on the film were "Is it the Beat," "Only Love," and "A Boy Like That," and the Selena tributes sung by other artists.

The only recordings by Selena heard on the film were the "Cumbia Medley," "Disco Medley," and "Where Did the Feeling Go?", which was played in the last half of the closing credits of the film. The Vidal Brothers "Oldies Medley" was also on the film. Included are rare tracks, hits, and cuts like the "Disco Medley, Part II", recorded live during Selenas 1995 concert at the Houston Livestock Show and Rodeo. All songs were recordings of Selena from concerts.

==Release==

===Box office===
Following its August 1995 announcement, Selena was slated for an August 1996 release date.  It was last pushed back to sometime at "the end of" 1996.  Ultimately, it was released in America on March 21, 1997, after being pushed back several times. After its opening weekend, Selena grossed a total of $11,615,722 domestically, opening at #2 at the United States box office.    In its second weekend, the film fell #3, grossing $6,138,838. The following weekend, it fell to No. 6, grossing $3,456,217. By April 20, 1997, Selena grossed a total of $32,002,285.  Its total lifetime gross stands at $35,281,794.  According to Box Office Mojo, Selena is the ninth highest-grossing musical biopic of all time. 

===Critical response===
Selena received mostly positive reviews from critics. Roger Ebert, film critic for the Chicago Sun-Times, was impressed by the acting, and gave Selena three-and-a-half stars out of four and wrote, "Young Selena is played by Becky Le Meza, who has a big smile and a lot of energy. The teenage and adult Selena is played by Lopez in a star-making performance. After her strong work as the passionate lover of Jack Nicholson in the current Blood and Wine, here she creates a completely different performance, as a loyal Quintanilla who does most of her growing up on a tour bus with her dad at the wheel." 
 TV type motion picture."  He believed the acting was top notch and wrote "Jennifer Lopez is radiant as the title character, conveying the boundless energy and enthusiasm that exemplified Selena, while effectively copying not only her look, but her mannerisms. I wonder if Selenas family, upon watching this performance, felt an eerie sense of déjà vu." 

Los Angeles Times film critic Kenneth Turan gave the film a mixed review. He wrote the film is part of a "completely predictable Latino soap opera." Yet, "there are chunks of Selena that only a stone could resist. This movie turns out to be a celebration not only of the singer but also (as "Whats Love Got to Do with It (film)|Whats Love" was for Angela Bassett) of the actress who plays her, Jennifer Lopez." 

Some critics, however, did not like how the film appears like a sanitized Selena portrait. Critic Walter Addiego considers Navas work a worshipful biography of her. Addiego, writing for the San Francisco Examiner, did have a few enjoyable moments viewing the film but wrote, "You cant help cheering for Selena, but the good feeling is diminished by the sense that her storys been simplified and sanitized."  The review aggregator Rotten Tomatoes reported that 64% of critics gave the film a positive review, based on thirty-nine reviews.  At Metacritic, which assigns a normalized rating out of 100 to reviews from film critics, it has a rating score of 65, indicating "Generally favorable reviews".

===Awards===
{| class="wikitable plainrowheaders sortable" 
|- style="background:#ccc; text-align:center;" Award
! Date
! Category
! Recipients and nominees Result
! scope="col" class="unsortable" | Ref.
|- ALMA Awards June 4, 1998 Outstanding Feature Film
|Selena
| 
|rowspan=6 style="text-align:center;"|   
|- Outstanding Actor in a Feature Film Edward James Olmos
| 
|- Jon Seda
| 
|- Outstanding Actress in a Feature Film Jennifer Lopez
| 
|- Jackie Guerra
| 
|- Outstanding Latino Director of a Feature Film Gregory Nava
| 
|- Golden Globe Awards 55th Golden January 18, 1998 Golden Globe Best Actress – Motion Picture Musical or Comedy Jennifer Lopez
| 
|style="text-align:center;"| 
|- Grammy Awards 40th Grammy February 25, 1998	 Grammy Award Best Score Soundtrack for Visual Media
| 
| 
|style="text-align:center;"| 
|- Imagen Awards April 1, 1998 Best Theatrical Feature Film
|Selena  
| 
|style="text-align:center;" rowspan=2| 
|- Lasting Image Award
| Jennifer Lopez, Selena
| 
|- Lone Star Film Awards 1998
|Best Actress Jennifer Lopez
| 
|rowspan=2 style="text-align:center;"| 
|- Best Supporting Actor Edward James Olmos
| 
|}

==Distribution==
The film opened in wide release on March 21, 1997 (1,850 theaters) and sales the opening weekend were $11,615,722. Selena ran for 15 weeks domestically (101 days) and eventually grossed* 60,000,000 ($35,281,794 in the United States. The film sales worldwide were considerably more. At its widest release the film was shown in 1,873 screens. The production budget of the film was approximately $20,000,000.  

A 10th Anniversary DVD edition of Selena was released on September 18, 2007 by Warner Home Video. The two-disc set contains the original theatrical version (127 minutes) and a directors cut version (134 minutes) of the film, which had been shown on several TV stations before. Extras include a Making of Selena: 10 Years Later featurette, a Queen of Tejano featurette, and nine additional scenes.   at DVD Active.
 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 