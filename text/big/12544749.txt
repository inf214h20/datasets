Butterfly Kiss
 
{{ Infobox film
| name = Butterfly Kiss
| image = Butterfly Kiss (1995 film).jpg
| caption =
| director = Michael Winterbottom
| writer = Frank Cottrell Boyce
| producer =
| starring = Amanda Plummer Saskia Reeves
| cinematography =
| editing =
| music =
| runtime = 88 minutes
| distributor =
| released =  
| language = English
| country = United Kingdom
| budget = £400,000
}}
Butterfly Kiss is a 1995 British film, directed by Michael Winterbottom and written by Frank Cottrell Boyce. It stars Amanda Plummer and Saskia Reeves. It was also released under the alternative title Killer on the Road. The film was entered into the 45th Berlin International Film Festival.   

==Plot==
Set on the bleak motorways of Lancashire, Butterfly Kiss tells the story of Eunice (Plummer), a bisexual serial killer, and Miriam (Reeves), a naive, innocent and lonely young girl who falls under her spell.

Miriam runs away from home and meets Eunice, who soon completely dominates her, leading her into sex and murder. At a truckstop, Eunice first offers the unwilling Miriam to a trucker for sex, then rescues her in mid-rape by murdering the driver. When the hitchhiking duo are picked up by another licentious male, Miriam returns to their motel room to find Eunice and their benefactor having rough sex in the shower. Mistaking the consensual sex for the rape from which Eunice earlier rescued her, Miriam returns the favor by beating their benefactor to death with the hand-held showerhead, to Eunices delight. Eunice finally brings Miriam to the ocean, where she has Miriam murder her.

==Main cast==
{| class="wikitable"
|-
! Actor !! Role
|-
| Amanda Plummer || Eunice
|-
| Saskia Reeves || Miriam
|-
| Kathy Jamieson || Wendy
|-
| Des McAleer || Eric McDermott
|-
| Lisa Riley || Danielle
|-
| Freda Dowie || Elsie
|-
| Paula Tilbrook || Ella
|-
| Fine Time Fontayne || Tony
|-
| Joanne Cook || Angela
|-
| Elizabeth McGrath || Waitress
|-
| Shirley Vaughan || Waitress
|-
| Paul Bown || Gary
|-
| Emily Aston || Katie
|-
| Ricky Tomlinson || Robert
|-
| Katy Murphy || Judith
|-
| Adele Lawson || Wife
|-
| Jeffrey Longmore || Husband
|-
| Suzy Yannis || Motel receptionist
|-
| Julie Walker || Shop assistant
|}

==Reception== schizophrenic personality."   

Variety (magazine)|Variety called it "An often breathtakingly original meld of road movie, lesbian love story, psychodrama and black comedy".   

Andrew Billen praised the brilliant dialogue in what he described as "a lesbian Thelma And Louise set on the M6 motorway|M6"  

== See also ==

*Aileen Wuornos

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 