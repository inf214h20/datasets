Enas delikanis
{{Infobox film
| name           = Enas delikanis Ένας ντελικανής
| image          = 
| image_size     = 
| caption        = 
| director       = Manolis Skouloudis
| producer       = 
| writer         = Manolis Skouloudis
| starring       = Alkis Giannaikas Manos Katrakis Ilya Livykou Kleo Skouloudi Dionyssis Papayannopoulos Kostas Kazakos Anna Paitazi Aliki Zografou Efi Ikonomou
| music          = Giannis Markopoulos
| cinematography = 
| photographer   = 
| editing        = 
| distributor    = Mask Films 1963
| runtime        = 90 min
| country        = Greece Greek
| cine.gr_id     = 
}}
 1963 Greece|Greek film directed by Manolis Skouloudis.  It stars Ilya Livykou and Dionysis Papagiannopoulos.  Filmed in the island of Crete.

==Plot==
Kakonissi.  The son of Pontikaki, Manouelis, had grown up in which he became more beautiful.  All the ladies from the village knew that were much from all the singing women, Paraskevoula.  Manouelis fell in love with Smaragditsa.  His father tried to bring himself a shame, brought his aunt Eirinaki which she live in the mountains along with their friends.  As her aunt seduced from her beauty and that she wanted to love.  Manouelis left from the mountains and headed for Ladochori at the time they had a funeral.  The president of the village wrote from his voice that received and knew the spot that the singer and headed to the house in which live their six ladies.

==Cast==

*Alkis Giannakas as Manouelis
*Manos Katrakis as Pontikakis
*Ilya Livykou as Eirinaki or Irinaki
*Kleo Skouloudi as the singers wife
*Dionyssis Papayannopoulos as a mayor
*Kostas Kazakos
*Anna Paitazi
*Aliki Zografou
*Efi Ikonomou as a tourist

==Awards==
* 5 Awards at the 1963 Thessaloniki Film Festival (photography (Dimos Sakellariou), first ladies role (Ilya Livykou), critic photography (Dimos Sakellariou), first ladies critic role (Ilya Livykou, and Alkis Giannakas).
*First Ladies role (Ilya Livykou) at the 1963 San Francisco Film Festival

==Other information==

*Tickets: 295,493
*It was the first film appearance by Alkis Giannakas.

==See also==
*List of Greek films

==External links==
* 
*   

 
 
 
 


 
 