The Children of Captain Grant (film)
{{Infobox film
| name =  The Children of Captain Grant 
| image =
| image_size =
| caption =
| director = Vladimir Vaynshtok   David Gutman
| producer =  L. Sokolovsky      M. Zarzhiykaya  
| writer =  Jules Verne (novel)   Oleg Leonidov  
| starring = Nikolai Cherkasov   Ivan Chuvelyov   Yuri Yuryev   Mariya Strelkova
| music = Isaak Dunayevsky  
| cinematography = Arkadi Koltsaty   Aleksandr Ptushko
| editing =  Tatyana Likhachyova      
| studio = Mosfilm  
| distributor = 
| released = 15 September 1936
| runtime = 88 minutes
| country = Soviet Union Russian
| budget =
| gross =
}} Mysterious Island. Scottish children go on a global search for their missing father, the sailor Captain Grant.

==Cast==
*   Nikolai Cherkasov as Jacques Paganel
* Yakov Segel as Robert Grant 
* Olga Bazarova as Mary Grant  
* David Gutman as MacNabs 
* Mariya Strelkova as Elena Glenarvan  Mikhail Romanov as Captain John Mangles   Edward Glenarvan Ayerton
* A. Adelung as Talkav  
* Nikolai Michurin as Innkeeper 
* Yuri Yuryev as Captain Tom Grant

==References==
 

==Bibliography==
* Evgeny Dobrenko & Marina Balina. The Cambridge Companion to Twentieth-Century Russian Literature. Cambridge University Press, 2011. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 