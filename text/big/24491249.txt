Mistrial (1996 film)
{{Infobox Film
| name           = Mistrial
| image          = 
| image_size     = 
| caption        = 
| director       = Heywood Gould
| producer       = Bart Brown Bruce Cohen Geena Davis Renny Harlin Rebecca Spikings
| writer         = Heywood Gould
| narrator       = 
| starring       = 
| music          = Brad Fiedel   
| cinematography = Paul Sarossy  
| editing        = Rod Dean Jon Poll    
| distributor    = HBO 
| released       = November 2, 1996
| runtime        = 89 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1996 United American drama film written and directed by Heywood Gould and starring Bill Pullman, Jon Seda, and Robert Loggia. The film aired on HBO.

==Plot==
After community activist Eddie Rios (Seda), charged with the murder of two NYPD officers, one of them his ex-wife, is found not guilty due to legal technicalities, arresting detective Steve Donohue (Pullman) takes the judge, jury, and Rios hostage, and decides to have a new trial, presenting evidence that was not previously allowed. His captain Lou Unger (Loggia) tries to convince Donohue to end his hostage taking peaceably.

==Background==
Although the film was shot principally in Canada, the NYPD and other NYC sources were credited with assisting and advising.

==Cast==
* Bill Pullman as Steve Donohue
* Robert Loggia as Captain Lou Unger
* Jon Seda as Eddie Rios
* Blair Underwood as Lieutenant C. Hodges
* Leo Burmester as Commissioner Russell Crane
* Roma Maffia as Laurie Meisinger
* James Rebhorn as Mayor Taylor
* Christina Cox as Officer Ida Cruz
* Josef Summer as Nick Mirsky
* Peter McNeill as Chief Inspector Ray Hartman

== Sources ==
* 
* 
*http://www.variety.com/profiles/TVMOW/main/55385/Mistrial.html?dataSet=1

 

 
 
 
 
 
 
 
 

 