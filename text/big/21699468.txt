Ragazzi fuori
{{Infobox Film name         =Ragazzi fuori image         =Ragazzi fuori.jpg director  =Marco Risi writer    =Aurelio Grimaldi starring  =Francesco Benigno Alessandro Di Sanzo Salvatore Termini Alfredo Li Bassi Maurizio Prollo Vincenza Attardo Roberto Mariano music      =Giancarlo Bigazzi
|cinematography=Mauro Marchetti editing     =Franco Fraticelli distributor =Cecchi Gori released    =14 September 1990 runtime     =110 minutes country     =Italy language    =Italian genre       =Drama
|}}
Ragazzi fuori ("Boys on the Outside") is an Italian language dramatic film directed by Marco Risi in the Italian neorealism|neo-neorealistic style and written by Aurelio Grimaldi. Released in 1990, it is the sequel to the 1989 film Mery per sempre. It stars Francesco Benigno, Alessandro Di Sanzo and Salvatore Termini.

== Synopsis ==
Ragazzi Fuori is the sequel to the 1989 dramatic film Mery per sempre, and features most of the same characters. The film is largely set in ZEN (Palermo)|ZEN, a bleak, economically deprived quarter on the northern outskirts of Palermo, Sicily, at the end of the 1980s. Its protagonist is Natale Sperandeo (played by Palermo-born actor Francesco Benigno), a young man who has just been released from Malaspina, a juvenile detention centre. Unable to find legitimate work, he takes up with his former gang, consisting of unemployed youths like himself, and perpetrates an armed robbery. 
 plainclothes police market of Vucciria after a long chase through the streets of Palermo for having robbed a car radio. 
 Sicilian dialect of Palermo.

== Cast and awards ==
   

Francesco Benigno won two awards, the Ciak dOro at the Venice Film Festival, and Premio Piper, for Best Actor in his portrayal of Natale Sperandeo. The film also features, among others, Alessandro Di Sanzo as Mario "Mery" Libassi, Salvatore Termini as Giovanni "King Kong" Trapani, Alfredo Li Bassi as Carmelo Vella, Maurizio Prollo as Claudio Catalano, Vincenza Attardo as Vita, Roberto Mariano as Antonino Patané, and Tony Sperandeo as Turris.
On 14 November 1990, Mariano was killed in a plane crash en route to Switzerland. 
Marco Risi won the David di Donatello Award in 1991 for Best Director.

== References ==

 
Ragazzi Fuori at the Internet Movie Database

 
 
 
 
 
 
 