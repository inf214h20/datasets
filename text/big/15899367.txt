Filming Othello
{{Infobox film
| name           = Filming Othello
| image          = Filming Othello.jpg
| image_size     = 
| caption        = 
| director       = Orson Welles
| producer       = Juergen Hellwig Klaus Hellwig
| writer         = Orson Welles
| narrator       = 
| starring       = Orson Welles Micheal MacLiammoir Hilton Edwards
| music          = Alberto Barberis Angelo Francesco Lavagnino
| release        = 
| cinematography = Gary Graver
| editing        = Marty Roth
| released       =  
| runtime        = 84 minutes
| country        = West Germany
| language       = English
| budget         = 
| gross          = 9.327 admissions (France)   at Box Office Story 
| preceded_by    = 
| followed_by    = 
| distributor    = Hellwig Productions
}} West German television, was the last completed feature film directed by Welles.

==Plot==

Filming Othello begins with Welles standing behind a  , William Shakespeare|Shakespeares play and the film I made of it."  Welles initially conducts a monologue where he recalls the events that lead up to the creation of Othello and some of the problems that plagued the production. As the film progresses, he switches to a conversation in a restaurant between himself and two of the film’s co-stars, Micheal MacLiammoir (who played Iago) and Hilton Edwards (who played  Brabantio). The three men talk at length about the making of Othello.  Welles then resumes his monologue from his position behind the moviola. He then runs footage on the moviola of a question and answer session he conducted during a 1977 screening of Othello in Boston. Welles concludes the film in his position as a monologuist, proclaiming: "There are too many regrets, there are too many things I wish I could have done over again. If it wasnt a memory, if it was a project for the future, talking about Othello would have been nothing but delight. After all, promises are more fun than explanations. In all my heart, I wish that I wasnt looking back on Othello, but looking forward to it. That Othello would be one hell of a picture. Goodnight."   

==Production==
 the second The Trial, was never completed. 

Filming Othello was shot in 16mm, with Gary Graver as the cinematographer. Welles shot the footage of his conversation with MacLiammoir and Edwards in Paris, France, in 1974, and shot the footage of his part of their conversation two years later in Beverly Hills, California.  Footage was also shot of Welles visiting Venice, Italy, but it was not included in the final print because it had been believed lost when Welles was moving around Europe.  However, many years later, cinematographer Gary Graver located at least some of the footage, and short excerpts can be seen in his 1993 documentary Working With Orson Welles, in which Welles (theatrically clad in black cape and black hat) rides around Venice in a gondola pointing out old filming locations, while crowds wave at him.

Filming Othello uses clips from Othello, but the footage is not accompanied by the film’s dialogue track. 

==Distribution==
Filming Othello was first shown at the 1978 Berlin Film Festival. It was first screened in the U.S. in 1979 at the Public Theater in New York, where it played on a double bill with Othello.  However, the film’s presentation did not receive newspaper reviews.   Filming Othello had no further U.S. screenings until it returned to New York in 1987 for an engagement at the Film Forum, a nonprofit cinema, and that presentation was acknowledged by Vincent Canby of The New York Times as "entertaining and revealing" and "full of priceless anecdotes."  

To date, Filming Othello has never been theatrically released or presented on home video.   The film has been kept out of circulation due to a dispute between the filmmaker’s daughter Beatrice Welles (who owns the rights to Othello), from his marriage to Paola Mori, and Oja Kodar, the Croatian actress and Welles’s companion and collaborator in his later years (who owns the rights to Filming Othello). Specifically, since the 1991 restoration of Othello overseen by Beatrice Welles, she has not allowed any footage of her fathers original version of Othello to be shown in any context, and as Filming Othello contains many clips of Othello, its circulation has been effectively blocked. Film critic and historian Jonathan Rosenbaum has accused Beatrice Welles of being motivated solely by profit in this decision, since she can only claim royalties from the restored version of Othello, and has thus ensured that only her version (which he believes to be inferior) is available. 

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 