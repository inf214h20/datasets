Green Zone (film)
{{Infobox film
| name           = Green Zone
| image          = Green Zone poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Paul Greengrass
| producer       =  
| writer         =  
| based on       =  
| starring       =   John Powell
| cinematography = Barry Ackroyd Christopher Rouse
| studio         = Working Title Films
| distributor    = Universal Pictures (US & UK) StudioCanal (France)
| released       =  
| runtime        = 115 minutes
| country        = United States United Kingdom France   Retrieved November 4, 2012 
| language       =  
| budget         = $100 million 
| gross          = $94.9 million 
}}
 war Thriller thriller film directed by Paul Greengrass. The storyline was conceived from a screenplay written by Brian Helgeland, based on a 2006 non-fiction book Imperial Life in the Emerald City by journalist Rajiv Chandrasekaran. The book documented life within the Green Zone in Baghdad during the 2003 invasion of Iraq.    
 invasion of Chief Warrant Iraqi weapons intel given to him is inaccurate. Moreover, Millers efforts to find the true story about the weapons are blocked by Pentagon official Clark Poundstone (Greg Kinnear). The cast also features Brendan Gleeson, Amy Ryan, Khalid Abdalla and Jason Isaacs.
 Antena 3 Films and Dentsu.  Principal photography for the film project began during January 2008 in Spain, later moving to Morocco and the United Kingdom.

Green Zone premiered at the Yubari International Fantastic Film Festival in Japan on February 26, 2010, and was released in Australia, Russia, Kazakhstan, Malaysia and Singapore on March 11, 2010, followed by a further 10 countries the next day, among them the United States, United Kingdom and Canada.  Although the film generally received positive critical reviews, it was a box office Box office bomb|flop, as it cost $100 million to produce plus $40 million in marketing, while the global theatrical runs only gave $94,882,549 in gross revenue. 

==Plot== invasion of Iraq. Al-Rawi suggests waiting for the Americans to arrive and have them perhaps make his army an offer to join their forces in forming a government coalition against foreign insurgents.
 Chief Warrant Iraqi weapons intel given to him is inaccurate and anonymous, stating that on his last three attempts to find WMDs, teams had taken casualties but come up with nothing. High-ranking officials quickly dismiss Millers theory about the intelligence being false. After the debriefing, Miller is then stopped by Martin Brown (Brendan Gleeson), a CIA officer based in the Middle East, who tells Miller that the next place he is going to investigate for WMDs is also empty, as a UN team had already searched there two months ago.

Meanwhile, Pentagon official Clark Poundstone (Greg Kinnear) is welcoming an Iraqi politician named Ahmed Zubaidi (Raad Rawi) at the Baghdad International Airport, where he is questioned by a journalist named Lawrie Dayne (Amy Ryan). Because of global pressure about the WMD intel not materializing, she says she needs to speak directly to "Magellan," to which Poundstone brushes her off, claiming Magellan is "locked up tight" and that not even he could access him.
 special operations personnel - however, Miller manages to maintain possession of the mans notebook. Dayne finds Poundstone again and complains, but he maintains his dismissive posture, justifying that the stakes are much larger than her role in selling newspapers.

Miller goes to Browns hotel in the Green Zone where he tells him what happened and gives him the notebook. Brown arranges for Miller to visit the man removed from his custody by the special operations personnel. Before leaving, he is approached by Dayne, who questions him about the false reports of WMDs. Miller soon finds the Iraqi informant in prison. Near death after being tortured by the special forces personnel, the man informs Miller that they "did everything you asked us to in the meeting." When Miller asks for clarification about which meeting the captive is referring to, he responds with one word: "Jordan." Miller then goes to Daynes hotel room to confront her about the bogus intel she published, but she refuses to disclose her source. He continues to press her about what she knows. After Miller tells her his evidence for Al-Rawi being the informant, "Magellan," Dayne reluctantly confirms that Magellan had met with a high-ranking official in February in Jordan.
 decision to First Persian Iraqi insurgency desperately tries to hold off the American forces before being overwhelmed by their air support. Later, in his hotel suite, Miller writes a report of everything that happened.

Miller confronts Poundstone in a meeting and gives him the report. Poundstone tells Miller that WMDs do not matter, causing a physical confrontation where Miller states that "the reasons for war always matter." Poundstone then rejoins the Iraqi meeting, only to see the Iraqi factional leaders yelling at each other and leaving the meeting - a fate previously predicted by Martin Brown. Afterwards, Dayne receives an emailed copy of Millers report. The recipient list includes reporters for over two dozen news agencies around the world.

In the final scene, the camera pans out from Millers convoy showing an oil facility to the right, suggesting the real reason of the war.

==Cast==
 
* Matt Damon portrays Roy Miller,    an idealistic US Army Chief Warrant Officer on the hunt for WMDs in Iraq and the films main protagonist.  Roy Miller is based on real-life Army chief warrant officer Richard "Monty" Gonzales. Damon joined the film with the assurance that production would conclude by April 14, 2008 so he could start working on the Steven Soderbergh film The Informant! on April 15, amid scheduling difficulties caused by the 2007–2008 Writers Guild of America strike.  Bush administrations Judith Miller."  
* Brendan Gleeson portrays Martin Brown, the CIA Baghdad bureau chief, loosely based on Jay Garner.  Pentagon Special Paul Bremer, the "...Coalition Provisional Authority head in 2003–04... 
* Yigal Naor portrays General Mohammed Al Rawi,  who is an informant called Magellan, loosely based on the real-life informant Rafid Ahmed Alwan aka "Curveball (informant)|Curveball". Rose, Steve   The Guardian, 8 March 2010 
* Jerry Della Salla portrays platoon sergeant Wilkins.
* Nicoye Banks portrays Perry. Delta and DEVGRU operators Task Force 88, whose mission is the elimination of Most-wanted Iraqi playing cards|"deck of cards" high-value targets.
* Martin McDougall portrays Mr. Sheen, CIA Baghdad assistant bureau chief. United 93. Riverbend and Alive in Baghdad.  Michael ONeill portrays Colonel Bethel
* Antoni Corone portrays Colonel Lyons  Tommy Campbell portrays the Chopper Comms Commander.
* Paul McIntosh portrays a CIA officer.
* Sean Huze portrays U.S. Army Sergeant Conway, a member of Roy Millers MET team. 
* Robert Harrison ONeil portrays a TV Journalist.
* Ben Sliney portrays the bureaucrat at VTC
* Said Faraj portrays Seyyed Hamza
* Abdul Henderson portrays Marshall, a member of Roy Millers MET

==Production==

===Development=== The Bourne United 93. The Bourne Supremacy and The Bourne Ultimatum,      and the actor joined the project in June 2007.  Actors Amy Ryan, Greg Kinnear, and Antoni Corone were later cast in January 2008.  Greengrass said of the projects contemporary relevance, "Film shouldnt be disenfranchised from the national conversation. It is never too soon for cinema to engage with events that shape our lives." 

===Themes and inspirations===
  Thomas Ricks, and Ron Suskind, in addition to Rajiv Chandrasekaran, whose book he optioned. He has even compiled a document, How Did We Get It So Wrong?, detailing what he learned. Although Greengrass initially wanted to make a smaller film, he eventually decided a bigger budget production would expose more people to the ideas in the film. 
 disbanding the Iraqi army portrayed in the film represent debates that actually took place by US policy makers. The issue of the culpability of the Fourth Estate, i.e. the mainstream (news) media, or MSM, in taking intelligence at face value, although embodied by a single character, represents a broad based failing in both the USA and UK, but for Greengrass the fault ultimately lay with those trying to manipulate them. Faraci, Devin   Chud 

Greengrass has said that both the Bourne films and Green Zone reflect a widespread popular mistrust of authority that was engendered by governments that have deliberately lied and have let their citizens down over the Iraq war.  The confusion surrounding the absence of WMD in Iraq also provided an ideal scenario for a thriller, in which the protagonist battles for the truth. 

===Filming===
Production of Green Zone was originally slated to begin in late 2007.  Instead, it began at the Los Alcázares Air Base in Spain    on January 10, 2008,  moved to Morocco, and finished filming in the UK in December 2008.

===Soundtrack===
The original motion picture soundtrack was composed by musician John Powell. Jorge Adrados mixed the sound elements for the chorus, while Jon Olive edited the films music. The soundtrack for the film was released on March 9, 2010, by the Varèse Sarabande music label. 

{{Infobox album
| Name = Green Zone: Original Motion Picture Soundtrack
| Type = Film score
| Artist = John Powell
| Cover = 
| Released = March 9, 2010
| Length = 52:41
| Label = Varèse Sarabande
| Reviews =
| Last album = 
| This album = Green Zone (2010)
| Next album =   (2010)
}}
{{Track listing
| collapsed       = no
| headline        = Green Zone: Original Motion Picture Soundtrack
| total_length    = 52:41
| title1          = Opening Book
| length1         = 2:32
| title2          = 1st WMD Raid
| length2         = 2:39
| title3          = Traffic Jam
| length3         = 2:59
| title4          = Meeting Raid
| length4         = 4:33
| title5          = Helicopter/Freddy Runs
| length5         = 2:43
| title6          = Questions
| length6         = 3:25
| title7          = Miller Googles
| length7         = 1:55
| title8          = Truth/Magellan/Attack 
| length8         = 3:50
| title9          = Mobilize / Find Al Rawi
| length9         = 5:17
| title10         = Evac Preps Part 1
| length10        = 8:36
| title11         = Evac Preps Part 2
| length11        = 3:24
| title12         = Attack and Chase 
| length12        = 5:26
| title13         = WTF
| length13        = 1:16
| title14         = Chaos/Email 
| length14        = 4:17
}}

==Release==
Green Zone opened in Australia and Russia on March 11, 2010. It was released in the United States and some other countries on March 12, 2010.

===Home media===
The film was released on DVD and Blu-ray in United States on June 22, 2010.   

===Box Office=== Shutter Island between the USA and UK, Green Zone did twice as well in the UK as on the other side of the Atlantic Ocean|Atlantic.

Given its budget of roughly $100 million, in addition to its $40 million in marketing, Green Zone has been referred to as a flop for its production company Universal Studios.    The Guardian stated that the film would be unlikely to recoup its production costs through box-office receipts alone.  Green Zone has grossed $94,882,549 in total worldwide ($35,053,660 in the United States and Canada plus $59,828,889 elsewhere). 

===Critical response===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"The action in "Green Zone" is followed by Greengrass in the QueasyCam style Ive found distracting in the past: lots of quick cuts between hand-held shots. It didnt bother me here. That may be because I became so involved in the story. Perhaps also because unlike the "Bourne (film series)|Bourne" films, this one contains no action sequences that are logically impossible."
|-
| style="text-align: left;" |—Roger Ebert, writing for the Chicago Sun-Times 
|} Bloody Sunday through the second and third Bourne movies (which turned Mr. Damon into a minimalist movie star), this director has honed his skill at balancing chaos with clarity."  Empire Magazines Mark Dinning was nearly as enthusiastic by awarding the film 4/5 stars and concluding that "There is a murky morality in the whole sorry saga of Iraq. Some of the motivation is money, some of it is a genuine — if confused — desire for good. Reel can’t quite match real in portraying this aspect, but Green Zone will nevertheless provoke thoughts as well as thrills — it’s an honest, compelling, smart blockbuster that dares to deliver on several levels. And in that, at the very least, star and director are bang on target."  

The film has received moderately positive response from critics. Review aggregation website Rotten Tomatoes gives the film a score of 53% based on reviews from 179 critics, with an average score of 6/10.    The sites consensus is "Matt Damon and Paul Greengrass return to the propulsive action and visceral editing of the Bourne (film series)|Bourne films – but a cliched script and stock characters keep those methods from being as effective this time around."  normalized rating of 0–100 on top reviews from mainstream critics, gave the film a "generally favorable" score of 61% based on 35 reviews. 

In the UK, The Daily Mail called the film "a preachy political thriller disguised as an action flick". The paper acknowledged that, while important political truths do emerge over the course of the film, the film overreaches itself as these points have "the air of being aimed at ignorant American teenagers."  Tim Robey in The Daily Telegraph conceded that "with all we retrospectively know about the wool-pulling to make the case for war, its a kick to follow a main character on the ground who smells a rat"; he nevertheless criticized the film for lacking credibility in its portrayal of a rogue hero who never faces a reprimand and never suffers paranoia. 

More enthusiastically, Andrew OHagan in The Evening Standard called Green Zone "one of the best war films ever made" because "it does what countless newspaper articles, memoirs, government statements and public inquiries have failed to do when it comes to the war in Iraq: expose the terrible lies that stood behind the decision of the US and Britain to prosecute the war, and it does so in a way that is dramatically brilliant, morally complex and relentlessly thrilling." 

The New York Times designated the film a Critics Pick and said that the movie, while addressing timely concerns, "seems to epitomize the ability of mainstream commercial cinema to streamline the complexities of the real world without becoming overly simplistic, to fictionalize without falsifying." 

===Political reaction=== Fox News.com states, "Given this set-up, audiences are encouraged to root for Millers rogue activities and against the government, represented in the film by a corrupt Pentagon chief played by Greg Kinnear."   
 conspiracy by sections of the American government is incorrect. He sees the film as an exciting "Bourne-in-Baghdad thriller".  Matt Damon cites Gonzales motives for working on the film as being "because we need to regain our moral authority." 

James Denselow, writing for The Guardian, praises the films portrayal of the conflict, saying "ultimately what gives the film its credibility is that it avoids any simplistic idea that Iraq could have simply been got right. Indeed Millers vision of exposing the WMD conspiracy and the CIAs plan to keep the Iraqi army is undermined by the films wildcard – a nationalist Shia war veteran who turns the plot on its head before delivering the killer line to the Americans when he tells them: It is not for you to decide what happens here  ."   

Greengrass defended his film in an interview with Charlie Rose, saying, "The problem, I think, for me is that something about that event strained all the bonds and sinews that connect us all together. For me its to do with the fact that they said they had the intelligence, and then it emerged later that they did not."  Matt Damon also defended the film, telling MTV News, "I dont think thats a particularly incendiary thing to say. I think thats a journey that we all went on and a fundamental question we all asked and its not partisan."  On March 13, Michael Moore commented: "I cant believe this film got made. Its been stupidly marketed as an action film. It is the most HONEST film about the Iraq War made by Hollywood." 

==See also==
 
* 2010 in film
* Green Zone

==References==
;Footnotes
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 