When Taekwondo Strikes
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = When Taekwondo Strikes
| image          = When Taekwondo Strikes.jpg
| caption        = 
| director       = Feng Huang Ng Sek Sammo Hung (action)
| producer       = Raymond Chow
| eproducer      = 
| aproducer      = 
| writer         = Feng Huang    
| starring       = Jhoon Rhee Angela Mao Anne Winton Sammo Hung Wong In Sik Kenji Kazama Carter Wong
| music          = 
| cinematography = Danny Lee
| editing        = Peter Cheung Golden Harvest
| released       = 1973
| runtime        = South Korea: 91 min USA: 95 min  Hong Kong
| awards         =  Cantonese Mandarin Mandarin
| budget         = 
}}
 martial arts fight choreography. Golden Harvest producer Andre Morgan. This was Jhoon Rhees only film.

==Plot==
The story is about the Korea under Japanese rule during World War II. A Korean nationalist played by Carter Wong gets into a fight with some non-Korean Japanese people and is chased into a church.  The priest there is captured and tortured. Trying to secure his release, the leader of the resistance, Jhoon Rhee is himself captured and tortured by the Japanese. Carter Wong, Angela Mao and Anne Winton have to now try and rescue him. This leads to an explosive climax with the heroes having to fight the likes of Wong In Sik (Hwang In-Shik), Sammo Hung and Kenji Kazama.

==Cast==
*Jhoon Rhee – Lee Chung Tung / Li Jun Dong
*Angela Mao – Wan Ling Ching / Huang Li Chen
*Anne Winton – Mary
*Andre E. Morgan – Father Lu Yi
*Carter Wong – Jin Zheng Zhi
*Chin Chun – Zhou
*Wong Fung – Lieutenant Makibayashi
*Wong In Sik (Hwang In-Shik) – Japanese Leader
*Kenji Kazama – Japanese Leader
*Sammo Hung – Japanese
*Alan Chui - Japanese
*Yuen Biao - Japanese (extra)
*Lam Ching-Ying - Japanese (extra)
*Wilson Tong - Korean at restaurant
*Baan Yun-Sang - Japanese
*Billy Chan - Japanese (extra)
*King Lee - Japanese
*Hsu Hsia - Japanese (extra)
*Po Tai - Japanese

==References==
*http://www.fareastfilms.com/reviewsPage/When-Tae-Kwon-Do-Strikes-1950.htm

== External links ==
*  

 
 
 
 


 