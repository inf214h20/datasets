Shadowlands (1993 film)
 
 
 
 

{{Infobox film
| name           = Shadowlands
| image          = Shadowlands ver2.jpg
| image_size     =
| caption        = UK theatrical release poster
| director       = Richard Attenborough
| producer       = Richard Attenborough Brian Eastman William Nicholson
| starring       = Anthony Hopkins Debra Winger Edward Hardwicke
| music          = George Fenton Roger Pratt
| editing        = Lesley Walker
| studio         = Price Entertainment
| distributor    = Savoy Pictures (US) Paramount Pictures (UK)
| released       =  
| runtime        = 131 minutes
| country        = United Kingdom
| language       = English
| budget         = $22 million
| gross          = $25,842,377
}} William Nicholson 1985 television production and 1989 stage adaptation of the same name. The original television film began life as a script entitled I Call it Joy written for Thames Television by Brian Sibley and Norman Stone. Sibley later wrote the book, Shadowlands: The True Story of C. S. Lewis and Joy Davidman.

==Plot== Magdalen College Douglas on their visit to England, not yet knowing the circumstances of Greshams troubled marriage.
 Warnie disrupted by the outspoken, feisty Gresham, whose uninhibited behaviour offers a sharp contrast to the rigid sensibilities of the male-dominated university. Each provides the other with new ways of viewing the world.

Initially their marriage is one of convenience, a platonic union designed to allow Gresham to remain in England. But when she is diagnosed with cancer, deeper feelings surface, and Lewis faith is tested as his wife tries to prepare him for her imminent death.

==Cast==
* Anthony Hopkins as C. S. Lewis|C. S. "Jack" Lewis
* Debra Winger as Joy Davidman Warren "Warnie" Lewis
* Joseph Mazzello as Douglas Gresham
* James Frain as Peter Whistler
* Julian Fellowes as Desmond Arding
* Michael Denison as Harry Harrington John Wood as Christopher Riley

==Critical reception==
Shadowlands received positive reviews from critics and maintains a 96% "fresh" rating on Rotten Tomatoes based on 28 reviews.

Roger Ebert of the Chicago Sun-Times called the film "intelligent, moving and beautifully acted." 

Rita Kempley of the Washington Post described it as "a high-class tear-jerker" and a "literate hankie sopper" and added, "William Nicholsons screenplay brims with substance and wit, though its essentially a soap opera with a Rhodes scholarship . . .   and Hopkins lend great tenderness and dignity to what is really a rather corny tale of a love that was meant to be." 

In Variety (magazine)|Variety, Emanuel Levy observed, "Its a testament to the nuanced writing of William Nicholson ... that the drama works effectively on both personal and collective levels ... Attenborough opts for modest, unobtrusive direction that serves the material and actors ... Hopkins adds another laurel to his recent achievements. As always, theres music in his speech and nothing is over-deliberate or forced about his acting ... Coming off years of desultory and unimpressive movies, Winger at last plays a role worthy of her talent." 

==Changes from the stage play or earlier television production==
The stage play opens with Lewis giving a talk about the mystery of suffering. This film opens with Lewis giving a radio broadcast about the sanctity of marriage.

In the stage play as in reality, Lewis and Davidman honeymoon in Greece. In the film, on their honeymoon they look for a "Golden Valley" in England, as depicted in a painting hanging in Lewis study.

As in the stage play, though not the earlier television film, Joy has only one son. In the original television film, as in reality, Joy had two sons, Douglas and David Gresham|David.

==Awards and honours==
*Academy Award for Best Actress (Debra Winger, nominee) Academy Award for Best Adapted Screenplay (nominee) BAFTA Alexander Korda Award for Best British Film (winner)
*BAFTA Award for Best Film (nominee)
*BAFTA Award for Best Actor in a Leading Role (Anthony Hopkins, winner)
*BAFTA Award for Best Actress in a Leading Role (Winger, nominee)
*BAFTA Award for Best Direction (nominee)
*BAFTA Award for Best Adapted Screenplay (nominee)
*National Board of Review Award for Best Actor (Hopkins, winner)
*Los Angeles Film Critics Association Award for Best Actor (Hopkins, winner) Southeastern Film Critics Association Award for Best Actor (Hopkins, winner)

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 