Seven Days (film)
{{Infobox film
| name           = Seven Days
| image          = Seven Days (film).jpg
| alt            = 
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =    
| rr             = Sebeun Deijeu
| mr             = Sebŭn Teijŭ}}
| director       = Won Shin-yeon
| producer       = Lee Seo-yeol   Jeong Yong-wook   Jeong Geun-hyeon   Im Choong-geun
| writer         = Yoon Jae-gu
| starring       = Yunjin Kim Park Hee-soon Kim Mi-sook
| music          = Kim Jun-seong
| cinematography = Choi Young-hwan
| editing        = Shin Min-kyung
| studio         = Prime Entertainment Yoon & Joon Films
| distributor    = Prime Entertainment
| released       =  
| runtime        = 125 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} crime Thriller thriller film directed by Won Shin-yeon, starring Yunjin Kim and Park Hee-soon.  

The film had 2,107,849 admissions nationwide and was the 9th most-attended domestic film of 2007.    In 2008, Kim won Best Actress at the Grand Bell Awards, and Park won Best Supporting Actor at the Blue Dragon Film Awards and Korean Film Awards. 

== Plot ==
Yoo Ji-yeon (Yunjin Kim) is a prominent lawyer, who has yet to lose a case. While Ji-yeon is taking part in a parents-only race at her daughters field day, her daughter disappears.

Later in the day, Ji-yeon receives a phone call from the man who abducted her daughter. The man makes it clear that he is not interested in her money. Rather, he tells her that the only way she will ever see her daughter again is to defend a five-time convicted felon who is appealing his conviction for rape and murder. Ji-yeon has only seven days before his trial ends.

== Cast ==
* Yunjin Kim as Yoo Ji-yeon, a lawyer
* Park Hee-soon as Kim Seong-yeol, a police detective
* Kim Mi-sook as Han Sook-hee, a professor of psychology
* Choi Moo-sung as Jeong Cheol-jin
* Jang Hang-sun
* Seo Dong-soo as Dr. Jo
* Shin Hyeon-jong as Mr. Oh
* Kwon Byeong-gil as Park Ki-bok
* Yang Jin-woo as Kang Ji-won, a rock singer
* Jung Dong-hwan as Kang Sang-man
* Lee Jeong-heon as a public prosecutor
* Oh Kwang-rok as Yang Chang-goo

== Awards and nominations == 2008 Asian Film Awards  Best Actress - Yunjin Kim

;2008 Baeksang Arts Awards
* Nomination - Best Actress - Yunjin Kim

;2008 Grand Bell Awards
* Best Actress - Yunjin Kim
* Best Sound - Lee Seung-chul, Lee Seong-jin
* Nomination - Best Film
* Nomination - Best Director - Won Shin-yeon
* Nomination - Best Supporting Actress - Kim Mi-sook
* Nomination - Best Cinematography - Choi Young-hwan
* Nomination - Best Music - Kim Jun-seong
* Nomination - Best Visual Effects - Yang Il-seok

;2008 Blue Dragon Film Awards
* Best Supporting Actor - Park Hee-soon
* Nomination - Best Film
* Nomination - Best Actress - Yunjin Kim
* Nomination - Best Director - Won Shin-yeon
* Nomination - Best Supporting Actress - Kim Mi-sook
* Nomination - Best Screenplay - Yoon Je-gu
* Nomination - Best Editing - Shin Min-kyung

;2008 Korean Film Awards
* Best Supporting Actor - Park Hee-soon
* Nomination - Best Actress - Yunjin Kim
* Nomination - Best Supporting Actress - Kim Mi-sook
* Nomination - Best Screenplay - Yoon Je-gu
* Nomination - Best Cinematography - Choi Young-hwan
* Nomination - Best Editing - Shin Min-kyung
* Nomination - Best Sound - Lee Seung-chul, Lee Seong-jin
==Remake==
Bollywood remake titled Jazbaa starring  Aishwarya Rai 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 