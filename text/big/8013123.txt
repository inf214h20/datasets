Black Swans (film)
{{Infobox film
| name           = Black Swans
| image          = Black Swans film poster.jpg
| caption        = Official poster
| director       = Colette Bothof
| producer       = Ralf Koot
| writer         = Arend Steenbergen
| starring       = Carice van Houten Dragan Bakema Mohammed Chaara
| music          = Han Otten
| cinematography = Richard Van Oosterhout
| editing        = Sander Vos
| distributor    = A-Film
| released       = 14 July 2005
| runtime        =
| country        = Netherlands English
| budget         = euro|€ 500,000
| preceded by    =
| followed by    =
}} 2005 Cinema Dutch drama drama film.
 Golden Calf for best sound at the Netherlands Film Festival in 2005.

==Plot==

Marleen (Carice van Houten) works as a volunteer in a rest home in Spain. When she meets Vince (Dragan Bakema) they are attracted to each other. Their passionate relationship has its ups and downs, and eventually Marleen ends up in a hospital. The film ends when Marleen walks into the sea.

==Production==
The film is set and filmed at the Costa de Almería in Spain in 2003. Due to financial problems the film was not released until 2005.

==Cast==
*Carice van Houten as Marleen
*Dragan Bakema as Vince
*Mohammed Chaara as Mo, friend of Vince

==External links==
* 
* 
* 

 
 
 


 
 