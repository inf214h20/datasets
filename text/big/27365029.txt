Face of a Fugitive
{{Infobox film
| name           = Face of a Fugitive
| image          = Facefugit2.jpg
| image_size     = 
| caption        = original film poster
| director       = Paul Wendkos
| producer       = David Heilweil Charles H. Schneer
| writer         = David T. Chantler Daniel B. Ullman Peter Dawson (story)
| narrator       = 
| starring       = Fred MacMurray James Coburn
| music          = Jerry Goldsmith
| cinematography = Wilfred M. Cline
| editing        = 
| studio         = Columbia Pictures
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film Luke Short). novelettes as well as 15 book length Western serials.   The working title was Justice Ends with a Gun. 

==Plot== Bank robber Marshal George Allison (Francis De Sales) who is taking Larsen aboard a train to begin a 5–10 year prison sentence.  Without animosity, Larsen tells the deputy that he will use his time in prison to plan more bank robberies.  This new robbery would be foolproof as Larsen feels that he was only caught by using a partner; the next time will be singlehanded.  Boarding the train, Larsen overpowers the deputy, takes his pistol and handcuffs one of his arms to the guardrail on the rear car of the train. Suddenly, Larsens younger brother Danny (Ron Hayes) unexpectedly comes from the train to free Larsen, even though Larsen was handling his escape fine. The elder Larsen chides Danny that he does not need help from anyone.

Danny explains that he has brought Jim a horse, and they flee. As they ride off the deputy produces a hidden derringer from an ankle holster, aiming at them. He mortally wounds Danny who kills the deputy.  The two board another train by hiding in the baggage car.  Jim explains his escape plans to Danny en route, but Danny dies.  Jim places his brothers corpse in a mail sack and throws it off a bridge passing over a river and vows to be alone in the future. 
 business suit which Danny brought, and Jim reboards the train as a passenger.  The only vacant seat is next to Alice (Gina Gillespie) a six-year-old girl who has been visiting her grandfather, an employee at the Enterprise mine.  The talkative Alice guesses that as she does not recognise Jim, he must be a visiting mining inspector.  Using the name Ray Kincaid, Larsen plays along with her guess and gathers information on the next town, Tangle Blue, Wyoming.  Mark Riley (Lin McCarthy), an earnest but inexperienced sheriff who is young Alices uncle, and a group of deputies stop the train to search for the deputys murderer, but they are satisfied with "Ray Kincaid the mining inspector" due to his travelling with Alice.  The deputies say that the wanted poster depicting the deputys murderer with Jim Larsens face will be arriving on the next days train and everyone will be checked entering or leaving the town.
 Dorothy Green). Alan Baxter) fencing off what he thinks is his land but which the government declares open range. 

Larsen/Kincaid uses his remaining money to have a shave then buy a horse and Horse tack|tack, a set of work clothes, and a pistol, belt and ammunition to replace the pistol he threw away during the search on the train.  Larsen/Kincaid finds all the roads away from Tangle Blue are guarded by deputies who are preventing anyone leave the town until the wanted posters come in. Returning to town and desperate for cash, he  decides to earn some money as being a deputy for Mark. 

Larsen/Kincaid attends a dance with Ellen who wants to leave Tangle Blue.  He proves his worth by stopping a showdown between Williams gang and Mark.  Escorting her and Alice home, they pass some deputies who have discovered Dannys unidentified body in the sack that the river has brought to town.  Though Larsen/Kincaid tries to avoid getting involved with Ellen, they fall in love. 

As part of his duties in enforcing the law, Mark cuts down Williams barbed wire fences, that Williams gang of toughs re-construct. Sheriff Mark reminds Larsen/Kincaid of Danny, and Mark is being menaced by Williams and his gang who threaten to kill Mark if he cuts down their fence one more time. Returning to town to drink, Williams and his gang menace Larsen/Kincaid who responds by beating up Williams, but Larsen/Kincaid is soon worked over by Williams gang.

The next day Larsen/Kincaid is the only deputy willing to go with Mark to cut down Williams fence.  Mark cuts down the fence then returns to town to meet the train arriving with the posters depicting the face of the fugitive leaving Larsen/Kincaid to watch the fence.  Though having the chance to escape, he notices one of Williams toughs Purdy (James Coburn) repairing the fence. Larsen re-cuts the barbed wire by shooting the strands with his Winchester rifle that sets the strands to wrap around Purdy.  He singlehandedly takes on Williams and his gang.

==Cast==
*Fred MacMurray as Jim Larsen/Ray Kincaid
*Lin McCarthy as Sheriff Mark Riley Dorothy Green as Ellen Bailey Alan Baxter as Reed Williams
*Myrna Fahey as Janet Hawthorne
*James Coburn as Purdy
*Gina Gillespie as Alice Bailey

==Production== Bell Moving Picture Ranch,  Corriganville (the ghost town)  and on the Sierra Railroad.   It also features an early film score by the prolific composer Jerry Goldsmith. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 