Satan's Brew
{{Infobox film
| name           = Satans Brew
| image          = Satansbrew.jpg
| alt            =  
| caption        = DVD cover
| director       = Rainer Werner Fassbinder
| producer       = 
| writer         = Rainer Werner Fassbinder
| starring       = Kurt Raab Margit Carstensen
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =  West Germany
| language       = German
| budget         = 
| gross          = 
}}
Satans Brew ( ) is a 1976 German film directed by Rainer Werner Fassbinder.

== Plot ==
Walter Kranz (Kurt Raab), a former revolutionary poet who attempts to base his life on that of Stefan George is lost for new ideas and penniless. Fortunately he can obtain a cheque from nymphomaniac Irmgart von Witzleben (Katharine Buchhammer) who wants to be threatened by a gun. Walter fires and disappears. Some more obscure things happen but in the end everybody is back on stage.

== Cast ==
* Kurt Raab : Walter Kranz
* Margit Carstensen - Andrée
* Helen Vita - Luise Kranz
* Volker Spengler - Ernst Kranz
* Ingrid Caven - Lisa
* Marquard Bohm - Rolf
* Y Sa Lo - Lana von Meyerbeer
* Ulli Lommel - Lauf
* Brigitte Mira - Walters mother
* Katherina Buchhammer - Irmgart von Witzleben  Armin Meier - Hustler

==External links==
* 
*   : Poem inspired by film at  

 
 
 
 

 
 