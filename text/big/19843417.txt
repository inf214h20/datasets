Aai (film)
{{Infobox film
| name           = Aai
| image          = Aai dvd.jpg
| alt            =  
| caption        = Official DVD Cover
| director       = A. Venkatesh (director)|A. Venkatesh
| producer       = K.Parthiban
| writer         = A. Venkatesh
| screenplay     = A. Venkatesh
| story          = A. Venkatesh
| based on       =   Sarath Kumar   Namitha   Vadivelu   Kalabhavan Mani Vincent Asokan                   
| music          = Srikanth Deva
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = December 17, 2004
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Sarath Kumar, Namitha, Vadivelu and Kalabhavan Mani. It was a successful film with a good soundtrack.

==Plot==
Aai is about a military officer who tries to cleanse the society of all bad elements. R. Sarath Kumar, gets agitated when he hears the word "aai", in this old wine in a new bottle story. 

The Movie opens with R. Sarath Kumar in a town in Tamil Nadu living happily with his Sister and friends. Sarath stays away from all wanted trouble and is forced to resort to fighting to defend his sister from the local Rowdy played by Vincent Asokan. A Flashback follows where it is revealed that he is a Military Officer and his sister is actually the sister of his best friend Kalabhavan Mani. It is also revealed that Sarath had maintained a low profile to protect his sister from his nemesis Kota Srinivasa Rao whom he had left crippled before he went into hiding.

How Sarath protects his sister and his love interest Namitha from Kota Srinivasa Rao and Vincent Asokan forms the rest of the story.

==Soundtrack==
*Aai Mailapur Mayila - Sabesh, Manicka Vinayagam, Suchitra
*Arjuna Arjuna - Udit Narayan
*Meyau Meyau - Srikanth Deva, Sadhana Sargam, Ganga
*Neathi Adida Aai - Sriram Parthasarathy, Kumar, Suchitra
*Ooru Onu Onuu - Vadivelu, R. Sarath Kumar, Anuradha Sriram, Paravai Muniyamma, Jayashri
*Veluthu Kathu - Shankar Mahadevan

==References==
 

 

 
 
 
 
 


 