What It Takes (film)
{{Infobox Film
| name           = What It Takes: A Documentary About 4 World Class Triathletes Quest for Greatness
| image          =
| director       = Peter Han
| cinematography = Andrew Ernst Peter Reid 
| producer       = Erik Blachford, Jason Costa, Rob Cromwell, Brian Hayes, Ben Heller, Jim Kalustian 
| editing        = Andrew Ernst, Michelle Odo 
| distributor    = WIT Group, LLC  
| released       = 2006 (U.S. release)
| runtime        = 96 min.  English 
}}
 documentary that Ironman triathletes through a year of training and preparation in advance of the 2005 Ironman World Championship in Kona, Hawaii|Kona, Hawaii.  

The film was shot digitally in native 16:9 widescreen using Panasonic AJ-SDX900 camcorders in 24 frames per second.  Filming began in November 2004 and completed in February 2006.

==External links==
*  
*  

 
 


 