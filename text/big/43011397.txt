Without Apparent Motive
{{Infobox film
| name           = Without Apparent Motive
| image          = Without Apparent Motive.jpg
| alt            = 
| caption        = 
| director       = Philippe Labro
| producer       = 
| writer         = 
| starring       = Jean-Louis Trintignant Carla Gravina Jean-Pierre Marielle Dominique Sanda
| music          = Ennio Morricone
| cinematography = Jean Penzer
| editing        = 
| studio         = 
| distributor    = 
| released       =   	
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Without Apparent Motive ( ) is a 1971 French thriller film directed by Philippe Labro.

==Cast==
* Jean-Louis Trintignant - Stéphane Carella
* Carla Gravina - Jocelyne Rocca
* Laura Antonelli - Juliette Vaudreuil 
* Jean-Pierre Marielle - Perry Rupert Foote
* Dominique Sanda - Sandra Forest
* Sacha Distel - Julien Sabirnou
* Stéphane Audran - Hélène Vallée
* Paul Crauchet - Francis Palombo
* Erich Segal - Hans Kleinberg

==External links==
*  

 
 
 
 
 


 
 