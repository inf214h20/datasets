St. Val's Mystery
 
 
 
 

{{Infobox film
| name           = St. Vals Mystery
| image          = St. Vals Mystery film poster.jpg
| alt            = 
| caption        = French language poster
| film name      = ( )
| director       = René Le Hénaff
| producer       = Édouard Harispuru
| writers        =  
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       =  
| music          = René Sylviano
| cinematography = Victor Arménise
| editing        = Marinette Cadix
| studio         = Studios de Boulogne
| distributor    = Compagnie Commerciale Française Cinématographique
| released       =  
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 2,397,153 admissions (France) 
}} comedy film starring Fernandel directed by René Le Hénaff,         Shot during the winter of 1944-1945 in the studios of Boulogne, this was the Fernandels first film following the liberation of Paris.      

The films original release title is Le mystère Saint-Val, and it was released in the United States in 1945 under the English title of St. Vals Mystery. It was then released in Denmark on October 23, 1950 as Det mystiske slot and in Portugal on June 1, 1954 as Fernandel, Polícia Amador.

==Plot==
An insurance-office clerk Désiré Le Sec (Fernandel) dreams of being a great detective. The clerks uncle (Marcel Carpentier) is his boss at that agency, and sends Désiré out on a frivolous mission to Saint-Val Castle, where the master of places has been found dead through mysterious circumstances.  Désiré uncovers a real life murder and becomes mixed up with the murder case, ending up spending a night in the forbidding and spooky old Saint-Val castle.

==Cast==
 
* Fernandel as Désiré Le Sec
* Jean Davy as Max Robertal
* Marcel Carpentier as Loncle de Désiré
* Marcel Pérès as Le brigadier
* Erno Crisa as Le vagabond 
* Jean Dasté as Lhuissier
* Alexandre Rignault as Antoine
* Pierre Renoir as Dartignac
* Paul Demange
* Viviane Gosset as Suzy
* Arlette Guttinguer as Rose
* Germaine Kerjean as Madame De Saint-Val		
* Maxime Fabert
 

==Reception==
The film was a big hit in France, recording admissions of 2,397,153.   at Box Office Story 
 Ten Little Indians but with a  "decidedly unfunny comic twist".   When seen with the now-removed musical numbers it contained in its original release, the film "was probably more digestible".  Summarizing, Travers felt the gags were predictable, the plot "hackneyed and pedestrian", and the "unimaginative pay-off definitely does not reward" the viewer.,  concluding that this marked the film as "clearly not Fernandels finest hour".   

==References==
 

==External links==
* 
*  at Films de France

 