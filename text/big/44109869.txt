Kanikanum Neram
{{Infobox film 
| name           = Kanikanum Neram
| image          = kanikaanumneram.png
| caption        =
| director       = Rajasenan
| producer       = Augustine Prakash
| writer         = Joseph Vattoli Rajasenan (dialogues)
| screenplay     = Rajasenan Sunitha Nedumudi Venu Ratheesh Saritha Vineeth
| music          = A. T. Ummer
| cinematography = N Karthikeyan
| editing        = G Murali
| studio         = Super Productions
| distributor    = Super Productions
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, directed by Rajasenan and produced by Augustine Prakash. The film stars Sunitha (actress)|Sunitha, Nedumudi Venu, Ratheesh and Saritha in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Sunitha as Indu
*Nedumudi Venu as Sethu
*Ratheesh as Raghu
*Saritha as Savithri
*Vineeth as Vinu
*Sadiq as Nandhu Vincent as Balagopalan
*KPAC Sunny as Parambil Keshavan
*Oduvil Unnikrishnan as Mathukutty
*Aranmula Ponnamma as Muthassi
*Alex as Shivan
*Jose Prakash as Parameshwan Menon 
*Mafia Sasi as Gunda

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nunakkuzhikkavilil || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Vaasantha Chandrikayo || KS Chithra || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 