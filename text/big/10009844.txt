Red Planet Mars
{{Infobox film
| name           = 
| image          = Red Planet Mard Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Harry Horner
| producer       = Donald Hyde Anthony Veiller
| screenplay     = Anthony Veiller John L. Balderston
| based on       =   Peter Graves Andrea King Orley Lindgren Walter Sande Marvin Miller
| music          = Mahlon Merrick
| cinematography = Joseph Biroc
| editing        = Francis D. Lyon
| distributor    = United Artists
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Peter Graves and Andrea King and was directed by art director Harry Horner in his directorial debut. 

==Plot==
An American astronomer obtains images of Mars suggesting large-scale environmental changes are occurring at a pace that can only be accomplished by intelligent beings with advanced technology. At the same time a colleague claims to have been contacting Mars by radio, using technology stolen from the Nazis after World War II. He communicates first through an exchange of mathematical concepts, like the value of pi, and then through answers to specific questions about Martian life. The transmissions claim that Mars is a utopia fueled by nuclear power, which has led to great technological advancement and the elimination of scarcity, but that there is no fear of nuclear war. 

This revelation leads to political and economic chaos, especially in the Western hemisphere, and is said to have "done more to smash the democratic world in the last four weeks than the Russians have been able to do in eleven years."  The United States|U.S. government imposes a news blackout and orders the transmissions to stop due to fears that the Soviet Union could pick up and decode their messages. This ends when the next message reveals that the Earth is condemned to the constant fear of nuclear war as a punishment for straying from the teachings of the Bible. Revolution sweeps the globe, including the Soviet Union, which is overthrown and replaced by a theocracy, which is met with celebration in America.
 Nazi who developed the original communication device prototype wants to announce that he has been duping the Americans with false messages from a secret Soviet-funded radio transmitter high in the Andes mountains of South America. He says that he transmitted the original messages supposedly from Mars, but that the United States government made up the religious messages, which he allowed because he wanted to see the destruction of the Soviet Union. The mystery thickens as it appears the messages may have continued even after the secret transmitter was destroyed in an avalanche, but the American transmitter is blown up before the message can be received.

==Cast== Peter Graves as Chris Cronyn
* Andrea King as Linda Cronyn
* Herbert Berghof as Franz Calder
* Walter Sande as Admiral Bill Carey Marvin Miller as Arjenian
* Willis Bouchey as President
* Morris Ankrum as Secretary of Defense Sparks
* Orley Lindgren as Stewart Cronyn
* Bayard Veiller as Roger Cronyn Robert House Peters, Jr. as Dr. Boulting, Mitchells assistant

==Reception==
===Critical response===
When the film was released, the staff at Variety (magazine)|Variety liked the film, writing, "Despite its title, Red Planet Mars takes place on terra firma, sans space ships, cosmic rays or space cadets. It is a fantastic concoction   delving into the realms of science, politics, religion, world affairs and Communism...Despite the hokum dished out, the actors concerned turn in creditable performances." 

The New York Times, while giving the film a mixed review, wrote well of some of the performances, "Peter Graves and Andrea King are serious and competent, if slightly callow in appearance, as the indomitable scientists. Marvin Miller is standard as a top Soviet agent, as are Walter Sande, Richard Powers and Morris Ankrum, as Government military men, and Willis Bouchey, as the President. " 

More recently, critic Bruce Eder also praised the film, writing, "Red Planet Mars is an eerily fascinating artifact of the era of the Red Scare, and also the first postwar science fiction boom, combining those elements into an eerie story that is all the more surreal because it is played with such earnestness." 

Yet, film critic Dennis Schwartz recently panned the film, writing, "One of the most obnoxious sci-fi films ever. It offers Hollywoods silly response to the 1950s Red Scare sweeping the country and promoted by the McCarthy senate hearings looking for commies under every bed cover. To realize how dumb this Cold War film is, try this question of the plots summary on for size: Can it be that the Martians are signaling Earth and that their leader is actually uttering the very word of God? This is one of those really bad propaganda films that has no entertainment value, as it shows how paranoic this country can be and how it can use religion at the drop of a radio signal to promote materialism and Christianity as a superior way of life than communism. This one might be the strangest and most twisted Red Menace films of all time. It ends with a hydrogen explosion in the lab killing two good American scientists and one lousy ex-Nazi scientist now working for the Russian Communists. The last message heard from Mars is an abbreviated one (thank God!): Ye have done well my good ... then there is just silence. The film leaves one with the impression that Mars is ruled by God."
 

==References==
 

== External links ==
*  
*  
*  
*   at the official Andrea King web site
*  

 
 
 
 
 
 
 
 
 
 