Living Is Easy with Eyes Closed
{{Infobox film
| name           = Living Is Easy with Eyes Closed
| image          = Living Is Easy with Eyes Closed.jpg
| caption        = Theatrical release poster
| director       = David Trueba
| producer       = Cristina Huete
| writer         = David Trueba
| narrator       = 
| starring       =  
| music          = Pat Metheny
| cinematography = Daniel Vilar
| editing        = Marta Velasco
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}} 
Living Is Easy with Eyes Closed ( ) is a 2013 Spanish comedy-drama film written and directed by David Trueba, and starring Javier Cámara. The films title comes from a line in the song "Strawberry Fields Forever", from The Beatles. 
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.      

==Plot==
It is 1966 in Albacete, an English teacher and die-hard Beatles fan Antonio (played by Javier Cámara) decides to go on a road trip to Almería in the hope of meeting John Lennon, who is shooting How I Won the War there under the direction of Richard Lester. On the way he picks up two hitch-hikers, Juanjo (Francesc Colomer) and Belén (Natalia de Molina), and the unlikely trio follow their dreams and look for their own freedom. 

==Cast==
* Javier Cámara as Antonio
* Natalia de Molina as Belén
* Francesc Colomer as Juanjo
* Jorge Sanz as Juanjos father

==Awards==
{| class="wikitable"
! Awards !! Category !! Nominated !! Result
|- Goya Awards Best Film
|  
|- Best Director
| David Trueba
|  
|- Best Actor
| Javier Cámara
|  
|-
| Best New Actress
| Natalia de Molina
|  
|-
| Best Original Screenplay
| David Trueba
|  
|-
| Best Original Score
| Pat Metheny
|  
|-
| Best Costume Design
| Lala Huete
|  
|- Premios Feroz
| colspan=2 | Best Comedy
|  
|-
| Best Director
| David Trueba
|  
|-
| Best Screenplay
| David Trueba
|  
|-
| Best Main Actor
| Javier Cámara
|  
|-
| Best Supporting Actress
| Natalia de Molina
|  
|-
| Best Original Soundtrack
| Pat Metheny
|  
|}

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 