Five Golden Flowers
{{Infobox film
| image         = Five Golden Flowers Poster.jpg
| name          = Five Golden Flowers
| caption       = 
| writer         = Zhao Jikang Wang Gongpu
| starring       = Mo Zijiang Yang Likun
| director       = Wang Jiayi
| studio         = Changchun Film Group Corporation China
| released       = 1959
| runtime        = 95 minutes Mandarin 
| budget         =
}}
Five Golden Flowers ( ) is a Chinese romantic musical film released in 1959 in film|1959.

== Synopsis ==
 ]] Jinhua (which is a common Chinese name for both sexes), and come across four other women named Jinhua — a steelworker, a tractor driver, a herder and a fertilizer maker — before he eventually finds his love, who works as a commune director. After some initial confusion, the two get married and there is a final chorus under the Butterfly Spring.    

== Modern Interpretation ==

Five Golden Flowers is a canonical example of films produced by state-run production companies in China during the 1950s and 60s depicting ethnic minorities in society. The Bai minority featured in Five Golden Flowers is shown to wear colourful costumes, engage in festive song and dance, and in romance, yet they are fully committed to socialist construction.    The filmmakers took care to use folk songs from the region and hire a Yunnanese leading lady.  The film is of particular interest because the portrayal of ethnic minorities allowed filmmakers to probe areas deemed too sensitive by the Chinese government.  

== References ==

 

== External Links ==

*  


 
 
 
 