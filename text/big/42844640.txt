What a Man (1930 film)
 
{{Infobox film
| name = What a Man
| image =
| caption =
| director = George Crone
| producer =
| writer = Courtenay Savage  (play) E.J. Rath (novel) Harvey Gates A.A. Kline
| narrator = Reginald Denny Harvey Clark Lucille Ward
| music =
| cinematography = Arthur L. Todd
| editing = Harry Chandlee
| studio = Sono Art-World Wide Pictures
| distributor =   Sono Art-World Wide Pictures
| released =  
| runtime = 72 minutes
| country = United States English
| budget =
| gross =
| website =
}} Reginald Denny, Harvey Clark. It was an adaptation of the play They All Want Something by Courtenay Savage, which was itself based on a novel by E.J. Rath. A separate Spanish language version Thus Is Life was made at the same time.  The film was remade in 1938 as Merrily We Live. It is also known by the alternative title The Gentleman Chauffeur.

==Plot==
A British ex-Grenadier Guards officer moves to America, but struggles to find work. After he is employed as a chauffeur to a wealthy family, he falls in love with his employers daughter.

==Cast== Reginald Denny as Wade Rawlins  
* Miriam Seegar as Eileen Kilbourne  Harvey Clark as Mr. Kilbourne 
* Lucille Ward as Mrs. Kilbourne  
* Carlyle Moore Jr. as Kane Kilbourne  
* Anita Louise as Marion Kilbourne  
* Norma Drew as Elsie Thayer 
* Christiane Yves as Marquise de la Fresne   Charles Coleman as William, the Butler 
* Greta Granstedt as Hanna, the Maid

==References==
 

==Bibliography==
* Waldman, Harry. Hollywood and the Foreign Touch: A Dictionary of Foreign Filmmakers and Their Films from America, 1910-1995. Scarecrow Press, 1996. 

==External links==
* 

 
 
 
 
 
 

 