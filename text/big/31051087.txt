POM Wonderful Presents: The Greatest Movie Ever Sold
{{Infobox film
| name = POM Wonderful Presents: The Greatest Movie Ever Sold
| image = The Greatest Movie Ever Sold Poster.jpg
| caption = Theatrical release poster
| director = Morgan Spurlock
| producer = Jeremy Chilnick Keith Calder Abbie Hurewitz Morgan Spurlock Jessica Wu
| writer = Jeremy Chilnick Morgan Spurlock
| starring = Morgan Spurlock J.J. Abrams Jimmy Kimmel
| music = Jon Spurney
| editing = Thomas M. Vogt
| cinematography = Daniel Marracino
| distributor = Sony Pictures Classics Stage 6 Films
| released =  
| runtime = 88 minutes 
| country = United States
| language = English
| budget = $1.5 million
| gross = $700,000 approx. (USA) (28 August 2011)
| italic title = force
}}
POM Wonderful Presents: The Greatest Movie Ever Sold (commonly shortened to The Greatest Movie Ever Sold) is a 2011 documentary film about product placement, marketing and advertising directed by Morgan Spurlock. The premise behind the production is that the documentary itself would be entirely paid for by sponsors, thus being a form of metafilm. The films slogan is "Hes not selling out, hes buying in".

==People appearing in the film==
Besides a great number of sponsoring and non-sponsoring corporate figures appearing in the film, others include:
 
*J. J. Abrams, filmmaker 
*Peter Bemis of Bemis/Balkind
*Peter Berg, filmmaker
*Big Boi from Outkast
*Noam Chomsky, Professor Emeritus of Linguistics at Massachusetts Institute of Technology
*Bob Garfield, co-host on On the Media
*Sut Jhally, Professor of Communication, University of Massachusetts Amherst
*Britt Johnson of mediaplacement
*Matt Johnson and Kim Schifino of Matt & Kim Kao Brands Company
*Gilberto Kassab, mayor of São Paulo, Brazil
*Jimmy Kimmel and a "live" show of Jimmy Kimmel Live!
*Richard Kirshenbaum of Kirshenbaum Bond Senecal + Partners
*Jon Bond of Kirshenbaum Bond Senecal + Partners
*Damian Kulash of OK Go
*Rick Kurnit, a lawyer specializing in advertising law Michael Levine of Levine Communications Office,Inc.
*Martin Lindstrom of Buyology Inc.
*Susan Linn, Author of Consuming Kids
*Mark Crispin Miller
*Regina Monteiro, Director of Urban Planning, City of São Paulo, Brazil
*Ralph Nader, consumer advocate 
*Brett Ratner, film director and producer  Island Def Jam Music
*Lynda Resnick, Owner/CEO of POM Wonderful 
*Tony Seiniger of Seiniger Advertising
*Stan Sheetz of Sheetz|Sheetz, Inc 
*Ben Silverman of Electus and formerly of NBC Entertainment
*Brian Steinberg, TV Editor of Advertising Age
*Quentin Tarantino, screenwriter/director 
*Donald Trump
*Matt Tupper of POM Wonderful
*David Art Wales of Ministry of Culture marketing consultancy
*Robert Weissman of Public Citizen
*Faris Yakob of kbs+p
*Lindsay Zaltman of Olson Zaltman Associates
 

==Release==
The film premiered at the Sundance Film Festival in January 2011. In the United States, the film had a limited release, opening on April 22, 2011 in
New York City, Los Angeles, San Francisco, Chicago, Washington D.C., Boston,  Philadelphia, San Diego, Phoenix, Arizona|Phoenix, and Austin, Texas.   The film opened the 2011 Hot Docs Canadian International Documentary Festival on April 28, 2011. 

==Reception==
The film received 72% positive reviews from film critic on the review aggregate website Rotten Tomatoes, with an average rating of 6.2 out of 10.  On Metacritic, the film received a score of 66 out of 100. 

Stephen Holden called the film "even more amusing than Super Size Me", pointing out that "more than once the movie shows Mr. Spurlock, armed with clever storyboards, selling his ideas with an enthusiasm and skill that would put Don Draper of Mad Men to shame." 

==Sponsors== final cut. billing  media impressions.   As of 26 August 2011, the film had an estimated gross of approximately US$638,476.  

Mane n Tail is featured prominently in the film,  but the films end titles disclose that they did not pay for the promotion. Instead, Mane n Tail provided free products to be used in the production of the film. Many other brands only provided products or contributed to the promotion of the movie. 

For 60 days, beginning April 27, 2011, the city of Altoona, Pennsylvania (home of Sheetz, one of the movies major sponsors) officially changed its name to "POM Wonderful Presents: The Greatest Movie Ever Sold, Pennsylvania" to help Spurlock promote the film, and received $25,000 for doing so. 

Other sponsors include:
  
* Amys Kitchen 
* The Aruba Tourism Authority    Ban
* Carrera Sunglasses
* Get It For Free Online
* Hyatt 
* JetBlue  
* KDF Car Wraps Merrell  Mini 
* Solstice Sunglass Boutique Trident
* Carmex
* MovieTickets.com
* Old Navy
* Ted Baker
* Petland Discounts
* Seventh Generation Inc.
* Sheetz  
* Thayers
 

==Music==
{{Infobox single
| Name           = The Greatest Song I Ever Heard
| Cover          = The-greatest-song-i-ever-heard.jpg
| Artist         = OK Go
| from Album     = The Greatest Soundtrack Ever Sold
| Released       = 1 April 2011 Digital download
| Recorded       = 2011 Pop
| Length         = 3:59
| Label          = Paracadute
| Writer         =
| Producer       =
| Certification  =
| Last single    = "White Knuckles" (2010)
| This single    = "The Greatest Song I Ever Heard" (2011)
| Next single    = "All Is Not Lost" (2011)
}}
Spurlock used Brooklyn dance-punk duo Matt and Kims music as the movie soundtrack and their song "Cameras" from the album Sidewalks (album)|Sidewalks for the opening credits. Band members Matt Johnson and Kim Schifino are also interviewed in the film. 

The films theme song is called "The Greatest Song I Ever Heard" by the rock band OK Go. The song was made available for download on 1 April 2011.  OK Go performed the song at the films premiere and at promotions for the film.  They also appeared live on The Late Late Show with Craig Ferguson performing the song. 

A soundtrack album was released to digital music stores on April 26, 2011, and on CD August 23, 2011.

{{Track listing
| headline        = The Greatest Soundtrack Ever Sold
| extra_column    = Performer(s)
| total_length    = 
| writing_credits = 
| title1          = CameraBuggin
| writer1         = 
| extra1          = Big Boi vs. Matt & Kim
| length1         = 3:33
| title2          = The Greatest Song I Ever Heard
| writer2         = 
| extra2          = OK Go
| length2         = 3:57
| title3          = Ill Never Love Again
| writer3         = 
| extra3          = Taio Cruz
| length3         = 3:49 Run On
| writer4         = 
| extra4          = Moby
| length4         = 3:43
| title5          = Cameras
| writer5         = 
| extra5          = Matt & Kim
| length5         = 3:33
| title6          = Sacramento
| writer6         = 
| extra6          = Oxygen
| length6         = 3:48
| title7          = Greatest Spurlock Spoken Word (You Ever Heard)
| writer7         = 
| extra7          = Spurlock and his People
| length7         = 2:50
| title8          = Brand Search
| writer8         = 
| extra8          = Jonathan Spurney
| length8         = 3:14
| title9          = Cha Cha Cha
| writer9         = 
| extra9          = Eric V. Hachikian
| length9         = 3:01
| title10          =  
| writer10         = 
| extra10          = Alexander Gibson & The London Festival Orchestra
| length10         = 2:15
}}

==References==
{{reflist| refs=
   
   
}}

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 