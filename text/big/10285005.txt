Jugnu (1947 film)
{{Infobox Film
| name           = Jugnu जुगनू  
| image          = Jugnu 1947 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Shaukat Hussain Rizvi
| producer       = Shaukat Art Production
| writer         = 
| narrator       =  Nurjehan Dilip Kumar
| music          = Feroz Nizami
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1947
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Indian film directed by Shaukat Hussain Rizvi. The film stars Noor Jehan, Dilip Kumar, Gulam Mohd., Sulochana (Sr.), Latika, Zia, Jilloo, Agha, Shashikala and now famous playback singer Mohammed Rafi.  This film was the first major hit for Dilip Kumar who went on to become one of Indian film industrys legends. 

Both Shaukat Hussain Rizvi and his wife Noor Jehan settled in Pakistan shortly after the film was released.  

==Plot==
Sooraj is from a wealthy household. He meets Jugnu when hes tried to commit suicide by jumpung off a cliff and jugnu tries to stop him. In reality it was a ply conceived by sooraj and his friends to distract jugnu and all the girls so that they can steal the food prepared by the girls. All of them study in the same college and live in hostel. And thus slowly on meeting few times Jugnu and Sooraj fall in love with each other. Jugnu is orphaned when she was young and is taken care of by a friend of her fathers who wishes for her to be married to his son Dilip. Soorajs parents come to know about his love for jugnu and oppose the match as jugnu is poor. One day in conversation with his father sooraj comes to know that they are not rich as all the assets (bungalow, things etc) owned by his father are mortgaged under heavy loan and so he plans to arrange marriage of Sooraj to a rich household so that the dowry received will help them pay off loan and continue living a wealthy life. Sooraj is surprised to hear this but he nevertheless is happy as now he can marry Jugnu as both are poor. But Soorajs mother goes to jugnu and pleads for her sons life explaining her predicament and jugnu promises her that she will get out of soorajs life. What happens next is the central plot of the story. Will sooraj and jugnu meet again? WIll the difference be sorted between them? Will the lovers love be sacrificed for parents happiness and money?

This movie was the first hit of Dilip Kumar. He gives a fabulous performance as Sooraj and perhaps this is starting of his claim to fame of being "Tragedy king".

==Cast==
* Dilip Kumar as Sooraj
* Noor Jehan
* Shashikala Ghulam Mohammed
* Ruby Mayer as Sulochana Agha
* Latika Zia
* Mohammad Rafi as College Boy
* Jillo

==References==
  

== External links ==
*  

 
 ,
 

 