Sunflower (1970 film)
{{Infobox film
| name           = I girasoli
| image          =Igirasoli film poster.jpg
| image size     =
| caption        =Theatrical release poster
| director       = Vittorio De Sica
| producer       = Arthur Cohn Joseph E. Levine Carlo Ponti
| writer         = Tonino Guerra Giorgi Mdivani Cesare Zavattini
| narrator       =
| starring       = Marcello Mastroianni Sophia Loren Lyudmila Savelyeva
| music          = Henry Mancini
| cinematography = Giuseppe Rotunno
| editing        = Adriana Novelli
| distributor    =
| released       =  
| runtime        = 101 minutes
| country        = Italy
| language       = Italian Russian
| budget         =
}}
 1970 Cinema Italian drama western movie to be filmed in the Soviet Union|USSR.

== Plot ==

"A woman born for love. A man born to love her. A timeless moment in a world gone mad."

Giovanna (Sophia Loren) and Antonio (Marcello Mastroianni) get married to delay  Antonios deployment during World War II.   After that buys them twelve days of happiness, they try another scheme, in which Antonio pretends to be a crazy man.  Finally, Antonio is sent to the Russian Front.  When the war is over, Antonio does not return and is listed as missing in action.  Despite the odds, Giovanna is convinced her true love has survived the war and is still in Russia.  Determined, she journeys to Russia to find him.

In Russia, Giovanna visits the sunflower fields, where there is supposedly one flower for each fallen Italian soldier, and where the Germans forced the Italians to dig their own mass graves. Eventually, Giovanna finds Antonio, but by now he has started a second family with a woman who saved his life, and they have one daughter. Childless, having been faithful to her husband, Giovanna returns to Italy, heartbroken, but unwilling to disrupt her loves new life.  Some years later, Antonio returns to Giovanna, asking her to come back with him to Russia. Meanwhile, Giovanna has tried to move on with her own life, moving out of their first home together and into her own apartment. She works in a factory and is living with a man, with whom she has a baby boy. Antonio visits her and tries to explain  his new life, how war changes a man, how safe he felt with his new woman after years of death. Unwilling to ruin Antonios daughters or her own new sons life, Giovanna refuses to leave Italy, expressing an intense emotional maturity in her choice. As they part, Antonio gives her a fur, which he had promised years before that hed bring back for her. The lovers lock eyes as Antonios train takes him away from Giovanna, and from Italy, forever.

== Cast ==

* Sophia Loren – Giovanna
* Marcello Mastroianni – Antonio
* Lyudmila Savelyeva – Masha (Maria)
* Galina Andreyeva – Valentina, Soviet official
* Anna Carena – Antonios Mother
* Germano Longo – Ettore
* Nadya Serednichenko – Woman in sunflower fields
* Glauco Onorato – Returning soldier
* Silvano Tranquilli – Italian worker in Russia
* Marisa Traversi – Prostitute
* Gunars Cilinskis – Russian Ministry Official
* Carlo Ponti, Jr. – Giovannas Baby
* Pippo Starnazza – Italian official
* Dino Peretti
* Giorgio Basso

== Awards ==

* David di Donatello: Best Actress (Sophia Loren) Best Music, Original Score

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 