Kazhumaram
{{Infobox film 
| name           = Kazhumaram
| image          =
| caption        =
| director       = AB Raj
| producer       =
| writer         =
| screenplay     =
| starring       = Sukumaran Sumalatha Sukumari Balan K Nair
| music          = Shankar Ganesh
| cinematography =
| editing        =
| studio         = SVS Films
| distributor    = SVS Films
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by AB Raj. The film stars Sukumaran, Sumalatha, Sukumari and Balan K Nair in lead roles. The film had musical score by Shankar Ganesh.   

==Cast==
*Sukumaran
*Sumalatha
*Sukumari
*Balan K Nair
*Kottarakkara Sreedharan Nair

==Soundtrack==
The music was composed by Shankar Ganesh and lyrics was written by Bichu Thirumala and Traditional. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kallavaattinoppam || SP Balasubrahmanyam || Bichu Thirumala || 
|-
| 2 || Muthuppanthal mullappanthal || P Jayachandran, Vani Jairam || Bichu Thirumala || 
|-
| 3 || Oru Thamburu Naadasarovaram || Unni Menon || Bichu Thirumala || 
|-
| 4 || Slokam ||  || Traditional || 
|-
| 5 || Thiramaalakal moodiya || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 
 

 