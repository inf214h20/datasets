Hotel for Dogs (film)
{{Infobox film
|name=Hotel for Dogs
|image=hotel_for_dogs.jpg
|caption=Theatrical release poster
|director=Thor Freudenthal
|producer=Lauren Shuler Donner Ewan Leslie Jonathan Gordon Jason Clark Marina Fabre
|screenplay=Jeff Lowell Mark McCorkle Bob Schooley based on= 
|starring=Emma Roberts Jake T. Austin Troy Gentile Kyla Pratt Johnny Simmons Lisa Kudrow Kevin Dillon Don Cheadle
|music=John Debney
|cinematography=Michael Grady
|editing=Sheldon Kahn DreamWorks Pictures Nickelodeon Movies Cold Spring Pictures The Montecito Picture Company The Donners Company Mavrocine
|distributor=Paramount Pictures
|released= 
|runtime=100 minutes
|country=United States
|language=English
|budget=$75 million
|gross=$117,000,198 	  
}} family comedy novel of guardians tell them that pets are forbidden at their home. They also take in other dogs to avoid the dogs being taken away by two cold hearted animal pound workers and police officers.
 DreamWorks Pictures Universal City, California.    The dogs in the film were trained for several months before shooting. Nearly 80 boys auditioned for the role of Bruce before Austin was ultimately selected.   

The film was released in the United States on January 16, 2009, and grossed approximately $17 million in its opening weekend in 3,271 theaters.

==Plot==
 
While in Central City, Louisiana, orphans Andi (Emma Roberts) and Bruce (Jake T. Austin) manage to sell a rock in a box to a pawn shop for twenty-seven dollars and steal an ice-cream truck, in order to feed their dog, Friday. However, they are quickly caught and marched off to the police station, where their social worker, Bernie Wilkins (Don Cheadle) picks them up and takes them back to their foster parents, Lois and Carl Scudder (Lisa Kudrow and Kevin Dillon, respectively), who appear not to care for either Andi or Bruce. When the two demand to know where their foster children have been, Bernie covers for them, telling them that it was his fault they were late. Despite his sympathies towards the two siblings, he warns them that they are playing a dangerous game by deliberately getting into trouble in order to escape Carl and Lois, since they could get fostered separately, something that both orphans are desperate to avoid. During Bernies visit, Friday returns home, and Bruce quickly bustles Bernie out of the door, since Fridays presence is to be kept secret.

The next morning, Andi and Bruce are horrified to find that Friday has snuck downstairs and is dangerously close to Lois discovering him.  pet shop to ask if anybody has seen him; there, they meet Dave, (Johnny Simmons) and Heather, (Kyla Pratt) who advise them to check the dog pound|Pound. Upon learning that Friday is indeed in the Pound but they cannot claim him unless their parents are there, Andi pays for Fridays return and suggests to Bruce that they find Friday a new home, since he deserves a real home. Whilst walking home, they find a gang of youths breaking into an abandoned hotel, only to flee when the police arrive; since Andi and Bruce are the only ones left at the scene, Andi urges Bruce to hide in the hotel. Whilst looking for Friday, who had curiously begun to explore the hotel, they find a tiny Boston Terrier and an English Mastiff, whom they name Georgia and Lenny, respectively. Confident that the three dogs get along, Andi and Bruce leave Friday at the hotel for the night.

The next morning, Andi and Bruce return to the hotel, with Andi warning her younger brother to not get too attached to the two stray dogs. Whilst Bruce stays at the hotel to keep the dogs quiet and creates a machine that allows them to play Fetch whenever they like, Andi heads to the pet shop to get some food for the strays, claiming that her parents rescue dogs, prompting Dave to ask her to take in three dogs-Shep, Romeo and Cooper-who nobody seems to want to adopt. Andi begrudgingly agrees.  Upon arriving at the hotel, Dave and Heather immediately agree to help the two siblings out. With six dogs to now look after, the friends set about trying to get the hotel in a decent enough state to suit the dogs needs, including building running machines, automatic feeders and a car simulator. Whilst theyre working, a local boy named Mark offers his assistance as more stray dogs begin to occupy the hotel.

Back at the Scudders household, Bernie eagerly tells them of a couple of new foster parents hes found Andi and Bruce. However, their new foster parents live hours away, the two turn the offer down in order to continue looking after the dogs, much to Bernies bewilderment. Whilst thinking over her decision, Dave invites Andi to a party, and she happily accepts; Mark, meanwhile, makes various efforts to gain Heathers attention. Whilst everyone else is at the party, Bruce is caught stealing a hairdryer from Lois, and is immediately interrogated by his foster parents; meanwhile, Andi bumps into an old acquaintance who accidentally notifies everyone that Andi is an orphan. Bruce manages to escape his house, only to find the hotel in a state; Lois and Carl follow him and the police are called-the dogs are quickly caught and sent to the Pound, while Bruce is taken away by two police officers. When Lois and Carl refuse to take Andi and Bruce back, Bernie is forced to send them to separate foster homes. Bernie, however, later discovers that they left one dog behind and begins to see why the kids loved doing what they did.

With all of the dogs to be put down the next day, Friday manages to escape his captivity and rushes over to find Dave, Heather and Mark, who in turn find Andi. They all hurry over to find Bruce. Meanwhile, Bernie decides to look around the hotel, where he finds a beagle who managed to avoid capture, and marvels at the creations Bruce made. Andi and Bruce manage to break into the Pound and release the dogs, where Bruce entices the horde of dogs to follow Daves van through the city with sausages. The strange event attracts the attention of the police, who follow the dogs to the hotel, where Bernie is waiting. With a large crowd gathered outside, Bernie tells the crowd about how Bruce, Andi and everyone else managed to create a family of dogs and lists the names of the dogs who live at the hotel, winning the publics hearts. The dog catchers are arrested, the pound is shut down and the police who always kept a close eye on the kids, allow to keep the dogs together.

As people eagerly explore the hotel, Bernie reveals to an overjoyed Andi and Bruce that he and his wife have decided to adopt them. The hotel re-opens as a grand Hotel For Dogs, where people can either adopt strays or board their dogs. Although Lois and Carl are invited to provide the entertainment for the Dog Lounge, the dogs quickly grow bored with their act, and the two are sent offstage in disgrace. Meanwhile, Andi, Bruce and Friday begin to happily settle into their new family.

==Cast==
* Emma Roberts as Andi, an orphan who, along with her younger brother Bruce, has lived with a number of foster parents before ending up with Lois and Carl Scudder. Roberts was cast in late 2007    and "knew as soon as soon as she started reading the script for Hotel for Dogs, she wanted to be part of the movie".   
* Jake T. Austin as Bruce, Andis younger brother who has a knack for mechanics. His inventions help keep the stray dogs living at the hotel fed and entertained while he and his sister are away. Nearly 80 boys tried out for the part before Austin was cast as Bruce. stray dogs and care for them at the hotel.
* Don Cheadle as Bernie Wilkins, a sympathetic social worker who tries his best to find a home for Andi and Bruce and to prevent them from being placed with separate foster families. Cheadle joined the cast in September 2007.    He described the film as "an opportunity to do a film that my kids can see" and praised both Emma Roberts and Jake T. Austin as being "really professional. They showed up to do the work and were serious and took it seriously and had acting coaches and everything".   
* Robinne Lee as Carol Wilkins, Bernies wife who initially tells her husband not to get too emotionally involved with the children he works with.
* Troy Gentile as Mark, a young kid who lives near Andi and Bruce and is eager to help with the dogs at the hotel.
* Kyla Pratt as Heather, an employee at the pet store who also wants to help rescue stray dogs. Pratt joined the film after she learned that Don Cheadle was also working on the project. She said in an interview that "they were telling me about all the different people who were gonna be in it, and I saw Don Cheadles name, and Im like, oh, absolutely". She also said of her co-star Emma Roberts that "Emma is so much fun to be around and shes hilarious. Emmas great because I wasnt sure how everything was going to be, because I was older than the other actors in the movie".   
* Lisa Kudrow as Lois Scudder, Andis and Bruces controlling, wannabe rock star foster mother. Kudrow signed up for the film in October 2007.    It was her first time working with dogs in a film. She said that her co-star Emma Roberts "was one of the draws for me to do this".     
* Kevin Dillon as Carl Scudder, Andis and Bruces foster father and an aging rocker who refuses to give up on his dreams of becoming a star. Dillon was cast in November 2007.    Dillon said in an interview that singing and playing guitar during the band practice scenes was "one of the things that was the most fun" while filming.   
* Yvette Nicole Brown as Ms. Camwell; was seen as either Marks mom or boss.
*Eric Edelstein as ACO Max; a cruel cold hearted animal pound worker, who along with his similar partner Jake, does everything in his power to lock up every stray dog in the city.

==Production==

===Development===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"It draws an interesting parallel between the kids and the dogs. Although I was aware that it was a risk to jump into directing my first feature working with both kids and animals, I recognized the importance and relevance of the story and thought it was worth it"
|-
|style="text-align: left;"|—Thor Freudenthal   
|}
The film rights to Lois Duncans novel were acquired by DreamWorks in June 2005.      According to Thor Freudenthal, DreamWorks first approached him about working on the film after a short film which he had worked on played at the Sundance Film Festival. Freudenthal said that DreamWorks "really embraced and responded to" the film and sent him an early version of the script. He was initially hesitant to sign onto the project, balking at the seemingly shallow title. However, he stated that after reading the script that he "realized you don’t think much about the logistics involved" and saw deeper messages and more complex aspects of the film.      Freudenthal was attracted to the "urban fairytale aspect" of the film, noting in an interview:

 

A producer of the film, Lauren Shuler Donner, is also an activist and dog lover, and was convinced that the books message about the importance of family "made the novel an ideal property to bring to the big screen"  Shuler Donner insisted that the movie "stand out from other family movies visually" and it was Freudenthals background in animation that gave him an edge over other directors. According to her, "It was the way he framed shots, the way he moved the camera, the use of color, the use of light. He’s very visually savvy and very specific." 

===Casting===
Emma Roberts was cast in August 2007 to play teenage older sister Andi. Freudenthal began his search for a young actress "who could carry a whole movie" and settled on then-16 year old Roberts. Ewan Leslie, a producer of the film, said in an interview of Roberts that she "is one of those young actors whose face just lights up the screen and she has the ability to play a wide range of emotions without any dialogue." 
 conducted a nationwide search for an actor to play Bruce, Andis whimsical and inventive younger brother. Jake T. Austin auditioned late, after nearly 80 other boys had tried out for the part. Jason Clark, another producer of the film, stated that Austin "was amazing on every level. He played the emotional beats very well, felt the role and also understood timing." 

The rest of the roles were cast in the following months. Don Cheadle, who plays Andi and Bruces protective social worker, joined the film in September 2007. Lisa Kudrow was cast as the siblings foster mother in October, and Johnny Simmons was cast that same month as Dave. Kyla Pratt was chosen to play Heather soon after. 
 breeds with different colors and facial structures "so that their look suggested their personality."  Freudenthal said that he deliberately chose both very small and very large dogs to create a contrast similar to the characters of Lenny and George in the John Steinbeck novella Of Mice And Men. The majority of the dogs cast were rescues. The lead dog, who plays Friday, was rescued about six months before shooting began. Crew members also helped to find adoptive homes for the abandoned dogs and several adopted dogs themselves. 

===Filming===
A Hollywood   The next phase of training involved using the dogs body language to express emotions: sadness, for example, was conveyed when a dog tucked its tail between its legs. Finally, the dogs were taken to public places to review the commands that they had learned. The purpose of this was to ensure that the dogs would perform in any location. According to Forbes, "You want the dogs to sense that everything is fine and they’ll still get their treat regardless of the location. The set becomes just another place for them to go."  The trainers worked with the human actors as well to "familiarize them with how the dogs behave and create a comfort level between the human and the dog actors."  The dogs were also trained to interact with the various gadgets in the film with early prototypes built by the special effects team.
Special effects supervisor Michael Lantieri enlisted to create the various contraptions invented by Bruce throughout the film to keep the dogs fed. One such gadget is a device which can be operated by the dogs to throw a ball to be fetched.

 

Other devices built for the film include a feeding machine that drops food into each of the dogs bowls on a timed schedule, a vending machine filled with shoes and other chew toys, a room filled with doors whose doorbells go off on their own, and another containing a replica of a car surrounded by fans which simulates for the dogs the experience of placing their heads through an open car window while driving.

All of the contraptions were created using objects that might actually be found in an abandoned hotel, and in such a way that they looked like they had been created by a gifted 13-year-old boy.

==Release==

===Box office=== Gran Torino, and others. It remained in release for 19 weeks and earned a total of $116,983,275 worldwide.    It is estimated to have earned $22,500,000 total over the four day weekend.

Moviefone called the opening, "pretty good for a fairly anonymous little family film opening against a higher-profile family film."  As of August 2011, the film has a reported box office gross of $73,034,460 for the United States and $43,965,738 internationally, for a total of $117,000,198. 

===Critical reception=== Marley has a lot he could learn from these dogs". Kent Turner, writing for School Library Journal, stated that while the book is "utterly realistic", the film is "fantastic" and thus fundamentally different.  Stephen Holden, writing for The New York Times, wrote that the film "is loaded with enough stupid pet and human tricks to satisfy David Letterman for years to come". 

It tied with Up (2009 film)|Up for Best Feature Film at the 24th Genesis Awards.

===Home media===
The film was released to DVD on April 28, 2009. It sold 773,000 units in the first week, bringing in $13,584,527 in revenue. As per the latest figures, 1,778,736 DVD units have been sold, translating to more than $30 million in revenue.  This does  not include Blu-ray sales.

===Other media=== Nintendo Wii, PC was released around the same time as the film.

==Soundtrack==
The score to Hotel for Dogs was composed by John Debney, who recorded his score at the Eastwood Scoring Stage at Warner Brothers.    Promo trailers for "Hotel For Dogs" contain songs such as "Run" by Nat & Alex Wolff, "(Lets Get Movin) Into Action" by Tim Armstrong featuring Skye Sweetnam and Youve Been Spiked by Chris Joss.

Hotel for Dogs: music from the motion picture is by Razor & Tie and the songs are: 
* A beautiful world (Tim Myers)  
* Get lucky (Dragonette)
* Into action (Tim Armstrong) 
* It had to be you (Motion City Soundtrack)
* Who who (Lisa Kudrow and Kevin Dillon) 
* Born to be wild (Steppenwolf)
* Ruff ruff ruff (Lisa Kudrow and Kevin Dillon)
* Say so (Uh Huh Her)
* Reason why (Rachael Yamagata) 
* Best days (Matt White) 
* My new best friend (Luke Tierney)
* Who let the Dogs out (Baha Men)

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 