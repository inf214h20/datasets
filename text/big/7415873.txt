The Stepping Stone
{{Infobox film
| name           = The Stepping Stone
| image          =
| caption        =
| director       = Reginald Barker Thomas H. Ince
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan(scenario)
| narrator       =
| starring       = Frank Keenan Mary Boland
| cinematography =
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States English intertitles
| budget         =
}}
 1916 silent silent drama lost film.     

==Plot==
Mary Beresford is the wife of unambitious law clerk Al Beresford. Thanks to Mary s tenacity and carefully calculated social-climbing, Al is promoted to the position of personal secretary of prominent financier Elihu Knowland. Unfortunately, success goes to Als head like a narcotic, and soon he has alienated everyone in New York, including Mary, who runs off for parts unknown.

==Cast==
* Frank Keenan - Elihu Knowland
* Mary Boland - Mary Beresford Robert McKim	- Al Beresford
* Margaret Thompson - Flora Alden
* Joseph J. Dowling - W. B. Prescott
* J. Barney Sherry - Horatio Wells

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 


 