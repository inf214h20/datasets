The Bounty Hunter (2010 film)
{{Infobox film
| name           = The Bounty Hunter
| image          = Bounty hunter poster.jpg
| alt            =  
| caption        = Theatrical teaser poster
| director       = Andy Tennant
| producer       = Neal H. Moritz
| writer         = Sarah Thorp
| starring       = Jennifer Aniston Gerard Butler
| music          = George Fenton
| cinematography = Oliver Bokelberg
| editing        = Troy Takaki
| studio         = Relativity Media Original Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 111 minutes 
| country        = United States
| language       = English
| budget         = $40–$50 million 
| gross          = $136.3 million 
}}
The Bounty Hunter is a 2010 American romantic action comedy film directed by Andy Tennant, starring Jennifer Aniston and Gerard Butler. The story centers on a bounty hunter (Butler) hired to retrieve his ex-wife (Aniston) who has skipped bail. The film was released in the United Kingdom and United States on March 19, 2010. 

==Plot== NYPD detective who now works as a bail enforcement agent (bounty hunter).  Milos ex-wife, Nicole Hurley (Jennifer Aniston), is an investigative reporter who has been arrested for assaulting a police officer (As she later clarifies, she grazed a patrol horse with her car in trying to get to a press conference).
 bond hearing to meet with her informant, causing the judge to revoke her bail and issue a warrant for her arrest.  Unfortunately, just before Nicole arrives, her informant, Jimmy (Adam Rose), is kidnapped.

Milo is ecstatic when Nicoles bail bondsman, Sid (Jeff Garlin), offers him the job of tracking down Nicole and bringing her to jail, for a bounty of $5,000.  After questioning Nicoles mother Kitty (Christine Baranski) (a cabaret singer in Atlantic City), Milo apprehends her at a race track in New Jersey, throws her into his car trunk and drives back towards Manhattan. Nicole manages to escape briefly before he catches up with her again.

Meanwhile, neither is aware that they are being stalked: Milo by two thugs sent by a bookie named Irene (Cathy Moriarty), because of outstanding gambling debts; Nicole by corrupt cop Earl Mahler (Peter Greene), who is connected with the story she is investigating, and both of them by Nicoles lovestruck coworker Stewart (Jason Sudeikis), bent on rescuing her from Milo. Stewart, however, is confused with Milo by Irenes thugs and kidnapped in his place.

Earl catches up and tries to kill Nicole, but the two narrowly escape. Milo is not interested in explanations until Nicole admits that shes found evidence that implicates their mutual friend and Milos ex-partner on the police force, Bobby (Dorian Missick), is involved with Earl. Angry, Milo decides to investigate the crime with her.

Clues from Earls car lead them to a country club, where they learn from a caddy that he owns a tattoo parlor in Queens, so they start to make their way there. Bobby warns the pair to stay off the road.

By coincidence, the nearest hotel is "Cupids Cabin," the bed and breakfast where they spent their honeymoon.  They have feelings for each other and they both admit they have made mistakes  . She calls her mother on advice what to do. When she is done, she comes out of the bathroom and overhears Milo telling Sid that he may or may not sleep with Nicole that night, but hes taking her to jail nevertheless.  Infuriated, she handcuffs Milo to the bed and makes her way to the tattoo parlor herself, finding Jimmy and freeing him before she is captured by Irenes thugs, still looking for Milo.

Milo manages to rescue her at a strip club. He calls an old friend from the police force and learns that Bobby is on his way to the polices evidence warehouse, which is being relocated to a new building. Bobby confronts Earl, who used to be his friend but has used Bobbys name to gain access to the warehouse and steal a large amount of confiscated narcotics and cash. Bobby decides to arrest Earl, but Earl draws a gun and shoots him, though not fatally. Milo and Nicole enter the warehouse and Milo is ambushed, but Earl is forced to surrender when Nicole points a shotgun at him.

Bobby explains that Earl was using him, as well as the man who supposedly committed suicide, to gain access to the warehouse. There was no proof, so Bobby was waiting for Earl to make his move before arresting him. Milo proudly notes that Earl might have gotten away with it if Nicole hadnt picked up certain clues. He and Nicole appear to have reconciled. They concede that sometimes their jobs have to come first. By way of demonstrating this, Milo then turns Nicole into the police, so she can make her court hearing the next day. 

On his way out of the precinct, Milo runs into a cop who insulted him earlier and punches him in the face. He is arrested and put in a cell next to Nicoles. He reminds her that it is their anniversary and they have to spend it together, no matter what. Through the bars they admit their love to each other and kiss.

==Cast==
* Jennifer Aniston as Nicole Hurley (previously Boyd), a Daily News reporter.
* Gerard Butler as Milo Boyd, a bounty hunter and former police officer.
* Jason Sudeikis as Stewart, Nicoles creepy co-worker and stalker who has unrequited feelings for her.
* Jeff Garlin as Sid, Milos friend and employer as a bail bondsman.
* Cathy Moriarty as Irene, a gift shop owner and local bookmaker.
* Ritchie Coster as Ray, one of Irenes minions.
* Joel Marsh Garland as Dwight, one of Irenes minions
* Siobhan Fallon Hogan as Teresa, the secretary of Sids business.
* Peter Greene as Earl Mahler, a corrupt cop.
* Dorian Missick as Bobby Jenkins, NYPD detective and best friend of Milo.
* Carol Kane as Dawn, a co-owner of a bed and breakfast.
* Adam LeFevre as Edmund, a co-owner of a bed and breakfast.
* Adam Rose as Jimmy, a bartender at a cop bar who snitches info to reporters for money.
* Christine Baranski as Kitty Hurley, Nicoles caring, wise, but somewhat perverted mother.
* Matt Malloy as Gary, a co-worker of Nicoles.

==Release==

===Box office=== Alice in Diary of a Wimpy Kid.  It grossed $20.7 million in its opening weekend.  As of July 5, 2010 it has grossed $67,061,228 in North America and $69,031,265 internationally for a worldwide total of $136,333,522.   

=== Critical response ===
  
The film was panned by critics. Review aggregation website Rotten Tomatoes gives a score of 13% based on 143. reviews. The consensus given is "Gerard Butler and Jennifer Aniston remain as attractive as ever, but The Bounty Hunters formula script doesnt know what to do with them — or the audiences attention."  Metacritic gives it a "generally unfavorable" score of 22%, based on review from 31 critics. 

Roger Ebert gave the film a half-a-star out of four, commenting that "neither   is allowed to speak more than efficient sentences to advance the plot" and that it is rife with "exhausted action clichés."  A.O. Scott of The New York Times gave the film a completely negative review and said it was rated PG-13 for "witless sexual innuendo and witless violence."  
Kerry Lengel of The Arizona Republic gave the film three and a half stars out of five: "As formula films go, The Bounty Hunter is more enjoyable than most, even if it packs in as many clichés as any." She also praises Anistons and Butlers performances, but is critical of the "improbable" plot. 

===Accolades===
  
The film was nominated for four Razzie Awards in 2011, including Worst Picture, Worst Actor (Gerard Butler), Worst Actress (Jennifer Aniston) and Worst Screen Couple (Aniston and Butler). It failed to "win" in any of those categories. 

===Home media===
The Bounty Hunter was released on DVD and Blu-ray Disc on July 13, 2010. The only extra material is 3 featurettes ("Making The Bounty Hunter", "Rules for Outwitting a Bounty Hunter", "Stops Along the Road: Hunting Locations").  It grossed $23,310,266 from DVD sales in North America. 

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 