Manik (film)
{{Infobox film
| name =Manik
| image =Manik.jpg
| image_size     =200px
| caption        = The poster for the film Manik
| director = Probhat Roy
| writer = Jeet Koyel Mullick
| producer = Pradip Bharadwaj
| distributor = Ashadeep Entertainment
| cinematography =P.B. Chaki
| editing =Atish Chandra Sarkar
| released = 22 April 2005
| country        = India
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| runtime = 166                          mins 
{{Cite web
|url=http://rental.bigflix.com/bigflix/Movie/Bengali/2004/Manik
|title=Manik - BIGFlix.com
|publisher=rental.bigflix.com
|accessdate=2008-11-14
|last=
|first=
}}
  Bengali
| music = Babul Bose
| website        =
}}
 Bengali Movie was released in 2005. Directed by  Probhat Roy,  the movie featured Jeet (actor)|Jeet, Koyel Mullick, Ranjit Mallick . This movie is Koyel Mullicks  second .
 
{{Cite news
|url=http://www.telegraphindia.com/1050429/asp/etc/story_4630503.asp
|title=  Manik
|publisher=www.telegraphindia.com
|accessdate=2008-11-14
|date=29 April 2005
|last=Grover
|first=Anil
|location=Calcutta, India
}}
  
{{Cite web
|url=http://calcuttatube.com/2008/01/18/manik-2004-jeet-koyel-mullick-blockbuster-watch-the-bengali-movie-online/
|title=Manik (2004)
|publisher=calcuttatube.com
|accessdate=2008-11-14
|last=
|first=
}}  
 
This film was shot at Kalimpong Golf Course.
 
{{Cite news
|url=http://www.telegraphindia.com/1041227/asp/bengal/index.asp
|title=The Telegraph - Calcutta : Bengal
|publisher=www.telegraphindia.com
|accessdate=2008-11-14
|last=
|first=
|location=Calcutta, India
|date=27 December 2004
}}
  
{{Cite news
|url=http://www.telegraphindia.com/1050331/asp/calcutta/story_4556863.asp
|title=Mixed menu for Poila Baisakh platter
|publisher=www.telegraphindia.com
|accessdate=2008-11-14
|last=
|first=
|location=Calcutta, India
|date=31 March 2005
}}
 

==Plot==
Samir Mitra is an unemployed educated guy who is looking for a job. His father Abir Mitra is a retired person. Samirs mother is suffering from cancer. One day Samir suddenly meets Manik Chandra Sadhukhan. Maniks father is Niranjan Sadhukan, he is also ill. Latika is Maniks younger sister. Manik has come to Kolkata to meet his fathers friend Chandrakanta Majumadar who is an established businessman. Manik meets with an accident on the way. Samir tries to save his life. Before his death Manik requests Samir to take care of his father and younger sister; he also tells Samir not to disclose the news of his accident. Manik gives Samir all required information. Samir becomes Manik; he meets Chandrakanta as Manik. Chandrakanta appoints him in his own office. Samir meets Chandrakantas daughter Ria and wife Manju, Ria and Samir gradually fall in love but Samir feels uncomfortable whenever he thinks that when everybody will come to know the truth that he is not Manik then what will happen. From his salary he maintains his own family and Maniks family. Everybody is impressed with his behavior and honesty. Suddenly Niranjan and Latika come to Kolkata because Niranjan is unwell. Niranjan has lost his eyesight and there is some problem in his heart also. Samir tries to hide from Niranjan but every body comes to know the truth. Kartik Sen, the business rival of Chandrakanta, kidnaps Ria and demands ransom. Samir and Chandrakanta rescue her with the help of police. Samir donates his fathers eye to Niranjan. Niranjan gets back his eyesight.

==Cast== Jeet
* Ranjit Mallick
* Koyel Mullick
* Samata Das
* Soma Dey
* Debdoot Ghosh
* Rajatava Datta
* Shyamal Dutta
* Santilal Mukherjee
* Rajesh Sharma
* Sagnik Chatterjee
* Biplab Chatterjee
* Alakananda Ray

==Crew==
* Producer(s): Pradip Bharadwaj
* Director: Probhat Roy
*Story:
*Production Design:
*Dialogue:
*Lyrics:  Gautam Susmit
*Editing :Atish Chandra Sarkar
*Cinematography: P.B. Chaki
*Fight:Shantanu Pal
*Singer:Babul Supriyo, Shreya Ghoshal, Jojo (Bengali singer)|Jojo, Reema Mukherjee

==Music==
শুভ জন্মদিন

==Critical reception==
 

==Awards==
*Awarded by ETV Lux Best Film in 2005 
{{Cite web
|url=http://mcgroup.co.in/home/ashadeep.php
|title=..::.. MA CHHINNAMASTIKA GROUP OF COMPANIES
|publisher=mcgroup.co.in
|accessdate=2008-11-14
|last=
|first=
}}  
 
*Awards   :Lux ETV Award as the Best Film in the year 2005. Won five awards.

* Anandalok Awards  (2005)
 Jeet

** Best Playback Singer (Female) :Shreya Ghoshal

==References==
 

==External links==
* 

 

 

 
 
 
 