Mr. Jones (2013 film)
 
{{Infobox film
| name           = Mr. Jones
| image          = File:Mr Jones 2013 horror thriller poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Karl Mueller
| producer       = Jamie Carmichael Ross M. Dinerstein Angela Sostre
| writer         = Karl Mueller
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Sarah Jones Mark Steger
| music          = Herwig Maurer
| cinematography = Mathew Rudenberg
| editing        = Saul Herckis
| studio         = Preferred Content, Preferred Film & TV
| distributor    = Anchor Bay Films Freestyle Releasing 
| released       =    
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Sarah Jones as a couple who go out to the woods to work on a film, but end up being terrorized by a series of increasingly strange events.  

==Plot==
 
Penny (Jones) is on her way out to the woods to help her boyfriend Scott (Foster) make a nature documentary. The two end up fighting, as Scott has not fully planned out his documentary and Penny had given up a good job to come help him. Things turn strange when one of Scotts possessions is stolen, prompting the two to seek it out. They end up finding a cabin filled with various strange, weird artifacts and figures. They eventually realize  it is the home of Mr. Jones (Mark Steger), an elusive artist who sends his artwork to random people with no rhyme or reason. Scott investigates the mythology and rumors surrounding Mr. Jones with the intent to make him the subject of his next documentary, despite warnings that he stay far away from the man in question.

==Cast==
*Jon Foster as Scott Sarah Jones as Penny
*Mark Steger as Mr. Jones
*Faran Tahir as The Anthropologist
*Stanley B. Herman as The Old Journalist
*Ethan Sawyer as Alleged Scarecrow Recipient
*Jordan Byrne as Peter Cavagnaro
*David Clennon as The Curator
*Jessica Dowdeswell as  Penny
*Diane Neal as The Scholar
*Rachel OMeara as The Skeptic

==Production==
Filming took place near Santa Clarita, California, at a movie ranch that had several derelict houses on it, as well as an abandoned mineshaft of which Mueller took advantage for some scenes that were supposed to have taken place in caverns.    While filming, Mueller was inspired by director David Lynch and the film Eternal Sunshine of the Spotless Mind, and wanted to design the films soundtrack to "make it feel like you’re under water, or in somebody’s head and they have a bad cold."  He was also inspired by the memory of a neighbor of his during childhood in Minnesota, as the man had lived in a "primitive shack of a cabin with no running water, trapped animals, and hung them up around the woods. He had lots of bizarre farming equipment overgrown by weeds around his house. He was the boogieman we made up stories about to scare ourselves at night."   

==Reception==
Critical reception for Mr. Jones has been predominantly negative and the movie holds a rating of 25% on Rotten Tomatoes (based on eight reviews) and 38 on Metacritic (based on five reviews).    Dread Central and The Hollywood Reporter both wrote mixed reviews,  and Dread Central commented that while the film was "nicely shot" and the acting was good, the final portion of the film was "hard to follow and sometimes even hard to see" and, "By the time I got to the final ten minutes, I had had enough of the ongoing trippy scenes and was absolutely ready for the end."  IndieWire criticized Mr. Jones for never fully delivering on its promise and compared it negatively to films created in "screenwriting classes during workshop sessions", as they felt that the movie was "sloppy". 

In contrast, Bloody Disgusting praised the movie and stated the film, "captures the visuals of a nightmare" and "gives the deeper viewer plenty of panicked thrills".  Twitch Film also gave a mostly positive review, noting that the film would not appeal to all audiences. 

==References==
 

==External links==
*  
*  

 
 
 
 
 