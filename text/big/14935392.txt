Next Day Air
{{Infobox film
| name           = Next Day Air
| image          = Next day air poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Benny Boom Michael Williams
| writer         = Blair Cobbs
| starring       = Donald Faison Mike Epps Wood Harris  Omari Hardwick Darius McCrary Yasmin Deliz Mos Def
| music          = The Elements
| cinematography = David A. Armstrong
| editing        = David Checel	 	
| studio         = A-Mark Entertainment Melee Entertainment Next Day Air Productions Secret Society Films
| distributor    = Summit Entertainment
| released       =  
| runtime        = 84 minutes  
| country        = United States
| language       = English
| budget         = 3,000,000 est  
| gross          = $10,027,047    
}}
 action comedy film that was released by Summit Entertainment on May 8, 2009.   The film starring Donald Faison and Mike Epps was produced on an estimated budget of $3 million.  Two criminals accidentally accept a package of cocaine which they must sell before the real owner finds it missing.

==Plot==
Leo works for Next Day Air (NDA), a package delivery company, but is going to get fired for any more mistakes. While delivering a package addressed to Jesus in apartment 303, Leo accidentally delivers it to apartment 302.  Before Leo can leave Jesus asks if Leo has the package and gets worried when he is empty handed.  Guch and Brody, two inept criminals, open the package and find ten bricks of cocaine hidden in a clay pot.  Brody remembers that his cousin, Shavoo, has cut cocaine before.  Shavoo and his partner, Buddy, come to Guchs apartment and settle on $15,000 a brick. 

Bodega, Jesuss boss and original sender of the package, calls to confirm the package was delivered.  Jesus tells Bodega it was not delivered even though the tracking information says otherwise.  Jesus is concerned that Bodega is going to kill him and assumes that Leo stole the package.  While searching Jesus tells Chita that his previous boss was killed because of a similar situation.  Jesus and Chita find Eric, another employee of NDA, who they hold at gun point and steal his watch.  Finally realizing that it is not Leo they continue their search.  

Shavoo has trouble getting the money from storage because he was robbed by the front desk clerk.  He interrogates the clerk and finds the storage garage where his money and supplies are.  Shavoo and Buddy locks the clerk and his accomplice inside a garage, bound and gagged with duct tape.

Bodega surprises Jesus in Philadelphia, to his dismay, and they search for Leo together.  Jesus tortures Leo but he cant remember anything.  Back at the apartments Leo is walking down the hallway and finally remembers he delivered it to 302. Bodega forces Leo to request the package back while Guch is inside counting the money.  Brody informs Leo that he sent the package back to NDA headquarters.  Bodega realizes something strange is going on and forces his way into the apartment. Everyone is at gun point but Guch takes the first shot at Jesus.  After the gunfight Shavoo limps away nearly dead; Leo, completely uninjured, jumps up and leaves with the money; and Chita comes to check on Jesus, who is wounded. Luckily, he is saved by the watch he took from another employee of NDA earlier. Jesus and Chita walk away with the cocaine as sirens can be heard in the background.

==Cast==
*Donald Faison as Leo: Faison "campaigned" to get this role almost to the point of begging because he wanted to work with Epps, Harris, and Def. 
*Mike Epps as Brody: Epps did not do any improvising because he wanted to show people he did not "have to improv to be in a movie and be funny and do a good job." 
*Wood Harris as Guch
*  ritual but was willing to do it after reassurance from her mom that God was on her side. 
*Omari Hardwick as Shavoo
*Darius McCrary as Buddy
*  to get her out the door and that helped her get the part.   
*Emilio Rivera as Bodega
*Lobo Sebastian as Rhino
*Mos Def as Eric
*Lauren London as Ivy
*Debbie Allen as Ms. Jackson Cassidy as Cass
*Jo D. Jonz as Wade

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"I was able to put my touches on it and add that extra spice to make it right."
&mdash;    Benny Boom, director   
|}
Benny Boom got attached to the project when a friend got him in touch with a producer. He fell in love with the script after reading it and believed he could put his own style on it.  He was also attracted to the script because it takes place in his hometown of Philadelphia. Next Day Air was shot in 20 days which Boom said he was able to do because he already had the film edited in his head.   In the audio commentary of the DVD, the director mentions how he forbade the use of the "n" word for on-screen dialogue, and how that element is sometimes incorrectly attributed to Mos Def.

==Reception==
===Critics===
The film has negative reviews receiving a 46/100 rating on Metacritic based on 20 reviews.  On Rotten Tomatoes it received a "rotten" rating with a 22% based on 60 reviews.  The general consensus was "Rife with half-baked jokes and excessive violence, Next Day Air is an uninspired stoner comedy." 

Sam Adams of the Los Angeles Times gave the film a negative review citing the fact that the director could not make up his mind, "whether he wants to make a straight-up crime movie or a tongue-in-cheek riff on the genre, and he lacks the wherewithal to do both at once."  He went on to say that the films plot had enough holes you could drive a delivery truck through."  The Boston Globe writer Janice Page thought the film had too many "deep talks" and that "none of these characters provides more than a smattering of laughs." 

Roger Ebert gave the film a more positive review stating that Benny Boom "knows what hes doing and skillfully intercuts the story strands."  Ebert also wrote that the film has a lot of "dire dialogue" and it is "very sunny"   Another positive review from Nathan Lee of The New York Times wrote that Next Day Air had "a script that snaps, characters that pop, a blaze of streetwise attitude."  He continued saying the film is "violent and profane but never vulgar or inhuman." 

===Box office=== 17 Again with approximately $4 million in revenue.  Next Day Air steadily declined in the box office during its eight-week run.  The film grossed $10,027,047 ranking it 111 in films released in 2009. 

===Home media===
Next Day Air was released on September 15, 2009 on DVD.  , it had grossed $4,578,713. 

==Soundtrack==
{{Infobox Album |
 | Name        = Next Day Air 
 | Type        = Soundtrack
 | Artist      = Various Artists
 | Cover       = 
 | Released    =  
 | Length      = 53:90
 | Label       = Melee
 | Producer    = 
 | Reviews     =
}}
{| class=wikitable width="51%" 
|-
! width=1% |  Track # 
! width=20% | Title
! width=20% | Performer Length (M:SS)
|-
| 1
| "Philly Cityscape"
| The Elements
| 01:06
|-
| 2
| "Count My Money"
| Trick Daddy
| 03:44
|-
| 3
| "Heat Rocks" 
| Raekwon
| 03:58
|-
| 4 
| "Get the Tapes" 
| The Elements
| 00:41
|-
| 5
| "Sixty Million Dollar Flow" 
| Glasses Malone
| 03:55
|-
| 6
| "Honestly" 
| Kurupt
| 03:21
|-
| 7
| "In the Life" 
| Lisa "Left Eye" Lopes 
| 03:45
|-
| 8
| "Videogame" 
| The Elements
| 00:51
|-
| 9
| "Blunts & Roses" 
| Penuckle
| 04:27
|-
| 10
| "Cerebro Orgasmo Envidia & Sofia"
| Martin Buscaglia
| 04:38
|-
| 11
| "Day Ago" 
| Spider Loc
| 03:00
|-
| 12
| "Its Real" 
| Darius McCrary
| 04:43
|-
| 13
| "Ave" 
| The Elements
| 00:42
|-
| 14
| "Gone Get It" 
| C-Dash
| 03:28
|-
| 15
| "Get It How You Live"
| 5 Grand
| 04:35
|-
| 16
| "So Fly"
| Meek Mill
| 03:47
|-
| 17
| "Next Day Air" 
| Prophet
| 02:28
|}

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 