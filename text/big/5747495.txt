Lage Raho Munna Bhai
 
 
{{Infobox film
| name           = Lage Raho Munna Bhai
| image          = Lage raho munna bhai.JPG
| caption        = Theatrical release poster
| director       = Rajkumar Hirani
| producer       = Vidhu Vinod Chopra
| story          = Rajkumar Hirani Vidhu Vinod Chopra
| screenplay     = Rajkumar Hirani Abhijat Joshi
| starring       = Sanjay Dutt Arshad Warsi Vidya Balan Boman Irani Dilip Prabhavalkar Dia Mirza Jimmy Shergill Kulbhushan Kharbanda Saurabh Shukla
| music          = Shantanu Moitra
| cinematography = C. K. Muraleedharan
| editing        = Rajkumar Hirani
| distributor    = Vinod Chopra Productions
| released       =   
| runtime        = 144 minutes
| language       = Hindi
| country        = India
| budget         =   {{cite web
| url = http://www.the-numbers.com/movies/2006/0LARM.php| title = Lage Raho Munnabhai| accessdate = 2007-05-03| work = The Numbers| publisher = Nash Information Services, LLC}} 
| gross          =     
}} 2006 Bollywood|Indian underworld don. In Lage Raho Munna Bhai, the eponymous lead character starts to see the spirit of Mahatma Gandhi. Through his interactions with Gandhi, he begins to practice what he refers to as Gandhigiri (a neologism for Gandhism) to help ordinary people solve their problems.
 National Film Awards. {{Cite news| url=http://boxofficeindia.com/showProd.php?itemCat=212&catName=MjAwNg==| archiveurl=http://wayback.archive.org/web/20131020213049/http://boxofficeindia.com/showProd.php?itemCat=212&catName=MjAwNg==| archivedate=2013-10-20| title=Box Office 2006 (Figures in Ind Rs) Tous Les 2007 Academy Award for Best Foreign Film.

==Plot== birthday of Gandhi. To prepare for the contest, Munnas sidekick Circuit (Arshad Warsi) kidnaps and bribes a group of professors to provide the answers for Munna. After winning the contest, Munna is granted an interview with Janhavi wherein he presents himself as a professor of history and a Gandhi specialist. Janhavi subsequently asks Munna to present a lecture on Gandhi to a community of senior citizens who live in her home, called the Second Innings House. To prepare for this event, Munna engages in intense study at a Gandhi institute. For three days and nights (and without food or sleep), Munna reads about the life and ideologies of Gandhi.
 lifestyle based upon Gandhism. Munna starts to co-host a radio-show with Janhavi and Gandhis image, guiding his audience to use Gandhigiri to solve everyday problems.
 underworld activities Vedic astrological makeup is believed by some to be devastating for marriage, mostly leading to the death of the spouse after a certain calculated period of marriage). Lucky appropriates the Second Innings House by sending Munna to Goa (keeping him out of the way) and then blackmailing him to let the matter pass or risk losing his love Janvi. In response, Munna launches a "non-violent" protest to reclaim the house. He calls this protest "Get Well Soon, Lucky" and asks his radio shows audience to send Lucky flowers (red roses especially) to help him recover from the "disease of dishonesty".

During this time Munna tells Janhavi the truth (via a letter he gives to her). Heartbroken, Janhavi leaves Munna. Munna receives another setback when he is tricked by Lucky into revealing his conversations with Gandhi before a public audience. At this conference, Munna finds that only after he has learned something about "Bapu"s life can the Gandhi image talk about it, which serves as proof for a psychiatrist in the audience that Munna is delusional. Gandhis monologue at the end of the film, however, questions this conclusion. Munna, despite these defeats, continues to use Gandhigiri, a decision that transforms Lucky, revives Janhavis affection, and resolves Simrans marriage. Lucky Singh eventually becomes a student of Gandhigiri and is greeted by Gandhis image not long after he has begun to study "Bapu"s life. Immediately he calls for a photograph to be taken of them together; this perplexes the photographer, who cannot see the Gandhi image.

==Cast==
 
*Sanjay Dutt as Murli Prasad Sharma or "Munna Bhai" Circuit , Munnas sidekick
*Vidya Balan as Jhanvi, radio-jockey and Munnas love-interest
*Boman Irani as Lucky Singh
*Dilip Prabhavalkar as the image of Mahatma Gandhi
*Dia Mirza as Simran, Luckys daughter, engaged to Sunny
*Jimmy Shergill as Victor DSouza, who having lost his fathers money, needs Munnas advice
*Kulbhushan Kharbanda as Kkhurana, a wealthy but superstitious businessman
*Saurabh Shukla as Batuk Maharaj, Kkhuranas astrologer
*Abhishek Bachchan as Sunny, Kkhuranas son, engaged to Simran
*Rohitash Gaud as Cuckoo (Luckys Sidekick)
*Parikshat Sahni as Victors father
*Ashwin Mushran as Hari Desai
* Hemu Adhikari as the Retired teacher
*Priya Bapat as the girl who calls Murli
*Ninad Kamat as the Lawyer
*Kurush Deboo as Dhansukh Bhai Patel, Gujarati Lawyer
*Arun Bali as a Second Innings Resident
*Achyut Potdar as a Second Innings Resident

==Production==
The Munna Bhai (film series)|Munna Bhai series began after Vidhu Vinod Chopra agreed to produce
Rajkumar Hiranis film Munna Bhai M.B.B.S. when no one else would (Hirani had worked as an editor on Chopras Mission Kashmir). They also collaborated on the script.    Rajkumar Hirani next began work on a film that was not related to the Munna Bhai series but instead focused on a man who hallucinates about Gandhi. Hirani was in talks with Aamir Khan about the screenplay. However, after some time Hirani decided to combine this script with a pending sequel to Munna Bhai M.B.B.S. and thus Khan was dropped from the project. Khan and Hirani would team up a few years later for 3 Idiots.   

The sequel was initially known as Munnabhai Meets Mahatma Gandhi and was later retitled Munnabhai 2nd Innings before being given its current name.   Director and screenwriter Rajkumar Hirani admitted in an interview that he felt the burden of expectation while writing the screenplay for Lage Raho Munna Bhai, as he had to create "something to match" the first film.    Initially there was some effort to incorporate scenes or particulars of the first film into the sequel (such as the idiosyncratic laugh of Dr. Asthana, portrayed by Boman Irani), but the risks of repetition were consciously averted. 

One of Hiranis goals in making the film was to revive an interest in Mahatma Gandhi, a figure whom he felt had been forgotten in contemporary India. To highlight this fact, Hirani recounted (during an interview) an incident with a Masala chai|chai-wallah boy (who brings tea to the crew) during production:
 Munnabhai fan and kept asking the name of the film. The first working title was Munnabhai Meets Mahatma Gandhi, and Shantanu (Moitra, the music director) told him. So he said, Munnabhai to theek hai, yeh Mahatma Gandhi kaun hai? (Munnabhai is fine, but who is this Mahatma Gandhi?) So this is the sad state of affairs today. I was shocked. And its not just the chai-wallah. A few days ago on TV a lot of politicians were asked India-related questions on the news channels, and I cant believe a lot of them dont know 2 October is Gandhi Jayanti|Gandhijis birthday! Many didnt know his first name. They kept saying, whats in a name, we respect his ideals, but come on! How can you not know his name?   

The other screenwriter, Abhijat Joshi (who teaches in the department of English at Otterbein College in Westerville, Ohio), stated that he had been conducting extensive research on Gandhi,    which inspired producer Chopra to involve Joshi in the creation of the second Munna Bhai screenplay. 

While writing the screenplay, Hirani and Joshi stayed together for more than four months. They developed scenes by going out for a walk and discussing the scene. They would not return home until they had created a scene that would make them laugh, or cry, or had some provocative thought.  While there was a shortage of resources during the shooting of Munna Bhai M.B.B.S., the crew did not encounter a financial crisis during the filming of Lage Raho Munna Bhai, as the team managed to receive whatever was deemed necessary (including a Jimmy Jib, a specific kind of camera crane, just for a single crane shot).  The film was shot on location in and around Mumbai, with Goa as a backdrop for the "Aane Charaane" song. 
 Circuit (portrayed by Arshad Warsi)—were retained from Munna Bhai M.B.B.S. Several actors from Munna Bhai M.B.B.S., appeared in Lage Raho Munna Bhai as different characters. {{cite web
| url = http://in.movies.yahoo.com/060802/24/66cth.html| archiveurl = http://web.archive.org/web/20060828195708/http://in.movies.yahoo.com/060802/24/66cth.html accessdate = 2007-04-20 | author = IndiaFM News Bureau| date = 2 August 2006 | work = Yahoo! Movies | publisher = Yahoo Web Services India Pvt Ltd}}  Vidya Balan was chosen to play the leading lady as her voice was thought to be appropriate for that of a radio jockey. {{cite web
| url = http://www.movietalkies.com/interviews/116/raju-hirani-speaks-on-‘lage-raho-munnabhai’ | title = Rajkumar Hirani | accessdate = 2007-04-20 | author = MovieTalkies.com | date = 4 September 2006 | work = Interviews | publisher = Movie Talkies.com }} 
 My Experiments with Truth as a preparation for Lage Raho Munna Bhai. Rather, he comments, his father, Sunil Dutt (who portrays Munna Bhais father in the first film, Munna Bhai M.B.B.S.) and his mother (the late actress Nargis) were his role models as they "were basically Gandhians. We were brought up with those values." 

Dilip Prabhavalkar, who portrays Gandhi, read Gandhis works "once again" to prepare for his role.    Boman Irani prepared for the role of Lucky Singh by spending time with Sardarjis (male Sikhs) in auto spare parts shops to research his role.  Vidya Balan ("Jahnavi") met with a couple of radio jockeys and watched them at work.   

==Themes and impact==

===Influences and allusions=== James Bond series.  Others have also likened the series to the work of Laurel and Hardy.  Some, however, have negated this comparison, stating that the series is more akin to the Road to... "buddy films" of Bob Hope and Bing Crosby.  Hirani stated that his work was deeply inspired by the films of Hrishikesh Mukherjee. 

===Social influence=== Colonial India and the Indian independence movement. Gandhi was a leader in this movement, challenging the British Empires presence in India through the use of Satyagraha (non-violence). In this context, Jahnavi and Munna Bhais non-violent protest against Lucky Singh serves as a metaphor for the Indian independence movement and the battle against the British Raj. 
 liberalisation brand, gandhigiri is the message."  Amelia Gentleman of The International Herald Tribune/New York Times stated that:
 The Congress Party recommended that all party members see the film. The Delhi authorities declared that tickets to the film would be sold tax free because of its assiduous promotion of Gandhian values." 

Mark Sappenfield of The Christian Science Monitor argues that the film was appealing because, "Gandhi gets his hands dirty. He appears as an apparition only visible to the wayward gangster, counselling him on how to help others deal with everyday problems."    Swati Gauri Sharma further suggests in The Boston Globe that what the United States "needs is a film that encourages people to take up Gandhigiri, Martin Luther King|Kinggiri, or John F. Kennedy|Kennedygiri. If it worked for Bollywood, it could work for Hollywood." 

===Gandhigiri-style protests===
 , a notable example of Satyagraha]] green card Bethesda Naval hospitals.    On 17 July, the USCIS announced that "it will accept applications from foreign professionals seeking permanent residency through an expedited process, reversing its earlier decision." USCIS Director Emilio T. Gonzalez noted, "The public reaction to the 2 July announcement made it clear that the federal governments management of this process needs further review ... I am committed to working with Congress and the State Department to implement a more efficient system in line with public expectations."   
 Mafia don Babloo Srivastava claimed to have been inspired by Lago Raho Munna Bhai to distribute roses as a "message of love and peace". 

===Impact===
Lage Raho Munna Bhai revived an interest in books about Gandhi.  In particular, demand for Gandhis autobiography My Experiments with Truth increased after the film debuted, including requests from prison inmates.   In addition, due to its influence, the film was made tax-free in Mumbai. 

==Release==

===Screenings===
Screened on 10 November 2006 in the United Nations auditorium, Lage Raho Munna Bhai was the first Hindi film to be shown at the UN.    The film was introduced by Shashi Tharoor, UN Under-Secretary-General of the United Nations|Under-Secretary General for Communications and Public Information. Taran Adarsh of Bollywood Hungama observed that, "there was thunderous clapping at the high points of the film, like the pensioner shedding his clothes. The applause at the end of the screening was unending. A vibrant question and answer session followed with director Rajkumar Hirani, writer Abhijat Joshi and actor Boman Irani, who flew to the U.S. for the screening."     The Indo-Asian News Service (IANS) noted that, "an evening that had started with massive security arrangements in the sombre UN setting, concluded in a festive atmosphere in the lounge of the UN with diplomats from other tables joining in raising a toast for the film."  The United Nations General Assembly announced on 15 June 2007 that 2 October, the day of Gandhis birth (Gandhi Jayanti), was to be "the International Day of Non-Violence." 

The Prime Minister of India, Manmohan Singh, was given a private screening of Lage Raho Munna Bhai. After viewing the film, he stated that it "captures Bapus message about the power of truth and humanism."    In a speech during his visit to South Africa, Singh said, "I was heartened to see recently that back home in India the most popular movie this festival season is a film about a young mans discovery of the universal and timeless relevance of the Mahatmas message."  Singh announced the creation of a new Public Services Bill to combat corruption in a press release dated 17 November 2006, and cited Lage Raho Munna Bhai as one of its influences. 

Lage Raho Munna Bhai was further screened at a global judiciary summit in Lucknow in December 2006. After viewing the film, Justice Kenneth Mithyane from South Africa commented, "The movie has re-enlivened the non-violence philosophy practiced by Mahatma Gandhi who continues to remain close to the hearts of the South Africans." Fatima Chouhan, a young member of the South African parliament, noted that, "Munnabhai will be widely appreciated in South Africa. Im carrying a couple of video discs for my family and friends." 
 Tous Les Cinema du Monde section of the 2007 Cannes Film Festival. TLage Raho Munna Bhai was well received as the audience had lined "up in long queues to catch the film that had been strongly recommended in festival reviews   not one person who entered the screening left before the end of the two-hours-thirty-minutes film."    In addition, "the screening of the movie at the festival saw people sitting on the aisles as the theatre was completely packed   there was also a big group of French students that clapped till the credits were finished." 
 University of Norfolk ONFilm Festival),  the Massachusetts Institute of Technology Lecture Series Committee on 23 March & 24 March 2007,  and  Harvard Law School on 3 April 2007 (as part of a series on nonviolence). 

===Home media and screenplay=== documentary on Munna meets Bapu." 

Lage Raho Munnabhai&nbsp;— The Original Screenplay was released in December 2009. Published by Om Books International in association with Vinod Chopra Films, it was launched at an event with the original cast and crew. The text includes an introductory note by Abhijat Joshi which details the drafting of the screenplay. It also includes a number of stills from the film as well as character profiles.  

==Reception==

===Critical response===
Lage Raho Munna Bhai was an acclaimed film.     gave four out of five stars stating that, "everything about this film works   Its rare to see a film that bounces between humour and sentiment so seamlessly. And it is rarer still to see characters become etched in the memory so enduringly that audiences become almost protective of them. Its testimony both to the quality of the writing and the performances, that Munna and Circuit have taken on a life of their own."  Vinayak Chakravorty of   parameters. Lage  raho, guys." The Tribune memorable films on Mahatma Gandhi by distinguished directors, namely Richard Attenborough and Shyam Benegal;one offering a respectful cinematic acquaintance and the other being didactic but inspiring. For all their earnestness, neither film stirred the popular imagination like LRM has done now."    Vaidyanathan from BBC declared that, "Lage Raho is not only as good as Munna Bhai M.B.B.S.|MBBS, but much better" calling it "a brilliant emotional roller coaster ride."   Jeremy Page of The Times discussed its enormous popularity upon release and noted the "serious point   about the need for tolerance, restraint and self-sacrifice."   
 Kabir Khan cited Lage Raho Munna Bhai as a model film for him as it "had an issue, but it was never once in your face. Rajkumar Hirani kept it all so subtle and yet conveyed the message so well. It was as commercial as it gets and audiences too were thoroughly entertained. Thats the way to make movies because it not just made all parties happy but also had a satisfied director at the end of it all." 

According to Tushar Gandhi, Gandhis great-grandson, it introduced the philosophies of Gandhi to a new generation, adding that "Bapu wouldve spoken the language of Gandhigiri if he were alive today. I really feel this film says something that needs to be told." 

Other critics offered more negative reviews. Ajit Duara argues in The Hindu that "the accomplished cultural sophistication and political genius of Mohandas Karamchand Gandhi has to be dumbed down to the astoundingly moronic levels of Lage Raho Munnabhai."  Filmmaker Jahnu Barua was also critical, stating that "Gandhian philosophy is serious business and Lage Raho Munna Bhai is not the right way to show it."  Jug Suraiya of The Times of India wrote that "thanks to Munnabhai, at best what exists of Gandhism is Gandhigiri, a watered down, Dale Carnegies How to Win Friends and Influence People version of the original." 

===Box office===
Lage Raho Munna Bhai was the third highest grossing Bollywood film of 2006, earning   gross in India alone and was rated a "Blockbuster".  It was also financially successful overseas,  earning   gross in the United Kingdom,   gross in North America, and   gross for the rest of the overseas proceeds, for a total of  . {{Cite news| url=http://www.boxofficeindia.com/cpages.php?pageName=overseas_earners | title=Overseas Earnings (Figures in Ind Rs)
|publisher=boxofficeindia.com| accessdate=2007-04-26 |archiveurl=http://web.archive.org/web/20131029203113/http://boxofficeindia.com/cpages.php?pageName=overseas_earners |archivedate=2014-10-29}}  Its total worldwide lifetime gross is  . 

===Accolades===
 
 National Film 2007 Academy Award for Best Foreign Film.  Although ultimately losing to Rang De Basanti as Indias official submission, the producers submitted it as an independent entry. However, neither film received an Oscar nomination. 

==Soundtrack==
{{Infobox album
| Name = Lage Raho Munna Bhai
| Type = soundtrack
| Artist =Shantanu Moitra
| Released =  
| Recorded = 
| Genre = Film soundtrack
| Length = 
| Label = T-Series
| Producer = 
| Last album  = Yahaan (2005)
| This album  = Lage Raho Munnabhai (2006)
| Next album  = Khoya Khoya Chand  (2007)
}}
{{Album ratings
| rev1 = Rediff
| rev1Score =       
| rev2 = Planet Bollywood
| rev2Score =       
}}

Swanand Kirkire won the National Film Award for Best Lyrics in 2007 for the song "Bande Mein tha Dum." 

{{Track listing
| extra_column    = Singers
| title1          = Lage Raho Munna Bhai
| extra1          = Vinod Rathod
| length1         = 4:25
| title2          = Samjho Ho Hi Gaya
| extra2          = Sanjay Dutt, Arshad Warsi, Vinod Rathod
| length2         = 3:31
| title3          = Aane Char Aane Karunya
| length3         = 4:30
| title4          = Pal Pal...Har Pal
| extra4          = Sonu Nigam and Shreya Ghoshal
| length4         = 4:32
| title5          = Bande Mein Tha Dum...Vande Mataram
| extra5          = Sonu Nigam, Shreya Ghoshal, Pranab Kumar Biswas
| length5         = 4:02
| title6          = Bande Mein Tha Dum
| note6           = Instrumental
| length6         = 3:15
| title7          = Aane Char Aane
| note7           = Remix
| length7         = 3:55
| title8          = Lage Raho Munnabhai
| note8           = Remix Shaan
| length8         = 4:18
}}

==Proposed sequel== Sanjay Dutts 1993 Mumbai blasts case and sentenced him to 5 years imprisonment. Thus, Hirani has shelved a proposed sequel to the earlier films, Munna Bhai Chale Amerika. He will instead develop a third installment to the Munna Bhai series with a different storyline, after Dutt is released from prison. 

==See also==
 
*List of artistic depictions of Mohandas Karamchand Gandhi

==Further reading==
 
*" ." Daily News and Analysis, November 21, 2014.
*Allagh, Harjeet Kaur. " ." The Hindu. 31 January 2009.
*Ahmed, Omar. "Bollywood: Carry ons a masterpiece ; LAGE RAHO MUNNA BHAI VERDICT: *****;  ." Birmingham Mail, 8 September 2006: 44.
*Chaturvedi, Anshul. " ." The Times of India. 2 October 2008.
*Ghosh, Sohini (October 2006). " " Communalism Combat 119
*National Public Radio, " ," 25 February 2009 (audio interview including section on Lage Raho Munna Bhai).
*Yelaja, Prithi. " ." Toronto Star, 2 October 2007.
 

==Notes==
 

==External links==
 
 
 
* 
*  - Vinod Chopra Films
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 