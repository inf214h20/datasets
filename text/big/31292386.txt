Chattam
 
 
{{Infobox film
| name           = Chattam
| image          = 
| writer         = Vishaka Takies    
| producer       = Natti Kumar   Tummalapalli Rama Satyanarayana
| director       = Arun Prasad P.A.|P.A. Arun Prasad
| starring       = Jagapathi Babu Vimala Raman
| music          = M. M. Srilekha
| cinematography = Jaswanth
| editing        = Gautham Raju
| studio         = Vishaka Takies
| distributor    = 
| released       =  
| runtime        = 1:48:55
| country        = India
| language       = Telugu
| budget         = 
}}
 action Telugu film produced by Natti Kumar & Tummalapalli Rama Satyanarayana on Vishaka Talkies banner, directed by Arun Prasad P.A.|P.A. Arun Prasad. Starring Jagapathi Babu, Vimala Raman in lead roles and music composed by M. M. Srilekha. The film recorded as flop at box office. 

== Plot ==
Gowri Shankar (Jagapathi Babu) is a circle inspector of Punjagutta and is known as a heavy corrupt police officer. Rangarao (Jeeva (Telugu actor)|Jeeva) is his assistant and his daughter Sindhu (Vimala Raman) is an aerobics teacher. Once, two sons of a big industrialist (Prasad Babu) kill two girls for not accepting their love by pouring acid on them. Soon, the two get killed by some unidentified person in the same fashion. The industrialist uses his political influence and get the case investigated by the CB-CID. The police commissioner (Chalapathi Rao) calls Gowri and tells him to handover all the evidences to the CB-CID officer (Murali Sharma). Even as the CID officer is investigating, another two persons get murdered in the same fashion. The CID officer makes an in-depth study into the case and finally comes to the conclusion that it is Gowri, who is behind the these murders. At this juncture, Gowri leaves for Mumbai and kills some gangsters who were kidnapping and selling young girls and exporting them to other countries. After he kills all the gangsters, the Mumbai police arrest him and keeps him in a jail, where an international terrorist Kasab from Pakistan was placed. The CBI officer gets a doubt and warns the jailer that Gowri intentionally surrendered to the police as he wanted to kill Kasab. What happens next should be seen on-screen.   

== Cast ==
 
* Jagapati Babu as Gowri Shankar
* Vimala Raman as Sindhu
* Ashish Vidyarthi 
* Murali Sharma as CID Officer
* Amit Dhawan as Ajmal Kasab
* Chalapathi Rao
* Rao Ramesh
* Amith
* Vijayachander
* Prasad Babu Kondavalasa  Jeeva as Ranga Rao
* Raghava
* Gundu Hanumantha Rao
* Gundu Sudarshan
* Duvvasi Mohan
* Junior Relangi
* Asha Saini as item number
* Siva Parvathi
* Meena
* Geetha Singh
 

==References==
 

 
 
 
 
 
 
 
 
 

 