A Very Good Young Man
{{Infobox film
| name           = A Very Good Young Man
| image          = 
| alt            = 
| caption        =
| director       = Donald Crisp
| producer       = Jesse L. Lasky Walter Woods
| starring       = Bryant Washburn Helene Chadwick Julia Faye Sylvia Ashton Jane Wolfe Helen Jerome Eddy Wade Boteler
| music          =  	
| cinematography = Charles Edgar Schoenbaum
| editor         = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent Walter Woods. The film stars Bryant Washburn, Helene Chadwick, Julia Faye, Sylvia Ashton, Jane Wolfe, Helen Jerome Eddy and Wade Boteler. The film was released on July 6, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Bryant Washburn as LeRoy Sylvester
*Helene Chadwick as Ruth Douglas
*Julia Faye as Kitty Douglas
*Sylvia Ashton as Mrs. Douglas
*Jane Wolfe as Mrs. Mandelharper
*Helen Jerome Eddy as Osprey Bacchus
*Wade Boteler as Tom Hurley
*Anna Q. Nilsson as Viva Bacchus
*Noah Beery, Sr. as Blood
*Edmund Burns as Adrian Love 
*Mayme Kelso as Mrs. Love Charles West	

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 