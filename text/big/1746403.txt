The Most Beautiful
 
{{Infobox film
| name = The Most Beautiful
| image = Ichiban_utsukushiku_poster.jpg
| director = Akira Kurosawa
| producer = Motohiko Ito Jin Usami
| writer = Akira Kurosawa
| starring = Yōko Yaguchi Takashi Shimura Takako Irie Ichiro Sugai Toho Studios
| distributor = Toho Company
| released =  
| runtime = 85 minutes
| language = Japanese
| country = Japan
| budget =
| music = Seiichi Suzuki
}} directed by Akira Kurosawa.

The film is set in an optics factory during the Second World War.

==Plot==
The film depicts the struggle for the workers at a lens factory to meet production targets during World War II. They continually drive themselves, both singly and as a group, to exceed the targets set for them by the factory directors.

==External links ==
* 
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 

 