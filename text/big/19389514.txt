A Lad an' a Lamp
{{Infobox film
| name           = A Lad an a Lamp
| image          =Lad an a lamp TITLE.JPEG
| image size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         =
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 16 51" 
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Robert F. McGowan.  It was the 119th (32nd talking episode) Our Gang short that was released.

==Plot==
Fascinated by the story of Aladdin and his magic lamp, the gang gather together with several gasoline and kerosene lamps and lanterns and a few electric lamps hoping that by rubbing them vigorously, a genie will appear. Thanks to a series of coincidences—not least of which involves a friendly stage magician—the kids become convinced that they have succeeded in invoking Aladdin. But their excitement turns to dismay when Stymie believes Spanky has transformed his kid brother Cotton into a monkey.

==Cast==
===The Gang=== Matthew Beard as Stymie
* Dorothy DeBorba as Dorothy
* Bobby Hutchins as Wheezer
* George McFarland as Spanky Dickie Moore as Dick
* Bobbie Beard as Cotton
* Georgie Billings as Georgie
* Dickie Jackson as Dickie
* John Collum as Uh-huh
* Bobby DeWar as Our Gang member
* Henry Hanna as Our Gang member
* Pete the Pup as Himself

===Additional cast===
* Donald Haines as Toughie
* Harry Bernard as Officer / Store proprietor (scene deleted)
* Dick Gilbert as Officer / Dick, construction worker Jack Hill as Audience member / Officer
* Florence Hoskins as Cooks girlfriend
* James C. Morton as Officer
* Lillian Rich as Introductory narrator
* Philip Sleeman as The Magician
* Charley Young - Fruit vendor
* Jiggs the Chimpanzee as Himself
* Harry Bowen as Audience member
* Efe Jackson as Pedestrian Jim Mason - Audience member

==Cast notes== Hook and Ladder, Free Wheeling, and Birthday Blues.

==Critique==
Despite a sequence in which Spanky enjoys a free meal at a lunch counter, courtesy of a trained monkey, A Lad an a Lamp suffers from an unusual dose of racist humor that seems inappropriate when viewed in the 21st century. For this reason, A Lad an a Lamp has been withdrawn from the "Little Rascals" television package.    It is currently available in its entirety on VHS and DVD.

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 