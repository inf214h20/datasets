The Squaw Man (1931 film)
:For other uses, see: The Squaw Man (disambiguation).
{{Infobox film
| name           = The Squaw Man
| image          = The Squaw Man (1931).jpg
| image_size     =
| caption        =
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| based on       = The Squaw Man (play)
| screenplay     = Lucien Hubbard Lenore J. Coffee Elsie Janis
| writer         = Edwin Milton Royle
| narrator       =
| starring       = Warner Baxter Lupe Velez Eleanor Boardman
| music          = Herbert Stothart
| cinematography = Harold Rosson
| editing        = Anne Bauchens
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 107 minutes
| country        = United States English
| budget         =
| gross          =
}}
The Squaw Man (1931 in film|1931) is a film directed by Cecil B. DeMille. It was the third version of the same play that he filmed, and the first in sound. It stars Warner Baxter in the leading role.

==Plot== Dickie Moore). Years later Henry arrives there with his wife Lady Diana (Eleanor Boardman), with whom James has been secretly in love.

==Cast==
* Warner Baxter as James Jim Wingate, aka Jim Carston
* Lupe Vélez as Naturich
* Eleanor Boardman as Lady Diana Kerhill
* Charles Bickford as Cash Hawkins
* Roland Young as Sir John Johnny Applegate
* Paul Cavanagh as Henry, Earl of Kerhill
* Raymond Hatton as Shorty
* Julia Faye as Mrs. Chichester Jones
* DeWitt Jennings as Sheriff Bud Hardy
* J. Farrell MacDonald as Big Bill (as J. Farrell McDonald)
* Mitchell Lewis as Tabywana Dickie Moore as Little Hal Carston
* Victor Potel as Andy

==Production and release== in 1914 in 1918. Billy the Kid and The Big Trail were being released.
 The Sign of the Cross which kick-started his career again.

==References==
 

==Bibliography==
* Birchard, Robert S. Cecil B. DeMilles Hollywood. University Press of Kentucky, 2004.

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 