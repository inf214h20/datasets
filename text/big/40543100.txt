Fasten Your Seatbelt (film)
{{Infobox film
| name           = Fasten Your Seatbelt 
| image          = Fasten_Your_Seatbelt_poster.jpg
| director       = Ha Jung-woo
| producer       = Kim Han-gil
| writer         = Ha Jung-woo
| starring       = Jung Kyung-ho
| music          = Kim Jung-bum
| cinematography = So Jung-oh  
| editing        = Kim Woo-il Fantagio Pictures CJ Entertainment
| released       =  
| runtime        = 94 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =    
| film name      = {{Film name
 | hangul         = 롤러코스터 
 | hanja          = 
 | rr             = Rolleo Koseuteo 
 | mr             = }}
}}
Fasten Your Seatbelt ( ; lit. "Rollercoaster") is a 2013 South Korean comedy film written and directed by actor Ha Jung-woo, in his directorial debut.     The film made its world premiere at the 18th Busan International Film Festival,  and was released in theaters on October 17, 2013.   

==Plot== Gimpo Airport with a Hallyu star on board runs into an unexpected storm and is in danger of crashing. A plane full of absurd characters―both crew members and passengers such as a businessman, a monk and a paparazzo—go through a series of comical shenanigans.

==Cast==
*Jung Kyung-ho as Ma Joon-gyu captain
*Kim stewardess
*Choi Kyu-hwan as Kim Hyeon-gi, tabloid reporter
*Kim Gi-cheon as Heo Seung-bok, Zha Cai Airlines chairman
*Kim Byung-ok as monk chief purser
*Kim Sung-kyun as Jo-rong, steward
*Ko Sung-hee as Minamito, Japanese stewardess
*Kim Ye-rang as Chan-mi, stewardess
*Lee Ji-hoon as ophthalmologist
*Son Hwa-ryung as Chun-nyeo, chairmans executive assistant
*Hwang Jung-min as Cha Bok-soon, middle-aged female fan
*Im Hyun-sung as co-pilot
*Kang Han-na
*Kim Joon-kyu
*Lee Soo-in
*Kim Jae-young

==Production==
Ha Jung-woo said hed always had the desire to study about film more, so he seized the opportunity to approach this movie not as an actor but as a director.   "Having pondered what kind of film I should make, I came to the conclusion that I would only be satisfied if the audience could enjoy it and get some laughs. This naturally led me to write a comedy," Ha said. 

Ha reportedly wrote the script based on his friend and fellow actor Ryoo Seung-bums real-life experience. The flight from Seoul to Tokyo normally only takes about two hours, but Ryoo once experienced plane turbulence that went on for almost seven hours, during which he genuinely thought he was going to die. 
 mandatory military service, and his characters appearance and wardrobe was inspired by K-pop star G-Dragon.  Most of the actors in the cast have also worked with Ha before—even the music director, a colleague he first met while working on My Dear Enemy in 2008. 
 read the screenplay every day with the entire cast for two months, conducting intensive rehearsals as if they were preparing for a stage play.   

==Box office==
Though ultimately not a big hit, due to its low budget Fasten Your Seatbelt broke even in just four days, attracting 200,000 viewers in the first six days of its release. 

==Awards and nominations==
2014 8th Osaka Asian Film Festival
*Most Promising Talent: Ha Jung-woo 
 50th Baeksang Arts Awards
*Nomination - Best New Director (Film): Ha Jung-woo

2014 34th Golden Film Festival
*Best New Actor: Jung Kyung-ho

==References==
 

==External links==
*   
*   
*   
* 
* 
* 

 
 
 
 
 
 