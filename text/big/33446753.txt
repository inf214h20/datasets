Aham (film)
 
{{Infobox film
| name           = Aham
| image          = Aham_film.jpg
| caption        = Aham DVD cover
| alt            =
| director       = Rajeevnath
| producer       = Rajeevnath
| writer         = Rajeevnath (story) Venu Nagavally (dialogues) Urvasi   Ramya Krishna
| music          = Raveendran
| cinematography = Santhosh Sivan
| editing        = Ravi
| studio         = Sreesankara Arts
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         = Rs.46 Lakhs
| gross          =
}}
 Malayalam film Urvasi and Ramya Krishna in the lead roles.

==Plot==
Aham is the story of Sidharthan (Mohanlal), who grew up in a disciplined and strict environment and tried to lead his family life in similar lines. But his wife (Urvasi (actress)|Urvasi) was not able to adjust with his crazy lifestyle. She wanted to become self-dependent but this created suspicion in Siddharthans mind. In a resulting argument and physical struggle, She got pushed from the balcony and goes into a coma. This makes Siddharthan lose his mental balance. Later he ends up in a mental asylum and leads a spiritual life there. At the end Siddharthan commits suicide.

==Cast==
* Mohanlal as Sidharthan Urvasi
* Ramya Krishna
* Neena Gupta
* Suresh Gopi
* Nedumudi Venu
* Jagathy Sreekumar Vaishnavi
* Rajeev Rangan
* Maniyanpilla Raju

==External links==
 

 
 
 


 