The Floating Castle
{{Infobox film
| name           = The Floating Castle
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = のぼうの城
| director       = Shinji Higuchi  Isshin Inudo
| producer       = Osamu Kubota  Shinji Ogawa
| writer         = Ryo Wada
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Mansai Nomura
| music          = Koji Ueno
| cinematography = Shoji Ebara  Motonobu Kiyoku
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 144 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2012 Japanese historical-drama film directed by Shinji Higuchi and Isshin Inudo, starring Mansai Nomura.
 Hojo clan. Against insurmountable odds, Narita Nagachika, the fortresss castellan, leads a group of 500 men against 5 000 men led by Ishida Mitsunari, part of Toyotomi clans greater army of 20,000 samurai. 

==Cast==
* Mansai Nomura as Nagachika Narita 
* Nana Eikura as Kaihime
* Hiroki Narimiya as Sakamaki
* Tomomitsu Yamaguchi as Izumi Koichi Sato as Tanba  Hideyoshi Toyotomi Masaie Natsuka Yoshitsugu Otani Mitsunari Ishida
* Masahiko Nishimura as Ujinaga Narita 
* Sei Hiraizumi as Yasusue Narita 
* Isao Natsuyagi as Monk 
* Takeo Nakahara as Hojo Ujimasa
* Honami Suzuki as Tama
* Gin Maeda as Tahee 
* Akiyoshi Nakao as Kazou
* Machiko Ono as Chiyo 
* Mana Ashida as Chidori

==Reception==
Mark Adams of Screen International gave the film a favorable review, describing the film as "  epic period action-comedy packed with wonderfully over-the-top characters, great production values and some spectacular sequences". 

==Awards== Japan Academy Prize, winning one for Best Art Direction.  Additionally it was nominated for three Asian Film Awards. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 
 