This Old Cub
{{Infobox Film name        = This Old Cub image       = Thisoldcubdvd.jpg director    = Jeff Santo producer    = Joe Mantegna Jeff Santo Walgreens Chicago Tribune writer      = Jeff Santo starring    = Ron Santo Bill Murray Joe Mantegna Gary Sinise distributor = Big Joe Productions JDRF Sony Pictures Home Entertainment (DVD) released    = August 25, 2004 runtime     = 122 min. language    = English budget      = unreleased purchase    =  
}}
 Pat Hughes, WGN Radio MLB Hall of Fame a few months earlier.  A portion of all proceeds from the release of This Old Cub are donated to the Juvenile Diabetes Research Foundation. The film has raised over a half-million dollars for the JDRF.  Cub shortstop Ernie Banks, Gary Sinise, Bill Murray, former Chicago Bears linebacker Doug Buffone, and many others are interviewed in the film, which is narrated by actor Joe Mantegna.
 Bill Holden to engage a 2100-mile walk from Arizona to Wrigley Field to raise funds for the JDRF.  Holden covered at least 12 miles each day, crossed six states, and battled his arthritis during the nearly seven-month trek.  Followed by the media, Holden arrived at Wrigley on July 1, 2005 where he threw out the first pitch and joined Santo in singing "Take Me Out to the Ball Game".  The venture raised over $250,000 for the charity, and caused a dramatic spike in sales of the DVD.  Derek Schaul, the leader of Chicago Cubs Bleacher Bums, wears a shirt to every game stating, "I walk for the cure because Ronnie cant!  Go Cubbies!"

This Old Cub was co-produced by Walgreens Drug Stores and the Chicago Tribune, both heavy sponsors of the JDRF.

==See also==

*List of films featuring diabetes

==External links==
* 
* 
* 
* 
*  
*  

 

 
 
 
 
 
 
 