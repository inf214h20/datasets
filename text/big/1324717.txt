Dil Ne Jise Apna Kahaa
{{Infobox film 
| name           = Dil Ne Jise Apna Kahaa दिल ने जिसे अपना कहा  
| image          = Dnjak.jpg
| caption        = Theatrical release poster
| director       = Atul Agnihotri
| producer       = Sunil Manchanda Mukesh Talreja
| story          = Atul Agnihotri
| screenplay     = Alok Upadhyaya Purnendu Shekhar
| starring       = Salman Khan Preity Zinta Bhoomika Chawla
| music          = A. R. Rahman Himesh Reshammiya 
| cinematography = Kishore Kapadia Stephen Fernandes
| editor         = Keshav Naidu
| distributor    = Orion Pictures MAD Entertainment Ltd. Reel Life Entertainment Pvt. Ltd.
| released       = September 10, 2004
| runtime        = 155 min. Hindi
| country        = India
| awards         = 
| budget         =
}}

Dil Ne Jise Apna Kahaa ( ,  }},   Bollywood film directed by Atul Agnihotri. The film was Agnihotris first film as a director. The film stars Salman Khan, Preity Zinta, Bhoomika Chawla, Delnaaz Paul, Riya Sen and Helen (dancer)|Helen. It is a remake of an English Movie Return to Me (2000)Starring David Duchovny, Minnie Driver.

==Plot==
Dil Ne Jise Apna Kahaa begins with Rishabh (Salman Khan) and Pari (Preity Zinta) who are deeply in love. He is a wealthy young man, working in an advertising agency while she is a hardworking, dedicated doctor. They marry and soon Pari is pregnant. Pari has a dream to create a hospital for children. Tragically, she is involved in an accident and dies in hospital. Paris last wish was to donate her heart to her patient Dhani (Bhoomika Chawla). Rishabh is devastated and opposes the plan to donate the heart; he goes ahead with Paris last request: the creation of a childrens hospital.

Dhani is cured, much to the joy of her family and her grandmother (Helen (dancer)|Helen). Rishabh has gone into depression but soon comes across Paris project to build a hospital for children. He begins to develop the hospital. Soon enough Rishabh and Dhani come across each other, and she feels an instant attraction to him. Rishabh ignores her advances as he is still very much in love with Pari. Rishabh does not know that Paris heart was given to Dhani but soon he realizes that. When he does, Dhani faints. While she is in the hospital, with doctors struggling to restart her broken heart, he falls in love with her. He tells her that he loves her, and if she loves him, she will pull through. She does, and the two get together.

==Cast==
*Salman Khan as Rishabh
*Preity Zinta as Pari
*Bhoomika Chawla as Dhani
*Rati Agnihotri as Dr. Shashi Rawat  Helen as Dhanis grandmother  
*Renuka Shahane as Rishabhs Sister  
*Aashif Sheikh as R. Tripathi
*Riya Sen as Kaveri 
*Delnaaz Paul as Dhillo

==Box office==
The film had a poor opening at the box office and in the end only grossed Rs. 7 crores. It was ultimately declared a flop. 

== Soundtrack ==
A. R. Rahman was initially signed as the music composer. He had composed 3 tracks and an instrumental, before he got busy and opted out. Himesh Reshammiya was signed in to quickly compose the rest of the tracks. A. R. Rahman accepted no remuneration for the film.
{{Infobox album |  
| Name           = Dil Ne Jise Apna Kaha   
| Cover          = 
| Caption        = 
|Type=soundtrack
| Artist         = A. R. Rahman & Himesh Reshammiya 
| Released       = 2004  (India) 
| Recorded       = Panchathan Record Inn Feature film soundtrack 
| Length         = 41:03  
| Label          = T-Series
| Producer       = 
| Certification  = 
| Chronology     =  A. R. Rahman
| Last album     = "Naani" (2004)
| This album     = "Dil Ne Jise Apna Kaha" (2004)
| Next album     = "Swades" (2004)
| Misc           = {{Extra chronology
| Artist         = Himesh Reshammiya
| Type           = albums
| Last album     = " " (2004)
| This album     = "Dil Ne Jise Apna Kaha" (2004)
| Next album     = " "  (2004)
}}
}}
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! #!! Song !! Artist(s) !! Composer !! Lyricist!! Length
|-
| 1
| Dil Ne Jise Apna Kaha
| Sujata Trivedi, Kamaal Khan
| A. R. Rahman
| Mehboob
| 06:18
|-
| 2
|Bindiya Chamakne Lagi
| Alka Yagnik, Udit Narayan
| Himesh Reshammiya Sameer
| 06:22
|-
| 3
| Yeh Dil To Mila Hai
| Alka Yagnik, Sonu Nigam
| Himesh Reshammiya Sameer
| 05:32
|-
| 4
| Jaane Bahara
| Kamaal Khan, Sadhana Sargam
| A. R. Rahman
| Mehboob
| 05:14
|-
| 5
| Go Balle Balle
| Krishnakumar Kunnath|KK, Alisha Chinoy, Jayesh Gandhi
| Himesh Reshammiya
| Mehboob
| 05:00
|-
| 6
| Zindagi Hai Dua Karthik
| A. R. Rahman
| Mehboob
| 05:25
|-
| 7
| Meri Nas Nas Mein
| Udit Narayan, Alka Yagnik, Jayesh Gandhi
| Himesh Reshammiya Sameer
| 05:36
|-
| 8
| Dil Ne Jise Apna Kaha (Instrumental)
| Instrumental
| A. R. Rahman
| Instrumental
| 02:20
|}

==References==
 

== External links ==
*  

 
 
 
 