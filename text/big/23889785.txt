Sting of Death
{{Infobox film
| name           = Sting of Death
| image          = 
| director       = William Grefe
| producer       = Joseph Fink Richard S. Flink Juan Hildago-Gato Joe Morrison John Vella Jack Nagle
| cinematography = Julio C. Chávez 
| studio         = Essen Productions Inc.
| distributor    = Image Entertainment Something Weird Video
| released       = October 17, 1965
| runtime        = 80 min
| country        = United States
| language       = English 
}}	 

Sting of Death is a 1965 B movie|B-horror film directed by William Grefe and written by Al Dempsey and Herschell Gordon Lewis.

==Plot== marine biologist currently studying jellyfish. The kids, Louise, Jessica, Donna, Susan and Karen, stay with a man named Dr. John Hoyt. The kids insult and ridicule Dr. Richardsons assistant, Egon, who is in love with Karen. Egon creates a murderous jellyfish mutant which begins to go on a killing spree. Egon eventually brings Karen to his secret laboratory in the swamp. Dr. Richardson then arrives, keeping the monster at bay as he and Karen escape the lab as one of the machines causes the place to explode, destroying both Egon and his aquatic, bloodthirsty creation.

==Cast== Joe Morrison as Dr. John Hoyt
* Valerie Hawkins as Karen Richardson John Vella as Egon Jack Nagle as Dr. Richardson
* Sandy Lee Kane as Louise
* Deanna Lund as Jessica
* Lois Etelman as Donna Blanche Devereaux as Susan
* Doug Hobart as the Jellyfish Man

==Reception==
The film was negatively received for its poor special effects and bland, unoriginal storyline. It currently has a rating of 5.0/10 on Internet Movie Database|IMDb. The Bad Movie Report gave the film a negative rating of 2, much like B-Movie Central which gave it the rating of 2 "bees".  

==References==
 

==External links==
*  

 
 
 

 