Street Dreams (film)
{{Infobox Film
| name = Street Dreams
| director = Chris Zamoscianyk
| image	=	Street Dreams FilmPoster.jpeg
| producer = Scott Mellini Chris Zamoscianyk Rob Dyrdek Jason Reinert Sal Masekela
| writer = Elisa Delson Rob Dyrdek Nino Scalia
| narrator =  Paul Rodriguez, Terry Kennedy   Jason Reinert
| music =
| cinematography = 
| editing = 
| distributor = Berkela Films
| released = June 12, 2009
| runtime = 
| DVD Release = October 26, 2010 USA
| English
| Gross = $84,944 
}}
 American film directed by Chris Zamoscianyk, produced by Rob Dyrdek, Jason Bergh and Sal Masekela. and written by Elisa Delson, Rob Dyrdek, and Nino Scalia.
 Paul Rodriguez) has a dream of being sponsored and one day going pro. He is an up-and-coming street skateboarder from Chicago with all the talent but has the world against him. Parents, friends and schoolmates cant understand how Derrick has so much passion for something that has no future in their eyes. As everyone pushes him around, including his own father, girlfriend, and even one of his closest friends, he doesnt give up on both achieving his dreams, and proving everyone wrong.

==Plot== Paul Rodriguez) has a dream of being sponsored and one day going pro. He is an up-and-coming street skateboarder from Chicago with all the talent but has the world against him. He and his fellow skateboarding friends Cash (Ryan Dunn), Reese (Terry Kennedy), and Troy (Rob Dyrdek), as well as filmographer friend Mikey (Adam Wylie), spend their days skating at spots around the city, being pursued by cops for skating where they shouldnt be, and planning how to make names for themselves. In Derricks case, this means landing one special trick on-camera for a sponsorship video that has never been done before, affectionately dubbed the N.A.C. (Not A Chance) by his friends. Unfortunately, the handrail at the local university he needs to practice his trick on has been skate-proofed, preventing him from making that goal a reality.

At home, Derricks passion has started interfering with his high school education, causing tension between him and his father (Yancey Arias). At the same time, Derricks preppy, partying girlfriend Samantha (Jordan Valley) is starting to make him lose focus of his goals with her high-maintenance personality. After the stress between Derrick and Samantha comes to a boil and they break up, he joins his friends on a trip to Ohio to visit a skate park with the perfect rail to practice the N.A.C., and so they all can practice for the coming Tampa AM amateur skateboarding contest in Florida. However, when Troy notices Derrick gaining more attention from sponsors and professional photographers while practicing the N.A.C., his jealousy takes over and forces the group to pack up and leave before Derrick can successfully land his trick. Thankfully, before leaving, Derrick has the chance to meet well-renowned skater Eric (Ryan Sheckler), and attractive older sister Taylor (C.C. Sheffield), and learns Eric will be competing in the Tampa Am contest as well.

When Derrick returns home to a furious father and the boring school routine, the only thing Derrick can think about is finally landing the N.A.C., so he comes up with a plan: taking an angle grinder to the welded knobs on the university rail, and skate it all night. To do this, he enlists the help and trust of his friends to watch for cops as he cuts the knobs off. All goes according to plan, until cops eventually show up, and Troy, who saw them coming, deliberately fails to inform the rest of the group, resulting in Derrick and Mikey getting arrested. This causes the tension and anger between Derrick and his father to reach a critical point, resulting in thrown fists and Derrick running away with his friends to Florida to compete in the Tampa AM.

Arriving in Tampa, the gang immediately gets into various shenanigans; Derrick plays a game of S.K.A.T.E. with some other skateboarders there for the competition, Cash and Reese cause a fiery explosion in the parking lot of their motel, and after dropping off their stuff in their room, head to the Ybor City strip to party. However, this quickly goes awry after Cash and Troy get too drunk on shots and cause a fistfight that ends with Cash being taken to jail for the night. Finally Derrick has had enough of Troys ego and calls him out on his faults in front of the group, prompting Troy to confess to letting the cops arrest Derrick and Mikey back at the rail. In a rage, Derrick abandons the group, just in time for Eric and Taylor to find him and allow him a place to stay in their motel room. After having a talk with Taylor, Derrick calls his father to let him know where he is and that hes okay, and his father wishes him luck in the competition.

The next day, Derrick arrives at the Tampa AM with Eric and Taylor, where Mikey, Reese, and a freshly-out-of-jail Cash happily meet up with him. During pre-competition practice though, Troy makes it abundantly clear that he and Derrick are enemies here, shoulder-checking Derrick and knocking him off his board. As the competition begins, Troy appears to be fairing better than Derrick, landing all of his tricks with Derrick only landing three. However, because Derricks tricks were harder and Troy played it easy with more basic tricks, Derrick leaves Troy behind as he qualifies first place for the finals. Troy is initially furious, but finally Cash steps up and puts him in his place, as well as a headlock. With the dust settled, all of Derricks friends wish him luck on his final run.

With the support of his friends carrying him, Derrick advances to the finals with full confidence. His plan here is simple: to land the N.A.C. in his final run. As he approaches his end goal he is certain of his ability to do so, but even at the last moment, Derrick fails once again trying to land it. However, even though the clock has run out and it wont count for the competition, the judges and fellow competitors want to see Derrick try again. With all of the skateboarding world watching, Derrick finally lands the N.A.C., with plenty of footage from Mikey for his sponsorship video. Derrick is instantly landed a sponsorship on the spot, and after trading phone numbers with Taylor, he hops in Troys van with the gang to ride back to Chicago in victory.

==Cast==

*Paul Rodriguez Jr as Derrick Cabrera 
*Ryan Dunn as Cash 
*Rob Dyrdek as Troy 
*Terry Kennedy as Reese 
*Ryan Sheckler as Eric 
*Adam Wylie as Mikey Robbins
*Brendan Miller as Brad 
*C.C. Sheffield as Taylor 
*Jordan Valacich as Samantha (as Jordan Valley)
*Yancey Arias as Tim Cabrera 
*Kate McGregor-Stewart as Virginia 
*Peter Reckell as Dan Reynolds 
*Rainbow Alexander  as April 
*Al Woodley as Cop 1 (as Alfred Woodley) 
*Luke Van Pelt as Bruce

==References==
 
http://www.the-numbers.com/movies/2009/0STDR.php

==External links==
*  
* 

 
 
 
 
 
 
 
 
 