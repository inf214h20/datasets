Bambuti (film)
{{Infobox film
| name           = Bambuti
| image          = 
| image_size     = 
| caption        = 
| director       = Bernhard Grzimek Michael Grzimek
| producer       = 
| writer         = Bernhard Grzimek Heinz Kuntze-Just
| narrator       = 
| starring       = See below
| music          = Wolfgang Zeller
| cinematography = Hermann Gimbel Michael Grzimek Herbert Lander
| editing        = Klaus Dudenhöfer
| studio         = 
| distributor    = 
| released       = 1956
| runtime        = 79 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Bambuti (originally Kein Platz für wilde Tiere) is a 1956 West German film directed by Bernhard Grzimek, Michael Grzimek.

Known as No Place for Wild Animals in the USA, the film documents the need for nature reserves in Africa.

== Plot summary ==
 

== Cast ==
*Viktor de Kowa as Narrator (voice)
*Carleton Young as Narrator (voice: English version)

== Soundtrack ==
 

== External links ==
* 
*  (German narration)

 
 
 
 
 
 
 


 