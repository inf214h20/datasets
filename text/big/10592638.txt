Mere Sanam
{{Infobox Film
| name           = Mere Sanam
| image          = MereSanam.jpg 
| image_size     =
| caption        = 
| director       = Amar Kumar
| producer       = G.P. Sippy
| writer         = Narendra Bedi (Story) Rajinder Singh Bedi (Screenplay & Dialogue)
| narrator       =  Mumtaz Pran Pran
| music          = O.P. Nayyar
| cinematography = K. Vaikunth
| editing        = M. S. Shinde
| distributor    = 
| released       = 1965
| runtime        = 
| country        =   India
| language       = Hindi
| budget         = 
}}

Mere Sanam (English: My Lover  ) was a Hindi film released in 1965 starring Asha Parekh, Biswajeet, Pran (actor)|Pran, Rajendra Nath and Mumtaz (actress)|Mumtaz. The film was a hit at the box office, especially because of the musical score by Majrooh Sultanpuri, O.P. Nayyar and Asha Bhonsle-Mohammed Rafi combination. 

The Songs Jaiye Aap Kahan Jayenge, Yeh Hai Reshmi Zulfon Ka Andhera, Pukarta Chala Hoon Main and Humdum Mere Maan Bhi Jao are milestones in Hindi film music.

This film is the unofficial remake of Come September which is a 1961 romantic comedy film directed by Robert Mulligan, and starring Rock Hudson, Gina Lollobrigida, Sandra Dee and Bobby Darin.

== Plot ==

Neena (Asha Parekh) is travelling to a remote holiday spot in the company of her mother and several female friends. During their journey they decide to stay overnight at a lodge. Another lodger, Kumar (Biswajeet), finds their presence undesirable and asks the caretaker to get the surprised group to leave. The caretaker explains that Kumar is mentally unstable and under the impression that he is the owner of the lodge.

After several misunderstandings Kumar and Neena fall in love and express their desire to get married. But subsequently Neena and her mother discover some intimate photographs of Kumar and a young woman named Kammo alias Kamini.

== Soundtrack ==
{{Infobox album |
 Name = Mere Sanam |
 Type = soundtrack |
 Cover =|
 Recorded = | Feature film soundtrack |
 Composer = O.P. Nayyar |
 Lyricist = Majrooh Sultanpuri |
 Label =  Saregama | 
 Reviews = |
 This album = Mere Sanam |
}}

{| class="wikitable sortable"
|-
! S.No !! Title !! Singer(s)
|-
| 1 || Roka Kayi Baar Maine Dil Ki Umang Ko || Mohammed Rafi & Asha Bhosle
|-
| 2 || Humne Toh Dil Ko Aapke Kadmon Pe Rakh Diya|| Asha Bhosle & Mohammed Rafi
|-
| 3 || Pukarta Chala Hoon Main || Mohammed Rafi
|-
| 4 || Jaaiye Aap Kahaan Jaayenge || Asha Bhosle
|-
| 5 || Ye Hai Reshmi Zulfon Ka Andhera || Asha Bhosle
|-
| 6 || Humdum Mere Maan Bhi Jaao || Mohammed Rafi
|-
| 7 || Tukde Hain Mere Dil Ke || Mohammed Rafi
|-
| 8 || Hue Hain Tumpe Aashiq Hum || Mohammed Rafi
|-
| 9 || Haaji Haaji Haaji Arrey Haaji Baba || Asha Bhosle, Mohammed Rafi
|}

==References==
 

==External links==
*  

 
 
 


 