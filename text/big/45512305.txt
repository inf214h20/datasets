Take a Chance (1933 film)
{{Infobox film
| name           = Take a Chance
| image          = Take a Chance poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Monte Brice Laurence Schwab 	
| producer       = 
| screenplay     = Monte Brice Buddy G. DeSylva Laurence Schwab Sid Silvers Richard A. Whiting James Dunn Dorothy Lee Lona Andre
| music          = 
| cinematography = William O. Steiner 	
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 James Dunn, Dorothy Lee and Lona Andre. The film was released on October 27, 1933, by Paramount Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9806E4DC1431E333A25754C2A9679D946294D6CF|title=Movie Review -
  Take a Chance - James Dunn, Lillian Roth and June Knight in a Film of the Musical Comedy Take a Chance. - NYTimes.com|work=nytimes.com|accessdate=26 February 2015}} 
 
==Plot==
 

== Cast ==  James Dunn as Duke Stanley
*June Knight as Toni Ray
*Lillian Roth as Wanda Hill
*Cliff Edwards as Louie Webb
*Lilian Bond as Thelma Green Dorothy Lee as Consuelo Raleigh
*Lona Andre as Miss Miami Beach
*Charles Buddy Rogers as Kenneth Raleigh Charles Richman as Andrew Raleigh
*Robert Gleckler as Mike Caruso Harry Shannon as Bartender
*George McKay as Steve

Vivian Vance appears in an uncredited role as a "dancehall girl".

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 