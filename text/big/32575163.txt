On Fertile Lands
{{Infobox film
| name           = On Fertile Lands
| image          = 
| image size     = 
| caption        = 
| director       = Erden Kıral
| producer       =  
| writer         =  
| starring       =  
| music          =  
| cinematography = Salih Dikişçi
| editing        =
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
}}
 1980 cinema Turkish drama film, directed by Erden Kıral based on a story by Orhan Kemal, featuring Tuncel Kurtiz as the head of a gtoup of day labourers looking for work in the Çukurova region. "The story," according to Rekin Teksoy, "became a legend about village farm labourers, their merciless conditions, their hopes, their pain and their goldenhearts." It won Grand Jury Prize for Best Screenplay at the Strasbourg Film Festival and two Golden Oranges at the 18th Antalya Golden Orange Film Festival.      

==Awards==
*Strasbourg Film Festival
**Grand Jury Prize for Best Screenplay: Erden Kıral (won)
*18th Antalya Golden Orange Film Festival
**Golden Orange for Best Director: Erden Kıral (won)
**Golden Orange for Best Supporting Actor: Yaman Okay (won)

==References==
 

==External links==
* 

 
 
 
 
 

 
 