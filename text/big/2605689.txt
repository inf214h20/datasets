The Waterdance
{{Infobox film
| name = The Waterdance
| image = The Waterdance (1992 Film Poster).jpg
| writer = Neal Jimenez
| producer = Marie Cantin (producer) Gale Anne Hurd (producer) Guy Riedel (executive producer) Michael Steinberg William Forsythe Elizabeth Peña Grace Zabriskie Kimberly Scott William Allen Young
| editing = Jeff Freeman
| cinematography = Mark Plummer
| music = Michael Convertino
| distributor = The Samuel Goldwyn Company
| released =  
| runtime = 106 min.
| country = United States
| language = English
| budget =
| gross = $1,723,319 (United States) 
}} William Forsythe and Helen Hunt.
 rehabilitation center. The title refers to a dream recounted by Raymond, the Snipes character, about dancing on the surface of a lake. Since, in Raymonds dream, he must continue dancing on the lake or sink and drown, the dream may be a metaphorical reference to the necessity of continually coping with the world.

== Plot ==
Joel Garcia (Eric Stoltz) is a writer who, after a hiking accident at a mountain, must struggle with paralysis.  At the same time, he carries a relationship with Anna (Helen Hunt), a married woman, with whom he was having an affair at the time of the accident.  The lovers attempt to carry on their affair during his emotional and difficult rehabilitation as a Paraplegia|paraplegic.

== Cast ==
* Eric Stoltz - Joel Garcia
* Helen Hunt - Anna
* Wesley Snipes - Raymond Hill William Forsythe - Bloss

== Reception ==
The Waterdance received mostly positive reviews from critics; it holds a 93% approval rating at Rotten Tomatoes.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 

 