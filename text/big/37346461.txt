Ranson's Folly (1926 film)
{{Infobox film
| name           = Ransons Folly
| image          =
| imagesize      =
| caption        =
| director       = Sidney Olcott
| producer       = Richard Barthelmess
| writer         =  (adaptation & scenario)
| starring       = Richard Barthelmess Dorothy Mackaill
| music          =
| cinematography = David W. Gobbett
| editing        = Helene Warne
| distributor    = First National Pictures
| released       =  
| runtime        = 78 minutes; 8 reels (7,322 feet)
| country        = United States
| language       = Silent(English intertitles)

}} silent film produced by and starring Richard Barthelmess and costarring Dorothy Mackaill. It is based on a Richard Harding Davis novel and 1904 play, Ransons Folly, and was filmed previously in 1910 and in 1915 by Edison Manufacturing Company|Edison.   

==Cast==
*Richard Barthelmess - Lt. Ransom
*Dorothy Mackaill - Mary Cahill
*Anders Randolf - The Post Trader Pat Hartigan - Sgt. Clancy William Bailey - Lt. Crosby(*William Norton Bailey)
*Brooks Benedict - Lt. Curtis
*C. C. Smith - Col. Bolland (as C.C. Smith USA)
*Pauline Neff - Mrs. Bolland
*Billie Bennett - Mrs. Truesdale
*Frank Coffyn - Post Adjutant
*Hans Joby - Judge Advocate (*as Capt. John S. Peters USN)
*Taylor N. Duncan - Capt. Carr (*as Taylor Duncan)
*J. C. Fowler - Col. Patten(as Jack Fowler)
*Edward W. Bowman - Pop Henderson (as E. W. Borman)
*Bud Pope - Abe Fisher
*Forrest Seabury - Drummer
*Chief Eagle Wing - Indian Pete
*Chief John Big Tree - Chief Standing Bear

==References==
 

==External links==
* 
* 
* 
*    at website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 


 