Rockers (1978 film)
{{Infobox Film
| name           = Rockers
| director       = Theodoros Bafaloukos
| image	         = Rockers FilmPoster.jpeg
| writer         = Theodoros Bafaloukos Leroy "Horsemouth" Wallace
| producer       = Patrick Hulsey
| distributor    = 
| released       = 1978
| runtime        = 100 min. English
| budget         = JA$500,000
}}

Rockers is a 1978  , 18 August 2013. Retrieved 18 August 2013 
 documentary but blossomed into a full-length feature showing the reggae culture at its peak.  With a budget of JA$500,000, the film was completed in two months. 

In this film, the culture, characters and mannerisms are authentic. The main rocker Leroy "Horsemouth" Wallace, for example, is shown living with his actual wife and kids and in his own home. Harry J Studios where many roots reggae artists recorded during the 1970s including Bob Marley. The film includes Kiddus Is recording of "Graduation In Zion" at the studio, which he happened to be recording when Bafaloukos visited the studio. 

Rockers premiered at the 1978 San Francisco Film Festival and had a theatrical release in the US in 1980. 

Samples of the films dialogue were used in the early 1990s jungle track, "Babylon" by Splash, "Terrorist Dub" by Californian ragga-metal band Insolence, in the track "Zion Youth" from the 1995 album Second Light by Dreadzone and in 2012 in the song "Smoke" by Inner Terrestrials. The Jamaican Patois spoken throughout the film is rendered with English language subtitles for a foreign audience.

==Plot== Kingston plans sound systems around the island.  The film starts as a loose interpretation of Vittorio de Sica| Vittorio de Sica’s The Bicycle Thief and turns into a reggae interpretation of the Robin Hood myth. 

==Cast==
* Leroy "Horsemouth" Wallace : himself Richard "Dirty Harry" Hall : himself Gregory "Jah Tooth" Isaacs : himself Jacob "Jakes" Miller : himself Robert "Robbie" Shakespeare : himself
* Frank Dowding :  Kiddus I (himself)
* Winston Rodney :  Burning Spear (himself)
* Manley Buchanan :  Big Youth (himself) Dillinger (himself) Jack Ruby : himself

== See also ==
*Rockers (soundtrack)

==References==
 

==External links==
* 
* Pop Matters review   Reggae Films website  
*  

 
 
 
 
 