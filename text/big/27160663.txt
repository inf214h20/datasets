Phantom (2002 film)
 
{{Infobox film
| name           = Phantom
| image          =
| director       = Biju Varkey
| producer       = Dr. Sudhakaran Nair
| writer         = Dennis Joseph
| narrator       = Innocent   Nishant Sagar Nedumudi Venu Lalu Alex Deva
| cinematography = Biju Viswanath
| editing        = A. Sreekar Prasad
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =2.5 crores
| gross         =10 crores
}} Malayalam action film directed by Biju Varkey. It stars Mammootty in the title role, and Manoj K. Jayan, Innocent (actor)|Innocent, Nishant Sagar, Nedumudi Venu and Lalu Alex in other pivotal roles.

==Plot==
Pailey (Mammootty) has come from Irattupetta, a small village in rural Kerala to Mumbai and made a name for him. He had come originally to Mumbai with the hope of making it big in Bollywood as an actor. But after many failed attempts, he becomes a stuntman who becomes the dupe for big stars. Due to his dare devil stunts he gets the name Phantom Pailey.

But one day while shooting a helicopter fight scene, he falls down and gets injured. Pailey understands that once a stuntman gets injured, his career ends, but he is made of different stuff. He decides to take a break and then come back to Mumbai. So he goes to a backward rural village of Kerala-Tamil Nadu border, called Pallikara where his younger brother Josekutty (Nishant Sagar) who is an officer at the local dam. But Paileys real ambition is to rejuvenate himself and get his wound healed and then go back to Mumbai.

Soon after his arrival an earthquake and landslide happens in the village and he gets involved in a fight between the locals and outsiders. The villagers are used to these natural calamities and following their complaint, to prevent such frequent earthquakes government puts a court order that quarry work should be stopped immediately. The contractor who mints money out of the quarry, Annachi (Manivannan) and his people are at loggerheads with the local people who are led by Gandhi Pillai (Nedumudi Venu) and the church priest (Innocent).

Josekutty is a budding artiste and he is in love with Pillais niece Hema (Monica). When Jose goes to Hyderabad for a competition, his elder brother Pailey (Mammootty) comes to get him married to Hema. In no time he wins over the villagers and cross swords with the baddies! Soon Annachi brings a corrupt police officer Sebastin (Manoj K Jayan), who is a sadist and a lawbreaker who is a constant nuisance for the villagers and Pailey is his main target. With the help of Annachi, Sebastin murders Josekutty and then it is pay back time for Phantom Pailey. 

==Cast==
*Mammootty as Phantom Pailey Innocent as The priest
*Nishant Sagar as Josekutty (Paileys brother)
*Manoj K. Jayan as The police officer
*Nedumudi Venu
*Lalu Alex as Dhimdhi Mathayi
*Cochin Haneefa
*Bindu Panikkar Malavika
*Monica Monica as Hema
*Aswathy as Chandrika
*Manivannan as Perumal 
*Joemon as Kennadi

==References==
 

==External links==
*  

 
 
 
 
 