Henry & June
 
 
{{Infobox film
| name           = Henry & June
| image          = Henry&June.jpeg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Philip Kaufman
| producer       = Peter Kaufman
| writer         = Philip Kaufman Rose Kaufman
| starring       = Fred Ward Uma Thurman Richard E. Grant Maria de Medeiros Kevin Spacey
| cinematography = Philippe Rousselot
| editing        = Dede Allen Vivien Hillgrove Gilliam William S. Scharf
| studio         = Walrus & Associates Universal Pictures
| released       =  
| runtime        = 136 minutes  
| country        = United States
| language       = English
| budget         =
| gross          = $23,472,449 
}} biographical drama book of the same name by the French author Anaïs Nin, and tells the story of Nins relationship with Henry Miller and his wife, June Miller|June.
 Best Cinematography Wild at Heart, also released in 1990.  

==Plot==
  Tropic of Cancer, but catalyzes the Millers separation, while she returns to Hugo.

==Cast==
* Fred Ward as Henry Miller
* Uma Thurman as June Miller Hugo
* Maria de Medeiros as Anaïs Nin
* Kevin Spacey as Richard Osborn
* Jean-Philippe Ecoffey as Eduardo Sanchez Maurice Escargot as Pop
* Liz Hasse as Jean
* Brigitte Lahaie as Henrys prostitute

==Rating==
 
Henry & June was the first film to receive the MPAAs rating of NC-17, which had been devised as a replacement for the X rating. NC-17 was intended to signify serious, non-pornographic films with more violence or (especially) sexual content than would qualify for an R rating. The inclusion of the postcard Nin views at the start of the film (which is of Hokusais The Dream of the Fishermans Wife), and some scenes of le Bal des Beaux Arts contributed to the NC-17 rating.
 banned in South Africa. The ban has since been lifted. The film was given an R18 rating in New Zealand.

==Reception==
 
Henry & June was a moderate box office success, grossing $11,567,449 in the domestic market and $11,905,000 internationally for a worldwide total of $23,472,449.   

==Soundtrack==
# Jean Lenoir, "Parlez-moi damour (song)|Parlez-moi damour" (Lucienne Boyer)
# Claude Debussy, Six épigraphes antiques: Pour légyptienne (Ensemble Musical de Paris)
# Francis Poulenc, "Les chemins de lamour" (Ransom Wilson and Christopher ORiley)
# Debussy,  )
# Harry Warren, "I Found a Million Dollar Baby" (Bing Crosby) Gnossienne No. 3" (Pascal Rogé)
# Satie, "Je te veux" (Jean-Pierre Armengaud)
# Debussy, "Sonata for Violin and Piano" (first movement) (Kyung-wha Chung and Radu Lupu)
# Frédéric Chopin, List of compositions by Frédéric Chopin| Nocturne No. 1 in C Major   (Paul Crossley)
# Georges Auric, "Sous les toits de Paris" (Rene Nazels)
# Jacques Larmanjat, lyrics by Francis Carco, "Le doux caboulot" (Annie Fratellini) Josef Suk)
# "Je mennuie" (Mark Adler)
# "Coralia" (Mark Adler)
# Irving Mills, "St. James Infirmary Blues" (Mark Adler)
# Francisco Tárrega, "Gran Vals" (Francisco Tárrega) Joaquin Nin-Culmell, Joaquin Nin-Culmell)
# Vincent Scotto, lyrics by George Koger and H. Vama, "Jai deux amours" (Josephine Baker)

==See also==
* In Search of Lost Time|The Captive (La Prisonnière)
* Nudity in film

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 