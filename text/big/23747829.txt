Detective Naani
 
{{Infobox film
| name           = Detective Naani
| image          = Detectivenaani.jpg
| caption        = Theatrical poster
| director       = Romilla Mukherjee
| producer       = Bombay Duck Films Romilla Mukherjee Jolly Mukherjee Urmilla Chatterjee
| writer         = Romilla Mukherjee Amit Verma
| music          = Jolly Mukherjee Romilla Mukherjee
| cinematography = Sanjay Kapoor
| editing        = Deepa Bhatia
| distributor    = Pinpoint Productions
| country        = India
| released       =  
| runtime       = 90 minutes
| language       = Hindi
| budget         =
| gross          =
}} Amit Verma and Shweta Gulati.

==Plot==
Its a regular day at Gulmohar Complex. Spunky, independent, 72 year old Naani is on her way back home from her daily morning walk. Suddenly her eye catches the face of a little girl peeping nervously from a 3rd floor window. The flat belongs to a newly arrived childless couple called Yadavs.

The girl hides away quickly. Naanis intrigue about the little girl leads her to a possible murder. Naani finds herself in the middle of a mystery where some people will come to her aid, some will be indifferent and some will prove to be dangerous. When the CID dismisses Naanis story due to lack of hard evidence, she transforms into a detective.

She uses her home-spun common-sense and logic and she carries out her investigation in classic "whodunit" style. Of course, Naanis rather eccentric methods of investigation lead to many quirky and humorous incidents. Her unusual team of deputies consists of her two inquisitive little grandchildren Anjali and Nakul, her divorced daughter Priya Sinha and a couple of teenagers Rohan and Neeti.

The search for one lost little girl leads Naani & Co. to a racket where the stakes are high, the criminals are ruthless, and their leader is powerful. Once Naani gets too close to their trail, she endangers herself. She finds herself sharing the same plight as the little girl she had seen in the window. Naanis team of amateur detectives get together and finally win the day with a little help from the CID.At the heart of this story is the terrified, kidnapped little girl. Her plight acts as a reminder to the audience that Naanis mission is a race against time. And yet, this adventure has a positive effect on all the characters that get involved. Bridges are built, relationships blossom, lessons are learnt.

The two squabbling teenagers fall in love, there is a whiff of a romance between Naanis divorced daughter and the dashing CID Inspector. Old Mr. Pal finds a renewed energy and interest in life. And of course, a lost 4-year-old little girl Neelima Daamle is finally re-united with her mother.

==Cast==
* Ava Mukherjee as Naani
* Simran Singh as Anjali Sinha Amit Verma as Rohan Malhotra
* Shweta Gulati as Neeti Tipnis
* Ankur Nayyar as CID Inspector Bhatia
* zain Khan as nakun somesh dutt
* Saili Shettye as Little Kidnapped Girl Neelima Daamle
* Hemu Adhikari as Mahesh Pal
* Atul Parchure as Petook
* Hemant Pandey as Tattu
* Sanjeeva Vatsa as Raj Yadav
* Shubhangi Gokhale as Madhu Pal
* Jaywant Wadkar as Goonda Pakya
* Sanjay Singh as Goonda Choti
* Amrita Raichand as Priya Sinha
* Mahru Sheikh as Tara Yadav

==External links==
*  

 
 
 