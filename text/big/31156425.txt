Raakuyil
{{Infobox film 
| name           = Raakuyil
| image          =
| caption        =
| director       = P Vijayan
| producer       = P Bhaskaran
| writer         = P. Bhaskaran
| screenplay     = P. Bhaskaran
| starring       = Adoor Bhasi Jose Prakash Sankaradi Adoor Pankajam
| music          = Pukazhenthi
| cinematography = Lekshman Gore
| editing        = K Sankunni
| studio         = Suchithramanjari
| distributor    = Suchithramanjari
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by P Vijayan and produced by P Bhaskaran. The film stars Adoor Bhasi, Jose Prakash, Sankaradi and Adoor Pankajam in lead roles. The film had musical score by Pukazhenthi.   

==Cast==
 
*Adoor Bhasi
*Jose Prakash
*Sankaradi
*Adoor Pankajam
*Bahadoor
*Chandraji
*Paravoor Bharathan
*Philomina Sudheer
*Sujatha Sujatha
*T. K. Balachandran
*Veeran
 

==Soundtrack==
The music was composed by Pukazhenthi and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innathe Mohana || S Janaki || P. Bhaskaran || 
|-
| 2 || Oro Hridayaspandana || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Shyaamasundari || S Janaki || P. Bhaskaran || 
|-
| 4 || Vaaruni Penninu Mukham Karuthu || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 