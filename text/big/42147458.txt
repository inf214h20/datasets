Dying of the Light (film)
 
{{Infobox film
| name           = Dying of the Light
| image          = Dying of the Light poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Schrader
| producer       = Scott Clayton Gary A. Hirsch Todd Williams Executive: Nicolas Winding Refn
| writer         = Paul Schrader
| starring       = Nicolas Cage Anton Yelchin Irène Jacob
| music          = Frederik Wiedmann
| cinematography = Gabriel Kosuth
| editing        = Tim Silano
| studio         = Red Granite Pictures Grindstone Entertainment Group
| distributor    = Lionsgate
| released       =  
| runtime        = 94 minutes  
| country        = United States
| language       = English Romanian Arabic
| budget         = $5 million
}} thriller film VOD formats on December 5, 2014. 

==Plot==
Veteran CIA agent Evan Lake has been ordered to retire, but when his protégé, Milton Schultz, uncovers evidence that Lakes nemesis, the terrorist Banir, has resurfaced, Lake goes rogue, embarking on a perilous, intercontinental mission to eliminate his sworn enemy. 

The film begins with Lake being tortured viciously by Banir, who desires to know who the infiltrating agent or the "double" as he calls it, in his cartel is. Lake does not relent and has his ear cut by Banir, after which he falls to the forums screaming in agony, subsequently followed by a raid carried out by a special operations team. 

22 years later, Lake is a CIA analyst, but continues to try to convince his superiors to place him back in the field. He is shown driving down the highway and appearing fazed, after which he visits a doctor and discovers he has frontotemporal dementia, which is irreversible.

==Cast==
* Nicolas Cage  as Evan Lake
* Anton Yelchin  as Milton Schultz
* Irène Jacob  as Michelle Zuberain 
* Victor Webster 
* Alexander Karim  as Muhammad Banir

==Production== Red Granite International joined as producers.    On August 19, it was announced that Lionsgate Home Entertainment had acquired the distribution rights to the film. 

On October 16, 2014, Schrader posted on his Facebook page: "We lost the battle. Dying of the Light, a film I wrote and directed, was taken away from me, reedited, scored and mixed without my input."  On December 8, 2014, his cinematographer - Gabriel Kosuth - explained, in a guest column on Variety.com, that his color-significant cinematography had been digitally altered and that he "was denied the possibility to accomplish in post-production what is any cinematographer’s duty: assuring that what audiences will see on cinema and television screens faithfully reflects the “look” intended by the director (according to the American Cinematographer Manual)". 

===Filming=== Gold Coast, Queensland had wrapped and the rest of the film would be shot in Romania. 

==Reception==
Dying of the Light was panned by critics; it currently holds a 9% rating, based on 32 reviews, on review aggregator website Rotten Tomatoes.  On Metacritic, the film has a 31/100 rating based on 12 critics, indicating "generally unfavorable reviews". 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 