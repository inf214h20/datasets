12 Rounds (film)
{{Infobox film
| name           = 12 Rounds
| image          = 12Rposter.jpg
| director       = Renny Harlin
| producer       = Becki Cross Trujillo Mark Gordon Renny Harlin Mike Lake Josh McLaughlin Vince McMahon
| writer         = Daniel Kunka Steve Harris  Ashley Scott Gonzalo Menendez Aidan Gillen Brian J. White Taylor Cole Vincent Flood
| music          = Trevor Rabin David Boyd
| editing        = Brian Berdan
| studio         = WWE Studios
| distributor    = 20th Century Fox
| released       =  
| runtime        = 108 minutes 
| country        = United States
| language       = English
| budget         = $22&nbsp;million 
| gross          = $18,184,083 
}} action film Steve Harris, Gonzalo Menendez, Aidan Gillen, Brian J. White, Ashley Scott, and Taylor Cole.  The film was released on March 27, 2009 in United States theaters.   

==Plot==
 
A sting operation to capture notorious arms dealer Miles Jackson goes awry when the FBIs inside man doublecrosses them. Miles rendezvouses with his girlfriend Erica Kessen who has a getaway car.

Officer Danny Fisher and his partner, Officer Hank Carver, are dispatched to help the FBI. On their way, they look at Miles record and see a surveillance video of him dancing with Erica. A sudden encounter with them when they spot Erica at a traffic light leads in Erica being killed and Miles being taken away. Miles swears vengeance on Danny.

One year later, Danny gets a phone call from Miles, who has escaped from prison. Danny runs outside the house looking for Miles. Dannys car and house explode, throwing Danny to the ground. After he recovers, Miles says he is launching a game of revenge called "12 Rounds". The house, the car, and Phil, the plumber who came to fix a pipe were "Round 1".

Molly, Danny’s girlfriend is kidnapped by Miles for ‘Round 2’. Miles promises that if Danny does all that he says and lives through all 12 rounds, he will let Molly go.

For "Round 3", Danny and Hank must follow a series of clues to locate the cell phone that Miles calls and for Round 4 Danny has to get to New Orleans Savings and Loan where a fire has broken out and he must extract two security deposit boxes within 20 minutes.

Hank has a lead on the man who helped kidnap Molly, and volunteers to look into that while Danny continues with the "game".

FBI Agents George Aiken and Ray Santiago approach Danny and he agrees to work with them to get Molly back.

For ‘Round 5’ one of the security boxes is a bomb and the other contains a clue to the next round. Danny is able to discover the bomb and get rid of it at the nick of time.
The other box contains a room key for a Hotel. The room is raided and found empty. The next task there for Danny is to get out of the elevator before it falls, in 60 seconds Danny manages to escape.

For round 6, Danny follows a series of clues to a bus where he finds Molly on board, wearing a bomb underneath her jacket. Danny is handcuffed to a bar and is given an envelope with a phone number as the clue to the next round. The Feds try to get Miles but he escapes with Molly.

When Danny is freed, he tells the Feds about the bomb. Hank shows up, and tells Danny he has located Miless henchman, Anthony Deluso.

Santiago approaches and offers his help. in round 7 Danny has to find the correct cell phone number that disarms bombs placed in different locations. Danny tries a number randomly and succeeds. 

Miles answers and tells him that his call disabled Streetcar 907s brakes. Danny and Santiago drive off. He and Santiago jump out as the car slams into the transformer, shutting off electricity for the whole neighborhood. Danny and Santiago then run along the streetcar, clearing people out of the way until it can slow to a halt.

In a welding factory, a mine planted by Miles explodes, killing Hank and Deluso. Danny gets word of this from Aiken. Aiken tells him that he has been obsessed with catching Miles since a Stinger missile which he stole, and Aiken failed to recover, was used to shoot down a passenger plane. Miles calls and says that Mollys bomb can now only be disarmed by Dannys fingerprint. He tells Danny to pay a visit to Erica. Danny, Santiago, and Aiken get into a car and start for the cemetery.

On the way Chuck Jansen, another detective, calls Danny to tell him that all five numbers in the envelope were rigged to the streetcar. Moreover, Miles had cameras monitoring the elevator shaft and set off the bomb five seconds early. Danny realizes that Willies death in the elevator episode was orchestrated by Miles and not just a chance casualty. Santiago does a check on Willie and finds he had a second job as a Homeland Security guard. They figure out that Miles was leading them to take out the power because in such cases Homeland Security comes in to move the unprotected cash. Miless grudge against Danny was only a cover for him to use Danny as a pawn in his scheme to steal this money.

Aiken tells Santiago to lock down the Mint, while he and Danny go after Molly. As they drive, Danny realizes that Round 12 must be a wild-goose chase, since Miles needs Molly, a nurse, to help him escape.

Meanwhile, Miles, dressed as a security guard, manages to steal the cash. He then uses Mollys ID card to get to a Medevac chopper on a hospital roof, transporting the money inside a body bag. He tells Molly to pilot the chopper.
Danny and Aiken race to the hospital roof as Molly takes off. In the fight that ensues, Aiken is wounded. Danny activates the touch phone-bomb and throws the switch away, leaving 30 seconds until the chopper blows and Molly and Danny jump out to a terrace pool. Miles is left in the exploding helicopter.

==Cast==
 
* John Cena as Detective/Officer Danny Fisher
* Ashley Scott as Molly Porter
* Aidan Gillen as Miles Jackson
* Brian J. White as Det. Hank Carver
* Taylor Cole as Erica Kessen
* Vincent Flood as Det. Chuck Jansen Steve Harris as Special Agent George Aiken
* Gonzalo Menendez as Special Agent Ray Santiago
*Travis Davis as Anthony Deluso

==Music==
The score of 12 Rounds was composed by  . He recorded his score with the Hollywood Studio Symphony at the Eastwood Scoring Stage at Warner Bros. Studios. 

==Reception==

===Box office===
The filmed opened at number seven at the box office, gaining an estimate of $1.75 million in its opening day and $5.3 million in its opening weekend. A further $2,498,325 in other countries helped take its worldwide total to $17,280,326.

===Critical response===
  normalized rating average score of 37, based on 11 reviews. 

==Home media and unrated version== UMD with an unrated "Extreme Cut" of the film on June 30, 2009.  In the first week, 12 Rounds opened at #1 at the DVD sales chart, selling 208,936 DVD units translating to revenue of $3.1m.  As of July 2011, 581,834 DVD units have been sold, bringing in $8,884,292 in revenue. This does not include Blu-ray Disc sales/DVD rentals.

The DVD is a one-disc set that includes: 
*Rated and extended "Extreme Cut" of the film
*Commentary by Director Renny Harlin, Writer Daniel Kunka and John Cena
*Two alternate endings
*“Crash Course: John Cena Stunts” featurette
*“Never-before-seen Cena gag reel” featurette

==Soundtrack==
*"Feel You" – Crumbland
*"Ready to Fall" – Rise Against
*"12 Rounds Suite" – Trevor Rabin

==Sequel==
 .  The sequel was released in 2013.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
  
 
 
 
 
 
 
 
 