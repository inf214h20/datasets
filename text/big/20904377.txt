Muqaddar
{{Infobox film
| name           = Muqaddar
| image          = Muqaddar.jpg
| caption        = Film poster
| director       = T L V Prasad
| producer       = Rajiv Babbar
| writer         = 
| narrator       =  Simran Moushumi Chatterjee Hemant Birje Puneet Issar Kiran Kumar
| music          = Anand Milind
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1996
| runtime        = 181 mins
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Indian Bollywood|Hindi film. Produced by Rajiv Babbar and directed by T L V Prasad, it stars Mithun Chakraborty, Puneet Issar and Kiran Kumar in lead roles and Moushumi Chatterjee, Simran (actress)|Simran, Ayesha Jhulka and Hemant Birje in supporting roles. 

==Plot==

Shiva (Mithun Chakraborty) always believed in making his own destiny. So when he fell in love with Meena (Ayesha Jhulka) daughter of his arch rival and enemy Parashuram (Puneet Issar), he knew he was inviting trouble. But Shiva got married to her. Parshuram could not bear with humilaition and decides to destroy Shiva. The city tremored when these two brave lions of underworld clashed. The gangwar between the rival gang of Parshuram Shiva rocked the city. Additional Commissioner of Police S.K.Khurana (Kiran Kumar) is appointed to bring the city back to normal. Shiva decides to eliminate the ACP. Shiva plants a bomb. Just when he is going to activate the bomb, he sees S.K.Khurana accompanied with his wife Bharati (Moushumi Chatterjee) and his daughter Pooja (Simran (actress)|Simran). Shiva does not activate the bomb, despite his hatred for his mother Bharati, Shiva is unable to kill her. Bharati goes to know about shiva. She goes to meet him. Shiva humilaitates Bharati. He cannot pardon Bharati for having abandoned him for his happiness. Although Shiva was alive she had accepted him for to be dead. No one knows what has destiny in store for each one of them. Pooja conceives a child. S.K.Khurana is against Pooja getting married to a criminals son. Bharati once again comes to Shiva for help. But,Will Shiva pardon his mother? Will he help her?Will Parashuram be able to settle the score? Shiva had never accepted his destinys decision, is he able to change it this time? forms the rest of the story.

==Cast==

*Mithun Chakraborty
*Ayesha Jhulka Simran
*Moushumi Chatterjee
*Hemant Birje
*Puneet Issar
*Kiran Kumar

==Reception==

Most of Mithuns 1996 release were Success as they followed the Low budget formula. Muqaddar was a satisfying success at the Box-office helping producers and distributors.

==External links==
*  
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Muqadar

 
 
 
 
 
 