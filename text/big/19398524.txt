For Pete's Sake!
 For Petes Sake}}
{{Infobox film
| name           = For Petes Sake!
| image          = For petes sake.JPEG
| image_size     =
| caption        =
| director       = Gus Meins
| producer       = Hal Roach
| writer         =
| narrator       = Matthew Beard Pete the Pup
| music          = Marvin Hatley Leroy Shield
| cinematography = Francis Corby
| editing        = Ray Snyder
| distributor    = Metro-Goldwyn-Mayer
| released       = 14 April 1934
| runtime        =17 50" 
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Gus Meins. It was the 127th (39th talking episode) Our Gang short that was released.

==Plot==
Wally had finished filling little Marianne Edwards favorite doll with sawdust. Just as he was handing it back to her, neighborhood bully, bully Leonard grabs it with a rope and throws it out on a road. A car then crushes it. The gang promise to purchase a new doll for the brokenhearted girl.

Unfortunately, the kids have no money, Leonards equally obnoxious father, who owns the toy store, agrees to give the kids a doll if they will hand over their beloved Pete the Pup in exchange. Balking at this arrangement, the kids concoct a variety of moneymaking schemes, all of them doomed to failure. Tearfully, the youngsters trade Pete for the doll. Pete does major damage to the store seconds later. The store owner then grabs the doll back. But Petes continued destruction convinces him to hand the doll and the dog back to the gang who then take it to Marianne.   

==Cast==
===The Gang===
* Wally Albright as Wally Matthew Beard as Stymie
* Scotty Beckett as Scotty
* Tommy Bond as Tommy
* George McFarland as Spanky
* Carlena Beard as Stymies sister
* Marianne Edwards as Marianne Jacqueline Taylor as Jane
* Edmund Corthell as Our Gang member
* Barbara Goodrich as Our Gang member
* Philbrook Lyons as Our Gang member
* Billie Thomas as Our Gang member
* Pete the Pup as Himself

===Additional cast===
* Leonard Kibrick as Leonard
* Fred Holmes as Fred
* Lyle Tayo as Freds Wife William Wagner as Storekeeper

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 