Purgatory (2007 film)
{{Infobox film
| name = Purgatory
| director = Isma Rubio 
| writer = Isma Rubio Carlos Calavia
| starring = Raul Ferrer Carlos Calavia Diana Isis Sergio Tafalla 
| producer = Yolanda Garcia
| music = Nacho Vera
| cinematography = Sergio Moliner
| editing = Juan Antonio Rubio
| released =  
| runtime = 10 minutes
| country = Spain Spanish
}}
Purgatory is a short film produced by Goma Films and directed by Spanish filmmaker Isma Rubio. It was released in 2007. The film is a ten minutes long short that depicts a pub that is not what it looks like, half way between heaven and hell.

==Plot==
"Three people, very different to each other, live their last experience."

==Awards==
* Selected Film in 2007 European Short Film Festival FEC Cambrils-Reus (ESP)
* Selected in 2008 Festival Cinema Villa de la Almunia (USA)

==External links==
* http://www.gomafilms.com
*  

 
 


 