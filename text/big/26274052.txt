Jack of All Trades (1936 film)
{{Infobox film
| name = Jack of All Trades
| image_size =
| image = Jack of All Trades FilmPoster.jpeg
| caption = Robert Stevenson Jack Hulbert
| writer =
| narrator =
| starring = Jack Hulbert
| music =
| cinematography = Charles Van Enger
| editing = Terence Fisher
| distributor =
| released = 1936
| runtime = 76 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
| amg_id =
}} Robert Stevenson and Jack Hulbert. It stars Jack Hulbert and Gina Malo. 

==Plot==
Jack, out of work and responsible for an aged mother, takes a succession of jobs, bluffing his way through them all. 

==Cast==
*Jack Hulbert as Jack Warrender
*Gina Malo as Frances Wilson
*Robertson Hare as Lionel Fitch
*Mary Jerrold as Mrs. Warrender
*Cecil Parker as Sir. Chas Darrington
*Athole Stewart as Bank Chairman
*Felix Aylmer as Managing Director
*Ian McLean as The Fire Raiser
*H.F. Maltby as Bank Director
*Fewlass Llewellyn as Bank Director

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 