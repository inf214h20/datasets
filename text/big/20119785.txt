The Executioner (1975 film)
{{Infobox film
| name           = The Executioner 
| image          = 
| caption        = 
| director       = Byun Jang-ho
| producer       = Kang Dae-jin
| writer         = Yoon Sam-yook 
| starring       = Il-seob Baek Ji-yeong Park
| music          = Jeong Yoon-joo
| cinematography = Lee Seong-chun
| editing        = Kim Yeong-hui
| distributor    = Sam Young Films Co., Ltd
| released       =  
| runtime        = 86 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name      = {{Film name
 | hangul         = 망나니
 | rr             = Mangnani
 | mr             = Mangnani}}
}} I Will Survive.

== Plot ==
An executioner and his family try to survive from the dead.

== Remake ==
The newer film I Will Survive is based on the film, although there is a difference when a main character rapes the daughter of noble man whom he had executed and saves her from jail.

== Cast ==
* Il-seob Baek
* Ji-yeong Park
* Jang-kang Heo
* Mu-yeong Kim
* Nan-yeong Kim

== External links ==
*  
*  

 
 
 
 
 
 


 