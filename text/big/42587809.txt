Pro Wrestlers vs Zombies
{{Infobox film
| name           = Pro Wrestlers vs Zombies
| image          = Pro Wrestlers vs Zombies film poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Cody Knotts
| producer       = 
| writer         = Cody Knotts
| starring       = Roddy Piper Kurt Angle
| music          = Marco Werba
| cinematography = Joseph Russio
| editing        = 
| studio         = Principalities of Darkness
| distributor    = Troma Entertainment 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} TNA wrestlers that portray themselves fighting against a horde of zombies.   

==Synopsis== Shane "The Franchise" Douglas accidentally kills a fellow wrestler in the ring, the deceased wrestlers brother Angus (Ashton Amherst) decides to take matters into his own hands by summoning an ancient demon to seek revenge. Hes granted the power of necromancy and Angus wastes no time in tricking Shane and Rowdy Roddy Piper into traveling to a remote location by promising them money in return for a personal appearance. Once there, Shane and Roddy must fight their way through hordes of flesh-eating zombies and stop Angus before he takes his revenge any further.

==Cast==
*Roddy Piper as Himself
*Kurt Angle as Himself
*Matt Hardy as Himself
*Jim Duggan as Himself
*Taya Parker as Herself
*Reby Sky as Herself
*Shane Douglas as Himself
*Richard John Walters as Tezcatlipoca
*Shannon M. Hart as Serena
*Ashton Amherst as Angus
*Jeremy Ambler as Ringside Zombie
*Camera Chatham Bartolotta as Amber Mike Walker as Kurt Angles Bodyguard (Michael) Matthew Rush as Greg Grove
*Cody Knotts as Himself James Quinn as Businessman / Zombie

==Production==
Filming for Pro Wrestlers vs Zombies took place in Parkersburg, West Virginia during 2013,  and Knotts raised part of the funding for the film via a successful Kickstarter campaign.  Kurt Angle was brought in to perform in the movie after he was approached by one of the films producers.    Angles role required a scene where he appears as a zombie, and he stated that the makeup process took about five hours and that he greatly enjoyed the experience.  He further commented that as most of the zombies were also professional wrestlers, he felt more comfortable performing various wrestling moves as they would be experienced with the various techniques employed in the ring and in practice.  The movie experienced some difficulties with filming when one of the performers was accidentally hit with a cooking pot by Roddy Piper, but did not otherwise have any major setbacks. 

==Reception==
Bleacher Report gave the film a positive review stating "It isnt trying to be smart or groundbreaking, just a lot of big, silly, violent fun." but noted that the movie would appeal most to wrestling fans, as  some of the plots elements and humor confusing, as they require that the viewer has prior knowledge of the wrestlers history.  Though Fangoria panned the film overall, as they felt that "Instead of capitalizing on the concept and going full-force, PRO WRESTLERS VS. ZOMBIES stretches its novelty to the breaking point by sitting on a repetitive story structure." 

== References==
 

==External links==
*  
*  
*  

 
 
 
 