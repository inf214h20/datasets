Early Spring Story
 
{{Infobox film
| name           = Early Spring Story
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 早春物語
| kana           = Sōshun Monogatari}}
| director       = Shinichirō Sawai
| producer       = 
| writer         = 
| starring       = Tomoyo Harada
| music          = Joe Hisaishi
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film directed by Shinichirō Sawai.

==Plot Summary==

17-year-old Hitomi Okinos mother died four years ago, and her father has remarried, causing Hitomi to worry that her father is throwing away all memory of her mother. During spring vacation, Hitomi is taking pictures for her schools photography club when she encounters a middle-aged man named Kajikawa, whose car is blocking the shot she was hoping to take. She later meets him again during a traffic jam while on her bicycle, and as they have dinner introduces herself as a 20-year-old college student. A few days later, she makes her way all the way to Tokyo and his workplace, and tags along with him to an evening party.
 arranged marriage.

During a meeting at work, Kajikawa receives a call from Hitomi, asking if hes seen the photo, which he hasnt. He pulls it out, realizes what it is, and meets her later in a cafe where she finally tells him that shes the daughter of that woman, and that her mother has died. Kajikawa talks of their past relationship and refuses to apologize for either his actions or his moving forward with his life.

Later, Hitomi sees Kajikawa heading into a hotel with another woman and follows them into the elevator. The other woman realizes immediately that Hitomi and Kajikawa have some kind of relationship, and forces the two together. In the hotel, Hitomi attempts to seduce Kajikawa again, saying shell be just like her mother was, but Kajikawa refuses her and tells her to leave, locking himself in the bathroom. When Hitomi doesnt, and lays down in the hotel bed, Kajikawa begins pulling the covers off, which frightens her. Hitomi runs into the bathroom, Kajikawa throws her clothes at her, and the two leave the hotel in his car. Hitomi, calmer, keeps insisting that they stop at various love hotels, and when Kajikawa doesnt, grabs the steering wheel, causing an accident.

In the hospital, her minor injuries patched up, Hitomi asks Kajikawa to kiss her, not in place of her mother but as herself. He does, and the kiss turns passionate. Kajikawa meets with Hitomis father to both tell him about their relationship and to return the photograph. Hitomis father tells Kajikawa that he was the man Hitomis father could never forget.

A subplot involving the relationship between one of Hitomis classmates and a teacher at their school ends when that student commits suicide. At the funeral, Hitomi and Kajikawa talk again. Kajikawa has quit his job and will be returning to America, where hes been invited to join a friends company.

Hitomi turns up at the last minute to see Kajikawa off at the airport, something her mother did not do. Kajikawa confesses that he has fallen in love with her. Hitomi says thank you, and goodbye, then walks away through the airport.

Over a still shot of the photo Hitomi took during her first encounter with Kajikawa, Hitomi can be heard talking to the same friend she was speaking to when the film opened. The friend says that shes a woman now she has experience, to which Hitomi counters that experience doesnt make a woman, that it is the pain of love, and that she herself is now a woman who has made a past for herself.

==Awards and nominations== Japan Academy Prize.   Best Director - Shinichirō Sawai
7th Yokohama Film Festival   
*Won: Best Actress - Tomoyo Harada
*5th Best Film

==References==
 

 

 
 
 
 