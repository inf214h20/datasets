Nanny McPhee
 
 
 
{{Infobox film
| name           = Nanny McPhee
| image          = Nanny mcphee.jpg
| alt            = silhoutte of Nanny McPhee against brightly coloured background
| caption        = Theatrical release poster Kirk Jones
| producer       = {{Plain list | 
* Lindsay Doran
* Tim Bevan
* Eric Fellner
}}
| screenplay     = Emma Thompson
| starring       = {{Plain list | 
* Emma Thompson 
* Colin Firth 
* Kelly Macdonald 
* Derek Jacobi 
* Patrick Barlow 
* Celia Imrie 
* Imelda Staunton 
* Thomas Sangster 
* Angela Lansbury
}}
| music          = Patrick Doyle
| cinematography = Henry Braham Nick Moore
| studio         = {{Plain list | 
* StudioCanal
* Metro-Goldwyn-Mayer
* Working Title Films
* Three Strange Angels
* Nanny McPhee Productions
}}
| distributor    = Universal Pictures
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom United States  France 
| language       = English
| budget         = $25 million 
| gross          = $122,489,822 
}} Kirk Jones. The film stars Emma Thompson and Colin Firth.  Thompson also scripted the film, which is adapted from Christianna Brands Nurse Matilda books. It had a sequel released in 2010, titled Nanny McPhee and the Big Bang. The filming location is  .

==Plot==
Widowed undertaker Cedric Brown has seven children. He is clumsy, loves his children but spends little time with them and cannot handle them. The children have had a series of nannies, which they systematically drive out by their bad behaviour. They also terrorise the cook, Mrs Blatherwick.

One day, Cedric discovers throughout the home references for a "Nanny McPhee". That stormy night, the children cause havoc in the kitchen. Cedric sees a shadow behind the door and opens it to reveal a frighteningly hideous woman, who states that she is Nanny McPhee. With discipline and a little Magic (paranormal)|magic, she transforms the familys lives. In the process, she changes from ugly to beautiful, her warts and unibrow disappearing. The children, led by the eldest son Simon, first try to play their tricks on her, but gradually start to respect her and ask her for advice. They change to responsible people helping their clumsy father in solving the family problems, making McPhee less and less needed.

The family is financially supported by Cedrics domineering and nearsighted Great Aunt Adelaide. She demands custody over one of the children. She first wants Christiana (Chrissie), one of the daughters, but Evangeline, Cedrics scullery maid, volunteers and Adelaide agrees, assuming she is one of the daughters. She threatens to reduce the family to poverty unless Cedric remarries within the month. The family would lose the house, and they would not be able to stay together. Desperate, Cedric turns to a vile and frequent widow, Mrs. Selma Quickly. The children assume from books that stepmothers are terrible; therefore they sabotage a visit of Mrs. Quickly, who leaves, angry at Cedric. After the children are explained the financial aspect they agree to the marriage, and appease Mrs. Quickly by confessing they were to blame for the disturbance of her visit, and lure her back to their father with tales of their Great Aunt Adelaides wealth.

The children discover that Mrs. Quickly is just as cruel as they suspected when she breaks their real mothers rattle (the only thing they had left of her). When everybody is gathered for the marriage ceremony, they disturb the ceremony by pretending there are bees, chasing the guests, and throwing the pastries intended for the banquet at everyone present. Cedric understands they do not like the bride, and does not like her himself, and therefore starts disturbing the ceremony himself. Mrs. Quickly cancels the marriage and storms off in anger. This seems to mean that Adelaides marriage deadline is missed, but Simon asks Evangeline whether she loves Cedric. She first denies, explaining that it would be inappropriate because of her station as maidservant, but then confirms she does. Cedric marries Evangeline the same day, Nanny McPhee (who is now beautiful) magically makes it snow which changes wedding decorations, satisfying Aunt Adelaides demand, and restores the childrens real mothers rattle for them.

Nanny McPhee leaves surreptitiously, in accordance with what she told the children before on her first night: "When you need me, but do not want me, then I must stay. When you want me, but no longer need me, then I have to go."

==Cast==
* Emma Thompson as Nanny McPhee
* Colin Firth as Cedric Brown
* Kelly Macdonald as Evangeline
* Angela Lansbury as Great Aunt Adelaide Stitch
* Celia Imrie as Mrs. Selma Quickly
* Imelda Staunton as Mrs. Blatherwick
* Derek Jacobi as Mr. Wheen
* Patrick Barlow as Mr. Jowls
* Thomas Sangster as Simon Brown
* Eliza Bennett as Tora Brown
* Jennifer Rae Daykin as Lily Brown
* Raphaël Coleman as Eric Brown
* Samuel Honywood as Sebastian Brown
* Holly Gibbs as Christianna "Chrissy" Brown
* Hebe and Zinnia Barnes as Aggie Brown
* Adam Godley as the Vicar

==Production==
Filming began in April 2004. The film reunites Emma Thompson, Colin Firth, Thomas Sangster and Adam Godley who all previously starred in Love Actually.

==Reception==

===Critical response===
Review aggregation website Rotten Tomatoes gives Nanny McPhee a score of 73% based on 130 reviews, a rating deemed Certified "Fresh". 

===Box office===
The film did well at the box office, earning $122,489,822 - $47,144,110 in the United States and $75,345,712 elsewhere. It premiered in the United States on 27 January 2006 with an opening weekend total of $14,503,650 in 1,995 theaters (an average of $7,270 per theatre) ranking at No. 2 (behind the Martin Lawrence film Big Mommas House 2). http://www.boxofficemojo.com/movies/?id=nannymcphee.htm 

==Sequels==
Emma Thompson revealed on Friday Night with Jonathan Ross that two more films were planned. The second film, Nanny McPhee and the Big Bang (also called Nanny McPhee Returns), was released in March 2010. It co-stars Rhys Ifans, Maggie Smith, Ralph Fiennes and Maggie Gyllenhaal. The character of Aggie Brown returns as the now elderly Mrs. Docherty. In it, Nanny McPhee takes charge of the children of a woman whose husband has gone to war. 

A third film was planned in modern-day England but with the second film only taking $93 million at the box-office, Thompson scrapped plans for future films. 

==See also==
* Nanny McPhee and the Big Bang

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 