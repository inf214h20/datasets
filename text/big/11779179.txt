Movin' In
{{Infobox film
| name = Movin In
| caption =
| image = Movin In FilmPoster.jpeg
| border = yes
| director = Griff Furst
| producer = Alex Yves
| writer = Alex Yves
| starring = Christy Carlson Romano Estelle Harris Yangzom Brauen Marco Schwab Michael Lawson Walter Andreas Mueller Birgit Steinegger Sven Epiney Dean Kreyling Samantha Cope Dan Ponsky Griff Furst Alex Yves
| distributor = YvesCreations
| released =  
| runtime = 95 minutes
| country = United States, Switzerland
| language = English
| budget =
}}
Movin In is a comedy film.

==Plot==
Movin In is a comedy about a young man from Switzerland who travels to Los Angeles to escape his dead-end life and to reconnect with a female pen pal with whom he has lost touch.

Everything falls apart when the pen pal turns out to be a complete fraud and he is reunited with an old friend (a real trouble magnet) who keeps pulling him into the most awkward situations.

The story takes unexpected turns when the two - somewhere between moving into an old folks home, going to auditions, and dealing with a phony agent and a group of gangsters - fall for the same girl. 

==Release==
A pre-final cut was featured in the 2009 Denver Festivus film festival. After an extensive post-production period the film then was completed just in time for the 2010 New York City International Film Festival (August 18, 2010). 

Movin In is currently distributed by Amazon (available for download or purchase of the DVD).

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 