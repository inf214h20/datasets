Naradhan Keralathil
{{Infobox film 
| name           = Naradhan Keralathil
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       = Jayasree Mani
| writer         = Cheri Viswanath
| screenplay     = Cheri Viswanath Mukesh Nedumudi Hari
| music          = M. K. Arjunan
| cinematography = Cross Belt Mani
| editing        = C Mani
| studio         = Vidya Movie Tone
| distributor    = Vidya Movie Tone
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Jagathy Sreekumar Mukesh
*Nedumudi Venu Hari
*Ratheesh
*Babitha
*Bahadoor
*Balan K Nair
*Bobby Kottarakkara
*C. I. Paul
*Kaduvakulam Antony
*Lalithasree
*Ravi Menon
*Shailaja Shari
*Thodupuzha Vasanthi
*Vettoor Purushan Vijayaraghavan
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dhoomam vallaatha dhoomam || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Hare raama || Sreekanth || P. Bhaskaran || 
|-
| 3 || Nandavanathile Sougandhikangale || Vani Jairam, Lathika || P. Bhaskaran || 
|-
| 4 || Vidyaavinodini || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 