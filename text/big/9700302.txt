Intellectual Property (film)
{{Infobox film|
  name     =Intellectual Property|
  image          =IP poster.jpg|
  director       =Nicholas Peterson |
  producer       =Nicholas Peterson   Martin Berneman |
  writer         =Nicholas Peterson Hansen Smith|
  starring       =Christopher Masterson   Lyndsy Fonseca|
  cinematography =Nic Sadler | 
  design =Dustin J. Cardwell | 
  editing        =Nicholas Peterson   Peter Devaney Flanagan |  
  runtime        =81 minutes |
  country        =United States |
  language       =English |
  gross          = |
  budget         =$500,000 |
  music          =Jasper Randall |
  awards         =Best Cinematography at the Australian International Film Festival|
|}}
 directed by Nicholas Peterson.

== Plot ==
  independent drama.

==Production== Santa Clarita Studios in California. Unusually for a low budget independent film, all scenes were shot on specially constructed sets. This was made possible by recycling sets left over from previous productions at the studio.
 Fuji Eterna Apple Final Dark Mind.

==Awards==
*Best Cinematography, Australian International Film Festival
*Best Film, Oxford International Film Festival   from OxfordFilms.com 
*Best Director, Oxford International Film Festival 
*Best Actor, Christopher Masterson, Oxford International Film Festival 
*Grand Jury Prize, Best Film, DC Independent Film Festival

==References==
 

== External links ==
 

* 
*  
* 
*http://www.iampeople.com
 

 
 
 
 
 