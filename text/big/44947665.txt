Goecha La In Search of Kangchenjunga
{{Infobox film name        = Goecha La in Search of Kanchenjunga image       = caption     = Promotional poster director    = George Thengummoottil editing     = Rohit A Chelat narration   = Jelu Jayaraj released    = 2012 runtime     = 27 minutes country     = India language    = India budget      =  gross       = 
}}

Goecha La in Search of Kanchenjunga is a film on trekking in the Himalayas made by the Indian writer George Thengummoottil.  The film portraits the high altitude Goecha La trek in Sikkim from Yuksam to Goecha La pass at an altitude of 5200 meters above sea level via Tshoka and Lamuney.  The film was selected as the best documentary film made by Department of Communication, PSG College of Arts and Science in 2012 and was selected by Tourism Department of Sikkim as the best film on Sikkim in 2012.

The film was edited by Rohit A Chelat grandson of V.V. Raghavan and nephew of C. Achutha Menon

==Technique==

Goecha La in search of Kangchenjunga was the first attempt to document this high altitude pass and solar powered cameras were used in the making of the film. 

==References==
 
 
*
*
*
*

 
 
 
 
 