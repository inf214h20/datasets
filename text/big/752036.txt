Man in the Wilderness
 

{{Infobox film
| name           = Man in the Wilderness
| image          = Man in the wilderness.jpg
| image_size     =
| caption        = Promotional poster for the film
| director       = Richard C. Sarafian
| producer       = Sandy Howard
| writer         = Jack DeWitt
| narrator       = Percy Herbert Henry Wilcoxon Norman Rossington Dennis Waterman Johnny Harris
| cinematography = Gerry Fisher
| editing        = Geoffrey Foot
| distributor    =
| released       =  
| runtime        = 104 minutes
| country        = United States English
| budget         =
}} Richard Harris as Zachary Bass and John Huston as Captain Henry.

The expedition is notable in the movie for bringing a large boat with them, borne on wheels.  Captain Henrys aim of using the boat to traverse the rivers (possibly the Missouri or the Platte) comes to naught in the final scene, when the expedition comes across the drained riverbed.

==Plot==
A classic survival story, told partly through flashbacks to Zachary Basss past. After being abandoned for dead by his fellow trappers, he undergoes a series of trials and adventures as he slowly heals and equips himself while he tracks the expedition, apparently intent on retribution for his abandonment, while earning the respect of the Indians he encounters.  However, when he finally confronts his fellow trappers and Captain Henry, he chooses not to seek revenge, but instead to focus on returning to his infant son.

==Production background== Andrew Henry Adirondack wilderness and less like the Absaroka country of the Yellowstone River. Not technically a "Spaghetti Western", Man was filmed in the rugged highlands near the desert-like Spanish terrain seen in many of Sergio Leones Italo westerns.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 