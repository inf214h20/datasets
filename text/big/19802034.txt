Rikki og mændene
{{Infobox film
| name           = Rikki og mændene
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr. Lisbeth Movin
| producer       = Lau Lauritzen, Jr. Jon Iversen Sara Kordon
| starring       = Ghita Nørby
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = ASA Film
| released       =  
| runtime        = 101 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Rikki og mændene is a 1962 Danish drama film directed by Lau Lauritzen, Jr. and Lisbeth Movin and starring Ghita Nørby.

==Cast==
* Ghita Nørby - Rikki
* Poul Reichhardt - Ole
* Holger Juul Hansen - Peter
* Preben Mahrt - Chris
* Bodil Steen - Dorte
* Bendt Rothe - Knud
* Palle Huld - Pedersen
* Kaspar Rostrup - Den unge mand
* Knud Schrøder - Ubehagelig kunde
* Poul Müller - Rikkis chef
* Ebba Høeg - Prostitueret
* Bjarne Forchhammer - Mand på huslejekontor
* Hugo Herrestrup - Købmand
* Bente Wienberg Hansen - 
* Ole Wisborg - Inspektør i bar
* Bodil Miller - Prostitueret
* Gitte Lee - Prostitueret (as Gitte Kröncke)
* Lisbeth Movin - Peters kone Karin - Rikkis datter
* Thorkil Lauritzen - George, bartender
* Gerda Elisabeth Borchgrevink - Barnepige (as Gerda Borchgrevink)
* Helle Hertz - Prostitueret

==External links==
* 

 

 
 
 
 
 
 
 
 


 