New Moon (1930 film)
{{Infobox film
| name           = New Moon
| image          =
| producer       = Jack Conway	 	 Frank Butler
| starring       = Lawrence Tibbett Grace Moore Adolphe Menjou Roland Young Gus Shy Emily Fitzroy
| music          = William Axt
| cinematography = Oliver T. Marsh
| editing        = Margaret Booth
| distributor    = Metro-Goldwyn-Mayer
| released       =United States December 1930 (premiere) 17 January 1931 (wide) Finland 16 November 1931 Denmark 14 July 1932
| runtime        = 78 minutes United States English
}}
 Broadway in 1928. The 1930 version of New Moon is a 1930 black and white American musical film, and is also known as Komissa Strogoff in Greece, Nymånen in Denmark and Passione cosacca in Italy.
 Jack Conway, of the same name, also based on the operetta directed by Robert Z. Leonard and W. S. Van Dyke (uncredited), starred Jeanette MacDonald and Nelson Eddy and also reworked the plot, though it was slightly more faithful than the 1930 version. However, the music was not always presented faithfully. The 1930 version added new songs not by Romberg, and the 1940 version turns the melancholy tango number "Softly, As in a Morning Sunrise", originally sung by the heros best friend, into a cheerful little ditty sung by Eddy while he shines his shoes.

==Synopsis==
New Moon is the name of the ship crossing the Caspian Sea. A young man named Lt. Petroff meets Princess Tanya and they have a ship-board romance. Upon arriving at the port of Krasnov, Petroff learns that Tanya is engaged to Governor Brusiloff. Petroff, disillusioned, crashes the ball to talk with Tanya. Found by Brusiloff, they invent a story about her lost bracelet. To reward him, and remove him, Brusiloff sends Petroff to the remote, and deadly, Fort Darvaz. Soon, the big battle against overwhelming odds will begin.

==Cast==
*Lawrence Tibbett – Lieutenant Michael Petroff
*Grace Moore – Princess Tanya Strogoff
*Adolphe Menjou – Governor Boris Brusiloff
*Roland Young – Count Igor Strogoff
*Gus Shy – Potkin
*Emily Fitzroy – Countess Anastasia Strogoff

==Soundtrack==
* "Lover, Come Back to Me"
:(1928)
:Music by Sigmund Romberg
:Lyrics by Oscar Hammerstein II
:Played during the opening credits
:Sung by Lawrence Tibbett at the tavern
:Reprised by him and Grace Moore at the fort
* "Farmers Daughter"
:(1930)
:Music by Herbert Stothart
:Lyrics by Clifford Grey
:Played by the band on the ship and sung in a gypsy language by Lawrence Tibbett
:Reprised by him with an English translation
:Played on piano and sung in the gypsy language by Grace Moore
* "Wanting You"
:(1928)
:Music by Sigmund Romberg
:Lyrics by Oscar Hammerstein II
:Sung a cappella by Lawrence Tibbett on the ship
:Reprised by him and Grace Moore on the ship
* "One Kiss"
:(1928)
:Music by Sigmund Romberg
:Lyrics by Oscar Hammerstein II
:Played on piano (and studio orchestra) and sung by Grace Moore
* "What Is Your Price Madam?"
:(1930)
:Music by Herbert Stothart
:Lyrics by Clifford Grey
:Played by the orchestra at the ball and sung by Lawrence Tibbett
* "Stout Hearted Men"
:(1928)
:Music by Sigmund Romberg
:Lyrics by Oscar Hammerstein II
:Sung by Lawrence Tibbett and soldiers at the fort
:Reprised by the men returning from the battle

==Trivia==
* The operetta The New Moon opened on Broadway in New York City on 19 September 1928 and closed on 14 December 1929 after 519 performances. The leads were played by Robert Halliday and Evelyn Herbert, and the supporting cast included Gus Shy, who is also in this movie.
* Some production charts included Hale Hamilton and Marie Mosquini in the cast, but they were not seen in the movie.
* Modern sources include in this film the songs "Marianne", "Funny Little Sailor Man" and "Softly, As in a Morning Sunrise" (all from the original stage production), but they were not heard.
* The credits list New Moon as the title of the original operetta, but its title was The New Moon.
* The production dates were from 22 July 1930 until 3 October 1930.

===Film Connections===
* New Moon is a version of  . Deep in My Heart – the Romberg written production number.

==External links==
*  

 

 
 
 
 
 