The Monster Club
 
 
 
{{Infobox film
| name           = The Monster Club
| image          = The Monster Club.jpg
| image_size     =
| caption        = Spanish language poster for the film
| director       = Roy Ward Baker
| producer       = Milton Subotsky Jack Gill (executive)
| writer         = Edward Abraham Valerie Abraham
| narrator       =
| starring       = Vincent Price Donald Pleasence John Carradine Stuart Whitman
| music          = Douglas Gamley
| cinematography = Peter Jessop
| editing        = Peter Tanner studio = Chips Productions Sword and Sorcery Productions ITC
| released       = 1980
| runtime        = 97 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 British horror horror film directed by Roy Ward Baker and starring Vincent Price and John Carradine. An anthology film, it is based on the works of the British horror author Ronald Chetwynd-Hayes|R. Chetwynd-Hayes. It was the final film from Milton Subotsky who was best known for his work with Amicus Productions; Amicus were well known for their anthologies but this was not an Amicus film. It was also the final feature film directed by Baker.

== Plot ==
A fictionalised version of author R. Chetwynd-Hayes (Carradine) is approached on a city street by a strange man (Price) who turns out to be a starving vampire named Eramus. Eramus bites the writer, and in gratitude for the small "donation", takes his (basically unharmed but bewildered) victim to the titular club, which is a covert gathering place for a multitude of supernatural creatures. In between the clubs unique music and dance performances, Eramus introduces three stories about his fellow creatures of the night.

===The Shadmock===
A young, financially struggling woman (Kellerman) takes a job at a secluded manor house owned by Raven (Laurenson), a hybrid creature called a Shadmock, who leads a troubled and tragic existence and is notorious for its demonic whistle. As time goes by, the girl, Angela, develops a friendship with the mysterious Shadmock, named Raven, and he eventually proposes to her. Alarmed, Angela refuses but her controlling boyfriend (Ward) forces her to go through with it to gain the Shadmocks vast wealth. At the night of the engagement party, Angela is caught robbing the Shadmocks safe, and screams that she could never love him. Heartbroken, the Shadmock whistles and destroys Angelas face. Upon seeing her, her boyfriend is driven insane and locked away in an asylum.

===The Vampires===
The timid son of a peaceable family of vampires lives a miserable, lonely life where he is bullied at school and his father (Johnson) spends little time with him. The son discovers his father is a vampire, being relentlessly if ineptly hunted by a team of bureaucratic undead-killers led by Pickering (Pleasence). The hunters break into the house and stake the vampire father, but the tables are turned when the father bites Pickering, meaning he will have to be staked by his own assistants.

===The Ghouls===
A movie director (Whitman) scouting locations for his next film pays a horrifying visit to an isolated, decrepit village, Loughville near Hillington, Norfolk, where the sinister residents refuse to let him leave. He discovers to his horror that the village is inhabited by species of corpse-eating demons called ghouls who unearth graves for food and clothes. And now there are no more graves to plunder and the ghouls are hungry for flesh. While imprisoned by the ghouls, he meets Luna (Dunlop), the daughter of a ghoul father and a deceased human mother. Luna advises him to hide in the church, as ghouls cannot cross holy ground. Whilst in the church, the director discovers the terrifying truth of Loughville; centuries before, a swarm of ghouls invaded the village, mated with the humans and made their nest there. The director with the aid of Luna attempts to escape, only for Luna to be killed by the ghouls and the director captured again and returned to the village by ghoul policemen.

At the end of the film, Eramus cheerfully lists to the other club-members all the imaginative ways that humans have of being horrible to each other, and declares that humans are the most despicable monsters of all. Thus Chetwynd-Hayes is made an honorary monster and member of the club.

== Cast ==
*Vincent Price as Eramus
*John Carradine as R. Chetwynd-Hayes
*Donald Pleasence as Pickering
*Stuart Whitman as Sam Richard Johnson as Lintoms father
*Barbara Kellerman as Angela
*Britt Ekland as Lintoms mother
*Simon Ward as George Patrick Magee as Innkeeper
*Anthony Valentine as Mooney Anthony Steel as Lintom Busotsky
*James Laurenson as Raven (The Shadmock)
*Geoffrey Bayldon as Psychiatrist
*Lesley Dunlop as Luna Neil McCarthy as Watson
*Warren Saire as Young Lintom

==Behind the scenes==
Despite Vincent Prices decades-long career as a horror actor, The Monster Club features what may be his only film performance as a vampire;  although he appeared as Dracula in the educational film "Once Upon a Midnight Scary". Christopher Lee was originally sought for the role of Chetwynd-Hayes, but flatly turned the offer down simply upon hearing the films title from his agent. Peter Cushing also turned down a role.
===In-jokes===
There are a number of in-jokes in the movie relating to Amicus Films:
*The character of Lintom Busotsky is a film producer, and his name is an anagram of the real films producer, Milton Subotsky.
*Busotsky introduces a film called From Beyond the Tombstone, an allusion to From Beyond the Grave Patrick Magee, Britt Ekland and Geoffrey Bayldon, all of whom appeared in the 1972 Amicus anthology film Asylum (1972 horror film)|Asylum.
*There is a reference to a producer "Dark John" - many Amicus films were made by John Dark. 

==Reception==
The film was released to cinemas in the UK on 24 May 1981.  

Chetwynd Hayes was disappointed with the film, finding the humour silly, disliking the script and how his original stories were changed (he said only Ghoulsville was faithful), and hating the pop music. He also thought John Carradine was too old to play him. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 160-161 

The movie was a commercial failure.  During the 1980s, it was released to video and TV and became a small cult film. It was liked mainly for the pairing of Carradine and Price, who were preparing to leave horror for other ventures.

== Music == Night perform the track "Stripper", which did not appear on either of their albums.

The films soundtrack album including both songs and instrumental tracks is included as a bonus feature on the US release of the DVD and Blu-ray.

==See also==
*Vampire film

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 