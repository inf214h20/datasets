Ben Hall and his Gang
 
 
{{Infobox film
| name           = Ben Hall and his Gang
| image          = 
| image_size     = 
| caption        =  John Gavin
| producer       = Herbert Finlay Stanley Crick
| writer         = Agnes Gavin 
| narrator       =  John Gavin Agnes Gavin
| music          = 
| cinematography = Herbert Finlay
| editing        = 
| studio = Crick and Finlay
| distributor    = 
| released       = 30 January 1911 
| runtime        = 3,600 feet 
| country        = Australia
| language       = Silent film
| budget         = 
}}
 Ben Hall, John Gavin, who also directed. It is considered a lost film.

==Plot== Ben Hall, including:
*Ben Halls home.
*My Child! My Child! You Have No Mother
*Ben Gambling to Forget his Sorrows.
*Ben Hall arrested.
*His First Crime.
*Ben Halls sensational escape from Bathurst Gaol.
*Sticking up three police disguised as shearers.
*Ben Halls first robbery under arms.
*Sticking up the Eugowra Mail.
*Black Bob shot.
*The Troopers leap for life.
*Hall meeting his false friend.
*The Wages of Sin.  

==Production==
This was the first film Gavin made for the producing team of Stanley Crick and Herbert Finlay after ending his association with H. A. Forsyth and Southern Cross Film Enterprises.Gavin publically announced he had left Southern Cross in advertising for the film.   

Gavin went on to make three more films for Crick and Finlay (Frank Gardiner, the King of the Road (1911), Keane of Kalgoorlie (1911), The Assigned Servant (1911)), and one for their new company, the Australian Photo-Play Company (The Mark of the Lash (1911)). 

==References==
 

==External links==
* 
*  at Ausstage
 

 
 
 
 
 
 


 