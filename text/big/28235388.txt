Compagni di scuola
 
 
{{Infobox film
| name           = Compagni di scuola
| image          = Compagni di scuola.jpg
| caption        = Film poster
| director       = Carlo Verdone
| producer       = Mario Cecchi Gori Vittorio Cecchi Gori
| writer         = Carlo Verdone Leonardo Benvenuti Piero De Bernardi
| starring       = Carlo Verdone, Nancy Brilli, Massimo Ghini, Eleonora Giorgi
| music          =
| cinematography = Danilo Desideri
| editing        = Antonio Siciliano
| distributor    =
| released       =  
| runtime        = 120 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Compagni di scuola is a 1988 Italian comedy film directed by and starring Carlo Verdone. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Plot==
Federica, a handsome 35 years old woman, left by her rich lover, organizes a reunion with her high school classmates, 15 years after the graduation.
The encounter takes a melancholic turn, as the guests start to reveal their dissatisfactions and annoyances.

==Cast==
* Carlo Verdone - Piero Il Patata Ruffolo
* Christian De Sica - Ciardulli aka Tony Brando
* Nancy Brilli - Federica
* Eleonora Giorgi - Valeria Donati
* Massimo Ghini - On. Mauro Valenzani
* Angelo Bernabucci - Walter Finocchiaro
* Natasha Hovey - Cristina Romagnoli
* Giusi Cataldo - Margherita Serafini
* Luisa Maneri - Gloria Montanari
* Athina Cenci - Maria Rita Amoroso
* Piero Natoli - Luca Guglielmi
* Alessandro Benvenuti - Lino Santolamazza
* Maurizio Ferrini - Armando Lepore
* Isa Gallinelli - Jolanda Scarpellini
* Giovanni Vettorazzo - Francesco Toscani
* Luigi Petrucci - Ottavio Postiglione

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 