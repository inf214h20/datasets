Irish Luck (1925 film)
{{infobox film
| name           = Irish Luck
| image          =
| imagesize      =
| caption        =
| director       = Victor Heerman
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Norman Venner (story "The Imperfect Imposter") Lois Wilson 
| music          =
| cinematography = Alvin Wyckoff
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       = November 22, 1925
| runtime        = 70 minutes 7 reels (7,008 feet)
| country        = United States Silent English intertitles
}}
Irish Luck is a 1925 silent film comedy-drama produced by Famous Players-Lasky and distributed by Paramount Pictures.  

==Cast==
*Thomas Meighan - Tom Donahue/Lord Fitzhugh Lois Wilson - Lady Gwendolyn
*Cecil Humphreys - Douglas Claude King - Solicitor
*Ernest Lawford - Earl
*Charles Hammond - Doctor
*Louise Grafton - Aunt
*S. B. Carrickson - Uncle
*Charles Mcdonald - Denis MacSwiney
*Mary Foy - Kate MacSwiney

==Preservation status==
A print exists at the George Eastman House film archive. 

== References ==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 


 