Murder at Dawn
{{Infobox film
| name           = Murder at Dawn
| image          = 
| caption        = 
| director       = Richard Thorpe
| producer       = John R. Freuler (executive producer) Burton L. King (supervising producer)
| writer         = Barry Barringer (original story and screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Edward A. Kull
| editing        = Frederick Bain
| studio         = 
| distributor    = 
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
}}

Murder at Dawn is a 1932 American film directed by Richard Thorpe.

The film is also known as The Death Ray in the United Kingdom.

== Plot summary ==
 

=== Differences from source ===
 

== Cast ==
*Jack Mulhall as Danny
*Josephine Dunn as Doris Farrington
*Eddie Boland as Freddie
*Marjorie Beebe as Gertrude
*Martha Mattox as The Housekeeper
*Mischa Auer as Henry
*Phillips Smalley as Judge Folger
*Crauford Kent as Arnstein
*Frank Ball as Dr. Farrington
*Alfred Cross as Goddard

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 