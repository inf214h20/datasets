The Farmer's Wife (1998 film)
For other films by the same name, see The Farmers Wife (disambiguation)

The Farmers Wife (1998 film) is a documentary by David Sutherland following the life and troubled marriage of a couple living on a farm in Nebraska.

==Plot==
Filmmaker David Sutherland   takes us deep inside the passionate, yet troubled, marriage of Juanita and Darrel Buschkoetter, a young farm couple in rural Nebraska facing the loss of everything they hold dear.

===Part 1===
 love affair and begins the journey to the core of their emotional struggles, which have pushed their marriage to the brink. Darrel and Juanita tell their own story, in their own words, without the intrusion of a narrator. It unfolds before the viewers eyes, as it is happening.

===Part 2===
In Part 2 of The Farmers Wife, the camera focuses on the rhythms of everyday life on the Buschkoetters farm. The film follows Juanita, Darrel, and their three girls through days reminiscent of a forgotten, simpler time in United States|America. In September, an early frost destroys thirty percent of their crop. Darrel must go to work at a nearby farm for seven dollars an hour and does his own farming at night. Juanita cleans houses while trying to get a college degree so Darrel can stay home and farm, but Darrel worries that if she goes off the farm shell find something she likes better. By Christmas, they are dead broke and unsure of their future.

===Part 3=== stress and exhaustion cause him to explode. In December, Juanita takes the girls and leaves for a week—it has a deep and profound effect on Darrel. Two months later, the marriage that had seemed almost doomed is miraculously transformed. Through counseling, Darrel learns to deal with his anger and undergoes extraordinary personal growth. Now he is the at-home parent, farming and caring for his three daughters. Juanita, who has earned a college degree, works at a respected crop insurance company in town, helping other farmers. In the end, through faith, hope, and hard work, the Buschkoetters save their farm and rediscover the love that holds them together.   

==References==
 

 
 
 
 
 
 
 

 