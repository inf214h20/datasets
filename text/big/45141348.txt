Onde Balliya Hoogalu
{{Infobox film 
| name           = Onde Balliya Hoogalu
| image          =  
| caption        = 
| director       = M. S. Nayak M. Nageshwara Rao
| producer       = Vanamala S Nayak
| writer         = 
| screenplay     =  Balakrishna Dikki Madhavarao
| music          = Chellapilla Satyam
| cinematography = S J Thomas
| editing        = Bal G Yadav Thathayya
| studio         = Amrithakala Productions
| distributor    = Amrithakala Productions
| released       =  
| runtime        = 138 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, Balakrishna and Dikki Madhavarao in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
 
*K. S. Ashwath
*Rajashankar Balakrishna
*Dikki Madhavarao
*Ranga
*B. M. Venkatesh
*Ashwath Narayan
*Muniyappa
*Chandrakala Jayanthi in guest appearance
*Pandari Bai in guest appearance
*B. V. Radha
*Rama
*Annapurna
*Bhujanga Rao
*Srirangamurthy
*Srinivasan
*Ramachandra Rao
*Nagaraj
*Srinath
*Durgaprasad Rao
 

==Soundtrack==
The music was composed by Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Riksha Gadi Mister || PB. Srinivas || Geethapriya || 04.57
|- Janaki || Geethapriya || 04.00
|- Janaki || Geethapriya || 04.14
|-
| 4 || Neenelli Nadevedoora || Mohammed Rafi || Geethapriya || 04.52
|-
| 5 || Ore Notada || PB. Srinivas, Sumithra || Geethapriya || 04.30
|-
| 6 || Yeko Yeko Yethako || Sumithra || Geethapriya || 04.16
|}

==References==
 

==External links==
*  

 
 
 


 