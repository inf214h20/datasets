Rock Paper Dice Enter
   }}
{{Infobox film
| name           = Rock Paper Dice Enter
| writer         = Kash Gauni
| director       = Shreela Chakrabartty
| producer       = {{Plainlist|
* Kash Gauni
* Shreela Charkrabartty
}}
| starring       = {{Plainlist|
 
* Kash Gauni
*  Richard Lee
*  Alyson Dicey 
*  Ojas Joshi
*  Curtis Rind
*  Maire Muncaster
*  Dave Wolkowski
*  Daniel Van Heyst
*  Rick Hardy
*  Brett Miles
*  Gilbert Allan
*  Tom Edwards
*  Georgette Starko
*  Chris W. Cook
*  Danielle Pilon
*  Sharlene Millang-Borst
*  Michelina Iuliano
*  Mark Kandborg
*  Ben Sures
*  Elissa Scott
*  Katie Gobert

}}
| music          = Lance Landiak
| cinematography = Adam Lee
| editing        = VedPrakash
| studio         = Rock Paper Films
| distributor    =  ]
| released       =  
| runtime        = 91 minutes
| country        = Canada
| language       = English
}}

Rock Paper Dice Enter is a 2014 Canadian thriller, directed by Shreela Chakrabartty. It is the first installment of the reported trilogy. It is based on the 2008 manuscript written by Kash Gauni. The film centers on the fictional American city of Strathaven, where security protocols are breached. Multiple timelines within film along with a diamond heist are suspense element of this film.

Kash Gaunis manuscripts started doing rounds in early 2011 and they were considered too intellectual to translate them on big screen. Kash Gauni was retained to write the screenplays and was integrated as a producer on the film. Alberta artiste rallied to make this film happen.

Following the shoot an extensive post and music work gave this film an edge. The rise of this film was slow and it slowly got cult like following.

==Critical reception==
Nominated for The Rosie Award, Best Dramatic Motion Picture of the year.  
 
==Worldwide release==
The film was released in India by PVR on February 7, 2014.   and Worldwide on June 6, 2014. 

==References==
 

==External links==
*  