Devilwood (film)
 {{Infobox film
| name           = Devilwood
| image          = Devilwood_poster.jpg
| caption        = Devilwood poster
| director       = Sacha Bennett
| producer       = Sacha Bennett
| writer         = Sacha Bennett
| narrator       = 
| starring       = John Simm
| music          = Edwin Sykes
| cinematography = Nic Lawson
| editing        = Jake Robertson
| distributor    = 
| released       =  
| runtime        = 11 minutes
| country        = United Kingdom English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Devilwood is a 2006 British short film. It was written and directed by Sacha Bennett, and stars John Simm.

== Plot == whores and Low-life|lowlifes. He offers payment for certain "papers" from Lord Faversham. Soon after, another stranger arrives - an attractive girl named Rossetti - looking for Dante. Neither she, nor Dante, are what they seem. Gabriel, now caught between the two of them, quickly discovers his world is about to end as the Devils work begins.

== Cast ==
*John Simm ...  Gabriel 
*Kate Magowan ...  Rossetti 
*Dylan Brown ...  Dante 
*Jonathan Parsons ...  Lord Faversham 
*Marina Fiorato ...  Lady Faversham 
*Mick Barber ...  Harry 
*Chris Gudgeon ...  Vagabond

== Awards ==
* Best Foreign Short Film - Queens International Film Festival (New York) http://www.imdb.com/title/tt0848581/awards 
* Best Horror - California Independent Film Festival 
* Best Cinematography - California Independent Film Festival 
* Best Cinematography - Phoenix International Horror & Sci-Fi Film Festival 
* Best Costume Design - Phoenix International Horror & Sci-Fi Film Festival 
* Nominated Best Foreign Short Film - Miami Film Festival 
* Nominated Best Horror - Halloween Film Festival (London) 

==References==
 

==External links==
*  

 
 
 
 
 
 