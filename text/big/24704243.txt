Biùtiful cauntri
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Biùtiful cauntri
| image          = Biùtiful cauntri poster.jpg
| alt            = 
| caption        = 
| director       = Esmeralda Calabria, Andrea DAmbrosio
| producer       = 
| writer         = 
| starring       = Raffaele Del Giudice, Mario & Patrizia Gerlando, Mario Cannavacciuolo, Sabatino Cannavacciuolo
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Lumiere & Co
| released       =  
| runtime        = 83 minutes
| country        = Italy
| language       = 
| budget         = 
| gross          = 
}}
Biùtiful cauntri is a 2007 Italian documentary film about illegal toxic waste dumping in Southern Italy. It was directed by Esmeralda Calabria and Andrea DAmbrosio and written by Calabria, DAmbrosio and Peppe Ruggiero. It focuses on the progressive poisoning of thousands of square miles of Southern Italian agricultural land and the deadly effects upon people, animals, and plant life in the areas of Sicily and Naples, and behind that the interwoven relations between the Italian government, corrupt pseudo-legitimate businessmen, and the Italian organized crime group, the Camorra. The title is literally the Italian pronunciation of "beautiful country". A secondary focus of the film is governmental inaction, in some cases lasting over a decade and a half, despite the pleas of the people affected.

==Release and book==
It was premiered in November 2007 at the Torino Film Festival, where it received a special mention. It was then released in ten Italian movie theaters in March 2008. On 15 July 2008 the film was released in France, where it received a positive review from Le Monde. The film was released on DVD along with a 90-page book published under the Senza Filtro imprint of Biblioteca Universale Rizzoli.

==Awards==
*2008 Nastro dArgento "Best Documentary"
*2008 Italian Golden Globes "Best documentary"
*2009 Giancarlo Siani Prize - First Prize "Ex Aequo" for Audiovisual Media (16 October 2009)

==See also==
* Naples waste management issue Ministry of the Environment and Italy#Environment
*   (in Italian Wikipedia)
* Capital punishment in Italy

==References==

  - pressbook    - pressbook  
 
 
 
 
 
 
 
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 