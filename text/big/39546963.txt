Robbers of the Range
{{Infobox film
| name           = Robbers of the Range
| image          =
| caption        =
| director       = Edward Killy
| producer       = Bert Gilroy
| writer         =
| starring       = Tim Holt
| music          =
| cinematography = Harry J. Wild
| editing        = RKO Radio Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Robbers of the Range is a 1941 Western. The film stars Tim Holt, Virginia Vale, and Ray Whitley. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p159  

It was the third in Tim Holts series of Westerns for RKO. 

==Plot==
A railroad agent goes to diabolical lengths to wrest land from a stubborn rancher. The rancher is falsely accused of murder. 

==References==
 

==External list==
*  

 
 
 
 
 


 