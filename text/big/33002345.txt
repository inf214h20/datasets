Resham Ki Dori
{{Infobox film
| name = Resham Ki Dori
| image = dvdreshamkidori.jpeg
| caption = Dvd Resham Ki Dori
| director = Atma Ram
| producer = T. C. Dewan
| screenplay = Ranjan Bose Kumud Chugani
| music = Shankar Jaikishan|Shankar-Jaikishan
| editing = Y. G. Chawhan|Y. G. Chavan
| distributor = Modern Pictures
| released = 21 August 1974
| runtime = 141 minute
| country =  
| language = Hindi
}}

Resham Ki Dori is a 1974 Bollywood film starring Dharmendra and Saira Banu. The film was rated "above average" at the box office.

== Plot & Reviews ==

Ajit and Rajoo become orphans. Ajit, as
the older brother takes care of his
younger sister, at great personal
sacrifice.
When he tries to save his sister from
sexual assault, death occurs. Is Ajit
responsible? He emerges as Vinod from
jail.
In English the loose translation of the
title would be "a silken thread" a
compact translation would be a "silken
skein".
Ajit, and his little sister are separated
by powers that neither he or she
understand. Resham ki dori - is the
thread that brings them back together.
This is a story of three generations of
arrogance of wealth. It is also a story of
great wealth, dishonest managers,
anger, arrogance in semi- feudal India.
In a social context, it is a story of the
death of the textile industry in Parel,
Lower Parel in Bombay. It is a story of
the exploitation of labour. Some scenes
of labour poverty is gut wrenching.
It is a vision of a person who could
reverse the process of attrition in the
textile industry, if only till the textile
mill was set on fire for insurance.
It is a great movie to watch, if you are
looking out for the social context.
Dialogues are predictable, mostly
studio shot, editing is a bit loose but
overall, a slightly differently managed
film than the usual movies of the time.
1980s.
Excellent storyline, and some superb
acting. 

== Cast ==

* Saira Banu as Anupama
* Dharmendra as Ajit Singh
* Kumud Chugani as Rajjo Sujeet Kumar as Dinesh
* Ramesh Deo as Police Inspector Ranbir Rajendernath as Banke Biharilal
* Rajan Haksar as Mastan
* Jayshree T. as Dancer in song "Sona Hai Chandi Hai"
* Sajjan as Vishal
* Meena T. as Sheela
* Master Bhagwan as Bhagwan Das
* D. K. Sapru as Anupamas Grandfather
* Kundan
* Shivraj as Bade Babu Janki Das as Babys Guardian
* Bhagwan Sinha
* Keshav Rana as Jailer
* Master Sachin as Young Ajit
* Baby Chintu as Young Rajjo
* Fatima Bai
* Ramlal
* Madhu Kunwar Ajit
* Leela Mishra as Bade Babus Wife (uncredited)
* Murad as The Judge (uncredited)
* H. L. Pardesi as Inmate #95 (uncredited)
* Chand Usmani as Shanti (uncredited)

== Soundtrack ==

{{Infobox album
| Name = Resham Ki Dori
| Type = Album
| Artist = Shankar Jaikishan|Shankar-Jaikishan
| Cover =
| Released = 31 December 1974 Feature film soundtrack
| Label = Saregama
| Producer = T. C. Dewan
}}

Music composed by Shankar-Jaikishan and lyrics by Indeewar|Indiwar, Neeraj & Hasrat Jaipuri. 

{|
|+ Original Motion Pictures
|-
! Track
! Song
! Singer(s)
! Lyric
|- 1
| Sone Ke Gehne Kyon Too Ne Pehne
| Mohd. Rafi
| Indiwar
|-
| 2
| Sona Hai Chandi Hai
| Asha Bhosle & Chorus
| Neeraj
|-
| 3
| Hai Jag Men Jiska Naam Amar
| Manna Dey
| Neeraj
|-
| 4
| Behna Ne Bhai Ki Kalai Se Pyar Bandha
Hai
| Suman Kalyanpur
| Indiwar
|-
| 5
| Chamka Pasina Ban Ke Nagina
| Kishore Kumar
| Indiwar
|-
| 6
| Zohra Jamal Hoon Bemisal Hoon
| Asha Bhosle
| Hasrat Jaipuri
|}

== Trivia ==

This film was to star Rajesh Khanna and Saira Banu in the lead but Khanna had date issues so asked director to take some one else.Then director replaced Khanna with Dharmendra. 

== Awards & Nominations ==

* Nomination Filmfare Award for Best Actor - Dharmendra as Ajit Singh
* Nomination Filmfare Award for Best Supporting Actress - Jayshree T. Indiwar for the song "Behna Ne Bhai Ki Kalai Se Pyar Bandha Hai"
* Nomination Filmfare Award for Best Music Director - Shankar Jaikishan|Shankar-Jaikishan
* Nomination Filmfare Award for Best Female Playback Singer - Suman Kalyanpur for the song "Behna Ne Bhai Ki Kalai Se Pyar Bandha Hai"

== References ==
 

== External links ==
*   by Internet Movie Database

 
 