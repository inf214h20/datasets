A Woman's Revenge (1990 film)
 
{{Infobox film
| name           = A Womans Revenge
| image          = 
| caption        = 
| director       = Jacques Doillon
| producer       = Alain Sarde
| writer         = Jacques Doillon Jean-François Goyet Fyodor Dostoyevsky
| starring       = Isabelle Huppert
| music          = 
| cinematography = Patrick Blossier
| editing        = Catherine Quesemand
| distributor    = 
| released       = 10 January 1990
| runtime        = 133 minutes
| country        = France
| language       = French
| budget         = 
}}

A Womans Revenge ( ) is a 1990 French drama film directed by Jacques Doillon and starring Isabelle Huppert.    It was entered into the 40th Berlin International Film Festival.   

==Cast==
* Isabelle Huppert - Cécile
* Béatrice Dalle - Suzy
* Jean-Louis Murat - Stéphane
* Laurence Côte - Laurence
* Sebastian Roché - Le dealer
* David Léotard - Le jeune homme
* Albert Le Prince - Le médecin (as Albert Leprince)
* Brigitte Marvine - La danseuse
* Pierre Amzallag - Le voisin
* Jean-Pierre Bamberger - La présence

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 