Silva (film)
{{Infobox film
| name =  Silva 
| image =
| image_size =
| caption =
| director = Aleksandr Ivanovsky 
| producer =   Leo Stein (operetta)   Béla Jenbach (operetta)
| narrator =
| starring = Zoya Smirnova-Nemirovich   Sergei Martinson  
| music = Emmerich Kalman (operetta)
| cinematography = Iosif Martov 
| editing =   
| studio = Sverdlovsk Film Studios 
| distributor = 
| released = 1944
| runtime = 
| country = Soviet Union Russian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Silva is a 1944 Soviet musical film directed by Aleksandr Ivanovsky and starring  Zoya Smirnova-Nemirovich and Sergei Martinson. It was part of a cycle of operetta films made in European cinema during the era.

==Production==
The film is an adaptation of the 1915 operetta Die Csárdásfürstin (also known as Silva after its title character) composed by  Emmerich Kalman with a libretto by  Leo Stein (writer)| Leo Stein and Béla Jenbach. It was made at the Sverdlovsk Film Studios in Yekaterinburg. 

==Popularity== escapist entertainment and largely rejected films with war themes. The money it earned per copy of the film issued exceeded even that of the most popular films of the year Guilty Without Fault. 

==References==
 

==Bibliography==
* Egorova, Tatiana K. Soviet Film Music: An Historical Survey. Psychology Press, 1997.
* Spring, Derek & Taylor, Richard. Stalinism and Soviet Cinema. Routledge, 2013.

==External links==
* 

 
 
 
 
 
 
 
 

 