A Little Girl in a Big City
{{Infobox film
| name           = A Little Girl in a Big City
| image          =
| caption        =
| director       = Burton L. King
| producer       = Gotham Productions
| writer         = Victoria Moore (scenario)
| based on       = play, A Little Girl in a Big City, by James Kyrle MacCurdy
| starring       = Gladys Walton Niles Welch
| music          =
| cinematography = Jack Young C.J. Davis
| editing        =
| distributor    = Lumas Film
| released       = July 20, 1925
| runtime        = 6 reels
| country        = USA
| language       = Silent..English intertitles

}}
A Little Girl in a Big City is a 1925 silent film drama directed by   film. 

It is preserved by the Library of Congress    It can be found on Grapevine DVD. 


==Cast==
*Gladys Walton - Mary Barry
*Niles Welch - Jack McGuire
*Mary Thurman - Mrs. Howard Young
*J. Barney Sherry - Howard Young
*Coit Albertson - D. V. Cortelyou
*Helen Shipman - Rose McGuire
*Sally Crute - Mrs. Barry
*Nellie Savage - Dolly Griffith

==References==
 


==External links==
* 
* 

 
 
 
 
 

 