Sexo con Amor
{{Infobox Film | name = Sexo con Amor
| starring    = Sigrid Alegría Álvaro Rudolphy Patricio Contreras María Izquierdo Boris Quercia Cecilia Amenábar Francisco Pérez-Bannen Javiera Díaz de Valdés
| image	=	Sexo con Amor FilmPoster.jpeg
| director    = Boris Quercia 
| writers     = Boris Quercia 
| producer    = Diego Izquierdo 
| distributor = 20th Century Fox 
| release date= August 4, 2006 
| runtime     =  
| language    = Spanish 
| rating      = 
| music       = 
| awards      = 
| budget      = 
}}

Sexo con Amor (Sex with Love) is a 2003 Chilean comedy film about sex and relationships. The film was directed by Boris Quercia, who also wrote the screenplay and even played a role in the film.

== Plot ==
A group of parents of fourth-grade students addresses how the school should deal with sex education. But sexuality is still an unresolved issue for many of the parents themselves. Sexo con Amor is the story of how three of these couples are ambushed by their own erotic passions. They each profess undying love to their partners while hopping enthusiastically on a merry-go-round of physical relations with one another, leading to sometimes tragic and sometimes hilarious effects.

==Cast==
*Sigrid Alegría as Luisa
*Álvaro Rudolphy as Álvaro
*Patricio Contreras as Jorge
*María Izquierdo	as Maca
*Boris Quercia as Emilio
*Cecilia Amenábar as Elena
*Francisco Pérez-Bannen as Valentín
*Javiera Díaz de Valdés as Susan
*Loreto Valenzuela as Mónica

== External links ==
*  
* http://sexoconamor.mex.tl/1928570_La-pelicula.html

 
 
 
 
 


 
 