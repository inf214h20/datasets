Znachor
{{Infobox film
| name           = Znachor
| image          =
| image size     =
| caption        =
| director       =Michał Waszyński
| producer       =
| writer         = Tadeusz Dolega-Mostowicz (novel), Anatol Stern (writer)
| narrator       =
| starring       =Kazimierz Junosza-Stepowski, Elżbieta Barszczewska
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =1937
| runtime        =100 minutes
| country        = Poland Polish
| budget         =
}}
 1937 Cinema Polish drama film directed by Michał Waszyński. It is followed by Profesor Wilczur (1938).

==Cast==
*Kazimierz Junosza-Stepowski ...  Prof. Rafal Wilczur, alias Antoni Kosiba
*Elżbieta Barszczewska ...  Beata Wilczurowa, wife / Maria Wilczurówna, daughter
*Witold Zacharewicz ...  Leszek Czynski
*Józef Wegrzyn...  Dr. Dobraniecki
*Mieczyslawa Cwiklinska...  Florentyna Szkopkowa
*Romuald Gierasienski ...  Franek, the usher (as R. Gierasienski)
*Stanislaw Grolicki ...  Prokop, father
*Wlodzimierz Lozinski ...  Wasyl Prokop
*Wojciech Brydzinski ...  Father Czynski
*Wanda Jarszewska ...  Mother Czynska
*Tadeusz Fijewski ...  Dr. Pawlicki
*Marian Wyrzykowski ...  Janek, lover
*Jacek Woszczerowicz ...  Jemiol
*Zygmunt Biesiadecki ...  The Tramp

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 
 