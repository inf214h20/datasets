Brahmastram
{{Infobox film
| name           = Brahmastram
| image          =
| caption        =
| writer         = Bhaskarbhatla Rajashekaran  
| story          = Suryakiran
| screenplay     = Suryakiran
| producer       = Nukarapu Surya Prakasa Rao
| director       = Suryakiran
| starring       = Jagapati Babu Neha Oberoi
| music          = Vaibhava
| cinematography = Sarath
| editing        = Nandamuri Hari
| studio         = S. P. Creations    
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

 Brahmastram  ( , Neha Oberoi in the lead roles and music composed by Vaibhava.            

==Cast==
*Jagapati Babu as Guru / Bangaram
*Neha Oberoi as Gayatri
*Kalabhavan Mani as Vasudev
*Ashish Vidyarthi as Rudra
*Kota Srinivasa Rao
*Brahmanandam
*M. S. Narayana
*Raghu Babu as A.K.47 Venu Madhav
*Kavitha
*Sujitha as Tulasi

==Soundtrack==
{{Infobox album
| Name        = Brahmastram
| Tagline     = 
| Type        = film
| Artist      = Vaibhav
| Cover       = 
| Released    = 2006
| Recorded    = 
| Genre       = Soundtrack
| Length      = 26:27
| Label       = MADHURA Audio
| Producer    = Vaibhav
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Vaibhav. Lyrics written by Bhaskarbhatla Ravikumar Music released on MADHURA Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:27
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Adugu Adugu
| extra1  = Venu,Gayathri
| length1 = 4:15

| title2  = Nuvvu Avunantava 
| extra2  = Karthik (singer)|Karthik,Swathi
| length2 = 4:48

| title3  = Om Hare Rama 
| extra3  = Andrea
| length3 = 4:05

| title4  = Jack & Jill Sujatha
| length4 = 4:24

| title5  = Machilipatnam 
| extra5  = Tippu (singer)|Tippu,Anuradha Sriram
| length5 = 4:42

| title6  = Brahmastram  
| extra6  = Vaibhav
| length6 = 4:13
}}
   

==References==
 

 
 


 