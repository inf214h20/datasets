A Different Story
{{Infobox film
| name           = A Different Story
| image          =File:A Different Story film poster.jpg
| alt            =A Different Story film poster
| caption        =A Different Story film poster
| director       = Paul Aaron
| producer       = Alan Belkin
| writer         = Henry Olek
| starring       = Meg Foster  Perry King  Valerie Curtin  Peter Donat
| music          = David Michael Frank (as David Frank)
| cinematography = Philip H. Lathrop
| editing        = Lynn McCallon
| studio         = Petersen Films AVCO Embassy Pictures  American Cinema Releasing
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| followed_by    =
}} American film gay man housemates but R by the Motion Picture Association of America.

While the depiction of the relationship in the film has been lauded, the film has been heavily criticized for stereotyped portrayals of homosexuality and its message that a homosexual can turn heterosexual.

== Plot ==
 real estate rental clients. She finds Albert squatting in one of her properties and she offers Albert to spend the night at her house on the couch. The next day, she goes to work, expecting Albert to move out, but instead Albert cleans her cluttered house and cooks a fantastic dinner. Without verbally acknowledging it, they agree that Albert can stay longer and perform domestic duties while Stella continues working. Albert also gets a part-time job as a valet parking|valet.

The next night, Stella has a date with Chris (Lisa James). Only when the two of them kiss does Albert realize Stella is a lesbian. Chris spends the night. In the middle of the night, Phyllis (Valerie Curtin), another lover of Stella, storms into the house and finds Stella in bed with Chris. Stella apologizes to Phyllis and they do not break off their relationship. In the meantime, Albert has found a new lover, Roger (Doug Higgins), that he met at the Gay bathhouse|baths. Though they continue their separate homosexual relationships, Stella and Albert find that they enjoy spending more time with each other than anyone else. Stellas parents visit one day and come under the impression that she and Albert are dating.
 illegal alien from Belgium. Stella marries him to prevent his deportation. On Alberts birthday, when they are both drunk, they have sex for the first time and enjoy it. From then on, they sleep in the same bed and begin acting like a heterosexual married couple. Stella becomes pregnant and eventually tells Phyllis, who has been distraught about how infrequently she sees Stella. Phyllis becomes suicidal, so Stella and Albert break into her apartment and find her with a gun. She threatens to kill Stella and fires, but the gun is not loaded. Phyllis bursts into tears.

Later the baby is born and they move into a new house. Albert begins a job as an apprentice fashion designer and Stella puts her job on hold to raise the baby. Stella becomes jealous that Albert may be having a homosexual affair with his boss, Ned (Guerin Barry). She sneaks into Alberts workplace late one evening after an office party and finds Albert naked in the shower not with Ned, but with a female model. Stella moves out of their home with the baby and threatens a divorce. Albert tries to apologize numerous times and gives one final try when Stella is showing a property to a client. When she doesnt accept him again, he drives away. She changes her mind, but before she can say anything, he crashes his motorcycle into a tree. She runs over, full of tears, but he is not seriously hurt.

== Cast ==

*Meg Foster as Stella Cooke
*Perry King as Albert Walreavens
*Valerie Curtin as Phyllis
*Peter Donat as Sills
*Richard Bull as Mr. Cooke
*Barbara Collentine as Mrs. Cooke
*Guerin Barry as Ned
*Doug Higgins as Roger
*Lisa James as Chris
*  as Richard II

== Reception ==

=== Film critics === Variety stated that "...Perry King and Meg Foster are excellent as a couple whose budding romance has just one problem: they are both gay. First class productions only - but serious - flaw is a Henry Olek script that begins with brilliant cleverness but dissolves by fadeout into formula banality." 
 Time Out Film Guide derides A Different Story as a "glossily persuasive film which presents its real gays as neurotics or gangsters, and offers us so many clichés about role reversal, marriage and pregnancy that it makes An Unmarried Woman look like an intelligent study of divorce." 

  movie review, June 14, 1978. Accessed December 9, 2009. 

=== Gay rights groups === gay rights groups protested the stereotyped depiction of homosexuals in the film and their ability to convert to heterosexuality. Gay Activists Alliance circulated a letter speaking out against the film.   Gay Left, a journal of gay rights and socialism, criticized A Different Story as another example of the gay films of the late 1970s which presented stereotyped homosexuals. "It often seems that the wider the commercial audience appeal is meant to be, the more objectionable are the gay characters and relationships portrayed. The superficially liberal approach of a film such as A Different Story is ultimately just as negative about gay sexuality."  Film critic Janet Maslin elaborates "The movies use of   homosexuality is indeed exploitative, insensitive, and offensive in a variety of ways. Even worse, it is unconvincing... Alberts homosexuality is nothing but a gimmick, something for the screenplay to coax him out of." 

== Home video ==

The movie was released on VHS by Sony Pictures on June 22, 1994. On July 25, 2006, it made its DVD debut through Trinity Home Entertainment.

== See also ==

* List of lesbian, gay, bisexual or transgender-related films

== Bibliography ==

* Hadleigh, Boze. The lavender screen: the gay and lesbian films: their stars, makers, characters, and critics. Secaucus, N.J: Carol Pub. Group; 1993. Chapter 24.

== References ==
 

== External links ==

* 

 
 
 
 
 
 
 