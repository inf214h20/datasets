The Hunter's Moon (film)
 The Hunters Moon is a 1999 action drama directed by Richard C. Weinman and starring Burt Reynolds. The supporting cast includes Keith Carradine and Hayley DuMond. Although this film did have a very limited theatrical run, it wasnt generally seen until its release on home video and remains one of the most obscure Reynolds films.

==Plot summary==

In this Depression-era drama, Carradine plays Turner, a World War I vet who is haunted  not only by memories of the war but by the civil and economic unrest of the time. He stumbles upon a beautiful backwoods mountainside where he falls hopelessly in love with Flo (Hayley DuMond), the daughter of tyrannical landowner Clayton Samuels (Burt Reynolds). Clayton does not approve of his daughters relationship and will stop at nothing to end it.

==Trivia==
*This was one of several roles Reynolds undertook following a financial crisis the artist underwent in the late 1990s. Boogie Nights was another. Many fans feel, however, The Hunters Moon  features the better performance of the two, despite the fact he was nominated for an Oscar for Boogie Nights.

*The chemistry between veteran Carradine and newcomer, Hayley DuMond was real. In 2006 they wed in real life.

==External links==
*  

 
 
 

 