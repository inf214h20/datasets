Choodalani Vundi

{{Infobox film
| name           = Choodalani Vundi
| image          = Choodalani Vundi..jpg
| writer         = Gunasekhar Diwakar Babu
| starring       = Chiranjeevi Soundarya Prakash Raj Anjala Zaveri
| director       = Gunasekhar
| producer       = Ashwini Dutt
| distributor    = Vyjayanthi Movies
| released       = 27 August 2001
| runtime        =
| cinematography = Chota K. Naidu
| language       = Telugu
| country        = India
| music          = Mani Sharma
| awards         =
| budget         =   6 Crores.
| Box Office     =  26 Crores.
}} Tollywood Action South Filmfare Awards. The movie was later dubbed in Hindi as Meri Zindagi Ek Agnipath. It was later remade into Tamil as Calcutta and Hindi as Calcutta Mail with Anil Kapoor and Rani Mukerji.

==Plot==
Ramakrishna (Chiranjeevi) is a mechanic whose life changes when he meets Priya (Anjala Zaveri) at a train station. She sees him and feels some inexplicable connection and runs away with him to flee her father, Mahendras (Prakash Raj) goons. They end up living in the forest with their son, but her father, who is an underworld don, kidnaps her so that he can marry her off to another dons son. Ramakrishna confronts her father and in the ensueing struggle Priya takes the bullet shot at her husband and dies. Their son, because of the shock, loses his voice and Ramakrishna is jailed. Mahendra takes the boy away to Kolkata, where the story originally started. Rama Krishna with the help of Padmavathi (Soundarya) reunites with his son.

==Cast==
* Chiranjeevi .......... Ramakrishna
* Soundarya ...........Padmavathi
* Prakash Raj..........Mahendra
* Anjala Zaveri..........Priya
* Dhulipala...............Mahendras Father
* Brahmanandam
* M. S. Narayana
* Brahmaji

==Box office== Tollywood in 1998.

==Awards== Filmfare Awards
* Filmfare Award for Best Music Director – Telugu - Mani Sharma
* Filmfare Award for Best Art Director – South - Thotta Tharani

==Plagiarism==
The beginning of the song - "Simbale Simbale" has visuals of Lion King (opening sequence). 

==External links==
*  
 

 
 
 
 
 
 
 
 
 

 