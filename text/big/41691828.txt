The Countess of Parma
{{Infobox film
| name = The Countess of Parma
| image =
| image_size =
| caption =
| director = Alessandro Blasetti
| producer = 
| writer = Alessandro Blasetti   Aldo De Benedetti   Gherardo Gherardi   Mario Soldati
| narrator =
| starring = Elisa Cegani   Antonio Centa   María Denis   Umberto Melnati
| music = Amedeo Escobar   Giovanni Fusco 
| cinematography = Otello Martelli   Giovanni Vitrotti
| editing = Ignazio Ferronetti   Alessandro Blasetti
| studio = Industrie Cinematografiche Italiane 
| distributor = Artisti Associati 
| released = 1936
| runtime = 86 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} International football player whose aunt has just acquired the store intending to replace its reliance on French fashions with Italian designs. 

Blasetti later described it as his only "white telephone film. 

== Cast ==
* Elisa Cegani as Marcella 
* Antonio Centa as Gino Vanni 
* María Denis as Adriana 
* Umberto Melnati as Carrani 
* Osvaldo Valenti as Duca di Fadda 
* Nunzio Filogamo as Conte di Sebasta
* Ugo Ceseri as Marco 
* Pina Gallini as Marta Rossi 
* Marichetta Stoppa as Una Guardarobiera 
* Giannina Chiantoni as La sarta della casa di mode 
* Mario Lembo as Il tassista 
* Pina Valli as Unindossatrice 
* Mirica Albis as Unaltra indossatrice

== References ==
 

== Bibliography ==
* Liehm, Mira. Passion and Defiance: Film in Italy from 1942 to the Present. University of California Press, 1984.
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.
* Paulicelli, Eugenia. Fashion Under Fascism: Beyond the Black Shirt. Berg, 2004.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 