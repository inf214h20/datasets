Jingle All the Way
 
 
{{Infobox film
| name           = Jingle All the Way
| image          = Jingle All the Way_poster.JPG
| border         = yes
| caption        = Theatrical release poster Sinbad Phil Jim Belushi Chris Columbus Michael Barnathan Mark Radcliffe
| director       = Brian Levant
| cinematography = Victor J. Kemper
| editing        = Kent Beyda Wilton Henderson Adam Weiss
| writer         = Randy Kornfield Chris Columbus  (rewrite; uncredited)  David Newman
| studio         = 1492 Pictures
| distributor    = 20th Century Fox
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $75 million  
| gross          = $129.8 million
}}
 Christmas family family comedy film directed by Brian Levant and starring Arnold Schwarzenegger and Sinbad (entertainer)|Sinbad, with Phil Hartman, Rita Wilson, Jake Lloyd, James Belushi and Robert Conrad. The plot focuses on two rival fathers, workaholic Howard Langston (Schwarzenegger) and stressed out postal worker Myron Larabee (Sinbad), both desperately trying to retrieve a Turbo-Man action figure for their respective sons on a last minute shopping spree on Christmas Eve.
 Chris Columbus Planet of Twin Cities Saint Paul at a variety of locations, including the Mall of America. After five weeks filming, production moved to California where scenes such as the end parade were shot. The films swift production meant merchandising was limited to a replica of the Turbo-Man action figure used in the film.

Although some critics felt the film was good family entertainment, it was met with a broadly negative response. Much criticism was attached to the films script, its focus on the commercialism of Christmas, Levants direction and Schwarzeneggers performance. Nevertheless, it proved a success at the box office, generating $129 million worldwide. In 2001, Fox was ordered to pay $19 million to Murray Hill Publishing for stealing the idea for the film; the verdict was overturned three years later.

==Plot==
  in Bloomington, Minnesota]]
Howard Langston (  of Turbo-Man, a popular childrens TV superhero toy that everyone is looking for. Along the way, Howard meets Myron Larabee (Sinbad (entertainer)|Sinbad), a postal worker dad with a rival ambition, and the two soon become bitter competitors in their race for the action figure. During his search, Howard repeatedly runs into Officer Alexander Hummell (Robert Conrad), a police officer who had earlier pulled him over for a traffic violation. After several failed attempts to find the toy in a store, Howard attempts to buy a Turbo-Man from a Mall of America Santa (James Belushi) who is actually the leader of a band of counterfeit toy makers. When he accuses the Santa of undermining the values of Christmas, Howard ends up in a brawl with the gang. He narrowly escapes when the police raid their warehouse and gets out by posing as an undercover detective.
 KQRS radio mail bomb, unaware that it really is one). Officer Hummell tries to open it and it blows up in his face. After his car is stripped by thieves, Howard is ultimately forced to return home empty-handed. Upon seeing Ted in his house placing the star on his tree, Howard gets angry and attempts to steal the Turbo-Man doll from Teds house that he had bought for his son Johnny (E.J. De La Pena), but changes his mind at the last moment. He is attacked by Teds reindeer and the commotion leads him to be caught by Ted and a distraught Liz. Liz and Jamie leave for the local Wintertainment Parade with Ted; Howard follows, aiming to make amends. At the parade, Ted makes a pass at Liz, but she turns him down by hitting him with a thermos of eggnog.

Howard runs into Officer Hummell and accidentally drenches him with hot coffee. In the ensuing chase, Howard runs into a preparations room for the parade and is mistaken for the actor who will play Turbo-Man on a parade float. As the "real" Turbo-Man, he presents the coveted limited-edition Turbo-Man doll to his watching son. Before he recognizes his father, Jamie is chased by Myron, who is dressed as Turbo-Mans arch enemy Dementor (having caught and tied up the real actor (Richard Moll)). As the crowd assume this is all part of the show, Howard attempts to rescue his child by utilizing the Turbo-Man suits equipment.

Howard catches Jamie as he falls from a roof and reveals himself to his son. Myron is arrested while ranting about having to explain the situation, and his failure to get the Turbo-Man toy, to his son. Touched by Myrons words, Jamie gives the doll to him and tells Howard that he does not need it since his father is "the real Turbo-Man".

==Cast==
 
*Arnold Schwarzenegger &ndash; Howard Langston Sinbad &ndash; Myron Larabee
*Phil Hartman &ndash; Ted Maltin
*Rita Wilson &ndash; Liz Langston
*Jake Lloyd &ndash; Jamie Langston
*Robert Conrad &ndash; Officer Alexander Hummell KQRS D.J. (Mr. Ponytail Man) Jim Belushi &ndash; Mall of America Santa
*E.J. De La Pena &ndash; Johnny Maltin
*Laraine Newman &ndash; First Lady Caroline Timmons
*Justin Chapman &ndash; Billy Timmons
*Harvey Korman &ndash; President Fallon Timmons 
*Richard Moll &ndash; Dementor
*Daniel Riordan &ndash; Turbo Man
*Jeff L. Deist &ndash; TV Booster/Puppeteer Paul "The Big Show" Wight &ndash; Giant Santa
*Chris Parnell &ndash; Toy Store Clerk
*Curtis Armstrong- Booster
 

==Production==
===Development===
  Chris Columbus experienced a similar situation in 1995 when he attempted to obtain a Buzz Lightyear action figure from the film Toy Story, released that year. As a result he rewrote Kornfields script, which was accepted by 20th Century Fox.    Columbus was always "attracted to the dark side of the happiest holiday of the year", so wrote elements of the film as a satire of the commercialization of Christmas.    Brian Levant was hired to direct the film. Columbus said Levant "underst  the humor in the material" and "was very animated and excited, and he had a vision of what he wanted to do". Levant said "The story that was important to me was between the father and son...its a story about love, and a fathers journey to deliver it in the form of a Turbo Man doll. The fact that I got to design a toy line and do the commercials and make pajamas and comic books was fun for me as a filmmaker. But at its root, the movies about something really sweet. Its about love and building a better family. I think thats consistent with everything Ive done." 
 Planet of USO tour of Bosnia and Herzegovina,  but Columbus waited for him to return to allow him to audition and, although Sinbad felt he had "messed" it up, he was given the part.  He improvised the majority of his lines in the film;    Schwarzenegger also improvised many of his responses in his conversations with Sinbads character.   

===Filming=== Twin Cities Linden Hills, Edina and Saint Paul. Unused shops in the Seventh Place Mall area were redecorated to resemble Christmas decorated stores,  while the Energy Park Studios were used for much of the filming and the Christmas lights stayed up at Rice Park for use in the film.  The Mall of America and the states "semi-wintry weather" proved attractive for the studio.  Although Schwarzenegger stated that the locals were "well-behaved" and "cooperative", Levant often found filming "impossible" due to the scale and noise of the crowds who came to watch production, especially in the Mall of America,    but overall found the locals to be "respectful" and "lovely people."    Levant spent several months in the area before filming in order to prepare. The film uses artistic license by treating Minneapolis and Saint Paul as one city, as this was logistically easier; the police are labeled "Twin Cities Police" in the film.  Additionally, the citys Holidazzle Parade is renamed the Wintertainment Parade and takes place on 2nd Avenue during the day, rather than Nicollet Mall at night. Levant wanted to film the parade at night but was overruled for practical reasons. 
 matte shots Pasadena furniture warehouse was used.  Turbo-Man was created and designed for the film. This meant the commercials and scenes from the Turbo-Man TV series were all shot by Levant, while all of the Turbo-Man merchandise, packaging and props shown in the film were custom made one-offs and designed to look "authentic, as if they all sprang from the same well."  Along with Columbus and Levant, production designer Leslie McDonald and character designer Tim Flattery crafted Turbo-Man, Booster and Dementor and helped make the full-size Turbo-Man suit for the films climax.  Principal production finished in August; Columbus "fine-tun  the picture until the last possible minute," using multiple test audiences "to see where the big laughs actually lie." 

==Soundtrack==
{{Infobox album
| Name        = Jingle All the Way
| Type        = soundtrack
| Artist      = Various
| Cover       = 
| Released    = 1996
| Recorded    = 
| Genre       = Various
| Length      = 36:51 TVT
| Producer    = 
}} Audio CD David Newmans Intrada Music Group released a Special Collection limited edition of Newmans full 23-track score on November 3, 2008. 

===Track listing===
{{tracklist
| extra_column    = Artist 
| writing_credits = yes
| title1          = Jingle Bells James Pierpont
| extra1          = The Brian Setzer Orchestra
| length1         = 2:18
| title2          = So They Say Its Christmas
| writer2         = Brian Setzer
| extra2          = Lou Rawls,  The Brian Setzer Orchestra
| length2         = 4:05
| title3          = Sleigh Ride
| writer3         = Leroy Anderson, Mitchell Parish
| extra3          = Darlene Love,  The Brian Setzer Orchestra
| length3         = 2:36
| title4          = Run Rudolph Run
| writer4         = Marvin Brodie, Johnny Marks
| extra4          = Chuck Berry
| length4         = 2:44
| title5          = Its the Most Wonderful Time of the Year
| writer5         = Edward Pola, George Wyle
| extra5          = Johnny Mathis
| length5         = 2:47
| title6          = Merry Christmas Baby
| writer6         = Lou Baxter, Johnny Moore Charles Brown
| length6         = 4:47
| title7          = Back Door Santa
| writer7         = Clarence Carter, Marcus Daniel
| extra7          = Clarence Carter
| length7         = 2:09
| title8          = The Christmas Song Robert Wells
| extra8          = Nat King Cole
| length8         = 3:10
| title9          = Jingle Bell Rock
| writer9         = Joe Beal, Joseph Carleton Beal, Jim Boothe, James Ross Boothe
| extra9          = Bobby Helms
| length9         = 2:12
| title10         = Father and Son David Newman, Cat Stevens
| extra10          = David Newman
| length10         = 3:00
| title11         = Finale Alfred Newman, Stephen Schwartz
| extra11          = David Newman
| length11         = 4:02
| title12         = Deep in the Heart of Xmas
| writer12         = Sammy Hagar, Jesse Harms
| extra12          = The Brian Setzer Orchestra and Darlene Love
| length12         = 2:52
}}

==Release== West Coast 101 Dalmatians. 

The world premiere was held on November 16, 1996 at the  , Space Jam and Ransom (1996 film)|Ransom; it went on to gross $129 million worldwide, recouping its $75 million budget.       The film was released on VHS in October 1997,  and in November 1998 it was released on DVD.  It was rereleased on DVD in December 2004,  followed by an extended directors cut in October 2007, known as the "Family Fun Edition". It contained several minutes of extra footage, as well as other DVD extras such as a behind the scenes featurette.     In December of the following year, the Family Fun Edition was released on Blu-ray Disc. 

==Reception==
{{Quote box
 | quote= "Despite its fairly entertaining buildup and somewhat serious commentary on materialism during the holidays, the end of the movie takes a realistic conceit and adds in comedy sci-fi elements. Not only does the movie take a turn for the cartoony, but the end is basically everybody laughing and learning their lesson, without any realistic resolution of the situation. Its as if the screenwriters couldnt figure an easy way out of Howards situation, so they added in slapstick comedy and the ending from an episode of Full House."
 | source= — Mike Drucker.   
 | width= 30em
 | bgcolor= transparent
 | align= right
 | qalign= left
 | salign= right
}}
The film received generally negative reviews from critics, garnering a 17% "Rotten" rating at Rotten Tomatoes, with 35 negative reviews out of 42 counted.    Emanuel Levy felt the film "highly formulaic" and criticized Levants direction as little more advanced than a television sitcom. Although he felt Hartman, Wilson and Conrad were not given much opportunity to shine due to the script, he opined that "Schwarzenegger has developed a light comic delivery, punctuated occasionally by an ironic one-liner," while "Sinbad has good moments".  Neil Jeffries of Empire (magazine)|Empire disagreed, feeling Schwarzenegger to be "wooden" and Sinbad to be "trying desperately to be funnier than his hat" but praised Lloyd as the "saving grace" of the film. 

  condemned the film, finding it more "cynical" than satirical, stating "this painfully bad movie has been inspired strictly by the potential jingle of cash registers." He wrote of Levants directorial failure as he "offers no...sense of comic timing," while "pauses in the midst of much of the dialogue are downright painful."  Trevor Johnston suggested that the film "seems to mark a point of decline in the Schwarzenegger career arc" and the anti-consumerism message largely failed, with "Jim Belushis corrupt mall Santa with his stolen-goods warehouse...provid  the films sole flash of dark humour." 

IGNs Mike Drucker praised its subject matter as "one of the few holiday movies to directly deal with the commercialization of Christmas" although felt the last twenty minutes of the film let it down, as the first hour or so had "some family entertainment" value if taken with a "grain of salt". He concluded the film was "a member of the so-corny-its-good genre," while "Arnold delivers plenty of one-liners ripe for sound board crank callers."  Jamie Malanowski of The New York Times praised the films satirical premise but felt it was "full of unrealized potential" because "the filmmakers   equate mayhem with humor."  Roger Ebert gave the film two-and-a-half stars, writing that he "liked a lot of the movie", which he thought had "energy" and humor which would have mass audience appeal. He was, though, disappointed by "its relentlessly materialistic view of Christmas, and by the choice to go with action and (mild) violence over dialogue and plot."  Kevin Carr of 7M Pictures concluded that while the film is not very good, as a form of family entertainment it is "surprisingly fun." 

  Razzie Award Worst Director, but lost to Andrew Bergman for Striptease (film)|Striptease; Sinbad, however, won the Blockbuster Entertainment Award for Favorite Supporting Actor in a Family film. 

==Lawsuit==
In 1998, Murray Hill Publishing sued 20th Century Fox for $150,000, claiming that the idea for the film was stolen from a screenplay they had purchased from high school teacher Brian Webster entitled Could This Be Christmas?. They said the script had 36 similarities with Jingle All the Way, including the plot, dialogue and character names.     Murray Hill President Bob Laurel bought the script from Webster in 1993, and sent it to Fox and other studios in 1994 but received no response and claimed the idea was copied by Kornfield, who was Foxs script reader.  In 2001, Fox were found guilty of stealing the idea and ordered to pay $19 million ($15 million in damages and $4 million in legal costs) to Murray Hill, with Webster to receive a portion.        Laurel died a few months after the verdict, before receiving any of the money.  On appeal, the damages figure was lowered to $1.5 million, before the verdict itself was quashed in 2004, with a judge deciding the idea was not stolen as Fox had bought Kornfields screenplay before he or anybody else at Fox had read Could This Be Christmas?.  

==Sequel==
A sequel, Jingle All the Way 2, was released straight-to-DVD in December 2014. Directed by Alex Zamm and produced by WWE Studios and 20th Century Fox, the film has a similar plot to the original, but is otherwise not connected and none of the original cast or characters returned. The lead roles were instead played by Larry the Cable Guy and Santino Marella. 

==See also==
* Jingle All the Way 2, 2014 film
* Jingle All the Way (TV special), 2011 TV film
* Jingle All the Way (Béla Fleck and the Flecktones album), 2008 album by Béla Fleck and the Flecktones
* Jingle All the Way (Crash Test Dummies album), 2002 album by by Crash Test Dummies
* Jingle Bells, popular traditional winter holiday song

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 