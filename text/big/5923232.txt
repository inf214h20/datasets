Crossing the Bridge
 
{{Infobox film
| name = Crossing The Bridge
| image = 
| caption = 
| director = Mike Binder
| producer = Jeffrey Silver Robert F. Newmyer
| writer = Mike Binder
| starring = Josh Charles Jason Gedrick Stephen Baldwin David Schwimmer Abraham Benrubi
| music = Lisa Haley Peter Himmelman
| cinematography = Newton Thomas Sigel (as Tom Sigel)
| editing = Adam Weiss
| studio = Touchstone Pictures Buena Vista Pictures
| released =  
| runtime = 103 minutes
| country = United States
| language = English
| budget = 
| gross = $479,676
}}
Crossing the Bridge is a 1992 American drama film starring Josh Charles, Stephen Baldwin and Jason Gedrick.

Characters Mort Golden (Josh Charles), Tim Reese (Jason Gedrick) and Danny Morgan (Stephen Baldwin) are friends who embark on a dangerous drug-smuggling venture.

The film was created by Mike Binder and loosely based on Binders friends during the late 1970s in the Detroit/Birmingham, MI area.

Much of the plot concerns the three friends driving into Canada as couriers in a drug deal. When returning to the United States at the Ambassador Bridge crossing between Windsor, Ontario and Detroit, the protagonists face possible capture by authorities.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 