The Victory of Conscience
{{infobox film
| name           = The Victory of Conscience
| image          =
| imagesize      =
| caption        =
| director       = Frank Reicher and George Melford
| producer       = Jesse L. Lasky Margaret Turnbull (scenario)
| cinematography = Dent Gilbert
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes; 5 reels
| country        = USA Silent English intertitles
}}
The Victory of Conscience is a 1916 silent film drama produced by Jesse L. Lasky at Famous Players-Lasky and distributed by Paramount Pictures. Frank Reicher directed and Lou Tellegen and Cleo Ridgely star.

==Cast==
*Cleo Ridgely - Rosette Burgod
*Lou Tellegen - Louis, Count De Tavannes
*Elliott Dexter - Prince Dimitri Karitzin
*Thomas Delmar - Remy
*Laura Woods Cushing - Mother Burgod
*John McKinnon - Father Burgod

==Preservation status==
The film is preserved at the Library of Congress.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 