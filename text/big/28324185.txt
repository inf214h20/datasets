The Lash (1934 film)
{{Infobox film
| name           = The Lash
| image          = 
| image_size     = 
| caption        =  Henry Edwards
| producer       = Julius Hagen Brock Williams
| narrator       = 
| starring       = Lyn Harding John Mills Joan Maude Leslie Perrins
| music          = W.L. Trytel Ernest Palmer
| editing        = 
| studio         = Twickenham Studios
| distributor    = Radio Pictures
| released       = 1934
| runtime        = 63 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British drama Henry Edwards horsewhips his son. The film was made as a quota quickie by Twickenham Studios, one of the largest producers of Quota films. 

==Cast==
* Lyn Harding - Bronson Haughton 
* John Mills - Arthur Haughton 
* Joan Maude - Dora Bush 
* Leslie Perrins - Alec Larkin 
* Mary Jerrold - Margaret Haughton 
* Aubrey Mather - Col. Bush 
* D. J. Williams (actor)|D. J. Williams - Mr. Charles 
* Roy Emerton - Steve 
* Victor Stanley - Jake

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 