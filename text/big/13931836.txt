Meet the Browns (film)
 Meet the Browns}}
{{Infobox film
| name = Tyler Perrys Meet the Browns
| image = Browns.jpg
| caption = Theatrical poster
| director = Tyler Perry
| producer = Tyler Perry Reuben Cannon
| writer = Tyler Perry
| based on =   David Mann Tamela Mann Lamman Rucker Jenifer Lewis Sofía Vergara Frankie Faison Margaret Avery
| music = Aaron Zigman
| cinematography = Sandi Sissel
| editing = Maysie Hoy
| studio  = Tyler Perry Studios Lionsgate
| released =  
| runtime = 108 minutes
| country = United States
| language = English
| budget = $20 million 
| gross = $41,975,388
}} romantic comedy-drama film released Lionsgate on play of the same name by Tyler Perry.

==Plot==
Brenda (Angela Bassett) is a single Chicago mother of three who has been struggling for years to keep her kids off of the streets. Suddenly let go from her job with no warning to speak of, the eternally optimistic mother begins to experience a suffocating sense of hopelessness for the very first time in her life. When Brenda receives a death notice claiming that the father she has never met has died, she quickly gathers up the kids and sets out for Georgia to attend the funeral. Upon arriving in the Deep South, the once fretful mother is pleasantly surprised to discover that there is a whole side of the family she never knew existed. A crass but good-natured clan that welcomes Brenda and her children with open arms, the Browns lazy summer afternoons and frequent trips to the county fair offer a much-needed contrast to the stress of surviving in inner city Chicago.

==Cast==
*Angela Bassett as Brenda Brown
*Rick Fox as Harry Belton
*Lance Gross as Michael Rhodes Jr. David Mann as Mr. Brown
*Jenifer Lewis as Vera Brown
*Sofía Vergara as Cheryl Barranquilla 
*Margaret Avery as Sarah Brown 
*Frankie Faison as Larry "L.B." Brown 
*Tamela Mann as Cora Simmons
*Phillip Van Lear as Michael Rhodes Sr.
*Kristopher Lofton as Calvin Melrose Park 
* Mariana Tolbert as Lena Rhodes
* Chloe Bailey as Tosha Brown Atlanta 
*Irma P. Hall as Miss Mildred
*Lamman Rucker as Will Brown
*LaVan Davis as Henrys
*Tyler Perry as Madea (Mabel Simmons)|Madea, Joe

==Differences from the Stageplay==
*The films plot mainly comes from another Tyler Perry production called Whats Done In The Dark. That particular play has a subplot where a single parent trying to raise a teenage son who gets involved in drug dealing in order to help his mother get out of financial ruin. That plot element is used heavily in Meet the Browns.

*The Browns are only a part of one of the films subplots instead of being the central focus like the stageplay and TV series.

*Mr. Brown is not the protagonist or focal character of this film. He is one of the supporting characters along with the other Brown family members. He is primarily used for comic relief. He later becomes the central character again in the TV series.

*The characters Kim, Milay and Gerald do not appear in the film. Nor do they exist. Kim is later replaced by a new character named Sasha in the TV series.

*In the stageplay, Will is married to Kim with children, and has cheated on Kim. Though in the film, none of that has happened because Kim is absent from this film and it appears that he is not married. However, in the TV series, Will has a wife named Sasha.

==Production==
Meet the Browns completed filming on October 26, 2007. Director Tyler Perry makes a brief but comical memorable cameo appearance
as his world famous gun-toting granny "Madea" and her grouchy brother Joe.

==Stageplay==
 
The film is based on the play of the same name. The film altered almost all the plot details found in the play.

==Television series==
 
 Meet the TBS starring David Mann stageplay and three episodes of Tyler Perrys House of Payne where Mann guest-starred as Mr. Brown, who learns that his deceased father states in his will that he wants him to open a senior citizens home. Brown enlists the help of the Paynes, Cora and Will. The other characters from the play and film are neither mentioned nor seen. However, Browns sister Vera made a guest appearance in the third season of the series.

===Differences from the film===
*Most of the Brown family members in this film do not appear in the series.
*In the film, Will has a minor role as Veras son. In the series, he is one of the main characters.

==Critical reception==
The film received mostly negative reviews from critics. As of February 17, 2015, the review aggregator Rotten Tomatoes reported that 32% of critics gave the film positive reviews, based on 57 reviews.  Metacritic reported the film had an average score of 45 out of 100, based on 14 reviews. 

==Box office performance== Horton Hears a Who!. Although the film ranked second at the box office, it ranked first in average gross per theater, grossing $9,977 per theater compared to Hortons $6,336 per theater. When the film ended its run at the box office, it had grossed $41,975,388 over its $20 million budget, making the film a success. 

==Soundtrack==
The soundtrack was released by Atlantic Records on March 18, 2008.

===Tracklist===
  Estelle
#Face Case
#Sweeter - Gerald Levert Brandy
#Ill Take You There - Kelly Price
#Love Again - Kelly Rowland
#This Gift - Deborah Cox
#Angel - Chaka Khan
#Alright - Ledisi
#Unify - Wynter Gordon
#My Love - Jill Scott
#Hallelujah - Tamela Mann

==Blu-ray / DVD release==

Tyler Perrys Meet the Browns was sold in 1-disc and 2-disc DVD and Blu-ray on July 1, 2008. DVD sales have so far gathered $17,810,803 in revenue. 

==References==
 

==External links==
*  
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 