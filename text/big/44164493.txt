Itha Oru Penkutty
{{Infobox film
| name           = Itha Oru Penkutty
| image          =
| caption        =
| director       = Jayadevan
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha
| screenplay     =
| starring       = Babitha Rajesh 
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Sreedevi Arts
| distributor    = Sreedevi Arts
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Jayadevan and produced by Purushan Alappuzha. The film stars Babitha and Rajesh  in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Babitha
*Rajesh 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oru Gaanachirakil || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 