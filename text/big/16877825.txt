Ace of Aces (1982 film)
 Ace of aces}}
{{Infobox film
| name           = Las des as
| image          = Las des as.jpg
| caption        =
| director       = Gérard Oury
| producer       = Alain Poiré, Horst Wendlandt
| writer         = Gérard Oury, Danièle Thompson Frank Hoffmann Rachid Ferrache Günter Meisner
| music          = Vladimir Cosma
| cinematography = Xaver Schwarzenberger
| editing        = Albert Jurgenson
| distributor    =
| released       = 1982
| runtime        = 100 minutes
| country        = France/Germany
| awards         = German
| budget         = gross = 5,452,593 admissions (France) 
}}

Las des as (The Ace of Aces; alternate English title: The Super Ace) is a 1982 French-German comedy film starring Jean-Paul Belmondo and directed by Gérard Oury. 

==Plot== fighter ace by the names of Gunther von Beckman (Hoffman) and Jo Cavalier (Belmondo) manage to drag each other out of the sky. An argument and subsequent fistfight about who is to be whose prisoner is rudely interrupted by an artillery barrage, forcing both to stick together in order to survive. In a humorous side scene, corporal Adolf Hitler (Meisner) is berated by his frustrated First Lieutenant Rosenblum for his clumsiness.
 Olympic Games in Berlin. On the train Jo meets young Simon Rosenblum (Ferrache), the grandson of aforementioned First Lieutenant Rosenblum and a Jew, and a beautiful reporter named Gaby Delcourt (Pisier), who is to interview Hitler. When his grandfather doesnt show up at the station, Simon asks Jo, whom he idolizes for his WWI days, to accompany him to his grandfathers bookstore. Arriving there, Jo gets into a fight with Gestapo agents who are demolishing the place, and subsequently he is asked by the whole Rosenblum family to hide them. Knowing no other place, he takes them to his teams hotel, which also happens to be Gabys domicile. Jo begins to flamboyantly flirt with Gaby, who seems to return his affections.
 the stadium general of the Luftwaffe. He fast-talks Gunther into borrowing his car, which he gives to the Rosenblums for their escape to Austria. Due to a critical blunder on the Rosenblums part, however, the whole family is caught before they reach the border, and only Simon escapes. The boy phones Gaby, who informs Jo. Torn between his affection for Gaby, his sense of duty for Simon, and the need to see his team in the games (though not in that order), Jo decides to settle the matter as quickly as possible and goes off to fetch Simon.

However, things do not go as planned. The Gestapo is hot on Jos heels, a bear drives him and Simon from their forest camp, and they temporarily pick up its cub, whom they spontaneously name Ludwig van Beethoven|Beethoven. Finally they are captured and taken to the next police station, where the rest of the Rosenblums are also held. Gunther arrives to secure the release of his friend, but Jo wont abandon the Rosenblums and takes Gunther (for all appearances) hostage. As they drive to the Austrian border, Gunther advises Jo to go with the Rosenblums since he is now considered a fugitive criminal. Reluctantly, Jo agrees.
 Berghof residence Angela (again Olympic boxing finals at the radio in the latters personal office. He procures an officers uniform, reveals himself to Gunther and Gaby, and devises a plan to rescue the Rosenblums by stealing Hitlers personal car, while (a very reluctant) Gunther is to create a diversion by eloping with Angela Hitler.

The film ends with a furious car chase between Jo, the Rosenblums and Gaby in one car, and Hitler and his adjutants in another, during the course of which the elderly Rosenblum reveals himself to his old subordinate. Startled by the unexpected encounter with his former commanding officer – and Jo cheekily disguising himself as Hitler -, Hitler crashes into a duck pond, while Jo and company successfully escape to Austria (a humorous hint on the Anschluss which would follow two years later), where they also encounter Beethoven again.
==References==
 
== External links ==
*  
* http://movies.nytimes.com/movie/83071/L-As-des-As/overview
* http://cinema.encyclopedie.films.bifi.fr/index.php?pk=36213

 

 
 
 
 
 
 
 
 
 
 
 
 
 