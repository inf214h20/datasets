Prudence and the Pill
 
 
{{Infobox film
| name           = Prudence and the Pill
| image          = Prudance and the pill.jpeg
| image size     =
| caption        = Film poster
| director       = Fielder Cook  Ronald Neame
| producer       = Kenneth Harper  Ronald J. Kahn
| writer         = Hugh Mills
| narrator       =
| starring       = Deborah Kerr  David Niven
| music          = Bernard Ebbinghous
| cinematography = Ted Moore
| editing        =
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        =
| country        = United Kingdom
| language       = English
| budget         = $3,570,000 
| gross = $4,500,000 (US/ Canada) 
| preceded by    =
| followed by    =
}}
 British comedy film made by Twentieth Century-Fox. It was directed by Fielder Cook and Ronald Neame and produced by Kenneth Harper and Ronald J. Kahn from a screenplay by Hugh Mills, based on his own novel. The music score was by Bernard Ebbinghouse and the cinematography by Ted Moore.

The film stars Deborah Kerr and David Niven with Robert Coote, Irina Demick, Joyce Redman, Judy Geeson, Keith Michell, Michael Hordern and Edith Evans.

== Plot ==
The film portrays the conflicting and comical attempts by five couples to avoid pregnancy by using contraceptive pills. All of their efforts are ultimately unsuccessful, with the result that all five of the women give birth the following year.

The story revolves around a wealthy London banker named Gerald Hardcastle (David Niven|Niven) and his wife Prudence (Deborah Kerr|Kerr), who live together unhappily, sleeping in separate bedrooms and speaking to each other only when necessary. The five couples in the film are (1) Gerald and his French mistress Elizabeth, or "Liz" (Irina Demick|Demick), (2) Prudence and her doctor, Dr. Alan Hewitt (Keith Michell|Michell), (3) the Hardcastless maid Rose (Vickery Turner|Turner) and their chauffeur Ted (Hugh Armstrong|Armstrong), (4) Geralds brother Henry (Robert Coote|Coote) and his wife Grace (Joyce Redman|Redman), and (5) Henry and Graces daughter Geraldine (Judy Geeson|Geeson) and her boyfriend Tony Bates (Lord David Dundas|Dundas).

All of the couples want to use a birth-control pill called "Thenol", but none of them wants to admit it. Prudence, Grace, and Ted manage to acquire supplies of pills, Grace through a prescription written by Hewitt, and Ted from the
local chemist, or pharmacist, who happens to be a friend of his. However, Grace soon becomes pregnant, because Geraldine has been stealing her pills and replacing them with aspirin tablets. After Geraldine admits her pill-switching scheme to Grace and Grace tells Gerald about it, Gerald uses the scheme on Prudence to generate incriminating evidence of her affair.

Meanwhile, believing that Rose is too conservative to accept contraception, Ted puts his tablets in a vitamin bottle and tells her she needs them for her health. However, Rose is worried about becoming pregnant, so she switches the pills in her vitamin bottle with the pills in Prudences Thenol bottle, just moments after Gerald replaces Prudences Thenol with aspirin. The result, then, is that Rose unwittingly trades Teds Thenol for Geralds aspirin. She soon becomes pregnant, and Ted tells Gerald about the pills he gave her, but says nothing about telling her they were vitamins. When Gerald asks her why her Thenol pills failed to work, she asks him how he knew about them, thinking that he has already found out about her taking Prudences pills, at which point they both realise that she has revealed her guilt.

Now knowing why Prudence is still not pregnant, Gerald buys more aspirin, determined to expose her relationship with Hewitt. This eventually works, as do whatever measures Grace took to keep her pills away from Geraldine. By the end of the film, Geraldine and Prudence are both expecting.

Eventually, Gerald gets into trouble with both Liz and Prudence. Liz grows dissatisfied in her covert relationship with Gerald, who has been hiding her from his family, and decides to leave him. Prudence finds the letter that Liz wrote to Gerald about her decision, and Gerald finds Hewitts Thenol prescription for Prudence. At first, neither Gerald nor Prudence is willing to grant the other a divorce, but Prudence offers to take the blame after becoming pregnant, as long as Gerald will spare Hewitts reputation. Gerald accepts this arrangement grudgingly, but before meeting with Hewitt, he happens to see Liz while driving through town, and she tells him she is going to have his baby. Now able to see her openly, and with a child on the way, Gerald quickly and enthusiastically agrees to an amicable divorce. A few months later, a total of six newborn babies arrive, Rose having had twins.

== Cast ==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
|	Deborah Kerr	|| 	Prudence Hardcastle
|-
|	David Niven	|| 	Gerald Hardcastle
|-
|	Robert Coote	|| 	Henry Hardcastle
|-
|	Irina Demick	|| 	Elizabeth Brett
|-
|	Joyce Redman	|| 	Grace Hardcastle
|-
|	Judy Geeson	|| 	Geraldine Hardcastle
|-
|	Keith Michell	|| 	Dr. Alan Hewitt
|-
|	Edith Evans	|| 	Lady Roberta Bates
|- David Dundas	|| 	Tony Bates
|-
|	Vickery Turner	|| 	Rose the maid
|-
|	Hugh Armstrong	|| 	Ted the chauffeur
|-
|	Peter Butterworth	|| 	Chemist
|-
|       Moyra Fraser  ||   Woman in Tea Shop
|-
|       Annette Kerr  ||  Geralds Secretary
|-
|       Harry Towb ||  Racetrack Official
|-
|       Jonathan Lynn ||  Chemists Assistant
|-
|       Michael Hordern (uncredited)||  Dr. Morley
|}

==References==
 
== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 