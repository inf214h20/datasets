Roger la Honte (1933 film)
{{Infobox film
| name = Roger la Honte 
| image =
| image_size =
| caption =
| director = Gaston Roudès
| producer = 
| writer =  Jules Mary (novel)  André Cayatte
| narrator =
| starring = Constant Rémy   Germaine Rouer   France Dhélia
| music =  
| cinematography = Jacques Montéran  
| editing =     Marthe Poncin 
| studio = Compagnie Cinematographique Continentale
| distributor = Compagnie Cinematographique Continentale
| released = 10 March 1933 
| runtime = 95 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama novel of the same name by Jules Mary.  The films sets were designed by the art director Claude Bouxin. 

==Cast==
* Constant Rémy as Roger Laroque  
* Germaine Rouer as Henriette Laroque  
* France Dhélia as Julia de Noirville  
* Marcelle Monthil as Victoire  
* Olympe Bradna as Suzanne Laroque 
* Samson Fainsilber as Lucien de Noirville  
* Paul Escoffier as Le juge dinstruction  
* Édouard Delmont as Linspecteur  
* Marcel Maupi as Linspecteur  
* Raymond Narlay as Le président des assises 
* Georges Mauloy as Le commissaire aux délégations  
* Henri Bosc  as Luversan 
* Jean Arbuleau

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999. 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 

 