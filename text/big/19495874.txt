Det gamle guld
{{Infobox film
| name           = Det gamle guld
| image          = Det gamle guld.jpg
| caption        = Film poster
| director       = Jon Iversen Alice OFredericks
| producer       = Henning Karmark
| writer         = Morten Korch Alice OFredericks
| narrator       = 
| starring       = Poul Reichhardt
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Det gamle guld is a 1951 Danish family film directed by Jon Iversen and Alice OFredericks. 

==Cast==
* Poul Reichhardt - Niels Sværke
* Tove Maës - Grethe Holm
* Maria Garland - Martha Sværke
* Per Buckhøj - Hans Sværke
* Ib Schønberg - Sognefoged Dines Mikkelsen
* Peter Malberg - Jens
* Erika Voigt - Jacobine
* Louis Miehe-Renard - Palle
* Birgitte Reimer - Klara Karius
* Sigurd Langberg - Propritær Karius
* Jørn Jeppesen - Hugo David
* Pia Ahnfelt-Rønne - Maria - pige på Dybegården
* Paul Holck-Hofmann - Bankdirektør Christen Møller - Arkæolog
* Agnes Phister-Andresen - Maren - nabokone
* Jørgen Henriksen - Niels som dreng
* Grethe Holmer
* Morten Korch - Himself
* Aksel Schiøtz - Singer (voice)

==External links==
* 

 

 
 
 
 
 
 
 
 