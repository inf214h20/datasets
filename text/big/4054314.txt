One Last Dance (2003 film)
One American romantic George De La Pena in a major role. It was filmed in Winnipeg, Manitoba, Canada. 

One Last Dance was choreographed by Patsy Swayze, Niemis mother-in-law and the mother of Patrick Swayze.    Niemi, who also wrote the script in addition to directing the film, drew its content from the real-life experiences and struggles of performing artists.

==Plot==
The story revolves around three dancers who are forced to reconcile their differences and pasts. Travis (Swayze), Chrissa (Niemi), and Max (De La Pena) were three students of master choreographer Alex McGrath, but they had a falling out many years ago over a particularly difficult piece that Alex had choreographed specifically for them. Unexpectedly, McGrath dies. This reunites the three, who agree to attempt the dance piece once more to save his company. In the process, however, they all reopen old emotional wounds that had never properly healed.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 