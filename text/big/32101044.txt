Young and Beautiful (film)
{{Infobox film
| name           = Young and Beautiful
| image          =
| caption        =
| director       = Joseph Santley
| producer       = Nat Levine
| writer         = Milton Crims (story) Joseph Santley (story) Dore Schary Colbert Clark (add. dialogue) Al Martin (add. dialogue)
| narrator       =
| starring       = William Haines Judith Allen Joseph Cawthorn
| music          =
| cinematography =
| editing        =
| studio         = Mascot Pictures
| distributor    =
| released       =  
| runtime        = 63 or 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Young and Beautiful (1934) is a romantic comedy film about a press agent who goes to great lengths to make his actress girlfriend a star, only to risk losing her in the process. It stars William Haines and Judith Allen.

==Cast==
*William Haines as Robert Preston
*Judith Allen as June Dale
*Joseph Cawthorn as Herman Cline
*John Miljan as Gordon Douglas
*Ted Fio Rito as Himself
*Al Shaw as Piano Mover (as Shaw and Lee)
*Sam Lee as Piano Mover (as Shaw and Lee)
*James Bush as Dick
*Vince Barnett as Sammy (as Vincent Barnett)
*Warren Hymer as The Champion
*Franklin Pangborn as Radio announcer
*James P. Burtis as Farrell
*Syd Saylor as Hansen
*Greta Meyer as Mrs. Cline
*Fred Kelsey as Hennessy

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 