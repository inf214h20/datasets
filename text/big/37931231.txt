Behaving Badly (film)
{{Infobox film
| name           = Behaving Badly
| image          = Jbehavingbadly.jpg
| caption        = Theatrical release poster
| director       = Tim Garrick
| producer       = Andrew Lazar Miri Yoon
| screenplay     = Tim Garrick Scott Russell
| based on       =  
| starring       = Nat Wolff   Selena Gomez   Mary-Louise Parker   Elisabeth Shue David Newman
| cinematography = Tim Garrick
| editing        = Matt Friedman
| studio         = Mad Chance Starboard Entertainment
| distributor    = Vertical Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $5.5 million
| gross          = 
}}

Behaving Badly is a 2014 American comedy film written and directed by Tim Garrick, which is the film adaptation of the 2000 Ric Browde autobiographical novel While Im Dead Feed the Dog. It stars Nat Wolff and Selena Gomez alongside Mary-Louise Parker. The movie was released on video-on-demand on July 1, 2014, before a theatrical release on August 1, 2014.   

==Plot==
 
Teenager Rick Stevens (Natt Wolff), has a crush on Nina Pennington (Selena Gomez). When Rick realises that Nina broke up with her boyfriend Kevin (Austin Stowell), he places a bet with Karlis Malinauskas (Nate Hartley), a mobsters son, that he will have sex with Nina before Arbor Day, leading to a serious change of events from having sex with his best friends mom (Elizabeth Shue), to having almost the whole town in jail, including Nina.   

==Cast==
* Nat Wolff as Rick Stevens
* Selena Gomez as Nina Pennington
* Mary-Louise Parker as Lucy Stevens/Saint Lola
* Elisabeth Shue as Pamela Bender
* Dylan McDermott as Jimmy Leach
* Lachlan Buchanan as Billy Bender
* Heather Graham as Anette Stratton-Osborne
* Ashley Rickards as Kristen Stevens Jason Lee as Father Krumins 
* Austin Stowell as Kevin Carpenter
* Cary Elwes as Joseph Stevens
* Patrick Warburton as Principal Basil Poole
* Gary Busey as Chief Howard D. Lansing
* Jason Acuña as Brian Savage
* Rusty Joiner as Keith Bender
* Nate Hartley as Karlis Malinauskas
* Mitch Hewer as Steven Stevens Scott Evans as Ronnie Watt
* Gil McKinney as Officer Joe Tackett
* Mindy Robinson as Kristens Friend
* Justin Bieber as Prisoner

==Production== Parental Guidance in the 2012 holiday season having already claimed the title, causing possible confusion. On April 9, 2014 Vertical Entertainment acquired the US rights to the film and had plans for August release.  It was released on iTunes and on demand on July 1, 2014, followed by ten theaters across the US on August 1, 2014.
 
Browde, the author of the original autobiographical novel, has disavowed the film as going against the source material of his book. 

==Home Media==
The film was released on DVD on October 28, 2014.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 