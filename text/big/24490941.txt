Roar of the Press
{{Infobox film
| name           = Roar of the Press
| image          = Poster of the movie Roar of the Press.jpg
| image_size     =
| caption        =
| director       = Phil Rosen
| producer       = Scott R. Dunlap
| writer         = Alfred Block (story) Albert Duffy (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Harry Neumann
| editing        = Jack Ogilvie
| distributor    = Monogram Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Roar of the Press is a 1941 American film directed by Phil Rosen.

== Plot ==
Married only a few hours, small-town girl Alice makes her first visit to New York with new husband Wally Williams, a hotshot reporter for the Globe.

A body falls from a building. Williams steals the identification and calls in the story to city editor MacEwen, who makes Wally follow it up. Reporters wives warn Alice to expect this kind of thing.

A personal ad leads Wally to a second corpse. The police read about in the Globe and angrily haul Wally in for questioning. Alices irritation grows, as does that of reporters from other newspapers at Wallys continued scoops.

Evildoers from an anti-American organization kidnap Wally, and when he wont reveal how he gets his information, they grab Alice as well. Sparrow McGraun runs a numbers racket but likes Wally better than these foreigners, so he saves the newlyweds. A grateful Wally gives this scoop to every paper except the Globe.

== Cast ==
*Jean Parker as Alice Williams
*Wallace Ford as Wally Williams
*Jed Prouty as Gordon MacEwan
*Suzanne Kaaren as Angela Brooks
*Harland Tucker as Harry Brooks
*Evalyn Knapp as Evelyn
*Robert Frazer as Louis Detmar Dorothy Lee as Frances Harris John Holland as Robert Mallon
*Maxine Leslie as Mrs. Mabel Leslie
*Paul Fix as Sparrow McGraun
*Betty Compson as Mrs. Thelma Tate
*Matty Fain as Nick Paul Eddie Foster as Fingers Charles King as Police Lieutenant Homer Thomas Frank OConnor as Police Lieutenant Jim Hall Dennis Moore as Henchman Toughy
*Robert Pittard as Tommy, the Newsboy

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 