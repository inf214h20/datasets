Eleanor and Franklin: The White House Years
{{Infobox film
| name           = Eleanor and Franklin: The White House Years
| image          = Eleanor and Franklin - The White House Years.jpg
| alt            = 
| caption        = DVD cover
| director       = Daniel Petrie
| producer       = Harry R. Sherman David Susskind
| writer         = James Costigan
| based on       =  
| starring       = Jane Alexander Edward Herrmann Priscilla Pointer Walter McGinn John Barry
| cinematography = James Crabe Michael S. McLean Rita Roland
| studio         = Talent Associates
| released       =   ABC
| runtime        = 180 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 32nd U.S. First Lady. Joseph Lash was a secretary and confidant of Eleanor and wrote other books on the couple.

Eleanor and Franklin focused on their respective childhoods, school years, courtship and the lead up to his election. Seven members of the original cast returned for the sequel, including the two main characters portrayed by Jane Alexander and Edward Herrmann. It also won 2 Emmys including outstanding special of the year. Daniel Petrie, who won an Emmy for the first movie, won again for best director. Both films were acclaimed and noted for historical accuracy.

==Cast==
*Edward Herrmann   – Franklin D. Roosevelt (FDR), 32nd President of the United States
*Jane Alexander    – Eleanor Roosevelt, 34th First Lady of the United States Marguerite Missy LeHand. Long-time secretary to Franklin and considered part of the family.
*Walter McGinn     – Louis Howe, intimate friend to both Roosevelts and political advisor to Franklin
*Rosemary Murphy   – Sara Delano Roosevelt, Franklins mother Anna Roosevelt, Eleanor and Franklins eldest child David Healy       – Theodore Roosevelt, 26th President of the United States, uncle to Eleanor and 5th cousin to Franklin
*Peggy McCay       – Grace Tully, long-time friend/secretary to Eleanor and became Franklins top secretary after Missy died. Stalin meeting personally with the leaders and setting up negotiations during World War II.
*Toni Darnay       – Malvina Thompson, Eleanors personal secretary First Lady Presidents Dwight D. Eisenhower and John F. Kennedy. Hyde Park estate
*Mark Harmon     – Robert Dunlap, a soldier
*Anna Lee          – Laura Delano, FDRs cousin Lucy Mercer, mistress of FDR Ike Hoover, Chief Usher of the White House; served both Roosevelt presidents Ray Baker         – James Roosevelt, oldest son of the Roosevelts who served as a secretary in his fathers White House and went on to become a U.S. Marine serving in World War II. He later became a Congressman from California for 10 years.
*Brian Patrick Clarke – John Aspinwall Roosevelt, youngest child of the Roosevelts
*Don Howard        – Elliot Roosevelt, son of the Roosevelts who served in World War II
*Joseph Hacker     – Franklin D. Roosevelt Jr., son of the Roosevelts who also served in the war
*Charles Lampkin   – Irvin McDuffie, FDRs African-American valet during the White House years
*Arthur Gould-Porter – Sir Winston Churchill, Prime Minister of the United Kingdom
*Robert Karnes     – United States Supreme Court Justice Charles Evans Hughes, frequent opponent of FDR in the courts. He also swore him in first 3 of the 4 times he was inaugurated. David Lewis       – United States Supreme Court Justice Melville Weston Fuller, who swore in Teddy Roosevelt.
*Gregory Koontz    – Curtis Roosevelt, eldest grandson of the Roosevelts; Annas son from first marriage
*Davy Muxlow       – John Roosevelt Boettiger, Roosevelts grandson and Annas son from second marriage

==DVD release==
The film was released on DVD by HBO Video on May 21, 2013, with Eleanor and Franklin (1976) on the second disc.

==External links==
*  

 
 
 

 
 
 
 
 
 