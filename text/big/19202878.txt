Saraswati Sabatham
 
{{Infobox film
| name           = Saraswati Sabatham
| image          = Saraswathi sabatham 1966.jpg
| image_size     = 
| caption        = 
| director       = A. P. Nagarajan
| producer       = A. P. Nagarajan
| writer         = A. P. Nagarajan Padmini K. Savitri Manorama Manorama Nagesh
| music          = K. V. Mahadevan
| cinematography = K. S. Prasad
| editing        = M. N. Rajan T. R. Natarajan
| studio         = Sri Vijayalakshmi Pictures
| distributor    = Sri Vijayalakshmi Pictures
| released       =  
| runtime        = 148 mins
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Manorama and Nagesh.

==Plot== sage and celestial troublemaker, begins the fight by visiting Saraswati (Savitri (actress)|Savitri), the Goddess of knowledge, and purposely annoys Her by saying that wealth is more important and abundant. Saraswati angrily said that She will prove that Her power, knowledge is more important. Narada then goes to Vaikuntha to see Lakshmi (Devika), the Goddess of wealth, and also annoys Her by saying that education is more abundant and is the best. Lakshmi also retorted that She will prove that Her power of wealth is more important. Narada finally goes to Mount Kailas, seeking Parvati(Padmini (actress)|Padmini), the goddess of power and strength and annoys Her too, saying that wealth and knowledge is more important. Parvati too stressed that She will prove that Her power of strength is more important.
 mute person, Vidyapati (Sivaji Ganesan), to speak and gives him all the knowledge in the world. There is a place where a Maharaja rules but he has no children. He is dying so his minister sends the royal elephant into the streets with a garland. The person whose neck the elephant puts the garland on will be the next Maharani or Maharaja. Lakshmi controls the royal elephant and makes it give the garland to a beautiful beggar girl (K.R. Vijaya). Now the beggar girl is a very wealthy Maharani and lives in a palace Due to her beauty, everyone wants to marry her. There is someone who has been a coward his whole life (Gemini Ganesan), whom Parvati blesses to become very strong.

Coincidentally the three people chosen by the Goddesses belong to the same country.Three of them became so proud of each others strength. As for the mute person, he has knowledge, as for the beggar girl, she has money and beauty and for the coward, he has bravery. One day the warrior was crowned chief of the soldier lead. When the singer was given a golden pendant, he rejected the gift and then insults the queen in the presence of all that she is a brainless ruler. Suddenly, the warrior ordered the singer to be put to prison without the queens permission. Actually, the queen had fallen head over heels in love with the musician. Secretly she went to the prison and ask him to marry her if he sings of her praise. He rejects. Making the queen angry which she shows the anger to the warrior. The warrior who was very angry killed all the queens soldiers and replace them with his own soldiers. Soon, everyone listened to only his orders. One Day, the warrior intended to kill both the queen and the musician but suddenly his sword disappeared.  In the end the Gods of the Holy Trinity, Lords Shiva(Harnath), Vishnu (Sivakumar), and  Brahma settle the dispute by explaining the importance of knowledge, wealth, and strength combined, and how dangerous it is if each power goes separately. Finally, the three Goddesses reconcile and the three blessed by them along with the common people unite and realise that knowledge, wealth and strength are all equally important in many ways. The three Goddesses blesses the country and Sage Narada, thanking him for making the world understand the unity of knowledge, wealth and strength and for uniting them also, marking the end of the film.

==Cast==
*Sivaji Ganesan as Vidyapati and Narada
*Gemini Ganesan as Malaya
*Devika as Lakshmi Padmini as Parvati Savitri as Saraswati
*Sivakumar as Vishnu
*K. R. Vijaya as Selvambigai Manorama
*Nagesh
*K. Sarangkapani
 

==Soundtrack==
The music composed by K. V. Mahadevan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:10
|-
| 2 || Deivam Iruppathu Engey || T. M. Soundararajan || 03:32
|-
| 3 || Kalviya Selvama Veerama || T. M. Soundararajan || 03:37
|-
| 4 || Komatha Engal Kulamatha || P. Susheela || 07:42
|-
| 5 || Rani Maharani || T. M. Soundararajan || 03:12
|-
| 6 || Thai Thantha || P. Susheela || 03:29
|-
| 7 || Uruvathaikattidum Kanaadi || P. Susheela || 03:40
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 