Haridas (1944 film)
 
{{Infobox film
| name           = Haridas
| image          = Haridas 3years.jpg
| caption        = Promotional poster of the film with a picture of M. K. Thyagaraja Bhagavathar displayed prominently, highlighting the films successful three-year run at Broadway theater (1944, 1945 and 1946).
| director       = Sundar Rao Nadkarni
| writer         = Ilangovan
| starring       = M. K. Thyagaraja Bhagavathar T. R. Rajakumari N. C. Vasanthakokilam N. S. Krishnan T. A. Madhuram Pandari Bai S. R. Krishna Iyengar                
| producer       = Royal Talkies
| distributor    = 
| music          = Papanasam Sivan G. Ramanathan
| cinematography = Adi Irani T. Muthuswamy
| editing        =  Sundar Rao Nadkarni
| released       = 16 October 1944
| runtime        = 117 minutes
| gross          =  20 lakh
| country        = India Tamil
}}

Haridas ( ) is a 1944   included Haridas in its list of 100 greatest Indian films of all time.   

==Production== Marathi film Deepavali Day (16 October) 1944.    Classical musician N. C. Vasanthakokilam, who was often compared to M. S. Subbulakshmi as a singer, played the role of Haridas wife.   The film also marked the debut of renowned Tamil actress Pandari Bai.     The comedic duo of N. S. Krishnan and T. A. Mathuram were also cast in this film. 

==Plot synopsis==
Haridas (Thyagaraja Bhagavathar) is a vain individual who spends his life in luxury and lust ignoring his wife(Vasanthakokilam). But when his wealth is appropriated by a courtesan (T. R. Rajakumari), he realizes lifes realities, reforms and spends the rest of his days serving his parents and God.

==Cast and crew==
  and   and T. A. Mathuram]]
*M. K. Thyagaraja Bhagavathar - Haridas
*T. R. Rajakumari - Haridas mistress
*N. C. Vasanthakokilam - Haridas wife
*N. S. Krishnan
*T. A. Mathuram
*P. B. Rangachari
*Harini
*Radhabai
*Pandari Bai
*S.R.Krishna Iyengar
*Sundar Rao Nadkarni - Editor and Director
*Ilankovan           - Dialogue
*G. Ramanathan       - Music
*Papanasam Sivan - Lyrics
*Adi Irani           - Cinematographer
*T. Muthuswamy       - Cinematographer
*H. Shantaram        - Art Director

==Soundtrack== Tamil usage. Papanasam Sivan was the composer for this film and G. Ramanathan was in charge of the orchestration. A partial list of songs from this film:
  and T. R. Rajakumari in Manmadhan Leelayil song]]
* Manmadha Leelayai Vendrar Undo
*"Krishnaa mukunda muraree"
*"Annaiyum thandhaiyum"
*"Vaazhvil oru thirunaal"
*"Nijamma idhu nijamma"
*"Kadhiravan vudhayam kanden"
*"Ullam kavarum en paavaai"
*"Natanam innum aadanam"
*"Ennalum Indha"
*"Thottadharkellam"
*"Enadhuyir nadhar"
*"Ennudal thanil"
*"Kanna vaa manivanna vaa"

==Reception==
Haridas hit the theatres on Deepavali day (16 October) 1944. It was a huge success and ran for 110 consecutive weeks till Deepavali day (22 November) 1946 at the Sun Theatres in Broadway, Madras.   Including different theatres it had an uninterrupted theatrical run of 133 weeks.  With the profits earned from the film, the producers established a knitting company in Madurai.  Bhagavathar became Tamil cinema industrys highest paid star and was offered a salary of  1 lakh per film.   Following the success of Haridas, Bhagavathar was immediately booked for as many as twelve films.  However, he was not able to enjoy his success as he was arrested in November 1944 as a suspect in the Lakshmikanthan Murder Case.   IBN Live included the film in its list of 100 greatest Indian films of all time. 

==See also==
Lakshmikanthan Murder Case

==References==
 

==External links==
 
*  
*   (In Public domain in India)
*   (In public domain in India)
*   (In public domain in India)
*   (In public domain in India)

==Bibliography==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 