Capriccio (1938 film)
{{Infobox film
| name =  Capriccio 
| image =
| image_size =
| caption = Karl Ritter
| producer =  Karl Ritter    Franz Baumann   Felix Lützkendorf   Karl Ritter   
| narrator = Paul Kemp   Paul Dahlke
| music = Alois Melichar   
| cinematography = Günther Anders (cinematographer)|Günther Anders  
| editing =  Gottfried Ritter       UFA
| distributor = UFA
| released = 11 August 1938  
| runtime = 105 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical comedy Karl Ritter Paul Kemp. The film is set in 18th century France, where a young woman enjoys a series of romantic adventures. The director, Ritter, was attempting to recreate the style of a Rene Clair comedy.  The film was criticised by both Joseph Goebbels and Adolf Hitler.  

== Selected cast ==
* Lilian Harvey as Madelone aka Don Juan
* Viktor Staal as Fernand de Villeneuve  Paul Kemp as Henri de Grau
* Paul Dahlke as Cesaire 
* Anton Imkamp as General dEstroux 
* Aribert Wäscher as Barberousse 
* Käte Kühl as Gräfin Mallefougasse 
* Margot Höpfner as Eve Mallefougasse 
* Hedi Höpfner as Anais Mallefougasse

== References ==
 

== Bibliography ==
* Ascheid, Antje. Hitlers Heroines: Stardom & Womanhood In Nazi Cinema. Temple University Press, 2010.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 