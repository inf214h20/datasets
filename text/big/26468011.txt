Pakkinti Ammayi
{{Infobox film
|name=Pakkinti Ammayi
|image=
|image_size=
|caption=
|director=Chittajallu Pullayya
|producer=Sushil Kumar Havaldar
|story=Arun Choudhary
|screenplay=Arudra and C.S.R. Rao
|writer= Muddu Krishna   (Dialogues) 
|narrator=
|starring=Anjali Devi Relangi Venkata Ramaiah A. M. Rajah Addola Narayana Rao C.S.R. Rao
|music=Aswathama
|cinematography=Biren De
|editing=
|studio=
|distributor=
|released=1953
|runtime=164 minutes
|country=India
|language=Telugu
|budget=
}}
 Bengali story Pasher Bari by Arun Chowdhury. The film featured Anjali Devi as the beautiful neighbour girl. Famous comedian Relangi Venkata Ramaiah acted as her lover Subbarayudu and veteran South Indian singer A. M. Rajah as his opponent.

==Plot==
Subbarayudu (Relangi) stays next to the house of Heroine Leela Devi (Anjali Devi). She madly likes dance and music very much. She appoints a music teacher Prem Kumar (Narayana Rao). She does not like the actions of Subbarayudu. But he likes her very much and even loves her. He takes the help of some friends and impresses that he knows music very well. She involves her teacher Prem Kumar against Subbarayudu. He takes the help of his friend (A.M.Rajah) and mimes as if singing good songs, while Rajah is singing from background. As a result heroine almost likes him. But after knowing that he is not the singer, she changes her mind. Subbarayudu acts as a suicidal act for her refusal. Heroine finally changes her mind and loves him.

==Credits==
===Cast===
* Relangi Venkataramaiah as  Subbarayudu
* Anjali Devi as  Leela Devi
* Addala Narayana Rao as Prem Kumar
* A. M. Rajah as Raja
* Kamaladevi
* Mohana Krishna
* Gangaratnam
* Shakuntala
* C. S. R. Rao    as Gas Lodge leader
* V. V. Tatachari    as friend of Subbarayudu
* R. K. Rao     as friend of Subbarayudu

===Crew===
* Director: Chittajallu Pullayya
* Producer: Sushil Kumar Havaldar
* Story: Arun Chowdary
* Screen adaptation: Arudra and C. S. R. Rao
* Dialogues: Muddukrishna
* Original Music: Aswathama (debut)
* Cinematography: Biren De
* Playback singers: A. M. Rajah

==Songs==
* "Kalayemo Idi Naa Jeevita Phalamemo" (Singer: A. M. Rajah)

==1952 film== Bengali film Pasher Bari (1952) directed by Sudhir Mukherjee and starring Bhanu Bannerjee. 

==1981 film== Chandra Mohan, S. P. Balasubrahmanyam and K. Chakravarthi.

==References==
 
 
* Naati 101 Chitralu (Telugu hit films released between 1931–1965), S. V. Rama Rao, Kinnera Publications, Hyderabad, 2006, pages: 86-7.
 

==External links==
*  

 
 
 
 
 

 