Summer Holiday (1948 film)
{{Infobox film
| name           = Summer Holiday
| image          = Summer Holiday (1948 film) poster.JPG
| image size     = 154px
| caption        = Australian theatrical release poster
| director       = Rouben Mamoulian
| producer       = Arthur Freed
| writer         = Eugene ONeill (play) Frances Goodrich Albert Hackett Irving Brecher Jean Holloway
| starring       = Mickey Rooney Gloria DeHaven Agnes Moorehead
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 92-93 minutes
| country        = United States
| language       = English
| budget         = $2,237,000  . 
| gross          = $1,609,000 
}} under that name by MGM in 1935 with Rooney in a much smaller role. Though completed in October 1946, the film sat on the shelf until 1948. 

In addition to Walter Huston, the supporting cast features Frank Morgan as the drunken Uncle Sid, portrayed earlier by Wallace Beery on screen and later by Jackie Gleason on Take Me Along|Broadway, as well as Marilyn Maxwell, Agnes Moorehead, Selena Royle and Anne Francis.

==Cast==
*Mickey Rooney as Richard Miller
*Gloria DeHaven as Muriel
*Walter Huston as Nat Miller
*Frank Morgan as Uncle Sid
*Jackie Butch Jenkins as Tommy
*Marilyn Maxwell as Belle
*Agnes Moorehead as Cousin Lily
*Selena Royle as Mrs. Miller
*Michael Kirby as Arthur Miller
*Shirley Johns as Mildred
*Hal Hackett as Wint
*Anne Francis as Elsie Rand John Alexander as Mr. McComber
*Virginia Brissac as Miss Hawley
*Howard Freeman as Mr. Peabody

==Reception==
The film was a disappointment at the box office, earning only $1,208,000 in the US and Canada and $401,000 elsewhere, resulting in a loss of $1,460,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 401 
==Notes==
 

==External links==
* 
*  
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 


 