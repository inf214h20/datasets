Akshayapaathram
{{Infobox film 
| name           = Akshayapaathram
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Sreekumaran Thampi
| writer         = Kedamangalam Sadanandan Sreekumaran Thampi (dialogues)
| screenplay     = MP Rajeevan Lakshmi
| music          = M. S. Viswanathan
| cinematography = JM Vijayan
| editing        = K Sankunni
| studio         = Bhavani Rajeswari
| distributor    = Bhavani Rajeswari
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Lakshmi in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Prem Nazir
*Kaviyoor Ponnamma
*Adoor Bhasi Lakshmi
*Sreelatha Namboothiri
*T. R. Omana
*K. P. Ummer

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kannante Chundathu || K. J. Yesudas, S Janaki || Sreekumaran Thampi || 
|-
| 2 || Madhuramulla Nombaram || Vani Jairam || Sreekumaran Thampi || 
|-
| 3 || Manassoru Thaamarapoyka || S Janaki, Chorus || Sreekumaran Thampi || 
|-
| 4 || Marannuvo Nee Hridayeshwari || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Priyamulla Chettan || P Susheela || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 