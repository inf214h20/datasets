Jack Ryan: Shadow Recruit
{{Infobox film
| name           = Jack Ryan: Shadow Recruit
| image = Jack Ryan Shadow Recruit poster.jpg
| caption        = Theatrical release poster
| director       = Kenneth Branagh
| producer       = {{Plainlist|
* Lorenzo di Bonaventura
* Mace Neufeld David Barron
* Mark Vahradian
 
}}
| screenplay     = {{Plainlist|
* Adam Cozad
* David Koepp
}}
| based on       =  
| starring       = {{Plainlist|
* Chris Pine
* Kevin Costner
* Kenneth Branagh
* Keira Knightley
}}
| music          = Patrick Doyle
| cinematography = Haris Zambarloukos Martin Walsh
| studio         = {{Plainlist|
* Skydance Productions di Bonaventura Productions Mace Neufeld Productions
* Buckaroo Entertainment
* Etalon Film
* Translux
}}
| distributor    = Paramount Pictures
| released       =   
| country        = United States 
| language       = English
| runtime        = 105 minutes 
| budget         = $60 million 
| gross          = $135.5 million    
}}
 action thriller thriller film, Jack Ryan Jack Ryan reboot that fourth actor to play Ryan, following Alec Baldwin, Harrison Ford, and Ben Affleck.
 di Bonaventura Mace Neufeld Productions, Buckaroo Entertainment, Etalon Film and Translux. Theatrically, it was distributed by Paramount Pictures and was released on January 17, 2014. In the home-video market, the film was released in Blu-ray disc format on June 10.

On February 4, 2014, the original motion picture soundtrack was released by the Varèse Sarabande music label in CD format. The soundtrack was composed and orchestrated by Patrick Doyle. For its theatrical run, the film had grossed over $130 million in revenue at the box office. Jack Ryan: Shadow Recruit was met with mixed critical reviews. The film is dedicated to Clancy, who died on October 1, 2013.

==Plot== Jack Ryan second lieutenant CIA who recruits him.
 stock brokerage, and as a covert CIA analyst, looking for suspect financial transactions that would indicate terrorist activity. When the Russian Federation loses a key vote before the United Nations, Ryan notices that the markets do not respond in the expected way. He discovers that trillions of dollars held by Russian organizations have disappeared. A large number of these funds are controlled by Viktor Cherevin (Branagh), a Russian tycoon.

Ryans employer conducts business with Cherevin, so when Ryan discovers certain accounts are inaccessible to him as auditor, he has reason to visit Moscow and investigate. On arrival he narrowly survives a murder attempt by an assassin (Anozie) posing as his bodyguard. Ryan sends an SOS and is surprised to learn his backup is Harper. During their debrief, Ryan explains how Cherevins shadow investments could make the United States vulnerable to complete financial collapse following a staged terrorist attack, which would also weaken global markets.

At his meeting with Cherevin the next day, he is told that the problem company and all of its assets have been sold, thus preventing Ryans audit. Meanwhile, Muller, now Ryans fiancée, concerned he may be having an affair, secretly flies to Moscow to meet him. Against protocol for unmarried couples, Ryan reveals that he works for the CIA, to her great relief. Improvising on the situation, Harper has Muller agree to be included in a plan to infiltrate Cherevins offices. Ryan and Muller meet Cherevin at an upscale restaurant across the street from Cherevins office. Over dinner, Ryan causes a scene and purposely insults Muller. Excusing himself, he gains access to Cherevins office where he downloads critical corruption files.

Ryan and the CIA discover Cherevin used a falsified death certificate to place his son, Aleksandr (Utgoff), in the United States as a sleeper agent. Ryan uses his talent for pattern recognition to locate Aleksandrs hideout and intended target, Wall Street. He locates a fake police response vehicle driven by Aleksandr and gives chase. After Ryan catches up and engages in a physical confrontation with Aleksandr, he discovers the bomb in the rear of the police decoy. Unable to defuse it, he hijacks the vehicle with Aleksandr in it. He later crashes it into the East River while simultaneously jumping out, as the bomb detonates, killing Aleksandr. Cherevin is executed by his co-conspirators in Russia. Ryan and Harper are called to the White House to brief the president.

==Cast==
  Jack Ryan
*Keira Knightley as Cathy Muller   
*Kevin Costner as Thomas Harper
*Kenneth Branagh as Viktor Cherevin
*Alec Utgoff as Aleksandr Borovsky
*Nonso Anozie  as Embee Deng  The Sum of All Fears, as a supporting character, Olson. 
*Gemma Chan    as Amy Chang 
*David Paymer as Dixon Lewis 
*Karen David as FBI Lead Agent 
*Mikhail Baryshnikov (uncredited) as Interior Minister Sorokin 

==Production==

===Development=== The Sum Jack Ryan film series, but nothing came to fruition.  In 2008, the company engaged director Sam Raimi to spearhead a revival of the series,  but he later dropped out due to his commitment to the Spider-Man in film#Sam Raimi series|Spider-Man film series. 
 Jack Ryan Clear and Present Danger (1994), was engaged to perform rewrites as well.  However, Zaillian withdrew after a few weeks and the film was put on hold by Paramount in order for Pine to reprise his role as James T. Kirk in Star Trek Into Darkness.  During this time, David Koepp was brought on to rewrite the script,  with shooting scheduled to begin in the second half of 2012. 

The film had an additional setback in March 2012, when director Bender dropped out due to scheduling conflicts.  Paramount and Skydance Pictures quickly hired Kenneth Branagh to replace Bender, in order to start production after Pine was finished with Into Darkness around September 2012.  "This script arrived and was un-put-down-able and I knew the previous films, Id read some of the books and, simple as that, it came out of the blue", said Branagh. "I was going to be making another movie, but it went away and this one came to me and I read it and responded very strongly and its the kind of the film that I go to see". 

===Pre-production===
  in Liverpool, United Kingdom.]]
Pre-production for Jack Ryan: Shadow Recruit took place at the production offices of August Street Films Limited, based at Pinewood Studios in Buckinghamshire.    Months after signing on as director, Kenneth Branagh also made a deal with the studio to star as the films villain. 

In early August 2012, Keira Knightley, Felicity Jones, and Evangeline Lilly were being considered for the female lead while Kate Beckinsale and Jessica Biel were approached but declined.  Days later, Knightley won the role.  That same month, Kevin Costner signed on to play Thomas Harper in a two-picture deal that will also see him play the character in a film adaptation of Clancys Without Remorse. 

===Production===
While images from the films production in  , which doubled as Moscow.   Filming in New York City had been completed earlier that month.  Other shooting locales included London and Moscow.  Later, reshoots were also done in New York City. 

===Music===
The musical score of Jack Ryan: Shadow Recruit was composed by Patrick Doyle, who has collaborated on many of Kenneth Branaghs directorial efforts.  Writing the score took Doyle up to seven months, which is more than usual.  "In Jack Ryan,   wanted a piece of music for this scene and I had the theme the following day for it," said Doyle on his relationship with Branagh. "I actually wrote the main theme of the movie, just sitting there watching it. This is the expectation of a person you’re very close to. There’s no pressure in that you’re so relaxed, especially someone like Kenneth. He brings out the best in you because he creates such a casual, relaxed, but hard-working environment at the same time."  A soundtrack album was released digitally by Varèse Sarabande on January 14, 2014 and in physical formats on February 4.  

{{Infobox album
| Name = Jack Ryan: Shadow Recruit – Music From the Motion Picture
| Type = Film score
| Artist = Patrick Doyle
| Cover = 
| Released =    (Digital)     (Physical) 
| Length = 73:12
| Label = Varèse Sarabande
| Reviews = Jack Ryan soundtrack The Sum of All Fears (2002)
| This album = Jack Ryan: Shadow Recruit (2014)
| Next album = 
| Misc       = {{Extra chronology
| Artist     = Patrick Doyle
| Type       = Soundtrack
| Last album = Brave (soundtrack)|Brave (2012)
| This album = Jack Ryan: Shadow Recruit (2014)
| Next album = Cinderella (2015 film)#Music|Cinderella (2015)
}}
}}

{{Track listing
| collapsed       = no
| headline        = Jack Ryan: Shadow Recruit – Music From the Motion Picture
| total_length    = 73:12
| title1          = Flying Over Afghanistan
| length1         = 2:43
| title2          = The United Nations
| length2         = 2:42
| title3          = Shadow Accounts
| length3         = 2:54
| title4          = The Window Reflection
| length4         = 1:51
| title5          = Rooftop Call
| length5         = 1:52
| title6          = Second Great Depression
| length6         = 3:19
| title7          = Faith Of Our Fathers
| length7         = 4:00
| title8          = Cherevin Meets Ryan
| length8         = 2:11
| title9          = Plan In a Van
| length9         = 1:51
| title10         = The Activation
| length10        = 2:19
| title11         = Aleksandr
| length11        = 1:54
| title12         = The Engagement
| length12        = 2:24
| title13         = Stealing The Data
| length13        = 7:59
| title14         = Get Out
| length14        = 4:19
| title15         = Moscow Car Chase
| length15        = 4:15
| title16         = The Lightbulb
| length16        = 4:37
| title17         = Unravelling The Data
| length17        = 4:37
| title18         = CIA Recruitment
| length18        = 1:41
| title19         = Chopper To NYC
| length19        = 1:40
| title20         = Bike Chase
| length20        = 3:47
| title21         = Jack And Aleksandr
| length21        = 3:15
| title22         = Picking This Life
| length22        = 1:04
| title23         = Ryan, Mr. President
| length23        = 3:28
| title24         = Shadow Recruit
| length24        = 2:30
}}

==Release== trailer for the film was released the same day.  In December 2014, Pine confirmed that there would not be a sequel to the film due to its lackluster box office performance. 

The film was later released in Video on Demand format for home media markets on May 20, 2014.  It was released on DVD and Blu-ray Disc|Blu-ray formats by Paramount Home Entertainment on June 10, 2014. 

==Reception==

===Box office===
Jack Ryan: Shadow Recruit earned $5.4 million on its opening day in the United States and Canada,  and reached $17.2 million by the end of its opening weekend.  The film opened below all previous Jack Ryan films despite a wider theatre count.  Based on exit-polling service CinemaScore, more than a third of the films opening weekend audience was over the age of 50. With only 15 percent of viewers under the age of 25, the film failed to attract younger viewers despite the casting of Chris Pine.     The film is currently the twenty-fourth highest grossing of 2014 and was the first film of the year to have reached over $100 million worldwide.  During its theatrical run, the film had grossed $50.6 million domestically and $85 million in foreign business, for a worldwide total of $135.6 million. 

===Critical response=== weighted average out of 100 to critics reviews, it holds a metascore of 57, based on 36 reviews, indicating "mixed or average reviews".  CinemaScore reported that audiences gave the film a "B" grade. 

{{quote box|quote=
The film is slow in getting started and once its underway its only intermittently involving. Its also occasionally far-fetched. Why is Jack the only person capable of facing the dangerous Cherevin? How does he overpower a hit man built like a refrigerator?|source=—Claudia Puig, writing for USA Today |align=left|width=40%|fontsize=85%|bgcolor=#FFFFF0|quoted=2}} Bourne movies, it is   close enough." 

Peter Travers of Rolling Stone was less enthusiastic about the film, saying it "has no personality of its own." Travers added, "Its a product constructed out of spare parts and assembled with computerized precision. Its hard to care when Jack turns operational and becomes a CIA robocop. The movie feels untouched by human hands."  Todd McCarthy of The Hollywood Reporter also gave the film a negative review, saying, "While   benefits from an attractive cast, the perennial allure of the spy game and the exoticism of the contemporary Moscow setting, the biggest problem afflicting this modest diversion is that its the sort of film in which computers get to the bottom of every problem that comes up in about five seconds." 

==See also==
 

==References==
;Footnotes
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 