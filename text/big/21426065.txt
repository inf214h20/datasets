Talpa (film)
{{Infobox film
| name           = Talpa
| image          =
| image size     =
| caption        =
| director       = Alfredo B. Crevenna
| producer       = Adolfo Lagos
| writer         = Edmundo Báez Juan Rulfo
| starring       = Lilia Prado
| music          =
| cinematography = Rosalío Solano
| editing        = Gloria Schoemann
| distributor    =
| released       = 20 December 1956
| runtime        = 99 minutes
| country        = Mexico
| language       = Spanish
| budget         =
}}

Talpa is a 1956 Mexican drama film directed by Alfredo B. Crevenna. It was entered into the 1956 Cannes Film Festival.   

==Cast==
* Lilia Prado - Juana
* Leonor Llausás - La presumida
* Víctor Manuel Mendoza - Tanilo Jaime Fernández - Esteban
* Hortensia Santoveña - La mère
* José Chávez (actor)|José Chávez
* Blanca Estela Limón
* Alicia Montoya
* José Muñoz (actor)|José Muñoz
* Aurora Walker
* Amado Zumaya

==References==
 

==External links==
* 

 
 
 
 
 
 
 