I Love You Rosa
{{Infobox film
| name           = I Love You Rosa
| image          = I_Love_You_Rosa_Poster.png
| caption        = 
| director       = Moshé Mizrahi
| producer       = Yoram Globus Menahem Golan Itzik Kol
| writer         = Moshé Mizrahi
| starring       = Zivi Avramson
| music          = 
| cinematography = Adam Greenberg
| editing        = Dov Hoenig
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
I Love You Rosa ( , Transliteration|translit.&nbsp;Ani Ohev Otach Rosa) is a 1972 Israeli film directed by Moshé Mizrahi. It was nominated for the Academy Award for Best Foreign Language Film.    It was also entered into the 1972 Cannes Film Festival.   

==Cast==
* Zivi Avramson - Esther
* Naomi Bachar - Luna
* Michal Bat-Adam - Rosa
* Yehuda Efroni - Don Yitzhak
* Levana Finkelstein - Jamila
* Esther Grotes - Alegra
* Gunther Hirschberg - narrator
* Avner Hizkiyahu - Rabbi
* Elisheva Michaeli - Regina
* Gabi Otterman - young Nissim
* Aliza Rosen - Rabbis wife
* Joseph Shiloach - Eli
* Moshe Tal - adult Nissim
* Sharit Yishai - Fortuna

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 