A Morte Comanda o Cangaço
 
{{Infobox film
| name           = A Morte Comanda o Cangaço
| image          = A Morte Comanda o Cangaço.jpg
| caption        = Film poster
| director       = Carlos Coimbra Walter Guimarães Motta
| producer       = Marcelo de Miranda Torres
| writer         = Carlos Coimbra Walter Guimarães Motta Francisco Pereira da Silva
| starring       = Alberto Ruschel Aurora Duarte Milton Ribeiro
| music          = Enrico Simonetti
| cinematography = Tony Rabatoni
| editing        = Carlos Coimbra
| distributor    = Cinedistri
| studio         = Aurora Duarte Produções Cinematográficas
| released       =  
| runtime        = 100 minutes
| country        = Brazil
| language       = Portuguese
}}
 Western action Best Foreign Language Film at the 33rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Alberto Ruschel as Raimundo Vieira
* Aurora Duarte as Florind
* Milton Ribeiro as Capitano Silvero
* Maria Augusta Costa Leite as Dona Cidinha
* Gilberto Marques as Coll. Nesinho
* Ruth de Souza
* Lyris Castellani
* Apolo Monteiro
* Edson França
* José Mercaldi
* Leo de Avelar
* Jean Lafront

==See also==
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 