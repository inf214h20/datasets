Little Presents
{{Infobox film
| name           = Little Presents
| image          = 
| caption        = 
| director       = Jack Witikka
| producer       = Risto Orko
| writer         = Aapeli
| starring       = Aapeli
| music          = 
| cinematography = Yrjö Aaltonen
| editing        = Armas Laurinen
| distributor    = 
| released       = 1 December 1961
| runtime        = 73 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Little Presents ( ) is a 1961 Finnish drama film directed by Jack Witikka. It was entered into the 12th Berlin International Film Festival.   

==Cast==
* Leif Aaltonen
* Aapeli - God (voice) (as Simo Puupponen)
* Kaarlo Halttunen - Clocksmith
* Eija Hämäläinen
* Pia Hattara - Vetterantin Torotea
* Hannes Häyrinen - Manager Palkeinen
* Raili Helander - Liisi
* Pentti Irjala - Caretaker Harakka
* Tea Ista - Vetterantin Klory
* Leo Jokela - Vennu Harakka
* Leevi Kuuranne - Jormalainen
* Irja Kuusla - Tattaris wife
* Heimo Lepistö - Tattari
* Eila Pehkonen - Hilma
* Pekka Pentti - Henry
* Nisse Rainne - Caretaker Kuikka
* Saara Ranin - Vetteranska
* Jouko Rikalainen - Osku
* Leo Riuttu - Friman Heikki Savolainen - Photographer Toivakka
* Irma Seikkula - Clocksmiths wife
* Eero Siljamäki - Immu
* Olavi Suhonen
* Tuukka Tanner - Pietari Jormalainen
* Elsa Turakainen - Karoliina
* Henny Valjus - Clocksmiths mother-in-law (as Henny Waljus)
* Reino Valkama - Cloth merchant

==References==
 

==External links==
* 

 
 
 
 
 
 
 