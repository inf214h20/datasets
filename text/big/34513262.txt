Killing Season (film)
 
{{Infobox film
| name           = Killing Season
| image          = Killing Season film poster.jpg
| caption        = Teaser poster
| producer       = Paul Breuls 
| director       = Mark Steven Johnson
| screenplay     = Evan Daugherty
| starring       = Robert De Niro John Travolta Milo Ventimiglia Elizabeth Olin
| music          = Christopher Young	 
| cinematography = Peter Menzies Jr.
| editing        = Sean Albertson
| studio         = Nu Image Millennium Films Corsan Pictures FilmEngine
| distributor    =
| released       =  
| runtime        = 91 minutes  
| budget         = 
| gross          = $39,881 (domestic) $1 million (international) 
| language       = English
| country        = United States
}}
 action thriller American and Serb war veteran. 

Daughertys script caught the attention of producers after winning the 2008 Script Pipeline  Screenwriting Competition.  The film received negative reviews from critics and was a box office bomb.

== Plot == Scorpions soldier Colonel Benjamin Ford (De Niro). Ford has fled to a cabin retreat somewhere in the Appalachian Mountains, to forget the war. Now a recluse, he meets Kovač, posing as a European tourist, during a hunting trip. The two men become friendly, until Kovač reveals his true identity. Intent on revenge, he initiates a gory game of cat-and-mouse with Ford. The latter is badly injured but is quick to rebound. After a showdown, Kovač is overpowered by Ford. They reach a peaceful compromise, however, after understanding each others predicament. Kovač quietly returns to Serbia, while Ford visits his son, to make up for missing his grandsons baptism.

==Cast==
* Robert De Niro as Benjamin Ford
* John Travolta as Emil Kovač
* Milo Ventimiglia as Chris Ford
* Elizabeth Olin as Sarah Ford

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "Its thrilling, imaginative, unexpected and dominated by two extraordinary characters on a collision course"
|-
| style="text-align: left;" | — Paul Breuls, Corsans Chief Executive Officer, when asked about the film. 
|}
 Villa Rica.  International sales for Killing Season, offered by the American Film Market, commenced on November 2, 2011, in Santa Monica.   American cellist/singer/songwriter Ben Sollee contributed solo cello performances as well as an original song,  "Letting Go",  for the end credits.

==Release==

===Critical response=== Daily News awarded the film one out of five stars, panning Travoltas characters Serbian accent.  David DeWitt of The New York Times stated that " ts not worthless, but its not good. As a genre film, its too ambitious; as an art film, its too obvious." 

== See also ==
 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 