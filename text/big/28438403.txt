Good Intentions (film)
 
{{Infobox film
| name           = Good Intentions
| image          = Good Intentions.jpg
| image_size     = 
| caption        = 
| director       = Jim Issa
| producer       = Pamela Peacock Richard Sampson 
| writer         = Anthony Stephenson
| narrator       = 
| starring       = Elaine Hendrix Luke Perry Jon Gries LeAnn Rimes Jimmi Simpson
| music          = 
| cinematography = 
| editing        = 
| distributor    = Phase 4 Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Good Intentions is a 2010 comedy directed by Jim Issa.  It is about Etta Milford (Elaine Hendrix), a Georgian housewife who creates a scheme to get back money for her family.

==Storyline==
Etta Milford (Elaine Hendrix) is saddled with taking care of her rambunctious children in a poor Georgia town while her husband, Chester (Luke Perry), spends all their money on his makeshift inventions. Etta hatches a plan to get back money for her family. It includes holding up her own husbands liquor store, blackmailing the sheriff, and robbing the grocery store. 

==Cast==
*Elaine Hendrix as Etta Milford
*Luke Perry as Chester Milford
*Jon Gries as Sheriff Ernie
*Jimmi Simpson as Kyle
*LeAnn Rimes as Pam
*Gary Grubbs as Zachary
*Gregory Alan Williams as Buck
*Randy McDowell as Rob
*Jim Cody Williams as Bo
*Ted Manson as Mr. Simmons

==Filming==
The film was shot on location in Atlanta, Georgia, and Rutledge, Georgia.

==DVD release==
The DVD was released by Phase 4 Films on March 9, 2010. 

==References==
 

==External links==
*  
*  

 
 
 

 