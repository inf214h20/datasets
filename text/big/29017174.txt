The Crimson Circle (1936 film)
{{Infobox film
| name =  The Crimson Circle 
| image = "The_Crimson_Circle"_(1936).jpg
| image_size =
| caption =
| director = Reginald Denham Richard Wainwright
| writer = Edgar Wallace (novel)   Howard Irving Young
| starring = Hugh Wakefield   Alfred Drayton   Niall MacGinnis   June Duprez
| music = 
| cinematography = Philip Tannura
| editing = 
| studio = Richard Wainwright Productions
| distributor = Universal Pictures 
| released = 1936
| runtime = 76 minutes
| country = United Kingdom English 
| budget =
| gross =
}} The Crimson independent producer Richard Wainwright Shepperton and Welwyn Studios. 

==Plot==
Detectives at Scotland Yard try to track down The Crimson Circle, a secret society of blackmailers.

==Earlier versions== silent version The Crimson DeForest Phonofilm The Clue of the New Pin.

==Cast==
* Hugh Wakefield as Derek Yale
* Alfred Drayton as Insp. Parr
* Niall MacGinnis as Jack Beardmore
* June Duprez as Sylvia Hammond Paul Blake as Sgt. Webster
* Noah Beery as Felix Marl
* Basil Gill as James Beardmore Gordon McLeod as Brabazon
* Renee Gadd as Millie Macroy
* Ralph Truman as Lawrence Fuller
* Robert Rendel as Commissioner
* William Hartnell as Minor role

==Critical reception==
The New York Times wrote, "after the first five minutes or so of the Globes current thriller from England, it may occur to you that the title, The Crimson Circle, is a matter of slight understatement. Please remember, then, that this is an Inspector Parr story, and that British producers do not presume to change Edgar Wallace titles, no matter how much more fitting something like The Gory Horde may seem. Anyway, after the first five minutes you will become reconciled to this omnibus of omicide, remembering, if you know your Edgar Wallace, that a dozen murders is about Parr for the course."  {{cite web|url=http://www.nytimes.com/movie/review?res=9501EEDA1F39EE3BBC4051DFB467838D629EDE|title=Movie Review -
  The Crimson Circle -  The Crimson Circle, Adapted From Edgar Wallaces Story, Opens at Globe -- Two New Foreign Films - NYTimes.com|work=nytimes.com}} 

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 