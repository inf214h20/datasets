Track 143
{{Infobox film  name = Track 143 image = File:Track-143.png imagesize = alt =  caption = festival poster of the film director = Narges Abyar producer = Abuzar Purmohammadi Mohammad-Hossein Qasemi writer = Narges Abyar starring = Merila Zarei music = cinematography = editing = released =   runtime = 92 minutes country = Iran language = Persian
}} 2014 Iranian drama War war film operation Valfajr Crystal Phoenix award for Audience Favorite Film.   

== Title ==
Track 143 is refer to one of the war tracks that located at chazzabbe (Persian language|Persian:چذابه) region and dead body of killed soldier is found at that place. 

  ]] -->

== Plot == operation Valfajr failed, they received news about Yonoss friend. Olfat is waiting for her son too. As she finds out that the Iraqi radio announces the Iranian captives names, she ties a radio on her back and carries it everywhere.
After fifteen year, a person calls her and say "My brother and your son are in the same prison in Iraq". Olfat gets happy and cleans the house, decorated the garden and invites some guests but the captive isnt her missing son. That is a mistake and just the first name is the same. After that adventure, she lives as a recluse, but is waiting and hopeful for the return of her son. Eventually the dead body of Yonos is found and his bones are given to Olfat. Iranians call such people martyrs and respect them and their family. 

== Cast ==
* Merila Zarei as mother (Olfat)
* Hesam Bigdelooa as olfats son
* Gelareh Abbasi as sister
* Mehran Ahmady as friend
* Javad Ezzati as a person who find soldiers body
* Thirty native actors and actresses

==Production==

===Background===
This film is a screen adaptation of Abyar’s novel titled The Third Eye with emphasis on the considerable role of women in Iran-Iraq war (1980-1988). Abyar selected one of the novels women and wrote the script. Her goal is to show the difficulties that women, particularly mothers have encountered during the war. The worst was waiting for the return of their sons. Some of them, like Jafar Rezaeis mother ties a radio on her back for fifteen years. Finally this mother, whose name is Akhtar Niyaz poor, received the dead body of her son. 

===Filming=== moghovie village. This village has a mine and there is no need to go to anywhere far to film in the mine. They have used filming on the hand technique to make filming in the village easy. In this movie, the director imagines thirty years of olfats life story and she shows difference at the best. For exampele, in earlier years, there was not power in village but finally there is. Abyar said:  I colored clothes to look so old". 

== Critical reception ==

===Reviews===
  mentions: "In this film, the pure essence of women is seen. He believes that Irans cinema needs such movies". Akbbar Nabavi says : "Native view of the film is the considerable characteristic of it. This cinemas critic considers track 143 as an influential film". Shahab Moradi, an academy and seminary lecturer says: "Track 143 illustrates Iran_Iraq war, martyrdom and captivity in the best way and this isnt against the war".Narges Abyar states: "I will continue directing films about the war". 

===Awards===
Abyar’s film received both the Crystal Simorgh award for Audience Favorite Film at 2014 Fajr International Film Festival and the event’s Best Actress Award that was presented to the film’s leading role actress Merila Zarei. The movie premiered at Iran’s 2014 Fajr International Film Festival and earned rave reviews by critics and audience. track 143 is also to represent Iranian cinema at the 67th Cannes International Film Festival  and Koreas Pusan Film Festival in May and October. 

==In the news==
 drama War war ‘Track 143’ directed by woman filmmaker Narges Abyar has received a purchase request from North America. 

The recent news about Track 143 states that Only during 9 days,the amount from the sale of this film has been 200 thousand dollars. The film is screened in 65 cinemas a.round the Country. 

==Exteranal link==
* 

==References==
 

 
 
 
 
 
 
 