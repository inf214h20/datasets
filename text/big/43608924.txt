Self Made (film)
{{Infobox film
| name           = Self Made
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Shira Geffen
| producer       = David Mandil Moshe Edery Leon Edery
| writer         = Shira Geffen
| starring       = Sarah Adler Samira Saraya Doraid Liddawi
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = May 16, 2014
| runtime        = 
| country        = Israel
| language       = Hebrew, Arabic, French with English subtitles
| budget         = 
| gross          = 
}} Israeli film directed by Shira Geffen.

==Synopsis==
The film follows an Israeli artist (Michal) and Palestinian DIY store clerk (Nadine) who swap their lives because of a mix-up at a border security checkpoint. Jordan Hoffman,  ,  , May 16, 2014  Leo Barraclough,  ,  , August 19, 2014 

==Cast==
*Sarah Adler as Michal.
*Samira Saraya as Nadine.
*Doraid Liddawi
*Naama Shoham
*Ziyad Bakri

==Critical reception==
The film was presented at the Cannes Film Festival and at the Jerusalem Film Festival.  It was also shown at the opening night of the AICE Israeli Film Festival in Melbourne, Australia. 

In a review for The Hollywood Reporter, critic Clarence Tsui said the film was full of "black humor" and "surreal episodes to talk about gritty issues in reality."  She added, "What Geffen is trying to put across is how an individuals appearance is just a façade."  She concluded that it was, "an entertaining and pensive thought about the straitjackets imposed on women or maybe just about everyone else too." 

==References==
 

 
 
 

 