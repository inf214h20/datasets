The Emperor Jones (1933 film)
 
{{Infobox film
| name           = The Emperor Jones
| image          = The Emperor Jones (1933 film).jpg
| director       = Dudley Murphy
| producer       = Gifford Cochran John Krimsky
| writer         = Eugene ONeill (play) DuBose Heyward (screenplay) Dudley Digges Frank H. Wilson Fredi Washington Ruby Elzy
| music          = Rosamond Johnson Frank Tours
| cinematography = Ernest Haller
| editing        = Grant Whytock
| distributor    = United Artists
| released       =  
| runtime        = 72 minutes  76 minutes (restored)  80 minutes (original)
| country        = United States
| language       = English
| budget         = $263,000   
}} play of Dudley Digges, Frank H. Wilson, and Fredi Washington. The screenplay was written by DuBose Heyward and filmed at Kaufman Astoria Studios with the beach scene shot at Long Beach, New York. Robeson starred in the ONeill play on stage, both in the United States and England, a role that had helped launch his career.

==Background==
The film is based rather loosely on Oneils play, but adds an entire backstory before ONeills actual play begins, and includes several new characters that do not appear in it (such as Jones wife, and a friendly priest who advises him to give up his evil ways). Some people consider the movie to be just a vehicle for showing off Robesons musical talent (he sings a number of times in the film). However, the film does provide what may be Robesons greatest dramatic performance in a movie, considered by many to be worthy of an Oscar nomination that it did not receive.

In the film version, the opening shots are of an African ritual dance. Some critics are quick to assess the opening as representative of the "primitive" black world to which Brutus Jones will eventually revert. However, more scholarly reviews of the film understand the complexities of the allusion to and comparison between the roots of the African-American church and the rhythmic chanting often seen in African religious practices. A quick dissolve takes us into a Baptist church in the American South, where the dancing of the congregation presents an image that argues for a continuity between the "savage" Africans and the ring-shout Baptists. Such suggestive editing may be the kind of element that causes viewers to suspect the film of racism.

Similarly, the film makes copious use of the word "nigger", as did The Emperor Jones|ONeills original play. African Americans criticized ONeills language at the time, so its preservation and expansion in the film present another cause for critique. Given Robesons subsequent career as a Civil Rights activist, the spectacle of his character using the term so frequently in regard to other blacks seems shocking today.

==Plot== crap game leads to a fight in which he inadvertently stabs Jeff, the man who had introduced him to the fast-life and from whom he had stolen the affections of the beautiful Undine (played by Fredi Washington).

Jones was imprisoned and sent to do hard labor. (A stint on the chain gang allows the film its first opportunity to show Robeson without his shirt on, an exposure of male nudity unusual for 1933 and certainly for a black actor. Here and later the director plays on Robesons sexual power and, implicitly, on cultural stereotypes about the libidinal power of black men.) Jones escapes the convicts life after striking a white guard who was torturing and beating another prisoner. Making his way home, he briefly receives the assistance of his girlfriend Dolly before taking a job stoking coal on a steamer headed for the Caribbean. One day, he catches sight of a remote island and jumps ship, swimming to the island.

The island is under the crude rule of a top-hatted black despot who receives merchandise from Smithers, the dilapidated white colonial merchant who is the sole Caucasian on the island. Jones rises to become Smithers partner and eventually "Emperor". He dethrones his predecessor with a trick that allows him to survive what appears to be a fusillade of bullets, creating the myth that he can only be slain by a silver one. Joness rule of the island involves increasing taxes on the poor natives and pocketing the proceeds.

The highlight is a twelve minute spoken monologue taken directly from The Emperor Jones|ONeills play, in which Brutus Jones (Robeson), hunted by natives in revolt, flees through the jungle and slowly disintegrates psychologically, becoming a shrieking hysteric who runs right into the path of his pursuers.

==Cast==
* Paul Robeson – Brutus Jones Dudley Digges – Smithers
* Frank H. Wilson – Jeff
* Fredi Washington – Undine
* Ruby Elzy – Dolly
* George Haymid Stamper – Lem Jackie "Moms" Mabley – Marcella
* Blueboy OConnor – Treasurer
* Brandon Evans – Carrington Rex Ingram – Court Crier
* Marietta Canty

==Production==
The film was originally meant to be shot on location in Haiti but Robeson wanted to work in the studio. 

==Awards==
In 1999, the film was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress, and selected for preservation in the National Film Registry.

==DVD release==
The film is in the public domain now, and can be purchased at many online outlets. A newly remastered version (with commentary and extras) was released on DVD by the Criterion Collection in 2006.

==References==
 
*  

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 