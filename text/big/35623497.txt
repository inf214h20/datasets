Una Noche (film)
{{Infobox film
| name           = Una Noche
| image          = Una_Noche_poster.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Lucy Mulloy
| producer       = Daniel Mulloy Lucy Mulloy Yunior Santiago Maite Artieda Sandy Perez Aguila 
| writer         = Lucy Mulloy
| starring       = Dariel Arrechada Anailín de la Rúa de la Torre Javier Nuñez Florián
| music          = 
| cinematography = Shlomo Godder Trevor Forrest
| editing        = Cindy Lee
| studio         = Una Noche Films
| distributor    = 
| released       =  
| runtime        = 89 minutes  
| country        = Cuba/ UK/ USA
| language       = Spanish English
}}
Una Noche (One Night) is a Cuban-set drama-thriller film written and directed by Lucy Mulloy.

==Premise==
Trapped in the nervous desperation of Havana, Raul dreams of escaping to Miami. When accused of assaulting a tourist, his only option is to flee. He begs his best friend, Elio, to abandon everything, including his family, and help him reach the forbidden land 90 miles across the ocean. Elio’s commitment is tested when he is torn between helping Raul escape and protecting his twin sister, Lila. Brimming with the nervous energy of Havana’s restless youth in the crumbling sun-bleached capital, Una Noche follows one sweltering day, full of hope and fraught with tension, that burns to a shocking climax. 

==Production==
Mulloy spent years in Havana researching for her first feature Una Noche. Whilst in Cuba Mulloys story developed as she searched for young untrained talent to take the lead roles.

==Reception==
Una Noche premiered at the 2012 Berlin International Film Festival and 2012 Tribeca Film Festival to international critical acclaim  and "earning rave reviews".  "A pulsing debut feature has an undercurrent of ribald comedy that doesnt entirely prepare the viewer for the harrowing turn it takes."  Una Noche shot to international media attention, ahead of its US premiere when two of the films lead actors, Javier Nuñez Florián and Anailín de la Rúa de la Torre, on their way to present the film at its US premier in Tribeca Film Festival, disappeared, reportedly defecting to the US.         In a highly publicized twist Javier Nuñez Florián and his co-star Dariel Arrechada went on to win the Best Actor Award with Nuñez remaining in hiding during the ensuing media frenzy.   "Una Noche cleaned up at Tribeca Film Festival in juried awards, taking home best actor, cinematography, and new director honors in the Narrative competition." 

==Accolades== Gotham Award Euphoria Calvin Klein Spotlight on Women Filmmakers’ ‘Live The Dream’ post production grant.  Una Noches world premiere was at the 2012 Berlin International Film Festival in the Generation Competition.

In the United States, Una Noche premiered at the Tribeca Film Festival and went on to win:
*Tribeca Film Festival Award for Best Director of a Feature Film - Lucy Mulloy
*Tribeca Film Festival Award for Best Actor in a Feature Film - shared between Javier Nuñez Florian and his co-star Dariel Arrechada 
*Tribeca Film Festival Award for Best Cinematography of a Feature Film - shared between Shlomo Godder and Trevor Forrest
*Gotham Award Spotlight Award For Women Filmmakers Una Noche
*Berlin Film Festival Nominated for Crystal Bear - Lucy Mulloy
*Berlin Film Festival Nominated for Cinema Fairbindet Prize 
*Berlin Film Festival Finalist for Teddy Award
*Brasilia International Film Festival Best Script Una Noche
*Deauville American Film Festival Grand Jury Prize Una Noche
*Athens International Film Festival Best Script Una Noche
*Fort Lauderdale International Film Festival Best Foreign Film shared between Lucy Mulloy, Sandy Pérez Aguila, Maite Artieda, Daniel Mulloy, Yunior Santiago Una Noche
*Fort Lauderdale International Film Festival Best Director Una Noche 
*Stockholm International Film Festival Telia Award Una Noche 
*International Film Festival of India Special Jury Prize Una Noche
*Oaxaca Film Festval Best Actor, Javier Nuñez Florian Una Noche
*Independent Spirit Awards Best First Feature Nomination, shared between Lucy Mulloy, Sandy Pérez Aguila, Maite Artieda, Daniel Mulloy, Yunior Santiago Una Noche
*Independent Spirit Awards Best Editor Nomination, Cindy Lee Una Noche

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 