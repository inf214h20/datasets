Black Comedy (film)
 
 
 {{Infobox film
| name           = Black Comedy
| image          = BlackComedyfilm.jpg
| alt            = 
| caption        = Hong Kong poster
| film name      = {{Film name
| traditional    = 黑色喜劇
| simplified     = 黑色喜剧
| pinyin         = Hēi Sè Xǐ Jù
| jyutping       = Hak1 Sik1 Hei2 Kek6}}
| director       = Wilson Chin
| producer       = Wong Jing
| writer         = Wong Jing
| starring       = Chapman To Wong Cho-lam Kimmy Tong
| music          = Ronald Ng Chu Chun Kit
| cinematography = Puccini Yu
| editing        = Matthew Hu
| studio         = Mega-Vision Pictures
| distributor    = Mega-Vision Pictures Gala Film Distributions
| released       =  
| runtime        = 90 minutes 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$2,445,308 
}} 2014 Cinema Hong Kong fantasy comedy film directed by Wilson Chin starring Chapman To, Wong Cho-lam, and Kimmy Tong.

==Plot==
Johnny Du Kei-Fung (Wong Cho-Lam) is a talented Hong Kong detective who dreams of becoming a member of the exclusive G4 Team that protects the citys Chief Executive (Patrick Dunn). Johnny is deemed too short for the team and his application is denied. At home, Johnny is bullied by his girlfriend Angel (Kimmy Tong), for being weak and unsuccessful.

Johnny meets a man named Vincent (Chapman To), who offers him three wishes in exchange for his soul. 
His first wish is to be pursued by a beautiful woman, whom Vincent delivers in the form of "Juicy" (also played by Kimmy Tong). Johnny soon discovers that Juicy is indiscriminately passionate, which makes him concerned about possible infidelity. In response, Johnny uses his second wish to turn Juicy into the innocent and unfailingly loyal QQ. He soon discovers, that QQ is unintelligent and is the daughter of triad figure Brother Drill (Tommy Wong), a coincidence that leads the police to believe he is a triad mole.

In order to change his superiors (Benz Hui) view of him, Johnny ventures into a drug lords lair hoping to solve a case, but he is killed. Vincent uses Johnnys third wish to reincarnate him as a wealthy, arrogant businessman in order for Johnny to complete his quest, win back Angel and protect the Chief Executive.

==Cast==
*Chapman To as Vincent, the Devil Prince
*Wong Cho-lam as Officer Johnny To
*Kimmy Tong as Angel / Juicy / QQ
*Jim Chim as Jim, the Angel
*Wilson Chin as Aprils lover
*Shirley Yeung as April
*Benz Hui as Johnnys superior
*Siu Yam-yam as The Mother of Hell
*Bob Lam as killer
*Evergreen Mak Cheung-ching as police
*Oscar Leung
*Patrick Dunn as Chief Executive of Hong Kong
*Alvina Kong
*Kingdom Yuen
*Ken Lo
*Yuen Cheung-yan
*Tommy Wong as Brother Drill
*Jerry Koo
*Jacqueline Chong
*Gill Mohindepaul Singh
*Lo Fan
*Gregory Wong as Ben
*Vanko Wong
*Tina Shek
*Calinda Chan
*Coffee Lam
*Lo Chung Yan
*Connie Man
*Doris Chow
*Ng Wan Po as policeman
*Fred Cheung as Cheung Chi Fung
*Hazel Tong

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 