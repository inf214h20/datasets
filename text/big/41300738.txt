Dakshina (film)
 
 
 
{{Infobox film
| name           = Dakshina
| director       = Tulsi Ghimire
| studio         = Ajambari Films Pvt. Ltd.
| writer         = Tulsi Ghimire
| starring       = Tulsi Ghimire Niruta Singh Bhuwan K.C. Bharati Ghimire Pukar Gurung Shrawan Ghimire
| music          = Ranjit Gazmer
| editing        = Tulsi Ghimire
| released       =  
| country        = Nepal
| language       = Nepali
}}

Dakshina ( ) is a 1994 film directed by Tulsi Ghimire who made this movie as a gift (dakshina) to his mentor (guru) Kamlakar Karkhanis. He had promised his mentor to make 10 Nepali films. Dakshina became one of the biggest hits of Tulsi Ghimires films.

==Cast==
* Tulsi Ghimire as Hari Sir
* Bharati Ghimire as Bhawana
* Bhuwan K.C. as Abhishek
* Pukar Gurung as Akash
* Niruta Singh as Suman

==Plot==
The film starts with Hari Sir, a professor at T.U., whose teaching and philosophy is followed by the students. Bhawana is wife of Hari sir and often denoted as Guru Aama. Prabhakar is a leader who wants to become a leader at any cost. Prabhakar persuades Pratap (one of the students) to join his party. He wants Pratap to become the leader of the campus and persuade other students to follow his Adarsha Party. Prabhakar starts finding Hari sir as his obstacle to become minister. Later, Hari Sir becomes a victim of his conspiracy and gets demoted as a primary teacher in a rural village. He decides to go there with his family (Bhawana and little Akash) and start a new life. When he reaches there he witnesses a drunk man is the teacher of the school. He starts the school which has been shut for years.

Soon after, their kid goes to college. Akash has big dreams and wants to succeed in life in no time. This leads him to steal Rs. 60, 000 which had been saved by Hari Sir and Guru Aama to establish the primary school to secondary school. Narendra who is college friend of Akash helps Prabhakar to indulge Akash in illegal activities. But, Akash is unaware of all these and gets arrested in a sting operation by KCPD (Kathmandu City Police Department). Listening to this bad news, Guru Aama has a heart attack.

Hari Sir tries to get Akash out of jail, but doesnt succeed. Pratap who conspired with Prabhakar to rusticate Hari Sir is now a DSP. He  inquires about the person who was there earlier. He finds out about Hari Sir and reason for being there. On the other hand, Guru aama is eagerly waiting for Hari Sir and Akash. Guru aama gets another heart attack after she hears Hari sir could not bring Akash back. Abhishek promises Hari sir and Guru aama to bring him back. Later,they find Guru aama dead on Hari sirs lap.

On Guru aamas death ceremony, Hari sir gets to know that Akash works for Prabhakar. He gets emotional and does not allow Akash to burn his mothers body which is a Hindu ritual. Later, Akash escapes from the cops while getting back to police station. Abhishek finds Akash and learns about his innocence. Later, DSP Pratap finds Akash, Abhishek and Narendra in the road on their way to arrest Prabhakar for his deeds.

Narendra goes to Prabhakar and warns to tell about his illegal activities to police. Prabhakar laughs and explains all the illegal activities he done to become a minister. Without his awareness, DSP had recorded the conversation. DSP Pratap tells him that finally gets evidence to arrest him which he had been looking since long. After much hiatus and fight, Prabhakar is alone and starts running in the street to save himself. He kills himself with DSP Prataps revolver to save himself from arrest and becoming infamous.

At the end, Hari Sir asks any of his student could pay "Dakshina" (gift) to him for his teachings. After much silence, two hands are raised attached to each other. Later, another two hands attached to each are raised. Hari Sir finds that it is Abhishek, Akash and DSP Pratap. He cannot control his happiness and dies of another heart attack. The movie ends with the slogan "Inaugural address of John F. Kennedy|Dont ask what your country has given to you, Ask what you have given to your country".


==Soundtrack==
{{Infobox album
| Name = Dakshina
| Type = soundtrack
| Artist = Ranjit Gazmer
| Released =  
| Genre = Film soundtrack
| Label = Music Nepal
}}
{| class="wikitable sortable"
|-
! Track !! Artists !! Length
|-
| Sapana Bhai Akahama Aauney || Sadhana Sargam ||
|-
| Euta Zero Nai Ho Hamro Zindagi ||  ||
|-
| Haami Nepali ||  ||
|-
| Yo Mutu Lai Kasari Samjhau || Narayan Gopal ||
|}

==External links==
 
* http://xnepali.net/movies/nepali-movie-dakshina/

 