Heaven's Seven
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Heavens Seven    7 ประจัญบาน
| director       = Chalerm Wong Pim
| writer         = Chalerm Wong Pim
| starring       =   Pongpot Wachirabunjon      Pongsak Pongsuwan        Thodsapol Siriwiwat      Amarin itipon     Pisek Intrakanchit     Cham Charum     Akom Preedakul
| distributor    = Thailand Sahamongkolfilm International
| released       =  
| runtime        = 110 minutes
| country        = Thailand
| language       = Thai
| gross          =  8.3 million baht
}}
Heavens Seven  ( Thai: 7 ประจัญบาน ) is a Thai action and  comedy film that was a remake of a television  movie. Chalerm Wong Pim, who is a Thai director, decided to remake. Heaven’s Seven was released on 26 April 2002. Heavens Seven movie is based on the third part of an original film that was shown on 22 January 1963 in Thailand.

==Plot==
The story of the movie starts when group of seven Thai soldiers finish their last risky mission. They retire from the military and go on to continue ordinary life with their families. However, bad memories still haunt them. Later, a merchant hires them to steal a load of gold from a truck caravan. Unfortunately, these belong to G.I.s of the U.S. Army. The U.S soldiers are angry about the theft and try to catch the seven soldiers by attacking the Thai soldier’s village. After the U.S. troops capture all the villagers, they make the villagers slaves. Meanwhile, the seven Thai fighters learn that the trucks do not contain any gold, but they contain chemical weapons that U.S forces intend use to destroy forests in Vietnam. Then, they go to the U.S. military camp to battle and successfully rescue their people to save their friends and relatives.

==Cast==
* Pongpat Wachirabunjong as Sgt.Dab Jampoh
* Pongsak Pongsuwan as Mhad Cherngmuay
* Thodsapol Siriwiwat as Tanguay Sae-lee
* Amarin itipon as Akkhi Mekyant
* Pisek Intrakanchit as Dhan Mahittha
* Cham Charum as Kla talumpook
* Akom Preedakul as Juk Biewsakul

==Reception==
This film is the one of Thai movies that earned the highest box office returns in Thailand. The film was successful and earned about 8.3 million baht. Later, Chalern Wong Pim created a sequel of this movie called Heavens Seven 2 (Thai: 7 ประจัญบาน ภาค 2 ) that was released in 28 April 2005.

==References==
 
* 
* 
* 

 


 