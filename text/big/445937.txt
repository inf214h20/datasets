Lost Horizon (1973 film)
{{Infobox film
| name           = Lost Horizon
| image          = Lost horizon ver1.jpg
| caption        = film poster by Howard Terpning
| director       = Charles Jarrott
| producer       = Ross Hunter
| writer         = Larry Kramer
| narrator       = George Kennedy Michael York  James Shigeta
| music          = Burt Bacharach  Hal David
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 150 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $3,800,000 (US/ Canada rentals) 
}} Michael York, Bobby Van, George Kennedy, Olivia Hussey, James Shigeta and Charles Boyer.
 film of James Hiltons novel Lost Horizon.

This was the final film produced by Ross Hunter.

==Synopsis==
This version is much closer to the 1937 film than to the original James Hilton novel. It tells the story of a group of travellers whose airplane is hijacked while fleeing a bloody revolution. The airplane crash lands in an unexplored area of the Himalayas, where the party is rescued and taken to the lamasery of Shangri-La. Miraculously, Shangri-La, sheltered by mountains on all sides, is a temperate paradise amid the land of snows. Perfect health is the norm, and inhabitants live to very old age while maintaining a youthful appearance.
 Bobby Van) is a third-rate comic and song and dance man who has a flair for working with the children of Shangri-La.
 Michael York). George has fallen in love with Maria (Olivia Hussey), a dancer, and wants to take her along when he leaves. Chang warns Richard that Maria came to Shangri-La over eighty years before, at the age of twenty. If she were to leave the valley, she would revert to her actual age.

Richard is summoned to meet the High Lama (Charles Boyer), who informs him that he was brought there for a reason, to succeed him as the leader of the community. However, on the night that the High Lama dies, George and Maria insist to Richard that everything the High Lama and Chang have said is a lie. They convince him to leave immediately.

Still in shock from the High Lamas death, Richard leaves without even saying goodbye to Catherine. Not long after their departure, Maria suddenly ages and dies, and George falls to his death down an icy ravine. Richard struggles on alone, ending up in a hospital bed in the Himalayan foothills. He runs away, back to the mountains, and miraculously finds the portal to Shangri-La once more.

==Critical reception==
Lost Horizon is considered one of the last in a string of box office musical failures which came in the wake of the success of    ... you can live indefinitely, lounging and puttering about for hundreds of years... the Orientals are kept in their places, and no blacks... are among the residents. Theres probably no way to rethink this material without throwing it all away.  
 John Simon remarked that it "must have arrived in garbage rather than in film cans." The songs were written by Burt Bacharach and Hal David, whose long partnership hit rocky ground within months of this films release. It was such a poor performer at the box office that the film was ridiculed as "Lost Investment." Bette Midler alluded to it as "Lost Her-Reason" and famously quipped, "I never miss a Liv Ullmann musical."  

The film was selected for inclusion in the book 50 Worst Films of All Time, co-written by critic Michael Medved. The film is listed in Golden Raspberry Award founder John Wilsons book The Official Razzie Movie Guide as one of The 100 Most Enjoyably Bad Movies Ever Made. 

However, star Peter Finch did say he enjoyed making the film. 

==Screenplay== Women in Love, the deal he engineered for this, combined with savvy investments, made it possible for him to live the rest of his creative life free of financial worries. In that sense, this film enabled Kramer to devote himself to the gay community activism and the writings (e.g., his ground-breaking AIDS play, The Normal Heart) which came later.

==Soundtrack==
In his 2013 autobiography, Burt Bacharach cites Lost Horizon as having come close to ending his musical career.  He stated that taken in isolation the songs worked, but failed in the context of the film. He stated that he was eventually banned from the dubbing studios at Todd-AO because of his increasing criticism of how the material was being handled.  Ultimately the experience caused a rift between Bacharach and lyricist Hal David, one which was never to be professionally reconciled.
 Bobby Van, and James Shigeta perform their own singing. George Kennedy was coached by Bacharach but was not used as a vocalist in the finished film. Olivia Hussey, Peter Finch and Liv Ullmann were dubbed by Andra Willis, Jerry Whitman, and Diana Lee respectively.
 bridge within the song. It was also a #32 hit for The 5th Dimension.

==Songs==
* "Lost Horizon" (sung by Shawn Phillips over the opening and closing credits)
* "Share The Joy" (Maria)
* "The World Is a Circle" (Catherine, Harry and children)
* "Living Together, Growing Together" (To Len and Company)
* "I Might Frighten Her Away" (Richard and Catherine)
* "The Things I Will Not Miss" (Sally and Maria)
* "If I Could Go Back" (Richard)
* "Where Knowledge Ends" (Faith Begins)" (Catherine)
* "Reflections" (Sally)
* "Question Me an Answer" (Harry and children)
* "I Come to You" (Richard)
 road show release. The dance sections of "Living Together, Growing Together" were cut, and the master negatives lost. "If I Could Go Back", "Where Knowledge Ends (Faith Begins)", and "I Come To You" were cut, but restored for the laserdisc release of the film. All of the songs appear on the soundtrack LP and CD. According to the notes on the laserdisc release, Kellerman and Kennedy had a reprise of "Living Together, Growing Together" that was also lost.

==DVD release==
On October 11, 2011, Columbia Classics, the manufacturing-on-demand unit of Sony Pictures Home Entertainment, released a fully restored version of the film on DVD in Region 1, which reinstated all of the elements cut after the roadshow release.  The DVD also contains supplemental features, including promos featuring producer Ross Hunter as well as the original song demos played and sung by composer Burt Bacharach.  Some of these demos contain different Hal David lyrics than the final versions utilized in the film.

On December 11, 2012, Screen Archives Entertainment (Twilight Time) released an exclusive Blu-ray Disc version of the film, with a 5.1 lossless soundtrack and an isolated film score.

==References==
 

==External links==
* 
*  information about the book, movie, and real life Shangri-Las.
*  Modern critical commentary on the film.

 
 
 

 
 
 
 
 
 
 
 
 
 
 