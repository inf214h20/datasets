Stopover Tokyo
{{Infobox Film
| name           = Stopover Tokyo
| image          = Stopover Tokyo Poster.jpg
| caption        = Theatrical poster
| director       = Richard L. Breen 
| producer       = Walter Reisch
| writer         = Richard L. Breen Walter Reisch based on = novel by John P. Marquand Ken Scott
| music          = Paul Sawtell
| cinematography = Charles G. Clarke
| editing        = Marjorie Fowler	
| distributor    = 20th Century Fox
| released       = December 1957
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = $1,055,000 
| gross          = $1,350,000 (US rentals) 
}} 1957 American Ken Scott. Filmed in Japan in CinemaScope, the film is set in Tokyo and follows a US counterintelligence agent foiling a communist assassination plot.

The film is based very loosely on final Mr. Moto novel by John P. Marquand. The biggest change is that Mr. Moto is entirely cut from the film.

It was the sole feature film directed by Richard Breen, a distinguished screenwriter.
==Plot==
US Intelligence Agent Mark Fannon (Robert Wagner) is sent to Tokyo on a routine courier mission but soon uncovers communist George Underwood’s (Edmond OBrien) plot to assassinate the American High Commissioner (Larry Keating).

Whilst there he meets Welsh receptionist (Joan Collins) who fellow agent Tony Barrett (Ken Scott) has the hots for. Though this causes initial animosity between the two they manage to race against the clock to foil the assassination.

==Cast==
 
*Robert Wagner as Mark Fannon
*Joan Collins as Tina Llewellyn
*Edmond OBrien as George Underwood Ken Scott as Tony Barrett
*Larry Keating as High Commissioner
*Sarah Shelby as High Commissioners wife
*Reiko Oyama as Koko
 
==Original Novel==
In the 1950s Marquand had not written a Moto novel in fifteen years. He received an offer to write one from Stuart Rose, the editor of the Saturday Evening Post, who offered Marquand $5,000 to travel to Japan and an advance of $75,000.   accessed 9 March 2015 

He decided to write a new one  because "I wanted to see whether or not I was still able to write a mystery, one of the most interesting forms of literary craftsmanship, if not art, that exists." Welcome, Mr. Moto By ANTHONY BOUCHER. New York Times (1923-Current file)   20 Jan 1957: 232.  

Marquand visited Japan for a month and wrote up the story towards the end of 1956. Mr Moto was not the actual hero of the novel - that role went to secret agent John Rhyce, who is sent to Tokyo to combat a communist plot along with fellow agent Ruth Bogart. John P. Marquands Mr. Moto Returns in a New Spellbinder
Butcher, Fanny. Chicago Daily Tribune (1923-1963)   20 Jan 1957: b1.  

The novel was serialised in the Post from 24 November 1956 to 12 January 1957 under the title "Rendezvous in Tokyo". The magazines editors did not like the storys unhappy ending but Marquand insisted upon it. The novel itself was published in early 1957.   Books and Authors: Marquand Thriller
New York Times (1923-Current file)   05 Dec 1956: 37.   It was a best seller and was, on the whole, well received, a writer on the New York Times calling it "superlative".  IN AND OUT OF BOOKS: Change Goodby Takes Winners
By HARVEY BREIT. New York Times (1923-Current file)   17 Feb 1957: BR5.   

The novel would later be re-issued under the titles Right You Are, Mr Moto and The Last Case of Mr Moto. 

==Production==
20th Century Fox, who made the original Peter Lorre Moto movies, bought the film rights to the story in April 1956, prior to publication. Sam Engel was originally going to produce and William Holden and Jennifer Jones were mentioned as possible stars. Sam Engel Seeks Holden, Jennifer Jones for New Film
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   13 Apr 1956: b8.   

The movie ended up being the first of a proposed series of movies from writers Richard Breen and Walter Reisch; Breen was to direct and Reisch to produce. Robert Stack was meant to play the lead but he refused the role because he did not want to go to Japan. CASTING TROUBLES BESET FOX FILMS: Dailey and Dana Wynter Give up Roles in Wald Pictures-- Robert Stack Suspended
By THOMAS M. PRYOR Special to The New York Times. New York Times (1923-Current file)   18 Apr 1957: 34.  Stack was suspended by the studio and the role given to Robert Wagner. FRED ZINNEMANN SIGNS MISS KERR: Director Acquires Star for Sundowners, His First Independent Film Effort Wagner Replaces Stack
By OSCAR GODBOUT Special to The New York Times.. New York Times (1923-Current file)   19 Apr 1957: 16.  

Cinematographer Charles G. Clarke made expansive use of location shooting in Kyoto, a sacred Shinto city which was only lightly bombed in World War II and taken off the nuclear bombing target list (from its original top listing) due to the efforts of Henry Stimson who argued for the preservation of its cultural assets.  

Actor Ken Scott was injured in a scene when Edmond OBrien shot a prop gun at him and a blank cartridge hit his face. There was no serious damage. Ex-Screen Star Alice Lake Jailed on Drunk Charge
Los Angeles Times (1923-Current File)   01 June 1957: 3. 

Fox were so impressed with ten-year-old star Reiko Oyamo they signed her to a long term contract. Japan Shirley Temple Contracted; Canadian Story Will Star More
Schallert, Edwin. Los Angeles Times (1923-Current File)   28 June 1957: B9 

==Reception==
Collins and Wagner promoted the film with a nationwide publicity tour. Stopover Tokyo Stars Going on Publicity Tours
Los Angeles Times (1923-Current File)   01 Nov 1957: B10.  

However it was not particularly successful at the box office.

The Chicago Daily Tribute praised the location photography but said the film "starts suspensefully, but ends limply". Film Keyed to Beauty of Japan: "STOPOVER: TOKYO"
TINEE, MAE. Chicago Daily Tribune (1923-1963)   08 Nov 1957: a5.   The Los Angeles Times liked the scenery which they thought "helps overcome somewhat routine plot development" but felt Wagner "goes about his spying work energetically although it is thought that this type of character isnt exactly his cup of tea." Stopover Tokyo Tale of Intrigue in Orient
Scott, John L. Los Angeles Times (1923-Current File)   08 Nov 1957: A7.  

Breen and Resich were later reported as working on another film for Wagner, The Far Alert, about NATO naval flyers. Looking at Hollywood: Film to Co-Star Murphy, Borgnine
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   30 Sep 1957: b14.   However this film was never made.

A year after the movie came out Marquand told the New York Times that:
 Mr Moto was my literary disgrace. I wrote about him to get shoes for the baby. I dont say I didnt have a pleasant time writing about him and he returned in Stopover Tokyo but I dont think Ill ever meet him again. Moto was an entirely different piece of writing from a so-called serious novel. He really became famous when they took him up in the movies. In book for he has never really sold well  - never more than 5,000 to 6,000 copies. I cant say why people remember him, except they must remember the serials and pictures. The World of John P. Marquand
By LEWIS NICHOLSNEWBURYPORT, MASS.. New York Times (1923-Current file)   03 Aug 1958: BR4.   
In 1959 Wagner disparaged the film:
 When I started at Fox in 1950 they were making sixty five pictures a year. Now theyre lucky if they make thirty. There was a chance to get some training in B pictures. Then TV struck. Everything went big and they started sticking me into Cinemascope spectacles. One day, smiling Joe Juvenile with no talent was doing a role intended for John Wayne. That was in a dog called Stopover Tokyo. Ive really had to work to keep up. PRESENTING A HAPPY ACT: WAGNER AND WOOD
By THOMAS McDONALDHOLLYWOOD.. New York Times (1923-Current file)   14 June 1959: X7.   

== References ==
 
*Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.

== External links ==
*  
*  at TCMDB
*  at Project Gutenberg
 

 
 
 
 
 
 
 

 