Land of the Pharaohs
{{Infobox film
| name           = Land of the Pharaohs
| image          = Land of the Pharaohs - poster.jpg
| image_size     = 250px
| caption        = 1955 theatrical poster
| director       = Howard Hawks
| producer       = Howard Hawks
| writer         = Harold Jack Bloom William Faulkner Harry Kurnitz
| narrator       = 
| starring       = Jack Hawkins Joan Collins James Robertson Justice 
| music          = Dimitri Tiomkin
| cinematography = Lee Garmes Russell Harlan
| editing        = Vladimir Sagovsky
| costumes       = Mayo
| studio = Continental Company
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 106m (US), 104m (UK)
| country        = United States
| language       = English
| budget         = $2.9 million (estimated)
| gross = $2.7 million (US)  
}} Great Pyramid. Novelist William Faulkner was one of the three screenwriters.
 The Robe, The Ten Ben Hur, and others. The film was shot on location in Egypt and in Romes Titanus studios.

==Plot==
In ancient Egypt, Pharaoh Khufu (Jack Hawkins) is obsessed with preparing his tomb for the "second life". Dissatisfied with his own architects offerings, he enlists Vashtar (James Robertson Justice), an ingenious man whose devices nearly saved his own people from being conquered and enslaved by Khufu. Khufu offers to free Vashtars people if he will build Khufu a Grave robbery|robber-proof tomb - although Vashtar will have to die when the pyramid-tomb is completed, to guard its secrets. During the years that the pyramid is being built, Pharaoh demands tribute and labor from all his territories, amassing gold and treasures to be interred with him.

Princess Nellifer (Joan Collins) comes as the ambassador of the tributary province of Cyprus. Claiming her province is poor and cannot afford to pay the assigned tribute, she offers herself to Pharaoh instead. She becomes Khufus second wife. 
 Sydney Chaplin), the captain of the guard, to take it off her and leaves. Later, Nellifer starts an affair with Treneh. who has become besotted with her.
 Dewey Martin), saves Khufu from being crushed to death by a runaway stone block. In order to get the injured Khufu out of the pyramid to help, Senta reveals that he too knows the tombs secrets, knowing that he must now share his fathers fate. A grateful Pharaoh offers him anything else within his power; Senta chooses Nellifers slave Kyra to save her from being whipped for her fierce independence.

Using a cobra obtained from a snake charmer with the help of Treneh, Nellifer plots to assassinate first Queen Nailla and her son Zanin, then Khufu, leaving her to rule Egypt. First, Treneh persuades Khufu that there is a rich hoard of treasure in a tomb far to the north, in order to draw him away from the palace. Then Nellifer gives Zanin a flute to practice and teaches him a tune the snake will be attracted to. Queen Nailla sees the snake approaching Zanin and throws herself on it to save her son.

After hearing of the queens death, Khufu sends out investigators to look for snake charmers. Panicking, Nellifer dispatches her servant Mabuna to kill Khufu at the oasis, but Mabuna only manages to wound Khufu before being killed. Suspecting Nellifer, Khufu rushes back to the city. In her chambers he overhears her and Treneh plotting; he kills Treneh in a sword fight, but is himself fatally wounded. The Pharaoh, before dying, see Nellifer wearing the necklace that he deliberately took from her earlier, and then dies while trying to get his doctor.

Hamar, deeming the tomb truly robber-proof, releases Vashtar and Senta from their death sentence, allowing them to leave Egypt and return to their homeland with their people.

During Pharaohs funeral his High Priest and lifelong friend Hamar, (Alexis Minotis, billed as Alex Minotis), has Nellifer accompany him into the burial chamber because she "must give the order" to seal the sarcophagus.  When her order is obeyed, it releases a large stone in a lower chamber, triggering Vashtars mechanism to seal the tomb.  Nellifer goes into hysterics when told the tomb is being sealed, realizing she is trapped. "Theres no way out," Hamar tells her, adding: "This is what you lied and schemed and murdered to achieve. This is your kingdom."

==Cast==
 
* Jack Hawkins as Pharaoh Khufu
* Joan Collins as Princess Nellifer Dewey Martin as Senta Alex Minotis as Hamar
* James Robertson Justice as Vashtar
* Luisella Boni as Kyra (as Luisa Boni)
* Sydney Chaplin as Treneh James Hayter as Mikka Kerima as Nailla
* Piero Giagnoni as Xenon

==Production notes==
* The costume designs are the work of French painter and costume designer Mayo (painter)|Mayo, already known for his work on Les Enfants du paradis or La Beauté du diable. 
 
* When the Pharaoh was inspecting, and rejecting, the Egyptian architects models for his tomb, the third model he looks at is a model of the actual interior of the pyramid built for Khufu. Rio Bravo (1959), four years later - the longest break between two movies in his career. unfinished pyramid of Baka. Elsewhere they built a ramp and foundation the size of the original pyramid, where thousands of extras were filmed pulling huge stone blocks.
* Other scenes were shot at a limestone quarry at Tourah, near Cairo, and at Aswan, a granite quarry located 500 miles away. At these sites 9,787 actors were filmed in one scene. 
* Hawks had between 3,000 and 10,000 extras working each day during the fifty-plus day shooting schedule. The government supplied the extras, half of whom were soldiers in the Egyptian Army

==Reception==
Lacking a strong cast, the film was unsuccessful at the box office, earning slightly less than its production budget. At box office, it earned $200,000 dollars short of the 2.9 million dollars spent on producing the film. As of 2013, Land of the Pharaohs currently holds 71% fresh approval rating on Rotten Tomatoes.

==See also==
* List of epic films
* List of historical drama films

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 