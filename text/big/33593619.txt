Alice or the Last Escapade
{{Infobox film
| name           = Alice or the Last Escapade
| image          = Alice or the Last Escapade.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Claude Chabrol
| producer       = Pierre Gauchet Patrick Hildebrand Eugène Lépicier
| screenplay     = Claude Chabrol
| based on       = Alices Adventures in Wonderland by Lewis Carroll
| narrator       = 
| starring       = Sylvia Kristel Charles Vanel
| music          = Pierre Jansen   
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = Filmel # P.H.P.G.
| distributor    = Union Générale Cinématographique (UGC)
| released       = 19 January 1977 	
| runtime        = 93 min.
| country        = France
| language       = French
| budget         = 
| gross          = 98,954 admissions (France) 

| preceded_by    = 
| followed_by    = 
}} Alice character pseudonymous surname). 

==Plot==
While leaving her husband, whom she has grown to despise, Alice (Sylvia Kristel) drives into the pristine countryside but must stop at an old house after her windshield cracks mysteriously. An old man and his butler welcome her at the mansion as if she were expected. The old man insists on her staying overnight. They even offer to have her car repaired in the morning. She is woken up in the middle of the night by a booming noise. The next day the car is there with a new windshield  but she is alone in the deserted house. After a good breakfast laid out for her she jumps into the car again but she cannot find the gateway to the country house from whence she came. A tree trunk seems to be in the way. Reluctantly she returns to the old house. She then tries to walk the way with her suitcase and she meets  a young man who tells her to accept the fact that there is no way out. Is she in limbo? She has to spend a second night in the mansion. The old man is there again and provides some explanations. The following day is a bright morning full of birdsong. Once more breakfast is ready for her in the lonely house. She takes the car again and here is the path and the gate to the highway. Is she really out? A few more strange characters come her way. Her windshield cracks again.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Sylvia Kristel || Alice Caroll  
|-
| Charles Vanel || Henri Vergennes  
|-
| Jean Carmet || Colas  
|-
| Fernand Ledoux || Le Vieil Homme et le Docteur  
|-
| André Dussollier	|| Le Jeune Homme et le Pompiste   
|-
| François Perrot ||LHomme de 40 ans    
|}

==Critical reception==
From TV Guide:
 

From www.devlidead.com:
 

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 