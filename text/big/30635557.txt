Egaro
{{Infobox film
| name           = Egaro
| image          = 
| alt            = 
| caption        = 
| director       = Arun Roy
| producer       = Magic Hour Entertainment
| writer         = 
| screenplay     = 
| story          =  Monu Mukherjee Shankar Chakraborty Rajat Ganguly Biswajit Chakraborty Tulika Basu Debaparna Chakraborty Tamal Roychoudhury Sunil Mukherjee Kharaj Mukherjee Gautam Haldar Bhaskar Banerjee Ranadip Bose Hirak Das Chandan Bhattacharjee Daviench Kelvin Others
| music          = Mayukh-Moinak
| cinematography = Gopi Bhagat
| editing        = Sanglap Bhowmik & Shyamal Karmakar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
Egaro, the Immortal Eleven    is a 2011 Bengali sports film based on a historical event. It was directed by debutant director Arun Roy. The film is based on the historical events leading to a football match between Mohun Bagan and East Yorkshire Regiment on 29 July 1911, a time when India was under the British rule. This was the first time when Mohun Bagan, or any native team won the IFA Shield. The film commemorated that event in its centenary year, i.e., 2011.

==Plot==
Egaro is also the first celluloid tribute to the eleven Mohun Bagan players who won the shield, ten of them playing barefoot clad in folded dhotis with just one of them, Sudhir Chatterjee, wearing boots against a team with the right kit, boots, dress, infrastructural support and the typical bias of the White rulers against the coloured Ruled. But Egaro is not just about football. It is about the patriotic passion that drove these eleven players to unite thousands of Indians from the entire eastern regions who flocked in from Dhaka, Burdwan, Midnapore crossing barriers of caste, class, community and language to watch the players kick and beat up the British teams on the playing field without being punished by the rulers because it was all in the game. It is about the killer spirit where the killer took prominence not because it was a fight to finish, but because the final match was a battlefield where the winning could speed the movement against Imperial rule and towards freedom. It did, in a manner of speaking. After the historic win on July 29, 1911, the British felt pressured enough to shift its capital from Calcutta to Delhi on December 12 the same year.

The film explores a parallel theme of an underground revolution brimming underneath following the Partition of Bengal in 1905 and the hanging of Khudiram Bose for attempting to kill Kingsford in 1908. Nagendra (Shankar Chakraborty) leads a group of young freedom fighters in a fight to eliminate high-ranking British officers with indigenous bombs and fire-arms without harming the old, the women and the children. He castigates one of his revolutionaries, also a Mohun Bagan player, for paying more attention to football than to the revolution. But over time, he is convinced that this final match is no less than the revolution he is leading. He comes to the finals and motivates the same member he threw out to rise and play when he faints during the match felled by one of the many false kicks of the British team.

Mohun Bagan had entered the finals after having vanquished strong teams of the British side such as Rangers, St. Xaviers and Middlesex which consolidated the team’s confidence that the British teams were not as unconquerable as they thought. East Yorkshire Regiment scored one goal before half-time sending the entire audience into a tizzy and depressing the home team. But after half-time, the team scored two goals one by Captain Sibdas Bhaduri and the other by Abhilash Ghosh, a striker. The preparation towards the final match spans a major slice of the film including family and personal hurdles the team members face. Abhilash is a student at Scottish Church College, along with teammate  Rajen Sengupta. Abhilash’s father does not care about his brilliant son’s involvement in a game that might anger the British rulers. Abhilash himself begins to rethink his stance because football, he feels, has reduced his duties towards those he loves. Sudhir Chatterjee, professor in the London Missionary Society College is insulted in the staff room by his British colleagues for daring to participate in a match against a British team. The Principal suspends him indefinitely. But Shibdas is determined to win even if one or two of the team drop out at the last minute. They dont.

Two British citizens prove that they too, are human. One of them is the match referee Puller who refuses to bend under pressure by the powers-that-be to treat the match as a ‘must-win-or-else’ ego trip against native Indians. The other is Piggot, a player of the Middlesex team who was badly injured in one eye by an inadvertent kick delivered by Abhilash. He gives Abhilash a clean chit though the officers who visit him at the hospital urge him to pronounce the Indian team player’s kick as intentional.

Egaro is a sports film where reality has been intermixed with elements of fiction. This makes the film more entertaining and exciting, filled with moments of suspense, hilarity, drama and a bit of romance thrown in delicately between Abhilash and Bina and the one-sided attraction his British colleague, the beautiful Elina feels towards her colleague Sudhir. Her reactions on the field where she sits on the British side and when Mohun Bagan scores each goal, she begins to fan herself rapidly to express her joy. The reactions of the audience provide a wonderful sense of relief and bonhomie and solidarity and patriotism bonded as one. Though three of the eight players are professional football players, their acting is beyond par and one cannot make the difference between the actors and the players. The cameos are wonderfully portrayed by renowned actors of Bengali cinema, theatre and television. The revolutionary segment is a bit overdrawn and dramatic at times. The tremendous violence by the police who kill revolutionary Satyen’s grandfather in prison could have been cut out. The flames of his pyre arousing flames of anger within his granddaughter Bina is a powerful touch.

Gopi Bhagat’s cinematography in one word is brilliant. Indraneel Ghosh’s art direction reproduces the period with old mansions and homes with brick walls with the plaster peeling off, the roadside shop selling hot fritters, the college staff room, the prison cell with amazing accuracy. Mayukh-Moinak’s music is very good but the background score by Arijit Singh is a bit too loud drowning the ambient sounds and the dialogue at some places.

==Inaccuracies==
 
* The opening scene shows a car carrying British officials being blown up by the Indian revolutionaries. The car shown is a Citroen DS which began production in 1955, and hence impossible to be owned by the British in 1911.
* The British police in 1911 Calcutta are shown wearing white uniforms. The white uniform of Calcutta police was introduced in 1923 by the then Commissioner, Sir Charles Augustus Tegart, prior to which Calcutta Police wore khaki uniforms like the rest of the country.
* The armored cars used by the Calcutta Police in 1911 are actually Army Vehicles used during World War II which did not come into existence nearly 30 years after the setting of the film.
* The Indian revolutionaries are shown brandishing an Indian flag of a design which was first used in the 1929 Congress Session.
* While discussing the need to spread the fire of the revolution to other parts of India like Punjab and Maharashtra, Nagen makes a reference to the city of Chandigarh, the capital of todays Indian state of Punjab, which came into existence only after 1947.
* The language shown to be used by the footballers in the film is the present-day Calcutta dialect. In reality, most of the footballers were immigrants from Eastern Bengal (present day Bangladesh) and would have spoken in the Bangal dialect of the early 1910s. The film makers apologize for this error in a pre-title screen.
* The language shown to be used in the 1911 newspapers is common Spoken Bengali. Common Bengali only began to be used as a printed language of newspapers in the 1970s, prior to which Sanskritised and Archaic Bengali was usually used in the newspapers.
* The languages shown to be used in the dialogues is highly contemporary and uses some figures of speech which were not in use before the 1990s.
* An Indian clerk in the Calcutta Municipal Corporation is shown to be shaking in fear while asking for leave from a British supervisor. Though included probably for adding some dramatic effect, such a situation would have never occurred as in reality a Municipality clerk in 1911 would have asked for leave from a Head Clerk, who would have also been an Indian.
* An Indian professor in a college in 1911 is shown to be called a "dirty Indian dog" by one of his colleagues, a British professor. This is a highly improbable incident more akin to the late 1700s than to the early 1900s.
* One of the British footballers is shown to be having a spiky hairstyle, which only came into practice in the late 1990s.
* Two of the British footballers are shown to be walking into a completely Indian neighbourhood all alone in an afternoon and kick and beat up a physically handicapped Indian without any kind of provocation. Though not impossible, this would have been a highly improbable incident.
* Most of the British footballers are shown to be speaking with South African or Irish accents.
* The film shows the British footballers mercilessly hitting the barefooted Indian footballers legs with their steel spikes and causing severe bleeding, concussions, bruises, fractures and causing some of them to leave the field and lose consciousness. Again added for dramatic effect, this portrayal is also completely unfounded. The only footballer to have a slight injury in the match was a British player who twisted his ankle.
* The Mohun bagan striker Abhilash Ghosh is portrayed as a very young man barely out of his teens, who is lanky, thin, fair, rather delicate and slightly built. The real Abhilash Ghosh was in his late twenties and a dark, stout and extremely broad man who was extremely aggessive in the field and was nicknamed "Black Bull" by his British opponents.
* The Mohun Bagan Captain Sibdas Bhaduri is shown to be a fiery and emotional man who leads by example and motivation. In reality, Sibdas was a quite and unassuming man who spoke very little and mostly kept to himself off the field.
* The Mohun Bagan midfielder Rajendranath Sengupta is shown to be involved in the extremist Indian Revolutionary movement when off the field, the extent that he practises shooting a pistol, smuggles bombs and plans attacks on British convoys. The real Rajendranath, though supportive of the Indian Revolutionary Movement was not known to be directly involved in its activities. He was more of a scholar and a sportsman who worked as a football commentator for Calcutta Radio until his retirement and subsequent death in 1962.
* The first goal scored by the British in the match is shown to be from a free kick, which is incorrect. In reality it was a field goal which was scored from normal play.
* The winner scored by Abhilash Ghosh is shown to be a header scored from the rebound of a bicycle kick. In reality this was also a field goal scored from normal play where the ball rolled into the net.

==Cast==
* Monu Mukherjee
* Shankar Chakraborty
* Rajat Ganguly
* Biswajit Chakraborty
* Tulika Basu
* Debaparna Chakraborty
* Tamal Roychoudhury
* Sunil Mukherjee
* Kharaj Mukherjee
* Gautam Haldar
* Bhaskar Banerjee
* Ranadip Bose
* Hirak Das
* Chandan Bhattacharjee
* Daviench
* Kelvin
* Kyle Smith
* Justin Smith

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 