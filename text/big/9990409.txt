Hi! Dharma!
{{Infobox film name           = Hi! Dharma! image          = Hi Dharma.jpg caption        = Theatrical poster director       = Park Chul-kwan producer       = Lee Joon-ik  writer         = Park Gyu-tae starring       = Park Shin-yang   Jung Jin-young music          = Park Jin-seok   Shin Ho-seop cinematography = Park Hee-ju editing        = Kim Sang-beom   Kim Jae-beom studio         = KM Culture distributor    = Cineworld released       =   runtime        = 95 minutes language       = Korean budget         =  gross          =   
}}
Hi! Dharma! ( ; literally "Hey Dharma Lets Play") is a 2001 South Korean comedy about gangsters who hide out in a monastery.  With 3,746,000 admissions, it was the fifth highest-grossing Korean film of 2001. 

A   was released in 2004.

== Plot ==
Five gangsters escape in a van after a bloody confrontation with the rival Chunno gang. They realize that they have a snitch in their own gang and that they cant get out of the country because the police will be looking for them.  So they go to the mountains and hide in a Buddhist monastery.

But the monks there dont want the gangsters as their guests.  They decide that if the gangsters can win three out of five contests, the gangsters can stay, but if they lose, they must leave immediately. The gangsters win enough contests, the last of them being suggested by the eldest monk: a challenge to fill up a broken water pot without plugging up the hole. The gangsters come up with the idea of putting the pot into the river. They are allowed to stay for a week.  But the younger monks still cant tolerate the gangsters, and attempt to persuade them to leave.

Meanwhile, the boss among the gangsters realizes who betrayed them but goes ahead and contacts him anyway, disclosing his location.  The former colleagues, now defected to the Chunno gang, show up near the monastery, dig a shallow mass grave and throw the gangsters they betrayed into it. But the monks come to the rescue of their unwanted guests.

Back at the monastery, both the monks and the gangsters are saddened to learn of the death of the eldest monk. After the funeral, the gangsters leave. Months later, they make varied donations to the monastery in gratitude for their hospitality.

==Cast==
*Park Shin-yang ... Jae-gyu
*Jung Jin-young ... Monk Jeong-myeong
*Park Sang-myun ... Bul-kom
*Kang Sung-jin ... Nal-chi
*Kim Su-ro ... Wang Ku-ra
*Hong Kyoung-in ... Rookie
*Kim In-mun ... Master
*Kim Young-moon
*Lee Dae-yeon ... Chang-guen
*Lee Mu-hyeon ... Dae-ho
*Lee Moon-sik .. Monk Dae-bong
*Lee Won-jong ... Monk Hyeon-gak
*Im Hyun-kyung ... Yeun-hwa
*Ryu Seung-soo ... Monk Myung-chun
*Kwon Oh-min ... Boy monk

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 