Bomb Harvest
 
 
{{Infobox film
 | name = Bomb Harvest
 | image = Bomb_harvest_poster.gif
 | caption = Theatrical release poster
 | director = Kim Mordaunt
 | producer = Sylvia Wilczynski
 | Screenplay = Kim Mordaunt and Sylvia Wilczynski
 | starring = Laith Stevens
 | music = Caitlin Yeo
 | cinematography =
 | editing = Sloane Klevin
 | distributor = TVF International
 | released = 2007
 | runtime = 88 minutes
 | country = Australia
 | language = English
 | budget =
 }}
Bomb Harvest is a 2007 documentary film directed by Australian filmmaker Kim Mordaunt and produced by Sylvia Wilczynski. It explores the consequences of war in Laos as it follows an Australian bomb disposal specialist, training locals in the skill of detonating bombs while trying to stop villagers, particularly children, from finding them and using them for scrap metal. 

During the   (UXO) continues to kill and injure people, and prevent them from using land, including growing food. UXO are a key factor in the poverty and stifling the development of the country.   

Bomb Harvest explores how three generations of people have been left to deal with the consequences of the air war, and depicts the bravery of those trying to clear up its remnants.
 Artivist Film Festival.

==See also==
*Cluster Munition Coalition
*Convention on Cluster Munitions
*International Campaign to Ban Landmines
*History of Laos
*Operation Barrel Roll
*Plain of Jars

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 
 
 
 