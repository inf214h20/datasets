Her Fractured Voice
{{infobox film
| name = Her Fractured Voice
| image =
| imagesize =
| caption =
| director =
| producer = 
| writer = Beatrice Joy
| cinematography =
| studio = United States Motion Picture Corporation
| distributor = Paramount Pictures
| released = June 1917
| runtime = 15 mins.
| country = United States
| language = Silent English intertitles
}}
 short silent silent comedy film starring Leatrice Joy (billed as Beatrice Joy). The film was produced by United States Motion Picture Corporation under the "Black Diamond Comedies" name.

==Preservation status==
A copy of the film is preserved at the Prelinger  Archives. 

==Cast==
*Leatrice Joy - (*as Beatrice Joy)
*Mildred Davis - Girl with bow ribbon (*unconfirmed, but looks like her)

==Plot==
A young woman (Joy) is obsessed with the idea that she can and must sing. Living on a farm, she has lots of open space in which to exercise her voice, but is compelled to admit that not even the farm animals will listen to her.

During an opportunity to sing in the choir, she awakens every living thing, among others a number of peacefully sleeping members of the congregation.

From the city comes a smooth-talking man who promises her the world if she will only be his. They go to the big city where, at a trial given to her in a cabaret, she nearly causes a riot, before the inevitable happy ending.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 