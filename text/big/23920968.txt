Slaves in Bondage
{{Infobox film
| name           = Slaves in Bondage
| image          =
| image_size     =
| caption        =
| director       = Elmer Clifton
| producer       = J.D. Kendis
| writer         = Robert Dillon
| narrator       =
| starring       = See below
| music          =
| cinematography = Edward Linden Earl Turner
| distributor    = Jay-Dee-Kay Productions
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
Slaves in Bondage (1937 in film|1937) is an American film directed by Elmer Clifton. It stars Lona Andre, Donald Reed, and Wheeler Oakman.

==Production background== white slavery prostitution rings operating in Americas larger cities.

The film is typical of the many exploitation film features of its time that claimed to warn the public about various kinds of shocking sin and depravity corrupting todays society. In reality, these films were cynical, profit-motivated vehicles that wallowed in lurid, taboo subjects such as drug abuse, promiscuous sex, venereal disease, polygamy, child marriages, etc. Some even included brief nude scenes such as Sex Madness (1937), Marihuana (film)|Marihuana (1936), and Assassin of Youth (1937) — also directed by Elmer Clifton.

== Plot summary ==
The story tells the tale of how naive country girls are lured to the big city with the promise of employment only to be abducted and forced to work as prostitutes in decadent, high-class brothels.

The film is loaded with scenes of scantily-clad prostitutes waiting for their next "appointment", performing risque dances or engaging in fetishistic entertainments for their clients.  If for nothing else, this film is notable for being one of the first (if not the first) to depict certain fetishes as a sexual specialty. For example, Belle, the brothel madam, shows Dona her Oriental Room ("reserved for the exotic") where two masochistic, lingerie-clad women are grappling on a bed and taking turns spanking each other. There is also an eroticized "catfight" scene with energetic wresting, slapping, hair pulling, and tearing of clothes staged for the patrons amusement.

== Cast ==
*Lona Andre as Dona Lee
*Donald Reed as Phillip Miller
*Wheeler Oakman as Jim Murray
*Florence Dudley as Belle Harris
*John Merton as Nick Costello
*Richard Cramer as Dutch Hendricks
*William Royle as Newspaper City Editor
*Edward Peil Sr. as Detective Captain
*Louise Small as Mary Lou Smith
*Matty Roubert as Good-Looking Freddie
*Suzanna Kim as Fan Dancer
 Donald Kerr, Fred Parker, Henry Roquemore, Lottie Smith and Arthur Thalasso appears uncredited.

==See also==
*List of rediscovered films

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 