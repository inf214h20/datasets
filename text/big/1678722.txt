Attack of the Giant Leeches
{{Infobox film
| name           = Attack of the Giant Leeches
| image          = Giantleeches.jpg
| image_size     = 
| border         = 
| alt            = A promotional film poster for Attack of the Giant Leeches
| caption        = A promotional film poster for Attack of the Giant Leeches
| director       = Bernard L. Kowalski
| producer       = {{Plainlist |
* Gene Corman
* Roger Corman
}}
| writer         = Leo Gordon
| screenplay     = Leo Gordon
| story          = Leo Gordon
| based on       =  
| narrator       = 
| starring       = {{Plainlist | Ken Clark
* Yvette Vickers
* Jan Shepard
}} Alexander Laszlo
| cinematography = John M. Nickolaus Jr.
| editing        = Carlo Lodato
| studio         = Balboa Productions
| distributor    = American International Pictures
| released       =  
| runtime        = 62 min
| country        = United States
| language       = English
| budget         = $70,000 (estimated) Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p148 
| gross          = 
}}

Attack of the Giant Leeches is a low-budget 1959 science fiction film from American International Pictures, directed by Bernard L. Kowalski and produced by Gene Corman. The screenplay was written by Leo Gordon. It was one of a spate of monster movies produced during the 1950s in response to cold war fears; in the film, a character speculates that the leeches have been mutated to giant size by atomic radiation from nearby Cape Canaveral.

The film has also been released as Attack of the Blood Leeches, Demons of the Swamp, She Demons of the Swamp, and The Giant Leeches. {{cite web
 | url = http://www.imdb.com/title/tt0053611/releaseinfo#akas
 | title = Release dates for Attack of the Giant Leeches
 | publisher = Internet Movie Database
 | accessdate = 2007-05-19
}} 

== Plot ==
In the Florida Everglades, a pair of larger-than-human, intelligent leeches are living in an underwater cave. They begin dragging local people down to their cave where they hold them prisoner and slowly drain them of blood.
 Ken Clark) sets out to investigate their disappearance. Aided by his girlfriend, Nan Grayson (Jan Sheppard), and her father, Doc Grayson, he discovers the cavern.

The monsters are finally destroyed when Steve, Doc, and some state troopers blow up the cavern with dynamite.

== Cast == Ken Clark as Steve Benton
*Yvette Vickers as Liz Walker
*Jan Shepard as Nan Greyson
*Michael Emmet as Cal Moulton
*Tyler McVey as Doc Greyson
*Bruno VeSota as Dave Walker
*Gene Roth as Sheriff Kovis Dan White as Porky Reed George Cisar as Lem Sawyer
*Guy Buccola as Giant Leech
*Joseph Hamilton as Old Sam Peters
*Walter Kelley as Mike
*Ross Sturlin as Giant Leech

== Production ==
The film was shot in eight days, including outdoor sequences at the Los Angeles County Arboretum and Botanic Garden. During filming, Gene Corman came down with pneumonia and wound up in the hospital. 

It was Kowalskis second film for AIP following Night of the Blood Beast. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 17  The music score was actually a stock score previously used in Night of the Blood Beast (1958), and can also be heard in Beast from Haunted Cave (1959). Internet Movie Database Trivia 

According to Kowalski, Gene Corman didnt want to pay the technicians the extra money for pushing the camera raft while they were filming in the water at the Arboretum, so he put on a bathing suit and did it himself. 

== Reception ==
In July 1992, Attack of the Giant Leeches was featured as a fourth-season episode of movie-mocking television show Mystery Science Theater 3000. Attack of the Giant Leeches was also featured on the nationally-syndicated horror host television show Cinema Insomnia,    and in the second episode of season five of Shilling Shockers, a New England-based television show hosted by the witch Penny Dreadful XIII.   

== Home media ==
*Attack of the Giant Leeches has received numerous "bargain bin" releases. Rhino Home Video as part of the Collection, Volume 6 box set.

== Remake ==
A remake of the film was released by Brain Damage Films in 2012.

== See also ==
* List of films in the public domain

== References ==
 

== External links ==
*   complete film on YouTube
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 