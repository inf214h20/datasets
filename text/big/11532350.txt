Monkey Trouble
{{Infobox film
| name           = Monkey Trouble
| image          = Monkey trouble.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Franco Amurri
| producer       = Mimi Polk Heidi Rufus Isaacs
| writer         = Franco Amurri Stu Drieger
| starring       = Thora Birch Harvey Keitel Christopher McDonald Mimi Rogers Alison Elliott Adam LaVorgna and Patrick Bingham as Dodger the monkey
| music          = Mark Mancina
| cinematography = Luciano Tovoli
| editing        = Ray Lovejoy
| studio         = Effe Productions
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes
| country        = United States Italy Japan
| language       = English
| budget         = 
| gross          = $16,453,258 
}}
Monkey Trouble is a 1994 comedy film directed by Franco Amurri and starring Thora Birch.

==Plot==
9-year-old Eva Gregory longs for a pet to call her own, but her divorced mother Amy does not think she is responsible enough, as her room is a mess, and shes completely unwilling to help out at all with her little half-brother Jack. Also her stepfather Tom, a police Lieutenant, is allergic to fur. She cannot keep the pet at her father Peters house because he is a pilot and travels a lot.

One day, a Capuchin monkey named Fingers runs away from his drunken master, a gypsy thief named Azro, who had trained the monkey to pick pockets and now blames him for Azros domestic woes. He meets Eva in the park, and she names him Artful Dodger|Dodger. At home, she finds that keeping him a secret is trouble. She only allows her toddler brother Jack, who cannot speak yet, into her room with Dodger.

As Eva spends certain weekends with her father, one of those weekends arrives. However, Peter leaves a phone message that hes in Canada and cant have Eva over. Eva takes advantage of this circumstance to have a personal weekend alone with Dodger in her fathers empty house, and hides her fathers answering machine announcement from her mother and stepfather, and has her friends mother drive her over to her fathers house. Once there, they realize that they have no food, but they manage to raise money at the nearby boardwalk with Dodger (who is secretly pickpocketing everybody) as a main attraction. At a grocery store that evening, Dodger steals food and hides it in Evas backpack, but Eva catches him in the act and returns the stolen groceries. When she returns home, she discovers golden jewelry and wallets in her backpack that the monkey had also stolen. So Eva spends the rest of the weekend teaching Dodger not to steal.

Azro, who has been looking for Fingers all this time, finds and kidnaps him at a pet shop where Eva had left him while she was at school. Azro discovers that the monkey refuses to steal anymore. Meanwhile, Amy and Tom, who has been dealing with reports on stolen jewelry, discover more stolen property in Evas room. They confront her about it, and Eva tries to explains about her hidden monkey, but they refuse to believe her. Things get worse when Peter stops by and reveals that he had been in Canada all weekend, which reveals that Eva had lied about the weekend. Eva --- already heartbroken at the disappearance of her beloved pet --- is additionally upset that no one will believe her, and so she runs away to look for Dodger.  She is accosted by Azro, who is furious about her teaching Dodger not to steal. Meanwhile, Jack ends up saying his first word, "monkey", revealing to Amy, Peter and Tom that there really is a monkey in the house (Dodger had escaped from Azro again shortly after Eva had run off, and had sneaked back into Evas bedroom) and that Eva had been telling them the truth. They all go out, with Toms fellow police officers, to look for her. Dodger saves her and Azro is arrested by Tom. Azros son Mark tries taking the monkey back, but fails. He ends up living with Eva, after she shows her mother that she is responsible and her stepfather discovers that he is not allergic to the fur of monkeys.

==Cast==
* Finster as Dodger
* Thora Birch as Eva Gregory
* Mimi Rogers as Amy
* Christopher McDonald as Tom
* Harvey Keitel as Azro
* Alison Elliott as Tessa
* Adam LaVorgna as Mark
* Robert Miranda as Drake
* Victor Argo as Charlie
* Remy Ryan as Katie

==Soundtrack==
# "Sold for Me" – The Aintree Boys
# "Posie" – The Aintree Boys Quo
# "VB Rap" – Gee Boyz
# "Girls" – Gee Boyz
# "Monkey Shines" – Robert J. Walsh

==Reception==
The movie received mixed reviews from critics.    

==Box office==
The film performed poorly at the box office. It debuted at number 3 at the American box office,  dropping to seventh place the following week.  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 