The White Dawn
{{Infobox film
| name           =   The White Dawn
| image          = The White Dawn 1974.jpg
| image size     =
| caption        =
| director       =  Philip Kaufman
| producer      =  Martin Ransohoff James Houston James Houston Thomas Rickman
| narrator       =
| starring       =  Warren Oates Timothy Bottoms Louis Gossett Jr.
| music          =  Henry Mancini Michael Chapman Douglas Stewart
| studio         =  American Film Properties Filmways
| distributor    =  Paramount Pictures
| released       =   21 July 1974 (New York City, New York)
| runtime        =   110 minutes
| country        =   USA English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

The White Dawn is a  , who co-wrote the screenplay.

==Plot==
When three whalers become stranded in Northern Canada’s Arctic in 1896, they are rescued by Inuit.  In the beginning, the Inuit accept the strangers European ways, but as this increasingly influences and affects their customs, things slowly fall apart and cultural tension grows until the climax.

==Cast==
*Warren Oates  – Billy
*Timothy Bottoms  – Daggett
*Louis Gossett, Jr.  – Portagee
*Joanasie Salomonie  – Kangiak
*Pilitak  – Neevee
*Simonie Kopapik  – Sarkak / Sark
*Akshooyooliak  – Old Mother
*Namonai Ashoona  – Nowya
*Atcheelak  – Hunters
*Higa Ipeelie  – Evaloo
*Oolipika Joamie  – Mia
*Meetook Mallee  – Ikuma
*Neelak  – Panee
*Seemee Nookiguak  – Avinga
*Jacob Partridge  – Archer
*Sagiaktok  – Shaman

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 