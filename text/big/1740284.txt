Gigi (1958 film)
{{Infobox film
| name           = Gigi
| image          = AmericanGigiPoster.jpg
| image_size     = 215px
| alt            = 
| caption        = Original poster
| director       = Vincente Minnelli
| producer       = Arthur Freed
| screenplay     = Alan Jay Lerner Niven Busch (uncredited)
| based on       = Gigi (1944 novella) by Colette
| starring       = Leslie Caron Louis Jourdan Maurice Chevalier Hermione Gingold
| music          = Frederick Loewe
| cinematography = Joseph Ruttenberg
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 115 minutes  
| country        = United States
| language       = English
| budget         = $3,319,355    Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History. Wayne State University Press, 2010. p. 162. 
| gross          = $13,208,725 
}} musical romantic of the same name by Colette. The film features songs with lyrics by Lerner; music by Frederick Loewe, arranged and conducted by André Previn.
 MGM musical Bells Are stage musical Broadway in 1973.

A pre-Broadway production of the musical, newly adapted by Heidi Thomas (Call the Midwife, Cranford, Upstairs Downstairs) and directed by Eric D. Schaeffer (Follies, Million Dollar Quartet) is planned to run the Kennedy Center in January 2015. 

==Plot==
Set in turn-of-the-20th century Paris, the film opens with Honoré Lachaille (Maurice Chevalier) among high society in the Bois de Boulogne. A charming old roué, he cynically remarks that "Like everywhere else, most people in Paris get married, but not all. There are some who will not marry, and some who do not marry. But in Paris, those who will not marry are usually men, and those who do not marry are usually women."  So marriage is not the only option for wealthy young bon vivants like his nephew Gaston (Louis Jourdan), who is bored with life. The one thing Gaston truly enjoys is spending time with Madame Alvarez (Hermione Gingold), whom he calls Mamita, and especially her granddaughter, the precocious, carefree Gilberte, aka Gigi (Leslie Caron). Following the family tradition, Madame Alvarez sends Gigi to her sister, Great Aunt Alicia (Isabel Jeans), to be groomed as a courtesan and learn etiquette and charm. To Alicia, love is an art, and a necessary accomplishment for Gigis social and economic future. The young girl initially is a very poor student who fails to understand the reasons behind her education. She enjoys spending time with Gaston, whom she regards as an elder brother.
 mistress and tries to rebuild his reputation with endless parties, he decides to take a vacation by the sea. Gigi proposes if she beats him at a game of cards he must take her and Mamita along. He accepts, and she happily wins (by cheating). During their holiday, Gigi and Gaston spend many hours together, and the two learn Honoré and Mamita once were romantically involved before becoming comfortable friends. Alicia insists Gigis education must increase dramatically if she is to catch a prize such as Gaston. Gigi is miserable with her lessons, but endures them as a necessary evil, though she still seems awkward and bumbling to her perfectionist great-aunt. When Gaston sees Gigi in an alluring white gown, he tells her she looks ridiculous and storms out, but later returns and apologizes, offering to take her to tea to make amends. Mamita refuses, telling him a young girl seen unchaperoned in his company might be labeled in such a way as could damage her future. Enraged yet again, Gaston storms out and wanders the streets of Paris in a fury.

Realizing he has fallen in love with Gigi, who no longer is the child he thought her to be, Gaston returns to Mamita and proposes he take Gigi as his mistress, promising to provide the girl with luxury and kindness. The young girl declines the offer, telling him she wants more for herself than to be passed between men, desired only until they tire of her and she moves on to another. Gaston is horrified at this portrayal of the life he wishes to give her, and leaves stunned. Gigi later decides she would rather be miserable with him than without him. Prepared to accept her fate as Gastons mistress, Gigi emerges from her room looking like a woman. Gaston is enchanted and takes her to dinner at Maxims Paris|Maxims, where she seems perfectly at ease. The stares of other patrons make Gaston extremely uncomfortable as he realizes Gigis interpretation of things may have been accurate after all, and discovers his love for her makes the idea of her as his mistress an unbearable one. He leaves the party with Gigi in tow and takes her home without explanation. After wandering the streets throughout the night, he returns to Mamitas home and humbly asks for Gigis hand in marriage.

The final sequence reverts to Honoré Lachaille, proudly pointing out Gaston and Gigi riding in their carriage in the Bois de Boulogne, which is filled with high society. The couple are elegant, beautiful, and happily married. Honoré has been a framing device for the film, which can be seen as a romantic victory of love over cynicism.

==Cast==
* Leslie Caron as Gilberte "Gigi"
* Louis Jourdan as Gaston Lachaille
* Maurice Chevalier as Honoré Lachaille
* Hermione Gingold as Madame Alvarez
* Isabel Jeans as Aunt Alicia
* Eva Gabor as Liane dExelmans
* Jacques Bergerac as Sandomir John Abbott as Manuel

==Musical numbers==
# Overture – Played by MGM Studio Orchestra
# "Honorés Soliloquy" – Sung by Maurice Chevalier
# "Thank Heaven for Little Girls" – Sung by Maurice Chevalier
# "Its a Bore" – Sung by Maurice Chevalier, Louis Jourdan and John Abbott
# "The Parisians" – Sung by Leslie Caron (dubbed by Betty Wand)
# "The Gossips" – Sung by Maurice Chevalier and MGM Studio Chorus
# "She is Not Thinking of Me" – Sung by Louis Jourdan
# "The Night They Invented Champagne" – Sung and Danced by Leslie Caron (dubbed by Betty Wand), Hermione Gingold and Louis Jourdan
# "I Remember It Well" – Sung by Maurice Chevalier and Hermione Gingold
# "Gastons Soliloquy" – Sung by Louis Jourdan
# "Gigi" – Sung by Louis Jourdan
# "Im Glad Im Not Young Anymore" – Sung by Maurice Chevalier
# "Say a Prayer for Me Tonight" – Sung by Leslie Caron (dubbed by Betty Wand)
# "Thank Heaven for Little Girls (Reprise)" – Sung by Maurice Chevalier and MGM Studio Chorus

(Note: The album sleeve of the Gigi soundtrack makes a curious cameo appearance on certain versions of the Pink Floyd Ummagumma#Cover art|Ummagumma album cover.)

==Production==
===Development===
Hollywood producer Arthur Freed first proposed a musical-ization of the Colette novella to Alan Jay Lerner during the Philadelphia tryout of My Fair Lady in 1954. When Lerner arrived in Hollywood two years later, Freed was battling the Hays Code in order to bring his tale of a courtesan-in-training to the screen. Another roadblock to the project was the fact Colettes widower had sold the rights to her novella to Gilbert Miller, who planned to produce a film version of the 1954 stage adaptation by Anita Loos. It cost Freed more than $87,000 to purchase the rights from Miller and Loos. 
 Broadway production An American in Paris for him. Both agreed Maurice Chevalier would be ideal for aging boulevardier Honoré Lachaille, and Lerner proposed Dirk Bogarde for Gaston. Lerner agreed to write the lyrics if Freed could convince Bogarde and designer Cecil Beaton to join the project. He decided to approach Loewe once again, and when he suggested they compose the score in Paris, Loewe agreed. 
 Love Life, a 1948 collaboration with Kurt Weill.  "Say a Prayer for Me Tonight", a solo performed by Gigi, had been written for Eliza Doolittle in My Fair Lady but was removed during the pre-Broadway run. Lerner disliked the melody, but Loewe, Freed, and Minnelli voted to include it in the film. 

===Casting=== Peter Hall. Anglicized to Three Coins in the Fountain, Freed offered him the role of Gaston. 

===Filming===
In late April, Freed and Minnelli and their respective entourages arrived in Paris. The weather had become unseasonably hot, and working in non-air-conditioned hotel rooms was uncomfortable. Minnelli began scouting locations while Freed and Lerner discussed the still incomplete script. Lerner had taken liberties with Colettes novella; the character of Honoré, nonexistent in the original book and very minor in the Loos play, was now a major figure. Gigis mother, originally a significant character, was reduced to a few lines of dialogue delivered off-screen. Lerner also expanded the focus on Gigis relationship with her grandmother. 

By mid-July, the composers had completed most of the score but still were missing the title tune. Loewe was at the piano while Lerner was indisposed in the bathroom, and when the former began playing a melody the latter liked, he later recalled he jumped up, "  trousers still clinging to   ankles, and made his way to the living room. Play that again, he said. And that melody ended up being the title song for Gigi." 

In September, the cast and crew flew to California, where several interior scenes were filmed, among them the entire scene in Maxims, which included a musical number by Jourdan. Lerner was unhappy with the look of the scene as it had been shot by Minnelli, and at considerable expense the restaurant was recreated on a soundstage and the scene was reshot by director Charles Walters, since Minnelli was overseas working on a new project. 

The film title design uses the artwork of Sem (artist)|Sems work from the Belle Époque.

==Release== Santa Barbara Royale Theatre, a legitimate theatrical venue in New York City, on May 15, 1958. 

===Box office===
According to MGM records, the film earned $6.5 million in the US and Canada and $3.2 million elsewhere during its initial theatrical release, resulting in a profit of $1,983,000.   

==Critical reception==
Bosley Crowther of The New York Times called it "a musical film that bears such a basic resemblance to My Fair Lady that the authors may want to sue themselves". He added, "But dont think this point of resemblance is made in criticism of the film, for Gigi is a charming entertainment that can stand on its own two legs. It is not only a charming comprehension of the spicy confection of Colette, but it is also a lovely and lyrical enlargement upon that storys flavored mood and atmosphere…Vincente Minnelli has marshaled a cast to give a set of performances that, for quality and harmony, are superb." 

Abel Green of Variety (magazine)|Variety called the film "100% escapist fare" and predicted it "is destined for a global boxoffice mopup". He added, "Alan Jay Lerners libretto is tailor-made for an inspired casting job for all principals, and Fritz Loewes tunes (to Lerners lyrics) already vie with and suggest their memorable My Fair Lady score… Miss Caron is completely captivating and convincing in the title role… Skillful casting, performance and presentation have endowed realism to the sum total… Director Minnellis good taste in keeping it in bounds and the general sound judgment of all concerned…distinguishes this Arthur Freed independent production. The Metrocolor rates recognition for its soft pastels under Joseph Ruttenbergs lensing; the Beaton costumes, sets and general production design are vivid physical assets at first sight. The skillful integration of words-and-music with the plot motivation makes this Gigi a very fair lady indeed as a boxoffice entry." 
 Time Out New York said, "The dominating creative contribution comes from Minnelli and Cecil Beaton… The combination of these two visual elitists is really too much—its like a meal consisting of cheesecake, and one quickly longs for something solid and vulgar to weigh things down. No doubt inspired by the finicky, claustrophobic sets and bric-à-brac, the cast tries (with unfortunate success) to be more French than the French, especially Chevalier. The exception is Gingold, who inhabits, as always, a world of her own." 

TV Guide rated the film 3½ out of five stars, calling it "Overbaked but enjoyable, and a banquet for the eyes, thanks to the visual wonder of the Minnelli-Beaton teaming… Caron…leads the cast in a contest to see who can be the most French. The winner is Chevalier, in a performance that makes one feel as if youre gagging on pastry… Perhaps if the sweetness of Gigi was contrasted with elements of honest vulgarity, the picture could balance itself out… Ten minutes into the movie, youve resolved the plot and are left to wallow in lovely frou-frou.   makes wonderful use of the usual Parisian landmarks, and benefits from extraordinary period costumes and sets." 

==Awards and nominations== 2004 Oscars ceremony with 11 Oscar nominations and 11 Oscar wins.

;Academy Awards Best Picture (winner) Best Director  (Vincente Minnelli, winner) Best Adapted Screenplay  (Alan Jay Lerner, winner) Best Art Direction (E. Preston Ames, F. Keogh Gleason, Henry Grace, and William A. Horning, winners) Best Cinematography (Joseph Ruttenberg, winner) Best Costume Design (Cecil Beaton, winner) Best Film Editing (Adrienne Fazan, winner) Best Original Score (André Previn, winner) Best Original Song ("Gigi" by Alan Jay Lerner and Frederick Loewe, winners)

;Golden Globe Awards Best Motion Picture – Musical or Comedy (winner) Best Director – Motion Picture (Vincente Minnelli, winner) Best Supporting Actress – Motion Picture (Hermione Gingold, winner) Best Actress – Motion Picture Musical or Comedy (Leslie Caron, nominee) Best Actor – Motion Picture Musical or Comedy (Maurice Chevalier, nominee) Best Actor – Motion Picture Musical or Comedy (Louis Jourdan, nominee)

;Other awards
* Writers Guild of America Award for Best Written American Musical (Alan Jay Lerner, winner)
* Directors Guild of America Award for Outstanding Directing – Feature Film (Vincente Minnelli and  assistant director George Vieira, winners) Grammy Award for Best Score Soundtrack Album for a Motion Picture, Television or Other Visual Media (André Previn, winner)

==Popular culture==
* The album cover appears on the cover for Pink Floyds Ummagumma (1969) album, designed by Storm Thorgerson. For the US/Canada and Australian releases the cover was airbrushed out because of fears of copyright infringement. The artwork was restored in the later CD releases in all territories.

*Television journalist Barbara Walters announced on her daytime talk show The View that Gigi is her favorite movie of all time. 

*Edith Bouvier Beale (best known as Little Edie) mentioned several times on the Maysles brothers documentary Grey Gardens that Gigi with Leslie Caron was absolutely marvelous".

==Digital restoration==  digitally restored, frame-by-frame digital restoration was done by Prasad Corporation removed dirt, tears, scratches and other defects. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 