Villa Amalia (film)
 
{{Infobox film
| name           = Villa Amalia
| image          = Villa-amalia film.jpg
| caption        = Film poster
| director       = Benoît Jacquot
| producer       = Edouard Weil
| writer         = Benoît Jacquot Julien Boivent Pascal Quignard
| starring       = Isabelle Huppert
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = France
| language       = French
| budget         = 
}}

Villa Amalia is a 2009 French drama film adapted from the novel by Pascal Quignard. It is directed by Benoît Jacquot and stars Isabelle Huppert.   

== Plot ==
Ann (Isabelle Huppert) is a gifted and brilliant musician whose sense of security falls to pieces when she witnesses her husband kissing another woman. Without hesitation, she abandons him and takes a headlong rush into the arms of a new beginning, embarking on a transnational journey that ultimately takes her to an isolated villa on the secluded island of Ischia, Italy. Once settled, Ann insists on goading herself to fresh extremes, and takes it upon herself to swim out as far into the ocean as possible. Fainting under the scorching summer rays, her floating body is pulled out of the water by local woman Giulia (Maya Sansa), with whom Ann begins to explore a whole new facet of life. 

==Cast==
* Isabelle Huppert - Ann
* Jean-Hugues Anglade - Georges
* Xavier Beauvois - Thomas
* Maya Sansa - Giula
* Clara Bindi - Marion
* Viviana Aliberti - Veri
* Michelle Marquais - La mère dAnn
* Peter Arens - Anns father Ignazio Oliva - Carlo
* Jean-Pierre Gos - The real-estate
* Jean-Michel Portal - Piano buyer
* Maurice Bernart
* Jean Coulon - Concert Organizer

== See also ==
* Villa Amalia (novel)
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 