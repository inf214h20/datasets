Zombieland
 
 
{{Infobox film
| name           = Zombieland
| image          = Zombieland-poster.jpg
| caption        = Theatrical release poster
| alt            = Poster for Zombieland with subtitle "Nut up or shut up" and movie credits. The four actors appear as a group all holding different weapons.
| director       = Ruben Fleischer
| producer       = Gavin Polone
| writer         = Rhett Reese Paul Wernick
| narrator       =  
| starring       = Woody Harrelson Jesse Eisenberg Emma Stone Abigail Breslin
| music          = David Sardy
| cinematography = Michael Bonvillain
| editing        = Peter Amundson Alan Baumgarten
| studio         = Relativity Media Pariah
| distributor    = Columbia Pictures
| released       =  
| runtime        = 87 minutes  
| country        = United States
| language       = English
| budget         = $23.6&nbsp;million 
| gross          = $102.4 million  
}}

Zombieland is a 2009 American horror comedy film directed by Ruben Fleischer from a screenplay written by Rhett Reese and Paul Wernick. The film stars Jesse Eisenberg, Woody Harrelson, Emma Stone, and Abigail Breslin as survivors of a zombie apocalypse. Together, they take an extended road trip across the Southwestern United States in an attempt to find a sanctuary free from zombies.
 Dawn of World War Z in 2013.

==Plot== strain of mad cow disease mutated into "mad person disease" that became "mad zombie disease" which overran the entire United States population, turning American people into vicious zombies. Unaffected college student "Columbus" (Jesse Eisenberg) is making his way from his college dorm in Austin, Texas to Columbus, Ohio to see whether his parents are still alive. He encounters "Tallahassee" (Woody Harrelson), another survivor who is particularly violent in killing zombies. Though he does not appear to be sociable, Tallahassee reluctantly allows Columbus to travel with him. Tallahassee mentions he misses his "puppy" that was killed by zombies, as well as his affinity for Twinkies, which he actively tries to find.  Survivors of the zombie epidemic have learned that it is best not to grow attached to other survivors, because they could die at any moment, so many have taken to using their city of origin as nicknames, i.e. "Columbus" is from Columbus, Ohio.  
 Los Angeles, an area supposedly free of zombies. After learning his home town has been destroyed, and his parents likely killed, Columbus decides to accompany the others to California. Along the trip, Columbus persists in trying to impress and woo Wichita.
 Monopoly that Tallahassee has not been grieving for his dog, but rather for his young son. Wichita becomes increasingly attracted to Columbus, and Tallahassee bonds with Little Rock, with whom he was previously at odds. Despite Wichitas attraction to Columbus, she fears attachment and leaves with Little Rock for Pacific Playland the next morning. Columbus decides to go after Wichita, and convinces Tallahassee to join him.
 drop tower ride called Blast Off, Tallahassee and Columbus arrive. Tallahassee lures the zombies away from the tower, creating a distraction for Columbus to get to the tower ride; both use the attractions to their advantage. Tallahassee eventually locks himself in a game booth, shooting zombies as they arrive. Columbus successfully evades and shoots through several zombies to reach the tower, but not before conquering some of his phobias and even changing one of his rules of survival. In thanks, Wichita kisses Columbus and reveals her real name, Krista; Little Rock gives Tallahassee a Twinkie. Columbus realises he now has what hes always wanted: a family.

==Cast==
* Jesse Eisenberg as Columbus 
* Woody Harrelson as Tallahassee
* Emma Stone as Wichita
* Abigail Breslin as Little Rock
* Amber Heard as 406
* Bill Murray as himself Mike White as Gas Station Owner

==Themes==

===The rules===
 
A running gag (and a central theme throughout the film) is the list of rules Columbus comes up with for surviving in the zombie-infested world. By the end of the film, his list has thirty-three rules, yet only a few are mentioned. A series of promotional videos starring Woody Harrelson and Jesse Eisenberg expanded on the list presented in the film. 

{{ordered list
| 1 = "Aerobic exercise|Cardio"    Ziploc bags" in a deleted scene) 
| 3 = "Beware of bathrooms"  
| 4 = "Seatbelts"
| item6_value = 6 | 6 = "Cast iron skillet"
| item7_value = 7 | 7 = "Travel light"
| item8_value = 8 | 8 = "Get a kickass partner" Bounty paper towels"
| item15_value = 15 | 15 = "Bowling Ball"
| item17_value = 17 | 17 = "Dont be a hero"; Columbus later changes the rule to "Be a hero" at the amusement park, after facing his greatest fear (a clown-zombie) to save Wichita and Little Rock.
| item18_value = 18 | 18 = "Limber up"
| item19_value = 19 | 19 = "Ziploc bags"
| item21_value = 21 | 21 = "Avoid strip clubs"
| item22_value = 22 | 22 = "When in doubt, know your way out"
| item28_value = 28 | 28 = "Double-knot your shoes"
| item29_value = 29 | 29 = "The buddy system"
| item31_value = 31 | 31 = "Check the back seat" 
| item32_value = 32 | 32 = "Enjoy the little things" 
| item33_value = 33 | 33 = "Swiss army knife"
| item34_value = 34 | 34 = "Clean socks"
| item48_value = 48 | 48 = "Hygiene"
| item49_value = 49 | 49 = "Always have backup"
}}

===Character names=== Little Rock) Beverly Hills", as well as Sister Cynthia Knickerbocker (disambiguation)|Knickerbocker, whom Columbus identifies as a "Zombie Kill of the Week" winner, and whose surname is actually an obsolete term for a citizen or inhabitant of New York City.  There is one exception in Bill Murray playing himself. At the end of the film, Wichita tells Columbus that her real name is Krista. 

==Production==

===Writing===
Writers Rhett Reese and Paul Wernick stated that the idea for Zombieland had "lived in   heads" for four-and-a-half years. The story was originally developed in 2005 as a spec script for television pilot in the summer of 2005. 
Wernick stated "Weve got a long brainstorming document that still to this day gets updated on a near-weekly basis with ideas".    Director Ruben Fleischer helped develop the script from a series into a self-contained feature by providing a specific destination to the road story, the amusement park. 

Earlier versions of the script called the protagonists Flagstaff and Albuquerque, rather than Columbus and Tallahassee, and the female characters were called Wichita and Stillwater.       vegetarian diet for 11 months.   

===Filming and design===
 
  Panavision Genesis digital camera  and had a 41-day shooting schedule. 
 Wild Adventures Water and Theme Park.  Some of the rides prominently featured in the film include Pharaohs Fury; the Double Shot (redubbed "Blast Off"); the Rattler; the Aviator; and the Bug Out. Another coaster seen, but not used, is the parks iconic Boomerang roller coaster. A haunted house facade was constructed at the theme park; but the interior was filmed on location at Netherworld Haunted House outside the city limits of Atlanta. 
 Tony Gardner, Hollywood films like 127 Hours, Hairspray (2007 film)|Hairspray, and Theres Something About Mary, was brought on to design the look of the films zombies.     Michael Bonvillain, who was Cloverfields cinematography|cinematographer, was brought on for the "lively" hand-held camerawork.    "Basically, its the end of the world; the entire nation is zombies", stated Gardner. "And   are trying to get from the east coast to the west coast". For one shooting scene, Gardner said, "There were 160 zombies, in prosthetic makeup|prosthetics, on set in an amusement park". He said it is "how you present yourself as a zombie that determines how people will react to you" and that "once the contact lenses go in", he thinks "all bets are off". 
 physically attractive actors who usually benefit from their looks as "a little off-putting" after seeing some of them in their character makeup for the first time. 

The zombies in Zombieland were described by the casting director as:
   }}

Harrelson had input into the wardrobe for his character, Tallahassee. "I never worked so long and hard on an outfit in my life," the actor has stated. "What this guy wears is who he is. You want to get a sense of this guy as soon as you see him. So I pick out the necklaces, the sunglasses. But the hat? The minute you see that on Tallahassee, you buy him. Hes real. And hes got a real cool hat".  Harrelsons choice of headwear for Tallahassee came not just down to style, but also to his environmental passions: the distinctive hat is handmade in Brazil by a company called The Real Deal using recycled cargo-truck tarps and wire from old truck tires. {{cite web
| date = September 23, 2009
| author = Michael Parrish DuDell
| title = Woody Harrelson Keeps Zombieland Green With Special Eco Requests
| url = http://www.ecorazzi.com/2009/09/23/woody-harrelson-keeps-zomblieland-green-with-special-eco-requests/
| work = Ecorazzi
}}
http://realdealbrazil.com/zombieland-harrelson-hat.asp
 
 TMZ photographer at New York Citys La Guardia Airport. His defense was that he was still in character and thought the cameraman was a zombie. 

===Effects===
The special effects team worked to create several visual elements. One of these elements is "The Rules for Survival", which appear on-screen as they are related to the audience by Columbus: "Do Aerobic exercise|cardio", "Beware of bathrooms", "Check the back seat", and so forth. The texts are rendered in 3D computer graphics|3-D. "When a previously stated rule becomes relevant—when nature calls, for instance—the relevant text pops up, occasionally getting splattered with blood."  Slate (magazine)|Slates Josh Levin said, "The pop-up bit works precisely because Zombieland unspools like a game—how can you survive a zombie horde armed with a shotgun, an SUV and a smart mouth?" 

==Release==
A trailer of  Zombieland was released on  June 18, 2009.  Distributed by Columbia Pictures, the film was released on October 2, 2009, a week earlier than originally advertised.      

===Box office===
The film debuted at #1 at the box office in North America, with ticket sales of $24,733,155 over its opening weekend averaging about $8,147 from 3,036 theaters, matching its production budget.  It was credited as having the second highest-grossing start on record for a zombie film behind the   claimed the record the following year, grossing over $290 million worldwide.  Zombieland closed on December 13, 2009, with a final gross of $75,590,286 in North America and $26,801,254 in other territories for a worldwide gross of $102,391,540 worldwide.   

CinemaScore polls conducted during the opening weekend, cinema audiences gave Zombieland an average grade of "A-" on an A+ to F scale. 

===Critical response===
  normalized rating to reviews from mainstream critics, the film holds a score of 73 out of 100 based on 31 reviews, indicating "generally favorable reviews".   

Film critic Roger Ebert was surprised by Zombielands ability to be significantly humorous while zombies remained the focus of the film and felt that "all of this could have been dreary, but not here. The filmmakers show invention and well-tuned comic timing". He credited Bill Murrays cameo appearance as receiving the "single biggest laugh" of the year, and gave the film 3 out of 4 stars.      28 Days Weeks Later". 

The films witty use of dialogue and popular culture was also praised by Ty Burr of The Boston Globe, who said the film "makes no claims to greatness" but that what it "has instead—in spades—is deliciously weary end-of-the-world banter";   
Michael Ordona of Los Angeles Times praised director Fleischer for "bring  impeccable timing and bloodthirsty wit to the proceedings".   

Some reviewers saw deeper levels in the plot and cinematography: cinematographer Michael Bonvillain was praised for capturing "some interesting images amid the post-apocalyptic carnival of carnage, as when he transforms the destruction of a souvenir shop into a rough ballet",  while Stephanie Zacharek of Salon.com said "the picture is beautifully paced" and highlighted "a halcyon middle section where, in what could be viewed as a sideways homage to Rebel Without a Cause, our rootless wanderers share a brief respite in an empty, lavish mansion".   

Claudia Puig of USA Today said that "underlying the carnage in Zombieland is a sweetly beating heart", and that "This road movie/horror flick/dark comedy/earnest romance/action film hybrid laces a gentle drollness through all the bloody mayhem".    Entertainment Weeklys Lisa Schwarzbaum concluded, "At the bone, Zombieland is a polished, very funny road picture shaped by wisenheimer cable-TV sensibilities and starring four likable actors, each with an influential following".   

  tries to win over his dream girl, a girl who has been hardened by life, and both feature a theme park. He goes so far as to call the film "an undead Adventureland—a Pride and Prejudice and Zombies for the Facebook generation".   

Time (magazine)|Time magazines Richard Corliss described the film as "an exhilarating ride, start to finish" and reasoned "Edgar Wright and Simon Pegg set a high bar for this subgenre with Shaun of the Dead, but Reese, Werner and Fleischer may have trumped them". "This isnt just a good zombie comedy. Its a damn fine movie, period. And thats high praise, coming from a vampire guy", he stated.   
 Time Out New York characterized the "extra injection of pop-culture neuroticism" as "the one innovation" of the film,    declaring that while Zombieland was funny, it wasnt particularly scary and stated that it "simply isnt as witty as Shaun of the Dead, forever the yuks-meet-yucks standard". 
Similarly, The Globe and Mails Rick Groen said "its far more charming than chilling and way more funny than frightening", though he suggested that Rule No. 32 to enjoy the little things was worth observing for a light comedy.   
Manohla Dargis of The New York Times classified the film as "  minor diversion dripping in splatter and groaning with self-amusement" and lamented the lack of a real plot more concrete than a series of comedy takes on zombie-slaying.   

===Accolades===
{| class="wikitable" style="font-size:95%"
|- style="text-align:center;"
! colspan=4 | List of awards and nominations
|- style="text-align:center;"
! style="background:#ccc;"| Award/ Film festival 
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s) and Nominee(s) 
! style="background:#ccc;"| Result
|-
| Broadcast Film Critics Association  Best Comedy Movie
|
|  
|-
| rowspan="2"| Detroit Film Critics Society 
| Best Supporting Actor
| Woody Harrelson
| rowspan="2"  
|-
| Best Ensemble
| Abigail Breslin Jesse Eisenberg Woody Harrelson Amber Heard Bill Murray Emma Stone
|-
| Empire Awards
| Best Horror
|
|  
|- Golden Schmoes 
| Best Comedy of the Year
|
|  
|-
| Best Horror Movie of the Year
|
|  
|-
| Biggest Surprise of the Year
|
| rowspan="3"  
|-
| Coolest Character of the Year
| (Tallahassee)
|-
| Best Action Sequence of the Year
| (Tallahassee vs. the Amusement Park)
|-
| Most Memorable Scene of the Year
| (Bill Murray Cameo)
|  
|-
| Best T&A of the Year
| Emma Stone
|  
|-
| rowspan="2" | MTV Movie Awards
| Best Scared-As-S**t Performance
| Jesse Eisenberg
| rowspan="2"  
|-
| Best WTF Moment
| (Bill Murray?! A Zombie?!)
|-
| rowspan="2" | Saturn Awards Best Horror Film
|
| rowspan="2"  
|- Best Supporting Actor
| Woody Harrelson
|-
| Sitges Film Festival 
| Audience Award
| Ruben Fleischer
|  
|-
| rowspan="9" | Scream Awards
| Ultimate Scream
|
|  
|-
| Best Horror Movie
|
|  
|-
| Best Scream-Play
| Rhett Reese Paul Wernick
| rowspan="4"  
|-
| Best Horror Actress
| Emma Stone
|-
| Best Horror Actor
| Woody Harrelson
|-
| Best Supporting Actress
| Abigail Breslin
|-
| Best Cameo
| Bill Murray
| rowspan="2"  
|-
| Best Ensemble
| Abigail Breslin Jesse Eisenberg Woody Harrelson Amber Heard Bill Murray Emma Stone
|-
| Best F/X
|
|  
|-
| St. Louis Gateway Film Critics Association
| Best Comedy
|
|  
|-
| Teen Choice Awards
| Choice Movie Actress: Comedy
| Emma Stone
|  
|}

===Home media===
Zombieland was released by Sony Pictures Home Entertainment on February 2, 2010 on Blu-ray Disc and DVD.   The film was released on March 15, 2010 on DVD and Blu-ray in the UK.  Select Best Buy stores sold a special edition on both DVD and Blu-ray with an additional disc featuring two featurettes. It was also released as a film for the PSP Universal Media Disc|UMD.

As of January, 2015, the film has sold 1,935,598 DVDs and 657,958 Blu-ray Discs totalling $39,165,702 and $16,291,929 respectively for a total of $55,457,631 in North America. 

==Sequel and television series==
 
Due to the films success, writers   came up to us after the final cut of the last scene and gave us a hug and said, Ive never wanted to do a sequel in the previous movies Ive done until this one." Wernick said he plans to have Jesse Eisenberg, Emma Stone, and Abigail Breslin to star again with Ruben Fleischer returning as the director and that the writers have "tons of new ideas swimming in their heads". Additionally, they want to make the comedy into an enduring franchise. "We would love to do several sequels", stated Wernick. "We would love to also see it on television. It would make a wonderful TV series". 

Reese and Wernick do not want to reveal any potential Zombieland sequel plot points. They are not planning on an immediate sequel, due to being heavily involved with other writing projects.  The original cast and director are all set to return and Fleischer is enthusiastic about the idea of doing the sequel in 3D.  Posted: Tuesday, December 1, 2009 By Tatiana Siegel  
the films producer, Gavin Polone. "I dont think you want to see Ordinary People in 3-D. But Zombieland is clearly one movie that will benefit from (the technique)."  Woody Harrelson and Jesse Eisenberg confirmed in February 2010 their return for the second installment of the series.  In 2010, Fleischer stated that he was working on the screenplay  and the creators have begun searching for another "superstar cameo". 

In July 2011, Jesse Eisenberg said that hes "not sure whats happening" with the sequel but that the writers are working on a script for Zombieland 2. Eisenberg expressed concern that a sequel would no longer be "relevant".  Woody Harrelson said that he was also hesitant to do a sequel, saying that "Its one thing to do it when it came out real good and it made a lot of people laugh, but then do a sequel? I dont know. I dont feel like a sequels guy."  In October 2011, it was reported that Fox Broadcasting Company and Sony Pictures were considering a television adaption of the series to be aired on CBS, with Paul Wernick and Rhett Reese writing the script, but with the main actors of the original film likely not returning. The television program was planned to begin in fall, 2012. These plans did not come to fruition.  In January 2013, it was revealed that the casting call for the production just went out for the main characters, with a few changes to the movie for the show and the adding of two new characters: Atlanta and Ainsley. 

In March 2013, it was announced that  .  

On May 17, 2013, Rhett Reese, creator of the TV adaptation, announced that Zombieland: The Series would not be picked up to be a series by Amazon. 

On October 1, 2014, it was reported that plans for a theatrical sequel will be moving forward with David Callaham writing the script and Ruben Fleischer returning to direct. It is unknown if Harrelson, Eisenberg, Stone or Breslin will reprise their roles. 

== References ==
 

==External links==
 
 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 