Millionnaires d'un jour
{{Infobox film
| name           = Millionnaires dun jour
| image          = 
| caption        = 
| director       = André Hunebelle
| producer       = 
| writer         = Alex Joffé Jean Halain
| starring       = Gaby Morlay
| music          = Jean Marion
| cinematography = 
| editing        = 
| distributor    = 
| released       = 13 December 1949 (France) 1 February 1952 (USA)
| runtime        = 82 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1949, directed by André Hunebelle, written by Alex Joffé, and starring by Gaby Morlay. The film features Louis de Funès as solicitor. 

== Cast ==
* Gaby Morlay: Hélène Berger, Pierres wife
* Jean Brochard: Pierre Berger, Hélènes husband
* Ginette Leclerc: Greta Schmidt
* Pierre Brasseur: Francis, the gangster
* André Valmy: Marcel, an accomplice of Francis
* Bernard Lajarrige: Philippe Dubreuil, journalist
* Louis de Funès: Philippes advocate

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 