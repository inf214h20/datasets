Nagin (1976 film)
 
 
{{Infobox film
| name           = Nagin
| image          = Nagin.jpg
| image_size     =
| caption        = Album Cover
| director       = Rajkumar Kohli
| producer       = Rajkumar Kohli
| writer         = Siddharth Raj Anand
| narrator       = Mumtaz
| music          = Laxmikant Pyarelal Verma Malik (lyrics) 
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 180 minutes
| country        = India
| language       = Hindi
| budget         =
}}
 horror fantasy thriller film produced and directed by Rajkumar Kohli. It features a huge ensemble cast including Sunil Dutt, Reena Roy, Jeetendra, Feroz Khan, Sanjay Khan, Vinod Mehra, Kabir Bedi, Rekha, Yogeeta Bali and Mumtaz (actress)|Mumtaz. The film was declared a super hit by Box Office India. 
 Telugu as Nagin role. Tamil with Sripriya playing the Nagin role in the film Neeya (film)|Neeya. 

Later  , which fared poorly and generated negative reviews; critics blamed the poor screenplay and direction as the result of the failure.

==Synopsis==
 a myth, when snakes are of certain age, they can assume human form. Thus, the film begins with a male and a female in their human forms, singing and dancing amorously with each other. When the male turns back into a snake, he is shot by a member of a hunting party, who thought the snake was going to attack the woman. The rest of the story deals with the female taking revenge on the group of friends responsible for her lovers death. In the end, Vijay is the only one alive and the nagin dies before killing him and realizes that she was wrong and that her revenge destroyed many lives just like her own life was destroyed. Near death, she sees her beloved in the sky calling out to her. She dies and reunites with him in heaven.

==Cast==

*Reena Roy as Nagin (Female Serpent)
*Sunil Dutt as Prof. Vijay
*Feroz Khan as Raj
*Sanjay Khan as Suraj
*Jeetendra as Nag (Male Serpent)
*Vinod Mehra as Rajesh
*Kabir Bedi as Uday
*Anil Dhawan as Kiran
*Ranjeet as Good Samaritan
*Rekha as Sunita Mumtaz as Rajkumari
*Yogita Bali as Rita
*Prema Narayan as Woman In Forest
*Neelam Mehra as Sheela
*Sulochana Latkar as Rajeshs Mother
*Premnath as Sapera
*Jagdeep as Hukka Bhopali Sapera Jr.
*Aruna Irani as Dancer
*Heena Kausar as Meena
*Roopesh Kumar as Meenas Husband
*Tun Tun as Maid
*Master Bittoo as Anu

==Soundtrack==
All music composed by Laxmikant-Pyarelal, all lyrics written by Verma Malik.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Singer(s) !! Duration
|-
| 1
| "Tere Sang Pyar Main Nahin Todna (Solo)"
| Lata Mangeshkar
| 05:08
|-
| 2
| "Tere Ishq Ka Mujh Pe Hua Yeh Asar Hain"
| Asha Bhosle, Mohammed Rafi
| 06:13
|-
| 3
| "Tere Mera Mera Tera"
| Suman Kalyanpur, Kishore Kumar, Chorus
| 07:21
|-
| 4
| "Tere Sang Pyar Main Nahin Todna (Sad Version)"
| Lata Mangeshkar
| 00:47
|-
| 5
| "Tere Mere Yaraane"
| Lata Mangeshkar, Mohammed Rafi
| 06:24
|-
| 6
| "Tere Sang Pyar Main Nahin Todna (Duet)"
| Lata Mangeshkar, Mahendra Kapoor
| 05:42
|}
 Eternal Sunshine of the Spotless Mind.

==References==

 

==External links==
* 

 
 
 
 
 
 
 


 