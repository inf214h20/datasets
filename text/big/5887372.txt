Beladingala Baale
{{Infobox film
| name           = Beladingala Baale
| image          =
| image_size     =
| caption        = A film of its own kind.
| director       = Sunil Kumar Desai
| producer       = B. S Murali
| writer         = Yandamoori Veerendranath
| screenplay     = Sunil Kumar Desai
| starring       = Anant Nag Suman Nagarkar
| music          = Guna Singh
| cinematography = P. Rajan
| editing        = R. Janardan
| distributor    =
| released       = 1995
| runtime        =
| country        =   India
| language       = Kannada
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 Telugu novel titled Vennelalo Aadapilla by Yandamoori Veerendranath which was translated into Kannada by Vamshi titled Beladingala Baale. It was later serialised in Sudha (weekly magazine)|Sudha, a Kannada weekly magazine. The Kannada and Telugu titles loosely translate to "lady in the moonlight."

Vamshi also wrote dialogues for the movie or rather dialogues from the novel are adapted in the film without any modifications. Beladingala Baale literally means "lady in the moonlight". Story revolves around Revanth(played by Ananth Nag), a Chess grandmaster who is set to find out the identity of his fan who calls him often and whom he refers to as beladingala baale. Beladingala Baale gives Revanth the clues to find her out. How Revanth accomplishes his task forms the gist of the movie.

==Plot details==
  Grandmaster from India and gains worldwide fame when he becomes the world champion by beating Polonski, the reigning world champion. He returns to Bangalore where he works as an assistant executive for Kanara Advertising Service. Revanth is congratulated by all his colleagues including his boss and close friend James (Ramesh Bhat). He is, however, disappointed because he even though he has won an international tournament, he has absolutely no fans, very limited press coverage and no sort of recognition from the society. As he is expressing his disappointment to James, he gets a call from a lady who claims that she is his fan and is calling to congratulate him personally. However, she refuses to reveal her name, instead challenges Revanth to identify her within the next half an hour, with the only clue that her name starts with R. He takes up the challenge and comes up with lots of Indian names starting with R but is not able to identify her. The lady disconnects the call as she gets offended by some of the names suggested by Revanth &mdash; which were actually provided to Revanth by James.

After a couple of days, she calls again, and he apologizes for his behavior. He is mesmerized by her voice and really falls for her. He expresses his desire to meet her. She accepts only if he can find out the number where she is calling him from in the next two minutes. She gives a puzzle for him to solve saying the answer would lead him to her phone number. Revanth solves it badly and goes to a wrong phone booth and gets in trouble. Revanth gets frustrated by his failure to identify her name and number. James suggests him to take the help of a telephone exchange to find from where the calls are coming. They fail to do that, but James meets a girl who is a telephone operator with whom he has an affair.

Revanth ends up in lot of trouble like getting arrested by the police for being in a room where a girl committed suicide, goes on to extreme tasks like measuring the angle of Dr Ambedkars hand in front of Vidhana Soudha all to find out the phone number and address of the mysterious women. "Beladingala Baale" gives Revanth one month to find where she lives. In the meantime, based on the clues in a letter posted by the mysterious lady, Revanth determines that her name is Ramya.

Revanth has an upcoming tournament against Polonski, who is visiting India. However, before playing Polonski Revanth has to win against the local chess master Prabhakar (Avinash). On the day of the match, he is accused of hypnotizing Prabhakar with a lemon in his pants which was actually given by a novelist called Seetharam. He is taking revenge as Revanth refused to write testimony for his book. Revanth is disqualified from the game and has to resign from his job, too. But when he was about to resign, Beladingala Baale calls him, encourages him not to give up, and to fight this allegation. She suggests that he should challenge Prabhakar and six other chess players into playing against him simultaneously, where he would play blindfolded. Revanth decides to try out her suggestion and challenges Prabhakar and six others. He successfully defeats each one of them, his tarnished image is cleared and he is hailed as a true champion.

Meanwhile James tells Revanth that he is getting married to Sulochana (Vanita Vasu), the telephone operator who helped them out while tapping his phone. Revanth tells Ramya over the phone that he is happy for James and expresses a desire to meet Ramya. She tells him to decode her address fast as the deadline is only four days away.

There are only a couple of days left and Revanth gets frustrated by his failure. He has a match against Polonski on the same day Ramyas deadline expires. He has to go to Delhi for the match. Ramya calls him and tells him that he can meet her right way as she is giving a party to her friends at Ashoka hotel and if he can arrive there in five minutes he can catch her. Revanth goes to Ashoka hotel but could only see her hand decorated with jewels in a room filled with her friends. Revanth then calls up James and asks him to come to the hotel with five of his friends on bikes. James arrives there with his friends and Revanth asks them to follow Beladingala Baales friends individually as he is confident that one of them will go to meet Ramya. Revanths plan backfires as his friends themselves get caught by the women while following them. He has to leave for his match to Delhi and he heads to the airport.

While playing against Polonski, Revanth is constantly disturbed as he has only a few hours to decipher Ramyas address. In fact, the moves he makes in the chess game start to solve the puzzles that Beladingala Baale had given him to find her address. He messes up his chess moves but he finds her address there. He realizes from the clues given by her that her house is in JP Nagar, Bangalore. Polonski wins the match, but Revanth exclaims, “You have won this match, but I have won the match of my life". He calls James and tells him to search for posh houses in JP Nagar which are named Ramya. He also calls Sulochana and asks her to find the phone number of anyone whose name or company starts with Ramya in JP Nagar. From this he finds her number as 6137336.

As he rushes out of Bangalore airport, he is met by Ramyas father. Revanth explains to him that there are still couple of minutes for the deadline to expire and he has identified the address and phone number so he has won the bet. Ramyas father asks him to accompany him to their home. On the way home in their car he hands a letter written by Ramya to Revanth. Ramya writes in the letter that she had a rare disease and had only six months to live. She wanted to make friends and not tell anyone about her disease. Revanth is devastated by her letter. Her father invites him inside but instead he turns around and starts walking away without even looking at her. On being asked, he tells James that she was in his imagination and he wants to keep her alive in his imagination, so he will not see her dead body. The film ends as he walks out of her houses entryway.

==Notes==
Throughout the movie, though the heroines face is not revealed, her voice echoes in the minds of the viewers. Manjula Gururaj, background singer of many popular Kannada film songs gave voice for heroine.
==Reviews==
The film got rave reviews from the critics. The commercial response did not quite match the critical reception of the film. The Hindu in its review said, "Beladingala Baale stands out for its screenplay, unusual dimensions and the multilayered plot. At the outset, the film is the story of an Indian chess grandmaster pursued by an anonymous woman caller who claims to be his fan. However, it results in an inner quest for the grandmaster as he literally sets out to search down the elusive caller."

==Awards==

Beladingala Baale was selected for a retrospective of Kannada films at the second Bangalore International Film Festival in 2007. 

===Karnataka state film awards 1995===
*Best Film
*Best Screenplay &mdash; Sunil Kumar Desai
*Special award for artistic contribution &mdash; Manjula Gururaj
*D.R. Seshadri Award for best director &mdash; Sunil Kumar Desai

==External links==
* 
* 

==References==
 

 
 
 
 