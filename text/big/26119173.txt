Velugu Needalu
{{Infobox film
| name           = Velugu Needalu
| image          =
| image_size     =
| caption        =
| director       = Adurthi Subba Rao
| producer       = D. Madhusudhana Rao Athreya
| narrator       = Savitri Kongara Girija Gummadi Gummadi B. Suryakantham
| music          = Pendyala Nageshwara Rao
| cinematography = P. N. Selvaraj
| editing        
| studio         =
| distributor    =
| released       = January 5, 1961
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Tamil version of the film was called Tuya Ulam. 
 

==Plot==
Venkat Ramaiah (SV Rangarao) and Kanaka Durga (Suryakantham) adopted Suguna (Savithri). When they have their own child Varalakshmi, Kanaka Durgas affection towards Suguna diminishes.  Vengalappa (Relangi) adopts suguna. She becomes a doctor and loves Chandram (ANR). Chandram develops Tuberculosis and forces Suguna to marry doctor Raghunath (Jaggayya). Dr. Raghunath expires in an accident. Chandram is cured of TB and returns. Suguna decides to dedicate her life to social work. She convinces Chandram to marry Varalakshmi. Kanaka Durga creates problems in the relations. Suguna corrects them.

==Credits==
===Cast===
* Akkineni Nageshwara Rao ...  Chandram
* Kongara Jaggaiah ...  Raghu Savitri ...  Suguna Girija ...  Varalakshmi Suryakantham ...  Kanaka Durgamma
* S. V. Ranga Rao ...  Rao Bahadur Venkataramaiah
* Relangi Venkataramaiah ...  Vengalappa
* Gummadi Venkateswara Rao
* Rajasulochana
* B. Padmanabham
* E. V. Saroja
* Hemalatha
* Peketi Sivaram
* Sarathi
* Surabhi Kamalabai Sandhya

===Crew===
* Direction: Adurthi Subba Rao
* Associate Director: K. Viswanath
* Producer: D. Madhusudhana Rao
* Production Company: Annapurna Pictures
* Art: S. Krishna Rao and G. V. Subbarao
* Choreography: P. S. Gopalakrishnan and A. K. Chopra.
* Cinematography: P. S. Selvaraj
* Dialogues: Acharya Atreya.
* Editing: M. S. Mani
* Lyrics: Srirangam Srinivasarao and Kosaraju Raghavaiah|Kosaraju.
* Music Director: Pendyala Nageswara Rao
* Playback Singers: P. Susheela, Ghantasala Venkateswara Rao, Jikki and Madhavapeddi Satyam Gandhi Nagar

==Soundtrack==
* "Bhale Bhale"
* "Challani Vennela Sonalu" (Singers: P. Susheela and Jikki) Ghantasala and P. Susheela)
* "Kala Kaanidi Viluvainadi Bratuku" (Lyrics: Srirangam Srinivasarao; Singer: Ghantasala)
* "Oh Rangayo Poola Rangayo" (Singers: Ghantasala and P. Susheela)
* "Paadavoyi Bhaaratheeyudaa" (Lyrics: Srirangam Srinivasarao; Singers: Ghantasala and P. Susheela)
* "Sariganchu Cheerakatti"
* "Siva Govinda Govinda"
* "Chitti Potti Chinnari"

==References==
 

==External links==
*  

 
 
 
 
 
 