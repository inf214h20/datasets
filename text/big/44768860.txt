Shooting Clerks
{{Infobox film
| name           = Shooting Clerks
| image          = Shooting Clerks film poster.png
| alt            =
| caption        = Film poster
| film name      = 
| director       = Christopher Downie
| producer       = Brett Murray  James Noir
| writer         = 
| screenplay     = Christopher Downie
| starring       = Kevin Smith Jason Mewes 
| narrator       = 
| music          = 
| cinematography = Christopher Downie
| editing        = Christopher Downie
| studio         = Auld Reekie Media For the Love of SMod The SMod Squad
| distributor    = 
| released       = TBA
| runtime        = 
| country        = United Kingdom
| language       =  English
| budget         = 
| gross          =  
}}
Shooting Clerks is an upcoming film directed by Christopher Downie and starring Kevin Smith and Jason Mewes.  It is being produced by Auld Reekie Media, For the Love of SMod and The SMod Squad.     

==Summary==
The biopic of how Kevin Smith bankrolled his $27,000 first film with maxed-out credit cards and became the darling of the Sundance Film Festival when Clerks debuted there in 1994.   

==Cast==
* Kevin Smith as Narrator / Larkin Eve   
* Jason Mewes as Wes Jameson
* Brian OHalloran as Elis Heimermann
* Marilyn Ghigliotti as Ali Thomlyn
* Matthew Postlethwaite as Walt Flanagan
* Scott Schiaffo as Don Smith
* Ernest ODonnell as Sergeant Svenning
* Christopher Downie as Mr. Connolly / Shemp Kevin
* Chris Bain as Jason Mewes / Jay

==Production== New Jersey, U.S., and Vancouver, British Columbia. 

==References==
 

==External links==
* 

 
 
 
 
 

 