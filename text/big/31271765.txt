Go, Johnny Go!
{{Infobox film
| name           = Go, Johnny Go!
| image          = Go Johnny Go.jpg
| image_size     = 225px
| caption        = Theatrical release poster by Tom Jung
| director       = Paul Landres
| producer       = Hal Roach, Jr.
| writer         = Gary Alexander Sandy Stewart Chuck Berry
| music          = Leon Klatzkin
| cinematography = Jack Etra
| editing        = Walter A. Hannemann
| studio         = Hal Roach Studios
| distributor    = Valiant Films
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
}}
 Sandy Stewart, and Chuck Berry. The film has also been released as Johnny Melody, The Swinging Story and The Swinging Story of Johnny Melody. 

==Plot summary==
 
This is a fiction account of a young singer Johnny Melody who gets discovered by Alan Freed. It starts with Chuck Berry and Alan discussing the young Melody with Barry intimating that he nearly ended in the slammer. Alan relays the story of how Johnny went from being a orphan with no last name to becoming a major singer.

==Cast==
  
* Alan Freed as Alan Freed
* Jimmy Clanton as Johnny Melody Sandy Stewart as Julie Arnold
* Chuck Berry as Chuck Berry
* Herb Vigran as Bill Barnett
* Frank Wilcox as Mr. Arnold
* Barbara Wooddell as Mrs. Arnold
* Milton Frome as Mr. Martin
 
* Joe Cranston as Band leader
* Martha Wentworth as Mrs. McGillacudy, Johnnys landlady
* Robert Foulk as Policeman at jewelry store
* Phil Arnold as Stagehand William Fawcett as Janitor at radio station
* Dick Elliott as Man waiting for the telephone
* Inga Boling as Secretary Joe Flynn as Usher who fires Johnny
 

;Cast notes
* Jimmy Clanton was involved in the music scene of New Orleans. The first single he released, "Just A Dream", was recorded with the assistance of studio musicians such as Mac Rebennack ("Dr. John, the Night Tripper") and Allen Toussaint. 
* Go, Johnny Go! was the only film appearance of Ritchie Valens, who died shortly after filming it in a plane crash, along with Buddy Holly and The Big Bopper.  The film was released after Valens death. 
* Go, Johnny Go! was Eddie Cochrans third and final appearance in a major picture. A second song, "I Remember", was filmed but cut from the final print.

==Songs==
   Johnny B Goode)- Chuck Berry
# "Ill Take A Long Time" - Jimmy Clanton
# "Jump Children" - The Flamingos
# "Angel Face" - Jimmy Clanton Harvey
# "Mama Can I Go Out" - Jo Ann Campbell
# "Teenage Heaven" - Eddie Cochran Sandy Stewart
# "My Love Is Strong" - Jimmy Clanton
  Memphis Tennessee" - Chuck Berry
# "Jay Walker" - The Cadillacs
# "You Better Know It" - Jackie Wilson
# "Please Mr. Johnson" - Cadillacs Sandy Stewart
# "Little Queenie" - Chuck Berry
# "Ooh My Head" - Ritchie Valens
# "Ship On A Stormy Sea" - Jimmy Clanton
 

==Production== Hal Roach Productions. Stafford, Jeff.   on TCM.com 

==References==
;Notes
 

==External links==
*  
*  
*  
*  

 
 
 
 
 