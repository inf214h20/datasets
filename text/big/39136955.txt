Naa Desam
{{Infobox film name           = Naa Desam image          =   caption        = director       = Kovelamudi Bapayya|K. Bapaiah producer       = K. Devi Vara Prasad S. Venkataratnam  writer         = Paruchuri Brothers  starring  Prabhakar Reddy music  Chakravarthi  cinematography = editing        = Kotagiri Venkateswara Rao  studio    = Pallavi Devi Productions  released       =   runtime        = country        =   awards         = language  Telugu
|budget         = gross          = website        =
}}
 Chief Minister of Andhra Pradesh. 

==Plot== Prabhakar Reddy), a young orphan named Bharath wrestles with life in his young age. Years later, now a young man (N. T. Rama Rao|NTR), Bharath works for Prathap Rao (Kaikala Satyanarayana) and is in love with Mohini (Jayasudha), who will not have anything to do with him due to his lack of ancestry. Bharath is determined to find out who his parents are, and the only one who can help him is the elusive, alcohol-induced and incoherent Kailasham.

==Cast==
*N. T. Rama Rao as Bharath
*Jayasudha as Mohini
*Kaikala Satyanarayana as Prathap Rao
*Giribabu as Kumar Prabhakar Reddy as Kailasham

==Music== Chakravarthi with lyrics written by Veturi Sundararama Murthy|Veturi. 

{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Chalapalilo
| extra1       = S. P. Balasubramaniam, P. Susheela
| lyrics1      = Veturi
| title2       = Eae Chempa Mudhuanadhiro
| extra2       = S. P. Balasubramaniam, P. Susheela
| lyrics2      = Veturi
| title3       = Nenokka Nethru Dhepam
| extra3       = S. P. Balasubramaniam
| lyrics3      = Veturi
 | title4       = Premaku Perantamu
| extra4       = S. P. Balasubramaniam, P. Susheela
| lyrics4      = Veturi
| title5       = Rojulanni Maraei
| extra5       =  S. P. Balasubramaniam
| lyrics5      = Veturi
| title6       =  Uvnadura Devudu
| extra6       = Madhavapeddi Ramesh
| lyrics6      = Veturi
}}

==Trivia== Hyderabad and Ooty. 
*The shooting of the entire film was completed in 19 days, which was a record. 
*NTR was paid   24 lakh for the film, which was a huge renumeration at the time. 
*All song sequences were filmed in just 5 days. Veturi wrote all 6 songs of the movie in just 2 days.  Paruchuri Gopalakrishna dubbed for NTR. 

==References==
 

==External links==
* 

 
 
 
 
 