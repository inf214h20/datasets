Moscowin Kavery
{{Infobox film
| name           = Moscowin Kavery
| image          = 
| caption        = 
| director       = Ravi Varman
| producer       = D. Ramesh Babu
| writer         = Ravi Varman Samantha Harshvardhan Santhanam Sebastian Seeman
| music          = Thaman
| cinematography = Ravi Varman Anthony
| studio         = R Films
| distributor    = Aascar Films
| released       =  
| runtime        = 96 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Samantha in Santhanam and Seeman playing supporting roles. Releasing on 27 August 2010, after nearly three years of production, the film was ultimately panned by critics and the film was a disaster. It never took off.

==Cast==
* Rahul Ravindran as Moscow Samantha as Kaveri Thangavelu
* Harshvardhan as Azhagan (wrongly tagged) Santhanam as Devaraj  Manikandan
* Seeman
* Rohini
* "Minnal" Deepa Suja in an item number
* Ravi Varman in a cameo appearance

==Development== Samantha and quickly said that she would be the heroine of the movie. He also included Rahul Ravindran as hero of the film. Then he tried to start the film in 2007. But there were budget contraints.

==Soundtrack==
{{Infobox album 
| Name = Moscowin Kaveri
| Type = soundtrack
| Artist = Thaman
| Cover = 
| Released = 15 May 2009
| Recorded = 2007-2009
| Genre = Film soundtrack
| Length = 
| Label = R Films
| Producer = Thaman
| Last album = Kick (2009 film)|Kick (2009)
| This album = Moscowin Kavery (2009)
| Next album = Eeram (2009)
}}

Film score and the soundtrack are composed by Thaman. Despite being Thamans first Tamil assignment as a composer, several of his later films had released prior to the release of Moscowin Kavery.
 
{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 
| title1          = Nee Onrum Azhagi
| extra1          = Navin, Rahul Nambiar
| length1         = 
| title2          = Then Muttham 
| extra2          = Naresh Iyer, Suchitra
| length2         = 
| title3          = Athikaalai Pookkal 
| extra3          = Vardhani, Thaman, Shankar Mahadevan
| length3         = 
| title4          = Gramam Thedi Vaada 
| extra4          = Tippu (singer)|Tippu, Ranjith (singer)|Ranjith, Rita 
| length4         = 
| title5          = Gore Gore
| extra5          = Karthik (singer)|Karthik, Suchitra
| length5         = 
}}

==References==
 


==External links==
*  

 
 
 
 
 


 