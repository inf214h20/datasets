The New Girlfriend (film)
{{Infobox film
| name           = The New Girlfriend
| image          = File:The New Girlfriend.jpg
| caption        =Theatrical release poster
| director       = François Ozon
| producer       = Eric Altmayer   Nicolas Altmayer
| screenplay     = François Ozon 
| based on       =  
| starring       = Romain Duris   Anaïs Demoustier   Raphaël Personnaz
| music          = Philippe Rombi 
| cinematography = Pascal Marti 
| editing        = Laure Gardette
| studio         = Mandarin Cinéma
| distributor    = Mars Distribution 
| released       =  
| runtime        = 107 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $3,141,808 
}}
 of the Toronto International Film Festival on 6 September 2014.   

== Cast ==
* Romain Duris as David / Virginia 
* Anaïs Demoustier as Claire 
* Raphaël Personnaz as Gilles 
* Isild Le Besco as Laura
* Aurore Clément as Liz 
* Jean-Claude Bolle-Reddat as Robert 
* Bruno Perard as Eva Carlton  
* Claudine Chatel as Nounou 
* Anita Gillier as Infirmière 
* Alex Fondja as Aide soignant
* Zita Hanrot as Serveuse Restaurant

== Release ==
The New Girlfriend premiered at the 2014 Toronto International Film Festival on 6 September.  The film was later screened at the San Sebastián Film Festival on 20 September 2014, where it won the Sebastiane Award. The jury commented "(Ozon) calls into question the labels and roles of masculinity and femininity" and the jury "appreciates the contribution of this film to move towards a personal liberation, that could reaffirm the identity of every person".  The film was also presented at the Zurich Film Festival on 2 October and the London Film Festival on 11 October.  The film was released theatrically in France on 5 November 2014.

== Critical response ==
Review aggregation website Rotten Tomatoes gives the film a score of 60% based on 5 reviews, with an average score of 5.2 out of 10. 

Writing for  , Justin Chang said "even as he (Ozon) heads down any number of tantalizing if borderline-nonsensical alleyways, Ozon maintains his diabolical wit, his infectious sense of play and his essential affection for his characters" and the film was "powered by beautifully controlled performances from Anaïs Demoustier and Romain Duris". 

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 40th César Awards Best Actor Romain Duris
|  
|- Best Costume Design Pascaline Chavanne
|  
|- 20th Lumières Awards Best Actor Romain Duris
|  
|- Prix Jacques Prévert du Scénario Best Adaptation
|François Ozon
|  
|- San Sebastián Film Festival Sebastiane Award
|The New Girlfriend
|  
|}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 