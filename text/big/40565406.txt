Love Yoou Soniye
 
 
{{Infobox film
| name=Love Yoou Soniye
| image=LUS KVB.jpg
| alt= A girl holding her lover.
| caption=Theatrical publicity poster. Producers knew immediately when setting up the shot—part of the montage it would be the "key art of the film". Karanvir noted, "This one shot tells you the entire story." Firebird Entertainment Pvt Ltd 
| director=Sahil Kohli
| story= 
| screenplay=
| starring= 
| music=Ishq Bector Shree D Rishi Rich Harry Anand
| cinematography= Ashok Salian
| editing=Shiva Bayappa Shekhar Reddy
| studio=Firebird Entertainment Pvt Ltd
| distributor=PVR (India)   B4U (Overseas)
| released=  
| runtime=140 minutes   
| country=India Punjabi 
| budget=2.50cr
| gross= 
}}
Love Yoou Soniye ( ) is a 2013 Punjabi romcom film directed by Sahil Kohli. It is presented by Kewal Sidhu and Mahendra Bohra. This is Karanvirs and Teejays debut as a producer. The film stars Karanvir Bohra and Teejay Sidhu in leads. Other cast includes Vindu Dara Singh, Upasna Singh, Hiten Paintal and Raghu Ram in supporting roles. The motion poster was released on 7 October and the theatrical trailer promo was released on 24 October 2013. The film released on 6 December in India and United Kingdom  

==Plot==
Loveleen (Teejay Sidhu|Sidhu) is an adorable child in the Sidhu family. Her parents love her to the core. During the first day of college she meets a boy named Karanvir Singh Gill (Karanvir Bohra|Bohra) whom she overtakes her car on Karanvirs bike. Filled with vengeance, he decides to teach her a lesson. When Loveleen arrives at her classroom, Karanvir poses as a professor and humiliates her. This too makes Loveleen angry. Karanvir invites his friends n Loveleens best friend Dolly to a pub which first she declines but agrees to come. During dance Karanvir accidentally touches Loveleen, which makes her really angry and she tries to slap him but Karanvir catches her hand while about she was to slap. Next day she complaints her one of a friend to beat up Karanvir but he empowers him after a fight. Then she takes Karanvir on No Mans Land where she accuses Karanvir of raping her. Karanvir slaps her and reveals his feelings towards her that he actually started liking her. Loveleen feels sorry for what she did and tries to apologise to him but Karanvir ignores her to the core. One day angrily she shouts Karanvir of ignoring her and tells that she would never talk to him. Now Karanvir after a long effort apologises her and they becomes best friend. Karanvir expresses his emotions to his elder brother Mahendra Singh Gill (Raghu Ram|Ram). Now as story moves on Karanvir confesses his love towards her and she confesses back too. Eventually Loveleens father (Shivendra Mahal) come to know about their love but agrees to meet his family. In a showroom, Mahendra wants to gift a car to Karanvir and chooses a car which was already been sold. Mahendra verbally argues with manager for the car and tries to slap him but he is hold back by owner. He also verbally fights with the owner and goes out of showroom. Now as Gill brothers head to Sidhus house Mahendra learns that owner was none other than Kewal Sidhu. Sidhu insults the Gill brothers which makes Karanvir furious and challenges to marry Lovleen at any cost. Meanwhile Sidhu arranges her marriage to a NRI Harmeet from Canada (Hiten Paintal|Paintal) Heartbroken Karanvir tries to meet her but held by security guards as he wasnt allowed to enter. During prayer at a Gurudwara he meets Parminder (Manav Gohil|Gohil) and helps him to infiltrate the Sidhus mansion by posing as a Sardaar look. He successfully enters the mansion as cook Parminder. Kewal suspects whether if he met Parminder somewhere. Now Harmeet enters the scene which makes irritation to Karanvir and jealous. During antakshari round Karanvir sings a song which makes realise Loveleen that Parminder is none other than Karanvir. Now he tricks the Sidhu family by framing Harmeet as Homosexual. This creates a humoric situation but Lovleens father disagrees this. Now Lovleen heartbroken and sad confesses her feelings towards Karanvir to her aunt. Now her dad listens this and fills with rage and now wants Loveleen to marry the next day. Now Loveleen pleads Karanvir for solution but Karanvir has no option left but to leave Loveleen forever. Next day Loveleen finds Karanvir is missing from mansion and Parminder for the house. Kewal learns from Karanvirs cellphone (which he left) that Parminder was none other than Karanvir. Loveleen runs to find Karanvir and tells him to run away with her. Karanvir strongly disagrees but Loveleen blackmails her that if he do not agree then she will commit suicide by jumping in a lake. She jumps but Karanvir dives to save her. Meanwhile Sidhu family, enraged, are on hunt for Loveleen and Karanvir. Karanvir brings Loveleen to mansion. Karanvir is beaten up by her uncle and brother. Kewal points a gun to shoot him but Mahendra enters the scene and saves him. Heartbroken Gill brothers left the mansion. Loveleen pleads her dad to leave her. Kewal finally agrees and re unites them.

==Cast==
*Teejay Sidhu as Lovleen Sidhu
*Karanvir Bohra as Karanvir Singh Gill / Parminder
*Raghu Ram as Mahendra Singh Gill (Karanvirs brother)
*Vindu Dara Singh as Lovleens uncle (Chachu)
*Hiten Paintal as Harmeet (Lovleens fiancee) (extended cameo)
*Manav Gohil as real Parminder (cameo)
*Tanya Abrol as Dolly
*Upasna Singh as Loveleens aunt (Chachi)
*Min Sidhu as Dollys brother
*Shivendra Mahal as Kewal Singh Sidhu (Loveleens dad)
*Harry Anand as Harry (Loveleens brother)
*Suyash Kumar Rai as College Rouge
*Manrina Rekhi as Manjeet (Harmeets sister)
*Sharanpreet as Loveleens bhabhi
*Happy Singh as Bunty
*Ram Meher Jangra as Shunty (Karanvirs friend)
*Krishna Bhansal as Sheru (house servant)

==Songs==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ek Pila"
| Harry Anand (lyrics too)
|-
| 2
| "Labda Phira"
| Shree D, Swaroop Khan, Palak Muchhal
|-
| 3
| "Love Karley"
| ShreeD, Palak Muchhal
|-
| 4
| "Moti"
| Ishq Bector, Shree D, Shivangi Bhayan
|-
| 5
| "Relief Ni"
| Ashim Kemson, Isheta Sarckar
|-
| "Love Yoou Soniye"
| Rishi Rich, Juggy D
|}

==Filming & Development==
  as Protagonists of film]]

Shooting was started in January 2013. First schedule was completed in Baroda, Gujarat whereas the second shooting schedule took place in Chandigarh, Punjab. Karanvir sported 2 looks in this film (One with a Sikh attire and second as a college student) . Teejay played his love interest. Raghu Ram played Karanvirs elder brother in the film whereas Vindu D. Singh played Teejays uncle in the film.  Music directors Ishq Bector, Shree D, Rishi Rich, Harry Anand will score the music for the film.  Music director Harry Anand portrayed Teejays brother in the film. Hiten Paintal had an extended cameo of a NRI. Manrina Rekhi, wife of popular music director Rishi Rich played a small part of Harmeets sister in the film. Mostly, the film used most Bollywood film plots. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 