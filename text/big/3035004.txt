Goopy Gyne Bagha Byne
 
 
{{Infobox film
| name           = Goopy Gyne Bagha Byne
| image          = GOOPI GYNE BAGHA BYNE - DVD COVER.jpg
| image_size     =
| caption        = DVD Cover for Goopy Gyne Bagha Byne
| director       = Satyajit Ray
| producer       = Purnima Pictures (Nepal Dutta, Asim Dutta)
| writer         = Satyajit Ray, adapted from Goopy Gyne Bagha Byne by Upendrakishore Roychowdhury
| narrator       = 
| starring       = Tapen Chatterjee, Rabi Ghosh, Santosh Dutta, Harindranath Chattopadhyay,  Jahor Roy, Santi Chatterjee, Chinmoy Ray
| music          = 
| cinematography = Soumendu Roy
| special effects = Rauko Effects Service
| editing        = Dulal Dutta
| sub-titling = Rauko Effects Service
| distributor    = 
| released       = 8 May 1969
| runtime        = 120 minutes
| country        = India Bengali
| budget         = Indian rupee|Rs. 600,000 ($80,000) 
}} Bengali Fantasy fantasy Adventure adventure Comedy comedy film Goopy Gyne Bagha Byne series, followed by a couple of sequels - Hirak Rajar Deshe was released in 1980 and Goopy Bagha Phire Elo, written by Ray, but directed by his son Sandip Ray was released in 1992.
 Sandesh magazine in 1915, with illustrations by Rays grandfather Upendrakishore Ray Chowdhury.   In 1961, after revival of Sandesh, Ray began contemplating the idea of making a film based on that story, and he was partly compelled by his son Sandeep to make a film which was less grim and adult.   This was matched by Rays own desire to make a movie, which unlike his previous movies, would cater to children. Plus this would also give him an opportunity to lace the story with music and dancing, a point his movies producers and distributors were always insisting upon.   Ray managed to convince producers to finance the movie, even though it was clear from the beginning that it will cost a lot of money.  
 Best Feature Best Direction Phil Hall said that the movie comes as a delightful surprise – Ray, it appears, not only possessed a great sense of humor but also enjoyed a stunning talent for musical cinema.    

{{Listen
| image = none
| filename = Gupi Gayen Bagha Bayen - Bhuter rajar bar (Satyajit Ray).ogv
| title = The king of ghosts grants Goopy and Bagha three boons
| alt = The king of ghosts grants Goopy and Bagha three boons scene,
| description = A clipping from the film Goopy Gyne Bagha Byne where the king of ghosts ("bhuter raja") grants Goopy and Bagha three boons. Significantly Satyajit Ray used his own voice for the voice of king of ghosts ("bhuter raja"). In these scene king of ghosts is speaking in rhyme. In 1980, Ray made a sequel to Goopy Gyne Bagha Byne, Hirak Rajar Deshe, in this film too, most of the dialogues exchanged by the protagonists of the film were rhyming.

Satyajit Ray praised S.V.Rau (Rauko) for the ghost dance effect by calling it a technical feat.
}}

==Story== ghosts who are fascinated by their music. The king of ghosts grants them three boons:

* They can get food and clothes whenever needed by clapping their hands,
* They are given a pair of magic slippers with which they can travel anywhere,
* They gain the ability to hold people in awe (literally, their music renders people motionless) with their music.
 Halla (the long lost brother of the king of Shundi) is planning to attack Shundi, after being poisoned with magic potion that makes him evil, given to the king of Halla by his self-centered prime minister. Goopy and Bagha travel to Halla in an attempt at preventing the attack, but are captured instead. Since they have now lost their slippers, they cant escape by magic, but manage to do so instead by strategy. They arrive singing and drumming when the soldiers are about to launch their attack, capturing the king of Halla, who is returned to Shundi. The two brothers are reunited and Goopy and Bagha marry the daughters of the two kings.

==Cast==
  
* Tapen Chatterjee - Gupi Gayen
* Rabi Ghosh - Bagha Bayen
* Santosh Dutta - King of Shundi / King of Halla
* Harindranath Chattopadhyay - Borfi (The Magician)
* Ajoy Banerjee - Visitor to Halla
* Ratan Banerjee - Court singer at Shundi
* Durgadas Bannerjee - King of Amloki
* Binoy Bose - Village elder / visitor to Halla
* Govinda Chakravarti - Goopys father
* Abani Chatterjee - Village elder
* Kartik Chatterjee - Court singer at Shundi / visitor to Halla
 
* Santi Chatterjee - Commander of Halla army
* Gopal Dey - Executioner
* Shailen Ganguli - Visitor to Halla
* Jahor Roy - Prime Minister of Halla
* Tarun Mitra - Court singer at Shundi
* Haridhan Mukherjee - Village elder
* Prasad Mukherjee - King of ghosts / village elder
* Khagen Pathak - Village elder
* Chinmoy Roy - Spy of Halla
* Joykrishna Sanyal] - Court singer at Shundi
* Mani Srimani - Visitor to Halla
 

==Production==

=== Origins ===

Around 1967, Ray started toying with the idea of creating a film based on extra terrestrial creatures on earth, and wrote a screenplay to that effect.  Marlon Brando and Peter Sellers were supposed to star in lead roles in the movie.       However, things did not turn out well between him and Columbia Pictures, and the project was shelved. Unable to make a fantasy movie in Hollywood, Ray decided that he will make one in India instead. He intended to reach a wider audience with this movie, prompted in part by the lukewarm box office performance of his previous movies Kapurush, Mahapurush and Nayak (1966 film)|Nayak.  R.D Bansal, who had produced those movies, became even less enthusiastic when he learnt of the films estimated budget, and, as Ray told Marie Seton in December 1967, he spent the remainder of 1967 scouting for finance, and almost reduced to the same situation as he had been during shooting Pather Panchali.  Finally towards the end of 1967, Nepal Dutta and Asim Dutta of Purnima Pictures agreed to lend some financial help. But since it was not substantial enough to shoot the entire movie in color, Ray decided to do it in black and white and show only the final scene in color. 

===Development and Filming===

 , similar to the drum Bagha plays.]]
 s dancing, the accompanying music using Ghatam, which produces a clattering sound that matches their dapper appearance.]]
 , a folk instrument similar to the one the sweet-seller used in Pather Panchali .]]
 , a drum like the Tabla, because this is regarded as a classical instrument.]]

The films pivotal sequence was a six and a half minute dance, divided into four numbers, performed by the ghosts of the forest in front of Goopy and Bagha. The numbers were intertwined into a phantasmagoria of contrasting styles and moods.  Ray settled on four classes of ghosts keeping in line with the four common classes in the social hierarchy in Hinduism, "since we have so many class divisions, the ghosts should have the same."  Thus came to be included the king and warriors, sahibs, fat people and the common people. Ray decided that the music ought to have some order, form and precision, instead of being just being a wooly, formless kind of thing.  He remembered a South Indian classical form he had once heard in the Delhi Film Festival, which used 12 musical instruments, of which he selected four. He deliberately avoided melody, because melody suggests a kind of sophistication.  Each class, except the sahibs, was played by actors appropriately dressed and made-up, the sahibs were shadow-puppets expertly under-cranked to create the illusion.   The dance culminates with the four classes positioned vertically, with the priests at the bottom and the common people at the top, in contrast with the traditional class hierarchy, Ray imagined the caste system upside down in reaction to the evolving nature of power.  

==Soundtrack and other songs==
# Bhuter Rajar Bor deoa
# Dekhore Nayan Mele
# Bhuter Raja Dilo Bor
# Maharaja! Tomare Selam
# Raja Shono
# Ore Baghare
# Halla Choleche Juddhe
# Ek Je Chhilo Raja...
# O Mantri Moshai
# Ore Baba Dekho Cheye

==Critical Reception==

Critical response was generally positive, with the length and special effects being the main points of criticism. Dennis Schwartz wrote that "Its only fault is that I thought it was too lengthy to hold the attention of children. But the appealing film dazzles with Rays lively score thats carried out very well by the films stars."  Lindsay Anderson said that it had got lovely things in it, but it went on for too long.  A critic, writing for The Guardian, said that this was Satyajit Ray at his least convincing. The Observer wrote that perhaps it would appeal to singularly unfidgety children. The Times observed, "Ray is a true poet of the cinema, but he finds his poetry in everyday reality; in all-out fantasy he seems somewhat prosaic". It was however, a smash hit at home. Ray later wrote to Seton, "It is extraordinary how quickly it has become part of popular culture. Really there isnt a single child who doesnt know and sing the songs".  

==Awards and Honors==

At the 16th National Film Awards.  the movie went on to win 2 huge awards: 
 Best Feature Film
 Best Direction 

It won 4 international awards :

*The Silver Cross at Adelaide
 Auckland Film Festival

*Best film at Melbourne International Film Festival 

*Merit Award in Tokyo 
 Berlin Film Festival.

==Remake==
Goopy Gyne Bagha Byne remade into Hindi language animated film named Goopi Gawaiya Bagha Bajaiya, directed by Shilpa Ranade.  It won and nominated for several international awards.     

==Sequels==

===Hirak Rajar Deshe===
 
Satyajit Ray made a sequel named Hirak Rajar Deshe, which released eleven years after release of Goopy Gyne Bagha Byne.

===Goopy Bagha Phire Elo===
 
Sandip Ray, son of director Satyajit Ray directed another sequel named Goopy Bagha Phire Elo. The film released twelve years after release of Hirak Rajar Deshe.

===Future===
Sandip Ray want to make another sequel to this series. He had received many request to make the fourth Goopy - Bagha movie. Ray said to The Times of India about the plot of fourth film: "Making a Goopy Bagha movie without Tapen and Rabi is unthinkable. The only way I can do a fourth is by taking the story forward and introducing Goopy and Baghas sons," he said. The idea to weave a story around the next generation came from a line from the introductory song Mora dujonai rajar jamai in Hirak Rajar Deshe — "aar ache polapan, ek khan ek khan... (we have one child each)". 

==See Also==

* Gayen
* Pather Panchali

==Notes==
 

==References==
 
*  
* {{cite book|last=Banerjee|first=Deb|title=Powerful and Powerless: Power Relations in Satyajit Ray’s Films
|url=http://kuscholarworks.ku.edu/dspace/bitstream/1808/6006/1/Banerjee_ku_0099M_10685_DATA_1.pdf|year=2009|publisher=University of Kansas}}
* {{cite book|last=Ray|first=Satyajit|title=Our Films, Their Films
|url=http://books.google.co.in/books/about/Our_Films_Their_Films.html?id=wqMHAQAAIAAJ|year=2013|publisher=Orient BlackSwan}}
 

==External links==
*  
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 