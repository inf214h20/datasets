Njangal Santhushtaranu
{{Infobox film
| name           = Njangal Santhushtaranu
| image          = Njangal Santhushtaranu.jpg
| image_size     =
| alt            =
| caption        =
| director       = Rajasenan
| producer       = Kochumon
| writer         = Rajasenan
| narrator       =
| starring       = Jayaram Abhirami
| music          = Ouseppachan
| cinematography = Venu
| editing        =
| studio         =
| distributor    = Aroma International & PJ Entertainments
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Njangal Santhushtaranu is a 1999 Malayalam film, made in Cinema of India|India, starring Jayaram.

==Plot==
Sanjeev (Jayaram) is a police officer who lives with his widowed father and two younger sisters, after death of his mother. He is also a singer in the police troupe along with his partner Salperu Sathanandhan (Jagathy Sreekumar), who is living with his two wives. One day he falls in love with young Geethu (Abhirami), who is the daughter of the director general of the police. The girl is a pampered child, who is spoiled by the love of her parents. Later they get married.

Young Geethu causes misery to Sajeevs sisters, like a stepmother. Once she plays a cruel trick by saying that Sajeev had died. His family is in pain, and when Sajeev returns home, everybody is shocked.  Sajeev slaps Geethu for her deed. She goes back to her home, in anger. Geethus family reveals to her that she is an adopted child, and so she should not feel proud of her legacy. She runs away to an orphanage. Her family visits her and gives her good advice. Geethu returns to her husbands home and starts a new life afresh.

==Cast==
*Jayaram ...  Sanjeevan I.P.S.
*Abhirami ...  Geethu DGP of police
*Jagathy Sreekumar ...  Salperu Sadasivan
*Oduvil Unnikrishnan ...  Sanjeevans Father Janardhanan ...  Idikkula Ittoop
*N. F. Varghese ...  Gurukkal
*Bindu Panicker ...  Saudamini
*Cochin Haneefa ...  Soudaminis Husband
*Seena Antony ...  Savitri, Sanjeevans Sister
*Kaviyoor Ponnamma ... Mother of the orphanage
*Bobby Kottarakkara ...  Policeman
*Kochupreman ...  Policeman

==External links==
*  

 
 
 
 
 
 
 
 


 
 