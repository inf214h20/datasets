Young Malang
 
 

{{Infobox film
| name           = Young Malang
| image          = Young Malang film poster.jpg
| caption        = Young Malang film poster
| director       = Rajdeep Singh
| producer       = Rahulinder Singh Sidhu
| writer         = 
| screenplay     =  
| story          = Manshendra Kailey
| starring       =  
| music          =  
| cinematography = Anshul Chobey
| editing        = Omkarnath Bagadi
| studio         =  
| distributor    = Surya Basic Brother
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Punjabi
| budget         =  
| gross          = 
}}
 Indian romantic comedy film written by Manshendra Kailey, directed by Rajdeep Singh, and produced by Rahulinder Singh Sidhu.    The film stars Yuvraj Hans, Neetu Singh, Vinaypal Buttar, Anita Kailey, Balli Riar and Anjana Sukhani, and debuted 20 September 2013.              Singers Mika, Javed Ali, Shafqat Amanat Ali and three actors in the movie, Yuvraj Hans, Balli Riar and Vinaypal Buttar, have sung the songs in the flick.  The film marks singer Balli Riars debut in Punjabi films.   

==Cast==
* Balli Riar as Jaskaran Brar
* Yuvraj Hans as Jazz
* Vinaypal Buttar as Kashmeer Singh
* Anita Kailey as Annie Neetu Singh as Brittany Begowal
* Anjana Sukhani as Kiran
* Yograj Singh as Maulla Jatt
* Chacha Raunki Ram as Prof. Laathi
* Kiran Kumar
* Balwinder Vicky   
* Mathira Special appearance in item song Lakh Ch Current

==Production==
It was announced in April 2013 that filming had commenced and would continue on primary locations in Chandigarh, Zirakpur, Kasauli, Himachal Pradesh,    with some shooting taking place in Italy.  Producer Rahulinder Singh Sidhu, who made a career change from politics to film production with this project,    explained the spirit behind the films name, stating "Its not just a title; its a state of mind. Those who are young will relate to it and those who are not so young will be able to take a nostalgic trip down the memory lane".   

==Music== Raptilez 1O1, Rap Group that "rocked the nation" with their appearance on Indias Got Talent.      

===Soundtrack===
#Young Malang, Singer: Mika Singh, Lyricist: Diljit Singh    
#Chori Chori, Singer: Javed Ali, Lyricist: Rajdeep Singh  
# Hawa Vich Mehkan, Singer: Vinaypal Buttar, Lyricist: Vinaypal Buttar 
# Ishq Di Kitaab, Singer: Gurmeet Singh, Lyricist: Sandeep Singh 
# Sapno Ka Saya, Singers: Vicky Bhoi, Yuvraj Hans , Vinaypal Buttar, Raptilez 1O1, Lyricist: Gurjinder Dhillon, Raptilez 1O1 
# Rabb Janda Hai, Singer: J Riaz, Lyricist: Gurjeet Khosa 
# Ishq Di Kitaab, Singer: Navraj Hans, Lyricist: Sandeep Singh 
# Doli, Singer: Vicky Bhoi, Lyricist: Gurjinder Dhillon 
# Preetma, Singers: Vicky Bhoi, Shallu Jain, Lyricist: Prem Chand 

==Reception==
The first review to come out for the film was mixed where the reviewer gave the film 3 stars. The reviewer wrote "Technically, the film will surprise you", offering that the film was "on par with any Bollywood film". They noted that the films producer, "has left no stone unturned in giving the film a grand look and together as a team, producer and director have employed some of the best technicians for the film." The reviewer concluded, "Overall, Very Good!"   

==References==
 

==External links==
*   
*   at CinePunjab.com
*  
*  

 
 
 
 
 
 
 
 
 