Intermission (film)
 
 
{{Infobox film
| name           = Intermission
| image          = Intermission ver2.jpg
| caption        = Theatrical release poster
| alt            = John Crowley
| producer       = Stephen Woolley Neil Jordan Alan Moloney
| writer         = Mark ORowe
| narrator       =
| starring       = Colin Farrell Cillian Murphy Kelly Macdonald Colm Meaney Shirley Henderson John Murphy
| cinematography = Ryszard Lenczewski
| editing        = Lucia Zucchetti
| studio         =
| distributor    = Buena Vista Pictures (UK) DreamWorks Pictures (US)
| released       =  
| runtime        = 105 minutes  
| country        = Ireland
| language       = English
| awards         = Won 6, Nominated For 10
| budget         = $5,000,000  . .    
| gross          = $4,856,298 
}}
 John Crowley and written by Mark ORowe. The film, set in Dublin, Ireland, has been shot in a documentary-like style, and contains several storylines which cross over one another.

== Plot ==
John and Deirdre are a recently separated young couple with an extended group of interrelated friends, family and co-workers.
  Garda Detective Jerry Lynch presents himself as a saviour who fights the "scumbags" on Dublins streets, and enlists the help of Ben Campion , an ambitious film-maker and the bane of his "go-softer" boss, who considers Lynch too nasty a subject to be shown on a mainstream “docusoap” series on Irish television.

While shooting a scene about a traffic accident, Ben is told to focus his attention on Sally, Deirdres sister, who helped the passengers after the double-decker bus they were on crashed. Sally is deeply insecure about her looks who grows bitter when Deirdre flaunts her new boyfriend, Sam, a middle-aged bank manager who has left his wife of 14 years, Noeleen, leaving her to question her own self-worth as a woman and wife.

John is utterly lost without Deirdre and is determined to win her back. He gets involved in an absurd plan along with Mick, the driver of the bus that crashed, and Lehiff. They kidnap Sam and force him to go to his bank to get money for a ransom. Things go awry when Sam, who has the money, is assaulted by an enraged Noeleen on the street and a couple of police officers appear. Mick and John flee the scene without their money.

Mick, who lost his job after the bus crash, becomes obsessed with taking revenge on Philip, the boy who had lobbed the stone into the bus windscreen. Things do not go quite his way, and he ends up learning a bitter lesson. Detective Lynch corners Lehiff in an open field, and the scene is set for a confrontation that ends in an unexpected way.

As the credits roll, Noeleen and Sam are back together in their house watching television. She is sitting purposely on the remote control and bullying him into changing the channels by hand.
 

== Cast ==
* Colin Farrell as Lehiff, a petty criminal with a penchant for getting into trouble with the law
* Kelly Macdonald as Deirdre
* Cillian Murphy as John, Deirdres former boyfriend
* Colm Meaney as Detective Jerry Lynch
* Shirley Henderson as Sally David Wilmot as Oscar
* Deirdre OKane as Noeleen
* Michael McElhatton as Sam, a middle-aged bank manager
* Tomás Ó Súilleabháin as Ben Campion, an ambitious film-maker
* Brian F. OByrne as Mick, the bus driver
* Ger Ryan as Maura, the mother

== Production ==

=== Technical details ===
 IFCO Rating: 15PG (cinema) / 15 (video)
* BBFC Rating: 18
* MPAA Rating: R

== Reception ==
The movie earned €2.5 million at the Irish box office, briefly becoming the most successful independent Irish film. Loach film breaks Irish box-office records
Kerr, Aine. The Irish Times (1921–Current File)   8 August 2006: 3. 

The film was well received by critics. Rotten Tomatoes gave the film an aggregate rating of 73% based on 93 reviews, with the critical consensus describing the film as "An edgy and energetic ensemble story". 

Noted critics Roger Ebert and Richard Roeper gave the film good reviews.  
Roeper described it as "a likable film about nasty people". 

Patrick Condren became the first Irishman to be nominated at the Taurus World Stunt Awards following his work in the film. In the biggest single stunt ever filmed in Ireland, a double-decker bus was flipped 20m into the air. 

== Box office ==
Intermission took $5 million to make, but it didnt do well in the box office, grossing just $4.8 million worldwide.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 