Princess of Mars
{{Infobox Film name           = Princess of Mars image          = princessofmarsdvdcover.jpg image_size     = director  Mark Atkins producer       = David Michael Latt David Rimawi Paul Bales writer  Mark Atkins based on       =   (uncredited) starring       = Antonio Sabato, Jr. Traci Lords music          = cinematography = editing        = distributor    = The Asylum released       =   runtime        = 90 Minutes country        = US language       = English budget         = gross          =
}} John Carter, which is also an adaptation of the novel. In Europe, the film was released with the title The Martian Colony Wars.

==Plot== John Carter (Antonio Sabato, Jr.) is a modern-day United States Army|U.S. Army sniper serving in Afghanistan, wounded in the line of duty and used in a teleportation experiment wherein he is transferred to Barsoom, here depicted outside our solar system. On this world, Carter exhibits the ability to leap amazing distances. Initially enslaved and held against his will on a chain and collar by the Tharks, he earns a rank among them and later saves a rival groups princess, the human-looking Dejah Thoris (Traci Lords), from death.
 Afghan mercenary who had betrayed him. When Sarka escapes, Carter helps Tarkas kill Hajus and become the new leader of the Tharks.

Captain Carter then learns that Dejah Thoris has fled to the planetary air-cleaning station that keeps Barsoom habitable, which Sarka damages, causing the atmosphere to deteriorate. John Carter and Sarka face each other in a duel; but Sarka is killed by an insect during the fight. After Carter and Dejah Thoris re-activate the station, Carter is returned to Earth, where he declines to tell his superiors about his adventures for fear they will colonize Barsoom, and returns to military duties while hoping to return to Barsoom.

== Main cast == John Carter
* Traci Lords – Dejah Thoris
* Matt Lasky – Tars Tarkas
* Chacko Vadaketh – Sarka / Sab Than
* Mitchell Gordon – Tal Hajus
* Noelle Perris – Sola

==Production==
This film makes extensive use of the Vasquez Rocks for its alien landscape, appearing throughout the film as different locations.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 