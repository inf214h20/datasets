The Harvest Month
{{Infobox film
| name           = The Harvest Month
| image          = 
| caption        = 
| director       = Matti Kassila
| producer       = Mauno Mäkelä
| writer         = Matti Kassila F. E. Sillanpää
| starring       = Toivo Mäkelä
| music          = 
| cinematography = Esko Nevalainen Nils Holm
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

The Harvest Month ( ) is a 1956 Finnish drama film directed by Matti Kassila. It was entered into the 1957 Cannes Film Festival.   

==Cast==
* Toivo Mäkelä - Viktor Sundvall
* Emma Väänänen - Saimi Sundvall
* Rauni Luoma - Maija Länsilehto
* Aino-Maija Tikkanen - Tyyne Sundvall
* Rauni Ikäheimo - Iita
* Senni Nieminen - Hanna Nieminen Heikki Savolainen - Mauno Viljanen
* Tauno Kajander - Taave
* Sylvi Salonen - Actress
* Topi Kankainen - Alpertti
* Pentti Irjala - 1# man at sauna
* Olavi Ahonen - 2# man at sauna
* Väinö Luutonen - Väinö Länsilehto
* Keijo Lindroos - Dancing man
* Mauri Jaakkola - Viktors friend
* Severi Seppänen - Doctor
* Kaija Siikala - Viktors first love

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 