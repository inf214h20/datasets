G.I. Jane
 
{{Infobox film name           = G.I. Jane image          = Gijane.jpg caption        = Theatrical release poster director       = Ridley Scott producer       =   screenplay     =   story          = Danielle Alexandra starring       =   music  Trevor Jones editing        = Pietro Scalia cinematography = Hugh Johnson studio         =   distributor    =   budget         = $50 million gross          = $97,169,156    released       =   runtime        = 124 minutes country        = United States language       = English
}} Special Warfare Group.

==Plot==
A  s, the military will integrate women fully into all branches of the Navy.

The first test is the training course of the (fictional) U.S. Navy Combined Reconnaissance Team (similar to U.S. Navy SEAL BUD/S). Senator DeHaven hand-picks topographical analyst Lieutenant Jordan ONeil (Demi Moore), because she is physically more feminine than the other candidates.
 hell week"). Command Master Chief John James Urgayle (Viggo Mortensen) runs the brutal training program that involves 20-hour days of tasks designed to wear down recruits physical and mental strength, including pushing giant ship fenders up beach dunes, working through obstacle courses, and hauling landing rafts.
 SERE training, the Master Chief ties her to a chair with her hands behind her back. Then he grabs hold of ONeil and slams her through the door and then he grabs her from the floor she fell on. And then he dunks her head in ice cold water repeatedly in front of the other crew members. ONeil fights back, and is successful in causing him some injury despite her immobilized arms. In so doing, she acquires respect from him, as well as from the other trainees.
 media learn fraternizing with women. ONeil is told that she will be given a desk job during the investigation and, if cleared, will need to repeat her training. She decides to "ring out" (ringing a bell three times, signaling her voluntary withdrawal from the program) rather than accept a desk job.
 voided and ONeil restored to the program.
 operational readiness exercise, is interrupted by an emergency situation that requires the CRT trainees support. The situation involves a reconnaissance satellite powered by weapons-grade plutonium that fell into the Libyan desert. A team of United States Army Rangers|U.S. Army Rangers is dispatched to retrieve the plutonium, but their evacuation plan fails, and the trainees are sent to assist the Rangers. The Master Chiefs shooting of a Libyan soldier to protect ONeil leads to a confrontation with a Libyan military patrol|patrol. During the mission, ONeil, using her experience as a topographical analyst, realizes when she sees the teams map that the Master Chief is not going to use the route the others believe he will in regrouping with the others. She also displays a definitive ability in leadership and strategy while rescuing the injured Master Chief, whom she and McCool pull out of an explosives-laden "kill zone." With helicopter gunships delivering the final assault to the defenders, the rescue mission on the Libyan coast is a success.
 Navy Cross and a book of poetry containing a short poem, "Self-pity", by D. H. Lawrence, as acknowledgment of her accomplishment and in gratitude for rescuing him.

==Cast==
* Demi Moore as Lieutenant Jordan ONeil
* Viggo Mortensen as Command Master Chief John James Urgayle
* Anne Bancroft as Sen. Lillian DeHaven
* Jason Beghe as Lieutenant Commander Royce
* Daniel von Bargen as Theodore Hayes Scott Wilson as Captain Salem
* John Michael Higgins as Chief of Staff Kevin Gage as Instructor Max Pyro
* David Warshofsky as Instructor Johns
* David Vadim as Sergeant First Class Cortez
* Morris Chestnut as Lieutenant McCool
* Josh Hopkins as Ensign F. Lee Flea Montgomery
* Jim Caviezel as Slov Slovnik
* Boyd Kestner as Lieutenant Wick Wickwire
* Dimitri Diatchenko as Trainee
* Angel David as Newberry
* Stephen Ramsey as Stamm

==Release==

===Reception===
 
 
G.I. Jane received mixed reviews from critics, where it currently holds a 55% rating on Rotten Tomatoes based on 31 reviews. Demi Moore won the Razzie Award for Worst Actress for her performance in the film.

===Box office=== grossing $11,094,241 its opening weekend, playing at a total of 1,945 theaters. In its second weekend the film stayed at #1, grossing $8,183,861 and playing in 1,973 theaters. In the end the film played in a widest release of 2,043 theaters and grossed $48,169,156 domestically, falling slightly short of its $50,000,000 budget.  The film made a total of $97,169,156 worldwide.

===Home media=== theatrical trailer. It was released on Blu-ray Disc|Blu-ray on April 3, 2007 with no extra features aside from trailers for other movies.  The film was also released on Laserdisc; this release featured an audio commentary by director Ridley Scott.  The film grossed $22,122,300 in rentals. 

==See also==

*List of films featuring the United States Navy SEALs

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 