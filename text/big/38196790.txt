Parai Zameen
{{Infobox film
| name           = Parai Zameen
| image          =
| image_size     =
| caption        =
| director       = Shaikh Hassan
| producer       = A. G. Mirza
| writer         = Dukhi Prem Nagri
| lyrics         =
| starring       = Talib Ali
| music          = Ghulam Nabi Abdul Lateef
| cinematography = Mehboob
| editing        =
| released       = 14 August 1958
| runtime        =
| country        = Pakistan Sindhi
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}

Parai Zameen is a Pakistani film released on 14 August 1958. It was directed by Shaikh Hassan, produced by A. G. Mirza and starred Talib Ali.

==See also==
* Sindhi cinema
* List of Sindhi-language films

==Further reading==
* Gazdar, Mushtaq. 1997. Pakistan Cinema, 1947-1997. Karachi: Oxford University Press.

==External links==
*  

 
 
 

 