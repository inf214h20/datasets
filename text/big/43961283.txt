Commercials: Unplugged
{{Infobox film
| name           = Commercials: Unplugged
| image          = 
| caption        = 
| director       = Andrea Molina
| producer       = Brian Herrera
| studio         = 
| screenplay     = Erick Carcamo, John Williams, Lissa Garcia
| starring       = Enrique Alejandro, Sarah Harrison, Ernesto Roche, Martin Fontanes
| cinematography = 
| editing        = 
| music          = 
| runtime        = 
| country        = United States
| language       = English
}}
Commercials: Unplugged is a 2014 American comedy short film, written by Erick Carcamo, and directed by Andrea Molina. Produced by the Florida Film Institute, INC. 

The film was screened on May 21, 2014 at the MDC-N in Miami, and it won for Best Screenplay of the year. 

==Plot summary==
Commercials: Unplugged, is a slapstick comedy about five workers of the Commercials Distribution Headquarters (CDH) who are choosing one out of three commercials to air on T.V. These arent just regular commercials, though. These commercials are parodies of real-life products. 

==Production==
The film was filmed in Miami, Florida, at the Robert Morgan Educational Center.

==References==
 

 
 
 
 
 

 