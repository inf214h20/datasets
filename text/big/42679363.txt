Love, Lies and Seeta
{{Infobox film
| name           = Love, Lies and Seeta
| image          = Love Lies Seeta.jpg
| caption       = Movie poster
| director       = Chandra Pemmaraju
| producer       = Chandra Pemmaraju
| writer         = Chandra Pemmaraju
| starring       = {{Plainlist|
* Melanie Kannokada
* Arjun Gupta
* Lavrenti Lopes
* Michael Derek
}}
| music          = Dan Omelia Robopop Wired Beats
| cinematography = Lukasz Pruchnik
| editing        = Kenneth Fabritius
| studio         = 
| released       =   India International Film Festival (IIFF) of Tampa Bay|IIFF, USA   (India)
| country        = United States
| language       = English
| runtime        = 100 minutes
}}

Love, Lies and Seeta is a 2012 Indian-American independent romantic comedy film written, produced, directed by Chandra Pemmaraju. Starring Melanie Kannokada,    Arjun Gupta, Lavrenti Lopes,    Michael Derek, it is built around three distinctly different guys all of whom pine for the same beautiful girl.  The movie premiered at the India International Film Festival (IIFF) of Tampa Bay in Florida and went on to play in various international film festivals before having a limited theatrical release in India on May 18, 2012 by Cinemax.    The film marks Melanie Kannokadas debut as lead in a feature film.

==Plot==
Love Lies and Seeta is a romantic musical comedy that envelopes four characters and their friends as they experience their 2010 summer in New York City. The movie follows the lives of the three male leads: “The Bollywood hero,” Arjun Gupta, “The Rock Star,” Michael Derek, and “The Hipster Geek,” Lavrenti Lopes who have all independently met the beautiful Seeta (Melanie Kannokada) at different stages in their lives. As the three men’s friendships grow, a chance encounter with Seeta makes them all realize that they have fallen in love with the same woman. They reach out to their friends outside the group to cope with falling in love with Seeta. The love triangle envelops the characters and their friends. The city backdrop depicts a New York Summer and makes its urban-natural beauty an important supporting character.

==Cast==
*Melanie Kannokada as Seeta 
*Arjun Gupta as Rahul
*Lavrenti Lopes as Bhavuk
*Michael Derek as Tom
*Ryan Vigilant as Tom Cruise
*Leah Kavita as Ramya
*Caroline Korale as Caroline
*Aaron Katter as Bud
*Daniel Wilkinson as Young John McKinsey, and
*Rob Byrnes as John McKinsey

==Production==
The film was shot completely in Manhattan and Brooklyn in NYC with an ensemble Indian American cast. The movie was produced in low-budget film|micro-budget and shot on location using sync sound. The movie is first of its kind where it was funded and supported by film enthusiasts. By using crowd sourcing method the producers raised all the money for its production.  

===Music===
The film features original music by various Independent musicians including Robopop, Timblane, New Life Crisis, Matt Hartke, La Dauphine and Wiredbeats.

==Release==
On May 18, 2012, Cinemax Motion Pictures Pvt Ltd, India released the film theatrically on a limited basis in the Indian sub-continent.  The film will be distributed by NYC based The Vladar Company in digital North American regions.

==Accolades==
Love Lies and Seeta was the closing night feature at the India International Film Festival (IIFF) of Tampa Bay in Florida in February 2012, and was opening night feature film in Art of Brooklyn International Film Festival in NY, August 2012. It played in various film festivals such as, NewFilmmakers Film Festival, New York; Riverside International Film Festival, CA; won the Award of Merit in Indie Fest, USA; Won an Honorary Mention Award in Los Angeles New Wave Indie Fest  and was a part of NFDC Film Bazaar at International Film Festival of India, Goa. The movie also played in Mumbai Film market and was nominated for the Best Film, Best Direction, Best Cinematography and Best Actor categories at the World Music & Independent Film Festival 2012, Washington D.C.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 