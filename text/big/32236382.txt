Bekaraar
{{Infobox film
| name = Bekaraar
| image = 
| writer = 
| starring = Sanjay Dutt Padmini Kolhapure Mohnish Bahl
| director = Rajendra Prasad V.B.
| producer = 
| music =  Laxmikant-Pyarelal
| lyrics = 
| released = 1983
| language = Hindi
}}

Bekaraar is an Indian film directed by Rajendra Prasad V.B. and released in 1983. The movie stars Sanjay Dutt, Padmini Kolhapure, Mohnish Bahl, Supriya Pathak. 

==Characters==
* Sanjay Dutt ... Shyam
* Padmini Kolhapure ... Sundari Gupta
* Mohnish Bahl ... Pradeep
* Supriya Pathak... Nisha

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bekaraar Kiya"
| Amit Kumar, Shailender Singh
|-
| 2
| "Door Door Kahin Majhi Pukara"
| Mahendra Kapoor
|-
| 3
| "Gaadi Chhuk Chhuk Chalti Hai"
| Asha Bhosle
|-
| 4
| "Main Ne Yeh Faisla Kar Liya Hai"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Siyapati Ramchandra Ki Jai"
| Asha Bhosle
|-
| 6
| "Tum Chale Aaye Ho Chalo Khair Hui"
| Kishore Kumar, Asha Bhosle
|}

==External links==
*  

 
 
 
 

 