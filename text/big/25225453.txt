The Mandarin Mystery
{{Infobox film
| name           = The Mandarin Mystery
| image          =The Mandarin Mystery.jpg
| image_size     =
| caption        =
| director       = Ralph Staub
| producer       = Nat Levine (producer) Victor Zobel (associate producer)
| writer         = Frederic Dannay (story) John Francis Larkin (writer) Manfred Lee (story) Gertrude Orr (writer) Rex Taylor (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack A. Marta
| editing        = Grace Goddard
| distributor    = Republic Pictures
| released       = 23 December 1936
| runtime        = 66 minutes (USA) 53 minutes (USA, edited version)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 
The Mandarin Mystery is a 1936 American film directed by Ralph Staub, loosely based on The Chinese Orange Mystery, a novel featuring detective character Ellery Queen.

== Plot summary ==
 

== Cast ==
*Eddie Quillan as Ellery Queen
*Charlotte Henry as Josephine Temple
*Rita La Roy as Martha Kirk
*Wade Boteler as Inspector Queen
*Franklin Pangborn as Mellish, the hotel manager George Irving as Dr. Alexander Kirk
*Kay Hughes as Irene Kirk William Newell as Detective Guffy
*George Walcott as Donald Trent
*Edwin Stanley as Howard Bronson
*Edgar Allen as Detective
*Bert Roach
*Richard Beach as Reporter
*Monte Vandergrift as Detective
*Grace Durkin as Girl on Street Corner
*Mary Russell as Girl on Street Corner
*Mary Bovard as Girl At Cocktail Bar
*June Johnson as Girl At Cocktail Bar

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 