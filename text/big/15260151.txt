Guests Who Arrived on the Last Train
{{Infobox film name           = Guests Who Arrived on the Last Train image          = Guests Who Arrived on the Last Train.jpg caption        = Poster for Guests Who Arrived on the Last Train (1967)
| film name = {{Film name hangul         =       hanja          = 막 로 온 손님들 rr             = Makcharo on sonnimdeul mr             = Makch‘aro on sonnimdŭl}} director       = Yu Hyun-mok  producer       = Seong Dong-ho writer         = Lee Sang-hyun Lee Eun-seong starring       = Lee Soon-jae Moon Hee music          = Han Sang-ki cinematography = Min Jeong-sik editing        = Hyeon Dong-chun distributor    = Dong Yang Films Co., Ltd. released       =   runtime        = 104 minutes country        = South Korea language       = Korean budget         =  gross          = 
}}
Guests Who Arrived on the Last Train ( ) is a 1967 South Korean film directed by Yu Hyun-mok. It was presented at the 6th Panama International Film Festival. 

==Synopsis==
A literary drama about a young man dying of lung cancer and his circle of friends. 

==Cast==
* Lee Soon-jae 
* Moon Hee
* Seong Hun
* Nam Jeong-im
* Kim Seong-ok
* Ahn In-sook
* Han Chan-ju
* Jeong Min
* Kim Ung
* Seong So-min

==Notes==
 

==Bibliography==
*  
*  
*  

 
 
 
 


 