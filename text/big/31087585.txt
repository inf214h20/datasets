Aaj Ka Goonda Raj
{{Infobox film
| name           = Aaj Ka Goonda Raj
| image          = AajKaGoondaRajfilm.png
| image_size     =
| caption        = Promotional Poster
| director       = Ravi Raja Pinisetty
| producer       = N.N. Sippy
| writer         = Anees Bazmee
| narrator       =
| starring       =  
| music          = Anand Milind
| editing        = Vellaiswamy
| studio         =
| distributor    =
| released       = 1992
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}
 Hindi film starring Chiranjeevi and Meenakshi Seshadri in the lead roles. Its a remake of Telugu film Gang Leader and gave the second hit for Chiranjeevi in Bollywood.

==Plot==
Raja, though he is righteous, honest, tough, a daredevil and educated, was an unemployed young man. With no work on hand he squanders his time with his four friends. This naturally upsets his family members: his dadi and elder brothers Amar and Ravi who care and worry for him.

Constantly troubled by lack of money, the family has lots of problems. Ravis pending IAS examination was the core problem they were facing. To raise money for the examination, Raja and his friends take up a job, one that entailed getting rid of a tenant who had illegally settled in a house. Raja had an easy time getting rid of Shalu. But from then on, Shalu creates hell for him and moves into his house and life permanently. Tejpal and Nagpal were power brokers. They were very influential and intimidating. They succeed in getting Ritu married to Ravi, now an IAS officer, on the threshold of becoming a collector. A perfect move thought the villains — Saxenas sister married to Rajas brother. Raja revolts but is cornered. He has many clashes with the duo. In one of these clashes Rajas friends and eldest brother Amar are murdered. All is lost until Raja and Ravi discover the truth about Tejpal and Nagpal. Raja and Ravi decide to end the Gundaraj.

==Cast==
* Chiranjeevi...Raja
* Meenakshi Seshadri...Shalu Sharma
* Raj Babbar...Ravi
* Dalip Tahil...SP Saxena
* Sharat Saxena...Nagpal
* Satish Shah...Jailer 
* Rakesh Bedi...Govind Ahuja (Guddu)
* Dina Pathak...Grandmother
* Tinnu Anand...Home Minister
* Prem Chopra...Tejpal
* Geetha (actress)|Geetha... Ritu
* Ravi Teja
* Parikshit Sahni...Amar
* Sumalatha...Ritu Saxena
* Kunika...Chanda
* Dan Dhanoa...Police Inspector

==Soundtrack==
The album featured several hit songs composed by Anand-Milind and written by Sameer. The album managed top sales in 1992 to feature in the list of best selling albums. 
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ek Pappi De De"
| Abhijeet Bhattacharya|Abhijeet, Sadhana Sargam
|-
| 2
| "Its A Challenge"
| Amit Kumar
|-
| 3
| "Lashkara Lashkara"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Pehle Bhi Roz"
| Abhijeet, Sadhana Sargam
|-
| 5
| "Tota Mera Tota"
| Abhijeet, Sadhana Sargam
|-
| 6
| "Tum Mujhe Ache"
| Abhijeet, Alka Yagnik
|}

==References==
 

==External links==
*  

 

 
 
 
 
 


 