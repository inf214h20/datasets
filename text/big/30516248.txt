Bhaktha Kuchela (1936 film)
{{Infobox film
| name           = Bhaktha Kuchela
| image          =
| image size     =
| alt            =
| caption        =
| director       = K. Subramaniam
| producer       = Krishnaswamy
| writer         =
| narrator       =
| starring       = Papanasam Sivan S. D. Subbulakshmi Susheela
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Bhaktha Kuchela  is a 1936 classic film in Tamil language directed and produced by Lawyer turned film maker K. Subramaniam) about the tale of friendship between the poor Brahmin Kuchela Papanasam Sivan who had 27 children and Lord Krishna (S. D. Subbulakshmi). Subramanyam cast Subbulakshmi in a double role as a male (Lord Krishna) and a female (Susheela, Kuchelas wife). Such casting was the first of its kind in Tamil film history and even Indian cinema.

==External links==
*http://www.thehindu.com/arts/cinema/article77029.ece

 
 
 
 


 