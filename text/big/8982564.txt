Magic Brush
{{Infobox film
| name = Magic Brush
| image = MagicBrushMaLiang.jpg
| caption =
| director = Jin Xi (靳夕)
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1954, 1955
| runtime =  20 mins
| country = China Mandarin
| budget =
| preceded by =
| followed by =
| tv_rating =
}}
 Chinese animated stop-motion film produced by the Shanghai Animation Film Studio.  There were two versions of the film.   In 1954 the first film was called "Ma Liang and his Magic Brush" (Chinese: 神笔马良).  In 1955 the second film was called "Magic Brush" (Chinese: 神笔).  They are also interchangeably referred to as the "Magic Pen" or "Magical Pen".

==Story==
The story is based on a folklore story. In a village, there lived a boy called Ma Liang, whose family was so poor that he could not go to school. But he loved painting and worked very hard at it. One day, he got a brush, which had a magical power—if you draw anything with it, it will become reality.
So he started to help everyone in his village with his magic brush. This information reached a local officer. He put Ma Liang into the prison and confiscated his brush.
The officer found that the brush did not have any power when it was in his hands, so he forced Ma Liang to draw a golden mountain. Ma Liang agreed and drew a golden mountain, but he also drew a big ocean around it. After the officer and his servants sailed across the sea, Ma Liang waved his brush and the officers sank into the water. 

==Adaptations==
The story has been readapted a number of times by Chinese authors, common versions include the story of the same name from author Han Xing as well as Hong Xuntao.  There are also American versions in "Tye May and the Magic Brush" by Molly Bang.

==Awards==
* Won the outstanding film award in the 1957 Poland Warsaw International Film Festival childrens competition.
* Won the childrens entertainment films award at the Venice, Italy 8th International Childrens Film Festival
* Won the silver award at the Damascus, Syrian 1st International Film Festival Expo
* Won the outstanding childrens film award at the Belgrade Yugoslavia 1st International Childrens Film Festival
* Recognized at the Canada 2nd Stratford International Film Festival awards
* In 1955 awarded for outstanding film by Chinas Ministry of Culture.

==See also==
*List of stop-motion films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 