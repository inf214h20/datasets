Trapped Ashes
 
{{Infobox film
| name = Trapped Ashes
| image = Trapped Ashes.jpg
| caption =
| director = Sean S. Cunningham Joe Dante Monte Hellman Ken Russell John Gaeta
| producer = Independent Film Fund Cinema Investment
| writer = Dennis Bartok
| starring = Jayce Bartok Henry Gibson Lara Harris
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 105 minutes
| country = United States English
}} 2006 Horror horror anthology film with segments directed by Sean S. Cunningham, Joe Dante, Monte Hellman, Ken Russell, and John Gaeta.

== Cast ==
* Jayce Bartok as Andy
* Henry Gibson as Tour Guide
* Lara Harris as Julia
* Scott Lowell as Henry
* Dick Miller as Max
* Michèle-Barbara Pelletier as Natalie John Saxon as Leo

== Reception ==
Received "33%" approval rating on a Rotten Tomatoes review (average rating 4.3/10). 

== References ==
 

== External links ==
*  

 
 
 
 

 
 
 
 

 