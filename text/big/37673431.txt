A Terminal Trust
{{Infobox film
| name           = A Terminal Trust
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Masayuki Suo
| producer       = Chihiro Kameyama
| writer         = Masayuki Suo
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Tamiyo Kusakari Koji Yakusho Takao Osawa Tadanobu Asano
| music          = Yoshikazu Suo
| cinematography = Rokuro Terada
| editing        = Junichi Kikuchi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 144 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2012 Japanese drama film directed by Masayuki Suo and starring Tamiyo Kusakari, Koji Yakusho, Takao Osawa and Tadanobu Asano. It is Suos first fiction film since I Just Didnt Do It (2007).

==Cast==
* Tamiyo Kusakari as Ayano Orii
* Koji Yakusho as Shinzo Egi
* Takao Osawa as Tsukahara
* Tadanobu Asano as Takai

==Release==
The film premiered at the Montreal World Film Festival in September 2012  and screened in the Special Screening section at the 25th Tokyo International Film Festival in October 2012. 

==Reception== Time Out Shall We Dance? leads Tamiyo Kusakari and Koji Yakusho play a doctor and her terminally ill patient, with a support cast including Tadanobu Asano and Takao Osawa."    Hugo Ozman of Twitch Film described the film as "a thought-provoking film that will please viewers who like serious dramas" and "a beautiful film with deep meanings and some of the best performances in a Japanese film this year."    However, John Defore of The Hollywood Reporter said, "Its understandable Suo would want to give so much screen time to the highly sympathetic Yakusho, but doing so doesnt serve the dramatic structure of a film that mightve been much more provocative than it is."   

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 


 