Dragon Squad
 
 
{{Infobox Film
| name           = Dragon Squad
| image          = DragonSquadposter.jpg
| image_size     = 
| caption        = Original theatrical poster Daniel Lee
| producer       = Michael Chou
| writer         = Daniel Lee Lau Ho-Leung
| narrator       =  Xia Yu Huang Shengyi Lawrence Chou Sammo Hung Maggie Q
| music          = Young Chen Henry Lai
| cinematography = Tony Cheung
| editing        = Azrael Chung Media Asia Films
| released       =  
| runtime        = 110 min
| country        = Hong Kong Cantonese English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} action film Daniel Lee, co-produced by Steven Seagal and starring Vanness Wu, Sammo Hung, Michael Biehn, Maggie Q, and Simon Yam.

==Plot==
  HKPD officers PLA sniper SAS operator James "Jie" Lam. They are greeted by Hon Sun, the one in charge of the case. However the escort of Tiger turns into a situation as the older Duen brother Panther Duen plans to rescue Tiger. The rescue attempt is aborted when the agents mistake an innocent civilian as the criminal attempting to break Tiger free and also discover that the convoy they are guarding is a decoy. Panther pulls out and disappears.
 707th Korean Colombian Armed US Navy SEALS and Lee Chun Pei another Korean operative. The real convoy transporting Tiger is ambushed and the operatives eliminate 7 officers on the scene. The INTERPOL agents arrive but fail to prevent their escape, causing Jie to lose his temper.

Back at the HQ, Commander Hon Sun reprimands the agents for disobeying orders but lets them off the hook. Lok visits his sick brother while off duty, Ho decides to do some of his own investigations and Jun, Jie and Suet are later introduced to Kong Long a retired police officer. It is revealed then that Petros is seeking revenge for the death of his brother Dominic. Kong Long was involved on a mission to take down Dominic and Tiger but because Kong didnt wait for backup it resulted in Tiger killing Dominic and the deaths of 6 officers. Later Lok, Ho and Suet save Kongs daughter from a fight in her own restaurant but it is then revealed that Kongs daughter hates him for a past event. The team then practice for their mission by improving their shooting accuracy in a local bar.

After several team introductions and attempts from both sides to getting to know the "players"
Captain Ko meets Petros in a cafe to initiate 2 phases of their mission. Ko will attempt to keep tabs on the INTERPOL agents while Petros will attempt to retrieve a microfilm which, for unknown purposes, is their main objective. It is rumoured to be in the hands of Panther Duen, and the only link is Yu Ching, Panthers girlfriend. As soon as Ko leaves the cafe, Petro randomly just happens to spot Ho walking down the street. Ho tries to hide his identity by pretending to be a visitor from out of town. Petros then retreats to a fortune teller temple where Ho attempts to take Petros down but Petros escapes using his training.

Petros then finds Yu Ching being bullied and pretends to befriend her, Even letting himself get beaten up by 3 Chinese gangsters who finish their beating by throwing a dumpster on Petros.
Yu Ching however is too trusting and even lets Petros into her house.

Meanwhile it is revealed Ko has a rivalry with Kong which earned him his scar. Ko tries to kill Kong in a locker room with a machete to machete fight but Ko is forced to retreat after the team is alerted to the danger.

Later Petros, Joe, Lee Chun Pei and Ko meet Panther for a deal for the microfilm, they even threaten him showing Tigers ear cut of earlier. This only results in a gunfight with the operatives eliminating most of Panthers Triads. Ho and Lok arrive on the scene attempting to fight Ko but Ko just defeats them and leaves saying "I dont have time for this" and the HKPD arresting Panther. He later gets released on lack of evidence

The Team then seeks advice from Kong on how to catch the operatives. Kong reluctly agrees to train them hard.

Eventually Hon Sun, prepares a sniper unit, and 3 teams of HKPD officers to ambush the operatives. Petros splits up with Ko after lying to Yu Ching he has to do something. Yuet easily eliminates the snipers, Petros takes out 2 of the HKPD teams and Ko eliminates the HKPD command squad. Hon Sun tries to fight back but is shot without mercy by Petros and is badly wounded.

Having enough of the operatives rampage, Ho, Lok, Jun, Suet and Jie attempt to ambush all the operatives at once. Yuet then engages in firefight with Jun, Petros, Lee Chun Pei and Ko enter a firefight as well against Suet, Jie, Ho and Lok. The operatives change tactics making it harder for the team to kill them. Joe Pearson breaks into a jeep in to evac Petros and Ko. Lee Chun Pei is killed by Suet with one shot from an MP5. Unfortunately Jun who was covering Suet gets distracted and Yuet kills Suet. Thus both sides lose one member of their team

Back at HQ, Hon Sun dies of his injuries and Suet is remembered by her comrades and even her target (Cameo by Andy On) After condolences for Suet. The team decides to catch the operatives using all the training. A China agency attempts to deport them but they are saved by Kong who only recently discovers the operatives true intentions. Their plan is then mobilzed.

Ho gets to Yu Ching first luring her away from the operatives tricking her in believing he is a friend of Petros. Jun shoots Chings car tire so Petros would pick her up. Ho then leads the operatives to an abandoned warehouse where Jun ambushes Yuet. Joe then enters the inside and is rammed by forklift by Jie. However Jie underestimates and after a few struggles Jie manages to impale Joe onto a large sign nail and electrocute him to death with a shock wire. Jun enters an intense sniper battle and eventually shoots Yuet dead. Kong finally defeats Ko, and with Ko committing suicide by stabbing himself.

Petros is the only operative left. Lok posing as a taxi driver drives Ho and Yu Ching away to a mall where Yu Ching is to deliver the microfilm to Panther Duen. Lok tries to keep Petros off Hos back but Lok is stabbed with a screw and is seemingly killed. Ho is then pistol whipped by Petros and meets back with Yu Ching. Petros decides to deliver the microfilm to Panther. In the cinema. From there Panther takes Petros hostage with a silenced pistol and kill him, but Petros disarms Panther and kills him.

Petros then attempts to leave with Yu Ching, The two engage in a final hand-to-hand confrontation, which results in Petros eventually killing Yu Ching with a spear gun also names Yu Ching dead, only to find Ho who has regained conscious waiting to ambush him. Petros attempts to shoot Ho from a high deck, only to be shot 2 times in the back by Lok who survived the screw stab. Still wounded Lok tells Ho to go after Petros. Ho and Petros then engage in a gunfight where both men shoot each other in the same laundry room at the beginning until their guns are empty. Ho is heavy wounded  but manages to shoot Petros to death and discovers the microfilm was in Chings toy doll all along.

Lok, Ho, Kong, Jie and Jun who have all apparently healed arrive at the same bar and fire all their pistols at the same time at the screen.

==Cast==
* Vanness Wu as Officer Wang Sun-Ho
* Shawn Yue as Officer Hung Kei-Lok Xia Yu as PRC Luo Zai-Jun
* Huang Shengyi as Officer Pak Yut-Suet
* Lawrence Chou as Officer James Lam, INTERPOL
* Sammo Hung as Kong Long
* Michael Biehn as Petros Angelo
* Li Bingbing as Yu Ching
* Heo Joon-ho as Captain Ko Tung-Yuen
* Maggie Q as Yuet
* Simon Yam as Commander Hon Sun
* Isabella Leong as Kongs Daughter
* Mark Henderson as Joe Pearson
* Philip Ng as Lee Chun Pei
* Andy On as Suets Undercover Target
* Kent Tong as Tiger Duen
* Ng Doi-yung as Panther Duen (as Hugo Ng)
* Abraham Boyd as Dominic Angelo
* Dan Cade as SWAT Team Instructor
* Bey Logan as Officer Kyle
* Gordon Liu as Ko 
* Liu Kai-Chi
* Daniel J. Solis as SWAT Team
* Vincent Sze
* Yuen Wah as Stall owner

== See also ==
List of Dragon Dynasty releases

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 