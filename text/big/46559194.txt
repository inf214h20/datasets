Hector the Mighty
{{Infobox film 
| name = Hector the Mighty
| image = Hector the Mighty.jpg
| director = Enzo G. Castellari
| writer =Enzo G. Castellari   Sandro Continenza  Lucio Fulci  Leonardo Martín
| starring =  
| music = Francesco De Masi
| cinematography =  Guglielmo Mancori
| editing =
| producer =Edmondo Amati   Raoul Katz 
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 1972 Cinema Italian comedy film directed by Enzo G. Castellari. A parody set in modern days of Homers Iliad, it is loosely based on the 1966 novel Le roi des Mirmidous by Henri Viard and Bernard Zacharias.    

== Cast ==
* Vittorio Caprioli as Menelaus
* Michael Forest as  Achilles Ulysses  Paris
* Helen
* Philippe Leroy as  Hector 
* Aldo Giuffrè as  Agamemnon Mercury
* Jove
* Haydée Politoff as  Chryseis
* Orchidea De Santis as Briseis
* Giancarlo Prete as  Patroclus, aka Clò-Clò
* Franca Valeri as  Cassandra
* Gianrico Tedeschi as  Priam 
* Caterina Boratto as Hecuba  Polites
* José Calvo as  The Lawyer

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 