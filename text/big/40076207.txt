Senathipathi
{{Infobox film
| name           = Senathipathi
| image          = 
| image_size     =
| caption        = 
| director       = M. Rathnakumar
| producer       = Malar Balu K. Dhandapani
| writer         = M. Rathnakumar
| starring       =   Deva
| cinematography = B. Kannan
| editing        = K. Pazhanivel
| distributor    = Malar Films
| studio         = Malar Films
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil drama Sukanya in Deva and was released on 10 November 1996.Dubbed In Hindi As Ek Kahar.  

==Plot==

Senathipathi (Sathyaraj) is a respected village chief who is married to Meenakshi (Sukanya (actress)|Sukanya), while his little brother Sethupathi (Sathyaraj) is a good-for-nothing young man. Senathipathi and Lingappan Naicker (Vijayakumar (actor)|Vijayakumar), another respected village man, are best friends. Senathipathi is charged to protect the village against the robbers. Lingappan Naickers daughter Aishwarya (Soundarya), a clever and arrogant girl, returns after studying in the city. Sethupathi and Aishwarya eventually fall in love with each other. Nagappan (Manivannan), a rich man, clashed in the past with Senathipathi and wants to take revenge on Senathipathis family.

==Cast==

  
*Sathyaraj as Senathipathi and Sethupathi
*Soundarya as Aishwarya Sukanya as Meenakshi
*Goundamani as Kathavarayan Senthil as Veera Bahu Vijayakumar as Lingappan Naicker
*Srividya as Lingappan Naickers wife
*Manivannan as Nagappan
*Ponvannan as Nagappans son Ponnambalam
*Mahanadi Shankar Chandrasekhar as Suruli (guest appearance) Thyagu
 
*Mohan V. Ram as Rangachari
*K. K. Soundar
*Oru Viral Krishna Rao
*Dhanapal
*Pasi Narayanan as Thavasi
*MLA Malaya MRK
*Bayilvan Ranganathan
*Periya Karuppu Thevar as Meenakshis father
*MLA Thangaraj
*Mahendran
*Viswanath
 

==Soundtrack==

{{Infobox album |  
| Name        = Senathipathi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 24:11
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vairamuthu and M. Rathnakumar.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Mano || 5:03
|- 2 || En Idhaya Thai Thirudivittai || S. P. Balasubrahmanyam, Swarnalatha || 5:20
|- 3 || Moonu Mozhama Malliyappoo || S. P. Balasubrahmanyam, K. S. Chithra || 4:35
|- 4 || Palaivana Roja || S. P. Balasubrahmanyam, K. S. Chithra || 4:39
|- 5 || Yarukkum Thalaivanangaa || S. P. Balasubrahmanyam, K. S. Chithra || 4:34
|}

==References==
 

 
 
 
 
 


 