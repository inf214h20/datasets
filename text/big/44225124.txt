Arjunan Pillayum Anchu Makkalum
{{Infobox film
| name           = Arjunan Pillayum Anchu Makkalum
| image          =
| caption        =
| director       = Chandrasekharan
| producer       = Chandrasekharan
| writer         = Babu Pallassery
| screenplay     = Babu Pallassery Innocent KPAC Kalpana
| music          = Mohan Sithara
| cinematography = Sreesankar
| editing        = K Rajagopal
| studio         = Sree Sakthi Productions
| distributor    = Sree Sakthi Productions
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, Kalpana in lead roles. The film had musical score by Mohan Sithara.   

==Cast==
 
*Jagathy Sreekumar Innocent
*KPAC Lalitha Kalpana
*Baiju Baiju
*Siddique Siddique
*Bindu Panicker
*Charmila
*Harishree Ashokan
*Jagadish
*Mala Aravindan
 

==Soundtrack==
The music was composed by Mohan Sithara and lyrics was written by Kaithapram and Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Evide Nin Daivaamsham || K. J. Yesudas || Kaithapram || 
|-
| 2 || Manchaadi Chundathum || KS Chithra, Chorus || Bichu Thirumala || 
|-
| 3 || Moham manassilittu || Chorus, Pradip Somasundaran || Bichu Thirumala || 
|-
| 4 || Vellikkinnam Niranju || K. J. Yesudas || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 

 