Rififi
{{Infobox film
| name           = Rififi
| image          = Rifififrenchposter.jpg
| image_size     = 
| border         = 
| alt            = Movie poster illustrates Tony "le Stephanois" wearing a green jacket over a red background. In the background Jo "le Suédois" attempts to pull a telephone away from his wife. Text at the top of the image includes the tagline "Tony le Stephanois est exact au rendez-vous...". Text at the bottom of the poster reveals the original title and production credits.
| caption        = Film poster with original French title
| director       = Jules Dassin
| producer       = {{plainlist|
*Henri Bérard
*Pierre Cabaud
*René Bézard}}
| writer         = {{plainlist|
*Auguste le Breton
*Jules Dassin
*René Wheeler}}
| based on       =  
| starring       = {{plainlist|
*Jean Servais
*Carl Möhner Robert Manuel
*Jules Dassin
*Janine Darcey
*Magali Noël}}
| music          = Georges Auric
| cinematography = Philippe Agostini
| editing        = Roger Dwyre
| studio         = 
| distributor    = Pathé
| released       =  
| runtime        = 115 minutes
| country        = France
| language       = French
| budget         = $200,000  {{cite news
|url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20020901/REVIEWS08/209010301/1023
|publisher=Chicago Sun-Times date =October 6, 2000
|title=Rififi (1954)|first=Roger |last=Ebert}} 
| gross          = 
}}
 Robert Manuel as Mario Farrati, and Jules Dassin as César "le Milanais". The foursome band together to commit an almost impossible theft, the burglary of an exclusive jewelry shop on the Rue de Rivoli. The centerpiece of the film is an intricate half hour heist scene depicting the crime in detail, shot in near silence, without dialogue or music. The fictional burglary has been mimicked by criminals in actual crimes around the world.  
 blacklisted from Hollywood, Dassin found work in France where he was asked to direct Rififi. Despite his distaste for parts of the original novel, Dassin agreed to direct the film. He shot Rififi while working with a low budget, without a star cast, and with the production staff working for low wages. 
 Best Director at the 1955 Cannes Film Festival.    Rififi was nominated by the National Board of Review for Best Foreign Film. Rififi was re-released theatrically in 2000 and is still highly acclaimed by modern film critics as one of the greatest works in French film noir. 

==Plot==
Tony "le Saint-Étienne#Demographics|Stéphanois" has served a five-year prison term for a jewel heist and is out on the street and down on his luck. His friend Jo approaches him about a smash-and-grab proposed by mutual friend Mario in which the threesome would cut the glass on a Parisian jewelers front window in broad daylight and snatch some gems. Tony declines. He then learns that his old girlfriend, Mado, took up in his absence with gangster Parisian nightclub owner Pierre Grutter. Finding Mado working at Grutters, Tony invites her back to his rundown flat. She is obviously well-kept, and Tony savagely beats her for being so deeply involved with Grutter. Tony changes his mind about the heist; he now accepts on the condition that they rob the jewelers safe instead of the window. Mario suggests they employ the services of Italian compatriot César, a safecracker. The four devise and rehearse an ingenious plan to break into the store and disarm its sophisticated alarm system.
 London contact. Meanwhile, Grutter has seen Mado and her injuries, who breaks off their relationship. Infuriated at Tonys interference in his life, he gives heroin to his drug-addicted brother Remi and tells him to murder Tony. Grutter sees the diamond César gave to Viviane and realizes that César, Mario, and Tony were responsible for the jewel theft. Grutter forces César to confess. Forsaking a 10 million franc police reward, Grutter decides to steal the jewels from Tonys gang, his brother Remi brutally murdering Mario and his wife Ida when they refuse to reveal where the loot is hidden. Tony retrieves it from the couples apartment and anonymously pays for a splendid funeral for them. He then goes looking for Grutter and stumbles onto the captive César, who confesses having squealed. Citing "the rules," Tony ruefully kills him.

Meanwhile, seeking to force their adversaries hand, Grutters thugs kidnap Jos five-year-old son Tonio and hold him ransom. The London fence arrives with the payoff, after which Tony leaves to single-handedly rescue the child by force, advising Jo it is the only way they will see him alive.  With Mados help he tracks Tonio down at Grutters country house and kills Grutters brothers Rémi and Louis while rescuing him. On the way back to Paris, Tony learns Jo has cracked under the pressure and agreed to meet Grutter at his house with the money. When he arrives Grutter tells him Tony has already snatched the child and kills Jo.  Seconds too late to save his friend, Tony is mortally wounded by Grutter before killing him as he tries to flee with the loot. Bleeding profusely, Tony drives maniacally back to Paris and delivers Tonio home safely before dying at the wheel as police and bystanders close in on him and a suitcase filled with 120 million francs in cash.

==Cast==
* Jean Servais as Tony "le Stéphanois": A gangster who recently returned from serving five years in prison for jewel theft. The eldest member in on the heist, Tony is godfather of namesake Tonio, son of Jo "le Suédois".
*   gangster Tony took the five year rap for. Jo invites Tony in on the heist.
*   gangster who came up with the original idea for a jewel heist.
*   of Perlo Vita. 
* Marcel Lupovici as Pierre Grutter: Leader of the Grutter gang and owner of the night-club LÂge dOr. He is the first to figure out Tonys responsibility for the diamond heist.
* Robert Hossein as Remi Grutter: A member of the Grutter gang addicted to heroin.
* Pierre Grasset as Louis Grutter: A member of the Grutter gang.
* Marie Sabouret as Mado: The former lover of Tony "le Stéphanois".
* Dominique Maurin as Tonio, the young son of Jo "le Suédois". Towards the end of the film, Tonio is kidnapped by the Grutter gang and is rescued by Tony "le Stéphanois".
* Janine Darcey as Louise, Jos wife and the mother of Tonio.

==Production==

===Development===
The film Rififi was originally to be directed by Jean-Pierre Melville, a later luminary of the heist film genre. Melville gave his blessing to American director Jules Dassin when the latter asked for his permission to take the helm.  It was Dassins first film in five years; Powrie 2006, p. 76.  he had been blacklisted by the House Committee on Un-American Activities after fellow director Edward Dmytryk named him a communist in April 1951. {{cite DVD notes 
|title= Rififi
|titlelink= Rififi
|origyear= 1955
|others= 
|type= Supplemental slideshow on DVD
|publisher= The Criterion Collection
|location=  New York, New York
|id=  ISBN 0-7800-2396-X
|year= 2001
|language= 
}}   Subsequently, Dassin attempted to rebuild his career in Europe. Several such film projects were stopped through long-distance efforts by the US government.    Dassin attempted a film LEnnemi public numero un, which was halted after stars  . 
 racist theme in which the rival gangsters were dark Arabs and North Africans pitted against light-skinned Europeans. As well, the book portrayed disquieting events such as necrophilia—scenes that Dassin did not know how to bring to the big screen.    For the rival gang, the producer suggested making them Americans, assuming Dassin would approve. Dassin was against this idea as he didnt want to be accused of taking oblique revenge on screen. Dassin downplayed the rival gangsters ethnicity in his screenplay, simply electing the Germanic "Grutter" as surname.  The greatest change from the book was the heist scene, which spanned but ten pages of the 250-page novel. Dassin focused his screenplay on it to get past other events he did not know what to do with.  As produced, the scene takes a quarter of the films running time and is shot with only natural sound, sans spoken words or music. 

===Filming=== Italian gangster Robert Manuel after seeing him perform a comic role as a member of Comédie-Française.  After a suggestion made by the wife of the films producer, Dassin cast Carl Möhner as Jo the Swede.  Dassin would use Möhner again in his next film He Who Must Die.  Dassin himself played the role of the Italian safecracker César the Milanese.  Dassin explained in an interview that he "had a cast a very good actor in Italy, whose name escapes me, but he never got the contract!...So I had to put on the mustache and do the part myself".  
 
Rififi was filmed during the wintertime in Paris and used real locations rather than studio sets.  Powrie 2006, p. 77.  Due to the low budget, the locations were scouted by Dassin himself.  Dassins fee for writing, directing, and acting was US$8,000.  Dassins production designer to whom he referred as "one of the greatest men in the history of cinema" was Alexandre Trauner. Out of friendship for Dassin, Trauner did the film for very little money.  Dassin argued with his producer Henri Bérard on two points: Dassin refused to shoot the film when there was sunlight claiming that he "just wanted grey"; {{cite DVD notes 
|title= Rififi
|titlelink= Rififi
|origyear= 1955
|others= 
|type= Jules Dassin Interview
|publisher= The Criterion Collection
|location=  New York, New York
|id=  ISBN 0-7800-2396-X
|year= 2001
|language=  Lemmy Caution film series. 

Rififis heist scene was based on an actual burglary that took place in 1899 along Marseilles cours St-Louis. A gang broke into the first floor offices of a travel agency, cutting a hole in the floor and using an umbrella to catch the debris in order to make off with the contents of the jewelers shop below. Powrie 2006, p. 73.  The scene where Tony regretfully chooses to kill César for his betrayal of the thieves code of silence was filmed as an allusion to how Dassin and others felt after finding their contemporaries willing to name names in front of the House Un-American Activities Committee. Powrie 2006, p. 77.  This act was not in the original novel. 

===Music and title===
 
Georges Auric was hired as the composer for the film. Dassin and Auric originally could not agree about scoring the half hour caper scene. After Dassin told Auric he did not want music, Auric claimed he would "protect  . Im going to write the music for the scene anyways, because you need to be protected". After filming was finished, Dassin showed the film to Auric once with music and once without. Afterward, Auric agreed the scene should be unscored. 

In 2001, Dassin admitted that he somewhat regretted the Rififi theme song, utilized only to explain the films title which is never mentioned by any other film characters.  The title is almost un-translatable into English; the closest attempts have been "rough and tumble" and "pitched battle." {{cite news
|url=http://pqasb.pqarchiver.com/latimes/access/62165228.html?dids=62165228:62165228&FMT=ABS&FMTS=ABS:FT&date=Oct+06%2C+2000&author=KENNETH+TURAN&pub=Los+Angeles+Times&desc=Movie+Review%3B+Rififi+Remains+the+Perfect+Heist+(Movie)%3B+Jules+Dassins+1955+thriller+has+lost+none+of+its+power+to+captivate+and+entertain+an+audience.&pqatl=google
|publisher=Los Angeles Times
|date=October 6, 2000 Moroccan Berber Berbers because of the Rif War. {{cite news
|url=http://www.nytimes.com/2000/07/16/movies/film-a-noir-classic-makes-it-back-from-the-blacklist.html?sec=&spon=&pagewanted=all
|publisher=The New York Times
|date=July 16, 2000
|title=FILM; A Noir Classic Makes It Back From the Blacklist |first=Michael |last=Sragow}}  In reality Rififi is a slang word deriving from "rif", the French military term for "combat zone" during Word War I, from the Middle French "rufe" for fire from the Latin "rufus" for red.  See rufous. The song was written in two days by lyricist Jacques Larue and composer Philippe-Gérard after Dassin turned down a proposal by Louiguy.  Magali Noël was cast as Viviane, who sings the films theme song.  Noël would later act for Italian director Federico Fellini, appearing in three of his films. 

==Release==
Rififi debuted in France on April 13, 1955. The film was banned in some countries due to its heist scene, referred to by the Los Angeles Times reviewer as a "master class in breaking and entering as well as filmmaking". {{cite news
|url=http://articles.latimes.com/2008/apr/01/local/me-dassin1
|publisher=Los Angeles Times
|date=April 1, 2008
|title=Blacklisted Director Jules Dassin Dies at 96 |first=Claudia |last=Luther}}  banned the film because of a series of burglaries mimicking its heist scene.   Lethbridge Herald, The
Saturday, August 18, 1956  Rififi was banned in Finland in the late 1950s. Törnudd 1986, p. 152.  In answer to critics who saw the film as an educational process that taught people how to commit burglary, Dassin claimed the film showed how difficult it was to actually carry out a crime. 
 
Rififi was a popular success in France which led to several other Rififi films based on le Bretons stories. Hardy 1997, p. 118.  These films include Du rififi chez les femmes (1959), Du rififi à Tokyo (1961), and Du rififi à Paname (1965). Hardy 1997, p. 119.  On its United Kingdom release, Rififi was paired with the British   announced that Stone Village Pictures acquired the remake rights to Rififi; the producers intending to place the film in a modern setting with Al Pacino taking the lead role.   

===Home media===
In   on April 21, 2003, and on Region B Blu-ray Disc|Blu-ray by the same publisher on May 9, 2011.         

==Critical reception== Daily Herald National Board of Review nominated the film as the Best Foreign Film in 1956.   
 subtitles that normalized rating average score of 97, based on 13 reviews.    The Killing" director Jean-Luc Godard regarded the film negatively in comparison to other French crime films of the era, noting in 1986 that "today it cant hold a candle to Touchez pas au grisbi which paved the way for it, let alone Bob le flambeur which it paved the way for." Godard 1986, p. 127. 

==See also==
 
*1955 in film
*List of French films of 1955
*List of French-language films
*List of crime films of the 1950s
*Rifampicin The Italian-developed notably red-colored antibiotics (rifamycins) isolated from a bacterium isolated in a forest on the French Riviera, were named (1957-1959) from the title of this film, indirectly also referring to the color red

==Notes==
*   †  The title Du Rififi chez les hommes does not directly translate into English. A rough translation would be Rumble among the men. 

==References==
 

==Bibliography==
*{{cite book
 | last= Powrie
 | first= Phil 
 | title= The Cinema of France
 |publisher= Wallflower Press
 |year= 2006
 |isbn= 1-904764-46-0
 |url=http://books.google.com/books?id=nKd2VC2Q95YC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Hunt
 | first= Bill
 |author2=Todd Doogan
  | title= The Digital Bits Insiders Guide to DVD: Insiders Guide to DVD
 |publisher= McGraw-Hill Professional
 |year= 2004
 |isbn= 0-07-141852-0
 |url=http://books.google.com/books?id=ZPvgA1QU1BMC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Hardy
 | first= Phil
 | title= The BFI Companion to Crime
 |publisher= Continuum International Publishing Group
 |year= 1997
 |isbn= 0-304-33215-1
 |url=http://books.google.com/books?id=4UZRcuwtbmEC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Truffaut
 | first= François
 |author2=Leonard Mayhew
  | title= The Films In My Life
 |publisher= Da Capo Press
 |year= 1994
 |isbn= 0-306-80599-5
 |url=http://books.google.com/books?id=aZsFHXk8BWoC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Levy
 | first= Emanuel  
 | title= All about Oscar: The History and Politics of the Academy Awards
 |publisher= Continuum International Publishing Group
 |year= 2003
 |isbn= 0-8264-1452-4
 |url=http://books.google.com/books?id=dH2Lb_YhIhAC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Hillier
 | first= Jim 
 |author2=Nick Browne |author3=David Wilson
  | title= Cahiers Du Cinema: Volume I: The 1950s. Neo-Realism, Hollywood, New Wave.
 |publisher= Routledge
 |year= 1985
 |isbn= 0-415-15105-8
 |url=http://books.google.com/books?id=dJY_vOE943QC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Törnudd
 | first= Klaus 
 | title= Finland and the International Norms of Human Rights
 |publisher= Martinus Nijhoff Publishers
 |year= 1986
 |isbn= 90-247-3257-3
 |url=http://books.google.com/books?id=w4qgtDF6Z2IC
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Foerstel
 | first= Herbert N.
 | title= Banned in the Media: A Reference Guide to Censorship in the Press, Motion Pictures, Broadcasting, and the Internet
 |publisher= Greenwood Publishing Group
 |year= 1998
 |isbn= 0-313-30245-6
 |url=http://books.google.com/books?id=LcZoAAAAIAAJ
 |accessdate=13 August 2009
}}
*{{cite book
 | last= Godard
 | first= Jean Luc 
 |author2=Jean Narboni |author3=Tom Milne
  | title= Godard on Godard
 |publisher= Da Capo Press
 |year= 1986
 |isbn= 0-306-80259-7
 |url=http://books.google.com/books?id=qvzDbqWmW4IC
 |accessdate=13 August 2009
}}

==External links==
*  
*  
*   at Metacritic
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 