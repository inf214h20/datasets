Dilwaala
{{Infobox film 
 | name = Dilwaala
 | image = Dilwaala86.jpg
 | caption = Promotional Poster
 | director = K. Murali Mohan Rao
 | producer = D. Rama Naidu T. Subbarami Reddy
 | writer = Paruchuri Brothers Pran
 | music = Bappi Lahiri
 | studio = Suresh Productions
 | cinematographer = S. Gopala Reddy
 | released =   
 | runtime = 135 min.
 | language = Hindi
 | budget =  4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1986 Hindi Indian feature directed by K. Murali Mohan Rao, starring Mithun Chakraborty, Smita Patil, Meenakshi Sheshadri, Sarika, Suresh Oberoi, Supriya Pathak, Gulshan Grover  and Pran (actor)|Pran.

==Plot==

Dilwaala is an action entertainer, featuring Mithun Chakraborty, Smita Patil, Meenakshi Sheshadri, Sarika, Suresh Oberoi, Supriya Pathak, Gulshan Grover, Aruna Irani  and Pran (actor)|Pran.

==Summary==

Ravi (Mithun Chakraborty) and his sister Judge Sumitra Devi (Smita Patil) never see eye to eye. MLA Raj Shekhars son Raghu (Gulshan Grover) gets married to the poor Kamla (Supriya Pathak) at his fathers behest. But not liking her, Raghu tortures and later kills Kamla. The father does his best to get his son out of the hands of law. Watch the drama unfolding in court in the presence of Judge Sumitra Devi and the role played by Ravi.

==Cast==

*Mithun Chakraborty  as   Ravi Kumar
*Smita Patil  as   Sumitra Devi 
*Meenakshi Sheshadri  as   Padma  
*Sarika  as   Sapna  Pran  as   MLA Raj Shekhar 
*Suresh Oberoi  as   Madanlal Sharma 
*Supriya Pathak  as   Kamla  Dulari   as   Chachi 
*Arun Govil  as   Mohan Kumar 
*Gulshan Grover  as   Raghu 
*Aruna Irani  as   Shanti 
*Shakti Kapoor  as   King Kong 
*Goga Kapoor  as   King Kongs Brother-In-Law 
*Asrani  as   Sub Inspector Sohan 
*Kader Khan  as   Sewakram Sitapuri 
*Viju Khote  as   Grocer 
*Shreeram Lagoo  as   Ganesh Vithal Kolhapure  Murad  as   Judge 
*Raza Murad  as   Public Prosecutor  Paintal  as   Mr. Narayan
*Renu Joshi  as   Mrs. Savitri Sitapuri 
*Bandini Mishra  as   Champa

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Dilwaala
* http://www.bollywoodhungama.com/movies/cast/4890/index.html

==External links==
*  

 
 
 
 