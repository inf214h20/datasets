List of Kannada films of 2004
 
 

==Commercially Successful Films of Year 2004==

Apthamitra - Blockbuster

Durgi (film)  - Hit
 Omkara - Hit

Veera Kannadiga - Super Hit

Kalasipalya - Blockbuster
 Malla - Blockbuster
 Maurya - Super Hit
 Monalisa - Super Hit

Joke Falls - Super Hit

==List of films released== Kannada film industry in India in 2004 in film|2004, presented in alphabetical order.

==Released films==

=== January–June ===
{| class="wikitable"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening
! style="width:13%;"|Title
! Director !! Cast !! Music Director !! Genre !! Notes 
|- January!
| rowspan="4"   style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N
| rowspan="1" style="text-align:center;"| 1st Anita Hassanandani, Chakri || Action / Romance || Remake of Telugu film Andhrawala
|-
| rowspan="1" style="text-align:center;"| 16th Deva || Drama || Remake of Tamil film Thanga Padakam
|-
| rowspan="1" style="text-align:center;"| 23rd
| Bisi Bisi || Ramanath Rugvedi || Ramesh Aravind, Madhuri Bhattacharya, Anu Prabhakar, Sihi Kahi Chandru || Milind Dharmasena || Comedy ||  
|-
| rowspan="1" style="text-align:center;"| 30th Sudhir || Pampa || Comedy ||  
|-
|- February!
| rowspan="6"   style="text-align:center; background:thistle; textcolor:#000;"| F E B
| rowspan="2" style="text-align:center;"| 6th
| Abbabba Entha Huduga || Hemanth Hegde || Arjun, Naaz, Seema Shetty, Avinash || Suresh Kumar || Romance ||  
|- Koti  K. Kalyan Vandemataram Srinivas Babji-Sandeep   Srishailam || Comedy / Romance ||  
|-
| rowspan="1" style="text-align:center;"| 13th Ashok || M N Krupakar || Action || 
|-
| rowspan="1" style="text-align:center;"| 20th
| Pandava || Anand P Raj || Vinod Raj, Charanraj, B. Jayashree || Gopi Krishna || Action / Drama ||
|-
| rowspan="2" style="text-align:center;"| 27th
| Malla (film)|Malla || V. Ravichandran || V. Ravichandran, Priyanka Trivedi, Mohan Shankar, Umashree, Tejashree || V. Ravichandran || Romance / Drama ||
|-
| Baithare Baithare || Gunakumar || Anand, Sharan (actor)|Sharan, Shyam, Urvashi Patel, Jyothi Krishna, Sangeeta Shetty, Dwarakish || Teja || Comedy || 
|-
|- March!
| rowspan="10"   style="text-align:center; background:#98fb98; textcolor:#000;"| M A R
| rowspan="3" style="text-align:center;"| 5th
| Dharma || K V Chandranath || Darshan (actor)|Darshan, Sindhu Menon, Jai Jagadish || Hamsalekha || Action || 
|- Koti || Romance || 
|-
| Teenagers || B Shankar || Nivas, Indranag || M N Krupakar || Romance || 
|-
| rowspan="2" style="text-align:center;"| 12th
| Maha Sambhrama || M Ramamurthy || Murali Krishna, Chaya Singh || Sangeeta Priya || Romance ||
|-
| Ok Saar Ok || Dayal Padmanabhan || Madan Patel, Anu Prabhakar  || Nag - Mahesh || Romance || 
|-
| rowspan="2" style="text-align:center;"| 19th
|  Saagari || Victory Vasu || Ramkumar, Bhavana (Kannada actress)|Bhavana, Bhavya, Sadhu Kokila || Sadhu Kokila || Romance / Drama || 
|-
| Muni || B R Keshava || Dharma, Durga Shetty, Gazar Khan || T Venkatesh || Action ||
|-
| rowspan="3" style="text-align:center;"| 26th
| Y2K || N. Lokanath || Nagendra Prasad, Mitisha Sharma, Sadhu Kokila, Mimicry Dayanand || Sadhu Kokila || Drama ||
|- Sai Kumar, Master Hirannaiah || Sadhu Kokila || Drama ||
|- Geetha || Stephen Prayog|| Drama || Based on a novel by M. K. Indira. 
|-
|- April!
| rowspan="9"   style="text-align:center; background:#ffdead; textcolor:#000;"| A P R
| rowspan="1" style="text-align:center;"| 2nd
| Agodella Olledakke || A R Babu || Jaggesh, Abitha, Tara Venu|Tara|| Raj Bhaskar || Comedy ||
|-
| rowspan="2" style="text-align:center;"| 9th
| Darshan || Ramesh Kitty || Darshan (actor)|Darshan, Navneeth, Chitra Shenoy || Sadhu Kokila || Action ||
|- Sai Kumar, Prashanti, Pavitra Lokesh || Stephen Prayog || Action ||
|-
| rowspan="2" style="text-align:center;"| 16th
| Preethi Nee Illade Naa Hegirali || Eshwar Balegundi || C. P. Yogeshwar, Anu Prabhakar || Chaitanya || Romance || 
|-
| Friendship || B R Keshav || Dharma, Shamitha || Mahesh || Drama ||
|-
| rowspan="3" style="text-align:center;"| 23rd
| Ranga SSLC || Yogaraj Bhat || Sudeep, Ramya, Rangayana Raghu || Sandeep Chowta || Comedy/ Romance / Drama || 
|- Thriller Manju, Swapna || M N Krupakar || Action ||
|-
| Kanasina Loka || G S Sarasa Kumar || Vasu, Gopika || Teja || Romance ||
|- 
| rowspan="1" style="text-align:center;"| 30th
| Durgi (film)|Durgi || P. Ravi Shankar || Malashri, Ashish Vidyarthi, Avinash, Raghuvaran, Kalabhavan Mani || Hamsalekha || Action ||
|-
|- May!
| rowspan="8"   style="text-align:center; background:#edafaf; textcolor:#000;"| M A Y
| rowspan="3" style="text-align:center;"| 7th Darshan || Valisha - Sandeep || Romance ||
|- Shruti || V. Manohar || Family ||
|-
| Nija || K V Raju || Jaggesh, Lahari || Raj Bharath || Drama ||
|-
| rowspan="1" style="text-align:center;"| 14th
| Bimba (film)|Bimba || Kavita Lankesh || Daisy Bopanna, Raksha || Issac Thomas Kottukapally || Drama ||
|-
| rowspan="1" style="text-align:center;"| 21st Sai Kumar, Durga Shetty || London Chandru || Action ||
|- 
| rowspan="3" style="text-align:center;"| 28th Kaveri || S. A. Rajkumar || Romance || 
|-
| Mellusire Savigana || Ramesh Krishna || Harish, Rashmi Kulkarni || V. Manohar || Romance ||
|-
| Samudra || Kushal Babu || Kushal Babu, Thriller Manju || M N Krupakar || Action ||
|-
|- June!
| rowspan="5"   style="text-align:center; background:#d9c1b0; textcolor:#000;"| J U N
| rowspan="1" style="text-align:center;"| 4th
| Pathi Patni Avalu || Vijay Gujjar || Adarsh, Amritha  || Shyam Sunder || Comedy || 
|-
| rowspan="2" style="text-align:center;"| 18th
| Baa Baaro Rasika || Dayal Padmanabhan || Sunil Raoh, Ramya Krishnan, Ashitha || Mahesh || Romance ||
|-
| Sakhi || S. Mahendar || Praveen Kumar, Poonam || Chaitanya || Romance ||
|-
| rowspan="2" style="text-align:center;"| 25th
| Kanasu || Sushil Mokashi || Sushil Mokashi, Sanjjanaa || Sameer Kulkarni || Drama ||
|-
| Devasura || A. N. Jayaramaiah || Devaraj, B. C. Patil || Guna Singh || Action ||
|-
|}

===July – December===
{| class="wikitable"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening
! style="width:13%;"| Title
! Director !! Cast !! Music Director !! Genre !! Notes  
|- July!
| rowspan="7"   style="text-align:center; background:#d0f0c0; textcolor:#000;"| J U L
| rowspan="2" style="text-align:center;"| 2nd
| Kanakambari || Dinesh Babu || Anu Prabhakar, Vijayalakshmi (Kannada actress)|Vijayalakshmi, Om Prakash Rao, Bank Janardhan || Praveen D. Rao || Horror ||  
|-
| Crime Story || B R Keshav || Devaraj, Shobharaj || Maruthi Meerajkar || Action|| 
|- 
| rowspan="2" style="text-align:center;"| 9th
| Love (2004 film)|Love || Rajendra Singh Babu || Auditya, Rakshita, Mohanlal, Amrish Puri || Anu Malik|| Romance / Action || 
|-
| Kanti || Bharath || Sri Murali, Ramya, Mumtaj || Gurukiran || Action ||
|-
| rowspan="1" style="text-align:center;"| 16th Meena || Hamsalekha || Drama ||  
|-
| rowspan="1" style="text-align:center;"| 23rd
| Target || H S Rajashekar || Thriller Manju, Ruchita Prasad || M N Krupakar || Action ||
|-
| rowspan="1" style="text-align:Center;"| 30th
| Sarvabhowma || Mahesh Sukhadhare || Shivrajkumar, Shilpa Shivanand || Hamsalekha || Action ||
|-
|- August!
| rowspan="8"   style="text-align:center; background:#c2b280; textcolor:#000;"| A U G
| rowspan="1" style="text-align:center;"| 6th Bhavana || Rajesh Ramanath || Action / Romance || 
|-
| rowspan="2" style="text-align:center;"| 13th
| Ajju || N T Jayarama Reddy || Sri Harsha, Ranjitha || Rajesh Ramanath || Action ||
|-
| Naari Munidare Gandu Parari || G K M Raju || Kashinath (actor)|Kashinath, Varsha, Tara Venu|Tara, Doddanna || Rajesh Ramanath || Comedy ||
|-
| rowspan="3" style="text-align:center;"| 20th
| Pakkadmane Hudugi || M S Rajashekar || Raghavendra Rajkumar, Ranjitha, Ananth Nag, Mohan Shankar || Rajesh Ramanath || Comedy || Remake of Hindi film Padosan
|-
| Bidalaare || Ramana || Anil Kalyan, Keerthi Chawla || K M Indra || Romance ||
|-
| Real Rowdy || Bhavani Shankar || Charanraj, Yashaswini || Layendra   Madan || Action ||
|-
| rowspan="2" style="text-align:center;"| 27th
| Apthamitra || P. Vasu || Vishnuvardhan (actor)|Vishnuvardhan, Ramesh Aravind, Soundarya, Prema (actress)|Prema, Dwarakish, Vinaya Prasad, Avinash || Gurukiran || Horror / Thriller / Drama || Remake of Malayalam film Manichitrathazhu
|-
| Shuklambharadaram || Mastaan || Mohan Shankar, Durga Shetty || S P Chandrakanth || Comedy ||
|-
|- September!
| rowspan="5" style="text-align:center; background:wheat; textcolor:#000;"| S E P
| rowspan="2" style="text-align:center;"| 10th
| Yahoo || Seetaram Karanth || Neetha, Dattathreya || M N Krupakar || Drama ||
|- Prakash || Prem Kumar, Preethi || Allwyn Fernandes || Romance ||
|-
| style="text-align:center;"| 18th
| Omkara (2004 film)|Omkara || Shivamani || Upendra, Preeti Jhangiani, Shweta Menon, Rangayana Raghu || Gurukiran || Drama / Action ||
|-
| rowspan="2" style="text-align:center;"| 24th Sumithra  || Rajesh Ramanath || Romance / Drama || Remake of Tamil film Muthu (film)|Muthu
|- Rajesh || Rajan Gurung || Drama ||
|-
|- October!
| rowspan="4"   style="text-align:center; background:#ffb7c5; textcolor:#000;"| O C T
| rowspan="2" style="text-align:center;"| 15th
| Kalasipalya (film)|Kalasipalya || Om Prakash Rao || Darshan (actor)|Darshan, Rakshita, Avinash, Sadhu Kokila || Sadhu Kokila || Action ||
|- Kashinath || Kashinath (actor)|Kashinath, Lakshmi Musuri, Varna, Bank Janardhan || V. Manohar || Comedy ||  
|-
| rowspan="1" style="text-align:center;"| 22nd
| Maurya (film)|Maurya || S. Narayan || Puneeth Rajkumar, Meera Jasmine, Roja (actress)|Roja, Devaraj || Gurukiran|| Romance || Remake of Telugu film Amma Nanna O Tamila Ammayi
|-
| style="text-align:center;"| 29th
| Shrushti || Basavaraj || Nagendra Prasad, Vinod Prabhakar, Priyadarshini, Roopa Hassan || S. A. Rajkumar || Drama ||
|-
|- November!
| rowspan="6"   style="text-align:center; background:#ace1af; textcolor:#000;"| N O V
| rowspan="4" style="text-align:center;"| 12th
| Nalla || V. Nagendra Prasad || Sudeep, Sangeetha (actress)|Sangeetha, Tara Venu|Tara, Srinath || Venkat Narayan || Romance / Drama || 
|-
| Santhosha || M R Ramesh || Siddharth, Rajesh Krishnan, Pranathi, Anitha  || Stephen Prayog || Romance ||  
|-
| Jennifer || Y S Balaraj || Y S Balaraj, Neetha || A T Raveesh || Romance ||
|-
| Dharma Yodharu ||  || Samarth, Roopa Ravindran ||  || Documentary ||
|-
| rowspan="1" style="text-align:center;"| 19th
| Trin Trin || T Chikkanna || Dharma, Ruthika  || Rajesh Ramanath || Suspense Thriller || 
|-
| rowspan="1" style="text-align:center;"| 26th
| Aliya Mane Tholiya || Nagendra Magadi || Om Prakash Rao, Ruchita Prasad || Rajesh Ramanath || Comedy ||
|-
|- December!
| rowspan="6"   style="text-align:center; background:#b8860b; textcolor:#000;"| D E C
| rowspan="3" style="text-align:center;"| 3rd
| Joke falls || Ashok Patil || Ramesh Aravind, Deepali, Neethu (actress)|Neethu, Dattanna || Mano Murthy || Comedy ||
|-
| Sardara || P N Satya || Darshan (actor)|Darshan, Gurleen Chopra, Srinivasa Murthy || Venkat Narayan || Action ||
|-
| Priya Manase || Madhu Mohana || Uday, Lathashree || Shivasatya || Romance || 
|-
| rowspan="1" style="text-align:center;"| 17th
| Jyeshta (film)|Jyeshta || Suresh Krissna || Vishnuvardhan (actor)|Vishnuvardhan, Ashima Bhalla, Ramesh Bhat, Aniruddh, Devaraj, Sindhu Menon || S. A. Rajkumar|| Action || Remake of Malayalam film Valliettan  
|-
| style="text-align:center;"| 24th Kanchana Ganga || Rajendra Singh Babu || Shivarajkumar, Sridevi Vijayakumar, Parvin Dabbas, Sumalatha, Sadhu Kokila, Arjun || S. A. Rajkumar || Drama ||
|-
| style="text-align:center;"| 31st
| Chappale || Naganna|| Sunil Raoh, Richa Pallod, Sihi Kahi Chandru || R. P. Patnaik || Romance ||
|-
|}

==References==
 

==External links==
*   at the Internet Movie Database

 
 

 
 
 
 
 