A Dash Through the Clouds
{{infobox film
| title          = A Dash Through the Clouds
| image          = A Dash Through the Clouds (1912).webm
| caption        =
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         = Dell Henderson
| starring       = Mabel Normand
| music          =
| cinematography = Percy Higginson
| editing        =
| distributor    = Keystone
| released       =  
| runtime        = 12 minutes
| country        = United States
| language       = Silent (English title cards)

}}
A Dash Through the Clouds is a 1912 short American silent comedy film directed by Mack Sennett, written by Dell Henderson and starring Mabel Normand. It has the distinction of being somewhat of an aviation film as Sennett employed the services of real life aviation pioneer, Philip Parmalee, a pilot for the Wright Brothers. Parmalee is preserved on film here as he was killed in a crash not long after making this film.

Sennett had a penchant for working with and showcasing real-life daredevils and public personalities and hired a real-life pilot, Phil Parmalee, who was a national hero at the time. He would employ Barney Oldfield in a similar fashion a year later in Barney Oldfields Race for a Life.

==Cast==
*Mabel Normand - Josephine
*Fred Mace - Arthur (aka Chubby)
*Philip Parmalee - Slim, the aviator
*Sylvia Ashton - Carmelita, Young Mexican Woman
*Jack Pickford - Mexican boy who warns Chubby
*Kate Bruce - Old Mexican Woman

other cast
*William J. Butler - Townsman Edward Dillon - Carmelitas Objecting Relative Charles Gorman - Townsman
*Grace Henderson - Townswoman
*Harry Hyde - Townsman
*J. Jiquel Lanoe - Townsman
*Alfred Paget - Townsman

==References==
 

==External links==
* 
*  available for free download at  

 
 
 
 
 
 
 
 
 
 
 


 