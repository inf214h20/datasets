Dean Spanley
 
{{Infobox film
| name           = Dean Spanley
| image          = Dean Spanley.jpg
| alt            =
| caption        = UK quad format cinema poster
| director       = Toa Fraser
| producer       = Matthew Metcalfe Alan Harris
| screenplay     = Alan Sharp
| based on       =  
| starring       = Jeremy Northam Peter OToole Sam Neill Bryan Brown Judy Parfitt Dudley Sutton
| music          = Don McGlashan
| cinematography = Leon Narbey
| editing        = Chris Plummer
| studio      = NZ Film Commission Atlantic Film Group General Film Corporation Lipsync Productions
| distributor    =Icon Film Distribution   Paramount Pictures   Miramax Films  
| released       =  
| runtime        = 100 minutes
| country        = New Zealand United Kingdom
| language       = English
| gross          =
| budget         = $15&nbsp;million
}} fantastic elements, Irish author Lord Dunsanys short novel My Talks with Dean Spanley, and stars Sam Neill as the Dean, Jeremy Northam and Peter OToole as Fisk Junior and Fisk Senior respectively and Bryan Brown as Wrather.

==Plot==
The screenplay is an adaptation of fantasy author Lord Dunsanys My Talks with Dean Spanley, a 14-chapter novella published in 1936. It is set in Edwardian England.

The narrative is called "a surreal period comedic tale of canine reincarnation exploring the relationships between father and son and master and dog".  Peter OToole said that the films use of comedy to explore the relationship between a father and son was part of the attraction for him: "All of us have had these difficult familial relationships and I think its a film for all of us who understand the relationship between a father and son. Its been interesting watching how various members of the crew have been looking at the monitors during scenes, because they come up to me and say, I had the same thing with my father." 

===Storyline===
In the very early 1900s, Henslowe Fisk lives beholden to his father, the difficult Horatio Fisk. The Fisk family has suffered first the loss of its younger son, Harrington Fisk (Xavier Horan), killed in the Second Anglo-Boer War, shortly followed by the death of Horatios wife. Fisk Senior is looked after by his housekeeper Mrs Brimley (Judy Parfitt) who has lost her husband. Fisk Junior reluctantly visits his father every Thursday.
 transmigration of souls. The lecture is also attended by the new local clergyman, Dean Spanley (Sam Neill).
 Tokay wine, Welsh Spaniel. These memories are acute and convincing, including rich feelings around food and communication with other canines, a deep distaste for cats and pigs, and the joy of serving his master. As the story unfolds, Fisk Junior comes to understand his fathers background better and the two draw closer. There is a sub-plot concerning Fisk Seniors childhood that receives an unexpected resolution forming the climax of the story.

==Main cast==
* Jeremy Northam as Fisk, Junior (Henslowe) (also narrator)
* Peter OToole as Fisk Senior (Horatio)
* Sam Neill as the Dean
* Bryan Brown as Wrather
* Judy Parfitt as Mrs Brimley
* Dudley Sutton as Marriott
* Art Malik as Swami Nala Prash  
* Charlotte Graham as the woman in the park
* Eva Sayer as a girl
* Elizabeth Goram-Smith as a young lady of stature
* James Lever as the cricketer
* Ramon Tikaram as the Nawab of Ranjiput

==Production==

===Optioning===
The novella was optioned from the Dunsany Will Trust through Curtis Brown of London by Alan Sharp. Margaret Pomeranz speaking with Sam Neill. {{cite episode
 | title = Dean Spanley Interview
 | url = http://www.abc.net.au/atthemovies/txt/s2506958.htm
 | series = At the Movies
 | serieslink = At the Movies (Australian TV series)
 | airdate = 4 March 2009
 | season = 6
 | number = 4
}}
   Support for the production came from both English (Screen East) and New Zealand (NZ Film Commission) government agencies, with financing completed by Aramid Entertainment, General Film Corporation and Lipsync Productions. Both producers, the director, some of the lead cast (Neill was born in Northern Ireland but is associated with New Zealand), the cinematographer, the editor, the composer and a number of other members of the production crew and cast are from New Zealand.

===Writing===
The adapted screenplay was written by Alan Sharp, with clearance from the Dunsany Literary Estate. Trevor Johnston has written, "If you read the original story before seeing the film ..., then see the film, what’s striking is that Sharp has not so much effected an adaptation as a reinvention." 

===Casting===
Led by Daniel Hubbard, the studio cast leading talent Sam Neill, Jeremy Northam, Bryan Brown and Peter OToole, along with a range of experienced actors.

===Locations=== the last Elm Hill in Norwich with its mixture of medieval, Tudor, Victorian and Edwardian buildings, as used in the Dunsanyesque box-office success Stardust (2007 film)|Stardust, as well as Norwich Cathedral cloisters.    Further filming took place in New Zealand.

===Technology===
The movie was shot on 16&nbsp;mm film and digitally, in 1:1.85 ratio, using Arri 416 and Arriflex D-20|D-20 cameras, with digital intermediate post-production by Lipsync Productions. 

===Music===
An original soundtrack was composed by New Zealand composer Don McGlashan. A soundtrack CD was released in New Zealand on Warner Music (NZ) 5186531802 consisting of 14 tracks and a running time of 41:05. 
Background choir music was provided by the 30-voice New Zealand choir Musica Sacra. 

==Release and reception==
Ahead of general release, Dean Spanley was shown twice at the 2008 Toronto International Film Festival, where it received a red-carpet gala premiere, the first New Zealand production ever to do so. {{
cite web | url = http://www.thebigidea.co.nz/article.php?sid=5939&mode=&order=0
 | title = Toronto Gala world premiere for Dean Spanley }} 
It also had two showings at the London Film Festival, one attended by the cast and closing with a standing ovation.   Dean Spanley was also shown at the largest film festival in Asia, the Pusan International.
 British Board of Film Classification.  Ireland it was certified "G" and was released on the same date. The film was certified in Australia as "G" also, and was released 5 March 2009, and in New Zealand 26 February 2009; distribution in both Australia and New Zealand was by Paramount.

In early November, the film was offered to USA distributors at the annual American Film Market (5–12 November), with two showings announced, and in early February 2009, Miramax bought the USA rights.  However, rather than opening in theatres in the U.S., it went straight to cable.

 
A region-2 DVD was released in 2009.  A region-1 DVD was released in 2010. 

The novella, out of print for some years, was re-issued from HarperCollins in 2008, with screenplay, set photos, publicity stills, cast interviews, and interviews and comments from the director, producers and crew members.

=== Critical response ===
 
Receiving a standing ovation at the gala premiere, initial commentary was positive (per reviews at IMDb.com and elsewhere), with particular praise for OTooles performance and the final "act".

Reviews were generally positive, Rotten Tomatoes website gives the film a rating of 85% "fresh" based on 26 reviews. The critical consensus describes the film as "Offbeat, whimsical, period-set shaggy dog story with daffy performances from Sam Neill and Peter O’Toole." 

 

Dean Spanley was longlisted for the 2009 British Academy of Film and Television Arts awards for Adapted Screenplay (Alan Sharp) and Supporting Actor (Peter OToole). 

==References==

 

==External links==
*  
*   at the NZ Film Commission
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 