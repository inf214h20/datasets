Dr. Dolittle 3
 
{{Infobox film
| name = Dr. Dolittle 3
| image = Dr Dolittle 3.jpg
| caption = DVD cover
| director = Rich Thorne John Davis
| writer = Nina Colman Kristen Wilson James Kirk Ecstasia Sanders
| music = Christopher Lennertz
| cinematography = Eric J. Goldstein
| editing = Tony Lombardo
| studio = Davis Entertainment
| distributor = 20th Century Fox Home Entertainment
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget =
}}
 
 family comedy Kristen Wilson as Lisa Dolittle and Norm Macdonald as the voice of Lucky the Dog.

Despite his absence, Eddie Murphys character was mentioned several times throughout the film. It is implied that he is away on business. Raven-Symonés character was only mentioned during a phone call to the older Doctor Dolittle near the beginning of the film despite her absence.

==Plot==
   first film. Though she was then an antisocial individual more interested in her science projects, Maya has transformed into the typical teenager. Like her sister Charisse, she inherits their father Johns capacity for communicating with fauna (she is a part-time veterinary assistant), her life is turned upside down on all fronts. She routinely lands in trouble with her parents, while her friends think shes gone crazy.

With John away on animal expeditions, Mayas mother Lisa sends her (with Lucky following along) to a ranch named "Durango", so she can find herself. The ranch is owned by Jud (John Amos), and his son Bo (Walker Howard). While there, Maya, who desperately tried to keep it under wraps so as not to arouse suspicion, uses her talent to "talk to the animals" in order to save Durango from being taken over by a neighboring ranch.

Maya is at first reluctant to reveal her ability, fearing rejection from her friends, but eventually does so. With her help, the Durango ranch enters a rodeo competition with a $50,000 award, and wins it. Also, she shares her first kiss with Bo and finally wins his heart.

==Cast==
* Kyla Pratt as Maya Dolittle
* Chelan Simmons as Vivica 
* Kristen Wilson as Lisa Dolittle
* John Amos as Jud
* Walker Howard as Bo
* Ecstasia Sanders as Tammy
* Calum Worthy as Tyler

===Voices===
* Norm Macdonald as Lucky (uncredited)
* Danny Bonaduce as Ranch Steer
* Gary Busey as Butch
* Melanie Chartoff as Black & White Hen
* Paulo Costanzo as Cogburn the Rooster
* Chris Edgerly as Diamond the Horse, LP the Horse, Pig, Rattlesnake
* Eli Gabay as Rodeo Bull, Rodeo Steer
* Vanessa Marshall as Tan Hen, White Hen
* Mark Moseley as Harry the Hawk, Patches the Horse, Ranch Steer, Rodeo Longhorns, Silver the Horse
* Jenna von Oÿ as Gracie
* Philip Proctor as Drunk Monkey, Stray Dog
* Susan Silo as Mary the Goat
* Maggie Wheeler as Brown Hen, Fluffy Hen

==Releases== Region 1 Region 2.

==Reception==
Although a relative success in sales, the film was a not well received by critics.

Of the two reviews given at Rotten Tomatoes, both were very negative:  one critic, Scott Weinberg, said "Cheap-looking, atrociously written, and delivered with all the energy of a breach-birth bovine, Dr. Dolittle 3 is all kinds of terrible." David Cornelius of efilmcritic.com described the film as "not so much poorly made as it is lazy and cheap."

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 