Maattukara Velan
{{Infobox film
| name           = MAATTUKARA VELAN
| image          = Maattukara Velan.jpeg
| caption        = 
| director       = P. Neelakantan
| producer       = N. Kanagasabai
| writer         = A. L. Narayanan Lakshmi S. A. Ashokan V. K. Ramasamy (actor)|V. K. Ramasamy Cho Ramaswamy
| music          = K. V. Mahadevan
| cinematography = V. Ramamoorthy
| editing        = K. Narayanan
| studio         = Jayanthi Films
| distributor    = Jayanthi Films
| released       =  
| runtime        = 166 minutes
| country        = India
| language       = Tamil
}} 1970 Tamil Lakshmi in lead roles, while S. A. Ashokan was villain.

==Plot==

Following a series of quiproquos, a brave cowherd, Vélan (MGR), illiterate, is taken for a brilliant young lawyer Raghu (alias Raghunath Sabhabadhi) (MGR), by them disturbing physical resemblance.

What appeared at first, simple and funny, gets more complicated and complicates seriously when both men begin to love, each from their part, their elected representative of their heart...

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran ||  as Velan and Raghu, (alias Raghunath Sabhabadhi)
|-
| J. Jayalalitha || as Lalithaa Sathiyanadhan, Velans Lover
|- Lakshmi ||  as Kamala Nagalingham, Raghus Lover
|-
| S. A. Ashokan ||  as The Chairman Nagalingham
|-
| V. K. Ramasamy (actor)|V. K. Ramasamy ||  as The Great Advocat R.K.Sathiyanadhan MABL
|-
| Cho Ramaswamy || as Sundharam, The Chairman Nagalingham s son
|-
| S. Varalakshmi || as Annapourni, Advocat R.K.Sathiyanadhan s wife
|-
| Sachu || as Kavéri, the handmaid
|-
| S. N. Lakshmi || as Raghunath Sabhabadhi s mother
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

The film, had musical score by K. V. Mahadevan.

It exists in 4 combination MGR - Pa. Neelakandhan - K.V.Mahadevan : KODUTHU VAITHAVAL ( 1963 ), in 1970, MAATTUKAARA VELAN and EN ANNAN and finally NETRU INDRU NAALAI ( 1974 ), "with the song, Nerungi nerungi pazhagum pothu..."

The film was highly positive reviews from audience and became blockbuster at box-office.

The film was big box-office hit of the year. 

The film had 400 housefull shows in the chennai city alone which was record. 

The movie was re-released in cinemascope and the print was good and the evening show was housefull.

Jigri Dost is the Hindi version of 1969, with Jeetendra in the double-role. Release Date: 16 August 1969

==Soundtrack==
The music composed by K. V. Mahadevan, while lyrics written by Kannadasan and Vaali (poet)|Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss)
|-
| 1 || Sathiyam Neeye || T. M. Soundararajan || 04:15
|-
| 2 || Oru Pakkam Parkkiral || T. M. Soundararajan, L. R. Eswari || 03:40
|-
| 3 || Poo Vaitha || T. M. Soundararajan, P. Susheela, L. R. Eswari || 04:13
|-
| 4 || Thottu Kollava || T. M. Soundararajan, P. Susheela || 03:05
|-
| 5 || Pattikada Pattanamma || T. M. Soundararajan, L. R. Eswari || 05:24
|-
| 6 || Oor Padam Thalattu || P. Susheela || 04:02
|}

==References==
 

==External links==
 

 
 
 
 
 