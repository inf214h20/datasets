Night World (1932 film)
{{Infobox Film
| name           = Night World
| image          = Night World 1932 film.jpg
| image_size     = 
| caption        = 
| director       = Hobart Henley
| producer       = Carl Laemmle, Jr.
| writer         = Allen Rivkin (story) P. J. Wolfson (story) Richard Schayer
| starring       = Lew Ayres Mae Clarke Boris Karloff
| cinematography = Merritt B. Gerstad
| editing        = Maurice Pivar
| distributor    = Universal Pictures
| released       = May 5, 1932
| runtime        = 58 mins.
| country        =   English 
}}
Night World (1932 in film|1932) is a pre-Code drama film featuring Lew Ayres, Mae Clarke, and Boris Karloff.  The supporting cast includes Hedda Hopper and George Raft.

==Production background==
The movie was directed by Hobart Henley and features an early Busby Berkeley music number, "Whos Your Little Who-Zis". Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 26  

==Plot== gay "queen". Although Karloff is a villain, he plays a charming man, quite unlike most of the parts he was allowed to play at the time.

==Cast==
* Lew Ayres as Michael Rand
* Mae Clarke as Ruth Taylor
* Boris Karloff as "Happy" MacDonald
* Dorothy Revier as Jill MacDonald
* Hedda Hopper as Mrs. Rand
* Louise Beavers as the maid
* George Raft as Ed Powell

==See also==
* List of American films of 1932
* Boris Karloff filmography

==References==
 

==External links==
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 

 