Indian Rupee (film)
 
 
 
{{Infobox film
| name = Indian Rupee
| image = Indian Rupee film.jpg
| caption = Theaterical poster Ranjith Balakrishnan
| producer = Shaji Nadesan Prithviraj Sukumaran Santhosh Sivan
| writer = Ranjith Balakrishnan
| starring = Prithviraj Sukumaran Thilakan Rima Kallingal Tini Tom Jagathy Sreekumar
| music = Shahabaz Aman
| cinematography = S Kumar
| editing = Vijay Shankar
| studio = August Cinema
| distributor = August Films
| released =  
| runtime = 155 minutes
| country = India
| language = Malayalam
| budget =
| gross =
}} Indian satirical Ranjith Balakrishnan, and produced by August Cinema. The film stars Prithviraj Sukumaran, Thilakan, Tini Tom and Jagathy Sreekumar in significant roles.
 National Film Best Feature Best Film .

==Plot==
Indian Rupee is a satirical dig on contemporary youth who attempt to make quick money without sweating it out. Jayaprakash or JP (Prithviraj Sukumaran) is a small time real estate dealer based in Kozhikode|Calicut, who dreams of making it big some day. JP is a school drop-out and in love with his cousin Beena (Rima Kallingal) who is a doctor, though her parents are not too keen about the marriage. JP along with his partner CH (Tini Tom) is scouting for a land deal. He is working under a senior agent Rayeen (Mamukoya) but wishes to break free as soon as possible. He believes that his fortunes are up for a change, when Achutha Menon (Thilakan), an old city dweller approaches him to sell off his land assets. The deal never happens, but Achutha Menon stays back with them and becomes a part of their life. Things start happening in a fantastic way for JP, soon after. JP manages to earn a decent fortune after some smart moves, but he learns some valuable lessons in the process, about the importance of earning money honestly in life.

==Cast==
* Prithviraj Sukumaran as Jayaprakash or JP
* Thilakan as Achutha Menon
* Rima Kallingal as Beena
* Tini Tom as C Hameed or CH
* Jagathy Sreekumar as "Gold" Pappan
* Lalu Alex as Surendran Kalpana as Mary
* Revathi as Dr. Sheela Koshy
* Mamukkoya as Rayeen
* Biju Pappan as Joy
* Sadiq Mallika as Saji (Jayaprakashs sister)
* Zeenath as Jayaprakashs mother
* Babu Namboothiri (cameo)
* Irshad
* Joju George
* Shammi Thilakan Augustine
* T. P. Madhavan
* Sasi Eranjikkal
* Sasi Kalinga
* Asif Ali as a land broker (cameo)
* Fahadh Faasil as Muneer, a land broker (cameo)

==Soundtrack==
{{Infobox album
| Name = Indian Rupee
| Type = soundtrack
| Artist = Shahabaz Aman
| Cover =
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length = 17:00
| Label = Mathrubhumi Music
| Producer = Shahabaz Aman
}}

The films soundtrack, which included four songs, was composed by Shahabaz Aman. The audio release function was held at Puliyarmala Krishna Gowdar hall, Kalpetta, Wayanad on 23 August 2011.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)!! Lyricist !! Duration
|-
| 1
| "Pokayayi" G Venugopal, Asha G Menon VR Santhosh
| 3:36
|-
| 2
| "Anthimanam" MG Sreekumar, Sujatha Mohan VR Santhosh
| 4:10
|-
| 3
| "Ee Puzhayum"
| Vijay Yesudas Mullanezhi
| 4:40
|-
| 3
| "Ee Puzhayum" (unplugged)
| Vijay Yesudas Mullanezhi
| 4:24
|}

==Reception== Ranjith Balakrishnan in presenting such an "oft-told story" in such a refreshing way. {{cite web url = http://www.sify.com/movies/indian-rupee-review-malayalam-14980724.html title = Indian Rupee Review publisher = Sify.com date = 7 October 2011 accessdate =7 October 2011}}
 

Deccan Chronicle said, "Touching a thriving social chord of life, Indian Rupee is a movie minus drama packed with the intricacies of lifes realities."   

Rediff.com observed – "Indian Rupee should be seen by people who think that good stories have vanished from films. Ranjith has just told the story of contemporary Kerala society in a most interesting way."

Nowrunning.com commented: "The details are downright gripping, the asides sparkling and the setups almost surreal, so much so that Indian Rupee hops straight into the years must-see movies list and scrambles right up to the top". the site added that it is as easily one of the best performances from Prithviraj, and says that "Indian Rupee would as much be remembered for Prithviraj the actor, as the actor would be remembered for the film." {{cite web url = http://www.nowrunning.com/movie/8825/malayalam/indian-rupee/3371/review.htm title = Indian Rupee Review publisher = Nowrunning.com date = 7 October 2011 accessdate =7 October 2011}}
 

Sify.com wrote -"Though it is a bit too melodramatic and even preachy at times, Indian Rupee is easily one of the finest films of the year. It has honesty written all over it and such efforts need to be seen and appreciated by the audience."   

==Accolades== National Film Awards (2011) Best Feature Film in Malayalam
 Kerala State Film Awardss (2011) Best Film

;Filmfare Awards South Best Supporting Actor - Malayalam - Thilakan Best Playback Singer (Male) - Malayalam - "Ee Puzhayum" - Vijay Yesudas

; Asianet Film Awards Ranjith Balakrishnan 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 