Command Decision (film)
 
{{Infobox film
| name           = Command Decision
| image          = Command Decision 1948 poster.jpg
| image_size     = 225px
| caption        = theatrical Poster
| director       = Sam Wood Sidney Franklin Gottfried Reinhardt
| writer         = William Wister Haines (play) George Froeschel William R. Laidlaw
| starring       = Clark Gable Walter Pidgeon Van Johnson Brian Donlevy
| music          = Miklós Rózsa
| cinematography = Harold Rosson
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $2,467,000  .  gross = $3,685,000 
}}
 stage play of the same name written by William Wister Haines, which he based on his best-selling novel.  The screenplay for the film was written by George Froeschel and William R. Laidlaw.  Haines play ran on Broadway for almost a year beginning in October 1947. 

Although portraying the strategic bombing of Nazi Germany in World War II, the film has virtually no action scenes, taking place almost entirely within the confines of the headquarters of its protagonist. Depicting the political infighting of conducting a major war effort, the films major theme is the emotional toll on commanders from ordering missions that result in high casualties, the effects of sustained combat on all concerned, and the nature of accountability for its consequences.

==Plot== Ministry of RAF representative PRO of the Eighth Air Force causes grumbling when his report reveals a record 48 bombers shot down bombing an undisclosed industrial target. Carwood questions whether any target could be worth such losses, but Brockhurst retorts that the U.S. 5th Bomb Division commander, Brig. Gen. "Casey" Dennis (Clark Gable), loves the war. Brockhurst travels to the base of the 32nd Bomb Group, where Dennis has his headquarters, and observes Boeing B-17 Flying Fortress|B-17s taking off on another major strike. He tries to milk information about the arrest of a decorated (and highly publicized) pilot, Captain Jenks, from T/Sgt. Evans (Van Johnson), an assistant in Denniss office, but Evans cordially rebuffs him.
 West Point Pentagon to congressional committee is due, while Garnet pleads for low loss missions because a Global Allocation conference at the Pentagon in three days might curtail more bombers.
 German jet fighter before it can go into service and crush American strategic bombing.  A third city, Fendelhorst, must also be attacked, and Denniss is the only bomb division able to reach these targets beyond the range of escorting U.S. fighters. A rare stretch of clear weather, about to end, presents an opportunity to complete the operation before the Luftwaffe can mount an impenetrable defense.
 Edward Arnold) of the visiting Military Affairs Committee. Kane gives permission to continue Stitch while Dennis agrees to award Jenks (who refused to fly the mission to Schweinhafen) a medal during Malcolms visit. While Kane wines and dines the Committee, Garnet offers Martin the job of chief of staff (and a promotion) in the Boeing B-29 Superfortress|B-29 command in the Pacific that Garnet believes he is in line for, unbeknownst to Dennis.

The next day the Committee is impressed by the takeoff of the mission, led by Martin, but back in headquarters, Malcolm bitterly accuses Dennis of recklessly causing heavy losses. As tensions rise, Evans uses political savvy to ease the situation. Martin sends the signal that Schweinhafen has been destroyed, but during the ceremony to decorate Jenks, Dennis is brought a message that Martins B-17 (and the crew of Capt. Jenks) is shot down. Malcolm renews his tirade against Dennis. Jenks unexpectedly tells his uncle to shut up and refuses his medal.  Dennis, emotionally shaken by Martins death, excuses himself to plan tomorrows mission. Kane is shocked that despite everything, Dennis plans to hit the final German jet factory target Fendelhorst. Kane relieves Dennis of command and replaces him with Garnet. While Garnet queries his staff about ordering an easy mission for the next day, he comes to the realization that Dennis hated every minute of his duties. Garnet makes the command decision to attack Fendelhorst while the weather permits. Dennis looks forward to a training command in the United States, where he can be near his family, but a message from the Pentagon orders him to the Pacific and the new B-29 command. Brockhurst, their differences ended by all he has observed, wishes Dennis well as he boards his aircraft.

==Cast==
  
* Clark Gable as Brig. Gen. K.C. "Casey" Dennis
* Walter Pidgeon as Maj. Gen. Roland Goodlaw Kane 
* Van Johnson as T/Sgt. Immanuel T. Evans 
* Brian Donlevy as Brig. Gen. Clifton I. Garnet
* Charles Bickford as Elmer Brockhurst
* John Hodiak as Col. Edward Rayton "Ted" Martin Edward Arnold as Congressman Arthur Malcolm
* Marshall Thompson as Capt. George Washington Bellpepper Lee 
* Richard Quine as Maj. George Rockton Cameron Mitchell as Lt. Ansel Goldberg
 
* Clinton Sundberg as Maj. Homer V. Prescott Ray Collins as Maj. Desmond Lansing
* Warner Anderson as Col. Earnest Haley
* John McIntire as Maj. Belding Davis
* Moroni Olsen as Congressman Stone
* John Ridgely as James Carwood
* Michael Steele as Capt. Lucius Jenks 
* Edward Earle as Congressman Watson
* Mack Williams as  Lt. Col. Virgil Jackson
* James Millican as  Maj. Garrett Davenport
 

==Production== San Fernando Valley Airport]]

Clark Gable joined the   Lt. Goldberg, was also a bombardier during the war.
 Command Decision, James Whitmore was contracted to MGM, although Van Johnson played the role in the film.   Barry Nelson provided the uncredited radio voice ("Cumquat B-Boy") of B-17 pilot bringing in his bomber after a raid with wounded aboard.

MGM bought the rights to William Wister Haines 1947 novel Command Decision at the behest of Clark Gable, who saw in it a starring role for himself.  MGM paid a $100,000 down payment, which would escalate to $300,000 if the novel were staged as a play by October 1947.  In the event, the play opened on Broadway on October 1 of that year.   Turner Classic Movies. Retrieved: June 7, 2013. 
 Robert Taylor March Air San Fernando Van Nuys, California.   aerovintage.com. Retrieved: April 25, 2009.  

The film uses extensive cuts of archived footage shot during the war, but all of it is of the many varied aspects of mission preparation, takeoffs and landings. Except for the sequence under the opening credits, of bomber formations leaving voluminous contrails and then dropping bombs, no combat footage is used in the movie. Only two exteriors were used, of Brockhurst driving up to the main gate of the base in a jeep, and of Martin saying farewell to Dennis at his bombers dispersal hardstand, totalling little more than a minute of film. The films only action scene involves closeups of Dennis "talking down" a bomber piloted by a bombardier. 

Director Sam Wood acknowledged the limitations of filming a stage play, shooting all scenes from a "relentlessly ground level", and used master shots and single-camera group shots that allowed the actors to use the choreography of the theatrical play to establish dramatic and moral relationships. Heath, Roderick.   This Island Rod, February 17, 2009. Retrieved: May 29, 2009. 

==Reception==
The premiere of Command Decision took place in Los Angeles on December 25, 1948, and the film went into general release in February 1949.   The premiere in Washington D.C., which took place sometime in February,
was attended by Vice President Alben W. Barkley, Secretary of State Dean Acheson, the Air Force Chief of Staff General Hoyt S. Vandenberg and other dignitaries. 

Command Decision was successful at the box office in 1949  earning $2,901,000 in the US and Canada and $784,000 elsewhere. However due to its high cost, MGM recorded a loss of $130,000 on the movie. 

It was named as one of the ten best films of 1948 by the New York Times and by Film Daily.  Critical review centered on the key dramatic elements of the film, especially concerning the human factors involved in making command decisions. Bosley Crowther noted: "... it is the performance of Clark Gable in this scene of a soldiers momentary grieving that tests his competence in the leading role. For this is not only the least likely but it is the most sentimental moment in the film, and the fact that Mr. Gable takes it with dignity and restraint bespeaks his worth. Otherwise, he makes of General Dennis a smart, tough, straight-shooting man, disciplinary yet human and a "right guy" to have in command."   Still, there were other reactions: in August 1949, the Los Angeles Times reported that a syndicated British film reviewer had called the film as ""insult to British audiences" on the basis that it gave the audience the idea that American precision bombing had won the war. 

The film did not receive any awards, although writers William Laidlaw and George Froeschel were nominated for two Writers Guild Awards, for "Best Written American Drama" and the Robert Meltzer Award for the "Screenplay Dealing Most Ably with Problems of the American Scene". 

On March 3, 1949, Clark Gable, Van Johnson, Walter Pidgeon, Brian Donlevy, John Hodiak, Edward Arnold and Richard Quine reprised their film roles in a 30-minute radio version of Command Decision for the NBC radio network program Screen Guild Theater, the first pre-recorded commercial show to be broadcast over the network from Hollywood.

==DVD release==
Command Decision was released on DVD on 5 June 2007 in the United States.

==References==

Explanatory notes
 

Citations
 

Bibliography
 
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Haines, William Wister. Command Decision: Five Great Classic Stories of World War II. New York: Dodd, Mead, 1980. ISBN 978-0-396-07873-9.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
* Parish, James Robert. The Great Combat Pictures: Twentieth-Century Warfare on the Screen. Metuchen, New Jersey: The Scarecrow Press, 1990. ISBN 978-0-8108-2315-0.
*  Reid, John Howard. "The Top Movies of 1949". Success in the Cinema: Money-Making Movies and Critics Choices.  Raleigh, North Carolina: Lulu.com, 2006. ISBN 978-1-84728-088-6.
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 