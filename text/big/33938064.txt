Wildfire (1925 film)
{{infobox film
| name           = Wildfire
| image          =
| imagesize      =
| caption        =
| director       = T. Hayes Hunter
| producer       = Distinctive Productions
| based on       =  
| writer         = 
| starring       = Aileen Pringle
| music          =
| cinematography = J. Roy Hunt
| editing        =
| distributor    = Vitagraph Company of America
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}}
Wildfire is a 1925 silent film produced by Distinctive Productions(a company founded by George Arliss) and distributed by the Vitagraph Company of America.  It is based on a successful 1908 play Wildfire that had starred Lillian Russell on Broadway and a young actor just starting out named Irving Cummings, later a silent director. This film stars Aileen Pringle. The story had been filmed before in 1915 with Lillian Russell herself and Lionel Barrymore.  This film survives in the Library of Congress collection in a nitrate print and at UCLA Film and Tv archive.     

==Cast==
*Aileen Pringle - Claire Barrington
*Edna Murphy - Myrtl Barrington
*Holmes Herbert - Garrison
*Edmund Breese - Sen. Woodhurst
*Antrim Short - Ralph Woodhurst
*Tom Blake - Matt Donovan
*Lawford Davidson - John Duffy
*Arthur Bryson - Chappie Raster
*Will Archie - Bud
*Edna Morton - Hortense
*Robert Billoupe - Valet

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 