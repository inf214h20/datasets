Man in the Middle (film)
{{Infobox film
| name           = Man in the Middle
| image          = "Man_in_the_Middle"_(1964).jpg
| image_size     = 
| border         = 
| alt            = The Winston Affair 
| caption        = British quad poster
| director       = Guy Hamilton
| producer       = Walter Seltzer
| writer         = 
| screenplay     = Willis Hall Keith Waterhouse
| story          = 
| based on       =  
| narrator       =  Barry Sullivan John Barry
| cinematography = Wilkie Cooper John Bloom
| studio         = Talbot Productions
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 94 min.
| country        =   English
| budget         = 
| gross          = $1,000,000 (US/ Canada) 
}}
 American Army British soldier. Mitchum plays Lieut. Col. Barney Adams, who has been assigned as the accused mans defense counsel. The film is also known as The Winston Affair, the title of the novel the film was based on, which was written by Howard Fast.

==Plot summary==
In the midst of World War II, Lieut. Col. Barney Adams’s superiors call on him to defend troubled US army Lieutenant Winston who has confessed to murdering a British non-commissioned officer. Military court officials want the cleanest possible trial for the lieutenant. They believe that Liet. Col. Adams, a war hero and distinguished lawyer, is the best man for the job. But when Adams begins to investigate the murder, he finds that this seemingly open-and-shut case is actually much more complicated. Before long he is absorbed in a dramatic struggle for a fair trial against the most overwhelming odds.

Friction develops between American and British troops stationed in India during World War II and culminates in physical outbreaks between the troops when Lieutenant Winston, an American, shoots British Staff Sergeant Quinn before 11 witnesses. American General Kempton assigns Lieut. Col. Barney Adams to defend Winston at his court-martial, assuring him that the Army Lunacy Commission has found Winston fit and sane.

Adams is informed by nurse Kate Davray that Colonel Burton, who headed the lunacy commission, refused to accept the report of the hospitals psychiatric head, Dr. Kaufman, who believes Winston is a psychopath. Burton is anxious to have Winston convicted and hanged to patch the strained relations between the two forces. Adams instructs Kaufman to bring his report to the trial, but when Burton is informed of this order he transfers Kaufman to a distant hospital. Adams visits British Major Kensington, this qualified psychiatrist also considers Winston to be psychopathic but has been warned not to interfere. Kensington believes Winston killed Quinn out of a feeling of victimization because Quinn, a sergeant, had the same duties as Winston, a lieutenant.

Winston, in an interview with Adams, raves that he killed Quinn for defiling the white race by consorting with a black woman. Though he despises Winston, Adams refuses to rig the trial, and he holds back his defense, waiting for Kaufman to arrive as a witness. When he learns that Kaufman has been killed in an accident on the way to the trial, Adams calls Kensington to the stand after establishing that no member of the lunacy commission is a qualified psychiatrist. As Kensington describes Winstons mental illness to the court, Winston cracks and begins raving. Adams wins his case and spends a few days of peace and happiness with nurse Davray before leaving the area. The friction between the troops is eased, and they prepare to enter battle in complete unity.

==Cast==
 
* Robert Mitchum as Lieutenant Colonel Barney Adams  
* France Nuyen as Kate Davray   Barry Sullivan as General Kempton  
* Trevor Howard as Major Kensington  
* Keenan Wynn as Lieutenant Winston  
* Sam Wanamaker as Major Kaufman  
* Alexander Knox as Colonel Burton  
* Gary Cockrell as Lieutenant Morse   Robert Nichols as Lieutenant Bender  
* Michael Goodliffe as Colonel Shaw  
* Errol John as Sergeant Jackson  
* Paul Maxwell as Major Smith  
* Lionel Murton as Captain Gunther  
* Russell Napier as Colonel Thompson  
* Jared Allen as Captain Dwyer David Bauer as Colonel Mayburt 
* Edward Underdown as Major Wyclif 
* Howard Marion-Crawford as Major Poole
* Al Waxman as Corporal Zimmerman
* Terence Cooper as Major Clement
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 