Don't Look Down (film)
 
{{Infobox film
| name           = Dont Look Down
| image          = Dont Look Down DVD cover.jpg
| alt            =
| caption        = DVD cover Larry Shaw
| producer       = Ted Babcock Wes Craven Richard Fischoff Marianne Maddalena Ron McGee Peter Sadowski Robert M. Sertner Erik Storey Randy Sutter Frank von Zerneck
| writer         = Gregory Goodell Billy Burke
| music          = J. Peter Robinson
| cinematography = David Geddes
| editing        = Michael D. Ornstein
| studio         = Craven-Maddalena Films Von Zerneck Sertner Films ABC  (television airing) 
| released       = 29 October 1998  (original airdate) 
| runtime        = 90 minutes
| country        = United States
| language       = English
}}
 horror television Larry Shaw and produced by Wes Craven. It originally aired on 29 October 1998 on American Broadcasting Company|ABC. The film is about a woman who is struggling to cope with the death of her sister and joins a group for sufferers of acrophobia. However, it appears her problems may only be starting.

== Production ==
 Prince George in Canada.

== Plot ==

The film starts with Carla, her sister and her boyfriend on a road trip in the cliffs. After a freak accident when Carlas sister falls from a cliff, Carla develops a fear of heights. In an effort to overcome her phobia she joins a support group, but when the other members of the group begin dying one by one, Carla begins to suspect she was the real target of the accident.

== Cast ==

* Megan Ward as Carla Engel Billy Burke as Mark Engel
* Terry Kinney as Dr. Paul Sadowski

== External links ==

*  
*  
*  

 

 
 
 
 


 