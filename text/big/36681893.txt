Malin Kundang (film)
{{Infobox film
| name           = Malin Kundang
| image          = Malin Kundang poster.jpg
| image_size     = 
| border         = 
| alt            = 
| based on       = Malin Kundang  (folktale) 
| caption        = Theatrical poster
| director       = D. Djajakusuma
| producer       = Elly Yunara
| screenplay     =Asrul Sani
| story          = 
| starring       = {{plainlist|
*Rano Karno
*Putu Wijaya
*Fifi Young
}}
| music          = Frans Haryadi
| cinematography = Kasdullah
| editing        = Soemardjono
| studio         = Remadja Ellynda Film
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}} the folktale of the same name. It follows a young boy who forgets his roots after spending much of his childhood at sea. Starring Rano Karno, Putu Wijaya, and Fifi Young, 
==Plot==
Malin (Rano Karno) is a young boy who lives in Sumatra with his mother (Fifi Young) and sister; his father had disappeared at sea several years earlier. When a group of pirates land at the village, Malin catches the eye of their leader, Nakoda Langkap, who takes the boy as his own. As they are leaving the village, they pass the ship of another pirate, the slaverunner Nakoda Hitam. Nakoda Langkap and his men take the ship, wounding (evil pirate captain) and freeing the slaves. One, a boy named Lalang, is orphaned when his mother is killed in the battle; Nakoda Langkap takes him in as a son too.

Nakoda Langkap raises the boys to be good sailors and respect others. He also honours his promise to Malins mother, bringing the boys back to visit her after a hundred full moons have passed. The now-adult Malin (Putu Wijaya) is pleased to see his mother, promising that he would rather turn to stone than forget her, and Malins sister falls for Lalang. As they leave the village, they once again run across Nakoda Hitam. In the ensuing battle, Nakoda Langkap is killed and Nakoda Hitam is captured. Malin, now the leader, promises to spare Nakoda Hitam in return for the location of his buried treasure. Upon receiving the information, Malin surrenders Nakoda Hitam to the police then smiles when the pirate is hanging|hanged.

Malin turns out to be a cruel leader, drastically different than his adoptive father. While Lalang and the lieutenant look on, Malin follows one of Nakoda Hitams men to his secret island refuge, where he kills all present&nbsp;– including women. Although Malin finds the treasure, including a golden throne, several of his men rise against him, including Lalang; they are banished to the sea in a small boat, eventually landing in Malins village.

Several years later, Malin has become a rich man and is married to a Chinese princess. When they are sailing near the village, Malins wife asks for coconut water and to stretch her legs; reluctantly they put ashore, where Malin is accosted by his mother and Lalang. When he denies that she is his mother, the villagers and Malins men fight. As Malin and his wife escape, a storm breaks out and the ship collapses. The following morning, as Malins mother weeps, they discover that Malin has become stone.

==Production== Minang Malin folktale of the same name.  However, the film changed several aspects of the folktale, such as the ending. In the original story, Malin had turned to stone after his mother cursed him; however, Sani considered it illogical for a Muslim mother to curse her son, no matter what his sins. 

Funding for the film came from Elly Yunara, the wife of the recently decease film producer Djamaluddin Malik.  Her production house, Remadja Ellynda Film, produced the film.  It was directed by D. Djajakusuma,  who had previously had experience in adapting part of the Mahābhārata in Bimo Kroda (1967).  Cinematography was handled by Kasudllah,  while editing was done by  Soemardjono and music was supplied by Frans Haryadi. 

Rano Karno, son of the actor Sukarno M. Noor, was cast as a young Malin. He attended the audition with his friend Itang Yunasz and, displeased with the changes to the story, blurted out "Thats not how the story goes."  Djajakusuma was impressed and immediately cast him; it was Karnos first film role.  Putu Wijaya&nbsp;– better known for his work in theatre&nbsp;– was cast as the adult Malin.  Fifi Young, who had been active in film since 1940,  was chosen as Malins mother. 

==Release and reception==
Malin Kundang was released in 1971.  The Indonesian film critic Salim Said wrote that the first portion of the story was well-done, considering Rano Karno perfect for young Malin and the story well put together. However, he considered the choice of Wijaya to be a poor one, as the actor did not resemble Rano Karno in the slightest. 

==References==
Footnotes
 

Bibliography
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
* {{cite news
  | last1 = Hamdani
  | first1 = Syviana
  | date=12 October 2010
  | title =Indonesian Star Rano Karno Is One for the Books
  |work=Jakarta Globe
  | url =http://www.thejakartaglobe.com/lifeandtimes/indonesian-star-rano-karno-is-one-for-the-books/400933
  | accessdate =1 January 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/64MYlFKHE
  |archivedate=1 January 2012
  }}
* {{cite web
  | title =Kredit Lengkap
  |trans_title=Full Credits
  |language=Indonesian
  |work=Filmindonesia.or.id
  |publisher=Konfidan Foundation
  | url =http://filmindonesia.or.id/movie/title/lf-m023-71-925784_malin-kundang-anak-durhaka/credit
  | accessdate =10 August 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/69nmNbQUA
  |archivedate=10 August 2012
  }}
*{{cite news
 |last=Marselli
 |title=Mengenang D. Djajakusuma dalam Perfilman Indonesia
 |trans_title=Remembering D. Djajakusuma in Indonesian Cinema
 |language=Indonesian
 |work=Kompas
 |date=6 November 1987
 |page=6
 |ref= 
}}
*{{cite book
 |last=Said
 |first=Salim
 |title=Pantulan Layar Putih: Film Indonesia dalam Kritik dan Komentar
 |trans_title=Song of the White Screen: Indonesian Films in Critique and Commentary
 |language=Indonesian
 |publisher=Sinar Harapan
 |location=Jakarta
 |year=1991
 |ref=harv
 |isbn=978-979-416-137-1
}}
 

==External links==
* 

 
 
 

 
 