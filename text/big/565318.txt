Lost in Space (film)
 
{{Infobox film
| name = Lost in Space
| image = Lost_in_space_movie_poster.jpg
| alt =
| caption = Theatrical release poster Stephen Hopkins
| producer = Mark W. Koch Stephen Hopkins Akiva Goldsman Carla Fry
| writer = Akiva Goldsman
| based on =   Jack Johnson Jared Harris
| music = Bruce Broughton Peter Levy
| editing = Ray Lovejoy
| studio = Jim Hensons Creature Shop
| distributor = New Line Cinema
| released =  
| runtime = 130 minutes  
| country = United States
| language = English
| budget = $80 million
| gross = $136.1 million
}} Stephen Hopkins and starring Matt LeBlanc, Gary Oldman and William Hurt. The film was shot in London and Shepperton, and produced by New Line Cinema. The plot is adapted from the 1965–1968 CBS television series Lost in Space. The film focuses on the Robinson family, who undertake a voyage to a nearby star system to begin large-scale emigration from a soon-to-be uninhabitable Earth, but are thrown off course by a saboteur and must try to find their way home.

Several of the actors from the original TV series had cameos in the film.
	 
==Plot== colonization of a distant planet much like our own. Meanwhile, a terrorist group, the Global Sedition, wants to interrupt the colonization to be able to take over the same planet themselves. Professor John Robinson, lead scientist of the Jupiter Mission, prepares to take his wife Maureen, daughters Judy and Penny and son Will on a 10-year mission in suspended animation to the nearby planet Alpha Prime, where they will build a companion "Jumpgate|hypergate" to the one orbiting Earth. The project is accelerated after Global Sedition terrorists attack Earths hypergate, but are stopped by fighter pilots, one of whom is Major Don West. When the pilot for the Jupiter Mission is murdered, West is assigned as his replacement.
  spy employed alien primate that they adopt as a pet. They also find the aging ship is infested with carnivorous spider-like lifeforms, with the leg of one accidentally scratching Dr. Smith after it is cut off. To escape the spiders attack, Don reactivates and overloads the Proteus engine, with the shock wave from the resulting explosion damaging the Jupiter II and forcing it to crash on the planet below.
 
Professor Robinson and Major West go out onto the surface in search of radioactive material to replace the burnt-out part of the ships core. They find a strange, growing bubble of unknown origin, which they must enter. They learn that the "holes" and "bubbles" are distortions of time and space, caused by future versions of Will and Dr. Smith constructing a time machine. The future Will Robinson wants to travel back in time to prevent the Robinsons from ever taking off. Robinson and West find out the bubble is actually a small schism in time on the same planet. They are all betrayed by the future Smith, who has mutated due to the spider scratch going untreated. He plans to unleash an army of spiders on the Earth. Robinson battles with Spider Smith while Major West returns to the stranded Jupiter II to evacuate everyone. Robinson stops Spider Smith by tearing open his eggsac, freeing the baby spiders, who then attack the injured Spider Smith. John pushes the mutated Smith into the uncompleted time portal, where he is torn apart by the gravitational field.
 
The others attempt to escape the planet in the Jupiter II, as the time machines warping tears everything apart. They are unable to reach escape velocity and the ship is destroyed by flying debris. Future Will finally recognizes his fathers deep love for his family and allows him to travel back through the time portal to before the Jupiter II attempts to escape. Knowing that the escape velocity wont be enough, John commands West to pilot the ship through the planets core as it breaks up, using the planets gravity to propel the ship out the other side. They escape, but the collapsed planet forms a small black hole that begins to suck the Jupiter II back in. To escape, the Robinsons once again activate the hyperdrive, using the Alpha Prime navigational data from the Proteus to blast off again, into potentially unknown space.

==Cast==
* William Hurt as Professor John Robinson
* Mimi Rogers as Professor Maureen Robinson
* Heather Graham as Dr. Judy Robinson
* Lacey Chabert as Penny Robinson Jack Johnson as Will Robinson
* Jared Harris as Older Will Robinson
* Matt LeBlanc as Major Don West
* Gary Oldman as Dr. Zachary Smith / Spider Smith
* Dick Tufeld as the voice of the Robot
 TV show cameo as a Global Sedition representative who deals with Dr. Smith in the film. Billy Mumy was likewise offered a cameo, but turned it down after being told he would not be considered for the part he wanted—the role of the older Will Robinson—because he was told that would "confuse the audience."

==Music==
TVT Records released a soundtrack album on March 31, 1998, featuring eleven tracks of Bruce Broughtons original score (which makes no reference to either of the TV themes composed by John Williams) and eight tracks of techno music (most of which is heard only over the films end credits).  A European version of the soundtrack album was released that omits the tracks "Spider Attack", "Jupiter Crashes", and "Spider Smith" in favor of three new songs unused in the film by Aah-Yah, Asphalt Ostrich, and Anarchy (song)|Anarchy.  Intrada Records released a score album for the film the following year. (The track "Thru the Planet" on the TVT album is not the same as "Through the Planet" on the Intrada release, but is a shortened version of Broughtons unused end title music heard on the score album as "Lost in Space.")

===TVT soundtrack album===
{{Infobox album
| Name       = Lost in Space: Original Motion Picture Soundtrack
| Type       = Soundtrack
| Artist     = Various
| Cover      = 
| Released   =  
| Recorded   =
| Genre      = Film score
| Length     = 67:59
| Label      = TVT Records
| Producer   =
}}

{{Track listing
|collapsed=Yes
| headline        = Lost in Space: Original Motion Picture Soundtrack
| extra_column    = Artist

| title1          = Lost in Space – Theme
| extra1          = Apollo 440
| length1         = 3:27

| title2          = Im Here... Another Planet
| extra2          = Juno Reactor / The Creatures
| length2         = 4:21

| title3          = Busy Child
| extra3          = The Crystal Method
| length3         = 7:27

| title4          = Bang On
| extra4          = Propellerheads
| length4         = 5:47

| title5          = Everybody Needs a 303
| extra5          = Fatboy Slim
| length5         = 5:49

| title6          = Will & Pennys Theme
| extra6          = Apollo 440
| length6         = 3:22

| title7          = Song for Penny
| extra7          = Death in Vegas
| length7         = 5:35

| title8          = Lost in Space Space
| length8         = 3:30

| title9          = Main Title
| extra9          = Bruce Broughton
| length9         = 1:03

| title10          = Reprogram the Robot
| extra10          = Bruce Broughton
| length10         = 2:17

| title11          = The Launch
| extra11          = Bruce Broughton
| length11         = 4:14

| title12          = The Robot Attack
| extra12          = Bruce Broughton
| length12         = 2:54

| title13          = The Proteus
| extra13          = Bruce Broughton
| length13         = 2:26

| title14          = Spiders Attack
| extra14          = Bruce Broughton
| length14         = 2:26

| title15          = Jupiter Crashes
| extra15          = Bruce Broughton
| length15         = 1:17

| title16          = Spider Smith
| extra16          = Bruce Broughton
| length16         = 2:42

| title17          = Kill the Monster
| extra17          = Bruce Broughton
| length17         = 3:54

| title18          = The Portal
| extra18          = Bruce Broughton
| length18         = 2:46

| title19          = Thru the Planet
| extra19          = Bruce Broughton
| length19         = 2:42
}}

=== Intrada score album ===
{{Infobox album  
| Name       = Lost in Space: Original Motion Picture Score
| Type       = Soundtrack
| Artist     = Bruce Broughton
| Cover      = 
| Released   =  
| Recorded   =
| Genre      = Film score
| Length     = 67:03
| Label      = Intrada Records
}}

{{Track listing
|collapsed=Yes
| headline        = Lost in Space: Original Motion Picture Score
| extra_column    =
| total_length    =

| all_writing     =
| all_lyrics      =
| all_music       = Bruce Broughton

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Prologue
| length1         = 0:57

| title2          = Preparing for Space
| length2         = 2:31

| title3          = The Launch
| length3         = 6:22

| title4          = Robot Attack
| length4         = 3:21

| title5          = Into the Sun
| length5         = 6:21

| title6          = Spiders
| length6         = 10:22

| title7          = A New World
| length7         = 1:25

| title8          = Guiding Stars
| length8         = 1:37

| title9          = The Time Bubbles
| length9         = 2:21

| title10          = Smiths Plan
| length10         = 1:21

| title11          = Will and Smith Explore
| length11         = 2:00

| title12          = Wills Time Machine
| length12         = 4:24

| title13          = Spider Smith
| length13         = 2:39

| title14          = Facing the Monster
| length14         = 8:46

| title15          = Attempted Escape
| length15         = 1:26

| title16          = The Time Portal
| length16         = 2:42

| title17          = Through the Planet
| length17         = 2:31

| title18          = Back to Hyperspace
| length18         = 1:38

| title19          = Fanfare for Will
| length19         = 0:27

| title20          = Lost in Space
| length20         = 3:24
}}

==Reception==
On its opening weekend, Lost in Space grossed $20,154,919 and debuted at number one at the box office, ending Titanic (1997 film)|Titanic s 15-week-long hold on the first-place position. It opened in 3,306 theaters and grossed an average of $6,096 per screening. Lost in Space grossed $69,117,629 in the United States, and $67,041,794 outside of America, bringing its worldwide total to $136,159,423,  making it a moderate box office success. Those results were deemed insufficient, however, to justify a planned sequel.

Reviews were generally negative for Lost in Space, with a 27% approval rating on   from 19 critics. 
 Boxoffice magazine rated the film at 1 and a half out of 5, calling it "the dumbest and least imaginative adaptation of a television series yet translated to the screen."  James Berardinelli was slightly more favorable, giving the film a rating of 2 and a half out of 4. While praising the films set design, he criticized its "meandering storyline and lifeless protagonists," saying that "Lost in Space features a few action sequences that generate adrenaline jolts, but this is not an edge-of-the-seat motion picture." 
 Worst Remake The Avengers, Godzilla (1998 film)|Godzilla and Psycho (1998 film)|Psycho.

==Home video==
Both a DVD and later a Blu-ray have been released for the film.  Both contain deleted scenes that resolve the films storyline and plot holes suggesting that the film was drastically cut down for cinematic release, prompting a suggestion that a directors cut must exist. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 