The Personals (1998 Taiwanese film)
 
{{Infobox Film
| name           = The Personals
| image          = 
| image_size     = 
| caption        = 
| director       = Chen Kuo-Fu
| producer       = Li-Kong Hsu
| writer         = Chen Kuo-Fu Chen Shih-Chieh
| narrator       = 
| starring       = Rene Liu
| music          = 
| cinematography = Nan-hong Ho
| editing        = Dar-lung Chang
| distributor    = 
| released       = 13 March 1998
| runtime        = 104 minutes
| country        = Taiwan Taiwanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Personals ( ) is a 1998 Taiwanese drama film directed by Chen Kuo-Fu. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

==Cast==
* Rene Liu - Dr. Du Jia-zhen
* Wu Bai
* Chao-jung Chen
* Bao-ming Gu
* Chin Shih-chieh
* Yi-nan Shih
* Chao-ming Wang
* Wen-hsi Chen
* Doze Niu (as Niu Cheng-tse)

==References==
 

==External links==
* 

 
 
 
 
 
 


 