Africa (film)
{{Infobox Hollywood cartoon|
| cartoon_name = Africa
| series = Oswald the Lucky Rabbit
| image = Oswald In Africa.jpg
| caption = Oswald sings his theme song.
| director = Walter Lantz Bill Nolan
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline
| voice actor =Pinto Colvig
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release date =  
| color process = Black and white
| runtime = 5:47
| language = English
| preceded by =Mexico (cartoon)|Mexico
| followed by =Alaska
}}
Africa is a 1930 Walter Lantz cartoon short featuring Oswald the Lucky Rabbit.

==Plot==
Oswald was riding through the Egyptian desert on his camel. The camel, though looking real on the exterior, is actually mechanical because of the two ball-shaped pistons inside which Oswald manipulates with his feet like bike pedals. One day, a lion was running toward them. To defend himself, Oswald brought out a rifle but it malfunctioned. As a final resort, Oswald fired the ball pistons from the camel like a cannon and aimed into the lions mouth. Terrified by its lumpy back, the lion runs away in panic.

Nearby where he is, Oswald saw an oasis and a palace. Upon seeing the apes dance and play instruments, the curious rabbit decides to join the fun. As he entered the palace, Oswald was greeted by the queen. The queen asked him who he is, and Oswald introduced himself in a song as well as giving advice for a possibly better lifestyle. Pleased by his visit, the queen asked Oswald if he would like to be her king. Oswald was at first uncertain, knowing he never met a queen, but immediately accepted. It turns out momentarily that the queen still has a king who shows up then throws Oswald out of the palace and into a pond full of crocodiles. Luckily, Oswald escapes unscathed and runs off into the desert.

==Notes==
Oswalds theme song is featured for the first time in this film. {{cite web
|url=http://lantz.goldenagecartoons.com/1930.html
|title=The Walter Lantz Cartune Encyclopedia: 1930
|accessdate=2011-04-24
|publisher=The Walter Lantz Cartune Encyclopedia
| archiveurl= http://web.archive.org/web/20110514224259/http://lantz.goldenagecartoons.com/1930.html| archivedate= 14 May 2011  | deadurl= no}}  The song was briefly shown again at the beginning of Alaska, the following cartoon.

==See also==
* Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 


 