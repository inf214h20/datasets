Manon (film)
{{Infobox film
| name           = Manon
| image          = Manon.jpg
| image_size     = 
| caption        = 
| director       = Henri-Georges Clouzot
| producer       = 
| writer         = Henri-Georges Clouzot Jean Ferry
| narrator       = 
| starring       = Cécile Aubry Michel Auclair
| music          = 
| cinematography = 
| editing        = 
| studio         = Alcina
| distributor    = Les Films de Jeudi DisCina (US)
| released       =   (France) 25 September 1950 (US)
| runtime        = 100 mins
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Manon ( ) is a 1949 French film directed by Henri-Georges Clouzot. It is a loose adaptation of the 1731 novel Manon Lescaut by Antoine François Prévost|Abbé Prévost.

The film won the Golden Lion at the Venice Film Festival.

==Cast==
*Serge Reggiani	... 	Leon Lescaut
*Michel Auclair	... 	Robert Dégrieux
*Cécile Aubry	... 	Manon Lescaut Andrex	... 	Le trafiquant
*Raymond Souplex	... 	M. Paul
*André Valmy	... 	Lieutenant Besnard / Bandit Chief
*Henri Vilbert	... 	Le commandant du navire / Ships Captain
*Héléna Manson	... 	La commère (une paysanne normande)
*Dora Doll	... 	Juliette
*Simone Valère	... 	Isé, la soubrette
*Gabrielle Fontan	... 	La vendeuse à la toilette
*Gabrielle Dorziat	... 	Mme Agnès
*Wanda Ottoni		
*Rosy Varte	... 	Petit rôle

==Plot==
Clouzot updates the setting to World War II, making the story about a French Resistance fighter who rescues a woman from villagers convinced she is a Nazi collaborator.

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 

 
 