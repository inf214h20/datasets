Gülen Gözler
 
{{Infobox film
| name           = Gülen Gözler
| image          = Gülen_Gözler.jpg
| image size     = 
| caption        = 
| director       = Ertem Eğilmez
| producer       = 
| writer         = Sadık Şendil Ahmet Üstel
| narrator       = 
| starring       = Münir Özkul Adile Naşit Müjde Ar Itır Esen Şener Şen Ayşen Gruda Halit Akçatepe
| music          = Melih Kibar
| cinematography = Hüseyin Özşahin
| editing        = 
| distributor    = 
| released       = 1 March 1977
| runtime        = 
| country        = Turkey
| language       = Turkish
| budget         = 
| preceded by    = 
| followed by    = 
}} 1977 Cinema Turkish comedy film directed by Ertem Eğilmez. 

==Plot==
Yaşar and his wife Nezaket have kept on producing children in the hope of finally getting a male child. But they end up with only daughters instead (who they give masculine names). Now they are stuck with the task of arranging suitable rich husbands for them.

==Cast==
*Münir Özkul (Yaşar)
*Adile Naşit (Nezaket)
*Müjde Ar (İsmet)
*Itır Esen (Nedret)
*Şener Şen (Vecihi)
*Ayşen Gruda (Fikret)
*Halit Akçatepe (Dursun)
*Mahmut Hekimoğlu (Temel)
*Şevket Altuğ (Laz Şevket)
*Lale Ilgaz (Hasret)
*Nejat Gürçen (Yunus)
*Sevda Aktolga (Hikmet)
*Ahmet Sezerel (Orhan)
*Tuncay Akça (Tuncay)
*İhsan Yüce (Hasan)

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 