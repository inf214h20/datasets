Ricky Rapper (film)
{{ infobox film
| name           = Ricky Rapper
| image          = Ricky rapper film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Mari Rantasila
| producer       = Lasse Saarinen Risto Salomaa
| writer         = Sinikka Nopola Tiina Nopola Annu Valonen Martti Suosalo Ulla Tapaninen
| music          = Iiro Rantala
| editing        = Tuuli Kuittinen
| studio         = Kinotar
| distributor    = Nordisk Film
| released       =  
| runtime        = 79 minutes 
| country        = Finland
| language       = Finnish
| budget         = € 1,134,478 
| gross          = $ 2,270,632 
}}
Ricky Rapper ( ) is a 2008 Finnish musical film.    The film is based on two volumes of Sinikka Nopolas and Tiina Nopolas childrens fantasy book series Ricky Rapper. The book volumes were originally released as Risto Räppääjä ja pakastaja-Elvi of 2000 and Risto Räppääjä ja Nuudelipää of 2001. Ricky Rapper is a comedy for children. It tells about the friendship between 10-year-old boy and girl in the normal suburb neighbourhood. The film was shot in Kartanonkoski in the city of Vantaa in the summer 2007. 

Ricky Rapper was the most viewed Finnish film in Finland in 2008 with its 209,506 viewers and it was the sixth most viewed of all the Finnish films.  The film received more than 215,000 viewers, and the DVD version has been sold almost 100 000 copies. 

The following film Ricky Rapper and the Bicycle Thief was released in 2010, and it received 38,425 viewers in its first weekend – almost double as the previous film. 

==Synopsis==
Ricky lives with his Aunt Serena and loves playing drums. However she breaks her leg and cousin Fanny moves in to take care of the household. "Fanny the Freezer" is strict and orderly – and this means no drums for Ricky. With the help of his downstairs neighbor Nelly Butterfly, Ricky seeks to restore order to the apartment and get his drums back.

==Cast==
*Niilo Sipilä as Ricky Rapper: A ten-year-old boy who likes to play drums
*Mimmi Lounela as Nelly Butterfly/Noodlehead: A girl who moves into Rickys building.
*Annu Valonen as Serena Rapper: Rickys aunt who dreams of Mr. Lindberg who lives downstairs.
*Martti Suosalo as Lennart Lindberg: A shy man who also dreams of Ms. Serena Rapper who lives upstairs.
*Ulla Tapaninen as Fanny the Freezer: Aunt Serenas cousin who moves in to take care of the household.

==References==
 

== External links ==
* 
* 

 
 
 
 