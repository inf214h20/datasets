Pantyhose Hero
 
 
 

{{Infobox film
| name           = Pantyhose Hero
| image          = PantyhoseHero.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = US film poster
| film name      = {{Film name
| traditional    = 脂粉雙雄
| simplified     = 脂粉双雄
| pinyin         = Zhī Fěn Shuāng Xióng  
| jyutping       = Zi1 Fan2 Seong1 Hung4 }}
| director       = Sammo Hung
| producer       = Sammo Hung
| writer         = 
| screenplay     = Barry Wong Szeto Cheuk Hon
| story          = 
| based on       = 
| narrator       = 
| starring       = Sammo Hung Alan Tam
| music          = Lowell Lo
| cinematography = Jimmy Leung
| editing        = Keung Chuen Tak Peter Cheung Bojon Films
| distributor    = Newport Entertainment
| released       =  
| runtime        = 99 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$15,672,845
}}
 1990 Cinema Hong Kong action Crime crime comedy film produced by and directed by Sammo Hung. Film stars Hung and Alan Tam.

==Plot==
While investigating on a serial murder case of homosexual couples, Officer Jeff Lau (Sammo Hung) and his partner Alan (Alan Tam) are sent undercover to pose as homosexual lovers to track the murderer.

==Cast==
*Sammo Hung as Jeff Lau
*Alan Tam as Alan / Gaykey
*Joan Tong as Chen Chen
*Jaclyn Chu as Inspector Chu Wai Man
*Yam Wai Hung as Boss
*Chung Fat as robber with chainsaw
*Billy Ching as Bosss assistant with orange jacket
*Ridley Tsui as Bartender
*Poon Chan Wai as Dick Cho
*Wu Ma as Commissioner Wu
*Corey Yuen Paul Chun as Captain Chan
*Andrew Lam as sushi chef
*Philip Chan as Officer raiding gay bar
*Tai Po as Man killed at construction site
*Lo Kin
*Raymond Lee
*Teddy Yip as Building security
*Siu Tak Foo
*Ricky Lau as taxi driver James Tien as Tsen
*Ban Yun Sang as rascal teasing Jeff on street
*Chu Tau as gangster in opening scene
*Mak Wai Cheung as gangster in opening scene
*Chow Kam Kong as gangster in opening scene
*Kong Miu Teng as gangster in opening scene
*Lam Hak Ming as gangster in opening scene
*Wong Chi Ming as thug in final fight scene
*Ha Kwok Wing
*Wong Chi Keung as rascal teasing Jeff on street
*Choi Kwok Keung
*Fei Kin as thug
*Huang Kai Sen as thug
*Hon Ping as thug

==Box office==
The film grossed HK$15,672,845 at the Hong Kong box office during its theatrical run form 8 August to 13 September in Hong Kong.

==See also==
*Sammo Hung filmography

==External links==
* 
* 
*  at Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 