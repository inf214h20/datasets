Ongole Githa
 
 
{{Infobox film
| name           = Ongole Gittha
| image          = Ongole Githa Poster.jpg
| caption        = Movie Poster Bhaskar
| producer       =B. V. S. N. Prasad
| writer         = Bhaskar Ajay Prakash Raj Prabhu Ahuti Prasad
| music          = G. V. Prakash Kumar Mani Sharma
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| editing        =
| studio         = Sri Venkateswara Cine Chitra
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =  
| website        =
}}
 Telugu action action masala masala comedy Bhaskar and produced by B. V. S. N. Prasad under Sri Venkateswara Cine Chitra.  The film features Ram Pothineni and Kriti Kharbanda in the lead roles. The soundtrack was be composed by G. V. Prakash Kumar, Mani Sharma also scored one song for the film and given the background score. The film was released on 1 February 2013 to negative reviews from critics and commercial failure at the box office. The hindi dub of the film was renamed as Mahaveer no.1.

==Plot== MLA (Ahuti Prasad) wants to buy the market so he can shift it to a land on the outskirts of the city. During this it is also revealed that Adikeshavulu is not a good man but is pretending to be a good one. Whenever he returns home after difficultly pretending to be a good person he stands in front of the mirror and removes all of his clothes. Out of anger for Adikeshavulu not giving him the market, The MLA makes White the chairman of the market. Adikeshavulu beats the MLA to a pulp and makes him announce Adikeshavulu the chairman. When the White agrees on the condition that Adikeshavulus daughter must marry him. Out of greed for the market, Adikeshavulu agrees and gets White and Sandy engaged. On the night after their engagement, White goes to Sandys house to meet her. But she escapes from him and reaches outside the house with her grandmother and mother and complains on White. Just then, Adikeshavulu comes in drunk but controlled but White understands and goes away but his father-like business partner in a drunk mode reveals that White has a father who lives in Tripura. Adikeshavulu uses this information and sends his man Durga (Abhimanyu Singh) to find Whites father. But at Tripura, Durga bumps into White while trying to kill an old enemy. It is now revealed that the old enemy (Prabhu) is Whites father and that Whites real name is Dorababu. Whites partner finds out and blames White of being the son of the person who murdered his family. It is now that White reveals his past. Years back, Narayana used to be the chairman of the Ongole market. Adikeshavulu was the brother of another man (Jaya Prakash Reddy), who envied Narayana and wanted his post. The man commits suicide as he could not get the post and taking pity, Narayana appoints Adikeshavulu his man and the market continues to prosper with Adikeshavulu pretending to be good. One day, Adikeshavulu starts collecting money from the sellers as the funds for building shelter and giving food for the sellers were insufficient. Narayana comes there, stops Adikeshavulu and tells him that he will give the money. Another man in the market who envies Narayana blames him of acting in front of the people and accuses him of telling Adikeshavulu to collect the money. But nobody believes him and continue to trust Adikeshavulu. Adikeshavulu kills the man along with Durga and it is revealed that it is he who killed his brother. White (Dorababu), decides now to take revenge. Meanwhile, Adikeshavulu kidnaps Narayana and decides to put him in a sack in the market and burn the market to ashes as he will get the insurance as profit. The market is burnt that night but White comes to save it and ends up chasing Adikeshavulu into the announcement office. There, Adikeshavulu reveals everything about himself, including the fact that he was the one who killed his brother, which White accidentally catches on the announcement mike. Everybody outside hears everything and charges at Adikeshavulu who ends up being killed by their beatings. The people welcome Narayana back in the market and finally he gets his post as market president back.

==Cast==
* Ram Pothineni as White/Dorababu
* Kriti Kharbanda as Sandhya/Sandy
* Prakash Raj as Adikeshavulu Prabhu as Narayana Ajay as Adikeshavulus brother
* Abhimanyu Singh as Durga
* Ahuti Prasad as MLA
* Raghu Babu
* Kishore Das as Pavuram
* Rama Prabha as Grandmother of Sandhya

==Production==

===Casting===                               Bhaskar approached Tamil actor Karthi, who was not able to give his dates.  Later, Bhaskar approached Allu Arjun and finally Ram Pothineni took up the offer.  A. Venkatesh (cinematographer)|A. Venkatesh was chosen as the films cinematographer,   while G. V. Prakash Kumar agreed to compose the films background score and soundtrack.  Initially the model turned actress Shubha Phutela was selected to share screen space with Ram.  She has encountered health problems after shooting for the first schedule in the Guntur mirchi yard. Later Kriti Kharbanda was selected for her role.  Prakash Raj, Abhimanyu Singh, Brahmanandam, Ahuti Prasad, Ajay, Raghu Babu, Rama Prabha, Kishore Das are playing other roles in this film. 

===Filming===
The team has successfully completed the first schedule in Guntur. The film unit has shot for more than 45 days in this schedule and has filmed important scenes and couple of action episodes at Guntur market yard. The second schedule of the film will be taking place in Tanuku from July first week.  The film has been disrupted due to the heavy rains by Nilam cyclone effect. 

==Release==
Ongole Githa was awarded an A Certificate by the Censor Board due to nude scenes of Prakash Raj. 

===Critical Reception===
The film received mixed reviews from critics. Apglitz gave a rating of -2.25 stating Outdated script with poor direction and screen play  Mahesh S Koneru of 123Telugu has  given -2.5 out of 5 saying Ram’s energy levels, Prakash Raj’s performance and Kriti Kharbandha’s beauty are the sole redeeming factors.  Supergoodmovies wrote The first half an hour gives an interesting feel of the movie and but it impresses you with a few romantic songs, fights and comedy.  Second half of the film goes into flashback mode. But the story gets struck in the second half with exhausted narration. 

===Impact===
Only because of Prakash Raj’s scenes, Ongole Githa was awarded ‘A’ certificate by the censor board. Thus after getting negative feedback about the scenes on day 1, the producer and creative think-tank took a collective decision of removing these scenes from the film.  

==Box office==
Ongole Githa was reported to collect around   in 17 days. Producers were later arrested by the Moral Police of Andhra Pradesh for fake collections. 
 

==Soundtrack==
{{Infobox album
| Name = Ongole Githa
| Longtype = to Ongole Githa
| Type = Soundtrack
| Artist = G. V. Prakash Kumar
| Cover =
| Released = 16 January 2013
| Recorded = 2012-2013 Feature film soundtrack
| Length = 21:51 Telugu
| Label = Aditya Music
| Producer = G. V. Prakash Kumar Mani Sharma
| Reviews =
| Last album = Naan Rajavaga Pogiren (2012)
| This album = Ongole Githa (2013)
| Next album = Annakodi (2013)
}} Bhaskar for the first time in his career and had also composed the music for Rams earlier film Endukante... Premanta!. Mani Sharma has also scored one song for the film and given the background score. The audio launch was held on 16 January 2013 at Annapurna Studios, Hyderabad. S. S. Rajamouli attended the audio launch function as chief guest and released the audio CD and several others like Ali, Kona Venkat, Vanamali, Bhaskarabhatla, Sravanthi Ravi Kishore graced the event. 
{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 21:51
| lyrics_credits  = yes
| music_credits   = yes

| title1          = Yerra Mirapallo
| music1          = G. V. Prakash Kumar
| extra1          = Shankar Mahadevan
| lyrics1         = Vanamali
| length1         = 3:45
| title2          = Raa Chilakaa
| music2          = G. V. Prakash Kumar
| extra2          = G. V. Prakash Kumar
| lyrics2         = Vanamali
| length2         = 4:30
| title3          = Chal Challe
| music3          = Manisharma
| extra3          = Narendra, Sahithi
| lyrics3         = Bhaskarabhatla
| length3         = 4:24
| title4          = Ye Pilla
| music4          = G. V. Prakash Kumar Ranjith
| lyrics4         = Vanamali
| length4         = 4:33
| title5          = Mama Maraju
| music5          = G. V. Prakash Kumar
| extra5          = Jai Srinivas, John, Gopal, Mouli, Ravi
| lyrics5         = Vanamali
| length5         = 4:39
}}

==References==
 

==External links==
* 

 

 
 
 