Why Did I Get Married Too?
 
{{Infobox film
| name = Why Did I Get Married Too?
| image = WhyDidIGotMarried2Poster2.jpg
| caption = Theatrical release poster
| director = Tyler Perry
| producer = Tyler Perry Reuben Cannon
| screenplay  = Tyler Perry
| starring = Janet Jackson Tyler Perry Tasha Smith Jill Scott Louis Gossett, Jr. Malik Yoba Michael Jai White Sharon Leal Richard T. Jones Lamman Rucker Cicely Tyson
| music = Aaron Zigman
| cinematography =
| editing = Maysie Hoy
| studio = Tyler Perry Studios Lionsgate
| released =  
| runtime = 121&nbsp;minutes
| country = United States
| language = English
| budget = $20 million 
| gross =  $60,095,852   
}}
Why Did I Get Married Too? is a 2010 American comedy-drama film produced by Lionsgate and Tyler Perry Studios and stars Janet Jackson, Tyler Perry, and Tasha Smith. It is the sequel to Why Did I Get Married? (2007),  The film shares the interactions of four couples who undertake a week-long retreat to improve their relationships.

==Plot==
 
The four couples prepare for their next marriage retreat, in the Bahamas. Sheila and her new husband, Troy, are the first to arrive, followed (in order) by Patricia and Gavin, Terry and Dianne, and Angela and Marcus. The men and the women separate to talk about the good and bad about their marriages. In a surprising twist, Sheilas ex-husband, Mike, arrives, and Angela immediately starts a fight until he leaves the women alone to go see the guys.

That night, he talks about his and Sheilas relationship, which angers Troy. Dianne accidentally calls Terry "Phil" in the course of conversation. Angela is insistent about getting the password to Marcus cell phone because she distrusts him, but Marcus distracts her using sex. Dianne and Terry hear arguing later and think its Angela and Marcus, but it turns out to be Patricia and Gavin. When Dianne goes to investigate, she finds Patricia but cannot get Patricia to tell her whats wrong. The next morning Sheila makes it clear that, though Mike says he misses her, she is completely over him. At the beach the women meet an elderly couple who have accidentally thrown a friends ashes on Angela. Sheila invites them to dinner and they accept. At the "Why Did I Get Married?" ceremony, Patricia announces to the group that she and Gavin are getting a divorce, causing an upset Gavin to walk away from her, because he did not know she was going to announce it to them.

Back in Atlanta, Gavin and his lawyer meet Patricia with her lawyer, Dianne. Patricia and Gavin have decided to split everything down the middle in the settlement, but Gavin reveals that Patricia has not offered up the account containing her $850,000 book revenue. Patricia refuses to give Gavin any of her book money, but as Patricia leaves, Gavin advises Dianne to tell Patricia to "prepare for a fight", as he intends to get half of that account as well. Angelas neighbor tells her shes been hearing sexual noises from the house when Angela is not home. Angela believes Marcus is cheating and confronts him live on his television show, who then gives her his cell phone and password. Gavin comes home very drunk and confronts Patricia. He takes their sons baby photos and taunts her about her perceived lack of emotions, even about their divorce and their sons death. He then assaults her, douses her in vodka, then burns the photos.

Elsewhere, at Sheilas request, Mike agrees to help find Troy a job. Angela lectures Dianne and Sheila about how all men cheat. Patricia changes the locks and catches Gavin, Terry, and Marcus moving Gavins things out, then learns Gavin has taken all their money, including her book money; enraged, Patricia trashes the house with Gavins golf clubs. Angela comes home early to catch Marcus cheating and finds a couple in her bed, but after shooting up the room, she notices it was just the gardener and the maid having sex. Terry finally confronts Dianne about her infidelity; she reveals that she has been having an emotional affair and begs for forgiveness. Marcus and Angela fight, then reconcile, but only to fight again after Angela discovers Marcus has another phone. Troy arrives at Mikes apartment after finding out Mike got him his police job. After finding Sheila there, he angrily attacks Mike. Sheila tearfully confesses that she has been taking Mike to chemotherapy; she tries to apologize for being dishonest, but he leaves her.

The women go to Patricias house to comfort her; they soon realize they are ruining their marriages and lives with their constant selfishness, lies, dishonesty and inconsideration. The next day, Troy, himself, apologizes to Mike for the incident, who forgives him and invites Troy to have a drink with him and the guys, beginning a new friendship. Mike tells the men to fix their marriages because life is too short.

The following day, Gavin finds himself humiliated at his job, harassed, and told off by an angry Patricia; to make matters worst, he is then struck and gravely injured by a truck. While the others wait to hear the status of his condition, a tearful and regretful Patricia instructs the wives to fix their marriages (such as Mike suggested to the husbands), and everyone makes up. Gavins doctor shows up and informs them that he has passed away. The couples decided to have a memorial service for Gavin in the Bahamas.  One year later. As Patricia exits a university building she approached by a colleague. She tells Patricia that she knows someone who wants to meet her, a philanthropist, but Patricia refuses. The professor goes on to tell her that she can at least say hi because the university needs funds. The man (Dwayne Johnson) tells her that her books have helped in his grieving process (divorce) and invites her to have coffee. The movie ends with Patricia smiling at him.

==Cast==
* Tyler Perry as Terry Bobb
* Janet Jackson as Patrica Agnew
* Jill Scott as Shelia Jackson
* Sharon Leal as Dianne Bobb
* Tasha Smith as Angela Williams
* Richard T. Jones as Mike
* Malik Yoba as Gavin Agnew
* Lamman Rucker as Troy Jackson
* Michael Jai White as Marcus Williams
* Louis Gossett, Jr. as Louis Jones
* Cicely Tyson as Ola Karrie
* Valarie Pettiford as Mom Dwayne "The Rock" Johnson as Daniel Franklin (uncredited)

==Production== sudden death, film production was halted for a short period of time, after which Jackson returned to continue with the project. On August 6, 2009, Jackson stated that she had finished filming her scenes. 

On June 16, 2009, Tyler Perry confirmed that the entire cast from the first film would return for the sequel.

==Reception==
The film received mixed reviews from critics.  Based on 43 reviews collected by Rotten Tomatoes, the film has an overall approval rating from critics of 28% with an average score of 4.5/10.  By comparison, Metacritic calculated an average score of 44%, based on 12 reviews.   

  graded the film a D, writing in her review: "Its a contradiction in terms to think of the phenomenally successful, prolific entertainment showman Tyler Perry as lazy, but theres no other description for this particular product: Terribly shot and crudely assembled."  Scott Wilson concurred, stating: "Why Did I Get Married Too? lacks the underlying goodwill of some of his better work and plays like a gift wrapped present to his detractors." Wilson graded the movie 1.5/5 stars.

==Box office== Clash of the Titans. Its opening weekend gross makes it the third highest opening for films created by Tyler Perry.  As of June 6, 2010 the movie has grossed over $60 million domestically. 

==Soundtrack==
Janet Jackson recorded a song for the Why Did I Get Married Too? soundtrack entitled "Nothing (Janet Jackson song)|Nothing". It served as the soundtracks lead single. Also, Cameron Rafatis single "Battles" was featured in this film.  Norwegian Christel Alsos was also featured with the song "Still". The soundtrack for the film was released on So So Def Recordings with distribution being handled by Malaco Records.

==Awards/Nominations==

NAACP Image Awards Outstanding Motion Picture Outstanding Actress in a Motion Picture - Janet Jackson Outstanding Supporting Actress in a Motion Picture - Jill Scott Outstanding Writing for a Motion Picture - Tyler Perry

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 