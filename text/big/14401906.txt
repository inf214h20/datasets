The Serpent (2006 film)
{{Infobox Film

 | name = The Serpent
 | image_size = 
 | caption = 
 | director = Éric Barbier
 | producer = Olivier Delbosc Eric Jehelmann Marc Missonnier Pierre Rambaldi Ted Lewis (novel) Trân-Minh Nam
 | narrator = 
 | starring = Yvan Attal
 | music = Renaud Barbier
 | cinematography = Jérôme Robert
 | editing = Véronique Lange
 | distributor = Wild Bunch Distribution
 | released = December 8, 2006 (Marrakech International Film Festival)
 | runtime = 
 | country = France French
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image = The Serpent VideoCover.jpeg
}}
 French thriller Ted Lewis.

==Plot==
A father on the verge of divorce sees his life fall apart after a former classmate brings murder, kidnapping and blackmail into his life. His only chance to escape is by entering the former classmates world.

==Cast==
* Yvan Attal as Vincent Mandel
* Clovis Cornillac as Joseph Plender
* Minna Haapkyla as Hélène
* Pierre Richard as Cendras
* Simon Abkarian as Sam
* Olga Kurylenko as Sofia
* Veronika Varga as Catherine
* Jean-Claude Bouillon as Max
* Pierre Marzin as Carbona
* Gerald Laroche as Becker
* Abdelhafid Metalsi as Police Officer at the cemetery
* Manon Tournier as Julietter

==See also==
* List of French films
* Cinema of France

==External links==
* 

 
 
 
 
 


 