The Road a Year Long
{{Infobox film
| name           = The Road a Year Long
| image          = The Road a Year Long.jpg
| caption        = 
| director       = Giuseppe De Santis
| producer       = 
| writer         = Giuseppe De Santis
| starring       = Silvana Pampanini
| music          =  	Vladimir Kraus-Rajteric
| cinematography = Marco Scarpelli
| editing        = 
| distributor    = 
| released       =  
| runtime        = 162 minutes
| country        = Italy Yugoslavia
| language       = Italian
| budget         = 
}}
The Road a Year Long ( ,  ) is a 1958 film directed by Giuseppe De Santis. A Yugoslavian-Italian international co-production|co-production, it was nominated for the Academy Award for Best Foreign Language Film.   

==Cast==
* Silvana Pampanini as Giuseppina Pancrazi
* Eleonora Rossi Drago as Susanna
* Massimo Girotti as Chiacchiera (Naklapalo)
* Bert Sotlar as Guglielmo Cosma (Emil Kozma)
* Ivica Pajer as Lorenco
* Milivoje Zivanovic as Davide
* Gordana Miletic as Angela
* Niksa Stefanini as David
* Hermina Pipinic as Agneza
* Lia Rho-Barbieri as Roza
* Antun Vrdoljak as Bernard

==See also==
* List of submissions to the 31st Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 
 
 