Who Goes There!
{{Infobox film
| name           = Who Goes There!
| image          = Whogoesthere.jpg
| image_size     = 
| caption        = 
| director       = Anthony Kimmins
| producer       = Anthony Kimmins
| writer         = John Dighton
| narrator       = 
| music          = Muir Mathieson
| cinematography =
| editing        =  George Cole   Peggy Cummins  A. E. Matthews
| distributor    = British Lion Film Corporation
| released       = 10 June 1952
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £123,542 (UK) 
| preceded_by    = 
| followed_by    = 
}}
 British comedy George Cole. farcical activities Grace and Favour house near St. James Palace in Central London.  It is based on a 1951 play by John Dighton, who also wrote the screenplay. 

It was retitled The Passionate Sentry for its United States release with American censors removing two uses of the word "Cripes!". 

A version of Who Goes There! was broadcast on the BBC Radio Saturday Night Theatre on 20 March 1954. 

==Cast==
* Nigel Patrick - Miles Cornwall
* Valerie Hobson - Alex Cornwall George Cole - Guardsman Arthur Crisp
* Peggy Cummins - Christina Deed
* Anthony Bushell - Major Guy Ashley
* A. E. Matthews - Sir Hubert Cornwall

==Quotes==
United they staggered, divided they fell.- Miles Cornwall commenting on Christinas alcoholic parents

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 


 
 