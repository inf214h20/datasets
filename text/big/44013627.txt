Athidhi (1975 film)
{{Infobox film
| name = Athidhi
| image =
| caption =
| director = KP Kumaran
| producer = Ramachandran
| writer = KP Kumaran
| screenplay = KP Kumaran
| starring = Sheela P. J. Antony Balan K Nair Kottarakkara Sreedharan Nair
| music = G. Devarajan
| cinematography = RM Kasthoori
| editing = Ravi
| studio = Rachana Films
| distributor = Rachana Films
| released =  
| country = India Malayalam
}}
  1975 Cinema Indian Malayalam Malayalam film, directed by KP Kumaran and produced by Ramachandran. The film stars Sheela, P. J. Antony, Balan K Nair and Kottarakkara Sreedharan Nair in lead roles. The film had musical score by G. Devarajan.   
 
==Cast==
*Sheela 
*P. J. Antony 
*Balan K Nair 
*Kottarakkara Sreedharan Nair 
*Santha Devi 
*Sandhya

==Soundtrack==
The music was composed by G. Devarajan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aham Brahmaasmi || Ayiroor Sadasivan, Manoharan, Soman, Thomas || Vayalar || 
|- 
| 2 || Seemanthini Ninte || K. J. Yesudas || Vayalar || 
|- 
| 3 || Thankathinkalthaazhika || P Madhuri || Vayalar || 
|}

==References==
 

==External links==

 
 
 


 