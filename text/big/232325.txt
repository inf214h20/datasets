Swati Kiranam
{{Infobox film
| name           = Swati Kiranam
| image          = Swathi Kiranam.jpg
| director       = K. Viswanath
| producer       = V. Madhusudhan Rao Jandhyala (dialogue) Vennelakanti (lyrics) Sirivennela Seetarama Sastry (lyrics) C. Narayana Reddy (lyrics) Madugula Nagaphani Sharma (lyrics)
| story          = K. Viswanath
| screenplay     = K. Viswanath
| Lyrics         = Vennelakanti Sirivennela Seetarama Sastry C. Narayana Reddy
| starring       = Mammootty Raadhika Sarathkumar Master Manjunath Jayanthi Sakshi Ranga Rao Dharmavarapu Subramanyam Ananth
| editor         = G. G. Krishna Rao
| cinematography = Kasthoori
| distributor    = Swathi Productions
| studio         = Meher Chaithanya Niketan Trust, Meher Nagar
| released       =  
| runtime        = 130 minutes
| language       = Telugu
| music          = K. V. Mahadevan
}}
 1992 Telugu Telugu drama musical drama prodigious young disciple. Filmfare Award for Best Music Direction, and the National Film Award for Best Female Playback Singer. 

==Plot==
The film starts with a shabby old man living as a recluse near a famous temple.  When he ventures out into the village, he is beaten by the shepherd who believe that he is the one who was snatching sheep and killing them some time ago.  When they hand him over to the police, the officer in charge identifies him as a once famous musician, Anantha Rama Sharma (Mammooty), hailed as Sangeeta Samrat (Emperor of music), who has been missing for four years.  The inspector RadhaKrishna (Achyuth) informs his aunt (who teaches music in his native village) of this and the past of Anantha Sharma is narrated through flashback.

; Flashback begins
Anantha Rama Sharma is a widely respected Carnatic singer with a large ego. This is established when he rejects the Padma Shri bestowed upon him by the Government of India, as he believes that the other awardees are not worthy to be mentioned with him.

Gangadhar (Master Manjunath) lives in the same village.  He rejects the societys norms (taking music classes and going to school) and spends his time sitting by the riverside.  The river inspires him to sing many songs in different tunes.  His music teacher, also the policemans aunt, recognizes his natural talent and tries to nurture it.  His school teacher (the music teachers brother) is also fond of Gangadhar.

Gangadhar wants to become a great singer, like Ananta Sharma, and his father (Dharmavarapu Subrahmanyam) encourages him.  His day comes when Anantha Sharma is honored in an auditorium. Gangadhar sings a song ("Aanathi Neeyara") in honor of Anantha Sharma which many consider to be much better than any of Anantha Sharmas four decades of work.  The boy is hailed as a child prodigy by everyone including Anantha Sharmas wife.

Anantha Sharma wants to imprison Gangadhars talents. So he asks Gangadhar to stay in his house and learn music as one must be well-experienced to sing on stage.  He goes to the extent of copying one of his tunes and he sings it on stage to get back his lost glory.  But Gangadhar and Anantas wife (Raadhika Sarathkumar) find this out.  He now feels guilty and defends himself by saying that he did it as he was afraid this child would destroy his name and fame.

In this emotional moment, he suffers a heart-attack.  To show his gratitude towards his adopted mother (Raadhika sarathkumar), Gangadhar kills himself.  For this means that there is no competition for Anantha Sharmas title of "Sangeeta Samrat".  But the event shocks the villagers and they start calling him a murderer.  To escape the public anger and insults, he runs away to be a recluse.
;Flashback ends

Ananta Sharma is still unconscious while in the police station.  When he awakes, he finds himself in the house of Gangadhars music teacher.  The husband of music teacher tells him that they have forgiven him and asks him to go back to his house.  At his house, he finds his wife (Raadhika sarathkumar) conducting music classes in his house (an institute named Gangadhar Music Academy) for little children.

He sits down amongst the children learning the music basics from his wife.  The movie ends with a scene when a young girl next to him chides him gently by correcting his way of singing a basic song note.

==Soundtrack==
{{Track listing
| headline     = Songs
| extra_column = Singer(s)
| all_lyrics   = Sirivennela Sitaramasastri, Vennelakanti, C. Narayana Reddy & Madugula Nagaphani Sharma
| all_music    = K. V. Mahadevan

| title1 = Aanathi Neeyara
| extra1 = Vani Jayaram

| title2 = Jaliga Jabilamma 
| extra2 = Vani Jayaram, K. S. Chithra

| title3 = Konda Konallo Loyallo
| extra3 = Vani Jayaram

| title4 = Om Guru
| note4  = Shloka
| extra4 = Vani Jayaram

| title5 = Pranati Pranati Pranati
| note5  = male
| extra5 = S.P.Balasubrahmanyam

| title6 = Pranati Pranati Pranati
| note6  = Child
| extra6 = Vani Jayaram

| title7 = Sangeetha Sahitya Samalankrute
| extra7 = S.P.Balasubrahmanyam

| title8 = Shivani Bhavani Sarvani
| note8  = male
| extra8 = S.P.Balasubrahmanyam

| title9 = Shivani Bhavani Sarvani
| note9  = child
| extra9 = Vani Jayaram

| title10 = Shruti Neevu
| extra10 = Vani Jayaram, K. S. Chithra

| title11 = Teli Manchu Karigindi
| extra11 = Vani Jayaram

| title12 = Vaishnavi Bhargavi
| extra12 = Vani Jayaram
}}

==Awards==
 
|-
| 1992
| Vani Jayaram (for song "Aanathi Neeyara") 
| National Film Award for Best Female Playback Singer
|  
|- 1992
| K. V. Mahadevan Filmfare Award for Best Music Director – Telugu
|  
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 