Yakuza Weapon
{{Infobox film
| name           = Yakuza Weapon
| image          = Yakuza-weapon-poster.jpg
| caption        = Film poster for Yakuza Weapon
| film name = {{Film name| kanji          = 極道兵器
| romaji         = Gokudō Heiki}}
| director       = Tak Sakaguchi Yūdai Yamaguchi
| producer       = Yoshinori Chiba Shuichi Takashino Toshiki Kimura
| writer         = Yūdai Yamaguchi Tak Sakaguchi
| based on       =  
| starring       = Tak Sakaguchi Jun Murakami Mei Kurokawa Shingo Tsurumi
| music          = Nobuhiko Morino 
| cinematography = Masakazu Oka
| editing        = Zensuke Hori
| studio         = Stairway
| distributor    = Nikkatsu
| released       =  
| runtime        = 104 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2011 Japanese film directed and written by Tak Sakaguchi and Yūdai Yamaguchi. Based on the manga by Ken Ishikawa, the film is about Shozo Iwaki (Tak Sakaguchi) who works as a mercenary in South America. Iwaki is informed of the death of his gang boss dad and discovers his former Yakuza henchman is involved in a double-cross. Yakuza Weapon was premiered at the 2011 Yubari International Fantastic Film Festival and opened theatrically in Japan on July 23, 2011.

== Plot ==
Shozo Iwaki (Tak Sakaguchi) is a strong fighter with a strong disregard for his own safety during fights by shrugging off all wounds and damage. He has a falling out with his father, the Boss Yakuza Kenzo Iwaki (Akaji Maro) and spends years in the South American jungle fighting mercenaries.  After a battle against opposing soldiers, Shozo is found by Red Tiger and his team, a Japanese special agent who informs him that his father was assassinated.   

Shozo returns home to find his old hideout is now a sleazy loan shark operation run by a former junior lieutenant in the Yakuza, and a rival Yakuza boss, Kurawaki (Shingo Tsurumi) who is double crossing him and trying to unite the other clans under his control. Shozo is then ambushed by his old Yakuza flame, Sister Nayoko (Mei Kurokawa) under the perception that he abandoned her when he left for South America. Upset with this turn of events, Shozo destroys various Kurawaki holdings until he finds that Sister Nayoko is kidnapped by Kurawaki himself. Shozo attacks the Kurawaki high-rise headquarters by blowing it up and rescues Nayoko. Kurawaki escapes in a military helicopter and blows off Shozos right arm and left leg. Shozo manages to retaliate by shooting down the helicopter with a grenade launcher. Kurawaki survives Shozos, but is severely scarred and dependent on a life-support robotic machine.   Kurawaki and his assistant plan to raise an army of mind controlled thugs, which includes Shozos former sparring partner and blood brother Tetsu (Jun Murakami), who has been driven mad due to the rape and death of his sister at the hands of a rival crime lord.   

Shozo is rebuilt by Red Tiger with a M61 Vulcan gatling gun as a prosthetic right arm, and a knee mounted rocket launcher. Nayoko again thinks Shozos Decision to side with Red Tiger is a foolish and leaves him only to be kidnapped again by Kurawaki. While on a field mission, Red Tiger threatens to control Shozos actions with a killswitch because he wants Kurawaki brought to him alive for questioning but Shozo blasts it out of his hand. Shozo goes on a raid against Kurawakis soldiers including mind controlled thugs and machine-gun armed nurses. He faces off against the mind-controlled Tetsu, who attacks him with a gatling gun and rocket launcher weapon made from his sisters body. Finding their weapons to be a stalemate, they resort to a fist fight where Shozo ultimately is victorious and kills Tetsu. Shozo tracks down Kurowaki who has installed a nuclear device in Kenzos body. Shozo claims that a true Yakuza isnt afraid of nuclear weapons and kills Kurowaki which detonates the nuclear weapon as well.

==Production==
Director Tak Sakaguchi had only 12 days to shoot the entire film. To make the film on this scale, he decided to bring in his friend and fellow director Yūdai Yamaguchi to also direct the film.    Yakuza Weapon is an adaptation of the manga Gokudō Heiki (1996) by Ken Ishikawa.     Yamaguchi felt that adapting a manga gave the story a wider appeal than his other film works such as Battlefield Baseball and Deadball.    The film is dedicated to Ishikawa who died in 2006. 

==Release==
Yakuza Weapon had its world premiere at the Yubari International Fantastic Film Festival|Yūbari International Fantastic Film Festival on February 27, 2011   The film was shown at the New York Asian Film Festival on July 9, 2011.  It received its Canadian premiere at the Fantasia Festival on July 16, 2011.  The film received its theatrical release in Japan on July 23, 2011.  Well Go USA has acquired the DVD, digital, Video-on-demand and television rights for Yakuza Weapon along with Mutant Girls Squad, Helldriver, Deadball and Karate-Robo Zaborgar. 

==Reception==
Fangoria gave Yakuza Weapon a positive review give the film a three out of four rating, while noting that "the tone does flag every so often, and the film could have used a bit of trimming to be made even leaner and meaner"  Eye For Film gave the film a three out of five rating, stating that the film is "difficult to maintain that Yakuza Weapon ever adds up to more than the sum of these parts"  The Montreal Gazette gave the film a negative review stating it was "mostly just annoying, even, dare I say, boring". The review went on to state that "almost all the dialogue is shouted, with faces scrunched in anger. Yeah, I know it’s supposed to be part of the joke, part of the general the over-the-topness, but it gets old fast."  Film Business Asia gave the film a six out of ten rating, finding that the film takes too long to decide whether its a "spoof yakuza movie or a real action/splatter feast" and that the film only gets on track when Shozo gets his bionic surgery. 

== Notes ==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 