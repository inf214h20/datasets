Chicago Cab
{{Infobox film
| name           = Chicago Cab
| image          = Chicago cab dvd cover.jpg
| image_size     = 
| alt            =
| caption        =  aka = Hellcab
| director       = Mary Cybulski John Tintori
| producer       = Suzanne De Walt Paul Dillon
| writer         = Will Kern
| starring       = Paul Dillon Gillian Anderson John Cusack Laurie Metcalf Julianne Moore
| music          = Page Hamilton
| cinematography = Hubert Taczanowski
| editing        = Mary Cybulski John Tintori
| studio         = Childs Will Productions GFT Entertainment New Crime Productions
| distributor    = Castle Hill Productions
| released       = October 1997 (Chicago International Film Festival) September 18, 1998 (theatrical)
| runtime        = 96 mins
| country        = United States
| language       = English
| gross          = $23,946
}}

Chicago Cab, also known as Hellcab, is a 1997 American film directed by Mary Cybulski and John Tintori. It is based on a play by Will Kern.

==Synopsis==

The film follows an unnamed taxi driver (played by Paul Dillon) over one day in Chicago. More than 30 passengers enter his taxi throughout the course of the film, providing snippets into their lives.    Among the actors giving cameo appearances are Gillian Anderson, John Cusack, Laurie Metcalf, Julianne Moore, John C. Reilly, Michael Shannon, Michael Ironside, and Reggie Hayes.

==Release and reception==

Chicago Cab had its premiere at the Chicago International Film Festival in October 1997, where it was nominated for a Golden Hugo Award. It was not released in movie theatres until September 18, 1998, when it played in two venues and earned $23,946. 

The film received criticism for having unrealistic taxi passengers, since all of the characters have an exciting story.  Roger Ebert, however, gave it three stars out of four, saying "Drama is always made of the emotional high points."  Emanuel Levy also gave a positive review: "A compassionate portrait of a lonely cabbie is at the center of the serio comedy ...   highlight perceptively the funny, scary and dreary moments in a typical working day of a city cab driver." 

Chicago Cab had a DVD release on April 7, 2009. 

==References==
 

==External links==
* 
* 

 
 
 
 
 


 