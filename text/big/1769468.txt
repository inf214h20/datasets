Le Dernier Combat
{{Infobox film
| name           = Le Dernier Combat
| image          = Le Dernier Combat.jpg
| caption        = Film poster
| director       = Luc Besson
| producer       = Luc Besson, Pierre Jolivet
| writer         = Luc Besson, Pierre Jolivet
| narrator       = 
| starring       =  
| music          = Éric Serra
| cinematography = Carlo Varini
| editing        = Sophie Schmit
| distributor    = Crystal Films
| released       =   
| runtime        = 93 minutes
| country        = France
| language       = None FF 3.5 million Hayward, p. 91 
| gross          = 
}}
 French film. It was the first feature-film to be directed by Luc Besson, and also features Jean Renos first prominent role. Music for the film was composed by Éric Serra. The film was the first of many collaborations between Besson, Reno and Serra. A dark vision of Apocalyptic and post-apocalyptic fiction|post-apocalyptic survival, the film was shot in black and white and contains only two words of dialogue.    It depicts a world where people have been rendered mute by some unknown incident.

==Plot==
The film opens to a shot of an abandoned office, where the main character (Pierre Jolivet), who is only identified as The Man in the end credits, is having intercourse with a sex doll. The Man is then seen attempting to salvage parts from abandoned vehicles, but returns to his dwelling empty handed, where he works on building a makeshift aircraft. The Man ventures outside the office building he lives in, which is surrounded by a desert wasteland. A group of men are shown surviving in the wasteland. They hold a man, The Dwarf (Maurice Lamy), captive, and force him to retrieve water for them. The Man, who has been observing the survivors, makes his way to their camp, stabs their leader, The Captain (Fritz Wepper) and retrieves a car battery. Survivors pursue The Man, though he is able to escape in his now completed aircraft.

The Brute (Jean Reno) is seen approaching a hospital with a box containing canned food. The Brute rings a bell, and The Doctor (Jean Bouise), instructs him to place the canned goods on the ground and back away from the door. The Doctor then takes the goods and closes the door before The Brute can get inside.
 raining fish. While searching for a way to cook the fish, The Man encounters The Brute. A fight ensues; the Brute gains the upper hand though The Man is able to escape.

The Man, now badly injured, wanders around until he finds The Doctor. The Doctor treats The Man and cooks him some fish. The Doctor inhales a form of gas that allows him to, with some difficulty, say a single word: Bonjour. The Man also takes the gas and is able to reply with the same word. Both are ecstatic about being able to speak. The Man and the Doctor bond over table tennis and painting, before The Man ventures outside into a sandstorm to retrieve a painting he found in the bar. The Brute, who has been living in the bar, returns and notices the painting is missing.

The Doctor prepares some food and blindfolds The Man. He leads The Man to a part of the hospital where a woman is kept. The Brute sets fire to the front door of the hospital, though The Doctor and The Man extinguish the flames. The Man and The Doctor go to bring food to the woman again, and The Man gives her a wrapped gift. They then catch The Brute attempting to saw through the iron bar gate, though are able to scare him off.

The Doctor and The Man prepare food for the woman, yet this time The Doctor permits the man to not be blindfolded, and encourages him to comb his hair. Meanwhile it is revealed that The Brute has breached the iron gate to the hospital. The Doctor is killed on the way to the woman when chunks of rock rain down from the sky. The Man, who does not know how to find the woman without The Doctor, attempts to locate her, though he is confronted by The Brute. A fight ensues, with The Man eventually killing The Brute. The Man then locates the womans room, though is devastated when he discovers that The Brute had already killed her.

The Man repairs his aircraft, and flies back to the original survivors he encountered. He kills their new leader and frees The Dwarf. The Dwarf shows the Man where The Captain keeps his concubine (Christiane Krüger). The Man greets her with a warm smile, which she returns.

==Cast==
* Pierre Jolivet as The Man
* Jean Bouise as The Doctor
* Fritz Wepper as Captain
* Jean Reno as The Brute
* Christiane Krüger as Captains Concubine
* Maurice Lamy as Dwarf

==Themes==
Writing in the book The Films of Luc Besson, Susan Hayward considered Le Dernier Combat and The Fifth Element to be Bessons two films which focus on the theme of environmental damage, as waste and pollution are visible throughout both films.  Capitalism was also considered to be a theme; consumer commodities were said to be signs of death, such as the abandoned cars in the desert and the floating washing machines in a flooded abandoned factory. 

==Production==
Besson described the film as an "imaginary excursion", stating he got the inspiration for the film from an abandoned cinema he saw in Paris. Thinking of all the other abandoned buildings there must be in Paris, Besson decided to create a world where all these places were together, and combined this idea with a previous suggestion that he make a feature-length version of his 1981 debut short film, LAvant Dernier. Le Dernier Combat was primarily filmed in Paris, with scenes depicted as begin in the desert filmed in Tunisia.  Locations for filming in Paris included a former Électricité de France building that was demolished the day after filming was completed, and the derelict area where the Bibliothèque nationale de France was later constructed. 

==Reception==
The film attracted 236,189 viewers at the French box office.    is born, which is by no means a bad thing. 

==Accolades==
Le Dernier Combat won Best Film,  Best Director and the Audience Jury Award at Fantasporto in 1984.    It also won Best Film and Best Director at the 1983 Sitges Film Festival,  and won the Critics Prize at the 1983 Brussels International Fantastic Film Festival.  It was nominated for Best First Work at the 1984 César Awards. 

==Home media==
Adam Tyner from DVD Talk gave a positive review of the DVD release in August 2001, awarding it 4 out of 5 stars for video, audio and content, though awarding it 0 stars for extras, noting the only extras were trailers for other films.  The same DVD was released alongside Bessons 1997 film, The Fifth Element as a Luc Besson 2-pack. Upon reviewing the 2-pack in April 2003, Ron Epstein from DVD Talk gave the Le Dernier Combat DVD 3½ out of 5 for video, audio and content, and ½ a star for extras. 

==References==
 
Bibliography
 
* 
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 