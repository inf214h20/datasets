Cat Ballou
{{Infobox film
| name           = Cat Ballou
| image          = Cat Ballou Poster.jpeg
| image_size     = 225px
| caption        = theatrical release poster
| producer       = Harold Hecht
| director       = Elliot Silverstein
| based on       = The Ballad of Cat Ballou (novel) by Roy Chanslor Walter Newman Frank Pierson
| starring       = Jane Fonda Lee Marvin Michael Callan Dwayne Hickman Nat King Cole Stubby Kaye
| cinematography = Jack A. Marta
| editing        = Charles Nelson
| music          = Frank De Vol (score) Mack David (songs) Jerry Livingston (songs)
| distributor    = Columbia Pictures
| released       = June 24, 1965 (US)
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget          =
| gross          = $20,666,667  Cole, Georgelle.   on TCM.com 
}}
 comedy Western Western film, Oscar for his dual role, Michael Callan, Dwayne Hickman, and singers Nat King Cole and Stubby Kaye, who together perform the movies theme song.
 Walter Newman and Frank Pierson from the novel The Ballad of Cat Ballou by Roy Chanslor. Chanslors novel was a serious Western, and though it was turned into a comedy for the movie, the filmmakers retained some darker elements. The film references many classic Western films, notably Shane (film)|Shane.

==Plot==
Catherine Ballou (Jane Fonda), who wants to be a schoolteacher, is returning home by train to Wolf City, Wyoming, to the ranch of her father, Frankie Ballou (John Marley). On the way, she unwittingly helps accused cattle rustler Clay Boone (Michael Callan) elude his captor, the sheriff (Bruce Cabot), when Boones Uncle Jed (Dwayne Hickman), a drunkard disguised as a preacher, distracts the lawman. 

At the ranch, she learns that the Wolf City Development Corporation is scheming to take the ranch from her father, whose sole defender is his ranchhand, an educated Native American, Jackson Two-Bears (Tom Nardini). Clay and Jed appear and reluctantly offer to help Catherine. She hires legendary gunfighter Kid Shelleen (Lee Marvin) to help protect her father from gunslinger Tim Strawn (also Lee Marvin), the hired killer who is threatening Frankie.

Shelleen arrives, a drunken bum whose pants fall down when he draws his gun, and who is unable to hit a barn when he shoots. Strawn kills Frankie, and when the townspeople refuse to bring Strawn to justice, Catherine becomes a revenge-seeking outlaw known as Cat Ballou. She and her gang rob a train carrying the Wolf City payroll. They take refuge in "Hole-in-the-Wall", where desperados go to hide from the law, but are thrown out when it is learned what they have done, since Hole-in-the-Wall can only continue to exist on the sufferance of Wolf City. Shelleen, inspired by his love for Cat, works himself into shape, dresses up in his finest gun-fighting outfit, and goes into town to kill Strawn, casually revealing later that Strawn is his brother.
 Reginald Denny), the head of the Wolf City Development Corporation. A struggle ensues, Sir Harry is killed, and Cat is sentenced to be hanged on the gallows. With Sir Harry dead, theres no hope for Wolf Citys future, and the townspeople have no mercy for Cat. As the noose is placed around her neck, Uncle Jed appears, again dressed as a preacher, and cuts the rope just as the trapdoor is opened. Cat falls through and onto a wagon, her gang then spiriting her away in a daring rescue.

==Cast==
  
*Jane Fonda as Catherine "Cat" Ballou
*Lee Marvin as Kid Shelleen and Tim Strawn
*Michael Callan as Clay Boone
*Dwayne Hickman as Jed
*Nat King Cole as The Sunrise Kid
*Stubby Kaye as Professor Sam the Shade
 
*Tom Nardini as Jackson Two-Bears
*John Marley as Frankie Ballou Reginald Denny as Sir Harry Percival
*Jay C. Flippen as Sheriff Cardigan
*Arthur Hunnicutt as Butch Cassidy
 

Cast notes
*Cole and Kaye are billed simply as "Shouters". They act as a Greek chorus, intermittently narrating the story through verses of "The Ballad of Cat Ballou", which, along with other songs in the film, was written by Mack David and Jerry Livingston. 

==Production==
*At the beginning of the opening credits, the Columbia Pictures "Torch Lady" does a quick-change into a cartoon Cat Ballou, who draws and fires her pistols into the air.
*The film was director Elliot Silversteins second feature film, and his relationship with producer Harold Hecht while filming was not smooth. 
*Ann-Margret was the first choice for the title role but her manager turned it down without letting the actress know. Ann-Margret wrote in her autobiography that she would have wanted the part.  The Man from Snowy River. Jack Palance desperately wanted the role but it was never offered to him. 
*Nat King Cole was ill with lung cancer during the filming of Cat Ballou. A chain smoker, Cole died four months before the film was released.
*The film was shot in 28 days. Robert Osborne|Osborne, Robert. Outro to Turner Classic Movies presentation of Cat Ballou (May 14, 2011)   John Chambers created the prosthetic nose worn by Lee Marvin as Strawn in the film.   
*A former Great Western Railway of Colorado 2-8-0 Consolidation steam locomotive, number 51, owned by Boulder Scientific Company of Boulder, Colorado, was used in the film, with scenes shot at Canon City, Colorado, in September 1964. 

==Reception==
Although the film received mixed reviews, it was popular with moviegoers and earned $20 million in ticket sales in 1965, making it one of the top ten moneymaking movies that year. 

==Awards and honors==
Lee Marvin awards won
*1965 Academy Award for Best Actor in a Leading Role
*1965 British Academy Award Winner for Best Actor
*1965 Golden Globe Award Winner for Best Actor
*1965 Silver Bear for Best Actor at the 15th Berlin International Film Festival   

In his Oscar acceptance speech, Lee Marvin concluded by saying, "I think, though, that half of this belongs to a horse somewhere out in San Fernando Valley," a reference to the horse Kid Shelleen rode, which appeared to be as drunk as Shelleen was. 

Academy Award nominations Best Film Editing Best Music, Scoring of Music, Adaptation or Treatment Best Music, Song - Jerry Livingston and Mack David for "The Ballad of Cat Ballou" Best Writing, Screenplay Based on Material from Another Medium.

American Film Institute recognition
* AFIs 100 Years... 100 Movies - Nominated
* AFIs 100 Years... 100 Laughs - #50
* AFIs 100 Years... 100 Heroes and Villains:
** Tim Strawn - Nominated Villain
* AFIs 100 Years... 100 Songs:
** The Ballad of Cat Ballou - Nominated
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) - Nominated
* AFIs 10 Top 10 - #10 Western

In June 2008, AFI revealed its "Ten top Ten"—the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. Cat Ballou was acknowledged as the tenth best film in the Western genre.  

==In popular culture== Bobby and AFI 100 Years, 100 Laughs television special. The Balladeers from their film, Theres Something About Mary, are inspired by similar characters in Cat Ballou. A Clockwork Orange.
*"Cat Ballou" is a card in the spaghetti western board game Bang!.
*In a 2014 interview on NPR, actor Bryan Cranston called Cat Ballou the "movie that had the most impact" on him when he was growing up. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 