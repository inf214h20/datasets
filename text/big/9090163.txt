The Seduction of Joe Tynan
{{Infobox film
| name           = The Seduction of Joe Tynan 
| image          = Seduction of Joe Tynan .jpeg
| caption        = Theatrical release poster
| director       = Jerry Schatzberg
| producer       = Martin Bregman
| writer         = Alan Alda  Barbara Harris Meryl Streep  Rip Torn  
Melvyn Douglas
| music          = Bill Conti
| cinematography = Adam Holender
| editing        = Evan A. Lottman   
| distributor    = Universal Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $19,595,168 
}}
The Seduction of Joe Tynan is a 1979 American political film drama directed by Jerry Schatzberg and produced by Martin Bregman.  The screenplay was written by Alan Alda, who also played the title role. 
 Barbara Harris, and Meryl Streep, with Rip Torn, Melvyn Douglas, Charles Kimbrough, and Carrie Nye.  Meryl Streep said that she was on "automatic pilot" during filming because she went to work not long after the death of John Cazale, adding that she got through the process largely due to how supportive Alda was.

==Synopsis==
Respected liberal Senator Joe Tynan is asked to lead the opposition to a Supreme Court appointment and he finds himself struggling with his own morality and the corruption surrounding him. He falls for a lovely lady attorney and has an affair that jeopardizes his marriage, and possibly, his career.

==Cast==
* Alan Alda as Joe Tynan Barbara Harris as Ellie Tynan
* Meryl Streep as Karen Traynor
* Rip Torn as Senator Kittner
* Melvyn Douglas as Senator Birney
* Charles Kimbrough as Francis
* Carrie Nye as Aldena Kittner Michael Higgins as Senator Pardew
* Blanche Baker as Janet
* Chris Arnold as Jerry
* Maureen Anderman as Joes Secretary
* John Badila as Reporter on TV Screen
* Robert Christian as Arthur Briggs
* Maurice Copeland as Edward Anderson
* Lu Elrod as Congresswoman at Party

==Awards==
Wins
* Los Angeles Film Critics Association Awards: LAFCA Award, Best Supporting Actor, Melvyn Douglas; Best Supporting Actress, Meryl Streep; 1979.
* National Board of Review of Motion Pictures: NBR Award, Best Supporting Actress, Meryl Streep; 1979.
*  , Meryl Streep; 1979.
* New York Film Critics Circle Awards: NYFCC Award, Best Supporting Actress, Meryl Streep; 1979.
* American Movie Awards: Marquee, Best Actor, Alan Alda; 1980.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 