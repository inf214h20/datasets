Room for One More (film)
{{Infobox film
| name           = Room for One More
| image          = Room_for_one_more_poster.jpg
| image_size     =
| caption        = Film poster
| director       = Norman Taurog
| producer       = Henry Blanke
| writer         = Anna Perrot Rose Jack Rose Melville Shavelson	
| narrator       =
| starring       = Cary Grant Betsy Drake
| music          = Max Steiner
| cinematography = Robert Burks
| editing        = Alan Crosland, Jr.
| distributor    = Warner Brothers
| released       =  
| runtime        = 98 minutes
| country        = United States English
| budget         = $1,079,000 
| gross          = $2.75 million (US) 
}}
 Room for One More, in 1962.

==Plot==
Grant, as "Poppy" Rose, is the harried husband of Anna, played by his real-life wife at the time, Betsy Drake. Anna has a place in her heart for foster children, and is able to draw out these neglected children and make them real members of the family, despite their various personality differences and physical challenges. Poppy, who on the surface seems reluctant to open his home to "one more", is, underneath, putty in the childrens hands, from start to finish.

==Cast==
 
*Cary Grant as George "Poppy" Rose
*Betsy Drake as Anna Rose
*Lurene Tuttle as Miss Kenyon
*Randy Stuart as Mrs. Foreman
*John Ridgely as Harry Foreman
*Irving Bacon as The Mayor
*Mary Treen as Mrs. Roberts
*Hayden Rorke as The Doctor
*Iris Mann as Jane
*George Winslow as Teenie
*Clifford Tatum Jr. as Jimmy-John
*Gay Gordon as Trot
*Malcolm Cassell as Tim
*Larry Olsen as Ben Roberts
 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 