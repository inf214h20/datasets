Zatoichi and the Doomed Man
{{Infobox film
| name = Zatoichi and the Doomed Man
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市逆手斬り
| romaji         = Zatōichi sakate-giri
}}
| director = Kazuo Mori
| producer = Sadao Zaizen
| writer = Shozaburo Asai
| based on       =  
| starring = Shintaro Katsu Sachiko Murase
| music = Seitaro Omori Hiroshi Imai
| editing        = Toshio Taniguchi
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 78 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichi and the Doomed Man is the eleventh episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
 
Zatoichi (Katsu) is given 50 lashes for illegal gambling in Shimokura. While in jail, his cellmate Shimazo (Mizuhara) claims to have been jailed on false charges of Burglary|housebreaking, arson, and murder, pleading with Ichi to contact one of his influential associates who can vouch for his innocence and to inform his wife and daughter of his situation.

==Cast==
* Shintaro Katsu as Zatoichi
* Fujiyama Hyakutaro as Kanbi
* Kenjiro Ishiyama as Boss Jubei Araiso
* Masako Akeboshi as Ochiyo
* Eiko Taki as Oyone
* Ryuzo Shimada as Yakuza boss
* Koichi Mizuhara as Shimazo
* Sachiko Murase as Shimazos wife 

==Reception==

===Critical response===
Zatoichi and the Doomed Man currently has three positive reviews, and no negative reviews at Rotten Tomatoes. 

Brian McKay, writing for eFilmCritic.com, gave Zatoichi and the Doomed Man three out of five stars and said that " ith the exception of one very funny Zatoichi impersonator, and one or two excellent action sequences, Zatoichi and the Doomed Man is a surprisingly lackluster installment, due to underdeveloped characters and a truncated ending that feels as if someone edited out the films third act using a dull katana.   While Ill still take a mediocre ZATOICHI movie from thirty years ago over a crappy Hollywood film of the present day, this one leaves the viewer with a sense of unfinished business on the narrative side, and rushed work on the production side. Worth seeing for the bright points mentioned above, but overall a forgettable entry to the series."   

==References==
 

==External links==
* 
*  
* 
* , review by J. Doyle Wallis for DVD Talk (25 August 2003)
* " by Thomas Raven for freakengine (July 2011)
*  review by D. Trull for Lard Biscuit Enterprises 
*  review by David Blakeslee for Criterion Reflections (7 April 2014)
*  review by Hubert for Unseen Films (13 February 2014)

 

 
 
 
 
 
 
 
 


 