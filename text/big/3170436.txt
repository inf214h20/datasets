Ten Nights of Dreams
{{Infobox book |  
| name          = Ten Nights of Dreams
| title_orig    = 夢十夜 (Yume jūya)
| translator    = 
| image         =  
| caption = 
| author        = Natsume Sōseki
| illustrator   = 
| cover_artist  = 
| country       = Japan
| language      = Japanese
| genre         = Short Story
| publisher     = Asahi Shimbun
| release_date  = 1908
| english_release_date =
| media_type    = 
| preceded_by   = 
| followed_by   = 
}}

  or Ten Nights Dreams is a series of short stories by Natsume Sōseki. It was serialized in the Asahi Shimbun from July 25 to August 5, 1908.

Sōseki composed ten strange dreams set in various time periods beginning with his own time (the Meiji period) and including dreams set in the age of the gods, the Kamakura period, and 100 years in the future. The series begins with the phrase "I had a dream like this" (こんな夢を見た Konna yume o mita).

==Plot==
;The First Night
The dreamer finds himself sitting at a bedside next to a dying woman. Because of her appearance, the dreamer still asks the woman if she truly is dying. After confirming that she would indeed pass away without doubt, the woman asked a favor of the dreamer. The request was that when she dies, the dreamer must bury her by digging the grave with a large pearl oyster shell, put a fragment of a fallen star on her grave as the tombstone and wait one hundred years at which point she would return to see the dreamer again. Once the woman died, the dreamer completed the request began his 100-year wait. At one point a green stalk sprouted from the ground, budding and blooming into a white lily. The dreamer kissed the petals of the lily and came to a realization that one hundred years had passed.

;The Second Night
In the second dream, the dreamer returns to his room after leaving a monk’s room. Once the dreamer confirmed the existence of the dagger until the cushion he sat on, the monk came and scorned the dreamer for not reaching spiritual enlightenment as a Samurai. The dreamer then declared that he would "trade enlightenment for the life of the priest, but only once   achieved enlightenment".  Continuing on, the dreamer said that if he did not achieve contemplation by tonight, he would end his own life out of humiliation. After the declaration, the dreamer unsheathes the dagger feeling an urge to stab. From that point the dreamer struggled the paint, anger and sorrow while remaining in his cushion. At one point the dreamer felt that he was no longer himself and when the clock stroke a sound, the dreamer finds himself clutching his dagger in his right hand.

;The Third Night
The dreamer is walking with a six-year-old child on his back. The child is believed by the dreamer to be his son but is unsure as to why the child is blind and bald. Fear of the child begins to set into the dreamer as he continues into the woods with the child still on his back, directing and predicting the dreamer’s path. Soon the child pointed out the bottom of a cedar tree asking the dreamer if he remembered. The child then says how it has been 100 years since he was killed by the dreamer. Upon hearing these words the dreamer remembers how "he had killed a blind man at the bottom of the same cedar tree 100 years ago".

;The Fourth Night
A long table with small folding chairs appear in the fourth dream. In this dream, the dreamer is a small child who watches the interaction between a woman bringing water and what appears to be an old man drinking sake. The old man takes towels and twists it in front of the children outside. Telling the children to keep their eyes on the towel soon to become a snake, the old man then puts the twisted towel inside a box. The dreamer continues to follow the man in hopes of seeing the snake but the man continues onwards into a river as he sings. The dreamer waits for the old man to surface but old man never does in the end.

;The Fifth Night
The dreamer is fighting a war but had suffered a defeat in which he was captured alive. Meeting with the enemy captain he was given the choice between life and death which the dreamer chose death. Before being executed the dreamer asked to see his woman once more before his death. The captain agreed to wait until the woman arrived before the rooster crowed. The woman came on a horse and upon hearing the crow of the rooster, she rushed forward as the horse collapsed throwing itself and the woman forward into a hole. The crowing was from Amanojaku, the nymph of perversity who the dreamer then considered his sworn enemy.

;The Sixth Night
The dream begins with the dreamer hearing that Unkei was carving two Temple guardians, Nio, at the main gate of Gokoku-ji. The dreamer mentions that the scenery looks as if it was the Kamakura Period, but the onlookers were of the Meiji era. Unkei is carving without paying any attention to the noisy audience. The onlookers create a theory that the sculpture comes from the wood, rather than the hand of Unkei. Thus, the dreamer comes to the conclusion that he too should be able to create a Nio and heads home to attempt it. Unfortunately he isn’t able to find one and concludes that the Nio no longer exists in the wood of the Meiji Period.

;The Seventh Night
The dream begins with the dreamer finding himself on board a large ship and does not know where the ship is headed. The dreamer faces a sense of being completely lost as he didn’t know where the boat was headed or when he would be able to get off. The dreamer contemplates on jumping off the ship into the sea instead of staying on the ship due to his feeling of being lost. Nights pass and the dreamer becomes even more unhappy as the other passengers seem to have forgotten that they were on a ship. Finally the dreamer decides to kill himself. With no one around one night, he jumps into sea, but regrets it immediately.

;The Eighth Night
The dream begins with the dreamer heading to the barber shop to get his hair cut. While getting his hair cut, he watches people pass by through the mirror. He sees his friend, Shōtarō, with a woman, a soybean-curd maker blowing on his horn, and a geisha who had not yet put her make-up on. His hair cutter asks if he had seen the goldfish vendor, and the dreamer replies that he hasn’t. The dreamer next hears somebody making rice cakes, but they never appeared in the mirror. Then the dreamer notices a woman who is concentrated in counting bills. When the dreamer turns around, the woman counting bills was nowhere in sight. Leaving the barber shop, the dreamer encounters the goldfish merchant and notices that he doesn’t move.

;The Ninth Night
The dream is set in a world that has "somehow become unsettled".  A mother and a three-year-old child is waiting for the father to return who had left a while back. Every day the child would ask his mother, "When will father be back home?” and the mother would reply "Over there", in which the child would repeat. At night the mother would take them to the shrine, to pray to the god Hachiman for her husband’s safety. The mother then ties the child to the balustrade of the oratory, and then she heads down the stairs to complete the prescribed hundred prayers as quickly as possible. It is then mentioned that the father had already been killed by a lordless warrior. At the end of the dream, the dreamer finally states that the story was told by his mother in his dream, thus leaving viewers to perhaps believe that the child in the story is the dreamer.

;The Tenth Night
The dream begins with the dreamer mentioning that Ken has come to tell him that Shōtarō, who has been mentioned in past dreams, is sick. Shōtarō is described as the best-looking man in the neighbourhood who wears a Panama hat. He has a hobby of looking at the faces of women, and when there is a lack of women, he turns to look at fruits. One day Shōtarō encountered a woman who "seemed to be a woman of quality".  He helped her out and ended up heading to her house and since then he had been away. Shōtarō returns after the seventh day of being away and tells his worried friends and relatives that he had taken a train to the mountains. Encountering a cliff during their trip, the woman invited Shōtarō to jump. He declines and she asks whether he preferred to be licked by pigs, which is a dislike of Shōtarōs. Declining the woman again, pigs begin to come at Shōtarō and he knocks each one off the cliff. Eventually he is licked by one and collapses. Ken tells the dreamer this story and advises him to not stare too much at women.

==Film adaptation==
 Japanese film called Yume Jūya premiered in 2007. The film is a collection of ten vignettes made by eleven directors (two worked together) ranging from industry veterans to novices. The movie was released by Cinema Epoch in October 2008.

* The First Dream
:: Director: Akio Jissoji
:: Screenwriter: Kuze Teruhiko
:: Stars: Kyōko Koizumi, Matsuo Suzuki, and Minori Terada

* The Second Dream
:: Director: Kon Ichikawa　
:: Screenwriter: Kokuji Yanagi
:: Stars: Tsuyoshi Ujiki and　Nakamura Umenosuke

* The Third Dream
:: Director: Takashi Shimizu
:: Stars: Keisuke Horibe and Yuu Kashii

* The Fourth Dream
:: Director: Atsushi Shimizu
:: Screenwriter: Shinichi Inotsume Koji Yamamoto and Toru Shinagawa

* The Fifth Dream
:: Director & Screenwriter: Keisuke Toyoshima
:: Stars: Mikako Ichikawa and Koji Ōkura

* The Sixth Dream
:: Director & Screenwriter: Matsuo Suzuki
:: Stars: Sadao Abe, TOZAWA and Yoshizumi Ishihara

* The Seventh Dream
:: Directors: Yoshitaka Amano and Masaaki Kawahara
:: Stars: Sascha and Fumika Hideshima

* The Eighth Dream
:: Director: Nobuhiro Yamashita
:: Screenwriter: Kenichiro Nagao Hiroshi Yamamoto

* The Ninth Dream
:: Director & Screenwriter: Miwa Nishikawa
:: Stars: Tamaki Ogawa and Pierre Taki

* The Tenth Dream
:: Director: Yūdai Yamaguchi
:: Screenwriters: Yuudai Yamaguchi and Junya Katō
:: Character Design: MAN☆GATARO
:: Stars: Kenichi Matsuyama, Manami Honjō, Kōji Ishizaka, and Yasuda Dai Circus

* Prologue and Epilogue
:: Direct: Atsushi Shimizu
:: Stars: Erika Toda

==See also==
  Dreams (1990), a film by Akira Kurosawa

==Notes==
 

==External links==
 
*  using modern orthography  

 
 

 
 
 
 
 
 
 
 