Flock of Dodos
{{Infobox Film
| name     = Flock of Dodos: The Evolution-Intelligent Design Circus
| writer   = Randy Olson
| director = Randy Olson
| producer = Ty Carlisle
| language = English
| runtime  = 84 minutes
| country  = USA
| distributor = New Video (US Home Video), Documentary Educational Resources (International)
}}

Flock of Dodos: The Evolution-Intelligent Design Circus is a documentary film by American marine biologist and filmmaker Randy Olson. It highlights the debate between proponents of the concept of intelligent design and the scientific consensus that supports evolution.
 SUNY Stony Brook, marking the celebration of Charles Darwins birthday.

==Synopsis== scientific consensus position of  evolution. 

The evolutionarily famous dodo (Raphus cucullatus) is a now-extinct bird that lived on the island of Mauritius. Due to its lack of fear of humans and inability to fly, the dodo was easy prey, and thus became known for its apparent stupidity. 

The film attempts to determine who the real "dodos" are in a constantly evolving world: the scientists who are failing to effectively promote evolution as a scientifically accepted fact, the intelligent design advocates, or the American public who get fooled by the "salesmanship" of evolution critics. The film gives equal air time to both sides of the argument, including intelligent design proponent Michael Behe and several of his colleagues.

While Randy Olson ultimately sides with the scientists who accept evolution, the scientists are criticized for their elitism and inability to efficiently present science to general public, which ultimately contributes to spread of misconceptions. 
 Paley to the present-day incarnation promoted by the Discovery Institute. Olson mixes in humorous cartoons of squawking dodos with commentary from his mother and interviews with proponents on both sides of the intelligent design/evolution debate.

On the intelligent design side, Olson interviews Behe, John Calvart (founder of the Access Research Network) and a member of the Kansas school board.  Olson also unsuccessfully tries to interview Kansas Board of Education member Connie Morris (associated with Kansas evolution hearings) and members of the Discovery Institute.

==Release==
The documentary premiered at the   in the US and available on DVD. 

The documentary was praised by the journal Nature (journal)|Nature  and a variety of other publications.  

In 2007, Olson released a collection of "pulled punches," of unreleased material that he chose to leave out that reflected poorly on intelligent design supporters. 

==Discovery Institute response==
Olson invited the  , February 7, 2007. 
 ontogeny recapitulating phylogeny." Myers and other critics of intelligent design have shown that each of these texts treats Haeckels theory of ontogeny recapitulating phylogeny as an example of an outdated exaggeration.  Myers notes in his rebuttal of the criticism from design proponents that, "I would add that progress in evolutionary biology has led to better explanations of the phenomenon that vertebrate embryos go through a period of similarity: it lies in conserved genetic circuitry that lays down the body plan."  

In early 2007, in response to Olsons claim, "the Discovery Institute is truly the big fish in this picture, with an annual budget of around $5 million," the Institute responded that their budget is only $4.2 million, and that they spend close to $1 million per year funding intelligent design.  

==References==
 

==External links==
* 
* 
*Profile of "Flock of Dodos" director   by Eric Sorensen in (2007) Forward thinkers: People to watch in 2007.  Conservation, 8(1).
* 
*  
*  

 
 
 
 
 
 
 
 