Aahaa
{{Infobox film
| name = Aahaa
| image = Aahaa DVD Cover.jpg
| caption = DVD Cover Suresh Krishna
| producer = R. Mohan
| writer = Crazy Mohan Vijayakumar Bhanupriya Srividya Sukanya Deva
| cinematography = S. Saravanan
| editing = Suresh Urs
| distributor = Shogun Films Ltd
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}
 Suresh Krishna starring Rajiv Krishna and Sulekha in the lead roles. The film was a decent hit among Deepavali releases and ran for 100 days. 

This film later remade in Telugu titled Aaha starring Jagapathi Babu.

==Plot==
Shriram (Rajiv Krishna) is the son of Pepsi Parasuram (Vijayakumar) and the younger brother of Raghu (Raguvaran). Raghu is married to Bhanupriya and has a son. While Raghu is a model son, Shriram is considered a wastrel. Shriram falls in love with Janaki (Sulekha), the daughter of a Brahmin cook (Delhi Ganesh). Parasuram opposes the match because of the difference in their status.

Meanwhile, Raghus college sweetheart, Gita (Sukanya) is dying and she wants to spend her last days with him. She contacts him after five years and they begin meeting secretly.

Shriram finds out about this and tries to keep the family secrets, take the blame for everyones shortcomings and becomes the family scapegoat and his fathers favorite whipping boy. The rest of the film examines whether or not Raghu is finally vindicated in the eyes of his family.

==Cast==
* Rajiv Krishna as Shriram
* Sulekha as Janaki Vijayakumar as Parasuraman
* Raghuvaran as Raghuraman
* Delhi Ganesh
* Bhanupriya as Rajeshwari
* Radha Bai
* Srividhya as Pattamaal Sukanya as Geetha
* Dhamu
* Krishna Master Mahendran as Ajay

==Production==
The film marked a comeback for actress Bhanupriya, while debutants Rajiv Krishna (Chandresh) and Sulekha were selected to play the lead roles. 

==Soundtrack==
Music is composed by Deva. Song "Mudhan Mudhalil" was lifted from Hindi song "Sochenge Tumhe Pyaar" from Deewana (1992 film)|Deewana.
* "Hip Hip Hurray" - Hariaran
* "Kozhi Vandhadhaa" - Malaysia Vasudevan, Yugendran, Sujatha, Anuradha Sriram
* "Mudhan Mudhalil" - Hariharan, Chithra
* "Mudhan Mudhalil II" - Hariharan
* "Yehi Hai Right Choice" - Gopal Rao, Sujatha
* "Seetha Kalyanam" - Krishnaraj, Malaysia Vasudevan, Meera Krishnan, Sujatha

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1998
|Aaha Cinema of Telugu
| Jagapathi Babu, Sanghavi, Jayasudha, Raghuvaran
| Suresh Krissna
|-
| 2009
|Ghauttham Cinema of Kannada
|Prem Prem Kumar, Mohan
|K. Rajeev Prashad
|-
|}

==References==
  

==External links==
*  

 

 
 
 
 
 
 


 