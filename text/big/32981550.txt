Conrad in Quest of His Youth
{{infobox film
| name           = Conrad in Quest of His Youth
| image          = Conrad in Quest of His Youth (1920) - 1.jpg
| image_size     = 125px
| caption        = Newspaper ad
| director       = William C. deMille
| producer       = Adolph Zukor Jesse Lasky
| writer         = Olga Printzlau (scenario)
| based on       =  
| starring       = Thomas Meighan
| music          =
| cinematography = L. Guy Wilky
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels, 5,926 feet
| country        = United States Silent (English intertitles)
}} silent comedy drama film directed by William C. deMille and starring Thomas Meighan. The film is based on a novel Conrad in Search of His Youth by Leonard Merrick adapted and written for the screen by Olga Printzlau. This film survives at the Library of Congress.  

==Cast==
*Thomas Meighan as Conrad Warrener
*Mabel Van Buren as Nina
*Mayme Kelso as Gina
*Bertram Johns as Ted
*Margaret Loomis as Roslind
*Sylvia Ashton as Mary Page
*Kathlyn Williams as Mrs. Adaile Charles Ogle as Dobson
*Ruth Renick as Tattie
*A. Edward Sutherland as Conrad(at 17)(*billed as Eddie Sutherland)

==DVD release== Region 0 DVD-R by Alpha Video on January 28, 2014. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 