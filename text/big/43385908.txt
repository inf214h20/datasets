Legend of the Sea Wolf
{{Infobox film
 | name = Legend of the Sea Wolf
 | image = Legend of the Sea Wolf.jpg
 | director = Giuseppe Vari
 | writer =  
 | starring = Chuck Connors Barbara Bach Ivan Rassimov
 | music =  Guido & Maurizio De Angelis
 | cinematography =  Sergio Rubini
 | editing = 
 | length = 92 minutes
 | released =  
 }} 1975 Italian adventure film directed by Giuseppe Vari. It is based on the novel The Sea-Wolf by Jack London.      

==Plot== shanghaied and wakes up abord Captain Larsens ship on a seal hunting voyage of indeterminate length.  Captain Larsen runs a tight ship using "hands on" techniques to quell on board dissension. With seamanship unknown to Humphrey, he is assigned to the ships cook as a Scullery maid.  The Captain informs Humphrey that the sea voyage will allow him to stand on his own two feet and not walk in his fathers shoes.

Unsuccessful in their seal hunt, Captain Larsen decides to poach on the seal hunting area of his brother, Death Larsen. Their ship also rescues three survivors from a steamship that has exploded, Maud Brewster and two stokers.

== Cast ==

*Chuck Connors: Wolf Larsen
*Barbara Bach: Maud Brewster
*Giuseppe Pambieri: Humphrey Van Weyden
*Luciano Pigozzi: Thomas Mugridge 
*Ivan Rassimov: Death Larsen 
*Rik Battaglia
*Pino Ferrara
*Lars Bloch
*Maurice Poli
*Nello Pazzafini
*Renato Baldini

==References==
 

==External links==
* 
 

 
 
 
  
 
 


 
 