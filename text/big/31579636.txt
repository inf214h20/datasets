The Dilettante
{{Infobox film
| name           = The Dilettante
| image          = 
| caption        = 
| director       = Pascal Thomas
| producer       = 
| writer         = Jacques Lourcelles Pascal Thomas
| starring       = Catherine Frot
| music          = 
| cinematography = Christophe Beaucarne
| editing        = 
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = France
| language       = French
| budget         = $3,4 million
| gross          = $3,641,227  
}}
The Dilettante ( ) is a 1999 French comedy film directed by Pascal Thomas and starring Catherine Frot, Sébastien Cotterot and Barbara Schulz.  The film was entered into the 21st Moscow International Film Festival where Catherine Frot won the Silver St. George for Best Actress.   

==Partial cast==
* Catherine Frot - Pierrette Dumortier
* Sébastien Cotterot - Éric
* Barbara Schulz - Nathalie
* Jacques Dacqmine - Delaunay
* Christian Morin - Edmond Rambert
* Marie-Christine Barrault - Thérèse Rambert
* Odette Laure - Zoé de la Tresmondière
* Michèle Garcia -  The College Manager
* Jean-François Balmer - Président of the tribunal
* Armelle - The Judge

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 