Le Drakkar
{{Infobox film
| name           = Le Drakkar
| image          = 
| image size     = 
| caption        = 
| director       = Jacques Pierre
| producer       = 
| writer         = Jacques Pierre Jean Amila
| narrator       = 
| starring       = Sady Rebbot
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1 December 1973
| runtime        = 
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Le Drakkar is a 1973 French film directed by Jacques Pierre.   

==Cast==
* Sady Rebbot - Michel
* Jean Franval - Paulot
* Pierre Fromont - Mimile
* Christine Malouvrier - Arlette
* Anouk Ferjac - Kate
* Jacques Charby - Jorioz
* Dominique Rozan - Henri
* Françoise Giret - Raymonde Nogaret
* Patrick Verde - Pierre Nogaret
* Isabelle Huppert - Yolande
* Nicolas Silberg - Jean-Paul
* André Charpak - Caparacci
* Louis Julien - Marie-Anne
* Alain Dorval - Broque
* Alain Tannenbaum - Rivoire
* Gilles Brandin - Gégé

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 