Sudigadu
 
 

{{Infobox film
| name           = Sudigadu
| image          = Sudigadu Poster.jpg
| caption        = Film poster
| director       =Bhimaneni Srinivasa Rao
| producer       = Chandrasekhar D Reddy 
| writer         = C. S. Amudhan
| starring       = Allari Naresh Monal Gajjar
| music          = Sri vasanth
| cinematography = Vijay Vulaganath
| editing        = Gowtham Raju
| studio         = Arundathi Movies
| distributor    = 
| released       =  
| runtime        = 
| country        = India 
| language       = Telugu
| budget         =   http://www.imdb.com/list/z1BNUchKVnY/ 
| gross          =   
}} tamil super hit movie Thamizh Padam.    The film features Allari Naresh and Monal Gajjar in the lead roles. 
  Allari Naresh plays double roles in this movie as father and son.  The film was released on 24 August 2012. 

It is said to be the Landmark Film of Allari Naresh. The movie has not only elevated his career and success graph, it has also opened new markets and new commercial possibilities for him. This is the first Allari Naresh movie to do so well in overseas territories and in a few countries, this is the first ever Allari Naresh film to be screened. The movie’s phenomenal openings and good collections have also cemented Allari Naresh’s status as a minimum guarantee star.

==Plot==
Kamesh (Allari Naresh) and his wife (Hema (actress)|Hema) become the proud parents of a dynamic and powerful new baby who is born with a six pack body. Just when the baby is born, Thikkal Reddy (Jaya Prakash Reddy) comes in as the villain in search of his enemy who runs into New Born Babys room. He prays to god to save him and hides in the room. The New Born Baby does urination for a long time which leads to the death of Thikkal Reddys elder son and he becomes a sworn enemy of the baby. To save the baby from Thikkal Reddy, Kamesh gets his mother (Kovai Sarala) to escape to Hyderabad. The baby grows up into Siva (Allari Naresh), a powerful and dynamic young guy with seemingly invincible powers like a Telugu Cinema hero.

Siva can make bullets stop, challenge time and even make Posani Krishna Murali speak intelligently. On a side note, Siva meets Priya (Monal Gajjar) over the course of time and falls in love with her. Thikkal Reddy’s gang members keep hunting for Siva and he decides to fight back. He also faces resistance from the mysterious Don D.

He fights them all with his powers and it is revealed that he is Siva Manohar I.P.S., a Young Cop in Undercover Operation. He has all the villains dead and when it comes to Don D, it is none other than his Grand Mother. She does so as to elevate Siva as a powerful hero and herself as a Don. At the court both are Exonerated and Siva is promoted as D.G.P. of Andhra Pradesh.

==Cast==
 
* Allari Naresh as Kamesh / Siva Manohar I.P.S. a.k.a. Siva
* Monal Gajjar as Priya
* Brahmanandam as Jaffa Reddy Ali as Doctor 
* M. S. Narayana as Rambo
* Krishna Bhagavaan as Priyas Fathers P.A.
* Raghu Babu as Henchman of Thikkal Reddy Venu Madhav as Car Driver (Special Appearance) Chandra Mohan in a Cameo appearance
* Chalapathi Rao as Commissioner of Police of Hyderabad
* Dharmavarapu Subramanyam as Rayudu 
* Posani Krishna Murali in a Cameo Appearance
* L.B. Sriram as Remo Kondavalasa as Robo
* Sayaji Shinde as Priyas Father
* Jaya Prakash Reddy as Thikkal Reddy Jeeva as D.G.P. of Andhra Pradesh
* Srinivasa Reddy as Siddha 
* Shankar Melkote as Judge 
* Suman Setty as Rambos Father 
* Narsing Yadav as Narsing 
* Kovai Sarala as Kameshs Mother / Don D Hema as Kameshs wife
* Sri Latha as Heroine Friend
* Yadagiri Rodda as Enemy of Thikkal Reddy 
* Kishore Daas as Thodagottudu school principal
 

==Critical reception==

Sudigadu has received mostly positive reviews from critics and audience.The Times Of India gave a rating of 4/5.The Hindu stating "Sudigadu is definitely worth a watch. The humour, here, is without malice and that itself is a big plus."NDTV gave a review stating "BHIMANENI SRINIVASAS TELUGU FILM SUDIGADU IS A LAUGH RIOT THAT KEEPS THE AUDIENCE ENTERTAINED RIGHT UP TILL THE END".Shekhar of Oneindia Entertainment gave a review stating "Sudigadu is a clean entertainer." Teluguone.com gave a review of rating 3/5 stating " Watch Sudigadu for hilarious spoof comedy and witty dialogues." Mahesh Koneru of 123telugu.com gave a rating of 3.25/5 stating "Sudigaadu is a good clean entertainer that will make you laugh out loud. The spoofs are brillent. Sudigaadu is one very enjoyable spoof on TFI and it makes for a good watch this weekend. Don’t miss it." Super Good Movies gave a rating of 3/5 stating "Sudigadu is an entertaining film which will please the audiences that can appreciate a complete spoof.

==Box office==
Sudigadu has rocked the film goers across the globe and proved super hit at the Box Office.The movie had a strong start in both Indian and overseas market with nearly 70% occupancy. The film has collected approximately Rs 58&nbsp;million at the Indian Box Office on its 1st day and its performance is far better than that of several Telugu films of 2012.
 Devudu Chesina Gabbar Singh, no other Telugu movie has received such a good response at the USA Box Office in recent months. According to trade analyst Taran Adarsh, the film has taken Rs 2,455,000 ($44,255) from its screening in the country on Thursday and Friday.
 Devudu Chesina Manushulu had collected Rs 348,000 ($6,288) from 29 screens on the first day. Onamalu had grossed Rs 180,000 ($3,243) in the first weekend. Adhinayakudu had raked in Rs 545,000 ($9,844) on its opening day in the country.Sudigadu collected a total collections of  till end of its run(world wide) 

==Soundtrack==
{{Infobox album 
| Name = Sudigadu
| Artist = Sri Vasanth
| Type = Album
| caption =
| Cover =
| Released =  
| Recorded = 2012
| Genre = Film soundtrack
| Length = 21:29
| Label = Aditya Music
| Producer = Sri Vasanth
| Last album =
| This album =
| Next album =
}}
The music launch of Sudigadu was held on 23 July in Hyderabad. Sri Vasanth has scored the music and it was also reported that Allari Naresh also sang a song in this film. The audio was released by Dasari, who handed over the first copy to Tanish, Nikhil, and Uday Kiran. It is the only film which had a Platinum Disc Function before the release of its audio.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 21:29
| lyrics_credits = yes
| title1 = Twinkle Twinkle
| lyrics1 = Ramajogayya Sastry Ranjith & Chorus
| length1 = 4:17
| title2 = Inky Pinky
| lyrics2 = Bhimaneni Roshita Sai
| extra2 = Allari Naresh, Rahul Sipligunj & Chorus
| length2 = 2:03
| title3 = Yenduke Cheli
| lyrics3 = Ananth Sreeram
| extra3 = Haricharan & Chorus
| length3 = 4:11
| title4 = Jagaalu Mottham
| lyrics4 = Sirivennela Sitaramasastri
| extra4 = Revanth
| length4 = 1:59
| title5 = Gajibiji Gathukula Roaddu
| lyrics5 = Ramajogayya Sastry
| extra5 = Sahithi, Sri Krishna, Revanth, Pruthvi Chandra
| length5 = 4:31
| title6 = Zara Zara Chandrabose
| extra6 = Allari Naresh, Geetha Madhuri, Vedala Hemachandra|Hemachandra, Haritha, Chorus
| length6 = 4:26
}}

==List of Spoofs==
Sudigadu is a movie made based on many popular Films in the past 10 Years and can be termed both a Satire & Homage to Telugu Cinema. The given below is a list of Some of the Spoofs by the Director Bhimaneni Srinivasa Rao only to provide some Laughs and are unintentional and Not to Hurt anyones Feelings.

===Qualifications of a Telugu Cinema Hero (By the Director)===
* Heroes worship their Mother more than Gods and Goddesses (Even in these days when all send their parents to Old Age Homes).
* Heroes take utmost care of their Sisters and save them from any Small to Big problems.
* Heroes are capable of making Heroines fall in love with them although Heroines are Rich & Heroes are Poor.
* Heroes punish anyone who does Crime and Harm to Society, even though they are the Fathers of Heroines (To Be Frank, Heroes always Love Villains Daughters)
* Heroes (wearing Modern Costumes) always give Lectures to Heroines to follow the Customs & Traditions. (But they Dance only with the Heroines wearing Modern Costumes only)
* Heroes feel their Friends Sisters to be their Sisters (There is a scope of Change in Opinion if one of their Friends Sisters is very Beautiful)
* Heroes can fight the Crimes & Injustice prevailing in the Society very easily
* Heroes can ride Air planes and Helicopters if Necessary, although it is the first time they see these.
* Heroes always Remain fresh even after long Fights with Hundreds of Men.
* Heroes have a Six Pack Body, and are experts in Dialogue Delivery and Fighting Skills.

===Parts of a Hero (By the Director)===
* Brain - 24x7 solves the problems faced by the People in the Society.
* Eyes - Get Red in Color with Anger after seeing the Injustice done to the People & Crimes Committed in the Society.
* Mouth - Speaks Hi-Fi Punch Dialogues very Easily.
* Shoulders - Carry on the Indian Financial System.
* Heart - Always strives for the Poor (When Heroine is not Beside him).
* Hands - Has the Capacity to Defeat Hundreds of Powerful Villains.
* Feet - Makes the Land prosper with all riches and happiness with its one touch.

===Film Episodes===
* Eega - The Style of Opening & Closing credits are adapted from S.S. Rajamoulis Eega i.e. the whole story is a narrative of a Father to his Daughter. Chatrapati - Bhavani Scene Racha - In the first fight scene the hero spoofs a dialogue from "Racha".
* Business Man - Most of the Dialogues of the film "Business Man" are uttered by the Hero, but in Hilarious Situations. Simha The villains son, upon getting kicked by the hero as infant goes into a coma and the doctors suggest he can be revived only after he gets kicked again by the hero.
* Jalsa - The villains son responds from the wheelchair when the hero passes by him.
* Chandramukhi - The heros entrance in the first fight scene
* Maryada Ramanna - The villain cannot kill people outside his house - only within. (As compared to the opposite condition in the original  )
* Pokiri - The heros real identity being that of a police officer. Boys and 7G Brundhaavan colony - The heros gang is hanging out on a wall and a friends dad admonishes him for wasting his (blood) money on cigarettes. Premikudu - The hero attempts to impress the heroine through dance. The funny side comes as Naresh draws face of a monkey rather than heroine.
*Mass- Another attempt of the hero to impress the heroine by telling her to spend 10 minutes time with her during which he takes her to a home where he has adopted failed-love people. Robo - Hero Scans various Dance books at a time in no time to Learn Dance and Impress his Lover. Also he kills a Villain by catching the bullet left by his Gun, Made a lot of Bullets and Firing them on the Goons stating "Happy Diwali Folks".
* Dookudu - The hero utters the famous dialogue "Mind lo fix aithe blind ga velipotha" (I go blindly if i am fixed in my mind) but following the dialogue, he hits a rack in his room.
* Ye Maaya Chesave - The heroine refuses to court the hero on accounting of being older to him by a day. Tagore - Hero murders a lady "adam teaser" who rapes several Men with the help of a Banana Peel Bharateeyudu - Hero murders a Criminal by showing him a scene from a daily serial pootha rekulu in large no. of televisions at a time as the Criminal was the Main reason for Water Problems in the City. Kushi - Heroine gets a shock during her childhood when a boy touches her. In the present Hero touches the hand of heroines hand which gives her a shock. She hugs him and confesses that she loves him, but the unknown truth to her is that the shock was due to an Electric Battery kept in the Heros mouth. A sorcerer finds the location of Hero through Google which is the satire of sorcerer character in Magadheera. Aparichitudu - Hero takes a criminal to a Buffalo shed but fails to create a buffalo stampede, which happens in Aparichitudu. Seeing this the Criminal dies laughing as he was a heart patient, which was Heros main motive.
* Khaleja - Srinivas Reddy plays a role similar to the role of Shafi in Mahesh Babus Khaleja
* Peddarayudu - Dharmavarapu Subrahmanyam plays a role which is a satire of Mohan Babu in Pedarayudu. Narasimha - A Bullet goes back after seeing Hero in front of it. (There an Enraged Bull runs away seeing the Hero) Prabhas is mentioned by heros grandmother to him in a hilarious way Leader - Suhasini to Rana is mentioned by heros grandmother to him in a hilarious way Sasirekha Parinayam Chandamama  - Yenduke Cheli song is pictured in a similar way to Elaa Entha Sepu song from Sasirekha Parinayam and Bugge Bangaarama song from Chandamama.
* Challenge (Reality Show) - The famous reality dance program is mimicked in a hilarious fashion. A typical judgment segment involving Omkar and three judges(Sundaram Master, Shiva Shankar Master and Posani Kirshna Murali) is presented after Nareshs dance performance.Shiva Shankar Master and Posani Kirshna Murali have acted as themselves in this scene. 7th Sense - Hero controls the minds of henchmen using hypnosis by just looking at them. Bommarillu - Heroine saying to a bull that it would get horns if it does not hit M.S.Narayana twice. Gabbar Singh - Hero says the famous "Naakkonchem Tikkundi.. Kaanee Daaniki O Lekkundi.." in a comic way. Arya - Heroine and her father exchange characters like hero and heroine in this movie. Siva - Heros name and villan and his side kick are portrayed similarly with the same names. Suryavamsam - Hero becomes rich within a song similar to this movie. Indra - Hero kisses land before entering his native place. Run - Hero is chased by thugs. He uses white paint to color his hair and the thugs find him  but  are unable to recognize him though hes in the same dress.
* Gharshana - The opening scene of the movie is imitated in a comic way by the hero.
* Dammu - The dialogue "padi taraalu ne vamsam lo maga pilladini kanali ante bayapadathaaru" is said in the film.
* Kondapalli Raja - Hero challenges heroines father about becoming richer than him.
* Julai - The dialogue "puvvulni ammayilani chupinchandi ra" is said by one of the thugs in a comic way. Arunachalam Hero creates a flower magically and throws it behind intended to hit the heroin, but in this case, he does it twice and they both miss the aim and hit the heroins father and maid. Buisnessman The hero as a child, says"Hyderabad ni uchha poyinchadaaniki"
==Awards==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 60th Filmfare Awards South Filmfare Award Best Supporting Actress Kovai Sarala
| 
|-
|}

==References==
 

== External links ==
*  

 

 
 
 