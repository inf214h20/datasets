All Things to All Men (film)
{{Infobox film
| name           = All Things to All Men George Isaac
| producer       = George Isaac Pierre Mascolo
| writer         = George Isaac
| starring       = Gabriel Byrne Rufus Sewell Toby Stephens Julian Sands
| music          = Thomas Wanker
| cinematography = Howard Atherton
| editing        = Eddie Hamilton
| released       =   }}
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = £3 million   
| gross          =
}} British film George Isaac.  It stars Gabriel Byrne, Rufus Sewell, Toby Stephens, and Julian Sands.  Sewell plays a dirty cop who manipulates both the underworld and police in order to entrap a thief.

== Plot ==
After a stakeout, Parker, a dirty cop, arrests the son of mob boss Joseph Corso.  Parker uses the arrest as leverage against Corso and forces him to recruit Riley, a thief that Parker wants to entrap.  After Corso threatens him, Riley reluctantly agrees to perform a burglary for Corso, unaware that he is being manipulated by Parker.  Parkers target, however, turns out to be more secure than expected, and Corso is unable to procure access codes.  Riley balks at Corsos suggestion that they torture a worker for the codes, so Parker retrieves the codes from Scotland Yards security system.  However, Dixon, Parkers protege, becomes increasingly suspicious that theres more to Parkers machinations than he lets on.  Dixon takes his concerns to Sands, Parkers long-time partner, who threatens to block Dixons promotion if he continues to ask questions.

During the burglary, Riley and Corsos lieutenant, Cutter, are surprised to find much more money than planned.  Riley questions how they could have lucked into such a major heist, but Cutter dismisses his concerns.  Cutter later makes an attempt on Rileys life and tries to escape with the loot, which results in a high speed chase through London.  Cutter dies when his car crashes, and Riley takes the money.  Meanwhile, Dixon convinces Sands to go straight, and Sands confronts Parker.  Unwilling to give up his schemes, Parker kills Sands and attempts to frame Corso for the murder.  Parkers plans go awry when Dixon survives an assassination attempt, and Parker becomes increasingly desperate to raise money to pay off his debts to powerful mobsters. Parker steals from several of his associates and plans his escape from London once he can retrieve the money stolen by Riley.

Riley sets up a meeting with Corso, and Parker uses his contacts to find the location.  Corso, Riley, Parker, and Dixon converge on the meeting spot, and Parker kills Corso.  Dixon arrives just as Parker is about to murder Riley and take the money.  Disgusted, Dixon kills Parker and lets Riley escape.  In the aftermath, Dixon meets with his superiors, who want him to keep silent about the details of the case. Dixon cynically compares Parkers actions to that of a mobster, and his superior agree, though they had been aware of much of Parkers actions.  In return for his silence, they offer him a promotion, and Dixon agrees, reasoning that the resulting scandal would be disastrous for the police force.

== Cast ==
* Gabriel Byrne as Joseph Corso 
* Rufus Sewell as Parker
* Toby Stephens as Riley
* Elsa Pataky as Sophia Peters
* Gil Darnell as Adrian Peters
* Leo Gregory as Dixon
* MC Harvey as Curtis Carter
* Pierre Mascolo as Mark Corso
* Julian Sands as Cutter
* Terence Maynard as Sands

== Production ==
Shooting took place in London over seven weeks.  Funding came in part from Mascolos father.  Isaac, who had never written a screenplay before, said that he could not find one that he wanted to film.  Mascolo was supportive when Isaac suggested that he write his own.  Influences included The Driver, The French Connection, Marathon Man, Layer Cake, and The Long Good Friday. 

== Release == PAL speed-up.

== Reception == Time Out London rated it 2/5 stars and called it "another London-set crime flick with mostly unfulfilled ambitions to the cool sheen of a Michael Mann thriller."   Alistair Harkness of The Scotsman rated it 3/5 stars and wrote, "All Things to All Men is the latest attempt to make a British Michael Mann-style crime epic based on a fundamental misunderstanding of what Michael Mann actually does as a filmmaker." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 