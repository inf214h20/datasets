Magic Island (film)
{{Infobox Film
| name           = Magic Island
| image          =
| image_size     =
| caption        =
| director       = Sam Irvin
| producer       = Albert Band Debra Dion
| writer         = Neil Ruttenberg Brent V. Friedman
| starring       = Zachery Ty Bryan Andrew Divoff Edward Kerr Lee Armstrong French Stewart Jessie-Ann Friend Oscar Dillon Abraham Benrubi Sean OKane Schae Harrison Janet Dubois Terry Sweeney Martine Beswick Isaac Hayes Sam Irvin
| music          = Richard Band
| cinematography = James Lawrence Spencer
| editing        = Margeret-Anne Smith
| studio         = Full Moon Entertainment Moonbeam Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
}}

Magic Island is a 1995 direct-to-video film released by Moonbeam Entertainment, the kidvid division of Full Moon Entertainment.

==Plot==

Jack Carlisle is a disillusioned 13-year old boy. His mother is always away at work since his father left. He decides to run away, as he feels his mom wont miss him. As he is ready to leave, his nanny convinces him to read a magic book that belongs to her. The book is about a pirate adventure on Magic Island. As Jack reads the book, he is sucked into the world and goes on numerous adventures with Prince Morgan, while fleeing the evil Blackbeard the Pirate. He is even saved by Lily, a beautiful mermaid, whom he falls in love with. Lily is given the power to turn into a human and accompanies Jack on his adventure. Along the way, Jack encounters sand sharks, a tree that grows the favorite food of the person who climbs it, and a cursed temple full of treasure. Jack also uses items he brought along with him in his "magic" bag to stop the pirates. Eventually Jack is able to return home. He wakes up to his loving mother, and finds that his jeans are now torn and frayed and the flag on Prince Morgans ship has transformed into his bag—signs that his adventure may have actually happened.

==DVD release==

As of August 17, 2012, neither Paramount Pictures or Full Moon Features have announced any plans to release the film onto DVD and Blu-ray.

==Cast==
*Zachary Ty Bryan as Jack Carlisle. The 13-year old boy who is convinced if he runs away, no one will miss him. He gets sucked into the book and joins Prince Morgan, Gwyn, and Dumas on their adventure to find the Treasure before the villainous Captain Blackbeard and his band of pirates.
*Andrew Divoff as Blackbeard. The wicked pirate who is determined to get the treasure first at all costs.
*Edward Kerr as Prince Morgan the leader of the good guys.
*Lee Armstrong as Gwyn the only female member of the good group. She is the tough fighter.
*French Stewart as Supperstein
*Jessie-Ann Friend as Lily, who is so named by the fish, for "Lily" means "Mermaid" in "fish-talk." She saves Jacks life when he almost drowns. By saving his life, she is allowed a wish and uses it to gain legs and join Mad Jack on his adventures.
*Oscar Dillon as Dumas, the religious member of the good group.
*Abraham Benrubi as Duckbone
*Sean OKane as Jolly Bob
*Schae Harrison as Mrs. Carlisle, Jacks mother.
*Janet Dubois as Lucretia, the Carlisles housekeeper who gives Jack her "Magic Island" book.
*Terry Sweeney as Funny Face (voice)
*Martine Beswick as Lady Face (voice)
*Isaac Hayes as Mad Face (voice)
*Sam Irvin as Carbassas, The guardian of the Treasure.

==External links==
*  
*  

 
 
 
 
 
 