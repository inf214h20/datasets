Two Moon Junction
{{Infobox film
| name = Two Moon Junction
| image = Twomoonmovieposter.jpg
| caption = Theatrical release poster
| director = Zalman King
| writer = MacGregor Douglas (story) Zalman King Martin Hewitt
| producer = Donald P. Borchers
| music = Jonathan Elias
| cinematography = Mark Plummer
| editing = Marc Grossman
| distributor = Lorimar Productions
| released =  
| runtime = 104 min.
| country = United States
| language = English
| gross = $1,547,397 (USA)
}}
 erotic thriller thriller and romance film written and directed by Zalman King, starring Sherilyn Fenn and Richard Tyson.    The original music score is composed by Jonathan Elias.

The film is noted for the final film appearances of Burl Ives and Hervé Villechaize, as well as the theatrical film debut of Milla Jovovich.

==Plot== Martin Hewitt) is away on business, April commences an affair with Perry (Richard Tyson), a carnival roustabout she had met a few days previously.

Aprils grandmother Belle Delongpre (Louise Fletcher) has assigned the local sheriff Earl Hawkins (Burl Ives) to keep an eye on her. Belle and the sheriff conspire to get rid of Perry on the day of April and Chads wedding. Perry escapes, and the film ends with April, wearing Chads wedding band and nothing else (in a shower), appearing in Perrys shower in a new location, ready for his return from work; they kiss.

==Main cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Sherilyn Fenn || April Delongpre
|-
| Richard Tyson || Perry Tyson
|-
| Louise Fletcher || Belle Delongpre
|-
| Burl Ives || Sheriff Earl Hawkins
|-
| Kristy McNichol || Patti Jean
|- Martin Hewitt || Chad Douglas Fairchild
|-
| Juanita Moore || Delilah
|-
| Don Galloway || Senator Delongpre
|-
| Millie Perkins || Mrs. Delongpre
|-
| Milla Jovovich || Samantha Delongpre
|-
| Hervé Villechaize || Smiley
|- 
| Dabbs Greer || Kyle
|}

==Filming locations==
The film was shot throughout Los Angeles County, with the major sequences filmed at The Shambala Animal Preserve, which is owned and operated by actress Tippi Hedren.

==Sequel==
The film was followed seven years later by a sequel, Return to Two Moon Junction. Louise Fletcher was the only significant actor to reprise a role in the sequel.

==Nominations==
*1988 Golden Raspberry Awards
:Winner: Worst Supporting Actress - Kristy McNichol

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 