The Watcher Self
 

{{Infobox film
| name           = The Watcher Self
| image          = The Watcher Self.jpg
| alt            = 
| caption        = 
| director       = Matt Cruse
| producer       = Matt Cruse Phil Macdonald
| writer         = Matt Cruse
| starring       = Karen French Julian Shaw Sylvia Seymour Lucy Charles Tony Stansfield Helen Barford
| music          = Paul Sumpter
| cinematography = Michelle Tofi
| editing        = Alex Weeks
| studio         = 
| distributor    = 
| released       = 
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Watcher Self is an upcoming British psychological thriller film written, produced and directed by Matt Cruse about one womans descent into hell.

==Plot==
Cora (Karen French) begins her day facing the consequences of a nightmare. Struggling to maintain a normal routine, she engages in a series of emotionally detached encounters and experiences a confusing psychological connection with the strange and elusive Van (Julian Shaw). Then echoes from the past threaten to derail her tenuous state of mind, and Cora becomes increasingly dislocated from her surroundings. Is she going insane, or is it something else? The Watcher Self is about what remains when the layers of sanity are gradually stripped away... and what may or may not be real.

==Cast==
* Karen French as Cora
* Julian Shaw as Van
* Sylvia Seymour as Wanda
* Lucy Charles as Kelly
* Tony Stansfield as Pitman
* Helen Barford as Rose

==Production==
The film was entirely self-funded by Matt Cruse and was shot on DSLR.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 