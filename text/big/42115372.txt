In Fear
{{Infobox film
| name           = In Fear
| image          = In Fear.jpg
| alt            =
| caption        = 
| director       = Jeremy Lovering
| producer       = James Biddle Nira Park
| writer         = Jeremy Lovering
| starring       = Iain De Caestecker Alice Englert Allen Leech
| music          = Roly Porter Daniel Pemberton
| cinematography = David Katznelson
| editing        = Jonathan Amos
| studio         = Big Talk Productions
| distributor    = StudioCanal UK  (UK)  Anchor Bay Entertainment  (USA)  Freestyle Releasing  (USA)
| released       =  
| runtime        = 85 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}

In Fear is a 2013 British psychological horror film directed and written by Jeremy Lovering. The movie premiered on 20 January 2013 at the Sundance Film Festival. It stars Iain De Caestecker and Alice Englert as a young couple terrorised by an unknown assailant.

==Plot==
After dating for just two weeks, Tom (Iain De Caestecker) invites  Lucy (Alice Englert) to go with him and some friends to a festival. The night before, Tom plans to take Lucy to the Kilairney House Hotel, which he booked online and is hidden away on a series of remote roads in the Irish countryside. Before making their way to the hotel, the couple stop at a pub and a confrontation occurs between Tom and some of the locals.

On the empty back road to the hotel, Tom and Lucy find themselves going in circles despite following the signs. Their satnav stops working. They eventually realise that they keep returning to the same point no matter which route they take and are unable to find their way back to the main road. Strange things begin happening, including Lucy spotting a man in a white mask and someone attempting to grab her from the darkness.

While speeding down the road away from their attacker, Tom clips a man in the road. He and Lucy pick up the man, who says his name is Max (Allen Leech). Max claims to be under attack by the same people stalking the couple. However, he is eventually revealed to be the true culprit. Tom kicks Max out of the car following a harrowing confrontation and Max breaks Tom’s wrist in a subsequent fight.

Lucy and Tom take their torches and search the woods when their car runs out of petrol. In the darkness Tom is grabbed and disappears. Lucy returns to the car alone and finds a petrol can in the front seat. After refilling the tank and with the satnav now mysteriously working again, Lucy drives on and eventually finds the hotel, but discovers that it is abandoned. The car park is a graveyard of derelict cars, suggesting that she and Tom are not the first victims.

Max returns in a Land Rover and pursues Lucy. When Lucy is able to stop the car, she finds a tube running from the exhaust pipe into the boot. She opens the boot and discovers Tom bound inside, dead from carbon monoxide poisoning from the tube forced into his throat.

As day breaks, Lucy finds the way back to the main road, but as she drives over a lonely moor towards it she sees Max standing in road in the distance. Max stretches out his arms and smiles at her. Lucy slams her foot on the pedal and accelerates towards Max.

==Cast==
*Iain De Caestecker as Tom
*Alice Englert as Lucy
*Allen Leech as Max

==Filming locations==
Although set in the Irish countryside, In Fear was filmed on and around Bodmin Moor in Cornwall.

==Reception==
Critical reception has been mostly positive and the movie currently holds a rating of 86% on Rotten Tomatoes (based upon 35 reviews) and 64 on Metacritic (based upon 10 reviews).   Much of the films praise centered around the camerawork,   and The Hollywood Reporter commented that "Loverings camera setups turn an already tight car into an increasingly claustrophobic setting".  Empire (magazine)|Empire gave a mixed review for In Fear, remarking that it had "atmosphere and enough proper scares to deliver on the promise of its title" but that it was also "contrived and nothing new plot-wise". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 