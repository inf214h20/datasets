Lovers in the Year One
 
{{Infobox film
| name           = Lovers in the Year One
| image          = 
| caption        = 
| director       = Jaroslav Balík
| producer       = 
| writer         = Jan Otčenášek Jaroslav Balík
| starring       = Marta Vančurová
| music          = 
| cinematography = Jan Čuřík
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Best Foreign Language Film at the 47th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Marta Vančurová as Helena Poláková
* Viktor Preiss as Pavel Krouza
* Libuše Švormová as Olga
* Petr Svojtka as Evžen
* Jana Švandová as Jana
* Ota Sklencka as Režisér
* Bedrich Prokos as Professor
* Jiří Kodet as Assistant
* Jan Teplý as Mladík
* Naďa Konvalinková as Jarmila
* Jitka Smutná as Zdena
* Jitka Zelenohorská as Jitka
* Zuzana Geislerová as Věra

==See also==
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 