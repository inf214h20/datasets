Gosford Park
 
 
 
{{Infobox film
| name           = Gosford Park
| image          = Gosford Park movie.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Robert Altman
| producer       = Robert Altman Bob Balaban David Levy
| writer         = Julian Fellowes
| story        = Robert Altman Bob Balaban
| starring       = Eileen Atkins Bob Balaban Alan Bates Charles Dance Stephen Fry Michael Gambon Richard E. Grant Derek Jacobi Kelly Macdonald Helen Mirren Jeremy Northam Clive Owen Ryan Phillippe Maggie Smith Kristin Scott Thomas Emily Watson
| music          = Patrick Doyle Andrew Dunn
| editing        = Tim Squyres
| studio         = Shepperton Studios Capitol Films
| distributor    = USA Films
| released       =  
| runtime        = 137 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $19.8 million   
| gross          = $87,754,044    
}} mystery film directed by Robert Altman and written by Julian Fellowes. The film stars an ensemble cast, which includes Helen Mirren, Maggie Smith, Michael Gambon, Derek Jacobi, Eileen Atkins, Alan Bates, Kristin Scott Thomas, Clive Owen, Emily Watson, Charles Dance, Tom Hollander and Laurence Fox. The story follows a party of wealthy Britons and an American, and their servants, who gather for a shooting weekend at Gosford Park, an English country house. A murder occurs after a dinner party, and the film goes on to present the subsequent investigation from the servants and guests perspectives.
 USA Films. It was released in February 2002 in the United Kingdom.

The film was successful at the box office, grossing over $87 million in cinemas worldwide, making it Altmans second most successful film after MASH (film)|MASH. It received multiple awards and nominations, including seven Academy Award nominations and nine British Academy Film Awards nominations.

The TV series Downton Abbey – written and created by Fellowes – was originally planned as a spin-off of Gosford Park, but instead was developed as a stand-alone property inspired by the film, set decades earlier. 

==Plot==
 
In November 1932, Constance, Countess of Trentham (Maggie Smith), and her ladys maid, Mary MacEachran (Kelly Macdonald) travel to Gosford Park for the weekend. On the way, they encounter actor Ivor Novello (Jeremy Northam), American film producer Morris Weissman (Bob Balaban) and Weissmans valet, Henry Denton (Ryan Phillippe). At the house, they are greeted by Lady Trenthams nephew Sir William McCordle (Michael Gambon), his wife Lady Sylvia McCordle (Kristin Scott Thomas) and their daughter, Isobel (Camilla Rutherford). The other guests include Lady Sylvias sisters, Louisa, Lady Stockbridge (Geraldine Somerville) and Lady Lavinia Meredith (Natasha Wightman) and their husbands, Raymond, Lord Stockbridge (Charles Dance) and Commander Anthony Meredith (Tom Hollander). Also in attendance are the Honourable Freddie Nesbitt (James Wilby) and his wife, Mabel (Claudie Blakley); Isobels suitor, Lord Rupert Standish (Laurence Fox) and his friend Jeremy Blond (Trent Ford).

Commander Meredith is in financial difficulty and brings up the matter with Sir William, who reveals that he is rescinding his investment in Merediths new business scheme. Sir William also reveals privately to Lady Sylvia that he may stop paying Lady Trenthams allowance. Mary and Lord Stockbridges valet, Parks (Clive Owen), are attracted to one another and exchange pleasantries. Denton asks a number of questions about life in service and Parks reveals that he was brought up in an orphanage. Denton meets Lady Sylvia and during the night, he goes to her room.

The next morning the men go out early on a pheasant shoot, and Sir William is slightly injured by a low shot. Later, the ladies join the gentlemen for an outdoor luncheon on the estate grounds, where Commander Meredith pleads with Sir William to not back out of the investment, breaking decorum by grabbing Sir Williams arm and causing him to shatter his cocktail glass on the ground.

While dressing for dinner, Lady Trentham and Mary are visited by Lady Sylvia, who reveals that Sir William is in a terrible mood with all of his guests after the events of the weekend and that he may stop paying his aunt her allowance. Lady Trentham is upset by this, and tersely tells Mary to be discreet about this unwelcome news.

Dinner that evening is tense and sombre, with the announcement that Commander Meredith will be leaving in the morning and that he now must prepare for bankruptcy thanks in part to Sir Williams withdrawal of his investment—news to which Sir William reacts with callous indifference. As the conversation progresses, tempers flare and Lady Sylvia attacks Sir William, implying that he was a First World War War profiteering|profiteer. The head housemaid, Elsie (Emily Watson), rises to his defence, breaking the class barrier, and thus revealing her affair with Sir William to everyone at the table. Everyone watches in shocked silence at this indiscretion, and Elsie hurries from the room—knowing that she will be dismissed. 

Sir William abruptly storms away from the dinner table and goes to the library, where the housekeeper, Mrs. Wilson (Helen Mirren) brings him coffee. He demands a glass of whisky instead. Lady Sylvia asks Mr. Novello to entertain the guests. George (Richard E. Grant, first footman), Parks, Mr. Nesbitt and Commander Meredith disappear and an unknown person goes to the library and stabs Sir William as he sits slumped in his chair. 

Minutes later, Lady Stockbridge goes to the library to entice Sir William to return to the party and her screams bring everyone to the room. Commander Meredith and Mr. Nesbitt do not offer an explanation of their disappearances, while George says he was fetching milk for the coffee service and Parks claims to have been fetching hot water bottles.

Inspector Thompson (Stephen Fry) and Constable Dexter (Ron Webster) arrive to investigate the murder. Dexter suggests that Sir William was already dead when he was stabbed. It is eventually surmised that Sir William was poisoned before being stabbed. Denton confesses to Jennings (Alan Bates), the butler, that he is not a valet but an American actor preparing for a film role. 

The next morning, Lady Sylvia goes for her usual morning ride, which surprises Inspector Thompson. Barnes (Adrian Scarborough) overhears Commander Meredith tell Lady Lavinia that Sir Williams death was lucky for them, as the investment is now secure. Barnes tells Inspector Thompson, who interrogates Meredith.

Mrs. Croft (Eileen Atkins) tells the kitchen maid, Bertha (Teresa Churcher), that Sir William was known for seducing the women working in his factories. If a woman became pregnant, Sir William offered two choices: keep the baby and lose your job, or give the baby up and keep your job. Those who gave up their babies were told that the adoptions were being arranged with good families. In reality, Sir William paid squalid orphanages to take the children. Mary goes to Parks room and tells him that she knows he is the murderer. Parks tells her that he discovered Sir William was his father, entered service and attempted to gain employment with someone in his circle. Parks tells Mary that he did not poison Sir William and Mary is relieved, as Parks only stabbed the corpse.

Mary listens to Lady Sylvia and Lady Constance discussing why Mrs. Croft and Mrs. Wilson are enemies. Lady Sylvia believes that the tension between them stems from the fact that Mrs. Wilson now outranks Mrs. Croft. Lady Constance asks if Mrs. Wilson was ever married and Lady Sylvia replies that her name was once Parks or Parker. Mary goes to Mrs. Wilson and the older woman reveals that she poisoned Sir William to protect her son, because she knew that Parks was there to kill Sir William. She also reveals that she and Mrs. Croft are sisters. After talking to Dorothy (Sophie Thompson), Mrs. Wilson goes to her room distraught and is comforted by Mrs. Croft. 

The guests drive away with the dismissed Elsie joining them, though she has stolen an unusual souvenir from the house...Sir Williams pet dog. Lady Sylvia waves good-bye to her guests and re-enters Gosford Park, while Jennings closes the doors.

==Cast==
* Maggie Smith as Constance Trentham
* Michael Gambon as William McCordle
* Kristin Scott Thomas as Sylvia McCordle
* Camilla Rutherford as Isobel McCordle
* Charles Dance as Lord Raymond Stockbridge
* Geraldine Somerville as Louisa Stockbridge
* Tom Hollander as Anthony Meredith
* Natasha Wightman as Lavinia Meredith
* Jeremy Northam as Ivor Novello
* Bob Balaban as Morris Weissman
* James Wilby as Freddie Nesbitt
* Claudie Blakley as Mabel Nesbitt
* Laurence Fox as Rupert Standish
* Trent Ford as Jeremy Blond
* Ryan Phillippe as Henry Denton
* Stephen Fry as Inspector Thompson
* Ron Webster as Constable Dexter
* Kelly Macdonald as Mary Maceachran
* Clive Owen as Robert Parks
* Helen Mirren as Mrs. Wilson
* Eileen Atkins as Mrs. Croft
* Emily Watson as Elsie
* Alan Bates as Jennings
* Derek Jacobi as Probert
* Richard E. Grant as George
* Jeremy Swift as Arthur
* Sophie Thompson as Dorothy
* Meg Wynn Owen as Lewis
* Adrian Scarborough as Barnes
* Frances Low as Sarah
* Joanna Maude as Renee
* Teresa Churcher as Bertha
* Sarah Flind as Ellen
* Finty Williams as Janet
* Emma Buckley as May
* Lucy Cohu as Lottie
* Laura Harling as Ethel
* Tilly Gerrard as Maud
* Will Beer as Albert
* Leo Bill as Jim
* Gregor Henderson-Begg as Fred
* John Atterbury as Merriman
* Frank Thornton as Mr. Burkett

==Themes== world wars, the impact of the First World War is also explored in the films screenplay.  It also mentions the decline of the British Empire and the peerage system. Writing for PopMatters, Cynthia Fuchs described surface appearances, rather than complex interpersonal relationships, as a theme of the film. 
 19th century novel.  Bob Balaban, an actor and producer for Gosford Park, says that the idea of creating a murder mystery told by the servants in the manor was an interesting one for him and Altman.   

Themes from the film were picked up and integrated into the series Downton Abbey by Julian Fellowes. Maggie Smith starred again in her role as a dowager countess, this time her title not being Trentham but Grantham.

==Production==

===Development and writing===
In 1999,   and Charlie Chan in London.  Altman chose British actor and writer Julian Fellowes to write the screenplay, because Fellowes knew how country houses operated.  Fellowes, who had never written a feature film before, received a telephone call from Altman, who asked him to come up with some characters and stories.     Fellowes was given a brief outline of the film: it was to be "set in a country house in the 30s and to have a murder in there somewhere, but for it to really be an examination of class".  Altman also wanted the film to explore the three groups of people: the family, the guests, and the servants.  Of the call, Fellowes said, "All the way through I thought this cant be happening -  a 50 year old fat balding actor is phoned up by an American movie director - but I did work as if it was going to happen". 

The original title of the film was The Other Side of the Tapestry, but Altman thought it was awkward. Fellowes began looking through some books and came up with Gosford Park.  Altman said: "Nobody liked it, everyone fought me on it. But when you make a picture using a name, thats its name. Its not a gripping title. But then MASH wasnt either." 

Fellowes says the screenplay was "not an homage to Agatha Christie, but a reworking of that genre". Fellowes was credited not only as the films writer but as a technical advisor, as well, meaning he wrote portions of the film as it was being produced. He notes that, when writing a large scene with many actors and characters, not everything the characters say during the scene is scripted; sometimes the actors are left to improvise other lines. 
 Sir Richard and Lady Kleinwort, to advise on correct procedures and arrangements on the set. Inch is credited as "Butler" immediately before Altman as Director in the final credits.

===Casting=== dames (Maggie Smith and Eileen Atkins). Three other members of the cast (Alan Bates, Helen Mirren and Kristin Scott Thomas) were later elevated to that status.

===Filming and editing===
 , where indoor scenes for Gosford Park were shot]] Wrotham Park for the exteriors, staircase, dining room and drawing room,  and Syon House for the upstairs bedrooms.  The opening sequence outside Lady Trenthams home was shot at Hall Barn, near Beaconsfield, Buckinghamshire, whose grounds were also used as the scene for lunch after the shoot. Sound stages were built to film the scenes of the manors downstairs area.    Shepperton Studios was used for off-location filming. 
 Andrew Dunn, Kodak Vision Editor Tim Squyres described the editing process on Gosford Park as an unusual one, as the dual cameras used were generally located in the same areas when filming, instead of the more standard method of setting up a scene directly.   

==Release== premiered on widely released USA Films.  It was released on 1 February 2002 in the United Kingdom. 

===Box office===
In its limited release opening weekend, the film grossed a mere $241,219, hitting No. 23 in the box office that weekend.  In its wide release, it grossed $3,395,759;  by the end of its run on 6 June 2002, Gosford Park grossed $41,308,615 in the domestic box office and a worldwide total of $87,754,044.  With that final total, Gosford Park became Altmans second most successful film at the box office after his 1970 film, MASH (film)|MASH. 

===Critical reception=== average rating of 7.5 out of 10.  According to the sites summary of the critical consensus, "A mixture of Upstairs, Downstairs (1971 TV series)|Upstairs, Downstairs; Clue (board game)|Clue; and perceptive social commentary, Gosford Park ranks among director Altmans best."  Metacritic, which assigns a score of 1–100 to individual film reviews, gives the film an averaged rating of 90 based on 34 reviews. 

 , an independent critic, gave Gosford Park an A minus rating. He described one of its themes as "illuminating a society and a way of life on the verge of extinction",    placing the interwar setting as an integral part of the films class study. However, he notes that because Altman is an independent observer of the society he portrays in the film, it does not have the biting qualities of his previous social commentaries such as Short Cuts, set in the directors home country of the United States.  Jonathan Rosenbaum in the Chicago Reader called it a "Masterpiece". 
 Michael Phillips placed Gosford Park at number nine on his list of Best Films of the Decade.  The film was placed at 82 on Slant Magazines list of best films of the 2000s. 

===Accolades===
  Academy Awards Best Picture Best Director, A Beautiful Best Original British Academy Best Film Best Costume Design (Jenny Beavan).  Mirren, Smith and Watson were all nominated for Best European Actress at the European Film Awards.  The film received five nominations at the 59th Golden Globe Awards; Altman won the Award for Best Director. 

At the 8th Screen Actors Guild Awards Mirren won Outstanding Performance by a Female Actor in a Supporting Role and the ensemble cast collectively won Outstanding Performance by a Cast in a Motion Picture.  The film won four more Best Cast awards from the Broadcast Film Critics Association, Florida Film Critics Circle, and Online Film Critics Society.    Fellowes received recognition for the films screenplay from the Writers Guild of America, where he won the Best Original Screenplay award.  The films score composer, Patrick Doyle received two nominations for his work. Doyle was nominated for Composer of the Year from the American Film Institute and he won the award for Soundtrack Composer of the Year from the World Soundtrack Awards.  

==Home media== region 1 compression artefacts, but describing an unfavourable darkness to scenes filmed within the manor house.    Both reviewers commented positively on the films score and soundtrack. Gonzalez wrote that "Gosford Park sounds amazing for a film so dialogue-dependent"  and Mack that "the audio transfer is about as good as it can get on a movie of this style". 

===Soundtrack===
 
Patrick Doyle composed the films score.     Doyle said that it can take him up to six months to create a film score, but Altman asked him to write and compose the music for Gosford Park in less than five weeks.  Doyle recorded the soundtrack at the London Air-Edel Recording Studios in October 2001.   The soundtrack also features six original songs by composer and playwright, Ivor Novello.    The actor who portrays Novello in the film, Jeremy Northam, sings all the songs and his brother, Christopher, accompanies him on the piano.  The soundtrack was released on 15 January 2002. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
{{succession box
| before = Almost Famous&nbsp;– Cameron Crowe
| after = Talk to Her&nbsp;– Pedro Almodóvar Academy Award for Best Original Screenplay
| years = 2001
}}
 

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 