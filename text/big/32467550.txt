Drifters (1929 film)
{{Infobox film| name           = Drifters
| image          = 
| image_size     = 
| caption        = 
| director       = John Grierson
| producer       = John Grierson
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = DOP Basil Emmott
| editing        = 
| distributor    = 
| released       = 1929
| runtime        = 61 minutes
| country        = United Kingdom
| language       = English
| budget         = £3,000
| preceded_by    = 
| followed_by    = 
}}
Drifters (1929) is silent documentary film by John Grierson, his first and only personal film. 

It tells the story of Britains North Sea herring fishery. The films style has been described as being a "response to avant-garde, Modernist films, adopting formal techniques such as montage – constructive editing emphasising the rhythmic juxtaposition of images – but also aimed to make a socially directed commentary on its subject"(Tate Gallery: Liverpool 2006). The film was successful both critically and commercially and helped kick off Griersons documentary film movement.     This film also showed that Grierson was not afraid to alter reality slightly in order to have his vision shown. For example when the boat he was on returned without a catch he bought another boats catch and tried to fake it. He ended up scrapping that film as it was not authentic enough. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 