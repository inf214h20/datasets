Finishing the Game
{{Infobox film
| image=FinishingtheGameposter.jpg
| name=Finishing the Game
| caption = Theatrical release poster
| director = Justin Lin
| writer = Josh Diamond
| producer = Julie Asato Salvador Gatdula Justin Lin Annabella Thorne
| cinematography = Tom Clancey
| released = 2007 Brian Tyler
| runtime = 88 minutes
| editing = Greg Louie
| distributor = IFC Films
| country = United States
| language = English 
}}
Finishing the Game is a 2007  .

Finishing the Game stars  , the 23rd VC FilmFest aka Los Angeles Asian Pacific Film Festival in Los Angeles, the 30th Asian American International Film Festival in New York, the DisOrient Film Festival of Oregon, the Asian Film Festival of Dallas, the 2007 DC Asian Pacific American Film Festival, and the 11th Annual Vancouver Asian Film Festival.

==Cast==
 .]]
 
*McCaleb Burnett as Tarrick Tyler
*Monique Curnen as Saraghina Rivas
*Roger Fan as Breeze Loo
*James Franco as Dean Silo
*Sung Kang as Cole Kim
*Mousa Kraish as Raja
*Dustin Nguyen as Troy Poon
*MC Hammer as Roy Thunder
*SuChin Pak as Connie Popavich-Mosimoto
*Bella Thorne as Sue
*Sam Bottoms as Martey Kurtainbaum
*Jake Sandvig as Ronney Kurtainbaum
*Michael Shamus Wiles as Police Officer Williams
*Nathan Jung as Bob
*Joseph McQueen as Leroy/Earl
*Ron Jeremy as Peter Dowd
*David Collard as Victor
*Meredith Scott Lynn as Eloise Gazdag
*Cassidy Freeman as Shirley
 

==Reception==
Reviews were mixed to negative. The film currently holds a "Rotten" rating of 35% on Rotten Tomatoes.

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 


 