Convicted Woman
{{Infobox Film name        = Convicted Woman image       = caption     =  director    = Nick Grinde producer    =  writer      = Martin Mooney Alex Gottlieb music       =  cinematography =  editing     =  starring    = Rochelle Hudson Frieda Inescort June Lang distributor = Columbia Pictures released    = January 31, 1940 (USA)  runtime     = 65 min. country     = United States language  English
|}}
Convicted Woman (1940) is a crime movie starring Rochelle Hudson and directed by Nick Grinde. It is also known as Dames and Daughters of Today.

==Cast==
{|class="wikitable"
|-
!Actor !! Role
|- Rochelle Hudson ||  Betty Andrews  
|- Frieda Inescort || Attorney Mary Ellis  
|- June Lang || Georgia Mason  
|- Lola Lane || Hazel Wren
|- Glenn Ford || Jim Brent (Reporter)   
|- Iris Meredith || Nita Lavore 
|- Lorna Gray || Frankie Mason 
|- Esther Dale || Chief Matron Brackett  
|- William Farnum || Commissioner McNeill 
|- Mary Field || Gracie Dunn 
|- Beatrice Blinn || May Sorenson  
|- June Gittelson || Tubby 
|- Dorothy Appleby || Daisy

|}

== References ==
 
*A guide to American crime films of the forties and fifties by Larry Langman, Daniel Finn

==External links==
* 

 

 
 
 
 
 