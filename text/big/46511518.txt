Ciboulette (film)
{{Infobox film
| name = Ciboulette 
| image =
| image_size =
| caption =
| director = Claude Autant-Lara
| producer = 
| writer =  Francis de Croisset (play)   Robert de Flers (play)   Jacques Prévert   Claude Autant-Lara
| narrator =
| starring = Simone Berriau   Robert Burnier   Armand Dranem
| music = Reynaldo Hahn     
| cinematography = Charles Bauer   Curt Courant  
| editing =     Henri Taverna   André Versein
| studio = Cipar Films   Pathé Consortium Cinéma 
| distributor = Pathé Consortium Cinéma
| released = 10 November 1933 
| runtime = 85 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} operetta of the same name. The films art direction was by Lazare Meerson and Alexandre Trauner. It was part of a popular cycle of operetta films during the decade.

==Cast==
*    Simone Berriau as Ciboulette 
* Robert Burnier as Antonin 
* Armand Dranem as Le père Grenu  
* André Urban as Monsieur Duparquet 
* Madeleine Guitty as La mère Pingret  
* Pomiès as Olivier Métra  
* Thérèse Dorny  as Zénobie  
* Guy Ferrant as Roger de Lansquenet  
* Marcel Duhamel as Le voleur  
* Jacques Prévert as LÂne  
* Ginette Leclerc as Une cocotte  
* Viviane Romance as Une cocotte  
* Monique Joyce as Une cocotte  
* Christiane Dor as La servante  
* Marie-Jacqueline Chantal as Une invitée chez Métra  
* Charles Camus as Grisart  
* Louis Florencie as Trancher  
* Pedro Elviro as Arthur et Meyer
* Lucien Raimbourg as Victor  
* Raymond Bussières as Un clochard 
* Eugène Stuber as Un fort des halles et un faune 
* Pépa Cara  
* Robert Casa  
* Andrée Doria 
* Gazelle
* Jean Lods
* Max Morise  
* Léon Moussinac 
* Pierre Sabas

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 
 
 
 
 
 
 

 