Village of Daughters
{{Infobox film
| name           = Village of Daughters
| caption        = 
| image	=	Village of Daughters FilmPoster.jpeg George Pollock     
| producer       = George H. Brown
| writer         = David Pursall
| starring       = Eric Sykes Scilla Gabel John Le Mesurier Grégoire Aslan Graham Stark Warren Mitchell
| music          = Ron Goodwin
| cinematography = Geoffrey Faithfull
| editing        = Tristam Cones
| distributor    = Metro-Goldwyn-Mayer|Metro-Goldwyn-Mayer (MGM)
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 1962 Cinema British comedy film directed by George Pollock.

==Plot==
Herbert Harris is a traveling salesman who makes his way into a remote Italian village to sell his wares. There, he finds many single and attractive women who all pursue him madly. He quickly learns that the village has all but one single man left, as the majority have left to find work. The only single man left is Antonio, a wealthy foreigner who is looking for a bride. Due to the large selection of women, a rule was created prior to the arrival of Harris, that the first outsider to enter the village would be the one to make the ultimate choice of the mans bride.

==Cast==
*Eric Sykes.....Herbert Harris
*Scilla Gabel.....Angelina Vimercati
*John Le Mesurier.....Don Calogere
*Grégoire Aslan.....Gastoni
*Graham Stark.....Postman
*Warren Mitchell.....Puccelli
*Yvonne Romain.....Annunziata
*Eric Pohlmann.....Marcio
*Ina De La Haye.....Maria Gastoni
*Peter Illing.....Alfredo Predati
*Jill Carson.....Lucia Puccelli
*Monte Landis......Faccino
*Talitha Pol.....Gioia Spartaco
*Edwin Richfield.....Balbino
*Mario Fabrizi.....Antonio Durigo

==Reception==
According to MGM records the film made a loss of $268,000.  . 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 