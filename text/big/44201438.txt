The White Black Sheep
{{infobox film name = The White Black Sheep director = Sidney Olcott producer = Inspiration Pictures writer = Jerome N. Wilson   Agnes Pact McKenna based on = Violet E. Powell (story) starring = Richard Barthelmess cinematographer = David W. Gobbett editing = Tom Miranda distributor = First national released =  
| runtime        = 8 reels
| country        = United States
| language       = Silent (English intertitles)
}}
The White Black Sheep is a 1926 American silent film produced by Inspiration Pictures and distributed by First National. it was directed by Sidney Olcott with Richard Barthelmess and Patsy Ruth Miller in the leading roles.

==Cast==
*Richard Barthelmess - Robert Kincairn
*Patsy Ruth Miller - Zelie
*Constance Howard - Enid Gower
*Erville Alderson - Yasuf
*William H. Tooker - Colonel Kincairn
*Gino Corrado - El Rahib
*Albert Prisco - Kadir
*Sam Appel - Dimos
*Col G.L. McDonell - Colonel Nicholson
*Templar Saxe - Stanley Fielding

==Production notes==
The White Black Sheep was shot in First National studios in Burbank CA.

==External links==
* 
    website dedicated to Sidney Olcott

 
 
 
 
 
 
 


 
 