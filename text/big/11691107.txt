Bristol Boys
{{Infobox film
| name     = Bristol Boys
| director = Brandon David
| writer   = Brandon David
| starring = Thomas Guiry Will Janowitz Tammy Trull Max Casella
| runtime  = 90 minutes
| language = English USD 
| country  = United States
| released =  
}}
Bristol Boys is a 2006 film written and directed by Brandon David. Shot in Bristol, Connecticut and Springfield, Massachusetts, the film is based on one of biggest drug busts of Connecticuts Statewide Narcotics Task Force, including the arrest of Davids longtime friend Kevin Toolen in 2001. {{cite web
  | title      = bristolpress.com
  | work       = Amy V. Talit: Bristol Boys uncovered
  | url        = http://www.bristolpress.com/site/news.cfm?newsid=18244572&BRD=1643&PAG=461&dept_id=10486&rfi=6
  | accessdate = 9 June 2007
  }}  Will Janowitz and Max Casella of The Sopranos fame star in the film, as do Dean Winters and David Zayas from Oz (TV series)|Oz.

== Plot ==
The story revolves around Michael "Little Man" McCarthy and the rise and fall of marijuana dealers from Bristol, Connecticut. "The film is about a group of working-class guys trying to get ahead by selling drugs," said David, "and the value system they try to live by." {{cite web
  | title      = newbritainherald.com
  | work       = Bristol Boys crime drama debuts today
  | url        = http://www.newbritainherald.com/site/index.cfm?newsid=18161254&BRD=1641&PAG=461&dept_id=594835&rfi=8
  | accessdate = 9 June 2007
  }} 

=== Inspiration ===
Director/writer Brandon David based the movie on the events leading up to and following a months-long investigation by the Statewide Narcotics Task Force which resulted in the arrest of 21 people from Bristol, Plymouth, Southington, and Thomaston. {{cite web
  | title      = courant.com
  | work       = Susan Dunne: Crime movie gives Bristol a black eye
  | url        = http://www.courant.com/features/lifestyle/hc-bristolboys.artapr03,0,2890942.story?coll=hc-headlines-life
  | accessdate = 9 June 2007
  }}  Davids friends Kevin Toolen and Miguel Rivera were arrested in the bust. David says he hoped to write on the experiences  of his friends and himself while transporting marijuana with Toolen cross country. {{cite web
  | title      = myspace.com
  | work       = Bristol Boys blog
  | url        = http://www.myspace.com/bristolboys
  | accessdate = 9 June 2007
  }} 
 DEA informant," David said, which was another motivation to make the movie.  David did take creative license on some realities, such as making "Little Mans" mother addicted to OxyContin and dying of an overdose. Toolens mother died in 2000 but had been suffering from leukemia.  Also, the police informants death in the movie is fiction. "There was no murder, but if I were the guy that ratted everybody out, Id be concerned," said David.

David denies the movie is an attack on Bristol. "The biggest message I want to get out is I didnt make a movie about Bristol. I made a movie about something that happened to people I know," he said.  However, he openly criticizes Bristol, calling it an "industrial wasteland."  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 