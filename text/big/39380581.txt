Fedz
 
{{Infobox film
| name           = Fedz
| image	         = Fedz.jpg
| image size     = 
| caption        = Promotional poster
| director       = Q
| producer       = Candice Vetter Yousaf Uddin
| writer         = Q
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Q Joseph Marcell Wil Johnson
| music          = Alexander Charles Elliot Dale Sumner Duncan Bridgeman
| cinematography = Sam Brown Ciro Candia James Martin Peter Emery Martyna Knitter Boyd Skinner
| editing        = Andrew Trotman Robert Hall Kant Pan Peter Hollywood
| studio         = Top Dog Productions
| distributor    = Top Dog
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British crime crime thriller film directed by, written by and starring Q. The film is about a renegade policeman attempting to investigate a terrorist group intending to release an airborne virus in London.

==Plot== Gary McDonald) Richie Campbel) and Tyson (Ashley Chin) to kill Mike.

Mike goes on the run to solve the virus case and obtain the virus antibodies. Whilst Mike is being pursued by his old colleagues, he obtains diaries about drugs the virus has been planted in from sports-coach, Coach McKenzie (Martina Laird). After she is murdered, Mike gives the diaries to a journalist, Trevor McBride (Wil Johnson), who is then kidnapped for ransom money in exchange for the antibodies, tortured and murdered by Razor (Andrew Harrison). After Mike tells Pete that he is a policeman, Pete orders Kent (Leon Herbert) to kill Mike.

For help Mike visits Shazz (Maya Sondhi), an ex-scientist who is married to his ex-colleague, Ritchie (David Keyes). Ritchie sends Mike away to Jack Huey (Dermot Keaney) in Brighton to be tortured by Razor (Andrew Harrison). Mike escapes, and kills Jack and Razor. Mike gives Shazz evidence incriminating Ritchie for her to pass onto Brighton police. Ritchie kidnaps Shazz and holds her hostage for ransom money. Mike enlists the help of a swat team, who help him kill Ritchie’s men in a warehouse. Ritchie is then killed by Mike’s former superior Whittaker (Justine Powell).

Mike declines Whittaker’s offer for his old job. Barry and Tyson are killed by Ty for doing a drug deal on the side, Mike then kills Ty and warns Ty’s driver that if Fast Eddie comes back then he will kill him and Fast Eddie, and then employs him as an informant.

Mike plans a holiday to Hawaii and goes back to his flat where he finds Slick Pete and his men, they all point a loaded guns at Mike. The film ends as a gunshot is fired.

==Cast==
 
*Q as Mike Jones
*David Keyes as Ritchie
*Dexter Fletcher as DS Hunter
*Joseph Marcell as Fast Eddie
*Wil Johnson as Trevor McBride
*Ashley Walters as Cherokee Blame
*Isabella Calthorpe as Detective Carter
*Shanika Warren-Markland as Ty
*Maya Sondhi as Shazz
*Martina Laird as Coach McKenzie
*Femi Oyeniran as Detective Harper
*Bradley Gardner as Slick Pete
*Justine Powell as DS Whitaker
*Dermot Keaney as Jack Huey
*Andrew Harrison as Razor
*Katia Winter as Alessandra Ragnfrid Gary McDonald as Rizzle Richie Campbell as Barry
*Ashley Chin as Tyson
*Leon Herbert as Kent
 

==Production and release==
 
Fedz was independently produced.    In Summer 2009, a short of the film premiered at a Hollywood film festival. At the time the film was titled Fever, and was used as a test screening on American audiences.      

The film was not presented to distributors and due to the demise of distributors; HMV, Blockbuster LLC|Blockbuster, and Revolver Entertainment, the producers used various online videos to promote a cinema release.         
 Shazam and SoundHound applications.      

On 12 April 2013, a live hangout event was streamed via Flavour Magazine on Google, which featured musicians from the film’s soundtrack and guests. In 2014, regional cinema tour is being planned due to the demand from the video on demand sales.   

On 5 November 2013, the film premiered at Genesis Cinema in Whitechapel, London, which also included live music, featured personal appearances from musicians from the film’s soundtrack and guests, and  was followed by an after-party.             

==Awards and nominations==
{| class="wikitable sortable"
! Year
! Award
! Category
! Recipient(s)
! Result
|-
| rowspan="1"| 2007
| Accolade Competition
| Award of Excellence
| Q
|  
|-
| rowspan="1"| 2013
| Official Mixtape Awards   
| Best Mixtape Compilation
| DJ Ames   
|  
|-
|}

==See also==
*Deadmeat

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 