Lieberman in Love
{{Infobox film
| name           = Lieberman in Love
| image          = 
| caption        = 
| director       = Christine Lahti
| producer       = Thom Colwell Leslie Hoffman Jana Sue Memel Hillary Anne Ripps Cynthia Sherman
| writer         = W. P. Kinsella Polly Platt
| starring       = Danny Aiello Christine Lahti
| music          = 
| cinematography = Marc Reshovsky
| editing        = Lisa Bromwell
| distributor    = 
| released       =  
| runtime        = 39 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 1996 for Best Short Subject.   

A short story by W. P. Kinsella, "Lieberman in Love", was the basis for the film. The Oscar win came as a surprise to the author, who, watching the award telecast from home, had no idea the film had been made and released. He had not been listed in the films credits, and was not acknowledged by director Christine Lahti in her acceptance speech. A full-page advertisement was placed in Variety apologizing to Kinsella for the error. 

==Plot==
At a resort, Joe Lieberman is attracted to a guest named Shaleen, who turns out to be a prostitute. They begin a professional relationship, which continues even after Joe develops a romantic interest in a woman who sells him a condo, Kate, who is married.

==Cast==
* Danny Aiello - Joe Lieberman
* Christine Lahti - Shaleen
* Nancy Travis - Kate
* Allan Arbus - Elderly Man
* Lisa Banes - Woman
* Beth Grant - Linda Baker
* David Rasche - M.C. at Luau
* Nick Toth - Mike Baker

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 