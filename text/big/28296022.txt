Krishnan Love Story
{{Infobox film
| name           = Krishnan Love Story
| image          = Krishnan-love-story poster.jpg
| alt            =  
| caption        = Theatrical poster Shashank
| producer       = Uday K. Mehta Mohan G. Nayak
| writer         = Shashank
| screenplay     = Shashank Sharan Pradeep Bogadi Harsha Chandra
| music          = Sridhar V. Sambhram
| cinematography = Shekar Chandra
| editing        = K. M. Prakash
| studio         = Sri Venkateshwara Krupa Enterprises
| distributor    = 
| released       = 18 June 2010
| runtime        = 147 minutes
| country        = India Kannada
| budget         = 15 Crores
}}
 action drama film written and directed by Shashank (director)|Shashank, and starring Ajay Rao, Radhika Pandit, Umashri, Achuth Kumar, Sharan, Pradeep, Harsha, Chandra. The music of this film is composed by Sridhar V. Sambhram.

==Plot==
Its saga of a girl born in a poor family who is forced to suppress her ambitions for want of money. Her romantic life makes her face some embarrassing situations. The story revolves around Geetha, (Radhika Pandit), a middle-class girl, who suppresses her ambitions due to poverty. She has to fear the society and also her drunkard brother (Chandru) who always mistreats her. Though she likes Krishna (Ajay Rao), she elopes with Narendra (Pradeep), son of a rich man, for money. A ghastly incident makes her lead a miserable life unable to face society, but her lover Krishna comes to her help. The story takes many exciting turns with a brilliant climax. Totally, it is full feeling maga story. The music of the film was super-hit because of tracks like "Hrudayave Bayaside Ninnane" sung by Sonu Nigam and "Neenaada Maathu" by Rajesh Krishnan.

==Cast==
* Ajay Rao
* Radhika Pandit
* Umashree
* Achyuth Kumar Sharan
* Pradeep Bogadi
* Harsha
* Chandra
* Padmaja Rao
* Ninasam Ashwath

==Soundtrack==
{{Infobox album
| Name        = Krishnan Love Story
| Type        = Soundtrack
| Artist      = Sridhar V. Sambhram
| Cover       = Kannada film Krishnan Love Story album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 11 June 2010
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Anand Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Sridhar V. Sambhram composed the films background score and music for its soundtrack. The lyrics for the soundtrack was written by V. Sridhar, Yogaraj Bhat, Jayant Kaikini and Shashank (director)|Shashank. The album consists of eight tracks. {{cite web |title= iTunes - Music - Krishnan Love Story (Original Motion Picture Soundtrack) |url= https://itunes.apple.com/ie/album/krishnan-love-story.../id518059285
 |publisher= iTunes |accessdate= 12 December 2014}} 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Galli Cricket
| lyrics1 = V. Sridhar
| extra1 = Desmond Louis, V. Sridhar
| length1 = 
| title2 = Thumba Aaramagi
| lyrics2 = Yogaraj Bhat
| extra2 = Harsha Sadananda, Anoop Seelin, V. Sridhar
| length2 = 
| title3 = Hrudayave Bayaside Ninnane
| lyrics3 = Jayant Kaikini
| extra3 = Sonu Nigam, Vidyashree
| length3 = 
| title4 = Mosa Madalende Neenu Shashank
| extra4 = Kailash Kher, Chetan Sosca
| length4 = 
| title5 = Mosa Madalende Neenu (Bit)
| lyrics5 = Shashank
| extra5 = Kailash Kher
| length5 = 
| title6 = Ondu Sanna Aasege
| lyrics6 = Shashank
| extra6 = Deepak Doddera
| length6 = 
| title7 = Nee Adada Maathu
| lyrics7 = Jayant Kaikini
| extra7 = Anuradha Bhat
| length7 = 
| title8 = Nee Adada Maathu
| lyrics8 = Jayant Kaikini
| extra8 = Rajesh Krishnan
| length8 = 
}}

==Box office==
The film opened to a good response and ran successfully across Karnataka. It has already completed 100 days. This film has been declared as a hit and one of the very few films which didnt tank at the box office.

==Awards== Filmfare Awards
;;Won Best Actress - Radhika Pandit Best Supporting Actress - Umashree
;;Nominated Best Director Shashank
*Filmfare Best Music Director - V. Sridhar Best Male Playback Singer - Sonu Nigam - "Hrudayave" Best Lyricist - Jayanth Kaikini - "Hrudayave"

==References==
 

==External links==
*   at oneindia.in
*  

 

 
 
 
 