Is Everybody Happy? (1929 film)
{{Infobox film
| name           = Is Everybody Happy?
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Archie Mayo
| producer       = Joseph Jackson James A. Starr
| narrator       = Ted Lewis Ann Pennington Ted Lewis Grant Clarke
| cinematography = Ben Reynolds
| editing        = Desmond OBrien
| studio         = Warner Brothers 
| distributor    = Warner Brothers
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Ted Lewis, Ann Pennington, and Julia Swayne Gordon, directed by Archie Mayo, and released by Warner Brothers. The music for the film was written by Harry Akst and Grant Clarke, except for "St. Louis Blues (song)|St. Louis Blues" by W. C. Handy and "Tiger Rag". The films title takes its name from Lewiss famous catchphrase "Is everybody happy?"

The films soundtrack exists on Vitaphone discs preserved at the UCLA Film and Television Archive, but the film itself is considered a lost film, per the Vitaphone Project website.
 Is Everybody Happy? (1943).

== Cast == Ted Lewis as Ted Todd
*Alice Day as Gail Wilson Ann Pennington as Lena Schmitt
*Lawrence Grant as Victor Molnár
*Julia Swayne Gordon as  Mrs. Molnár
*Otto Hoffman as Landlord
*Purnell Pratt as Stage Manager
*Muggsy Spanier as Himself

==Soundtrack==
*"Wouldnt It Be Wonderful?" - written by Harry Akst, Grant Clarke
*"Im the Medicine Man For the Blues" - written by Harry Akst, Grant Clarke
*"Samoa" - written by Harry Akst, Grant Clarke
*"New Orleans" - written by Harry Akst, Grant Clarke
*"In the Land of Jazz" - written by Harry Akst, Grant Clarke
*"Start the Band" - written by Harry Akst, Grant Clarke
*"St. Louis Blues" - written by W. C. Handy
*"Tiger Rag" - music by Henry Ragas (as H. W. Ragas), Nick LaRocca (as D. J. La Rocca), Larry Shields (as L. Shields), Tony Sbarbaro (as A. Sbarbaro) and Edwin B. Edwards (as E. B. Edwards); lyrics by Harry DeCosta (as Harry Da Costa)

==References==
 

== External links ==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 