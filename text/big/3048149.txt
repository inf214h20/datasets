The Boat on the Grass
 
{{Infobox film
| name           = The Boat on the Grass
| image          = 
| image_size     = 
| caption        = 
| director       = Gérard Brach
| producer       = Jean-Pierre Rassam
| writer         = Gérard Brach Roman Polanski Suzanne Schiffman
| narrator       = 
| starring       = Claude Jade Jean-Pierre Cassel John McEnery Valentina Cortese
| music          = François Rabbath
| cinematography = Étienne Becker
| editing        = Claude Barrois
| distributor    = Valoria Films
| released       = April 16, 1971
| runtime        = 90 mins
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French film directed by Gérard Brach. It was entered into the 1971 Cannes Film Festival.   

==Cast==
* Claude Jade - Eleonore
* Jean-Pierre Cassel - David
* John McEnery - Oliver
* Valentina Cortese - Christine
* Paul Préboist - Leon
* Micha Bayard - Germaine
* Pierre Asso - Alexis
* Jean de Coninck - Jean-Claude
* Bernard Salvage - Le parasite
* Carla Marlier - La parasite
* Louis Jojot

==Plot summary==

In this gentle, tragic drama, Olivier (John McEnery) is a wealthy young man. He spends his time building a boat on the lawn with his friend David (Jean-Pierre Cassel), a poor fisherman whom he grew up with. Though hardly idyllic, the relative calm provided by their friendship is disrupted by Eleonore (Claude Jade), a cute and determined young woman who sets her sights on David. She wants to wean David from his friendship with Olivier and plays on Davids long-dormant jealousy of Oliviers wealth and easy life. Eleonore also plays the flipside of the jealousy issue, claiming that Olivier has made passes at her.

 , who brings the right mixture of conventionalism and self-interest into her role."

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 

 