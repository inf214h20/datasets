The Cycle (1975 film)
 
{{Infobox film
| name           = The Cycle
| image          = 
| alt            = 
| caption        = 
| director       = Dariush Mehrjui
| producer       = 
| writer         = Dariush Mehrjui
| story          = Gholam-Hossein Saedi
| based on       = The play "Aashghaal-duni" by Gholam-Hossein Saedi
| starring       = Saeed Kangarani Ezzatollah Entezami Ali Nassirian Esmail Mohammadi Fourouzan Bahman Fersi
| music          = Hormoz Farhat
| cinematography = Houshang Baharlou
| editing        = Talat Mirfendereski
| studio         = 
| distributor    = 
| released       = 1975 (1978 in Iran)
| runtime        = 101 minutes
| country        = Iran
| language       = Persian
| budget         =  rials
}}
 submission for Best Foreign Language Film at the 50th Academy Awards,  the first year that Iran participated in the award. The film was banned for three years before being given permission to release, and was finally released in Iran on April 12, 1978.

The films plot centered on "the illegal trade in blood donations against the backdrop poverty and life in shanty-towns".  A young man, Ali (Saeed Kangarani), takes his sick father (Esmail Mohammadi) to a hospital in Tehran. When his father is unable to be admitted, they wait outside the hospital and meet Sameri (Ezzatollah Entezami). Dr. Sameri offers them enough money for Alis fathers treatment if they assist him with some work. They meet him the next morning at a crossroads and get in a truck with some other people, unaware of what they will be doing or where they will be going. They arrive at a laboratory, where Alis father is asked to give blood. He refuses, but Ali agrees to have his blood taken, and for that he is given 20 Iranian toman|tomans. Sameri deals in blood - He buys the blood cheaply off of the poor and addicted, and sells it on to the hospital.

During Alis dealings with the hospital, he meets Esmail (Ali Nassirian) and a young nurse, Zahra (Fourouzan). Ali begins working under Sameri, collecting donors for blood. Alis fathers health gradually worsens, until one day he dies. Ali dives deeper and deeper into blood dealing, and his new life begins.

==Cast==
* Saeed Kangarani as Ali
* Ezzatolah Entezami as Dr. Sameri
* Fourouzan as Zahra
* Bahman Fersi as Doctor Davoudzadeh
* Esmail Mohammadi as Alis Father
* Ali Nassirian as Esmail
* Rafi Halati

==See also==
* List of submissions to the 50th Academy Awards for Best Foreign Language Film
* List of Iranian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 


 