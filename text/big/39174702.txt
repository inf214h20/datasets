Blind Detective
{{Infobox film
| name           = Blind Detective
| image          = BlindDetective.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 盲探
 | simplified     = 盲探
 | pinyin         = Máng Tàn
 | jyutping       = Mang4 Taam3
 | poj            = }}
| director       = Johnnie To
| producer       = Johnnie To Wai Ka-Fai 
| writer         = 
| screenplay     = {{plainlist|
*Wai Ka-Fai
*Yau Nai-Hoi
*Ryker Chan
*Yu Xi
}}
| starring       = {{plainlist|
*Andy Lau
*Sammi Cheng}}
| music          =  Hal Foxton Beckett
| cinematography = Cheng Siu-Keung 
| editing        =  Allen Leung 
| studio         = {{plainlist| Media Asia Film Production Emperor Film Production
*Sil-Metropole Organisation|Sil-Metropole Organization
*Milkyway Image
*Beijing Rosat Film and TV Production
*Media Asia Film Distribution (Beijing) }}
| distributor    = Media Asia Distributions
| released       =  
| runtime        = 129 minutes
| country        = Hong Kong China   
| language       = Cantonese
| budget         = HK$85,000,000 (US$10.9 Million) 
| gross          = US$36,417,038 
}}
Blind Detective is a 2013 Hong Kong-Chinese romantic comedy and crime thriller film directed by Johnnie To and starring Andy Lau and Sammi Cheng. 

The film was shown as part of the Shanghai International Film Festival. 

==Plot==
Forced to leave service after turning blind, former detective Johnston Chong See Tun (Andy Lau) ekes out his living by solving cold cases for police rewards. During a case involving the search for the culprit who throws acid off roofs, he meets an attractive hit team inspector Goldie Ho Ka Tung. (Sammi Cheng). When Ho notices Johnstons impressive investigative mind despite lack of vision, she enlists his help in a personal case she is unable to solve on her own. The two work together to solve the case as well as other cold cases.

==Cast==
*Andy Lau as Johnston Chong See Tun (莊士敦), blind detective, a former inspector of the Regional Crime Unit known as "The God of Cracking Cases"
*Sammi Cheng as Goldie Ho Ka-tung (何家彤), a female police inspector Guo Tao as Szeto Fat-bo
*Gao Yuanyuan as Tingting
*Zi Yi
*Lang Yueting
*Lo Hoi-pang
*Bonnie Wong
*Lam Suet
*Philip Keung
*Mimi Zhu
*Stephanie Che

 
 
 

==Production==
Filming of Blind Detective began in the second half of 2011 in Hong Kong. In June 2012, filming halted after Sammi Cheng was diagnosed with Ménière’s disease before resuming filming in August 2012.   The film held a worship ceremony on 3 September 2012.  The music in the film was provided by Canadian television music composer Hal Foxton Beckett. 

==Release==
The film was selected to play as part of the Midnight selection at the 2013 Cannes Film Festival.    while it was be theatrically released on 4 July 2013 in Hong Kong and China. 

==Reception== The Mission Drug Wars." 

Andrew Chan of the Film Critics Circle of Australia writes, "As a love story, it works, but the film falls flat on the ground with a padded up detective story that will take more than a gallon of gold to convince. "   

==Awards and nominations==
*2013 Sitges Film Festival
**Won: Best Actor (Andy Lau)  
*2013 50th Golden Horse Film Awards
**Nominated: Best Actress (Sammi Cheng)
*2014 33rd Hong Kong Film Awards
**Nominated: Best Screenplay
**Nominated: Best Actress (Sammi Cheng)
**Nominated: Best Original Film Song (Andy Lau & Sammi Cheng)
*2014 21st Hong Kong Film Critics Society Awards
**Nominated: Best Actress (Sammi Cheng)

==See also==
*Andy Lau filmography
*Johnnie To filmography

==References==
 

==External links==
*  

 
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 