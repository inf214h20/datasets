American Perfekt
{{Infobox Film 
| name = American Perfekt
| image = Americanperfekt.jpg
| caption = Original Theatrical Poster
| director = Paul Chart
| writer = Paul Chart
| starring = Fairuza Balk Robert Forster Amanda Plummer Paul Sorvino David Thewlis
| producer = Irvin Kershner
| music = 
| cinematography= William Wages
| distributor = British Broadcasting Corporation
| released = 11 June 1997
| runtime = 99 minutes
| country = United States
| language = English
}}

American Perfekt is a 1997 road movie/thriller (genre)|thriller/drama film written and directed by Paul Chart, produced by Irvin Kershner. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

==Plot==
Jake Nyman decides to take a professional vacation where all decisions will be made by the flip of a coin. He meets up with disenchanted Sandra Thomas, who becomes excited by the potential of having the coin make all the decisions. See, Flipism. Things seem okay, until Sandra vanishes and Alice becomes involved in Jakes life.

==Main cast==
* Fairuza Balk, as Alice Thomas 
* Robert Forster, as Jake Nyman 
* Amanda Plummer, as Sandra Thomas 
* Paul Sorvino, as Sheriff Frank Noonan 
* David Thewlis, as Santini 
* Chris Sarandon, as Deputy Sammy  Geoffrey Lewis, as Willy 
* Jay Patterson, as Bertnie 
* Judson Mills, as Junior 
* Rutanya Alda, as Gloria
* Belinda Balaski, as Rita

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 