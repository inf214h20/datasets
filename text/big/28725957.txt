Stamboul (film)
Stamboul British drama film directed by Dimitri Buchowetzki and starring Warwick Ward, Rosita Moreno, Margot Grahame, and Garry Marsh, and released by the British division of Paramount Pictures. Buchowetski also co-directed El hombre que asesino with Fernando Gomis, the Spanish-language version of the film, also released by Paramount.  

The film is based on the novel Lhomme que assasina (1906) by Claude Farrere and on a play by Pierre Frondaie. 

==Plot==
In the lead-up to the First World War, a French military attaché falls in love with the wife of a prominent German in Stamboul in the Ottoman Empire. 

==Cast==
* Warwick Ward ...  Col André de Sevigne
* Rosita Moreno ...  Baroness von Strick
* Margot Grahame ...  Countess Elsa Talven
* Henry Hewitt ...  Baron von Strick
* Garry Marsh ...  Prince Cernuwitz
* Alan Napier ...  Bouchier
* Abraham Sofaer ...  Mahmed Pasha
* Stella Arbenina ...  Mme. Bouchier
* Annie Esmond ...  Nurse
* Eric Pavitt ...  Franz

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 