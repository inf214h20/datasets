Venus of the South Seas
{{Infobox film
| title          = Venus of the South Seas
| image          =
| caption        =
| director       = James R. Sullivan
| producer       =
| writer         =
| starring       = Annette Kellerman
| cinematography =
| editing        =
| studio         = Lee-Bradford Company
| distributor    = Davis Distributing Co.
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} Prizma Color process.

The 55-minute four-reel film, made by an American company and shot in Nelson, New Zealand. The film, with the final reel in Prizmacolor, was restored by the Library of Congress in 2004.  

==Plot ==
The daughter of a man who owns a South Seas pearl business falls in love with a wealthy traveler. Her father dies, leaving her the business, but a greedy ship captain schemes to take the business from her.

== Cast==
*Annette Kellerman as Shona Royal
*Roland Purdu as John Royal
*Norman French as Captain John Drake 
*Robert Ramsey as Robert Quayle Jnr

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p33 (1997, Oxford University Press, Auckland) ISBN 019 558336 1  
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 
 