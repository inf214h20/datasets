Doomed Caravan
 
{{Infobox film
| name           =Doomed Caravan
| image          =
| image_size     =
| caption        =
| director       = Lesley Selander
| producer       = Harry Sherman Joseph W. Engel
| writer         = Clarence E. Mulford (story) J. Benton Cheney (screenplay) Johnston McCulley (screenplay)
| narrator       = William Boyd
| music          = John Leipold
| cinematography = Russell Harlan
| editor       = Carroll Lewis Sherman A. Rose
| distributor    = Paramount Pictures
| released       = January 10, 1941
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
}} American Western Western film William Boyd.  The film is a serial Western and part of the Hopalong Cassidy series.  It is the 32nd entry in a series of 66 films.

==Cast== William Boyd as Hopalong Cassidy
* Andy Clyde as California
* Russell Hayden as Lucky Jenkins
* Minna Gombell as Jane Travers
* Morris Ankrum as Stephen Westcott
* Georgia Ellis as Diana Westcott
* Trevor Bardette as Ed Martin
* Pat J. OBrien as Henchman Jim Ferber
* Ray Bennett as Henchman Pete Gregg
* José Luis Tortosa as Governor Don Pedro

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 

 