Vincent and Me
{{Infobox film
| name           = Vincent and Me
| image          = VincentAndMe.jpg Alexandre Vernon Dobtcheff Anna-Maria Giannotti Andrée Pelletier Jeanne Calment Matthew Mabe Tchéky Karyo
| music          = Pierick Houdy
| cinematography = Andreas Poulsson
| writer         = Michael Rubbo
| producer       = Rock Demers Claude Nedjar
| director       = Michael Rubbo
| distributor    =
| released       =  
| country        = Canada
| runtime        = 101 mins French
| awards         =
| budget         =
}}
 Tales for All (Contes Pour Tous) series of childrens movies created by Les Productions la Fête.

At the age of 114, Jeanne Calment appeared briefly in the film as herself, making her the oldest film actress ever.  Calment claimed to have met Vincent van Gogh ca. 1888 when she was 12 or 13.   

==Plot==
Jo loves to draw, and she is good enough at it to win a scholarship. She goes to the city from her small town to study at a special art school, where more than anything else, she hopes to learn to paint like her hero, Vincent van Gogh. While sketching faces one day, she encounters a mysterious European art dealer who buys a few of her drawings, and commissions her to do some more. He rewards her handsomely for her work, and goes back to Amsterdam. Not long after, Jo is shown a magazine story about the "discovery" and million dollar sale of some of the drawings of young Vincent van Gogh, drawings only she and her friend, Felix, know are hers. The only thing to do is for Jo and friends to get to Amsterdam and find the mystery man. Or better still, go right to the source and speak to Vincent himself in 19th century Arles.

==Cast==
*Nina Petronzio as Jo
*Christopher Forrest as Felix Murphy
*Paul Klerk as Joris Alexandre Vernon Dobtcheff as Dr. Winkler
*Anna-Maria Giannotti as Grain
*Andrée Pelletier as Mrs. Wallis
*Matthew Mabe as Tom Mainfield
*Tchéky Karyo as Vincent van Gogh
*Jeanne Calment as herself
*Kiki Classen
*Maria Giannotti
*Inge Ipenburg
*Michel Maillot
*Wally Martin
*Martijn Overing

==References==
 

==External links==
*  
*  
*    

 
 
 
 
 
 
 


 
 