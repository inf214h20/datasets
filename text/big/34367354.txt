Italian Secret Service
{{Infobox film
 | name =Italian Secret Service
 | image =  Italian Secret Service.jpg
 | director = Luigi Comencini
 | writer =Leonardo Benvenuti  Luigi Comencini  Piero De Bernardi  Massimo Patrizi
 | starring =  Nino Manfredi Françoise Prévost (actress)|Françoise Prévost
 | music = Fiorenzo Carpi
 | cinematography =  Armando Nannuzzi
 | editing =  Nino Baragli
 | producer = Angelo Rizzoli
 | distributor =
 | released =  5 February 1968
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 1968 Cinema Italian comedy film directed by Luigi Comencini. For his performance in this film and in Il padre di famiglia, Nino Manfredi was awarded with a Golden Plate at the 1968 Edition of David di Donatello.  

== Cast ==
*Nino Manfredi: Natalino Tartufato
*Françoise Prévost (actress)|Françoise Prévost: Elvira Spallanzani
*Clive Revill :Charles Harrison
*Gastone Moschin: Lawyer Ramirez
*Jean Sobieski: Edward Stevens
*Giampiero Albertini: Ottone
*Alvaro Piccardi: Ciro
*Giorgia Moll:  "The Bird"
*Enzo Andronico: Femore
*Attilio Dottesio: Russian Agent

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 
 