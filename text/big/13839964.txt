Odour of Chrysanthemums (film)
{{Infobox Film
| name           = Odour Of Chrysanthemums
 
| caption        = John Bates (Jake McCollough) with his siter Annie Bates.
| writer         = Novel: D. H. Lawrence Screenplay: Mark Partridge
| starring       = Geraldine ORawe, Geraldine James, Jake McCollough Mark Partridge
| producer       = Andrea Wallace Grant
| composer    = Stephen Daltry
runtime        = 28mins
}}
 2002 short film directed by Mark Partridge and based on the short story by D. H. Lawrence.

== Plot ==
The film tells of a Nottinghamshire coalminers wife, a young mother, waiting for her abusive husband Walter to come home. She blames his drinking for his absence. It later turns out he has been killed in a pit accident. Laying out his corpse, after it is brought home from the mine, makes her realize they never really knew each other. Upon the discovery that her husband has died, the protagonist, Elizabeth, is able to remain astonishingly calm and collected, especially in front of her children. In contrast, Walters mother, who lives near the young couple and their children, becomes hysterical, highlighting her overbearing and somewhat irritating nature. The presence of pink chrysanthemums throughout the story represents Elizabeths constant desire for some hint of beauty within her life. One of the miners who brings in Walters body knocks over the vase of flowers, symbolizing Elizabeths loss of control over her life. Now that her husband, the provider for the family, has died, she has nowhere to turn. Despite the possible financial issues that could ensue, it is indicated that Elizabeth feels the family might be better off because of Walters abusive and angry nature.

== Cast ==
*Elizabeth Bates - Geraldine ORawe
*Grandma Bates - Geraldine James
*John Bates - Jake McCollough
*Annie Bates - Florence Romney-Scriven Bruce Alexander

== Crew ==
*Director - Mark Partridge 
*Producer - Andrea Wallace Grant
*Set Nurse - Helen Smith
*Costume Designer - Wyn Humphries
 "Composer" - Stephen Daltry

==Trivia==
* The role of John Bates was almost given to Jake McColloughs younger brother Daniel, who was closer to the characters age of six. However, he was rejected when deemed "too cute" for the role of the grumpy child by director Mark Partridge. Jake, who was aged eight at the time, therefore won the role.
* Filming took a total of Two weeks although the actors playing the children only attended one week of filming
* Costume Designer Wyn Humphries went of to design costumes for the live action Garfield films.
*Some of the movie was filmed at Pinewood Studios

== Awards ==
The Film won First Prize in the short film category of the Milan Film Festival.

== External links ==
* 
* 

 
 
 
 
 
 

 