Nuns on the Run
 
 
{{Infobox film
| name           = Nuns on the Run
| image          = Nuns_on_the_run_poster.jpg
| caption        = Theatrical release poster
| tagline        = The Story of an Immaculate Deception.
| writer         = Jonathan Lynn 
| starring       = Eric Idle Robbie Coltrane Camille Coduri Janet Suzman Doris Hare 
| director       = Jonathan Lynn 
| cinematography = Michael Garfath Michael White
| studio         = HandMade Films
| distributor    = 20th Century Fox
| released       = 16 March 1990
| music          = Yello Hidden Faces Frank Fitzpatrick (Music Supervisor) George Harrison
| editing        = David Martin
| runtime        = 89 minutes  English
| country        = United Kingdom
| budget         =
| gross          = $10,959,015
}} 1990 British comedy film starring Eric Idle and Robbie Coltrane, also featuring Camille Coduri and Janet Suzman. It was written and directed by Jonathan Lynn and produced by HandMade Films. Many of the outdoor scenes were shot in Chiswick. The soundtrack was composed and performed by Yello.

==Plot== short sighted aspiring psychology student.

Brian and Charlie ultimately come up with a plan to betray their gang and flee the UK, whilst robbing a Triad gang of their ill-gotten money through drug dealing. However, Casey, no longer trusting Brian and Charlie, plans to eliminate them at the same time, ordering his hit men, Abbott and Morley, to kill them during the Triad gang robbery. Faith, having overheard all about the plan, tries to remove Brian from the scene, but fails. Brian and Charlies plan to rob the Triads and flee, goes well until their car runs out of petrol, as Charlie had forgotten to refill it. They flee the Triads, Abbott and Morley on foot and hide in a nunnery, later dressing up as Nuns and introducing themselves to the Sister Superior, Liz (Janet Suzman) as Sisters Euphemia (Brian) and Inviolata (Charlie). When Casey comes to know about their back-stabbing, he becomes enraged and places a bounty on Brian and Charlies heads.

Faith, who witnessed the gunfight and saw Brian and Charlie enter the nunnery, goes to the nunnery to find them, but she is taken to the infirmary after Sister Liz notices a gunshot wound on Faiths arm (inflicted by Morley, who wanted to silence her). Brian spots her and visits Faith in her room, but in wanting her to leave them alone for her safety, he tells her that he is married. Faith lets slip that she is going to the church to confess. Brian, worried that the priest, Father Seamus, may advise Faith to go to the police, enlists Charlies help in distracting Father Seamus long enough for Brian to impersonate him. During the confession, Faith admits that she does love Brian, with the latter advising her to "keep her trap shut". However, as Faith leaves the church, the Triads who have been waiting outside, abduct her in their van and interrogate her about their stolen money. Faith tells them that Casey will most likely have it, as Brian and Charlie worked for him. The Triads release her, but she bumps into a pole and bangs her head when she falls over, ending up in the hospital.
 nun attire behind. Abbott and Morley break down the door, but are too late to catch them. That night, after narrowly avoiding the Triads, Brian and Charlie sneak back into the nunnery through the window of a rather eccentric nun, Sister Mary, who wakes up and starts a commotion. Brian and Charlie make it to their rooms just in time and slip into their spare nun clothes. Once alone, Charlie convinces Brian that their best option now is to take their money and just go to the airport, but Brian learns that Faith is in the hospital and goes to see her as a nun. Brian begs Charlie to let him bring Faith with them, but Charlie refuses, spotting a Triad posing as a janitor, observing them.

Brian and Charlie prepare to carry out their plan, but Sister Mary asks them about the previous night and recognises them as the two men who broke into her room by noticing Charlies growing beard and the upright toilet seat. They tie her up and break into the cupboard to regain their confiscated money, but the other nuns, including the superior, confront them and they are forced to tell them the truth before making a run for it. Unfortunately, Morley who sees them steal a mans utility truck with the money and he and Abbott give chase, as do Sisters Liz and Mary. Morley contacts Casey, who joins the chase, as do the Triads, who were closely watching him. Brian forces Charlie to drive to the hospital, where he tells Faith the truth whilst Charlie distracts their enemies, the nuns and the police. Ultimately, Casey, Morley and Abbott meet their individual fates as Brian and Charlie escape from the hospital with Faith, after stealing two nurses uniforms and an ambulance. Soon after, two police officers apprehend the two nurses dressed with Charlies and Brians nun habits, only to find out that they are not who they are looking for. The Triads later spot them, but are unable to give chase due to injuries sustained from their previous encounters with Brian and Charlie. Sisters Liz and Mary find a briefcase of money left behind accidentally by Brian and Charlie, and decide to donate it to a drug rehabilitation clinic.

At the airport, Brian and Charlie book a flight to Brazil for themselves and Faith, but an airport police officer appears and requests the airline agent to notify him if she comes across anyone called Hope or McManus. The final scene shows Faith on the plane to Brazil, apparently alone, until Brian and Charlie appear before her, disguised as flight attendants.

==Cast==
* Eric Idle as Brian Hope/Sister Euphemia of the Five Wounds. Tired of his violent gangster lifestyle, he conspires with his best friend Charlie to flee to Brazil and lead a law-abiding life, but a romantic relationship with Faith Thomas proves to be a minor delay.
* Robbie Coltrane as Charlie McManus/Sister Inviolata of the Immaculate Conception. Brians best friend, Charlie makes plans to flee to Brazil with him and the gangs savings. His knowledge of Catholicism aids them in hiding out in the nunnery.
* Janet Suzman as Sister Liz, the Sister Superior of the nunnery. Though she initially receives Brian and Charlie warmly while disguised as nuns, she is visibly disappointed when she realises they are wanted criminals, but ultimately decides against turning them in to the police. myopic and hemophobic Catholic, she serves as Brians love interest throughout the film. Though Charlie also cares for her, he disapproves of Brians relationship with her on account of the risks involved and very reluctantly agrees for her to go with them to Brazil. Her fear of blood is established when she faints at the sight of the gunshot wound made by Morley.
* Robert Patterson as Mr. "Case" Casey. Brian and Charlies boss and the one who killed Louie, their former boss. Young and ambitious, his methods clash with Brian and Charlies ideals and they ultimately betray him. At the same time, he has plans to get rid of them too. Later on, he places a bounty on their heads, but following a final confrontation with Brian and Charlie in the hospital, he is arrested by the police, when he drops his gun in the middle of the foyer. He is last seen in the back of a police van shouting at Brian and Charlie who escape to the airport, however the van is soundproof and his tirade is muffled.
* Doris Hare as Sister Mary of the Sacred Heart. Though well-meaning, she is rather rude and ill-tempered, frequently getting on Brian and Charlies nerves, especially when she loses the key to the cupboard where their briefcases of stolen money is being stored.
* Lila Kaye as Sister Mary of the Annunciation. An alcoholic and addicted to gambling, Sister Mary is the first nun to deduce that Brian and Charlie are actually men, partly due to having had a previous encounter with them out of their disguises. Tom Hickey as Father Seamus, the somewhat lecherous priest of the nunnery. He appears to take a liking to a disguised Brian, unaware that he is a man, and Charlie later lures him away for a talk to prevent him from speaking with Faith and telling her to go to the police. Robert Morgan as Morley, one of Caseys henchmen. He conspires with Casey to kill Brian and Charlie and rob a local Triad mob of their ill-gotten money. He is last seen holding Sister Liz and Sister Mary at gunpoint in the hospital, believing that they are Brian and Charlie, and his fate is left unrevealed, though it can be assumed he fled.
* Winston Dennis as Abbott, one of Caseys henchmen and the bouncer of his health club. He is frequently seen with Morley, very similar to Brian and Charlie. He is last seen in the hospital where he threatens Brian, Charlie and Faith with a gun, only to be struck in the stomach and subdued by Faiths brother, leaving him by the side of a door. His fate is left unrevealed.
* The Triads:
**Gary Tang as Ronnie Chang, the head of the Triads and the owner of a drinking bar which is a front for laundering money from crack cocaine. Casey plans to steal his ill-gotten money, but it is instead stolen by Brian and Charlie. Chang himself is shot in the leg by Morley in the ensuing shootout.
**David Forman as Henry Ho. He catches Brian and Charlie follow him and the other Triads home to see where they live, and threatens to chop their arms off if they do it again. A later encounter with Brian and Charlie in a speeding taxi leaves him with an injured arm.
**Nigel Fan as Dwayne Lee. He is frequently seen with Henry Ho, apparently his best friend. He injures his head during an encounter with Brian and Charlie in a speeding taxi.
**Ozzie Yue as Ernie Wong, the most senior of the Triads. Following the theft of their money, he poses as a Janitor in Faiths hospital in order to wait for Brian and Charlie, but during the rescue, Charlie knocks him out with a bedpan.

==Reception==

The movie received mixed reviews from critics, and was criticised in the United States for its lack of depth and excessive use of nuns for humour. Nuns on the Run currently holds a 47% rating on Rotten Tomatoes.

===Negative===
 Bob Harper, announced that they would be barred from press screenings of future films released by the company, though he backed down later that year under pressure from the Chicago Film Critics Association (of which neither Siskel nor Ebert was a member).  Michael Wilmington of the Los Angeles Times noted that as far as drag comedies go, the film "has some bawdy class--but only because of its casting." 

===Positive===

Vincent Canby wrote in The New York Times that "Nuns on the Run is a great leveler. It makes everyone in the audience feel a rascally 8 years old, the age at which whoopee cushions (when they work) seem the greatest invention since firecrackers."  Owen Gleiberman wrote in one of Entertainment Weeklys first issues that the film "isnt a madcap-hysterical, end-of-the-empire drag farce; it doesnt hash over what Monty Python did definitively over 20 years ago. Its a cleverly directed caper comedy about two crooks on the lam, and it has its fair share of chuckles." 

===Box Office===

The film was successful in the US on limited release, making $658,835 in first screenings at 76 theatres.  Nuns on the Run grossed $10,959,015 US dollars, according to Box Office Mojo.

==See also==
* Cross-dressing in film and television

==References==
 

==External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 