Everyone (film)
 
{{Infobox film
| name           = Everyone
| image          = Evryone-bill-marchant.jpg
| caption        = Original film poster
| director       = Bill Marchant
| screenplay     = Bill Marchant
| story          =  Mark Hildreth
| producer       = Christine Lawrance  Bill Marchant  Stephen Park
| music          = 
| cinematography = Jane Weitzel
| editing        = Tony Dean Smith
| distributor    = 
| released       = 2004 
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Canadian film written and directed by Bill Marchant. The film shot in Vancouver, British Columbia is about a gay couple, Ryan (Matt Fentiman) and Grant (Mark Hildreth) having a wedding ceremony in their backyard. They have invited the family and all the emotional baggage that comes with it.

==Cast==
;Starring
*Matt Fentiman as Ryan Mark Hildreth as Grant
*Brendan Fletcher as Dylan
;Other cast
 (in alphabetical order) 
*Katherine Billings as Rebecca
*Michael Chase as Gale
*Suzanne Hepburn as Trish
*Bill Marchant as Shepard
*Cara McDowell as Rachel
*Andrew Moxham as Kalvin
*Stephen Park as Luke
*Carly Pope as Rena
*Tom Scholte as Roger
*Nancy Sivak as Madeline
*Debra Thorne as Betty
*Anna Williams as Jenny

==Awards==
*2004: Won Golden Zenith prize for "Best Film from Canada" at Montreal World Film Festival Mark Hildreth at Leo Awards
*2005: Nominated for "Feature Length Drama: Best Supporting Performance by a Male" for Brendan Fletcher

==External links==
* 

 
 
 
 
 
 
 