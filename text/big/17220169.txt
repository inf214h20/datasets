Woman (1968 film)
{{Infobox film name           = Woman image          = Woman (1968 film).jpg caption        = Theatrical poster director       = Jeong Jin-u Yu Hyon-mok Kim Ki-young  Jung Jin-woo Yu Hyun-mok producer       = Park Gwan-sik writer         = Lee Eun-seong Kim Seung-ok Kim Ki-young starring       = Shin Seong-il  Moon Hee music          = Han Sang-ki cinematography = Jang Seok-jun Son Hyeon-chae Choe Ho-jin editing        = Kim Hee-su distributor    = Korea Art Movie Co., Ltd. released       =   runtime        = 100 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name      = {{Film name hangul         =   hanja          =   rr             = Yeo mr             = Yŏ context        = }}
}} 1968 three-part South Korean film directed by Kim Ki-young, Jung Jin-woo and Yu Hyun-mok. The film was based on ideas of Kim Ki-youngs wife, Kim Yu-bong, and Kim directed the last third. 

==Synopsis==
The film is a melodrama about a man who falls in love with a woman while traveling to Seoraksan. The man becomes infatuated with the womans hair. The woman, who has a terminal illness, promises to leave her hair to the man after she has died. Later the man finds that the woman has died, and her hair has been sold to someone else. He then has a romantic relationship with another woman who turns out to be his mother. 

==Cast==
* Shin Seong-il 
* Moon Hee
* Kim Ji-mee
* Choi Eun-hee

==Notes==
 

==Bibliography==
*  
*  
*  

 

 
 
 
 


 