When She Jumped
{{Infobox film
| name           = When She Jumped
| image          = Cuando ella saltó.jpg
| caption        = Poster
| director       = Sabrina Farji
| producer       = Jorge Poleri
| writer         = Lucía Ercasi, Sabrina Farji
| starring       =
| music          =
| cinematography = Martín Mohadeb
| editing        = Ian Kornfeld
| distributor    = Instituto Nacional de Cine y Artes Audiovisuales (INCAA)
Primer Plano Film Group S.A.
| released       =  
| runtime        = 97 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}}
When She Jumped ( ) is a 2007 Argentine drama film directed and written by Sabrina Farji. The film stars Iván de Pineda and Andrea Galante and deals with the theme of suicide.

Iván de Pineda is nominated for a 2008 Best New Actor Silver Condor award for his performance as Ramiro.

==Cast==
*Iván de Pineda as Ramiro
*Andrea Galante as Lila / Ángela
*Juan M. Aguiar as Neighbour
*Boy Olmi as El Zafiro
*Lalo Mir as El Licenciado
*Victoria Carreras as Diana Triada
*Sandra Ballesteros as Ana
*Leonardo Ramírez as  Seferino
*Darío Levy as Uno
*Diego Cosin as Dos
*Antonia De Michelis  as nurse
*Zoe Trilnick Farji as Niña Gitana

==External links==
*  
*   at the cinenacional.com  

 
 
 


 