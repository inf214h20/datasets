Pakten (film)
Pakten (  film, directed by Leidulv Risan.

It made headlines in Norway as it was the first Norwegian film to star several respected Hollywood stars, namely veteran actors Robert Mitchum and Cliff Robertson. It also boasted some of the biggest acting names from Sweden (Erland Josephson), Germany (Hanna Schygulla and Ernst Jacobi), Austria (Nadja Tiller) and Norway.
 Cologne and Heidelberg.

Its budget of 5,000,000 USD was above average for a Norwegian movie at the time.
 doctor Carl (Espen Skjønberg) collapses in the streets of Oslo, and awakens in the hospital. To his great surprise he finds himself surrounded by his old buddies Ernest (Mitchum), Ted (Robertson) and August (Josephson). Taking matters into their own hands they "kidnap" the dying Carl and embark on an emotional journey back to Heidelberg, where they met studying medicine before World War II. Their plan is to fulfill Carls final wish but soon find their cheerful trip overshadowed as they reveal a plot dating back to the pre-war Nazi era.

The movie received mainly fair reviews although many seemed to agree that the Nazi-subplot was too melodramatic, and got in the way of its feel-good nature.

== Trivia ==

While he had a supporting role in a 1997 TV movie, this was Hollywood-legend Robert Mitchums last starring role on the big screen. It was also the first time he had worked on a Norwegian movie (Mitchums mother was Norwegian).

==External links==
*  

Swedish: Pakten

 
 
 
 
 
 
 


 