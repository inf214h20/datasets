Tilly of Bloomsbury (1921 film)
 Tilly of Bloomsbury}}
{{Infobox film
| name           = Tilly of Bloomsbury 
| image          =
| caption        = Rex Wilson
| producer       = 
| writer         = Ian Hay Tom Reynolds Campbell Gullan
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = 
| released       = September 1921
| runtime        = 50 mins
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 British silent silent comedy Rex Wilson Tom Reynolds, Henry Kendall Tilly of Bloomsbury by Ian Hay, and was the first of three film adaptations. 

==Cast==
* Edna Best - Tilly Welwyn Tom Reynolds - Samuel Stillbottle
* Campbell Gullan - Percy Welwyn Henry Kendall - Dick Mainwaring
* Helen Haye - Lady Adela Mainwaring Frederick Lewis - Abel Mainwaring
* Georgette de Nove - Martha Welwyn
* Leonard Pagden - Lucius Welwyn
* Isabel Jeans - Sylvia Mainwaring
* Vera Lennox - Amelia Mainwaring
* Lottie Blackford - Mrs. Banks

==References==
 

 
 
 
 
 
 
 
 
 
 
 


 
 