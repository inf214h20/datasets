Yellowbird (film)
 
{{Infobox film
| name           = Yellowbird
| image          = Sc-yellowbird-onesheet-final.jpg
| director       = Christian De Vita
| producer       = Corinne Kouper
| writer         = Antoine Barraud & Cory Edwards
| starring       = Seth Green Dakota Fanning Christine Baranski Yvette Nicole Brown Richard Kind Jim Rash Danny Glover Elliott Gould
| music          =  Stephen Warbeck 
| studio         = TeamTO and Haut et Court
| distributor    = SC Films International 
| released       =  
| runtime        = 90 minutes
| country        = France, Belgium
| language       = English
| budget         = €10&nbsp;million 
}}

Yellowbird ( ) is a 3D stereoscopic CG-animated film produced in 2014 by Paris-based TeamTO and directed by Christian De Vita. 

==Plot==
Darius, the leader of a flock of birds, is wounded just before it is time for the birds to migrate to Africa. Information about how to lead the migration has to be passed to the first bird that encounters Darius. That is Yellowbird who is excited by the challenge but has very little life experience.

==Cast==
* Seth Green as Yellowbird, a non migratory bird
* Dakota Fanning as Delf, Darius daughter and a teacher
* Christine Baranski as Janet, Karls mother and Darius sister
* Yvette Nicole Brown as Ladybug, Yellowbirds best friend
* Richard Kind as Michka, Janets husband
* Jim Rash as Karl, Janet and Michkas son
* Danny Glover as Darius, leader of the flock and Delfs father
* Elliott Gould as the Owl
* Brady Corbet as Willy, Flecks older brother
* Jamie Denbo as Maggie: Max, Gigi, Anton and Lisas mother		
* Jadon Sand as Fleck, Willys young brother	
* Cedric Yarbrough as Fleck adult
* Zachary Gordon as Max, Anton, Lisa and Gigis brother	
* Joey King as Gigi, Lisa, Anton and Maxs sister	 Ryan Lee as Anton, Max, Lisa and Gigis brother	
* Tara Strong as Lisa, Max, Anton and Gigis sister
* Conchata Ferrell as Marjorie
* Chris Parson as Rodent 1
* H. Michael Croner as Rodent 2
* André Sogliuzzo as Squirrel, Hunter
* Fred Tatasciore as Pigeons 

== Details ==

* Title : Yellowbird
* Directed by : Christian De Vita
* Written by : Antoine Barraud
* Additional writing : Cory Edwards
* Visual Development : Benjamin Renner
* Music : Stephen Warbeck 
* Production : Corinne Kouper
* Production Companies : TeamTO, Haut et Court, Panache Productions et la Compagnie Cinématographique 
* French Distribution : Haut et Court
* World Sales (except France) : SC Films International
* Original version : anglais
* Budget : €10&nbsp;million 
* Format : couleur - Ratio|1,85 - sound 7.1
* Genre : stereoscopic 3D
* Running time : 90 minutes
* Country of origin : France

== References ==
 

==External links==
*  
*  

 
 
 
 