Immaan Dharam
{{Infobox film
| name           = Immaan Dharam
| image          = 
| image size     = 
| caption        = 
| director       = Desh Mukherjee
| producer       = Premji Suchitra Films Pvt. Ltd.
| writer         = Salim-Javed
| screenplay     = Salim-Javed
| story          = Salim-Javed
| narrator       =  Helen Amrish Puri Prem Chopra Shriram Lagoo A. K. Hangal Om Shivpuri Utpal Dutt C.S. Dubey Satyendra Kapoor Pinchoo Kapoor Sajjan Mac Mohan Jagdish Raj
| music          = Laxmikant-Pyarelal Anand Bakshi (lyrics)
| cinematography = Nariman A. Irani
| editing        = Das Dhaimade
| studio         = 
| distributor    = Bombino Video Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}}

Immaan Dharam (faith in Urdu and Hindi, respectively) is a 1977 Hindi film. Written by Javed Akhtar, produced by Premji, it is directed by Desh Mukherjee. The film stars Amitabh Bachchan, Shashi Kapoor, Sanjeev Kumar (actor)| Sanjeev Kumar and Rekha. The music is by Laxmikant Pyarelal.The film was a disaster at box office.

==Plot ==

This is a story about two guys who act as mock witnesses and always hang around court to give evidence as per the requirement of the case every time. They have never believed in God but eventually they have to accept the power of God. Ahmed and Mohan Kumar Saxena are petty thieves, and con-men. They career is spent as false witnesses near the Bombay courts, where they get paid a set sum of rupees for false depositions. This changes when they meet and are influenced by Kabir Das, and decide to go straight. They find that its virtually impossible to earn a living as honest citizens. Then Kabir Das is arrested and imprisoned by a murder he claims he did not commit, and the duo promise to help him, and find out who the real killer is, but they themselves end up getting in trouble. 

==Cast==
*Amitabh Bachchan - Ahmed Raza
*Shashi Kapoor - Mohan Saxena
*Sanjeev Kumar - Kabir Das
*Rekha - Durga
*Aparna Sen - Shyamlee Helen - Jenny Francis
*Amrish Puri - Dharam Dayal
*Prem Chopra - Ranjeet
*Shreeram Lagoo|Dr. Shreeram Lagoo - Govinda Anna
*A. K. Hangal - Masterji, Shyamlees Father
*Om Shivpuri - Seth Jamuna Das
*Utpal Dutt - Balbir Singh, Military Man
*M. B. Shetty - Kargah Sudhir - Gupta
*C.S. Dubey - Lawyer, Gullu Miyas Case Satyan Kappoo - Prosecuting Lawyer, Jamuna Das Case
*Pinchoo Kapoor - Kabirs lawyer
*Sajjan - Lawyer, Jennys Case
*Raj Kishore - Munshi Macmohan - False witness
*Baby Rani - Pinky Francis
*Jagdish Raj - Police Inspector
*Gajanan Jagirdar - Barkat Chacha

==Crew==
*Director - Desh Mukherjee
*Writer - Salim-Javed 
*Producer - J. N. Manchanda, Premji
*Production Company - Suchitra Films Pvt. Ltd.
*Editor - Das Dhaimade
*Cinematographer - Nariman A. Irani
*Art Director - Marutirao V. Kale
*Stunts - Kodi S. Irani, M. B. Shetty
*Costume and Wardrobe - Dhanji Mistry, Keshav Rao
*Choreographer - P. L. Raj
*Music Director - Laxmikant-Pyarelal
*Lyricist - Anand Bakshi
*Playback Singers - Mohammed Rafi, Asha Bhosle, Mahendra Kapoor, Kishore Kumar, Lata Mangeshkar, Mukesh (singer)|Mukesh.

==Music==
{|class="wikitable"
|-
!Song Title !!Singers !!Time
|-
|"Ae Kaash Main Dekh Sakti" Lata Mangeshkar
|5:55
|-
|"Duniya Ek Adalat Hai"
| Mohammed Rafi, Kishore Kumar
|3:50
|-
|"Hum Jhooth Bolte Hain" Mohammed Rafi, Kishore Kumar
|6:15
|-
|"Kuncham Kuncham" Asha Bhosle, Mukesh, Mahendra Kapoor
|8:30
|-
|"O Jatta Aai Baisakhi" Mohammed Rafi, Mukesh  
|4:35
|-
|}

== External links ==
*  

 
 
 
 
 