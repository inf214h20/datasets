The Three Dances of Mary Wilford
{{Infobox film
| name           = The Three Dances of Mary Wilford
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene 
| producer       = 
| writer         = Johannes Brandt   Robert Wiene
| narrator       = 
| starring       = Friedrich Feher   Erika Glässner   Ludwig Hartau
| music          = 
| editing        = 
| cinematography = Willy Gaebel
| studio         = Ungo-Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}}
 German silent silent drama Die Sünderin. 

==Cast==
* Friedrich Feher  
* Erika Glässner   
* Ludwig Hartau   
* Reinhold Köstlin   
* Hermann Vallentin

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 