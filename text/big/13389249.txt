Days of Darkness (2007 Canadian film)
{{Infobox film
| name           = Days of Darkness
| image          = Age Des Tenebres Poster.jpg
| caption        = Original movie poster
| director       = Denys Arcand
| producer       = Denise Robert Daniel Louis Dominique Besnehard
| writer         = Denys Arcand
| starring       = Marc Labrèche Diane Kruger Sylvie Léonard Emma de Caunes Didier Lucien
| music          = Philippe Miller
| cinematography = Guy Dufaux
| editing        = Isabelle Dedieux
| distributor    = Alliance Films
| released       =  
| runtime        = 104 minutes
| country        = Canada France
| language       = French
| budget         = 
| gross          = 
}}
Days of Darkness ( , translated as The Dark Ages, also known as The Age of Ignorance) is a 2007 French Canadian comedy-drama film written and directed by Denys Arcand. It is the third part of Arcands loose trilogy which began with The Decline of the American Empire and the Academy Award-winning The Barbarian Invasions. It was screened out of competition at the 2007 Cannes Film Festival.   

==Cast==
* Marc Labrèche : Jean-Marc Leblanc
* Diane Kruger : Veronica Star
* Sylvie Léonard : Sylvie Cormier-Leblanc
* Caroline Néron : Carole Bigras-Bourque
* Véronique Cloutier : Line
* Rufus Wainwright : le prince charmant chantant (The charming singing prince)
* Macha Grenon : Béatrice de Savoie
* Emma de Caunes : Karine Tendance
* Didier Lucien : William Chérubin
* Rosalie Julien : Laurence Métivier
* Monia Chokri : Aziza
* Jean-René Ouellet : Saint Bernard de Clairvaux
* André Robitaille : le supérieur immédiat (the immediate supervisor)
* Hugo Giroux : Thorvald le Viking
* Christian Bégin : le motivateur hilare
* Pierre Curzi : Pierre
* Gilles Pelletier : Raymond Leclerc
* Johanne-Marie Tremblay : Constance Lazure
* Françoise Graton : Madame Leblanc
* Jacques Lavallée : Romaric
* Michel Rivard : le curé
* Thierry Ardisson : himself
* Laurent Baffie : himself
* Bernard Pivot : himself
* Donald Sutherland : himself

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 