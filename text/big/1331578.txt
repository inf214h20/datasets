White Noise (film)
{{Infobox film
| name           = White Noise
| image          = White Noise movie.jpg
| caption        = Promotional poster
| director       = Geoffrey Sax
| producer       = Paul Brooks
| writer         = Niall Johnson
| starring       = Michael Keaton Deborah Kara Unger Mike Dopud Ian McNeice Chandra West Keegan Connor Tracy
| music          = Claude Foisy
| cinematography = Chris Seager
| editing        = Nick Arthurs
| studio         = Gold Circle Films Brightlight Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 101 minutes
| country        = Canada United Kingdom United States
| language       = English
| budget         = $10 million  
| gross          = $91,196,419	 
}} thriller film, electronic voice postmodern novel White Noise by Don DeLillo.

The movie did very well at the box office despite generally poor reviews from both critics and audiences. This success led Universal and other studios to realize that there was an untapped audience for horror films released in January, and begin releasing higher-quality horror films such as Cloverfield during that period, usually dismissed as the winter dump months of the movie calendar.

==Plot==
Jonathan Rivers (Michael Keaton) is a successful architect and lives a peaceful life with his wife Anna (Chandra West) until her unexpected disappearance. Eventually, he is contacted by Raymond Price (Ian McNeice), who claims that his own son had also died. He says he has recorded messages from Anna through electronic voice phenomena (EVP). While Jonathan is initially dismissive and angered, he later learns about his wifes tragic drowning. Desperate, he begins to believe that the recorded voice is indeed that of his wife. Jonathan becomes obsessed with trying to contact her himself, despite warnings from a psychic, Mirabelle Keegan (Keegan Connor Tracy), who tries to tell him how the recording can attract other, unwanted entities. A woman named Sarah Tate (Deborah Kara Unger), who also came to Raymond for his EVP work because she lost her fiancé, befriends Jonathan.

Raymond is found dead. Jonathan begins to be followed by three demons attracted by his obsession with EVP, and finds that some of the messages he is coming across are from people who are not yet dead, but may soon be. Jonathan hears cries from a woman who he finds in a car with a child. He is able to save the child, but not the woman. At that womans funeral, which Jonathan and Sarah both attend, Jonathan approaches the husband and tells him about what happened. The latter thanks Jonathan for saving his son but then asks to be left alone. The husband continues to tell Jonathan to stay away from him and his family. Afterwards, Jonathan sees images of another person, a missing woman named Mary Freeman, while working with his EVP devices. Sarah is later seriously injured by a fall from a balcony while possessed by the demons, that incident which was foreshadowed by Sarahs image being among those on the EVP devices.

Jonathan locates the site of his wifes death by following signs on recordings and he also finds his wifes abandoned car. Jonathan finds a set of computers and electronic equipment on site. A construction worker (Mitchell Kosterman) from his company, who has been doing his own EVP work, is holding Mary captive. He has been under the control of the demons to kill all these people, including Anna. The three demons torture Jonathan by breaking his arms and legs and cause him to fall to his death, but a SWAT team along with Detective Smits (Mike Dopud) arrives and are able to save Mary by shooting the construction worker dead. After his funeral, Jonathans voice can be heard on the radio through static interference saying "Im sorry" to his son. The child recognizes the voice and smiles. Sarah, at the graveside in a wheelchair, is menaced by odd noises. And right before the credits roll in the camera flashes to a TV where Jonathan and his wife are visible.

==Reception==
The film was panned by critics, with a 8% rating at Rotten Tomatoes.  Despite this, the film was a financial success, making back over nine times its $10 million budget.

==Cast==
* Michael Keaton as Jonathan Rivers
* Deborah Kara Unger as Sarah Tate
* Mike Dopud as Detective Smits
* Ian McNeice as Raymond Price
* Chandra West as Anna Rivers
* Nicholas Elia as Mike Rivers
* Keegan Connor Tracy as Mirabelle Keegan 
* Sarah Strange as Jane
* Amber Rothwell as Susie Tomlinson
* Suzanne Ristic as Mary Freeman
* Mitchell Kosterman as Work Man

==Sequel==
A sequel titled   was released in January 2007.

==Legacy==
 Anne Thompson during a 2013 indieWIRE panel discussion.    
 Ride Along The Devil Inside, which took in $35 million despite a strongly negative reaction from critics and audiences. "Ever since White Noise was a hit in 2005, that’s what started it. If you look back at every first weekend, besides expanding titles, the only new release is usually one crappy horror movie," C. Robert Cargill of Aint It Cool News told Hollywood.com in 2013.   

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*   
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 