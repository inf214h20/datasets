My Forbidden Past
{{Infobox film
| name           = My Forbidden Past
| image	         = My Forbidden Past FilmPoster.jpeg
| image_size     = 225px
| caption        = Theatrical release poster Robert Stevenson
| producer       = Polan Banks Robert Sparks
| screenplay     = Marion Parsonnet
| based on       =  
| writer         =  Leopold Atlas  
| starring       = Robert Mitchum Ava Gardner Melvyn Douglas
| music          = Friedrich Hollaender
| cinematography = Harry J. Wild
| editing        = George C. Shrader
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,150,000 (US rentals) 
}} Robert Stevenson. It stars Robert Mitchum and Ava Gardner.  Adapted from Polan Banks novel Carriage Entrance by Leopold Atlas.

==Plot summary==

In 1890s New Orleans, wrongly believing Barbara Beaurevelle (Gardner) had stood him up on the eve of their elopement, Dr. Mark Lucas (Mitchum) has returned from South Africa accompanied by Corinne (Janis Carter), a woman he married on the rebound. Determined to win him back, Barbara bribes her cousin (Melvyn Douglas) to break up the marriage, a cold-blooded scheme that ends in death and the doctor suspected of murder.

==Cast==
* Robert Mitchum as Dr. Mark Lucas
* Ava Gardner as Barbara Beaurevelle
* Melvyn Douglas as Paul Beaurevelle
* Lucile Watson as Aunt Eula Beaurevelle
* Janis Carter as Corinne Lucas
* Gordon Oliver as Clay Duchesne
* Basil Ruysdael as Dean Cazzley
* Clarence Muse as Pompey
* Walter Kingsford as Coroner Will Wright as Luther Toplady

==Reception==
The film recorded a loss of $700,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p256. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 

 