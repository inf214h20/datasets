Million Dollar Legs (1932 film)
{{Infobox film
| name           = Million Dollar Legs
| image          = Susan Fleming Argentinean Magazine AD.jpg
| image_size     =
| caption        = Susan Fleming
| director       = Edward F. Cline
| producer       = Herman J. Mankiewicz B. P. Schulberg
| writer         = Nicholas T. Barrows Joseph L. Mankiewicz
| starring       = Jack Oakie W. C. Fields Andy Clyde Lyda Roberti Susan Fleming Ben Turpin
| music          = Rudolph G. Kopp(uncredited) John Leipold(uncredited)
| cinematography = Arthur L. Todd
| editing        =
| distributor    = Paramount Pictures
| released       = July 8, 1932
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

Million Dollar Legs (1932 in film|1932) is an American comedy film starring Jack Oakie and W. C. Fields, directed by Edward F. Cline, produced by Herman J. Mankiewicz (co-writer of Citizen Kane) and B. P. Schulberg, co-written by Joseph L. Mankiewicz, and released by Paramount Pictures.  The movie was inspired by the 1932 Summer Olympics, held in Los Angeles.

==Cast (in credits order)==
*Jack Oakie as Migg Tweeny
*W. C. Fields as The President
*Andy Clyde as The Major-Domo
*Lyda Roberti as Mata Machree
*Susan Fleming as Angela
*Ben Turpin as mysterious man
*Hugh Herbert as Secretary of the Treasury George Barbier as Mr. Baldwin Dickie Moore as Willie - Angelas brother
*Ben Taggart as the ships captain (uncredited)

==Synopsis== weightlifting competition, and collect a large cash reward that has been offered to medalists by Tweenys employer. Tweeny then sets out to find athletes to make up  Klopstokias Olympic team, and quickly discovers that the country abounds in athletes of preternatural abilities. The team, with Tweeny as their trainer, boards a steamship bound for America.

Meanwhile, the rebellious cabinet ministers, who are determined to sabotage Klopstokias Olympic bid, have enlisted the services of "Mata Machree, the Woman No Man Can Resist" (Lyda Roberti), a Mata Hari-based spy character who sets out to destroy the Klopstokian teams morale by seducing each athlete and then setting them against each other in a collective brawl. Her efforts have the intended effect: when the team arrives in Los Angeles they are in no condition to compete. After a pep talk from Tweeny fails to inspire them, Angela tracks down Mata, defeats her in an underwater fight, and forces a confession from her before the assembled team, which restores the athletes fighting spirit. They take to the field and begin winning events.

By the time the weightlifting competition begins, Klopstokia needs only three more points for victory. In the films final scene, Tweeny excites The Presidents fierce temper in order to inspire him to a final superhuman effort. The President throws a 1000-lb weight at Tweeny, missing him but  winning both the weightlifting competition and the shot put for Klopstokia.

==Reviews== New Yorker David Denby Duck Soup, which came out the following year and is actually more disciplined."

Critic Pauline Kael claimed that Million Dollar Legs was one of her favorite films. 

==Notes==
Susan Fleming, who portrayed the presidents daughter in the film, later married Harpo Marx.  Producer Herman Mankiewicz subsequently co-wrote Citizen Kane with Orson Welles, and his brother and co-writer of this films script, Joseph L. Mankiewicz, wrote and directed All About Eve two decades later.

Hank Mann apparently played the role of a customs inspector in the film, but his part was cut from the final release print. All promotional material for the film lists him in the main cast, and one lobby card shows him in costume with George Barbier . Mann was a comedian and silent film star and founding member of the Keystone Cops.

According to Bill Marx, son of Harpo Marx, Mankiewicz originally developed this story with the intent of making it a Marx Brothers film, but they declined it. 

==References==
 

==External links==
* 
*http://www.newyorker.com/arts/critics/notebook/2010/06/07/100607gonb_GOAT_notebook_denby

 

 
 
 
 
 
 
 
 
 