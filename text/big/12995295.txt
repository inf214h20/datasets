Genghis Khan (1965 film)
 
{{Infobox film
| name = Genghis Khan
| image =
| caption =
| director = Henry Levin
| producer = Irving Allen
| writer = Beverley Cross (screenplay) Berkely Mather (story) Clarke Reynolds (screenplay)
| starring = Omar Sharif Stephen Boyd James Mason
| music = Dušan Radić
| cinematography = Geoffrey Unsworth
| editing = Geoffrey Foot
| distributor = Columbia Pictures
| released = April 15, 1965 (Germany) June 23, 1965 (U.S.) July 31, 1965 (Japan) August 30, 1965 (Sweden) August 30, 1965 (UK) September 22, 1965 (France) October 2, 1965 (Italy) October 14, 1965 (Hong Kong) November 5, 1965 (Finland) December 10, 1965 (Australia) May 23, 1966 (Denmark)
| runtime = 127 min.
| country = United Kingdom Yugoslavia
| language = English
| awards =
| budget =
| gross = $2,250,000 
}}
 Doctor Zhivago. The film also included James Mason, Stephen Boyd, Robert Morley, Françoise Dorléac and Telly Savalas. 

A 70 mm version of the film was released by CCC Film in West Germany. It was filmed in Yugoslavia.

==Cast==
*Omar Sharif (Temujin, later Genghis Khan)
*Stephen Boyd (Jamuga)
*James Mason (Kam Ling)
*Eli Wallach (Shah of Khwarezm)
*Françoise Dorléac (Bortei)
*Telly Savalas (Shan)
*Robert Morley (Emperor of China)
*Michael Hordern (Geen)
*Yvonne Mitchell (Katke)
*Woody Strode (Sengal)
*Kenneth Cope (Subutai|Subotai)
*Roger Croucher (Massar)
*Don Borisenko (Jebai)
*Patrick Holt (Kuchiuk)
*Susanne Hsiao (Chin Yu)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 