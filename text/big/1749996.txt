Forklift Driver Klaus – The First Day on the Job
 
{{Infobox film
| name           = Forklift Driver Klaus – The First Day on the Job
| image          = Forklift-driver-klaus.png
| image_size     =
| caption        = Konstantin Graudus as Klaus
| director       = Stefan Prehn Jörg Wagner
| producer       = Michael Sombetzki
| writer         = Jörg Wagner Stefan Prehn
| narrator       = Egon Hoegen
| starring       =
| music          =
| cinematography = Matthias Lehmann
| editing        = Andrea Stabenow
| distributor    =
| released       =  
| runtime        = 9 min
| country        = Germany
| language       = German
| budget         = €90,000
| gross          =
}}

Forklift Driver Klaus – The First Day on the Job (in German Staplerfahrer Klaus – Der erste Arbeitstag) is a German short film from 2000 about the first day of Klaus work as a forklift driver. The film is a parody of work safety films from the 1980s.

The film was written and directed by Stefan Prehn and Jörg Wagner. Konstantin Graudus plays the role of Klaus, and Egon Hoegen is the narrator. It adds to the air of authenticity that the narrators voice is well known from road safety films, such as "Der 7. Sinn". 

The film quickly became famous, much thanks to its splatter film violence, which fans regard as comical due to its extreme and obviously fake nature.  The film received several awards and was made available on DVD by Anolis Entertainment in 2003, dubbed in English, French and Spanish.

==Plot==
The film is presented as a safety instruction video for forklift truck drivers and shows the first day of work for newly qualified forklift truck driver Klaus. The film highlights, in a gory manner, the dangers of unsafe operation of machinery. As the film progresses the injuries/deaths become more brutal, beginning with things like a man falling from the fork lift after he was lifted improperly, and closing with the most violent: ending in a stray chainsaw being driven around by a severed arm on the floor, reaching and ripping through a man who had already been cut in half waist-down due to Klaus previous accident. A gory POV shot of the chainsaw chopping through the man is shown. The film ends as Klaus is decapitated by the chainsaw and two men are left impaled onto the forklift prongs, screaming. The forklift drives off into the sunset as the impaled men continue to scream with the chainsaw racing after them.
End theme "Happyland" was written by French composer Laurent Lombard.

==Context==

Although the film is not officially part of the German training and education system for forklift trucks, it is frequently shown by instructors to lighten the mood. 

==Awards==
The film has won many awards, including: 2001
* The Jury Award for Best Short Film and the Audience Award for Best Short Film at the San Sebastian Horror and Fantasy Film Festival in 2001 2002
* The Friedrich-Wilhelm-Murnau-Award at the Day of the German Short Film awards in 2002
* The German Film Critics Award for Best Short Film at the German Film Critics Association Awards in 2002 2003 (where it also won 3rd place in the Best Short Film category)

==External links==
*  
* 
*  

==References==
 
 
 
 
 
 
 
 
 