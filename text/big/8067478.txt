It's a Boy Girl Thing
 
{{Infobox film
| name           = Its a Boy Girl Thing
| image          = BOYGIRLTHING POSTER.jpg
| caption        =
| director       = Nick Hurran   
| producer       = Steve Hamilton Shaw David Furnish Martin F. Katz Geoff Deane
| starring       = Samaire Armstrong Kevin Zegers Sherry Miller Robert Joy Sharon Osbourne  Maury Chaykin Mpho Koaho Emily Hampshire Brooke DOrsay Genelle Williams
| music          = Christian Henson
| cinematography = Steve Danyluk
| editing        = John Richards
| studio         = Rocket Pictures
| distributor    = Icon Productions
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom Canada
| language       = English
| budget         =
| gross          = $7,385,434 
}} Geoff Deane, starring Samaire Armstrong and Kevin Zegers and set in the United States but produced in the United Kingdom. The producers of the film are David Furnish, Steve Hamilton Shaw of Rocket Pictures and Martin F. Katz of Prospero Pictures. Elton John serves as one of the executive producers. 
 UK and has since then been released in some countries in cinemas, in others directly to DVD, and in others as a TV film. Most of the school scenes were shot at Western Technical-Commercial School in Toronto, Ontario, Canada.

==Plot== football player while Nell is a nerdy, literature-loving girl. They loathe each other and are constantly in dispute. One day their class goes on a school trip to a museum and they are forced to work together on an assignment. They quickly begin arguing in front of a statue of the ancient Aztec god Tezcatlipoca.  As they argue, the statue casts a spell upon them — causing them to wake up in each others bodies the next morning. When they arrive at school, they immediately blame each other for the body swap, but agree to pretend to be the other person until they can find a way to switch back. At first, they seem to succeed, but quickly return to arguing when they each feel the other is misrepresenting them in the opposite body, such as Woody (in Nells body) answering a question oddly and surprising a teacher.
 Chinos and Oxford cotton button-down" making Woodys appearance look "dorky" which frustrates Woody, and he is even more frustrated after he hears about how Nell (in Woodys body) failed Woodys football practice the previous day. As payback, Woody (in Nells body) dresses in inappropriate and provocative clothing the following day. After school, Nell (in Woodys body), in retaliation, breaks up with Breanna (Brooke DOrsay), Woodys girlfriend, much to the disappointment of Woody. The humiliation competition continues when Woody (in Nells body) drives off with a biker boy, Nicky (Brandon Carrera), and makes Nell (in Woodys body) think she is going to lose her virginity. However, Woody decides it is "so gay" and leaves Nicky just as he is removing his clothing.
 Yale interview. Later that night Nell (in Woodys body) is getting drunk at a party while Woody (in Nells body) is stuck at a slumber party listening to all the gossip about Woody, and surrounded by nailpolish, pajamas and slippers.

After spending so much time together, Nell and Woody become very fond of each other and start to understand each other better. The night before the interview and the game, they agree to go to the Homecoming Dance together, as "not a date." The day of the interview and match, Woody goes to Yale for the interview and at first messes things up and is asked to leave, but he starts to talk about poetry in Hip hop music|rap, which impresses and astonishes the interviewer. After that, he goes to the match and watches Nell run in the winning touchdown in the closing seconds. A college recruiter witnesses his good performance and wants to talk to him later. After the game, they congratulate each other for their successes. Shortly after this, the spell lifts and they return to their original bodies. The scene finishes with Woody being kissed by Breanna and Nell going home very upset about it.

The following day, Woody tries to talk to Nell but is stopped by her mother, who sees Woodys family as uneducated. Nell receives a letter from Yale informing her that she has been accepted to Yale, meaning that her interview (done by Woody in her body) was successful. However, she is still upset with Woody and has decided not to go to the Homecoming Dance. Meanwhile, Nells father has a talk with her on the porch about Woody, during which she confesses she truly likes him, and her father surprises her with a dress and shoes for the dance. At the dance, Woody sees Nell and they leave the school together and share a kiss in front of their houses. The following day, Nell tells her mother that she is taking a years sabbatical before attending  Yale, and hops into Woodys car as they drive off together.

==Cast==
* Samaire Armstrong as Nell Bedworth / Woody Deane   
* Kevin Zegers as Woody Deane / Nell Bedworth 
* Sherry Miller as Katherine Bedworth
* Robert Joy as Ted Bedworth
* Sharon Osbourne as Della Deane
* Maury Chaykin as Stan Deane
* Mpho Koaho as Harry Horse Horson
* Dan Warry-Smith as Team Guy #1
* Emily Hampshire as Chanel
* Brooke DOrsay as Breanna Bailey
* Genelle Williams as Tiffany
* Alex Nussbaum as Mr. Zbornak
* Jack Duffy as Old Man
* John Bregar as Richard
* David Ndaba as Team Guy #2 Robert James Ramsay as Team Guy #3

==Soundtrack== Be Strong" Push the Red Dress" Marz and many more. (Woody, as Nell, refers to "Goodbye Yellow Brick Road" as "crappy music"!)

==Response==
The movie currently holds a 63% rating on Rotten Tomatoes  and a 6.4 out of 10 on IMDB indicating mixed to positive reviews.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 