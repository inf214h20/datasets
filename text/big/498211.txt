Jesus of Montreal
 
{{Infobox film
| name           = Jesus of Montreal
| image_size     =
| image	         = Jesus of Montreal FilmPoster.jpeg
| caption        = Movie poster
| director       = Denys Arcand
| producer       = Monique Létourneau
| writer         = Denys Arcand
| narrator       =
| starring       = Lothaire Bluteau Catherine Wilkening  
| music          = Yves Laferriere
| cinematography = Guy Dufaux
| editing        = Isabelle Dedieu
| distributor    = Koch-Lorber Films|Koch-Lorber (Region 1 DVD)
| released       = 17 May 1989 (France) 25 May 1990 (USA)
| runtime        = 118 minutes
| country        = Canada France
| language       = French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Jesus of Montreal ( ) is a 1989 Canadian film directed by Denys Arcand.

==Plot and allegory==
The film centres on a group of actors in Montreal, Canada who are gathered by Daniel, an actor hired by a Roman Catholic site of pilgrimage ("le sanctuaire") to present a Passion play in its gardens.

The sanctuary is implied to be Saint Josephs Oratory (although this organization actually refused permission to film there). In fact, the idea for the film came to its director after an actor apologized for appearing with a beard at an audition. The actor explained that he had the role of Jesus in a passion play at St-Josephs Oratory. Arcand went to see the play and recalls "I saw actors in a mediocre production which received shouted applause from the tourists. I decided I had to make a film". 

The actors interpretation of the life of Jesus is unconventional (including, for example, the statement that the biological father of Jesus was a Roman soldier, who left Palestine shortly afterwards). Still, it draws on current academic theories and research. The challenging production becomes the toast of the city. The higher authorities of the religious order that controls the sanctuary (or of the Roman Catholic Church, this is left vague) strongly object to this Biblical interpretation, and forcefully stop a performance.
 Jewish General Hospital. Despite immediate, skilled, and energetic efforts by the doctors and nurses to revive him, Daniel is pronounced Brain death|brain-dead. Daniels doctor asks for the consent of his friends to take Daniels organs for donation (since Daniel has no known relatives). Daniels physician states that the staff would have been able to save him, if he had been brought to them half an hour earlier.

The film is structured so that Daniels story parallels that of Christ. Some of the points of contact are: 
*Daniel has returned to Montreal after spending a long period travelling in "the East".
*Contradictory and uncertain stories are told about Daniels life story.
*In the opening scene, one actor points to Daniel, calling him "a much better actor", which echoes John the Baptist foretelling the arrival of Jesus the Messiah.
*The first actor later "sells-out" and lets his head be used in an advertisement, paralleling John the Baptists beheading.
*The actors then gather for the Passion play, some of them leaving safe jobs to do so, recalling Jesus gathering the Disciple (Christianity)|disciples. Jesus casts the money-lenders out of the Temple.
*Daniels arrest and court appearance before an indecisive judge, played by the films director himself, parallels Jesus appearance before Pontius Pilate.
*The smooth elite lawyer, who lays out a grand commercial career for Daniel, looking down from a skyscraper at the city, refers to the temptation of Christ by the devil atop a high pinnacle. Jewish General Jews killed Jesus The Good Samaritan Parable applauds the Samaritan - an outsider of the community to which Jesus preaches - for behaving as a true neighbour should. Daniel is not helped at the Catholic hospital - his own community - but is instead helped at the Anglo-Jewish hospital - a religious and linguistic group very different from Daniels.
*The resurrection of Jesus is depicted as the donation of Daniels organs, which live on in the lives of others.
*Daniels eyes are used to "heal" the blind.
*The founding of the Christian church becomes the plans for an experimental theatre company, which is "incorporated".

==Locations used for filming==
There are a few shots in the film of the exterior of Saint Josephs Oratory, taken from a distance. However, the interior church scenes were filmed in Montreal’s Church of St. Michael and St. Anthony. The exterior scenes of the sanctuary gardens were mainly filmed in areas which had not yet been built upon, around the Université de Montréal’s engineering school, the École Polytechnique de Montréal. The exterior scenes around a statue of Jesus were filmed at what was then the location of Marianopolis College, at 3880 Côte-des-Neiges Road.

==Cast==
* Lothaire Bluteau (Daniel)
*   (Mireille)
* Johanne-Marie Tremblay (Constance)
* Rémy Girard (Martin)
* Robert Lepage (René)
* Gilles Pelletier (Fr. Leclerc)
* Roy Dupuis (Marcel Brochu)

==Awards==
 TIFF List of Top 10 Canadian Films of All Time, in 1993 and 2004,  and was nominated for the 1989 Academy Award for Best Foreign Language Film.  In 2010 it was nominated for both Best Picture and Best Foreign Film at the 1st Annual 20/20 Awards.

==See also==
*Cinema of Quebec
*Culture of Quebec
*Dramatic portrayals of Jesus
*List of Quebec movies
*Passion (Christianity)

==References==
 

==External links==
* 
* 
* 
*  at the  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 