All About Lily Chou-Chou
{{Infobox film
  | name = All About Lily Chou-Chou
  | image = Lily-Chou-Chou 000.jpg
  | caption = Theatrical release poster
  | director = Shunji Iwai
  | producer = Koko Maeda
  | writer = Shunji Iwai
  | starring = Hayato Ichihara Shugo Oshinari Ayumi Ito Takao Osawa Miwako Ichikawa Izumi Inamori Yū Aoi
  | cinematography = Noboru Shinoda
  | editing = Yoshiharu Nakagami
  | distributor = Rockwell Eyes
  | released = 2001
  | runtime = 146 min
  | country = Japan
  | language = Japanese
  }}
  is a 2001 Japanese film, written and directed by Shunji Iwai, that portrays the lives of 14-year-old students in Japan and the effect the enigmatic singer Lily Chou-Chous music has on some of them.

==Plot==
All About Lily Chou-Chou follows two boys, Shūsuke Hoshino and Yūichi Hasumi, from the start of junior middle school when they first meet, and into second grade. The film has a discontinuous storyline, starting midway through the story, just after the second term of junior high school begins, then flashes back to the first term and summer vacation, and then skips back to the present.

In elementary school, Hoshino was one of the best students in school, but was picked on by his classmates. Hoshino and Hasumi meet and become friends when they join the kendo club, and Hoshino invites Hasumi to stay over at his house. Hoshinos family is wealthy in comparison to Hasumis family. Hasumi mistakes Hoshinos attractive young mother for his sister.

The kendo club summer camp training is tough, and Hoshino, Hasumi and some other first-grade boys decide to take a trip to Okinawa. Once there, Hoshino has a traumatic near-death experience and his personality changes from good-natured to dangerous and manipulative. Back at school in September for second term, he takes his place as class bully and shows his newfound power by ruining the lives of his classmates. An alternative voice, that of the character Sumika Kanzaki, attributes Hoshinos personality change to the collapse of his familys business and his parents divorce; this matches several scenes connecting the decline of Hoshino – who has had to change his name – to divorce.

Hasumi, the confused and shy former friend of Hoshino, finds himself sucked into his now-tormentors gang. He is ridiculed and coerced into doing Hoshinos dirty work, and finds solace only in the ethereal music Lily Chou-Chou makes, and acting as web editor for his fan website. Things become far worse for everyone when Hasumi is assigned to supervising Shiori Tsuda, whom Hoshino has blackmailed into enjo kōsai, and another girl is raped by Hoshinos lackeys after unwittingly offending the schools girl gang. The whole quagmire comes to a head when Hasumi heads to Tokyo to see a Lily Chou-Chou concert, where he encounters the last person he thought would be there.

The story of Hoshino and Hasumi is paralleled by messages posted to a Lily Chou-Chou message board which are displayed on screen. Until the meeting at the concert, it is left up to the viewer to figure out which characters in the story are posting under what names.

== Production ==

On April 1, 2000, Shunji Iwai went live with his internet novel, in the form of a website called Lilyholic, where he posted messages as several characters on the BBS. Readers of the novel were free to post alongside Iwais characters and interact with each other, indeed this BBS is where some of the content from the movie comes from. After the main incident in the novel took place, posting was closed and the second phase of the novel started, about the lives of 14-year-olds. (The novel is available on CD-ROM, but only in Japanese.)

Production on the film began in Ashikaga, Tochigi|Ashikaga, Tochigi Prefecture on August 13, 2000 and ended on November 28, 2000. It premiered at the Toronto International Film Festival on September 7, 2001 and opened in Japan on October 6, 2001.

Iwai was the first Japanese director to use the, at the time, completely new digital video camera, the 24p|"24 Progressive" to shoot the film.

It is thought that Iwai was inspired to shoot in digital by his friend, the anime and live-action film director Hideaki Anno, who shot his own digital film entitled Love & Pop, in 1998. Anno later cast Iwai as the lead in his second live-action film, Shiki-Jitsu. After the films a release a synopsis written from the point of view of the main character Yūichi was published online to explain the films events. 

==Music==
The soundtrack of Lily Chou-Chou was written and arranged by Takeshi Kobayashi, with vocals by the singer Salyu. It features a number of songs that are sung by the fictional rock star Lily Chou-Chou in the film. The soundtrack also makes heavy use of the classical music of Claude Debussy.

In 2010 Salyu and Takeshi Kobayashi released a new song under the YouTube name LilyChouChou2000, suggesting that the Lily Chou-Chou moniker was still alive.

== Cast ==
*Shugo Oshinari as Shūsuke Hoshino (星野修介 Hoshino Shūsuke), the best student in school who, after a trip to Okinawa, becomes a bully. Posts under the alias Blue Cat (青猫 Ao Neko).
*Hayato Ichihara as Yūichi Hasumi (蓮見雄一 Hasumi Yūichi), Hoshinos former friend who becomes a reluctant member of his gang and will later on be bullied by Hoshino. Yūichi is the leading character in the movie. He admins an online Lily Chou-Chou BBS under the alias Philia (フィリア Firia) and is a great fan of the singer.
*Ayumi Ito as Yōko Kuno (久野陽子 Kuno Yōko), a classmate of Yūichis. A brilliant pianist, she is the envy of a clique of powerful girls, and therefore is also bullied. She is raped by Hoshinos gang and cuts off her hair as a way of avoiding Shiori Tsudas fate.
*Yū Aoi as Shiori Tsuda (津田詩織 Tsuda Shiori), a classmate of Yūichi who gets blackmailed into enjo kōsai by Hoshino. Yūichi befriends her later on, and introduces her to Lilys music. Near the end of the movie she takes her life. Yuki Ito as Kamino, one of the boys in the blue school uniforms at the train station when Kuno is introduced.
*Izumi Inamori as Izumi Hoshino (星野いずみ Hoshino Izumi), Hoshinos mother. It is unknown if she is single. She loves her son very much and welcomes Yūichi with open arms during a junior high sleepover at the Hoshino household. A classmate of Yūichi suggests that this is done to ensure that Yūichi will enjoy being Hoshinos friend.
*Salyu as Lily Chou-Chou, the enigmatic and ethereal singer that Yūichi, Tsuda and others in the film are fans of. She is hardly seen in the film, except on a video screen near the storys end, but her music is heard throughout the movie. She is said by her fans to channel what is called "the Luminiferous aether|Ether", which is not unlike the invisible substance once thought by ancient philosophers to be the field that light travels through. This "ether" can be heard in the calm, melancholy songs she sings.
*Takao Osawa as Tabito Takao
*Miwako Ichikawa as Shimabukuro

== Details ==
*Quentin Tarantino used the song "Kaifuku Suru Kizu (Wounds that heal)" from the Lily Chou-Chou soundtrack in Kill Bill, in the scene where the Bride views Hattori Hanzos sword collection.
*The idea of Lily Chou-Chou the rock star was inspired by Faye Wong.
*Extras at the concert scene were given an index card with extremely detailed information as to the thought process they should be going through during filming. There were hundreds of extras, partly made up of fans of the internet novel who had BBS meet ups during the day. Posters from the BBS are visible in the background of this scene and can be spotted by watching for clues from their posts. Arabesque No. 1" that she made it her cell phone ringtone. track team, not the kendo team.
*Debussy wrote his famous Childrens Corner Suite (1909) for his beloved daughter whom he nicknamed Chou-chou. Love Letter (1995). 
*The movies original runtime was 157 minutes, but the original print of the 157 minute version no longer exists because it was burned. The extra 11 minutes was composed of extra and intense footages of the rape scene, a scene with Yūichi in the beach (similar to Hoshinos drowning scene) and an extended funeral scene. 

==Awards and nominations==
*2002 Berlin International Film Festival – Panorama (Shunji Iwai)
*2002 6th Shanghai International Film Festival – Best Music (Takeshi Kobayashi) and Special Jury Award (Shunji Iwai)

==Box office totals==

*Budget:¥150, 000, 000 ($1,249,656)

Local:
*Japan: ¥3, 026, 188, 000 ($25,211,298)
*Opening Week Gross: ¥514, 775, 000 ($4,288,612)
*Date Released: October 6, 2001
*In release: 15.7 weeks

International:
*USA: $26,485 (¥3,179,328)

Rentals:
*Japan: ¥810,340,000 ($6,750,436)

==References==
 

== External links ==
* 
* 
*   
* 
*  
*  -(New York Times review)

 

 
 
 
 
 