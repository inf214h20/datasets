Prayikkara Pappan
{{Infobox film
| name           = Prayikkara Pappan
| image          =
| caption        =
| director       = TS Suresh Babu
| producer       =
| writer         = Shaji Pandavath
| screenplay     = Shaji Pandavath Murali Chippy Geetha Jagadish
| music          = S. P. Venkatesh
| cinematography = Dinesh Babu
| editing        = K Sankunni
| studio         = SKB Films Combines
| distributor    = SKB Films Combines
| released       =  
| country        = India Malayalam
}}
 1995 Cinema Indian Malayalam Malayalam film, Geetha and Jagadish in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
  Murali
*Chippy Geetha
*Jagadish Madhu
*Raghavan Raghavan
*Sukumaran
*Vijayakumar
*Aliyar
*Bheeman Raghu Chithra
*Ganesh Kumar
*Kollam Thulasi
*Kuthiravattam Pappu
*Mamukkoya
*Sabnam
*Santhakumari
 

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kokkum poonchirakum || KS Chithra, Jagadish || Bichu Thirumala || 
|-
| 2 || Kombukuzhal || MG Sreekumar, P Jayachandran || Bichu Thirumala || 
|-
| 3 || Naagaveena meeti || MG Sreekumar || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 