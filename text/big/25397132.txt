Ourselves Alone (film)
 
 
{{Infobox film
| name           = Ourselves Alone
| image          = 
| image_size     = 
| caption        = 
| director       = Brian Desmond Hurst
| producer       = 
| writer         = Dudley Lesley Marjorie Deans Denis Johnston Philip MacDonald John Lodge John Loder
| music          = Harry Acres
| cinematography = Walter J. Harvey Brian Langley
| editing        = James Corbett
| distributor    = British International Pictures
| released       = 27 April 1936
| runtime        = 68 min.
| country        = United Kingdom
| language       = English
}}
 Irish Sinn John Lodge, John Loder and Antoinette Cellier.

==Synopsis==

The film opens with an IRA ambush of a police convoy carrying two captured members of the IRA. Irish Police Inspector Hannay (John Lodge) and British Captain Wiltshire of the Royal Intelligence Corps (John Loder) both turn out to be in love with Maureen Elliot (Antionette Cellier) sister of the IRA leader. The IRA leader is subsequently shot by Wiltshire. Hannay realises that Maureen is in love with Wiltshire and, as a final gesture, takes the blame for shooting her brother himself. Maureen then helps Captain Wiltshire to escape an IRA trap.

==Cast== John Lodge as Inspector Hanney John Loder as Capt. Wiltshire
* Antoinette Cellier as Maureen Elliott
* Niall MacGinnis as Terence Elliott
* Clifford Evans as Commander Connolly
* Jerry Verno as Private Parsley
* Bruce Lester as 2nd Lt. Lingard
* Maire ONeill as Nanny
* Tony Quinn as Maloney
* Paul Farrell as Hogan

==Reception== The Informer (1935) by other critics, but dissented from this opinion himself. "One of the silliest pictures which even an English studio has yet managed to turn out", he wrote. 

The film was voted the seventh best British movie of 1936. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 