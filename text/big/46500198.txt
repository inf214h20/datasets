Behind the Facade
{{Infobox film
| name = Behind the Facade 
| image =
| image_size =
| caption = Georges Lacombe   Yves Mirande
| producer =  Arys Nissotti   Pierre OConnell
| writer =  Max Kolpé    Georges Lacombe   Yves Mirande
| narrator =
| starring = Lucien Baroux   Jules Berry   André Lefaur
| music = André Gailhard      
| cinematography = Victor Arménise   Robert Juillard  
| editing =     Marthe Poncin   
| studio = Régina Films  ENIC (Italy)
| released = 14 March 1939 
| runtime = 85 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Georges Lacombe and Yves Mirande and starring Lucien Baroux, Jules Berry and André Lefaur.  The films sets were designed by the art director Lucien Aguettand. When the owner of an apartment block is found murdered, two policeman investigate the lives of the various tenants who live there.

==Cast==
* Lucien Baroux as Le commissaire Boucheron  
* Jules Berry as Alfrédo DAvila  
* André Lefaur as Corbeau  
* Gaby Morlay as Gaby  
* Elvire Popesco as Madame Rameau  
* Michel Simon as Picking 
* Betty Stockfeld as Langlaise  
* Erich von Stroheim as Eric  
* Simone Berriau as Lydia 
* Missia as Joséphine Picking  
* Raymond Segard as Robert Bernier  
* Marcel Orluc as Gérard Bernier 
* Jean Daurand as Le télégraphiste  
* Elmire Vautier as Marie, lhabilleuse de Lydia  
* Lina Darwils as Une locataire  
* Robert Ozanne as Le brigadier  
* Claude Sainval as Le gigolo de Gaby  
* Raymone as La bonne de Madame Mathieu  
* Joffre as Monsieur Martin, laveugle  
* Lise Courbet as Paulette 
* Andrex as André Laurent, lemployé de banque  
* Julien Carette as Le soldat 
* Aimé Clariond as Le président Bernier  
* Jacques Dumesnil as Albert Durant, le jouer de poker  
* Paul Faivre as Le concierge  
* Marcel Simon as Jules, lamant de Gaby 
* Gaby Sylvia as Madeleine Martin  
* Gabrielle Dorziat as Madame Bernier  
* Jacques Baumer as Lambert  
* Marguerite Moreno as La sous-directrice 
* Georges Bever as Un chauffeur de taxi  
* Rivers Cadet as Un agent  
* Roger Doucet 
* Hélène Flouest 
* Georges Malkine as Un chauffeur de taxi  
* Franck Maurice as Un agent  
* Yves Mirande as Le clochard sur le banc  
* Robert Moor as Un agent  
* Henri Nassiet as Petit rôle 
* Jean Wiener

== References ==
 

== Bibliography ==
* Aitken, Ian. The Concise Routledge Encyclopedia of the Documentary Film. Routledge, 2013.

== External links ==
*  

 
 
 
 
 
 
 

 