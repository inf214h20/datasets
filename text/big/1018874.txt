First Daughter (2004 film)
{{Infobox film
| name           = First Daughter
| image          = First Daughter poster.jpg
| caption        = Theatrical film poster
| screenplay     = Jessica Bendinger Kate Kondell
| story          = Jessica Bendinger Jerry OConnell
| narrator       = Forest Whitaker Amerie Rogers  Michael Keaton
| director       = Forest Whitaker John Davis Mike Karz Wyck Godfrey
| editing        = Richard Chew
| cinematography = Toyomichi Kurita New Regency Davis Entertainment
| distributor    = 20th Century Fox
| gross          = $10.4 million. 
| released       =  
| runtime        = 106 minutes
| language       = English
| country        = United States
| music          = Michael Kamen Blake Neely
| awards         = Gold Arias 
| budget         = $30 million
}} Amerie Rogers as Samanthas roommate, Mia Thompson.
 John Davis. Whitaker likened First Daughter to a fairy tale, characterizing it as "the story of a princess who leaves the castle   to go out in the world to discover who and what she is."  The film had languished in "development hell" for several years, and was further delayed even after its completion.  The film was not a commercial success upon its eventual release, and received overwhelmingly negative reviews.

==Plot== Secret Service agents everywhere she goes, including the ladies room, Samantha finally believes she has the chance to break out of her cocoon when she is given the opportunity to attend college in California.

Though still followed by Secret Service agents, Samantha at last feels as if she is leading a normal life. She ends up sharing a dorm with boy-crazed Mia (Amerie), and the two instantly strike up a begrudging partnership. In a classroom, Samantha meets James (Blucas), who doesnt treat her differently because of who her father is. Samantha feels that James is the icing on the cake of her new life at the college. But there is more to James than she initially thought, and Samantha must learn that the two sides of her life do not have to be separate from each other in order for her entire life to be content.

==Cast==
*Katie Holmes as Samantha MacKenzie, the Presidents only daughter, who leaves the White House in Washington, D.C. and heads to California.
*Marc Blucas as James Lansome, a college student who has more going on than he initially lets on. Amerie Rogers as Mia Thompson, Samanthas feisty roommate who finds it difficult when Samantha steals her spotlight.
*Michael Keaton as John MacKenzie, Samanthas father, who is also the President of the United States. He has a difficult time letting go of his only daughter. He is also on the road campaigning for his re-election.
*Margaret Colin as First Lady Melanie MacKenzie, Samanthas mother, who is more willing to let Samantha leave, but she still firmly sticks by her husbands decisions.
*Lela Rochon as Liz Pappas, the Presidents personal secretary, who is often the caught between Samantha and her father in arguments.
*Michael Milhoan as Agent Bock, one of Samanthas personal Secret Service Agents, who follows her every move.
*Dwayne Adway as Agent Dylan, Samanthas other soft-spoken personal Secret Service Agent, who along with Agent Bock, protects Samantha while she is attending school.
*Vera Wang as herself.

==Production== Rob Thomas was hired to rewrite the script.   For unknown reasons, the film was not produced at that time, although OConnell later received a "story by" credit for the film from the Writers Guild of America. (The films original film producer|producer, Mike Karz, was also credited as a producer in the final print of the film.)
 Los Angeles San Marino stood in for the exterior of the building in the first scene. 

==Reception==
The film received overwhelmingly negative reviews. It currently holds an 8% approval rating at Rotten Tomatoes based on 84 reviews (7 positive, 77 negative).  A number of viewers and reviewers pointed out that the films plot was very similar to that of the film Chasing Liberty. Indeed, Chasing Liberty s working title was First Daughter. This plot too involved the Presidents daughter trying to experience life away from the White House.

The film was a financial failure. Opening in fifth place at the box office,  First Daughter ended up with just $9.1 million in domestic ticket sales and $10.4 million worldwide.    It was Katie Holmess second least successful mainstream film after Teaching Mrs. Tingle.  The film performed better on home video and DVD, where it made $13.14 million in combined rentals and sales. 

==Notes==
 

==External links==
* 
* 
* 
*  at Metacritic

 

 
 
 
 
 
 
 
 
 
 
 
 