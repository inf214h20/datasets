Doodlebug (film)
{{Infobox film
| name           = Doodlebug
| image          = Doodlebug film screenshot.jpg
| caption        = Opening screenshot
| director       = Christopher Nolan
| producer       = Emma Thomas Christopher Nolan Steve Street
| writer         = Christopher Nolan
| starring       = Jeremy Theobald 
| cinematography = Christopher Nolan
| distributor    = N/A Cinema16
| editing        = Christopher Nolan 
| music          = David Julyan
| released       =  
| runtime        = 3 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,000 (exact amount unknown)
}}
 1997 short short psychological thriller film by Christopher Nolan. 

==Plot==
The story consists of a grungy man, in a filthy apartment. He is anxious and paranoid, trying to kill a small bug-like creature that is scurrying on his floor. revealed that the bug resembles a miniature version of himself. He squashes the bug with his shoe. But every movement the "doodlebug" makes is later matched by the grungy man himself, and he is later squashed by a larger version of himself.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 



 