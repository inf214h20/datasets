Friends and Lovers (1931 film)
{{Infobox Film
| title          = Friends and Lovers
| image          = FriendsandLoversPoster.jpg
| caption        = Theatrical poster for the film
| director       = Victor Schertzinger
| producer       = William LeBaron Wallace Smith (adaptation) Jane Murfin
| based on       =  
| starring       = Lili Damita Adolphe Menjou Laurence Olivier Eric von Stroheim
| music          = Victor Schertzinger Max Steiner
| cinematography = J. Roy Hunt William Hamilton
| distributor    = RKO Radio Pictures
| released       = 3 October 1931
| runtime        = 68 min.
| country        = United States
| language       = English 
| budget         = 
| gross          = 
}}Friends and Lovers (1931 in film|1931) is an American drama film released by RKO Radio Pictures, directed by Victor Schertzinger, and starring Adolphe Menjou, Lili Damita, Laurence Olivier, Erich von Stroheim, and Hugh Herbert.  

The film recorded a loss of $260,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p41 

==Cast==
*Adolphe Menjou as Captain Geoffrey Roberts
*Lili Damita as Alva Sangrito
*Laurence Olivier as Lieutenant Ned Nichols
*Erich Von Stroheim as Colonel Victor Sangrito
*Hugh Herbert as McNellis
*Frederick Kerr as General Thomas Armstrong
*Blanche Friderici as Lady Alice
*Vadim Uraneff as Ivanoff
*Jean Del Val as Marquis Henri De Pezanne

==Plot==
British Army captain Geoff Roberts  (Menjou) carries on an affair with Alva (Damita), the wife of the cruel Victor Sangrito (von Stroheim). Sangrito, however, is well aware of the affair, as he uses his beautiful wife to lure men into romance with her, then blackmailing them to save their careers. 

When Roberts falls into Sangritos trap, he pays the blackmail and leaves for India, hoping to forget Alva, whom he loved but now believes betrayed him. After some time in India, he is joined by his young friend and bosom companion Lt. Ned Nichols (Olivier). Nichols, too, is in love with a woman back in England -- the same woman. 

Although the two friends nearly come to blows over Alva, they eventually realize that she has been false to them both and that their friendship far outweighs their feelings for a mendacious woman. However, when the two are invalided home, they encounter Alva again, and learn that she may not have betrayed them after all.

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 

 