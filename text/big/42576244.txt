Render to Caesar
{{Infobox film
| name               = Render to Caesar
| image              = Render to caesar poster.jpg
| alt                = 
| caption            = Theatrical poster
| director           = Desmond Ovbiagele   Onyekachi Ejim 
| producer           = Nnamdi Oboli   Omoni Oboli   Desmond Ovbiagele
| writer         = Desmond Ovbiagele
| starring       =  
| music          = Michael Ogunlade
| cinematography = Johnny Askwith
| editing        = Steve Sodiya
| studio         = Mighty Man Entertainment
| distributor   = 
| released       =  
| runtime        = 120 minutes
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}
 crime thriller Trace and FCMB  tells the story of two friends who return from abroad to join the Nigeria Police Force, only to be faced with an impossible mystery case involving a criminal, Caesar (Lucky Ejim) who has been terrorising the city of Lagos for quite sometime.

Although the film received mixed critical reviews, it won awards for "Best Original Screenplay" and "Best Actor in a Supporting Role" at the 2014 Nollywood Movies Awards .  It also received awards for "Best Screenplay" and "Best Sound Design" at the 2014 Best of Nollywood Awards.  

==Cast==
*Wale Ojo as Pade
*Gbenga Akinnagbe
*Omoni Oboli as Alero
*Bimbo Manuel
*Femi Jacobs
*Onyekachi Ejim
*Dede Mabiaku
*Kalu Ikeagwu
*Chris Iheuwa
*Kehinde Bankole
*KC Ejelonu
*Steve Onu
*MC Abbey
*Yvonne Ekwere

==Release== Silverbird Galleria, Victoria Island, Lagos on 28 March 2014.   In 2015, it was shortlisted for the main feature film competition at the 24th edition of FESPACO in Ouagadougou, Burkina Faso.  It was also an Official Selection at the 2015 San Diego Black Film Festival,  as well as the 2014 Pan African Film Festival (PAFF). 

==Reception==
===Critical reception===
Render to Caesar has been met with mixed reviews. Sodas & Popcorn gave it a rating of 3 out of 5 stars, it cited that the film has plot holes, says the technicalities are flawed mostly as a result of "lazy job". It concluded by stating: "For a genre which hasn’t really been explored in the Nigerian movie scene much, it tries to be unique by bringing in witty dialogues and a good degree of continuity, until pieces of the big puzzle (which are supposed to add up) fail to do so. Render to Caesar is an experiment, which even with its many flaws, in the end is a big step forward".    Wilfred Okiche of YNaija says: "Render to Caesar wants to be many things at once. A crime thriller, a police procedural, a love story and a twisty film noir". He pointed out that although the film has its good moments, the moments are often ruined with bizarre turn of events, easy predictability or ridiculous plotholes. He added: "The special effects are not as spectacular as they could have been, the acting isn’t as fine as it should and the screenplay, with its twists and turns isn’t as impressive as it fancies itself". He concluded by saying the film seem more like an "experiment" than a "confident production".   

Wilfred Okiche commended the film for its brilliant portrayal of the Nigerian Police force. He cited Ejim as the outstanding act in the film, but also stated that there is no chemistry between Omoni and Gbenga on screen and that their romantic scenes turn out to be one of the down sides of the screenplay. 

===Accolades===
Render to Caesar received 9 nominations and won 2 awards at the 2014 Nollywood Movies Awards. It also received 3 nominations and won 2 awards at the 2014 Best of Nollywood Awards.

{| class="wikitable sortable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|- Best of Nollywood Awards   (2014 Best of Nollywood Awards) 
| Best Screenplay
| Desmond Ovbiagele
|  
|-
| Best Actor in a Leading Role
| Gbenga Akinnagbe
|  
|-
| Best Sound Design
| Michael Ogunlade
|  
|- Nollywood Movies Awards   (2014 Nollywood Movies Awards)  
| Best Director
| Desmond Ovbiagele
|  
|-
| Best Original Screenplay
| Desmond Ovbiagele
|  
|-
| Best Actor in a Leading Role
| Gbenga Akinnagbe
|  
|-
| Best Cinematography
| Johnny Askwith
|  
|-
| Best Actor in a Supporting Role
| Lucky Ejim
|  
|-
| Best Editing
| Steve Sodiya
|  
|-
| Best Sound Design
| Michael Ogunlade
|  
|-
| Best Music Soundtrack
| Michael Ogunlade & Seun Owoaje
|  
|-
| Best Rising Star (Male)
| Lucky Ejim
|  
|}


==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 