Love for Life
{{Infobox film
| name = Love for Life
| image = Love_for_Life.jpg
| caption = 
| director = Gu Changwei
| producer = Gu Changwei Chen Xiaodong
| writer = Yan Laoshi Yang Weiwei Gu Changwei  
| starring = Zhang Ziyi Aaron Kwok Pu Cunxin Jiang Wenli Cai Guoqing Li Danyang Tao Zeru Wang Baoqiang
| cinematography = Yao Tao Christopher Doyle
| distributor = 
| released =  
| runtime = 105 minutes
| country = China
| language = Mandarin
| budget = 
}}

Love for Life ( ), also known as Life Is a Miracle, Til Death Do Us Part  and Love for Live, is a 2011 film directed by Gu Changwei.  This is Gus third film as director after a lengthy career as a cinematographer for some of Chinas top directors. It was released on 10 May 2011 in China.

==Plot==
Opening narration:

"Once there was a village called Goddess Temple, high up in the mountains. 

Once there was a fever that the world called AIDS. It snuck into our village softly and everyone who got it died like falling leaves." 

Story

The peacefulness of a rural village has been disrupted by an outbreak of a disease, which the locals call a fever. Villagers learn very quickly that there is no cure for the disease and refuse to have anything to do with the infected. Lao Zhuzhu is a teacher at the now-abandoned village school and the father of Zhao Qiquan, the blood merchant responsible for causing the outbreak. He decides to make amendments to the villagers on behalf of his unrepentant son by inviting all the infected villagers, including his younger son, Zhao Deyi, to live with him at the village school, where they will look after one another. 

One day, Shang Qinqin, donning a red jacket, arrives at the school compound to join the small community. A few days later, her red jacket is stolen but no one is willing to admit to committing the theft. The villagers come to an agreement that the thief be given a chance by letting him to return the jacket later at night when everyone else has fallen asleep, but no one does so. Instead, a second theft occurs, and this time it is the Village Head who has his precious lifelong diary and some money stolen. This time, Deyi insists that everyones belongings be searched. But instead of finding the Village Heads diary and money, the villagers find Qinqins red jacket among the belongings of another infected villager, Old Bump. The Village Heads diary was never found. Soon after, Deyi goes to ask the Village Head to come out for his meal, but instead discovers that he has died on his bed, clutching the diary in his hands. 

While living at the abandoned school compound, mutual empathy grows quickly between Qinqin and Deyi, for they were both rejected by their spouses after being infected by the fever. Empathy grows into love, as they soon develop an adulterous romantic relationship while still being married to their respective spouses. One day, Qinqin and Deyi are caught in the act by Qinqins husband, Xiaohai, who was tipped off by two non-infected villagers and Xiaohai forces Qinqin to pack up and return home. 

Eventually, Qinqin is chased out and asked by her mother-in-law to return only when her husband has found a new wife so that they can proceed with a divorce. Deyi takes her in and the both of them move to a tiny cottage on the hilltop, commencing a life full of constant negativity from fellow villagers. Deyi decides that he wants to marry Qinqin and the pair kneel down and beg a reluctant Lao Zhuzhu to help by requesting Qinqins mother-in-law to grant a divorce. Xiaohai agrees, but on the condition that Deyi wills him the house that he once shared with Haoyan, his former wife. 

Deyi and Qinqin move into Deyis house while waiting for Qiquan to help them get back their marriage licences from the city government. On the day they are finally married, Qinqin, in a striking red two-piece ensemble, and Deyi, wearing a red tie carelessly-tied around his neck, strut down the streets of the village, giving out lucky candies to villagers who run from them upon sight. Their exuberant spirits are not to be dampened, and the pair continue with their walk around the village, announcing their marriage and showing off their marriage licences. 

Despite not having the blessings and approval of the people around them, Qinqin and Deyi overcome great obstacles to pursue a life together as husband and wife. 

One wintry day, Deyi succumbs to a severe fever. In a desperate attempt to ease Deyis burning sensation, Qinqin immerses herself in a tub of icy cold water. Deyi finally falls asleep with a cold Qinqin in his arms. The next morning, Deyi wakes up and weakly reaches for Qinqin, who is shown lying on the floor by the side of the bed.

==Cast==
*Zhang Ziyi as Shang Qinqin
*Aaron Kwok as Zhao Deyi
*Tao Zeru as Lao Zhuzhu (Deyis father)
*Pu Cunxin as Zhao Qiquan (Deyis elder brother)
*Cai Guoqing as Zhao Xiaohai (Deyis cousin; Qinqins husband)
*Li Danyang as Haoyan (Deyis wife)
*Sun Haiying as Si Lun, the Village Head
*Wang Baoqiang as Big Mouth
*Jiang Wenli as Cook Lady
*Hu Zetao as Zhao Xiaoxin (narrator; Qiquans son)

==Background==
HIV/AIDS related subject matters are rarely touched upon in China, and always thought to be a taboo. This film is acclaimed to be able to make a change. Backed by the government, the film is said to be the first time the Plasma Economy|blood-selling scandal of the 1990s has been featured in mainstream Chinese culture. 

==References==
 

==External links==
* 
*   at the Chinese Movie Database

 

 
 
 
 