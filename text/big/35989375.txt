Aluna
{{Infobox film
| name           = Aluna
| image          = 2012 Film Poster of Aluna.jpg
| image_size     = 220
| alt            =
| caption        = Theatrical release poster
| director       = Alan Ereira
| producer       = Alan Ereira
| starring       = {{Plainlist|
* Alan Ereira 
* Mama Manuel Coronado
* Mama Shibulata Zarabata
* Francisca Zarabata
}}
| music          = Alejandro Ramirez Rojas
| cinematography = Paulo Andrés Pérez
| editing        = Andrew Philip
| studio         = Sunstone Films
| distributor    = 
| released       =   
| runtime        = 91 minutes
| country        = United Kingdom Colombia Kogi (English Subtitles)
| budget         = £899,000 (estimated) 
| gross          = 
}}

  Kogi tribe modern world Kogi retreat back to civilisation hidden in a mountain in Sierra Nevada de Santa Marta, Colombia.  
 Kogis have re-emerged. Long-ago realising that the importance of their warning had not been grasped.  As well as warning Younger Brother they have decided to share their secret sciences in the belief that sharing these new sciences will share their burden of changing the world for the better. 

==Content summary== Kogis choose Kogis ask Kogi Mamos proactive and decide that there will be "no more secrets". They wanted to demonstrate their planetary healing sciences in front of the cameras to demonstrate their secret science to the modern world and to show visible and measurable results.  They also wish to teach other people how to conduct these sciences in order to heal the world and they spoke with modern scientists as they feel that they have a different science to show them. 

===Kogi demonstration=== modern world of the importance preserving the planet, the Kogi give a demonstration of the interconnectivity of the planet. Which the Kogis claim will produce visible and measurable results.   Kogis travel to London|London, England to fetch 400 kilometres (250 miles) of gold thread, reportedly the longest ever made.  
 Kogis Mamos Professor Richard Ellis who is astounded by the Kogi Mamas knowledge of both our own solar system and recent astronomical discoveries such as dark energy. 
 Mamos may be at the cutting edge 
 estuaries feeds source of interconnectedness of everything on the earth and this was the key element in what they were doing. The theme of what they were doing was showing that the earth itself is a living body in which everything is interconnected and damage to some of it is damage to all of it."— Alan Ereira.  

==Participation at UN summits==
The 1990 documentary   was shown at the Earth Summit in Rio de Janeiro, Brazil in 1992 and was seen to inspire delegates to take action. 
 Kogi Mamos shall be attending the conference the Rio conference could again be inspired by the Kogis.

==Production== The Heart of the World. 
 Kogis had their own indigenous film crew this gave the film the advantage that previously unfilmed holy sites and practices which modern film crews were restricted from seeing could now be filmed.  Throughout the collaboration the Kogis exercised their own autonomy with the films Film director|director, Alan Ereira, looking forward to see what the indigenous film crew had recorded. The Mamos also took charge of what was going to be filmed as consulted Aluna through divinations. 
 Sound Recordist to work with their indigenous counterparts. 

==Participation in festivals== Sheffield International Documentary Festival where it would have its World premiere.  

==References==
 

==External links==
*http://www.alunathemovie.com/en Official website of the second and final Kogi Mamos documentary
*http://www.taironatrust.org Information about the Kogis in South America
*  2011 video report by Alan Ereira after filming in Colombia
*  

 
 
 
 
 
 
 
 
 
 