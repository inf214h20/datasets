Truth Be Told (2012 film)
 
{{Infobox film
| name           = Truth Be Told
| image          = File:This is a promotional poster for the documentary film Truth Be Told. The poster art copyright belongs to the production company of the film, Smithkraft Productions, LLC.jpg
| caption        = Theatrical poster
| director       = Gregorio Smith
| producer       = Gregorio Smith   Mark Mahler
| studio         = Smithkraft Productions, LLC
| Music          = George Ilijin
| runtime        = 86 minutes
| country        = United States}}
 documentary about growing up in the Jehovahs Witnesses religion.  The title refers to the Jehovahs Witnesses’ perception that their beliefs are the truth.

== Overview ==

Truth Be Told focuses on seven individuals raised in the Jehovahs Witnesses religion.  In a series of informal interviews they reveal experiences including the effects of proselytizing door-to-door, shunning non-observant family and friends, suffering the discouragement of pursuing dreams like gaining a higher education and missing other societal holidays and customs. The film cuts between talking-head interviews and visual storytelling techniques including dramatic reenactments, motion graphic sequences and montages.  Actors were filmed in front of a greenscreen and composited to create a virtual Kingdom Hall, the house of worship used by Jehovahs Witnesses.

The documentary features an interview with retired mixed martial arts fighter and television personality Nathan Quarry.
Quarry grew up as a member of the Jehovahs Witnesses in a variously oppressive and controlling environment. After a period of self-discovery, Quarry rejected his Jehovahs Witnesses upbringing, which caused him to become alienated from his family and former friends.

Truth Be Told is described as an exposé of the Jehovahs Witnesses religion that discloses a profit-driven, isolationist culture characterized by fear, totalitarian corporate leadership, intellectual & spiritual intimidation, suspension of critical thinking, failed prophecies, doctrinal inconsistency and improper handling of physical and sexual abuse allegations within the church. 

The documentary is the first feature-film directed by Gregorio Smith.  He describes his film as an honest glimpse into the culture of growing up in the Jehovah’s Witnesses religion that is at once immersive, informational, expository and controversial.
 

== References ==
 

== External links ==
*  
*   at the Internet Movie Database
*  

 
 
 
 
 