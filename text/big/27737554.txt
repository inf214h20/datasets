Uninvited Guest
{{Infobox film
|
| image = Uninvited Guest poster.jpg
| caption = 
| director = Timothy Wayne Folsome 
| writer = Timothy Wayne Folsome
| starring = Mekhi Phifer Mari Morrow Malinda Williams Mel Jackson Wanya Morris
| producer =  Kevin D. Hightower Larry Spud Raymond Harold Folsome William Hightower Keith Hightower  
| distributor = Trimark Pictures  Astro Distribution(Germany)
| budget = 
| gross = $170,832
| released = September 22, 2000 min
| language = English 
}}
Uninvited Guest is a 2000 thriller, written and directed by Timothy Wayne Folsome. The film stars Mekhi Phifer, Mari Morrow, and Mel Jackson. The film was released September 22, 2000.

==Summary==
Debbie (Mari Morrow) and Howard (Mel Jackson) are celebrating their wedding anniversary. Or so Debbie thought. A special night together turns into a nightmare when Howard lets a man named Silk (Mekhi Phifer) into the house to use the phone, and besides the car trouble that brought him to the door, he brings along a string of frightening murders, Debbie then finds out that her husband and best friend set a hit on her, just to earn a lot of money. but all of that backfires on everyone, especially Debbie, who gets shot by her own husband.

Tagline: Hes the reason you never talk to strangers

==Plot==
The movie begins with a group of children and teens playing, until they see someone walk past them (leaving them with an eerie feeling). The mysterious stranger Silk (Mekhi Phifer) walks into the store and exposes his gun (signalling a robbery), Mecca (Kim Fields) berates him for being a criminal, but before she can finish, the man angrily draws his gun and gives her to the count of 3 to give him the money, she submits before he got to the 3rd number, he was distracted by a passing truck and shoots her when he saw her pulled out a shotgun, knowing someone heard the gunfire, Silk quickly fled the area. While away from the premises, he was disappointed in the low amount of money. Shortly after, Silk hears something and readies his gun pointing it at an unseen person exclaiming "who the f**k are you".

The scene opens with Debbie (Mari Morrow) who has just finished reading one of Howards scripts and praises it, she gets ready to celebrate her anniversary with her husband, the door opens as Howard (Mel Jackson), Mo (Wanya Morris), and Tre (William Christopher-Stephens) arrived to see her nude (much to everyones surprise and Debbies dismay), she became upset that her husband would work on his script rather than celebrating their anniversary, Debbie then leaves to go visit her friend Tammy (Malinda Williams). Meanwhile, Howard, Mo, and Tre were outside when they heard a knock on their door, Tre opens the door and sees a stranger who claims that his car broke down the road and he needs to use their phone, they brought him in the house.

Taking a break from the script, the guys and Silk play a poker game, Debbie returned along with Tammy to get ready to go to the mall, but Tammy immediately becomes infatuated with the stranger. Afterwards, a few games of poker, Silk openly flirts with Debbie. He compliments her and her paintings in the hallway (reminiscing her childhood with her late father), and while she injured her leg in the kitchen, as he sat her down on the counter and massaged her foot, she was unknowingly being seduced by Silk and it appears he is giving her oral sex, unbeknownst to her trance. Soon after Tammy walks in and Silk had disappeared, Tami sees Debbies panties on the floor and storms off after lashing out at Debbie.

After a long day, both Mo and Tre are exhausted and ready to go home, though Howard wants to finish the script, they then get a ring on the door bell, and its an officer looking for an escaped convict, (which turns out to be the stranger they let in earlier) named Andrew Anderson (Silks real name), who was serving a life sentence for first-degree murder, the three realizes the situation, until Silk ambushes the cop from behind, while pointing his gun at them, holding them hostage in the basement. Meanwhile Debbie takes a bath, and shortly after realizes that Silk was watching her, demanding to know why hes still here and whereabouts of her husband, Debbie goes outside to find a letter on the table (which reads "out for beer"), Silk offers to take Debbie out to go find them on Howards motorcycle, but ends up going to an art museum.

Still trying to search for Howard, Debbie ends up being swayed by Silk about their thoughts on the paintings, they later returned back to the house where Debbie finds Silk in the Jacuzzi trying to seduce her, he drags her in to have sex with her, resistant at first, she gives in to her lustful desires, until she spots Howard looking out the window bound and gagged, she immediately leaves the hot tub and goes into the living room, which Silk follows with his gun (revealing himself as a criminal), he commands Debbie to go downstairs, whilst finding Mo, Tre, and the unconscious cop there as well. Silk declares his love for Debbie and wants her to leave the country with him, she rejects his offer, disappointed by the response, he readied his gun to shoot the cops body, which left everyone around Silk horrified, he ties her to the chair while having a conversation with Howard, taunting him about the affair (even going as far as to joke and laugh about it), he then leaves to hide the officers body and get rid of the police car, Howard felt outraged at his wifes infidelity, the two then argued about the fate of their marriage.

Silk returns, ungagging Mo and Tre as well, Howard decides to let Silk take her, and when Silk asks for Mos opinion, Silk smacks and warns him to not insult "his woman" again. After several verbal exchanges between them, Silk decided that Mo would be the first to die, before leaving the basement with Debbie. Mo knowing that they were likely going to be killed, he tells Howard and Tre about the mistakes he made in his life, he thought that Howards script was a chance to redeem himself, he referred the two as his good friends. Upstairs Silk converses with Debbie about their future together, Debbie retorts about them spending the rest of their lives eluding the law, to which he replied as "exciting". She distracts him by making her a glass of water, while she calls the police, he responds that he already disconnected the phone, so she decides to physically escape, with Silk chasing after her, he catches up to her at the front door, and puts her up against the wall, he forces her downstairs, killing Mo and threatened to kill Howard next if she ever ran off again, Silk calms down after she complied by going upstairs to pack.

In the basement, Howard and Tre conversed about their fate, Silk and Debbie arrived downstairs they get a ring on the doorbell. Howard opens the door with Tammy wanting to speak to Debbie, unconvinced when he tells her that she wasnt home, she also criticized him for the way he treats her (she also asks about the $10,000 to which he admits that he lost by gambling). She attempted to make her way in, but hears a strange noise (which shows Silk holding Debbie at gunpoint, then pointing it at Howard), he says it was probably Tre, Tammy leaves asking for Debbie to call her when she got home. Pleased with the performance, the three go back downstairs, tying the two back up, he wonders on what to do with the remaining two men, their promise not to say anything fell on deaf ears, he then asks Tre on how he wanted to die, before Howard scolds Silk. Howard desperately bets Silk against Tre, saying that if Tre wins theyd go free, and if Silk won theyre dead, Silk agrees to the bet and the two fought, he easily beats Tre, as Silk goads Howard, Tre recovers and ambushes Silk from behind with a piece of wood, causing the latter to fall and lose hold of his gun while being attacked, a curious Tammy rushes downstairs to find Silk on the floor, she defends him from Tre. Recovering his gun, Silk gets up and prepares to shoot Tre, a frightened Tammy asks was Mo under the sheet, Silk admits to killing him, Tre berates Silk for it, he then tells Silk to just shoot him already, in which he happily does, shocking those around him, and shoots him a second time guessing that "faggots just dont die easy".

After the situation Silk ungags Debbie to comfort her, she replies that she hates him, he then decides to ungag Howard for his advice, who was outraged by Tre"s death and made death threats towards an unfazed Silk. Tammy took that opportunity to escape, forcing Silk to chase after her, he caught up to her and throw her back in house, he followed her after she ran up the stairs (while telling Debbie not to try anything or else her friend dies) Debbie and Howard formulate a plan for the latter to get the police while she helps Tammy, Silk opens the door to one of the rooms, he turns the lights on while looking around for her, easily concluding that she was hiding under the bed, he proceeded to give her the count of 3 to come out, she gives in and they go back downstairs, Debbie takes him by surprise (holding a kitchen knife to his neck) and Tammy gets hold of his gun. The two then forced him into the basement and tying him up, the two women ponder on what to do with him before finally coming to a decision by castrating him, minutes later Howard arrived down to tell them he called the police and he came to assist them, he asks for Tammy give him the gun, while their backs are turned, he frees Silk and reveals himself as the mastermind behind everything that happened. Confused, Debbie demands to know his motives, Howard explains that after his previous scripts failed, he decided to make one based on a true story. He also admits that he hired Silk (that unseen person Silk met earlier in the movie was Howard) to kill Mo and Tre for the sole purpose of promoting his new script, he exclaims them as "gullible and stupid" for falling for his earlier $3 million script, Debbie asks if he planned on having her killed as well, he simply nods with indifference, infuriating her. Howard explains the sacrifices of his plan and that he still had the $10-thousand, giving Silk $5000 and promising him the other half before tying the women up.

The scene shifts to Howard in the kitchen waiting for Silk to show up, asking Silk for his whereabouts with his motorcycle, the latter retorted that he was just out having "fun", Howard retorts "fun" like: killing a cop, shoving his gun in his mouth, all before sarcastically stating that the real joy was about the affair earlier.  Howard and Silk are then having a discussion about near success of the plan. Silk becomes angry at Howards goading and puts him against the wall, pulls his gun out demanding knowledge about the other half of his money, Howard responds that he would have to finish the plan to get it, Silk leaves the kitchen after calming down. The scene shifts to Debbie and Tammy downstairs, the latter attempts to flatter Howard, but Howard ends up laughing it off.

As Silk comes back into house, Howard wonders the formers whereabouts, Silk replies that he had to make a "run", he decides to finish the final phase of the plan, Silk questions his plan and losing a woman like Debbie for the sake of its success, convinced of its success he tells Silk to just finish it, Silk simply shrugs and agrees. Downstairs Silk readies his gun to kill them, Debbie tells Silk that she done some thinking, since her husband no longer cares about her and that shes a free woman, she decides to be with Silk, wanting to know if she meant it, Silk wants Debbie to give him a "sneak preview" of what to expect, she convinced him to go upstairs. While upstairs Debbie appears to perform oral sex on Silk, but it was only just a ruse by kneeing him in the groin, smacking him with a telephone (he drops the gun), she tries to obtain the weapon while fighting off Silk, however she managed to knock him back while accidentally pulling the trigger, shooting him in the stomach, which caused Silk to pass out and fall to the floor, Debbie gets to the side of the bed.

Howard enters the room to see if Silk done the job, only to find that it was the other way around with the gun now in Debbies possession. The two conversed, Moments later she decided it was time for him to go to jail, much to his protest. When Tammy arrived, he swipes the gun and he tells her that hes not going to jail, demanding them to get away from the door, an injured Silk gets up and asks for his gun back, but Howard says that hes in control. Howard then explained that they could tell the authorities that Silk was responsible for the murders, seconds later, it was revealed Tammy was in on the plan as well, feeling betrayed by both her husband and best friends avarice. Howard intervenes, asking if they came to a decision, Silk watches on throughout the situation. Silk attempted to hold Howards arm down for Debbie to escape, but the latter breaks free and accidentally shoots her, much to everyones shock. The scene then shifts to the afterlife with Debbies dad painting until she appears walking towards him. It then shifts back to Howard and the others, Silk crawls towards her, but Howard warned him to get away from her, Silk simply replied "no loose ends", Howard then shoots him in the head, ending the convicts life.

At the art museum the tour guide explains to the tourists that the painting was by a father and daughter and that they call it "Life". The scene shifts to the people and paramedics outside, news truck, and the medics uncovering the sheets that covered Mo and Tres bodies, before finally shifting it to Howard, Tammy, and the homicide detective, Howard explains that Silk had killed both Mo and Tre, they make-up the rest of the story about Silk killing Debbie and it was Tammy who saved her and Howards life, The Detective proclaimed her as a hero. He leaves and they momentarily express their victory (albeit bittersweet) about their success. Meanwhile upstairs the paramedics take Debbies corpse and a photographer takes a picture of Silk, the detective kneels down while looking at the fugitives lifeless body, a member of the investigation team picks up a camera to show the detective that its still recording (showing a video of Debbie holding the gun, looking down at the recorder), he then gets up and says "play it back".

==Cast==
 
{| class="wikitable"
|-
! Actors/Actresses !! Role
|-
| Mekhi Phifer || Silk/Andrew Anderson
|-
| Mari Morrow || Debbie
|-
| Malinda Williams || Tammy
|-
| Mel Jackson || Howard
|-
| Wanya Morris || Mo
|-
| William-Christopher Stephens || Tre
|-
| Kim Fields || Mecca
|-
|}

==Production==
The film was produced in Columbus, Reynoldsburg, and Youngstown Ohio  the artwork featured in the film was by Robert S. Wright, a central Ohio artist.

==References==
 

==External links==
*   at Internet Movie Database

 