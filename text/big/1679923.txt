The Deadly Mantis
 
{{Infobox film
| name = The Deadly Mantis
| image = Deadlymantis.JPG
| caption = Film poster by Reynold Brown
| director = Nathan H. Juran 
| producer = William Alland
| writer = William Alland, Martin Berkeley Craig Stevens William Hopper Alix Talton Pat Conway
| music = Irving Gerts, William Lava
| cinematography = Ellis W. Carter
| editing = Chester Schaeffer
| distributor = Universal Studios|Universal-International
| released =  
| runtime = 79 min
| country = United States
| language = English
| budget =
}}
 Craig Stevens, William Hopper, Alix Talton, and Pat Conway. It was filmed in black and white and runs for 1 hour and 19 minutes.

In February 1997, The Deadly Mantis was featured as an episode of Mystery Science Theater 3000.

==Plot synopsis==
In the South Seas, a volcano explodes, eventually causing North Pole icebergs to shift. Below the melting polar ice caps, a 200-foot-long praying mantis, trapped in the ice for millions of years, begins to stir.  Soon after, the military personnel at Red Eagle One, a military station in northern Canada that monitors information gathered from the Distant Early Warning Line, realize that the men at one of their outposts are not responding to calls.  Commanding officer Col. Joe Parkman flies there to investigate, and finds the post destroyed, its men disappeared and giant slashes left in the snow outside.

When a blip on the outposts radar screen is soon sighted, Joe sends his pilots out, and one is attacked.  Joe searches the wreckage, and this time, in addition to the huge slashes, finds a five-foot-long pointed object in the snow.  He takes it to General Mark Ford at the Continental Air Defense Command in Colorado Springs, Colorado.  Ford gathers top scientists, including Professor Anton Gunther, to examine the object, but after they fail to identify it, Gunther recommends calling in Dr. Nedrick Jackson, a paleontologist at the Museum of Natural History.

When Ned receives the call from Ford, he is helping museum magazine editor Marge Blaine plan her next issue, and later dodges her questions as she begs him for a big scoop.  Later, after examining the object, Ned recognizes it as a torn-off spur from an insects leg, and soon guesses, from evidence that the creature ate human flesh, that it must be a gigantic praying mantis.  Meanwhile, in the Arctic, the people of an Eskimo village spot the mantis in the sky, and although they hurry to their boats to escape, it swoops down and kills several men.

Ned is sent to Red Eagle One to investigate further, and upon leaving, discovers that Marge has finagled permission to accompany him as his photographer.  They reach the base, where all the men, including Joe, are smitten by Marges beauty.  That night, Marge and Joe join Ned in his office and discuss the creature, not realizing that it is drawing close to the office window. Marge suddenly catches sight of it and screams, and the bug attacks the building.  Although the full unit opens fire on the mantis with automatic rifles and a flame-thrower, it is unscathed and moves away only after planes encircle it.

Hours later, the base remains on red alert, but they finally hear that the bug has attacked a boat off the Canadian coast, which means, Ned calculates, that it is flying at a speed of 200 miles an hour. Ford calls a press conference to announce the bugs existence, and asks the Ground Observer Corps to track its whereabouts.  Over the next few days, Ned, Marge and Joe tirelessly track the bugs progress, with the help of military and civilian observers.  Late one night, Joe drives Marge home, stopping briefly to ask for, and receive, a kiss.  They are distracted by a report of a nearby train wreck, and although they assume it to be an ordinary accident, soon after, a woman leaving a bus sees the mantis, and all emergency personnel are put on alert.  The mantis is then sighted in Washington, D.C., atop the Washington Monument.

Joe is one of the pilots who bravely attempt to drive the bug toward the sea, but a dense fog throws him off course, and he flies directly into the mantis.  As the wounded mantis drops to the ground and crawls into the Manhattan Tunnel, Joe safely parachutes to the ground.  Ford heads a team that seals off the tunnel, filling it with smoke to provide cover for Joe and his special unit of men, who enter the tunnel armed with rifles and three chemical bombs.  They creep past wrecked cars until suddenly the bug appears in the fog only a few yards ahead of them.  They shoot at it, but it lumbers on, forcing them backward.  The mantis seems immune to the ammunition and the first chemical bombs until, only feet from the tunnel entrance, Joe throws a bomb in its face, and it collapses, dead.
 autonomic reflex, Joe takes the opportunity to pull Marge into an embrace.

==Cast== Craig Stevens as Col. Joe Parkman
* William Hopper as Dr. Nedrick Ned Jackson
* Alix Talton as Marge Blaine
* Donald Randolph as Gen. Mark Ford
* Pat Conway as Sgt. Pete Allen
*   as Prof. Anton Gunther Paul Smith as Corporal, Parkmans Clerk
* Phil Harvey as Lou, Radar Man
* Floyd Simmons as Army Sergeant Paul Campbell as Lt. Fred Pizar
* Helen Jay as Mrs. Farley

==Reception== IMDb and has a current rating of 38% on review aggregator website Rotten Tomatoes.  The website Atomic Monsters looked at the film in a somewhat positive light, giving it a "radioactive rating of 5 atomic blasts out of 5". 

==References==
 
*  

==External links==
*  
*   at Rotten Tomatoes
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 