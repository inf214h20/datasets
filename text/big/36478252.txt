Miss Nobody (2010 film)
 
{{Infobox film 
| name           = Miss Nobody
| image          = Miss Nobody.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Tim Cox|T. Abram Cox
| producer       = 
| writer         = Doug Steinberg
| starring       = Leslie Bibb Adam Goldberg Brandon Routh Kathy Baker
| music          = 
| cinematography =
| editing        = 
| distributor    = 
| released       = 
| runtime        = 90 minutes
| country        = United States
| language       = English
| gross          = 
}}
Miss Nobody is a 2010 American film.

==Plot==
Sarah Jane McKinney (Leslie Bibb) has been working as a secretary at Judge Pharmaceuticals for years, and shes eager to earn better pay and more prestige. As a young girl, her alcoholic father was killed by a statue of Saint George that fell off a church. Ever since, she has looked to St. George as her guardian angel. When a junior executive position opens up at Judge, Sarahs friend Charmaine (Missi Pyle) encourages Sarah to join her in applying for the job. Sarahs mother Claire (Kathy Baker) helps her forge her resume, and Sarah ends up winning the job. 

On her first full day of work, Sarah arrives at her office to find it being packed up by Milo Beeber (Brandon Routh). He explains that the Human Resources manager who promoted Sarah got fired, and Milo transferred in from corporate headquarters. Sarah consigns herself to being Milos secretary. Charmaine coaches Sarah to go along with Milos inevitable sexual advances as a way to get ahead. After a working dinner, Sarah goes back to Milos apartment. She is horrified to find out that he is engaged and tries to ward off his advances. Milo chases her up a portable ladder. When Sarah shoves him away, Milo falls backwards and is impaled on an umbrella. 

Sarah does not report his death and is promoted to Milos position. Another executive at Judge, Nan Wilder (Vivica A. Fox) confronts Sarah about Milos death. Milo had texted Nan a picture of Sarah at his apartment. Knowing that Nan could report her to the police, Sarah decides to push her in front of an oncoming subway train. She soon receives a faxed picture showing her standing behind Nan on the subway platform. Believing that fax came from Pierre JeJeune (Patrick Fischler), Nan electrocutes him by activating the offices sprinkler system while Pierre is Xeroxing his butt in a kinky sex game. 

Promoted into Pierres position, Judge sends in a consultant, Morty Wickham (David Anthony Higgins), to help Sarah get a handle on her new job. Morty was Pierres protege, and when he embarrasses Sarah at a corporate event, she decides to kill him. She severs the gas line to Mortys oven, and when he tries to light a bong, his house explodes. The string of deaths at Judge are being investigated by Detective Sergeant Bill Malloy (Adam Goldberg) who is also boarding at Sarah and Claires house. Sarah and Malloy have begun dating, but she worries that he might be closing in on her.

She decides that she needs a fall guy and settles for Joshua Nether (Eddie Jemison). She seduces Joshua and gains access to his personal information, which she uses to set up dummy accounts. Sarah embezzles money from Judge through the dummy accounts, setting everything up to look like Joshua is to blame. Malloy happens to see Sarah and Joshua leaving a restaurant after one of their dates and is heartbroken. When Malloy confronts Sarah about her betrayal, she thinks he is referring to the murders she has committed. She realizes he is referring to Joshua, and they reconcile. 

After she poisons Joshua, Sarah is blackmailed by someone who claims that she knows about the embezzled money. Sarah quickly plots out who could be extorting her and various ways to kill them. She even poisons one co-workers bottled water, just in case. It turns out that Charmaine is blackmailing Sarah. She was jealous of Sarahs promotion, and she was secretly behind most of the events that led Sarah to commit the series of murders. During their confrontation, Charmaine ends up chasing Sarah to the top of a bell tower, where Sarah causes Charmaine to fall to her death. 

Thinking her troubles are behind her, Sarah gets engaged to Malloy. At work, she asks her assistant for a glass of water while she writes him a letter of recommendation. As she sips the water, her assistant explains that he had to get the water from her co-workers office. Realizing that it is the water that Sarah had poisoned, she concludes that she got what she deserved.

==Cast==
* Leslie Bibb as Sarah Jane McKinney
* Missi Pyle as Charmaine
* Kathy Baker as Claire McKinney
* Brandon Routh as Milo Beeber
* Adam Goldberg as Bill Malloy
* Paula Marshall as Cynthia Bardo

==External links==
* 

 
 
 
 


 