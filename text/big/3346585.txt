In Cold Blood (film)
 
{{Infobox film
| name           = In Cold Blood 
| image          = In cold blood99.jpg
| caption        = original U.S. poster
| director       = Richard Brooks 
| producer       = Richard Brooks
| screenplay     = Richard Brooks
| based on       =   Robert Blake Scott Wilson Paul Stewart
| music          = Quincy Jones
| cinematography = Conrad Hall
| editing        = Peter Zinner
| studio         = Pax Enterprises, Inc.
| distributor    = Columbia Pictures
| released       =  
| runtime        = 135 minutes
| country        = United States
| language       = English
| budget         = $3.5 million
| gross          = $13,000,000 
}} Original Score, Adapted Screenplay.
 Garden City and Holcomb, Kansas; Kansas State Penitentiary, where Smith and Hickock  were executed; and the Clutter residence, where the murders took place.

In 2008, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot== Perry Smith Robert Blake) Scott Wilson) concoct a plan to invade the home of the Clutter family, as Mr. Clutter supposedly keeps a large supply of cash on hand in a safe. While the two criminals feel their plan for the robbery is sound, it quickly unravels, resulting in the murders of Mr. and Mrs. Clutter and their two teenage children. The bodies are discovered the next day, and a police investigation is immediately launched. As the investigation builds, the two wanted men continue to elude law enforcement by heading south and crossing into Mexico; but, after a while, they return to the U.S. and decide to travel to Las Vegas to win some money at gambling. There, they are arrested for violating parole, being in possession of a stolen car, and passing bad checks.

The police separately interrogate the two men about the Clutter murders. Both Smith and Hickock admit to passing bad checks, but they deny knowing anything about the murders. Next, the police confront them with evidence, such as a bloody footprint matching the boots worn by one of the men ("You boys signed your own work!"); but they are slowed by Smiths refusal to provide answers. The police claim that another mistake made by the men is that they left a witness. Finally, Hickock confesses and states that he does not want to be executed for the crime, claiming that Smith committed all of the murders. When Smith learns that Hickock confessed, he recounts how, although it was he, Smith, who wielded the knife and pulled the trigger for the four killings, Hickock was there beside him as an active accomplice.

Smith and Hickock are both found guilty of the crime and sentenced to be hanging|hanged. A representation of their final moments and their execution is presented at the conclusion of the film.

The story of the murders is told in flashback, or analepsis, after the subjects arrests.

==Cast== Robert Blake as Perry Smith Scott Wilson as Dick Hickock
*John Forsythe as Alvin Dewey Paul Stewart as Jensen, the reporter
*Gerald S. OLoughlin as Harold Nye
*Jeff Corey as Dicks father
*John Gallaudet as Roy Church
*James Flavin as Clarence Duntz
*Charles McGraw as Perrys father
*Jim Lantz as Officer Rohleder
*Will Geer as Prosecuting attorney
*John McLiam as Herbert Clutter
*Paul Hough as Kenyon Clutter
*Ruth Storey as Bonnie Clutter
*Brenda C. Currin as Nancy Clutter
*Donald Sollars as Clothing Salesman

==Response== Scott Wilson Robert Blake as the killers. Another thing that helped the film was the use of black-and-white photography to heighten the tension, giving it the "you are there" touch. Brooks added to the films authenticity by filming in the actual locations, including the Kansas State Penitentiary, where the executions of Smith and Hickock took place. 
 Best Director, Best Cinematography, Best Original Best Adapted Screenplay. At the time of its release, it was rated "For Mature Audiences", which meant no children under 17 were allowed to see the film without parents or legal guardians of age; now the MPAA has rated the film "R", due to its violence and mature nature. In the 54 Best Legal Films of all-time,  In Cold Blood received three votes. 

;American Film Institute Lists:
*AFIs 100 Years...100 Movies - Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Perry Smith & Dick Hickock - Nominated Villains 
*AFIs 100 Years of Film Scores - Nominated 
*AFIs 10 Top 10 - #8 Courtroom Drama

==Television remake==
  1996 miniseries was also made based on the book, directed by Jonathan Kaplan and with a screenplay by Benedict Fitzgerald. In that adaptation, Anthony Edwards portrayed Dick Hickock, Eric Roberts played Perry Smith, and Sam Neill played Kansas Bureau of Investigation detective Alvin Dewey. 

==See also==
*In Cold Blood (miniseries), the TV miniseries adaptation of In Cold Blood.
*Capote (film)|Capote a 2005 film about Capotes researching and writing of In Cold Blood.
*Infamous (film) a 2006 film covering the same time period in Capotes life.

== References ==
 

== External links ==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 