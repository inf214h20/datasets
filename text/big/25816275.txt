Onibi (film)
{{Infobox film
| name           = Onibi
| image          = 
| caption        = 
| film name =  
| director       = Rokuro Mochizuki
| producer       = Yoshinori Chiba Toshiki Kimura
| writer         = Toshiyuki Morioka
| starring       = Yoshio Harada Reiko Kataoka
| music          = Kenichi Kamio
| cinematography = Naosuke Imaizumi
| editing        = Yasushi Shimamura
| studio         = 
| distributor    = Gaga Communications
| released       =  
| runtime        = 101 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film directed by Rokuro Mochizuki.  

== Cast ==
*Yoshio Harada - Noriyuki Kunihiro
*Reiko Kataoka - Asako Hino
*Show Aikawa - Naoto Tanigawa
*Kazuki Kitamura - Hideyuki Sakata 
*Ryushi Mizukami - Hanamura
*Hiroyuki Tsunekawa - Satoshi Fujima
*Ryuji Yamamoto - Hiroshi Fujima
*Yoshiaki Fujita - Kinjo
*Ei Kawakami - Yoshida
*Toshihiro Kinomoto - Aoki
*Seiroku Nakazawa - Nagashima
*Masai Ikenaga - Kizaki
*Eiji Minakata - Kanigawa
*Hajime Yamazaki - Driving School Teacher
*Eiji Okuda - Myojin

==Awards and nominations==
19th Yokohama Film Festival 
* Won: Best Film
* Won: Best Director - Rokuro Mochizuki
* Won: Best Actor - Yoshio Harada
* Won: Best Supporting Actress - Reiko Kataoka

==References==
 

== External links ==
* 
*   at   (in Japanese)
*   at   (in Japanese)

 

 
 
 
 
 


 