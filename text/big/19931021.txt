Kaal (2007 film)
{{Infobox film
| name = kaal
| image =Kaal Pk.jpg
| image_size     =
| caption        = Dvd cover of kaal 
| director = Bappaditya Bandopadhyay
| writer =Shakti Barthwal
| starring = Chandrayee Ghosh  Rudranil Ghosh
| producer = Srishti Productions
| distributor =  
| cinematography =Rana Dasgupta
| editing =        
| released = 6 January 2007    
| country        = India
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| runtime = 118 Minutes Bengali
| music =Abhijit Bose
}} Bengali  film released in 2007 in film|2007. The film directed by Bappaditya Bandopadhyay stars Chandrayee Ghosh, Rudranil Ghosh, Dola Chakraborty, Sandhya Shetty.

It was selected for screening at the Cairo International Film Festival, 2007, the Sao Paulo International Film Festival 2007 and the Stockholm International Film Festival 2007. Incidentally, this is the third successive year that Bandopadhyays film is being screened at Sao Paulo International Film Festival. The film was already successfully screened at the Osians Cinefan Festival of Asian and Arab Cinema, 2007, 
{{cite web
|url=http://in.movies.yahoo.com/news-detail/11195/Bappadityas-KAAL-selected-at-Cairo-Film-Fest.html
|title=Bappadityas KAAL selected at the Cairo Film Fest |work= Yahoo! India Movies
|accessdate=2008-10-25
|last=
|first=
}}
  where it was one of the Indian films competing in the festival. 
{{cite web url = http://220.226.203.134/filmhouse/schedules/dfilms.php?secid=4 title = 9th Osians Cinefan - Festival sections: Indian Films in competition publisher = Osians 2007 accessdate = 2008-11-03
}}
 

==Plot==
KAAL is about four women who are trapped in the world of human trafficking because of poverty. They move from a rural area into a large citya fter being abducted in various ways by the local middleman Ratan (Rudranil Ghosh). They ultimately become call-girls for the sake of survival. Ranidi (the boss, played by Sandhya Shetty) grooms the depressed and oppressed girls and gives them a complete make over in a short time.

The movie depicts the corruption associated with their entire business. The girls accept the fact that money is better than sunshine and sweeter than honey. They forget their miserable past. They get caught up in making more money and buying more fun. They understand that surviving in this treacherous world is not possible through love, honesty and humanity. Hence they let themselves drain away in the world of consuming and consumption. The girls are shown traveling to the top hotels in Kolkata. The middleman Ratan is shown trying to go to Singapore. The village police inspector and the other partners in the game(Indian Border Security Force) continue to operate.

The movie concludes on a negative note. It depicts the basic reality of how simple girls from villages all over India are being stolen, sold and used in the open market of sex and lust in metropolitan cities. The movie shows a never-ending oppression of women and decay of human values.

==Director’s note==
 
"In a way, this film is a sequel to my earlier film Kantatar (Barbed Wire). The partition, at one point of time, caused human displacement in a large scale in our country. The present trend of globalization has once again unleashed a massive displacement of human beings. The backdrop is also even more complex. It was therefore impossible to maintain a linear narration. Kaal is a multilayered film dealing with human displacement." 
{{cite web
|url=http://kaalthefilm.tripod.com/index.html
|title=KAAL
|publisher=kaalthefilm.tripod.com
|accessdate=2008-10-25
|last=
|first=
}}
 
 

== Cast ==
*Chandrayee Ghosh...
*Rudranil Ghosh....Ratan
*Dola Chakraborty ...
*Sandhya Shetty...
*Sankar Debnath...
*Samapika Debnath...
*Rupsa Guha...
*Nemai Ghosh...
*Iqbal Sultan....
*Mishar Ray...
*Partha Sarathi Deb...

==Crew==
*Director: Bappaditya Bandopadhyay
*Producer: Bhaidani Films Abdulla Vaidani, Manik Vaidani
*Screenplay:
*Cinematography: Rana Dasgupta
*Editors:
*Music: Abhijit Bose
*Art Director: Gautam Bose
*Public Relations:Debabrata Roychoudhury
*Assosiate Director:Manoj Michigan
*Costume Designer:Geetanjolly Jolly

==Censorship==
Bappaditya Bandopadhyay is in trouble with the regional Censor Board for some scenes in Kaal being ‘too graphic’. The scene is about the rape of an illegal migrant girl from Bangladesh by personnel of the Border Security Force. The director says that his film is on human trafficking and is based on NGO reports. He is now waiting for the revision committee to see his film. "The problem is that any criticism against any authority is met with iron clamps. The basic principles of democracy are being violated and the right to self-expression is either denied completely, or censored conveniently," says Bappaditya.
 
{{cite web
|url=http://www.screenindia.com/old/archive/archive_fullstory.php?content_id=14842
|title=Kaal In trouble
|publisher=www.screenindia.com
|accessdate=2008-10-25
|last=Chatterji
|first=Shoma A 
}}
 
 
 

==References==
 

== External links ==
* 
* 
* 

 
 
 
 
 