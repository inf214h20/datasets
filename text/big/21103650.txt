Om-Dar-Ba-Dar
{{Infobox film
| name           = Om Dar-Ba-Dar
| image          = Avant Garde Omdarbadar Kamal Swaroop Poster.jpg
| caption        = Theatrical Poster
| director       = Kamal Swaroop producer        = National Film Development Corporation of India (NFDC)
| starring       = Anita Kanwar Aditya Lakhia Gopi Desai Manish Gupta writer          = Kuku  (dialogue)  music           = Rajat Dholakia  Kuku  (lyrics) 
| cinematography = Ashwin Kaul Milind Ranade
| editing        = Ravi Gupta  Priya Krishnaswamy
| distributor    = PVR Pictures Directors Rare
| released       =  
| runtime        = 101 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          = 
}} absurdist storyline to satire mythology, arts, politics and philosophy.  The film won the Filmfare Critics Award for Best Movie in 1989.
 Berlin where it premiered, and soon became a cult film. In 2013, National Film Development Corporation of India (NFDC) had planned an official national release of a digitally restored print of the film.   The film finally released in Indian theaters after 26 years, on January 17, 2014. 

==Synopsis==
Om-Dar-Ba-Dar is a portrait of life in Ajmer town, Rajasthan. The film tells the story of a young boy named Om in the period of his carefree adolescence and its harsh disillusions. The story starts as a comedy and ends as a thriller. Om has a rather strange family. His father, Babuji, a government employee, leaves his job so that he can dedicate himself to astrology; Oms older sister, Gayatri, is dating a good-for-nothing fellow. Om is involved in science, but is also attracted to magic and religion. In all it seems that his really outstanding skill is his ability to hold his breath for a long time.

==Cast==
* Anita Kanwar as Phoolkumari
* Gopi Desai as Gayatri
* Lalit Tiwari as Jagdish
* Bhairavchandra Sharma
* Lakshminarayan Shastri as Oms Father
* Ramesh Mathur
* Aditya roy as Om
* Manish Gupta as Young Om
* Peter Morris Messe

==Release==
The film was made on abudget of Rs. 10 lakhs.  It had its premiere at the Berlin Film Festival in 1988, and was played at the film festival circuit and even became a cult film. However it was never commercially released in India, only a video release. The film received renewed attention when it was screened at Experimenta, an experimental film festival in Mumbai in 2005. Thereafter, it went into a digital restoration project funded by the National Film Development Corporation of India (NFDC). Eventually, the digitally-restored version was released on January 17, 2014, by PVR Cinemas in metro cities.   

==Themes==
The movie was described by its director Kamal Swaroop as a story of Lord Brahma, and it sprouted from the idea that in Hinduism, although Lord Brahma was considered the father of the entire universe, strangely no one ever worshiped him.    Swaroop also said that the films script was written based solely on dreams and images that he had and claimed he "cannot think in words." 

==Soundtrack== escapist "break" from the storyline.   

==Legacy & Influence==
Although the film was never released or seen in India during its initial rounds at the film festivals, Om-Dar-Ba-Dar has in the past 20 years gained a huge cult following and fame amongst film critics, scholars, industry insiders and cinephiles alike. One of the first serious articles about the film was written on the film blog The Seventh Art.  The blog stated, "Swaroop’s film is an antithesis to whatever is recognized globally as Indian cinema – a reason good enough to make Om-Dar-Ba-Dar a must-see movie" and that the movie can be defined as many things, the most popular of them "the great Indian LSD trip."  The film can also be looked at as a jab at mainstream Indian cinema, and many of the themes and images in the film are direct satires of conventions of Bollywood filmmaking.   
  Imtiaz Ali mentioned the vast amount of influence that the film had on aspiring independent directors in Indian cinema, stating that Om-Dar-Ba-Dar is "like old wine" and "antiquated because of the 25-year delay in its release". 

Director Anurag Kashyap also mentioned on his film blog that in his directorial venture Dev.D, the song "Emotional Attyachar" was inspired in its music and staging from the song "Meri Jaan" in Om-Dar-Ba-Dar. 

== References ==
 

==External links==
*  
*   Indianauteur
*  
*  
 

 
 
 
 
 
 
 
 
 