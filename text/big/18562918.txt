Poola Rangadu
 
{{Infobox film
| name           = Poola Rangadu
| image          = Poola Rangadu.jpg
| caption        =
| director       = Adurthi Subba Rao
| producer       = D. Madhusudhana Rao
| based on       =  
| screenplay     = Mullapudi Venkata Ramana Muppala Ranganayakamma Jamuna Vijaya Gummadi Allu Suryakantham B. Padmanabham Gitanjali Bhanu Prakash
| music          = Saluri Rajeshwara Rao
| cinematography =
| editing        =
| distributor    = Annapurna Pictures
| released       = 24-11-1967
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Tollywood film ANR and Jamuna Ramanarao|Jamuna. It is a commercial hit film with some good songs.

==Plot==
Rangadu (ANR) is called Poolarangadu, because of his purity of life. His father Veerayya is a manager at a mill, and is framed for the murder of one of the partners, Purushotham. He is sent to jail for a crime he didnt commit. Rangadu works hard to get his sister Padma educated. She marries a doctor, Prasad, who happens to be Purushothams son. When he finds out she is Veerayyas daughter, he leaves her. Rangadu is enraged and beats the guy who informs Prasad. Rangadu is sent to jail. There he meets his father, learns about his innocence and vows to get him out. Once Rangadu is released, he earns the goodwill of the other two partners and finally exposes them. Veerayya is released, Rangadu marries his lover Venkata Laxmi (Jamuna) and Prasad takes Padma back.

==Soundtrack==
# Chigurulu vesina kalalannee (Lyrics:   and K.B.K. Mohan Raju)
# Chillara rallaku mokkutu vunte (Lyrics:   and Ch. Nagaiah)
# Misamisalade chinadana (Lyrics: C. Narayana Reddy; Singers: Ghantasala and P. Susheela)
# Nee nadumupaina cheyi vesi (Lyrics: C. Narayana Reddy; Singers: Ghantasala and P. Susheela)
# Neetiki nilabadi nijayiteega (Lyrics: Kosaraju; Singer: Ghantasala)
# Neevu raavu nidura raadu (Lyrics: Dasarathi Krishnamacharya; Singer: P. Susheela)

==Box-office==
* The film ran for more than 100 days in 11 centers in Andhra Pradesh.The film was remade in tamil as enn annan with mgr and became a super hit there also 

==References==
 

==External links==
*  

 

 
 
 
 
 