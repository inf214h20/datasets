Who Done It? (1949 film)
{{Infobox Film |
  | name           = Who Done It? |
  | image          = 384 whodoneit lobbycard.jpg|
  | caption        =  |
  | director       = Edward Bernds |
  | writer         = Edward Bernds| Charles Knight Dudley Dickerson|
  | cinematography = Ira H. Morgan | 
  | editing        = Henry DeMond |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 33" |
  | country        = United States
  | language       = English
}}
Who Done It? is the 114th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Charles Knight) (also a member of the Phantom Gang) greeting the trio. Goodrichs niece flirts with Shemp, ultimately trying to poison him. Finally, a towering goon named Nikko (Duke York) chases the Stooges from room to room. After Shemp knocks Nikko cold, he literally bumps into an unconscious Goodrich, who spills the Phantom Gangs plot. A fight then ensues with the lights out, and the Stooges ultimately get the baddies.
  in Who Done It?, one of the Stooges finest efforts]]

==Production notes== Richard Lane. Schillings part was written as a combined Curly/Larry role, while Lanes was as Moe. Schilling & Lanes version of Who Done It? was called Pardon My Terror, which also included Emil Sitka, Dudley Dickerson and Christine McIntyre in its cast. 

Bernds later admitted that the rewritten script was not a good fit for Schilling & Lane and was determined to have the Stooges film Who Done It?. He took his original script, substituted several lines meant for Curly with lines fitted for Shemps brand of comedy. Stooge expert Jon Solomon, author of The Complete Three Stooges: The Official Filmography and Three Stooges Companion commented that "this well-balanced mixture of physical abuse, verbal banter, and emotional surprise is particularly vibrant even for a Stooge film." He added that "the scene in which...Christine McIntyre and Shemp point out their favorite painting while switching the drugged drink elegantly builds to a climax unparalleled in all of Stoogedom with forty seconds of Shemp wheezing, t-king, meeping, shuttering, flipping, flopping, chicken-with-its-head-cut-off-ing, and then slamming his legs on the ground in near rigor mortis." 

On the last day of production, while shooting the scene in which the Stooges crash through a door, Moe sprained his ankle. Since production could not be delayed, he taped it up and kept going. During the scenes filmed in the hallway, he is noticeably limping.

Who Done It? was remade in 1956 as For Crimin Out Loud, the last film Shemp made before his death in November 1955. {{cite book | last = Solomon| first = Jon| authorlink = Jon Solomon| coauthors =
| title = The Complete Three Stooges: The Official Filmography and Three Stooges Companion
| publisher = Comedy III Productions, Inc| year = 2002| location = | pages = 346–347 
| url = http://www.amazon.com/Complete-Three-Stooges-Filmography-Companion/dp/0971186804/ref=sr_1_1?ie=UTF8&s=books&qid=1201570359&sr=1-1 | doi =| id = | isbn = 0-9711868-0-4}} 

==References==
 

== External links ==
* 
* 
*  at  

 

 
 
 
 
 
 
 
 
 
 
 