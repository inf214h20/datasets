Tareeq al-Amal
{{Infobox film
| name           = Tareeq al-Amal  طريق الأمل 
| image          = Tareeq Amal.jpg
| image_size     = 200px
| caption        = Tareeq al-Amal poster
| director       = Ezzel Dine Zulficar
| producer       = Helmy Rafla
| writer         = Youssef Gohar
| starring       = Faten Hamama Rushdy Abaza Shukry Sarhan
| distributor    =  1957
| runtime        = 105 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
}}
 1957 Egyptian romance film|romance/drama film directed by the Egyptian film director Ezzel Dine Zulficar. It starred Rushdy Abaza, Shukry Sarhan, and Faten Hamama.

== Plot ==
Faten is a young woman whose mother suffers from a malicious disease that forbids her from working. This leaves her and her mother with no money, which forces her to work as a prostitute. One day she meets a man who sympathizes with her situation and decides to save her from this unpleasant work. He falls in love with her and decides to marry her, but his mother does not allow him because of who she is. Faten returns to working as a prostitute. One day, she saves her lovers sister which convinces his mother who accepts her as her sons wife.

== References ==
* 
* 
* 

== External links ==
*  

 
 
 
 
 


 
 