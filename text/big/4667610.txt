Naked City: Justice with a Bullet
{{Infobox film
| name           = Naked City: Justice with a Bullet
| image          = 
| image_size     = 
| caption        = 
| director       = Jeff Freilich
| producer       = Jeff Freilich
| writer         = Malvin Wald (characters)   Jeff Freilich
| narrator       = 
| starring       = Scott Glenn   Courtney B. Vance   Giancarlo Esposito   Kathryn Erbe   Robin Tunney
| music          = Ashley Irwin   Jon Tomasello
| cinematography = Miroslaw Baszek David Hicks
| distributor    = Showtime Networks
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Naked City: Justice with a Bullet is a 1998 criminal drama film about two detectives who have to protect two girls who have been robbed of all their money and luggage. Kim Poirier has a cameo appearance in the film.

==Cast==
*Scott Glenn as Sgt. Daniel Muldoon
*Courtney B. Vance as Officer James Halloran
*Giancarlo Esposito as Chaz Villanueva
*Eli Wallach as Deluca
*Kathryn Erbe as Sarah Tubbs
*Robin Tunney as Merri Coffman Barbara Williams as Eva

== External links ==
*  

 
 
 
 
 
 
 


 