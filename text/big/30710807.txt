F.A.L.T.U
 
 

{{Infobox film
| name           = F.A.L.T.U
| image          = F.A.L.T.U.jpg
| caption        = Theatrical release poster
| director       = Remo Dsouza
| producer       = Vashu Bhagnani
| writer         = Mayur Puri (dialogue)
| screenplay     = Tushar Hiranandani   Mayur Puri
| story          = 
| based on       =  
| starring       = Arshad Warsi Riteish Deshmukh Jackky Bhagnani Puja Gupta Chandan Roy Sanyal Angad Bedi
| music          = Sachin-Jigar
| cinematography = Santhosh Pandit
| editing        = 
| studio         = 
| distributor    = Puja Entertainment Ltd.
| released       =     
| runtime        = 127 minutes
| country        = India
| language       = Hindi/English
| budget         =   200&nbsp;million 
| gross          =   230&nbsp;million 
}}
 Hindi comedy film directed by Remo DSouza and produced by Vashu Bhagnani under the banner Puja Entertainment (India) Ltd.    Its plot heavily borrows from the 2006 Hollywood comedy, Accepted. 
 Akbar Khan and Darshan Jariwala appear in supporting roles. The film released on 1 April 2011, to generally negative reviews from critics, though was a moderate success at the box office.

==Plot==
The plot is identical to the movie Accepted and was partly shot in Mauritius.
The story revolves around a group of friends, Ritesh (Jackky Bhagnani|Jackky), Nanj (Angad Bedi|Angad) and Puja (Puja Gupta|Puja), who all received extremely bad marks in their exams. One of their close friends, Vishnu (Chandan Roy Sanyal|Chandan) has passed with top marks in pressure of his father, and has enrolled into the top high school of India. In order to make their parents happy and proud, the four friends create a fake university titled "Fakirchand and Lakirchand Trust University (short for F.A.L.T.U !)" with the help of Riteshs childhood friend Google (Arshad Warsi). Things take a turn for the worse when the parents would like to see the university F.A.L.T.U. To make things go right, Ritesh and Google hire a fake principal, Bajirao (Ritesh Deshmukh|Riteish) to act as the principal for one day. However, after the parents visit, a bunch of foolish kids apply for F.A.L.T.U thinking it is a real university. Unable to send them back, the trio, along with Vishnu, Google and Bajirao, turn F.A.L.T.U into an official trust university. Soon enough, the government files a case against every student/member of F.A.L.T.U for creating a fake college. Now the group of friends must fight for their rights, and keep F.A.L.T.U as a university in order to give the kids education.

==Cast==
* Arshad Warsi as Google Chand
* Ritesh Deshmukh as Principal Bajirao
* Jackky Bhagnani as Ritesh Virani
* Puja Gupta as Puja Nigam
* Chandan Roy Sanyal as Vishnu Vardhan
* Angad Bedi as Niranjan "Nanj" Nair
* Boman Irani as Principal Sharma (Special Appearance)
* Darshan Jariwala as Jeevanlal Akbar Khan as Mr. Vardhan
* Mahesh Thakur as Mr. Nigam
* Vijay Kashyap as Mr. Nair
* Vivek Vaswani as Examiner
* Himani Shivpuri as Contest Judge
* Mithun Chakraborty as Himself (special appearance)
* Remo DSouza as Himself (special appearance)

==Soundtrack==
{{Infobox album| 
| Name        = F.A.L.T.U
| Type        = soundtrack
| Artist      = Sachin-Jigar
| Cover       = F.A.L.T.U. album cover.jpg
| Released    =  
| Recorded    = 2011
| Producer = Sachin-Jigar Feature film soundtrack
| Length      = 
| Label       = Sony Music
}}
The films soundtrack is composed by Sachin - Jigar|Sachin-Jigar with lyrics penned by Sameer (lyricist)|Sameer. The song "Le Ja Tu Mujhe" by Atif Aslam was in the top 10 chart busters of 2011. 

{{Infobox album |  
 Name = F.A.L.T.U
| Type = Soundtrack
| Artist = Sachin - Jigar
| Cover =
| Released = 1 April 2011
| Genre = Film soundtrack
| Length = 
| Label = Sony Music
| Producer = 
| Last album = Life Partner (2009)
| This album = F.A.L.T.U (2011)
| Next album = Hum Tum Shabana (2011)
}}

===Track listing===

The soundtrack contains 11 songs. 

==References==
 