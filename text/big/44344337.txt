Cinderella or the Glass Slipper
{{Infobox film
| name           = Cinderella or the Glass Slipper
| image          = 
| alt            =
| caption        = 
| director       = Georges Méliès
| producer       = Charles Pathé
| based on       =  
| cinematography = 
| editing        = Ferdinand Zecca
| studio         = Star Film Company
| distributor    = Pathé Frères
| released       =  
| runtime        = 
| country        = France
| language       = Silent
}}
 the fairy tale by Charles Perrault.

==Production==
The film was made in the summer and autumn of 1912.   
Cinderella is played by a sixteen-year-old actress, Louise Lagrange.    Prince Charming is also played by an actress.  Méliès himself makes an appearance in the film as the Princes messenger who searches for the owner of the glass slipper.  The film features extensive use of outdoor location filming, a practice common in Mélièss later films. 

Like all of Mélièss 1911–1912 films, Cinderella was made under the supervision of Charles Pathé for his studio Pathé Frères.  After receiving Mélièss work, Pathé authorized the filmmaker Ferdinand Zecca to edit it. Zecca cut the film down to half the length Méliès intended, and is also probably responsible for adding the cross-cutting effects and medium shots seen in the film, as these devices are highly unusual in Mélièss style. (Mélièss mistress and eventual wife, the actress Jeanne dAlcy, later accused Zecca of deliberately sabotaging the film in order to ruin Mélièss career. This charge was never proven, however.) 

==Release and reception==
According to December 1912 advertisements, the film scheduled for release on 3 January 1913.    It was advertised as a féerie en 2 parties et 30 tableaux, daprès le chef-dœuvre de Charles Perrault.  It was not a success, partially because of the directorial conflict between Méliès, Zecca, and Pathé, and partially because Mélièss theatrical style had fallen out of fashion by 1912.   

==References==
 

==External links==
* 

 
 

 
 

 