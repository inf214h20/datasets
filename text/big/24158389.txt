Junglee (2009 film)
 {{Infobox film
| name           = Junglee
| image          =
| alt            =  
| caption        = 
| director       = Duniya Soori
| producer       = Rockline Venkatesh
| writer         = Duniya Soori
| screenplay     = Duniya Soori
| story          = Duniya Soori
| starring       = Duniya Vijay  Andritha Ray  Adi Lokesh  Rangayana Raghu
| music          = V. Harikrishna
| cinematography = Satya Hegde
| editing        = Deepu S Kumar
| studio         = Rockline Production
| distributor    = Yama Release Group  
| released       = Feb.06.2009
| run time       = 143min
| country        = India
| language       = Kannada
| music on       = Anand Audio
| budget         =  9crore
| gross          =  35 crores 
}} Soori and produced by Rockline Venkatesh.

== Cast ==
* Duniya Vijay as Junglee Prabhakar
* Andritha Ray as Padma
* Rangayana Raghu as Gudde Narasimha
* Adhi Lokesh
* Sureschandra
* Megabhagavatar
* Kashi as Padmas father
* Jaidev
* Balu nagendra
* Naveen
* Shobaraj

==Soundtrack==

{|class="wikitable" width="70%"
! Track # || Song || Singer(s) !! Lyricist
|-
| 1 || "Hale Paatre" || Kailash Kher, Sowmya Raoh || Yograj Bhat 
|-
| 2 || "Neenendare" || Sonu Nigam || Jayant Kaikini 
|-
| 3 || "Ee Majavada" || Suma Shastry || Yogaraj Bhat 
|-
| 4 || "O Nalmeya" || M. D. Pallavi Arun || Jayanth Kaikini
|-
| 5 || "Junglee Shivalingu" || Tippu (singer)|Tippu, Anuradha Bhat, Rangayana Raghu, Chorus || Yograj Bhat
|-
| 6 || "Thale BaachkoLo" || Vijay, Rangayana Raghu (backing) || Yograj Bhat
|-
|}

==References==

 

==External links==
*  

 
 
 
 
 


 