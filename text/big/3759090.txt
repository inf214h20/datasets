Another Time, Another Place (1958 film)
{{Infobox Film
  | name = Another Time, Another Place
  | image =Cartanot.jpg
  | image_size = 200 px
  | caption = Theatrical film poster Lewis Allen
  | producer = Joe Kaufmann
  | based on = Weep No More by Lenore J. Coffee
  | writer = Stanley Mann Barry Sullivan Glynis Johns Sean Connery
  | cinematography = Jack Hildyard
  | music = Douglas Gamley
  | editing = Geoffrey Foot
  | studio = Lanturn Productions Kaydor Productions Ltd.
  | distributor = Paramount Film Service
  | released = May 2, 1958
  | runtime = 98 minutes
  | language = English
  | budget = 
}} Barry Sullivan and Sean Connery. The film is based on Lenore J. Coffees 1955 novel Weep No More.

==Plot==
An American reporter, Sara Scott (Turner) is working in Europe during World War II and begins an affair with a British reporter named Mark Trevor (Connery).  Sara is conflicted on whether to marry her rich boss (Sullivan) or the charming young reporter she is having an affair with.  Finally, she chooses Mark, only to find that he is married and has a son.  The two separate shortly thereafter, then decide to stay together and work out their problems.

After the war ends, Mark Trevor is killed in an accident, sending Sara into mourning.  Her boss, after a few months, convinces her to come back to the United States and work for him.  Before she goes, she visits Trevors hometown and lives for a time with his wife (Johns) and son as she works to fashion Trevors war reporting into a book.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Lana Turner || Sara Scott
|- Barry Sullivan || Carter Reynolds
|-
| Raymond Burr || Walt Radak
|-
| Glynis Johns || Kay Trevor 
|-
| Sean Connery || Mark Trevor 
|-
| Terence Longdon || Alan Thompson 
|-
| Sid James || Jake Klein
|- Martin Stephens || Brian Trevor
|-
| Doris Hare || Mrs. Bunker
|-
| John Le Mesurier || Doctor Aldridge
|- Cameron Hall || Alfy
|-
| Robin Bailey || Captain Barnes
|}

==Production==
During the films principal photography in England, Connery was confronted by gangster Johnny Stompanato, then-boyfriend of Lana Turner. The jealous man pointed a gun at Connery and warned him to keep away from Turner. Connery answered by grabbing the gun out of Stompanatos hand and twisting his wrist, causing him to run off the set. 
Stompanato was killed by Lana Turners daughter in April 1958.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 