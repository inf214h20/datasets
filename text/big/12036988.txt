Huella de luz
 

{{Infobox Film
| name           = Huella de luz
| image          = Huelladeluz.jpg
| image_size     =
| caption        = Film poster
| director       = Rafael Gil
| producer       = 
| writer         = Wenceslao Fernández Flórez (novel and screenplay)
| narrator       = 
| starring       = Antonio Casal Isabel de Pomés Juan Quintero
| cinematography = Alfredo Fraile
| editing        = Juan Pallejá
| distributor    = 
| released       = 22 March 1943
| runtime        = 76 minutes
| country        =   Spanish
| budget         = 
}} 1943 Spain|Spanish comedy film directed by Rafael Gil on his directing debut. It is based on a novel by Wenceslao Fernández Flórez.

==Cast==
* Isabel de Pomés ...  Lelly Medina
* Antonio Casal ...  Octavio Saldaña
* Juan Espantaleón ...  Sánchez Bey
* Camino Garrigó ...  Madre de Octavio
* Juan Calvo ...  Mike
* Fernando Freyre de Andrade ...  Moke
* Mary Delgado ...  Rosario
* Nicolás D. Perchicot ...  Mayordomo
* Ramón Martori ...  Medina
* Julio Infiesta ...  Jacobito
* José Prada ...  Cañete
* Alejandro Nolla ...  Gerente del hotel
* Ana María Campoy ...  Isabel
* Francisco Hernández ...  Don Eduardo
* Joaquín Torréns ...  Conserje
* Fernando Porredón ...  Emilio

==External links==
*  

 
 
 
 
 
 
 
 


 
 