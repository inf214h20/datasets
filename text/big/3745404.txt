Hellraiser: Bloodline
{{Infobox film
| name           = Hellraiser IV: Bloodline
| image          = Hellraiser bloodline ver2.jpg
| caption        = Promotional movie poster
| director       = Kevin Yagher (credited as Alan Smithee) Joe Chappelle (uncredited)
| producer       = Nancy Rae Stone
| writer         = Peter Atkins Adam Scott Tom Dugan
| music          = Daniel Licht
| cinematography = Gerry Lively
| editing        = Randy Bricker Rod Dean Jim Prior
| studio         = Dimension Films
| distributor    = Miramax Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $4,000,000
| gross          = $16,675,000 
}}

Hellraiser IV: Bloodline is a 1996 American science fiction film|science-fiction horror film and the fourth installment in the Hellraiser (film series)|Hellraiser series, which serves as both a prequel and a sequel. Directed by Kevin Yagher and Joe Chappelle, the film stars Doug Bradley as Pinhead (Hellraiser)|Pinhead, reprising his role and now the only remaining original character and cast member. Other cast members include Bruce Ramsay, Valentina Vargas and Kim Myers. 
This was the last Hellraiser film to be released in theaters and the last to have any sort of major official involvement with series creator Clive Barker and also the final installment in chronology.

==Summary== Lament Configuration (the robot is subsequently destroyed); however, several guards led by Rimmer capture Paul. Paul tells Rimmer the story of his bloodline.

A flashback is shown to around 400 years ago. Philip Lemarchand, a French toymaker, makes the Lament Configuration for a wealthy aristocrat named Duc de LIsle, who is obsessed with dark magic. He and his apprentice, Jacques, kill a woman and use her to summon the Cenobite slave Angelique.  However, Angelique and Jacques betray and kill de LIsle. Lemarchand, in the process of inventing a design (the Elysium Configuration) to destroy the demons, attempts to steal back the box but is discovered. Jacques callously informs the toymaker that he and his bloodline are cursed until the end of time because of the box he created, before ordering Angelique to kill him. However, his pregnant wife survives.
 Pinhead arrives. Angelique is perturbed by Pinheads appearance, and the reunion informs her that Hell has revised its ideological foundations, and is no longer the place she once knew. Upon seeing the building, Pinhead intends to use the basement to establish a permanent gateway between Hell and Earth, and succeed in his previous attempt to conquer Earth. Angelique initially cooperates, using her wiles and Merchants memories to solicit his compliance, but Pinhead is impatient and infuriated by her methods, and kidnaps Merchants wife and son to force the issue. Having grown accustomed to a decadent life on Earth, Angelique decides she wants no part of Hells new fanatical austerity, and intends to force Merchant to activate the Elysium Configuration and destroy Hell, thus freeing her. But the process is incomplete and the attempt fails; Pinhead kills Merchant before his wife solves the box and forces Pinhead back to Hell, taking Angelique with him.

Returning to 2127, Rimmer has Paul locked up, and the Cenobites trick the guards into releasing them from their holding cell; upon learning of Merchants intentions, they kill the entire security team until only Rimmer and Merchant remain. Rimmer releases Paul, who has a plan to destroy the Cenobites (and built Minos for that specific reason). 

Paul distracts Pinhead with a hologram while he gets on the shuttle with Rimmer, and activates the Elysium Configuration. A series of powerful lasers and mirrors create a field of perpetual light, while the station transforms and folds around the light to create a massive box. The light is trapped within the box, which then self-destructs, destroying the Cenobites.

==Cast== Pinhead
*Bruce Ramsay as Philippe "Toymaker" Lemarchand / John Merchant / Doctor Paul Merchant
*Valentina Vargas as Peasant Girl / Angelique / Angelique-Cenobite
*Kim Myers as Bobbi Merchant Adam Scott as Jacques
*Christine Harnos as Rimmer
*Charlotte Chatton as Genevieve Lemarchand
*Mickey Cottrell as Duc de LIsle
*Paul Perri as Edwards / Skinless Parker
*Pat Skipper as Carducci Tom Dugan as Chamberlain
*Jody St. Michael as Chatterer Beast
*Louis Turenne as Auguste de LMoure
*Courtland Mead as Jack Merchant
*Wren T. Brown as Parker
*Sally Willis as MINOS Space Station Computer Voice
*Louis Mustillo as Sharpe
*Kenneth Tobey as Hologram-Priest (uncredited)
*Mack Miles as Corbusier - Gambler 1 / Clown-Cenobite 1 (uncredited)
*Andrew Magnus as Delvaux - Gambler 2 / Clown-Cenobite 2 (uncredited)

==Production==
Kevin Yagher disowned the version with cuts made behind his back due to conflicting artistry ideas. Yaghers version contained much more graphic imagery, plot, and explained everything that happened in the film. The producers disagreed and demanded Pinhead should appear sooner despite every version of the script up until then having him appear around the 40-minute mark. Yagher eventually walked away and never finished filming some final scenes, and Joe Chappelle was brought on to finish the film, filming new scenes from re-writes including the narrative framing device. Some scenes of the original script were thus never shot.
Yagher substituted the generic Directors Guild pseudonym "Alan Smithee". 
==Filming==
The filming began in 1995.
The script, a fourth draft written by Peter Atkins, may be found at the internet site The Hellbound Web.  Kevin Yagher cut four different directors cuts, ranging from 82 to 110 minutes.

==Release==
===Critical reception===
Bloodline was heavily marketed and initially received better reception than its predecessor. It garnered some positive reviews amongst a field of negative criticism. Some praised its expanded scope while others derided its cheaper horror aspects. It currently has a 25% rating on Rotten Tomatoes, slightly higher in rank than its predecessor. Nevertheless it became the last Hellraiser movie to have a theatrical release. 
 

===Box office===
In the United States and Canada, it grossed $16,675,000. 

==See also==

* List of films featuring space stations

==References==
 

==External links==
*  
 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 