I Dreamed of Africa
{{Infobox film
| name           = I Dreamed of Africa
| image          = I Dreamed of Africa Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Hugh Hudson
| producer       = Stanley R. Jaffe Allyn Stewart
| screenplay     = Paula Milne Susan Shiliday
| based on       =  
| starring       = Kim Basinger Vincent Perez Eva Marie Saint Daniel Craig
| music          = Maurice Jarre
| cinematography = Bernard Lutic
| editing        = Scott Thomas
| distributor    = Columbia Pictures
| released       =  
| runtime        = 114 minutes
| country        = United States English
| budget         = $50 million
| gross          = $14,400,327
}} I Dreamed conservation work. It was screened in the Un Certain Regard section at the 2000 Cannes Film Festival.   
==Synopsis==
A divorced Italian socialite changes her life after surviving a car crash. She marries a man she doesnt know well and moves with him and her young son to Kenya, where they start a ranch. She faces many problems, both physical and emotional, that will test her.

==Cast==
* Kim Basinger as Kuki Gallmann
* Vincent Perez as Paolo Gallmann
* Eva Marie Saint as Franca
* Daniel Craig as Declan Fielding
* Liam Aiken & Garrett Strommen - Emanuele (age 7 and age 17)
* Lance Reddick as Simon
* Connie Chiume as Wanjiku

==Soundtrack==
This includes "Voi che sapete", sung by Brigitte Fassbaender (mezzo soprano), with the Vienna Philharmonic Orchestra, István Kertész (conductor)|István Kertész Under the Sun  conducting, from Act 2 of the opera Le nozze di Figaro (The Marriage of Figaro), K. 492, composed by Wolfgang Amadeus Mozart.  This was one of the last films scored by acclaimed composer Maurice Jarre.

==Reception== Worst Actress (also for Bless the Child).

It was also a huge financial Box office bomb|flop: its budget was $50 million while worldwide gross was less than $15 million. 

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*  

 

 
 
 
 
 
 
 
 
 
 
 