Little Senegal (film)
{{Infobox film
| name           = Little Senegal (film)
| image          = Little-senegal-rachid-bouchareb.jpg
| caption        = Theatrical release poster 
| director       = Rachid Bouchareb
| producer       = 
| writer         = Rachid Bouchareb  Jean-Pierre Ronssin
| starring       = Sotigui Kouyaté Sharon Hope Roschdy Zem Karim Traoré Adetoro Makinde Adja Diarra Malaaika Lacario
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2001
| runtime        = 
| country        = Algeria, France, Germany
| language       = Wolof, English, French, Arabic
| budget         =
}} 2001 Algerian directed by Rachid Bouchareb. It was Algerias submission to the 73rd Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

==Cast==
*Sotigui Kouyaté ... Alloune
*Sharon Hope ... Ida
*Roschdy Zem ... Karim
*Karim Traoré ... Hassan (credited as Karim Koussein Traoré)
*Adetoro Makinde ... Amaralis
*Adja Diarra ... Biram
*Malaaika Lacario ... Eileen
*Toy Connor ... Girl on Bridge (supporting)

==See also==
 
*List of submissions to the 73rd Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 