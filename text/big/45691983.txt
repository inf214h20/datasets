Things Are Looking Up (film)
{{Infobox film
| name =  Things Are Looking Up
| image =
| image_size =
| caption =
| director = Albert de Courville
| producer =Herbert Mason    Michael Balcon
| writer =  Stafford Dickens    Daisy Fisher   Con West   Albert de Courville
| narrator = Max Miller Mary Lawson
| music = Louis Levy
| cinematography = Charles Van Enger   Glen MacWilliams
| editing = R.E. Dearing
| studio = Gaumont British Picture Corporation
| distributor = Gaumont British Distributors
| released = 1935
| runtime = 77 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Max Miller and William Gargan. It was made at Islington Studios by British Gaumont, an affiliate of Gainsborough Pictures.  The films sets were designed by Alex Vetchinsky. Courtneidge plays a dual role as the sisters Bertha and Cicely Fytte. Bertha is a dour schoolteacher, while the bubbly Cicely runs a nearby showground. When Bertha surprisingly elopes, Bertha takes her place at the school to prevent her getting the sack. It was the film debut for Vivien Leigh. 

==Cast==
*  Cicely Courtneidge as Cicely Fytte / Bertha Fytte   Max Miller as Joey  
* William Gargan as Van Gaard  Mary Lawson as Mary Fytte 
* Mark Lester as Chairman  
* Henrietta Watson as Miss McTavish  
* Cicely Oates as Miss Crabbe  
* Judy Kelly as Opal  
* Dick Henderson as Mr. Money 
* Dickie Henderson as Mr. Moneys Son  
* Charles Mortimer as Harry 
* Hay Plumb as Tennis Umpire   Danny Green as Big Black Fox  
* Suzanne Lenglen as Madame Bombardier 
* Vivien Leigh as Schoolgirl (uncredited)
* Alma Taylor as Schoolmistress (uncredited)
* Wyn Weaver as Governor (uncredited) Ian Wilson as Drummer in Band (uncredited)

==References==
 

==Bibliography==
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.
*Howard Reid, John. (2005). Hollywoods Miracles of Entertainment. Lulu.com  

==External links==
*  
*   Britmovie | Home of British Films

 

 
 
 
 
 
 
 
 

 