Going Down in LA-LA Land
{{Infobox film
| name           = Going Down in LA-LA Land
| image          = Going_down_in_la_la_land.jpg
| alt            = 
| caption        = 
| director       = Casper Andreas
| producer       =  
| writer         = Casper Andreas
| based on       =  
| starring       = Matthew Ludwinski, Allison Lane and Michael Medico
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Going Down in LA-LA Land is a 2011  comedy-drama film written and directed by Swedish filmmaker Casper Andreas and based on the novel by Andy Zeffer.  It was released in New York City on April 20, 2012 and in Los Angeles on May 18, 2012.   

==Plot==

When he is unable to find paying work as an actor in New York, Adam (Matthew Ludwinski) decides to try Los Angeles. He moves in with his eccentric but lovable friend Candy (Allison Lane), whose fiancee Frank supports her. Adam gets a reception job at a talent agency, but gets fired for coming in late one day. He then meets Nick (Casper Andreas), a guy from the gym who works as a director and photographer. Nick helps Adam land a job with Jet Set Productions, a company that produces gay pornography.

He is offered extra money to appear in the companys videos, but insists that he only wants to work behind the camera. Unfortunately, Adam starts to struggle financially when Frank breaks up with Candy. Nick, who is now dating Adam, convinces him to appear in a solo masturbation video under the name "Andrew" in order to help pay his rent. After making more videos, his boss Ron tells him about his escort business where he pairs actors with high-profile clients.

Through his work, Adam is introduced to John; a successful actor from the popular TV sitcom Life Lessons. The two start to see each other regularly, and John offers Adam a job as his assistant, which he happily takes. He later encounters Nick, whos begs for money after his addiction to meth caused him to lose his job. He also apologizes for how he treated Adam, and leaves.

Sometime later, Adam is then put into the public eye when his porn star career is exposed in several tabloids. While John later explains to him that he knows Adam wouldnt tell anyone about their relationship, he cant risk losing his career by being associated with him. While Candy wants Adam to take advantage of his new found fame since John fired him, he says he could never hurt John. Adam then almost dies when he mixes alcohol and pills.

While in the hospital, John calls Adam to see if they can meet up without knowing about his accident. Adam then tells John that while he loves him, he cant go back to keeping their relationship a secret. Adam decides to move to Miami and figure out what he wants to do. While packing, John shows up at the apartment and says he wants Adam back into his life. They go outside where a group of paparazzi have been waiting, and kiss in front of the cameras before driving away.

==Cast==
{| class="wikitable"
|-
! style="background:silver;"| Actor
! style="background:silver;"| Role
|-
| Matthew Ludwinski || Adam
|-
| Allison Lane || Candy
|-
| Michael Medico || John
|-
| Casper Andreas || Nick
|-
| John Schile || Ron
|-
| Jesse Archer || Matthew
|-
| Bruce Vilanch || Missy
|-
| Judy Tenuta || Zinnea
|-
| Alec Mapa || Himself
|-
| Perez Hilton || Ricky
|-
| Daniel Hayes || Lucas
|-
| Taaffe OConnell || Job Interviewer
|-
| Annie Wood || Sitcom Wife
|-
| Michelle Akeley || Stacy
|-
| Kim Allen || Ms. Campbell
|-
| Cesar Arambula || Oscar Party Guest #1
|-
| Brent Bailey || Dean
|-
| Anadel Baughn || Beverly Hills Lawyer
|-
| Daniel Berilla || Perry
|-
| Jeremy Building || Dark-haired Model
|-
| C. Stephen Browder || Marvin
|-
| Mark Cirillo || Alec Mapas Fan
|-
| Damian Corboy || Production Assistant
|-
| Scott DeFalco || Frank
|-
| Spencer Falls || Baby-faced Model
|-
| Sean Hemeon || Oscar Party Guest #2
|-
| Angelina Hong || Kim
|-
| William Jackson || Older Actor
|-
| Gregg Jacobson || Sun-burned Model
|-
| William Thomas Jones || Robert
|-
| Linda Larson || Casting Director
|-
| Claes Lilja || Scott
|-
| Paul Martignetti || Career Waiter
|-
| Tom Nobles || Wayne
|-
| Erik Passoja || Mr. Katz
|-
| Brian Putnam || Paparazzi
|-
| Kurt Scholler || Slave
|-
| Todd Sherry || Collin
|-
| Ashley Slaton || Starlet
|-
| Billy Snow || Gym Sales Guy
|-
| Geoff Stirling || The Football Coach
|-
| Sherry Weston || Older Actress
|}

==Reception==

Variety (magazine)|Variety suggested that Director Casper Andreas, whose first two films are downplayed as "trivial pursuits," "shows signs of maturing talent" with this film. They concluded that the film was a "polished but entertaining look at biz realities from a gay perspective...well crafted and diverting throughout." 
 Out Magazines Glenn Payne called the casting of Ludwinski as Adam "pitch perfect", describing his performance as "refreshing and unexpected". In the same review, Payne remarks that "Andreas as a director has cemented his stylistic form with this film and has improved leaps and bounds since his 2004 debut Slutty Summer". http://www.out.com/entertainment/popnography/2012/04/23/review-Matthew%20Ludwinski-porn-going-down-la-la-land  Lane, too, is praised for her "loveable but also relatable performance". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 