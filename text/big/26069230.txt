C.H.O.M.P.S.
{{Infobox film
| name           = C.H.O.M.P.S
| image          = CHOMPS film poster.jpg
| caption        = Theatrical poster to C.H.O.M.P.S
| director       = Don Chaffey producer = Burt Topper Joseph Barbera
| story          = Joseph Barbera
| screenplay     = Joseph Barbera Duane Poole Dick Robbins
| starring       = Wesley Eure Valerie Bertinelli Conrad Bain
| music          = Hoyt Curtin
| cinematography = Charles F. Wheeler
| editing        = Dick Darling Warner E. Leighton
| studio         = Hanna-Barbera MGM (2005, DVD)
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
C.H.O.M.P.S. is a 1979 film directed by Don Chaffey and was his final feature film.

==Plot==
Brian Foster (Wesley Eure), a young inventor, creates a robotic dog for use as part of a home protection system. C.H.O.M.P.S. is an acronym for "Canine HOMe Protection System." Ralph Norton (Conrad Bain) is his boss who he constantly argues with.  Nortons daughter Casey (Valerie Bertinelli) and Foster develop a relationship.  A rival company wants the dog and sends a few petty criminals to dognap "C.H.O.M.P.S."

==Principal cast==
* Wesley Eure as Brian Foster
* Valerie Bertinelli as Casey Norton
* Conrad Bain as Ralph Norton
* Chuck McCann as Brooks
* Red Buttons as Bracken
* Jim Backus as Mr. Gibbs

==Background==
Joseph Barbera approached his friend Samuel Z. Arkoff of American International Pictures about his company collaborating with Hanna-Barbera on live-action films. Though William Hanna and other members of Hanna-Barbera were not eager to venture beyond the animation field, according to Barbera, Arkoff was enthusiastic about the ideas that Barbera presented, and agreed to do nine films together. Barberas first idea was for a film about a super-canine, robotic Doberman pinscher guard dog which would capitalize on several ideas popular at the time. 

Barbera recalled that Arkoffs son Louis suggested that rather than a Doberman, the dog would have to be a non-threatening dog in the Benji mold. Barbera attributes this change in focus in the story to the films lackluster performance at the box-office. In his autobiography, Barbera wrote that the film "did okay... but it never made the splash it should have". Because of this, the future film deals between Hanna-Barbera and AIP were canceled. 

Burt Topper worked on the movie as producer with Barbera. 
This film was in theaters during 1979 starring Brain Foster and Casey Norton

==Critical appraisal==
On the films release, Variety (magazine)|Variety wrote, "although it features a cute canine hero, a pair of do-gooding young people and a bevy of silly-minded adults, pic has little of the action or charm that lure audiences." The review noted that director Don Chaffey "has done what he can to keep the pic moving given what he has to work with." Of the performers, Variety judged, "Actors are uniformly okay but theres really only one star in this picture, Chomps. Benji hes not." 

Judging the film to be "unpretentious but slightly dismal in its execution, the Los Angeles Times wrote, "The premise is engaging enough to entertain dog lovers and kids for awhile  , but the screenplay... is mediocre television sitcom fare and too thin to sustain an entire movie." 

==Availability== got its start) released C.H.O.M.P.S in DVD format on April 12, 2005. 

==References==
 

==Bibliography==
*  
* The Hollywood Reporter, v.251 n.34, May 19, 1978, p.&nbsp;19.
* The Hollywood Reporter, v.259 n.37, December 20, 1979, p.&nbsp;3.

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 