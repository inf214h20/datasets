Hotel Splendide (film)
 
 
 
{{Infobox film
| name           = Hotel Splendide
| image          = Hotel_splendide_2000_poster_.jpg
| caption        = Hotel Splendide promotional poster
| director       = Terence Gross
| producer       = Robert Buckler Ildiko Kemeny
| writer         = Terence Gross
| starring       = Toni Collette Daniel Craig
| music          = Mark Tschanz
| cinematography = Gyula Pados
| editing        = Michael Ellis
| studio         = 
| distributor    = 
| released       = 22 September 2000
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} independent dark comedy film, written and directed by Terence Gross and starring Toni Collette and Daniel Craig. The film appeared in a number of British and European film festivals but was not released in the US, although it did appear on cable networks on channels catering to independent film. It is currently  available in the UK on DVD and the US on DVR on demand.

The film tells the story of the Blanche family who run a dark and dismal health resort on a remote island which is only accessible by ferry. The spa program consists of feeding the guests seaweed and eel-based meals, then administering liberal colonic irrigation. The spa is run by the family matriarch, Dame Blanche, until her death.

Things continue with her children running the resort until Kath, the resorts former sous chef and the love interest of one of the sons, comes back to the island unannounced. Stranded between monthly ferries, she is a catalyst for a series of events that turns life as it is known at Hotel Splendide on its ear.

==Cast==
*Toni Collette as Kath
*Daniel Craig as Ronald Blanche
*Katrin Cartlidge as Cora Blanche
*Stephen Tompkinson as Dezmond Blanche
*Hugh OConor as Stanley Smith
*John Boswall as Bellboy

==External links==
*  
*  

 
 
 
 
 
 


 
 