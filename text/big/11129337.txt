Two Lost Worlds
{{Infobox Film 
 | name = Two Lost Worlds
 | caption = 
 | director = Norman Dawn
 | producer = Boris Petroff
 | writer = Tom Hubbard/Phyllis Parker/Boris Petroff (story)/Bill Shaw
 | starring = James Arness  Kasey Rogers
 | music = Alex Alexander
 | cinematography = Harry Neumann
 | editing = Fred R. Feitshans Jr.
 | distributor = Sterling Productions Inc. Eagle-Lion films
 | released = January 5, 1950
 | runtime = 61 minutes
 | language = English
 | budget = 
 | image = Two Lost Worlds VideoCover.jpeg
}}
 Laura Elliott. scripted by Tom Hubbard and voice-over narrative by Bill Shaw), directed by Norman Dawn, and distributed by Eagle-Lion Classics Inc., with a 1952 reissue by Classic Pictures Inc..

==Plot== Bill Kennedy) a rancher. A romantic rivalry develops and the pirates, who attacked Kirk and his ship kidnap her along with her friend, Nancy Holden (Jane Harlan). Kirk and Shannon pursue the pirates and they soon wind up on a volcanic island, inhabited by dinosaurs.

==Trivia==
There are no original dinosaur effects in the film. The dinosaurs appear 58 minutes into the film during the final reel. They were taken from stock footage recycled from the film One Million B.C. (1940).

The film was shot in Red Rock Canyon State Park (California) in Cantil, California.

==External links==
*  

 
 
 
 
 
 
 
 


 