Kindergarten Cop
 
{{Infobox film
| name           = Kindergarten Cop
| image          = Kindergarten Cop film.jpg
| caption        = Theatrical release poster
| director       = Ivan Reitman
| producer       = Ivan Reitman Brian Grazer
| screenplay     = Murray Salem Herschel Weingrod Timothy Harris
| story          = Murray Salem
| starring       = Arnold Schwarzenegger Penelope Ann Miller Pamela Reed Linda Hunt Carroll Baker
| music          = Randy Edelman Michael Chapman
| editing        = Wendy Greene Bricmont Sheldon Kahn
| studio         = Imagine Entertainment
| distributor    = Universal Pictures
| released       = December 21, 1990
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $202 million
}}
Kindergarten Cop is a 1990 American comedy film directed by Ivan Reitman and starring Arnold Schwarzenegger  as John Kimble, a tough police detective, who must go undercover and pose as a kindergarten teacher to catch drug dealer Cullen Crisp (Richard Tyson), before he can get to his ex-wife and son. Along the way, he discovers his passion for teaching and considers changing his profession to become an educator. Pamela Reed plays his partner, Phoebe OHara, and Penelope Ann Miller plays Joyce, the teacher who becomes his love interest. The original music score was composed by Randy Edelman. 

==Plot==
 
After years of pursuing drug dealer Cullen Crisp (Richard Tyson), Los Angeles Police Detective John Kimble (Schwarzenegger) has him on a murder charge after Crisp shot and killed an informant who has given him information regarding the whereabouts of his ex-wife, Rachel Myatt Crisp, and his son Cullen Jr. However, the informants girlfriend is unwilling to testify in court.

Kimble, accompanied by detective and former teacher Phoebe OHara (Pamela Reed), go undercover in Astoria, Oregon, to find Crisps ex-wife. It is believed that Rachel stole millions of dollars from Crisp before fleeing. The detectives are going to offer her a deal to testify against Cullen in exchange for immunity. OHara is to act as the substitute teacher in the sons kindergarten class at Astoria Elementary School. Kimble will discover the identity of the mother.

Unfortunately, OHara, who is hypoglycemic, gets a terrible case of the stomach flu and falls ill at the last moment so Kimble takes the teachers job. The suspicious school principal, Miss Schlowski (Linda Hunt), is convinced he will not last long before quitting. Kimble adapts progressively to his new status even though he has no formal teaching experience. Using his pet ferret as a class mascot, his police training as a model for structure of the classes, and positive reinforcement, he becomes a much-admired and cherished figure to the children. In turn, Kimble begins to like his cover job and his young charges. He also deals with a case of child abuse. Miss Schlowski witnesses his actions, the principal assures him that even though she didnt agree with his methods, she can see that he is a good teacher.

Kimble becomes fond of Dominics mother Joyce Palmieri (Penelope Ann Miller), who also works at the school. Joyce, like many other of the students mothers, is on terms of estrangement from her husband, so that she will not speak of him. This excites the suspicions of Kimble. In a series of conversations with the gradually more trusting Joyce, Kimble slowly deduces that she has to be Rachel Crisp and that Dominic is Crisps son.

Meanwhile, back in California the murder witness dies after using spiked cocaine provided by Eleanor Crisp (Carroll Baker). The case is over because the prosecution has no further evidence. Crisp  is freed from prison and immediately heads to Astoria.

Crisp and his mother arrive in the town and immediately begin looking for the child, after which Crisp starts a fire in the school in order to get the boy. Upon being witnessed, he takes his own son hostage but Kimble manages to kill him before he can hurt Dominic. Crisps mother then discovers her dead son but before she can shoot Kimble, OHara arrives and knocks her unconscious with a baseball bat. 

Eleanor is arrested, while the unconscious Kimble (much to the sadness of the children) is hospitalized. During Kimbles recovery, OHara and her chef fiancée announce their marriage, inviting him to it. After Kimble recovers, he visits the school, and Joyce kisses him in front of all the kids.

==Cast==
*  n born LAPD street cop whos forced to take an undercover assignment as a Kindergarten teacher.
* Penelope Ann Miller as Joyce Palmieri/Rachel Myatt Crisp: a teacher whom Kimble falls for but who is also Crisps ex-wife. Has a young son, Dominic, who is in Kimbles kindergarten class.
* Pamela Reed as Detective Phoebe OHara: Kimbles partner and a former schoolteacher who gets food poisoning, forcing him to cover for her on the assignment. She briefly poses as his sister, "Ursula Kimble".
* Linda Hunt as Miss Schlowski: the school principal. Though initially suspicious of Kimble, she eventually grows to respect him when he assaults Zachs universally-disliked abusive father.
* Richard Tyson as Cullen Crisp, Sr.: a drug kingpin who is obsessed with tracking down his ex-wife and son.
* Carroll Baker as Eleanor Crisp: Crisps overbearing mother and assistant in his criminal empire who is just as obsessed with seeing her grandson again.
* Christian and Joseph Cousins as Dominic Palmieri/Cullen Crisp, Jr.: Joyce and Crisps son who becomes close to Kimble.
*Andrew Dimarco as Zach Sullivan: a shy student in the class and the first one who Kimble suspects is Cullen Jr., though he later learns that his parents are still married and dealing with domestic violence. As a policeman with a hatred for injustice, Kimble later assaults his father, whom he knows the law cant touch easily, and berates his mother for her cowardice to protect him.
* Cathy Moriarty as Jillian
* Ben Diskin as Sylvester, Jillians son anatomical differences between boys and girls which he apparently learned from his father who is a gynecology|gynecologist.
* Sarah Rose Karr as Emma, one of Kimbles students
* Richard Portnow as Captain Salazar, Kimble and OHaras boss
* Tom Kurlander as Danny, a drug runner and Crisps informant, as well as his murder victim
* Alix Koromzay as Cindy, Dannys drug-addict girlfriend and the only witness to his murder, which helps lead to her demise. Bob Nelson as Henry Shoop: OHaras fiancee Tom Dugan as Crisps lawyer
* Emily Eby as Julie
* Odette Yustman as Rosa
* Angela Bassett as Flight Attendant

Brian Bruney was an extra in the film. He was 8 years old at the time.

Bill Murray, Patrick Swayze, and Danny DeVito were all approached to play the role of John Kimble.

In addition to the Cousins twins, a second set of identical ones were cast to play part of Kimbles class. The difference was that Tiffany and Krystle Mataras, who later went on to star as the daughters of one of the antagonists in Problem Child 2, actually played a set of identical ones named Tina and Rina.

==Filming locations==
Exterior scenes at Astoria Elementary School were filmed at John Jacob Astor Elementary School, located at 3550 Franklin Ave. in Astoria, Oregon.   

At Astoria, Universal Studios hired local artists Judith Niland & Carl Lyle Jenkins to paint murals on the walls, provided new playground equipment, fenced the playground, and laid a new lawn and hedges around the school building. Most of the filming was completed after school was out in June 1990,    therefore many of the students and staff were able to be in the movie as extras.    Students artwork was also used. Teachers and neighbors, as well as students, were used in filming; viewers see the custodian, "Mr. John" raising the flag for an early morning scene.  Of note, Schwarzeneggers contract required that a private studio (for daily workouts and weightlifting) be provided for the actor and his personal staff; a suitable studio was located but when an agreement could not be reached, the actor threatened to pull out of the production. An Astoria business owner stepped in and donated unused commercial space deemed suitable and the shoot went on.

Also filmed in or near Astoria:
* John and Phoebe stayed at the Bayview Motel,  . The vintage lodging facility "played itself" in the film.
* Scenes involving John and Phoebe walking to dinner, and Crisp and his mother shopping, were filmed on Commercial Street in downtown Astoria.
* The exterior portions of the restaurant scene were filmed outside the Seafare Restaurant at the Red Lion Inn, 400 Industry St., in Astoria.
* Scenes at Joyce and Dominics house (interior and exterior) were filmed at a private residence located at 414 Exchange St., Astoria.
* Highway scenes were filmed on U.S. 26 east of Seaside, Oregon, 20 miles from Astoria.
* The school picnic was filmed at Ecola State Park near Cannon Beach, Oregon, 25 miles south of Astoria.

Information on Astoria-area locations are courtesy of the Astoria & Warrenton Area Chamber of Commerce. 

Interior school scenes were shot at Universal Studios in Hollywood.  The films opening scene was filmed at the Westfield MainPlace in Santa Ana, California and South Coast Plaza in Costa Mesa, CA. 

==Reception==
The film received mixed reviews from critics. On Rotten Tomatoesthe film has a rating of 51%, based on 35 reviews.  On Metacritic, it has a score of 61 out of 100, based on 15 critics, indicating "generally favorable reviews". 

Reviewer Caryn James of The New York Times has said, "Like Twins (1988 film)|Twins, which was also directed by Ivan Reitman, nothing in the film is as funny as the idea of it."   Empire he said "with a heart of purest mush, the film still manages to be generally entertaining" and gave it 3 stars out of 5.  In a review for EW.com, it was said at the time of release that "the movie never quite gels" and it is not going to generate quite the mega-hit business their producers are counting on", and gave the movie a C grade.  Roger Ebert said it is made up of two parts that shouldnt fit, but somehow they do, making a slick entertainment out of the improbable, the impossible and Arnold Schwarzenegger" and awarded the film three stars. 
 Criterion Collection, policier and family comedy genres in the same film.      It was officially released on Blu-ray on July 1, 2014.

===Box office===
Despite the mixed reviews, the film was a box office success, making over $200 million worldwide. 

==Soundtrack==
  
{{Infobox album Name = Kindergarten Cop: Original Motion Picture Soundtrack  Type = Film score Artist = Randy Edelman  Longtype = Digital download / Audio CD) Cover = Kindergarten Cop (Original Motion Picture Soundtrack).jpg Released = August 31, 1993 Length = Label = Varese Sarabande Reviews =
}}
;Tracklist:
#  "Astoria School Theme"   
#  "Childrens Montage"   
#  "Love Theme (Joyce)"   
#  "Stalking Crisp"   
#  "Dominics Theme/A Rough Day"   
#  "The Line Up/Fireside Chat"   
#  "Rain Ride"   
#  "The Kindergarten Cop"   
#  "Poor Cindy/Gettysburg Address"   
#  "A Dinner Invitation"   
#  "Love Theme Reprise"   
#  "A Magic Place"   
#  "Kimball Reveals the Truth"   
#  "The Tower/Everything Is OK"   
#  "Fire at the School"   
#  "Closing"  

==See also==
 
* List of American films of 1990

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 