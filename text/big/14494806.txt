A Boy Named Sue (film)
{{Infobox Film
| name           = A Boy Named Sue
| image          = BoyNamedSueFilm.jpg
| image_size     = 
| caption        = 
| director       = Julie Wyman
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Julie Wyman
| distributor    = Women Make Movies
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
A Boy Named Sue is a 2001 documentary film directed by Julie Wyman. It shows the life and transition of Theo, a young adult, raised female, involved in a lesbian relationship, who undergoes various stages of a sex reassignment surgery (including a mastectomy and hormone therapy) to become male. The protagonist is filmed extensively throughout, gives a number of interviews, and eventually settles down as a gay male. The films title is taken from the song A Boy Named Sue.

==Distribution and reception==
The film played at several festivals including the 2000 San Francisco International Lesbian and Gay Film Festival and Reel Affirmations. {{cite web
  | title =Release dates for A Boy Named Sue
  | publisher =Internet Movie Database
  | url =http://www.imdb.com/title/tt0297034/releaseinfo
  | accessdate =2008-01-03  }}
  {{cite web
  | last =Halberstam
  | first =Judith
  | title =A Boy Named Sue
  | publisher =Women Make Movies
  | url =http://www.wmm.com/filmcatalog/pages/c524.shtml
  | accessdate =2008-01-03  }} GLAAD Media Award. {{cite web
  | title =Awards for A Boy Named Sue
  | publisher =Internet Movie Database
  | url =http://www.imdb.com/title/tt0297034/awards
  | accessdate =2008-01-03  }} 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 


 
 