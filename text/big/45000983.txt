Shri Chaitanya Mahaprabhu (film)
{{Infobox film
| name           = Shri Chaitanya Mahaprabhu 
| image          = Shri_Chaitanya_Mahaprabhu.jpg
| image_size     = 
| caption        = Bharat Bhushan as Chaitanya Mahaprabhu
| director       = Vijay Bhatt
| producer       = Vijay Bhatt
| writer         = 
| narrator       = 
| starring       = Bharat Bhushan Durga Khote Ameeta Madan Puri
| music          = R. C. Boral
| cinematography = V. N. Reddy
| editing        = 
| distributor    = 
| studio         = Prakash Pictures
| released       = 1954
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1954 Hindi Kanhaiyalal and B M Vyas. 

The film was about the 15th century "medieval Vaishnav poet saint" and social reformer of Bengal, Chaitanya Mahaprabhu, whom many considered as an Avatar of Krishna.     He was himself a Krishna devotee who is revered and worshipped even today.

==Plot== Gaya to perform prayers for his father. There he meets a saint called Ishvara Puri. He soon develops complete devotion to Krishna and starts chanting and singing his praises. His Radha and Krishna Bhakti songs attract people from all walks of life. He takes renunciation and shaves off his head. He settles in Puri, concentrating on his Bhakti and disappears while singing a bhajan.

==Cast==
* Durga Khote
* Bharat Bhushan
* Ameeta Kanhaiyalal
* Madan Puri
* B. M. Vyas
* Krishna Kumari
* Sulochana Chatterji
* Umakant

==Awards== Filmfare Best Actor Award for Shri Chaitanya Mahaprabhu for 1954.  

==Soundtrack==
Music directed by R. C. Boral and lyrics by Bharat Vyas. The playback singers of the 16 (one repeat) songs were Mohammed Rafi, Lata Mangeshkar, Asha Bhosle, Talat Mehmood, Binota Chakraborty, Dhananjay Bhattacharya. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Preetmaye Sansar Pyare
| Talat Mehmood
|-
| 2
| Sar Pe Gagariya Tirchi Najariya
| Lata Mangeshkar and chorus
|-
| 3
| Woh Gokul Ka Gwala Tha
| Dhananjay Bhattacharya, Lata Mangeshkar
|-
| 4
| Teri Preet Samajh Na Aaye
| Lata Mangeshkar
|-
| 5
| Kya Ho Gaya Kya Mujhe Ho Gaya
| Talat Mehmood and chorus
|-
| 6
| Band Huye Sawan Dwar Sabhi
| Dhananjay Bhattacharya
|-
| 7
| Deen Bandhu Bhaav Sindhu Taarak
| Mohammed Rafi
|-
| 8
| Gokul Ke Is Raas Ko Karne Chaknachur
| Asha Bhosle, Mohammed Rafi
|-
| 9
| Kajrare Naina Chhup Chhup Ghaat Kare 
| Lata Mangeshkar
|-
| 10
| Baaje Muraliyaa Baaje, Sapane Hue Saanche 
| Lata Mangeshkar
|-
| 11
| Gokul Gaon Kadamb Ki Chhaon 
| Mohammed Rafi
|-
| 12
| Nimaayi Chand Gore Chand 
| Binota Chakraborty
|-
| 13
| Hari Hari Bol Mukund Madhav 
| Dhananjay Bhattacharya
|-
| 14
| Kanha Kanha Poonam Ki Raat Hai 
| Asha Bhosle
|-
| 15
| Yada Yada Hi Dharmasya Kahan Gaya Woh Vachan 
|
|}

==References==
 

==External links==
* 

 

 
 
 
 
 