Testament of Youth (film)
 
 
{{Infobox film
| name           = Testament of Youth
| image          = Testament of Youth (film) POSTER.jpg
| alt            = 
| caption        = UK theatrical release poster
| director       = James Kent
| producer       = Rosie Alison David Heyman
| writer         = Juliette Towhidi
| based on       =  
| starring       = Alicia Vikander Kit Harington Colin Morgan Emily Watson Hayley Atwell Dominic West Miranda Richardson
| music          = Max Richter
| cinematography = Rob Hardy
| editing        = Lucia Zucchetti
| studio         = BBC Films Heyday Films Lionsgate
| released       =  
| runtime        = 129 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} First World same name Oxford studies to become a war nurse.     The film was directed by James Kent and written by Juliette Towhidi.

==Plot== serve at front lines. Brittain follows their sacrifice, leaving college to join the Voluntary Aid Detachment as a nurse tending the wounded and dying (both British and German) in London, Malta and France.   

==Cast==
* Alicia Vikander as Vera Brittain 
* Kit Harington as Roland Leighton  Victor Richardson 
* Taron Egerton as Edward Brittain 
* Dominic West as Mr Brittain 
* Emily Watson as Mrs Brittain 
* Joanna Scanlan as Aunt Belle 
* Hayley Atwell as Hope  Jonathan Bailey as Geoffrey Thurlow 
* Alexandra Roach as Winifred Holtby 
* Anna Chancellor as Mrs Leighton 
* Miranda Richardson as Miss Lorimer
* Charlotte Hope as Betty

==Production== BBC2 in 1979 with Cheryl Campbell as Vera Brittain.

Film development had the support of Shirley Williams, Brittains daughter, and of Mark Bostridge, Brittains biographer, editor, and one of her literary executors, who was reportedly acting as consultant on the film. Heyday Films producers David Heyman and Rosie Alison would be producing the film with the BBC, while Juliette Towhidi was adapting the film.  James Kent would direct the film.  On 4 February 2014 Protagonist Pictures came on board to handle international sales and launched the film at the Berlin International Film Festival in that month. 

===Casting=== Jonathan Bailey and Anna Chancellor.   

===Filming===
Principal photography began on 16 March 2014 in London, Oxford and Yorkshire, including a number of locations across the North York Moors.      
 Ravenscar and Welbeck Estate in Nottinghamshire provided several locations, including the scenes at Uppingham School, Melrose house and the Étaples field hospital. The lake scenes were filmed in Darley Dale in Derbyshire.
 Victor Richardson, Morgan interviewed a series of ex-service men and women; he contacted Blind Veterans UK and spent a day at the charitys Brighton Centre, where he received the same training as blind veterans, while blindfolded. 

===Music=== musical score was composed by Max Richter,  after taking over for Mark Bradshaw who was previously attached. 

==Distribution==
===Marketing=== trailer was released on 1 August 2014.  The second trailer was then released on 10 November 2014. 

A book by   on 4 December 2014. The book includes a chapter on the making of the film. 

===Theatrical release=== British Film Institute London Film Festival on 14, 16 and  17 October  2014.    On 16 January 2015, Sony Pictures Classics acquired the distribution rights to the film for North America, Latin America and Asia from Protagonist Pictures.  The film will be released in France on 24 June 2015. 

==Reception==
===Critical response===
Testament of Youth was well received upon its release. Review aggregator Rotten Tomatoes reports that 84% of 25 film critics have given the film a positive review, with an average rating of 6.6 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 75 based on six reviews. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 