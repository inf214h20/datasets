Down Terrace
{{Infobox film
| name           = Down Terrace
| image          = 
| alt            =  
| caption        = 
| director       = Ben Wheatley
| producer       = Andrew Starke
| writer         = Robin Hill Ben Wheatley
| starring       = Robert Hill Robin Hill Julia Deakin
| music          = Jim Williams
| cinematography = Laurie Rose
| editing        = Robin Hill Ben Wheatley
| studio         = 
| distributor    = Magnolia Pictures
| released       =   |df=y}}
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = $30,000   
| gross          = $9,812 
}}

Down Terrace is a 2009 British crime film directed by Ben Wheatley and starring Robert Hill, Robin Hill and Julia Deakin.

== Plot ==
Upon release from prison, Bill (Robert Hill) and his son Karl (Robin Hill) arrive home at Down Terrace in Brighton. With the help of his wife Maggie (Julia Deakin), Bill decides to find the rat in his criminal operation and a tale of recrimination, betrayal and murder ensues. Meanwhile, Karl grows increasingly edgy and uncomfortable with his dysfunctional family. When Karls girlfriend Valda shows up pregnant, Karl announces that they plan to get married, but his parents disapprove and demand that he get a paternity test. Bills employee Garvey tells Karl that Valda dated Garveys brother for a while recently, which enrages Karl; Karl murders Garvey and enlists Erics help in secretly burying the body. Worried about Garveys unexplained disappearance and that his personal assassin might talk about a previous attempt on Garveys life, Bill orders Eric to murder Pringle and Pringles wife, leaving their three-year-old son an orphan. Eric himself is murdered when Maggie doubts his loyalty. The carnage attracts Johnny, a London gangster, who tells Bill that the lack of subtlety and stability has put Karl and his family at risk; Maggie promises to rein in Bill. Karl, who suspects that his parents have murdered Eric, accuses them of making deals with the police after he hears a death-bed confession from Berman, their lawyer. Eventually, Valda talks Karl into murdering his parents: Karl shoots his father to death, and Valda stabs his mother.

== Cast ==
* Robert Hill as Bill
* Robin Hill as Karl
* Julia Deakin as Maggie David Schaal as Eric
* Tony Way as Garvey
* Kerry Peacock as Valda
* Michael Smiley as Pringle
* Mark Kempner as Councillor Berman
* Gareth Tunley as Johnny
* Kali Peacock as Mrs Garvey

== Release ==
Down Terrace premiered at the 2009 Fantastic Fest.   It was released on DVD on 17 May 2011. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 85% of 34 surveyed critics gave the film a positive review; the average rating is 6.7/10.   Metacritic rated it 68/100.   Lou Lumenick gave the film three stars out of four, saying about Ben Wheatley that "He may finally get Hollywoods attention with this profanely funny comedy he shot in just eight days".   Stephen Holden of The New York Times called it a "grimly amusing" and "persuasively acted" film that "has too many narrative gaps for its pieces to cohere satisfactorily."   Anthony Quinn of The Independent rated it 4/5 stars and called it a "genuinely different" gangster film that shows great promise for Wheatley.   Robert Bell of Exclaim! called it "an anomalous and consistently hilarious, if flawed, comedy of idiosyncrasy and misanthropy."   David Parkinson of Empire (film magazine)|Empire rated it 3/5 stars and called it a "bleakly hilarious reclamation of the British crime genre from peddlers of mockney muppetry."    Philip French of The Guardian called it a "highly entertaining, low-budget black comedy".   Michael Rechtshaffen of The Hollywood Reporter wrote, "Theres a deadpan streak of larceny coursing through the corroded pipes of "Down Terrace," a darkly comedic approach to the British working-class social realism inhabited by Ken Loach and Mike Leigh."   Ronnie Sheib of Variety (magazine)|Variety wrote, "Cleverly channeling gangster tropes through a British kitchen sink drama|kitchen-sink soap opera, TV scribe-helmer Ben Wheatley has concocted a nifty black comedy, with a little help from his friends, in Down Terrace."   Kevin Thomas of the Los Angeles Times called it a "distinctive and idiosyncratic" film that "is long on talk but generates its own internal rhythms and pace that makes it feel bracing and vibrantly alive."   Lisa Schwarzbaum of Entertainment Weekly rated the film A- and called it "a dark and hilarious thwomping of the whole miserablist British gangster genre."   Jason Anderson of the Toronto Star called it "an enjoyably nasty piece of business" that is "both horrific and hilarious". 

=== Awards and recognition ===
Down Terrace won the Raindance Award at the British Independent Film Awards 2009  and the Next Wave Awards for Best Feature and Best Screenplay at the Fantastic Fest 2009. 

On 8 February 2011 Ben Wheatley won the award of Most Promising Newcomer for Down Terrace at the Evening Standard British Film Awards for 2010. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 