Vengeance of Rannah
{{Infobox film
| name           = Vengeance of Rannah
| image          =
| image_size     =
| caption        =
| director       = Bernard B. Ray
| producer       = Bernard B. Ray (producer) Harry S. Webb (associate producer) Joseph ODonnell (screenplay)
| narrator       =
| starring       = Bob Custer, Rin Tin Tin Jr.
| music          =
| cinematography = Paul Ivano
| editing        = Holbrook N. Todd
| distributor    = Reliable Pictures
| released       = 1936
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 western directed by Bernard B. Ray and produced by Ray and Harry S. Webb for Reliable Pictures, starring Bob Custer and Rin Tin Tin Jr.

== Cast ==
*Rin Tin Tin Jr. as Rannah
*Bob Custer as Ted Sanders John Elliott as Doc Adams
*Victoria Vinton as Mary Warner Roger Williams as Frank Norcross
*Oscar Gahan as Henchman Nolan Eddie Phillips as Henchman Macklin Ed Cassidy as Sam, posing as Barlow Wally West as Deputy Barlow

==Plot==
Insurance investigator Ted Sanders is assigned to look into a robbery/murder. When he arrives at the crime scene he is met by a local deputy. What he doesnt know, however, is that the "deputy" is actually part of the gang that committed the crime. He plants some of the stolen money in Sanders room to frame him for the crime, then arrests him.

== External links ==
* 
* 

 
 
 
 
 
 
 


 