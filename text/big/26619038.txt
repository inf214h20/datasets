Point Blank (1998 film)
 
{{Infobox film
| name           = Point Blank
| image          = Point Blank 1998.jpg
| alt            =
| caption        = DVD cover
| director       = Matt Earl Beesley
| producer       = Nile Niami Patrick D. Choi
| writer         = Jim Bannon Chuck Konzelman Daniel Raskov Cary Solomon
| starring       = Mickey Rourke Danny Trejo
| music          = Stephen Edwards
| cinematography = Keith L. Smith
| editing        = Edward R. Abroms
| studio         = Interlight New Cinema Co.
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Point Blank is a direct-to-video film directed by Matt Earl Beesley and starring Mickey Rourke.  The film was shot on location in Fort Worth, Texas.

==Plot==
 
A group of escapees from prison take over a shopping mall, only to be stopped by the brother of one of the fugitives.

==Cast==
* Mickey Rourke as Rudy Ray
* Danny Trejo as Wallace Kevin Gage as Joe Ray
* James Gammon as Dad
* Frederic Forrest as Mac Bradford
* Paul Ben-Victor as Howard

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 
 