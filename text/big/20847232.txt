Strike Up the Band (film)
{{Infobox film
| name           = Strike Up the Band
| image          = Strike-Up-the-Band-1940.jpg
| image size     =
| caption        = Film poster
| director       = Busby Berkeley
| producer       = Arthur Freed John Monks Jr.  Fred F. Finklehoffe
| narrator       =
| starring       = Judy Garland Mickey Rooney
| music          = Leo Arnaud George Stoll
| cinematography = Ray June Ben Lewis
| studio         = Loews Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $854,000  . 
| gross          = $3,494,000 
| preceded by    =
| followed by    =
}}
Strike Up the Band is a 1940 American black and white musical film. It is directed by Busby Berkeley and stars Mickey Rooney and Judy Garland.

A very famous, memorable quote from the film is "Take that boy on the street. Teach him to blow a horn, and hell never blow a safe.", spoken by Paul Whiteman.

As well as being commercially released as a VHS in its own right, it was also released on 25 September 2007 by  , Babes on Broadway, Girl Crazy, and Strike Up the Band. 

The original taglines for the film were:  "THEIR SUNNIEST, FUNNIEST, DOWN-TO-MIRTHIEST HIT!; IT BEATS THE BAND!; "Melodious with WHITEMANS BAND; and The merriest pair on the screen in a great new musical show!

In keeping with MGMs practice of the time, the film soundtrack was recorded in stereophonic sound but released with conventional monaural sound.  At least some of the original stereo recording has survived and been included in some home video releases, including the Mickey Rooney - Judy Garland Collection. 

==Plot summary==
Jimmy Connors, a student at Riverwood High School, also plays the drums in the school band, but dreams of playing in a dance band. He and his "pal" Mary Holden sell the school principal the idea of forming the band and putting on a dance. The principal is initially doubtful, but then agrees to buy the first ticket. The event is a success and the schools debt for the instruments in paid off.

Famous band leader Paul Whiteman (played by himself) sponsors a contest in Chicago for the best high school musical group, and Jimmy decides the band must compete. In three weeks, the kids write, plan, and put on a show (without apparently affecting their schoolwork). The melodrama, called Nell from New Rochelle, is also a success and raises the money to go to Chicago, but theyre still short. A loan from Paul Whiteman himself deals with that obstacle. But when Willie, a member of the cast, is injured and needs a critical and urgent operation, the band gives the money up so Willie can be flown to Chicago for the operation.

The band raises the money anyway, competes in Chicago, and wins the $500 prize. To Jimmy goes the honor of leading all the bands in a grand finale performance.

==Cast==
*Mickey Rooney - James Jimmy Connors
*Judy Garland - Mary Holden Paul Whiteman and His Orchestra - Group Performers
*June Preisser - Barbara Frances Morgan
*William Tracy - Philip Phil Turner
*Larry Nunn - Willie Brewster
*Paul Whiteman - Paul Whiteman
*Margaret Early - Annie
*Ann Shoemaker - Mrs. Jessie Connors
*Francis Pierlot - Mr. Judd
*Virginia Brissac - Mrs. May Holden
*George Lessey - Mr. Morgan
*Enid Bennett - Mrs. Morgan
*Howard C. Hickman - Doctor (as Howard Hickman) Sarah Edwards - Miss Hodges, a Teacher
*Milton Kibbee - Mr. Holden
*Helen Jerome Eddy - Mrs. Brewster

==Soundtrack==
*"Strike up the Band"
:(1927)
:Music by George Gershwin
:Lyrics by Ira Gershwin
:Played during the opening credits
:Sung by Judy Garland, Mickey Rooney, and chorus in the finale
*"Our Love Affair"
:(1939)
:Music by Roger Edens
:Lyrics Arthur Freed
:Played during the opening and end credits
:Played on piano by Mickey Rooney and sung by Judy Garland and Mickey Rooney with orchestral accompaniment
:Reprised by the animated fruit orchestra
:Reprised by the band at rehearsal and at the dance
:Reprised by Judy Garland and Mickey Rooney in the finale
:Played as background music often
*"Do the La Conga"
:(1939)
:Music and Lyrics by Roger Edens
:Performed by Judy Garland, Mickey Rooney, Sidney Miller,
:William Tracy and chorus at the dance
:Reprised by the cast in the finale
*"Nobody"
:(1939)
:Music and Lyrics by Roger Edens
:Sung by Judy Garland
*"Oh Where, Oh Where Has My Little Dog Gone?"
:(uncredited)
:Traditional
:Played as background music at the start of the fair sequence
*"The Gay Nineties"
:Music and Lyrics by Roger Edens
:Performed by Judy Garland, Mickey Rooney, William Tracy,
:Margaret Early and chorus at the Elks Club show
*"Nell of New Rochelle"
:(1939)
:Music and Lyrics by Roger Edens
:Performed by Judy Garland, Mickey Rooney and chorus in the Elks club show
*"Sidewalks of New York"
:(1894) (uncredited)
:Music by Charles Lawlor
:A few notes played at the start of the Nell of New Rochelle sequence
*"Walking Down Broadway"
:(uncredited)
:Traditional
:Music Arranged by Roger Edens
:Sung by the chorus in the Nell of New Rochelle sequence
*"A Man Was the Cause of It All"
:(1939)
:Music and Lyrics by Roger Edens
:Sung by Judy Garland in the Nell of New Rochelle sequence
* "After the Ball"
:(1892) (uncredited)
:Music by Charles Harris
:Played as dance music in the Nell of New Rochelle sequence
* "Sobre las olas (Over the Waves)"
:(1887) (uncredited)
:Music by Juventino Rosas
:Played as background music in the Nell of New Rochelle sequence
*"Heaven Will Protect the Working Girl"
:(1909) (uncredited)
:Music by A. Baldwin Sloane
:Lyrics by Edgar Smith
:Sung by Judy Garland, Mickey Rooney and chorus in the Nell of New Rochelle sequence
*"Home! Sweet Home!|Home, Sweet Home"
:(1823) (uncredited)
:Music by H.R. Bishop
:Played as background music when Nell rocks the cradle
*"Ta-ra-ra Boom-der-é"
:(1891) (uncredited)
:Written by Henry J. Sayers
:Danced to and sung by June Preisser and sung by the chorus in the Nell of New Rochelle sequence
:Reprised in the finale of the Nell of New Rochelle sequence
*"Come Home, Father"
:(1864) (uncredited)
:Music and Lyrics by Henry Clay Work (1864)
:Sung by Larry Nunn and Judy Garland in the Nell of New Rochelle sequence
*"The Light Cavalry Overture"
:(uncredited)
:Music by Franz von Suppé
:Played in the Nell of New Rochelle sequence several times
*"Rock-a-Bye Baby"
:(1886) (uncredited)
:Music by Effie I. Canning
:Played as background music when Willie is told to go home
*"Five Foot Two, Eyes of Blue (Has Anybody Seen My Girl)?"
:(uncredited)
:Music by Ray Henderson
:Played as background music when Jimmy and Barbara wait for her parents
*"When Day is Done"
:(uncredited)
:Music by Robert Katscher
:Opening number played by Paul Whiteman and Orchestra at Barbaras party
*"Wonderful One"
:(uncredited)
:Music by Paul Whiteman and Ferde Grofé Sr.
:Played as dance music by Paul Whiteman and Orchestra at Barbaras party
*"Drummer Boy"
:(1939)
:Music by Roger Edens
:Lyrics by Roger Edens and Arthur Freed
:Performed at Barbaras party by Judy Garland, Mickey Rooney (on drums and vibraphone) and other band members
:Reprised by the cast in the finale
*"China Boy"
:(uncredited)
:Written by Dick Winfree and Phil Boutelje
:Played as background music during the travel and contest montage
*"Hands Across the Table"
:(1934) (uncredited)
:Music by Jean Delettre
:Played as background music during the travel and contest montage Limehouse Blues"
:(1922) (uncredited)
:Music by Philip Braham
:Played as background music during the travel and contest montage
*"Tiger Rag"
:(1918) (uncredited)
:Written by Edwin B. Edwards, Nick LaRocca, Tony Sbarbaro, Henry Ragas and Larry Shields
:Played as background music during the travel and contest montage
*"Columbia, the Gem of the Ocean"
:(1843) (uncredited)
:Music Arranged by Thomas A. Beckett
:Played as background music when the flag is raised at the end

==Awards==
In 1941, the year after the film was released, the film was nominated for three Academy Awards.    Douglas Shearer (M-G-M SSD) won an Oscar for Best Sound, Recording and Roger Edens and George Stoll were nominated for an Oscar in the category of Best Music, Original Song for the song "Our Love Affair". George Stoll and Roger Edens were also nominated for an Oscar in the category of Best Music, Score.

==Box Office==
According to MGM records the film earned $2,265,000 in the US and Canada and $1,229,000 elsewhere resulting in a profit of $1,539,000. 

==Film Connections==
The film is referenced in:
*The Big Store (1941) -  Seen on Marquee opposite store, without the names of Mickey Rooney and Judy Garland and
*"  (#5.6)" (1974) -  Movie title used in title
*"Alice: Alice Strikes Up the Band (#5.18)" (1981) -  title reference
*Life with Judy Garland: Me and My Shadows (2001) (TV)

The film is featured in:
*We Must Have Music (1942)
*Thats Entertainment! (1974)
*"  (#1.9)" (1989)
*  (1996) (TV)
*  (1998) (TV)

The film is spoofed in
*"  (#1.4994)" (2006) - Paraphrases the title (only one letter in first word differs)

==Critical response==
Paul Mavis, DVDTalk: 
:"This is American salesmanship at its most uplifting and technically proficient; no wonder there were widespread reports of cheering in movie theatres during this finale."

Daily Variety:
:"While all the young principals do themselves proud, Garland particularly achieves rank as one of the screens great personalities. Here she is for the first time in the full bloom and charm which is beyond childhood, as versatile in acting as she is excellent in song - a striking figure and a most oomphy one in the wild abandon of the La Conga."

Movie and Radio Guide, 1940:
:"The La Conga danced by Mickey Rooney and Judy Garland in Strike Up the Band is nothing less than sensational. For that reason, Movie and Radio Guide hereby christens the number The Roogaronga. This title is a combination of the first three letters of Mickeys and Judys last names, to which has been added the identifying dance classification."

Variety, September 18, 1940:
:"Strike Up the Band is Metros successor to Babes in Arms, with Mickey Rooney, assisted by major trouping on the part of Judy Garland ... Picture is overall smacko entertainment ... and Mickey Rooney teamed with Judy Garland is a wealth of effective entertainment."

==International versions==
The film is known by a variety of different names all around the world, including Armonías de juventud in Spain, En avant la musique in Belgium (dubbed version) (French title), Heiße Rhythmen in Chicago in Germany, Me jazzikuninkaat in Finland, Musica indiavolata in Italy, Vi jazzkonger in Denmark and Vi jazzkungar in Sweden.

==References==
 

==External links==
 
 
*  
*  
*  
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 