Daddy's Little Girls
 
{{Infobox film
| name = Daddys Little Girls
| image = Gabrielle union14.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Tyler Perry
| producer = Tyler Perry Reuben Cannon
| writer = Tyler Perry
| starring = Gabrielle Union Idris Elba Louis Gossett, Jr. Tracee Ellis Ross Terri J. Vaughn Malinda Williams
| music = Brian McKnight
| cinematography = Toyomichi Kurita	
| editing = Maysie Hoy Lionsgate Tyler Perry Studios Reuben Cannon Productions 
| distributor = Lionsgate
| released =  
| runtime = 95 minutes
| country = United States
| language = English
| budget =
| gross = $31,609,243 
}}
Daddys Little Girls is a 2007  ) as well as the first of Perrys films to not be based on any of the filmmakers stage plays.

==Plot==
Monty James ( ), Lauryn (Lauryn Alisa McClain), and Sierra (Sierra Aylina McClain). He has dreams of owning his own shop someday. Montys children have been cared for by their maternal grandmother for many years. However, he realizes she is dying from lung cancer and she dies within a few weeks. His ex-wife, Jennifer (Tasha Smith), disrupts the tranquility of the service and demands that her daughters live with her. Jennifer later tells Monty that she is suing for custody. Strapped for cash and hoping to make his dream reality, Monty accepts help from Maya (Malinda Williams), an employment agency worker. Maya recommends Monty as a driver for Julia Rossmore (Gabrielle Union), an attorney in Atlanta.
 Craig Robinson), who is very loud and obnoxious. She also ends up on a blind date with a guy who she thinks is perfect for her until his wife and kids expose him as an unfaithful married man. When Monty and Julia meet, she is uptight, pushy, and insists on having everything done on schedule. Monty also finds out his eldest daughter, Sierra, accidentally started a fire in his home. Social Services were notified and the children were sent to live with their mother and her boyfriend, Joe (Gary Sturgis). Julia, sitting in the car, goes into the hospital to demand Monty to take her home, but sees Social Services sending his children to live with Jennifer. Monty returns to his day job as a mechanic and finds the owner Willie (Louis Gossett Jr.) has been injured in a robbery attempt. Willie offers to sell the shop to Monty for a $10,000 deposit. Monty later goes to Julia for help, but she refuses, leaving Monty to defend himself. However, during the court case, Julia walks in and defends him.

The next evening, Monty goes to see Julia, who is astonished to see that he has provided for his daughters their entire lives. Julia finishes Monty’s case preparation and Monty walks Julia out of the office to her car. Monty discovers it is her 32nd birthday and takes her to his favorite jazz club. Eventually, Julia dances with Monty. As they are traveling home, Julia kisses Monty and asks him to spend the night. Monty is willing but Julia is too drunk and vomits in the bathroom. Julia changes her mind and tells Monty to go home. Over the next few weeks, Julia begins to feel confused. Monty is a decent guy, but she is a successful woman trying to find a successful man. She goes to Montys apartment to meet his daughters during his visitation right. When theyre at the aquarium, Julia sees one of her friends there and gets reminded that she has to be conscious of decisions which impact her career. Monty overhears Julia’s friend and is hurt.

At the child custody hearing, Julia argues that it would be in the childrens best interest for Monty to be awarded custody. Jennifer’s lawyer says that Monty is irresponsible due to an earlier conviction of statutory rape. Julia feels betrayed because she didn’t hear this information from Monty earlier; she leaves, telling Monty that custody will be awarded to Jennifer. Monty returns home distraught. At 3:00 am, Montys daughters arrive at his house and inform him that Joe has abused them, proving it by showing him that Joe hit China on her back , leaving a large bruise on her back. Monty drives away and ends up crashing into Jennifer and Joes car, after which he pulls Joe out of the car and he begins to beat him  and a crowd gathers. Joes thugs arrive and begin to attack Monty, but the crowd defends Monty against them. Julia sees a report on the incident, in which Monty is identified as having been "falsely convicted" of rape.

Everyone appears in court. Jennifer and Joe are facing drug charges, because cocaine was found in Joes car and house, while Monty is to be charged with assault. Julia arrives just in time to represent Monty, apologizing for not hearing his side of the story. The witnesses all agree to testify against Joe but refuse to testify against Monty, so Jennifer and Joe are jailed without bail while the case against Monty is dropped.  Monty tells Julia that he loves her.  Montys daughters greet him and Julia at the auto shop that now bears his name. Monty and Julia kiss, and the neighbors celebrate Montys success.

==Cast==
* Idris Elba as Monty James, a hard-working mechanic and the protagonist
* Gabrielle Union as Julia Rossmore, Montys lawyer and love interest
* Louis Gossett, Jr. as Willie
* China Anne McClain as China James
* Lauryn Alisa McClain as Lauryn James
* Sierra Aylina McClain as Sierra James
* Tasha Smith as Jennifer Jackson, Montys ex-wife and the films main antagonist who is bent on getting custody of their three daughters
* Gary Sturgis as Joe, Jennifers gangster boyfriend and drug dealer who has the town in fear. He is the secondary antagonist.
* Terri J. Vaughn as Brenda, Julias lawyer friend who does not approve of Monty
* Malinda Williams as Maya Elizabeth
* Tracee Ellis Ross as Cynthia, Julias other colleague friend who does not like Monty
* Cassi Davis as Rita, Jennifers aunt and sister of Jennifers mom, Kat
* LaVan Davis as Lester
* Timon Kyle Durrett as Ronald Johnson
* Brian J. White (uncredited) as Christopher Craig Robinson (Cameo appearance|cameo) as Byron, a loud-mouthed, middle-aged rapper.
* Juanita Jennings as Kat Jackson, Jennifers mom who died in the beginning of the story
* Mazviita Foto as Amy

==Reception==

===Box office=== Ghost Rider, Bridge to Terabithia, Norbit s second weekend, and Music and Lyrics,  and has grossed $31,609,243 worldwide, making it Tyler Perrys lowest-grossing film.

===Critical reaction=== Ebert & Richard Roeper|Roeper.

==Soundtrack==
 , Dionne Warwick, and Cissy Houston. The song "Cant let You Go" by Anthony Hamilton is not on the soundtrack. Also, the song "Beautiful" by Meshell Ndegeocello is featured in the movie.
 Anthony Hamilton featuring Jaheim & Musiq Soulchild - "Struggle No More (The Main Event)"
# R. Kelly - "Dont Let Go" Tamika Scott of "Xscape" - "Greatest Gift"
# Adrian Hood - "Brown Eyed Blues"
# Whitney Houston, Cissy Houston, Dionne Warwick and The Family - "Family First"
# Yolanda Adams - "Step Aside"
# Brian McKnight - "I Believe"
# Beyoncé Knowles - "Daddy (Beyoncé Knowles song)|Daddy" Anthony Hamilton - "Struggle No More" Governor - "Blood, Sweat & Tears"
# Charles "Gator" Moore - "A Change Is Gonna Come"

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 