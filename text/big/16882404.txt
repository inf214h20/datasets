Lady Robinhood
 
{{Infobox film
| name           = Lady Robinhood
| image          = LadyRobinHood.jpg
| caption        = Film still
| director       = Ralph Ince
| producer       = 
| writer         = Clifford Howard Burke Jenkins Fred Myton Robert Ellis Boris Karloff
| music          = 
| cinematography = Silvano Balboni
| editing        = 
| distributor    = Robertson-Cole Pictures Corporation
| released       =  
| runtime        = 6 reels
| country        = United States  Silent (English intertitles)
| budget         = 
}} silent drama film directed by Ralph Ince, starring Evelyn Brent, and featuring Boris Karloff.   

==Cast==
* Evelyn Brent as Señorita Catalina / La Ortiga Robert Ellis as Hugh Winthrop
* Boris Karloff as Cabraza William Humphrey as Governor
* DArcy Corrigan as Padre
* Robert Cauterio as Raimundo

==Preservation status==
This is now considered a lost film,    but a trailer for the film survives in the collection of the Library of Congress.

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 