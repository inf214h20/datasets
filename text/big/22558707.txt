Tormented (2009 British film)
{{Infobox film
| name = Tormented
| image = Tormented-2.jpg
| image_size = 
| alt = 
| caption = Original release poster
| director = Jon Wright
| producer = Cavan Ash Tracy Brimm Arvind Ethan David Kate Myers
| writer = Stephen Prentice
| starring = Alex Pettyfer Tuppence Middleton April Pearson Calvin Dean Dimitri Leonidas Tom Hopper Larissa Wilson Georgia King Sophie Wu
| music = Paul Hartnoll
| cinematography = Trevor Forrest
| editing = Matt Platts-Mills
| studio = BBC Films Pathé Forward Films
| distributor = Warner Bros.   Paramount Vantage   MPI Home Video   IFC Films  
| released =  
| runtime = 91 minutes
| country = United Kingdom
| language = English
| budget = £700,000
| gross = £284,757
}} Orbital member Paul Hartnoll.

==Plot==
 
Head girl Justine Fielding (Tuppence Middleton) is escorted out of Fairview High School by the police, as other pupils look on. 
 hypocrite because she did not really know him. He is then thrown out by the sadistic PE teacher. Later on, Justine is talking to her friend Helena (Mary Nighy) when school heart-throb Alex (Dimitri Leonidas) decides to invite her to a party that his popular friends, Bradley (Alex Pettyfer), Tasha (April Pearson), Khalillah (Larissa Wilson), Sophie (Georgia King) and Marcus (Tom Hopper), are having. She decides to go.

At the party, when Justine arrives, Tasha tells Jez (Ben Lloyd-Hughes), the DJ, to say something about her appearance. He begins to rap about her in an unflattering manner. When Alex tells him to stop, he is literally thrown out of the party by Bradley. Later, Alex and Justine go to one of the bedrooms and kiss. Meanwhile, Bradley, Tasha, Khalillah, Marcus and Sophie all receive insulting and degrading text messages from Mullets number. During this, someone puts on a clown costume and grabs a chainsaw, before heading up to Alex and Justine. It is Bradley, who is pranking them.

Jez goes to the cemetery and urinates on Mullets grave. He is stabbed. 
 James Floyd), the leader of the schools emo clique, about removing a website he had put up about Mullet. Justine is persuaded to hang around with the popular crowd, as well as tells Helena that she wont be able to make it to her DVD night. She finds a teddy bear (stolen from Mullets grave) in her locker and assumes it is from Alex. He asks her on another date after this, which she agrees to. She goes to see Jason and he tells her that Mullet was in love with her and hands her his suicide note. While she is reading the note, a teacher tells her that she will make a great pupil at Oxford University. In the schools recording studio, Nasser is then attacked and sees Mullets ghost. The ghost increases the volume of Nassers music and forces him to listen, rendering him deaf. He is taken to the ambulance, yelling to everyone: "Mullets back!". However, no one believes him.

The gang get more messages. Bradley believes Jason is sending them. Jason, meanwhile is in art class. The art teacher tells everyone that the guillotine is faulty and they shouldnt touch it. A boy sent by Bradley fetches Jason out of class, then Bradley and Marcus assault him, but they are interrupted by another message, showing Jason is innocent. The PE teacher arrives and gives Jason detention. Later, Justine confronts Jason about the note, but he denies forging it, as well as breaks down in tears about the horrific bullying that caused Darren Mullet to kill himself. He tells Justine about the vicious texts sent to Mullet, as well as a website that Bradleys gang created about him. When Justine asks who was responsible for Mullets suicide, Jason glances at the popular kids. She shows Alex the suicide note, but his friends tell her she cannot hand it into the police, because as it accuses Justine of hating him, she would implicate herself in his death. The girls decide that Helena has been sending the messages and attack her in the bathroom, with Tasha smashing her phone. After swim practice, Sophie tells Justine that she should sleep with Alex that night, but goes back into the swimming pool, where she is attacked by Mullets ghost until she drowns.

Later, Justine and Alex sleep together, but are seen by Mullet who had brought some flowers for her. Mullet tears the badge from her uniform, as well as re-arranges her fridge magnets, calling her a "dirty slut". Marcus is then in PE class, where he sees Mullets ghost and the PE teacher tells him to go and have a shower, just as the art teacher arrives and tells the head that they have found Sophies body. In the shower, he is led to a mirror with "moron" on it which Marcus hates being called, then is brutally whipped with a towel by the ghost of Mullet while nude. He manages to fight him off with a cricket bat after painfully popping his bloodied eye back into his skull and heads outside, running into Helena, who accuses him of trying to rape her because he is semi naked. He manages to get outside, but is impaled through the skull upon a fence by the ghost. Later on, the gang are at Bradleys house, where they have an argument about who killed their friends, ending in Tasha and Justine getting into a fight and Justine is thrown into the swimming pool. Bradley decides to go and dig Mullets body up at the cemetery to prove that Mullet cannot be responsible.

Justine goes home, as well as demands that Alex shows her the website, where she tearfully witnesses the diabolical stunts inflicted on Mullet by Bradley and his friends - including Alex (who refers to Justine as "Head Girlus Frigidus Bitchus"). Justine is even more revolted when she realizes that she was rude to Mullet because he interrupted a conversation about her going to Oxford, as well as ignored his pleas for her to stop the in-crowd torturing him. She tells Alex that he is deceitful and a liar, as well as that their relationship is over.

Tasha becomes drunk in Bradleys car as he took her to dig up Mullet. After attempting to dig up Mullets corpse, Bradley breaks down and begins to cry with grief over the death of his friends. Tasha consoles him and they have sex in Bradleys car. Tasha sees Mullets ghost and tries to get Bradley off her. Despite her desperate attempt to get Bradleys attention, Mullet drags Bradley out of the vehicle and rips off his penis (this was confirmed in an interview with Alex Pettyfer) killing Bradley due to blood loss. Tasha gets free and runs into an open grave. She explains that she lied to Mullet, telling that she loved him ever since they met each other. Mullet decapitates her with shovel.

The next day, Justine arrives at the art class, where Jason has painted Edvard Munch|Munchs "The Scream" painting, as well as tells him that she was responsible for Mullets death. He tells her that it has been his fault, because he was scared of being bullied, so he told Bradley that Mullet fancied her. She leaves the room, where Mullet appears and kills him by jamming two pencils up his nose and slamming his head against the table. Justine approaches Helena and tells her that she is sorry, that her friends, including Alex were all horrible. Khalillah runs over and asks her where Tasha is, but Justine tells her she doesnt care. Khalillah then gets a text message from Tasha telling her to meet her in the art room.

Before Justine can follow, she is confronted by Alex, who tells her he is going to kill Mullet for what he has done to everyone. Justine tells him that Khalilah is in danger. Khalillah gets to the empty art room, as well as sees "The Scream" with her name on it, at which point Mullet puts a plastic bag over her head, before using the guillotine to slice her hands off. She tries to call the head teacher, but he is too busy with the police, who tell him that Justines badge has been found near the bodies of Bradley and Tasha. They get to the art room and find Jasons and Khalillahs bodies, but are attacked by Mullet. They break free, by stabbing him in the stomach with a screwdriver and head to the common room, where Mullet follows them. Alex tries to stop him, but is stabbed in the hand with the screwdriver and his hand is tightly nailed in the floor. Justine gets him to stop, but he begins to choke her to death. She grabs his inhaler and throws it across the room, telling Alex to smash it. He does and the ghost begins to die. Justine tells Alex to leave, but he starts being a bully again and punches Mullet, knocking a sofa over, as well as finding Mullets other inhaler which Alex had hidden from Mullet when he was alive. Mullet then uses it and then stabs Alex in the throat with the screwdriver. Mullet flees just as the police arrive. Justine is then led out and into the police car seen earlier, having been arrested for the murders. She sees Mullets ghost but then it disappears, what happened next to her is never revealed.

During the credits, there is a sequence where the foul-mouthed PE teacher is telling students off, while Mullet is shown getting ready for revenge on him.

==Cast==
{| class="wikitable"
|-
! Actor
! Character
! Description
|-
| Alex Pettyfer 
| Bradley White
| The rich, handsome but vicious and foul-mouthed leader of Fairviews popular crowd. Bradley is a cruel, heartless bully who abuses all outside his clique for sadistic glee. He later reveals a vulnerable side after Sophie and Marcus are killed. He meets his end in the cemetery after Mullet hauls him from his car and castrates him.
|-
| April Pearson
| Natasha Cummings
| Bradleys conniving and sadistic girlfriend who is feared by many of the girls at her school. She is far cleverer and more wicked than Sophie, as well as is a major participant in the bullying of Darren Mullet. She also shows a strong (but discreet) resentment and jealousy towards Justine. She is decapitated by Mullet after he chases her into an open grave.
|-
| Dimitri Leonidas
| Alex
| Confident and popular, Alex is an ambivalent teen who seems to have more depth than his friends. His feelings for Justine are genuine, but he fears angering Bradley, as well as claims he had no choice in bullying Mullet, but it is clear he too gained joy from it. Mullet kills him with a screwdriver to the throat.
|-
| Georgia King
| Sophie
| Sophie is the shallow and ditzy bottle-blonde of the in-crowd, who clearly revels in her elite social status. She is dating Marcus and almost displays a compassionate side, which she quickly hides beneath a sarcastic and cunning exterior. She is the first of the bullies to die. Mullet drowns her in the school swimming pool.
|-
| Larissa Wilson
| Khalillah happy slapped Mullet, who never fought back. She dies after Mullet chops off her hands in the art room.
|-
| Calvin Dean
| Darren Mullet
| Mullet was an obese, awkward student who suffered from asthma. He was nicknamed "Shrek" by his classmates, who took diabolical pleasure in torturing him verbally and physically. Mullet was deeply in love with Justine but she literally didnt even notice him. Mullet killed himself to escape the bullying, but returned from the dead to get revenge upon the popular students who made his life hell.
|-
| Tom Hopper
| Marcus
| Bradleys strong, athletic second-in-command, who pins the victim down whilst Bradley punches him. Marcus is dating Sophie, as well as is probably the only person who does not fear Bradley. He has a strange fetish for smelling girls, as well as affectionately tells Sophie she "smells like cabbage". Marcus fights back at the ghostly Mullet, but loses and is impaled on a fence through his head.
|-
| Tuppence Middleton
| Justine Fielding
| Fairview Highs angelic head girl who is unaware of the circumstances surrounding Mullets death, but after she begins dating Alex, the awful truth is slowly revealed to her. Justine is pretty and popular, yet seen as a snob by her peers. She gives a speech at Mullets funeral simply to enhance her image, as well as later laughingly admits she didnt even know who he was. Mullet grows to hate her after she begins seeing Alex and she is framed for the murders.
|-
| Mary Nighy
| Helena
| Fairviews prim deputy head girl and Justines best friend. Helena is quiet and studious and begins to resent Justine for hanging with the cool crowd of Fairview. She is later assaulted by Tasha and her friends, as well as ends her friendship with Justine.
|-
| Olly Alexander
| Jason Banks
| Mullets only friend, as well as possibly the only person in school as unpopular as him. Jason seems to have a slight hint of courage, as he is the only one to denounce the school for driving Mullet to suicide, as well as is particularly profane in his tirade about the bullies. He admits out of guilt he was the one who revealed the truth about Mullets love for Justine to Bradleys gang to protect himself. Outraged, Mullet kills him.
|-
| Sophie Wu
| Mai Lee
| Nassers gothic girlfriend.
|- Hugh Mitchell
| Tim
| A sullen goth who hangs with Nasser.
|- James Floyd
| Nasser
| The ringleader of Fairviews emos. He finds death "erogenous" and criticizes Mullet for hanging himself rather than cutting his wrists. He is attacked by Mullet in the school studio and is driven deaf after Mullet increases the music volume and forces him to listen until his ears bleed.
|-
| Peter Amory
| Headteacher
| The ineffective head of Fairview High, who shows no concern for his students and does little to end bullying in his school.
|-
| Ruby Bentall
| Emily
| A timid friend of Justines. She has no dialogue in the film.
|- Geoff Bell
| Gordon
| The malicious gym teacher, who despises weaklings - Jason in particular - and openly favours the strong, confident popular kids.
|}

==Release== world premiere took place on 19 May at the Empire Cinema, Leicester Square followed by a national premiere at Cineworld in Birmingham on 21 May 2009. Although a US release date of July 10 had been speculated, sources are lacking. The US release come over Paramount Vantage.  The film ran on the IFC Festival 2009 on 21 September 2009. 

The films budget was under £1 million ($1.5 million). 

IFC Films released the film in the US via on-demand service in late October 2009.  

==Reception==
The film has received mixed to positive reviews from critics. It holds a 69% rating and an average rating of 5.7 on Rotten Tomatoes from 27 reviews and 5.2 out of 10 on Internet Movie Database. 
 snogging take Empire called it "cynical, gruesome fun" and "consistently funny" and awarded the film 4 stars out of 5. 

Sky Movies also awarded the film 4 stars out of 5. In the news awarded the film 9 out of 10,  and Real awarded the film 5 stars. 

==Home media==
The DVD was released in the UK on 28 September 2009. It—along with the cinema release—was heavily cut in order to get a 15 rating. The DVD included an in-vision cast and director commentary and documentaries about the making of the film. The US DVD is unrated and was released on August 31, 2010.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 