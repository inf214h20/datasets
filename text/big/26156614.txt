Chrystal (film)
{{Infobox film
| name           = Chrystal
| image          =
| caption        = Theatrical poster
| alt            = The poster shows the main characters of the movie Lisa Blount (left) and Billy Bob Thornton (right) standing in a heavily forested area in the Arkansas wilderness staring off into the distance. Ray McKinnon
| producer       = Peter E. Strauss Bruce Heller David Koplan II Walton Goggins Lisa Blount Ray McKinnon
| writer         = Ray McKinnon
| starring       = Billy Bob Thornton Lisa Blount Ray McKinnon Harry Lennix Walton Goggins Grace Zabriskie Johnny Galecki Colin Fickes Max Kasch James Intveld Kathryn Howell Harry Dean Stanton
| music          = Stephen Trask
| cinematography = Adam Kimmel
| editing        = Myron Kerstein
| studio         = Ginny Mule Pictures Panache Productions First Look Studios 
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $80,858 
}} Ray McKinnon, produced the film.    The story is about a woman named Chrystal (Lisa Blount) who has been traumatized both physically and mentally from a car accident that took the life of her son. Joe (Billy Bob Thornton), Chrystals husband, has just been released from jail after a 16-year sentence stemming from multiple crimes he committed.

==Plot== Ozark Mountains. traumatic events in her past.

The movie begins in a rather dramatic fashion, depicting Joe fleeing from the police in a high speed police chase with his wife and son in the same car. While weaving down the mountain roads at a high rate of speed, Joe loses control of the vehicle and ends up rolling down a hill and, subsequently, crashing into a tree. Chrystal is severely injured in the accident, suffering a broken neck. Their son, who the police presumed was flung through the windshield, was never found at the scene of the accident, or anywhere in the surrounding areas. For his role in running from the police and causing injuries to his passengers, Joe is arrested and sentenced to 16 years imprisonmet for a variety of crimes, including fleeing to avoid justice. 

Upon his release from the state prison, Joe comes back to his home in search of a change in his life. He ends up coming back home to his wife, who hadnt divorced him even while he was away in prison. Because of Joes run-in with the law, and the resulting car accident and loss of their child, Chrystal is permanently injured in her neck that left her a one-time quadrapalegic. Although Chrystal has managed to regain limited mobility in her body, she has completely lost her enthusiasm, emotions, or will to live. As Joe begins to slowly work his way back into her life, she is unsure of whether to accept him once again, fearing what may happen if she does so. He now wants to change and atone for his past life of crime. Forced to face his past to continue with his future, Joe runs into an old enemy of his, Snake (Ray McKinnon). Worried about him, Snake invites Joe to rejoin him in his illegal drug operation. 

==Cast==

* Chrystal: the protagonist. Joes wife, she is played by actress Lisa Blount. She has lost touch with reality, her emotions, and the people around her after an accident that permanently disabled her and resulted in her sons disappearance.
* Joe: Chrystals husband. He has served a 16-year jail sentence for numerous crimes. Joe comes back into Chrystals life in hopes of changing for the better, only to find Chrystal in her current condition. Joe is played by actor Billy Bob Thornton. Ray McKinnon.
* Larry: played by actor Walton Goggins.
* Kalid: played by actor Harry Lennix.
* Barry: played by the actor Johnny Galecki.
 

==Critical reception==
Reviews for Chrystal were generally positive. On Rotten Tomatoes, Chrystal received an 80% based on 20 reviews, with an average score of 7 out of 10.

==Release and box office totals== grossed $80,701 grossed $19,074. 

==References==
 

==External links==
*  

 
 
 
 