Tire dié
{{Infobox film
| name           = Tire dié
| director       = Fernando Birri
| producer       = Edgardo Pallero
| writer         = Fernando Birri 7 other writers
| cinematography = Oscar Kopp Enrique Urteaga
| editing        = Antonio Ripoll
| released       = 1960
| runtime        = 33 minutes
| country        = Argentina Spanish
}} Argentine Documentary documentary directed by Fernando Birri and written Birri and seven other writers. The short film, billed as a "survey film", chronicles the harsh life of lower-class slums in Santa Fe, Argentina.

==Production history==
Fernando Birri, born in Santa Fe in 1925, left at the age of 25 for Rome to study film-making at the Centro Sperimentale di Cinematografia, from 1950 to 1953. In 1956 he returned to Santa Fe, to form the Film Institute at the Universidad Nacional del Litoral university, and started filming footage for his project, Tire dié, over a three-year period.

The documentary revolves around a particular set of poor kids who chase a train nicknamed tire dié on a daily base, begging for cents. The title, tire dié is a homonym of the phrase tire diez ("throw ten  "). Kids would chase the slow train every day and run along begging for coins from the passengers who leaned out in curiosity. The film also interviews a number of adults, whose voices are dubbed by professional actors.

The film was released two years after its completion in 1958, which gave time for Birri to film and screen what became his first film, La primera fundación de Buenos Aires in 1959. A year later, Tire dié was premiered. The film earned Birri critical acclaim and paved his way for further projects of similar nature, like Buenos Aires, Buenos Aires (1960) and more famously Los inundados (1961), which won the Venice Film Festival award for Best First Film.

==External links==
*  .
*  

 
 
 
 
 
 


 