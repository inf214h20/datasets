Swallowtail (film)
{{Infobox film
| name           = Swallowtail
| image          = Swallowtail Butterfly Poster.jpg
| director       = Shunji Iwai
| producer       = Shinya Kawai
| writer         = Shunji Iwai
| starring       = Hiroshi Mikami, Chara (singer)|Chara, Ayumi Ito Yosuke Eguchi Andy Hui Atsuro Watabe
| music          = Takeshi Kobayashi
| cinematography = Noboru Shinoda Kadokawa Herald
| released       =  
| runtime        = 148 minutes Japanese  English  Mandarin
}}

Swallowtail, also known as Swallowtail Butterfly (スワロウテイル Suwarōteiru) is a 1996 Japanese crime film directed by Shunji Iwai, starring Hiroshi Mikami, pop-singer Chara (singer)|Chara, and Ayumi Ito.

The film was shot on hand-held cameras using jump cuts and other visual techniques.  It covers a wide array of themes and genres, from social-realism to coming-of-age to crime.

A theme song for the film under Yen Town Band "Swallowtail Butterfly (Ai no Uta)" gained No. 1 on the Oricon Weekly Singles Chart.

==Plot==
The film is set in Tokyo at an unspecified point in the near future when the Japanese yen has become the strongest currency in the world. This attracts an influx of Immigration|immigrants, legal and illegal, to work in the city. The immigrants give the city the nickname  . The Japanese natives, however, despise the nickname, and in retribution call the immigrants by the homophone  , anglicised as "Yentowns" in the films English subtitles.      
 prostitute named Glico (Chara), who names her Ageha (Japanese for Swallowtail butterfly|swallowtail). Under Glicos care, Ageha starts a new life. 

The immigrant characters, who speak Japanese, English, Mandarin, or Cantonese, earn their living by committing petty crimes and engaging in prostitution. Ageha does not participate in any of these activities, but is protected by Glico and the other immigrants. The film does not make clear whether Ageha is Japanese or an Asian immigrant. 

Eventually, due to a sudden twist of fate, the immigrants are given a chance to realize their various dreams. But in doing so, they destroy their solidarity, and have to face their problems separately.

==Cast==
* Hiroshi Mikami as Fei Hong Chara as Glico
* Ayumi Ito as Ageha
* Yosuke Eguchi as Ryo Ranki
* Andy Hui as Mao Fu
* Atsuro Watabe as Ran
* Tomoko Yamaguchi as Shen Mei
* Nene Otsuka as Reiko
* Kaori Momoi as Suzukino
* Tadanobu Asano as Customer in club

==Awards==
* 1997 Japanese Academy Awards - Newcomer of the Year (Ayumi Ito)
* 1997 Japanese Academy Awards - Most Popular Film
* 1998 Fant-Asia Film Festival - Best Asian Film

Swallowtail Butterfly was also nominated for but did not win the following awards:

* 1997 Japanese Academy Awards - Best Actress (Chara)
* 1997 Japanese Academy Awards - Best Art Direction (Yohei Taneda)
* 1997 Japanese Academy Awards - Best Cinematography (Noboru Shinoda)
* 1997 Japanese Academy Awards - Best Film
* 1997 Japanese Academy Awards - Best Lighting (Yūki Nakamura) Osamu Takizawa)
* 1997 Japanese Academy Awards - Best Supporting Actress (Ayumi Ito) Moscow Film Festival - Golden St. George Award   

==Box office gross==
Japan: ¥2,928,000,000

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 