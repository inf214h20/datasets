Flesh and Blood (1922 film)
{{Infobox film
| name           = Flesh and Blood
| image          = Flesh and Blood 1922 Poster.jpg
| caption        =
| director       = Irving Cummings
| producer       =
| writer         = Louis Duryea Lighton (story & scenario) Lon Chaney
| cinematography =
| editing        =
| distributor    = Western Pictures Exploitation Company
| released       =   reels (5300 ft)
| country        = United States
}} Lon Chaney and directed by Irving Cummings. The film originally had a segment with Chinese players in color. 

==Synopsis==
In the film, the main character (Lon Chaney) escapes from prison where he has been for 15 years to see his daughter (Edith Roberts). But she is engaged to the son of the crook who framed him, complicating his plan of revenge. The movie features an interesting setting in San Franciscos Chinatown.

==Cast== Lon Chaney - David Webster
* Edith Roberts - The Angel Lady
* Noah Beery, Sr. - Li Fang
* DeWitt Jennings - Detective Doyle Ralph Lewis - Fletcher Burton
* Jack Mulhall - Ted Burton
* Togo Yamamoto - The Prince Kate Price - Landlady
* Wilfred Lucas - The Policeman

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 