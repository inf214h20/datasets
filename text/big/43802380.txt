Man on Ground
{{Infobox film
| name                = Man on Ground
| image               = Man on Ground poster.jpg
| alt                 = 
| caption            = Film poster
| director            = Akin Omotoso
| producer            = Hakeem Kae-Kazim   Fabian Adeoye Lojede   Rosie Motene   Akin Omotoso
| starring            =  
| cinematography      = Paul Michelson 
| editing             = Aryan Kaganof 
| screenplay          =  
| music         = Joel Assaizky   Amu
| writer     = Akin Omotoso
| studio         = Tom Pictures
| distributor   = 
| released            =   English  Yoruba  Zulu
| runtime            = 80 minutes
| country             = Nigeria   South Africa
| budget         = 
| gross          =
}}

Man on Ground is a 2011 Nigerian South African drama film directed by Akin Omotoso. It was screened and premiered at the 2011 Toronto International Film Festival.  The film tells a story about how Xenophobia in South Africa affect the lives of two Nigerian brothers.    

==Plot==
Ade, an accomplished financial executive and his brother Femi are South-African immigrants. Unknown to Ade, his brother, who is in South-Africa because of a self-imposed exile due to political affiliation in Nigeria has been kidnapped. On discovery that his brother is missing Ade carries out investigations to unravel the mystery and discovers the difficult lifestyle subjected to him. Ade pays homage to a former employer of Femi, when violence occurred which forced him to live with the boss. The frequent violent riot in the neighbourhood opens up many revelation on the life of his brother.

==Cast==
*Hakeem Kae-Kazim as Ade
*Fabian Adeoye Lojede as Femi
*Fana Mokoena as Timothy
*Bubu Mazibuko as Lindiwe
*Thishiwe Ziqubu as Zodwa
*Makhaola Ndebele as Vusi
*Mandisa Bardill as Nadia
*Joshua Chisholm as Young Ade
*Mbongeni Nhlapo as Young Femi

==Release==
It had its premiere at the 2011 Toronto International Film Festival on 12 September 2011.   

==Accolades==
{| class="wikitable sortable" style="width:100%"
|+List of Major Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|-   Africa Movie Africa Film Academy   (8th Africa Movie Academy Awards)  Best Film
| Akin Omotoso
|  
|- Best Sound
| 
|  
|- Best Best Cinematography
| 
|  
|-
 Best Editing
| 
|  
|- Best Actor
| Hakeem Kae-Kazim
|  
|- Best Supporting Actor
| Fana Mokoena
|  
|- Best Director
| 
|  
|-
| Special Jury Prize
| Akin Omotoso
|  
|-
|}

==References==
 

==External links==
* 

 
 
 