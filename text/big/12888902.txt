Father Brown (film)
 
 
 
{{Infobox film
| name           = Father Brown
| caption        = 
| image	=	Father Brown FilmPoster.jpeg
| director       = Robert Hamer
| producer       = Paul Finder Moss
| writer         = G. K. Chesterton (stories) Thelma Moss Maurice Rapf Robert Hamer
| starring       = Alec Guinness Joan Greenwood Peter Finch Cecil Parker Bernard Lee
| music          = Georges Auric
| cinematography = Harry Waxman
| editing        = Gordon Hales
| distributor    = Columbia Pictures
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} mystery comedy The Blue Cross", a short story by G. K. Chesterton, but confined the action to London.  

==Plot== Flambeau (Finch), is a master of disguise and is elusive, as Father Brown pursues him and tries to convince him to abandon his criminal career.

==Cast==
* Alec Guinness as Father Brown
* Joan Greenwood as Lady Warren
* Peter Finch as Flambeau
* Cecil Parker as The Bishop
* Bernard Lee as Inspector Valentine
* Sid James as Bert Parkinson
* Gérard Oury as Inspector Dubois
* Ernest Clark as Bishops Secretary
* Aubrey Woods as Charlie
* John Salew as Station sergeant
* Sam Kydd as Scotland Yard sergeant John Horsley as Inspector Wilkins
* Ernest Thesiger as Vicomte de Verdigris
* Jack McNaughton as Railway Guard
* Hugh Dempster as Man in bowler hat
* Eugene Deckers as French Cavalry Officer

==Adaptation==
It is based on the G. K. Chesterton Father Brown stories and was directed by Robert Hamer. The screen adaptation was written by Thelma Moss, and is based on several of the Father Brown stories, notably "The Blue Cross". 

==Critical reception== Variety said the film was, "distinguished mainly by the excellent casting of Alec Guinness in the title role." 
*The New York Times found it "a leisurely, good-humored film." 

==Awards and nominations==
Venice Film Festival
*1954: Nominated, "Golden Lion Award" - Robert Hamer

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 