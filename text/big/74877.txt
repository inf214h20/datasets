Let's All Go to the Lobby
{{Infobox film
| name = Lets All Go to the Lobby
| image = Lets All Go to the Lobby.png anthropomorphic snack food items singing Lets All Go to the Lobby.
| released = 1953| runtime = 1 minute
| director =
| producer =
| writer =
| starring =
| music =
}} snipe played anthropomorphic Chewing audience members illusion of depth. Eagan (2010), p. 543-544 

==Background== featured presentation. previews of coming attractions, courtesy requests for the audience, and notices concerning the concession stand of the movie theater.  According to film historian Scott Simmon, the history of advertising films begins with Admiral Cigarette (1897) by William Heise. 

The  . Filmack commissioned a series of Technicolor trailers aimed at informing audiences about a theaters newly installed concession stand.   Lets All Go to the Lobby was one of these films. 

The trailer was animated by Dave Fleischer (producer of Popeye cartoons) and produced by Filmack Studios. Fleischer was identified as the creator of this short film in a list of Filmacks releases which reported that "Both trailers were produced exclusively for Filmack by Dave Fleischer...". Specific details for his involvement are lacking, and the rest of the production crew remains unknown.   Production may have started by 1953, but Robbie Mack (a later owner of Filmack) estimates it was completed c. 1955. The release date is typically estimated to 1957. The original production records are considered lost.  Filmack sold to various theater owners the right to use the film, which it still owns. 

==Content== illusion of Walt Disney Productions.  Later, a group of four consumers are depicted enjoying their purchased food items. The sequential action of this simple scene uses techniques that can be traced back to Gertie the Dinosaur (1914) by Winsor McCay. 
 The Bear Went Over the Mountain", "For Hes a Jolly Good Fellow", and Marlbrough sen va-t-en guerre" (c. 1709). The tune is of unknown origin and probably predates the lyrics of these songs. It was well enough known in the early 19th century to be used for a passage in "Wellingtons Victory" (1813) by Ludwig van Beethoven. 

While the use of animation in lieu of photography lends a degree of abstraction to the idea of concessions, in Americas Film Legacy, Daniel Eagan argues that " ith its simple, repetitive lyrics and streamlined animation, Lets All Go to the Lobby has a hypnotic pull that is as compelling today as it was fifty years ago."  He also notes that by choosing not to simply photograph the offered items, the creators of the film managed to avoid using brand names for the products for sale. 

==Legacy==
Filmack has continued selling copies in the decades since its production. The company estimates that 80% of independent theaters  have screened the film at various points, making the film familiar to a large number of viewers. 

Further color snipes, such as ones using a clock and featuring singing and dancing hot dogs, popcorn boxes, candy bars and other concession stand products took their cue from this trailer. Those targeted at drive-in theaters directed their patrons to the snack bar. Such shorts are still used for this purpose.

In 2000, "Lets All Go to the Lobby" was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Sources==
*  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 