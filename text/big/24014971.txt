Three Live Ghosts (1929 film)
{{infobox_film
| name           = Three Live Ghosts
| image          =
| imagesize      =
| caption        =
| director       = Thornton Freeland
| producer       = Max Marcin
| writer         = Frederick Stewart Isham (play:Three Live Ghosts)
| starring       = Beryl Mercer
| music          = Hugo Riesenfeld
| cinematography = Robert H. Planck
| editing        = Robert Kern
| distributor    = United Artists
| released       = September 15, 1929
| runtime        = 8 reels(7,486 feet)
| country        = United States
| language       = English
}} Harry Stubbs, Robert Montgomery, and Tenen Holtz. Three veterans of World War I return home to London after the armistice, only to find they have been mistakenly listed as dead.  It was based on the play Three Live Ghosts by Frederic S. Isham.
 Three Live Ghosts.

==Cast==
*Beryl Mercer - Mrs. Gubbins
*Hilda Vaughn - Peggy Woofers Harry Stubbs - Bolton
*Joan Bennett - Rose Gordon
*Nanci Price - Alice
*Charles McNaughton - Jimmie Grubbins Robert Montgomery - William Foster
*Claud Allister - Spoofy
*Arthur Clayton - Paymaster
*Tenen Holtz - Crockery Man
*Shayle Gardner - Briggs
*Jack Cooper - Benson
*Jocelyn Lee - Lady Leicester

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 


 