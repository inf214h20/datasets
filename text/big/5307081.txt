The Lost Battalion (1919 film)
 
{{Infobox film
| name           = 
| image          = File:The-Lost-Battalion-1919-cover.jpg|thumb|right|
| alt            = 
| caption        = Slide promoting the film
| film name      = 
| director       = Burton L. King 
| producer       = Edward A. MacManus  
| writer         = Charles A. Logue  
| screenplay     = 
| story          = 
| based on       = 
| starring       = 
| narrator       = 
| music          = 
| cinematography = A. A. Cadwell
| editing        = 
| studio         = MacManus Corporation 
| distributor    = W. H. Productions Company
| released       = 1919
| runtime        = 
| country        = United States Silent
| budget         = 
| gross          = 
}}
 1919 American silent war 77th Infantry Lost Battalion") Argonne Forest Charles Whittlesey remade in 2001 by Russell Mulcahy.

==Synopsis==
The men in the 308th Regiments 77th Division, have been drafted from diverse ethnic, economic, and social groups in New York. Two men are fighting Chinatown tongs, one is a burglar, another is a wealthy merchants son in love with his fathers stenographer, who dreams of becoming the greatest movie actress, another is a private in love with the merchants ward, and finally there is "the Kicker," who finds fault with everything. After training in Yaphank and in France, the 463 men advance under the command of Lt. Col. Charles W. Whittlesey into the "Pocket" of the Argonne Forest, to help break down the supposedly impregnable German defense. Cut off from Allied troops and supplies, and surrounded by the enemy, the Division, nicknamed "The Lost Battalion," withstands six days without food or water. When the German commander asks for their surrender, Whittlesey replies, "Tell them to go to hell!" The Chinese rivals fight bravely side-by-side, while the burglar dies heroically. After their rescue, the survivors are given a parade in New York, and are reunited with their families and sweethearts.

==Cast==
*Major-General Robert Alexander - (Himself)  
*Lt. Col. Charles W. Whittlesey - (Himself)  
*Major George McMurtry - (Himself)  
*Captain William J. Cullen - (Himself)  
*Lt. Arthur F. McKeogh - (Himself)  
*Lt. Augustus Kaiser - (Himself)  
*Private Abraham Krotoshinsky - (Himself)  
*Helen Ferguson - (The Stenographer)  
*Marion Coakley - (Nancy Crystal)  
*Mrs. Stuart Robson - (The landlady)  
*Blanche Davenport - (The mother)  
*Lt. Jordan - (Himself)  
*Bessie Lern - (The girl next door)  
*Sydney DAlbrook - (The burglar)  
*Gaston Glass - (Harry Merwin)  
*Jack McLean - (The Kicker)  
*William H. Tooker     
*Stephen Grattan     
*J. A. King     

==Units involved==
*Company A, 308th Infantry
*Company B, 308th Infantry
*Company C, 308th Infantry
*Company E, 308th Infantry
*Company G, 308th Infantry
*Company H, 308th Infantry
*Company K, 307th Infantry
*Company C, 306th Machine Gun Battalion
*Company D, 306th Machine Gun Battalion
 The Distinguish Service Cross or Silver Star citations that where later upgraded to the Silver Star Medal.

==External links==
 
* 
* 
* 
 
 
 
 
 
 
 
 