Parting Shots
Parting Shots is a 1999 film starring Chris Rea, Felicity Kendal, Oliver Reed, Bob Hoskins, Diana Rigg, Ben Kingsley, John Cleese and Joanna Lumley. It was the last film directed by Michael Winner.

==Plot==
The film concerns Harry Sterndale (Rea), a wedding photographer who is mistakenly told by his doctor that he has six weeks to live and begins to kill people who have wronged him in his life, only to find that the diagnosis was incorrect. Also he paid an assassin (Reed) to kill him instead of living out his last days in custody and nearly leaves it too late to call off the hit. The assassin takes after the two and after being arrested for assassinating the dictator of a fictional country he takes the blame for all of the murders that occurred during the film. It ends with Rea and Kendal visiting the assassin in prison as newlyweds.

== Cast ==
*Chris Rea - Harry Sterndale
*Felicity Kendal - Jill Saunders
*Oliver Reed - Jamie Campbell-Stewart
*Bob Hoskins - Gerd Layton
*Diana Rigg - Lisa
*Ben Kingsley - Renzo Locatelli
*John Cleese - Maurice Walpole
*Joanna Lumley - Freda
*Gareth Hunt - Inspector Bass
*Nicholas Gecks - Detective Constable Ray
*Patrick Ryecart - Cleverley  
*Peter Davison - John  
*Nicky Henson - Askew  
*Caroline Langrishe - Vanessa  
*Edward Hardwicke - Dr. Joseph  
*Nicola Bryant - Beverley  
*Brian Poyser - President Zlomov  
*Sheila Steafel - Presidents Wife
*Timothy Carlton - Commissioner Grosvenor
*Roland Curram - Lord Selwyn  
*Jenny Logan - Lady Selwyn
*Sarah Parish - Ad Agency Receptionist
*Andrew Neil - TV Newsreader

==Reception==
Parting Shots was not well received by critics with Total Film s review describing Winners work as "offensive", "incompetent" and "bad in every possible way".  Andrew Collins gave a strongly negative
review of the film: "Parting Shots... is going to set the course of British film-making back 20 years. It is not only the worst British film produced in this country since Carry On Emmannuelle (quite a feat in itself), it is a thoroughbred contender for the crown of Worst Film Ever Made".  In a hostile overview of Winners films, Christopher Tookey claimed "Parting Shots is not only the most horrible torture for audiences that Winner has ever devised. It is also profoundly offensive, even by Winners standards". Fred and Rosemary West". Christopher Tookey, "Michael Winners latest film is his most offensive yet". The Daily Mail, May 11, 1999, (p.11).  Tookey also denounced Parting Shots as "the most tasteless, abysmal comedy of all time."    
Interviewed about Parting Shots, Charlotte OSullivan, 
  later claimed "Parting Shots...was directed by Michael Winner and despite the glittering cast, was possibly the worst film ever made".  In its entry on Michael Winner, the book Contemporary British and Irish Film Directors claimed Parting Shots  "makes a bold challenge for the hotly contested mantle of worst British film ever made."
 Contemporary British and Irish Film Directors: A Wallflower Critical Guide, edited by Yoram Allon, Del Cullen, 
and Hannah Patterson. Wallflower Press, 2001, ISBN 1903364213   (p.353).  British film historian I.Q. Hunter, discussing the question "What is the worst British film ever made?", listed Parting Shots as one of the candidates for that title. 

==See also==
*List of films considered the worst

==References==
 

==External links==
* 

 

 
 
 