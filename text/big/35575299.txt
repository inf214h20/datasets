Split Decisions
{{Infobox film
| name           = Split Decisions
| image          = Split decisions dvd cover.jpg
| image_size     = 280
| caption        = DVD cover
| director       = David Drury
| writer         = 
| narrator       = 
| starring       = Craig Sheffer
| music          = 
| cinematography =  Thomas Stanford
| studio         = 
| distributor    = 
| released       = November 11, 1988
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Split Decisions is a 1988 film directed by David Drury. It stars Craig Sheffer, Jeff Fahey and Gene Hackman. 

==Plot==

On the east side of New York, boxing trainer Dan McGuinn is trying to prepare one of his sons, Eddie, to earn a chance to fight in the Olympic Games, while his other son, Ray, has fallen in with shady men from organized crime. As further information rears into view Eddie has found out who had killed his brother. Vengeance in return for his brothers death. Eddie enters the ring with Pedroza, the murderer, and defeats him late in the midst of the rounds.

==Cast==
*Craig Sheffer as Eddie McGuinn
*Jeff Fahey as Ray McGuinn
*Gene Hackman as Dan McGuinn
*Jennifer Beals as Barbara Uribe
*Eddie Velez as Julian Snake Pedroza
*Harry Van Dyke as Douby

==References==
 

==External links==
* 

 
 
 
 

 