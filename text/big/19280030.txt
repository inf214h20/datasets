Goodbye Emmanuelle
{{Infobox film
|image=Goodbye-emmanuelle.jpg
|border=yes
|caption=Theatrical poster
|director=François Leterrier
|writer=Monique Lange François Leterrier Emmanuelle Arsan (character)
|starring=Sylvia Kristel
|producer=Yves Rousset-Rouard 
|music=Serge Gainsbourg    
|cinematography=Jean Badal    
|editing=Marie-Josèphe Yoyotte  
|released= December 15, 1977 (USA)
|runtime=100 minutes
|country=France
|language=French gross = 990,953 admissions (France) 

|distributor= 
}}
 1977 French softcore Sex erotica movie directed by François Leterrier, and starring Sylvia Kristel. The music score is by Serge Gainsbourg.  In this sequel, Emmanuelle and Jean move to the Seychelles, where she leaves him.

==Cast==
*Sylvia Kristel as  Emmanuelle
*Umberto Orsini as  Jean
*Alexandra Stewart as  Dorothée
*Jean-Pierre Bouvier as  Grégory
*Olga Georges-Picot as  Florence
*Charlotte Alexandra as  Chloe

==Production==
Goodbye Emmanuelle was intended as the last of a trilogy that included Emmanuelle (film)|Emmanuelle (1974) and Emmanuelle 2 (1975).   It was shot on the Seychellois island of La Digue. 

==Release== Bob and Showtime cable channels.  

==Reception==
In his The New York Times|New York Times review, John Corry observed that "The scenery   wins every time", but was less favourable about what he deemed "wearisome" sex scenes.  He reflected on both aspects at the end of his critique: "The question in the movie is whether Francois Leterrier, its director, was so absorbed in the lovemaking that he just allowed the scenery to creep in, or whether he put it in on purpose. Maybe it doesnt matter." 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 


 
 