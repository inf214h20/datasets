The Bad Boy (1917 film)
{{Infobox film
| name        = The Bad Boy
| image       =
| producer    =
| screenplay  = Chester Withey Frank E. Woods
| starring    = Robert Harron Richard Henry Cummings Josephine Crowell Mildred Harris
| director    = Chester Withey
| studio      = Fine Arts Film Company
| distributor = Triangle Film Corporation
| released    =  
| runtime     =
| language    = Silent film English intertitles
| country     = United States
| budget      =
}}
 silent crime Richard Cummings, and Mildred Harris. The film marks the debut of Colleen Moore, who plays a supporting role in the film. 

==Plot==
Small town youth Jimmie Bates (Robert Harron) is a well-intentioned, but troubled youth. Jimmie is a rowdy boy who is always getting into trouble and playing pranks on his friends and neighbors. Although deeply in love with young Mary (Mildred Harris), he eventually spurns Marys affection for the more outgoing and worldly young Ruth (Colleen Moore).

Eventually, Jimmies father Mr. Bates (Richard Henry Cummings), in a fit of exasperation with the boys antics, beats him severely and Jimmie runs away from home. While on the lam Jimmie becomes involved with a criminal gang of thieves and Jimmie serves a sentence in jail. After completing his sentence, Jimmie vows to turn over a new leaf. However, the gang of thieves decide they are going to rob Jimmies hometown bank. Jimmie tries desperately to foil the attempt during the robbery and is wounded and  arrested by the sheriff (William H. Brown) as the robbery suspect. Jimmie escapes from jail and seeks out his true love Mary who hides Jimmie at her home and nurses him back to health.

After regaining his strength, Jimmie sets about vindicating himself to his parents and townspeople. Jimmie eventually pursues and captures real perpetrator in his fathers yard. After his capture, the criminal finally admits that Jimmie was not a participant in the robbery attempt and Jimmie is finally redeemed in the eyes of his family.

== Cast ==
{| border="1" cellpadding="4"  cellspacing="0"
|- bgcolor="#CCCCCC"
! Role || Actor
|-
| Jimmie Bates || Robert Harron
|-
|  Mr. Bates || Richard Henry Cummings
|-
| Mrs. Bates || Josephine Crowell
|-
| Mary  || Mildred Harris
|- William H. Brown
|- James Harrison
|-
| Ruth || Colleen Moore
|-
| Yeggman  || Elmo Lincoln
|- Harry Fisher
|-
| Uncredited || Carmel Myers
|-
|}

==References==
;Notes
 

;Bibliography
* Silent Star, by Colleen Moore.
* Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 