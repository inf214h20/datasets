The Bride Wore Boots
{{Infobox film
| name           = The Bride Wore Boots
| image          = Thebrideworeboots.jpg
| image_size     =
| caption        = Lobby card
| director       = Irving Pichel
| producer       = Seton Miller
| writer         = Harry Segall Dwight Michael Wiley
| narrator       =
| starring       = Barbara Stanwyck Robert Cummings Diana Lynn
| music          = Friedrich Hollaender
| cinematography = Stuart Thompson
| editing        = Ellsworth Hoagland
| distributor    = Paramount Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
}}

The Bride Wore Boots is a 1946 romantic comedy film with Barbara Stanwyck in the title role, playing opposite Robert Cummings. A very young Natalie Wood is seen in the film, directed by Irving Pichel.

This was Stanwycks last feature comedy.  Some years later, she complained to columnist Hedda Hopper, "Ive always got my eye out for a good comedy. Remember Ball of Fire and The Lady Eve? But they dont seem to write that kind of comedy anymore -- just a series of gags." 

==Plot==
Sally Warren runs a horse farm, but husband Jeff has a dislike and fear of horses. He is a Civil War historian and lecturer, which bores Sally but is very popular with local ladies who call themselves the Mason-Dixon Dames.

As a Christmas gift, Jeff tries to please his wife by buying her a horse called Albert, but her horse trainer Lance Gale, an old beau, insults Jeff about the kind of horse he picked. Sally in turn buys Jeff a desk that belonged to Jefferson Davis, but the Dames claim its a fake and one of them, Mary Lou Medford, makes a pass at Jeff.

The next time Sally catches the same woman kissing Jeff, she sues him for divorce. Jeff ends up hiring Mary Lou as his secretary. To spite his wife, Jeff also enters Albert in the big Virginia Cup steeplechase race that Sallys always longed to win.

Alberts jockey is thrown, so Jeff reluctantly leaps into the saddle. He is thrown off repeatedly while trying in vain to catch Lances horse in the race. But his effort impresses Sally, who reconciles with Jeff at the finish.

==Cast==
* Barbara Stanwyck as Sally Warren
* Robert Cummings as Jeff Warren
* Diana Lynn as Mary Lou Medford
* Patric Knowles as Lance Gale
* Robert Benchley as Uncle Tod
* Natalie Wood as Carol Warren

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 