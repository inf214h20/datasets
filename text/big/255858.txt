Johnny English
 
 
 
{{Infobox film
| name = Johnny English
| image = Johnny English movie.jpg
| image_size = 
| border = 
| alt = 
| caption = British release poster
| director = Peter Howitt
| producer = Tim Bevan Eric Fellner Mark Huffam Neal Purvis William Davies
| starring = Rowan Atkinson Natalie Imbruglia Ben Miller John Malkovich
| music = Edward Shearmur
| cinematography = Remi Adefarasin
| editing = Robin Sales
| studio = StudioCanal Working Title Films Universal Pictures
| released =   
| runtime = 88 minutes   
| country = United Kingdom   
| language = English
| budget = $40 million 
| gross = $160,583,018 
}}
 William Davies, and the film was directed by Peter Howitt. The film grossed a total of $160 million worldwide.  The film was followed by a sequel, 2011s Johnny English Reborn.

==Plot==
The film opens with a fantasy sequence in which Johnny English (Rowan Atkinson), an inept British Intelligence agent, is "Agent One". He sneaks into a building, distracts two guard dogs with toys, knocks out two guards and seduces a woman who threatens him. He is awakened from his fantasy, just as he is about to kiss the woman, by his sidekick, Angus Bough (Ben Miller). After being assured that English has checked the submarine hatch codes personally, the real Agent One (Greg Wise) leaves on a mission. The audience then learn that Agent One died in action when his submarine hatch "failed to open". A bomb then wipes out Britains remaining agents, all of whom were attending the funeral of Agent One, leaving only English. Nobody notices the hearse, which sped from the scene minutes earlier.
 Crown Jewels. magazine from his pistol. English chases their car, a hearse, but accidentally ends up trailing the wrong hearse after being stopped by a red light. Convinced the burial party he discovers is an act, he accuses the mourners and the priest before realizing his mistake after another man told English he was the hearse driver. Bough rescues him by pretending that English is an escaped asylum inmate.

The pair then uncovers the mastermind of the theft, French prison entrepreneur and descendant of William the Conqueror, Pascal Edward Sauvage (John Malkovich), who aided in restoring the Crown Jewels. English reports his suspicions to the head of MI7, Pegasus (Tim Pigott-Smith), but Pegasus, who is a personal friend of Sauvage, dismisses his concerns as absurd and orders English to leave Sauvage alone. In the car park, one of Sauvages henchmen attacks English and Bough, and escapes when English mistakenly attacks Bough. Against Pegasus wishes, English and Bough infiltrate Sauvages headquarters via parachute, but English lands on the wrong building, abseiling the identical Royal London Hospital. He holds several staff and patients at gunpoint, before realising his mistake.
 Foreign Secretary Royal Familys line of succession, thus making Sauvage the automatic heir to the throne.

Lorna visits English at his flat, as his mission was reassigned to her, and persuades English to join her. They travel to France, infiltrate Sauvages chateau, and overhear his proposal to turn the United Kingdom into a giant prison once he is crowned King. However, English accidentally triggers a microphone, alerting Sauvage to their presence. In an attempt to steal an incriminating DVD, English accidentally drops it onto a tray full of identical discs without looking and takes the wrong one. Taken hostage, the two agents are rescued by Bough and return to England on the day of the Coronation of the British monarch|coronation.

At Sauvages coronation, English sneaks in disguised as the representative of the English bishops. He publicly accuses Sauvage of treason, and unaware that the fake Archbishop is no longer being used, English attempts to pull off the Archbishops face, believing it to be a mask. Failing this, English pulls down the Archbishops trousers to look for a tattoo borne by the impostor, which is obviously absent. English then radios to Bough to tell him to play the DVD they retrieved. Bough does so, resulting in three-quarters of the worlds population watching a video of English, in a shower cap and underpants, dancing and miming along to "Does Your Mother Know" by ABBA (Sauvage had previously had Englishs home bugged in order to study him). English escapes, but comes back, swinging from a wire above Sauvage and the Archbishop, grabbing St Edwards Crown before it touches Sauvages head. Out of frustration, Sauvage reveals his true intentions to London by pulling a gun and shooting at English. A bullet grazes Englishs hand and he drops the crown, but before the Archbishop can crown Sauvage, English knocks him out of the throne and is inadvertently crowned himself. He places Sauvage under arrest, reveals the plot to the Queen and allows her to return to the throne in return for a Orders, decorations, and medals of the United Kingdom|knighthood.

The film ends with English driving Lorna to the top of a mountain somewhere in the south of France where Johnny accidentally presses the eject button while about to kiss her, and Lorna shoots into the sky. A mid-credits scene shows Lorna landing in a swimming pool where Bough is sitting. Also alongside the pool is the fake criminal whom English had described knocking out to the chief of security earlier in the movie, noted for his orange frizzy hair and eye patch.

==Cast==
* Rowan Atkinson as MI7 agent, Johnny English
* Ben Miller as MI7 agent, Angus Bough
* John Malkovich as Pascal Edward Sauvage
* Natalie Imbruglia as Interpol Agent Lorna Campbell
* Oliver Ford Davies as the Archbishop of Canterbury
* Tim Pigott-Smith as Pegasus
* Kevin McNally as the Prime Minister
* Douglas McFerran as Klaus Vendetta
* Steve Nicolson as Dieter Klein
* Greg Wise as Agent One
* Tim Berrington as Roger
* Prunella Scales as Queen Elizabeth II
* Tasha de Vasconcelos as Countess Alexandra (Exotic Woman)
* Nina Young as Pegasus secretary
* Sam Beazley as Elderly man
* Kevin Moore as Doctor
* Jack Raymond as French reception waiter
* Jenny Galloway as the Foreign Secretary
* Chris Tarrant (voice) as Radio announcer
* Trevor McDonald (voice) as News Caster

==Production==
The character of Johnny English himself is based on a similar character called Richard Latham who was played by Atkinson in a series of British television advertisements for Barclaycard.  The character of Bough (pronounced Boff) was retained from the advertisements though another actor, Henry Naylor, played the part in the ads. Some of the gags from the advertisements made it into the film, including English incorrectly identifying a waiter, and inadvertently shooting himself with a tranquiliser ballpoint pen.

===Filming locations===

 
* Some scenes were filmed at Canary Wharf in London— indeed, the film duplicates the single real tower into two identical ones (albeit on the real site) for the fictional London Hospital and Sauvages headquarters at 1 Canada Square.
* The scenes set in  .
* Both the exteriors and interiors in the opening credits sequence scene are in Mentmore Towers. 
* Sandringham is Hughenden Manor. 
* The exterior and interior of MI7s headquarters which English enters at the start is Freemasons Hall, London, which is also used as Thames House (the MI5 headquarters) in Spooks. A20 road (with Dover Castle in the background) and then  enters the Port of Dover (with a "Dover Ferry Terminal" sign, Dovers Athol Terrace and the White Cliffs of Dover in the background) to catch a ferry to France, were all shot on location. 
* The exterior of Sauvages French château is actually the castle atop St Michaels Mount in Cornwall.
* A scene was filmed in Hong Kong, China.
* The scenes in Brompton Cemetery were filmed there.

==Reception==
The film holds a 33% approval rating on the review site Rotten Tomatoes based on 116 reviews with the consensus "A tame spy spoof that elicits infrequent chuckles."  On Metacritic, the film holds a score of 51 based on 32 reviews. 

==Soundtrack==
All tracks written by Edward Shearmur and performed by London Metropolitan Orchestra unless otherwise noted.
# "A Man for All Seasons" (Hans Zimmer, Robbie Williams) – Robbie Williams
# "Theme from Johnny English" (Howard Goodall)
# "Russian Affairs"
# "A Man of Sophistication" Bond
# "Truck Chase"
# "The Only Ones" – Moloko
# "Parachute Drop"
# "Pascals Evil Plan"
# "Theme from Johnny English(Salsa Version)" (Howard Goodall) – Bond
# "Off the Case"
# "Cafe Conversation"
# "Into Pascals Lair"
# "Zadok the Priest" – Handel
# "Does Your Mother Know" – ABBA
# "For England"
# "Riviera Highway"
# "Agent No. 1"

==Home media==
Johnny English was released on DVD on 13 January 2004 and on Blu-ray Disc|Blu-ray on 28 February 2012 along with its sequel Johnny English Reborn.

==Sequel==
A sequel, Johnny English Reborn, was released in October 2011. Filming for the sequel began in September 2010, seven years after the release of the original and concluded in March 2011. The film follows Johnny English, now training in Asia after being disgraced in an earlier mission, as he attempts to foil a plot to assassinate the Chinese Premier.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 