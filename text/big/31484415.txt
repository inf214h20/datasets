Flame (1996 film)
{{Infobox film
| name           = Flame
| image          =
| image size     =
| alt            =
| caption        =
| director       = Ingrid Sinclair
| producer       = Joel Phiri Simon Bright
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Marian Kunonga Ulla Mahaka
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = California Newsreel
| released       =  
| runtime        = 85 minutes
| country        =
| language       = English
| budget         =
| gross          =
}}

Flame is a controversial 1996 war film directed by Ingrid Sinclair,    produced by Joel Phiri and Simon Bright,    and stars Marian Kunonga and Ulla Mahaka.    It was the first Zimbabwean film since independence set during the Rhodesian Bush War, and served as a tribute to the Zimbabwe African National Liberation Armys many female guerrillas. 

==Plot==
In 1975, Florence, an impulsive young peasant girl from the Mashonaland countryside, decides to run away and join the Zimbabwe African National Union (ZANU) after her father is detained by the Rhodesian Security Forces. She and her friend Nyasha subsequently brave the dangers of the African bush on a long trek to a refugee camp in neighbouring Mozambique, where they are trained as militants by ZANUs insurgent wing, ZANLA. Both adopt new revolutionary identities: Nyasha becomes Liberty, representing her desire for self-sufficiency, while Florence brands herself Flame, representing passion.

Life in the Mozambican refugee camps is hard: Flame and Liberty must learn to deal with the hardship and constant dangers of insurgent life. To complicate matters, some unscrupulous male guerrillas - such as Comrade Che, a ZANLA political commissar, assume that female members of the party would be available to satisfy their sexual desires. When Flame rejects his advances, Che rapes and impregnates her with his child.

Although naturally devastated, Flame reconciles with Che and is shattered when he and their son are killed by a Rhodesian   principles of mutual support and shared purpose once preached by ZANU during the bush war.

The two acquaintances finally attend a Heroes Day party, nearly fifteen years after their struggle against white Rhodesia began, in an increasingly authoritarian Zimbabwe. They continue to greet passerby with the old Pan-Africanism|pan-African slogan, "A luta continua" ("the struggle continues") as the film closes.

==Cast==
* Marian Kunonga as Florence (Flame) 
* Ulla Mahaka as Nyasha (Liberty) 
* Robina Chombe as Charity   
* Dick Chigaira as Rapo 

==Production==
The film was shot in Zimbabwe. 

==Reception==
Flame was selected for the 1996 Cannes Film Festival.   

===Awards===
Flame received several awards in the following film festivals: 

Southern African Film Festival, Harare
*OAU Prize - Best Film
*Jury Award - Best Actress
*Jury Award - Best Director

Journées de Cinématographe de Carthage, Tunis
*Special Jury Prize - Best Film

Amiens Film Festival, Amiens, France
*Prix du Public - Best Film
*Palmares du Jury - Best Actress
*OCIC Award - Best Film

M-Net Film Awards, Cape Town
*Best Music

The Annonay International Film Festival, France
*The Grand Prix - Best Film

The Milan African Film Festival
*Premio del Pubblico (The Public Prize)
*Concorso Lungometraggi - Migliore Opera Prima (Best First Film)

The Human Rights Watch International Film Festival, New York
*The Nestor Almendros Prize

The International Womens Film Festival in Turenne (1998)
*The Jury Award for Best Film
*The Youth Award for Best Feature Film

===In Zimbabwe=== censors and became a box office success and the number one film of the year in Zimbabwe. 

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 