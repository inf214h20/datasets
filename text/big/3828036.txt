Ask a Policeman
{{Infobox film
| name           = Ask a Policeman
| image          = "Ask_a_Policeman"_(1939).jpg
| caption        =
| director       = Marcel Varnel Edward Black
| writer         = Marriott Edgar Sidney Gilliat(story) Val Guest J.O.C. Orton Charles Oliver Herbert Lomas
| music          = Louis Levy
| cinematography = Derick Williams
| editing        = R.E. Dearing
| studio         = Gainsborough Pictures
| distributor    = Metro-Goldwyn-Mayer  
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
}}
Ask a Policeman is a 1939 British comedy film directed by Marcel Varnel which stars Will Hay, Moore Marriott and Graham Moffatt. The title comes from the popular music hall song Ask a Policeman. The Turnbotham Round police force are threatened with dismissal by their Chief Constable and decide to manufacture some smugglers to keep their jobs. When they encounter real smugglers, their plans begins to fall apart.

==Synopsis==

Will Hay plays Sergeant Samuel Dudfoot, an inept Police officer|policeman, stagnating in the sleepy village of Turnbotham Round (pronounced Turnbottom), where there has been no crime for a decade. After the Chief Constable tells them that there is not enough local criminal activity to justify their police station|stations existence, three incompetent policemen decide to start manufacturing crimes to "fiddle the figures". Dudfoot, together with Albert Brown (Graham Moffatt) and Jerry Harbottle (Moore Marriott) create a crime wave by framing motorists in a speed trap and concocting false evidence.

They also leave a keg of whisky unattended in order to frame someone as a smuggler - and then accidentally discover a real smuggling ring!

==Plot==
 
The film opens with Sergeant Dudfoot talking about his life as a policeman at Turnbotham Round during a radio broadcast. His staff Albert and Harbottle (played by Graham Moffatt and Moore Marriott) enter with Harbottle after theyve been poaching and Harbottle ruins the broadcast.

The next morning, Dudfoot decides to find some crime and finally comes up with the idea of setting up a speed trap.

The plan fails, and in executing it Dudfoot hits a motorist on the back of the head, knocking him out. He drives him to the police station, only to discover he is their Chief Constable. Albert decides to wreck the chiefs car, making it look like he had a crash. To make this more believable, Dudfoot crashes it through the window of Harbottles shop. Later when the trio tell the chief he had a nasty road accident, it backfires since the chief remembers the incident, but the Squire of Turnbotham Round then claims that he witnessed the accident, saving Dudfoot, Harbottle and Albert from a lot of trouble.

The Chief leaves after Harbottle makes up a story about a Headless Horseman when questioned about his old looks. Dudfoot states that they need to arrest a criminal soon or else their police station will be closed down and Harbottle takes him to the library to look for books on crime. On their way the coastguard stops them and tells them his brother a lighthouse keeper wants a light hung up on top of the police station as his grandmother is very ill and he agreed to the idea that if he could see the light on the Police Station tower hed know his grandmother was still alive. Unknown to the cops, this is connected to the smugglers.

Later Albert suggests that they should capture some smugglers by placing a keg of brandy on the beach and getting a witness to see what happens. Dudfoot comes back into the station with a fisherman, who is carrying a keg of brandy and Albert and Harbottle say they havent taken their keg down to the beach yet, therefore resulting in two kegs of liquor. 

Albert’s girlfriend Emily screams and passes out as she claims to have seen a Headless Horseman. Later Albert spots the Headless Horseman too and after an encounter with him in the Squire’s garage, they are scared off by the Horseman, though  Harbottle finds a small package which he tucks away.

Back at the police station, the Chief Constable phones them about the smuggling and instructs them to find the navigational light the smugglers are using. In spite of the light episode with the coastguard, the three policemen brush off the idea that the coastguard is involved with smuggling. A warning note to keep their noses out of things is wrapped around a stone thrown in through the police station window.

A ticking sound is heard from the package that Harbottle earlier picked up and they find pocket watches inside. Harbottle then recites a rhyme which tells the legend of the Headless Horseman, although he doesnt know the last line, but his father does. So the trio decide to pay him a visit. Harbottles father reveals the line thus also revealing the place, the Devils Cave where the smuggling is taking place. 

The trio investigate the cave, follow a tunnel and discover many barrels of liquor and other things that seemed to belong to Harbottle. They figure that they are in their own cellar. They decide to call the Chief Constable, but are confronted by the Squire who reveals he is the leader of the smugglers. After a fight in the dark, the smugglers lock the trio in their own cell and escape deciding to give chase in their car, but since the other police agents think they are smugglers as well, their car is also wanted.

After a chase, the police agents finally capture the smugglers. The Chief Constable asks the Squire if he has seen him before, but the Squire denies this. Dudfoot then reveals the story of the accident at Harbottles shop, and the Chief Constable orders that the trio be arrested. Dudfoot punches the Chief Constable and the trio run as fast as they can along the race track away from the other pursuing policemen.

==Cast==
* Will Hay - Sergeant Samuel Dudfoot
* Graham Moffatt - Albert Brown
* Moore Marriott - Jerry Harbottle/Harbottle Senior
* Glennis Lorimer - Emily Martin
* Peter Gawthorne - Chief Constable Charles Oliver - The Squire Herbert Lomas - The Coastguard
* Dave OToole - Dudfoots witness
* Noel Dainton - Revenue Officer
* Cyril Chamberlain - Radio Announcer Brian Worth - Broadcasting Engineer
* Patrick Aherne - First motorist
* Desmond Llewelyn - (uncredited) "headless horsemen". 

==Remakes==
In 1982, comedy duo Cannon and Ball filmed a loose remake of Ask A Policeman, titled The Boys In Blue. Though not a remake, 2007s Hot Fuzz borrowed the plot device of policemen uncovering organised crime in a sleepy area of rural England.

==References==
 

==Bibliography==
* Luxford, Albert J. & Owen, Gareth. Albert J. Luxford, the gimmick man: memoir of a special effects maestro. McFarland & Company, 2002.

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 