Two Weeks Vacation
 
{{Infobox Hollywood cartoon
| cartoon name      = Two Weeks Vacation
| series            = Goofy
| image             = Two Weeks Vacation.jpg
| image size        = 
| alt               = 
| caption           = Theatrical release poster
| director          = Jack Kinney
| producer          = Walt Disney
| story artist      = Al Bertino
| narrator          = Alan Reed
| voice actor       = Pinto Colvig Alan Reed
| musician          = Oliver Wallace
| animator          = Ed Aardal Hugh Fraser Dan MacManus (effects) George Nicholas John Sibley
| layout artist     = Al Zinnen
| background artist = Art Riley Walt Disney Productions RKO Radio Pictures
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 6 minutes
| country           = United States
| language          = English
| preceded by       = Teachers Are People
| followed by       = How to Be a Detective
}} Walt Disney RKO Radio Pictures. The cartoon follows Goofy on an ill-fated vacation trip traveling cross country. It was directed by Jack Kinney and features the voices of Pinto Colvig as Goofy and Alan Reed as the narrator and a hitchhiker.

==Plot== Fond du dude ranching, North Woods."
 vignettes showing Goofys many travel problems, all accompanied by sarcastic comments from the narrator. The first problem Goofy faces is getting stuck behind a white slow-moving travel trailer pulled by a yellow car that he cant seem to pass. The same trailer continues to follow Goofy, yet always remains one step ahead of him and causes most of his problems.

The first time Goofy tried to pass it, a milk bottle falls off the trailers platform and damages his cars tire and motor and is forced to go to a workshop to get it fixed. The owner fixes the motor but the workshop then closes for two weeks before Goofy asks them to fix the tire and is forced to do it himself. The trailer comes by while hes working. After getting his car fixed, Goofy races through the desert and comes across a traveler and offers him a lift but he refuses. Goofy passes a traffic light which is on "stop". He managed to stop and go back to it. While waiting, a rain cloud shows up and pours water over Goofys car. At the same time, the traffic light switches to "go" and the trailer passes him. In response, Goofy tries to pass it a second time, but dust was swept out the trailers door, clouding Goofys sight. This also causes him to drive off the road and into a tree, allowing the trailer to pass him again. It was then nighttime and Goofy tries to find a place to sleep. He comes across a sign and after striking a match, he lifts his car with a carjack to shine its headlights on the sign so he can read it. He then turns around and notices that all hotels have no vacancy except one. However, his car runs out of gas before he can reach it and worse of all, the trailer has taken the extra spot in the hotel. Goofy pushes his car to a gas station, but it suddenly rolls down a hill, leading him to a hotel that has a beautiful designed house which reveals to be a fake display in front of a plain cabin. He stays there for the night, but was forced to leave when a train wakes him up. A really tired Goofy travels through the night, being stunned by passing cars along the way. He once again encounters the trailer and can see people partying inside of it and is nearly hit by a truck when he tries to pass it again. He is finally able to pass the trailer by driving on a cliff wall, but when he yells at the driver, he is alarmed to see no one driving the car. He speeds away to avoid the car and trailer which is running out of control. Goofy is then knocked out of his car and into the trailers car. After he notices that hes driving the trailer, he sees his own car slowing down while he passes it and is caught by a policeman. He is then arrested for speeding and is finally seen in jail, but happy for having found the "perfect haven for rest and relaxation."

==Releases==
*1952 &ndash; Original theatrical release
*1957 &ndash; Walt Disney anthology television series|Disneyland, episode #4.11: "How to Relax" (TV)
*c. 1983 &ndash; Good Morning, Mickey!, episode #35 (TV)
*1984 &ndash; " " (VHS)
*1992 &ndash; " " (VHS) The Ink and Paint Club, episode #1.43: "On Vacation" (TV)
*2002 &ndash; " " (DVD)
*2005 &ndash; "  (DVD)

==External links==
* 
*  at The Encyclopedia of Animated Disney Shorts
*  from 2719 Hyperion, essay on the topic of travel trailers in Disney films; May 6, 2008

 
 
 
 
 