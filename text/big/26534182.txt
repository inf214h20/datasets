Pandippada
{{Infobox film
| name = Pandippada
| image = Pandippada.jpg
| image_size =
| alt =
| caption = DVD poster
| director = Rafi Mecartin Dileep    Anoop
| writer = Rafi Mecartin
| narrator = Rafi Mecartin
| starring = Dileep  Navya Nair  Prakash Raj  Harisree Asokan Rajan P. Dev
| music = Suresh Peters   (Songs)   S. P. Venkatesh  (background score) 
| cinematography = Saloo George
| editing = Harihara Puthran
| studio = Graand Production
| distributor = Kas Kalasangam  Wrights Releases 
| released = 4 July 2005  (Kerala) 
| runtime = 148 minutes
| country = India Tamil
| budget =
| gross =  
| preceded_by =
| followed_by =
}}
 Malayalam comedy film written and directed by Rafi Mecartin, starring Dileep (actor)|Dileep, Navya Nair, and Prakash Raj in the lead roles. It also stars Harisree Asokan, Rajan P. Dev, Salim Kumar, Ambika (actress)|Ambika, Cochin Haneefa, Indrans, and Sukumari in other pivotal roles.   The film was co-produced by Dileep under the banner of Graand Production.

The story follows Malayali Bhuvana Chandran (Dileep), an unsuccessful entrepreneur in a lot of debt, who gets involved in a land conflict between two powerful landlords in Tamil Nadu: Pandi Durai (Prakash Raj) and Karuppayya Swami (Rajan P. Dev).

==Plot==
Pandippada is the story of a rivalry between Pandi Durai (Prakash Raj) and Karuppayya Swami (Rajan P. Dev) who are landlords that brutally rule villages. Bhuvana Chandran (Dileep (actor)|Dileep) is a 25 year old man who gets a piece of land in the same village. He has a lot of debts and in order to clear that, he has to sell this land. But Pandi Durai and Karuppayya will not allow him to sell the land.

Bhuvana chandran comes to this village to check the possibilities of selling the land. Then he sees his best friend Bhasi (Harisree Ashokan) who lived there and asks for his help. Bhasi takes him to Pandu Durai whose gang he was a member of and Bhuvana joins Pandis group. Later he falls in love with Meena (Navya Nair) who is the daughter of Karuppayya Swami. 
 

But Pandi Durai also wants to marry Meena and this makes Pandi and Bhuvana as enemies. Pandi wants to kill Bhuvana. Karuppayya fixes the marriage of Meena and Bhuvana. Pandi also fixes his marriage with Meena on the same date. In the fight that follows, Bhuvana beats up Pandi and finally gives a gun to him and asks him to shoot him if he cannot see him as a brother. Pandi realises his mistakes and forgives them both and attends their marriage.

==Cast== Dileep ... Bhuvana Chandran
* Navya Nair ... Meena
* Prakash Raj ... Pandi Durai
* Harisree Asokan ... Bhasi
* Rajan P. Dev ... Karuppayya Swami
* Salim Kumar ... Umagandhan Ambika ... Mallika
* Cochin Haneefa... Ummachan
* Sukumari ... Pandi Durais Mother
* T. P. Madhavan ... Bhuvanachandrans Father
* Indrans ... Veeramani
* Subbalakshmi ... Meenas Grandmother
* Neena Kurup ... Bhuvanachandrans Sister
* Zeenath ... Bhuvanachandrans Mother

==Soundtrack==
The music was composed by Suresh Peters and lyrics was written by Chittoor Gopi, RK Damodaran, IS Kundoor, Nadirsha and Santhosh Varma. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ariyaathe Ishtamaay   || || || 
|-  Jyotsna || Chittoor Gopi || 
|- 
| 3 || Ariyaathe Ishtamaay   || V Devanand || Chittoor Gopi || 
|- 
| 4 || Mayilin || Sujatha Mohan, Afsal, Vidhu Prathap || RK Damodaran || 
|- 
| 5 || Mayilin   || Sujatha Mohan, Afsal, Vidhu Prathap || RK Damodaran || 
|- 
| 6 || Mele mukilin || KS Chithra, Afsal || IS Kundoor || 
|- 
| 7 || Panchaayathile || Sujatha Mohan, Afsal || Nadirsha || 
|- 
| 8 || Ponkanavu minukkum || Mano (singer)|Mano, Ranjini Jose, Timmi || Santhosh Varma || 
|}

==Reception==
The movie ran for more than 100 days and was declared as a box office success. This once again confirmed Dileeps status as a star.

==References==
 
Sounds:
Dileep ... Bhuvana Chandran
Harisree Asokan ... Bhasi
Navya Nair ... Meena
Prakash Raj ... Pandi Durai
Cochin Haneefa... Ummachan
Sukumari ... Pandi Durais Mother
T. P. Madhavan ... Bhuvanachandrans Father
Indrans ... Veeramani
Subbalakshmi ... Meenas Grandmother

==External links==
*  

 
 
 
 
 