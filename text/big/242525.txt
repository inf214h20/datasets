Victor Victoria
 
{{Infobox film
| name           = Victor Victoria
| image          = Victor Victoria (1982 film).jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Blake Edwards Tony Adams Blake Edwards
| screenplay     = Blake Edwards Hans Hoemburg (concept)
| based on       =  
| starring       = {{plainlist|
* Julie Andrews
* James Garner Robert Preston
* Lesley Ann Warren
* Alex Karras
}}
| music          = Songs:  
| choreography   = Paddy Stone
| cinematography = Dick Bush
| editing        = Ralph E. Winters
| studio         = Pinewood Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 132 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          = $28,215,453
}} Robert Preston, Tony Adams, Academy Award for Original Music Score. It is a remake of the 1933 German film Viktor und Viktoria.

==Plot== Robert Preston), aka Toddy; Richard dresses, takes money from Toddys wallet and leaves Toddys apartment. Going about his day, Toddy, a performer at Chez Lui in Paris, sees Labisse, the club owner, auditioning a frail, impoverished soprano, Victoria Grant (Julie Andrews). After the audition, Labisse drily writes her off, and she responds by sustaining a pitch to shatter his wine glass using resonant frequency. That night, Richard comes to Chez Lui as part of a straight foursome and Toddy incites a brawl by insulting Richard and the women in his group. Labisse fires Toddy and bans him from the club. Walking home, he spots Victoria dining at a restaurant, and she invites him to join her. As neither of them can pay for the meal, she dumps a cockroach in her salad to avoid paying their check, but it escapes and the whole place breaks out in havoc.

The duo run out through the rain to Toddys, and he invites her to stay when she finds that the rain has shrunk her cheap clothes. The next morning Richard shows up to collect his things. Victoria, who is wearing his clothes, hides in Toddys closet. When she thinks that Richard might harm Toddy, she ambushes Richard and literally kicks him out. Witnessing this, Toddy is struck with the inspiration of passing Victoria off as a man (the illusion convinced Richard who stumbles downstairs to his friends waiting in the car claiming a strange man wearing his clothes hit him) and presenting her to Andre Cassell (John Rhys-Davies), the most successful agent in Paris, as a female impersonator.

Cassell accepts her as Count Victor Grazinski, a gay Polish female impersonator and Toddys new boyfriend. Cassell gets her a nightclub show and invites a collection of club owners to the opening. Among the guests is King Marchand (James Garner), an owner of multiple clubs in Chicago, who is in league with the mob. King attends with his ditzy moll Norma Cassidy (Lesley Ann Warren) and burly bodyguard Bernstein (Alex Karras), aka Squash. Victor is a hit, and King is smitten, but devastated and incredulous when she is "revealed" as a man at the end of her act. King is convinced that "Victor" is not a man.

After a quarrel with Norma and his subsequent failure with her later that night, King sends her back to America. Determined to get the truth of Victors gender, King sneaks into Victoria and Toddys suite and confirms his suspicion when he spies her getting into the bath. He keeps his knowledge secret and invites Victoria, Toddy, and Cassell to Chez Lui, where Toddy is now welcomed due to Victors status as a big star. Another fight breaks out with exactly the same foursome as before; Squash and Toddy are arrested with the bulk of the club clientele, but King and Victoria escape. King kisses Victoria pretending that he does not care about Victorias gender (although he of course actually knows that she is a woman), leading them to get together.

Squash returns to the suite and catches King with Victoria in bed. King tries to explain, but soon receives a shocker himself - Squash reveals himself to be gay. Meanwhile, Labisse hires a P.I., Charles Bovin, to investigate Victor. Victoria and King live together for a while, but keeping up the public act of Victoria being a man strains the relationship and King ends it. Back in Chicago, Norma tells Kings club partner Sal Andretti (Norman Chancer), that King is having an affair with Victor.

At the same time that Victoria has decided to give up the persona of Victor in order to be with King, Sal arrives and demands that King transfer his share of the empire to Sal for a small portion of its worth. Squash tells Victoria whats happening, and she interrupts the paperwork signing to show Norma that she is really a woman, and prevent King from having to lose his stake. That night at the club Cassell tells Toddy and Victoria that Labisse has lodged a complaint against him and "Victor" for perpetrating a fraud. The Inspector confirms to Labisse that the performer is a man and Labisse is an idiot.

In the end, Victoria joins King in the club as her real self. King is stunned, as moments earlier, the announcer had said that Victor was going to perform. It is revealed that Toddy is masquerading as Victor. After an intentionally disastrous, but hilarious performance, Toddy claims that this is his last performance. The film ends with King, Squash, Victoria, Cassell and the public applauding enthusiastically.

==Cast==
 
* Julie Andrews as Victoria Grant/Count Victor Grazinski
* James Garner as King Marchand Robert Preston as Carroll "Toddy" Todd
* Lesley Ann Warren as Norma Cassidy
* Alex Karras as "Squash" Bernstein
* John Rhys-Davies as Andre Cassell
* Graham Stark as the Waiter
* Peter Arne as Labisse
* Malcolm Jamieson as Richard Di Nardo
* David Gant as the diner manager
* Sherloque Tanney as Charles Bovin
* Michael Robbins as Manager of Victorias hotel
* Maria Charles as Madame President
* Glen Murphy as Boxer
* Geoffrey Beevers as Police Inspector
* Norman Alden as Man in Hotel with Shoes (uncredited)
* Neil Cunningham as the MC
 

==Musical numbers==
The vocal numbers in the film are presented as nightclub acts, with choreography by Paddy Stone. However, the lyrics or situations of some of the songs are calculated to relate to the unfolding drama. Thus, the two staged numbers "Le Jazz Hot" and "The Shady Dame from Seville" help to present Victoria as a female impersonator. The latter number is later reinterpreted by Toddy for diversionary purposes in the plot, and the cozy relationship of Toddy and Victoria is promoted by the song "You and Me", which is sung before the audience at the nightclub. 

# "Gay Paree" - Toddy (music composed by Henry Mancini)
# "Le Jazz Hot!" - Victoria (music composed by Henry Mancini)
# "The Shady Dame from Seville" - Victoria (music composed by Henry Mancini)
# "You and Me" - Toddy, Victoria (music composed by Henry Mancini)
# "Chicago, Illinois" - Norma (music composed by Henry Mancini)
# "Crazy World" - Victoria (music composed by Henry Mancini)
# "Finale/Shady Dame from Seville (Reprise)" - Toddy (music composed by Henry Mancini)

==Production== German film Viktor und Viktoria by Reinhold Schünzel. According to Edwards, the screenplay took only one month to write. There was also a 1935 remake named First a Girl, made in the United Kingdom and directed by Victor Saville, about a woman who stands in for a female impersonator and becomes a hit. Julie Andrews watched the 1933 version to prepare for her role.

The costume worn by Julie Andrews, in the number "The Shady Dame from Seville" is in fact the same costume worn by Robert Preston at the end of the film. It was made to fit Preston, and then, using a series of hooks and eyes at the back, it was drawn in tight to fit Andrews shapely figure. Additional black silk ruffles were also added to the bottom of the garment, to hide the differences in height. The fabric is a black and brown crepe, with fine gold threads woven into it, that when lit appears to have an almost wet look about it. 
 TV movie One Special Night (1999).

==Reception==
Victor/Victoria currently holds a 96% fresh rating on review aggregate website Rotten Tomatoes, with the consensus "Driven by a fantastic lead turn from Julie Andrews, Blake Edwards musical gender-bender is sharp, funny and all-round entertaining." 

==Awards== Best Original Song Score and its Adaptation or Adaptation Score. It was also nominated for: Best Actor in a Supporting Role (Robert Preston) Best Actress in a Leading Role (Julie Andrews) Best Actress in a Supporting Role (Lesley Ann Warren) Best Art Tim Hutchinson, William Craig Smith, Harry Cordwell) Best Costume Design Best Writing, Screenplay Based on Material from Another Medium   

American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs − #76

==See also==
* Cross-dressing in film and television

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*   at Archive of American Television

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 