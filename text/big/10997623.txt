The Swiss Conspiracy
{{Infobox film
| name           = The Swiss Conspiracy Jack Arnold
| producer       = Maurice Silverstein
| image          =
| image_size     = 175px
| caption        = Michael Stanley John Ireland
| cinematography = W.P. Hassenstein
| distributor    = S.J. International Pictures
| released       = 1976 (UK) September 1977 (USA)
| runtime        = 83 min. English
| country        = Germany United States
| music          =
| awards         =
| budget         =
}} 1976 action Jack Arnold. It was co-produced between Germany and the United States.

==Plot==
A Swiss bank learns that the confidentiality of several anonymous numbered accounts has been compromised and blackmail threats have been made to five holders of the accounts. They include a crooked arms dealer, who received a demand for five million Swiss francs. He refuses to pay and is shot dead. The bank is also told to pay ten million francs to keep the accounts secret.
 John Ireland), John Saxon) and Dutchman Andre Kosta (Arthur Brauss).
 mistress of the banks vice-president, Franz Benninger (Anton Diffring). There is also Benninger himself as well as Korsak (Curt Lowens) and Sando (David Hess), who are out to kill Hayes and Christopher.

Bank president Johann Hurtil (Ray Milland) cannot believe that Benninger is corrupt. However, it emerges that the latter transferred control of a bank account to his mistress, who was legally entitled to it but didnt have the correct documents.

Captain Hans Frey (Inigo Gallo) of the Swiss Federal Police is suspicious of Christophers activities and follows him.

The bank decides to pay the blackmailer, using uncut diamonds. Christopher insists on accompanying the diamonds to the collection point high in the snow-covered Alps. The blackmailees turn out to be blackmailing each other and the collector of the diamonds is shot, falling off a high alpine rock face. Christopher recovers the stones.

==Cast==
 
* David Janssen as David Christopher
* Senta Berger as Denise Abbott John Ireland as Dwight McGowan John Saxon as Robert Hayes
* Ray Milland as Johann Hurtil
* Elke Sommer as Rita Jensen
* Anton Diffring as Franz Benninger
* Arthur Brauss as Kosta 
 

==Production==
Though burdened by a poor soundtrack and many obvious James Bond-esque gimmicks, the film is notable for beautiful scenery, being filmed entirely in and around Zurich.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 