Dreamland (2007 film)
{{Infobox film
| name           = Dreamland
| image          = Dreamland07.jpg
| caption        = Dreamland DVD cover 
| director       = James P. Lay
| producer       = Kenny Saylors Kyle Saylors 
| writer         = James P. Lay Kenny Saylors Kyle Saylors
| starring       = Jackie Kreisler Shane Elliott Mark Bernier Jonathan Breck
| music          = Jason Livesay Nolan Livesay
| cinematography = Jonathan Hale
| editing        = Chris Conlee
| distributor    = Image Entertainment
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
}}
Dreamland is an American science fiction film that was released on DVD February 27, 2007.

==Plot==
In the Nevada mountains between Las Vegas and Reno in the desolate nuclear testing grounds of Dreamland (Area 51), a young couple Megan (Jackie Kreisler) and Dylan (Shane Elliott) stop in a greasy spoon cafe where they learn about the Area 51 government base a few miles away. After they get back on the road, Dylan turns on the radio.  The only broadcast he can find is a speech from Adolf Hitler at the 1936 Olympic Games. The car dies and a visitor appears from another moment in time. When Megan and Dylan look closely they realize that it is Hitler from the past. Past and present intersect throughout the film. Demonic versions of those long dead are also encountered.

==External links==
*  
* {{cite web
| title =Excerpt of the film
| work =image-entertainment.com
| url =http://www.image-entertainment.com/dvd/detail.cfm?productID=49402 
| accessdate=2007-02-19 archiveurl = archivedate = 2007-03-25}}

 
 
 
 
 


 