Captain Midnight (serial)
{{Infobox film
| name           = Captain Midnight
| image          = Captain Midnight.jpg
| image_size     = 175px
| caption        =
| director       = James W. Horne
| producer       = Larry Darmour George Plympton same name created by Wilfred G. Moore and Robert M. Burtt.
| narrator       = Knox Manning Dave OBrien James Craven Bryant Washburn Sam Edwards Guy Wilkerson
| music          = Lee Zahler
| cinematography = James S. Brown Jr. Black and white Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 270 minutes 15 episodes
| country        = United States
| language       = English
| budget         =
| gross          =
}} serial released serial of the same name broadcast from 1938 to 1949.

==Plot==
Captain Midnight was only one of the many aviation serials released in wartime whose leading characters were derived from early pulp magazines and radio favorites. In this serial, Captain Albright is an extremely skilled aviator better known as Captain Midnight, who is assigned to neutralize the sinister Ivan Shark, an evil enemy scientist who is bombing major American cities. Our hero leads the Secret Squadron, whose staff includes Chuck Ramsay who is Midnights ward and Ichabod Icky Mudd, the Squadrons chief mechanic. Shark has developed a highly efficient mercenary organization, being aided by his daughter Fury who is highly intelligent and second in command, Gardo the henchman and Fang, an Oriental ally. Unfortunately, Shark is after a new range finder invented by the altruistic scientist, John Edwards, whose beautiful daughter, Joyce, the villains attempt to capture to blackmail the inventor.

==Cast==
{|
|- Dave OBrien Dave OBrien || Captain Albright / Captain Midnight
|- Dorothy Short || Joyce Edwards
|- James Craven James Craven || Ivan Shark
|- Bryant Washburn || John Edwards
|- Sam Edwards || Chuck Ramsey
|- Guy Wilkerson || Ichabod Icky Mudd
|- Luana Walters || Fury Shark
|- Joe Girard || Major Steel
|-
| Ray Teal || Borgman, henchman
|-
| George Pembroke &nbsp; &nbsp; &nbsp; || Dr. James Jordan
|-
| Chuck Hamilton || Martel, henchman
|-
| Al Ferguson || Gardo, henchman
|- Franklyn Farnum || G-Man Allen
|- George Magrill || Red, henchman
|- Edward Peil Sr. || Fang
|- Jack Perrin || Cop
|- Lee Shumway || G-Man Burns
|- Slim Whitaker || Police Car 27 patrolman
|-
|}

==Chapter titles==
# Mysterious Pilot
# The Stolen Range Finder
# The Captured Plane
# Mistaken Identity
# Ambushed Ambulance
# Weird Waters
# Menacing Fates
# Shells of Evil
# The Drop to Doom
# The Hidden Bomb
# Sky Terror
# Burning Bomber
# Death in the Cockpit
# Scourge of Revenge
# The Fatal Hour
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = pp. 232
 | chapter = Filmography
 }} 

==Release==
This serial was released in Latin America in November 1942, under the title El Capitán Medianoche, in English with Spanish subtitles.

==See also== List of film serials by year
*List of film serials by studio

==References==
  

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 

 