The Flats (film)
{{Infobox Film |
|  name           = The Flats
|  image          = The Flats Poster.jpg
|  caption        = Theatrical release poster
|  director       = Kelly Requa Tyler Requa
|  writer         = Tyler Requa Kelly Requa
|  starring       = Chad Lindberg Judy Herrera Sean Christensen Cristen Coppen 
|  cinematography = Lawrence Schweich 
|  distributor    = IndieFlix
|  released       = 2002
|  country        = United States
|  language       = English
|  runtime        = 102 minutes
}}
The Flats is a 2002 independent film written and directed by brothers Tyler Requa and Kelly Requa.  Filmed in October 2001 near Mt. Vernon and the Skagit Valley of Washington, The Flats was produced by Clear Pictures, LLC  and premiered at the 2002 Seattle International Film Festival.

==Plot summary==
The Flats tells a cautionary tale of friendship and desire played out among a group of 20-somethings transitioning into adulthood.  Main character Harper and his buddies make the most of their freedom prior to an impending court-ordered jail sentence.  Things get even more serious when Harper and his best friends girlfriend, Paige, begin a sobering relationship.

==Featured cast==
* Chad Lindberg as Harper 
* Sean Christensen as Luke 
* Judy Herrera as Paige
* Cristen Coppen as Kate
* Lindsay Beamish as Jennifer 
* Greg Fawcett as Chaz
* Danny Pickering as Mark 
* Swil Kanim as Jesse
* Luc Reynaud as Tully 
* Paul West Jr. as Lukes Father
* Rabecca Rosencrans as Lukes Mother

==Awards==
*DC INDEPENDENT FILM FESTIVAL - Grand Jury Special Mention
*KINDRED INTERNATIONAL FILM FESTIVAL - Winner, Best Actor (Chad Lindberg)
*NEW YORK INTERNATIONAL INDEPENDENT FILM FESTIVAL - Winner, Best Score (Sean Christensen), Best Actress (Judy Herrera)

==External links==
*  
*  

 
 
 
 
 
 


 