Superman III
{{Infobox film
| name           = Superman III
| image          = Superman III poster.jpg
| caption        = Theatrical release poster
| writer         = {{Plainlist| David Newman
* Leslie Newman }}
| based on       =   Joe Shuster}}
| starring = {{plainlist|
* Christopher Reeve
* Richard Pryor
* Jackie Cooper
* Marc McClure
* Annette OToole
* Annie Ross
* Pamela Stephenson
* Robert Vaughan
* Margot Kidder
}}
| music          = {{Plainlist|
* Ken Thorne
* John Williams  
* Giorgio Moroder   }}
| cinematography = Robert Paynter
| editing        = John Victor-Smith
| studio         = {{Plainlist|
* Cantharus Productions N.V.
* Dovemead Films }}
| distributor    = Warner Bros.
| director       = Richard Lester 
| producer       = {{Plainlist|
* Ilya Salkind
* Pierre Spengler }}
| released       =  
| runtime        = 125 minutes
| country        = United Kingdom 
| language       = English
| budget         = $39 million 
| gross          = $59.9 million
|}}
Superman III is a 1983 British/American  , released on July 23, 1987.
 campy tone, as well as the casting and performance of Pryor, Reeve was praised for his much darker performance as the corrupted Superman. Following the release of this movie, Pryor signed a five-year contract with Columbia Pictures worth $40 million. 

==Plot== CEO Ross Webster. Webster is obsessed with the computers potential to aid him in his schemes to rule the world financially. Joined by his sister Vera and his "psychic nutritionist" Lorelei Ambrosia, Webster blackmails Gorman into helping him.
 Smallville for his high school reunion. En route, as Superman, he extinguishes a fire in a chemical plant containing highly-unstable Beltric acid that can produce corrosive vapor when superheated. At the reunion Clark is reunited with childhood friend Lana Lang, a divorcée with a young son named Ricky, and harassed by Brad Wilson, an ex-boyfriend of hers.
 Vulcan to Lois Lanes Daily Planet interview with Superman, in which Superman identified it as his only weakness. Gus uses Vulcan to locate Kryptons debris in outer space.

Lana convinces Superman to appear at Rickys birthday party, but Smallville turns it into a town celebration. Gus and Vera, disguised as United States Army officers, give Superman the Kryptonite as a gift, but it appears ineffective. However, Superman becomes selfish, focusing on his lust for Lana, causing him to delay rescuing a truck driver. Superman begins questioning his self-worth; he becomes depressed, angry and casually destructive, committing petty acts of vandalism such as blowing out the Olympic Flame and straightening the Leaning Tower of Pisa.

Webster orders Gorman to direct all oil tankers to the middle of the Atlantic Ocean and have them idle until further notice. Ross makes a deal with Gorman, agreeing to build a supercomputer in return for Gormans aid. When the captain of one tanker refuses to idle, Superman dumps its oil into the ocean.

Superman goes on a drinking binge, but is overcome by guilt and undergoes a nervous breakdown. After nearly crash-landing in a junkyard, Superman splits into two personas: the immoral, selfish, corrupted Superman and the moral, righteous Clark Kent. They engage in a battle, ending when Clark strangles his evil identity. Restored to his normal self, Superman repairs the damage his evil counterpart caused.
 MX missile en route to the Grand Canyon and the villains hideout, Superman confronts Webster, Vera and Lorelei for a final showdown. He is forced to battle Gormans supercomputer, which weakens him with a beam of pure Kryptonite. 

Horrified by the prospect of "going down in history as the man who killed Superman", Gorman destroys the Kryptonite ray with a firefighters axe, whereupon Superman flees. The computer becomes self-aware and defends itself against Guss attempts to disable it, draining power from electrical towers. Ross and Lorelei escape from the control room, but Vera is pulled into the computer and transformed into a cyborg. Vera attacks her brother and Lorelei with beams of energy that immobilize them. Superman returns with a canister of the Beltric acid from the chemical plant he saved earlier. Superman places the canister by the supercomputer, which does not resist as it suspects no immediate danger. The intense heat emitted by the machine causes the acid to turn volatile, destroying the supercomputer. Superman flies away with Gus, leaving Webster and his cronies to deal with the authorities.  He drops Gus off at a West Virginia coal mine.

Superman returns to Metropolis. As Clark, he pays a visit to Lana, who has relocated to the big city and found employment as the new secretary to Perry White. He is attacked by Brad, who has stalked Lana in Metropolis, and Brad ends up thrown out on a room service cart. The film ends with Superman flying into the sunrise for further adventures.

==Cast==
* Richard Pryor as August "Gus" Gorman: A bumbling computer genius who works for Ross Webster to destroy Superman.
*   twice, Superman meets a new villain: Ross Webster, who is determined to control the worlds coffee and oil supplies. Superman also battles personal demons after an exposure to a synthetic form of kryptonite that corrupts him.
* Robert Vaughn as Ross Webster: A villainous multimillionaire. After Superman prevents him from taking over the worlds coffee supply, Ross is determined to destroy Superman before he can stop his plan to control the worlds oil supply. He is an original character created for the movie.
*  .
* Annie Ross as Vera Webster: Ross sister and partner in his corporation and villainous plans.
* Pamela Stephenson as Lorelei Ambrosia: Ross assistant and girlfriend. Lorelei, a voluptuous blonde bombshell, is well-read, articulate and skilled in computers, but conceals her intelligence from Ross and Vera, to whom she adopts the appearance of a superficial fool. As part of Ross plan, she seduces Superman.
* Jackie Cooper as Perry White: The editor of the Daily Planet.
*  , which put her in the middle of a front-page story.
* Marc McClure as Jimmy Olsen: A photographer for the Daily Planet.
* Gavan OHerlihy as Brad Wilson: Lanas former boyfriend.

Film director/puppeteer Frank Oz originally had a cameo in this film as a surgeon, but the scene was ultimately deleted. That scene had been included in the TV extended version of the film.

==Production==
===Development=== treatment for this film that included Brainiac (comics)|Brainiac, Mister Mxyzptlk and Supergirl, but Warner Bros. did not like it.  The treatment was released online in 2007.  The Mr. Mxyzptlk portrayed in the outline varies from his good-humored comic counterpart, as he uses his abilities to cause serious harm. Dudley Moore was the top choice to play the role.  Meanwhile, in the same treatment, Brainiac was from Colu and had discovered Supergirl in the same way that Superman was found by the Jonathan and Martha Kent|Kents. Brainiac is portrayed as a surrogate father to Supergirl and eventually fell in love with his "daughter", who did not reciprocate his feelings, as she had fallen in love with Superman.

===Music===
 
  synthesizer pop, Giorgio Moroder was hired to create songs for the film (though their use in the film is minimal).

==Distribution==

===Promotion=== Warner Books Arrow Books in the United Kingdom to coincide with the films release; Severn House published a British hardcover edition. Kotzwinkle thought the novelization "a delight the world has yet to find out about."  However, writing in Voice of Youth Advocates, Roberta Rogow hoped this would be the final Superman film and said, "Kotzwinkle has done his usual good job of translating the screenplay into a novel, but there are nasty undertones to the film, and there are nasty undertones to the novel as well. Adults may enjoy the novel on its own merits, as a Black Comedy of sorts, but its not written for kids, and most of the under-15 crowd will either be puzzled or revolted by Kotzwinkles dour humor." 

A video game for Superman III was developed for the Atari 8-bit family of computers by Atari, Inc. in 1983, but was ultimately cancelled. A prototype box for the Atari 5200 version also exists, although existence of the actual game for this console remains unconfirmed.  The game (perhaps intended to be like Missile Command) would have been loosely based on the plotline for Superman III. 

==Reception==

===Box office===
The total domestic box office gross (not adjusted for inflation ) for Superman III was United States dollar|$59,950,623.  The film was the 12th highest grossing film of 1983 in North America. 

===Critical response===
Reviews for the film were mixed from fans and mostly negative from critics. At  .  Film critic   including Worst Supporting Actor for Richard Pryor and Worst Musical Score for Giorgio Moroder. 

Audiences also saw   in 1987, with which the Salkinds had no connection). After Margot Kidder publicly criticized the Salkinds for their treatment of Donner,  the producers "punished" the actress by reducing her role in Superman III to a brief cameo.  

In his commentary for the 2006 DVD release of Superman III, Ilya Salkind denied any ill will between Margot Kidder and his production team and denied the claim that her part was cut for retaliation. Instead, he said, the creative team decided to pursue a different direction for a love interest for Superman, believing the Lois and Clark relationship had been played out in the first two films (but could be revisited in the future). With the choice to give a more prominent role to Lana Lang, Lois part was reduced for story reasons. Salkind also denied the reports about Gene Hackman being upset with him, stating that Hackman was unable to return because of other film commitments.
 A Hard Three Musketeers slapstick sequence rather than in outer space. Superman III is commonly seen as more or less a goofy (albeit uneven) farce rather than a grand adventure picture like the first two movies. 

On Richard Lesters direction of Superman III, Christopher Reeve stated:

 
 David and Leslie Newman, was also criticized.  When Richard Donner was hired to direct the first two films, he found the Newmans scripts so distasteful that he hired Tom Mankiewicz for heavy rewrites. Since Donner and Mankiewicz were no longer attached to the franchise, the Salkinds were finally able to bring their "vision" of Superman to the screen and once again hired the Newmans for writing duties. 
 William Buckley - all those delicious ponderings, popping of the eyes, licking of the corner of the mouth." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 