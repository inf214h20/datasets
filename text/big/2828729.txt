The Cup (1999 film)
{{ infobox film
| image       = The Cup film.jpg
| caption     = DVD cover Khyentse Norbu
| starring    = Orgyen Tobgyal, Neten Chokling Khyentse Norbu
| producer    = Jeremy Thomas Raymond Steiner Malcolm Watson
| studio      =
| distributor = Palm Pictures Fine Line Features (USA) Festival Media (USA DVD)
| released    = 29 August 1999
| runtime     = 93 minutes
| country     = Bhutan
| language    = Tibetan
| budget      = 
}}
 1999 film Khyentse Norbu. novice monks 1998 World Cup final. 

The movie was entirely shot in the Tibetan refugee village Bir in India (Himachal Pradesh) (almost entirely between Chokling Gompa and Elu Road).   

Producer Jeremy Thomas had developed a relationship with Norbu when he was an advisor on Bertoluccis Little Buddha.  Thomas later remembered his experience making the film:

  }}

== Release ==

The Cup was released to DVD on November 13, 2007 in North America by Festival Media (IBFF). The DVD was mastered from a new direct-to-digital transfer from the original film, and includes a bonus documentary entitled Inside The Cup, featuring the director discussing the film, cinema in general and Buddhist philosophy, along with outtakes from the film. There is also a directors commentary audio track.

==References==
 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 


 
 