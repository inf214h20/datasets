Hot Pepper (1933 film)
{{infobox film
| name           = Hot Pepper
| image          = Hot Pepper - 1933 film.jpg
| imagesize      =
| caption        = 1933 theatrical poster
| director       = John G. Blystone Jasper Blystone (assistant director) William Fox
| writer         = Dudley Nichols (story) Barry Conners (writer) Philip Klein (story)
| starring       = Lupe Vélez Edmund Lowe
| music          = George Lipschultz William Spielter
| cinematography = Charles G. Clarke
| editing        = Alex Troffey
| distributor    = Fox Film Corporation
| released       = January 15, 1933
| runtime        =
| country        = United States
| language       = English
}}
Hot Pepper (1933) is a comedy film starring Lupe Vélez, Edmund Lowe, and Victor McLaglen, directed by John G. Blystone and released by Fox Film Corporation. The film appeared before the enforcement of the Production Code. 
 What Price Glory? (1926), starring Lowe and McLaglen in their characters of Sergeant Harry Quirt and Captain Jim Flagg with Dolores Del Rio as the female costar. The pair made a sequel to that film called The Cock-Eyed World (1929), costarring Lili Damita. Two other films followed before Hot Pepper. 

==Cast==
*Edmund Lowe - Harry Quirt
*Lupe Vélez - Pepper
*Victor McLaglen - Jim Flagg
*El Brendel - Olsen
*Lillian Bond - Hortense
*Boothe Howard - Trigger Thomas
*Gloria Roy - Lily
*Andre Cheron - Maitre d
*Russ Clark - Egan (uncredited)

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 