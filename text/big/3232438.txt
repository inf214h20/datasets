The Candidate (1972 film)
{{Infobox film
| name            = The Candidate
| image           = Candidateposter.jpg
| caption         = Theatrical poster Michael Ritchie
| producer        = Walter Coblenz
| writer          = Jeremy Larner
| starring        = Robert Redford Peter Boyle
| music           = John Rubinstein
| cinematography  = Victor J. Kemper John Korty
| editing         = Robert Estrin Richard A. Harris
| distributor     = Warner Bros.
| released        =  
| runtime         = 109 minutes
| country         = United States
| language        = English
| budget          =
}} Michael Ritchie. 1968 Democratic Presidential nomination.

==Plot== Democratic candidate to oppose California U.S. Senator Crocker Jarmon, a popular Republican Party (United States)|Republican. With no big-name Democrat eager to enter the unwinnable race, Lucas seeks out Bill McKay (Robert Redford), the idealistic, charismatic son of former governor John J. McKay (Melvyn Douglas).

Lucas gives McKay a proposition: since Jarmon cannot lose and the race is already decided, McKay is free to campaign saying exactly what he wants. McKay accepts in order to have the chance to spread his values, and hits the trail. With no serious Democratic opposition, McKay cruises to the nomination on his name alone. Lucas then has distressing news: according to the latest election projections, McKay will be defeated by an overwhelming margin. Lucas says the party expected McKay to lose but not to be humiliated, so he moderates his message to appeal to a broader range of voters.

McKay campaigns across the state, his message growing more generic each day. This approach lifts him in the opinion polls, but he has a new problem: Because McKays father has stayed out of the race, the media speculates his silence is an endorsement of Jarmon. McKay grudgingly meets his father and tells him the problem, and the elder McKay tells the media he is simply honoring his sons wishes to stay out of the race.

McKay continues to gain in the polls until he is only nine points down. Jarmon then proposes a debate. McKay agrees to give answers tailored by Lucas, but just as the debate is ending, McKay has a pang of conscience and blurts out that the debate addressed no real issues, such as poverty and race relations. Lucas is furious, as this will hurt the campaign. The media try to confront McKay backstage, but arrive as his father congratulates him on the debate; instead of reporting on McKays outburst, the story becomes the reemergence of the former governor to help his son. The positive story, coupled with McKays fathers help on the trail, further closes the polling gap.

With election day a few days away, Lucas and McKays father set up a meet and greet with a Labor Union representative to discuss another possible endorsement. During the meeting, the Union representative tells McKay that he feels that they can do a lot of good for each other if they work together. McKay ostensibly tells him that he is not interested in associating with him, but the tension is quelled with uncomfortable yet unanimous laughter. After a publicized endorsement with the Union rep, and with Californian workers now behind him, McKay seems to have taken the lead.

On election day, McKay wins. In the final scene, he escapes the victory party and pulls Lucas into a room while throngs of journalists clamor outside. McKay asks Lucas, "Marvin ... What do we do now?" The media throng arrives to drag them out, and McKay never receives an answer.

==Cast==
* Robert Redford as Bill McKay
* Peter Boyle as Marvin Lucas
* Melvyn Douglas as Former California Governor John J. McKay
* Don Porter as Senator Crocker Jarmon
* Allen Garfield as Howard Klein
* Karen Carlson as Nancy McKay Quinn Redeker as Rich Jenkin
* Morgan Upton as Wally Henderson Michael Lerner as Paul Corliss
* Kenneth Tobey as Floyd J. Starkey
* Natalie Wood as Herself
* Chris Prey as David
* Joe Miksak as Neil Atkinson
* Jenny Sullivan as Lynn
* Tom Dahlgren as The pilot
* Gerald Hiken as The station manager Leslie Allen as Mabel
* Mike Barnicle as Wilson
* Broderick Crawford as Jarmon as Narrator (uncredited)

==Reception==
The film was critically acclaimed, with most praise going towards the script and lead performance. New York Times reviewer Vincent Canby applauded Redfords performance and commented that "The Candidate is serious, but its tone is coldly comic, as if it had been put together by people who had given up hope." 

Christopher Null, from filmcritic.com, gave the film 4.5/5, and said that "this satire on an American institution continues to gain relevance instead of lose it." 

The film holds a fresh score of 95% on review aggregate Rotten Tomatoes, based on eighteen critical reviews.   

===Awards=== Best Writing Best Sound (Richard Portman and Gene Cantamessa).   

==References==
 

==External links==
*  
*   at Rotten Tomatoes
 
 

 
 
 
 
 
 
 
 
 
 
 
 