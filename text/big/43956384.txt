Udayam (1973 film)
{{Infobox film 
| name           = Udayam
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       =
| writer         = Ettumanur Chandrasekharan Nair Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi Madhu Sharada Sharada Adoor Bhasi Prem Prakash
| music          = V. Dakshinamoorthy
| cinematography = SJ Thomas
| editing        = K Sankunni
| studio         = Suchithramanjari
| distributor    = Suchithramanjari
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by P. Bhaskaran . The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, Adoor Bhasi and Prem Prakash in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
  Madhu as Rajasekharan Sharada as Geetha
*Adoor Bhasi as Ready Krishnapillai
*Prem Prakash as Unni
*Sankaradi as Raman Pillai
*Shobha as Kochu Geetha
*T. R. Omana as Lakshmikkuttiyamma Raghavan as Mohandas
*T. S. Muthaiah as Vasu Pillai
*Adoor Bhavani as Bhavaniyamma
*Amba
*Bahadoor as Ittiyavira
*CK Aravindakshan
*KV Mathew as Sadandan
*Master Vijayakumar as Kochu Rajasekharan
*PK Nambiar
*PO Thomas as Thomas
*Philomina as Ikkavamma
*Radhamani as Vanaja
*Raghava Menon as Chakkochan
*Ramankutty Menon as Narayana Pillai
*Rani Chandra as Hema
*Thodupuzha Radhakrishnan as Uthuppu
*Vanchiyoor Radha
*Madhuri as Dancer
*Bhaskaran Nair
*Pala Thankam
*T. K. Balachandran
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran and Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chaale chaalicha || S Janaki || P. Bhaskaran || 
|-
| 2 || En Mandahaasam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Ente Makan Krishnanunni || S Janaki || P. Bhaskaran || 
|-
| 4 || Kalayude Devi || S Janaki, Ambili || Sreekumaran Thampi || 
|-
| 5 || Karalinte Kadalaassil || P Jayachandran || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 