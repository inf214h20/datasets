Farm House (film)
{{Infobox film
| image= Farmhouse_One_Sheet_Style_A.jpg
| caption =
| name = Farmhouse
| director = George Bessudo Steven Weber William Lee Scott Jamie Anne Allman Drew Sidora Kelly Hu Daniel P. Coughlin
| screenplay = Daniel P. Coughlin, Jason Hice
| producer = Mike Karkeh Todd Chamberlain Paul Seng EP Robert Rodriguez
| distributor = IFA Distribution
| released =  
| runtime =
| country = United States
| language = English
}}
 Daniel P. Coughlin.   

==Plot==
Scarlet and Chad are a young married couple, financial troubles and the death of their infant son convince them to move to Seattle, in the hopes of starting a new life. During the drive, Chad falls asleep at the wheel and runs off the road, crashing their vehicle. Although the car itself is totaled, the couple is unharmed. They go to a nearby farmhouse and meet the owner, "Sam" (short for Samael), his wife Lilith, and their deaf handyman helper, Alal. Sam and Lilith let Chad and Scarlet sleep at their house, but during the night Chad and Scarlet are savagely knocked unconscious by Lilith and Sam.

Upon waking, Chad and Scarlet discover that Sam and Lilith intend to torture and kill them. They nearly drown Scarlet in a barrel, but after they leave the room Chad resuscitates her, and the couple tries to flee the farmhouse. Chad is caught by Lilith, but wrestles away a shotgun from her and knocks her unconscious. Scarlet, aided by Alal, runs away into a vineyard field, but is eventually caught by Sam, who kills Alal and takes Scarlet back into the house. While Sam goes to look for Chad, Lilith tortures Scarlet by scraping the flesh off her knees with a grater. Chad interrupts the torture session and kills Lilith by stabbing her in the head with a meat thermometer. Chad tries to set Scarlet free, but is unable to do so before Sam gets back. Chad and Sam fight, and Scarlet stabs Sam in the back with a chefs knife. The couple flees in Sams pickup truck, but Sam revives and pursues them; Chad runs Sam over with the truck several times before they drive off.

After several hours, they begin to wonder why it is not yet morning, and the truck runs out of fuel. Chad and Scarlet set off on foot, only to find themselves mysteriously back at Sam and Liliths farmhouse. Sam, Lilith, and Alal are all revived and confront them. It is revealed that Chad and Scarlet actually died in the car crash and are now in hell. They meet Satan, who shows them that they have been being punished for murdering their son in order to collect a life insurance premium that would be enough to pay back a debt that Chad owed. Scarlet pleads with Satan for a second chance, and she gets it. The film ends with Scarlet looking into the eyes of her infant son. The scene fades to black, leaving the outcome of Scarlets decision open to interpretation of the viewer.

==Cast==
* Jamie Anne Allman - Scarlet
* William Lee Scott - Chad Steven Weber - Samael
* Kelly Hu - Lilith
* Nick Heyman - Alal
* Jack Donner - The Dark One / Doctor Miller
* Sam Sarpong - Jonas
* Drew Sidora - Rebecca
* Adair Tishler - Young Scarlet
* Flynn Beck - Young Scarlets Mother
* Brandon Molale - Man
* Henry Hudson - Gunther
* Elyssa Williams - Dead Baby

==See also==
*  
*  

==References==
 

==External links==
*  

 
 
 
 
 
 