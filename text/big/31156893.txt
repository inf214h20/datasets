Seelabathi
 
{{Infobox film
| name           = Seelabathi
| image          = 
| image size     =
| caption        =
| director       = R. Sarath
| producer       = 
| story          = R.Sarath 
| screenplay     = R. Sarath
| narrator       = Narain
| music          = Ramesh Narayan
| cinematography = Anand Radhakrishnan
| editing        =
| studio         = Rithu Films
| distributor    = Shirdhisai Release
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Narain in the lead roles. The film, according to the director, is for "all those girls who have been reported missing from several parts of the State."  The film opened on 12 December to critical acclaim. 

==Plot==
The movie begins with the arrival of Seelabathi (Kavya Madhavan) and her mother Sumangala (Urmila Unni) to Kerala from Bengal. Though Sumangala returns to Bengal, Seelabathi stays behind because she gets a temporary job as a computer teacher in a school, for a few months. She stays with her grandparents. Her grandfather (Nedumbram Gopi) is a farmer who loves farming more than perhaps anything else. And in school, she soon gets adapted to things and becomes friendly with her students. Sheelabathi also meets a young doctor Jeevan ( Narain), with whom she becomes very friendly. In the village there are people who come to dig huge bore-wells and consequently there is much of water scarcity. And young students disappear mysteriously from the school and end up being sexually exploited. Their parents accuse that it is due to the new teacher’s computer courses that such incidents were happening in the village. Then the story continues telling how Sheelabathi overcomes to all problems.

==Cast==
* Kavya Madhavan as Seelabathi Narain as Dr. Jeevan Bhasker
* Urmila Unni as Sumangala
* Nedumbram Gopi
* Indrans as Vasu/Basu
* C. K. Babu as farmer

==References==
 

==External links==
*   at the British Film Institute Movie Database
*   at the Malayalam Movie Database

 
 
 

 