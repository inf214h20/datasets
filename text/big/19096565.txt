Hour of Decision (film)
{{Infobox film
| name           = Hour of Decision
| image          =
| image_size     =
| caption        =
| director       = C.M. Pennington-Richards
| producer       = Monty Berman
| writer         = Norman Hudis
| based on       =  
| starring       = Jeff Morrow Hazel Court
| music          = Stanley Black
| cinematography = Stanley Pavey
| editing        = Douglas Myers
| studio         = Tempean Films
| distributor    =
| released       = 1957
| runtime        = 81 mins
| country        = United Kingdom
| language       = English
}}
Hour of Decision is a 1957 British mystery film directed by C.M. Pennington-Richards and starring Jeff Morrow, Hazel Court and Anthony Dawson. Ir received 6.2 stars on the IMDb. 

==Plot==
The British wife of an American journalist begins receiving letters blackmailing her over a love affair. Suspicion points to her when the blackmailer is found murdered.

==Cast==
* Jeff Morrow as Joe Saunders
* Hazel Court as Margaret Saunders / Peggy
* Anthony Dawson as Gary Bax
* Mary Laura Wood as Olive Bax
* Alan Gifford as J. Foster Green
* Carl Bernard as Inspector Gower
* Lionel Jeffries as Albert Mayne
* Anthony Snell as Andrew Crest
* Vanda Godsell as Eileen Chadwick
* Robert Sansom as Reece Chadwick
* Garard Green as Tony Pendleton
* Marne Maitland as Club Waiter
* Arthur Lowe as Calligraphy Expert
* Margaret Allworthy as Denise March
* Richard Shaw as Detective Sergeant Dale Frank Atkinson as Caretaker Michael Balfour as Barman
* Reginald Hearne as Personnel Manager
* Dennis Chinnery as Studio Photographer

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 