Objectified
 
{{Infobox Film
| name           = Objectified
| image          = Objectified cover.jpg
| image_size     = 190px
| caption        = 
| director       = Gary Hustwit
| producer       = Gary Hustwit    
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Shelby Siegel Laura Weinberg
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Objectified is a feature-length documentary film examining the role of everyday non-living Physical body|objects, and the people who design them, in our daily lives. The film is directed by Gary Hustwit. Objectified premiered at the South By Southwest Festival on March 14, 2009.

According to Swiss Dots Production, the film is the second part in a 3-film series Design Trilogy 
{{cite web
|url=https://www.youtube.com/watch?v=S9E2D2PaIcI
|title=Objectified trailer
|publisher=
|accessdate=2009-11-13
|last=
|first=
}}
  the first being Helvetica (film)|Helvetica about the famous typeface and the third and last film being the documentary Urbanized.

==Appearing characters==
*Paola Antonelli - Design Curator, Museum of Modern Art (New York)
*Chris Bangle - Former Design Director, BMW Group (Munich)
*Andrew Blauvelt - Design Curator, Walker Art Center Erwan Bouroullec - Designer (Paris) Ronan Bouroullec - Designer (Paris)
*Anthony Dunne - Designer (London)
*Agnete Enga - Senior Industrial Designer, Smart Design
*Dan Formosa - Design & Research, Smart Design (New York)
*Naoto Fukasawa - Designer (Tokyo)
*Jonathan Ive - Senior VP Industrial Design, Apple (Cupertino)
*Hella Jongerius - Designer (Rotterdam)
*Marc Newson - Designer (Paris)
*Fiona Raby - Designer (London)
*Dieter Rams - Former Design Director, Braun Kronberg (Germany)
*Karim Rashid - Designer (New York)
*Alice Rawsthorn - Design Editor, International Herald Tribune
*Amber Shonts - Model
*Davin Stowell - CEO & Founder, Smart Design
*Jane Fulton Suri - IDEO Rob Walker - New York Times Magazine

==References==
 

== External links ==

* 
* 
* 
* 

 
 
 
 
 
 
 
 


 