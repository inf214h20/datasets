Downtown: A Street Tale
 
 Downtown: A Street Tale is a 2004 American drama film.
 Latina stripper Ashley; upper class drug addict Hunter; Romanian prostitute Raquel; and newly arrived from Texas rock star-wannabe Billy and his pregnant girlfriend Cheri. Added to the mix are social worker Aimee Levesque, who operates the fictional shelter Haven House (based on the real-life Covenant House) and keeps an eye on Angelo and his street kids; and a pornography|pornographer, a strip-club owner, and a wheelchair-using drug dealer. Their stories are told in a series of vignettes unfolding in the days just prior to Christmas.
 Chad Allen John Savage as the drug dealer.
 AFI Film Festival on November 7, 2004. Following limited releases in New York City and Los Angeles in 2007, it was screened twice at the Cannes Film Festival in May.

The soundtrack, available on AJM Records, includes a jazz-infused rendition of the Petula Clark classic Downtown (Petula Clark song)|"Downtown" by Irene Cara and "Children of Color" performed by Clark.

The movie was Line Produced by Daniel Sollinger

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 


 