Coldplay Live 2012
 
 
{{Infobox film
| name           = Coldplay Live 2012
| image          = Coldplay Live 2012 (Poster).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original UK cinematic poster
| director       = Paul Dugdale
| producer       = Jim Parsons
| starring       = Guy Berryman Jonny Buckland Will Champion Chris Martin Rihanna
| narrator       = Guy Berryman Jonny Buckland Will Champion Chris Martin
| music          = Coldplay Jon Hopkins
| cinematography = James Tonkin Brett Turnbull 
| editing        = Simon Bryant Tim Thompsett Tom Watson 
| studio         = 
| distributor    = Parlophone
| released       =  
| runtime        = 96 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
{{Infobox album
| Name          = Coldplay Live 2012
| Type          = Live album
| Longtype      = and Video| Video 
| Artist        = Coldplay
| Cover         =
| Released      = 14 November 2012
| Recorded      = 2011, Glastonbury Festival; 2012, Stade de France, Paris and Bell Centre, Montreal
| Genre         = Alternative rock, post-Britpop, electronic rock
| Length        = 66:36
| Label         = Capitol Records|Capitol, Parlophone, EMI
| Producer      = Coldplay, Jim Parsons
| Last album    = Mylo Xyloto (2011) 
| This album    = Coldplay Live 2012 (2012) Ghost Stories (2014)
}}

Coldplay Live 2012 is the collective name for the official documentation of the Mylo Xyloto Tour, performed, recorded and released by British alternative rock band Coldplay. The project consists of a worldwide theatrical and home media film release, Coldplays third live album, after Coldplay Live 2003 and LeftRightLeftRightLeft, and a 183-page e-book depicting the tour. The film and album were recorded over many dates over the 2011 Festival Tour and the Mylo Xyloto Tour beginning in October 2011 and ending in September 2012. The film was opened for one night only on 13 November 2012, at select cinemas around the world. The DVD and Blu-ray release of the film and the live album release occurred on 14 November 2012.

==Release and promotion== Phil Harvey in April 2012. It was first announced under the title "ColdplayFilm".  The name was also inscribed on the Xylobands used on the Mylo Xyloto Tour between April and September 2012. The film/album was first officially announced on 25 September 2012.  On the same day, two websites promoting Live 2012 launched. One promoting the theatrical release of the film, and one promoting the project in general.   A two minute trailer accompanied the announcement, released online and shown in theaters attached to other films as well.  In addition, an exclusive preview of "Us Against the World" was shown on the Live 2012 website, a swell as another select track off the film of users choice.  A 183-page e-book documenting the tour was also announced. 

Live 2012 was first released as an exclusive, one-night-only theatrical release, shown in cinemas across 57 countries around the world.  The film opened at 7:00pm at respective timezones (7:30pm at some screenings), on 13 November 2012. The film was shown twice on the night, closing before midnight.  A partnership deal between Parlophone and Studio 3 saw the screening of Live 2012 on US premium cable television as well.  The film aired exclusively on 14 November 2012 on Epix (TV channel)|Epix, the night after the theatrical release and a week before its commercial release in the United States.

==Reception==
{{Album ratings
| rev1      = Herald Sun
| rev1Score = 4.5/5    
| rev2      = Digital Spy
| rev2Score =     
| rev3      = The Upcoming
| rev3Score =     
| rev4      = IndieWire
| rev4Score = B+    
| rev5      = Contactmusic
| rev5Score = 9/10    
| rev6      = HeyUGuys!
| rev6Score =      
}}

===Critical reception=== collab with a popstar and an appearance on The X Factor later, however, and they almost fucked their rep… Chris Martin bawling backing vocals to the Little Mix girls would have been uglier than Javier Bardem toothless in Skyfall, yet thats all it would have taken." He also says it showcases well the "ridiculous effort that goes in to each and every concert." and that the film/album "skilfully showcases just how incredible an adventure it is to observe a world-beating band at the top of their game. And those still to be converted to the Coldplay cause even after watching this? Well, they can fuck off, then."  Drew Taylor of IndieWire wrote a similar very positive review, praising the Mylo Xyloto tour itself as well, and stating that "even a cursory understanding of what its like to watch Coldplay live is better than nothing at all. Wristbands not included."  Sarah Milton of The Upcoming wrote a very positive review focusing on the bands ability to connect with their fans through Live 2012, writing that "This release of their previous and current work defines their unstoppable prowess by taking tracks to a personal level with their fans. This is a skill not many achieve live, but with this album, Coldplay succeeds. It deserves immense recognition, and we can rest assured that Coldplay will keep raising the bar of stadium rock shows for years to come." 
 Violet Hill, Every Teardrop Is a Waterfall)". It was the album of the week in the Herald Sun. 

Jim Pusey wrote a positive review for Contactmusic, praising the film for its visual effects and cinematography. He also compared the film to the bands previous live video, Live 2003, noticing "a band thats now less awkward and self-conscious." He also gave a positive note to the documentary itself, stating that the most interesting parts were the "behind the scenes interludes that punctuate the performances. Each band member has an opportunity to reveal not only their enthusiasm for playing live around the globe, but also their perspective on the different elements that have made up the tour. These short vignettes include details of how the Mylo Xyloto artwork evolved and how they tried to translate that to a live setting with audience members wearing glowing wristbands. This peek behind the curtain is beneficial as it makes the gig footage from 3 different stops on the tour (including their most recent Glastonbury headline set) more engaging."  Kenji Lloyd of HeyUGuys! wrote that the film "goes above and beyond any ordinary concert film, which is just what you’d expect from a band that isn’t any ordinary band. They go the further mile, always looking to take things further, do things better, make the extra effort. As the band plays through the darkening night, we’re treated to the equally impressive artwork and handwritten lyrics that are a part of their live shows, appearing on the screen itself in a fantastic bleeding of creativity." 

===Commercial performance=== Irish Music DVDs Chart in the week of its release.   

==Track listing==
All songs written and performed by Guy Berryman, Jonny Buckland, Will Champion, Chris Martin and Brian Eno, except where noted.    

{{Track listing
| headline  = CD / Digital download 
| title1    = Mylo Xyloto
| length1   = 0:56 
| note1     = Stade de France, 2 September 2012
| title2    = Hurts Like Heaven
| length2   = 4:16
| note2     = Stade de France, 2 September 2012
| title3    = In My Place
| length3   = 3:54
| note3     = Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin
| title4    = Major Minus
| length4   = 3:39
| note4     = Plaza de Toros de Las Ventas, 26 October 2011 / Stade de France, 2 September 2012 Yellow
| length5   = 6:51
| note5     = Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin
| title6    = God Put a Smile upon Your Face
| length6   = 5:21
| note6     = Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin
| title7    = Princess of China
| note7     = with Rihanna, Stade de France, 2 September 2012
| length7   = 3:48 Up in Flames
| length8   = 3:17
| note8     = Stade de France, 2 September 2012
| title9    = Viva la Vida
| length9   = 4:58
| note9     = Glastonbury Festival, 25 June 2011 / Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin Charlie Brown
| length10  = 5:00
| note10    = Stade de France, 2 September 2012 Paradise
| length11  = 5:32
| note11    = Stade de France, 2 September 2012 Us Against the World
| length12  = 3:52
| note12    = Bell Centre, 27 July 2012 Clocks
| length13  = 4:44
| note13    = Bell Centre, 27 July 2012) (Berryman/Buckland/Champion/Martin
| title14   = Fix You
| length14  = 5:00
| note14    = Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin
| title15   = Every Teardrop Is a Waterfall
| note15    = with  "M.M.I.X." (introduction) , Stade de France, 2 September 2012 / Glastonbury Festival, 25 June 2011 / Bell Centre, 27 July 2012 / Hollywood Bowl, 4 May 2012 / Plaza de Toros de Las Ventas, 26 October 2011
| length15  = 5:25
}}
{{Track listing
| headline  = Film / DVD / Blu-ray  
| collapsed = yes
| title1    = Intro
| length1   = 0:37 
| note1     = includes elements of "Takk..."
| title2    = Mylo Xyloto
| length2   = 0:43
| title3    = Hurts Like Heaven
| length3   = 4:02
| title4    = In My Place
| length4   = 3:48
| title5    = Intermission 1
| length5   = 4:08
| note5     = Narrated by Chris Martin, includes elements of "Dont Let It Break Your Heart"
| title6    = Major Minus
| length6   = 3:30
| title7    = Yellow
| length7   = 4:29
| title8    = Intermission 2
| length8   = 3:51
| note8     = Narrated by Jonny Buckland, includes elements of "Charlie Brown" Violet Hill
| length9   = 3:49
| note9     = La Cigale, 31 October 2011) (Berryman/Buckland/Champion/Martin
| title10    = God Put a Smile upon Your Face
| length10   = 4:58
| title11   = Princess of China
| note11    = with Rihanna
| length11  = 3:59
| title12   = Intermission 3 The Escapist" and "Akio"
| length12  = 3:02
| title13    = Up in Flames
| length13   = 3:13
| title14   = Viva la Vida
| length14  = 4:01
| title15   = Intermission 4
| note15    = Narrated by Guy Berryman, includes elements of "Up with the Birds"
| length15  = 3:45
| title16   = Charlie Brown
| length16  = 4:45
| title17   = Paradise
| length17  = 4:37
| title18   = Us Against the World
| length18  = 3:59
| title19   = Clocks
| length19  = 5:07
| title20   = Intermission 5
| note20    = Narrated by Chris Martin, includes elements of "Mylo Xyloto" and "Hurts Like Heaven"
| length20  = 3:09
| title21   = Fix You
| length21  = 4:54
| title22   = Every Teardrop Is a Waterfall
| note22    = with  "M.M.I.X." (introduction) 
| length22  = 4:49
| title23   = Outro and Credits
| note23    = includes "Up with the Birds"
| length23  = 5:27
}}
{{Track listing
| headline  = DVD / Blu-ray Extras
| collapsed = yes The Scientist
| note24    = Stade de France, 2 September 2012) (Berryman/Buckland/Champion/Martin
| length24  = 5:09
| title25   = Dont Let It Break Your Heart
| note25    = with  "A Hopeful Transmission" (introduction)  Stade de France, 2 September 2012
| length25  = 4:27
| title26   = Photo Gallery
| length26  = 4:05
}}
;Sample credits    Peter Allen and Adrienne Anderson.
*"Princess of China" features a sample from "Takk..." written by Jón Þór Birgisson, Orri Páll Dýrason, Georg Hólm and Kjartan Sveinsson, performed by Sigur Rós.
*"Up with the Birds" features a sample from "Driven by You" by Brian May. It also includes a lyrical sample from "Anthem" by Leonard Cohen.

==Charts and certifications==
 
 

===Peak positions===
{|class="wikitable sortable plainrowheaders"
|-
!Chart (2012)
!Peak position
|- Australian Recording Australian Music DVDs Chart    1
|- Austrian Albums Chart  10
|- Belgian Music DVDs Chart (Flanders)  1
|- Belgian Music DVDs Chart (Wallonia)  1
|- Brazilian Music DVDs Chart  2
|- Czech Albums Chart  4
|- Danish Music DVDs Chart  1
|- Dutch Music DVDs Chart  1
|- French Albums Chart  5
|- German Albums Chart  3
|- Irish Recorded Irish Music DVDs Chart  2
|- Helicon Records|Israeli Albums Chart  1
|- Italian Albums Chart  10
|- Federation of Italian Music DVDs Chart  1
|- Mexican Albums Chart  6
|- Polish Albums Chart  11
|- Portuguese Music DVDs Chart    1
|- Productores de Spanish Music DVDs Chart  1
|- Swiss Albums Chart  11
|- Swiss Music Swiss Music DVDs Chart  1
|- Official Charts UK Music Video Chart  1
|- Sino Chart|China Music DVD Chart  3
|-
|}
 

===Year-end charts===
{|class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!Chart (2012)
!Position
|- ARIA Charts|Australian Music DVDs Chart  2
|- Dutch Music DVDs Chart  1
|- Italian Albums Chart  85
|-
|}

===Certifications===
 
 
 
 
 
 
 
 
 
 
 
 

==Release history==
{| class="wikitable plainrowheaders"
|- Country
!scope="col"|Date Label
!scope="col"|Format Catalog no.
|-
| Worldwide  (Select cinemas) 
| 13 November 2012 
| N/A
| Theatrical release
| N/A
|-
| Japan
| 14 November 2012
| rowspan="12"| Parlophone, EMI Digital download
| N/A
|-
| Australia
| rowspan="10"| 16 November 2012
| rowspan="11"| 50999 015137 2 1, P015 1372 (CD+DVD) 50999 015139 9 8, P015 1399 (DVD+CD) 50999 015141 9 3, P015 1419 (BD+CD)
|-
| Germany
|-
| Switzerland
|-
| Austria
|-
| Belgium
|-
| Netherlands
|-
| Luxembourg
|-
| Finland
|-
| Norway
|-
| Ireland
|-
| United Kingdom
| 19 November 2012
|-
| Canada
| rowspan="7"| 20 November 2012 Capitol
| rowspan="2"| 509990 15137 2 1 (CD+DVD)
|-
| United States
|-
| Italy
| rowspan="5"| Parlophone, EMI
| rowspan="6"| 50999 015137 2 1, P015 1372 (CD+DVD) 50999 015139 9 8, P015 1399 (DVD+CD) 50999 015141 9 3, P015 1419 (BD+CD)
|-
| Spain
|-
| Mexico
|-
| Thailand
|-
| Korea
|-
| Sweden
| 21 November 2012
| EMI
|-
| China 
| 31 August 2013 
| STARSING CULTURE
| CD+DVD
| 9787798503234
|}

==References==
 

==External links==
* The   for the Live 2012 project.
* The   for the Theatrical release of Live 2012.
*   on Discogs.

 

 
 
 
 
 
 
 
 
 
 