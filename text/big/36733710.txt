The Remake
{{Infobox film
| name           = The Remake
| image          = TheRemakeFilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| director       = Tommy Brunswick
| producer       = Todd Brunswick   Tommy Brunswick
| writer         = Todd Brunswick   Dwayne Roszkowski
| screenplay     = 
| story          = Dwayne Roszkowski
| based on       = 
| narrator       = 
| starring       = Cassy Harlo   Jessica Hall   Heather Doba
| music          = James Souva
| cinematography = Todd Brunswick
| editing        = Todd Brunswick
| studio         = Crossbow 5 Entertainment
| distributor    = R-Squared Films   IFM World Releasing
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $15,000
| gross          = 
}}

The Remake is a 2006 horror film directed by Tommy Brunswick, and written by Todd Brunswick and Dwayne Roszkowski.

== Plot ==
 cult classic Slaughter Camp 13, has his throat slit by a hooded assailant.

The next day, the crew of the Slaughter Camp 13 remake head out to the shooting location early, as a part of a bonding exercise. The group are followed by Harolds killer, who had murdered and carjacked the clerk of a store the crew stopped at. After the group reach their destination and set up, the killer spies on them, garrots Kevin with a cable when he goes off for some solitude, and impales Ron with a pipe while he is using the outhouse. By nightfall, the other crew members become worried about Kevin and Ron, so they pair off and go in search of them. After becoming separated from his search buddy Tom, Ari is hacked in the face with a machete by the killer. The next to die is Amy, who is forced to ingest cleaning chemicals when she wanders off alone to snort cocaine.

Back at the campsite, Marco and Sally Anne (who had stayed behind in case Kevin or Ron came back) are stabbed to death with a pole while having sex in their tent. Their bodies are found Seanna, Adrienne, and Peter, who cannot get any cell phone reception, or drive away because all the vehicles have been sabotaged. The trio decide to go to the camp tool shed to see if it has anything that can fix the cars, and are beaten there by Tom, who is beheaded with a shovel by the killer. Moments later, the remaining crew members arrive, with Peter gathering up supplies and heading back to the cars alone, instructing Adrienne and Seanna to stay behind and barricade themselves in the shed. When Peter returns to the vehicles and tries one after tinkering with it, he is knocked out by the killer, who was hiding in the backseat.

Seanna becomes hysterical and runs back to the campsite, where the killer ambushes her, burns her face on the gas grill, and jams kebab sticks down her throat. Adrienne goes after Seanna, and sees what looks the killer seated on a log at the campsite. She attacks the figure with an axe, only to discover that it was a bound and gagged Peter. The blood splattered killer then appears and beheads Adrienne. Afterward, he turns to a hidden camera and proclaims, "No more fucking remakes!" The killer sends the tape to Steve Lehman, a Perimount Pictures executive, who calls the studio and orders that production of Slaughter Camp 13 be halted.
 Michael Myerss mask and knife, and heads out.

== Cast ==

* Cassy Harlo as Seanna Birmingham
* Heather Doba as Adrienne Steel
* Jessica Hall as Amy
* Doug Kolbicz as Tom
* Dwayne Roszkowski as Ronald
* Jason Ryan as Peter Miner
* James Block as Marco
* Adam Lorenz as Ari
* Kris Smith as Kevin Wringer
* Meshelle Melone as Sally Anne
* Gerard Gratiot as Harold Dingle
* Jon Manthei as The Fan
* Rudy Hatfield as Steve Lehman
* Eric Gildenberg as Kid at Market
* Tommy Brunswick as Dead Clerk
* Matt Huls as Butt Double

== Reception ==

Horror Movie Fans gave the film a B+, and said The Remake "is very flawed through laughable acting and characters we cant care about, but it remains to be fun and has a spirit for creative works, calling for the rejection of remakes, unoriginal work, and making films for the money rather than for the craft, and encourages the love of making them and exerting a message and meaning".  A score of three stars out of five was awarded by Slasherpool, which stated The Remake is "a relatively well-crafted slasher flick" that despite its basic premise was worth a watch because "it takes a stand against horror remakes and I am very appreciative that someone took the time to make a movie like this". 

Wildside Cinema found The Remake mildly entertaining, even if it did "serve up a few tired clichés as well as giving in to some of the very stereotypes this film was attempting to dispel (i.e. horror is all about T&A)". 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 