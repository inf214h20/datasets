American Psycho 2
{{Infobox film
| name = American Psycho 2
| image = American Psycho 2.jpg
| image_size = 215px
| alt = 
| caption = DVD cover
| director = Morgan J. Freeman
| producer = Ernie Barbarash
| screenplay = Alex Sanger Karen Craig
| based on =  
| starring = Mila Kunis William Shatner
| music = Norman Orenstein
| cinematography = Vanja Cernjul
| editing = Mark Sanders Lions Gate Films
| distributor = Lions Gate Home Entertainment
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = $10,000,000
}} American Psycho. It is directed by Morgan J. Freeman and stars Mila Kunis as Rachael Newman, a driven criminology student who is drawn to murder. The film also features William Shatner as a professor.
 adapted from a script titled The Girl Who Wouldnt Die. 

==Plot==
The film starts with a 12-year-old girl whose babysitter is on a date with serial killer Patrick Bateman. After Bateman kills and starts to dissect her babysitter, the girl stabs him with an ice pick. Fast forward to the present day and the girl, who is named Rachael Newman, is now a college student studying criminology under Professor Starkman, a former FBI agent. Rachael aspires to join the FBI and is determined to get the teaching assistant position under Starkman, which would make her a shoo-in for the FBI training program. 
 TA position, Rachael decides to murder her as well. After she does so Professor Starkman discovers Cassandras body and calls Daniels to tell him that "shes dead". But he does not identify the victim, and Daniels assumes it must be Rachael. Distraught, Professor Starkman leaves his teaching position, which angers the obsessed Rachael. She reveals that she is not Rachael Newman: she killed the real Rachael at the beginning of the semester and assumed her identity. 

During spring break Rachael stays on campus and confronts Starkman, who had taken Valium and alcohol, and tries to seduce him to get the job. However, Starkman sees she is wearing a dress and necklace he had given to Cassandra. She then confesses her crimes to him, her "crush" on him, and that she knew about his affairs with various women (which included her former babysitter, whom he indirectly killed, since he had told her about Bateman and she decided to track him down), as he backs up towards the window in a state of confusion and fear. Rachael blows him a kiss, and he falls out the window to his death. As she leaves, Rachael realizes a janitor has witnessed Starkmans death, and she murders the janitor, too. Driving away from campus with Starkman in her car, Rachael is stopped by campus security, whom she stabs to death with an ice pick. 

As the film reaches its conclusion, Daniels and two cops pursue Rachael in a car chase, started when she sped by the cops who had Daniels in the car. It ends with Rachael driving off a cliff, resulting in the car exploding. At this point, she is presumed to be dead by the cops who witnessed the event and the media. 

In the final scene, Dr. Daniels is giving a lecture on Rachaels mind and how he wrote a book about her. When he looks up from speaking with a student, he sees Rachael, who has not died after all; she indirectly reveals that she killed Starkmans last assistant, Elizabeth McGuire, and stole her identity to get into Quantico. She allows Dr. Daniels to know because she believes there is no point in committing the perfect crime if no one knows about it and that she is proud of his success. The body that was in the car was the real Rachael. As she walks out of his class, Dr. Daniels is obviously disturbed and rattled.

==Cast==
* Mila Kunis as Rachael Newman, the serial killer, and a student of Professor Starkmans class, seeking to learn everything she can about serial killers.
** Susan Dalton as Rachael Newman, the real Rachael whose identity was stolen.
** Jenna Perry as Young Rachael
* William Shatner as Robert Starkman, a college professor and former FBI agent. He teaches in the Behavioral and Social Sciences department and is Rachaels professor.
* Kim Schraner as Elizabeth McGuire, Starkmans assistant
* Geraint Wyn Davies as Eric Daniels, the school psychiatrist
* Michael Kremko as Patrick Bateman, the killer (in the original, Bateman was played by Christian Bale) in this film, the character exists only in Rachaels memories.
* Robin Dunne as Brian Leads, one of Rachaels classmates; he is choked to death with a condom by her.
* Kim Poirier as Barbara Brown, a student in Starkmans class who is hoping for a good grade, only to be set aside due to the professors affair with Cassandra Blaire.
* Lindy Booth as Cassandra Blaire, a student in Starkmans class with whom he is having an affair.
* Charles Officer as Keith Lawson
* Shoshana Sperling as Gertrude Fleck
* Lynne Deragon as Mrs. Newman
* Philip Williams as Mr. Newman
* Kay Hawtrey as Mrs. Daniels

==Reception==
American Psycho 2 received mostly negative reviews. Review aggregation website Rotten Tomatoes gives the film a score of 11% based on 9 reviews, with an average score of 3.1 out of 10.  

Film critic Rob Gonsalves wrote, "American Psycho 2 wasnt even supposed to be an American Psycho sequel, for Christs sake! Lions Gate noticed that the first film got critical acclaim and didnt do too poorly in theaters, so they dusted off an unrelated script and modified it to link it (tenuously) to the first film."  

The film was denounced by American Psycho author Bret Easton Ellis.  In 2005, Kunis expressed embarrassment over the film, and spoke out against the idea of a sequel. 

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 