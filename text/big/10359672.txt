Glory to the Filmmaker!
{{Infobox film
| name           = Glory to the Filmmaker!
| image          = Kantokubanzaiposter.jpg
| caption        = Theatrical release poster
| director       = Takeshi Kitano Masayuki Mori
| writer         = Takeshi Kitano
| narrator       = Beat Takeshi Tōru Emori Susumu Terajima Yuki Uchida Anne Suzuki Ren Osugi
| music          = Shinichirô Ikebe
| cinematography = Katsumi Yanagishima
| editing        = Takeshi Kitano
| studio         = Bandai Visual Tokyo FM Dentsu TV Asahi
| distributor    = Tokyo Theatres Office Kitano
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}} surrealist autobiography|autobiographical Achilles and the Tortoise.

==Style== Japanese film, Kitano described the film as "a cinematic extension of   manzai comedy routines that continues in much the same vein as Takeshis|  last feature." 

==Plot==
Kitano plays a hapless film director in search of a commercial hit, while suffering failure after failure as he tries out different genres.

== Cast ==
*Takeshi Kitano
*Tōru Emori
*Kayoko Kishimoto
*Anne Suzuki
*Keiko Matsuzaka
*Yoshino Kimura
*Kazuko Yoshiyuki
*Yuki Uchida
*Akira Takarada
*Yumiko Fujita
*Ren Osugi
*Susumu Terajima
*Naomasa Musaka

==Reception==
In 2007, the Venice Film Festival introduced a new award named after the film, Kitano was also the first recipient of the Glory to the Filmmaker! award. 

==References==
 

==External links==
*  
*  
*   
* 

 

 
 
 
 
 


 
 