Uncle Silas (film)
{{Infobox film
| name           = Uncle Silas
| image          = Uncle silas film poster english.jpg  
| caption        = English film poster
| director       = Charles Frank
| producer       = Josef Somlo, Laurence Irving
| writer         = Ben Travers, from the novel by Sheridan le Fanu
| starring       = Jean Simmons Derrick de Marney Katina Paxinou
| music          = Alan Rawsthorne, played by the London Symphony Orchestra, conducted by Muir Matheson
| cinematography = Robert Krasker
| editing        = 
| distributor    = 
| released       = 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 British drama heiress is pursued by her uncle, who craves her money following her fathers death.

==Plot==
Caroline Ruthyn is the teenage niece of the her elderly uncle Silas, a sickly and at one time unbalanced man who becomes her guardian on the death of her father. The fact that Silas is broke and greedy and young Caroline is the heir to her fathers vast fortune is reason enough for Caroline to be wary, but her fears increase when she meets Silass perverted son and when she discovers that her fearsome former governess, Madame de la Rougierre, is working with her uncle...

==Cast==
* Jean Simmons - Caroline Ruthyn
* Katina Paxinou - Madame de la Rougierre
* Derrick De Marney - Uncle Silas
* Derek Bond - Lord Richard Ilbury
* Sophie Stewart - Lady Monica Waring
* Esmond Knight - Doctor Bryerly
* Reginald Tate - Austin Ruthyn
* Manning Whiley - Dudley Ruthyn
* Marjorie Rhodes - Mrs Rusk
* John Laurie - Giles
* Frederick Burtwell - Branston George Curzon - Sleigh
* O. B. Clarence - Victor Clay
* Frederick Ranalow - Rigg
* Patricia Glyn - Mary Quince
* Robin Netscher - Tom

==References==
 

==External links==
* 

 
 
 
 
 
 


 