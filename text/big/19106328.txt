Beyond the Curtain
 
{{Infobox film
| name           = Beyond the Curtain
| alt            =  
| image	=	Beyond the Curtain FilmPoster.jpeg
| caption        = 
| director       = Compton Bennett
| producer       = John Martin
| writer         = Compton Bennett Charles F. Blair John Cresswell
| starring       = Richard Greene Eva Bartok Marius Goring Lucie Mannheim
| music          = Kenneth Pakeman
| cinematography = Eric Cross
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Beyond the Curtain is a 1960 British drama film written and directed by Compton Bennett and starring Richard Greene and Eva Bartok. 

==Plot==
An East German refugee finds herself trapped back in her old home city of Dresden when a flight she is on is forced down while flying between Berlin and West Germany. She is used by the Stasi, who want her to help them locate her dissident brother.

==Cast==
* Richard Greene as Captain Jim Kyle  
* Eva Bartok as Karin von Seefeldt  
* Marius Goring as Hans Körtner  
* Lucie Mannheim as Frau von Seefeldt  
* Andree Melly as Linda  
* George Mikell as Pieter von Seefeldt   John Welsh as Turner  
* Denis Shaw as Krumm  
* Annette Carell as Governor  
* Gaylord Cavallaro as Twining  
* Leonard Sachs as Waiter  
* Brian Wilde as Bill Seddon  
* Steve Plytas as Zimmerman  
* Guy Kingsley Poynter as Captain Law
* André Mikhelson as Russian Colonel

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 