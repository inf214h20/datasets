They Came for Good
{{Infobox Film
| name           = They Came for Good
| image          = T10544v44j8-2-.jpg 
| image_size     = 175px
| caption        = 
| director       = Amram Nowak
| producer       = 
| writer         = Manya Starr
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 1997 
| runtime        = min.
| country        =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary by Oscar nominated director Amram Nowak that explores the challenges and contributions of Jews during Americas founding history. The film brings to life a series of personalities, including Asher Levy, Louis Moses Gomez, Rebecca Gratz, Uriah Phillips Levy, Levi Strauss, Isaac Leeser, Isaac Mayer Wise, Judah Benjamin, the Warburgs, the Jacob Schiff|Schiffs, and Emma Lazarus.

==Summary==
They Came for Good is a two part documentary series of critical acclaim.
 America came to the New World to practice their religion openly, and once they arrived they began to shape the budding nation.
 Orthodox and Reform movements to the Ashkenazi entrepreneurs who crossed the Atlantic Ocean and Midwestern prairie to create new American identities.

In 1820, most took a subdued approach to demonstrating their Jewishness. They were a shadow of a population, numbering less than 3,000 nationwide — a minority in a nation of minorities. In the years that followed, however, a massive influx of immigrant Ashkenazi Jewry from Europe arrived to cause a spiritual and economic sea change. The film covers American Jews entrepreneurial spirit from the mostly-Jewish peddlers selling goods on the back roads of the South to those who run the general stores that will one day grow into nationwide chains like Macy’s, Sears, Gimbels, Sterns and Filenes, Jews were major purveyors of commercial good in this country and, thus, major contributors to popular culture.

==Filmmaker==
Director Amram Nowak was nominated for an Oscar for his film Isaac in America. He has also worked as a writer and producer.

==See also==
*History of the Jews in the United States
*Jewish history in Colonial America

Other documentaries about American-Jewish history:
* 
*A Home on the Range
*From Swastika to Jim Crow

==References==
* 
* 

==External links==
* 
* 
*  

 
 
 
 
 
 