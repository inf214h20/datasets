Prince Yeonsan (film)
{{Infobox film
| name           = Prince Yeonsan
| image          = Prince Yeonsan.jpg
| caption        = Theatrical poster for Prince Yeonsan (1961)
| director       = Shin Sang-ok 
| producer       = Shin Sang-ok
| writer         = Lim Hee-jae
| starring       = Shin Yeong-gyun
| music          = Jeong Yoon-joo
| cinematography = Bea Sung-hak Jeong Hae-jun
| editing        = Kim Young-hee Yang Seong-ran
| distributor    = Shin Films
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Yeonsan gun
 | mr             = Yŏnsan kun
 | context        = }}
}} 1961 South Korean film directed by Shin Sang-ok.  Among several awards including Best Actor and Best Actress, it was chosen as Best Film at the first Grand Bell Awards ceremony.     

==Synopsis==
A historical drama about Yeonsangun of Joseon as a prince trying to restore the status of his mother, the deposed and Queen Yun. 

==Cast==
* Shin Young-kyun: Prince Yeonsan 
* Shin Seong-il
* Kim Dong-won: Seongjong
* Ju Jeung-ryu: Yun Pyebi, the desposed queen
* Han Eun-jin: Mother of Yun
* Heo Jang-kang Kim Jin-kyu
* Do Kum-bong: Jang Nok-su
* Jeon Ok
* Choi Nam-hyeon
* Kim Hie-gab
* Lee Ye-chun
* Nam Kung-won
* Hwang Jeong-su
* Lee Min-ja

==Contemporary reviews==
* November 22, 1961. " ", Kyunghyang Sinmun 
* December 3, 1961. "촬영소식/「연산군」전편" 신정에 개봉/상영 5시간의 장척물  신문 Hankook Ilbo
* December 3, 1961. "  원커트/ 발랄한 용자 / 엄앵란양", Hankook Ilbo
* December 6, 1961. "  한숨에 내달린 출세가도", The Dong-a Ilbo
* December 30, 1961. "국산대작이 볼만 / -신협-제작영화도 이색 / 정초 시내 개봉관 -프로-", Kyunghyang Sinmun

==Notes==
 

==Bibliography==
*  
*  
*  
  
  
  
 
 
 
 

 
 
 
 
 
 


 