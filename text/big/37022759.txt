Freighters of Destiny
{{Infobox film
| name           = Freighters of Destiny
| image          = FreightersofDestiny.1931.jpg
| caption        = Promotional poster for the film Fred Allen   
| producer       = Fred Allen 
| writer         = Adele Buffington 
| based on       = Tom Keene 
| music          = Arthur Lange Ted McCord  William Clemens  RKO Radio Pictures 
| released       =  
| runtime        = 60 minutes 
| country        = United States
| language       = English
| budget         = $39,000 
| gross          =
}} Western produced Fred Allen, Tom Keene, a well known actor of B movie|B-films. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p47 

==Plot==
John Macy runs a wagon train caravan which supplies a remote settlement. His son, Steve, helps him in running the caravan, as well as dating one of the local girls in the town, Ruth. Randolph Carter is a local banker who covets the commerce created by the wagon train. Carter hires gunmen to attack the caravan, who in a raid, kill Macy, and take possession of the wagons and their supplies. Steve, who was not with the caravan at the time of the attack, takes over running the caravan. Due to the losses inflicted by the gunmen, Steve is forced to go to the local bank for a loan, in order to keep the operation running. However, Carter is the banker and refuses to provide him with the necessary funds.  Two of Steves friends, Rough and Ready, go to the bank in an attempt to convince Carter to change his mind.  When the opportunity arises, the two friends help themselves to the needed funds from the banks safe.

Steve takes the money, not knowing how it was obtained. When Carter accuses Steve of stealing the money, he is forced to flee.  When he and his two friends take refuge in a cabin in the mountains, they uncover the true gunmen who have been plaguing the caravan, who are plotting to attack the next caravan, as well as disclosing where they have hidden the wagons they have stolen previously. As Rough and Ready head out to recover the stolen wagons, Steve heads into the town to expose Carter.  However, Carter has already convinced the town leaders that Steve is not to be trusted, and that he should be given the contract to run the caravan.

Steve, hiding from the townspeople, now that Carter has turned them against him, manages to convince Ruth of his innocence.  He escapes from the town and joins Rough and Ready, who are bringing the stolen caravan and the much needed supplies into town. The caravan is once again attacked by Carters henchmen, but this time they are ready, and defeat the gunmen.  The caravan arrives in town, at which point Steve exposes Carter as the mastermind behind the attacks.  Steve remains in control of the caravan, and wins the hand of Ruth.

==Cast== Tom Keene as Steve Macey
* Barbara Kent as Ruth
* Mitchell Harris as Randolph Carter Frederick Burton as John Macey Frank Rice as Rough
* Billy Franey as Ready William Welsh as Mercer Fred Burns as Sheriff Charles Whittaker as Toller
* Tom Bay as Heavy

==References==
 

==External links==
*  at IMDB

 
 