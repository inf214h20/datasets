Vaanchinathan
{{Infobox film
| name           = Vaanchinathan
| image          =
| caption        =
| director       = Shaji Kailas
| writer         = Liyakath Ali Khan
| starring       = Vijayakanth, Sakshi Shivanand Ramya Krishnan Prakash Raj
| producer       =
| cinematography =
| music          = Karthik Raja
| editor         =
| released       =  
| runtime        = 
| language       = Tamil
| country        = India
}} Tamil action film directed by Shaji Kailas in his Tamil debut. The script was written by Liyakath Ali Khan. It stars Vijayakanth, Sakshi Shivanand Ramya Krishnan and Prakash Raj in pivotal roles.  The film received mixed reviews.The character of Prakash raj is heavily inspired from the bond villain Elliot carver played by Jonathan Pryce in Tomorrow never dies.   

==Plot==
Vanchinathan(Vijayakanth) is naturally the supercop who has been transferred from Gujarat. He is someone who is not averse to using the law in his own ways by killing someone and picking a suitable alibi, as long as the person is evil and will not be punished by the law. Chidambaram(Prakashraj) is a newspaper magnate who thrives on chaos and confusion which will help him boost sales of his paper. Their enmity becomes personal when Chidambaram challenges Vanchinathan to arrest him when he cleverly commits a murder in broad daylight in front of Vanchis own eyes.

==Cast==
*Vijayakanth as Vanchinathan
*Ramya Krishnan
*Sakshi Shivanand
*Prakash Raj
*Anju Aravind
*Nassar Raj Kapoor
*Kalabhavan Mani
*Pyramid Natarajan
*Delhi Ganesh

==Production==
Vijaykanth watched shaji kalidas aaram thampuram and liked it very much and asked director to remake it in tamil.But however it was not made do to busy schedules of Vijaykanth and shaji.however in 2000 shaji decided to make film in tamil but he felt that aaram thampuram script became old and he wrote new script keeping Vijaykanth in mind which became vanchinathan. 

The team had initially agreed terms with Suresh Gopi to feature in a pivotal role, but his unavailability led to team casting Prakash Raj.  Shilpa Shetty had also signed on to star in the film, but later opted out due to other commitments.  A fight scene involving Vijayakanth, Ramya Krishnan and rowdies was shot in a set erected at AVM studios.  The song sequences was shot at New Zealand. 

==Release==
Distributors who have bought this film had incurred heavy losses.  Post release it was rumoured that footage of two heroines have been deleted to reduce the timing which earned criticism.  

==Critical reception==
*Rediff wrote"But the masala is appetising and in the final analysis, that is all that counts". 
*Lolluexpress claimed that film is a "collection of scenes from actors previous movies". 

==Soundtrack==
Music is composed by Karthik Raja collaborated with Vijayakanth for second time after Alexander (2004 film)|Alexander. 
*Muthamida Vendum
*Sirikkum Siripile
*Amul Baby

==Notes and references==
 

==External links==
*  

 
 
 
 
 
 
 
 
 