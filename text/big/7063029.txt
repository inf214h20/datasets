A Plumbing We Will Go
{{Infobox film
  | name           = A Plumbing We Will Go 
  | image          = Plumbinglobbycard.jpg
  | caption        = 
  | director       = Del Lord
  | writer         = Elwood Ullman John Tyrrell Bud Jamison Monte Collins Eddie Laughton
  | cinematography = Benjamin H. Kline
  | editing        = Art Seid
  | producer       = Del Lord Hugh McCollum
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        =  17 31" |
  | country        = United States
  | language       = English
}}

A Plumbing We Will Go is the 46th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
After being found innocent on a charge of chicken stealing, the Stooges attempt to catch a live fish from a pet store aquarium tank. A beat cop (Bud Jamison) catches them in action and gives chase, forcing the boys to pose as plumbers to avoid being incarcerated. 
 
The Stooges manage to destroy the entire plumbing system in the home in grand fashion. Curly attempts to repair a leak in the upstairs bathroom and ends up constructing a maze of pipes that traps him. Larry digs up the front lawn in search of the water shutoff valve. In addition, Moe and Curly end up connecting a water pipe with another nearby pipe housing electrical wires, leading to water exiting every electrical appliance in the mansion. When a hostess invites her guests to watch Niagara Falls on her new TV set, the whole company gets doused with water.

The homeowner arrives to see his house in shambles and accidentally undoes the Stooges convoluted repair work. As they are about to reprimand him, it becomes clear that the homeowner happens to be the judge who found them innocent a few hours earlier. In a typical Stooge ending, the Stooges run for their lives-followed by the Judge and a whole squad of motorcycle policemen.

A side-gag for this short involves the chef in the kitchen played by Dudley Dickerson. He first battles an out of control sink faucet (thanks to Moe), then watches later as both the clock and ceiling light act up.  After the Stooges "fix" the plumbing, water breaks the ceiling light, then gushes from the stove.  The chef battles the stove twice before giving up and retreating from the kitchen.

==Production notes==
A Plumbing We Will Go was filmed on December 13-18, 1939.    It was a remake of Sidney & Murrays  1934, short Plumbing for Gold and would be remade again with El Brendel and Shemp Howard as Pick a Peck of Plumbers in 1944. The Stooges remade A Plumbing We Will Go as Vagabond Loafers and Scheming Schemers using stock footage. Interestingly, the original story in Plumbing for Gold involved searching for a lost ring which the Stooges did not use until Scheming Schemers. 

Curly would recreate the maze-of-pipes gag six year later in Swing Parade of 1946. Shemp attempted it as well Vagabond Loafers and Scheming Schemers, while Joe DeRita also attempted the gag in Have Rocket, Will Travel.  The chicken-stealing segment that opens the film was also reworked in Listen, Judge. 

Aside from the aforementioned reworked films, footage from A Plumbing We Will Go also reappeared in the 1960 compilation feature film Stop! Look! and Laugh!.   

Like A Ducking They Did Go, the title is a play on the childrens song, "A-Hunting We Will Go". 
 DuMont model that would have cost her $445. Using the US Bureau of Labor Statistics formula, adjusting for inflation, a consumer would have to pay almost $7,600 today for the same set.

==Reception==
A Plumbing We Will Go is considered a quintessential Three Stooges film. Ranking as a consistent fan favorite, the film was also a favorite of star Curly Howard. 

As of 2010, A Plumbing We Will Go is the highest-rated Three Stooges film on the Internet Movie Database. 

==See also==
*Three Stooges Filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 