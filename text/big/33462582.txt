The British Guide to Showing Off
 

{{Infobox film name = The British Guide to Showing Off image = TheBritishGuideToShowingOff FilmPoster.jpg caption = Theatrical Poster director = Jes Benstock producer = Dorigen Hammond starring = Andrew Logan Brian Eno Zandra Rhodes Richard OBrian Julian Clary Ruby Wax Derek Jarman music = Mike "Flowers" Roberts studio = Living Cinema distributor = Verve Pictures released =   country = United Kingdom
}}

The British Guide to Showing Off is a 2011 documentary film written and directed by Jes Benstock, which follows the build-up and execution of the twelfth Alternative Miss World - artist Andrew Logans pastiche of the Miss World beauty pageant.            

==Synopsis==
As well as charting the latest incarnation of the Alternative Miss World, the film studies the history of the show itself, which first took place in 1972 and has had a number of high-profile celebrities both entering and judging it.

==Cast==
The film features contributions from Brian Eno, Ruby Wax, Zandra Rhodes, Richard OBrien, Nick Rhodes and Grayson Perry.

==Reception==
Variety (magazine)|Variety wrote that the film was "Heartfelt and humorous" in it attention to the life of "eccentric" artist Andrew Logan and his creation of the "Alternative Miss World competition". They favorably compare director Jes Benstocks documentary to other works by artist Logan, calling it a "mixed-media collage" through its blending of archival footage and photos, its inclusion of commentary from the artist and his relatives, its inclusion of input from notables from the worlds of art, fashion, music and theater, and inventive animation. 

Screen Daily, in speaking toward the films study of artist Andrew Logan and his creation of the Alternative Miss World competition in 1972, wrote that the film is an "exuberant and joyous look into the life and work." They note that the film has a "simple structure" that ties together two aspects of Logans life, his career as an alternative artist and creation of the Alternative Miss World competition, and Logans 2009 search for a venue and the subsequent design and staging of the latter event. 

The Guardian in describing the film wrote that: "The events are a very English combination of carnival, kids dressing-up parties, drag balls and PoW camp shows." and "...this playful movie is partly a biography of the gay, ever-cheerful, Oxford-educated sculptor Andrew Logan and the extravagantly staged Alternative Miss World shows hes been putting on at various London venues since 1972." 

==References==
 

== External links ==
*  
*  

 
 