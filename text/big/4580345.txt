A Matter of Time (film)
{{Infobox film
| name           = A Matter of Time
| image          = matteroftime.jpg
| caption        = Theatrical release poster
| director       = Vincente Minnelli
| producer       = Samuel Z. Arkoff Giulio Sbarigia
| writer         = John Gay
| based on = novel The Film of Memory by Maurice Druon
| starring       = Ingrid Bergman Liza Minnelli Charles Boyer Isabella Rossellini
| music          = Score: Nino Oliviero Songs: Fred Ebb John Kander George Gershwin B.G. DeSylva
| cinematography = Geoffrey Unsworth Peter Taylor
| distributor    = American International Pictures
| released       = October 7, 1976
| runtime        = 97 minutes
| country        = United States/Italy
| language       = English
| budget         = $5 million 
}}
 1976 Cinema Italian musical musical fantasy film directed by Vincente Minnelli. The screenplay by John Gay is based on the novel Film of Memory by Maurice Druon. The fictional story is based loosely on the real life exploits of the infamous Italian eccentric, the Marchesa Casati, whom Druon knew during her declining years in London while he was stationed there during World War II. The film marked the first screen appearance for Isabella Rossellini and the last for Charles Boyer, and proved to be Minnellis final project.

==Plot==
The film opens at a mid-1950s press conference, where scenes are shown for an upcoming film starring Nina (Liza Minnelli), a popular screen celebrity. While on her way to the conference, Nina looks at herself in an ornate mirror, which triggers a flashback to her arrival in Rome, when she was 19 years old. Her cousin, Valentina (Tina Aumont), has arranged for her to work as a chambermaid in a dilapidated hotel. 
 Contessa Sanziani (Ingrid Bergman), who was once the toast of Europe. The Contessa receives a visit from her husband, Count Sanziani (Charles Boyer), from whom she has been estranged for 40 years. Old quarrels are revived and Sanziani leaves the hotel, sadly telling the manager that he does not wish to be informed if anything should happen to his wife.

After having a discussion with Nina, the Contessa decides to take her under her wing and turn her into a lovely and sophisticated woman. Nina is troubled by a birthmark on her forehead, but the Contessa assures her that someday important men will be eager to press their lips to it. One evening, the Contessa summons Nina to her room and shows her a scarlet sari an Indian ambassador had once given her. 

She insists that Nina undress and places the sari on her. The Contessa then cuts Ninas long, dark hair and puts makeup on her and transforms the maid into a beautiful woman. Nina tells the Contessa she wishes she could be just like her, but the Contessa says that is a silly desire. While listening to the Contessas stories, Nina imagines herself living out the Contessas existence, triggering a series of fantasy sequences, all set in elaborate settings like casinos and Venetian palazzos. 

On a rare day off from work, Nina explores Rome and begins to sense the wonderful possibilities that may lie in store for her. That evening, while she is performing a task for the Contessa, the latter suffers a mental breakdown. The manager of the hotel, angered by the Contessas wailing, insists that she must leave the hotel within a few days.

The next morning, Nina seeks help from Mario (Spiros Andros), a frustrated screenwriter who lives in the hotel. She has brought with her some of the Contessas old stock certificates, hoping that Mario will be able to determine their worth. Mario tells her the certificates are worthless and that he feels no pity for the Contessa. Nina reacts angrily and leaves his room.

Later on, Nina goes to a bank and finds that Mario was very nearly right. Most of the certificates are, indeed, worthless, but one, from the Bank of Congo, is worth a enough to pay the Contessas hotel bill for several weeks – Italian lira|₤150,000 (about $240 USD in 1954, the year the film takes place). -1}} terms, that would be about $ .}} 

She uses part of this money to help pay the Contessas hotel expenses. That same day, Nina goes to a restaurant to pick up the Contessas dinner. A screen director, Antonio Vicari (Gabriele Ferzetti), sees Nina in the restaurant and asks Mario, who is writing a screenplay for him, to introduce him to the young woman. The introduction is made, and arrangements are made for Nina to have a screen test. 

Before she leaves for the studio, she finds that the Contessa has abruptly checked out of the hotel to find an old flame, Gabriele dOrazio (Orso Maria Guerrini). The Contessa is no longer thinking clearly; she hurries into the street and is hit by a car. She is taken, unconscious, to a Catholic charity hospital. 

Meanwhile, Nina has difficulties with her screen test, until Mario gets her to talk about the Contessa. Her subsequent show of passion impresses Vicari, who decides he wants Nina to star in his next picture. 

Nina hurries off the set, and after a search, she and Mario locate the hospital where the Contessa is under the care of Sister Pia (Isabella Rossellini, her real-life daughter, in her first film role). Nina is taken to the Contessas bedside, but the old woman has just died. Deeply saddened, Nina takes the Contessas ornate mirror as a remembrance and leaves the hospital.

The film jumps forward to the present time. Nina has become a motion picture star. She arrives at the press conference. As she steps out of her limousine, a girl hurries up and says she wants to be just like Nina when she grows up.

==Production notes==
The novel had been adapted for the stage by Paul Osborne as Contessa in 1965. Minnelli read the book in 1966 but only obtained the film rights in 1973. He raised the funds via Jack Skirball, a sometime producer. Eventually American International Pictures agreed to co finance with Italian producer Giulio Sbarigia. Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 203-206 

Shooting began in February 1975 in Rome and Venice and was meant to take fourteen weeks. However the movie ended up going behind schedule. Minnellis cut of the movie went for over three hours. 

Cost-conscious American International Pictures executives, dismayed by filming delays and rising expenses, wrested control of the film from Vincente Minnelli. A Stand on A Matter of Time
Kilday, Gregg. Los Angeles Times (1923-Current File)   16 Oct 1976: b7.  Liza Minnellis husband Jack Haley Jr re-cut the film down to 97 minutes. Vincente Minnelli later disowned it, and fellow director Martin Scorsese took out ads in the trade papers chastizing AIP for its treatment of the screen legend. 

The film, released in Italy as Nina, was shot on location in Rome and Venice.

John Kander and Fred Ebb wrote "The Me I Havent Met Yet" and the title tune. Do It Again" by George Gershwin and Buddy G. DeSylva also was heard in the film, performed by Nina (Liza Minnelli) in the ballroom of a Venetian palazzo.

==Cast==
*Ingrid Bergman ..... Countess Sanziani
*Liza Minnelli ..... Nina
*Charles Boyer ..... Count Sanziani
*Isabella Rossellini ..... Sister Pia
*Tina Aumont ..... Valentina
*Fernando Rey ..... Charles Van Maar
*Spiros Andros ..... Mario
*Gabriele Ferzetti ..... Antonio Vicari
*Orso Maria Guerrini ..... Gabriele dOrazio
*Amedeo Nazzari ..... Tewfik
*Giampiero Albertini ..... Mr. DePerma
*Arnoldo Foà ..... Pavelli
*Anna Proclemer ..... Jeanne Blasto

==Critical reception==
In his review in the New York Times, Vincent Canby said, "It is full of glittery costumes and spectacular props. It is performed by talented, sophisticated people who adopt the faux-naif gestures of an earlier show-biz tradition, and though it is expensive, it sounds peculiarly tacky ... the film has the air of an operetta from which the music has been removed. Its even acted that way ... Because A Matter of Time has moments of real visual beauty, and because what the characters say to each other is mostly dumb, it may be a film to attend while wearing your earplugs."  

Roger Ebert of the Chicago Sun-Times called it "a fairly large disappointment as a movie, but as an occasion for reverie, it does very nicely. Once weve finally given up on the plot – a meandering and jumbled business – were left with the opportunity to contemplate Ingrid Bergman at 60. And to contemplate Ingrid Bergman at any age is, I submit, a passable way to spend ones time ... she possesses a radiant screen personality ... for people who love movie romance, A Matter of Time must have seemed like a dream project. And yet the movie just doesnt hold together."  

In Time (magazine)|Time, Jay Cocks stated, "It makes for an awkward occasion: a group of gifted people working so far below their best talents that everything takes on the giddy air of a runaway charade ... the movie could have worked with hard effort and a little magic, but something has gone terribly wrong. Director Minnellis once wondrous alchemy turns everything to lead. The movie is disjointed, sappy, hysterical; and the actors, perhaps sensing trouble, press on with painful, overbearing desperation ... A Matter of Time does not look at all like a Minnelli movie. The fastidious craftsmanship that he has through the years expended even on the lowliest undertaking is nowhere in evidence."  

==Note==
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 