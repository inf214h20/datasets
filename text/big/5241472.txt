Metallic Blues
Metallic Blues (in Hebrew language|Hebrew, a transliteration of the English,"מטאליק בלוז") is a 2004 film Co-production (filmmaking)|co-production (Israel/Germany/Canada) directed by Israeli director Danny Verete.

==Plot==

The film (often categorized as a tragicomedy, but the comedic elements are limited) concerns two Israeli car salesmen who initially think the world is their oyster after a rare 1985 Lincoln Continental limousine – curiously with Quebec license plates (perhaps a nod to the largely Québécois production crew, although real plates in Quebec are actually only available in the rear of a car, not the front like in the film) – falls into their laps at their used-car dealership in Tel Aviv. After reviewing a publication by the corporate German dealership Auto Decker in Düsseldorf, they are led to believe that the car could net them as much as €50,000 and sail with the vehicle to Germany in order to sell it. While learning that the task may not be as easy as hoped, they are also confronted with a number of emotional episodes. Shmuel (portrayed by Avi Kushnir) is an Ashkenazi Jew, whose parents were Holocaust survivors, and is surprised to find how affected he is by the voyage. His companion Siso (played by Moshe Ivgy) is a Mizrahi Jew of modest means who, unlike his companion, has no working knowledge of English (spoken routinely by Shmuel to the various German characters they encounter), and is therefore often overwhelmed and confused during their experience. To Siso, Shmuels history is largely unknown, and a tragic curiosity.

The dialogue between Siso and Shmuel is in Hebrew, but due to the circumstances, much of the film is also in English and German.

==Acclaim==
 San Francisco and Seattle International Film Festival|Seattle.

The film was Israels second top domestic box office hit in 2005, with approximately 50,000 viewers.

==External links==
* 
* 

 
 
 
 
 
 


 
 