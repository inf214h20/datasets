The FJ Holden
 
 
{{Infobox film name           = The FJ Holden image          = caption        = producer       = Michael Thornhill director       = Michael Thornhill writer         = Terry Larsen Michael Thornhill starring       = Paul Couzens Eva Dickinson Carl Stever Sigrid Thornton music  Jim Manzie cinematography = David Gribble editing        = Max Lemon studio         = FJ Films distributor    = Umbrella Entertainment released       =   runtime        = 105 minutes country        = Australia language       = English budget         = AU$318,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 312  gross =AU $710,000 (Australia)
}}
The FJ Holden is a 1977 Australian film directed by Michael Thornhill. The FJ Holden is a snapshot of the life of young teenage men in Bankstown, New South Wales, Australia in the 1970s and deals with the characters difficulty in reconciling mateship with respect for a girlfriend.

Debi Enker in Australian Cinema comments: "The FJ Holden presents the suburbs as a cultural and spiritual desert. It is a place where regular bouts with the bottle are the only antidote for lives without hope or direction."

The film initially received a R classification from the Australian Film Board of Review, but after an appeal to the censors it was revised to a M classification for moderate sex scenes and moderate coarse language. However, all states except Victoria and New South Wales exercised their right to override the Commonwealth decision and retained the R classification. 

==Plot== FJ Holden. Kevin meets Anne (Eva Dickinson) at a party and she agrees to let him to drive her home. Bob joins the ride, and she has sex with both men. However, a relationship develops between Anne and Kevin and they go to restaurants, race cars, and get drunk. The romance falters, partly because Kevin lets Bob watch them having sex in her bedroom. 

Drunk and upset, Kevin tries to talk to Bob, who is incapable of a serious conversation. Bob is secretly happy that he has his friend back, but neither is capable of saying what he feels.

==Cast==
*Paul Couzens as Kevin
*Eva Dickinson as Anne
*Carl Stever as Bob
*Gary Waddell as Deadlegs
*Graham Rouse as sergeant
*Karlene Rogerson as Cheryl
*Vicky Arkley as Chris
*Robert Baxter as Senior Constable
*Colin Yarwood as Brian
*Sigrid Thornton as Wendy
*Ray Marshall as Mr Sullivan
*Maggie Kirkpatrick as Betty Amstead
*Harry Lawrence as security guard

==Production==
The film originated with a series of comic poems from Terry Larsen. The budget was raised from Greater Union and the Australian Film Commission. It was shot in November and December 1976 in western Sydney.  Most of the young actors were amateurs. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p89-90 

==Reception==
The FJ Holden grossed $710,000 at the box office in Australia,  which is equivalent to $3,266,000 in 2009 dollars. This was despite the fact the film was rated "R" in several states. It sold to some overseas countries and eventually recovered its cost. 

==Home Media==
The FJ Holden was released on DVD with a new print by Umbrella Entertainment in November 2005. The DVD is compatible with all region codes and includes special features such as the theatrical trailers, Australian trailers and audio commentary with Mike Thornhill moderated by Peter Galvin.   

==See also==
* Cinema of Australia

==References==
 
* 

==External links==
* 
*  at Australian Screen Online
*  at the National Film and Sound Archive
*  at Oz Movies

 

 
 
 
 
 
 