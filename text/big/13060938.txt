The Bell Hop
 
{{Infobox film
| name           = The Bell Hop
| image	         = The Bell Hop FilmPoster.jpeg
| caption        = Film poster
| director       = Larry Semon Norman Taurog
| producer       = Larry Semon Albert E. Smith
| writer         = Larry Semon Norman Taurog
| starring       = Oliver Hardy
| music          = 
| cinematography = Hans F. Koenekamp
| editing        = 
| distributor    = Vitagraph Company of America
| released       =  
| runtime        = 20 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Larry Semon - The bellhop
* Oliver Hardy - Hotel manager (as Babe Hardy) Frank Alexander - A government official
* Al Thompson
* Pete Gordon
* Norma Nichols - A maid
* William Hauber - Hotel detective

==See also==
* List of American films of 1921
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 