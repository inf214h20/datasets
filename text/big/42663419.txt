The Lady and the Hooligan
{{multiple issues|
 
 
}}

{{Infobox film
| name = The Lady and the Hooligan
| image = Baryshnya i khuligan Vladimir Mayakovsky, Alexandra Rebikova).jpg
| director = {{plainlist|
* Vladimir Mayakovsky
* Yevgeni Slavinsky
}}
| production = Neptun-Film
| writer =  Vladimir Mayakovsky
| based on =  
| starring = {{plainlist|
* Vladimir Mayakovsky
* Aleksandra Rebikova
* Fyodor Dunayev
}}
| cinematography = Yevgeni Slavinsky
| editing =
| distributor =
| released =  
| runtime = 43 minutes
| country = Russia Russian intertitles
| budget =
}}
 
The Lady and the Hooligan (original title: Барышня и хулиган ) is a 1918 Russian silent film directed by Vladimir Mayakovsky and Yevgeni Slavinsky. It is based on the play La maestrina degli operai (The Workers Young Schoolmistress) by Edmondo De Amicis.

==Plot summary==
A young schoolmistress arrives in a small village to teach reading and writing to boys and men. A hooligan sees her on the street and falls in love with her. Soon he begins to attend her classes. When one lesson is disturbed by one of the students, he beats him. The student seeks revenge with the help of his father and some of his friends. The hooligan is stabbed to death in a fight. Before dying, he asks his mother to call the schoolmistress. After the schoolmistress has kissed him on the lips, he closes his eyes and dies.

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 