Madame Tutli-Putli
{{Infobox film
| name           = Madame Tutli-Putli
| image          = Madame Tutli-Putli.PNG
| caption        = Madame Tutli-Putli reading on the train. Chris Lavis Maciek Szczerbowski
| producer       = Marcy Page
| writer         =
| starring       = Laurie Maher
| music          = Jean-Frédéric Messier Set Fire to Flames
| distributor    = National Film Board of Canada (NFB)
| released       =  
| runtime        = 17 minutes
| country        = Canada
| language       =
| budget         =
}}
Madame Tutli-Putli is a 2007   DVD as well as from the NFB.

==Plot==
Madame Tutli-Putli boards a night train for a mysterious and suspenseful journey. When train robbers cut open a mans stomach and steal his kidneys, she tries to escape.

==Research and production== composited human eyes to the stop motion puppets.    For the lead character, actress Laurie Maher was recorded acting out the motions, but only her eyes and eyebrows were ultimately visible in the final film.

==Awards==
  Oscar nomination in January 2008.

In late June 2008, Madame Tutli-Putli won the "Best of the Festival" award at the Melbourne International Animation Festival.  At the Ars Electronica Festival 2008 Chris Lavis received a Golden Nica in the category "Computer Animation/Film/VFX" of the Prix Ars Electronica.  Madame Tutli-Putli received the Academy of Canadian Cinema and Television Award for Best Animated Short at the 28th Genie Awards.  Animation Show of Shows.

=="NFB at the Oscars" contest and promotion== Best Animated Short Film, the NFB announced that it would make the film freely available for viewing online, but with a caveat—each of the 23,287 frames had to first be unlocked, one by each visitor from a unique IP address. Once the entire film was unlocked, it was made available for viewing at CBC.ca. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 