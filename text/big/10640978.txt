Jeevana Poratam
{{Infobox film
| name = Jeevana Poratam
| image =
| caption =
| director =
| writer =
| starring = Rajinikanth Shobhan BabuVijayashanti Radhika Sarathkumar
| producer = T. Subbarami Reddy P.Sashi Bhushan
| music = Chakravarthy
| editor =
| released = 10 April 1986
| runtime = Telugu
| budget =
}}

Jeevana Poratam is a 1986 Telugu film is directed by Rajachandra. It starrs Shobhan Babu and Rajinikanth in the lead roles as brothers.

==Cast==
* Rajinikanth
* Shobhan Babu
* Radhika Sarathkumar
* Vijayashanti
* Sarath Babu Naresh
* Kaikala Satyanarayana
* P. L. Narayana Velu
* Prasad Babu
* Urvashi
* Pushpalatha
* Devi
* Srilakshmi

==Synopsis==
Jeevana Poratam is the story about the mismanagement and selfishness of the leaders of India. Many people had striven for the independence of the country, but when we got the independence then the scenario is totally opposite.Bharath (Shoban Babu) has become the victim. In spite of being a gold medalist he had to suffer unemployment. His father gummadi always scolds him for this. He has 2 brothers rajni and naresh and a sister. Rajinikanth joins hands with bad people and knowing this shoban babu scolds him and Rajinikanth disappears and comes in the end after joining the army (he loses is hand in war). Vijayasanthi and shohan are related and love each other..Meanwhile vijayasanthi gets a job in Sarat babus office and sarat babu loves her. Vijaysanthi slowly falls for Sarat Babu seeing his wealth and neglects Shoban Babu. She even gets engaged to Sarat Babu. At this time, Radhika enters his life. Due to circumstances, Shoban Babu joins hands with Rao Gopalrao and becomes rich. Rajinikanth comes at this time and helps his brother to leave the bad people and everything ends well with Vijayasanthi losing her life in the course.

==Dubbing==
Rajinikanths dubbing was given by actor Rajendra Prasad.

==Crew==
* Lyrics: Veturi, C.Narayanareddy
* Music: Chakravarthy

 
 
 


 