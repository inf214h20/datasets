The Walking Hills
{{Infobox film
| name           = The Walking Hills
| image_size     =
| image	         = The Walking Hills FilmPoster.jpeg
| caption        =
| director       = John Sturges
| producer       = Harry Joe Brown
| writer         = Alan Le May
| narrator       =
| starring       = Randolph Scott Ella Raines Arthur Morton
| cinematography = Charles Lawton Jr. William Lyon
| distributor    = Columbia Pictures (USA)
| released       =  
| runtime        = 78 mins.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}} 1949 western film directed by John Sturges.

==Plot==
One day in Mexicali, a poker game in a saloon includes horse breeder Jim Carey, rodeo rider Shep, a prospector called Old Willy, stranger in town Frazee and a couple of cowboys, Chalk and Johnny. A conversation begins about a wagon load of gold lost 100 years ago in the Walking Hills, and how one of them recently spotted an old abandoned wagon there.

The men set out together to search for it. They are joined by a woman, Chris Jackson, who once broke off an engagement to Jim to marry Shep, who then jilted her.

It turns out Shep fled because he had been accused of killing a man accused of cheating at cards. The mans father, King, hired a detective who turns out to be Frazee, and who is now sending signals to King along the trail.

Tempers flare and Johnny dies after a fight. Chalk stampedes the horses and is shot by Jim in self-defense. A terrible sand storm develops, resulting in Frazees death and the uncovering of the wagon. Old Willy finds it, but its empty.

Shep decides to turn himself in to the law, and Chris rides off with him. Jim has a hunch, meanwhile, that the wagon wasnt empty when Old Willy found it. He is right.

==Cast==
* Randolph Scott as Jim
* Ella Raines as Chris  William Bishop as Shep
* Edgar Buchanan as Old Willy Arthur Kennedy as Chalk John Ireland as Frazee
* Jerome Courtland as Johnny
* Russell Collins as Bibbs
* Josh White as Josh
* Houseley Stevenson as King
* Reed Howes as Young King

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 