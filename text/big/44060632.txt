Snehikkan Oru Pennu
{{Infobox film
| name           = Snehikkaan Oru Pennu
| image          =
| caption        =
| director       = N. Sukumaran Nair
| producer       =
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi
| starring       = Thikkurissi Sukumaran Nair Sankaradi Sreelatha Namboothiri Vasanthi
| music          = G. Devarajan
| cinematography = MC Sekhar
| editing        = MS Mani
| studio         = Suvarna Arts
| distributor    = Suvarna Arts
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed N Sukumaran Nair. The film stars Thikkurissi Sukumaran Nair, Sankaradi, Sreelatha Namboothiri and Vasanthi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Thikkurissi Sukumaran Nair
*Sankaradi
*Sreelatha Namboothiri
*Vasanthi
*Bahadoor
*MG Soman Meena
*Sangita

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aararo Thechu Minukkiya || P. Madhuri || Yusufali Kechery ||
|-
| 2 || Makaram Vannatharinjille || P. Madhuri || Yusufali Kechery ||
|-
| 3 || Ormayundo Maankidaave   || P. Madhuri || Yusufali Kechery ||
|-
| 4 || Ormayundo Maankidaave   || P Jayachandran || Yusufali Kechery ||
|-
| 5 || Poochakku Poonilavu Paalu Pole || K. J. Yesudas || Yusufali Kechery ||
|-
| 6 || Snehikkaanoru Pennundenkil || K. J. Yesudas || Yusufali Kechery ||
|}

==References==
 

==External links==
*  

 
 
 


 