Tora-san's Promise
{{Infobox film
| name = Tora-sans Promise
| image = Tora-sans Promise.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Mikiko Otonashi
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 101 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Torasan and a Paper Balloon  is a 1981 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Mikiko Otonashi as his love interest or "Madonna".  Tora-sans Promise is the twenty-eighth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
Tora-san returns to his familys home to attend an elementary school class reunion. After he embarrasses himself by getting drunk and insulting all his ex-classmates, he resumes his travels. In Kyushu he meets an outspoken 18-year-old girl who becomes enamored of Tora-san and follows him around. One of Tora-sans old friends is terminally ill and makes Tora-san promise him to marry his wife once he is gone.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Mikiko Otonashi as Mitsue Kuratomi
* Kayako Kishimoto as Aiko Odajima
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)

==Critical appraisal==
Stuart Galbraith IV writes that Tora-sans Promise is a middle-quality entry in the Otoko wa Tsurai yo series, though the series has a high standard.  The German-language site molodezhnaja gives Tora-sans Promise three and a half out of five stars.   

==Availability==
Tora-sans Promise was released theatrically on December 28, 1981.  In Japan, the film has been released on videotape in 1986 and 1996, and in DVD format in 2002 and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 