Lipstick (1960 film)
{{Infobox film
 | name =Lipstick
 | image = Lipstick (1961 film).jpg
 | caption =
 | director = Damiano Damiani
 | writer = 
 | starring =   
 | music =  Giovanni Fusco
 | cinematography =   Pier Ludovico Pavoni
 | editing =  Fernando Cerchio
 | producer =
 | distributor =
 | released =1960
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 1960 Cinema Italian Crime film|crime-drama film directed by Damiano Damiani. It is the feature film debut of Damiani, after two documentaries and several screenplays.    The films plot was loosely inspired by actual events.  Pietro Germi reprised, with very slight modifications, the character he played in Un maledetto imbroglio. 

== Cast ==

*Pierre Brice: Gino
*Georgia Moll: Lorella
*Pietro Germi: Commissario Fioresi
*Laura Vivaldi: Silvana
*Bella Darvi: Nora 
*Ivano Staccioli: Mauri
*Renato Mambor: Vincenzo
*Erna Schürer: Cinzia

==References==
 

==External links==
* 

 

 
  
 
 
 
 

 