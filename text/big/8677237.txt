Old Men in New Cars
{{Infobox film
| name = Old Men in New Cars
| image = Oldmeninnewcars.jpg
| caption = DVD front cover
| director = Lasse Spang Olsen
| producer = Michel Schønnemann
| writer = Anders Thomas Jensen
| starring = Kim Bodnia   Nikolaj Lie Kaas   Tomas Villum Jensen
| music = George Keller
| editing = Mikkel E.G. Nielsen
| distributor = Sandrew Metronome TLA Releasing
| released =  
| runtime = 95 min
| country = Denmark Danish
}}
Old Men in New Cars ( ), (2002) is a Danish action comedy film directed by Lasse Spang Olsen. The movie stars Kim Bodnia, Nikolaj Lie Kaas, and Tomas Villum Jensen. It is a prequel to Olsens 1999 comedy In China They Eat Dogs. 

==Synopsis==
The last wish of the dying "Monk" is for his foster child, Harald, to find his real son, Ludvig. But Ludvig is currently in a Swedish prison cell. Peter and Martin – the two chefs – help to get him out and soon father and son meet for the first time in their lives. They get on from the word go, but now dad needs a liver transplant in Ecuador and Ludvig and Harald set about raising the wherewithal. Everything goes wrong when they try to rob a bank, though they meet Mille, who puts them onto a new trail, and Peter and Martin also make a contribution. However, soon they have the cops and the anti-terror corps on their tails.

==Cast==
* Kim Bodnia ... Harald
* Nikolaj Lie Kaas ... Martin
* Tomas Villum Jensen ... Peter
* Brian Patterson ... Vuk
* Torkel Petersson ... Ludvig
* Iben Hjejle ... Mille
* Jens Okking ... Munken
* Jacob Haugaard ... Erling
* Slavko Labovic ... Ratko
* Thomas Rode Andersen ... Dan Hansen

==References==
 

==External links==
* 

 
 
 
 
 


 
 