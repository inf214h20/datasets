Riddick (film)
 
 
{{Infobox film
| name             = Riddick
| image            = Riddick poster.jpg
| alt                 =  
| caption          = Theatrical release poster
| director         = David Twohy
| producer       = {{Plain list |
*Vin Diesel
*Ted Field
*Samantha Vincent
}}
| screenplay     = David Twohy
| based on       =  
| starring       = {{Plain list |
*Vin Diesel
*Jordi Mollà
*Matt Nable
*Katee Sackhoff
*Dave Bautista
*Bokeem Woodbine
*Raoul Trujillo
*Karl Urban
 
}}
| music          = Graeme Revell
| cinematography = David Eggby
| editing        = Tracy Adams
| studio         = {{Plain list |
*Radar Pictures
*One Race Films
}}
| distributor    = {{Plain list | Universal Pictures
*Entertainment One  
}}
| released       =  
| runtime        = 119 minutes  
| country        = {{Plain list |
*United Kingdom
*United States
}}
| language       = English
| budget         = $38 million 
| gross          = $98.3 million   
}} science fiction thriller film, title character, Pitch Black (2000) and The Chronicles of Riddick (2004).
 IMAX Digital theaters.

==Plot==
  and buries Riddick alive.

Riddick emerges from the rubble with a broken leg, which he sets and splints while fending off native predators: vulture-like flying animals, packs of jackal-like beasts and swarms of venomous, scorpion-like water dwelling creatures called Mud Demons. Needing time to heal, Riddick hides himself within some abandoned ruins. After hes fully healed, Riddick notices a vast savanna beyond some rocky cliffs, but the only passage through is impeded by several muddy pools infested with Mud Demons. He begins injecting himself with Mud Demon venom in order to build an immunity and constructs improvised melee weapons, as well as raises and trains an orphaned jackal-beast pup. The two eventually defeat the dominant Mud Demon and reach the savannah. Riddick notices a series of approaching storms, and concludes they will unleash countless more of the Demons, who must keep their skin wet at all times to survive. Needing to get off-world, Riddick activates an emergency beacon in an abandoned mercenary station, which broadcasts his identity to mercenary groups within the area.

Two ships promptly arrive in answer to the beacon, the first a group of bushwhacker bounty hunters led by a violent and unstable man named Santana, and the second a better-equipped team of professional mercenaries led by a man named Johns. Riddick leaves them a blood message promising they will all die unless they leave one of their ships and depart the planet on the other. Rubio, Nunez and Falco are killed during the first night, forcing a reluctant Santana to cooperate with Johns. Riddick later manages to steal power nodes from each of the teams ships and then approaches Johns and Santana to strike a deal for their return. However, the conversation turns into an ambush. Johns second-in-command, Dahl, shoots Riddick with several rounds of horse tranquilizer, and Riddicks jackal-beast is shot and killed by Santana.
 Pitch Black). When the storms finally reach the station, large numbers of Mud Demons emerge from the muddy ground, and besiege the station, killing Lockspur and Moss. Johns agrees to release Riddick in order to locate the hidden power cells. Santana stops him and attempts to kill Riddick, who is worth twice as much dead as he is alive. Riddick, with only one leg free, beheads Santana with his own sword and the group releases Riddicks chains.

They then fight their way to the ship which houses the hover bikes with Vargas being killed. Johns, Santanas man Diaz, and Riddick leave the ship together on hover bikes on a mission to retrieve the power nodes. During their journey, Diaz knocks Johns bike over the side, causing him to crash. He is then picked up by Riddick. After they reach the power nodes, Riddick reveals to Johns about his sons addiction to morphine and a spineless attempt by his son to utilize a child as bait for the hostile animals on the world they were stranded on twelve years prior. With both of them distracted, Diaz attempts to kill Riddick and Johns. Riddick fights and kills him, but not before unintentionally damaging the only working hover bike (Diaz had already disabled the other one).

Riddick and Boss Johns fend off a seemingly endless horde of Demons while running back to the station. Riddick is severely wounded. Johns takes both nodes and abandons Riddick. After treating his wound, Riddick begins to fight a futile battle against the advancing Demons. Just when it seems he is about to be killed, Johns arrives in one of the ships and shoots the creatures while Dahl descends to rescue Riddick. Riddick then takes the other ship and—as he flies away from the planet—he is stopped by Johns in the other ship. Johns asks Riddick where he will go, but thinks better and decides to not know. Riddick praises Johns for being a better man than his son and departs into deep space. In the theatrical version, this is the end of the film, while in the extended version, there is an additional scene which takes place aboard the Necromonger ship.      

===Differences in theatrical and extended versions===
A number of additions to the plot are included in the directors cut of the movie which was released on Blu-ray and DVD on January 14, 2014  and provided a longer story than the theatrical version.     The extended cut of the movie is 488.8 sec. (or approx. 8 minutes 9 seconds) longer  than the theatrical version and includes extended and additional scenes not seen in the theatrical version.

In the new ending to the movie, Krone, who had previously left Riddick for dead on the planet, is seen kneeling before the window and praying to the Underverse. Through the viewport, the entire fleet is seen gravitating toward a massive portal in space, presumably the "threshold" to the Underverse. Riddick arrives and threatens Krone, demanding to know where Vaako is. Krone begins by telling him that Vaako is "no longer among us" and continues on by saying that Vaako did indeed plan to "honor his word" and bring Riddick to Furya, but that Krone had interfered with Vaakos plan and made a choice to abandon Riddick on the other planet. As Krone condemns Riddick for being a "misbeliever" and unfit to lead the Necromongers, Riddick thrusts his blade into the back of Krones neck, killing him and coldly muttering, "Too many words." Riddick then turns to a female slave and asks her if Vaako is alive or dead, to which she replies "both". Riddick appears somewhat puzzled at this response and approaches the viewport, gazing at the massive portal outside while Vaakos voice echoes the word "transcendence".

==Cast==
* Vin Diesel as Riddick
* Matthew Nable as Colonel R. "Boss" Johns 
* Jordi Mollà as Santana
* Katee Sackhoff as Dahl
* Dave Bautista as Diaz
* Bokeem Woodbine as Moss
* Raoul Trujillo as Lockspur 
* Conrad Pla as Vargas
* Nolan Gerard Funk as Luna
* Danny Blanco Hall as Falco
* Noah Danby as Nuñez
* Neil Napier as Rubio
* Karl Urban as Siberius Vaako
* Alex Branson as Lex Branman
* Andreas Apergis as Krone
* Keri Hilson as Santanas Prisoner

==Production==

===Development===
Rumors of a third film in the Chronicles of Riddick series had circulated since 2006.  At first, Twohy assumed that the film would be an   in exchange for the ownership to the rights to the Riddick franchise and character.  Over the next four years, Diesel periodically posted information on his own Facebook page updating fans on the films progress. In November 2009, shortly after Twohy had finished the script, he announced that pre-production work was underway. 
 Pitch Black), a priority for them, and they plan to shoot it lean and quickly. 

In September 2011, it was announced that Karl Urban would reprise his role as Vaako from The Chronicles of Riddick.  In January 2012, it was announced that Katee Sackhoff and Matt Nable had also joined the cast.  Since they did not have enough money to shoot the film in its entirety, Diesel had to mortgage his house, obtain loans and spend most of his personal money on the production of the film, "I had to leverage my house," Diesel said. "If we didnt finish the film, I would be homeless." 

===Principal photography===
The first image of Vin Diesel on the films set was released on January 20, 2012.  Filming began in January 2012 and concluded at the end of March 2012.    The film entered post production in April 2012.  The teaser trailer for the film was released on March 22, 2013 via Vin Diesels official Facebook page. 

==Release==

===Critical response=== normalized rating out of 100 top reviews from mainstream critics, calculated a score of 49 out of 100 based on 33 reviews. 

Alonso Duralde of   gave the film a positive review, saying "An improbable but very enjoyable sequel that recaptures much of the stripped-down intensity of Diesel and director David Twohys franchise starter Pitch Black."  Justin Lowe of The Hollywood Reporter gave the film a negative review, saying "Faithful to the template if not the spirit of previous installments, this flabby second sequel barely manages to advance Riddicks considerable personal mythology."  Keith Staskiewicz of Entertainment Weekly gave the film a C+, saying "Twohy succeeds in staging moments both tense and funny, but theyre fewer and farther between than one would hope, and the dialogue is served up with a heaping helping of cheese, especially when delivered in Diesels low-frequency growl." 

Jocelyn Noveck of the Associated Press gave the film one and a half stars out of four, saying "Sloppy is a kind word, actually, for the ridiculously clumsy dialogue in Riddick, the third and latest installment of the sci-fi saga."  Keith Uhlich of Time Out New York gave the film three out of five stars, saying "Watching this see-in-the-dark muscleman brooding against gorgeous otherworldly vistas, all while crafting pointy homemade weapons and befriending a scene-stealing CGI canine (no joke), is a sci-fi aficionados delight."  Bruce Demara of the Toronto Star gave the film three out of four stars, saying "The CGI-created landscape is impressively rendered and detailed, there are loads of cool gadgetry and, of course, plenty of action and "ghosting"—i.e. killing."  Michael Phillips of the Chicago Tribune gave the film three out of four stars, saying "This is not one of those Johnny-come-lately sequels preoccupied with getting a new audience up to speed on where the story was. Its about living in the moment, in the now, and killing in the now."  Soren Anderson of The Seattle Times gave the film two out of four stars, saying "As a creature feature, Riddick isnt half bad, though its far from truly good."  Joe Neumaier of the New York Daily News gave the film one out of five stars, saying "The story feels like quicksand. Riddick, which couldnt even qualify for proper summer movie placement, moves like Martian molasses and cant present an action scene to save its life. Youll wish you had Uncle Martins ability to speed people—not to mention awful movies—up." 

Barbara VanDenburgh of The Arizona Republic gave the film three out of five stars, saying "Riddicks at his most fun when the pressures on, and he retreats to plan something special for his new visitors. All the build-up pays off with tense showdowns in the dark, thrilling restraint and ominous suggestion giving way to slasher gore."  Rafer Guzman of Newday gave the film two and a half stars out of four, saying "Its B-grade cinema made with A-level intelligence and imagination—skillfully directed, surprisingly well acted and gratifyingly preposterous."  David Hiltbrand of The Philadelphia Inquirer gave the film two out of four stars, saying "Somehow along the way, in the films only attempt to humanize him, Riddick acquires a pet. That makes Riddick the first (and one hopes the last) film to borrow heavily from both Alien and Old Yeller."  Claudia Puig of USA Today gave the film one out of four stars, saying "Move along, theres nothing to see and no one to root for in this murky franchise reboot."  Stephanie Merry of The Washington Post gave the film two and a half stars out of four, saying "Riddick" can be cheesy and silly, not to mention excessively violent, but its also fun."  Kyle Smith of the New York Post gave the film a mixed review, saying "The movie jogs along nicely without ever getting a case of the stupids; far from being a bloated "John Carter," its just a pared-down yarn of survival: "Die Hard" on a planet." 

Stephen Whitty of the Newark Star-Ledger gave the film one and a half stars out of four, saying "The dark palette and extreme close-ups turn every fight into a muddy blur; the staging, which should feel claustrophobic, is too often set in vast (and unconvincing) landscapes."  Rene Rodriguez of The Miami Herald gave the film one out of four stars, saying "A modestly budgeted bone Universal Pictures threw at Diesel so he would keep starring in "Fast and Furious" pictures. Those movies are bank; "Riddick" is rank."  Adam Graham of Detroit News gave the film a C–, saying "Twohy is trying to do something with his original character. Its just not clear what that something is anymore. Maybe its time this franchise fades to black for good."  James Berardinelli of ReelViews gave the film three out of four stars, saying "Much of the film is over-the-top, but that wont be a surprise to those who saw the previous two installments. Diesel is in fine form, growling his lines and being the most menacing person on screen even when hes in chains."  Amy Nicholson of The Village Voice gave the film a negative review, saying "The only reason to root for Riddick is that his name is on the ticket stub. But hes so dull and the hunters so weird that were literally cheering for the movie to kill off its personality, one throat slash at a time."  Nick Schager of Slant Magazine gave the film one out of four stars, saying "For a film about a killing machine who can see at night, its fittingly ironic that the film itself is, both narratively and visually, a dark, muddled mess." 

===Box office===
Riddick grossed $42,025,135 in North America, and $56,312,160 in other countries, for a worldwide total of $98,337,295.  In North America, the film opened to number one in its first weekend, with $19,030,375.  In its second weekend, the film dropped to number three, grossing an additional $6,841,800.  In its third weekend, the film dropped to number eight, grossing $3,656,620.  In its fourth weekend, the film dropped to number 13, grossing $1,607,145. 

===Home media===
Riddick was released on DVD and Blu-ray on January 14, 2014.  The film grossed an additional $23,026,441 through domestic Blu-ray and DVD sales. 

==Sequel==
On January 29, 2014, Vin Diesel announced on his Facebook page that Universal Pictures wishes to develop a fourth Riddick film, again prompted by robust DVD sales of the most recent film in the series. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 