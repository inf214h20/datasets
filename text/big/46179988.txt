Xuxa Gêmeas
{{Infobox film
| name           = Xuxa Gêmeas
| image          = Xuxagemeas1.jpg
| caption        = 
| director       = Jorge Fernando	
| producer       = Diler Trindade
| writer         = Jorge Fernando Patricia Travasso Flávio de Souza
| starring       = Xuxa Meneghel Ivete Sangalo Murilo Rosa
| music          = Ary Sperling	
| cinematography = Edgar Moura	
| editing        =
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}} Xuxa Produções and by 20th Century Fox of Brazil. 

The plot is of two sisters separated in childhood that are reunited unexpectedly. On the one hand, Elisabeth - created by a wealthy family - only cares in to inherit his fathers (Ary Fontoura), the newspaper "O Dourado". Meanwhile, Mel struggle to keep the sponsorship of the same company, with which it maintains a school, not realizing that it is also the daughter of the owner.

== Plot ==
Two identical twins were separated when they were babies, as one of them came crawling fathers luxury car and accidentally entered the trailer of a troupe of artists. Thirty years later Elisabeth (Xuxa) is the presidents father graphic empire, while her twin sister, Mel (Xuxa), runs a school of performing arts in a poor community. Their lives intersect when Elisabeth cuts the sponsorship of the company to school of Mel, which causes it to go to the company to complain. 

== Cast ==
 
*Xuxa Meneghel as Elizabeth Dourado / Mel Monthiel (Margareth Dourado)
*Ivete Sangalo as Alice / ela mesma
*Murilo Rosa as Ivan
*Maria Clara Gueiros as Jennifer Smith McCartney da Silva
*Luís Salém as butler
*Fabiana Karla as nurse practitioner
*Eike Duarte as Byte
*Maria Mariana Azevedo as Poodle
*Thiago Martins as Tigre
*Ary Fontoura as dr. Julio César Dourado
*Leandro Hassum as Zé Mané
*John Klarner as Juquinha1
*Marcius Melhem as Manézinho
*Márcia Cabrita as Diana
*Emiliano Queiroz as Mr. Constantino
*Patrícia Travassos as receptionist
*Beto Carrero as himself
*Jorge Fernando as master of ceremonies
*Jorge de Sá as clown-mór
*Elizângela as aunt

== See also ==
* List of Brazilian films of 2006

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 