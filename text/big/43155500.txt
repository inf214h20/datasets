Marquis Preferred
{{infobox film
| title          = Marquis Preferred
| image          =
| imagesize      =
| caption        =
| director       = Frank Tuttle
| producer       = Frank Tuttle
| writer         = Frederick Arnold Kummer(story) Ernest Vajda(story) Ethel Doherty(writer)
| starring       = Adolphe Menjou
| music          =
| cinematography = Harry Fischbeck
| editing        =
| distributor    = Paramount Pictures
| released       = January 27, 1929
| runtime        = 60 minutes
| country        = USA
| language       = Silent film..(English intertitles)
}}
Marquis Preferred is a 1929 silent film comedy directed by Frank Tuttle and starring Adolphe Menjou. It was produced and distributed by Paramount Pictures. 

An extant film at the Library of Congress. Of late the film is not listed in the AFI Catalog of Features 1921-30. This was likely noticed since the film physically exists. The omission has been corrected and the film is listed on the AFIs website.  

==Cast==
*Adolphe Menjou - Marquis dArgenville
* Nora Lane - Peggy Winton
* Chester Conklin - Mr. Gruger
* Dot Farley - Mrs. Gruger (*billed Dorothy Farley)
* Lucille Powers - Gwendolyn Gruger
* Mischa Auer - Albert
* Alex Melesh - Floret
* Michael Visaroff - Jacques

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 


 