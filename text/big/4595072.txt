Shaolin Drunkard
 
 
{{Infobox film
| name           = Shaolin Drunkard
| image          =
| image_size     = 
| caption        = The Drunk Monk of Shaolin Drunkard
| director       = Yuen Woo-ping
| producer       = 
| writer         = Yuen Woo-ping Chung Hing Chiu
| narrator       = 
| starring       = Cheung-Yan Yuen Eddy Ko Shun-Yee Yuen
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1983
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Shaolin Drunkard ( ; Orig. Tian shi zhuang xie, aka Wu Tang Master, aka Miracle Fighters 2) is a 1983 Kung Fu comedy directed by Yuen Woo-ping, written by Yuen Woo-ping and Chung Hing Chiu, and starring Cheung-Yan Yuen, Eddy Ko, and Shun-Yee Yuen.

This very strange movie shows the sort of thing Yuen Woo-ping will do when he is left to his own designs and imagination. Even strange for him, this movie involves vampires, huge monster toads, and drunk monks. For some of the effects puppets were used, including a very creepy/realistic dummy version of the Drunk Monk. The fight scenes are very creative and show off Yuen Woo-pings weird sense of style and choreography.

==External links==
* 

 

 
 
 
 
 
 

 
 