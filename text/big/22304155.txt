Graveyard Shift (1990 film)
{{Infobox film
| name = Graveyard Shift
| image = Graveyard Shift film.jpg
| caption = Theatrical poster
| director = Ralph S. Singleton
| screenplay = John Esposito Short story by Stephen King David Andrews Kelly Wolf Stephen Macht Brad Dourif
| producer = William J. Dunn Ralph S. Singleton
| music = Brain Banks Anthony Marinelli Peter Stein
| editing = Jim Gross Randy Jon Morgan
| distributor = Paramount Pictures
| released =  
| runtime = 89 minutes
| country = United States
| language = English
| budget = $10.5 million
| gross = $11,582,891 
}}
 short story Night Shift.  The movie was released in October 1990.

==Plot==
When an abandoned textile mill is reopened, several employees meet mysterious deaths. The link between the killings being that they all occurred between the hours of 11 p.m. and 7 a.m.—the graveyard shift. The sadistic mill foreman (Stephen Macht) has chosen newly hired drifter John Hall (David Andrews) to help a group clean up the mills rat-infested basement. The workers find a subterranean maze of tunnels leading to the cemetery—and a bear-sized, hairless, bat-like creature that hunts at night.

==Production==
The movie was filmed in the village of Harmony, Maine at Bartlettyarns Inc., the oldest woolen yarn mill in the United States (est. 1821). The historic Bartlett mill was renamed "Bachman" for the movie, an homage to Kings pseudonym, Richard Bachman. The interior shots of the antique mill machinery, and the riverside cemetery, were in Harmony. Other scenes (restaurant interior, and giant wool picking machine) were at locations in Bangor, Maine, at an abandoned waterworks and armory. A few other mill scenes were staged near the Eastland woolen mill in Corinna, Maine, which subsequently became a Super Fund site.

==Reception==
The film received poor reviews from critics, and currently holds a 13% rating on Rotten Tomatoes.

Graveyard Shift was a modest box office success for Paramount.  The film was released October 26, 1990 in the United States, opening in first place that weekend.   It grossed a total of $11,582,891 domestically. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 