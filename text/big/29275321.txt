Hot Snow (film)
{{Infobox film
| name           =  Hot Snow
| image          = 
| alt            = 
| caption        = 
| director       = Gabriel Yegiazarov
| producer       = Adolf Fradis
| writers        = Yuri Bondarev
| screenplay = Yuri Bondarev, Evgeni Grigoriev, Gabriel Yegiazarov
| narrator       =  Yuri Nazarov, Anatoly Kuznetsov
| music          = Alfred Schnittke
| cinematography = Fiodor Dobronravov
| editing        =  
| studio         =  Mosfilm
| distributor    = 
| released       = 18 December 1972
| runtime        = 105 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} Soviet film, directed by Gabriel Yegiazarov.

==Plot== von Mansteins trying to 6th Army in Battle of Stalingrad|Stalingrad. 

==Production==
The film is an adaptation of Yuri Bondarevs eponymous 1969 novel, which was itself based on Bondarevs own wartime experience as a battery commander in Stalingrad. 

==Reception==
Hot Snow was viewed by 22.9 million people, but failed to secure any nominations or awards.  

==Select cast== Yuri Nazarov as Sergeant Ukhanov. Boris Tokarev as Lieutenant Kuznetsov.
* Anatoly Kuznetsov as Vesnin.
* Georgiy Zhzhonov as General Bessonov.

==References==
 

==External links==
*   on the IMDb.

 
 
 
 
 
 

 