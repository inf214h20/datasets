Gunasundari Katha
{{Infobox film
| name           = Gunasundari Katha
| image          = Gunasundari Katha.JPG
| image_size     =
| caption        =
| director       = Kadiri Venkata Reddy
| producer       =
| writer         = Kamalakara Kameswara Rao Pingali Nagendra Rao Kadiri Venkata Reddy William Shakespeare
| narrator       = Sriranjani Kasturi Siva Rao Vallabhajosyula Sivaram Relangi Venkata Ramaiah Govindarajula Subba Rao Shanta Kumari K. Malathi Pamarthi Venkateswara Rao Jandhya Gaurinatha Sastry T. G. Kamala Devi Hemalatha Kallakoori Sadasiva Rao T. Kanakam Vijaya Nirmala
| music          = Addepalli Rama Rao Ogirala Ramachandra Rao
| cinematography = Marcus Bartley
| editing        = C. P. Jamboolingam
| studio         = Vauhini Studios
| distributor    =
| released       = 1949
| runtime        = 172 minutes
| country        = India Telugu
| budget         =
}} Sriranjani in the title role as Gunasundari. It is loosely based on the William Shakespeares King Lear and his three daughters.  Sri Tulasi Jaya Tulasi song by P. Leela was a hit with public and sung by housewives even today.

==The plot==
King Ugrasena (Govindarajula Subba Rao) of Dhara Nagaram has 3 daughters - Rupasundari (Shantakumari), Hemasundari (Malathi) and Gunasundari (Sriranjani). Rupa and Hema are married to their cousins, Haramati (Subba Rao) and Kalamati (Relangi) respectively. Guna comments one day in a discussion that the husband is the most important entity for a girl. Her angered father, the King, then marries Guna to a blind, limp, mute and deaf pauper, Daivadeenam (Kasturi Siva Rao).

When it is discovered that Daivadeenam is in fact a prince under a curse, Guna and Daivadeenam are banished from the Kingdom. They live far away in a small hut. The King is fatally ill. Only a precious gem, the Mahendra Mani, can save him. So the three sons-in-law set out to find the gem and Daivadeenam finds it. Haramati and Kalamati steal it from him and the King is cured. Another curse hits Daivadeenam, who now turns into a bear. Pleased with Gunasundaris devotion, Lord Shiva and Parvati bless Daivadeenam and he becomes normal once again. The King, understanding the truth finally, makes Daivadeenam the King.

==Cast==
{| class="wikitable"
|-
! Actor / Actress !! Character
|-
| Govindarajula Subba Rao || King Ugrasena
|- Sriranjani || Gunasundari 
|-
| Santha Kumari || Rupasundari
|-
| Malathi || Hemasundari
|-
| Vallabhajosyula Sivaram ||
|-
| Kasturi Siva Rao  || Daivadeenam
|-
| Relangi Venkataramaiah || Kalamati
|-
| Subba Rao || Haramati
|-
| T. G. Kamala Devi ||
|}

==Songs==
There are many lyrics all of them written by Pingali Nagendra Rao. The music score is composed by Ogirala Ramachandra Rao.
* Adiye Eduraivachche Daka Pada Munduku Padipodam (Singers: Relangi and Pamarti Krishna Murthy)
* Amma Mahalakshmi Dayacheyavamma (Singer: Ghantasala Venkateswara Rao)
* Challani Doravele O Chandamama (Singers: Malathi and Shanta Kumari)
* Kala Kala Aa Kokilemo Palukarinche Vintiva (Singers: Malathi and Shanta Kumari)
* Kalpagama Tallivai Ghanata Velasina Gouri (Singer: P. Leela)
* O Chaaru Sheela Le Javarala (Singer: V. Shivaram)
* O Matha Raavaa Naa Mora Vinavaa (Singer: P. Leela)
* Ore Ore Brahma Devuda (Singer: Kasturi Shiva Rao)
* Siri Thalam Vesenante (Singers: Kasturi Shiva Rao and P. Leela)
* Sri Thulasi Jaya Thulasi Jayamuneeyave (Singer: P. Leela)
* Thelusukondayya (Singers: T. G. Kamala Devi and Chorus)
* Upakaara Gunaalayavai Vunnavu Kade Maatha (Singer: P. Leela)

==References==
 

==External links==
*  
*  

 

 
 
 
 

 