Velvet Fingers
{{Infobox film
| name           = Velvet Fingers
| image	         = Velvet Fingers FilmPoster.jpeg
| caption        = Film poster
| director       = George B. Seitz
| producer       = George B. Seitz
| writer         = James Shelley Hamilton Bertram Millhauser
| starring       = George B. Seitz Marguerite Courtot
| music          = 
| cinematography = 
| editing        = 
| distributor    = Pathé Exchange
| released       =  
| runtime        = 15 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film lost by some sources,    a copy of it is actually available in the archives of the Cinémathèque Française.   

==Cast==
* George B. Seitz - Velvet Fingers
* Marguerite Courtot - Lorna George
* Harry Semels - Professor Robin
* Lucille Lennox - Clara
* Frank Redman - Pinky Thomas Carr - Mickey (as Tommy Carr)
* Joe Cuny - Needless Smith
* Al Franklin Thomas
* Edward Elkas

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 