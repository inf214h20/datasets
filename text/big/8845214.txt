The Adventures of Buratino (1959 film)
{{Infobox film
| name = The Adventures of Buratino
| image = Adventuresofburatinodvd.jpg
| caption = Krupnyy Plan DVD cover
| imdb_rating =
| director = Dmitriy Babichenko Ivan Ivanov-Vano Mikhail Botov
| producer = Dmitriy Babichenko Ivan Ivanov-Vano
| writer = Nikolai Erdman Lyudmila Tolstaya
| starring = Georgiy Vitsin Yelena Ponsova T. Dmitrieva Aleksandr Baranov N. Gulyayeva Margarita Korabelnikova Yevgeniy Vesnik V. Lepko
| narrator =
| music = Anatoliy Lepin
| cinematography =
| editing = N. Mayorova
| distributor =
| released = 1959 (USSR)
| runtime = 67 minutes
| country = USSR Russian
| budget =
| preceded_by =
| followed_by =
}} traditionally animated feature film directed by the "patriarch of Russian animation", Ivan Ivanov-Vano, along with Dmitriy Babichenko and Mikhail Botov.  It was produced at the Soyuzmultfilm studio in Moscow and is based on Aleksey Nikolayevich Tolstoy|Tolstoys The Golden Key, or Adventures of Buratino.
==Plot==
History of adventures of the wooden doll of Buratino which is cut out from a log by the organ-grinder Carlo.

==Creators==
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director-producers
| Dmitriy Babichenko Ivan Ivanov-Vano
| Дмитрий Бабиченко Иван Иванов-Вано
|-
| Director
| Mikhail Botov
| Михаил Ботов
|-
| Scenario
| Nikolai Erdman Lyudmila Tolstaya
| Николай Эрдман Людмила Толстая
|-
| Art Directors
| Svetozar Rusakov Pyotr Repkin
| Светозар Русаков Пётр Репкин
|-
| Artists
| D. Anpilov K. Malyshev O. Gemmerling G. Nevzorova Protr Korobayev I. Kuskova
| Д. Анпилов К. Малышев О. Геммерлинг Г. Невзорова Пётр Коробаев И. Кускова
|-
| Animators Faina Yepifanova Vadim Dolgikh Fyodor Khitruk Boris Butakov Kirill Malyantovich Igor Podgorskiy Vladimir Pekar Vladimir Popov Konstantin Chikin Vladimir Krumin Yelena Khludova Valentin Karavayev
|Фаина Епифанова Вадим Долгих Фёдор Хитрук Борис Бутаков Кирилл Малянтович Игорь Подгорский Владимир Пекарь Владимир Попов Константин Чикин Владимир Крумин Елена Хлудова Валентин Караваев
|-
| Camera Operators
| Mikhail Druyan
| Михаил Друян
|-
| Composer
| Anatoliy Lepin
| Анатолий Лепин
|-
| Sound Operator Nikolai Prilutskiy
|Георгий Мартынюк
|- Script Editor Raisa Frichinskaya
|Раиса Фричинская
|- Editor
|N. Mayorova
|Н. Майорова
|- Voice actors Georgiy Vitsin Yelena Ponsova T. Dmitrieva Aleksandr Baranov N. Gulyayeva Margarita Korabelnikova Yevgeniy Vesnik V. Lepko
|Георгий Вицин Елена Понсова Т. Дмитриева Александр Баранов Н. Гуляева Маргарита Корабельникова  Евгений Весник В. Лепко
|}

==Awards==
*Minsk, 1960 - First prize in the animated film category

==Familiar expressions==
*Not cROAKastor oil, and castor oil!
*It is better to die of an illness, than of castor oil.
*Well, boys, time you are living, go to wash hands!
*The past will bathe present.

==DVD releases==
*Films by Jove, May 18, 1999 ( ), Pinocchio and the Golden Key (aka. The Adventures of Buratino).
*Krupnyy Plan, 2004? (DVD region code|R5, PAL) - version restored by Krupnyy Plan ("full restoration of image and sound").  Contains original Russian soundtrack, no subtitles. Included film: The Adventures of Buratino.  Other features: Before and after restoration, photo album, director filmographies.
*Films by Jove, 2006 (DVD region code|R0?, NTSC) - version restored by Films by Jove in the 1990s.  Contains Russian soundtrack with English subtitles.  Included films: The Adventures of Buratino, Boy From Napoli (Мальчик из Неаполя), Chipollino (Чиполино).

==See also==
 
*History of Russian animation
*List of animated feature-length films

==External links==
*  at animator.ru (English and Russian)
* 
*   

===DVDs===

*   
**   

 
 

 
 
 
 
 
 
 
 