Doubletime
{{Infobox film
| name           = Doubletime
| image          = Doubletime_poster.jpg
| caption        = Festival poster
| director       =  
| producer       = Stephanie Johnes Alexandra Johnes  Andrea Meditch  Julie Goldman
| music          = Pete Miser  Jonathan Zalben Joel Goodman
| cinematography = Stephanie Johnes
| editing        = Miki Milmore
| distributor    = Discovery Films
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
}} jump roping Double Dutch. The film follows two disparate teams—one suburban white and one inner-city black—as they train to compete against each other for the very first time.

==Plot==
In the last 30 years jump roping has moved off the sidewalks and onto the stage. It now features astounding acrobatics, lightning speed and international competition. Doubletime follows the top two American teams: The Bouncing Bulldogs of Chapel Hill, North Carolina and The Double Dutch Forces of Columbia, South Carolina. Although they train in neighboring states, the Bulldogs and the Forces scarcely cross paths as they belong to separate leagues that do not compete against one another.

The Bulldogs represent the best of gymnastic freestyle jumping found mostly in white suburbia while the Forces belong to the inner- city African American tradition of Double Dutch. For the first time, both the Bulldogs and the Forces decide to enter a competition at the world famous Apollo Theater called the Holiday Classic. The film features four young athletes (age 11 to 18) who display courage, skills and charisma as they passionately prepare for the event. Doubletime culminates on stage in Harlem with this rowdy crowd- pleasing contest which features “fusion” routines where Double Dutch is blended with hip- hop dance and music.

==Cast==
* Ray Fredrick, coach of the Bouncing Bulldogs
* Joseph Edney, a member of the Bouncing Bulldogs
* Timothy Martin, a member of the Bouncing Bulldogs
* Erica Zenn, a member of the Bouncing Bulldogs
* Joy Holman, coach of the Double Dutch Forces
* Lacie Doolittle, a member of the Double Dutch Forces
* Antoine Cutner, a member of the Double Dutch Forces
* Tia Rankin, a member of the Double Dutch Forces
* Mike Peterson, an assistant coach of the Double Dutch Forces
* David Walker, the founder of the American Double Dutch League
* Richard Cendali, the founder of U.S.A. Jump Rope League

==Awards and festivals==
* Seattle Film Festival, Best Documentary
* “Best Sports Documentary” Newsday, 2007
* Heartland Film Festival, Crystal Heart Award
* South by Southwest Film Festival, Audience Award Finalist
* Tribeca Film Festival, Audience Award Nominee
* AFI/ Silverdocs
* Mill Valley Film Festival
* Edinburgh International Film Festival

==See also==
* Spellbound (2002 film)|Spellbound (2002 film)
* Mad Hot Ballroom

==External links==
*  
*  
*   Variety Magazine Review.
*   Austin Chronicle Review.
*   Film Threat Review.
*   Wall Street Journal.
*   CBS News.
*   NPR.
*   North Carolina Public Radio.

 
 
 
 
 
 
 
 
 