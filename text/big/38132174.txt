Storm Over Lisbon
{{Infobox film
| name           = Storm Over Lisbon
| image          = Storm Over Lisbon 1944 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = George Sherman
| producer       = George Sherman
| writer         = Dane Lussier  
| screenplay     = Doris Gilbert
| story          = Elizabeth Meehan
| starring       = {{Plainlist|
* Vera Ralston
* Erich von Stroheim
* Richard Arlen Robert Livingston
}}
| music          = Charles Maxwell
| cinematography = John Alton Arthur Roberts
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Storm Over Lisbon is a 1944 American thriller film directed by George Sherman and starring Vera Ralston, Erich von Stroheim and Richard Arlen. During World War II the owner of a Lisbon nightclub attempts to gain secret information in order to sell to the Japanese. 

==Main cast==
* Vera Ralston as Maria Mazarek
* Erich von Stroheim as Deresco 
* Richard Arlen as John Craig  Robert Livingston as Bill Flanagan 
* Eduardo Ciannelli as Blanco 
* Mona Barrie as Evelyn 
* Otto Kruger as Alexis Vanderlyn  Sarah Edwards as Maude Perry-Tonides 
* Alice Fleming as Agatha Sanford-Richards 
* Kenne Duncan as Paul
* Frank Orth as Murgatroyd 
* Aida Broadbent Girls as Dancers

==References==
 

==Bibliography==
* Lennig, Arthur. Stroheim. University Press of Kentucky, 2000.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 