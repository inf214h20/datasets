Sunny (1941 film)
{{Infobox film
| name           = Sunny
| image          = Sunny (1941 film) poster 1.jpg
| caption        =
| director       = Herbert Wilcox
| producer       = Merrill G. White (associate producer) Herbert Wilcox (producer)
| writer         = Oscar Hammerstein II (play) Otto A. Harbach (play) Sig Herzig
| narrator       =
| starring       = See below Anthony Collins
| cinematography = Russell Metty
| editing        = Elmo Williams
| distributor    =
| released       = May 30, 1941 
| runtime        = 98 minutes
| country        = USA
| language       = English
| budget         = $676,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross          = $1,096,000 
| preceded_by    =
| followed_by    =
| website        =
}}
 1941 film John Carroll, Grace Hartman, Paul Hartman, Frieda Inescort, and Helen Westley.

It is the second film version of the musical; the first was made in 1930.

== Cast ==
*Anna Neagle as Sunny OSullivan
*Ray Bolger as Bunny Billings John Carroll as Larry Warren
*Edward Everett Horton as Henry Bates Grace Hartman as Juliet Runnymede
*Paul Hartman as Egghead
*Frieda Inescort as Elizabeth Warren
*Helen Westley as Aunt Barbara
*Benny Rubin as Maj. Montgomery Sloan
*Muggins Davies as Muggins Richard Lane as Reporter
*Martha Tilton as Queen of Hearts
*Torben Meyer as Jean (head waiter)

== Soundtrack ==
*Anna Neagle and John Carroll - "Dye Love Me?" (Music by Jerome Kern, lyrics by Otto A. Harbach and Oscar Hammerstein II)
*"Believe Me If All Those Endearing Young Charms" (Music traditional, Lyrics by Thomas Moore)
*Anna Neagle and Ray Bolger - "Jack Tar and Sam Gob"
*Martha Tilton and chorus - "The Lady Must Be Kissed"
*Danced by Ray Bolger - "Ringmaster"
*Anna Neagle - "Sunny" (Music by Jerome Kern, lyrics by Otto A. Harbach and Oscar Hammerstein II)
*Danced by Grace Hartman and Paul Hartman - "Two Little Love Birds" (Music by Jerome Kern, lyrics by Otto A. Harbach and Oscar Hammerstein II)
*Danced as "The Mohache" by Grace Hartman and Paul Hartman - "Bolero" (Written by Maurice Ravel)
*Bolger also sung by Anna Neagle and John Carroll - "Who?" (Music by Jerome Kern, lyrics by Otto A. Harbach and Oscar Hammerstein II)

==Reception==
The film made a profit of $7,000. 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 