Out of the Blue (2006 film)
 
 
 
{{Infobox film
| name = Out of the Blue
| image    = Outoftheblue-poster.jpg
| director = Robert Sarkies
| producer = Steven OMeagher Tim White
| writer   = Graeme Tetley Robert Sarkies
| starring = Karl Urban Matthew Sunderland Lois Lawn Simon Ferry Tandi Wright Paul Glover William Kircher Georgia Fabish Fayth Rasmussen
| cinematography = Greig Fraser
| editing  = Annie Collins English 
| distributor = The Weinstein Company (DVD)
| released =  
| country  = New Zealand
| budget   = NZ$6 million
}} New Zealand film directed by Robert Sarkies and starring Karl Urban. The film premiered at the 2006 Toronto Film Festival in Canada and was released in New Zealand on 12 October 2006 to minor controversy. The film has since grossed well over $1 million at the New Zealand box-office taking it into the top ten highest grossing local films. {{
cite news| date = 14 December 2006 | url = http://www.nzherald.co.nz/section/6/story.cfm?c_id=6&objectid=10415128 | title = Aramoana film cracks $1 million | work = NZ Herald | publisher = APN Holdings | accessdate = 3 September 2010}} 

==Story== David Gray, an unemployed gun enthusiast, went on a shooting spree, killing 13 people before being killed by police.

==Plot Summary== David Gray, an unemployed man in his 30s who lives in his parent’s small holiday home, cycles into town where he has an argument with staff at a bank over a minor issue. Unstable & angry, he returns home where he has a cache of fire-arms, including a semi-automatic rifle. 
Late in the afternoon, he notices children from a neighbouring house have wandered onto his yard and he angrily abuses them, sparking a heated verbal exchange with their father, Garry Holden. Gray goes inside his house and then quickly re-emerges armed with the SAR, shooting Holden dead. 
Holden’s two young daughters, Chiquita and Jasmine, and his girlfriend Julie-Anne’s adopted daughter Rewa, witness the murder and flee inside Holdens house, attempting to hide. Gray enters and soon locates them.

Chiquita is then seen fleeing, having being wounded, trying to get help for Jasmine and Rewa (whose deaths occur off-screen). She reaches Julie-Anne and they both get into her van and drive towards the scene, trying to rescue the other girls, only to find Holden’s house has been set on fire. Julie-Anne is then forced to drive to safety as Gray fires at her vehicle.

Nearby residents hear the shooting and see the smoke, not yet comprehending what is happening. Gray enters a nearby house and shoots dead both elderly male occupants (their deaths occur off-screen). 
Elderly widow Eva Dickson, who recently has had hip surgery, ventures out with her walking frame to see what is happening, alongside a neighbour Chris Cole. Earlier, Dickson’s middle-aged son James had left the house, looking for his dog. An unseen Gray opens fire, hitting Cole and narrowly missing Dickson. Having collapsed and unable to stand up again, Dickson crawls inside her house to ring the police and then goes back to Cole, lying badly wounded but still conscious, to tell him that help is coming (Cole later succumbs to his injuries).

A utility with six people on board, including three children, drives up from the nearby beach, stopping near the burning house. Gray emerges and opens fire. The subsequent shootings are not shown, only the noise and the look of horror on the face of a nearby witness.
  Stewart Guthrie, Constable Nick Harvey and Detective Paul Knox. They arrive at Grays and Holden’s houses, seeing the carnage around the utility. The police attempt to surround Gray’s house but the gunman surprises Guthrie from behind, shooting him dead. Harvey, armed with a rifle, briefly has Gray in his sights but hesitates, missing his chance. 
Knox reaches the utility, discovering one of the occupants still alive- 3-year-old Stacey Percy, who has been wounded in the abdomen but is still conscious. Knox and Harvey enter Gray’s house but find it empty. They get into a car, Harvey nursing Stacey, trying to keep her conscious and Knox holding the bodies of the other two children from the utility, and drive to where a police cordon is being established, handing Stacey over to paramedics. Harvey is physically ill as he reacts to the fear and trauma.
 crib and, finding it deserted, spends the night there.

The next day, police have arrived en masse and are combing the town, searching for Gray. Armed Offenders Squad (AOS) officers locate the crib and surround the small house. After a brief exchange of gunfire, the officers throw tear-gas canisters into the house. Gray abruptly emerges, screaming obscenities and firing wildly from the hip. The waiting officers open fire and hit Gray several times, the gunman collapsing. With difficulty, the AOS men restrain him and then wait nearby as Gray dies of his wounds.

A postscript follows, consisting of a montage of scenes, including the full squad of AOS officers escorting Eva Dickson from her home as a mark of respect for her bravery, Chiquita Holden and Stacey Percy in hospital, both recovering from their wounds, the deliberate torching of Gray’s house that occurred several days after the massacre and a list of names of the 13 people who died on 13 November.

== Cast ==
* Karl Urban as Nick Harvey David Gray
* Lois Lawn as Eva Helen Dickson
* Simon Ferry as Garry Holden
* Tandi Wright as Julie-Anne Bryson
* Paul Glover as Paul Knox Sergeant Stu Guthrie
* Georgia Fabish as Chiquita Holden
* Fayth Rasmussen as Stacey Percy

== Reaction == Long Beach, a settlement six kilometres from Aramoana.
 Office of Film and Literature Classification has classified the film Out of the Blue as R15       (restricted to viewers under 15 years of age) with the descriptive note "violence and content that may disturb".  The film was restricted because the murders it depicts are likely to cause younger viewers distress and threaten their sense of personal safety.  "Out of the Blue deals with recent events involving real people.  For that reason we consulted with the families of victims and the Aramoana community.  We were impressed by the articulate and heartfelt comments they made at our meetings" said Chief Censor Bill Hastings. 

The film deals with violence in a realistic but restrained way.  The effect the film has on its audience is likely to depend on the circumstances of the viewer.  Mr Hastings said "for some of those closely involved in the events it portrays the film may be upsetting and traumatic.  Other people may view it as a sensitive portrayal of the responses of ordinary people to horrific events." 

One of the AOS officers who located and shot Gray, Mike Kyne, took exception to the way they were portrayed in the film as standing to one side and casually smoking cigarettes whilst a hand-cuffed Gray lay on the ground, dying of his injuries. Kyne said, Thats bullshit. Poetic licence. In reality, none of the AOS officers had smoked at the scene and after restraining Gray, they had called an ambulance. (Unlike the scene in the film, Gray did not die outside the crib but later in the ambulance en route to hospital.) Also, in the exchange of gunfire prior to Grays capture, one of the AOS members was shot in the ankle, which is not seen in the film. 

== References ==
 

==External links ==
*  
*  
*   An interview with Robert Sarkies
*   an essay by Alexander Greenhough, published in Metro 163 (December 2009) pp 108–11
*  

 
 
 
 
 
 
 