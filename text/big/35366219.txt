The Wild Racers
 
{{Infobox film
| name           = The Wild Racers
| image          =
| alt            =
| caption        =
| director       = Daniel Haller Roger Corman (uncredited)
| producer       = Joel Rapp Roger Corman (uncredited) associate Tamara Asseyev Pierre Cottrell
| writer         = Max House
| narrator       = Fabian Mimsy Farmer Judy Cornwell
| music          = Mike Curb Sidewalk Productions
| cinematography = Néstor Almendros camera operator Daniel Lacambre
| editing        = Verna Fields Dennis Jakob Ron Silkosky
| studio         = The Filmmakers
| distributor    = American International Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Wild Racers is a 1968 American feature film about a Grand Prix racing car driver. 

==Plot==
Stock car racer Jo Jo Quillico goes to Europe after an accident. He is hired by a race car tycoon to be runner up for a more experienced racer on the European circuit, working with his mechanic Charlie. However in his first race, Jo Jo cant help winning.

He has a series of love affairs, including with a shallow Englishwoman, but cannot see himself in a long term relationship - until he meets Katherine. He falls in love and begins to support his racing car partner. When his partner is injured, Jo Jo takes his chance and scores several victories. However, he breaks up with Katherine.

==Cast== Fabian as Jo Jo Quillico
*Mimsy Farmer as Katherine Pearson
*Alan Haufrect as Virgil
*Judy Cornwell as Pippy
*David Landau as Ian
*Warwick Sims as Charlie
*Talia Shire
*Dick Miller

==Production==
The movie was partly funded by Roger Corman. Tamara Asseyev was working as Cormans assistant when assigned to help produce the film.  Asseyev says Daniel Haller only agreed to direct it if Roger Corman agreed to also finance Hallers pet project, Paddy (film)|Paddy (1970).  

During production the film was known as Hells Racers. 

Mimsy Farmer was working in a hospital in Canada when director Dan Haller called asking if she wanted to be in the film. "I jumped at the chance to go to Europe and also to see my brother Philip, who was living in London at the time," she later recalled. "It was the best move I’d made up to then and I loved traveling in France, Spain, and Holland."   accessed July 5, 2014 

The Wild Racers was shot in six countries in five and a half weeks with two weeks preparation. Shes Young, Pretty and Produces Film: Incomplete Source Thomas, Kevin. Los Angeles Times (1923-Current File)   May 22, 1970: h1.   Most of the movie was filmed on locations without permission. "We were one step ahead of the law the whole time," says Haller.  This was a method which had been used by Corman on The Young Racers (1963). Talia Coppola played the second lead, scouted locations and did the set dressings.

The racing advisers were Peter Theobald (English), Jan Pieter Visser (Dutch), Jean Pierre Arlet (French) and Carlos Diego (Spanish).

The movie was shot by Nestor Almendros, and Daniel Lacambre worked as camera operator.  Almendros later called the experience on the film influential in his career:
 It was an insignificant movie. But the importance of the experience was we learned to work very fast. Its a two fold area; we realized that because you are faster, you are not necessarily worse in cinema; and because you take a long time to prepare something, its not necessarily going to be better. With every shot you take time somehow; some shots you take longer than with others. But, on the whole, you just have to go ahead and shoot and follow your intuition. Sometimes if you think too much you sort of lose the intuition and the natural flow.  
Dan Haller later recalled:
 If I didnt think we had gotten a shot, Id have Nestor shoot it again and have them print both takes. But by the time we got to Paris we were totally exhausted because it was a really grueling schedule and I would never even get to see a location. Talia Coppola was with us on the film and she would be scouting locations for us, and Id tell her when you get to Rouen, or someplace like that, just go buy all the postcards you can and well use that to find our locations. Thats how we did a lot of it.   access July 13, 2014   1968 riots and also Spain. Haller said Alemndros, a fugitive from General Franco|Francos Spain, was highly anxious during the Spanish leg of the shoot for fear he would be arrested for leaving the country. 

Roger Corman said the film included "one of the greatest examples of co-ordinated shooting Ive ever been involved with." Corman:
 When we were in Paris we wanted a scene by the eternal flame of the Arc de Triomphe. Not only could we not get permission to shoot there, we didnt even have permission to be shooting in France! (laughter) So we worked out a sequence where I was in one car, Nestor was in one car, Chuck Hannawalt was in one car, and the two actors, Fabian and Mimsey Farmer, were in another car. The actors had already rehearsed the scene and we all drove around the Arc de Triomphe and then stopped our cars. We stopped traffic and everyone ran out to get the shot. Nestor put the camera down and Fabian and Mimsy knew exactly where to go. Dan got the shot and we jumped back in our cars and took off.  
Peter Gardiner, who had done film effects for Roger Corman on The Trip, provided special effects.  Autopsy Scheduled in Death of Film Maker
Los Angeles Times (1923-Current File)   October 23, 1968: b5.  
One of the lead actors was dubbed by Corman regular Dick Miller, who appears in a cameo. 

Frances Doel worked as a script supervisor.

==Reception==
Haller claimed he didnt think AIP "wanted to release the film at all". 

The Christian Science Monitor called it "unusually well-photographed... the film is aimed at teenagers but thwarts its own purpose by inclusion of too much sexuality." MOVIE GUIDE: Recent releases
The Christian Science Monitor (1908-Current file)   April 24, 1968: 14.  

Quentin Tarantino later described the film as his favourite racing car movie:
 It’s shot like an Antonioni movie, with very little dialogue, most of which is voice-over. And no shot in the movie lasts more than twenty seconds. The quick edits keep you on the edge of your seat. It’s very avant-garde, but it still delivers a proper racing movie. Classy.  

==References==
 

==External links==
* 
*  at TCMDB
*  at New York Times
*  at Grindhouse Database

 

 
 
 
 
 
 

 