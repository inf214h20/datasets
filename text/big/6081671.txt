McDull, the Alumni
 
 
{{Infobox film 
|  name        = McDull, da Alumni (春田花花同學會) 
|  image       = McDull.jpg
|  image size  = 185px
|  border      = yes 
|  genre       = Animation, Comedy 
|  director    = Samson Chiu 
|  producer    = Peter Chan Brian Tse 
|  writer      = Brian Tse
|  starring    = 
|  studio      = Morgan & Chan Films
|  distributor = Sil-Metropole Organisation 
|  music       = 
|  released    =   
|  runtime     = 92 min. 
| country      = Hong Kong
|  language    = Cantonese
|  budget      = 
}}
 animated film directed by Samson Chiu. It is the third film adaptation of the popular McDull comic book series, following My Life as McDull, and McDull, Prince de la Bun. The film features a large ensemble cast of many of Hong Kongs cinematic icons.

The third film in the series finds McDull and his friends satirically exploring different roles in society.

==Cast==
*Ronald Cheng
*Kelly Chen
*Gigi Leung Anthony Wong
*Eric Tsang
*Josie Ho
*Shawn Yue
*Daniel Wu
*Jan Lamb
*Francis Ng
*Nicholas Tse
*Jaycee Chan Miu Kiu-Wai
*Cheung Tat-Ming
*Alex Fong Lik-Sun
*Isabella Leong
*Miki Yeung Lai Yiu-Cheung
*Christopher Doyle
*Zhou Bichang Steven Cheung
*Theresa Fu
*Bolin Chen

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 


 
 