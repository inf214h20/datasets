The Crooked Circle (1932 film)
 
{{Infobox film
| name           = The Crooked Circle
| image          = The-crooked-circle-1932.jpg
| image_size     = 
| caption        = Film poster
| director       = H. Bruce Humberstone
| producer       = William Sistrom
| writer         = Ralph Spence (original screenplay) Tim Whelan (additional dialogue)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Robert Kurrle
| editing        = Doane Harrison
| distributor    = Sono Art-World Wide Pictures (1932 release) Astor Pictures (re-release)
| released       = 25 September 1932
| runtime        = 70 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Crooked Circle (1932 in film|1932) is an American film, a comedy-mystery directed by H. Bruce Humberstone.
 Don Lee NBC Television experimental station WX2BS, now WNBC-TV in New York City.  

==Characters and story==
Amateur detectives in the Sphinx Club are rivals of an evil gang known as The Crooked Circle. When a Sphinx tip leads to an arrest of a Crooked Circle member, they swear revenge on Sphinx member Colonel Theodore Walters (Berton Churchill). Nora Rafferty (Zasu Pitts) complains to Old Dan (Christian Rub) about life in creepy Melody Manor. 

Brand Osborne (Ben Lyon) intends to resign from the Sphinx Club, and his replacement is the Indian Yoganda (C. Henry Gordon), who proclaims, "Evil is on the way." When Rafferty sees Yogandas turban, she says, "Im sorry you got a headache, sir. Shall I get you a Bromo-Seltzer?" Policeman Arthur Crimmer (James Gleason) attempts to straighten out the confusion.

== Cast ==
*Zasu Pitts as Nora Rafferty
*James Gleason as Arthur Crimmer
*Ben Lyon as Brand Osborne
*Irene Purcell as Thelma Parker
*C. Henry Gordon as Yoganda
*Raymond Hatton as Harmon (The Hermit)
*Roscoe Karns as Harry Carter
*Berton Churchill as Col. Walters
*Spencer Charters as Kinny
*Robert Frazer as The Stranger
*Ethel Clayton as Yvonne
*Frank Reicher as Rankin
*Christian Rub as Old Dan Tom Kennedy as Mike, the policeman

==See also== Party Girl, the first commercial comedy-drama feature film shown on the Internet

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 