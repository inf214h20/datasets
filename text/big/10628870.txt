Valli (film)
{{Infobox film|
| name = Valli
| image = Valli rajini.jpg
| caption = Poster
| director = K. Natraj
| writer = Rajinikanth Priyaraman Vadivelu Govinda Dilip
| producer = Rajinikanth
| music = Ilaiyaraaja
| cinematography = Ajayanan Vincent
| editing = Ganesh Kumar
| studio = Rajini Arts
| distributor = Rajini Arts
| released = 20 August 1993
| runtime =
| country = India Tamil
| budget =
}}
 Tamil Director K. Nataraj. It stars Rajinikanth in a guest role with Sanjay and Priyaraman in the lead roles. The films music was composed by maestro Ilayaraja. This movie got dubbed into Telugu as Vijaya. It was a big failure at the box office incurring huge losses to Rajinikanth, the producer.

==Plot==
Valli (Priya Raman) returns to her village after studying for 15 years in the city. Her cousin (Hariraj) celebrates her arrival to the city. He was in love with Valli since childhood days, but changed after she went to study in the city. She is no more in love with him. She falls in love with a city guy called Shekar (Sanjay) who comes to the village with his friends for hunting. Shekar cheats her and escapes to the city. Later Shekar is brought back by her cousin. Instead of marrying the city guy she kills him for cheating her.

==Cast==
*Rajinikanth 
*Priya Raman 
*Vadivelu
*Hariraj
*Sanjay

==Production==
K. Natraj, Rajinikanths friend from university who earlier directed Anbulla Rajinikanth was approached by Rajini to take part as an assistant director in Annamalai (film)|Annamalai to which Natraj gladly accepted. Then Rajini approached his friends and announced that he would like to make a film for them. The script of "Valli" was written by Rajini himself. Rajini revealed that the first thing came to his mind while scripting the film was the climax. He imagined that the girl should kill the boy who destroyed his life as opposed to the typical cliche of Tamil films where the raped victim is made to marry the rapist, he also revealed that he had finished writing the screenplay within 7 days. http://rajinifans.com/interview/vikatan1993.php  Priyaraman made her debut as heroine while Hariraj and Sanjay were introduced in this film. Rajini was not interested to appear in cameo appearance but with insistence of his friends he accepted to do small role and finished his portions within five days. 
 Alex as actor. The filming was held at Chalakudi, Pollachi and Red Hills. Hari_(director)|Hari, director of Saamy and Singam was one of the assistants in this film. http://cinema.maalaimalar.com/2013/01/07231703/valli-movie-rajini-get-to-help.html 

==Sound Track==
The music composed by Ilaiyaraaja. The song "Ennulle Ennulle" remains one of the popular songs from this film.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Lyrics
|-  1 || Vaali (poet)|Vaali
|- 2 || Enna Enna Kanavu || Ilaiyaraaja
|- 3 || Ennulle Ennulle || Swarnalatha
|- 4 || Ku Ku Koo || Latha Rajinikanth || Pulamaipithan
|- 5 || Vaali
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 