No End (film)
{{Infobox film
| name        = No End
| image       = noend.jpg
| caption     = Theatrical release poster
| writer      = Krzysztof Kieślowski Krzysztof Piesiewicz
| starring    = Grażyna Szapołowska Maria Pakulnis Aleksander Bardini
| director    = Krzysztof Kieślowski
| producer    = Ryszard Chutkowski
| music       = Zbigniew Preisner
| cinematography = Jacek Petrycki
| editing     = Krystyna Rutkowska
| released    = 17 June 1985
| runtime     = 109 minutes
| country     = Poland
| language    = Polish
}}
 1985 film Solidarity in 1981.    Kieślowski worked with several regular collaborators for the first time on No End.

==Plot==
A Polish translator, Ulla (Grażyna Szapołowska), grieves for her recently deceased lawyer husband. As she copes with her loss, the family of her husbands last client, Darek Stach, contacts her in need of legal documents and advice. Ulla struggles with caring for her son, and alternately trying to remember and to forget her husband, while Darek struggles to come to terms with his imprisonment for political dissidence. Ullas husbands ghost observes these events, occasionally becoming visible to Ulla and Darek.

==Cast==
* Grażyna Szapołowska as Urszula Zyro
* Maria Pakulnis as Joanna Stach
* Aleksander Bardini as Lawyer Mieczyslaw Labrador
* Artur Barciś as Darek Stach Danny Webb as American
* Jerzy Radziwilowicz as Antek Zyro
* Michal Bajor as Miecio (aplikant)
* Marek Kondrat as Tomek, Anteks friend
* Tadeusz Bradecki as Hipnotyzator
* Krzysztof Krzeminski as Jacek Zyro
* Marzena Trybala as Marta Duraj
* Adam Ferency as Rumcajs
* Elzbieta Kilarska as Antonis Mother
* Jerzy Kamas as Judge Biedron
* Hanna Dunowska as Justyna
* Jan Tesarz as Joannas Father
* Andrzej Szalawski as Lawyer   

==Production==
The film was Kieślowskis first writing collaboration with the screenwriter Krzysztof Piesiewicz, who co-wrote the screenplays for all of Kieślowskis subsequent films, and the earliest of his films with music by Zbigniew Preisner, who provided the musical score for most of Kieślowskis subsequent films. As in his later scores, Preisners music is explicitly referenced by the characters in the film itself, in this case with the main characters son playing the theme on a piano at home.

==Reception==
===Critical response===
No End received positive critical reviews. In his review in A.V. Club, Noel Murray felt that the film deserved to be "counted among his acknowledged classics." Murray gave it an A+ rating.   

In his review in Cinemania, Dan Jardine wrote, "No End is Kieslowski’s dry run for Blue, both are wrenching and  lensed studies of one woman’s struggle to deal with the death of loved ones in a larger  charged context. Where they differ: While similarly bleak and sorrowful, Blue finds a tortured peace, a painful hope, where No End is a giant sinkhole of despair."   

In his review in the Chicago Reader, Jonathan Rosenbaum called the film "terse, suggestive, and pungent, with juicy performances by Bardini and Szapolowska."   

On the aggregate reviewer web site Rotten Tomatoes, the film received a 90% positive rating from top reviewers based on 10 reviews, and a 77% positive rating from audience reviews based on 682 reviews.   

==References==
;Citations
 

;Bibliography
 
*  
*  
 

==External links==
*  
*  
*  

 

 
 
 