Christmas in Connecticut
{{Infobox film
| name           = Christmas in Connecticut
| image          = ChristmasInConnecticut.jpg
| caption        = One of theatrical release posters Peter Godfrey William Jacobs
| writer         = Lionel Houser Adele Comandini Aileen Hamilton (story)
| starring       = Barbara Stanwyck Dennis Morgan Sydney Greenstreet
| music          = Frederick Hollander
| cinematography = Carl E. Guthrie
| editing        = Frank Magee
| distributor    = Warner Bros.
| released       =  
| runtime        = 102 minutes
| language       = English
| budget         =
| country        = United States
| gross          = $3 million   
}}
 American List Christmas film romantic comedy Peter Godfrey, and starring Barbara Stanwyck, Dennis Morgan, and Sydney Greenstreet.   

==Plot==
  as Elizabeth Lane in Christmas in Connecticut]]

Elizabeth Lane (Barbara Stanwyck) is a single food writer living in New York whose articles about her fictitious Connecticut farm and her husband and baby are admired by housewives across the country. Her publisher, Alexander Yardley (Sydney Greenstreet), is unaware of the charade and insists that Elizabeth host a Christmas dinner for returning war hero Jefferson Jones (Dennis Morgan).  Facing a career-ending scandal for her and her editor, Dudley Beecham (Robert Shayne), she is forced to comply. In desperation, Elizabeth agrees to marry her friend John Sloan (Reginald Gardiner), who has a farm in Connecticut, even though she does not love him.  She enlists the help of her friend, chef Felix Bassenak (S.Z. Sakall), who has been providing her with recipes for her articles.
 Una OConnor), the housemaid, and a neighbors baby that they will pretend is their baby. Elizabeth and John plan to be married immediately by Judge Crothers (Dick Elliott), but the ceremony is interrupted when Jefferson arrives.  Jefferson and Elizabeth give the baby a bath; after dinner they spend time together in the barn. They are falling in love, but Jefferson restrains himself because he believes that Elizabeth is married with a child.

The judge returns on Christmas morning, but the ceremony is postponed a second time when a different neighbors baby is presented instead of the one from the day before. The household is alarmed when Felix claims that the baby has swallowed his watch.  After the judge leaves, Felix admits to Elizabeth that he had lied about the watch to stop the wedding. While the household attends a local dance, the babys real mother arrives to pick up her baby. Yardley witnesses her leaving with the child, and assumes someone is kidnapping the baby. He calls the police and the newspapers. Elizabeth and Jones spend the night in jail, mistakenly charged with stealing a neighbors horse and sleigh, and return to the farm early the next morning. Yardley chastises Elizabeth for being out all night and accuses her of neglecting her child. Elizabeth finally confesses all. Furious, Mr. Yardley fires her. 

Jeffersons fiancée, Mary Lee, arrives unexpectedly. Dejected, Elizabeth retires to pack her things and leave the farm.  Felix learns that Mary Lee has already married someone else and must break the engagement. He entices Yardley into the kitchen with the smell of cooking kidneys. He fabricates a story about a competing magazines attempts to hire Elizabeth, and Yardley decides to hire her back at double her salary.  Felix tells Jones that he is free to pursue Elizabeth. Elizabeths packing is interrupted, first by Yardley and then by Jones. After teasing her that he is a cad who woos married women, Jefferson reveals the truth. The couple kisses and plan to marry.

==Cast==
* Barbara Stanwyck as Elizabeth Lane
* Dennis Morgan as Jefferson Jones
* Sydney Greenstreet as Alexander Yardley
* Reginald Gardiner as John Sloan
* S.Z. Sakall as Felix Bassenak
* Robert Shayne as Dudley Beecham Una OConnor as Norah
* Frank Jenks as Sinkewicz
* Joyce Compton as Mary Lee
* Dick Elliott as Judge Crothers 

==Remake==
 

In 1992, a remake of Christmas in Connecticut was made, starring Dyan Cannon as Elizabeth, Kris Kristofferson as Jefferson Jones, and Tony Curtis as Mr. Yardley. The made-for-TV movie, which first aired on TNT (TV channel)|TNT, was directed by Arnold Schwarzenegger, who also made a cameo as the man sitting in front of the media truck. In this remake, Elizabeth "Blane" is the hostess of her own cooking show. When her manager, Alexander Yardley, introduces her to Jefferson Jones—a forest ranger who lost his cabin in a fire—he asks her to make Jones Christmas dinner live on her show. As in the original, Elizabeth isnt as talented as she seems. This version was not as well-received as the original. As one critic wrote, "Youll be hungry for a better movie after suffering through this film".    

==References==
 

==External links==
*  
*  
*  

; Remake
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 