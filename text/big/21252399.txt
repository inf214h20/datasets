The Last Bridge
 
{{Infobox film
| name           = The Last Bridge
| image          = Die Letzte Brücke.jpg
| caption        = Film poster
| director       = Helmut Käutner
| producer       = Carl Szokoll
| writer         = Helmut Käutner Norbert Kunze
| starring       = Maria Schell
| music          = 
| cinematography = Elio Carniel
| editing        = Hermine Diethelm Paula Dvorak
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Austria
| language       = German
| budget         = 
}}

The Last Bridge ( ) is a 1954 Austrian war drama film directed by Helmut Käutner. It tells the story of a German nurse who is sent to the front as a punishment for tending a wounded Yugoslav soldier. The film was entered into the 1954 Cannes Film Festival.     

==Cast==
* Maria Schell as Dr. Helga Reinbeck
* Bernhard Wicki as Boro
* Barbara Rütting as Milica
* Carl Möhner as Martin Berger
* Pavle Mincic as Momcillo
* Horst Hächler as Leutnant Scherer
* Robert Meyn as Stabsartz Dr. Rottsieper
* Zvonko Zungul as Partisan Sava
* Tilla Durieux as Mara
* Fritz Eckhardt as Tilleke
* Janez Vrhovec as Partisan Vlaho
* Walter Regelsberger as Nachrichtensoldat
* Steffie Schwarz as Oberschwester
* Bata Stojanovic as Partisan
* Stevo Petrovic as Partisan Ratko
* Milan Nesic as Partisan
* Franz Eichberger as Gebirgsjäger
* Heinrich Einsiedel as Gebirgsjäger
* Pero Kostic as Partisan

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 