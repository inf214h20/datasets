Aastiparulu
{{Infobox film
| name           = Aastiparulu
| image          = Aastiparulu.jpg
| image size     =
| caption        =
| director       = V. Madhusudhan Rao
| producer       = V. B. Rajendra Prasad
| writer         = Acharya Atreya
| narrator       = Jayalalitha Gummadi Gummadi G. Varalakshmi
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| distributor    =
| released       = 1966
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| preceded by    =
| followed by    =
}}
Aastiparulu is a 1966 Telugu Drama film directed by V. Madhusudhan Rao and starring Akkineni Nageswara Rao, Jaggaiah, Gummadi Venkateswara Rao and Jayalalitha. The film was remade in Tamil as En Thambi.

==Cast==
{| class="wikitable"
|-
! Actor/Actress !! Character
|-
| Akkineni Nageswara Rao || Krishna/Seenu
|-
| Kongara Jaggaiah || Bhaskar "Baachi"
|- Jayalalitha || Radha
|- Gummadi || Zamindar
|-
| G. Varalakshmi ||  Zamindars sister
|- Suryakantam ||  Zamindars wife
|-
| Chittor V. Nagaiah || Diwan
|-
| Relangi Venkataramaiah || Prasad
|- Girija || Varam
|}

==Soundtrack==
There are about 10 songs and poems in the film.  Ghantasala and P. Susheela)
* "Chali Chali Chali Vechani Chali" (Singers: Ghantasala and P. Susheela)
* "Chitti Ammalu Chinni Nannalu Mana Iddarike Telusule Mamatalu" (Singer: Ghantasala)
* "Erra Errani Buggaladana Nalla Nallani Kannuladana" (Singers: Ghantasala and P. Susheela)
* "Magavadivale Egaresukupo Pagavadivale Nanu Dochukupo (Singer: P. Susheela)
* "Midisi Padaku Midisi Atta Kootura" (Singer: Ghantasala)
* "Soggade Chinni Nayana Okka Pittanaina Kottaledu Soggadu" (Singer: P. Susheela)

==Awards==
* Filmfare Award for Best Film – Telugu - V. B. Rajendra Prasad (1966)
* The film won Nandi Award for Best Feature Film in 1966 from Government of Andhra Pradesh.

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 