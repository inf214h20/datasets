The Founding of a Republic
{{Infobox film
| name = The Founding of a Republic
| image = The Founding of a Republic - poster.jpg
| caption = Film poster
| film name = {{Film name| simplified = 建国大业
| traditional = 建國大業
| pinyin = Jiàn Gúo Dà Yè
| jyutping = Gin3 Gwok3 Daai6 Jip6 }}
| director = Huang Jianxin Han Sanping
| producer = Huang Jianxin Han Sanping
| writer = Chen Baoguang Wang Xingdong
| starring = Tang Guoqiang Zhang Guoli Xu Qing Liu Jin Chen Kun Wang Wufu
| music = Shu Nan
| cinematography = Zhao Xiaoshi
| editing = Xu Hongyu Media Asia Emperor Motion Pictures Universe Entertainment Polybona Films China Movie Channel Beijing Guoli Changsheng Movies & TV Productions Beijing Hualu Baina Film & TV Production Jiangsu Broadcasting Corporation DMG Entertainment Beijing Xinbaoyuan Movie & TV Investment
| distributor = China Film Group Beijing Polybona Film Distribution  (China)  Universe Films Distribution  (Hong Kong) 
| released =  
| runtime = 135 minutes
| country = China
| language = Mandarin
| budget = $8.8&ndash;$10 million
| gross = $62.5 million  
}} China Film Group (CFG) to mark the 60th anniversary of the Peoples Republic of China. The film was directed by Huang Jianxin and China Film Group head Han Sanping.
 Communist Party, but in response to reactions outside Mainland China, Huang Jianxin, the films co-director, "has said it was unfair to describe The Founding of a Republic as propaganda, since modern Chinese audience were too sophisticated to swallow a simplistic rendering of history."  While the Taiwan government has asserted that it has no plans to actively censor the film, it has yet to be released because Taiwans 2009 annual quota of 10 mainland films was already reached.   

The film retells the tale of the Communist ascendancy and triumph, and has a star-studded cast including Andy Lau, Ge You, Hu Jun, Leon Lai, Zhang Ziyi, Donnie Yen, Jackie Chan, Jet Li, Zhao Wei, and directors Jiang Wen, Chen Kaige and John Woo, many of whom make cameo appearances so brief they could be easily missed; the leading roles are played by actors equally renowned in China, such as Tang Guoqiang and Zhang Guoli, but they are less well-known internationally. A CFG spokesman said many stars answered Han Sanpings call to appear in the film, and waived their fee. Thus, the movie kept to its modest budget of 60&ndash;70&nbsp;million yuan (US$8.8&ndash;$10 million). According to the executive at one of Chinas top multiplex chains, the film unusually marries "the core of an ethically inspiring film with commercial packaging." 

==Plot== civil war and to establish a multi-party government in China.
 National Assembly President of the Republic of China (ROC). At the same time, the peace negotiations between the CPC and KMT fail and the civil war continues. Other political figures such as Zhang Lan, Soong Ching-ling and Li Jishen, support the CPC because they oppose Chiangs government, even though they are in non-battleground areas such as Shanghai and Hong Kong.
 Red Army NRA in subsequent battles and eventually Chiangs forces retreat to Taiwan in December 1949. On 1 October 1949, Mao Zedong proclaims the Peoples Republic of China with its capital at Beijing, marking the start of a new era for China.

==Cast==

===Main figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|- Central Peoples Government
|-
| Zhang Guoli || Chiang Kai-shek || President of the Republic of China (ROC), Director-General of the Kuomintang (KMT)
|-
| Xu Qing || Soong Ching-ling || Sun Yat-sens wife; later Vice President of the Peoples Republic of China (PRC)
|- Premier of the PRC
|-
| Chen Kun || Chiang Ching-kuo || Chiang Kai-sheks son; Youth League of the Three Peoples Principles leader, later President of the ROC
|- Red Army, later Vice Chairman of the PRC
|- Vice President of the ROC
|- CPC Central President of the PRC
|-
| Wang Jian || Ren Bishi || CPC Central Committee secretary
|- NRA general, later Vice Chairman of the PRC
|-
| Wang Bing || Zhang Lan || Chairman of the China Democratic League, later Vice Chairman of the PRC
|-
| Vivian Wu || Soong May-ling || Chiang Kai-sheks wife and Soong Ching-lings sister; later First Lady of the ROC
|- Chahar government, PRC Water Resources Minister
|}

===Chinese Communist Party figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|- CPPCC Chairwoman
|- CMC
|-
| Zong Liqun || Peng Dehuai || Second-in-command of the Red Army, later Vice Chairman of the CMC
|- You Liping || Lin Biao || Red Army general, later Vice Chairman of the PRC
|-
| Che Xiaotong || Liu Bocheng || Red Army general, later Marshal of the PLA
|- Vice Premier of the PRC
|- Chen Yi Foreign Minister of the PRC
|-
| Sun Jitang || Luo Ronghuan || Red Army general, later Marshal of the PLA
|-
| Wang Jun || Xu Xiangqian || Red Army general, later Marshal of the PLA
|-
| Ao Yang || Nie Rongzhen || Red Army general, later Marshal of the PLA
|-
| Ye Jin || Ye Jianying || Red Army general, later Vice Chairman of the CPC Central Committee
|- Hou Yong || Chen Geng || Red Army general
|-
| Zhao Ningyu || Liu Yalou || Red Army general
|-
| Zhang Erdan || Jiang Qing || Mao Zedongs wife
|- State Planning Commission
|-
| Wang Huaying || Pan Hannian || CPC politician, later Deputy Mayor of Shanghai
|-
| Huang Xiaoming || Li Yinqiao || Mao Zedongs chief bodyguard
|-
| Ma Yue || Yan Changling || Mao Zedongs chief bodyguard
|- PRC Culture Ministry
|-
| Xu Fan || Liao Mengxing || Liao Zhongkai and He Xiangnings daughter; Soong Ching-lings secretary, later member of the All-China Womens Federation
|- Chen Hao || Fu Dongju || Fu Zuoyis daughter; Peoples Daily reporter
|-
| Chen Daoming || Yan Jinwen || Deputy leader of Shanghai Investigative Branch of State Secrets Department
|- PRC Foreign Affairs Ministry official 
|}

===Kuomintang figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|-
| Leon Lai || Cai Tingkai || NRA general, later CPC member
|-
| Chen Kaige || Feng Yuxiang || Chiang Kai-sheks sworn brother, NRA general, Vice President of National Military Council
|- BIS
|-
| Hu Jun || Gu Zhutong || Commander-in-chief of NRA; later Chief of staff of NRA
|-
| Andy Lau || Yu Jishi || NRA general
|- NRA Air Force general
|- NRA Navy NPC member
|-
| Xiao Wenge || He Yingqin || NRA general
|-
| You Yong || Bai Chongxi || NRA general
|-
| Li Qiang || Chen Cheng || NRA general, later Vice President of the ROC
|-
| Yang Xiaodan || Zhang Zhizhong || NRA general, later CPC member
|- Kung Hsiang-hsi and Soong Ai-lings son
|- Wu Kuo-chen || Mayor of Shanghai, later Governor of Taiwan Province
|-
| Zhong Xinghuo || Huang Shaoxiong || Vice President of Control Yuan, later NPC member
|-
| Xia Gang || Shao Lizi || NRA secretary, later NPC member
|-
| Ding Zhicheng || Lu Guangsheng || BIS agent
|-
| Tao Zeru || Wu Tiecheng || Vice President of Legislative Yuan, later Senior Advisor to the Office of the President of the ROC
|-
| Xu Huanshan || Yu Youren || President of Control Yuan
|-
| Zhao Xiaoshi || Sun Fo || Sun Yat-sens son; President of Legislative Yuan
|-
| Sun Xing || Du Yuming || NRA general
|-
| Lin Dongfu || Gan Jiehou || Li Zongrens private secretariat representative
|-
| Ye Xiaojian || Tai Chi-tao || President of Examination Yuan
|-
| Zhang Hanyu || Liu Congwen || 
|}

===China Democratic League figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|- KMT Revolutionary Committee member
|- PRC Supreme Peoples Court
|-
| Bi Yanjun || Luo Longji || CDL member, later CPC member
|-
| Wu Gang || Wen Yiduo || Poet, CDL member
|-
| Qiao Lisheng || Guo Moruo || Author, poet, historian, archaeologist; later President of the Chinese Academy of Sciences
|-
| Deng Chao || Xu Beihong || Artist, later President of the China Central Academy of Fine Arts
|-
| Li Bin || He Xiangning || Artist, CDL member, later CPC member
|- PRC Health Minister
|-
| Liu Yiwei || Li Huang || Chinese Youth Party founder
|}

===Other notable historical figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|-
| Feng Xiaogang || Du Yuesheng || Shanghai Green Gang boss
|-
| John Woo || Liu Wenhui || Sichuan warlord
|-
| Feng Yuanzheng || Philip Fugh || John Leighton Stuarts personal assistant
|-
|}

===Foreign political figures===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role !! Description
|- Aleksandr Pavlov || Joseph Stalin || Soviet Union leader
|- Secretary of Secretary of Defense
|-
| Donald Freeman || Patrick J. Hurley || United States Ambassador to China
|-
| Leslin H Collings || John Leighton Stuart || United States Ambassador to China, President of Yenching University
|}

===Fictional characters===
{|class="wikitable" border="1" cellspacing="1"
|-
! Actor !! Role
|-
| Sun Honglei || Hu Liwei, Central Daily News reporter
|-
| Fan Wei || Guo Bencai, Mao Zedongs cook
|- Liu Ye || Red Army old soldier
|-
| Ge You || Red Army Fourth Division leader
|-
| Wang Baoqiang || rowspan=2 | Red Army Fourth Division soldiers
|-
| Wang Xuebing
|-
| Huang Shengyi || Xinhua News Agency broadcaster
|-
| Jackie Chan || Reporter interviewing Li Jishen
|- Chen Hong || Reporter interviewing Zhang Lan
|-
| Li Youbin || Newspaper agency head
|-
| Zhang Jianya || Chiang Kai-sheks deputy
|-
| Lian Jin || Zhou Zhirous deputy
|-
| Sun Xing || Yu Jishis deputy
|-
| Guo Xiaodong || NRA officer
|-
| Liu Ye || rowspan=2 | KMT police officers
|-
| Huang Zixuan
|-
| Guo Degang || Cameraman
|-
| Zhang Shen || Translator accompanying Liu Shaoqi to the Soviet Union
|-
| Gong Beibi || rowspan=4 | Red Army female soldiers
|-
| He Lin
|-
| Yang Ruoxi
|-
| Che Yongli
|-
| Tony Leung Ka-fai || rowspan=11 | CPPCC members
|-
| Feng Gong
|-
| Zhao Wei
|-
| Miao Pu
|-
| Dong Xuan
|-
| Chen Shu
|-
| Ning Jing
|-
| Shen Aojun
|-
| Wang Yajie
|-
| Zhao Baoyue
|-
| Wang Fuli
|}

==See also==
* The Founding of a Party

==References==
 
 

==External links==
*  
*   at the Chinese Movie Database

 
 
 
 
 
 
 