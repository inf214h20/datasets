Penny Gold
{{Infobox film
| name           = Penny Gold
| image          = "Penny_Gold"_(1973).jpg
| image_size     =
| caption        = UK theatrical poster
| director       = Jack Cardiff George H. Brown
| writer         =   David D. Osborn (as David Osborn) Liz Charles-Williams
| narrator       =
| starring       = James Booth  Francesca Annis  Nicky Henson John Scott
| cinematography =  Ken Hodges
| editing        =  John Trumper
| studio         =   Fanfare Films Ltd. (as A Fanfare Film)
| distributor    =  Scotia-Barber (UK)
| released       = 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Jack Cardiff and starring James Booth, Francesca Annis, Nicky Henson and Joss Ackland.  Two policemen investigate a series of murders, discovering the motivation was an extremely valuable Postage stamp|stamp.

==Cast==
* James Booth - Matthews
* Francesca Annis - Delphi/Diane
* Nicky Henson - Rogers
* Joss Ackland - Jones
* Richard Heffer - Claude
* Sue Lloyd - Model
* Joseph OConor - Blachford
* Una Stubbs - Anna
* George Murcell - Doctor Merrick
* Marianne Stone - Mrs Parsons
* Penelope Keith - Miss Hartridge
* John Savident - Sir Robert Hampton
* Clinton Greyn - Van Der Meij
* Christian Rodska - Clerk
* Marc Zuber - Hotel Receptionist
* Anthony Naylor - Rugby Player
* John Rhys-Davies - Rugby Player
* Rodney Cardiff - Doctor
* Stephanie Smith - Delphi/Diane as a child
* Peter Salmon - Male model
* Michael Buchanan - Male model

==Critical reception== Time Out noted, "a brilliant opening sequence, otherwise this flat-footed British thriller is hampered by something like the worlds worst script, including flashbacks no one would ever conceivably flash back to, and by a cumbersome storyline about big league stamp trading";  while Sky Movies wrote, "the spirit of the British crime movie of the Fifties lives on in this old-fashioned thriller about the hunt for a rare stamp - the Penny Gold of the title. Jack Cardiff directs with obvious affection for a genre long past but its hard on such distinguished players as Francesca Annis and James Booth not to have more meat on which to bite."  

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 