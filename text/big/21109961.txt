The Four Feathers (1921 film)
 
 
{{Infobox film
| name           = The Four Feathers
| image          =
| caption        =
| director       = René Plaissetty
| producer       =
| based on       =  
| writer         = Daisy Martin
| starring       = Harry Ham Mary Massart Cyril Percival Henry Vibart
| cinematography = Jack E. Cox
| editing        =
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       =  
| runtime        = 5,000 feet 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 silent war novel of on location 1915 American on location in North Africa.  It was reasonably successful on its release. 

==Synopsis==
When a British army officer, Harry Faversham, resigns his commission on the eve of his regiments departure for service in the Sudan he is sent four white feathers of cowardice by his comrades and fiancee. In an attempt to redeem himself, Faversham travels out to the Sudan where he saves the lives of his former comrades.

==Cast==
* Harry Ham as Harry Faversham
* Mary Massart as Ethne Eustace
* Cyril Percival as Jack Durrance
* Henry Vibart as General Faversham
* Tony Fraser as Abou Fatma
* Robert English as Lieutenant Sutch
* Harry Worth as Major Willoughby
* Gwen Williams as Mrs. Adair
* M. Gray Murray as Dermond Eustace
* C. W. Cundell as Lieutenant Trench
* Roger Livesey as Harry as a child

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 