Days of Waiting: The Life & Art of Estelle Ishigo
{{Infobox film
| name = Days of Waiting
| image =
| caption =
| director = Steven Okazaki
| producer = Steven Okazaki Assoc. Producer Cheryl Yoshioka
| writer = Steven Okazaki
| narrator = Dorothy Stroup Additional Narration Lynn ODonnell
| starring =
| music =
| cinematography = Steven Okazaki
| editing = Steven Okazaki Asst. Editor Cheryl Yoshioka
| distributor = Farallon Films Center for Asian American Media
| released =  
| runtime = 28 minutes
| country = United States
| language = English
| budget =
}} documentary short internment camp for Japanese Americans during World War II. The film was inspired by Ishigos book, "Lone Heart Mountain", and won an Academy Award for Documentary Short Subject    and a Peabody Award.

== Background == West Coast concentration camps, Estelle Peck Ishigo refused to be separated from her Nisei (second generation Japanese American) husband. She voluntarily accompanied him to the Heart Mountain War Relocation Center. A painter and illustrator, Ishigo documented her experience through her art. She later published these works and wrote about her experience in her book, "Lone Heart Mountain," which along with personal papers, were the basis of the film. She was discovered living in destitution in her senior years, by the filmmakers as they researched her story.

== Awards ==
* Academy Award for Documentary Short Subject - 63rd Academy Awards (1991)
* Peabody Award – as presentation in PBS TV series, P.O.V. (1991)
* Gran Prix, Clermont-Ferrand International Short Film Festival (1991)

==References==
 

== External links ==
*  
* 
*  by Estelle Ishigo, at the Charles E. Young Research Library, UCLA, via Calisphere.

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 