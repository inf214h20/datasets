The Touch (2002 film)
 
 
 
{{Infobox film
| name           = The Touch
| chinese name   = 天脈傳奇
| caption        = 
| image	=	The Touch FilmPoster.jpeg
| director       = Peter Pau
| producer       = Thomas Chung Michelle Yeoh
| writer         = Julien Carbon Laurent Courtiaud
| starring       = Michelle Yeoh Ben Chaplin Richard Roxburgh
| music          = Basil Poledouris
| cinematography = Peter Pau
| editing        = 
| distributor    = 
| released       = 1 August 2002
| runtime        = 103 minutes
| country        = Hong Kong, Malaysia
| awards         =  English  Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 adventure martial arts film directed by Crouching Tiger Hidden Dragon cinematographer Peter Pau and starring Michelle Yeoh, Ben Chaplin and Richard Roxburgh. It was produced by China Film Co-production Corporation, Han Entertainment, Mythical Films, Pandasia Entertainment and Tianjan Studios, with the  distribution handled by Miramax Films.

Apart from special effects sequences shot in soundstages, the film was shot on-location in the Nepal and Peoples Republic of China|China. Some of the mountain ranges in which the film was shot were not open to filmmakers earlier. 

==Synopsis==
 acrobats who have been performing for many generations. The family are, in secret, guardians of a holy treasure accessible only by a spectacular jump which, to everyone else, is impossible to perform. 

One of the family members (the main characters brother) and his girlfriend are kidnapped by a ruthless treasure hunter (Roxburgh) to procure the priceless relic for him. Yeohs character Pak Yin, with the help of Eric (Chaplin), her master thief ex-boyfriend, pursues them into an ancient desert where legends say the treasure is buried in order to uncover and protect the treasure that her ancestors had sworn to keep safe. The action culminates in a climactic sequence set in the booby-trapped subterranean Buddhist temple.

==Reception==

The film was generally panned by critics for its cliched storyline and primitive visual effects.

==External links and references==
* 

 
 
 
 
 
 

 
 