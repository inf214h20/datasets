Fast Five
 
 
 
{{Infobox film
| name                 = Fast Five
| image                = Fast Five poster.jpg
| caption              = Theatrical release poster
| director             = Justin Lin
| producer             = {{Plain list|
* Neal H. Moritz
* Vin Diesel
* Michael Fottrell
}} Chris Morgan
| based on             =  
| starring             = {{Plain list|
* Vin Diesel
* Paul Walker
* Jordana Brewster
* Tyrese Gibson Chris "Ludacris" Bridges
* Matt Schulze
* Sung Kang
* Dwayne Johnson
 
}} Brian Tyler
| cinematography       = Stephen F. Windon
| editing              = {{Plain list|
* Christian Wagner
* Kelly Matsumoto
* Fred Raskin
}}
| production companies = {{Plain list|
* Original Film
* One Race Films
}}
| distributor          = Universal Pictures
| released             =   
| runtime              = 130&nbsp;minutes 
| country              = United States
| language             = English
| budget               = $125 million 
| gross                = $626.1 million 
}}
 Chris Morgan. It is the fifth installment in The Fast and the Furious|The Fast and the Furious franchise. It was released first in Australia on April 20, 2011, and then in the United States on April 29, 2011. Fast Five follows Dominic Toretto (Vin Diesel), Brian OConner (Paul Walker), and Mia Toretto (Jordana Brewster) as they plan a heist to steal $100&nbsp;million from corrupt businessman Hernan Reyes (Joaquim de Almeida) while being pursued for arrest by Diplomatic Security Service|U.S. Diplomatic Security Service (DSS) agent Luke Hobbs (Dwayne Johnson).

When developing Fast Five, Universal Studios deliberately departed from the street racing theme prevalent in previous films in the series, to transform the franchise into a heist action series involving cars. By doing so, they hoped to attract wider audiences that might otherwise be put off by a heavy emphasis on cars and car culture. Fast Five is considered the transitional film in the series, featuring only one car race and giving more attention to action set pieces such as gun fights, brawls, and the heist of $100&nbsp;million. The production mounted a comprehensive marketing campaign, marketing the film through social media, virtual games, cinema chains, automobile manufacturers, and at NASCAR races.
 Fast & Furious (2009) to become the highest-grossing film in the franchise. Fast Five has grossed over $625&nbsp;million worldwide, making it number 66 on the all-time worldwide list of highest-grossing films, in unadjusted dollars, and the 2011 in film|seventh-highest-grossing film of 2011.

The film was praised by critics, who liked the combination of comedy and "action sequences that toy idly with the laws of physics";  some labeled the film the best of the series.  Johnson was singled out for his performance, critics calling him "the best thing, by far, in Fast Five"  and remarking that scenes shared by Johnson and Diesel were often the "best moments".  Despite the positive response, many were critical of the films running time, considering it too long, and others criticized the treatment of women, stating "  cameo strikingly in buttock form. Others actually have first names."  South American reviewers were critical of the films portrayal of Rio de Janeiro as a haven for drug trafficking and corruption, labeling it a "stereotype".  A sequel, Fast & Furious 6, was released in May 2013 to box office success, surpassing Fast Five as the highest-grossing film in the franchise. Another sequel, Furious 7, released in April 2015, soon surpassed Fast & Furious 6, grossing over $1 billion worldwide.

==Plot==
  Lompoc Prison Rio de Janeiro. Awaiting Doms arrival, Mia and Brian join their friend Vince and other participants on a job to steal three cars from a train. Brian and Mia discover that agents from the Drug Enforcement Administration|U.S. Drug Enforcement Administration (DEA) are also on the train and that the cars are seized property. When Dom arrives with the rest of the participants, he realizes that one of them, Zizi, is only interested in stealing one car, a Ford GT40. Dom has Mia steal the car herself before he and Brian fight Zizi and his henchmen, during which Zizi kills the DEA agents assigned to the vehicles. Dom and Brian are captured and brought to crime lord Hernan Reyes, the owner of the cars and Zizis boss. Reyes orders the pair be interrogated to discover the location of the car, but they manage to escape and retreat to their safehouse.

While Brian, Dom, and Mia examine the car to discover its importance, Vince arrives and is caught trying to remove a computer chip from it. He admits he was planning to sell the chip to Reyes on his own, and Dom forces him to leave. Brian investigates the chip and discovers it contains details of Reyes criminal empire, including the locations of US$100&nbsp;million in cash.

Following the murder of the DEA agents aboard the train, blamed on Dom and his team, DSS agent Luke Hobbs and his team arrive in Rio to arrest Dom and Brian. With the help of local officer Elena Neves, they travel to Doms safehouse, but find it under assault by Reyes men. Brian, Dom and Mia escape, and Dom suggests they split up and leave Rio, but Mia announces she is pregnant with Brians child. Dom agrees to stick together and suggests they steal Reyes money to start a new life. The trio organizes a team to perform the heist, recruiting Han Seoul-Oh, Roman Pearce, Tej Parker, Gisele Yashar, Leo, and Santos. Vince later joins the team after saving Mia from being captured by Reyes men, earning Doms trust once more.

Hobbs and his team eventually find and arrest Dom, Mia, Brian, and Vince. While transporting them to the airport for extradition to the United States, the convoy is attacked by Reyes men, who kill Hobbs team. Hobbs and Elena are saved by Dom, Brian, Mia, and Vince as they fight back against Reyes men and escape, but Vince is shot in the process and dies. Wanting to avenge his murdered team, Hobbs and Elena agree to help with the heist. The gang breaks into the police station where Reyes money is kept and tear the vault from the building using their cars, dragging it through the city with police in pursuit. Believing they cannot outrun the police, Dom makes Brian continue without him while he attacks the police and the pursuing Reyes, using the vault attached to his car to smash their vehicles. Brian returns and kills Zizi while Reyes is badly injured by Doms assault. Hobbs arrives on the scene and kills Reyes. Hobbs refuses to let Dom and Brian go free but, unwilling to arrest them, agrees to give them a 24-hour head start to escape. The gang splits Reyes money, leaving Vinces share to his family, before the members go their separate ways.

On a tropical beach, Brian and a visibly pregnant Mia relax. They are met by Dom and Elena. Brian challenges Dom to a final, no-stakes race to prove who is the better driver.

In a post-credits scene, Hobbs is given a file by Monica Fuentes concerning the hijack of a military convoy in Berlin. In the file, Hobbs discovers a recent photo of Doms former girlfriend Letty Ortiz, who had been presumed dead.

==Cast==
{{multiple image
   | direction = vertical
   | width     = 175
   | footer    = Top to bottom: Diesel, Walker and Brewster reprised their roles from the original The Fast and the Furious (2001).
   | image1    = VinDieselMarch09.jpg
   | alt1      =
   | caption1  =
   | image2    = Paul Walker.jpg
   | alt2      =
   | caption2  =
   | image3    = JordanaBrewsterMarch09cropped.jpg
   | alt3      =
   | caption3  =
  }}
 
* Vin Diesel as Dominic Toretto
:A professional criminal, street racer, and fugitive. Diesel was reportedly paid $15&nbsp;million to star in and produce the film. 
* Paul Walker as Brian OConner
:A former FBI agent turned criminal. He is in a relationship with Mia Toretto. Walker did many of his own stunts for the film, training with parkour professional Paul Darnell to improve his movement. 
* Jordana Brewster as Mia Toretto 
:Dominics sister and the girlfriend of Brian OConner.
* Tyrese Gibson as Roman Pearce
:Brians childhood friend. Gibsons involvement was confirmed on June 30, 2010.  Gibson was committed to   at the time he signed on to Fast Five. He was forced to fly between Puerto Rico and Atlanta to accommodate both films.  Chris "Ludacris" Bridges as Tej Parker
:Brians and Romans friend from Miami. Ludacris confirmed his involvement in the film on July 12, 2010, when he stated he had arrived in Puerto Rico to begin filming. 
* Matt Schulze as Vince
:Dominics childhood friend. Schulze had appeared in the first film, and it was confirmed on July 16, 2010, that he would be returning. 
* Sung Kang as Han Seoul-Oh 
:A street racer, Dominics business partner in the Dominican Republic, and Giseles love interest.
* Dwayne Johnson as Luke Hobbs
:A Diplomatic Security Service agent. According to producer Vin Diesel, the role of Hobbs was originally developed with Tommy Lee Jones in mind. However, when reading feedback on his Facebook page, Diesel noted a fan stating a desire to see Diesel and Johnson in a film together. Diesel and Lin then redesigned the role for Johnson.  Johnson wanted to work with Universal Studios, citing their support for him during his transition from wrestling to acting.  He described the role as a former bounty hunter turned US Marshal  and as "the governments version of the best bounty hunter on the planet".  He undertook an extensive daily workout regime to enlarge his physique, wanting his character to appear as a "hunter" and to be formidable enough to present a credible threat to the protagonists. 
* Gal Gadot as Gisele Yashar
:A former Mossad agent and love interest of Han. Although Gadot had prior experience handling motorbikes, she was required to learn how to ride the larger, more powerful Ducati Streetfighter for the film. 
* Joaquim de Almeida as Hernan Reyes
:A ruthless drug lord posing as a legitimate business man. It was confirmed on July 16, 2010, that Almeida would play antagonist Hernan Reyes.  Having previously played several antagonists, he hesitated to take this role, but accepted it after speaking with Lin and hearing his take on the character. 
* Elsa Pataky as Elena Neves
:A Rio police officer who works with Hobbs team and becomes Dominics love interest. It was confirmed on July 16, 2010, that Pataky would take this role.  She underwent several days of tactical training with a police/military technical advisor and was required to learn how to handle her gun in a variety of situations to portray Elena believably. 
 Fast & Furious.   Eva Mendes appears in an uncredited cameo as agent Monica Fuentes, reprising her role from 2 Fast 2 Furious. 

==Production   ==

===Development=== Chris Morgan and producer Neal H. Moritz would all return to their roles for the new installment.   Moritz said that, following the success of Fast & Furious (2009), which had reunited Diesel, Brewster, Walker and Rodriguez from the original film, the production wanted to bring them back again for the next one. Diesel felt that the story between the characters portrayed by himself and Walker should continue, envisioning it as three chapters, of which Fast Five would be the last. Diesel also wanted to bring back a variety of characters that had been in previous films without interacting, put them together and "have a lot of fun". 

The production had originally intended to film on location in Rio de Janeiro. However, the Puerto Rican government offered tax incentives totaling nearly $11 million, influencing the decision to film there, using Puerto Rico to represent Rio de Janeiro.  
 The French Connection (1971), with Fast Five as the transitional movie.  In April 2011, Universal chairman Adam Fogelson said: 
 Fast Six together for us was: Can we take it out of being a pure car culture movie and into being a true action franchise in the spirit of those great heist films made 10 or 15 years ago? 

Fogelson said that the racing aspect had put a "ceiling" on the number of people willing to see films in the series, and that, by turning it into a series where car driving ability is just one aspect of the film, he hoped to increase the series audience. 

===Writing===
 |style=padding:10px; background-color: #EFEFEF;}}

Lin wanted to explore the elements of "freedom and family" in the film and collaborated with Morgan towards that ideal, both having worked together on previous installments of the franchise. Morgan worked with Diesel to produce a story arc that would further explore and develop Diesels character.  An idea involving heisting a large safe had been conceived by Morgan during the production of Fast & Furious, but that films premise did not work with how Morgan envisioned it.  He later incorporated it into Fast Five. 

===Filming===
On a budget of $125 million,  the shooting of Fast Five was scheduled for July and August 2010.  Shooting had started by July 14, 2010,  but it took longer than forecast: the film was still being shot in early November 2010. 

Three film units worked simultaneously. The main cast were required to travel to Rio at the behest of Lin, who felt it important to understand the area and its culture to give the film a good sense of place.  Diesel agreed that it was important to shoot key scenes in Brazil, commenting "we were able to shoot where other productions might not be able to shoot because our franchise has such good street cred." 
 Ipanema Beach, Dona Marta Christ the Redeemer statue. Establishing shots of the heist team members were taken as each arrived in Rio. Gibson was filmed arriving in character at Rio de Janeiro-Galeão International Airport|Galeão International Airport but, when it became publicly known that a scene was being shot at the airport, the cast and crew were mobbed. A similar situation occurred while Ludacris was shooting a scene in which his character buys a car to drive around the city.  A scene where the completed heist team walk down a beach was filmed in Copacabana (Rio de Janeiro)|Copacabana. 
 San Juan. Isla Verde Hato Rey and Río Piedras, Puerto Rico|Río Piedras districts of San Juan. 

A foot pursuit in which Diesel, Brewster and Walker are chased across favela rooftops by Johnson and his team was filmed over the course of a week in the small hillside town of Naranjito, Puerto Rico. The scene was considered difficult to shoot, as pathways were slippery from moist tropical heat and the scene involved actors and stunt doubles running while avoiding dogs, chickens and other stray animals loose in the area. To capture the scene, a 420-foot cable-camera rig was used to allow for a fast moving, birds-eye view of the action, and cameras on cranes were set up on rooftops and in alleyways.  Walker and Brewster made multiple takes of the conclusion of the scene, requiring them to jump nearly 30 feet from a building onto a waiting safety mat.  In total the production employed 236 technicians, 13,145 extras, and generated 16,824 room nights at hotels, contributing $27 million to the Puerto Rican community. 

Filming moved to Atlanta, Georgia for the final phase.  Wenham and his team transformed a defunct train yard into an abandoned auto plant used by the protagonists as their headquarters.  Redesigning the train yard took place over several months. It was required to allow enough space for stunt drivers to drive into the building, and it had to include an integrated lighting system. The design team removed walls, hauled out old railway cars, suspended rusted car parts and auto-plant car rails, and constructed smaller buildings within the main building to transform the site. With twenty-five pages of script to shoot in a limited time, cinematographer Stephen Windon and his team spent three weeks setting up a series of high-powered, motorized lights in the rafters of the building, that could be controlled remotely to allow lighting to be altered quickly while fully illuminating the set.  An exterior scene involving Diesel and Walker attending a car party, involving several high-performance cars, was filmed near the Georgia Dome.  The train heist scene was filmed in Rice, California over three weeks. 

A brawl scene between Johnsons and Diesels characters was considered difficult to choreograph. The characters were written to be equally formidable, so the fight was punctuated with moments of character development, as Moritz felt this made the fight more exciting. The scene required several weeks of rehearsal and more than a week of filming by the actors and their stunt doubles, who incurred several minor injuries. 

===Vehicle stunts===
  district in Hato Rey, Puerto Rico.]]
The climatic vault heist required four weeks of preparation with every scene and camera angle determined in advance.  The filmmakers hired stunt director Spiro Razatos and stunt coordinator Jack Gill to direct the second-unit action filming.  The pair initially began research for the stunt by testing the capabilities of the prop vaults and the Dodge Chargers driven by Diesel and Walker. Filmed on the streets of Hato Rey,  the chaotic scene demanded specific timing that had to be synchronized with the various character interactions also occurring during the scene. Razatos chose to use a series of camera cars including a crane-mounted camera atop a Porsche Cayenne, which allowed him to film from a variety of angles and heights while the vehicles were in motion, and a Subaru STI with a steel cage built around it that allowed for tracking shots.  The Subarus driver Allen Padelford would occasionally accidentally collide with the vault creating a shower of sparks that inadvertently became useful footage. Padelford also developed a top-mount dual-drive system for the Chargers that allowed a stunt driver to control the vehicle from the roof, while the actor focused on their performance inside the car. 

Six versions of the 8-foot high vault were built, each with specific uses. One of the vaults was a façade built onto the front of a semitruck and was used for filming close shots of the vault destroying street cars. Another vault was a reinforced, four-wheel self-drive vehicle that was connected to 30-foot cables and dragged through the streets of San Juan by the two stunt Dodge Chargers.  The four-ton vault was driven by stunt driver Henry Kingi, who had to wear a temperature controlled suit to compensate for the temperatures within that could exceed 100 degrees.  A scene where the vault tumbled as the cars rounded a corner was a practical effect, and the result was more violent than the filmmakers had anticipated.  Over 200 vehicles were destroyed by the vault during filming.  Several stunts had to be cut including a final scene that would have seen the vault hanging over the edge of the Teodoro Moscoso Bridge. This stunt was abandoned when it was determined that even the powerful Chargers would not be able to support the vaults weight. 

The train heist presented several challenges. The filmmakers were required to effectively purchase a length of working railroad for filming and the trains necessary to ride the tracks. Then trucks had to be built that could race the train and meet the needs of the heist itself. Lin also required that the cars being carried on the train be able to jump out of the train at full speed. The scene took precise execution.  The filmmakers chose to use cars they could cheaply replicate for the train heist—a 1972 De Tomaso Pantera, a 2007 Corvette GS Roadster, and a Ford GT40—to avoid the expense of replacing a genuine $2 million Ford GT40. 

==Marketing==
 -branded, virtual drive-in theater in Car Town, shows the trailer for Fast Five.]]
The first trailer for Fast Five was released on Diesels Facebook page on December 14, 2010, in what was believed to be the first ever use of this marketing approach.    At the time, Diesels page had over 20 million subscribers (one of the top five personal sites of celebrities), providing a wide audience for the trailer.  Following this debut, other members of the cast with personal social media platforms released the teaser trailer to their own fans.  

The Facebook game Car Town by Cie Games and the theater chain Regal Entertainment Group (REG) collaborated with Universal in a cross-media marketing promotion.  Car Town allowed players to view the trailer for the film in an  , in-game drive-in theater and race around a virtual Rio de Janeiro.  The game also featured missions and locations based on the plot of the film, while allowing players to race against Fast Five characters and take part in a bank heist.  REG offered players of Car Town the ability to purchase tickets in-game via Fandango for films at REG theaters.  By buying these tickets in-game, players were given promotional codes which in turn allowed them to unlock a virtual 1970s Dodge Charger, used by Diesels character in the original film.  REG promoted the partnership between the film and the game in their theaters across 37 states, online and through social media, while Universal promoted it via their own Facebook, Twitter and YouTube sites.   In October 2011, it was claimed that over 200 million races had taken place within the virtual Rio de Janeiro environment in the six months since the campaigns April launch. 
 NASCAR Sprint Cup Series throughout April 2011 – the opening month of Fast Five. Dodge also sponsored the world premiere of the film in Rio de Janeiro. 

== Release ==
The premiere of Fast Five took place on April 15, 2011, at the Cinépolis Lagoon theater in Rio de Janeiro, Brazil.  It was hosted by actress Susie Castillo and sponsored by car manufacturer Dodge.  

===Box office=== 2011 film, Fast Six), Universal film.  It achieved a worldwide opening weekend of $109.6 million.  The film reached a peak of number 55 on the list of all-time highest-grossing films worldwide in October 2011.  It became the highest-grossing film of the series in worldwide grosses (as well as separately in the US and Canada, and outside the US and Canada)  but was out-grossed in all three cases by Fast & Furious 6. 

;Outside the United States and Canada
Fast Five is the ninth highest-grossing 2011 film,  the second highest-grossing film of the Fast and Furious franchise (behind Fast 6)  and the fifth highest-grossing Universal film.  It was initially released in Australia on Wednesday, April 20, 2011&nbsp;– nine days before the release date in North America&nbsp;– followed by releases in the UK, South Korea and New Zealand. The earlier start in these countries was timed to coincide with their Easter holidays  and avoided competition from forthcoming summer films,  although this placed it in direct competition with Thor (film)|Thor in some countries. By the end of its opening weekend, the film had accrued a total of $23.4 million from these countries.   On its second weekend, Fast Five earned $46.3 million across 3,139 theaters in 14 countries, ranking first at the box office in each of its ten new markets. For the overall weekend, it ranked second behind Thor.  In the third weekend of release, Fast Five topped the box office in a further 44 countries, playing in a total of 6,979 theaters across 58 countries. It ranked first during the weekend with $85.8 million.   It set an opening-weekend record in the United Arab Emirates ($1.65 million), holding this record for two weeks before being out-grossed by Pirates of the Caribbean: On Stranger Tides ($2.24 million). 

Across all markets, the film scored Universals highest-grossing opening weekend in Russia, Spain, Turkey,  Argentina, Brazil,  Chile, France,  India, Italy,  Malaysia, Mexico,  the Netherlands, Thailand, the United Arab Emirates and Vietnam. 

;United States and Canada
Fast Five is the second highest-grossing film in the Fast and Furious franchise (behind Fast 6),  the sixteenth highest-grossing Universal film,  the sixth highest-grossing 2011 film,  the second highest-grossing heist/caper film, behind Inception,  and the second highest-grossing car-racing film, behind Cars (film)|Cars.  According to Box Office Mojo, Fast Five is one of the most successful sequels of 2011, when taking into account that it is one of few to have outperformed the immediately-preceding instalment of its franchise in the US and Canada. 

Fast Five opened on April 29, 2011, in 3,644 theaters,  It took $3.8 million in midnight showings, setting new records for the Fast and the Furious series and   ). It also achieved the third-largest spring opening, behind The Hunger Games and Alice in Wonderland.   The film also set an opening-weekend record among films starring Diesel, Walker, Brewster, Johnson, Moritz and Lin,  records overtaken in all cases by Fast 6. 

The film opened dropped 62% on its second weekend, earning $32.4 million,  and ranking second behind Thor.  This result was partially attributed to the reduction in IMAX and large-format screens showing the film (reduced from 244  to 20 ), since IMAX only contributed $510,000 to the films second-weekend gross.  On June 4, 2011, 37 days after release in the US, the film became the first of 2011 to accrue more than $200 million.  The film received a one-week re-release in IMAX theaters on September 30, 2011. 

{| class="wikitable" style="width:99%;"
|-
! rowspan="2" style="text-align:center;"| Release date(s)
! rowspan="2" style="text-align:center;"| Budget
! colspan="3" style="text-align:center;"| Box office revenue
! colspan="3" style="text-align:center;" text="wrap"| Box office ranking (current/peak)
|-
! style="text-align:center;"| US & Canada
! style="text-align:center;"| International
! style="text-align:center;"| Worldwide
! style="text-align:center;"| Release year
! style="text-align:center;"| All time US
! style="text-align:center;"| All time  worldwide
|-
|  
| style="text-align:center;"|$125,000,000 
| style="text-align:center;"|$209,837,675 
| style="text-align:center;"|$416,300,000 
| style="text-align:center;"|$626,137,675 
| style="text-align:center;"|#7 / #1  
| style="text-align:center;"|#116 / #103  
| style="text-align:center;"|#66 / #55  
|-
| colspan="9" |  
Note(s)
* Box office ranking accurate as of February 2013.
 
|}

===Critical reception===
  received praise from several critics for his performance.]]
Fast Five received positive reviews from critics. On Rotten Tomatoes, the film has a rating of 78%, based on 192 reviews, with an average rating of 6.4/10. The sites critical consensus reads, "Sleek, loud, and over the top, Fast Five proudly embraces its brainless action thrills and injects new life into the franchise."  On Metacritic, the film has a score of 67 out of 100, based on 29 critics, indicating "generally favorable reviews". 
 The Telegraph Time Out London also commented that the film was too long and criticized the simplistic characters and dialog, but she called the film "slick" and stated that these criticisms could be overlooked because "it doesnt take itself too seriously."  Variety (magazine)|Variety focused on the roles of Johnson and Diesel, lamenting the current lack of 1980s-style "brawny" leading men and of the "manly men" typical of the 1950s and 1960s, and calling their pairing "a welcome injection of tough-guy vigor". Variety commented that, based on Fast Five, a "sixth entry could be something worth waiting for".  The New Yorker called the action scenes "spectacular", praising director Lin by saying his "direction and the sharp editing never confuse or lose momentum", but also found the film too long and criticized the dialog, labeling it "subpar  Oceans Eleven (2001 film)|Oceans Eleven-style banter".   On the characters, The New Yorker considered Walker and Diesel "serviceable", but singled out Johnson for praise for bringing a "hip, comic knowingness to his role ... his enjoyment is infectious and keeps the movie speeding along." 

Total Film welcomed the return of Ludacris and Tyrese Gibson to "  the film with much-needed laughs" and felt that Johnson fit into the established cast with ease, though it believed the film itself was "no mould-breaker."  Peter Travers of Rolling Stone, who disliked the previous movies, gave the film 2.5 stars out of 4, praising the transformation of the series into a heist film ("Damn it, it works"), commenting favorably on scenes between Johnson and Diesel, and judging that "Fast Five will push all your action buttons, and some you havent thought of."   The Los Angeles Times felt that scenes shared by Diesel and Johnson were the "best moments" and appreciated the humor, but considered the pacing a "strange mix", switching between exposition, comedy scenes and then sudden action.   The reviewer echoed other critics sentiments concerning the running time of the film, but concluded that "the sheer audacity of "Fast Five" is kind of breathtaking in a metal-twisting, death-defying, mission-implausible, B-movie-on-steroids kind of way", labeling it the "best" of the series. 

Both Empire and Variety noted that the final chase scene of Fast Five contained allusions to Bad Boys II (2003): Variety stated that the scene "seems inspired in part by a similarly spectacular scene in Bad Boys II";  Empire said that it "nearly out-Bad-Boys-2s Bad Boys 2". 
 Time Out New York stated that "The Fast and the Furious movies havent exactly gotten better as theyve gone along" but gave the director a backhanded compliment, saying "Justin Lin, taking his third turn behind the franchises wheel, is at least a competent hack."   Ebert was more complimentary, saying "Justin Lin is emerging as a first-rate director in this second-rate genre"  and Rolling Stone managed "Justin Lin, who misdirected the last two sequels, finds his pace this time, staging dynamite action."

===Brazilian critics===
 
Brazilian reviewers criticized the use of Rio de Janeiro in the film, claiming it was stereotyped as "dominated by heavily armed drug traffickers, corrupt police, and sexy women".   O Globo accused the producers of using "aerial shots and quick editing" to "deceive the viewer" into believing that the criminal acts take place in Rio.  Globo also reacted negatively to the use of "foreigners" to represent Brazilians, "speaking Portuguese with laughable accents".   Veja (magazine)|Veja agreed with O Globo, saying, "The city of Rio and the Rio Film Commission supported the production. But the image that will spread across the world is exactly what the city doesnt want." 

===Accolades and recognition=== Outstanding Actor in a Motion Picture.  The films sound and music editing was nominated for a Golden Reel Award for Sound Effects and Foley in a Feature Film by the Motion Picture Sound Editors society.  The film also received two Saturn Award nominations from the Academy of Science Fiction, Fantasy & Horror Films for Best Action/Adventure Film and Best Editing for Kelly Matsumoto, Fred Raskin, and Christian Watner. 
 The Artist BitTorrent with approximately 9.3 million downloads.  

{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient!! Result !! Ref.
|-
|rowspan="9"| 2011
||  BMI Film Music Award
| Film Music Brian Tyler
| 
| style="text-align:center;"|   
|-
|rowspan="3"|  Golden Trailer Awards
| Best Action TV Spot
| "Fast Women" – Universal Pictures, AV Squad
| 
|rowspan="3" style="text-align:center;"|  
|-
| Best Summer 2011 Blockbuster Poster
| "Fast Five" – Universal Pictures, Cold Open
| 
|-
| Best Summer Blockbuster 2011 TV Spot
| "Superbowl" – Universal Pictures, AV Squad
| 
|-
|rowspan="5"|  Teen Choice Awards Choice Movie – Action
| Fast Five
|  
| style="text-align:center;"|   
|- Choice Movie Actor – Action
| Vin Diesel
|  
| rowspan="4" style="text-align:center;"|  
|-
| Dwayne Johnson
|  
|-
| Paul Walker
|  
|- Choice Movie Actress – Action
| Jordana Brewster
|  
|-
|rowspan="7"| 2012
|rowspan="2"| 38th Peoples Choice Awards|Peoples Choice Awards
| Favorite Action Movie
| Fast Five
| 
| rowspan="2" style="text-align:center;"|   
|-
| Favorite Action Movie Star
| Vin Diesel
| 
|-
|rowspan="1"| 17th Critics Choice Awards|Critics Choice Awards
| Best Action Movie
| Fast Five
| 
| rowspan="1" style="text-align:center;"|  
|-
|rowspan="1"| NAACP Image Award Outstanding Actor in a Motion Picture
| Vin Diesel
| 
| rowspan="1" style="text-align:center;"|  
|- Golden Reel Awards
| Sound Effects and Foley in a Feature Film
| Fast Five
| 
| rowspan="1" style="text-align:center;"|  
|-
|rowspan="2"| Saturn Awards Best Action/Adventure Film
| Fast Five
| 
| rowspan="2" style="text-align:center;"|  
|-
| Best Editing
| Kelly Matsumoto, Fred Raskin, and Christian Watner
| 
|-
|}

===Home media===
On August 2, 2011,   5.1 sound. A triple pack was also released containing a Blu-ray, DVD, and digital copy of the film in either Blu-ray or DVD packaging.    The Blu-ray versions contains several exclusive additional features, including behind-the-scenes footage, cast and crew interviews, a "virtual car garage" that provides further details on the vehicles used in the production, and music tracks from the film.  Both the DVD and the Blu-ray contain a theatrical and extended cut of the film,  director commentary, deleted scenes, a gag reel and features on the three central characters.  Commenting on the extended cut, Lin said "this is the version that I prefer."  During first week sales in the United States the DVD was the number 1 selling DVD, the number 1 rental DVD,  and the number 2 selling Blu-ray disc behind the Blu-ray re-release of The Lion King.  57% of the total first week disc sales of Fast Five were the Blu-ray disc version. 

To promote the release of the DVD and Blu-ray, Universal Studios Home Entertainment sponsored the first race of the NASCAR Camping World Truck Series, at the Chicagoland Speedway.  The event, renamed as the "Fast Five 225", took place on September 16, 2011, with Gibson and Brewster as Grand Marshals; Gibson sang the American national anthem for the event and Brewster acted as honorary starter.  The event served as the first race in the Chase for the Sprint Cup and was won by Austin Dillon.   The deal marked the first time that a film promotion had been allowed to take over a NASCAR race as a title sponsor.   Continuing the partnership with Car Town, the game was used as the exclusive means of pre-ordering the Blu-ray/DVD combo release at Walmart, via players clicking on a Walmart-themed truck, which in turn provided the player with Fast Five branded in-game rewards. 

==Soundtracks==
  Slaughterhouse and Claret Jai for the soundtracks lead single, "Furiously Dangerous". 

The film score was released on May 3, 2011, by Varèse Sarabande.  The release has 25 tracks and plays for 78&nbsp;minutes.   The score spent four weeks on the Billboard 200|Billboard 200, peaking as high as number 60 and reaching number 24 on the digital albums chart and number 5 on the soundtrack-only chart. 

==Sequel==
 
A sixth film in The Fast and the Furious series was planned in February 2010 as development on Fast Five began, and in April 2011, it was confirmed that Morgan had begun work on a script for the potential sixth film.    Diesel and Moritz returned as producers for the film and Lin would return to direct. Fast & Furious 6 was released on May 24, 2013. 

==References==
{{Reflist|30em|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

 {{cite web |last=Mottram|first=James|url=http://www.totalfilm.com/reviews/cinema/fast-five|title=Fast Five Like Pixars Cars, but more cartoony. archivedate = July 5, 2011| deadurl=yes}} 

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

}}

Documents
* }}

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 