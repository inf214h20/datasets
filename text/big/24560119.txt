The Dance of Life
 Dance of Life}}
{{Infobox film
| name           = The Dance of Life
| image          = Poster of the movie The Dance of Life.jpg
| image_size     = 
| alt            = 
| caption        =  John Cromwell A. Edward Sutherland
| producer       = 
| writer         = Benjamin Glazer (screenplay) Arthur Hopkins (play "Burlesque") Julian Johnson George Manker Watters (play "Burlesque")
| narrator       = 
| starring       = Hal Skelly Nancy Carroll Dorothy Revier Ralph Theodore
| music          = Adolph Deutsch Vernon Duke John Leipold
| cinematography = J. Roy Hunt
| editing        = George Nichols Jr.
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  	
| runtime        = 115 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Swing High, When My Baby Smiles at Me (1948). 
 Astoria Studios John Cromwell and A. Edward Sutherland. 
 public domain (in the USA) due to the claimants failure to renew its copyright registration in the 28th year after publication. 

==Cast==
*Hal Skelly -  "Skid" Johnson
*Nancy Carroll - Bonny Lee King
*Dorothy Revier - Sylvia Marco
*Ralph Theodore - Harvey Howell
*Charles D. Brown - Lefty
*Al St. John - Bozo
*May Boley - Gussie
*Oscar Levant - Jerry
*Gladys DuBois - Miss Sherman James Quinn - Jimmy Jim Farley - Champ Melvin George Irving - Minister
*Gordona Bennet - Amazon Chorus Girl
*Miss La Reno - Amazon Chorus Girl
*Cora Beach Shumway - Amazon Chorus Girl
*Charlotte Ogden - Amazon Chorus Girl
*Kay Deslys - Amazon Chorus Girl
*Magda Blom - Amazon Chorus Girl
*Thelma McNeil - Gilded Girl (as Thelma McNeal) John Cromwell - Doorkeeper
*A. Edward Sutherland - Theater Attendant

==Plot==
Burlesque comic Ralph Skid Johnson (Skelly), and dancer Bonny Lee King (Carroll), end up together on a cold, rainy night at a train station, when hes thrown out and shes rejected from the same show.

The two things they have in life are dancing and each other, if she could only keep him away from the booze, long enough to keep dancing.
 All That Jazz, from an earlier era.

==Soundtrack==
*"True Blue Lou"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
:Sung by Hal Skelly
*"The Flippity Flop"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
*"King of Jazzmania"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
*"Ladies of the Dance"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
*"Cuddlesome Baby"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
*"Mightiest Matador"
:Music by Richard A. Whiting
:Lyrics by Sam Coslow and Leo Robin
*"Sweet Rosie OGrady"
:Written by Maude Nugent
*"In the Gloaming"
:Music by Annie Fortescue Harrison
:Lyrics by Meta Orred
*"Sam, the Old Accordion Man"
:Written by Walter Donaldson

==Preservation status==
No color prints survive, only black-and-white prints made in the 1950s for TV broadcast.

==See also==
*List of early color feature films

==References==
 

==External links==
* 
* 
* 
*  at AMCTV.com
*  at Yahoo
*  at OVGuide.com
* 
*  at The New York Times

 
 

 
 
 
 
 
 
 