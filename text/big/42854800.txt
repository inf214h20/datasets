The Impersonator
{{Infobox film
| name           = The Impersonator
| image          = "The_Impersonator"_(1960).jpg
| caption        = Theatrical poster for 1962 continental release
| director       = Alfred Shaughnessy
| producer       = Anthony Perry
| writer         = Kenneth Cavander  Alfred Shaughnessy John Crawford Jane Griffiths
| music          = De Wolfe
| cinematography = John Coquillon John Bloom
| studio         = Eyeline Productions Herald
| distributor    = Bryanston Films (UK)
| released       =  
| runtime        = 64 minutes
| country        = United Kingdom
| language       = English
| preceded_by    =
| followed_by    =
}}
 John Crawford were incorporated to give this second feature some transatlantic box office appeal.   

==Plot==
In an effort to improve relations between a US air force base and the sleepy local English village nearby, airman Sergeant Jimmy Bradford  organises a school trip to see the pantomime "Mother Goose". Meanwhile a prowling killer is on the loose and after a night out with the victim, the finger of suspicion points at Bradford. 

==Cast== John Crawford Jane Griffiths
*Mrs Lloyd -	 Patricia Burke
*Harry Walker -	 John Salew
*Tommy Lloyd -	 John Dare
*Detective Superintendent Fletcher -	 John Arnatt
*Principal boy in "Mother Goose" -              Yvonne Ball
*Colonel -  Edmund Glover

==Critical reception==
*Writing in The Radio Times, David Parkinson gave the film two out of  five stars, saying, "its shoestring stuff, but still better than most British B-movies." 
* Britmovie called the film an "excellent British B-thriller produced by Bryanston on a budget of £23.000 that is a cut above the majority of second features." 

==References==
 

==External links==
*  

 
 
 
 
 
 