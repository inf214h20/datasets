Rhythm of the Wave
 
 
 Taiwanese film spoken in Mandarin language, first released in 1974 by Cine Art Film Company ( ).

;Cast
* Josephine Siao Chin Han
* Jenny Hu
* Ko Chun-hsiung ( )

;Crew
* Ho Yu-ye ( ), executive producer
* Li Hsing, director
* Chang Yung-hsiang ( ), script

==Soundtrack==
Hai yun ( ) is a soundtrack album, released in 1974 by Lee Fung Records ( ). Unless otherwise, tracks are sung in the film by Teresa Teng, music was arranged by Lin Chia-ching ( ), lyrics were written by Chuang Nu ( ), and songs were composed by Ku Yue ( ).

;Side A
# "Hai yun" ( ) – main theme
#: Re-recorded by Polydor Records for the 1977 compilation Greatest Hits by Teresa Teng
# "Remember You, Remember Me" (  "Jide ni jide wo") – sub-theme
# "Hey! I Tell You" (  "Hai! Wo gaosu ni") – sub-theme
# "Warmth" (  "Wennuan"), sung by Yuanye Sanchong Chang ( )
#: sub-theme of the 1974 film Where the Seagull Flies ( )
# "First Love (keyboard instrumental)" (  "Chulian–dianziqin yanzou")

;Side B
# "Hey! I Tell You" (  "Hai! Wo gaosu ni") – sub-theme, rendered by Wan Sha-lang ( )
# "Three Dreams" (  "San ge meng") – sub-theme
# "First Love" ( ) – sub-theme, sung by Yuanye Sanchong Chang ( )
# "Cherish the Flowers" ( ) – sub-theme
# "Hai yun (violin instrumental)" (  "Hai yun–xiaotiqin yanzou")

== External links ==
*   at Lee Hsing exhibitions
*  
*  

 
 

 
 
 
 
 
 
 


 