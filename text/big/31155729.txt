Mulkireedam
{{Infobox film 
| name           = Mulkireedam
| image          =
| caption        =
| director       = NN Pisharady
| producer       = NN Pisharady
| writer         = Kaladi Gopi
| screenplay     = Sharada Adoor Bhasi P. J. Antony Sankaradi
| music          = Prathap Singh
| cinematography =
| editing        =
| studio         =
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed and produced by NN Pisharady. The film stars Sharada (actress)|Sharada, Adoor Bhasi, P. J. Antony and Sankaradi in lead roles. The film had musical score by Prathap Singh.   

==Cast== Sharada
*Adoor Bhasi
*P. J. Antony
*Sankaradi
*T. R. Omana
*Indira Thampi
*N. Govindankutty
*Nellikode Bhaskaran
*S. P. Pillai

==Soundtrack==
The music was composed by Prathap Singh and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deva Yeshunaayaka || Thambi || P. Bhaskaran || 
|-
| 2 || Kanakaswapna || S Janaki || P. Bhaskaran || 
|-
| 3 || Kookaatha Poonkuyile || Thambi || P. Bhaskaran || 
|-
| 4 || Kulikazhinju || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 