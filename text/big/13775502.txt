Cloistered Nun: Runa's Confession
{{Infobox film
| name           = Cloistered Nun: Runas Confession
| image_size     = 190px
| image          = Cloistered Nun Runas Confession.jpg
| caption        = Theatrical poster 
| director       = Masaru Konuma
| producer       = 
| aproducer      =
| writer         = 
| starring       = Luna Takamura Kumi Taguchi
| music          = 
| cinematography = 
| editing        = 
| distributor    = Nikkatsu
| released       = January 8, 1976 (Japan)
| runtime        = 74 minutes
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         = 
}}

  (1976) is a Japanese pink film in the nunsploitation genre starring pop singer Luna Takamura, directed by Masaru Konuma and produced by Nikkatsu. AllRovi reports of the film, "As Konuma is not one to pull political punches for the sake of lightweight erotic fluff, the film is far more pointed than the numerous European sexy nun films it ostensibly resembles."  Jasper Sharp calls it "a typically profane offering in the most peculiar of Nikkatsus pornographic subgenres". 

==Synopsis==
Young Runa/Luna entered a convent after losing her boyfriend to her sister Kumi. Her dream of a tranquil life at the convent is shattered when she is raped by the abbot and introduced to a life of torture and abuse. Leaving the convent she returns to her sister and former boyfriend to exact revenge by snaring them in a bogus financial scheme to promote Christianity in Japan and then departing with their money and a lesbian lover from the convent.

==Cast==
* Luna Takamura as Runa/Luna
* Aoi Nakajima
* Kumi Taguchi as Kumi
* Roger Prince as the Abbot
* Yōko Azusa
* Shin Nakamaru

==Availability== region 1 DVD release of Cloistered Nun: Runas Confession with English subtitles was released by KimStim on July 1, 2013. 

==References==
 

== Sources ==
*  
*  
*  

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 
 