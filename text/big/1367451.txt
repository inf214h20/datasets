Greystoke: The Legend of Tarzan, Lord of the Apes
 
 
{{Infobox film
| name           = Greystoke: The Legend of Tarzan, Lord of the Apes
| image          = Greystoke.jpg
| caption        = Theatrical release poster
| director       = Hugh Hudson
| producer       = Hugh Hudson Stanley S. Canter
| writer         = Robert Towne (as P.H. Vazak) Michael Austin
| based on       =  
| starring = {{Plainlist|
* Ralph Richardson
* Ian Holm
* James Fox
* Christopher Lambert
* Andie MacDowell
}}  John Scott
| cinematography = John Alcott
| editing        = Anne V. Coates
| distributor    = Warner Bros.
| released       =  
| runtime        = 143 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = $45,858,563
}}
Greystoke: The Legend of Tarzan, Lord of the Apes is a 1984 British film directed by Hugh Hudson and based on Edgar Rice Burroughs novel Tarzan of the Apes (1912). Christopher Lambert stars as Tarzan (though the name Tarzan is never used in the films dialogue) and Andie MacDowell as Jane Porter (Tarzan)|Jane; the cast also includes Ralph Richardson, Ian Holm, James Fox, Cheryl Campbell, and Ian Charleson.
 Best Actor Best Writing, Best Makeup. Disney animated Best Original Song for "Youll Be in My Heart").

==Plot==
John, Lord Clayton (Paul Geoffrey), the heir to The 6th Earl of Greystoke, and his wife Alice (Cheryl Campbell) set sail and are quickly shipwrecked on the African coast. After John (or Jack) builds a home in the trees, Alice gives birth to a son. Soon after, Alice grows delirious and dies. When Lord Clayton finds her recently dead, the tree house is visited by curious gorillas, and John is killed by one of the apes. The female of the group, Kala (Tarzan)|Kala, carries her dead infant, but upon hearing the cries of the infant human in his crib, she adopts the boy and raises him among the family of Mangani. The little human child thus grows up naked, Wildness|wild, and Liberty|free.

At age 5, the boy (Danny Potts) is still trying to fit in with his ape family. When a black panther attacks, he learns how to swim to evade it while another gorilla is killed.

At age 12, the boy (Eric Langlois) discovers the tree-house in which he lived as a baby with his mother and father and finds a wooden block, with pictures of both a boy and a chimpanzee painted on it. It is there, after seeing himself in a mirror, that the physical difference between him and the rest of his ape family is made more apparent. He later discovers his fathers hunting knife and how it works. The objects fascinate the naked ape boy, who carries them with him.
 natives and kills one of them. He is intensely aggrieved by his mothers death and cannot reconcile this event, wailing and howling forlornly at the sky.

Years later, a Walloon people|Walloon, Philippe DArnot (Ian Holm), leads a band of British adventurers along the river, though he is disgusted by their boorish nature and love of blood and sport. A band of natives attack the party, killing everyone except Philippe, who is injured and conceals himself in the trees. The now half-naked man (Christopher Lambert) finds Philippe and nurses him back to health. DArnot discovers that the man is a natural mimic and teaches him to speak rudimentary English. DArnot deduces that this man is the son of the late Lord John and Lady Alice of Greystoke and calls the man "Jean" (the French version of John). Jean agrees to return to England with his benefactor and reunite with his human family.
 Lowlands of Scotland, John is welcomed by his grandfather, The 6th Earl of Greystoke (Sir Ralph Richardson), and his ward, a young American woman called Jane (played by Andie MacDowell and voiced by Glenn Close). The Earl is now elderly and has obviously suffered from the loss of his son and daughter-in-law years earlier, displaying eccentric behaviour and sometimes forgetting that John is his grandson, not his son returned.

John is seen as a novelty by the local social set, and some of his behaviour is seen as threatening and savage. He befriends a young mentally disabled worker on the estate and in his company relaxes into his natural behaviour.

Jane teaches John more English, French, and social skills (such as table manners and dancing), and the two become very close, making love one evening in secret.

Lord Greystoke seems to enjoy renewed vigor at the return of his grandson and, reminiscing about his childhood game of using a silver tray as a toboggan on a large flight of stairs in the grand house, decides to relive the old pastime. He crashes at the foot of the stairs and slowly dies, apparently from a head injury, in the arms of his grandson. At his passing, John displays similar emotion and lack of understanding about death as he did in Africa following the death of his adoptive mother.
 Natural History Museum in South Kensington in London with Jane. During their visit, John is disturbed by the crude displays of stuffed animals. He discovers many caged apes from Africa, including his adoptive ape father, Kerchak.

The two recognise one another, and John breaks open the cage and escapes with Kerchak before releasing other caged animals, pursued by police and museum officials. They make it to a woodland park, but when Kerchak climbs a tree to avoid capture, he is fatally shot and John is devastated to lose yet another loved one, yelling to the crowd, "He was my father!" That night, John, 7th Earl of Greystoke, rides his carriage in circles in front of his country house, howling to the sky and wailing "Father!" 

Feeling that he cannot assimilate to the cruel nature of human society, John decides to return to Africa and reunite with his gorilla family. Philippe and Jane escort him back to Africa and to the jungle where Philippe and John first met. There, John changes back into his loincloth, then returns to the world and life he understands. Jane does not join him, but Philippe expresses his hope that perhaps they may someday be reunited.

==Cast== John Clayton / Tarzan, Lord of the Apes (later succeeds his grandfather in the Peerage, becoming The 7th Earl of Greystoke)
** Tali McGregor – Infant Tarzan
** Peter Kyriakou – One Year Old Tarzan
** Danny Potts – Five Year Old Tarzan
** Eric Langlois – Twelve Year Old Tarzan
* Ralph Richardson – The 6th Earl of Greystoke
* Ian Holm – Capitaine Philippe DArnot
* James Fox – Lord Charles Esker Jane Porter
** Glenn Close – Jane Porter (voice, uncredited)
* Cheryl Campbell – Alice, Lady Clayton
* Ian Charleson – Jeffson Brown Major Jack Downing
* Nicholas Farrell – Sir Hugh Belcher
* Paul Geoffrey – John (Jack), Viscount Clayton Captain Billings
* Hilton McRae – Willy
* David Suchet – Buller John Wells – Sir Evelyn Blount
* Paul Brooke – The Rev. Stimson

===Ape puppeteers=== John Alexander – White Eyes Christopher Beck – Droopy Ears
* Ailsa Berk – Kala Peter Elliott – Silverbeard
* Mak Wilson – Figs

==Production==
In a departure from most previous Tarzan films, Greystoke returned to Burroughs original novel for many elements of its plot. It also utilized a number of corrective ideas first put forth by science fiction author Philip José Farmer  in his mock-biography Tarzan Alive, most notably Farmers explanation of how the speech-deprived ape man was later able to acquire language by showing Tarzan to be a natural mimic. According to Burroughs original concept, the apes who raised Tarzan actually had a rudimentary vocal language, and this is portrayed in the film.

Greystoke rejected the common film portrayal of Tarzan as a simpleton that was established by Johnny Weissmullers 1930s renditions, reasserting Burroughs characterisation of an articulate and intelligent human being, not unlike the so-called "new look" films that Sy Weintraub produced in the 1960s.

The second half of the film departs radically from Burroughs original story. Tarzan is discovered and brought to Scotland, where he fails to adapt to civilization. His return to the wild (having already succeeded his grandfather as Lord Greystoke) is portrayed as a matter of necessity rather than choice, and he is separated forever from Jane, who "could not have survived" in his world.

In his book Harlan Ellisons Watching, Harlan Ellison explains that the films promotion as "the definitive version" of the Tarzan legend is misleading. He details production and scripting failures which in his opinion contribute to the films inaccuracy. 

The film was shot in Korup National Park in western Cameroon and in Scotland. Several great houses in the UK were used for the Greystoke family seat:  Kelso in Roxburghshire, Scotland, is used for the exterior and ballroom scenes,
* Hatfield House in Hertfordshire (entrance hall, grand staircase),
* Blenheim Palace in Oxfordshire.
 Personal Best. Oscar nomination for best adapted screenplay.
 dubbed in post-production by Glenn Close. This was due to MacDowells southern US accent, apparently deemed unsuitable for the character, but was not done to provide an English accent for her character as some have held. The young Jane featured at the beginning of the film is portrayed as American, which is consistent with Burroughs.

Both Danny Potts (Tarzan, aged 5) and Eric Langlois (Tarzan, aged 12) were completely nude in all of their scenes.
 Oscar nomination Best Supporting Actor. The film was dedicated to his memory.

Several actors from  , Ian Holm, Cheryl Campbell, Nicholas Farrell, Nigel Davenport, and Richard Griffiths.

Though the name Tarzan is in the films title, it is not mentioned at all in the film itself. The Ape Man is referred to either as Lord Greystoke or John.

==Reception==
The film received a mixed-to-positive reaction, scoring a 64% "Fresh" Rating on Rotten Tomatoes, albeit scoring lower with audiences with a score of 60%. Neal Gabler described the film as visually beautiful, but devoid of thematic or intellectual value. 

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 