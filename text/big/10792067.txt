Tiger (1979 film)
{{Infobox film|
| name = Tiger
| image = Tigertelugufilm.jpg
| caption = DVD Cover
| director =N.Ramesh
| writer =
| starring = N. T. Rama Rao Rajinikanth Radha Saluja Anjali Devi Subhashini
| producer = P.Gangadhara Rao
| music = Sathyam
| editor =
| released = 5 September 1979
| runtime = Telugu
| budget =
}}

Tiger is a Telugu film directed by N.Ramesh and produced by P.Gangadhara Rao. It was the first time N. T. Rama Rao and Rajinikanth starred together. It was also Rajinikanths 50th film in his career.

==Plot==
NTR and Rajini are siblings separated during childhood. NTR lives with his mother (Anjali Devi) in a village while, Rajini works becomes a policeman. Meanwhile, Rajini is employed by Prabhakar Reddy to kill NTR. While attempting to kill NTR, a serious fight takes place and both come to find out they are siblings. In the end they both kill Prabhakar Reddy, who is the main villain and reunite with their mother, played by Anjali Devi. Radha Saluja and Subhashini play the love interests of N. T. Rama Rao and Rajinikanth respectively.

==Cast==
* N. T. Rama Rao
* Rajinikanth
* Radha Saluja
* Subhashini
* Anjali Devi
* Allu Ramalingaiah

==External links==
*  

 
 
 
 


 