Mera ur kärlekens språk
{{Infobox film
| name           = Mera ur kärlekens språk
| image          = Mera ur kärlekens språk.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Torgny Wickman
| producer       = Inge Ivarson
| screenplay     = Torgny Wickman Inge Hegeler Sten Hegeler
| narrator       = 
| starring       = 
| music          = 
| cinematography = Lasse Björne
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Sweden
| language       = Swedish
| budget         =  SEK 3,749,000 (Sweden)
}} Swedish sex educational film directed by Torgny Wickman. It is a sequel to the 1969 film Language of Love  and had a sequel in 1971, Kärlekens XYZ.  In 1973 the three films were edited together into a new film, Det bästa ur Kärlekens språk-filmerna ("The Best from the Language of Love Films"). 

The film dealt more with alternative sexuality and life styles and the disabled but was equally successful financially as Language of Love.

==Cast==
* Maj-Briht Bergström-Walan
* Inge Hegeler
* Sten Hegeler
* Bertil Hansson
* Johan Wallin
* Bengt Lindqvist
* Bengt Berggren
* Bruno Kaplan
* Tommy Hedlund
* Anna Berggren
* Mirjam Israel
* Ove Alström
* Göran Bergstrand
* Curt H:son Nilsson
* Lars Lennartsson
* Suzanne Hovinder
* Mogens Jacobsen
* Rune Pär Olofsson
* Annakarin Svedberg
* Wenche Willumsen
* Lars Ljungberg
* Helena Rohde
* Bent Rohweder
* Lasse Lundberg

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 
 