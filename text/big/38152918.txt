Sans laisser de traces
{{Infobox film
| name           = Sans laisser de traces
| director       = Grégoire Vigneron
| image	=	
  | image_size     = 
  | caption        = 
| producer       = Marc Missonnier Olivier Delbosc
| writer         = Laurent Tirard Grégoire Vigneron
| narrator =  Benoît Magimel
| starring       = Benoît Magimel Julie Gayet François-Xavier Demaison
| music          = Christophe La Pinta
| cinematography = Laurent Dailland
| editing= Valerie Deseine
| studio         = Fidélité Films Scope Pictures   Wild Bunch     Mongrel Media
| released       = 10 March 2010
| runtime        = 95 minutes 
| country        = France Belgium
| language       = French English
| gross          = $1,160,447 
}}
Sans laisser de traces (also known as Immaculate, Traceless, Indelible and Indélébile ) is a 2010 French Crime film and Grégoire Vignerons directorial debut.  

==Plot== French company which employs 15;000 people. Some days after they have discussed their according speeches, Etienne also learns that his beautiful wife has finally become pregnant. In spite of his apparently being so lucky, Etienne is haunted by nightmares.  chemical formula from an unknown inventor 15 years ago.  
Patrick persuades Etienne he could fix everything retroactively if he met the inventor and spoke to him. Unfortunately the man reacts in way neither of the two friends anticipated. Overwhelmed by wrath he is determined to destroy Etiennes life by suing him and eventually he poses even physically threatening. Trying to protect his friend Patrick accidentally kills the man. It is also Patrick who decides for both of them to conceal what just happened. 
Etienne pays Patrick 30,000 Euros in order to get rid of him. But Patrick spends his new money on a prostitute and gets in trouble when he claims she had stolen from him. After Etienne has bailed him out he prepares to send Patrick far away. He creates a well-paid job for him in Singapore. On his way there Patrick gets arrested at the airport for smuggling drugs and his finger prints are taken. They match the fingerprints which were found at the crime scene. 
Patrick blackmails Etienne, demanding 3 million Euros. Etienne cannot get the sum together. Subsequently he decides to turn himself in.

==Cast==
*Benoît Magimel: Etienne
*Julie Gayet: Clémence
*François-Xavier Demaison: Patrick Chambon
*Léa Seydoux: Fleur
*Stephane De Groodt: Kazinski
*André Wilms: François
*Dominique Labourier: Micheline
*Jean-Marie Winling: Maurice

==Reception==
Iwan Pranowo of   evaluated Sans laisser de traces 
as a "nice surprise" but also belittled it as merely a French version of Match Point. {{Cite web|url= http://www.variety.com/review/VE1117942445/?refcatid=31 
|title= This movie here is really a nice surprise. Although actually, if Im to be very critical about it, I would call Traceless as unashamed rip-off/copycat of a Woody Allens Match Point (2005) which features Scarlett Johansson and won the Academy Award 2006 for Best Original Screenplay!|accessdate=2013-01-07}}   Variety derided 
the film as "a paint-by-numbers tale of greed, betrayal and seduction" 
which was "marginally saved by first-rate acting and production values" 
 {{Cite web|url= http://www.variety.com/review/VE1117942445/?refcatid=31 
|title= a paint-by-numbers tale of greed, betrayal and seduction thats only marginally saved by first-rate acting and production values|accessdate=2013-01-07}} 

==DVD Release== subtitles has been published by Mongrel Media in 2010.  

==References==
 

==External links==
*  
*  
*  
*  
*  at Cineuropa 
*   

 
 
 
 
 
 
 
 
 