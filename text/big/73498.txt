Fury (1936 film)
{{Infobox film
| name            = Fury
| image           = Fury poster.jpg
| image_size      =
| caption         = French theatrical poster
| director        = Fritz Lang
| writer          = Bartlett Cormack Fritz Lang
| starring        = Sylvia Sidney Spencer Tracy Bruce Cabot
| producer        = Joseph L. Mankiewicz
| music           = Franz Waxman
| cinematographer = Joseph Ruttenberg Frank Sullivan
| distributor     = Metro-Goldwyn-Mayer
| released        =  
| runtime         = 92 minutes
| country         = United States
| language        = English
| budget          =$604,000  . 
| gross           = $1.3 million James Curtis, Spencer Tracy: A Biography, Alfred Knopf, 2011 p292. 
}} lynched and Edward Ellis and Walter Brennan. Loosely based on the events surrounding the Brooke Hart murder, the movie was adapted by Bartlett Cormack and Lang from the story Mob Rule by Norman Krasna. Fury was Langs first American film.

==Plot==
  as Joe Wilson]]
 Edward Ellis) refuses to give up his prisoner, the enraged townspeople burn down the building, two of them also throwing dynamite into the flames as they flee the scene.  Unknown to anyone else there, the blast frees Wilson, but kills his little dog Rainbow, who had run in to comfort him in the cell.

The district attorney (  footage of twenty-two people caught in the act.

However, Katherine is troubled by one piece of evidence. The defense attorney had tried to get his clients off by claiming that there was no proof Joe was killed, but an anonymous letter writer had returned a partially melted ring belonging to Joe. Katherine notices that a word is misspelled just as Joe used to spell it.

She discovers that Joe escaped the fire and that Joes brothers are helping him get his revenge by framing the defendants for his murder. She goes to see Joe and pleads with him to stop the charade, but he is determined to make his would-be killers pay. However, his conscience starts preying on him and, in the end, just as the verdicts are being read, he walks into the courtroom and sets things straight.

==Cast==
*Sylvia Sidney as Katherine Grant
*Spencer Tracy as Joe Wilson
*Bruce Cabot as Kirby Dawson
*Walter Abel as District Attorney Edward Ellis as Sheriff
*Walter Brennan as "Bugs" Meyers
*Frank Albertson as Charlie
*George Walcott as Tom Arthur Stone as Durkin
*Morgan Wallace as Fred Garrett
*Gwen Lee as Mrs. Fred Garrett
*George Chandler as Milton Jackson Roger Gray as Stranger
*Edwin Maxwell as Vickery
*Howard C. Hickman as Governor
*Jonathan Hale as Defense Attorney
*Leila Bennett as Edna Hooper
*Esther Dale as Mrs. Whipple
*Helen Flint as Franchette Frederick Burton as Judge Daniel Hopkins

==Production==
Norman Krasna was inspired to write the story after reading about a lynching in The Nation. He pitched the idea to Samuel Marx and Joseph Mankiewicz at MGM who were attracted to it. Krasna says he never actually wrote the story down, he verbally pitched it to Mankiewicz who then dictated it. *McGilligan, Patrick, "Norman Krasna: The Woolworths Touch", Backstory: Interviews with Screenwriters of Hollywoods Golden Age, University of California Press,1986 p218-219  A number of changes were made from Krasnas story to the final script. Hollywood
By Sidney Skolsky. The Washington Post (1923-1954)   08 June 1936: 14. 

Fury was Langs first American film, and is considered by critics to have been compromised by the studio, which forced Lang to tack on a reconciliation between Tracys character and his girlfriend at the end.  The film was a major departure for MGM, which at the time was known for lavish musicals and glitzy dramas &ndash; the expensive production features expansive and stylised sets to create its gritty world and its style is more in keeping with the social issue films associated with Warner Brothers, such as I Am a Fugitive from a Chain Gang. 
 The Wizard of Oz.

==Reception== Academy Award Best Writing, Original Story. In 1995, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being deemed "culturally, historically, or aesthetically significant."

The movie was very popular, earning domestic rentals of $685,000 and $617,000 overseas.  According to MGM records, the final profit was $248,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 219 

==Notes==
 

==External links==
 
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 