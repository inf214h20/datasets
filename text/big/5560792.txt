Pinocchio 3000
{{Infobox film
| name = Pinocchio 3000
| image = Pinocchio 3000.jpg
| caption =
| director = Daniel Robichaud
| producer = Louis Duquet
| writer = Claude Scasso Peter Svatek
| starring = Malcolm McDowell Whoopi Goldberg Howie Mandel Helena Evangeliou Gabrielle Elfassy Howard Ryshpan
| music = James Gelfand
| cinematography =
| editing = Claudette Duff
| studio = CinéGroupe Filmax
| distributor = 
| released =  
| runtime = 79 minutes
| country = Canada
| language = English
| budget =$12 million
| gross = $15,485,280
}}
Pinocchio 3000 (or P3K, Pinocchio the Robot) is a 2004 Canadian computer-animated film distributed by Christal Films. Like A.I. Artificial Intelligence, it is a futuristic science fiction interpretation of the classic tale The Adventures of Pinocchio where Pinocchio is a robot brought to life by tapping into a citys power surge, rather than a puppet animated by Magic (paranormal)|magic.

The story centers on the basic legend of Pinocchio attempting to fit into living with humans, having difficulties, becoming frustrated with them, and eventually overcoming them. Most of the base elements and characters have been used, in different forms.

The story takes place in the town of Scamboville, a futuristic city constantly under development under the reign of its namesake, Scamboli.

==Plot== Geppetto making a robot, Pinocchio, as his son. Meanwhile, an evil mayor named Scamboli is making a technology city called "Scamboville", to get rid of nature. He also hates children except for his beloved daughter, Marlene. Marlene has a problem about there being no space for children to have fun. So, Scamboli is going to make a theme park called "Scamboland".

This night, Geppetto and Spencer the Penguin are preparing to make Pinocchio come to life. But Scamboli seizes control of the city mains to light up his theme park for the Grand Opening. So, Geppetto has no choice but to steal his electricity. Suddenly, Scamboland has a power cut and the children leave. After Pinocchio comes to life, much to his familys delight, Cyberina the fairy appears. She decides to grant Geppettos wish to turn Pinocchio into a real boy if he  learns about Right and Wrong.

The next morning, Pinocchio is walking his way to school with Spencer, but he meets up with Zach, Cynthia and Marlene. Marlene challenges Pinocchio to an Imagination game, hosted by Cyberina.

Marlene wins the game, but Pinocchio snatches the medal from her. As he runs away, he comes across Scambolis robotic henchmen, Cabby and Rodo, who take Pinocchio to see Scamboli. While they talk to each other, Pinocchio says, "Life would be great if kids were more like us", sparking an idea in Scambolis diabolical brain.

With the true opening of Scamboland, he makes Pinocchio into an attraction, but when Geppetto gets word of this, he tries to convince him to come home. While Pinocchio performs at a concert, Scamboli kidnaps Geppetto. Afterward, all the children board a roller coaster ride called "A Whale of a Change", which transforms all of them into "Scambobots". Meanwhile, Pinocchio gives Marlene her medal back and befriends her, and spend the night together at Marlenes private garden.

As they awaken the next morning, Marlene is crestfallen to find that Scambobots have destroyed her garden. Hearing Pinocchio laughing at her dismay, she gives the medal to him and revokes her vow of friendship. But Pinocchio, realizing that he had accidentally helped Scamboli, leaves to find his Dad. He returns home, but finds that his father isnt there, but Spencer is. He tells Pinocchio that he went off to get Pinocchio, so they head off to find him, only to find Scamboli turned Geppetto into a robot to kill Pinocchio. After Spencer blinds Scambolis with his camera and steals the remote that controls Geppeto and the other Scanbobots, Pinocchio and Spencer hide out in the "Tunnel of Danger" ride, where Scamboli manages to trap them. Marlene arrives and helps Pinoccchio to  avoid the tunnels many dangers.  However, Scamboli incapacitates Marlene, so he can kill Pinocchio with a laser gun. Pinocchio uses the medal to shield himself from the laser, causing the beam to reflect back at Scamboli and destroy his weapon. Meanwhile, Cabby accidentally gave Geppetto the remote that controls all Scambobots, getting them fired. Geppetto then commands the robots to get Scamboli.
 
Scamboli attempts  to escape in Cabbys shuttle, but is caught by a Scambocop. It tosses Scamboli inside a shuttle and flies down to the Whale ride. Pinocchio, Geppetto, Marlene and Spencer go to turn the robots back into  children. Soon its Geppettos turn, but Scamboli presses a button to stop the machines. Pinocchio goes inside the whale and tries to fix it. Pinocchio finds the out- of- reach button, so he begins to tell a lie about his personality . Once he reached it, Scamboli was caught on the cart. Pinocchio then realizes that everything was his fault. Cyberina appears, Pinocchio told her that he has learned about Right and Wrong and turns Pinocchio into a real boy and Geppetto back into a human. Suddenly, Scamboli, turned into a robot, appears and Marlene was shocked. Cyberina borrows Cynthias "Funbrella" to make sunshine and bring all the plants Scamboli has destroyed. It ends with Spencer taking a picture of Pinocchio, Geppetto and Marlene.

==Characters==

=== Geppetto family ===

====Pinocchio====
Pinocchio is an artificial intelligence designed by Geppetto. In this, his nose doesnt so much grow and retract, a feature installed by Cyberina. While initially entertaining the idea of becoming human, after being discriminated against he defensively adopts a position of (possibly righteous) superiority to humans, as he has a faster processing brain and more extensive imagination circuits. When he is cheated out of his victory in an imagination contest with Marlene, he snatches the trophy medal from her. This object becomes a strong component in the story later.

Despite his robot pride, he gains an appreciation for life and humans through his father and in befriending Marlene, and possesses a desire to be accepted by children, even if it means co-operating with the evil Scamboli.

====Geppetto====
A robot scientist who builds a robot named Pinocchio. He is the father of Pinocchio and is overprotective of Pinocchio. He caused a blackout at the opening of Scamboland for enough electricity to power his robot son Pinocchio. He gets annoyed with Spencer the Cyber Penguin for talking about his Penguin relatives and tells him to zip it. He lives inside the automated house with computers. He also tries to rescue Pinocchio from the Scamboland theme park.

====Spencer====
Spencer is a robotic talking penguin who accompanies Pinocchio on his adventure for some of the time, attempting to help him make moral decisions, guide him through the city, and avoid danger. He wears a suit and monocle like Disneys interpretation of Jiminy Cricket, and is obviously meant to fill the said characters role. Unlike in the Disney tale, he is not employed by Cyberina (the Blue Fairy interpretation) and does not play as strong a role in the story. He is a bit clumsy and laid-back, and is shown to be Geppettos assistant.

====House====
Geppettos house has an artificial intelligence which can control the doors, windows, and other appliances. This is a build upon the concept of intelligent homes already in progress with technological habitation designs.

===Scamboli Family===

====Mayor Scamboli==== raises his little girl alone. He has ambitions to transform the city, removing all signs of nature and replacing it with aesthetic metal and technology. He does greatly love his daughter and is very protective of her. He is controlling, and sees children and life as a random factor he cannot account for, motivating him to transform both into something he understands: metal.

====Marlene====
Marlene (or Marlène) is Scambolis daughter, and initially is portrayed as a stereotypical spoiled rich girl. It is eventually revealed that she is a more complex character, interested in ecological preservation and semi-technophobic. This may be the root of her initial disdain for Pinocchio. She is also revealed to be Pinocchios romantic interest. She is torn between her love for her father and the environment, as he refuses to stop attacking it. She wears her hair in pigtails, kept back with a series of coloured hooplike hair bands.

====Cab and Rodo====
Scambolis robots are under both his and his daughter Marlenes command, and the lack of seniority in command structure is later evidenced when they zoom back and forth confused under conflicting commands from the two of them. They appear and are stupid, probably responsible for Marlenes stereotyping of robots, and a source of physical humour in the movie. Cab is a tall and lanky yellow robot, Rodo is a short red robot that resembles a porcupine. Pinocchio initially feels kinship with these two and begins to espouse how much more intelligent robots are with them, but then discovers that he is rather a unique case. Are based on the Fox and the Cat.

===Miscellaneous characters===

====Cyberina====
Cyberina is the robotic fairy of the city. Her name is a play on the name Sabrina, with the word Cyber inputted, or possibly an extension of the word Cyber similar to Ballet and Ballerina, implying she is one who is proficient with cybernetics. She is based on the character the Blue Fairy from the original story.

Whether or not she has real magical powers is unknown, as she doesnt actively use them. She does however have a large grasp over various technologies and the ability to implement them. She promises to help Pinocchio figure out how to be a real boy, saying he must learn the difference between right and wrong to do so. She plays a stronger role in the story than in most interpretations, and with a large amount of comic relief, influenced by her voice actress, Whoopi Goldbergs personality and appearance.

She wears eccentric glasses, and has a mermaid-like lower body through which she can fly, and sparking circuits coming from the top of her head, reminiscent of Whoopis hair, albeit standing straight up, as if charged with static electricity.

====Zack and Cynthia====
Playing a minor role, representing other children, these children are introduced as the example of average kids living in Scamboville. They appear throughout the story, and seem to be friends of Marlene.

====Scambocop====
He is a robot policeman who patrols the skyways in a flying car in Scamboville. He is a strict police officer who tries to keep the skyways safe. He tells Zack who jumped to another building saving the flower he is trespassing in a restricted zone. He also chases after Pinocchio in the flying taxi bus.

==Music==
The musical score, by James Gelfand, is quite varied, ranging from pop to soft kids music. It has been designed with a modern appeal to kids, but pleasant enough for adults. In one instance, Pinocchio sings onstage.

The movies theme song is Whats the Difference? expressing Pinocchios feelings regarding his robotness.

==See also==
 
*List of animated feature films

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 