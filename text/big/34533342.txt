It All Starts Today
 
{{Infobox film
| name           = It All Starts Today
| image          = 
| caption        = 
| director       = Bertrand Tavernier
| producer       = Frédéric Bourboulon Alain Sarde
| writer         = Dominique Sampiero Bertrand Tavernier Tiffany Tavernier
| starring       = Philippe Torreton
| music          = 
| cinematography = Alain Choquart
| editing        = 
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = France
| language       = French
| budget         = 
}}

It All Starts Today ( ) is a 1999 French drama film directed by Bertrand Tavernier. It was entered into the 49th Berlin International Film Festival where it won an Honourable Mention.   

==Cast==
* Philippe Torreton as Daniel Lefebvre
* Maria Pitarresi as Valeria
* Nadia Kaci as Samia Damouni
* Véronique Ataly as Mrs. Lienard
* Nathalie Bécue as Cathy
* Emmanuelle Bercot as Mrs. Tievaux
* Françoise Bette as Mrs. Delacourt
* Christine Citti as Mrs. Baudoin
* Christina Crevillén as Sophie
* Sylviane Goudal as Gloria
* Didier Bezace as Inspector
* Betty Teboulle as Mrs. Henry
* Gérard Giroudon as Mayor

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 