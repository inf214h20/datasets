The Ark of the Sun God
{{Infobox film
| name           = 
| image          = ArkoftheSunGod.jpg
| image_size     = 
| caption        = DVD cover
| director       = Antonio Margheriti
| producer       = Giovanni Paolucci 
| writer         = Giovanni Paolucci (story)   Giovanni Simonelli 
| narrator       = 
| starring       = David Warbeck
| music          = Aldo Tamborelli    
| cinematography = Sandro Mancori    
| editing        = Alberto Moriani    
| distributor    = Flora Film
| released       = 21 October 1987 (France)
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1983 cinema Italian action film adventure of the Raiders of the Lost Ark genre starring David Warbeck. It was directed by Antonio Margheriti. The film was partly filmed and produced in Turkey.

==Plot Outline==
A safecracker takes a job where he must go to Istanbul and steal a scepter that once belonged to the god Gilgamesh but is now in the temple of a secret cult. 

==Cast==
*David Warbeck ...  Rick Spear 
*John Steiner ...  Lord Dean 
*Susie Sudlow ...  Carol 
*Luciano Pigozzi ...  Beetle (as Alan Collins) 
*Ricardo Palacios ...  Mohammed 
*Achille Brugnini ...  Rupert (as Anthony Berner) 
*Aytekin Akkaya ...  Prince Abdullah 
*Süleyman Turan

== External links ==
*  

 
 
 
 
 
 
 
 

 