Spring Is Here (film)
{{Infobox film name = Spring Is Here image = Spring_Is_Here_1930_Poster.jpg
|caption= Theatrical release poster producer  John Francis Dillon director = John Francis Dillon writer = James A. Starr   Based on the musical play by Owen Davis starring = Alexander Gray Louise Fazenda Ford Sterling Inez Courtney Frank Albertson Natalie Moorhead music = Cecil Copping Alois Reiser Richard Rodgers Lorenz Hart Harry Warren cinematography = Lee Garmes editing = distributor =   released =   runtime = 69 minutes language = English country = United States
}} Sam Lewis Joe Young. The film starred Lawrence Gray, Bernice Claire, Louise Fazenda and Alexander Gray.

==Synopsis==
 
As the film begins, we find Bernice Claire in love with Lawrence Gray. Claires father, played by Ford Sterling, disapproves of Lawrence but approves another suitor, played by Alexander Gray. Alexander is shy and clumsy while Lawrence is outgoing and romantic. When Bernice returns one night at 5 a.m. with Lawrence, her father orders him to stay away from his daughter. Alexander, being discouraged at being rejected by Bernice, is offered help by Inez Courtney, Bernices younger sister. Alexander follows her advice and attempts to make Bernice jealous to get her attention. He makes love to several women, including Bernices mother. The trick works and soon Bernice thinks she is deeply in love with Alexander. Sterling gets into a argument with Lawrence and tells him to leave his house for good. Lawrence returns in the middle of the night to elope with Bernice but Alexander shows up and carries her off for himself. In the morning they are found together in Bernices room, to the shock of the family, and they eventually reveal to everyone that they have eloped.

==Cast==
*Lawrence Gray - Steve Alden 
*Bernice Claire - Betty Braley Alexander Gray - Terry Clayton 
*Louise Fazenda - Emily Braley
*Ford Sterling - Peter Braley
*Inez Courtney - Mary Jane Braley
*Frank Albertson - Stacy Adams
*Natalie Moorhead - Mrs. Rita Conway 
*Brox Sisters - Singing Trio

==Songs==
*"Spring Is Here (in Person)" Performed by Frank Albertson and Inez Courtney
*"Yours Sincerely" Performed by Alexander Gray and Bernice Claire
*"With a Song in My Heart" Performed by Lawrence Gray and Bernice Claire
*"Bad Baby" Performed by Inez Courtney
*"Cryin for the Carolines" Performed by the Brox Sisters
*"Have a Little Faith in Me" Performed by Alexander Gray and Bernice Claire
*"How Shall I Tell?" Performed by Bernice Claire
*"Whats the Big Idea?" Performed by Frank Albertson and Inez Courtney (vocal and dance)
*"With a Song in My Heart" Reprised by Alexander Gray and Bernice Claire

==Adaptation and preservation==
The film survives intact and has been broadcast on television and cable.  An abridged version of the film was released in 1933 as the musical short Yours Sincerely.

==External links==
* 
** 
* 
* 

 

 
 
 
 
 
 
 
 