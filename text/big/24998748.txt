Face (2000 film)
{{Infobox film
| name = Face
| image = Face_(2000_film)_Poster.jpg
| image_size =
| caption = Japanese film poster.
| director = Junji Sakamoto
| producer =
| writer = Junji Sakamoto Isamu Uno
| starring = Naomi Fujiyama Michiyo Okusu
| music = Yasuhiro Kobayashi
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 123 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}} 2000 Cinema Japanese film Japan Academy Prize it won one award and received four other nominations.

==Plot==
Sullen and withdrawn ugly elder sister Masako toils endlessly with mending chores in her widowed mothers dry cleaning shop, seething with hatred for her flashy younger sister Yukari, who visits only for free laundry service. When mother dies and Yukari persists in her abuse, Masako cracks and strangles her. She flees and takes a number of identities and odd jobs, meeting people as she goes. To her surprise Masako finds people in general to be kind and helpful (although shes sexually abused more than once) and she blossoms as a personality, even to the extent of becoming a popularly liked bar hostess like her murdered sister.

== Cast ==
* Naomi Fujiyama
* Michiyo Okusu
* Riho Makise

== Awards and nominations == Japan Academy Prize.   Best Director - Junji Sakamoto
*Nominated: Best Picture Best Screenplay - Junji Sakamoto and Isamu Uno
*Nominated: Best Actress in a Supporting Role - Michiyo Okusu
*Nominated: Best Music - Yasuhiro Kobayashi
25th Hochi Film Award  
*Won: Best Film
*Won: Best Actress - Naomi Fujiyama
22nd Yokohama Film Festival  
*Won: Best Film
*Won: Best Director - Junji Sakamoto
*Won: Best Screenplay - Junji Sakamoto and Isamu Uno
*Won: Best Actress - Naomi Fujiyama

== References ==
 

== External links ==
*  

 
{{Navboxes  title = Awards  list =
 
 
 
 
}}

 
 
 
 
 


 