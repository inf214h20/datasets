At the End of Daybreak
{{Infobox film
| name           = At the End of Daybreak
| image          = 
| alt            =
| caption        = 
| film name      = 心魔
| director       = YuHang Ho
| producer       = 
| writer         = YuHang Ho
| screenplay     = 
| story          = 
| based on       =  
| starring       = Kara Hui, Tien You Chui, Yasmin Ahmad
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =      
| runtime        = 94 minutes
| country        = Hong Kong
| language       = 
| budget         = 
| gross          =  
}}


At End of Daybreak (Shah Mo) (心魔) is a Hong Kong movie. It was released November 5, 2009. It was directed and written by YuHang Ho.   It runs 94 minutes. 

== Plot ==
Tuck (Tsui Tin-Yau) is 23 year-old Malaysian from a lower-class family, who lives with his divorced mother (Kara Hui) and generally lays about doing nothing. He dates Ying (Ng Meng-Hui), a fifteen-year-old schoolgirl whose quiet defiance defines her character.   . The Hollywood Reporter. 

== Cast ==
* Kara Hui
* Tien You Chui
* Yasmin Ahmad
* Azman Hassan
* Hassan Muthalib
* Meng Hui Ng
 

== Box office ==
HKD 48,000 (Hong Kong) ( 2009)
MYR 30,000 (Malaysia) ( 2010)

== Awards ==
* 46th Golden Horse Awards - Kara Hui - Best supporting actress
* 16th Hong Kong Film Critics Society Awards - Kara Hui - Best actress
* 4th Asian Film Awards - Kara Hui - Best actress
* Best Newcomer - Jane Ng Meng Hui
* 29th Hong Kong Film Awards - Kara Hui - Best actress
* 10th Chinese Film Media Awards - Kara Hui - Best actress
* 10th Changchun Film Festival- Kara Hui - Best actress
* Vladivostok International Film Festival - Kara Hui - Best actress
* 2009 Locarno International Film Festival - Asian Film Award 

== Festival ==
* 2009 (62nd) Locarno International Film Festival - August 5–15 - International Competition 
* 2009 (34th) Toronto International Film Festival - September 10–19 - Contemporary World Cinema 
* 2009 (28th) Vancouver International Film Festival - October 1–16 - Dragons and Tigers
* 2009 (14th) Pusan International Film Festival - October 8–16 - A Window on Asian Cinema
* 2009 (6th) Hong Kong Asian Film Festival - October 15–30 - Closing Film 
* 2009 (22nd) Tokyo International Film Festival - October 17–25 - Winds of Asia - Middle East
* 2010 (39th) International Film Festival Rotterdam - January 27-Feb. 7th
* 2010 (9th) Asian Film Festival of Dallas - July 23–29, 2010 *Southwest Premiere

== References ==
 

== External links ==
* http://www.imdb.com/title/tt1483799/
* http://www.haf.org.hk/haf/pdf/project07/project2.pdf

 
 


 