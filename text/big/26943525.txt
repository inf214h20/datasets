Under the Greenwood Tree (1918 film)
{{Infobox film
| name           = Under the Greenwood Tree
| image          =
| caption        =
| director       = Emile Chautard
| producer       = Famous Players-Lasky
| based on       =  
| writer         = Adrian Gil-Spear (scenario) Eugene OBrien
| music          =
| cinematography = Jacques Bizeul
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (4,543 ft)
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 British film swims in the nude in a pond.  The title refers to a line in William Shakespeares play As You Like It (Act II, Scene V). The film is classified as lost film|lost. 

==Plot==
Mary Hamilton an heiress tires of fortune hunting men and takes her secretary Peggy to join a group of gypsies undercover. As the women head into the woods Sir Kenneth one of Marys close male friends follows them dressed as a gypsy. Jack Hutton a wealthy landowner wants the gypsies off his land and has Sir Kenneth jailed. Hutton then seeks out Marys camp, not knowing her true identity, and wants her thrown off the land as well but then catches her swimming in a moonlit pond. Hutton falls in love with Mary and Mary asks him to dine. When Hutton leaves a band of gypsies attacks Marys wagon and tie her up. Jack then tries to rescue Mary but is beaten by the gypsies. Sir Kenneth has by then been released from jail and arrives with Peggy, the two of them are now in love. After they cut Mary loose, Sir Kenneth and Peggy head off to be married leaving Mary to care for Hutton. As Hutton recuperates Mary tells him the truth that she is an heiress and not a gypsy as she had led Hutton to believe. They are later married. 

The poor in the film, as represented by the lazy gypsies who rob Mary, do not compare well to the heroic but naive members of the upper class. 

==Cast==
* Elsie Ferguson - Mary Hamilton Eugene OBrien - Jack Hutton
* Edmund Burns - Sir Kenneth Graham
* Mildred Havens - Peggy Ingledew
* John Ardizoni - Karl
* Robert Milasch - Pete
* Robert Vivian - Griggs Charles Craig - Hurrell Hutton
* Henry Warwick - Earl of Hexham
* James A. Furey - Sinclair

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 