Prosperity (film)
{{Infobox film
| name           = Prosperity
| image          =
| image_size     = 
| caption        = 
| director       = Sam Wood
| producer       = Irving Thalberg (uncredited)
| screenplay     = Irving Thalberg
| narrator       =  Norman Foster| Leonard Smith
| editing        = Le Vanway
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
}}

Prosperity is a 1932 American comedy-drama film starring Marie Dressler and Polly Moran. The two leading actresses play longtime matriarchal ladies comically sparring off each other, and trying to control their intertwined lives.

==Plot==
Maggie Warren (Dressler) inherited a family bank during the Depression and Lizzie Praskins (Moran) is one of her biggest depositors. Maggie’s son John is engaged to Lizziess daughter Helen.  All kinds of farces happen when the would-be mothers-in-law battle for setting the weddings protocol including their different preferences of choosing the pastor to perform the ceremony.

As the story goes on, Lizzie has a panic attack based on rumors about the bank going to belly-up. She hysterically withdraws all her money causing all other customers in the bank to panic and they in return take out their money.  The Warren family  bank is forced to close.  Maggie’s naive son gets swindled out of his mother’s bonds. As farces go, at the end the swindlers are caught and Maggie’s matriarchal resourcefulness with her wised-up son gets the bank solvent again, and the two matriarchal families are bonded with mirthful resolutions.
   

==Cast==
*Marie Dressler as Maggie Warren
*Polly Moran as Lizzie Praskins
*Anita Page as Helen Praskin Norman Foster as John Warren
*Jacquie Lyn as Cissy
*Jerry Tucker as Buster
*Charles Giblyn as Mayor
*Frank Darien as Ezra Higgins
*Henry Holland as John Roche

==References==
 

 

 
 
 
 
 
 
 
 


 