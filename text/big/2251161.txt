Damien: Omen II
{{Infobox film
| name = Damien: Omen II
| image = DamienomenII.jpg
| caption = Theatrical release poster Don Taylor
| producer = Harvey Bernhard
| screenplay = Stanley Mann Mike Hodges
| story = Harvey Bernhard
| based on = characters created by David Seltzer
| starring = William Holden Lee Grant Jonathan Scott-Taylor
| music = Jerry Goldsmith Bill Butler Robert Brown
| distributor = 20th Century Fox
| released = June 9, 1978
| runtime = 107 minutes
| country = United States
| language = English
| budget = $6.8 million 
| gross = $26,518,355 
}}

Damien: Omen II is a 1978 American  , in 1981.

This was Lew Ayres final film role and the film debut of Meshach Taylor. The official tagline of the film is "The First Time Was Only a Warning." Leo McKern reprises his role as Carl Bugenhagen from the original film; he is the only cast member of the series to appear in more than one installment.

Although the film was both less successful than the first at the box office and received mainly mixed reviews, it still made enough money making it a success and has developed a cult following.

==Plot==
A week after the burial of Robert and Katherine Thorn, archeologist Carl Bugenhagen (Leo McKern) learns of the survival of their adopted son Damien Thorn|Damien. Confiding to his friend Michael Morgan (Ian Hendry) that Damien is the Antichrist, Bugenhagen attempts to convince him to give Damiens guardian a box containing the means to kill Damien. As Morgan is unconvinced, Bugenhagen takes him to some local ruins to see the mural of Yigaels Wall, which was said to have been drawn by one who saw the Devil and had visions of the Antichrist as he would appear from birth to death. Though Morgan believes him upon seeing an ancient depiction of the Antichrist with Damiens face, both he and Bugenhagen are buried alive as the tunnel collapses on them.
 Elizabeth Shephard). Hart was a colleague of Keith Jennings, the journalist decapitated seven years previously after befriending Robert Thorn to investigate the circumstances surrounding Damiens birth and adoption by the Thorns. Hart has pieced together the circumstances of Jennings death after seeing Yigaels Wall. Though no one believes her, Joan believes she may have been mistaken about Damien until she sees his face at his school and drives off in a panic. On the road, after her cars engine mysteriously dies, Joan is attacked by the raven as it pecks out her eyes and then watches her get run over by a passing truck.

At Thorn Industries, manager Paul Buher (Robert Foxworth) suggests expanding the companys operations into agriculture; however, the project is shelved by senior manager Bill Atherton (Lew Ayres), who calls Buhers intention of buying up land in the process immoral. At Marks birthday, Buher introduces himself to Damien, invites him to see the plant, and also speaks of his approaching initiation. Buher seemingly makes up with Atherton, who drowns after falling through the ice at a hockey game the following day. A shocked Richard leaves on vacation, leaving Buher to oversee the agriculture project in principle and returning to find that he initiated land purchases on his own.

Meanwhile, at the academy, Damiens new commander, Sgt. Neff (Lance Henriksen), is revealed to be a secret Satanist like Buher as he takes the boy under his wing while advising him not to draw any attention to himself until the right moment. He also points him to the Biblical Book of Revelation|Revelation, chapter 13, telling Damien that, for him, the book is precisely that; a revelation. Damien reads the passage, discovering the 666 Mark of the Beast on his scalp. Learning his true nature he flees the Academy grounds in a terrified panic. Later, alerting Buher that he intends to tell Richard that some of the land they obtained was taken from people who were murdered after having refused to sell their land, Dr. David Pasarian (Allan Arbus) is killed when he and his assistant suffocate from toxic fumes during an industrial accident. The incident injures Damiens class, who were visiting the plant at the time. When Damien alone is found to be unharmed by the fumes, a doctor (Meshach Taylor) suggests keeping him in the hospital as a precaution. The doctor discovers that Damiens marrow cells resemble that of a jackal; before he can investigate any further or report his findings, however, he is bifurcated by a falling elevator cable.
 always belonged to him". Having heard the altercation from an outside corridor, Damien sets the room on fire, with Ann consumed in the flames. Damien then exits the burning museum and is picked up by the family driver, Murray, as the fire department arrives.

==Cast==
* William Holden as Richard Thorn
* Lee Grant as Ann Thorn
* Robert Foxworth as Paul Buher
* Lew Ayres as Bill Atherton
* Sylvia Sidney as Aunt Marion
* Jonathan Scott-Taylor as Damien Thorn
* Nicholas Pryor as Dr. Charles Warren
* Lance Henriksen as Sergeant Daniel Neff Elizabeth Shephard as Joan Hart
* Lucas Donat as Mark Thorn
* Allan Arbus as David Pasarian
* Fritz Ford as Murray
* Meshach Taylor as Dr. Kane
* Leo McKern (uncredited) as Carl Bugenhagen
* Ian Hendry (uncredited) as Michael Morgan

==Production==

===Crew===
David Seltzer, who wrote the first films screenplay, was asked by the producers to write the second. Seltzer refused as he had no interest in writing sequels. Years later, Seltzer commented that had he written the story for the second Omen, he would have set it the day after the first movie, with Damien a child living in The White House. With Seltzer  turning down Omen II, producer Harvey Bernhard duly outlined the story himself, and Stanley Mann was hired to write the screenplay.

After Bernhard had finished writing the story outline and was given the green light to start the production, the first person he contacted was Jerry Goldsmith because of the composers busy schedule. Bernhard also felt that Goldsmiths music for The Omen was the highest point of that movie, and that without Goldsmiths music, the sequel would not be successful. Goldsmiths Omen II score uses similar motifs to his original Omen score, but for the most part, Goldsmith avoided re-using the same musical cues. In fact, the first movies famous "Ave Satani" theme is used only partially, just before the closing credits begin. Goldsmith composed a largely different main title theme for Omen II, albeit one that utilises Latin phrases as "Ave Satani" had done. Goldsmiths Omen II score allows eerie choral effects and unusual electronic sound designs to take precedence over the piano and gothic chanting. 

Richard Donner, director of the first Omen movie, was not available to direct the second, as he was busy working on Superman (1978 film)|Superman. British film director Mike Hodges was hired to helm the movie. During production, the producers believed that Hodges methods were too slow, and so they fired him and replaced him with Don Taylor, who had a reputation for finishing films on time and under budget. However, the few scenes Hodges directed (some of the footage at the factory and at the military academy, all of the early archaeology scenes, and the dinner where Aunt Marion shows her concern about Damien) remained in the completed film, for which Hodges retains a story credit. In recent interviews, Hodges has commented sanguinely on his experiences working on Omen II.

===Casting===
Academy Award-winning veteran actor William Holden was the original choice to star as Robert Thorn in the first Omen, but turned it down as he did not want to star in a picture about the devil. Gregory Peck was selected as his replacement. The Omen went on to become a huge hit and Holden made sure he did not turn down the part of protagonist Richard Thorn in the sequel. Lee Grant, another Academy Award|Oscar-winner, was a fan of the first Omen and accepted enthusiastically the role of Ann Thorn.
 The Birds (1963).

===Locations=== CBOT Tower and the Willis Tower visible in the background.

Other locations included Lake Forest Academys campus, which was used as the Thorn Mansion, the Northwestern Military and Naval Academys Geneva Lake campus, which was used for the military academy, with real Geneva Lake students portraying most of the academy cadets, and the Murphy Estate on Catfish Lake in Eagle River, Wisconsin for the skating scene, with local children playing the skaters. The Thorn Museum aka Field Museum of Natural History was also used in several scenes throughout the film including some of the movies final minutes. 

==Reception==
The film received mixed reviews and currently holds a 41% rating on review aggragator Rotten Tomatoes based on 22 reviews.   

==Soundtrack==
Unlike  ), Jerry Goldsmiths score was recorded in the US, with the soundtrack album re-recorded in Britain for financial reasons. Lionel Newman conducted both the film and album versions; Varèse Sarabande later released an expanded CD including both, the liner notes of which explain the reasons behind the re-recording (a short lived union rule meant that musicians had to be paid the full amount for the film use AND album use if the soundtrack was released on LP, doubling their fee. It was cheaper, therefore, to re-record in the UK than pay the orchestra double in the US). The liner notes also explain that some of the soundtracks pieces have been re-written slightly or even merged for the album re-recording. The audio quality of these UK recorded album tracks also sounds noticeably more dynamic. Some sections of the films soundtrack - the tapes of which were thought lost for many years - were discovered to have warped in storage and have noticeable and uncorrectable flaws. (The film soundtrack is listed from track 11 onwards).

# Main Title (5:03)
# Runaway Train (2:38)
# Claws (3:14)
# Thoughtful Night (3:05)
# Broken Ice (2:19)
# Fallen Temple (2:55)
# I Love You, Mark (4:37)
# Shafted (3:00)
# The Knife (3:21)
# End Title (All The Power) (3:24)
# Main Title (2:03)
# Face of the Antichrist (2:20)
# Fallen Temple (1:33)
# Aunt Marions Visitor (:36)
# Another Thorn (1:18)
# A Ravenous Killing (3:07)
# Snowmobiles (1:11)
# Broken Ice (2:21)
# Number of the Beast (1:33)
# Shafted (3:00)
# The Daggers (1:56)
# Thoughtful Night (2:36)
# I Love You, Mark (4:12)
# Runaway Train (1:10)
# The Boy Has To Die (1:24)
# All The Power and End Title (3:14)

==DVD release==
The film was released as part of The Omen Quadrilogy set in the US and UK in 2000, and was not available separately until 2005. In 2006, to coincide with the DVD release of the remake of the original film, The Omen and its sequels were released individually and together in an ultimate Pentalogy boxset digitally remastered and with more bonus features. In 2008, it was released on  .

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 