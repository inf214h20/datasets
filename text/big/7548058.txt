Best of the Best 2
 
 
{{Infobox Film
| name           = Best of the Best 2
| image          = Bestofthebest2.jpg
| caption        = Theatrical release poster
| director       = Robert Radler
| producer       = Phillip Rhee Peter Strauss
| eproducer      = Frank Giustra Michael Holzman
| aproducer      = Deborah Scott
| writer         = Max Strom John Allen Nelson
| starring       = Eric Roberts Phillip Rhee
| music          = David Michael Frank
| cinematography = Fred Tammes
| editing        = Bert Lovitt Florent Retz
| studio         = The Movie Group Picture Securities Ltd.
| distributor    = 20th Century Fox
| released       = March 5, 1993
| runtime        = 101 min.
| country        = United States
| awards         =
| language       = English
| budget         = 
| gross = $6,044,652 (USA) 
}}

Best of the Best 2 is a 1993 martial arts film directed by Robert Radler, and starring Eric Roberts and Phillip Rhee. It is the first sequel to the 1989 film Best of the Best.  The plot follows three of the characters from the original film, and was released on DVD on February 6, 2007.

==Plot==

After returning home from South Korea to great acclaim, three members of the U.S. National Karate Team (Tommy Lee, Alex Grady, and Travis Brickley) set up their own martial arts studio in Las Vegas. Unbeknownst to his friends, however, Travis has been competing at "The Coliseum", a brutal underground fighting arena run by a shady promoter named Weldon (Wayne Newton), whose protégé Brakus (Ralf Möller) is the owner and undefeated champion. Though the rules of the Coliseum state that a challenger must defeat three of its "Gladiators" in order to face Brakus, Travis wastes little time in challenging Brakus outright. Amused by Traviss arrogance, Weldon eagerly grants his wish.

Meanwhile, Alexs eleven-year-old son Walter (Edan Gross) begins testing for his black belt, but ultimately falls short of his goal. When his father makes an impassioned speech praising his son for his maturity, Walter takes it upon himself to cancel his own babysitter. Not about to leave his son home alone all night, Alex insists that Walter accompany Travis to his bowling league. It is only then that Travis finally reveals his secret to Walter, who blackmails Travis into letting him watch the fight with Brakus. It turns out to be a tragic mistake, as Brakus mercilessly pummels Travis and breaks his neck, killing him instantly.

Walter runs home and alerts his father and Tommy, and together they proceed to the dance club which serves as a front for the Coliseum. They are intercepted by Weldon, who claims that Travis left the Coliseum on his own. Tommy searches the city to no avail, until the police find Traviss body floating in the river along with his damaged car, the apparent result of an auto accident.

Furious, Alex and Tommy return to the club and confront Brakus, who openly admits to killing Travis. In the resulting skirmish, Tommy connects with a punch that sends Brakus face-first into a mirror, scarring his cheek. Brakus then condemns both Alex and his son to death, but orders Weldons henchmen to bring Tommy back alive.

At Traviss funeral, Alex and Tommy are startled by the appearance of Dae Han (Simon Rhee), Tommys old rival from South Korea. Still owing a debt to Tommy for sparing his life, Dae Han pledges to help his friends bring Traviss killer to justice, even if the police will not.

The next day, while riding his bike home from school, Walter finds himself being aggressively tailed by a black vehicle. He returns home to warn his father and Tommy, but they suddenly come under attack by a group of armed men. After fending them off, they quickly pack up and head out of town, eventually seeking refuge with Tommys Native American grandmother. They also encounter Tommys uncle James (Sonny Landham), a once-promising fighter whose career was all but ruined due to a clash with Brakus. Claiming to know how to defeat him, James begins to train Alex and Tommy in their bid for revenge.

Their training does not last long, however, as Weldons henchmen track them down. Despite being vastly outgunned, James tries to intervene but is tragically shot to death. While Tommy is forced into the waiting helicopter, Alex and the others are herded back into the house. As Weldons men prepare to execute them and blow up the house, Walter provides a distraction which enables Alex to overpower the gunman. To avoid suspicion, Tommys grandmother prompts Alex to fire four shots to signal their deaths, at which point the thugs set fire to the gasoline trail, causing a massive explosion. After emerging from the cellar unharmed, Alex leaves Walter with his girlfriend Sue (Meg Foster), then recruits Dae Han and his Korean teammates to storm the Coliseum and rescue Tommy.

At the Coliseum, Tommy fights his way through the Gladiators and earns the right to face Brakus. Disheartened by the perceived loss of his friends and family, Tommy fights bravely but appears to be overmatched by the formidable Brakus. As Brakus prepares to finish him off, however, Alex suddenly breaks through into the arena, his mere presence giving Tommy a second wind. A vicious barrage of kicks send Brakus to the canvas, and Tommy warns him to stay down. But Brakus does not comply, and Tommy has no choice but to break his neck, much to the delight of the bloodthirsty crowd.

With his champion defeated, Weldon announces Tommy as the new owner of the Coliseum and invites him to say a few words to the live audience. Disgusted with the brutality of the fights, Tommy takes the microphone and declares the Coliseum closed. When Weldon protests, Alex silences him with a swift elbow to the face. Alex and Tommy then leave the arena together, turning off the lights as they exit.

==Cast==

*Alexander Grady - Eric Roberts
*Tommy Lee - Phillip Rhee
*Travis Brickley - Christopher Penn
*Walter Grady - Edan Gross
*Brakus - Ralf Möller
*Sue - Meg Foster
*James - Sonny Landham
*Weldon - Wayne Newton
*Finch - Patrick Kilpatrick
*Grandma - Betty Carvalho
*Dae Han - Simon Rhee
*Sae Jin Kwon - Hayward Nishioka
*Yung June - Ken Nagayama
*Karate Student - Michael Treanor (uncredited)

==Reception==

Best of the Best 2 was a box office flop, earning only $6,607,218.   It received mostly negative reviews from critics,  but few positive ones.  As a result, further sequels were not given a theatrical release. The film currently holds an 11% rating on Rotten Tomatoes.

Nevertheless the movie was a great success on the VHS-to-rentals market following its theatre release, and it was this success that spawned two sequels that similarly found their niche in the direct-to-video market.

Film historian Leonard Maltin gave the film a negative review, but also said, "  actually improves on its lame predecessor, which appallingly wasted its top-drawer cast."

Returning star Eric Roberts claims to have made this film to compensate for the original. In Maltins words, "What a considerate guy!"

==Soundtrack==

* World Destiny (Performed By: Rave Crusader) 
* Paranoid (Performed By: Angel Ice)
* Je NAime Que Toi (Performed By: Angel Ice)
* Down for the Count (Performed By: Mark Yoakam)
* Guilty (Performed By: Public Nuisance) 
* Willie Rise (Performed By: Lil D & Big C)
* Everybody Loves a Winner (Performed By: William Bell)
* No Guts, No Glory (Performed By: Jeff Steele)
* (To Be) The Best of the Best (Performed By: Mark Free)

==In popular culture== World Wrestling German Bodybuilding|bodybuilder-turned-wrestler Achim Albrecht.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 