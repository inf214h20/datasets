Algiers (film)
{{Infobox film
| name           = Algiers
| image          = Algiers 1938 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster John Cromwell
| producer       = Walter Wanger
| based on       =  
| writer         = James M. Cain  
| screenplay     = John Howard Lawson
| starring       = {{Plainlist|
* Charles Boyer
* Sigrid Gurie
* Hedy Lamarr
}}
| music          = {{Plainlist|
* Vincent Scotto
* Mohamed Ygerbuchen
}}
| cinematography = James Wong Howe
| editing        = {{Plainlist|
* Otho Lovering
* William H. Reynolds
}}
| studio         = United Artists
| distributor    = United Artists
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $691,833 Bernstein, Matthew (2000). Walter Wagner: Hollywood Independent. Minnesota Press, p. 439. 
| gross          = $951,801 
}}
 John Cromwell and starring Charles Boyer, Sigrid Gurie, and Hedy Lamarr.    Written by John Howard Lawson, the film is about a notorious French jewel thief hiding in the labyrinthine native quarter of Algiers known as the Casbah. Feeling imprisoned by his self-imposed exile, he is drawn out of hiding by a beautiful French tourist who reminds him of happier times in Paris. The Walter Wanger production was a remake of the successful 1937 French film Pépé le Moko, which derived its plot from the Henri La Barthe novel of the same name.   
 public domain (in the USA) due to the claimants failure to renew its copyright registration in the 28th year after publication. 

==Plot==
Pepe Le Moko (Boyer) is a notorious thief, who escaped from France after his last great heist to Algeria. Since his escape, Moko became a resident and leader of the immense Casbah, or "native quarter," of Algiers. French officials arrive insisting on Pepes capture are met with unfazed local detectives, led by Inspector Slimane (Calleia), who are biding their time. Meanwhile, Pepe begins to feel increasingly trapped in his prison-like stronghold, a feeling which intensifies after meeting the beautiful Gaby (Lamarr), who is visiting from France. His love for Gaby soon arouses the jealousy of Ines (Gurie), Pepes Algerian mistress.

==Cast==
* Charles Boyer as Pepe le Moko
* Sigrid Gurie as Ines
* Hedy Lamarr as Gaby
* Joseph Calleia as Inspector Slimane Alan Hale as Grandpere
* Gene Lockhart as Regis
* Walter Kingsford as Chef Inspector Louvain Paul Harvey as Commissioner Janvier Stanley Fields as Carlos
* Johnny Downs as Pierrot
* Charles D. Brown as Max
* Robert Greig as Giraux
* Leonid Kinskey as LArbi
* Joan Woodbury as Aicha
* Nina Koshetz as Tania

==Reception==
The film earned a profit of $150,466. 

===Academy Awards=== Best Actor (nomination) – Charles Boyer Best Supporting Actor (nomination) – Gene Lockhart Best Art Direction (nomination) – Alexander Toluboff Best Cinematography (nomination) – James Wong Howe

==Radio adaptation==
Algiers was dramatized as an hour-long radio play on two broadcasts of Lux Radio Theater, first on July 7, 1941 with Charles Boyer and Hedy Lamarr, second on December 14, 1942 with Boyer and Loretta Young.

==In popular culture==
The 1938 movie Algiers  was most Americans introduction to the picturesque alleys and souks of the Casbah. It was also the inspiration for the 1942 Warner Brothers movie Casablanca (film)|Casablanca which was written specifically for Hedy Lamarr in the female lead role. However, MGM refused to release Hedy Lamarr despite all efforts by Warner Brothers.

The invitation "Come with me to the Casbah," which was heard in trailers for Algiers but not in the film itself, became an exaggerated romantic overture promising exoticism and mystery, largely owing to its use by Looney Tunes cartoon character Pepé Le Pew, himself a spoof of Pépé le Moko.  The amorous skunk used "Come with me to ze Casbah" as a pickup line.  In 1954, the Looney Tunes cartoon The Cats Bah specifically spoofed Algiers, with the skunk enthusiastically declaring to Penelope the Cat, "Do not come with me to ze Casbah...We shall make beautiful musics   togezzer right ere!"

==See also==
* Casbah (film)|Casbah (1946)
* The Battle of Algiers (1966)
* List of American films of 1938
* List of films in the public domain

==References==
 

==External links==
 
*   on  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 