Father of the Bride (1991 film)
{{Infobox film
| name           = Father of the Bride 
| image          = Father of the bride poster.jpg
| image_size     = 200px
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Shyer
| producer       = Carol Baum Nancy Meyers Howard Rosenman
| screenplay     = Charles Shyer Nancy Meyers Frances Goodrich Albert Hackett
| based on       =   Kimberly Williams Martin Short
| music          = Alan Silvestri John Lindley
| editing        = Richard Marks
| studio         = Sandollar Productions
| distributor    = Touchstone Pictures
| released       =  
| runtime        = 105 minutes 
| country        = United States
| language       = English
| budget         = $20,000,000 
| gross          = $89,325,780 
}} Kimberly Williams, film of the same name.
 Hallmark commercials that featured the smiling faces of the happy couple and sneak-peeks at the backs of numerous greeting cards. This film is number 92 on Bravo (US TV channel)|Bravos "100 Funniest Movies".

Martin portrays George Banks, a businessman and owner of an athletic shoe company (called side kicks), who, when he finds out his daughter is getting married, does not want to give her away. He eventually learns to live with his new son-in-law and realizes that as long as his daughter is happy, he is happy.

The film opened to positive reviews, and became a major box office success, earning more than four times its budget. With the success of the film, a sequel, Father of the Bride Part II was released in 1995.

==Plot== Kimberly Williams), returns from Europe, telling them she is engaged to Bryan MacKenzie (George Newbern), a man from an upper-class family from Bel-Air, Los Angeles, California|Bel-Air, despite them only having known each other for three months. The sudden shock turns the warm reunion into a heated argument between George and Annie, but they quickly reconcile in time for Bryan to arrive and meet them. Despite Bryans good financial status and likeable demeanour, George takes an immediate dislike to him while his wife, Nina (Diane Keaton), accepts him as a potential son-in-law.

George and Nina meet Bryans parents, John and Joanna MacKenzie. Though George feels comfort from John also expressing how shocked he had initially been at Bryans marriage plans, he quickly gets into trouble when he begins nosing around and eventually ends up falling into the pool when cornered by the MacKenzies vicious pet Dobermans. All is forgiven, however, and the Banks meet with an eccentric European wedding designer, Franck Eggelhoffer (Martin Short) and his assistant, Howard Weinstein (B.D. Wong), where George immediately begins complaining about the price of the extravagant wedding items. The high price, $250 a head, plus the problems of wedding invitations begin to take their toll on George and he becomes slightly insane. The last straw occurs when his wrongly sized tuxedo, which he had struggled to put on, rips when he bends his back. He leaves the house to cool off, but ends up causing a disturbance at a supermarket. Fed up with paying for things he doesnt want, he starts removing hot dog buns from their 12-bun packets so as to match the 8-dog packets of hot dogs. He ends up getting arrested, but Nina arrives to bail him out on the condition that he stop ruining Annies wedding.

With help from Nina and Franck, George becomes more relaxed and accepting of the wedding, particularly when Annie and Bryan receive rather expensive gifts from extended family members, but the wedding plans are put on hold when they have a row over a blender he gave to her as a gift, which only gets worse when she refuses to believe his story about Georges antics at his house when he fell in the pool. George takes Bryan out for a drink, initially intending to get rid of him for good, but seeing his heartbroken face and genuine claim that he loves Annie, George has a change of heart and finally accepts him. He confesses to Annie that what happened at Bryans house was true, and she and Bryan reconcile.

Despite some last minute problems with the weather, the wedding is finally prepared, almost one year after Bryan and Annies first meeting. They marry and the reception is held at the house, despite a nosy police officer objecting to the number of parked cars in the street. George, unfortunately, misses Annie throwing the bouquet and is unable to see her before she and Bryan leave for their honeymoon in Hawaii. She, however, calls him from the airport to thank him and tell him that she loves him one last time before they board the plane.

With the house now empty and the wedding finished, George finds solace with Nina and dances with her.

==Cast==
* Steve Martin as George Banks
* Diane Keaton as Nina Banks Kimberly Williams as Annie Banks
* Kieran Culkin as Matty Banks
* George Newbern as Bryan MacKenzie
* Peter Michael Goetz as John MacKenzie
* Kate McGregor-Stewart as Joanna MacKenzie
* Martin Short as Franck Eggelhoffer
* B.D. Wong as Howard Weinstein
* Richard Portnow as Al
* David Pasquesi as Hanck
* Chauncey Leopardi as Cameron
* Eugene Levy as Singer at audition
* Marissa Lefton as 3-year-old Annie
* Sarah Rose Karr as 7-year-old Annie
* Amy Young as 12-year-old Annie

==Soundtrack==
The films soundtrack was scored by Alan Silvestri and was influenced by Jazz and Christmas instrumentations. It contains the following tracks:

# "Main Title"
# "Annies Theme"
# "Drive to Brunch"
# "Snooping Around"
# "Pool Cue"
# "Annie Asleep"
# "Basketball Kiss"
# "The Wedding"
# "Snow Scene"
# "Nina at the Stairs"
# "The Big Day"
# "Annie at the Mirror Pachelbel Canon"
# "The Way You Look Tonight" - Alan Silvestri, Fields, Dorothy
# "My Annies Gone"
# "The Way You Look Tonight (Reprise)"
# "End Credits"

The following songs are also featured in the film: My Girl" - The Temptations
* "(Today I Met) The Boy Im Going to Marry" - Darlene Love
* "Chapel of Love" - The Dixie Cups

==Reception==

The film opened to generally favorable reviews. The review aggregator website Rotten Tomatoes reported that 73% of critics gave the film a positive rating, based on 41 reviews, with an average score of 6/10. Its consensus states that "while it doesnt quite hit the heights of the original, this remake of the 1950 classic is pleasantly enjoyable, thanks in large part to winning performances from Steve Martin and Martin Short."  Contrastingly, it received 51/100 on Metacritic. Roger Ebert called Bride "one of the movies with a lot of smiles and laughter in it, and a good feeling all the way through. Just everyday life, warmly observed."  The film drew $15 million on its debut. 

===Awards and nominations===
;MTV Movie Awards Kimberly Williams
* 1992; nominated, "Best Comedic Performance" - Steve Martin

;BMI Film Awards
* 1993: won, "Best Movie" - Father of the Bride

;Young Artist Award
* 1993; nominated, "Best Young Actor Co-starring in a Motion Picture" - Kieran Culkin

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 