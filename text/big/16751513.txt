The Secret Life of Bees (film)
{{Infobox Film
| name           = The Secret Life of Bees
| image          = Secret life of bees.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Gina Prince-Bythewood
| producer       = Lauren Shuler Donner James Lassiter Will Smith Jada Pinkett Smith Joe Pichirallo
| screenplay     = Gina Prince-Bythewood
| based on       =  
| starring       = Queen Latifah Dakota Fanning Jennifer Hudson Alicia Keys Sophie Okonedo Paul Bettany
| music          = Mark Isham
| cinematography = Rogier Stoffers
| editing        = Terilyn A. Shropshire
| studio         =   The Donners Company
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $11 million
| gross          = $39,947,322   
}}
 the novel of the same name by Sue Monk Kidd. The film was directed by Gina Prince-Bythewood and produced by Will Smith, with his wife, Jada Pinkett Smith, as executive producer. The film is noted for Queen Latifahs critically acclaimed performance as August Boatwright. The film was released in North America on October 17, 2008 and in the United Kingdom on December 5, 2008.

Set in South Carolina in 1964, this is the tale of Lily Owens (Dakota Fanning), a 14-year-old girl who is haunted by the memory of her late mother Deborah Owens (Hilarie Burton). Lily flees with Rosaleen (Jennifer Hudson), her caregiver and only friend, to a town in South Carolina where she uncovers the secrets to her mothers past. Taken in by Boatwright sisters &mdash; August (Queen Latifah), May (Sophie Okonedo) and June (Alicia Keys) &mdash; Lily finds comfort in their world of bee keeping and develops a romance with her new friend Zach (Tristan Wilds). She learns about female power, as the Boatwright sisters show her their black Virgin Mary, her mothers past and much more.

==Plot==
The movie begins with Lily having a flashback: her mother is packing, when T-Ray enters the room and grabs her. Frightened, she grabs a gun but it is knocked out of her hands. Lilys mother holds out her hand for Lily to hand her the gun. Then, a gunshot is heard and Lily states that she accidentally killed her mother, while trying to help her when she was young.

In the night, Lily sees many bees in her room and tells her father but T-Ray yells at her to go back to bed. The next night, Lily runs into their peach tree orchard and digs up a box she buried there. The box contains her mothers gloves, a small photograph of her mother, and a flat wood carving of the Black Madonna. She unbuttons the bottom part of her blouse and sets the carving on her stomach. While Lily lies staring at the stars and talking to her mothers spirit, she hears her father calling. He accosts her. As there is no one else, T-Ray takes Lily home.

The next morning, the housekeeper Rosaleen surprises Lily with a cake for her 14th birthday. As T-Ray enters, Rosaleen states that she is going to take Lily into town to buy her a training bra. Lily asks him to tell her about her mother as her birthday gift but he says very little. When Rosaleen and Lily walk uptown, they are confronted by three racist men who insult Rosaleen. She is beaten up and arrested. After T-Ray and Lily get home, T-Ray punishes Lily sending her to her bedroom and he and Lily argue. When Lily calls him a liar, he again tells her to stay in her room. Lily waits until he is gone before she writes a letter saying "Dont bother to look for me. People like you who tell lies should rot in hell". After setting the note on the table, she grabs the bag she has packed and goes to the hospital "colored ward" to find Rosaleen.

They run away to a town, where Lilys mother was thought to have once lived, called Tiburon. They find a honey jar with the same picture of the Virgin Mary that Lily had. When they ask who made the honey, they are led to the house of August Boatwright and her sisters, May and June. May had a twin, April, who is revealed to have died during their childhood. Her condition makes her have difficulties distinguishing others problems from hers. She sees everyones problems as her concern and she has difficulties coping with her "extra problems." Lily lies and says that she is an orphan, that Rosaleen is a nanny, and that they have no place to stay. August offers to let them help with the honey in exchange for shelter, much to Junes dismay.

Lily experiences growing up in their household. June has a boyfriend named Neil, who is trying to marry her but she always refuses. Lily questions August as to why she uses "black" Mary pictures. She claims that she was sent by God to help African Americans have peace and equality. When Lily is introduced to the local church, she has a flashback when the memories of the night her mother died begin to come to her. She discovers a wall outside of the house that has little pieces of paper stuffed between the holes. It was built for May after April died. August claimed that they were psychically linked to one another and were like a single person. After April died, May became very emotional and easily broken down by sad situations. She would write all the things that troubled her and put them into the wall.

Lily and a teenage boy named Zach, who works with the bees become friends. Soon after Lily meets with Zach and they start to talk about each others futures. Lily confesses she loves English and wants to become a writer as Zach confesses he wants to become a lawyer. On the ride to town to deliver honey, Lily confides in Zach the truth of their situation and the reason why she came to August. He gives her a notebook because she claimed to want to be a writer. They decide to watch a movie together but Lily sits in the "colored" section. This incites a racist mob who kidnap Zach and call Lily names for choosing to associate with him. Zachs mother and the Boatwright family are very worried and concerned. June and August decide not to tell May of Zachs disappearance, for fear of what it would do to her fragile mental state.

Zachs mother comes to the house to pray before the statue of the Black Madonna in the living room and runs into May afterwards. She confides in an unknowing May who subsequently goes into shock. The others grow worried after May disappears for an unusual amount of time. They go out to look for her only to discover that Mays sadness and dismay led her to kill herself, by placing a large rock upon her chest and lying down in a shallow section of the river by their home. She writes a suicide note to August and June, claiming that she was too tired of carrying the weight of the world on her shoulders and that she would be much happier with her sister and parents in Heaven. Soon after Mays death, Zach returns but bruised and distraught. After her funeral, August and June slowly begin to recover from their loss, with June even finally agreeing to marry Neil, whom she had broken up with in a fit of indecisive anger and fear. Realizing the sisterly bond they have formed with Rosaleen, August and June embrace her as their new sister and decide to call her "July".

Meanwhile, Lily shows August the picture of her mother and August admits that she knew all along. Lily confesses that she, while trying to help her mother, wound up killing her. She also confesses her belief that Zachs disappearance was all her fault, as well, so she needs to leave, convinced it is best for everyone that she leave since she is unlovable and destroys all that she comes across. Lily breaks down crying and runs to the honey house, where she breaks several jars. August finds her and gives over the things that her mother left, when she went back to get Lily, though Lily doesnt believe her. Lily receives her mothers old comb, pin, a piece of poetry and a picture of her mother with baby Lily. This shows that Deborah did love her daughter. Zach gives her a necklace telling Lily to never forget their story and she smiles and says she wont. They share a kiss.

Later on, T-Ray comes to the Boatwright home looking for Lily. When he sees Lily wearing her mothers pin, he mistakes her for her mother for a second and proceeds to grab and drag her out of the house, all the while telling her she cant leave him. In a moment of panic, Lily calls him Daddy, causing him to come to himself. She tells him shes staying there and after August shows up to calm the scene and assure him theyll take care of her, he reluctantly gives August permission to take care of Lily, for as long as she wanted to stay there. As T-Ray drives off, he admits that the day Deborah left, she wasnt only coming back for her stuff, but coming back for Lily. He says he lied because she wasnt coming back for him.

Lilys voice-over states that she thought, as T-Ray drove off, saying "Good riddance", he was really saying "Lily, youll be better off here with all of these mothers." Lily is shown writing the contents of the entire story into the notebook that Zach gave her and she puts it in Mays wall and is shown wearing Zachs necklace. She walks off into the honey house as the scene fades to black.
 

==Cast==
* Queen Latifah as August Boatwright
* Jennifer Hudson as Rosaleen "July" Daise
* Alicia Keys as June Boatwright
* Dakota Fanning as Lily Owens
* Sophie Okonedo as May Boatwright
* Paul Bettany as T-Ray Owens
* Hilarie Burton as Deborah Owens
* Tristan Wilds as Zach Taylor
* Nate Parker as Neil
* Shondrella Avery as Greta

==Production== 33rd Annual Toronto International Film Festival, and had an October 17, 2008 theatrical release. 

==Reception==
 
The Secret Life of Bees received mixed reviews. Rotten Tomatoes gives the film a score of 58% based on 136 reviews, with the consensus "The Secret Life of Bees has moments of charm, but is largely too maudlin and sticky-sweet."  
Writing in The New York Times, reviewer A. O. Scott thought the film to be "a familiar and tired fable".  
Roger Ebert found it "enchanting". 

 
The movie was   3 at the North American box office for its opening weekend with $10,527,799.  , the film has made $47,270,658 in the United States and Canada.  

 
The movie won the awards for "Favorite Movie Drama" and "Favorite Independent Movie" at the 35th Peoples Choice Awards. 

The film received seven NAACP Image Award nominations, which include Outstanding Motion Picture, Outstanding Actress in a Motion Picture (Queen Latifah, Dakota Fanning), Outstanding Actor in a Motion Picture (Nate Parker), and Outstanding Supporting Actress in a Motion Picture (Alicia Keys, Jennifer Hudson, and Sophie Okonedo). The movie won the Image Award for Outstanding Motion Picture.

==References==
 

==External links==
*  
*  
*  
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 