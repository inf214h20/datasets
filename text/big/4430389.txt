Eternity and a Day
{{Infobox film
| name           = Eternity and a Day
| image          = Eternite_affiche.jpg
| caption        = french film poster
| director       = Theo Angelopoulos
| producer       = Theo Angelopoulos Eric Heumann Giorgio Silvagni
| writer         = Tonino Guerra Theo Angelopoulos Petros Markaris Giorgio Silvagni
| starring       = Bruno Ganz Isabelle Renauld Fabrizio Bentivoglio
| music          = Eleni Karaindrou
| cinematography = Yorgos Arvanitis  Andreas Sinanos 
| editing        =
| distributor    = Artistic License Merchant Ivory Productions (U.S.) Artificial Eye (UK)
| released       =  
| runtime        = 132 minutes
| country        = Greece
| language       = Greek (parts in English and Italian)
| budget         =
}} 1998 Cinema Greek film starring Bruno Ganz, and directed by Theo Angelopoulos. The film won the Palme dOr and the Prize of the Ecumenical Jury at the 1998 Cannes Film Festival.   

==Plot==
Alexander (Bruno Ganz), a bearded poet, leaves his seaside apartment in Thessaloniki after learning he has a terminal illness and must enter a hospital the next day for an unspecified "test". He is trying to get his affairs in order and find a new master for his dog.

Alexander saves a six- or seven-year-old boy who is a vagrant window washer from a band of policemen who are chasing down similar boys. He pays a visit to his thirtyish daughter (Iris Chatziantoniou), and musing on his likely dead wife, Anna (Isabelle Renauld), who appears as almost the same age as their daughter. At his daughter’s apartment, he does not tell her of his diagnosis, instead hands her letters written by his wife, her mother. She reads them. He learns that his daughter and her lover have sold his apartment for demolition without telling him.

The boy is trying to leave Greece but the way to Albania is not exactly an easy one, Alexander sees at the snowy mountain border an eerie barbed wire fence with what seem to be bodies stuck to it. As the pair wait for the gate to open, they have a change of mind about crossing, when the boy admits has been lying about his life in Albania. The two of them barely escape a border sentry who chases them and make it back to Alexanders automobile.

The boy’s perilous existence brings Alexander out of his stupor and self-pity, and seemingly re-energizes him in his love for a dead 19th century Greek poet, Dionysios Solomos (Fabrizio Bentivoglio), whose poem he longs to finish. 

The old poet and the boy are connected by fear. The former over what lies ahead, and if his life has had impact, and the latter over what lies ahead in his — especially a perilous return trip to Albania where, as he explains to Alexander, the path over the mountains is lined with land mines, as well as men who kidnap street boys to sell them for black market adopters (as well as possibly the sex trade). 

He pays a visit to his housekeeper, Urania (Helene Gerasimidou). She is manifestly smitten with him, but is in the middle of a wedding party and dance between her son and his bride. The scene plays on until Alexander interrupts. He leaves the dog, and then the dance and music, which had stopped, resume as if nothing had halted it. 

The boy goes to the ruins of a hospital, mourning another young boy, Selim, via a candlelight vigil, with dozens of other youths. 

The pair take a bus trip and encounter all sorts of people, from a tired political protester to an arguing couple to a classical music trio. They also look out the window as a trio of people on bicycles pedal by them, oddly dressed in bright yellow raincoats.

The boy departs in the middle of the night, stowing aboard a huge, brightly lit ship whose destination is unknown. 

Alexander enters his old home. He looks about, exits out the back door, and into the sunny past where Anna and other friends are singing. They stop, ask him to join them, then they all dance, and soon, there is only the poet and his wife in motion. Then, she slowly pulls away, and he claims his hearing is gone. He also cannot see her, it seems. He calls out and asks how long tomorrow will be, after he had told her he refuses to go into the hospital, as planned. She tells him tomorrow will last eternity and a day.

==Cast==
* Bruno Ganz as Alexandre
* Isabelle Renauld as Anna
* Fabrizio Bentivoglio as the poet
* Achileas Skevis as the child
* Alexandra Ladikou as Annas mother
* Despina Bebedelli as Alexandres mother
* Helene Gerasimidou as Urania
* Iris Chatziantoniou as Alexandres daughter
* Nikos Kouros as Annas uncle
* Alekos Oudinotis as Annas father
* Nikos Kolovos as the doctor

== Soundtrack == score by ECM New Series label in 1998.

==Accolades==
{| class="wikitable"
|+ List of awards and nominations
! Award !! Category !! Recipients and nominees !! Result
|- 1998 Cannes Palme dOr||Theodoros Angelopoulos|| 
|- Prize of Theodoros Angelopoulos|| 
|- Greek State 1998 Greek Best Film||Theodoros Angelopoulos|| 
|- Best Supporting Eleni Gerasimidou|| 
|- Best Director||Theodoros Angelopoulos|| 
|- Best Screenplay||Theodoros Angelopoulos|| 
|- Best Music||Eleni Karaindrou|| 
|- Best Set Giorgos Ziakas, Costas Dimitriadis|| 
|- Best Costumes Giorgos Patsas|| 
|}

== See also ==
* Infinity plus one

== References ==
 

== External links ==
*  
*    
*    

 
 

 
 
 
 
 
 
 