Catch and Release (film)
{{Infobox film
| name           = Catch and Release
| image          = Catch and releaseposter.jpg
| caption        = Official film poster
| alt            = 
| director       = Susannah Grant
| producer       = Casey Grant Josh Siegel Matthew Tolmach Jenno Topping
| writer         = Susannah Grant
| starring       = Jennifer Garner Timothy Olyphant Kevin Smith Sam Jaeger Juliette Lewis
| music          = Brian Transeau Tommy Stinson
| cinematography = John Lindley
| editing        = Anne V. Coates
| distributor    = Columbia Pictures
| released       = Austin Film Festival October 20, 2006 United States January 26, 2007
| runtime        = 124 minutes
| country        = United states
| language       = English
| budget         = $25 million
| gross          = $16,158,487	
}} Erin Brockovich. It stars Jennifer Garner as a woman mourning her fiancés death who finds a more than welcoming shoulder to cry on in his best friend. Timothy Olyphant and filmmaker Kevin Smith co-star.

==Plot==

The film opens with Gray (Jennifer Garner) at the funeral of her fiancé, Grady.  They were supposed to be married that day, but due to a boating accident that happened during his bachelor party weekend, he was killed. Gray is very upset and eventually seeks solace by hiding in the bathtub.  Meanwhile, one of Gradys friends, Fritz (Timothy Olyphant), has seduced one of the catering staff and takes her into the bathroom, not realizing that Gray is there. Gray is forced to listen to them have sex. Once the caterer leaves, she pulls back the shower curtain, startling Fritz.

Gray visits the attorney to determine what will happen to Gradys estate. Grady did not have a will and since he and Gray were not married, she will not inherit his money.  The attorney reveals that Grady had an investment account with a million dollars in it, which Gray knew nothing about. She determines that she can no longer afford to rent the house that they had picked out and she was already living in.  With help from Gradys friends Dennis (Sam Jaeger) and Sam (Kevin Smith), she puts her things into storage and moves in with them into Gradys old room. Fritz, who lives in L.A., is also staying in the house during his visit.

Tensions continue between Gray and Fritz. As Gray investigates Gradys investment account, she finds that every month, he transferred $3,000 out of the account.

During the night, a cell phone rings. Gray wakes up Fritz, thinking the phone is his. He says that its not his and turns it off. At the same time, they realize that the phone must have belonged to Grady.  Gray grabs the phone first and turns it on to find ten voicemail messages. She listens to a string of messages from a woman who goes from cautiously concerned to increasingly angry as each message progresses.

Eventually, the other woman, Maureen (Juliette Lewis), and her son turn up, trying to get answers about Grady and the money. When Gray encounters them, she realizes that the boy is 3 years old, not 8, so the affair happened while she and Grady were together. Gray confronts Fritz about the revelation. She slaps him and he pins her arms against the wall. They kiss, but part and leave without a word.

Gray doesnt really want anything to do with Maureen, but the guys are reluctant to send her away. They all spend time together and get to know each other. Sam and Maureen begin to develop a connection. Tensions subside between Gray and Fritz, and they sleep together. Gray also begins to find she likes having Maureen and her son Matty around.
They all take a day trip to a river, where Sam teaches Matty to fly fish. Dennis tries to confess that he thinks hes in love with Gray, which does not end the way Dennis expects.

The results determine that Grady was not the father. Maureen is devastated, unsure how she will support her child without Gradys money. Gray goes to Gradys mother and tells her that she thinks that Grady must have known that the child wasnt his, but that his money could make a difference in the boys life. She offers Gradys mother the engagement ring in exchange for her giving Gradys money to Maureen.

The group gathers at the dedication ceremony for the peace garden that Dennis has built to memorialize Grady. Gradys mother gives Maureen a certified check. When Gray finds out, she offers the ring back, but Gradys mother tells her to keep it. She says that she never cared about the ring or the money, she just wanted her son back.

At the end of the film Gray goes to Malibu.  She goes to Fritzs house and finds him outside at the beach playing fetch with his dog.  She throws the ball and the dog runs after it. Fritz turns to see her standing there, and they embrace and kiss.

==Cast==
*Jennifer Garner as Gray Wheeler
*Timothy Olyphant as Fritz Messing
*Sam Jaeger as Dennis
*Kevin Smith as Sam
*Juliette Lewis as Maureen
*Joshua Friesen as Mattie
*Fiona Shaw as Mrs. Douglas
*Tina Lifford as Eve
*Georgia Craig as Persephone

==Production==
Catch and Release was filmed in the spring/summer of 2005, in Vancouver, British Columbia, Canada, with a reprise in December 2005. In July 2005, several scenes were filmed in Boulder, Colorado, where the story takes place.

==Soundtrack==
The original film score is produced by Brian Wayne Transeau (BT) and Tommy Stinson.
 Jewel and "Breathe (2 AM)" by Anna Nalick.

Music featured in Catch and Release is performed by:

# Foo Fighters -Razor 
# The Lemonheads - My Drug Buddy 
# Blinker the Star - A Nest For Two   
# The Magic Numbers - Mornings Eleven 
# Gary Jules - Pills 
# Steve Durand - Electrified And Ripe   
# New Radiant Storm King - The Winding Staircase    Audible - Sky Signal 
# Peter Maclaggan - Leaving The Ground 
# Joshua Radin - What If You 
# Gomez - These 3 Sins 
# Alaska! - Resistance   
# Paul Westerberg - Let The Bad Times Roll 
# The Swallows - Turning Blue    Andrew Rodriguez - What I Done 
# Death Cab For Cutie - Soul Meets Body  Doves - There Goes The Fear

==Reception==

===Box office=== In Her Just Like Because I Said So, other medium sized box office successes.

The film ended up with a final gross of $15,539,051 in the United States and $456,458 overseas, and ended up being a box office letdown.  

===Critical response===
Critical reaction towards the film has been largely negative.  The Rotten Tomatoes ratings was at 21% and 29% for Cream of the Crop respectively.  On Metacritic the film had a slightly better 43, indicated as "Mixed or Average Reviews."   The film did receive "Two Thumbs Up" from Richard Roeper and guest critic Govindini Murty. Even with most of the reviews being negative, a number of critics praised the performance given by Kevin Smith. 

==References==
 

==External links==
*  
*  
*   Review at Blast Magazine
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 