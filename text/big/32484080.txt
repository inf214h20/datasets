Tulku (film)
{{Infobox film
| name           = Tulku
| image          = Tulku DVD cover.jpg
| alt            = 
| caption        = DVD cover
| director       = Gesar Mukpo
| producer       = Kent Martin
| writer         = Gesar Mukpo
| narrator       = Gesar Mukpo Dzongsar Khyentse  Wyatt Arnold  Ogyen Trinley Dorje  Reuben Adrian Derksen
| music          = 
| cinematography = Pablo Bryant   Ethan Neville
| editing        = 
| studio         = National Film Board of Canada
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          =
}}
 reincarnated Tibetan Buddhist masters.

For over 700 years tulkus have been sought out as highly revered leaders and teachers of Tibetan Buddhism. Beginning in the 1970s, several tulkus have been identified as having incarnated in the West. These new, Western-born, very modern tulkus lead lives prone to culture clash and identity confusion.

==Background==
Gesar Mukpo, who wrote and directed Tulku, was born in 1973, the son of world-renowned Tibetan Buddhist master   as a tulku in Berkeley, California.

In the film, Mukpos British mother describes her scandalous marriage to a Tibetan monk, and her vision in a dream of a being who asked to be her son. When Gesar was born and was identified as a tulku, his father believed he could be a great teacher, but did not send him away to a monastery, believing it would separate him from his environment too much.

Mukpo, who grew up internationally, and whose father died in 1987, lives an ordinary secular life in Halifax Regional Municipality|Halifax, Nova Scotia. He has a daughter, is separated from his wife, and is a music video director and producer. Aware of the irony of his situation and the ambiguity of his life purpose, in the film he sets out to interview other Western tulkus to see if their disorientation is similar to his own, and to see how each has coped with the unique status of Western-born tulku. Mukpo travels to various locations to interview other young Western tulkus and the significant people in their lives. In the process, he relates his own life story and dilemmas as well.

==Synopsis==
Gesar Mukpo begins by interviewing a fellow Canadian, Dylan Henderson, who was the first Caucasian tulku discovered in the West, recognized in 1975 by Chögyam Trungpa Rinpoche as the incarnation of one of his teachers. The identification was confirmed by Rangjung Rigpe Dorje, the 16th Karmapa, who requested that Henderson come to the Rumtek Monastery in India for the rest of his life.  Chögyam Trungpa, however, recommended that he remain in the West. Henderson maintains his Buddhist studies and practices, but without the form and structure present in the East. He has a degree in anthropology and history.

In New York City, Mukpo visits his younger half-brother Ashoka Mukpo,  who was also identified as a tulku. Ashoka, like Gesar, leads a secular life, working in the U.S. division of Human Rights Watch.  Although he has not adopted the life of a Buddhist tulku, he has a thangka wall-hanging portrait of his previous incarnation, Khamyon Rinpoche, in his apartment.  Ashoka was enthroned as a tulku in Tibet, and found the experience, as well as the expectations of others, very intense and at times uncomfortable. He feels his path is not to be a teacher, wearing monks robes, but rather to help others and give back in ways appropriate to his location and culture.

  (left), in Northern India.]] Bir in Chinese occupation Dzongsar Khyentse The Cup, Travelers and Magicians) as well as a renowned Buddhist master, speaks eloquently about the development of the tulku system, and also about its flaws and possible failings, especially as Buddhism spreads in the West. Mukpo describes his own internal conflict between his Buddhist side and his Western side, lamenting both the seeming incongruousness of the practice when he became a teen and wanted to fit in, and the pressure and obligation he has felt because he bears the designation of tulku.

Mukpo meets a 20-year-old from San Francisco, Wyatt Arnold, who has been studying Tibetan in India for the past year.  Arnold was identified as a tulku as a young child and enthroned at the age of five.  He was slated to go to India at that time, but his parents decided against it. Arnold speaks about his early memories of his former incarnation, and about his fond memories of his childhood Tibetan Buddhist teacher in the U.S.,  . September 24, 2009.  and seeks advice and feedback from Mukpo, who is 14 years his senior.

In   for a year. While in Nepal, Mukpo also interviews Reuben Derksen, who was born in Amsterdam in 1986, raised in Nepal and Bhutan, and recognized as a tulku at the age of 11.  Derksen is the most cynical of all the young tulkus Mukpo interviews, having had a largely negative experience at the Tibetan Buddhist monastery in India he lived at for three years following high school. Although he no longer considers himself a Buddhist, he still goes back annually to emcee the weeklong Buddhist ceremony in Bhutan, mainly because his presence makes the people there so happy.

Returning home to Halifax and his family, Mukpo reflects upon his life and upon the experiences of the tulkus and teachers he has interviewed. He admits that there are no easy answers to the complications and contradictions of being a Westerner identified as a Tibetan tulku in a modern, rapidly changing world. One compensation to the culture conflict is the meaningful connection formed to beloved teachers, communities, and heritages. Speaking about his fellow Western tulkus, he concludes, "There is no certain path for any of us, other than the path of self-discovery."

==Cast==
*Gesar Mukpo
*Dylan Henderson
*Ashoka Mukpo Dzongsar Khyentse Rinpoche
*Wyatt Arnold
*Ogyen Trinley Dorje
*Reuben Adrian Derksen
*Diana Mukpo

==Production==
To fund the film, Gesar Mukpo approached the National Film Board of Canada, as part of the Reel Diversity Competition for emerging filmmakers of colour. According to Mukpo:
 I had applied to the Reel Diversity program before and not gotten it. So I was skeptical about applying again. A couple of days before the deadline, I hadnt written anything, and I sat down and thought, Screw it, I might as well tell my story.

I didnt want to look and fish for some story. It was just there. I poured it out in two paragraphs.... The idea wrote itself.

This was something that was real, it was true, and its a story I could tell in a way that nobody else could. As a filmmaker, thats what you want — to make people connect with something real and intimate. I felt this story had that. Its my story. }}

Tulku took two years to complete, and was shot on location in Nova Scotia, Florida, New York City, India, and Nepal. While Mukpo was filming in Bir Tibetan|Bir, two local Westerners offered to take him and his cameraman paragliding for free, resulting in beautiful aerial shots of Northern India.  The film also features rare archival footage from Tibet, and archival footage and photographs of Tibetan masters such as Chögyam Trungpa Rinpoche, Rangjung Rigpe Dorje, and Chagdud Tulku Rinpoche.

The soundtrack to the film includes two prominently featured songs by Don Brownrigg: "In It", and "Remember Home".

==Release, broadcast, and DVD==
Tulku premiered on May 25, 2009, at the DOXA Documentary Film Festival.  It was also an official selection at the Vancouver International Film Festival,  the International Buddhist Film Festival,  the Buddhist Film Festival Europe,  the Atlantic Film Festival,  and the Calgary International Film Festival.  

The film was televised on August 10, 2010 on the CBC News Networks program, The Passionate Eye.  The film also screened at Boulder Theater in Boulder, Colorado on  August 18, 2010. 

The special edition DVD of the film was released in March 2011 by Festival Media.   The DVD includes 60 minutes of bonus features, including a 35-minute uncut in-depth interview with Dzongsar Khyentse Rinpoche, and a candid 20-minute post-film interview with Gesar Mukpo reflecting on the film and his life and place in the world.

==Critical reception==
Tulku has received favorable print reviews. Shambhala Sun called it "intensely personal",  and The Coast described the documentary as "both inspiring and, like Mukpo, endearingly down to earth."  The Epoch Times calls the documentary "a first-of-its-kind film", adding that "Gesar Mukpo ... tells a vivid story". 

Angela Pressburger, daughter of famed British filmmaker Emeric Pressburger, deemed Tulku an "intimate and honest exploration". Writing in the Shambhala Times, she also observed, at the films premiere:

 

==See also==
*Unmistaken Child
*My Reincarnation
*Rinpoche

==References==
 

==External links==
* 
*  at the Internet Movie Database
* 
* 

 

 
 
 
 
 
 
 