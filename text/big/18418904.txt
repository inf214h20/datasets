The Heart's Cry
{{Infobox film
| name           = The Hearts Cry
| image          = 
| image_size     = 
| caption        = 
| director       = Idrissa Ouedraogo
| producer       = 
| writer         = Jacques Akchoti Robert Gardner Idrissa Ouedraogo
| narrator       = 
| starring       = Richard Bohringer Saïd Diarra
| music          = Henri Texier
| cinematography = Jean-Paul Meurisse Jean Monsigny
| editing        = Luc Barnier
| distributor    = 
| released       =  : April 5, 1995
| runtime        = 86 minutes
| country        = Burkina Faso France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1994 Burkina French drama film directed by Idrissa Ouedraogo.
==Plot==
Moctar is a young boy who, although born in France, has grown up in Mali. At the age of eleven, he moves with his family to live in Paris. Moctar struggles to adjust to life in France, and is homesick for Africa. He begins to see visions of a hyena in the street. When he tells people, nobody believes him. He is laughed at by his schoolmates and sent to the school psychologist. He meets a man in the street called Paulo who helps Moctar to understand his visions. {{cite web
  | last = Brennan
  | first = Sandra
  | authorlink = 
  | title = Le Cri du Coeur > Overview
  | publisher = AllMovie
  | url = http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:134091
  | accessdate =2008-07-13 }}  {{cite web
  | last = Berardinelli
  | first = James
  | authorlink = James Berardinelli
  | title = Review: The Cry of the Heart
  | publisher = ReelViews
  | year = 1995
  | url = http://www.reelviews.net/movies/c/cry_heart.html
  | accessdate =2008-07-13 }} 
==Cast==
*Richard Bohringer as Paulo
*Saïd Diarra as Moctar
*Félicité Wouassi as Saffi
*Alex Descas as Ibrahim Sow
*Clémentine Célarié as Deborah
*Jean-Yves Gautier as Paul Guerin
*Cheik Doukouré as Mamadou
*Adama Ouédraogo as Adama
*Ginette Fabet as Firmine
*Adama Kouyaté as Grandfather
*Valérie Gil as Miss Romand
*Grégoire Le Du as Olivier
==Reception==
It won the OCIC Award - Honorable Mention at the 1994 Venice Film Festival.
==References==
 
==External links==
* 
*  
 
 
 
 
 
 
 
 
 
 