Rahasyam (1969 film)
{{Infobox film 
| name           = Rahasyam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = KP Kottarakkara
| starring       = Prem Nazir Sheela Jayabharathi Adoor Bhasi
| music          = BA Chidambaranath
| cinematography = PB Mani
| editing        = TR Sreenivasalu
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by KP Kottarakkara. The film stars Prem Nazir, Sheela, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by BA Chidambaranath.   

==Cast==
 
*Prem Nazir as Babu & CID K.K.Nair (Double Role)
*Sheela as Ammini
*Jayabharathi as Sulochana
*Adoor Bhasi as Shankaran
*Jose Prakash as Damodaran
*CR Lakshmi
*Friend Ramaswamy
*K. P. Ummer as Prasad
*Leela Meena as Mrs. Shyamala Thambi
*N. Govindankutty as Dance Master Govindankutty
*Paravoor Bharathan as Vikraman
 

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aayiram Kunnukalkkappurath || S Janaki || Sreekumaran Thampi || 
|-
| 2 || Hum To Pyar Karne Aaye || P Jayachandran, B Vasantha, CO Anto || Sreekumaran Thampi || 
|-
| 3 || Mazhavillu Kondo || P. Leela || Sreekumaran Thampi || 
|-
| 4 || Mazhavillukondo Bit || P. Leela || Sreekumaran Thampi || 
|-
| 5 || Thottaal Veezhunna Praayam || Kamukara || Sreekumaran Thampi || 
|-
| 6 || Urangan Vaikiya || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 