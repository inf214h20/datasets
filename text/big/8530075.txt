Woman Sesame Oil Maker
{{Infobox film 
  | name = Woman Sesame Oil Maker
  | image =Woman Sesame Oil Maker.jpg
  | caption =  Xie Fei 
  | producer = 
  | writer =  
  | starring = Siqin Gaowa
  | music = Wang Liping
  | cinematography = Bao Xiaoran
  | editing = 
  | distributor = Facets Multimedia (Region 1 DVD) 1993 (China) February 16, 1994 (New York City only)
  | runtime = 105 minutes
  | country = China
  | language = Mandarin
  | budget = 
}}
 Chinese film. In the English speaking world it has been released in the United Kingdom under the title Women from the Lake of Scented Souls (the more literal translation) and more recently on DVD as Woman Sesame Oil Maker.  It is adapted from a novel by Zhou Daxin (周大新), "The Fragrant Oil Mill by the Lake of Scented Souls" (香魂塘畔的香油坊).
 Xie Fei and tells the story of a woman in a small village in Hebei  who runs a small sesame oil business that becomes unexpectedly successful, but who then uses her money to buy for her mentally disabled son a peasant bride.

The film won the Golden Bear for Best Film at the 43rd Berlin International Film Festival in 1993, a prize it shared with Ang Lees The Wedding Banquet.    

==Plot==
Illiterate villager Second Sister Xiang (Siqin Gaowa) was sold as a tongyangxi (child bride) to her present husband, a man with a lame leg, when she was seven.  She has an intellectually disabled son Dunzi, who suffers from epileptic fits and is now grown up, and a younger daughter Zhier.  Through her diligence she has started a successful sesame oil mill and now becomes the richest person in her village.

A Japanese lady investor decides to invest in her business after visiting her mill.  Meanwhile, Sister Xiang tries to find a bride for her son.  She engineers her sons marriage with Huanhuan (Wu Yujuan), a peasant girl whose family is in dire financial straits.  
 adulterous affair with family friend Ren, who fathered her daughter Zhier.

Ren later initiates a break-up with Sister Xiang. Sister Xiang falls ill.  She is heartbroken, despite her sesame oil being awarded top prize by the provincial government.  

Sister Xiang gradually realizes that Huanhuan, like her, suffers as a kindred spirit.  She decides to release Huanhuan by asking Huanhuan to go for a divorce. But Huanhuan states that her life is already ruined.  She then breaks down.

== References ==
 

==External links==
*  
*  
*  
*   at the Chinese Movie Database
*   podcast of a lecture by Dr. Julian Ward, University of Edinburgh

 

 
 
 
 
 
 
 
 