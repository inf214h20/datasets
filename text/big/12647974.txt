The Jewish Steppe
{{Multiple issues|
 
 
}}
{{Infobox film
| name           = The Jewish Steppe
| image          = 
| alt            =  
| caption        = Crimea seen in red
| director       = Valery Ovchinnikov
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 16 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
 documentary about Soviet agrarian commune was destroyed.  

==Summary==

“Why should the Jewish people go to Palestine where the land is less productive and requires big investments?” a Jewish newspaper asked at the time of the settlement, “Who go so far if the fertile Crimean land is beckoning to the Jewish people?” 

At the turn of the twentieth century, antisemitism was common in Russia. Legislation was passed that limited Jews to working only in retail and handicrafts. When these laws were lifted, around the time the Russian Revolution of 1917, pogroms broke out. Approximately 30,000 Jews left for the Crimean Peninsula. Rare pictures and film footage from the Russian State Film and Photo Archives are narrated in The Jewish Steppe to explain how they lived there.       

One newspaper wrote that everyone on the steppe was competing with each other to work harder. When, in 1931, famine occurred in Russia, the Jewish settlements continued to have an abundant harvest that helped feed the rest of the nation during its grain shortage.   

Two years after it was settled, the area was recognized as the Soviet Unions first Jewish district. They went on to establish schools and two colleges. 
 
“As a result of healthy life and labor,” a local farmer commented in a newspaper, “peace of mind is replacing the nervousness typical for Jewish people, movements have become measured, and faces have become calm.” He went on to say that these changes were particularly noticeable in the younger generation. 

Under Joseph Stalin, the commune was destroyed, leaving only archival footage and documents.

==See also==
*History of the Jews in Russia and the Soviet Union
*Joseph Stalin
*Crimean Peninsula

Other documentaries about Jews of the Diaspora: Jews of Iran
*Trip to Jewish Cuba
*Queen of the Mountain
*Next Year in Argentina
* 
*In Search of Happiness
*Balancing Acts
*Baba Luba
*My Yiddish Momme McCoy
*A Home on the Range
*From Swastika to Jim Crow
*Song of a Jewish Cowboy

==References==
*{{cite web
  | title = The Jewish Steppe
  | work =
  | publisher =Cinema Guild
  | date =
  | url =http://cinemaguild.com/catalog/catalog.htm?http%3A//cinemaguild.com/mm5/merchant.mvc%3FScreen%3DPROD%26Store_Code%3DTCGS%26Product_Code%3D1881%26Category_Code%3DJS
  | accessdate = August 7  }}

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 
 