Chirabandhavya
 
{{Infobox film|
| name = Chirabandhavya
| image = 
| caption =
| director = M. S. Rajashekar
| writer = Sai Suthe
| starring = Shivrajkumar  Subhashri  Bharathi   Chi Guru Dutt
| producer = R Niveditha   N Prema   K Doreswamy
| music = Hamsalekha
| cinematography = Mallikarjun
| editing = T. Shashikumar
| studio = Shashwathi Chitra
| released =  
| runtime = 134 minutes
| language = Kannada
| country = India
| budgeBold textt =
}} Kannada drama romance drama film directed by M. S. Rajashekar and produced by Shashwathi Chitra. The film features Shivarajkumar, Subhashri and Bharathi in the lead roles.  The films plot is based on the novel of the same name written by Sai Suthe.  

== Cast ==
* Shivarajkumar 
* Subhashri
* Bharathi (Sukanya)
* Chi Guru Dutt
* K. S. Ashwath
* Thoogudeepa Srinivas
* Pandari Bai
* Mynavathi
* Girija Lokesh
* Ashwath Narayan
* Pramila Joshai
* Avinash

== Soundtrack ==
The soundtrack of the film was composed by Hamsalekha. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Cheluva Cheluva
| extra1 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Thaatha Thaatha
| extra2 = Shivarajkumar
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Raam Raam
| extra3 = S. P. Balasubrahmanyam 
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Koli Kalla
| extra4 = S. P. Balasubrahmanyam 
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Baaligu Bhoomigu Rajkumar
| lyrics5 = Hamsalekha
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 