Les Espions
{{Infobox film
| name           = Les Espions
| image          = Lesespions.jpg
| image_size     = 
| caption        = 
| director       = Henri-Georges Clouzot
| producer       = 
| writer         = Henri-Georges Clouzot Jerome Geronimi
| narrator       =  Sam Jaffe Peter Ustinov
| music          = Georges Auric Christian Matras
| editing        = 
| distributor    = 
| released       = 1957
| runtime        = 
| country        = France Italy
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1957 French film film directed by written by Sam Jaffe, Martita Hunt and the directors wife, Véra Clouzot. The music was composed by Georges Auric.

==Plot summary==
The plot concerns a doctor at a run-down psychiatric hospital, who is offered a large sum of money to shelter a new patient. Soon the place is full of suspicious and secretive characters, all apparently international secret agents trying to find out who and what the patient is.

== External links ==
*   at the Internet Movie Database
*  

 

 
 
 
 
 
 
 
 
 

 
 