Les Gens qui s'aiment
{{Infobox film
| name           = Les Gens qui saiment
| image          = Lesgensquisaimentposter.jpg
| director       = Jean-Charles Tacchella
| producer       = Gérard Jourdhui
| writers        = Jean-Charles Tacchella Richard Berry Jacqueline Bisset Julie Gayet Bruno Putzulu
| music          = Raymond Alessandrini
| cinematography = Dominique Chapuis   
| editing        = Anna Ruiz
| released       = July 5, 2000
| runtime        = 90 minutes
| country        = France Belgium Luxembourg Spain
| language       = French
}} Richard Berry and Jacqueline Bisset. It was released in France on 5 July 2000.

==Plot==
Jean-Francois (Berry) is the presenter of a love story-based radio show, he himself has a long-term erratic open relationship with Angie (Bisset). Angie has two daughters, the eldest is the married archetypal housewife, a lifestyle Angie cannot understand. But Winnie, her youngest feels restrained by the strangleholds of a traditional relationship. 

==Cast== Richard Berry as Jean-Francois
*Jacqueline Bisset as Angie
*Julie Gayet as Winnie
*Bruno Putzulu as Laurent
*Marie Collins as Juliette the Housekeeper

==References==
 

==External links==
*  
*  

 
 
 
 
 


 