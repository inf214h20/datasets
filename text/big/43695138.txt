Hoppy Serves a Writ
{{Infobox film
| name           = Hoppy Serves a Writ
| image          = Hoppy Serves a Writ poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Archainbaud
| producer       = Harry Sherman 
| screenplay     = Gerald Geraghty William Boyd Andy Clyde Jay Kirby Victor Jory George Reeves Jan Christy 
| music          = 
| cinematography = Russell Harlan
| editing        = Sherman A. Rose 
| studio         = Harry Sherman Productions
| distributor    = United Artists
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film William Boyd as Hopalong Cassidy.  The supporting cast features Andy Clyde, Victor Jory and George Reeves. The film remains noteworthy today as the debut performance of unshaven newcomer Robert Mitchum, who made an impression upon the studio by generating a surprising fan mail response exactly as Clark Gable had after playing an extremely similar unshaven role in The Painted Desert, a Western starring William Boyd produced a dozen years earlier.  

==Cast== William Boyd as Hopalong Cassidy
*Andy Clyde	as California Carlson
*Jay Kirby as Johnny Travers
*Victor Jory as Tom Jordan
*George Reeves as Steve Jordan
*Jan Christy as Jean Hollister Hal Taliaferro	as Henchman Greg Jordan
*Forbes Murray as Ben Hollister
*Robert Mitchum as Henchman 
*Byron Foulger as Danvers 
*Earle Hodgins as Bartender, Desk Clerk
*Roy Barcroft as Rancher Tod Colby

==References==
 

==External links==
*   in the Internet Movie Database

 
 
 
 
 
 
 

 