Bankomatt
 
{{Infobox film
| name           = Bankomatt
| image          = Bankomatt.jpg
| caption        = Film poster
| director       = Villi Hermann
| producer       = Villi Hermann Enzo Porcelli
| writer         = Villi Hermann Giovanni Pascutto
| starring       = Bruno Ganz
| music          = Franco Piersanti
| cinematography = Carlo Varini
| editing        = Fernanda Indoni
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Italy Switzerland
| language       = Italian
| budget         = 
}}

Bankomatt is a 1989 Italian-Swiss drama film directed by Villi Hermann. It was entered into the 39th Berlin International Film Festival.   

The story follows a man planning and conducting a bank robbery. It is revealed he was formerly an employee of the bank, and is acting out of disgruntlement towards the current boss.

==Cast==
* Bruno Ganz as Bruno
* Giovanni Guidelli as Stefano
* Francesca Neri as Maria
* Omero Antonutti as Ernesto Soldini
* Pier Paolo Capponi as Impiegato banca
* Roberto De Francesco as Amico di Stefano
* Fabrizio Cerusico
* Andrea Novicov
* Renata Pozzi
* Tatiana Winteler

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 