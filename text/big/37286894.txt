Making a Man
{{Infobox film
| name           = Making a Man
| image          =
| caption        =
| director       = Joseph Henabery
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Peter B. Kyne (story "Humanizing Mr. Winsby") Albert S. Le Vino (scenario) Jack Holt
| cinematography = Faxon M. Dean
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 6 reels
| country        = United States
| language       = Silent(English intertitles)
}} Jack Holt, and based on the Peter B. Kyne story "Humanizing Mr. Winsby".  

==Cast== Jack Holt - Horace Winsby
*J. P. Lockney - Jim Owens
*Eva Novak - Patricia Owens
*Bert Woodruff - Henry Cattermole
*Frank Nelson - Shorty
*Robert Dudley - Bailey

==Preservation status==
This film is now considered a lost film.  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 