Joe Palooka, Champ
{{Infobox film
| name           = Joe Palooka, Champ
| image          = "Joe_Palooka,_Champ"_(1946).jpg
| image_size     =
| caption        =
| director       = Reginald Le Borg
| producer       = Hal E. Chester
| writer         = Cy Endfield Albert DePina Hal E. Chester
| narrator       =
| starring       = Alexander Laszlo
| cinematography = Benjamin H. Kline
| editing        = Bernard W. Burton
| distributor    = Monogram Pictures
| released       =  
| runtime        = 70 min.
| country        = United States
| language       = English
}}

Joe Palooka, Champ is a 1946 American film featuring the popular comic-strip boxer Joe Palooka.  This film is the beginning of a series of eleven sequels from Monogram:   

* Gentleman Joe Palooka (1946) 
* Joe Palooka in the Knockout (1947) 
* Joe Palooka in Fighting Mad (1948) 
* Joe Palooka in Winner Take All (1948) 
* Joe Palooka in the Big Fight (1949) 
* Joe Palooka in the Counterpunch (1949) 
* Joe Palooka Meets Humphrey (1950) 
* Joe Palooka in Humphrey Takes a Chance (1950) 
* Joe Palooka in the Squared Circle (1950) 
* Joe Palooka in Triple Cross (1951) 

== Cast ==
* Leon Errol as Knobby Walsh  
* Joe Kirkwood, Jr. as Joe Palooka  
* Elyse Knox as Anne Howe  
* Eduardo Ciannelli as Florini  
* Joe Sawyer as Lefty  
* Elisha Cook, Jr. as Eugene  
* Warren Hymer as Ira Eyler   Robert Kent as Ronnie Brewster  
* Sam McDaniel as Smoky  
* Sarah Padden as Mom Palooka   Michael Mark as Pop Palooka  
* Dave Willock as Mr. Rodney   
* Lou Nova as Al Costa  
* Eddie Gribbon as Louie the Louisiana Lion  
* J. Farrell MacDonald as Long-Count Bowman  

== External links ==
*  
*  

 

 
 
 
 
 
 