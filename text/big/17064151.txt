The Raindrop
{{Infobox film name           = The Raindrop image          = GuleGule.jpg image_size     =  caption        = Theatrical Poster director       = Zeki Ökten producer       =   writer         = Fatih Altinoz narrator       =  starring       =   music          = Engin Duzyol cinematography = Ferenc Pap  editing        = Nevzat Dişiaçık studio         = UFP distributor    =  released       =   runtime        = 108 minutes country        = Turkey language       = Turkish budget         =  preceded_by    =  followed_by    = 
}}
 
The Raindrop ( ) is a 2000 Turkish comedy-drama film, directed by Zeki Ökten, which chronciles the friendship of five elderly residents of Bozcaada. The film, which went on nationwide general release across Turkey on  , won four awards at the 37th Antalya "Golden Orange" International Film Festival, including the Golden Orange for Best Film. It was described as, "sappy yet well narrated," by author Rekin Teksoy. The Turkish title "güle güle" means "goodbye" said by the one who stays, not by the one who leaves.   

==Synopsis==
This film tells the story of a circle of five friends (four men and a woman) who are longtime residents of the Aegean island Bozcaada. The group includes a good-hearted auto mechanic, a retired military officer and his wife, an emotionally volatile bachelor, and Galip, a hopeless romantic who has corresponded with a Cuban woman, Rosa, for thirty-five years. When Galip at last vows to join her in Cuba, his friends make it their mission to reunite the pair.

==Cast==
*Eşref Kolçak as Celal
*Zeki Alasya as İsmet
*Metin Akpınar as Galip
*Yıldız Kenter as Zarife
*Şükran Güngör as Şemsi
*Yüksel Aksu
*Ayşegül Aldinç
*Haluk Bilginer
*Kaan Çakır as Dilaver
*Ece Uslu
*Serra Yılmaz

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 


 
 