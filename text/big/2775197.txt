Love (1991 film)
{{Infobox film
| name = Love
| image = Lovehindifilm.jpg
| image size = 
| caption = LP Vinyl Records Cover Suresh Krishna
| producer = Shyam Bajaj Suresh Krishna
| starring = Salman Khan  Revathi
| music = Anand-Milind
| cinematography =S.M.Anwar
| editing = M. S. Shinde
| distributor =
| released = 30 August 1991
| runtime =
| country = India Hindi
| budget = 1.50 crores
| gross = 
}}
 Suresh Krishna. Telugu blockbuster Venkatesh and Revathi. It could not repeat the success of the original and ended up as an average grosser. The makers changed the tragic climax from the original one to a happy ending.

==Plot==
Jailed as a juvenile for killing his abusive father (Sudhir Kumar) who is responsible for his mothers suicide, Prithvi (Salman Khan) is unable to stand any atrocity. He meets Maggie Pinto (Revathi Menon) and after a few chance meetings, they both fall in love. Maggie takes Prithvi to meet her parents, but they reject him after learning about his past. Maggie and Prithvi persist, so Maggies mother, Stella Pinto (Rita Bhaduri), calls the police and has Prithvi jailed. Guruji (Amjad Khan) comes to Prithvis aid and bails him out. Prithvi and Maggie continue to meet, but Stella finds out and intervenes and sends rogues to attack Prithvi, during which Maggie is wounded. How this affects everyone close to her is the crux of the story.

==Cast==
* Salman Khan - Prithvi
* Revathi Menon - Maggie
* Amjad Khan - Guruji
* Harish Kumar - Prithvis Friend
* Shafi Inamdar - John
* Rita Bhaduri - Stella
* Suhas Joshi - Prithvis mother Sudhir - Prithvis Father

==Soundtrack==
{{Infobox album |  
 Name = Love |
 Type = Soundtrack |
 Artist = Anand-Milind |
 Cover = Lovemusiccover.jpg|
 Released = 1991 |
 Recorded = | Feature film soundtrack |
 Length = |
 Label = Venus Records & Tapes|
 Producer = Anand-Milind | 
 Reviews = |

 Last album = Lahu Luhan   (1991) |
 This album = Love (1991) |
 Next album = Mohabbat Mohabbatein  (1991) |
}}

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! #!!Title !! Singer(s)

|-
| 1
| "Sathiya Ye Tune Kya" 
| S.P. Balasubrahmanyam, K.S. Chitra
|-
| 2
| "Aaja Aaja Give Me A Kiss"
| S.P. Balasubrahmanyam, K.S. Chitra
|-
| 3
| "My Love Meri Priyatama"
| S.P. Balasubrahmanyam, K.S. Chitra
|-
| 4
| "We Are Made For Each Other"
|  S.P. Balasubrahmanyam, K.S. Chitra
|-
| 5
| "I Am Sorry"
| S.P. Balasubrahmanyam
|-
| 6
| "Aayi Bahar Khilte Huvye Gul" 
| Sapna Mukherjee
|}

==External links==
*  

 

 
 
 
 
 
 
 


 
 