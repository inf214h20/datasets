Kalyanaraman (2002 film)
 
{{Infobox film
| name           = Kalyanaraman
| image          = Kalyanaraman.jpg
| image_size     =
| caption        = Shafi
| Lal
| writer         = Benny P Nayarambalam
| narrator       = Dileep  Kunchacko Boban Navya Nair
| music          = Berny Ignatius
| cinematography = P. Sukumar
| editing        = Harihara Puthran
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Shafi starring Dileep and Telugu as Kalyanaramudu(2003) starring Venu, Suman (actor)|Suman, Prabhudeva and Nikita Thukral.

==Plot==
Thekkedathu Ramankutty (Dileep (actor)|Dileep) is a marriage organizer. He meets Gauri (Navya Nair) while arranging the marriage for her sister Radhika (Jyothirmayi), daughter of a family friend Ambattu Thampi (Lalu Alex). Radhikas fiancee does not turn up for the marriage. Ramankutty suggests that his brother Dr. Sivadas (Bobby Alumoodan) will be ready for the marriage and Dr. Sivadas and Radhika gets married on the same day itself. Ramankutty and Gauri fall in love. But on the day of their engagement, Radhika dies in a kitchen fire accident.

When Thampi visits the astrologer Meppattu Thirumeni (Thilakan) to see the horoscope match, he tells them that women married into the Thekkedathu family are fated to die young. Thampi, scared for his only remaining daughters life, asks Ramankutty to back out of the marriage. Saddened and yet wanting to do the right thing, Ramankutty calls off the wedding on the pretense of not being comfortable with Gauris close relations with her cousin Unni (Kunchako Boban). Devastated by this allegation, Gauri backs out of the marriage, only to find the truth later. She runs out in search of Ramankutty and gets involved in a road accident on the way. She is rushed to the hospital and while still battling for life, she wishes that Ramankutty tie the wedding knot before her operation. Ramankutty ties the knot, fearing the worst. Miraculously, Gauri comes out alive.

The story is told in flashback by an aged Ramankutty who bids farewell to his spellbound teenage audience at the temple, and joins his wife Gauri in lighting the lamp.

==Cast== Dileep as Thekkedathu Ramankutty
* Kunchako Boban as Unni (Cousin of Gouri)
* Navya Nair as Gouri Lal as Thekkedathu Achuthankutty (Elder brother of Ramankutty)
* Lalu Alex as Ambattu Thampi (Father of Gouri and Radhika)
* Jyothirmayi as Radhika (Elder sister of Gouri)
* Boban Alumoodan as Dr. Sivadas (Elder brother of Ramankutty and Younger brother of Achuthankutty) Innocent as Ponjikkara Kesavan
* Salim Kumar as Pyari
* T. P. Madhavan
* Unnikrishnan Namboothiri as Thekkedathu Gopalakrishnan
* Thilakan as Meppattu Thirumeni
* Kalaranjini as Saraswathy (Mother of Gouri and Radhika)
* Kochu Preman as U. P. P. Menon
* Jose Pellissery
*Narayanankutty Siddique as Doctor
* Narayanankutty Reena
* Subbalakshmi as Karthyani
* Dimple Rose

==Soundtrack==
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Lyricist !! Music
|-
| 1 || Kadhayile rajakumariyum... || K. J. Yesudas|Dr. K. J. Yesudas || S. Rameshan Nair || Berny Ingnatious
|-
| 2 || Thinkale poothinkale... || Afsal (singer)|Afsal&M.G.Sreekumar || Kaithapram || Berny Ingnatious
|- Afsal (singer)|Afsal || - || Berny Ingnatious
|-
| 4 || Raakadal kadanjedutha... || K. J. Yesudas|Dr. K. J. Yesudas || S. Rameshan Nair || Berny Ingnatious
|- Sujatha || S. Rameshan Nair || Berny Ingnatious
|- Innocent & others || - || - 
|}

==External links==
*   Oneindia

 
 
 
 
 