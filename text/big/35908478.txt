The Purple Mask
 
{{Infobox film
| name           = The Purple Mask
| image          = The Purple Mark 55.jpg
| image size     =
| caption        = 
| alt            =
| director       = H. Bruce Humberstone
| producer       = Howard Christie
| executive producer = 
| writer         = 
| starring        = Tony Curtis Angela Lansbury Gene Barry
| music          = 
| cinematography = 
| studio         = Universal International Pictures
| distributor    = Universal Pictures
| released       = 1955
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    =
| followed by    =
}}
The Purple Mask is a 1955 swashbuckler film, starring Tony Curtis and set in 1803 France. 

==Cast==
* Tony Curtis as Rene de Traviere / The Purple Mask
* Colleen Miller as Laurette de Latour 
* Angela Lansbury as Madame Valentine 
* Gene Barry as Capt. Charles Laverne
* Allison Hayes as Irene de Bournotte
 

==See also==
 
* 1955 in film
* List of adventure films of the 1950s
* List of American films of 1955
* List of Universal Pictures films
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 