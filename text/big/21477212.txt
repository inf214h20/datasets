Kalishankar (2007 Bengali film)
 
{{Infobox film
| name = Kalishankar
| image =
| caption = Poster of Kalishankar
| director =Prashanta Nanda
| writer =
| starring = Prasenjit Chatterjee Anu Chowdhury Anuvab Mohanty Jisshu Sengupta Ashish Vidyarthi Victor Banerjee  Swastika Mukherjee
| producer =
| distributor =
| cinematography =
| editing =
| released =  
| country        = India
| budget       =
| gross        =
| runtime      = 120 minutes Bengali
| music        = Prashanta Nanda Shanti Raj
}} Bengali film directed by Prashanta Nanda. The film is starring Prasenjit Chatterjee, Anu Chowdhury, Anuvab Mohanty, Jisshu Sengupta, Ashish Vidyarthi, Swastika Mukherjee and Victor Banerjee. This Cinema of West Bengal|Bengali-Oriya film is a remake of Rakesh Roshans blockbuster Karan Arjun on the theme of reincarnation. Director Prashant Nanda has turned the story around. Instead of being the story of the offsprings (played by Salman Khan and Shahrukh Khan in the original) this is the story a pair of grandchildren and their grandfathers revenge on their wrongdoers. He play the grandfather, naturally. The film has a lot of stick-wielding action.  Shoma A. Chatterji of www.screenindia.com commented that this movie is Manmohan Desai wine in Bengali bottle.Kali-Shankar is the similarity with old Manmohan Desai films wherein brothers separated in childhood grow up to find themselves on two sides of the fence. Then, when they recognise their blood link through a birthmark or a talisman, they join forces to fight the enemy together. 
{{Cite web
|url=http://www.screenindia.com/old/fullstory.php?content_id=18069
|title=Kali-Shankar:Manmohan Desai wine in Bengali bottle
|publisher=www.screenindia.com
|accessdate=2009-02-10
|last=
|first=
}}
 

==Plot==
Kali, Shankar and Tithi are the three motherless children of the acting priest of a local Krishna temple in an anonymous small town/village in West Bengal. The local MLA Debu Soren’s henchmen kill their father because he insists on standing witness in court against a murder committed at Soren’s behest. Kali, the elder brother, kills the corrupt lawyer as he walks out of the courtroom after the judge has delivered his judgement. Kali goes to jail for ten years. Shankar meets with a freak accident and is brought up by a local Muslim fakir. But the accident has left him with a memory loss and a penchant for violence. He grows up to be one of Debu Soren’s favourite henchmen. The little girl Tithi, who is picked off the streets by the maid of Debu’s mistress, grows up to become a wayward girl on the verge of turning alcoholic.

After ten long years, Kali is released. He returns to the temple to meet his grandfather who was away when Kali’s father was killed. The grandfather, a temple priest, encourages the grandson to avenge his father’s death. Kali, Shankar and Tithi are united and from then on, it is one long chain networking of maramari, dishoom-dishoom, blood and gore and the killing of the criminals one after the other in the same way their father was killed - by electric shock. The two young men come home to roost and it is one big happy family in the temple with their two girlfriends pitching in and Tithi hoping to tie the knot with the handsome young police officer.

==Cast==
*Prasenjit Chatterjee .... Kali
*Anuvab Mohanty .... Shankar
*Ashish Vidyarthi .... Debu Soren
*Anu Chowdhury .... Lover of Kali
*Swastika Mukherjee .... Lover of Shankar
*Jisshu Sengupta .... Policeman
*Victor Banerjee .... Grandfather of Kali and Shankar
*Sagarika .... Tithi
*Rajatabha Dutta .... Gittu Hotelwalla
*Laboni Sarkar
*Shankar Chakraborty

{| class="wikitable"
|+Crew
!Job title
!Member
|- Director
|Prashanta Nanda
|- Producer
|rowspan=6|
|- Presenter
|- Music Director
|- Cinematographer
|- Editor
|- Playback Singer
|}

==See also==
*Kalishankar

==References==
 

==External links==
* 
* 
 

Dishoom

 
 
 
 