Storm over Mont Blanc
{{Infobox film
| name           = Storm over Mont Blanc
| image          = Storm over Mont Blanc Poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = German theatrical release poster
| director       = Arnold Fanck
| producer       = {{Plainlist|
* Gabriel Levy
* Harry R. Sokal
}}
| writer         = Arnold Fanck
| narrator       = 
| starring       = {{Plainlist|
* Leni Riefenstahl
* Sepp Rist
* Ernst Udet
}}
| music          = Paul Dessau
| cinematography = {{Plainlist|
* Hans Schneeberger
* Richard Angst
* Sepp Allgeier
}}
| editing        = Arnold Fanck
| studio         = Aafa-Film AG
| distributor    = Aafa-Film AG
| released       =  
| runtime        = 93 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}

Storm over Mont Blanc ( ) is a 1930 German film written and directed by Arnold Fanck and starring Leni Riefenstahl, Sepp Rist, and Ernst Udet.    The film is about a man who works alone at the Mont Blanc weather station gathering data. His only contact with the world below is via Morse code signals. He is joined by a woman friend, who helps him survive a terrible storm over the mountain. Filmed on location in Arosa, Switzerland, Babelsberg Observatory in Potsdam, Germany, and Mont-Blanc in Chamonix, France, the film is notable for its dramatic mountain footage and depictions of a violent snow storm. Storm over Mont Blanc premiered in Dresden, Germany on 25 December 1930.

==Plot==
Meteorologist Hannes (Sepp Rist) works alone at a remote weather station below the summit of Mont Blanc in the French Alps. Every day he gathers data on weather conditions. When the weather is clear, he is able to use his telescope to gaze down at the town of Chamonix in the valley far below. His solitary life is interrupted only by the daily weather reports he transmits via Morse code to various weather centers throughout Europe. He is also able to listen to his friend Walter perform his organ concerts on the radio.

At the Chamonix observatory down in the valley, an astronomers daughter, Hella Armstrong (Leni Riefenstahl), gazes up at the night sky, helping her father with his work. Every evening she looks forward to Hannes weather reports and messages—like hearing from a close friend even though theyve never met. In addition to her scientific interests, Hella enjoys skiing with her friends in the mountains. One day while skiing with her friends, she meets Udet (Ernst Udet) in his small aircraft. Together they fly above the clouds to the upper reaches of Mont Blanc. As they approach Hannes weather station, she drops a small evergreen tree down by parachute with a note introducing herself. Enchanted by the gesture, he carefully decorates his tree for Christmas.

Thrilled with her flying experience over Mont Blanc, Hella persuades her father to join her on another flight to the weather station to visit Hannes—an old friend of her father who once worked at that station. At the small remote station, Hella is fascinated by the scientific equipment—and with Hannes. The next morning, Hannes takes Hella with him to the summit of Mont Blanc to capture that days climate conditions. While they are away, Hellas father goes out to study the geology and falls from a rocky ledge and is killed. The young meteorologist and his female companion witness the fatal accident.

After they return to the station, a major storm closes in around them. Hannes does everything he can to comfort the grieving Hella. Before she leaves with the team that arrived to bring her fathers body back down to the valley, Hannes asks her to look up his good friend Walter. In the coming days, Hella mourns the death of her father. One day she visits Walter, who is sick. Hella takes care of him and helps him to recover, which in his loneliness he comes to misinterpret as romantic affection. Meanwhile, Hannes impatiently counts the days when he can be relieved from his work and see Hella again. But when he receives a letter from Walter informing him of his intended engagement to Hella, who knows nothing of the matter, he is so shocked by the news that he decides to stay at the weather station for another season and sends the replacement team back home.

Soon after, a severe storm descends on Mont Blanc. While out gathering his weather data, to his dismay Hannes loses his gloves, and his fingers soon freeze. Back at the weather station, with wind howling around him, Hannes realizes the danger he faces. He tries to revive feeling in his fingers, but is unsuccessful. He then attempts to make his way down the mountain, but the storm is too severe, and the snow bridges over the deep gorges have collapsed, cutting off the path. Exhausted and with hope nearly gone, he makes his way back to the storm damaged station and manages to send out an emergency SOS signal. Soon a rescue team is dispatched.

When Hella is informed that the rescue team cannot reach the station, she contacts Udet, knowing that an aircraft rescue is Hannes only hope. Udet promptly responds and flies at great risk through a dangerous electrical storm to the station, where he finds Hannes in urgent need of care and arranges to heat the station and prepare some food.  Meanwhile, Hella and the rescue team make their way up the mountain. After a difficult climb, they reach the weather station and Hannes gazes upon Hella with loving gratitude.

==Cast==
* Leni Riefenstahl as Hella Armstrong
* Sepp Rist as Hannes
* Ernst Udet as Udet, the Pilot
* Mathias Wieman as Walter Petersen
* Friedrich Kayßler as Armstrong, Hellas Father 
* Alfred Beierle
* Ernst Petersen
* David Zogg as Skifahrer
* Beni Führer as Skifahrer
* Rähmi as Bergführer
* Guzzi Lantschner as Bergführer
* Benno Leubner as Bergführer
* Harald Reinl as Bergführer, Hella Armstrong (Double) 
* Luggi Foeger
* Claus von Suchotzky as Pilot des Aufnahmeflugzeuges
* Josef Gumboldt
* Hans Kogler
* Otto Leubner
* Kurt Reinl
* Julius Rähmi
* Walter Traut

==References==
;Notes
 
;Bibliography
 
* Fanck, Arnold (1931) Stürme über dem Montblanc. Ein Filmbildbuch. Nach dem gleichnamigen Film Concordia OCLC 20841117
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 