Balaraju
{{Infobox film
| name           = Balaraju
| image          =
| image_size     =
| caption        =
| director       = Ghantasala Balaramaiah
| producer       = Ghantasala Balaramaiah
| writer         = Prayaaga
| narrator       =
| starring       = Akkineni Nageswara Rao Anjali Devi S. Varalakshmi D. S. Sadasiva Rao Kasturi Siva Rao
| music          = C. R. Subbaraman Galipenchala Narasimha Rao Ghantasala Venkateswara Rao
| cinematography = P. Sridhar
| editing        = L. M. Lal
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Balaraju ( . It is the first Silver Jubilee Telugu film starring Akkineni Nageswara Rao, S. Varalakshmi and Anjali Devi.

==The Plot==
One Yakshini fell in love with another Yaksha. As a result, Indra curses them to born as humans on the Earth. The Yaksha is born as Balaraju (Akkineni) and Yakshini as Sita (Varalakshmi). Balaraju does not remember the past love story. Sita follows him and tries to remind the past love history and finally succeeds after many twists.

==Cast==
* Akkineni Nageshwara Rao  ...   Balaraju		
* Anjali Devi		
* S. Varalakshmi   ...    Sita		
* D. S. Sadasiva Rao		
* Kasturi Siva Rao
* Seetaram

==Crew==
* Director: Ghantasala Balaramaiah	
* Producer: Ghantasala Balaramaiah	
* Production Company: Pratibha Films
* Story: Prayaaga	 	
* Original Music: C. R. Subburaman, Galipenchala Narasimha Rao and Ghantasala Venkateswara Rao	 	
* Cinematography: P. Sridhar	 	
* Film Editing: L. M. Lal	 	
* Art Direction: S. V. S. Ramarao	 	
* Sound Department: V. Meenakshi Sundaram	
* Playback singers: Kasturi Siva Rao, Ghantasala Venkateswara Rao, S. Varalakshmi and Vakkalanka Sarala
* Lyrics: Prayaaga and Samudrala Sr.

==Songs==
# alakApati aluka chEsi - Chorus
# celiyaa kanaraavaa - (Singer: Ghantasala Venkateswara Rao)
# chaaluraa vagalu - (Singers: S. Varalakshmi, Akkineni Nageswara Rao)
# chooDa chakkani chinnadi mEDagadilO unnadi - Seetaram)
# dEvuDayyA dEvuDu - (Singer: Kasturi Sivarao)
# evarinE nEnevarinE - (Singer: S. Varalakshmi)
# gUTilO chilakEdi
# mari vErE lErevarayyA paramESA - (Singer: S. Varalakshmi)
# navOdayam navOdayam navayugaSObhA - (Singer: Ghantasala Venkateswara Rao and chorus)
# neeku neevADu lEDu - (Singer: S. Varalakshmi)
# O bAlarAjA jAli lEdA - (Singer: S. Varalakshmi)
# O bAlarAjA prEmE yerugavA - (Singer: S. Varalakshmi)
# okarini nAnavESAnu - (Singer: Kasturi Sivarao)
# raaja raaraa naa raajaa raaraa - (Singer: S. Varalakshmi)
# roopamu nerayA pati roopamu - (Singer: S. Varalakshmi)
# teeyani vennela rEyi - (Singer: Vakkalanka Sarala)
# tEli cUDu haayi - (Singers: Ghantasala Venkateswara Rao and S. Varalakshmi)
# varAlakoona ninnu kAna - (Singer: Kasturi Sivarao)
# varuNaa varshingadayya - (Singer: S. Varalakshmi)

==Boxoffice==
* The film ran for more than 100 days in 11 centers in Andhra Pradesh. 
* The Silver Jubilee celebrations were held at Jaihind Talkies, Vijayawada on June 4, 1948. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 