Asudem
{{Infobox film
| name           = Asudem
| image          = 
| alt            = 
| caption        = 
| director       = Daryush Shokof
| producer       = 
| writer         = Daryush Shokof
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Asudem is an American horror film written and directed by Daryush Shokof. Asudem is the reverse writing of medusa indicates a surreal story of a woman Yangzom Brauen in the woods, where she meets Satan experiencing a heavenly vision after consuming hallucinogenic mushrooms. The film is shot in black and white and entirely in Berlin, Germany in 2006.

== Plot ==
The film begins with a woman and her dog living in a destroyed train-station during a war we never see. The location, setting, and compositions of the film are similar with most other films by Daryush Shokof in which the location, situation or both is confined to one place with universal elements as if it could be any place on earth.
The woman confronts strange happenings throughout the film as the intensity of the incidents increase in danger until she finally meets Satan himself who wants to finish her off.  She decides to poison Satan by feeding him wild mushrooms in the woods and in the hope that he would have heavenly visions to suffocate him to death.

== Cast ==
* Yangzom Brauen
* Joachim Paul Assboeck
* Guido Foehrweisser
* Narges Rashidi
* Manuel Cortez
* Benjamin Morik
* Ernest Hausmann
* Annabelle Mandeng
* Araba Walton
* Farvad Esfandyar

== Awards ==
Asudem won the best horror-film Genre award at the New York International Film and Video Festival NYIIFVF in 2007. 

==References==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 


 