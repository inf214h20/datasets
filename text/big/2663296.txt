The Duellists
 
 
{{Infobox film
| name           = The Duellists
| image          = Duellistsposter.jpg
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = David Puttnam
| music          = Howard Blake
| cinematography = Frank Tidy
| screenplay     = Gerald Vaughan-Hughes
| based on       =  
| starring       = Keith Carradine Harvey Keitel
| editing        = Pamela Power
| distributor    = Paramount Pictures
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = $900,000
}}
The Duellists is a 1977 historical drama film and the directorial debut of Ridley Scott. It won the Best Debut Film award at the 1977 Cannes Film Festival.    The basis of the screenplay is the Joseph Conrad short story The Duel (titled Point of Honor in the United States) published in A Set of Six.

==Plot==
In  , to put Feraud under house arrest. As the arrest takes place in the house of Madame de Lionne (Jenny Runacre), a prominent local lady, Feraud takes it as a personal insult from dHubert. Matters are made worse when Feraud asks dHubert if he would "let them spit on Napoleon" and dHubert doesnt immediately reply. Upon reaching his quarters, Feraud challenges dHubert to a duel. The duel is inconclusive; dHubert slashes Ferauds forearm but is unable to finish him off, because he is attacked by Ferauds housemaid. As a result of his part in the duel, dHubert is dismissed from the Generals staff and returned to active duty with his unit.

The war interrupts the mens quarrel and they do not meet again until six months later in Augsburg in 1801. Feraud immediately challenges dHubert to another duel and seriously wounds him. Recovering, dHubert takes lessons from a fencing master and in the next duel (held in a cellar with heavy sabres), the two men fight each other to a standstill. Soon afterwards, dHubert is relieved to learn he has been promoted to captain. Military protocol forbids officers of different ranks from duelling.

The action moves to 1806 when dHubert is serving in Lübeck. He is shocked to hear that the 7th Hussars have arrived in the city and that Feraud is now also a captain. Aware that in two weeks time he is to be promoted to major, dHubert attempts to slip away but is spotted by Ferauds perpetual second. Feraud challenges him to another duel, which is to be fought on horseback with sabres. DHubert slashes his opponent across the forehead; Feraud, blinded because the cut bleeds heavily into his eyes, cannot continue the fight. DHubert considers himself the victor and leaves the field ebullient.

Soon afterwards, Ferauds regiment is posted to Spain. The pair chance upon each other, during the French Armys disastrous retreat from Moscow in 1812. Before they can resume the duel, Cossacks attack forcing dHubert and Feraud to fight together, rather than each other.
 Alan Webb). Edward Fox) attempts to recruit dHubert, as rumours of Napoleons imminent return from exile abound. DHubert refuses to command a brigade if the Emperor returns from Elba. When Feraud, also a brigadier-general and a leading Bonapartist, hears this he declares dHubert is a traitor to the Emperor. He claims that he always suspected dHuberts loyalty, which is why he challenged him to a duel in the first place.
 Louis XVIII. Feraud is arrested and is expected to be executed for his part in the Hundred Days. DHubert approaches the Minister of Police Joseph Fouché (Albert Finney) and persuades him to release Feraud (without revealing dHuberts part in his reprieve). Feraud is paroled to live in a certain province under police supervision.

After Feraud learns of dHuberts promotion in the new French Army, he sends two former officers to seek out dHubert, so he can challenge him to a duel with pistols. Eventually the two men meet in a ruined château on a wooded hill. Feraud rapidly discharges both his pistols, before being caught at point blank range by dHubert, who refuses to shoot him because tradition dictates he now owns Ferauds life. He tells Feraud he must submit to his decision, that in all future dealings Feraud shall conduct himself "as a dead man".

The duel ends and dHubert returns to his life and happy marriage, while Feraud returns to his provincial exile. The closing image of the film depicts Feraud in silent contemplation, gazing at the horizon in utter solitude unable to pursue the obsession that has consumed him for so many years.

==Cast==
 
* Keith Carradine as Armand dHubert
* Harvey Keitel as Gabriel Feraud
* Albert Finney as Joseph Fouché, Minister of Police Edward Fox as Bonapartist agent
* Cristina Raines as Adele, later dHuberts wife
* Robert Stephens as Brigadier-General Treillard
* Tom Conti as Dr Jacquin, an army surgeon and friend of dHubert
* John McEnery as Ferauds second in the final duel
* Diana Quick as Laura, dHuberts mistress
* Alun Armstrong as Lieutenant Lacourbe, a friend of dHubert
* Maurice Colbourne as Ferauds second
* Gay Hamilton as Ferauds mistress
* Meg Wynn Owen as Leonie, dHuberts sister
* Jenny Runacre as Madame de Lionne, a lady in Strasbourg Alan Webb as Adeles uncle
* Arthur Dignam as dHuberts second in the final duel
* Matthew Guinness as the Mayor of Strasbourgs nephew Dave Hill William Hobbs
* W. Morgan Sheppard as the fencing master Liz Smith as the fortune teller Hugh Fraser
* Michael Irving
* Tony Matthews
* Pete Postlethwaite as Treillards valet (this was his first feature film appearance)
* Stacy Keach as the Narrator (voice only)
 

==Historical basis==
 , the basis for Feraud]] Conrad short Dupont and François Fournier-Sarlovèze|Fournier-Sarlovèze, whom Conrad disguised slightly, changing Dupont into dHubert and Fournier into Feraud.

In The Encyclopedia of the Sword, Nick Evangelista wrote:
 
As a young officer in Napoleons Army, Dupont was ordered to deliver a disagreeable message to a fellow officer, Fournier, a rabid duellist. Fournier, taking out his subsequent rage on the messenger, challenged Dupont to a duel. This sparked a succession of encounters, waged with sword and pistol, that spanned decades. The contest was eventually resolved when Dupont was able to overcome Fournier in a pistol duel, forcing him to promise never to bother him again. 
 

They fought their first duel in 1794 from which Fournier demanded a rematch. This rematch resulted in at least another 30 duels over the next 19 years, in which the two officers fought mounted, on foot, with swords, rapiers and sabres.

==Critical reception==
Reception for The Duellists has been generally positive, with only two critics giving a negative review resulting in a 91% "Fresh" rating and averaging 7.2/10 at Rotten Tomatoes, along with the consensus "Rich, stylized visuals work with effective performances in Ridley Scotts take on Joseph Conrads Napoleonic story, resulting in an impressive feature film debut for the director." 

The film has been compared to Stanley Kubricks Barry Lyndon. In both films, duels play an essential role. In his commentary for the DVD release of his film Scott comments that he was trying to emulate the lush cinematography of Kubricks film, which approached the naturalistic paintings of the era depicted.
 William Hobbs. Richard Holmes.

The main locations used for shooting the film were in and around Sarlat-la-Canéda in the Dordogne region of France.

==Home media==
On 29 January 2013, Shout! Factory released the film on Blu-ray. 

==References==
 

==External links==
 
*  
*  
*   – Full text of the short story by Joseph Conrad on which the film is based.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 