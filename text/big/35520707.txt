The Church of Saint Coltrane
 
The Church of Saint Coltrane is a short documentary film produced by Alan Klingenstein and directed by Jeff Swimmer. It was filmed in 1996. Its subject is the famous jazz saxophonist John Coltrane, who became deeply religious after overcoming his addictions to alcohol and heroin in 1957.  Posthumously, he was made the patron saint of the Saint John Coltrane African Orthodox Church|St. John William Coltrane African Orthodox Church church in San Francisco, which holds jam sessions every Sunday that are "five-hour jam sessions interspersed with liturgy, sermons, and fellowship."      

The 26 minute documentary film received awards at seven film festivals. In 1998, it was shown on Bravo (U.S. TV channel)|BRAVO,  then sold to cable networks in Europe and Asia. 

==References==
 

 
 
 
 
 
 
 
 
 
 


 