Josselyn's Wife
{{Infobox film
| name           = Josselyns Wife
| image          = Josselyns Wife (1919) - Ad 2.jpg
| caption        =
| director       = Howard C. Hickman
| producer       = Bessie Barriscale
| writer         = Fred Myton (scenario)
| based on       = novel by Kathleen Norris
| starring       = Bessie Barriscale
| music          =
| cinematography = L. Guy Wilky
| editing        =
| distributor    = Robertson-Cole
| released       = May 5, 1919
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} lost  1919 American silent drama film based on a novel by Kathleen Norris. It was directed by Howard C. Hickman and starred Bessie Barriscale, Nigel Barrie, and Joseph J. Dowling. 

The novel was refilmed again in 1926 with Pauline Frederick.

==Cast==
*Bessie Barriscale - Ellen Latimer Josselyn
*Nigel Barrie - Gibbs Josselyn
*Kathleen Kirkham - Lillian Josselyn
*Joseph J. Dowling - Grandpa Latimer Ben Alexander- Tommy Josselyn
*Leslie Stuart - Lindsay Pepper
*Marguerite De La Motte - Lizzie
*Josephine Crowell - Aunt Elsie
*George Hackathorn - Joe Latimer
*Helen Dunbar - Mrs. Rose
*Tom Guise - Thomas Josselyn

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 


 