Raining Stones
{{Infobox film
| name           = Raining Stones
| image_size     = 
| image	=	Raining Stones FilmPoster.jpeg
| caption        = 
| director       = Ken Loach
| producer       =  Jim Allen
| narrator       =  Bruce Jones
| music          = 
| cinematography = Barry Ackroyd
| editing        = 
| distributor    = Northern Arts Entertainment (USA, theatrical) Fox Lorber (USA DVD)
| released       = 11 March 1993 (USA, limited)
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Bruce Jones, Tom Hickey and Gemma Phoenix. It tells the story of a man who cannot afford to buy his daughter a First Communion dress, and makes disastrous choices in trying to raise the money. The film won the Jury Prize at the 1993 Cannes Film Festival.   

==Cast== Bruce Jones – Bob
* Julie Brown – Anne
* Gemma Phoenix – Coleen
* Ricky Tomlinson – Tommy Tom Hickey – Father Barry
* Mike Fallon – Jimmy
* Ronnie Ravey – Butcher
* Lee Brennan – Irishman
* Karen Henthorn – Young Mother
* Christine Abbott – May
* Geraldine Ward – Tracey William Ash – Joe
* Matthew Clucas – Sean
* Anna Jaskolka – Shop Assistant
* Jonathan James – Tansey
* Ken Strath - Councillor Strath

==Reception== Grand Prix of the Belgian Syndicate of Cinema Critics.

==Filming locations== Langley Estate, Middleton in the Metropolitan Borough of Rochdale, Greater Manchester.
* Bob Williams flat – as well as the butchers shop in the opening scenes – were located opposite the junction of Wood Street and Windemere Road.
* The pub from the carpark of which Bobs Transit van was stolen was The Falcon, located on the corner of Threlkeld Road and Bowness Road. The Falcon has since been demolished and the site remains undeveloped as of 2010.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 


 