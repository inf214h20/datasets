Yerra Mandaram
 
{{Infobox film
| name           = Yerra Mandaram
| image          = Erra Mandaram.jpg
| image_size     =
| caption        =
| director       = Muthyala Subbaiah
| producer       = Pokuri venkateswara rao
| writer         = 
| narrator       =  Rajendra Prasad Yamuna P. L. Narayana Sanjeevi
| music          = 
| cinematography = 
| editing        =Gautham Raju
| studio         =
| distributor    = Ashok Chennareddigari
| released       = 1991
| runtime        =
| country        = India Telugu
| budget         =
}}
 Rajendra Prasad and Yamuna played the male and female lead respectively. The entire movie was shot in a remote village.

==Making of the movie==

A large portion of the movie was shot in a village Nandanavanam,Ulavapadu and near Singarayakonda, Prakasam District,AP. The villagers of Nandanavanam co-operated well with the film unit. 

==Plot==

Rajendra Prasad plays an un-educated and innocent person working in a theatre. Yamuna loves Rajendra Prasad is married to him. Due to reservation system provided by the Government for the post of Panchayat President, Rajendra Prasad is elected as the President of Gram panchayat by the village people under the head of a landlord. The landlord later reveals his intention of selecting Rajendra Prasad as president as he is innocent and can be made puppet in his hands. Rajendra Prasad wants to reform  his village but is left helpless as he lacks courage. Later one day he feels ashamed of himself for being so powerless and unable to stop the crimes committed by the Landlord. He revolts back which results in his arrest with false accusations and is killed by the Landlords men after he returns from the police station . The police take the Landlords side and close the case reporting that the person killed was someone other than Rajendra Prasad. The landlord is then murdered by Yamuna and her son, both of whom allege the murder was committed by Rajendra Prasad who is still said to be alive by the police. The movie ends with Yamuna and her son walking free.

==Awards==
* The film won the Nandi Award for Best Feature Film in 1991 from Government of Andhra Pradesh. Rajendra Prasad won the Nandi Award for Best Actor.

==External links==
*  

 

 
 
 
 

 