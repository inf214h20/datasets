Thavasi
{{Infobox film
| name           = தவசி    Thavasi
| image          = 
| caption        = 
| director       = K. R. Udhayashankar
| writer         = Chinna Krishna
| starring       = Vijayakanth Soundarya Jayasudha Prathiuksha
| producer       = Jayaprakash V. Gnanavel Vidyasagar
| editor         = Vasu
| released       =  
| runtime        = 
| country        = India
| cinematography = Boopathi
| language       = Tamil
}}

Thavasi (  action film directed by Udaya Shankar starring Vijayakanth in dual role, with Soundarya, Prathiuksha, Jayasudha and Nassar playing other pivotal roles. The film was remade in Kannada as Mallikarjuna_(film)|Mallikarjuna with V. Ravichandran. 

==Plot==
Thavasi (Vijaykanth) is an influential, do-good landlord in a small village, who commands respect from village. Thavasi and his wife (Jayasudha) arrange for the marriage of their son Boopathi (Vijaykanth) with Priya (Prathiuksha). Sankarapandi (Nassar) who had killed Thavasis sister, after marrying her wants to marry his daughter (whom he had with his second wife) to Boopathi. Thanagarasu who is accused of stealing jewels commits suicide. Thangarasus mother questions Thavasis judgement. Thavasi decides to send Bhoopathi to his home. Maragathammal hates Boopathi and treats him like a dirt though Boopathi tolerates it. Sankarapandi wants his daughter to wed Bhoopathi. Boopathi unknown to Maragathammal saves Thanagarasus dumb sister from Kottaiperumal and also reforms Kaarmegam which Karmegam reveals to Maragathammal in Boopathis marriage. In the end, a goon who was supposed to be dumb blurts out the truth that Sankarapandi was responsible for the temple theft and he murdered Thanagarasu and made it to look like suicide. In the end, Bhoopathi unites with Priyadarshini.

==Cast==
*Vijaykanth as Thavasi and Boopathi
*Soundarya as Priyadarshini
* Prathiuksha
*Vadivelu as Azhagu
*Jayasudha as Thavasis wife and Boopathis mother
*Nassar as Sankarapandi
*Prathyusha as Nandhini
*Nizhalgal Ravi as Rajadurai, Priyadarshini father Sriman as Thangarasu
*Vadivukkarasi as Maragathaamal, Thangarasus mother
*Ilavarasu as Karmegam, Maragathaamals son in law Ponnambalam as Kotta Perumal, Maragathaamals brother Thyagu
*Eswari Rao Kuyili

==Production==
The film marked the second collaboration between Vijaykanth and KR Udhayasankar after Padhavi Pramanam (1994).

The first phase of shooting of Thavasi started in Chennai and continued in Pollachi and Udumalpet. Song sequences involving Vijayakanth and Soundharya were shot in Kerala and Kodaikanal. Kumbakonam Mahamaham tank was specially designed for Thavasi as its grandest set. Around 10,000 junior artistes participated in the holy dip. Around 30 artistes performed them while nine cameras whirred from all directions to roll.  

==Soundtrack==
{| class="wikitable" style="width:60%;"
|-
! Song Title !! Singers 
|-
| "Thanthana Thanthana Thaimasam" || K. J. Yesudas, Sadhana Sargam 
|-
| "Desing Raja Thaan" || S. P. Balasubramanyam, Sujatha Mohan
|-
| "Ethanai Ethanai" || Shankar Mahadevan, Sujatha Mohan
|-
| "Yelai Imayamalai" || Manickka Vinayakam
|-
| "Panjaangam Paarakadhe" || Srivardhini, Chitra Sivaraman, Shankar Mahadevan
|}

==Reception==
The film received mixed reviews from critics

Rediff wrote, "Trouble is, everyone of them have gone through the selfsame motions so often in the past that they seem to sleepwalk through this film, doing their stuff by rote and with little conviction. The crisis is predictable, ditto the denouement. And the fights, songs and comedy tracks that bridge crisis and denouement fail to grip." 

Bizhat wrote, "Director Udayasankar has done justice in the screenplay and direction area (except for the climax). The last one reel is filled with masala and it is very cinematic." 

Lolluexpress.com wrote, "Although Thavasi has the same old story line the movie was enjoyable to watch. Vijaykanths matured acting and Soundaryas Screen presence were simply superb. A couple of songs were really sweet to hear. Nasser came out with yet another Outstanding performance as he usually does. Vadivelus comedy was really good. So the movie has various features that can be enjoyed by everyone and was not like other "ARVA KOLARU" movies. These kind of movies can be watched and enjoyed and the movie came at the right time. The movie for sure is a hit movie just because it gave us everything what people expect." 

Mouthshut.com wrote, "Watch it as a good time pass." 

Balaji Balasubramaniam of Thiraipadam.com wrote, "Vijayakanth doesnt do much to differentiate between his two roles with respect to gestures or mannerisms and so its just the make-up that does the job. The special effects director too has little to do with the two roles rarely appearing in the same frame. Soundarya looks cute while Jayasudha, appearing in Tamil after a long time, is adequate as the dutiful wife and affectionate mother. Acting honors go to Nasser who manages to jazz up another routine villainous role with a nice smirk while taunting Vijayakanth. Nasser enjoys himself in the scene where he confronts Vijayakanth at the marriage hall. Vadivelus individual encounters with the mad man are loud and unfunny but he is tolerable when paired with Vijayakanth. Pratyuksha is on hand for a single duet. Vidyasagar has had some hit soundtracks recently but Thavasi is definitely not going to be added to the list." 
 
==References==
 

==External links==
* 

 
 
 
 
 