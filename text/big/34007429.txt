Prosperity Blues
{{Infobox Hollywood cartoon|
| cartoon_name = Prosperity Blues
| series = Krazy Kat
| image = Krazyprosperityblues.jpg
| caption = Screenshot
| director = Ben Harrison Manny Gould
| story_artist = Ben Harrison
| animator = Al Eugster Preston Blair
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = October 8, 1932
| color_process = Black and white
| runtime = 5:51 English
| Seeing Stars
| followed_by = The Crystal Gazebo
}}

Prosperity Blues is a short cartoon distributed by Columbia Pictures, and as part of the Krazy Kat films.

==Plot==
Krazy Kat is pulling a box filled with apples, and is trying to sell them. Unfortunately, most people around are low in cash and are too depressed to eat anything because of bad economic times. Moments later, he finds a customer in a spiffy horse. The spiffy horse pays Krazy a check with a considerable amount. Delighted by this, Krazy tries to deposit it in the bank. After getting into a tussle with individuals trying to snatch it, Krazy finds himself chasing his check as it is getting blown away.

After an airborne trip, the check finds its way back into the pockets of the spiffy horse. The spiffy horse then advises Krazy to be happy before putting a smiling mouth on the felines frowning face. As a result, Krazy is happy and that he pretty much forgotten his problems. Krazy continues the spiffy horses work in putting smiling mouths on others, thus inverting their moods. Overtime, the publics depression is gone and somehow their financial problems also follow.

Krazy, the spiffy horse, and a hare go on to parade across the country, promoting their encouragements to be happy. Eventually, Krazy is seen walking up the steps of some capitol where he is greeted by Uncle Sam.

==Notes==
*The songs Smile, Darn Ya, Smile! and Happy Days Are Here Again are used in the film.
*The short is available in the Columbia Cartoon Collection: Volume 3. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 