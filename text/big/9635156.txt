Paisan
{{Infobox film
| name           = Paisan
| image          = paisaposter.jpg
| image_size     = 
| caption        = 
| director       = Roberto Rossellini
| producer       = Rod E. Geiger Roberto Rossellini Mario Conti Alfred Hayes Vasco Pratolini
| narrator       = Giulio Panicali
| starring       = Carmela Sazio Robert Van Loon Dots Johnson Alfonsino Maria Michi Gar Moore Harriet Medin Renzo Avanzo William Tubbs Dale Edmonds Cigolani Renzo Rossellini
| cinematography = Otello Martelli
| editing        = Eraldo Da Roma Arthur Mayer & Joseph Burstyn Metro-Goldwyn-Mayer
| released       =    	
| runtime        = 134 minutes
| country        = Italy Italian English English Sicilian Sicilian
| budget         = 
}} Italian Campaign during World War II when Nazi Germany was losing the war against the Allies of World War II|Allies. A major theme is communication problems due to language barriers.

The film was nominated for both the Academy Award for Best Writing (Original Screenplay) and the BAFTA Award for Best Film from any source. It was the most popular Italian film at the box office in 1945-46, finishing ahead of Mario Mattolis melodrama Life Begins Anew. 

==Plot==

===1st Episode===
During the Allied invasion of Sicily, an American reconnaissance patrol makes its way to a Sicilian village at night. Only one of the Americans speaks Italian. Local Carmela (Carmela Sazio) agrees to guide them past a German minefield. They take shelter in the ruins of a seaside castle.

While the others take a look around, Joe (Robert Van Loon) is assigned to keep an eye on Carmela. Despite the language barrier, Joe starts to overcome her indifference. However, he is shot by a German sniper. Before the small German reconnaissance patrol reaches the castle, Carmela hides Joe in the basement. When the Germans send her for water, she sneaks back and checks on Joe, only to find him dead. She takes his rifle and starts shooting at the enemy. The Germans throw her off a cliff to her death and leave. When the Americans return, they find Joes body and assume Carmela killed him.

===2nd Episode===
The Allies invade mainland Italy and capture the port of Naples. An orphaned street urchin named Pasquale (Alfonsino Pasca) happens upon Joe (Dots Johnson), an embittered, completely drunk African-American soldier. When Joe falls asleep, Pasquale takes his boots. The next day, Joe, a military policeman, nabs Pasquale in the act of stealing supplies from a truck. Joe demands his boots back, but when the boy takes him to where he lives, the sight of the squalor causes Joe to leave without them.

===3rd Episode===
Fred (Gar Moore) is one of the soldiers who helps liberate Rome. During a rest stop, he gets out of his tank and persuades city resident Francesca (Maria Michi) to let him wash up in her apartment. In the little time they spend together, they are attracted to each other, and Fred promises to return.

Six months later, Fred is back in Rome, where he is taken by a street prostitute back to her place. He wants nothing to do with her; instead he tells her how he futilely searched for Francesca, not recognizing her as the prostitute. When he falls asleep, Francesca slips out, asks the building manager to give her home address to Fred when he wakes up. However, she waits in vain; the now-cynical Fred, who thinks the address given him is of a whorehouse, throws away the piece of paper away and heads back to his unit.

===4th Episode===
The southern half of Florence is freed, but fierce fighting continues across the river in the other half between Italian partisans and the Germans and their die-hard fascist supporters. All the bridges other than the Ponte Vecchio have been blown up, stalling the Allied advance. American nurse Harriet (Harriet Medin) is frantic to get across and be reunited with a painter.

She learns that he is now "Lupo", the leader of the partisans. She and partisan Massimo (Renzo Avanzo), a man desperate for news of his family, risk their lives and find a way across. However, Harriet is devastated to learn that Lupo has been killed.

===5th Episode===
Three American chaplains are welcomed to stay the night at a newly liberated Roman Catholic monastery. Captain Bill Martin (William Tubbs), who is the only one of the chaplains who speaks Italian, acts as interpreter. The monks are dismayed to learn from Martin that only he is a Catholic; his two colleagues are a Protestant and a Jew.
When the guests and their hosts sit down to supper, Martin observes that the monks have nothing on their plates. He inquires and learns that the monks have decided to fast in the hope of gaining the favor of Heaven to convert the other two to the true faith.

===6th Episode=== OSS are Po delta. They rescue two downed British airmen, but run out of ammunition in a battle with the enemy and are captured. The partisans are summarily executed the next day, as they are not protected by the Geneva Conventions; two of the outraged prisoners of war are shot when they try to interfere.

==Production== Burstyn & Mayer), helping its visibility. 

Rossellini enlisted six writers to each write a short script on the subject. In order of episodes, they were Klaus Mann, Marcello Pagliero, Sergio Amidei, Federico Fellini, Rossellini, and Vasco Pratolini. Each episode also took place in a different Italian location. Despite the scripts, Rossellini often improvised with the actors and rewrote the stories as they were being filmed. During the first episode filmed in Sicily, Rossellini completely threw out the script and coached the non-professional, illiterate lead actress Carmela Sazio to a performance that would gain critical praise. Wakeman, John. World Film Directors, Volume 2. The H. W. Wilson Company. 1987. p. 962. 

==Critical reception==
Bosley Crowther of the New York Times hailed it, writing it "marks a milestone in the expressiveness of the screen."    He went on to say "It is useless to attempt an explanation, in familiar and concrete terms, of its basic theme and nature, for it is not an ordinary film—neither in form nor dramatic construction nor in the things it has to say", "the antithesis of the classic story film".  He ended his review with "This is a film to be seen—and seen again." 

Jóse Luis Guarner praised the first episode, stating that the camera "keeps still throughout the long conversation, content to look and record, like a film by Louis Lumiere. A lot more is suggested than can actually be seen: the soldiers loneliness, his need to talk to someone, his longing for home and family, the girls growing confidence...to show all this with such economy of means is one of the greatest secrets of the cinema. The whole of Paisà witnesses the same pressing need to portray a complex reality directly, at one go." Guarner went on to call it "Rossellinis first masterpiece, a masterpiece of neorealism as well as one of the peaks of film history." 

  praised the films newsreel footage-like style in adding to the realism and compared the scene of peasants being rounded up in the Po Valley to the Odessa Step sequence in Battleship Potemkin. 

TV Guide called it "perhaps Rossellinis greatest achievement", "a wartime portrait full of humor, pathos, romance, tension, and warmth", and "a film unlike any other the world had seen".    "PAISAN highlights the power of the neorealist style better than almost any other film." 

The Chicago Readers Dave Kehr observed that "The episodes all seem to have an anecdotal triteness ... but each acquires a wholly unexpected naturalness and depth of feeling from Rossellinis refusal to hype the anecdotes with conventional dramatic rhetoric." 

Richard Brody of The New Yorker noted that "the sketch-like format of the six-part Paisan, from 1946, enabled him to mix actors and nonactors, to film in sequence and improvise his stories as he went along, and to use newsreel-style camerawork." 

All eight Rotten Tomatoes reviews are favorable toward the film.  Director Martin Scorsese has also listed it as one of his favourite films of all time and his all-time favorite of the Rossellini films. 

==Influence== Battle of Algiers (1967), in which he adopted Rosselini’s techniques of using non-professional actors and real locations. 

==References==
 

== External links ==
* 
* 
* 
*  at    
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 