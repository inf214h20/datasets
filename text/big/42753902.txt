Geronimo (2014 film)
{{Infobox film
| name           = Geronimo
| image          = 
| caption        =
| director       = Tony Gatlif
| producer       = Tony Gatlif
| screenplay     = Tony Gatlif
| starring       = Céline Sallette Rachid Yous David Murgia Nailia Harzoune
| music          = Delphine Mantoulet Valentin Dahmani 
| cinematography = Patrick Ghiringhelli 	
| editing        = Monique Dartonne
| studio         = Princes Production
| distributor    = Les films du losange 
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         = € 2.49 million 
| gross          = 
}}

Geronimo is a 2014 French drama film directed by Tony Gatlif. It premiered in the Special Screenings section at the 2014 Cannes Film Festival on 20 May.   

==Cast==
* Céline Sallette as Geronimo 
* Rachid Yous as Fazil 
* David Murgia as Lucky 
* Nailia Harzoune as Nil 
* Vincent Heneine as Antonieto
* Adrien Ruiz as El Piripi 
* Aksel Ustun as Kemal 
* Tim Seyfi as Tarik 
* Sébastien Houbani as Hassan 
* Finnegan Oldfield as Nikis Scorpion 
* Arthur Vandepoel as Alex  
* Maryne Cayon as Soda 
* Pierre Obradovic as Yougos
* Alexis Baginama Abusa as Yaxa  Sergi López as Le père de Geronimo 

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 
 

 
 