Millie (film)
{{Infobox film
| name           = Millie
| image          = Millie 1931 poster.jpg
| caption        = Theatrical poster John Francis Dillon
| producer       = Charles R. Rogers Harry Joe Brown (assoc.)   
| writer         = Charles Kenyon Ralph Morgan 
| based on       =  
| starring       = Helen Twelvetrees
| music          = Arthur Lange 
| cinematography = Ernest Haller    Fred Allen 
| distributor    = RKO Radio Pictures
| released       =    |ref2= }}
| runtime        = 85 minutes
| country        = United States English
| budget         =
|}}
 John Francis James Hall, John Halliday and Anita Louise.

==Plot==
Millie (Helen Twelvetrees) is a naive young woman who marries a wealthy man from New York, Jack Maitland (James Hall). Three years later, unhappy in her marriage due to her husbands continued infidelity, she asks for and receives a divorce. Because of her pride, she does not want his money, however, she also does not want to remove her daughter from a comfortable lifestyle.  She allows Jack and his mother (Charlotte Walker), to retain custody of Millies daughter, Connie (Anita Louise). Focusing on her career, she rises through the hierarchy of the hotel where she is employed, shunning the attention of the rich banker, Jimmy Damier (John Halliday), preferring the attentions of the reporter, Tommy Rock (Robert Ames), although, due to her prior sour relationship, refuses to marry.  Eventually, Millie is promoted to the head of operations for the hotel.  At the same time, Tommy is offered a lucrative position at the bank by Damier, as a favor to Millie.  However, at the celebration party, Millie discovers that Tommy, just like Maitland, is cheating on her.  

Betrayed a second time, Millie becomes very bitter.  With her female cohorts, Helen and Angie (Lilyan Tashman and Joan Blondell, respectively), she becomes a woman who loves a good time, floating from man to man.  This goes on for several years, until she hears that Damier has taken an interest in her teen-age daughter, Connie, who bears a striking resemblance to Millie.  Millie warns Damier to leave her daughter alone, but, although he promises to stay away from Connie, he ignores Millies warning, and takes Connie to a remote lodge to seduce her. Millie is tipped off, goes to the lodge with a gun, confronts Jimmy and kills him.

In the following murder trial, Millie tries to keep her daughters name out of the press and claims not to remember why she shot Jimmy. She says that another woman ran out of the lodge after the shot but claims that she didnt see who the woman was and has no idea as to the other womans identity.  The prosecution thus claims that Millies motive was jealousy of Jimmys romantic relationship with this unknown other woman.  Millies friends, however, help to bring out the truth, and when the jury finds out that Millies true motive was to protect her daughter from Jimmys lascivious intentions, they acquit her. In the end, Millie is reunited with her daughter and her estranged husbands family.

==Cast==
*Helen Twelvetrees as Millie Blake Maitland
*Lilyan Tashman as Helen Riley
*Robert Ames as Tommy Rock James Hall as Jack Maitland John Halliday as Jimmy Damier
*Joan Blondell as Angie Wickerstaff
*Anita Louise as Connie Maitland
*Edmund Breese as Bob OFallon
*Frank McHugh as Johnny Holmes Charlotte Walker as Mrs Maitland
*Franklin Parker as Spring
*Charles Delaney as Mike Harry Stubbs as Mark Marks

(Cast as per American Film Institute|AFIs database) 

==Production==
Donald Henderson Clarke finished his novel, Millie, during summer 1930.  The novel was first offered to Metro-Goldwyn-Mayer, who passed on it due to its racy content.    In August of that year, it was reported that Charles R. Rogers had purchased the film rights to the novel, and had signed Charles Kenyon to adapt it into a screenplay, as well as selecting John Francis Dillon to direct.  Although Rogers had signed an agreement to distribute his independent films through RKO, it was reported that he would be overseeing the production on the Universal lot.  Even though he was incorrectly identified as "Rolph Murphy", Ralph Morgan was signed to collaborate with Kenyon on the screenplay adaptation in September.  Less than a week later, Helen Twelvetrees signed on for the titular role;  and it was reported that the screenplay adaptation had been completed.  Rogers would choose Ernest Haller to shoot the film and sign him for the project in the beginning of October. 

In January RKO announced the film would be released in February,  and it was released on February 8, 1931. 

==Notes==
The film was an independent production by Charles Rogers, but became the property of RKO when he agreed to become their production chief.   

The theme song, "Millie", had words and music by Nacio Herb Brown. 
 public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

The films tagline was "Torn From Her Arms ... Child Of Love A Woman Can Give But Once." 

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 