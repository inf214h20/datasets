The Greene Murder Case (film)
{{infobox film
| title          = The Greene Murder Case
| image          =
| imagesize      =
| caption        =
| director       = Frank Tuttle
| producer       = B. P. Schulberg
| writer         = Willard Huntington Wright|S.S. Dine(novel) Louise Long(screenplay) Richard H. Digges Jr.(intertitles,silent version)
| starring       = William Powell Florence Eldridge Jean Arthur
| music          = Karl Hajos
| cinematography = Henry Gerrard
| editing        = Verna Willis
| distributor    = Paramount Pictures
| released       = August 11, 1929
| runtime        = 69 minutes
| country        = United States
| language       = English

}}
The Greene Murder Case is a 1929 talking film produced and released by Paramount Pictures and based on the novel The Greene Murder Case, by S.S. Van Dine(aka Willard Huntington Wright). The novel had been published a year before this film was made. It stars William Powell in his first Philo Vance outing. Florence Eldridge and Jean Arthur costar.

In 1937 the film was remade as Night of Mystery.

==Cast==
*William Powell - Philo Vance
*Florence Eldridge - Sibella Greene
*Ullrich Haupt, Sr. - Dr. Arthur Von Blon
*Jean Arthur - Ada Greene
*Eugene Pallette - Sgt. Ernest Heath
*E. H. Calvert - District Attorney John F. x. Markham
*Gertrude Norman - Mrs. Tobias Greene
*Lowell Drew - Chester Greene
*Morgan Farley - Rex Greene
*Brandon Hurst - Sproot
*Augusta Burmeister - Mrs. Gertrude Mannheim
*Marcia Harris - Hemming

uncredited
*Veda Buckland - Nurse
*Shep Camp - Medical Examiner
*Charles E. Evans - Lawyer Canon
*Helena Phillips Evans - Miss OBrien, Police Nurse
*Mildred Golden - Barton
*Harry Strang - Cop in House

==References==
 

==External links==
* 
* 

 
 
 
 
 

 