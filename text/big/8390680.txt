Open from 18 to 24
{{Infobox film
| name = Open from 18 to 24
| image = Abiertode18a24.jpg
| caption = Screen-capture of film
| director = Víctor Dínenzon
| producer = Claudia Cohen
| writer = Víctor Dínenzon Horacio Peña
| music = Emilio Kauderer Hugo Colace
| editing = Juan Carlos Macías
| distributor =
| released = June 2, 1988
| runtime = 88 minutes
| country = Argentina
| language = Spanish
| budget =
| followed_by =
}} Argentine drama film directed and written by Víctor Dínenzon. 
 Horacio Peña, and others.

==Plot==
 tango class is led by Carla, who just lost the love of her life, Vincente.

The mostly middle-aged, middle-class students attend the class for a variety of reasons, but for the most part they enjoy the sensual romanticism of the tangos dance movements and music.
 jealousies and rivalries of the students become unscaled. At the films end, the leader Carla reveals a surprising fact about herself.

==Cast==
*Gerardo Romano Horacio Peña
*Bernardo Baras
*Néstor Francisco
*Zulma Grey
*Chris La Valle
*Jorge Luz
*Jorge Abel Martín
*Silvia Peyrou
*Aldo Piccione
*Omar Pini
*Carmen Renard
*Nora Sajaroff
*Cora Sanchez
*Carlos Santamaría
*Eduardo Santoro
*Néstor Zacco

==Exhibition==
The film was released  on 2 June 1988.

==Footnotes==
 

==External links==
*   at the cinenacional.com  
*  

 
 
 
 
 
 
 
 