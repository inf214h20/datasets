Le Père Noël a les yeux bleus
{{Infobox film
| name           = Le Père Noël a les yeux bleus
| image          = 
| caption        = 
| director       = Jean Eustache
| producer       = Luc Moullet
| writer         = Jean Eustache
| starring       = Jean-Pierre Léaud Gérard Zimmermann Henri Martinez
| music          = René Coll César Gattegno
| cinematography = Philippe Théaudière
| editing        = Christiane Lack
| distributor    = 
| released       = January 26, 1969 (US release)
| runtime        = 50 min.
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Le Père Noël a les yeux bleus (1966) is a fifty minute film starring French nouvelle vague icon Jean-Pierre Léaud, whose character takes on a job dressing up as Santa Claus in order to save money for a stylish duffel coat. It was the second commercial film made by French director Jean Eustache, who would go on to make several other featurettes. 

The title translates as Santa Claus Has Blue Eyes.

==External links==
* 

 
 
 
 
 
 