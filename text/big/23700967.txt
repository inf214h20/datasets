Ashokavanam
{{Infobox film
| name = Ashokavanam
| image =
| caption =
| director = M. Krishnan Nair (director)|M. Krishnan Nair
| producer = TE Vasudevan
| writer = V Devan Mani Muhammed (dialogues)
| screenplay = Mani Muhammed
| starring = Jagathy Sreekumar Adoor Bhasi Jose Prakash Sukumaran
| music = V. Dakshinamoorthy
| cinematography = C Namasivayam
| editing =
| studio = Jaya Maruthi
| distributor = Jaya Maruthi
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by TE Vasudevan. The film stars Jagathy Sreekumar, Adoor Bhasi, Jose Prakash and Sukumaran in lead roles. The film had musical score by V. Dakshinamoorthy.   
 
==Cast==
*Jagathy Sreekumar 
*Adoor Bhasi 
*Jose Prakash 
*Sukumaran 
*Unnimary 
*Balan K Nair 
*MG Soman  Sudheer 
*Vijayalalitha
 
==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi and Vellanad Narayanan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Maalakkaavadi || K. J. Yesudas || Sreekumaran Thampi || 
|- 
| 2 || Madhyavenal Raathri || P Jayachandran || Sreekumaran Thampi || 
|- 
| 3 || Premathin Lahariyil || S Janaki, Ambili || Vellanad Narayanan || 
|- 
| 4 || Sukhamenna Poovuthedi || P Jayachandran, Ambili, CO Anto || Vellanad Narayanan || 
|}

==References==
 

==External links==
*  

 
 
 


 