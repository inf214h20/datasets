Angels with Dirty Faces
 
 
{{Infobox film
| name            = Angels with Dirty Faces
| image           = Angels with Dirty Faces Film Poster.jpg
| image size      = 
| caption         = Theatrical release poster
| director        = Michael Curtiz
| producer        = Samuel Bischoff
| writer          = Rowland Brown John Wexley Warren Duff Ben Hecht (uncredited) Charles MacArthur (uncredited) Pat OBrien George Bancroft
| music           = Max Steiner
| cinematography  = Sol Polito
| editing         = Owen Marks
| distributor     = Warner Bros.
| released        =  
| runtime         = 97 minutes
| country         = United States
| language        = English
| budget          = 
| gross           =
}}
 Pat OBrien, George Bancroft. The film was written by Rowland Brown, John Wexley, and Warren Duff with uncredited assistance from Ben Hecht and Charles MacArthur.

==Plot== Pat OBrien) are childhood friends who robbed a railroad car as kids.  Rocky saved Jerrys life during the chase by pulling him out of the way of a steam train while running from the guards that saw them.  Rocky was then caught by the police, but Jerry—who could run faster—escaped.  Rocky, after being sent to reform school, grows up to become a notorious gangster, while Jerry has become a priest.

 

Rocky returns to his old neighborhood, where Jerry is the parish priest and intends to keep young boys away from a life of crime.  Six of those boys, Soapy (Billy Halop), Swing (Bobby Jordan), Bim (Leo Gorcey), Patsy (Gabriel Dell), Crabface (Huntz Hall), and Hunky (Bernard Punsly), idolize Rocky, and Jerry attempts to keep his former friend from corrupting them. (These boys were to star in Dead End Kids/East Side Kids/The Bowery Boys films).
 George Bancroft), a shady businessman and municipal contractor.  They try to dispose of Rocky, but he finds the record book that they keep where they list the bribes to city officials.  Jerry learns of these events and warns Rocky to leave before he informs the authorities.  Rocky ignores his advice and Jerry gets the publics attention and informs them all of the crooked government, causing Frazier and Keefer to plot to kill him. Rocky overhears this plot and kills them to protect his childhood friend.

Rocky is then captured following an elaborate shootout in a building, and sentenced to die.  Jerry visits him just before his execution and asks him to do him one last favor—to die pretending to be a screaming, sniveling coward, which would end the boys idolization of him.  Rocky refuses, and insists he will be "tough" to the end, and not give up the one thing he has left, his pride.  At the very last moment he appears to change his mind and has to be dragged to the electric chair (whether his cries are genuine or done only to fulfill Jerrys request is left to the viewers imagination). The boys read newspaper headlines that Rocky died a coward, although not believing it at first, Father Jerry verifies that the paper account was accurate. Then Father Jerry asks them to say a prayer with him, "for a boy who couldnt run as fast as I could".

==Cast==
===Main cast===
* James Cagney as Rocky Sullivan Pat OBrien as Fr. Jerry Connolly
* Humphrey Bogart as Jim Frazier
* Ann Sheridan as Laury Martin George Bancroft as Mac Keefer
* Edward Pawley as Edwards, guard
* Adrian Morris as Blackie
* Frankie Burke as William "Rocky" Sullivan, as a boy
* William Tracy as Jerome "Jerry" Connelly, as a boy
* Joe Downing as Steve
* Marilyn Knowlden as Laury Martin, as a child

===The Dead End Kids===
* Billy Halop as Soapy
* Bobby Jordan as Swing
* Leo Gorcey as Bim
* Gabriel Dell as Pasty
* Huntz Hall as Crab
* Bernard Punsly as Hunky

==Awards and honors== New York Best Actor Best Director Best Writing, Original Story.

Angels with Dirty Faces was nominated for AFIs 10 Top 10#Gangster|AFIs Top 10 Gangster Films list. 
 You Cant Boys Town.

==Production== Pat OBrien were great friends offscreen. Angels with Dirty Faces was the sixth of nine feature films they would make together.]]

When first offered the project, Cagneys agent was convinced that his star property would never consent to playing a role where he would be depicted as an abject coward being dragged to his execution. Cagney, however, was enthusiastic about the chance to play Rocky. He saw it as a suitable vehicle to prove to critics and front office honchos that he had a broad acting range that extended far beyond tough guy roles. Bogart, for one, was very impressed by the death house scene and informed Cagney as such. 
 Dead End, he quickly hired the cast.  For the first test as The Dead End Kids, Warner cast them in the movie Crime School opposite Humphrey Bogart, which was a success which led to the culmination of this movie.

After this movie, Michael Curtiz would work again with James Cagney in films such as Yankee Doodle Dandy and Captains of the Clouds.  Curtiz would later reteam with Humphrey Bogart for his landmark film, that won him an Oscar, Casablanca (film)|Casablanca.

The film would mark the first of three films with Cagney and Bogart, the next two films would be made the following year, The Oklahoma Kid and The Roaring Twenties.

==Adaptations to other media==
Angels with Dirty Faces was dramatized as a radio play on the May 22, 1939 broadcast of Lux Radio Theater, with James Cagney and Pat OBrien reprising their film roles.

==Legacy==
Warner Brothers created a 1939 cartoon spoofing this film, titled Thugs with Dirty Mugs.

A parody of the film appears in  , scenes are shown from a sequel to that film, Angels with Even Filthier Souls. 

Ram Jaane is a 1995 Indian Bollywood remake. Shahrukh Khan was cast as Rocky in the movie alongside Juhi Chawla in one of his early villain roles during his first years in Bollywood. It took almost three years to complete.

The film became an inspiration for a sketch on Sesame Street, titled "Monsters with Dirty Faces".

==References==
 

== External links ==
 
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 