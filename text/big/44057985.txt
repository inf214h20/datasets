Bhrashtu
{{Infobox film
| name = Bhrashtu
| image =
| caption =
| director = Thriprayar Sukumaran
| producer = TK Baburajan
| writer = Madambu Kunjukuttan MR Joseph (dialogues)
| screenplay = MR Joseph
| starring = Sukumaran Kuthiravattam Pappu MS Namboothiri Paravoor Bharathan
| music = MS Baburaj
| cinematography = RM Kasthoori (Moorthy)
| editing = Ravi
| studio = Ragachira
| distributor = Ragachira
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by Thriprayar Sukumaran and produced by TK Baburajan. The film stars Sukumaran, Kuthiravattam Pappu, MS Namboothiri and Paravoor Bharathan in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Sukumaran 
*Kuthiravattam Pappu 
*MS Namboothiri
*Paravoor Bharathan 
*Ravi Menon  Reena 
*Sreekala Sujatha

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Naattika Sivaram. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Bhrashtu Bhrashtu || K. J. Yesudas || Naattika Sivaram || 
|- 
| 2 || Neeyevide Kanna || S Janaki || Naattika Sivaram || 
|- 
| 3 || Thechi Poothe || Vani Jairam, Chorus || Naattika Sivaram || 
|- 
| 4 || Veli Kazhinja || S Janaki || Naattika Sivaram || 
|}

==References==
 

==External links==
*  

 
 
 


 