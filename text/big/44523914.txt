The Debt (2014 film)
{{Infobox film
| name           = The Debt
| image          = The_Debt_film_poster.jpg
| director       = Mahmoud Shoolizadeh
| producer       = Mahmoud Shoolizadeh
| writer         = Mahmoud Shoolizadeh, Richard Levine
| starring       = Courtney Gardner, Chris Morrissey, Kela Holmes, Nicole Gafford Murphy
| cinematography = Mathew Miller
| editing        = Mahmoud Shoolizadeh
| released       =  
| runtime        = 32 minutes
| country        = United States
| language       = English
}}
The Debt is a short film produced and directed by Mahmoud Shoolizadeh, that has participated in several international film festivals http://moondancefilmfestival.com/festival-info/2014-moondance-finalists/  http://www.angaelica.com/festivals/history/2014cgiff/  http://lfc.aforiglobal.com/entries.aspx  http://www.bciff.com/images/BCIFF_2014_Screening_Schedule_10.3.14.pdf  http://www.uffest.com/schedule  http://www.goiaf.com/uploads/1/0/1/9/10198752/golden_orchid_international_animation_festival_2014_official_selection.pdf  and has won two awards. http://moondancefilmfestival.com/festival-info/2014-moondance-award-winners/  Although the film is based in the city of Jacksonville, Florida, it was filmed in St. Marys, Georgia as well as in Jacksonville. Several local newspapers have published articles and discussed this film in detail.    

==Plot==
The short fiction film The Debt  is about a Combat Veteran whose life is falling apart unexpectedly faces her past, and its not what she thought.
"Lisas family appears normal and happy. Her loving husband dotes on her and their beautiful daughter. But in her heart lies a secret that eats at her soul like a malignant cancer, causing irritability and unpredictable outbursts that make her a stranger in her own home. Shame and guilt from wartime experiences fill every waking and sleeping moment. Desperate, she decides to take an extreme action to escape her past. Instead, she comes face-to-face with a surprising truth."  

==Awards and Nominations==
 

 
 

* Best Film Award in 15th "Moondance International Film Festival, Sep 2014", Boulder, Colorado, USA  
* Best Female Actor Award in 15th "Moondance International Film Festival Sep 2014", Boulder, Colorado, USA  
* Best 10th Voyage Studios Award at the "Flagler International Film Festival", Palm Coast, Florida, USA, January 2015 
* Best Supporting Actress Award at the North Carolina "First in Aviation" States Global Film Festival, January 2015, North Carolina, USA http://www.ncgff.org/film-festival-awards-2015.html 
* Best Child Actor Award in North Carolina "First in Aviation" States Global Film Festiva, January 2015, North Carolina, USA 
* Nominated for "Best Drama" Award at the "Flagler International Film Festival", Palm Coast, Florida, USA, January 2015 http://flaglerfilmfestival.com/2015-nominations/ 
* Nominated for "Best Film in Florida" Award at the "Flagler International Film Festival", Palm Coast, Florida, USA, January 2015 
* Nominated for "Best Ensemble Cast" Award at the "Flagler International Film Festival", Palm Coast, Florida, USA, January 2015 

==Festival Participations==
* 15th Moondance International Film Festival, Sep 2014, Boulder, Colorado, USA  
* Columbia Gorge International Film Festival, Aug 2014, Vancouver, WA, USA  
* CineFest Global International Film Festival, Oct 2014, USA, shown in 10 Cities in USA and 10 Countries  
* Mount Vernon International Film Festival, Sep 2014, New York, USA.  
* Golden Orchid International Film Festival, Sep 2014, State University, Pennsylvania, USA   
* Bayou City Inspirational Film Festival, Oct 2014, Houston, TX, USA  
* UnderFunded International Film Festival, Sep 2014, Provo, Utah, USA  
* Oregon Underground International Film Festival, Nov 2014, Oregon, USA 
* Jumpthecut International Film Festival, Jan 2015, Singapore 
* Flagler International Film Festival, Jan 2015, Palm Coast, Florida, USA   
* Borrego Springs International Film Festival, Jan 2015, Borrego Springs, California, USA 
* Atlas & Aeris International Magazine of Independent Film, Jan 2015, New York City, USA    
* North Carolina "First in Aviation" States Global Film Festival 

== References ==
 

== External links ==
*  
* http://www.simplysonicstudios.com/#!the-debt/c23qo

 
 