That's Entertainment! III
{{Infobox film
| name = Thats Entertainment! III
| image = ThatsEntertainment3.jpg
| caption = Original theatrical poster
| director = Bud Friedgen Michael J. Sheridan
| producer = Bud Friedgen Michael J. Sheridan Peter Fitzgerald (executive)		
| writer = Bud Friedgen Michael J. Sheridan
| starring = Gene Kelly June Allyson Lena Horne
| music = Marc Shaiman
| cinematography =
| editing = Turner Entertainment Co.
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 113 min. (DVD version)
| language = English
| budget =
}}

Thats Entertainment! III (1994) is a documentary film released by Metro-Goldwyn-Mayer to celebrate the studios 70th anniversary. It was the third in a series of retrospectives that began with the first Thats Entertainment! (1974) and Thats Entertainment, Part II (1976). Although posters and home video versions use the title without an exclamation mark, the actual on-screen title of the film uses it.

In order to provide a "hook" for audiences who by 1994 had become accustomed to viewing classic movies on home video and cable TV (luxuries not widely available when the first two Thats Entertainment! films were released), the producers decided to feature film footage cut from famous MGM musicals. Many of these numbers were shown for the first time in Thats Entertainment! III.

Highlights include:
 Lady Be Good, shot from a second camera that revealed the well-orchestrated, behind the cameras activity needed to keep the scene moving smoothly.
* "Aint it the Truth", a Lena Horne performance from Cabin in the Sky which was cut (Horne suggests it was censored) before the films release because Horne sang the song in a bubble bath. Easter Parade Annie Get Your Gun ("Im an Indian Too" and "Doin What Comes Naturlly") before Garland was replaced by Betty Hutton. Garland then made Summer Stock, her last film for MGM in 1950. The original theatrical release omits "Doin What Comes Naturally" but it is included on the home video version. The March Jailhouse Rock (also featured in Thats Entertainment! III). 
* Debbie Reynolds singing "You Are My Lucky Star" in a sequence cut from Singin in the Rain. Show Boat before she was dubbed by vocalist Annette Warren.
* The opening dance sequence from The Barkleys of Broadway with the credits overlay removed so that the dance routine by Fred Astaire and Ginger Rogers can be viewed unobstructed for the first time.
* An alternate performance of "I Wanna Be a Dancin Man" by Astaire from the film The Belle of New York. In the alternate take, Astaire wears informal clothes; the studio requested the number be reshot in formal dress. In the film, both performances are shown side-by-side to demonstrate the thoroughness of Astaires rehearsal process since both performances are virtually identical. Torch Song using the same vocal track but now lip-synched by Joan Crawford.
* An alternate version of "A Lady Loves" performed by Debbie Reynolds in I Love Melvin, intercut with the version used in the film (the cut version is set in a farmyard while the version used takes place in opulent surroundings).
* A contortionist performance from the film Broadway Rhythm, featuring the Ross Sisters.

Hosts for the third installment in the Thats Entertainment! series were Gene Kelly (in his final film appearance), June Allyson, Cyd Charisse, Lena Horne, Howard Keel, Ann Miller, Debbie Reynolds, Mickey Rooney, and Esther Williams, making her first appearance in a theatrical film in more than 30 years. Thats Entertainment! III had a limited theatrical release in 1994. According to columnist Robert Osborne, writing for The Hollywood Reporter at the time, the film did "pleasant business" at New Yorks Ziegfeld Theatre.

All three films were released to DVD in 2004. The box set collection of the films included a bonus DVD that included additional musical numbers that had been cut from MGM films as well as the first release of the complete performance of "Mr. Monotony" by Judy Garland (the version used in Thats Entertainment! III is truncated). The home video version of Thats Entertainment! III also contains several musical numbers not in the theatrical release. The film was later remastered for high-definition release on Blu-ray and HD DVD.

==Appearances==
{{columns-list|4|
*June Allyson
*Fred Astaire
*Lucille Ball
*Jack Benny
*Ingrid Bergman
*Ray Bolger
*Lucille Bremer
*Jack Buchanan
*Billie Burke
*Cyd Charisse
*Claudette Colbert
*Joan Crawford
*Xavier Cugat
*Arlene Dahl
*Marion Davies
*Doris Day
*Gloria DeHaven
*Marlene Dietrich
*Marie Dressler
*Jimmy Durante
*Buddy Ebsen
*Nelson Eddy
*Cliff Edwards
*Vera-Ellen
*Nanette Fabray
*Greta Garbo
*Ava Gardner
*Judy Garland
*Betty Garrett
*Greer Garson
*Paulette Goddard
*Dolores Gray
*Kathryn Grayson
*Jean Harlow
*Katharine Hepburn
*Lena Horne
*Betty Hutton
*Louis Jourdan
*Buster Keaton
*Howard Keel
*Gene Kelly
*Grace Kelly
*Hedy Lamarr
*Angela Lansbury
*Peter Lawford
*Vivien Leigh
*Oscar Levant
*Carole Lombard
*Myrna Loy
*Jeanette MacDonald Tony Martin
*Joan McCracken
*Ray McDonald
*Ann Miller
*Carmen Miranda
*Marilyn Monroe
*Ricardo Montalbán
*Polly Moran
*Jules Munshin
*George Murphy
*Donald OConnor
*Janis Paige
*Eleanor Powell
*Jane Powell
*Elvis Presley
*Luise Rainer
*Debbie Reynolds
*Ginger Rogers
*Mickey Rooney
*Norma Shearer
*Frank Sinatra
*Ann Sothern
*Elizabeth Taylor  Robert Taylor
*Lana Turner
*Nancy Walker
*Esther Williams Robert Young
}}

==Musical numbers== Ziegfeld Follies (1946)
*"My Pet Song" - The Five Locust Sisters from The Five Locust Sisters (1928)
*"Singin in the Rain (song)|Singin in the Rain" (finale) - Cliff Edwards and MGM Studio and Orchestra Chorus from The Hollywood Revue of 1929 (1929) The March of Time (1930)
*"Clean as a Whistle" - MGM Studio and Orchestra Girls Chorus from Meet the Baron (1933) Naughty Marietta (1935) Hollywood Party (1934) Robert Taylor, George Murphy and the MGM Studio and Orchestra Chorus from Broadway Melody of 1938 (1937) Lady Be Good (1941) Good Morning" Babes in Arms (1939)
*"Ten Percent Off" - Jimmy Durante and Esther Williams from This Time for Keeps (1947)
*"Tom and Jerry fame" - Esther Williams from Dangerous When Wet (1953)
*"Finale of Bathing Beauty" - Esther Williams from Bathing Beauty (1944)
*"Cleopatterer" - June Allyson from Till the Clouds Roll By (1946) Best Foot Forward (1943) Anchors Aweigh (1945) Easter Parade (1948) Good News (1947) On the Town (1949)
*"Baby, You Knock Me Out" - Cyd Charisse and the MGM Studio and Orchestra Chorus gentlemen from Its Always Fair Weather (1955) For Me and My Gal (1942)
*"Newspaper Dance" - Gene Kelly from Summer Stock (1950) Words and Music (1948) An American in Paris (1951) Fit as a Fiddle" - Gene Kelly and Donald OConnor from Singin in the Rain (1952)
*"The Heather on the Hill" - Gene Kelly and Cyd Charisse from Brigadoon (film)|Brigadoon (1954)
*"You Are My Lucky Star" (outtake) - Debbie Reynolds from Singin in the Rain (1952) Tony Martin Ziegfeld Girl (1941)
*"A Lady Loves" - Debbie Reynolds and the MGM Studio and Orchestra Chorus from I Love Melvin (1953)
*"Thanks a Lot But No Thanks" - Dolores Gray from Its Always Fair Weather (1955) Torch Song (1953)
*"Two Faced Woman" (outtake) - Cyd Charisse (dubbed by India Adams) from The Band Wagon (1953)
*"Mama Yo Quiero" - Mickey Rooney from Babes on Broadway (1941) Words and Music (1948) Just One of Those Things" - Lena Horne from Panama Hattie (1942)
*"Aint it the Truth" (outtake) - Lena Horne from Cabin in the Sky (1943) Show Boat (1951)
*"Cant Help Lovin Dat Man" - Lena Horne from Till the Clouds Roll By (1946) Annie Get Your Gun (1950) Annie Get Your Gun (1950) Words and Music (1948) Everybody Sing (1938)
*"In Between" - Judy Garland from Love Finds Andy Hardy (1938) The Wizard of Oz (1939) The Wizard of Oz (1939)
*"How About You?" - Judy Garland and Mickey Rooney from Babes on Broadway (1941) Ziegfeld Girl (1941)
*"Who (Stole My Heart Away)?" - Judy Garland from Till the Clouds Roll By (1946)
*"March of the Doagies" - Judy Garland, Ray Bolger, Cyd Charisse, Marjorie Main and the MGM Studio and Orchestra Chorus from The Harvey Girls (1946) Get Happy" - Judy Garland from Summer Stock (1950) Easter Parade (1948) Easter Parade (1948) Easter Parade (1948)
*"The Girl Hunt Ballet" - Fred Astaire and Cyd Charisse from The Band Wagon (1953)
*"Swing Trot" - (main title) Fred Astaire and Ginger Rogers and the MGM Studio and Orchestra Chorus from The Barkleys of Broadway (1949)
*"I Wanna be a Dancin Man" - Fred Astaire from The Belle of New York (1952) Anything You Can Do" - Betty Hutton and Howard Keel from Annie Get Your Gun (film) (1950) Silk Stockings (1957) Love Me or Leave Me (1956) Jailhouse Rock" Jailhouse Rock (1957) Gigi (1958)
*"Thats Entertainment! (song)|Thats Entertainment!" - Fred Astaire, Cyd Charisse and the MGM Studio and Orchestra Chorus from The Band Wagon (1953)

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 