Up the Sandbox
 
{{Infobox film
| name           = Up the Sandbox
| image          = Upthesandbox.jpg
| image_size     =
| caption        = VHS cover artwork, circa. 1980s
| director       = Irvin Kershner
| producer       = Robert Chartoff Irwin Winkler
| writer         = Anne Roiphe (novel) Paul Zindel
| narrator       =
| starring       = Barbra Streisand  David Selby
| music          = Billy Goldenberg
| cinematography = Gordon Willis Robert Lawrence
| studio         = Barwood Films First Artists
| distributor    = National General Pictures
| released       =  
| runtime        = 97 min
| country        = United States English
| budget         =
| gross          = $3,500,000 (US/ Canada rentals) 
}}
 comedy film directed by Irvin Kershner, starring Barbra Streisand.

Paul Zindels screenplay, based on the novel by Anne Roiphe, focuses on Margaret Reynolds, a bored young New York City wife and mother who slips into increasingly bizarre fantasies.

The cast includes David Selby, Paul Benedict, George S. Irving, Conrad Bain, Isabel Sanford, Lois Smith, and Stockard Channing in her film debut.

==Plot==
Margaret Reynolds,  a young wife and mother, severely bored with her day-to-day life in New York City and neglected by her husband, slowly slips into depression, finding refuge in her outrageous fantasies: her mother breaking into her apartment, an explorers demonstration of tribal fertility music at a party causing strange transformations, and joining terrorists to plant explosives in the Statue of Liberty.

==Production==
Director Irvin Kershner reportedly told Barbra Streisands biographer James Spada that he originally wasnt happy with the script but was advised not to express his dissatisfaction to Streisand. Several days into filming, when Streisand went to Kershner and asked him why they were having so much trouble, he told her they had started shooting with a weak script. Kershner said, "Your people warned me not to tell you." To which Streisand laughed, "Thats ridiculous! If a script isnt good enough, lets work to improve it." 
 Samburu tribesmen Masai tribe.

Streisand remembered Kenya as "quite beautiful ... I remember it being so hot. We had no air conditioner or anything, so I had a little, dinky trailer filled with flies. Flies everywhere. But I loved the people, the Samburu people, and I made very good friends with a woman of the tribe. We didn’t speak the same language, obviously, but she understood what I was trying to say to her. She showed me how to dress. Everything was held together with safety pins so nobody had to sew anything. I had the greatest outfits. You rip the fabric and you safety pin in where you want it. And then jewelry made out of telephone wires, little beads. She taught me how they put makeup on their eyes with the ground stone, blue ..." 

==Reception==
A number of critics praised Streisands performance. According to Pauline Kael, "Barbra Streisand   never seemed so radiant as in this joyful mess, taken from the Anne Richardson Roiphe novel and directed by Irvin Kershner. The picture is full of knockabout urban humor ". 
 Roger Ebert, who gave the film three out of four stars, also had nothing but praise for her in his review: "This is a Barbra Streisand movie, and so we know the central character wont (cant) be stereotyped; nothing even remotely like Streisand has existed in movies before.  She does not give us a liberated woman, or even a woman working in some organized way toward liberation. Instead, she gives us a woman who feels free to be herself, no matter what anyone thinks. This is a kind of woman, come to think of it, who is rare in American movies ". 

Audiences avoided it, though, and it proved to be one of her lowest-grossing films.

Up The Sandbox was released in a Region 1 DVD on October 5, 2004 as a part of the Barbra Streisand Collection.

==See also==
* List of American films of 1972

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 