It's Only Talk
{{Infobox film name           = Its Only Talk image          = Hiroki_its_only_talk.jpg caption        = Its Only Talk film poster director       = Ryūichi Hiroki producer       = Akira Morishige writer         = Akiko Itoyama (original book) Haruhiko Arai (screenplay) starring       = Shinobu Terajima Etsushi Toyokawa Akira Emoto Shunsuke Matsuoka Nao Omori Tomorowo Taguchi Satoshi Tsumabuki music          = Nido cinematography = Kazuhiro Suzuki editing        = Junichi Kikuchi runtime        = 126 minutes distributor    = Gold View Co., Ltd (international) released       =   country        = Japan language       = Japanese
}}
Its Only Talk is a Japanese film, released in 2005 and is based on the prizewinning novel of the same title written by Akiko Itoyama and directed by Ryūichi Hiroki.

==Plot summary==
"Kamata Town: not an ounce of chic..." "Perhaps I should move there..." So begins Yukos story.

Yuko, is 35 years old; unemployed, single and is on medication to combat manic depression.  There are a number of men in Yukos life:
* her College friend, Homma, now a member of parliament;
* K, a confessed pervert who she meets on the Internet;
* Yasuda, a manic depressive young gang member;
* Soichi, Yukos cousin who separated from his wife and child, and was also dumped by his mistress.

Yuko seems to create a different persona depending on whom she is talking to at the time.  By the end of the movie, Yuko knows that she needs more than what Soichi and the other men in her life can give her.    

==Cast==
Shinobu Terajima as Yuko.
As an actress, Ms. Terajima, is the embodiment of "the present" as well as of Japanese cinema. Her humorous and at times serious performance as Yuko shows us a glimpse of a new kind of woman: being alone, yet accepting what she may want to deny and soldiering on in life.
*Ms. Terajima was born in 1972 in Kyoto. Her father is a famous Kabuki actor, and her mother is well known actress Junko Fuji, who starred in the Red Peony Gambler series for Toei Company.  Ms. Terajima debuted on TV at the age of 17 and then shifted to the theater.  She starred in two films in 2003, Akame 48 Waterfalls (directed by Genjiro Arato) and Vibrator (directed by Ryūichi Hiroki)  .  Her acting in both films is outstanding and she received major acting awards in Japan for both films.

Etsushi Toyokawa plays Yukos cousin, Shoichi  .
*Mr. Toyokawa was born in 1962 in Osaka. He started his acting career in the theater. He is well known in Japan where he   .  He has received many acting awards in Japan. Mr. Toyokawa also writes and directs; his first novel was published in 2003.

Shunsuke Matsuoka plays Homma, Yukos old time friend. Honma believes that true love can cure his impotence.
*Mr. Matsuoka was born in 1972 in Tokyo. He started work as a fashion model, but moved to acting when he was cast in the leading role in Mr. Ryūichi Hirokis film, 800 Two Lap Runners 1994. Mr. Matsuoka has been cast in many Japanese films since 1994.

Tomorowo Taguchi plays a self-confessed pervert, "K". In normal life "K" is married and works as an architect.
*Mr. Taguchi was born in 1957 in Tokyo. He is known in the international film scene, as he has worked with many notable directors like Shōhei Imamura and Shinya Tsukamoto. He is an active musician (has his own band), writer and he directed a film.  He also narrated a well known non-fiction program on TV.

Satoshi Tsumabuki plays Noboru, the manic depressive gang member. Yuko meets him through the Internet.
*Mr. Tsumabuki was born in 1980 in Fukuoka prefecture|Fukuoka. He was chosen to be an actor when he won an audition among some 3 million candidates in 1997.  In 1998 he was the lead role in the film, Subarashiki Hibi (Wonderful Days).  Mr. Tsumabuki is reviewed by film critics as one of Japans very talented young stars.

== Production ==
Producer:  Akira Morishige
*Mr. Morishige began his film career by editing films.  His first credit as a producer was Naoto Yamakawas film New Dawn of Billy the Kid, 1986.  Since then, he has produced many successful movies.  In 2004 he received a special award for producing Ryūichi Hirokis film, Vibrator.

Cinematographer: Kazuhiro Suzuki
*Mr. Suzukis first film as a cinemtographer was This Window is Yours by Tomoyuki Furumaya, 1995. He has worked consecutively with director Ryūichi Hiroki since 1999.

Screenwriter: Haruhiko Arai
*Mr. Arai first worked for Kōji Wakamatsu as an assistant director and then decided to become a screen writer.  He wrote and directed for the first time in 1997, the film Body and Soul.

Original novel written by: Akiko Itoyama
*"Its Only Talk", Akiko Itoyamas first novel won a major literary monthly award, the Bungakukais 96th  Newcomers Prize.  Ms. Itoyamas narration rejects sentimentality and shows that even hopeless people can be depicted with warmth.

==Its Only Talks message==
Its Only Talk not only highlights the illness of manic depression, it also suggests ways to care for the patient.  The film suggests that through tender loving care, and by acknowledging their reality, and seeking help as patients, people
suffering from manic depression could get better.

== Awards == Grand Prize 2006
*Audience Award: Barcelona Asian Film Festival 2006
*FIPRESCI Award: Brisbane International Film Festival 2005

== See also ==
*Brisbane International Film Festival
*Singapore International Film Festival

== References ==
 
 

== External links ==
*  
* 
* 

 

 
 
 
 
 