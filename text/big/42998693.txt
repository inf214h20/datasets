Pride (1998 film)
 
 
{{Infobox film
| name      =Pride
| image     =Pride (1998) DVD cover.jpg
| image size   = 220px
| border     = 
| alt      = US DVD cover
| caption    = US DVD cover
| director    = Shunya Itō
| producer    = 
| writer     = Hiroo Matsuda
| starring    = {{plain list|
*Masahiko Tsugawa Ayumi Ishida Scott Wilson
*Ronny Cox
}}
| music     =  Michiru Ōshima
| cinematography = Yudai Kato
| editing    =Takeo Araki
| studio     = Toei Company
| distributor  = 
| released    =  
| runtime    = 161 minutes
| country    = Japan
| language    = 
| budget     = 
| gross     = 
}} hanged by Japan Academy Prizes. Although the filmmakers intended the film to open dialogue on Japanese history, it was controversial in China, Korea, and Japan owing to concerns of historical revisionism|revisionism.

==Plot== attack on Japan surrenders, allies begin war crimes.
 28 individuals Sir William Webb. All twenty-eight plead not guilty, and Tojo charges the Americans with hypocrisy for trying him despite acts such as the atomic bombings of Hiroshima and Nagasaki.

As the victors are the ones conducting the trials, Tojo and his co-defendants are unable to receive a fair trial, and some of the prosecutions witnesses give   for his role in the war. This sentence is carried out on 23 December 1948.

==Production==
{{multiple image Hideki Tōjō.
| image1    = Masahiko Tsugawa Tokyo Intl Filmfest 2005.jpg
| alt1      = Masahiko Tsugawa
| width1    = 171

| image2    =Hideki Tojo.jpg
| alt2      = Hideki Tojo
| width2= 200
}} the right wing.  
 Ayumi Ishida Scott Wilson and Ronny Cox appeared as Prosecutor Keenan and Justice Webb, respectively. Indian actor Suresh Oberoi played Radhabinod Pal, the lone dissenting judge&nbsp;– according to AllMovies Jonathan Crow, the films only non-Japanese hero. The film also featured Eiji Okuda, Naho Toda, Gitan Ōtsuru, and Anupam Kher.  
 Tokyo Trial, US Department of Defense footage, had taken a similarly negative view of the trials and argued that the US had also committed war crimes during the 20th century.  

==Themes==
Pride  depiction of Tojo is highly positive. Rather than the "absolute monster" depicted in American films on him, he is depicted as a strong, highly nationalistic, leader who loves his family and wants only to rid Asia of colonial rule. By comparison, the prosecutor Keenan is portrayed as a noisy and ignorant, yet scheming man.  This depiction is based on the argument that Japans war-time actions were misunderstood, and that these actions were not intended as acts of aggression, but as acts of self-preservation. 

The historian Peter High notes that Pride is one in a line of Japanese works from the late 1990s, including the films Tower of Lilies and Wings of God, in which the Japanese are portrayed as victims of American vindictiveness and viciousness.  This trend, possibly influenced by the economic downturn then in progress, was backed heavily by older Japanese businessmen. The Tokyo Trial came to be seen as the source of a loss of Japanese identity and tradition. The academic   wrote a book in 1996 which proclaimed the trial to be  , and the businessman Maeno Tōru blamed a "Tokyo Trial view of history" for the negative view of Japanese history and culture.  Futamura quotes Itō as saying:

 }}

==Release and reception==
Pride premiered on 23 May 1998,  in 140 theatres nationwide.  It was a commercial success, selling the most tickets of any domestic production released in the first half of the year.  Reviews of the film in Japan were generally positive, and included praise for the stars "feverish" acting (from the Asahi Shimbun) and the quality of the sets (from the Sankei Shimbun).  Crow, however, gave the film two and a half stars out of five. 
 Japan Academy Outstanding Performance by an Actor in a Leading Role (Tsugawa) and Best Art Direction (Akira Naitô). It won neither, with Best Actor being taken by Akira Emoto for Dr. Akagi and Best Art Direction being won by Katsumi Nakazawa for his work in Begging for Love. 

In Japan, Pride was given a home release in VHS in December 1998. A Region 2 DVD followed in May 2011.  Liberty International released a DVD edition of the film in North America on 25 November 2003; this version included subtitles and a picture gallery. 

==Controversy and legacy==
International response to the film was highly critical owing to concerns of historical revisionism, and Crow suggests that China and Korea&nbsp;– both of which had suffered under Japanese rule during World War II&nbsp;– viewed the film as "deliberate provocation" in light of Japans unwillingness to recognise its past human rights abuses.  Complaints included that the film whitewashed Tojos role in the war, or that it justified the actions taken by Japan.  Response in Japan was more positive, with CBS News recording only a single protest,  although similar concerns of revisionism were echoed. 

Sato, in an interview, stated that the film was meant to "kindle a more nuanced debate about Tojo" rather than the "black and white" depictions which were more common.  Tojos granddaughter, Yuko Iwanami, stated that the film "challenged the image of her grandfather as a villain" by presenting a truth which had been "erased" after the war.  Itō, meanwhile, stated that he "wanted to show how Tojo fought with pride", standing and facing the tribunal on his own. 
 Tokyo Trial, it focused on the Chinese judge Mei Ju-ao (played by Damian Lau) and portrayed  Tojo (Akira Hoshino) as a gruff and unrepentant man. 

==References==
 

==Works cited==
 
*{{Cite web
 |publisher=Nippon Academy-shō Association
 |url=http://www.japan-academy-prize.jp/prizes/?t=22
 |script-title=ja:第22回日本アカデミー賞優秀作品
 |trans_title=22nd Japan Academy Prize
 |archiveurl=http://www.webcitation.org/6Q4TU1rvS
 |archivedate=4 June 2014
 |accessdate=4 June 2014
 |ref= 
|language=Japanese
 }}
*{{Cite web
 |script-title=ja:プライド　運命の瞬間
 |trans_title=Pride: Moment of Fate
 |language=Japanese
 |work=Kinema Junpo
 |url=http://www.kinenote.com/main/public/cinema/detail.aspx?cinema_id=40760
 |archiveurl=http://www.webcitation.org/6QAuIZEov
 |archivedate=8 June 2014
 |accessdate=8 June 2014
 |ref= 
}}
*{{Cite web
 |script-title=ja:プライド　運命の瞬間
 |trans_title=Pride: Moment of Fate
 |language=Japanese
 |work=Tsutaya
 |url=http://www.tsutaya.co.jp/works/10002998.html
 |archiveurl=http://www.webcitation.org/6QCWo6BJe
 |archivedate=9 June 2014
 |accessdate=9 June 2014
 |ref= 
}}
*{{Cite web
 |last=Crow
 |first=Jonathan
 |archiveurl=http://www.webcitation.org/6QAvnTk8N
 |archivedate=8 June 2014
 |accessdate=8 June 2014
 |url=http://www.allmovie.com/movie/pride-v168188/
 |title=Pride (1998)
 |publisher=Rovi
 |work=AllMovie
 |ref= 
}}
*{{Cite news
 |last=Elley
 |first=Derek
 |date=26 July 2006
 |archiveurl=http://www.webcitation.org/6QCW8wPU3
 |archivedate=9 June 2014
 |accessdate=9 June 2014
 |url=http://variety.com/2006/film/reviews/international-military-tribunal-far-east-1200514531/
 |title=Review: International Military Tribunal Far East
 |work=Variety
 |ref= 
}}
*{{Cite news
 |last=Fackler
 |first=Martin
 |date=7 April 1998
 |archiveurl=http://www.webcitation.org/6QCPHPGA7
 |archivedate=9 June 2014
 |accessdate=9 June 2014
 |url=http://community.seattletimes.nwsource.com/archive/?date=19980407&slug=2743863
 |title=Japanese Film Shows Tojo As Hero -- World War Ii Leader Acted In Self-Defense, Movie Studio Claims
 |work=The Seattle Times
 |ref= 
}}
*{{Cite book
 |title=War Crimes Tribunals and Transitional Justice: The Tokyo Trial and the Nuremburg Legacy
 |first=Madoka
 |last=Futamura
 |publisher=Routledge
 |year=2008
 |location=New York
 |ref=harv
 |url=http://books.google.co.id/books?id=bC4Vp-G2AIYC&pg=PA98
 |isbn=978-0-415-42673-2
}}
*{{Cite news
 |date=15 May 1998
 |archiveurl=http://www.webcitation.org/6QCOIB8C6
 |archivedate=9 June 2014
 |accessdate=9 June 2014
 |url=http://www.cbsnews.com/news/new-movie-makes-tojo-a-hero/
 |title=New Movie Makes Tojo A Hero
 |work=CBS News
 |ref= 
}}
* {{cite book
 |last=High
 |first=Peter B.
 |url=http://books.google.co.id/books?id=6XiA9DOuvjAC&pg=PR26
 |title=The Imperial Screen: Japanese Film Culture in the Fifteen Years War, 1931-1945
 |year=2003
 |publisher=University of Wisconsin Press
 |series=Wisconsin Studies in Film
 |location=Madison
 |isbn=978-0-299-18130-7
 |ref=harv
}}
*{{Cite web
 |archiveurl=http://www.webcitation.org/6QAveAtt2
 |archivedate=8 June 2014
 |accessdate=8 June 2014
 |url=http://www.allmovie.com/movie/pride-v168188/cast-crew
 |title=Pride: Cast and Crew
 |publisher=Rovi
 |work=AllMovie
 |ref= 
}}
*{{Cite web
 |archiveurl=http://www.webcitation.org/6QAwLckdN
 |archivedate=8 June 2014
 |accessdate=8 June 2014
 |url=http://www.allmovie.com/movie/pride-v168188/releases
 |title=Pride: Releases
 |publisher=Rovi
 |work=AllMovie
 |ref= 
}}
*{{Cite news
 |last=Schilling
 |first=Mark
 |date=2 July 2010
 |archiveurl=http://www.webcitation.org/6QCNusu9J
 |archivedate=9 June 2014
 |accessdate=9 June 2014
 |url=http://www.japantimes.co.jp/culture/2010/07/02/films/film-reviews/lost-crime-senko-lost-crime-flash/#.U5V9ECifjHw
 |title=Lost Crime Senko (Lost Crime&nbsp;— Flash)
 |work=The Japan Times
 |ref= 
}}
* {{cite book
 |last=Thomas
 |first=Julia
 |url=http://books.google.co.id/books?id=MKPBEx5DbiMC&pg=PA256
 |chapter=Photography, National Identity, and the Cataract of Times: Wartime Images and the Case of Japan
 |editor1=David E Lorey
 |editor2=William H Beezley
 |title=Genocide, Collective Violence, and Popular Memory: The Politics of Remembrance in the Twentieth Century
 |year=2002
 |publisher=SR Books
 |series= World Beat Series
 |volume=1
 |location= Wilmington, Del
 |isbn=978-0-415-42673-2
 |ref=harv
}}
 

==External links==
* 

 

 
 
 
 
 
 
 
 