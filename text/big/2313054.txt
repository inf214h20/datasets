The Naked Truth (1957 film)
 
 
{{Infobox film
| name           = The Naked Truth
| image          = The Naked Truth - UK film poster.jpg
| image_size     =
| caption        = UK film poster
| director       = Mario Zampi
| producer       = Mario Zampi
| writer         = Michael Pertwee
| starring       =  
| music          = Stanley Black
| cinematography = Stanley Pavey
| editing        = Bill Lewthwaite
| distributor    = J. Arthur Rank Film Distributors
| released       = 3 December 1957
| runtime        = 92 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
}}
 1957 Cinema British film comedy starring Terry-Thomas, Peter Sellers and Dennis Price. Peggy Mount, Shirley Eaton and Joan Sims also appear. It was produced and directed by Mario Zampi and written by Michael Pertwee.

==Plot==
Nigel Dennis (Dennis Price) is a blackmailer who threatens to publish embarrassing secrets in his magazine The Naked Truth. After attempting to blackmail a famous scientist (who commits suicide), and an MP (who suffers a heart attack in parliament, and probably succumbs), his latest targets are Lord Henry Mayley (Terry-Thomas), television host Sonny MacGregor (Peter Sellers), writer Flora Ransom (Peggy Mount), and model Melissa Right (Shirley Eaton).

Several of them decide independently that murder would be a better solution than paying. However, it is Mayley who by sheer bad luck nearly ends up the victim of both MacGregors and Ransoms schemes. The four eventually join forces and try again. That attempt also fails, but Dennis is then arrested for an earlier crime.

When Dennis threatens to reveal all at his trial, Mayley comes up with a scheme to break him out of prison and send him to South America, with the help of hundreds of Denniss other victims. They phone in numerous fake calls for help, distracting the London police, while Mayley, MacGregor, and MacGregors reluctant assistant Porter (Kenneth Griffith), disguised as policemen, whisk Dennis away.

Knocking Dennis unconscious periodically, they finally end up in the fully enclosed cabin of a blimp on the way to a rendezvous with an outbound ship. To their dismay, when he comes to, Dennis refuses to go along with their plan, as he in fact never wanted to reveal any of their secrets in front of court. He was, in fact, winning the trial anyway. He reveals the evidence were his copies of "The Naked Truth", who were destroyed by the plotters earlier, unintentionally freeing Dennis of all charges.
Unaware of where he is, and happy to have outsmarted his opponents again, he then steps out for some air and plummets to the ocean below. However, when MacGregor celebrates by shooting his pistol, it punctures the blimp, which comically shoots away into the distance.

==Cast==
* Terry-Thomas as Lord Henry Mayley
* Peter Sellers as Sonny MacGregor
* Peggy Mount as Flora Ransom
* Shirley Eaton as Melissa Right
* Dennis Price as Nigel Dennis
* Georgina Cookson as Lady Lucy Mayley, Henrys wife
* Joan Sims as Ethel Ransom, Floras daughter and reluctant accomplice
* Miles Malleson as Reverend Cedric Bastable, Floras fiancé
* Kenneth Griffith as Porter
* Moultrie Kelsall as Mactavish
* Bill Edwards as Bill Murphy, Melissas rich Texan boyfriend
* Wally Patch as Fred - paunchy old man
* Henry Hewitt as Gunsmith John Stuart as Police Inspector David Lodge as Constable Johnson
* Joan Hurley as Authoress
* Peter Noble as Television Announcer
* Victor Rietti as Doctor

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 