The Wonderful World of the Brothers Grimm
 
{{Infobox film name           = The Wonderful World of the Brothers Grimm image          = Wonderfullgrimm.jpg caption        = Souvenir program cover director       = Henry Levin George Pal (fairy tale sequences) producer       = George Pal screenplay  William Roberts David P. Harmon story          = David P. Harmon starring       = Lawrence Harvey Claire Bloom Karlheinz Böhm Barbara Eden Walter Slezak Oscar Homolka Yvette Mimieux Russ Tamblyn Jim Backus Beulah Bondi Terry-Thomas Buddy Hackett music          = Leigh Harline Bob Merrill (songs) cinematography = Paul Vogel editing        = Walter A. Thompson studio         = George Pal Productions distributor    = Metro-Goldwyn-Mayer Cinerama Releasing Corporation released       =   runtime        = 135 minutes country        = United States Germany language  English
|budget         = $6.25 million Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History Wayne State University Press, 2010 p 164  gross          = $8,920,615  . The Numbers. Retrieved June 13, 2013. 
}}
 Oscar and was nominated for three additional Academy Awards. Several prominent actors — including Laurence Harvey, Karlheinz Böhm, Jim Backus, Barbara Eden, and Buddy Hackett — are in the film.  

It was filmed in the Cinerama process, which was photographed in an arc with three lenses, on a camera that produced three strips of film.   Three projectors, in the back and sides of the theatre, produced a panoramic image on a screen that curved 146 degrees around the front of the audience.

==Plot==
The story focuses on the Grimm brothers, Wilhelm (Laurence Harvey) and Jacob (Karlheinz Böhm), and is biographical and fantastical at the same time. They are working to finish a history for a local Duke (Oscar Homolka), though Wilhelm is more interested in collecting fairy tales and often spends their money to hear them from locals. Tales such as "The Dancing Princess" and "The Cobbler and the Elves" are integrated into the main plot. One of the tales is told as an experiment to three children in a book store to see if publishing a collection of fairytales has any merit. Another tale, "The Singing Bone", is told by an old woman (Martita Hunt) in the forest who tells stories to children, while the uninvited Wilhelm secretly listens through an open window. The culmination of this tale involves a jeweled dragon and features the most involved usage of the films special effects.  
 Tom Thumb from the 1958 film.  His fever breaks and Wilhelm recovers completely, continuing his own work while his brother publishes regular books including a history of German grammar and a book on law. Jacob, shaken by his brothers experience, begins to collaborate on the fairy tales with Wilhelm. 

They are ultimately invited to receive honorary membership at the Berlin Royal Academy, which makes no mention of the tales in their invitation. Jacob prepares to make a speech deliberately insulting the Academy for snubbing Wilhelm. As their train pulls into the station, hordes of children arrive, chanting, "We want a story!" Wilhelm begins, "Once upon a time, there were two brothers." The children cheer, and the film ends with a caption card that reads "…and they lived happily ever after."

==Cast==
*Laurence Harvey - Wilhelm Grimm / The Cobbler ("The Cobbler and the Elves")
*Karlheinz Böhm - Jacob Grimm (as Karl Böhm)
*Claire Bloom - Dorothea Grimm
*Walter Slezak - Stossel
*Barbara Eden - Greta Heinrich
*Oskar Homolka - The Duke (as Oscar Homolka)
*Martita Hunt - Anna Richter (Story Teller)
*Betty Garde - Miss Bettenhausen
*Bryan Russell - Freidrich Grimm
*Ian Wolfe - Gruber
*Walter Rilla - Priest
*Yvette Mimieux - The Princess ("The Dancing Princess")
*Russ Tamblyn - The Woodsman ("The Dancing Princess")/ Tom Thumb (in Wilhelms Dream)
*Jim Backus - The King ("The Dancing Princess")
*Beulah Bondi - The Gypsy ("The Dancing Princess")
*Terry-Thomas - Sir Ludwig ("The Singing Bone")
*Buddy Hackett - Hans ("The Singing Bone")
*Otto Kruger - The King at Ludwigs Trial ("The Singing Bone")
*Arnold Stang - Rumplestiltskin (in Wilhelms dream)
*Hal Smith (actor)| Hal Smith, Mel Blanc, Pinto Colvig, and Dallas McKennon| Dal McKennon Voicing The Puppetoons - The Elves ("The Cobbler and the Elves")
*Peter Whitney - The Giant (uncredited)
*Tammy Marihugh - Pauline Grimm
*Cheerio Meredith - Mrs. Von Dittersdorf

==Reception==
===Box office performance=== theatrical rentals. 14th highest grossing film of 1962.

===Accolades===
The film won an Academy Award and was nominated for three more:   
;Won Best Costume Design, Color - Mary Wills
;Nominated Best Art George Davis, Lawrence of Arabia) Best Cinematography, Lawrence of Arabia) Best Music, The Music Man)

==Cinerama== How the West Was Won, before the notion of using Cinerama for narrative film was abandoned as impractical.
 Cinerama Inc, now a unit of Pacific Theatres, had long ago abandoned the process, put the machinery and negatives into storage at their old Forum Theatre in Los Angeles, and sold off the remaining Cinerama travelogue prints to be used as sound spacers.  MGM held the elements for How the West Was Won and The Wonderful World of the Brothers Grimm, which they co-produced with Cinerama Inc. When the MGM Library was sold to Ted Turner, all the MGM elements went to the Turner company.

When Turners Home Video unit released Wonderful World of the Brothers Grimm on VHS and Laserdisc, the film was transferred from an anamorphic 35mm element that was a composite of the 3 Cinerama panels.  This video seems to be a general release version, missing the Overture, Prologue, Entr’acte, and Exit Music from the Cinerama roadshow presentation. In the late 1990s, a grass roots Cinerama revival reached from a specially retrofitted theatre in Dayton, Ohio, to the Pictureville Cinema museum in Bradford, England, drawing audiences from throughout the world to see Cinerama films saved or assembled by collectors. Talk of renovating the Seattle Cinerama theatre and Cinerama Dome in Hollywood, with the ongoing ability to show 3-strip Cinerama films, led to discussing the possibility of striking new prints of 3 strip Cinerama features to show there. 

While Cinerama Inc, under supervision of former Cinerama Dome manager John Sittig, examined their negatives of This Is Cinerama and discovered they were intact enough to strike a new print, Richard May, then in charge of MGM movies for the Turner organization, was able to report that the negatives for How the West Was Won were all in excellent shape. New Cinerama prints of both features were struck for occasional showings at the Seattle Cinerama, and the Cinerama Dome in Hollywood, while the Pictureville Cinema in Bradford continued to show collected prints scheduled at regular intervals.

==Damage to negatives==
 
When asked about the possibility of striking a new print of Wonderful World of the Brothers Grimm, Richard May said that the negatives had sustained some water damage while in storage, and that the costs of making a new print were prohibitive.

In subsequent years, a good deal of speculation appeared regarding the extent of damage to the Grimm negatives. Some  reported the damage as "extensive" while others  claimed it a total loss. Many  believed that West and Grimm were stored with Pacifics Cinerama titles under less than ideal conditions, and it was often assumed  the water damage to Grimm had happened there. The Turner/MGM film library was eventually sold to Warner Bros., but the actual elements had been very carefully stored by the Turner organization for many years.  
 IB Technicolor, the Grimm elements included protection separation elements of the three primary colors, all of which are in fine shape, and can be used to create a new negative. He said there was not enough sales potential for Grimm to justify the substantial cost of restoration. As film negatives are often stored horizontally and stacked in boxes, it seems  unlikely that the entire negative (three strips for every reel) could have been water damaged unless there had been a serious flood. The director of a Cinerama documentary has said he has examined the Grimm elements, and - far from being "extensive" - the water damage to the three-strip negatives was limited to only the far edge of one of the three panels, and even then only on some reels.

In online discussions with film fans, a Warner Home Video spokesman  reportedly  stated that they would not remaster Grimm to DVD from the 35mm anamorphic composite used for the Laserdisc because it was a general release version, missing the Overture, Prologue, Entracte and Exit music from its original roadshow engagements. Fans  had pointed out that this version did not contain the full width of the three Cinerama panels. 

Turners own cable channel Turner Classic Movies (TCM) has been airing Grimm about twice a year,  complete with all the elements that were missing from the laserdisc.  This sparked a rumor  that the film had been restored.  A Cinerama fansite (http://cinerama.topcities.com/wwotbgld.htm)  has posted images illustrating the difference between the original three panels, the laserdisc, and the current TCM broadcast version, demonstrating that the latter shows more of the original picture.  All the audio elements (Overture, sound for the Prologue, Entracte, Exit music) were readily available, and have been released in a two-disc CD soundtrack, so the only visual added on TCM showings was about a minute and 19 seconds of picture for the Prologue, and a still shot for the roadshow presentation music. The source of the currently broadcast complete version seen on TCM is still unknown.  It has since been reported  that there is also an existing 65mm negative which contains all three panels of the film.

===Legacy=== TCM held another successful film festival in Hollywood in the spring of 2012, which included a special Cinerama screening of How the West Was Won at the Cinerama Dome. At that point, TCM expressed an interest in showing Wonderful World of the Brothers Grimm in Cinerama at the Dome in their 2013 festival.

The Bradford screening in April 2012 of The Wonderful World of the Brothers Grimm marked the first public theatrical presentation of the full Cinerama version in forty years. This print has many splices, (being compiled by Mitchell from several original prints) and some problems reportedly caused by shrinkage in the C (Right side) panel reel early in part one of the movie. Despite these challenges, the film was warmly received, reportedly looking very good in some parts. While Bradford quickly scheduled a second screening of The Wonderful World of the Brothers Grimm for another festival in June 2012, a spokesman for Hollywoods Cinerama Dome reported that the condition of the print made it doubtful that it could be screened there. Unlike Bradford, which still uses classic vertical reel to reel film transport, the Dome runs movies off of platters, which can place extra stress on a fragile print. At that point attention turned to the 65mm composite negative (holding all 3 panels) and the possibility that it could be scanned for a digital screening at the Dome. While not ideal for exhibition, the digital scan could bolster some hope of a video release, or at least a better transfer broadcast on TCM. TCM was reportedly involved in talks to share funding for a new Digital Cinema Print (DCP) to be made from the 65mm negative, but it was decided to screen the actual print at the Dome, so discussion of making a DCP has not led to a commitment or decision as yet.
 Cinerama Inc. spokesman John Sittig reported this was the most requested title at the festival, and both screenings sold out all the most desirable seats well in advance. At the first screening, actor Russ Tamblyn spoke comically about the difficulties of filming in Cinerama.  

The screening was interrupted for a few minutes during the opening prologue after a frame alignment issue. Then after the intermission, at the end of the black leader during the entracte music, the projector showing the right panel rolled to a stop and burned a hole through the film, causing an approximately 45 minute delay as the projection team worked to synch the 3rd panel with the other two and the soundtrack. The second showing had no picture issues at all, but the sound failed a few minutes into act two. After an approximately 30 minute delay, the picture came back on, with all 3 panels in synch and there were no further problems. These screenings not only pleased existing fans, they won new ones among younger patrons seeing the film and/or Cinerama for the first time.

Worldwide fans also want to have this picture on home video, and a petition surfaced online to gather support for some kind of a DVD or Blu-ray release sometime in the future. (http://www.change.org/petitions/wonderful-world-of-the-brothers-grimm-on-dvdbluray) At the Cinerama festival, a spokesman for Image Trends, the company that has been digitally mastering the Cinerama travelogues for DCP and eventual home video release, stated that they believe they could cost effectively produce a digital version of The Wonderful World of the Brothers Grimm from either the partially damaged 3 strip negs or the 65m composite negative, both of which they have already done with other Cinerama films.  

There is still some hope Turner Classic Movies may at least help turn the tide for this film, as the warm reception from the two recent screenings would encourage TCMs desire to show the film at their Hollywood film festival in the spring of 2013. However, given its condition, it is not known whether this print would be available to travel back from England again. There is also some hope among Netflix DVD and streaming subscribers that adding The Wonderful World of the Brothers Grimm to their "saved" list might possibly lead to its being added as a streaming option, which often occurs for older films that may not be considered commercial enough for a DVD release.

==See also== The Brothers Grimm (2005)
*List of stop-motion films

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 