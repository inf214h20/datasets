Mickey's Service Station
 
{{Infobox Hollywood cartoon cartoon name=Mickeys Service Station
|series=Mickey Mouse
|director=Ben Sharpsteen producer          =Walt Disney story artist      = narrator          = voice actor       =Walt Disney Clarence Nash Pinto Colvig Billy Bletcher musician          =Leigh Harline animator  Dick Lundy Wolfgang Reitherman Bill Tytla Walt Disney Productions
|distributor=United Artists release date=  (USA) color process=Black-and-white colorized
|runtime=7:22 minutes
|country=United States
|language=English preceded by=The Band Concert followed by=Mickeys Kangaroo
}}
 Walt Disney Productions and released by United Artists theatrically on March 16, 1935. This was the second last Mickey Mouse cartoon shot in black and white  and is also the last B&W appearances of Donald Duck, Goofy, and Peg-Leg Pete. The films plot centers on Mickey, Donald and Goofy trying to fix Peg-Leg Petes car, but end up causing much trouble in the process. This was also the first Mickey/Donald/Goofy trio cartoon.     

The film was directed by Ben Sharpsteen, who mostly directed Silly Symphonies cartoons. This was the first Mickey Mouse cartoon he directed. Wilfred Jackson, who directed most Mickey Mouse cartoons at the time, did not play a part in this one. 
 colorized and is available in color on YouTube and on DVD versions of Disneys House of Mouse.   

==Plot==
Mickey Mouse, Donald Duck and Goofy are working at a car service station, working together to fix a broken car. Soon Pete (Disney)|Peg-Leg Pete comes by, and angrily demands that they fix a squeak in his tires. He tells them that if they dont get the job done in ten minutes, theyll have their heads cut off. Scared by the thought, they quickly get to work. Slight gags show them to be inept at car repairing, including scenes where Goofy and Donald pull each other through the cars horn, Mickey getting stuck in a tire several times, and Goofy smashing pieces of the cars engine. 

Finally Mickey is able to squeeze a tire and pop out what was making the noise: a grasshopper. The trio smash several pieces of the car with hammers before Mickey is able to drive away the grasshopper. They then get to the job of repairing the car, noticing that their ten minutes are almost up. They are better at repairing the car, though Donald gets accidentally injured a few times and Mickey gets himself stuck in the tires again. When they finally finish the job, Mickey gets in the drivers seat and tests the engine, but Goofy accidentally hits a lever making the stand the car is on go up. Goofy saves the car and Mickey from crashing by making the stands base land in Goofys shirt. 

But the weight of the stand causes Goofy to wobble around, causing Goofy to enter an underground station and pick up a bucket of motor oil. As Goofy struggles to shake the bucket off, he lets loose several rivets and finally gets the bucket off by sliding on a skateboard. He swings around on a gas pump and heads toward Donald, who puts down a plank of wood for Goofy to cross the underground station. Goofy bangs into Donald and causes Donald to hit a larger oil can, which splatters oil everywhere. Goofy slides around on the oil (his movements looking like he is doing a tap dance) and finally gets off the slick, causing Mickey and the car to crash to the ground, and Goofy gets his head stuck in the lever. Just then, Pete arrives. He pushes Mickey aside and gets into the car, but due to the teams inadequate work the car wont start. They tiptoe away while Pete is distracted and as they watch the car dismantles itself until only the engine is remaining. The engine moves forward on four "legs" and chases Pete away, banging into him the whole time.   

==Voice cast==
* Walt Disney as Mickey Mouse 
* Clarence Nash as Donald Duck 
* Pinto Colvig as Goofy 
* Billy Bletcher as Pete 

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 