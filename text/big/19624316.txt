Mr. Schneider Goes to Washington
 

{{Infobox film
| name           = Mr. Schneider Goes to Washington
| image          = Mr. Schneider Goes to Washington DVD cover.jpg
| alt            = 
| caption        = DVD cover
| film name      = 
| director       = Jonathan Neil Schneider
| producer       =  
| writer         = Jonathan Neil Schneider
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          =  
| cinematography =  
| editing        =  
| studio         = 
| distributor    = Fruckis Chuck Films
| released       =   
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American toungue-in-cheek campaign financing in Washington, D.C.|Washington.  The film debuted at the New Orleans Film Festival in 2007, and was released on DVD in 2008.

==Background==
Schneider was inspired to make his documentary after watching a 2004 Senator Ernest Hollings interview on 60 Minutes in which Holings lambastedl the influence of money and lobbyists on the political process.     He resigned from producing America’s Next Top Model and other television shows to fund his own documentary about campaign finances.   
 Jack Oliver Senator Mike Crapo but, when Schneider was not allowed its use, he instead  recruited homeless people to recreate scenes from the video.   

==Synopsis==
Frustrated by Washington and his apathy towards it, Mr. Schneider is finally shaken off his comfortable couch and compelled to storm to the capital of the world’s only superpower to find out what is going on with his government.  Quickly, Mr. Schneider discovers that things in Washington are even worse than he imagined. Because of their dependence on big business and special interests to finance their political futures, almost every decision the President, Vice-President and Members of Congress make is corrupted. After all, there is no bigger issue facing our political leaders than getting re-elected. From education to health care, social security to taxes, foreign policy to gas prices, Americans’ interests repeatedly take a back seat to that of special interests.
Amazingly, Washington’s political elite agrees. Lobbyists, Members of Congress, lawyers, even the Commissioner of the agency responsible for regulating the influence of money in Washington candidly admit this is the most destructive influence on American democracy.  Yet no one seems to care. More people voted for their favorite American Idol candidate than for their favorite candidate for President of the United States. We care more about the marital status of our favorite celebrity than what our elected leaders are doing in Washington.  This isnt lost on the media, whose news coverage reflects its audience’s preoccupation. The result: a population of uniformed, disengaged and disenfranchised non-voters hold the world’s only super power in check. 

==Cast==
 
* Jonathan Neil Schneider
* Patrick Basham
* Tony Coelho Michael Clayton Danny Davis
* Jehmu Greene
* Bill Hillsman
* Nikki Hunter
* Dennis Johnson
* Sunny Lane
* Brianna Love
* David Mindich
* John Leboutillier
* Trevor Potter
* Tony Parker as Lobbyist A
* Christopher Shays Rodney Smith Scott Thomas
* Steve Weissman
* Wright Andrews
* Jan Witlod Barron
* Johan Bloom
* Mike Fraioli
* Craig Holman
* Kevin Michael Key as Senator Mike Crapo
* Amanda Scarnati
* Barbara Lippert
* Sashae Siaibi as Lobbyist B
* Steve Weiss
* Lexi Tyler
* Adam Morse
 

==Reception==
The 2007 New Orleans Film Festival wrote, "Amazingly Mr. Schneider has made a film about corruption and apathy that is informative, entertaining and enraging."    
 Cucalorus Film Festival wrote, "Not all political documentaries are dull and staid, this one has porn stars. Perhaps it’s what you’d expect from a reality producer, but the result is a virtual makeover of the genre to make it fresh and fun."      

In March 2008, Lee Iacocca praised the film, calling it a "fantastic documentary film" which was timely, funny, and entertaining, and recommended "to all my friends in the media, if you want to know what Lee Iacocca thinks is wrong with politics, watch Mr. Schneider Goes to Washington." 

==References==
 

==External links==
*  
*  

 
 
 
 