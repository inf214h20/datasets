Something to Hide
{{Infobox Film
| name           = Something to Hide
| image          = 
| image_size     = 
| caption        =  Alastair Reid Michael Klinger
| writer         = 
| narrator       = 
| starring       = Peter Finch Shelley Winters Colin Blakely
| music          = Roy Budd
| cinematography = Wolfgang Suschitzky
| editing        = Reginald Beck   
| studio         = 
| distributor    = 
| released       =  1972
| runtime        = 99 mins.
| country        = United Kingdom
| language       = English
}} Alastair Reid, based on a 1963 novel by Nicholas Monsarrat.  The film stars Peter Finch, Shelley Winters, Colin Blakely, Linda Hayden and Graham Crowden.   Finch plays a man harassed by his shrewish wife (Winters) who, after picking up a pregnant teenage hitchhiker (Hayden) is driven to murder and madness.  The film was not released commercially in the United States until 1976. 

==Cast==
* Peter Finch as Harry Field 
* Shelley Winters as Gabriella 
* Colin Blakely as Blagdon 
* John Stride as Sergeant Tom Winnington 
* Linda Hayden as Lorelei 
* Harold Goldblatt as Dibbick 
* Rosemarie Dunham as Elsie  Helen Fraser as Miss Bunyan  Jack Shepherd as Joe Pepper 
* Graham Crowden as Lay Preacher

==References==
 

 
 
 
 
 
 


 
 