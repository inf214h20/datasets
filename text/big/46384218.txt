Man and His Kingdom (film)
{{Infobox film
| name           = Man and His Kingdom 
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = 
| writer         = A.E.W. Mason (novel)   Kinchen Wood 
| starring       = Valia   Harvey Braban   Bertram Burleigh 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures 
| distributor    = Stoll Pictures
| released       = August 1922 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} novel of the same title by A.E.W. Mason. 

==Cast==
* Valia as Lucia Rimarez  
* Harvey Braban as Sagasta 
* Bertram Burleigh as Eugene Rimarez 
* Lewis Gilbert as President Rimarez  
* Gladys Jennings as Ternissa Dennison 
* M.A. Wetherell as Gregory Dane  

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 