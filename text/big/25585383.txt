Alice's Adventures in Wonderland (1910 film)
{{Infobox film
| name           = Alice in Wonderland
| image          = Gladys-Hulette-Alice.jpg
| caption        = Gladys Hulette as Alice
| director       = Edwin S. Porter
| writer         = Lewis Carroll (book)
| starring       = Gladys Hulette
| distributor    = Edison Manufacturing Company
| based on       =  
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = Silent film 
}}
Alices Adventures in Wonderland is a 10-minute black-and-white    

Made by the Edison Manufacturing Company and directed by Edwin S. Porter, the film starred Gladys Hulette as Alice.   Being a silent film, naturally all of Lewis Carrolls nonsensical prose could not be used, and, being only a one-reel picture, most of Carrolls memorable characters in his original 1865 novel similarly could not be included. What was used in the film was faithful in spirit to Carroll, and in design to the original John Tenniel illustrations. Variety (magazine)|Variety complimented the picture by comparing it favourably to the "foreign" film fantasies then flooding American cinemas. 

==Gallery==
  The Mad Hatter and the March Hare
 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 

 