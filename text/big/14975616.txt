At Coney Island
{{Infobox film
| name = At Coney Island
| image =
| image_size =
| caption =
| director = Mack Sennett
| producer = Mack Sennett
| writer =
| narrator =
| starring = Mack Sennett Mabel Normand Ford Sterling
| music =
| cinematography = Arthur C. Miller
| editing =
| distributor = Mutual Film
| released =  
| runtime =
| country = United States English intertitles
}}

At Coney Island is a 1912 American short silent comedy starring Mack Sennett, Mabel Normand, and Ford Sterling. The film was directed and produced by Mack Sennett. The film was shot on location at Coney Island.

==Cast==
* Mack Sennett as The Boy
* Mabel Normand as The Girl
* Ford Sterling as The Married Flirt
* Gus Pixley as The Other Rival

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 