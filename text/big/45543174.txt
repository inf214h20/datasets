Rahi (film)
{{Infobox film
| name           = Rahi
| image          = Rahi_(1952).jpg
| image_size     = 
| caption        = Film Poster
| director       = K. A. Abbas
| producer       = K. A. Abbas
| writer         = Mulk Raj Anand K. A. Abbas  (script) 
| narrator       = 
| starring       = Dev Anand Nalini Jaywant Balraj Sahni Manmohan Krishna Anil Biswas Prem Dhawan  (lyrics) 
| cinematography = Ramchandra
| editing        = 
| distributor    =
| studio         = Naya Sansar
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1952 Hindi social drama film produced and directed by K. A. Abbas.   
The film was based on Mulk Raj Anands novel "Two Leaves and a Bud" (1937), which was scripted by Abbas.    It was produced as a bilingual in Hindi as Rahi and in English as The Wayfarer, under the Naya Sansar banner. Its screenplay was by Mohan Abdullah and  V. P. Sathe and the cinematographer was Ramchandra. The film starred Dev Anand and Nalini Jaywant with Balraj Sahni, David (actor)|David, Achla Sachdev and Manmohan Krishan.    

The story revolved around an ex-army man played by Dev Anand, who takes up a job in a tea estate, only to get disillusioned with the management, and his situation. Nalini Jaywant played his love interest as a tea leave picker. Her acting was appreciated in the film getting "great critical acclaim".    

==Plot==
Dev Anand is an ex-army officer who gets a job in a tea estate. He is hired as a an over-all manager by the English owners. Anand proves a loyal and diligent worker meting out punishment to the workers on the behest of his owners. He falls in love with one of the tea-picking girls (Nalini Jaywant). The workers rebel against the harsh and sometimes brutal management of the estate owners. Nalini Jaywant is shot by the owner in the rioting when the workers get out of control. A gradual change has also come over Anand, especially in his dealings with the tea workers, and losing interest, he leaves the place. 

==Cast==
* Dev Anand
* Nalini Jaywant
* Balraj Sahni David
* Manmohan Krishna
* Achla Sachdev Rashid Khan
* Habib Tanvir
* S. Michael
* K. Sethi
* Shaukat Hashmi

==Soundtrack== Anil Biswas were cited as being melodious.    The lyricist was Prem Dhawan and the playback singers were Hemant Kumar, Lata Mangeshkar and Meena Kapoor. 

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Chand So Gaya Taare So Gaye 
| Meena Kapoor
|-
| 2
| Holi Khele Nandlala Biraj Me 
| Ira Majumdar
|-
| 3
| O Janevale Raahi Ik Pal Ruk Jaana
| Lata Mangeshkar
|-
| 4
| O Janewale Raahi Ek Pal Ruk Jana (Sad) 
| Lata Mangeshkar
|-
| 5
| Ek Kali Aur Do Patiya (1)
| Lata Mangeshkar, Hemant Kumar, Meena Kapoor
|-
| 6
| Ek Kali Aur Do Patiya (II)
| Lata Mangeshkar, Meena Kapoor
|-
| 7
| Yeh Zindagi Hai Ek Safar 
| Hemant Kumar
|-
| 8
| Zulm Dha Le Tu Sitam Dha Le 
| Hemant Kumar, Lata Mangeshkar, Meena Kapoor
|-
| 9
| Mashal Se Mashal Jala Kar 
|
|}

==References==
 

==External links==
*  at The Hindu
*  at Muvyz, Ltd

 

 
 
 
 
 