Spring in a Small Town
{{Infobox film name        = Spring in a Small Town image       = Spring in a Small Town poster.jpg director    = Fei Mu writer      = Short story: Li Tianji starring  Wei Wei Li Wei distributor = United States (DVD): Cinema Epoch studio      = Wenhua Film Company country  China
|released 1948
|runtime     = 98 min. language  Mandarin
|budget      =
}} 1948 and directed by Fei Mu. The film was based on a short story by Li Tianji ( ), and was produced by the Wenhua Film Company.

Though its reputation suffered after 1949 in mainland China after the Communist revolution, within the last 20 years it had become known as one of the greatest Chinese films ever made.   The original negative of the film is kept at the China Film Archive.    

==Synopsis== Wei Wei) has long been rendered loveless, though both still feel concern for the other. Liyans young teenage sister Dai Xiu (Zhang Hongmei), meanwhile, is too young to remember the past, and stays cheerful and playful in the ruins of her home. Lao Huang (Cui Chaoming) is an old faithful servant of the Dai family.
 Li Wei), a doctor from Shanghai and a former flame of Zhou Yuwen before she ever met her husband. The rest of the film details the love quadrangle among Zhou Yuwen, Dai Liyan, Zhang Zhichen and Dai Xiu. Zhou Yuwen is conflicted between her love for Zhang and her loyalty to her husband and his family. Dai Liyan loves his wife, but feels unworthy for her and ashamed of himself, especially in comparison with Zhang Zhicheng. Dai Xiu, newly turned sixteen, develops romantic feelings for Zhang Zhichen. Zhang Zhichen is conflicted between his love for Zhou Yuwen and his loyalty to his friend Dai Liyan. That Zhou Yuwen and Zhang Zhichen still loved each other soon became apparent to Dai Liyan and Dai Xiu. Dai Liyan attempts suicide, but is resuscitated by Zhang Zhichen. Zhang Zhichen departs, and Lao Huang and Dai Xiu walks him off to the train station. He promises to return in a year. Zhou Yuwan, watching from the wall, is joined by her husband by her side, as Zhang Zhichen departs.

==Cast==
  Wei Wei (韋偉) as Zhou Yuwen (周玉紋 Zhōu Yùwén), the heroine;
*Shi Yu (石羽) as Dai Liyan (戴禮言 Dài Lǐyán), her husband; Li Wei as Zhang Zhichen (章志忱 Zhāng Zhìchén), Dai Liyans childhood friend and Yuwens former lover
*Cui Chaoming as Lao Huang (老黃 lǎo Huáng), Dai and Yuwens loyal servant;
*Zhang Hongmei as Dai Xiu (戴秀 Dài Xiù), Dai Liyans young sister.

==Reputation==
Made after the war and the so-called "Solitary Island" period of Shanghai film-making, Spring in a Small Town, unlike its leftist predecessors of the 1930s, was a more intimate affair with only tangential references to the politics of the day. Indeed, the film can be distinguished from those earlier works by its more mature treatment of inter-personal conflicts, particularly in the sense that there are no villains or antagonists except for time and circumstance. Even the husband, who ostensibly stands between Zhou Yuwen and Zhang Zhichens love, is an inherently decent and good human being.
 Communists as Communist victory Hong Kong Wang Chao also declared the film to be one of his favorites and Fei Mu the director he most admired.  In 2002, the film was remade by Tian Zhuangzhuang as Springtime in a Small Town.

==DVD releases==
Spring in a Small Town was released on Region 1 DVD on May 8, 2007 by Cinema Epoch. The disc features English subtitles.

An earlier DVD region code|all-region DVD version was also released in the United States by the Guangzhou Beauty Culture Communication Co. Ltd on December 1, 2006.

==See also==
* List of films in the public domain

==Notes==
 

==External links==
*  
*  
*  
*   at the Chinese Movie Database

 

 
 
 
 
 
 
 