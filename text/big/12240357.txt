No End in Sight
 
{{Infobox film
| name           = No End In Sight
| image          = No end in sight poster.jpg
| caption        = Theatrical release poster Charles Ferguson
| producer       = Charles Ferguson Jennie Amias Audrey Marrs Jessie Vogelson Alex Gibney|Alex&nbsp;Gibney&nbsp;(executive)
| starring       = Campbell Scott (narrator)
| music          = Peter Nashel
| editing        = Chad Beck Cindy Lee
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
}} occupation of Iraq. The film marks the directorial debut of Academy Award-winning documentary film producer Charles H. Ferguson. The film premiered January 22, 2007 at the 2007 Sundance Film Festival. The film opened in limited release in the United States on July 27, 2007, playing in two theaters.  , the film had grossed $1.4 million, and had been released on DVD.   

==Interviews== ORHA (the Office for Reconstruction and Humanitarian Assistance, later replaced by the Coalition Provisional Authority|CPA, the Coalition Provisional Authority). Thirty-five people are interviewed who have since become disillusioned by what they experienced at the time. In particular, many of those interviewed claim that the inexperience of the core members of the Bush administration — and their refusal to seek, acknowledge or accept input from more experienced outsiders — was at the root of the disastrous occupation effort. Others include former soldiers stationed in Iraq, as well as authors and journalists critical of the war planning.

Those interviewed are: General Jay Garner, who briefly ran the reconstruction before being replaced by L. Paul Bremer Ambassador Barbara Bodine, who was placed in charge of the Baghdad embassy Richard Armitage, State Department
*Robert Hutchings, former chairman of the National Intelligence Council Colin Powells former chief of staff
*Col. Paul Hughes, worked in the ORHA and then the CPA, currently serving as a Senior Advisor to the U.S. Institute of Peace
*George Packer, author of The Assassins Gate.
*Chris Allbritton, journalist and blogger for Time (magazine)|Time magazine.
*Marc Garlasco, Senior Iraq Analyst 1997–2003, Defense Intelligence Agency
*Joost Hiltermann, Mideast director, International Crisis Group
*Samantha Power, current U.S. Ambassador to the United Nations, Author, A Problem From Hell; professor, Harvard University, Kennedy School of Government
*James Fallows, national editor, The Atlantic; author, Blind into Baghdad
*Paul Pillar, National Intelligence Officer for the Mideast (2000–2005); National Intelligence Council
*Ali Fadhil, Iraqi journalist
*Seth Moulton, Lieutenant, U.S. Marines
*David Yancey, Specialist, Military Police, U.S. Army
*Hugo Gonazalez, Field Artillery Gunner, U.S. Army
*Omar Fekeiki, office manager, Washington Post Baghdad bureau
*Nir Rosen, journalist
*Walter B. Slocombe

==Content==
No End in Sight is a documentary film that focuses on the two-year period following the American invasion of Iraq in March 2003. The film asserts that serious mistakes made by the administration of President George W. Bush during that time were the cause of ensuing problems in Iraq, such as the rise of the Iraqi insurgency (Iraq War)|insurgency, a lack of security and basic services for many Iraqis, sectarian violence and, at one point, the risk of complete civil war.

According to No End in Sight, there were three especially grave mistakes made by L. Paul Bremer, the head of the Coalition Provisional Authority|CPA:
 ORHA had identified at least twenty crucial government buildings and cultural sites in Bagdad, but none of the locations were protected; only the oil ministry was guarded. With no police force or national army to maintain order, ministries and buildings were looted for their desks, tables, chairs, phones, and computers. Large machines and rebars from buildings were also looted. Among those pillaged were Iraqi museums, containing priceless artifacts from some of the earliest human civilizations, which No End in Sight suggested had sent chilling signals to the average Iraqi that the American forces did not intend to maintain law and order. Eventually, the widespread looting turned into an  organized destruction of Baghdad. The destruction of libraries and records, in combination with the "De-Baathification", had ruined the bureaucracy that existed prior to the U.S. invasion. ORHA staff reported that they had to start from scratch to rebuild the government infrastructure. Rumsfeld initially dismissed the widespread looting as no worse than rioting in a major American city and archival footage of General Eric Shinseki stating his belief of the required troop numbers reveals the awareness of the lack of troops.

*Bremers first official executive order implementing "De-Baathification" in the early stages of the occupation, as he considered members disloyal. Saddam Husseins ruling Baath Party counted as its members a huge majority of Iraqs governmental employees, including educational officials and some teachers, as it was not possible to attain such positions unless one had membership. By order of the CPA, these skilled and often apolitical individuals were banned from holding any positions in Iraqs new government. 
 second official executive order disbanding all of Iraqs military entities, which went against the advice of the U.S. military and made 500,000 young men unemployed. The U.S. Army had wanted the Iraqi troops retained, as they knew the locals and could maintain order, but Bremer refused as he felt that they could be disloyal. However, many former Iraqi soldiers, many with extended families to support, then decided that their best chance for a future was to join a militia force. The huge arms depots were available for pillaging by anyone who wanted weapons and explosives, so the former Iraqi soldiers converged on the military stockpiles. The U.S. knew about the location of weapon caches, but said that it lacked the troops to secure them; ironically, these arms would later be used against the Americans and new Iraqi government forces.
 
The film cites these three mistakes as the primary causes of the rapid deterioration of occupied Iraq into chaos, as the collapse of the government bureaucracy and army resulted in a lack of authority and order. It was the Islamic fundamentalists that moved to fill this void, so their ranks swelled with many disillusioned Iraqi people.

==Reception==
The film holds a Metacritic score of 89 out of 100, based on 27 reviews. 

A. O. Scott of The New York Times called it "exacting, enraging" and said "  presents familiar material with impressive concision and impact, offering a clear, temperate and devastating account of high-level arrogance and incompetence." Scott said "most of the movie deals with a period of a few months in the spring and summer of 2003, when a series of decisions were made that did much to determine the terrible course of subsequent events" and wrote "the knowledge and expertise of military, diplomatic and technical professionals was overridden by the ideological certainty of political loyalists." Scott also remarked, "It might be argued that since L. Paul Bremer|Mr. Bremer, Donald Rumsfeld|Mr. Rumsfeld and Paul Wolfowitz|Mr. Wolfowitz declined to appear in the film, Mr. Ferguson was able to present only one side of the story. But the accumulated professional standing of the people he did interview, and their calm, detailed insistence on the facts, makes such an objection implausible." Scott concluded, "It’s a sober, revelatory and absolutely vital film."   
 Iraq war into an enraging, apocalyptic litany of fuck-ups." Nelson said the film "is certainly a film about failure, perhaps the ultimate film about failure. Or maybe a film about the ultimate failure?", also writing that the film "is less a work of investigation (or activism) than history." Rob Nelson wrote, "Focusing on the war itself, Ferguson is chiefly interested in compiling a filmed dossier of incompetence—not so much to argue that the war could have been won and won early, but to suggest that the magnitude of arrogant irresponsibility will carry aftershocks as far into the future as the mind can imagine." Nelson also said, "Fergusons approach is at once relentless and, with the help of Campbell Scotts flat narration, chillingly calm and composed." Nelson wrote, "The evidence speaks for itself, and No End in Sight&mdash;addressed to those wholl be swayed against the war by ineptitude more than immorality&mdash;is the rare American documentary that doesnt appear to preach to the converted, or at least not only to the converted", also saying "For those of us whove opposed the war for years, the movie is at once intensely frightening and, it must be admitted, disturbingly reassuring."  Roger Ebert of the Chicago Sun-Times gave the film 4 stars and said "This is not a documentary filled with anti-war activists or sitting ducks for Michael Moore. Most of the people in the film were important to the Bush administration." Ebert concluded, "I am distinctly not comparing anyone to Hitler, but I cannot help being reminded of the stories of him in his Berlin bunker, moving nonexistent troops on a map, and issuing orders to dead generals." 

At the 2007 Sundance Film Festival, No End in Sight won the Special Jury Prize for Documentaries. 

On January 22, 2008, No End in Sight was named by the Academy of Motion Picture Arts and Sciences as one of 5 films nominated for a prize in the "Best Documentary Feature" category.   

Time (magazine)|Time magazines Richard Corliss named the film one of the Top 10 Movies of 2007, ranking it at #7.  Corliss praised the film, saying it "stands out for its comprehensive take on how we got there, why we cant get out", and opined that everyone should see it, calling it "the perfect stocking-stuffer for holiday enlightenment."   

No End in Sight received the following awards in the 2007 film season: 
*National Society of Film Critics Award: Best Non-Fiction Film
*New York Film Critics Circle Awards: Best Non-Fiction Film
*Los Angeles Film Critics Association Awards: Best Documentary/Non-Fiction Film
*San Francisco Film Critics Circle: Best Documentary
*Florida Film Critics Circle Awards: Best Documentary
*Southeastern Film Critics Association Awards: Best Documentary
*Toronto Film Critics Association Awards: Best Documentary

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2007. 

*1st - Stephen Hunter, The Washington Post
*3rd - Ty Burr, The Boston Globe
*5th - A. O. Scott, The New York Times
*5th - Lisa Schwarzbaum, Entertainment Weekly Dana Stevens, Slate (magazine)|Slate
*6th - Marc Mohan, The Oregonian
*6th - Michael Sragow, The Baltimore Sun
*6th - Rene Rodriguez, The Miami Herald
*7th - Marc Savlov, The Austin Chronicle
*7th - Richard Corliss, Time (magazine)|TIME magazine
*8th - Ann Hornaday, The Washington Post
*8th - Peter Rainer, The Christian Science Monitor
*9th - Carrie Rickey, The Philadelphia Inquirer
*9th - Owen Gleiberman, Entertainment Weekly The Wind That Shakes the Barley)
*10th - Stephanie Zacharek, Salon.com|Salon (tied with Redacted (film)|Redacted)

==Book version==
A book version of No End in Sight is available from the publisher PublicAffairs. 

==See also==
*Frontline (U.S. TV series)|Frontline (U.S. TV series) &ndash; A scene from the program " " is shown in No End in Sight.
*Inside Job (2010 film)|Inside Job, Fergusons second Academy Award-winning documentary about the late-2000s financial crisis

==References==
 

== External links ==
*  
*   at Magnolia Pictures
*  
*  
*  
*  
*  
*   at sundance.org
;Interviews
*  
*  
* 

 
 
 
 
 
 
 
 
 
 