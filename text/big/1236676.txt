Empire (1964 film)
{{Infobox film
| name           = Empire
| image          = Empire Andy Warhol.jpg
| image_size     =
| caption        = Two frames of the film
| director       = Andy Warhol
| producer       = Andy Warhol
| written        = Andy Warhol
| cinematography = Jonas Mekas
| editing        =
| distributor    =
| released       =  
| runtime        = 485 minutes
| country        = United States Silent
| budget         =
}} silent black-and-white film made by Andy Warhol. It consists of eight hours and five minutes of continuous slow motion footage of the Empire State Building in New York City. Abridged showings of the film were never allowed, and supposedly the unwatchability of the film was an important part of the reason the film was created. However, a legitimate Italian VHS produced in association with the Andy Warhol Museum in 2000 contains only an extract of 60 minutes. Its use of the long take in extremis is an extension of Warhols earlier work the previous year with Sleep (film)|Sleep.  Warhol employed Rob Trains to be the projectionist for a screening of the film. Trains miscalculated and mixed the order and speed of the reels for the eight-hour movie. After a positive review in The New York Times, Warhol actually liked the “mistake” and employed Trains for the entire summer.

==Shooting==
Empire was filmed on the night of July 25–26, 1964 from 8:06 p.m. to 2:42 a.m. from the 41st floor of the Time-Life Building, from the offices of the Rockefeller Foundation. The film was shot at 24 frames per second but is projected at 16 frame/s, so that, even though only about 6 hours and 36 minutes of film was made, the film when screened is about 8 hours and 5 minutes long.

==Synopsis==
The film begins with a totally white screen and as the sun sets, the image of the Empire State Building emerges. The floodlights on its exterior come on, the buildings lights flicker on and off for the next 6½ hours, then the floodlights go off again in the next to the last reel so that the remainder of the film takes place in nearly total darkness. {{cite book
 | last = Angell
 | first = Callie
 | year = 1994
 | title = The Films of Andy Warhol Part II
 | url = http://www.amazon.com/o/ASIN/B0006P70ZM
 | id = ASIN B0006P70ZM
 | page = 16
}}  The movie was filmed with Andy Warhol directing and filmmaker Jonas Mekas working as cinematographer. During three of the reel changes, filming recommenced before the lights in the filming room were switched off, making the faces of Warhol and Mekas momentarily visible in the reflection of the window each time.   

==Showings==
In 2005, the film was projected in its entirety on the external wall of the Royal National Theatres fly tower in London.       It was also screened in its entirety on Saturday December 4, 2010 at the Varsity Theater in Chapel Hill, North Carolina, with live musical accompaniment, by The University of North Carolina at Chapel Hills Ackland Art Museum and Interdisciplinary Program in Cinema. It was also projected in its entirety on Saturday May 11, 2013 at the MIC (Museo interattivo del Cinema) in Milan, Italy.

==Significance== historical and Nerve selected Empire as one of "The Thirteen Greatest Long-Ass Movies of All Time". 
{{ wayback
  | url = http://www.nerve.com/CS/blogs/screengrab/archive/2007/12/12/the-thirteen-greatest-long-ass-movies-of-all-time-part-2.aspx
  | title = The Thirteen Greatest Long-Ass Movies of All Time, Part 2
  | date = 20080316030703
}} 

==See also==
* Andy Warhol filmography
* List of American films of 1964
* List of longest films by running time
* Artist Wolfgang Staehles work Empire 24/7 (1999 - 2004), a live-webcam installation showing the Empire State Building continuously.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 