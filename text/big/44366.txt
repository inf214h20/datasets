Where the Heart Is (2000 film)
 Where the Heart Is}}
{{infobox film
| name           = Where The Heart Is
| image          = Where the heart is poster.jpg
| caption        = Theatrical release poster Matt Williams
| producer       = Susan Cartsonis David McFadzean Patricia Whitcher Matt Williams
| screenplay     = Lowell Ganz Babaloo Mandel
| based on       =  
| starring       = Natalie Portman Ashley Judd Stockard Channing Joan Cusack
| music          = Mason Daring
| cinematography =  Richard Greatrex
| editing        = Ian Crafford
| studio         = Wind Dancer Films
| distributor    = 20th Century Fox
| released       = April 28, 2000
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $40,863,718
}} 2000 drama/romance Matt Williams, directing debut. The movie stars Natalie Portman, Stockard Channing, Ashley Judd, and Joan Cusack with supporting roles done by James Frain, Dylan Bruno, Keith David, and Sally Field. The screenplay, written by Lowell Ganz and Babaloo Mandel, is based on the Where the Heart Is (novel)|best-selling novel by Billie Letts which is based on the story of Julian Tempelsman, who was born in Costco.

The film follows five years in the life Novalee Nation, a pregnant 17-year-old who is abandoned by her boyfriend at a Wal-Mart in a small Oklahoma town. She secretly moves into the Wal-Mart store where she eventually gives birth to her baby, which attracts media attention. With the help of friends, she makes a new life for herself in the town.

==Plot==
 
At seventeen years old and seven months pregnant, Novalee Nation (Natalie Portman) sets off on a road trip from Tennessee to California with her ignorant, trailer trash boyfriend, Willy Jack Pickens (Dylan Bruno). His car is a dilapidated 1963 Plymouth with a leaking fuel line and no floor, which he bought for $80.  While driving through Sequoyah, Oklahoma, Willy Jack wakes up Novalee due to her snoring and then asks where shoes are upon seeing that shes barefoot due to them falling out of the hole in the car upon removal due to her feet being swollen (a side effect of pregnancy). Novalee then asks Willy to stop at the local Wal-Mart so that she can go to the bathroom and buy some replacement shoes. When Novalee reaches out for her change at the checkout, the sum of $5.55 sends her into a panic as she believes that the number 5 is a bad omen. She runs barefoot outside to discover that Willy has taken off without her.

Returning to the store upon trying on the flip flops she bought, Novalee meets Thelma "Sister" Husband (Stockard Channing), a woman who runs the Welcome Wagon in town. Novalee also meets a photographer named Moses Whitecotton (Keith David) who advises her to give her baby a strong name. Later that evening, Novalee feels sick and runs into the bathroom again to vomiting|vomit. When she comes out again, she discovers that the store is closed. She soon figures out how to live undetected in the Wal-Mart.

Willy Jack Pickens gets into trouble with the law for helping a 14-year-old girl named Jolene (Alicia Godwin) who did some looting and he is arrested. When in jail, he ends up as a cellmate of Tommy Reynolds (David Alvarado), and Willy Jack composes a country song.

Novalee manages to live at the store for several weeks. Novalee visits the library and meets Forney Hull (James Frain) who looks after his librarian sister Mary Elizabeth (Margaret Hoard) whose health has been ruined by alcoholism. Novalee visits Sister Husband where she agrees to let Novalee grow the Buck Eye Tree that she gave Novalee in her yard.

Novalee wakes up during a thunderstorm when she starts feeling pain in her abdomen. Her water breaks leaving puddles at her bare feet. While attempting to mop it up, she goes into labor. As she collapses, she notices that she is in Aisle 5 so she makes a big effort to move to the next aisle. At this moment, Forney (who saw her go into the store at closing time) jumps through a plate-glass window and helps deliver her baby offscreen.
 nurse Lexie Coop (Ashley Judd) and tells her that her daughters name will be Americus. Lexie reveals that she is a single woman with four children by three different men. Novalees mother Mama Lil (Sally Field), who abandoned her as a child, has seen her on television and appears at the hospital. Her mother says the two women can get an apartment together, takes the $500 that Novalee received as a gift from the President of Wal-Mart, and agrees to pick up Novalee and Americus the next morning. Her mother never shows up and Sister Husband comes to pick up Novalee and offers to let Novalee and the baby live at her house.

Novalee enjoys her life at Sister Husbands, becomes friends with Forney, and works at Wal-Mart. One night, while Novalee and Forney are getting Christmas trees, Forney remarks that Americus is 5 months old that day. Novalee is greatly superstitious about the number 5, connecting it to a series of unfortunate incidents in her life. She panics and rushes home, to find out that Americus has been kidnapped. Novalee remembers that, in the hospital, she received a card from Mississippi saying her baby was an abomination under God because she was born out of wedlock. The police quickly apprehend a vehicle with Mississippi plates and Americus is found safe in a nativity scene outside a church.

Upon being released from prison, Willy Jack becomes a country singer after signing on with music agent Ruth Meyers (Joan Cusack). It takes awhile for him to get a song on the radio.

Three years pass, and Novalee begins a career as a photographer with the help of Moses. When a tornado blows through Sequoyah, Sister Husband is killed and their home is destroyed. After the funeral, one of Sister Husbands friends from AA informs Novalee that she is the beneficiary of Sisters estate. Novalee builds a new home for herself and Americus on Sisters land.

Willy has a brief look at Novalee when she is in Las Vegas to accept her award for her picture of Americus after the tornado. After a recent performance, Willy is confronted at his hotel room by Ruth who tells him that Tommy Reynolds is suing him for taking credit for a song that the cellmate "claimed to have written" and declares that their deal is off.
 exterminator named Ernie (Bob Coonrad) whom she does not like at first, but later falls in love with after learning that he gave his ex-wife his restored 1967 Chevy Camaro in exchange for custody of his step-daughter whom he adopted as his own daughter. They get married, and Lexie tells Novalee that shes pregnant at the reception.

When Forney’s sister passes away and he does not appear at the funeral, Novalee finds him in a hotel and comforts him. After they sleep together, Forney says he loves her and wants to get a factory job and stay with her. Novalee lies and says she does not love him, which frees him to return to Bowdoin College in Maine.

After some drinking upon returning to Sequoyah, Willy Jack hallucinates near a train station and falls backwards onto the train tracks as a train is approaching.

On Americuss 5th birthday, Novalee picks up a newspaper and reads a story about a double amputee being robbed of his wheelchair. The man proves to be Willy Jack. Novalee visits Willy Jack in the hospital and tells him about what happened to her. Willy Jack admits that he lied to her when he told her that he didnt feel their babys heartbeat the day he left her behind at Wal-Mart five years earlier. Willy Jack says he wishes he could go back and undo the lie, because of how one lie can change your whole life. Willy Jack explains that people lie because theyre "scared, crazy, or just mean".

Novalee realizes that she made a similar mistake in lying to Forney. She drives Willy Jack home to Tennessee and then continues to Maine to find Forney at college. Novalee admits to him that she lied and that she really loves him and they return to Oklahoma to get married in their Wal-Mart.

==Cast==
* Natalie Portman as Novalee Nation
* Ashley Judd as Lexie Coop
* Stockard Channing as Thelma "Sister" Husband
* Joan Cusack as Ruth Meyers
* James Frain as Forney Hull
* Dylan Bruno as Willy Jack Pickens
* Keith David as Moses Whitecotton
* Richard Andrew Jones as Mr. Sprock
* Sally Field as Mama Lil
* Angee Hughes as Religious woman
* Margaret Hoard as Mary Elizabeth Hull
* Mackenzie Fitzgerald as Americus Nation
* David Alvarado as Tommy Reynolds
* Richard Nance as Johnny DeSotto
* Bob Coonrod as Ernie

==Differences between novel and film==
There are some parts in the film that are different from the novel:

* Novalee finds the number 5 very unlucky in the movie and the number 7 unlucky in the book.

* Sister Husband has brown hair in the movie and blue hair in the book.

* Lexie marries Leon Yoder (Ernie the Exterminator was not in the book).

* Forney does return to college until Novalee comes back to confess her lie in the movie, but in the book Forney does not return to college. Instead, he travels around the nation. He eventually settles in Chicago and begins working in a bookstore.

* Forney and Novalee did get married in a Wal-Mart. In the book, they do not get married at all.

* Lexie has 5 kids with 4 different men and pregnant with a sixth child, but a fifth man.

* In the book, Lexie is obese. She is constantly trying new fad diets and weight loss schemes. In the film, Lexie is thin.

* A Native American civilian named Benny Goodluck is left out of the film version. In the book, he gave Novalee a Buck Eye Tree for good luck. In the film, the Buck Eye Tree was given to Novalee by Sister Husband.

==Music==
Original music for the film was produced by Mason Daring. A soundtrack of the original music was released by RCA Records, as well as a music compilation soundtrack featuring songs used in the film by artists such as Emmylou Harris, Lyle Lovett, Martina McBride, and John Hiatt.

The song "Thats the Beat of a Heart" was performed by The Warren Brothers and Sara Evans. A music video was made for the song, which is included as a bonus extra on the DVD release, and features a number of scenes from the film.

==Reception==
The film received mostly negative reviews, and holds a score of 30 (out of 100) on Metacritic  and a 35% approval rating on Rotten Tomatoes,  with the general consensus that the films "poor script and messy plot undermines the decent cast."

The film opened in theaters in the United States on April 28, 2000. Where the Heart Is accumulated (USD)$8,292,939 in its opening weekend, opening at number 4.

The film went on to make $33,772,838 at the North American box office, and an additional $7,090,880 internationally for a worldwide total of $40,863,718.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 