Lost and Found on a South Sea Island
{{Infobox film
| name           = Lost and Found on a South Sea Island
| image          =
| caption        =
| director       = Raoul Walsh
| producer       = Samuel Goldwyn
| writer         = Paul Bern
| starring       = House Peters Pauline Starke
| cinematography = Clyde De Vinna
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English English intertitles
}}
 nude scene involving a young woman bathing. 

One reel from this film survives according to a recent biography on director Raoul Walsh. Likewise a foreign archive Cineteca Del Friuli, Germona, claims is said to have a full print. 

==Plot==
Captain Blackbird (Peters) meets the beautiful Lorna (Starke) on the island Pago Pago. However, Lorna is promised to the evil Chief Waki. She and her lover Lloyd Warren (Moreno) beg the captain for help, but he refuses.

==Cast==
* House Peters - Captain Blackbird
* Pauline Starke - Lorna
* Antonio Moreno - Lloyd Warren
* Mary Jane Irving - Baby Madge
* Rosemary Theby - Madge
* George Siegmann - Faulke
* William V. Mong - Skinner
* Carl Harbaugh - Waki
* William Haines - Extra

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
* 
* 
* 
*  at Virtual History
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 