Blueprint for Robbery
{{Infobox film
|name=Blueprint for Robbery
|image=Blueprint for Robbery.jpg
|caption=
|director=Jerry Hopper
|producer=Bryan Foy
|writer=Irwin Winehouse A. Sanford Wolfe
|starring=J. Pat OMalley Robert J. Wilke Robert Gist
|music=Nathan Van Cleave
|cinematography=Loyal Griggs   
|editing=Terry O. Morse
|distributor=Paramount Pictures
|released= 
|runtime=87 minutes
|country=United States
|language=English 
|budget=
}}

Blueprint for Robbery is a 1961 American heist film directed by Jerry Hopper. The film was based on the Great Brinks Robbery of 1950. 

== Cast ==

*J. Pat OMalley as Pop Kane
*Robert J. Wilke as Capt. Swanson
*Robert Gist as Chips McGann
*Romo Vincent as Fatso Bonelli
*Jay Barney as Red Mack
*Henry Corden as Preacher Doc
*Tom Duggan as District Attorney James Livingston
*Sherwood Price as Gus Romay
*Robert Carricart as Gyp Grogan
*John Indrisano as Nick Tony
*Paul Salata as Rocky
*Joe Conley as Jock McGee
*Marion Ross as Young Woman
*Barbara Mansell as Bar Girl

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 


 