7aum Arivu
 
 

{{Infobox film
| name           = 7aum Arivu
| image          = Ezhaam arivu.jpg
| caption        = Promotional poster
| director       = A. R. Murugadoss
| producer       = Udhayanidhi Stalin
| writer         = A. R. Murugadoss Suriya Shruti Haasan Johnny Tri Nguyen
| music          = Harris Jayaraj
| cinematography = Ravi K. Chandran
| released       =   Anthony
| studio         = Red Giant Movies
| distributor    = Red Giant Movies
| runtime        = 168 minutes
| country        = India
| language       = Tamil

| budget         =    
| gross  =     
}}
 Indian science thriller film Suriya and Legacy effects, Indian film after Enthiran.  Telugu as 7th Sense which released simultaneously along with the Tamil original.    The film was also dubbed in Hindi as Chennai vs China  as well as in Malayalam with the original title. The film was also one out of several films selected by the South Indian Film Chamber of Commerce for being the Indian submission at the Academy Award for Best Foreign Language Film.   

==Plot==
The film depicts the life of Bodhidharma (Suriya (actor)|Suriya), a master of martial arts and medical remedies, who is depicted in the film as the son of a great Tamil king of the Pallava Dynasty. He is sent to China by his guru, who requests him to stop the spread of a pandemic disease existing there from spreading to India. Initially the people in China treated him as an inferior but later when he cures a little girl from the deadly disease and fights against some people who ill treated villagers there, the people of China began to respect and worship him. He begins to teach them how to cure many diseases, the skills of hypnotism and the physical training of the Shaolin monks that led to the creation of Shaolinquan. However after several years, when he expresses his desire to return to India, the villagers plot to poison him and bury him near the temple, believing that their place would be disease free if he is buried there. Bodhidharma agrees to die and subsequently becomes a fundamental figure in Chinese history, affectionately being dubbed as "Damo".

The film switches to Modern China|modern-day China, where Dong Lee (Johnny Tri Nguyen) is given the task of starting a government planned biological war against India, known as Operation Red. Dong Lee arrives in Chennai starts this operation by injecting a virus into a street dog. This disease was the one which occurred during the time of Bodhidharma in China. Meanwhile Subha Srinivasan (Shruti Haasan), a student of genetics researches that Bodhidharma can be brought back to life if his sample of DNA is matched with another sample of DNA which she finds in Aravind (Suriya (actor)|Suriya) a descendant of Bodhidharma who is working as a circus artist nearby. Dong Lee finds out Subhas mission to revive Bodhidharma and plans to kill her first, so that the disease cannot be stopped.

Subha approaches Aravind, but begins to love him and spends time with him.  One day, Aravinds family see Subha and clearly remember that she visited them a year before to find Aravind and know all about him for the Bodhidharma research. Saving herself, she lies by saying that she doesnt know them. Later that night, Aravinds uncle (Ilavarasu) explains all what happened one year before when they met Subha. Aravind goes and meets Subha, and is enraged upon realising the truth. However the next day, he reconciles with Subha in a love failure mood and agrees to contribute to the research and hence save the country. The research begins of giving life to Bodhidharma and to end Operation Red. But Dong Lee, who is capable of doing anything, does all impossibly bad things, mastered in "Nokku Varmam" (hypnotism), a martial art which was actually taught to the Chinese by Bodhidharma.

Subha goes to her genetics department and announces that Operation Red can be stopped if they read and make use of the cures in a book written by Bodhidharma. However, the department laughs and refuses to believe her, claiming that an ancient book is of no use in modern times. Disappointed, Subha leaves. After some time, Subha and Aravind find out that Subhas professor from the genetics department is assisting Dong Lee in Operation Red. They sneak into the professors apartment and learn about the operation, without his knowledge. That night Aravind sneaks Subhas phone and gets to know that she actually doesnt want to leave him and wants to marry him. The next day, the professor is caught red-handed by Aravind, Subha and her friends, but surrenders and explains that he received a huge sum of money from the China government if he carried out Operation Red. After the professor gives the gang more details about Operation Red and how to stop it, they leave. However, after a while, Dong Lee arrives and the professor once again teams up with him to save his own life.

Dong Lee causes numerous havoc in the city, which Subha, Aravind and their gang barely escape. They finally locate a research centre where they can activate Bodhidharma DNA in Aravind, and decide to hide there for a few days. They all deactivate their phones so that no-one can trace them. But Dong Lee somehow traces Subha and the gang with the help of two of Subhas outside friends. However, he kills both of them afterwards. Dong Lee then locates the place of the research centre. Subha and her friends in the research centre escape in a van, but Dong Lee chases them and the van collapses. An unconscious Aravind gets beaten badly by Dong Lee at first, but then manages to regain Bodhidharma skills somehow and kills Dong Lee. Bodhidharma uses an ancient medicine to cure the disease.

==Cast== Suriya as Bodhidharma and Aravind
* Shruti Haasan as Subha Srinivasan
* Johnny Tri Nguyen as Dong Lee
* Guinnes Pakru as Aravinds friend
* Ashwin Kakumanu as Ashwin
* Saahil Chitkara as Imran Saahil
* Dhanya Balakrishna as Malathi
* Misha Ghoshal as Nisha
* Avinash
* Ilavarasu as Aravinds uncle Abhinaya as Bodhidharmas wife
* Azhagam Perumal in a guest appearance

==Production==

===Casting=== Suriya would Abhinaya of Naadodigal fame were selected to play minor supporting roles in the film.  

===Influences===
Sources claimed that the film drew inspirations from Christopher Nolans Inception. However, the director dismissed the news, reasoning that he had begun shooting much before the release of Inception. Murugadoss further denied reports that the film was a remake of the Bollywood flick Chandni Chowk to China, as both were supposed to be set in China, and that the films concept was similar to that of the Hollywood film Perfume (2001 film)|Perfume, confirming the script as original.  After completing the film, he emphasised that he had not been inspired or remade any film, stating that Hollywood filmmakers can "feel free to remake 7aum Arivu". 

===Filming=== Suriya had an ankle injury due to which the shot was postponed for a period of 3 days.  Suriya trained to get a Six pack abs for a Kung Fu sequence within 16 days, following cinematographer Ravi K. Chandrans request.  Shabina Khan designed the costumes for Suriya and Shruti, staying with the team throughout the filming process in Bangkok and Hong Kong.  The song "Mun Andhi" was shot in Thailand.  Though initial reports stated that the film would feature a time machine, they were later dismissed. 
 Legacy effects Hollywood for the Terminator Jurassic Park Iron Man, Edward Scissorhands, and Avatar (2009 film)|Avatar.   

==Soundtrack==
{{Infobox album
|  Name        = 7aum Arivu
| Longtype    = to 7aum Arivu
| Type        = Soundtrack
| Artist      = Harris Jayaraj
| Cover       = 7aam Arivu Artwork.jpg
| Caption     = Original CD cover
| Release     = 22 September 2011
| Recorded    = 2011 Feature film soundtrack
| Length      = 30:29 Tamil
| Label       = Sony Music
| Producer    = Harris Jayaraj
| Reviews     =
| Last album  = Force (2011 film)|Force (2011)
| This album  = 7aum Arivu (2011)
| Next album  = Nanban (film)|Nanban (2011)
}}
{{Album ratings
| rev1 = Behindwoods
| rev1Score =   
| rev2 = Rediff
| rev2Score =   
}} Wang Hao. Jai and Anjali (actress born 1986)|Anjali, lead pair of Murugadoss production Engeyum Eppodhum, while Bollywood actress Isha Sharvani, Lakshmi Rai and several international artists performed on stage,  which were choreographed by Shobi.    Actors Dhanush, Karthi, Jeeva (actor)|Jiiva, Jayam Ravi, Vishal Krishna and Ram Charan Teja attended the function,    with Dhanush releasing the trailer.  Though initial reports said that Shahrukh Khan would appear in the launch,  it was later dismissed. 
{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    = 30:29
| lyrics_credits  = yes
| title1          = Oh Ringa Ringa
| lyrics1         = P. Vijay
| extra1          = Roshan, Jerry John, Benny Dayal & Suchitra
| length1         = 5:34
| title2          = Mun Andhi
| lyrics2         = Na. Muthukumar Karthik & Megha
| length2         = 6:14
| title3          = Yellae Lama
| lyrics3         = Na. Muthukumar Shalini & Shruti Hassan
| length3         = 5:21
| title4          = Yamma Yamma Kabilan
| extra4          = S. P. Balasubrahmanyam & Swetha Mohan
| length4         = 6:06
| title5          = Innum Enna Thozha
| lyrics5         = P. Vijay
| extra5          = Balram, Naresh Iyer & Suchith Suresan
| length5         = 4:58
| title6          = The Rise of Damo
| lyrics6         = Madhan Karky Hao Wang, Sunitha Sarathy
| length6         = 3:16
| note6           = Chinese song
}}

===Critical response===
The soundtrack received positive reviews. P. G. Devi from Behindwoods gave 3.5/5 and commented: "The album promises a couple of big chart-busting hits like the peppy Ringa Ringa, the melodious Mun Andhi and the pathos-ridden Yamma Yamma. Though there is a déjà vu feel all around the album, its a sure shot commercial success".    Indiaglitz stated that "the unique stamp of Harris can be felt throughout the disc, and that makes songs   a treat to senses", recommending it for "those who love and appreciate good music".  Pavithra Srinivasan from Rediff provided 2.5/5, mentioning that "it looks like Harris Jeyaraj has run out of steam. While Yemma Yemma and The Rise of Damo are appealing, the rest sound like he remixed some of his own older numbers, or chose to be inspired by other classics   7aum Arivus music does have its moments, but these are few and far between."    Prakash Upadhyaya from Oneindia.in said that the album had "variety of songs. The soundtracks will have larger appeal when it is watched". 

==Release== Sun TV.  The film was given a "U" certificate by the Indian Censor Board,  but did not get the 30 percent entertainment tax waiver.  The film was scheduled for a Diwali release on 26 October 2011; with producer Udhayanidhi Stalins intervention, it was brought forward by one day, opening ahead of the other films.  The distribution rights in Kerala were acquired for  24&nbsp;million, releasing in more than 100 screens.  In Chennai, the film released in 51 screens.  The film was the largest release in Suriyas career, opening with 1000 prints worldwide, with 400 prints in Tamil Nadu alone. The Telugu dubbed version, 7th Sense had the second largest release in Andhra Pradesh for a dubbed film by opening up in 400 screens across the state.  A special screening was shown to actor Kamal Haasan.  In Malaysia, the film was released in record 53 screens. In the US, the film was released in both Tamil and Telugu in 50 screens.  

===Critical reception===
7aum Arivu received mixed reviews. Indiaglitz claimed that Murugadoss had made a "bold attempt" and "succeeded in it with the help of Suriya and Udhayanidhi Stalin", lauding him for "conveying a bitter truth   in a sugar coating", and going on to claim it to be a "winner in all his invasions".  Behindwoods described it as "technical finesse catering to commercial compulsions", giving the film 3 out of 5.  Rediff gave it 3 out of 5, noting that it was "worth a watch" and had "several things working for it".  Sify called the film "average", writing that "the effort of Murugadoss to make a special kind of film is laudable but seems to lack the imagination required to pull off what he set out to achieve".  Nowrunning.com rated it 3/5 stating that "a hopeful beginning makes this routine fare an absolute disappointment. This is not a bad film. You just expect better from a director like Murugadoss". 

===Box office===
7aum Arivu took a huge opening on 25 October, a day prior to Diwali.  In its six-day opening weekend, the film grossed  402.5&nbsp;million worldwide.    In Chennai alone, the film earned  90&nbsp;million nett in its lifetime.    The film eventually emerged an average grosser at the box office, earning   worldwide, but against a high budget and distribution price. 

===Controversies===
  Tamilian rather film historian Tamil films Veerapandiya Kattabomman Parthiban Kanavu.  A man named Babu T. Raghu had pointed out in a press conference that the monk was 150 years old when he reached China, while in the film they had depicted him in his 20s. He expressed his desire to debate with the film-makers, and also said that he had material evidence to prove it, while sparking off a hunger strike across the country on Bodhidharmas followers.  Other sources also reported a similar issue.   

 
Later reports claimed that the film was very similar to the game Assassins Creed. Some were of the opinion that the film and the game were based on the same concept, despite being different in other aspects. The film Velayudham, which had released alongside 7aum Arivu was also compared with the game. 

The film was sent for CBFC certification with dummy background score and the official re-recording happened thereafter triggering protests demanding re-certification. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 