The Man Who Watched Trains Go By
{{Infobox film
| name           = The Man Who Watched Trains Go By
| image          = The-Man-Who-Watched-the-Trains-Go-By_79713fdd.jpg
| image_size     =
| caption        = The Man Who Watched Trains Go By poster (U.S. title The Paris Express)
| director       = Harold French
| producer       = David Berman Josef Shaftel Raymond Stross
| writer         = Georges Simenon (novel) Paul Jerrico Harold French
| narrator       =
| starring       = Claude Rains Marius Goring Märta Torén
| music          = Benjamin Frankel
| cinematography = Otto Heller
| editing        = Vera Campbell Arthur H. Nadel
| distributor    = Eros Films (UK) MacDonald Pictures (USA)
| released       = December 1952
| runtime        = 82 minutes
| country        = UK / USA
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

The Man Who Watched Trains Go By (1952 in film|1952) is a crime drama film, released in the United Kingdom with an all-European cast, including Claude Rains in the lead role. Rains plays the role of Kees Popinga, who is infatuated with Michele Rozier (Märta Torén). The film was released in the United States in 1953 under the title The Paris Express. It was directed by Harold French and based on the novel by Georges Simenon. This was Rains seventh film in color, his first being Gold Is Where You Find It (1938).

==Cast==
*Claude Rains as Kees Popinga
*Marius Goring as Lucas
*Märta Torén as Michele Rozier
*Ferdy Mayne as Louis
*Herbert Lom as Julius de Koster, Jr.
*Lucie Mannheim as Maria Popinga
*Anouk Aimée as Jeanne
*Eric Pohlmann as Goin
*Felix Aylmer as Mr. Merkemans
*Gibb McLaughlin as Julius de Koster, Sr.
*Michael Nightingale as Popingas Clerk

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 

 