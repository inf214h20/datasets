The Band Wagon
 
{{Infobox film
| name           = The Band Wagon
| image          = The Band Wagon poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Vincente Minnelli
| producer       =  
| writer         =  
| starring       =  
| music          =   Harry Jackson
| editing        = Albert Akst
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $2,873,000  . 
| gross          = $3,502,000 
}}
 Broadway play will restart his career. However, the plays director wants to make it a pretentious retelling of Faust, and brings in a prima ballerina who clashes with the star.
 Broadway musical The Band Wagon, with a book by George S. Kaufman and starring Fred Astaire and his sister Adele Astaire|Adele. (Fred Astaire also stars in the movie.) The movies dances and musical numbers were staged by Michael Kidd. 
 Dancing in Flying Colors. Buddy and Vilma Ebsen. In the movie version of The Band Wagon, the song was reworked to show off Astaires musical talents.
 Best Costume Best Music, Best Writing, Story and Screenplay. Screenwriters Betty Comden and Adolph Green, who received the nomination for the screenplay, patterned the films characters Lester and Lily Marton after themselves, although the fictional characters were a married couple and Comden and Green were not romantically involved.
 list of best musicals.

==Plot==
Stage and screen star Tony Hunter, a veteran of musical comedy, is concerned that his career might be in decline. His good friends Lester and Lily Marton have written a stage show that they believe is perfect for his comeback.

Tony signs up, despite misgivings after the director, Jeffrey Cordova, changes the light comedy into a dark reinterpretation of the Faust legend, with himself as the Devil and Tony as the Faust character. Tony also feels intimidated by the youth, beauty, and classical background of his female co-star, noted ballerina Gabrielle "Gaby" Gerard. Unbeknownst to him, she is just as insecure in his presence, awed by his long stardom.

Eventually, it all proves too much for Tony. He walks out, but Gaby speaks with him alone and they work out their differences. They also begin to fall in love, though she already has a commitment to the shows choreographer Paul Byrd.

When the first out-of-town tryout in New Haven proves to be a disaster, Tony persuades Jeffrey to let him convert the production back into what the Martons had originally envisioned. Tony takes charge of the production, taking the show on tour to perfect the new lighthearted musical numbers. Since the original backers have walked out, Tony finances it by selling his personal art collection. Byrd walks out, but Gaby remains.

The revised show proves to be a hit on its Broadway opening. Afterwards, Gaby lets Tony know how she feels about him.

==Cast==
 
* Fred Astaire as Tony Hunter
* Cyd Charisse as Gabrielle Gerard
* Oscar Levant as Lester Marton
* Nanette Fabray as Lily Marton
* Jack Buchanan as Jeffrey Cordova James Mitchell as Paul Byrd
* Ava Gardner as herself (cameo)

==Musical numbers==
in order of appearance 
 
# "By Myself" — Tony (introduced in the stage musical Between the Devil) Flying Colors)
# "Thats Entertainment! (song)|Thats Entertainment!" — Jeffrey, with Tony, Lester and Lily.
# "The Beggars Waltz" — danced by Cyd Charisse, James Mitchell, and corps de ballet Dancing in the Dark" — Tony and Gabrielle
# "You and the Night and the Music" — Chorus, danced by Tony and Gabrielle
# "Something to Remember You By" — Chorus
# "High and Low" — Chorus
# "I Love Louisa" — Tony, Lester, and Lily
# "New Sun in the Sky" — Gabrielle
# "I Guess Ill Have to Change My Plan" — Tony and Jeffrey Flying Colors)
# "Triplets" — Tony, Jeffrey, and Lily (The three performers dance on their knees, costumed in baby attire) (introduced in the stage musical Between the Devil)
# "Girl Hunt Ballet" — Tony and Gabrielle

One musical number shot for the film, but dropped from the final release, was a seductive dance routine featuring Charisse performing "Two-Faced Woman". As with the other Charisse songs, her singing was dubbed by India Adams. Adams recording of the song was reused for Torch Song for a musical number featuring Joan Crawford. The retrospective Thats Entertainment! III released the Charisse version to the public for the first time. This footage was also included with the most recent DVD release of The Band Wagon itself. 

==Reception==
According to MGM records the film earned $2.3 million in the US and Canada and $1,202,000 in other countries, resulting in a loss of $1,185,000. 
==Stage adaptation== musical stage adaptation, titled "Dancing in the Dark," premiered at The Old Globe Theatre (San Diego) March 4 – April 20, 2008, with plans to bring the show to Broadway theatre|Broadway. Gary Griffin directs, with a book by Douglas Carter Beane and choreography by Warren Carlyle. The cast includes Patrick Page as the "deliciously pretentious" director-actor-producer Jeffrey Cordova, Mara Davi playing Gabrielle Gerard and Scott Bakula as "song-and-dance man" Tony Hunter.    

In the Variety (magazine)|Variety review of the musical Bob Verini wrote: "Theres no reason this reconstituted "Band Wagon" cant soar once it jettisons its extraneous and self-contradictory elements. But "Dancing" is some distance from finding its footing, despite finales admonition to "Admit were a hit and well go on from there." Not yet." 

A revised version of the stage adaptation under the name The Band Wagon opened in November 2014 as part of a New York City Center Encores! special event starring Brian Stokes Mitchell, Tracey Ullman, Michael McKean, Tony Sheldon and Laura Osnes.

==Music videos==
 , Jackson notably pays homage to the film on at least three successive albums.

Steve Martin and Gilda Radner perform a seriocomic parody homage to the "dancing in the dark" dance segment on an episode of Saturday Night Live, originally broadcast on April 22, 1978.

==References==
;Notes
 

;Bibliography
* Diane Stevenson, "In Praise of Praise" in the Stanley Cavell special issue, Jeffrey Crouse (ed.), Film International, Issue 22, Vol. 4, No. 4, 2006, pp.&nbsp;6–13.

==External links==
*  
*  
*  
*  
*  
*   
*  

{{Navboxes
|title=Articles and topics related to The Band Wagon
|state=collapsed
|list1= 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 