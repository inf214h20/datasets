Joseph: King of Dreams
{{Infobox film name            = Joseph: King of Dreams image           = joseph_king_dreams.jpg caption         = DVD cover director        = Rob LaDuca Robert C. Ramirez producer        = Ken Tsumura Jeffrey Katzenberg screenplay      = Eugenia Bostwick-Singer Raymond Singer Joe Stillman Marshall Goldberg starring        = Ben Affleck Mark Hamill James Eckhouse Richard McGonagle Richard Herd music           = Daniel Pelfrey, songs and lyrics by John Bucchino editing         = Michael Andrews Greg Snyder John Venzon studio          = DreamWorks Animation distributor     = DreamWorks Pictures released        =   runtime         = 75 min. country         = United States language        = English
}}
 animated biblical musical family Joseph from the Book of Genesis in the Bible and also serves as a prequel to the 1998 film The Prince of Egypt. Composer Daniel Pelfrey said "the film was designed as a companion piece to Prince of Egypt...Of course, Joseph turned out to be very different than Prince of Egypt,   very challenging and rewarding".      

The Book of Angels explains "In this film we are shown how Joseph makes use of his dreams to guide him through his life, and where this adventure leads him". 

Co-director Robert Ramirez has said that "The reviews for Joseph have generally been very good, but   a period years ago when the film was not working very well, when the storytelling was heavy-handed, klunky and   we discovered as a crew   made it a whole lot better".   

== Plot ==
Joseph is the youngest of Jacob (Bible)|Jacobs eleven sons and a favorite of his fathers; thus inciting the brothers jealousy when Joseph grows conceited and arrogant when constantly pampered by his parents. When he receives a beautiful coat from his father, his brothers fear that he may become the clans leader upon their fathers death. One evening, Joseph dreams that the sheep his brothers are tending are attacked by Gray wolf|wolves. Later, a wolf pack attacks the flock and Joseph is nearly killed until Jacob saves him. Jacob becomes furious that Joseph was abandoned by his brothers, and amazed that Josephs dream came true. Judah (Bible)|Judah, the eldest of the brothers and their leader, merely dismisses this. The next night, Joseph dreams that his brothers each carry sheaves of wheat that bow to Josephs gigantic sheaf, and that he is a brilliant star surrounded by ten smaller stars and the sun and the moon; and Jacob predicts that Joseph shall supersede his brothers. The latter retreat to a cave and determine to do away with Joseph. Joseph overhears this, and the brothers tear his cloak and hurl him into a pit until nightfall. When withdrawn, Joseph is sold to desert slave traders, and thence into Egypt, while his brothers tell their father that he was killed by wolves.

In Egypt, Joseph is made the servant of the Egyptian dignitary   about his talent and offer of help, to secure a release from prison. The butler promises to tell Pharaoh but forgets; but Asenath supplies food to Joseph regularly.
 Simeon imprisoned. Questioned by Asenath, he reveals his past. The next day the brothers reappear with a young man named Benjamin, who is Josephs almost identical younger brother. Simeon is released and Joseph asks Benjamin about his family; to learn that Rachel is dead and his own death presumed. To exact revenge, Joseph invites the brothers to a feast and has his own golden chalice concealed in Benjamins bag while no one is looking; and upon its discovery, orders that Benjamin be enslaved. At this, his older brothers offer themselves instead, and Judah confesses having sold Joseph himself and is has haunted him and his brothers ever since. Touched by their honesty and genuine love for Benjamin, Joseph identifies himself to them, reconciles and invites them and their families to live with him at the palace. Shortly after, he is reunited with his father.

==Cast==
{{columns-list|3| Joseph speaking voice David Campbell Joseph singing voice Judah
* Richard Herd - Jacob speaking voice
** Russell Buchanan - Jacob singing voice
* Maureen McGovern - Rachel
* Jodi Benson - Asenath Zuleika
* James Eckhouse - Potiphar
* Richard McGonagle - Pharaoh
* Dan Castellaneta - Auctioneer, Horse Trader
* René Auberjonois (actor)|René Auberjonois - Butler
* Ken Campbell - Baker Steven Weber - Simeon (Hebrew Bible)|Simeon, Slave Trader
* Jess Harnell - Issachar / Lead Trader
* Piera Coppola - Zuleikas servant, Additional Voices
* Emily Eby - Servant
* Matt Levin - Benjamin
}}

==Production==

===Conception and The Prince of Egypt===
Crosswalk.com explained:

  }}

Executive Producer Penny Finkelman Cox and DreamWorks worker Kelly Sooter noted the challenge in telling a Bible story faithfully yet still making it interesting and marketable: "we had to take powerful themes and tell them in a way thats compelling and accessible for all ages". They also noted that though it was destined to be a direct-to-video project from the beginning, "the quality of the animation does not suffer...Our approach to the movie was to develop it with the same quality and storytelling that we did with Prince of Egypt", and added that "one of the most challenging parts of the movie was creating Josephs dream sequences, which look like a Van Gogh painting in motion". Sooter also explained "Its a very interactive story...It really is beneficial to be able to sit with a family and talk through some of the things that are happening."  Nassos Vakalis, who helped storyboard and animate the film, said "I had to travel a lot to Canada to see work done in a few studios that were subcontracting part of the movie".  Composer Daniel Pelfrey explained "I must say the writers and directors did a great job staying true to the story and bringing it into a presentation for a contemporary audience." 

===Early work===
Ramirez explained the early stages of the films production:

 

===Screening and production troubles===
Ramirez explained how things turned awry at the film screening:
 

===A new vision===
 

Ramirez explained the shift from disjointed set pieces to a character-driven story:

 

===Cracking the story===
Ramirez explained they cracked the story by returning to the basics of storytelling.

 

===Casting and approach to characters===
Mark Hamill, who was cast as Judah, Josephs eldest brother, explained that the choices he made regarding his character:

 

Ramirez  explained one of the main themes in the movie by analyzing how Joseph reacts upon seeing his brothers for the first time after they sold him into slavery:

 

Jodie Benson (Asenath, Josephs wife) was thrilled to work on the film, after seeing how well the team led by Jeffrey Katzenberg handled the story of Moses in The Prince of Egypt. Thus she also had a lot of faith in the production. Benson didnt audition for the part, and was instead offered it. Unlike some of the other characters, she provides both the speaking and singing voices of Asenath. It took twelve days to record her lines, and the only other voice actor she worked with was the singing voice for Joseph, David Campbell. Benson explained her character is the "voice of reason and the voice of trying to do the right thing to reconcile   with his brothers". Her character was given a much larger role than what is presented in the Bible; Benson thought developing Asenath further was a good move. 

==Music==

===Score===
All songs were produced and arranged by Danny Pelfrey, and he also composed the score. Hans Zimmer, the composer for The Prince of Egypt, had approved of Pelfrey taking over his role after the latter, a relative unknown at the time, did a couple of interviews at DreamWorks. Pelfrey explained "Through the process   gave me input as to what they like to hear, mostly through the arranging and production of the songs. After that he got too busy but he gave me the foundation and communication skills I needed to successfully complete the project".  After receiving the job, Pelfrey read as many different translations of the original Bible text as he could, to find story nuances that he could incorporate. In regard to his collaboration with DreamWorks, he said "Before starting the input was pretty sketchy, but it was an ongoing process with lots of dialog with writers, producers and directors along the way. Jeffery Katzenberg always ultimately approved everything. He was directly involved with the entire process."  He also explained "I had never done a musical before...  helped me incorporate the sounds from Prince of Egypt as well as guided me in the song production". 

Pelfrey used choral choirs sparingly in his score, with notable examples being "a small female group in the beginning for what I was calling God’s theme, and in the big scene at the end, which was the reunion of Joseph, his brothers and Jacob, his father". This was because the effect reminded him of angels, adding "I also I think it was more appropriate to the sonic tapestry and created a more uplifting feeling".  He described his musical style in the film as "World/Orchestral", noting that the instruments used were more regional than specifically Egyptian, incprporating: "Duduk, Ney, Rebaba, Ban-Di, Bansuri, Moroccan Flute, Zampona, and a great variety of percussion including Djmbe, Darabuk, Dholak, Udu, etc etc". In regard to using instrumentation from an inaccurate historical context, he said "I always thought...that the exact historical and geographical use of the instruments is not as important as the evocative or dramatic effect...So, I didn’t really concern myself too much with right place, right time.  A temp-track was made for the score, though Dreamworks "were not too attached to it"; some parts were tracked with "Fantasia on a Theme by Thomas Tallis" by Vaughan Williams.

Pelfrey said "Since I had never done a musical before, it was interesting to note the difference between producing these songs as opposed to doing a record. In a musical, the songs advance the story and I had to help that process, as well as make the songs belong to the fabric of the film and the palette of the score. Although this was animation, it certainly did not call for a cartoon approach, due to the depth of the story. The film needed more of a live-action treatment to the score. "Joseph: King of Dreams also allowed me to work with the best producers in the business and helped make this a very successful experience both personally and professionally."  He explained "  is the reason the Symphonic Suite from Joseph was created. He contacted me about wanting to present it in a concert he was doing in Knoxville where he is the conductor and music director, so I created the suite especially for them. He has created a vibrant and thriving orchestra there and they were all very welcoming to me." It was performed in LA by the Los Angeles Jewish Symphony in August 2010.  

===Songs===
Music and lyrics to all seven of the songs were written by John Bucchino. A soundtrack was not released with the film. 

{{columns-list|3| David Campbell)
# "Bloom" (Maureen McGovern)
# "Marketplace" (Ensemble Cast)
# "Whatever Roads at Your Feet" (David Campbell) Better Than I" (David Campbell)
# "More than You Take" (David Campbell & Jodi Benson)
# "Bloom" (Reprise) (Jodi Benson)\
}}

==Release==
As the only DreamWorks Animation   and  . 

The direct-to-video film was "made available to Christian retailers, but mainly will be sold in traditional retailers such as Wal-Mart and Target and video stores". The sale success of Joseph was to some degree influence whether more animated Bible stories would be released by DreamWorks.  As of 2014, Prince and Joseph have been the only two.

===Book tie-ins===
Nashville publisher Tommy Nelson, the kids division of the Christian publishing company Thomas Nelson Inc., partnered DreamWorks to publish four companion book titles based on the film, and has exclusive publishing rights to Joseph ("a read-along tape, a sticker storybook, a 48-page hardcover storybook with illustrations from the film, and a smaller hardcover storybook which retells the story of Joseph" ). One of them, My Sticker Storybook: Joseph and his Brothers (published 1 Nov 2000) was a sticker storybook that followed the plot Joseph, and was written by Dandi Daley Mackall.  The 48-page storybook (published1 Nov 2000, and sometimes subtitled "Classic Edition") featured images from the film, a retelling by Mackall, and was a "stand-alone book, as well as a splendid companion to the video", also written by Mackall.  Joseph, King of Dreams: read-along (8 Mar 2001) was a full-color storybook and accompanying cassette which "capture  all the emotional and dramatic high points". Written by Catherine McCafferty, it included the song "Better Than I" and dialogue from the film.  A fourth book was published as well.

==Critical reception==
 

The film received mixed to positive reviews from critics. While praising the films merits including animation, storytelling, and music, much of the criticism came with comparing it negatively to its theatrically released sequel The Prince of Egypt. The song You Know Better Than I was singled out for praise by numerous critics, as were the van Gogh-inspired dream sequences. Many noted that the animated hieroglyph effects were similar to those from Prince, and suggested that the film stuck closer to the Bible source material than Prince too.

DecentFilmsGuide gave the movie a B for Overall Recommendability and 3/4 stars for Artistic/Entertainment Value, writing "Artistically, the best thing about Joseph: King of Dreams is the visionary animation work in the dream sequences...I caught my breath at the first glimpse of these dreams, which look like living, flowing Van Goghs". However it wrote "Joseph: King of Dreams is not remotely in the same class as The Prince of Egypt.   is much more a children’s movie". It said the songs "while cheerful and uplifting, are generally unmemorable", and described the animation as "fine but not wonderful". It noted that "once one stops making unfair comparisons to a theatrical film made on a much bigger budget, Joseph: King of Dreams is very much worthwhile on its own more modest terms". Nevertheless, the review complimented the "ominous tune Marketplace, and said "In one small way, Joseph: King of Dreams even outshines the earlier film: The spirituality of its signature song, You Know Better Than I, is much more profound than anything in the more mainstream "There Can Be Miracles".  DVD Verdict wrote "Joseph: King of Dreams will shatter any expectations you may have about direct-to-video animated features. This is no halfhearted attempt to cash in on the success of The Prince of Egypt, but is instead a fully realized and carefully crafted story of its own. This film could easily have been released theatrically, although its running time is maybe just a bit short for that", praising its animation, music, and storytelling.  PluggedIn wrote "while not as eye-popping as Prince of Egypt,   is impressive for a direct-to-video title. Artfully executed dream sequences. Uplifting songs. It also takes fewer liberties than Prince of Egypt did".  Lakeland Ledger said "At its best, the story communicated the sense of desperation and yearning that make up the tale and provides a sense of the emotions that underscore the story".  Jan Crain Rudeen of Star-News wrote "As with Price of Egypt, the best part of Joseph for me was the discussion it sparked afterward with my kids". 

The Movie Report gave the film 3.4 stars, writing "while clearly not on the level of that 1998 classic, it is a solid piece of work that is about on par with the SKGs spring theatrical release The Road to El Dorado"...Joseph is a new technical benchmark for straight-to-tape animated features, putting Disneys chintzy home video efforts to shame. It added "Bucchinos work is downright forgettable; the only song making the slightest inkling of an impression is Josephs--and the films--central number, Better Than I".  ChristianAnswers.net gave the film 4/5 stars, writing "Although the visual effects were not as outstanding as in The Prince of Egypt, the storyline does stay closer to the biblical version". The site added "The music was enjoyable, especially the song Better Than I".  "CommonSenseMedia rated the film 3/5 stars, writing "The animation is accomplished. Particularly compelling are the dream sequences, which almost look like animated Van Gogh paintings", however noting "it lacks   Egypts poignant tunes and powerful storytelling".  The Los Angeles Times wrote "with its beautiful, big-screen quality, flowing animation and striking computer-generated imagery--and with its dignity and heart--is a fine telling of the biblical story".  Variety (magazine)|Variety said "King of Dreams has just as much cross-generational appeal as its predecessor, and doesnt make the mistake of skewing primarily toward moppets. To put it another way: This is family entertainment in the best sense of the term, for which many families will be immensely grateful." 

===Awards and nominations===

 
|-
| 2000
| "Better Than I"
| Video Premier Award for Best Song
|   
|-
| 2001
| Joseph: King of Dreams
| Silver Angel Award for Feature Film
|  
|-
| 2001
| Joseph: King of Dreams
| Annie Award for Outstanding Achievement in an Animated Home Video Production
|  
|-
| 2001
| Penney Finkelman Cox (executive producer) Steve Hickner (executive producer) Jeffrey Katzenberg (executive producer) Ken Tsumura (producer) DVD Exclusive Video Premiere Award for Best Animated Video Premiere
|  
|-
| 2001
| Eugenia Bostwick-Singer Marshall Goldberg Raymond Singer Joe Stillman 
| DVD Exclusive Video Premiere Award for Best Screenplay
|  
|-
| 2001
| Ben Affleck (voice) Luc Chamberland (animation director: Joseph) 
| DVD Exclusive Video Premiere Award for Best Animated Character Performance
|  
|-
| 2001
| Rob LaDuca Robert C. Ramirez
| DVD Exclusive Video Premiere Award for Best Directing
|  
|-
| 2001
| Daniel Pelfrey
| DVD Exclusive Video Premiere Award for Best Original Score
|  
|}

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 