Marco the Magnificent
{{Infobox film
| name           =La Fabuleuse aventure de Marco Polo
| image          =La Fabuleuse aventure de Marco Polo.jpg
| image_size     =
| caption        =
| director       = Denys de La Patellière and Raoul Lévy
| producer       =
| writer         =Noël Howard, Raoul Lévy
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1965
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1965 International co-production (Afghanistan, Socialist Federal Republic of Yugoslavia|Yugoslavia, Egypt, France, Italy) adventure film directed by Denys de La Patellière and Raoul Lévy.

==Cast==
*Horst Buchholz	... 	Marco Polo
*Anthony Quinn	... 	Kublai Khan, Mongol Emperor of China
*Akim Tamiroff	... 	The Old Man of the Mountain
*Elsa Martinelli	... 	The Woman with the Whip
*Robert Hossein	... 	Prince Nayam, Mongol Rebel Leader
*Grégoire Aslan	... 	Achmed Abdullah
*Omar Sharif	... 	Sheik Alla Hou, The Desert Wind
*Orson Welles	... 	Akerman, Marcos Tutor
*Massimo Girotti	... 	Nicolo, Marcos Father
*Folco Lulli	... 	Spinello, Venetian Merchant
*Guido Alberti	... 	Pope Gregory X
*Lynne Sue Moon	... 	Princess Gogatine (as Lee Sue Moon)
*Bruno Cremer... 	Guillaume de Tripoli, a Knight Templar
*Jacques Monod	... 	Nicolo de Vicenza, a Knight Templar
*Mica Orlovic	... 	Matteo, Marcos Uncle

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 