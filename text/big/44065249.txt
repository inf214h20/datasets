Ammaayi Amma
{{Infobox film
| name = Ammaayi Amma
| image =
| caption =
| director = M Masthan
| producer = Harifa Rasheed
| writer = Pappanamkodu Lakshmanan
| screenplay = Pappanamkodu Lakshmanan
| starring = Sukumari Jayabharathi Adoor Bhasi Ashalatha
| music = A. T. Ummer
| cinematography = KN Sai
| editing = K Sankunni
| studio = HR Combines
| distributor = HR Combines
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by M Masthan and produced by Harifa Rasheed. The film stars Sukumari, Jayabharathi, Adoor Bhasi and Ashalatha in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
  
*Sukumari 
*Jayabharathi 
*Adoor Bhasi 
*Ashalatha
*Pattom Sadan 
*Sankaradi 
*Sreelatha Namboothiri 
*Prathapachandran  Kanchana 
*MG Soman  Meena 
*PK Venukkuttan Nair
*PR Varalekshmi
*Sreekala Vincent 
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Anukuttan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aattinkutti Thullichaadi || S Janaki || Anukuttan || 
|- 
| 2 || Krishna Jagannaadha || P Susheela || Anukuttan || 
|- 
| 3 || Mazhavil Maanathinte Maaril || K. J. Yesudas, S Janaki || Anukuttan || 
|}

==References==
 

==External links==
*  

 
 
 


 