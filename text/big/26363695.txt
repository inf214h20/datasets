Ghosts on the Loose
{{Infobox film
| name           = Ghosts on the Loose
| image	=	Ghosts on the Loose FilmPoster.jpeg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = William Beaudine Barney A. Sarecky (associate producer) Kenneth Higgins (original screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Mack Stengler
| editing        = Carl Pierson
| distributor    =
| studio         = Monogram Pictures
| released       =  
| runtime        = 67 minutes (DVD) 65 minutes (copyright length)
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Ghosts on the Loose is a 1943 American film and the fourteenth film in the East Side Kids series, directed by William Beaudine.

The film was released in the United Kingdom as Ghosts in the Night. 

==Plot== Sammy Morrison), Billy Benedict) Katzman Gang, (the producer of the film series).

On this happy day only one thing is slightly bothering Jack. The house he has purchased is well below the market value due to rumours that the house next door is a haunted house. The house next door is actually used by a German spy ring led by Emil (Bela Lugosi). Emil is furious that his minion has sold the neighbouring house to Jack as it will be needed for future activities as both houses are connected by secret tunnels. Emil orders his minion, Tony (Wheeler Oakman) to buy it back from Jack.

Jack is mystified by the reasons for the house being wanted by another party. Jack does accept the money for the sale where the minion gives him a note with the address of the neighbouring "haunted" house where he can be reached.

On his way to their honeymoon Jack drops the note with the address of the neighbouring house. Muggs picks up the address thinking it is the house that Jack and Betty are moving into and decides to surprise the couple by having the gang clean and tidy the house before the couple arrive.

At the Honeymoon Hotel Jack is given an urgent message to contact the party who originally sold him the house.  The wife (Blanche Payson) is worried about the strange activities in the house next door to the house Jack bought leading to the haunted rumours.  She wishes to warn Jack and she also telephones the police to investigate. Jack and Betty drive to their house to get to the bottom of the rumours.
 New Order entitled "How to destroy the Allies of World War II|Allies". As Jack and Betty and the police arrive the gang takes on Emil and his spy ring.
 German Measles (his face is decorated with swastikas).

==Cast==

===The East Side Kids===
*Leo Gorcey as Mugs
*Huntz Hall as Glimpy
*Bobby Jordan as Danny Sammy Morrison as Scruno
*Billy Benedict as Benny (a.k.a. Skinny)
*Stanley Clements as Stash
*Bobby Stone as Dave (a.k.a. Rocky) Bill Bates as Sleepy (a.k.a. Dave)

===Additional cast===
*Bela Lugosi as Emil
*Ava Gardner as Betty
*Rick Vallin as Jack
*Minerva Urecal as Hilda
*Wheeler Oakman as Tony
*Peter Seal as Bruno
*Frank Moran as Monk
*Jack Mulhall as Lieutenant
*Harry Depp as John G. Elwood (uncredited)
*Tom Herbert as Park Central Plaza Desk Clerk (uncredited)
*Robert F. Hill as Minister at Wedding (uncredited)
*Eddie Laughton as Wedding Usher (uncredited)
*Kay Marvis as Bridesmaid (uncredited) Ray Miller as Police Officer Mulligan (uncredited)
*Blanche Payson as Mrs. John G. Elwood (uncredited)
*Snub Pollard as Flower Delivery Man (uncredited)

==Production==
* Later on in the film, when The East Side Kids, Rick Vallin, and Ava Gardner are waiting for the cops, Muggs says, "They should be here any minute". Glimpy responds by saying, "Who?", to which Muggs says, "Oh, hes on first". This is an obvious reference to the vaudeville routine Whos on First, made famous during this period by Abbott and Costello.
 rationing of coffee from 1942 onwards.

* Bill Bates only East Side Kids film.
 Follow the Leader (his scenes were actually unused footage from a previous East Side Kids film). Clements would not work with the boys again until 1956, when he was brought in to replace Leo Gorcey as the leader of The Bowery Boys.

*Gabriel Dell and Dave Durand are absent.

*Leo Gorceys wife Kay Marvis appears as a bridesmaid during the wedding scenes.

==Soundtrack==
* Bill Bates and sung by The East Side Kids - "Drink to Me Only with Thine Eyes" (Music by R. Melish, lyrics (poem "To Celia" by Ben Jonson)
* Bill Bates - "Bridal Chorus (Here Comes the Bride)" from Lohengrin (opera)|Lohengrin (Written by Richard Wagner) The Wedding A Midsummer Felix Mendelssohn-Bartholdy)

==References==
 

==External links==
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 