House of the Damned (1996 film)
{{Infobox film
| name           = House of the Damned
| image          = 
| caption        = 
| director       = Sean Weathers
| producer       = Aswad Issa
| writer         = Sean Weathers
| starring       = Valerie Alexander
| music          = Sean Weathers
| distributor    = Full Circle Filmworks
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
House of the Damned is a 1996 American zombie horror comedy film that was written and directed by Sean Weathers. It was first released on August 20, 1996 through Full Circle Filmworks, but was re-released in 2011 through Music Video Distributors (MVD). The film is Weathers first film and was dedicated to his best friend and script supervisor Jahvaughn Lambert, who committed suicide before the film was finished editing.

== Plot ==
After the gruesome and mysterious murder of her father, Liz goes back home for answers, only to be greeted by more questions and the horrible possibility that her mother may be the killer. Before Liz can comprehend everything going on around her, she finds that the murder was just a ruse to get her home and that she is the killers true target.

== Cast ==
* Valerie Alexander as Liz 
* Blue as Heather
* Glenn "Illa" Skeete as Ben
* Monica Williams as Emily
* Buddy Love as David
* Johnny Black as Nahum
* Walter Romney as Steve
* Kendra Ware as Stacy
* Clinton Philbert as Kevin

==Reception==
In a mixed review, Bill Gibron of DVD Talk wrote that while it was "barely watchable at times" it was "mostly enjoyable" and "does grab us with its uniqueness and backdrop".  HorrorNews.nets review was slightly more positive, remarking on the films uniqueness and commenting that the films "over the top" feeling "  to the charm of the film".   David Johnson of DVD Verdict wrote that the film was barely comprehensible and horribly executed.   Rod MacDonald of SF Crowsnest wrote that it is "excruciating to watch". 

== See also ==
* List of zombie films
* List of post-1960s films in black-and-white
* Boston in fiction

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 