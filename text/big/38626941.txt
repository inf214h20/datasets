Un peu de soleil dans l'eau froide
{{Infobox film
| name           = Un peu de soleil dans leau froide
| image size     = 
| image	         = Un peu de soleil dans leau froide.jpg	
| caption        = 
| director       = Jacques Deray
| producer       = 
| writer         = Jacques Deray Jean-Claude Carrière
| starring       = Claudine Auger
| music          = Michel Legrand
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}
Un peu de soleil dans leau froide, internationally released as A Few Hours of Sunlight and A Little Sun in Cold Water, is a 1971 French film directed Jacques Deray adapted from the novel of Françoise Sagan.  The title quotes the poet Paul Éluard.

== Synopsis ==
In Limoges, Nathalie Silvener, a married woman falls for Gilles, a depressed and brilliant parisian journalist, himself in a relationship with a model.

== Casting ==
* Claudine Auger : Nathalie Silvener
* Marc Porel : Gilles Lantier
* Judith Magre : Odile
* Barbara Bach : Héloïse / Elvire
* Jean-Claude Carrière : François, le mari de Nathalie
* Gérard Depardieu : Pierre, le frère de Nathalie
* Jacques Debary : Fairmont
* André Falcon : Florent 
* Marc Eyraud : Rouargue
* Mireille Perrey
* Roger Pelletier 		
* Guido Mannari : Thomas

==Reception== Time Out was very critical, calling it "fatuous" and saying "Porel gives one of the most boring, suburban, asexual performances imaginable".  DVD Talk called it "dated and dull". 

==The novel==
It is based on a 1969 book by Françoise Sagan. BSCNews calls it "a superb novel", praising Sagans "simple and poetic" style. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 