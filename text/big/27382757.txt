Beyond a Reasonable Doubt (2009 film)
{{Infobox film
| name = Beyond a Reasonable Doubt
| image = Beyond a Reasonable Doubt 2009 Poster.jpg
| alt=
| caption =
| director = Peter Hyams
| producer = Steven Saxton Stephanie Caleb
| writer = Peter Hyams
| starring = Jesse Metcalfe Amber Tamblyn Michael Douglas Joel Moore Orlando Jones
| music = David Shire
| cinematography = Peter Hyams
| editing = Jeff Gullo
| studio = Foresight Unlimited RKO Pictures Signature Pictures
| distributor = After Dark Films Anchor Bay Films Autonomous Films
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| budget = $25 million
| gross = $3,992,844 
}} American film, 1956 film of the same name by Fritz Lang. Written, directed and filmed by Peter Hyams, the new version starred Jesse Metcalfe, Michael Douglas and Amber Tamblyn. The production was announced in February 2008 and filming began the following month. 

==Plot==
 
The film opens on a close-up of a frightened womans eyes. The camera pulls back to reveal that shes undergoing a blind tasting of coffee by local TV reporter CJ Nicholas (Jesse Metcalfe). He heads to the courthouse to watch Shreveport District Attorney Mark Hunter (Michael Douglas) give his final argument on a murder case. CJ pigeonholes Assistant D.A. Ella Crystal (Amber Tamblyn) afterward and asks her for the videotape of the murderers interrogation. He also presses her for a date. She reluctantly agrees to both.

At the TV station, CJ drags cameraman Corey Finley (Joel Moore) into his editors office and pitches a corruption story on Hunter. The video reveals that the interrogating officer, Lt. Merchant, offers the murderer a cigarette. CJ theorizes that Hunter instructed Merchant to plant the butt at the murder scene. Nicholas argues that most of Hunters high profile convictions rely too greatly on circumstantial evidence to be a coincidence. The editor rejects the story, informing CJ and Corey that he is shutting down their investigative series.

CJ convinces Corey to lay a trap for Hunter by planting circumstantial evidence that incriminates CJ. After a prostitute is murdered, CJ pays off Detective Ben Nickerson (Orlando Jones) to get access to the full report. A witness walking his Jack Russell terrier claims to have seen the man in a black ski mask and sweat suit struggling with the victim. The witness dog attacked. A footprint at the scene was made by a Montalvo tennis shoe, which is no longer produced. The murder weapon was determined to be a switchblade knife.

CJ and Corey start by buying the knife at a gun store. He buys a pair of Montalvos from BidMore4Le$$. They purchase the sweat suit and ski mask and then rescue a Jack Russell terrier from a shelter. CJ is alone with the dog when he cries out in pain. He claims to Corey that the dog bit him before he was ready. The wound is on his left calf, just as the police report indicates.

Finally, CJ douses himself in liquor and drives recklessly through town to get himself arrested. As Corey bails him out, Det. Nickerson notices his Montalvo shoes. Det. Nickerson gets a search warrant and finds all the evidence CJ has planted for them. Lt. Merchant takes over the investigation, which flusters Det. Nickerson.

During the trial, CJ lets D.A. Hunter lay out the entire case against him, which relies entirely on circumstantial evidence. Lt. Merchant warns Hunter that when he looked at CJs bank statements, all the purchases showed up. He says that his initial investigation revealed that Corey accompanied him for all the transactions. Merchant surmises that CJ is trying to trap them. Hunter says, "We cant have that, can we?"

When Hunter rests his prosecution, Corey races to his apartment to get the DVD which contains all the incriminating video. His apartment has been ransacked and the disc is missing. He rushes to a bank where he retrieves a copy from his safe deposit box. On his way back to the courtroom, Merchant chases Corey into a fatal accident, destroying the DVD.

CJ is sentenced to the death penalty, despite revealing the truth on the stand. The lack of any corroborating evidence dooms him. As Hunter points out, just because he has receipts for sweatpants and knives after the murder occurred, it does not eliminate the possibility that he already owned similar items prior to the murder. Once in prison, CJ reveals to Ella what hes been doing. Skeptical at first, Ella begins her own investigation into Hunter.

She eventually retrieves the crime scene photo from the first murder. Digital forensics reveal that the cigarette butt was inserted into the photo. As she leaves the lab, Lt. Merchant chases her through a parking lot. Just as he is about to run her over, Det. Nickerson shoots him. Nickerson informs Ella that he has suspected Merchant of corruption for a while.

Hunter is arrested, and CJ is freed. Back home together, Ella is watching the news as CJ sleeps beside her. The newscaster points out that CJs exoneration means that the real murderer of the prostitute is still loose. When Ella sees the picture of the prostitute on the TV, she immediately recognizes the star tattoos on her hands as the same ones on the homeless woman from CJs award-winning report. She quietly calls the cops and puts on her clothes. When CJ wakes, Ella confronts him. The homeless woman never existed, and CJ paid the prostitute to pose for the completely false story. She followed him to Shreveport and was blackmailing him. CJ insists that catching Hunter was worth the cost of the prostitutes life. Ella walks out on him, yelling at him "the four letter word", as the police arrive.

==Critical reception==
The movie was a critical and commercial failure. It has a 7% rating on Rotten Tomatoes, based on 28 reviews.  The New York Times concluded that Amber Tamblyn looked "thoroughly bored" throughout the proceedings.  Variety (magazine)|Variety called the remake "entirely soulless".  The Los Angeles Times wrote that "the leads cant lend either spunk or gravitas to what was already a preposterous yarn 50 years ago". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 