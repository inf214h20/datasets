The Big Noise (1936 film)
{{Infobox film
| name =  The Big Noise
| image =
| image_size =
| caption =
| director = Alex Bryce John Findlay
| writer = Gerard Fairlie   Gene Markey   Harry Ruskin
| narrator =
| starring = Alastair Sim   Norah Howard   Fred Duprez 
| music = Colin Wark
| cinematography = Stanley Grant
| editing = 
| studio = Fox Film Company
| distributor = Fox Film Company 
| released = 1936
| runtime = 65 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website = musical comedy film directed by Alex Bryce and starring Alastair Sim, Norah Howard and Fred Duprez. The film was a quota quickie made at Wembley Studios by the Hollywood studio 20th Century Fox|Foxs British subsidiary. 

==Synopsis==
A clerk in an oil company is promoted in order to make him the fall guy for a series of illegal transactions that have brought it to the brink of ruin, but instead manages to turn the business around

==Cast==
* Alastair Sim as Finny 
* Norah Howard as Mary Miller 
* Fred Duprez as Henry Hadley 
* Grizelda Harvey as Consuelo 
* C. Denier Warren as E. Pinkerton Gale 
* Viola Compton as Mrs. Dayton 
* Peter Popp as Jenkins 
* Howard Douglas as Gluckstein 
* Reginald Forsyth as Orchestra Leader 
* Edie Martin as Old Lady

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 