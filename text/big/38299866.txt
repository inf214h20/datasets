The Night Riders (1920 film)
 
 
{{Infobox film
| name           = The Night Riders
| image          =
| caption        =
| director       = Alexander Butler 
| producer       = G.B. Samuelson
| writer         = Ridgwell Cullum    (novel)   Irene Miller
| starring       = Maudie Dunham   Albert Ray   Alexander Butler   Russell Gordon
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors 
| released       = July 1920
| runtime        = 5,000 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent western Universal City Cornish emigrant to Canada battles against cattle rustlers in Alberta.

==Cast==
* Maudie Dunham as Diana Marbolt 
* Albert Ray as John Tresler 
* "Andre Beaulieu" (Alexander Butler) as Jack Marbolt 
* Russell Gordon as Jake Harnach 
* C. McCarthy as Doctor Ostler  Joe De La Cruz as Undetermined Role 
* Goober Glenn   
* William Ryno

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 