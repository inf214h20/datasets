Three Naked Sisters: Lewdness
{{Infobox film
| name = Three Naked Sisters: Lewdness
| image = Three Naked Sisters - Lewdness.jpg
| image_size = 210px
| caption = Theatrical poster for Three Naked Sisters: Lewdness (2006)
| director = Yasufumi Tanaka 
| producer = 
| writer = Yasufumi Tanaka Tadashi Naitō Akira Fukuhara
| narrator = 
| starring = Mayu Asada Sakurako Kaoru Komari Awashima
| music = Ichimi Ōba
| cinematography = Katsuji Oyamada
| editing = Shōji Sakai
| studio = Haikyū
| distributor = Shintōhō
| released = March 31, 2006
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2006 Japanese pink film co-written and directed by Yasufumi Tanaka. It won the award for Tenth Best Film at the Pink Grand Prix ceremony.    The film was also given a Pearl Prize at the Kansai region Pinky Ribbon Awards.  The film was director Tanakas debut work,  and he was given a Best New Director award at the Pink Grand Prix for this film. 

==Synopsis==
   

==Cast==
* Mayu Asada as Hoshimi Chiyoda 
* Sakurako Kaoru as 華乃美月
* Komari Awashima ( ) as 華乃美夜
*  Masayoshi Takeda ( ) as Susumu Yoshioka
* Yasushi Takemoto ( ) as Masashi Ōtsuka
* Yumi Yoshiyuki as Yumiko Ōtsuka
* Osamu Tanpopo ( ) as Hiroshi Chiyoda
* Korehiko Kazama ( ) as Man A
* Tomohiro Okada ( ) as Man B
* Mutsuo Yoshioka ( ) as Man C

==Availability==
Yasufumi Tanaka filmed Three Naked Sisters: Lewdness for Haikyū and it was released theatrically in Japan by Shintōhō on March 31, 2006.    Its home video release title is  .  Three Naked Sisters: Lewdness was released on DVD in Japan on March 7, 2008,  and became available through the Hokuto Corporations online DMM service on January 30, 2009. 

==Bibliography==
*  
*  
*  

==External links==
*   at xcity.jp / Shintoho (Official site)

==Notes==
 

 
 
 
 
 

 

 
 
 
 


 
 