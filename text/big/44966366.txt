Muck (film)
{{Infobox film
| name           = Muck
| image          =Muck 2015 film poster.png
| caption        =Film poster
| director       = Steve Wolsh
| writer         = Steve Wolsh
| starring       = Lachlan Buchanan Puja Mohindra Bryce Draper Stephanie Danielson Laura Jacobs Grant Alan Ouzts Lauren Francesca Jaclyn Swedberg Kane Hodder
| released        =March 13, 2015
| runtime        = 90 Minutes
| country        = USA
| language       = English
| budget         = $250,000
| distributor    = Anchor Bay Entertainment 
| studio         = WithAnO Productions

}}

Muck is a 2015 horror film and the directorial debut of Steve Wolsh, who also wrote and produced the film.  It had its world premiere on February 26, 2015 at The Playboy Mansion and received a limited theatrical release on March 13 of the same year, followed by a video on demand release on March 17.    It stars Lachlan Buchanan, Playboy Playmate of the Year 2012 Jaclyn Swedberg, Stephanie Danielson, YouTube star Lauren Francesca, and Kane Hodder. Funding for Muck was partially raised through a successful Kickstarter campaign that raised $266,325. 

Muck is the first film in a trilogy and will be followed by Muck: Feast of Saint Patrick, which will serve as a prequel to the events in the 2015 film.  
 
==Plot==
 
Five friends break into a holiday home in Cape Cod to hide from a group of albino killers on the loose. 

==Cast==
* Lachlan Buchanan as Troit
* Puja Mohindra as Chandi
* Jaclyn Swedberg as Terra
* Bryce Draper as Noah
* Lauren Francesca as Mia
* Stephanie Danielson as Kylie
* Laura Jacobs as Desiree
* Grant Alan Ouzts as Billy J. Munch
* Kane Hodder as Grawesome Crutal
* Gia Skova as Victoria Cougar
* Audra Van Hees as Miss Cape Cod 2013
* Ashley Green Elizabeth as Miss Cape Cod 2012
* Mike Perfetuo as Ned
* Matt Perfetuo as Lucky
* Victoria Sophia as Dedee
* Leila Knight as Kitty
* Victoria Meincke as Messa

==Soundtrack==
The Muck soundtrack features 20 original tracks, and is a mixture of rock, blues, and country. The original score was composed by Dan Marschak and Miles Senzaki. 

==Reception==
Critical reception for Muck has been predominantly negative and the film holds a rating of 0% on Rotten Tomatoes, based on 5 reviews.      Common elements of criticism centered upon the films plot, which the Village Voice considered "as scant and demeaning as the costumes".  The Los Angeles Times and Bloody Disgusting noted that while the film appeared to be meant as "a throwback to old-school horror", but that "Rather than evincing any expertise or affinity for the genre, Wolshs effort seems glib and hollow."   We Got This Covered also panned the film, writing that "Muck isnt a horror movie - its a wannabe skinflick that would ignore an on-screen kill if the actress breasts were jiggling in the slightest. Because bewbz!" 

In contrast Aint It Cool News gave a more favorable opinion of the movie and wrote "About as subtle as stripper in church, don’t expect your brain to be stimulated, but other areas (specifically your guts and below) are bound to be rumbled when you wade through MUCK." 

==References==
 

==External links==
*  
* IMDB
**  
**  , prequel

 
 