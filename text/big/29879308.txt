The Message (2009 film)
 
 
{{Infobox film
| name = The Message
|image=Themessage2009.jpg
|caption=poster
| director = Chen Kuo-Fu Gao Qunshu
| producer = Chen Kuo-Fu Wang Zhongjun Wang Zhonglei
| writer = Chen Kuo-Fu Zhang Jialu
| based on =  
| starring = Zhou Xun Li Bingbing Huang Xiaoming Zhang Hanyu Alec Su
| music = Michiru Oshima
| cinematography = Jake Pollock
| editing = Yang Xiao
| distributor = Huayi Brothers
| studio = 
| released =  
| runtime = 118 minutes
| country = China
| language = Mandarin
| budget = $7 million
| gross = $ unknown
}}
The Message ( : Fēngshēng, literally "Sound of the Wind") is a 2009  : Fēngshēng), and was co-directed by Chen Kuo-Fu and Gao Qunshu.

Despite being a blockbuster, The Message has received extensive critical praise and was nominated for a total of thirteen awards at the 2009 Golden Horse Film Festival, 2010 Asian Film Awards, 2010 Hong Kong Film Awards, and 2010 Hundred Flowers Awards. It won the best film award at 17th Beijing University Student Film Festival.  Li Bingbing won the Best Leading Actress Award at the 46th Golden Horse Film Awards for her role as the code-breaker chief in this movie. 

==Synopsis==
In Nanking 1942, following a series of assassination attempts on officials of the Japanese-controlled puppet government, the Japanese spy chief Taketa (Huang Xiaoming) gathers a group of suspects in a mansion house for questioning. A tense game of "cat and mouse" ensues as the Chinese espionage agent attempts to send out a crucial message while protecting his/her own identity. 

April 26, 1940, former Nationalist vice president Wang Jingwei made peace with Japan and set up a Japan-supported regime during World War II, a puppet government. Oct 10, during an anniversary ceremony of the government, a Wang government high official was assassinated. Taketa (Huang Xiaoming), chief intelligence officer of the Japanese Imperial Army, believed that it was an action of an underground anti-Japan group "Old Ghost". He believed there was a mole, nicknamed "Old Ghost" (aka The Phantom) inside the Wang governments Anti-Communist Command. Determined to catch Old Ghost, Taketa sent a false telegraph and arrested 5 suspects who saw the telegraph, bringing them to the closely guarded fortress Qiu Castle.

They are:
(1) Anti-Communist squad captain Wu Zhiguo (Zhang Hanyu), 
(2) Bai Xiaonian (Su Youpeng), an aide to the commander, 
(3) chief telegraph decoder Li Ningyu (Li Bingbing), 
(4) mailroom staff Gu Xiaomeng (Zhou Xun) 
(5) military intelligence director Jin Shenghuo (Ying Da)

Taketa (Huang Xiaoming)and Wang Governments intelligence chief Wang Tianxiang (Wang Zhiwen) need to find out the "Old Ghost" in five days. Staying in the closed fortress for five days, the suspects were tormented from constant interrogations. They began snitching against each other for their own survival, and there were more and more horrific physical torture coming up. The interrogators were getting impatient, and the tormented suspects were about to lose their sanity.

 
 

==Casting==

===Main cast===
*Huang Xiaoming as	Takeda (Wutian)
*Zhou Xun as Gu Xiaomeng
*Li Bingbing as Li Ningyu
*Zhang Hanyu as Wu Zhiguo
*Wang Zhiwen as Wang Daoxiang
*Su Youpeng (Alec Su) as Bai Xiaonian
*Ying Da as Jin Shenghuo

===Supporting cast===
*Ai Dai 
*Liu Jiajia
*Liu Weiwei
*Shi Zhaoqi
*Wu Gang
*Zhu Xu
*Zhang Yibai
*Duan Yihong
*Ni Dahong

==References==
 

==External links==
* Official website (English):  
* Official website (Chinese):  
*  

 

 
 
 
 
 
 
 