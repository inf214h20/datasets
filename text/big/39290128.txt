Réquiem para Laura Martin
{{Infobox film
| name           = Réquiem para Laura Martin
| image          = Réquiem para Laura Martin Poster.jpg
| caption        = 
| director       = Luiz Rangel Paulo Duarte
| producer       = 
| writer         = Paulo Duarte
| starring       = Anselmo Vasconcellos Claudia Alencar Ana Paula Serpa
| music          = Paulo Duarte
| cinematography = 
| editing        = Paulo Duarte
| studio         = FM Produções
| distributor    = 
| released       =  
| runtime        = 
| country        = Brazil
| language       = Portuguese
| budget         = 
| preceded_by    = 
| followed_by    = 
| mpaa_rating    = 
| tv_rating      = 
}}

Réquiem para Laura Martin is a 2011 Brazilian drama film directed by Luiz Rangel and Paulo Duarte, starring Anselmo Vasconcelos, Claudia Alencar and Ana Paula Serpa. 
 Gramado Film Festival, winning the awards for best direction and best actress, for Claudia Alencar, at the Festcine Petrópolis. It was nominated for three categories at the Madrid International Film Festival, including the categories of best film, best foreign film and best song, composed by Paulo Duarte, Claudio Girardi and Marconi de Morais. 

==Plot==
A famous conductor divides his obsession for music with the sickly love for his muse Laura Martin. A strange love triangle is formed between the conductor, Laura and Rachel, the wife of the conductor, which ends up accepting the situation to not lose the husband. 

==Cast==
* Anselmo Vasconcellos as The conductor
* Claudia Alencar as Raquel
* Ana Paula Serpa as Laura Martin
* Carlo Mossy as Dr. Guilherme
* José Fernando Muniz as Jonas
* Luciano Szafir as Giulliano

==References==
 

==External links==
*  
*  

 
 
 
 