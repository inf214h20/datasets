The Road (2011 film)
{{Infobox film
| name           = The Road
| image          = The_Road_film_poster.jpg
| caption        = U.S. Theatrical movie poster
| director       = Yam Laranas
| producer       =  
| writer         = Yam Laranas
| starring       =  
| music          = Johan Soderqvist
| cinematography = Yam Laranas
| screenplay     =  
| editing        =  
| studio         = GMA Films
| distributor    =  
| released       =  
| runtime        = 115 minutes
| country        = Philippines
| language       =  
| budget         =
| gross          = PHP 3.9   
}}
The Road (stylized as The Яoad) is a 2011 Filipino psychological horror film directed by Yam Laranas, and starring Carmina Villaroel, Marvin Agustin, Rhian Ramos, Barbie Forteza and TJ Trinidad.  

The film was released in the Philippines by GMA Films on November 30, 2011 to positive reviews and moderate success. It was commercially released in the United States by Freestyle Releasing on May 11, 2012.

==Plot==

===Prologue===
Luis (TJ Trinidad) is a successful policeman who has been recently awarded for his skill in investigations, despite his track record of being reckless and failing to follow protocol. After his award ceremony, the chief of police is approached by a woman asking for new information about her daughters who went missing twelve years ago. Luis asks for the names of her daughters so he can try to find them, and the woman tells him that their names are Joy and Lara Luna. Asking his partner to retrieve the missing persons file, Luis sets out for his investigation, despite his colleague Gregs advice that he shouldnt give the woman false hope.

===Story 1: 2008===
Ella (Barbie Forteza) sneaks out at night with her cousin Janine (Lexi Fernandez) and Janines boyfriend Brian (Derrick Monasterio), taking her moms car so that they can find somewhere for Brian to teach Janine how to drive for her drivers test. Ella and Brian are on bad terms because of Brians alleged cheating on her cousin, however, and tensions are high between the two. On the highway they spot a patrol car, which causes them to panic as they dont have drivers licenses. Brian tries to make a U-turn, but accidentally drives into a fence blocking a dirt road. While checking for damage to the car, Brian notices a gate and drives into the isolated road beyond, thinking it the perfect spot for them to learn to drive. Eventually the three notice a red car which keeps on passing them although they never pass it themselves, and that the car doesnt seem to have a driver. Panicking, Brian tries to get them off the road but discover that they are stuck in an endless loop, passing by the same tree again and again. Finally, the car stalls and Brian and Janine gets out of the car while Ella stays inside. Janine tries to convince Brian to go back for her cousin but Brian refuses to do so and instead promises Janine that they will go back for her with help. As the two walk through the woods, they spot a crashed red car. Meanwhile, finally panicking, Ella leaves the car and manages to catch up with the two. Ella manages to call her father and beg for help, but the red car suddenly bursts into flames, causing the three to leave the crash site. While running, the two girls are separated from Brian. They manage to get back into their car and attempt to drive away, but a ghost behind the wheel scares Janine. Ella, in a panicked state of mind, accidentally opens her car door and falls out of the car while Janine loses control and hits her head on the wheel. As Ella opens her eyes, she screams upon seeing the ghost of a burned woman looking down at her.

A few hours later, Ellas father, Luis and some officers, appear at the same road. They find Brian dead by the corn fields and it is revealed that Janine died when she hit her head on the wheel but they are unable to find Ella. Luis and Officer Allan find a house deep within the woods. Officer Greg tells them that he already searched the house and tells them that Ella isnt there. They also find the crashed car that the three teens passed by earlier. When Luis opens the backseat door, he sees a skeleton wearing a heart shaped locket.

The next day, they take the three bodies to the morgue. It is revealed that the skeleton corpse that they found is Lara Luna, whom her mother identified through her locket.

===Story 2: 1998===
Lara (Rhian Ramos) is driving with her younger sister Joy (Louise delos Reyes) on the same dirt road. As they drive, they pass by a teenage boy walking along the road (Alden Richards) and a few feet away from him, their car overheats. They get out and then ask him where they can get water and the boy tells them that hell give them water by his house, which is the same house Luis and the others found years later. As they wait for him outside his house, Lara senses that something is wrong and tells Joy that they should leave. As they try to, the boy comes out and hits them with a wooden stick.

Lara wakes up a few hours later, with her foot chained to a bed post. She hears Joys screams for help and begs him to let her and her sister go. The boy takes Joy down to the basement and proceeds to savagely beat her. A few minutes later, he returns Joy to her room beside her sisters. Lara apologizes to Joy that she couldnt protect her but Joy tells her that its not her fault. As the boy comes to take Lara, she puts up a fight and scratches his neck. For now, the boy leaves her alone. Lara notices that he dropped the keys to her chains and proceeds to take it. She tries to search for Joy but notices that shes no longer there. She manages to sneak out of the house and falls into a pit which has corpses dumped in it. She sees that one is Joy. As she cries, she apologizes once again and finally leaves. The boy, meanwhile, has taken their car and proceeds to chase Lara with it. Eventually, he gently runs her over. As Lara tries to crawl away, The boy knocks her out and wraps a plastic bag around her head and ties a belt around her neck and her hands. As he drives back to his house, Lara suddenly awakens, causing the boy to crash. He leaves the car and watches it and Lara burn.

Back to the present day, Luis takes the officers back to the same house to look for Ella. As he and Officer Allan scope around, they find a picture of the old owners of the house. As Officer Allan goes to search someplace else, Luis finds Ella, who wasnt noticed by Officer Greg. She keeps yelling at him to let us go home and finally passes out as Luis holds her in his arms. Luis takes her to the rendezvous point and takes her to the ambulance. As Ellas father tries to talk to her, she says that their car stalled due to overheating and tells him that they asked a teenage boy for water, revealing that Ella is possessed by Lara. Luis and Officer Allan decide to return to the house.

===Story 3: 1988===
A young boy (Renz Valerio) lives with his family in the house deep in the woods. As he looks out of his window, he notices a girl named Martha (Ynna Asistio) looking for her mother.  The girl was there to see if the family needed their laundry doing. It is revealed that the boy isnt allowed to come out of the house and his abusive mother Carmela (Carmina Villaroel) tells Martha to come back tomorrow morning. Mistaking her sons conversation with Martha as an attempt to get out, Carmela locks him in a cabinet. During the evening, the boys father (Marvin Agustin), who is a preacher, comes home and lets him out of the cabinet. The reason behind his isolation is because his parents believe that the outside world is full of sin. The reason behind Carmelas bitterness is because the family is struggling financially and her husband refuses to ask money from his congregation. The next day, the boy spots Martha doing their laundry. As he comes down and watches her by the door, she invites him to come out to play but he tells her that hes not allowed to go outside. Wanting him to experience some fun, Martha goes inside with a bucket of soap and water to blow bubbles. Carmela goes downstairs and is enraged at the sight. As Martha tries to stop Carmela from hurting the boy, Carmela accidentally knocks her into the door post and dies. As the boy tries to wipe the blood away, he faints. Later that night, the boy wakes up and sees his mother hugging another man. As he goes down, his father tells him to wait upstairs so he and his mother can talk. It is revealed then that Carmela plans to leave her husband and the boy for the man. The boys father tries to convince her not to do it, but she is stubborn and decides to go through with it. As the boy listens, he passed out by his bed. The next morning his mother woke him up to have breakfast. When he came downstairs, his father tells him that Carmela left them and his father starts to throw up due to drunkenness. The boy adamantly insists that his mother is still there and in a fit of rage, his father shows him Carmelas corpse, revealing that his father killed Carmela. Frightened, the boy locks himself in the cabinet where he sees Marthas corpse. His father tries to get him out of the cabinet but when he sees Marthas corpse, he is shocked and has a breakdown. The next morning, the boy comes out of the cabinet and when he enters his parents room, he sees that his father has hanged himself. It is revealed that the boy didnt go out of the house until he became a teenager and that the boy was the one who killed Joy and Lara Luna.

===Epilogue===
Back to the present, Luis and Officer Allan arrive at the house and when Allan finds a room bound with a new padlock, Luis orders him to beat the door down. When he does so, he sees that the room is a bed room. As they look around, Officer Allan notices Luis medal. Luis knocks him out, wraps his head with a sheet and seals it with a belt around his neck, revealing to the audience that the boys in the second and third parts of the movie was none other than Luis himself. He walks back to the rendezvous point as other cops search the house. As he walks, he keeps on muttering Im sorry, Mama. It wont happen again. Back at the rendezvous point, Ellas father asks her who was the boy and Ella, still possessed by Lara, points at Luis. As the Chief of Police calls out to him, Luis shoots the chief two times in the chest. He gets in his car and drives away but his car stalls. As he tries to get out, the ghosts of Joy and Lara are with him in his car. He reaches for his gun and it is shown that Lara is forcing his hand to point his gun to his chin. As he mutters Mama, Im sorry, Lara forces him to shoot himself. A few minutes later, the other cops find Luis in his car, dead.

The film ends with Luis as a young boy, finally able to go outside of his house and be free from his mother.

== Cast ==
*TJ Trinidad as Luis 

*Carmina Villaroel as Carmela 

*Marvin Agustin as Luis Father 

*Rhian Ramos as Lara 

*Barbie Forteza as Ella 

*Alden Richards as Teenage Luis 

*Lexi Fernandez as Janine  

*Louise delos Reyes as Joy 

*Derrick Monasterio as Brian 

*Ynna Asistio as Martha

*Renz Valerio as Boy Luis

*Jacklyn Jose as Sisters Mom

*John Regala as Chief
 
*Allan Paule as Greg

*Lloyd Samartino as Ellas Father

*Gerald Madrid as Allan

*Ana Abad Santos as Janines Mother

*Dex Quindoza as Mailman

== Release ==
According to its director/writer Yam Laranas, the film took five years to make, and is also intended to be released internationally.   According to a tweet by film producer Joey Abacan on January 23, 2012, the film is to be released in U.S. and Canada cinemas within the year. It is scheduled to have a commercial U.S. and Canada theatrical release on May 11, 2012 with English subtitles in over 25 markets/theaters.     It will also be showing again to some selected digital theaters in the Philippines in May 9, 2012.

===International Release===
These are the dates and country wherein The Road has been released:

{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 100%; width=100%;"
|-  style="background:#ccc; text-align:center;"
| Date || Year || Country || Notes || Reference 
|-
| November 30 || 2011 || Philippines || Released Nationwide || 
|-
| April 8 || rowspan="4"| 2012 || Belgium || competed in  30th Brussels International Fantastic Film Festival  ||       
|-
| May 9 || Philippines  (Re-Release)  || Selected Digital Theaters only  ||
|-
| May 11 || North America || Limited Theaters in North America || 
|-
| May 24 || Singapore || Released Nationwide within Singapore ||      
|}

(notes: International Release is different from International Screening)

===Home Media Release===
According to The Roads director, Yam Laranas, The Road will be released onto DVD and Blu-ray formats on Tuesday, July 10, 2012 across stores in the U.S. such as Walmart as well as outlets such as Netflix, Redbox, Amazon and iTunes.

==Reception==

===Box office===
As of May 13, 2012, the film opened with an estimated $61,200 in North America, ranking 27th in the box office, along with $849,565 in the Philippines, for a worldwide total of $910,765. The film has grossed $5,455 for the reshowing in the Philippines. 

===Ratings===
MTRCB rated this film R-13 while the Cinema Evaluation Board gave the film a review of Grade B. The film  was released on November 30, 2011.  
 Rated R due to language, violence, gruesome and disturbing images, and intense scenes of terror. 

While Singapore rated this film NC-16 (not for children under 16 years old.) 

===Critical response===
{{Album ratings
| rev1 = Click The City
| rev1Score =     
}}

The Road has received positive feedback from film critics, with a current "fresh" percentage of 71% and an average rating of 6.2/10 on the review aggregator website Rotten Tomatoes, based on 18 reviews.    Reviewers have praised the atmosphere and the cinematography of the film, but panned its storyline and editing.

Jeannette Catsoulis of The New York Times wrote, "A powerfully atmospheric blend of ghostly encounters, horrific situations and missing-persons mysteries from the Philippine director Yam Laranas."  Robert Abele of the Los Angeles Times remarked, "The films three-pronged narrative does a fair job of laying a spooky groundwork for the revelatory emotional sadism that lies behind most acts of evil; it just takes a bit of clunky exposition to get there."  V.A. Musetto of the New York Post wrote, "(Laranas) delivers a maximum of suspense and horror, working wonders with a small budget."  Sam Adams of The A.V. Club gave a positive review, saying "The Road spends most of its time going in circles, working and reworking a small set of potent images. There’s a fine line between simple and crude, and The Road weaves back and forth over it like a rattletrap with a sudden flat, but Laranas makes a virtue of his limited means, steering clear of distraction and into the heart of darkness." 

A mixed review came from Ed Gonzales of Slant Magazine, who gave the film 2 stars out of four, saying, "For a spell, the film gets by on its unpretentious flair for atmosphere, even its disconcerting nonsensicality." 

Philbert Ortiz Dy of Click The City gave the film 3.5 out of 5 stars. he stated, "But The Road, like many horror films today, is insistent on providing some sort of twist, regardless of how little sense it actually makes. The whole package is still pretty good, but it’s difficult to ignore that particular misstep."  ...&nbsp;That aside, this is a pretty decent horror experience. The film runs a little long, but part of that is due to the film’s patience. The film isn’t just trying to shock people. It builds atmosphere and constructs horrific situations that are meant to genuinely disturb. It isn’t just about ghosts popping out of nowhere. It’s more about the creeping realization that there are some things in this world that aren’t quite right, and people are going to get hurt because of it. Laranas’ meticulously composed frames make great use of the interplay between light and shadow, setting up an eerie atmosphere that permeates every second of the film. . 

===Selected digital theaters===
According to SM Cinemas and  ,  These are the cinemas in the Philippines where The Road starts re-showing on May 9, 2012:
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 100%; width=100%;"
|-  style="background:#ccc; text-align:center;"
| Mall || Cinema # || Location || Notes
|-
| SM Megamall || 9 || Mandaluyong City || rowspan="4"| Closed
|-
| SM City Manila || 8 || Manila 
|-
| SM City Fairview || 5 || rowspan="2"| Quezon City 
|-
| SM North EDSA || 10 
|}
As of May 17, 2012, SM Cinemas officially closed The Road in re-showing.

==Accolades==
 
 
{| class="wikitable" style="width:100%;"
|+List of awards and nominations
|-
! style="width:20%;"| Award
! style="width:10%;"| Date of Ceremony
! style="width:25%;"| Category
! style="width:20%;"| Nominee
! style="width:8%;"| Result
! style="width:5%;"| Ref
|-
|  rowspan="4"| PMPC Star Awards for Movies
| rowspan="4"| 14 March 2012
| colspan="2"| Movie of the Year
| align="center"  
| rowspan="4" style="text-align:center;"|       
|-
| Movie Director of the Year
| Yam Laranas
|  
|-
| New Movie Actor of the Year Alden Richards
|  
|-
| Movie Cinematographer of the Year
| Yam Laranas
|  
|-
| rowspan="7"| Golden Screen Awards
| rowspan="7"| 24 March 2012
| Breakthrough Performance by an Actress
| Lexi Fernandez
|  
| rowspan="7" style="text-align:center;"|       
|-
| rowspan="2"| Breakthrough Performance by an Actor
| Alden Richards
|  
|-
| Derrick Monasterio
|  
|-
| Best Cinematographer
| Yam Laranas
|  
|-
| Best Sound
| Albert Michael Idioma
|  
|-
| Best Musical Score
| Johan Soderqvist
|  
|-
| Best Visual Effects
| Underground Logic
|  
|- FAMAS Awards
| rowspan="16"| 24 September 2012 Best Picture
|   
| rowspan="16" style="text-align:center;"|       
|- Best Director
| Yam Laranas
|  
|- Best Actor
| TJ Trinidad
|  
|- Best Actress
| Rhian Ramos
|  
|- Best Supporting Actor
| Marvin Agustin
|  
|- Best Supporting Actress
| Carmina Villaroel
|  
|-
| Best Child Actor
| Renz Valerio
|  
|-
| Best Screenplay
| Yam Laranas, Aloy Adlawan
|  
|-
|  Best Story
| rowspan=2 | Yam Laranas
|  
|-
| Best Cinematography
|  
|-
| Best Art Direction
| Melchor Defensor
|  
|-
| Best Sound
| Alex Tomboc, Lamberto Casas Jr., Addiss Tabong
|  
|-
| Best Editing
|  Mae Carzon, Yam Laranas
|  
|-
| Best Special Effects
| Jessie Abiva, Carlo Abello, Ryan Abugan
|  
|-
| Best Visual Effects
| Nathaniel Robite, Joseph Ramos, Ryan Jose Ticsay
|  
|-
| Best Musical Score
| Johan Söderqvist
|  
|}
 

== See also ==
* Sigaw The Echo Patient X

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 