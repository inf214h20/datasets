Shampoo (film)
{{Infobox film
| name           = Shampoo
| image          = Shampooposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Hal Ashby
| producer       = Warren Beatty
| writer         = Robert Towne Warren Beatty
| starring       = Warren Beatty Julie Christie Goldie Hawn Lee Grant Jack Warden Tony Bill
| music          = Paul Simon
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Robert C. Jones
| studio         = Rubeeker Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 minutes  
| country        = United States
| language       = English
| budget         = $9 million  
| gross          = $49,407,734 
}} satirical romance romantic comedy-drama film written by Robert Towne and directed by Hal Ashby. It stars Warren Beatty, Julie Christie and Goldie Hawn, with Lee Grant, Jack Warden, Tony Bill and in an early film appearance, Carrie Fisher.
 Election Day 1968, the day Richard Nixon was first elected as President of the United States, and was released soon after the Watergate scandal had reached its conclusion.  The political atmosphere provides a source of dramatic irony, since the audience, but not the characters, are aware of the direction the Nixon presidency would eventually take.  However, the main theme of the film is not presidential politics but sexual politics; it is renowned for its sharp satire of late-1960s sexual and social mores.

The lead character, George Roundy, is reportedly based on several actual hairdressers, including  , the screenwriter Towne based the character on Beverly Hills hairdresser Gene Shacove.

==Plot==
Shampoo is set during a 24-hour period in 1968, on the eve of a presidential election that would result in Richard Nixons election to the American presidency. George Roundy is a successful Beverly Hills hairdresser, whose occupation and charisma have provided him the perfect platform from which to meet, and bed, beautiful women, including his current girlfriend Jill.

Despite this, George is dissatisfied with his professional life; he is clearly the creative star of the salon, but is forced to play second fiddle to the "nickel-and-diming," mediocre hairdresser who owns the place. He dreams of setting up his own salon business, but lacking the cash to do so, turns to wealthy lover Felicia and her unsuspecting husband Lester to bankroll him. Georges meeting with Lester supplies a second secret for him to keep from his would-be benefactor: Lesters current mistress, Jackie, is Georges former girlfriend, perhaps the most serious relationship he has ever had.

Lester, who assumes George is gay, invites him to escort Jackie to a Republican Party election night soiree, at which George finds himself in the same room as a number of present and former sexual partners. The principals adjourn to a posh counterculture party, and the night quickly descends into drugs, alcohol and sexual indulgence. In the films dramatic climax, Lester and Jill happen upon George and Jackie having vigorous sex on a kitchen floor. Just before their identities are revealed, an impressed Lester exclaims: "Now, thats what I call fucking!" When Jill recognizes the writhing couple, she throws a chair at them; as George backpedals, trying to placate Jill, Jackie sees him for the cad he is, and flees.

George realizes that Jackie is his true love and proposes to her. By then it is too late: Jackie announces that Lester is divorcing Felicia and taking Jackie to Acapulco. With Felicia gone, Jill gone, and now Jackie gone, the film thus pairs sexual revelation with Georges deeper moral development, but ends bleakly for the protagonist, despite his epiphany.

==Cast==
* Warren Beatty as George Roundy
* Julie Christie as Jackie Shawn
* Goldie Hawn as Jill
* Lee Grant as Felicia Karpf
* Jack Warden as Lester Karpf
* Tony Bill as Johnny Pope
* Jay Robinson as Norman
* George Furth as Mr. Pettis
* Randy Scheer as Dennis
* Susanna Moore as Gloria
* Carrie Fisher as Lorna Karpf
* Luana Anders as Devra
* Mike Olton as Ricci
* Richard E. Kalk as Detective Younger
* Brad Dexter as Senator East
* William Castle as Sid Roth

==Reception==
 

Upon its release, the film generally received positive reviews from critics who lauded its talented cast and sharp, satirical writing. Praise was not universal; some critics, including Roger Ebert, pronounced it a disappointment.  
From reviews compiled retrospectively, review aggregation website Rotten Tomatoes gives the film a score of 63% based on 30 reviews. 

  One Flew Over the Cuckoos Nest, and The Rocky Horror Picture Show.

The year after its release saw a blaxploitation send-up, Black Shampoo.

==Awards==
   Academy Awards
-Wins    Best Actress in a Supporting Role: Lee Grant
-Nominations Best Actor in a Supporting Role: Jack Warden Best Writing, Original Screenplay: Robert Towne and Warren Beatty George Gaines

;American Film Institute recognition
* 2000: AFIs 100 Years...100 Laughs – #47
  Golden Globes
-Nominations Best Motion Picture (Musical or Comedy) Best Motion Picture Actor (Musical or Comedy) – Warren Beatty Best Motion Picture Actress (Musical or Comedy) – Julie Christie & Goldie Hawn Best Supporting Actress (Motion Picture) – Lee Grant

;Other awards
* 1975 National Society of Film Critics Award for Best Screenplay
* 1976 Writers Guild of America Award#1970s|Writers Guild of America Award – Best Comedy Written Directly for the Screen
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 