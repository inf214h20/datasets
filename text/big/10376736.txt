Carnival in Flanders (film)
{{Infobox film
| name           = Carnival in Flanders
| image          = LaKermesseHéroique.jpg
| caption        = Poster
| director       = Jacques Feyder
| producer       = Pierre Guerlais
| writer         = Jacques Feyder Charles Spaak (story) Robert A. Stemmle Bernard Zimmer (dialogue)
| starring       = Françoise Rosay Jean Murat André Alerme
| music          = Louis Beydts
| cinematography = Harry Stradling Sr.
| editing        = Jacques Brillouin
| distributor    = Films Sonores Tobis
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Carnival in Flanders is a 1935 French historical romantic comedy film directed by Jacques Feyder. Its original French title is La Kermesse héroïque and it is widely known under that name. A German-language version of the film was made simultaneously and was released under the title Die klugen Frauen.

==Plot== Spanish occupation, the town of Boom, Belgium|Boom, in the midst of preparations for its carnival, learns that a Spanish duke with his army is on the way to spend the night there. 

Fearing that this will inevitably result in rape and pillage, the mayor &mdash; supported by his town council &mdash; has the idea of pretending to be newly dead, in order to avoid receiving the soldiers.  But his redoubtable wife Cornelia despises this strategem and organises the other women to prepare hospitality and to adapt their carnival entertainments for the Spaniards (who insist on entering the town anyway). 

Such is the warmth of the women’s welcome that not only do the Spaniards refrain from misbehaviour, but on their departure the Duke announces a year’s remission of taxes for the town.  

Cornelia allows her husband to take the credit for their good fortune, but she has in the meantime thwarted his plans for their daughter to marry the town butcher instead of the young painter Brueghel whom she loves.

==Cast==
*Françoise Rosay as Cornelia de Witte, Madame la Bourgmestre/Madame Burgomaster
*André Alerme as Korbus de Witte, le bourgmestre/The Burgomaster (as Alerme)
*Jean Murat as Le duc dOlivarès/The Duke
*Louis Jouvet as Le chapelain/The Priest
*Lyne Clevers as La poissonnière/The Fish-Wife (as Lynne Clevers)
*Micheline Cheirel as Siska
*Maryse Wendling as La boulangère/The Bakers Wife
*Ginette Gaubert as Laubergiste/The Inn-Keepers Wife
*Marguerite Ducouret as La femme du brasseur/The Brewers Wife
*Bernard Lancret as Julien Breughel, a young painter
*Alfred Adam as Josef Van Meulen, le boucher
*Pierre Labry as Laubergiste/The Inn-Keeper
*Arthur Devère as Le poissonnier/The Fishmonger (as Arthur Devere) Marcel Carpentier as Le boulanger/The Baker
*Alexander DArcy as Le capitaine/The Captain (as Alexandre Darcy)
*Claude Sainval as Le lieutenant/The Lieutenant (as Claude Saint Val) Delphin as Le nain/The Dwarf

==Background and production== Spanish occupation. Pieter de Hoogh &mdash; and an elaborate creation of a Flemish town was undertaken (in suburban Paris) by the designer Lazare Meerson.  Sumptuous costumes were provided by Georges K. Benda.  The strong cast included Feyder’s wife Françoise Rosay and Louis Jouvet.

The film was produced by the French subsidiary of the German firm Tobis, and it was made in two versions, French and German, with alternative casts (apart from Françoise Rosay who appeared in both).

==Reception==
On the strength of its richly detailed tableaux and the confident manner in which Feyder animated his historical farce, the film enjoyed considerable success in France and elsewhere in the world. The film historian Raymond Chirat pointed to the combination of the admirable sets, the splendid costumes, the biting irony of the story, and the quality of the acting which earned the film a cascade of awards, the admiration of the critics, and the support of the public.   Georges Sadoul referred to “this important work, of exceptional beauty”.   Feyder won the Best Director Award at the 4th Venice International Film Festival in 1936. 

However, even on its first appearance in 1935 this tale of occupation and cheerful collaboration also caused uneasiness, and the screenwriter Henri Jeanson deplored the “Nazi inspiration” of the film. It was indeed enthusiastically praised in Germany, and its première in Berlin (15 January 1936) took place in the presence of Joseph Goebbels. (Yet, a few days after the outbreak of war in 1939, the film was banned in Germany and the occupied countries of Europe, and Jacques Feyder and Françoise Rosay subsequently sought refuge in Switzerland.) 

It was in Belgium that the film caused greatest controversy, perhaps for the unflattering portrayal of Flemish leaders in the 17th century, or in suspicion of covert references to the German occupation of Belgian territory during the First World War.  At any rate, the release of the film led to brawls in cinemas in Antwerp, Ghent, and Bruges. 

Even two decades later (1955), its enduring reputation irked François Truffaut who wrote, in a broadside against so-called ‘successful’ films: “In this regard, the most hateful film is unarguably La Kermesse héroïque because everything in it is incomplete, its boldness is attenuated; it is reasonable, measured, its doors are half-open, the paths are sketched and only sketched; everything in it is pleasant and perfect.” 

Nevertheless, this remains probably the most popular and widely known of Jacques Feyder’s films.

==Awards==
* 1936 Grand Prix du cinéma français
* 1936 4th Venice International Film Festival : Jacques Feyder, best director
* 1936 National Board of Review, USA : best foreign film
* 1937 New York Film Critics Circle Awards : best foreign film
* 1938 Kinema Junpo Awards : best foreign film

==Influences== Carnival in Flanders, which was produced in 1953.

==Further reading==
* LAvant-scène: cinéma, 26: La Kermesse héroïque. (Paris: Avant-Scène, 1963).  .

==References==
 

==External links==
*  
*  
*  
*  
*   at filmsdefrance

 

 
 
 
 
 
 
 
 
 
 
 