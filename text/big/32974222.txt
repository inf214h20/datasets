Cinesound Varieties
 
 
{{Infobox film
| name           = Cinesound Varieties
| image          = Cinesound_Varieties.jpg
| image_size     = 
| caption        = Still from the film
| director       = Ken G. Hall
| producer       = Ken G. Hall Vic Roberts George D. Parker
| based on      = 
| narrator       = 
| starring       = Fred Bluett
| music          = 
| cinematography = Frank Hurley
| editing        = 
| studio         = Cinesound Productions
| released       = May 1934
| runtime        = 60 mins
| country        = Australia
| language       = English
| budget         = £2,500 
| gross = £2,000 "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19-20 
}}
Cinesound Varieties is a 1934 Australian variety short film from director Ken G. Hall made to go out on a double-bill with the full-length feature, The Silence of Dean Maitland (1934). Only 18 minutes of the film survive today. 

==Synopsis==
There were two main components of the film:

1) Evolution of a Waltz - a musical presentation with Hamilton Webber and the State Orchestra illustrating the evolution of the waltz from the age of Mozart to Irving Berlin

2) Nautical Nonsense - a musical comedy revue, featuring several Australian variety stars including
*Fred Bluett and his Boy Scouts story as pirates in Sydney
*the Tom Katz saxophone band
*soprano Angela Parselles 
*tap dancing by the Lowell brothers
*musical numbers by the Cinesound Octette
*the Cinesound Beauty Ballet of twenty Australian girls.
 
There were also appearances by Emanel Aarons at the grand organ and an adagio dance by the Orlandos. 

==Cast==
*Fred Bluett
*Hamilton Webber

==Production== Charles Chauvel was using Cinesounds studio for a film. There were also a number of scenes shot on Sydney harbour.

"It was written in a hurry and it was a bad effort," said Hall later. "Im not proud of it." 

==Reception==
===Critical===
Contemporary reviews were poor. The Sydney Morning Herald wrote that:
 Pace is the essence of a variety show, and pace is what Cinesound Varieties definitely lacks... The text which holds everything together is painfully weak, and the humour deplorable. These are two points in which every Australian film so far, except On Our Selection has come desperately to grief; and, even in On Our Selection, the actors and the photographs had to triumph over unfavourable material. Talk of winning success in oversea markets will remain so much beating of empty air as long as producers continue to give text and narrative value last place in their attention, instead of putting these matters first. Sooner or later, the literary side of things must come into its own.  
"It is difficult to believe that Cinesound Varieties comes from the same studio as The Silence of Dean Maitland - if it did," said the reviewer from The Argus. "In it all those things that should not be done are done and all that should be done are left undone." 

"A sadly overdressed musical revue which has inherited all the evils the talkies were ever heir to, except the American slang," said The Advertiser. 
===Box Office===
According to a contemporary trade report it is likely the film lost an estimated £1,200. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Australian Screen Online

 

 
 
 