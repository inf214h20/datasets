Endrendrum Kadhal
 
{{Infobox film
| name = Endrendrum Kadhal
| image =  | Vijay Rambha Rambha M. N. Nambiar Banupriya Raghuvaran|
| director = Manoj Bhatnagar|
| producer = K. T. Kunjumon  Manoj Bhatnagar|
| story = Manoj Bhatnagar|
| screenplay = Manoj Bhatnagar|
| cinematography = K. S. Shiva|
| editing = B.S.Vasu – Saleem
| studio = Sameera Films
| distributor = Gentleman Film International
| writer = Crazy Mohan  (dialogue) 
| released = 5 March 1999 |
| runtime = 160 min. |
| country =  | Tamil |
| music = Manoj Bhatnagar|
| budget = 
}}
 Tamil film Vijay and Rambha in the lead roles with Raghuvaran and Banupriya in other pivotal roles.  Veteran actor M. N. Nambiar also played a supporting role, while Manoj Bhatnagar also composed the films music. The film opened to average reviews but was an average success.

==Plot==
Vijay is the managing director of a large shipping corporation. He lives in a joint family with his father (Nambiar), two brothers (Radharavi and Dhamu), their wives and children and a spinster sister (Banupriya). He goes to Switzerland to stipulate a contract with Nizhalgal Ravi and Raghuvaran. There he meets Rambha, Ravis sister. The two fall in love. Ravi approves of Vijay but when he expresses the condition that Vijay should stay with them after the marriage because India is not good, he refuses his offer and returns home. But later Raghuvaran and Rambha travel to India to mend fences. Raghuvaran learns that Banupriya had been dumped by Ravi for a better life and Vijay is just waiting for an opportunity to kill the man who destroyed his sisters life. He devises ways of preventing Ravi from attending the marriage but later he tells Banupriya the truth. Vijay overhears this and is angered, but Raghuvaran solves all the problems by deciding to marry Banupriya. Banupriya agrees for this too, making everyone happy. Finally Vijay, seeing his sister happy leaves Ravi alone. Eventually Rambha and Vijay get married.

==Cast== Vijay as Vijay Rambha as Meenu (Meenakshi)
*M. N. Nambiar as Sethupathi
*Banupriya as Pooja
*Raghuvaran as Shekher
*Radha Ravi as Krishna
*Nizhalgal Ravi as Nagaraj
*Charle as Sabapathi
*Dhamu as Vasu
*Visali Kannadasan as a television interviewer
*S. A. Rajkumar as a singer

==Production== Meena was initially approached for the film, but had to turn the offer down due to her busy schedule. 

==Release==
This low-budget production opened to mixed reviews, with a critic from Indolink.com claiming "even though the film has absolutely nothing new to offer, it doesn’t test your patience."  The Deccan Herald labelled that "all in all it is a pleasant film"  and that "Vijay’s a little glib, Rambha’s weight loss makes her look haggard and Raghuvaran as not a villain is nice", claiming it was "worth a look". 

The co-producer K. T. Kunjumon, who was still reeling under the failure of Ratchagan, faced further financial crunch owing to the failure of this film, with L. Suresh, a financier in Chennai publicly threatening legal action against the producer for failure to pay back a loan of  75 lakh taken out for the film. 

==References==
 

 
 
 