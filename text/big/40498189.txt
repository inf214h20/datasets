Flower and Snake (2004 film)
{{Infobox film
| name           = Flower and Snake 
| image          =
| caption        =
| director       = Takashi Ishii
| producer       = Kazuo Shimizu
| eproducer      =
| aproducer      =
| writer         = Takashi Ishii (screenplay)
| starring       = Aya Sugimoto
| music          = Gorō Yasukawa
| cinematography =
| editing        = Yūji Murayama Toei
| released       = March 13, 2004 (Japan)
| runtime        = 115 minutes
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}

 , released in 2004, is a Japanese film based on the 1974 movie Flower and Snake directed by Masaru Konuma and starring Naomi Tani. The earlier film, based on a novel by Oniroku Dan, was part of Nikkatsus Roman Porno series. The 2004 version, directed by Takashi Ishii and starring Aya Sugimoto, has been described as marking a "watershed moment in the history of Japanese film censorship" with "some of the most extravagant scenes of sexual cruelty and graphic nudity to be passed off as mainstream entertainment in any part of the world."   

==Plot==
On the surface, Shizuko (Aya Sugimoto) is a beautiful and talented tango dancer married to a handsome and successful businessman Takayoshi Tōyama, but she is troubled by recurrent masochistic dreams and her inability to be sexually aroused by her husband. But her husband is heavily indebted to gangsters and yakuza boss Kanzō Morita also has a video supplied by Kawada, a disgruntled former employee, which implicates Tōyama in a bribery scheme. Morita tells Tōyama that his only recourse is his beautiful wife who is an obsession to his mentor, the politically powerful Ippei Tashiro. When Tōyama finds that Tashiro is 95 years old, he convinces himself that turning his wife over to him will not be a major problem. When he brings his wife to the supposed masked ball, however, she is kidnapped and made part of a private bondage show for the elderly yakuza chief and his twisted friends. Shizuko resists at first but submits when her female bodyguard Kyōko (who has also been kidnapped) is submitted to sexual torture and threatened with death. Shizuko is then subjected to a series of punishments including abundant rope bondage. When her husband repents and finally reaches her after paying the yakuza, her only response is "Do me!". After more sexual adventures, she finally escapes, but was any of this reality or just one of her elaborate masochistic dreams?

==Cast==
* Aya Sugimoto as Shizuko Tōyama  
* Renji Ishibashi as Ippei Tashiro 
* Hironobu Nomura   as Takayoshi Tōyama
* Kenichi Endou as Kanzō Morita 
* Misaki   as Kyōko Nojima
* Yōzaburô Itō   as Clown man 
* Yoshiyuki Yamaguchi   as Ryō Eguchi
* Shun Nakayama as Kazuo Kawada
* Shigeo Kobayashi as Yoshizawa

==Production==
Masaru Konumas 1974 version of Flower and Snake was the Nikkatsu studios first venture into S&M oriented films.  The 2004 film is related only by name and general theme to the earlier film but it shares much of the spirit of the Roman Porno S&M features.  For the film, star Aya Sugimoto "worked closely with bondage master Go Arisue. Though Sugimoto spent roughly 75 percent of her screen time unclothed and participating in humiliating acts, she appeared every bit in control of her sexual composure." 

==Sequels== Toei on May 14, 2005. Aya Sugimoto continued in her role as Shizuko Tōyama but with a much older husband in this version. The locale is now Paris but the plot still centers on sado-masochism with Kenichi Endou as the artist Ikegami who leads Shizuko into his shadowy world.  Toei released a second sequel to the film on August 28, 2010 as   but this time starring Minako Komukai as Shizuko. This sequel was directed by Yusuke Narita and featured an appearance by Kei Mizutani. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 