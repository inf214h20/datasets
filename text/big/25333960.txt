Frontier of the Dawn
{{Infobox film
| name           = Frontier of the Dawn
| image          = La frontière de laube.jpg
| caption        = Original French poster
| director       = Philippe Garrel
| producer       = Edouard Weil
| writer         = Marc Cholodenko Arlette Langmann
| starring       = Louis Garrel Laura Smet Clémentine Poidatz
| music          = Jean-Claude Vannier
| cinematography = William Lubtchansky
| editing        = Yann Dedet
| distributor    = Les Films du Losange
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
Frontier of the Dawn ( ) is a 2008 French drama film directed by Philippe Garrel. The film stars Louis Garrel, Laura Smet, and Clémentine Poidatz and was photographed by cinematographer William Lubtchansky in black and white.  It premiered at the 2008 Cannes Film Festival in competition.

== Cast ==
* Louis Garrel as François 
* Laura Smet as Carole 
* Grégory Gadebois as Caroles friend
* Clémentine Poidatz as Ève  
* Olivier Massart as Le père dEve

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 