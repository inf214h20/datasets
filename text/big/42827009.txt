Beautiful Noise (film)
 
 
{{Infobox film
| name           = Beautiful Noise
| image          = Beautiful Noise poster.jpg
| alt            = A digital vector-drawn image of various effects pedals. Blue block text above reads "Beautiful Noise".
| caption        = Official film poster
| director       = Eric Green
| producer       = {{plainlist|
*Eric Green
*Sarah Ogletree
}}
| writer         = Eric Green
| starring       = {{plainlist|
*Kevin Shields
*Jim Reid
*Robin Guthrie
*Bobby Gillespie
*Douglas Hart
*Colm Ó Cíosóig
*Debbie Googe
*Simon Raymonde
}}
| music          = Brad Laner
| editing        = Sarah Ogletree
| studio         = HypFilms
| released       =  
| runtime        = 90&nbsp;minutes
| country        = United States
| language       = English
| budget         = $84,740   
}} rock bands—Cocteau My Bloody Valentine—and their influence on shoegazing and other alternative rock genres. Beautiful Noise features extracts from over 50&nbsp;interviews with bands and artists, as well as archival footage and music videos.

Green commenced production on Beautiful Noise in early 2005&nbsp;with producer and editor Sarah Ogletree; production was largely completed by 2008&nbsp;although the project stagnated due to various financial and legal issues. In response, Green began a successful crowdfunding campaign on Kickstarter in hopes of securing final financial investment for the films release. The campaign was supported by several of the bands featured in Beautiful Noise through social media.
 Washington on May 31, 2014.

==Overview== My Bloody Robert Smith of The Cure. 

In addition to interviews, Beautiful Noise includes never-before-seen footage, television appearances, music videos and tour projections of the films featured bands. The film also "highlights new bands influenced by  ",  such as A Place to Bury Strangers and M83 (band)|M83. 

==Production== punk   Hip-hop music|rap" but no similar documentaries about bands he was a "longtime fan" of. Pre-production of Beautiful Noise began in early 2005&nbsp;and by March, Green and Ogletree had commenced production on the film. Greens interviews with the films featured cast "spanned over several years". 

In 2007, an email was leaked to a journalist about Beautiful Noise. Several prominent publications, including The Guardian,  NPR and Pitchfork Media reported on the film and "inspired a wider of level of interest" from the public.  As a result, Green and Ogletree began a crowdfunding campaign on Kickstarter to raise finances to cover licensing of the music featured in the film, as well as clearances and distribution.  Green hoped to raise $75,000&nbsp; on Kickstarter and the campaign was supported by various artists raising awareness via social media. The project was successfully funded on December 16, 2012&nbsp;and a further $9,740&nbsp;was pledged after a total of 1,511&nbsp;users contributed. 
 Angus Cameron. Medicine founder Brad Laner also contributed a musical score to the film.  Following public awareness of the film after the 2007&nbsp;email leak, John Nugent and Timothy ODonnell contributed further artwork to the film, presented alongside Camerons additions. A final edit of Beautiful Noise was completed by Ogletree after Greens successful campaign on Kickstarter. 

==Release== Washington on May 31&nbsp;and the international premiere was held at the Sheffield Doc/Fest in Sheffield, England on June 8.  

==References==
 

===Bibliography===
* 

==External links==
* 
* 
* 
*  at Kickstarter
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 