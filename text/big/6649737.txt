The Toy That Saved Christmas
 
{{Infobox film
| name = The Toy That Saved Christmas
| image = veggietales_dvd_toy2.jpg
| caption = The 2002 VeggieTales Classics DVD.
| writer = Phil Vischer
| director = Phil Vischer Chris Olsen
| producer = Chris Olsen Big Idea, Inc. Word (with the new animation) Warner Home Video Lyrick Studios (with the old animation) Dan Anderson Ken Cavanagh Shelby Vischer Bridget Miller
| editing = Mike Nawrocki
| music = Kurt Heinecke Phil Vischer Mike Nawrocki Alan Moore Jason Moore
| released =  
| runtime = 33 minutes 26 seconds
| language = English
}}
The Toy That Saved Christmas is the sixth episode of the VeggieTales animated series and the first holiday special in that series. This video has been released in 1996 on VHS and distributed by Word. In 1998, Lyrick Studios has re-released the program on VHS with the old animation and the new animation. Unlike its predecessors it does not feature an opening/closing countertop.  Like the other holiday episodes, it also has no "A Lesson In…" subtitle.

== Plot == Jimmy and Jerry Gourd (who mistook it for a pizza) and the episode begins.
 Grandpa George Annie about Wally P. Nezzer (List of VeggieTales characters#Mr. Nezzer|Mr. Nezzer), the owner of a toy company, who is spreading the word via television commercial about his newest toyline, “List of VeggieTales characters#Buzz-Saw Louie|Buzz-Saw Louie”, the only toy featuring an actual working buzz saw built into his right arm and a trigger in his nose that makes him tell kids to get more toys. After seeing the commercial, the previously content kids of Dinkletown begin whining to their parents about wanting more toys. Mr. Nezzer explains to his assistant (List of VeggieTales characters#Mr. Lunt|Mr. Lunt) that this is a strategy to create demand for his toys, which will result in him making lots of money. As many Buzz-Saw Louie dolls roll off the production line, one of them inexplicibly comes to life. That night, Louie wonders if theres more to Christmas than materialistic greed and escapes the factory to find the true meaning of Christmas.
 Silly Song Scallion #1), The Peach). All the visitors are invited into Larrys house and get a cookie (except for the IRS agent). Santa (Bob) finally shows up, but rather than being jolly, he angrily chases after the bank robber and the Viking for stealing his belt and hat.
 Luke Chapter 2: verses 8-14. In this, George explains to the group that the true meaning of Christmas is not to get, but to give, the way God gave us Jesus, the greatest gift of all. The kids are heartened by the news, but puzzle about how to tell the rest of the Veggies before Christmas Day.
 Laura Carrot shows Mr. Nezzer kindness by giving him a gift and he quickly repents the errors of his ways. However, the sled with Bob, Larry, Junior, and Louie is accidentally sent on its fateful course. A chase soon ensues in which Mr. Nezzer and a half dozen of his penguin workers attempt to save the doomed Veggies. The tables turn, however, when Louie is able to divert the sled’s course, leaving Mr. Nezzer alone racing toward the ravine. Louie again springs into action and the penguins and Veggies are able to save Mr. Nezzer just as he is about to plummet to certain death. The story concludes with everybody in Dinkletown, including Mr. Nezzer, getting together to celebrate a Christmas party. Next-door to the party, Buzz-Saw Louie is hard at work in his new house, putting the buzz-saw for good use making furniture.
 the Rudolph special instead). Regardless, he closes out with his usual line, "God made you special and he loves you very much".

== Cast of Characters ==

===VeggieTales Christmas Spectacular===
* Mr. Nezzer as the announcer
* Bob the Tomato and Larry the Cucumber as the hosts
* Pa Grape as the director
* Junior Asparagus singing "While By My Sheep"
* Jimmy and Jerry Gourd as stage crew
* Archibald Asparagus singing "Ring, Little Bells"

=== Oh, Santa! ===
* Larry the Cucumber as himself Scallion #1 as a bank robber
* Pa Grape as a Viking The Peach as an IRS agent
* Bob the Tomato as Santa Claus

===The Toy that Saved Christmas=== Grandpa George as the narrator and mailman Annie as Georges granddaughter (debut)
* Bob the Tomato as himself
* Larry the Cucumber as himself
* Junior Asparagus as himself Dad Asparagus as himself Mom Asparagus as herself
* Mr. Nezzer as Wally P. Nezzer
* Mr. Lunt as Mr. Nezzers assistant Buzz Saw Louie as himself (debut) Laura Carrot as herself Lenny Carrot as himself Dad Carrot as himself Mom Carrot as herself (debut Cameo) Baby Lou as himself (debut Cameo) Dad Pea as himself (debut) Mom Pea as herself (debut Cameo) Percy Pea as himself (debut)
* List of VeggieTales characters#The Peas|Lil Pea as himself (debut)
* The Penguins as themselves

== Songs ==
Due to the format of the show, this episode does not contain the usual "VeggieTales Theme Song" and "What We Have Learned", although the "VeggieTales Theme" is still present. It also contains the following songs:

* Cant Believe Its Christmas, sung by Junior, Laura, Lenny, Percy, and Lil Pea
* Grumpy Kids, sung by Buzz-Saw Louie
* Oh, Santa! (Silly Song), sung by Larry, Bob, Scallion #1, and Pa Grape

===In the VeggieTales Christmas Spectacular special===

*While By My Sheep, Sung by Junior Asparagus (Lisa Vischer) Kling Glöckchen/Ring Little Bells! Archibald Asparagus (Phil Vischer) and Larry the Cucumber (Mike Nawrocki)

==External links==
* 
* 
* 
*   
*  

 

 
 
 
 
 