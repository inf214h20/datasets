God Loves Uganda
{{Infobox film
| name           = God Loves Uganda
| image          = 
| caption        =
| director       = Roger Ross Williams
| writer         =
| starring       = Lou Engle Jonathan Hall Rev. Kapya Kaoma Rev. Robert Kayanja Rev. Jo Anna Watson Jesse & Rachelle Digges Bishop Christopher Senyonjo Rev. Martin Ssempa Scott Lively
| producer       = Roger Ross Williams  Julie Goldman
| music          = Mark degli Antoni
| cinematography = Derek Wiesehahn 
| editing        = Richard Hankin (supervising editor) Benjamin Gray
| line producer  = Carolyn Hepburn
| associate producers  = Casper de Boer Paige Ruane Betsy Ford
| distributor    = Variance Films
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| gross          =
}}
  Uganda Anti-Homosexuality Act, which at one point raised the possibility of the death penalty for gays and lesbians. The filmmakers follow a group of young missionaries from the International House of Prayer in their first missionary effort in another nation, as well as interviewing several evangelical leaders from the US and Uganda.

Williams was inspired to make God Loves Uganda when he met David Kato, an LBGT activist who was killed in 2011, ostensibly in a robbery. Kato told there was an untold story of the damage American fundamentalist evangelicals are doing in Uganda; of the insidious nature of their aggressive effort to harvest young, unclaimed souls to preach a gospel of love intertwined with a gospel of intolerance.   

==Reception==
 Mike Bickle and other IHOP leaders. 
===Critical reception===
Joe Mirabella at the Huffington Post described it as the "most terrifying film of the year"; Tim Wu at Slate (magazine)|Slate and Bill Blezek at the Omaha World-Herald described the film as "disturbing."    On the other hand, John G. Stackhouse, Jr. of Christianity Today criticized the film for perceived "evangelophobia" and trading in "propaganda", likening the film to the 2006 film Jesus Camp. 

===Awards===
God Loves Uganda has won the following awards:
* Full Frame Inspiration Award at the Full Frame Documentary Film Festival
* Grand Jury Prize for Best Documentary at the Dallas International Film Festival
* Best Feature Length Documentary at the Ashland Independent Film Festival
* Audience Award Documentary at the Mountain Film Festival
* Best Overall Documentary at the DocuWest Film Festival
* Sheffield Youth Jury Award at the Sheffield Doc/Fest
* Grand Jury Pink Peach Feature at the Atlanta Film Festival
* Jury Award for Documentary Feature at the Tampa International Gay and Lesbian Film Festival

==See also==
* Call Me Kuchu

==References==
 

==External links==
*  
*  
*  
* 
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 