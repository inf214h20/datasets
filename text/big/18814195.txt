Secret Lives: Hidden Children and Their Rescuers During WWII
{{multiple issues|
 
 
 
}}
  2002 documentary directed by Aviva Slesin. {{cite web
  | title = IMDb | publisher = Aviva Slesin -Internet Movie Data base 
  | date = August 2008
  | url = http://www.imdb.com/name/nm0805754/
  | accessdate = August 11, 2008}}   It deals with the complicated associations involving two groups of guardians and the children who are grateful to each pair.

==Summary==
The term "Hidden Children" or "Hidden Children of the Holocaust" refers to children, mainly Jewish, who were "hidden" in some way to prevent them from being caught and most likely murdered by the Nazis. Many such children survived by being placed within another, usually Catholic, family, and then raised as-if a member of that family.  Many aspects of such a situation are discussed in the Wikipedia article Hidden Children, to which the reader is referred.

This documentary film presents many aspects of the situation of those Hidden Children who were raised as-if a member of another family. 

This film brings up the issue of kinship and allegiance, and raises concerns of continued existence. After non-Jewish households throughout the Holocaust rescued Jewish youngsters, these children had to resolve an important matter: "Who are my true family? my actual original Jewish parents who initially nurtured me, or the  parents who took me in and nurtured me later?"?

No individual among Europe’s general population could have expected the terror that the Nazi regime would inflict on Jewish children.  Many gentiles worked with the members of Hitler’s army and even more disregarded the carnage so that they could evade the watchful eye of the government.  However a small number of courageous persons endangered themselves to save Jewish lives.

The film looks at how the Holocaust shattered and formed two separate but overlapping family units; herself a “hidden child,” documentarian Aviva Slesin ventures to discover the end result of the encounter that others like her lived through.  Both sets of parents and the youth they had in common have an opportunity to voice their opinions. All three sides occupied a role during wartime conflict.  They are the: former hidden Jewish youngster(s), now adult, the Jewish parents who decided to sacrifice their offspring in the desire to protect them and the compassionate gentile households who ran the risk of being imprisoned or put to death by protecting Jewish children. Decades after World War II, they are free to reflect on this event in their lives and their emotions regarding one another, with astonishingly different responses. One  young girl was certain that her Jewish family was trying to discipline her by shipping her off to a different home.  This girl was ultimately unhappy with her saviours, but not all had experiences like her.  A second child, today a grown Jewish man is abashed to admit that the time he was separated from his biological mother, were the “best years” of his youth.

Surprisingly open, the individuals of Secret Lives tell honestly what this psychologically draining ordeal was like for everyone concerned.  One Jewish parent involved recalls reasoning that the gentile family could bring up her daughter in whichever manner they pleased, provided that her child  could live.

Nevertheless, the brave deed of sheltering a Jewish youth did have its opponents. A lady whose mother housed Jewish kids all through the war reveals that she felt ignored and disadvantaged because of her mom’s divided attention.  Even today this woman is still upset and stunned that her mother was prepared to sacrifice her own children so as to rescue those of complete strangers.  She goes on to say, “I have been angry most of my life.” Following years in concealment, shielding their true selves and at times their physical being, the conclusion of World War II led the hidden Jewish children to individual freedom. One man reminisces, teary-eyed, that when he found out the war had ended, he snatched a Dutch flag and sprinted up and down the road yelling, “I’m a Jew!”

However, for a majority of the children, the end of the war produced even more sorrow. Although most of the world’s population rejoiced in the downfall of Hitler and the Nazi domination, the “hidden children” had to cope with the unpleasant reality that their parents were dead and that they would not be reunited as a family. Other Jewish children faced distress of a different sort. Now they were confronted by the awareness that they would be separated from the various locations which they had known as their residences for an extended period of time and the folks who had cared for them.  This would enable them to re-familiarize themselves with tattered and starved parents who resembled strangers and felt unfamiliar. A Jewish mother interviewed for the documentary describes how it was to be reconciled with her daughter, explaining that her daughter connected with everyone except her, the girl’s own mother.  The woman did not fault her child because, “You can’t love a person just because they say she is your mother.”

==Notes==
 

==References==

*{{cite news
  | title = Secret Lives - Movie - Review
  | newspaper = The New York Times 
  | date = May 2003
  | url =http://movies.nytimes.com/movie/review?res=9F00E3DB173EF935A25756C0A9659C8B63
  | accessdate = August 11, 2008}}

*{{cite web
  | title = Secret Lives: Hidden Children & Their Rescuers...2002: Movie and film reviews
  | publisher =Answers.com
  | date = September 2004
  | url =http://www.answers.com/topic/secret-lives-hidden-children-their-rescuers-during-wwii
  | accessdate = August 11, 2008}}

*{{cite news
  | title = Secret Lives: Hidden Children and Their Rescuers During WWII Movie Review
  | newspaper = Boston Globe
  | date = July 2003
  | url =http://www.boston.com/movies/display?display=movie&id=2786
  | accessdate = August 11, 2008
| first=Janice
| last=Page}}

==External links==
* 
* 
* 

 
 
 
 
 
 