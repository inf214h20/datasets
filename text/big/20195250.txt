Meerabai Not Out
{{Infobox Film
| name           = Meerabai Not Out
| image          = Meerabainotout.JPG
| caption        = Poster of Meerabai Not Out
| director       = Chandrakant Kulkarni
| producer       = Pritish Nandy
| writer         = Soumik Sen
| dialogue       = Ajit Dalvi
| concept        = Raj Kaushal
| cinematography = Rajiv Jain
| starring       = Mandira Bedi Mahesh Manjrekar Anupam Kher
| music          = Sandesh Shandilya
| distributor    = Pritish Nandy Communications
| released       = 5 December 2008
| country        = India
| language       = Hindi
}}
Meerabai Not Out, based on cricket, is a 2008 Hindi Bollywood film that stars Mahesh Manjrekar, Mandira Bedi, Eijaz Khan and Anupam Kher. It is a Pritish Nandy Communications presented movie, directed by Chandrakant Kulkarni.

Mandira Bedi plays a deglam role of a cricket-crazy fan. Anil Kumble is also seen for a small role in the film. 

The film was released in India on 5 December 2008.

==Cast==
* Anupam Kher  ...  Dr. Awasthi  
*  Mahesh Manjrekar  ...  Manoj Anant Achrekar  
*  Mandira Bedi  ...  Meera A. Achrekar  
*  Eijaz Khan  ...  Dr. Arjun Awasthi  
*  Vandana Gupte  ...  Mrs. Achrekar / Aai  
*  Prateeksha Lonkar  ...  Neelima M. Achrekar  
*  Anil Kumble  ...  Himself

==Plot==
Meera Achrekar lives a middle-classed lifestyle in a Shivaji Park Chawl, along with her widowed mother; brother Manoj, his wife, Neelima, and their son, Mayank. After the passing of their father, Anant, Manoj, who was just 18 at that time, took over the financial reins of this family, while she, herself, got employed as a Maths Teacher with Vishwa-Prem Vidyalaya, and heads the Meera XI cricket team in the colony. Her mother and Manoj are on the look-out for a suitable groom, but her obsession with cricket along with the bahenji bespectacled looks turn to her disadvantage. Things start to look up after she dramatically meets with heart specialist Dr. Arjun Awasthi, who lives in a mansion with his widower dentist father. The two families meet, and decide to get the couple married. On the day of the formal engagement, however, things spiral out of control when Meera does not show up.

==Soundtrack==

The Soundtrack was composed by Sandesh Shandilya, Sukhwinder Singh and lyrics were penned by Irfan Siddique, R.N. Dubey & Soumik Sen

{| class="wikitable "
|-
! Track # !! Title !! Singer(s) 
|-
| 1 || "Hai Rama" || Sukhwinder Singh & Neha Kakkar 
|-
| 2 || "Meerabai Not Out" || Neeraj Shridhar & Vijay Prakash 
|-
| 3 || "O Dil Sambhal" || Shaan & Sunidhi Chauhan 
|-
| 4 || "Chal De Rapat" || Vijay Prakash & Sandesh Shandilya 
|-
| 5 || "Kaisi Yeh Shaam" || Kunal Ganjawala
|-
| 6 || "O Dil Sambhal - Remix" || Kunal Ganjawala 
|-
| 7 || "Chal De Rapat - Remix" || Vijay Prakash & Sandesh Shandilya 
|-
|}

==External links==
*  
* 
* 
* 

 
 
 
 
 


 