Tahaan
 
 
{{Infobox film name        = Tahaan – A Boy With a Grenade image       = Tahaan_Poster.jpg caption     = Promotional poster for the film writer      = Santosh Sivan Ritesh Menon Paul Hardart starring    = Purav Bhandare Anupam Kher Sarika Rahul Bose Rahul Khanna Victor Banerjee director    = Santosh Sivan producer    = Shripal Morakhia Mubina Rattonsey editing     = Shakti Hasija distributor = GKIDS (USA)  released    = 5 September 2008 runtime     = language    = Hindi budget      = cinematography = Santosh Sivan music       = Taufique Qureshi soundtrack  = country     = India awards      = followed by =
}}

Tahaan – A Boy With a Grenade ( . The film is based on the life of a young boy and his pet donkey.    It is a fable-like journey of the  ous eight-year-old boy, whose life revolves around the pursuit to find real purpose in his little world. The film stars Purav Bhandare as the young boy. Anupam Kher, Sarika, Rahul Bose, Rahul Khanna and Victor Banerjee form the rest of the cast. It was filmed on location in Jammu and Kashmir.

== Plot ==
Tahaan (Purav Bhandare) lives with his grandfather (Victor Banerjee), mother Haba (Sarika) and older sister Zoya (Sana Shaikh) in the Kashmir valley. The family lives with the ardent hope that the boys father, whos been missing for over three years, will return home. The death of the grandfather pushes them into financial crisis. The local moneylender and his manager (Rahul Khanna) take away the familys assets including Tahaans donkey, Birbal, in lieu of an unpaid loan. While the rest of the family seeks redemption from the crises, Tahaan is determined to bring Birbal back home.

After salvaging money using various means, Tahaan reaches the moneylender to reclaim Birbal. He is told that old Subhan Darr (Anupam Kher) bought the donkey and went across the mountains in which Tahaans father went missing. Gathering courage, Tahaan goes in search of the old man. He finds him and he follows Subhan and his assistant Zafar (Rahul Bose) and their mule train, leading Birbal despite their protests. Although Subhan promises to return Birbal to Tahaan if he can win a race against the incompetent Zafar, when he wins Subhan refuses to give him Birbal. Instead, Subhan gives the donkey to his eight-year-old nephew. Zafar tries to give Tahaan his sunglasses as a replacement for the donkey, but Tahaan will not accept the gift.

On his way back home, Tahaan encounters Idrees, a teenager who discourages him, saying that his efforts will not be sufficient to get Birbal back. Instead he suggests to do him a favour. Tahaan is asked to take a package across the mountains in his onward journey. Upon seeing his eagerness, Idrees hands him over a grenade and says that when the time is right, he will be told what needs to be done. At a checkpoint the package and grenade are not discovered due to the fact that the soldiers know and trust Subhan Darr. Tahaan is about to commit a terrorist act with the grenade and has already removed the pin, when he changes his mind and throws it safely in a river. He then sees his father emerge from the building he almost blew up.
Subhans nephew learns that Tahaan is fond of Birbal, and at his request Subhan gives the donkey back to Tahaan.

==Cast==
*Purav Bhandare.... Tahaan
*Rahul Bose.... Zafar
*Anupam Kher.... Subhan 
*Sarika.... Haba
*Victor Banerjee.... Grandfather
*Rahul Khanna.... Kuku Saab
*Sana Sheikh.... Zoya
*Ankush Dubey.... Idrees
*Dheirya Sonecha.... Yasin
*Rasika Dugal

== Production == The Terrorist (1999), Asoka (2001 film)|Asoka (2001), Anandabhadram (2005) and Before the Rains (2007), award-winning director Santosh Sivan got the idea for this film after reading a newspaper report. He formed a fable-like story from the report.   

Since Kashmir is a strife-torn area, films are rarely picturised there. However in the case of this film, Sivan thought that audiences can relate it to the film well.  It was only after 18 years that a film was filmed in Kashmir. {{cite web|title=
Tahaan, story of a Kashmiri boy|url=http://www.ptinews.com/pti\ptisite.nsf/0/0D871DB589DB0655652574B4002F80ED?OpenDocument|date=29 August 2008|accessdate=4 September 2008|agency=Press Trust of India}}   

While filming in Pahalgam, Sivan realised to his surprise that children were comfortable with guns. It seemed to him that it was a part of everyday life for them.       The film makes eloquent use of Quranic verses or azaan, for which the director took help from research scholars in Kashmir.

 When shooting there we only wanted to show the real life. I had heard some Quranic verses there at some Dargah. I thought I could use them to send a message of hope. I wanted to use the azaan for a nice purpose, a beautiful thing, not for any wrong deed... At the end I wanted to show the positive power of a dream.  

==Critical reception==
The film opened to generally positive reviews. Ziya Us Salam of The Hindu hailed the film as a visual poem and "Responsible cinema, brilliant cinema."  Raja Sen of Rediff gave it 4 out of 5 stars, calling it a "must-watch".  Rajiv Masand of CNN-IBN called it a "film of great virtue" and gave 3 out of 5 stars.  Baradwaj Rangan called it a "film of first-rate performances". 

==Awards==
*Tahaan won a High Commendation in Childrens Feature Film section at the 2009 Asia Pacific Screen Awards. 
*Tahaan Won Best feature film award, CIFEJ Award (Centre International du Film pour l Enfant et la Jeunesse)and UNICEF Award at 11th Olympia International Film Festival for Children and Young People in 2008 held at Greece 
*Tahaan won "The German Star of India award" at "Bollywood and Beyond" festival at Stuttgart Germany in 2009 

==Festivals==
The film has gone to the following festivals:

;2008
*Pusan
*Rome
*Cinekid
*London Film Festival
*Olympia Film Festival in Greece. (Winner of 3 awards at the festival)
*Amazonas in Brazil

;2009
*Palm Springs Festival in the USA.
*Hong Kong International Film Festival
*Stockholm International Film Festival
*New York International Children’s Film Festival
*Fribourg International Film Festival in Switzerland
*Cairo International Film Festival
*Seattle International Film Festival
*Titanic International Film Festival (Hungary)
*Movies that Matter (Amsterdam) Showcase sponsored by the Amnesty International.
*Munich Film Festival
*Stuttgart Festival

== Distribution ==

Tahaan was picked up for US distribution by GKIDS.  The film will play as the Centerpiece Selection of the MIAAC Indian Film Festival,  as well as the Starz Denver Film Festival,  the St Louis Film Festival,  the Three Rivers Film Festival,  and the New York/San Francisco International Childrens Film Festival. 

== References ==
 

== External links ==
* 
*  

 

 
 
 