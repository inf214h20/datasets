The Body (2001 film)
{{Infobox Film |





 name = The Body image = The-body2001.jpg director = Jonas McCord producer = Rudy Cohen writer = Jonas McCord starring = Jason Fleyming John Wood Vernon Dobtcheff music = Serge Colbert cinematography = Vilmos Zsigmond editing = Alain Jakubowicz Lesley Walker studio = MDP Worldwide distributor = TriStar Pictures released = 2001 runtime = 109 min language = English budget =
}} novel by Richard Sapir, and starring Antonio Banderas and Olivia Williams.
 Vatican to archaeologic finding by Dr. Sharon Galban (Williams) which is suspected to be the remains of the body of Jesus Christ. This finding puts Gutierrez faith and his doubts in constant confrontation with Galbans scientific views.

Also, the finding stirs the political problems between Palestine and Israel in the area, while also shaking the foundations of Christianity itself. Both of these problems put Dr. Galban, and Gutierrez himself, in danger.

==Plot==

Dr. Sharon Galban (Williams) finds an ancient skeleton in Jerusalem in a rich mans tomb. Coloration of the wrist and ankle bones indicates the cause of death was crucifixion. Several artifacts, including a gold coin bearing the marks of Pontius Pilate and a jar dating to 32 A.D., date the tomb to the year Christ died. Faint markings on the skull consistent with thorns, the absence of broken leg bones, occupational markers suggesting the deceased was a carpenter, and a nick on the ribs from a pointed object lead authorities to suspect that these could be the bones of Christ. The different reactions of politicians, clerics, religious extremists&mdash; some prepared to use terror to gain their ends&mdash;to the religious, cultural and political implications of the find, make life difficult and dangerous for the investigators as they seek to unearth the truth.

Father Matt Gutierrez is assigned by the Vatican to investigate the case and to protect the Christian faith. He sets out to prove that the bones are not those of Jesus, but as there is more and more evidence to support the claim, his faith begins to waver. Troubled by the case, he turns to Father Lavelle (Derek Jacobi) who commits suicide because he cannot reconcile the scientific evidence with his faith. This event causes Father Gutierrez to turn from his faith, but he comes back to it in the end. He also comes to understand that it is the Catholic Church that he is protecting and not the faith, and decides to resign from his priesthood.

==Cast==
* Antonio Banderas - Father Matt Gutierrez
* Olivia Williams - Dr. Sharon Galban
* John Shrapnel - Moshe Cohen
* Derek Jacobi - Father Lavelle Jason Fleyming - Father Walter Winstead
* Lillian Lux - Mother
* Mohammed Bakri - Abu Yusef John Wood - Cardinal Pesci
* Makram Khoury - Nasir Hamid
* Vernon Dobtcheff - Monsignor
* Jordan Licht - Dorene, Dr. Golbans daughter

==Themes==
The film deals mainly with two subjects:
* The Israeli-Palestinian conflict in the region where the body is found. Both sides believe that control of the site will give them an upper hand in the conflict. resurrection of Jesus Christ in the Christian faith.

==Archaeology in the Film==
Dr. Galban is much more meticulous in her work than many other film archaeologists are. She examines the bones found without removing them from the site, and is very careful not to compromise the remains or any other artifacts found. The small things at the site are not overlooked and looting is one of the problems she faces, which are both aspects of real archaeology  (See also: Small Finds). For instance, she uses several methods for dating the tomb such as thermoluminesence dating and relative dating techniques.

===Thermoluminesence Dating===
The clay jar that is found inside the tomb was brought to a lab for two kinds of testing: dating and composition of contents. Dr. Galban wanted to determine the age (and therefore date the tomb) and figure out what the jar was used for. Thermoluminescence dating is commonly used on ancient pottery and tools. This technique is used to determine the time passed since an object was fired (such as pottery). It has been found to be very accurate, to within ±1 to 10%.  It works by thermally stimulating the object in question, thereby releasing the energy accumulated in the object during its preliminary excitation.  In other words, when an object is fired, electrons are released within the object itself and its clock is set to "0". Thermoluminescence releases trapped electrons and uses the amount of energy released to determine the time since that initial firing (time 0).
 anointing jar, which was one aspect of Jewish burial rituals.

===Shroud of Turin===

Briefly in the film Father Matt Gutierrez and archaeologist Dr. Sharon Galban talk about the famous Shroud of Turin.  The 4.1m x 1.1m linen cloth is well known for its depiction of an image of a man that looks very similar to the description of Jesus Christ. The man on the shroud suffered from physical injuries very similar to those that someone would bear after being crucified, consistent with the Biblical description of Christs crucifixion. Father Gutierrez mentions the Shroud of Turin when there is a question over the height of the body that is found in the tomb. He says the bones in the tomb can’t possibly be those of Jesus Christ because the height of the skeleton found does not fit with the height of the man believed to be Christ portrayed on the Shroud of Turin.

The bones in the tomb belong to a person who would stand at a height of approximately 5’5”, whereas the body that is believed to be Christ’s on the shroud is of a man of 5’11.5” to 6’2”; while the Bible does reference Jesus as being tall, during the times of Christ such a height would indeed have been remarkable. Dr. Galban responds with some animosity towards his comment because the Shroud of Turin has been proven through scientific testing to be a hoax and not the image of Jesus Christ but instead a victim of Roman crucifixion.  The Shroud of Turin is believed to be an artists representation of Jesus by means of using an impression technique on the linen 

==External links==
* 
* 
* 
* 
* 

==References==
 
*  
*  

 
 
 
 
 