Jews of Egypt (film)
{{Infobox film
| name           = Jews of Egypt
| image          = Jews-of-egypt-poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Amir Ramses
| producer       = Haitham Al-Khamissi
| writer         = Amir Ramsis, Mostafa Youssef
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2013
| runtime        =
| country        = Egypt French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Jews of Egypt ( ) is an Egyptian  . Monday 25 February 2013. Retrieved on 9 April 2013.  Alastair Beach of  . Sunday 21 March 2013. Retrieved on 9 April 2013. 

==Content==
The film covers the Jewish involvement in Egyptian business and arts in the first half of the 20th Century. It then mentions the founding of Israel in 1948, the Egyptian Revolution of 1952, and the Suez Crisis in 1956. Due to the crisis, the Jews of Egypt were forced into exile. 

People giving testimonials in the film include exiled Egyptian Jews, most of whom lived in   who had participated in an attack of Jewish shops in Egypt in 1947; and Essam Fawzi, a sociologist. 

==Production==
Director Amir Ramses said that he had considered making the film for several years. Ramses and producer Haitham Al-Khamissi self-funded the film, believing that relying on a sponsor, whether the sponsor was Arab or not, would hamper the neutrality of the film. 

Ramses took a six month trip to prepare for making the film. Ramses began conducting research in late 2008. Research consisted of locating and interviewing Jews within Egypt, building a "historical skeleton," and then obtaining print media, videos, and other archival material. Film shooting began in 2009. The 2011 Egyptian revolution caused work on the film to be suspended. In the meantime the director and his group took a trip to Morocco. Work on the film was made for one additional year until the September 2012 completion. 

==Release==
The avant-première occurred in October 2012 during the Panorama of the European Film. Amir Ramses said that the premiere took place in a "blatantly intellectual context."  It was screened at the northern hemisphere winter 2012 Arab Camera Festival in Rotterdam and the January 2013 Palm Springs International Film Festival. 

In the first week of March 2013 the film was scheduled for screening in theatres in Cairo.  On Wednesday March 13, 2013, producer Haitham El-Khameesy said that the Censorship Bureau officials did not issue a permit for a release of his film in Egyptian cinemas and that they requested to view the film before they could allow its screening. Reuters said that security source told them that the permit had been granted and that it had not prevented its screening.  The films Egyptian cinema screening was ultimately scheduled for 27 March 2013. 

==Reception==
In 2013 Sara Elkamel of Al Ahram Weekly said "So far, the film has been attacked sporadically in the press, mostly in the form of normalization with Israel accusations, but the filmmaker has not received death threats or direct attacks." 

After the Egyptian government canceled the screening of the film, Khaled Diab, an Egyptian-Belgian blogger, journalist, and writer, produced an opinion piece in Haaretz in which he argued that "This damages the push-back against strong anti-Jewish sentiment gripping the country, while failing to remind Egyptians of a past era of diversity and tolerance."  Ada Aharoni, the editor of "The Golden Age of the Jews From Egypt," said "This film claimed Jews had it good in Egypt and left only to America and France, not Israel — and still it was banned." 

==See also==
 
* Cinema of Egypt
* Joseph Cicurel
* Togo Mizrahi
* Laila Mourad
* Mounir Mourad

==References==
 

==External links==
*   official website
**" ."   
*  
* Razek, Rami Abdel (رامي عبد الرازق) " ." ( ) Al-Masry Al-Youm. Wednesday 3 April 2013.
* " ." ( ) CNN Arabic. Sunday 24 March 2013.

 
 
 
 