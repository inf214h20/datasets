Ramapurada Ravana
{{Infobox film
| name           = Ramapurada Ravana
| image          =
| image_size     =
| caption        =
| director       = Rajachandra
| producer       = C Jayaram G Radhakrishna
| writer         =
| screenplay     = Geetha Aarathi Thoogudeepa Srinivas
| music          = Rajan-Nagendra
| cinematography = B C Gowrishankar
| editing        = Manohar
| studio         = Durgaparameshwari Productions
| distributor    =
| released       =  
| country        = India Kannada
}}
 1984 Cinema Indian Kannada Kannada film,  directed by  Rajachandra and produced by C Jayaram and G Radhakrishna. The film stars Anant Nag, Geetha (actress)|Geetha, Aarathi and Thoogudeepa Srinivas in lead roles. The film had musical score by Rajan-Nagendra.   

==Cast==
 
*Anant Nag Geetha
*Aarathi
*Thoogudeepa Srinivas
*C R Simha
*Dinesh
*Manu
*Umesh
*Lokanath
*Bheema Rao
*Keerthiraj
*K. S. Ashwath
*Thimmayya
*Neegro Johny
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || A Aa E Ee Sariyagi || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.33
|-
| 2 || Kanda Naguthiroo || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.21
|-
| 3 || Gata Gatane Ninna || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.11
|}

==References==
 

==External links==

 
 
 
 


 