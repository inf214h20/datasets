La Chèvre
{{Infobox Film
| name           = La Chèvre
| image          = La Chevre film.jpg
| caption        = 
| director       = Francis Veber
| producer       = Alain Poiré
| writer         = Francis Veber
| starring       = Pierre Richard Gérard Depardieu
| music          = Vladimir Cosma
| cinematography = 
| editing        = 	
| distributor    = 
| released       = 1 December 1982 (Mexico) 9 December 1981 (France)
| runtime        = 95 minutes
| country        = France Mexico French Spanish Spanish
| budget         = 
| gross          = $ 56.6 millions
}}
La Chèvre ( , starring Pierre Richard and Gérard Depardieu. It is the first of three films featuring Richard and Depardieu as a comic duo.

An American remake of this film was made in 1991, starring Martin Short and Danny Glover, entitled Pure Luck.

==Plot==
La Chèvre features Depardieu as the tough-guy private detective Campana, hired to find Marie, the daughter of a rich businessman, who has mysteriously disappeared while vacationing in Mexico. The case turns out to be complicated – several attempts to find her have already failed. A psychologist, Meyer, who works for the businessman, suggests a plan. Marie is known to be extremely unlucky and accident-prone; the psychologist advises sending someone equally accident-prone to find her, on the theory that what happened to her may also happen to him, and thus, following her steps while the detective tags along, the daughter can be found and returned home. Richards character Perrin is an awkward, accident-prone accountant who works for the businessman, and is chosen to implement the scheme. The adventures of an odd duo begin...

==Cast and roles==
* Pierre Richard — François Perrin
* Gérard Depardieu — Campana
* Pedro Armendáriz Jr. — The Captain Corynne Charbit — Marie Bens
* Maritza Olivares		
* André Valardy — Meyer
* Jorge Luke — Arbal
* Sergio Calderón — Prisoner
* Michel Robin — Alexandre Bens
* Robert Dalban — The technician
* Jacqueline Noëlle		
* Michel Fortin — The man at Orly
* Marjorie Godin		
* Jean-Louis Fortuit		
* Pulcher Castan

==Release history==
{|class="wikitable"
!Country
!Release date
|- France
|9 December 1981
|- Germany
|19 March 1982
|- Netherlands
|15 July 1982
|- Mexico
|1 December 1982
|- Italy
|13 February 1983
|- Soviet Union 1985
|- United States 26 July 1985
|}

==Sources==
*  at Films de France site.

==External links==
* 
*  
*    

 

 
 
 
 
 
 
 
 
 
 
 

 
 