Say Anything...
 
{{Infobox film
| name           = Say Anything...
| image          = Say_Anything.jpg
| caption        = Theatrical release poster
| director       = Cameron Crowe
| producer       = Polly Platt
| writer         = Cameron Crowe
| starring       = John Cusack Ione Skye John Mahoney
| music          = Anne Dudley Richard Gibbs
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Richard Marks
| studio         = Gracie Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $16 million 
| gross          = $21.5 million 
}}
 romantic comedy-drama film written and directed by Cameron Crowe in his directorial debut. In 2002, Entertainment Weekly ranked Say Anything... as the greatest modern movie romance, and it was ranked number 11 on Entertainment Weekly  list of the 50 best high-school movies. 

The film follows the relationship between Lloyd Dobler (John Cusack), an average student, and Diane Court (Ione Skye), the valedictorian, immediately after their graduation from high school.

==Plot summary==
Set in Seattle, Washington, the film features Lloyd Dobler (John Cusack), an average student and aspiring kickboxing|kickboxer, who attempts a relationship with the sweet valedictorian Diane Court (Ione Skye) immediately after their graduation from the same high school. Diane has just won a major fellowship to study in England, and will be going there at the end of the summer. Highly intelligent yet socially naive, Diane is intrigued by Lloyds charming persona and confidence to take a chance on someone like her. She agrees to Lloyds request for a date, and the two of them begin seeing each other regularly.

Lloyd seeks advice and counsel from his sister and several close female friends who are genuinely looking out for Lloyds best interests as he embarks on his first serious romantic relationship. Dianes father, Jim (John Mahoney), is under investigation by the Internal Revenue Service for alleged tax violations committed at the nursing home he owns; and, as her relationship with Lloyd deepens, Diane worries that she should be spending more time with her father, rather than with Lloyd. Also, Jim does not approve of his daughter dating a slacker, and pressures her to break up with him.
 aviophobic Diane on her flight to England.

==Cast==
* John Cusack as Lloyd Dobler
* Ione Skye as Diane Court
* John Mahoney as Jim Court
* Lili Taylor as Corey Flood
* Polly Platt as Mrs. Flood
* Bebe Neuwirth as Mrs. Evans
* Loren Dean as Joe
* Pamela Adlon as Rebecca
* Chynna Phillips as Mimi
* Jeremy Piven as Mark
* Eric Stoltz as Vahlere
* Philip Baker Hall as IRS Boss
* Lois Chiles as Dianes Mother (uncredited)
* Joan Cusack as Constance Dobler (uncredited)
* Dan Castellaneta as Dianes teacher (uncredited)

==Soundtrack==
Allmusic said the soundtrack, like the film, is "much smarter than the standard teen fare of the era."  The soundtrack consists of these songs:
 Nancy Wilson   Cult of Personality" – Living Colour  
* " " – Joe Satriani  
* "You Want It" – Cheap Trick  
* "Taste the Pain" – Red Hot Chili Peppers   In Your Eyes" – Peter Gabriel  
* "Stripped (song)|Stripped" – Depeche Mode   Fishbone   The Replacements   Freiheit  
* "Lloyd Dobler Rap"  

==Critical reception==
Chicago Sun-Times film critic Roger Ebert called it "one of the best films of the year—a film that is really about something, that cares deeply about the issues it contains—and yet it also works wonderfully as a funny, warmhearted romantic comedy."  Ebert later included it on his 2002 Great Movie list, writing, "Say Anything exists entirely in a real world, is not a fantasy or a pious parable, has characters who we sort of recognize, and is directed with care for the human feelings involved."  It has also received a "98% Fresh" rating (39 fresh/1 rotten) at Rotten Tomatoes, with the consensus reading: "One of the definitive Generation X movies, Say Anything is equally funny and heartfelt -- and it established John Cusack as an icon for left-of-center types everywhere." 

The film also had detractors. Variety (magazine)|Variety called it a "half-baked love story, full of good intentions but uneven in the telling."  Caryn James of The New York Times said the film
 
resembles a first-rate production of a childrens story. Its sense of parents and the summer after high school is myopic, presented totally from the teen-agers point of view. Yet its melodrama—Will Dad go to prison? Will Diane go to England?—distorts that perspective, so the film doesnt have much to offer an actual adult, not even a sense of what its truly like to be just out of high school these days. The film is all charming performances and grace notes, but there are plenty of worse things to be.  

==TV series==
A TV series based on the movie was planned by NBC and 20th Century Fox, but producers Aaron Kaplan and Justin Adler did not know that Cameron Crowe had not approved of the project. When they found out his views, the show was dropped. 

==References==
 

== External links ==
*  
*  
*  
*  
*   at The Numbers
*  . Sam Adams. Los Angeles Times. October 25, 2009.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 