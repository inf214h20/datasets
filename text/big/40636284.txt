1 by Two
{{Infobox film
| name = 1 by Two
| image = 1 by Two Movie Poster.jpg
| image_size = 
| caption = When the mind sings, you face the music.
| director = Arun Kumar Aravind
| producer = B. Rakesh
| writer = Jeyamohan
| narrator = Abhinaya
| music = Gopi Sundar
| cinematography = Jomon Thomas
| editing = Prejish Prakash
| studio = Universal Cinema
| distributor = Thameens Release
| released =  
| runtime = 150 minutes
| country = India
| language = Malayalam
| budget         =   
| gross    =  
}} Malayalam psychological Abhinaya in the lead roles. The music department is handled by Gopi Sunder with lyrics penned by Hari Narayanan. Cinematography is handled by Jomon Thomas. 

== Cast ==
* Fahadh Faasil as Yusuf Marikkar
* Murali Gopy as Dr. Hari Narayan and Ravi Narayan
* Honey Rose as Dr. Prema Abhinaya as Raziya
* Shyamaprasad as Dr. Cheriyan Sruthi Ramakrishnan as Meghna
* Azhagam Perumal as Narayanan Pillai
* Ashvin Mathew as Dr. Balakrishnan

==Production==
It is Arun Kumar Aravinds fourth directorial venture.  Fahadh Faasil was initially chosen to play the lead role of police officer, a role he had not played before. Shyamaprasad plays the role of a psychiatrist Dr. Cheriyan. Honey Rose and Abhinaya are playing the female leads in 1 by Two. Shruthi Ramakrishnan, Ashwin Mathew and Azhakar Perumal are the other characters seen in supporting roles. Murali Gopy plays the role of a surgeon in the film. Bangalore, Mysore and Palakkad were the major locations where the film was shot.

The trailer was released on 11 March 2014 and the main release scheduled for April 19.
 

==References==
 

==External links==
* 

 
 
 
 


 