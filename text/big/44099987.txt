Manjeeradhwani
{{Infobox film
| name           = Manjeeradhwani
| image          =
| caption        =
| director       = Bharathan
| producer       = John Paul (dialogues)
| screenplay     = Bharathan
| starring       = Sakshi Sivanand Vineeth Kaviyoor Ponnamma Nassar
| music          = Ilayaraja
| cinematography = Tirru
| editing        = A. Sreekar Prasad
| studio         = Priya Arts
| distributor    = Priya Arts
| released       =  
| country        = India Malayalam
}}
 1998 Cinema Indian Malayalam Malayalam film,  directed by Bharathan. The film stars Sakshi Sivanand, Vineeth, Kaviyoor Ponnamma and Nassar in lead roles. The film had musical score by Ilayaraja.   

==Cast==
*Sakshi Sivanand
*Vineeth
*Kaviyoor Ponnamma
*Nassar
*Priyanka
*Shanthi Krishna

==Soundtrack==
The music was composed by Ilayaraja and lyrics was written by MD Rajendran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Swapna Bhoomiye || KS Chithra, MG Sreekumar || MD Rajendran || 
|-
| 2 || Jalatharanga Leela || KS Chithra, MG Sreekumar || MD Rajendran || 
|-
| 3 || Mohini enikkaay || KS Chithra, Pradip Somasundaran || MD Rajendran || 
|-
| 4 || Padumanaabha || Arundhathi || MD Rajendran || 
|-
| 5 || Rani lalitha || KS Chithra, Biju Narayanan || MD Rajendran || 
|-
| 6 || Rim jim || KS Chithra, Biju Narayanan || MD Rajendran || 
|-
| 7 || Thappu Thakilu Melam || KS Chithra, Chorus || MD Rajendran || 
|}

==References==
 

==External links==
*  

 
 
 
 
 