Nadja (film)
 Nadja}}
{{Infobox film
| name           = Nadja
| image          = Nadjaposter.jpg
| caption        =
| director       = Michael Almereyda
| producer       = David Lynch Mary Sweeney
| writer         = Michael Almereyda
| starring       = Elina Löwensohn Peter Fonda Karl Geary
| music          = Simon Fisher Turner
| cinematography = Jim Denault
| editing        = David Leonard
| distributor    = October Films
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = United States dollar|$1,000,000 (estimated)
}}
 Van Helsing. As the characters names suggest, Nadja is a vampire film, but treating elements of the genre in an understated arthouse style.

The deadpan acting, episodic nature of the plot, and the presence of Martin Donovan and Löwensohn are suggestive of a Hal Hartley film though he was not involved in the production. The Chicago Review called it "Hal Hartley meets David Lynch".  The style of the film changes from dramatic horror to horror comedy by the end as evidenced by the laughing vampire toy during a trip to Romania.
 My Bloody Valentine and "Strangers" and "Roads" by Portishead (band)|Portishead.

==Cast== Van Helsing
* Suzy Amis as Cassandra
* Galaxy Craze as Lucy
* Martin Donovan as Jim
* Jared Harris as Edgar
* Karl Geary as Renfield
* Elina Löwensohn as Nadja
* David Lynch (also Executive producer) as morgue attendant (cameo)

==See also==
*Vampire film

==External links==
*  
 

 

 
 
 
 
 

 