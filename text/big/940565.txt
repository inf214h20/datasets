Monsoon Wedding
 
 
 
{{Infobox film name = Monsoon Wedding image = Monsoon_Wedding_poster.jpg image caption = Movie poster director = Mira Nair writer = Sabrina Dhawan starring = Naseeruddin Shah Lillete Dubey Shefali Shah Vasundhara Das Vijay Raaz Tillotama Shome music = Mychael Danna producer = Caroline Baron Mira Nair editing = Allyson C. Johnson cinematography = Declan Quinn studio = Mirabai Films distributor = USA Films released = 30 August 2001 (première at Venice Film Festival|Venice) 30 November 2001 (Worldwide)    runtime = 114 min language = Punjabi
|budget = US$1,200,000  gross = US$30,787,356   
}} Punjabi Hindu wedding in Delhi. Writer Sabrina Dhawan wrote the first draft of the screenplay in a week while she was at Columbia Universitys MFA film program.  Monsoon Wedding earned just above $30 million at the box office.  Although it is set entirely in New Delhi, the film was an international co-production between companies in India, the United States, Italy, France, and Germany.  
 Golden Globe Award nomination. A musical based on the film was in development and was premiered on Broadway in April 2014.  The film was premiered in the Marché du Film section of the 2001 Cannes Film Festival.  

==Plot== arranged a marriage with a man, Hemant Rai (Parvin Dabas), she has known for only a few weeks. As so often happens in the Punjabi culture, such a wedding means that, for one of the few times in each generation, the extended family comes together from all corners of the globe, including India, Australia, Oman, and the United States, bringing its emotional baggage along.

The movie focuses on the struggle put by Pimmi and Lalit for their daughter. Meanwhile, Tej Puri (Rajat Kapoor), Lalits brother-in-law, who had given Lalit a new life, arrives. But a cousin of Aditi, Ria Verma (Shefali Shah), stays away from Tej, as the latter had earlier molested her, and now does the same to a younger relative, Aliya. 

Aditi tells Hemant about her passionate affair with her married ex-boss Vikram, to which Hemant first reacts angrily but later accepts the fact, as Aditi is breaking up with Vikram. On the other hand, P.K. Dubey (Vijay Raaz), the wedding planner, falls in love with Alice, the maid of the Vermas. One night before the marriage, after a series of jokes and dances, Ria catches Tej taking Aliya for a "drive" - supposedly planning to rape her. Ria reaches them on time and confesses that she knows everything about his nature. Heartbroken after this conflict, Ria leaves the ceremony. The next day, Lalit goes to Ria and pleads her to return, to which she agrees; but he also tells her that he cant disown Tej: as a family member, they are indebted to Tej. But hours before the wedding, Lalit tells Tej and his wife to leave the wedding and the family, as he will go to any extent to protect the children of the family. Tej and his wife leave the family and the wedding. 

Soon, during the wedding ceremony, Dubey and Alice get married happily with the help of the Vermas; Aditi and Hemant tie the knot; Ria moves on with her past life and supposedly falls in love with another family member. All is well and the end shows everyone happy and dancing to the music.

== Cast ==
* Naseeruddin Shah as Lalit Verma
* Lillete Dubey as Pimmi Verma
* Shefali Shah as Ria Verma
* Vasundhara Das as Aditi Verma
* Vijay Raaz as Parabatlal Kanhaiyalal P.K. Dubey
* Tillotama Shome as Alice
* Parvin Dabas as Hemant Rai
* Kulbhushan Kharbanda as C.L. Chadha
* Kamini Khanna as Shashi Chadha
* Rajat Kapoor as Tej Puri
* Randeep Hooda as Rahul Chadha
* Neha Dubey as Ayesha Verma
* Roshan Seth as Mohan Rai
* Soni Razdan as Saroj Rai
* Jas Arora as Umang Chadha
* Natasha Rastogi as Sona Verma
* Ram Kapoor as Shelly
* Dibyendu Bhattacharya as Lottery

==Soundtrack==
 
The soundtrack includes a qawwali by Nusrat Fateh Ali Khan, a ghazal by Farida Khanum, a Punjabi song by Sukhwinder Singh, an old Indian song by Rafi, a folk dance song.
The film includes an Urdu ghazal, Aaj Jaane Ki Zid Na Karo (Dont Be So Stubborn About Leaving Today) sung by Pakistani artist Farida Khanum.

{{tracklist
| all_music       = Mychael Danna (except where listed)
| music_credits   = yes
| title1          = Feels Like Rain
| length1         = 0:28
| title2          = oh kanjara ve  
| note2           = Performed by Sukhwinder Singh
| music2          = Sukhwinder Singh
| length2         = 5:11
| title3          = Baraat
| length3         = 2:13
| title4          = Aaj Mausam Bada Beimann Hai  (*)   
| note4           = Performed by Mohammed Rafi
| music4          = Laxmikant-Pyarelal  (*) 
| length4         = 3:20
| title5          = Your Good Name
| length5         = 3:38
| title6          = Delhi.com
| length6         = 1:41
| title7          = Fuse Box
| length7         = 2:31
| title8          = Mehndi / Madhorama Pencha
| note8           = Performed by Madan Bala Sindhu
| length8         = 3:26
| title9          = Banished
| length9         = 0:52
| title10          = Good Indian Girls
| length10         = 3:41
| title11          = Fabric / Aaja Savariya
| note11           = Performed by MIDIval Punditz
| length11         = 3:01
| title12          = Allah Hoo
| note12           = Performed by Nusrat Fateh Ali Khan
| length12         = 4:39
| title13          = Hold Me, Im Falling
| length13         = 2:57
| title14          = Love and Marigolds
| length14         = 2:45
| title15          = Chunari Chunari  (**)  Abhijeet and Anuradha Sriram
| music15          = Anu Malik  (**) 
| length15         = 4:08
| title16          = Aaja Nachle
| note16           = Performed by Bally Sagoo feat. Hans Raj Hans
| music16          = Bally Sagoo
| length16         = 3:40
| title17          = Aaj Mera Jee Kardaa - (Zimpala remix)
| length17         = 4:56
| title18          = Fuse Box - Alex Kids Dub Remix
| length18         = 6:14
| title19          = Fuse Box - Julio Black Remix
| length19         = 3:03

}}

*  (*)  Originally featured in the Hindi film Loafer (1973 film)|Loafer (1973)
*  (**)  Originally featured in the Hindi film Biwi No.1 (1999)

==Awards==
 
The movie won the Golden Lion at the Venice Film Festival. Mira Nair was the second Indian (after Satyajit Ray for Aparajito) to receive this honour.

===Won===
 
* British Independent Film Award for Best Foreign Language Film
* Canberra International Film Festival - Audience Award (Mira Nair)
* Independent Spirit Awards - Producers Award (Caroline Baron)
* Venice Film Festival - Golden Lion (Mira Nair)
* Venice Film Festival - Laterna Magica Prize (Mira Nair)

===Nominated===
  BAFTA Award for Best Foreign Language Film (Caroline Baron, Mira Nair)
* Broadcast Film Critics Association Award for Best Foreign Language Film European Film Award for Best Non-European Film (Mira Nair)
* Golden Globe Award for Best Foreign Language Film
* Online Film Critics Society Award for Best Foreign Language Film
* Satellite Award for Best Foreign Language Film

==Home media==
This film was released on DVD in 2002. In 2009, it was released as part of the Criterion Collection. 

==Stage musical==
 
The film will hit Broadway in November 2015.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 