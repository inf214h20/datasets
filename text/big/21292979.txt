Men and Women (1999 film)
{{Infobox film 
|  name = Men and Women
|  image = Men_And_Women_1999film.jpg
|  caption = 
|  director = Liu Bingjian 
|  writer = Liu Bingjian Cui Zien Qing Yang Yu Bo
|  producer = Li Jinliang
|  music = Zhao Jiping Qu Xiaosong Xu Jun
|  editing = Ah Yi
|  distributor = 
|  released = Locarno International Film Festival|Locarno August 12, 1999
|  runtime = 89 minutes
|  language = Mandarin
|  budget =  
|  country = China
}}
Men and Women ( ) is a 1999 Chinese comedy-drama film directed by Liu Bingjian. The film was co-written by Cui Zien, one of the few openly gay writers in China. Cui also has a cameo-role in the film, as the host of an underground radio show.

One of Chinas few films touching on LGBT-films, Men and Women was cast with openly gay actors.    Unlike earlier films like Zhang Yuans East Palace, West Palace, Men and Women focused on the daily lives of its characters, rather than their underground existence in Chinese society.

The film won a FIPRESCI award at the 1999 Locarno International Film Festival. 

== Plot ==
Men and Women follows the travels of a young homosexual man, Xiao Bo, who goes to Beijing in search of a job. There he is taken in by Qing Jie, who not only gives him a home in her apartment, but also a job in her clothing store. While she tries to set Xiao Bo with her friend A Meng, Xiao Bo resists and eventually moves out when he is assaulted by Qing Jies husband. He moves in with his friend, Chong Chong, with whom a romantic relationship is kindled.

Qing Jie, meanwhile, discovers that she may have feelings for A Meng, and decides to leave her husband.

== Cast ==
*Yu Bo as Xiao Bo
*Yang Qing as Qing Jie, the casts only professional actor (Yang had previously acted in Chinese television), Yang plays Qing Jie, who befriends Xiao Bo and gives him both a place to stay and a job at her clothing store.

== See also ==
* East Palace, West Palace, director Zhang Yuans earlier film, often seen as the first Chinese film to have homosexual themes.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 
 