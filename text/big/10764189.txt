My Little Duckaroo
 
 
{{Infobox Hollywood cartoon|
| cartoon_name = My Little Duckaroo
| series = Merrie Melodies/Daffy Duck
| image = My_Little_Duckaroo_Titles.jpg
| caption = The title card. Charles M. Jones
| story_artist = Michael Maltese Richard Thompson   Lloyd Vaughan   Ben Washam
| voice_actor = Mel Blanc
| musician = Milt Franklyn Edward Selzer
| distributor = Warner Bros. Pictures   Vitaphone Corporation
| release_date = November 27, 1954 (USA)
| color_process = Technicolor
| runtime = 6 min, 40 sec.
| movie_language = English
}}
 Edward Selzer.  It was directed by Chuck Jones and written by Michael Maltese.   bcdb.com May 9, 2011 

This cartoon in many ways resembles the 1951 short directed by Chuck Jones entitled Drip-Along Daffy.  In this animated piece, upon seeing a wanted poster with a reward of $10,000.00 for the dead or alive capture of Nasty Canasta, Daffy Duck sets out alongside his companion Porky Pig|Porky, and his trusty steed to retrieve the villain and collect on the money.

==Summary==
Much like Drip-Along Daffy, this cartoon parodies the widely popular Westerns of the time period.  The Masked Avenger (Daffy) and his sidekick Comedy Relief (Porky) ride along in the desert until they come across a poster offering $10,000 reward for Nasty Canasta, wanted dead or alive for crimes including horse stealing, candy stealing, gag stealing, and square dancing in a round house.  "This looks like a job for... the Masked Ee-venger  ," he shouts. "And besides, it isnt the principle of the thing, its the money."

Following large conspicuous signs to Canastas hide-out, Daffy tells Porky to wait outside whilst I go in and fix his little red wagon. Daffy bursts into the hide-out, to find Canasta sitting peacefully at a table playing cards. He announces himself as the Masked Avenger but Canasta pays no attention until Daffy offers advice on his card game. Challenged to play cards, Daffy departs and returns in new cowboy garb (but without the mask), confident of victory.
 coat of paint. Now Im going home to mother.)

==See also==
* List of cartoons featuring Daffy Duck
* List of cartoons featuring Porky Pig

==References==
 

==External links==
* 

 
 
 
 
 
 
 