The Mirror (1999 film)
 
 
{{Infobox film
| name = The Mirror
| image = 
| alt = 
| caption = 
| film name = {{Film name| traditional = 怪談之魔鏡
 | simplified = 怪谈之魔镜
 | pinyin = Guài Tán Zhī Mó Jìng
 | jyutping = Gwaai3 Taam4 Zi1 Mo1 Geng3}}
| director = Siu Wing Raymond Wong Ha Boon-kwang Raymond Wong Liu Xiaoguang
| starring = Nicholas Tse Ruby Lin Lillian Ho Law Lan Jack Neo Xu Fan Sherming Yiu
| music = Marco Mak
| cinematography = Kwong Ting-wo
| editing = Marco Mak
| studio = Mandarin Films
| distributor = Mandarin Films Distribution
| released =  
| runtime = 87 minutes
| country = Hong Kong
| language = Cantonese Mandarin Hokkien
| budget = 
| gross =
}} Raymond Wong.

==Plot==
The film is divided into five unrelated segments, with an antique dressing table with a mirror as the plot device.

The first segment is set in a brothel in ancient China. A courtesan is murdered and her blood spills onto the mirror on her dressing table.

The second segment is set in Shanghai in 1922. Mary, a disabled heiress to a large mansion, is given an antique dressing table by her cousin as a birthday gift. She notices that there is something strange about the mirror and starts receiving eerie phone calls reminding her about her dark secrets in the past. She had an affair with a professor who was already married and had a family. In order to silence him and take over his mansion, she poisoned him to death. One night, she is confronted by her two servants, who reveal themselves to be actually the daughters of the professor she murdered. They avenge their father.

The third segment is set in Singapore in 1988 during the Ghost Festival. James, a solicitor, has a one-night stand with Lora and tries to get away in the morning but is stopped by her gangster-looking brother, Roman, who forces James to marry Lora. Lora moves into Jamess house and brings along an antique dressing table, which she says she inherited from her deceased parents. One day, a woman approaches James and offers him a million dollars to be the defence lawyer for her son, who has been accused of raping a lady and murdering her boyfriend. Overcome by greed, James ignores his conscience, defends the accused in court, and wins the case. James gets into a car accident later and his face is injured so badly that he has to undergo reconstructive surgery. When the bandages are removed, James is horrified to see that he now looks exactly like the rape victims dead boyfriend.

The fourth segment is set in Hong Kong in 1999. Mings grandmother and Mings cousin, Yu, are waiting for Ming to come home from his overseas studies. When Ming returns, he brings along his girlfriend, Judy, and announces his decision to marry Judy. Yu becomes very jealous because she is in love with Ming. One day, Ming and Judy purchase an antique dressing table from a shop and bring it home. Yu finds the mirror very weird and starts feeling uneasy and resentful. A puppy recently bought by Ming and Judy keeps barking at her. One night, the puppy is brutally killed and Mings grandmother goes missing on the following night. Mings grandmother is eventually found dead with her body dismembered. Yu becomes the murder suspect and is immediately arrested. Later, Ming brings Judy to in front of the mirror and tells her that he knows that she was actually the murderer and her true intention was to seize his grandmothers inheritance. Judy reveals her true colours and tries to kill Ming but misses her step and ends up impaling herself on a pair of scissors stuck to the dressing table.

The fifth segment is set in Taiwan in 2000. A woman approaches the antique dressing table and is shocked to see her eyeballs falling out from their sockets in her reflection in the mirror. The movie ends here.

==Cast==
: In order of appearance:
* Sherming Yiu as courtesan
* Xu Fan as Mary
* Li Zhirui as dumb girl Chen Shasha as Marys housekeeper
* Jack Neo as James
* Lynn Poh as Lora
* John Cheng as Roman
* Karen Tong as rape victim
* Lin Ruping as accuseds mother Raymond Wong as rape victims boyfriend
* Lillian Ho as Yu
* Law Lan as Mings grandmother
* Nicholas Tse as Ming
* Ruby Lin as Judy

== External links ==
*  
*  

 
 
 
 
 