Hum Ho Gaye Aapke
 
{{Infobox film
| name           = Hum Ho Gaye Aap Ke
| image          =
| image size     =
| alt            =
| caption        =
| director       = Agathiyan
| producer       = Ganesh Jain Ratan Jain
| writer         = Sunil Kumar Agrawal Ahathian
| narrator       =
| starring       = Fardeen Khan Reema Sen Suman Ranganathan
| music          = Songs:  
| cinematography = Ravi Yadav
| editing        =
| studio         =
| distributor    = Venus Records
| released       =  
| runtime        =
| country        = India Hindi
| budget         =
| gross          = Rs.16.4&nbsp;million 
| preceded by    =
| followed by    =
}}

Hum Ho Gaye Aapke is a 2001 Bollywood film, directed by Agathiyan and stars Fardeen Khan and Reema Sen in the lead roles. The film is known to be the Bollywood debut of Reema Sen. It was released on 13 August 2001. It is a remake of Tamil movie Gokulathil Seethai (1996).

==Cast==
*Fardeen Khan	 ... 	Rishi Oberoi   
*Reema Sen	... 	Chandni Gupta 
*Apurva Agnihotri	... 	Mohan Sachdev (as Apurva Agnihotri)
*Suman Ranganathan	... 	Nikki (as Suman Rangnathan)
*Sadashiv Amrapurkar	... 	Manager
*Suresh Oberoi	... 	Mr. Oberoi
*Mahesh Thakur	... 	Mandeep
*Anant Mahadevan
*Shammi	... 	Auntie
*Achyut Potdar	... 	Mohans Father
*Suhas Joshi	... 	Mohans Mother
*Kinjal Joshi	... 	Mohans Sister
*Neena Kulkarni	... 	Mrs. Gupta
*Meghna	... 	Chandnis Sister
*Ali Asghar	... 	Manjeet

==Soundtrack==
The soundtrack was composed by Nadeem Shravan and the lyrics were penned by Sameer. The soundtrack consists of 7 tracks. The songs were popular in 2001 and gained good mass popularity. Singers like Kumar Sanu, Udit Narayan, Alka Yagnik, Sonu Nigam & Sunidhi Chauhan  lent their voices for the soundtrack.
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Title !! Singer(s)!! Length
|-
| 1
| "Hum Ho Gaye Aapke"
| Kumar Sanu, Alka Yagnik
| 05:04
|-
| 2
| "Pehli Baar Dil Yun"
| Kumar Sanu, Alka Yagnik
| 05:35
|-
| 3
| "Abhi To Mohabbat Ka" 	
| Udit Narayan, Alka Yagnik
| 05:10
|-
| 4
| "Re Mama" 	
| Sunidhi Chauhan
| 04:58
|-
| 5
| "Der Se Hua (Male)"
| Kumar Sanu
| 04:42
|-
| 6
| "Der Se Hua (Female)"
| Alka Yagnik
| 04:40
|-
| 7
| "Ishq Hai Kya"
| Sonu Nigam
| 04:42
|}

==References==
 

==External links==
*  

 

 
 
 
 
 

 