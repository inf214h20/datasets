The Watcher (film)
{{Infobox Film
| name = The Watcher
| image = The Watcher theatrical poster.jpg
| image_size =
| caption = Theatrical poster
| director = Joe Charbanic
| producer = Christopher Eberts Elliott Lewitt Jeff Rice Clark Peterson
| writer = Darcy Meyers David Elliot  Clay Ayers
| narrator = Chris Ellis Keanu Reeves
| music = Marco Beltrami Michael Chapman
| editing = Richard Nord
| distributor = Universal Pictures 2000
| runtime = 97 min
| country = United States English
| budget = $30 million
| gross = $47,267,829 (Worldwide)   
| preceded_by =
| followed_by =
}} 2000 United American thriller thriller film directed by Joe Charbanic and starring James Spader, Marisa Tomei and Keanu Reeves. Set in Chicago, the film is about a retired FBI agent who is stalked and taunted by a serial killer.

==Plot==
Retired FBI Special Agent Joel Campbell (James Spader) lives in Chicago, where he is struggling to come to terms with his failure to capture a serial killer back when he was working in Los Angeles. Campbell attends therapy sessions with Dr. Polly Beilman (Marisa Tomei), but otherwise has no friends or social life.
 Chris Ellis), and comes to the conclusion that the same serial killer has arrived in Chicago. FBI Special Agent in Charge Ibby (Ernie Hudson) tries to persuade Campbell to return to the case, but he refuses.

One night Campbell receives a phone call from the killer, David Griffin (Keanu Reeves), who reveals that he followed Campbell to Chicago and wants to rebuild the "rapport" they once had. Griffin tells Campbell that he will send a photo of a girl in the morning, and Campbell has until 9:00 pm that night to find her. Campbell tells Ibby that he wants back in on the case, and his request is granted.

Campbell works together with Mackie and the rest of the team in getting the word out on finding the girl before the deadline. However, by the time Campbell gets her house number and calls, Griffin is already there, and has slit her throat. Griffin suggests they continue with a different girl. The next day similar events follow as Campbell and his team try to find the next girl before the 9:00 pm deadline. They corner and almost catch Griffin, but he manages not only to kill the second girl but also escape. Campbell is injured during the car chase.

The next day, another photo arrives, but it turns out to be the image of Lisa Anton (Yvonne Niami), Campbells former lover who was killed indirectly because of Griffin back in Los Angeles. Campbell goes to Lisas grave, where Griffin is waiting for him. Griffin explains that he has Beilman hostage somewhere, and only wants to talk with him. Campbell negotiates for Beilmans safety, and Griffin eventually agrees to bring Campbell to see her. During the drive, Griffin explains that he cares a great deal for Campbell, and considers him a "good friend". Campbell secretly uses his cell phone to call Mackie, cluing him in on the situation. Campbell is taken to the abandoned warehouse where Griffin knocks him out. Campbell is tied up in a chair while Griffin takes a wire and starts to strangle Beilman. Campbell distracts Griffin from his task by saying thank you. When Griffin asks Campbell to repeat himself, Campbell stabs him with a pen before shooting him with a double-barrelled shotgun. Campbell rescues Beilman and gets them both to safety as the warehouse explodes, killing Griffin.

==Cast==
* James Spader as FBI Special Agent Joel Campbell
* Marisa Tomei as Dr. Polly Beilman
* Keanu Reeves as David Allen Griffin
* Ernie Hudson as FBI Special Agent in Charge Mike Ibby Chris Ellis as Detective Hollis Mackie
* Robert Cicchini as FBI Special Agent Mitch Casper
* Yvonne Niami as FBI Special Agent Lisa Anton

==Reception== Battlefield Earth.

===Box office===
The film opened at the top spot of the North American box office making $9,062,295 USD in its opening weekend. It had a 36% decline in gross earnings the following week but was enough to keep the film at the top spot. Its total domestic gross was  $28,946,615.

==Production==
* Reeves has stated that he was not interested in the script but was forced into doing the film when a friend forged his signature on a contract. He performed the role rather than get involved in a lengthy legal battle. He was contractually prevented from disclosing this until twelve months after the films US release.  
* The Watcher was filmed between October and December 1999 on location in Chicago, Illinois and Oak Park, Illinois.
* This movie featured the 1996 hit "6 Underground" performed by Sneaker Pimps.

==References==
 
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 