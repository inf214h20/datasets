The Last Tempest
 
{{Infobox film
| name           = The Last Tempest
| image          = 
| caption        = 
| director       = Li Han-hsiang
| producer       = Run Run Shaw Duwen Zhou
| writer         = Li Han-hsiang
| starring       = Ti Lung
| music          = 
| cinematography = Luying He
| editing        = Hsing-lung Chiang
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}} Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Ti Lung as Emperor Kuang Xu
* Lisa Lu as Empress Dowager Cixi

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 