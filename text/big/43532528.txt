Ente Entethu Mathrem
{{Infobox film
| name           = Ente Entethu Mathrem
| image          = EnteEntethuMathrem.png
| caption        = Promotional Poster designed by Kitho
| director       = J. Sasikumar
| producer       =
| writer         = AR Mukesh Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Karthika Shalini|Baby Shalini Lalu Alex Johnson
| cinematography = NA Thara
| editing        = G Venkittaraman
| studio         = Bijis Films
| distributor    = Bijis Films
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Baby Shalini and Lalu Alex in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
*Mohanlal as Menon Karthika as Sheela
*Shobana as Ambili Baby Shalini as Sreemol, Bindu (double role)
*Lalu Alex as Gauthaman
*Mala Aravindan as Sathyasheelan Innocent as Vakkachan
*Sukumari as Padmavathi/Sheelas mother
*Valsala Menon as Mother Superior Santhakumari as Kalyaniyamma
*Lalithasree as Rudrani
*James as Mathai

==Soundtrack== Johnson and lyrics was written by RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaromal kunjurangu || P Susheela || RK Damodaran || 
|-
| 2 || Nin Mounam || MG Sreekumar || RK Damodaran || 
|-
| 3 || Ponninkudam || Lathika || RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 