Allari Pidugu
{{Infobox film
| name           = Allari Pidugu
| image          = Allari pidugu.jpg
| caption        = Theatrical release poster
| writer         = Paruchuri Brothers  
| producer       = M. R. V. Prasad
| director       = Jayanth C. Paranjee
| starring       = Nandamuri Balakrishna Katrina Kaif Charmy Kaur
| music          = Mani Sharma
| cinematography = Ajay Vincent
| editing        = Marthand K. Venkatesh
| studio         = P. B. R. Art Productions
| released       =  
| runtime        = 2:51:09
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Allari Pidugu ( ) is a 2005 Telugu cinema|Telugu, Action film produced by M. R. V. Prasad on P. B. R. Art Productions banner, directed by Jayanth C. Paranjee. Starring Nandamuri Balakrishna, Katrina Kaif, Charmy Kaur  in the lead roles and music composed by Mani Sharma. The film met with negative reviews from critics and recorded as flop at box office.

==Plot==
Major Charkavarthy (Puneet Issar), has two sons Ranjit and Giri (both played by Nandamuri Balakrishna|Balakrishna). Giri is the younger one, who is the village guy and in love with his cousin Subbalakshmi (Charmy Kaur). Ranjit turns into an ACP and meets his match Swati (Katrina Kaif). G.K. (Mukesh Rishi) is a member of the Parliament, whose anti-social activities are contained by Ranjit. Also, Chakravarti who spends 14 years in prison after falsely charged by G.K. in a case is released. G.K. plans to take revenge on the family. But, the person who comes to the rescue of the family and the people threatened by the villain’s plans is Giri. The story is on how he ends as winner, by saving lives and winning respect from his father.

==Cast==
{{columns-list|3|
* Nandamuri Balakrishna as ACP Ranjit Kumar & Giri (Dual role)
* Katrina Kaif as Swati
* Charmy Kaur as Subbalakshmi
* Mukesh Rishi as G.K.
* Puneet Issar as Major Chakravarthy
* Rahul Dev 
* Kota Srinivasa Rao 
* Paruchuri Venkateswara Rao 
* Tanikella Bharani 
* Ahuti Prasad 
* Chalapathi Rao 
* Vijaya Rangaraju 
* Subbaraju 
* Benarjee 
* Raghu Babu 
* Raghunath Reddy 
* Subbaraya Sharma 
* Thotapalli Madhu  AVS
* Pruthvi Raj
* Ananth 
* Gundu Hanumantha Rao  Sumitra 
* Kavitha 
* Geetha Singh 
* Vimala Sri 
* Rathi 
* Jaya Vani 
* Ramya Chowdary 
}}

==Soundtrack==
{{Infobox album
| Name        = Allari Pidugu
| Tagline     = 
| Type        = film
| Artist      = Mani Sharma
| Cover       = 
| Released    = 2005
| Recorded    = 
| Genre       = Soundtrack
| Length      = 31:17
| Label       = Aditya Music
| Producer    = Mani Sharma
| Reviews     =
| Last album  = Athadu   (2005)  
| This album  = Allari Pidugu   (2005)
| Next album  = Jai Chiranjeeva   (2005)
}}

Music composed by Mani Sharma. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 31:17
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Dikki Dikki
| lyrics1 = Bhaskarabhatla Chitra
| length1 = 5:44

| title2  = Mallellona Illaira
| lyrics2 = Veturi Sundararama Murthy Sujatha
| length2 = 4:41

| title3  = Chinukulaaga
| lyrics3 = Kandikonda
| extra3  = Ranjith (singer)|Ranjith, Suchitra 
| length3 = 5:19

| title4  = Maa Subbalachamma
| lyrics4 = Veturi Sundararama Murthy SP Balu, Sunitha
| length4 = 5:03

| title5  = Ongolu Githaro
| lyrics5 = Jaladanki 
| extra5  = SP Balu, Mahalakshmi Iyer
| length5 = 4:57

| title6  = Nede Eenade
| lyrics6 = Sahiti  
| extra6  = Mallikarjun,Sri Vardhini 
| length6 = 5:18
}}

==Others== Hyderabad

==External links==
*  

 

 
 
 


 