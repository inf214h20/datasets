Bhairava Dweepam
{{Infobox film
| name           = Bhairava Dweepam
| image          = Bhairava Dweepam.jpg
| caption        =
| writer         = Raavi Kondala Rao  
| story          = Singeetam Srinivasa Rao
| screenplay     = Singeetam Srinivasa Rao
| producer       = B. Venkatarami Reddy
| director       = Singeetam Srinivasa Rao Rambha
| music          = Madhavapeddi Suresh
| cinematography = Kabir Lal
| editing        = D. Raja Gopal
| studio         = Chandamama Vijaya Combines
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Rambha in the lead roles and music composed by Madhavapeddi Suresh, which remained a chartbuster. The film recorded as Super Hit at box office.

==Plot==
  Vijay Kumar) of the Chandraprabha Dynasty deserts Vasundhara (K. R. Vijaya), a woman who gives birth to his son. Vasundhara,  loses the child in a cyclone while crossing a river. She is fouf the hermitage revives her. Vasundhara, on learning of the loss of her son attempts suicide. Concerned, Jamadagni creates a mystical flower on a bush, and tells Vasundhara that the flower will be in fresh and in bloom as long as her son is alive and well. Meanwhile, the child is found by tribals and taken to their village, Kotala Kona. The village chieftain and his wife adopt the child and name him Vijay (Nandamuri Balakrishna).

Once Vijay and Kondanna (Babu Mohan) goes for a tree of water, which gives immortality. From there Vijay hears a song by a princess Padma Devi (Roja Selvamani) in a Vasanta Vanam (Garden).  He admires her beauty and escapes from the scene. The soldiers report this incident in the court of Brahmananda Bhupati (Kaikala Satyanarayana) of Kartikeya Dynasty. Vijaya intrudes into the castle to the see Padma secretly. He speaks with padma and escapes from the kings castle successfully with his sword fight skill. As a result, Padma falls in love with Vijay.

Brahmananda Bhupati invites the young warriors to Swayamvara. Vijay and Kondanna arrives to the castle in disguise. At the same time Uttar (Giri Babu)  &  Dakshin (Subhalekha Sudhakar), sons of Jayachandra Maharajas second wife too arrive to the castle. Confused Brahmananda Bhupati allows both parties to stay in the castle for one day. After a song with Padma, Vijays disguise is revealed in the castle.

In a very far away Island, Bhairava, a wizard – who aims at winning immortality, is performing kshudra puja for the giant sculpture of dark goddess. Wanting to sacrifice a virgins blood for the dark goddess, at a night, with his magic, he brings Padma, along with the bed, to the Island. In the trance Padma gives a word to the dark goddess that she will come back on the next full moon day for her offering. The same night, Bhairava sends Padma back her castle.

Next morning Brahmananda Bhupati calls in a court doctor to medicate Padma. The doctor reveals that she was affected by black magic.  Brahmananda Bhupati is made to mistake that a tribal man like Vijay could have performed such black magic. Vijay is secured with the chains and brought into the castle by the soldiers. When Brahmananda Bhupati reveals that Padmani was taken ill, Vijay unchains himself and meets Padma in the castle. Padma reveals what happened at that full moon night. Vijay escapes from the chase of soldiers, but falls unconscious at a place near Jamadagni Ashram. Vasundhara and few men see Vijay in unconsciousness.  After the death of Queen, Uttar and Dakshin grabs the throne of Chandraprabha Dynasty, by leaving their father alone in a desert.

In Jamadagni Ashram Vijay comes out from unconsciousness. Vasudhara and Vijay could not recognize to each other. Vijay reveals the princess Padma has a life threat on coming full moon night. Vasudhara blesses Vijay, by tying a magical rope to his arm. The wizard fog comes again into the castle to put all into unconscious. Vijay sees Padma along with her cot is magically flying into the air. He jumps and hangs to a side of the flying bed.  He is pulled down by some roots which come up from a cave near to the Island of Bhairava. Vijay slays the roots which results a hermit cursed nymph to regain her form. As a gratitude, the nymph blesses Vijay with a magical ring and reveals that Bhairava is about to sacrifice Padma on a full moon night.

Vijay enters the cave in which the Bhairava is performing Kshudrapuja to the dark goddess. Bhairava advices Padma to have a bath and wear sacrificial costume.  At the pool in the cave Vijay brings back Padma from the trance. With the help of the magical ring, Vijay takes Padma on the bed, whilst Bhairava is performing puja facing towards the dark goddess. Bhairava sends a two headed dragon to stop Vijay taking Padma on the Cot. The dragon separates Vijay from the cot, but the bed reaches the castle. Vijay kills the dragon in the air. The dragon bursts out and Vijay is dropped down into the sea.

A couple of naughty devils (Suthi Velu) and (Kovai Sarala) finds Vijay falling unconscious on the desert shore of the sea. With the magic, the devils bring Vijay to consciousness. Their story was that they were devils in the court of Bhairava. They stood against the evil sorcery, for why Bhairva stuffed them in a bottle and threw the bottle at the shore of the sea, sharing a border with the desert. They were redeemed out when a blind man had kicked the bottle. To feed the hungry blind man, they grab the fruits from Tuumburadeva (God of music) temple. When the blind man is about to eat the plate of fruits,  a white flying horse prevents him eating by kicking the plate. Thence the devils are making him to eat, but the horse is preventing him to eat by kicking the plate.  The devils plead Vijay to convince the horse and make the blind man to eat the food, so that they can take the leave. Vijay with his art of music convinces the horse. With Vijays plead to the horse, the blind man regains his previous form of king.

The naughty devils reveals to Vijay that Sata-ratna (100 gem) Necklace found in Yakshini Loka, would protect Padma from the threat of Bhairava. Vijay directs the king to Kotala Kona. Brahmananda Bhupati announces that he would give half of the dynasty along with his daughter in marriage, to the one who saves his daughter. As directed by the naughty devils Vijay travels towards the east and reaches Yakshini Loka, where he meets Lilliputs. In a comic incident Vijay saves Lilliputs. The Lilliputs help Vijay to reach Yakshini Loka and shows the locked room in which the necklace is placed. A Yakshini (Rambha (actress)|Rambha) start to dance on seeing Vijay, whilst the Lilliputs begin to find the key for opening the door. In the dance Vijay finds the key tied to the ankle of Yakshini. In false romance with Yakshini, the Lilliput steals the key. Vijay enters into the room by facing hurdes and then enters a room of glasses. In the room of glasses he fights with a dreadful monster, which has life in the glasses.  The case of necklace appears upon Vijay killing the monster by breaking the glasses.  Yakshini curses Vijay who is rushing out with the necklace. To the curse Vijay totally loses his handsomeness and turns into a very ugly man. Yakshini reveals that the necklace will lose its power if either it is thrown away onto ground or if Vijay reveals out any one who he is.

On a full moon night, Vijay, in the form of an ugly man, enters the castle with the necklace and pleads Padma to wear it. Padma and others could not recognize who the ugly man is. In the panic, Padma wears the necklace, which drives off the death fog entering the castle. Brahmananda Bhupati remembers of his promise and arranges for the marriage between the ugly man and Padma. Bhairava sends a devil in the disguise of a priest by name Mattepa Sastri (Basavaraju Venkata Padmanabha Rao|Padmanabham) to upset the marriage. Mattepa Sastri says that the necklace was stolen by the ugly man from Vijay. In anger Padma throws the necklace down. As a result, the necklace loses its power and Mattepa Sastri vanishes from there along with Padma and appears at Bhairavudu. Brahmananda Bhupati realizes that the ugly man was Vijay.

Vijay returns to Jamadagni Ashram to meet Vasundhara. The ugly man reveals his flashback. Vasundhara realizes that the ugly man to be her son and reveals that Chandraprabha Maharaja to be his father. Vijay invokes the horse. Vasundhara prays for a goddess and takes all the ugly of her son. Vijay regains his shape to her prayer and goes to the Bhairava Dweepa on the white horse to save Padma from the sacrifice. In the fight Vijay slays the head of Bhairava with the sacrificial sword. Bhairava dies and it results in the collapse of the dark goddess along with the ruin of the cave. Vijay escapes out with Padma.  The nymph appears again and regains the shape of his mother. Vijay reaches the castle with Padma. The movie ends with the happy marriage of Padma with Vijay and the union of Chandraprabha Dynasty and Kartikeya Dynasty.

==Cast==
 
*Nandamuri Balakrishna as Vijay
*Roja Selvamani as Padmavathi Rambha as Yakshini Satyanarayana as Brahmananda Bhupati Vijayakumar as Jayachandra Maharaju
*Vijaya Rangaraju as Bhairavudu
*Subhalekha Sudhakar as Dakshin
*Giri Babu as Uttar
*Babu Mohan
*Mikkilineni as Jamadagni Maharshi Padmanabham as Mattepa Shastri 
*Malladi
*Suthi Velu as Naughty devil
*Vinod as Shoora Varma
*Bheemiswara Rao Chitti Babu as Lilliput
*Garimalla Visweswara Rao as Lilliput
*K. R. Vijaya as Vasundhara Manorama
*Sangeeta Sangeeta as Padmas mother
*Radhabai
*Rajita as Madanika
*Kovai Sarala as Naughty devil
*Athili Lakshmi
*Syelaja
 

==Soundtrack==
{{Infobox album
| Name        = Bhairava Dweepam 
| Tagline     = 
| Type        = film
| Artist      = Madhavapeddi Suresh 
| Cover       = 
| Released    = 1994
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:17
| Label       = Supreme Music
| Producer    = Madhavapeddi Suresh 
| Reviews     =
| Last album  = Madam   (1993)  
| This album  = Bhairava Dweepam   (1994)
| Next album  = Maatho Pettukoku   (1995)
}}

Music composed by Madhavapeddi Suresh. All songs are blockbusters. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:17
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Yentha Yentha Vintha Mohamo Sirivennela Sitarama Sastry SP Balu,Sandhya
| length1 = 5:46

| title2  = Ghataina Prema Ghatana
| lyrics2 = Sirivennela Sitarama Sastry Chitra
| length2 = 3:54

| title3  = Sri Thumbura Naaradha
| lyrics3 = Veturi Sundararama Murthy 
| extra3  = SP Balu
| length3 = 5:51

| title4  = Virisinadhee Vasanthagaanam
| lyrics4 = Singeetham Srinivasa Rao
| extra4  = Chitra
| length4 = 4:59

| title5  = Narudaa O Narudaa
| lyrics5 = Veturi Sundararama Murthy 
| extra5  = S. Janaki
| length5 = 4:34

| title6  = Ambaa Shambavi  
| lyrics6 = Vadepalli Krishna  
| extra6  = S. Janaki
| length6 = 4:02
}}

===Music===
* "Ambaa Shambavi Bhadraraja ghamana Kali Hymavatishwari Trinayana"
:(Lyricist: Vadepalli Krishna; Music: Madhavapeddi Suresh; Singer: S. Janaki)
* "Ghataina Prema Ghatana Deetaina Neti Natana"
:(Lyricist: Sirivennela Sitaramasastri; Music: Madhavapeddi Suresh; Singers: S.P. Balasubramaniam, K. S. Chitra)
* "Narudaa O Narudaa Yemi Korika" "Koruko Kori Cheruko Cheri Yeluko Baalakaa"
:(Lyricist: Veturi Sundararama Murthy; Music: Madhavapeddi Suresh; Singer: S. Janaki)
* "Sri Thumbura Naaradha Nadamrutam" "Swara Raga Rasa Bhava Talanvitam"
:(Lyricist: Veturi Sundararama Murthy; Music: Madhavapeddi Suresh; Singer: S.P. Balasubramaniam)
* "Virisinadhee Vasanthagaanam Valapula Pallakigaa"
:(Lyricist: Singeetham Srinivasa Rao; Music: Madhavapeddi Suresh; Singer: K. S. Chitra)
* "Yentha Yentha Vintha Mohamo" "Ratikantuni Srungara Mantramo"
:(Lyricist: Sirivennela Sitaramasastri; Music: Madhavapeddi Suresh; Singers: S.P. Balasubramaniam, Sandhiya)

==Awards==
* This film won the Nandi Award for Best Feature Film for 1994.
* Singeetam Srinivas Rao won the Nandi Award for Best Director in 1994.
* S. P. Balasubrahmanyam won Nandi Award for Best Male Playback Singer for the song "Sri Thumabara Narada" in 1994.
* Filmfare Award for Best Actor – Telugu - 1994.
* S. Janaki won the Nandi Award for Best Female Playback Singer for the song "Naruda O Naruda Yemi Korikaa" in 1994.
* Kabir Lal won Special jury Award by the Andhra Pradesh Govt.

==Others== Hyderabad

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 