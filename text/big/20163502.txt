2 Harihar Nagar
{{Infobox film
| name           = 2 Harihar Nagar
| image          = Harihar Nagar 2.jpg
| alt            =  
| caption        =  Lal
| producer       = P. N. Venugopal
| writer         = Lal Mukesh Jagadish Siddique Ashokan Ashokan Lakshmi Rai
| music          = Alex Paul Venu
| editing        = V. Sajan
| studio         = 
| distributor    = Lal Releases (India) PJ Entertainments (Europe)
| released       =  
| runtime        = 136 minutes
| country        = India
| language       = Malayalam
| budget         = 4 crore
| gross          = 20 crore
}} Malayalam film Lal and produced by P N Venugopal. It is a sequel to In Harihar Nagar (except the first scene were the four meet each other) and a prequel to In Ghost House Inn and John Honai. The films chronicle the story of four very close friends Mahadevan (Mukesh (actor)|Mukesh), Govindan Kutty (Siddique (actor)|Siddique), Appukuttan (Jagadish), and Thomas Kutty (Ashokan (actor)|Ashokan). The film ended up as a blockbuster in the Kerala box office.

==Plot==
The movie starts with a flashback, in front of a church, in 1980 when the four main characters were children. This scene the beginning of their friendship. Thomas Philip (a.k.a. Thomas Kutty) loses money to a scam artist, who responds to his complaints with violence. He is rescued by the other three who respond to his calls of distress. They then invite Thomas Kutty to join their group who, above all, vow never to cheat on one another. The scene is transitioned by the quote "Thomas kutty Vittodaa!" (Thomas kutty, time to run!) once the scam artist brings his friends to return the beating he got. 

The movie transitions to the current time: Each of them are introduced again, 20 years later. Mahadevan is a Psychologist and an unhappily married father in the Middle East. Govindan Kutty is a Civil Engineer-cum-CEO whos happily married and settled in Kochi. Appukuttan (now Dr. Appukuttan Nair) is a dental specialist, living in Bombay who often fights with his wife and twins. The prologue show them preparing to travel back to Harihar Nagar to attend Thomas Kuttys wedding; he, after years of "enjoying" his bachelor life, has decided to settle down and marry an orphan who belonged to the same church as him. The prologue also highlights that none of them have really changed from their flirtatious ways, despite being married.

After arriving at Harihar Nagar, they settle into Govindan Kuttys old house at Harihar Nagar. Disappointed at how life has become sombre after growing up, they decide to revert to their younger selves for the rest of the trip. To jump start their "trip back to youthfulness," Mahadevan suggests that they should create problems in the neighborhood: According to him, its only with problems that one becomes youthfully energetic. 
 
While staying at Govindan Kuttys house, they are greeted with flowers left at the doorstep with a tag reading "Maya." Immediately thinking that this is their old Maya, they go to meet her. They barely miss her, but Appukuttan throws a rock and breaks her cars rear window. An anonymous woman steps out of the car. But it is not "their" Maya. They are immediately arrested. Later, "Maya" comes and gets them out of jail saying that they are her friends and didnt recognize it because she hadnt seen them in a long time. Once out, she says not to bother her anymore because of this. Maya lives right across from Govindan Kuttys house. Trying to find the weakness that will draw her attention, one night Appukuttan looks to the window and is frightened by the sight of a ghostly figure with a "burned" face. 

Thomas Kutty is not staying with the other three on that night, but he arrives the next morning. Mahadevan says a sentimental story to make Maya believe that theyre not here for wrong reasons. She later comes and visits them and becomes their friend. A hilarious sequence follows.

It is revealed that Maya is John Honais daughter, in cahoots with her evil brother (John was the villain in the first part). Thomas Kutty gets kidnapped days before his wedding and his three friends are suspected. They are followed by the police. Meanwhile Junior Honai reveals that he has Thomas kutty. When the threesome (with great difficulty) enters Honais hiding place they find Thomas Kutty. Honai demands the box filled with money which the old Maya had given them. The box is now a bank with each of the foursome knowing two digits of the password. The four escape, but now Appukuttan is missing. Maya is found and chided. But she tells her story full of dire circumstances. The correct code for the box sounds faulty. It seems that Appukuttan who is in the hands of Honai has forgotten his two digits.

After a lot of twists and turns they confround Honai who has a time bomb attached to a clinging lizard. The time bomb comically gets caught over each person. But then things get serious. The time bomb gets caught on Thomas kutty and Honai presses the ignition to burst in 30 seconds. Thomas Kutty runs to Honai in the last few seconds and pushes Honai and himself into the next room.The other three friends understood that Thomas Kutty cheated them for money.At last they again become best friends.

== Cast == Mukesh as Mahadevan Siddique as Govindan Kutty
*Jagadish as Appukuttan  Ashokan as Thomas Kutty
*Lakshmi Rai as Maya/Christiana Honai Lena as Parvathi Govindan Kutty
*Sudipto Balav as Freddy Honai Rohini as Mahadevans Wife 
*Narayanankutty
*Reena Basheer as Appukuttans Wife
*Rakhi as Thomas Kuttys Wife
*Appa Haja as Police Sub Inspector Cheriyan
*Kalabhavan Rahman as Constable
*Vineeth as Shyam
*Salim Kumar as Ayyappan Janardhanan as Vikariyachan
*Kochu Preman as Marriage Broker
*Cherthala Lalitha as Mother Superior
*Geetha Vijayan as Maya (guest appearance)
*Kaviyoor Ponnamma as Andrews mother (guest appearance)
*Rekha as Sister Josephine (guest appearance)
*Thrissur Elsy as Mahadevans mother (guest appearance)
*Atlas Ramachandran as himself (guest appearance)
*Revathi Sivakumar as Mahadevans Daughter(guest appearance)

==Release==
===Box office===
The film was released in 65 screens all over Kerala and had a successful opening, owing to the cult following of its prequel.   Its major opponent Sagar Alias Jacky Reloaded ended up as a box-office disaster. This was the second biggest grossing films of the year. . The satellite rights of the film was sold for an amount of   1.2 crores. 

===Critical reception===
The film mostly opened to high positive reviews from the critics. Sify  wrote " 2 Harihar Nagar is just what intelligent filmmaking is all about and, no wonder, this delightful comedy is a winner from the word go."  Rediff wrote "The main reason 2 Harihar Nagar works is because the performance of the foursome that Lal is able to extract, giving us a sense of deja vu" and gave it 3 stars out of 5.  Indiaglitz wrote "To Harihar Nagar definitely offers rib tickling comedy and delightful watch from the word goes, with apt mixing up of inexhaustible laughter and few moods of seriousness at intervals." 

==References==
 

==External links==
* 
* 

 
 
 
 