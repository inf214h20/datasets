Bangarwadi
{{Infobox film
| name           = Bangarwadi
| image          =
| image_size     =
| caption        =
| director       = Amol Palekar
| producer       = National Film Development Corporation of India Doordarshan
| writer         = Vyankatesh Madgulkar (screenplay and dialogues)
| narrator       =
| starring       = Chandrakant Kulkarni Chandrakant Mandare  Adhishree Atre Nandu Madhav Sunil Ranade Upendra Limaye
| music          = Vanraj Bhatia
| cinematography = Debu Deodhar
| editing        = Waman Bhosale
| distributor    =
| released       = 1995
| runtime        = 124 mins
| country        = India
| language       = Marathi
| budget         =
| gross          =
}} princely state of Aundh, during the 1940s.

Bangarwadi has drawn various accolades. It won 5 Maharashtra state awards in 1996, National Award 1995 – Best Feature Film in Marathi. The movie also has distinction of being screened at several international film festivals like Karlovy Vary International Film Festival 1996, Birmingham International Film Festival 1996, London Film Festival, UK 1996, 15th FAJR International Film Festival, Iran 1997, Cairo International Film Festival, Egypt 1996, Bagota International Film Festival, Columbia—1996 and the Sarajevo Film Festival, Prague 1996
 {{Cite web
| url = http://chapekar-parva.blogspot.in/2010/09/bangarwadi-teachers-struggle.html
| title = BANGARWADI - A TEACHER’S STRUGGLE
| accessdate = 21 July 2013
| author = 
| last = 
| first = 
| authorlink = http://www.blogger.com/profile/12763887425871937420
| coauthors = 
| date = 4 September 2010
| work = 
| publisher = 
| pages = 
| language = 
| quote = 
| archiveurl = 
| archivedate = 
}} 

==Plot==
The story begins with the young schoolteacher walking alone towards the village called Bangarwadi, across a deserted landscape. When the teacher reaches Bangarwadi, he finds that the school is not working and people are reluctant to send their children to school. The schoolteacher, with a support of Karbhari (village head) successfully convince the local people to send their children to school. Over the next months, he successfully runs the school. Teacher tries to help the illiterate and needy people of the village with all possible means, which sometimes land him into troubles. After successful run at school, he convinces the villager to set up a gymnasium through community participation. He invites the king of the state, Pant Pratinidhi for its inauguration. A sudden death of village head leaves village and schoolteacher shocked, this is followed by a prolong drought. The schoolteacher tries his best to get help from government with frequent letters describing the graveness of situation, but gets no response. Drought forces people of Bangarwadi to abandon the village, leaving the schoolteacher alone with no students.

==Cast==
*Chandrakant Kulkarni as a schoolteacher
*Chandrakant Mandare as Karbhari(village head)
*Adhishree Atre as Anji
*Nandu Madhav as Ananda Ramoshi
*Sunil Ranade as Aayub
*Nagesh Bhonsle as Daddu Baltya
*Hiralal Jain as Kakabu
*Kishore Kadam as Rama
*Upendra Limaye as Sheku

==Awards==
National Award for the best Feature Film in Marathi, 1995,   Kalnirnaya Award Best Film (1997), Filmfare Award (1997), for Best Film and Direction.

==References==
 

==External links==
*   title Bangarwadi review-New York Times]
*  

 
 

 
 
 


 