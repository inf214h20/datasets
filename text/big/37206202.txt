Sea Shadow (film)
{{Infobox film
| name           = Sea Shadow
| image          = Sea Shadow poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Nawaf Al-Janahi
| producer       = Daniela Tully Rami Yasin
| writer         = Mohammed Hassan Ahmed
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Omar Al Mulla Neven Madi Abrar Al Hamad
| music          = Ibrahim Al-Amiri
| cinematography = Paulo Ares
| editing        = Tony Ruthnam Raul Skopecz
| studio         = Image Nation
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = United Arab Emirates
| language       = Arabic AED ( )
| gross          = 
}}
Sea Shadow is a 2011 Emirati coming-of-age film written by Mohammed Hassan Ahmed and directed by Nawaf Al-Janahi. The film was the first from Image Nation to be filmed in the United Arab Emirates. The film premiered at the Abu Dhabi Film Festival in October 2011 and was released in theaters throughout the Gulf Arab states in November 2011. It premiered in the United States at the Palm Springs International Film Festival in January 2012.

==Synopsis==

Two Emirati teenagers, Mansoor and Kaltham, live in a small neighborhood by the sea. They face their families traditional values as they consider what paths to take as adults. 

==Cast==
{|cellspacing="0" style="padding-left: 1.5em;"
|- valign="top"
|
{| cellpadding="0" cellspacing="0"
|-
| Omar Al-Mulla
| &nbsp;… Mansoor
|-
| Neven Madi
| &nbsp;… Kaltham
|-
| Arbar Al-Hamad
| &nbsp;… Sultan
|-
| Aisha Abdul Rahman
| &nbsp;… Umm Mansoor
|}
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|
{| cellpadding="0" cellspacing="0"
|-
| Bilal Abdullah
| &nbsp;… Abu Mansoor
|-
| Ahmad Iraj
| &nbsp;… Jasim
|-
| Hasan Rajab
| &nbsp;… Abu Jasim
|-
| Ali Al-Jabri
| &nbsp;… Shahid
|}
|}

==Production==

Sea Shadow is written by Mohammed Hassan Ahmed and directed by Nawaf Al-Janahi, who directed the short film Mirrors of Silence and the feature film The Circle. Al-Janahi read the screenplay for Sea Shadow in late 2009 and was considering directing it when Imagination Abu Dhabi (now Image Nation), impressed with The Circle, approached him with the opportunity to direct.  The company, founded in 2008, developed and financed Sea Shadow as its first film to be produced in the United Arab Emirates.     Reuters reported that Sea Shadow deals with the theme of modernization among Gulf Arab societies. The story moves from Ras al-Khaimah through Dubai to Abu Dhabi. Screenwriter Ahmed said, "The aim is to reflect a completely Emirati atmosphere, from the towers, buildings and modern cities to the old quarters where Emiratis still live."    With a production budget of 3.67 million UAE dirham|AED,  filming took place in October and November 2010. 

==Release==

The film had its world premiere on  , 2011 at the Abu Dhabi Film Festival. It was theatrically released on  , 2011 across the United Arab Emirates, Qatar, Kuwait, Oman and Bahrain.  Sea Shadow made its United States premiere on  , 2012 at the Palm Springs International Film Festival.  The film was released on DVD in the United Arab Emirates on  , 2013. 

==Critical reception==

Mark Adams at Screen Daily called Sea Shadow "a gentle and delightfully shot" film. He said it lacked "real drama" but made up for it "in terms of charming performances and deep-rooted sense of honesty".  Jay Weissberg at Variety (magazine)|Variety said the film did not connect scenes well enough. He said of the main characters, "Scenes between the two friends are the strong point here, but simple dialogue crosses the line from unsophisticated to simplistic, and the pic rarely comes alive." 

==References==
 

==External links==
* 
* 

 
 
 
 
 