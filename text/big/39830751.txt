Annie (2014 film)
 
{{Infobox film
| name           = Annie
| image          = Annie2014Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Will Gluck
| producer       = {{plainlist|
* Will Smith
* Jada Pinkett Smith Shawn "Jay-Z" Carter
* Caleeb Pinkett
* James Lassiter
* Jay Brown
* Tyran Smith
}}
| writer         = {{plainlist|
* Will Gluck
* Aline Brosh McKenna
}}
| based on       =   and   
| starring       = {{plainlist|
* Jamie Foxx
* Quvenzhané Wallis
* Rose Byrne
* Bobby Cannavale
* Cameron Diaz
}}
| music          = Charles Strouse Greg Kurstin 
| cinematography = Michael Grady
| editing        = Tia Nolan
| studio         = {{plainlist|
* Village Roadshow Pictures
* Overbrook Entertainment
}}
| distributor    = Columbia Pictures
| released       =  
| runtime        = 118 minutes    
| country        = United States
| language       = English
| budget         = $65 million    
| gross          = $133 million 
}} musical comedy-drama Sony Pictures contemporary adaptation Broadway musical of the same name, which was in turn based upon the 1924 comic strip Little Orphan Annie by Harold Gray, the film stars Quvenzhané Wallis, Jamie Foxx, Rose Byrne, Bobby Cannavale, and Cameron Diaz.
 theatrical film television film, Annie began production in August 2013 and opened on December 19, 2014    to generally negative reviews, but was a box-office success, grossing over $132 million.
 Best Actress Best Original Worst Supporting Actress.

==Plot==
  Annie Bennett lives in foster care with several other girls in the care of the cruel Colleen Hannigan. She spends each Friday waiting outside the restaurant where she believes her parents will return to collect her.
 germaphobe who doesnt connect to commoners well, and is losing badly. The rescue goes viral on the internet and Stacks numbers spike. Stacks campaign manager Guy Danlily suggests that he invite Annie to live with him as means to further boost his poll numbers. Stacks reluctantly agrees, but over time, develops true affection for Annie and his assistant Grace Farrell, and plans to adopt her.

After a disastrous public appearance, Guy Danlily (with the help of Miss Hannigan) arranges to have Annie claimed by impostors pretending to be her parents. But as they enact their plan, Guy betrays Miss Hannigan, who starts having second thoughts. Around the same time, Annie soon learns that the impostors are not her parents, and she is being kidnapped. Hannigan confesses Guys scheme to Stacks, who then fires Guy.

After a chase across Manhattan, Annie is rescued and Stacks quits the election to prove that he does care for Annie, reuniting them both.

==Cast== Annie Bennett, a foster child who lives in a foster home who desires to search for her parents. Oliver Warbucks.
* Rose Byrne as Grace Farrell, Stacks faithful personal assistant and Annies mother figure.
* Bobby Cannavale as Guy Danlily,  a "bulldog political adviser" to Stacks.
* Cameron Diaz as Miss Colleen Hannigan, the cruel control freak of the foster home where Annie resides. She is based on Agatha Hannigan.
* Adewale Akinnuoye-Agbaje as Nash, "the tough but lovable bodyguard and driver for Stacks and a good friend of Annie." He evokes the traits of Punjab and The Asp. 
* Tracie Thoms and Dorian Missick as Annies "fake parents", based on the characters Lily St. Regis and Rooster Hannigan. 
* David Zayas as Lou, the local bodega owner who is a friend of Annie and has a crush on Miss Hannigan. He evokes the traits of the laundryman Mr. Bundles. 
 
* Nicolette Pierini as Mia Putnam, the smallest foster girl.
* Amanda Troya as Pepper Ulster, the bossiest foster girl.
* Eden Duncan-Smith as Isabella Sullivan,  the oldest of Annies foster sisters.
* Zoe Margaret Colletti as Tessie Dutchess, one of Annies foster sisters.
* Stephanie Kurtzuba as Mrs. Kovacevic, the New York Family Services worker who becomes close with Annies case.
* Taryn Gluck as Street Rat #1
* Alexandra Gluck as Street Rat #2
* Victor Cruz as Teacher

===Cameos===
* Scarlett Benchley as Mermaid (Satana)
* Patricia Clarkson as Focus group woman
* Michael J. Fox as himself
* Mila Kunis as Andrea Alvin
* Ashton Kutcher as Simon Goodspeed
* Bobby Moynihan as Guy in bar
* Rihanna as Moon Goddess Sia as Animal Care & Control Volunteer

Phil Lord and Christopher Millers names appear in the end credits of MoonQuake Lake

==Musical numbers==
 
While the film incorporates notable songs from the original Broadway production, written by composer   style. Lyrics to some songs were also updated to reflect the differences in the films storyline and settings.  Sia and Kurstin wrote three new songs for the soundtrack, including "Opportunity", "Who Am I", and "Moonquake Lake". Sia additionally co-wrote "The Citys Yours" with Stargate (production team)|Stargate.  

 
# "Overture"
# "Maybe" – Annie, Tessie, Mia, and Isabella
# "Its the Hard Knock Life" – Annie, Tessie, Mia, Isabella, and Pepper
# "Tomorrow (song from Annie)|Tomorrow" – Annie
# "I Think Im Gonna Like It Here" – Annie, Grace, and Mrs. Kovacevic
# "Little Girls" – Miss Hannigan
# "The Citys Yours" – Will and Annie
# "Opportunity" – Annie
# "Easy Street" – Guy and Miss Hannigan
# "Who Am I?" – Miss Hannigan, Will, and Annie
# "I Dont Need Anything But You" – Will, Annie, and Grace
# "Tomorrow/I Dont Need Anything But You" (Finale) – Cast

==Production== Sony first Willow was Ryan Murphy became front-runner to direct the film,  but by March, he had declined. 

The production soon began seeking a screenwriter, and actress Emma Thompson was considered.  No developments arrived until May 2012, when Will Smith appeared on Good Morning America and provided updates, including that the film would be set in modern-day New York City, that Thompson was providing a script, and that Jay-Z would also provide newly written songs for the film.  In July 2012, We Bought a Zoo screenwriter Aline Brosh McKenna wrote a second draft of the script.  In August, it was announced production was to begin in Spring 2013. 

In January 2013, Easy A director Will Gluck was hired to direct, but Willow Smith had dropped out. 

===Casting=== Oscar nominee Quvenzhané Wallis had replaced Smith in the lead role,  and the film had scheduled a Christmas 2014 release. 

In March 2013, the search for the rest of the cast continued, and Justin Timberlake was rumored for the role of Daddy Warbucks.  This was proven false when Jamie Foxx signed on for the role, now named Will Stacks.  In June 2013, Cameron Diaz was cast as Miss Hannigan, after Sandra Bullock declined. 

In July 2013, Rose Byrne joined the cast as Grace Farrell, Stackss faithful assistant  and in August, Boardwalk Empire star Bobby Cannavale joined the cast as a "bulldog political adviser" to Will Stacks.  In September, the rest of the cast was announced: Amanda Troya, Nicolette Pierini, Eden Duncan-Smith, and Zoe Colletti as Annies foster sisters. 

As of September 19, 2013, principal photography had begun.   Shooting was done at Grumman Studios.  Other scenes were filmed at the new Four World Trade Center.

==Changes from prior adaptations==
 
  lower class.   

The character of Oliver Warbucks was modified to create William Stacks, an entrepreneur in the technology sector (particularly, the mobile phone industry) turned politician, who is trying to run for Mayor of New York City. Annie also no longer lives in an orphanage, but is kept in foster care.    Miss Hannigans first name is changed to Colleen, instead of her previous film name Agatha. While Hannigan is complicit in deceiving Stacks and Annie that Annies birth parents have been found (conspiring with Sparks campaign mananger Guy Danlily), they are not impersonated by Hannigans brother Rooster and his Girlfriend Lily as in the original version. Instead, Guy has people he uses for this kind of work take Annie. Hannigans character is also softened from her prior appearances, to the point of experiencing guilt over her part in separating Stacks and Annie, and even helping to rescue Annie from her false parents in the films finale. Annies dog  Sandy is a female in this film, as opposed to past adaptations where the dog is a male.

==Release==
The film officially premiered at the Ziegfeld Theater in New York City on December 7, 2014. 

===Piracy=== breach of Columbias parent company Sony Pictures Entertainment. Within three days of the initial leak, Annie had been downloaded by an estimated 206,000 unique IPs.    By December 9, the count had risen to over 316,000. The chief analyst at Boxoffice (magazine)|BoxOffice.com felt that despite this, the leak was unlikely to affect Annie s box office performance as the demographic who pirates movies isnt the target audience for the film. 

===Box office===
Annie opened on December 19, 2014, and earned $5,289,149 on its opening day. In the first weekend, the film made $15,861,939, ranking third in the domestic box office behind other new releases   and  .  The film grossed $85.9 million in North America and $48.7 million overseas for a worldwide total of $134.6 million.   

===Critical reception===
Annie has received negative reviews among critics. On Rotten Tomatoes, a review aggregator, the film has a 27% approval rating based on 139 reviews; the average score is 4.4/10. The sites consensus reads, "The new-look Annie hints at a progressive take on a well-worn story, but smothers its likable cast under clichés, cloying cuteness, and a distasteful materialism."  On Metacritic, the film has a score of 33 out of 100, based on 38 critics, indicating "generally unfavorable reviews".  Audiences rated the film an A- on CinemaScore. 

  of the   and the present, and the populist message, however overstated, always registers as sincere." Sachs also praised director Will Gluck for "striking a buoyant tone that feels closer to classic Hollywood musicals than contemporary kiddie fare."   

The soundtrack, rearranged by Sia and Greg Kurstin, received a polarizing response from critics, with much criticism going towards the heavy use of auto-tune. Entertainment Weekly described its soundtrack as an auto-tuned "disaster", noting that "you wont ever hear a worse rendition of Easy Street than the one performed by Diaz and Cannivale — I promise".    David Rooney of The Hollywood Reporter says "all but a handful of the existing songs have been shredded, often retaining just a signature line or two and drowning it in desperately hip polyrhythmic sounds, aurally assaultive arrangements and inane new lyrics."    Matt Zoller Seitz however, praised the soundtracks new songs.   

The performances, however, were more positively received by some film critics. IGN.com praised Wallis and Foxx for being "on-point" throughout much of the film, as well as Rose Byrne, calling her the "surprise" of the film.    Matt Zoller Seitz called Wallis "the first Annie to bring something both culturally and personally new to this role", and praised the rest of the cast too, including Foxx and Byrne.  However, Cameron Diazs performance was widely panned, with critics calling it "vampy",  as well as "strident and obnoxious".  Peter Travers of Rolling Stone (magazine)|Rolling Stone says that she "overacts the role to the point of hysteria".   

===Accolades===
{| class="wikitable plainrowheaders" 
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|-
! scope="row" rowspan="2"| Golden Globe Award  January 11, 2015 Best Lead Actress in a Comedy or Musical Motion Picture
| Quvenzhané Wallis
|  
|- Best Original Song
| "Opportunity" – Greg Kurstin, Sia Furler, Will Gluck
|  
|-
! scope="row"| NAACP Image Award  
| February 6, 2015 Outstanding Actress in a Motion Picture
| Quvenzhané Wallis
|  
|- 20th Critics Broadcast Film Critics Association Awards   January 15, 2015   Best Young Actor/Actress
| Quvenzhané Wallis 
|  
|-
! scope="row" rowspan="2"| Golden Raspberry Award  February 21, 2015 Worst Prequel, Remake, Rip-off or Sequel
| Annie
|  
|- Worst Supporting Actress
| Cameron Diaz
|  
|-
! scope="row" rowspan="3"| Nickelodeon Kids Choice Award  March 28, 2015
| Favorite Movie Actress
| Cameron Diaz
|  
|-
| Favorite Villain
| Cameron Diaz
|  
|-
|}

===Home media===
Annie was released on DVD and Blu-ray Disc|Blu-ray/DVD combo pack on March 17, 2015. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 