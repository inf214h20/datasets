Wait 'til This Year
{{Infobox film
| name           = Wait til This Year
| image          = Wait Till This Year.jpg
| image_size     =
| caption        = DVD cover of Wait til This Year
| director       =
| producer       = Monika Lahiri
| writer         =
| narrator       =
| starring       = Monika Lahiri
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2004
| runtime        =
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Wait til This Year is a reality-type docu-drama film which follows a Boston Red Sox fan during the 2004 baseball season, when the team ended their famous losing streak by winning the World Series.
   The Boston Globes Boston.com, January 13, 2005. 
The film was aired on New England Sports Network (NESN) and then released on DVD afterward.    The Boston Globes Boston.com, November 17, 2005.
 
Monika Lahiri starred (as the "character" Monika) and produced the film, which combines reality, documentary and scripted scenes when following the events from the perspective of Monika. 
 Yankees fan, and one a Sox fan. 

==Cast==
The "cast" includes those playing scripted fictional roles, while also including persons who appear as themselves, such former Red Sox star players, who made cameo appearances as themselves.
*Monika Lahiri - Monika, lead character and wife of Jes.  ; URL last accessed April 9, 2006. 
*Ges Selmont - Ges, husband of Monika. 
*Chris Kies	- Rob, a Sox fan, and friend of Alex. 
*Stephen Kunken - Alex, a Yankees fan, and friend of Rob.  WTIC FOX 61  
*Tony Terzi - Himself, sports reporter  
*Jim Rice - Himself, former Sox player. 
*Dwight Evans - Himself, former Sox player.  Bill Lee - Himself, a former Sox player  
*Carl Yastrzemski - Himself, former Sox player. 
*Julie Dubela - Herself, national anthem singer   

==References==
 

 
 
 
 
 
 


 