Dragon from Russia
 
 
 
 
{{Infobox Film
| name = Dragon from Russia 紅場飛龍
| image = Dragoncover.jpg
| image_size = DVD cover
| caption = DVD front cover
| director = Clarence Fok
| producer = Dean Shek
| writer = Ryoichi Ikegami Kazuo Koike Nina Li Chi Carrie Ng Loletta Lee Dean Shek
| music =
| cinematography = Peter Ngor Chi-kwan
| editing = Ming Lam Wong
| distributor = Cinema City Company

Mei Ah Entertainment 
Fortune Star (current home video rights holder)
| released =    
| runtime = 96 min.
| country = Hong Kong
| language = Cantonese
}} martial arts action film Nina Li Chi, Carrie Ng, Loletta Lee and Dean Shek. This was Dean Shek’s final film appearance, who has retired from acting in two years later.

==Plot==
Yao Long and May Yip are orphans who live in Russia with their adoptive family. Through the years, the two fall in love and promise that they will be together forever. One fateful day, Yao witnesses a murder being committed by a mysterious assassination|assassin. Shortly after this, he is captured and brainwashed by a mysterious cult of assassins call themselves "800 Dragons". martial art training to become the perfect assassin for the 800 Dragons.

During one of his missions, Yao is seen by May, his past lover who is still looking for him. The code of the 800 Dragons is that anyone who sees an assassin during their mission needs to be killed. But Yao begin to remember his past when May talks to him about their past relationship. Yao decides to follow his heart instead of the strict code of the assassin by not killing May. Now Yao and May must hide for their life. Knowing that Yao fails to follow the order, the 800 Dragons are now trying to kill both of them. 

==Cast==
* Sam Hui – Mr. Yo
* Maggie Cheung – May Nina Li Chi – Chime
* Carrie Ng – Huntress
* Loletta Lee – Pearl
* Dean Shek – Snooker
* Hing Suen – Sunny
* Tak Yuen – Master of the Head / Teddy Wong
* Ying Bai – Frankie
* Shun Lau – Kishudo Sarah Lee – Queenie

==DVD release== Region 2 DVD.
 City Hunter.

==References==
 

 
 
 
 
 
 
 
 