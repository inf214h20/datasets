Ghetto Freaks
 
{{Infobox film
| name           = Ghetto Freaks
| image          =  

| image size     = 
| caption        = 
| director       = Robert J. Emery
| producer       = George B. Roberts,Paul Rubenstein
| screenplay     = John Pappas, Robert J. Emery
| story          = 
| starring       = Paul Elliot, Gabe Lewis, Mickey Shiff
| music          = Tomas Baker, Al Zbacnic
| cinematography = Paul Rubenstein
| editing        = Robert J. Emery, Ellen Rubenstein
| studio         = Cinar Productions
| distributor    = 
| released       = 1970
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Ghetto Freaks is a 1970 American   film, despite almost all the films actors, including the male and female leads, being white.   , grindhousedatabase.com, accessed Mar. 4, 2015. 

The film has also been distributed under the title Wages of Sin. 

Something Weird Video released the original version of the film on VHS as Love Commune in 1992, and released the later Ghetto Freaks version on VHS and DVD in 2002 and again in 2004 as a special edition DVD. 

==Plot== panhandling and underground newspaper on the street. They stage a protest march against the Vietnam War on Clevelands Public Square, discussing their viewpoints with random passersby. For recreation, they attend a rock concert at a club, and frequently use marijuana and LSD. Under the influence of LSD (illustrated by the filmmakers using various psychedelic effects), the hippies engage in nude dancing and uninhibited sex, and one girl experiences a bad trip. 

A rudimentary plot concerns the hippies handsome, womanizing leader, Sonny (Paul Elliot), becoming attached to Donna (Gabe Lewis), a naive young girl who runs away from her parents home to join the commune after a chance meeting with Sonny at the rock club. Sonny and Donnas newfound happiness is threatened by Billy, a violent drug dealer, who pressures Sonny to push drugs for the local racket (crime)|rackets. Sonny refuses, leading to a tragic conclusion in which Donna is killed.

==Cast==
* Paul Elliot as Sonny
* Gabe Lewis as Donna (aka Diane)
* Mickey Shiff as Halo
* Jim Coursar as Mousey
* Nick Kleinholtz III as Stringbean
* Toni Ceo as Marla
* Tom Baker as Cleaver
* Virginia Morris as Girl on bad trip Bob Wells as Donnas father

==Production notes==
The film was set in, and shot on location in Cleveland, Ohio. Locations include Public Square and the surrounding downtown area, University Circle, and the Detroit-Superior Bridge.

Bob "Hoolihan" Wells, who in 1970 was well known in Cleveland as a television weatherman under the name "Hoolihan the Weatherman" and as co-host of the late-night movie and comedy program Big Chuck and Lil John|The Hoolihan and Big Chuck Show, appears briefly in the film as the father of a runaway girl who joins the hippies. 

The dance scenes were choreographed by Jeff Kutash, a dancer on the locally produced TV series Upbeat_(TV_series)|Upbeat. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 