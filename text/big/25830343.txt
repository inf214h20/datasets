I Want You (1951 film)
{{Infobox film
| name           = I Want You
| caption        = 
| image	=	I Want You FilmPoster.jpeg
| producer       = Samuel Goldwyn
| director       = Mark Robson
| writer         = Irwin Shaw
| starring       = Dana Andrews
| music          = Leigh Harline
| cinematography = Harry Stradling
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures 
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.5 million (US rentals) 
}}
I Want You is a 1951 film directed by Mark Robson taking place in America during the Korean War. The film was nominated for the Academy Award for Best Sound (Gordon E. Sawyer).   

==Plot== Army combat combat engineer for four years during World War II, he and wife Nancy have two young children. George Kress asks Martin to write a letter to the Selective Service System stating that his son, George Jr., is "indispensable" for their company and thus exempt from the draft. Martin refuses, and George Jr. joins the army as the Korean War begins.

Martins younger brother Jack is in love with college student Carrie Turner. Despite a trick knee he is drafted; Jack suspects that her father, who is on the local draft board and opposes their relationship, is the reason. His mother, who lost a son during the last war, asks Martin to write an "indispensable" letter for his brother; he seriously considers it but does not do so, and Nancy criticizes Jack for his reluctance to serve. Jack joins the army, where he briefly sees George Jr. before the latter goes to Korea.

The army reports that George Jr. is missing in action, and his father drunkenly blames Martin. His former superior officer, having rejoined the military, asks Martin to join him; although eligible for exemptions, he agrees. Jack and Carrie marry during a furlough before he also goes overseas.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 