Billy Elliot the Musical Live
 
 
{{Infobox film
| name = Billy Elliot the Musical Live
| image = Billy Elliot the Musical Live.jpg
| border = no
| caption = Blu-ray cover
| director = Stephen Daldry
| producer = David Furnish Lee Hall
| based on = Billy Elliot
| starring = Elliott Hanna Ruthie Henshall Deka Walmsley
| music = Elton John
| cinematography = 
| editing = 
| studio = Working Title Films Universal Pictures
| released =  
| runtime = 169 minutes
| country = United Kingdom
| language = English
| budget = 
| gross = $3,705,169 
}} filmed version of Elton Johns Billy Elliot the Musical (2005), which in turn was based on the 2000 film Billy Elliot. Stephen Daldry directed both the original film and the 2014 musical adaptation.
 West End, Billy Elliot Live was broadcast live to cinemas in several European countries, followed by further worldwide screenings. North American screenings took place on 12, 15, and 18 November. 

The filmed production stars Elliott Hanna as Billy, Ruthie Henshall as Mrs. Wilkinson, and Deka Walmsley, Ann Emery, and Chris Grahamson as Billys father, grandmother, and older brother, respectively.

==Plot==

===Act I===
 coal miners strike is just beginning ("The Stars Look Down"). Motherless eleven-year-old Billy is required to stay behind after his boxing class and finds his way into a ballet class run by Mrs. Wilkinson. He is the only boy, but becomes attracted to the grace of the dance ("Shine"). The secret is at first easily kept, as the only person home at the time is his grandmother. She reveals her abusive relationship with her dead husband and that she too loved to dance, which made everything all right ("Grandmas Song").

While his brother, father, and neighbours are on strike and clash with riot police, Billy continues to take dance lessons, keeping it a secret from his family ("Solidarity"), a number which intersperses the violent reality of the strike with the peaceful practise of ballet.

Eventually, Mr. Elliot discovers Billy in the ballet class and forbids him from attending the lessons. Mrs. Wilkinson, who recognizes Billys talent, privately suggests that he should audition for the Royal Ballet School in London. To prepare for the audition, she offers free private lessons. Billy is not sure what he wants to do so he visits his best friend Michael for advice. He finds Michael wearing a dress. He persuades Billy to have fun with him by dressing up in womans clothing and disdaining the restrictive inhibitions of their working class community ("Expressing Yourself").

Billy arrives for his first private ballet lesson bringing with him things to inspire a special dance for the audition ("Dear Billy (Mums Letter)"). He begins learning from and bonding with Mrs. Wilkinson while he develops an impressive routine for his audition ("Born to Boogie"). Mrs. Wilkinsons daughter Debbie tries to discourage Billy because she has a crush on him. Meanwhile, Billys father and brother Tony are engaged in daily battles with riot police that often turn bloody. They struggle to support the family with very little in strike and union pay, a difficult task that goes on for nearly a year.

When the day of the Royal Ballet School audition comes, police are coming through the village and Tony has been injured by the police. Because Billy had not come to the miners hall to get picked up by Mrs. Wilkinson for the audition, she goes to the Elliot home. There, Billys family and some members of the community have gathered. She is forced to reveal that she has been teaching Billy ballet in preparation for this very day. This news upsets Billy’s father and Tony, who gets in an argument with Mrs. Wilkinson. Tony tries to force Billy to dance on the table in front of everyone. The police approach and, as everyone escapes, Billy calls out to his father saying that his mother would have let him dance, but his father refuses to accept that, saying that, "Your Mams dead!". Billy goes into a rage ("Angry Dance"), and for nearly a year, stays away from anything related to ballet.

===Act II===

Six months later at the miners annual Christmas show, the children put on a show disparaging Prime Minister Margaret Thatcher, who is seen as the antagonist by the coal miners ("Merry Christmas, Maggie Thatcher"). Billys father gets drunk and sings an old folk song that elicits memories of his deceased wife and the usually stoic man leaves in tears ("Deep Into the Ground"). Left alone with Billy in the Community Centre, Michael reveals he has feelings for him, but Billy explains that the fact that he likes ballet does not mean that he is gay. Michael gives him a kiss on the cheek. Michael tries to get Billy to show him some dancing, but Billy is sad and just tells him to leave.

Michael departs, but leaves a music player running. Billy feels like dancing for the first time since the day of the aborted audition and dances while dreaming of being a grown-up dancer ("Swan Lake"). Unknown to Billy, his father arrives and watches him dance. Overcome with emotion, his father goes to Mrs. Wilkinson’s house to discuss Billy’s prospects as a dancer. She confirms Billys talent, but is not sure whether or not he would get into the Royal Ballet School. Mrs. Wilkinson offers to help pay for the trip to London for the audition, but Mr. Elliot refuses. He leaves questioning his working-class pride and the future mining has for his boys.

Mr. Elliot decides the only way to help Billy is to return to work. When Tony sees his father cross the picket line, he becomes infuriated and the two argue over what is more important: unity of the miners or helping Billy achieve his dream ("He Could Be A Star"). The argument eventually comes to blows and Billy is hit accidentally. One of the miners chastises them for fighting and says that the important thing is looking after the child. One by one, the miners give money to help pay for the trip to the audition, but Billy still does not have enough for the bus fare to London. A strike-breaker arrives and offers him hundreds of pounds. An enraged Tony attempts to shun his donation, but no one else speaks up in his support. Now drained of hope, Tony dismally ponders whether theres a point for anything anymore, and runs off.

Billy and his father arrive at the Royal Ballet School for the audition. While Mr. Elliot waits outside, an upper-crust Londoner highlights the contrast between the Elliots and the families of the other applicants. Mr. Elliot meets a dancer with a thick Northern accent. The dancer confesses that his father does not support his ballet career. He sharply advises Mr. Elliot to "get behind" his boy. Billy nervously finishes the audition with a sinking feeling that he did not do well. As he packs his gear, he lets that emotion overwhelm him and he punches another dancer who was trying to comfort him. The audition committee reminds Billy of the strict standards of the school. They have received an enthusiastic letter from Mrs. Wilkinson explaining Billys background and situation, and they ask him to describe what it feels like when he dances. Billy responds with a heartfelt declaration of his passion ("Electricity (Elton John song)|Electricity").

Back in Durham, the Elliots resume life, but times are tough and the miners are running a soup kitchen to ensure everyone is fed. Eventually, Billy receives a letter from the school and, overwhelmed and fearful, knowing that it heralds the end of the life he has known, informs his family that he wasnt accepted. Tony retrieves the letter from the waste bin and discovers that his brother was accepted. At the same time, the miners union has caved in; they lost the strike. Billy visits Mrs. Wilkinson at the dance class to thank her for everything she did to help him. Debbie is sad that Billy will be leaving.

Billy packs his things for the trip to the school and says goodbye to the soon to be unemployed miners who are returning unhappily to work ("Once We Were Kings"). Billy says goodbye to his dead mother, who often visits him in his imagination ("Dear Billy (Billys Reply)"). Michael arrives to say goodbye and Billy gives him a kiss on the cheek. Billy takes his suitcase and walks out to his future alone.

The entire cast comes out on stage and calls Billy back to celebrate the bright future ahead of him ("Finale").

==Cast==
* Elliott Hanna as Billy Elliot
* Ruthie Henshall as Sandra Wilkinson
* Deka Walmsley as Jackie Elliot
* Ann Emery as Grandma
* Chris Grahamson as Tony Elliot
* Zach Atkinson as Michael Caffrey
* Liam Mower as Older Billy
* David Muscat as Mr. Braithwaite
* Claudia Bradley as Mrs. Elliot
* Howard Crossley as George
* Demi Lee as Debbie Wilkinson

==Musical numbers==
 
 
;Act I
* "The Stars Look Down" – Company
* "Shine" – Ballet Girls, Mrs. Wilkinson, and Mr Braithwaite
* "Grandmas Song" – Grandma
* "Solidarity" – Ballet Girls, Billy, Mrs. Wilkinson, Miners, and Police
* "Expressing Yourself" – Billy, Michael, and Ensemble
* "The Letter (Mums Letter)" – Mrs. Wilkinson, Mum, and Billy
* "Born to Boogie" – Mrs. Wilkinson, Billy, and Mr. Braithwaite
* "Angry Dance" – Billy and Male Ensemble
 

;Act II
* "Merry Christmas, Maggie Thatcher" – Tony and Partiers
* "Deep Into the Ground" – Jackie
* "Swan Lake" – Billy and Older Billy
* "He Could Be a Star" – Jackie, Tony, and Miners
* "Electricity (Elton John song)|Electricity" – Billy
* "Once We Were Kings" – Company
* "The Letter (Billys Reply)" – Billy and Mum
* Finale – Company
 

==Release== The Equalizer with £1.9m  ($3,094,159). 

==Home media==
The film was released 24 November 2014 on DVD and Blu-ray Disc|Blu-ray in the United Kingdom. This release differs slightly from what was originally broadcast to cinemas in that certain camera angles were changed and that it incorporates shots from a practice shoot the day prior to the original live broadcast.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 