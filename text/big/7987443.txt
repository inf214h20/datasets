Triple Crossed
:For the similarly titled 2013 film directed by Sean Paul Lockhart, see Triple Crossed (film)
{{Infobox Film |
  | name           = Triple Crossed
  | image          =Triplecrossed1959titlecard.JPEG
  | caption        = 
  | director       = Jules White 
  | writer         = Warren Wilson Larry Fine Mary Ainsley Diana Darrin Connie Cezon
  | cinematography = Fred Jackman Jr. 
  | editing        = Saul A. Goodkind
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 48"
  | country        = United States
  | language       = English
}}

Triple Crossed is the 189th and penultimate short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Larry is a womanizer who is having an affair with Moes wife Belle (Mary Ainslee). At the same time, he is also making eyes at Joes fiancee, Millie (Angela Stevens). However, Moe tracks down the conniving Larry at his pet shop, and gives him the works before Larry calms him down. Realizing he needs to cover his tracks, Larry looks for a "fall guy" in the form of Joe. Larry then gets Joe a job as an underwear salesman and the first place he goes is Moes home. 

While Joe is modeling his ware, Larry lies to Moe about Joes advances on Millie. Both of them go storming over to Moes, while Joe flees up the chimney. After making a quiet getaway, Joe bumps into Larry, and turns him in. 

Joe explains to Moe how Larry had set him up.  Millie reveals how Larry had tricked her into coming there.  Moe tells her Larry had tricked him, too.  Millie and Joe make up while an angry Moe punishes Larry.

 

==Production notes==
Triple Crossed is a remake of 1952s He Cooked His Goose, using ample stock footage. New footage was filmed on December 18, 1957, the day before shooting commenced for Flying Saucer Daffy, the last Stooge short to be filmed. 

When Moe shoots at Joe up the chimney, you can hear Shemp Howards yell from He Cooked His Goose. In a cost-saving measure, Joe Bessers voice was not dubbed over Shemps for authenticity. In addition, when Larry is walking in the hallway from the elevator to meet Moe’s wife, Joe is hiding in the closet wearing a Santa Claus outfit; however, it is Shemp who is whistling at Larry. Also, Shemp’s face can be seen in the background peeking from underneath the bearskin rug and can be seen when he opens the janitor closet door and slams Larry in the hallway. 

Over the course of their 24 years at Columbia Pictures, the Stooges would occasionally be cast as separate characters. This course of action always worked against the team; author Jon Solomon concluded "when the writing divides them, they lose their comic dynamic."    In addition to this split occurring in Triple Crossed (as well as He Cooked His Goose, the film it originated from), the trio also played separate characters in Rockin in the Rockies, Cuckoo on a Choo Choo, Gypped in the Penthouse, Flying Saucer Daffy and Sweet and Hot.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 