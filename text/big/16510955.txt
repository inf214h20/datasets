Peter Pan (1988 film)
 
{{Infobox Film
| name           = Peter Pan
| image          = Peter Pan 1986.jpg
| image_size     = 
| caption        = 
| director       = 
| producer       = Roz Phillips
| writer         = Paul Leadon, J. M. Barrie (original author)
| narrator       = 
| starring       =  John Stuart
| cinematography =  Peter Jennings
| distributor    = NuTech Digital
| released       = 1988 (Australia)
| runtime        = 50 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 John Stuart.  The copyright in this film is now owned by Pulse Distribution and Entertainment  and administered by digital rights management firm NuTech Digital. 

== Plot summary ==
 Lost Boys, Tiger Lily.  After all the excitement, Wendy announces that it is time to return home, and she invites the Lost Boys to come along, so that they may be returned to their real mothers.  Smee, one of Captain Hooks pirates, follows the children, and he too is reunited with his mother.  Wendy, John and Michael invite Peter Pan to stay with them in their home in London, but Peter Pan refuses, for that would mean he would have to grow up, something he would never want to do.  They part, but Peter Pan welcomes them to return to Neverland someday.

==Cast==

Nine actors provided the voices for the dozen or so characters with speaking parts.
*Phillip Hinton
*Keith Scott
*Daniel Floyd
*Jonathan Panic
*Carol Adams
*Olivia Martin
*Jaye Rosenberg
*Ben Brennan
*Michael Anthony

== See also ==
* Peter and Wendy
* J. M. Barrie
* Burbank Films Australia

== References ==
 

== External links ==
*  
*  
*   at the Big Cartoon DataBase
*  

 

 
 
 
 
 
 
 
 
 
 
 