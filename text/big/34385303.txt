Wounded (film)
{{Infobox film
| name           = Wounded
| image          = 
| alt            = 
| caption        = 
| director       = Krishna Mishra
| producer       = Shree Hari Om Films
| writer         = Saloni Goel Aziz Burney
| starring       = 
| music          = Ajay Babla Mehta
| cinematography = Aloke Dasgupta
| editing        = Kamal Saigal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 1.25 cror  
| gross          = 
}}
Wounded: The Bandit Queen is a 2007 Hindi language film that was directed by Krishna Mishra.   The film was first released on March 9, 2007. The film is based on the true story of Seema Parihar,  who plays herself in the film. 

==Plot==
Parihar was abducted at the age of 13 from her village and the film depicts her transformation into a bandit.

==Cast==
*Kanhaiya as Lalaram
*Seema Parihar as herself
*Sushma Das as Seemas Mother
*Kailash Bajpai as Seemas Father

==Production==
Planning for Wounded began around 2002, when Tarun Kumar Bagchi, A.K. Joshi and Krishna Mishra were inspired to create a film based upon Seema Parihars life. The three men lobbied for Parihars release on bail, as they wanted her to play as herself in the movie.  They were successful and filming began in 2004, upon which point it received some media notice due to it being one of the first films where a former dacoit plays herself.   During filming Mishra and Parihar received threats of bodily harm from dacoit Nirbhaya Gujjar, who led the gang that Parihar formerly led,  as he believed that she was "defaming the lives of dacoits".  Several attacks were made upon the production.  Upon completion of filming, the censor board of India initially refused to clear Wounded as they had objections over slang words used in the movie.  The Bombay High Court later cleared the film without removing any of the language that the censor board wanted to remove. 

==Reception==
Turnout for Wounded was poor and reception was described as "lackluster" by the Hindustan Times.  

===Awards===
*Critics Award at the Leicester Expo Film Festival (2005, won) 

==See also==
*Seema Parihar
*Bandit Queen
*Phoolan Devi

==References==
 

==External links==
*  

 
 
 