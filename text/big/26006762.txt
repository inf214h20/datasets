The Pad and How to Use It
{{Infobox Film
| name           = The Pad and How to Use It
| image          = 
| image_size     = 
| caption        = 
| director       = Brian G. Hutton
| producer       = Ross Hunter
| writer         = 
| narrator       = 
| starring       = Brian Bedford
| music          = 
| cinematography = 
| editing        = 
| distributor    =  Universal Pictures
| released       = September 23, 1966
| runtime        = 86 min.
| country        = United States
| language       = English
| budget         = $300,000 Freddie Fan of Filmdom Finds Lost Audience: The Lost Audience Discovered
Warga, Wayne. Los Angeles Times (1923-Current File)   21 June 1970: q1.  
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Pad and How to Use It is a 1966 comedy film directed by Brian G. Hutton. It was based on a one-act play by Peter Shaffer.

A sensitive man named Bob Handman (Brian Bedford), who lives alone in his apartment, encounters  what he believes to be his ideal woman, Doreen (Julie Sommars), at a classical music concert. They arrange to meet at a later date at his pad. Because he is so unwordly he asks his best friend Ted (James Farentino) along to the date as well for moral support. It transpires that she only went to the classical concert because she was given a free ticket by a co-worker. She has no interest in classical music, which is Bobs passion. But she is charmed by Ted who prepares the evening meal and flirts with her outrageously while Bob gets drunk. Bob and Ted fall out and Doreen goes off with Ted. The movie ends with Bob sitting in a darkened room, listening to the aria from Madame Butterfly. He gets up and drags the phonograph needle across the record several times, placing the needle back on the record. As he sits in the dark crying  the record skips repeatedly over the scratched aria.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 