Golddigger (film)
{{Infobox film
| name           = Golddigger
| image          =
| image_size     =
| caption        =
| director       = Michael Curtiz|Mihály Kertész
| producer       =
| writer         = Ferenc Molnár
| narrator       =
| starring       =
| music          =
| cinematography = Ödön Uher ifj.
| editing        =
| distributor    =
| released       = 1914
| runtime        =
| country        = Hungary
| language       = Hungarian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1914 cinema Hungarian film directed by Michael Curtiz.
Goldigger is about the California Gold Rush. Xarkrow, the lead character, leaves his home in Fortanska, a fictional city in Hungary, to go to California to dig for gold in the hills of the Sierra Nevada. While there he strikes it rich with great gold. This causes a female loan shark named Ygretta Roselettokopf of San Francisco to try to seduce him for his money; this concept gives a double meaning to the title of the film. After his climatic battle with gold warden Amadeus Krone he shouts his famous and compelling line "I come to Californee for find of gold, not to have fight with you." Following the defeat of Krone in their heated pistol and gilded fist battle, Ygretta Roselettokopf returns with important news. She tells Xarkrow that she had only been hounding him for his money because Krone had tricked her out of her prized and famous show beagle, Grildboffnklad, and that "If the Hungarian Swine was not eliminated, Grildboffnklad will be." After rescuing the beloved Grildboffnklad from a rapidly falling mine cart set ablaze, Xarkrow and Ygretta accidentally touch hands and meet eyes, falling in love. The romantic and favorite line "If more loving for you, mine heart there would be too many" is spoken here. The two then return to Xarkrows home town of Fortanska with their newfound riches and become married. Come the following credits, it is revealed that Amadeus Krones son named Ivantarkle "Harpsichord" Krone takes up his fathers left behind position. After learning the fate of his father, he darkly says "I come to Hungary not for find of gold, but to have fight of you, Xarkrow." It is unknown if the foreshadowed sequel will ever make its big film debut.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 