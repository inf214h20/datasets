The Black Godfather
{{Infobox film
| name           = The Black Godfather
| image          = Blackgodfatherposter.jpg
| caption        = Theatrical release poster John Evans
| producer       = Jerry Gross John Evans Rod Perry Don Chastain Jimmy Witherspoon Duncan McLeod Diane Sommerfield Phil Moore Martin Yarbrough
| cinematography = Jack Steely
| editing        = James Christopher
| distributor    = Cinemation Industries
| released       = September 3, 1974
| runtime        = 90 min.
| country        = United States
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 John Evans, Rod Perry, Don Chastain, Diane Sommerfield and Jimmy Witherspoon.

==Plot==
J.J. (Rod Perry), a rising star in the black crime scene, is in the process of consolidating his power over the neighborhood. One of the only remaining obstacles is the white heroin cartel that is understandably reluctant to abandon such a lucrative market.  Tensions rise, and an explosive confrontation is the result.

==Plot synopsis==
The movie begins with an overview of a city skyline.  J.J. (Rod Perry) and his friend are seen walking down a street; they stop and one of the men points to a house.  They proceed to walk up the stairs that lead to the front door.  As Tommy opens the door he pulls out a gun that jams on him.  The person inside the house shoots him and proceeds outside to shoot the other man.  Tommy dies in the arms of J.J. in an alley; so the other man walks away and is met by Nate Williams (Jimmy Witherspoon) who helps him by taking him to safety.  Williams takes out the bullet in J.J.’s arms and tends to the wound.

The next morning J.J. and Williams talk.  J.J. is curious as to why Williams decided to save and take care of him after he was shot.  Williams was impressed by J.J.’s courage in trying to break into a known gangster’s house.  J.J.’s motive was only to survive and “get some bread”.  Williams offers J.J. some advice in surviving on the streets and gives him some money.  This is the beginning of J.J.’s rise to power in the street.

After the opening credits are shown, J.J. has grown in power and position on the street.  He longs for more as he sells drugs and tries to gain better status on the streets.  He believes that money buys dignity and that it does not matter where it comes from.  J.J. wants to get rid of another drug dealer named Tony Burton(Don Chastain).  J.J. has a problem with Tony, a white gangster, trafficking heroine into a predominantly black community.  The drugs have been negatively affecting the kids and adults because many of them become addicted. He talks to a man called Diablo (Damu King) about taking care of securing the neighborhood from Tony’s men and any other drug dealer pushing in the territory.

Lieutenant Joe is a crooked cop that takes bribes from drug dealers for his own gain.  He enters J.J.’s headquarters at the club and goes up to talk to him in his office.  J.J. explains to him that he is willing to go to war with Tony and any other drug dealer that exploits the black community.  He tells Joe to relay the information back to Tony, knowing that Joe takes bribes from any and all drug dealers so long as he gains money.

Lieutenant Joe leaves the bar and in the next scene is walking poolside at Tony’s home.  He tells Tony that J.J. wants Tony to stay out of the territory and is prepared to fight if Tony does not comply.  Tony explains that he does not make the territory, but just runs it and is not backing down. Williams and J.J. meet together at the club and talk together in the office.  Williams has brought his daughter, Yvonne, along. Williams warns J.J. about trying to push Tony out of the territory.  He orders J.J. not to take action.  J.J. disagrees, explains his position and is set on pushing Tony out of the community because of the drugs he brings in and the negative effects on the community.  Yvonne comes into the office while the two become heated in argument so J.J. and Nate stop talking.

The next morning Tony goes to visit Nate Williams and speak with him.  Tony tells Nate that he wants business running as usual and that J.J. should stop harassing his people.  He threatens and welcomes the idea of bloodshed.  Williams is not intimidated and tells Tony to be careful otherwise the bloodshed will spill over to his side.  He tells Tony to leave his office if that was all he had to say.

J.J., Williams and his bodyguard talk at the gym.  The idea of change and how times are different are discussed.  The bodyguard and J.J. fight but are still friendly with one another after.  This is a way J.J. establishes his reputation and masculinity.

The next scene, two African American gangsters are seen patting down two white gangsters for drugs.  They hold them at gunpoint and tell them to leave and never come back otherwise they will kill them.

J.J. and Yvonne are seen coming back to his place where they sleep together.  In the morning, she is seen walking around the house and admiring it.  She puts on a music record, makes coffee and sits on the couch.  J.J. and the woman talk and she begin to talk about the expectations men have of women.  J.J. tells her he loves her but just cannot commit for the time being.

Diablo and his crew of men chase a car down with a white driver.  His car flips over, and they proceed to take a bag from his car.  They meet up at a house and go upstairs to question a white man (the same one that was patted down earlier).  An entire group of African American gangsters surround him as he sits in a single chair in the room.  They ask him why he was near the high school in the area.  Being threatened, he tells them about a shipment of drugs coming in but does not know exactly know when or how.  J.J. and his men keep questioning him but it is clear that he does not know anything else.  They allow him to run away.  The next scene, they break into another gangster’s home and kill him.  They do not care about taking any of the drugs.  At this point, J.J. and the men are taking more and more action to stomp out the men who are invading and have stake on their territory.

Lieutenant Joe and Tony are seen speaking back at the pool.  Joe tells Tony that J.J. has been doing work on another drug dealer.  Tony is still not scared and does not back down.  He calls J.J. a spook and continues to go about his business.  A member of the community goes to J.J.’s club and tells him about what she overheard at Tony’s.  She tells him that there is a shipment coming in worth three and a half million dollars.  J.J. is grateful and pays her for bringing him information.

Diablo’s men are set up and are caught at a home by police.  Lieutenant Joe tells Tony to watch the news for this as he laughs. Tony’s plan of bringing in a large shipment continues.  He talks on the phone with one of his men who confirm that the process is running smoothly and that it should be flying in New Orleans soon.

J.J. and his men are waiting at the airport with artillery and start a gunfight with members of Tony’s gang.  He succeeds in breaking up the delivery and theft of the shipment and calls Tony to taunt him in his office.  This becomes the final push for Tony to take action against J.J.

Tony immediately responds by going to Nate William’s house to ask him about where J.J. is.  Nate has no idea about J.J.’s whereabouts or about the shipment that J.J. stole from Tony.  Tony proceeds to assault Nate in order to get information.  Lieutenant Joe tries to stop him but they end up shooting him for getting in the way.  Tony kidnaps Yvonne after beating Williams to death to use her as a bargain for the shipment of drugs.

Diablo and J.J. begin arguing about what to do and nearly end up fighting each other physically back at their headquarters.  J.J. cools down and tells Diablo that the problem is “out there” against Tony and not within themselves and the community. Tony calls J.J. and the two work out a deal for the package.  Tony says he’ll give $350,000 and Yvonne back in exchange for the drugs.  J.J. says to bring only one man.  J.J.’s men recorded the phone conversation and listened to it over again.  One of J.J.’s men recognizes the bell sound in the background and puts Tony’s general location near a church.  J.J. orders the men to put out flyers of Yvonne in the area so that the community can help search for her.  Another of J.J.’s men goes home where his father recognizes a picture of Yvonne on the printed flyers and tells him that he saw her being taken down into the hospital.

A confrontation occurs when J.J. and his men go to the hospital and confront Tony and his men.  A violent final gunfight occurs as J.J. and Diablo run into the kitchen area of the hospital.  Tony hides behind a trashcan with Yvonne behind him as he looks for J.J.  Being in much distress, Yvonne grabs a butcher knife and while Tony is not looking, stabs him to death.  Tony falls to the floor and J.J. and Diablo walk to Yvonne who can only cry after the ordeal.  The movie ends with a final shot of Tony dead on the floor.

==Cast==
Rod Perry as J.J., a young African American male who becomes a street boss after being taken in by the “Black Godfather”.  J.J. is exposed to violence when his good friend gets shot and killed after the two try to steal from a drug dealer.  Luckily surviving, J.J. becomes dedicated to keeping the black community free from drugs even if it means killing.  Rod Perry is also a former NFL player.  This is his first film playing the main role.

Damu King as Diablo, second to J.J. in power, does most of the dirty work in carrying out violence against whoever is in the way.  Diablo is trusted by J.J. and is relied upon to be the muscle of their operations.

Jimmy Witherspoon as Nate Williams, “the Black Godfather”.  Nate Williams takes in J.J. and helps him survive after being shot.  He gives J.J. the money to start up and take care of himself.  Williams is the older generation of street boss who oversees everything.

Duncan McLeod as Joe Sterling, a crooked police lieutenant who cuts deals with drug dealers on the side.  Sterling overlooks much of the drug trafficking made by the gang members but still leaves room for morality when it comes to taking a human life.

Don Chastain as Tony Burton, rival mafia boss who is in charge of all the drug trafficking happening in J.J.’s community.  Burton is the main antagonist.  He is very influential, powerful and dangerous and has no problem with violence.

Diane Sommerfield as Yvonne Williams, daughter of Nate Williams.  Yvonne is the love interest of J.J.  She is known to be important to J.J. and a target of kidnapping for Tony Burton.

==Themes==
The Black Godfather came out in 1974.  This is an era in time where Blaxploitation film became a popular genre for the audience of young black men.  The movie was released shortly after titles such as Shaft (1971 film)|Shaft and Sweet Sweetbacks Baadasssss Song came out.  Almost unknown, The Black Godfather follows the same formula and aspects of many blaxploitation films.  There is a strong, African American, male protagonist (J.J.); a negatively portrayed white antagonist (Tony); a corrupt white policeman (Joe); and a beautiful, young African American female love interest (Yvonne).  The setting is also placed in an urban area where the characters deal or area involved with drugs, violence and gangs.  This sort of film appealed to an audience that was not used to seeing African Americans at the center of a role.  Nelson George writes in Blackface: Reflections on African-Americans and the Movies that “one of the chief appeals of blaxploitation’s best films was that by changing the point of view of a generic film you could capture, even fleetingly, some glimpse of our urban reality” (George 30). Placing blacks in the limelight “was crucial to the magic of blaxploitation”; this gave blaxploitation movies like The Black Godfather an audience (George 30).

Conflict between whites and blacks is a  “recurring blaxploitation theme” according to Nelson George (George 53).  It can take form in many different ways.  There could be conflict between white and black mobsters.  White authority figures may be corrupt and black criminal figures may be in conflict with them.  These themes can be seen in Shaft and other blaxploitation films.  In The Black Godfather, these complicated relationships take form between the interactions of J.J., Tony, Joe and Nate.  J.J. and Tony are opposed to one another in conflict as a white and black mobster.  J.J. is concerned for a black community and takes on an active role in keeping the community together.  This is different from many passive roles that blacks have in other film genres.  This was one of the appeals of blaxploitation.  With the strong character of Tony who is dominating in power and stature, it is appealing for urban black audiences to see a black main character who can equally meet and challenge the notion of power that Tony holds in the territory.  The character of Joe fulfills the role of a stereotypical white, corrupt authority figure.  He works more on Tony’s side in the film.  He acts as more of a nuisance to J.J.; collecting bribes from time to time to offer J.J. an alternative to legal action.  Surprisingly there is also conflict between Joe and Tony.  Toward the end of the film, the identity of cop and gangster are more recognized by the two than their race.  Tony kills Joe in a violent interrogation because Joe tries to fulfill his role as an officer of the law.

Young women and sexuality:
“Young women, of all colors, are sexual pawns or playthings,” writes Nelson George in speaking about blaxploitation film (George 53).  There are several instances in the film where this can be seen.  When Diablo and his crew of men raid a house of a drug dealer, a young white woman who is unable to take her heroine begs Diablo for the drugs back.  She offers money, drugs and finally sex in return for the drugs.  They gangsters end up leaving her to her own broken state by refusing her advances.  Clearly she is portrayed as a bad girl in the film, being mixed with social taboos that may not be seen as respectable in society.  Norma Manatu writes in African American Women and Sexuality in the Cinema, “social rewards await those who remain within the borders of ‘appropriate’ sexual behaviors” (Manatu 56).  Usually black women are portrayed as ones who seem to only “require…raw sexual pleasure” (Manatu 58).  But in this film, the strong female role in Yvonne covers an unusual basis for black women in film.  Her scene when talking in the morning with J.J. depicts a tender and sweet emotional side of her.  It also shows that J.J. cherishes her as his love interest.  According to Manatu’s research, she finds that researchers implicitly find in their work that “romantic love primarily accentuates sentiment and emotion, attributes clearly not associated with, nor expected of, black women” (Manatu 58).  With Yvonne, Evans as a filmmaker portrays her as an emotionally sensitive and smart woman and plays with notions of power being that Yvonne ends up killing the main antagonist (Tony) stopping the potential deaths of J.J., Diablo, and herself.  There is a problem when film portrays negative images and assigns specific roles constantly to one group.  “The dominant defining characteristic of black women as ‘sexual other’ may, in fact become seared in viewers’ minds…perpetuation of the negative imagery of black female sexuality involves a complex set of social systems and beliefs that works together to preserve the image” (Manatu 50).  Different constructions of the black female can be found in many blaxploitation films.

==References==
 

==External links==
*  

 
 
 
 
 
 
 