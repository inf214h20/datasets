Eight Hundred Heroes
 
 
{{Infobox film
| name           = Eight Hundred Heroes
| image          = EightHundredHeroes+1975-59-b.jpg
| caption        = Film poster
| director       = Shan-hsi Ting
| producer       = 
| writer         = Shan-hsi Ting Chin Han
| music          = 
| cinematography = Wenjin Lin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Ko Chun-hsiung as Lieutenant Colonel Xie Jinyuan
* Brigitte Lin as Yang Huimin, a girl scout guide deliver a flag to the soldiers
* Hsu Feng as Ling Weicheng, Xies wife
* Sylvia Chang as Li Cini, a girl guide Chin Han as Major Shangguan Zhibiao
* Chang Yi
* Carter Wong Chin Han
* Chan Hung-lit
* Peter Yang
* Sihung Lung

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Taiwanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 