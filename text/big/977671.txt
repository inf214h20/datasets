Sommersby
 
{{Infobox film
| name           = Sommersby
| image          = Sommersby (movie poster).jpg
| caption        = Sommersby Promotional Movie Poster
| director       = Jon Amiel
| producer       = Arnon Milchan Steven Reuther
| writer         = 1982 screenplay: Daniel Vigne     Sarah Kernochan
| starring       = Richard Gere   Jodie Foster   Bill Pullman   James Earl Jones
| music          = Danny Elfman
| cinematography = Philippe Rousselot Peter Boyle Le Studio Canal+ Regency Enterprises Alcor Films
| distributor    = Warner Bros.
| released       = February 5, 1993
| runtime        = 114 minutes
| country        = United States
| language       = English 
| budget         = $30 million
| gross          = $140,081,992
}}
 romantic drama film directed by Jon Amiel and starring Richard Gere, Jodie Foster, Bill Pullman and James Earl Jones.
 Reconstruction period Civil War, French peasant Martin Guerre (previously filmed by Daniel Vigne as The Return of Martin Guerre with Gérard Depardieu in 1982).

==Plot==
John "Jack" Sommersby (Gere) left his farm to fight in the American Civil War and is presumed dead after six years. Despite the hardship of working their farm, his apparent widow Laurel (Foster) is quite content in his absence, because Jack was an unpleasant and abusive husband. She even makes remarriage plans with one of her neighbors, Orin Meacham (Pullman), who despite his own hardships (such as a wooden foot, which he wears to replace one that was lost in the war) has been helping her and her young son with the farmwork.

One day, Jack seemingly returns with a complete change of heart. He is now kind and loving to Laurel and their young son, Rob. In the evenings, he reads to them from Homers Iliad, which the old Jack never would have done. He claims that the book was given to him by a man he met in prison, and that "War changes you; makes you appreciate things." Jack and Laurel rekindle their intimacy, which leads to Laurel becoming pregnant.
 Burley tobacco Confederate veterans about the inclusion of former slaves.

One black freedman living on Sommersbys land is brutally attacked and dropped at Sommersbys door, by men proclaiming themselves the Knights of the White Camellia (one of them is Meacham, distinguished by his wooden foot). Jack is threatened in an attempt to force him to exclude Black people from the landowning but he refuses, saying that they can "own what they pay for".

Upon taking the townspeoples money, he sets off to buy the tobacco seed claiming that the crops will raise enough funds to rebuild the town church. Great suspicion and skepticism falls upon him (and by association, Laurel and their son) when he does not return at the expected time. He does, however, return. All those that bought in on the deal set to work, transforming the dull and lifeless plantation into a breeding ground of promise and prosperity. Laurel gives birth to a daughter, Rachel.

Shortly after Rachels  . Laurel and Jacks lawyer agree to argue that her husband is an impostor, not the same man who left Laurel to fight in the war. This would save her husband (or supposed husband) from hanging for murder, but he would still be imprisoned for several years for fraud and military desertion. Meacham devises this plan in exchange for Laurel promising to marry him upon "Sommersbys" imprisonment.

Jack fires the lawyer and sets about re-establishing himself as the real Sommersby. Several witnesses are brought up to discredit this Sommersby as a fraud, who state that he is one Horace Townsend, an English teacher and con artist from Virginia. One witness says that the man currently posing as Jack defrauded his township of several thousand dollars after claiming he wanted to help rebuild the schoolhouse there. He is also said to have deserted the Confederate Army and ended up in prison. Sommersby quickly discredits the mans testimony by identifying him as one of the Klansmen who had threatened him earlier. He points out that Orin Meacham was another of those men, and that this is all a set-up to try to rob the new black farmers of the land they have bought. When the black judge, Barry Conrad Isaacs (Jones) confronts him, the witness snaps, "When the Yankees have all gone youll be back in the field where you belong!" The judge sentences him to 30 days for contempt, increased to 60 days upon the mans protest.

When Laurel is called as a witness, Jack asks her to give the reason she knows he is not the "real" Jack, and Laurel reveals his kind nature is what convinced her of his being an impostor, admitting "…because I never loved him the way I love you!" With this, she says that she believes the man before her to be her real husband. Judge Isaacs calls Jack to his bench to ask whether he wishes to be tried as Jack Sommersby, even if it will certainly mean death by hanging. Jack glances at the freed blacks who have been farming his land, and then he glances at his wife and his daughter, who would be respectively condemned as an adulteress and a bastard child if he claimed the identity of Horace Townsend. He calmly states that he wants to be tried as John "Jack" Sommersby.

Jack is convicted of first degree murder and sentenced to death by hanging.  While awaiting death, he is asked by Laurel to tell the truth about his identity and Horace Townsend. Laurel mentions the book on Homers works that he holds. Jack tells her the story of how a man had to share a cell with another man, who looked like they could have been brothers. After sharing a cell for four years, they got to know everything about each other.

Upon his release, Jack Sommersby killed another man, then died from a wound he got during the fight. Horace Townsend then buries Jack Sommersby, which is seen in the opening scene of the film. When Laurel asks him "You mean you buried Jack," he answers "I buried Horace." Horace Townsend decided to assume Jack Sommerbys identity, and no longer be the man he was. Laurel begs him to tell the truth, pleading for him to return home with her. Jack concludes by saying he cant admit to being Horace, because Laurel and the children would lose everything.

As Jack is taken to the gallows, he asks Laurel to be amongst the crowds, as he cannot "hang alone". As Jack is about to be hanged, Laurel makes her way to the front of the crowd. Jack calls for her, claiming to the executioner that he "isnt ready". She calls back to him, and the two see each other before he is executed.

The closing scenes show Laurel walking up a hill with flowers. She then kneels by the gravestone of "John Robert Sommersby" and lays the flowers down for him. It is revealed that work is being done on the steeple of the village church, as Jack had wished.

==Cast==
* Richard Gere - John "Jack" Sommersby/Horace Townsend
* Jodie Foster - Laurel Sommersby
* Brett Kelley - Rob Sommersby, son
* Bill Pullman - Orin Meecham
* James Earl Jones - Judge Barry Conrad Isaacs
* Lanny Flaherty - Buck William Windom - Reverend Powell
* Wendell Wellman - Travis
* Clarice Taylor - Esther
* Frankie Faison - Joseph
* R. Lee Ermey - Dick Mead Richard Hamilton - Doc Evans
* Maury Chaykin - Lawyer Dawson Ray McKinnon - Lawyer Webb
* Caileb Ryder/Caitlen Ryder - Baby Rachel

==Reception==

The film got mixed, but mostly positive reviews from critics, and currently holds a 63% on Rotten Tomatoes, marking it a close "Fresh."  Critics praised the acting and chemistry of the two leads, Gere and Foster, but panned the ending for its vague redemption of the imposter. 

The sweeping score by Danny Elfman was praised.

== Related stories==

Sommersby is based on the French film The Return of Martin Guerre which in turn is based on the true story of Martin Guerre.

The same basic theme was used in an episode of The Simpsons, "The Principal and the Pauper," when it turns out that Principal Seymour Skinner is actually Armin Tamzarian (a delinquent orphan from New Orleans) who assumed the identity of Sergeant Seymour Skinner when the latter was missing and assumed dead. When the real Seymour Skinner returns home, the townspeople turn against him, discovering that for all of his faults, Armin Tamzarian is actually a better "Seymour Skinner" than the real one. It is interesting to note the episodes working title was Skinnersby, in reference to the film.
 assumes his identity. The widow of the real Don Draper, Anna Draper, hunts him down believing Don has run out on her. Instead she discovers the switch and becomes a very close friend of the fake Don Draper (perhaps more so than the real one).

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 