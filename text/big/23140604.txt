See You After School
{{Infobox film
| name           = See You After School
| image          = SYASchool.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         = 방과후 옥상
| hanja          =    
| rr             = Banggwahu Oksang
| mr             = Pangkwahu Oksang}}
| director       = Lee Seok-hoon
| producer       = Choi Hee-il
| writer         = Lee Seok-hoon Kim Tae-hyun Jung Yoon-jo
| music          = Kim Jin-han
| cinematography = Choi Jin-woong
| editing        = Moon In-dae
| studio         = 
| distributor    = Cinema Service
| released       =  
| runtime        = 103 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 2006 South Korean comical melodrama about typical losers zero to hero conquest.    

== Plot ==
Goong-dahl, a typical loser billed as the "unluckiest man alive" returned to the school after one years intense training of reject students treatments. Now he is up to the challenge of posing him self other than a typical loser. With the leads from his another old fellow mate he started with a new face by threatening and rescuing hot girl "Min-ah" from several oldies in the school. Everything went smoothly up to the plan until one of the bully is happened to be the schools notorious thug "Jae-koo" thus receiving invitation to a grand duel on the roof top after the school.

After the light of adventure shading with the price of upcoming fierce grand duel Goong-dahl along with his ally seeks alternatives to avoid the grand duel. But adding more wood to the fire all the plans back fired and cemented his legend thanks to bolstering myths spread by famous mouth flickers in the school.

Meanwhile unknown to "Goong-dahl" group of thugs from out side also trying to hunt him mistakenly believing he is the their arch enemy Jae-koo. Nevertheless during the intermission Goong-dahl fallen in love with Min-ah and became the resurrection dragon of loser students union in the school.

But after all the hard work done, he is survived by only two options before the duel. Either become a predator of fellow losers or the protector of fellow losers. What will be his ultimatum? Is once a loser forever a loser?

== Cast ==
* Bong Tae-gyu ... Namgoong Dahl Kim Tae-hyun ... Ma Yun-sung
* Jung Yoon-jo ... Choi Min-ah
* Ha Seok-jin ... Kang Jae-goo
* Jo Dal-hwan ... Byung-goo
* Park Chul-min ... homeroom teacher
* Im Hyun-sik ... doctor
* Chu Sang-mi ... female doctor
* Son Byeong-wook ... Tae-son
* Choi Eun-joo ... Mae-soon
* Jeong Dae-hun ... student on bus
* Lee Dal-hyeong ... Park Ki-bong
* Noh Hyung-ok ... Hong-soo
* Lee Je-hoon ... Ahn-kyung
* Lee Ho-young ... Chinese teacher
* Hong Gyung-yeon ... Dals mother

==References==
 

== External links ==
*  
*  
*  

 
 
 

 