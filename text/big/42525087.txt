Suyetchai MLA
{{Infobox film
| name           = Suyetchai MLA
| image          = 
| image_size     =
| caption        = 
| director       = Guru Dhanapal
| producer       = Stanley P. Rajan T. M. Varadharajan A. Abraham Selvakumar M. G. Karunanidhi
| writer         = Guru Dhanapal
| starring       =  
| music          = Sabesh-Murali
| cinematography = D. Shankar
| editing        = R. Suresh Rajhan
| distributor    =
| studio         = Stanwin Creations
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 2006 Tamil Tamil political satire film directed by Guru Dhanapal. The film features Sathyaraj and Goundamani in lead roles. The film had musical score by Sabesh-Murali and was released on 5 May 2006, after many delays. 

==Plot==

Nambirajan (Sathyaraj) and Visky (Goundamani) are best friends. Both of them are very helpful to MLA Arivudaiyan  (Nassar) with all his work. Priya (Ammu), Nambirajan’s sister, loves Rakesh, a rich boy. Nambirajan and Rakeshs father Muthiah Pillai (Rajan P. Dev) prepare their engagement. In the function, Muthiah Pillai talks ill about Nambirajan’s poverty and insults him. Muthiah Pillai says he would wed his son with Nambirajan’s sister only when Nambirajan becomes a rich person. He also says he would wed his daughter (Abitha) to Nambirajan at that time. Nambirajan accepts this challenge. So he decides to stand in election as an individual party.

==Cast==

 
 
*Sathyaraj as Nambirajan
*Goundamani as Visky
*Prakash Raj as Ilampirai
*Cochin Haneefa Mantra
*Abitha
*Thilakan
*Nassar as Arivudaiyan
*Rajan P. Dev as Muthiah Pillai
*Rakesh as Rakesh
*Ammu as Priya
  Ponnambalam
*Delhi Ganesh
*Anu Mohan
*Besant Ravi
*Babilona
*Brinda Parekh
*Shakeela
*Srilekha
*K. P. A. C. Lalitha
*Paravai Muniyamma
*Anjali Devi
 

==Production==

===Development=== Maaman Magal, Guru Dhanapal teams up for the third time with the duo Sathyaraj and Goundamani. Sathyaraj said, "I feel the film will certainly entertain viewers as it will be an out-and-out laugh riot". The director, Guru Dhanapal, said "Sathyaraj gets maximum mileage of the story with his histrionic and comedy talent".   

===Casting===
Sathyaraj pairs with Raasi (actress)|Mantra. Prakash Raj plays a big role and the rest of the cast includes Thilakan, Nassar, Rajan P. Dev, Cochin Haneefa, Ponnambalam (actor)|Ponnambalam, Delhi Ganesh, and a new find Rakesh. Sabesh-Murali composed the film soundtrack and background music, while D. Shankar and R. Suresh Rajhan were the cinematographer and the editor respectively. 

===Filming===
The film has completed and was ready for the release, and at this time, the LIC Housing Finance has taken the film to court. The official had stated that Stanley P. Rajan and his associates, who jointly produced the film, had cheated the company of Rs. 50.39 lakhs. According to him, Mr. Rajan had obtained a housing loan of Rs. 25 lakhs by submitting fake address and false particulars in his application. His associates too applied for and availed themselves of the loan using a similar modus operandi. After obtaining the housing loans, they diverted the sum for film production without using for building houses.  

==Soundtrack==

{{Infobox album |  
| Name        = Suyetchai MLA
| Type        = soundtrack
| Artist      = Sabesh-Murali
| Cover       = 
| Released    = 2006
| Recorded    = 2004 Feature film soundtrack |
| Length      = 12:46
| Label       = 
| Producer    = Sabesh-Murali
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sabesh-Murali. The soundtrack, released in 2004, features 3 tracks with lyrics written by Gangai Amaran.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Irungattu Kottai || Manikka Vinayagam, Kavitha Gopi || 4:13
|- 2 || Mera Mera Mera Ooru || Kavitha Gopi || 4:19
|- 3 || Puli Varuthu Puli Varuthu || Mahathi || 4:14
|}

==References==
 

 
 
 
 