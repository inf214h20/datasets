Zappa (film)
 
{{Infobox film
| name           = Zappa
| caption        = Film poster
| image	         = Zappa FilmPoster.jpeg
| director       = Bille August
| producer       = Per Holst
| writer         = Bille August Bjarne Reuter
| starring       = Adam Tønsberg
| music          =
| cinematography = Jan Weincke
| editing        = Janus Billeskov Jansen
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}
 Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Adam Tønsberg as Bjørn
* Morten Hoff as Mulle
* Peter Reichhardt as Steen
* Lone Lindorff as Bjørns mother
* Arne Hansen as Bjørns father Thomas Nielsen as Henning
* Solbjørg Højfeldt as Steens mother
* Bent Raahauge Jørgensen asSteens father (as Bent Raahauge)
* Inga Bjerre Bloch as Mulles mother
* Jens Okking as Mulles father
* Elga Olga Svendsen as Bjørns grandmother
* Willy Jacobsen as Bjørns Grandfather
* Ulrikke Bondo as Kirsten
* Søren Frølund as Teacher Kaalormen
* Michael Shomacker as Asger

==See also==
*List of Danish submissions for the Academy Award for Best Foreign Language Film
*List of submissions to the 56th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 