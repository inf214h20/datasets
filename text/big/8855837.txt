Al-Manzel Raqam 13
{{Infobox film
| name           = Al-Manzel Raqam 13  المنزل رقم 13 
| image          = House-no-13.jpg
| image size     =
| caption        = Al-Manzel Raqam 13 poster
| director       = Kamal El Sheikh
| producer       =
| writer         =
| starring       = Faten Hamama Mahmoud El-Meliguy Emad Hamdy
| music          =
| cinematography =
| distributor    = 1952
| runtime        =
| country        = Egypt
| language       = Arabic
| budget         =
}}

Al-Manzel Raqam 13 or Al-Manzel Raqam Talata`sh ( , House No. 13) is a classic 1952 Egyptian mystery/crime film directed by Kamal El Sheikh. It starred Faten Hamama, Mahmoud El Meliguy, and Emad Hamdy and was chosen as one of the best 150 Egyptian film productions in 1996, during the Egyptian Cinema centennial.

== Plot ==

A psychotic psychiatrist has killed a young man, `Abbas, and plans a conspiracy. He convinces his friend and patient, Sharif, that he had killed `Abbas after hypnotizing him. He also orders Sharif to give him the money that `Abbass wife should receive, all to make Sharif seem like the suspect. Sharif is believed to have killed `Abbas for the money, and is shockingly arrested during his wedding.

== References ==
*  , Faten Hamamas official site. Retrieved on January 10, 2007.
*  , Aramovies. Retrieved on January 10, 2007.

== External links ==
*  

 
 
 
 
 
 


 
 