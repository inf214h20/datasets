Whatever Works
 

{{Infobox film
| name           = Whatever Works
| image          = Whatever works.jpg
| caption        = Promotional film poster
| director       = Woody Allen
| producer       = Letty Aronson Stephen Tenenbaum
| writer         = Woody Allen
| starring       =  Ed Begley, Jr. Patricia Clarkson Larry David Michael McKean Evan Rachel Wood
| music          =  
| cinematography = Harris Savides
| editing        = Alisa Lepselter Wild Bunch
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $35,085,646 
}}
Whatever Works is a 2009 American comedy film directed and written by Woody Allen, starring Larry David, Evan Rachel Wood, Patricia Clarkson, Ed Begley, Jr., Michael McKean, and Henry Cavill.

==Plot== Columbia professor. Divorced, he eschews human contact except for his friends (Michael McKean, Adam Brooks, Lyle Kanouse) and students, criticizing everyone he meets for not matching him intellectually.
  southern background, fundamentalist parents in Mississippi and ran away from them. She asks if she can stay the night, which Boris eventually allows, and she stays with him while shes looking for a job. Melodie develops a crush on Boris despite their age difference and their varying cultures and intelligence.

Melodie finds a job as a dog walker while still living with Boris. Out on the job, she meets Perry (John Gallagher, Jr.) and they arrange a date. When she comes back home, she explains to Boris that she didnt like Perry because he loved everything in the world too much. Boris realizes that he loves her and they get married.

After a year passes, her mother Marietta (Patricia Clarkson) finds Melodie, explaining that she and her husband John (Ed Begley, Jr.) thought Melodie had been kidnapped. She goes on to tell her that John left her and sold their house after John lost money in the stock market. She meets Boris and is disappointed with him, so she tries to convince Melodie to end her marriage. The three go for lunch at a restaurant and meet Boris friend, Leo (Conleth Hill). As Marietta goes to use the restroom, Randy James (Henry Cavill) inquires about Melodie. Marietta slyly decides to recruit him to end Melodies marriage. Later that evening, Leo, who had taken an interest in Marietta, asks her over for dinner. They spend the evening together, and they both discover that she is a wonderful photographer and he even makes plans to contract her professionally.

Boris explains to the audience that the next few weeks, Marietta changed and started experimenting in artistic photography, exotic new habits, and having an open relationship with Leo and his business partner, Morgenstern (Olek Krupa). Marietta still hates Boris and continues to arrange for Melodie to marry Randy. She takes her to an outdoor craft market and "accidentally" runs into Randy, who questions her about her marriage. Melodie initially sees past Mariettas attempt and tells him that her marriage is fine. She warns her mother to stop her attempt at once, but Marietta keeps trying. Later shopping for clothes, Melodie meets Randy in  another planned encounter with her and gets her to admit that her relationship with Boris is not entirely satisfying. He invites her to the boat he lives on, and the two end up kissing and beginning an affair.

John arrives at Boris and Melodies home full of regret and hopes to get the family back together. They all go to Mariettas photography exhibit opening together, and he sees how his ex-wife has changed since she moved to New York. Distraught, he retreats to a bar, drinking away his misery. While there, he meets a recently divorced gay man named Howard (Christopher Evan Welch), and realizes that he is also gay.

Melodie tells Boris she is in love with Randy. Boris is disheartened by this and jumps out a window again, but this time lands on Helena (Jessica Hecht) breaking her arm and leg. As he visits her in the hospital, he asks her if there is anything he can do to make up with her, and Helena says she would like to go to dinner with Boris.

Finally, Boris hosts a New Years Eve party, at which everyone is seen in their new relationships: Marietta with Leo and Morgenstern, John with Howard, Melodie with Randy, and finally Boris with Helena. Melodie and her parents had, each one separately, completely shed their former southern conservative mindsets and wholeheartedly adopted the liberal New York way of life and values. (John tells that his former membership in the National Rifle Association had been but a sublimation of his repressed homosexuality.) They are now all the best of friends, and at midnight heralding a new year they kiss and Boris tells the audience that you just have to find all the enjoyment that you can, that you have to find "whatever works".

==Cast==
* Samantha Bee as Chess Mother
* Ed Begley, Jr. as John Celestine Adam Brooks as Boris Friend
* Henry Cavill as Randy Lee James
* Patricia Clarkson as Marietta Celestine
* Larry David as Boris Yelnikoff
* Jessica Hecht as Helena
* Conleth Hill as Leo Brockman 
* Lyle Kanouse as Boris Friend
* Olek Krupa as Morgenstern
* Carolyn McCormick as Jessica
* Michael McKean as Joe
* Christopher Evan Welch as Howard Cummings (née Kaminsky)
* Evan Rachel Wood as Melodie St. Ann Celestine

==Release==
On February 2, 2009, Variety magazine|Variety reported that Sony Pictures Classics had purchased U.S. distribution rights to Whatever Works. It premiered at the Tribeca Film Festival in New York City,  on April 22, 2009. Sony gave the film a limited US release on June 19, 2009. Maple Pictures released the film in Canada theatrically and released the DVD in October 2009. The film had its UK release on June 25, 2010.   

==Production==
The film was shot in New York City, marking Allens return to his native city after shooting four films in Europe. David was hesitant to take the role, pointing out to Allen that his work on Curb Your Enthusiasm was improvisation, but Allen encouraged him to take the role anyway.   
 url = http://www.starpulse.com/news/index.php/2009/06/16/woody_allen_larry_david_evan_rachel_wood title = Woody Allen, Larry David, Evan Rachel Wood & Others Discuss Whatever Works work = Starpulse Entertainment News date = accessdate = 2009-06-19
 }} 

==Soundtrack==
* "Hello, I Must Be Going (song)|Hello, I Must Be Going" - Groucho Marx and Cast
* "Salty Bubble" - Tom Sharpsteen and His Orlandos
* "Butterfly By" - Heinz Kiessling
* "Honeymoon Swoon" - Werner Tautz
* "If I Could Be With You (One Hour Tonight)" - Jackie Gleason
* Symphony No. 9 (Beethoven) in D Minor, Op. 125  - Royal Philharmonic Orchestra
* Wedding March - Royal Philharmonic Orchestra
* Symphony No. 5 (Beethoven) in C Minor  - Royal Philharmonic Orchestra
* "Desafinado" - Stan Getz and Charlie Byrd
* "Spring Will Be A Little Late This Year" - Red Garland
* "Menina Flor" - Charlie Byrd
* Auld Lang Syne - Dick Hyman & His Orchestra
* "Happy Birthday To You" - Larry David

==Reception==
The film received mixed or average reviews from critics.    Rotten Tomatoes reported that 49% of critics gave positive reviews based on 160 reviews with an average score of 5.4/10.  According to another review aggretator, Metacritic, which assigns a normalized rating out of 100 top reviews from mainstream critics, the film has an average score of 45% based on 30 reviews. 

Upon the films US release, A. O. Scott of The New York Times wrote:  Lubitschean verve of which Mr. Allen, when he’s on his comic game, is capable. To be sure there is a measure of vigor in Larry David’s performance in the central role of existential kvetcher, a formerly eminent physicist named Boris Yelnikoff. Mr. David does a lot of shouting and some antic gesticulating, and even throws himself out a window. But frantic action is not the same as acting, and there is barely a moment in Whatever Works in which Mr. David rises even to the level of credible impersonation. 

==See also==
*Uncertainty principle

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 