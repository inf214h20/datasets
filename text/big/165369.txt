This Is Cinerama
 
{{Infobox film
| name =This Is Cinerama
| caption =
| image	=	This Is Cinerama FilmPoster.jpeg
| director = Merian C. Cooper
| producer = Robert L. Bendick Merian C. Cooper
| based on = 
| screenplay = 
| starring = Lowell Thomas
| music = Miklós Rózsa
| cinematography = Harry Squire
| editing = William Henry  Milton Shifman
| distributor = 
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| budget = 
| gross = $12.5 million (est. US/ Canada rentals) 
}}
 aspect ratio so the viewers peripheral vision is involved.  This is Cinerama premiered on 30 September 1952 at the New York Broadway theatre, in New York City.

==Synopsis==
The film begins in black-and-white and in standard 4:3 aspect ratio, as travel writer and newscaster Lowell Thomas appears on screen to discuss the evolution of motion picture entertainment, from the earliest cave paintings designed to suggest movement, up to the introduction of color and sound. At the conclusion of the 12-minute lecture, Thomas speaks the words "This is Cinerama" and the screen expands into the full Cinerama aspect ratio and colour as a series of vignettes, narrated by Thomas begin.

The film includes scenes of the roller coaster from Rockaways Playland, then moves on to a scene of the temple dance from Aida, views of Niagara Falls, a performance by a church choir (out of pace with the rest of the film, this segment is shot in black and white), a performance by the Vienna Boys Choir, scenes of the canals of Venice, a military tattoo in Edinburgh, a Bull fighting|bullfight, more from Aida, a sound demonstration in stereophonic sound|stereo, scenes from Cypress Gardens amusement park including a water skiing show,  and the playing of America the Beautiful as scenes are shown from the nose of a low flying B-25 Mitchell|B-25.

The producers were Thomas, Merian C. Cooper, and Robert L. Bendick, directed by Bendick (and an uncredited Mike Todd, Jr.). Cooper had long experience with technical innovation in cinema, including King Kong.

==Awards== Academy Award Best Music, Scoring of a Dramatic or Comedy Picture. Although the score was credited to Louis Forbes, who conducted the music, it was composed by Paul Sawtell, Max Steiner (who composed the opening credit sequences, Cypress Gardens, the Flight Across America, and the End Credits) and Roy Webb.
 Library of Congress deemed the film "culturally, historically, or aesthetically significant" and inducted it into the National Film Registry for permanent preservation.

==Distribution== road show.
 How the West was Won, and will be accompanied by an Audio Commentary and an assortment of other special features. On October 18, 2012, the Turner Classic Movies cable network aired This is Cinerama in what was announced as the first American TV broadcast of the film in its original aspect ratio (again, in the Smilebox format).

The films copyright status is unclear. It apparently fell into the public domain in 1980, 28 years after being filed for copyright, but some allege that it was renewed, erroneously, a matter of months after this expiration.

==See also==
*Cinerama
*Cinerama Adventure

==Notes==
 

== External links ==
*  

 

 
 
 
 