Tokyo Olympiad
 
{{Infobox film
| name           = Tokyo Olympiad
| image          = Tokyoolympiadposter.jpg
| caption        = North American release poster
| director       = Kon Ichikawa
| producer       = Suketarō Taguchi
| writer         = Kon Ichikawa Ishio Shirasaka Shuntarō Tanikawa Natto Wada
| narrator       = Ichiro Mikuni
| starring       =
| music          = Toshiro Mayuzumi
| cinematography = Kazuo Miyagawa
| editing        = Tatsuji Nakashizu
| studio         =Organizing Committee for the Games of the XVIII Olympiad Toho Company
| distributor    = Toho (USA) Actueel Film (1965) (Netherlands) Dino de Laurentiis Distribuzione (1966) (Italy) American Broadcasting Company(1968) (USA) (TV) Criterion Collection, The (2004) (USA)
| released       =  
| runtime        = 170 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
 1965 documentary film directed by Kon Ichikawa which documents the 1964 Summer Olympics in Tokyo. Like Leni Riefenstahls Olympia (1938 film)|Olympia, which documented the 1936 Summer Olympics in Berlin, Ichikawas film was considered a milestone in documentary filmmaking. However, Tokyo Olympiad keeps its focus more on the atmosphere of the games and the human side of the athletes instead of concentrating only on the winners and the results. It is one of the few sports documentaries included in the book 1001 Movies You Must See Before You Die.

==Production history==
The 1964 Summer Olympics were seen as vitally important to the Japanese government. Much of Japans infrastructure had been destroyed during World War II and the Olympics were seen as a chance to re-introduce Japan to the world and show off its new modernised roads and industry as well as its burgeoning economy. Every Olympics since the first modern games in 1896 Summer Olympics had been committed to film to some extent or another, usually financed by the International Olympic Committee for reasons of posterity. For the 1964 Olympics the Japanese government decided to finance their own film and initially hired Akira Kurosawa who, at the time, was the most famous Japanese director worldwide thanks to films such as Ikiru and Seven Samurai. However, Kurosawas famous tendency for complete control - he demanded to not only direct the film but the opening and closing ceremonies as well - led to his dismissal. This led to the bringing in of Ichikawa, who had a reputation of coming into productions where events hadnt followed the initial plans.

==Controversy==
Ichikawas vision of the Tokyo Olympics was controversial at the time as it was the opposite of what the Japanese government wanted and expected of the film. Ichikawa presented a film which was very much a cinematic and artistic recording of the events, more concerned with the athletes than the events, than the journalistic, historical recording that was desired by its financiers. As a result, the Japanese Olympic Committee forced Ichikawa to re-edit the picture to better suit their requirements, with the final, re-edited, version clocking in at 93 minutes rather than the originals 170 minutes.

==Reception==
The film is held in very high regard and is seen, alongside Leni Riefenstahls Olympia (1938 film)|Olympia, as one of the best films about the Olympics and one of the best sports documentaries of all time. Based on 11 reviews collected by the film review aggregator Rotten Tomatoes, 100% of critics gave the film a positive review. 

==Availability==
The film is somewhat difficult to find in the west, with the Criterion Collection DVD version out of print. It can be found on eBay fairly regularly, however, often fetching prices of between £35/$70. It can also be found on video from Homevision in the US and Tartan Video in the UK, though these too are out of print and can fetch prices of upwards of £30/$60.

==Other Official Films of the Olympic Games==
* Olympia (1938 film)|Olympia (1938), directed by Leni Riefenstahl about Berlin 1936 Rome 1960 Munich 1972
* 16 Days of Glory (1986), directed by Bud Greenspan about Los Angeles 1984

==See also==
 
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 