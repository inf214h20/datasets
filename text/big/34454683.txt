Agent 3S3, Massacre in the Sun
{{Infobox film
| name           = Agent 3S3, Massacre in the Sun
| image          = Agent 3S3, Massacre in the Sun.jpg
| image size     =
| caption        =
| director       = Sergio Sollima
| producer       =  
| writer         =  
| narrator       =
| starring       =
| music          =Piero Umiliani
| singer         = Orietta Berti
| cinematography = Carlo Carlini
| editing        = Bruno Mattei
| distributor    =
| released       =   1966
| runtime        =
| country        =   Italian
| budget         =
}}

Agent 3S3, Massacre in the Sun (originally titled Agente 3S3, massacro al sole, also known as Agent 3S3: Hunter from the Unknown and Hunter from the Unknown) is a   (1965) with Agent 3S3 once again played by George Ardisson. Orietta Berti, a popular italian singer, sings in english the films theme "Trouble galore".    

This was filmed on locations in Berlin and Ibiza.  It was coproduced by France, where it was released as Agent 3S3, massacre au soleil, and Spain, where is known as 3S3, agente especial and Agente 3S3 enviado especial.

==Cast==
*George Ardisson 	as	 Walter Ross, Agent 3S3 Frank Wolff  	as	Ivan Tereczkov 
* Evi Marandi  	as	 Melissa   
* Michel Lemoine  	as		Radek 
* Fernando Sancho  	as		General Siqueiros 
* Luz Márquez 	as	Miss Barrientos 
* Eduardo Fajardo 	as	Professor Karlesten 
* Leontine May  	as	Josefa	 
* John Karlsen 	as	Tereczkovs Boss 
* Kitty Swan 	as	Night-club Singer (as Kersten Svanhold)
* Salvatore Borghese   
* María Granada

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 