Aai Tuza Ashirwad
 

{{Infobox film
| name           = Aai Tuza Ashirwad
| image          = Aai Tuza Ashirwad.png
| alt            =  
| caption        = Theatrical release poster
| director       = Mayur Vaishnav
| producer       = Murli Nallapa
| writer         = 
| starring       = Alka Kubal Ashok Saraf Ramesh Bhatkar
| music          = Prakash More
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Aai Tuza Ashirwad is a Marathi film, which released on 9 August 2004.  The movie produced by Murli Nallapa and directed by Mayur Vaishnav, is based on an emotional saga between a mother and her son.

== Synopsis ==
Suraj is raised with great love and care by his mother Urmilla and grandfather Raosaheb. While Urmilla adores Suraj, Raosaheb is particularly proud of his intelligence as well as sporting skills. However, they both dislike Surajs habit of lying. Every time he lies, Urmilla devoids all communication with Suraj as punishment. Unable to tolerate his mothers silence, Suraj vows never to lie again. Nevertheless, things take a dramatic turn when Suraj is forced to break his promise and instead of punishment, he incurs Urmillas blessings. What is this lie that he is forced to utter….?

== Cast ==

The cast in this movie includes Alka Kubal, Ashok Saraf, Ramesh Bhatkar & Others

==Soundtrack==
Singing stalwarts Lata Mangeshkar and Pandit Jasraj have teamed up for the first time to record a song this movie. 
The music has been directed by Prakash More and the songs in the movie are:

===Track listing===
{{Track listing
| title1 = Aai Tuza Aashirwad | length1 = 4:21
| title2 = Nij Re Nandlala | length2 = 3:32
| title3 = Harinamacha Mahima Sarya Jagbhar | length3 = 2:51
| title4 = Chala Chala Darshanala Ashtvinayaka Chala | length4 = 5:49
}}

== References ==
 
 

== External links ==
*  
*  
*  

 
 