Odukkam Thudakkam
{{Infobox film 
| name           = Odukkam Thudakkam
| image          =
| caption        =
| director       = Malayattoor Ramakrishnan
| producer       = MO Joseph
| writer         = Malayattoor Ramakrishnan
| screenplay     = Malayattoor Ramakrishnan Rajkumar K. P. Ummer
| music          = G. Devarajan
| cinematography = Vipin Das
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Rajkumar and K. P. Ummer in lead roles. The film has a musical score by G. Devarajan.   

==Cast==
*Ratheesh
*Kalaranjini Rajkumar
*K. P. Ummer
*Nanditha Bose

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Malayattoor Ramakrishnan, P. Bhaskaran and Pulamaipithan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaromale amale aaradhike azhake || K. J. Yesudas || Malayattoor Ramakrishnan || 
|-
| 2 || Ente Sankalpa mandaakini || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Kaalai vantha sooriyane || P. Madhuri, Chorus || Pulamaipithan || 
|}

==References==
 

==External links==
*  

 
 
 

 