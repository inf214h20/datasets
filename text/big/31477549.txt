Disturbed (film)
{{Infobox Film
| name           = Disturbed
| image          = Disturbed 1990 movie poster.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Charles Winkler
| producer       = Patricia Foulkrod Brad Wyman
| writer         = Emerson Bixby Charles Winkler Geoffrey Lewis Priscilla Pointer
| music          = Steven Scott Smalley
| cinematography = Bernd Heinl
| editing        = David Handman
| distributor    = Second Generation Films
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
Disturbed is a 1990 American horror film directed by Charles Winkler starring Malcolm McDowell as a psychiatrist who rapes a young woman in his care, then must deal with her vengeance-seeking daughter 10 years later.

==Plot==
Dr. Derrick Russell (Malcolm McDowell) rapes one of the patients in his care. When she throws herself from the roof shortly afterward, he describes her suicide as a consequence of her clinical depression|depression. Ten years later, he plans to rape another patient, Sandy Ramirez (Pamela Gidley). What Russell does not know is that Sandy is the daughter of his previous victim, and that she is bent on revenge. A post-credit sequence depicts a man kissing the camera before he laughs.

==Cast==
* Malcolm McDowell as Dr. Derrick Russell Geoffrey Lewis as Michael Kahn
* Priscilla Pointer as Nurse Francine
* Pamela Gidley as Sandy Ramirez
* Irwin Keyes as Pat Tuel
* Clint Howard as Brian

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 