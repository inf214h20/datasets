The Fox with Nine Tails
{{Infobox film
| name           = The Fox with Nine Tails
| image          = The Fox with Nine Tails film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Gumiho
 | mr             = Kumiho }}
| director       = Park Heon-su
| producer       = Shin Chul Yu Hyeok-ju Ha Gwang-hwi
| writer         = Park Heon-su
| starring       = Ko So-young Jung Woo-sung
| music          = Lee Dong-jun
| cinematography = Ku Jung-mo
| editing        = Park Soon-duk
| distributor    = ShinCine Communications
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
}} 1994 South Korean film, and was the feature debut of director Park Heon-su.  It was also the film debut of lead actors Ko So-young and Jung Woo-sung, who would later star together in Beat (1997 film)|Beat (1997) and Love (1999).  

== Plot ==
Harah is a kumiho in the guise of a beautiful young woman, who desperately desires to become human. She falls in love with a charming taxi driver, Hyuk, and tries to use him to achieve her goal. But an agent from hell has been sent to track down and destroy her.

== Cast ==
* Ko So-young ... Harah
* Jung Woo-sung ... Hyuk
* Dokgo Young-jae
* Bang Eun-hee
* Lee Ki-young
* Lee Gun-hee
* Seo Gi-woong
* Kwon Hae-hyo
* Ahn Suk-hwan
* So Il-seop

== Production == Korean film industry by pioneering the fantasy genre and using chaebol funds from the Byuksan Group to cover the budget.  The opening scenes of the film depicting hell used approximately 200 Extra (actor)|extras, with the set costing in the region of   ( ). 

== Release ==
The Fox with Nine Tails was released on 24 July 1994.   

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 

 