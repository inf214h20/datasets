Randilonnu
{{Infobox film
| name           = Randilonnu
| image          =
| caption        =
| director       = AS Prakasam
| producer       =
| writer         =
| screenplay     =
| starring       = Sukumaran Ravi Menon Ushakumari Jayageetha
| music          = M. S. Viswanathan
| cinematography = Kanniyappan
| editing        = Prasad Rao
| studio         = United Enterprises
| distributor    = United Enterprises
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed AS Prakasam. The film stars Sukumaran, Ravi Menon, Ushakumari and Jayageetha in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*Sukumaran
*Ravi Menon
*Ushakumari
*Jayageetha
*Chandralekha Sadhana
*Kaviyoor Ponnamma
*KPAC Lalitha
*Prathapachandran Kunchan
*Sankaradi
*Master Sunil
*Master Rajan
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Mankombu Gopalakrishnan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Karanam Thettiyal || Jolly Abraham || Mankombu Gopalakrishnan ||
|-
| 2 || Love Me Like || P Jayachandran, LR Anjali || Mankombu Gopalakrishnan ||
|-
| 3 || Panchavankaattile || S Janaki, M. S. Viswanathan || Mankombu Gopalakrishnan ||
|-
| 4 || Panchavankaattile   || S Janaki, M. S. Viswanathan || Mankombu Gopalakrishnan ||
|-
| 5 || Thaarake Rajatha Thaarake || Vani Jairam || Mankombu Gopalakrishnan ||
|}

==References==
 

==External links==
*  

 
 
 


 