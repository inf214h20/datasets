City on Fire (1987 film)
 
 
 
{{Infobox film 
| name = City on Fire
| image = CityOnFire1987.jpg
| image_size =
| caption = Hong Kong theatrical poster
| director = Ringo Lam
| producer = Karl Maka Ringo Lam
| writer = Ringo Lam Danny Lee Chow Yun-fat Sun Yueh Carrie Ng Roy Cheung
| music = Teddy Robin Kwan
| cinematography = Andrew Lau
| editing = Wong Ming-Lam
| distributor = Cinema City Film Co. Ltd.  (Hong Kong) 
| released =   
| runtime = 98 min.
| country = Hong Kong Mandarin 
| budget = 
| gross = Hong Kong HK$ 19,723,505
}}
City on Fire ( ) is a 1987 Hong Kong crime film written, produced and directed by Ringo Lam.

==Plot== undercover cop who is under pressure from all sides. His boss, Inspector Lau (Sun Yueh), wants him to infiltrate a gang of ruthless jewel thieves; in order to do this he must obtain some handguns; his girlfriend (Carrie Ng) wants him to commit to marriage or she will leave Hong Kong with another lover; and he is being pursued by other cops who are unaware that he is a colleague.

What is more Chow would rather quit the force. He feels guilty about having to betray people who have become his friends, even if they do happen to be killers, drug dealers, loan sharks and protection racketeers: "I do my job, but I betray my friends."

To add to his problems, he begins to bond with Fu (Danny Lee), a member of the gang.

==Cast== Danny Lee Sau-yin as Fu
*Chow Yun-fat as Chow
*Sun Yueh as Inspector Lau / Uncle Kung
*Carrie Ng as Hung
*Roy Cheung as Inspector John Chan
*Maria Cordero as Lounge Singer
*Jessica Chau as Lily
*Yeh Fang as Chow Nam
*Victor Hon as Bill
*Kong Lau as Inspector Chow
*Elvis Tsu as Chan Kam-wah
*Wong Kwong-leung as Kwong
*Cheng Mang-Ha
*Parkman Wong
*Jessica Chow
*Ringo Lam

 
 

==Production==
Filming began in Hong Kong in 1985 and concluded around the Christmas season

==Reception==
City on Fire is Ringo Lams most celebrated work. The film has been critically acclaimed, holding a 91% "fresh" rating at Rotten Tomatoes. 

==Influence== 1992 film Michigan film student created a 1995 short film, Who Do You Think Youre Fooling?, which mixed dialog and visuals from both movies to demonstrate the similarities.  In addition to Reservoir Dogs, critic Matt McAllister notes that one "can equally see the influence of City On Fire - and similar Hong Kong cops-and-robbers movies - on many other Hollywood undercover cop movies such as Point Break."  
 The Killer, Chow plays a hitman who bonds with Lee, this time appearing as the cop.

==References==
 

==External links==
*  
*  
*  
* An   by Ron Lim on the similarities between City on Fire and Reservoir Dogs
* A   from hkfilm.net that also outlines the similarities between both movies.
*  

 

 
   
 
 
 
 
   
 
 
    
 
 