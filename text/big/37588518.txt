Any Day Now (2012 film)
 
{{Infobox film
| name           = Any Day Now
| image          = Any Day Now (2012 Film).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Travis Fine
| producer       = Travis Fine Kristine Fine Liam Finn Chip Hourihan
| writer         = Travis Fine George Arthur Bloom
| starring       = Alan Cumming Garret Dillahunt Gregg Henry Jamie Anne Allman Chris Mulkey Don Franklin Kelli Williams Alan Rachins Frances Fisher Isaac Leyva 
| music          = Joey Newman
| cinematography = Rachel Morrison Tom Cross
| studio         = PFM Pictures
| distributor    = Music Box Films
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $201,395 
}}
Any Day Now is  a 2012 American drama film directed by Travis Fine who rewrote the original screenplay that George Arthur Bloom had written 30 years previously.

==History==
Bloom describes the extraordinary story behind getting the movie made:
"The screenplay for Any Day Now was inspired by a true story – not based on a true story. I wrote the original script 30 years ago. A friend of mine in NY introduced me to a gay man named Rudy. Rudy lived on Atlantic Ave in Brooklyn. At that time, Atlantic Ave was pretty rundown. It has been gentrified since then. Rudy lived in a tiny apartment and had very little money. He befriended a 12-year old boy who lived a few blocks away. The boy had been abandoned by his druggie/prostitute mother, and lived with his grandmother. The grandmother didn’t do much to provide for the boy, who didn’t speak. I’m guessing he was Autistic, but there was no money to do anything about it. Rudy would bring the boy to his apartment, see to it that he was properly clothed and fed, and he did what he could to get him into school. He practically raised him. That is where reality ended and my writer’s imagination took over. After spending time with Rudy and the boy, I got to wondering what would happen if Rudy decided to adopt him. I did my research and spoke to a number of people about the problems a gay man would have adopting a boy. Remember, this was 1980. The times were a lot different then, although we still have a long way to go. Several months later I had a screenplay."

Despite having a compelling story, Bloom would have to wait 32 years to actually see the movie made. He explains:
"My son, PJ, is one of the top Music Supervisors in LA, as well as a record producer and publisher. Among other shows, PJ is the Music Supervisor on GLEE. Travis Fine, the director of Any Day Now, made another independent movie 3 years ago called THE SPACE BETWEEN. Travis and PJ were friends in high school. When Travis needed help with the music on his movie he contacted PJ. When the movie was done, Travis told PJ he was looking for another movie to do, something small, with heart, and about something important. PJ, who has known about my script his entire adult life, told Travis the story. Travis loved it, and asked that I send him the screenplay. I did, and he said he wanted to make it, with the caveat that he could do some rewriting. We discussed that, and I agreed. Travis did his rewrite, raised the money, hired the actors, and made the movie. If you discount the first 30 years of trying to get the movie made, the last couple have gone by quickly. I love everything Travis did to the script, and he’s made a marvelous movie. He changed the boy who didn’t speak to a boy with Down Syndrome, and cast an extraordinary Down Syndrome actor to play the part. Travis made several other significant changes, but the heart and soul of my screenplay remain as the anchor to the movie."  

==Plot== Family Services intervenes and takes Marco to foster care. Rudy enlists Paul to help him gain custody of Marco and the two visit Marianna in prison to coax her into signing the temporary guardianship papers, which she does. All is well as Rudy and Paul become Marcos guardians, but when Rudy and Pauls relationship is called into question by the court system, the two men find themselves spiraling into a legal battle to become the legal and permanent guardians of the fascinating boy who showed them both the real joy of what it means to be a parent. After having their home (Marcos living environment) evaluated, the men are put in front of a judge who is to decide whats best for the child. The evaluation comes back positive and it is decided that Rudy and Paul are great parents for Marco. However, just as the court is about to rule in favor of the men, Marcos mother is released from prison. She takes back custody of Marco, leaving Rudy and Paul without their son. Marco is heard saying as he is taken back to his mothers apartment, "this is not my home, this is not my home." As expected, Marianna returns to her old ways (using drugs, sleeping around, etc) and fails to take care of Marco. One night as she is having sex, Marianna tells Marco to step outside of the apartment. Marco begins to wander the streets in search of Pauls house (his true home). However, Marco is unable to find the house and dies outside, alone. The movie ends with a letter/monologue from Paul. The letter, which contained Marcos obituary, was sent to all who doubted the couple as Marcos parents. The hope was for them to realize the mistake they made that ended in this boys untimely death.

==Cast==
* Alan Cumming as Rudy Donatello
* Garret Dillahunt as Paul Fliger
* Gregg Henry as Lambert
* Jamie Anne Allman as Marianna DeLeon
* Chris Mulkey as District Attorney|D.A. Wilson
* Don Franklin as Lonnie
* Kelli Williams as Miss Flemming
* Alan Rachins as Judge Richard Resnick
* Frances Fisher as Judge Meyerson
* Isaac Leyva as Marco DeLeon
* Mindy Sterling as Miss Mills
* Miracle Laurie as Monica
* Michael Nouri as Miles Dubrow
* Jeffrey Pierce as Officer Plitt
* Anne OShea as Mrs. Lowell
* Randy Roberts as P.J.
* Louis Lombardi as Mr. Blum Joe Howard as Dr. Watkins
* Randy Thompson as Coco
* Ezra Buzzington as Larry
* Clyde Kusatsu as Dr. Nakahura
* Kamala Lopez as Miss Martinez

==Reception==
Any Day Now received generally positive reviews, currently holding a 80% "certified fresh" rating on Rotten Tomatoes. 

==Awards==
* Chicago International Film Festival 2012 - Audience Choice Award for Best Narrative Feature 
* Seattle International Film Festival 2012 - Best Actor Award, Alan Cumming    
* Seattle International Film Festival 2012 - Best Film 
* Tribeca Film Festival 2012 - Heineken Audience Award   
* Outfest 2012 - Audience Award - Outstanding Dramatic Feature Film   
* Outfest 2012 - Outstanding Actor in a US Dramatic Feature Film, Alan Cumming 
* Provincetown International Film Festival 2012 - Audience Award 
* Woodstock Film Festival 2012 - Audience Award 
*GLAAD Media Award 2012 - Best Film in Limited Release 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 