Jaane Kahan Se Aayi Hai
 
 
{{Infobox film
| name           = Jaane Kahan Se Aayi Hai
| image          = Jaane Kahan Se Aayi Hai poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Milap Zaveri
| producer       = Mukesh Talreja  Nikhil Advani
| writer         =
| starring       = Ritesh Deshmukh Jacqueline Fernandez Sonal Sehgal Ruslaan Mumtaz
| music          = Sajid-Wajid
| cinematography = Attar Singh Saini
| editing        = 
| studio         = People Tree Films
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =   
| gross          =  
}}
Jaane Kahan Se Aayi Hai is a 2010 Bollywood romance comedy|sci-fi romance film directed by Milap Zaveri. It stars Riteish Deshmukh, Jacqueline Fernandez, Sonal Sehgal and Ruslaan Mumtaz in lead roles. It was Warner Bros.s third Indian movie production. The film released on 9 April 2010, received mixed reviews from critics, and was declared under average grosser at the box office. Even though it was released in 2010, Jaane Kahan Se Aayi Hai has yet to receive a home video release.

==Plot==
Rejected by females since his birth, Mumbai-based Rajesh Parekh works as a clap-boy with Bollywood film-maker Farah Khan, and lives a wealthy lifestyle with his father, Ramnikbhai, and mother, Sushila. The country is agog with the popularity of Farahs latest mega-star, Desh, who has a huge female fan following. Rajesh meets and is attracted to Deshs sister, Natasha, but ends up with heartbreak when she rejects him. While watching a movie in a drive-in theater, a gorgeous amazon lands in his arms, and passes out. He takes her home, confides in his friend, Kaushal Milan Tiwari, and attempts to find out who she is. The mystery increases when she communicates that she is from Venus, and is on Earth to find true love. They decide to name her Tara, and show her photos of two of the hottest males on Earth - Brad Pitt and Desh. Since the former is already taken, and the latter single, they decide to try and woo him. But will a ego-stricken actor, who has thousands of gorgeous women willing to do his bidding, fall for an alien?

==Cast==
* Ritesh Deshmukh as Rajesh Parekh
* Jacqueline Fernandez as Tara
* Ruslaan Mumtaz as Desh
* Vishal Malhotra as Kaushal Milind Tiwari
* Sonal Sehgal as Natasha
* Satish Shah as Mr. Parekh
* Supriya Pilgaonkar as Mrs. Parekh
* Boman Irani as Doctor (cameo appearance)
* Amrita Rao as Tia (cameo appearance)
* Anvita Dutt Guptan as Journalist (uncredited appearance)
* Vijay Patkar as Waiter
Special appearances as themselves;
* Akshay Kumar
* Farah Khan
* Karan Johar
* Priyanka Chopra Sajid Khan

Thivan MBBS

==Reception==
The film received mixed reviews from critics. It holds a negative aggregate rating of 4.0/10 at ReviewGang.  However, it also received many positive reviews. Taran Adarsh from Bollywood Hungama rated it 2.5/5 and added "Jaane Kahan Se Aayi Hai is a time-pass movie... You won’t be that disappointed with this one." 

==Music==
The music Jaane Kahan Se Aayi Hai is composed by Sajid-Wajid. The soundtrack received notably mixed critical response and did well at stores. A review said, "They (the composer duo) are back to their usual stuff in Jaane Kahan Se Aayi Hai. The soundtrack has retention value have the potential to stand out." 
;Track listing Rashid Ali (05:01)
#"Nacha Main" – Sonu Nigam & Sowmya Raoh (04:53) Shaan (04:45)
#"Koi Rok Bhi Lo" – Sonu Nigam & Sunidhi Chauhan (05:09)
#"Jaane Kahan Se Aayi Hai" – Shaan (04:45)
#"Keh Do Zara (Remix)" – Rashid Ali (03:32)

==References==
 

==External links==
*  
* 

 
 
 
 