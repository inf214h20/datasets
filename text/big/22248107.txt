Color Me Olsen
Color short independent film directed and written by Darren Stein. 

==Plot== Dorothy and Pirate Johnny, they think this could be their big break. Crossing the gender divide, they take on the roles of mega-famous twin sisters Mary-Kate and Ashley Olsen, losing themselves in their "roles" of the tween superstars, until their fabricated personas threaten to overtake their own identities forever.

==Reviews==
After its showing at the Tribeca Film Festival, reviewer Daniel Montgomery characterized the film as "ugly, depressing and vicious," asking "Why would anyone write this, direct it, star in it, or watch it? It’s reprehensible and should be punishable by heavy fines and jail time. Grade: F."  

In contrast, online reviewer Scott Hoffman found the film "at first cheerfully amusing and entertaining," but concluded that it was "eerily similar to watching a great comedic sketch thats jettisoned to its punch line way too soon, with all the very important build up strangely absent."  

San Francisco Chronicle critic David Wiegand was "not all that fond" of "a concept in search of smarter execution than you get here."  

Another Tribeca reviewer, Ryan Stewart, described the film as one of two from Mood Enhancer (Tribecas short films program) that "some will come away loving...There are a handful of genuine laughs in this one". 

==Cast==
*Kelsey Sanders as Mary-Kate Olsen
*Jessica Amento	as Ashley Olsen
*Ken Barnett as Superman
*Kevin Berntson as Glitter Dad
*Clint Catalyst as Oompa Loompa
*Peggy Dunne as Glitter Woman
*Edmund Entin as Ashley / Taylor
*Gary Entin	as Mary-Kate / Hanson
*Dana Galinsky as Tourist Daughter
*Bruce Green as Pirate Johnny
*S.A. Griffin as Tourist Dad
*Liza Journo as Cupcake Girl
*Paul Ryjin as Sand
*Diane Salinger as Dorothy
*Rachel Winfree as Tourist Mom

==References==
 

==External links==
* 
* 

 
 
 
 
 