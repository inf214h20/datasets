David Copperfield (1969 film)
 
 
{{Infobox film
| name           = David Copperfield
| caption        =
| image	=	David Copperfield FilmPoster.jpeg
| director       = Delbert Mann
| producer       = Frederick H. Brogger
| writer         = Jack Pulman
| based on       =  
| starring       = Robin Phillips Ralph Richardson Ron Moody Laurence Olivier 
| music          = Malcolm Arnold
| cinematography = Ken Hodges
| editing        = Peter Boita
| distributor    =
| released       =  
| runtime        = 118 minutes 120 minutes (US)
| country        = United Kingdom
| language       = English
}} novel of the same name by Charles Dickens adapted by Jack Pulman, who later went on to adapt the Roman saga I, Claudius (TV series)|I, Claudius for BBC Television. The film was made in the UK for 20th Century Fox Television with some exteriors filmed in Suffolk.  Some interior scenes were filmed at The Swan Hotel in Southwold.

The film starred Robin Phillips in the title role and Ralph Richardson as Micawber. Among other well-known actors and actresses featured, some in cameo parts were Richard Attenborough, Laurence Olivier, Susan Hampshire, Cyril Cusack, Wendy Hiller, Edith Evans, Michael Redgrave and Ron Moody.

==Plot==
Charles Dickens Immortal Story of a Young Mans Journey to Maturity. This version finds David Copperfield (Robin Phillips) as a young man, brooding on a deserted beach. In flashback, David remembers his life in 19th century England, as a young orphan, brought to London and passed around from relatives, to guardians, to boarding school. He relives his struggle to overcome the loss of his idyllic childhood and the torment inflicted by his hated step-father after his mother’s death. Then virtually abandoned on the streets of Victorian London, David Copperfield is flung into manhood and contends bravely with the perils of big-city corruption and vice; hardships which ultimately fuel his triumph as a talented and successful writer.

==Cast==
*Richard Attenborough as Mr. Tungay
*Cyril Cusack as Barkis
*Edith Evans as Aunt Betsy Trotwood
*Pamela Franklin as Dora Spenlow
*Susan Hampshire as Agnes Wickfield
*Wendy Hiller as Mrs. Micawber
*Ron Moody as Uriah Heep
*Laurence Olivier as Mr. Creakle David Copperfield
*Michael Redgrave as Dan Peggotty
*Ralph Richardson as Mr. Micawber
*Emlyn Williams as Mr. Dick
*Sinéad Cusack as Emily
*James Donald as Mr. Murdstone James Hayter as Porter
*Megs Jenkins as Clara Peggotty
*Anna Massey as Jane Murdstone Andrew McCulloch as Ham Peggotty
*Nicholas Pennell as Thomas Traddles
*Corin Redgrave as James Steerforth
*Isobel Black as Clara Copperfield
*Liam Redmond as Mr. Quinion
*Donald Layne-Smith as Mr. Wickfield Christopher Moran as Boy Steerforth   Jeffrey Chandler as Boy Traddles
*Kim Craik as Child Emily
*Helen Cotterill as Mary Ann
*Alastair Mackenzie as Little David  

==DVD release==
The film is available on DVD.

==Notes==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 