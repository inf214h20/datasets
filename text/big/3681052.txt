Thousands Cheer
 
{{Infobox film
| name           = Thousands Cheer
| image          = Thousands cheer.jpeg
| caption        = Theatrical release poster
| director       = George Sidney
| producer       = Joe Pasternak Richard Collins John Boles Ben Blue Frances Rafferty
| music          = Irving Berlin Lorenz Hart Jerome Kern Nacio Herb Brown Richard Rogers George Gershwin Ira Gershwin Max Steiner Dmitri Shostakovich
| cinematography = George J. Folsey
| editing        = George Boemler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $1,568,000  .  gross = $5,886,000 
}}

Thousands Cheer is a 1943 American comedy musical film released by Metro-Goldwyn-Mayer. Produced at the height of the Second World War, the film was intended as a morale booster for American troops and their families.

==Plot== John Boles and Mary Astor) to reconcile. During the first part of the film, Grayson sings several numbers and Kelly performs one of his most famous routines, dancing with a mop as a partner.

The secondary plot involves preparations for a major live show for the soldiers which will feature many MGM musical and comedy stars. For the second half of the film, all pretenses of a storyline are effectively abandoned as the film instead becomes a variety showcase of comedy, song, and dance, with all of the performers (save Kelly and Grayson) appearing as themselves. The show portion is hosted by Mickey Rooney.

==Cast==
* Kathryn Grayson as Kathryn Jones  
* Gene Kelly as Private Eddie Marsh  
* Mary Astor as Hyllary Jones   John Boles as Colonel Bill Jones  
* Ben Blue as Chuck Polansky  
* Frances Rafferty as Marie Corbino  
* Mary Elliott as Helen Corbino  
* Frank Jenks as Sergeant Koslack  
* Frank Sully as Alan  
* Dick Simmons as Captain Fred Avery  
* Ben Lessy as Silent Monk

==Guest stars==
Performing as "guest stars" in the films show segment were: Judy Garland, Lena Horne, Red Skelton, Ann Sothern, Lucille Ball, Frank Morgan, Virginia OBrien, Eleanor Powell, Marilyn Maxwell, June Allyson, Gloria DeHaven, Donna Reed, Margaret OBrien, the Kay Kyser Orchestra and others. Pianist-conductor José Iturbi appears as himself in both segments of the film; this was his first acting role in a film and he would go on to make several more appearances (usually playing himself) in MGM musicals.

==Musical numbers== Honeysuckle Rose" Franz Liszts Rhapsodie #11.

"I Dug a Ditch in Wichita", a song told from the point of view of a soldier who used to dig ditches, is the movies underlying theme song, performed several times in the film with different arrangements and approaches, climaxing in the above-mentioned Kay Kyser performance. Grayson also sings a version, using an exaggerated (and out-of-character) "cowboy" accent, and Kelly dances to an instrumental version, using a mop as a partner.
 
After a brief resumption (and resolution) of the earlier storyline, the film ends with Grayson leading an international chorus of men (the United Nations Chorus) in a song pleading for world peace. The song, entitled "United Nations", actually predates the establishment of the United Nations political body by two years, but not the Declaration by United Nations which was made on 1 January 1942.

==Reception==
According to MGM records the film earned $3,751,000 in the US and Canada and $2,135,000 elsewhere resulting in a profit of $2,228,000. 
 Sempra Libera to Judy Garland and The Joint is Really Jumping!. It would have been easy for Metros labour to result in a top-heavy production under a less resourceful producer than Joe Pasternak. His steadying hand is quite evident."

The New York Herald Tribune : "a prodigal and sumptuous picture. It is   Kelly who saves the picture from being merely a parade of personalities - Judy Garland is attractive as she gets Iturbi to bang out some swing rhythms on the piano.  - George Sidney has staged it expansively." 

==Awards== Best Cinematography, Best Score Best Art Direction (Cedric Gibbons, Daniel B. Cathcart, Edwin B. Willis, Jacques Mersereau).   

==References==
 

==Further reading==
* 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 