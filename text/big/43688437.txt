Kill Me Three Times
 
 
{{Infobox film
| name           = Kill Me Three Times
| image          = Kill Me Three Times poster.jpg
| caption        = Theatrical release poster
| director       = Kriv Stenders
| producer       =  
| writer         = James McFarland
| starring       =  
| music          = Johnny Klimek
| cinematography = Geoffrey Simpson
| editing        = Jill Bilcock
| studio         =  
| distributor    = Magnet Releasing
| released       =  
| runtime        = 90 minutes
| country        = Australia United States
| language       = English
| budget         = 
}}

Kill Me Three Times is a 2014 American-Australian black comedy thriller film directed by Kriv Stenders. It was selected to be screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.    The film was released in the United States on April 10, 2015, by Magnet Releasing.

==Plot==
Set in Eagles Nest, Western Australia, private detective and assassin Charlie Wolfe (Simon Pegg) is hired to kill Alice Taylor (Alice Braga) by her husband, wealthy motel proprietor Jack Taylor (Callan Mulvey). When Charlie arrives, he realizes that he isnt the only one planning to kill Alice. Lucy (Teresa Palmer) and her husband Nathan (Sullivan Stapleton) carry out Alices murder after carefully making it look like an accident. Their motives are connected to the fact that Alice is having an affair with handsome mechanic Dylan (Luke Hemsworth), whom she plans to run away with. 

==Cast==
* Simon Pegg as Charlie Wolfe
* Alice Braga as Alice Taylor
* Sullivan Stapleton as Nathan Webb, a dental surgeon with a gambling debt
* Teresa Palmer as Lucy Webb, Nathans awful, manipulative wife and receptionist
* Luke Hemsworth as Dylan Smith
* Callan Mulvey as Jack Taylor   
* Bryan Brown as Bruce Jones, a corrupt cop

==Production==
 , Alice Braga and Luke Hemsworth at the Los Angeles premier of the film in March 2015]]
Filming started in September 2013, in Perth. Shooting took place in various locations in Western Australia, including wine-producing area Margaret River. Post-production was done in Victoria. 

 
The screenplay was written by Irish screenwriter James McFarland, his first screenplay. The story is told from three different points of view.   

==Release==
The film was first released at the Toronto International Film Festival in September 2014, then in the U.K. at the London Film Festival the following month.   It was then released in the U.S. for limited audiences in April 2015.   

==Reception==
Kill Me Three Times received negative reviews from critics. On Rotten Tomatoes, the film has a rating of 9%, based on 43 reviews, with a rating of 3.8/10. The sites critical consensus reads, "Kill Me Three Times offers Simon Pegg an opportunity to play against type as a villain; unfortunately, its derivative storyline fails to offer much of anything to viewers."  On Metacritic, the film has a score of 30 out of 100, based on 20 critics, indicating "generally unfavorable reviews". 

Justin Chang of Variety (magazine)|Variety said of Pegg and the film, "Simon Pegg plays a bemused hitman in Kriv Stenders tiresomely derivative Australian-noir bloodbath."   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 