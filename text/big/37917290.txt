Menace (1934 film)
 
 
 

{{Infobox film
| name           = Menace
| image          =
| image_size     =
| caption        =
| director       = Adrian Brunel 
| producer       = Norman Loudon
| writer         = Victor Varconi   A. R. Rawlinson   Heinrich Fraenkel
| narrator       =
| starring       = Victor Varconi   Joan Maude   D. A. Clarke-Smith   Hubert Leslie
| music          = Colin Wark
| cinematography = George Stretton   Claude Friese-Greene
| editing        =  Sound City
| distributor    = Reunion Films 
| released       = December 1934
| runtime        = 70 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Sound City production company. It was also known by the alternative title When London Sleeps.

==Cast==
* Victor Varconi as Stephen Ronsart 
* Joan Maude as Lady Conway 
* D. A. Clarke-Smith as Sir Robert Conway 
* Hubert Leslie as Mr. Jones 
* Joan Matheson as Mrs. Jones 
* J.A. ORourke as OLeary 
* Shayle Gardner as Commissioner 
* Wilfred Noy as Dean

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B film. British Film Institute, 2007.
* Low, Rachael. History of the British Film: Filmmaking in 1930s Britain. George Allen & Unwin, 1985 .

==External links==
* 

 

 
 
 
 
 
 
 

 
 