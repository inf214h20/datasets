Blue Eyed
 
{{Infobox film
| name           = Blue Eyed
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Bertram Verhaag
| producer       = 
| writer         = Bertram Verhaag
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Blue Eyed is a 1996 documentary film by Bertram Verhaag in which Jane Elliott is teaching a workshop on racism.
She separates people regarding their eye color. The brown or green eye color people are considered to be superior to the blue eye color people. Through this division she creates a whole environment where educated adults, many times in position of power, even though being aware of taking part in a workshop, disagree, argue with each other and cry, not being able to cope or stand the situation in which they are put.
Jane Elliott argues that despite people considering themselves open and caring, they never know how deep is the repression and outcasting which they help to create by doing nothing against it and conforming with the current situation.

The documentary received the 1996 IDFA Audience Award.

==External links==
*   on YouTube
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 