Music Machine (1991 film)
 Samuel Wrights Music Machine albums. The video also spun off a partially animated series called Lessons in Agapeland. It features Stan Freberg and Pat Boone as voices for certain characters.

== Synopsis ==

While flying a kite on a hill, two children named Stevie and Nancy are swept away aloft by their kite in sci-fi fashion to a fantasy, Alices Adventures in Wonderland|wonderland-type world called Agapeland. It is a lush green place near a river, containing over-sized mushrooms, living plants, and friendly woodland animals.

Shortly after their arrival in the pleasant Agapeland, Stevie and Nancy discover the Marvelous Music Machine, which Stevie explains is a "quanamatic digilator." Stevie tries to make it work, but only succeeds in causing it to produce a loud noise, which shoves him back into the Music Machines caretaker (possibly creator), The Conductor.

When one of the children asks how the machine works, The Conductor simply smiles and replies: "You put something in it  , and a song comes out." The next few minutes are spent singing songs, playing around, and learning important things.

But partway into the film, the storyline (which has so far stayed close to the plot of the original 1978 cassette) takes a twist: across the river from Agapeland, it is revealed that there is a barren, dry, creepy, dark place plagued with misery (due to its lack of love). This land is inhabited by the Pudges (three barefoot, waif-like, young characters similar to dwarf (mythology)|dwarves), and their scheming, grouchy "friend", Mr. Pims.

Upon the transition to the miserable land, we see Mr. Pims enlist the three Pudges, Snard, Grum, and Bubort, to help him in a mastermind, wildly wacky scheme of schemes; steal Agapelands source of happiness (the Music Machine) so that their land may be just as lush and green. Pims also figures out a byproduct as well—once the machine is away from Agapeland, people will have to buy their happiness from him and the Pudges.

Immediately, Pims puts his plan into action. He and the Pudges pack up a mule-driven wagon, raft across the river to Agapeland, and spy on Stevie, Nancy, and the conductor, waiting for a moment to steal the machine.
 jackknife to cut the ropes and thwart Pims plan (and it makes the Pudges and Pims escape from Agapeland as soon as they can), but the Music Machine has been wrecked and disformed from all of its mistreatment and no longer works.

The Conductor does everything he can to fix it, but it refuses to play anything. Nancy, upon realizing that the Music Machine is ruined, cries a tear, which falls into the slot. The music machine comes back to life!

== Quotes ==

Nancy: "The music machine must make everybody happy!" 
The Conductor: "Not everybody, Nancy. Some people just seem to enjoy being miserable."

Nancy: "The music machine was so wise. How did it know all those things?" 
The Conductor: "Oh, we all know them, Nancy. We just... dont always listen to what we know."

Mr. Pims: (after explaining his plan to steal the music machine) "Anyone who wants to be happy will have to buy their happiness from us. In other words, well be rich." 
Bubort: "Yeah, and we might even be happy, too!" 
Mr. Pims: "Dont be ridiculous. When youre rich, you dont need to be happy."

== Notes ==
 1977 album.

 

 
 
 
 
 