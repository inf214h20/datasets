Secret People (film)
 
 
{{Infobox film
| name           = Secret People
| image          = Secretpeopleposter.jpg
| image_size     = 
| caption        = Original UK poster
| director       = Thorold Dickinson
| producer       = Sidney Cole Wolfgang Wilhelm
| starring       = Audrey Hepburn Valentina Cortese Serge Reggiani Charles Goldner
| music          = Roberto Gerhard
| cinematography = Gordon Dines
| editing        = Peter Tanner
| studio   = Ealing Studios
| distributor    = General Film Distributors
| released       =  
| runtime        = 96 min.
| country        = United Kingdom
| language       = English
| budget         = 
}} 1952 Cinema British drama film, written and directed by Thorold Dickinson and starring Valentina Cortese, Serge Reggiani and Audrey Hepburn. The film is mainly known for providing Audrey Hepburn with her first significant film role, and for leading to her big breakthrough in Roman Holiday. Because on 18 September 1951, shortly after Secret People was finished and while waiting for its premiere, Thorold Dickinson made a screen test with the young starlet and sent it to director William Wyler, who was in Rome preparing Roman Holiday. He wrote a glowing note of thanks to Dickinson, saying that "as a result of the test, a number of the producers at Paramount have expressed interest in casting her."  

==Plot==
In 1930, Maria Brentano (Valentina Cortese) and her younger sister Nora (Audrey Hepburn) flee to London as their father is about to executed by his countrys dictator. Seven years later, Maria unexpectedly meets Louis (Serge Reggiani), her childhood sweetheart, who is engaged in a plot to assassinate the dictator. Maria is persuaded to play an active part in the plan, but it all goes horribly wrong when the bomb they plant kills an innocent waitress, causing Maria much distress.

==Main cast==
* Valentina Cortese as Maria Brentano
* Serge Reggiani as Louis Balan
* Audrey Hepburn as Nora Brentano
* Charles Goldner as Anselmo
* Michael Shepley as Manager of the British Pavillion

==Reception==
Although finished before August 1951 (the film was screened by the   found it to be "a confused, inarticulate, disappointing film, neither as imagniative nor as intellectually exciting as it should be." 

==External links==
*  
*  
* 
*  

==References==
 

 

 
 
 
 
 
 
 

 