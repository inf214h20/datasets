Blooded (film)
 
 
 
{{Infobox film
| name           = Blooded
| image          = Blooded DVD cover.jpg
| image size     = 200 px
| alt            =
| caption        = DVD cover
| director       = Edward Boase
| producer       = Nick Ashdon James Walker
| starring       = Nick Ashdon Oliver Boot Sharon Duncan Brewster Tracy Ifeachor Joseph Kloska Jay Taylor Cicely Tennant
| music          = Ilan Eshkeri Jeff Toyne
| cinematography = Kate Reid
| editing        = Edward Boase
| studio         = Blooded Ltd. Magma Pictures Ptarmigan ACP
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 independent horror James Walker, Bradford International Film Festival on 18 March 2011.

The plot involves an animal rights action group calling themselves the "Real Animal League", which kidnaps five young deer hunters and then hunts them. It is filmed in a scripted reality style, imitating a real documentary film. As part of the promotion, distributor Revolver Entertainment created a website, realanimalleague.com, for the fictional Real Animal League (RAL), which mentions the film.    An email statement to the real animal rights group, Animal Liberation Front (ALF), supposedly from the RAL claiming that the film misrepresented them, was reprinted on the ALF website. The Evening Standard reported that the film "caused outrage after graphic scenes showing activists attacking five deer-stalkers were posted on the internet, in a viral publicity campaign,"  and the films topic has provoked reactions from parties on both side of the hunting debate in the UK. 

==Synopsis== first person style as the five become the "Game (food)|game" in a hunt, with the intention of using the footage as a warning to other hunters. It is their "documentary" footage that is the basis of the film.        

==Cast==
* Nick Ashdon as Lucas Bell
* Oliver Boot as Charlie Bell
* Sharon Duncan-Brewster
* Tracy Ifeachor as Eve Jourdan
* Joseph Kloska as Ben Fitzpatrick
* Jay Taylor as Activist
* Cicely Tennant as Liv Scott
* Adam Best
* Isabella Calthorpe
* Neil McDermott
* Mark Dexter

==Promotion==
As part of the film promotion, Revolver Entertainment created a website for the fictional Real Animal League (RAL),   and a YouTube channel for the RAL, which both mentioned the film.  According to director Edward Boase, the production company created the appearance of a "fully-fledged animal rights website",  with links to real organisations in order to make the fake organisation appear authentic, but "not to the point where you couldnt find out that it wasnt real."   As reported in First Post, the hunting incident in the film was fictional, there was no kidnapping, and there is no organisation called the "Real Animal League".   Revolver Entertainments marketing department sent an email statement to the real animal rights group, Animal Liberation Front (ALF), supposedly from the Real Animal League, claiming that the film misrepresented the RAL. The email was reprinted in its entirety on the ALF website.    When the event and RAL were revealed as fictional, and after thousands of complaints,  YouTube deemed the videos as inappropriate and removed the footage that had been posted under the guise of the Real Animal League of the films protagonists being chased and forced into confessions, and subsequently deactivated the YouTube channel of the fictional RAL. 

==Release== Bradford International Film Festival on 18 March 2011.   It will have limited release in select theatres and Video on demand|on-demand services on 1 April 2011.      A DVD release is planned for 4 April 2011,    which will include extras such as commentary by film makers, extended interviews, the "making of Blooded", and the short film Home Video by Ed Boase. 

==Critical response==
The films topic has provoked reactions from parties on both side of the hunting debate in the UK.      In promoting the film, its makers originally asserted that it was a re-creation of an actual event that occurred after the enactment of the 2005 hunting ban in England,  maintaining that the film, rather than trying to make any political points, only investigates "the nature of extremism" in any form,    and "encourages debate". 

The Evening Standard wrote that the film "caused outrage after graphic scenes showing activists attacking five deer-stalkers were posted on the internet, in a viral publicity campaign." 

The Independent noted the films controversial stance, and that as the films asserted protagonists were a group of extreme animal activists, it generated "much chatter on the interweb" after clips appeared on Youtube.  They wrote however, "it all has the whiff of a clever publicity stunt".   This is a stance echoed in many other online reviews with suggestions that it is a mockumentary that leaves the viewer with   "no doubt that it is fabricated".  A website called Film Pilgrim. for example, noted that the project first began as an online viral video, but was expanded into a feature film, writing that the expanded footage "has many people debating about the authenticity of the footage."  They praised the films score, writing "The soundtrack for Blooded is one of its most rewarding elements", being "the bulk of the glue that holds the film together."  They offered that the "idea of turning an internet viral video into a feature film/documentary is a pretty interesting concept", but that the film fell flat stylistically in that the "reconstructed footage feels like an actual film which detracts from the realism that the original virals created". They speculated in their review that there was in fact no such organization as the Real Animal League at all, and that the film was created as a hoax in the Blair Witch tradition.   
 The List classed it as an example of a "fantastic piece of filmmaking that shows what is achievable if you get creative within your budget".   

==Soundtrack==
The films soundtrack was created by award-winning and Ivor Novello-nominated composer Ilan Eshkeri and Jeff Toyne.  The score was recorded by the London Metropolitan Orchestra at British Grove Studios and mixed by Steve McLaughlin.   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 