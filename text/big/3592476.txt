Track of the Cat
{{Infobox film
| name           = Track of the Cat
| image          = Track of the Cat (1954) movie poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = William A. Wellman
| producer       = Robert Fellows John Wayne 
| screenplay     = A.I. Bezzerides
| based on       =  
| starring       = Robert Mitchum Teresa Wright
| music          = Roy Webb 
| cinematography = William H. Clothier
| editing        = Fred MacDowell
| studio         = Batjac Productions|Wayne/Fellows Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| gross         = $2 million 
| budget          = 
}}
Track of the Cat is a 1954 film directed by William A. Wellman and starring Robert Mitchum and Teresa Wright. The film is based on a 1949 adventure novel of the same name by Walter Van Tilburg Clark. This was Wellmans second adaptation of a Clark novel, the first being The Ox-Bow Incident.  Track of the Cat was produced by John Wayne and Robert Fellows for their Batjac Productions|Wayne/Fellows production company.

==Plot==
The squabbling Bridges family spends a harsh winter on their remote ranch in northern California in the early years of the 20th century. Crude and quarrelsome middle brother Curt (Robert Mitchum) bullies his noble, unselfish eldest brother Arthur (William Hopper), while youngest brother Harold (Tab Hunter) endures Curt’s abuse in browbeaten silence. Their mother (Beulah Bondi) is a bigoted religious zealot and their father (Philip Tonge) is a loquacious, self-pitying drunk. Bitter old maid sister Grace (Teresa Wright) is temporarily gladdened by the arrival of Harold’s fiancé, spirited Gwen (Diana Lynn).

Their ancient Native American hired hand Joe Sam (Carl Switzer) alerts the family to a panther prowling the hills. Many years before his family was wiped out by a panther. Joe Sam’s superstitious dread of the panther irritates domineering Curt. Curt and Arthur split up to track the panther while the family tensely awaits their return.

Gentle Harold tries to avoid conflict with his parents while Gwen tenderly encourages him to assert his claim to an equal share of the ranch. Although Grace tries to support her youngest brother and his fiancé, Ma Bridges spews hateful suspicion at Gwen, but she ignores the family’s histrionics calmly for Harold’s sake.

By the end of the story, the major conflicts have been resolved, but not without tragedy and loss. The remaining characters seem hopeful that their ordeal may have created the basis for a happier future.

==Cast==
* Robert Mitchum as Curt Bridges
* Teresa Wright as Grace Bridges
* Diana Lynn as Gwen Williams
* Tab Hunter as Harold Bridges
* Beulah Bondi as Ma Bridges
* Philip Tonge as Pa Bridges
* William Hopper as  Arthur Bridges
* Carl Switzer as Joe Sam

==Production== Washington and Mitchum regarded shooting in the deep snow and cold as the worst filming conditions he had ever experienced. 
 William Clothier was designed to highlight black and white and downplay colors. Only key elements like the blue matches, the fire, and Mitchums red coat stand out.

==Reception==

===Critical response===
Film critic Bosley Crowther gave the film a mixed review, writing, "But, for the most part, Mr. Wellmans big-screen picture seems a heavy and clumsy travesty of a deep matriarchal melodrama or a Western with Greek overtones. And the business of the brother hunting the panther in the great big CinemaScope outdoors, while the family booze and blather in the ranch-house, has the nature of an entirely different show ... This, in the last analysis, is the trouble with the film: it has no psychological pattern, no dramatic point. Theres a lot of pretty snow scenery in it and a lot of talk about deep emotional things. But it gets lost in following some sort of pretense." 
 The Ox-Bow The High Poe work and the primitive savageness of Indian folklore. Cinematographer William H. Clothier bleached out the primary colors and that gave the images the look of a black and white film. The haunting luminous look created was very effective in charging the film with the sub-textual sexual energy that lingers from the hot melodramatics and also giving it an alluring aura of mystery." 

==References==
 

==External links==
*  
*  
*  
*  

===DVD Reviews===
*   film review by Glenn Erickson at DVD Savant
*   film review by Erik Rupp at Vista Records
*   film review by Paul Schultz at The-Trades

 
 

 
 
 
 
 
 
 
 
 
 
 