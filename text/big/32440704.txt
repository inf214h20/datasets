Apocalypse Oz
{{Infobox film
| name           = Apocalypse Oz
| image          = 
| image size     =
| alt            = 
| caption        = 
| director       = Ewan Telford
| producer       = Bradley Warden
| writer         = Ewan Telford

Frank L Baum
Joseph Conrad
| narrator       = 
| starring       = 
| music          = 
| cinematography = Kev Robertson
| editing        = Ewan Telford
Bradley Warden
| studio         = CineClash Productions 
| released       = 2006
| runtime        = 25 mins.
| country        = 
| language       = English/ Vietnamese
| budget         = $38,000 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 The Wizard of Oz, and uses only dialogue from these films.

==Plot==
Dorothy Willard, an Amerasian product of the Vietnam War, is tired of living with her abusive foster parents in Kansas. Dorothy decides that "theres no horror like home" and accepts a dream mission that takes her deep into the desert to hunt down and "terminate with extreme prejudice" an insane, renegade US Army colonel - codenamed The Wizard.

==Cast==
* M.C. Gainey as Kurtz, "The Wizard"
* Alexandra Gizela as Dorothy Willard
* Billy Briggs as Hunk, a stoner hitchhiker and version of The Scarecrow
* Brian Poth as Doorman
* Kevin Glikman as Kilgore, a cop and a version of the Wicked Witch
* Tammy Garrett as Giang
* Amy Lyndon as Em
* Don Paul as Henry

==Reception==
The film received positive reviews. Film Threats review said “Apocalypse Oz” had the ability to ruin it from the very beginning but Telford’s direction, convincing performances and an original concept make this worth watching." Quiet Earth described it as a "kinetic punk-rock gem of a film... something entirely new   attempts to explore new ways of telling stories."

==External links==
*  
*  
*  
*   on YouTube

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 