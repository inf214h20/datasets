Stories of Our Lives
{{Infobox film
| name           = Stories of Our Lives
| image          = Stories_of_Our_Lives_Film_Poster.jpg
| caption        = Stories of Our Lives Film poster
| director       = Jim Chuchu
| producer       = Wangechi Ngugi
| writer         = Jim Chuchu Njoki Ngumi
| starring       = Kelly Gichohi Paul Ogola Tim Mutungi Mugambi Nthiga Rose Njenga Janice Mugo Allan Weku Maina Olwenya Louis Brooke Judy Gichohi
| music          = Jim Chuchu
| cinematography = Dan Muchina Jim Chuchu
| editing        = Dan Muchina Jim Chuchu
| studio         = The NEST Collective
| distributor    = 
| released       =  
| runtime        = 62 minutes
| country        = Kenya
| language       = English Swahili
| budget         = $15,000
}}

Stories of Our Lives is a Kenyan film, released in 2014. Created by the members of The Nest Collective, a Nairobi-based arts collective, the film is an anthology of five short films dramatizing true stories of LGBT life in Kenya. 

==Plot==

The five vignettes that make up the film are as follows:

===Ask Me Nicely===

Kate - a rebellious young high school student - encounters Faith, a fellow student in the school corridors. They begin a secret relationship, until the school principal takes action to separate the two by suspending Kate from the school. While away from the school, Kate impulsively has a sexual encounter with a boy in her neighborhood. Upon her return, Kate tells Faith about the encounter with the boy. This annoys Faith, leading to an end of their relationship.

===Run===

After negotiating a business deal with a disc duplicator, Patrick stumbles upon a local gay bar while walking with his best friend, Kama. Kama expresses negative sentiments about the bar as they walk past it. Patrick later returns to the club for a night out, hoping no one will find out. Kama spots Patrick leaving the bar, and they have a violent confrontation about it. Patrick runs away to escape the fight.

===Athman===

Farm workers Ray and Athman have been close friends for years. Hurt by Athman’s flirtatious relationship with newcomer Fiona, Ray has an awkward conversation with Athman about their relationship. Athman reiterates that he isnt interested in a sexual relationship with Ray. They reconcile, then Ray asks Athman whether he can kiss him. Athman is taken aback by the question and leaves, uncomfortable. The two reconcile again the next day, but Ray decides to leave the farm.

===Duet===

Jeff - a researcher visiting the UK for a conference - hires escort Roman for an hour-long session in his hotel room. Roman arrives, and - sensing Jeffs anxiety - attempts to calm him down. Jeff asks if they can talk a little before engaging in any physical activity. The two sit and have a conversation about inter-race relations. Roman then offers to give Jeff a massage, which then leads to Jeff being less anxious. The two proceed to make out.

===Each Night I Dream===

Liz visualizes dramatic escape plans for herself and partner Achi when local legislators threaten to enforce anti-gay laws.

==Production==
Stories of Our Lives began as a documentation project by the Nest Collective. The collective travelled around Kenya, recording audio interviews with persons identifying as LGBTIQ. These recordings formed the basis for the film vignettes.  . Variety (magazine)|Variety, September 5, 2014.  The films $15,000 came from Uhai/EASHRI - an East African sexual rights fund based in Nairobi, Kenya - and was shot by the collective over a period of eight months using a single Canon DSLR video camera.  

==Release==
Due to the legal status of homosexuality in Kenya putting the members of the collective at risk of arrest, the individual members of the collective remained anonymous in the films credits.  . Variety (magazine)|Variety, September 5, 2014.  When the film premiered at the 2014 Toronto International Film Festival in September 2014, three of the collectives members — Jim Chuchu, George Gachara and Njoki Ngumi — opted to reveal their names in an interview with Torontos LGBT newspaper Xtra!. 

===Controversy===

The film was rejected for distribution and screening in Kenya by the Kenya Film Classification Board, on the grounds that the film "promotes homosexuality, which is contrary to national norms and values" of Kenya.  Executive producer Gachara was subsequently arrested on charges of violating the countrys Films and Stage Plays Act. 

===Critical response===
Stories of Our Lives has received positive reviews. Reviews from the Huffington Post described the film as an "intimate, masterly portrayal of Kenyas LGBT community" and "one of the most triumphant and stunning films of the year".  Reviews from IndieWire also described the film as "a beautiful little film about love, about humanity, about one of the many facets of what it means to be African". 

The film won a Jury Prize from the Teddy Award jury at the 65th Berlin International Film Festival and came second in the Panorama Audience Award.   

==Music==
The films original score and songs were composed and produced by the films director, Jim Chuchu. The collective released the soundtrack as a free download on Bandcamp on September 24, 2014. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 