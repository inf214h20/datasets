Sensation Hunters (1933 film)
{{Infobox film
| name           = Sensation Hunters
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Vidor
| producer       = Trem Carr (producer) Robert E. Welsh (supervising producer) Paul Schofield (adaptation)
| starring       = 
| music          = 
| cinematography = Sidney Hickox
| editing        = Carl Pierson
| distributor    = Monogram Pictures
| released       = 30 August 1933 (premiere)
| runtime        = 73 minutees
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Sensation Hunters is a 1933 American B-movie directed by Charles Vidor and released by Monogram Pictures.

== Cast ==
* Arline Judge as Jerry Royal
* Preston Foster as Tom Baylor
* Marion Burns as Dale Jordan
* Kenneth MacKenna as Jimmy Crosby
* Juanita Hansen as Trixie Snell
* Creighton Hale as Fred Barrett
* Cyril Chadwick as Upson
* Nella Walker as Mrs. Grayson
* Harold Minjir as Hal Grayson
* Finis Barton as Miss Grayson
* Zoila Conan as Alcoholic Girl
* Sam Flint as Ships Captain
* Walter Brennan as Stuttering Waiter

== Soundtrack ==
*Arline Judge and chorus - "If It Aint One Man" (Written by Bernie Grossman and Harold Lewis)
*Marion Burns - "Theres Something In the Air" (Written by Bernie Grossman and Harold Lewis)

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 