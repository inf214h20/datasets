Torch Singer
{{Infobox film
| name           = Torch Singer
| image          = Torchsinger.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Alexander Hall
* George Somnes
}} Albert Lewis
| screenplay     = {{Plainlist|
* Lenore J. Coffee
* Lynn Starling
}}
| story          = Grace Perkins
| starring       = {{Plainlist|
* Claudette Colbert
* Ricardo Cortez
* David Manners
* Lyda Roberti
}}
| music          = Ralph Rainger
| cinematography = Karl Struss
| editing        = Eda Warren
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Torch Singer is a 1933 film made by Paramount Pictures, directed by Alexander Hall and George Somnes, and starring Claudette Colbert, Ricardo Cortez and David Manners and Lyda Roberti.
 Liberty magazine  (May 20–27, 1933).

It was released on DVD (as part of a six disc set entitled "Pre-Code Hollywood Collection") on April 7, 2009. 

==Cast==
*Claudette Colbert as  Sally Trent, aka Mimi Benton
*Ricardo Cortez as Tony Cummings
*David Manners as Michael Mike Gardner
*Lyda Roberti as Dora Nichols
*Baby LeRoy as Bobby, Doras Baby at 1 Year
*Charley Grapewin as Andrew Juddy Judson
*Sam Godfrey as Harry, Radio Announcer
*Florence Roberts as Mother Angelica
*Virginia Hammond as Mrs. Julia Judson
*Mildred Washington as  Carrie, Mimis Maid
*Cora Sue Collins as Sally at 5 Years
*Helen Jerome Eddy as Miss Spaulding
*Albert Conti as Carlotti
*Ethel Griffies as Agatha Alden

==Quotes==
*Mimi Benton: "Well, Ill tell you what happened to her. While you were touring China, she went through hell. Its a nice place, you must go there someday."

*Michael Gardner: "Youve changed all right! Youre selfish, hard. Mimi Benton: Sure I am, just like glass. So hard, nothingll cut it but diamonds. Come around some day with a fistful. Maybe we can get together."

*Mimi Benton: "Dont ever let any man make a sucker out of you. Make him know what youre worth. Anything they get for nothing is always cheap."

==See also==
* Torch song

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 