Insan Jaag Utha
 
 
{{Infobox film
| name =Insan Jaag Utha
| image = 
| director =Shakti Samanta
| producer =Shakti Samanta 
| writer = Nabendu Ghosh  (screenplay)  Vrajendra Gaur  (dialogue)  story = Saroj Mitra Nasir Hussain  Bipin Gupta Madan Puri Shailendra (lyrics)
| cinematography=Chandu
| editing =Dharamvir
| distributor = 
| released =1959 Hindi
| country= India
| budget = 
| gross= 
| awards = 
}}
 1959 Hindi Nasir Hussain, Bipin Gupta and Madan Puri in lead roles. It has music by Sachin Dev Burman, with lyrics by Shailendra (lyricist)|Shailendra.
 Howrah Bridge (1958). Insaan Jaag Utha was his attempt to shift genre to social themes. However after the film didnt do well at box office, he shifted back to making entertainment-oriented films for another decade, before attempting the genre with films like Aradhana (1969 film)|Aradhana (1969), Kati Patang (1970) and Amar Prem (1971). 

==Cast==
*Sunil Dutt as Ranjeet
*Madhubala as Gauri Nasir Hussain as Laxmandas
* Bipin Gupta
*Madan Puri as Mohan Singh
* Minoo Mumtaz as Muniya

== Soundtrack ==
{{Infobox album  
| Name        = Insaan Jaag Utha
| Type        = Soundtrack
| Artist      = Sachin Dev Burman
| Cover       = 
| Released    = 1959 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = HMV now Sa Re Ga Ma|
| Producer    = Sachin Dev Burman
| Reviews     = 
| Last album  = Kaagaz Ke Phool  (1959)
| This album  = Insaan Jaag Utha  (1959)
| Next album  = Manzil (1960 film)|Manzil   (1960) 
|}}

The soundtrack of the film was composed by S. D. Burman, with lyrics by Shailendra (lyricist)|Shailendra. It is noted for the duet Jaanu Jaanu Ri sung by Asha Bhonsle and Geeta Dutt, which was shot on location at the under-construction Nagarjuna Sagar Dam.   
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#eeeeee" align="center"
! Song !! Singer (s)
|-
| Chand Sa Mukhda
| Asha Bhonsle, Mohd. Rafi
|-
| Jaanu Jaanu Ri
| Asha Bhonsle, Geeta Dutt 
|-
| O Mehnatkash Insaan Jaag Utha
| Asha Bhonsle, Mohd. Rafi, Chorus
|-
| Dekho re dekho log ajooba
| Mohd. Rafi, Chorus
|-
| Baat Badhti Gayi Khel Khel Mein
| Asha Bhonsle
|-
| Dekho Kya Ishare Hain Baharon Se Nazaron
| Asha Bhonsle
|-
| Aankhen Char Hote Hote O Gaya Pyar
| Asha Bhonsle 
|}

== References ==
 

==External links==
*  
*  , Rajshri Films Official channel.
*  

 
 
 
 
 
 