Woman of Malacca
{{Infobox film
| name           = Woman of Malacca 
| image          = 
| image_size     = 
| caption        = 
| director       = Marc Allégret
| producer       = 
| writer         = Francis de Croisset (novel)   Claude-André Puget   Jan Lustig
| narrator       = 
| starring       = Edwige Feuillère   Pierre Richard-Willm   Betty Daussmond   Jacques Copeau
| music          =  Louis Beydts
| editing        = Yvonne Martin   
| cinematography = Jules Kruger 
| studio         = Régina Film 
| distributor    = Tobis Film
| released       = 1 October 1937
| runtime        = 113 minutes
| country        = France
| language       = French
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} French drama film directed by Marc Allégret and starring Edwige Feuillère, Pierre Richard-Willm and Betty Daussmond. It was based on a 1935 novel by the Belgian writer Francis de Croisset. It was a major success on its initial release. 

A separate German-language version Andere Welt was also made.

==Synopsis==
A young Englishwoman, Audrey Greenwood, marries an army officer to escape her dreary life as a school teacher. Accompanying her husband out for colonial service in Malacca, she soon grows unhappy with her marriage, and falls in love with a local sultan, Prince Selim.

==Cast==
* Edwige Feuillère as Audrey Greenwood 
* Pierre Richard-Willm as Prince Selim 
* Betty Daussmond as Lady Lyndstone 
* Jacques Copeau as Lord Brandmore 
* Gabrielle Dorziat as Lady Brandmore 
* Jean Debucourt as Sir Eric Temple 
* Jean Wall as Le major Carter 
* Liliane Lesaffre as Lady Johnson 
* Ky Duyen as Le japonais 
* Foun-Sen as La servante 
* William Aguet as Gerald 
* Alexandre Mihalesco as Sirdae Raman 
* René Bergeron as Le Docteur 
* Magdeleine Bérubet as Mademoiselle Tramont 
* Charlotte Clasis as Une amie dAudrey 
* Marthe Mellot as La sous-maîtresse de linstitut Tramont 
* Robert Ozanne as Un journaliste 
* René Fleur as Un journaliste 
* Michèle Lahaye as Une dame anglaise 
* Colette Proust as Une dame anglaise 

==References==
 

==Bibliography==
* Kennedy-Karpat, Colleen. Rogues, Romance, and Exoticism in French Cinema of the 1930s. Fairleigh Dickinson, 2013.
* Passerini, Luisa, Labanyi, Jo & Diehl, Karen. Europe and Love in Cinema. Intellect Books, 2012.

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 

 