The Secret of the Purple Reef
{{Infobox film 
| name           = The Secret of the Purple Reef
| image          = 
| image size     = 
| alt            = 
| caption        = http://img.movieberry.com/static/photos/68147/poster.jpg
| director       = William Witney
| producer       = Gene Corman
| writer         = Harold "Yabo" Yablonsky
| based on       =  
| narrator       =   Jeff Richards
| music          = Buddy Bregman
| cinematography = Kay Norton
| editing        = Peter C. Johnson
| studio         = Associated Producers Incorporated
| distributor    = 20th Century Fox
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $86,000
| gross          =  
}} 

The Secret of The Purple Reef is a 1960 - 20th Century Fox CinemaScope DeLuxe Color film based on a novel by Dorothy Cottrell entitled The Silent Reefs. It starred soon-to-be-famous actors, Richard Chamberlain and Peter Falk. A caribbean based mystery involving the disappearance of a ship called the "Cloud".

==Plot== Jeff Richards) show up in a mid 50s four door Citroen Traction Avant in early era San Juan, Puerto Rico. They arrive to solve the mystery, the circumstances of their fathers drowning death in the Caribbean. The action begins to perk up as the two brothers run up against a gang of unruly pirates who seem to know more than they reveal. One of the films highlights are several scenes of an E.G. Van de Stadt designed 35 foot sailing yacht, the "Starwright".  The vessel adds to the Caribbean charm and plays an integral part of moving the pirates to other parts of the island.

==Cast==                        Jeff Richards as Mark Christopher
* Margia Dean as Rue Amboy
* Peter Falk as Tom Weber
* Richard Chamberlain as Dean Christopher
* Terence De Marney as Ashby
* Robert Earl Jones as Tobias
* Gina Petrushka as Grandmere
* Larry Markow as Kilt
* Philip Rosen as Twine
* Frank Ricco as Henri
* Jerry Mitchell as Jerry

==Legacy==
Although considered a "B" film, it is notable in that The Secret of The Purple Reef introduces Richard Chamberlain and Peter Falk to moviegoers and appears as part of their early film career.

==References==
* Cottrell, Dorothy.(1952)  The Silent Reefs. Curtiss Publishing Company. New York.
* Halliwell, Leslie.  (1977) The Filmgoers Companion 6th Ed.  Avon Books. New York. pp.&nbsp;135, 249
* Katz,Ephraim.  The Film Encyclopedia (1979) HarperCollins Publishing. New York.  pp.&nbsp;222,400
* The Films of 20th Century Fox 1st Edition. (1979)  Citadel Press. New Jersey.  pp.&nbsp;322

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 