Chakravarthy (1995 film)
{{Infobox film
| name           = Chakravarthy
| image          = 
| caption        = 
| director       = M. Baskar
| producer       = M. Baskar
| writer         = M. Baskar
| starring       =   Deva
| cinematography = Viswam Natraj
| editing        = Krishnamoorthy - Siva
| studio         = Oscar Movies
| distributor    = 
| released       =  
| country        = India
| runtime        = 130 min
| language       = Tamil
}}
 1995 Tamil Tamil crime Karthik and Deva and was released on 28 July 1995. 

==Plot==
 CID officer, lives in Bhanus rental house. Bhanu (Bhanupriya) hates the police department so Chakravarthy lies about his job. His superior (Major Sundarrajan) reveals that Bhanu killed her half-brother Boopathy (Nizhalgal Ravi). Chakravarthy begins to investigate in this odd affair.

==Cast==
 Karthik as Chakravarthy
*Bhanupriya as Bhanu
*Goundamani as Chinnappa
*V. K. Ramaswamy (actor)|V. K. Ramaswamy as Ithiraj
*Nizhalgal Ravi as Boopathy
*Major Sundarrajan
*LIC Narasimhan as Pradeep
*Kumarimuthu
*Loose Mohan

==Soundtrack==

{{Infobox album |  
  Name        = Chakravarthy |
  Type        = soundtrack | Deva |
  Cover       = |
  Released    = 1995 |
  Recorded    = 1995 | Feature film soundtrack |
  Length      = 19:12 |
  Label       = | Deva |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1995, 4 features tracks with lyrics written by Pulamaipithan and Muthulingam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Hello Hello Dear || S. P. Balasubrahmanyam || 5:07
|- 2 || Edathakaal || S. P. Balasubrahmanyam || 4:49
|- 3 || Vaazhe Thoppu || Mano (singer)|Mano, S. Janaki || 4:51
|- 4 || Enga Enga Pottivacha || Mano (singer)|Mano, S. Janaki || 4:15
|}

==References==
 

 
 
 
 
 