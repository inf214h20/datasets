Crime School
{{Infobox film
| name = Crime School
| image = crimeschool.jpg
| caption = Theatrical release poster
| director = Lewis Seiler
| producer = Bryan Foy
| writer = Crane Wilbur Vincent Sherman
| starring = Humphrey Bogart Gale Page Billy Halop Bobby Jordan Huntz Hall Leo Gorcey Bernard Punsly Gabriel Dell Charles Trowbridge
| music = Max Steiner
| cinematography = Arthur Todd
| editing = Terry Morse
| distributor = Warner Bros.
| released =  
| runtime = 86 minutes
| country = United States
| language = English
| budget =
}}
Crime School is a 1938 Warner Bros. film directed by Lewis Seiler and starring the Dead End Kids and Humphrey Bogart.

==Plot==
A junkman (Frank Otto) does business with the  ), Squirt (Bobby Jordan), Spike (Leo Gorcey), Goofy (Huntz Hall), Fats (Bernard Punsly), and Bugs (Gabriel Dell). When the boys ask for a $20 payoff, "Junkie" says "Five is all youll get. Now take it and get out of here." In a rage, Spike strikes the man in the back of the head with a hard object, and the junkman falls to the floor and doesnt move.  When Judge Clinton (Charles Trowbridge) cannot convince the boys to divulge which one struck the damaging blow, they are all sent to reform school.

The superintendent of the state reformatories, Mark Braden (Humphrey Bogart), visits the school and finds that it is being mismanaged.  As a way of starting over, he fires Morgan (Cy Kendall), the cruel warden, and four ex-convict guards, while retaining the head guard, Cooper (Weldon Heyburn).   Braden runs the school himself and attempts to parole the kids, while romancing Frankies sister, Sue Warren (Gale Page).

Meanwhile, Cooper is afraid that Braden will learn of Morgans illegal use of the food budget, which would implicate him as well.  He learns that Spike is the one who dealt the blow to the junkman and blackmails him.  He gets him to tell Frankie that Braden is forcing Sue to pay for the special treatment the boys have received.  Although untrue, it causes the kids to escape from the school.

On the outside, they confront Braden and learn the truth.  Cooper "discovers" that the kids have escaped and Morgan calls the press to discredit Braden and get him fired.  Once the kids are back at the school and the police arrive, Braden delivers evidence about Morgans fraud, and Morgan is arrested.

==Background== Dead End, they advertised the kids as The Crime School Kids in this film, and their next, Angels with Dirty Faces.  However, the name did not catch on and they remained The Dead End Kids. 
* Before the film was released, Halop, Dell, Hall, and Punsly were released from their contracts by Warner Brothers and they went on to make a film at Universal Studios|Universal, Little Tough Guy.  The success of this film caused Warner to reconsider and they were rehired at a substantial raise. 
*The Dead End Kids received top billing over Humphrey Bogart for Crime School, with their typeface also larger than Bogarts in posters and advertising.

==Cast==
===The Dead End Kids===
* Billy Halop as Frankie Warren
* Bobby Jordan as Lester "Squirt" Smith
* Huntz Hall as Richard "Goofy" Slade
* Leo Gorcey as Charles "Spike" Hawkins
* Bernard Punsly as George "Fats" Papadopolos
* Gabriel Dell as Timothy "Bugs" Burke

===Additional cast===
* Humphrey Bogart as Mark Braden
* Gale Page as Sue Warren
* George Offerman, Jr. as Red
* Weldon Heyburn as Cooper
* Cy Kendall as Morgan
* Charles Throwbridge as Judge Clinton
* Spencer Charters as Intoxicated doctor
* Donald Briggs as New doctor
* Frank Jaquet as Commissioner 
* Helen MacKellar as Mrs. Burke
* Al Bridge as Mr. Burke
* Sibyl Harris as Mrs. Hawkins
* Paul Porcasi as Nick Papadopolos
* Frank Otto as Junkie Ed Gargan as Officer Hogan
* James B. Carson as Schwartz
* Hally Chester as Boy

==Home media==
Warner Archives released the film on made to order DVD in the United States on August 4, 2009.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 