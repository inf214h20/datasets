The Brontë Sisters
 
{{Infobox film
| name           = The Brontë Sisters
| image          = The Bronte Sisters poster.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| producer       = Yves Gasser   Klaus Hellwig   Yves Peyrot Alain Sarde (executive)
| writer         = Pascal Bonitzer  André Téchiné   Jean Gruault
| starring       = Isabelle Adjani Marie-France Pisier Isabelle Huppert Pascal Greggory
| music          =  , etc...
| cinematography = Bruno Nuytten
| editing        = Claudine Merlin
| studio         = Action Films   Gaumont  FR3 Gaumont
| released       =  
| runtime        = 115 minutes
| country        = France
| language       = French
| budget         =
}}
The Brontë Sisters ( ) is a 1979 French drama film directed by André Téchiné. It tells the story of the famous Brontë siblings.
 Souvenirs den France (1975) and Barocco (1976), he was able to find the necessary financing. Produced by Gaumont Film Company|Gaumont, the films originally running time was cut from three to less than two hours upon its release at the 1979 Cannes Film Festival.   
 Charlotte and Isabelle Huppert as Anne Brontë|Anne. Pascal Greggory plays their brother Branwell Brontë. The plot centers on the sisters sombre relationship with Branwell.

Set in a careful recreation of the period, the film follows the bleak lives of the four siblings in less than a ten year span. It begins in 1834, when at the age of seventeen Branwell painted the famous portrait of his three sisters, in which he originally included his own image, and ends around 1852 when Charlotte, now a famous author, is the only surviving sibling.

==Plot==
Four young siblings: Charlotte, Branwell, Emily and Anne, live a stoic existence in a small village in the English country side. Their old father, an Anglican minister, a rigid spinster aunt and Tabby, the maid, complete their household. The siblings have artistic ambitions and rely upon each other for companionship. Branwell is a painter and a self-portrait with his sisters is worthy of the general admiration of the family. He wants to pursue a professional career, but only goes as far as to establish a friendship with Leyland, a sculptor. Emilys favorite pastime is to walk across the bleak moors that surround the village dressed as a man. Anne, the youngest of the siblings, is her companion. Charlotte, more ambitious than the others, convinces their reluctant aunt to give her money to go to Belgium in order to study French. Her idea is to eventually come back and open a school. With their aunts money and permission, Charlotte and Emily go to Brussels. Once there, Charlotte falls secretly in love with her teacher Monsieur Heger, who is already married. Emily plays the piano at school, but has a hard time there and is teased by her classmates for being English and Protestant in a Catholic country. Meanwhile, in England, Anne finds employment as a governess, taking over the education of the daughter of a wealthy family.

While his sisters are away, Branwell deals alone with the death of their aunt. Her death makes Emily and Charlotte come back home. Emily is relieved and helps Branwell to find solace, taking him to the Black Bull Inn, the tavern and hotel of the town. Charlotte, on the other hand, lovesick, returns as soon as possible to Brussels to be reunited with Monsieur Heger, but her love is unrequited. Thanks to Anne, the aimless dreamer Branwell finds a steady job as the teacher of Edmund, the young son of the Robinson family, Annes wealthy employers. Mr Robinson is strict, and, with his air of superiority, humiliates both Anne and Branwell. Mrs Robinson, flirty and unsatisfied, starts an ill-fated affair with Branwell. When Anne finds out about their relationship, she quits her job and returns home. Both Branwell and Charlotte have to deal with their broken hearts. After the death of her husband, Mrs Robinson sends Branwell a letter ending their affair.

Branwells life takes a dark turn. He gives himself over to drinking and becomes addicted to opium. During a windy night, a fire starts in his bedroom and he has to be rescued from amongst the flames by his sisters. Sneaking into Emilys bedroom and searching amongst her things, Charlotte discovers Emilys poems. Deeply impressed, she finally is able to convince the reluctant Emily to have them published. Soon the three sisters have their poems, and later a novel each, published. Reviews of Emilys novel, Wuthering Heights, are particularly harsh. However, the novels of Currer, Ellis and Acton Bell, the pen names adopted by the three sisters, are the talk of London literary circles. Speculations about the sex and identity of the Bells force Charlotte and Anne to go to London to introduce themselves to George Smith, Charlottes publisher.

Unaware of his sisters literary accomplishments, Branwell dies of marasmus exacerbated by heavy drinking. Emily, stricken by tuberculosis, refuses all medical treatment, insisting on carrying on with her household chores. When she finally agrees to send for a doctor, it is too late, and she dies. Anne is also terminally ill with tuberculosis. Following her wishes, Charlotte takes her to see the ocean for the first time, and Anne dies during that trip.

Charlotte is the only survivor among the four siblings. Left alone with her elderly father, she pursues her literary career and begins a romantic relationship with Arthur Nicholls, her fathers curate. In the company of Mr. Nicholls and her publisher, Mr Smith, Charlotte goes to the opera in London and meets the famous author William Thackeray.

==Cast==
 
 
* Isabelle Adjani as Emily Brontë
* Marie-France Pisier as Charlotte Brontë
* Isabelle Huppert as Anne Brontë
* Pascal Greggory as Branwell Brontë Patrick Magee Reverend Brontë
* Hélène Surgère as Mrs. Robinson
* Roland Bertin as Arthur Bell Nicholls|Mr. Nicholls
  Aunt Elizabeth Monsieur Héger
* Adrian Brine as Mr. Robinson
* Julian Curry as George Murray Smith|Mr. Smith
* Rennee Goddard as Tabby, the maid Leyland
* Roland Barthes as William Makepeace Thackeray
 

==Background == silent era Wuthering Heights Jane Eyre, Robert Stevenson in 1943.

The very lives of the three Brontë sisters did not attract Hollywoods same level of interest, even though a great number of biographies and semi-fictional books based on the lives has been published. Arthur Kennedy as Branwell, Devotion made no effort in authenticity. It portrays Emily in love with revered Nichols who is in love with Charlotte. The film, made as potboiler romance, had  no resemblance to the actual lives of the Brontë sisters and was decried by Brontë enthusiast for its blatant inaccuracies.

The Brontës of Haworth, a four-part drama made for Yorkshire television, was broadcast in 1973 with a script written by Christopher Fry; directed and produced by Marc Miller. Emily was played by Rosemary McHale, Charlotte by Vickery Turner, Anne by Ann Penfold and Branwell by Michael Kitchen. Shot on authentic locations, The Brontës of Haworth was very well received.

Téchiné biopic on the Brontë, conceived in the early 1970s, was only the third project on the famous authors lives and the first accurate portray of them make on film.

==Casting== Souvenirs d’en Camille Claudel, an artistic success for both.
 La Reine Henri III. Huppert, Adjani and Greggory where only in their mid-twenties when the film was made.

Isabell Huppert had risen to prominence  in  s Aloïse, a film written by Téchiné. Huppert and Adjani famously did not get along and make the production of The Brontë Sisters difficult.
 Patrick Magee, A Clockwork English and French

==Music==
The music for the film was  arranged by Philippe Sarde, a brother of Alain Sarde, who had produced Téchinés two previous films and was executive producer of The Brontë Sisters. Philippe Sarde had written original music for Souvenirs d’en France and Barocco, but in The Brontë Sisters he  arranged adaptions of classical pieces on place of an original score. Gioachino Rossini overture to Tancredi and music from Robert Schumann, where among the compositions used following the mold of Stanley Kubrick reused of classical music in his films.

==Reception==
The film was ill received as its premiered at the Cannes film festival in May 1979, where Francis Ford Coppolas Apocalypse Now eclipsed other films in competition. However, The Brontë Sisterss reputation has grown since then. Today is seen as an accurate representation of the lonely and bleak lives of the Brontë siblings.

==Home media==
The Brontë Sisters was remastered and re-released as part of the Cohen Film Collection by the Cohen Media Group. It came out on DVD and Blu-ray on July 30, 2013.

Both editions include an audio commentary with film critic Wade Major and Brontë scholar Sue Lonoff de Cuevas. They also include Dominique Maillets 2012  The Ghosts of Haworth, an hour-long documentary on  the conception, making, and reception of the film through interviews with Téchiné, co-writer Pascal Bonitzer, Brontë-scholar Claire Bazin, costumier Christian Gasc, and actor Pascal Gregory. Two theatrical trailers, one for the original 1979 French release and one from the recent theatrical re-release complete the extra features.  
 
Previously to 2013, the film had been released on DVD only in region 2. It was released in Spain in French with Spanish subtitles or dubbed in Spanish as the options offered, but it is currently out of print.    The film was released on DVD in Sweden in 2009 as part of a box set of Brontë-related films.
 Gaumont released a Region B Blu-ray in France on 9 May 2012. 

==See also==
* Isabelle Huppert filmography

==Bibliography==
*Marshall, Bill, André Téchiné, Manchester University Press, 2007, ISBN 0-7190-5831-7

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 