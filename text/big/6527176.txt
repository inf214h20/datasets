Paprika (2006 film)
{{Infobox film
| name           = Paprika
| image          = Paprikaposter.jpg
| caption        = Theatrical release poster
| film name      = {{Infobox name module
| kanji          = パプリカ
| romaji         = Papurika
}}
| director       = Satoshi Kon
| screenplay     = {{plainlist|
* Seishi Minakami
* Satoshi Kon
}}
| based on       =  
| starring       = {{plainlist|
* Megumi Hayashibara
* Tōru Emori
* Katsunosuke Hori
* Tōru Furuya
}}
| music          = Susumu Hirasawa
| cinematography = Michiya Katou
| editing        = Takeshi Seyama Madhouse
| distributor    = Sony Pictures Entertainment Japan
| released       =    
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $944,915   
}} animated film novel of the same name, about a research psychologist who uses a device that permits therapists to help patients by entering their dreams. It is Kons fourth and final feature film before his death in 2010.
 Madhouse Inc. animated and produced the film alongside Sony Pictures Entertainment Japan, which distributed it in Japan. The score was composed by Susumu Hirasawa.

The soundtrack is significant for being the first film to use a  ."      

==Plot==
In the near future, a revolutionary new psychotherapy treatment called dream therapy has been invented. A device called the "DC Mini" allows the user to view peoples dreams. The head of the team working on this treatment, Doctor Atsuko Chiba, begins using the machine illegally to help psychiatric patients outside the research facility, using her alter-ego "Paprika", a sentient persona that she assumes in the dream world.

Paprika counsels Detective Toshimi Konakawa, who is plagued by a recurring dream. Its incompleteness is a great source of anxiety for him. At the end of the session, she gives Konakawa a card with a name of a website on it. This type of counselling session is not officially sanctioned, so Chiba, her associates and Konakawa must be cautious that word does not leak out regarding the nature of the DC Mini and the existence of Paprika. Chibas closest ally is Doctor Kōsaku Tokita, a genius man-child and the inventor of the DC Mini. Because they are unfinished, the DC Minis lack access restrictions, allowing anyone to enter another persons dreams, which poses grave consequences when they are stolen. Almost immediately, the chief of the department, Doctor Toratarō Shima, goes on a nonsensical tirade and jumps through a window, nearly killing himself.

Upon examining Shimas dream, consisting of a lively parade of objects, Tokita recognizes his assistant, Kei Himuro, which confirms their suspicion that the theft was an inside job. After two other scientists fall victim to the DC Mini, the chairman of the company, who was against the project to begin with, bans the use of the device completely. This fails to hinder the crazed parade, which manages to claim Tokita, who went inside Himuros dream trying to find answers and intruded into Konakawas dream. Paprika and Shima take matters into their own hands and find that Himuro is only an empty shell. The real culprit is the chairman, with the help of Doctor Morio Osanai, who believes that he must protect dreams from mankinds influence through dream therapy. Paprika is eventually captured by the pair after an exhausting chase. There, Osanai admits his love for Chiba and literally peels away Paprikas skin to reveal Chiba underneath. However, he is interrupted by the outraged Chairman who demands that they finish off Chiba; as the two share Osanais body, they battle for control as they argue over Chibas fate. Konakawa enters the dream from his own recurring dream, and flees with Chiba back into his. Osanai gives chase through Konakawas recurring dream, which ends in Konakawa shooting Osanai to take control of the dream. The act actually kills Osanais physical body with a real bullet wound.

Dreams and reality have now merged. The dream parade is running amok in the city, and reality itself is starting to unravel. Shima is nearly killed by a giant Japanese doll, but is saved by Paprika, who has become an entity separate from Chiba thanks to dreams and reality merging. Amidst the chaos, Tokita, in the form of a giant robot, eats Chiba and prepares to do the same for Paprika. The chairman returns in the form of a living nightmare, reveals his twisted dreams of omnipotence, and threatens to darken the world with his delusions. A ghostly apparition of Chiba appears and reveals that she has been in love with Tokita this whole time and has simply been repressing these emotions. She comes to terms with her own repressed desires, reconciling herself with the part of her that is Paprika. Paprika returns to Tokita, throwing herself into his body. A baby emerges from the robotic shell and sucks in the wind, aging as she sucks up the chairman himself, becoming a fully-grown combination of Chiba and Paprika. In this new form, she is able to consume the chairmans dream form and end the nightmare he created before fading away. 

In the final scene, Chiba sits at Tokitas bedside as he wakes up. Later on, Konakawa visits the website from Paprikas card and receives a message from Paprika: "Atsuko will change her surname to Tokita...and I suggest watching the movie Dreaming Kids." Konakawa enters a movie theater and purchases a ticket for Dreaming Kids.

==Production==

===Cast=== modest psychiatrist and researcher at the Institute for Psychiatric Research. She uses the DC Mini to treat her clients inside their dreams under the guise of her alter ego  . Chiba is voiced by Cindy Robinson in the English dub. obese child-at-heart genius and the inventor of the DC Mini. He is Chibas closest ally, although she often treats him coldly. Tokita often calls Chiba "At-chan" as a symbol of affection. Tokita is voiced by Yuri Lowenthal in the English dub.
* Tōru Emori as  , the wheelchair-bound chairman of the Institute for Psychiatric Research who calls himself the "protector of the dreamworld" but is in fact using the DC Mini for his own nefarious purposes, seemingly interested in using it to regain mobility as well as power. Inui is voiced by Michael Forest in the English dub. David Lodge in the English dub.
* Akio Ōtsuka as  , a friend of Shima and a client of Paprika. He is haunted by a recurring dream that stems from an anxiety neurosis. He is infatuated with Paprika. Konakawa is voiced by Paul St. Peter in the English dub.
* Hideyuki Tanaka as Guy
* Kōichi Yamadera as  , a researcher and colleague of Atsuko Chiba who in reality is helping the Chairman in his evil plans. He is obsessively in love with Chiba. Osanai is voiced by Doug Erholtz in the English dub.
* Satomi Kōrogi as a Japanese doll that reappears throughout the story
* Daisuke Sakaguchi as Kei Himuro, a friend of Tokita and a suspect in the theft of the DC Mini
* Mitsuo Iwata as Doctor Yasushi Tsumura and
* Rikako Aikawa as Doctor Nobue Kakimoto, two scientists who fall victim to the DC Mini thief
* Yasutaka Tsutsui the author of the novel the film is based on as Kuga, and
* Satoshi Kon, the director as Jinnai, two bartenders who befriend Konakawa.

===Music===
{{Infobox album |  
| Name        = Paprika Original Soundtrack
| Type        = Soundtrack
| Artist      = Susumu Hirasawa
| Cover       = Paprika_OST.jpg
| Released    = November 23, 2006
| Recorded    = 
| Genre       = 
| Length      =  
| Label       = TESLAKITE (Japan) Milan Entertainment (United States|USA)
| Producer    = Chaos Union
| Reviews     = 
| 
| Chronology  = Susumu Hirasawa soundtrack
| Last album  = Paranoia Agent Original Soundtrack (2004)
| This album  = Paprika Original Soundtrack (2006) 
}}
 soundtrack was released on November 23, 2006 under the TESLAKITE label. It was composed by Susumu Hirasawa. A bonus movie was included on the CD, using the closing theme to Paprika. The last track, "The Girl in Byakkoya", features three spoken lines in Vietnamese language|Vietnamese. The soundtrack is significant for being the first made for a film to use a Vocaloid (Lola as the "voicebank") for some vocals.   "The Girl in Byakkoya" shares its name with an alcoholic cocktail served in the café bar GAZIO, which serves other cocktails named after Hirasawas songs; the establishment is operated by Yuichi Hirasawa, Susumu Hirasawas brother. 

==== Track listing ====
#  
#  
#  
#  
#  
#  
# "Lounge" (2:05)
#  
#  
#  
#  
# "Parade (instrumental)" (5:51)
#  

==Release==

===Festivals===
Paprika premiered on September 2, 2006, at the 63rd Venice Film Festival.   It screened at the 44th New York Film Festival, playing on October 7, 2006. It competed at the 19th Tokyo International Film Festival October 21–29, 2006, as the opening screening for the 2006 TIFF Animation CG Festival.  It also competed in 27th Fantasporto from February 23 to March 3, 2007. Paprika was shown at the 2007 National Cherry Blossom Festival in Washington, D.C., as the closing film of the Anime Marathon at the Freer Gallery of the Smithsonian, and at the 2007 Greater Philadelphia Cherry Blossom Festival. It played at the Sarasota Film Festival on April 21, 2007, in Sarasota, Florida. Additionally, it was shown at the 39th International Film Festival in Auckland, New Zealand, on July 22, 2007, and was shown as the festival traveled around New Zealand.

==Reception==
 
Paprika has received positive reviews from most film critics. It holds an 84% "Fresh" approval rating on the review aggregator website Rotten Tomatoes, based on 85 reviews, with an average rating of 7.3/10 and the consensus, "Following its own brand of logic, Paprika is an eye-opening mind trip that rarely makes sense but never fails to dazzle. The film weaves in and out of dream worlds seamlessly and presents an offbeat puzzle of a fantasy."  Metacritic, which assigns a weighted average score, rated the it 81 out of 100, based on 26 reviews from film critics.  Paprika won the Best Feature Length Theatrical Anime Award at the sixth-annual Tokyo Anime Awards during the 2007 Tokyo International Anime Fair. 
 Andrez Bergen of Yomiuri Shimbun praised the Paprika as the "most mesmerizing animation long-player since Miyazakis Spirited Away five years ago" (in 2001). He also praised the films animation and backgrounds.  Mick LaSalle of the San Francisco Chronicle gave the a positive review, saying that the film is a "sophisticated work of the imagination" and "challenging and disturbing and uncanny in the ways it captures the nature of dreams". LaSalle later went on to say that the film is a "unique and superior achievement."  Rob Nelson of The Village Voice praised the film for its visuals. However, he complained about the plot, saying that Paprika is not "a movie thats meant to be understood so much as simply experienced - or maybe dreamed." Nelson later went on to say that Kon "maintains a charming faith in cinemas ability to seduce fearless new (theater) audiences, even one viewer at a time."  Manohla Dargis of The New York Times said that the film has a "sense of unease about the rapidly changing relationship between our physical selves and our machines." Dargis praised Kon for his direction, saying that he "shows us the dark side of the imaginative world in Paprika that he himself has perceptively brightened."  Helen McCarthy in 500 Essential Anime Movies said that Paprika "proves once again that the great science fiction doesnt rely on giant robots and alien worlds". 

Conversely, Roger Moore of the Orlando Sentinel gave a negative review, saying "With a conventional invade-dreams/bend-reality plot, its a bit of a bore. Its not as dreamlike and mesmerizing as Richard Linklaters rotoscope-animation Waking Life, less fanciful than the Oscar-winning anime Spirited Away."  Bruce Westbrook of the Houston Chronicle said the film "is as trippy as a Jefferson Airplane light show" and criticized the characters and the dialogue. 
 The Lord Time Out Time Out Newsweek Japan 100 best films of all time, while the American edition of Newsweek included it among its top twenty films of 2007.  Metacritic has listed the film among the top 25 highest-rated science fiction films of all time,  and the top 30 highest-rated animations of all time. 

===Awards and nominations===
Paprika received the following awards and nominations: 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2006  Festival du nouveau cinéma|Montréal Festival of New Cinema 
|Publics Choice Award Satoshi Kon
|  
|- 2006  63rd Venice International Film Festival Golden Lion|Golden Lion (Best Film) Satoshi Kon
|  
|- 2007
|Fantasporto Critics Choice Award (Prêmio da Crítica) Satoshi Kon
|  
|- 2007
|Newport Beach Film Festival Feature Film Award for Best Animation Satoshi Kon
|  
|- 2007
|Online Online Film Critics Society Awards Online Film Best Animated Film
|
|  
|}

==Live-action adaptation==
A live-action adaptation of Paprika, to be directed by Wolfgang Petersen, was in development in 2010.  However, since the release of Inception, the Christopher Nolan movie which came out that same year and was inspired by this anime,  there has not been any significant update to whether Petersens adaptation will be produced.

==See also==
* Japanese films of 2006
* Lucid dream
 

==References==
 

==External links==
 
*   (US)
*   (Japan)
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 