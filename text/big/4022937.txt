Northfork
 
{{Infobox film name            = Northfork image           = Northfork poster.jpg caption         =  director        = Michael Polish producer        = Mark Polish Michael Polish writer          = Mark Polish Michael Polish  starring        = James Woods Nick Nolte Duel Farnes Mark Polish Daryl Hannah Peter Coyote Robin Sachs music           = Stuart Matthewman cinematography  = M. David Mullen editing         = Leo Trombetta distributor     = Paramount Classics released        = July 11, 2003 (limited) runtime         = 103 minutes language        = English budget          = $1,900,000 gross           = $1,599,804 
}} Twin Falls Idaho (1999) and Jackpot (2001 film)|Jackpot (2001).

==Plot==

The films narrative consists of several interwoven subplots taking place in the town of Northfork, Montana circa 1955. A new dam is being built which will flood the valley of Northfork, and the town is in the midst of an evacuation. The narratives focus on several individuals who, for one reason or another, have yet to evacuate. Walter OBrian (Woods) and his son (Mark Polish) are on the evacuation team, helping to evacuate the last few inhabitants of Northfork. Father Harlan (Nick Nolte) is one such individual, who has stayed behind to care for Irwin (Duel Farnes), a dying orphan too weak to leave town. While the OBrians and their co-workers encounter an array of unusual characters, Irwin discovers that he is the "unknown angel" and finds himself a family in his dreams.

==Reception==
Northfork received mixed to positive reviews from critics and has a rating of 56% on Rotten tomatoes based on 101 reviews with an average rating of 6 out of 10. The consensus states "Visually poetic, but may be too dramatically inert for some."  The film also has a score of 64 on Metacritic based on 31 reviews. 

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 