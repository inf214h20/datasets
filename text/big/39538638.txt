The Goose of Sedan
{{Infobox film
| name           = The Goose of Sedan
| image          = 
| image_size     = 
| caption        = 
| director       = Helmut Käutner 
| producer       = Paul Claudon   Walter Ulbrich
| writer         = Jean LHôte (novel)   Helmut Käutner
| narrator       = 
| starring       = Hardy Krüger   Jean Richard   Dany Carrel   Françoise Rosay
| music          = Bernhard Eichhorn 
| editing        = Klaus Dudenhöfer
| cinematography = Jacques Letellier UFA   C.A.P.A.C.
| distributor    = UFA (West Germany)
| released       = 22 December 1959
| runtime        = 90 minutes
| country        = France   West Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} West German comedy war film directed by Helmut Käutner and starring Hardy Krüger, Jean Richard and Dany Carrel. It was based on the novel Un Dimanche au Champ DHonneur by Jean LHôte. The film was one of a growing number of co-production (filmmaking)|co-productions between the two countries during the era.  It was also released under the alternative title Without Trumpet or Drum.

==Synopsis==
After the Battle of Sedan during the Franco-Prussian War a pair of soldiers, one French and one German, become separated from their respective units.  Taking shelter in a farmhouse, the two begin to bond despite their rivalry over a woman.

==Partial Cast==
* Hardy Krüger as Fritz Brösicke 
* Jean Richard as Leon Riffard 
* Dany Carrel as Marguerite 
* Françoise Rosay as La grand-mère de Marguerite 
* Theo Lingen as Colonel Tuplitz 
* Helmut Käutner as Königliche Hoheit 
* Fritz Tillmann as Hauptmann Knöpfer 
* Ralf Wolter as Uhlan Lehmann
* Lucien Nat as Captain 

==References==
 

==Bibliography==
* Bergfelder, Tim. International Adventures: German Popular Cinema and European Co-productions in the 1960s. Berghahn Books, 2005.
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 