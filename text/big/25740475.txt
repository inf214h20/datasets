Tenkōsei
{{Infobox film
| name           = Tenkōsei
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 転校生
| kana           = てんこうせい
| romaji         = Tenkosei}}
| director       = Nobuhiko Obayashi
| producer       = 
| writer         = Wataru Kenmotsu
| starring       = Satomi Kobayashi Toshinori Omi
| music          = 
| cinematography = 
| editing        = 
| studio         = Art Theatre Guild Nippon Television
| distributor    = Shochiku
| released       =  
| runtime        = 112 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 1982 Japanese fantasy directed by Nobuhiko Obayashi. It is also known by the alternate English titles Exchange Students and I Are You, You Am Me. 

==Cast==
* Toshinori Omi as Kazuo Saitō
* Satomi Kobayashi as Kazumi Saitō Makoto Satō as Akio Saitō (father of Kazuo)
* Kirin Kiki as Naoko Saitō (mother of Kazuo)
* Jō Shishido as Kouzou Saitō (father of Kazumi)
* Wakaba Irie as Chie Saitō (Mother of Kazumi)
* Masuno Takahashi as Masuno Saitō (Grandma)
* Munenori Iwamoto as Masaaki Kaneko
* Daisuke Ohyama as Kenji Sakui
* Etsuko Shihomi as Mitsuko Ono

==Awards==
4th Yokohama Film Festival  
*Won: Best Film
*Won: Best Screenplay - Wataru Kenmochi
*Won: Best Newcomer - Satomi Kobayashi

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 


 