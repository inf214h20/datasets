Ezhamkadalinakkare
{{Infobox film 
| name           = Ezhamkadalinakkare
| image          = Ezhakadalinakkare.jpg
| caption        =
| director       = IV Sasi
| producer       = NG John
| writer         = A. Sheriff
| screenplay     = A. Sheriff
| starring       = P. Bhaskaran Henry Marsal Janardanan Jo Washington
| music          = M. S. Viswanathan
| cinematography = K Ramachandrababu
| editing        = K Narayanan
| studio         = Geo Movies
| distributor    = Geo Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi and produced by NG John. The film stars P. Bhaskaran, Henry Marsal, Janardanan and Jo Washington in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*P. Bhaskaran
*Henry Marsal
*Janardanan
*Jo Washington
*K. R. Vijaya
*MG Soman Padmini
*Ravikumar Ravikumar
*Reena Reena
*Seema Seema
*Vidhubala
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madhumaasam bhoomithan || P Jayachandran || P. Bhaskaran || 
|-
| 2 || Madhumaasam bhoomithan || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Malaranippanthalil || Vani Jairam || P. Bhaskaran || 
|-
| 4 || Suralokajaladhaara || Vani Jairam, Jolly Abraham || P. Bhaskaran || 
|-
| 5 || Swargathin nandana || P Susheela, P Jayachandran || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 