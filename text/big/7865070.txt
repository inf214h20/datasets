A Time for Dancing
{{Infobox film
| name           = A Time for Dancing
| image          = A_Time_for_Dancing_DVD_case_cover.jpg
| caption        = DVD cover
| director       = Peter Gilbert
| writer         = Davida Wills Hurwin
| starring       = Larisa Oleynik Shiri Appleby Peter Coyote
| music          = Laurence Rosenthal
| cinematography = Alex Nepomniaschy
| editing        = Amy E. Duddleston Stuart H. Pappé
| distributor    = StudioWorks Entertainment
| released       = 2000
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
|}}
 2000 United American drama drama film starring Larisa Oleynik, Shiri Appleby and Peter Coyote, and directed by Peter Gilbert. The movie is an adaptation based on the novel of the same title by Davida Wills Hurwin.
 blockbuster in Italy reaching the top position for several weeks. It was released on Showtime (TV network)|Showtime, and was nominated for a Daytime Emmy in 2004. 

Sam Russell (Shiri Appleby) tells the story of her best friend Jules Michaels (Larisa Oleynik). They met at the age of 6 in a dance class. Over the years they had become best friends. Sam dances, but Jules is a true dancer,with true passion towards it and views it as important as life itself. Unfortunately, her passion becomes impossible when it turns out she has cancer.

Even after the bad news has been confirmed, Jules has a hard time dealing with it and still insists upon going for dance. To decrease the rate of which the cancer is spreading, she started going for chemotherapy, which leaves her very exhausted after each time. It also causes her hair to drop out, as such, Jules gradually has no choice but to start accepting the fact and that she has to stop dance because her body is always too lethargic.

However, Jules stops the chemo to dance once more and auditions for Juilliard in NYC as it has always been a dream of hers to get in. It took a lot out of her but it paid off and she got accepted.

Not long after though, Jules lost the fight to cancer and died.

Sam opens the letter from Juilliard and replies:
"Jules Michaels wont be attending Juilliard cause she died".  

==References==
 

 
 
 
 
 
 
 
 
 


 