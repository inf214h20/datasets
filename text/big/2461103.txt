Bitter Rice
{{Infobox film
| name           = Riso amaro
| image          = Movies-Riso_Amaro.jpg
| caption        = Theatrical poster
| director       = Giuseppe De Santis
| producer       = Dino De Laurentiis
| writer         = Giuseppe De Santis Carlo Lizzani Gianni Puccini
| starring       = Vittorio Gassman Doris Dowling Silvana Mangano Raf Vallone Checco Rissone Carlo Mazzarella
| music          = Goffredo Petrassi
| cinematography = Otello Martelli
| editing        = Gabriele Varriale
| distributor    = Lux Film Distributing Corporation
| released       =  
| runtime        = 108 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
Bitter Rice ( ) is a 1949 Italian film made by   can mean either "rice" or "laughter", riso amaro can be taken to mean either "bitter laughter" or "bitter rice".
 100 Italian films to be saved.

==Plot==
The film begins at the start of the rice-planting season in northern Italy. In an effort to escape the law, two small-time thieves, Francesca (Doris Dowling) and Walter (Vittorio Gassman), hide amongst the crowds of female workers heading to the rice fields of the Po Valley. While attempting to board the train for the fields, the pair runs into Silvana (Silvana Mangano), a peasant rice worker. Francesca boards the train with Silvana, who introduces her to the planters way of life. Francesca does not have a work permit, and struggles with the other "illegals" to find a place on the rice fields. After initial resistance from documented workers and bosses, the scabs are allowed a place in the fields. At the fields, Silvana and Francesca meet a soon-to-be-discharged soldier, Marco (Raf Vallone), who unsuccessfully tries to attract Silvanas interest. 

Toward the end of the working season, Walter arrives at the fields, intending to steal a large quantity of rice. Excited by his criminal lifestyle, Silvana becomes attracted to Walter. She causes a diversion to help him carry out the heist, but Francesca and Marco manage to stop Walter and his accomplices. Francesca and Silvana face each other, armed with pistols; Francesca confronts Silvana and explains that she has been manipulated by Walter. In response, Silvana turns her gun toward Walter and murders him. Soon afterward, her guilt leads her to commit suicide. As the other rice workers depart, they pay tribute to her by sprinkling rice upon her body.

==Cast==
* Vittorio Gassman as Walter
*  )
* Silvana Mangano as Silvana (Italian voice: Lydia Simoneschi; English dubbing: Bettina Dickson) 
* Raf Vallone as Marco
* Checco Rissone as Aristide
* Nico Pepe as Beppe
* Adriana Sivieri as Celeste
* Lia Corelli as Amelia
* Maria Grazia Francia as Gabriella
* Dedi Ristori as Anna
* Anna Maestri as Irene
* Mariemma Bardi as Gianna
* Maria Capuzzo as Giulia
* Isabella Zennaro as Rosa
* Carlo Mazzarella as Gianetto

==Themes==
In the film, the character Silvana represents enchantment with behavior modeled in American films, such as chewing gum and boogie-woogie dancing. Her downfall shows director Giuseppe De Santiss condemnation of these products of American capitalism.  In addition, Silvana was considered by many audiences to be overly-sexualization|sexualized. This sexualization and the melodramatic presence of death and suicide in the film cause it to diverge from typical Italian neorealism. 

==Awards and honors== Best Story 1950 Academy Awards.
 100 Italian films to be saved, a collection of films that "changed the collective memory of the country between 1942 and 1978".    The collection was established by the Venice Film Festival in collaboration with Cinecittà and curated by Fabio Ferzetti, with input from Gianni Amelio and other Italian film critics. Many of the films selected represent the "Golden Age" of Italian cinema, which was manifested in the neorealist movement. 

==References==
* 
* 

==Notes==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 