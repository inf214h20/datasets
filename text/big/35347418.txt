Transgression (1931 film)
{{Infobox film
| name           = Transgression
| image          = TransgressionFilmPoster.1931.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Herbert Brenon Ray Lissner (assistant)
| producer       = William LeBaron Herbert Brenon
| writer         = Benn W. Levy
| based on       =  
| screenplay     = Elizabeth Meehan
| narrator       = 
| starring       = Kay Francis Paul Cavanagh Ricardo Cortez
| music          = Max Steiner
| cinematography = Leo Tover Arthur Roberts
| studio         = 
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 70 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1931 Pre-Code drama film directed by Herbert Brenon, using a screenplay written by Elizabeth Meehan, adapted from Kate Jordans 1921 novel, The Next Corner.  The film stars Kay Francis (on loan from Paramount), Paul Cavanagh (on loan from Fox), and Ricardo Cortez, and deals with the romantic entanglements of a wealthy English businessman, his wife and a Spanish nobleman.
 RKO Radio Pictures, it was premiered in New York City on June 12, 1931, with a national release two weeks later on June 27.

==Plot summary==
Required to travel to India for a year to oversee financial matters, English businessman Robert Maury (Paul Cavanagh) is in a quandary regarding his young wife.  His older sister, Honora (Nance ONeil), suggest that he leave her at their country estate, where she can keep an eye on her.  But his wife, Elsie (Kay Francis), is fearful of the boredom which may set in if she were to remain on the isolated property. Maury gives in to his wifes fears, and decides to allow her to move to Paris for the duration of his time on the sub-continent.

In Paris, she falls under the guidance of the sophisticated Paula Vrain (Doris Lloyd), who begins to teach her how to fit into the decadent Parisian lifestyle. She quickly assimilates to her surroundings, and begins to attract attention from the men in her social sphere. One in particular, a Spanish nobleman named Don Arturo de Borgus (Ricardo Cortez), begins to pay her special attention. Elsie struggles to keep the relationship platonic, and as her husbands year-long absence draws to a close, she decides that the temptation has become too great. With Maurys return imminent, Elsie is convinced to attend one last party by Paula, who unbeknownst to Elsie is working on Don Arturos behalf. At the party the Spanish nobleman gives Elsies seduction one last-ditch attempt. And it is beginning to work. Arturo invites Elsie to spend the weekend at his estate in Spain.  She is considering the offer when Maury shows up unexpectedly. He is dismayed by the changes in his wife. He had left an innocent behind, and now he has come back to a sophisticated, jaded woman. His dismay, coupled with their year-long separation, causes him to act cool towards her. It is this coolness which makes up her mind.  When Maury requests that she return to England with him the next day, she defers, saying she wants to stay behind to say goodbye to the friends she has made while in Paris. 

After Maury leaves for England, she heads to Arturos.  Once there, Arturo begins an all-assault to sexually seduce her. In this, he is abetted by his servant, Serafin (John St. Polis). As she weakens, before she will fully succumb, her conscious makes her write a letter to her husband in England, confessing everything. She gives the letter to Serafin to post for her, and is about to fully give in to Arturo, when a local peasant, Carlos (Agostino Borgato), appears and accuses Arturo of seducing and impregnating his young daughter, who died during childbirth. Furious, Carlos shoots and kills Arturo. Horrified at her almost tragic mistake, she realizes that she must intercept her confession before Maury has an opportunity to read it. Serafin claims that he has already written it, so Elsie determines to return to the English estate and intercept it there.

Back in England, she waits day by day to head off the postman. Her furtive actions arouse the suspicions of Honora. When she discovers a news article regarding Arturos death, those suspicions are heightened, believing that Elsie might have been the unnamed woman mentioned in that article. When she accuses Elsie of infidelity in front of Maury, he defends his wife, leading to Honora deciding to finally leave the estate. It is shortly after that Serafina arrives, threatening to reveal Elsie and Arturos relationship to Maury, and claiming that he is carrying the confessional letter. Realizing that she loves her husband, she refuses to help in the plan to hurt him. When Serafin confronts Maury with the lurid details, he is disappointed, for Maury refuses to be outraged. Chastened, Serafin departs, and Maury accepts his wife back into his loving arms.

==Cast==
 
(Cast list as per AFI database) 

* Kay Francis as Elsie Maury
* Paul Cavanagh as Robert Maury
* Ricardo Cortez as Don Arturo de Borgus
* Nance ONeil as Honora "Nora" Maury
* Doris Lloyd as Paula Vrain
* John St. Polis as Serafin, Arturos butler
* Ruth Weston as Viscountess de Longueval
* Adrienne DAmbricourt as Julie
* Agostino Borgato as Carlos

==Production==
A remake of the 1924 silent film, The Next Corner, which was produced by Paramount Pictures, was put on RKOs production schedule. During production, the film was known by the name of the earlier picture, as well as Around the Corner,    but in early June, the final name was announced as Transgression.  The new name was selected as a result of a pool among the publicity men employed by RKO. 
 William Welch.  

By the end of April it was decided that Leo Tover would be the films cinematographer, re-uniting him with Brenon for the first time in four years. Brenon had directed three of the first five films Tover had photographed, the last being 1927s The Telephone Girl.  The two had collaborated on the first film version of the famous F. Scott Fitzgeralds classic novel, The Great Gatsby.    RKO premiered the film in New York city on June 12, 1931. It was released nationally later in the month on June 27. 

===French version===

RKO produced a French version of the film titled Nuit dEspagne, which was directed by Henri de la Falaise. It was announced in April that he would be directing the version, which would be the third French variation he did for RKO.  He selected Jeanne Heibling and Geymond Vital to play the Francis and Cavanaugh roles, respectively.  The only actor to appear in both films was actress Adrienne DAmbricourt, in a small role.    The film went into production in early May, 1931. 

==Critical response==
Motion Picture Daily did not enjoy the film.  While they complimented the acting corps on their performances, and Brenon on his directing, they felt the script was completely inadequate, stating, "Herbert Brenons direction is satisfactory and had he had something to work with the picture might have had a chance".  The Film Daily likewise had a lukewarm opinion of the film, also praising the acting and directing, but finding the overall story "punchless".  They called the film a "highly sophisticated drama", but complained that it contained "... little comedy relief and a wealth of talk, but particularly no action."  Screenland did not like the story, but also enjoyed the acting. 

The most comprehensive review of the film came from Mordaunt Hall of The New York Times. And it was by far the most complimentary. He gave good marks to Brenons direction, calling it a mostly "intelligently filmed story", stating that Brenon had managed in "eliciting the spectators interest", and that the photography had "some compelling atmospheric effects in some of the sequences." He did have some reservations about the overall film, however, stating that "It is not endowed with any great degree of subtlety and the comings and goings of the characters are set forth a trifle too abruptly." Hall also felt the acting was done well, giving particular praise to Paul Cavanaugh and Nance ONeil, although he felt that Ricardo Cortez could have given a stronger performance.   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 