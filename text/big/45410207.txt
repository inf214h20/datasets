Three Who Paid
{{Infobox film
| name           = Three Who Paid
| image          = 
| alt            = 
| caption        = 
| film name      =   Colin Campbell
*William A. Wellman  (assistant director)  }}
| producer       =  
| writer         =  
| screenplay     = Joseph F. Poland 
| story          = 
| based on       =   
| starring       = {{Plain list|*Dustin Farnum
*Bessie Love
*Frank Campeau}}
| narrator       =  
| music          = 
| cinematography = Don Short 
| editing        = 
| studio         = Fox Film  
| distributor    = Fox Film 
| released       =    reels 
| country        = United States Silent  (English intertitles) 
| budget         = 
| gross          =  
}} Western Melodrama#Film|melodrama George Owen Colin Campbell,   and stars Dustin Farnum, with Bessie Love and Frank Campeau. 

==Plot==
Riley Sinclair (Farnum) seeks to avenge the death of his brother, whose three companions – Quade, Sanderson, and Lowrie – left him to die in the desert. Two of the three men die, and the third is spared so that he can confess to the crime. Sinclair helps John Caspar (Love), a schoolteacher, who is actually a rich young woman who is trying to get away from her opportunist husband. When her identity is revealed, she and Sinclair fall in love.      

==Cast==
*Dustin Farnum as Riley Sinclair   
*Bessie Love as Virginia Cartright/John Caspar
*Fred Kohler as Jim Quade
*Frank Campeau as Ed Sanderson
*Robert Daly as Sam Lowrie
*Wiliam Conklin as Jude Cartright
*Robert Agnew as Hal Sinclair

==Production==
The film was filmed in Orange County, California.  Production was delayed when Bessie Love, who was responsible for her own wardrobe, forgot to bring spirit gum to hold her wig, the prop man gave her LePages glue, which adhered the wig to her head. 

==Release and reception==
The film was "a first rate production",  but had issues. For the parts of the film when her character is masquerading as a man, Bessie Love was deemed unconvincing.  Overall, the film received mixed reviews. Positive reviews:
* 
* 
* 
* 
* 
  Mixed & lukewarm reviews:
* 
* 
  Negative reviews:
* 
* 
* 
 

On its release, some theaters showed the film with the short Nobodys Darling.   

==References==
;Notes
 

;Bibliography
* 

==External links==
* 
* 


 
 
 
 
 
 