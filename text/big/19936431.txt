Mermaid Got Married
 
{{Infobox film
| name           = Mermaid Got Married
| image          = MermaidGotMarried.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional poster.
| director       = Norman Law Man
| producer       = Clarence Ford
| writer         = 
| screenplay     = Jo-yee Cheung
| story          =  Splash
| starring       = Ekin Cheng Christy Chung Takeshi Kaneshiro
| music          = 
| cinematography = Hung-Chuen Lau
| editing        = Hsiung Pan
| studio         = Wishing Well Film Company
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 96 min.
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK $3,250,291 
}}
 1994 Cinema Hong Kong fantasy film|fantasy-romantic comedy film directed by Norman Law Man, starring Ekin Cheng, Christy Chung and Takeshi Kaneshiro. Based on the US film Splash (film)|Splash, the plot involves a mermaid from under the sea who travels on to land and falls in love with a human man.

==Plot==
In the opening scene, a young boy named Chi is on a boat with his mother and her boyfriend. He falls overboard and almost drowns because he cannot swim. However he ends up on a beach, and claims that he was rescued by a "big fish".

Years later, Chi (Ekin Cheng) has grown up but still has a fear a water due to the accident. He has become a teacher, and obtains a new job at a local high school. Students are drawn to him because of his youth and good looks. Despite his attempts to be professional, he is aggressively flirted with by Kiki (Teresa Mak), a popular girl who has the nickname "Princess". This makes Kenji (Takeshi Kaneshiro), Kikis on-again-off-again boyfriend, jealous.

One day Chi brings his class on a field trip to the docks. During an altercation he is pushed into the water, sinks and loses consciousness. He is rescued by an orange-tailed mermaid (Christy Chung), who takes him to shore. She is the same mermaid that rescued him when they were young, and due to this second meeting believes they were destined to meet. She releases a magic pearl from her stomach, which she uses to revive a still unconscious Chi. She is interrupted when people approach and flees in a panic. Chi accidentally swallows the pearl and wakes up, thinking that he was merely swept to shore by the water. The mermaid attempts to swim home, but discovers that she cant without her pearl. She decides to find Chi, knowing that she can pass as human with legs as long as she doesnt get wet.

The mermaid tracks Chi down to his school, and there she is mistaken for a workers niece, "Siu May". She is befriended by Kiki and attends the school as a student. Although Siu May can walk on legs, she must bathe every so often to "recharge". One of these sessions takes place in the girls bathroom, and her large tail is accidentally seen by Miss Yuen (King-Tan Yuen), a teacher, who faints in shock. Siu Mays secret is also discovered by Kenji, who promises to help keep her hidden, and the two becomes friends. After this encounter Miss Yuen becomes obsessed with finding and catching this "big fish", enlisting the schools principal (Kent Cheng) and other friends to her cause.

Siu May and Chi spend time together and become close. Chi discovers he can swim now, which is due to the pearl in his stomach; he and Siu May swim together in the school pool, with Chi agreeing to be blindfolded when Siu May claims she doesnt have a bathing suit. Siu May decides that the best way to get her pearl back is to kiss him. After a dinner date together eating clams, Siu May gets the chance, but she is so caught up with her new feelings for Chi that she forgets to summon the pearl. On Chis birthday, he confesses that he has feelings for her, and Siu May protests that he cant, because she is a "fish". Siu May stands under the rain and allows herself to transform into her mermaid shape. Chi falls down in shock, and at that moment Miss Yuen and her colleagues run in and capture Siu May, carrying her away.

Chi, Kenji and Kiki work together to rescue Siu May where she is being imprisoned in a house swimming pool. They are successful, and the next morning return to the docks. Siu May has her pearl now and can return home. Chi and Siu May declare their love for each other, and Chi adds that hell wait for her. Siu May jumps into the sea, transforms and swims away.

Afterward, Miss Yuen, the principal and group follow Chi around in the hopes of finding Siu May again. One day Chi  bumps into a bridesmaid at a wedding, who looks identical to Siu May. The principal takes a hose from a nearby gardener and starts spraying the new girl. She doesnt transform into a mermaid, which makes Miss Yuen and group assume she is not Siu May, so they wander off. But when Chi invites the new girl to eat clams she gets excited and lets slip that she is, indeed, Siu May. The pair embrace.

==Cast==
* Ekin Cheng - Chi, a young teacher at a high school, he was saved from drowning twice by a mermaid, and eventually falls in love with her.
* Christy Chung - Siu May, a mermaid who can gets legs when on dry land, she falls in love with Chi.
* Takeshi Kaneshiro - Kenji, a high school student who befriends Siu May.
* Teresa Mak - Princess/Kiki, a high school student who befriends Siu May.
* Kent Cheng - Principal, the principal of the high school.
* King-Tan Yuen - Miss Yuen, a teacher at the high school.
* Siu-Ming Lau - Uncle Lau, a janitor/general worker at the high school, he mistakes the mermaid for his real niece, Siu May.
* Dennis Chan - Officer Chan, a friend of Miss Yuen and the Principal, he works together with them to capture the mermaid.
* Elaine Law Suet-Ling - Fatty, Kikis best friend.
* Rocelia Fung Wai-Hang - Chis mother.

==Soundtrack==
Faye Wong performed two songs for the film:
* "Angel" - the main theme, played during Chi and Siu Mays swimming pool scene and during the end credits;
* "Sky" - played while Chi walks sadly on the shore after Siu May leaves.
These songs are released in Faye Wongs 1994 album Sky (Faye Wong album)|Sky.

==Reception==

 

==References==
 

== External links ==
* 

 
 
 
 
 
 
 