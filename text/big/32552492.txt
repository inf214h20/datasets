Winnie Mandela (film)
{{Infobox film
| name = Winnie Mandela
| image = Winnie Mandela film.jpg
| alt = 
| caption = film poster
| director = Darrell Roodt
| producer = Michael Mosca T.D. Jakes
| writer = Darrell Roodt, Andre Pieterse, Paul Ian Johnson (Screenplay)
| based on =  
| starring = Jennifer Hudson Terrence Howard Wendy Crewson Elias Koteas Justin Strydom
| cinematography = Mario Janelle
| editing = Sylvain Lebel
| studio = Equinoxe Films
| distributor = Image Entertainment D Films TDJ Enterprises Film Bridge International
| released =  
| country = South Africa / Canada
| language = English
| gross = $80,634 
}}
Winnie Mandela is a 2011 drama  , and stars Jennifer Hudson, Terrence Howard, Wendy Crewson,  Elias Koteas, and Justin Strydom. Image Entertainment released the film in theaters on September 6, 2013.       

==Plot== Free State town, betrayal by friends and allies, and more than a year in solitary confinement.  Upon her release, she continues her husbands activism against apartheid and, after his release from prison, suffers divorce due to her infidelity and political pressures.  She also faces accusations of violence and murder and in the end, must own up to her actions in court, while many still remain loyal to her because of her fight against apartheid. 

==Cast==
* Jennifer Hudson as Winnie Mandela/Winnie Madikizela-Mandela
* Terrence Howard as Nelson Mandela
* Wendy Crewson as Mary Botha
* Elias Koteas as de Vries  

==Production==
Writers Andre Pieterse and Darrell Roodt, who also directed, developed the screenplay based on Anne Marie du Preez Bezrobs biography Winnie Mandela: A Life.  The film was produced by Equinoxe Films.  Filming took place in Johannesburg, Cape Town, and Robben Island in South Africa beginning in April 2010. 

==Controversy==
Winnie Mandela criticized the fact that she was not consulted for the making of a film about her life, stating, "I have absolutely nothing against Jennifer  , but I have everything against the movie itself.  I was not consulted. I am still alive, and I think that it is a total disrespect to come to South Africa, make a movie about my struggle, and call that movie some translation of a romantic life of Winnie Mandela." 

Some South African actors also criticized the selection of American actors for South African roles; Oupa Lebogo, general secretary of the Creative Workers Union (CWU) said of Hudsons casting, "This decision must be reversed, it must be stopped now. If the matter doesnt come up for discussion, we will push for a moratorium to be placed on the film being cast in South Africa. We are being undermined, there is no respect at all." 

A preview of the film released in November 2010 was referred to by the filmmakers as originating from computer hackers, saying it was an "unauthorised download originated from a secure website belonging to our sales representative, Filmbridge ... The material was not formally edited, graded or sound-mixed. At this stage, the producers have elected to make no further comment and are investigating the matter." 

==Release and reception==
The film premiered at the Toronto International Film Festival in September 2011. 

By April 2012, T.D. Jakes and his company TDJ Enterprises/Film Bridge International had taken over the production, distribution and marketing of the film.   It was released to a limited number of theatres in Canada on October 5, 2012.  On May 16, 2013, Image Entertainment acquired the rights to release the film in North America.   Retrieved May 17, 2013   

The film holds a   rating on Rotten Tomatoes based on reviews from 34 critics. The sites consensus states, "Winnie Mandela takes few chances and delves only superficially into its subjects life, making it feel more like a too-earnest made-for-TV movie than a proper biopic."  Rick Groen of Globe and Mail says, "Winnie begins as hagiography and ends in hellish confusion," and Linda Barnard of the Toronto Star said, "It wont satisfy history students curious about this polarizing figure, nor fans of its star, Oscar winner Jennifer Hudson."    David Rooney of The Hollywood Reporter stated, "Solid performances are undercut by lack of storytelling integrity in this plodding biopic,"  and Ed Gibbs of The Guardian said, "This syrupy biography of the former wife of Nelson Mandela seeks to sugar-coat South Africas complex history." 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 