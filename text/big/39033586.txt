Sivasakthi
{{Infobox film
| name           = Sivasakthi
| image          = 
| image_size     =
| caption        = 
| director       = Suresh Krissna
| producer       = M. G. Sekar S. Santhanam
| writer         = Balakumaran (dialogues)
| screenplay     = Suresh Krissna
| story          = Santosh Saroj
| starring       =   Deva
| cinematography = V. Pratap
| editing        = B. Lenin V. T. Vijayan
| distributor    = M. G. Pictures
| studio         = M. G. Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil crime Prabhu and Rambha in Deva and Telugu under the same title. It is loosely a remake of the Hindi movie Agneepath (1990 film)|Agneepath.  

==Plot==

Marc Zuber Antony alias Tony (Mahesh Anand), an international smuggler, ended up in an isolated island after killing some navy officers. He decided to hide his merchandises there and he threatened the inhabitants of the island. In the process, Tony killed the islands school teacher (Girish Karnad) in front of his wife, his son Siva and his daughter Priya.

Many years later, Siva (Sathyaraj) becomes rich in the city and lives with his sister Priya (Rambha (actress)|Rambha) and his mother (Sujatha (actress)|Sujatha). Siva is haunted by his fathers murder and he tries to take revenge with the help of his friend Anbu (Nizhalgal Ravi).

Priya falls in love with a happy-go-lucky man Sakthi (Prabhu (actor)|Prabhu). Later, Siva appoints Sakthi and he becomes his best friend.

One day, Siva kills in front of his mother Tonys henchman and he reveals to his mother that he is a smuggler. Shocked after hearing this, Sivas mother and Priya leave his house and they decide to live in Sakthis house.

Actually, Sakthi is an undercover police officer who tries to arrest Siva. Tony was supposed to be dead according to the police report but Sakthi finds out that he is not dead. The rest of the story is what happen to Siva, Sakthi, Priya and Tony.

==Cast==

*Sathyaraj as Siva Prabhu as Sakthi Rambha as Priya Sujatha as Sivas mother
*Mahesh Anand as Marc Zuber Antony alias Tony
*Nizhalgal Ravi as Anbu
*Girish Karnad as Sivas father
*Ragasudha as Vandana
*K. S. Jayalakshmi as Kameswari
*R. N. K. Prasad as R. N. K. Prasad
*Jaya Prahasam as Gaja
*Mohan V. Ram  Devayani in a guest appearance

==Soundtrack==

{{Infobox album |  
| Name        = Sivasakthi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 15:20
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Chumma Chumma || S. P. Balasubrahmanyam, Ragini Santhanam || 4:30
|- 2 || Mannin Mainthan || S. P. Balasubrahmanyam || 0:40
|- 3 || My Name Is Sakthi || S. P. Balasubrahmanyam || 3:20
|- 4 || Na Dhin Dhinna || Mano (singer)|Mano, Swarnalatha || 3:10
|- 5 || Marumagale || S. P. Balasubrahmanyam, K. S. Chithra || 3:40
|}

==References==
 

 

 
 
 
 
 
 