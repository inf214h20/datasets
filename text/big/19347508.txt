Boxing Gloves (film)
{{Infobox film
| name           = Boxing Gloves
| image          =
| image size     =
| caption        = Robert A. McGowan as Anthony Mack
| producer       = Robert F. McGowan Hal Roach
| writer         = Robert F. McGowan Hal Roach H. M. Walker
| narrator       =
| starring       = Joe Cobb   Norman Chaney   Allen Hoskins   Jean Darling   Harry Spear   Mary Ann Jackson   Bobby Hutchins   Jackie Cooper
| music          = Ray Henderson
| cinematography = F. E. Hershey Art Lloyd
| editing        = Richard C. Currier MGM
| released       = September 9, 1929
| runtime        = 17 21" 
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Robert A. McGowan under the pseudonym "Anthony Mack".       Produced by Hal Roach and released to theaters by Metro-Goldwyn-Mayer on September 9, 1929, it was the 90th Our Gang short to be released.

==Plot==
Joes and Chubbys friendship is tested when their affections for Jean cause the two best friends to fight. At the same time, Farina and Harry have been unsuccessfully trying to go into business as fight promoters, and find in Joes and Chubbys rivalry the ingredients for a perfect boxing match.

==Notes==
*Boxing Gloves marks the film debut of Jackie Cooper, who would go on to a successful career both with and after Our Gang.
*The third Our Gang short made with sound, Boxing Gloves was advertised as "all-talking," although much of the film is silent footage without overdubbed sound. It is the earliest Our Gang entry included in the Little Rascals television syndication package.

==Cast==
===The Gang===
* Norman Chaney as Chubby Chaney
* Joe Cobb as Joe Cobb
* Jean Darling as Jean
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Harry Spear as Harry
* Pete the Pup as Himself

===Additional cast===
* Jackie Cooper as First angry spectator/Reporter at fight
* Bobby Mallon as Announcer Graham McCracker
* Andy Shuford as Chubbys trainer
* Donnie Smith as Donnie Charlie Hall as Sidewalk diner attendant
* Johnny Aber as Extra
* Godfrey "Duffy" Craig as Extra
* Bill Johnson as Undetermined role
* Billy Schuler as Undetermined role

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 