Bloodwork (film)
 
{{Infobox film
| name           = Phase One
| image          =BloodworkPoster1.jpg
| caption        = Theaterical film poster
| director       = Eric Wostenberg
| producer       = David S. Ward Brandon Nutt Chris Chesser Karen Glasser Ken Locsmandi
| writer         = David Nahmodan
| starring       = Travis Van Winkle Tricia Helfer Mircea Monroe
| music          = Lee Sanders
| cinematography = Vinit Borrison
| editing        = Olena Kuhtaryeva
| distributor    = IFC Films
| released       = May 10, 2012  (Netherlands) 
| runtime        = 
| country        = United States Canada
| language       = English
| budget         = $3 million 
| gross          = $12,345,456  
}}
Phase One (alternatively titled Bloodwork or The Last Experiment ) is a 2012 Canadian-American thriller directed by Eric Wostenberg and written by David Nahmod. The film stars Travis Van Winkle and Tricia Helfer. Filming began on 9 November 2009   Northern Life. 10 November 2011  and wrapped in December. 

== Plot ==
Two friends have hopes of traveling around Europe. As they need to fund the trip, they enlist as patients in allergy drug testing, a sure way to make easy money. However it turns out that rather than allergy medicine testing, they have enlisted in a government sponsored program on human regeneration. They are overwhelmed by the reality and seek to escape the medical unit. 

== Cast ==
* Travis Van Winkle as Greg
* Tricia Helfer as Dr. Wilcox
* Mircea Monroe as Stacey
* John Bregar as Rob Jeffries
* Tamara Feldman 
* Yanna Mcintosh as Patrica
* James Purcel as Taylor
* Rik Young as Nigel Denton
* Joe Pingue as Aaron
* Stephen Bogaert as Ira
* Anna Ferguson as Maggie
* Eric Roberts as The man in suit

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 