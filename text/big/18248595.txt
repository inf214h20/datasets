Leelai
{{Infobox film
| name = Leelai
| image = Leelai - Poster.jpg
| director = Andrew Louis
| producer = Ramesh Babu
| writer = Andrew Louis
| starring =  
| music = Satish Chakravarthy
| editing = Saravanan
| cinematography = Velraj
| studio = R Films Aascar Film Pvt. Ltd
| released = 27 April 2012
| runtime = 
| country = India
| language = Tamil
| budget =
}} Manasi Parekh Santhanam and Suhasini Raju in pivotal roles.  The film which began in May 2008, opened after production delays on 27 April 2012 to positive reviews.    

==Plot==
The film begins with a trio of three friends in college (Malar,Mona,Gayathri). Mona falls in love with a guy called Karthik (Shiv Pandit) despite the advice of Malar (Manasi Parekh Gohil) who suspects Karthik to be a complete flirt. One day, while on a date, Mona mentions to Karthik, Malars views about him and this gets Karthik to start hating Malar for interfering in his love life. When Karthik calls Mona, Malar picks the call up accidentally and they get into an argument with Karthik venting out his anger towards her interfering in his love life. Shortly later, he breaks up with Mona. Gayathri believing Mona was a wrong choice in Karthiks life and begins to date Karthik, while Malar is firmly against it. The relationship ends the same way as his previous relationship with Mona. A year or two later both Karthik and Malar work at the same software company at different floors in the same building. One day, while trying to call his friend on Malars floor, he accidentally calls Malar and the two realize their identities and presence in the building and the age-old feud resumes. Over lunch, both Malars friend (Vicky) and his own friend (Suja) advise him to sort it with Malar. Vicky questions why Karthik would fight with such a beautiful woman such as Malar, Karthik not knowing what Malar looks like decides to take a look at her. When he sees Malar he falls in love at first sight, he then tries to reconcile with Malar, but Malar is not willing to forgive a flirt such as Karthik. Karthik becomes desperate and begins to follow Malar and in a chance meeting orchestrated by Karthik he introduces himself as a sweet and kind person by the name of Sundar. Malar who has never met Karthik face to face, believes his false identity and begins to open up to Sundar. They start to get to know each other and meet often, when one day his friend Suja who works with Malar finds out about his game. Suja offers Karthik an ultimatum to reveal his true identity, once Malar declares her love for Sundar. Karthik tries to salvage his relationship with hilarious consequences. But, all said and done, how Karthik reveals to Malar about his true identity and her acceptance or rejection forms the crux of the story. Vicky (N. Santhanam|Santhanam) Malars friend provides extra comic relief to the story.

==Cast==
*Shiv Pandit as Karthik Manasi Parekh as Karunai Malar Santhanam as Vicky
*Suhasini Raju as Suja
*Maya as Mona
*Vibha Natarajan as Gayathri
*Lakshmi Ramakrishnan as Karthiks mother

==Production== Ravichandran and Manasi Parekh who has been part of popular shows on Television such as Star One’s India Calling and Star Pluss Gulaal (TV series)|Gulaal. A major portion of the film was shot in the popular software company HCL (Chennai Office). The song "Oru Killi" was shot at the Maheshwar Fort in Madhya Pradesh, while the other songs were shot in Goa and Puducherry respectively.
 Ravichandran eventually won the case, forcing the other film to change its title from Leelai to Naan Aval Adhu.  The film had been ready for release since 2010, with Shiv Pandit in between making his début in Hindi films with Shaitan (film)|Shaitan.  The failure of Ramesh Babus other production Moscowin Kavery starring Rahul Ravindran and Samantha Ruth Prabhu|Samantha, also became a factor behind the delay.
 Ravichandran signed a deal to distribute Billa II, dubbed in trade circles as the "hottest film of the year", and thus theatres have shown their support for his brother by giving Leelai a big release. Furthermore after the success of Oru Kal Oru Kannadi, actor N. Santhanam|Santhanams popularity heightened with several of his films being released around the same period such as Kadhal Pisase and Vinayakudu (film)|Vinayaga to capitalize on his market reach.  

==Critical reception== Manasi Parekh bring the urban Chennaiite alive - they are expressive, emote well, and nowhere do their lines sound jarring".  Vivek Ramz of in.com rated the film 3/5 and noted that "Leelai is a watchable rom-com" and added "If you are game for urban romance, then Leelai is worth a watch for its simple yet realistic treatment with fun".  Another critic gave the film three stars out of five, concluding "Leelai may have its drawbacks, yet it is feel-good, fun and engaging. Job well done Andrew Louis".  Rohit Ramachandran of Nowrunning.com rated it 2.5/5 stating that "For the most part, Andrew Louiss Leelai is earnest in its narration and interested in its characters". 

==Release==

Despite positive reviews from the critics, the film managed to do average business at the box office mainly due to a complete lack of publicity and predominantly having new faces in the cast.  The film also saw a delayed release due to production issues.

== Soundtrack ==
{{Infobox album|  
  Name        = Leelai
|  Type        = Soundtrack
|  Artist      = Satish Chakravarthy
|  Cover       = Leelai_Soundtrack_Album_Cover.jpg 
|  Released    = 24 August 2009
|  Recorded    = Feature film soundtrack
|  Length      = 28:50 Sony Music
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Leelai (2009)
|  Next album  = Kanimozhi (film)|Kanimozhi (2010)
}}
The music is composed by debutant Satish Chakravarthy, a classmate of the director, Andrew Vasanth Louis.  Satish also wrote the lyrics for two of the songs.  The soundtrack released on 24 August 2009, whilst the music composer later stated that he has also worked on a song for the theatrical trailer. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| title1          = Ponmalai
| extra1          = Benny Dayal
| title2          = Oru Killi Oru Killi Reprise
| extra2          = Satish Chakravarthy
| title3          = Jilendru Oru Kalavaram 
| extra3          = Satish Chakravarthy
| title4          = Bubble Gum
| extra4          =  Sunitha Sarathy, Benny Dayal, Suvi
| title5          = Unnai Partha Pinbhu
| extra5          = Haricharan, Marianne 
| title6          = Oru Killi Oru Killi
| extra6          = Shreya Ghoshal, Satish Chakravarthy
}}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 