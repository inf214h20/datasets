Mara Maru
{{Infobox film
| name           = Mara Maru
| image          = File:Mara Maru - Poster.jpg
| image_size     = 
| caption        = 1952 Theatrical Poster Gordon Douglas
| producer       = David Weisbart
| writer         = N. Richard Nash
| based on       = story by Phillip Yordan Sidney Harmon Hollister Noble
| narrator       = 
| starring       = Errol Flynn Ruth Roman
| music          = Max Steiner
| cinematography = Robert Burks
| editing        = Robert L. Swanson
| studio         = 
| distributor    = Warner Bros.
| released       = April 23, 1952 Of Local Origin
New York Times (1923-Current file)   23 Apr 1952: 25.  
| runtime        = 98 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.5 million (US & Canada rentals)  970,719 admissions (France) 
}}
 Gordon Douglas. It stars Errol Flynn and Ruth Roman.   
==Plot summary==
Gregory Mason and Andy Callahan are partners in a salvage business in Manila. Callahan is murdered after some drunken talk about sunken treasure; Mason is suspected by Lt Zuenon of the local police because he is in love with Callahans wife, Stella.

Mason is arrested but is released after Steven Ranier comes forward and says he witnesses the murder. Ranier says he is a private investigator who worked for Callahan and Mason hires him to help him find Callahans killer.

Ranier introduces Mason to Brock Benedict, who wants Mason to find and recover a diamond-encrusted religious icon off the Philippines coast, located in a sunken PT boat on which Mason and Callahan served. He offers to split the proceeds with Mason and Stella.

Mason goes to find the treasure on board the "Mara Maru", with Benedict and Stella coming along. Stella falls in love with Mason and discovers Benedict is planning to kill him as soon as the treasure is found. She tells Mason this but he insists on continuing the expedition.

Mason finds the icon and at first intends to keep it for himself. A storm hits and Stella, Mason and Masons assistant Manuelo take refuge in a village. Manuel steals the cross intending to return it to the church, but is caught by Senor Ortega, who originally took the cross to hide it from the invading Japanese. Mason gets the cross off Ortega, just as Benedict and his henchmen arrive. A fight ensues in which Ortega gives his life to save Mason. Mason hands the cross to Manuelo to give to the church and he and Stella are united.
==Cast==
* Errol Flynn as Gregory Mason
* Ruth Roman as Stella Callahan
* Raymond Burr as Brock Benedict
* Paul Picerni as Steven Ranier Richard Webb as Andy Callahan
* Dan Seymour as Lt. Zuenon
* Georges Renavent as Ortega (as George Renavent)
* Robert Cabal as Manuelo
* Henry Marco as Perol

==Production==
Warner Bros announced in January 1950 that they had bought the films story off Philip Yordan, Sydney Harmon and Hollister Noble. Drama: Garbo Picture All Set; Douglas Subject Bought; U-I on Headline Trail
Schallert, Edwin. Los Angeles Times (1923-Current File)   16 Jan 1950: 23.   Ivan Goff and Ben Roberts were originally reported as working on the script, which was described as about five war veterans who buy a Japanese war boat and set about salvaging a sunken war vessel.  Everett Freeman was assigned to produce. FILMLAND BRIEFS
Los Angeles Times (1923-Current File)   05 June 1950: B12.   In July 1950 Warners announced it for the coming year. JACK WARNER INDICATES NO TV FILMS PLANNED
Los Angeles Times (1923-Current File)   14 July 1950: 22.  

In May of the following year David Weisbart was announced as producer. Warner Bros. Lists 52 Films on Schedule
Los Angeles Times (1923-Current File)   30 May 1951: A3.   In September, Warners said that Errol Flynn would star and Gordon Douglas would direct.  Richard Nash had written the script. ERROL FLYNN GETS LEAD IN SEA STORY: Actor Will Star for Warners in Mara Maru, Dealing With Search for Lost Bullion Leonard Gets Assignment
By THOMAS M. PRYOR Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   19 Sep 1951: 36.  

Ruth Roman and Raymond Burr were brought on board to support Flynn. Drama: Desert Rats Follows Desert Fox; Unusual Napoleon Film Slated
Schallert, Edwin. Los Angeles Times (1923-Current File)   23 Oct 1951: B7.   (Burrs weight had gone down from 300 to 185 pounds. Drama: Eileen Christy Lead With Bill Shirley
Los Angeles Times (1923-Current File)   30 Nov 1951: 26.  )

Flynn made the film after an extensive period of travelling.  

It was shot at Warner Bros and on location at Los Angeles and Newport Harbors, Balboa Island, Catalina Island and the San Fernando Mission (doubling for a Manila Cathedral). Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 177  
==Reception==
According to the New York Times
 The gobbledegookish title of Warners Mara Maru is not the only obscure and unexciting thing about this stale adventure film. Its wholly improbable build-up of a criss-cross of rivalries... is bleakly confused and grossly tiresome, and when the action does finally get around to the business of diving for the treasure it is hackneyed and cheaply emotionalized. Even Errol Flynn and Ruth Roman as the working stars in its cast give the impression of being bored and indifferent toward it all.  
The Los Angeles Times said that "while its scenes of physical action are scarcely original... they keep ones eyes on the screen with a fair amount of absorption. What ails the overall production is that its performers talk too darn much." Flynn Finds Drama on Ocean Floor
Scheuer, Philip K. Los Angeles Times (1923-Current File)   03 May 1952: 12.  

The Chicago Daily Tribue called it a "preposterous affair" with "a boringly complicated plot... poor." Flynn Is Rugged Deep Sea Diver in This Movie: "MARA MARU"
Tinee, Mae. Chicago Daily Tribune (1923-1963)   08 May 1952: c5.  

It was the last movie Flynn made for Warner Bros in Hollywood for that studio, where he had started out in 1935. However he did go on to appear in The Master of Ballantrae for them, and return to make Too Much, Too Soon. Drama: MGM Gives Big Boost to Total Output; Errol Flynn Ends Warner Pact
Schallert, Edwin. Los Angeles Times (1923-Current File)   22 Mar 1954: B7.  
==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 