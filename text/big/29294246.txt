The Girl in Possession
{{Infobox film
  | name           = The Girl in Possession
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Monty Banks
  | producer       = Irving Asher
  | writer         = Monty Banks Henry Kendall
  | music          =
  | cinematography = Basil Emmott
  | editing        =  Warner Brothers-First First National Productions
  | released       = March 1934
  | runtime        = 72 minutes
  | country        = United Kingdom
  | language       = English
  }} Henry Kendall and directed by Monty Banks, who also wrote the screenplay and featured in the film himself.

The film was a quota quickie production shot at Twickenham Studios, with La Plante as wisecracking New York girl Eve Chandler who receives the good news that she has inherited a large country estate in England.  She crosses the Atlantic with her pal Julie, only to find that things are not as straightforward as she had been led to believe.  Complications ensue as she crosses paths with a silly-ass toff (Claude Hulbert), an unscrupulous continental lothario (Banks) and a snobbish butler (Charles Paton) before she manages to sort matters out with the help of the kindly Sir Mortimer (Kendall), with whom she falls in love.  

==Cast==
* Laura La Plante as Eve Chandler Henry Kendall as Sir Mortimer
* Claude Hulbert as Cedric
* Monty Banks as Caruso
* Bernard Nedell as de Courville
* Charles Paton as Saunders
* Millicent Wolf as Julie Garner
* Ernest Seton as Wagstaff

==Preservation status==
The Girl in Possession is classed by the British Film Institute as a lost film. 

When in February 1956, Jack Warner sold the rights to all of his pre-December 1949 films; inclusing (The Girl in Possession) to Associated Artists Productions (which merged with United Artists Television in 1958, and later was subsequently acquired by Turner Broadcasting System in early 1986 as part of a failed takeover of MGM/UA by Ted Turner).

==References==
 

==External links==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 


 