The Raid: Redemption
 
{{Infobox film
| name           = The Raid: Redemption
| image          = The Raid Redemption.jpg
| caption        = Theatrical release poster Gareth Evans
| producer       = Ario Sagantoro Gareth Evans
| starring       = {{Plain list |
* Iko Uwais
* Joe Taslim
* Donny Alamsyah
* Yayan Ruhian
* Pierre Gruno
* Tegar Setrya
* Ray Sahetapy
}}
| music          = Fajar Yuskemal Aria Prayogi    
     Mike Shinoda Joseph Trapanese  
| cinematography = Matt Flannery Dimas Imam Subhono
| editing        = Gareth Evans MNC Pictures XYZ Films
| distributor    = Celluloid Nightmares   Sony Pictures Classics Stage 6 Films  
| released       = 8 September 2011  (Toronto International Film Festival|TIFF)  20 January 2012  (Sundance Film Festival|Sundance)  23 March 2012  (Indonesia) 
| runtime        = 101 minutes  
| country        = Indonesia
| language       = Indonesian
| budget         = $1.1 million     
| gross          = $9.1 million   
}} Indonesian martial martial arts action film Gareth Evans and starring Iko Uwais.
 fight choreography by Uwais and Yayan Ruhian, who also worked on Merantau. The U.S. release of the film features music by Mike Shinoda and Joseph Trapanese.

After its world premiere at the   could not secure the rights to the title; this also allowed Evans to plan out future titles in the series.   It was released in the United States on DVD and Blu-ray Disc on 14 August 2012. 

A sequel, The Raid 2 (known as The Raid 2: Berandal in Indonesia), was released in 2014 to positive reviews; winning even more awards. Another sequel, The Raid 3, is planned for release in 2018 or 2019.

==Plot==
The film opens with Indonesian SWAT officer Rama praying, practicing silat and bidding goodbye to his wife, who is pregnant with his child. Rama joins a 20-man elite police squad, including Officer Bowo, Sergeant Jaka, and Lieutenant Wahyu, for a raid on an apartment block in Jakartas slums. The team intends to capture crime lord Tama Riyadi, who owns the block and lets criminals around the city rent rooms under his protection. Arriving undetected, the team sweeps the first floors and subdues various criminal tenants; they also temporarily detain an innocent tenant delivering medicine to his sick wife.  Continuing undetected to the sixth floor, the team is spotted by a young lookout, who raises the alarm before he is shot and killed by Wahyu.

Tama calls in reinforcements, who ambush the police officers patrolling the outside and the first five floors, killing and maiming a majority of them. Cutting the lights, Tama announces over the PA system that the police are trapped on the sixth floor stairwell, and he will grant free permanent residence to those who kill the intruders. In the darkness, the team is soon ambushed by shooters from above and behind, killing of a great number of them. Jaka learns from Wahyu that the mission is not officially sanctioned by the police command; nobody knows their location, and no backup or reinforcements will arrive. The remaining officers are forced to take cover in an empty apartment, where Officer Bowo is shot and injured by a mob of pursuing tenants. To save him, Rama constructs an improvised explosive device that kills the tenants, giving them a small window of time. With more antagonists approaching, the team splits into two groups covertly: Jaka, Wahyu, and Dagu retreat to the fifth floor, while Rama and Bowo ascend to the seventh.

Fighting their way to the apartment of the innocent tenant they released earlier, Rama and Bowo plead with him to help them; although his sick wife urges him to not get involved, he reluctantly agrees and hides the officers in a secret passage. A machete gang arrives and ransacks the mans apartment, but when they fail to find Rama and Bowo, they leave. After tending to Bowos wounds, Rama leaves him with the couple to search for Jakas team; however, he crosses paths again with machete gang. He manages to dispatch one member and flee, but is forced to fight the rest of them with his bare hands. Rama defeats and kills most of the gang, including their leader, who he uses to soften his fall when he jumps out of a window to an apartment below to flee another mob of pursuing tenants. He then continues his search, only to be captured by Andi, Tamas right-hand man. It is then revealed that Rama and Andi are estranged brothers, and that Rama signed up for the mission to search for Andi and convince him to return home, at the urging of their father.

Concurrently, Jaka and his group are found by Mad Dog, Tamas ruthless henchman. Wahyu flees, and Jaka orders Dagu to follow Wahyu. Mad Dog captures Jaka, but instead of shooting him, Mad Dog challenges him to hand-to-hand combat. After Mad Dog defeats and kills Jaka, he drags the corpse to an elevator.  Andi tells Rama to wait before leaving and meets up with Mad Dog.  However, Tama has seen Andi talk to Rama on the numerous security cameras in the building. Realizing Andis betrayal when he didnt return with a corpse, Tama stabs Andi in the hand and turns him over to Mad Dog.

Rama regroups with Wahyu and Dagu, who go on to fight through a narcotics lab, and they head for Tama on the 15th floor. Rama, finding Andi being beaten by Mad Dog, separates from Wahyu and Dagu to save him. Mad Dog allows him to free Andi and fights both brothers simultaneously. Mad Dog initially has the upper hand, but the brothers prove to be match for him when working together. After an intense and grueling fight, Mad Dog eventually overcomes the brothers, but is stabbed in the neck by Andi while trying to finish off Rama. This weakens him enough for the brothers to finally kill him.

Meanwhile, Wahyu and Dagu confront Tama, only for Wahyu to betray and kill Dagu. Wahyu takes Tama hostage with the intention of using him to escape, but Tama taunts Wahyu by revealing that Tama has been waiting for the team before the events of the movie began and Wahyu was set up by his corrupt higher-ups; even if Wahyu escapes, he will be killed later.  Wahyu kills Tama and attempts suicide, only to find that he has no bullets left.

Andi uses his influence over the tenants to allow Rama to leave with Bowo and a detained Wahyu. The tenant who protected Bowo watches from a window and grins with delight. Andi also hands over numerous blackmail recordings Tama made of corrupt officers taking bribes, hoping that Rama can use them to his benefit. Rama asks Andi to come home, but Andi refuses, due to his acclimation to his criminal lifestyle. Before Rama leaves, Andi asserts he can protect Rama in his role as a criminal boss, but that Rama could not do the same for him. Andi turns around and walks back to the apartment block with a grin that breaks into a wide-smile, whilst Rama, with Wahyu and an injured Bowo, exits to an uncertain future.

==Cast==
 
* Iko Uwais as Rama
* Donny Alamsyah as Andi
* Joe Taslim as Sergeant Jaka
* Ray Sahetapy as Tama Riyadi
* Yayan Ruhian as Mad Dog
* Pierre Gruno as Lieutenant Wahyu
* Tegar Satrya as Bowo
* Eka "Piranha" Rahmadia as Dagu
* Verdi Solaiman as Budi
* Ananda George as Ari
* Yusuf Opilus as Alee
* Iang Darmawan as Gofar
* M. Iman Aji as Eko
* Zaenal Arifin as Zaenal
* Hanggi Maisya as Hanggi
 

==Production== Gareth Evans Indonesian for Thugs), a large scale prison gang movie intended to star not only Merantau actors Iko Uwais and Yayan Ruhian but also an additional pair of international fight stars. A teaser trailer was shot, but the project proved more complex and time consuming than anticipated.  After a year and a half, Evans and the producers found themselves with insufficient funds to produce Berandal, so they changed the film to a simpler but different story with a smaller budget. They called the project Serbuan Maut (The Raid).  Producer Ario Sagantoro considers the film to be lighter than Merantau. Gareth Evans also considers it to be "a lot more streamlined," stating that "Merantau is more of a drama film|drama" while The Raid is more of a "survival horror film." 
 Panasonic AF100. 

All guns used in the film were Airsoft replicas, to avoid the costs associated with having to deal with firearms. All the shots of the guns actions cycling, muzzle flashes and casings ejecting were added digitally.

==Music==
 
While the film was still in production, in May 2011, Sony Pictures Worldwide Acquisitions acquired the distribution rights of the film for the U.S. and tasked Mike Shinoda of Linkin Park and Joseph Trapanese to create a new score for U.S. market.   The film premiered at the 2011 Toronto International Film Festival with the original score from the Indonesian version which was composed by Aria Prayogi and Fajar Yuskemal, who also composed Evanss previous film, Merantau. The Raid made its debut in the U.S. with Trapanese and Shinodas version at Sundance 2012. 

On his blog, Shinoda stated that his score was over 50 minutes and almost all instrumental. After film production, he had room for two more songs, but did not want to sing or rap, so he posted pictures of two music artists.  Deftones/Crosses (band)|††† frontman Chino Moreno guest performed "RAZORS.OUT", which was leaked online on 16 March 2012,  as rap group Get Busy Committee performed "SUICIDE MUSIC" for the film. 
 

==Release==
 ).  Deals were also made with distributors from Russia, Scandinavia, Benelux, Iceland, Italy, Latin America, Korea and India during the film screening at the TIFF. 

==Reception==

===Critical response===
Reviews were highly positive. Rotten Tomatoes gives the film a score of 85% based on reviews from 143 critics, with an average score of 7.5/10. The websites consensus was "No frills and all thrills, The Raid: Redemption is an inventive action film expertly paced and edited for maximum entertainment." 

Roger Ebert gave the film a single star out of four; he criticized the lack of character depth, and noted that "the Welsh director, Gareth Evans, knows theres a fanboy audience for his formula, in which special effects amp up the mayhem in senseless carnage."  Ebert was himself criticized for this assessment of the film, and he later published a defense of his review. {{cite web | last = Ebert| first = Roger | authorlink= Roger Ebert | url = http://www.rogerebert.com/rogers-journal/hollywoods-highway-to-hell
 | title = Hollywoods Highway to Hell :: rogerebert.com :: Reviews |publisher= Rogerebert.com | date= 2012-03-26 | accessdate= 2013-12-03}} 

===Box office===
In its Sony Pictures Classics debut in the United States and Canada on 23–25 March 2012, The Raid: Redemption grossed $220,937 from 24 theaters for a location average of $15,781.  For its widest opening release weekend in the United States and Canada on 13–15 April 2012, the film grossed $961,454 from 881 theaters, and ranked 11th overall.   In the United Kingdom, the film grossed $660,910 on its opening weekend.  In Indonesia, approximately 250,000 people watched the film in the first four days of release, and it was considered a great turnout for a country that only has about 660 theater screens nationwide.    As of 8 July 2012, the film has grossed $4,105,123 in North America. The film grossed approximately $9.1 million worldwide. 

==Accolades==
 The film received numerous awards and nominations both from local and international institutions.        At the 2012 Maya Awards, which is dubbed by local media as the Indonesian version of the   (the Indonesian equivalent to the Academy Awards); which was considered a snub.

{|  style="text-align: center" class="wikitable sortable"
!Year
!Award
!Category
!Nominee(s)
!Result
|- 2011
|2011 Toronto International Film Festival
|Peoples Choice Award - Midnight Madness
|The Raid: Redemption
| 
|- 2012
|Jameson Dublin International Film Festival Audience Award
|The Raid: Redemption
| 
|- 2012
|Jameson Dublin International Film Festival Dublin Film Critics Circle Awards for Best Film
|The Raid: Redemption
| 
|- 2012
|European Imagine Film Festival Silver Scream Award
|The Raid: Redemption
| 
|- 2012
|Indiana Film Journalists Association Best Foreign Language Film
|The Raid: Redemption
| 
|- 2012
|  Prix du Public
|The Raid: Redemption
| 
|- 2012
|1st Maya Awards Best Film
|The Raid: Redemption
| 
|- 2012
|1st Maya Awards Best Director Gareth Evans
| 
|- 2012
|1st Maya Awards Best Supporting Actor Ray Sahetapi
| 
|- 2012
|1st Maya Awards Best Make-Up Jerry Oktavianus
| 
|- 2012
|1st Maya Awards Best Sound Mixing Arya Prayogi
Fajar Y.
| 
|- 2012
|1st Maya Awards Best Promotional Poster Design
|The Raid: Redemption
| 
|- 2012
|1st Maya Awards Best Art Direction Timoty D. Setianto
| 
|- 2012
|1st Maya Awards Best Editing Gareth Evans
| 
|- 2012
|1st Maya Awards Best Special Effects Andi Noviandi
| 
|- 2012
|1st Maya Awards Best Cinematography Matt Flanery
| 
|- 2013
|Indonesian Indonesian Movie Awards Most Favorite Film
|The Raid: Redemption
| 
|- 2013
|Indonesian Indonesian Movie Awards Best Supporting Actor Ray Sahetapy
| 
|- 2013
|Indonesian Indonesian Movie Awards Best Supporting Actor Yayan Ruhian
| 
|- 2013
|Indonesian Indonesian Movie Awards Best On-Screen Duo Iko Uwais
Donny Alamsyah
| 
|}

==American remake== Gareth Evans Chris and Liam Hemsworth were being eyed for roles by the studio.  On 27 May 2014, Variety reported that the films production was delayed until early 2015.  On 13 June 2014, Frank Grillo was the first to be announced to star in the remake, and is a fan of the original.   On 16 June 2014, Geek Tyrant revealed that the remake will be set in the near future.  On 4 August 2014, The Wrap reported that Taylor Kitsch has been cast in the lead role, and also that XYZ Films is returning to produce the remake, which is expected to hew closely to the original film. 

==Sequel==
 
While developing The Raid in script form, Evans started to toy around with the idea of creating a link between it and his initial project, Berandal. It was later confirmed that Berandal would serve as a sequel to The Raid.   Evans has also stated his intention to make a trilogy. 

Sony pre-bought U.S., Latin American and Spanish rights to the sequel. Alliance/Momentum pre-bought for the United Kingdom and Canada; Koch Media acquired the film for German speaking territories; Korea Screen pre-bought for Korea; and HGC pre-bought for China. Deals for other major territories were also in negotiations. 

Titled Berandal for the Indonesian market and as The Raid 2 for U.S. market, the sequel had a "significantly larger" budget than its predecessor, and its shooting schedule included approximately 100 days of physical production.  Pre-production began in September 2012 while filming began in January 2013.  

==Other media==
A comic book based on The Raid was released on 21 May 2012. A stop-motion sixty second video depicting the plot of The Raid using cats made out of clay made by Lee Hardcastle was included in the special features disk. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 