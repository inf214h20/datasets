Proyecto Dos
{{Infobox film
| name           = Proyecto Dos 
| image          = Proyecto Dos film poster.jpg
| image size     = 
| alt            = 
| caption        = Proyecto Dos theatrical poster
| director       = Guillermo Fernández Groizard
| producer       = Mike Downey Zorana Piggott José Antonio Romero Sam Taylor
| writer         = Nacho Cabana Guillermo Fernández Groizard Manuel Valdivia Chus Vallejo Margarita Varea
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Helena Carrión Óscar Casas Adrià Collado Josep Maria Pou Lucía Jiménez
| music          = Daniel Sánchez de la Hera Christopher Slaski Crispin Taylor
| cinematography = 
| editing        = José Ramón Lorenzo Picado
| studio         = Film and Music Entertainment DeA Planeta Home Entertainment Ensueño Films Rioja Films Producciones Cinematográficas S.L.
| distributor    = DeA Planeta Home Entertainment Spain Sunfilm Entertainment Germany DVD
| released       =  
| runtime        = 141 minutes
| country        = Spain Spanish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Spanish action feature debut Malaga Spanish Film Festival,   and its theatrical debut on April 25, 2008.  

==Production==
The films working title was Laberinto de espejos.  The director stated that the casting process was tedious, but that they had the freedom to rewrite portions of the film as suitable actors were found, to better match character to actors.   The production was shot on locations in Argentina, London, England|London, and Madrid, Spain|Madrid.  The film went to festivals in Europe under the English language title of Project Two, and was released on DVD in 2009 under the German title Projekt 2.  It is screening in 2010 under the shorter title Dos. 

==Synopsis==
Diego leads a pleasant and ordinary life. One night on TV, he sees a man identical to himself get killed in a road accident in Argentina. From then on he begins to realise that nothing is what he thought: not his parents, not his wife... not even himself.

==Partial cast==
* Helena Carrión as Gema
* Óscar Casas as Mateo
* Adrià Collado as Diego Durand
* Josep Maria Pou as Malcolm
* Lucía Jiménez as Susan Durand
* Manuel Zarzo
* Tomás del Estal
* Yaiza Esteve as Olga
* Núria Gago as Emilia
* Jesús Granda
* Alfonso Lara as Martin
* María Luisa Merlo
* Carlos Olalla as Eldrich
* Bruno Squarcia as Dajanov
* Arturo Briones as Agente británico
* Cynthia Bachilieri as Sara
* Andrew Bicknell as John

==Reception==
 
Público (Spain)|Público found the style pleasing, writing "Proyecto Dos utiliza un estilo muy visual y un ritmo en el que combinan imágenes rápidas y ralentizadas para ir sumando preguntas en pos de la sorpresa final." (Project Two uses a visual style and rhythm which uses combined images that move quickly and then are slowed by additional questions after the final surprise.) 

Soitu found the pacing fine and discounted any inclination to compare it with similar American films when they wrote "Nos encontramos ante una película de acción y de intriga bastante bien resuelta.  La película tiene un buen ritmo y mantiene la intriga y la acción de manera convincente. Hay quien la clasificaría de “americana” en el sentido que es una película que recuerda a ciertas producciones de Hollywood, pero en mi opinión es una película española de un género y estilo al que no estamos acostumbrados en el cine de este país.  Las películas españolas habitualmente se relacionan con la comedia o con el cine de author y, en este caso, se está presentando una película con acción, intriga, persecuciones, tiros, effectos especiales… que pienso que es una propuesta interesante dentro de lo que se considera cine español. " (We are faced with an action movie and intrigue rather well resolved. The film has a good pace and treats the intrigue and action in a convincing manner.  There are some who would sort it as "American" in style and state it is a film that reminds them of Hollywood productions, but in my opinion is a Spanish film genre and style that are not used in the films of this country.  Spanish films are usually associated with comedy or art cinema, and in this case, we are presented with a film of action, intrigue, persecution, shots and special effects ... I think its an interesting proposal in what is considered Spanish cinema.) 

Marcus Littwin of Die-besten-Horrorfilme praised the film by writing "Wer extrem spannende Thriller mit leichten Mystery-Touch liebt, ist bei diesem spanischen Werk von Guillermo Fernández Groizard, der auch am Drehbuch mitgeschrieben hat, genau richtig.  Die Charaktere sind allesamt interessant gezeichnet und gut besetzt.  Die Story nimmt schnell Fahrt auf und fesselt den Zuschauer von der ersten Minute an.  Die eindrucksvollen Bilder und Visionen, die rasante Action und ultraspannende Story, die immer wieder mit Wendungen aufwartet, sind ein echter Adrenalinkick."  (If you love extremely exciting thrillers with a light-touch of mystery, this Spanish work of Guillermo Fernández Groizard who also co-wrote the script, is just right.  The story quickly takes a ride and ties itself to the audience from the first minute.  The characters are all interesting and the cast well drawn.  The stunning images and visions are offered in twists in a fast action and ultra-exciting story that gives a real adrenaline rush.) 

Conversely, Carmen Porschen of Movie Maze found the film started well, but had major flaws when she wrote "Die verwirrenden Bilder und Erzählungen erregen eine Menge Aufmerksamkeit und scheinen einen interessanten Plot einzuleiten.  Doch was folgt, ist ein dürres Krimifilmchen auf Fernsehniveau, bei dem alle tollen Einfälle einfach unoriginell abgegrast oder total vernachlässigt werden. Doch nach diesem hoffnungsvollen Start erlebt der spanische Film von Regisseur Guillermo Groizard einen kontinuierlichen Abbau, der nach und nach alle Hoffnungen auf einen klugen und überzeugenden Plot zerschlägt. Projekt 2 rutscht zusehends in die Sparte Fernsehkrimi ab, was daran liegen könnte, dass der Regisseur bisher nur für TV-Serien verantwortlich war. Im Film schlägt sich das nicht nur in den einfallslosen Geschehnissen nieder, sondern auch an den hölzernen Dialogen. Das kann zwar auch an der schlechten Synchronisation liegen, aber es fällt einem einfach immer wieder auf." (The disturbing images and stories excite a lot of attention and seem to take an interesting plot. But what follows is just a dry thriller movies akin to the television standard in which all the great ideas are unoriginal, grazed, or totally neglected.  After its promising start we experience in the Spanish film by director Guillermo Groizard a continuous degradation which gradually destroys all hope for an intelligent and compelling plot.  Project 2 slips rapidly into the category TV thriller, which could be because the director was previously responsible only for TV series. In the film itself suggests that... reflected not only in the uninspired events, but also to the wooden dialogue. While this can be due to the bad dubbing, it is repeated time and again.) 

==Recognition==
===Awards and nominations=== Malaga Spanish Film Festival 

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
*   at the Internet Movie Database

 
 
 
 