Along the Roadside
{{Infobox film
| name           = Along the Roadside
| image          = Along the Roadside Poster.jpg
| alt            =   Along the Roadside US Version
| caption        = 
| director       = Zoran Lisinac
| producer       = Vladimir Lisinac
| writer         = Zoran Lisinac
| starring       = Iman Crosson Angelina Häntsch
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Along the Roadside is a 2015 American comedy-drama film written and directed by Zoran Lisinac. His award-winning debut film stars Iman Crosson, Angelina Häntsch, Michael Madsen, Lazar Ristovski and Daniel Grozdich. 

The film tells a story about two people from different parts of the world (Varnie is from Oakland and Nena from Germany) who after a chance encounter set out on a road trip across California, both for the reasons of their own – Varnie escaping his pregnant girlfriend and Nena searching for the concert of her favorite band (the elusive Blonde Priest). The film explores themes of racism, social responsibility, "postromance" and culture clash against the backdrop of vast California – from San Francisco to LA and inland across the desert.   
Varnie, a self-proclaimed "sailor who has left one shore but hasnt yet reached the other one" could most aptly be fitted into, what the academic Marco Abel refers to as "postromance cinema" in which the characters reject he idealized notion of lifelong monogamy.

Varnie finds his antipode in Nena, a colorblind tourist from Germany who sees the world in all its shades of gray or as critic Aaron Shore, in his A-grade review of the movie, noted, "an interesting contrast to Varnies racial features as an African American." 

Along the Roadside has been named "the most unique film of the year" after its Mill Valley Film Festival premiere in October 2013. 

The use of the original music in this film is unique in that it serves as a Greek chorus – addressing both the characters and the audience alike.  The original songs were supplied by Cole Bonner to be passed off as the songs of the elusive band Blonde Priest - nobody in the story has heard of - which poses a question, "what is Nena really after?". 

Along the Roadside has been on the festival circuit since 2013 and is getting its commercial release on March 17, 2015.

==References==
 

==External links==
* 

 
 
 


 