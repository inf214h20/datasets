Testament of Orpheus
 
{{Infobox film
| name           = The Testament of Orpheus
| image          = Testamentoforpheus.jpg
| caption        = DVD cover
| imdb_rating    = 
| director       = Jean Cocteau
| producer       = Jean Thuillier
| writers        = Jean Cocteau
| starring       = Jean Cocteau Édouard Dermit Henri Crémieux María Casares
| music          = Georges Auric George Frideric Handel Martial Solal
| cinematography = 
| editing        = Marie-Josephe Yoyotte
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = France
| language       = French
| budget         = 
}}
Testament of Orpheus ( ) is a 1960 film directed by and starring Jean Cocteau. It is considered the final part of the Orphic Trilogy, following The Blood of a Poet (1930) and Orphée (1950). In the cast are Charles Aznavour, Lucia Bosé, María Casares, Nicole Courcel, Luis Miguel Dominguín, Daniel Gélin, Jean-Pierre Léaud, Serge Lifar, Jean Marais, François Périer and Françoise Sagan.

It also includes cameo appearances by Pablo Picasso and Yul Brynner. The film is in black-and-white, with just a few seconds of color film spliced in.

==External links==
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 

 
 