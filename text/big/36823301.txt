Miss Conspirator
{{Infobox film
| name           = Miss Conspirator 
| image          = File:Miss_Conspirator_poster.jpg
| film name      = {{Film name
| hangul         =   GO
| rr             = Misseu GO
| mr             = Missŭ GO}}
| director       = Park Chul-kwan 
| producer       = Jang So-heong   Kim Chang-a   Kim Myeong-sim 
| writer         = Jung Bum-sik   Jung Sik   Choi Moon-suk 
| starring       = Go Hyun-jung   Yoo Hae-jin
| music          = 
| cinematography = Ryu Eok
| editing        = Steve M. Choe
| distributor    = Next Entertainment World 
| released       =  
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Miss Conspirator ( ; lit. Miss Go) is a 2012 South Korean action comedy film starring Go Hyun-jung as a nerdy, reclusive cartoonist with a severe case of sociophobia who somehow gets mixed up in a drug deal involving one of the biggest organized crime groups in Korea, and is forced to deal with her phobia and interact with others as she runs from the police.   

==Plot==
Chun Soo-ro is a timid, geeky cartoonist with constant panic attacks and has debilitating phobia of all forms of social interaction. In the past, she relied on her sister to help her, but her sister is now about to leave the country. At the Busan port terminal, where her sister just departed, Soo-ro suddenly suffers another panic attack. At this time, a nun approaches her and helps her take her medication. Later, the nun asks Soo-ro to deliver flowers and a cake to a man that the nun confesses she loves.

Soo-ro unsuspectingly agrees to make the delivery and goes to the hotel room where the man is staying. Nobody answers the door, but the hotel room door is unlocked and slightly ajar. She walks into the hotel room to drop off the delivery, but is shocked to see a man, stabbed to death in a chair. Three other men then enter the hotel room. Soo-ro is able to hide then make a quick escape.

Unbeknownst to Soo-ro, she is now wanted by the police and two of the biggest crime syndicates. The cops believe she is part of a drug dealing operation worth 42 million dollars and the two gangs want to get back the drugs that was supposed to be delivered in the cake box.

With the help of five men she meets who turn her life upside down, Soo-ro slowly transforms into the queen of crime.  

==Cast==
*Go Hyun-jung - Chun Soo-ro    
*Yoo Hae-jin - Red Shoes 
*Sung Dong-il - Boss Sung
*Lee Moon-sik - Sa Yeong-cheol Go Chang-seok - Detective So
*Park Shin-yang - Baek Bong-nam
*Jin Kyung - Miss Go
*Nam Sung-jin - detective
*Ha Jae-sook - Young-shim
*Lee Won-jong - Soo-ros psychiatrist
*Kim Byung-chul - Dokgaegoori ("Poison Frog")

==References==
 

== External links ==
*    
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 