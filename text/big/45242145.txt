Is Matrimony a Failure?
{{Infobox film
| name           = Is Matrimony a Failure?
| image          =
| alt            = 
| caption        =
| director       = James Cruze
| producer       = Jesse L. Lasky Walter Woods Lois Wilson Walter Hiers ZaSu Pitts Arthur Hoyt Lillian Leighton
| music          =  Karl Brown
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent Walter Woods. Lois Wilson, Walter Hiers, ZaSu Pitts, Arthur Hoyt and Lillian Leighton. The film was released on April 16, 1922, by Paramount Pictures.  

==Plot==
 

== Cast ==
*T. Roy Barnes as Arthur Haviland
*Lila Lee as Margaret Saxby Lois Wilson as Mabel Hoyt
*Walter Hiers as Jack Hoyt
*ZaSu Pitts as Mrs. Wilbur
*Arthur Hoyt as Mr. Wilbur
*Lillian Leighton as Martha Saxby
*Tully Marshall as Amos Saxby
*Adolphe Menjou as Dudley King
*Sylvia Ashton as Mrs. Pearson
*Charles Stanton Ogle as Pop Skinner
*Ethel Wales as Mrs. Skinner
*Sidney Bracey as Bank President
*William Gonder as Policeman
*Lottie Williams as Maid
*Dan Mason as Silas Spencer
*William H. Brown as Chef 
*Robert Brower as Marriage License Clerk 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 