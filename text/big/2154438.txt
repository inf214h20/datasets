White Man's Burden (film)
{{Infobox film
| name           = White Mans Burden
| image          = White Mans Burden.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Desmond Nakano
| producer       = Lawrence Bender
| writer         = Desmond Nakano
| narrator       =
| starring       = John Travolta Harry Belafonte
| music          = Howard Shore
| cinematography = Willy Kurant
| editing        = Nancy Richardson
| studio         = UGC Rysher Entertainment A Band Apart
| distributor    = Savoy Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $3,760,525 (USA)
}}
 alternative US|America black and white United Americans have reversed cultural roles.  The film was written and directed by Desmond Nakano. 

The film revolves around Louis Pinnock, a white factory worker (John Travolta), who kidnaps Thaddeus Thomas, a black factory owner (Harry Belafonte) who fired him over a perceived slight. 

== Plot ==
Louis Pinnock (John Travolta) is a struggling urban factory worker. In this alternative reality, it is a large underclass of white Americans who live in rundown, crime-infested ghettos and face prejudice from the broader society, while the comfortable middle and upper class is predominantly black.

In an effort to go above and beyond in his position (hoping to become a foreman soon), in the candy factory in which he works, Pinnock delivers a package for his boss to successful CEO Thaddeus Thomas (Harry Belafonte). After Pinnock accidentally sees Thomass wife coming out of the shower, he is subsequently dismissed from his job, assaulted by the police and forced to watch his family evicted. In a radical quest for justice, Pinnock kidnaps Thomas, which forces the two men to bond, as well as argue over race relations and the roots of social inequality.

==Cast==

*John Travolta as Louis Pinnock
* Harry Belafonte as Thaddeus Thomas
* Kelly Lynch  as Marsha Pinnock
* Margaret Avery  as Megan Thomas Tom Bower as Stanley Andrew Lawrence as Donnie Pinnock
* Bumper Robinson  as Martin Tom Wright  as Lionel
* Sheryl Lee Ralph   as  Roberta Judith Drake  as Dorothy
* Robert Gossett   as  John
* Wesley Thompson  as  Williams Tom Nolan as Johansson
* Willie C. Carpenter as  Marcus
* Michael Beach  as a policeman

==Reception==

The movie gained a mixed to negative reception from movie critics.    

The movie was not a box office success. 

==See also==
* The White Mans Burden
* Fable (TV play)|Fable (TV play) (UK, 1965)
* Lions Blood

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 