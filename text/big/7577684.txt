Extreme Limits
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Extreme Limits
| image          = Extreme_Limits_DVD_Cover.jpg
| director       = Jay Andrews
| producer       = T. J. Terrier
| writer         = Steve Latshaw John Beck Julie St. Claire Hannes Jaenicke
| music          = Neal Acree
| cinematography = Andrea Rossotto
| editing        = Michael Kuge
| distributor    = Wynorski Collections Corporation
| released       = March 14, 2001 (North America)
| runtime        = 93 minutes
| country        = United States English
}}

  and where the death ray is discovered in Siberia]] B movie.
 John Beck), thug for hire Julian Beck (Hannes Jaenicke) attempts to steal it as its being flown back to the USA. After the plane crash-lands in Canada, CIA agent Jason Ross (Treat Williams) is tasked with the recovery of the device before it falls into enemy hands. While the movie is predominantly an action film, it does incorporate some elements of science fiction due to the circumstances surrounding the death ray.

Extreme Limits, although completed in 2000,  was not released on home video in North America until August 14, 2001. 
Footage from three other films (Cliffhanger, Narrow Margin, and The Long Kiss Goodnight) is used throughout Extreme Limits.

==Plot==
In the heart of Siberia, Dr. Maurice Hunter, a famous amateur archeologist, his daughter Nadia, and their group of guides are taking part in an expedition to locate a fabled death ray created by inventor Nikola Tesla. The team eventually finds the death ray hidden in a mountain cave. The guides are being paid to kill the Hunters and steal the death ray. Before they can murder the pair, however, Dr. Hunter activates the death ray.

The Hunters bring Teslas Ray to an eastern Russian city, and before boarding a charter plane back to the USA, are joined by their associates George (Dr. Hunters American partner) and Peter (a Russian born assistant).
 hijack the plane at gunpoint, and their leader identifies himself as Julian Beck. Beck demands the death ray from Dr. Hunter, and informs the passengers that they are going to be leaving a bomb behind in its place. This news riles up the passengers and a fight breaks out before the death ray can be transferred or the bomb received. In the ensuing struggle, gunfire is exchanged and, as a result, the plane loses control; this loss in altitude violently severs the tether, causing the bomb to be dropped, blowing up the other plane. After a brief struggle to stay airborne, Capt. Lorenzo crash lands the jet in the Canadian Rockies, breaking the tail away from the fuselage, and sucking all of the terrorists out except for Beck.

Meanwhile, the CIA is tracking the flight of the plane. After confirming that the plane has in fact crashed, Ross and Douglas are rerouted to the Canadian Rockies to aid in the recovery effort, and to assure that the ray does not fall into enemy hands.

At the crash site, Dr. Hunter take on the role of de facto leader and sends his daughter Nadia down the mountain with the death ray to contact the authorities while the rest of the survivors stay behind. Moments after she heads out, Beck regains consciousness, and begins pursuit after her. Before nightfall, Nadia reaches an empty ski cabin, breaks in, and contacts CIA
Director Rance. Beck, having lost Nadias trail, hikes into town and calls for backup. With the death rays location known, agents Ross and Douglas arrive at the cabin to accompany Nadia and the device back to safety.  As the group is about to leave, Beck appears in another Helicopter, killing Douglas. Ross and Nadia make their way out the back with the death ray, and escape in an old pickup truck. Pursued through the forest, Ross and Nadia quickly lose Beck, and board a passenger train heading to Niagara Falls, Ontario.

Facing freezing cold, and with no signs of rescue, the crash survivors start to grow restless. The appearance of a bear outside the wreckage prompts George and Capt. Lorenzo to lead a group out in to the wilderness, hoping to find help; Dr. Hunter, Peter, and a few other stay behind. That evening, while looking for firewood, Peter is mauled and killed by the bear; witnessing this, Dr. Hunter and fellow passenger Stuart Elliot go out on a mission to kill the bear. Finding the bear the next night, Stuart is killed stabbing the bear with a syringe of battery acid and holding it in place while Dr. Hunter shoots it in the head. Meanwhile, the group led by George and Capt. Lorenzo overnights in a cave. The next morning, after the group resumes their hike down the mountain, George dies suddenly of a heart attack. After the group spots a rescue helicopter in the distance searching for the crashed plane, one of the members fires a gun at it, starting a massive avalanche. Capt. Lorenzo and the majority of the party are killed, leaving only two survivors.
 hotwires a Rainbow Bridge.  Ross staggers out of the truck and Beck seems to have won when Nadia, in her car, flies in and saves Rosss life. Ross jumps in and as they are driving away, Ross reveals that he has set the self-destruct on the death ray, prompting Nadia to drive full speed. The death ray then explodes, blowing up the bridge and Beck.

Back in the Rockies, a helicopter discovers the crash site, and the remaining passengers of the crashed plane are rescued. Later, when Dr. Hunter inquires as to what happened to the death ray, Ross smiles, answering simply, "Its history."

===Inconsistencies===
There are a large number of inconsistencies throughout the film, likely due to the heavy use of footage from other sources. For example, when the plane crashes in the mountains, it is shown coming to rest with the front half of the fuselage hanging off a rocky cliff; moments later the plane is shown completely on solid ground in what looks like a forest. In another scene, where agent Ross and Nadia are chased by a helicopter in a pickup truck, the model of truck changes repeatedly throughout. Even the DVD packaging contains numerous inconsistencies in the description, stating:

 

This synopsis is incorrect in that the flight is carrying a death ray (not explosives), the plane crashes in Canada (not Alaska), Agent Ross works for the CIA (not the U.S. Military), and that Agent Ross main concern is recovering the death ray (not rescuing the passengers).

==Cast==
The top-billed actors are:
*   and hand-to-hand combat. John Beck as Dr. Maurice Hunter: Amateur archeologist and thrill seeker. Before discovering the death ray, Dr. Hunter traveled the world, searching for other lost artifacts.
* Julie St. Claire as Nadia Hunter: Dr. Hunters daughter, she accompanies him on his expedition to discover the death ray. Often expressing bitterness towards his lifestyle, she is more than willing to risk her life to protect him, Agent Ross, and the death ray.
* Hannes Jaenicke as Julian Beck: Criminal hired to intercept the death ray. Beck is a world class thug-for-hire, taking on the highest bids from world governments to recover and steal sensitive items.

The second-billed actors are:
* Gary Hudson as Capt. Edward Lorenzo: Compromised pilot working with Julian Beck. Capt. Lorenzo works frequently with terrorist organizations, receiving large sums of money in his off-shore bank account for his services. Despite this, he shows signs of remorse, disliking those that pay him and attempting to lead his passengers to safety after the plane crash.
*  , she survives the crash and develops a deep relationship with Dr. Hunter. Having written several books that Dr. Hunter has read, the two become close friends and discuss their pasts as they await rescue.
* Steve Franken as Stuart Elliott: Crash survivor who describes himself as simply an "innocent bystander". Stuart sees clumsy and inept soon after the plane crashes, but quickly proves his worth by giving his life protecting the other passengers from a wild bear.
* Ava Fabian as Jessica Martin: Host of a TV nature show and crash survivor, she has little actual outdoor experience.
* Lorissa McComas as Wendy Yates: Crash survivor and daughter of Bob Yates, her main goal after the plane crashes is keeping her father alive.
* J. Patrick McCormack as Bob Yates: Crash survivor and father of Wendy Yates. He is diabetic and, after all of his insulin is lost in the crash, poses a serious problem to the rest of the passenger.
*  , she has an ongoing and difficult relationship with fellow passenger and agent Don Spengler.
*  .
* John Putch as George: As Dr. Hunters American assistant, he works logistics behind the scenes of his adventures. He also works out deals for TV and book deals once Dr. Hunter has found what hes looking for.
* Allan Kolman as Peter: As Dr. Hunters Russian assistant, he works logistics behind the scenes of his adventures. He is also responsible for doing background checks on any people Dr. Hunter will interact with over the course of his travels.
* Richard Riehle as Alan Douglas: CIA Agent and conspiracy theorist. Partners with Agent Ross, he is constantly discussing his time in the CIA during the Kennedy assassination and cold war. He is later killed by Beck. William Monroe as Herb Carver: CIA Agent working with Julian Beck. He meets Ross and Nadia on the train, and later allows Beck to steal the death ray.
* George Buck Flower as Edward Simmons: Train conductor

Also credited are:
* Kyle Heffner as Jessica Martins Manager
* Nikki Fritz as Linda: Air Traffic Supervisor tracking the death ray.
* Eric Baum as Winston: Air Traffic Controller tracking the death ray.
* Jack Shearer as Rance: CIA Director
* Robert Covarrubias as Vargas: Guide who helps Dr. Hunter find the death ray, and then turns on him.
* Bongo the Bear as Himself

== Reception ==
The film was generally panned by critics, who complained of "terrible dialogue",  "major plot holes",  and the pilfering of existing footage from other films such as Cliffhanger.   
While Treat Williams performance was received fairly well with one critic saying he "did his best with the material",  the acting from other cast members was seen as "laughable".  These apparent shortcomings lead to the  film being widely regarded as a "cheap action flick". 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 