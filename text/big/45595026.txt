The World of Alfred Hitchcock
{{Infobox film
| name = The World of Alfred Hitchcock
| director = Annett Wolf 
| writer = Annett Wolf
| with = Alfred Hitchcock Bruce Dern
| cinematography = Donald H. Stern
| editing = William H. White
| production companies = DR (broadcaster) Don Stern Productions 
| released = 1976
| runtime = 46 minutes
| country = Denmark / United States
| language = English
}}
 The World of Alfred Hitchcock  is a 1976 Danish T.V. documentary directed by Annett Wolf, about the English film director Alfred Hitchcock. Shot after the completion of Family Plot, it was part of a series of films she made in Hollywood during the one year leave of absence she took from DR (broadcaster)

==Synopsis==
In the setting of his office at Universal Studios, Alfred Hitchcock shares his vision of what an artistic film should be in the framework of a commercial industry, the difference between suspense films and mystery films, his conception of eroticism and his vision of Scandinavian women. He also explains in details how he created the suspense in the famous sequence of North by Northwest in the corn field. The film contains an original interview with Bruce Dern, who describes Hitchcock’s method as a director, and film clips from North by Northwest, Frenzy, Topaz, and Family Plot.

 
==See also==
* 1976 in film
* Cinema of Denmark

==External links==
*  

 
 
 
 
 
 
 


 