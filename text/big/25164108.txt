Natarang
{{Infobox film
| name           = Natarang
| image          = Natarang (film).jpg
| alt            =  
| caption        = Theatrical poster for Natarang
| director       = Ravi Jadhav
| producer       =  
| screenplay     = Ravi Jadhav
| story          =  based on        = 
| starring       =   
| music          = Ajay Atul
| cinematography = Mahesh Limaye
| editing        = Jayant Jathar
| studio         =  
| distributor    =  
| released       =   }}
| runtime        = 120 minutes
| country        = India
| language       = Marathi
| budget         = 
| gross          =   (lifetime) 
}}
Natarang, also spelled Natrang ( , an   word for "artist", especially a theatre artist) is a 2010 Indian Marathi film directed by debutant Ravi Jadhav and starring Atul Kulkarni and Sonalee Kulkarni. Veteran music composer duo Ajay Atul composed the original score for the film.  

Based on Natarang, 1978 Marathi novel by Anand Yadav|Dr. Anand Yadav, the film depicts the journey of a young artiste in overcoming hurdles in the form of family, friends, society and to finally realise the unthinkable dream. Set in the 1970s, in the backdrop of a village in Maharashtra, Natarang highlights the emotions related to gender bias and the sacrifices of an artiste for the love of his art.   

==Plot==
Gunvantrao Kagalkar aka Guna (Atul Kulkarni), a poor village labourer, nurtures an obsession for Tamasha - a popular theatrical folk art form of Maharashtra. Unforeseen circumstances lead him to lose his job as labourer and lands him in situation where he sets up a theatre company along with his friend and mentor Pandba (Kishor Kadam). Guna is convinced that his troupe cannot take off unless it has a female dancer. After painstaking search, Pandoba finds Naina (Sonalee Kulkarni) , the daughter of his former lover Yamunabai (Priya Berde), who is willing to dance for the company on the condition that it has a "Nachya", a "pansy" character, a man who acts in an effeminate manner. As no one is willing to take up the role, due to the eunuch taboo, Guna takes it upon himself to play the character. The strongly built Guna takes up the challenge of doing the role due to his passion for the art.

Despite lack of support from his family, Guna works hard to get the role right, and his play becomes successful in a very short while. However, due to their success, the play gets entangled in the power struggle between two local politicians who wish to use its success for political mileage. Meanwhile, Gunas father dies in his village, and his wife and son are subject to harassment from other villagers. Rival political gangs attack Gunas play and torch his theater. Guna is accused of being a eunuch and gang-raped. However, despite being rejected by his family and discouraged by his friends, Guna continues with his stage career, where he is joined only by Naina. It is implied that Guna and Naina marry, and that their play gathers national and international fame. The film ends with an aged Guna, now addressed as Gunvantrao Kagalkar wins the lifetime achievement award at a major awards ceremony.

==Adaptation== Marathi novel named Natarang (novel)|Natarang by  Anand Yadav. Yadav initially expressed qualms about entrusting a debutant director with the film adaptation of his novel, however he was convinced by Ravi Jadhavs study and understanding of the novel. Yadav later expressed satisfaction over the film.   

Yadavs 1978 novel was earlier adapted to a theater production, whose performance however "caused pains to the author."  There were plans for a film adaptation starring Ganpat Patil as Guna and Nilu Phule as Pandoba, but these did not work out.

==Cast==
* Atul Kulkarni as Guna Kagalkar. Atul has described the role as physically as well as emotionally challenging. Kulkarni had to first build up his physique for the role of a laborer, and lose it to portray a panzy.   
* Sonalee Kulkarni  as Nayna Kolhapurkarin, the lead dancer in Gunas troupe. Kulkarni learned dance from the age of five, and her dance performances in the film were well received by audiences world wide.   
* Vibhavari Deshpande as Daarki Kagalkar, Gunas traditional wife who is displeased about his involvement in the Tamāśā.
* Kishor Kadam as Pandoba, Gunas friend and mentor. Marathi legend Nilu Phule was considered for the role of Pandoba in an earlier version of the film. 
* Priya Berde as Yamunabai, Naynas mother and Pandobas former love interest.
* Amruta Khanvilkar in a special appearance in the Lāvaṇī song Wajle ki Bara

==Release== Multiplex in Andheri, Mumbai on New Years Day 2010. The premiere was attended by luminaries from both the Marathi and the Bollywood film industries.    It was the first time a Marathi film was released in a grand premiere.

It was re-released on popular demand with English subtitles on January 22.   

==Soundtrack==
The original and the background scores were composed by Ajay Atul, based on the song lyrics by Guru Thakur. The script and story of the film demands period compositions and traditional dance numbers as in Lavani and Gavalan. Vijay Chavan received special accolades for his performance on the dholki. 

The music has been described as "soulful, melodious, and rhythmically rural" and as "touching just the right chords"  

{{Infobox album| 
| Name = Natarang
| Type = Soundtrack
| Artist = Ajay Atul 200px
| Released = January 1, 2010
| Recorded =
| Producer = Nikhil Sane Amit Phalke  Meghana Jadhav Feature film soundtrack
| Length =
| Label =
}}

===Tracklist===
{| class="wikitable"
|-  style="background:#ff9; text-align:center;"
! Track # !! Song !! Singer(s)
|-
| 1 || "Natarang Ubha" || Ajay Atul and Chorus
|- Ajay Gogavale
|-
| 3 || "Wajle Ki Bara" || Bela Shende and Chorus
|- Ajay Atul|Ajay Gogavale
|- Ajay Gogavale
|- Ajay Gogavale
|- Ajay Gogavale and Chorus
|-
| 8 || "Apsara Aali" || Bela Shende, Ajay Atul
|}

==Critical reception== MAMI (Mumbai Academy of the Moving Image) film festival, and the Pune International Film Festival. It was the only Indian film to be selected in the "Above the cut" category in MAMI.  It was selected as the opening film in the Asian Film Festival in Kolhapur. 
 Zee Gaurav Awards  ceremony, including best director for Ravindra Jadhav, best music composition for Ajay-Atul and best supporting actor for Kishor Kadam. 

Although the Lāvaṇī dance sequences were well received by the audiences, they were criticized by traditional Lāvaṇī performers and academics as having an item song slant. Critics have especially targeted the "provocative dress style" of Sonalee Kulkarni  and Amruta Khanvilkar, saying that it runs contrary to traditional, conservative Lāvaṇī costume. 

===Box office===
Natarang opened to a successful run state wide and earned Rs. 70 million in the first three weeks of its run.  Overall, Marathi films earned Rs. 200 million in the first quarter of 2010, outperforming Bollywood in both box office collection and critical acclaim.   (accessdate=2010-09-15)  The films Natarang, Mahesh Manjrekars Shikshanachya Aaicha Gho and Paresh Mokashis Harishchandrachi Factory themselves collected more than Rs. 160 million at the box office. 

==Participations in Film Festivals== MAMI - Mumbai Film Festival.
* Goa Film Festival
* Third Eye  - ASEAN Film Festival.
* Participated in PIFF (Pune Film Fest).
* 33rd Göteborg Fest 2010
* Munich Film Festival 2010

==Awards==
* Participated in MAMI (Mumbai Academy of the Moving Image festival) & received awards in "Above The Cut" category.
* Sant Tukaraam Award for the Best Film in PIFF (Pune International Film Fest) Zee Gaurav Award  2010. Zee Gaurav Award  2010. Zee Gaurav Award  2010 . Zee Gaurav Award 2010- Khel Mandla. Zee Gaurav Award 2010- Khel Mandla. Zee Gaurav Award 2010- Wajle Ki Bara. Zee Gaurav Award 2010. Zee Gaurav Award 2010- Apsara Aali.
* Best Music Director- Ajay Atul - V Shantaram awards 2010 
* Best Background Score- Ajay Atul - V Shantaram awards 2010
* Best Lyrics- Guru Thakur - V Shantaram awards 2010 - Khel Mandla
* Best Playback Singer Male- Ajay Gogavale - V Shantaram awards 2010 - Khel Mandla
* Best Playback Singer Female- Bela Shende - V Shantaram awards 2010 - Kashi Mi Jau Mathurechya Bajari
* Best Choreography- Phulwa Khamkar - V Shantaram awards 2010 - Apsara Aali   
* Best Playback Singer Male- Ajay Gogavale - BIG FM awards 2010 - Khel Mandla 
* Best Playback Singer Female- Bela Shende - BIG FM awards 2010 - Apsara Aali 
* Best Lyrics- Guru Thakur - BIG FM awards 2010 - Khel Mandla 
* Best Song Of The Year- Music: Ajay Atul - BIG FM awards 2010 - Apsara Aali 
* Best Music Director- Ajay Atul - BIG FM awards 2010  National Film Awards   
* Atul Kulkarni Nominated As Best Performance By An Actor Asia Pacific Screen Awards 2010-    

==See also== Pheta

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 