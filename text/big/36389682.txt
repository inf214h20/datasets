The Heat (film)
 
 
{{Infobox film
| name           = The Heat
| image          = The Heat poster.jpg
| alt            = Two women wearing sunglasses, one holding a rocket launcher. Image is stylized using only black, red, and white. 
| caption        = Theatrical release poster
| director       = Paul Feig
| producer       = {{Plainlist|
* Peter Chernin
* Jenno Topping}}
| writer         = Katie Dippold
| starring       = {{Plainlist|
* Sandra Bullock
* Melissa McCarthy
* Demián Bichir
* Marlon Wayans
* Michael Rapaport
* Jane Curtin}} Mike Andrews
| cinematography = Robert Yeoman
| editing        = {{Plainlist|
* Brent White
* Jay Deuby}}
| studio         = {{Plainlist| Chernin Entertainment
* TSG Entertainment}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 117 minutes   
| country        = United States
| language       = English
| budget         = $43 million   
| gross          = $229.9 million 
}} buddy cop FBI Special Boston Detective Shannon Mullins, who must take down a mobster. The film was released in the United States on June 28, 2013.

==Plot==
FBI Special Agent Sarah Ashburn (Sandra Bullock) is a very skilled and effective investigator in New York City, but is despised by her fellow agents for her arrogance and condescending attitude. In Boston, she meets Shannon Mullins (Melissa McCarthy), a skilled but foulmouthed and rebellious police officer with the Boston Police Department. Ashburns by-the-book philosophy clashes with Mullins rugged and violent style of police work. Under pressure from Hale (Demián Bichir), her FBI boss, Ashburn reluctantly agrees to team up with Mullins.
 bug on DEA agents Craig (Dan Bakkedahl) and Adam (Taran Killam), who have been working the Larkin case for several months and are worried that their case will be compromised. Ashburn and Mullins discover a surveillance video in the DEA agents van showing Mullins brother, Jason (Michael Rapaport), apparently connected to Larkins organization. Jason was recently released from prison, having been put there by Mullins to keep him off the streets and out of trouble. 

Ashburn convinces Mullins to go to her parents home to ask Jason for information on Larkin. On their arrival at the home, it becomes apparent that Mullins parents (particularly her mother) and three other brothers (two of whom have girlfriends) still resent Mullins for her involvement in Jasons incarceration. Jason, however, does not have any ill feelings toward his sister, and tips her off about the body of a murdered drug dealer by the name of Sal Netalie in an abandoned car. Chemicals on the victims shoes lead Ashburn and Mullins to an abandoned paint factory, where they witness a drug dealer being murdered by Julian Vincent, vicious criminal and second-in-command of Larkins organization. They apprehend Julian but are unable to extract any substantial information regarding Larkins whereabouts, even with Mullins going so far as to play Russian Roulette with Julians testicles.

The pair spend the evening bonding in a bar, where a drunk Ashburn reveals that her foster child past may be partly to blame for her attitude. After a night of raucous drinking and partying, Ashburn wakes up the following morning to discover that, in her drunkenness, she has given her car keys to Wayne, one of the bar patrons. After unsuccessfully pleading for the keys, Ashburn and Mullins watch as the patron starts the car and is killed by a Car bomb|bomb. They discover that Julian has escaped from custody and means to harm Mullins family, so Mullins moves her family into a motel. Jason leaves, intending to join the Larkin organization in an attempt to help Mullins solve the case. Jason gives her a tip about a drug shipment coming into Boston Harbor. Despite Mullins reluctance, Ashburn gets the FBI to take down the shipment. The FBI finds that the ship is actually an innocent pleasure cruise ship. Jason was being tested by Larkin, who shoots Jason for informing the FBI about the supposed drug shipment. Jason escapes death but falls into a coma. A falling out occurs between Mullins and Ashburn, with Mullins vowing to bring her brothers attacker to justice. They then reconcile when they arrest several drug dealers as a way of gaining leads to Larkins whereabouts, including one particular drug dealer who was earlier arrested by Mullins.

Ashburn and Mullins go to equip themselves with assault weapons from Mullins extensive personal arsenal, and infiltrate one of Larkins warehouses. Despite taking out several of Larkins men with a hand grenade, the two officers are captured and bound. Julian is about to torture them with knives when he gets called away by Larkin. Before Julian leaves, he stabs Ashburn in the leg and leaves the knife in the wound. Mullins removes the knife from Ashburns leg and uses it to cut the rope binding her hands. Before she can finish freeing herself and Ashburn, they are discovered by Craig and Adam. Craig begins to untie the two women, but is shot and killed by Adam. Ashburn and Mullins learn that Adam is actually Larkin, who has been working his own case from inside the DEA for several months. Julian returns and Larkin orders him to kill Ashburn and Mullins while he goes to the hospital to kill Jason. After Larkin leaves, Mullins manages to finish freeing herself and Ashburn incapacitates Julian with a head butt. Mullins and Ashburn race to the hospital to save Jason.

Upon their arrival, Mullins rushes to find Jason. Ashburn, hindered by the stab wound in her leg, lags behind, unable to move quickly. Mullins learns that, due to the foul language she and her family exhibit, the doctor moved Jason to another room in the hospital; she finds Jasons room, only to be disarmed by Larkin. He is about to kill Jason when Ashburn, having had to crawl to the room, subdues Larkin by shooting him in the genitals (much to Mullins surprise, as she would never actually done so; scaring Julian earlier was only a way of making him talk). With Larkin captured, Ashburn requests to stay in the FBIs Boston field office, having developed a strong friendship with Mullins. Jason is shown having fully recovered from his coma. The film ends with Mullins receiving a commendation from the Boston Police Department. Her family are present and they cheer Mullins, now having reconciled with her. Ashburn later gets a call from Mullins to look in her year book. Mullins had signed the back of Ashburns yearbook with the words, "Foster kid, now you have a sister", showing the strong friendship that Mullins felt for a previously unpopular Ashburn.

As a surprise, Mullins brings to Ashburn the cat that she had found in Ashburns neighbors house, believing it was hers. Earlier, when Mullins saw a photo of Ashburn with the neighbors cat, Ashburn had lied and said it was her cat which had gone missing in New York. Mullins quickly deduces that the cat is not Ashburns; Ashburn confesses and the cat is boxed to be shipped back, ending the film.

==Cast==
 
 
* Sandra Bullock as Special Agent Sarah Ashburn
* Melissa McCarthy as Detective Shannon Mullins, Boston Police Department
* Demián Bichir as Hale
* Marlon Wayans as Special Agent Levy
* Michael Rapaport as Jason Mullins
* Jane Curtin as Mrs. Mullins
* Spoken Reasons as Rojas DEA
* Taran Killam as Special Agent Adam, DEA Michael McDonald as Julian Vincent Tom Wilson as Captain Woods
* Adam Ray as Hank LeSoire
* Tony Hale as The John
* Kaitlin Olson as Tatiana Krumula
* Joey McIntyre as Peter Mullins Michael B. Tucci as Mr. Mullins
* Bill Burr as Mark Mullins
* Nathan Corddry as Michael Mullins
* Jessica Chaffin as Gina
* Jamie Denbo as Beth
* Andy Buckley as Robin
* Ben Falcone as Blue-collar man
* Steve Bannos as Wayne
* Lance Norris as Scruffy bar patron
* Raw Leiba as Paint factory henchman
* Paul Feig as Doctor
* John Ross Bowie as FBI officer
* Zach Woods as Paramedic
 

==Release==
While originally intended to be released on April 5, 2013, Fox pushed back the release date to June 28, 2013.  The film held its world premiere in New York City on June 23, 2013. 

===Box office===
The Heat earned $39,115,043 in North American markets during its opening weekend.  The film has grossed $159,582,188 in the domestic market and $70,348,583 internationally for a worldwide total of $229,930,771.  The Heat was a box office success (from an estimated $43 million budget) and was also 2013s second-highest-grossing comedy (Were the Millers topped it with $269,994,119).

===Critical response===
The film has received mixed to positive reviews from critics. Review aggregator Rotten Tomatoes gives the film a rating of 65% based on 165 reviews. The sites consensus reads, "The Heat is predictable, but Melissa McCarthy is reliably funny and Sandra Bullock proves a capable foil."  Metacritic gives the film a score of 60 out of 100, based on 37 critics, signifying "mixed or average reviews". 

===Home media===
The Heat was released on   crew. 

==Sequel and spin-off==
Shortly after the films release, director Feig announced that the film will be followed by a sequel.   As of October 2013, Bullock stated that she wont come back for the sequel and the project itself was put on hold.    Instead, the sequel is presumably replaced with a spin-off that will center around Jamie Denbo and Jessica Chaffin who took the roles of Beth and Gina in the first film.   

==Awards and Nominations==
{| class="wikitable sortable"  Award
! Category
! style="background: #FBFC8A;"|Recipient(s) and Nominee(s) Result
|-
! Alliance of Women Film Journalists
| Actress Most in Need of a New Agent
| Melissa McCarthy
|  
|- American Comedy Awards Best Comedy Actress - Film
| Sandra Bullock
|  
|-
| Melissa McCarthy
|  
|- Funniest Motion Picture
|  
|- Teen Choice Awards Choice Summer Movie Star: Female
| Sandra Bullock
|  
|-
| Melissa McCarthy
|  
|-
| Choice Movie Chemistry
| Sandra Bullock Melissa McCarthy
|  
|- Choice Summer Movie Comedy
|  
|-
| Choice Movie: Hissy Fit Melissa McCarthy 
|  
|- Broadcast Film Critics Association Broadcast Film Best Actress in a Comedy
|  
|-
| Sandra Bullock 
|  
|- Broadcast Film Best Comedy Movie
|  
|- Golden Trailer Awards
| Don LaFontaine Award for Best Voice Over
| Twentieth Century Fox 
|  
|-
| Best Comedy TV Spot
| Twentieth Century Fox Open Road Entertainment
|  
|-
! MTV Movie Awards Best Comedic Performance
| Melissa McCarthy
|  
|-
! rowspan="4"|Peoples Choice Awards Favorite Comedic Movie
|  
|- Favorite Comedy Movie Actress
| Melissa McCarthy
|  
|-
| Sandra Bullock
|  
|-
| Favorite Movie Duo
| Sandra Bullock Melissa McCarthy
|  
|-
! St. Louis Gateway Film Critics Association Best Film-Comedy 
|  
|-
! Women Film Critics Circle
| Best Comedic Actress
| Melissa McCarthy
|  
|}

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
* OHehir, Andrew. " ." Salon (magazine)|Salon. June 28, 2013.
*   at The  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 