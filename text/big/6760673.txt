The Lower Depths (1936 film)
{{Infobox film
| name           = The Lower Depths
| image          = Les bas fonds.jpg
| caption        = Theatrical release poster
| director       = Jean Renoir
| producer       = Alexandre Kamenka
| writer         = Screenplay:  
| starring       = Jean Gabin Suzy Prim Louis Jouvet
| music          = Jean Wiener
| studio         = Films Albatros
| distributor    = Gaumont-Franco Film-Aubert
| country        = France
| released       = 11 December 1936 (France) 10 September 1937 (US)
| runtime        = 95 minutes
| language       = French
| budget         =
| cinematography = Fédote Bourgasoff
| editing        = Marguerite Renoir
|}}
 1936 Cinema French Drama drama film play of the same title by Maxim Gorky. A glimpse into the lives of several people living at the bottom of the social heap, focuses on a petty criminal, the woman he loves, and a bankrupt newcomer to the slum where the story is set. The film is an example of the poetic realism. It received the first Louis Delluc Prize in 1937.
 Japanese film version of Gorkys play, The Lower Depths (1957 film)|Donzoko (1957). Both are available on one DVD from the The Criterion Collection.

==Cast==
* Jean Gabin as Wasska Pepel
* Suzy Prim as Vassilissa Kostyleva
* Louis Jouvet as The Baron
* Jany Holt as Nastia
* Vladimir Sokoloff as Kostylev
* Robert Le Vigan as The Alcoholic Actor
* Camille Bert as The Count
* René Génin as Louka
* Paul Temps as Satine
* Robert Ozanne as Jabot de Travers
* Henri Saint-Isle as Kletsch
* Junie Astor as Natascha

==See also==
*Cinema of France
*List of French language films

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 