Singaravelan
{{Infobox film
| name = Singaravelan
| image = Singaravelan.jpg
| caption = 
| director = R. V. Udayakumar
| producer = R. K. Baskar 
| writer = Panchu Arunachalam
| narrator =
| starring = Kamal Haasan Kushboo Jaishankar Goundamani Charle Vadivelu
| music = Ilaiyaraaja
| cinematography = Abdul Rehman
| editing = B. Lenin V. T. Vijayan
| distributor = Pavalar Creations
| studio = Pavalar Creations
| released = 13 April 1992 
| runtime =
| country = India
| language = Tamil
| budget = 
}}
 1992 Kollywood|Indian Tamil comedy film starring Kamal Haasan in the lead role of the protagonist. The film also stars Kushboo Sundar|Khushboo, Goundamani, Mano (singer)|Mano, Vadivelu, Charle|Charlie, Manorama (Tamil actress)|Manorama, Jaishankar, Nizhalgal Ravi among others.

The film revolves around a young man (Kamal Hassan) living in a village with his mother. He gets to know that his mother was estranged from her brothers family owing to her marriage with his father. She tells him that while her brother and his wife met with a fatal car accident soon after, they have a daughter (Khushboo) who since grew up in the city of Chennai. She hands him a photo of a baby Khushboo and asks him to track her down and marry her.

Accordingly, Kamal sets out for the big city and heads for his friend Manos house who is living with his 3 friends - Goundamani, Charlie and Vadivelu. The four help him in his endeavor and the consequent adventures form the rest of the story. The film received mainly positive reviews.

== Plot ==
The film starts with Singaravelan a.k.a. Velan (Kamal Hassan), on his birthday taming a wild bull by just talking to him. He seems to share a special bond with animals as he works in his farm filled with animals. Later that day, his mother, Parvathy (Sumithra), calls him and decides to tell him the truth about his father. Velan, in his hyper imagination, thinks that his father was killed by four goons (as is the case of many Tamil films then) but soon learns that his father did actually die of a brain illness like he had been told before. But, he had died heartbroken that his marriage with Velan’s mother was not accepted by her sister-in-law and they had broken all links. Further, they believe that it was Parvathy’s curse that killed them in a car accident and they get promise from the guardian, that their daughter should not be married to Parvathys son. Velan’s mother reveals that it is Velan’s duty to get married to her brothers daughter Sumathi (Khusboo) to bring the separated family back together.  Armed with only a picture of the girl when she was 4, he leaves for Madras to fulfill his mother’s wishes.

Velan arrives to Manos home to implement his mission. Mano lives with three more roommates: Charlie, Vadivelu and Goundamani. Velan greets them and explains his mission. All of them find the reason weird but still offer to help him.

The next day, they visit a computer centre and generate an image of the teen age Sumathi. He tries to find her in the city but Madras proves to be too big for him. Bored, he and his friends visit the beach to think for a plan. Surprisingly, they find a woman playing tennis to be Sumathi, the girl they search for. Someone hits the ball into the sea and Sumathi, who goes to fetch the ball, and Velan goes to help her. But he accidentally drowns her but manage to pull an unconscious Sumathi out of the water. When she becomes conscious, she blames him for drowning her as she didn’t need any help as she was a trained long distance swimmer. Despite of friends advice to drop the love for Sumathi due to her arrogant nature, Velan proceeds with next step of his mission as a challenge.

Later on, Velan and his friends follow her to a 5-star restaurant. On the stage in the dining hall, a flautist is performing. Once the performance ends, Sumathi gets up and kisses the old flautists hand. Velan too offers his hand to be kissed, wherein he is asked whether he knows anything about music. To prove her wrong, Velan dresses up in a fancy leather suit, adorned with various musical instruments, singing the song Pudhu Cheri Katcheri (புது சேரி கச்சேரி). But Sumathi insults the performance. Velan on his part teases with harmless intention, which also earns Sumathis dislike.  

Velan meanwhile attempts to attract Thaiamma (Manorama), the guardian of Sumathi by visiting her house daily. Thaiamma is attracted by Velans entertaining nature. But she also confirms that he is not the son of Parvathi. Velan lies by saying his mothers name as Mahalakshmi. Though Thaiamma certifies Velan as a good chap, Sumathi continues to hate him. Sumathi’s uncle, Natarajan (Jaishankar) visits Velan to warn him to stop teasing his niece. There he comes to know about his father is an industrialist Kaliannan Gounder (V.K. Ramaswamy) who has disowned him due to his passion for music. Unfortunately, the General manages to meet the Industrialist to verify the Velans family background and finds everything true via the mouth of industrialist. But Velan and his friends has already visited the industrialist and convinced him to lie to the general to help them out.

Then Velan managed to attract Sumathi gradually through a series of events which make Sumathi realizes her femininity. Their love becomes strong when Sumathi hears that Velan is very serious at hospital as he was attacked by some men hired by her uncle as he lied that he was the son of an industrialist. She finds Velan dressed up with full bandages in his body (a drama by Velan who counter attacked the men sent by the uncle and instructs them to convey that the men attacked Velan, to Sumathi as part of the love mission).Velans mother is informed about her sons hospitalization by a person from Madras and immediately rushes to see her son. There she understoods that facts are not true and feels easy. Velan and Sumathis love grows steadily and they decide to get engaged.

On the day of engagement, through Sincere Sivamani (Nizhalgal Ravi), the manager at Sumathi’s factory, Sumathi come to know that Velan is, in fact, the son of Paravathy, cancels the engagement and refuses to speak to him. In the mean time, the general finds that Sivamani is a crook who has been using Sumathis premises to print counterfeit bills. Sivamani kills the general and frames Velan. Velan escapes the police and goes on the run. His friends, mother and the industrialist are arrested as accomplishes. Sivamani, decides to kill Velan and marry Sumathi for her wealth and tricks Thaiamma to accept his proposal to get married to Sumathi. He also uses one of his men to bring Velan in and to kill him, but Velan beats up all the bad guys and calls the police.

Together with the police, Sivamani’s ally, his mother and friends he goes to the wedding hall and proves that Sivamani was the real killer and stops the wedding and gets him arrested. Sumathi realizes the true love of Velan and requests his mother to marry them. Velan and Sumathi get married. The story ends with the happiness of all members re-united at Velans house.

== Cast ==
* Kamal Hassan as Singaravelan a.k.a. Velan Khushboo as Sumathi Sumithra as Parvathi
* Jaishankar as General Nataraj Mano as Mano
* Goundamani as Drums Mani
* Vadivelu as "Mikel Jacksan Thangachi" Subha
* Charle as Flute Ramasamy Manorama as Thaiyamma
* Nizhalgal Ravi as "Sincere" Sivamani
* V. K. Ramasamy (actor)|V. K. Ramaswamy as Kaliannan Kounder
* Ajay Rathnam as thug
* Vijayakumar as Velans father (cameo)
* Malaysia Vasudevan
* Venniradai Moorthy as Doctor

== Reception ==
Termed as a perfect summer entertainer, Singaravelan was a Super-duper hit at the box office.  It was the first movie of any kind to be released in more than two theatres in a single complex in Chennai.  

== Soundtrack ==
The music of the film has been composed by Ilaiyaraaja. 
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Lyrics
|- 1 || Pottu Vaitha Kathal Thitam ||  
|- 2 || Innum Ennai Enna ||  
|- 3 || Vaali
|- 4 || Vaali
|- 5 || Vaali
|- 6 || Thoodhu Selvadharadi || S. Janaki || 02:26 || Ponnadiyan
|- 7 || Vaali
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 