A Girl of Yesterday
{{Infobox film
| name           = A Girl of Yesterday
| image          = A Girl of Yesterday.jpg
| caption        = Theatrical poster 
| director       = Allan Dwan
| writer         = Mary Pickford (scenario)
| story          = Wesley C. MacDermott
| producer       = Daniel Frohman Adolph Zukor
| starring       = Mary Pickford Frances Marion
| distributor    = Famous Players-Lasky Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = Silent English intertitles
}}
 silent comedy film directed by Allan Dwan, and distributed by Paramount Pictures and Famous Players-Lasky. The film starred Mary Pickford (who also wrote the scenario) as an older woman. Before this film, Pickford was mainly cast in "little girl" roles which were popular with the public.  The picture costarred Pickfords younger brother Jack Pickford|Jack, Marshall Neilan, Donald Crisp, and Frances Marion, who later became a prolific screenwriter. Real life aviation pioneer Glenn L. Martin also made a cameo in the film.  

==Cast==
* Mary Pickford - Jane Stuart
* Jack Pickford - John Stuart
* Gertrude Norman - Aunt Angela
* Donald Crisp - A.H. Monroe
* Marshall Neilan - Stanley Hudson
* Frances Marion - Rosanna Danford
* Lillian Langdon - Mrs. A.H. Monroe
* Claire Alexander - Eloise Monroe
* Glenn L. Martin - Pilot

  Promotional still Kenneth Douglas,  aviator Glenn Martin, and Mary Pickford
 

==Preservation status==
The film is now considered a lost film.  

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 