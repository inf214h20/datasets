Constance (1998 film)
{{Infobox Film name = Constance director   = Knud Vesterskov producer   = Lene Børglum Peter Aalbæk Jensen Lars von Trier writer     = Knud Vesterskov starring   = Katja Kean Anaïs Mark Duran Niels Dencker music          = Peter Kyed Peter Peter cinematography = Steen Møller Rasmussen editing        = Rikke Malene Nielsen
|distributor= Team Video Plus (Denmark) Magma (Germany)
|released= 1998 (Denmark) country        = Denmark runtime    = 70 mins language = Danish
|}}
 hardcore pornographic film ever to have been produced by an established mainstream film studio.  

Constance is based on the Puzzy Power Manifesto  developed by Zentropa in 1997, and was the first in a series of pornographic films aimed particularly at a female audience.  The others would be Zentropas Pink Prison (1999) and All About Anna (2005).

==Plot==
A young woman, Constance (Anaïs), arrives at the mansion of the experienced Lola (Katja Kean), where she is initiated into the mysteries of sexuality. The story is told in flashback via a framing device with lyrical diary excerpts and narration read by mainstream actresses Christiane Bjørg Nielsen and Hella Joof. (In the English-language version, narration is by Danish actress Susan Olsen and Helle Fagralid).

==Critical reception==
The film was shown in mainstream cinemas in Europe, and was reviewed by mainstream film critics. Jack Stevenson, Fleshpot: cinemas sexual myth makers & taboo breakers, 2nd edition, Critical Vision, 2000, ISBN 1-900486-12-1  The Stockholm Film Festival arranged a special screening in Stockholm on Valentines Day. 

Constance became a considerable success, and generated considerable hype, especially in Scandinavia.  It was nominated for three AVN Awards: Best Art Direction - Video; Best Music; and Best Videography. The reaction of film critics was "mixed." 

In 2006, it was in part this films claim of mainstream audience viewing behind a process that ended up with porn being legalized in Norway. 

In September 2007, German weekly magazine   is the first to have realised this and produced valuable quality porn films for women." 

== References ==
 

==External links==
* 
*  

 
 
 
 