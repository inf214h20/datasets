Tennessee's Pardner
{{Infobox film
| name           = Tennessees Pardner
| image          = 
| alt            = 
| caption        = 
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Marion Fairfax
| story          = Bret Harte
| starring       = Fannie Ward Jack Dean Charles Clary Jessie Arnold Ronald Bradbury Raymond Hatton
| music          = 
| cinematography = Percy Hilburn 
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western film directed by George Melford and written by Marion Fairfax. The film stars Fannie Ward, Jack Dean, Charles Clary, Jessie Arnold, Ronald Bradbury and Raymond Hatton. The film was released February 6, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Fannie Ward as Tennessee
*Jack Dean as Jack Hunter
*Charles Clary as Tom Romaine
*Jessie Arnold as Kate Kent
*Ronald Bradbury as Bill Kent
*Raymond Hatton as Gewilliker Hay
*James Neill as The Padre

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 