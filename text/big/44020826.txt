Sajani (1940 film)
{{Infobox film
| name           = Sajani
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sudama Productions
| writer         =  
| narrator       =  Dixit 
| music          = Gyan Dutt
| cinematography = 
| editing        = 
| distributor    =
| studio         = Sudama Productions
| released       = 1940
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1940 Hindi social film directed by Sarvottam Badami for Sudama Productions.  Scripted by Zia Sarhadi,    the film had music by Gyan Dutt and starred Prithviraj Kapoor, Sabita Devi, Snehprabha Pradhan, Noor Jehan, Dixit (actor)|Dixit, and Ghory.  Badami left Sagar Movietone where he had made satirical comedies to join his "mentor" Ambalal Patel at Sudama Productions to make "socially relevant film(s)", where Sajani was one of the first.       Snehprabha Pradhan acted in several films produced by Chimanbhai Desai in 1940, including Sajani.

==Cast==
* Prithviraj Kapoor
* Sabita Devi
* Snehprabha Pradhan
* Nurjehan Dixit
* Ghory
* Kesari
* Shakir
* Tarabai

==Music==
The film’s music was composed by Gyan Dutt with lyrics by Pyare Lal Santoshi, Pandit Indra Chandra and Zia Sarhadi. The singers were Brijmala, Gyan Dutt and Snehlata Pradhan. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Kheton Mein Milne Aaye Sajan Aur Sajni
| Brijmala
| P. L. Santoshi
|-
| 2
| Main Deepak Ki Bati Priyatam
| Snehprabha Pradhan
| Santoshi
|-
| 3
| Main Hun Khet Ki Rani Re
|
| Pandit Indra Chandra
|-
| 4
| Meri Guyian Chali Ithlaye
| Snehprabha Pradhan
| Santoshi
|-
| 5
| Mora Piya Bas Kaiaun Desh Ho
|
| Zia Sarhadi
|-
| 6
| Musafir Jaana Tujhko Dur Chala Ja
| Gyan Dutt
| Santoshi
|-
| 7
| Pahuncha De Babuji Ko Ae Yar
| Gyan Dutt
| Santoshi
|}

==References==
 

==External links==
* 

 

 
 
 

 