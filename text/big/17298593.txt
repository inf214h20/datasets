Mission: Impossible – Ghost Protocol
 
{{Infobox film
| name           = Mission: Impossible – Ghost Protocol
| image          = Mission_impossible_ghost_protocol.jpg
| director       = Brad Bird
| producer       = Tom Cruise J. J. Abrams Bryan Burk
| writer         = Josh Appelbaum André Nemec
| based on       =  
| starring       = Tom Cruise Jeremy Renner Simon Pegg Paula Patton Michael Nyqvist Vladimir Mashkov
| music          = Michael Giacchino
| cinematography = Robert Elswit Paul Hirsch TC Productions FilmWorks Stillking Productions
| distributor    = Paramount Pictures
| released       =   
| runtime        = 132 minutes 
| country        = United States
| language       = English
| budget         = $145 million   
| gross          = $694.7 million 
}}
Mission: Impossible – Ghost Protocol (previously titled Mission: Impossible 4) is a 2011 American  , and director  s director) and  s editor,   cameras. The film was released in North America by   (2006) and followed by   (2015).

Upon release, Mission: Impossible – Ghost Protocol became a critical and commercial success, becoming the highest-grossing Mission: Impossible film,  and  the highest-grossing film starring Tom Cruise.   

==Plot==
  IMF agent SVR agent  Anatoly Sidorov arrests Ethan, suspecting him as a key player in the attack.
 black operation nuclear launch-control device; however, he now needs the activation codes from the Budapest courier in order to launch nuclear missiles at the United States.

The exchange between Moreau and Hendricks right-hand man, Wistrom, is due to take place in the Burj Khalifa in Dubai. There, Ethans team members separately convince Moreau and Wistrom that they have made the exchange with one another. However, Moreau identifies Brandt as an agent. While Ethan chases Wistrom—only to realize that Wistrom is actually Hendricks in disguise, escaping with the codes—Jane detains Moreau. Moreau attempts to kill the inexperienced Benji, and Jane kicks her out a window to her death. Brandt accuses Jane of compromising the mission for revenge against Moreau, but Ethan accuses Brandt of keeping secrets from them, as he has demonstrated skills decidedly atypical of a mere analyst. While Ethan seeks more information from Bogdan, Brandt confides to Benji and Jane that he was assigned as security detail to Ethan and his wife Julia while they were on vacation in Croatia. While Brandt was on patrol, Julia was killed by a Serbian hit squad, prompting Ethan to pursue and kill them before he was caught by the Russians and sent to prison.
 virus before sending a signal from a television broadcasting tower to a Russian Delta-class submarine|Delta III-class nuclear submarine in the Pacific to fire at San Francisco. Ethan pursues Hendricks and the launch device while the other team-members attempt to bring the broadcast station back online. Ethan and Hendricks fight over the launch-control device before Hendricks jumps to his death with it to ensure success. Benji kills Wistrom, allowing Brandt to restore power to the station and enabling Ethan to deactivate the missile, while the fatally wounded Hendricks witnesses the failure of his plan as he dies. Sidorov happens upon the scene in time to see what Ethan has done and realizes that the IMF is innocent of bombing the Kremlin.

The team reconvenes weeks later in Seattle with Ethan meeting up with Luther Stickell and accepting a new mission. Brandt refuses at first and confesses to Ethan about being assigned to protect Julia and failing. However, Ethan reveals that both Julias death and the murder of Serbians were actually faked in order to infiltrate the Moscow prison while protecting Julia, a relieved Brandt accepts the mission. Julia is then shown alive and smiles at Ethan from far away.

==Cast==
* Tom Cruise as Ethan Hunt, the IMF teams leader.
* Jeremy Renner as William Brandt, the IMF Secretarys chief analyst and a former IMF field agent.
* Simon Pegg as Benji Dunn, an IMF technical field agent and part of Hunts team.
* Paula Patton as Jane Carter, a member of Hunts team.
* Michael Nyqvist as Kurt Hendricks, also known as "Cobalt", a Swedish nuclear strategist.
* Vladimir Mashkov as Anatoly Sidorov, a Russian intelligence operative following Hunt and his team.
* Samuli Edelmann as Marius Wistrom, Hendricks right-hand man 
* Ivan Shvedoff as Leonid Lisenker, a nuclear code expert who is forced to work with Hendricks.
* Anil Kapoor as Brij Nath, a rich Indian businessman in the media industry.
* Léa Seydoux as Sabine Moreau, a French assassin for hire.
* Josh Holloway as Trevor Hanaway, an IMF agent.
* Pavel Kříž as Marek Stefanski.
* Miraj Grbić as Bogdan, a Russian prisoner freed by Hunt.
* Ilia Volok as the Fog, an arms dealer and Bogdans cousin.
* Tom Wilkinson (uncredited) as the IMF Secretary.
* Ving Rhames (uncredited cameo) as Luther Stickell, Hunts colleague.
* Michelle Monaghan (uncredited cameo) as Julia Meade-Hunt, Hunts wife. 
* Lavell Crawford (cameo) as Julias Bodyguard.
* Mike Dopud as a Kremlin sub-cellar hallway guard.
* Ivo Novák as a Russian agent.
* Brian Caspe as a British News anchor.
* April Stewart as a Swedish translator.
* Nicola Anderson as Julias friend.

==Production==
{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "When we were first looking at the image of Tom climbing the Burj, in the long shots we could not only see the traffic in the reflections when he presses down on the glass... But you actually saw the glass warp slightly because of the pressure of his hand. You would never see that in 35mm. The fact that the screen fills your vision and is super sharp seems more life-like."
|-
| style="text-align: right;" |&nbsp;—Brad Bird describing the advantages of filming in the IMAX format.   
|} The Dark Knight. 
 multiplexes as opposed to grand theaters, and vetoing "first runs" in favor of wider initial releases. 

Principal photography took place from October 2010 to March 2011.    Filming took place in Mumbai, Prague, Moscow, Vancouver, Bangalore, and Dubai.      Tom Cruise performed a sequence where Ethan Hunt scales the outside of the Burj Khalifa tower, which is the worlds tallest building, without the use of a stunt double.   Although Cruise appears to be free solo climbing in the film with the help of special gloves, in reality, he was securely attached to the Burj Khalifa at all times by multiple cables.  Industrial Light & Magic digitally erased the cables in post-production.  Following Cruises example, Patton and Seydoux also chose to forgo the use of stunt doubles for their fight scene at the Burj Khalifa where Carter exacts her revenge upon Moreau for Hanaways death. 

Many of the films interior scenes were shot at Vancouvers Canadian Motion Picture Park, including a key transition scene in a specially equipped IMF train car and the fight between Hunt and Hendricks in a Mumbai automated Multi-storey car park|multi-level parking garage (which was constructed over a six-month period just for the film).   The films climax scene was shot with Indian film actor Anil Kapoor in the Sun Network office in Bangalore.   Also, the films opening Moscow prison escape scenes were shot on location in a real former prison near Prague.  

Bird, having directed several Disney and Pixar films and short films, incorporated the trademark "A113" into the movie on two separate occasions. The first is the design print on Agent Hanaways ring during the flashback sequence, and the second being when Hunt calls in for support and uses the drop callsign, Alpha 1-1-3.

==Soundtrack==
{{Infobox album  
| Name       = Mission: Impossible – Ghost Protocol: Music from the Motion Picture
| Type       = Soundtrack
| Artist     = Michael Giacchino
| Cover      =
| Alt        =
| Released   =  
| Recorded   =
| Genre      = Film score
| Length     =  
| Label      = Varèse Sarabande
| Producer   = Michael Giacchino Monte Carlo  (2011)
| This album  = Ghost Protocol (2011) John Carter  (2012)
| Misc        = {{Extra chronology
| Artist       =   Score
| Last album   =   (2006)
| This album   = Ghost Protocol (2011)
| Next album   =
}}
}}

The films score was composed by Michael Giacchino, his second for the franchise and his third collaboration with Bird following The Incredibles and Ratatouille. The soundtrack was released by Varèse Sarabande on January 10, 2012. 

===Track listing===
{{Track listing
| all_music = Michael Giacchino
| title1 = Give Her My Budapest
| length1 = 1:57
| title2 = Light the Fuse One
| length2 = 2:01
| note2 = Contains Mission: Impossible Theme by Lalo Schifrin
| title3 = Knife to a Gun Fight
| length3 = 3:42
| title4 = In Russia, Phone Dials You
| length4 = 1:40
| note4 = Contains Mission: Impossible Theme and "The Plot" by Lalo Schifrin
| title5 = Kremlin with Anticipation
| length5 = 4:12
| note5 = Contains Mission: Impossible Theme and "The Plot" by Lalo Schifrin
| title6 = From Russia with Love
| length6 = 3:37
| note6 = Contains Mission: Impossible Theme by Lalo Schifrin
| title7 = Ghost Protocol
| length7 = 4:58
| note7 = Contains Mission: Impossible Theme by Lalo Schifrin
| title8 = Railcar Rundown
| length8 = 1:11
| note8 = Contains Mission: Impossible Theme by Lalo Schifrin
| title9 = Hendricks Manifesto
| length9 = 3:17
| note9 = Contains Mission: Impossible Theme by Lalo Schifrin
| title10 = A Man, A Plan, A Code, Dubai
| length10 = 2:44
| note10 = Contains Mission: Impossible Theme by Lalo Schifrin
| title11 = Love the Glove
| length11 = 3:44
| note11 = Contains Mission: Impossible Theme by Lalo Schifrin
| title12 = The Express Elevator
| length12 = 2:31
| note12 = Contains Mission: Impossible Theme by Lalo Schifrin
| title13 = Mission Impersonatable
| length13 = 3:55
| title14 = Moreau Trouble Than Shes Worth
| length14 = 6:44
| title15 = Out for a Run
| length15 = 3:54
| title16 = Eye of the Wistrom
| length16 = 1:05
| title17 = Mood India
| length17 = 4:28
| note17 = Contains Mission: Impossible Theme by Lalo Schifrin
| title18 = Mumbais the Word
| length18 = 7:14
| title19 = Launch Is on Hendricks
| length19 = 2:22
| title20 = Worlds Worst Parking Valet
| length20 = 5:03
| note20 = Contains Mission: Impossible Theme by Lalo Schifrin
| title21 = Putting the Miss in Mission
| length21 = 5:19
| note21 = Contains Mission: Impossible Theme by Lalo Schifrin
| title22 = Mission: Impossible Theme (Out with a Bang Version)
| length22 = 0:53
}}

==Distribution==

===Marketing===
  at the Taj Mahal for the film promotion.]]
In July 2011, a teaser trailer for Ghost Protocol was released illustrating new shots from the film, one of which being Tom Cruise scaling the worlds tallest building, the Burj Khalifa building in Dubai.  Moreover, prior to its release, the studio presented IMAX footage of the film to an invitation-only crowd of opinion makers and journalists at central Londons BFI IMAX theater. One of the many scenes that were included was a chase scene in a Dubai desert sandstorm. 

During November 2011, the Paramount released a Facebook game of the film in order to promote it.  The new game allowed players to choose the roles of IMF agents and assemble teams to embark on a multiplayer  journey. Players were also able to garner tickets to the films U.S. premiere and a hometown screening of the film for 30 friends. 

===Theatrical release===
Following the world premiere in Dubai on December 7, 2011,  the film was released in IMAX and other large-format theaters in the U.S. on December 16, 2011,    with general release on December 21, 2011.

===Home media===
Mission: Impossible – Ghost Protocol was released on Blu-ray Disc, DVD and digital download on April 17, 2012.  The home media releases, however, do not preserve the original  ,   ,  and    will switch between 2.40:1 for regular scenes and 1.78:1 for IMAX scenes.

==Reception==

===Critical response===
Mission: Impossible – Ghost Protocol holds a 93% approval rating on the review aggregator website   assigned the film a score of 73 based on 38 reviews, considered to be "generally favorable reviews". 

Roger Ebert of the Chicago Sun-Times gave the film 3.5 out of 4 stars, saying the film "is a terrific thriller with action sequences that function as a kind of action poetry".  Stephen Whitty of The Star-Ledger wrote, "The eye-candy—from high-tech gadgets to gorgeous people—has only been ratcheted up. And so has the excitement." He also gave the film 3.5 out of 4 stars.  Giving the film 3 out of 4 stars, Wesley Morris of The Boston Globe said, "In its way, the movie has old-Hollywood elegance. The scope and sets are vast, tall, and cavernous, but Bird scales down for spatial intimacy." 

Philippa Hawker of The Sydney Morning Herald gave the film 3 stars out of 5, and said it is "ludicrously improbable, but also quite fun."     Owen Gleiberman of Entertainment Weekly opined that the movie "brims with scenes that are exciting and amazing at the same time; theyre brought off with such casual aplomb that theyre funny, too. ... Ghost Protocol is fast and explosive, but its also a supremely clever sleight-of-hand thriller. Brad Bird, the animation wizard, ... showing an animators miraculously precise use of visual space, has a playful, screw-tightening ingenuity all his own."  Roger Moore of The Charlotte Observer said, "Brad Bird passes his audition for a career as a live-action director. And Ghost Protocol more than makes its bones as an argument for why Tom Cruise should continue in this role as long as his knees, and his nerves, hold up." He gave the film 3 out of 4 stars. 

===Box office=== War of the Worlds from the top spot.   

In limited release at 425 locations in North America, it earned $12.8 million over its opening weekend.  After five days of limited release, on its sixth day, it expanded to 3,448 theaters and reached first place at the box office with $8.92 million.  The film reached the No. 1 spot at the box office in its second and third weekends with $29.6 million and $29.4 million respectively.   Though only 9% of the films screenings were in IMAX theaters, they accounted for 23% of the films box office. 

Outside North America, it debuted to a $69.5 million in 42 markets representing approximately 70% of the marketplace. In the United Arab Emirates, it set an opening-weekend record of $2.4 million (since surpassed by   film outside North America.  It topped the box office outside North America for three consecutive weekends (during December 2011)  and five weekends in total (the other two in 2012).  Its highest-grossing markets after North America are China ($102.5 million),  Japan ($69.7 million) and South Korea ($51.1 million). 

=== Accolades ===
{| class="wikitable" style="width:99%;"
|- Award
! Category
! Recipients and nominees Result
|-
| Alliance of Women Film Journalists  
| Kick Ass Award for Best Female Action Star
| Paula Patton
| 
|- Golden Reel Awards  Best Sound Editing: Sound Effects and Foley in a Feature Film
| Mission: Impossible – Ghost Protocol
| 
|-
| Nickelodeon Kids Choice Awards|Kids Choice Awards
| Favorite Buttkicker
| Tom Cruise
| 
|-
| rowspan="2" |  MTV Movie Awards    Best Fight
| Tom Cruise vs. Michael Nyqvist
| 
|- Best Gut-Wrenching Performance
| Tom Cruise
| 
|- Saturn Awards  Saturn Award Best Action or Adventure Film
| Mission: Impossible – Ghost Protocol
| 
|- Best Director
| Brad Bird
| 
|- Best Actor
| Tom Cruise    
| 
|- Best Supporting Actress
| Paula Patton
| 
|- Best Music
| Michael Giacchino
| 
|- Best Editing Paul Hirsch
| 
|-
| rowspan="3" |  Teen Choice Awards   
|  
| Mission Impossible – Ghost Protocol
| 
|-
|  
| Tom Cruise
| 
|-
|  
| Paula Patton
| 
|- Visual Effects Society Awards
| Outstanding Models in a Feature Motion Picture
| John Goodson, Paul Francis Russell and Victor Schutz
|  
|- World Stunt Awards
| Best Stunt Coordinator and/or 2nd Unit Director
| Pavel Cajzl, Dan Bradley, Russell Solberg, Gregg Smrz and Owen Walstrom
| 
|}

==Sequel==
 
In December 2011, Simon Pegg suggested that he and Tom Cruise are interested in returning for a fifth Mission: Impossible film.  Paramount is also reportedly interested in fast-tracking a fifth film due to the fourth films success.  Bird has stated that he probably would not return to direct a fifth film, but Tom Cruise has been confirmed to return.  It was revealed on 3 August 2013 that Christopher McQuarrie will be the director of Mission Impossible 5.  Principal photography began in February 2014 in London.  Paramount Pictures has set the film for a July 31, 2015 release date. 

==See also==
 
*  , the television series that served as an inspiration for the film series.

== References ==
 

==External links==
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 