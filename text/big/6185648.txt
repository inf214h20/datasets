The Competition (film)
{{Infobox Film
| name           = The Competition
| image          = Poster of the movie The Competition.jpg
| caption        = 
| director       = Joel Oliansky
| producer       = William Sackheim
| eproducer      = Howard Pine
| writer         = Joel Oliansky Joel Oliansky & William Sackheim (story)
| starring       = Richard Dreyfuss Amy Irving Lee Remick Sam Wanamaker
| music          = Lalo Schifrin
| cinematography = Richard H. Kline
| editing        = David Blewitt
| distributor    = Columbia Pictures
| released       = December 3, 1980
| runtime        = 125 min.
| country        = United States
| language       = English language|English}}

The Competition is a 1980 American drama film starring Richard Dreyfuss and Amy Irving, directed by Joel Oliansky. 

==Plot==
Paul Dietrich is an extremely gifted but disillusioned classical pianist running out of time to prove himself. He logically knows it is time to give up his attempts to enter piano competitions and instead accept a salaried position as a music teacher. Paul also needs to help his mother and his seriously ill father, but he decides to travel to San Francisco for an international piano competition. Doing so could cost him his job waiting for him in Chicago; nevertheless, he wants to try his luck for the last time before passing the age limit to compete.

The competition for a financial grant and two years of concert engagements pits the intense and arrogant Paul against a select group of talented artists. He advances to the final round of six, which includes a brash New Yorker named Jerry DiSalvo, who only knows how to play one concerto, Michael Humphries, who rehearses in the nude, Canadian pianist, Mark Landau, who is note-perfect but emotionally moribund, and a meek Russian girl, Tatjana Baronova, whose teacher disrupts the competition by defecting to the United States.

Another contestant, Heidi Joan Schoonover, is a young American who developed a romantic inclination toward Paul after meeting him at a music festival. Heidis esteemed music teacher, Greta Vandemann, advises her to avoid letting personal matters interfere with her concentration. Heidi is also rudely rebuffed by Paul, who also wants to avoid any distraction.

Despite his situation, Paul asks Heidi for a date and pours his heart out to her about his family situation. Just before the competition, she realizes how much winning means to Paul and wants to drop out. Greta, angry, later chastises Paul, blaming him for hurting Heidis chances by exploiting her guilt over competing against him.

Paul finds Heidi and says that he loves her, and persuades her to stay in the competition. Partway through her performance, Heidis piano develops a technical problem forcing her to stop. Rather than fold under pressure, Heidi angrily demands to play a different concerto and performs it magnificently. Heidi wins the competition, and Paul finishes in second place.

Immediately after winning, Heidi is ecstatic because she and Paul had agreed to form a partnership, combining their talents and resources to help one another, no matter who won. To her surprise, Paul is upset to realize that she is a more proficient player, telling her he is unable to accept the partnership, then leaves. However, Paul eventually arrives at the celebration party after the competition, ready to take part in Heidis victory and to be in her life.

==Cast==
Starring
*Richard Dreyfuss - Paul Dietrich
*Amy Irving - Heidi Joan Schoonover
*Lee Remick - Greta Vandemann
*Sam Wanamaker - Andrew Erskine

Co-starring
*Joseph Cali - Jerry DiSalvo
*Ty Henderson - Michael Humphries
*Vicki Kriegler - Tatjana Baronova
*Adam Stern - Mark Landau (see Adam Stern (conductor))
*Bea Silvern - Madame Gorshev
*Gloria Stroock - Mrs. Dietrich
*Philip Sterling - Mr. Dietrich
*Priscilla Pointer - Mrs. Donellan James B. Sikking - Brudenell
*Delia Salvi - Mrs. DiSalvo

==Music== The Los Angeles Philharmonic Orchestra Conducted by Lalo Schifrin

*Alberto Ginastera|Ginastera, Piano Sonata No. 1 Eduardo Delgado, Pianist
*Johannes Brahms|Brahms, Piano Concerto No. 1 Ralph Grierson, Pianist
*Frédéric Chopin|Chopin, Piano Concerto in E minor Lincoln Mayorga, Pianist
*Sergei Prokofiev|Prokofiev, Piano Concerto No. 3 in C Major Daniel Pollack, Pianist
*Ludwig van Beethoven|Beethoven, Piano Concerto No. 5 Chester B. Swiatkowski, Pianist
 Wilbur Jennings Song by Randy Crawford

==Awards nomination==
;1981 Academy Awards
*Nominated: Best Film Editing - David E. Blewitt
*Nominated: Best Music, Original Song: "People Alone" (Music by Lalo Schifrin, Lyrics by Will Jennings)

;1981 Golden Globe Awards
*Nominated, Best Original Score - Motion Picture - Lalo Schifrin

;1st Golden Raspberry Award Worst Actor (Richard Dreyfuss)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 