Outfoxed
 
{{Infobox film name = Outfoxed image = outfoxed_poster.jpg distributor = MoveOn.org Brave New Films released =   runtime = 78 mins budget = United States dollar|US$200,000
}}
 2004 documentary film by filmmaker Robert Greenwald that says the Fox News Channel, and its owner, Rupert Murdoch, use the channel to promote and advocate right-wing views. {{cite web  | last =Blackwelder | first =Rob | title = OutFoxed: Rupert Murdochs War on Journalism | work=NicksFlickPicks | date = 6 October 2004 | url = http://www.nicksflickpicks.com/outfoxed.html bias belies the channels motto of being Fair and balanced|"Fair and Balanced". {{cite web  | last =Hanley | first =Tyler | title = Outfoxed
  | work= Palo Alto Weekly | date =10 September 2004 | url = http://www.paloaltoonline.com/movies/reviews/Outfoxed?review_id=1274 | accessdate = }}  {{cite web  | last =Cracknell | first =Ryan | title = Outfoxed: Rupert Murdoch’s War on Journalism | work= Movie Views| date =1 September 2004 | url = http://movieviews.ca/outfoxed-rupert-murdochs-war-on-journalism
| accessdate = }} 

The documentary had a limited theatrical release,    was distributed in DVD format by the Political action committee MoveOn.org, and was sold online through Internet retailers such as Amazon.com.   MoveOn.org had helped promote the DVD release by taking out a full-page advertisement in The New York Times.   

Following the release of Outfoxed, Greenwald and Brave New Films produced a related series of anti-Fox viral videos, collectively entitled Fox Attacks.   

==Synopsis== conglomerate on freedom of the press. 

Some of Outfoxeds coverage includes:
*Review of Fox Newss coverage during the lead-up to, and the aftermath of, the 2003 invasion of Iraq.   
*Interviews with former Fox News journalists, discussing incidents where Fox News allegedly pressured journalists to slant their reports towards support for the Republican Party.  Bill OReilly allegedly attempt to intimidate guests with whom they disagree, such as author and activist Jeremy Glick.    Republican politicians, particularly those in the George W. Bush administration, than to Democratic Party (United States)|Democrats. 
*Examination of whether Fox News premature result-calling of the 2000 presidential election contributed to George W. Bush officially being elected. 
*Scrutiny of Fox News management, including Murdoch and president Roger Ailes, both conservatives, in allegedly controlling the networks content, and editorial control from Murdoch down allegedly ensuring which stories and issues are covered and the strongly conservative perspective of such coverage. 

Former Fox News journalists appear in the film critiquing the methods and perceived integrity of their former employer.  Jon Du Pre, a former reporter for Fox Newss West Coast bureau, said that he had been suspended by Fox News management because his live shots from the Ronald Reagan Presidential Library on Ronald Reagans birthday — which Du Pre described was like a "holy day" to Fox Newss hierarchy — were not "celebratory enough."  A former Fox News military contributor, Larry C. Johnson, said that he was in high demand to give on-air analysis on the "War on Terrorism", until he called into question on Hannity & Colmes whether or not the United States could fight two wars (in Afghanistan and Iraq) simultaneously, an incident after which Johnson says he was ignored as a potential Fox News contributor. 

==Reviews==
Outfoxed received positive reviews from critics, earning 85% on the Rotten Tomatoes Tomatometer.   
 liberal audience with an already vehement aversion to Fox News partisan coverage." 

Howard Kurtz of The Washington Post praised Greenwalds uncovering of "...a handful of memos from a top Fox executive", which he argued suggested network bias over the war in Iraq and the investigation of the September 11 attacks. Kurtz was critical of how Greenwalds allegations relied on "orders, or attitudes, of an unnamed they...", and was critical of the filmmaker for making "...no effort at fairness or balance himself." {{cite news |first=Howard |last=Kurtz |authorlink=Howard Kurtz |author= |coauthors= |title=Tilting at the Right, Leaning to the Left: Robert Greenwalds Outfoxed Has Its Own Slant on Balance |url=http://www.washingtonpost.com/wp-dyn/articles/A41604-2004Jul10.html
 |work=The Washington Post |pages= |page= |date=2004-07-11  |accessdate= 2007-07-20|language= |quote= }} 

==Fox News and Rupert Murdochs response==

Megan Lehmann wrote in the New York Post, a newspaper owned by Rupert Murdochs News Corporation, that the movie was a "narrowly focused, unapologetically partisan documentary," and that it "is so one-sided, it undermines its own integrity." {{cite news |first=Megan |last=Lehmann |authorlink=Megan Lehmann |author= |coauthors= |title=Fair and Balanced, this Docs Not  |url=http://www.nypost.com/p/entertainment/fair_and_balanced_this_doc_not_EChJjh4XvZXkzW68hoK0PN
 |work=New York Post |pages= |page= |date=2004-08-06  |accessdate= 2009-10-11|language= |quote= }} 

Fox News called the film "illegal copyright infringement" for its use of clips from Fox News Channel programs.   

Fox also said the film misrepresented the employment of four people identified as former Fox News employees.  Fox News said Alexander Kippen and Frank ODonnell had actually been employees of WTTG, the Fox owned-and-operated station in Washington, D.C., and not employees of Fox News Channel. It said that Jon Du Pre, identified as a former anchor in the film, had actually been a reporter, and that his contract had not been renewed because he was "a weak field correspondent and could not do live shots."  It said that Clara Frenk, identified as a former producer in the film, had actually been a "pool booker" who "expressed no concern about the editorial process" while employed there. Fox also pointed out that Frenk had been a volunteer for Bill Clintons 1992 Presidential campaign. 

Fox News challenged any news organization that thought this was a major story to "put out 100 percent of their editorial directions and internal memos   Fox News Channel will publish 100 percent of our editorial directions and internal memos, and let the public decide who is fair." 

==See also==
* Fox News Channel controversies
* List of documentaries
* The medium is the message
* Politico-media complex
* Propaganda model

==References==
 

==External links==
* 
*    - The critical documentary on YouTube.

 
 

 
 
 
 
 
 
 
 
 
 
 