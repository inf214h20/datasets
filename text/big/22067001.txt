Doctor and the Healer
 
{{Infobox film
| name           = Doctor and the Healer
| image          = 
| caption        = 
| director       = Mario Monicelli
| producer       = Guido Giambartolomei
| writer         = Ennio De Concini Luigi Emmanuele Agenore Incrocci Mario Monicelli Furio Scarpelli
| starring       = Vittorio De Sica Marcello Mastroianni
| music          = Nino Rota
| cinematography = Luciano Trasatti
| editing        = Otello Colangeli
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Doctor and the Healer ( ) is a 1957 Italian comedy film directed by Mario Monicelli.   

==Cast==
* Vittorio De Sica - Antonio Locoratolo
* Marcello Mastroianni - Dr. Francesco Marchetti
* Marisa Merlini - Mafalda
* Lorella De Luca - Clamide
* Gabriella Pallotta - Pasqua
* Alberto Sordi - Corrado
* Virgilio Riento - Umberto
* Carlo Taranto - Scaraffone
* Ilaria Occhini - Rosina Riccardo Garrone - Sergeant
* Giorgio Cerioni - Galeazzo Pesenti
* Gino Buzzanca - Il sindaco di Pianetta
* Franco Di Trocchio - Little Vito

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 