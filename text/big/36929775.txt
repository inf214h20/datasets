The City of Silent Men
{{infobox film
| name           = The City of Silent Men
| image          = The City of Silent Men (1921) 1.jpg
| imagesize      = Lois Wilson, Thomas Meighan, George MacQuarrie Tom Forman
| producer       = Adolph Zukor Jesse Lasky
| writer         = Frank Condon (scenario)
| based on       =   Lois Wilson
| music          = Harry Perry
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels; 6,326 feet
| country        = United States Silent (English intertitles)
}} Tom Forman Lois Wilson.  

==Plot==
Based upon a summary in a film publication,    Jim Montogmery (Meighan) escapes from Sing Sing prison and goes west to start a new life under the name Jack Nelson. He becomes superintendent of a large mill and falls in love with the owners daughter Molly (Wilson). He tells her of his past life and she believes that he is innocent, so they are married. Prison officials pardon Old Bill (Everton), who planned Jims escape, as bait in their attempt to recapture Jim. Detective Mike Kearney (MacQuarrie) finally lands his man but Jim places his fingers in the mill machinery to spoil the tell-tale fingerprints. Later Old Bill wins a confession from the crook that actually did the crime for which Jim was sentenced, leading to a pardon for Jim.

==Cast==
*Thomas Meighan - Jim Montogmery Lois Wilson - Molly Bryant
*Kate Bruce - Mrs. Montgomery
*Paul Everton - Old Bill
*George MacQuarrie - Mike Kearney
*Guy Oliver - Mr. Bryant

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 


 