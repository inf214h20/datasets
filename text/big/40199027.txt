Mystery Road
 
{{Infobox film
| name           = Mystery Road
| image          = File:Mysteryroad2013.jpg
| caption        = Theatrical film poster
| director       = Ivan Sen
| producer       = David Jowsey
| writer         = Ivan Sen Jack Thompson Ryan Kwanten Tasma Walton
| music          = Ivan Sen
| cinematography = Ivan Sen
| editing        = Ivan Sen
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}

Mystery Road is a 2013 Australian crime film written and directed by Ivan Sen. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

==Plot==
Near the rural town of Winton, Queensland, a truck driver hears a wild dog growling and finds the body of a teenage girl named Julie Mason inside one of the tunnels. Detective Jay Swan (Aaron Pedersen) is called from the city to investigate the murder. He learns that Julie was a drug addict and discovers she had sex with truck drivers for money. One night, Jay follows his colleagues, Johnno (Hugo Weaving) and Robbo (Robert Mammone), and sees them arrive at a run-down building in the middle of nowhere. When they spot him in the distance, he flees and they flag him down. Johnno asks him if he has ever killed anyone accidentally.
 David Field). Before Jay leaves, he sees a young man (Ryan Kwanten) leaving in a white hunting truck. Jay finds out that teenage girl Nellie Dargon has also gone missing. Upon hearing that Julie frequented the Dusk till Dawn motel to sleep with truckers, Jay sets out to find a man who was there the night of her death.

When he asks the motel owner (Zoe Carides) of any frequent guests who stayed on that night, she tells him about William Smith, who drives a white hunting truck. He tracks the car down and arrives at a house in the middle of nowhere. Upon seeing Smith, Jay realises he is the same man he saw at Sams farm. Smith insults Jay, refuses to speak and states that he works as a kangaroo hunter. He and his biker roommate intimidate Jay. Jay goes back to the police station and searches for Smiths police record, but fails to find one. He finds that Smith is actually Sams son, Pete.

The next day, Jay follows Pete onto a local street. Pete gets out of his car and Johnno enters, unaware of Jay. Jay follows Johnno to a resting place where he meets up with a local criminal named Wayne Silverman (Damian Walshe-Howling). Jay arrives at Waynes house, but Wayne sees him and flees. Jay blocks his way with his car and arrests him. Wayne mentions that he was asked to look after some heroin and he left it in his car. When he left the car, it got stolen. In the middle of the interrogation, Johnno knocks on the door. He tells Jay to leave and reveals that Wayne is his informant.

Later, Jay sees a suspicious orange car and drives up to a hill, where he uses his Winchester rifle to scope the car. A Land Rover appears and the orange cars driver pulls Wayne out and hands him over to the Land Rovers driver. He finds Nellies dead body and receives a call from the local pathologist that the fabric from a car seat was found under Julies nails. Jay searches Julie house and opens the back of the TV, finding several bags of cocaine. Jay arranges a trade in the hill.

Jay arrives at the hill and the two cars show up. A man wearing a hockey mask exits one of the cars and Jay gives him the bag but as Jay stands there a surprise shot from a distance hits him in the arm. He runs back to his car and a shootout ensues in which Jay is wounded again. Jay sees Petes truck in the distance, Pete is attempting to shoot Jay through telescopic sights with a hunting rifle. The masked man tries to outflank Jay but is killed with a surprise shot to the head by Johnno, who is sitting on a rock in the distance supporting Jay with a snipers rifle. The orange car comes towards Jay and Jay shoots the driver and Petes biker roommate who is also in the car. Pete and Johnno exchange long distance shots at each other and we cut back to Jay, who is firing his remaining ammo at the Land Rover which is trying to drive away. He shoots everyone dead, gets his rifle from his car and scopes Johnno, only to find he has been shot by Pete. Jay approaches the masked man and removes his mask, discovering he is actually Robbo. In the meantime Pete has driven off, but Jay sees his hunting truck in the distance below. He shoots two of his tyres out and the two exchange rifle fire, with Jay eventually shooting Pete in the head. Jay searches the Land Rover and finds Sam in the back shot through the neck. He sees scratch marks on the backseat and realises they killed Julie.

At sunset, Jay prepares to leave Winton. He drives past Marys house and sees her and Crystal standing on the footpath. He gets out of the car. They stare at each other and the screen cuts to black.

==Cast==
* Aaron Pedersen as Detective Jay Swan
* Hugo Weaving as Johnno
* Ryan Kwanten as Pete Bailey/William Smith
* Tony Barry as Sarge
* Damian Walshe-Howling as Wayne Silverman
* Tasma Walton as Mary Swan
* Robert Mammone as Robbo
* Bruce Spence as Jim, the local undertaker David Field as Sam Bailey Jack Thompson as Charlie Murray
* Samara Weaving as Peggy Rogers
* Roy Billing as Weapon Shop Owner
* Zoe Carides as Motel Owner Jack Charles as Jays uncle

==Reception== The Sunday Age called it a "deeply satisfying and slow-burning modern-day western set in outback New South Wales, Ivan Sens outstanding film Mystery Road bridges the current divide in Australian cinema with a prominent and precise work." 

Not all reviews were positive though. Alex Doenau of Trespass commented "There simply isnt enough dynamism to justify Sens story. Australian movies have to work harder to secure audiences beyond those who go to them out of a sense of duty or worthiness; Mystery Road simply doesnt go that far." 

The film was also a rare foreign screening at the 2014 Pyongyang International Film Festival. 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (3rd AACTA Awards|3rd)  AACTA Award Best Film David Jowsey
| 
|- AACTA Award Best Direction Ivan Sen
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actress Tasma Walton
| 
|- AACTA Award Best Editing Ivan Sen
| 
|- AACTA Award Best Sound Lawrence Horne
| 
|- Nick Emond
| 
|- Joe Huang
| 
|- Phil Judd
| 
|- Les Fiddess
| 
|- Greg Fitzgerald
| 
|- Asia Pacific Screen Awards Best Actor Aaron Pedersen
| 
|- Australian Film Critics Association Awards Best Film David Jowsey
| 
|- Best Director Ivan Sen
| 
|- Best Actor Aaron Pedersen
| 
|- Best Supporting Actor Hugo Weaving
| 
|- Best Supporting Actress Tasma Walton
| 
|- Best Screenplay Ivan Sen
| 
|- Best Cinematography
| 
|- Film Critics Circle of Australia Awards Best Film David Jowsey
| 
|- Best Director Ivan Sen
| 
|- Best Script
| 
|- Best Actor Aaron Pedersen
| 
|- Best Actress Tasma Walton
| 
|- Best Supporting Actor Tony Barry
| 
|- Hugo Weaving
| 
|- Best Cinematographer Ivan Sen
| 
|- Best Editing
| 
|- Best Music
| 
|- Best Production Design Matthew Putland
| 
|-
|}

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 
 
 
 
 
 