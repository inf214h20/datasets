Naya Daur (1978 film)
{{Infobox film
| name           = Naya Daur
| image          = Naya Daur.jpg
| image size     =
| caption        = Movie Poster
| director       = Mahesh Bhatt
| producer       = Sudesh Gupta
| writer         = Rakesh Kumar (Story) Arjun Dev Rashk (dialogue)
| starring       = Rishi Kapoor Bhavana Bhatt Farida Jalal Danny Denzongpa
| music          = R.D. Burman
| released       =  1 Jan 1978
| country        = India
| language       = Hindi
}} Hindi film produced by Sudesh Gupta and directed by Mahesh Bhatt. The film stars Rishi Kapoor, Bhavana Bhatt, Farida Jalal, Danny Denzongpa, Madan Puri and Sharat Saxena. The assistant director was Moeen Amjad. Music composed by  R.D. Burman, lyrics by Anand Bakshi.

==Cast==
* Rishi Kapoor as Mahesh Chopra
* Bhavana Bhatt as Kiran Mehta
* Danny Denzongpa as Mark
* Farida Jalal as Jenny
* Pinchoo Kapoor as	Dharamdas
* Shashi Kiran as Garage Mechanic
* Shreeram Lagoo as Jennys dad 
* Paintal as Manghu
* Om Prakash as Chopra
* Madan Puri as Mehta
* Purnima as Shanti Chopra
* Ranjeet as Ronnie
* Sharat Saxena as Ronnies goon
* Major Anand 	 
* Leena Das as Cabaret Dancer
* Sadiq Irani 	 	 
* Ghanshyam Rohera	 
* D.K. Sapru as Hotel Manager
* Shivraj as Ronnie
* Raj Tilak as Ronnies goon

===Tracklist===
{| class=wikitable
|-
!Song
!Singer(s)
|- Chalo Kahin Aur Chaltey Hain Kishore Kumar, Asha Bhosle
|- Mujhey Doston Tum Galey Se Laga Loh Mohammad Rafi,Danny Denzongpa, Asha Bhosle
|- Paani Ke Badley Me Peekar Sharab Kishore Kumar, Danny Denzongpa
|- O Paisewalon, Badey Insaanon Kishore Kumar 
|}

 
 
 


 