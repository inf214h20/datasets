Ek Baap Chhe Bete
{{Infobox film
| name           = Ek Baap Chhe Bete
| image          = Ek-Baap-Chhe-Bete-1978-poster.jpg
| image_size     = 
| caption        =  Mehmood
| producer       = 
| writer         =
| narrator       = 
| starring       =Yogeeta Bali
| music          = Rajesh Roshan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1978 Bollywood film directed by Mehmood Ali|Mehmood. The film stars Yogeeta Bali.

==Family Members in the movie==
Mehmood and all his six sons acted together in this movie along with his American wife Tracy Ali 

Masood Ali aka Pucky Ali (Son of Mehmood and Madhu)  
The eldest born played a role in this movie & also Hamare Tumhare which was also the debut movie for Anil Kapoor.

Maqsood Mehmood Ali aka Lucky Ali (Son of Mehmood and Madhu)  
He started his movie career as adult in Yehi Hai Zindagi. Today he has ventured out to be a famous singer songwriter, composer.

Maqdoom Ali aka Macky Ali (Son of Mehmood and Madhu)       Kunwara Baap as he was in real life affected by polio. He too acted in the movie Ek Baap Chhe Bete. He later made a music album enacted by Macky himself in the music video Yaaron sab dua karo and later in Tirchi topiwale, sung by Altaf Raja.

Masoom Ali (Son of Mehmood and Madhu) 
Produced the movie Dushman Duniya Ka. He too had acted in this movie.

Mansoor Ali (Son of Mehmood and Tracy Ali)  
He had acted in this movie.

Manzoor Ali (Son of Mehmood and Tracy Ali)  
Debuted played the lead role of drug addicted youth in the movie Dushman Duniya Ka. He too had earlier acted in this movie.

Tracy Ali (Wife of Mehmood)  
Had played the role of the American wife in this movie.

==Cast==
*Yogeeta Bali   
*Nutan Behl   
*Jaya Bhaduri   
*I. S. Johar   
*Shubha Khote    Mehmood
*Moushmi Chatterji
*Lucky Ali

== References ==
 

==External links==
*  

 
 
 


 