Death of a Dynasty
 
{{Infobox Film 
|  name           = Death of a Dynasty
|  image          = Death of a Dynasty film poster.jpg
|  caption        = Theatrical release poster
|  director       = Damon Dash   
|  producer       = Steven C. Beer Damon Dash Isen Robbins Aimee Schoof
|  writer         = Adam Moreno Kevin Hart Capone Rob Stapleton Rashida Jones Devon Aoki Charlie Murphy Damon Dash
|  music          = Ron Feemster
|  cinematography = David Daniel
|  editing        = Chris Fiore
|  distributor    = TLA Releasing
|  released       = May 7, 2003 (premiere) April 29, 2005 (limited release)
|  runtime        = 92 minutes
|  country        = United States
|  language       = English 
|  budget         =
}}
 Capone and Damon Dash. It also features cameo appearances by musicians, actors and celebrities such as Jay-Z, Mariah Carey, Chloë Sevigny, Carson Daly, and Aaliyah. 

== Release and production ==
It premiered at the Tribeca Film Festival in 2003 and was also shown at the Cannes Film Festival. The film was not released to theaters in the United States until 2005. It was co-produced by Roc-A-Fella Films and distributed by TLA Releasing. 

Damon Dash directed the film and Adam Moreno wrote its screenplay. The film also made a dedication to singer Aaliyah who was engaged to Damon Dash before she passed on August 21, 2001. 

==Cast==
*Ebon Moss-Bachrach as Dave Katz Kevin Hart as Sean Combs|P-Diddy / Cop 1 / Dance Coach / Hyper Rapper / H. Lector
*Capone Lee as Damon Dash
*Rob Stapleton as Jay-Z / Bootlegger 1 / Hot Boy 1 / Dre / A1 / Gay Guy
*Rashida Jones as Layna Hudson
*Devon Aoki as Picasso
*Charlie Murphy as Dick James / Dukey Man / Sock Head
*Damon Dash as Harlem
*Tony T. Roberts as Town Car Driver/Host Loon as Turk
*Stephanie Raye as Monica
*Beanie Sigel as Charles Sandman Patterson/Himself
*Gerald Kelly as Funkmaster Flex /Biggs/ Angry Blackman
*Chloë Sevigny as Sexy Woman No. 1
*Kari Wuhrer as Sexy Woman No. 2
*Duncan Sheik as Well-Dressed Man
*Andrew Kling as Magician
*Ed Lover as Himself
*Doctor Dré as Himself
*Carson Daly as Himself
*Walt Frazier as Himself Rell as Himself
*Jay-Z as Himself
*Camron as Himself
*Mark Ronson as Himself
*M.O.P. as Themselves State Property as Themselves
*Mariah Carey as Herself
*Riddick Bowe as Himself
*Russell Simmons as Himself

== External links ==
*  

 
 
 
 
 
 


 