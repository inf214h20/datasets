Deyyam
{{Infobox film
| name           = Deyyam
| image          = Deyyammovietitle.jpg
| image_size     =
| caption        = Deyyam Movie Title
| director       = Ram Gopal Varma
| producer       = Ram Gopal Varma
| writer         = Ram Gopal Varma
| narrator       = Ganapathi Rao Kommanapalli  (Dialogues) 
| starring       = J. D. Chakravarthy Maheswari Jayasudha Tanikella Bharani Jeeva (Master) Ravi Chandra Padmala
| music          = Viswanatha Satyanarayana
| co-director    = Yogi
| cinematography = Chota K. Naidu
| choreography   = Ahmed Khan
| editing        = Bhanodaya
| studio         =
| distributor    = Varma Corporation
| released       = June 1996
| runtime        =
| country        = India Telugu
}}

Deyyam ( ) is a 1996 Telugu horror film, written, produced and directed by Ram Gopal Varma. 

==Plot==
The plot starts with a man returning home late  night. He  passes through the graveyard; on the way to home and he finds a ghost mistaken for a woman and follows her. He will be killed by the group of ghosts. Scene moves to a happily living couple Murali and Sindhu (Jayasudha), who lives with their 3 years old son Chinni (Master Ravi Chandra Padmala) and Sindhus sister Mahi (Maheswari). They  plan to buy an  estranged new farm house, which is near to graveyard. Sindhus sister Mahi (Maheswari) always roams with her boyfriend Narsing (J. D. Chakravarthy). The problem comes when Sindhus son  starts making imaginary friends in their new home. It is later revealed that his "imaginary friends" are ghosts. Sequence of events happens showing the essence of ghosts in the house. Things become much worsen when Chinni will become prey to the ghosts. Sindhu sees the ghost of Chinni and feels that he is still alive. In the meantime, Narsing once while waiting for Mahi in the night near the graveyard road, mistakes Ghost to mahi. However, he survives from there. Later he comes to know from his friends that a builder built the farm house on the land of graveyard. Narsing understands that Mahis family is in big trouble. He warns Mahi to move away from that house immediately. But Mahi being from an atheist family doesnt cares about his words. The ghosts continues to kill the family members one after the another. At last, Mahi and Narsing tries to escape from the ghosts but when they return home, they find that all the protagonists became ghosts . The final scene shows Mahi and Narsing are about to be surrounded by ghosts and left with no way to escape.

==Track listing==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCC Telugu (Deyyam)
|- bgcolor="#CCCCCC" align="center"
! No.
! Title
! Singer
! Length
! Lyricist
|-
| 1|| "Alli Billi Kurra Cheera" || || || |
|-
| 2|| "Deyyam Pilupu" || || || |
|-
| 3|| "Ee Kulasaala Gulabi" || || || |
|-
| 4|| "Hello Hello O Missu" || || || |
|-
| 5|| "O Jaabili" || || || |
|}

==Trivia==
* Composer Sathyam scored the title track "Debbaku Tha Dongala Mutha" for Dongala Mutha, his second collaboration with Ram Gopal Varma after Deyyam.
* Harris Jayaraj played the keyboard, while Mani Sharma worked as the assistant music director to Sathyam for Deyyam.

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 