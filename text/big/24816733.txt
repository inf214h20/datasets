Touched (1983 film)
 
{{Infobox film
| name           = Touched
| image          =
| image_size     =
| caption        = John Flynn
| writer         = Lyle Kessler
| narrator       =
| starring       = Robert Hays
| music          = Shirley Walker
| cinematography = Fred Murphy
| editing        = Harry Keramidas
| distributor    =
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}} John Flynn.

== Plot ==
Its plot is about two inmates of a mental institution, a man named Daniel and a woman named Jennifer, who are not as ill as the psychiatrists at the institution perceive them to be.  Daniel and a Jennifer seek to escape and make lives for themselves together in the world at large.

== Locations ==
The film features extensive scenes shot on the boardwalk in Wildwood, NJ, including locations on and near Moreys Pier, during the end of the summer season and the beginning of the off-season in 1982.  Other amusement piers, including Hunts Pier, can be seen in the backgrounds of scenes.

== Music ==
In addition to the instrumental score, the film also features a song sung by Laura Branigan.

==External links==
* 

 

 
 
 
 


 