Farewell to Nostradamus
{{Infobox film
| name     = Lupin the Third: Farewell to Nostradamus 
| image          =
| writer         = 
| starring       = Kanichi Kurita   Kiyoshi Kobayashi   Makio Inoue   Goro Naya   Eiko Masuyama 
| director       = Shunya Itō    Takeshi Shirato 
| producer       = 
| distributor    = Toho Company, Ltd.  TMS Entertainment
| released   =  
| runtime        = 100 minutes 
| country  = Japan 
| language = Japanese 
| music          = Yuji Ohno
| budget         = 
}}
 , sometimes also referred to as To Hell With Nostradamus, is a 1995 Japanese anime film. It is the fourth feature film in the Lupin III franchise.  The North American release was done by FUNimation; it was released individually and later made a part of the "Final Haul" box set.

==Plot==
A simple diamond heist leads Lupin into the machinations of a bizarre cult. The cult is based around the prophecies of Nostradamus, and they kidnap Julia, the daughter of Douglas, a wealthy American who is seeking the presidency, along with Lupins diamond. At stake is the lost book of prophecy Douglas holds in the vault at the top of his skyscraper.

Can Lupin rescue Julia, find a way into the vault, and discover the connection between the cult and the kidnapping? And what about his diamond?

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 


 