Vaanmathi
{{Infobox film
| name =Vaanmathi
| image =
| image_size =
| caption =
| director = Agathiyan
| producer = Sivashakthi Pandian
| writer = Agathiyan Swathi
| Deva
| cinematography = Thangar Bachan
| editing = Lancy Mohan
| studio = Sivashakthi Movie Makers
| distributor = Sivashakthi Movie Makers
| released = January 15, 1996
| runtime = 147 mins
| country = India Tamil
| budget =  1.9 crore
}}
 Swathi in the lead roles. This film portrays hatred that slowly transforms to love between two young souls who are from very different financial backgrounds.  The film was released on January 15, 1996 and went on to become a commercial success, running for 175 days at the box office. 

==Plot==
Krishna (Ajith) belongs to a middle-class family, where his father is a womanizer and brought up Krishna as a good for nothing fellow. Krishna is also a youngster who likes to waste time his time, by hanging out with his friends and roaming behind girls. Vaanmathi (Swathi) is the daughter of a rich business woman who is very arrogant and does anything for money. For example she even abandoned her husband when Vaanmathi was a baby for money. Vaanmathi is a girl who grew up to be a bully, by bringing a bunch of friends in her vehicle and always takes a video camera with her. Then later initiates them. Krishna and Vaanmathi play tricks on each other and fight after they met but eventually fall in love, but Vaanmathis mother is not happy with that. She dislikes Krishna as sees him as her enemy number 1.

She tried everything to break them up and she even arranges a marriage between Vaanmathi and son of the Governor of Tamil Nadu. If their love will be united, forms the rest of the story.

==Cast==
*Ajith Kumar as Krishna Swathi as Vaanmathi
*Vadivukkarasi
*Vijay Krishnaraj Pandu
*Dhamu

==Soundtrack==
The music composed by Deva (music director) |Deva. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|- Vaali (poet) Vaali
|-
| 2 || Oru Naalum || P. Unni Krishnan, Anuradha Sriram
|-
| 3 || Poontha Malli || Mano (singer) |Mano, K. S. Chithra
|- Deva
|- Mano
|-
| 6 || Vaikaaraiyil || S. P. Balasubrahmanyam, K. S. Chithra
|}

==Release==
The film became a major success and subsequently set up a future collaboration between the director and actor in Kadhal Kottai. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 