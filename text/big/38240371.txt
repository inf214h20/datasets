Char... The No-Man's Island
{{Infobox film
| name           = Char... The No-Mans Island 
| image          = Char... The No-Mans Island poster.png
| alt            = 
| caption        = Film poster
| director       = Sourav Sarangi
| producer       = 
| writer         = Sourav Sarangi
| starring       = Rubel Mondal Sofikul Sheikh
| music          = 
| cinematography = Sourav Sarangi
| editing        = Sourav Sarangi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = India
| language       = Bengali language|Bengali, Hindi
| budget         = 
| gross          = 
}}
Char... The No-Mans Island is a 2012 documentary film directed by Sourav Sarangi. The documentary is a co-production by India, Japan, Norway, Italy and England. The film was shown in the Berlin Film Festival.      

== Plot ==
The film revolves around Rubel, a young boy who wants to attend school, but whose financial circumstances force him to become a smuggler from India to Bangladesh.  Every day, he has to cross a river that forms the border between the two countries.  He stays at an island named Char which is a no-mans land and is patrolled by the border security force of both countries.   

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 
 