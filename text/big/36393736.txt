From a Roman Balcony
{{Infobox film
 | name = From a Roman Balcony
 | image =  From a Roman Balcony.jpg
 | caption =
 | director = Mauro Bolognini
 | writer =  
 | starring = Jean Sorel
Lea Massari
 | music = Piero Piccioni  
 | cinematography = Aldo Scavarda    
 | editing =Nino Baragli
 | producer =
 | distributor =
 | released = 1960 
 | runtime =
 | awards =
 | country =
 | language = Italian
 | budget =
 }}
From a Roman Balcony (also known as Pickup in Rome and The Crazy Day) is a 1960 drama film directed by Mauro Bolognini.   It is a co-production between Italy (where it was released as La giornata balorda) and France (where it is known as Ça sest passé à Rome).

The film is based on several tales by Alberto Moravia, who collaborated on the screenplay. The Italian theatrical release suffered several censorship problems, including the blocking of screenings, and a criminal complaint against director Bolognini and screenwriters Moravia and Pier Paolo Pasolini. 

The film was released on an Italian Region 2 DVD by A&R Productions in April 2014.

== Cast ==
* Jean Sorel : David 
* Lea Massari : Freya 
* Jeanne Valérie : Marina 
* Rik Battaglia : Carpiti 
* Valeria Ciangottini : Ivana 
* Isabelle Corey : Sabina 
* Paolo Stoppa : Moglie

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 
 