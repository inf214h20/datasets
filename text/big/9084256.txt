Ispiritista: Itay, May Moomoo!
{{Infobox film
| name           =  
| image          = 
| caption        = 
| director       = Tony Y. Reyes 
| producer       =  
| writer         = 
| screenplay     =  
| story          =   
| starring       =  
| music          = Michael Alba
| cinematography = Marissa Floirendo
| editing        = Renewin Alano
| studio         =   
| distributor    = Regal Films
| released       =  
| runtime        = 110 minutes
| country        = Philippines
| language       =  
| budget         = 
| gross          = 
}}
 Philippines comedy film|comedy/fantasy film|fantasy/horror film directed by Tony Y. Reyes, based on a story concept by Reyes, Antonio P. Tuviera, and Nino Tuviera-Rodriguez, with a screenplay by Reyes and R.J. Nuevas, and starring Vic Sotto, Cindy Kurleto, Iza Calzado and BJ Forbes. 

==Synopsis==
Victor (Vic Sotto) started drinking when his wife Linda (Iza Calzado) suddenly died although he keeps it from his son Tom Tom (BJ Forbes).

One day, Victor thinks of a way by which he can make others who are also mourning feel better—and make money in the process. With the help of two cohorts, Wally (Wally Bayola) and George (Jose Manalo), Victor passes himself off as an Spiritism|Ispiritist who can communicate with restless spirits for a price. His son has no idea that his dad is a fake medium and in fact, idolizes his father. He proudly tells his classmates that his father gets rids of ghosts for a living.

Victor is hired by Tom Tom’s school to speak to the ghost of a little girl and ask her to leave the campus. As his dad prepares, a surprised Tom Tom discovers that he can see and talk to Didith (Abigail Arazo), the little girls ghost. She tells Tom Tom what needs to be done to calm her restless soul. Tom Tom obeys her instructions, and she stops haunting the school. Victor takes the credit and accepts the school’s thanks for a job well done.

After this success, Victor quickly finds more ghost-busting work. Impressed by how he handled the lady, Lalaine (Cindy Kurleto) hires Victor to rid her boarding house of a quartet of spirits (Teri Onor, Allan K., Soxy Topacio, Antonio Aquitania) who are scaring her boarders. Victor promptly accepts, believing Lalaine might be a possible wife for him and a mother for his son. However, Lalaine discovered his con not only losing a love interest but also the respect of his son.  Just when Victor is about to apologize for his sins, he realizes that he has suddenly developed the power to see and speak to ghosts. Victor realizes what he must make up for his lies. During the films climax, Victor, Tom Tom, Lalaine, Wally, George, and an absent-minded priest Fr. Ben (Jonee Gamboa) must enter a haunted mansion and get rid of the spirits that inhabit it.

==Cast==
  
* Vic Sotto as Victor Espiritu
* Cindy Kurleto as Lalaine Morales
* Iza Calzado as Linda
* BJ Forbes as Tomtom Espiritu
* Jose Manalo as George
* Robert Arevalo as Señor Segundo
* Keempee de Leon as Ghost
* Allan K. as  Allan
 
* Gladys Guevarra as Salve
* Marissa Delgado as Mrs. Bernabe
* Dick Israel as Brother Jojo
* Redford White as Mang Teroy
* Sugar Mercado as Border
* Wally Bayola as Wally
* Joonee Gamboa as Father Ben
* Teri Onor as Teri
 
* Antonio Aquitania as Bert
* Soxy Topacio as Soxy
* Jacky Woo as Mr. Kho
* Mae Akizuki as Mrs. Kho
* Rudy Meyer as Mr. Bernabe
* Jean Saburit as Corazon
* Angel Sy as Cookie
 

==Recognition==
The film earned 10 million pesos on its first day, a new record for Regal Films and APT Entertainment. 

===Awards and nominations===
The film received a 2006 FAP Awards nomination.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 