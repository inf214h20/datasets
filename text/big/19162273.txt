The Gilded Six Bits
"The Gilded Six-Bits" is a 1933 short story by Zora Neale Hurston, who is considered one of the pre-eminent writers of 20th-century African-American literature and a leading prose writer of the Harlem Renaissance. Hurston was a relative newcomer on the literary scene when this short story was published, but eventually had greater success with her highly acclaimed novel, Their Eyes Were Watching God. "The Gilded Six-Bits" is now published in Hurstons compilation of short stories entitled Spunk (short story collection)|Spunk in which it is now considered one of her best stories.  "The Gilded Six-Bits" is a story full of love, betrayal, and forgiveness. It portrays the life of two happy newlyweds who both test their relationship and their love for one another when a charismatic outsider comes into their community and into their home. The story embodies Hurstons typical writing style in which it focuses on the common African-American lifestyle, represented by regional dialect and metaphors, and is set in her native town Eatonville, FL where it reflects the traditions of the community. "The Gilded Six-Bits" symbolizes the meaning of a true marriage and the truth that lies underneath its meaning.

==Plot Summary== silver dollars through the front door for Missie May to pick up. Like always, she pretends to be mad that he is throwing the money and playfully chases him, then goes through his pockets to find a little present that he has bought her.

That night, with the silver dollars placed next to Missie Mays plate during dinner, Joe tells her that he is going to take her out to a new ice cream parlor opened by a new rich black man in town from Chicago, who goes by the name of Otis D. Slemmons. To many people in the community, Slemmons is seen as a charismatic and wealthy black man. Joe discusses Mr. Slemmons fine clothes, all the while Missie May seems to be unimpressed, suggesting that he might be lying about his wealth and Social status|success, and stated "Aw, he dont look no better in his clothes than you do in yourn. He got a puzzlegut on im and he so chickle-headed, he got a pone behind his neck," which is implying that there is no other man that can be better nor look better than Joe. Joe still believes that Slemmons is still a great man and decides to take Missie May to the ice cream parlor so he can show his beautiful wife off in hopes of meeting Mr. Slemmons.

At the ice cream parlor, Missie May still shows that she is not impressed by the man but shows some keen interest in his wealth. After returning home Joe tells Missie May that although he may not have money like Slemmons, hed rather be broke as long as he has her. Every week, they make their visit to the ice cream parlor. Its not until one night that Joe gets off work early the test of love is finally revealed. Joe arrives home to find Slemmons in the bed alongside Missie May. Missie May pleads and apologizes to Joe by revealing that the only reason she slept with Slemmons was to get money from him. Joe shows anger by attacking Slemmons, but after he leaves he shows no more signs of anger. The next morning, Joe treats the day as if it was just an ordinary day and asks Missie May why she is not eating breakfast. Missie May cannot understand why Joe does not leave her, but he continues to torture her by carrying around the golden coin Slemmons left behind to symbolize the affair.

Days go by and the couple slowly drifts away until Joe comes home complaining of aches and pains. He asks Missie May to give him a massage and that night they end up sleeping together for the first time in a long time. When Joe gets up that morning, he leaves behind Slemmons coin, in his attempt to torture her, by paying her for her services. When Missie May examines the coin, she realizes that it is nothing but a gilded half dollar. She realizes that Slemmons was not a rich man after all, but a liar and a fake. At that point, Missie May feels ashamed and embarrassed by throwing all of the happiness away for something that seemed too good to be true.

A few weeks after this event, Missie May finds out that she is pregnant and Joe is not concerned whether the baby is his or not. Fortunately at the end of the story, the baby is born, and after some convincing by his mother, Joe accepts the baby as his. Joe is truly happy and takes the gilded coin to the candy shop to buy his wife some chocolates. That night when he returns home, he continues the tradition between the two, by tossing the silver half dollars to Missie May to symbolize the happiness that is again within the house, and for the first time in a while, they are finally a happy couple again. 

==Themes, influences, and symbols==
"The Gilded Six-Bits:" The short story was coincidentally published the year the United States went off the gold standard and Franklin D. Roosevelt asked loyal Americans to exchange their gold for silver and paper money. This type of money had significance in the story in which it personifies the character, Otis D. Slemmons as having gold money and symbolizes him being known as a counterfeit.   
 theme by portraying that objects are covered to make them better than they are, which can be seen with the gilded half-dollar. Also, people are seen as two faced, which can be seen by both Otis D. Slemmons and Missie May. Marriage is also a disguised form of prostitution, the flexible use of language makes communication impossible, and the double meaning of words functions as the representation of fraud as seen by the six bits. 

Biblical allusions: In   and some barley, Hosea attempts to buy back his wife. Also, after the night of the affair, at breakfast Joe tells Missie May she cries too much.  He compares her to Lots wife. He states, "Dont look back lak Lot (biblical person)|Lots wife and turn to salt." 

Influences from Hurstons own life: Hurston was married and divorced twice. Her rocky marriage occurred just prior to the writing of "The Gilded Six-Bits", which portrays a marriage full of hatred and infidelity. In the story, Missie Mays infidelity tests the strength of the marriage with Joe, a marriage which ultimately surpasses its weaknesses. The marriage is spared because, despite Hurstons hardships in her own marriages, she saw marriage as an important foundation capable of providing possibilities in life. "The Gilded Six-Bits" was influenced by her educational endeavors in anthropology and her unsuccessful marriage with Herbert Sheen. 

==Film==
The movie of the The Gilded Six-Bits is based on the actual story of a happy couple whose life is disrupted when a fancy hustler played by Wendell Pierce comes to town enticing Missie May played by TKeyah Crystal Keymah with the promise of gold.  Novella Nelson also stars as Mama Banks, Joes (Chad Coleman) cautious mother. The film aired on the Showtime cable network in February 2001 and was directed by Booker T. Mattison.

==References==
 

==External links==
* 

 
 
 
 
 
 