Alina (film)
{{Infobox film
| name           = Alina
| image          = Alina1950.jpg
| image size     =
| caption        = DVD
| director       = Giorgio Pastina
| producer       = Arrigo Atti
| writer         = Carlo Duse, Faldella
| narrator       =
| starring       = Gina Lollobrigida, Amedeo Nazzari
| music          = Franco Casavola
| cinematography = Tonino Delli Colli
| editing        =Giancarlo Cappelli
| distributor    = Acta Film, Filmolimpia, La Quercia
| released       = August 6, 1950
| runtime        = 90 minutes
| country        = Italy Italian
| budget         =
}}
 1950 Italy|Italian drama film directed by Giorgio Pastina. The film stars Gina Lollobrigida as  Alina and Amedeo Nazzari as  Giovanni.

==Cast==
*Gina Lollobrigida as  Alina
*Amedeo Nazzari as  Giovanni
*Doris Dowling as  Marie
*Juan de Landa as Lucien
*Otello Toso as Marco
*Lauro Gazzolo as Alinas Husband
*Camillo Pilotto as Andrea
*Gino Cavalieri as Giulio
*Vittorio André
*Oscar Andriani as A smuggler
*Frank Colson as LAmericano
*Augusto Di Giovanni as Il brigadiere

== External links ==
*  

 
 
 
 
 
 
 

 
 