Adventures of Frank and Jesse James
{{Infobox film
| name = Adventures of Frank and Jesse James
| director =Fred C. Brannon Yakima Canutt
| image	=	Adventures of Frank and Jesse James FilmPoster.jpeg
| producer =Franklin Adreon
| writer =Franklin Adreon Basil Dickey Sol Shor John Crawford Sam Flint
| cinematography =John MacBurnie
| distributor =Republic Pictures
| released       =   {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 106–107
 | chapter = 
 }} 
| runtime         = 13 chapters / 180 minutes (serial) 
| language =
| budget   = $149,985 (negative cost: $149,805) 
}} Republic Serial film serial.

==Cast==
* Clayton Moore as Jesse James AKA John Howard
* Steve Darrell as Frank James AKA Bob Carroll
* Noel Neill as Judy Powell
* George J. Lewis as Rafe Henley John Crawford as Amos Ramsey
* Sam Flint as Paul Thatcher

==Production==
Adventures of Frank and Jesse James was budgeted at $149,985 although the final negative cost was $149,805 (a $180, or 0.1%, under spend).  It was the cheapest Republic serial of 1948, despite having one more chapter than the other two serials. 

It was filmed between 5 April and 26 April 1948.   The serials production number was 1700. 

This was the fourth of only four 13-chapter serials to be released by Republic.  The previous three were released in 1947, the only original serials released in that year. 

==Release==
===Theatrical===
Adventures of Frank and Jesse James official release date is 30 October 1948, although this is actually the date the sixth chapter was made available to film exchanges. 

This was followed by a re-release of Darkest Africa, re-titled as King of Jungleland, instead of a new serial.  The next new serial, Federal Agents vs. Underworld, Inc., followed in 1949. 

The serial was re-released on 16 April 1956 between the similar re-releases of Manhunt of Mystery Island and King of the Rocket Men.  The last original Republic serial release was King of the Carnival in 1955. 

==Stunts== Tom Steele as Jesse James (doubling Clayton Moore)
* Dale Van Sickel as Rafe Henley (doubling George J. Lewis)

==Special effects==
* Lydecker brothers

==Chapter titles==
# Agent of Treachery (20min)
# The Hidden Witness (13min 20s)
# The Lost Tunnel (13min 20s)
# Blades of Death (13min 20s)
# Roaring Wheels (13min 20s)
# Passage to Danger (13min 20s)
# The Secret Code (13min 20s)
# Doomed Cargo (13min 20s)
# The Eyes of the Law (13min 20s)
# The Stolen Body (13min 20s)
# Suspicion/The Death Trap (13min 20s)  - a clipshow|re-cap chapter
# Talk or Die! (13min 20s)
# Unmasked (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 247
 }} 

==See also==
* List of film serials by year
* List of film serials by studio
* Jesse James Rides Again (1947) - Earlier Jesse James Serial
* The James Brothers of Missouri (1949)- Later Jesse James Serial

==References==
 

==External links==
* 
*  

 
{{succession box  Republic Serial Serial 
| before=Dangers of the Canadian Mounted (1948 in film|1948)
| years=Adventures of Frank and Jesse James (1948 in film|1948)
| after=Federal Agents vs. Underworld, Inc (1949 in film|1949)}}
 
 
 

 
 
 
 
 
 
 
 