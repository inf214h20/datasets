CHiPs
 
{{Infobox television
| show_name = CHiPs
| image =  
| runtime = 48 minutes per episode (excluding commercials)
| creator = Rick Rosner Tom Reilly (1982–1983)
| theme = John Parker (season 1) Alan Silvestri (arrangement of Parkers theme, Season 2 to end of series)
| composer = John Parker Mike Post (1.1, 1.5, 1.6) Pete Carpenter (with Post) Robert Drasnin JJ Johnson Nelson Riddle (1.16) Billy May Alan Silvestri George Romanis (2.2) Bruce Broughton (2.6) Luchi de Jesus (season 6)
| company = Rosner Television MGM Television
| country = United States
| network = NBC
| first_aired = September 15, 1977
| last_aired = July 17, 1983
| num_seasons = 6
| num_episodes = 139 (and 1 TV Movie)
| list_episodes = List of CHiPs episodes
}}
 MGM Studios (now owned by Turner Entertainment) that originally aired on NBC from September 15, 1977, to July 17, 1983.  CHiPs followed the lives of two motorcycle police officers of the California Highway Patrol. The  series ran for 139 episodes over six seasons, plus one reunion TV movie from October 27, 1998.

==Synopsis==
CHiPs was a lightweight action crime drama, which included elements of comedy in every episode (several of the first season episodes play as out-and-out comedies).  Over-the-top freeway pileups, which occurred in almost every episode, were a signature of the show.  There was little if any actual violence on CHiPs, and the show can be classified as a dramedy.  The episodes filled a standard hour-long time slot, which at the time required 48 minutes of actual programming.

The show was created by Rick Rosner, and starred Erik Estrada as macho, rambunctious Officer Francis ("Frank") Llewellyn "Ponch" Poncherello and Larry Wilcox as his straitlaced partner, Officer Jonathan "Jon" Baker. With Ponch the more trouble-prone of the pair, and Jon generally the more level-headed one trying to keep him out of trouble with the duos gruff yet fatherly commanding officer Sergeant Joseph Getraer (Robert Pine), the two were Highway Patrolmen of the Central Los Angeles office of the California Highway Patrol (CHP, hence the name CHiPs).

As real-life CHP motor officers rarely ride in pairs, in early episodes this was explained away by placing the trouble-prone Ponch on probationary status, with Jon assigned as his field training officer.  Eventually, by the end of the first season, this subplot faded away (Ponch completed his probation) as audiences were used to seeing the two working as a team.

===A typical CHiPs episode===
  Malibu or vignettes often transpired during the course of "routine" traffic enforcement.

A light-hearted subplot would also be included, such as Harlan trying to hide a stray dog from Getraer at the office.  A more serious theme, such as Ponch trying to keep a kid from his old neighborhood out of a potential life of crime, might also be included.   After a few failed attempts to apprehend the gang that had been menacing L.A.s freeways, the episode would invariably culminate in Ponch and Jon leading a chase of the suspects (often assisted by other members of their division), climaxing with a spectacular series of stunt vehicle crashes.
 jet skiing or skydiving), designed to showcase the pairs glamorous Southern California lifestyle.  Often, Ponch would attempt to impress a woman he had met during the episode with his athletic prowess or disco dancing, only to fail and provide Jon, Getraer, and others with many laughs. As the preliminary end credits would start, the image would freeze multiple times, showing various characters laughing or otherwise enjoying the social scene.

Some of the more outlandish plots included Ponch and Bobby helping a girl who believed that she was being targeted by UFOs and them racing against time to defuse a battery about to explode on an intelligent experimental police robot.

Officers rarely drew their service weapons on the show. Officer Barry Baricza was the only officer to ever draw a gun (twice he drew his Colt Python and once he unracked his Ithaca 37 riot gun).

==Production==
According to a 1998 TV Guide article, show creator Rick Rosner was a reserve deputy with the Los Angeles County Sheriffs Department.  During a coffee break on an evening patrol shift in the mid-1970s he saw two young CHP officers on motorcycles which gave him the idea for this series.  He later created 240-Robert, which seemed like a hybrid of CHiPs and Emergency!.

 
 Marine artilleryman.
 Kawasaki Police Special with the series, in fact they rode the C-Series Kawasaki, which had an oval windshield rather than the later models fiberglass fairing.
 Highway 2) Interstate 210) Highway 118) in Sylmar, California were used. For the racing scenes in the episode "Drive, Lady, Drive" they used the Riverside International Raceway in Riverside, California.

Although doubles were used for far-off shots and various stunt or action sequences, Wilcox and Estrada did a great deal of their own motorcycle riding, and performed many smaller stunts themselves. Although Wilcox emerged relatively injury-free, Estrada suffered various injuries several times throughout the run of the series. In several early first season episodes, a huge bruise or scar can be seen on his arm after he was flung from one of the motorcycles and skidded along the ground. But his worst accident came when he was seriously injured in a motorcycle accident while filming a season three episode in August 1979, fracturing several ribs and breaking both wrists.  The accident and Estradas subsequent hospitalization was incorporated into the series storyline.

Prior to being cast in CHiPs Estrada had no experience with motorcycles, so he underwent an intensive eight-week course, learning how to ride. In 2007 it was revealed that he did not hold a motorcycle license at the time CHiPs was in production, and only qualified for a license after three attempts, while preparing for an appearance on the reality television show Back to the Grind.

Estrada and Wilcox never drew their firearms over the course of the series. (This did occur in the made-for-TV reunion movie CHiPs 99.) The only character on the series depicted as drawing his firearm was Baricza (Brodie Greer), and he did so three times. The first was his radio cars Ithaca 37 shotgun in season 1s episode "Rainy Day",  where the CHiPs conduct a felony traffic stop of a motorhome-based casino.  The second was in season 2s premiere, "Peaks and Valleys", against two hillbillies armed with a Tommy-gun and a double-barrel shotgun who had ambushed his unattended patrol car for fun. Here the action was only implied, with his hand motion just below camera range. The last was in season 4s "Karate", in which a karate-trained car burglar (Danny Bonaduce) attacked him with a Bō, but Baricza drew his gun to stop Bonaduce.

NBC aired reruns of this series on its 1982 daytime schedule from April to September.

During the original run of the series, syndicated reruns of older episodes were retitled CHiPs Patrol to avoid confusion.  Later syndicated reruns after the show went out of production reverted to the original title.

==Cast of characters==
 
* Larry Wilcox as Officer Jonathan A. Baker (1977–1982) / 7-Mary-3
* Erik Estrada as Officer Francis (Frank) Llewelyn "Ponch" Poncherello / 7-Mary-4 (15-Mary-6 in the final season)
* Robert Pine as Sergeant Joseph (Joe) Getraer / S-4
* Lew Saunders as Officer Gene Fritz (1977–1979) / 5-David-5 (7-David in some episodes)
* Brodie Greer as Officer Barry "Bear" Baricza (1977–1982) / 7-Adam (7-David in one episode)
* Paul Linke as Officer Arthur (Artie) "Grossie" Grossman / 7-Mary-5
* Lou Wagner as Harlan Arliss, Automobile/Motorcycle Mechanic, CHP (1978–1983)
* Brianne Leary as Officer Sindy Cahill (1978–1979) / 7-Charles
* Randi Oakes as Officer Bonnie Clark (1979–1982) / 7-Charles
* Michael Dorn as Officer Jebediah Turner (1979–1982) / 7-David Tom Reilly as Officer Bobby "Hot Dog" Nelson (1982–1983) / 15-Mary-7
* Tina Gayle as Officer Kathy Linahan (1982–1983) / 7-Mary-10
* Bruce Penhall as Cadet/Officer Bruce Nelson (1982–1983) / 15-Mary-8 Clarence Gilyard, Jr. as Officer Benjamin Webster (1982–1983)
* Bruce Jenner as Officer Steve McLeish (1981–1982)

===Cast changes=== strike over Tom Reilly (Officer Bobby Nelson).
 1981 and 1982 Speedway World Championships, was also introduced as cadet–probationary officer Bruce Nelson, Bobbys younger brother in 1982–83. The season 6 episode "Speedway Fever" (aired November 7, 1982) centered on Penhalls character Nelson winning the 1982 Speedway World Final at the Los Angeles Memorial Coliseum, with scenes filmed in the pits during the meeting. The episode also used television coverage of the final (with dubbed commentary). Penhall later admitted that having a bodyguard and having to have makeup done in the pits in full view of his competitors at the World Final only added to the pressure he was under both as a rider and a rookie actor and that it felt weird having to "buddy up to Ponch" in front of the other riders while the World Final was taking place. In order to become a full-time member of the CHiPs cast, Penhall had officially announced his retirement from speedway racing on the podium of the 1982 World Final.
 LAPD for possession of controlled substances during a traffic stop. As a result, Bobby was featured much less prominently in later episodes of the season, with Bruce taking his place for most of the remaining episodes.

==Broadcast history==
(all times Eastern/Pacific Time; subtract one hour for Central/Mountain Time)
* September, 1977 – March, 1978: NBC Thursday, 8-9PM
* April, 1978: NBC Saturday, 8-9PM
* May – August, 1978: NBC Thursday, 8-9PM
* September, 1978 – March, 1980: NBC Saturday, 8-9PM
* March, 1980 – March, 1983: NBC Sunday, 8-9PM
* April – May, 1983: NBC Sunday, 7-8PM
* May – July, 1983: NBC Sunday, 8-9PM

* September, 2014 – present: reruns on MeTV, Monday-Friday, 6PM Eastern/Pacific, 5PM Central/Mountain.

===International===
In the United Kingdom, the series was broadcast by  , Knight Rider (1982 TV series)|Knight Rider, Magnum, P.I.|Magnum, P.I. and Whiz Kids (TV series)|Whiz Kids.
 STV and Tyne Tees; HTV and Yorkshire completed TVS and TSW finally finished series six in 1987 after starting in 1985. A few companies repeated the series in 1987.

The entire series was shown in New Zealand on TVNZ in the 1980s.

==Home media==
Warner Home Video has released the first three seasons of CHiPs on DVD in Regions 1, 2 & 4.  The third season DVD was released in Region 1 on March 3, 2015. 

Seasons 1 and 2 are also available for purchase at the online iTunes Store.  

{| class="wikitable"
|- DVD Name Ep # Release dates
|-
! Region 1 !! Region 2 (UK) !! Region 4
|- 22 || June 5, 2007 || August 20, 2007 || September 6, 2007
|- 23 || June 3, 2008 || September 22, 2008 || September 3, 2008
|- 24 || March 3, 2015 || - || -
|}

==Merchandise== Mego in the late 1970s. Due to the materials used to construct the figures, many of them have discolored (typically turning green) or started to decompose over the years, making good conditioned examples quite hard to find on the collectors market.  There was also a series of six die-cast model vehicles produced by Imperial Toys.
 annuals were produced by World International Publishing Ltd, containing stories, photos, puzzles and features on the stars. There are four annuals in total, one each for 1980–83.  

In 2006, a limited edition soundtrack was released on CD by Turner Classic Movies music division via Film Score Monthly, featuring the original recordings of the main theme by John Parker (Parkers theme replaced an unused composition by Mike Post and Pete Carpenter, who scored the pilot) and in-episode musical scores from many episodes of the second season, as composed and conducted by Alan Silvestri, the series primary (and from seasons three to five sole) composer until the final season. Silvestri also arranged the theme as heard from season two onwards, and it is this version that is heard here – the soundtrack album also includes the "Trick or Treat" score composed and conducted by Bruce Broughton, his only work for the series.  In 2008, music from the third season was released; an album of music from the fourth season followed in 2010.

==In popular culture==
In the Galactica 1980 episode "The Super Scouts", a California Highway Patrol motor officer references CHiPs by lamenting to his partner, "How come this never happens to those two guys on TV?" when two Colonial warriors escape the CHP duo by using flying motorcycles. 

Erik Estrada portrayed the character of Ponch on the first-season Family Guy episode, "I Never Met the Dead Man". 

The alternative rock band Seven Mary Three gets its name from Jons call sign. 

The 2014   features a parody TV series titled CHoPs, with Estrada providing the voice of helicopter officer Nick "Loopn" Lopez. 

==CHiPs 99==
{{Infobox film
| name = CHiPs 99
| caption = CHiPs 99 Movie Poster
| image = CHiPs 99 FilmPoster.jpeg
| director = Jon Cassar
| producer = Erik Estrada Chris Morgan Rick Rosner Larry Wilcox
| writer = Rick Rosner Morgan Gendel David Ramsey Brodie Greer Bruce Penhall Paul Linke
| music = Stacy Widelitz
| cinematography = David Geddes
| editing = Ron Spang Turner Films, Inc.
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget =
}}
 Captain and CHP Commissioner. Other original cast members were Officer Frank Poncherello returning from a 15-year hiatus with the CHP and Officer Barry Baricza.

==Revivals==
In 2005, a theatrical release motion picture version of the show was announced, starring Wilmer Valderrama as Ponch,  though as of 2013 this production was still "stalled".  Erik Estrada and Larry Wilcox were rumored to make cameo appearances. In a 2002 episode of MADtv, Valderrama and fellow That 70s Show cast member Danny Masterson were featured in two parodies of CHiPs, which featured the two actors as Ponch and Baker respectively. Mila Kunis also appeared in the second sketch. In a 2002 episode of That 70s Show, Valderramas character, Fez, was seen in the "most likely" section of the yearbook as "most likely to appear as Ponch in a musical version of CHiPs".

On September 2, 2014, Warner Bros. announced a film adaptation of the show, which Andrew Panay would be producing along with Dax Shepard.  Shepard will also write, direct and star in the film as Officer Jon Baker; Michael Peña will play Frank “Ponch” Poncherello. 

==References==
 

==External links==
*  
*  
*  

 

{{sequence 1980
|list=CHiPs Super Bowl 1981
|next=60 1982
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 