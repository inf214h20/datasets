Sopping Wet Married Teacher: Doing It in Uniform
{{Infobox film
| name = Sopping Wet Married Teacher: Doing It in Uniform
| image = Despite All That - DVD.jpg
| caption = Sopping Wet Married Teacher: Doing It in Uniform (1999) in DVD release as Despite All That
| director = Shinji Imaoka 
| producer = 
| writer = Shinji Imaoka
| narrator = 
| starring = Mitsuyo Suwa Atsuko Suzuki
| music = Gaō
| cinematography = Kazuhiro Suzuki
| editing = Shōji Sakai
| distributor = Kokuei Shintōhō
| released =  
| runtime = 62 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
  aka   is a 1999 Japanese pink film written and directed by Shinji Imaoka. It was given Honorable Mention at the Pink Grand Prix ceremony. 

==Synopsis==
Naoko, female art teacher suffering from bipolar disorder spends her time at home compulsively chopping cabbage. She occasionally then begins cutting herself, causing her husband, Yoshio, to intervene. Yoshio attempts to help Naoko by dressing as a woman, "Yoshiko", in whom Naoko can confide. Yoshio has a low sex drive, and hopes to stimulate his interest by having Naoko dress as a school girl. When so attired, shopping for appropriately adolescent socks, Naoko meets Kimiko, one of her female students. Kimikos parents have abandoned her, and she stays with a strange man named Tokio. Naoko suggests that Kimiko come to live with her. At their home, Kimiko proceeds to seduce both Naoko and her husband. During a sex session with Yoshio, she binds him and paints obscenities onto his body.       

==Cast==
* Mitsuyo Suwa ( ) as Naoko  
* Atsuko Suzuki ( ) as Kimiko
* Takeshi Itō ( ) as Yoshio
* Mikio Satō ( ) as Tokio

==Critical appraisal==
In his Behind the Pink Curtain: The Complete History of Japanese Sex Cinema, Jasper Sharp writes that Imaokas placing a mentally-unbalanced woman at the center of Sopping Wet Married Teacher: Doing It in Uniform is a brave thing for a pink film director to do. Imaokas film, according to Sharp, plays like a softcore version of John Cassavetes A Woman Under the Influence (1974) with a dark sense of humor. 

The Asian Extreme Cinema website Snowblood Apple notes that the low budget of Sopping Wet Married Teacher: Doing It in Uniform shows through in the film, finding fault with technical aspects as well as the acting. The reviewer finds the characters unappealing and the actors unattractive, with the character of Kimiko is judged to be the most interesting. Judging the film, "On the surface... yet another skinflick, albeit a rather untitillating and downbeat one, the cast being profoundly unattractive and the sex scenes mostly shot in low candlelight. Yet Imaoka is saying something about power and, perhaps, its a morality tale..." the review concludes, "While it wont be on the top of anyones must-see list, if youve got an hour spare and a taste for a slightly thoughtful pink film, keep Despite All That in mind." 

The French website Cinetrange also mentions Sopping Wet Married Teacher: Doing It in Uniforms obvious low budget, reflected in the minimalistic technical aspects such as camera movement. This review comments that Imaoka shows us how the Naokos marriage is influenced by the introduction of the sexually-predatory female student. " e see in the end that the deconstruction of the long-term relationship only leads to a stronger relationship between Naoko and hers husband. So, despite all that..." Of the erotic content, the review mentions, "lets remember a pinku eiga is not only about social issues. We also get some soft sex scenes. Dont expect many graphics things but there are at least one or two scenes that are pretty hot and suggestive." 

At the UK website unrated.co, Carl T. Ford writes that Imaoka uses the low budget of the film to his advantage by creating a documentary-influenced film which intentionally works against creating any eroticism in the sex scenes. He writes that Imaoka "isnt interested in appealing to an audience seeking sexual thrills, but instead offers an intelligent and subversive look at sexual politics." According to Ford, Imaoka skillfully uses props, such as the cabbage and the knife, to reflect the inner states of mind of his characters, and he considers the acting to be above-average for a pink film. He concludes, "the film maintains audience interest for its innovative and reflective insights into relationships and their power balance patterns, and remains a thought-provoking and somewhat artistic entry in a genre too often given over to bondage, bums, and breasts with little brains at the helm." 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 
 

 
 
 
 
 