Agni Sakshi (1982 film)
{{Infobox film
| name           = Agni Sakshi
| image          = 
| image_size     = 
| caption        = 
| director       = K. Balachander 
| producer       = Rajam Balachander
| writer         = K. Balachander
| starring       = Sivakumar Saritha
| music          = M. S. Viswanathan
| cinematography = B. S. Lokanath
| editing        = N. R. Kittu
| studio         =
| distributor    = Kavithalayaa Productions
| released       = 14 November 1982
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Agni Sakshi is a Tamil language film directed by K. Balachander, starring Sivakumar and Saritha in the lead roles. Rajinikanth and Kamal Haasan play a short cameo as themselves. Saritha was portrayed as a Schizophrenia patient in this film. The movie was a box office failure. However, Saritha won Tamil Nadu State Film Award for Best Actress.

==Cast==
* Sivakumar
* Saritha

===Friendly appearances===
* Kamal Haasan
* Rajinikanth Seema

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|- Vaali (poet) Vaali
|-
| 2 || Aarambam Adhikarathin || M. S. Viswanathan
|-
| 3 || Adiye Kannamma || P. Susheela
|-
| 4 || Kana Kaanum || S. P. Balasubrahmanyam, Saritha
|-
| 5 || Unnai Enakku || S. P. Balasubrahmanyam
|-
| 6 || Vanakkam Mudhal || S. P. Balasubrahmanyam, P. Susheela
|}

==References==
 

==External links==
* 		

 
 
 

 
 
 
 
 
 


 