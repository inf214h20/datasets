Oh Sailor Behave
{{Infobox film name = Oh Sailor Behave image = Oh_Sailor_Behave_1930_Poster.jpg producer = director = Archie Mayo writer = Joseph Jackson Sid Silvers from the play by Elmer Rice starring = Charles King Ole Olsen Chic Johnson Vivien Oakland Lotti Loder music = Joseph Burke David Mendoza cinematography = Devereaux Jennings editing = William Holmes distributor = Warner Bros. released =   runtime = 70 minutes language = English
|country= United States
}}
Oh Sailor Behave (1930 in film|1930) is a musical comedy produced and released by Warner Brothers, and based on the play See Naples and Die, written by Elmer Rice.  The film was originally intended to be entirely in Technicolor and was advertised as such in movie trade journals. Due to the backlash against musicals, it was apparently released in black-and-white only.

==Plot== Charles King) is sent to Venice to interview a Romanian general, who is played by Noah Beery. While in Venice Charlie falls for a young heiress named Nanette Dodge (Irene Delroy). When Charlie is unable to get an interview with the Romanian general, a local siren named Kunegundi (Vivien Oakland), who is the generals favorite helps him. Meanwhile, Nanette learns that her sister is being blackmailed by Prince Kasloff of Russia (Lowell Sherman), to whom she wrote some incriminating letters. Nanette attempts to vamp the Prince in order to obtain the love letters. The Prince, however, tricks her and demands that Nanette marry him if she wants to save her sister. After being repeatedly rebuked by Nanette, the prince hires the Romanian general (Noah Beery) to kidnap her and force her into  marriage. Charlie, thinking she has eloped, consoles himself with Kunegundi (Vivien Oakland) and almost marries her until he realizes the truth about Nanette and that she has been kidnapped by the Prince. Charlie sets out to rescue her and when the Prince shows up disguised as the general he shoots Prince Kasloff. Charlie and Nanette are happily reunited.  
 Ole Olsen and Chic Johnson provide comic relief that is completely unrelated to the main story. They play the part of two American sailors stationed in Naples who attempt to find a wooden-legged thief who has robbed the navy storehouse in Venice. Louisa, a local siren (played by Lotti Loder) leads them on and embroils them in trouble.

==Music==
*"When Love Comes In The Moonlight"
*"Leave A Little Smile"
*"Highway to Heaven"
*"The Laughing Song"
*"Tell Us Which One Do You Love"
 
==Production background== The March of Time (1930). 
*This was to be Charles Kings last musical movie. He went back to the Broadway stage, since movie audiences had grown tired of musicals, and never returned to the screen.
*Due to the public apathy towards musicals, Warner Bros. did not debut this film in the usual prestigious movie theaters. The film was immediately  placed in general release with no fanfare.
*Comedians Olsen and Johnson were added to the film because of the growing public apathy towards serious stage actors such as King and Delroy. The movie was marketed as a comedy film with these comics billed as "Americas funniest clowns."

== Preservation ==
The version of the film released in the United States, late in 1930, survives intact.  A print is at the Museum of Modern Art, and is in the Turner Classic Movies film library as well as the Library of Congress.  The complete soundtrack also survives on Vitaphone disks. The film was released on dvd through the Warner Archive Collection in 2014.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 