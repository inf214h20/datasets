Escobar: Paradise Lost
{{Infobox film
| name           = Escobar: Paradise Lost
| image          = Paradise_Lost_theatrical_release_poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andrea Di Stefano
| producer       = Dimitri Rassam 
| writer         = Andrea Di Stefano
| starring       = {{Plainlist|
* Benicio Del Toro
* Josh Hutcherson
* Claudia Traisac
* Brady Corbet
* Carlos Bardem
* Ana Girardot
}}
| music          = Max Richter
| cinematography = Luis David Sansans
| editing        = {{Plainlist| David Brenner
* Maryline Monthieux
}}
| studio         = {{Plainlist|
* Chapter 2
* Nexus Factory
* Pathé
* Roxbury Pictures uFilm
}}
| distributor    = The Weinstein Company|RADiUS-TWC
| released       =  
| runtime        = 120 minutes 
| country        = {{Plainlist|
* France
* Spain
* United States
}}
| language       = {{Plainlist|
* English
* Spanish
}}
| budget         = 
| gross          = $3.6 million   
}}
 romantic thriller film, written and directed by Andrea Di Stefano.   It is the directorial debut of Di Stefano. The film chronicles the life of a surfer, who falls in love while visiting his brother in Colombia and finds out that the girls uncle is Colombian drug lord Pablo Escobar. 

The Weinstein Company|RADiUS-TWC acquired the North American distribution rights of the film in February 2014  The film premiered at the 2014 Toronto International Film Festival on September 11, 2014.     and will theatrically release the film in the United States in 2015.   

==Plot==
Opening in the summer of 1991, young Canadian surfer Nick is called into a cartels hideout and tasked with committing a murder on the drug lords behalf. Speeding off on his mission along the dark road and hardly able to keep his breath, Nick is stuck in a conundrum that only becomes clear as it flashes back to 1983. Arriving on the Colombian coast to run a surf camp with his eager brother (Corbet), Nick meets the smarmy Maria (Traisac), and quickly falls for her. The sunny beaches provide a notable visual contrast to the murkier scenes that follow, as Nick gradually realizes the extent of Escobars power. At social gatherings, Escobars domineering personality leaves Nick in a confused state about his priorities.   

==Cast==
* Josh Hutcherson as Nick Brady
* Benicio del Toro as Pablo Escobar
* Brady Corbet as Dylan Brady
* Claudia Traisac as Maria
* Ana Girardot as Anne
* Carlos Bardem as Drago
*Aaron Zebede as Pepito Torres

==Production==

===Pre-production===
Of the storyline, Di Stefano claimed "the idea came from three sentences   heard from a police officer about a real-life young Italian fellow who went to Colombia to meet his brother, somehow became close to the Escobar family, and then got in trouble." 

Hutcherson served as an executive producer for the film, alongside Andrea Di Stefano, assisting with casting and blocking shots. 

===Casting===
On December 17, 2012, it was rumoured that  Josh Hutcherson was in talks to be cast in the leading role.    The next day it was confirmed he was cast as Nick Brady, a surfer dude who visits his brother in Colombia and falls in love with a local woman named Maria only to discover shes the niece of the highly dangerous narcoterrorist.  |author=Amanda Bell|date=December 18, 2012|accessdate=January 1, 2015}}  On March 25, 2013, Brady Corbet was cast as Hutchersons character brother, Dylan Brady. 

===Filming===
Principal photography was expected to begin in Panama in March 2013.   Filming was initially expected to last a month and a half,  finishing on May 30, 2013. However, it was rumored filming was also conducted during June and July 2013. 

==Release==
Escobar: Paradise Lost made its world premiere at the 2014 Toronto International Film Festival on September 11, 2014.  It also screened at the 2014 Telluride Film Festival, San Sebastian Film Festival, Rome Film Festival, and Zurich Film Festival.  
 second quarter of the year. 

===Marketing===
On July 14, 2014, a teaser trailer was released.  In August 2014, four new stills were released.   Official trailers were released on September 3, 2014  |date=September 3, 2014|accessdate=September 3, 2014}}  and November 13, 2014. 

===Home media===
Escobar: Paradise Lost was released on DVD and Blu-Ray on March 19, 2015 in France   and April 15, 2015 in Australia and New Zealand.  Further DVD and Blu-Ray releases include in the United Kingdom, Germany, Spain and the Netherlands on August 17, 2015.  

==Reception==

===Box office===
During its opening in France, the film debuted with a weekend total of $601,554. Its opening weekend in Spain brought in $620,845 and $79,637 in the United Arab Emirates.  As of January 4, 2015, the film has grossed $3,562,536 in these three markets. 

===Critical response===
Review aggregator Rotten Tomatoes gives the film a 60% approval rating based on reviews from 10 critics, with an average score of 5.6/10.  Metacritic gives the film a score of 63 out of 100 based on reviews from 5 critics, indicating "generally favorable reviews". 

At the Telluride Film Festival, Escobar: Paradise Lost received a generally positive critical response. Writing for The Hollywood Reporter, Todd McCarthy called the film "an absorbing and suspenseful drug trade drama" along with citing that "del Toro’s presence, like Marlon Brando|Brando’s in The Godfather, looms over everything that happens here". McCarthy also stated that "Di Stefano shows some real directorial chops in the film’s central and impressively extended action-suspense sequence". However, "the romantic interplay between Nick and Maria gets a bit tiresome and redundant due to the fact that they’re both so extremely nice and agreeable; Nick’s naivete and goody two-shoes Canadianism (he stresses that he’s not a Yank) also prove wearisome". 

Writing for Indiewire, Eric Kohn gave the film a B and praised the performances of del Toro and Hutcherson writing that del Toro "turns Escobar into a subdued terror whose ability to order murders with ease provides the movie with its chief source of dread". While Hutcherson "imbues the character with a believability that transcends the scripts limitations". However, Kohn also criticised the film as it "fails to develop the rest of its characters as well as it does for its two central men. The screenplay is similarly marred by formula, lagging whenever it hits certain high melodramatic notes, and reminding us of the stakes in play with mopey, dime-store gravitas". 

===Accolades===
{| class="wikitable sortable"
|+ Awards
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2015
|Platino Ibero-American Film Awards Best Picture 
|Escobar: Paradise Lost
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 