The Intruders (1969 film)
 
{{Infobox film
| name           = The Intruders
| image          = 
| image size     = 
| caption        =  Lee Robinson Lee Robinson Joy Cavill
| executive producer = John McCallum Bob Austin Lee Robinson
| based on       = story by Ross Napier
| starring       = Ed Devereaux
| music          = Eric Jupp
| cinematography = Peter Menzies Ron & Valerie Taylor (underwater)
| editing        = Don Saunders
| distributor    = Regent Films
| studio         = Woomera Productions
| released       = 12 December 1969
| runtime        = 100 mins
| country        = Australia
| language       = English
| budget         = 
}}
 Lee Robinson. It is a spin-off of the popular Skippy the Bush Kangaroo TV series.

==Synopsis==
A gang of criminals led by Meredith is looking for sunken treasure off Mallacoota, pretending to be diving for abalone. Sonny, son of Matt Hammond, the Chief Ranger of Waratah National Park, investigates with their  family friend, Clancy. Sonny and Clancy are kidnapped. Skippy comes to the rescue. After a speedboat chase and a fight in the sand dunes, Meredith is captured. 

==Cast==
*Ed Devereaux as Matt Hammond
*Tony Bonner as Jerry King Ken James as Mark
*Garry Pankhurst as Sonny
*Liza Goddard as Clancy
*Ron Graham as Yordan
*Jeanie Drynan as Meg
*Kevin Miles as Meredith
*Jack Hume as Curtis
*Jeff Ashby as Graigoe
*George Assang as Sigigi
*John Unicomb as Bernie
*Mike Dawkins as Scott
*Robert Bruning
*Harry Lawrence
*Skippy

==Production==
Filming began in October 1968 using the same crew as the TV series, with location shooting in Mallacoota and interior work in Sydney.

Underwater photography was done by the husband and wife team of Ron and Valerie Taylor. 

==Release==
The film failed to match the popularity of the TV series and was not a box office success.  John McCallum later claimed they:
 Got the money back on the film but we thought it would be a bigger success in the cinema. If they could see it for nothing at home, the Mums and Dads werent too keen to take the kids and pay at the cinema. We sold it to the Childrens Film Foundation in England and they did well with it. They cut it down to a 60-minute version and played in Saturday mornings in the cinemas.  

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at Australian Screen Online
*  at Oz Movies
 

 
 
 
 
 


 