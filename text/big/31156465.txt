Dharmayudham
{{Infobox film 
| name           = Dharmayudham
| image          =
| caption        =
| director       = A Vincent
| producer       = 
| writer         = VT Nandakumar
| screenplay     = VT Nandakumar
| starring       = Prem Nazir Srividya Kaviyoor Ponnamma Adoor Bhasi
| music          = G. Devarajan
| cinematography = Soorya Prakash
| editing        = G Venkittaraman
| studio         = Karthika Films
| distributor    = Karthika Films
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by A Vincent . The film stars Prem Nazir, Srividya, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Srividya
*Kaviyoor Ponnamma
*Adoor Bhasi
*P. J. Antony
*Bahadoor
*Nanditha Bose
*Rony Vincent

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran and G Kumarapilla. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dukhathin Kaippuneer || P Jayachandran || P. Bhaskaran || 
|-
| 2 || Kaamukahrithil kavitha || P. Madhuri || G Kumarapilla || 
|-
| 3 || Mangalaam Kaavile || P Jayachandran, P. Madhuri, Kaviyoor Ponnamma || P. Bhaskaran || 
|-
| 4 || Praananadha Enikku || Ayiroor Sadasivan || P. Bhaskaran || 
|-
| 5 || Sankalpa Mandapathil || P Jayachandran || P. Bhaskaran || 
|-
| 6 || Smarikkan Padippicha || P Susheela || P. Bhaskaran || 
|-
| 7 || Thrichevadikal || P Susheela || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 