The White Horse Inn (1926 film)
 
{{Infobox film
| name           = The White Horse Inn 
| image          =
| caption        =
| director       = Richard Oswald
| producer       = Richard Oswald
| writer         = Oskar Blumenthal (play) Gustaf Kadelburg (play) Alfred Halm
| narrator       = Max Hansen Henry Bender Livio Pavanelli
| music          = Werner R. Heymann
| editing        =
| cinematography = Arpad Viragh
| studio         = Richard-Oswald-Produktion
| distributor    = Süd-Film
| released       =  
| runtime        = 70 minutes
| country        = Germany
| language       = German
| budget         =
| gross          =
}} silent comedy Max Hansen and Henry Bender. It is based on the play The White Horse Inn by Oskar Blumenthal and Gustav Kadelburg.

==Cast==
* Liane Haid - Josefa Voglhuber, Wirtin  Max Hansen - Leopold Brandmayer, Zahlkellner 
* Henry Bender - Wilhelm Giesecke 
* Livio Pavanelli - Doctor Siedler 
* Maly Delschaft - Ottilie Giesecke 
* Hermann Picha - Hinzelmann, Privatgelehrter 
* Ferdinand Bonn - Bauer 
* Anton Pointner   
* Anita Dorris

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 