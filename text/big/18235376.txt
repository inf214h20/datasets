Daffy Rents
{{Infobox Hollywood cartoon
| series            = Looney Tunes (Daffy Duck and Speedy Gonzales)
| image             = 
| caption           = 
| director = Robert McKimson Michael OConnor  Manny Perez George Grandpré Norm McCabe
| layout_artist = Dick Ung
| background_artist = Tom OLoughlin Gonzales Gonzales
| musician = Irving Gertz
| producer = David H. DePatie Friz Freleng
| distributor    = Warner Bros. Pictures The Vitaphone Corporation
| release_date      = March 26, 1966 (USA)
| color_process     = Technicolor
| runtime           = 6 minutes
| preceded_by       = Clippety Clobbered
| followed_by       = A-Haunting We Will Go (1966_film)|A-Haunting We Will Go
| movie_language    = English
}}

Daffy Rents is a 1966 Looney Tunes cartoon featuring Daffy Duck and Speedy Gonzales.

==Synopsis==
Daffy gets called by the aptly named Dr. Ben Crazy (a parody of the then popular Ben Casey) to remove Speedy from his cats-only nursing home. To do his dirty work for him, Daffy uses his robot Herman to catch Speedy. Unfortunately, Herman proves to be no match for the mouse, (and Speedys rat cousin, Ramon) and both he and Daffy eventually give up. When he tries to get his money after getting rid of Speedy, the temperamental Dr. Crazy throws Daffy through the window and out of the office!

The cartoon then closes with Daffy remarking: "Its a sad state of affairs, when a mouse can make a machine turn a duck into a chicken on account of a rat.  What a revolting development!"

==See also==
* The Golden Age of American animation
* List of Daffy Duck cartoons

==References==
* 
* . Online. July 1, 2008.
*  Online. July 1, 2008.

==External links==
*  
*  

 
 
 
 
 


 