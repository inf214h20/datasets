Rascals (film)
 
 

{{Infobox film
| name           = Rascals 
| image          = Rascals2.jpg  
| caption        = Theatrical release poster
| director       = David Dhawan
| producer       = Sanjay Dutt Sanjay Ahluwalia Vinay Choksey
| screenplay     = Yunus Sajawal
| starring       = Ajay Devgan Sanjay Dutt Kangna Ranaut Lisa Haydon Arjun Rampal
| music          = Songs: Vishal-Shekhar Background Score: Atul Raninga Sanjay Wandrekar  
| cinematography = Vikas Sivaraman
| editing        = Nitin Rokade
| distributor    = Eros Entertainment
| released       =  
| country        = India
| language       = Hindi 
| budget         =   
| gross          =   
}}
 box office flop. 

==Plot==
Chetan Chauhan (Sanjay Dutt) and Bhagat Bhosle (Ajay Devgan) are two con artists, who have recently robbed Anthony Gonsalves (Arjun Rampal). Bhagat first meets Anthony and steals his suitcase, covering it up with a fake suitcase of his own, and tries to leave with it, but the real suitcase accidentally comes out of the fake one, causing Anthony to chase Bhagat, who escapes. When Anthony goes to his car, he finds out that someone stole it. Chetan takes the car to a dealer, who changes the color of the car to fool Anthony in case he comes.

The two cons meet each other on a flight to Bangkok. Bhagat brags to Chetan that he is going to meet a girl named Dolly(Lisa Haydon), who he has paid for 4 nights and 3 days. Bhagat even tells Chetan the code word for meeting Dolly. When they reach the airport in Bangkok, both rob each other, but only Chetan is successful, as he gets Bhagats cash and expensive watch, while Bhagat gets Chetans fake wallet with fake money and fake credit cards. The two become enemies since then. But both meet again when their eyes fall on Khushi (Kangna Ranaut). Chetan lies that he is a donor, who helps the needy, while Bhagat acts to be a blind man, who was once a Navy officer. Both of them play foul tricks to get Khushi, which leads to disastrous results! Later, Bhagat gets hurt in an accident with his motorbike, and claims that the shock repaired his eyesight. After a party, Bhagat, Khushi, and Chetan go to a church while they are drunk, where they meet father Pascal (Satish Kaushik).

Khushi says she wants to marry both Bhagat and Chetan, but Father Pascal says that only one will marry Khushi, and the two must fight each other in the ring. The two fight, while accidentally hitting Father Pascal in the process. Chetan and Bhagat eventually get unconscious, and wake up to find Pascal beaten up(by the cons themselves), and Khushi is missing. Soon, Anthony greets the two cons, and tells them that he has kidnapped Khushi. If they want her, they need to give him the money that they stole from him in 24 hours. The two first go to a local bank in Bangkok, and tries to tell the bankers to give them money. The bankers get confused by what they mean, and tells them about their loan policy. Soon, a group of real robbers come to the bank. One of them insults Bhagat and Chetan, and the two fight back, taking out all the robbers in the process. As the cons try to leave with the cash, a banker stops them and asks for it back. Later, they steal money dressed as Santa, and goes to Anthonys plane. Anthony gives them Khushi. When the cons ask Khushi who does she love more, she goes straight to Anthony. The two cons were actually conned by Anthony, Khushi(Anthonys girlfriend), Father Pascal(Khushis real father),and BBC(A friend of Anthony).

Later when Anthony reaches back home in India, he realizes that Bhagat and Chetan had stole from his own home to give him the ransom money, thus conning him of his own cash.

==Cast==
* Ajay Devgan as Bhagat Bhosle (Bhagu)
* Sanjay Dutt as Chetan Chauhan (Chetu)
* Arjun Rampal as Anthony Gonsalves 
* Kangna Ranaut as Khushi
* Lisa Haydon as Dolly
* Chunky Pandey as Bhagat Bholabhai Chaganmal a.k.a. BBC
* Hiten Paintal as Nano (Chetans sidekick)
* Satish Kaushik as Father Pascal
* Mushtaq Khan as Usmaan
* Bharti Achrekar as Rosy Gonsalves
* Anil Dhawan as Prithviraj Rana
* Mansha Bahl as Marry Gonsalves

==Production==
Film was originally set to release on 15 July 2011 alongside Zindagi Na Milegi Dobara but was delayed and was set to release on 12 August 2011. Later, the film was announced to release on Diwali, 26 October, thus clashing with Shahrukh Khan s production, Ra.One. The film was later given to 6 October 2011, thus having a Navratri|Dussehra release.

==Reception==

===Critical reception===
The film opened to negative reviews from critics. Rajeev Masand of CNN-IBN said that "Rascals is a third-rate comedy that borders on the offensive, and criticized the fact that it makes light of everything from starving orphans in Somalia to the physically handicapped." {{cite web|title=Rascals Review|url=http://ibnlive.in.com/news/masand-rascals-is-infuriatingly-unfunny-film/190965-47-84.html
|publisher=CNN-IBN|author=Rajeev Masand|date=7 October 2011}}  Sukanya Verma of Rediff commented that "Rascals is a series of misadventures that are dumb, dated and dreary", giving it 1.5 stars out of 5.  NDTV rated it 1.5 in a scale of 5, noting that "Wanted urgently: A makeover for David Dhawans brand of comedy. Seriously, this comedy is not funny."  Manisha Lakhe from DNA said that "Hammering your toes would be more entertaining than Rascals", rating the movie 1 out of 5.  Gaurav Malani from   gave the film 1 star out of 5. 

===Box office===
Although receiving negative response from critics worldwide, Rascals managed to have a fairly strong opening in most parts of the country with 60-70% collections for early shows at multiplexes. The film opened to full houses in single screens. Its first day collection was Rs 70&nbsp;million net on the opening day.  It had a huge fall throughout the start of the second week.   The movie collected around 67.5&nbsp;million nett it its second week and had a two-week total of 485&nbsp;million nett approx. Also at the overseas, the film had a good opening. The movie is said to have grossed around $750,000 (Rs 3,67,57,498) at Overseas Markets. The film grossed £318,000 (Rs 1,81,47,833) in United Kingdom, $165,000 in North America, $225,000 (Rs 30,02,245) in UAE and $40,000 (Rs 20,28,425) in Australia. On the whole, the film was declared as an flop grosser worldwide. 

==Soundtrack==
The films music has been composed by Vishal-Shekhar while lyrics penned by Irshad Kamil and Anvita Dutt Guptan. The rights were sold to Venus Records and Tapes.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| title1 = Rascals
| extra1 = Neeraj Shridhar
| length1 = 4:25
| title2 = Parda Nasheen
| extra2 = Neeraj Shridhar, Sunidhi Chauhan
| length2 = 4:23
| title3 = Tik Tuk
| extra3 = Daler Mehndi, Monali Thakur, Vishal Dadlani
| length3 = 3:53
| title4 = Shake It Saiyyan
| extra4 = Sunidhi Chauhan, Haji Springer
| length4 = 3:54
| title5 = Rascals (Dance Mix)
| extra5 = Neeraj Shridhar, Vishal Dadlani
| length5 = 4:08
| title6 = Shake It Saiyyan (Hip Hop Mix)
| extra6 = Sunidhi Chauhan, Haji Springer
| length6 = 3:50
}}

==Controversy==
Rascals faced legal problems when the multiplex giant Cinemax filed a legal suit against the producers of the film Vinay Choksey and Sanjay Ahluwalia. The said suit demanded a stay order on the release of the film. Confirming this, Vinay Choksey says, "Yes it is true that Cinemax has filed a law suit against us. However, the amount of Rupee|Rs. 20&nbsp;million was taken for a totally different reason and Rascals is in no way a part of that sum. It is just an arm twisting technique that they are employing to recover that sum."

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 