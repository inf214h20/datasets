The Tested
The 2009 independent feature film written and directed by Russell Costanzo and produced by Melissa B. Miller of  .  It is based on the award winning short of the same name.

Chosen to participant in the 2009   &   in New York City.  The Tested  is one of only ten narrative rough cuts chosen by the Lab for its artistic vision and outstanding promise.

==Synopsis==
Log Line:  A life shattering tragedy sends three people on vastly different paths to a similar goal of redemption and understanding.

One year ago while on duty as a plain clothes cop, Julian Varone gunned down an unarmed teen.  In that time Darralynn Warren, the teens mother, has spiraled into a pit of despair, while his brother, Dre, has gone another route... ganglife.  As for Julian, its time for him to get back to work.

It is a story about redemption, injustice of poverty, the cycle of violence and how these three lives, affected by a terrible tragedy, cannot find closure without the other.

==Production==
Writer/director Russell Costanzos inspiration for the short film came out of the fascinating dichotomy between his Gramercy Park neighborhood in New York City and it being the home to one of the Top 12 Most Dangerous Schools in the entire city  (according to the New York State Department of Education). This statistic, coupled with a horrifying incident involving a pregnant woman, who, while walking past the school, was struck by a stool thrown out of a 6th floor window, sparked his interest.

It made me think about what causes someone to act so violently, or how terrifying it’d be to attend one of New York’s toughest high schools. How do you learn? How do you progress?  When I went to high school my biggest concern was having to get through Beowulf… these kids have to worry about getting stabbed! he says.

After responding to a call for submissions by New York City based Clarendon Entertainment for short story ideas, Costanzo and Executive Producer Rodney Parnther honed the script and went into production on the short film in August 2005.  

The short took home the Best of Fest award at the 2006 Los Angeles International Short Film Festival  beating out over 600 short films and went on to make its television debut on BET J.  It was also awarded, Best Cinematography at the 2007 Vision Fest Film Festival. 

In June 2008 after securing the necessary production funds, Shoebox Pictures quickly jumped into pre-production, hiring the award winning casting director Eve Battaglia and started securing talent and key crew for the film.

Producer Melissa B. Miller has recently teamed up with Joseph Scarpinito and Scarpe Diem Productions to create a new web channel called  

Principal Photography commenced in August 2008 and shot for 24 days in and around the New York City Area.

==Cast==
*Aunjanue Ellis as Darraylynn Warren, the mother of a wrongfully slain son who must find it in her soul to forgive what has happened and be the best mother she can for her remaining two children.
*Armando Riesco as Julian Varone, an NYPD cop who is facing the most difficult time of his life...getting back to work after nearly a year off duty due to an accidental shooting of an unarmed black teen.
*Frank Vincent as Lieutenant Marino, the head of the police unit in which Julian has been transferred - does his best to give Julian some important advice as he acclimates back to life as a cop.
*Michael Morris Jr. as Dre Warren, a teenager who is propelled onto a path of rage and loneliness by his older brother’s untimely death – joins a street gang as an outlet and a way to connect with other people.
*Nathan Corbett as Curtis, Dre’s former childhood friend – recruits him into the Black Knights gang – learns a hard lesson about the pressures of ganglife.
*Tobias Truvillion as James, a man who carries himself with a serene violence – the multi-dimensional head of the Black Knights gang – carries the weight of being the protector of his community.
*Annie Parisse as Lisa Varone,  the estranged wife of tortured NYPD cop Julian Varone. Her life was torn apart with the tragic accidental shooting of Derek Warren, and now one year later shes forced to understand what her marriage has become.

==Key Crew==
* Writer/Director: Russell Costanzo
* Producer: Melissa B. Miller
* Executive Producer: W. Michael Weinstein
* Director of Photography: Chris Scarafile
* Editor: Michael Taylor and Russell Costanzo
* Production Designer: Alexandra Lynn
* Costume Designer: Rebecca Muh

==References==
 

==External links==
*  
*  

 
 
 
 
 
 