Quid Pro Quo (film)
{{Infobox film
| name           = Quid Pro Quo
| image          = Quid pro quo post.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Carlos Brooks
| producer       = {{Plainlist|
* Sarah Pillsbury
* Midge Sanford
}}
| writer         = Carlos Brooks
| starring       = {{Plainlist|
* Nick Stahl
* Vera Farmiga
}}
| music          = Mark Mothersbaugh
| cinematography = Michael McDonough
| editing        = {{Plainlist|
* Charles Ireland
* Lauren Zuckerman
}}
| studio         = {{Plainlist|
* HDNet Films
* Sanford/Pillsbury Productions 
}}
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $11,864   
}} drama thriller thriller film written and directed by Carlos Brooks and starring Nick Stahl and Vera Farmiga. The film is about a semi-paralyzed radio reporter who investigates a story that uncovers an odd subculture leading to a disturbing self-realization. The film premiered at the 2008 Sundance Film Festival on January 20, 2008,  and was released in the United States on June 13, 2008.

==Synopsis==
Isaac Knott (Nick Stahl) is a successful radio talk show host on a New York City public radio outlet. He lost the use of his legs at the age of eight in an automobile accident that also claimed the lives of his parents. He is confined to a wheelchair.

One day, Isaac learns about a man who showed up at a local hospital and demanded to have his legs amputated. The man was part of a secret subculture of able-bodied people who want to be paraplegics. They use wheelchairs whenever possible, and they try to deaden their legs through artificial means. Isaac becomes fascinated by these strange people, and begins studying the phenomenon for a news piece on his radio show.

Through his research, Isaac meets Fiona (Vera Farmiga), a sexy but mysterious blonde who collects and restores Chinese art. Fiona also owns a wheelchair she doesnt really need. Increasingly attracted to her, Isaac tries to learn all he can about her role in the fake-paraplegic underground. Fiona, however, does not give away her secrets for free. Soon, Isaac discovers that the exchange of information and trust goes deeper the longer they know one another.

==Cast==
* Nick Stahl as Isaac Knott
* Vera Farmiga as Fiona
* Rachel Black as Janice Musslewhite
* Jessica Hecht as Edie
* Jacob Pitts as Hugh
* Ashlie Atkinson as Candy
* Pablo Schreiber as Brooster
* Jeane Fournier as Charlene Coke
* Michal Sinnott as Isaacs Mom
* Joshua Leonard as Isaacs Dad
* James Frain as Father Dave
* Aimee Mullins as Raine
* Dylan Bruno as Scott Kate Burton as Merilee
* Ellen Marlow as Young Fiona
* Tommy Nelson as Young Isaac

==Production==

===Background===
On his inspiration for the film, director Carlos Brooks commented: "I was interested in taking this story into the psychological realm, where no matter what our physical condition, we all have the same psychological potentials or limitations that we struggle with. Isaac puts on these shoes which allow him to walk again. That send him on a quest where, in the end, he discovers that he has really been investigating himself." 

===Filming===
Principal photography for Quid Pro Quo took place on location in Upper Freehold, New Jersey, with additional road scenes shot in La Conner, Washington.   

==Critical reception==
The film received mixed reviews from film critics. As of June 13, 2008, the review aggregator website  , Quid Pro Quo never develops its effective parts into a convincing whole."  On Metacritic, the film currently holds an average score of 56 out of 100, based on 11 reviews. 

==See also==
*Body integrity identity disorder

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at IFC.com
*  
*  
*  

 
 
 
 
 
 