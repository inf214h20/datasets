Master of Bankdam
{{Infobox film
| name           = Master of Bankdam
| image          = "Master_of_Bankdam".jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 1-sheet theatrical poster
| director       = Walter Forde
| producer       = Edward Dryhurst Walter Forde
| writer         = 
| screenplay     =  Edward Dryhurst Moie Charles (additional dialogue)
| story          = 
| based on       =  
| narrator       =  Stephen Murray Linden Travers David Tomlinson
| music          = Arthur Benjamin
| cinematography = Basil Emmott
| editing        = Terence Fisher
| studio         = Holbein Films
| distributor    = General Film Distributors (UK)
| released       =   
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} 1947 British Thomas Armstrong. Stephen Murray, Linden Travers and David Tomlinson. Two generations of brothers struggle for control of the family business in 19th-century Yorkshire.

The film is the story of Bankdam, a small Yorkshire Mill. Run by the Crowther family, around 1860 it prospers and grows under its patriarch owner, Simeon Crowther. After family upheavals the firm goes through several crises under the management of his sons Zebediah and Joshua, who tend in oppose one and other. Joshua dies with many others in Mill collapse, partially blamed on his brother Zebediah. Joshuas role is taken over by his son Simeon. The old patriarch, Simeon dies. Zebediah with ill health retires to Vienna for treatment leaving his son, Lancelot Handel, with power of attorney in his absence. Things at the Mill deteriorate and a fatally ill, Zebediah returns and, with a mob outside the door, in a final scene he makes amends and entrusts Bankdam, not to his own son, but to Simeon as he realises that he is the only person that can save Bankham.

It was produced by Walter Forde and Edward Dryhurst, and directed by Walter Forde.. It was adapted for screen by Edward Dryhurst with additional dialogue by Moie Charles. The music is by Arthur Benjamin, performed by London Philharmonic and conducted by Muir Mathieson.   There is also evidence that it was produced by Ernest G. Roy of Nettlefold Studios according to his death notices.

The film was made by Holbein Films at Nettlefold Studios, Walton-on-Thames, Surrey, England. It was produced by Rank Films and distributed by Prestige Films. 

==References==
 

==External links==
  
*   Hal Ericksons review
*  
*  
*   Review by BOSLEY CROWTHER
Published: October 17, 1949

 

 
 
 
 
 
 
 
 
 


 
 