To Rome with Love (film)
 
{{Infobox film
| name           = To Rome with Love
| image          = To rome with love ver2.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Woody Allen
| producer       = Letty Aronson Stephen Tenenbaum Giampaolo Letta Faruk Alatan
| writer         = Woody Allen
| starring       = Woody Allen Alec Baldwin Roberto Benigni Penélope Cruz Judy Davis Jesse Eisenberg Greta Gerwig Ellen Page
| cinematography = Darius Khondji
| editing        = Alisa Lepselter Medusa Film Gravier Productions Perdido Production Medusa Distribuzione   Sony Pictures Classics   
| released       =  
| runtime        = 112 minutes 
| country        = United States Italy 
| language       = English Italian
| budget         =
| gross          = $73,244,881 
}}
 magical realist    romantic comedy film written and directed by and starring Woody Allen in his first acting appearance since 2006. The film is set in Rome, Italy; it was released in Italian theaters on April 13, 2012,  and opened in Los Angeles and New York City on June 22, 2012. 
 separate vignettes: a clerk who wakes up to find himself a celebrity, an architect who takes a trip back to the street he lived on as a student, a young couple on their honeymoon, and an Italian funeral director whose uncanny singing ability enraptures his soon to be in-law, an American opera director.

==Plot==
To Rome with Love tells four unrelated stories taking place in Rome. The second story, Antonios, is a direct lift with some amendments of an entire Fellini film, The White Sheik (1952).

===Hayleys Story===
American tourist Hayley falls in love with and becomes engaged to Italian pro bono lawyer Michelangelo while spending a summer in Rome. Her parents, Jerry (Woody Allen) and Phyllis, fly to Italy to meet her fiancé and his parents. During the visit, Michelangelos mortician father Giancarlo sings in the shower and Jerry, a retired—and critically reviled—opera director, feels inspired to bring Giancarlos gift to the public. Jerry convinces a reluctant Giancarlo to audition in front of a room of opera bigwigs, but Giancarlo performs poorly in this setting. Michelangelo accuses Jerry of embarrassing his father and trying to use him to revive his own failed career, which in turn breeds discontent between Michelangelo and Hayley.

Jerry then realizes that Giancarlos talent is tied to the comfort and freedom he feels in the shower; Jerry stages a concert in which Giancarlo performs at the Teatro dellOpera while actually washing himself onstage in a purpose-built shower. This is a great success, so Jerry and Giancarlo decide to stage the opera Pagliacci, with an incongruous shower present in all scenes. Giancarlo receives rave reviews, while Jerry is unaware that his direction has again been slammed as he doesnt understand Italian and so cannot read the reviews. Giancarlo decides to retire from opera singing, because he prefers working as a mortician and spending time with his family. But he appreciates being given the chance to live his dream of performing Pagliacci, and his success has mended the relationship between Michelangelo and Hayley.

===Antonios Story===

Newlyweds Antonio and Milly plan to move to Rome because Antonios uncles have offered him a job in their familys business. After checking into their hotel, Milly decides to visit a salon before meeting Antonios relatives. She becomes lost and loses her cell phone, but ends up at a film shoot where she meets Luca Salta, an actor she idolizes. He invites her to lunch. Back at the hotel, Antonio is worried Milly will be late for their lunch date with his aunts and uncles. Anna, a prostitute, then arrives, having mistakenly been sent to his room.

Despite his protests, she wrestles him into a compromising position just as his relatives arrive; the only way he can think to save face is to introduce Anna as Milly, and he convinces her to pose as Milly. The group goes to lunch at the same restaurant Luca takes Milly. Antonio becomes jealous as Luca flirts with Milly, but they dont see Antonio. Antonios uncles and aunts then take him to a party. Antonio has nothing in common with the people hes introduced to, but most of the male guests are Annas clients. Anna and Antonio walk in the garden, and Antonio talks about how pure Milly is. When Anna finds out he was a virgin before meeting Milly, she seduces him in the bushes.

Meanwhile, Luca tries to seduce Milly at his hotel room. Milly decides to have sex with him, but then an armed thief emerges and demands their valuables. Suddenly, Lucas wife and a private investigator arrive. Milly and the thief climb into bed and fool Mrs. Salta into believing the hotel room is theirs while Luca hides in the bathroom. Once his wife has left, Luca runs off. The burglar flirts with Milly and she has sex with him instead. When she returns to the hotel room, she and Antonio decide to return to their rustic hometown—but first they begin to make love.

===Leopoldos Story===

Leopoldo lives a mundane life with his wife and two children. The best part of his day is watching his bosss beautiful secretary Serafina walk around the office. Inexplicably, he wakes up one morning to discover that he has become a national celebrity. Paparazzi document his every move. Reporters ask him what he had for breakfast, if he wears boxers or briefs, whether he thinks it will rain. Leopoldo even becomes a manager at his company, and Serafina sleeps with him. He begins dating models and attending fancy film premieres. The constant attention wears on him, though. One day, in the middle of interviewing Leopoldo, the paparazzi spot a man "who looks more interesting," and they abandon Leopoldo. At first, Leopoldo welcomes the return to his old life. But one afternoon he breaks down when no one asks for his autograph. Leopoldo has learned that life can be monotonous and wearying whether one is a celebrity or a normal man. Still, it is much better to be a weary celebrity than it is to be a weary regular man.

===Johns Story===
John (Alec Baldwin), a well-known architect, is visiting Rome with his wife and their friends. He had lived there some thirty years ago, and he would rather revisit his old haunts than go sightseeing with the others. While looking for his old apartment building, John meets Jack, an American architecture student who recognizes him. Jack happens to live in Johns old building, and invites him up to the apartment he shares with his girlfriend Sally (Greta Gerwig). Throughout the rest of the story, John appears as a quasi-real and quasi-imaginary figure around Jack and makes unusually frank observations of events.  Sally tells Jack that she invited her best friend Monica, an actress, to stay with them and tells him that Monica (Ellen Page) gives off a sexual vibe that drives men crazy. John predicts Monica will bring trouble, and John keeps telling Jack that Monica will lead him to trouble. Even though John cautions Jack against cheating with Monica, he begins to succumb to her charms. Sally sets Monica up with Leonardo, one of their friends, and Jack is jealous of their relationship. One night he and Monica decide to cook dinner for Sally and Leonardo. They flirt more and more until Jack kisses Monica; they go down to his car to have sex.

Jack, now besotted with her, plans to leave Sally for Monica, but they decide Jack should wait until Sally finishes her midterms for Jack to break up with her. The trio go out for lunch after Sallys exams, and when they are alone, Jack tells Monica he plans to dump Sally that night. They make plans to travel to Greece and Sicily together. Then Monica gets a phone call from her agent who says she has been offered a role in a Hollywood blockbuster. She will film in Los Angeles and Tokyo for the next five months and she immediately becomes completely focused on preparing for the role. She forgets about traveling with Jack, who realizes how shallow she is. John and Jack walk back to the Roman street corner where they met and they part ways.

==Cast==
Grouped by storylines
{|
|valign=top|
* Alison Pill as Hayley, Michelangelos fiancée
*   as Michelangelo, Hayleys fiancé
* Woody Allen as Jerry, Hayleys father and Phyllis husband
* Judy Davis as Phyllis, Hayleys mother and Jerrys wife
* Fabio Armiliato as Giancarlo, Michelangelos father
|valign=top|
* Roberto Benigni as Leopoldo, a clerk and temporary celebrity
* Monica Nappo as Sofia, Leopoldos wife
* Cecilia Capriotti as Serafina, a secretary
*   as Marisa Raguso, an interviewer for Leopoldo
|-
|valign=top|
* Alec Baldwin as John, successful architect and Jacks acquaintance and adviser
* Jesse Eisenberg as Jack, Sallys boyfriend
* Greta Gerwig as Sally, Jacks girlfriend and Monicas best friend
* Ellen Page as Monica, Sallys best friend
*   as Leonardo
|valign=top|
*   as Antonio, Millys husband
* Alessandra Mastronardi as Milly, Antonios wife
* Penélope Cruz as Anna, a prostitute
* Simona Caparrini as Joan, Antonios aunt
* Ornella Muti as Pia Fusari, an actress
* Antonio Albanese,  as Luca
* Riccardo Scamarcio as hotel thief
|}

===Non-actor cast===
*  , the TG3 anchorwoman, is a real-life journalist of the Italian network Rai 3. The scene is shot in the real TG3 studio. 
* Pierluigi Marchionne, who plays a traffic policeman in the initial sequence, is a real Rome policeman. Woody Allen saw him directing traffic in Piazza Venezia and added that scene for him to be in. 

==Production== to the 14th century book by Italian author Giovanni Boccaccio, but several people didnt understand the reference, so he retitled it Nero Fiddles.    The new title was still met with confusion, so he settled on the final title To Rome with Love, although he has stated that he hates this title. 

==Release==
===Box office===
To Rome with Love was a box office success. As of November 15, 2012, it has earned $16,685,867    in the United States making it the seventh highest-grossing film in the 21 year history of Sony Pictures Classics. 

As of November 27, its worldwide total stands at $73,039,208.  This is Woody Allens fourth best ever box office total (not counting for adjusted ticket prices). 

===Critical reception===
The film has generally received mixed reviews from critics. The review aggregator Rotten Tomatoes gives the film a score of 43% based on reviews from 165 critics with an average score of 5.4/10.    The critical consensus is that "To Rome With Love sees Woody Allen cobbling together an Italian postcard of farce, fantasy, and comedy with only middling success."  Metacritic gives the film an average score of 55 out of 100, and thus "mixed or average reviews", based on eighteen professional critics.  Roger Ebert gave the film 3 stars out of 4 writing "To Rome With Love generates no particular excitement or surprise, but it provides the sort of pleasure he seems able to generate almost on demand." 

A. O. Scott of The New York Times found some of the scenes "rushed and haphazardly constructed" and some of the dialogue "overwritten and under-rehearsed", but also recommended it, writing "One of the most delightful things about To Rome With Love is how casually it blends the plausible and the surreal, and how unabashedly it revels in pure silliness."    On the other hand, David Denby of The New Yorker thought the film was "light and fast, with some of the sharpest dialogue and acting that he’s put on the screen in years." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at The Numbers
*  . On Point. June 19, 2012.
*  
*   filming locations at Movieloci.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 