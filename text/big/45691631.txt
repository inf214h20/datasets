A Maid of the Silver Sea (film)
 
{{Infobox film
| name           = A Maid of the Silver Sea
| image          =
| caption        =
| director       = Guy Newall George Clark
| writer         = John Oxenham  (novel)   Guy Newall  Cameron Carr
| music          = 
| cinematography = 
| editing        = 
| studio         = George Clark Productions
| distributor    = Stoll Pictures
| released       = November 1922
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Cameron Carr. novel of the same title by John Oxenham. 

==Cast==
*   Ivy Duke as Nance Hamon  
* Guy Newall as Stephen Gard  
* A. Bromley Davenport as Old Tom Hamon   Cameron Carr as Tom Hamon  
* Lilian Cavanagh as Julie  
* Charles Evemy as Berne Hamon  
* Winifred Sadler as Mrs. Hamon  
* Percy Morrish as Peter Mauger  
* Marie Gerald as Grannie   Charles Wood as Seneschal 
* Norman Loring as Doctor

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 