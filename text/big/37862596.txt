Country Wedding
{{Infobox film
| name     = Country Wedding
| image    = 
| director = Valdís Óskarsdóttir
| writer   = Valdís Óskarsdóttir
| starring = Ingvar Eggert Sigurðsson   Ólafur Darri Ólafsson   Björn Hlynur Haraldsson
| cinematography = Anthony Dod Mantle
| editing = Valdís Óskarsdóttir
| music = The Tiger Lillies
| producer = Árni Filippusson   Davíð Óskar Ólafsson   Hreinn Beck
| country = Iceland
| language = Icelandic
| runtime = 95 minutes
| released = 2008
}}
Country Wedding ( ) is a 2008 Icelandic film directed by Valdís Óskarsdóttir.  The film was released in Iceland on the 28th of August 2008. 

==Plot==
Inga and Barði decide to get married in a church in the countryside. They hire one bus for each family and guests and a band to play in the party. Inga travels with her parents and brother; her best friend Lára that brings her senile grandmother and her flirt Egill; and her mothers mate Svanur that has also a secret affair with Lára. Barði travels with his parents and sister Auður; the gay cousin of his father and his mate; and his friends Síði that Inga Hates and Grjóni that arrives later. They get lost and while seeking the church, the tension increases and secrets are disclosed in the dysfunctional families and friends in the great day of Inga and Barði. 

== Cast ==
* Ingvar Eggert Sigurðsson - Séra Brynjólfur
* Ólafur Darri Ólafsson - Egill
* Björn Hlynur Haraldsson - Barði
* Ágústa Eva Erlendsdóttir - Auður
* Gísli Örn Garðarsson - Grjóni
* Sigurður Sigurjónsson - Tómas
* Þröstur Leó Gunnarsson - Svanur
* Nína Dögg Filippusdóttir - Lára
* Hanna María Karlsdóttir - Imba
* Árni Pétur Guðjónsson - Stefán
* Kristbjörg Kjeld - Brynhildur
* Theódór Júlíusson - Lúðvík
* Nanna Kristín Magnúsdóttir—Inga

==References==
 

==External links==
*  
*  
* 

 
 


 