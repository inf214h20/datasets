Gigi (1949 film)
Gigi French comedy film directed by Jacqueline Audry and starring Gaby Morlay, Jean Tissier and Yvonne de Bray.  A young woman is manoeuvered into an arranged marriage with an older man, by her scheming aunt. It was based on the novella Gigi written by Colette.
 Gigi story, starring Leslie Caron, was filmed in 1958.  The 1949 film is included as an extra on the 2008 2-disc DVD  and 2009 Blu-ray Disc  versions of the 1958 film.

==Plot==

The story begins in 1900 fin de siecle Paris, through the eyes of the elderly Honoré LaChaille, who is a man who knows how to fully appreciate beauty, as he is watching the wonderfully untouched and lively sixteen-year-old school girl Gilberte "Gigi" Alvarez. Gigi happens to be the young ward and grand daughter of Honorés old friend and amorous acquaintance, the elegant Mme. Alvarez.

Honorés nephew, an arbiter elegantarium brat named Gaston LaChaille, is the center of the Paris social life, and the rich heir to the sugar throne of France. Bored with the tedious social life, Gaston finds peace and quiet only in the humble apartment of Mme. Alvarez, where he pays his visits on a regular basis.

Gigi is equally bored with her situation, since she is being thoroughly educated in the fine art of social etiquette, culture and all the finer things in life that ordinary men without means never would come close to. She doesnt comprehend the usefulness of all this peculiar information and skills, but both her grandmother and her strict great-aunt Alicia insists she comply with their education and that she soon will discover its meaningful purposes. Alicia tells Gigi that everything in life of importance, even love, has to be learned as an art form.

One day Gigi meets Gaston at her grandmothers apartment, and the young bachelor instantly falls for the girls rugged, unfinished charm. He invites her to go skating with him and his present mistress, Liane dExelmans. The event ends in disaster when Gigi talks down Liane behind her back, telling Gaston she finds her boring and uninteresting. Gaston also suspects Liane of having an affair with her skating instructor.

Gaston decides to end his relationship with Liane, and turns to his uncle Honoré for the proper advice. To get his revenge, Gaston pays the instructor a sum of money to leave Liane, before ending their relationship permanently. Encouraged by Honoré, Gaston starts participating heavily in the Paris social life, attending every upcoming fashionable party, to show that he remains unaffected by the end of his relationship. He doesnt visit Mme. Alvarez for weeks during this busy time, but Gigi is able to follow his adventures in the paper every week. 

Then Gaston unexpectedly reappears at Mme Alvarez apartment, bringing Champagne and chocolate. He announces that he will soon be leaving Paris for a while, traveling to a seaside resort. Gigi bets that if she wins a game of cards over him, he is forced to bring her and Mme. Alvarez with him to the coast. Gigi wins the game and Gaston fulfills his promise, taking them on his journey. Honoré also accompanies them. Gaston begins to treasure the company of young Gigi, who differs from the company he normally surrounds himself with in Paris.

Back in Paris, Alicia is furious that Gigi has been socializing with Gaston without having finished her education. The two elderly ladies decide that Gigi has to be kept away from Gaston and that her education should continue. When Gaston travels to Monte Carlo for a few weeks, Gigi is trained every day in the art of conversation, dressing and  etiquette.  She takes on a more subtle, composed persona, which is a great disappointment to Gaston when he returns from his trip. In an effort to reverse the process, he invites Gigi to join him in a popular but questionable club for tea. Gigi is prevented from going by her grandmother, who doesnt want Gigis reputation soiled.

Gaston is very upset by this treatment but slowly changes his mind, seeing that Gigi indeed has transformed into a beautiful young desirable woman. An agreement is reached between him and the two old ladies guarding Gigi, that she is to be Gastons formal female acquaintance&mdash;his lover. Gigi is not consulted regarding the terms of the agreement. When she hears about the formal arrangement, she is upset and refuses to take part in it. She will not play the game her aunt and grandmother has set up for her. Gaston then  admits that he has fallen in love with her, and cannot be just a friendly acquaintance of hers as before. Gigi is shocked and upset, accusing Gaston of not caring enough for her and enter into the foul agreement in the first place, and the two part on bad terms.

Gigi changes her mind and sends Gaston a note, saying she will accept the agreement just to be with him and not lose him entirely. Gaston returns to the apartment, and they leave together for a formal social event. Gaston is disappointed, however, with Gigis sophisticated behavior and brings her home early, doubting his decision to go through with the agreement. After wandering around the streets of Paris in despair that night, Gaston ultimately returns to the apartment to ask for Gigis hand in marriage.

==Cast==
* Gaby Morlay ...  Tante Alicia
* Danièle Delorme ...  Gilberte dite Gigi
* Jean Tissier ...  Honoré
* Yvonne de Bray ...  Mamita
* Franck Villard ...  Gaston Paul Demange ...  Emmanuel
* Madeleine Rousset ...  Liane dExelmans
* Pierre Juvenet ...  Monsieur Lachaille
* Michel Flamme ...  Sandomir
* Colette Georges ...  Minouche
* Yolande Laffon ...  Madame Lachaille
* Hélène Pépée ...  Andrée

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 