The Harvest (2013 film)
{{Infobox film
| name           = The Harvest
| image          = The Harvest (2013 film).jpg
| alt            =
| caption        = 
| director       = John McNaughton
| producer       = 
| writer         = Stephen Lancellotti
| starring       = Samantha Morton Michael Shannon Natasha Calis
| music          = George S. Clinton
| cinematography = Rachel Morrison
| editing        = Bill Pankow
| production companies = Das Films Elephant Eye Films Living Out Loud Films 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The Harvest is a 2013 horror thriller film that was directed by John McNaughton. It is the first feature film he has directed in over a decade (his last feature film being the 2001 movie Speaking of Sex) and his first horror venture since Haeckels Tale, a 2006 episode of the horror series Masters of Horror.     The movie had its world premiere on 19 October 2013 at the Chicago International Film Festival and follows a young girl (Natasha Calis) that befriends a seemingly lonely and confined boy her own age, only to fall afoul of his mother.

==Synopsis==
Reeling from the loss of her mother and father, the teenage Maryann (Natasha Calis) moves in with her grandparents and is grateful when she manages to befriend Andy (Charlie Tahan). As Andy is very, very sick he must stay within his home per the instructions of his mother Katherine (Samantha Morton), who also forbids him from having any visitors. When their visits are discovered, Andys father Richard (Michael Shannon) is initially fine with the visits but Katherine is having none of it and her behavior grows increasingly erratic.

==Cast==
*Samantha Morton as Katherine
*Michael Shannon as Richard
*Natasha Calis as Maryann
*Charlie Tahan as Andy
*Peter Fonda as Grandfather
*Leslie Lyles as Grandmother
*Meadow Williams as Sandra

==Reception==
Critical reception for The Harvest has been mostly positive.   IndieWire, Variety (magazine)|Variety, and Bloody Disgusting all gave favorable reviews for the film,   and Blood Disgusting heralded the film as "a triumphant return for one of indie cinema’s edgiest directors." 

==Release==
The film premiered on 19 October 2013 as part of the Chicago International Film Festival) and was screened on July 21, 2014 in Canada as part of the Fantasia International Film Festival.  The premiere, for a limited screening in the IFC Center and the video on demand release, is set for the April 10, 2015. 

==References==
 

==External links==
*  
*  at Fantasia Festival

 

 
 
 
 