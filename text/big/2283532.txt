Basket Case (film)
 

{{Infobox film|
|  name           = Basket Case
|  image          = Basketcaseposter.jpg
|  writer         = Frank Henenlotter
|  starring       = Kevin Van Hentenryck Terri Susan Smith Beverly Bonner
|  director       = Frank Henenlotter
|  producer       = Edgar Ievins
|  music          = Gus Russo
|  distributor    = Analysis Film Releasing Corporation
|  released       =  
|  runtime        = 91 minutes
|  language       = English
|  country        = United States
|  budget         = $35,000 
}} low budget, bizarre humor, and strong violence. The film gained an audience in the 1980s due to the advent of home video and  has become a cult film.   Kevin Van Hentenryck stars as a normal-looking person who seeks vengeance for the unwanted surgery that separated him from his deformed Siamese twin brother.

== Plot ==
Duane Bradley arrives in New York City with a locked basket.  After he gets a room at a cheap hotel, the contents of the basket are finally revealed: in it lives his deformed Siamese twin brother, Belial. Although conjoined at birth, the twins were surgically separated at an early age against their will, and Belial deeply resents being cut off from his normal-looking brother. As the twins seek revenge against the doctors responsible for their separation, Duane befriends a nurse, Susan.  Jealous, Belial attacks and kills her, frustrated with his inability to rape her.  Enraged at his brother for his actions, Duane attempts to kill Belial, which results in the two brothers falling from a hotel window.

== Cast ==
* Kevin Van Hentenryck as Duane Bradley
* Terri Susan Smith as Sharon
* Beverly Bonner as Casey
* Robert Vogel as Hotel manager
* Diana Browne as Dr. Judith Kutter
* Lloyd Pace as Dr. Harold Needleman
* Bill Freeman as Dr. Julius Lifflander
* Joe Clarke as Brian Mickey ODonovan

== Production == 42nd Street.     Henenlotter wrote the film as he walked around Times Square, which he called a "seedy, wonderful atmosphere." 

The special effects for Belial consist largely of a puppet in some scenes and stop motion in others. When Belials hand is seen attacking his victims, it is really a glove worn by Henenlotter. The full size Belial puppet is also seen in the scenes where Belial is seen with an actor or where his eyes glow red. The Belial rampage sequence used stop motion animation. 

== Release ==
Basket Case was released theatrically in the United States by Analysis Film Releasing Corporation beginning in April 1982.  It played as a midnight movie for several years after this. 

The film was first released on DVD in the United States by Image Entertainment in 1998.  This version is currently out of print.  The film was re-released on special edition DVD by Something Weird Video in 2001.   It was released on Blu-ray September 27, 2011.   Henenlotter supervised the Blu-ray release himself and fixed many of the issues he had with previous releases.  This release was based directly on the original prints, which had initially been thought lost.  

== Reception ==
Rotten Tomatoes reports that 75% of 20 surveyed critics gave the film a positive review; the average rating is 6.2/10.   Variety (magazine)|Variety called it "an ultra-cheap monster film" with fine acting but criticized the blowup from 16&nbsp;mm.   David Harley of Bloody Disgusting rated it 3.5/5 stars and wrote that "its exactly the kind of movie it sets out to be."   Heather Wixson of Dread Central rated it 3.5/5 stars and called it an "insane masterpiece that lovingly celebrates the sometimes schlocky and sleazy side of cinema".   G. Noel Gross of DVD Talk rated it 5/5 stars and called it "an undeniable, unavoidable and unforgettable clasSICK".   Patrick Naugle of DVD Verdict wrote, "The movie is just pure shock value" but "a heck of a lot of fun."   John Kenneth Muir wrote that it is "a fine, competent low-budget effort that generates thrills and discomfort not only from its tale of symbiotic (and separated) Siamese twins, but from its authentic sense of place.  New York City has never felt more delightfully and dangerously squalid."  Muir goes on to call it "oddly compelling, deeply disturbing and inexplicably touching". 

Rex Reeds quotation ("This is the sickest movie ever made!") used in promotion was not from any printed review.  Reed had sought out the film after hearing negative reviews and was asked his opinion after emerging from the cinema.  Unknown to Reed, the person who asked him was director Frank Henenlotter.  Initially furious that his comment was used to promote the film, Reed eventually relented and granted permission. 

== References ==
 

== Further reading ==
*  

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 