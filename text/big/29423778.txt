Cold Weather (film)
 
{{Infobox film
| name = Cold Weather
| image = Cold Weather.jpg Aaron Katz
| producer = Brendan McFadden Ben Stambler Jay Van Hoy Lars Knudsen Aaron Katz Aaron Katz Brendan McFadden Ben Stambler
| starring = Cris Lankenau Trieste Kelly Dunn Raúl Castillo Robyn Rikoon
| cinematography = Andrew Reed Aaron Katz
| music = Keegan DeWitt
| distributor = IFC Films
Axiom Films  
| released =  
| country = United States
| language = English
}} Aaron Katz, Dance Party USA.  

Cold Weather premiered at the South by Southwest Film Festival in March 2010 and was released in the United States by IFC Films on February 4, 2011.

==Plot==
The film, set in Portland, opens with Doug (Cris Lankenau) moving in with his sister, Gail (Trieste Kelly Dunn). Doug has recently moved from Chicago, where he was studying forensic science and lived with now ex-girlfriend Rachel (Robyn Rikoon). After getting a job at an ice factory, he befriends Carlos (Raúl Castillo), a co-worker who also DJs on the side. Rachel arrives in Portland to train at the home office of the Chicago law firm where she is employed, but suddenly disappears, leaving a trail of intriguing clues. Doug, Gail and Carlos begin investigating her whereabouts.

==Production==
Cold Weather was shot on location in  . 

==Reception==
Cold Weather opened to very positive reviews and currently holds a 78% "fresh" rating on the film review aggregator site  .      .  The film received "Two Thumbs Up" from  . 

==References==
 

== External links ==
*  
*  
*  
*   at IFC Films
*  
*  
*  

 
 
 
 
 