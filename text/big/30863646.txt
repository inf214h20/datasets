Mondo cane
 
{{Infobox film
| name = Mondo cane
| image = Mondo cane poster.jpg
| caption = Theatrical poster
| director = Gualtiero Jacopetti Paolo Cavara Franco Prosperi
| producer = Gualtiero Jacopetti Angelo Rizzoli
| narrator = Stefano Sibaldi
| writer = Gualtiero Jacopetti Paolo Cavara
| cinematography = Antonio Climati Benito Frattari
| music = Riz Ortolani Nino Oliviero
| editing = Gualtiero Jacopetti
| distributor = Cineriz
| released = Italy:   United States: April 3, 1963
| runtime = 108 minutes Italian
| gross = $2,000,000 (US/ Canada) 
}}
 Italian filmmakers Western film audiences.  These scenes are presented with little continuity, as they are intended as a kaleidoscopic display of shocking content rather than presenting a structured argument. Despite its claims of genuine documentation, certain scenes in the film are either staged or creatively manipulated to enhance this effect. 
 exploitation documentaries, many of which also include the word "Mondo" in their title. 

==Vignettes== their meat. Tokyo has Los Angeles leis and Port Moresby, New Guinea.

==Production==
 
In the beginning, as Cavara (who took the helm for European and Euro-Asiatic zone) and his supervisor Stanis Nievo interviews revealed, Mondo Cane was a unique project conceived with La donna nel Mondo, and worked at the same time  (1960–62). 

==Reception== exploitation documentaries, genre known Addio zio drama Wild Eye (Occhio selvaggio).

===Awards=== Una vita English by Norman Newell. In 36th Academy Awards|1963, the song was nominated for the Academy Award for Best Song, where it lost to "Call Me Irresponsible" from the film Papas Delicate Condition.

==Influence==
The film spawned several direct sequels, starting with Jacopetti and Prosperis own Mondo Cane 2 (also known as Mondo Pazzo), released the following year. Much later, in the 1980s, two more sequels emerged: Mondo Cane Oggi: LOrrore Continua and Mondo Cane 2000: LIncredible. Kerekes & Slater, pp. 152-153.  The franchise continued into the nineties with two sequels from the German Uwe Schier; despite the fact that they were the fifth and sixth films in the series, they were titled Mondo Cane IV and Mondo Cane V. Kerekes & Slater, pp. 157-158. 
 Mondo Bizarro, Mondo Daytona, Mondo Freudo (1966), Mondo Mod, Mondo Infame and Mondo Hollywood; Kerekes & Slater, p. 107.  later examples include the Faces of Death series.

The film also inspired lampooning, including Mr. Mikes Mondo Video, written by Saturday Night Lives Michael ODonoghue and starring members of the contemporary cast of that program. 
 Mondo Cane.

==References==
 

==Bibliography==
*Goodall, Mark. Sweet & Savage: The World Through the Shockumentary Film Lens. London: Headpress, 2006.
*Kerekes, David, and David Slater. Killing for Culture: An Illustrated History of Death Film from Mondo to Snuff. London: Creation Books, 1995.
* Stefano Loparco, Gualtiero Jacopetti - Graffi sul mondo. The first complete biography dedicated to the co-director of Mondo cane. Il Foglio Letterario, 2014 - Isbn: 9788876064760
* Fabrizio Fogliato, Paolo Cavara. Gli occhi che raccontano il mondo Il Foglio Letterario 2014. (It contains Mondo canes original subject, never published before, and a developed critical analysis on great and artistic approach by Cavara in Mondo cane and La donna nel mondo (pp. 57-88; 187-212)

== External links ==
*  

 
 
 
 
 
 
 
 