Clannad (film)
{{Infobox film
| name               = Clannad
| film name = {{Film name| kana               = クラナド
| romaji             = Kuranado}}
| image              = Clannad movie.jpg
| caption            = Regular edition DVD cover.
| director           = Osamu Dezaki
| producer           = 
| writer             = Makoto Nakamura   Jun Maeda (visual novel)
| based on           =  
| starring           = Kenji Nojima Mai Nakahara Ryō Hirohashi Hōko Kuwashima Daisuke Sakaguchi
| music              = Yoshichika Inomata
| cinematography     = Takeshi Fukuda Tomokazu Shiratori
| artdirector      = Shoichiro Sugiura
| editing            = Masahiro Goto
| studio             = Toei Animation
| distributor        =  Toei Company
| released           =  
| runtime            = 90 minutes
| country            = Japan
| language           = Japanese
| budget             = 
}} visual novel Nagisa Furukawa.
 high school uniform from Clannad emerged from the bus to hand out flyers and pink and white colored thundersticks to passersby to promote the film. 

==Plot== Tomoya Okazaki Nagisa Furukawa. Youhei Sunohara Tomoyo Sakagami, Kyou Fujibayashi, quickly goes around; uses red paint to deface the posters and write invalid on them because of school policy. This angers Youhei greatly and he beats up one of the student council members despite Tomoyos and Kyous warning; Tomoya also takes a stand for Nagisa.
 Kouko Ibuki, soliloquy based Yusuke Yoshino; Akio and List of Clannad characters#Sanae Furukawa|Sanae. Tomoya is challenge to a baseball match with Akio to see if Tomoyas a man worthy for Nagisa, but cannot throw the ball due to an injured arm he received from his father in a bad fight, and even gets to stay overnight after being heavily persuaded by Akio.

Like Nagisa, Tomoya has also had a recurring dream ever since he was a child. In his dream, he initially is disembodied in an illusionary world where he is the only thing "alive". He finds a discarded human-sized doll and uses it as a body to travel around the world on an old bicycle. After some searching, he finds a large sakura tree known as the "Tree of Promises" where he believes he will meet the person he has felt is with him in this illusionary world.

When the school festival finally begins, Nagisa reveals that she has not finished the script, but still wants to go on with the play since the story is still within her. Nagisa has her performance in the evening, so in the meantime Nagisa hangs out with Tomoya and Youhei. During lunch, she tells them her story why she wanted to do drama was due to her parents formally having been theater actors, but they both had to give up acting after Nagisa was born. Nagisa wants to do drama so as to continue her parents dreams in her footsteps. For the play, Sanae gives Nagisa her wedding dress to use as her costume, much to Akios surprise. Nagisa starts with her monologue without a hitch, and during her recitation Kouko gives stage directions to Tomoya and Youhei via headsets. Gradually, Tomoya comes to realize that the story Nagisa is reciting is the same dream that he has had, and is shocked to find that Nagisa also had the same dream of the illusionary world. At the plays conclusion, Tomoya believes that he and Nagisa were meant to be together and confesses his love to her.
 Ushio for the child. Despite the doctors warning, she insists that she will be fine. One winter night, Nagisa finally gives birth to a baby girl Ushio and Nagisa dies that same night. This causes Tomoya to go into deep depression, during which he does not go to work, or even visit his daughter who is now being raised by Nagisas parents.
 Naoyuki Okazaki comes over and confesses that hes putting Ushio in the same relationship he did when his mother had died, which greatly surprises him. Later, Naoyuki requested Tomoyas friends: Youhei, Tomoyo, Kyou, Kouko and her husband Yusuke to take him out on a retreat for a few days in order to break his depression, and they take Tomoya out of his house so fast he does not even know what is going on. Once they tell him on the train, Tomoya is dead set on going back home but Yusuke convinces him to stay. When they arrive at their destination, Tomoya searches for another platform at the station, anticipating this, Akio and Sanae were waiting for him with Ushio. Just as Tomoya begins to walk away, his friends arrive blocking his way, then Tomoya turns around back at Ushio, she runs towards him holding a stuffed dango. She trips midway which makes Tomoya leap out and catch her. Picking her up with them smiling, he is able to see the continuation of his old dream once more, and sees Nagisa under the Tree of Promises smiling lovingly at him and Ushio.

==TV and film differences== Kyou (Ryō Tomoyo (Hōko Kotomi appearing Fuko not Ryou does Kouko is Toshio Koumura Yusuke (Hikaru Midorikawa) plays a bigger role in the film in how he helps Tomoya obtain a job at an electrical company and remains his friend during Tomoyas depression.
 graveyard shift Akio and Sanae mostly Yuichi Nakamura in the TV series). However the ending between the series and film has one major change, with Nagisa being brought to life in the series whereas she remains deceased by the conclusion of the film.

==Production==
The film was first announced to be in production at the Tokyo Anime Fair on March 23, 2006 for a planned 2007 release. The original character design was conceived by   film, also providing the character design. The screenplay was written by Makoto Nakamura who had worked on the first Kanon anime television series in 2002, and the Air (film)|Air film in 2005, two other anime adapted from visual novels originally made by Key. Finally, the film was directed by Osamu Dezaki, who has been involved with animation direction since the first Astro Boy anime in 1963, and went on to direct the Air film.

==Media releases==

===DVDs=== Sentai Filmworks licensed the film and released the film in both English-subtitled and dubbed format in March 2011. 

===Music=== arranged by Lia named Comiket 72 on August 17, 2007, but was only given to those who bought film tickets in advance. The List of Clannad soundtracks#Clannad Film Soundtrack|films original soundtrack was released on November 21, 2007 by Frontier Works.

==References==
 

==External links==
*  at Toei Animation  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 