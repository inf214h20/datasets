The Moo Man
{{Infobox film
| name           = The Moo Man
| image          = DVD cover image for The Moo Man.jpg
| alt            = 
| caption        = 
| director       = Andy Heathcote
| producer       = Heike Bachelier Andy Heathcote
| screenplay     = 
| based on       = 
| starring       = Steve Hook Ida the cow
| music          = Stephen Daltry
| cinematography = Andy Heathcote
| editing        = Heike Bachelier
| studio         = Trufflepig Films
| distributor    = 
| released       = 2013
| runtime        = 98 minutes    
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 organic dairy farm in Sussex, England. The film had its premiere at the 2013 Sundance Film Festival, where it competed in the World Cinema Documentary category.       
 unpasteurised organic milk. Near Hailsham, Sussex, Longleys Farm is situated on the Pevensey Levels.    The farm was started by Steves father Phil, who is still involved in the business, and who also features in the film, along with Steves wife and four children. 

The film covers Steves struggles to keep the 55-head-herd farm afloat in the face of the power of the supermarkets,    and shows his close relationship with his cows, especially Ida, a 12-year-old Friesian.      The documentary was filmed over four years.   

The film was selected as one of the twelve competitors in the World Cinema Documentary category at the Sundance Film Festival, held in Utah, US in January 2013. The films director, Andy Heathcote, explained that he and his partner Heike Bachelier, the films co-producer and editor, had almost not applied to enter the festival because the entry fee was a lot for their small production company, Trufflepig Films, to find: "Sundance selection was something we never even thought to be a realistic possibility. We weren’t even going to apply. For a hand-to-mouth DIY film outfit like us, the £75 fee might at least fill the petrol tank. However, can an indie outfit like us NOT apply to Sundance? No, of course not. The car was light on juice for a week, but I’m so very glad it was."   

In a preview of the 2013 Sundance Film Festival, festival director John Cooper said of the film: "I have kind of a crazy favourite, a film I watched and just couldn’t shake until the end. It’s a documentary called The Moo Man about a dairy farmer in England. I found it to be such a beautiful story, really touching. It’s probably not something that most people would rush to, but I found it really special."     
 Hailsham Pavilion Cinema on 30 September 2012.    The film is scheduled to have five screenings at the Sundance Film Festival; its world premiere was held there on 21 January 2013.  

==References==
 

==External links==
* 
* 
* 
*  
*  
* 

 
 
 
 
 
 
 
 