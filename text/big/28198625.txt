The Maker (film)
 
{{Infobox Film
| name           = The Maker
| image          = The Maker (film).jpg
| image_size     = 
| caption        =  Tim Hunter
| producer       = Demitri Samaha
| writer         = Rand Ravich
| narrator       = 
| starring       = Matthew Modine Mary-Louise Parker Jonathan Rhys-Meyers Fairuza Balk
| music          = Carol Sue Baker
| cinematography = Hubert Taczanowski
| editing        = Scott Chestnut
| distributor    = Amuse Video
| released       = October 17, 1997
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Tim Hunter. The Maker was released on October 17, 1997 in the United States of America.

==Plot==
Josh (Jonathan Rhys Meyers) is a high school guy who lives with adoptive parents and is involved in little crimes with his friends, including young lesbian Bella (Fairuza Balk). Suddenly his elder brother Walter (Matthew Modine) comes out of the blue after leaving home 10 years ago when he was 18 and not being heard of all those years. Walter starts to involve Josh in various new criminal activities, including robbery.

==Cast==
*Matthew Modine as Walter Schmeiss
*Mary-Louise Parker as Officer Emily Peck
*Jonathan Rhys Meyers as Josh Minnell
*Fairuza Balk as Bella Sotto
*Michael Madsen as Skarney

== External links ==
 

 

 
 
 
 
 


 