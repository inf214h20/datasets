Baabul (2006 film)
{{Infobox film

 | name = Baabul
 | caption = 
 | director = Ravi Chopra
 | producer = B. R. Chopra
 | writer = 
 | narrator =  John Abraham Om Puri
 | music = Aadesh Shrivastav Barun Mukherjee
 | editing = 
 | distributor = B. R. Films
 | released = 8 December 2006
 | runtime = 169 minutes
 | country = India
 | language = Hindi
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image= Baabul VideoCover.png
}}
 John Abraham and Hema Malini. The movie was dubbed in Telugu as Premabhishekam. The movie celebrates B. R. Chopras 50 years in cinema.

== Synopsis ==
 
Balraj Kapoor (Amitabh Bachchan) is a rich businessman, happy and content with his world.  He and his wife Shobhna (Hema Malini) live happily together and they wait for their only son Avinash a.k.a. Avi (Salman Khan) to return from his studies in the United States. After an absence of 7 years, Avinash comes back to India.  He reunites with his parents at the airport and races home. Back home, Avi joins his fathers business.

One day they decide to go golfing and Balraj accidentally hits the ball which ruins a painting. The young woman painting the picture, Malvika a.k.a. Millie (Rani Mukerji) gets furious with Balraj and leaves.  Later on, Avi runs into Millie on the golf course, and they start to talk.  Millie tells him that a stupid old man ruined her painting and points out that it was Balraj; but she doesnt know that the old man is Avis father and he doesnt tell her.  They start to hang out together while slowly falling in love with each other.
 John Abraham). She tells him that she loves someone, which devastates him because he is also in love with her.  One day, Millie goes to Avis office to drop off a painting and finds out that Balraj is Avis father.  When Avi shows up, she leaves the office crying.  He chases after her and says that the only reason he lied to her was because he loves her.  She doesnt accept his apology right then, but when Balraj apologizes to her for ruining her painting, she forgives them both.

Avi asks Millie to get dressed up for a party at his house. When she arrives, there is no one except Avi. He proposes to her and she accepts.  The whole family visits Millies house to discuss marriage and everyone agrees, except Rajat. He tells Avi that hell let him marry her if only he promises to always keep her happy and never let her get sad. Avi and Millie get married and soon theyre expecting a child.  Time goes on, Millies water breaks while in yoga class and Avi and she flee to the hospital where they are blessed with a healthy baby boy.

Their child Ansh grows up to be a sweet and intelligent boy.  The family throws a birthday bash for Ansh and wait for Avi who is in a meeting.  But Avi never arrives home.  He is involved in an accident and dies.  Millie is devastated.  Balraj seeks to bring happiness back into Millies life and gets into contact with Rajat.  He learns that Rajat still loves Millie. Balraj asks Rajat to marry Millie. However, there is stiff opposition from Shobhna and Balrajs conservative older brother Balwant (Om Puri). Shobhna fears losing her grandson Ansh, while Balwant thinks a widows remarriage is a sin.  However, Balraj stands firm and convinces everyone. In the end, Rajat and Millie get married, and everything is well. Avinash comes in as a spirit and blesses his dad with happiness.

==Cast==
* Amitabh Bachchan ... Balraj Kapoor
* Rani Mukerji ...Malvika Talwar-Kapoor (Milli)
* Hema Malini ...Shobhana Kapoor (Balrajs Wife)
* Salman Khan ...Avinash Kapoor (Avi)
* John Abraham (actor)| John Abraham ... Rajat Verma
* Om Puri ... Balwant Kapoor (Elder Brother of Balraj)
* Sarika .... Pushpa (Widowed sister-in-law of Balwant and Balraj)
* Aman Verma ... Shobhnas brother
* Parmeet Sethi .... Balwants son
* Sharat Saxena .... Malvikas father
* Smita Jaykar .... Malvikas mother
* Rajpal Yadav .... Servant
* Shriya Saran ... appeared in title track

== Music ==
The music is composed by Aadesh Srivastava. The soundtrack has 11 songs. The soundtrack was one of the most successful soundtrack of 2006.

{| class="wikitable"
|- style="background:#d1e4fd;"
! Song !! Singer(s) !! Other notes
|-
|Gaa Re Mann || Alka Yagnik, Kavita Krishnamurthy, Kailash Kher & Sudesh Bhosle || Filmed on Salman Khan, Amitabh Bachchan, Rani Mukerji, and Hema Malini
|-
|Come On Come On || Amitabh Bachchan, Sonu Nigam, Vishal Dadlani, Aadesh Shrivastava & Ranjit Barot || Filmed on Salman Khan, Amitabh Bachchan, and Hema Malini
|-
|Har Manzar || Kunal Ganjawala || Filmed on John Abraham and Amitabh Bachchan
|-
|Baawri Piya Ki || Sonu Nigam || Filmed on Salman Khan and Rani Mukerji
|-
|Bebasi Dard Ka Alam || Kunal Ganjawala || Filmed on John Abraham, Rani Mukerji, and Amitabh Bachchan
|-
|Keh Raha Hai || Sonu Nigam and Shreya Ghoshal || Filmed on Salman Khan and Rani Mukerjii
|- Jagjit Singh || Filmed on Amitabh Bachchan
|- Richa Sharma || Originally a 15 minute song was Bollywoods longest track, film used 2-3 min. version. 
|-
|Har Manzar (Remix)(DJ Suketu - Arranged by Aks) || Kunal Ganjawala ||
|-
|Come On Come On(Remix)(DJ Suketu - Arranged by Aks) || Amitabh Bachchan, Sonu Nigam, Vishal Dadlani, Aadesh Shrivastava & Ranjit Barot ||
|-
|Vaada Raha || Sonu Nigam || Filmed on Rani Mukherjee
|}

== Crew ==
* Director: Ravi Chopra
* Screenplay: Suresh Nair
* Story: Dr. Achala Nagar
* Dialogue: Ravi Chopra
* Producer: B. R. Chopra
* Music: Aadesh Shrivastava
* Cinematography: Barun Mukherjee
* Editor: Godfrey Gonsalves
* Art Direction: Keshto Mandal
* Choreography: Farah Khan, Remo D’Souza, Vaibhavi Merchant & Rajeev Surti Sameer
* Action: Mahendra Verma
* Costume Designs:
** Sabyasachi Mukherjee- Rani Mukerji
** Neeta Lulla- Hema Malini
** Alvira Khan- Salman Khan John Abraham
** Gabbana Akhbar- Amitabh Bachchan

== Response ==
=== Box office ===
Baabul opened really well in Mumbai with a decent response of 75%. However the rest of the country saw an average opening. The small centres opened to poor response and overall the film did not do quite well in India but was an above average grosser.  However it did great business overseas, entering at the 8th position on the UK charts in its first week.  It was a big hit in the overseas market. 

=== Reviews ===
Reviews have been generally mixed. The first reviews of the film were positive,   but it has generally gathered some sharp criticism along the way. Film critic Samrat Sharma calls the first half "irrelevant in most parts", and also "hollow and undeveloped".  Reviewer Baradwaj Rangan was even more scathing when he said that, "the casting of the leads and the high-gloss setting of their lives just do not make the subsequent events convincing." 

== Awards ==
Nominated
 John Abraham 

==See also==
*Babul

==References==
 

==External links==
*  

 
 
 
 
 