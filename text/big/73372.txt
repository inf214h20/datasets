Blacksmith Scene
{{Infobox film
| name           = Blacksmith Scene
| image          = BlacksmithScene.jpg
| caption        = Screencap from Blacksmith Scene William K.L. Dickson
| producer       = 
| writer         =  John Ott
| music          = 
| cinematography = William Heise
| editing        = 
| distributor    = Edison Manufacturing Company
| released       =  
| runtime        = 34 seconds
| country        = United States Silent
| budget         = 
| gross          =
}}

 
 short black-and-white William K.L. French inventor motion picture movie camera|camera.
It is historically significant as the first Kinetoscope film shown in public exhibition on May 9, 1893 and is the earliest known example of actors performing a role in a film. In 1995, Blacksmith Scene was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". It is the second-oldest film included in the Registry, after Newark Athlete (1891).

==Plot== stationary camera. striker to either side (portrayed by Edison employees). The smith uses a heated metal rod he has removed from a fire and places it on the anvil. All three begin a rhythmic hammering. After several blows the metal rod is returned to the fire. One striker pulls out a bottle of beer, and they each take a drink. Following this drink they then resume their work. 

==Production== Edison Manufacturing 1890 under Black Maria Brooklyn Institute on May 9, 1893.    

According to the  . 

==Cast==
* Charles Kayser as Blacksmith
* John Ott as Blacksmith

==Awards and nominations==
{|border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!Year
!Award
!Category — Recipient(s)
|-
| 1995 || National Film Registry || National Film Registry
|}

== Current status == Henry Ford Museum; it is the source of the negative preserved by the Museum of Modern Art film archive.  Another copy is at the Edison National Historic Site, administered by the National Park Service. Because the film was finished before 1923, its copyright has expired; it is freely available on the World Wide Web.

==See also==
*Treasures from American Film Archives

==References==
 

==External links==
* 
* 
*  
*  
*   on YouTube

 
 
 
 
 
 
 
 