Incident in New Baghdad
{{Infobox film
| name           = Incident in New Baghdad
| image          = 
| image_size     =
| caption        = 
| director       = James Spione
| producer       = 
| writer         = 
| narrator       =
| starring       =
| music          = Emile Menasche
| cinematography = 
| editing        = James Spione
| distributor    = 
| released       =
| runtime        = 22 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}

Incident in New Baghdad is a 2011 short documentary film about the July 12, 2007 Baghdad airstrike, directed by James Spione.

The film features a first-person account from Ethan McCord, one of the first soldiers to arrive at the scene of the airstrike that killed between 12   to "over 18"   people and wounded 2 children   in New Baghdad during the Iraq War.

The film premiered theatrically at the 2011 Tribeca Film Festival, where it won the prize for Best Short Documentary.  It was nominated for the Best Documentary Short Subject at the 84th Academy Awards.  It was the second Kickstarter-funded film to be nominated for an Academy Award. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 