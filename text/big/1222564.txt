Girl 6
 
{{Infobox film 
 | name           = Girl 6
 | image          = Girl six poster.jpg
 | caption        = Theatrical release poster
 | director       = Spike Lee
 | producer       = Spike Lee
 | writer         = Suzan-Lori Parks Madonna Quentin Tarantino Ron Silver John Turturro Naomi Campbell Prince
 | cinematography = Malik Hassan Sayeed
 | studio         = Fox Searchlight Pictures 40 Acres and a Mule Filmworks
 | distributor    = 20th Century Fox
 | editing        = Sam Pollard
 | released       = March 22, 1996
 | runtime        = 108 minutes
 | country        = United States
 | language       = English
 | budget         = $12 million
 | gross          = $4,939,939
}}
 director Spike soundtrack is composed entirely of songs written by Prince (musician)|Prince. The film was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.    Directors Quentin Tarantino and Ron Silver make cameo appearances as film directors at a pair of interesting auditions. It is the first film directed by Lee in which he did not write the screenplay.

==Plot==
Judy, also known as Girl 6 (Theresa Randle), is at a very awkward audition with Quentin Tarantino.  Judy seems to grin and bear it.  Tarantino reveals that the film Judy is auditioning for is "the greatest romantic, African-American film ever made. Directed by me, of course."  However, Judy grows suspicious of the audition, when "Q.T.", tells her that "Wesley, Denzel, Fish (burne) . . ."  are signed up to play supporting roles.  Judy seems to keep her cool until it is requested that she remove her blouse so "Q.T." and his assistant can see her breasts.  She reluctantly complies, but not for long.  She walks out on the audition.

Her agent (John Turturro) is furious.  Having worked hard to get Judy her audition with such a prestigious director, he quickly and angrily drops her from his roster of clients.  Her melodramatic acting coach (Susan Batson) is also extremely displeased. When Judy tells her why she did not go through with the audition, the acting coach still does not see any reason why Judy should have walked out.  This, topped with the fact that Judy has not paid her rent in a very long while, forces her to drop Judy from her roster of clients as well.
 waiting tables at a club, etc.  At one point she agrees to be an extra on a movie set.  However, it is cold and unpleasant, as is the director. Judy is sick with a cold, and still trying to secure work.  While reading a newspaper, she sees an ad for a "friendly phone line", as well as one with the headline, "mo money, mo money, mo money".  She circles them both.

This brings her to a meeting at a phone sex office.  She meets the boss (Jenifer Lewis), who seems to be an assertive but friendly woman.  The two click and the "audition" goes over just fine.  The boss, now known as Lil, says that although she cant promise anything, shell put in the good word for Judy.  Judy also goes to another meeting, but the boss there wants her to do more visually related work, so she declines.  Then, she attends another meeting, at a strip club/phone sex line with a relaxed boss (Madonna (entertainer)|Madonna).  She would take the job, but the content allowed for on-line discussion is a bit too heavy for her taste.  She decides to stick to her original application with Lil.

We now look in on Judys dissolved relationship with her kleptomaniac ex-husband (Isaiah Washington), as well as her relationship with her baseball-memorabilia obsessed best friend, Jimmy (Spike Lee).  Jimmy is reliant on his collection for money, but until it accumulates enough age to be worth money, he gets his rent money from Judy.

Throughout the film, the phone sex line, having been secured at Lils company, begins to take its mental toll on the newly christened Girl 6.  She trusts her clients too much at times, and is therefore tricked repeatedly.  She even agrees to meet one of her callers at one point, but he never shows, leaving her on a bench alone.  It is visible to everybody, especially Lil and Jimmy, that Judy is having a breakdown.  The movie culminates in a dark sequence in which she enters a snuff fantasy with a caller (Michael Imperioli).  It becomes serious when she discovers the caller knows where she lives.  Running upstairs for shelter with Jimmy, she decides that it is time to leave the phone sex career behind and get her acting career in motion.  Finally reconciling with her ex, she decides to move to Los Angeles|L.A.

Judy attends another audition in which she experiences the same problem as the one with "Q.T."  She again walks out.  However, it is clear that Girl 6 has reclaimed her dignity, and will find work sooner or later.

==Cast==
*Theresa Randle as Girl 6 
*Isaiah Washington as Shoplifter 
*Spike Lee as Jimmy 
*Jenifer Lewis as Boss #1 - Lil 
*Debi Mazar - Girl #39 
*Peter Berg as Caller #1 - Bob 
*Michael Imperioli as Scary Caller #30 
*Dina Pearlman as Girl #19 
*Maggie Rush as Girl #42 
*Desi Moreno as Girl #4 
*Kristen Wilson as Salesgirl #1 
*K Funk as Salesgirl #2 (as k funk) 
*Debra Wilson as Salesgirl #3 
*Naomi Campbell as Girl #75 
*Gretchen Mol as Girl #12 
*Shari Freels as Girl #29 - Punkster 
*Richard Belzer as Caller #4 - Beach 
*Larry Pine as Caller #33 - Wall Street  Coati Mundi as Caller #8 - Martin 
*Delilah Cotto as Caller #8 - Christine 
*Anthony Nocerino as Caller #6  Tom Byrd III as Caller #18
*Bray Poor as Caller #14 
*Joseph Lyle Taylor as Caller #3 / Caller #16  Madonna as Boss #3 
*Arthur J. Nascarella as Boss #2 - Male in Office 
*John Turturro as Murray - the Agent 
*Quentin Tarantino as Director #1 - NY 
*Ron Silver as Director #2 - LA 
*Mica Hughes as Directors Assistant 
*Leonard L. Thomas as Co-Agent  Joie Susannah Lee as Switchboard Operator
*Rolonda Watts as Reporter Nita 
*Carol Jenkins as Newscaster Carol  Jim Jensen as Newscaster Jim 
*Halle Berry as Herself 
*Jacqueline McAllister as Angela    
*Novella Nelson as Angelas Aunt 
*Billie Neal as Angelas Mother 
*Susan Batson as Acting Coach 
*Ranjit Chowdhry as Indian Shopkeeper 
*Rita Wolf as Wife of Indian Shopkeeper 
*Andrea Navedo as Phone Girl 
*Lesley-Camille Troy as Phone Girl 
*Michele Kelly as Phone Girl 
*Yohan Lim as Korean Deli Owner 
*Anne Ok as Wife of Korean Deli Owner 
*John Cameron Mitchell as Rob 
*Keith Randolph Smith as 2nd A.D. 
*Nelson Vasquez as Ronnie - the Guard 
*Al Palagonia as Man Mistaken for Bob Regular 
*Mekhi Phifer as Himself (cameo) (uncredited)

==Reception==
Girl 6 earned mostly mixed-to-negative reviews during its release and currently holds a 33% rating on Rotten Tomatoes based on 33 reviews.  

===Box office===
The film was not a box office success. 

==Home media== Malcolm X and She Hate Me.

==See also==
* Girl 6 (album)|Girl 6 (album)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 