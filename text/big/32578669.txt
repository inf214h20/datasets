Ek Baar Phir
{{Infobox film
| name           = Ek Baar Phir
| image          = Ek-Baar-Phir-1980.jpg
| image size     = DVD cover
| caption        =
| director       = Vinod Pande
| producer       = Vinod Pande
| writer         =
| screenplay     =
| story          = Vinod Pande
| dialogue       =
| narrator       =
| starring       = Suresh Oberoi, Deepti Naval, Saeed Jaffrey, Chitto Chopra, Pradeep Verma, Vinod Pande
| opentheme      =
| music          = Raghunath Seth Vinod Pande (lyrics)
| cinematography = Nadeem Khan
| editing        = Kant Pan, Pradeep Roy
| studio         =
| distributor    =
| released       =     
| runtime        = 147 min  
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}

Ek Baar Phir ( ;   film produced and directed by Vinod Pande. This offbeat social drama casts Suresh Oberoi and Deepti Naval as lead pair with Saeed Jaffrey, Chitto Chopra, Pradeep Verma, Vinod Pande in support cast.

The story is about the incompatibility between a film star husband and a traditional wife resulting in extra-marital relationship.

==Plot==
Kalpana (Deepti Naval), a middle class girl, is excited about her marriage with a film star Mahendar Kumar (Suresh Oberoi). The star is busy with his profession and is a flirt. Kalpana learns this after marriage and finds Mahendar has no time to spend with her. Both happen to visit London on Mahendars project. There she meets a young student Vimal (Pradeep Verma), studying arts at London University, who makes art works of the people around and exhibits them. He requests Kalpana for a sketch, which she obliges. They meet very often and fall in love with each other. With him, Kalpana sees the new phase and joy of life, which she has not seen earlier. They get intimate and Kalpana finds herself guilty of this. She stops meeting Vimal for some time. One day she finds a letter from Vimal and replies him. She starts meeting him again and finds herself swaying between Mahendar and Vimal. At this juncture, she decides to break away from Mahendar and joins Vimal to lead a joyful life. The rest story shows the turn of events thereafter, in the lives of Mahendar, Kalpana and Vimal.

==Cast==
*Suresh Oberoi  as   Mahender Kumar
*Deepti Naval  as   Kalpana Kumar
*Saeed Jaffrey  as  Saeed
*Pradeep Verma
*Avtar Singh
*Chitto Chopra
*Collin Alexander
*Dilip
*Kalpana Pandya
*Kinsan
*Krishan Gould
*Leena Patel
*Mansur
*Maria
*Meenu Budhwar
*Mohammed
*Paravati Meharaj
*Prem Prakash
*Salim Shaikh
*Vinod Gurani
*Vinod Pande
*Wasim Siddiqui
*Yolande
*Zubair Zalaria

==Crew==
*Direction &ndash; Vinod Pande
*Story &ndash; Vinod Pande
*Production &ndash; Vinod Pande
*Editing &ndash; Kant Pan, Pradeep Roy
*Cinematography &ndash; Nadeem Khan
*Art Direction &ndash; Daniel De Waal
*Music Direction &ndash; Raghunath Seth
*Lyrics &ndash; Vinod Pande
*Playback &ndash; Anuradha Paudwal, Bhupinder Singh (musician)|Bhupinder, Kittu, Suresh Wadkar

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Vinod Pande
| all_music       = Raghunath Seth

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Baby I Have A Crush On You
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Kittu
| length1         = 4:35

| title2          = Bambai Se Aaya Hoon
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Suresh Wadkar
| length2         = 5:45

| title3          = Jeevan Ek Sauda Hai
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Anuradha Paudwal
| length3         = 6:35

| title4          = Jane Yeh Mujhko Kya Ho Raha Hai
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Bhupinder
| length4         = 7:35

| title5          = Yeh Paude Yeh Patte Yeh Phool
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Anuradha Paudwal, Bhupinder
| length5         = 4:55

| title6          = Orchestral Music
| note6           = Ek Baar Phir
| writer6         =
| lyrics6         =
| music6          =
| extra6          =
| length6         = 4:40
}}

==References==
 

==External links==
* 

 
 
 