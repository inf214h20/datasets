The Golden Claw
{{Infobox film
| name           = The Golden Claw
| image          =
| caption        =
| director       = Reginald Barker
| producer       = Thomas H. Ince (Triangle-Ince)
| writer         = C. Gardner Sullivan Frank Mills
| cinematography =
| editing        =
| distributor    = Kay-Bee Pictures
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}}
 reel drama released by Kay-Bee Pictures and starred Bessie Barriscale.

==Plot==
Bessie Barriscale played the role of Lillian Henry, “a beautiful young girl who enjoys all the tinsel and glitter of society life and is willing to marry simply for money and all the luxuries and pleasures that its possession will bring.”     One reviewer described Barriscale’s character as a “young girl” who “decides that she will practically sell herself to a youth of wealth.”   Frank Mills plays Bert Werden, “the young unspoiled man, who was determined to give his wife her fill of his continual search for the golden fleece.” 

Following their marriage, Bert Werden loses his fortune, and his wife goads him to restore her to wealth and luxury again.  He works day and night and bets on the stock market. His health is almost wrecked and he becomes “money mad” and neglects his wife.  Eventually, both characters learn that other things are more important than money.  Barriscale’s character wins her husband back and gives him $50,000, a sum he had given her at the start of their marriage, to give him a new start.   

==Cast==
*Bessie Barriscale ..... Lillian Henry Frank Mills ..... Bert Werden 
*Robert Dunbar ..... Alec Werden 
*Wedgwood Nowell ..... Graham Henderson 
*Truly Shattuck ..... Lucy Hillary

==Critical reception==
One reviewer said of the film: "A motion picture story of the highest class, of vital subject, original development and striking characterization, The Golden Claw is one of those rare products calculated to interest many millions of intelligent people and bring into the fold those other millions of intelligent people who are repelled by poor examples of new art."     The same reviewer praised the film’s direct approach in dealing with its subject matter: "The Golden Claw attacks the problem of money madness with great simplicity and directness so far as the line of action is concerned, but the admirable solution is so subtle that only fine characterization and such powerful interpretation as that given by Bessie Barriscale and Frank Mills."  
Another reviewer noted: "As a bitter satire on the futility of piling up money simply for the sake of possessing it, the film is declared to be exceptionally strong." 

==References==
 

==External links==
*  

 
 
 
 