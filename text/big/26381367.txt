Du Cote D'Orouet
{{Infobox Film
| name           = Du Cote DOrouët
| image          = 
| image_size     = 
| caption        = 
| director       = Jacques Rozier
| producer       = 
| writer         = Jacques Rozier Alain Raygot
| narrator       = 
| starring       = Danièle Croisy Françoise Guégan Caroline Cartier
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1973
| runtime        = 150 minutes
| country        = France
| language       = French 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Du Cote DOrouët is a 1973 French film directed by Jacques Rozier. The film is about three young girls that have their summer vacation in a villa on the beaches of Orouët. It was first screened at the Cannes Film Festival in 1971.

==Cast==
*Danièle Croisy as Joëlle
*Françoise Guégan as Kareen
*Caroline Cartier as Caroline
*Bernard Menez as Gilbert
*Patrick Verde as Patrick

==External links==
*  


 
 

 