Darfur (film)
{{Infobox film
| name           = Darfur
| alt            =  
| image          = Darfur FilmPoster.jpeg
| caption        = DVD cover
| director       = Uwe Boll
| producer       = Uwe Boll Dan Clarke
| writer         = Uwe Boll Chris Roland
| starring       = Edward Furlong Billy Zane Kristanna Loken
| music          = Jessica de Rooij
| cinematography = Mathias Neumann
| editing        = Thomas Sabinski
| studio         = 
| distributor    = Phase 4 Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} conflict in Darfur, starring David OHara, Kristanna Loken, Billy Zane and Edward Furlong. The film was also released as Attack On Darfur.

== Plot ==
The plot of Darfur revolves around six Western journalists who visit a small village in Darfur in western Sudan under the escort of a squad of troops of the African Union peacekeeping mission. When they learn the brutal state sponsored militia called the Janjaweed are heading towards the village, they are faced with an impossible decision: leave Sudan and report the atrocities to the world, or risk their own lives and stay in the hopes of averting a certain slaughter.
 Black African men, women, and children. Despite their efforts to save some villagers, Captain Tobamke, Theo, and Freddie are all killed one by one in the subsequent shootout with the Janjaweed, but not after killing or wounding a few dozen of the savage milita. The surviving Janjaweed then burn the village to the ground and move on, presumably to continue their genocide rampage across the Darfur landscape.

The final scene shows the female member of the journalist team, Malin Lausberg, who had fled with most of the other reporters and AU soldiers during the Janjaweed attack, now return to the destroyed village the next day with a group of AU soldiers only to find everyone dead, including two of her colleagues. But she finds an infant that Freddie protected by hiding under Theos dead body as the sole survivor of the massacre. Malin takes the baby with her as she and the rest of the AU troops leave the destroyed village behind.

==Cast==
* Kristanna Loken as  Malin Lausberg
* David OHara as  Freddie Smith
* Noah Danby as Theo Schwartz
* Matt Frewer as Ted Duncan
* Hakeem Kae-Kazim as  Captain Jack Tobamke
* Sammy Sheik as Janjaweed Commander
* Billy Zane as Bob Jones
* Edward Furlong as Adrian Archer

== Development ==
Darfur was filmed outside of Cape Town, South Africa in February and March 2009. Boll himself describes the film as "something thats very shocking". Much of the dialogue is improvised by the actors and the film is shot mostly with handheld cameras to convey a sense of realism, similar to Bolls previous films Stoic (film)|Stoic and 1968 Tunnel Rats.   Eye for Film Andrew Robertson 

==Reception==
In September 2010 Darfur won the New York International Independent Film and Video Festival prize for the best international film.  
 John Pendergast and Amnesty International were both reported to be impressed with the film. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 