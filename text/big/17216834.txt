A Different Approach
 
{{Infobox film
| name           = A Different Approach
| image          = 
| alt            =  
| caption        = 
| director       = Fern Field
| producer       = {{Plainlist|
*Jim Belcher
*Fern Field}}
| writer         = Jim Belcher
| starring       = {{Plainlist|
*Melissa Sue Anderson
*Lucie Arnaz
*Stockard Channing
*Michael Keaton}}
| music          = Marvin Laird
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 21 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Different Approach is a 1978 film starring Michael Keaton. The film is about An all-star educational film about the positive side of hiring people with disabilities. A committee of government representatives sits and watch the film Michael Keatons characters assembled to sell companies on hiring the handicapped, which takes "a different approach" by combining several approaches—most of them suggested by Hollywood personalities. 

Segments include a Busby Berkeley-style dance number featuring people in wheelchairs, a phony TV commercial in which comedian Carl Ballantine touts handicapped workers as if he were a hard-sell used car salesman, and several more.  One of the committee members is played by All in the Family creator Norman Lear, affecting a heavy German accent. His character makes offhand remarks which imply that he is a former Nazi technocrat brought over after the war. The timid chairman of the committee is played by Sorrell Booke, "Boss Hogg" of the original "Dukes of Hazzard" television series. (No references available: description in this paragraph from first-hand viewing of the film twice at showings sponsored by government and corporate HR departments in the San Francisco Bay Area around 1979 and 1980.)

The film was nominated for an Academy Award for Best Live Action Short Film.

==External links==
* 

 
 

 