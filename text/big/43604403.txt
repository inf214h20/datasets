Man, Woman and Child (film)
{{Infobox film
| name           = Man, Woman and Child
| image          = Man, Woman and Child poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Dick Richards
| producer       = Stanley Beck Elliott Kastner Elmo Williams
| screenplay     = Erich Segal David Zelag Goodman
| based on       =  
| starring = {{plainlist|
* Martin Sheen
* Blythe Danner
* Craig T. Nelson
* David Hemmings
}}
| music          = Georges Delerue Buddy Kaye
| cinematography = Richard H. Kline
| editing        = David Bretherton
| studio         = Gaylord Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,314,561 
}}
 book of the same name. The film stars Martin Sheen, Blythe Danner, Craig T. Nelson, David Hemmings, Nathalie Nell and Maureen Anderman. The film was released on April 1, 1983, by Paramount Pictures.

==Plot==
 

== Cast ==
*Martin Sheen as Robert Beckwith
*Blythe Danner as Sheila Beckwith
*Craig T. Nelson as Bernie Ackerman
*David Hemmings as Gavin Wilson
*Nathalie Nell as Nicole Guerin
*Maureen Anderman as Margo
*Sebastian Dungan as Jean-Claude Guerin
*Arlene McIntyre as Jessica Beckwith
*Melissa Francis as	Paula Beckwith 
*Billy Jayne as Davey Ackerman 
*Ruth Silveira as Nancy Ackerman
*Jacques François as Louis

==Reception==
The film opened to number 13 in its first weekend, with $802,702. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 