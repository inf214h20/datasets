The Hanging Tree
 
{{Infobox film name            = The Hanging Tree image           = Poster of the movie The Hanging Tree.jpg director        = Delmer Daves producer  Richard Shepherd writer          = Dorothy M. Johnson (novel) Wendell Mayes Halsted Welles starring        = Gary Cooper Maria Schell Karl Malden music           = Jerry Livingston (title song) Max Steiner cinematography  = Ted D. McCord editing         = Owen Marks distributor     = Warner Bros. released        =   runtime         = 106 min. country         = United States language  English
|budget          = gross           = $2.2 million (est. US/ Canada rentals) 
|}}
 lynch mob, then learns of the mans past and tries to manipulate him.

The film was the first one for Gary Coopers Baroda Productions company. This marked the first film of Scott. He and Malden later teamed for 1970s Patton (film)|Patton, for which Scott won an Academy Award.

==Plot==
Joseph Frail—doctor, gambler, gunslinger—rides into town looking to set up a doctors office. He passes by the "hanging tree," an old oak with a thick branch over which has been slung a rope with a frayed end, presumably a former noose.
 temporary servitude as a method of payment. Frail houses and feeds Rune while covering his identity as the wounded sluice robber.

A stagecoach is robbed and overturned, killing the driver and a male passenger.  But the daughter of the male passenger is believed to have survived.  A search party is formed, and sole survivor Elizabeth Mahler is found by the town opportunist, "Frenchy."

Crippled by burns, blindness and dehydration received from overexposure, very attractive Elizabeth is moved into a house next to the doctors house to begin her recovery. The placement causes much chagrin among the towns righteous women, who believe that Elizabeth may be paying for her medical care through illicit behavior.

Frenchy sneaks in under the guise of trying to strike a business deal with Elizabeth, but instead tries to forcefully kiss her. Frail witnesses the aggression and chases Frenchy back to town. Frail beats him up and threatens to kill him.  There are many witnesses, including a mad faith healer, Dr. Grubb, who sees Frails medical practice as a threat.
 prospector so that she can pay off Frail and get out from under his control.

She teams up with Rune and Frenchy, who plan to buy a claim and set up a sluice.  To get money, she pawns a family heirloom necklace. It is worthless, but Frail secretly tells the storekeeper to loan her however much money she needs.  Thus Frail secretly continues to control her.

She finds out and asks Frail why he couldnt respond to her affection. He reveals that his wife had an affair with his own brother.  He found them together, both dead, an apparent murder-suicide. In a rage, he burned down their house with their bodies in it.  He tells Elizabeth he is "not allowed to forget."
 glory hole" of gold under a large tree stump.  They ride into town as heroes, tossing a few pieces of gold to the townsfolk.  The gaiety quickly turns into a riot of the lawless town members led by Dr. Grubb.  While the lawful citizens of the town are engaged in fighting fires set by Grubb, Frenchy takes advantage of the commotion to make advances on Elizabeth. Her disinterest sparks a brutal physical assault as he attempts to rape her. Frail again catches Frenchy just in time. A fist fight ensues and Frenchy pulls his pistol and shoots, attempting to kill Frail, but misses. Frail responds in self-defense killing Frenchy in front of the rioting townsfolk.

Seeing his opportunity to remove his "competition", Grubb incites the mob to lynch Frail.  They carry him to the hanging tree, tie his hands, and stand him up in a wagon bed, the rope around his neck.  Rune and Elizabeth rush in carrying their gold and the deed to their claim.  Elizabeth offers everything to the townsfolk if they will let Frail live.  As the mob members now fight each other to grab the gold and claim document, the lynch party disperses.

Elizabeth now feels she has finally repaid Frail in full. Rune slips the noose off and Elizabeth turns to walk away downhill. "Doc" Frail calls out her name. She turns back, and steps to the end of the wagon. He kneels down, cups her chin with both hands, and they touch foreheads, while the balladeers haunting refrain plays in the background.

==Cast==
* Gary Cooper as Doc Frail
* Karl Malden as "Frenchy" Plante
* Maria Schell as Elizabeth Mahler
* George C. Scott as Grubb
* Karl Swenson as Tom Flaunce
* Ben Piazza as Rune

==Production==
Principal photography was done on location in the  , located in the mountains west of Yakima, Washington. The movie scenes during the opening movie credits and movie title, where Gary Cooper rides alongside the river on horseback with a pack horse in tow, were filmed about mid-June in 1958, just northeast of Goose Prairie, Washington, along the north bank of the Bumping River. The fictional small gold mining town of Skull Creek was a temporary movie set constructed along the south side of Little Rattlesnake Creek by its confluence with Rattlesnake Creek, just southwest of Nile, Washington. 

==Soundtrack== the title song that was nominated for best song at the 32nd Annual Academy Awards and the 1960 Golden Laurel Award for Best Song.  The text is a short reference to the films story. It was also released on the reissue of the album Gunfighter Ballads and Trail Songs (1959) by Marty Robbins who performed this song in the opening credits of this film. A known cover-version is by Frankie Laine who performed this song at the 32nd Academy Awards.

The films score was composed by Max Steiner.
 The Hanging Tree 
Lyrics by Mack David 
Music by Jerry Livingston 
Vocal by Marty Robbins

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 