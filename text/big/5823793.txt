Ripley's Game (film)
 
{{Infobox film
| name = Ripleys Game
| image = Ripleys game poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Liliana Cavani
| producer = Simon Bosanquet Ricardo Tozzi Ileen Maisel
| screenplay = Charles McKeown Liliana Cavani
| based on =  
| starring = John Malkovich Dougray Scott Ray Winstone Lena Headey
| music = Ennio Morricone
| cinematography = Alfio Contini Jon Harris
| studio = Mr. Mudd|mr. mudd
| distributor = 01 Distribuzione   Entertainment Film Distributors   Fine Line Features  
| released =  
| runtime = 110 minutes  
| country = Italy United Kingdom United States
| language = English
| budget =  30 million
}} thriller film the 1974 novel of the same name, the third in Patricia Highsmiths "Tom Ripley|Ripliad", a series of books chronicling the murderous adventures of con artist Tom Ripley. John Malkovich stars as Ripley, opposite Dougray Scott and Ray Winstone. Highsmiths novel was previously adapted in 1977 as The American Friend by director Wim Wenders, starring Dennis Hopper and Bruno Ganz.

==Plot==
Tom Ripley is involved in an art scam in Berlin, partnered with Reeves, a thuggish British gangster whom he orders to remain out on the street as the deal takes place. A violent argument breaks out in which Ripley kills one of his "customers". He gives the money to Reeves but keeps the artwork for himself, curtly informing Reeves that their partnership is over.

Three years later, Ripley is extremely wealthy, living in a lush villa in Veneto with his wife Luisa, a beautiful harpsichordist. Invited by a neighbor to a party, Ripley has a pleasant time until he overhears the host, Jonathan Trevanny, insulting his taste and making a guarded reference to his questionable past. Ripley briefly confronts him, then sullenly leaves the party.

Reeves resurfaces, much to Ripleys annoyance, asking him to eliminate a rival mobster. Remembering the slight, Ripley recommends that an amateur be hired to do it &ndash; Trevanny, a law-abiding art framer who is dying of leukemia. Reeves offers a bewildered Trevanny the job. He turns it down at first, but cant resist the money, which he could leave to his wife, Sarah, and son, Matthew, upon his death.

Trevanny goes through with the job, a hit in Berlin, which he assumes will be a one-time-only assignment. Reeves has other ideas, however; he blackmails Trevanny into taking on another assassination, this time a much more complicated one on a train.

Trevanny panics and freezes up on the train, but Ripley intervenes in the nick of time. After the two of them dispatch three hoodlums in the toilet, Trevanny forms an uneasy friendship with Ripley and returns home. He then vainly attempts to persuade Sarah that the money he suddenly possesses is the result of visiting a hospital in Berlin and volunteering for an experimental drug trial.

The mobsters associates come to Italy seeking revenge. They storm the villa and kill Reeves, leaving his body in the boot of their car. Ripley has set traps for them, however, and terminates each, with Trevannys increasingly eager assistance.

Trevanny comes home to find two more thugs holding his wife captive. Ripley spots the killers car outside in the bushes and doubles back in time to save the day, but in the end Trevanny sacrifices himself to save Ripley from a wounded assassin. Genuinely puzzled by Trevannys selflessness, Ripley tries to give Sarah her husbands share of the blood money, but she only spits in his face in reply. That night, Ripley attends Luisas concert as if nothing has happened, but smiles briefly at the memory of Trevannys bravery.

==Cast==
* John Malkovich as Tom Ripley
* Dougray Scott as Jonathan Trevanny
* Ray Winstone as Reeves
* Lena Headey as Sarah Trevanny
* Chiara Caselli as Luisa Harari
* Sam Blitz as Matthew Trevanny
* Paolo Paoloni as Franco
* Evelina Meghnagi as Maria
* Lutz Winde as Ernst
* Wilfred Xander as Belinsky

==Reception== The Talented Mr. Ripley, Ripleys Game) and Malkovich "precisely the Tom Ripley I imagine when I read the novels," praising what he felt to be "one of   most brilliant and insidious performances."  Ebert criticized the decision not to release the film theatrically in North America, writing: "The failure to open it theatrically was a shameful blunder." 
 The Talented Mr. Ripley. But Malkovich owns the role. He plays it for keeps."  David Rooney of Variety (magazine)|Variety wrote, "Malkovichs elegantly malicious performance gives Ripleys Game a magnetic center, complemented by Liliana Cavanis efficient direction and an enjoyable retro feel that recalls the British Cold War thrillers of the 1960s. Despite some pedestrian plotting and a final act that could be tighter, this is suspenseful adult entertainment that should find a receptive audience." 

Other critics were less favorable, such as Peter Bradshaw of The Guardian, who gave the film two stars out of five.  Some critics compared the film unfavorably to Wim Wenders 1977 adaptation, The American Friend. Nathan Rabin of The Onion s The A.V. Club|A.V. Club remarked, "Ripleys Game fatally lacks the squirmy, desperate humanity that made Wenders take on the same material so hauntingly tragic. Like Malkovichs suavely generic international criminal, its all craft and no soul, with complexity and depth functioning as collateral damage for its slick thriller mechanics."  Neil Youngs Film Lounge, giving Ripleys Game a score of 6 out of 10, called the film a "largely uninspired" adaptation by a "pedestrian" director, calling The American Friend "brilliant" by comparison, feeling that "any viewer who is a fan of Highsmith and/or The American Friend will have major problems with this version." 

==References==
 

==External links==
 
*  
*  
*  
*  
*  , filmbrain.com comparison of The American Friend and Ripleys Game

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 