The Lady Craved Excitement
 
 
 
{{Infobox film
| name           = The Lady Craved Excitement
| image          = "The_Lady_Craved_Excitement"_(1950).jpg
| image_size     =
| caption        =
| director       = Godfrey Grayson Francis Searle
| producer       = Anthony Hinds
| writer         = John Gilling Francis Searle
|  based on       = a BBC radio serial by Edward J. Mason Sidney James Andrew Keir Frank Spencer
| cinematography = Walter J. Harvey
| editing        = John Ferris
| studio         = Hammer Films
| distributor    = Exclusive Films (UK)
| released       = August 1950 (UK)
| runtime        = 60 mins
| country        = UK
| language       = English
| budget         =
| gross          =
}} Bray Studios. 

==Plot==
Cabaret artists Pat and Johnnys careers are hampered by Pats craving for excitement. She leads them into a number of dangerous situations, but also help to uncover a conspiracy to smuggle valuable works of art out of the country. 

==Cast==
* Hy Hazell as Pat
* Michael Medwin as Johnny Sidney James as Carlo
* Thelma Grigg as Julia Lafaine
* Andrew Keir as Septimus K. Peterson Danny Green as Boris
* John Longden as Inspector James Ian Wilson as Mugsy Barbara Hamilton as 1st Chorus Girl
* Jasmine Dee as 2nd Chorus Girl
* Gordon Mulholland as Lunatic

==Critical reception==
Britmovie wrote, "barely watchable by today’s standards (and probably not much more tolerable at the time), it nevertheless remains of passing interest for its cast, which includes Michael Medwin, Sid James and Andrew Keir, all of them then in the early stages of what would prove to be lengthy and successful careers."  

==References==
 

 

 

 
 
 
 
 
 
 


 
 