Apothecary (film)
{{Infobox film
| name           = Apothecary
| image          = Apothecary.jpg
| caption        = First look poster
| director       = Madhav Ramadasan
| producer       = George Mathew  Baby Mathew
| screenplay     = Hemanth Kumar   Madhav Ramadasan Abhirami Meera Nandan
| music          = Sheikh Ellahi
| cinematography = Hari Nair
| editing        = K. Srinivas
| studio         = Arambankudiyil Cinemas
| distributor    = Time Ads Release
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Abhirami and Meera Nandan.

==Plot==
Dr. Vijay Nambiar ( Suresh Gopi ) is a skilled and well-known neurosurgeon. He lives with his wife Dr. Nalini Nambiar ( Abhirami ) and their two kids. They both work at the Apothecary Hospital. The film begins with Nambiyar being admitted to the hospital because of a motor vehicle accident. He has suffered a brain injury and his chance of survival seems slim. Subin Joseph(Jayasurya) a patient of Nambiyar, comes to visit him. Subin recounts his experience at the hospital under Nambiyars care. The hospital is revealed to be run greedily, with patients being subjected to expensive and unnecessary tests and medicines. Nambiyar is a doctor with a good reputation, but is forced by the management to run clinical trials for new drugs. He realizes that such trials are necessary for progress, but the lack of ethical protocol and the side effects of such drugs turn the lives of the patients into a living hell. This dilemma consumes him, causing him to have hallucinations of his clinical trial patients. His soul is locked in a surreal struggle between the good and the bad actions of his work. His accident was a result of such an episode, where he falls to the road in a fit of panic. Finally, he is redeemed with the realization that his work has saved thousands, compared to the few lives destroyed by his actions. Should he live, he would go on to save even more lives. Nambiyar returns to life a changed man. He confronts the hospital board about their actions. He reminds them of the word Apothecary which was used years back, to refer to doctors who treated village folk with their medicines. Stating that the hospital simply retains that name, he leaves the hospital to set up a charitable institution. His wife and several of the hospital staff and patients accompany him on his new journey.

==Cast==
* Suresh Gopi as Dr. Vijay Nambiar
* Jayasurya as Subin Joseph
* Asif Ali as Prathapan Abhirami as Dr.Nalini Nambiar, wife of Dr. Vijay
* Meera Nandan as Daisy, lover of Subin
* Raghavan Nair as Dr. Shankar Vasudev
*Indrans as Joseph, father of Subin
* Kavitha Nair as Sabira Usman Thampy Antony as Dr. Ali Ahamed
* Neeraj Madhav as Shinoy Joseph, brother of Subin
* Lishoy as Dr. Peethambaran
* Malavika as Meenu
* Jayaraj Warrier as Varkkichan
* Arun as Dr. Raheem
* Seema G. Nair as Clara, mother of Subin
* Jayan Cherthala as Sajan Xavier
* Mohanakrishnan as Hameed
* Dr. George Mathew as Dr. Mathew Koshi
* Shivakumar as Dr. MK Nambiar, father of Dr. Vijay
* Preman as Meenus father
* Sajad Brite as Usman, husband of Sabira
* Neeraja as Dr. Nimisha
* Bindu Krishna as Dr. Maria

== Reception ==
Apothecary got generally positive reviews from critics. Paresh C Palicha of Rediff movies praised Suresh Gopis performance and also noted that the film could have been better.  Nicy V.P. of International Business Times wrote that Apothecary is a must watch. She also praised performance of Jayasurya. 

==Awards== National Film Awards
* Best Actor - Jayasurya : Nominated  

==References==
 

== External links ==
*  


 
 
 