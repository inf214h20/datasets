The Dark Past
{{Infobox film
| name           = The Dark Past
| image          = The Dark Past movie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Rudolph Maté
| producer       = Buddy Adler
| screenplay     = Philip MacDonald Michael Blankfort Albert Duffy
| writer         = Malvin Wald Oscar Saul
| based on       =   William Holden Nina Foch Lee J. Cobb
| music          = George Duning
| cinematography = Joseph Walker
| editing        = Viola Lawrence
| distributor    = Columbia Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Blind Alley (1939), also released by Columbia, and based on a play by American playwright James Warwick. 

==Plot==
A psychoanalyst and his young family and some friends are taken hostage by a gang led by an escaped killer, Al Walker. The doctor gets the killer to talk to him in an attempt to find out the killers unconscious motivation for his evil ways.

Walker relates a dramatic dream hes been having since childhood. Eventually, his crimes are traced back to his childhood and lack of parental guidance, and by the end of the night the doctor has calmed the killers murderous rage and prevented any further killings.

==Cast== William Holden as Al Walker
* Nina Foch as Betty
* Lee J. Cobb as Dr. Andrew Collins
* Adele Jergens as Laura Stevens Stephen Dunne as Owen Talbot
* Lois Maxwell as Ruth Collins
* Berry Kroeger as Mike
* Steven Geray as Prof. Fred Linder
* Wilton Graff as Frank Stevens
* Kathryn Card as Nora

==Reception==

===Critical response===
When the film was released the film critic at The New York Times gave the film a positive review writing, "William Holden is excellent as the dream-shackled gunman, who is at once ruthless, nervous and explosively dangerous but who grudgingly complies with the doctors screwball tactics. As counterpoint is Lee J. Cobbs equally fine portrait of the unflustered scientist who is dedicated to curing people not killing them. And, Nina Foch does a competently restrained job as the gangsters moll, who learns her man is suffering from an Oedipus complex. The doctors house guests, including Steven Geray, Adele Jergens and Wilton Graff, and their captors, especially Berry Kroeger, give unobtrusive but neat characterizations. Neat, too, is the word for this small but well-made Christmas package." 

More recently, film critic Dennis Schwartz gave the film a mixed review stating the film was well acted, but called the film, "... pure Hollywood hokum." 

==References==
 

==External links==
*  
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*   by Robert Osborne

 

 
 
 
 
 
 
 
 
 
 
 