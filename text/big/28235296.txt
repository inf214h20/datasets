Vacanze di Natale
 
{{Infobox film
| name           = Vacanze di Natale
| image          = Vacanze di Natale.jpg
| caption        = DVD cover
| director       = Carlo Vanzina
| producer       = Aurelio De Laurentiis Luigi De Laurentiis
| writer         = Carlo Vanzina Enrico Vanzina
| starring       = Jerry Calà, Claudio Amendola, Christian De Sica
| music          = Giorgio Calabrese
| cinematography = Claudio Cirillo
| editing        = Raimondo Crociani
| distributor    = Filmauro
| released       =  
| runtime        = 97 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Vacanze di Natale is a 1983 Italian comedy film directed by Carlo Vanzina. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Plot== Riccardo Garrone, while the second is made up of people extremely vulgar and rough to Rome contains the funny characters Claudio Amendola, Marilù Tolo and Mario Brega. The stories and adventures of the members of the two families intersect and give rise to hilarious misunderstandings, while a penniless pianist (Jerry Calà) fall in love again with his ex-girlfriend (Stefania Sandrelli). Roberto Covelli (De Sica) has an American girlfriend but worthy of much attention and persistently courted by Mario Marchetti (Amendola) which was initially became involved in a meeting with the girlfriend of a friend of his. The contact between the two families will be done through the acquaintance of Mario with a member of Covelli which, when seen in front of the rude common people of Rome are upset. After many adventures finished good and evil Samanthas girlfriend, Roberto, will notice the sad courting the mild Mario and give him what he wishes for one night before leaving for evers America. Meanwhile, the Covelli Family with astonishment and regret discovers that his young son Roberto is gay. One year after the holiday, families reunite in Sardinia during the summer and will create a new whirlwind of hilarious situations.

==Cast==
* Jerry Calà - Billo, il pianista del Night Club
* Christian De Sica - Roberto Covelli
* Karina Huff - Samantha
* Claudio Amendola - Mario Marchetti
* Antonella Interlenghi - Serenella Riccardo Garrone - Giovanni Covelli
* Rossella Como - Signora Covelli
* Guido Nicheli - Donatone
* Marco Urbinati - Luca Covelli
* Mario Brega - Arturo Marchetti
* Rossana Di Lorenzo - Erminia Marchetti
* Marilù Tolo - Grazia Tassoni
* Stefania Sandrelli - Ivana
* Roberto Della Casa - Cesare
* Paolo Baroni - Collosecco
* Licinia Lentini - Moira
* Moana Pozzi - Luana
* Clara Colosimo -  Farmacista

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 