Tehzeeb (2003 film)
{{Infobox film
| name = Tehzeeb
| image = TehzeebDVD.jpg
| caption = DVD cover
| director = Khalid Mohammed
| producer = Paresh Talpade
| writer = Khalid Mohammed Javed Siddiqui
| narrator = 
| starring = Shabana Azmi Urmila Matondkar Arjun Rampal Diya Mirza
| music = A. R. Rahman
| cinematography = Santosh Sivan
| editing = A. Sreekar Prasad
| distributor =
| released =  
| runtime = 143 min
| country = India Hindi
| budget =
| gross =
}}
Inspired by  : تہزیب,   film directed by Khalid Mohammed. It premiered on 21 November 2003. The film stars Shabana Azmi, Urmila Matondkar, Diya Mirza, Arjun Rampal and Rishi Kapoor in a special appearance. Urmila and Shabana were praised for their roles.

==Plot==
Anwar (Rishi Kapoor) and Rukhsana (Shabana Azmi) had been married for years. After the birth of their daughters Tehzeeb and Nazeen, Anwar goes into depression and commits suicide. Rukhsana is an ambitious and popular singer. After her husbands mysterious death, her elder daughter Tehzeeb (Urmila Matondkar) suspects her to be the cause of his departure. Despite the court declaring Rukhsana innocent, Tehzeeb still bears a grudge against her and accuses her of deserting her and Nazeen (Dia Mirza).

Years later, Tehzeeb has married Salim (Arjun Rampal) against her mothers wishes. Nazeen is mentally challenged, and Tehzeeb takes her under her custody. Tehzeeb lives happily with her husband and Nazeen, until Rukhsana decides to visit them and renew ties after five years. Both mother and daughter are happy about the upcoming visit, but the tension between them turns up eventually.

Many challenges and arguments arise because of Tehzeeb and Rukhsanas differences. But Tehzeeb and her mother have many good moments and Rukhsana grows close with both her daughters. Disaster then strikes when Nazeen shoots herself. Here the truth comes out that Anwar killed himself and Rukhsana wasnt responsible. Rukhsana wants to take Nazeen with her, but Tehzeeb doesnt agree. Salim convinces her and she finally gives her consent.

On the day that Rukhsana is to leave, she is sitting on the swing with her eyes closed. Tehzeeb goes up to her and apologizes, tells her she still loves her, but Rukhsana doesnt reply. Tehzeeb shakes her and Rukhsana falls over. In panic, Tehzeeb calls Salim. It is revealed that Rukhsana neglected her health and had a heart attack which she died of.

At the end of the movie, Tehzeeb is singing one of her mothers songs in her memory to a crowd while Salim and Nazeen watch her.

==Cast==
* Shabana Azmi as Rukhsana Jamal
* Urmila Matondkar as Tehzeeb Mirza
* Arjun Rampal as Salim Mirza
* Diya Mirza as Nazneen Jamal
* Rishi Kapoor as Anwar Jamal (special appearance)
* Namrata Shirodkar as Aloka
* Satish Kaushik as Kamal Choksi
* Rekha Rao as Maid
* Palak Jain
* Diana Hayden as Sheena Roy

==Awards==
Won
* Zee Cine Award Best Actor in a Supporting Role - Female – Shabana Azmi
Nominated
* Zee Cine Award Best Lyricist – Javed Akhtar
* Zee Cine Award Best Choreography – Remo
* Star Screen Award Best Supporting Actress – Shabana Azmi
* Filmfare Best Supporting Actress Award – Shabana Azmi

==Music==
{{Infobox album |  
 Name = Tehzeeb |
 Type = Soundtrack |
 Artist = A. R. Rahman |
 Cover = TehzeebCD.gif|
 Released =   13 October 2003 (India)|
 Recorded = Panchathan Record Inn | Feature film soundtrack |
 Length = |
 Label =  T-Series |
 Producer = A. R. Rahman |
 Reviews = |
 Last album = Kangalal Kaidhu Sei (2003) |
 This album = Tehzeeb (2003) |
 Next album = Udhaya (2003)|
}}

The soundtrack of the film contains six songs. The music is by the award-winning composer A.R. Rahman. The lyrics were written by Javed Akhtar but the Ghazal songs had lyrics adapted from traditional poems of famous poets. Rahman introduced a singer Sujata Bhattacharya, who later renamed herself as Madhushree. An additional song "Habibi" was featured in the movie but was not released in cassettes or CDs.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Lyrics
|-
|"Khoye Khoye Aankhen" Shaan
| Adapted from Shahd Azimabadi
|-
| "Meherbaan"
| Asha Bhosle, Sukhwinder Singh
| Javed Akhtar
|-
| "Na Shiqwa Hota"
| Madhushree
| Javed Akhtar
|-
| "Sabaq Aisa"
| Madhushree
| Adapted from Dagh Dehlvi
|-
| "Mujhpe Toofan Uthaye"
| Madhushree
| Adapted from Momin Khan Momin
|-
| "I Wanna Be Free"
| Anupama (singer)|Anupama, Mathangi
| Blaaze
|-
| "Na Shiqwa Hota"
| Vijaya
| Javed Akhtar
|-
| "Habibi"  (additional song) 
| Sunitha Sarathy
| Javed Akhtar
|}

==External links==
*  

 
 
 
 
 