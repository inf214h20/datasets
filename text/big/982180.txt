Robin Hood (1973 film)
 
{{Infobox film
| name        = Robin Hood 
| image       = Robinhood 1973 poster.png
| border      = ues
| caption     = Theatrical release poster
| director    = Wolfgang Reitherman 
| producer    = Wolfgang Reitherman
| screenplay  = Larry Clemmons Ken Anderson
| narrator    = Roger Miller
| starring    = {{plainlist|
* Phil Harris
* Andy Devine
* Peter Ustinov
* Terry-Thomas
* Brian Bedford
* Monica Evans
* Carole Shelley
* Pat Buttram
* Roger Miller }}
| music       = Score:   Floyd Huddleston Walt Disney Productions Buena Vista Distribution 
| released    =  
| runtime     = 83 minutes
| country     = United States
| language    = English
| budget      = $1.5 million
| gross       = $32 million 
}}
 Walt Disney Walt Disney anthropomorphic animals Prince John, and Robin Hood wins the hand of Maid Marian.

== Plot ==
 
 Prince John farthing from a young rabbit, Skippy, who had just received it as a birthday present. However, Robin Hood, disguised as a beggar, sneaks in and gives back some money to the family, as well as his hat and a bow to Skippy in honor of his birthday.

Skippy and his friends test out the bow, but Skippy fires an arrow into the grounds of Maid Marians castle. The children sneak inside, meeting Maid Marian and her attendant Lady Kluck. Maid Marian reveals she and Robin were childhood sweethearts but they have not seen one another for years. Meanwhile, Friar Tuck visits Robin and Little John, explaining that Prince John is hosting an archery tournament, and the winner will receive a kiss from Maid Marian. Robin decides to participate in the tournament disguised as a stork whilst Little John disguises himself as the Duke of Chutney to get near Prince John. Sir Hiss discovers Robins identity but is trapped in a barrel of ale by Friar Tuck and Alan-a-Dale. Robin wins the tournament, but Prince John exposes him and has him arrested for execution despite Maid Marians pleas. Little John threatens Prince John in order to release Robin, which leads to a fight between Prince Johns soldiers and the townsfolk, all of which escape to Sherwood Forest.

As Robin and Maid Marian fall in love again, the townsfolk mock Prince John, describing him as the "Phony King of England". Enraged by the insult, Prince John triples the taxes, imprisoning most of the townsfolk who cannot pay. The Sheriff visits Friar Tucks church to collect from the poor box, but when Friar Tuck protests, the Sheriff arrests him for high treason. Prince John plans to execute Friar Tuck, giving him the chance to capture Robin Hood when he comes to rescue Friar Tuck.

Robin and Little John sneak in, with Little John managing to free all of the prisoners whilst Robin steals Prince Johns taxes, but Sir Hiss awakens to find Robin fleeing. Chaos follows as Robin and the others try to escape to Sherwood Forest. The Sheriff corners Robin after he is forced to return to rescue Tagalong. During the chase, Prince Johns castle catches fire and forces a trapped Robin Hood to leap from a tower into the moat below. Little John and Skippy fear Robin is lost, but he surfaces safely after using a reed as a breathing tube, which drives Prince John into a blind rage.

Later, King Richard returns to England, placing his brother, Sir Hiss and the Sheriff under arrest and allows his niece Maid Marian to marry Robin Hood, turning the former outlaw into an in-law.

===Alternate ending===
The alternate ending (included in the "Most Wanted Edition" DVD) is a deleted version of the storys conclusion, primarily utilizing still images from Ken Andersons original storyboard drawings of the sequence. As Robin Hood leaps off of the castle and into the moat, he is wounded (presumably by one of the arrows shot into the water after him) and carried away to the church for safety. Prince John, enraged that he has once again been outwitted by Robin Hood, finds Little John leaving the church, and suspects the outlaw to be there as well. Sure enough, he finds Maid Marian tending to an unconscious Robin Hood, and draws a dagger to kill them both. Before Prince John can strike, however, he is stopped by his brother, King Richard, having returned from the Crusades. King Richard is appalled to find that Prince John has left his kingdom bleak and oppressed. Abiding his mothers wishes, King Richard decides he cannot banish Prince John from the kingdom, but does grant him severe punishment (which explained how Prince John, Sir Hiss, and the Sheriff ended up in the Royal Rock Pile). King Richard returns Nottingham to its former glory (before leaving for the Third Crusade), knights Robin Hood as Sir Robin of Locksley, and orders Friar Tuck to marry Robin Hood and Maid Marian.

A short finished scene from the planned original ending, featuring King Richard and revealing himself to vulture henchmen Nutsy and Trigger, appeared in the Ken Anderson episode of the 1980s Disney Channel documentary series Disney Family Album. This scene, at least in animated form, does not appear on the Most Wanted Edition DVD. 

== Cast ==
* Brian Bedford as Robin Hood (a fox)
* Monica Evans as Maid Marian (a fox)
* Phil Harris as Little John (a bear)
* Roger Miller as Alan-a-Dale (a rooster)
* Andy Devine as Friar Tuck (a badger) Prince John King Richard (lions)
* Terry-Thomas as Sir Hiss (a snake)
* Carole Shelley as Lady Kluck (a chicken)
* Pat Buttram as The Sheriff of Nottingham (a wolf)
* Ken Curtis and George Lindsey as Nutsy and Trigger, respectively (vultures) Sexton and his wife, respectively (church mice)
* Billy Whitaker, Dana Laurita and Dori Whitaker as Skippy, Sis, and Tagalong, respectively (rabbits) 
* Richie Sanders as Toby (a turtle)
* Barbara Luddy as Mother Rabbit
* Candy Candido as the Captain of the Guard (crocodile)
* J. Pat OMalley as Otto (a dog)
 country singer Roger Miller as the movies songwriter and narrator.

== Production ==
Initially, the studio considered a movie about Reynard the Fox. However, due to Walt Disneys concern that Reynard was an unsuitable choice for a hero, the project languished.  Ken Anderson used the central element of an animated fox in Robin Hood, however.

Robin Allan writes in his book Walt Disney and Europe that "Ken Anderson wept when he saw how his character concepts had been processed into stereotypes for the animation on Robin Hood."  According to Ward Kimball and Ollie Johnston, one such casualty was the concept of making the Sheriff of Nottingham a goat as an artistic experiment to try different animals for villains, only to be overruled by the director who wanted to keep to the villainous stereotype of a wolf instead. 

===Reuse of footage, sound, and voice actors=== Snow White The Jungle Book, and The Aristocats. 

== Release == Walt Disney Classics Collection), 1994, 1998 (as part of the Walt Disney Masterpiece Collection), and in 2000 (as part of the Walt Disney Gold Classic Collection) and has stayed in general release since then. It was first released on DVD in 2000 also as part of the Walt Disney Gold Classic Collection. The remastered "Most Wanted Edition" DVD ("Special Edition" in the UK) was released in 2006 and featured a deleted scene/alternate ending, as well as a 16:9 matted transfer to represent its original theatrical screen ratio. In 2013, the movie was released as a 40th Anniversary Edition Blu-ray/DVD/Digital Copy combo pack.

== Reception == Best Song The Way the film of the same name. 
 Top 10 Animated Films list. 

== Soundtrack ==
#"Whistle-Stop" written and sung by Roger Miller
#"Oo De Lally" written and sung by Roger Miller
#"Love (Disney song)|Love" written by Floyd Huddleston and George Bruns and sung by Nancy Adams
#"The Phony King of England" written by Johnny Mercer and sung by Phil Harris
#"The Phony King of England Reprise" sung by Terry-Thomas and Pat Buttram
#"Not In Nottingham" written and sung by Roger Miller
#"Love/Oo-De-Lally Reprise" sung by Chorus
 University of University of Wisconsin.

Although a full soundtrack to Robin Hood has never been released on compact disc in the US, a record of the film was made at the time of its release in 1973, which included its songs, score, narration, and dialogue. Both "Oo De Lally" and "Love" appear on the CD collection,  .
 Fantastic Mr. Fox. 

The song "Whistle-Stop" was used in the Super Bowl XLVIII commercial for T-Mobile. 
 Android which shows animals of different species playing together. 

==See also==
 
*Cultural depictions of John of England
 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 