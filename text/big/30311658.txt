Dark Nature
 
 
{{Infobox film
| name           = Dark Nature
| image          = Dark Nature.jpg
| caption        = 
| director       = Marc de Launay
| producer       = Marc de Launay Mondo Ghulam Eddie Harrison Tom Kelly Ted Mitchell
| writer         = Eddie Harrison
| starring       = Niall Greig Fulton Imogen Toner Vanya Eadie Joanna Miller James Bryce Jane Stabler Tom Carter Len McCaffer Callum Warren-Brooker Doreen McGillivray
| music          = 
| cinematography = Andrew Begg
| editing        = Bill Gill Joe Speirs
| studio         = Mandragora Productions
| distributor    =
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British thriller film written by Eddie Harrison, directed by Marc de Launay, and starring Niall Greig Fulton, Vanya Eadie and Imogen Toner. Shot on a small budget, it was filmed in the Scottish Highlands from September to October 2008 before receiving a limited cinematic showing in 2009. The film went on to be released on DVD and Blu-ray Disc|Blu-ray in the United Kingdom and the United States in May 2010.

== Plot ==
Jane and her family travel to Scotland to spend time with Janes mother. Unbeknownst to Jane and her family, their relatives have been murdered by a sinister force, and that dark and unnatural something remains, waiting for them. Despite her best efforts, the already established tensions with her daughter Chloe only manage to escalate, while her son and her lover have troubles of their own.

== Cast ==
* Vanya Eadie as Jane
* Imogen Toner as Chloe
* Niall Greig Fulton as McKenzie
* James Bryce as Jonathan
* Len McCaffer as Alex
* Joanna Miller as Emily
* Jane Stabler as Magda
* Tom Carter as Hayward
* Callum Warren-Brooker as Sean
* Dorren McGillivray as Mrs Petrie

== Reception ==
Dark Nature received generally mixed reviews from critics. Detroit Burns of film review website Sound on Sight applauded the film, stating "Its strong point is patience and a commitment to build up.  The use of the rich Scottish landscape and maniacal score create a stark and tense atmosphere, an atmosphere that the film creates very well."  He did, however, criticize the films pace and general stillness, stating that Dark Nature s "weakness is in the films hesitance to go one-step further" and that "its death scenes are rather anti-climatic and serve no purpose other than the elimination of a character from the rest of the script".     Light entertainment blog Pop Matters called the film "pristine" and "practically popping off the screen in its shot of digital dynamic", but attacked director Marc de Launay for "trying to redefine the category, taking much of the mystery out of your standard slice and dice".     Horror fansite Horrortalk applauded the film as a "quiet and deliberately paced look at the balance between man and nature" and considered the film to echo "the more existential cinema of the 1970s" and that it focuses on "internal struggle while adding horror elements". Horrortalk did, however, admit that the results of such elements were mixed.     

Gordon Sullivan of DVD Verdict moderately approved the film. Further to applauding its cinematography, Sullivan congratulated Marc de Launay as building a "dread-soaked atmosphere". He stated "The family relationship is the best part of the film by far", claiming "they display some very good chemistry". While considering the film to be itself confused as to "whether it wants to be a slice-and-dice thriller or an eco-horror film", Sullivan nonetheless considered that de Launay "shows himself to be a skilled young filmmaker with a good sense of humor and a knack for atmosphere". He went on to say "There arent a lot of jokes in the film, but I caught myself laughing at a few of the darker situations. The director balances the humor nicely with the dread, which comes through strongly in the foggy shores of the Scottish landscape".   

The film was further reviewed by Joseph Howell, Scottish newspaper Daily Record (Scotland)|Daily Record, film websites Mad Mad Movies and Horror Movie A Day             as well as other online critics.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 