Umbartha
{{Infobox film
| name           = Umbartha
| film name = उंबरठा
| image          = Umbartha.jpg
| image_size     =
| caption        = Umbartha VCD cover
| director       = Jabbar Patel
| producer       = Jabbar Patel D. V. Rao
| writer         = Vijay Tendulkar Vasant Dev  (dialogue) 
| based on       =  
| screenplay     = Vijay Tendulkar
| narrator       =
| starring       = Smita Patil Girish Karnad Shrikant Moghe Ashalata Wabgaonkar Kusum Kulkarni Purnima Ganu
| music          = Hridaynath Mangeshkar Ravindra Sathe (background score)
| cinematography = Rajan Kinagi
| editing        = N. S. Vaidya
| distributor    =
| released       =  
| runtime        =
| country        = India Marathi
| budget         =
| gross          =
}} Marathi film Best Feature Film in Marathi at the 29th National Film Awards for "a sincere cinematic statement on the theme of a woman seeking to establish her identity by pursuing a career, even at the risk of alienation from her family".      

The film is based on a Marathi novel "Beghar" (translation: Homeless) by Shanta Nisal and was also simultaneously made in Hindi as Subah with the same cast.

==Plot==
Sulabha Mahajan (played by Smita Patil) is a woman who dreams to step out of the four walls of the house, assume greater responsibility as a citizen and play an important role in shaping the society. She has passion to do something constructive for the abused, assaulted, neglected and traumatized womenfolk of the society she lives in. She gets a job offer as Superintendent of a Womens Reformatory Home in a remote town of Sangamwadi. The job offer raises objections from her lawyer husband Subhash (played by Girish Karnad) and conservative mother-in-law who refuse to understand her need to move to the town and work for rehabilitation of the women. But her sister-in-law supports her by offering help in looking after her young daughter Rani. Determined Sulabha then goes ahead with her dream job.
 MLA Bane has been regularly using the inmates of the home to satisfy his sexual needs. Two of the inmates decide to run away but are forcefully brought back to the home. They both commit suicide by burning themselves. Sulabha is then questioned by committee and newspapers for her improper control on the home. An administrative enquiry is set up against her. It is then that she decides to resign and give up all her work and return home.

When she returns to her home she is happily welcomed by her sister-in-law but not so much by her mother-in-law. She then comes to know that her husband Subhash has been involved with another woman in her absence. His betrayal changes her mind and she again sets off to follow her dream work.

==Cast==
* Smita Patil  as Sulabha  Mahajan
* Girish Karnad  as Advocate Subhash Mahajan
* Shrikant Moghe as Dr. Mohan Mahajan (Subhashs elder brother)
* Ashalata Wabgaonkar  as Maya Mahajan (Mohans wife)
* Kusum Kulkarni as Mrs.  Mahajan (Sulabhas mother-in-law)
* Purnima Ganu as Rani
* Radha Karnad as young Rani
* Satish Alekar as Walimbe (principal)
* Mukund Chitale as Gate-man
* Surekha Divakar  as  Farida
* Daya Dongre  as Chairman Sheela Samson
* Ravi Patwardhan as MLA Bane
* Vijay Joshi as Peon
* Jayamala Kale as Sugandha
* Sandhya Kale as Heera, Clerk / Typist
* Swaroopa Khopikar  as Utpala Joshi
* Manorama Wagle as Warden of Reformatory Home

==Soundtrack==
The soundtrack of the film is composed by Hridaynath Mangeshkar on lyrics written by Vasant Bapat and Suresh Bhat. All songs are sung by Lata Mangeshkar, except "Ganjalya Othas Majhya"  which is sung by Ravindra Sathe.
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no
| title1 = Sunya Sunya Maifilit Majhya| extra1 = Lata Mangeshkar | lyrics1 = | length1 = 04:22
| title2 = Ganjalya Onthas Majhya | extra2 = Ravindra Sathe | lyrics2 = | length2 = 04:47
| title3 = Chand Matala Matala| extra3 = Lata Mangeshkar | lyrics3 = | length3 = 05:45
| title4 = Gagan Sadan Tejomay| extra4 = Lata Mangeshkar | lyrics4 = | length4 = 04:37
}}

==Awards==
* 1982 - National Film Award for Best Feature Film in Marathi
* 1982 - Maharashtra State Film Awards - Best Film
* 1982 - Maharashtra State Film Awards - Best Director - Jabbar Patel
* 1982 - Maharashtra State Film Awards - Best Actress - Smita Patil

==References==
 

==External links==
*  

 
 

 
 