The Inconvenient Truth Behind Waiting for Superman
{{Infobox film
| name           = The Inconvenient Truth Behind Waiting for Superman
| image          = 
| caption        =
| director       = Julie Cavanagh Darren Marelli Norm Scott Mollie Bruhn Lisa Donlan
| writer         = Julie Cavanagh Darren Marelli Norm Scott Mollie Bruhn Lisa Donlan
| narrator       =
| music          = Glen Cavanagh
| cinematography = 
| editing        = Julie Cavanagh Darren Marelli Norm Scott Mollie Bruhn Lisa Donlan
| studio         = The Grassroots Education Movement
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| gross          = 
}}
The Inconvenient Truth Behind Waiting for Superman is a 2011 documentary produced by the Grassroots Education Movement.  The film was directed, filmed, and edited by Julie Cavanagh, Darren Marelli, Norm Scott, Mollie Bruhn, and Lisa Donlan.  It counters the position taken by the Davis Guggenheim 2010 documentary Waiting for "Superman".  The title is a play on words on Guggenheims previous documentary, An Inconvenient Truth.

== See also ==
* Diane Ravitch

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 