Almost Angels
 
{{Infobox film
| name = Almost Angels
| image = Almost Angels 1962.jpg
| caption =
| tagline = With a broken voice often comes a broken heart
| director = Steve Previn
| producer =
| writer = Vernon Harris  Robert A. Stemmle
| narrator =
| starring =
| music = Heinz Schreiter
| cinematography =
| editing = Walt Disney Productions Buena Vista Distribution
| released = September 26, 1962
| runtime = 93 minutes
| country = US
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Walt Disney Productions film about a group of boys in the Vienna Boys Choir.

The film shows the choristers recruitment process, the rehearsals and the life in the institution as well. The story also conveys the value of friendship, honesty and loyalty.
 Augarten Palais, the surroundings of Vienna and some other places of the beautiful Austrian landscape. 

In the United States, the film was released as the second half of a double bill. The first feature on the double bill was the 1962 re-release of Lady and the Tramp.

==Plot==
Tony Fiala (played by Vincent Winter) is a working-class boy whose greatest desire is to become a member of Viennas most famous choir. His father, however, wants his son to follow in his own footsteps as an engine driver. Unlike his loving and supportive mother, he sees no future for the boy in music. Sean Scully), who is the leading chorister and the most experienced solo voice. When Peter finds out that Tony has a wonderful, clear treble voice, he feels threatened by the talented new boy. Peters jealousy will prompt him to do everything in his power to ruin his rivals public performances and his good image as a boarder, to the point of endangering Tonys life. The sabotage will eventually end but the breaking of Peters voice will change the events drastically.

==Cast==
* Vincent Winter as Tony Fiala Sean Scully as Peter Schaefer
* Peter Weck as Max Heller
* Hans Holt Director Eisinger
* Bruni Löble as Frau Fiala
* Fritz Eckhardt as Herr Fiala
* Denis Gilmore as Friedel Schmidt
* Hennie Scott as Ferdie
* Hans Christian as Choirmaster
* Hermann Furthmosek as Choirmaster
* Walter Regelsberger as Choirmaster

==Songs and music==
The film takes advantage of the story itself to present traditional Austrian and German songs performed by the children. Besides the Lieder, there are some international scores and instrumental music:
 Johann Strauss Jr.)
# "Willkommen"
# "Heidenröslein" (Little Rose of the Heath) by Heinrich Werner / Johann Wolfgang von Goethe
# Unidentified piece for piano and oboe by Wolfgang Amadeus Mozart
# "Der Kuckuck"
# "Wohlan die Zeit ist kommen" from Ludwig Schubart
# "Der Lindenbaum" (Am Brunnen vor dem Tore) by Franz Schubert / Wilhelm Müller
# "Tra la la, der Post ist da" (The Postman) by Rudolf Löwenstein
# "Omnes de Saba Venient" (Graduale by Joseph Eybler)
# "Kindersinfonie" (Toy Symphony by Leopold Mozart)
# "Lustig ist das Zigeunerleben"
# "Das Hennlein Weiss"
# "Guten Abend, Gute Nacht" (Good Evening, Good Night) by Johannes Brahms
# "Greensleeves" (Traditional English Song)
# "Ländler" Blue Danube Johann Strauss Jr.)

==Popular culture== The Sound of Music and six years after Sissi - The Young Empress, Almost Angels uses the same formula of combining a family story, beautiful scenery, beloved music and Austrian local customs and traditions. Although it had limited distribution in theatres, the movie aroused the interest in choral institutions and in the Vienna Boys Choir itself. Almost Angels was telecast broken up into two parts on the Disney anthology television series. The film was released on DVD as part of the Disney Movie Club.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 