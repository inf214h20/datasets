Frisco Sally Levy
{{Infobox film
| name = Frisco Sally Levy
| caption =
| image =
| director = William Beaudine
| story = Alfred A. Cohn Lew Lipton Joseph Farnham
| screenplay = Alfred A. Cohn
| adaptation = Vernon Smith Kate Price Sally ONeil
| music =
| cinematography = Max Fabian
| editing = Blanche Sewell
| distributor = Metro Goldwyn Mayer
| released =  
| runtime =  70 minutes
| language = English
| country = United States
| budget =
}} lost 1927 comedy silent film directed by William Beaudine and starring Sally ONeil and Roy DArcy, which was released on April 2, 1927.    

==Plot==
Colleen Lapidowitz (ONeil) falls in love with an Irish police officer named Patrick Sweeney (Delaney), which is a relief to her Jewish father Isaac (Holtz) and Irish mother Bridget (Price) who have tried to discourage her interest in a sleazy lounge lizard named Stuart Gold (DArcy).

==Cast==
*Tenen Holtz as Isaac Solomon Lapidowitz  Kate Price as Bridget OGrady Lapidowitz 
*Sally ONeil as Sally Colleen Lapidowitz 
*Leon Holmes as Michael Abraham Lapidowitz 
*Turner Savage as Isidore Pastrick Lapidowitz 
*Helen Levine as Rebecca Patricia Lapidowitz 
*Roy DArcy as I. Stuart Gold 
*Charles Delaney as Patrick Sweeney
*Cameo the Dog

==Crew==
*Cedric Gibbons - Art Director David Townsend - Art Director
*René Hubert (costume designer)|René Hubert - Costume Design

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 