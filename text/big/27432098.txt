9 to 5: Days in Porn
{{Infobox film
| name           = 9to5 – Days in Porn
| image          = 9to5 - Days in Porn theatrical poster.jpg
| alt            =  
| caption        = Theatrical poster Jens Hoffmann
| producer       = Cleonice Comino
| writer         = Jens Hoffmann
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = Alex McGowan Michael Meinl
| cinematography = Jens Hoffmann
| editing        = Christopher Klotz Kai Schröter
| studio         = F24 Film
| distributor    = Media Entertainment GmbH (theatrical) Strand Releasing (DVD release)
| released       =  
| runtime        = 95 minutes
| country        = Germany
| language       = German/English
| budget         = 
| gross          = 
}} Jens Hoffmann.       It is also known as The Porn Diaries.

==Background==
The documentary took five years to produce,  with over three years filming.    Through interviews with 77 persons, the film profiles 10 different members of the U.S.s porn industry of the San Fernando Valley, where 80% of American pornography films are produced.   With an emphasis on the actors themselves, the documentary includes individuals involved in production. 

The documentary first screened at the Montréal Film Festival in August 2008.   It was released to the European film market in February 2009 with a world premiere in Berlin, Germany.   It then screened in March 2009 at the Miami International Film Festival and the Buenos Aires International Festival of Independent Cinema in March, and in June at the Bellaria Film Festival, in Bellaria – Igea Marina|Bellaria, Italy. 

==Reception==
Matt Prigge of Philadelphia Weekly wrote that while the documentary "deserves credit for trying to find as many angles from which to look at an industry as successful as it is loathed", it dealt too much with "vacuous porn star confessionals", its scope was too wide, the film lacked organization, and that "its take on the biz sometimes seems less complex than schizophrenic." 

Conversely, Cameron McGaughy of DVD Talk wrote that whether "youre a porn addict, a lover of revealing documentaries or just a curious voyeur, you certainly wont be bored with these 100 minutes."  He felt that director Hoffman kept enough distance from the subject matter to allow viewers to decide for themselves.  McGaughy was "surprisingly pleased with Hoffmanns effort", summarising that the film was "amusing, brutal, candid, honest, hysterical and heartbreaking" and that porn aficionados will find themselves "entertained and enlightened". 

Daniel Bickermann of Schnitt praised the film, writing "Hoffmanns großes und wichtiges Verdienst ist es dabei, beide Seiten parallel zu zeigen, nie zu urteilen und dabei trotzdem immer echte Sympathie für seine Figuren zu beweisen. So entstehen einige Momente reinster Dokumentarmagie"  ("Hoffmanns great and important merit and is to show both sides in parallel, never judging, while still proving genuine sympathy for his characters. This results in some moments of pure documentary magic"). 

The JoBlo offered that he expected either a "gritty, highly-critical depiction of the porn industry, or a cheap, sexy fluff-piece produced and promoted by the porn industry."  In admitting he was wrong in his original expectation, he wrote, "Amazingly, director Jens Hoffman crafts a surprisingly objective film, one that shows both the ups and downs of an industry that contains plenty of them, while avoiding ever preaching to either side." 

The List of newspapers in Germany|Express offered "Hoffmanns Dokumentation wurde auf vielen Filmfestivals gefeiert, erhielt sogar eine Nominierung für den Deutschen Kamerapreis. Und die Kritiker sind sich einig: Der Regisseur porträtiert sensibel und intim, ohne schlüpfrig und voyeuristisch zu werden."  ("Hoffmanns documentary was celebrated at many film festivals, even received a nomination for the German Camera. And the critics agree: The director portrayed sensitively and intimately, without having to be slippery and voyeuristic.") 
 N24 offered "Hoffmann interessiert nicht die milliardenschwere Industrie, die dahinter steckt, ihn interessieren die Leute hinter und vor der Kamera. Er kommt den Akteuren nahe, die Szenen sind intim, aber nie voyeuristisch.". ("Hoffmann isnt interested in the billion-dollar industry thats behind it, hes interested in the people behind and in front of the camera.  He gets close to the actors, the scenes are intimate, but never voyeuristic.") 

==Interviews== Mia Rose, and filmmaker Jim Powers.     
 Canadian pornographic French pornographic Cindy Crawford, Lorelei Lee, Slovak pornographic British pornographic Alex Sanders, adult model Andrew Blake.      

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
* 
* 
* 

 
 
 
 
 