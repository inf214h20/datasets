The Legend of the Golden Gun
{{Infobox television film
| name      = The Legend of the Golden Gun
| image     =
| image_size = 
| caption   = Western
| creator   = 
| director  = Alan J. Levi
| producer  = B. W. Sandefur (producer) Harve Bennett (executive producer) Harris Katleman (executive producer) Dean Zanetos (associate producer) James D. Parriott  (supervising producer)
| starring  = Jeff Osterhage Hal Holbrook Carl Franklin Robert Davi
| music = Jerrold Immel
| writer    = James D. Parriott
| editing    = Robert F. Shugrue
| runtime   = 100 minutes
| cinematography = Gerald Perry Finnerman
| studio = Bennett/Katleman Productions Columbia Pictures Television NBC Columbia TriStar Domestic Television
| country   = United States
| language  = English
| network   = NBC
| released  =  
}}

The Legend of the Golden Gun is a 1979 Made-for-TV Western film, starring Jeff Osterhage, Hal Holbrook, Carl Franklin, and Robert Davi

==Plot Summary==
A young farmer and a bible-quoting runaway slave team up to track down the legendary Confederate guerrilla William Quantrill. Along the way they run into a legendary gunfighter, J.R. Swackhammer, who teaches the young farmer how to shoot and gives him a special gun that shoots seven rounds, the seventh which he must use to fight evil.

==Cast==
* Jeff Osterhage as John Golden
* Carl Franklin as Joshua Brown
* Robert Davi as William Quantrill
* Keir Dullea as General Custer
* Michele Carey as Maggie
* John McLiam as Jake Powell
* Elissa Leeds as Sara Powell
* R. G. Armstrong as Judge Harrison Harding
* Hal Holbrook as J.R. Swackhammer William Bryant as William Ford Rex Holman as Sturges
* J. Brian Pizer as Captain Marks
* R. L. Tolbert as Buffalo Bill
* Budge Taylor as Dr. Wheller Walt Davis as Soldier #3
* Michael Yama as The photographer David Holbrook as The young outlaw

==External links==
* 

 
 
 
 
 
 
 
 


 