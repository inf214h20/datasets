Three on a Spree
{{Infobox film
| name           = Three on a Spree
| image          = 
| image_size     = 
| caption        = 
| director       = Sidney J. Furie
| producer = Edward Small (executive) George Fowler David E. Rose (executive)
| writer         = 
| narrator       = 
| starring       = Jack Watling
| music          = 
| cinematography = Stephen Dade
| editing        = 
| studio = Caralan Productions
| distributor    = United Artists 
| released       = November 1961
| runtime        = 83 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British comedy film directed by Sidney J. Furie, based on the 1902 novel Brewsters Millions, which had been previously filmed by Edward Small in Brewsters Millions (1945 film)|1945. Three on a Spree
Archer, Eugene. New York Times (1923-Current file)   31 Aug 1961: 22. 

==Cast==
* Jack Watling as Michael Brewster 
* Carole Lesley as Susan 
* Renee Houston as Mrs. Gray  John Slater as Sid Johnson 
* Colin Gordon as Mitchell 
* John Salew as Mr. Monkton 
* Julian Orchard as Walker 
* Libby Morris as Trixie 
* Cardew Robinson as Micki 
* Ernest Clark as Colonel Drew  Ronald Adam as Judge
==References==
 
==External links==
* 
*  at TCMDB

 
 
 
 
 
 
 
 
 

 