Cameo Kirby
{{Infobox film
| name           = Cameo Kirby
| image          = Cameo Kirby 1923 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Ford William Fox
| writer         = Robert N. Lee
| based on       =  
| starring       = {{Plainlist| John Gilbert
* Gertrude Olmstead
}}
| music          = 
| cinematography = George Schneiderman
| editing        =  Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = Silent film, English intertitles
| budget         = 
| gross          = 
}}
 silent drama film directed by John Ford and featured Jean Arthur in her onscreen debut. It was Fords first film credited as John Ford instead of Jack Ford.    It was based on a play by Booth Tarkington and Harry Leon Wilson. The story had been filmed as a silent before in 1915 with Dustin Farnum, who had originated the role on Broadway in 1909. The film was remade as a talking musical film in 1930.

Prints of the film exist in the UCLA Film and Television Archive and at the Cinemateca Portuguesa (Portuguese Film Archive), in Lisbon.   

==Cast== John Gilbert as Cameo Kirby
* Gertrude Olmstead as Adele Randall Alan Hale as Colonel Moreau
* Eric Mayne as Colonel Randall
* W. E. Lawrence as Tom Randall (as William E. Lawrence) Richard Tucker as Cousin Aaron Randall
* Phillips Smalley as Judge Playdell Jack McDonald as Larkin Bunce
* Jean Arthur as Ann Playdell
* Eugenie Forde as Madame Davezac
* Frank Baker (uncredited)
* Ken Maynard (uncredited)
* Ynez Seabury (uncredited)

==References==
 

==External links==
* 
* 
*  at Virtual History

 
 

 
 
 
 
 
 
 
 
 
 
 


 