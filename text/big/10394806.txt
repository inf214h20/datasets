The Bride Comes Home
{{Infobox film
| name           = The Bride Comes Home 
| image          = Bridecomeshome.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Wesley Ruggles 
| producer       = Wesley Ruggles
| screenplay     = Claude Binyon
| story          = Elisabeth Sanxay Holding
| starring       = {{Plainlist|
* Claudette Colbert
* Fred MacMurray Robert Young
}}
| music          = Heinz Roemheld
| cinematography = Leo Tover
| editing        = Paul Weatherwax
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Bride Comes Home is a 1935 comedy film made by Paramount Pictures, directed by Wesley Ruggles, and written by Claude Binyon and Elisabeth Sanxay Holding.

==Plot== Robert Young). They discuss love and wedding plans. However when Bristow would seem to marry her, Anderson prepares a plan to take her back. This is a romantic comedy with money, bad tempers and love in the balance.

The Bride Comes Home also co-stars William Collier, Sr. and Donald Meek.

==Cast==
*Claudette Colbert as Jeannette Desmereau
*Fred MacMurray as Cyrus Anderson Robert Young as Jack Bristow
*William Collier, Sr. as Alfred Desmereau
*Donald Meek as The judge
*Richard Carle as Frank (butler)
*Edgar Kennedy as Henry
*Johnny Arthur as Otto
*Kate MacKenna as Emma
*Jimmy Conlin as Len Noble
*Edward Gargan as Cab driver

==External links==
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 


 