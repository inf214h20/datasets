My Little Eye
 
{{Infobox film
| name           = My Little Eye
| image          = Mylittleeyeposter.jpg
| image_size     = 250px
| caption        = UK release poster James Watkins Stephen OReilly Laura Regan Jennifer Sky Bradley Cooper
| director       = Marc Evans
| producer       = Alan Greenspan Jane Villiers David Hilton Jon Finn
| music          = Bias
| editing        = Marguerite Arnold
| cinematography = Hubert Taczanowski WT 2  Productions Universal Pictures Odeon Films  (Canada) 
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = $3 million
| gross          = $9,376,084
}} Big Brother. The title refers to the guessing game I spy.

==Plot==
  Stephen OReilly) and Rex (Kris Lemche) is at a high level. Instead of their regular food deliveries, packages containing sinister object arrive. A letter stating that Dannys grandfather has died, a gun with 5 bullets and a bottle of champagne.

It is less than a week until the 6 months are up, and things begin to go wrong. Not only are the food packages being messed with, but the heating goes out, and Emma finds disturbing reminders of her past around the house, so much so that she begins to think a boy from her childhood is threatening her.

One day a man named Travis Patterson (Bradley Cooper) arrives, claiming he is lost in the woods, his GPS has died. He seems completely oblivious to the cameras in the house and the contestants are puzzled to find out he does not recognize them, even though he says he lives on the internet. That night, Rex and Danny get stoned, Emma goes to bed early, Matt tries to talk to her and Travis has sex with Charlie. After everyone is asleep, Travis sneaks around the house, talking directly into the camera, communicating with whoever is watching them. 

The next morning, Travis leaves before Charlie wakes. In the afternoon, while taking a walk outside, Danny discovers Travis backpack covered in blood and shredded to pieces. The contestants assume he was attacked by an animal but Rex believes Travis works for the people running their show - there were no prints of any kind by the backpack. He says it is just a trick to make them panic and leave, which would mean a forfeit of prize money. After the house guests take this into consideration,  they plea to the cameras and their viewers to call for help.

The group tries to continue with their daily routines. Emma discovers her underwear among Danny’s belongings and confronts him, unaware that Travis planted them there the previous night. Although he denies stealing them, Emma refuses to believe him. When he tries to make peace with Emma by giving her a crudely carved wooden cat, Emma later mocks him with Charlie, which Danny overhears. 

The next morning the group finds Danny dead, hanging from a balcony with a rope around his neck. At this point the house guests no longer care about their prize money and decide to use the emergency radio to call for help. After not being able to contact someone via radio they decide to wait until the next morning, when a helicopter is scheduled to retrieve them for staying in the house for the required amount of time to win.
 hacking trick he knows and still cannot gain access. He then tried legitimately paying with Travis card, but the limit on the card ($50 000) is less than the required fee to view the site, The group realizes that the subscribers are paying more than $50,000 to see the website, and become both confused and unnerved. Shakily, Rex then brings up the possibility of this being a snuff film.

Unexpectedly, the encrypted site gives the house guests access to a page displaying their pictures and odds at winning, along with Danny‘s pictured shaded another color to represent his death. The screen then shows a live video of them, sitting in the supposedly camera-free room. The contestants then gather weapons to assure their safety, with Matt offering to stand guard with the gun. Rex searches through Travis backpack again and finds a flare and they decide to set it off on the roof in the hopes that someone nearby will see it and rescue them. He states he does not want to go up alone, so Emma joins him. Whilst burning the flare, Rex assures Emma that the boy from her past is not following her, its just The Company messing with her head. He promises that, the next morning, they will walk out together, saying The Company can stuff their money.

In the house, Charlie and Matt dusciss what they will do after everything is over. Matt, seeing Charlie is tense, offers to give her a massage. charlie asks Matt why he signed up for the game, he has all he needs. Matt replies that he signed up for the fear, and that it feels good. Suddenly he addresses a camera, asking if he should "kill her now". He then suffocates her using a plastic bag, asking the camera "thats what you want?"

Having finished burning the flare, Rex and Emma renter the house, unaware of Charlies death. Whilst Emma sleeps, Rex stands guard over her. He falls asleep however, and wakes with a cough. He goes downstairs for a drink and finds his video game is on, he goes over to investigate and decapitated with an axe by Matt.

Matt then fires a single shot from his gun while running upstairs, waking Emma and telling her he was chased. They hide in the attic and Matt tells her that "they got Charlie". Emma wants to go save Tex, whom she still believes alive, but Matt tells her he is only worrying about yet now, that they belong together. Emma refuses his advances, which leads Matt to attempt to rape her, before she stabs him in the back (literally) and runs. She manages to get his the gun and then shoots him, just as a car approaches the house.

Emma gets out of the house to find a cop (Nick Mennell) waiting outside. The officer asks Emma if she was the one who set off the flare. Emma is terror stricken, and tries to tell him that people are dead and Matt is after her. She pleads with the officer not to go into the house. The cop handcuffs Emma and places her into his car, stating he has to follow procedure. While in the car Emma is seen being filmed. As the injured Matt crawls out of house, he converses with the cop, begging the cop to let him kill Emma, after spending so long in the house. the cop refuses, asking Matt what he thinks he (the cop) has been doing in that time, and it is revealed that they are working together. The cop unpicks his car, Emma gets out and runs away. The cop then shoots her in the back with a rifle. She falls to the ground.
 champagne together. As they talk, it becomes apparent that the cop set all this up by himself, along with Travis, and very rich clients who are paying a high fee to watch the murders. He tells Matt that there are always "five suckers" to play the game with. When Matt corrects him to "four" as he is still alive, Emma is also revealed to be alive, trapped in a small room. The cop ignores Matts correction and comments that Emmas "a wriggler". Just as Matt observes the captured Emma, the cop shoots him in the head, calling Travis over the radio. the cop leaves the house, and Travis says that next time hell be the cop. Emma, who witnesses all this, falls to the ground screaming in terror, as her picture is broadcast over the internet. She hammers at the door, but it does not give. As she falls to the ground, dead or unconscious, the cameras, one by one, shut off.

==Cast==
*Sean Cw Johnson as Matt
*Kris Lemche as Rex Stephen OReilly as Danny
*Laura Regan as Emma
*Jennifer Sky as Charlie
*Nick Mennell as The Cop
*Bradley Cooper as Travis Patterson

==Home media==
My Little Eye is available on DVD from Universal Studios|MCA/Universal Home Video with most of the special features available on the Region 2 Special Edition including a filmmakers commentary and deleted scenes. There is an audio mode "Conversations of the Company (Eavesdropping Audio Track)" which allows the viewer to listen to the radio conversations between the members of the company: Travis and "the cop". However, during this mode, the viewer cannot hear all of the dialogue of the cast in the scene. A UK release contains a Special Mode where viewers see the film from the perspective of an internet subscriber, and more extra features become unlocked as the film goes on. You can watch other things going on in the house in real time to whats happening in the film.

==See also==
*List of films featuring surveillance

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 