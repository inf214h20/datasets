Daughters of Satan
{{Infobox film
| name           = Daughters of Satan 
| image          = Superbeast & Daughters of Satan poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Hollingsworth Morse
| producer       = Aubrey Schenck
| screenplay     = John C. Higgins
| story          = John A. Bushelman 
| starring       = Tom Selleck Barra Grant Tani Guthrie Paraluman Vic Silayan Vic Díaz
| music          = Richard LaSalle
| cinematography = Nonong Rasca 
| editing        = Anthony DiMarco 
| studio         = A & S Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Daughters of Satan is a 1972 American horror film directed by Hollingsworth Morse and written by John C. Higgins. The film stars Tom Selleck, Barra Grant, Tani Guthrie, Paraluman, Vic Silayan and Vic Díaz. The film was released on November 1, 1972, by United Artists.   It was released as a double feature with Superbeast (film)|Superbeast.

==Plot==
 

== Cast ==	 
*Tom Selleck as James Robertson
*Barra Grant as Chris Robertson
*Tani Guthrie as Kitty Duarte
*Paraluman as Juana Rios
*Vic Silayan as Dr. Dangal
*Vic Díaz as Carlos Ching
*Gina Laforteza as Andrea
*Ben Rubio as Tommy Tantulco
*Paquito Salcedo as Mortician
*Chito Reyes as Guerilla
*Bobby Greenwood as Mrs. Postlewaite 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 