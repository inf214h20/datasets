Sister My Sister
{{Infobox film
| name           = Sister My Sister
| image          = Sister My Sister DVD.jpg
| image_size     =
| caption        = DVD cover
| director       = Nancy Meckler
| producer       =
| writer         = Wendy Kesselman
| narrator       =
| starring       = Julie Walters Joely Richardson Jodhi May
| music          =
| cinematography =
| editing        =
| distributor    = British Screen Productions Channel Four Films NFH Productions
| released       =  
| runtime        = 89 min.
| country        = United Kingdom English
| budget         =
}} British actresses Julie Walters, Joely Richardson, and Jodhi May. The film is directed by Nancy Meckler and written by Wendy Kesselman, based on her own play, My Sister in This House. Both the play and the subsequent film deal with societal repression and its victims.
 Papin murder case, where two sisters brutally murdered their employer and her daughter. The murder shocked the country, and there was much speculation about the sisters, including allegations that they were having an incestous lesbian affair with each other.

== Plot ==

Christine (Richardson) is the maid of a well-to-do widow (Julie Walters) and her daughter (Sophie Thursfield). Her sister, Lea (May) is hired on the recommendation of Christine. The two sisters become increasingly alienated from their employer, separated by barriers between the social class|classes. With only each other to turn to and Christine experiencing much jealousy as to her sisters interest in anyone else, the relationship becomes sexual, adding to the tension between the sisters and their employer. The tension  ultimately leads to paranoia, repressed rage and murder.

== Cast ==
*Julie Walters as Madame Danzard
*Joely Richardson as Christine
*Jodhi May as Lea
*Sophie Thursfield as Isabelle Danzard
*Amelda Brown as Visitor #1
*Lucita Pope as Visitor #2
*Kate Gartside as Sister Veronica
*Aimee Schmidt as Young Lea
*Gabriella Schmidt as Young Christine

== Related films ==
 same name. It starred Glenda Jackson and Susannah York as the maids, and Vivien Merchant as their employer.
 French film of Claude Chabrol, La Cérémonie, with Isabelle Huppert and Sandrine Bonnaire.  The characters are not the Papin sisters, but are two women that end up murdering their employer. It is an adaptation from the novel A Judgement in Stone by Ruth Rendell.

The story was also filmed as Murderous Maids, a French film starring Sylvie Testud and Julie-Marie Parmentier, and directed by Jean-Pierre Denis.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 