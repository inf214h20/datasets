Three Blind Mice (2008 film)
 
{{Infobox film
| name = Three Blind Mice
| image = Three Blind Mice.jpg
| caption = Three Blind Mice Film Poster
| director = Matthew Newton
| producer = Ben Davis   Caitlin Stanton
| executive producer = Michael Favelle   Ben Ferris
| writer = Matthew Newton
| starring = Matthew Newton   Ewen Leslie  Toby Schmitz  John Foreman
| cinematography = Hugh Miller
| distributor = Titan View 
| released =  
| runtime = 94 minutes
| country = Australia
| language = English
| gross = A$9,590 (Australia) 
}}
Three Blind Mice is a 2008 feature film written, directed by and starring Matthew Newton. It is the second film directed by Matthew Newton and premiered at Sydney Film Festival   in 2008. To date it has screened at over fourteen international and Australian festivals.    

==Plot==
Three young Australian naval officers hit the streets of Sydney for one last night before being shipped out to Iraq. The dynamic between the three friends is uneasy; Sam (Ewen Leslie) has been mistreated at sea and is going AWOL, Dean (Toby Schmitz) has a fiancé and the future in-laws to meet, and Harry (Matthew Newton) just loves playing cards. Throughout the night the boys struggle with what a night in Sydney can offer, as details of their last six months at sea emerge. 

==Cast==
The film features many distinguished and critically acclaimed Australian actors.    The full cast list is as below:
{| class="wikitable"
|-
! Actor !! Role
|-
| Toby Schmitz || LEUT Dean Leiberman
|-
| Matthew Newton|| SBLT Harry McCabe
|-
| Ewen Leslie || SBLT Sam Fisher
|-
| Gracie Otto || Emma
|-
| Marcus Graham || John
|-
| Clayton Watson || Vito
|-
| Alex Dimitriades || Tony
|-
| Pia Miranda || Sally 
|-
| Barry Otto || Fred
|-
| Heather Mitchell || Kathy
|-
| Jacki Weaver || Bernie Fisher
|-
| Charles Tingwell || Bob Fisher
|-
| Brendan Cowell || LCDR Glenn Carter 
|}

==Themes==
Three Blind Mice has been noted as the first portrayal of Australian soldiers serving during the War in Iraq.    While Newton has stated the film is not a political statement, it does portray his belief that young men should be making mistakes, rather than going to war.    The film also references iconic Australian military legends, such as Gallipoli   Primarily, the film explores what it is to be a man, and what that means to live and act in a male-dominated world today.      

==Production ==
Three Blind Mice was filmed in a Guerilla style; on location without proper permits and in locations in Sydney that are not often seen on screen.    
The film relied solely on independent funding, while Screen Australia contributed funds for the film to be transferred to 35mm print.  
Cast and crew from the film was largely made up of friends of Newtons. Gracie Otto who was a lead support in the film also edited. 
It has been noted in several reviews that the film was inspired by John Cassavetes, and thus many scenes were improvised around the screenplay. 
Newton has stated that he intended to make a multi-narrative film, so that every character could treat the film as though they were the lead,  rather than rely on a sole protagonist.

==Reception== At the Movies. Primarily, both reviewers noted the strong performances by the distinguished cast. 
While the performances by the cast were particularly acclaimed for their freshness,  Matthew Newtons screenplay was also commended and noted for its maturity, comedy and realism.  His directorial ability was also commended.   
In the opening scene Mathew Newton is wearing the epaulettes of a Leut Commander (two and a half rings). In later scenes he is wearing the single ring of a Sub Lieutenant on his jacket.
All Officers in the film are wearing their medal ribbons on the right breast, when they should be worn on the left breast.

== Release ==
Three Blind Mice did not secure a theatrical distributor for several months throughout 2008 and 2009. Finally in April 2009, 
Titan View picked up distribution rights for Australia and New Zealand. 

Three Blind Mice struggled to secure a theatrical release in Sydney, the city it was produced in.    Finally, the Chauvel Cinema agreed to show the film for five consecutive Friday nights.  Titan View CEO John L. Simpson noted that this limited release was due to the citys arthouse cinemas rejecting the film as not commercial enough. 

Meanwhile, Three Blind Mice secured a limited theatrical release in New York and Los Angeles, promoted by exposure at such festivals as SXSW. 

===Awards and Festivals===
Three Blind Mice has featured and been in competition at the following festivals 
* 2008 Sydney Film Festival - In Competition (No wins, Jury Commendation)
* 2008 Times BFI London Film Festival - Won FIPRESCI Prize  
* 2008 International Thessaloniki Film Festival - Won Best Screenplay
* 2008 Toronto Intl Film Festival
* 2008 Melbourne International Film Festival
* 2008 Toronto International Film Festival - Discovery section.
* 2008 AFI FEST (Los Angeles) - In Competition, WINNER- Best Screenplay.
* 2008 Canberra International Film Festival
* 2009 Dublin International Film Festival
* 2009 Glasgow International Film Festival
* 2009 SXSW (South By Southwest)
* 2009 Philadelphia Film Festival
* 2009 Cleveland International Film Festival
* 2009 Seattle International Film Festival

Despite critical acclaim at several international and local festivals, Three Blind Mice was not nominated for an AFI within Australia  

==References==
 

==External links==
*  

 
 
 