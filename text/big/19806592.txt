Martha (1967 film)
{{Infobox film
| name           = Martha
| image          = 
| caption        = 
| director       = Erik Balling
| producer       = Francis Carabott Bo Christensen
| writer         = Henning Bahs Erik Balling Preben Kaas
| starring       = Ove Sprogøe
| music          = 
| cinematography = Jørgen Skov
| editing        = Birger Lind
| distributor    = Nordisk Film
| released       =  
| runtime        = 93 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Martha is a 1967 Danish comedy film directed by Erik Balling and starring Ove Sprogøe.

==Cast==
* Ove Sprogøe - Hovmester Watson
* Morten Grunwald - 2nd Engineer Knud Hansen
* Poul Reichhardt - Chief Engineer Brovst
* Poul Bundgaard - 2nd Engineer Alf
* Karl Stegger - Kaptajn Peter Nielsen
* Preben Kaas - Kokken
* Helge Kjærulff-Schmidt - Skibsreder O.P. Andersen
* Lily Weiding - Frk. Bruun
* Hanne Borchsenius - Lone Andersen
* Eleni Anousaki - Laura
* Henrik Wiehe - Styrmanden
* Birger Jensen - Halfdan
* Paul Hagen - Telegraph operator Marius Knudsen
* Bjørn Puggaard-Müller - Johansen
* Gunnar Bigum - Regnskabschef
* Holger Vistisen - Dørmand
* Sverre Wilberg - Skibsreder Amundsen
* Sotiris Moustakas - Hector
* Antonis Papadopoulos - Aias
* Odd Wolstad - Kaptajn på Harald
* Lone Luther - Rengøringsdame
* Stavros Christofides - Fyrbøder Alexander

==External links==
* 

 
 
 
 
 
 


 
 