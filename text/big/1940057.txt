Gamera (film)
 
 
{{Infobox film|
 name = Gamera |
 image = Gamera_1965.jpg 
| caption        = 1965 Theatrical Poster
| director = Noriaki Yuasa |
 producer = Hidemasa Nagata Yonejiro Saito Masaichi Nagata |
 writer = Nisan Takahashi Yonejiro Saito |
 starring = Eiji Funakoshi Harumi Kiritachi  Junichirô Yamashita |
 music = Tadashi Yamauchi |
 cinematography = Nobuo Munekawa |
 editing = Tatsuji Nakashizu | Daiei Film Co., Ltd.  Daiei Film Co., Ltd. |
 released =   |
 runtime = 80 min. (Original)  86 min. (USA) |
 country = Japan | Japanese English English |
 budget = |
}}

  is a 1965 Science fiction Kaiju film directed by Noriaki Yuasa. The films plot involves an ancient giant turtle monster called "Gamera" who has been unleashed from its icy tomb by an atom bomb only to terrorize the entire country of Japan.

The film is the first in a series of Kaiju (giant monster) films featuring the monster Gamera. Though created to capitalize on the success of Tohos popular Godzilla films, Gamera did manage to find success on its own and spawn its own franchise. After several sequels over the years, fans have widely regarded Gamera as one of Japans most memorable Kaiju characters, right behind Godzilla. It was one of five Gamera films to be featured on the movie-mocking television show Mystery Science Theater 3000.

==Plot==
In an icy North American region, a Soviet bomber is shot down by an American fighter jet. The bomber crashes and its cargo, a low-level atomic bomb, explodes. The resulting cataclysm awakens a giant, prehistoric monster called "Gamera", who has the appearance of a giant turtle with large tusks.
 Fujisawa and retreats back  into the sea. Scientists and government officials hold a conference to discuss killing the monster.

Gamera destroys a research ship, kills the crew, and then heads to Tokyo. He is sedated and  hit by group of bombs. Gamera awakens in time, protected from the bombs by his hard shell, and escapes the attack with a new ability: flying.

Gamera lands in Haneda Airport, and proceeds to Tokyo, rampaging through the city. The military observes that he ate fire from the buildings he destroyed, and they devises "Plan Z". Using fire, they  lure Gamera to a missile complex, where he is imprisoned in a giant rocket ship. Held by strong chains, the monster is launched into outer space, presumably never to return.

==English versions==
 
 
Gamera was originally presented in the United States by World Entertainment Corp. and Harris Associates, Inc., who renamed the film Gammera the Invincible. Reportedly the extra M in the monsters name was to prevent confusion over the pronunciation. Premiering in New Orleans on December 15, 1966, this was the only film in the original Gamera series to receive a theatrical release in America.

Gammera the Invincible was heavily re-edited from its original Japanese version. To Westernize the film for American audiences, director Sandy Howard filmed new footage starring Albert Dekker, Brian Donlevy, John Baragrey and Dick ONeill. Many of these new scenes replaced similar scenes from the original cut that featured Caucasian extras. Several other scenes from the Japanese version were removed altogether to make room for the new American scenes. Although not credited in the film, its likely that the remaining Japanese dialogue was dubbed into English by Titan Productions, Inc. in New York. Notable Titan performers such as Bernard Grant, Paulette Rubinstein and Larry Robinson all lent their voices to the film. Future Speed Racer voice actors Peter Fernandez, Jack Curtis and Corrine Orr also participated in the re-recording sessions.

In 1985, American producer Sandy Frank purchased the rights to five of the original eight Gamera films and distributed them to television and home video via King Features. Frank retitled and re-released the first film under the title Gamera. Unlike the previous American version of the film, Frank changed very little from the original Japanese cut of the film. New opening credits set against a stock shot of the ocean were attached to the film and a new English dub Americanized most of the characters names ("Toshio" became "Kenny", "Kyoko" became "Katherine", "Aoyagi" became "Alex", etc.). None of the footage shot by Sandy Howard for the 1966 American version was seen in this version of the film.

==Mystery Science Theater 3000==
The movie was satirized twice in the B-movie-mocking series Mystery Science Theater 3000 (MST3K). It first appeared on the show during its first season on Minnesota UHF station KTMA, debuting on November 12, 1988. On August 6, 1991, it was again featured as the second episode of the third season on cable channel Comedy Central. Since the writing on the show had evolved a great deal since the KTMA days, the writing staff ended up tossing out all the improvised jokes from the first episode and re-wrote the entire show. The 1985 "Sandy Frank version" of the film was shown in both episodes.

==Extreme Makeover: Home Edition==
Clips from Gamera were used for the scene covering the demolition in the   "Watson Family, Part 1" special episode on November 26, 2012. 

==DVD releases==
Alpha Video
* Released: May 20, 2003
* This is an unauthorized release featuring the original US version, Gammera the Invincible (pan-and-scan).
Dollar Entertainment
* Released 2004. Pan and scan of Gammera the Invincible. DVD includes Attack of the Monsters, aka Gamera vs. Guiron.
Mill Creek Entertainment
*Released 2005. This is an unauthorized release featuring the US version. Gammera the Invincible. Part of a 5-disc set, Monsters, containing 19 other films.
St. Clair Entertainment
* Released: February 19, 2008
* This is an unauthorized release featuring the original US version, Gammera the Invincible (letterboxed).
Shout! Factory
* Released: May 18, 2010 
* This release was authorized by Kadokawa Pictures and features the original Japanese version of the film for the first time on DVD.

Shout! Factory
* Second release: August 2, 2011
* MST3K vs. Gamera: Volume XXI. Box set featuring the five Gamera episodes of Mystery Science Theater 3000 (Gamera, Gamera vs. Barugon, Gamera vs. Gyaos, Gamera vs. Guiron, and Gamera vs. Zigra.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

=== Mystery Science Theater 3000 ===
*  
*  

===Gammera the Invincible===
*  

====References==== Variety Weekly. December 27, 1967.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 