Salvage (2009 film)
{{Infobox film
| name           = Salvage
| image          = Salvage DVD cover.jpg
| caption        = DVD cover
| director       = Lawrence Gough
| producer       = Julie Lau Alan Patterson
| starring       = Neve McIntosh Shaun Dooley Linzey Cocker
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} Alan Patterson. The film stars Neve McIntosh, Shaun Dooley and Linzey Cocker as residents in a suburban street who find themselves isolated from the outside world following an emergency. The film was one of three produced in Liverpool to celebrate the citys status as EU City of Culture in 2008, and was filmed on the set of former soap opera Brookside.    It was produced on a minimal budget, and was the last time the Brookside set was used for filming purposes before it was sold to a private developer. Neve McIntosh won two Best Actress awards for her role in the film.

==Plot==
On Christmas Eve morning, a paperboy delivers newspapers in a cul-de-sac. Hearing a row between an Indian couple, he peers through the window, but is spotted and chased away by the man, Mr Sharma. He flees into the woods behind the street, but is surprised and killed by an unknown assailant.

Meanwhile, Clive (Dean Andrews) is driving his 14-year-old daughter Jodie (Linzey Cocker) to stay with her mother for Christmas. She is reluctant, as her parents are estranged, her father has custody, and she does not get on with her mother. At her mothers house in the cul-de-sac, she finds nobody apparently in, but lets herself in with the key hidden in a flower pot. Hearing noises upstairs, she finds her mother, Beth (Neve McIntosh), having sex with a man, Kieran (Shaun Dooley). Disgusted, she storms out and goes across the road to the home of her friend Lianne. Beth follows her, but Liannes mother Pam (Debbie Rush) refuses to let her in.

As Beth stands in the street, a helicopter flies overhead and a team of black-clad special forces soldiers appears in the street and orders everyone inside at gunpoint. Mr Sharma emerges from his house covered in blood and advances towards the soldiers with a knife. They shoot him dead. Beth and Kieran stay in her house listening to gunfire. Kieran reveals that he is happily married with two sons. He and Beth appear to just have had a one-night stand. Before the power goes off, a television news report shows a shipping container which has washed up on a nearby beach. Three bodies were found near it and another further inland.

Hearing noises upstairs they discover the loft door open and a bloodstained sledgehammer. A figure appears and Kieran stabs him in the throat. As he dies, Beth realises it is her next-door neighbour Peter, who has smashed his way through the communal wall between their lofts.
 Kevin Harvey), inside and bandage his wounds. He says that Mr Sharma was an al-Qaeda terrorist and the shipping container contained arms for a terrorist operation. The army have sealed off the street to contain the terrorists, confirming Kierans suspicions. However, later Beth hears him talking on his radio to his commander and saying that the creature is very fast and deadly and they are all dead. When she confronts him, he admits the truth. Special forces "rescued" a group of people from abroad, but instead of releasing them they pumped them full of drugs to create a secret weapon. All but one died, but that one proved an uncontrollable monster. It was sealed in a shipping container and being taken for destruction by a helicopter, but the helicopter went down in the sea. The shipping container washed up on the beach, but before special forces could arrive it was forced open by three drunk teenagers. The creature killed them and escaped.

At this point the creature appears at the window and starts to force its way in. Beth and Kieran flee into the loft, but Kieran is dragged down as he tries to climb in. Beth manages to reach her own house, but comes face to face with the sergeant major (Ray Nicholas) commanding the special forces team, who knocks her out with his rifle.

Beth awakens to find herself tied up outside, presumably to act as bait to lure out the creature. It is now dark. At this point, Kieran, badly wounded but still alive, appears and frees her. They run straight into the sergeant major, who stabs Kieran to death. Beth flees into the woods, pursued by the soldiers, and finds the paperboys body. The creature appears, but does not notice her, and attacks the soldiers. Beth returns to the cul-de-sac and goes to Liannes house to try to find her daughter. She finds Lianne hiding in the living room. Lianne tells her that Jodie has gone home. Beth flees the house ahead of the remaining soldiers. Shots suggest that they have killed Lianne and are disposing of all witnesses. Going back to her own house, she finds Jodie, but they are attacked by the creature. Beth manages to stab it to death as it attacks Jodie, but as she stands up and screams in triumph she is shot dead by one of the soldiers.

==Cast==
*Neve McIntosh as Beth
*Shaun Dooley as Kieran
*Linzey Cocker as Jodie
*Dean Andrews as Clive
*Debbie Rush as Pam Kevin Harvey as Akede
*Ray Nicholas as the Sergeant Major
*Paul Opacic as Corporal Simms
*Ben Batt as Trooper Jones

 

==Production==
Salvage was one of three films produced celebrating Liverpool culture to coincide with the citys status of EU City of Culture in 2008. It was directed by Lawrence Gough. 

Filming for Salvage had begun by March 2008, when the Liverpool Daily Post reported that the set of Brookside Close had been rented to a local production company to use as a production set. The production was a low-budget production titled Salvage. {{cite news
|url=http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2008/03/19/brookside-close-becomes-set-for-horror-movie-99623-20643700/ |title=Brookside Close becomes set for horror movie |date=19 March 2008 |publisher=Trinity Mirror|newspaper= Liverpool Daily Post|accessdate= 17 November 2012|archiveurl= http://www.webcitation.org/6CFlW1We3|archivedate= 17 November 2012}}  This was the last time the houses of the Brookside set were ever used for production purposes.  It was sold to a private developer months later. 

==Release and reception== Time Out observed that the film had made clever use of the set. 
 International Fantasy Film Award for Best Actress in 2010,  as well as the Award for Best Horror Actress at the Fantastic Fest.   

==References==
 

==External links==
* 

 
 
 
 
 