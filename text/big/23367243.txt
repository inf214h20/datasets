A Man of Principle
 
{{Infobox film
| name           = Cóndores no entierran todos los días
| image          = A man of principle.jpg
| caption        = Theatrical release poster
| director       = Francisco Norden
| producer       = Francisco Norden
| writer         = Gustavo Álvarez Gardeazábal Francisco Norden Dunav Kuzmanich
| starring       = Frank Ramírez Vicky Hernández Isabela Corona
| music          =
| cinematography = Carlos Suárez
| editing        = José Alcalde  Francisco Norden
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Colombia
| language       = Spanish
| budget         =
}}
 Best Foreign Language Film at the 57th Academy Awards, but was not accepted as a nominee. 

==Plot==
Leon Maria Lozano is a humble worker and a Colombian Conservative Party member living in Tulua, Colombia, in a time where liberals rule following the close 1946 presidential election. For his activism he is discriminated against by the majority of people except by Gertrude Potes, a senior military liberal and a few other liberals.

In those years political killing were common. News of conservatives crimes against liberals leads to the Liberals being condemned as Masons and atheists. Rosendo Zapata, a senior member of the Liberal party insults the conservative party when talking with Lozano, but Lozano demands respect. Leon Marias job as a bookseller is poor, but Miss Gertrudis convinces the mayor to give him a job as a cheese salesman in the market square.

On the presidential election day, to his surprise, Leon Maria learns that the Conservative Party has won the elections. Miss Gertrudis hopes the victory is temporary. Two years later news spreads of around the country of the assassination of popular Liberal leader Jorge Eliecer Gaitán. The village church and other Conservative-leaning institutions close fearing reprisal. Leon Maria notes the rising revolutionary atmosphere and wishing to protect the interests of conservatives, he and other militants get weapons and keep watch. That night Maria Leon scares the protesters with dynamite before they can burn the church. The next day Gertrudis and the town liberals are surprised to find Leon Maria hailed as a hero of the conservatives. Leon Maria is quick to capitalized on his new fame. No longer an outcast, he begins to build up power and influence in the town and receives support from the Conservative party in the capital.

By 1950 the Liberals do not return to power and the liberal mayor of Tulua is replaced by a conservative, Leon Maria soon after becomes an assassin taking advantage of the situation to eliminate his enemies. His conservative supporters become his henchmen called The Birds (in Spanish: Pajaros) and they begin a campaign of murder and intimidation. They strike down Rosendo Zapata and many others, often with little or no cause. Gertrude begin to fear for her life.

In one of his first hits, he sends a "Pajaro" attack against the local jail to free imprisoned conservatives to join his private army. The Mayor passes by Leon Maria overseeing the operation from his car and asked what is happening. The mayor is frightened by this man, but Leon Maria Lozano does not hesitate to criticize him, treating him as a weak and insisting that he should be sponsoring this fight. The mayor disagrees, pointing out that many of the prisoners are common criminals, but he can do nothing to stop Leon Maria Lozano.

The Conservative Party reward Leon Maria Lozano by inviting him to Bogotá to show their full support and protection for the continuation of their patriotic mission. Leon Maria has his daughter, who is beginning to fall in love with a local liberal boy, accepted into a prestigious boarding school. Meanwhile, the Liberals in Tulua come together to express concern over the Pajaros and the reign of terror:

For if the threat is the birds, what we face is a condor, in Spanish: Pues si la amenaza son los pájaros, a lo que nos enfrentamos es a un cóndor.

With this phrase, Gertrude Potts gives Leon Maria Lozano his famous alias. During the wave of killings the murdered liberals are left in other municipalities to be buried anonymously as "N.N.". All the people are afraid to confess or speak out, for fear of being killed by "The Condor"

Leon Maria Lozano, now "the Condor" is transformed into a sinister and Machiavellian man, not only pursuing the Liberals, but anyone who opposes his regime. After being criticized by a journalist, one of "the birds" travels to the mans office and abruptly shoots him. Later, Leon Maria Lozano is poisoned with a cheese fritter and seems on the verge of death. The whole village comes out to celebrate at night, singing, dancing and shooting off fireworks just outside his house . But the Condor doesnt die. After recovering, Leon Maria Lozano orders to kill the musicians who played that night. No one attends the funeral, afraid they will be the next victims of the condor.
 The Four Horsemen of the Apocalypse, who want his soul. 

Leon Maria Lozano reign begins to crumble after a massacre in Recreo, close to Tulua, where women are raped and killed. Leon Maria is blamed though he was not involved. He is unable to determine who gave the order and is outraged and frustrated, while popular sentiment turns even further against him, even within the conservatives. Around this time President Gustavo Rojas Pinilla loses power, and the Condor loses his political protection. Committees meet to discuss an end to The Violence.

The Conservative Party, no longer willing to tolerate the atrocities of Leon Maria Lozano, sends him to   attack (as others suspected), but from an assassins bullet.

==Cast==
* Frank Ramírez as Leon Maria Lozano
* Isabela Corona as Dona Gertrudis Potes
* Víctor Morant as Padre Amaya
* Santiago García  as Rosendo Zapata
* Luis Chiape as Gustavo Gardeazabal
* Juan Gentile as Andres Santacoloma
* Vicky Hernández as Agripina
* Antonio Aparicio as Lamparilla
* Carlos Parada as Atehortua

==See also==
* List of submissions to the 57th Academy Awards for Best Foreign Language Film
* List of Colombian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 