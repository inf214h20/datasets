The Bar-C Mystery
 
{{Infobox film
| name           = The Bar-C Mystery
| image          = 
| caption        = 
| director       = Robert F. Hill
| producer       = C.W. Patton
| writer         = William Sherwood Raymond Spears
| starring       = Dorothy Phillips Wallace MacDonald
| cinematography = 
| editing        = 
| distributor    = Pathé Exchange
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial directed by Robert F. Hill. It is now considered to be lost film|lost.   

==Cast==
* Dorothy Phillips - Jane Cortelyou
* Wallace MacDonald - Nevada
* Ethel Clayton - Mrs. Lane
* Philo McCullough - Robbins
* Johnny Fox - Tommy
* Violet Schram - Wanda
* Fred DeSilva - Grisp Julie Bishop (as Jacqueline Wells)
* Billy Bletcher
* Jim Corey
* Al Hart
* Fred Kohler
* Tom London
* Francis McDonald
* Victor Potel

==See also==
* List of American films of 1926
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 