The Echo of Youth
{{Infobox film
| name = The Echo of Youth
| image = The Echo of Youth (1919) - Ad 1.jpg
| caption = Ad for film
| director = Ivan Abramson
| writer = Ivan Abramson
| starring =
| producer = Graphic Film Corporation (28 April 1919).  , The Beaver County Times 
| distributor =
| budget =
| released =  
| country  = United States Silent (English intertitles) reels
}}
 silent drama Charles Richman, Leah Baird, Pearl Shepard, and Marie Shotwell.

==Plot==
Cabaret singer Olive Martin (played by Baird) approaches her former lover Peter Graham (Richman), just recently elevated to the Supreme Court, about the fact that he is the father of her out-of-wedlock son.  To avoid exposing this scandal, Olive demands that Peter divorce his wife (played by Shotwell) and marry her.  Meanwhile, the alleged son, Harold (played by Jack McLean) is falling in love in Boston with Anita (Pearl Shepherd)&mdash;who is Peters daughter with his wife.  News of their engagement and impeding marriage of incest requires Peter to divulge what he knows and forbid the marriage.  In typical Abramson fashion, however, it is revealed that Olive has lied about Harold being her son—instead he is the son of Olives brother-in-law merely being used by Olive for blackmail!  Peters plan to commit suicide is successfully stopped, and the wedding free to proceed.  , Retrieved May 10, 2012  

==Cast== Charles Richman as Peter Graham
*Leah Baird as Olive Martin
*Pearl Shepard as Anita Graham
*Jack McLean as Harold Martin
*William Bechtel as Thomas Donald
*Marie Shotwell as Ruth Carlyle Graham
*Howard Hall as John Carlyle
*Peggy Shanor as Marian Ducet
*Philip Van Loan as Marcel Ducet

==Background==
Released in February 1919, the film was the last picture Abramson directed for Graphic Film Corporation while it was a joint venture with  , for example, called it "awful" and the "worst picture on the docket." (2 February 1919).  ,   ("... if twice in one month did I have to sit through things like "The Echo of Youth.")  (2 May 1919).  , Chicago Tribune ("Sometime They Are Going to Give Her a Good Picture Again. The Echo of Youth Is Awful.") 

==References==
 

==External links==
 
* 
*  at American Film Institute

 
 
 
 
 
 