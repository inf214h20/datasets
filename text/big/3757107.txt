God Is My Co-Pilot (film)
{{Infobox film name     =God Is My Co-Pilot image    =God Is My Co-Pilot (film).jpg caption  =DVD cover director =Robert Florey writer   =Abem Finkel Peter Milne starring =Dennis Morgan Dane Clark producer =Robert Buckner
|cinematography=Sidney Hickox Charles A. Marshall editing  =Folmer Blangsted
|distributor=Warner Bros. released =April 7, 1945 (Premiere: Macon, Georgia) runtime  =90 min. country  =United States language =English budget   = music    =Leo F. Forbstein
}} American war film based on the autobiography of the same name by Robert Lee Scott, Jr. The film tells the story of Scotts association with the Flying Tigers and the United States Army Air Forces in China and Burma during World War II.

==Plot== the Philippines against the Japanese capital of Tokyo. When the mission is cancelled after he arrives in British Raj|India, Scott flies transport aircraft over The Hump into China.

Scott persuades Claire Chennault (Raymond Massey), the leader of the Flying Tigers to let him fly with the airmen such as "Tex" Hill (John Ridgely) who have been fighting the Japanese as a mercenary air force. Scott gets his chance to fight, ultimately engaging in combat with the deadly fictional Japanese pilot known as "Tokyo Joe" (Richard Loo).

==Cast==
As appearing in screen credits (main roles identified):   IMDb. Retrieved: May 12, 2012. 
* Dennis Morgan as Robert Lee Scott Jr.
* Dane Clark as Johnny Petach
* Raymond Massey as Maj. Gen. Claire L. Chennault
* Alan Hale, Sr. as "Big Mike" Harrigan, Missionary Priest
* Richard Loo as "Tokyo Joe"
* John Ridgely as David Lee "Tex" Hill|"Tex" Hill Craig Stevens as Ed Rector
* Andrea King as Catherine Scott, Robert Lee Scott Jr.s wife Merian Cooper
* Warren Douglas as Bob Neale
* Stephen Richard as Sgt. Baldridge
* Charles Smith as Pvt. Motley
* Minor Watson as Col. Caleb V. Haynes

==Production== Luke Auxiliary Curtiss P-40Es North American North American AT-6 trainers, painted in camouflage and Japanese markings. 
 
 Flying Tigers, were evident in the background, along with two P-40Es reclaimed from the AAF Reclamation Depot in San Diego.  The films air operations were directed by Hollywood "stunt pilots" Frank "Speed" Nolta and Major Frank Clarke.  Col. Scott served as a technical advisor and flew in a number of sequences, reprising his role as a Flying Tiger. 

==Reception==
Regarded as typical Hollywood fare by most moviegoers, the script nonetheless attempted to be mainly faithful to Col. Robert Lee Scott Jr.s original story of his exploits over China, and bringing in enough of his backstory to let the audience feel they knew him. By basing the film on exploits of actual historical figures (only occasionally resorting to fictional characters such as "Tokyo Joe"), the film gained considerable credibility.  However, by 1945 the American film-going public were wary of what was essentially seen as another in a series of patriotic, "flag-waving" films. Critics relegated it to an "also-ran" position regarding the sub-plot of Scotts inspirational message as forced. The New York Times reviewer, Bosley Crowther noted that the "pious injection of the spiritual in an otherwise noisy action film is patently ostentatious and results in a maudlin effect." 

Premiered in Macon, Georgia, Scotts hometown, the film went on to commercial success as one of the last of the patriotic productions to be screened during wartime.  In a modern context, the film has received a revival in interest as it is now considered one of the "classic" aviation films primarily due to its aerial scenes, which were even at the time, considered one of its assets.  Along with Scotts role in telling the story of the Flying Tigers, God is My Co-Pilot is now considered more as a historical record. 

==References==
;Notes
 
;Citations
 
;Bibliography
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Hardwick, Jack and Ed Schnepf, . "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 