Tale of Two Mice
 
{{Infobox Hollywood cartoon
|cartoon_name=Tale Of Two Mice
|series=Looney Tunes
|image=TaleofTwoMiceOriginalTitle.jpg
|caption=The original title card of A Tale Of Two Mice.
|director=Frank Tashlin
|story_artist=Warren Foster Art Davis
|layout_artist=Richard H. Thomas
|voice_actor=Mel Blanc Tedd Pierce
|musician=Carl Stalling
|producer=
|distributor=Warner Bros.
|release_date=June 30, 1945
|color_process=Technicolor
|runtime=7:48
|movie_language=English
}}
Tale of Two Mice is a 1945 Warner Bros. cartoon in the Looney Tunes series, directed by Frank Tashlin. It is a sequel to 1942s A Tale of Two Kitties, with the Abbott and Costello characterizations ("Babbit and Catstello") now cast as mice. They are voiced by Tedd Pierce and Mel Blanc respectively.

==Plot==
Babbit sends Catstello to get some cheese from the refrigerator putting Catsello in danger and peril with a cat, until finally Catsello obtains a block of cheese. Babbit doesnt want it, as he doesnt like Swiss cheese. Fed up with Babbits bossiness, Catsello force feeds Babbit the Swiss cheese.

==Edited versions==
*The (now defunct) WB! channel cut a scene where Catstello asks Babbit if flying on a toy plane to get the cheese will work, with Babbit stating that if it doesnt work, then hes a jackass, and when the plane crashes back in the mouse hole, Catstello bellows, "Jackass! A jackass!" and brays like a donkey.

==See also==
*Looney Tunes and Merrie Melodies filmography (1940–1949)

==External links==
* 
* 
*  - with original titles

 
 
 
 
 


 