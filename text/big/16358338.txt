The Man Who Smiles
{{Infobox film
| name           = Luomo che sorride
| image          =
| image size     =
| caption        =
| director       = Mario Mattoli
| producer       = Giuseppe Amato
| writer         = Luigi Bonelli
| narrator       =
| starring       = Vittorio De Sica, Umberto Melnati, Enrico Viarisio, Assia Noris and Paola Borboni
| music          = Cesare A. Bixio
| cinematography = Arturo Gallea
| editing        = Fernando Tropea
| distributor    =
| released       = 1936
| runtime        =
| country        = Italy Italian
}}
 1936 Italian comedy film about an Oedipus Complex, directed by Mario Mattoli. The film stars Vittorio De Sica,  Umberto Melnati, Enrico Viarisio, Assia Noris and Paola Borboni.

The film premiered in the USA on 16 April 1937.

==Cast==
*Vittorio De Sica as  Pio Fardella
*Umberto Melnati as  Dino
*Enrico Viarisio as  Commendator Ercole Piazza
*Assia Noris as  Adriana
*Paola Borboni as  La contessa
*Armando Migliari as  Agostino
*Luisa Garella as  Edvige
*Vanna Vanni
*Ermanno Roveri

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 