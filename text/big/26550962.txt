Jumping for Joy
{{Infobox film
| name           = Jumping for Joy
| image          = "Jumping_For_Joy"_(1956).jpg
| image_size     = 
| caption        = Original campaign book
| director       = John Paddy Carstairs
| producer       = Raymond Stross Jack Davies
| narrator       = 
| music          = Larry Adler
| cinematography = Jack E. Cox
| editing        = John D. Guthridge
| starring       = Frankie Howerd   Stanley Holloway   Joan Hickson   Lionel Jeffries
| distributor    = The Rank Organisation
| released       = 1956
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Jumping for Joy is a 1956 British comedy film directed by John Paddy Carstairs and starring Frankie Howerd, Stanley Holloway, Joan Hickson and Lionel Jeffries.

==Plot summary== doping dogs.  

==Cast==
* Frankie Howerd - Willie Joy
* Stanley Holloway - Captain Jack Montague
* A.E. Matthews - Lord Reginald Cranfield Tony Wright - Vincent
* Alfie Bass - Blagg
* Joan Hickson - Lady Emily Cranfield
* Lionel Jeffries - Bert Benton
* Susan Beaumont - Susan Storer
* Terence Longdon - John Wyndham
* Colin Gordon - Max, the commentator
* Richard Wattis - Carruthers Danny Green - Plug Ugly
* Barbara Archer - Marlene William Kendall - Blenkinsop
* Ewen Solon - Haines
* Reginald Beckwith - Smithers

==Critical reception==
Halliwells Film and Video Guide 2000 describes the film as a "totally predictable star comedy which needs livening up"  and the Time Out Film Guide 2009 describes the film as "lame".  While TV Guide called the film a "Sporadically funny comedy". 

==Musical score==
The New York Times noted, "the delightful harmonica score in Jumping for Joy is provided by American expatriate Larry Adler". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 