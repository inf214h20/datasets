Operation C.I.A.
{{Infobox film
| title = Operation C.I.A.
| image = OPCIApos.jpg
| caption = Original film poster
| director = Christian Nyby
| producer = Peer J. Oppenheimer
| writer = Bill S. Ballinger Peer J. Oppenheimer
| starring = Burt Reynolds Danielle Aubry John Hoyt Kieu Chinh Vic Diaz Marshall Thompson
| music = Paul Dunlap Richard Moore
| editing = Joseph Gluck George Watters Allied Artists
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = 
| gross =
}}
Operation C.I.A. is a 1965 black-and-white spy thriller film set in Saigon but filmed in Bangkok. Written by Bill S. Ballinger wrote a series of spy thrillers at the time and is also Burt Reynolds first lead role. Allied Artists had previously filmed their A Yank in Viet-Nam on actual South Vietnamese locations but by 1965 the security situation had deteriorated to such an extent that the safety of the film makers could not be guaranteed.   The film was originally titled Last Message from Saigon with an announcement made in 1964 it would be filmed in Saigon, Hong Kong and Bangkok.

==Plot==
Secret Agent Mark Andrews is sent to Saigon where he prevents the assassination of the American Ambassador. 

==Popular references== Archer in the 4th episode of the 3rd season by Burt Reynolds (played by himself), who described the movie as "God awful".

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 