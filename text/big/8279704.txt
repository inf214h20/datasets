The James Brothers of Missouri
{{Infobox film
| name           = The James Brothers of Missouri
| image          = Poster of the movie The James Brothers of Missouri.jpg
| director       = Fred C. Brannon
| producer       = Franklin Adreon
| writer         = Royal Cole William Lively Sol Shor Keith Richards Robert Bice Noel Neill Roy Barcroft Patricia Knox Lane Bradford Stanley Wilson
| cinematography =Ellis W. Carter
| distributor    = Republic Pictures
| released       =   31 August 1949 {{Cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 114–115
 | chapter =
 }} 
| runtime         = 12 chapters / 167 minutes 
| language        = English
| budget          = $164,986 (negative cost: $164,757) 
| awards          =
}}
 Republic Serial film serial.

==Cast== Keith Richards as Jesse James
* Robert Bice as Frank James
* Noel Neill as Peg Royer
* Roy Barcroft as Ace Marlin
* Patricia Knox as Belle Calhoun
* Lane Bradford as Monk Tucker

==Production==
The James Brothers of Missouri was budgeted at $164,986 although the final negative cost was $164,757 (a $229, or 0.1%, under spend). 

It was filmed between 6 July and 27 July 1949.  The serials production number was 1705. 

===Stunts=== David Sharpe Tom Steele
* Dale Van Sickel

===Special effects===
Special effects creasted by the Lydecker brothers.

==Release==
===Theatrical===
The James Brothers of Missouris official release date is 31 August 1949, although this is actually the date the sixth chapter was made available to film exchanges. 

==Critical reception==
Cline dismisses this serial as a "quick warm-over" of the first two Jesse James serials. {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 3. The Six Faces of Adventure
 | page = 42
 }} 

==Chapter titles==
# Frontier Renegades (20min)
# Racing Peril (13min 20s)
# Danger Road (13min 20s)
# Murder at Midnight (13min 20s)
# Road to Oblivion (13min 20s)
# Missouri Manhunt (13min 20s)
# Hangmans Noose (13min 20s)
# Coffin on Wheels (13min 20s)
# Dead Mans Return (13min 20s)
# Galloping Gunslingers (13min 20s) - a clipshow|re-cap chapter
# The Haunting Past (13min 20s)
# Fugitives Code (13min 20s)
 Source:   {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 249
 }} 

==See also==
* Jesse James Rides Again (1947) - earlier Jesse James Serial
* Adventures of Frank and Jesse James (1948) - earlier Jesse James Serial
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  

 
{{Succession box Republic Serial Serial
| before=King of the Rocket Men (1949 in film|1949)
| years=The James Brothers of Missouri (1949 in film|1949)
| after=Radar Patrol vs Spy King (1949 in film|1949)}}
 
 
 
 

 
 
 
 
 
 
 
 
 