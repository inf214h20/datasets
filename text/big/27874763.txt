Sri Simhachala Kshetra Mahima
{{Infobox film
| name           = Sri Simhachala Kshetra Mahima
| image          = Sri Simhachala Kshetra Mahima.jpg
| image_size     =
| caption        =
| director       = B. V. Prasad
| producer       =
| writer         = B. V. Prasad (story) Rajashri (dialogues)
| narrator       = Krishna Kumari Girija Rajanala Rajanala Chalam G. Ramakrishna
| music          = T. V. Raju
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1965
| runtime        =
| country        = India Telugu
| budget         =
}}
Sri Simhachala Kshetra Mahima is a 1965 Telugu film written and directed by B. V. Prasad on his debut. 

The film is based on the stories related to Sri Varaha Narasimha Kshetram of Simhachalam in Andhra Pradesh, India.

==Plot==
The story begins with Mahavishnu in Narasimha Avatar killing Hiranyakashipu. Prahlada requests him to reduce his anger. Quitened Narasimha blesses him. Prahlada requests him to show his Varaha avatar and to bless his devotees on the hill. Mahavishnu obliges him and takes the form of Varaha Narasimha.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Tadepalli Lakshmi Kanta Rao || Pururava Chakravarthi
|- Krishna Kumari || Apsarasa Sirisha
|-
| Chittor V. Nagaiah || Father of Pururava
|-
| G. Ramakrishna || Mahavishnu
|-
| Chalam || Narada
|-
| Rajanala Kaleswara Rao || Indra
|-
| Relangi Venkataramaiah || Anavasaram
|- Girija || Queen of Women Kingdom
|}

==Soundtrack==
There are 12 songs and padyams in the film. 
* Andala O Sundara  Anuragala (Singers: P. Susheela and S. Janaki)
* Baboo Biraana Kanipinchara Kanipinchi Naa Baadha Tholaginchara (Singer: S. Janaki)
* Chinnari Ponnari Vennela Raasi Jo Jo (Singer: S. Janaki)
* Haayiga Edo Teeyaga Naa Kalale Pandaga (Singer: P. Susheela)
* Jayahe Mohana Roopa Gaana Kalaapa (Singer: P. B. Srinivas)
* Neelaati Revukaada Neredu Chettuneeda (Singer: L. R. Eswari)
* O Deva Varaaha Mukha Nrusimha Sikha (Singer: S. Janaki)
* Oho Elika Ide Veduka Ee Vasantha Vinodalu Neeku Kaanuka (Singers: P. Susheela group)
* Oppula Kuppa Vayyari Bhama Magaadu Pilichade (Singers: Pithapuram Nageswara Rao, Swarnalata group)
* Raave Viribala Dari Raave Eevela (Singer: P. B. Srinivas)
* Raavoyi Raaja Kanaraavoyi Raaja (Singer: S. Janaki and P. B. Srinivas)
* Simhachalamu Mahaa Punya Kshetramu (Lyrics:  )

==References==
 

 
 
 


 