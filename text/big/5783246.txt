Quartet (1981 film)
  
{{Infobox film
| name = Quartet
| image = Quartet poster.jpg
| caption = Theatrical release poster James Ivory
| producer = Ismail Merchant
| writer = Ruth Prawer Jhabvala (based on the novel by Jean Rhys) Anthony Higgins Richard Robbins
| cinematography = Pierre Lhomme
| editing = Humphrey Dixon Gaumont (France)
| released = May 1981 (at Cannes Film Festival) 25 October 1981 (US)
| runtime = 101 min
| country = United Kingdom
| language = English, French
| budget =
}}
 Merchant Ivory Anthony Higgins and Alan Bates, set in 1927 Paris. It premiered at the 1981 Cannes Film Festival and was an entry for the Sélection Officielle (Official Selection). It was adapted from the novel by the same name by Jean Rhys.

==Plot==

Stephan, a shady Polish art dealer, is convicted of selling stolen artwork and is sentenced to prison for one year. Mado, his wife of uncertain nationality (possibly Creole), finding herself without any financial resources and at Stephans urging, moves in to the apartment of H.J. Heider, a wealthy Englishman, and his wife Lois, a painter. H.J. has a history of inviting young women to move in to the "spare room" and with whom he then has sexual relations. Lois permits this arrangement because she wants to keep H.J. happy.

Most of the movie is a character study of the four principals. Mado visits Stephan in prison once a week, although both H.J. and Lois complain about it. Mado succumbs to H.J.s advances, and it is unclear how much of it is willing and how much is not.

After Stephan is released from prison, he leaves France but does not take Mado with him.

==Cast==
* Isabelle Adjani as Marya Zelli (nickname Mado)
* Alan Bates as H. J. Heidler
* Maggie Smith as Lois Heidler Anthony Higgins as Stephan Zelli
* Sheila Gish as Anna
* Suzanne Flon as Madame Hautchamp
* Pierre Clementi as Theo
* Daniel Mesguich as Pierre Schlamovitz
* Virginie Thévenet as Mademoiselle Chardin
* Daniel Chatto as Guy
* Armelia McQueen as Night Club Singer

==Awards==
*   for Isabelle Adjani   
*  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 