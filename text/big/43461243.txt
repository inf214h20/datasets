Hot Chocolate Soldiers
 

The Hot Chocolate Soldiers is an American animated short film from Walt Disney Productions, released on 1 June 1934.

==Synopsis==

A group of confectionery soldiers go to war against a neighboring cookie castle.  As the army parade through the city streets, Chocolate Ladies and Soldiers wives blow kisses to bid them goodbye. After multiple thwarted attempts to enter, the soldiers successfully conquer the fort defended by a troop of Gingerbread Soldiers by using a cunning Trojan Horse shaped like a large, candy-colored pigeon.  The soldiers then take their Gingerbread prisoners to their city, but the sun beats down on the soldiers and melts them into a river of chocolate.

==Datasheet==

*Original title: The Hot Chocolate Soldiers
*Other Titles: The Hot Choc-late Soldiers
*Director: Walt Disney
*Supervision scenario: George Stallings
*Voice: Don Wilson (the narrator)
*Animateur: Leonard Sebring, Cy Young, Louie Schmitt, Ben Sharpsteen, Roy Williams, Jack Kinney, Frank Oreb, Milt Schaffer, Bob Kuwahara, Ugo DOrsi, Joe DIgalo
*Sets: Emil Flohri Carlos Manriquez
*Producer: Walt Disney
*Distributor: Metro-Goldwyn-Mayer
*Output dates: 1 June 1934
*Copyright deposit: May 14, 1934
*New York premiere: May 24 to June 7, 1934 Rialto
*Image Format: Color (Technicolor)
*Length1: 5 min 04 seconds
*Language: English
*Country: Flag of United States United States

==Lyrics==


THE HOT CHOC-LATE SOLDIERS (1934)


Look at the brave hot chocolate soldiers, marching away to war,
Followed by more hot chocolate soldiers, fresh from the candy store

Look at the band thats going with them, look at the drums theyve got,
Marching in syncopated rhythm, rhythm that makes them hot

Here they come, here they come, with a candy fife and a chocolate drum,
Here they come, here they come, with their tummies full of rum

Look at those lovely chocolate ladies, giving a parting sigh,
Tillies and Flos and Kates and Sadies, throwing a kiss goodbye

Listen to me, hot chocolate soldier, hide from the flaming sun,
Dont get too hot, hot chocolate soldier, til the battles won

Theyre off to fight the battle of vanilla, Over there, over there

They crossed the springs and mountain tops and through the fields of lollipops,
Its war, its war, the popguns roar!

From planes to strand and candy hight,
Theyll bomb the foe with cracker-jack

Theyre off to fight those great big gingerbread men,
And theyll come marching home


Look at the brave hot chocolate soldiers, marching away from war,
Followed by more hot chocolate soldiers, back to the candy store

Look at the band thats coming with them, look at the drums theyve got,
Marching in syncopated rhythm, rhythm that makes them hot

Here they come, here they come, with a candy fife and a chocolate drum,
Here they come, here they come, with their tummies full of rum

Look at those lovely chocolate ladies, waving to greet their men,
Tillies and Flos and Kates and Sadies, happy as brides again

Listen to me, hot chocolate soldier, hide from the flaming sun,
Dont get too hot, hot chocolate soldier, now the battles won


http://www.subzin.com/search.php?title=Hollywood+Party&title_id=M71378d5&search_sort=Popularity&type=All&pag=34

http://www.subzin.com/search.php?title=Hollywood+Party&title_id=M71378d5&search_sort=Popularity&type=All&pag=35

==External links==

* 
* 
* 

==References==
 

 
 

 