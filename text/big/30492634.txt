Flight North
{{Infobox film
| name           = Flight North
| image          = 
| image size     = 
| caption        = 
| director       = Ingemo Engström
| producer       = Ingemo Engström
| writer         = Ingemo Engström Klaus Mann
| starring       = Katharina Thalbach
| music          = 
| cinematography = Axel Block
| editing        = Thomas Balkenhol
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = West Germany Finland
| language       = Finnish
| budget         = 
}}

Flight North ( ,  ) is a 1986 German-Finnish drama film directed by Ingemo Engström. It was entered into the 36th Berlin International Film Festival.   

==Cast==
* Katharina Thalbach as Johanna
* Jukka-Pekka Palo as Ragnar
* Lena Olin as Karin
* Tom Pöysti as Jens
* Britta Pohland as Suse
* Käbi Laretei as Mother

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 