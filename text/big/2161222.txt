Sarkar (film)
 
 
{{Infobox film
| name           = Sarkar
| image          = Sarkar movie poster.jpg
| caption        = Theatrical release poster
| director       = Ram Gopal Varma
| producer       = Parag Sanghavi Ram Gopal Varma
| written by     = Manish Gupta
| narrator       =
| starring       = Amitabh Bachchan Abhishek Bachchan Kay Kay Menon Rukhsar Rehman 
Kota Srinivasa Rao Katrina Kaif Tanisha Mukherjee Anupam Kher
| music          = Background Score: Amar Mohile Songs: Bapi-Tutul
| cinematography = Amit Roy
| editing        = Nitin Gupta Amit Parmar
| studio         =
| distributor    = K Sera Sera Sahara One
| released       =  
| runtime        = 123 mins
| country        = India
| language       = Hindi
|  budget        =   http://ibosnetwork.com/asp/filmbodetails.asp?id=Sarkar 
| gross          =   
}}
 politics and Crime film|crime, directed by Ram Gopal Varma, inspired  partly by the The Godfather.
The film stars Amitabh Bachchan in the title role alongside his real-life son, Abhishek Bachchan as the younger son, Shankar Nagre, along with Kay Kay Menon, Katrina Kaif, Anupam Kher, Supriya Pathak and Tanisha Mukherjee. 
 American Academy of Motion Pictures library.  The blockbuster film was premièred at the New York Asian Film Festival.   

==Plot== Don Corleone in The Godfather for same reason) for justice (which the corrupt law and order system has failed to deliver) which Sarkar promptly establishes by having the rapist beaten up by his henchmen. His son, Vishnu (Kay Kay Menon), plays a sleazy producer who is more interested in the film actress Sapna (Nisha Kothari) than his wife Amrita (Rukhsar). Sarkars other, more upright son, Shankar (Abhishek Bachchan), returns from the United States with his love Pooja (Katrina Kaif) after completing his education there. Poojas doubts about Sarkars image cause Shankar, who firmly believes in his fathers righteousness, to break up with her later in the movie.
 Zakir Hussain) tries to strike a deal with Sarkar; he promptly refuses on moral grounds and also forbids him from doing it himself. Rasheed tries to eliminate Sarkars supremacy with the help of Selvar Mani (Kota Srinivasa Rao), Sarkars former associate and Swami Virendra (Jeeva (Telugu actor)|Jeeva). Meanwhile, they trap Sarkar by assassinating a righteous, upright, Ahimsa political leader and an outspoken critic of Sarkar, Motilal Khurana (Anupam Kher). Everyone, including Vishnu believe that Sarkar is guilty but Shankar has deep faith in his father. Sarkar gets arrested. Shankar now takes over the position of Sarkar temporarily. On learning of a plot to murder his father in prison, he approaches the police commissioner (Anant Jog) who mocks him and his father besides not providing protection. By the time he reaches the prison and appropriate action is taken, the attempt on Sarkars life is already made. Sarkar is later acquitted. He remains bedridden as Shankar takes on Sarkars enemies. Meanwhile, Selva Mani, Swami and Rasheed try to convince Vishnu to murder Sarkar. Vishnu was previously thrown out of Sarkars house because he had murdered the actor who was having an affair with Sapna. Vishnu returns home pretending to have repented. When he approaches Sarkar in the dark of the night with the intent of murdering him, Shankar foils his plan and later kills him (establishing justice by the way of his father). Shankar eliminates Rasheed and Selva Mani. He also succeeds in making Swami his puppet. Shankar has also realised that Chief Minister Madan Rathore (Deepak Shirke) also has a part in the attempt to end Sarkar and his rule. This results in legal action against the Chief Minister. The closing scenes show people approaching Shankar for justice and his father apparently retired.

==Cast==

* Amitabh Bachchan as Subhash Nagre (Sarkar)
* Abhishek Bachchan as Shankar Nagre
* Kay Kay Menon as Vishnu Nagre
* Katrina Kaif as Pooja (Mona Ghosh Shetty as the dubbing voice)
* Tanisha Mukherjee as Avantika
* Anupam Kher as Motilal Khurana
* Supriya Pathak as Pushpa Nagre
* Rukhsaar Rehman as Amrita (Vishnus wife)
* Ishrat Ali as Khansaab
* Raju Mavani as Vishram Bhagat Zakir Hussain as Rashid
* Kota Srinivasa Rao as Selvar Mani
* Ravi Kale as Chander
* Virendra Saxena as Girls Father
* Anant Jog as Police Commissioner
* Deepak Shirke as Madan Rathod
* Carran Kapoor as a Hero in Vishnus film
* Nisha Kothari as a Heroine in Vishnus film
* Jeeva as Virendra Swami
* Mangal Kenkre as Shoba
* Saurabh Dubey as Poojas Father

==Commercial and critical reception==
Sarkar was superhit at the box office and was greeted very well by critics. Critics liked the way the movie indigenised The Godfather and introduced a political angle to it. The actors, particularly the trio of Amitabh Bachchan, Abhishek Bachchan and Kay Kay Menon, were lavished with praise for their controlled yet intense performances.
Abhishek Bachchan in particular, was rewarded for his portrayal as a son who is inexperienced but enters an unknown world so as to save his father. He won the following awards:

* Filmfare Best Supporting Actor Award
* Zee Cine Award Best Actor in a Supporting Role - Male IIFA Award for Best Supporting Actor

==Soundtrack==
The music of the film is composed by Bapi and Tutul. Lyrics are penned by Sandeep Nath.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Deen Bandhu
| extra1 = Reeta Ganguli
| lyrics1 = Sandeep Nath
| length1 = 4:36
| title2 = Deen Bandhu Theme
| extra2 =
| lyrics2 =
| length2 = 4:24
| title3 = Govinda - Song
| extra3 = Amitabh Bachchan, Kailash Kher, Bapi, Tutul
| lyrics3 = Sandeep Nath
| length3 = 2:59
| title4 = Govinda - Trance
| extra4 = Bapi, Tutul, Janaki
| lyrics4 = Sandeep Nath
| length4 = 3:25
| title5 = Jitni Oochaeeyan
| extra5 = Krishna, Farhad
| lyrics5 = Sandeep Nath
| length5 = 3:27
| title6 = Mujhe Jo Sahi Lagta Hai
| extra6 = Amitabh Bachchan, Kailash Kher
| lyrics6 = Sandeep Nath
| length6 = 3:03
| title7 = Sam Dam Bhed
| extra7 = Kailash Kher
| lyrics7 = Sandeep Nath
| length7 = 3:29
| title8 = Shaher, Shaher Ke Hazaron Sawal
| extra8 = Kailash Kher
| lyrics8 = Sandeep Nath
| length8 = 3:54
| title9 = The Govinda Omen
| extra9 = Choir
| lyrics9 = Sandeep Nath
| length9 = 1:58
| title10 = The Want For Power
| extra10 = Krishna, Farhad, Prasana Shekhar
| lyrics10 = Sandeep Nath
| length10 = 2:04

}}

==Remake== Telugu remake, Rowdy had Telugu as well..

==Sequel==
A sequel titled Sarkar Raj was released on 6 June 2008 with Amitabh Bachchan, Abhishek Bachchan (who reprise their roles from the original) and new entrant Aishwarya Rai Bachchan. Supriya Pathak, Tanisha Mukherjee and Ravi Kale also reappeared in their respective roles from Sarkar. The film released on 6 June 2008, was critically and commercially successful.

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 