Her Twelve Men
{{Infobox film
| name = Her Twelve Men
| image_size = 190px
| image	= Her Twelve Men FilmPoster.jpeg
| caption = Theatrical release poster
| director = Robert Z. Leonard
| producer = John Houseman William Roberts Laura Z. Hobson Louise Baker (story)
| starring =
| music = Bronislau Kaper
| cinematography = Joseph Ruttenberg
| editing = George Boemler
| studio =
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| budget = $1,534,000  . 
| gross = $1,418,000 
}}  William Roberts and Laura Z. Hobson, based on story by Louise Baker .

==Plot==
Although she has no teaching experience, widow Jan Stewart is hired by headmaster Dr. Barrett to be the first woman to teach at The Oaks boarding school.

Jan gets to know her twelve students and fellow faculty member Joe Hargrave, who is dating the rich Barbara Dunning. She has so much sympathy for one young boy, Bobby Lennox, whose globe-trotting parents neglect him, that she reads letters pretending they are from his mother that Jan wrote herself.

A wealthy Texan widower, Richard Oliver, enrolls his son. Richard Jr. instantly alienates the other boys with his attitude and by refusing to confess to causing a fire alarm to go off, for which Dr. Barrett punishes the entire class.

The boys father wants him sent home and Jan is asked to accompany him on the journey. She wins young Richards trust and gains Richard Sr.s interest as well. He ends up proposing, but Jan and Joe ultimately realize they were meant for one another.

==Cast==
*Greer Garson as  Jan Stewart 
*Robert Ryan as  Joe Hargrave  Barry Sullivan as  Richard Y. Oliver, Sr. 
*Richard Haydn as Dr. Avord Barrett 
*Barbara Lawrence as  Barbara Dunning 
*James Arness as  Ralph Munsey 
*Rex Thompson as  Homer Curtis 
*Tim Considine as Richard Y. Oliver, Jr. 
*David Stollery as  Jeff Carlin 
*Frances Bergen as  Sylvia Carlin 
*Ian Wolfe as  Roger Frane  Donald MacDonald as  Bobby Lennox 
*Dale Hartleben as  Kevin Ellison Clark III 
*Ivan Triesault as  Erik Haldeman

==Reception==
According to MGM record the film earned $817,000 in the US and Canada and $601,000 elsewhere, resulting in a loss of $116,000. 

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 


 