Mama Bhagne (film)
 

{{Infobox film
| name           = Mama Bhagne
| image          = MamaBhagne.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Anup Sengupta
| producer       = Ishika Films Pvt. Ltd.
| writer         = Manjil Bandyopadhyay
| screenplay     = Manjil Bandyopadhyay 
| story          = Manjil Bandyopadhyay
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Ananya Chatterjee Ranjit Mallick Biswajit Chakraborty
| music          = Subhayu Bedajna
| cinematography = Badal Sarkar
| editing        = Atish Dey Sarkar
| studio         = 
| distributor    = Moser Baer
| released       = Sep 17, 2009
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Anup Sengupta and produced by Ishika Films Pvt. Ltd. under the banner of  Ishika Films Pvt. Ltd.The film features actors Prosenjit Chatterjee and Ranjit Mallick and Ananya Chatterjee in the lead roles. Music of the film has been composed by Subhayu Bedajna.   

== Cast ==
* Prosenjit Chatterjee as a Raja
* Ranjit Mallick as a Rajas Maternal Uncle
* Ananya Chatterjee as Payel
* Biswajit Chakraborty as Payels Father
* Arpita Mukherjee
* Raja Chattopadhyay as G.P.(Govinda Purakaysto)
* Shankar Chakraborty

== References ==
 

 
 
 


 