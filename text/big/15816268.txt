Devil's Doorway
{{Infobox film
| name           = Devils Doorway
| image          = O Images2.jpg
| image_size     = 225px
| alt            =
| caption        = Theatrical release poster
| director       = Anthony Mann
| producer       = Nicholas Nayfack
| screenplay     = Guy Trosper
| narrator       = Robert Taylor Louis Calhern Paula Raymond
| music          = Daniele Amfitheatrof
| cinematography = John Alton
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =$1,373,000  . 
| gross          = $2,096,000 
}} Robert Taylor as an Indian who returns home from the American Civil War a hero awarded the Medal of Honor. However, his hopes for a peaceful life are shattered by bigotry and greed.

==Plot== Civil War may be over back East, but prejudice still rules the West. A full-blooded Shoshone Indian named Lance Poole distinguished himself in the war, winning the Congressional Medal of Honor, only to return home to Medicine Bow, Wyoming, to something a far cry from a heros welcome.

Townspeople resent the fact that Lance and his father own a large and valuable piece of land. A doctor refuses to treat Lances father, who dies, while Lance himself is unable to even buy a drink in the local saloon. A loophole in a law involving homesteaders is used by biased attorney Verne Coolan to strip Lance of his property. Lance turns to a female lawyer, Orrie Masters, who fails to acquire the necessary petition signatures they need to overturn the law.

Coolan organizes sheepherders and attempts to drive out Lance by force. Shoshone tribesmen fight by Lances side, using his cabin for a fort. Orrie calls in the U.S. Cavalry to create a truce, only to have them side with Coolan and the town. Its a lost cause. Lance is at least able to kill Coolan, but not before being already seriously wounded himself at the Shoshone barricade.

Lance Poole then turns the responsibility for the surviving women and children over to the only surviving male child, who leads them away from the barricade and presumably in the direction of the reservation. Afterward, Lance puts on his civil war sergeants major uniform, and walks out to the cavalry commander and his former lawyer. The commander salutes Poole first, as that is the custom when greeting a medal of honor recipient. Lance then dies from exsanguination.

==Cast== Robert Taylor as Lance Poole
*Louis Calhern as Verne Coolan
*Paula Raymond as Orrie Masters
*Marshall Thompson as Rod MacDougall James Mitchell as Red Rock
*Edgar Buchanan as Zeke Carmody Rhys Williams as Scotty MacDougall
*Spring Byington as Mrs. Masters
*James Millican as Ike Stapleton
*Bruce Cowling as Lieutenant Grimes Fritz Leiber as Mr. Poole

==Reception==
According to MGM records the film earned $1,349,000 in the US and Canada and $747,000 overseas, resulting in a profit of $25,000. 
===Critical response=== Broken Arrow, is a Western with a point of view that rattles some skeletons in our family closet. Robert Taylor may strike you as a rather peculiar choice to play a full-blooded Indian, but give the man credit for a forceful performance. Indeed, his is the only role that is not a stereotype. However, the other players give good performances even though they represent characters that are as much a part of the Western film formula as horses and sagebrush." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 