Avanti Popolo (1986 film)
 
{{Infobox film
| name           = Avanti Popolo
| image          = 
| caption        = 
| director       = Rafi Bukai
| producer       = 
| writer         = Rafi Bukai
| starring       = Salim Dau
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Israel
| language       = Arabic
| budget         = 
}}
 Best Foreign Language Film at the 59th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Salim Dau as Haled el Asmar
* Suhel Haddad as Gassan Hamada
* Tuvia Gelber as David Pozner
* Danny Segev as Yacaov Hirsh
* Dani Roth as Dani Sela Barry Langford as English Journalist
* Michael Koresh as Military Attache

==See also==
* List of submissions to the 59th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 