How to Save a Marriage and Ruin Your Life
{{Infobox film
| name           = How to Save a Marriage and Ruin Your Life
| image          = 
| image_size     = 
| caption        = 
| director       = Fielder Cook
| writer         = Stanley Shapiro
| narrator       = 
| starring       = Dean Martin
| music          = Michel LeGrand and The Ray Conniff Singers
| cinematography = 
| editing        = 
| studio         = Nob Hill Productions Inc.
| distributor    = Columbia Pictures
| released       = January 29, 1968
| runtime        = 102 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $2,500,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
How to Save a Marriage and Ruin Your Life is a 1968 film directed by Fielder Cook. It stars Dean Martin and Stella Stevens. 

==Plot==
David Sloane is an attorney and a bachelor whose married pal Harry Hunter is having an affair. David decides to do something about it so Harry doesnt mess up his home life.

The scheme is to make a play for Harrys mistress himself. David meets and courts Harrys attractive employee, Carol Corman, determined to break up her fling with Harry once and for all.

Davids plan goes wrong because he has the wrong woman. Harrys actual mistress is Carols next-door neighbor, Muriel Laszlo. As soon as he learns (mistakenly) that she is seeing another man, Harry decides to give his marriage to Mary one more try.

Carol and Muriel come to realize what happened. They decide to team up, giving David and Harry a taste of their own medicine.

==Cast==
*Dean Martin as David Sloane
*Stella Stevens as Carol Corman
*Eli Wallach as Harry Hunter
*Anne Jackson as Muriel Laszlo
*Betty Field as Thelma
*Jack Albertson as Mr. Slotkin 
*Alan Oppenheimer as Everett Bauer 
*George Furth as Roger
*Woodrow Parfrey as Eddie Rankin

==References==
 

==External links==
* 

 

 
 
 
 
 

 