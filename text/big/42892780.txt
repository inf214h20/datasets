Indulekha (1967 film)
{{Infobox film
| name           = Indulekha
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Kalanilayam Krishnan Nair
| producer       = Kalanilayam Krishnan Nair
| writer         = 
| screenplay     = Vaikom Chandrasekharan Nair
| story          = O. Chandu Menon
| based on       =  
| narrator       = 
| starring       = Rajmohan Sreekala Sankaradi
| music          = V. Dakshinamoorthy
| cinematography = T. N. Krishnankutty Nair Chandran
| editing        = V. P. Varghese
| studio         = Kalanilayam Theatres]
| distributor    = Seetha Films Leela Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Indulekha (1967) is a film adaptation of the 1889 Malayalam novel Indulekha (novel)|Indulekha by O. Chandu Menon. The film was directed and produced by Kalanilayam Krishnan Nair under the banner of Kalanilayam Theatres. Krishnan Nairs son Rajmohan played the role of Madhavan while that of the titular character Indulekha was played by Sreekala. 

==Cast==
 
* Rajmohan as Madhavan
* Sreekala as Indulekha
* Sankaradi
* Aravindaksha Menon
* Vaikom Mani
* Changanassery Natarajan
* Pappanamkodu Lakshmanan
* T. C. N. Nambiar
* Kandiyoor Parameswaran Kutty
* Cherthala Raman Nair
* Muthukulam N. K. Achary
* Ravikumar
* P.C. Appan
* Ayyappan Pillai
* Kodungalloor Ammini Amma
* Thankamani
* Omana
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Pappanamkodu Lakshmanan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambiliye Arikilonnu || P. Leela, Kamukara || Pappanamkodu Lakshmanan ||
|-
| 2 || Kanneer || Ammini || Pappanamkodu Lakshmanan ||
|-
| 3 || Kannetha Doore || P. Leela || Pappanamkodu Lakshmanan ||
|-
| 4 || Maanasam Thirayunnuthaare || P. Leela, Kamukara || Pappanamkodu Lakshmanan ||
|-
| 5 || Manuja || Gangadharan || Pappanamkodu Lakshmanan ||
|-
| 6 || Nale Varunnu Thozhi || P. Leela || Pappanamkodu Lakshmanan ||
|-
| 7 || Poothaaliyundo || Kamukara, Ammini || Pappanamkodu Lakshmanan ||
|-
| 8 || Salkkalaadevithan || Kamukara, Gangadharan, Ammini || Pappanamkodu Lakshmanan ||
|-
| 9 || Varivande Nee Mayangi Veenu || Kamukara || Pappanamkodu Lakshmanan ||
|-
| 10 || Vazhithaara || Gangadharan || Pappanamkodu Lakshmanan ||
|}

==References==
 

==External links==
*  

 
 
 

 