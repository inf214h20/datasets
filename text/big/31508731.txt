Freelancers (film)
{{Infobox film
| name           = Freelancers
| image          = Freelancers (film).jpg
| caption        =
| director       = Jessy Terrero Randall Emmett George Furla
| writer         = L. Philippe Casseus 
| starring       = 50 Cent  Forest Whitaker Robert De Niro
| music          = 
| cinematography = 
| editing        = Kirk M. Morri
| distributor    = Lionsgate 
| studio         = Grindstone Entertainment Group Cheetah Vision Emmett/Furla/Oasis Films|Emmett/Furla Films Envision Entertainment Corporation
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $20 million  
| gross          =
}}
Freelancers is a crime-drama film directed by  . It had a limited release in theaters in Los Angeles and New York on August 10, 2012 and was released on DVD and Blu-ray on August 21, 2012.   

== Plot == turning states evidence, Maldonado frames Sarcone for a theft from powerful mob boss Gabriel Baez. Baez orders Maldonado to kill one of his friends as a sign of loyalty, which he reluctantly does. With Baezs help, Maldonado then kills the dirty cops responsible for his fathers death. At the end, the District Attorneys office approaches Maldonado, urges him not to take Sarcones place as Baezs right-hand man, and Maldonado ponders whether to go straight or not.

== Cast ==
* 50 Cent as Jonas "Malo" Maldonado
* Forest Whitaker as Lieutenant Detective Dennis Lureu
* Robert De Niro as Captain Joe Sarcone
* Matt Gerald as Billy Morrison
* Beau Garrett as Joey
* Malcolm Goodwin as A.D. Valburn
* Robert Wisdom as Terrence Burke
* Dana Delany as Lydia Vecchio
* Vinnie Jones as Sully
* Pedro Armendariz Jr. as Gabriel Baez
* Michael McGrady as Robert Jude
* Andre Royo as Daniel Maldonado
* Jeff Chase as Angie
* Jesse Pruett as Mercer Bartender

== Production ==
In February 2011, Variety (magazine)|Variety announced that 50 Cent will play a lead role in the film.  He will star as the "son of a slain NYPD officer who joins the force and welcomed by his father’s former partner into the ranks of his vice crime task force — and on to a team of rogue Gotham cops." A month later, Deadline (magazine)|Deadline reported that Robert De Niro and Forest Whitaker had joined the film.  Variety (magazine)|Variety announced in May of that year that Dana Delany would play a lead female role.   50 Cent said that he felt compassion for his character despite the characters ambiguous morality. That and other aspects of the film that he could identify with from his own experiences drew him to the project. 

== Release ==
Freelancers had a limited release on August 10, 2012, and was released on home video on August 21, 2012.   It was number six in the top ten DVD and Blu-ray rentals in the week of August 30, 2012. 

== Reception ==
R. L. Shaffer of IGN rated it 3/10 and wrote, "Freelancers is a dull, lifeless cop drama built on cliches, powered by throwaway supporting performances from Forest Whitaker and Robert De Niro."   William Harrison of DVD Talk rated it 1.5/5 and wrote, "Freelancers gives its audience nothing it hasnt seen a hundred times before in better cop dramas. This tale of corruption and retaliation is predictable and largely uninteresting, and actors 50 Cent, Robert De Niro and Forest Whitaker do little more than go through the motions."   Gordon Sullivan of DVD Verdict called it a derivative and forgettable cop drama with a good cast. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 