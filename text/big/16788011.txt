Graduation (film)
{{Infobox film
| name           = Graduation
| image          = Graduationposter08.jpg
| caption        = Promotional poster Michael Mayer
| producer       = Jane Sindell Bob Degus Scott Hanson Robin Bradford
| writer         = Michael Mayer D. Cory Turner
| starring       = Chris Lowell Shannon Lucio Riley Smith
| music          = Brian Ralston
| cinematography = Matthew Uhry
| editing        = Jane Kurson
| studio         = Blumhouse Productions
| distributor    = Redwood Palms Pictures Truly Indie
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English Spanish
| budget         = 
| gross          = 
}}
Graduation is a 2007 coming-of-age film starring Chris Lowell, Chris Marquette and Riley Smith released theatrically in May 2008. It was made in collaboration with the producers of The Virgin Suicides and Seabiscuit (film)|Seabiscuit. The film was shot in Beaver Falls, Pennsylvania in 2005.  Additional scenes were shot in and outside of North Hills Senior High School.

==Plot==
A group of high school friends from suburban Pittsburgh—Carl (Chris Marquette), Polly (Shannon Lucio), Chauncey (Riley Smith) and Jackson (Chris Lowell)—are about to graduate. Polly and Chauncey are dating, Carl needs a date to prom and Jackson doesnt seem to know what to do with his life. When Carls mom becomes ill and needs $100,000 for surgery, Polly comes up with a plan: rob her dads bank. The plan seems foolproof, and graduation is the perfect alibi; all the kids need to do is steal the keys to the banks vault. However, things get complicated when Carl falls in love with a bank teller named Suzi and Polly falls in love with Jackson. Through it all—including an unforeseen hostage crisis—the friends learn a lot about themselves.

==Release==
The movie was released theatrically on May 2, 2008 (limited release). It came out on DVD on May 13, 2008.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 