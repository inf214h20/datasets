Secrets (1933 film)
{{Infobox film
| name = Secrets
| image = Secrets 1933 poster.jpg
| caption = Theatrical poster
| director = Frank Borzage
| writer = Rudolph Besier (play) May Edginton (play) Salisbury Field Frances Marion Leonard Praskins Leslie Howard
| producer = Mary Pickford Alfred Newman
| cinematography = Ray June Hugh Bennett
| runtime = 90 minutes
| released =  
| country = United States
| language = English
| distributor = United Artists
}}
 Western film directed by Frank Borzage and starring Mary Pickford in her last film role. The film is a remake of Secrets (1924 film)|Secrets (1924), a silent film starring Norma Talmadge.

In 1930, Pickford had begun a remake of the Norma Talmadge Secrets titled Forever Yours with director Marshall Neilan and actors Kenneth MacKenna and Don Alvarado. After spending $300,000, Pickford stopped production and destroyed all negatives. {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p93 

==Plot==
Wealthy banker and shipowner William Marlowe and his wife Martha have their hearts set on marrying their daughter Mary to English aristocrat Lord Hurley. However, Mary has other ideas. She has fallen in love with John Carlton, one of her fathers clerks. When Mr. Marlowe finds out, he fires John. John decides to go west to make his fortune, then return for Mary, but she insists on going with him. They elope.
 rustle the herd. John rounds up the other ranchers. They catch and hang three of the gang, including Jakes brother, but Jake gets away. Vowing revenge, Jake and his men attack the Carlton home. Fortunately, help arrives and the rustlers are wiped out. The baby succumbs to illness during the gunfight.

Years pass, and the Carltons prosper greatly. Four more children are born, and John runs for governor of the state. They host a party on the night before the election at their mansion. Lolita Martinez, Johns lover, scandalizes everyone by showing up. In private, she insists that Mary free John to marry her. Mary agrees, but John spurns his mistress and begs his wifes forgiveness; she gives it on condition that he tell her about all his prior lovers. Lolita makes public their affair, but John still wins the election.

Later, he becomes a senator, serving for thirty years in Washington, D.C. before deciding to retire and move back to California. This puzzles the couples grown children; Mary explains that they want time for themselves, to enjoy secrets they can share with no one else. When their offspring still oppose their decision, the couple sneak away.

==Cast==
*Mary Pickford as Mary Marlowe Carlton Leslie Howard as John Carlton
*C. Aubrey Smith as Mr. William Marlowe
*Blanche Friderici as Mrs. Martha Marlowe (as Blanche Frederici)
*Doris Lloyd as Susan Channing, Marys friend
*Herbert Evans as Lord Hurley
*Ned Sparks as Sunshine
*Allan Sears as Jake Houser
*Mona Maris as Señora Lolita Martinez
*Huntley Gordon as William Carlton as an Adult
*Ethel Clayton as Audrey Carlton as an Adult
*Bessie Barriscale as Susan Carlton as an Adult
*Theodore von Eltz as Robert Carlton as an Adult
==References==
 
==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 