Io piaccio
{{Infobox film
 | name =Io piaccio
 | image =  Iopiaccio.jpg
 | director = Giorgio Bianchi
 | writer = Marcello Marchesi Vittorio Metz  
 | starring =Walter Chiari
 | music = Nino Rota
 | cinematography = Anchise Brizzi
 | editing = 
 | released = 1955 Italian
 }} 1955 Italian comedy film directed by Giorgio Bianchi.       

== Plot ==
Professor Maldi, a researcher on the company held by Commendatore Tassinetti (Aldo Fabrizi), experiments on various animals, and especially on the capon Gildo, its preparation which should give courage to the men. Pressed by Tassinetti Maldi decides to experiment on himself the latest version of its compound, without waiting to know the reaction of the capon. 

Soon, the shy Maldi finds himself desired by every woman he meets: rather than courage, his discovery provides an irresistible fascination for twenty-four hours.

== Cast ==

* Walter Chiari: Prof. Roberto Maldi 
* Aldo Fabrizi: Commendatore Tassinetti 
* Peppino De Filippo: Nicolino Donati Dorian Gray: Doriana Paris 
* Bianca Maria Fusari: Sandra, Maldis assistant  
* Tina Pica: Sibilla 
* Mario Carotenuto: Marassino 
* Sandra Mondaini:Giovanna
* Lina Volonghi: Lucia, Tassinettis wife  
* Valeria Fabrizi: Wardrobe supervisor
* Enrico Glori: Butler at Caprice nightclub 
* Riccardo Billi: Husband
* Erminio Spalla: Dorianas confidence man
* Bruno Corelli: Director
* Dina Perbellini: Marassinos wife

==References==
 

==External links==
* 

 
  
   
 
 
 
 

 
 
 