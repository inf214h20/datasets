In Dreams (film)
{{Infobox film
| name           = In Dreams
| image          = In dreams.jpg
| image_size     = 
| caption        = 
| director       = Neil Jordan
| producer       = Charles Burke Redmond Morris Stephen Woolley
| screenplay     = Bruce Robinson Neil Jordan
| based on       =  
| starring       = Annette Bening Katie Sagona Aidan Quinn Stephen Rea Robert Downey, Jr. Paul Guilfoyle
| music          = Elliot Goldenthal
| cinematography = Darius Khondji
| editing        = Tony Lawson Leading Role    = Jenn Dragon
| studio         = Amblin Entertainment 
| distributor    = DreamWorks Pictures
| released       = January 15, 1999 (USA) April 30, 1999 (UK)
| runtime        = 98 minutes
| country        = United States English
| budget         = $30 million 
| gross          = $12 million 
}}
In Dreams is a 1999 psychological thriller film directed by Neil Jordan. It stars Annette Bening as a New England illustrator who begins experiencing visions of a missing child who turns out to be her own daughter; through her dreams, she begins having psychic connections to a serial killer responsible for the murder of her daughter and several other local children.

==Plot==

Claire Cooper (Bening) is a suburban housewife and mother who illustrates childrens stories and is married to an airline pilot (Aidan Quinn). Her world unravels when her daughter is kidnapped and brutally murdered.

Claire is haunted by her daughters murderer, a serial killer named Vivian Thompson (Downey). She has several visions of murders he commits next. When doctors diagnose her as psychosis|psychotic, Claire is committed to a mental institution. She receives a vision of Vivian kidnapping another child, so she escapes to track him down and stop him from killing again.

== Cast ==
 
* Annette Bening as Claire Cooper
* Katie Sagona as Rebecca Cooper
* Aidan Quinn as Paul Cooper
* Robert Downey, Jr. as Vivian Thompson
* Paul Guilfoyle as Detective Jack Kay
* Kathleen Langlois as Snow White
* Jennifer Berry as Hunter
* Amelia Claire Novotny as Prince
* Kristin Sroke as Hunter
* Robert Walsh as Man at School Play
* Denise Cormier as Woman at School Play John Fiore as Policeman
* Ken Cheeseman as Paramedic
* Dennis Boutsikaris as Doctor Stevens
* Stephen Rea as Doctor Silverman
* Margo Martindale as Nurse Floyd
* June Lewin as Kindly Nurse
* Pamela Payton-Wright as Ethel
* Krystal Benn as Ruby
* Dorothy Dwyer as Foster Mother
 

==Reception==
  The Exorcist meets A Nightmare on Elm Street, but it simply lacks a new and terrifying take on the dream/reality premise."  The film currently holds a 25% rating on Rotten Tomatoes based on 51 reviews.

==Soundtrack==
The soundtrack to In Dreams was released on January 12, 1999.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 49:27 

| title1          = Agitato Dolorosa
| length1         = 4:59
| extra1          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title2          = Claires Nocturne
| length2         = 2:38
| extra2          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title3          = Pull of Red
| length3         = 2:07
| extra3          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title4          = Appellatron
| length4         = 3:33
| extra4          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title5          = Wraith Loops
| length5         = 3:27
| extra5          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title6          = Rubber Room Stomp
| length6         = 2:00
| extra6          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title7          = Pulled by Red
| length7         = 1:11
| extra7          = Elliot Goldenthal featuring London Metropolitan Orchestra

| title8          = Scytheoplicity
| length8         = 2:26
| extra8          = Elliot Goldenthal featuring London Metropolitan Orchestra
 In Dreams
| length9         = 2:50
| extra9          = Roy Orbison

| title10         = Rebeccas Abduction
| length10        = 4:32
| extra10         = Elliot Goldenthal featuring London Metropolitan Orchestra

| title11         = Premonition Lento
| length11        = 1:42
| extra11         = Elliot Goldenthal featuring London Metropolitan Orchestra

| title12         = While We Sleep
| length12        = 2:36
| extra12         = Elliot Goldenthal featuring London Metropolitan Orchestra

| title13         = Dont Sit Under the Apple Tree (With Anyone Else but Me)
| length13        = 2:17
| extra13         = The Andrews Sisters

| title14         = Andante
| length14        = 3:38
| extra14         = Elliot Goldenthal featuring London Metropolitan Orchestra

| title15         = Elegy Ostinato
| length15        = 4:11
| extra15         = Elliot Goldenthal featuring London Metropolitan Orchestra

| title16         = Dream Baby
| length16        = 4:30
| extra16         = Elizabeth Fraser

}}

== References ==
 

== External links ==
 

*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 