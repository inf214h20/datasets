The Garden (2008 film)
{{Infobox film
| name           = The Garden
| image          = Gardenposter09.jpg
| image_size     =
| caption        = Promotional film poster
| director       = Scott Hamilton Kennedy
| co-producers   = Vivianne Nacif and Dominique Derrenger
| writer         = Scott Hamilton Kennedy
| executive producers = Stuart Sender and Julie Bergman Sender
| starring       =
| music          = Doug DeAngelis Gabriel Tenorio
| cinematography = Scott Hamilton Kennedy
| editing        = Alex Blatt Tyson FitzGerald Scott Hamilton Kennedy
| studio         = Black Valley Films
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English / Spanish
| budget         =
| gross          =
}}
The Garden is a 2008 American documentary film directed by Scott Hamilton Kennedy. It tells the story of the now demolished South Central Farm; a community garden and urban farm located in Los Angeles, California. The Garden details the plight of the farmers who organized and worked on the farm. The owner of the lot decided he didnt want to allow the farmers to use it anymore, and had the garden bulldozed.  The film was nominated for an Academy Award for Best Documentary Feature on 22 January 2009.

The Garden includes appearances by Danny Glover, Daryl Hannah, and Antonio Villaraigosa.

==Awards== Best Documentary Feature in the 81st Academy Awards.

The International Documentary Association nominated it for the Pare Lorentz Award.

It won the Grand Jury Award from the 2008 Silverdocs Documentary Festival.

==See also==
* South Central Farm

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 