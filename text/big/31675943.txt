Poraali
{{Infobox film
| name = Poraali
| image = Poraali poster.jpg
| alt =
| caption =
| director = Samuthirakani
| producer = M. Sasikumar
| writer = Samuthirakani Swati Niveda Vasundhara Kashyap
| music = Sundar C Babu
| cinematography = S. R. Kathiir
| editing = A. L. Ramesh
| studio = Company Productions
| distributor =
| released =  
| runtime = 145 minutes
| country = India
| language = Tamil
| budget =
| gross =
}} Swati alongside Niveda, Vasundhara and Ganja Karuppu in pivotal roles, released on December 1, 2011.  Poraali illustrates an astute fighter who can take on a large, greedy group. The Telugu dubbed version of this film, Sangarshana also released on December 1, 2011. Upon release, the film generally received positive reviews and became super hit, although the Telugu version sank without a trace. The film was remade in Kannada as Yaare Koogadali starring Puneeth Rajkumar which released on December 20, 2012 and turned out to be a super hit.

==Plot==
The story begins on a rainy night, when Ilangkumaran(M. Sasikumar|Sasikumar) and Nallavan(Allari Naresh) escape from a place and enter chennai with some suspense hidden behind their past. They settle at Pulikuttys (Ganja Karuppu) residence and find jobs in a petrol bunk. His honesty and kind heartedness win people. A girl named Bharathi (Swati Reddy|Swathi) comes into his life. Her initial wrong belief about Kumaran changes as soon as she knows his kind-heartedness. Soon,love blooms between them. Ilangkumaran along with his friends start a venture which grows fast. This makes Pulikutty to give advertisement in a Magazine with their photo.

Then comes a group chasing for Kumaran and his friends. Within a few minutes Soori, friend of Kumaran arrives chennai and shares some news. Kumarans old life is shown in the second half and it is revealed that he is mentally affected because of his dad and stepmothers desire for wealth. Finally, the film comes to an end after many interesting twists.

==Cast==
* M. Sasikumar as Ilangkumaran
* Allari Naresh as Nallavan Swati as Bharathi
* Vasundhara Kashyap as Maari
* Niveda as Thamizhselvi
* Jayaprakash as Doctor
* Ganja Karuppu as Pulikutti Soori as Pichai
* G. Gnanasambandam as House owner
* Namo Narayanan
* Gnanavel

==Soundtrack==
{{Infobox album|  
| Name = Poraali
| Longtype = to Poraali
| Type = Soundtrack
| Artist = Sundar C Babu
| Cover =
| Border = yes
| Alt =
| Caption = Original CD cover
| Released =06 November 2011
| Recorded = Audiophiles Feature film soundtrack
| Length = 20.36 Tamil
| Label = Vega Music
| Producer = Sundar C Babu
| Reviews =
| Last album =
| This album =
| Next album =
}}
The soundtrack is scored by Sundar C Babu
{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 20.36
| lyrics_credits = yes
| title1 = Vithiya Potri
| lyrics1 = Yugabharathi
| extra1 = M. Sasikumar, Samuthirakani
| length1 = 2.33
| title2 = Yaar Ivan
| lyrics2 = Na. Muthukumar
| extra2 = Shankar Mahadevan
| length2 = 5.06
| title3 = Vedi Podu
| lyrics3 = Kabilan
| extra3 = Velmurugan & Thanjai Selvi
| length3 = 3.36
| title4 = Yaar Ivan II
| lyrics4 = Na. Muthukumar
| extra4 = Chinmayi
| length4 = 4.39
| title5 = Engiruthu
| lyrics5 = Na. Muthukumar
| extra5 = Chinmayi
| length5 = 1.34
| title6 = Vegamaai Adhivegamaai
| extra6 = Instrumental
| length6 = 2.38
}}

==Release==
The satellite rights of the film were secured by STAR Vijay. The film was given a "U/A" certificate by the Indian Censor Board.

==Critical Reception== Rediff gave the film 3 out of 5, saying that "Poraali is worth a watch".  Indiaglitz stated that "Poraali provides sparkling moments that will linger in our hearts."   A critic from filmics wrote that "Porali is definitely an enjoyable entertainer with a big message."   Anupama Subramanian of Deccan Chronicle said "Script triumphs after the struggle"   Sify rated the film 4/5 stating that "Poraali has its heart in the right place, and Sasikumar’s charismatic, alluring appeal lifts this film considerably."   A critic from Top10cinema wrote that "Poraali has multiple issues running horizontally with the main plot laced in the second half."   According to New Delhi Television|NDTV "Samudrakani manages to get his grip back on the narration, knotting it all up into a fairly engaging whole."   Selena of Cini.in gave it 4.2 out of 5 and calling "A nice movie with a powerful message narrated with the Samudrakani flavour."   Supergoodmovies gave 3/5 and said "There are many interesting things to watch out in Porali."   On the contrary, Rohit Ramachandran of Nowrunning.com rated it 2/5 calling the movie "A vague experience"  Behindwoods also rated it 2 star stating that "This earnest Porali needed more power."  According to IBNLive "Porali lacks finesse and reason". 

==References==
 

==External links==
*  

 

 
 
 
 