Looking for Kitty
{{Infobox film
| name           = Looking for Kitty
| image          = Looking for kitty poster.jpg
| caption        = Movie poster for Looking for Kitty
| director       = Edward Burns
| producer       = Margot Bridger Aaron Lubin
| writer         = Edward Burns
| starring       = Edward Burns David Krumholtz Chris Parnell Rachel Dratch Connie Britton Kevin Kash
| music          = Robert Gary PT Walkley
| cinematography = William Rexer
| editing        = Sarah Flack
| distributor    = THiNK Film
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = $250,000 
| box office     = $4,480 
}}
Looking for Kitty is a 2004 American film written and directed by Edward Burns, in which he plays a private detective in New York City, hired by David Krumholtz to help track down his run-away wife.  It premiered at the Tribeca Film Festival on May 6, 2004.  It had a limited theatrical release in September 2006 and was released on DVD the following month, on October 24, 2006.

Saturday Night Live alumni Rachel Dratch and Chris Parnell have small parts, while Burns regulars Connie Britton and Kevin Kash have small roles as Burns neighbor and building superintendent, respectively.

==Critical reaction==
Critics were generally unimpressed by Looking for Kitty.  It has a 39% rating on Rotten Tomatoes, qualifying it as "rotten", and a 43 on Metacritic, indicating "mixed or average reviews".  Elizabeth Weitzman of The New York Daily News wrote that it "offers moments of striking insight amid the inevitable self-indulgence."  Noel Murray of The Onion A.V. Club was harsher, writing, "Burns has continued to cram one-dimensional characters into thinly plotted comedy-dramas, hoping to re-impress moviegoers with his aloof leading-man charm and faux-natural, trying-too-hard-to-be-funny dialogue."  Michael Atkinson of The Village Voice wrote, "It might be the most maturely conceived role in Burnss films, but the plot around it is flimsy, the visual storytelling simpleminded, and the general ideas for character one-note."

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 

 