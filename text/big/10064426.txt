Haunting Me
{{Infobox film
| name           = Haunting Me
| image          = Haunting-me-thai-film.jpg
| caption        = 
| director       = Poj Arnon
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Five Star Production
| released       =  
| runtime        = 
| country        = Thailand
| language       = Thai
| budget         = 
}}
Haunting Me ( ;  ) is a 2007 Thai horror-comedy film directed by Poj Arnon.

==Synopsis==
Jay-Taew, Cartoon, Mot-dum and Songkram are four aging katoey (drag queens) who run a boarding house for boys in provincial Thailand. After helping to cover up the mysterious deaths of "Pancake" and Num-Ning, two local teens, their spirits begin to haunt the dormitory, forcing the "girls" to try all sorts of crazy schemes to get rid of the ghosts. Eventually, they realize that the only way to do this is to help the ghosts to avenge their deaths.

==Cast==
*Jaturong Mokjok as Jay-Taew
*Sukonthawa Koetnimit as Khaao Tuu
*Ekkachai Srivichai as Mot-Dum
*Yingsak Chonglertjetsadawong as Cartoon
*Koti Arambawy as Pancake
*Panward Hemmanee as Num Ning
*Wiradit Srimalai as Namo
*Ratchanont Sukpragawp as Gaai
*Thanakorn Jaipinta as Giao
*Sophia La as Mom
*Anna Mokjok as Policewoman
*Somlek Sakdikul as Buckteeth
*Siwawat Sappinyo as Sim
*Chaiwat Thongsaeng as Main

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 
 