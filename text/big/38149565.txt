Singham Returns
 
 
{{Infobox film
| name           = Singham Returns
| image          = Singham Returns Poster.jpg
| caption        = Theatrical release poster
| director       = Rohit Shetty
| producer       =  
| writer         = Sajid-Farhad  (Dialogue) 
| screenplay     = Yunus Sajawal
| story          = Rohit Shetty
| starring       =  
| music          =  
| cinematography = Dudley
| editing        = Steven Bernard
| studio         =  
| distributor    = Reliance Entertainment
| released       =  
| runtime        = 145 minutes 
| country        = India
| language       = Hindi Marathi 
| budget         =    
| gross          =    3 weeks   
}}
 Indian action thriller film directed by Rohit Shetty and produced by Reliance Entertainment.    A sequel to the 2011 film Singham, actor Ajay Devgan reprises his role from the previous film, as well as co-producing the project, while Kareena Kapoor Khan plays the female lead.  The film was released worldwide on 15 August 2014. 

==Plot ==
Bajirao Singham (Ajay Devgan) gets transferred to Mumbai and he discovers that one of his team members (Mahesh) is found dead with large bags of money. Bajirao decides to unearth the truth behind it and in this process, he collides with a powerful Baba Ji, Satyaraj Chandar (Amole Gupte) who has high profile connections with politicians.

During this period, Guruji (Anupam Kher), is killed by Baba ji, even though Singham was present at the scene. He vows revenge. Because of this, he has to pretend that he has resigned in front of his family. Due to this his father asks him and his childhood friend, Avni (Kareena Kapoor Khan), to come to their home town, Shivgargh. After some comic scenes, Singham and Avni, fall in love with each other. However, we are then revealed to the fact that, Singham hasnt resigned and came to Shivgargh on a mission to get some proof against Baba Ji.

When they get back to Mumbai, Baba Ji starts threatening Singhams family.  He also harms all of Gurujis candidates, including Avni, because her brother is one of the candidates. Singham manages to save everyone but before they resign, a worker of Baba Ji wakes up from coma and testifies against him. Singham arrests Baba Ji and his worker, Prakash Rao.  However, some party workers attack outside the court, killing the witness.  This causes Baba Ji and Prakash Rao to get a bail.But after Baba Ji gets released Singham goes to his house without police uniform and shoots at his bums and Prakash Rao and Baba tell all the truth.

A few weeks later Gurujis party wins the elections.While going to jail Prakash Rao and Baba threaten Singham.But the van in which Prakash Rao and Baba were going stopped for some time. And a water tanker crashes the van and both of them die.Inspector Phadnis (Vineet Sharma)tells media that there was a breakfail so it is an accident.

The films ends with Singham,Daya and Phadnis walking away.

==Cast==
* Ajay Devgan as D.C.P. Bajirao Singham
* Kareena Kapoor Khan as Avni Kamat
* Amole Gupte as Satyaraj Chandra Baba 
* Anupam Kher as Gurukant "Guruji" Acharya
* Dayanand Shetty as Inspector Daya
* Vineet Sharma as Inspector Phadnis Zakir Hussain as Prakash Rao
* Mahesh Manjrekar as Chief Minister Vikram Adhikari
* Sonalee Kulkarni as Maneka
* Jitendra Joshi as Ashish (Manekas boyfriend)
* Sameer Dharmadhikari as Kishore Kamat
* Deepraj Rana as Sunil Prabhat
* Sharat Saxena as Police Commissioner Rathore
* Ashwini Kalsekar as Meera Shorri
* Govind Namdev as Manikrao Singham
* Pankaj Tripathi as Altaf
* Sachin Nayak as Bhola

==Production==

===Filming===
Director Rohit Shetty announced his thoughts on making a sequel to one of his finest movies Singham (2011). After filming Chennai Express with Shahrukh Khan, Shetty reportedly has begun filming for Singham 2 with the lead star Ajay Devgn.  Filming was expected to go on floors by December 2013 after Shettys last directorial venture, Chennai Express (2013) released with Shetty starting the work of Singham 2s pre-production along with that of his another film which incidentally is a sequel too, Golmaal 4.   However Ajay Devgn the lead of the film canned 20 days of photography for his another film Action Jackson, directed by Prabhudeva and start another leg of the shoot from 10 November 2013.  After several delays owing to Devgns shooting his other film Action Jackson concurrently the principal photography for a 15-day schedule of the film started from 15 May 2014 in Goa.  The shoot was held in few places like streets of Siolim, a village at located at a distance of about 25 Kilometres from Panaji.  The ones which were held in Panaji were mostly indoor ones.   

==Music==
{{Infobox album
| Name = Singham Returns
| Type = Soundtrack
| Cover = Singham Return artwork.jpg
| Artist =  
| Released =   Feature Film Soundtrack
| Length =  
| Label = T-Series
| Chronology = Jeet Gannguli
 | Last album = CityLights (2014 film)|Citylights (2014)
 | This album = Singham Returns (2014)
 | Next album = Alone (2015 film)|Alone (2014)
 {{Extra chronology
 | Artist = Meet Bros Anjjan
 | Type = Soundtrack
 | Last album = Kick (2014 film)|Kick (2014)
 | This album = Singham Returns (2014)
 | Next album = Sharafat Gayi Tel Lene (2014)
 }}
{{Extra chronology
 | Artist = Ankit Tiwari
 | Type = Soundtrack
 | Last album = Ek Villain (2014)
 | This album = Singham Returns (2014)
 | Next album = PK (film)|PK (2014)
 }}
{{Extra chronology
 | Artist = Yo Yo Honey Singh
 | Type = Soundtrack Kick (2014)
 | This album = Singham Returns (2014)
 | Next album = The Shaukeens (2014)
 }}
 {{Singles
 | Name = Singham Returns
 | Type = Soundtrack
 | Single 1 = Kuch Toh Hua Hai
 | Single 1 date =   
 | Single 2 = Aata Majhi Satakli
 | Single 2 date =   
 }}
}}

The soundtrack for the film was composed by Jeet Gannguli, Meet Bros Anjjan, Ankit Tiwari and Yo Yo Honey Singh. All the composers composed one song each. The soundtrack album contains four songs and one remix.

The first single track "Kuch Toh Hua Hai" sung by Ankit Tiwari & Tulsi Kumar was released on 25 July 2014.

Another single "Aata Majhi Satakli" sung and composed by Yo Yo Honey Singh was released on 1 August 2014.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| total_length =  
| title1 = Sun Le Zara
| extra1 = Arijit Singh
| music1 = Jeet Gannguli
| lyrics1 = Sandeep Nath
| length1 = 04:51
| title2 = Singham Returns Theme
| extra2 = Meet Bros Anjjan, Mika Singh
| music2 = Meet Bros Anjjan
| lyrics2 = Shabbir Ahmed
| length2 = 04:38
| title3 = Kuch Toh Hua Hai
| extra3 = Ankit Tiwari, Tulsi Kumar
| music3 = Ankit Tiwari
| lyrics3 = Sandeep Nath, Abhendra Kumar Upadhyay
| length3 = 05:07
| title4 = Singham Returns Remix (MBA Swag)
| extra4 = Meet Bros Anjjan, Mika Singh
| music4 = Meet Bros Anjjan
| lyrics4 = Shabbir Ahmed
| length4 = 02:49
| title5 = Aata Majhi Satakli
| extra5 = Yo Yo Honey Singh, Mamta Sharma, Nitu Choudhry
| music5 = Yo Yo Honey Singh
| lyrics5 = Yo Yo Honey Singh
| length5 = 03:14
}}

==Reception==
 Independence Day HJS wrote CBFC asking that the film be banned on grounds of depiction of Hindu saints in a bad light thereby hurting the religious sentiments.  Shetty denied the allegation stating that he has never made anything controversial in his career. 

Singham Returns received mixed reviews from Indian critics.    Taran Adarsh of Bollywood Hungama gave it 4 stars and said "The film is a complete mass entertainer with power-packed drama, hi-intensity dialogue and towering performances as its aces. The brand value attached to it coupled with a long weekend will help the film reap a harvest and rule the box office in days to come."  Rohit Khilnani of India Today gave 3 stars and said "Singham Returns is strictly for fans".  Shweta Kaushal of Hindustan Times gave two stars and said "Watch Singham Returns for the action, not comedy".  Mohar Basu of Koimoi gave 2.5 stars and said "Ajay Devgan and the thrills his punches manage to evoke but the wafer thin plotline, hammy acting, predictable narrative and the lack of fun and excitement that drives a Rohit Shetty film".  Shubra Gupta of the Indian Express gave 1.5 stars and said "The sequel to ‘Singham’ is chock full of the usual car on jeep action. Explosions go off at regular intervals. Shootouts – one really well shot – occur frequently."  Rajeev Masand of CNN-IBN gave 2.5 stars and said "The predictable story tires you out eventually". 

Three weeks after release, Bollywoodhungama.com estimated a   box office collection.   

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 
 