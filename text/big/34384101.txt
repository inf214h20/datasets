The Rookie Bear
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Rookie Bear
| series = Barney Bear
| image =
| caption =
| director = Rudolf Ising
| story_artist =
| animator =
| voice_actor =
| musician =
| producer = Rudolf Ising MGM Cartoons
| distributor = Metro-Goldwyn-Mayer (Original) Turner Entertainment (Later via Warner Bros.)
| release_date = 1941 (USA) Technicolor (two-color)
| runtime = 7:59
| movie_language = English
}}

The Rookie Bear is an MGM cartoon featuring Barney Bear.

==Plot==
Barney Bear is selected by a drawn Draft Number to enlist in the army. His hibernation is interrupted when a telegram is delivered to him and, misinterpreting the words on the telegram, assumes its an actual vacation. Barney enters the base with vacation supplies, but discovers his true purpose when bumping into the heavy artillery and is refused departure. He enlists through answering "simple" questions, having his photo taken, his physicality examined, his flat feet inflated, his teeth fixed and in the end, when hes passed all his exams, has his butt stamped. He finally gets his uniform, gun and gas mask "which is thoroughly tested". After putting on a pair of heavy shoes, Barney goes marching for 10,000 miles, but is tired (as well as his shoes, literally) after just 10 miles. His shoes get hotter and hotter until they sprout out popcorn. The whole thing turns out to be a dream when a spit from Barneys fireplace wakes him up by burning his rear. Before Barney can go back to sleep, he receives a telegram telling him to enlist in the army, along with a P.S., saying "And this time, buddy, It Aint No Dream!", much to Barneys dismay.

==Reception== Best Animated Short Film, but lost against The Walt Disney Company|Disneys Lend a Paw.

==See also==
* The Bear That Couldnt Sleep
* Bah Wilderness
* Goggle Fishing Bear
* Wee-Willie Wildcat
* Bird-Brain Bird Dog

== References ==
 

 
 
 
 


 