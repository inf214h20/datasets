Crime and Punishment (1998 film)
 
{{Infobox film
| name           = Crime and Punishment
| image          = Crime and Punishment 1998.jpg
| image_size     = 
| caption        = 
| director       = Joseph Sargent
| producer       = Howard Ellis Robert Halmi Sr. Joseph Sargent David Stevens
| based on       = Crime and Punishment by Fyodor Dostoyevsky
| narrator       = 
| starring       = Patrick Dempsey
| music          = Stanislas Syrewicz
| cinematography = Elemér Ragályi
| editing        = Ian Farr NBC Studios
| distributor    = National Broadcasting Company Trimark Pictures
| released       = October 11, 1998
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Crime and Punishment is a 1998 television film directed by Joseph Sargent. It stars Patrick Dempsey and Ben Kingsley.  

==Cast==
*Patrick Dempsey as Rodya Raskolnikov 
*Ben Kingsley as Porfiry
*Julie Delpy as Sonia Marmeladova
*Eddie Marsan as Dimitri
*Lili Horváth as Dounia

==Production==
Filming took place in Budapest, Hungary. 

==References==
 

==External links==
* 

 
 

 
 
 