Lost Boundaries
 
{{Infobox film
| name           = Lost Boundaries
| image          = Advertisement for Lost Boundaries.jpg
| image_size     =
| caption        =
| director       = Alfred L. Werker
| producer       = Louis De Rochemont William L. White (story) Charles Palmer (adaptation) Eugene Ling Virginia Shaler Furland de Kay (add. dialogue)
| narrator       =
| starring       = Beatrice Pearson Mel Ferrer Susan Douglas Rubes
| music          =
| cinematography = William Miller
| editing        = Dave Kummins
| distributor    = Film Classics
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
}}
 passed for white while living in New England in the 1930s and 1940s. The film won the 1949 Cannes Film Festival award for Best Screenplay.   

==Source==
William Lindsay White, a former war correspondent who had become editor and publisher of the Emporia Gazette, published Lost Boundaries in 1948.  It was just 91 pages long, and a shorter version had appeared the previous December in Readers Digest|Readers Digest.   The story was also reported with photographs in Life, Look, and Ebony.  White recounted the true story of the family of Dr. Albert C. Johnston (1900-1988)  and his wife Thyra (1904-1995),  who lived in New England for 20 years, passing as white despite their Negro backgrounds until they revealed themselves to their children and community.

Lost Boundaries focuses on the experience of their eldest son, Albert Johnson, Jr., beginning with the day Albert Sr. tells his sixteen-year-old son that he is the son of Negroes who have been passing as white. The story then recounts the lives of the parents. Dr. Johnston graduates from the University of Chicago and Rush Medical College, but finds himself barred from internships when he identifies himself as Negro. He finally secures a position at Maine General Hospital in Portland, Maine|Portland, which had not inquired about his race. In 1929, he establishes a medical practice in Gorham, New Hampshire. He and his blue-eyed, pale-skinned wife Thyra are active in the community and no one suspects their racial background, at least not enough to comment on it or question them. In 1939 they move to Keene, New Hampshire, where he takes a position at Elliot Community Hospital. At the start of World War II, he applies for a Navy post as a radiologist but is rejected when an investigation reveals his racial background. Struck by this rejection, he then shares his and his wifes family history with his eldest son Albert, who responds by isolating himself from friends and failing at school. Albert joins the Navy, still passing as white, but is discharged as "psychoneurotic unclassified". Albert then tours the U.S. with a white schoolfriend, visiting relatives and exploring lives on either side of the color line.  Much of the book is devoted to Albert Jr.s personal exploration of the world of passing, where he learns how the black community tolerates its members who pass but disapproves of casual crossing back and forth between the black and white communities. The other Johnson children have their own problems adjusting their new identity and the acceptance and rejection they experience. Finally, Albert Jr., attending the University of New Hampshire tells his seminar on international and domestic problems "that perhaps he could contribute something to this discussion of the race problem by telling of the problem of crossbred peoples. because he was himself a Negro."

The film adaptation does not follow the younger Johnson on his exploration of the broader racial landscape, but instead the son in the film visits Harlem, where he witnesses lives lived in the street rather than in the private homes of his New Hampshire environment and becomes involved in violence. The film ends on a note of interracial reconciliation as the white population excuses the Johnstons deception without examining the economic social pressures that led them to pass as white. The film, in one critics analysis, presents a subject of racial violence and social injustice within the bounds of a family melodrama.   

==Casting and production==
Producer Louis de Rochemont was originally to have worked on the film under contract with Metro-Goldwyn-Mayer. Because they could not agree on how to treat the story, with de Rochemont insisting on working "in the style of a restaged newsreel",    he mortgaged his home and developed other sources of financing so he could worked independently.    Its status as an independent production led some theaters and audiences to view it as an art film, and art houses proved significant in the films distribution.  De Rochement recruited local townspeople and found a local pastor to play the role of the minister. 

Filming took place on location in Portsmouth, New Hampshire, Kittery, Maine,  Kennebunkport, Maine, and Harlem, New York.  Mel Ferrer accepted his role reluctantly, at a point in his career when he hoped to direct either theater or film.  The leading roles were all cast with white actors, so that the film does not present any instances of racial passing, nor of interracial intimacy,  but the white actors chosen were unknown to the public to support the uncertain nature of their backgrounds in the film.

==Plot== Ray Saunders), Pullman porter porter until there is an opening in a black hospital.

When Scott appears for work in Georgia (U.S. state)|Georgia, the black hospital director tells him that the board of directors has decided to give preference to Southern applicants and rescinds the job offer. Marcia insists her husband continue searching for another medical job. In the meantime, they live in Boston with Marcias parents, who have successfully been passing as white. Her father and some of their black friends suggest they do the same. Instead, Scott continues to apply as a Negro and is repeatedly rejected. Scott finally yields, quits his job making shoes, and masquerades as white for a one-year internship in Portsmouth, New Hampshire.
 penetrating ulcer. His patient turns out to be Dr. Walter Bracket (Morton Stevens), the well-known director of a local clinic. Impressed, Dr. Bracket offers Scott a position as town doctor in Keenham, a fictionalized version of Keene, New Hampshire), where he will replace Brackets recently deceased father. Scott declines the offer, explaining that he is a Negro. Dr. Bracket, though he admits he would not have made the offer had he known, recommends Scott take the job without revealing his race. With his wife pregnant, Scott reluctantly agrees. Scott and Marcia worry how their child will appear at birth and are relieved that he appears as white as they do. When the local rector and his wife welcome Scott and Marcia to Keenham, Marcia tells them her boy will be named after Scotts mentor, Dr. Charles Howard. The rector tells Marcia he once met a doctor by that name, but that was a black man so he suggests she means a different Dr. Howard. Scott slowly earns the trust and respect of the residents. 

By 1942, when the United States enters World War II, the Carters are pillars of the community. Their son, Howard, attends the University of New Hampshire, while daughter Shelly (Susan Douglas Rubes) is in high school. The Carters have managed to keep their secret from the community as well as from their own children. Scott goes to Boston once a week to work at the Charles Howard Clinic, which he and Jesse Pridham established for patients of all races. Howard invites a black classmate, Arthur Cooper (William Greaves), to visit his family in Keenham. Shelly worries aloud what her friends will think about a List of ethnic slurs#C|"coon" staying in their home. Scott sternly orders her never to use that word again. When Arthur goes to a party with Howard, some guests make bigoted remarks behind his back.

Scott and Howard enlist in the United States Navy|Navy, but after Scotts background is investigated his commission as a lieutenant commander is suddenly revoked for "failure to meet physical conditions". The only position in the Navy for blacks is as a steward. The Carters have no choice but to reveal their secret to their children. Howard breaks up with his white girlfriend and disappears. He roams the streets of Harlem and rents a room there. When Shellys boyfriend Andy asks her what she is going to do about the "awful rumor" circulating about her family, Shelly confesses that it is true. Her boyfriend asks her to the school dance anyway, but she rejects the invitation. In Harlem, Howard investigates screams coming from a tenement building and finds two black men fighting. When one is knocked down and the other pulls out a gun, Howard intervenes. The gun goes off and the gunman flees, but Howard is taken into custody. To a sympathetic black police lieutenant (Canada Lee), Howard explains, "I came here to find out what its like to be a Negro." Arthur Cooper collects his friend from the police station.

Howard goes to his fathers clinic. Scott tells him: "I brought you up as white. Theres no reason why you shouldnt continue to live that way." When they attend a church service, the minister preaches a sermon of tolerance, then notes that the Navy has just ended its racist policy. The congregation sings a hymn using the words of New England poet James Russell Lowell: "Once to evry man and nation/Comes the moment to decide,/In the strife of truth with falsehood/For the good or evil side." The narrator announces that Scott Carter remains the doctor for a small New Hampshire town.

==Cast==
 
* Beatrice Pearson - Marcia Carter
* Mel Ferrer - Scott Mason Carter
* Susan Douglas Rubes - Shelly Carter (as Susan Douglas)
* Richard Hylton - Howard "Howie" Carter
* Grace Coppin - Mrs. Mitchell, Marcias mother
* Wendell Holmes - Mr. Morris Mitchell, Marcias father
* Carleton Carpenter - Andy
* Seth Arnold - Clint Adams
* Parker Fennelly - Alvin Tupper
* Ralph Riggs - Loren Tucker
* William Greaves - Arthur "Art" Cooper
* Ray Saunders - Dr. Jesse Pridham (as Rai Saunders)
* Leigh Whipper - Janitor 
* Morton Stevens - Dr. Walter Brackett
* Maurice Ellis - Dr. Cashman
* Alexander Campbell - Mr. Bigelow
* Edwin Cooper - Baggage man
* Royal Beal - Detective Staples
* Canada Lee - Lt. "Dixie" Thompson
* Robert A. Dunn - Rev. John Taylor (as Rev. Robert A. Dunn)
 

==Reception==
The film premiered in Keene on July 24, 1949.   

The Screen Directors Guild awarded Werker its prize for direction for the third quarter of 1949. 

Atlanta banned the film under a statute that allowed its censor to prohibit any film that might "adversely affect the peace, morals, and good order of the city."   Memphis did so as well, with the head of the Board of Censors saying: "We dont take that kind of picture here." 

  said it had "extraordinary courage, understanding and dramatic power." 

Ferrer later said: "This was a very, very radical departure from any kind of fiction film anybody was making in the country. It was a picture which broke a tremendous number of shibboleths, and it established a new freedom in making films." 

In 1986, Walter Goodman located Lost Boundaries within the Hollywood film industrys treatment of minorities: 
 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 