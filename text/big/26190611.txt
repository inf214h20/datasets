Road Kill (2010 film)
{{Infobox film
| name = Road Train
| image = Road Train.jpg
| caption = Theatrical release poster
| director = Dean Francis
| producer = Michael Robertson
| writer = Clive Hopkins
| starring =  
| music = Rafael May
| cinematography = Carl Robertson
| editing = Rodrigo Balart
| released =  
| studio =  
| distributor = Polyphony Entertainment
| runtime        = 90 minutes
| country       = Australia
| language = English
}} 
Road Train is an Australian horror film, known as Road Kill in the U.S., directed by Dean Francis  and written by Clive Hopkins.  It stars Xavier Samuel, Bobby Morley, Georgina Haig and Sophie Lowe. 

== Plot ==
 
Driving through the Australian outback, best friends Marcus (Xavier Samuel) and Craig (Bobby Morley) and their friends, Liz (Georgina Haig) and Nina (Sophie Lowe), are confronted by a road train.  The monstrous vehicle comes up behind their SUV and pushes them off the road. Craigs arm is broken. The friends decide seek help from the driver of the truck, which has stopped some distance up the road. The driver is nowhere to be found. Distant gunshots are heard, and a distant, crazed figure in the bush screams and runs towards them. Panicked, the friends commandeer the truck.  Driving through the outback, the trucks radio comes on by itself and lulls the four to sleep, leaving the truck to drive itself off the road and up a hill. Liz goes off in search of a shack she claims to have seen, while Marcus attempts to turn the truck around.  When the truck refuses to start, he accompanies Liz, while Nina looks after Craig.

Nina discovers the trucks fuel tanks are empty, but finds a large pipe hanging underneath the trailer, filled with a mysterious red goo. Craig, tormented by visions of three-headed hellhound Cerberus snarling and barking, finds a key to the trailers in the back of the cab and investigates the rearmost trailer.  The trailers door closes itself behind him.  Confused,  Nina searches for Craig in the wilderness.  Marcus and Liz have a disagreement over her sleeping with Craig; Liz wanders off into the desert alone in search for the shack, while Marcus stays on the road. Marcus has a run-in with the trucks driver who shoots himself, and Liz finds the shack, but it is rundown and abandoned. Inside, she finds unlabeled cans also containing a mysterious red goo. Desperate for water, she drinks some, but quickly runs back to the truck after finding bloody remains. Liz and Nina try to start the truck again, but they see Marcus trying to destroy the truck. He is now wearing the clothes of the truck driver and carrying his gun. The women overpower him, tying him onto the over-sized spare tire cage below one of the trailers, and resume trying to start the truck again.

Craig emerges from the trailers, looking much stronger than when he went in, and kills Marcus because he knows that they have to get away from the truck. Liz tries to guide from the rear of the truck as Nina backs the truck up, but eventually hides to continue drinking more of the mysterious red goo. Then Nina, unable to see Liz, exits the truck cab and is confronted by Craig. Craig attempts to seduce Nina into the rear trailer, but Nina is suspicious. Once she hears Liz cry for help from inside the trailer, she pushes Craig in, latches the door, and padlocks it, securing him inside.  Alone, Nina succeeds in turning the truck around and returns to the main road. There, she stops the truck and considers its trailers. She enters the forward trailer and is horrified to discover that it is an abattoir, grinding human bodies, including Marcuss, into the red goo that fuels the truck. Shocked, she returns to the cab and drives off.

Moments later, she spots a car and tries to signal them for help. But the truck radio turns on again by itself and during the distraction, Craig and Liz break into the cab. As the three fight, Craig, taking control, rams the car. Liz is thrown from the cab by Nina. Nina is knocked out. When Nina awakens, she finds Craig dragging Lizs body to the trailer while asking her to help. Craig again tries to persuade her to enter the trailer, claiming they have a magnificent opportunity. Nina plays along and tries to flee before Craig hits her head on the side of the cab. He then drags her into the trailer but she manages to overpower him and flees into the bush, with Craig in pursuit. He catches her, but she manages to grab the truck drivers gun and kill him. As she emerges from the bush, she spots the couple from the wrecked car searching the truck. She runs toward them, screaming warnings.  But the couple, having been run off the road, hearing distant gunshots, and seeing a crazed, screaming figure running towards them, commandeer the truck and drive off. Nina watches in horror as the truck speeds away to begin a new cycle.

==Cast==
* Xavier Samuel as Marcus
* Bob Morley as Craig
* Sophie Lowe as Nina
* Georgina Haig as Liz

==Production==
Road Train was shot in Adelaide, South Australia and Flinders Ranges.  Michael Robertson produced the film for ProdigyMovies, Screen Australia and The South Australian Film Corporation. 

== Release == Dungog Film Festival.  Road Train will be re-titled in the USA as Road Kill.  The US premiere had the film as part of the Fangoria Fright Fest on 22 June 2010.  It is set to be released by Lightning Entertainment in 6 August 2010 in the United States via DVD,  Video on Demand and Digital download.  The film was released in the United Kingdom on 30 August 2010. 

=== Promotion ===
The official trailer was released on 17 November 2009. 

== Reception ==
In a negative review, Dread Central wrote, "Despite a promising beginning with potentially interesting characters and a creepy, intimidating concept (a sinister road train), Road Kill squanders its potential by hurling itself off the rails, descending into rank absurdity."  The Australian was more positive, comparing it to an early Steven Spielberg film, Duel (1971 film)|Duel, and noting the promise of the filmmakers.  

== Soundtrack ==
The score was composed by Australian filmmusic artist Rafael May. 

== See also ==
* Cinema of Australia
* Upír z Feratu and Blood Car, two films about cars that use blood for fuel

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 