Star Wars Episode II: Attack of the Clones
 
 
{{Infobox film
| name           = Star Wars Episode II: Attack of the Clones
| image          = Star Wars - Episode II Attack of the Clones (movie poster).jpg
| alt            = Film poster. A young man is seen embracing a young woman. A man holds a lightsaber. In the foreground, there is a man wearing a suit.
| caption        = Theatrical release poster
| director       = George Lucas
| producer       = Rick McCallum
| writer         =
| screenplay     =  {{plainlist|
* George Lucas
* Jonathan Hales
}}
| story          = George Lucas
| starring       = {{plainlist|
* Ewan McGregor
* Hayden Christensen
* Natalie Portman
* Ian McDiarmid
* Samuel L. Jackson
* Christopher Lee
* Frank Oz
}}
| music          = John Williams
| cinematography = David Tattersall
| editing        = Ben Burtt
| studio         = Lucasfilm
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 142 minutes 
| country        = United States
| language       = English
| budget         = $115 million 
| gross          = $649.4 million 
}}
 the galaxy civil war. secede from Galactic Republic. Anakin Skywalker Clone Wars.
 high definition digital 24-frame system. 

The film was released on May 16, 2002. It garnered mixed reviews from critics  but was a financial success; however, it also became the first Star Wars film to be internationally out-grossed in its year of release. The film was released on DVD and VHS on November 12, 2002 and was later released on Blu-ray on September 16, 2011. A sequel, titled  , was released in 2005.

==Plot==
 
 Galactic Republic Jedi Master Anakin Skywalker. The two Jedi thwart another attempt on her life and subdue the assassin, Zam Wesell, a shape-shifter who is killed by her bounty hunter client with a toxic dart before she can reveal his identity. The Jedi Council assigns Obi-Wan to identify and capture the bounty hunter, while Anakin is assigned to escort Padmé back to Naboo, where the two fall in love.
 clones is genetic template. Boba to Shmi in pain, and travels to Tatooine with Padmé to save her. They meet Owen Lars, Anakins stepbrother who is the son of Shmis new husband Cliegg Lars. Cliegg tells Anakin that Shmi was abducted by Tusken Raiders weeks earlier and is most likely dead. Determined to find her, Anakin ventures out and finds the Tusken campsite. He discovers too late that his mother has been tortured by the tribe. As she dies from her wounds, Shmi reunites with Anakin. Anakin kills the Tuskens in his rage and returns to the Lars homestead with Shmis body. After revealing his deed to Padmé, Anakin says that he wants to prevent death.
 Supreme Chancellor emergency powers superweapon to Clone Wars, Anakin is fitted with a robotic arm and secretly marries Padmé on Naboo, with C-3PO and R2-D2 as their witnesses.

==Cast==
  (left) as Obi-Wan Kenobi and Hayden Christensen (right) as Anakin Skywalker in Attack of the Clones.]]
*   investigating a mysterious assassination attempt, leading him to discover the making of a clone army. the Force".
*  , who has recently been elected the planets senator. emergency powers upon the outbreak of the Clone Wars, but is secretly the Sith Grand Master behind the starting conflict.
* Samuel L. Jackson as Mace Windu: A Jedi Master sitting on the Jedi Council who warily watches the Galactic Senates politics.
*   who is Darth Sidious new Sith apprentice and a suspect in Obi-Wans investigation.
*   as a use to the cloning facilities on Kamino for the creation of the clone army and, in addition to his wage, requested an unaltered clone for himself to take as his son.
* Daniel Logan as Boba Fett: Jangos son, who is created from his "father"s DNA.
* Frank Oz voices Yoda: The centuries-old Jedi Grand Master of an unknown species who, in addition to sitting on the Jedi Council, is the instructor for young Jedi.
* Anthony Daniels as C-3PO: A protocol droid built by Anakin for his mother, who now works on the Lars homestead. Kenny Baker as R2-D2: An astromech droid often seen on missions with Anakin and Obi-Wan.
* Silas Carson as Nute Gunray and Ki-Adi-Mundi: Gunray is the Viceroy of the Trade Federation, who attempts to assassinate Padmé as revenge for his loss against her people on Naboo. Ki-Adi-Mundi is a Jedi Master and a member of the Jedi Council.
* Leeanna Walsman as Zam Wesell: A shape-shifting Clawdite bounty hunter and partner of Jangos, who is given the task of assassinating Padmé.
*   whom Padmé appoints Representative of Naboo.
* Rose Byrne as Dormé: The handmaiden to Senator Padmé Amidala.
* Verónica Segura as Cordé: A handmaiden and decoy to Senator Padmé Amidala. (Killed in an attack)
* Pernilla August as Shmi Skywalker: Anakin Skywalkers mother. She dies in his arms after being kidnapped and tortured by Tusken Raiders. Jack Thompson as Cliegg Lars: Moisture farmer who purchased Shmi Skywalker, freed and married Shmi, becoming the stepfather of Anakin Skywalker.
* Oliver Ford Davies as Sio Bibble: Governor of Naboo.
* Ayesha Dharker as Jamillia: Queen of Naboo
* Joel Edgerton as Owen Lars: Son of Cliegg Lars and stepbrother of Anakin Skywalker.
* Bonnie Piesse as Beru Whitesun: Owens girlfriend.
* Jay Lagaaia as Captain Typho: Head of security for Senator Padmé Amidala.

E!|E! Online reported that Lucas had allowed N Sync to film a small background cameo appearance, in order to satisfy his daughters. They were subsequently cut out of the film in post-production.  The end credits erroneously list Alan Ruscoe as playing Neimoidian senator Lott Dod. The character was actually another Neimoidian named Gilramos Libkath, played by an uncredited David Healey and voiced by Christopher Truswell.

A large search for the new Anakin Skywalker was performed across the United States. Lucas auditioned various actors, mostly unknown, before casting Christensen. Among the many established actors who auditioned were Jonathan Brandis, Ryan Phillippe,  Colin Hanks,  and Paul Walker.  Leonardo DiCaprio also met with Lucas for the role, but was "definitely unavailable" according to DiCaprio publicist Ken Sunshine.  Co-star Natalie Portman later told Time (magazine)|Time magazine that Christensen "gave a great reading. He could simultaneously be scary and really young." 

==Production==

===Writing===
After the mixed critical response to The Phantom Menace, Lucas was hesitant to return to the writing desk. In March 2000, just three months before the start of principal photography, Lucas finally completed his rough draft for Episode II. Lucas continued to iterate on his rough draft, producing a proper first and second draft. For help with the third draft, which would later become the shooting script, Lucas brought on Jonathan Hales, who had written several episodes of The Young Indiana Jones Chronicles for him, but had limited experience writing theatrical films. The final script was completed just one week before the start of principal photography.

As an in-joke, the films working title was Jar Jars Big Adventure, a sarcastic reference to the negative fan response to the Episode I character. 
 A New Hope;   he later came up with an alternate concept of an army of clone shocktroopers from a remote planet which were used by the Republic as an army in the war that followed. 

===Filming===
 
 
  Plaza de España in Seville, Spain, in Italy at the Villa del Balbianello on Lake Como, and in the former royal Palace of Caserta. At his own personal request, Samuel L. Jacksons character Mace Windu received a lightsaber that emitted an amethyst glow, as opposed to traditional blue and green for "good guys" and red for "bad guys." {{cite episode
| title        = Samuel L. Jackson
| series       = Inside the Actors Studio
| serieslink   = Inside the Actors Studio Bravo
| airdate      = 2002-06-02
| season       = 8
| number       = 15
}}  Reshoots were performed in March 2001. During this time, a new action sequence was developed featuring the droid factory after Lucas had decided that the film lacked a quick enough pace in the corresponding time-frame. The sequences previsualization was rushed, and the live-action footage was shot within four and a half hours.  Because of Lucas method of creating shots through various departments and sources that are sometimes miles and years apart from each other, Attack of the Clones became the first film ever to be produced through what Rick McCallum called "virtual filmmaking." 

Like The Phantom Menace, Attack of the Clones furthered technological development, effectively moving   and Vidocq (2001 film)|Vidocq).  Despite Lucas efforts to persuade movie theaters to switch to digital projectors for viewing of Episode II, few theaters did. 

===Visual effects===
The film relied almost solely on digital animatics as opposed to storyboards in order to previsualize sequences for editing early on in the films production. While Lucas had used other ways of producing motion-based storyboards in the past, after The Phantom Menace the decision was made to take advantage of the growing digital technology.  The process began with Ben Burtts creation of what the department dubbed as "videomatics", so called because they were shot on a household videocamera. In these videomatics, production assistants and relatives of the department workers acted out scenes in front of chroma key|greenscreen. Using computer-generated imagery (CGI), the previsualization department later filled in the green screen with rough background footage. Burtt then cut together this footage and sent it off to Lucas for changes and approval. The result was a rough example of what the final product was intended to be. The previsualization department then created a finer version of the videomatic by creating an animatic, in which the videomatic actors, props, and sets were replaced by digital counterparts to give a more precise, but still rough, look at what would eventually be seen. The animatic was later brought on set and shown to the actors so that they could understand the concept of the scene they were filming in the midst of large amount of bluescreen used. Unlike most of the action sequences, the Battle of Geonosis was not storyboarded or created through videomatics but was sent straight to animatics after the department received a small vague page on the sequence. The intent was to create a number of small events that would be edited together for pacing inside the finished film. The animatics department was given a free hand regarding events to be created within the animatic; Lucas only asked for good action shots that he could choose from and approve later. State of the Art: The Previsualization of Episode II DVD Special Feature,   

  as seen in the film.|alt=Yoda holding a lightsaber.]]

In addition to introducing the digital camera, Attack of the Clones emphasized "digital doubles" as computer-generated models that doubled for actors, in the same way that traditional stunt doubles did. It also furthered the authenticity of computer-generated characters by introducing a new, completely CGI-created version of the character   relied on a stunt double to perform the most demanding scenes instead. Lees face was superimposed onto the doubles body in all shots other than closeups, which he performed himself. Lucas often called the duel crucial to the animation department, as it had such potential to be humorous rather than dramatic. 

===Music===
 
The soundtrack to the film was released on April 23, 2002 by Sony Classical Records.  The music was composed and conducted by John Williams, and performed by the London Voices and London Symphony Orchestra.  The soundtrack recreates "The Imperial March" from the film The Empire Strikes Back for its first chronological appearance in Attack of the Clones, even though a hint of it appeared in the previous movie in one of the final scenes. A music video for the main theme "Across the Stars" was produced specifically for the DVD. 

==Themes==
  emergency powers", Napoleon Bonaparte, First Consul civil rights. 

 
 War journalism, combat films, and footage of World War II combat influenced the documentary-style camera work of the Battle of Geonosis, even to the point that hand-held shakes were digitally added to computer-generated sequences.   

The   field is the backdrop of a major star battle in the middle of the film. Obi-Wan escapes Jango Fett by attaching his spacecraft to an asteroid in order to disappear from the enemy sensors;  ?" This is an allusion to A New Hope where Anakin, as Darth Vader, kills Obi-Wan aboard the Death Star. Also, Count Dooku cuts off Anakins arm, similar to when Darth Vader cut off Luke Skywalkers hand in The Empire Strikes Back.

==Release== Fox Network on March 10, 2002 between Malcolm in the Middle and The X-Files,  and was made available on the official Star Wars web site the same day. The outplacement firm Challenger, Gray & Christmas from Chicago predicted before the films release that U.S. companies could lose more than $319 million in productivity due to employees calling in sick and then heading to theaters to see the film. 
 BMCC Performing digital remastering process. Because of the technical limitations of the IMAX projector, an edited, 120-minute version of the film was presented. 
 Lucasfilm Ltd. by the fan site.

A pirate copy was allegedly made at a private showing, using a digital recorder that was pointed at the screen. This copy spread over the internet, and analysts predicted up to a million fans would have seen the film before the day of its release.  In addition, authorities seized thousands of bootlegs throughout Kuala Lumpur before the film opened.   

===Home media=== ILM animation director Rob Coleman, and ILM visual effects supervisors Pablo Helman, John Knoll, and Ben Snow. Eight deleted scenes are included along with multiple documentaries, which include a full-length documentary about the creation of digital characters and two others that focus on sound design and the animatics team. Three featurettes examine the storyline, action scenes, and love story, and a set of 12 short web documentaries cover the overall production of the film. 

The Attack of the Clones DVD also features a trailer for a  . The story, which Lucas approved, was meant to be humorous. 

The DVD was re-released in a prequel trilogy box set on November 4, 2008. 

The Star Wars films were released by 20th Century Fox Home Entertainment on Blu-ray Disc on September 16, 2011 in three different editions. 
 Walt Disney digital download on April 10, 2015.      

===3D re-release=== Celebration Europe II in July 2013. 

== Reception ==

=== Critical response ===
The review aggregator website Rotten Tomatoes reported a 67% approval rating for the film based on 218 reviews, the general consensus being "Containing more of what made the Star Wars series fun, Attack of the Clones is an improvement over The Phantom Menace."  On Metacritic, the film holds a score of 53 based on 39 reviews, which indicates "Mixed or average reviews". 

Critics called the dialogue "stiff" and "flat".  The acting (particularly by Christensen and Portman) was also disparaged by some critics.    Conversely, other critics felt fans would be pleased to see that   as it neared release.  ReelViews.nets James Berardinelli gave a positive review concluding that "In a time when, more often than not, sequels disappoint, its refreshing to uncover something this high-profile that fulfils the promise of its name and adds another title to a storied legacy." 

Roger Ebert, who had praised all of the other Star Wars films, gave Episode II only two out of four stars, noting, "  someone who admired the freshness and energy of the earlier films, I was amazed, at the end of Episode II, to realize that I had not heard one line of quotable, memorable dialogue." About Anakin and Padmes relationship, Ebert stated, "There is not a romantic word they exchange that has not long since been reduced to cliché."  Leonard Maltin, who also liked all of the previous installments, only awarded two stars out of four to this endeavor as well, as seen in his Movie and Video Guide from the 2002 edition onward. Maltin cited an "overlong story" as reason for his dissatisfaction and added, "Wooden characterizations and dialogue dont help." 
 Worst Picture, Worst Director Worst Screenplay Worst Supporting Worst Supporting Worst Screen Worst Remake or Sequel.  It took home two awards for Worst Screenplay (George Lucas) and Worst Supporting Actor (Hayden Christensen). 

=== Box office === Harry Potter 100 highest-grossing films of all time when adjusted for inflation.

== Novelizations ==
 
Two novels based on the movie were published, a tie-in junior novel by Scholastic Corporation|Scholastic,  and a novelization written by R. A. Salvatore, which includes some unique scenes.  A four-issue comic book adaptation was written by Henry Gilroy and published by Dark Horse Comics. 

==References==

===Footnotes===
  Walt Disney Studios.    The digital distribution rights belong to Walt Disney Studios Home Entertainment|Disney, as Lucasfilm retained the films digital distribution rights prior to its acquisition by Disney. 
 

===Citations===
 

===Sources===
*  
**  <!--
   check later what "v3.0 means, perhaps there is a different date for that
 -->
*  

==External links==
 
 
 
*   at  
*   at  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 