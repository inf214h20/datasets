Besahara

{{Infobox film
| name           = Besahara
| image          = Besahara1987film.jpg
| caption        = 
| director       = Ishara
| producer       = S. Waris Ali
| screenplay     = Mushtaq Jalili	
| starring       = Rajan Sippy Kader Khan Bharat Kapoor Shashikala Priya Tendulkar Vidyashree
| music          = Usha Khanna  Gauhar Kanpuri (Lyrics)
| cinematography =  Akbar Balam 	
| editing        =  Tara Singh
| released       =  1 Jan 1987
| runtime        = 135 min
| country        =  India
| language       =  Hindi
}}

Besahara is a 1987 Bollywood family drama film directed by Ishara. It stars Rajan Sippy, Kader Khan, Bharat Kapoor, Shashikala, Priya Tendulkar, Vidyashree.

==Cast==
* Bharat Kapoor
* Kader Khan
* Mazhar Khan
* Shashikala
* Rajan Sippy 
* Priya Tendulkar
* Vidyashree

==Soundtrack==
* Saajan Teri Baahon Mein
* Dushman Hai Sab Ki Jaanki
* Mujhko Raahon Pe Tum Chhodkar
* Suniye Huzoor Aadmi Ki

==References==
 

==External links==
 

 
 
 


 