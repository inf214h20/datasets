Denk bloß nicht, ich heule
{{Infobox film
| name           = Denk bloß nicht, ich heule
| image          = Denk nicht 65.jpg
| image size     =
| caption        =
| director       = Frank Vogel
| producer       =
| writer         = Manfred Freitag, Joachim Nestler
| narrator       =
| starring       =
| music          = Hans-Dieter Hosalla
| cinematography = Günter Ost
| editing        = Helga Krause	
| distributor    = 
| released       = 1965
| runtime        = 91 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} banned in East Germany because of its social criticism.       The film was said to have "problematized the oppression of critical young people in East German schools."   

==Cast==
* Peter Reusse as Peter Neumann
* Anne-Kathrein Kretzschmar as Anne
* Hans Hardt-Hardtloff as Annes Vater
* Jutta Hoffmann as Uschi
* Helga Göring as Frau Naumann
* Harry Hindemith as Herr Naumann
* Herbert Köfer as Herr Röhle
* Fred Delmare as Brigadier
* Arno Wyzniewski as Dieter
* Horst Buder as Ami
* Hans H. Marin as Langer
* Heinz-Dieter Obiora as Valente
* Armin Mechsner as Jonny
* Alexander Lang as Latte
* Hans-Peter Körner as Klaus
* Uwe Karpa as Bubi
* Werner Dissel as Mantek Gerhard Klein as Ober
* Carmen-Maja Antoni as Student with glasses

==References==
 

==External links==
*  

 
 
 
 


 
 