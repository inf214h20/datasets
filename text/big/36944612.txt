The Hypnotist (2012 film)
 
{{Infobox film
| name           = The Hypnotist
| image          = TheHypnotist2012.jpg
| caption        = Film poster
| director       = Lasse Hallström
| producer       = Börje Hansson Peter Possne Bertil Olsson
| screenplay     = Paolo Vacirca
| based on       =  
| starring       = Tobias Zilliacus
| music          = 
| cinematography = Mattias Montero
| editing        = Sebastian Amundsen Thomas Täng
| studio         = 
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
 novel of Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Tobias Zilliacus as Joona Linna
* Mikael Persbrandt as Erik Maria Bark
* Lena Olin as Simone Bark
* Helena af Sandeberg as Daniella
* Jonatan Bökman as Josef
* Oscar Pettersson as Benjamin
* Eva Melander as Magdalena
* Anna Azcarate as Lydia
* Johan Hallström as Erland
* Göran Thorell as Stensund
* Jan Waldekranz as Shulman
* Emma Mehonic as Evelyn
* Tomas Magnusson as Petter
* Nadja Josephson as Aida

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 

 
 