Double Tap (film)
 
 
{{Infobox film
| name           = Double Tap
| image          = DoubleTapfilmposter.jpg
| alt            = 
| caption        = Film poster
| film name = {{Film name| traditional    = 鎗王
 | simplified     = 枪王
 | pinyin         = Qiāng Wáng
 | jyutping       = Ceong1 Wong4}} Bruce Law
| producer       = Derek Yee
| screenplay     = Yeung Sin Ling Bruce Law Lee Hau Shek Yeung Chin Hung
| story          = Joseph Cheung Derek Yee Alex Fong Ruby Wong Monica Chan
| music          = Peter Kam Anthony Chue
| cinematography = Venus Keung Tony Cheung
| editing        = Kwong Chi Leung
| studio         = Film Unlimited Golden Harvest
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$3,987,152
}} 2000 Cinema Hong Kong Bruce Law, Alex Fong, Monica Chan and Ruby Wong. It was hailed as one of Leslie Cheungs most memorable action films since A Better Tomorrow. A follow up film, Triple Tap, directed by Derek Yee was released in 2010 with only Alex Fong reprising his role as Inspector Miu with a new storyline and new cast consisted of Louis Koo and Daniel Wu.

==Synopsis== IPSC champion who is also a gun expert who tinkers with his pistols and modified its magazine to perfect his technique known as double tap which is the terminology for a shooter placing two shots in the same exact spot, to maximize marksmanship in any competition.
 IPSC Hong IPSC supervisor, again one of Mius friend. Rick was forced to kill him with his trademark double tap as the former approaches Colleen and Vincent. Miu took note of this astounding technique in the post-mortem while Ricks experience from this encounter was strangely exciting to him and he discovers his lust for murdering others in cold blood.

As further murders were committed by an unknown gunman with the similar double tap technique, the list of possible candidates by the police was narrowed down to just a few candidates but Miu was utterly convinced that Rick was also behind all those heinous crimes and hauls him up for investigation. Although Rick was released after an interrogation (having had no solid evidence incriminating him), Miu continued to pursue him and pressurise Rick to an extent that the murderer was suddenly possessed by rage and decided to take the law in his own hands. Rick went to his favorite shooting range and lay in wait for his pursuers. Arming himself with live rounds for his modified pistol, he engaged in a fierce gunfight with the cops and escaped at the first possible opportunity.

It later transpired that Rick was indeed haunted by his earlier murder at the competition but his insatiated lust for murder was further fuelled by his obsession with the double tap technique as he sought to perfect it. Further, he has inadvertently discovered a new use for his pistols: to kill those who opposed him and those who pushed him to the brink.

==Cast and roles==
*Leslie Cheung - Rick Pang (shooting champion turned rogue by a mentally disturbing shooting accident) Alex Fong - Miu Chi Sun (Ricks chief competitor and nemesis)
*Ruby Wong - Colleen (Ricks girlfriend)
*Vincent Kok - Vincent (gun enthusiast)
*Joe Cheung - Joe (shooting competitor)
*Monica Chan - Ellen (Mius wife who is also a doctor)

==External links==
*  

 
 
 
 
 
 
 