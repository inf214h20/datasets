The Woman Who Desires Sin
{{Infobox film
| name           = The Woman Who Desires Sin
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Biegański
| producer       = Wiktor Biegański
| writer         = Wiktor Biegański
| narrator       =  Nora Ney
| music          = 
| editing        = 
| cinematography = Seweryn Steinwurzel   Antoni Wawrzyniak
| studio         = 
| distributor    = 
| released       = 22 November 1929
| runtime        = 
| country        = Poland
| language       = Silent   Polish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish silent silent crime film directed by Wiktor Biegański and starring Carlotta Bologna, Tadeusz Wenden and Stefan Łada. It was less popular with critics than some of Bieganskis earlier films. 

==Cast==
*  Carlotta Bologna as Irena Parecka 
* Tadeusz Wenden as Ing. Janusz Stoma 
* Stefan Łada as Witold Tyński  Nora Ney as Maryna 
* Oktawian Kaczanowski as Tadeusz Parecki, Irenas father 
* Jerzy Jabłoński as Agent policyjny 
* Alma Kar as Wanda, Irenas friend 
* Alojzy Kłyko as Kuba 
* Wacław Korwin as Sędzia śledczy 
* Włodzimierz Metelski as Ryszard Zychoń 

==References==
 

==Bibliography==
* Haltof, Marek. Polish National Cinema. Berghahn Books, 2002.

==External links==
* 

 
 
 
 
 
 
 
 
 

 