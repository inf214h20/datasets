Karinagam
{{Infobox film
| name           = Karinagam
| image          =
| caption        =
| director       = KS Gopalakrishnan
| producer       =
| writer         = Jagathy NK Achari
| screenplay     = Jagathy NK Achari Anuradha Balan K Nair Bheeman Raghu
| music          = A. T. Ummer
| cinematography =
| editing        = A Sukumaran
| studio         = Vinod Arts
| distributor    = Vinod Arts
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by KS Gopalakrishnan. The film stars Ratheesh, Anuradha (actress)|Anuradha, Balan K Nair and Bheeman Raghu in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Ratheesh Anuradha
*Balan K Nair
*Bheeman Raghu
*Ranipadmini
*TG Ravi

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kulirunnen meyyaake || KS Chithra || Poovachal Khader || 
|-
| 2 || Lajjaalolayayi || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Mohamaakum naagangal || KS Chithra, Krishnachandran || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 