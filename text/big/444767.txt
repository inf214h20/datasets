Seabiscuit (film)
 
{{Infobox film
| name           = Seabiscuit
| image          = Seabiscuit ver2.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Gary Ross Kathleen Kennedy Frank Marshall Gary Ross Jane Sindell
| screenplay     = Gary Ross
| based on       =  
| narrator       = David McCullough Gary Stevens William H. Macy
| music          = Randy Newman
| cinematography = John Schwartzman
| editing        = William Goldenberg
| studio         = Spyglass Entertainment The Kennedy/Marshall Company
| distributor    = Universal Pictures (North America) DreamWorks Pictures  (International) 
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| budget         = $87 million
| gross          = $148.3 million   
}}
Seabiscuit  is a 2003 American   by Laura Hillenbrand. The film is loosely based on the life and racing career of Seabiscuit, an undersized and overlooked thoroughbred race horse, whose unexpected successes made him a hugely popular media sensation in the United States during the Great Depression.

==Plot== Tom Smith (Chris Cooper) come together as the principal jockey, owner, and trainer of the championship horse Seabiscuit, rising from troubled times to achieve fame and success through their association with the horse.

Red is the child of a Canadian family financially ruined by the Great Depression. In desperate need of money, the family leaves Red with a horse trainer. Red eventually becomes a jockey, but makes extra money through illegal boxing matches which left him blind in one eye. Howard is a clerk in a bicycle shop who gets asked by a passing motorist to repair his automobile, a technology which has recently been introduced. As a result, Howard becomes knowledgeable enough with automobiles to increase their performance and sell them as a dealer, eventually becoming the largest car dealer in California and one of the Bay Areas richest men. However, his son is killed in an automobile accident while driving the family car, which sends Howard into a bout of deep Depression (mood)|depression, which eventually results in his wife (Valerie Mahaffey) leaving him.

On a trip to Mexico to obtain a divorce and to drown his sorrows, he meets Marcela Zabala (Elizabeth Banks).  Marcela helps Howard overcome his depression, mainly through horse-riding. After marrying Marcela, Howard acquires a stable of horses and later has a chance encounter with the skilled and kindly horse trainer and drifter Smith. Howard hires Smith to manage his stables after Smith, who specializes in rehabilitating injured and abused horses, explains to Howard "You dont throw a whole life away just cause its banged up a little bit". Smith convinces Howard to acquire the colt "Seabiscuit", who comes from noted lineage but had been deemed "incorrigible" by past handlers and was later broken and trained to lose against better horses.
 Gary Stevens) to be Seabiscuits new rider.
 Santa Anita when he is injured and has to stop. Red helps him to recover and get fit enough to race again. The last race is again at the Santa Anita, and Red rides him this time after putting a special self-made brace on his own leg to keep it stable. Woolf is on a different horse. Seabiscuit drops to last place and trails the pack, but Woolf holds back to be alongside Red and let Seabiscuit "get a good look". After a short conversation, Seabiscuit surges and wins the race.

Red says, "You know, everyone thinks that we found this broken down horse and fixed him, but we didnt. He fixed us. Every one of us. And I guess in a way, we kinda fixed each other, too."

==Cast== John Red Pollard
* Jeff Bridges as Charles S. Howard Tom Smith
* Elizabeth Banks as Marcela Zabala-Howard Gary Stevens as George Woolf
* William H. Macy as "Tick Tock" McLaughlin Eddie Jones as Samuel Riddle

==Production==
The film was shot at Santa Anita Park in Arcadia, California, Keeneland Race Course in Lexington, Kentucky and Saratoga Race Course in Saratoga Springs, New York. Keeneland was chosen to double for Pimlico Race Course because Pimlico had dramatically changed physically since Seabiscuits time. The film also marks a second collaboration between director Gary Ross and actors Tobey Maguire and William H. Macy, who worked together in Rosss 1998 film Pleasantville (film)|Pleasantville.

==Critical reception==
The film received positive reviews from critics. On the review website Rotten Tomatoes, 77% of critics gave the film positive reviews, based on 199 reviews, and an average rating of 7.1/10, with the consensus: "A life-affirming, if saccharine, epic treatment of a spirit-lifting figure in sports history". 

American Film Institute Recognition:
*AFIs 100 Years...100 Cheers- #50

==Accolades==
{| class="wikitable"
! Group !! Category !! Recipient !! Result
|- ASCAP Film and Television Music Awards
| Top Box Office Films
| Randy Newman
|  
|-
| rowspan="7" | 76th Academy Awards    Best Picture Kathleen Kennedy Frank Marshall Gary Ross
|  
|- Best Writing (Adapted Screenplay)
| Gary Ross
|  
|- Best Art Direction
| Jeannine Claudia Oppewall   Leslie A. Pope  
|  
|- Best Cinematography
| John Schwartzman
|  
|- Best Costume Design
| Judianna Makovsky
|  
|- Best Film Editing
| William Goldenberg
|  
|- Best Sound Andy Nelson Anna Behlmer Tod A. Maitland
|  
|- 54th ACE Eddie Awards Best Edited Feature Film – Dramatic
| William Goldenberg
|  
|-
| American Society of Cinematographers Awards 2003
| Outstanding Achievement in Cinematography in Theatrical Releases
| John Schwartzman
|  
|- Broadcast Film Critics Association Awards 2003 Best Film
|
|  
|- Best Screenplay
| Gary Ross
|  
|-
| Directors Guild of America Awards 2003 Outstanding Directing – Feature Film
| Gary Ross
|  
|-
| 61st Golden Globe Awards Best Motion Picture – Drama
|
|  
|- Satellite Awards 2003 Best Supporting Actor – Motion Picture
| Jeff Bridges
|  
|- Best Art Direction and Production Design
|
|  
|- Best Cinematography
| John Schwartzman
|  
|- Best Costume Design
| Judianna Makovsky
|  
|- Best Editing
| William Goldenberg
|  
|- Best Original Score
| Randy Newman
|  
|- Best Adapted Screenplay
| Gary Ross
|  
|- Best Sound
|
|  
|-
| rowspan="2" | 10th Screen Actors Guild Awards Outstanding Performance by a Male Actor in a Supporting Role
| Chris Cooper
|  
|- Outstanding Performance by a Cast in a Motion Picture Gary Stevens
|  
|-
| Writers Guild of America Awards 2003 Best Adapted Screenplay
| Gary Ross
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 