Sounds Like a Revolution
{{Infobox film
| name           = Sounds Like a Revolution
| image          = Sounds Like a Revolution.jpg
| image_size     = 204×291
| caption        = Promotional film poster
| writer         = Susan Martin
| director      = Summer Love & Jane Michener
| producer      = Summer Love & Jane Michener
| narrator       = Jackie Richardson
| starring       = Paris, Fat Mike, Michael Franti & Justin Sane
| released       = 2010
| country        = Canada English
}}

Sounds Like a Revolution is a feature documentary about recent protest music in America.
Directed by Canadian directors Summer Love and Jane Michener, the film was released in June 2010 and had its world premiere at NXNE festival and its theatrical premiere at The Royal Theatre in Toronto, Canada.
 Wayne Kramer (MC5) to Tom Morello (Rage Against the Machine) and more.

==Music==
The documentary features songs from Anti-Flag, NOFX, Paris (rapper)|Paris, Michael Franti & Spearhead, Blue King Brown and The Disposable Heroes of Hiphoprisy. Also additional songs are included from The Dixie Chicks, Ministry, Body Count, Johnny Dix and The Coup.

==References==
*  
*   
* http://doc.soundslikearevolution.com/press/

==External links==
*  
*   on the Internet Movie Database


 
 
 
 
 

 