Snakehead Terror
 
{{Infobox television film
| name           = Snakehead Terror
| image          =  
| image_size     =
| genre          = science fiction horror
| writer         = A.G. Lawrence 
| starring       = Bruce Boxleitner Carol Alt William B. Davis Chelan Simmons 
| director       = Paul Ziller
| producer       = Elizabeth Sanchez Paul Ziller 
| cinematographer = Mark Dobrescu Ken Williams
| studio         = Cinetel Films Sci Fi Pictures
| distributor    = Syfy
| runtime        = 92 minutes
| network        = Syfy
| released       = March 13, 2004
| country        = Canada USA
| language       = English 
| budget         = 
}}
 snakehead fish incident in a Crofton, Maryland pond. The other film is  Frankenfish. Swarm of the Snakehead is an independently produced creature comedy based on the same Crofton incident, and the only one of the three actually filmed in Maryland.

==Plot== snakehead fish mutate, surviving previous chemical attacks. The fish transform from pests to predators when human growth hormones are dumped into the local lake in the hopes of reviving the local fishing industry. Thriving on the hormones, the Snakehead fish grow to monstrous proportions, devouring everything within reach. Capable of moving and eating on land, they are forced to leave the now barren lake in a desperate search for food - animal, vegetable or human.  In a race against time, the local Sheriff and biologist Lori Dale, try to save the Sheriffs teenage daughter and her friends, along with the town itself,  from being eaten alive.

==Cast==
*Bruce Boxleitner as Sheriff Patrick James
*Carol Alt as Lori Dale
*Chelan Simmons as Amber James
*Juliana Wimbles as Jagger
*Ryan McDonell as Luke
*Chad Krowchuk as Craig
*Matthew MacCaull as James
*William B. Davis as Doc Jenkins
*Alistair Abell as Sammy
*Doug Abrahams as deputy Clark
*Brenda Campbell as reporter
*Gardiner Millar as hunter #1
*Bro Gilbert as deputy Reece
*P. Lynn Johnson as Norma Gary Jones as Colin Jenkins
*Don MacKay as Ray Wilkens

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 
 