Flic Story
{{Infobox film
| name           = Flic Story
| image          = Flicstory.jpg
| image_size     = 
| caption        = Original movie poster, featuring Delon and Trintignant
| director       = Jacques Deray
| producer       = Alain Delon
| writer         = Roger Borniche (autobiography) Alphonse Boudard 
| narrator       = 
| starring       = Alain Delon, Jean-Louis Trintignant
| music          = Claude Bolling
| cinematography = Jean-Jacques Tarbès   
| editing        = Henri Lanoë   
| distributor    = Adel Productions Lira Films Mondial Televisione Film
| released       =  October 1, 1975  November 14, 1975  January 16, 1976  April 8, 1977
| runtime        = 107
| country        = France French
| budget         = 
| gross          = 1,970,875 admissions (France) 
| preceded_by    = 
| followed_by    = 
}}
 French crime thriller  released on October 1, 1975, based on the autobiography of the same name written by French police detective Roger Borniche. Both film and book portray Borniches nine-year pursuit of French gangster and murderer Emile Buisson, who was executed on February 28, 1956. Hayward p. 279  Directed by Jacques Deray, the film stars Alain Delon and Jean-Louis Trintignant as Borniche and Buisson respectively, supported by Claudine Auger  and André Pousse.

==Plot==
Flic Story follows a nine-year pursuit of Emile Buisson through France during the 1940s and 1950s, and illustrates the pursuit as a battle of intellect, focusing on a growing understanding between Buisson and the protagonist Borniche.  Derays humanizing of the characters was a trait used in his other films, and was a popular counter-cliché concept in France during the 1970s. 

The film story depicts Emile Buisson, following the death of his wife and child, escaping from a psychiatric institution in 1947 and returning to Paris. Buisson, who three years later would become Frances public enemy number one, begins a murderous rampage through the French capital. The opening scene shows reluctant detective (flic is the French slang equivalent of "cop" in English) Borniche, who is given the case and pursues Buisson for three years,   at   retrieved July 30, 2007   while the latter evades capture by killing informants and anyone else he feels may give him away.  Borniche, who unlike his colleagues, prides himself in a methodical approach, hunts Buisson through numerous alleyway chases, rooftop pursuits, car chases and gunfights, while putting his lover Catherine (Auger) in danger.    at   retrieved July 30, 2007 

When bureaucracy intervenes with Borniches attempts, and politicians and the media begin speculating,  he uses the assistance of another criminal, Paul Robier (Crauchet) to apprehend Buisson. The serial killer is finally captured after having committed over 30 murders and 100 robberies.  The final sequences sees Buisson telling Borniche that he would like to "take a hacksaw" to the throat of his informer, prompting a critically lauded line from Borniche that he would not get the chance. 

==Production==
Writers Bénédicte Kermadec and   retrieved July 30, 2007 

===Foreign releases===
Flic Story was released through 1975 to 1977 in the   retrieved July 30, 2007 

==Reception==
Flic Story received mainly positive reviews from critics. James Travers of Film de France praised the film for a "quality feel and sombre mood" and the lead actors for "humanity and depth". Travers also noted several similarities to the films of Jean-Pierre Melville, particularly Le Samourai. Travers names the film as one of Derays best, although the "end result isn’t quite a masterpiece".  Other internet reviews noted similarities with Melville, and complimented the film for "unsentimental verve, intelligent pacing and refreshing honesty".  

Susan Hayward, author of French National Cinema, also complimented the film, saying it departed from mainstream style. She gave particular praise on the differences between Flic Story and American films of the same genre, by the way Deray focuses on the intellects rather than the brawn of the two leading characters, as well as the understanding that grows between the two during "months of interrogation". 

Gary Giddins, printing his review from the August 16, 2005 issue of The New York Sun, praised the film as "the most interesting and resonant" of Derays work, and gave particular credit to Trintignants "hair-trigger" performance. He also complimented the detail in the secondary characters, and said it was honest in its support for the death penalty. Giddens also, however, criticised the films pacing. Giddins p. 196 

==Cast==
* Alain Delon - Roger Borniche
* Jean-Louis Trintignant - Emile Buisson
* Renato Salvatori - Mario Poncini
* Claudine Auger - Catherine
* Maurice Biraud - The patrol of the Saint-Appoline hotel
* André Pousse - Jean-Baptiste Buisson Mario David - Raymond Pelletier
* Paul Crauchet - Paul Robier
* Denis Manuel - Lucien Darros
* Marco Perrin - Vieuchene
* Henri Guybet - Hidoine
* Maurice Barrier - Rene Bollec
* Françoise Dorner - Suzanne Bollec
* William Sabatier - Ange
* Adolfo Lastretti - Jeannot

==Notes==

 

==References==

* Cannon, Steve Popular Music in France from Chanson to Techno: Culture, Identity and Society, 2003 ISBN 0-7546-0849-2
* Giddins, Gary Natural Selection: Gary Giddins on Comedy, Film, Music, and Books, 2006 ISBN 0-19-517951-X
* Hayward, Susan French National Cinema, 2005 ISBN 0-415-30782-1
* Lisanti, Tom Film Fatales: Women in Espionage Films and Television, 1962-1973, 2002 ISBN 0-7864-1194-5

==External links==
*   at the Internet Movie Database
*   at the British Film Institute
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 
 