The Wild Bunch: An Album in Montage
{{Infobox film
| name           = The Wild Bunch: An Album in Montage
| image          =
| caption        =
| director       = Paul Seydor
| producer       = Nick Redman  Paul Seydor
| writer         =
| narrator       = Nick Redman
| starring       =
| music          = 
| cinematography =
| editing        = Paul Seydor
| distributor    =
| released       =  
| runtime        = 34 minutes
| country        = United States
| language       = English
| budget         =
}}
 short documentary film directed and edited by Paul Seydor.  The occasion for the creation of this documentary was the discovery of 72 minutes of silent black-and-white 16&nbsp;mm film footage of Sam Peckinpah and company on location in northern Mexico during the filming of The Wild Bunch. Todd McCarthy described it as, "A unique and thoroughly unexpected document about the making of one of modern cinemas key works, this short docu will be a source of fascination to film buffs in general and Sam Peckinpah fanatics in particular."  Michael Sragow wrote that the film is "a wonderful introduction to Peckinpah’s radically detailed historical film about American outlaws in revolutionary Mexico — a masterpiece that’s part bullet-driven ballet, part requiem for Old West friendship and part existential explosion. Seydor’s movie is also a poetic flight on the myriad possibilities of movie directing."  Seydor and Redman were nominated for the Academy Award for Best Documentary (Short Subject).   

==Cast==
* Walon Green as Himself (voice)
*   Newell Alexander as Cliff Coleman, William Holden (voice)
* Edmond OBrien as Himself (voice)
*   James R. Silke as Friend / colleague (voice) (as Jim Silke)
*   Mitch Carter as Gordon Dawson, Strother Martin (voice)
* Jerry Fielding as Himself (voice)
*   Sharon Peckinpah as Herself (voice)
* L. Q. Jones as Himself (voice)
* Ernest Borgnine as Himself (voice)
* Peter Rainer as Bud Hulburd (voice)
* Ed Harris as Sam Peckinpah (voice)

==Video releases==
The documentary was included in the 1999 VHS release as well as 2006 and 2007 DVD releases of The Wild Bunch.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 