Jus primae noctis (film)
{{Infobox film
 | name = Jus primae noctis 
 | image = Jus primae noctis (film).jpg
 | caption =
 | director = Pasquale Festa Campanile
 | writer =  
 | starring =  
 | music = Riz Ortolani
 | cinematography = Silvano Ippoliti
 | editing =   Nino Baragli
 | language = Italian 
 }}
Jus primae noctis is a 1972 Italian comedy film directed by Pasquale Festa Campanile.    

== Plot ==
Ariberto de Ficulle is a nobleman who came into possession of a small feud by marrying the ugly Matilde Montefiascone. Domineering in the village and fighting constantly with Gandolfo, Ariberto, not satisfied, also restores the "ius primae noctis.

== Cast ==
* Lando Buzzanca as  Ariberto da Ficulle 
* Renzo Montagnani as Gandolfo 
* Marilù Tolo as Venerata
* Felice Andreasi as Friar Puccio 
* Toni Ucci as Guidone 
* Paolo Stoppa as the pope
* Gino Pernice as Marculfo
* Ely Galleani as Beata
* Alberto Sorrentino as the friar 
* Giancarlo Cobelli as Curiale 
* Ignazio Leone 
* Clara Colosimo

==References==
 

==External links==
* 

 
 
 
 
 


 
 