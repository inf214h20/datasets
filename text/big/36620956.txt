Shubhakankshalu
{{Infobox film
| name           = Shubhakankshalu
| image          = Image_of_Subhakankshalu_film.jpg
| alt            =  
| caption        =
| director       = Bhimaneni Srinivasa Rao
| producer       = N.V. Prasad   S. Naga Ashok kumar
| writer         = Chintapalli Ramana
| screenplay     = Bhimaneni Srinivasa Rao
| story          = Vikraman Raasi
| Koti
| cinematography = Mahinder
| editing        = Gautham Raju
| studio         = Sri Sai Deva Productions
| distributor    =
| released       =   
| runtime        = 154 min 10 sec
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Raasi in the lead roles and music composed by S. A. Rajkumar & Saluri Koteswara Rao|Koti.  The film is remake of 1996 Tamil movie Poove Unakkaga.  The film recorded as Super Hit at box-office.

==Plot==
The film begins with story between two families, Christians and Hindus: Stephen (Nagesh) and Seetaramaiah (Kaikala Satyanarayana|Satyanarayana) have been best friends since college and so have their respective sons, Moses (Anand Raj) and Balaramaiah (Devan (actor)|Devan). Moses younger brother, Robert (Maharshi Raghava) and Balaramaiahs younger sister, Janaki (Rajitha) fall in love with each other. Against their families wishes, they elope. Ever since then, the tie war is going between two families.

After 25 years later, Chandu (Jagapati Babu) comes to the village and announces that he is the son of Robert and Janaki along with his friend Gopi (Betha Sudhakar|Sudhakar). He wants to reconcile the feuding families. Chandu stays in the house of the loud & frightful Nadabrahmam (Amanchi Venkata Subrahmanyam|AVS). He gradually endears himself to both families. Later, both his grandmothers suggest marriage. To avoid the situation, Gopi suggests that Chandu tell them that he is married. Chandu decides that this is the only way, thus lying that he is married to a Hindu-Christian like himself, Nirmala Mary.

Suddenly, Nirmala Mary (Ravali) arrives. Chandu is confused, as she was just a figment of his imagination. After several encounters, Nirmala Mary comes clean with Chandu, telling him that she is the only child of Robert and Janaki. She then confronts Chandu, asking him why he wants to unite the two families. Then Chandu reveals his past.

Chandu while living in the city, he lived next to a ladies hostel, where Nandini (Mantra (actress)|Raasi) daughter of Balaramaiah, was his resident. As she and her friends were trying to find a good song for a competition, Chandu indirectly helps her and falls in love with her. However she treats him as only a good friend. When Chandu was about to confess his love for her, Nandini instead tells him about her love for Lawrence, Moses son. Hearing their plan to marry without their parents consent due to the Robert-Janaki problem, Chandu volunteers to unite the families so that they can marry with the blessings of their families.

After listening to this Nirmala respects Chandus character of making his lovers marriage with her lover and she also volunteers him and they carry out the mission. Meanwhile Balaramaiah learns of Nandini-Lawrence love. The news spreads quickly like wildfire and both families now think they have run away and want to kill the members of the other family. Meanwhile, Chandu has reconciled the grandparents of the two families. Only the fathers and the lovers are his main concern. At the end, he manages to unite the lovers and their families in a logical manner.

After the marriage, the truth of Chandus appearance is revealed, by arrival of Robert & Janaki. Later, the families ask him to marry Nirmala (whose actual name is Priyadarshini) to make him a member of their family. However, he refuses, telling them that he loved a girl and does not want to think of anyone else.

==Cast==
 
* Jagapati Babu as Chandu Sudhakar as Gopi
* Ravali as Nirmala Mary/Priyadarshani Raasi as Nandini Satyanarayana as Sitaramaiah
* Nagesh as Stephen
* Anand Raj as Moses Devan as Balaramaiah AVS as Nadabrahmam
* Y. Vijaya as Nadabrahmams wife
* Sowkar Janaki as Sitaramaiahs wife
* Sukumari as Rosy (Stephens wife)
* Brahmanandam as writer Mallikarjuna Rao as Buchi
* Maharshi Raghava as Robert
* Sudha as Nandinis mother
* Rajitha as Janaki Krishna in a special appearance
* Ranjitha in a special appearance
 

==Soundtrack==
{{Infobox album
| Name        = Shubhakankshalu
| Tagline     =
| Type        = film Koti
| Cover       =
| Released    = 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 32:40
| Label       = Shivani Audio Company
| Producer    =
| Reviews     =
| Last album  = 
| This album  =
| Next album  =
}}

Music composed by S. A. Rajkumar &Saluri Koteswara Rao|Koti. Music released on Shivani Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 32:40
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Gundeninda Gudigantalu Sirivennela Sitarama Sastry
| extra1  = S. P. Balasubrahmanyam|S. P. Balu, Renuka
| length1 = 4:30

| title2  = Manasa Palakave
| lyrics2 = Sirivennela Sitarama Sastry Chitra
| length2 = 5:10

| title3  = Addanki Chera
| lyrics3 = Shanmukha Sharma
| extra3  = S. P. Balu, Chitra
| length3 = 4:46

| title4  = Ananda Ananda (F)
| lyrics4 = Shanmukha Sharma
| extra4  = Chitra
| length4 = 5:08

| title5  = Panchavannela Chilaka
| lyrics5 = Sirivennela Sitarama Sastry Sujatha
| length5 = 3:43

| title6  = O Pori Panipuri 
| lyrics6 = Bhuvana Chandra 
| extra6  = S. A. Rajkumar
| length6 = 4:11

| title7  = Ananda Ananda (M)
| lyrics7 = Shanmukha Sharma
| extra7  = S. P. Balu
| length7 = 5:03
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 