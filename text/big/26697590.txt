Paris Connections
{{Infobox film
| name = Paris Connections
| image = 
| caption = 
| director = Harley Cokeliss
| producer = 
| screenplay = Michael Tupy
| story = Jackie Collins
| starring =  
| music = 
| cinematography = 
| editing = 
| distributor =  
| released =  
| runtime = 
| country = United Kingdom
| language = English
| budget = 
| gross = 
}} 
Paris Connections is a 2010 British film directed by Harley Cokeliss, now in post-production.  The script was written by Michael Tupy, based on a thriller by Jackie Collins. 

This is the final acting appearance of Xena alumna Hudson Leick prior to her retiring to focus on her job at the Healing Heart Yoga Center.  

== Plot ==
At the beginning of Paris fashion week, a beautiful young model is brutally murdered. Investigative journalist Madison Castelli, certain that it is more than the "crime of passion" the French press says, comes to Paris to follow her story.

== Cast ==
* Nicole Steinwedell as Madison Castelli
* Charles Dance as Aleksandr Borinski
* Anthony Delon as Jake Sica
* Hudson Leick as Coco De Ville
* Trudie Styler as Olivia Hayes
* Anouk Aimée as Agnès St. Clair
* Chloé Dumas as Candi
* Caroline Chikezie as Natalie
* Fabien De Chalvron as Sergei

==References==
{{reflist|refs=

   

}}

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 