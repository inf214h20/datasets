Action Jackson (2014 film)
 
 
{{Infobox film
| name=Action Jackson
| image          = Action Jackson 2.jpg
| caption        = Theatrical release poster 
| director       = Prabhu Deva
| producer       = Gordhan Tanwani Sunil Lulla
| screenplay     = Prabhu Deva A. C. Mugil Shiraz Ahmed
| story          = A. C. Mugil
| starring       = Ajay Devgn Sonakshi Sinha Yami Gautam Kunaal Roy Kapur Manasvi Mamgai Anandaraj
| music          = Songs:  
| cinematography = Vijay Kumar Arora
| editing        = Bunty Nagi
| studio         = Baba Films
| released       =  
| distributor    = Eros International
| runtime        = 144 minutes  
| country        = India
| language       = Hindi
}} action comedy film directed by  Prabhu Deva and produced by Gordhan Tanwani and Sunil Lulla. It features Ajay Devgn in dual roles, alongside Sonakshi Sinha, Yami Gautam and Manasvi Mamgai as the female leads. Kunaal Roy Kapur appears in a supporting role with Anandaraj portraying the main antagonist. Prabhu Deva and Ajay Devgn have paired for the first time with this film. Action Jackson released on 5 December 2014. 
==Plot==
Vishi (Ajay Devgn) is a small-time crook who would do anything for money. He falls in love with Khushi (Sonakshi Sinha), a simple HR executive, who then begins a relationship with him. One day, Vishi, who frequently gets involved in fights, publically beats up a street gangster, who later seeks a revenge attack with his gang. The gangster and his gang brutally beat up Vishi, and easily escape the scene. It is then revealed that the person they had beaten is actually Vishis lookalike, Jai. On another occasion, a police inspector arrests Jai and hands him over to a group of gangsters led by Pedro (Ketan Karande). Jai single-handedly kills them all and is revealed to be a professional assassin and previous right-hand man of underworld criminal Xavier Fonseca (Anandaraj), nicknamed AJ. After AJ single-handedly rescued Xaviers sister Marina (Manasvi Mamgai) from a bunch of kidnappers, Marina falls for Jai but he rejects her as he is married to Anusha (Yami Gautam).

Angered, Xavier tries to kill Anushka twice to get AJ married to Marina, which leaves her brutally injured. Jai then escaped to India and is in the process of getting her treated. When Vishi spots Jai, he reveals his identity and story to Vishi. Jai then requests Vishi to go to Bangkok disguised as AJ to fool Xavier and his sister; meanwhile AJ makes time to get his wife operated on, as she is expecting a baby. Vishi agrees to help and goes to Bangkok along with his friend Musa (Kunaal Roy Kapur). Meanwhile, Insp. Shirke finds AJs lookalike and tries to inform Xavier but ended up murdered. The style of murder triggers suspection. Marina tortures Musa to know the truth. He reveals that Vishi is the one who masqueraded as AJ. Xavier hatches a plan to bring AJ to Bangkok by kidnapping his wife,baby and lookalike and threatens to kill all three of them. It is revealed up that Vishi is actually in Mumbai and AJ and himself switched their places in the airport. AJ reaches the spot and kills all Xaviers henchmen, Xavier himself and Marina. The film ends with AJ  and Vishi meeting at airport and Anusha advices Vishi to marry Khushi soon.It even shows that Musas hand is broken

==Cast==
* Ajay Devgan as Vishi / Jai (AJ)
* Sonakshi Sinha as Khushi
* Yami Gautam as Anusha
* Kunaal Roy Kapur as Musa 
* Manasvi Mamgai as Marina 
* Anandaraj as Xavier
* Prabhu Deva special appearance in the song "AJ"
* Ketan Karande as Pedro
* Razzak Khan
* Arti Puri as Khushis Friend
* Shahid Kapoor special appearance in the song "Punjabi Mast"
* Prabhas special appearance in the song "Punjabi Mast"
* Natasa Stankovic as Natasha
* Ankita Bhargava as Khushis Friend
* Puru Raajkumar as Inspector Shirke
* Sulabha Arya as Vishis neighbour
* Shawar Ali as Nawab (cameo)
* Rocky Verma as Riyas father in New Zealand

==Critical reception==
Prarthna Sarkar from International Business Times gave Action Jackson 1 star out of 5, stating it is a "bad dream" for viewers.  Gayatri Sarkar from Zee News gave 1 star out of 5, stating that the film failed to entertain the audience.  Deepanjana Paul from Firstpost criticised the film for its poor direction, editing and story.  Film critic Rajeev Masand rated the film 1 out of 5, feeling that the film has excessive violence.  Under a headline calling the film a feminist milestone, Raja Sen for Rediff rated it 3/5 saying that while the film is filled with "relentless absurdity", the women propel the narrative in a refreshing way.  Rachit Gupta from Filmfare gave a similar rating of 3/5, stating that the genre mixing of unscripted comedy and stylized action is " too inconsistent to garner any appreciation".  Mumbai Mirror similarly rated it 3/5 and responded positively. 

==Soundtrack==
{{Infobox album
| Name = Action Jackson
| Type = Soundtrack
| Artist = Himesh Reshammiya
| Cover =
| Released =   Feature Film Soundtrack
| Label = Eros Music
| Last album = Kick (2014 film)|Kick (2014)
| This album = Action Jackson (2014)
| Next album = Prem Ratan Dhan Payo (2015)
}}
The soundtrack was composed by Himesh Reshammiya, while the lyrics were written by Shabbir Ahmed and Sameer Anjaan.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| title1 = Keeda
| extra1 = Himesh Reshammiya, Neeti Mohan
| music1 = Himesh Reshammiya
| lyrics1 = Shabbir Ahmed
| length1 = 3:06
| title2 = Punjabi Mast
| extra2 = Himesh Reshammiya, Ankit Tiwari, Arya Acharya, Alam Gir Khan, Vineet Singh, Mohan
| music2 = Himesh Reshammiya
| lyrics2 = Shabbir Ahmed
| length2 = 4:14
| title3 = Chichora Piya 
| extra3 = Himesh Reshammiya, Shalmali Kholgade
| music3 = Himesh Reshammiya
| lyrics3 = Sameer Anjaan
| length3 = 4:27
| title4 = Dhoom Dhaam
| extra4 = Ankit Tiwari, Palak Muchhal
| music4 = Himesh Reshammiya
| lyrics4 = Sameer Anjaan
| length4 = 4:38
| title5 = Gangster Baby
| extra5 = Neeraj Shridhar, Mohan
| music5 = Himesh Reshammiya
| lyrics5 = Shabbir Ahmed
| length5 = 4:40
| title6 = Keeda
| note6 = Remix
| extra6 = Himesh Reshammiya, Mohan
| music6 = Himesh Reshammiya
| lyrics6 = Shabbir Ahmed
| length6 = 3:00
| title7 = Punjabi Mast
| note7 = Remix
| extra7 = Himesh Reshammiya, Tiwari, Acharya, Khan, Singh, Mohan
| music7 = Himesh Reshammiya
| lyrics7 = Shabbir Ahmed
| length7 = 3:53
| title8 = Chichora Piya
| note8 = Remix
| extra8 = Himesh Reshammiya, Kholgade
| music8 = Himesh Reshammiya
| lyrics8 = Sameer Anjaan
| length8 = 4:08
| title9 = Keeda (Reprise) 
| extra9 = Himesh Reshammiya, Mohan
| music9 = Himesh Reshammiya
| lyrics9 = Shabbir Ahmed
| length9 = 3:06 
}}

== References ==
 

==External links==
 

 
 
 
 
 
 
 
 