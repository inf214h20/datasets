Payback Season
{{Infobox film
| name           = Payback Season
| image          = Paybackseasonposter.jpg
| director       = Danny Donnelly
| producer       = Danny Donnelly John Adams Justin King
| writer         = Danny Donnelly Jenny Fitzpatrick
| starring       = Adam Deacon Nichola Burley David Ajala Leo Gregory Anna Popplewell Geoff Hurst
| music          = 
| cinematography = James Martin
| editing        = Oliver Parker
| studio         = Pure Film Productions Angry Badger Pictures 
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 88 mins.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British drama film that was released to cinemas on March 9, 2012. The film was directed by Danny Donnelly, and stars Adam Deacon, Nichola Burley and Leo Gregory.   

==Plot==
Jerome (Adam Deacon), is a successful young footballer, who in the midst of playing the most important season of his career. When he goes to visit his mom on the housing estate he grew up on, he accidentally bumps into some of his old childhood friends, lead by drug dealing loanshark and gangster Baron (David Ajala). Jerome offers to take the lads on a night out - but Baron, living in jealousy of Jeromes success, takes advantage of the situation and asks him for £10,000 to tide over his cashflow problem. Jerome agrees to give him the money, but no sooner does he do so, he finds that Baron has enlisted his younger brother Aaron (Liam Donnelly) to help him on a hit. When he confronts Baron, Baron informs him that in order to keep his brother safe, he will need to stump up another £10,000. Not realising that he is being blackmailed, Jerome agrees. A week later, Baron threatens him for more money. Realising that he is being taken for a mug, he enlists the help of his trainer Andy (Leo Gregory) to inform Baron that he wont be getting any more money. However, the warning soon backfires on Jerome when Baron trashes his car and attacks Andy with a knife, leaving him in intensive care. With no choice but to put a stop to Baron, Jerome arrives at his flat to confront him, only to be stabbed in the leg by Baron in the process. With time slowly running out, the arrival of one of Barons heavies stops a fight between the two. Baron orders him to shoot Jerome, only for him to shoot Baron before running away. Jerome is left on the floor, breathing heavily.

==Cast==
* Adam Deacon as Jerome 
* Nichola Burley as Lisa 
* David Ajala as Baron 
* Leo Gregory as Andy 
* Anna Popplewell as Izzy 
* Geoff Hurst as Adam Avely 
* DJ Spoony as Club DJ 
* Zaraah Abrahams as Clarissa  Ross Anderson as Waiter 
* Liam Donnelly as Aaron 
* Alex Esmail as Leon
* Philip Howard as Clubber Cameo 
* Louisa Lytton as Keisha 
* Sinead Moynihan as Charlotte 
* Kelly Wenham as Melissa  Danny Young as Ian 
* Nina Young as Sandra

==Production==
In August 2011, Geoff Hurst signed on to play the part of a football agent, making this his first role in a feature film.      

===Critical reception ===
Variety (magazine)|Variety wrote that the film was "let down by generic material thats light on both action and persuasive plotting",  but offered that a later edited release may be more successful.     View Auckland panned the film and its director, and wrote that the film was "poorly directed and often excruciating to watch",  with both a simplistic script and "an awkward central performance from BAFTA Rising Star Adam Deacon."   They felt that while Deacon was fine when in "wise-cracking support roles or weaselly scumbags",  he was "nobodys idea of a charismatic leading man, let alone a Premiership footballer."  They did note that while the performances of Leo Gregory and David Ayala were strong, co-star Nichola Burleys part was unconvincing and Anna Popplewells was "shockingly bad."     The Observer also panned the film, describing it as a "poorly directed, badly written, inadequately acted" film, in which "nothing rings true, not even the tones on the characters mobile phones."     The Independent awarded it 2 out of 5 stars but praised David Ajalas performance for its "intensity, menace and humour".   

==References==
 

==External links==
*   at the Internet Movie Database
*  

 
 
 
 
 
 