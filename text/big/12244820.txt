Belli Moda
{{Infobox film name           = Belli Moda image          =  image_size     =  caption        =  director       = Puttanna Kanagal producer       = T. N. Srinivasan writer  Triveni
|based on       =   screenplay     = Puttanna Kanagal starring  Kalpana K. S. Ashwath Pandari Bai Balakrishna (Kannada actor)|T. N. Balakrishna music          = Vijaya Bhaskar cinematography = R. N. K. Prasad editing        = V. P. Krishnan distributor    =  released       = 1967 runtime        =  country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}

Belli Moda ( ) is a 1967 Kannada movie by Puttanna Kanagal. Belli Moda literally means silver cloud. It was based on a novel Belli Moda,  by the acclaimed Kannada writer Anasuya Shankar|Triveni.

==Plot==
The movie examines the tumults in the lives of the people residing in the Belli Moda estate. Indira is the heiress of her fathers estate, named Belli Moda. A young man,  Mohan, is engaged to her and is desirous of owning the Belli Moda, once Indiras father passes away. Mohan returns from the US, only to discover that his fiancees mother has died in labor, leaving behind a son - the new inheritor of Belli Moda. This shatters Mohans dreams of the estate and he bitterly refuses to marry Indira. One day, Mohan meets with a car accident and is severely injured. He is nursed by Indira, and presently, Mohan falls truly in love with her and proposes for marriage. However, this time, Indira refuses to marry him and breaks his heart, for he only cared about her money.

==Cast==
* Kalyan Kumar Kalpana
* Ashwath
* Pandari Bai Balakrishna
* B. Jaya (actress)|B. Jaya

==Reception==
Bellimoda gave Puttanna the first break in his directorial venture. Kalyan Kumar, known for his sophisticated portrayal of roles, plays the antihero. Actress Kalpana is excellent as the protagonist. She later came to be known as Minugu Thare (The Twinkiling Star) owing to a reference in the title song of this movie. Her residence in Bangalore was also called Belli Moda, as a tribute to the movie and her well-established identity owing to the same. Actors Ashwath and Pandari Bai  provide great supporting performances. Music is excellent with some great songs which are etched in everybodys memory forever. This film screened at IFFI 1992 Kannada cinema Retrospect.

==Award==
Karnataka State Film Awards 1967-68
* Second Best Film 
* Best Actress – Kalpana 
* Best Supporting Actress – Pandaribai 
* Best Music Director – Vijaya Bhaskar 
* Best Screenplay – S R Puttanna Kanagal 
* Best Cinematographer – R N K Prasad 
* Best Editing – V P Krishnan 

==Sound track==
{| class="wikitable sortable"
|-
! Title !! Singers !! Lyrics 
|- Susheela || R. N. Jayagopal 
|-
| Ideega Nee Doorade|| P.B. Sreenivas, P. Susheela|Susheela|| R. N. Jayagopal 
|- Susheela || R. N. Jayagopal
|-
| Moodala Maneya Muttina Neerina || S. Janaki || D. R. Bendre
|-
| Muddhina Giniye Baaro|| P. Susheela|Susheela|| R. N. Jayagopal 
|-
| Odeyithu Olavina|| P.B. Sreenivas || R. N. Jayagopal 
|}

== External links ==
*  

 
 
 
 
 
 


 