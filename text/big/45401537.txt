Charming Sinners
{{Infobox film
| name           = Charming Sinners
| image          = 
| alt            = 
| caption        = 
| director       = Robert Milton 	
| producer       = 
| screenplay     = W. Somerset Maugham Doris Anderson
| starring       = Ruth Chatterton Clive Brook Mary Nolan William Powell Laura Hope Crews Florence Eldridge
| music          = Karl Hajos W. Franke Harling
| cinematography = Victor Milner 
| editing        = Verna Willis 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Charming Sinners is a 1929 American drama film directed by Robert Milton and written by W. Somerset Maugham and Doris Anderson. The film stars Ruth Chatterton, Clive Brook, Mary Nolan, William Powell, Laura Hope Crews and Florence Eldridge. The film was released on August 17, 1929, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Ruth Chatterton as Kathryn Miles
*Clive Brook as Robert Miles
*Mary Nolan as Anne-Marie Whitley
*William Powell as Karl Kraley
*Laura Hope Crews as Mrs. Carr
*Florence Eldridge as Helen Carr
*Montagu Love as George Whitley
*Juliette Crosby as Margaret
*Lorraine MacLean as Alice
*Claud Allister as Gregson

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 