The Morals of Marcus
 
{{Infobox film
| name           = The Morals of Marcus
| image          = 
| image_size     = 
| caption        = 
| director       = Miles Mander
| producer       = Julius Hagen
| writer         = William John Locke (novel)   Guy Bolton   H. Fowler Mear   Miles Mander
| narrator       =  Ian Hunter Adrianne Allen Noel Madison
| music          = W.L. Trytel
| cinematography = Sydney Blythe Jack Harris
| studio         = Twickenham Studios
| distributor    = Gaumont British Distributors
| released       = 1935
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British comedy Ian Hunter 1915 with 1921 with May McAvoy.

==Production== The Morals of Marcus Ordeyne by William John Locke and was filmed at Twickenham Studios. It was part of the producer Julius Hagens ambitious programme of film production at Twickenham. Hagen brought in Vélez, a leading Mexican actress, to give the film greater international appeal.

==Main cast==
* Lupe Vélez - Carlotta Ian Hunter - Sir Marcus Ordeyne
* Adrianne Allen - Judith
* Noel Madison - Tony Pasquale
* J.H. Roberts - Butler

==References==
 

==Bibliography==
* Richards, Jeffrey (ed.) The Unknown 1930s: An Alternative History of the British Cinema, 1929-1939. I.B. Tauris, 1998.

 

 
 
 
 
 
 
 
 
 


 