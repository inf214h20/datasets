Porzûs
{{Infobox film
 | name = Porzûs
 | image = Porzûs poster 97.jpg
 | caption =
 | director = Renzo Martinelli
 | writer =  	Renzo Martinelli   Furio Scarpelli
 | starring =  
 | music =  Flavio Colusso 
 | cinematography =  Giuliano Giustini
 | editing =   
 | producer =  
| released       =  
| country        =   Italian
 }} 1997 Cinema Italian historical war-drama film written and directed by Renzo Martinelli.     For his performance in this film Lorenzo Crespi won the Globo doro for best breakthrough actor,  while Gastone Moschin was nominated for Silver Ribbon for best supporting actor. 

== Plot ==
One of the survivors of the Porzûs massacre, Storno, travels to Slovenia to visit Geko, who did not see since the end of the war. The two recall all the dramatic events that have seen them protagonists between late 1944 and February 1945.

== Cast ==
*Lorenzo Crespi as  Carlo Tofani "Geko" (young)
*Gastone Moschin as  Carlo Tofani "Geko" (old)
*Lorenzo Flaherty as  Umberto Pautassi "Storno" (young)
*Gabriele Ferzetti as  Umberto Pautassi "Storno" (old)
*Giuseppe Cederna as  "Nullo"
*Giulia Boschi as Ada Zambon
*Bruno Bilotta as "Dinamo"
*Massimo Bonetti as  "Gobbo"
*Lino Capolicchio as  "Galvano"
*Gianni Cavina as  "Spaccaossi"
*Salvatore Calaciura as  "Africa"
*Victor Cavallo as  "Scabbia"
*Mariella Valentini as  Albina 
*Pietro Ghislandi as  "Faccia-smorta"

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 