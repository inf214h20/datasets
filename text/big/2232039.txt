Scarecrow (1973 film)
{{Infobox film
| name = Scarecrow
| image = Scarecrow_movieposter.jpg
| image_size =
| caption = original film poster
| director = Jerry Schatzberg
| producer = Robert M. Sherman
| writer = Garry Michael White Richard Lynch
| music = Fred Myrow
| cinematography = Vilmos Zsigmond
| editing = Evan Lottman
| distributor = Warner Bros.
| released =   11 April 1973 (New York City only)
| runtime = 112 minutes
| language = English
| gross = $4,000,000 (US/ Canada rentals) 
}}
 1973 road movie starring Gene Hackman and Al Pacino.

==Plot==
 
The story revolves around the odd relationship between two  ), a short-tempered ex-convict, and Francis Lionel "Lion" Delbuchi (Al Pacino), a childlike ex-sailor. They meet on the road in California and agree to become partners in a business, once they reach Pittsburgh.

Lion is on his way to Detroit to see the child he has never met and make amends with his wife Annie, to whom he has been sending all the money he made while at sea. Max agrees to make a detour on his way to Pittsburgh, where the bank that Max has been sending all his seed money is located. His plans are to open a car wash, with Lionel as a partner.
 Richard Lynch). Max proceeds to teach Riley a lesson, rekindling his friendship with Lion.

The two have a profound effect on each other, with Lion becoming more of an adult and Max loosening up his high-strung aggression (at one point doing a tongue-in-cheek striptease to defuse a fight at a bar). When they do finally make it to Detroit, Max has to take care of Lion, who becomes catatonic after hearing of the passing of his unborn child (a lie made up by Annie to make Lion feel guilty for leaving them).

==Cast==
* Gene Hackman as Max Millan
* Al Pacino as Francis Lionel "Lion" Delbuchi
* Eileen Brennan as Darlene
* Dorothy Tristan as Coley
* Ann Wedgeworth as Frenchy Richard Lynch as Riley
* Penelope Allen as Annie Gleason
* Richard Hackman as Mickey Grenwood
* Al Cingolani as Skipper
* Rutanya Alda as Woman in camp

==Casting==
* Penelope Allen plays the mother of Pacinos son in Scarecrow. She would next appear as the head teller of the bank that Pacino robs in Dog Day Afternoon.
* Gene Hackmans brother Richard appears as Mickey in the film.

==Awards and Criticism==
  Grand Prix at the 1973 Cannes Film Festival with The Hireling directed by Alan Bridges.    

In a   of the film from the time of its 2013 re-release, Peter Bradshaw of the Guardian described the film as "a freewheeling masterpiece", describing Hackman and Pacino as giving "the performances of their lives".

Peter Biskind, on the other hand, described the film as being of "secondary" significance in his book Easy Riders, Raging Bulls.

==References==
 

Director Jerry Schatzberg still resides in New York City, where he is working on several film projects, including a sequel to "Scarecrow," co-written with Bruce Springsteens former Publicist, Seth Cohen  . Variety Magazine interviewed Schatzberg about the planned sequel, and confirmed that Al Pacino had recently been provided with a copy of the script   .

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 