Beverly Hills Brats
 
{{Infobox film
| name           = Beverly Hills Brats
| image          = BHBratscover.jpg
| caption        = Beverly Hills Brats DVD cover
| director       = Jim Sotos	 	 Terry Moore Jerry Rivers Terry Moore Terry Moore Fernando Allende George Kirby Cathy Podewell Ramon Estevez
| music          = Barry Goldberg
| cinematography = Harry Mathias
| editing        = Jerry P. Frizell		 	
| distributor    = Taurus Entertainment Company
| released       = 1989
| runtime        = 90 minutes
| country        = USA
| awards         = English
| budget         =
}}
 American comedy Terry Moore, cameo role.

==Plot==
Scooter (Peter Billingsley|Billingsley) is a teen from a wealthy Beverly Hills family. After his plastic surgeon father (Martin Sheen|Sheen) remarries, Scooter is virtually ignored by his father and stepmother (Terry Moore (actress)|Moore), and treated badly by his two other spoiled siblings, Sterling (Ramon Estevez) and Tiffany (Cathy Podewell). Scooter devises a plan to fake his own kidnapping to get his parents attention and enlists the help of two bumbling crooks, Clive (Burt Young|Young) and Elmo (George Kirby|Kirby). After Scooter is "kidnapped" and a ransom is demanded, he quickly realizes that his plan failed to work and his parents dont miss him.

==Cast==
*Peter Billingsley as Scooter
*Martin Sheen as Dr. Jeffery Miller Terry Moore as Veronica
*Burt Young as Clive
*George Kirby as Elmo
*Cathy Podewell as Tiffany
*Ramon Estevez as Sterling
*Natalie Schafer as Lillian
*Fernando Allende as Roberto
*Joe Santos as Spyder
*Robert Tessier as Slick
*Ruby Keeler as Goldie
*Vito Scotti as Jerry
*Pat Renella as Lt. Gofield
*Whoopi Goldberg as Herself
*Cort McCown as Bart
*Tonya Townsend as Tulip
*Duncan Bravo as Manny Jimmy Justice as Deacon
*(Michael J Aronin) as the Cop

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 


 