Variety Jubilee
{{Infobox film
| name           = Variety Jubilee
| image          =
| image_size     =
| caption        =
| director       = Maclean Rogers
| producer       = F.W. Baker
| writer         = Kathleen Butler   Mabel Constanduros 
| narrator       =
| starring       = Reginald Purdell   Ellis Irving   Lesley Brook   Betty Warren
| music          = Percival Mackey
| cinematography = Geoffrey Faithfull
| editing        = A. Charles Knott
| studio         = Butchers Film Service
| distributor    = Butchers Film Service
| released       = 14 June 1943
| runtime        = 92 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} historical musical Champagne Charlie. 

==Cast==
* Reginald Purdell as Joe Swan 
* Ellis Irving as Kit Burns 
* Lesley Brook as Evelyn Vincent 
* Betty Warren as Florrie Forde 
* Tom E. Finglass as Eugene Stratton 
* John Rorke as Gus Elen 
* Marie Lloyd Jr. as Herself 
* George Robey as Himself 
* Charles Coborn as Himself 
* Ella Retford as Herself  Charles Shadwell as Himself 
* Joan Winters as Herself 
* Nat Ayer as Himself
* Slim Rhyder as Himself  
* Tessa Deane as Herself  
* Wilson, Keppel and Betty as Themselves
* Band of the Coldstream Guards as Themselves
* Peter Noble as Chairman 
* Louis Bradfield as Chris Burns 
* Amy Dalby as Suffragette
* Arthur Hambling as Commissionaire 
* Bryan Herbert as Guest at Theatre Opening 
* Vi Kaley as Evelyns Dresser  
* David Keir as Theatre Bar Patron 
* Pat McGrath as Kit Burns Jr  George Merritt as Music Hall Chairman

==References==
 

==Bibliography==
* Harper, Sue. Picturing the Past: The Rise and Fall of the British Costume Film. British Film Institute, 1994.
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain 1939-48. Routledge, 1992.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 