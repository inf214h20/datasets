Sabarimala Sree Dharmashastha
{{Infobox film 
| name           = Sabarimala Sree Dharmashastha
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = CRK Nair
| writer         = Mythology Kedamangalam Sadanandan (dialogues)
| screenplay     = Kedamangalam Sadanandan Hari Manavalan Joseph Sridevi
| music          = V. Dakshinamoorthy Jaya Vijaya
| cinematography = Shailan Bose
| editing        =
| studio         = Sastha Films
| distributor    = Sastha Films
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by CRK Nair. The film stars Thikkurissi Sukumaran Nair, Hari (actor)|Hari, Manavalan Joseph and Sridevi in lead roles. The film had musical score by V. Dakshinamoorthy and Jaya Vijaya.   

==Cast==
*Thikkurissi Sukumaran Nair Hari
*Manavalan Joseph
*Sridevi
*T. S. Muthaiah Ambika
*Kottarakkara Sreedharan Nair Padmini
*Ragini Ragini
*S. P. Pillai

==Soundtrack==
The music was composed by V. Dakshinamoorthy and Jaya Vijaya and lyrics was written by MP Sivam, Bhoothanadha Sarvaswam, P. Bhaskaran, Vayalar Ramavarma, Traditional, Sankaracharyar, K Narayanapilla and Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ayyappa Sharanam || K. J. Yesudas || MP Sivam || 
|-
| 2 || Darshanam Punya Darshanam || K. J. Yesudas || MP Sivam || 
|-
| 3 || Dhyaaye Charu Jata || P Jayachandran || Bhoothanadha Sarvaswam || 
|-
| 4 || Ellaam Ellaam || KP Brahmanandan, Jaya Vijaya, KK Balan, M Henry, RC Suresh, S Joseph, Vaikkom Gopinath, VT Aravindakshamenon || P. Bhaskaran || 
|-
| 5 || Harisreeyennaadyamaay || Nanu Aasan || Vayalar Ramavarma || 
|-
| 6 || Hemaambaraadambaree || P. Leela || Vayalar Ramavarma || 
|-
| 7 || Karagre vasathe || Ambili || Traditional || 
|-
| 8 || Lapannachyuthananda || K. J. Yesudas, P. Leela, Ambili, Latha Raju, P Susheeladevi || Sankaracharyar || 
|-
| 9 || Madhuraapura Nayike || P. Leela || Sankaracharyar || 
|-
| 10 || Mudaakaraatha Modakam   || P Jayachandran, Ambili, KP Brahmanandan, Jaya Vijaya, Latha Raju, P Susheeladevi || Sankaracharyar || 
|-
| 11 || Neyyitta Vilakku || P Susheela || K Narayanapilla || 
|-
| 12 || Njaattuvelakku Njan Natta || P Susheeladevi || Vayalar Ramavarma || 
|-
| 13 || Om Namasthe Sarvashaktha || P Jayachandran, KP Brahmanandan, Kesavan Nampoothiri || K Narayanapilla || 
|-
| 14 || Paarvanendu || P. Leela, Ambili, Latha Raju, P Susheeladevi, Leela Warrier || P. Bhaskaran || 
|-
| 15 || Sharanam Sharanam || Jaya Vijaya || Sreekumaran Thampi || 
|-
| 16 || Shiva Rama Govinda || K. J. Yesudas || Traditional || 
|-
| 17 || Thripura Sundaree Naadhan || KP Brahmanandan, Jaya Vijaya, KK Balan, M Henry, RC Suresh, S Joseph, Vaikkom Gopinath, VT Aravindakshamenon || Sreekumaran Thampi || 
|-
| 18 || Unmaadinikal Udyaanalathakal || P. Leela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 