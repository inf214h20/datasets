Music of the Heart
{{Infobox film
| name           = Music of the Heart
| image          = Music of the heart.jpg
| caption        = 
| director       = Wes Craven
| producer       = Susan Kaplan Marianne Maddalena Allan Miller Walter Scheuer
| writer         = Pamela Gray
| starring       = Meryl Streep Gloria Estefan Angela Bassett Aidan Quinn Cloris Leachman Jane Leeves Kieran Culkin Charlie Hofheimer
| music          = Mason Daring
| cinematography = Peter Deming
| editing        = Gregg Featherman Patrick Lussier
| distributor    = Miramax Films
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$27 million
| gross          = United States dollar|$14,859,394 	 
}}
 directed by Buena Vista Distribution. The film is a dramatization of the true story of Roberta Guaspari, portrayed by Meryl Streep, who co-founded the Opus 118 Harlem School of Music. The film also stars Aidan Quinn, Gloria Estefan, and Angela Bassett. It is director Wes Cravens only foray outside of the horror/thriller genre to date, aside from his contribution to the multifaceted and directorially diverse Paris, je taime. It was also his only film to receive Academy Award nominations.

==Plot==
the film opens with violinist Roberta Guaspari having been deserted by her US Navy husband and feeling devastated, almost Suicide|suicidal. Encouraged by her mother, she attempts to rebuild her life and a friend from student days recommends her to the head teacher of a school in the tough New York area of East Harlem. Despite a degree in music education, she has little experience in actual music teaching, but shes taken on as a substitute violin teacher. With a combination of toughness and determination, she inspires a group of kids, and their initially skeptical parents. The program slowly develops and attracts publicity.

Ten years later, the string program is still running successfully at three schools, but suddenly the school budget is cut and Roberta is out of a job. Determined to fight the cuts, she enlists the support of former pupils, parents and teachers and plans a grand fund-raising concert, Fiddlefest, to raise money so that the program can continue. But with a few weeks to go and all participants furiously rehearsing, they lose the venue. Fortunately, the husband of a publicist friend is a violinist in the Guarneri Quartet, and he enlists the support of other well-known musicians, including Isaac Stern and Itzhak Perlman. They arrange for the concert to be mounted at Carnegie Hall. 
 Karen Briggs, Sandra Park, Diane Monroe, and Joshua Bell, join in the performance, which is a resounding success.

The films end credits declare that the Opus 118 program is still running successfully. They also report that the schools funding was restored during the making of the film.

==Production== Best Documentary Madonna was originally signed to play the role of Guaspari, but left the project before filming began, citing "creative differences" with Craven. When she left, Madonna had already studied for many months to play the violin.  Streep learned to play Double Violin Concerto (Bach)|Bachs Concerto for 2 Violins for the film.

==Awards and nominations==
Streep received nominations for an Academy Award, a Golden Globe and a Screen Actors Guild Award for her lead performance.   IMDb. Accessed January 28, 2007. 

The films theme song, "Music of My Heart", scored songwriter Diane Warren a nomination for an Academy Award for Best Original Song, and a Grammy Award nomination for Best Song Written for a Motion Picture, Television or Other Visual Media. 

The film marked the screen debut of singer Gloria Estefan.   CNN, October 29, 1999. Accessed January 28, 2007. 

==Critical reception==
The film got mixed reviews but with a positive trend. Most critics applauded Meryl Streeps portrayal of Roberta Guaspari. The film had a 62% approval rating at Rotten Tomatoes.  Critic Eleanor Ringel Gillespie of the Atlanta Journal-Constitution concluded that "There are more challenging movies around. More original ones, too. But "Music of the Heart" gets the job done, efficiently and entertainingly."  Roger Ebert gave the film three stars out of four and wrote that "Meryl Streep is known for her mastery of accents; she may be the most versatile speaker in the movies. Here you might think she has no accent, unless youve heard her real speaking voice; then you realize that Guasparis speaking style is no less a particular achievement than Streeps other accents. This is not Streeps voice, but someone elses - with a certain flat quality, as if later education and refinement came after a somewhat unsophisticated childhood."  Steve Rosen said that "The key to Meryl Streeps fine performance is that she makes Guaspari unheroically ordinary. Ultimately that makes her even more extraordinary." 

==Soundtrack album track listing==
# "Music of My Heart" - Gloria Estefan and N Sync|*NSYNC (4:32)
# "Baila" - Jennifer Lopez (3:54)
# "Turn the Page" - Aaliyah (4:16) Menudo (4:37)
# "Seventeen" - Tre O (3:48) C Note (5:04)
# "Do Something" (Organized Noize Mix) - Macy Gray (3:53)
# "Revancha de Amor" - Gizelle dCole (4:06)
# "Nothing Else" - Julio Iglesias, Jr. (4:23)
# "Love Will Find You" - Jaci Velasquez (4:34)
# "Music of My Heart" (Pablo Flores Remix) - Gloria Estefan and *NSYNC (4:23) 
# "Concerto in D Minor for Two Violins" - Itzhak Perlman and Joshua Bell (3:56)

==Box office==
The film opened at #5 at the North American box office making United States dollar|$3.6 million in its opening weekend.

==See also==
* White savior narrative in film

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 