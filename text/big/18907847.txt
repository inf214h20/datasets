A Little Death: A Modern Day Fairytale
 
 
A Little Death is a 16mm short film that was created by   and   in 1995. Its concept evolved from an earlier idea called Into The Void, which involve a male character walking in on his lover in bed with another. The interest of the idea centred on the ambiguity of the lovers gender, and by inference the sexual orientation of the observer. {{cite video  | people = Perkins, Simon; Swadel, Paul (Directors) | date = 1994
| title =   
| medium = 16mm
| location = Auckland, New Zealand | publisher = James Wallace Productions}}  {{cite web | date = 24 February 2004 | title = A Little Death | work = folksonomy | location = UK
| url = http://folksonomy.co/index.php?s=190  | accessdate = 3 September 2010}} 

Read a   of the short film featured in the NZ Pavement Magazine (1995).  .

The script for this film was written as a Beatscript {{cite web | last = Perkins | first = Simon | year = 2005 | work = folksonomy | location = UK | accessdate = 3 September 2010
| title = Beatscript: action-centred scripting for shortfilms and animations
| url = http://folksonomy.co/?permalink=475 }}  {{cite web | last = Perkins | first = Simon | year = 2005 | work = folksonomy | location = UK | accessdate = 3 September 2010
| title = Beatscript: action-centred scripting for shortfilms and animations The Coming of Age of The New Zealand Short Film ( ).

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 


 
 