The Horde (film)
 
{{Infobox film
| name           = The Horde
| image          = Maxim Suhanov and Roza Hairullina in the film "The Horde".JPG
| director       = Andrei Proshkin
| producer       = Natalya Gostyushina Sergey Kravets
| writer         = Yury Arabov
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Maxim Sukhanov Roza Hairullina
| music          = Alexey Aygi
| cinematography = Yury Raysky
| editing        = 
| studio         = Pravoslavnaya Entsiklopedia (Orthodox Encyclopædia)
| distributor    = 
| released       =  
| runtime        = 127 minutes
| country        = Russia
| language       = Russian, Karachay-Balkar
| budget         = $12 million
| gross          = 
}} Saint Alexius khan Jani Beg from blindness.

==Plot== Ivan the Fair (Vitaly Khaev) hand Alexius to him as a healer. Alexius is reluctant but Ivan sees this as a rare opportunity to delay the inevitable Tatar attack on Moscow. Eventually, Alexius succumbs and, accompanied by Jani Begs retainers Timer (Fedot Lvov) and Badakyul (Aleksey Yegorov), travels to Saray-Jük with his keleynik Fedka (Aleksandr Yatsenko). They fail to cure Taidulas blindness and Alexius is banished, while Fedka is taken as a slave for desecrating the Threshold (door)|threshold. After a period of suffering and subsequent sanctification of Alexius, Taidulas eyes are healed. Alexius and Fedka return to Moscow. Shortly after, Jani Beg is assassinated by his son Berdi Beg (Moge Oorzhak).

==Language== Kipchak spoken by the 14th century Golden Horde. 

==Accolades==
*   Moscow International Film Festival:
** “Silver George” for the best director: Andrey Proshkin
** “Silver George” for the best actress: Roza Hairullina NETPAC Prize: ″Admirable combination of perfect cinematic images together with a strong idea of mercy in the times of severe oppression″.

==References==
 
*  

==External links==
*  

 
 
 
 
 
 
 
 
 


 