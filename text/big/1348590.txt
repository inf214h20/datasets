The Protector (1985 film)
 
 
 
{{Infobox film
| name           = The Protector
| image          = TheProtector1985.jpg
| caption        = Film poster of the US version
| director       = James Glickenhaus
| producer       = David Chan Shek Hong-chan Raymond Chow Leonard Ho
| writer         = James Glickenhaus Edward Tang
| starring       = Jackie Chan Danny Aiello Roy Chiao Moon Lee Yeung Kwan
| music          = Ken Thorne
| cinematography = Chang Yao-chu Mark Irwin Johnny Koo Jimmy Leung  Ardy Lam Gary Hoh
| editing        = Yao Chung-chang Evan A. Lottman Barbara Minor Peter Cheung Golden Harvest (Hong Kong)  Warner Bros. (USA)
| released       =  
| runtime        = 91 min.
| country        = Hong Kong United States
| awards         = English
| budget         =
| gross          = Hong Kong: HK$13,917,612   United States: US$981,817 
}}
The Protector ( ) is a 1985 Hong Kong-American   as a response to this film.

==Plot==
===US version=== New York police officers Billy Wong (Jackie Chan) and Michael Alexander (Patrick James Clarke) are patrolling the neighborhood. On the radio, a dispatcher says that a truck was stripped by their premises, so the cops check it out. As the trucker comes to, he sees that his cargo has been stolen. Michael radios in the crime, while Billy passes the driver his hat, saying, "Welcome to New York".

Later, the two cops are in a bar. Billy Wong (Jackie Chan) goes to the bathroom while Michael Alexander (Patrick James Clarke) orders another round. Outside, a gang plan to rob the bar, unaware of the cops inside. They charge in busting the door and start roughing up the customers and gathering them by the till. Billy, still in the bathroom, has heard the commotion. The gang forces the customers to stand by the register, while one member tries to open the locked bathroom. Gun drawn, he breaks down the door, but Billy has overhead everything and shoots the gangster four times before he can make the shot.
 harbor police air unit who arrive on the scene and hoist him a line. Billy grabs onto the line, and lets his speedboat run into the henchmans boat, destroying both.
 Bill "Superfoot" Wallace), one of the bodyguards to Martin Shapiro (Ron Dandrea). A kidnapping has taken place, and nobody knows why. They later learn that crime boss Harold Ko (Roy Chiao) may have smuggled Shapiros daughter, Laura (Saun Ellis), to Hong Kong for ransom. The men get a lead – Garrucci has made calls to a massage parlor.

While investigating, they get massages, but Billy sees a reflection of his masseuse pulling out a knife so he jumps, kicking her in the face and throws a lamp at the other masseurs. Billy and Garoni fight off the remaining clientele, before getting questioned by the leader of the massage parlour.

They go to Lee Hing (Kwan Yeung), to cash in a coin. A man named Stan Jones (Kim Bass) gets on the boat, asking for supplies. Stan warns Billy and Garoni they are being followed and Billy goes to get information from the leader. The leader throws a knife at him and escapes onto another boat. Billy and Garoni head back to their hotel, finding cash in a suitcase on the bed. They are attacked by two men, but manage to kill them. They make their exit and are taken to the police station in Hong Kong.

Garoni follows Garrucci to a drug laboratory, while Billy sees Hings tortured and murdered body on his sunken boat. They go to a Buddhist priest who tells them what they are looking for. Billy, Garoni and Stan go to the drug lab, and destroy it, saving Laura Shapiro in the process. Garoni is shot by Garrucci, and is held hostage unless Billy returns Laura to Ko.
 cargo lifter half-loaded with boxes, with Billy dodging them. Garoni goes outside with the gang and kills a sniper with a 6-shot 20mm cannon. Billy is shot at, but eventually makes it to the top of a crane and Kos helicopter gets crushed when Billy drops the crane on the helicopter killing Ko .
 NYPD Medal of Honor.

===Hong Kong version===
Even though the narrative is the same, the Chinese version has a subplot featuring Sally Yeh in connection to the coin and her uncle working at the massage parlour. A large number of scenes were cut to improve the pace of the film and to cut down on any instances of nudity. Fully dressed lab women were added as a replacement to give more logical sense for the Chinese version. A lot of swearing and American slang has been totally changed to make the story cohesive enough for the story to work for the Chinese audience.

Bill Wallace also has an extra scene outside the ice warehouse against Lee Hoi San in which it sets him up as a formidable opponent against Jackie Chan later on in the warehouse. The final fight scene is re-edited to make it more of a Hong Kong style. Overall, the script has been cleaned up to make it compatible for Chans audience, to make it distinctive enough from the US version. 

==Cast==
* Jackie Chan – Billy Wong
* Danny Aiello – Danny Garoni
* Moon Lee – Soo Ling
* Roy Chiao – Harold Ko
* Peter Yang – Lee Hing (uncredited)
* Sandy Alexander – Gang Leader
* Jesse Cameron-Glickenhaus – Jesse Alexander
* Becky Ann Baker – Samantha Alexander (as Becky Gelke)
* Kim Bass – Stan Jones
* Sally Yeh – May Fung Ho / Sally (uncredited)
* Paul L. Smith – Mr. Booar (uncredited) Bill Wallace – Benny Garrucci
* Victor Arnold – Police Captain
* Shum Wai – massage house manager (uncredited)
* Irene Britto – Masseuce
* Ron Dandrea – Martin Shapiro
* Saun Ellis – Laura Shapiro
* Lee Hoi San – Wing (Hong Kong version) (uncredited)
* Alan Gibbs – Gunman
* David Ho – David
* Joe Maruzzo – Marina Attendant (as Joseph Maruzzo) John Spencer – Kos pilot Mike Starr – Hood (as Michael Starr)
* James Glickenhaus – Man walking in front of store (uncredited) Joe Wong – Sergeant Chan
* Kam Bo-wong – Bald Thug (as Kobe Wong)
* Fung Hak-on – Thug with Ice Pic
* Wan Faat – Thug
* Johnny Cheung – Thug
* Lam Wan-seung – Thug
* Lee Fat-yuen – Thug
* Chung Wing – Thug
* Tai Bo – Thug
* Patrick James Clarke – Michael
* John Ladalski – Kos Van Driver (uncredited)
*Big John Studd – Huge Hood
* Robert Mak
* Mark Cheung

==Production==
According to his book I Am Jackie Chan: My Life in Action, Chan broke his hand while filming a stunt scene.   

==Version comparison==
The relationship between James Glickenhaus and Jackie Chan was, according to various sources, highly contentious for most of the production.  Chan was appalled at the way Glickenhaus directed the fight scenes, feeling that his methods were sloppy and lacked attention to detail.  At one point he offered to direct the fight scenes himself, but Glickenhaus refused.  Things became so bad that Chan walked off the set, but was forced to return and finish the film by contractual obligation.  However, when preparing the film for release in Hong Kong, Chan completely re-edited the film and shot new footage to both fit his style of film making and remove all content he found objectionable.

The following changes were made by Jackie Chan for the Hong Kong release of the film:

===Scenes in the US===
* The orange bearded crook plays with the teddy bear near Michael. (deleted)
* Billy Wong asks a civilian where the black bearded crook who shot Michael has gone. (added)
* The black bearded crook climbs over a chain-linked fence. (deleted)
* Michaels funeral. (deleted)

===Scenes in Hong Kong===

====Massage parlour====
* Extra dialogue between Billy, Danny and the Chinese police chief, and the intro to the massage parlour was shortened for the HK version. In the original US edit, the British HK police chief warns them about their actions and to use discretion, and a native HK police officer tells Bill and Danny about the number they traced to a massage parlor. The HK edit dubs the dialog and shortens the scene so that the chief tells them about the massage parlor.
* A few short sequences featuring Billy in action in the massage parlour. (deleted in order to make the scene flow better)
* Billy catches the gun. (slowed down)

====Boat and dock area====
* Billy locates Sally Yeh, fights with two guys in a gym and interviews Sally Yeh. (added)
* Small scene with the African-American on the boat. (deleted)
* A few scenes when Billy chases Sally Yehs uncle on the boat. (deleted)
* The death of Moon Lees father on the boat. (deleted)
* Billys scene with Moon Lee on the boat and buddhist prayer. (deleted)
* Mr. Ko talks to Benny Garrucci. (replacing a different scene)
* Mr. Kos assistant beats Sally Yehs uncle and plots to kill Billy and Danny. (added)
* A deep male voice talking to Billy on the phone is replaced by a female voice demanding that he should leave. The original US edit has Billy saying "Its not your money we want, its your ass." Whereas in the HK edit, he says, "Its not your money we want, its Laura Shapiro."
* Benny Garrucci beats up Moons father and friend. (added)
* A bomb in Sally Yehs bedroom, her uncle warns Billy and Sally, the departure of Sally and her uncle. (added)

====Drug lab and warehouse====
* A small scene of Bill preparing to shoot someone. (deleted)
* The fight scene between Billy Wong and Benny Garrucci moves faster and has more close-ups. (re-edited)
* Benny and Billy fight near a chain-link fence. (added)
* Billy trying to block the Bennys brass knuckles with a large metal can. (added)
* Billy smashes a pot over Bennys head. (slowed down)
* Billy spins a large gear handle to hit Benny in the face. (deleted)
* The fight between Billy and Mr. Kos henchmen has more close-ups cut in. (re-edited)
* Benny Garrucci attacks Billy with a concrete saw. (re-edited)

===Changes to content===
* All cursing has been excised and American slang replaced. The HK edit dubs all of the English dialog without properly translating the cursing, often dubbing it over to say something almost completely different.
* All nudity with women has been excised, with the drug lab re-shot to show fully dressed lab workers.
* The score is slightly different.

==Reception==

The movie had a mixed to negative reception when it was released in the United States in 1985.  

In an interview by Hong Kong film expert Bey Logan with James Glickenhaus held before Chan achieved mainstream success with American audiences, Logan mentioned that many of his fans were disappointed with the movie. An unfazed Glickenhaus responded "Well, you know thats still the most successful Jackie Chan movie internationally and always will be because the American audience, the mainstream audience will never sit still for Jackies style of action".   
 Crime Story". Those later serious films, apart from the Lo Wei serious movies, will help establish Jackie that hes an all round actor and is not restricted to playing comedic roles.   

John J Puccio comments that "Chans charm is in precious little evidence and his martial-arts stunts are limited to a few jumps and spills. Without Chans contributions, the film is nothing more than a clichéd, wannabe thriller". He points out that "The Protector" isnt just badly written; its uniformly awful all the way around. The acting is mechanical; the action is gratuitous; the pacing is humdrum; and the background music is trite and redundant. 

==Box office==
In North America, The Protector was a box office disaster, making only US $981,817. Chans re-edited version grossed HK $13,917,612 in Hong Kong, a respectable sum, but significantly less than any of Chans domestic films at the time.

==Home Media==
*Warner Bros. released their U.S Version on DVD, VHS, & Laserdisc.
 Crime Story on January 15, 2013. 

==See also==
*Jackie Chan filmography

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 