Those Wonderful Movie Cranks
 
{{Infobox film
| name           = Those Wonderful Movie Cranks
| image          = 
| caption        = 
| director       = Jiří Menzel
| producer       = Jan Suster
| writer         = Jirí Menzel Oldrich Vlcek
| starring       = Rudolf Hrusínský
| music          = 
| cinematography = Jaromír Sofr
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}} Czech comedy Best Foreign Language Film at the 52nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Rudolf Hrusínský - Pasparte
* Vladimír Mensík - Slapeta
* Jirí Menzel - Kolenatý
* Vlasta Fabianová - Emílie Kolárová-Mladá
* Blazena Holisová - Evzenie
* Jaromíra Mílová - Pepicka
* Josef Kemr - Benjamín
* Oldrich Vlcek - Berousek
* Josef Somr - Ourada
* Vladimír Huber - Hynek
* Marie Rosulková - Madame
* Hana Buresová - Aloisie

==See also==
* List of submissions to the 52nd Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 