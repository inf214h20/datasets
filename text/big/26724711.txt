Creature of Destruction
{{Infobox film
| name           = Creature of Destruction
| image          =
| image_size     =
| caption        =
| director       = Larry Buchanan
| producer       = Larry Buchanan (producer) Edwin Tobolowsky (associate producer)
| writer         = Tony Huston (writer)
| narrator       =
| starring       = See below
| music          = Ronald Stein
| cinematography = Robert C. Jessup
| editing        =
| distributor    = American International Pictures
| released       =
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Creature of Destruction is a 1967 American made-for-television film directed by Larry Buchanan. It is an uncredited color remake of the 1956 film The She Creature directed by Edward L. Cahn.

== Plot summary ==
The plot concerns an oily hypnotist whose experiments in hypnotic regression take his unwitting female subject to a past life as a prehistoric humanoid form of sea life. He uses the physical manifestation of the prehistoric creature to commit murders, either for revenge or notoriety.

== Cast ==
*Les Tremayne as Dr. John Basso
*Pat Delaney as Doreena
*Aron Kincaid as Capt. Theodore Dell
*Neil Fletcher as Sam Crane
*Annabelle Weenick as Mrs. Crane
*Roger Ready as Lt. Blake
*Ron Scott
*Suzanne Roy as Lynn Crane
*Byron Lord as Investigating psychiatrist / The Creature
*Barnett Shaw as Investigating psychiatrist
*Scotty McKay as Singer

==Production==
The film was one of a series of low-budget color remakes Larry Buchanan directed for AIP (American International Pictures) in Dallas. Goodsell, Greg, "The Weird and Wacky World of Larry Buchanan", Filmfax, No. 38 April/May 1993 p 64  

Aron Kincaid made the film as part of an out-of-court settlement with AIP. He filmed for two weeks to meet his contractual obligations then left to return home. Buchanan was upset as he still had three days of scenes for Kincaid to do. He accompanied him in the cab to the airport, taping the rest of his dialog in the back seat. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p238-239 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 