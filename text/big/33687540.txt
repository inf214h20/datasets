Jo Hum Chahein
 
{{Infobox film
| name           = Jo Hum Chahein
| image          = Jo Hum Chahein Theatrical Release Poster 02.jpg
| alt            =  
| caption        = Theatrical Release Poster
| director       = Pawan Gill
| producer       = Ali Alvi
| writer         =  Simran Mundi Alyy Khan Achint Kaur Yuri Suri Rithvik Dhanjani Sachin Gupta Kumaar
| cinematography = Manush Nandan
| editing        = Bakul Matiyani
| studio         = Alvi Production
| distributor    = 
| released       =  
| runtime        = 134 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 2011 Bollywood|Hindi Simran Mundi, Alyy Khan and Achint Kaur.  It was a debut film for both the lead actors and the director. The film was released on 16 December 2011  to a mixed response. The official DVD of the film was released on 18 January 2012. 

== Plot == Simran Mundi) and the two have a war of words; for the first time in his life his advances have been rejected by a girl. Rohan, Abhay and 28 new stockbrokers start work at Bombay Bulls but after three months only the 10 best will survive and get permanent placements. Rohan pitches clients but doesnt climb the charts; he remains near the bottom, so he uses the help of Vikram Khurrana (Alyy Khan), the firms best stockbroker, to climb the charts faster. Vikram introduces Rohan to Amrita Singhania (Achint Kaur), a wealthy society lady who is instantly taken by Rohans boyish charm; the same night he also bumps into Neha and this time he doesnt budge until she agrees to go on a date with him. The next day, Amrita invites Rohan to a beach party where she tries to seduce him with her sultry body.

Rohan takes Neha to a spiritual date and courts her further until she completely falls in love with him. At the same time he pursues Amrita, who agrees to let him make trades for her and upon making her a large profit she invites him to celebrate in Goa. The same weekend is Rohans 25th birthday and Neha comes to Goa to surprise him, but finds him in bed with Amrita. Rohan cant remember what happened, he was too drunk; heartbroken Neha storms away. In Mumbai, Rohan makes two more attempts to apologize but Neha wont give him another chance. The distance between them grows. He also has a showdown with Dalip and Abhay. Rohan gets more entwined into Amrita and Vikrams worlds but misses and longs for Neha. At Nehas best friend Shivangis (Mansi Multani) engagement to Abhay, Rohan publicly begs Neha for forgiveness; she tells him shes pregnant but doesnt need him in her life. Rohan discovers Amrita and Vikram were just using him for their selfish plans; he has a showdown with Amrita and Vikram and gets thrown out of the firm. Rohan apologizes to his father for all his wrongdoings, then goes to Nehas house in Delhi to ask to be a part of their childs life only to discover that shes aborted their child. She wasnt ready for a child and was only having it to hurt him by keeping their child away from him. Rohan is shattered; Neha apologizes and asks him back in her life. Heartbroken, they embrace and accept each other for their flaws.

== Cast ==
* Sunny Gill as Rohan Bhatia Simran Mundi as Neha Kapoor
* Alyy Khan as Vikram Khuranna
* Achint Kaur as Amrita Singhania
* Yuri Suri as Dalip Bhatia
* Samar Virmani as Abhay
* Mansi Multani as Shivangi
* Rithvik Dhanjani as Aakash

== Production == Simran Mundi, a debut for Producer Aman Gill, who was an Executive Producer on Sanjay Leela Bhansalis Black (2005 film)|Black  and was the Head of Domestic Distribution at Viacom 18 Motion Pictures where he distributed blockbuster films like Ghajini (2008 film)|Ghajini, Singh Is Kinng, Golmaal Returns, Welcome (2007 film)|Welcome, Jab We Met,  a debut for Director Pawan Gill who was an assistant director at Yash Raj Films,  the Hindi Language debut for DOP Manush Nandan, and debut film as an Editor for Bakul Matiyani. The film had Production Design by Amrita Mahal & Sabrina Singh, Music and Background score by Sachin Gupta, Lyrics by Kumaar, Sound Design by Nimesh Chheda, Costume by Priyanjali Lahiri, Choreography by Adil Shaikh, Dialogues by Rashmi Kulkarni and Casting by Amita Sehgal. The film was shot from May to October 2011 in Pune, Goa, Mumbai and Ladakh. 

== Reception ==
The film received an average response from Top Critics, Taran Adarsh of Bollywood Hungama gave the film 2.5 stars,  Rajeev Masand of CNN-IBN gave the film 2 stars,  Nikhat Kazmi and Gaurav Malani of The Times of India also gave it 2 stars. 

== Soundtrack == Sachin Gupta and Lyrics are by Kumaar. The album has five tracks, Aaj Bhi Party is a young Party Song, Ishq Hothon Se is a quintessential Love Ballad, Peepni is Night Club Song, Abhi Abhi Dil Toota Hai is a Sad Song and One More One More is a fun filled Punjabi Engagement Song.  The Music was released by T-Series and received favorable reviews by most critics. Glamsham gave it 3.5 stars,  and Bollywood Hungama gave it 3 stars. 

=== Track listing ===
{{track listing
| extra_column = Singer(s)
| music_credits = no
| title1 =  
| extra1 = Suraj Jagan
| music1 = Sachin Gupta
| lyrics1 = Kumaar
| length1 = 3:45
| title2 =   KK & Shreya Ghoshal
| music2 = Sachin Gupta
| lyrics2 = Kumaar
| length2 = 5:05
| title3 =  
| extra3 = Jaspreet Singh & Monali Thakur
| music3 = Sachin Gupta
| lyrics3 = Kumaar
| length3 = 3:40
| title4 =   KK & Shashaa Tirupati
| music4 = Sachin Gupta
| lyrics4 = Kumaar
| length4 = 4:40
| title5 =  
| extra5 = Neeraj Shridhar & Sunidhi Chauhan
| music5 = Sachin Gupta
| lyrics5 = Kumaar
| length5 = 3:50
}}

== References ==
 

== External links ==
*   at Bollywood Hungama
*  
*   at Facebook

 
 
 