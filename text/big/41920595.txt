A Taste of Money
{{Infobox film
| name           = A Taste of Money 
| image          = "A_Taste_of_Money"(1960).jpg
| image_size     =
| caption        = UK theatrical poster
| director       = Max Varnel
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         =  Mark Grantham	(original story)
| narrator       = Pete Murray
| music          =  James Wilson   (as Jimmy Wilson)
| editing        = Desmond Saunders
| studio         = Danziger Productions
| distributor    = United Artists Corporation
| released       = 1960
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Pete Murray. 

==Plot==
An elderly spinster who works as a cashier for an insurance company, plots to rob her employers, by concocting the perfect crime. 

==Cast==
*Morrissey -   Dick Emery
*Miss Brill - 	Jean Cadell Pete Murray
*White - 	Ralph Michael
*Joe - 	Donald Eccles
*Tyler - 	C. Denier Warren John Bennett
*Simpson - 	Robert Raglan Mark Singleton
*Ruth -	        Christina Gregg
*Barman - 	Derek Sydney

==Critical reception==
TV Guide called the film "an uninspired comedy," and noted, "Cadell, at 76 years of age, turns in a charming performance, but that alone cannot save the picture from mediocrity";  while Sky Movies wrote, "minor but amusing British comedy thats all the better for its coat of Technicolor. Character actress Jean Cadell is a joy in a rare leading role, and Dick Emery makes a perfect not-quite-funny gangster - his best work for the cinema. Christina Gregg is a pretty heroine in a film, which breaks little fresh ground, yet remains constantly entertaining thanks to good performances."  

==External links== BFI
* 

==References==
 

 
 
 
 