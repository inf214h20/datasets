American Boy: A Profile of Steven Prince
 
{{Infobox film
| name           = American Boy: A Profile of Steven Prince
| image          = American boy film poster.jpg
| caption        = Promotional poster (with Italianamerican)
| director       = Martin Scorsese
| producer       = Bert Lovitt
| writer         = Julia Cameron (treatment) Mardik Martin (treatment)
| starring       = Steven Prince, Martin Scorsese, George Memmoli
| music          = Michael Chapman
| editing        = Amy Jones Bert Lovitt
| distributor    = New Empire Films
| released       =  
| runtime        = 55 min.
| country        = United States English
| budget         = $155,000   
}}
 documentary directed Pulp Fiction. Prince also tells a story about his days working at a gas station, and having to shoot a man he caught stealing tires, after the man pulled out a knife and tried to attack him. This story was retold in the Richard Linklater film Waking Life.

The Neil Young song "Time Fades Away" is featured during the films closing credits.

A sequel, American Prince, was released in 2009 and was directed by Tommy Pallotta.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 