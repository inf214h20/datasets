Ankahee (2006 film)
 
{{Infobox film
| name = Ankahee
| image = Ankaheeposter.jpg
| caption = Theatrical poster
| director = Vikram Bhatt
| producer = Pritish Nandy Rangita Pritish Nandy
| writer = Screenplay: Yash-Vinay Story: Vikram Bhatt
| narrator =
| starring = Aftab Shivdasani Ameesha Patel Esha Deol
| music = Pritam
| cinematography = Pravin Bhatt
| editing = Diwakar Bhonsle Virendra Gharse
| distributor = Pritish Nandy Communications
| released = 19 May 2006
| runtime = 151 mins
| country = India
| language = Hindi
| budget =
| gross =
| preceded by =
| followed by =
}}
Ankahee ( : انکہی, English: Untold) is an Indian film directed by Vikram Bhatt and starring Aftab Shivdasani, Ameesha Patel and Esha Deol. The film was originally titled Aakhir. This movie was based upon the life of former Miss Universe Sushmita Sen, who was publicly in a relationship with Indian film director Vikram Bhatt.  The director later denied the facts, but the movie evolves around the same story.

==Plot==
Ankahee begins with Sheena (Hrishita Bhatt) arriving at home, when her mother Nandita (Ameesha Patel) tells her that her father Shekhar (Aftab Shivdasani) has sent her a letter and wants to meet her as he’s dying from illness. Sheena refuses to meet her father, as he abandoned her & her mother for sixteen years when Sheena was just six years of age. As her mother persuades her to meet him, she finally agrees to pay her father a visit and she sees him in a bad situation under careful nursing. Unable to express his feelings, Shekhar lends his daughter a diary in which he wrote about all the incidents that led to his break-off. Henceforth, the story of Dr. Shekhar unspools.

Sixteen years back, the world of Shekhar and Nandita with their little daughter was nearer to perfection so much that no one could suspect that in his complacent heart was lying the seed of betrayal. Shekhars life takes a U-turn as he meets an actress and a former beauty queen, Kavya Krishna (Esha Deol), at a hospital with her wrist being slit and slowly the two fall for each other and continue to meet even after her wrist is cured. Shekhars friend, Dr. Kunal Mehta,a Psychiatrist (Amin Hajee), then confronts him to stay away from this woman as he feels that their constant interaction will cause damage to Shekhars marital life.

A week later, Shekhar goes to Goa for a press conference and Kavya follows him there and tells him she had no choice other than this to break her feeling of loneliness. Something brews between the two as Shekhar goes back to his home in Mumbai. Soon enough, Nandita notices that Shekhar is uncomfortable with the surroundings and as she tries to help him he starts yelling at her about why she is giving him so much importance in her life. Further problems take place as Shekhar misses his daughters annual school function, and then he decides to confront Kavya and asks her to end this relationship. But Kavya leaves no stone unturned, she goes and visits him at his office at the hospital and in tears asks him to forget all what he said yesterday and explains to him how she can feel happy only with him. Shekhar then hugs her and explains to her how much he loves her back.

Back at home, Nandita tells Shekhar that a journalist from Mid-Day newspaper had called and wanted to speak to him. Ignoring this call, the next day Shekhar discovers that his relationship with Kavya is printed in the newspaper and has become the talk of the town. Kunals wife Shilpa (Ashwini Kalsekar) then spots Nandita at a restaurant and shows her the newspaper, leaving her in complete shock. Later, Shekhar arrives at home as he and Nandita confront each other about the affair as the confrontation ends with Nandita giving him a slap on the face. The next day, Shekhar finds Nandita sitting at his office as she apologizes to him for what happened the previous night and tells him about how she wants both of them to fix whats been broken and how she will do anything just to save this marriage from breaking. She later visits Kavyas house to confront her and asks her to let her family live in peace. As Shekhar is informed about Nanditas visit to Kavya, he loses his temper and decides to leave the house telling Nandita how he regrets his marriage.

The next day, Sheenas parents are called to her school to pick her up as shes sick while the principal tells them how hes upset with the recent change in Sheenas behavior at school. Sheena then tells her father that her classmates have been picking on her after his affair with the star, but he denies it when she asks him whether it’s true or not. Later that day, Kavyas manager (Deepak Qazir) asks her to stop making her personal life public as her reputation is being ruined. Kavya then gets angry, blaming Nandita for whats happened and she fires her manager thinking he is working against her. She then asks Shekhar to make a choice between her and Nandita as wouldnt mind risking her own life to keep her man exclusively for herself. Torn between the two women, Shekhar decides to split from his wife and his daughter and chooses Kavya over his family not knowing that he would regret this choice forever. Shekhar then gets a call from Nandita, telling him that he should pay his daughter a visit as shes sick. Kavya refuses to let him go and tells him this is Nanditas plan to take him back. Shekhar gets furious and yells at her saying she is mad, lonely, and mentally ill. Kavya is hurt, so she grabs a gun, and shoots herself. Shekhar goes back to Nandita asking her for forgiveness, but she turns him down. After reading all this, Sheena goes back to her father and hugs him as he apologizes to her saying that he feels hurt because he has not been a good father. She accepts his apology, but the next day she wakes up to find out that her father has died. Sheena cries to her mother asking her to forgive him too and she finally agrees to forgive him.

==Cast==
* Aftab Shivdasani as Shekhar Saxena
* Ameesha Patel as Nandita Saxena
* Esha Deol as Kavya Krishna
* Hrishita Bhatt as Sheena Saxena (Special appearance)
* Amin Hajee as Dr. Kunal Mehta
* Vikas Bhalla as Kavyas ex-boyfriend
* Ashwini Kalsekar as Shilpa Mehta
* Deepak Qazir as Kavyas manager
* Vikram Bhatt (Uncredited)

==Soundtrack==
The soundtrack for Ankahee has music by Pritam with lyrics from Amitabh Varma, Sameer and Subrat Sinha.

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Aa Paas Aa
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          =  Shreya Ghoshal
| length1         = 06:33
| title2          = Ankahee
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Kunal Ganjawala
| length2         = 04:06
| title3          = Ankahee - 1
| note3           =
| writer3         =
| lyrics3         =
| music3          = Shaan
| length3         = 04:56
| title4          = Ek Pal Ke Liye
| note4           =
| writer4         =
| lyrics4         =
| music4          = KK
| length4         = 05:55
| title5          = Ek Pal
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          =  Sonu Nigam
| length5         = 05:43
| title6          = Ek Pal 2
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          =  Shreya Ghoshal
| length6         =  05:39
| title7          = Lamha
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          =  Babul Supriyo
| length7         =  05:02
| title8          = Tum Se
| note8           =
| writer8         =
| lyrics8         =
| music8          =
| extra8          =  Kunal Ganjawala
| length8         =  05:08
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 