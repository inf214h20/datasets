17 Again (film)
 
 
{{Infobox film
| name           = 17 Again
| image          = 17again.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Burr Steers
| producer       = Adam Shankman   Jennifer Gibgot
| screenplay     = Jason Filardi Thomas Lennon Michelle Trachtenberg Matthew Perry Sterling Knight 
| music          = Rolfe Kent
| cinematography = Tim Suhrstedt
| editing        = Padraic McKinley
| studio         = Offspring Entertainment
| distributor    = New Line Cinema
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $40 million   
| gross = $136.3 million   
}} Thomas Lennon and Michelle Trachtenberg in supporting roles. The film was released in the United States on April 17, 2009.

==Plot==
In 1989, seventeen-year-old Mike ODonnell (Zac Efron) learns from his girlfriend Scarlet Porter (Allison Miller) that she is pregnant during the start of his high school championship basketball game. Moments after the game begins, he leaves the game and goes after Scarlet, abandoning his hopes of going to college and becoming a professional basketball player.  

In 2009, Mike ( ) and Alex (Sterling Knight) want nothing to do with him. Later, while visiting his high school to reminisce, an encounter with a mysterious janitor (Brian Doyle-Murray) transforms Mike back into his seventeen-year-old self.

Mike then enrolls in high school posing as Mark Gold, Neds son, and plans to go to college with a basketball scholarship. As he befriends his bullied son and discovers that his daughter has a boyfriend who does not respect her, Mike comes to believe that his mission is to help them. He meets Maggies boyfriend and Alexs bully, Stan (Hunter Parrish), the captain of the basketball team, and embarrasses him in front of the whole school after Stan insults Alex. Later, in Sex Education class while the teacher is handing out condoms to the students in a basket, Stan turns to Mike and refuses to give him any, saying that he does not need them, causing quiet laughter among the class. Mike then makes a speech about love and sex in front of the whole class for Maggies benefit, causing all of the girls to give back their condoms.  Stan then takes the condoms and takes a handful claiming that he is stocked up for the weekend and kisses Maggie passionately. Because of this, Mike loses his temper and starts a fight with Stan on the floor, which is being taped by other students and eventually goes viral within a matter of minutes. Mike loses the fight and Ned is called up to the school, where he instantly becomes attracted to the principal.

Mike comforts Maggie when Stan dumps her after she refuses to sleep with him. With Mikes help, Alex overcomes Stans bullying to obtain a place with Mike on the basketball team and the girlfriend he desires. 

Through their children, Mike spends time with Scarlet, who is attracted to his remarkable resemblance to her husband in high school. Mike has difficulty resisting his desire for her despite the relationships clear inappropriateness. At the same time, he must fend off Maggies sexual advances.

Mike soon realizes that Scarlet is the best thing that ever happened to him and finally realizes that his own selfishness has driven his family away. He tries to re-unite with her, briefly forgetting his young form and kisses her during a party, in front of Maggie and other girls, and unsuccessfully explains to her that he is actually her husband. On the day of the court hearing to finalize Scarlet and Mikes divorce, Mike makes one last attempt to win her back (as Mark) by reading a supposed letter from Mike. He states that although he couldnt set things right in the beginning of his life, it doesnt extinguish the fact that he still loves her. After he exits, Scarlet notices that the "letter" is actually the directions to the courtroom and she begins to grow curious. As a result, she postpones the divorce by a month. During a high school basketball game, Mike reveals himself to Scarlet. As Scarlet once again runs away down the hall, Mike decides to chase her down once more, but not before handing the ball off to his son. Mike is then transformed back into his thirty-seven-year-old self, and reunites with Scarlet.

The film ends with Mike receiving the gift of a whistle from Ned in celebration of his new job as basketball coach.

==Cast==
* Matthew Perry as Mike ODonnell (37)   
**Zac Efron as Mike ODonnell (17) / Mark Gold
* Leslie Mann as Scarlet ODonnell (née Porter) (37)
** Allison Miller as Scarlet Porter (17)
* Michelle Trachtenberg as Margaret Sarah "Maggie" ODonnell, Mikes daughter
* Sterling Knight as Alex ODonnell, Mikes son Thomas Lennon as Ned Gold (37)
** Tyler Steelman as Ned Gold (17)
* Hunter Parrish as Stan 
* Katerina Graham as Jamie
* Melora Hardin as Principal Jane Masterson
* Jim Gaffigan as Coach Murphy
* Drew Sidora as Cameron
* Tiya Sircar as Samantha
* Vanessa Lee Chester as Karla
* Nicole Sullivan as Naomi Adam Gregory as Dom
* Brian Doyle-Murray as The Janitor
* Margaret Cho as Health Teacher
* Josie Loren as Nicole
* Melissa Ordway as Lauren
* Justin Padoran as Jordan

==Reception==
===Critical response===
The film received mixed reviews. Review aggregation website Rotten Tomatoes gives the film a rating of 55%, based on 142 reviews, with an average rating of 5.4/10. The sites consensus reads, "Though it uses a well-worn formula, 17 Again has just enough Zac Efron charm to result in a harmless, pleasurable teen comedy."  On Metacritic, the film has a score of 48 out of 100, based on 27 critics, indicating "mixed or average reviews".  

Roger Ebert gave the film 3 stars out of 4. 

 

===Box office===
The film was predicted to take in around $20 million in its opening weekend.  Opening in 3,255 theaters in the United States and Canada, the film grossed $23,722,310 ranking #1 at the box office, with 70% of the audience consisting of young females.  By the end of its run, 17 Again grossed $64,167,069 in North America and $72,100,407 internationally, totaling $136,267,476 worldwide. 

==Soundtrack==
{{Infobox album  
| Name     = 17 Again: Original Motion Picture Soundtrack
| Type     = Soundtrack
| Artist   = Various Artists
| Cover    = 
| Border   = yes
| Released = April 21, 2009
| Genre    = Soundtrack
| Label    = New Line Records
}}

17 Again: Original Motion Picture Soundtrack was released on April 21, 2009 by New Line Records. {{cite web
| url=http://www.amazon.com/17-Again-Original-Picture-Soundtrack/dp/B001T46UGE
| title=17 Again: Original Motion Picture Soundtrack
| work=Amazon.com
| accessdate=2009-04-22
| archiveurl= http://web.archive.org/web/20090420155133/http://www.amazon.com/17-Again-Original-Picture-Soundtrack/dp/B001T46UGE| archivedate= 20 April 2009  }} 

===Track listing===
# "On My Own" by Vincent and The Villains
# "Cant Say No" by The Helio Sequence
# "L.E.S. Artistes" by Santigold
# "Naïve (song)|Naïve" by The Kooks
# "This Is Love" by Toby Lightman
# "You Really Wake Up the Love in Me" by The Duke Spirit
# "The Greatest" by Cat Power
# "Rich Girls" by The Virgins This Is for Real" by Motion City Soundtrack
# "Drop" by Ying Yang Twins
# "Cherish (Kool & the Gang song)|Cherish" by Kool & The Gang Bust a Move" by Young MC Danger Zone" by Kenny Loggins

===Additional music credits===
* "Kid" by The Pretenders
* "Nookie (song)|Nookie" by Limp Bizkit The Underdog" Spoon
* "High School Never Ends" by Bowling for Soup  
* "Push It Fergasonic (DJ Axel Mashup)" by Fergie (singer)|Fergie, Salt-n-Pepa, JJ Fad

The orchestral score was written by Rolfe Kent and orchestrated by Tony Blondal. It was recorded at Skywalker Sound

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 