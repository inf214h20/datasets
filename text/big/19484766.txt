En fuldendt gentleman
 
{{Infobox film
| name           = En fuldendt gentleman
| image          = En fuldendt gentleman.jpg
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = Henning Karmark
| writer         = Flemming Geill Osvald Helmuth Carl Viggo Meincke
| starring       = Osvald Helmuth
| music          = 
| cinematography = Eskild Jensen
| editing        = Marie Ejlersen
| distributor    = 108 minutes
| released       =  
| runtime        = 
| country        = Denmark 
| language       = Danish
| budget         = 
}}

En fuldendt gentleman is a 1937 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast==
* Osvald Helmuth - Mortensen
* Lau Lauritzen, Jr. - Baron Henrik Falkenstjerne
* Oda Pasborg - Bodil von Hauch
* Karen Jønsson - Karen von Hauch Albrecht Schmidt - Grev Urne
* Ellen Margrethe Stein - Grevinde Urne
* Eigil Reimers - Grev Tage Urne
* Else Jarlbak - Ulla Platen
* Thorkil Lauritzen - Kammerjunker Torben von Gothenburg
* Erika Voigt - Apoteker Hornfleth
* Knud Almar - Inspektør Boye
* Alex Zander - Chauffør
* Carola Merrild - Stuepige Henry Nielsen - Frisørkunde
* Betty Helsengreen

==External links==
* 

 
 

 
 
 
 
 
 
 


 