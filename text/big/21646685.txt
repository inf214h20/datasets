Twisted (1986 film)
{{Infobox Film
| name           = Twisted
| image          = 
| image_size     = 
| caption        = 
| director       = Adam Holender
| producer       = Hemdale Film  Bruce Graham  Glenn Kershaw
| writer         = Christopher Coppola  Nick Vallelonga
| narrator       = 
| starring       = Christian Slater  Lois Smith  Tandy Cronyn
| music          = Michael Bacon
| cinematography = Alexander Gruszynski
| editing        = Lillian Benson  Peter Hammer	 	
| distributor    = Hemdale Film
| released       = 1986
| runtime        = 87 min
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 horror and psychological thriller starring Christian Slater, Lois Smith, and Tandy Cronyn.

== Plot ==
One evening, the Collins family discovers their maid, Mrs. Murdock, dead at the end of their steps; her neck is broken. Evidently, she had an accident; now they need a new babysitter for an upcoming party. The sensible Helen meets little Susan Collins at the discount market and likes her, so she offers to do the job. She does not know Susans teenage brother Mark: technically skilled and good in school, but restive and cunning. Mark also listens regularly to German marching music from the Third Reich. As soon as the parents have left, he psychologically terrorizes Helen and his sister with electronic tricks. Williams (Karl Taylor), a school jock whom Mark burned earlier in science class, is out for revenge; Mark murders him with a fencing sword. 

Ultimately, Mark himself is killed when Helen knocks him onto a spiked German helmet. Marks parents come home to find the house in shambles; they blame Helen and have her arrested, unaware that Mark lies dead upstairs. Secretly, Susan dons her late brothers glasses and proceeds to listen to his Nazi music and the cycle begins anew.

== Cast ==
* Christian Slater as Mark Collins
* Lois Smith as Helen Giles
*  as Evelyn Collins
* Dan Ziskie as Phillip Collins
*   as Susan Collins
* Dina Merrill as Nell Kempler

== External links ==
*  
*  

 
 
 
 
 
 


 
 