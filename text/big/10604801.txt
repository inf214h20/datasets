Naval Commandos
 
 
{{Infobox film
| name           = Hai jun tu ji dui (Naval Commandos)
| image          =
| image size     =
| caption        =
| director       = Chang Cheh   Pao Hsueh-Li   Wu Ma   Liu Wei-Ping
| producer       =
| writer         =
| narrator       =
| starring       = Liu Yung   David Chiang   Ti Lung   Fu Sheng   Philip Kwok   Lu Feng   Chiang Sheng
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        =   Hong Kong   Taiwan Cantonese
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} War of Resistance/WWII (1937-45).

==Plot==

The Chinese Navy led by its captain (Ti Lung) is attempting to defend its coastline from a Japanese attack, they send their commando unit led by Szu Shih into the mainland to enlist help to thwart the Japanese before they attack. Japanese officers have been hanging out in a place owned by David Chiang and his hot-headed body guard Fu Sheng but they are secretly opposed to the Japanese. The commandos enlist the help of Chiang and Sheng to kill the Japanese officers led by Shan Mao and destroy their attacking ships.

==Cast==
* Liu Yung - Chinese Navy Admiral Vice-Admiral An Chi Pang
* Ti Lung – Chinese Navy Captain Liang Kuan Chin
* David Chiang – Shanghai Boss Song San
* Fu Sheng – Shanghai resistance fighter Shiao Liu
* Chi Kuan-Chun - Chinese Navy Captain Hu Ching Yuan 
* Shih Szu – Shanghai resistance movement leader Cui Hsia
* Kuo Chui – Naval Commando Sgt. Shu Kuan
* Chiang Sheng – Naval Commando Sgt. Chiang Ping Kuang
* Lu Feng - Naval Commando Sgt. Shu Shiang-Lin
* Tang Yen-Tsan - Naval Commando Sgt. Shao Kang Fa
* Shan Mao – Japanese Naval Captain Hiroda

==External links==
*  


 
 
 
 
 
 


 
 