Journey to the Seventh Planet
{{Infobox Film
| name           = Journey to the Seventh Planet
| image          = JourneytotheSeventhPlanet.jpg
| caption        = Theatrical release poster
| director       = Sidney W. Pink
| producer       =
| writer         = Ib Melchior Sidney W. Pink
| starring       = John Agar Greta Thyssen Carl Ottosen Ove Sprogøe Ann Smyrner Mimi Heinrich
| music          = Paul Dunlap
| cinematography = Jack Greenhalgh
| editing        = Philip Cahn
| studio         = Cinemagic Inc.
| distributor    = American International Pictures (USA)
| released       = March 1962
| runtime        = 77 minutes
| country        = Denmark United States
| language       = Danish English
| budget         = $75,000   accessed 15 April 2014 
}}
Journey to the Seventh Planet is a 1962 science fiction film. It was directed by Sid Pink, written by Pink and Ib Melchior,  and shot in Denmark with a budget of only US$75,000. The seventh planet of the title is Uranus, and a crew is being dispatched there by the United Nations on a mission of space exploration. The films ideas of astronauts exploring outer space only to confront their inner mindscapes and memories precede the similar-themed 1972 film Solaris (1972 film)|Solaris by a full decade (although the novel Solaris precedes this film by a year).  The film is also reminiscent of Ray Bradburys 1948 short story Mars is Heaven! that appeared in the 1950 book The Martian Chronicles. 

==Plot==
During their journey to Uranus, an alien presence briefly assumes control of the crews minds. They awaken safely but notice that a long - and unexplained - period of time has passed by. Upon landing, the crew finds a forested land oddly like Earths, rather than the cold, bleak world they were expecting. This forest is surrounded by a mysterious barrier. One of the crew pushes his arm through the barrier, only to have it frozen.

New features and forms begin to appear each time they are imagined by the crew. A familiar-looking village appears, complete with attractive women the various male crew members have known in the past. Soon, they must face a series of strange beasts including a giant bi-pedal cyclopean rodent and a lobster-like insect. The crew realizes that they have been the victims of mind control by a gigantic one-eyed brain living in a cave. There, they are confronted by the "Being," whose mysterious brain cuts to the inner thoughts of the explorers and causes their thoughts to appear as seemingly real. The brain-Being plans to possess the astronauts bodies and have them take it with them back to Earth where it will implement a plan for global domination. The crew gradually come to realize their peril and start to fight back against the presence, even eliciting aid from the sympathetic women. They must then confront the Being in its lair while it assaults each with monsters spawned from their fears.

==Cast==
* John Agar as Capt. Don Graham
* Carl Ottosen as Eric
* Peter Monch as Karl
* Ove Sprogøe as Barry OSullivan
* Louis Miehe-Renard as Svend
* Ann Smyrner as Ingrid
* Greta Thyssen as Greta
* Ulla Moritz as Lise
* Mimi Heinrich as Ursula
* Annie Birgit Garde as Ellen
* Bente Juel as Colleen

==References==
 

===Additional reading===
* Sidney W. Pink: So You Want to Make Movies (Pineapple Press, 1989)
* Nicolas Barbano: Twice Told Tails - The Two Versions of Reptilicus, in Video Watchdog #96 (2003)

==External links==
*  
*   review at Badmovies.org
*   film trailer at YouTube

 
 
 
 
 
 
 
 