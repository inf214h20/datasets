Tycoon (2002 film)
{{Infobox Film
| name           = Tycoon (lit. Oligarch)
| image_size     = 
| image = 
| caption =      English Poster Pavel Loungine (Lungin)
| producer       = Erich Weissberg Pavel Loungine   Yuli Dubov
| narrator       = 
| starring       = Vladimir Mashkov Mariya Mironova
| music          = Leonid Desyatnikov
| cinematography = 
| editing        = Sophie Brunet
| distributor    = 
| released       = 
| runtime        = 123 min.
| country        = Russia France Germany Russian 
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2002 Russian Pavel Loungine (or Lungin).

==Plot==
During the Mikhail Gorbachev years, Platon Makovsky and four buddies of his are university students who jump on the private capitalism movement. Fast-forward 20 years, Platon finds himself the richest man in Russia. But as such, he and his friends are drawn more and more into relations with suspect organizations. They also have to face ever more brutal attempts to subjugate them by the Kremlin. Makovsty attempts to compete with this ever-present political power, by becoming as "creating a Kremlin" himself.

==Background== Boris Berezovsky and his partners. Dubov was Berezovskys partner himself, and a president of his LogoVAZ company. The novel is claimed to be historically precise in many aspects. The names of the characters were changed from their real life counterparts, though keeping resemblance (e.g. Boris Berezovsky, who changed his name to Platon Elenin in exile in 2004, to Platon Makovsky, Badri Patarkatsishvili to Lari Teishvili, etc.)

In June 2009, Dubov and Berezovsky were convicted in absentia (at that time, they were both living in exile in England) in a Russian court for the events that served as the basis for the book to 9 and 13 years of imprisonment respectively. The book served as one of the pieces of evidence against them.   

==External links==
*  

==References==
 

 
 
 
 


 