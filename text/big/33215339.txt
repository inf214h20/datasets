Bandhan (1940 film)
{{Infobox film
| name           = Bandhan
| image          = Bandhan (1940).jpg
| image_size     =
| caption        =
| director       = N.R. Acharya
| producer       = Sashadhar Mukherjee
| writer         = J.S. Casshyap, Amiya Chakrabarty
| narrator       =
| starring       = Saraswati Devi Kavi Pradeep (lyrics)
| cinematography =
| editing        =
| studio         = Bombay Talkies
| released       =  
| runtime        =
| country        = British Raj Hindi
| budget         =
| gross          =
| website        =
}}
 1940 Indian Bollywood film directed by N.R. Acharya. It stars Leela Chitnis, Ashok Kumar and Suresh. It was the second highest grossing Indian film of 1940.  It was produced by Bombay Talkies.       

==Cast==
*Leela Chitnis as Beena
*Ashok Kumar as Nirmal
*Suresh 		
*P.F. Pithawala 		
*V.H. Desai as Bholanath
*Shah Nawaz as Suresh
*Purnima Desai as Gauri
*Jagannath 	
*Arun Kumar

==References==
 

==External links==
*  

 

 
 
 