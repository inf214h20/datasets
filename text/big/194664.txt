The Flintstones (film)
{{Infobox film
| name           = The Flintstones
| image          = Flintstones_ver2.jpg
| caption        = Theatrical poster for The Flintstones, designed by Drew Struzan
| director       = Brian Levant
| producer       = Bruce Cohen
| writer         = {{Plainlist|
* Tom S. Parker
* Jim Jennewein
* Steven E. de Souza }}
| based on       =  
| starring       = {{Plainlist|
* John Goodman
* Rick Moranis
* Elizabeth Perkins
* Rosie ODonnell
* Kyle MacLachlan
* Halle Berry
* Elizabeth Taylor }} David Newman
| cinematography = Dean Cundey
| editing        = Kent Beyda
| studio         = {{Plainlist|
* Amblin Entertainment
* Hanna-Barbera|Hanna-Barbera Productions }} Universal Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $46 million
| gross          = $341.6 million
}}
 buddy comedy a famous actress) and Elizabeth Taylor (in her final theatrical film appearance), as Pearl Slaghoople, Wilmas mother. The B-52s (as The BC-52s in the film) performed their version of the cartoons theme song. 

The movie was shot in California at an estimated budget of $46,000,000 and then was released on May 27, 1994, making it a box-office success, though it was panned by and received negative reviews by the consensus of film critics. Observers criticized the storyline and tone, which they deemed too adult and mature for family audiences, but praised its visual effects, costume design, art direction and John Goodmans performance as Fred Flintstone.

==Plot==
 
 Betty (Rosie Wilma (Elizabeth Perkins), remains supportive of Freds decision, however.

Cliff holds an aptitude test, where the worker with the highest mark will become the new Vice President of the company. Barney gets the highest score, but remembers his promise and switches his paper with Fred, whom he knows will fail miserably. Fred receives the promotion, but his first order is to fire Barney, since Barney now effectively has the lowest score. Fred reluctantly complies, but does his best to help Barney support his family, even inviting the Rubbles to live with them so that they can rent out their home. However, the perks of Freds new job put a strain on his friendships, as Wilma catches him in a flirtatious moment with Miss Stone, and the Rubbles are put off by Fred and Wilmas increasingly snobbish behavior. Cliff eventually tricks Fred into firing all the workers, despite advice from his office dictabird (Harvey Korman). Things come to a head at a restaurant where Barney, working as a waiter, sees the workers rioting on the news, and confronts Fred. In the ensuing argument, Barney finally admits that he switched tests with Fred, and the Rubbles move out, despite having nowhere to live. Disgusted with Freds behavior, Wilma leaves Fred for her mothers house, taking their daughter Pebbles with her.

Fred goes to the quarry and discovers his mistake and Cliffs plot, but also learns that Cliff has engineered the scheme to make it look as if Fred embezzled the money, and calls the police. A manhunt for Fred ensues, by the police and the angry workers. Wilma and Betty see this on the news, and break into Slate and Co. to get the dictabird, the only witness who can clear Fred, unaware that Cliff saw them from his office window. Fred tries to infiltrate a cave where the workers are seeking refuge, but they see through his disguise and try to lynch him. Barney shows up, now working as a snow cone vendor, and is almost hanged as well after he admits his part. Fred and Barney make amends, but before they can be hanged, Wilma and Betty turn up with the dictabird, who tells the workers that Cliff was the one who fired them and convinces them to let Fred and Barney go.

However, Cliff breaks into Freds house, vandalizes it, kidnaps Pebbles and Bamm-Bamm, ties up Pearl and Dino, and leaves behind a ransom note demanding the Dictabird in exchange for the childrens safe return. Fred and Barney confront Cliff at the quarry, where Cliff has tied Pebbles and Bamm-Bamm to a huge machine. Though they hand him the Dictabird, Cliff sets the machine off to stall them. Barney rescues the children while Fred destroys the machine. The dictabird escapes from Cliff and lures him back to the quarry, where Miss Stone knocks him out, having had a change of heart after learning that Cliff planned to betray her. The police arrive with Wilma and Betty and Cliff attempts to flee, but is stopped by a falling substance which engulfs and traps him after turning solid.

With the dictabirds help, Fred is cleared of all charges, while Miss Stone is arrested as Cliffs accomplice, though Fred is confident she will be granted leniency for helping them stop Cliff. Mr. Slate (Dann Florek), impressed with the substance that Fred inadvertently created by destroying the machine, dubs the substance "concrete" and makes plans to produce it with Fred as President of its division, thus ending the Stone Age, but Fred declines. Barney protests that this is a great position for Fred and not like the problems of before, but Fred explains to Mr. Slate that wealth and status changed him for the worse, and simply asks that all the workers be rehired and given the job benefits he originally set out to achieve, which is granted. As the Flintstones and Rubbles have finally made amends, Fred and Barney get into a humorous quarrel when Fred once again asks Barney for a small amount of money for breakfast. As they argue and leave, Mr. Slate remarks to himself "There goes the best executive I ever had." A live action replay of the original ending has Fred being locked out by Baby Puss!

==Cast==
* John Goodman as Fred Flintstone
* Elizabeth Perkins as Wilma Flintstone
* Rick Moranis as Barney Rubble
* Rosie ODonnell as Betty Rubble  
* Kyle MacLachlan as Cliff Vandercave
* Halle Berry as Miss Sharon Stone
* Elizabeth Taylor as Pearl Slaghoople
* Richard Moll as Hoagie
* Irwin Keyes as Joe Rockhead
* Harvey Korman as the voice of the Dictabird
* Dann Florek as Mr. Slate
* Jonathan Winters as Grizzled Man, A co-worker of Fred and Barneys Dino (appears through archival audio)
* Elaine & Melanie Silver  as Pebbles Flintstone
* Hlynur & Marinó Sigurðsson as Bamm-Bamm Rubble
* The B-52s as The BC-52s
* Jean Vander Pyl (the original voice of Wilma) as Mrs. Feldspar 
* Laraine Newman as Susan Rock
* Jay Leno  as a talk show host
* William Hanna as a boardroom executive  Mersandes
* Sam Raimi as a Fred Flintstone look-alike 
* Michael Richards as a paper man
* Chris Rock as a co-worker at the quarry

===Casting info===
According to pre-release publicity for The Flintstones, Sharon Stone was to play Miss Sharon Stone, but turned it down as she was already working on The Specialist. Nicole Kidman was the second choice for the role, but Halle Berry won the part after a screen test. Steven Spielberg said Danny DeVito was the original first choice for Barney. DeVito eventually turned down the role as he felt he was too gruff to do the character properly and reportedly suggested Rick Moranis for the role. Premiere (magazine)|Premiere magazine showed Elizabeth Montgomery as being considered for the role of Pearl Slaghoople. Rosie ODonnell claims she won the role of Betty Rubble with her impersonation of the cartoon characters signature giggle.

==Production==
In 1985, producers Keith Barish and Joel Silver bought the rights for a live-action feature film version of The Flintstones and commissioned Steven E. de Souza to write a script with Richard Donner hired to direct. Silver was said to be interested in casting James Belushi in the role of Fred. De Souzas script was eventually rejected and Mitch Markowitz was hired to write a script. Said to be a cross of The Grapes of Wrath, Markowitz commented that "I dont even remember it that well, but Fred and Barney leave their town during a terrible depression and go across the country, or whatever that damn prehistoric thing is, looking for jobs. They wind up in trailer parks trying to keep their families together. They exhibit moments of heroism and poignancy". Markowitzs version was apparently too sentimental for director Donner, who disliked it.  Eventually, the rights were bought by Amblin Entertainment and Steven Spielberg who, after working with John Goodman on Always (1989 film)|Always, was determined to cast him in the lead as Fred. Brian Levant was hired as director, knowing he was the right person because of his love for the original series. They knew he was an avid fan of the series because of his Flintstones items collection and the knowledge he had from the series. 

When Levant was hired, all previous scripts for the film were thrown out. Levant then recruited what he called an "all-star writing team" which consisted of his writer friends from television shows such as Family Ties, Night Court and Happy Days. "This is a sitcom on steroids", said Levant. "We were just trying to improve it." Dubbed the Flintstone Eight, the group wrote a new draft but four more round table sessions ensued, each of which was attended by new talent. Lowell Ganz and Babaloo Mandel took home a reported $100,000 for just two days work.  Rick Moranis was also present at Levants roundtables, and later described the film as "one of those scripts that had about 18 writers".  The effects for Dino, Dictabird, and the other prehistoric creatures were provided by Jim Hensons Creature Shop.

==Reception==

===Critical response=== average score Siskel and Ebert, Gene Siskel of the Chicago Tribune and Roger Ebert of the Chicago Sun-Times gave the film two thumbs down. They both mentioned that its main story lines (embezzlement, mother-in-law problems, office politics and extra-marital affairs) were storylines for adult films, and ones that children wouldnt be able to understand. However, many critics praised the films look and costume design, which was made by Rosanna Norton and John Goodmans performance.     Other reviews in which were positive, Time Magazine said "The Flintstones is fun", and Joel Siegel from Good Morning America called the film "pre-historical, hysterical... great fun".
 Worst Remake or Sequel. However, the film also received four Saturn Award nominations, including Best Fantasy Film, Best Costume Design and Best Supporting Actress for Rosie ODonnells and Halle Berrys performances. In a 1997 interview, Flintstones co-creator and Hanna-Barbera co-founder and co-chairman Joseph Barbera stated that, although he was impressed by the films visuals, he felt the story "wasnt as good as I could have made it." 

===Box office performance===
Despite the negative reviews, The Flintstones was a box office success, grossing $130,531,208 domestically, including the $37,182,745 it made during its 4-day Memorial Day opening weekend in 1994. It fared even better overseas, making another $211,100,000, for a total of $341,631,208 worldwide, against a $46 million budget.  

===Prequel===
A prequel, The Flintstones in Viva Rock Vegas, was released in 2000. The original main cast did not reprise their roles of the characters, though Rosie ODonnell provided the voice of an octopus who gave massages to younger versions of Wilma and Betty. Irwin Keyes returned as Joe Rockhead, the only cast member to reprise his role from the first film. Unlike its predecessor, it failed at the box office.

===Marketing===
McDonalds marketed a number of Flintstones promotions for the movie, including the return of the McRib sandwich and the "Grand Poobah Meal" combo with it, a line of premium glass mugs, and toys based on characters and locations from the movie. In the commercials and released items for the Flintstones promotion, McDonalds was renamed "RocDonalds" with stone age imagery, similarly to other businesses and proper names in the Flintstones franchise. A video game based on the film was developed by Ocean software and released for the Super Nintendo Entertainment System, Game Boy and Mega Drive/Genesis (Sega Channel exclusive) in 1995.

==Home media releases==
The film was released, first on VHS and Laserdisc on November 8, 1994 by Universal Studios Home Entertainment|MCA/Universal Home Video, then later made its DVD debut on September 24, 2002 and finally was released on Blu-ray on August 19, 2014.

==See also==
* List of American films of 1994
* Theatrically released films based on Hanna-Barbera cartoons

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 