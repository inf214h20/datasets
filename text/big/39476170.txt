Museum Hours
 
{{Infobox film
| name           = Museum Hours
| image          = Museum Hours poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Jem Cohen
| producer       = Jem Cohen Paolo Calamita Gabriele Kranzelninder Guy Picciotto Patti Smith
| writer         = Jem Cohen
| starring       = Mary Margaret OHara Bobby Sommer
| music          = Mary Margaret OHara
| cinematography = Jem Cohen Peter Roehsler
| editing        = Jem Cohen Marc Vives
| studio         = Gravity Hill Films Little Magnet Films KGP Kranzelbinder Gabriele Production
| distributor    = The Cinema Guild   Soda Pictures  
| released       =  
| runtime        = 106 minutes  
| country        = Austria United States
| language       = German English
| budget         = 
| gross          = $554,361 
}}
Museum Hours is a 2012 Austrian-American drama film written and directed by Jem Cohen. The film is set in and around Viennas Kunsthistorisches Museum.

==Plot==
When a Vienna museum guard befriends an enigmatic visitor, the grand Kunsthistorisches Museum becomes a crossroads that sparks explorations of their lives, the city, and the ways art reflect and shape the world.

One Vienna winter, Johan, a guard at the grand Kunsthistorisches Museum encounters Anne, a visitor called to Austria for a family medical emergency. Never having been to Austria and with little money, she wanders the city in limbo, taking the museum as her refuge. Johann, initially wary, offers help, and theyre drawn into each others worlds. Their meeting sparks an unexpected series of explorations – of their own lives and the life of the city, and of the way artwork can reflect and shape daily experience. The museum is seen not as an archaic institution of historical artifacts, but as an enigmatic crossroads in which, through the art, a discussion takes place across time with vital implications in the contemporary world. The "conversations" embodied in the museums collection revolve around the matters that most concern us: death, sex, history, theology, materialism, and so on. Its through the regular lives of the guard and displaced visitor that these heady subjects are brought down to earth and made manifest.

Near the films end, Johann and Anne are exploring on the fringe of the city when her ill cousins condition reaches a crisis point.

==Cast==
* Mary Margaret OHara as Anne
* Bobby Sommer as Johann
* Ela Piplits as Gerda Pachner

==Release== premiered at the 2012 Locarno International Film Festival, had its North American premiere within the 2012 Toronto International Film Festival, and screened within such U.S. film festivals as South by Southwest and Maryland Film Festival.

The film was acquired for U.S. distribution by The Cinema Guild.

==Awards and nominations==

===Film===
{| class="wikitable sortable"
|-
!Year
!Award
!Category
!Result
|- 2014
|Spirit Independent Spirit Awards  John Cassavetes Award
| 
|- 2014
|Spirit Independent Spirit Awards  Best Editing
| 
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 