¡Ay Carmela!
 
{{Infobox film
| name           = ¡Ay Carmela!
| image          = Ay Carmela, film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Carlos Saura
| producer       = 
| writer         = Rafael Azcona, José Sanchís Sinisterra (play), Carlos Saura
| narrator       = 
| starring       = Carmen Maura, Andrés Pajares, Gabino Diego
| music          = Alejandro Massó
| cinematography = José Luis Alcaine
| editing        = Pablo González del Amo
| distributor    = Prestige Films
| released       = 1990
| runtime        = 102 min. Spain
| language       = Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1990 Cinema Spanish film eponymous Ay play by nationalist side during the closing months of the Spanish Civil War.

==Plot== mute as the result of an explosion - are a trio of travelling vaudeville performers. Amidst the chaos of the Spanish Civil War, they are in the town of Montejo, entertaining republican troops with their variety show. They are survivors who are motivated, not exactly by patriotism, but by a desire for self-preservation. Their show consists of four acts. It begins with Carmela singing and dancing a traditional song. The audience is enthusiastic during her performance, but the mood changes completely when the sound of approaching nationalist planes is heard.

As the planes fly overhead, Paulino reads a poem by Antonio Machado which introduces a note of patriotic fervour in accordance with republican feeling in 1938. The seriousness of the moment is followed by a comic routine in which Paulino twists himself into a variety of ridiculous postures in an attempt to break wind. The fourth and final act is a tableau vivant in which Carmela represents justice while Paulino brandishes the republican flag and they sing a song of freedom.
 Italian officer, Lieutenant Amelio di Ripamonte. Surprisingly, the lieutenant, learning that they are performers, wants them to take part in a show he has been planning to entertain the nationalist troops. They must stage a burlesque of the Republic in exchange for their freedom.

For the variety show that they are to perform to the nationalists, Paulino rewrites their old script. From the outset, the fiery and patriotic Carmela is defiant and unwilling to go along with it, displaying her true convictions as an anti-fascist.  However, Paulino persuades her that since their lives are at stake she must collaborate in the performance of the now anti-republican numbers. 
 Russian lover, played by Gustavete. In a number which gives full scope to all the possible sexual innuendos the audience cares to imagine, Carmela invites the doctor to insert his thermometer in her, to which he refuses, making the excuse that it is broken.
 
Carmela, increasingly irritated by the mockery of the Republic and enervated by the presence of the Polish soldiers, gradually loses heart in her performance, and her frustration at the mockery of the ideals she holds dear seethes to the surface jeopardizing the credibility of the parody. The sketch quickly disintegrates as the Polish soldiers begin to rebel in the galleries and the fascists become infuriated. The scene comes to a climax as Carmela starts singing Ay Carmela and lowers the republican flag to expose her breasts in defiance of the earlier cries of Whore! from the audience. A nationalist officer then emerges from the stalls, raises a pistol and shoots Carmela in the forehead. Gustavete, suddenly recovers his voice calling out in anguish, but Carmela falls to the floor dead.

The next scene shows Paulino and Gustavete visiting Carmelas rudimentary grave which they decorate with flowers and the latter’s chalk board, now redundant since Gustavete regained his voice when Carmela was shot. The only words here are spoken by Gustavete – "Come on, Paulino" – as he leads him away. The two men take to the road again and the song "Ay Carmela (song)|¡Ay Carmela!" rises in the background closing the film as it had begun and taking it into the credits.

==Cast==
* Carmen Maura as Carmela
* Andrés Pajares as Paulino
* Gabino Diego as Gustavete
* Mauricio De Razza  as Lieutenant Ripamonte
* José Sancho as Captain

==Production ==
Made in 1990, ¡Ay Carmela!  was director Carlos Saura’s twenty-third, feature-length film and, in his own words, the first in which he was able to treat the subject of the Civil War with any kind of humour: "I would have been incapable a few years ago of treating our war with humour… but now it is different, for sufficient time has passed to adopt a broader perspective, and here there is no doubt that by employing humour it is possible to say things that it would be more difficult, if not impossible, to say in another way". Edwards, Indecent Exposures, p. 116  

In Saura’s earlier films, allusions to the war and to its consequences were characterized by violence and brutality, and if there was any humour at all it was grim and ironic. Despite the fact that the action in ¡Ay Carmela! is set fully in the War, Saura’s treatment of it employs comic effects, including farce.  

The film is based on the play of the same name by the Valencian dramatist, José Sanchís Sinisterra.   The play was a success in Spain and was translated to English and staged in London. Edwards, Indecent Exposures, p. 130  The play focuses entirely on the two principal characters, Carmela and Paulino, and tells their story largely in flashback.   When it begins, Paulino is alone and depressed, for Carmela is already dead, the victim of a fascist bullet at their last performance as variety artist. In the first part of the play Carmela returns as a ghost to converse with Paulino, blaming him for all that has happened, and in the second part evokes in detail the fatal performance. The play contains only two characters and a single setting. Edwards, Indecent Exposures, p. 117  Saura adapted the play with the help of scriptwriter Rafael Azcona who had worked with him many times before but with whom he had broken in 1975 prior to the making of Cria Cuervos. 
 To Be or Not to Be. Schwartz, The Great Spanish Films, p. 102  

The film takes its title from the song "Ay Carmela", which begins and ends the film. Originally a song from the War of Independence against Napoleon, it had been adapted and became the favourite song of the Republican soldiers and of the International Brigade during the Spanish Civil War. 

==DVD release==
¡Ay Carmela! is available in Region 2 DVD in Spanish with English and French subtitles.

==Awards==
Winner of the 1990 Goya Awards for: Best Film
* 
* 
* 
* Best Adapted Screenplay: Carlos Saura, Rafael Azcona
* 
*Best Editing: Pablo González del Amo
*Best Sound:  Gilles Ortion, Alfonso Pino
*Best Costume Design: Rafael Palmero, Mercedes Sánchez
*Best Make-Up: José Antonio Sánchez, Paquita Núñez
*Best Production Design: Rafael Palmero
*Best Production Supervision: Víctor Albarrán
*Best Special Effects: Reyes Abades

==Notes ==
 

== References ==
* Edwards, Gwynne, Indecent Exposures, PMarion Boyars, 1995, ISBN 0-7145-2984-2
* Schwartz, Ronald, The Great Spanish Films: 1950 - 1990, Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

== External links ==
*  
* http://www.youtube.com/watch?v=OFZLwsA-Si8

 
 

 
 
 
 
 
 
 
 
 
 
 
 