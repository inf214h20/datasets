Zombie Women of Satan
{{Infobox film
| image          =
| alt            =
| caption        = Steve OBrien Warren Speed
| producer       = Steve OBrien
| writer         = Warren Speed Seymour Mace Steve OBrien
| starring       = Warren Speed Victoria Hopkins Christian Steel
| music          = Dan Bewick
| cinematography = Steve OBrien
| editing        = Richard Johnstone
| studio         = 24:25 Films Growling Clown Entertainment
| distributor    = Revolver Entertainment
| released       =   |df=y}}
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = £70,000   
| gross          =
}} Steve OBrien and Warren Speed, written by Warren Speed with contributions from Seymour Mace and OBrien, and starring Speed, Victoria Hopkins, and Christian Steel.  A group of circus freaks must combat zombies and cultists while trying to rescue a captive.

== Plot ==
Johnny Hellfire, Pervo the Clown, Zeus the midget, and Damage the strongman are a circus freak troupe on a promotional tour along with a goth group fronted by Skye Brannigan, whose sister Rachel has gone missing.  Along the way, they appear on a show hosted by Tycho Zander, who also happens to lead a sex cult.  Tychos father Harry accidentally transforms the women in Tychos cult into zombies, and they attack.  The circus freaks must stop the zombies, rescue Skyes sister, and confront Tycho.

== Cast ==
* Warren Speed as Pervo the Clown
* Seymour Mace as Johnny Hellfire
* Victoria Hopkins as Skye Brannigan
* Victoria Broom as Rachel Brannigan
* Peter Bonner as Zeus
* Joe Nicholson as Damage
* Bill Fellows as Dr. Henry Englebert Zander III
* Christian Steel as Tycho Zander
* Kathy Paul as Florence Mother Zander
* Marysia Kay as Red Zander
* Gillian Settle as Blue Zander
* Kate Soulsby as Harmony Starr

== Production ==
Zombie Women of Satan was shot in Newcastle and Gateshead in 2009 for £70,000.  

== Release ==
The premiere was at the 2009 London FrightFest Film Festival.   Supermarkets in the UK refused to stock the DVD due to its lurid title.   Revolver Entertainment released it on DVD in the UK on 21 June 2010.   Screen Media Films released it on DVD in the US on 29 March 2011. 

== Reception ==
Ian Berriman of SFX (magazine)|SFX rated it 1/5 stars and compared it negatively to amateur porn.  Berriman concluded that it is "dull and ugly", suited only as propaganda against decadent Western culture.   Jeremy Blitz of DVD Talk rated it 2/5 stars and wrote, "Zombie Women of Satan provides sporadic fun, lots of blood and gore, and even more bare female flesh. But thats about it. The jokes fall flat as often as they work, the story is pretty incomprehensible and there are few really likeable characters."   Marc Patterson of Brutal As Hell called it "a lowbrow, craptastic good time, exploding with cheap gore gags and bountiful nudity."   Peter Dendle wrote, "The movie is adolescent in tone and content, and the zombies are played mostly for visual objectification and batting practice." 

== Sequel ==
Chris Greenwood shot a sequel, Zombie Women of Satan 2, in 2013. It stars Pete Bennett and Michael Fielding, and it has a planned release date of sometime in 2014. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 