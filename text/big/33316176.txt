Haunted Poland
{{Infobox film
| name           = Haunted Poland
| image          = Haunted Poland Poster.png
| caption        = Theatrical release poster
| director       = Pau Masó
| writer         =
| starring       = Ewelina Lukaszewska Pau Masó Irene González Dominik Lukaszewski
| music          = 
| cinematography = 
| editing        = Pau Masó
| studio         = Maso & Co Productions
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Poland
| language       = English Polish
| budget         = 
| gross          = 
}}
Haunted Poland ( ) is a 2011 horror film in the found footage genre, pieced together from amateur footage. The film was produced by Maso & Co Productions. Told cinéma vérité-style, Haunted Poland depicts the contents of recorded tape filmed by a couple Ewelyn played by Ewelina Lukaszewska, and Pau played by Pau Masó who visited Poland to meet and visit family. However, our duo soon find themselves disturbed by all manner of strange phenomena upon visiting the girls hometown where she once played with a Ouija board.  The film  premiered at the AIFF on November 20, 2011.

A sequel,   ( ), has been announced. 

==Plot==
In 2011, a couple living in America, Ewelina and Pau, travel to Poland to meet the girls family. As the film goes by, strange phenomena occurs, but her reluctant "non-believing" boyfriend, just pokes fun at her instead of supporting her, while she is actually having a hard time.

==Cast==
* Ewelina Lukaszewska
* Pau Masó as Pau
* Irene González as Irene
* Dominik Lukszewski as Ewelyns brother

==Production==
Filming began in July 2011 and lasted seven days.   

==Release==
The film will premiere at the 2011 American International Film Festival (AIFF) on November 20 as well as the Sundance Film Festival. 

==Reception==
Haunted Poland has garnered negative reviews and the director/actor has been accused of "borrowing liberally from Paranormal Activity".  In addition, the first teaser for the film holds a 90% negative bar rate on YouTube.  The director released a statement on Dread Central to stop the negativity the trailer created "Haunted Poland was meant to be an experimental project, says director Masó" and continues "This is not a Hollywood film with a budget of 20 million dollars. This film was completely shot on a consumer camera, with an estimate budget of 1000.00$."   Additionally, the actor insisted later on, that the film was independent by making this statement "This movie is a no-budget and will be approximately 90 minutes". 

==Awards and nominations==

On October 2011, Pau Masós directorial debut Haunted Poland won "Best Movie/Documentary/Video/Script featuring  Religiosus/Supernatural theme" on the American International Film Festival 2011. 

==See also==
* Found footage (genre)

==References==
 

==External links==
*  
*  
*  
* " " on Bloody Disgusting
* " " on Horror News
* " " on Dread Central
* " " on Filmweb
* " " in the JoBlo.com

 
 
 
 
 
 
 
 
 
 
 
 