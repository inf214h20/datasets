Janda Pai Kapiraju
{{Infobox film
| name           = Janda Pai Kapiraju
| image          = Janda pai kapiraju poster.jpg
| alt            =  
| caption        = 
| director       = Samuthirakani
| producer       = K. S. Sreenivasan
| writer         =  Nani Siva Balaji Amala Paul Ragini Dwivedi
| music          = G. V. Prakash Kumar
| cinematography = M. Sukumar - M. Jeevan
| editing        = S.N.Fazil
| studio         = Vasan Visual Ventures
| distributor    = 
| runtime        =
| released       =  
| country        = India
| language       = Telugu
| budget         = 10 Crores
| gross          =
}} Telugu Action Nani and Tamil as Nimirndhu Nil with Jayam Ravi. While the Tamil version released in 2014 and went onto become a BlockBuster, the Telugu version released on March 21 2015. Nani played a dual role in his career for the first time. The film received positive reviews.

== Plot ==
Arvind an employee in a company came to city for job from a rural area.
He dont know about the peoples mentality of outer world. Only he
studied about the people in books. He cant tolerate
corruption, bribing, breaking traffic rules, lavish and urban culture
of youth in cities like friendship with opposite sex, going to pubs
etc., He was caught by traffic police for a small mistake though not
done intentionally. Although he has all documents with him, he was asked to bribe
by traffic police. But he denied it, then a complaint booked on him and
prosecuted by court. In the court he complained on police for bribing.

Finally he did a sting operation with 2 of his friends and also 10 other 
honest people in various government offices by applying government
identities like  Pan card, voter card, ration card for an
anonymous person. For this the respected authorities of 147 members
were allegedly booked cases.
Unlike other films it focused on corruption on Government
employees not Politicians. As he believe that  a political leader is in power for
only 5 years but the latter is for 38 years. So corruption must be
plucked at the Government offices.
==Cast== Nani as Arvind
* Amala Paul as Indumathi
* Ragini Dwivedi
* Ruthika Srinivasan
* Sarathkumar
* Nassar
* Anil Murali
* Subbu Panchu
* Parvathy Nair
* Vennela Kishore
* Tanikella Bharani
* Namo Narayana
* Chitra Lakshmanan Chandramohan
* Siva Balaji
* Baby Angela as younger sister of Amala Paul(Twin 1)
* Baby Andrea as younger sister of Amala Paul (Twin 2)
* Jeyam Ravi (Cameo appearance in Rajadhi raja song)

==Production==
The film was launched on August 1, 2013 at Annapurna Studios. The films first schedule of shooting started a day later. {{cite web|url=http://timesofindia.indiatimes.com/entertainment/regional/telugu/news-interviews/Nanis-new-film-Janda-Pai-Kapiraju-launched/articleshow/15308638.cms |title=Nani’s new film Janda Pai Kapiraju launched |publisher=Times of India |date=February 2014
 |accessdate=3 August 2012}}  Nani plays dual roles - that of a 24-year-old and of a 18-year-old. Amala Paul will play the female lead in both the versions.  The movie will be produced by KS Sreenivasans Vaasan Visual Ventures.  Meghana Raj was reported to have an important role in the film,  but she said on Twitter that she was not acting in the film.    Ragini Dwivedi was roped in to play an important role.  By March 2013, shooting of the movie was said to be 50% complete and first look was launched on Nanis birthday.  The next schedule began in the same month at Hyderabad whose duration was of 3 weeks in which scenes on Nani and Amala Paul were shot.  On November 26, 2013 Nani on his Twitter revealed the looks of his Dual roles.  On December 5, 2013 a press note was released which revealed that the films shoot was wrapped up that day.  The movie released on 21 March. 

==Soundtrack==
{{Infobox album
| Name = Janda Pai Kapiraju
| Longtype = To Janda Pai Kapiraju
| Type = Soundtrack
| Artist = G. V. Prakash Kumar
| Cover = Janda Pai Kapiraju Audio.jpg
| Released = December 28, 2013
| Recorded = 2012 Feature film soundtrack
| Length = 19:29 Telugu
| Label = Saregama
| Producer = G. V. Prakash Kumar
| Reviews =
| Last album = Nimirndhu Nil (2013)
| This album = Janda Pai Kapiraju (2013)
| Next album = 
}}

G. V. Prakash Kumar composed the Music for both the Telugu and Tamil Versions which featured the same soundtrack. The Audio was launched on Saregama Music Label at Shilpakala Vedika, Hyderabad on December 28, 2013. 

{{tracklist
| headline        = 
| extra_column    = Artist(s)
| total_length    = 19:29
| all_lyrics      = Anantha Sreeram

| title1          = Inthandanga
| extra1          = Javed Ali, Shasha, G. V. Prakash Kumar
| length1         = 03:25

| title2          = Dont Worry Be Happy
| extra2          = Jai Srinivas, Priya Himesh
| length2         = 03:38

| title3          = Rajadhi Raja Hemachandra
| length3         = 04:34

| title4          = Thelisinadi
| extra4          = Haricharan, Saindhavi
| length4         = 04:31

| title5          = Gita Verses
| extra5          = Haricharan
| length5         = 03:19
}}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 