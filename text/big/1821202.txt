Barabbas (1961 film)
{{Infobox film
| name           = Barabbas
| image          = Barabbas film poster.jpg
| image_size     =
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Fleischer 
| producer       = Dino De Laurentiis
| screenplay     = Nigel Balchin Diego Fabbri Christopher Fry
| based on       =   Arthur Kennedy Jack Palance Silvana Mangano Harry Andrews Ernest Borgnine
| music          = Mario Nascimbene
| cinematography = Aldo Tonti
| editing        = Alberto Gallitti Raymond Poulton Dino de Laurentiis Cinematografica
| distributor    = Columbia Pictures
| released       =  
| runtime        = 137 minutes
| country        = Italy
| language       = English
| gross = $2,900,000 (US/ Canada) 
}} religious epic Passion narrative Arthur Kennedy, Nobel Prize-winning 1950 novel of the previous film version of the novel, in Swedish language|Swedish, had been made in 1953.

The film was directed by Richard Fleischer and shot in Verona and Rome under the supervision of producer Dino De Laurentiis. It included many spectacular scenes, including a battle of gladiators in a Cinecittà film studio mock-up of the arena, and a crucifixion shot during a real eclipse of the sun.

==Plot==
Shortly before the crucifixion of Christ, Pontius Pilate offers to release either Jesus Christ or Barabbas in keeping with the local custom. As the Bible story goes, Barabbas is the one the crowd chooses.

Barabbas returns to his friends, and asks where his lover Rachel is. They inform him that Rachel had changed while he was away, and has become a follower of Christ. Rachel soon returns, but she is not overjoyed to see Barabbas.

As Christ dies, the sky turns dark. Shaken by this, Barabbas goes to witness the crucifixion. Afterwards, he goes to watch Christs body being sealed in the tomb. On the third morning, Barabbas goes to the tomb to find it open, and the corpse gone. Rachel tells him that Christ has risen, but Barabbas dismisses this as illusion, or that his followers had taken his body. He goes to see the Apostle Peter and Christs other followers; they do not know where he is, but do believe he is risen.

Rachel begins to talk to others in Jerusalem about Christ. This results in her being stoned to death at the insistence of the priests. When Barabbas comes across them later while robbing a caravan, he throws stones at one of them rather than fleeing, and is captured by Roman soldiers. Pilate finds that the law forbids him to execute someone who has been pardoned as Barabbas has, so he instead sentences him to the sulfur mines of Sicily.

Barabbas manages to survive a hellish existence for the next twenty years in the mines.  Eventually, he is chained to Sahak, a sailor who was sent to the mines for being on watch when some slaves escaped. Sahak is a Christian. At first, Sahak hates him for being chosen to live by the mob instead of "the Master", but the two men eventually become friends. After some time, Sahak becomes too weak to work, and is about to be killed - but the mine is destroyed in an earthquake. Sahak and Barabbas are the only survivors. As such, they are considered lucky by Julia, the superstitious wife of the local prefect. The prefect is due to leave for Rome to become a Senator. Julia insists that Barabbas and Sahak be brought along for good luck.

Once in Rome, the men are trained to become gladiators by Torvald &nbsp;&ndash; the top gladiator in Rome. Just after one of the gladiatorial events, Sahak is overheard sharing his faith with other gladiators, and is condemned to death for Law of majestas|treason. When the others deliberately miss with their thrown spears, he is executed by Torvald. The next day, Torvald and Barabbas battle in the arena, with Barabbas winning. Impressed with Barabbas, the Roman Emperor Nero sets him free. Barabbas takes Sahaks corpse to the catacombs, where the local Christians are worshiping, for a proper burial.
 the persecutions that followed the fire. Throughout his life, Barabbas was reputed to be the man who could not die; having finally placed his faith in Christ, he dies. This movie does not however follow the book very closely.

==Cast==
 
* Anthony Quinn as Barabbas Arthur Kennedy as Pontius Pilate
* Jack Palance as Torvald
* Silvana Mangano as Rachel
* Harry Andrews as Peter
* Ernest Borgnine as Lucius
* Katy Jurado as Sara
* Vittorio Gassman as Sahak
* Norman Wooland as Rufio
* Valentina Cortese as Julia Arnoldo Foa as Joseph of Arimathea
* Michael Gwynn as Lazarus
* Laurence Payne as Disciple
* Douglas Fowley as Vasasio
* Guido Celano as Scorpio
* Enrico Glori
* Carlo Giustini as Officer
* Gianni di Benedetto as Officer
* Robert Hall as Commander of Gladiators
* Rina Braido as Tavern Reveler
* Nando Angelini
* Tullio Tomadoni as Blind Man Joe Robinson as Gladiator
* Frederich Ledebur as Officer
* Marcello Di Martire
* Spartaco Nale as Overseer
* Maria Zanoli as Beggar Woman
* Gustavo De Nardo
* Vladimiro Picciafuochi

;Uncredited
* Roy Mangano as Jesus Christ
* Paola Pitagora as Mary Magdalene
* Rina Franchetti as Mary Clopas
* Piero Pastore as Nicodemus
* Vera Drudi as Salome
* Nino Segurini as Apostle John
* Jacopo Tecchi as Apostle Thomas Emperor Nero
* Sharon Tate as Patrician in arena
 

==Production== music score total eclipse of the sun, which had been considered to be a supernatural event in the Judean age, really took place.  

==Reception==
Barabbas received positive reviews; it currently holds a 100% rating on Rotten Tomatoes. 

==Awards==
*Nominee Best Color Cinematography - Italian National Syndicate of Film Journalists (Aldo Tonti)
*Nominee Best Costume Design - Italian National Syndicate of Film Journalists (Maria De Matteis)
*Nominee Best Production Design - Italian National Syndicate of Film Journalists (Mario Chiari)
*Selected Top Foreign Films of the Year - National Board of Review

==Biography==
* 

==See also==
 
* 1961 in film
* Italian films of 1961

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 