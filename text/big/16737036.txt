A Feud There Was
 
{{Infobox film
| name           = A Feud There Was
| image          = 
| caption        = 
| director       = Tex Avery
| producer       = Leon Schlesinger
| writer         = Melvin Millar
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mel Blanc Billy Bletcher Hugh Farr Bob Nolan
| music          = Carl W. Stalling
| cinematography = 
| editing        = Treg Brown
| studio         = 
| distributor    = 
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Feud There Was is a 1938    , although it did not resemble the more familiar "cwazy wabbit" voice which would later be performed for Fudd by Bryan. The characters singing voice was provided by Roy Rogers and additional vocals in the cartoon were done by the Sons of the Pioneers.

==Synopsis==
Two feuding families of stereotypical Hillbilly|hillbillies, the Weavers and the McCoys, spend their time taking potshots at each other. At one point, a McCoy asks if there are any Weavers in the movie audience. One man, shown as a silhouette against the screen, answers in the affirmative, and the McCoy takes a shot at him.

In the midst of the fray, a yodeling, bulbous-nosed, domestic peace activist enters the feud zone on a motorscooter bearing the words "Elmer Fudd, Peace Maker", and goes to each side preaching peace and an end to wanton bloodshed. Neither side is impressed, and when "Elmer" attempts once more to preach peace to both families, both sides get furious at him and open fire on the would-be peace maker together. When the smoke clears, only "Elmer" is left standing. He gives a final yodel and says "Good night, all!", and the Weaver in the movie audience yells "Good night!," taking one more shot at the star.

==External links==
* 
* 

 
 
 
 
 
 
 


 