Paganini (1934 film)
{{Infobox film
| name =  Paganini
| image =
| image_size =
| caption =
| director = E. W. Emo
| producer = Helmut Eweler Alfred Greven Franz Tappers
| writer = Béla Jenbach Paul Knepler Georg Zoch
| narrator =
| starring = Iván Petrovich Eliza Illiard Theo Lingen Adele Sandrock
| music = Franz Lehár
| cinematography = Ewald Daub
| editing = Martha Dübber
| studio = Majestic-Film
| distributor = Neue Deutsche Lichtspiel-Syndikat Verleih (NDLS)
| released = 3 July 1934
| runtime = 87 minutes
| country = Nazi Germany
| language = German
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Paganini or I Liked Kissing Women (German:  ) is a 1934 German operetta film directed by E. W. Emo and starring Iván Petrovich,   and Theo Lingen.  It is an adaptation of Franz Lehárs 1925 operetta Paganini (operetta)|Paganini.

==Cast== Paganini
* Anna Elisa of Lucca
* Theo Lingen as her court chamberlain Pimpinelli
* Adele Sandrock as her lady-in-waiting Gräfin Zanelli
* Maja Feist as her court lady Comtesse Jeanne dAnvier
* Rudolf Klein-Rogge as Count Hédouville, courier to Napoleon
* Aribert Wäscher as theatre director Sebaldus Manzetti
* Erika Glässner as Thalia, his wife
* Maria Beling as Bella, her niece
* Veit Harlan as Enrico Tortoni, member of the troupe Manzetti
* Gustav Mahncke as commander of the body guard of Duchess Anna Elisa
* Erich Dunskus as inn keeper Franz Weber as servant
* Egon Brosig
* Karl Harbacher
* Hans Hemes
* Wolfgang von Schwindt

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918–1945. University of California Press, 1999.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 