Au galop
{{Infobox film
| name = Au galop
| image = 
| caption = 
| director = Louis-Do de Lencquesaing
| screenplay = 
| based on = 
| starring = Louis-Do de Lencquesaing, Alice de Lencquesaing, Valentina Cervi, Marthe Keller
| producer = Gaelle Bayssiere, Didier Creste, George Fernandez
| released =  
| runtime = 93 minutes
| country = France
| language = French
| budget         = $2,220,000
| gross          = $251,755 
}}
Au galop is a 2012 French comedy film.      

==Plot==
Ada is in a relationship, with a child, about to get married, when she meets Paul, a single man with a daughter and an overbearing mother.  His father dies. 

==Cast==
*Louis-Do de Lencquesaing as Paul
*Marthe Keller as Mina
*Valentina Cervi as Ada
*Alice de Lencquesaing as Camille
*Bernard Verley as BonP
*Xavier Beauvois as François
*Laurent Capelluto as Christian
*Ralph Amoussou as Louis
*Emola Romo-Renoir as Zoé
*Denis Podalydès as the editor
*André Marcon as the financial advisor
*George Aguilar as the taxi driver
*Jeanne La Fonta as the singer

==Critical reception==
*Variety (magazine)|Variety compared it to "a sad soap, only with literary ambitions and more nudity".  The Hollywood Reporter suggested that first-time director Louis-Do de Lencquesaing should stick to being an actor: "one gets the impression here that his talent is best expressed on the other side of the lens." 

==References==
 

 
 
 
 
 
 

 
 