Friends (2001 film)
 
{{Infobox film
| name           = Friends
| image          = Friends DVD Cover.jpg
| caption        = DVD Cover Siddique
| Siddique Gokul Krishna Vijay Suriya Suriya Ramesh Devayani Vijayalakshmi Vijayalakshmi
| Appachan
| cinematography = Anandakuttan
| editing        = T. R. Shekar K. R. Gowrishankar
| music          = Ilaiyaraaja
| studio    = Swargachitra
| released       =  January 14, 2001
| runtime        = 167 minutes
| country        = India Tamil
| budget         =  5 crore
| gross          =  30 crore
}} Tamil comedy Siddique and Suriya and Sriman and Malayalam film Sreenivasan and Jayaram.The film released in January 2001 and received positive critical acclaim.The movie completed a 175-day theatrical run at the box office making it fourth hit in a row for the actor Vijay(actor)|Vijay.The movie was a commercial success and one among the top grossers of the year. 

==Plot==
The threesome who lend reason to the title are Aravindhan(Vijay(actor)|Vijay),Chandru(Suriya (actor)|Suriya) and Krishnamurthy(Ramesh Khanna).Aravindhan(Vijay(actor)|Vijay),Chandru(Suriya (actor)|Suriya) and Krishnamurthy(Ramesh Khanna) value friendship over everything else, even family,and for this reason,Chandru resists the advances of Aravindans sister Amutha (Vijayalakshmi (Kannada actress)|Vijayalakshmi).When the trio take up a painters job at a mansion, Aravindan falls in love with Padmini (Devayani (actress)|Devayani),who lives there. Padminis jealous cousin Abhirami(Abinayasri) makes him believe that his overtures are reciprocated. When the truth is revealed and Padmini rejects him outright,Chandru stands up for his friend and speaks ill of her.This makes her swear to separate the friends.

After the marriage,Aravindan realises the hand of Abhinaya behind all the mishaps and Padmini too realises Chandru is innocent. Then the marriage of Chandru and Amutha is arranged. The cousin of Aravindan who is in love with Amutha from his young age tries to separate the pair. He makes Chandru believe that Padmini is trying to kill Amutha.Padmini is now angry with Chandru.At last Aravindan tells the truth that in their young ages Chandrus deaf and dumb brother was unknowingly killed by Aravindan.The cousin of Aravindan records this and makes this to be heard by Chandru.Chandru and Aravindan go by their ways.Aravindan tries to convince Chandru but he falls from the top of a mountain. Chandru believes that Aravindan is dead and feels very much for him. But after five years later, he comes to meet Aravindans family, only to find out that Aravindan is not dead, but has ended up in coma.His cousin is now torturing his family a lot. When he comes to know that Chandru has come back, he goes and starts to beat him up.After hearing his beloved friend Chandrus cry of pain,Aravindan gets his memory back.Aravindan beats up the cousin and they all rejoin and live happily.
 Devayani and Vijayalakshmi respectively due to date problems.

==Cast==
  Vijay as Aravindhan Suriya as Chandru
*Ramesh Khanna as Krishnamoorthy Devayani as Padmini Vijayalakshmi as Amutha
*Abinayasri as Abhirami
*Vadivelu as Nesamani
*Charle as Gopal Sriman as Gautham
*Radha Ravi as Abhiramis father Rajeev as Aravindhan and Amuthas father
*Pyramid Natarajan|V. Natarajan as Gauthams father
*A. R. Srinivasan Padminis fathers friend
*Rathan as Paminis father
*Joju George
*Saritha as Aravindan and Amuthas mother
*Sathyapriya as Gauthams mother
*S. N. Lakshmi as Aravindhan, Gautham and Amudhas grandmother
*Madan Bob as Manager Sundaresan
*Radha Bhai as Padmini and Abhiramis grand mother
*Shanthi Williams as Padminis fathers friends wife
*Crane Manohar as Nesamanis worker
 

==Crew==
  Siddique
*Dialogue: Gokulakrishnan Appachan
*Music: Ilaiyaraaja
*Cinematography: Anandakuttan
*Editing: T. R. Shekar and K. R. Gowrishankar
*Art: Mani Suchithra
*Fights: Kanal Kannan
*Lyrics: Pazhani Bharathi
*Choreography: Raju Sundaram, L. L. Cool Jayanth and Ravidev
*Costumes: T. K. Velayudham
*Banner: Swargachitra
 

==Production== Vijay and Suriya came Mukesh co-starring Siddique who Sreenivasan in Simran replaced Simran and Devayani and Vijayalakshmi from Mysore.Shooting was held in a fast pace on location in Ooty,Pollachi,Pazhani and Udumalaipettai. 

==Soundtrack==
The film featured music composed by Ilaiyaraaja received positive response and topped the charts.
{{Infobox album|  
  Name        = Friends
|  Type        = Soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       = Friends DVD Cover.jpg
|  Released    = 2001
|  Recorded    = Feature film soundtrack
|  Length      = 40.55
|  Label       = Five Star Audio
|  Producer    =
|  Reviews     =
|  Last album  = Mugam (2001)
|  This album  = Friends (2001) Housefull (2001)
}}

==Track list==
{{track list
| headline        = Track-list
| extra_column    = Artist(s)
| total_length    = 40.55
| title1          = Thendral Varum  
| extra1          = Hariharan (singer)|Hariharan, Bhavatharini
| lyrics1         = Pazhani Bharathi
| length1         = 05:03
| title2          = Kuyilikku Koo Koo
| extra2          = S. P. Balasubramaniam, Hariharan, Shankar Mahadevan
| lyrics2         = Pazhani Bharathi
| length2         = 05:06
| title3          = Rukku Rukku 
| extra3          = Yuvan Shankar Raja, Vijay Yesudas, Sowmya Raoh
| lyrics3         = Pazhani Bharathi
| length3         = 04:30
| title4          = Manjal Poosum Manjal Poosum
| extra4          = Sujatha Mohan, Devan
| lyrics4         = Pazhani Bharathi
| length4         = 05:08
| title5          = Penkaloda Potti 
| extra5          = Hariharan (singer)|Hariharan,Sujatha Mohan
| lyrics5         = Pazhani Bharathi
| length5         = 05:02
| title6          = Poonkatrae Hariharan
| lyrics6         = Pazhani Bharathi
| length6         = 05:26
| title7          = Vaanam Perusuthan
| extra7          = S. P. Balasubramaniam, Vijay Yesudas, Arun Mozhi
| lyrics7         = Pazhani Bharathi
| length7         = 05:10
}}
==Release== Sun TV for a record price. The film was given a clean "U" certificate by the Indian Censor Board.

==Critical Response==
The film received highly positive reviews from critics upon release in 2001.Bizhat stated "Friends is a engaging entertainer with the first part moving at a fast pace with ribtickling humour and slapstick comedy thrown in. The second half moves with the smooth transition and an effective climax and stated   in the first half and sentiments in the second half which is overwhelmed by the twists and turns with an effective acting by Vijay(actor)|Vijay. 

==BoxOffice==
The film took a big opening at the box office despite being released with Ajith Kumars Dheena.With a 175 days theatrical run among the screens of Tamil Nadu,Friends was one among the most commercially successful films of that year and Vijay(actor)|Vijays acting was highly appreciated.

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 