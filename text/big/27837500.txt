Obsession (1997 film)
{{Infobox film
| name           = Obsession
| director       =  
| producer       = Wolfgang Esterer George Hoffmann Dagmar Rosenbauer
| writer         = Marie Noelle Peter Sehr
| starring       = Daniel Craig Heike Makatsch Charles Berling
| music          = Micki Meuser David Watkin
| editing        = Heidi Handorf
| studio         = 
| distributor    = 
| released       =     (Toronto Film Festival) }}
| runtime        = 100 minutes
| country        = France Germany
| language       = French English German
| budget         = 
}}

Obsession is a 1997 Franco-German drama film directed by   and starring Daniel Craig. It is about two men involved with one woman. The film was released in Germany on 28 August 1997 and premiered in Canadas Toronto Film Festival on 9 September the same year.

==Cast==
*Marie-Christine Barrault as Ella Beckmann
*Charles Berling as Pierre
*Heike Makatsch as Miriam Auerbach
*Seymour Cassel as Jacob Frischmuth
*Allen Garfield as Simon Frischmuth
*Daniel Craig as John MacHale
*Daniel Gelin as Xavier

==Awards and nominations==
*1997 – San Sebastián International Film Festival – Recipient: Peter Sehr
*1998 –  

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 