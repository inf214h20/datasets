Arrah-na-Pogue
{{Infobox film
| name           = Arrah-na-Pogue
| image          = Arrah_na_Pogue_Wiki.jpg
| alt            =
| caption        = From left to right: Robert Vignola, Sidney Olcott and Gene Gauntier.
| director       = Sidney Olcott
| producer       = Sidney Olcot
| writer         = Gene Gauntier
| based on       =  
| starring       = {{plainlist|
* Gene Gauntier
* Jack J. Clark
* JP McGowan
* Robert Vignola
}}
| music          = Walter C. Simon
| cinematography = George K. Hollister
| editing        =
| studio         = Kalem Company
| distributor    = General Film Company
| released       =  
| runtime        =
| country        = United States
| language       = Silent film (English intertitles)
| budget         =
| gross          =
}}
Arrah-na-Pogue is a 1911 American silent film produced by Kalem. It was directed by Sidney Olcott with Gene Gauntier, Jack J. Clark, JP McGowan and Robert Vignola. Gene Gauntier adapted a play written by Dion Boucicault, Arrah-na-Pogue, an Irish word that can be translated as "Annah of the Kiss". 

== Plot ==
 

== Cast ==
* Gene Gauntier - Arrah-na-Pogue
* Robert Vignola - Feeney
* Sidney Olcott - Shaun the Post
* Agnes Mapes - Fanny Powers
* Jack J. Clark - Beamish McCoul Arthur Donaldson - OGrady
* JP McGowan - Secretary of State
* Anna Clark 
* Harry OBrien
* Thomas P. Glancy
* George H. Hardin
* Charles Lully

== Production notes ==
The film was shot in Beaufort, County Kerry, Ireland. 

== References ==
 
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  
== External links ==
*  
*     website dedicated to Sidney Olcott

 
 
 
 
 
 