She's the One (1996 film)
{{Infobox film
| name = Shes the One
| image = Shestheonemovieposter.jpg
| caption = Theatrical release poster
| director = Edward Burns
| producer = Edward Burns   Michael Nozik John Sloss
| writer = Edward Burns
| starring = Edward Burns Jennifer Aniston Cameron Diaz Mike McGlone John Mahoney
| music = Tom Petty and the Heartbreakers
| cinematography = Frank Prinzi
| editing = Susan Graef
| studio = Fox Searchlight Pictures
| distributor = 20th Century Fox
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = $3 million 
| gross = $13,795,053
}}
Shes the One is a 1996 comedy-drama film, and the second feature film to be written and directed by New York actor and director Edward Burns. It stars Jennifer Aniston and Cameron Diaz, and is one of Tom Pettys few movie soundtracks.

==Plot== Fitzpatrick brothers, Mickey (Edward Burns) and Francis (Mike McGlone) and the tribulations of love, family and betrayal. Mickey is a New York City blue-collared taxi driver, unhappy over an act of infidelity committed by Heather (Cameron Diaz), his ex-fiancée. Francis is a white-collared Wall Street stock investor, who unknown to his wife, Renee (Jennifer Aniston), is having an affair.

During weekends, Mickey and Francis visit their parents who live on Long Island. Their mother is never seen on screen, while they have better time interacting with their father, Frank (John Mahoney), who is an emotionally bereft conservative man as well as an old-school sexist and a bigot who takes them out fishing on his motorboat, but refuses to let women on board due to family tradition. Frank always gives Mickey and Francis advice to live out their lives any way they can and to always go for what drives them to succeed.

While driving his cab one day, Mickey picks up Hope (Maxine Bahns), a girl and NYU art student with whom he becomes infatuated and impulsively marries after a few days. This causes consternation for his brother, largely because he was not asked to be best man at the wedding. Mickey moves in with Hope, but later becomes disillusioned with aspects of their lifestyle, including frequent power cuts in their ramshackle apartment. Francis is also having problems in his marriage. He is concerned that he is being unfair to Heather, his mistress, the very same woman Mickey left, by continuing to stay with Renee. At the same time, Renees Italian-American family, mostly her younger sister Molly (Amanda Peet), suggest the problem with Francis lack of interest in her is that he may be gay, so she has Mickey and their father, Frank, confront him about it. He denies being gay, but admits to being unfaithful. 

Meanwhile, Francis arrogance leads him to take potshots at his brother for the apparent lack of forward progression in his life with his wife, while Francis argues with Heather about her own sexual infidelity with a much older man referred to as "Papa". One day, Mickey picks up Heather in his cab and goes up to her apartment to retrieve a television that belonged to him during their relationship. Heather responds by demanding Mickeys watch, which she purchased as a gift. Mickey relents, which she implies she wants more from him, but he does not reciprocate, instead reminds her of her affair, and her previous profession. Throughout all this, Mickey and Francis father offers more egostical and selfish advice to them. Then Frank learns, during a fishing trip with the churchs pastor, that his supposedly highly religious wife hasnt been to Church Mass in months. 

On a visit to his mistresss apartment, Francis learns that Mickey and Heather have recently met. Francis makes an unexpected visit to his brothers apartment and questions Mickey about his visit with Heather. They get into an argument over whether or not Mickey had sex with Heather. Later, Mickey discovers that she is the woman Francis has been having an affair with. The revelation escalates to an argument in their parents home, during a birthday dinner for their father. In an attempt to settle the dispute, Frank takes both Francis and Mickey outside and ties boxing gloves on each, with the intent of having them fight out their differences. Mickey proves the victor. 

Eventually, Francis confronts Renee about his affair and files for divorce, in order to marry Heather. When Mickey finds out about the impending marriage, he informs Francis that Heather was once a prostitute during her college days. This causes Francis to get cold feet and it angers Heather. She tells Mickey that she thought that he didnt have a problem with her prior choice of employment, and Mickey says that he doesnt, but Francis may, and that he has a right to know.

Meanwhile, Hope has already dropped a bomb on Mickey: she is moving to Paris very soon and Mickey is not sure about whether or not to join her. On a visit to the bar where Hope works, Mickey discovers that Connie (Leslie Mann), one of Hopes co-workers, claims to have had a "special relationship" with Hope before the marriage. This troubles Mickey so much that he begins to avoid Hope. Later, Hope tells Mickey that she is unsure if he should come to Paris after all and admits to having a "semi-lesbian affair" with Connie some years back. As a result, Mickey and Hopes relationship appears to be about to break up. 

Due to Francis indecision over marriage in light of the news that Heather was a prostitute, Heather decides to marry "Papa", the wealthy older man she has been seeing. When Francis threatens to tell Papa that Heather was a prostitute, Heather tells Francis that Papa was "her best customer". He then calls Renee in hopes of getting back together with her. But she is already in another relationship with Scott Sherman, a mutual acquaintance whom Francis had made fun of in an earlier scene.

In the final scenes of the film, Mickey and Francis meet with their father at his house where the distraught Frank tells them that their mother had just left him the previous day for a hardware store owner whom she has been having an affair with for several months. Unsure about everything now, Frank finally apologizes to Mickey and Francis for giving them bad advice about life and love when his own wife was cheating on him behind his back. As a result, the three men decide to go out fishing aware that despite the failure of their love lives, they will always have each other. As they prepare the motorboat to cast off, only Mickey decides it would be better if he tried to have one last talk with Hope before she leaves America. Only their father has arranged a surprise guest - Hope. The movie ends with Hope asking to drive the boat, but Frank, aware that women are not allowed on board, says it is too soon for that.

==Cast==
* Edward Burns as Mickey Fitzpatrick
* Mike McGlone as Francis Franny Fitzpatrick
* Cameron Diaz as Heather Davis
* Jennifer Aniston as Renee Fitzpatrick
* Maxine Bahns as Hope 
* John Mahoney as Mr. Fitzpatrick
* Leslie Mann as Connie
* Amanda Peet as Molly
* Frank Vincent as Ron
* Anita Gillette as Carol
* Malachy McCourt as Tom
* Robert Weil as Mr. DeLuca
* Beatrice Winde as Older Woman
* Eugene Osborne Smith as Older Man
* Tom Tammi as Father John
* Raymond De Marco as Doorman
* Ron Farrell as Scott

==Reception==
Critical reception of the film was largely positive. Review aggregation website   and Lisa Schwarzbaum.   Chris Hicks said, "Better, however, are Diaz, lending charm to a character who could have been quite unsympathetic, and especially Aniston, whose decent, trusting character is quite appealing. Best of all, however, is John Mahoney, hilarious as the bombastic patriarch of the Fitzpatrick clan, who refers to his sons as sisters and calls them Barbara or Dorothy while offering ill-advised sarcasm in place of fatherly wisdom."  For Louise Keller, "Burns, Diaz, Bahns and Aniston inject an energy and charisma of their own, and theyre fun to watch." Paul Fischer found that Diaz and Aniston are "both in fine form".  Alison Macor said, "As Francis wife Renee, Aniston provides one of the few bright spots in Shes the One. Playing Renee as the wry voice of sanity among the rest of the characters, Aniston shows that shes the one who makes this film somewhat enjoyable." 

Critics were less forgiving of Maxine Bahns. Mick LaSalle said about her, "The graduate student, Hope, is played by Maxine Bahns, Burns real-life girlfriend, who was also his love interest in McMullen. Throw a rock out the window, and its sure to hit someone with more acting talent than Bahns. She cant say a line without it ringing false and keeps smiling nervously, like a shy person at a party. In a way, its rather sweet that Burns keeps casting Bahns. But Shes the One would have been much improved had Burns given Jennifer Aniston the Bahns role. Instead Aniston is wasted here as the unloved wife of Mickeys callous brother, Francis (Mike McGlone of McMullen). She looks great, but all she gets to do is whine and smoke, and she all but disappears two-thirds of the way into the film." 

==Music== soundtrack album leaves out most of the instrumental music featured in the film, and includes a number of songs that are not in the film, or are only heard playing dimly in the background. Many of the songs were written (and some recorded) for Pettys 1994 success Wildflowers (Tom Petty album)|Wildflowers. Singles released from the soundtrack included two versions of the song "Walls" (featuring Fleetwood Macs Lindsey Buckingham), "Climb that Hill" and a song written by Lucinda Williams, "Change the Locks". The album also included a cover of "Asshole", a song by Beck. The album is referenced by Snoop Dogg in Katy Perrys song "California Gurls" in tribute to Malibu, California, where Tom Petty resides.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 