Rogues of Sherwood Forest
{{Infobox film
| name           = Rogues of Sherwood Forest
| image          = Poster of the movie Rogues of Sherwood Forest.jpg
| image_size     =
| caption        = Gordon Douglas
| producer       =
| writer         =
| narrator       =
| starring       = John Derek Diana Lynn George Macready Alan Hale, Sr. Heinz Roemheld Arthur Morton
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
}} Gordon Douglas Douglas Fairbanks Errol Flynn in 1938, one of the longest period over which any film actor played the same major role (Sylvester Stallone played Rocky Balboa over an even longer span, between 1976 and 2006.) It was also Hales final film. The film was written by Ralph Gilbert Bettison and George Bruce.

==Plot==
In its view on history, evil King John resumes his old ways after the death of Richard the Lionheart with the plan to keep his power by importing Continental mercenaries and paying them through oppressive taxation. King John first attempts to kill the son of his old nemesis Robin.  His henchmen fix a faulty protective cap to the lance of a Flemish Knight who challenges Robin in a joust.  When Robin survives the lance attack he challenges his opponent to a joust without protective devices, impaling the Flemish Knight.

Having returned from the Crusades, Robin and Little John re-recruit the aging Merrie Men who wage a guerilla type war throughout the realm with intelligence provided by Lady Mariannes carrier pigeons.
 seal the Magna Carta. 

==Production==
Gig Young was the first choice for the role but was suspended by Columbia when he refused to play Robin Hood.  The film was shot at the Corriganville Movie Ranch. 

==Cast==
* John Derek as Robin Hood
* Diana Lynn as Lady Marianne de Beaudray King John
* Alan Hale, Sr. as Little John
* Paul Cavanagh as Sir Giles Count of Flanders
* Billy House as Friar Tuck
* Lester Matthews as Alan-a-Dale
* Billy Bevan as Will Scarlet (billed as William Bevan) Baron Fitzwalter
* Donald Randolph as Archbishop Stephen Langton Matthew Boulton as Abbot

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 