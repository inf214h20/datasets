Ōsama Game
{{Infobox film
| name           = Ōusama Game
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Norio Tsuruta
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Airi Suzuki   Yurina Kumai   Dori Sakurada
| music          = 
| cinematography = 
| editing        = 
| studio         = BS-TBS   CBS
| distributor    = MGM Home Entertainment (Current)
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a   (pen name: Pakkuncho), consisting of five volumes.　A film based on the novel was released in 2011, and directed by Norio Tsuruta.  The theme song of the film is "Amazuppai Haru ni Sakura Saku" by Berryz Kobo and Cute (Japanese band)|Cute. 

==Synopsis==
Ōsama Game 
On October 19, a class of 32 students receive a message from a mysterious sender called "King" at midnight, giving them a set of rules and an order.
 Ōsama Game Climax 
Being the only survivor of the Ōsama Game, Nobuaki transfers to a new school, thus starting the tragedy once more.
 Ōsama Game Presence 
Set before the events of Climax, about Natsukos Ōsama Game before she transferred.

==Rules==
# All students in the class shall participate.
# After receiving an order from the King, the order must be completed within 24 hours.
# People who do not complete their order shall be punished.
# Withdrawing from the game is not allowed.

==Orders==
Ōsama Game
 1st Order: Male student Seat No.4 Inoue Hirofumi, Female student Seat No.19 Nakao Minako. These two people must kiss.  
 2nd Order: Male student Seat No.18 Toyoda Hideki, Female student Seat No.5 Imoto Yuuko. Toyoda Hideki must lick Imoto Yuukos foot.  
  3rd Order: Male student Seat No.18 Toyoda Hideki, Female student Seat No.3 Ishii Satomi. Toyoda Hideki must touch Ishii Satomis breasts.  
  4th Order: Male student Seat No.17 Tasaki Daisuke, Female student Seat No.20 Nakajima Misaki. The two people must make love.  
  5th Order: Male student Seat No.30 Yahiro Shouta. Yahiro Shouta must give an order in front of everybody. The person who receives the order, must follow it, as if it was an order from the King.
  Shoutas Order: Male student Seat No.17 Tasaki Daisuke. Tasaki Daisuke must hang himself.  
  6th Order: Male student Seat No.21 Hashimoto Naoya, Female student Seat No.9 Ueda Kana. The entire class must hold a Friendship Vote for these two. The side who has the least votes shall be punished. If the Friendship Vote is not held, both sides will be punished. ※ Votes cannot be passed.  
  Extra order: The Kings order must be completed within 5 minutes. Ueda Kana did not complete her order, but she is no longer in this world, and therefore is unable to complete it. As a result, the person who was also involved in the order Hashimoto Naoya will be punished. Male student Seat No.21 Hashimoto Naoya must find somemone to make love to. If this order is not executed, the punishment is spontaneous combustion.  
 7th Order This game involves all male students. Prepare 100 pieces of paper, write 1-100 on them. Going by the seat numbers of the male students, starting from No.1, each person may choose to draw 1-3 cards, and thus continuing, the person who draws the person who draws card no.100 will be punished. If the game is not conducted, all male students will be punished. Please enjoy the game. ※The punishment this time is a heart attack.  
 8th Order Female student Seat No.22 Hirano Nami. Hirano Nami must give herself an order. Following the order, is equivalent to following the Kings order.  
 9th Order Male student Seat No.12 Kanazawa Nobuaki. You must lose your most important possession.  
 10th Order Male student Seat No.1 Adachi Shingo, Female student Seat No.15 Kinoshita Akemi. These two must each send two messages saying   to their classmates. The classmates who receive the message will die an accidental death.  
 11th Order All students in the class need to pay attention. People who do any unneeded action within the Kings game,will be punished. Also, Male student Seat No.28 Mizuuchi Yusuke, Heart attack. Male student Seat No.30 Yahiro Shouta, death by suffocation. Female student Seat No.15, drowned to death. These people have broken the rules of the game.
 12th Order A student in the class must roll a die. Depending on the number rolled, the person who rolled the die must specify the same number of classmates. The person who rolled the die, as well as the people specified, will all be punished.  ※ If the die isnt rolled, or nobody is specified, then everyone in the class shall be punished. ※ Within five minutes after rolling the die, following the number rolled, specify the same number of classmates. The method to specify, is to have the person who rolled the die send a message to the specified people. Sending a message to someone who is already dead is invalid.
 12th Order Amendment Within five minutes from now, following the number rolled, specify the same number of classmates. The method to specify, is to say their name. Specify immediately.  
 13th Order Male student Seat No.12 Kanzaki Nobuaki must kill Honda Chiemi with his own hands. Also, the following participants will be punished. Female student Seat No.6 Iwamura Rie, punishment by decacipation.  These people have broken the rules of the game.
 Ōsama Game Climax

==Characters==
Ōsama Game
* Seat No.1 Adachi Shingo  
* Seat No.2 Abe Toshiyuki  
* Seat No.3 Ishii Satomi  
* Seat No.4 Inoue Hirofumi  
* Seat No.5 Imoto Yuuko  
* Seat No.6 Iwamura Rie  
* Seat No.7 Iwamoto Maki  
* Seat No.8 Ueda Yosuke
* Seat No.9 Ueda Kana  (Committed suicide by jumping out of a window before punishment was announced, was not punished as a result.)
* Seat No.10 Ushiji Motoki  
* Seat No.11 Ohno Akira  
* Seat No.12 Kanazawa Nobuaki  
* Seat No.13 Kawakami Yuusuke  
* Seat No.14 Kawano Chia  
* Seat No.15 Kinoshita Akemi  
* Seat No.16 Shirokawa Mami  
* Seat No.17 Tasaki Daisuke  
* Seat No.18 Toyoda Hideki  
* Seat No.19 Nakao Minako  
* Seat No.20 Nakajima Misaki  
* Seat No.21 Hashimoto Naoya  
* Seat No.22 Hirano Nami  (Committed suicide by jumping off a cliff.)
* Seat No.23 Fujioka Toshiyuki  
* Seat No.24 Honda Chiemi  
* Seat No.25 Matsushima Yoshifumi
* Seat No.26 Matsumoto Masami  
* Seat No.27 Maruoka Kaori  
* Seat No.28 Mizuuchi Yusuke  
* Seat No.29 Miyazaki Emi  
* Seat No.30 Yahiro Shouta  
* Seat No.31 Yamaguchi Hiroko  
* Seat No.32 Yamashita Keita  

==Cast==
* Airi Suzuki
* Yurina Kumai
* Dori Sakurada
* Hisanori Satō
* Risako Sugaya
* Chisato Okai
* Maasa Sudo
* Chinami Tokunaga
* Momoko Tsugunaga Saki Nakajima
* Miyabi Natsuyaki
* Saki Shimizu
* Saki Mori
* Miharu Tanaka
* Yoshihiko Hosoda
* Satoshi Tomiura
* Ayumu Satō
* Kensuke Ōwada
* Maimi Yajima
* Mai Hagiwara
* Hitomi Yoshizawa

==References==
 

==External links==
*    

 

 
 
 
 
 
 
 