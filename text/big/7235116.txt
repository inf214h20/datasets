Someone to Watch Over Me (film)
{{Infobox film
| name           = Someone to Watch Over Me
| image          = Someone to Watch Over Me poster.jpg
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = Thierry de Ganay Mimi Polk Gitlin Harold Schneider
| writer         = Howard Franklin
| starring       = Tom Berenger Mimi Rogers Lorraine Bracco
| music          = Michael Kamen
| cinematography = Steven B. Poster
| editing        = Claire Simpson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $17 million
| gross          = $10,278,549
}} romance crime crime thriller George and Ira Gershwin song from which the film takes its title, here sung by Sting (musician)|Sting, and Vangelis "Memories of Green", originally from Scotts Blade Runner.

==Plot==
Socialite Claire Gregory (Mimi Rogers) attends a party and art show sponsored by one of her oldest friends, Winn Hockings (Mark Moses). Accompanying her is her straitlaced boyfriend, Neil Steinhart (John Rubinstein). In another part of town, there is another party, this one for newly appointed detective Mike Keegan (Tom Berenger).

Winn is accosted by a former partner, Joey Venza (Andreas Katsulas), who is angry because Winn had not come to him to borrow money for his new art studio. After a short argument, he stabs Winn to death. Claire witnesses the killing as she steps out of the elevator; she screams and is spotted by Venza. He pursues her, but she manages to get back into the elevator just in time.

The police are called in and the new detective Keegan is there. He is a married man, but immediately falls for Claire.  Along with fellow cops, he is assigned to protect Claire until she can make a positive ID of Venza (once he is arrested) and testify in court.

Keegan is determined to protect Claire and goes to extremes to do so. Venza makes numerous threats and attempts on her life, nearly succeeding at one point. Keegan and his wife Ellie (Lorraine Bracco) separate over his involvement in the case. He and Claire acknowledge their love but Keegan cannot bring himself to simply abandon his family.

At the end, Venza, who draws out Keegan by taking his son hostage, is shot by Ellie and killed.  Claire breaks up with her staid boyfriend and intends to go to Europe to get over Keegan, who returns to his wife and son.

==Cast==
*Tom Berenger as Det. Mike Keegan
*Mimi Rogers as Claire Gregory
*Lorraine Bracco as Ellie Keegan
*Jerry Orbach as Lt. Garber
*John Rubinstein as Neil Steinhart
*Andreas Katsulas as Joey Venza
*Tony Di Benedetto as T.J.
*Jim Moriarty as Koontz
*Mark Moses as Win Hockings
*Daniel Hugh Kelly as Scotty

==Reception==
Someone to Watch Over Me earned positive reviews from critics, and it currently holds a 76% rating on Rotten Tomatoes based on 25 reviews with the consensus stating: "Its plot is sometimes hard to swallow, but some fine acting and director Ridley Scotts stylish visual flair make Someone to Watch Over Me an engaging police thriller".

===Box office===
Someone to Watch Over Me was a box office disappointment despite positive reviews, bringing in a total of $10,278,549 in a limited run at the theater.  

==Home media==
The film did better on VHS. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 