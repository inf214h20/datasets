Tim Tyler's Luck (serial)
{{Infobox Film
| name           = Tim Tylers Luck
| image          = TimTylersLuck JunglePirates.jpg
| image_size     = 
| caption        = 
| director       = Ford Beebe Wyndham Gittens
| producer       = Henry MacRae Elmer Tambert (associate)
| writer         = Wyndham Gittens Norman S. Hall Ray Trampe Lyman Young
| narrator       =  Frances Robinson Norman Willis
| music          = William Schiller Clifford Vaughan
| cinematography = Jerome Ash
| editing        = Saul A. Goodkind (supervisor) Joseph Gluck Louis Sackin Alvin Todd
| distributor    = Universal Pictures 1937
| runtime        = 12 chapters (212 min)
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial movie serial based on the comic strip Tim Tylers Luck.

==Plot==
Tim Tyler stows away on a ship bound for Africa to find his father, Professor James Tyler.  He meets, and is joined by, Lora Lacey, who is chasing the criminal "Spider" Webb, the man responsible for framing her brother.

==Cast==
* Frankie Thomas as Tim Tyler Frances Robinson as Lora Lacey, posing as Lora Graham
* Jack Mulhall as Sargeant Gates
* Al Shean as Professor James Tyler, Tims father Norman Willis as "Spider" Webb
* Anthony Warde as Garry Drake Earl Douglas as Jules Lazarre
* William Billy Benedict as Spud Frank Mayo as Jim Conway Alan Gregg as Brent, one of Spiders henchman
* Stanley Blystone as Captain Clark
* Everett Brown as Mogu, Spiders native henchman
* Skippy as Ju Ju, the Chimp

==Critical reception==
Stedman considers this to be perhaps the best of Universals "Jungle Thrillers."  Tim Tylers Luck has good direction and convincing performances.  The serial has quiet moments balancing the action, which was rare for a serial.  The characterization is more nuanced then "might have been expected in an action serial". {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5 
 | pages = 119
 | chapter = 4. Perilous Saturdays
 }} 

==Chapter titles==
# Jungle Pirates
# Dead Mans Pass
# Into the Lions Den
# The Ivory Trail
# Trapped in the Quicksands
# The Jaws of the Jungle
# The King of the Gorillas
# The Spider Caught
# The Gates of Doom
# A Race for Fortune
# No Mans Land
# The Kimberley Diamonds
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 219
 | chapter = Filmography
 }} 

==DVD release==
All 12 chapters were released on a single DVD on February 28, 2006 with special features including a 2005 interview with Frankie Thomas, bios, the original theatrical trailer, and bonus classic cliffhanger trailers.

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial  Radio Patrol (1937)
| years=Tim Tylers Luck (1937)
| after=Flash Gordons Trip to Mars (1938)}}
 

 

 
 
 
 
 
 
 
 
 
 