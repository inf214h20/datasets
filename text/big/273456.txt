The Love Bug
 Love Bug}}
{{Infobox film| name =The Love Bug
  | image =Lovebugmviepstr.jpg
| caption =  Robert Stevenson Bill Walsh Bill Walsh Don DaGradi Dean Jones Michele Lee David Tomlinson Buddy Hackett
  | music = George Bruns Edward Colman
  | editing = Cotton Warburton Walt Disney Productions Buena Vista Distribution
  | released =    (limited)   
  | runtime = 108 minutes
  | country = United States
  | language = English
  | budget = $5 million 
  | gross    = $51,264,000 
}}
 series of Walt Disney anthropomorphic pearl-white, Beetle named Herbie.  It was based on the 1961 book Car, Boy, Girl by Gordon Buford.
 Dean Jones), love interest, auto showroom SCCA national champion who sells Herbie to Jim and eventually becomes Jims racing rival.

==Plot== grand theft. A heated argument between Jim and Thorndyke is settled when Carole persuades Thorndyke to drop the charges if Jim buys the car on a system of monthly payments.
 Riverside later that month. Jim accepts, and despite Thorndykes underhanded tactics, he and Herbie take victory. Over the next few months they go on to become the toast of the Californian racing circuit, while Thorndyke suffers increasingly humiliating defeats. Thorndyke finally snaps, and persuades Carole to take Jim out on a date while he sneaks round to Jims house. After getting Tennessee drunk on his own Irish coffee recipe, Thorndyke proceeds to tip the remainder of the whipped cream into Herbies gasoline tank. At the following days race, an apparently hungover Herbie shudders to a halt and backfires while Thorndyke blasts to victory. However, as the crowd admires Thorndykes victory, Herbie blows some cream from the coffee out of his exhaust pipe, covering Thorndyke.

 
 road race, launch himself compensation that Jim can no longer afford. Using the Chinese he learned while in Tibet, Tennessee tries to reason with Wu, and learns that he is a huge racing fan who knows all about Jim and Herbies exploits. Wu is willing to drop the charges in exchange for becoming Herbies new owner. Jim agrees to this, as long as Wu allows him to race the car in the El Dorado. If Jim wins, Wu will be able to keep the prize money but has to sell Herbie back for a US dollar|dollar. Wu replies to this proposal in clear English: Now you speak my language!.
 Sierra Nevada mountains from Yosemite Valley and back. Before the start of the race, Thorndyke persuades Mr. Wu to make a wager with him on its outcome. Thorndyke (with his assistant Havershaw acting as co-driver) pulls every trick in the book to ensure he and his Thorndyke Special are leading at end of the first leg of the race. As a result of Thorndykes shenanigans, Jim (with Carole and Tennessee as co-drivers) limps home last with Herbie missing two wheels and having to use a wagon wheel to get to the finish line. Despite Tennessees best efforts, it looks as if Herbie will be unable to start the return leg of the race the following morning. Thorndyke then arrives and claims that this makes him the new owner of the car. Wu regretfully tells Jim of the wager and that in accordance with its terms this is true. Thorndyke, thinking he is Herbies new owner, gloats to Jim about what hes going to do to Herbie and kicks Herbies front fender, but Herbie then unexpectedly lurches into life and chases Thorndyke from the scene, showing he is more than willing to race on. Thanks to some ingenious shortcuts, Jim is able to make up for lost time in the second leg and is neck and neck with Thorndyke as they approach the finish line. In the ensuing dogfight, Herbies hastily welding|welded-together body splits in two. The back half of the car (carrying Tennessee and the engine) crosses the line just ahead of Thorndyke, while the front (carrying Jim and Carole) rolls over the line just behind, meaning Herbie takes both first and third place.

In accordance with the terms of the wager, Mr. Wu takes over Thorndykes car dealership (hiring Tennessee as his assistant), while Thorndyke and Havershaw are relegated to lowly mechanics.  Meanwhile, a fully repaired Herbie chauffeurs the newlywed Jim and Carole away on their honeymoon.

==Cast== Dean Jones .... James "Jim" Douglas
*Michele Lee .... Carole Bennet
*David Tomlinson .... Peter Thorndyke
*Buddy Hackett .... Tennessee Steinmetz Joe Flynn .... Havershaw
*Benson Fong .... Mr. Wu
*Joe E. Ross .... Detective
*Barry Kelley .... Police sergeant
*Iris Adrian .... Carhop
*Gary Owens .... Announcer

==Production notes==

===Story and development===
 Dean Jones Disney film produced under Walt Disneys involvement, just two years after his death in 1966. Although Jones tried to pitch him a serious, straightforward film project concerning the story of the first sports car ever brought to the United States, Walt suggested a different and much better car story for him, which was Car, Boy, Girl, a story written in 1961 by Gordon Buford.

Car, Boy, Girl, The Magic Volksy, The Runaway Wagen, Beetlebomb, Wonderbeetle, Bugboom and Thunderbug were among the original development titles considered for the film before the title was finalized as The Love Bug.

Herbie competes in the Monterey Grand Prix, which, except for 1963, was not a sports car race. The actual sports car race held at Monterey was the Monterey Sports Car Championships.
 Apollo GT, Buick V8 engine.  This car exists today, is in the hands of a private collector, and has been restored as it was seen in the movie with its yellow paint and number 14 logo. 

==="Herbie"===
 MG and a pearl white Volkswagen Beetle. The Volkswagen Beetle was chosen as it was the only one that elicited the crew to reach out and pet it.

The Volkswagen brand name, logo or shield does not feature anywhere in The Love Bug, as the automaker did not permit Disney to use the name. The only logo can be briefly seen in at least two places, however. The first instance is on the brake pedals during the first scene where Herbie takes control with Jim inside (on the freeway/when Herbie runs into Thorndykes Rolls Royce), and in fact it is shown in all the future scenes when Jim is braking. The second instance is on the ignition key, when Jim tries to shut down the braking Herbie. The later sequels produced, however, do promote the Volkswagen name (as sales of the Beetle were down when the sequels were produced).
{{MLBBioRet Align  = Right Image  = LAret53.PNG Name   = Donald Drysdale Number = 53 Team   = Los Angeles Dodgers Year   = 1984
|}}
 skits about a ski instructor named Klaus, who speaks with a German accent as he introduces his fellow ski instructors, who are named Hans, Fritz, Wilhelm, and Sandor. At the end of the skit, Hackett would say "If you aint got a Herbie (pronounced "hoy-bee"), I aint going."
 Bill Walsh, who was a fan of Los Angeles Dodgers baseball player Don Drysdale (Drysdales jersey number, later retired by the team, was 53).

Walsh also gave Herbie his trademark red, white and blue racing stripes presumably for the more patriotic color and came up with the films gags such as Herbie squirting oil and opening the doors by himself. 
 SAE at   in factory configuration (though only   by the European DIN system which measured engine output as installed in the car with cooling fan and exhaust system attached)

Herbie has his own cast billing in the closing credits, the only time this was done in the entire series of films.

Today, only a handful of the original Herbie cars are known to exist. Car #10 was recovered from a warehouse in Pennsylvania, and has been preserved-still sporting its original paint from the movie. 

===Deleted scenes===

The bonuses on the DVD provide two deleted scenes named "Used Car Lot" and "Playground".
 used car script and a single black-and-white photograph of Jim talking with the salesman at the lot.

An unfilmed scene at the end of the story that was scripted and storyboarded was to have shown Herbie playing with children at a nearby playground prior to taking the newly married Jim and Carole off on their honeymoon.

===Stock footage===
 footage from the film Fireball 500. Parts of this scene can also be found in a 1966-model year dealer promotional film by Chevrolet, titled Impact 66.

===Shooting locations===
 Willow Springs, California.
 

===Cast and crew===

Andy Granatelli, who was popular at the time as a presence at the Indianapolis 500 as well as the spokesman for STP (motor oil company)|STP, appears as himself as the racing association president. Announcer Gary Owens (of Laugh-In fame) and reporter Chick Hearn also appear as themselves. The driving scenes were choreographed by veteran stunt man Carey Loftin 
 Bob Drake, Robert James, Bob Harris, Jack Perkins, Fred Stromsoe, Ronnie Rondell, and Kim Brewer.

===Promotion===

During one scene in the movie, Herbie has lost one of his wheels, and Tennessee is hanging out of the passenger side door to balance him. The door opens, and there is no "53" logo on the door. This image was used heavily to promote the film.

==Reception== third highest-grossing review aggregate website Rotten Tomatoes. 

==Legacy==
Four theatrical sequels followed:  . Some parts of the racing sequences from The Love Bug were later reused for Herbies dream sequence in Herbie Rides Again, responding to Grandma Steinmetzs telling Willoughby Whitfield that Herbie used to be a famous racecar.

A five-episode TV series,  , was released on June 22, 2005, by Walt Disney Pictures.
 Walt Disney Worlds All-Star Movies Resort in Orlando, Florida, Herbie has been immortalized in the "Love Bug" buildings 6 and 7.

==Home Media==
The Love Bug was released on VHS on March 4, 1980. It was re-released on September 11, 1991 and on October 28, 1994 with Herbie Rides Again. The film was soon re-released again on September 16, 1997 along with the entire Herbie The Love Bug film series. It was released on DVD for the first time on May 20, 2003.
It was released again with its sequels on a four movie collection in 2012. No Blu-ray release for the film has been announced.

==Note==

In the scene in which Herbie competes at Laguna Seca, the banner over the start/finish line reads "Monterey Grand Prix". The 1968 Monterey Grand Prix was a Can Am Series race, and did not feature production cars.

==References==
 

==External links==
 
 
*   
* 
* 
* 
* 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 