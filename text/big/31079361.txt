Wings of the Morning (film)
{{Infobox film
| name           = Wings of the Morning
| image          = 
| alt            = 
| caption        = 
| director       = Harold D. Schuster
| producer       = Robert Kane Tom Geraghty John Meehan Annabella   Henry Fonda   Leslie Banks
| music          = Arthur Benjamin   Muir Mathieson
| cinematography = Ray Rennahan James B. Clark
| studio         = New World Pictures 
| distributor    = Twentieth Century Fox (UK)   Héraut Film (France)
| released       = January 1937 (UK) 11 March 1937 (US)
| runtime        = 89 mins
| country        = United Kingdom
| language       = English
| budget         = $500,000
| gross          = 
}} British drama film directed by Harold D. Schuster and starring Annabella (actress)|Annabella, Henry Fonda, and Leslie Banks.  Glenn Tryon was the original director but he was fired and replaced by Schuster. It was the first ever three-strip technicolor movie shot in England or Europe. 
 John McCormack Jane and Peter Fonda, on the set at Denham Studios|Denham. 

==Plot summary==
The story, set in the late 1880s, concerns the tempestuous love between an Irish nobleman and the fiery Spanish gypsy he loves. 

==Cast== Annabella as Young Marie/Maria, Duchess of Leyva 
* Henry Fonda as Kerry Gilfallen 
* Leslie Banks as Lord Clontarf 
* Stewart Rome as Sir Valentine 
* Irene Vanbrugh as Old Marie 
* Harry Tate as Paddy 
* Helen Haye as Aunt Jenepher 
* Edward Underdown as Don Diego (as Teddy Underdown)
* Teddy Underdown as Don Diego Mark Daly as James Patrick Aloysius Jimmy Brannigan
* Sam Livesey as Angelo
* E.V.H. Emmett as Racing Commentator
* R.C. Lyle as Racing Commentator (as Captain R.C. Lyle) John McCormack as Himself - the Tenor
* Steve Donoghue as Himself
* Evelyn Ankers as a party guest (uncredited)
* Hermione Darnborough as a gypsy dancer

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 