Karunyam
{{Infobox film
| name           = Karunyam
| image          = 
| caption        = 
| director       = A. K. Lohithadas|A.K Lohithadas
| producer       = Azeez Auseppachan
| writer         = A. K. Lohithadas|A.K Lohithadas Murali Divya Unni Nedumudi Venu bgm  Johnson
| cinematography = Ramachandra Babu
| editing        = 
| released       =  
| studio         = 
| distributor    = Kavyachandrika Release
| country        = India
| runtime        = 
| language       = Malayalam
| budget         = 
| gross          = 
}} Malayalam family Murali and Divya Unni in the leading roles. The film upon release, was only a moderate success but was critically acclaimed and is considered to be one of the best films written by Lohithadas. The film discusses about the mishaps faced by an unemployed man in his family. It is considered as one among the best scripts written by AK Lohithadas. The major portions of the film was shot at Lakkidi,Palakkad.
The performances from the lead cast was well appreciated, especially that of Murali, who plays the complex character of a loving but stressful father, who sacrifices everything for his family. It is considered to be one of the finest performances from Murali.

==Cast==

* Jayaram as Satheeshan Murali as Gopi
* Divya Unni as Indu Janardhanan as K. K. Nair
* Nedumudi Venu as Sukumaran
* Rahna as Anu Kalabhavan Mani" as Rajan
* Chandini as Jayasree Sreenivasan as Gopalakrishnan
* Salu Kuttanadu as Narayanan

==Satellite Rights==

Sun Network holds the satellite rights of the movie
==External links==
*  

 
 


 