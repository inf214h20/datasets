Full of Life
 

{{Infobox film
| name           = Full of Life
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Quine
| producer       = Fred Kohlmar
| based on       =  
| writer         = 
| narrator       = 
| starring       = Judy Holliday Richard Conte
| music          = George Duning
| cinematography = Charles Lawton Jr.
| editing        = Charles Nelson
| distributor    = Columbia Pictures
| released       = June 10, 1957
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.3 million (US rentals) 
}}

Full of Life is a 1956 film directed by Richard Quine. It stars Judy Holliday and Richard Conte. It was nominated for an award by the Writers Guild of America in 1957.  

==Plot summary==
[Writer Nick and his wife Emily are expecting their first child. When a necessary home repair proves too costly to afford, Nick must swallow his pride and visit his father, a proud immigrant stonemason with whom he has a difficult relationship, and ask him to do the work. Confronting the issues of religion and family tradition which have separated father and son causes Nick and Emily to reevaluate their lives and the things they value most. 

==Cast==
* Judy Holliday as Emily Rocco
* Richard Conte as Nick Rocco
* Salvatore Baccaloni as Papa Victorio Rocco
* Esther Minciotti as Mama Pauletta Rocco
* Joe De Santis as Father Gondolfo
* Silvio Minciotti as Joe Muto
* Penny Santon as Carla Rocco
* Arthur Lovejoy as Mr. Jameson
* Eleanor Audley as Mrs. Jameson
* Trudy Marshall as Nora Gregory
* Walter Conrad as John Gregory
* Sam Gilman as Dr. Atchison

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 