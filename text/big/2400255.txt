All the Right Moves (film)
{{Infobox film
| name           = All the Right Moves
| image          = All the Right Moves Poster.jpg
| caption        = Theatrical release poster Michael Chapman Stephen Deutsch Phillip Goldfarb Gary Morton
| writer         = Michael Kane
| starring = {{Plainlist|
* Tom Cruise
* Craig T. Nelson
* Lea Thompson
}} David Richard Campbell
| cinematography = Jan de Bont
| editing        = David Garfield Lucille Ball Productions
| distributor    = 20th Century Fox
| released       = October 21, 1983
| runtime        = 91 minutes
| country        = United States
| awards         = 
| language       = English
| budget         = 
| gross          =  $17,233,166 
| preceded_by    = 
| followed_by    = 
}}
 Michael Chapman football season in Johnstown, Pennsylvania, and Pittsburgh.

==Plot summary==
Stefan "Stef" Djordjevic (Tom Cruise) is a Serbian-American high school defensive back who is both gifted in sports and academics seeking a college football scholarship to escape the economically depressed small western Pennsylvania town of Ampipe and a dead-end job and life working at the mill like his father and brother Greg. Ampipe is a company town whose economy is dominated by the towns main employer, American Pipe & Steel, a steel mill struggling through the downturn of the early 1980s recession. 

Most of the film takes place after the big football game against undefeated Walnut Heights High School. Ampipe appears headed to win the game, when a fumbled handoff in the closing seconds—as well as Stefens pass interference penalty earlier in the game—leads to a Walnut Heights victory. Following the game, Coach Nickerson (Craig T. Nelson) lambastes the fumbler in the locker room, telling him he "quit". When Stefen retorts that the coach himself quit, the coach kicks him off the team.

In the aftermath, disgruntled Ampipe fans vandalize Coach Nickersons house and yard. Stefen is present and is a reluctant participant, but is nonetheless seen by Nickerson as the vandals flee. From there, Stefen deals with personal battles, including dealing with the coach blacklisting him among colleges because of his attitude and participation in the desecration of Nickersons yard and house. 

Stefen, frustrated by what Nickerson did, angrily confronts his former coach. In the end, Nickerson realizes he was wrong for blacklisting Stefen. He has accepted a coaching position at Cal Poly San Luis Obispo and offers Stefen a full scholarship for playing football there, which he accepts.

==Cast==
* Tom Cruise as Stefen "Stef" Djordjevic
* Craig T. Nelson as Nickerson
* Lea Thompson as Lisa Lietzke 
* Charles Cioffi as Pop
* Gary Graham as Greg
* Paul Carafotes as Salvucci
* Chris Penn as Brian
* Sandy Faison as Suzie
* James A. Baffico as Bosko
* Mel Winkler as Jess Covington
* Walter Briggs as Rifleman
* George Betor as Tank Leon as Shadow 
* Terry OQuinn as Freeman Smith
* Joseph J. Schultz as Steelworker (Uncredited)

==Production==
The production was filmed over seven weeks in Johnstown, Pennsylvania in mid 1983, co-produced by Lucille Ball and Phillip Goldfarb. A sixty-year-old, recently closed high school was used as the location of the film, along with the towns Point Stadium. Actress Lea Thompson was inserted as a new student at Ferndale Area High School for three days prior to shooting.  Tom Cruise was similarly inserted into Greater Johnstown High School but was recognized immediately. 

==Release==
The film has a score of 53% on Rotten Tomatoes based on 19 reviews    and a score of a generally favorable 62% on Metacritic based on 7 reviews. Jay Carr from The Boston Globe stated "Cruise is believable as an athlete," and Janet Maslin of The New York Times called it "a well-made but sugar-coated working-class fable about a football star."   

Among the unfavorable reviews, TV Guide called the movie "cliche-riddled" and Richard Corliss of Time (magazine)|Time called it a "naive little movie (that) hopes to prove itself the Flashdance of football."   

==See also==
* List of American films of 1983

==References==
 

==External links==
 
* 
* 
*  at TomCruise.com
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 