Losing to Win
 

{{infobox film name = Losing to Win director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier starring = Gene Gauntier Jack J. Clark cinematography = George Hollister
| released =  
| runtime = 1000 ft
| country = United States language = Silent film (English intertitles) 
}}

Losing to Win is a 1911 American silent film produced by Kalem Company. It was directed by Sidney Olcott with Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Diana Grant
* Jack J. Clark - Jack Carlyle

==Production notes==
The film was shot on board RMS Baltic and in New York and Ireland during summer 1911.

==References==
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  
* The Moving Picture World, Vol 9, p 683 and p 911. 
* The New York Dramatic Mirror, March 27, 1911. 
* Supplement to The Bioscope, November 9, 1911, p XXI.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 


 