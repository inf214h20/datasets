Code of the West (1925 film)
{{Infobox film
| name           = Code of the West
| image          = Code of the West (1925 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William K. Howard
| producer       = Jesse L. Lasky Adolph Zukor
| writer         = Zane Grey Lucien Hubbard David Butler George Bancroft Gertrude Short
| music          = 
| cinematography = Lucien N. Andriot
| editing        =  
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent David Butler, George Bancroft and Gertrude Short. The film was released on April 6, 1925, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Owen Moore as Cal Thurman
*Constance Bennett as Georgie May
*Mabel Ballin as Mary Stockwell
*Charles Stanton Ogle as Henry Thurman David Butler as Bid Hatfield George Bancroft as Enoch Thurman
*Gertrude Short as Mollie Thurman
*Lillian Leighton as Ma Thurman
*Eddie Gribbon as Tuck Merry Pat Hartigan as Cal Bloom
*Frankie Lee as Bud 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 