Existenz
 
{{Infobox film
| name           = eXistenZ
| image          = EXISTENZ.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = David Cronenberg
| producer       = David Cronenberg András Hámori Robert Lantos
| writer         = David Cronenberg
| starring       = Jennifer Jason Leigh Jude Law Ian Holm Don McKellar Callum Keith Rennie Sarah Polley Christopher Eccleston Willem Dafoe
| music          = Howard Shore
| cinematography = Peter Suschitzky
| editing        = Ronald Sanders Harold Greenberg UGC
| distributor    = Momentum Pictures   Alliance Atlantis   Dimension Films  
| released       =  
| runtime        = 97 minutes  
| country        = Canada United Kingdom France
| language       = English
| budget         = Canadian Dollar|CAD$31 million (United States dollar|USD$15 million )
| gross          = $2,856,712   Retrieved May 25, 2013 
}} science fiction body horror film written, produced, and directed by Canadian director David Cronenberg. It stars Jennifer Jason Leigh and Jude Law.
 psychological statement about how humans react and interact with the technologies that surround them. In this case, the world of video games.

András Hámori and  " in Hungarian language|Hungarian.  

== Plot ==
In the near-future, organic virtual reality game consoles known as "game pods" have replaced electronic ones. The pods are attached to "bio-ports," outlets inserted at players spines, through biotechnological umbilical cords. Two game companies, Antenna Research and Cortical Systematics, compete against each other. In addition, a group of "realists" fights both companies to prevent the "deforming" of reality.

Antenna Researchs Allegra Geller (Jennifer Jason Leigh), the worlds preeminent game designer, is testing her latest virtual reality game, eXistenZ, with a focus group at a seminar. She is shot in the shoulder by an assassin named Noel Dichter (Kris Lemche) with an organic pistol, which is undetectable by security. As Dichter is gunned down by the security team, security guard (and marketing trainee) Ted Pikul (Jude Law) rushes to Geller and escorts her outside.

Allegra discovers that her pod, which contains the only copy of the eXistenZ game, may have been damaged due to an "UmbyCord" being ripped out as the game was being downloaded. To test it, she must plug into the game in tandem with another player she can trust, and talks a reluctant Pikul into installing a bio-port in his own body so he can play the game with her. Pikul, one of a dwindling few who has refused to have a bio-port installed to this point, at first objects due to a phobia about "surgical penetration," but eventually gives in. They head to a gas station run by a black-marketeer named Gas (Willem Dafoe) to get it installed. Gas deliberately installs a faulty bio-port and the game pod is damaged. Gas (a shotgun in hand) reveals that he is going to kill Allegra for the bounty on her head, but Pikul shoots him with the rivet gun used to install the port.

The pair make their way to a former ski lodge used by Allegras mentor, Kiri Vinokur (Ian Holm). He and his assistant repair the damaged pod and give Pikul a new bio-port. Inside the game, Pikul realizes that it is hard to tell whether his or Allegras actions are their own intentions or the games. When they meet DArcy Nader (Robert A. Silverman), a video game shop owner, Pikul speaks rudely to him but then expresses surprise at his own rudeness. Allegra informs him that it was the doing of his game character.

Reality becomes more distorted when they use new micro pods given to them by Nader and gain newer identities as workers at a game pod factory. There, they meet Yevgeny Nourish (Don McKellar), who claims to be their contact in the Realist underground. Nourish recommends that Pikul order the special for lunch at a Chinese restaurant near the factory. Once in the restaurant, Pikul "pauses" the game in order to get back to the real world but then finds out that he is unable to distinguish presumed reality from illusion. Back in the restaurant, Pikul develops an urge to eat the unappetizing special which turns out to be an assortment of cooked mutant animals. Pikul constructs a familiar object from the inedible parts—the pistol originally used to shoot Allegra. He points it at her as a joke, but then Pikul identifies their Chinese waiter (Oscar Hsu) as an enemy and shoots him instead. In keeping with game parameters, the other patrons of the restaurant appear more frozen than shocked and return to their meals when Pikul tells them it was a simple misunderstanding about the bill.  When the pair return to the game store Hugo Carlaw (Callum Keith Rennie) informs them that Nourish is actually a double agent for Cortical Systematics. Thus the waiter Pikul murdered was the actual contact. The following day the two plan to sabotage all the game pods in the factory by plugging into a diseased pod. When Allegra becomes infected Pikul frees her by cutting the umbycord. But then Allegra almost bleeds to death before Nourish appears with a flame thrower, directing it at the diseased pod. The pod bursts, releasing deadly spores all over the factory. Before leaving, Allegra stabs Nourish in the back with a knife.

Allegra and Pikul suddenly find themselves back in the ski lodge, and it seems that they have lost the game.  They discover that Allegras game pod is also diseased. Pikul is confused by the diseases crossover from the game into reality. However, Allegra immediately notices Pikul rubbing his back and realizes that Vinokur gave Pikul an infected bioport in order to destroy her game. She inserts a disinfecting device into Pikuls bioport. Unexpectedly, Carlaw reappears as a Realist resistance fighter and escorts Allegra and Pikul outside to witness the death of eXistenZ. Before Carlaw can kill Allegra, he is shot in the back by Vinokur, who is a double agent for Cortical Systematics. He informs Allegra that he copied her game data while he was fixing her pod. In revenge, she kills Vinokur. Pikul then reveals that he himself was sent to kill her; he is a Realist. But she informs him that she knew of his true intentions ever since he pointed the gun at her in the Chinese restaurant. So she kills him, detonating the disinfecting device in his bioport by remote control.

In yet another unexpected twist, the two are then shown sitting on a stage together with the main players from the game.  It turns out that the story itself is in fact a virtual reality game called "tranCendenZ" played by the cast. This is enforced by more naturalistic acting coupled with significantly less formulaic dialogue along with cutaways from Allegra and Pikul. Another difference occurs when it is revealed that players are using electronic devices rather than game pods. The real game designer, Nourish, feels uneasy because the game started with the assassination of a game designer and had an overall anti-game theme that he suspects originated from the thoughts of one of the testers. Pikul and Allegra approach him (with Pikuls pet dog close by) and ask him if he should pay for his "crimes" of deforming reality. As Merle (Sarah Polley), Nourishs assistant, calls for security, Pikul and Allegra grab pistols hidden under the dogs false mane and shoot Nourish and Merle to death.  As in the restaurant scene, the other players appear more frozen than shocked. As Pikul and Allegra leave, they aim their guns at the person who played the Chinese waiter, who first pleads for his life, then asks if they are still in the game. The last shot is of Pikul and Allegra standing together in wide-eyed silence, apparently unsure of the answer.

==Cast==
* Jennifer Jason Leigh as Allegra Geller
* Jude Law as Ted Pikul
* Ian Holm as Kiri Vinokur
* Don McKellar as Yevgeny Nourish
* Callum Keith Rennie as Hugo Carlaw
* Sarah Polley as Merle
* Christopher Eccleston as the Seminar Leader
* Willem Dafoe as Gas
* Robert A. Silverman as DArcy Nader
* Oscar Hsu as Chinese Waiter
* Kris Lemche as Noel Dichter
* Vik Sahay as Male Assistant

==Production==
The films plot came about after Cronenberg conducted an interview with Salman Rushdie for Shift (magazine)|Shift magazine in 1995. At the time, Rushdie was in hiding due to a Fatwah being put on his life by Muslim extremists due to his controversial book The Satanic Verses. Rushdies dilemma gave Cronenberg an idea of "a Fatwah against a virtual-reality game designer". Existenz was originally set up at Metro-Goldwyn-Mayer, but they rejected the idea due to its complex structure. 

==Novelizations== Christopher Priest wrote the tie-in novel to accompany the movie Existenz, the theme of which has much in common with some of Priests own novels.
* In 1999, a graphic novel credited to David Cronenberg and Sean Scoffield was published.

==Reception==
The film received generally positive reviews, with a 71% approval rating at Rotten Tomatoes. 

===Awards=== Berlin Film Festival   
* Won, Silver Bear: David Cronenberg
* Nominated, Golden Bear: David Cronenberg

Amsterdam Fantastic Film Festival
* Won, Silver Scream: David Cronenberg

Genie Awards
* Won, Best Achievement in Editing: Ronald Sanders
* Nominated, Best Achievement in Art Direction/Production Design: Carol Spier, and Elinor Rose Galbraith
* Nominated, Best Motion Picture: David Cronenberg, Robert Lantos, and Andras Hamori
 Golden Reel Awards
* Nominated, Best Sound Editing in a Foreign Feature: David Evans, Wayne Griffin, Mark Gingras, John Laing, Tom Bjelic, and Paul Shikata

Saturn Awards
* Nominated, Best Science Fiction Film (lost to The Matrix)

==See also==
* Alternate reality game
* Game studies
* Simulated reality
* The Thirteenth Floor (1999 film)
* The Matrix (1999 film)

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 