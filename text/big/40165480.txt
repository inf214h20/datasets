Shravani Subramanya
{{Infobox film
| name           = Shravani Subramanya
| image          = Shravani Subramanya.jpg
| caption        = First look of the film
| director       = Manju Swaraj
| producer       = K. A. Suresh
| writer         = Manju Swaraj Ganesh Amulya
| music          = V. Harikrishna
| cinematography = B. Suresh Babu
| editing        = 
| studio         = Suresh Arts
| narrator       = Sudeep
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         =  
| gross          =  
}} Ganesh and Amulya in lead roles. This is the second collaboration of both stars after the 2007 blockbuster hit Cheluvina Chittara. Amulya makes her comeback after a hiatus through this film. 

The film features the score and soundtrack composed by V. Harikrishna. The principal photography of the film began at Mysore around the Lalit Mahal palace and has been progressing at a brisk pace in the same city.  The first teaser of the film was released on YouTube website by the producer on 29 June 2013.

==Synopsis==
The film is a romantic comedy where Ganesh plays the role of an aspiring singer and dreaming of making it big in the music industry. Amulyaplays the role of a simple girl and the love interest of Ganeshs character.

==Cast== Ganesh as Subramanya
* Amulya as Shravani
* Ananth Nag as Seetharam Tara as Anuradha
* Avinash as Kempegowda
* Yashas as Sudeep
* Sadhu Kokila
* Neenasam Ashwath
* Parul Yadav (Special appearance)
* Sudeep as Narrator (voice)    

==Production==

===Launch=== Tara do the other pivotal roles.

===Filming===
The film was shot in and around the Mysore city. The crew has shot in Lalith Mahal Road and St Philomena Church, among others. 

==Release==
The film was released worldwide. In karnataka it released around 95+ screens, and next week it added around 43 screens. It also released in USA Germany and UK. 

==Soundtrack==
{{Infobox album  
| Name        = Shravani Subramanya
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = Shravani Subramanya audio.jpeg
| Border      = yes
| Caption     = Soundtrack cover
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 16:52
| Label       = D Beats
}}

V. Harikrishna composed the music for the film and the soundtracks. The soundtrack album marks the re-entry of veteran playback singer Manjula Gururaj after ten years.  The album consists of four soundtracks. 

{{Track listing
| total_length   = 16:52
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Aakalbenne
| lyrics1 	= Prof. Krishne Gowda
| extra1        = Manjula Gururaj
| length1       = 3:45
| title2        = Kannalle Kannittu
| lyrics2 	= V. Nagendra Prasad Shaan
| length2       = 4:22
| title3        = Naguva Mogava Kaviraj
| Nanditha
| length3       = 4:15
| title4        = Ninna Nodo
| extra4        = Sonu Nigam Kaviraj
| length4       = 4:30
}}

==Critical response==
Shravani Subramanya received positive reviews from critics upon its release. B. S. Srivani of Deccan Herald gave the film a rating of four out of five and praised the roles of all the departments in the film and wrote, "Shravani seems to be tailor made for Amulya. She is the perfect foil for Ganesh, who is in fine form here."  G. Arun Kumar of The Times of India too gave the film a rating of four stars out of five and wrote, "Ganesh makes a lasting impression on the viewer with his effortless performance, it is Amulya who steals the show with her bubbly act."  Shyam Prasad S. of Bangalore Mirror gave the film a rating of 3.5/5 and praised the roles of directing, acting and the music departments in the film. 

==Box office==
Shravani Subramanya opened to decent occupation all over Karnataka. It collected around   in its first week after theatrical release and   after 100 days.  The film was one of the major blockbusters of 2013.  

==Home media==
Shravani Subramanya was released in DVD with Dolby Digital DTS 5.1 audio and with English subtitles. The movie was also released in 3 in 1 DVD and VCD.

==References==
 

==External links==
*  

 
 
 
 
 
 
 