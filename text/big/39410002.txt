The Voices (film)
 
{{Infobox film
| name           = The Voices
| image          = The Voices film poster.png
| alt            = 
| caption        = Film poster
| director       = Marjane Satrapi
| producer       = {{Plainlist|
* Matthew Rhodes
* Adi Shankar
* Roy Lee
* Spencer Silna}}
| writer         = Michael R. Perry
| starring       = {{Plainlist| 
* Ryan Reynolds
* Gemma Arterton
* Anna Kendrick
* Jacki Weaver}}
| music          = Olivier Bernet
| cinematography = Maxime Alexandre
| editing        = Stephanie Roche
| studio         = {{Plainlist|
* 1984 Private Defense Contractors
* Babelsberg Studio Mandalay Vision
* Vertigo Entertainment}}
| distributor    = Lions Gate Entertainment
| released       =  
| runtime        = 104 minutes  
| country        = {{Plainlist|
* United States
* Germany}}
| language       = English
| budget         = 
| gross          = $5,000 (North America) 
}}
The Voices is a 2014 black comedy drama film directed by Marjane Satrapi and written by Michael R. Perry. The film stars Ryan Reynolds, Gemma Arterton, Anna Kendrick, and Jacki Weaver. The film had its world premiere at 2014 Sundance Film Festival on January 19, 2014. 

The film was released on demand and in theaters in the United States on February 6, 2015.

==Plot==
Jerry (Ryan Reynolds) is that chipper guy clocking the nine-to-five at a bathtub factory, with the offbeat charm of anyone who could use a few friends. With the help of his court-appointed psychiatrist, he pursues his office crush (Gemma Arterton). However, the relationship takes a sudden, murderous turn after she stands him up for a date. Guided by his evil talking cat and benevolent talking dog, Jerry must decide whether to keep striving for normalcy, or indulge in a much more sinister path.

==Cast==
* Ryan Reynolds as Jerry Hickfang and the voices of Bosco, Deer, Mr.Whiskers,  and Bunny Monkey
* Gulliver McGrath as young Jerry
* Anna Kendrick as Lisa
* Gemma Arterton as Fiona
* Jacki Weaver as Dr. Warren
* Sam Spruell as Dave
* Adi Shankar as Trendy John Ella Smith as Alison
* Paul Chahidi as Dennis Kowalski
* Stanley Townsend as Sheriff Weinbacher
* Valerie Koch as Jerrys mother
* Paul Brightwell as Jerrys stepfather
* Alessa Kordeck as Sheryl
* Stephanie Vogt as Tina TV

==Production==
On March 5, 2014, Lions Gate Entertainment acquired the US distribution rights to the film. 

===Filming===
Principal photography began in April 2013 in Berlin, Germany. 

==Reception==
The Voices has received generally positive reviews. On Rotten Tomatoes, the film holds a rating of 74%, based on 39 reviews, with an average rating of 6.7/10. The sites critical consensus reads: "The Voices gives Ryan Reynolds an opportunity to deliver a highlight-reel performance -- and offers an off-kilter treat for fans of black comedies." 

==Release==
The film was released on video on demand and in limited release on February 6, 2015. In the films opening weekend the film made $5,000. 
==Awards and nominations==
* 20th annual LEtrange Festival in Paris bestowed two awards on the film: the Canal+ Nouveau Genre Award (the festivals Grand Prize) and the equally prestigious Audience Award.   
* 2015 Festival International du Film Fantastique de Gérardmer granted two more honors to the film: the Audience Award and the Jury Award. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 