On the Border (film)
{{Infobox film
| name           = On the Border
| image          = On_The_Border_1930_Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = William C. McGann
| writer         = Lillie Hayward (Screenplay) Armida John Litel Philo McCullough William Rees
| distributor    = Warner Bros.
| released       =  
| runtime        = 46 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Armida sings two songs in the picture.

==Synopsis==
Philo McCullough, as Farrell, is the head of a smuggling ring who are attempting to smuggle Chinese workers across the border from Mexico. McCullough stops at a hacienda, near the Mexican border, which is owned by Bruce Covington (as Don Jose). Covingtons dog, Rin Tin Tin, senses that something is hidden under the vegetables, which McCullough has in his trucks, and discovers the Chinese. McCullough attempts to buy the hacienda from Covington. McCullough also hopes to get Armida (as Pepita), Covingtons daughter, as part of the deal. Meanwhile, some border agents (John Litel and William Irving) disguised as tramps, discover McCulloughs plans. Armida and Rin Tin Tin take a liking to Litel. Meanwhile, Covington has innocently sold his hacienda to McCullough. When the smugglers find out who Litel real is, they capture him. Rin Tin Tin manages to save him at the last minute. The border patrol then surrounds the hacienda and as Farrell tries to escape in a car but Rin Rin Tin captures him in the nick of time.

==Cast==
*Rin Tin Tin as Rinty Armida as Pepita
*John Litel as Dave
*Philo McCullough as Farrell 
*Bruce Covington as Don José  Walter Miller as Border Patrol Commander  William Irving as Dusty

==Preservation==
A 16mm copy of the film is preserved at the Wisconsin Center for Film & Theater Research.  No 35mm copies are known to exist.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 