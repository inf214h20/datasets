Lottery Ticket (2010 film)
{{Infobox film
| name          = Lottery Ticket
| image         = Lottery ticket poster.jpg
| caption       = Theatrical poster
| director      = Erik White
| screenplay    = Abdul Williams
| story         = Erik White Abdul Williams
| producer      =   starring       =  
| music          = Teddy Castellucci
| cinematography = Patrick Cady
| editing        = Harvey Rosenstock
| studio         =   Warner Bros. Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $17 million    gross        =   $24,719,879   
}}
 Bow Wow, Brandon T. Jackson, Naturi Naughton, Keith David, Charlie Murphy, Gbenga Akinnagbe, Loretta Devine and Ice Cube in lead roles. The story follows a young man who wins a $370 million lottery, and soon realizes that people from the city are not his real friends, but are after his money.  The film released on August 20, 2010.

==Plot==
The film follows Kevin Carson ( ) and Stacey (Naturi Naughton).  He comes across Lorenzo (Gbenga Akinnagbe), the neighborhood bully. Meanwhile, everyone in his neighborhood is trying to win the Mondo Million Dollar Lottery of $370 million. Lorenzo demands that Kevin gives him and his three friends three sets of sneakers each. When Lorenzo shows up to Kevins job, and grabs the shoes, the alarms go off and the police arrive. after Kevin accidentally pay the shoes for Lorenzo, he attempts to explain to the police that he did not intend to give the shoes to Lorenzo, he is arrested and Kevin loses his job.
 Fourth of July weekend. News of Kevins winning ticket spreads, and the entire neighborhood swarms him and his home, begging for a cut of the money. Nikki Swayze (Teairra Marí) who previously rejected Kevin, suddenly develops an interest in him. This angers Stacey, who tells Kevin that Nikki is only after his money, but Kevin does not believe it. Kevin and Benny meet a loan shark, Sweet Tee, who gives Kevin $100,000 to go out and have fun. After her date with Kevin, Nikki tries to make Kevin get her pregnant, but Kevin does not want a baby and refuses to impregnate her. Nikki then reveals to Kevin that she only wanted the money. 

Kevin and Benny have an argument about the ticket which leads to them not speaking to each other. He goes to Staceys house, and she tells Kevin that she thinks he was wrong about the entire situation. He also tells her that she is the girl for him, which leads to her getting angry and telling him to get out. He kisses her and she responds accordingly, but they are interrupted by the arrival of her mother. As Kevin leaves her house, Lorenzo knocks him unconscious and takes his $5,000 sneakers and his ticket. He wakes up in the apartment of Mr. Washington (Ice Cube), who talks with him. Kevin also takes time to reconcile with Benny. In the end, the neighborhood has a block party. Kevin faces off against Lorenzo. On the verge of defeat, Kevin is saved by Mr. Washington, who comes and knocks Lorenzo unconscious. 

Months later, Kevin, Benny and Stacey board Kevins new helicopter and fly off to work at Kevins sneaker company.

==Cast== Bow Wow as Kevin Carson 
* Brandon T. Jackson as Benny
* Naturi Naughton as Stacey
* Loretta Devine as Grandma
* Ice Cube as Jerome "Thump" Washington
* Gbenga Akinnagbe as Lorenzo Mack
* Keith David as Sweet Tee
* Terry Crews as Jimmy
* Charlie Murphy as "Semaj" (James)
* Teairra Mari as Nikki Swazey
* Jason Weaver as Ray Ray Leslie Jones as Tasha
* Vince Green as Malik
* Malieek Straughter as Deangelo
* T-Pain as Junior
* Bill Bellamy as Giovanni Watson
* Mike Epps as Reverend Taylor
* IronE Singleton as Neighbor
* Lil Twist as Boy in neighborhood

== Reception ==
The film received generally mixed to negative response worldwide, garnering a 34% rating out of 79 reviews on Rotten Tomatoes,  with the critical consensus reading: "Theres a worthwhile message at the heart of Lottery Ticket, but its buried under stale humor, tired stereotypes, and obvious clichés." On Metacritic, the film holds a score of 50 out of 100, on 24 critics, indicating "mixed or average reviews".

==Soundtrack==
The following is a track listing of songs for the film, Lottery Ticket. Songs marked with an * are just songs that can be briefly heard in the film.

===Songs featured in the film===
#"Workin Man Blues" - Aceyalone  (featuring Bionik) 
#"Look At Me Now" - King Juju
#"Lord Rescue Me" - Jason Eskridge
#"If Youre Really Hood" - the Handlebars
#"What You Talkin About" - Classic
#"How Low" - Ludacris
#"I Make the Hood Look Good" - T-Drop
#"Tim & Bob Groove 1" - Tim & Bob
#"We Like to Party" - Ben and Family
#"Mysterious Love" - Lamar J and Deshawn Williams (of Take 2)
#"I Be Doin It" - Classic Envy
#"Gangsta Party" - Classic
#"Southside" - Johnny Ringo Chris Brown
#"Money (Thats What I Want) - Barrett Strong
#"Hallelujah (disambiguation) "*
#"All Your Bass" - T-Pain
#"Tim & Bob Groove 2" - Tim & Bob
#"Deez Hips" - Dem Naughty Boyz
#"Oh Happy Day" - Edwin Hawkins Singers
#"Whoa Now" - B Rich
#"Million Bucks" - Maino  (featuring Swizz Beatz) 
#"Tim & Bob Groove 3" - Tim & Bob
#"I Invented Sex" - Trey Songz  (featuring Drake (entertainer)|Drake) 
#"Standing in the Rain" - Al Green
#"Come By Here My Lord" - Tick Ticker
#"Un-Thinkable (Im Ready)" - Alicia Keys  (featuring Drake (entertainer)|Drake)  Let My People Go" - Darondo
#"Take Your Shirt Off" - T-Pain
#"Here to Party" - Classic Bow Wow  (featuring Sean Kingston) 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 