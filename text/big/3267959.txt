Sólo Con Tu Pareja
{{Infobox film
| name           = Sólo Con Tu Pareja
| image          = Sólo Con Tu Pareja DVD Cover.jpg
| caption        = Criterion Collection DVD cover
| producer       = Alfonso Cuarón
| director       = Alfonso Cuarón
| writer         = Carlos Cuarón Alfonso Cuarón
| starring       = Daniel Giménez Cacho   Claudia Ramírez
| music          = Carlos Warman
| cinematography = Emmanuel Lubezki
| editing        = Alfonso Cuarón   Luis Patlán
| distributor    = Warner Bros.
| released       = 1991
| runtime        = 94 minutes Spanish
}} Mexican film by Alfonso Cuarón.

This was the first full-feature film for Cuarón who had previously worked at Televisa, a Mexican television company. The stars of this film are Daniel Giménez Cacho (the narrator on Cuaróns second Mexican film Y Tu Mamá También) and telenovela star Claudia Ramírez.
 government of Mexicos IMCINE (Instituto Mexicano de Cinematografía) had already decided what films they would finance that year. Fortunately for the Cuaróns, one of the projects was canceled and the IMCINE funds were assigned to Sólo Con Tu Pareja.

After the film was completed, the Mexican government refused to distribute it, but Sólo Con Tu Pareja was presented at several international festivals. It won awards at the Ariel Awards (by the Mexican Academy of Film) and at the Toronto Film Festival. After this international recognition the movie was finally shown in 1993 in its country of origin where it became a box-office success. It occupies the 87th place on the list of the 100 best movies of the cinema of Mexico.

In 2006, Criterion Collection released Sólo Con Tu Pareja in DVD (Region 1), with a new remastered transfer and with the shorts Noche de Bodas by Carlos Cuarón and Cuarteto Para el Fin Del Tiempo (Alfonso Cuaróns first short, made when he was a film student in 1983).

==Plot==
Tomás Tomás (Daniel Giménez Cacho) is a young yuppie playboy and publicist. Silvia (Dobrina Liubomirova), a nurse the victim of one of his adventures, tries to get back at him by typing "positive" on his AIDS test. Tomás, trying to end his life, meets Clarissa (Claudia Ramírez), a flight attendant, who is also trying to kill herself after finding out her fiance is having an affair.

==Awards==
===Ariel Award in 1992===
*Best Original Story - Alfonso Cuarón & Carlos Cuarón

Also nominated for:
*Best Cinematography - Emmanuel Lubezki
*Best First Work - Alfonso Cuarón
*Best Screenplay - Alfonso Cuarón & Carlos Cuarón

==Cast==
* Daniel Giménez Cacho as Tomás Tomás
* Claudia Ramírez as Clarisa Negrete
* Luis de Icaza as doctor Mateo Mateos
* Dobrina Liubomirova as Silvia Silva
* Astrid Hadad as Teresa de Teresa
* Isabel Benet as Gloria Gold
* Toshirô Hisaki as Takeshi
* Carlos Nakasone as Koyi
* Ricardo Dalmacci as Carlos
* Claudia Fernández as Lucía
* Luz María Jerez as Paola
* Nevil Wilton as Pasmal John Keys as  Absolut Boozer
* Raúl Valerio as guard at the Torre Latinoamericana
* Montserrat Ontiveros as Mrs. Panza
* Regina Orozco as Mrs. Dolores
* Rodolfo Arias as Hernán Cortés on the commercial for "Jalapeños Caseros Gómez" Montezuma on the commercial for "Jalapeños Caseros Gómez"
* Claudette Maillé as la Burbus
* Ariel López Padilla as Burbus boyfriend
* Marcos Mohar as boy at the elevator
* Jiussana Briseño as Nancy of Continental Airlines
* Arturo Ríos as contratenor in a dream about the Mexicana de Aviación plane
* Adriana Olivera as voice of the beeper operator
* Jonás Cuarón as Kid at Wedding

==External links==
*     at the Cinema of Mexico page of the ITESM
*  
*  
*  

 

 
 
 
 
 
 
 
 