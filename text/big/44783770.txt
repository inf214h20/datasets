Samudram (1999 film)
{{Infobox film
| name           = Samudram
| image          =
| caption        =
| writer         = Shobhan Babu Srinivas  
| story          = Krishna Vamsi
| screenplay     = Krishna Vamsi 
| producer       = S.Bhagavan DVV Danayya
| director       = Krishna Vamsi Srihari Prakash Raj Ravi Teja
| music          = Shashi Preetam
| cinematography = S.K.A. Bhupathi
| editing        = Shankar
| studio         = Sri Balaji Art Creations
| released       =  
| runtime        = 167 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu Action film produced by S.Bhagavan & DVV Danayya on Sri Balaji Art Creations banner, directed by Krishna Vamsi. Starring Jagapati Babu, Sakshi Shivanand in the lead roles, Srihari (actor)|Srihari, Prakash Raj, Ravi Teja in pivotal roles and music also composed by Shashi Preetam.      

==Plot==
Sagar (Jagapathi Babu) is a fun loving guy, always have happy life with his widowed mother (Sudha), sister Chanti (Prathyusha), lover Rajya Lakshmi (Sakshi Sivanand) and along with his friends. Chepala Krishna (Tanikella Bharani) and his younger brother Chepala Nani (Ravi Teja) are the noted smugglers in Vizag. Chepala Krishna has a life time ambition to become an MLA. Sagar & Nani always have clashes from childhood.
 
C.I.Sri Hari (Srihari (actor)|Srihari) an honest Police Officer newly apponted to that area and  Krishnam Raju (Sivaji Raja) S.I. of same area is very loyal to him. Both of them gets to know about all the criminals in that area and Chepala Krishna tops the most wanted criminals list. When Chepala Krishna is about to file his nominations for the post of an M.L.A, he gets arrested by Sri Hari. Sri Hari makes sure that Chepala Krishna would not go to assembly by booking him as a rowdy sheeter. For that  Chepala Krishna develops enemity on Sri Hari. One day Sri Hari gets information that Chepala Nani is doing smuggling at a particular place. Sri Hari spots Nani and starts chasing him with the help of Krishnam Raju. At the same Sagars sister marriage arrangements are going on. Sagar goes to bazaar to buy something. As the chase gang enters a gully, Nani and Krishnam Raju kills Sri Hari. Sagar sees everything and try to rescue Sri Hari and he is implicated in the murder case and sentenced for his life. 

Nukaraju (Prakash Raj) is a humble constable, who is transferred to that area. He is an avid admirer of late Sri Hari. Sagars mother gets hospitalized when she resists Chepala Nani who tried to rape the sister of Sagar. Nukaraju is on the duty to accompany Sagar to the hospital to visit Sagars mom. There Sagar gives Nukaraju a slip and escapes from the hospital and he targets Chepala Krishna, Chepala Nani and Krishnam Raju. Remaining story how Sagar proofs himself as innocent.

==Cast==
 
*Jagapati Babu as Sagar
*Sakshi Shivanand as Rajyalakshmi
*Ravi Teja as Chepala Nani Srihari as C.I. Srihari
*Prakash Raj as Conistable Nukaraju
*Tanikella Bharani as Chepala Krishna
*Sivaji Raja as S.I. Krishnam Raju
*CVL Narasimha Rao as Lawyer
*Devadas Kanakala as Judge
*Rajababu as Rajyalakshmis father
*Uttej as Sagars friend
*Sudha as Sagars mother
*Prathyusha as Chanti Raasi as item number
*Delhi Rajeswari as Chepala Krishnas wife
 

==Soundtrack==
{{Infobox album
| Name        = 
| Tagline     = 
| Type        = film
| Artist      = Shashi Preetam
| Cover       = 
| Released    = 1999
| Recorded    = 
| Genre       = Soundtrack
| Length      = 23:58
| Label       = Supreme Music
| Producer    = Shashi Preetam
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Shashi Preetam. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 23:58
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Soniye Sirivennela Sitarama Sastry
| extra1  = Shashi Preetam
| length1 = 5:04

| title2  = Muddala Muthayame
| lyrics2 = Suddala Ashok Teja
| extra2  = Preethi
| length2 = 5:00

| title3  = Deeyo Deeyo 
| lyrics3 = Sirivennela Sitarama Sastry
| extra3  = Shashi Preetam
| length3 = 4:26

| title4  = Laila O Dlaila
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = Sowmya Raoh
| length4 = 4:40

| title5  = Hoosh Hoosh
| lyrics5 = Sirivennela Sitarama Sastry
| extra5  = KK (singer)|KK,Malik
| length5 = 4:44 
}}
   

==Awards==
Nandi Award for Best Villain-Tanikella Bharani

==References==
 

 

 
 
 
 


 