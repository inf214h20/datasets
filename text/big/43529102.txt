The Day Passed
 
{{Infobox film
| name           = The Day Passed
| image          = 
| image_size     =
| alt            = 
| caption        = 
| director       = Arif Babayev
| producer       = 
| writer         = Anar Rzayev
| starring       = Leyla Shikhlinskaya Hasan Mammadov Hasanagha Turabov
| music          = 
| cinematography = 
| editing        = 
| distributor    = Azerbaijanfilm
| released       =  
| runtime        = 83 min.
| country        = Azerbaijan
| language       = Azerbaijani
| budget         = 
| gross          = 
}}
 romantic drama film directed by Arif Babayev.  It is an adaptation of Anar Rzayevs "Georgian surname" novel.  The film stars Leyla Shikhlinskaya, Hasan Mammadov and Hasanagha Turabov.

==Plot==
The plot is based on two peoples unsuccessful relationship.    The film reflects atmosphere of Baku. 

==Cast==
* Leyla Shikhlinskaya as Asmar
* Hasan Mammadov as Ogtay
* Hasanagha Turabov as Ogtays work colleague
* Chingiz Aliloghlu as Jamal
* Mukhtar Avsharov as postman
* Sadaya Mustafayeva as Asmars mother

==See also==
* Xoca Dolu
* Jeyhun Mirzayev

==References==
 

==External links==
*  

 
 
 
 
 

 
 