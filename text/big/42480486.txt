Game (2014 film)
{{Infobox film
| name           = Game: He plays to win
| image          = game2014.jpeg
| alt            = 
| caption        = Theatrical Release poster
| director       = Baba Yadav
| producer       = Reliance Entertainment Grassroot Entertainment
| screenplay     = N.K. Salil
| story          = AR Murugadoss Jeet Subhasree Ganguly
| music          = Jeet Ganguly Jalsha Movies Production Team
| editing        = MD Kalam
| studio         = Reliance Entertainment   Jalsha Movies Production
| distributor    = Reliance Entertainment Jalsha Movies Production
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}} Bengali film Jeet and Vijay and Kajal Aggarwal in the Lead roles. It is also remade in Hindi named Holiday (2014 film)|Holiday. The film is a Blockbuster hit...    

==Plot== Captain in the Indian Army, returns to Kolkata from Kashmir. On his arrival, his parents and younger sisters force him to see Trisha (Subhashree Ganguly), who they choose for him to be married to. At the bride-viewing ceremony, Abhimanyu makes up excuses to avoid marrying her, which includes commenting about her being old-fashioned. On the contrary, Trisha is a college-level boxer, who is completely modern in her outlook. Abhimanyu realises this and proposes to her, which she reciprocates.

One day, while travelling around the city with his police officer-friend Santilal, Abhimanyu witnesses the explosion of a bus in which they had travelled. He manages to capture the man who laid the bomb, but he escapes from the hospital where he was kept under custody.  kidnaps the bomber again, and also forces the police officer who helped the bombers escape, to commit suicide. Abhimanyu soon learns that the bomber is a mere executor, a sleeper cell, whose only role was to plant the bomb. He also discovers that the Islamic terrorist group Harkat-ul-Jihad al-Islami, which the bomber belongs to, has planned various such attacks in the city in a couple of days. Enlisting the help of his fellow Army men and Balaji, Jagadish manages to thwart these attacks and kill the sleeper cell leaders brother and eleven other terrorists, including the first sleeper cell.

When the leader of the terrorist group (Sourav Chakraborty) learns about the role of Abhimanyu in the failure of the terrorist attack, he begins to target the families of the army men, except Abhimanyu, by kidnapping someone close to them. When Abhimanyu realises the plan, he substitutes one of the people to be kidnapped, with his younger sister . Using his pet dog and his sisters dupatta, he manages to reach the terrorists hideout, rescuing his sister, who was about to be killed after Abhimanyus bluff was exposed, and the other victims and eliminating the terrorists assembled there. Asif Ali , the second-in-command of the sleeper cells is captured and killed by Abhimanyu.

When this attack fails, the terrorist leader decides to target Ahimanyu himself. He asks Abhimanyu to surrender to him or else there would be more terrorist attacks. Abhimanyu decides to sacrifice his life and devises a plan with his fellow army men. Ahimanyu meets the leader in a ship, which has been rigged with a bomb planted by Abhimanyus friend. When he learns about the leaders plan however, which is to expose Abhimanyus army team as terrorists and knowing about a Muslim terrorist in the Indian defence — Vinod Sharma (Arindam Sil), he fights the leader and escapes with him in a boat. After the ship explodes, he kills the leader. Abhimanyu confronts Vinod Sharma and forces him to commit suicide, and later returns to Kashmir with his team.
== Cast == Jeet as Captain in Defence Intelligence Agency (India)|DIA, a wing of Indian Army.
* Subhasree Ganguly as Trisha, a boxer.
* Sourav Chakraborty as sleeper cells unnammed leader
* Shankar Chakraborty as Dibakar Panda, Abhimanyus senior commanding officer.
* Arindam Sil as Vinod Sharma
* Biswajit Chakraborty as Abhimanyus father
* Subhashish Mukherjee as Trishas father (Cameo Appearance)
* Arindol Bagchi as A.C.P Raghav Sen
* Charan Shekhon as Jeets friend in the Indian Army.
* Pritam as Jeets friend in the Indian Army.
* Vashcar Dev as Jeets friend in the Indian Army. 
* Yusuf Chishti as Jeets friend in the Indian Army.
* Arijit Roy

==Releasing==
"Game – He Plays To Win" will be releasing in a grand way with approx. 203 halls, which is a biggest opening in Bengal.

==Production==
The films production commenced in 2013, The soundtrack is composed by Jeet Ganguly,  The trailer of Game released on 2 May 2014 and got a bumper response both from critics and audience. Army officer for which Jeet lost some weight to fit in the role and to look 6&nbsp;years younger from his real biological age. Jeet also sported a Crew cut|crew-cut hairstyle for his role.
Whilst Subhasree will play a role of boxer and Avimanyu (Jeets) love interest Trisha.

== Soundtrack ==
{{Infobox album  
| Name       = Game – He plays to win
| Type       = Soundtrack
| Artist     = Jeet Ganguly
| Cover      =
| Alt        = 
| Released   = 23.may.2014
| Recorded   = 2014 Feature film soundtrack
| Length     = 15:01
| Label      = Reliance Entertainment
| Producer   = Reliance Entertainment
| Last album = Chirodini Tumi Je Amar 2 (2014)
| This album = Game (2014)
| Next album = The Mafia (2014 film)|Mafia (2014–2015)
}}
{| border="4" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f1f5fc; border: 1px #abd5f5 solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor=" #d0e5f5" align="center"
! Track !! Song !! Singer(s) !! Duration (min:sec) !! Music !! Rating (Out of 10 stars) !! Lyricist !! Verdict
|-
| 1 || "Game-Title Track" ||   || 6 || Raja Chanda || Hit
|- Jalsha Movies Production Team || Blockbuster Hit
|-
| 3 || "Ore Manwa Re" ||   || 9.4 || Prosen || Semi Hit
|- Jalsha Movies Production Team || Flop
|}

==See also==
* Thuppakki 2012 film in Tamil.
*   2014 film in Hindi.
*   2014 Bengali Action Flim 

==References==
 

==External links==
 

 
 
 
 


 