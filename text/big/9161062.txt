West Point of the Air
{{Infobox film
| name           = West Point of the Air
| image          = West Point poster.jpg
| image_size     = 220px
| caption        = Theatrical release poster Richard Rosson
| producer       = Monta Bell
| screenplay     = Frank Wead Arthur J. Beckhard
| story          = James Kevin McGuinness John Monk Saunders
| narrator       = Robert Young Lewis Stone Maureen OSullivan
| music          = Charles Maxwell
| cinematography = Clyde De Vinna Charles A. Marshall
| editing        = Frank Sullivan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         = $591,000 "The Eddie Mannix Ledger." Margaret Herrick Library, Center for Motion Picture Study, Los Angeles.  gross = $1,317,000 
}} Robert Young, Robert Taylor Richard Rosson. Nixon, Rob.   Turner Classic Movies. Retrieved: August 12, 2013.  

==Plot==
At Randolph Field, Texas, Master Sergeant "Big Mike" Stone (Wallace Beery) has aspirations for his son, "Little Mike" (Robert Young) to follow in his footsteps as a pilot. Following graduation from West Point, "Little Mike", along with his best friend, Phil Carter (Russell Hardie), become pilots in training at Randolph Field, commanded by Phils father, General Carter (Lewis Stone), but complications soon arise.

"Little Mike" has a childhood sweetheart, Phils sister, "Skip" (Maureen OSullivan) but is also being pursued by Dare Marshall (Rosalind Russell). Out late on a date with Dare, the next morning, "Little Mike" causes a crash during Phils solo flight, which ends with Phil losing a leg. Seeing what may happen after a crash, General Carter orders all the flying cadets into the air so they wont lose their nerve. Still in training, "Little Mike" blaming himself for his friends accident, flies poorly during a test, causing another aircraft to crash and disabling his own by wiping out the landing gear. His father takes to the skies and brings him back safely but a fight erupts on the ground when "Big Mike" attempts to cover up for his sons flying. The fight is witnessed and the Sergeant is dishonorably discharged from the service.

In a later meeting, "Little Mike" comes upon his father, now a drunk and toiling as a mechanic. Trying to help his son once again, "Big Mike" takes his sons aircraft up on a flare dropping mission, but crashes. His son comes to his aid and in a daring underwater rescue, proves his mettle. The army officials recognize both mens valor, and reinstates the Sergeant, and allows his son to graduate. Dare disapproves of "Little Mike" staying in the military, but he finally rejects her and also realizes that his true love is "Skip" .

==Cast==
As appearing in screen credits (main roles identified): 
 
* Wallace Beery as Master Sergeant "Big Mike" Stone Robert Young as Second Lieutenant "Little Mike" Stone
* Lewis Stone as General Carter
* Maureen OSullivan as "Skip" Carter
* Russell Hardie as Second Lieutenant Phil Carter
* Rosalind Russell as Mrs. Dare Marshall
* James Gleason as Joe "Bags"
* Henry Wadsworth as Second Lieutenant Pettis Robert Taylor as "Jasky" Jaskarelli  Robert Livingston as "Pip" Pippinger
* Frank Conroy as Captain Cannon
 
 

==Production== Randolph Field Travel Air Speedwing. 

As aviation films in the 1930s were increasingly finding it difficult to create the spectacular crashes that were often a feature of earlier periods, not only was safety a factor but also the number of war-weary aircraft that had been the staple of the time, were becoming harder to find. A Fokker D. VII that had flown in Hells Angels (film)|Hells Angels (1930), was refurbished by Mantz and appeared in West Point of the Air. 

==Reception==
Produced in an era of an America preparing for war, West Point of the Air was recognized for its value as a "recruiting poster" for the military. The review in The New York Times, emphasized that aspect of the film, "The Hollywood cinema continues its arguments on behalf of preparedness in "West Point of the Air," which chants the glories of the military service and the importance of iron discipline."  In a more recent review, Leonard Maltin remarked that, "Master Sergeant Beery pushes reluctant son Young through army air training for his own satisfaction. Good cast enlivens standard drama."  

==Box Office==
According to MGM records the film earned $677,000 in the US and Canada and $640,000 elsewhere resulting in a profit of $262,000. 

==References==

 

==References==
Notes
 

Citations
 

Bibliography
 
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0. 
* Farmer, James H. Broken Wings: Hollywoods Air Crashes. Missoula, Montana: Pictorial Histories Pub Co., 1984. ISBN 978-9-999926-515.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Wynne, Hugh. The Motion Picture Stunt Pilots & Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing, 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 