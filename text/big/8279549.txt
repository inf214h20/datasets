Son of Zorro
 
{{Infobox film
| name           = Son of Zorro
| image          = Poster of the movie Son of Zorro.jpg
| director       = Spencer Gordon Bennet Fred C. Brannon
| producer       = Ronald Davidson
| writer         = Franklin Adreon Basil Dickey Jesse Duffy Sol Shor George Turner Peggy Stewart Ernie Adams Stanley Price Edmund Cobb Ken Terrell
| cinematography = Bud Thackery
| distributor    = Republic Pictures
| released       = {{Film date|1947|01|18|U.S. serial|ref1= {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 96–97
 | chapter = 
 }} |1957|12|23|re-release|ref2= }}
| runtime         = 13 chapters (180 minutes (serial)  6 26½-minute episodes (TV)  English
| country         = United States
| budget          = $146,723 (negative cost: $156,745) 
| awards          = 
}}
 Republic Serial film serial. George Turner starred as a descendant of the original Zorro in 1860s United States.

==Plot==
A man returning home after having fought in the Civil War discovers that corrupt politicians have taken over the county and are terrorizing and shaking down the citizens. He dons the costume of his ancestor, the famous Zorro, and sets out to bring them to justice.

==Cast== Jeffrey "Jeff" Stewart/Zorro Peggy Stewart as Kate Wells
*Roy Barcroft as Boyd
*Edward Cassidy as Sheriff Moody Ernie Adams as Judge Hyde
*Stanley Price as Pancho
*Edmund Cobb as Stockton
*Ken Terrell as George Thomas

==Production==
Son of Zorro was budgeted at $156,745 although the final negative cost was $119,343 (a $10,022, or 6.8%, overspend).  It was the cheapest Republic serial of 1947.   It was filmed between 21 June and 20 July 1946 under the working title Zorro Strikes Again.   The serials production number was 1695. 
 Jungle Girl.  This marked the first time Republic had re-released a serial to add to their first run serial releases. 

===Stunts===
*Dale Van Sickel as Boyd, doubling Roy Barcroft Fred Graham as Boyd, doubling Roy Barcroft
*Ken Terrell as Pancho, doubling Stanley Price Tom Steele
*Tommy Coats
*John Daheim
*Ted Mapes
*Eddie Parker
*Post Park
*Gil Perkins
*Rocky Shahan
*Duke Taylor
*Bud Wolfe

===Special effects===
The special effects were created by the Lydecker brothers.

==Release==
===Theatrical=== Jungle Girl instead of a new serial.  This was the first time Republic had re-released a serial.  This was followed by the next new serial, Jesse James Rides Again.   The serial was re-released on 23 December 1957 between the similar re-releases of Radar Men from the Moon and Zorros Fighting Legion.  The last original Republic serial release had been King of the Carnival in 1955. 

===Television===
In the early 1950s, Son of Zorro was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes. 

==Chapter titles==
# Outlaw Country (20 min)
# The Deadly Millstone (13min 20s)
# Fugitive from Injustice (13min 20s)
# Buried Alive (13min 20s)
# Water Trap (13min 20s)
# Volley of Death (13min 20s)
# The Fatal Records (13min 20s)
# Third Degree (13min 20s)
# Shoot to Kill (13min 20s) - a clipshow|re-cap chapter
# Den of the Beast (13min 20s)
# The Devils Trap (13min 20s)
# Blazing Walls (13min 20s)
# Check Mate (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 245
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{succession box  Republic Serial Serial 
| before=The Crimson Ghost (1946)
| years=Son of Zorro (1947)
| after=Jesse James Rides Again (1947)}}
{{succession box  Serial 
| before=Zorros Black Whip (1944)
| years=Son of Zorro (1947)
| after=Ghost of Zorro (1949)}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 