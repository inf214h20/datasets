Hercules (1997 film)
 

 
{{Infobox film
| name           = Hercules
| image          = Hercules (1997 film) poster.jpg
| caption        = Promotional poster
| director       = Ron Clements John Musker
| producer       = Ron Clements John Musker
| writer         = Ron Clements John Musker Barry Johnson
| narrator       = Charlton Heston
| starring       = Tate Donovan Danny DeVito James Woods Susan Egan Rip Torn
| music          = Alan Menken
| editing        = Tom Finan Robert Hedland Walt Disney Feature Animation Buena Vista Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $85 million   
| gross          = $252.7 million 
}} musical Fantasy fantasy film Walt Disney Roman name, Hercules), the son of Zeus, in Greek mythology.

Produced during a period known as the Disney Renaissance, Hercules received positive reviews,    and earned $252.7 million in box office revenue worldwide. 

Hercules was later followed by the direct-to-video prequel  , which served as the pilot to  , a syndicated Disney TV series focusing on Hercules during his time at the Prometheus academy.

==Plot== Titans beneath Hades plots the Fates for help, Hades learns that in eighteen years, a planetary alignment will allow Hades to locate and free the Titans to conquer Olympus, but only if Hercules does not interfere. Hades sends his minions Pain and Panic to dispose of Hercules. The two succeed at kidnapping and feeding him a formula that turns him mortal, but fail to remove his superhuman strength before Hercules is found and adopted by the farmers Amphitryon and Alcmene.
 Pegasus to find the satyr Philoctetes (Disney)|Philoctetes—"Phil" for short—who is known for training heroes. The two meet Phil, who has retired from training heroes due to numerous disappointments, but Hercules inspires him to follow his dream to train a true hero who will be recognized by the gods. Phil trains Hercules into a potential hero, and when he is older, they fly for Ancient Thebes (Boeotia)|Thebes. On the way, they meet Megara (Disney)|Megara—"Meg" for short—a sarcastic damsel whom Hercules saves from the centaur Nessus (mythology)|Nessus. However, after Hercules, Phil, and Pegasus leave, Meg is revealed to be Hades minion, having sold her soul to him to save an unfaithful lover.
 Hydra to fight Hercules. Hercules continually cuts off its heads, but more heads replace them until Hercules kills the monster by causing a landslide. Hercules is seen as a hero and a celebrity, but Zeus tells Hercules he is not yet a true hero. Driven to depression, Hercules turns to Meg, who is falling in love with him. Hades learns of this and on the eve of his takeover, offers a deal that Hercules gives up his powers for twenty-four hours on the condition that Meg will be unharmed. Hercules accepts, losing his strength, and is heartbroken when Hades reveals that Meg is working for him.

Hades unleashes the Titans who climb Olympus and capture the gods, while a Cyclops goes to Thebes to kill Hercules. Phil inspires Hercules to fight and kill the cyclops, but Meg is crushed by a falling pillar saving Hercules from it, allowing him to regain his strength. Hercules and Pegasus fly to Olympus where they free the gods and launch the Titans into space where they explode, though Meg dies before he returns to her. With Megs soul now Hades property, Hercules breaks into the Underworld where he negotiates with Hades to free Meg from the Styx in exchange for his own life. His willingness to sacrifice his life restores his godhood and immortality before the life-draining river can kill him, and he rescues Meg and punches Hades into the Styx. After reviving Meg, she and Hercules are summoned to Olympus where Zeus and Hera welcome their son home. However, Hercules chooses to remain on Earth with Meg. Hercules returns to Thebes where he is hailed as a true hero as Zeus creates a picture of Hercules in the stars commemorating his heroism.

==Voice cast==
 
 mythological deity Heracles. Supervising animator Andreas Deja described Hercules as "...not a smart aleck, not streetwise, hes just a naive kid trapped in a big body", and that Donovan "had a charming yet innocent quality in his readings". Donovan had not done any voice-over work prior to Hercules. Deja integrated Donovans "charming yet innocent quality" into Hercules expressions.    Hercules as a teenager, while Roger Bart provided his singing voice. Randy Haycock served as the supervising animator for Hercules as an infant and teenager. Eric Goldberg, Snow White and Bacchus in Fantasia (1940 film)|Fantasia as the inspirations for the characters design. Goldberg mentioned that they discovered that Danny DeVito "has really different mouth shapes" when they videotaped his recordings and that they used these shapes in animating Phil. 
*James Woods as Hades (Disney)|Hades. Producer Alice Dewey mentioned that Hades "was supposed to talk in a slow and be menacing in a quiet, spooky way", but thought that James Woods manner of speaking "a mile a minute" would be a "great take" for a villain.  Woods did a lot of ad-libbing in his recordings, especially in Hades dialogues with Megara. Nik Ranieri, the supervising animator for Hades, mentioned that the character was "based on a Hollywood agent, a car salesman type", and that a lot came from James Woods ad-libbed dialogue. He went on to say that the hardest part in animating Hades was that he talks too much and too fast, so much so that "it took   two weeks to animate a one-second scene". Ranieri watched James Woods other films and used what he saw as the basis for Hades sneer. 
*Susan Egan as Megara (Disney character)|Megara. Supervising animator Ken Duncan stated that she was "based on a 40s screwball comedienne" and that he used Greek shapes for her hair ("Her head is in sort of a vase shape and shes got a Greek curl in the back."). 
*Frank Welker as Pegasus. Ellen Woodbury served as the supervising animator for Pegasus.
*Rip Torn and Samantha Eggar as Zeus and Hera, Hercules birth-parents. Anthony DeRosa served as the supervising animator for both characters. In the Swedish dub Max von Sydow provided the voice for Zeus. Thalia and Clio respectively), the narrators of the films story. Michael Snow served as the supervising animator for the Muses.
*Bobcat Goldthwait and Matt Frewer as Pain and Panic, Hades henchmen. James Lopez and Brian Ferguson respectively served as the supervising animators for Pain and Panic.
*Patrick Pinney as the Cyclops. Dominique Monfrey served as the supervising animator for the Cyclops.
*Hal Holbrook and Barbara Barrie as Amphitryon and Alcmene, Hercules adoptive parents. Richard Bazley served as the supervising animator for both characters.
*Amanda Plummer, Carole Shelley and Paddi Edwards as Clotho, Lachesis (mythology)|Lachesis, Atropos, the three Fates who predict Hades attempt to conquer Olympus. Nancy Beiman served as the supervising animator for the three characters.
*Paul Shaffer as Hermes. Michael Swofford served as the animator for Hermes. Chris Bailey served as the animator for Nessus.
*Wayne Knight as Demetrius
*Keith David as Apollo
*Charlton Heston has a cameo role as the opening narrator.

==Production==

===Development=== pitched by The Little Walt Disney greenlight Treasure Planet or not produce Treasure at all.     Turning down adaptation proposals for Don Quixote, The Odyssey, and Around the World in Eighty Days, the directors were notified of animator Joe Haidars pitch for a Hercules feature.  "We thought it would be our opportunity to do a "superhero" movie," Musker said, so "Ron and I being comic book fans. The studio liked us moving onto that project and so we did  ." 

===Casting===
Writing the role of Philoctetes (Disney)|Philoctetes, Musker and Clements envisioned Danny DeVito in the role. However, DeVito declined to audition so Ed Asner, Ernest Borgnine, Dick Latessa were brought in to read for the part. After Red Buttons had auditioned, he left stating "I know what youre gonna do. Youre gonna give this part to Danny Devito!" Shortly after, the directors and producer Alice Dewey approached DeVito at a pasta lunch during the filming of Matilda (1996 film)|Matilda, where DeVito signed on to the role. 
 Hades proved Jack  ?"    After DeVito notified Nicholson of the project, the next week, the studio was willing to pay Nicholson $500,000 for the role, but Nicholson demanded roughly a paycheck of $10 to $15 million, plus a 50% cut of all the proceeds from Hades merchandise.  Unwilling to share merchandising proceeds with the actor, Disney came back with a counter offer that was significantly less than what Nicholson had asked for. Therefore, Nicholson decided to pass on the project.  Disappointed by the lack of Nicholson, Clements and Musker eventually selected John Lithgow as Hades in fall 1994. After nine months of trying to make Lithgows portrayal of Hades work, Lithgow was released from the role in August 1995.    According to John Musker, Ron Silver, James Coburn, Kevin Spacey, Phil Hartman, and Rod Steiger arrived to the Disney studios to read as Hades.  When they invited James Woods to read for the part, the filmmakers were surprised by Woodss interpretation, and Woods was hired by October 1995. 

===Design=== Gaston in Beauty and Jafar in Scar in The Lion King) with about four animators on his crew, but he had a team of twelve or thirteen for Hercules.  Given Deja had worked with three villains before, he was first offered Hades, but asked to animate Hercules instead - "I knew if would be more difficult and more challenging, but I just needed that experience to have that in your repertoire." 

==Music==
{{Infobox album
| Name        = Hercules: An Original Walt Disney Records Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Hercules_soundtrack_cover.jpg
| Genre       = Pop music|Pop, Gospel music|gospel, Soul music|soul, Contemporary R&B|R&B, musical theatre
| Released    = May 27, 1997
| Recorded    =
| Length      = Walt Disney
| Producer    = Alan Menken, David Zippel
| Chronology  = Walt Disney Animation Studios The Hunchback of Notre Dame (1996)
| This album  = Hercules (1997)
| Next album  = Mulan (soundtrack)|Mulan (1998)
| Misc              = {{Singles
 | Name            = Hercules: An Original Walt Disney Records Soundtrack
 | Type              = soundtrack
 | single 1         = Go the Distance
 | single 1 date = 1997
 }}
}} Turkish version of the film, "Go the Distance" was sung by Tarkan (singer)|Tarkan, who also performed the vocals for the adult Hercules.

"Go the Distance" was nominated for both the Academy Award for Best Original Song and the Golden Globe Award for Best Original Song, but ultimately lost both to Celine Dions hit "My Heart Will Go On" from Titanic (1997 film)|Titanic.

Belinda Carlisle recorded two versions of "I Wont Say (Im in Love)" as well as a music video for promotional purposes. Though the English dub eventually opted not to use it, several foreign dubs have it in place of the reprise of "A Star Is Born" in the ending credits. These dubs include, but are not limited to, the Swedish one, the Finnish one, the Icelandic one and the Russian one. Curiously enough, the DVD release of the Swedish dub has replaced it with the reprise of "A Star Is Born".

Track list:
#"Long Ago..." - Charlton Heston The Gospel Truth/Main Title - Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas
#The Gospel Truth II - Roz Ryan
#The Gospel Truth III - Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas
#"Go the Distance" - Roger Bart
#Oh Mighty Zeus (Score)
#"Go the Distance (Reprise)" - Roger Bart
#"One Last Hope" - Danny DeVito
#"Zero to Hero" - Tawatha Agee, Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas
#"I Wont Say (Im in Love)" - Susan Egan, Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas
#"A Star Is Born" - Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas
#"Go the Distance (Single)" - Michael Bolton
#The Big Olive (Score)
#The Prophecy (Score)
#Destruction of the Agora (Score)
#Phils Island (Score)
#Rodeo (Score)
#Speak of the Devil (Score)
#The Hydra Battle (Score)
#Megs Garden (Score)
#Hercules Villa (Score)
#All Time Chump (Score)
#Cutting the Thread (Score)
#A True Hero/A Star Is Born (End Title) - Lillias White, LaChanze, Roz Ryan, Cheryl Freeman, and Vanéese Y. Thomas

==Release==
 

===Marketing=== PC and PlayStation. 

===Home media===
The films first home video release, on VHS, was February 3, 1998 in the US as part of the Walt Disney Masterpiece Collection series. A Limited Issue came out on DVD November 9, 1999, followed by on August 1, 2000, a re-issue to VHS and DVD as part of the Walt Disney Gold Classic Collection. The film was released on a Special Edition Blu-ray, DVD, and Digital HD on August 12, 2014. 

===Video game===
 
 
A video game based on the film was released for the PlayStation and Microsoft Windows in 1997, later put on the PlayStation Network online service for the PlayStation 3.

==Reception== Pnyx hill, but the Greek government declined after Greek media and public panned the film. A Greek newspaper entitled Adsmevtos Typos called it "another case of foreigners distorting our history and culture just to suit their commercial interests". 

After a one-theater release on June 15, 1997, Hercules had its wide release on June 27, 1997. With an opening weekend of 	$21,454,451, it opened at the second spot of the box office, after Face/Off.  The film grossed only $99 million during its run at the North American box office, something Disneys executives blamed on "more competition".  The international totals for Hercules raised its gross to $253 million. 

===Critical reception===
Review aggregator Rotten Tomatoes reported the film had garnered a 84% rating based on reviews from 49 critics, with an average rating of 7/10. The sites consensus states "Fast-paced and packed with dozens of pop culture references, Hercules might not measure up with the true classics of the Disney pantheon, but its still plenty of fun." 

Film critic Roger Ebert of the Chicago Sun-Times wrote a positive review of the film, enjoying the story as well as the animation. Ebert also praised James Woods portrayal of Hades, stating that Woods brings "something of the same verbal inventiveness that Robin Williams brought to Aladdin (1992 Disney film)|Aladdin".  The New York Times critic Janet Maslin also praised Woodss performance remarking "Woods shows off the full verve of an edgy Scarfe villain", and added "On any level, earthly or otherwise, the ingenious new animated Hercules is pretty divine."  James Berardinelli, film critic for ReelViews, awarded the film 3 out of 4 stars writing, "Hercules has the dubious distinction of being the least-enchanting cartoon Disney has fashioned in over a decade", but remained critical of the storyline, visual artwork, and characters. 

Writing for The Washington Post, Desson Howe criticized the film as an "insipid, lifeless, animated feature".  Likewise, Rita Kempley of Washington Post blasted the film as "a Looney Tunes|Looney-Tunesy spoof of muscle-bound movies, celebrity worship and, curiously enough, the studios own shameless hucksterism." 

===Awards and nominations=== Academy Awards 
:* Academy Award for Best Original Song - "Go the Distance" (Nominated) Golden Globes 
:* Golden Globe Award for Best Original Song - "Go the Distance" (Nominated)
* Saturn Award  Best Fantasy Film (Nominated)
* Blockbuster Entertainment Awards 
:* Favorite Animated Family Movie (Nominated)
:* Favorite Song from a Movie - "Go the Distance" (Nominated)
* Young Artist Award  Young Herculess voice (Nominated)
* Annie Awards
{| class="wikitable" style="width:70%;"
|- "
! Result !! Award !! Winner/Nominee Recipient(s)
|-
| Nominated || Animated Theatrical Feature ||
|-
| Won || Individual Achievement in Producing || Alice Dewey (Producer) John Musker (Producer) Ron Clements (Producer)
|-
| Won || Individual Achievement in Directing || John Musker (Director) Ron Clements (Director)
|- Ken Duncan (Supervising Animator&nbsp;— Meg)
|-
| Won || Individual Achievement in Character Animation || Nik Ranieri (Supervising Animator&nbsp;— Hades)
|-
| Won || Individual Achievement in Effects Animation || Mauro Maressa (Effects Supervisor)
|}

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 