Thrill of a Romance
{{Infobox film  
| name           = Thrill of a Romance
| image          = Thrillofaromanceposter.jpg
| caption        = Theatrical release poster
| director       = Richard Thorpe
| producer       = Joe Pasternak
| writer         = Richard Connell Gladys Lehman
| starring       = Van Johnson Esther Williams Frances Gifford Carleton G. Young Lauritz Melchior
| music          = Calvin Jackson George Stoll
| cinematography = Harry Stradling
| editing        = George Boemler
| distributor    = Metro-Goldwyn-Mayer 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English budget = $1,410,000  . 
| gross = $7,205,000 
}}
 American romance film released by Metro-Goldwyn-Mayer in 1945, starring Van Johnson, Esther Williams and Carleton G. Young, with musical performances by opera singer Lauritz Melchior . The film was directed by Richard Thorpe and written by Richard Connell and Gladys Lehman.

The film tells the story of Cynthia Glenn, who, after a whirlwind romance, marries a rich businessman. However, on the first day of their honeymoon, her new husband is called away to Washington, leaving her alone at a resort. During this time, she meets and falls in love with a war hero, Tommy Milvaine, played by Van Johnson.
 Easy to Love (1953). 

Thrill of a Romance was a box office success, becoming the eighth-highest grossing film of 1945.

==Plot==
Cynthia Glenn (Esther Williams) is a swimming instructor in Los Angeles, California, where she lives with her scatterbrained aunt and uncle Nona and Hobart (Spring Byington and Henry Travers). While demonstrating a dive to her students, she catches the eye of an interested stranger, Bob Delbar (Carleton G. Young). Cynthia returns home to find that she has received flowers from the stranger. The two court for one month, and then get married.

They leave immediately after the wedding on their honeymoon to the hotel Monte Belva, where they encounter the famous opera singer, Nils Knudsen (Lauritz Melchior). Major Thomas Milvaine (Van Johnson), who is also staying at the hotel, notices Cynthia.  J.P. Bancroft, a rich colleague, calls Bob and insists that he come to Washington to complete a deal. While Cynthia cries over Bobs departure, Tommy, who is staying next door, comforts her.

The next day, Cynthia ventures down to the pool, where she and J.P.Bancrofts daughter, Maude (Frances Gifford) speculate as to which hotel guest is Major Thomas Milvaine, the decorated war hero, who shot down "16... or was it 26 war planes?" and was stuck on a deserted island for a month. After Maude teases Cynthia about being at the hotel without her husband on her vacation, Cynthia performs an elaborate dive and swims around the pool, where she proceeds to run into Major Milvaine himself, who cant actually swim, so she teaches him how.
 make love to her. She calls Bob in Washington and begs him to return, but he tells her that he cant.
 Sunset Trail. Tommy returns to the hotel, learns where Cynthia has gone, and sets off after her on the trail. Meanwhile, Bob telephones the hotel and leaves a message indicating that he will be arriving the following morning. Tommy finds Cynthia and they proceed to walk together until they come across a tree with initials engraved on it. Cynthia tells Tommy that she loves him as well, but wants to give her marriage to Bob a chance, and lets him know that once they get back to the hotel, they should say their goodbyes and never see each other again. However, on the return trip, they lose their way and are forced to spent the night in the woods.
 never actually divorced from his previous wife. Tommy leaves to become an instructor at Darwin Field, and Cynthia returns home to her aunt and uncle. Nils Knudsen (Lauritz Melchior) telephones Tommy, and later, the two arrive at Cynthias house late at night with Dorsey and a contingent of his orchestra, where they serenade the Glenn house, as well as the rest of the neighborhood. Tommy lip-synches Knudsens voice to a love song to Cynthia. Cynthia runs outside to Tommy, and the two share a kiss while Knudsen continues singing, leaving Nona, and a bemused Hobart, to wonder how Tommy can sing and kiss at the same time.

==Cast==
* Esther Williams as Cynthia Glenn
* Van Johnson as Major Thomas Milvaine
* Carleton G. Young as Robert G. Delbar
* Frances Gifford as Maude Bancroft
* Henry Travers as Hobart Hobie Glenn
* Spring Byington as Nona Glenn
* Lauritz Melchior as Nils Knudsen
* Jane Isbell as Giggling Girl
* Ethel Griffies as Mrs. Sarah Fenway  
* Donald Curtis as K.O. Karny   Jerry Scott as Lyonel  
* Fernando Alvarado as Julio  
* Helene Stanley as Susan Dorsey  
* Vince Barnett as Oscar the waiter  
* Billy House as Dr. Tove  
* Joan Fay Macaboy as Betty  
* Tommy Dorsey as Himself (as Tommy Dorsey and His Orchestra) Jeff Chandler as Singer
* The King Sisters as Specialty Act
Drummer Buddy Rich (uncredited, except for the BR logo on his bass drum) notably performs a short solo in one scene, as well playing with the Dorsey Orchestra in several others. 

Sixteen-year-old Jerry Scott sings a beautiful rendition of Because (You Come to Me with Naught Save Love).

==Production==
When attempting to create the right shade of blue for the swimming pool, the set decorator discovered that the paint he had used to color the cement had dissolved after adding the chlorine to the pool, creating a mess with the consistency of homogenized milk. The pool had to be drained and refilled.   

In her autobiography, Williams said that the studio attempted to put her and costar Van Johnson together in public as much as possible, even though she was involved with future husband (and ex-husband) Ben Gage. When asked why they didnt date, Johnson replied "because Im afraid she cant get her webbed feet into a pair of evening sandals."  

While filming, Williams and Thorpe rarely got along. After Williams forgot several lines during one take and the cast and crew began to leave for lunch, Williams notified Thorpe of her mistake. He called the entire crew back to the stage, saying "Turn the lights back on, boys. This lady wants to act." Williams locked herself in her dressing room for the rest of the day. After that episode, Thorpe stopped picking on her. 

When filming the backstroke scenes in the swimming pool, Williams had to place her hand under Johnsons back to keep him afloat. 
 Office of War Information voiced concern that the film, set in an elegant resort, would pose problems with overseas distribution. A memo from the agency claimed that films boasting of American opulence would be resented by the allies closer to the fighting front. 

==Release==
The film wrapped on October 1, 1944, and was released the following year. It was previewed in a small neighborhood outside of Los Angeles. Cards filled out by the audience were filled with comments such as "Van is a darling" and "I love that boy...I love him more than Frank Sinatra|Frankie."    

The film premiered at the Graumans Egyptian Theatre in Hollywood, with the proceeds going to the war wounded. Johnson was overcome by female fans upon his arrival and exit to the theater. Fans stole his handkerchief, boutonnière and buttons from his shirt. They also yanked his tie, tore his collar and ripped his red hair from his head, leaving his scalp bleeding. 

===Critical response===
A 1945 New York Times review of the film claimed that "the minutes drag on here unthrillingly" and that "as for Miss Williams, she models a bathing suit handsomely and cuts a fine figure in the water. But right there her talent ends." 

A reviewer for the Los Angeles Times wrote, "Thrill of a Romance is all bright colors but the luster is only glaze deep. But its gaudiness will carry it through, especially with the fans."  

When the film opened at the Capitol Theater in Manhattan, the critic from the New York Herald Tribune remarked that Johnson gave "the type of performance that has endeared him to the younger set. He is the antithesis of the wolf...clean cut, amiable, a little shy, and needing aid and comfort."  

===Box office===
Thrill of a Romance was the eighth top grossing film of 1945 in the US and Canada, earning $4,338,000.     It also earned $2,682,000 in other countries, resulting in a profit of $3,259,000. 

===Home media=== Fiesta (1947), Easy to Love (1953). 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 