Rembrandt (1942 film)
{{Infobox film
| name           = Rembrandt
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Hans Steinhoff
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Ewald Balser Hertha Feiler Gisela Uhlen Aribert Wäscher
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} German historical historical drama film directed by Hans Steinhoff and starring Ewald Balser, Hertha Feiler, Gisela Uhlen and Aribert Wäscher.  It was based on the novel Zwischen Hell und Dunkel by V. Tornius and depicts the life of the Dutch painter Rembrandt.

==Cast==
* Ewald Balser ...  Rembrandt 
* Hertha Feiler ...  Saskia van Rijn 
* Gisela Uhlen ...  Hendrickje Stoffels 
* Elisabeth Flickenschildt ...  Geertje Dierks 
* Theodor Loos ...  Jan Six 
* Aribert Wäscher ...  Saskias Verwandter Ujlenburgh 
* Paul Henckels ...  Radierer Seeghers 
* Hildegard Grethe ...  Frau Seeghers 
* Wilfried Seyferth ...  Ulricus Vischer 
* Paul Rehkopf ...  Bruder Adriaen 
* Rolf Weih ...  Schüler Eeckhout 
* Clemens Hasse ...  Schüler Philip 
* Helmut Weiss ...  Schüler Cornelis (as Helmut Weiß) 
* Heinrich Schroth ...  Doktor Tulp 
* Robert Bürkner ...  Notar Wilkens 
* Karl Dannemann ...  Banning Cocq
* Walther Süssenguth ... Piet

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 