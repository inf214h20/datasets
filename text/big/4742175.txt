Mortuary (2005 film)
{{Infobox film
| name           = Mortuary
| image          = Mortuaryposter.jpg
| caption        = Theatrical release poster
| director       = Tobe Hooper
| producer       = {{plainlist|
* Tony DiDio
* E. L. Katz
* Peter Katz
* Alan Somers
}}
| writer         = {{plainlist|
* Jace Anderson
* Adam Gierasch
}}
| starring       = {{plainlist|
* Dan Byrd
* Alexandra Adi
* Denise Crosby
* Rocky Marquette
* Courtney Peldon
* Bug Hall
}}
| music          = Joseph Conlan
| cinematography = Jaron Presant
| editing        = Andrew Cohen Echo Bridge Entertainment
| distributor    = Echo Bridge Home Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
Mortuary is a 2005 American zombie film directed by Tobe Hooper. It stars Dan Byrd, Alexandra Adi and Denise Crosby.

== Plot ==
After the loss of their father, the Doyle family – Leslie, Jonathan, and Jamie – move to the Fowler Mortuary in hope of starting a new life but find it in poor condition. Jonathan goes to the local diner where he meets Cal and his two girlfriends, Tina and Sara. Cal tells Johnathan about the legend of Bobby Fowler, an abused and deformed boy who lived in the mortuary. Jonathan also meets Liz, a local girl; Grady, Lizs best friend; and Rita, Lizs aunt, who employed her to work at the diner. Jonathan, Liz, and Grady become friends quickly. That night, Cal, Sara and Tina go to the graveyard outside the mortuary and vandalize it; they then go into one of the crypts, where they are attacked by Bobby Fowler.

The next day, Jonathan, Liz and Grady go back to Johnathans house, where Grady gives them drugs. Shortly afterwards, Sheriff Howell shows up to welcome Leslie to the town. He informs her about his attempt to stop "graveyard babies". He also is looking for Cal, Sara, and Tina. Sheriff Howell investigates the crypts, where Bobby Fowler infects him. The next day at the diner, Johnathan and Liz are working, when Cal and Sara arrive messy. Cal has a rage attack and Sara begins vomiting black ooze. Rita comes in to calm her down but is infected in the process. At the mortuary, Leslie performs her first embalming. While doing so, she  makes a mess of the embalming fluids. While going to clean it up, one of the bodies gets up and infects Leslie. Jonathan, Jamie, Liz, and Grady return to Jonathans house for dinner, where Leslie, now infected with the mysterious ooze, has prepared a sort of ooze soup. The four notice a black fungus growing on the wall. Liz pours salt in her soup which makes it bubble and sends Leslie into a rage in which she attacks Jonathan and Jamie. Liz and Grady quickly escape.

Shortly afterwards, Jonathan fights off his mother and saves Jamie. The four escape outside but are forced to retreat into a crypt, because they are chased there by an infected man. The four make the shocking discovery that Bobby lives in the crypt and dug tunnels under the cemetery. They find Tina, who is still alive, but are attacked by the mother. They find a ladder which leads back into the house, where they barricade themselves. They question Tina and eventually cut her hand to see if her blood has turned into the black ooze, a sign of the infection. Sheriff Howell assaults the house a shotgun. Tina goes into the kitchen to clean the wound but is infected by contaminated water. Grady accidentally pours salt on her, burning her. The group now knows the weakness of the infected, but their victory is short-lived after they find that Jamie, who Jonathan had left in her room to keep her safe, has been captured.

Shortly after entering the tunnels, they  see and hear things; a hand suddenly punches through Gradys chest, giving him enough time to say quietly, "run", before dying. They soon find the source, a well filled with black ooze. Shortly thereafter, Jamie is found, but captured by Bobby Fowler and taken back to the lair, where they confront him by throwing salt in the well. Cal is killed by getting his head blown off by the Sheriff, and the other infected are burned with salt; Bobby is sucked in and the three escape the house. The three, happy to escape but still grieving the deaths of Leslie and Grady, are about to leave the house, but Jonathan is pulled underground, and Leslie, alive again, pulls Jamie into the house, and Lizs fate is left untold.

== Cast ==
* Denise Crosby as Leslie
* Dan Byrd as Jonathan
* Stephanie Patton as Jamie
* Taren Smith as Cal
* Courtney Peldon as Tina
* Tarah Paige as Sara
* Rocky Marquette as Grady
* Michael Shamus Wiles as Sheriff Howell

== Production ==
Filming began in January 2004 in Pomona, California. 

== Release ==
Mortuary premiered at the New York City Horror Film Festival on October 21, 2005.  It received a limited theatrical release in January 2006.   It was released on Blu-ray July 29, 2008. 

== Critical reception ==
Bloody Disgusting rated it 2/5 stars and said that "its for die-hard Hooper fans only."   Steve Barton of Dread Central rated it 3/5 stars and called the film "dumb fun" that has an uneven tone.  Barton concludes, "In the end, despite its identity crisis and other short comings, theres a lot to like about Mortuary." 

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 