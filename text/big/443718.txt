Days of Heaven
{{Infobox film
| name           = Days of Heaven
| image          = Daysofheavenposter.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Terrence Malick
| producer       = Bert Schneider Harold Schneider
| writer         = Terrence Malick Brooke Adams Sam Shepard Linda Manz
| music          = Ennio Morricone Leo Kottke 
| cinematography = Néstor Almendros Haskell Wexler
| editing        = Billy Weber
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $3,000,000
| gross          = $3,446,749 }}
 Brooke Adams, Sam Shepard and Linda Manz. Set in 1916, it tells the story of Bill and Abby, lovers who travel to the Texas Panhandle to harvest crops for a wealthy farmer. Bill encourages Abby to claim the fortune of the dying farmer by tricking him into a false marriage.

Days of Heaven was Malicks second feature film, after the enthusiastically received Badlands (film)|Badlands (1973), and was produced on a budget of $3,000,000. Production was particularly troublesome, with a tight shooting schedule and significant budget restraints. Additionally, editing took Malick a lengthy three years, due to difficulty with achieving a general flow and assembly of the scenes. This was eventually solved with an added, improvised narration by Linda Manz.  The film was scored by Ennio Morricone and photographed by Nestor Almendros and Haskell Wexler.
 Best Director Award at the Cannes Film Festival. Despite initially unfavorable reviews, Days of Heaven has since become one of the most acclaimed films of the 1970s, particularly noted for its cinematography.

In 2007, Days of Heaven was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
The story is set in 1916.  Bill (Gere), a Chicago manual laborer, knocks down and kills a boss (Margolin) in the steel mill where he works. He flees to the Texas Panhandle with his girlfriend Abby (Adams) and younger sister Linda (Manz),
 who provides the film narration. Bill and Abby pretend to be siblings to prevent gossip. The three hire on as part of a large group of seasonal workers with a rich, shy farmer (Shepard). The farmer learns that he is dying, although the nature of the illness is not specified. 

After the farmer falls in love with Abby, Bill encourages her to marry the rich farmer so they can inherit his money after he dies. The marriage takes place and Bill stays on the farm as Abbys "brother". The farmers foreman suspects their scheme. The farmers health unexpectedly remains stable, foiling Bills plans. Eventually, the farmer discovers Bills true relationship with Abby. At the same time, Abby has begun to fall in love with her husband. After a locust swarm and a fire destroy his wheat fields, the incensed farmer goes after Bill with a gun, but Bill kills him with a screwdriver, fleeing with Abby and Linda. The foreman and the police pursue and eventually find them. Bill is killed by the police. Later, Abby inherits the farmers money and leaves Linda at a boarding school. Abby leaves town on a train with soldiers leaving for World War I. Linda runs away from  school with a friend.

==Cast==
*Richard Gere as Bill Brooke Adams as Abby
*Sam Shepard as The Farmer
*Linda Manz as Linda
*Robert J. Wilke as farm foreman
*Stuart Margolin as mill foreman Timothy Scott as harvest hand
*Doug Kershaw as fiddler
*Richard Libertini as vaudeville leader

==Production==
  Brooke Adams and actor/playwright Sam Shepard for the lead roles. Paramount Pictures CEO at the time Barry Diller wanted Schneider to produce films for him and agreed to finance Days of Heaven. At the time, the studio was heading in a new direction. They were hiring new production heads who had worked in network television, and, according to former production chief Richard Sylbert, "  product aimed at your knees".  Despite the change in direction, Schneider was able to secure a deal with Paramount by guaranteeing the budget and taking personal responsibility for all overages. "Those were the kind of deals I liked to make ... because then I could have final cut and not talk to nobody about why were gonna use this person instead of that person", Schneider said. 

Malick admired cinematographer Nestor Almendros work on The Wild Child and wanted him to shoot Days of Heaven. Almendros 1986  Almendros was impressed by Malicks knowledge of photography and willingness to use little studio lighting. The two men modeled the films cinematography after silent films, which often used natural light. They drew inspiration from painters such as Johannes Vermeer, Edward Hopper (particularly his House by the Railroad), and Andrew Wyeth, as well as photo-reporters from the start of the 20th century. 

===Principal photography===
  
|ref=  For the closeups and insert shots, thousands of live locusts were used which had been captured and supplied by the Canadian Department of Agriculture.  align = right width = 40%}} Heritage Park Historical Village, Calgary.    

  designed and made the period costumes from used fabrics and old clothes to avoid the artificial look of studio-made costumes. 

 

According to Almendros, the production was not "rigidly prepared", allowing for improvisation. Daily call sheets were not very detailed and the schedule changed to suit the weather. This upset some Hollywood crew members not used to working this way. Most of the crew were used to a "glossy style of photography" and felt frustrated because Almendros did not give them much work.  On a daily basis, he asked them to turn off the lights they had prepared for him. Some crew members said that Almendros and Malick did not know what they were doing. The tension led to some of the crew quitting the production. Malick supported what Almendros was doing and pushed the look of the film further, taking away more lighting aids, and leaving the image bare.  
 Polaroids of the scene, then examined them through very strong glasses".  According to Almendros, Malick wanted "a very visual movie. The story would be told through visuals. Very few people really want to give that priority to image. Usually the director gives priority to the actors and the story, but here the story was told through images".    
 magic hour, which Almendros called: "a euphemism, because its not an hour but around 25 minutes at the most. It is the moment when the sun sets, and after the sun sets and before it is night. The sky has light, but there is no actual sun. The light is very soft, and there is something magic about it. It limited us to around twenty minutes a day, but it did pay on the screen. It gave some kind of magic look, a beauty and romanticism."  Lighting was integral to filming and helped evoke the painterly quality of the landscapes in the film. A vast majority of the scenes were filmed late in the afternoon or after sunset, with the sky silhouetting the actors faces, which would otherwise be difficult to see.    Interior scenes that feature light coming in from the outside, were shot using artificial light to maintain the consistency of that intruding light. The "magic look", however, would also extend to interior scenes, which did occasionally utilize natural light. 

For the shot in the "locusts" sequence, where the insects rise into the sky, the film-makers dropped peanut shells from helicopters. They had the actors walk backwards while running the film in reverse through the camera. When it was projected, everything moved forward except the locusts.    For the close-ups and insert shots, thousands of live locusts were used which had been captured and supplied by Canadas Department of Agriculture.  

While the photography yielded the director satisfactory results critically, the rest of the production was difficult from the start.  The actors and crew reportedly viewed Malick as cold and distant. After two weeks of shooting, Malick was so disappointed with the dailies, he "decided to toss the script, go Leo Tolstoy instead of Fyodor Dostoyevsky, wide instead of deep   shoot miles of film with the hope of solving the problems in the editing room."  

The harvesting machines constantly broke down, which resulted in shooting beginning late in the afternoon, allowing for only a few hours of light before it was too dark to go on. One day, two helicopters were scheduled to drop peanut shells that were to simulate locusts on film; however, Malick decided to shoot period cars instead. He kept the helicopters on hold at great cost. Production was lagging behind, with costs exceeding the budget $3,000,000 by about $800,000, and Schneider had already mortgaged his home in order to cover the overages.    
 John Bailey The Man Who Loved Women (1977). Almendros approached cinematographer Haskell Wexler to complete the film. They worked together for a week so that Wexler could get familiar with the films visual style.  

Wexler was careful to match Almendros work, but he did make some exceptions. "I did some hand held shots on a Panaflex", he said, "  the opening of the film in the steel mill. I used some diffusion. Nestor didnt use any diffusion. I felt very guilty using the diffusion and having (sic) the feeling of violating a fellow cameraman."  Although half the finished picture was footage shot by Wexler, he received only credit for "additional photography", much to his chagrin. The credit denied him any chance of an Academy Award for his work on Days of Heaven. Wexler sent film critic Roger Ebert a letter "in which he described sitting in a theater with a stop-watch to prove that more than half of the footage" was his.   

===Post-production===
  Looking for Mr. Goodbar. 

According to Schneider, the editing for Days of Heaven took so long that "Brooks cast Gere, shot, edited and released   while Malick was still editing".  A breakthrough came when Malick experimented with voice-overs from Linda Manzs character, similar to what he had done with Sissy Spacek in Badlands. According to editor Billy Weber, Malick jettisoned much of the films dialogue, replacing it with Manzs voice-over, which served as an oblique commentary on the story.  After a year, Malick had to call the actors to Los Angeles, California to shoot inserts of shots that were necessary but had not been filmed in Alberta. The finished film thus includes close-ups of Shephard that were shot under a freeway overpass. The underwater shot of Geres falling face down into the river was shot in a large aquarium in Spaceks living room.  
 project for The Thin Red Line twenty years later. 

==Soundtrack==
The soundtrack for Days of Heaven is a strong reflection of the films context. Ennio Morricone provided the films score and received his first Academy Award nomination in his soundtrack composing career for his work on the film.    Morricone recalled the process as being "demanding" and said of Malick: "He didnt know me very well, so he made suggestions, and in some cases, gave musical solutions. This kind of annoyed me because hed say: This thing . . . try it with three flutes. Something impossible! So, to humor him, I would do it with three flutes and then hed decide to use my version after all. His was impossible or I would have written it myself. And more nitpicking like that which means he was very attentive and careful about music." 

There are three main scores composed to Days of Heaven: the main theme, which references “Carnival of the Animals|Aquarium", the seventh movement from Camille Saint-Saëns’s Carnival of the Animals, a "pastoral melody" for flute, and finally a love theme. The soundtrack was remastered and re-released in July 2011 in a two-disc edition and features excerpts of Manzs narration. {{cite news
  | last = 
  | first =
  | title = Days of Heaven: Film Score
  | work = Soundtrack Collector
  | pages =
  | publisher =
  | date = July 22, 2011
  | url = http://www.soundtrackcollector.com/title/25599/Days+Of+Heaven
  | accessdate = March 27, 2012 }} 

Additional songs were contributed by guitarist Leo Kottke. Kottke was originally approached by Malick for the entire score, but declined. {{cite web url        =http://acousticeclectic.org/transcripts/leotalk.html title      =AcousticEclectic.org - Leo Kottke Interview last       =Bost first      =Otto date       =March 1, 1999 website    =acousticeclectic.org publisher  =acousticeclectic.org accessdate =August 11, 2013}} 

==Reception==
===Initial reaction=== Best Director—making him the first American director to win the award since Jules Dassin in 1955 for Rififi (in a joint win shared with two other directors). Technically, the film was a commercial failure: its box office gross of $3,446,749 was only slightly more than it cost to make the film ($3 million).  

Critical reaction initially varied, with many critics polarised by the beauty of the film, however finding a flaw in a perceived weakness of the story. Dave Kehr of The Chicago Reader offered a positive review and wrote: "Terrence Malicks remarkably rich second feature is a story of human lives touched and passed over by the divine, told in a rush of stunning and precise imagery. Nestor Almendross cinematography is as sharp and vivid as Malicks narration is elliptical and enigmatic. The result is a film that hovers just beyond our grasp—mysterious, beautiful, and, very possibly, a masterpiece". {{cite news
  | last = Kehr
  | first = Dave
  | title = Days of Heaven
  | work = The Chicago Reader
  | pages =
  | publisher =
  | year = 1978
  | url = http://onfilm.chicagoreader.com/movies/capsules/2482_DAYS_OF_HEAVEN
  | accessdate = December 11, 2008
  | archiveurl = http://www.webcitation.org/5vB9tXN6P
  | archivedate = December 23, 2010 }}  
Variety Magazine called the film "one of the great cinematic achievements of the 1970s." {{cite news
  | last = 
  | first = 
  | title = Days of Heaven
  | work = Variety Magazine
  | pages =
  | publisher =
  | date = October 23, 2007
  | url = http://www.variety.com/review/VE1117790271.html?categoryid=31&cs=1
  | accessdate = March 29, 2012}}  Gene Siskel of The Chicago Tribune also wrote that the film "truly tests a film critics power of description ... Some critics have complained that the Days of Heaven story is too slight. I suppose it is, but, frankly, you dont think about it while the movie is playing". {{cite news
  | last = Siskel
  | first = Gene
  | title = Days of Heaven
  | work = Chicago Tribune
  | pages =
  | publisher =
  | date = October 9, 1978
  | url =
  | accessdate = }}  Time (magazine)|Time magazines Frank Rich wrote, "Days of Heaven is lush with brilliant images". {{cite news
  | last = Rich
  | first = Frank
  | title = Days of Heaven Time
  | pages =
  | publisher =
  | date = September 18, 1978
  | url = http://www.time.com/time/magazine/article/0,9171,916396,00.html
  | accessdate = September 9, 2009
  | archiveurl = http://www.webcitation.org/5vBA4XKHV
  | archivedate = December 23, 2010 }}  The periodical went on to name it one of the best films of 1978. {{cite news
  | last =
  | first =
  | title = Cinema: Years Best Time
  | pages =
  | publisher =
  | date = January 1, 1979
  | url = http://www.time.com/time/magazine/article/0,9171,916590,00.html
  | accessdate = September 9, 2009
  | archiveurl = http://www.webcitation.org/5vBA6v6Hh
  | archivedate = December 23, 2010 }}  Nick Schager of Slant Magazine has called it "the greatest film ever made." {{cite news
  | last = Schager
  | first = Nick
  | title = Days of Heaven review
  | work = Slant Magazine
  | pages =
  | publisher =
  | date = October 22, 2007
  | url = http://www.slantmagazine.com/film/review/days-of-heaven/3213
  | accessdate = December 23, 2010
  | archiveurl = http://www.webcitation.org/5vBAIbjN1
  | archivedate = December 23, 2010 }} 

Meanwhile, detractors targeted the films direction of storyline and structure. In his review for The New York Times, Harold C. Schonberg wrote, "Days of Heaven never really makes up its mind what it wants to be. It ends up something between a Texas pastoral and Cavalleria Rusticana. Back of what basically is a conventional plot is all kinds of fancy, self-conscious cineaste techniques." {{cite news
  | last = Schonberg
  | first = Harold C
  | title = Days of Heaven
  | work = The New York Times
  | pages =
  | publisher =
  | date = September 14, 1978
  | url = http://movies.nytimes.com/movie/review?_r=2&res=EE05E7DF173EE767BC4C52DFBF668383669EDE&partner=Rotten%20Tomatoes
  | accessdate = December 11, 2008 }}  Additionally, Monica Eng of the Chicago Tribune criticised the lack of significant plot and stated "the story becomes secondary to the visuals". {{cite news
| last = Eng
  | first = Monica
  | title = Days of Heaven
  | work = The Chicago Tribune
  | pages =
  | publisher =
  | date = October 9, 1978
  | url = http://pqasb.pqarchiver.com/chicagotribune/access/38116948.html?dids=38116948:38116948&FMT=ABS&FMTS=ABS:FT&type=current&date=Jan+17%2C+1999&author=Contributing%3A+Monica+Eng%2C+Allan+Johnson%2C+Lisa+Stein.&pub=Chicago+Tribune&desc=%22DAYS+OF+HEAVEN%22+OCT.+9%2C+1978&pqatl=google
  | accessdate = June 14, 2012 }} 

The Chicago Sun-Times’ Roger Ebert responded to these criticisms by saying:  Terrence Malicks "Days of Heaven" has been praised for its painterly images and evocative score, but criticized for its muted emotions: Although passions erupt in a deadly love triangle, all the feelings are somehow held at arms length. This observation is true enough, if you think only about the actions of the adults in the story. But watching this 1978 film again recently, I was struck more than ever with the conviction that this is the story of a teenage girl, told by her, and its subject is the way that hope and cheer have been beaten down in her heart. We do not feel the full passion of the adults because it is not her passion: It is seen at a distance, as a phenomenon, like the weather, or the plague of grasshoppers that signals the beginning of the end.  

===Contemporary reception=== Great Movies list.  In 2007, the Library of Congress selected the film for preservation in the United States National Film Registry.

In 2012, Time (magazine)|TIME included the film among the 20 new entries added to the magazines "Times All-TIME 100 Movies|All-TIME 100 Movies" list.  The same year, Days of Heaven ranked #112 in the British Film Institutes decennial Sight & Sound critics poll of the greatest films ever made,  and #132 in the directors poll of the same magazine.

===Awards=== Costume Design, Original Score, Sound (John John Wilkinson, Barry Thomas). Prix de la mise en scène (Best Director award) at the 1979 Cannes Film Festival. {{cite news
  | last =
  | first =
  | title = Sweeping Cannes Time
  | pages =
  | publisher =
  | date = June 4, 1979
  | url = http://www.time.com/time/magazine/article/0,9171,946279,00.html
  | accessdate = September 9, 2009
  | archiveurl = http://www.webcitation.org/5vBATPd59
  | archivedate = December 23, 2010 }}  Furthermore, he was named the best director by the National Society of Film Critics.

American Film Institute recognition
* AFIs 100 Years...100 Movies—Nominated 
* AFIs 100 Years...100 Passions - Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition)—Nominated 

===Home media===
Days of Heaven has been released on home video on various different formats over the years. Its first notable release was on home video in the early 1980s, followed by various reissues in the 1980s and 1990s. In particular, the film was released on a special widescreen edition format on home video to preserve the films original theatrical aspect ratio, which was uncommon for videotapes at the time, with majority of them being pan and scan, a technique that crops a portion of the image to focus on the more important composition. This often results in the side being cut out and the middle centre being the only remaining part. Days of Heaven premiered on DVD on March 30, 1999 with no special features. The feature itself was presented in widescreen and released by Paramount Pictures, the copyright owner of the film itself. It was rereleased on DVD in 2004, again without special supplements.

In 2007, the Criterion Collection released an exclusive special edition of the film, with digitally remastered sound and picture, supervised by Malick, editor Billy Weber and camera operator John Bailey. Bonus features include an audio commentary by art director Jack Fisk, editor Billy Weber, costume designer Patricia Norris, and casting director Dianne Crittenden, an audio interview with Richard Gere, video interviews with Sam Shepherd, Haskell Wexler and John Bailey, and a booklet featuring an essay on the film by Adrian Martin and an extract from Néstor Almendros autobiography. {{cite news
  | last =
  | first =
  | title = DVD Review: Days of Heaven - The Criterion Collection
  | work = Seattle Post Intelligencer
  | pages =
  | publisher =
  | date = March 28, 2010
  | url = http://www.seattlepi.com/ae/movies/article/DVD-Review-Days-of-Heaven-Criterion-Collection-886862.php
  | accessdate = June 14, 2012
  | archiveurl = 
  | archivedate = }}  The Criterion Collection also released a Blu-ray disc format of the film on March 7, 2010, with the same special features. The design art created by Criterion for the films packaging makes a departure from the early video releases, featuring a still of Geres character in the wheat fields, with the mansion in the horizon.

==Notes==
 

==References==
* Almendros, Nestor (1986) A Man with a Camera. Farrar, Straus and Giroux.
* Biskind, Peter (1998) Easy Riders, Raging Bulls. New York: Simon & Schuster.

==Further reading==
* Charlotte Crofts (2001), From the "Hegemony of the Eye" to the "Hierarchy of Perception": The Reconfiguration of Sound and Image in Terrence Malicks Days of Heaven, Journal of Media Practice, 2:1, 19-29.
* Terry Curtis Fox (1978), The Last Ray of Light, Film Comment, 14:5, Sept/Oct, 27-28.
* Martin Donougho (1985), West of Eden: Terrence Malicks Days of Heaven, Postscript: Essays in Film and the Humanities, 5:1, Fall, 17-30.
* Terrence Malick (1976), Days of Heaven, Script registered with the Writers Guild of America, 14 Apr; revised 2 Jun.
* Brooks Riley (1978), Interview with Nestor Almendros, Film Comment, 14:5, Sept/Oct, 28-31.
* Janet Wondra (1994), A Gaze Unbecoming: Schooling the Child for Femininity in Days of Heaven, Wide Angle, 16:4, Oct, 5-22.

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 