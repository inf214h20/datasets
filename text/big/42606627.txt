Çanakkale 1915
{{Infobox film
| name           = Çanakkale 1915
| image          = Canakkale 1915 poster.jpeg
| alt            = 
| caption        = 
| film name      =  
| director       = Yesim Sezgin
| producers      =  
| writer         = 
| screenplay     = Turgut Özakman
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Can Atilla
| cinematography =  
| editing        = Vanessa Taylor production companies =  
| distributor    = Tiglon Film
| released       =  
| runtime        = 128 minutes 
| country        = Turkey
| language       = Turkish
| budget         = $ 5,000,000
| gross          =   7.6 million  Turkish lira
}}

Çanakkale 1915 ( ) is a 2012   on March 17, 2015.   

==Plot==
The story of the film is about the Gallipoli Campaign during World War I on the Gallipoli Peninsula in Turkey in 1915.  The film covers the resurrection of Turkey following its defeat in the Balkan War, through depictions of Sergeant Mehmet Ali (Ali Ersan Duru) from Biga, Çanakkale|Biga, Corporal Seyit and many others.  To help Russia and invade İstanbul, Allied Forces attacked Çanakkale with one of the largest fleets ever assembled at that time. Through a series of historical sketches, the film documents how they were defeated despite many difficulties and hardships.

==Cast==
 
* Sevket Çoruh
* Baris Çakmak
* İlker Kızmaz
* Bülent Alkis
* Serkan Ercan
* Ufuk Bayraktar
* Emre Özcan
* Baran Akbulut as Velie
* Özgür Akdemir
* Riza Akin
* Ali Ersan Duru as Mehmet Ali
* Koray Kadiraga
* Mert Karabulut
* Fatma Karanfil
* Celil Nalcakan
* Ali Oguz Senol
 

==Critical response==
The film received generally favorable reception across Turkey.  Film critics were appreciative of the battle scenes, but found forgivable flaws with scripting and acting.     Reviewer Atilla Dorsay said it was an over the top drama which, while a bit over-done, was not bad for its kind in its depicting events of the greatest importance to the Turkish nation. Reviewer Mehmet Açar felt the resistance soul filled the audience, as when the film aimed to feel the spirit of resistance in Çanakkale, it managed to do so. While dialogue was problematic in drama scenes the battle scenes were successful.  Reviewer Kerem Akça felt the film was worth the effort and in places quite impressive, despite significant weaknesses with script and acting. Reviewer Uğur Vardan felt the real issue brought forth in the film was poverty in the film looking at both sides of imperialism. While the production was highly successful in items such as costume design, it was extremely weak in terms of side issues.     In speaking about various films releasing in Turkey to commemorate the centenary of the Battle of Gallipoli, Variety (magazine)|Variety noted that film Canakkale 1915 was "based on the bestselling historical fiction by Turgut Ozakman" and that it "focuses on the battle as a foundation for the Turkish Republic".     Hurriyet Daily News called the film "a sure hit in the box office".     Daily Sabah reported on Water Diviner and in speaking of three other recent Turkish films dealing with the Gallipoli battle, wrote "Çanakkale 1915 was the most successful of the three films",  screening for 43 weeks, drawing 918,181 viewers, and having a box office return of 7,607,755 Turkish lira.        Todays Zaman wrote "director Yeşim Sezgin’s newest Çanakkale 1915 might actually outdo Sinan Çetin’s Çanakkale Çocukları in terms of its aggression, opportunism and crooked reductionism."   The reviewer found it frustrating that some scenes "are so blatantly full of improbable valor that they border on the comical because the filmmakers have taken themselves too seriously as they leave behind any kind of introspection."   

==References==
 

==External links==
*  at the Internet Movie Database
Keynote: “Canakkale 1915” Hosted by FTAA-ATAA, CUNY John Jay College 25 March 2015. http://www.youtube.com/watch?v=5U-SC8zzyh0


 
 
 
 
 
 
 
 
 
 
 
 
 