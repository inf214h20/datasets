Cujo (film)
{{Infobox film
| name = Cujo
| image = CujoVHScover.jpg
| caption = Theatrical release poster
| director = Lewis Teague
| producer = Robert Singer Daniel H. Blatt 
| screenplay = Don Carlos Dunaway Lauren Currier
| based on = Cujo by Stephen King
| starring = {{Plainlist|
* Dee Wallace Daniel Hugh-Kelly
* Danny Pintauro
* Ed Lauter Christopher Stone
}} Charles Bernstein
| cinematography = Jan de Bont
| editing = Neil Travis Taft Entertainment Sunn Classic Pictures PSO International    
| released = August 12, 1983 
| runtime = 91 minutes
| country = United States
| language = English
| budget = $8 million
| gross = $21,156,152 (USA)
}} thriller film same name. It was directed by Lewis Teague, and written by Don Carlos Dunaway and Lauren Currier.   

==Plot==
The Trentons – Vic, Donna and their son Tad – are a normal suburban family. Vic works in advertising, Donna is a housewife and Tad is a sensitive little boy who has a fear of monsters in his closet. One day, the Trenton family takes their car to the rural home of abusive mechanic Joe Camber for some repairs, where they meet Cujo – the Camber familys easy-going St. Bernard (dog)|St. Bernard who has a bite on his nose that Donna notices, but thinks little of it. Vic and Donnas marriage is tested when Vic learns that Donna has been having an affair with her ex-boyfriend, Steve Kemp.
 rabid bat, drives him mad, and he kills the abusive Joe and his alcoholic neighbor, Gary Pervier. Vic goes out of town on a business trip. Donna and Tad return to the Cambers house for more car repairs and Cujo attacks them. Donna and Tad take shelter in their Ford Pinto, but the alternator dies and the two are trapped. Therefore, they are forced to stay inside their car while Cujo attacks repeatedly. The hot sun makes the conditions nearly unbearable and Donna knows that she must do something before they both die from heatstroke or dehydration. Attempts at escape, however, are foiled by the mad Cujos repeated attacks. Donna decides that she must risk leaving the car, but Cujo bites her on the leg and forces her back inside. Eventually, the badly wounded Donna makes another desperate escape attempt but is again attacked and must return to the car. Vic arrives home to rekindle his marriage, but finds out Donna and Tad are missing. He suspects the possessive Steve Kemp of kidnapping, but then realizes his wife and son might be at the Cambers residence. The local Sheriff comes to the house for a brief standoff, before Cujo kills him.
 impaled by the broken bat. Donna then breaks the windshield with the sheriffs revolver and retrieves Tad, as Cujo broke all of the door handles. As Donna revives the dehydrated and over-heated Tad in the house, a recovered Cujo breaks through the kitchen window and tries to kill them, but Donna kills him with the Sheriffs pistol. Vic arrives and is reunited with Donna and Tad.

==Cast==
* Dee Wallace as Donna Trenton
* Danny Pintauro as Tad Trenton Daniel Hugh-Kelly as Vic Trenton Christopher Stone as Steve Kemp
* Ed Lauter as Joe Camber
* Kaiulani Lee as Charity Camber Billy Jacoby as Brett Camber
* Mills Watson as Gary Pervier
* Jerry Hardin as Masen
* Sandy Ward as George Bannerman

==Production==
The original director was Peter Medak, who left the project two days into filming, along with his DOP Tony Richardson. They were replaced by Lewis Teague and Jan de Bont respectively. 
==Reception==
Reviews of the film were mixed, and a more recent collation of reviews on Rotten Tomatoes has earned Cujo a "rotten" rating of 59% based on 29 reviews. Eleanor Mannikka of the New York Times wrote that:

 

Cujo was a modest box office success for Warner Brothers.  The film was released August 12, 1983 in the United States, opening in second place that weekend.   It grossed a total of $21,156,152 domestically,  making it the fourth highest grossing horror film of 1983.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 