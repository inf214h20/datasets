Michael the Brave (film)
{{Infobox film
| name           = Michael the Brave
| image          = Mihai Viteazul1970.jpg
| caption        = 
| director       = Sergiu Nicolaescu
| producer       = 
| writer         = 
| starring       = Amza Pellea Ion Besoiu Olga Tudorache Sergiu Nicolaescu Ilarion Ciobanu Mircea Albulescu Florin Piersic Ioana Bulcă
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 203 minutes Romania
| language       = Romanian
| budget         = 
| gross          = 
}}
   Romanian historic Mihai Viteazu, principalities (Wallachia, Principality of Transylvania) in one country. The film was released in 1970 in Romania, and worldwide by Columbia Pictures as The Last Crusade.

==Plot==
 
 

==Cast==
*Amza Pellea        ... 	Mihai Viteazul
*Ion Besoiu	        ... 	Sigismund Báthory
*Olga Tudorache	... 	mother of Mihai Viteazul
*Irina Gardescu	... 	Contessina Rossana Viventini Andrei Báthory
*Sergiu Nicolaescu	... 	Selim Pasha Sinan Pasha
*Ilarion Ciobanu	... 	Stroe Buzescu
*Aurel Rogalschi	... 	Rudolf II
*Ioana Bulcă        ... 	Doamna Stanca
*Septimiu Sever	... 	Radu Buzescu
*Florin Piersic	... 	Preda Buzescu
*Klára Sebök	... 	Maria Cristina de Graz
*Mircea Albulescu	... 	Popa Stoica
*Emmerich Schäffer	... 	Giorgio Basta
*Colea Răutu        ...     Sultan Murat III Alexandru The Evil One
*Alexandru Herescu  ...     Ionică
*Corneliu Gârbea    ...     General Baba Novac
*Alexandru Repan    ...     Earl Viventini Archduke Maximilian

==Production==
 
The film was produced in 1970 after a script by Titus Popovici. It starred Amza Pellea in the lead role, while the cast included a number of the best Romanian actors at the time, including Sergiu Nicolaescu, Ion Besoiu, Olga Tudorache, Florin Piersic, Ilarion Ciobanu, Silviu Stănculescu, and Mircea Albulescu.   

The film had initially been intended to be an American-Romanian superproduction, with Columbia Pictures proposing actors such as Charlton Heston, Orson Welles, Laurence Harvey, Elizabeth Taylor, Richard Burton, or Kirk Douglas. But at Nicolae Ceauşescus intent, the production was approved only with Romanian actors.   

The story, recognized for its historical accuracy, is full of grand battle scenes, political plots, betrayals and family drama.  To reenact the battles that Mihai Viteazul fought against the Turks, some 5,000 soldiers of the Romanian army were brought to the set.    Some reports put this number around 10,000 soldiers.    The film has two parts and was shot in several locations, such as Istanbul, Prague and Calugareni,  but also the Danube, the Black Sea, Alba Iulia, Carpathian Mountains, Bucharest, Sibiu, Sinaia, and Miraslau.   
The budget at the time was around 14 million Romanian leu|lei,  which in 2010 was estimated to be worth to be around 500,000 dollars, a relatively high sum for its period. 

==Release==
The film was released in Romania in 1970 and ran in the cinemas for several years. Worldwide, it was distributed by Columbia Pictures, under the name The Last Crusade.      The film debuted outside of Romania in July 1971 at the 7th Moscow International Film Festival and in East Germany, and in March 1972 in Finland, in June 1972 in West Germany and in April 1973 in Japan. 

In 2000, the soundtrack of the film was redone in Dolby Surround.  It is estimated that the film will be re-released in Blu-Ray format sometime in 2010. 

==Reception== El Cid.    

With a rating on 8.4 on Internet Movie Database,  it is rated as the twentieth best historic film of all time. It is considered 18th in the biography category,  40th in the war category,  34th in the action category,.  The director himself, Sergiu Nicolaescu, declared that he was impressed by the rating especially as it is an American website and the film was released four decades before. 

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Romanian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
*http://wondersinthedark.wordpress.com/2009/06/12/mihai-viteazul-no-49/

 

 
 
 
 
 
 
 
 
 