Killing Heat
{{Infobox film
| name           = Killing Heat
| image          = Killing_heat.jpg
| image_size     =
| caption        = English-language release
| director       = Michael Raeburn
| producer       = Mark Forstater Catharina Stackelberg
| writer         = Doris Lessing Michael Raeburn
| based on       =  
| starring       = Karen Black John Thaw John Kani Patrick Mynhardt John Moulder Brown Margaret Heale
| music          = Lasse Dahlberg Björn Isfält
| cinematography = Bille August
| editing        = Thomas Schwalm
| distributor    = Chibote Swedish Film Institute
| released       = 2 September 1982 (Australia)
| runtime        = 105 minutes
| country        = Sweden Australia Zambia
| language       = English
}}
 Doris Lessings 1950 novel, The Grass Is Singing. It stars Karen Black and  John Thaw and was filmed in Zambia.

== Plot ==
The film takes place in Southern Rhodesia in the 1940s. Mary, a city woman, marries a farmer named Dick Turner. Mary is pulled from the comforts of her cosmopolitan life and forced to live on Dicks unsuccessful farm. Mary slowly becomes insane and has a sexual affair with her black servant, Moses. When Mary and Moses affair is discovered Mary asks Moses to leave the farm. Moses returns and murders Mary. The film deals with the issues of colonialism, the white mans role in Africa, and the relationships between the races and genders.   

== Cast ==
* Karen Black as Mary Turner
*John Thaw as Dick Turner
*John Kani as Moses
*Patrick Mynhardt as Charlie Muller
*John Moulder Brown as Tony Marston
*Margaret Heale as Ellen Muller

==References==

 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 