Namets!
{{Infobox film
| name         = Namets!
| image        = Namets.png
| caption      = Theatrical poster
| director     = Jay Abello
| producer     = Vicente Garcia Groyon   Fiona Borres   Marya Ignacio   Tara Illenberger
| writer       = Vicente Garcia Groyon 
| starring     = Christian Vasquez Angel Jacob Peque Gallaga Dwight Gaston Monsour del Rosario
| music        = Vince de Jesus
| cinematography = Anne Monzon
| editing       = Fiona Borres
| distributor  = Videoflick Productions
| released     =  
| runtime      = 90 minutes
| country      = Philippines Hiligaynon / English Php 1 million 
| gross        = 
}}
 Filipino independent Hiligaynon word namit, which means "yummy" or "delicious".

==Plot== Negrense cuisine instead of Italian. The pair then set off across the province re-discovering the unique aspects of Negrense food.

==Cast==
*Jacko Teves: Christian Vasquez
*Cassie Labayen: Angel Jacob
*Boss Dolpo: Peque Gallaga
*Oscar: Dwight Gaston
*Rodrigo Labayen: Louie Zabaljauregui
*Imelda Teves: Michelle Gallaga
*Nena Teves: Marivic Lacson
*Babyboy Labayen: Monsour del Rosario
*Farmer: Ronnie Lazaro
*Caveman: Joel Torre

===Production=== Hiligaynon with English subtitles. 

==References==
 

==External links==
* 
* 

 
 