Wild Poses
{{Infobox film
| name           = Wild Poses
| image          = Wild poses TITLE.JPEG
| caption        = 
| director       = Robert F. McGowan
| producer       = F. Richard Jones Hal Roach
| writer         = Carl Harbaugh Hal Roach H. M. Walker Hal Yates Matthew Beard Jerry Tucker Tommy Bond Emerson Treacy Gay Seabrook Franklin Pangborn Stan Laurel Oliver Hardy
| music          = Marvin Hatley Leroy Shield
| cinematography = Francis Corby
| editing        = William H. Terhune
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 18 14" 
| country        = United States
| language       = English
}}
Wild Poses is short subject in the Our Gang (The Little Rascals) series.
It was produced and directed by Robert F. McGowan for Hal Roach Studios and first released on October 28, 1933 by Metro-Goldwyn-Mayer.    It was the 125th Our Gang short that was released.

A sequel to the previous Our Gang short, Bedtime Worries, Wild Poses features a brief cameo by Laurel & Hardy.

== Plot ==
Otto Phocus (Franklin Pangborn) is a haughty photographer hellbent on taking a formal portrait of a terrified Spanky (George McFarland). The little guy has been told by the gang that Phocus plans to "shoot" him; thinking the camera is a cannon. This leads Spanky to avoid having his picture taken, and his habit of punching Phocus in the face with regularity.

Phocus serves as Spankys foil in other ways as well. He tries to get Spanky to pose with an exaggerated sweet smile on his face; when Spanky sees Phocus ridiculous grimace he turns to his Dad (Emerson Treacy) and says, "Hey Pop, do you see what I see?" Later, when Spankys friends have filled the rubber shutter bulb with water, and Phocus squeezes it, squirting Spankys Dad with water, his Mom (Gay Seabrook) tells Spanky, "Thats how they take watercolor pictures."
Finally, after having successfully taken Spankys picture, Phocus discovers the gang exposed his photographic plates, rendering "all my lovely work for nothing!". Spanky manages to give him one more "buss" before his family leaves the studio in disgust.

==Cast==
===The Gang===
* George McFarland as Spanky Matthew Beard as Stymie
* Tommy Bond as Tommy
* Jerry Tucker as Jerry

===Additional cast===
* Gay Seabrook as Gay, Spankys mother
* Emerson Treacy as Emerson, Spankys father
* Franklin Pangborn as Otto Phocus, the portrait photographer
* Georgie Billings as Georgie
* Stan Laurel and Oliver Hardy as Babies
* George Stevens, Jr. as Role unknown (scenes deleted)

== Laurel and Hardy cameo ==
At the beginning of the film, a salesman is seen soliciting Otto Phocus services throughout a residential neighborhood. At one home, he tells a housewife that she has "two of the most photogenic children" he has ever seen. 

The camera cuts to reveal the womans two children, portrayed in a brief cameo by Laurel and Hardy, dressed in baby clothes and using giant sets from their short Brats (1930). Laurel and Hardy briefly fight over a baby bottle, until Laurel eye-pokes Hardy and emerges victorious as the scene transitions to set the main plot in motion.

==Notes== short directed Robert McGowan until 1936s "Divot Diggers".

==See also==
* Our Gang filmography

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 