Kill Cruise
{{Infobox film |
| name = Kill Cruise  Der Skipper (German title)
| image = 
| caption = 
| director = Peter Keglevic
| writer = Peter Keglevic
| starring = Jürgen Prochnow Patsy Kensit Elizabeth Hurley
| producer = Matthias Wendlandt
| music = Brynmor Llewelyn Jones
| cinematography = Edward Klosinski
| editing = Susanne Schett
| distributor = Rialto Pictures
| released = April 5, 1990 (Germany)
| runtime = 98 min.
| awards = 
| preceded_by =
| followed_by =
| country = Germany German English English
}} German title Der Skipper) is a 1990 film directed by Peter Keglevic and starring Jürgen Prochnow, Patsy Kensit and Elizabeth Hurley.

==Plot summary==
The story begins at sea when the unnamed Skipper (Prochnow) of the sailboat Bella Donna reports a man overboard. The victim, Paul Pelikan, drowns before he can be rescued. People in the Bella Donnas home port of Gibraltar suspect that the Skipper deliberately pushed Paul overboard because hed had an affair with the Skippers wife Mona (Grażyna Szapołowska). Though nothing is proven, Mona believes the Skipper did in fact throw Paul overboard and leaves him. Heartbroken, the Skipper spends most of his time at the local bars getting drunk and swearing he will sail again, though the townspeople dont take his claim seriously. 

Meanwhile, two English girls Su (Kensit) and Lou (Hurley) are barely making a living singing at local clubs. Though fast friends, there seems to be tension between the two. They put on a show at the California Bar that night, singing an English language version of Porque te vas (translated as Youre Leaving Me). The Skipper is present for the performance and falls hard for the two, especially after Lou performs an impromptu striptease at the end. After spending the night with the two girls, the Skipper agrees to their request to sail them to Barbados. 

Though the voyage will take four weeks, all seems to be well at the start. The Skipper does express some concern in his ships log about how close the two girls are, then injects himself with a syringe. As time passes, Su flirts with the Skipper, but becomes upset when her advances are ignored, revealing a somewhat unbalanced personality. As tensions further mount, Su snoops through the Skippers quarters and finds his serum bottles, syringes and his log book. She tells Lou that she thinks the Skipper is a murderer and a drug addict. When he discovers the missing serum, which turns out to be insulin, he throws Su overboard with a line, in an attempt to get her to reveal where she hid it. He pulls her back in and gives her an ultimatum, "Ill give you one hour". He soon becomes very ill and incapacitated. A desperate Lou eventually finds the insulin and injects the Skipper, saving his life. A grateful Skipper later confesses his love for Lou. Lou responds "What about Su?" The Skipper replies "What about Su?" Su then walks in on them having sex. Su is heartbroken and starts crying. Lou, feeling guilty, tries to comfort Su.

Things seem to have smoothed over at the end, when the girls reprise their act, singing "Youre Leaving Me" again. Without warning, Lou shoots the Skipper with a spear gun, killing him almost instantly. In terror, Su flees below deck. Lou takes a moment to draw a heart and arrow in the Skippers pooling blood before going to the galley to make a cup of tea. She seems oddly calm while Su is quaking in terror over whats happened. Lou tries to calm Su down but ends up smothering her with a pillow. 

An end title states that, three weeks later, the Bella Donna had drifted to the Caribbean island of St. Lucia, with no one aboard.

==Production details==
*Filmed on location in Gibraltar and Malta.

==Critical reception==
*"Trivial thriller, psychologically implausible, with a weak acting performance." ("Trivialer Thriller, psychologisch unglaubwürdig und mit schwachen schauspielerischen Leistungen."), Lexikon des internationalen Films 

*"Logic goes overboard pretty early..." ("Die Logik geht ziemlich früh über Bord..."), Cinema 

==External links==
* 

==References==
 

 
 
 
 
 