Snuff (film)
 
{{Infobox film
| name           = Snuff
| image          = Poster of the movie Snuff.jpg
| image_size     =
| caption        = Roberta Findlay Horacio Fredriksson
| producer       = Jack Bravman Allan Shackleton
| writer         = Michael Findlay Roberta Findlay A. Bochin
| narrator       =
| starring       = Margarita Amuchástegui Ana Carro Liliana Fernández Blanco Roberta Findlay Alfredo Iglesias Enrique Larratelli Mirtha Massa Aldo Mayo Clao Villanueva
| music          =
| cinematography =
| editing        =
| distributor    = Monarch Releasing Corporation Blue Underground (DVD)
| released       = 1976
| runtime        = 80 min.
| country        = U.S.A. / Argentina
| language       = English
| budget         = $30,000
| preceded_by    =
| followed_by    =
}}

Snuff is a 1976 splatter film, and is most notorious for being marketed as if it were an actual snuff film.   This picture contributed to the urban legend of snuff films, although the concept did not originate with it.
==Plot==
 
==Production== Michael and Roberta Findlay. Filmed in Argentina in 1971 on a budget of $30,000,  it depicted the actions of a Charles Manson|Manson-esque murder cult, filmed mainly in silence due to the actors understanding very little English language|English. The film financier Jack Bravman received an out-of-court settlement from AIP so the latter could use the title for the 1972 Jim Brown film of the same name. The Findlays film enjoyed a very limited theatrical release. 

Independent low-budget distributor and sometime producer Allan Shackleton took the film and shelved it for four years—but was inspired to release it with a new ending, unbeknownst to the original filmmakers, after reading a newspaper article in 1975 on the rumor of snuff films produced in South America and decided to cash in on the urban legend. He added a new ending, filmed in a Cinéma vérité|vérité style by Simon Nuchtern,  in which a woman is brutally murdered by a film crew, supposedly the crew of Slaughter.   The new footage purportedly showed an actual murder, and was spliced onto the end of Slaughter with an abrupt cut suggesting that the footage was unplanned and the murder authentic.  This new version of the film was released under the title Snuff, with the tagline "The film that could only be made in South America... where Life is CHEAP". 

Once the film was released, distributor Shackleton reportedly hired fake protesters to picket movie theaters showing the film.  This soon became moot when Women Against Pornography began staging real protests, outraged at the films purported imagery of sexual violence. The groups protest received coverage by such media outlets as the CBS Evening News.

==Hoax==
Although the film was exposed as a hoax in Variety (magazine)|Variety in 1976, it became popular in New York, Philadelphia, Los Angeles and Boston. 

The rumours persisted that the film showed a real-life murder. " rompted by complaints and petitions from well-known writers, including Eric Bentley and Susan Brownmiller, and legislators", an investigation began into the circumstances surrounding the films production  conducted by New York District Attorney Robert M. Morgenthau, who dismissed the supposedly "real" murder as "nothing more than conventional trick photography—as is evident to anyone who sees the movie".   Morgenthau reassured the public that the actress being dismembered and killed in the ending of the film "is alive and well", having urged the police to trace her. 

==Cast==
* Margarita Amuchástegui as Angelica
* Ana Carro as Ana
* Liliana Fernández Blanco as Susanna Roberta Findlay as Carmela
* Alfredo Iglesias as Horsts father
* Enrique Larratelli as Satan
* Mirtha Massa as Terry London
* Aldo Mayo as Max Marsh
* Clao Villanueva as Horst Frank
* Michael Findlay as Detective (uncredited)

==Further reading==
* Kerekes, David & Slater, David (1994). Killing For Culture. Creation Books.  ISBN 1-871592-20-8
* Johnson, Eithne & Schaefer, Eric. "Soft Core/Hard Gore: Snuff as a Crisis in Meaning," in Journal of Film and Video, University of Illinois Press, (Volume 45, Numbers 2-3, Summer-Fall, 1993): pages 40–59.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 