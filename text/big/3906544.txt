The Devil Bat
{{Infobox Film |
  name     = The Devil Bat|
  image          = Devilbatposter.jpg|
  director       = Jean Yarbrough |
  writer         = George Bricker John T. Neville | Dave OBrien Guy Usher |
  producer       = |
  distributor    = Producers Releasing Corporation | 1940 Weaver, Tom (1993). "The Devil Bat (PRC, 1940)" in Poverty Row Horrors! Monogram, PRC and Republic Horror Films of the Forties. Jefferson, North Carolina: McFarland & Co. ISBN 0-89950-756-5. p. 14.  |
  runtime        = 68 m |
  language = English language|English|
  budget         = |
}} horror movie Yolande Mallott, Dave OBrien Donald Kerr as the protagonists. The film later had a 1946 sequel titled Devil Bats Daughter.

==Plot==
The story involves a small town cosmetic company chemist (Lugosi) who is upset at his wealthy employers, because he feels they have denied him his due share of company success. To get revenge, he breeds giant bats. He then conditions them to kill those wearing a special shaving|after-shave lotion he has concocted. He cleverly distributes the lotion to his enemies as a "test" product.  

Once they have applied the lotion, the chemist then releases his Devil Bats in the night, which kill his two former partners and three members of their families. A hot shot big city reporter gets assigned by his editor to cover and help solve the murders. He (OBrien) and his bumbling photographer (Kerr) begin to unwind the mystery with some comic sidelights. The mad chemist is, predictably, done in by his own shaving lotion, and by his own creation—the dreaded Devil Bat.

==Cast ==
 
 
* Béla Lugosi as Dr. Paul Carruthers
* Suzanne Kaaren as Mary Heath Dave OBrien as Johnny Layden
* Guy Usher as Henry Morton
* Yolande Donlan (credited as Yolande Mallott) as Maxine
* Donald Kerr as "One-Shot" McGuire
* Edward Mortimer as Martin Heath
* Gene ODonnell as Don Morton
* Alan Baldwin as Tommy Heath
* John Ellis as Roy Heath Arthur Q. Bryan as Joe McGinty
* Hal Price as Police Chief Wilkins John Davidson as Prof. Percival Garland Raines
* Wally Rairdon as Walter King

==Production== poverty row studios first horror film. Weaver (1993). p. 15.  The shooting of the film began a little more than one week later.  PRC was known for shooting its films quickly and cheaply, but for endowing them with a plentiful amount of horror,  and The Devil Bat established this modus operandi. 

==Current status==
Following its theatrical release, The Devil Bat fell into public domain and since the advent of home video, has been released in countless truncated, poorly edited video and DVD editions.

In 1990, the film was restored from original 35mm elements by Bob Furmanek and released on laserdisc by Lumivision.
 colorized version. Both the restored black-and-white and colorized versions were subsequently released on DVD. 

==Reception==
In the 1993 book Poverty Row Horrors!, Tom Weaver judges The Devil Bat as one of Lugosis best films for the poverty row studios. 

==See also==
 
* Béla Lugosi filmography

==References==
 

== External links ==
*  
*  
*  
*  
*  

==Further reading==
* Weaver, Tom (1993). "The Devil Bat (PRC, 1940)" in Poverty Row Horrors! Monogram, PRC and Republic Horror Films of the Forties. Jefferson, North Carolina: McFarland & Co. ISBN 0-89950-756-5. pp.&nbsp;14–25.
   
 

 
 
 
 
 
 
 