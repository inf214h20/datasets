Godzilla vs. SpaceGodzilla
{{Infobox film
| name           = Godzilla vs. SpaceGodzilla
| image          = GXSG.jpg
| caption        = Japanese theatrical release poster
| director       = Kensho Yamashita
| producer       = Tomoyuki Tanaka Shogo Tomiyama
| writer         = Hiroshi Kashiwabara
| starring       = Megumi Odaka Jason Case Jun Hashizume Zenkichi Yoneyama Akira Emoto Towako Yoshikawa Kenpachiro Satsuma
| music          = Takayuki Hattori
| cinematography = Masahiro Kishimoto
| studio      =  Toho
| distributor    = Toho TriStar Pictures  (Sony Pictures Entertainment) 
| released       =    
| runtime        = 108 minutes
| country        = Japan
| language       = Japanese
| budget         = US $10.3 million
| box office     = US $20 million
}}
 science fiction kaiju film produced by Toho. Directed by Kensho Yamashita and featuring special effects by Koichi Kawakita, the film starred Megumi Odaka, Jason Case, Jun Hashizume, and Akira Emoto. It was the twenty-first film in the Godzilla series. The film featured Godzilla battling an evil doppelgänger from outer space called SpaceGodzilla (Jason Case).
 Columbia Tristar Home Video.

A manga adaptation was also produced shortly before the films release; it was written by Kanji Kashiwabara and illustrated by Takayuki Sakai, published by Shogakukans Ladybug Comics line. 

==Plot== Biollante and Mothra are exposed to intense radiation from a black hole. The celestial fission creates a highly aggressive extraterrestrial beast closely resembling Godzilla. This "SpaceGodzilla" quickly makes its way to Earth.

Meanwhile, a group of soldiers and scientists are setting up at Birth Island to try Project T against Godzilla, who has taken up residence on the isle. The plan is to plant a device on Godzilla which will allow the Japan Self Defense Forces to control the mutant dinosaur telepathically. With help from psychic Miki Saegusa, the project is put into action, but it ultimately fails. Afterward, the Cosmos, Mothras twin priestesses, appear to Miki and warn her of SpaceGodzillas arrival. With Mothra gone to space, the world will have to rely on Godzilla to stop the invader. SpaceGodzilla gets closer and closer to Earth, destroying a NASA space station along the way. M.O.G.U.E.R.A., a giant penguin-like robot built by the JSDF to replace the Mechagodzilla, is sent in to intercept SpaceGodzilla, but the alien uses a number of psychic attacks to cripple the robot, forcing it to retreat.
 Little Godzilla. SpaceGodzilla mercilessly attacks the small creature when he approaches it curiously, and Godzilla comes to his sons rescue. Godzilla puts up a good fight, but finds itself overwhelmed by SpaceGodzillas power and is knocked out. With Godzilla temporarily subdued, SpaceGodzilla creates a small cage made of crystals and traps Little Godzilla inside of it. It then leaves for Japan, intending to destroy it. Godzilla, after failing to free its son from the cage, pursues SpaceGodzilla.

Shortly thereafter, the Yakuza capture Miki and brings her back to their base in Fukuoka in an attempt to use Project T to gain control of Godzilla. Fortunately, a recovery team is successfully dispatched and Miki and the team escape before SpaceGodzilla arrives and destroys the building. SpaceGodzilla lands in central Fukuoka and forms a massive fortress of celestial crystals. M.O.G.U.E.R.A. arrives to once again fight SpaceGodzilla, but is still no match for it. Godzilla arrives in Kagoshima Bay and fights SpaceGodzilla, but SpaceGodzillas cosmic powers easily allow it to gain the upper hand.

The JSDF discovers that SpaceGodzilla is using Fukuoka Tower as a power converter, using it to transform the Earths core into an energy that SpaceGodzilla can absorb, slowly killing the planet. While Godzilla wrestles with SpaceGodzilla, M.O.G.U.E.R.A. splits into two different mechs: the Star Falcon, a flying battleship, and the Land Moguera, a tank. The mechs damage the crystal fortress while Godzilla pushes over Fukuoka Tower, cutting off SpaceGodzillas energy supply. M.O.G.U.E.R.A. quickly reforms and blasts off SpaceGodzillas crystal-like shoulder formations, weakening it. Enraged, SpaceGodzilla beats M.O.G.U.E.R.A. into submission and starts to throttle Godzilla, who further wounds SpaceGodzilla by drilling into its neck.

With SpaceGodzilla brutally beaten and sporting a hole in its neck, M.O.G.U.E.R.A. uses up the last of its power supply by ramming into the alien beast, but SpaceGodzilla impales M.O.G.U.E.R.A. with its tail and hurls it into the remains of the crystal fortress. Godzilla blasts SpaceGodzilla with its atomic ray, sending SpaceGodzilla crashing on top of M.O.G.U.E.R.A.s remains. With a supercharged atomic blast, Godzilla finally eradicates SpaceGodzilla, and cosmic energy floats from the inferno and vanishes in the atmosphere.

Having won the fight, Godzilla makes its way back to Birth Island. Before Godzilla departs, Miki uses her psychic powers to remove the mind control device from Godzillas neck; it turns to her and nods in gratitude. Little Godzilla is then free from the crystal prison and can blow bubbles of fire.

==Cast==
* Megumi Odaka as Miki Saegusa
* Jun Hashizume as Lieutenant Koji Shinjo
* Zenkichi Yoneyama as Lieutenant Kiyoshi Sato
* Akira Emoto as Major Akira Yuki
* Towako Yoshikawa as Professor Chinatsu Gondo
* Yōsuke Saitō (actor)|Yōsuke Saitō as Doctor Susumu Okubo
* Kenji Sahara as Minister Takayuki Segawa
* Akira Nakao as Commander Takaki Aso
* Koichi Ueda as Commander Hyodo
* Sayako Osawa and Keiko Imamura as the Cosmos
* Ronald Hoerr as Professor Alexander Mammilov
* Tom Durran as Yokuza Boss McKay
* Kenpachiro Satsuma as Godzilla, the King of the Monsters and the main monster protagonist who defeats SpaceGodzilla in order to free his son. Little Godzilla, Godzillas son whos been imprisoned by SpaceGodzilla.
* Jason Case as SpaceGodzilla, the main antagonist of the film who was created by Godzillas cells transported into space by Biollante and Mothra being sent through a black hole and is now bent on killing Godzilla.
* Wataru Fukuda as Moguera|M.O.G.U.E.R.A, a transforming anti-Godzilla mech that aids Godzilla in defeating SpaceGodzilla, and it is the second protagonist of the film.

==English Version== international version of the movie, an English title card was superimposed over the Japanese title, as had been done with the previous 90s Godzilla films.
 Columbia TriStar Home Entertainment released Godzilla vs. SpaceGodzilla and Godzilla vs. Destoroyah on home video on January 19, 1999. This was the first time either film had been officially released in the United States. TriStar used the Toho dubs, but cut the end credits and created new titles and opening credits for both films. Tohos complete international version of Godzilla vs. SpaceGodzilla (sans any onscreen text besides the English title) has been broadcast on several premium movie channels since the early 2000s.

==Box office==
Released on December 10, 1994, the film sold approximately 3,200,000 tickets in Japan and grossed around $20,000,000 (U.S) (US$32,000,000 world wide).

==Critical reaction==
Critical reaction to Godzilla vs. SpaceGodzilla has been mostly mixed. Monster Zero called the film "a curiously uninvolving effort" that "disappoints in nearly all aspects of the production"
  American Kaiju criticized the "wildly uneven pacing," "uneven special effects," and "exceedingly lumpy story," but added that "most of the special effects are pretty fair" and "the monster battles are mostly fun."  DVD Cult said, "It does have some great destruction scenes and monster battles; two things that make these films worthwhile to begin with. The monster SpaceGodzilla is excellently designed, and is certainly far more menacing than anything Dean Devlin and Roland Emmerich ever dreamed up."  Toho Kingdom said the film is "far from terrible" and "an underrated movie" but felt it suffered from an "overly complicated story," "underdeveloped characters," and "forgettable" music. 

==Home Media releases==

Sony - Blu-ray (Toho Godzilla Collection)  
* Released: May 6, 2014
* Picture: AVC-1080P 
* Sound: Japanese and English (5.1 DTS)
* Subtitles: English (Dubtitles) and French
* Extras: Teasers and Theatrical Trailers (7 minutes)  
* Notes: This is a 2-Disc double feature with Godzilla vs. Mechagodzilla II.

DVD

Columbia/Tristar Home Entertainment
* Released: February 1, 2000
* Aspect Ratio: Widescreen (1.85:1) Anamorphic  
* Sound: English (2.0)
* Case type: Keep Case
* Region 1
* Note: A double feature with Godzilla vs. Destoroyah. On the U.S. DVD release, the final scene in which Godzilla is in the water while Echoes of Love (Date of Birth) plays is cut; however, it is left in the TV, on demand and Japanese DVD versions.

Universe Laser
* Released: November 24, 2006
* Aspect Ratio: Widescreen
* Sound: (Japanese, Cantonese) Dolby Digital Stereo
* Subtitles English, Chinese (Traditional/Simplified)
* Region 3

== Awards ==
In 1995, the film won the Best Grossing Films Award - Silver Award.

==References==
 

==External links==
*  
*  
*  
*  
*  page 1 Godzilla vs. SpaceGodzilla/Godzilla vs. Destoroyah toho kingdom page 2

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 