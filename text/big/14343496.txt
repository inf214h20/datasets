The Owl (film)
The 1991 action genre television movie. The film was directed by an "Alan Smithee", and it was inspired by the 1984 novel of the same name   written by Bob Forward, who also wrote the screenplay. The film starred Adrian Paul, Patricia Charbonneau, Brian Thompson and Erika Flores.  . Retrieved November 21, 2007. 

The film was only released in Sweden, on November 18, 1998, because no United States television network wanted the film. 

== Plot ==

Alex LHiboux, a ruthless mercenary-cum-vigilante, is known as the Owl because he never sleeps. His insomnia comes from a combination of a medical disorder and recurring nightmares of the murder of his wife and daughter. Alex is approached by Lisa, a young girl whose father is missing. She awakens painful memories of his own child, but after some persuasion from a policewoman friend, he agrees to help her.

== References ==
 

 
 
 
 
 


 
 