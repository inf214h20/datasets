Seed (2007 film)
{{Infobox film
| name           = Seed
| image          = Seedposter.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Uwe Boll
| writer      = Uwe Boll
| producer   = Uwe Boll   Dan Clarke   Shawn Williamson
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Andrew Jackson
| music          = Jessica de Rooij
| cinematography = Mathias Neumann
| editing        = Karen Porter Boll KG Productions    Pitchblack Pictures
| distributor    = Vivendi Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         = $10 million
| gross          = 
}}

Seed is a 2007 Canadian horror film written, produced, and directed by Uwe Boll.  Filming ran from July 17 to  August 11, 2006 in British Columbia, Canada, on a $10 million budget.

== Plot ==

As a boy, a reclusive and antisocial Sufferton resident, Max Seed, was disfigured in a school bus crash that killed everyone else involved in it. In 1973, Seed began torturing and murdering people, filming some of his victims starving to death in his locked basement, and ultimately racking up a bodycount of Number of the Beast|666. In 1979, Seed is arrested by Detective Matt Bishop in a siege that claims the lives of five of Bishops fellow officers. Seed is sentenced to death by electric chair, and incarcerated on an island prison, where he is a model inmate, only acting out when he kills three guards who try to rape him.

On Seeds execution date, the electric chair fails to kill him after two shocks. Not wanting Seed to be released due to a state law that says any convicted criminal who survives three jolts of 15,000 volts each for 45 seconds walks, the prison staff and Bishop declare Seed dead, and bury him in the prison cemetery. A few hours later, Seed digs his way out of his grave and returns to the prison, where he kills the executioner, doctor, and warden before swimming back to the main land. The next day, while investigating the massacre, Bishop realizes Seed was responsible when he discovers the serial killers empty cemetery plot.
 long shot showing him beating a bound woman with a lumberjacks axe for five straight minutes. One day, a videotape showing Bishops house is sent to the detectives office. Knowing this means Seed is going to go after his family, Bishop races home, finding his wife, Sandy, and daughter, Emily, gone, and the four officers charged with guarding the house dismembered in the bathroom.

Driving to Seeds old residence, Bishop is lured into a basement room containing a television and a video camera, and locked inside. The television turns on, and depicts Seed with Sandy and Emily. Emily informs Bishop that Seed wants Bishop to shoot himself. When Bishop hesitates, Seed kills Sandy with a nail gun, prompting Bishop into shooting himself in the head, believing that doing so will make Seed release his daughter. Instead, Seed takes the daughter to the room containing her fathers corpse, and locks her in it. As Emily sobs for her parents, the film ends.

== Cast ==

* Will Sanderson as Maxwell "Max" Seed
* Michael Paré as Detective Matthew Bishop
* Ralf Möller as Warden Arnold Calgrove
* Jodelle Ferland as Emily Bishop
* Thea Gill as Sandra Bishop Andrew Jackson as Doctor Parker Wickson
* Brad Turner as Officer Thompson
* Phillip Mitchell as Officer Simpson
* Mike Dopud as Officer Flynn
* John Sampson as Officer Ward
* Tyron Leitso as Officer Jeffery
* Michael Eklund as Executioner

== Release ==

The film was shown on the Weekend of Fear Festival in Erlangen, Germany on April 27, 2007. Director Uwe Boll was there to present the film and also to answer questions. Before the film started Boll emphasized the brutality of the film. Furthermore he pointed out that People for the Ethical Treatment of Animals|PETA, who receive 2.5% of world wide sales of the film, made archival recordings of animal cruelty available for the use in Seeds introduction,  though this footage was obscured on-screen. Boll stated that this alteration had occurred in the processing laboratory without his knowledge.

== Awards ==

* Best Special Effects – 2007 New York City Horror Film Festival

== Sequel ==

A sequel, directed by Marcel Walz and entitled  , was released in 2014. It was produced by Boll, and stars Caroline Williams, Christa Campbell, Nick Principe, Jared Demetri Luciano, Manoush, Natalie Scheetz, Annika Strauss, and Sarah Hayden.  

== References ==

 

== External links ==

*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 