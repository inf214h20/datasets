Maya (1966 film)
 
{{Infobox film
| name           = Maya
| image          = Maya theatrical release poster.jpg
| caption        =  John Berry Frank King Maurice King
| screenplay     = John Fante
| story          = Gilbert Wright
| based on       =  
| starring       = Jay North Sajid Khan Clint Walker
| music          = Riz Ortolani
| cinematography = Günther Senftleben
| editing        = Richard V. Heermance
| studio         = King Brothers Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =    
| country        = United States English
}}
 coming of John Berry and starred Jay North and Sajid Khan.

==Synopsis==
Fourteen-year-old Terry Bowen (Jay North) travels from the US to India to meet his father Hugh Bowen (Clint Walker) for the very first time. After a dispute with his father, Terry runs away and is befriended by Raji (Sajid Khan). Together, Terry and Raji have many adventures in the jungles of India. The cultural and religious differences between American Christian Terry and Indian Hindu Raji add to the conflicts in the plot, but together the two boys overcome their differences to survive and to deliver Maya (a sacred white elephant) and her calf to a far away temple.

==Cast==
* Clint Walker as Hugh Bowen, an American living in India
* Jay North as Terry Bowen, Hughs son
* Sajid Khan as Raji, an Indian boy who befriends Terry
* I. S. Johar as One-Eye
* P. Jairaj as Gammu Ghat
* Nana Palsikar as Rajis Father
* Uma Rao as One Eyes Daughter
* Madhusdan Pathak as Station Master
* Sonia Sahni 	as Sheela

== Television series == television series that aired on NBC during the 1967&ndash;1968 season. Sajid Khan and Jay North reprised their roles and the series lasted 18 episodes.

==References==
 	

== External links ==
*  
* Jalal din http://www.imdb.com/name/nm0227499/bio
*  
*  
*  

 

 
 
 
 
 
 
 
 