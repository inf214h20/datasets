Small Town Deb
{{Infobox film
| name           = Small Town Deb
| image          = 
| alt            = 
| caption        = 
| director       = Harold Schuster
| producer       = Lou L. Ostrow
| screenplay     = Ethel Hill
| story          = Jane Withers
| starring       = {{Plainlist|
* Jane Withers
* Jane Darwell
* Bruce Edwards
* Cobina Wright
* Cecil Kellaway
* Katharine Alexander
}}
| music          = Emil Newman Mack Gordon Harry Warren
| cinematography = Virgil Miller
| editing        = Alexander Troffey
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Small Town Deb is a 1941 teenage comedy by 20th Century Fox directed by Harold Schuster. Costumes were made by Herschel McCoy.

==External links== AFI Catalog

 


 