The Worst Movie Ever!
 
{{Infobox film 
 | name = The Worst Movie EVER!
 | caption = 
 | director = Glenn Berggoetz
 | producer = Glenn Berggoetz
 | writer = Glenn Berggoetz
 | starring = Glenn Berggoetz
 | music =
 | cinematography = Erik Lassi
 | editing = Alan Dague-Greene
 | distributor = Driving With Our Eyes Shut
 | released =  
 | runtime = 76 minutes
 | language = English
 | country = United States
 | budget = United States|$1,000
 | gross = $21,010
 | image = 
}} 2011 American sci-fi/action/drama/horror/comedy/musical film written, produced, and directed by Glenn Berggoetz.   

==Filmmaker background==
Berggoetz has previously written and directed several low budget films, including Guernica Still Burning (2008), Bad Movies, Good Showers, and Civil Engineers (2009), To Die Is Hard (2010), Therapissed (2010), and Evil Intent (2010).   

==Synopsis==
According to its movie summary the film includes, among other things: robot aliens, angst-ridden teens, cleavage-wielding soul takers, dark overlords, cross-dressers, pregnant 14-year-old cougars, macho scientists, and Santa Claus.

==Cast==
* Glenn Berggoetz as Johnny, Petey, Dr. Lars Coolman

==Critical response== IFC offers that the film lives up to its title and that it holds the possibility of becoming a cult hit, by writing that it is "quickly becoming the stuff of Internet legend as the worst grossing movie ever, a sales hook that plays nicely with that title. This week , almost 70,000 people watched the films trailer on YouTube. If even a fraction of those folks become curious enough to seek the film out, we could have a new cult hit on our hands."       Since reporting of the films lackluster premiere, the films trailer has become a "mini-hit on YouTube" and initiated "something of a cult following on Facebook."    The films director has stated that the low gross was not intended as a publicity stunt,  and resulted from both the film being scheduled to screen as part of the theaters monthly "midnight screenings", and through problems in stirring interest in the theatrical release. 

==Release==
The film had its festival premiere at the Van Wert Independent Film Festival in Van Wert, Ohio on July 8, 2011, where director Glenn Berggoetz spoke at a breakfast symposium and hosted a midnight screening of the film.      

The film had its theatrical premiere on August 19, 2011, in a single cinema,  the Laemmle Sunset 5 in   which attracted 6 patrons and $20 in revenue during its opening week.   According to the websites Boxofficemojo and The-numbers, the film has developed a bit of a following as it has now gone over $15,000 in box office revenues.

According to director Glenn Berggoetz, the film sold just one ticket over the weekend (for the sole Saturday screening), with nobody attending the Friday screening. 

Attempts by the theater owner and filmmaker to locate the one individual who paid to see the film over its opening weekend have so far failed.   

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 