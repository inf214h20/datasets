Year at Danger
 
{{Infobox film
| name           = Year at Danger
| image          =Year at Danger.jpg
| image size     =
| caption        =
| director       = Steve Metze, Don Swaynos
| producer       = Steve Metze
| writer         =
| narrator       =
| starring       = Steve Metze
| music          = George Oldziey
| cinematography =
| editing        = Don Swaynos
| distributor    =
| released       =
| runtime        = 93 min. US
| Arabic
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 2007 Independent independent Documentary film|documentary. Nine days after getting married, Steve Metze found out he was being deployed as part of Operation Iraqi Freedom. Metze, a West Point graduate, Desert Storm veteran, and documentary filmmaker, decided to pack a camera and document his year in Iraq. The film consists of footage shot by Metze during his deployment to Iraq and was edited by Don Swaynos.

It won the Grand Jury Award at the 2008 DeadCENTER Film Festival and was an Official Selection of the 2007 Austin Film Festival and the 2008 GI Film Festival.

== External links ==
*  
*  

 
 
 
 
 
 
 


 