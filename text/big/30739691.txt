Guard 13
{{Infobox film
| name           = Guard 13
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Eduard Fiker Eduard Fiker Karel Steklý
| starring       = Dana Medřická
| music          = 
| cinematography = Jan Stallich
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Czechoslovakia
| language       = Czech
}}

Guard 13 ( ) is a 1946 Czech crime film directed by Martin Frič.   

==Cast==
* Dana Medřická as Frantiska Brabcová aka Fróny
* Jaroslav Marvan as Inspektor Cadek
* Ella Nollová as Matka Klouzanda, hospodská
* Vilém Pfeiffer as MUDr. Karel Chrudimský
* Nora Cífková as Hlavsová, snoubenka Chrudimského
* Milos Nedbal as Komisar Dr. Barák
* Blanka Waleská as Wang - Liová
* Jaroslav Sára as Wang-li, cínský podomní obchodník
* Ladislav H. Struna as Karta, kasar
* Alois Dvorský as Hlas kartova otce
* Vladimír Repa as Draboch, detektiv
* Otto Rubík as Pobozný, detektiv
* Vladimír Leraus as Docent patologie
* Vladimír Hlavatý as Jindra, císník
* Anna Gabrielová as Prostitute

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 