Annie-for-Spite
{{Infobox film
| name           = Annie-for-Spite
| image          = Annie for Spite.jpg
| image size     =
| caption        = Theatrical poster James Kirkwood
| producer       =
| writer         = Frederick J. Jackson (story) Julian La Mothe
| narrator       =
| starring       = Mary Miles Minter
| music          =
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}} James Kirkwood. The film is based upon the story Annie for Spite by Frederick J. Jackson.

==Plot==
Annie Johnson is a poor orphan working in a shop, when one day a wealthy widow adopts her. She doesnt know she is only adopting her to take revenge on her daughter-in-law Emily. When the widow dies, Annie inherits her estate. Emily is outraged and sues her. Emily finally wins, but Annie has the last laugh when her husband falls in love with Annie and leaves Emily. 

==Cast==
* Mary Miles Minter - Annie Johnson George Fisher - Willard Kaine Nottingham
* Eugenie Forde - Mrs. Emily Nottingham
* Gertrude Le Brandt - Mrs. J.G. Nottingham
* George Periolat - Andrew Walters

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 