The Open Road
{{Infobox Film
| name           = The Open Road
| image          = Open road poster.jpg
| image_size     = 
| caption        = Promotional film poster
| director       = Michael Meredith
| producer       = Jordan Foley Laurie Foxx Charlie Mason  Michael Meredith Justin Moore-Lewy David Schiff
| writer         = Michael Meredith
| starring       = Jeff Bridges Justin Timberlake Kate Mara Harry Dean Stanton Lyle Lovett  Mary Steenburgen 
| music          = Christopher Lennertz
| cinematography = Yaron Orbach
| editing        = 
| distributor    = Anchor Bay Entertainment
| released       = August 28, 2009
| runtime        = 90 minutes  91 minutes (Germany)
| country        = United States
| language       = English
| budget         =  
| gross          = 
}} 2009 comedy-drama film written and directed by Michael Meredith. It stars Justin Timberlake, Kate Mara, Jeff Bridges, and Mary Steenburgen and was produced by Anchor Bay Entertainment. Country singer Lyle Lovett and Harry Dean Stanton are also among the cast.  Filming began in Hammond, Louisiana in February 2008, and continued in Memphis, Tennessee, at Whataburger Field in Corpus Christi, Texas, Houston,Texas and elsewhere in the southern United States.  The film received a limited release on August 28, 2009.

==Plot==
The movie centers on Carlton Garrett (Justin Timberlake), the adult son of baseball legend Kyle Garrett (Jeff Bridges) and a minor-league baseball player with the Corpus Christi, Texas Hooks.  One night, after another game in which he continues to slump at the plate, Carlton gets a phone call from his grandfather Amon (Harry Dean Stanton), who tells him that Carltons mother, Katherine (Mary Steenburgen) has fallen ill and is at the hospital.  Katherine requires a surgery that the doctors recommend; however she refuses to sign the waiver until Kyle comes to visit her.  After Carlton agrees to find his estranged father, together with his ex-girlfriend Lucy (Kate Mara), the two of them fly to Columbus, Ohio to break the news to Kyle, who is there for an autograph signing and doesnt have a cell phone.

After a tense meeting, in which its revealed that Carlton and Kyle havent seen or spoken to each other in five years, Kyle agrees to fly to Texas to see Katherine with his son and Lucy.  The next morning, while preparing to get through airport security, Kyle realizes that hes lost his wallet and hence his identification, therefore hes not allowed to board the flight.  Lucy suggests that the three of them rent a car and drive.  While at a gas station one evening, Kyle asks Lucy to bring him his bag which contain painkillers for his various aches.  Lucy notices Kyles wallet in the bag, revealing that Kyle intentionally misplaced his wallet in hopes of not having to get on the plane and thus not make the trip; Lucy keeps this discovery a secret from Carlton.

As the trip progresses, Carlton and Kyle attempt to piece-together their fragile relationship.  Also, Lucy tells Carlton that shes engaged to a new man; this creates a tension between the two, as their love towards each other remains.  Many delays occur, as Kyle becomes increasingly anxious about seeing Katherine and becomes more and more unreliable.  He seems to make up for this, however, when he tells Carlton and Lucy that he knows someone who works at the Memphis airport who can get him through security without I.D.  While waiting for their flight, Kyle tells Carlton that the employee he knows doesnt exist and it was a cover-up story.  Carlton realizes that Kyle has had his wallet the entire time and has purposely been delaying the trip.  He angrily storms off to talk to Lucy, leaving Kyle unattended.  After an argument with Lucy, whom he finds out knew about the wallet the whole time, he returns to where Kyle was sitting to find him missing.  Carlton and Lucy get on the flight anyway and Carlton exits the flight to look for his father but not before telling Lucy that if he doesnt come back before takeoff, to get off the plane.  Mad at Carlton and their argument, Lucy decides to stay on the plane anyway and she flies home; Carlton is also unsuccessful in finding Kyle.  Carlton calls Katherine and she tells him that if his father is anywhere in Memphis, hed be at The Peabody Hotel downtown.

When Carlton arrives at the hotel he finds out from the front desk that no one by the name of Kyle Garrett has checked-in.  Tired at this point, Carlton takes a room for the night.  Later that evening, during a conversation with the Peabody Hotel bartender (Lyle Lovett), the bartender tells him that Kyle Garrett is indeed staying in the hotel but under an alias.  Furious, Carlton confronts Kyle in Kyles room.  After forcing himself in, Carlton and Kyle wrestle each other to the ground, where Kyle tells Carlton that he loves Katherine but loves himself even more.  Soon Kyle agrees to continue with the trip.  The next day Carlton gets a phone call: Katherine has gotten an infection and the doctors have been forced to operate earlier than expected.

Carlton, Kyle, Amon and Carltons grandmother Virge (Lenore Banks) wait anxiously in the hospital waiting room.  Soon Lucy shows up and she and Carlton reconcile.  The doctor comes out and tells the family that the operation was a success.  The next morning, while still at the hospital, Carlton tells Lucy that hes quitting baseball to focus on becoming a writer and Lucy tells Carlton that shes not a fiancee anymore.  At this point its assumed that the two have gotten back together.  Carlton drives Kyle to the airport, where Kyle agrees to come visit on New Years and Carlton agrees to visit Kyle for Christmas.  The ending scene shows Carlton clearing out his baseball locker and walking to his jeep, where Lucy is waiting for him.  The two drive off together.

==Cast==
* Jeff Bridges as Kyle Garrett
* Justin Timberlake as Carlton Garrett
* Mary Steenburgen as Katherine Garrett
* Kate Mara as Lucy
* Harry Dean Stanton as Amon
* Lyle Lovett as a Bartender
* Ted Danson (cameo) as Coach

==References==
 

==External links==
* 

 
 
 
 
 
 