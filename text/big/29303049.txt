L'impossible Monsieur Pipelet
{{Infobox film
| name           = Limpossible Monsieur Pipelet
| image          = 
| caption        = 
| director       = André Hunebelle
| producer       = P.A.C - Pathé Cinéma (France)
| writer         = Jacques Gut Jean Halain
| starring       = Michel Simon Louis de Funès
| music          = Jean Marion
| cinematography = 
| editing        = 
| distributor    = Pathé Consortium
| released       = 26 July 1955 (France)
| runtime        = 92 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy Drama drama film from 1955, directed by André Hunebelle, written by Jacques Gut, starring Michel Simon and Louis de Funès. The film is known under the titles "The Impossible Mr. Pipelet" (international English title), "Ma fille et ses amours" Belgium French title), "Mijn dochter is verliefd" (Belgium Flemish title). 

== Cast ==
* Michel Simon : Maurice Martin
* Gaby Morlay : Germaine Martin, wife of Maurice
* Etchika Choureau : Jacqueline Martin, the single daughter of Maurice and Germaine
* Louis de Funès : Uncle Robert, the brother of Germaine and the husband of Mathilda
* Louis Velle : Georges Richet, the son of the owners and doctor-to-be
* Mischa Auer : the unsuccessful writer, a lodger
* Maurice Baquet : Jojo, the oldest son of Robert an Mathilde, the boxer
* Jean Brochard : Mr Richet, the businessman who owns the building
* Jean-Jacques Delbo : Mr Francis, Mss Gretas pimp
* Jacques Dynam : Mr Durand, the father-to-be
* Georgette Anys : Aunt Mathilde, Roberts wife
* Renée Passeur : Misses Richet, the businessmans wife
* Jess Hahn : Jérome K. Smith, un Américain lodger
* Dominique Maurin : Dédé, the youngest son of Robert an Mathilde
* Nicky Voillard : Miss Greta, a lodger in the 5th stock
* Jacqueline Gut : Misses Smith, Jérômes wife
* Christiane Chambord : Myriam
* Noël Roquevert : the retired colonel, a lodger
* Benoîte Lab : the innkeeper
* Simone Bach : Jacquelines friend
* Paul Azaïs : the caterer

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 


 