Barry Munday
{{Infobox film
| name           = Barry Munday
| image          = Barry Munday.jpg
| image_size     = 
| caption        = 
| director       = Chris DArienzo
| producer       = 
| screenplay     = Chris DArienzo
| story          = 
| based on       =  
| narrator       =  Patrick Wilson Judy Greer Missi Pyle Chloë Sevigny Cybill Shepherd Colin Hanks Billy Dee Williams Malcolm McDowell
| music          = 
| cinematography = Morgan Susser
| editing        = Joan Sobel
| distributor    = Stick N Stone Productions Prospect Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}} Patrick Wilson as the titular character, as well as Judy Greer, Malcolm McDowell, Chloë Sevigny, Cybill Shepherd, Billy Dee Williams, Emily Procter, Colin Hanks, Jean Smart, Mae Whitman, and Kyle Gass. It premiered at the South by Southwest Film Festival in March 2010.  The film was released on October 1, 2010.

==Plot== Patrick Wilson), a lonely womanizer, wakes up after being attacked to realize that hes missing his "family jewels". To make matters worse, he learns hes facing a paternity lawsuit filed by a woman, Ginger (Judy Greer), he cant remember having sex with. Though unintentional, the two discover that their meeting and subsequent "accidents" opened up new opportunities for personal growth and relationships.

==Cast== Patrick Wilson  ...  Barry Munday
*Judy Greer  ...  Ginger Farley
*Chloë Sevigny  ...  Jennifer Farley
*Jean Smart  ...  Carol Munday
*Malcolm McDowell  ...  Mr. Farley
*Billy Dee Williams  ...  Lonnie Green
*Cybill Shepherd  ...  Mrs. Farley
*Shea Whigham  ...  Donald
*Barret Swatek  ...  Lucy
*Missi Pyle  ...  Lida Griggs
*Christopher McDonald  ...  Dr. Preston Edwards
*Trieu Tran  ...  Moe
*Razaaq Adoti  ...  Spiro
*Emily Procter ... Deborah
*Matt Winston  ...  Kyle Pennington
*Kyle Gass  ...  Jerry Sherman from Barrys support group

==References==
 

==External links==
*  
*  

 
 
 
 
 

 