Fine Dead Girls
{{Infobox Film
| name           = Fine Dead Girls
| image_size     = 
| image	=	Fine Dead Girls FilmPoster.jpeg
| caption        = 
| director       = Dalibor Matanić
| producer       = Jozo Patljak
| writer         = Dalibor Matanić, Mate Matišić
| narrator       = 
| starring       = Olga Pakalović Nina Violić Inge Appelt Krešimir Mikić Ivica Vidović Jadranka Đokić Boris Miholjević Milan Štrljić
| music          = Jura Ferina, Pavle Miholjević
| cinematography = Branko Linta
| editing        = Tomislav Pavlić
| studio         = 
| distributor    = 
| released       = 2002 (Croatia)
| runtime        = 77 min
| country        = Croatia Croatian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2002 film introduced on Pula Film Festival. The film has been named one of the best Croatian movies of the last decade. It caught much attention due to its controversial, provocative themes.

== Plot ==
Iva and Marija, who are a lesbian couple, rent an apartment in Zagreb in a building that seems to be quiet and a safe environment for their love. But as time passes by, the atmosphere in the house gets more and more aggressive. 
 war Croatian society. 

The conservative religious father of Marija is secretly tracing his daughter and pays Lidija to try to seduce Iva, which fails.

After Olga finds out that Iva and Marija are lesbians, the situation escalates to rape, murder and kidnapping.

== Cast ==
* Olga Pakalović as Iva
* Nina Violić as Marija
* Krešimir Mikić as Daniel
* Inge Appelt as Olga the Landlady
* Ivica Vidović as Blaž
* Milan Štrljić
* Mirko Boman
* Jadranka Đokić as Lidija the Prostitute
* Boris Miholjević as Perić the Gynecologist
* Marina Poklepović as Marina Kostelac
* Janko Rakos
* Ilija Zovko

== Awards and nominations ==
=== Pula Film Festival 2002 ===
* Audience Award "Golden Gate Pula"
* Big Golden Arena – Jozo Patljak
* Golden Arena – Best Actor in a Supporting Role: Ivica Vidović, Best Actress in a Supporting Role: Olga Pakalović, Best Director: Dalibor Matanić

=== Geneva Cinéma Tout Ecran 2003 ===
* Young Jury Award – Dalibor Matanić

=== Sochi International Film Festival 2003 === At kende sandheden (2002))
* Golden Rose – Dalibor Matanić (nominated)

== External links ==
* 
*  at Filmski-Programi.hr  

  

 
 
 
 
 
 