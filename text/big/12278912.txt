The Babe Ruth Story

{{Infobox film
| name           = The Babe Ruth Story
| image          = Babe Ruth Story (1948 movie).jpg 
| image_size     = Fox Video VHS release
| director       = Roy Del Ruth
| producer       = Roy Del Ruth
| writer         = George Callahan Bob Considine (book and screenplay) Babe Ruth (book)
| narrator       = Knox Manning
| starring       = William Bendix Claire Trevor Charles Bickford Edward Ward
| cinematography = Philip Tannura
| editing        = Richard Heermance Allied Artists
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 worst films ever made.

==Plot== call the Boston Braves, even though they want him to play in the games despite his age. During one game, Babe gets stressed out and cant continue playing, and retires from baseball after that game. Sadly, this means he goes off contract by retiring during his time with the Braves, and is fired from anything related to baseball. Later, Babe complains of neck pain, and is learned he is dying from throat cancer. The news of this leads fans to send letters telling Babe that they care. The doctors decide to try a treatment on Babe with a chance that hell survive; as Babe is taken to surgery, the narrator give words of encouragement to baseball fans, crediting Babe Ruth for Americas love of the sport.

==Critical reception== World Series home run Chicago Cubs. Ruth delivers on a promise he made to a young cancer patient that he would hit a home run. Not only does Ruth succeed in fulfilling the promise, but the child immediately shows signs of improvement.  Dan Shaughnessy 
of The Boston Globe called The Babe Ruth Story "the worst movie I ever saw"  while The Washington Times stated that the film "stands as possibly the worst movie ever made."  The film has been called one of the worst sports films ever by Newsday and The A.V. Club,   and called one of the worst biopics by Moviefone and Spike (TV network)|Spike.    Michael Sauter included it in his The Worst Movies of All Time book and Leonard Maltin called it "perfectly dreadful."    The Cinema Snob, played by Brad Jones, reviewed this movie on his web series. In his review, along with the many criticisms such as noting how inaccurate the movie is with the real Babe Ruth, going as far as to make him naive and larger than life when he cures those almost as if hes a god; in the end, he points out how much the film went that far in treating its subject like Babe Ruth like the way they did, probably thinking the films positive vibes is what made the real Babe Ruth live a few weeks longer after the films release.

==Cast==
* William Bendix as Babe Ruth
* Claire Trevor as Claire Hogsdon Ruth
* Charles Bickford as Brother Matthias
* William Frawley as Jack Dunn
* Sam Levene as Phil Conrad
* Matt Briggs as Col. Jacob Ruppert
* Fred Lightner as Miller Huggins
* Mark Koenig as Himself
* Mel Allen as Himself

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 


 