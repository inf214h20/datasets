Escape from Galaxy 3
  }}
{{Infobox film
 | name           = Escape from Galaxy 3
 | caption        = Theatrical poster  Ben Norman 
 | producer       = Dino Mangogna John Thomas
 | starring       = Sherry Buchanan Fausto Di Bella Don Powell Chris Avram Attilio Dottesio Max Turilli Don Powell
 | cinematography = Sandro Mancori
 | editing        = Gianfranco Amicucci Prism (USA)
 | released       = February 12, 1981
 | runtime        = 92 min.
 | country        = USA / Italy
 | language       = English / Italian
 | budget         = unknown
}}
 Italian 1981 science fiction film. The film is notorious for using stock footage from Starcrash for all its model scenes.

==Plot==
In a distant galaxy, aboard a space station, Princess Belle Star (Sherry Buchanan) approaches her father King Ceylon (Auran Cristea) with her Space Captain, Lithan (James Milton). She advises that their scanners have detected a ship that doesnt belong in their galaxy. The King declares that Oraclon "The King of the Night" (Don Powell) has come to take over his territories. Lithan suggests they put plan Epsilon into action.

Aboard his ship (which looks like a large mechanical clawed fist), Oraclon communicates with Ceylon who refuses to surrender. Oraclon launches his attack, destroying Ceylons Space Station and the planet Exalon, but not before Ceylon sends Lithan and his daughter to obtain help from his ally "Antares". Oraclon gives chase and manages to damage their ship but they manage to elude his ships by going to hyperspace with no set coordinates.

They approach and land on a planet that looks like Earth. Their landing scares off the human population who attack them with sticks and rocks. Lithan and Belle scare them off by shooting but not killing anybody. They are eventually captured by the primitives and are to be executed, but before they can be executed in a crater, a child slips and almost falls to her death until Lithan springs into action and saves the child - proving they mean the primitives no harm.

While Lithan and Belle Star are treated as honored guests, the village elder hints that they are the only survivors of an ancient ill-fated civilization. When Oraclon reaches the planet and thinks theyve found a radiation signature for the ship he states Earth destroyed itself in an atomic war long ago.

The ship is repaired, Belle Star remarks that her father once told her that people who "taste the joys of life lose their gift of immortality" and that they may no longer be immortal. Following this, the Earthlings hold a festival where the winner chooses his mate. The winner selects Belle Star, making Lithan declare his love for her. They run off to the beach to make love when Oraclon attacks the village, prompting Lithan and Bellastar to leave, but the village elder refuses to let them and tries to force them to stay. Lithan and Bellastar overpower the villagers and escape.

During the long trek back to their galaxy, Lithan and Belle Star make love, which Oraclon is able to watch from his ship. With nowhere to go they surrender to Oraclon. He takes them back to Exalon which he exclaims is dead and lifeless. He declares he will spare them both but Belle Star will be his slave forever and Lithan will spend the rest of his life in forced labor. They say their final goodbyes and in a moment of distraction, Lithan vaporizes Oraclon, kills the guards and frees the kings who were taken prisoner. The kings return to their homeworlds, Lithan and Belle Star return to Earth to live their lives as mortals.

==Main cast==
* Sherry Buchanan - Belle Star: Princess and daughter of King Ceylon. She is the sensual companion of Lithan who wants to experience the "joys" they witness on Earth and falls in love with Lithan.
* Fausto Di Bella - Lithan (as James Milton): Superpowered immortal Space Captain to Belle Star. Has no idea what water is or emotions. Eventually falls in love with Belle Star.
* Don Powell - Oraclon: The evil "King of the Night" who seeks to rule the whole galaxy, and seeks out the escaped Belle Star and Lithan.
* Chris Avram - King Ceylon (as Auran Cristea): The peaceful King of Exalon who refuses to surrender to Oraclon and is killed at the beginning of the movie.
* Attilio Dottesio - Village Elder: The villager who rallies together all the other villagers and reveals that they are the survivors of a long lost civilization.
* Max Turilli - Jemar: Oraclons lackey. Usually makes reports or repeats Oraclons commands to the rest of the ship.

==External links==
* 

 
 
 
 
 
 
 