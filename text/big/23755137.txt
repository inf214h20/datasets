Remorse at Death
{{Infobox film
| name           = Remorse at Death
| image          =
| caption        =
| writer         = Mei Lanfang Qi Rushan
| starring       = Mei Lanfang
| director       = Fei Mu
| cinematography = Li Shengwei
| producer       =
| studio         = Yihua Film Company
| country        = China
| runtime        =
| released       =  
| language       = Mandarin
| budget         =
| film name = {{Film name| jianti         = 生死恨
| fanti          = 生死恨
| pinyin         = Shēngsǐ hèn}}
}} color film.  Remorse at Death is a filmed stage performance of the Chinese opera of the same name. It starred and was co-written by Mei Lanfang, one of the centurys best-known Chinese opera singers. The film is also known as Happiness Neither in Life Nor in Death and Wedding in the Dream. 

Today, as part of Feis later works, Remorse at Death has been overshadowed by Fei Mus immediate follow-up, the classic melodrama Spring in a Small Town (1948).

== Notes ==
  

== References == 
* Zhang, Yingjin. Chinese National Cinema. Routledge, 2004. ISBN 0-415-17290-X.

== External links ==
*  
*   at the Chinese Movie Database

 
 
 
 

 