Cesta do hlubin študákovy duše
 
{{Infobox film
| name = Cesta do hlubin študákovy duše
| image =
| image size =
| alt =
| caption =
| director = Martin Frič
| producer = Alois Fiala Jan Sinnreich
| writer = Martin Frič Jan Kaplan Jaroslav Zák
| starring = Jindřich Plachta
| music =
| cinematography = Ferdinand Pecenka
| editing = Jan Kohout
| studio =
| distributor =
| released = 31 October 1939
| runtime = 88 minutes
| country = Czechoslovakia
| language = Czech
| budget =
| gross =
}}
 Czech comedy film directed by Martin Frič.    It was released in 1939.

==Cast==
* Jindřich Plachta - Matulka - Natural science teacher
* Jaroslav Marvan - Vobořil - Mathematics Teacher
* Frantisek Vnouček - Voříšek - Czech Language Teacher
* Milos Nedbal - Šeda - French Teacher
* Jaroslav Průcha - Rabiška - Physics teacher
* František Kreuzmann - Tuřík (as Frantisek Kreuzman)
* Ludvík Veverka - Prof. Kahuda
* Vojta Novák - Dr. Vondrák
* Jarmila Holmová - Rázová - Geography Teacher
* Karel Veverka - Schoolmaster
* Ferenc Futurista - Janitor Petule
* Ladislav Pešek - Kulík
* R. A. Strejka - Peterka (as A. R. Strejka)
* Rudolf Hrušínský - Vanĕk
* Frantisek Filipovský - Mazánek

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 