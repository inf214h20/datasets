The Final Stage
 
{{Infobox film
| name           = The Final Stage
| image          = 
| image size     =
| caption        = 
| director       = Frank Howson
| producer       = Frank Howson executive: Peter Boyle
| writer         = Frank Howson
| based on = 
| narrator       =
| starring       = Tommy Dysart Abigail Adrian Wright
| music          = 
| cinematography = 
| editing        = 
| studio =  Boulevard Films
| distributor    = 
| released       = 1995
| runtime        = 73 mins
| country        = Australia English
| budget         =
| gross = 
| preceded by    =
| followed by    =
}}
The Final Stage is a film directed by Frank Howson. 

==Production==
It was based on an early play by Howson and was the seventh film from Boulevard Films and the third which Howson directed. He shot it over two weeks with a cast of six actors on one set. 

The movie has been called one of Howsons most personal works. 

==Fraud Case==
Howson later tried to pass the film off as another movie The Boy Who Dared to Dream in order to enable investors to achieve a tax deduction. This was unsuccessful and Howson was convicted of fraud. 

==References==
 

==External links==
* 

 
 
 
 
 

 