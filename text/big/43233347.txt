Maiden's Rock
{{Infobox film
| name = Maidens Rock
| image =
| image_size =
| caption =
| director = Boris Grezov
| producer = 
| writer =  Boris Grezov
| narrator =
| starring = Boris Grezov  Katya Syoyanova   Vyara Salplieva-Staneva
| music = 
| cinematography = Albert Davidov   Charl Keneke
| editing =
| studio =   Aktzionerno Druzhestvo Luna 
 | distributor = 
| released = 30 September 1922 
| runtime = 
| country = Bulgaria Bulgarian intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent drama film directed by Boris Grezov and starring Grezov, Katya Syoyanova and Vyara Salplieva-Staneva. 

==Cast==
*  Boris Grezov as Stoyan 
* Katya Syoyanova as Lilyana  
* Vyara Salplieva-Staneva as Gyula  
* Mihail Goretzki as Bozhichko, selski kmet   Dimitar Stoyanov as Trichko 
* Yordan Minkov as Doychin, bashtata na Trichko  
* Dimitar Keranov as Mladen, priyatel na Stoyan   Ivan Stanev as Asen. mlad tziganin  
* Cherneva as Adzhara, stara tziganka 
* Petko Chirpanliev as Hyusein, Tziganski stareyshina

== References ==
 

== Bibliography ==
* Marcel Cornis-Pope & John Neubauer. History of the Literary Cultures of East-Central Europe: Junctures and disjunctures in the 19th and 20th centuries. Volume IV: Types and stereotypes. John Benjamins Publishing, 2010.
 
== External links ==
*  

 
 
 
 
 
 
 

 