Face (2009 film)
{{Infobox film
| name           = Face
| image          = Face poster.jpg
| caption        = 
| director       = Tsai Ming-liang
| producer       = Jacques Bidou
| writer         = Tsai Ming-liang
| narrator       = 
| starring       = Fanny Ardant
| music          = 
| cinematography = Pen-jung Liao
| editing        = Jacques Comets
| distributor    = 
| released       =  
| runtime        = 138 minutes
| country        = Taiwan France
| language       = French Chinese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French film directed by Tsai Ming-liang. It was nominated for the Golden Palm at the 2009 Cannes Film Festival.   

==Cast==
* Fanny Ardant as La productrice et la reine Hérodias
* Laetitia Casta as La star et Salomé
* Jean-Pierre Léaud as Antoine et le roi Hérode
* Kang-sheng Lee as Hsiao-Kang as Le réalisateur
* Lu Yi-ching as La mère
* Mathieu Amalric as Lhomme du fourré
* Nathalie Baye
* Samuel Ganes as Le garçon du buisson
* Olivier Martinaud as The young father
* Jeanne Moreau
* François Rimbau as Régisseur

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 