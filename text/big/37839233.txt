Teenage Exorcist
{{Infobox film
| name           = Teenage Exorcist
| image          = 
| alt            = 
| caption        = 
| director       = Grant Austin Waldman
| producer       = Fred Olen Ray Caryle Waldman Drew Waldman Grant Austin Waldman
| screenplay     = Fred Olen Ray Brinke Stevens Ted Newsom
| based on       = 
| starring       = Eddie Deezen Brinke Stevens Robert Quarry
| music          = Chuck Cirino
| cinematography = William H. Molina
| editing        = David R. Schwartz
| studio         = 
| distributor    = 
| released       = February 23, 1994
| runtime        = 87 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Teenage Exorcist is a 1991 American comedy horror film directed by Grant Austin Waldman and written by Brinke Stevens from a story by Fred Olen Ray. The film stars Stevens, Eddie Deezen and Robert Quarry. Though it was shot in 1991, Teenage Exorcist wasnt released on video until 1994.

==Plot synopsis==
Diane (Brinke Stevens), a prim and proper college grad student, rents a spooky old house from a creepy realtor (Michael Berryman). Unfortunately for Diane, an ancient demon (Oliver Darrow) resides in her basement and shes quickly possessed by the spirits of the house, turning her into a leather-clad, chainsaw-wielding succubus. When Dianes sister Sally (Elena Sahagun), Sallys brother-in-law Mike (Jay Richardson) and Dianes boyfriend Jeff (Tom Shell) come to visit only to be attacked by her demonic incarnation, they summon a priest, Father McFerrin (Robert Quarry), to handle the situation. Failing that, Father McFerrin attempts to call an exorcist, only to dial the wrong number and unwittingly place an order at the local pizza parlor. When the pizza delivery boy (Eddie Deezen) arrives at the house, its up to him and the rest of the gang to destroy the demon and stop his nefarious scheme.

==Cast==
*Brinke Stevens as Diane
*Eddie Deezen as Eddie
*Oliver Darrow as The Demon
*Jay Richardson as Mike
*Tom Shell as Jeff
*Elena Sahagun as Sally	
*Robert Quarry as Father McFerrin
*Kathryn Kates as Maid
*Michael Berryman as Herman

==Reception==
Critical reception to Teenage Exorcist was primarily negative, though it has received mixed praise from cult film audiences. Legendary film critic Joe Bob Briggs, though giving the film only 1 star, was amused by the films low-brow humor and Brinke Stevens revealing costumes, awarding it with his catchphrase "Joe Bob says check it out".  Allmovie gave the movie a rating of two stars out of five without a written review. 

==External links==
* 
* 

==References==
 

 
 
 
 
 
 
 