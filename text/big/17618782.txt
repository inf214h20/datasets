The Class (2008 film)
 
{{Infobox film
| name           = The Class
| image          = Entrelesmurs.jpg
| caption        = Theatrical release poster featuring Esmeralda Ouertani and Rachel Regulier
| director       = Laurent Cantet
| producer       = Caroline Benjo Carole Scotta
| writer         = François Bégaudeau (novel and screenplay) Robin Campillo (writer) Laurent Cantet (writer)
| starring       = François Bégaudeau
| music          = 
| cinematography = Pierre Milon
| editing        = Robin Campillo
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 128 minutes
| country        = France
| language       = French
| budget         = 2 480 000 €
| gross          = 13 221 320 €
}} 2006 novel literature teacher in a middle school in the 20th arrondissement of Paris, particularly illuminating his struggles with "problem children" Esmerelda (Esmeralda Ouertani), Khoumba (Rachel Regulier), and Souleymane (Franck Keïta). The film stars Bégaudeau himself in the role of the teacher.  

The film received the Palme dOr at the 2008 Cannes Film Festival,  making it the first French film to do so since 1987, when Maurice Pialat won the award for Under the Sun of Satan.

==Plot==
The film covers an academic year, beginning with the teachers gathering for the autumn term, introducing themselves to each other and being welcomed by the principal, an unsmiling figure wearing rimless glasses. It ends with an informal game of football between staff and pupils and a long hand-held shot of an empty classroom.

The camera never leaves the school. The film is set in the staff room, the playground, the dining room, the principals office, a conference room, and the classroom where François Marin (François Bégaudeau) teaches French (i.e., language arts) to a mixed group of 13- and 14-year-olds. 

The film concentrates on Marin as he tries to keep order in the class, mediating between conflicting ethnic groups, quieting the rowdy, bringing out the reticent, and trying to educate them. The class is difficult, and in some ways the brightest are the most disruptive; when he teaches them the complexities of French verbs, they challenge the need to know such things, stating their limited use in modern speech. He gives them The Diary of Anne Frank to study, but many do not bother to read it. One of the most intelligent pupils, the nihilism|near-nihilistic Esmerelda, of Tunisian origin, says she cant be bothered to read books. Marin tries to get an insight into the inner workings of the pupils; they write self-portraits which describe their aspirations, hobbies and dislikes. These are eventually collated by Marin who creates an end-of-year book with them. Marin manages to win over the sparsely driven Souleymane, from Mali, by allowing him to develop his gift for photography and make his self-portrait a pictorial biography. However, Souleymanes insolence and disobedience are his downfall as a confrontation with Marin and the class ends in an act of violence. Souleymanes violence lands him a hearing in front of the discipline committee, which results in his expulsion. 

== Cast ==
* Burak Ozyilmaz: Burak
* Boubacar Toure: Boubacar
* Carl Nanor: Carl, a student who arrived during the year (after expulsion from another middle school)
* Esmeralda Ouertani: Esmeralda, another class representative
* Franck Keita: Souleymane, a student who enters the Disciplinary Board
* François Bégaudeau: François Marin, a French teacher and form teacher
* Jean-Michel Simonet: The headmaster
* Louise Grinberg: Louise, the class representative, first class
* Rachel Régulier: Khoumba, a student who refuses to read
* Vincent Robert: Hervé, the sports teacher.

==Reception==
 
The film has received critical acclaim, achieving a 96% rating at Rotten Tomatoes out of 150 reviews counted.  Metacritic lists Entre les murs with a rating of 92,  making it one of the best reviewed films of the year according to the website.

The film was warmly reviewed by the critic  s Zéro de Conduite, to Nicolas Philiberts Être et avoir. Laurent Cantet, whose parents were both teachers, carries it on and he elicits marvellous performances...As the teacher at a tough, racially mixed, inner-city school in Paris, Marin (François Bégaudeau), neither weary cynic nor wide-eyed idealist, is a decent, determined realist..not a saint, though by the end of the school year he has exhibited certain of the necessary qualities." 

The film was the featured opening night selection at the 46th New York Film Festival in 2008.

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.   
 Dana Stevens, Slate (magazine)|Slate 
*2nd - Kenneth Turan, Los Angeles Times (tied with A Christmas Tale)  Michael Phillips, Chicago Tribune 
*2nd - Peter Rainer, The Christian Science Monitor  David Denby, The New Yorker 
*5th - David Edelstein, New York (magazine)|New York magazine 
*5th - Dennis Harvey, Variety (magazine)|Variety 
*5th - Ella Taylor, LA Weekly (tied with A Christmas Tale) 
*5th - Stephanie Zacharek, Salon.com|Salon 
*7th - Andrea Gronvall, Chicago Reader 
*7th - Robert Mondello, National Public Radio|NPR 
*8th - Owen Gleiberman, Entertainment Weekly 
*8th - Sean Axmaker, Seattle Post-Intelligencer 

==Awards and nominations==

===Won=== Cannes Film Festival
**Palme dOr (Laurent Cantet)

*César Awards
**Best Writing - Adaptation (François Bégaudeau, Robin Campillo and Laurent Cantet)

*Image Awards
**Outstanding Foreign Motion Picture

*Independent Spirit Awards
**Best Foreign Film

*International Film Festival Cinematik
**Meeting Point Europe Award

===Nominated===
*Academy Awards Best Foreign Language Film

*César Awards
**Best Director (Laurent Cantet)
**Best Editing (Robin Campillo and Stephanie Leger)
**Best Film
**Best Sound (Olivier Mauvezin, Agnes Ravez and Jean-Pierre Laforce)

*European Film Awards
**Best Director (Laurent Cantet)
**Best Film

*Satellite Awards
**Best Foreign Language Film

==References==
 

==External links==
* 
* 
*  
*  
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 