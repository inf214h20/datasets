September Love
{{Infobox film
| name           =Septemberliebe
| image          = 
| image size     =
| caption        =
| director       =Kurt Maetzig
| producer       =Hans Mahlich
| writer         = Herbert Otto
| narrator       =
| starring       =Doris Abeßer
| music          = Helmut Nier
| cinematography = Joachim Hasler
| editing        =Lena Neumann
| distributor    = PROGRESS-Film Verleih
| released       = 8 February 1961
| runtime        =78 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
September Love (  film directed by Kurt Maetzig. It was released in List of East German films|1961.

==Plot==
Leading chemist Hans Schramm is betrothed to Hanna, but falls in love with her younger sister Franka. The two attempt to repress their feelings, but eventually begin an affair. When Hans is extorted by a group of West German agents, who demand to know about his secret work, he is griped by panic and decides the only way out is to flee to the West. Franka discovers his plans and informs the Stasi. Hans perceives it as betrayal at first, but after all ends well, he realizes she only wanted the best for him.

==Cast==
*Doris Abeßer - Franka
*Ulrich Thein - Dr. Hans Schramm
*Annekathrin Bürger - Hanna
*Hans Lucke  - Lieutenant Unger
*Kurt Dunkelmann - Priest Hübenthal
*Anna-Maria Besendahl -Hans mother
*Micaëla Kreißler - Milli
*Heinz Laggies - Stasi agent
*Karl Heinz Oppel - Stasi agent

==Production==
While the Khruschev Thaw allowed a considerable degree of liberalization in all Eastern Block countries, including in East Germany, the commitment of DEFA to follow the Socialist Unity Party of Germanys line was reaffirmed in the 1958 Bitterfeld Conference; although many pictures with less ideological restrictions were made at the time, the studio devoted much resources to produce films about the East-West German tensions. September Love was one of eight major works of this genre made between 1959 to 1964. 

Maetzig told an interviewer that he was influenced by the sharpening political climate, on the eve of the erection of the Berlin Wall: "it became clear that a confrontation of some kind was brewing... We could not stand and watch... As events lurched toward a crisis". 

The script was adapted from Herbert Ottos novel by the same name. 

==Reception==
The film was a commercial success and received well by the audiences. 

Peter Ulrich Weiss regarded September Love as a continuation of DEFAs long-established tradition of "Saboteur Thrillers", pitting the East German populace against a menace from the West.  Antonin and Mira Liehm viewed it also as a forerunner of a new subgenre, aimed at rationalizing the building of the Wall, but using a new setting, mostly love stories, rather than plain political agitation.  Heinz Kersten defined the film as "a completely incontroversial romance, that is remarkable for the unusual quality of the acting... but still propagates the old political message."  Joshua Feinstein categorized September Love among one of the East German film focusing on the exploits of a single female protagonist, a theme that was popular with DEFA.  Philip Broadbent and Sabine Hake noted that it was one of several pictures made during the early 1960s that "insisted on the unifying effect" that the closed border with the West had on ordinary people. 

==References==
 

==External links==
*  
*  on PROGRESS website.
*  on film-zeit.de.
*  original poster on ostfilm.de.
*  on DEFA Sternstunden.

 

 
 
 
 