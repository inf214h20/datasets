The Red Robe
{{Infobox film
| name = The Red Robe
| image =
| image_size =
| caption =
| director = Jean de Marguenat
| producer =  
| writer =  Eugène Brieux  (play)
| narrator =
| starring = Constant Remy   Suzanne Rissler   Marcelle Praince   Jacques Grétillat
| music = Adolphe Borchard     
| cinematography = Coutelen   André Dantan   Enzo Riccioni  
| editing =     Raymond Lamy 
| studio = 
| distributor = 
| released = 1933
| runtime = 91 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Red Robe sometimes translated as The Red Dress (French:La robe rouge) is a 1933 French drama film directed by Jean de Marguenat and starring Constant Remy, Suzanne Rissler and Marcelle Praince.  It is based on a 1900 play by Eugène Brieux.

==Cast==
*  Constant Rémy as Le procureur Vagret  
* Suzanne Rissler as Yanetta  
* Marcelle Praince as Madame Vagret  
* Jacques Grétillat as Mouzon  
* Daniel Mendaille as Etchepare  
* Pierre Arnaudy as Lécolier 
* Marcelle Barry as Madame Bunerat  
* Robert Charlys as Le bedeau  
* Alexandre Colas as Le boulanger  
* Gaston Dubosc as Le Bouzule 
* Jean Dunot as Benoît 
* Lily Fairlie as Bertha  
* Anthony Gildès as Delorme   Isidora as Madame Birman  
* Pierre Juvenet as Le président des assises  
* Tony Laurent as Le directeur de prison  
* Raoul Marco as Bridet  
* Pierre Marnat as Le lieutenant de gendarmerie  
* Georges Mauloy as Le procureur général  
* Marthe Mellot as La mère dEtchepare  
* Armand Morins as Brunerat  
* Pierre Munié as Maître Ardeuil  
* Marthe Sarbel as Madame La Bouzule  
* Louis Vonelly as Maître Placat 
* Cécile Barrette 
* Andrée Doria

== References ==
 

== Bibliography ==
* Lucy Mazdon & Catherine Wheatley. Je T Aime... Moi Non Plus: Franco-British Cinematic Relations. Berghahn Books, 2010.

== External links ==
*  

 
 
 
 
 
 
 

 

 