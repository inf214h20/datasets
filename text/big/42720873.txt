Mondo candido
{{Infobox film
| italic title   = 
| name           = Mondo candido
| image          = Mondo candido.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Gualtiero Jacopetti, Franco Prosperi 
| producer       = Camillo Teti
| writer         = 
| screenplay     = Gualtiero Jacopetti, Franco Prosperi, Claudio Quarantotto
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Riz Ortolani
| cinematography = Giuseppe Ruzzolini
| editing        = 
| studio         = 
| distributor    = 
| released       = 21 February 1975
| runtime        = 107 mins
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
 1975 black Mondo directors Gualtiero Jacopetti and Franco Prosperi. The film is a liberal adaptation of Voltaires novel Candide. 

==Plot== Christopher Brown) Cunegonda (Michelle Miller). However, after a sudden invasion, Cunegonda disappears and Candido begins his errant in search of her and true love. The events take him to modern times and places like Israel where he witnesses the Arab–Israeli conflict and New York City, the living epitome of global capitalism.

==Cast==
* Christopher Brown: Candido
* Jacques Herlin: Dr. Panglos  
* Michelle Miller: Cunegonda
* José Quaglio: Inquisitor
* Steffen Zacharias: Sage
* Gianfranco DAngelo: Baron 
* Salvatore Baccaro: Ogre 
* Alessandro Haber: A lover of Cunegonda 
* Giancarlo Badessi: Spanish governor
* Richard Domphe: Cacambò

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 