Dynamite Smith
{{Infobox film
| name           = Dynamite Smith
| image          = 
| director       = Ralph Ince
| story          = C. Gardner Sullivan 
| screenplay     = C. Gardner Sullivan Charles Ray Jacqueline Logan Bessie Love Wallace Beery
| distributor    = Pathé Exchange
| country        = United States Henry Sharp 
| released       =  
| runtime        = 70 minutes
| language       = Silent film English intertitles
| budget         = 
}}
 silent drama Charles Ray, Bessie Love and Wallace Beery.

==Synopsis==
The film tells the story of a young reporter, Gladstone Smith (Ray), who is pursued by murderer "Slugger" Rourke (Beery). Gladstone flees to Alaska with the killers wife Violet (Love) and eventually confronts his cowardice when the murderer pursues them to Alaska. 

==Cast==
* Charles Ray as Gladstone Smith
* Wallace Beery as "Slugger" Rourke 
* Bessie Love as Violet
* Jacqueline Logan as Kitty Gray
* Adelbert Knott as Dad Gray
* Lydia Knott as Aunt Mehitabel Russell Powell as Colin MacClintock
* S.D. Wilcox as Marshall

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 