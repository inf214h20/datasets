The Phynx
{{Infobox Film |
  name=The Phynx |
  image= Poster of The Phynx.jpg|
  caption = | George Foster | Dennis Larden   Lonny (Lonnie) Stevens|
  director=Lee H. Katzin |
  music=Mike Stoller | 	 
  distributor=Warner Bros. Pictures |
  released=May 6, 1970 |
  runtime=81 min. |
  language=English |
  movie_series=|
  awards=| George Foster |
  budget= |
  |}}

The Phynx is a 1970 comedy film directed by Lee H. Katzin about a rock and roll band named The Phynx and their mission in foreign affairs.  The group is sent to the country of Albania to locate celebrity hostages taken prisoner by Communists.

This turned out to be the final film appearance for several of the veteran performers in the cast, including Leo Gorcey, George Tobias and Marilyn Maxwell. 

==History==

The Phynx was barely released and has since become something of an obscure, rarely seen cult film; bootleg copies for many years turned up on auction websites before Warner Archive officially released the film on DVD for the first time in October 2012. 

==Cast==
*The Phynx... Themselves
**Michael A. Miller
**Ray Chipperway Dennis Larden
**Lonny Stevens
*Lou Antonio... Corrigan
*Mike Kellin... Bogey
*Michael Ansara... Col. Rostinov
*George Tobias... Markevitch
*Joan Blondell... Ruby
*Martha Raye... Foxy
*Larry Hankin... Philbaby Pat McCormick... Father OHoolihan Ultra Violet... Felice
*Rich Little... Voice in Box
*Susan Bernard... London Belly
*Sally Struthers... Worlds No. 1 Fan
*Patty Andrews
*Rona Barrett
*James Brown Edgar Bergen and Charlie McCarthy
*Busby Berkeley
*Dick Clark
*Xavier Cugat
*Cass Daley
*Andy Devine
*Fritz Feld
*Leo Gorcey (in his final film role, released after his death.)
*Huntz Hall John Hart
*Louis Hayward George Jessel
*Ruby Keeler
*Patsy Kelly
*Dorothy Lamour
*Guy Lombardo
*Trini Lopez
*Joe Louis
*Marilyn Maxwell
*Butterfly McQueen Pat OBrien
*Maureen OSullivan
*Richard Pryor
*Harold Sakata
*Colonel Sanders
*Jay Silverheels
*Ed Sullivan
*Rudy Vallee
*Clint Walker
*Johnny Weissmuller

==See also==
* List of American films of 1970

==References==

 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 