Puppet Master (film)
 Puppet Master}}
 
 
{{Infobox film
| name           = Puppet Master
| image          = Puppet_Master.jpg
| caption        = Film poster
| director       = David Schmoeller
| producer       = Hope Perello Charles Band
| writer         = Charles Band Kenneth J. Hall William Hickey Irene Miracle Jimmie F. Skaggs Robin Frates Matt Roe Kathryn OReilly Mews Small
| music          = Richard Band
| cinematography = Sergio Salvati
| editing        = Thomas Meshelski
| distributor    = Full Moon Features Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $400,000
}} theatrical release in summer 1989, before being released on home video the following September, Puppet Master was ultimately pushed to a direct-to-video release on October 12, 1989, as Charles Band felt he was likely to make more money this way than he would in the theatrical market. The film was very popular in the video market and since developed a large cult following that has led to the production of ten sequels.

==Plot==
  Shredder Khan, spies get Toulon shoots himself in the mouth with a pistol.

50 years later, in 1989, psychics Alex Whitaker, Dana Hadley, Frank Forrester and Carissa Stamford make contact with an old colleague of theirs, Neil Gallagher, and conclude he found Andre Toulons hiding place. Each one of them experiences a different vision; Alex sees Neil pointing a gun at a young womans head, and dreams of leeches sucking blood out of his stomach, while Dana for-sees her possible death.

The psychics meet at the Bodega Bay Inn that Neil resides at and meet Neils wife, Megan, the woman from Alexs vision. They also meet the housekeeper, Theresa. The psychics are skeptical that Neil took a wife but it is forgotten when Megan tells them that Neil shot himself. Theresa, Megan, and Alex leave the body, leaving Frank, Carissa and Dana. Dana stabs a long pin into Neils corpse to verify that he is in fact dead.

Each psychic experiences a vision; Dana tells Theresa not to go near the fireplace, Alex sees Neil wearing a mask while dancing with Megan in the dining room, and Carissa sees Neil assaulting a woman in the elevator. As the sun sets, Pinhead (Puppet Master)|Pinhead, another living puppet, climbs out of Neils casket. That night at dinner, Dana makes several remarks about Neil that causes Megan to leave the table. Alex goes after her and explains about the powers of the people in the group. Carissa is a Psychometry (paranormal)|psychometrist, and she can touch an object and give the objects history, Dana can tell fortunes and locate things and people, and Alex himself has premonitions in his dreams and when hes awake. All four of them were helping Neil in his research of Alchemy, and during that time, Frank and Neil discovered that the Egyptians created a method of giving life to inanimate figurines, a power passed down to practitioners of magic, and Dana tracked down the location of Andre Toulon, the last true alchemist, to the hotel. But because he hasnt made contact with them in a while, Dana and the rest think he screwed them over and took whatever he was looking for himself, and theyre there to take it and settle the score.
 punches her until she manages to knock him away, only to have her throat cut by Blade, using his knife-hand, fulfilling her fortune.
 metaphysically speaking", eternal life. He contacted them all so they wouldnt take the secret from him, and he hopes to use their bodies for future human experiments (the first being Megans parents), expressing disgust of working with the puppets and violently throws Jester at a chair. Seeing this attack on one of their own, the puppets revolt against Neil, brutally killing him in front of Alex and Megan.
 taxidermic dog, method (although the films sequel dispels that she has become the next Puppet Master).

==Cast== William Hickey Andre Toulon
* Paul Le Mat as Alex Whitaker
* Irene Miracle as Dana Hadley
* Jimmie F. Skaggs as Neil Gallagher
* Robin Frates as Megan Gallagher
* Matt Roe as Frank Forrester
* Kathryn OReilly as Carissa Stamford
* Mews Small as Theresa
* Barbara Crampton as Woman at Carnival
* David Boyd as Buddy
* Peter Frankland as Max
* Andrew Kimbrough as Klaus

===Featured puppets=== Blade
* Jester
* Pinhead
* Tunneler
* Leech Woman Shredder Khan Gengie
==Release==
 

==Reception==
 
Critical reception for the film has been mixed to negative.

TV Guide gave the film a negative review calling it "a pointless variation on the killer-doll genre". 
Wes R. from Oh the Horror.com gave the film a positive review stating, "Despite its flaws, Puppet Master emerges as one of the more enjoyable of the killer toy type horror films".  It currently has a 33% "Rotten" on Rotten Tomatoes 

==Home video==
  stereoscopic versions digital download through the iTunes Store; his first foray into the digital market.

===20th Anniversary Edition Blu-ray===
On June 15, 2010, Full Moon re-released Puppet Master (in its original widescreen format for the first time) in a collectors edition 2-disc set with its eighth canon sequel,   (Puppet Master vs. Demonic Toys is not from Full Moon Features and is not considered part of the series). The set includes packaging resembling Toulons trunk, a poster for Axis of Evil, mini-poster cards for all of the films in the series, and stickers featuring each puppet. The set is available in DVD and Blu-ray, marking Full Moons first foray into the Blu-ray market, including both their first film (Puppet Master) and their latest film (Axis of Evil, at the time).  As of July 27, 2010 both Axis Of Evil and new release of the first film are available to own separately on DVD and Blu-ray.

==Remake== remaking 1989s Puppet Master in 3-D film|3-D.  Similarly, the original film was reissued by Razor Digital in 2007 in DualDisc format, featuring both standard and stereoscopic versions, as well as being uncut, unlike the Full Moon release.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 