Midnight FM
{{Infobox film name           = Midnight FM image          = 200pxlatenightfmmovieposter.jpeg director       = Kim Sang-man producer       = Je Jeong-hun Kim Hong-baek Je Jeong-hun writer         = Kim Sang-man Kim Hwi starring       = Soo Ae Yoo Ji-tae music          = Kim Jun-seong cinematography = Kim Tae-gyeong editing        = Shin Min-kyung distributor    = Lotte Entertainment  released       =   }} runtime        = 106 minutes country        = South Korea language       = Korean budget         =  gross          =   (South Korea) 
| film name = {{Film name hangul         =   FM  hanja          =  의 FM rr             = Simyaui FM  mr             = Simyaŭi FM}}
}}
Midnight FM ( ) is a 2010 South Korean thriller film by Kim Sang-man starring Soo Ae and Yoo Ji-tae. 

Soo Ae won Best Actress at the 31st Blue Dragon Film Awards for her performance. 

== Plot ==
Ko Sun-young is a popular television announcer and midnight DJ on a show that mixes film analysis with selected songs from the associated soundtracks. Sun-young decides to quit her job after her daughter, Eun-soo, requires heart surgery that is only available in the United States. On her last day of work, Sun-youngs sister Ah-young babysits Eun-soo at Sun-youngs apartment. While she is on the air, Sun-young receives a call from a man named Han Dong-soo, who claims to be her fan. Inspired by Travis Bickle, the unstable vigilante from Taxi Driver, Dong-soo has begun to murder pimps and drug dealers throughout the city. When he hears about Sun-youngs imminent retirement, he takes her family hostage and demands that she retool her final broadcast to his specifications. However, Sun-youngs boss discards the new playlist on the assumption that Sun-young herself submitted it.

When Sun-young does not follow his instructions and instead calls the police, Dong-soo murders the policemen that arrive and cuts off one of Ah-youngs toes. With the help of Son Deok-tae, an awkward, obsessed fan, Sun-young attempts to recreate her first broadcasts. When her boss intervenes, Dong-soo murders Ah-young. Furious and in shock, Sun-young explains the situation to her confused co-workers. Dong-soo forces Sun-young to quote her previous commentary on Taxi Driver, in which she requests a hero like Bickle to arise, and demands that she call him a hero; she reluctantly does so. Meanwhile, Sun-young tries to sneak out of the studio to rescue her daughter, but Dong-soo has already fled the house. Sun-young chases after him, and Dong-soo leads her to where he has kidnapped a man he claims to be a human trafficker. The man denies the charge and begs for his life. Dong-soo orders Sun-young to kill the man, but she refuses. Sun-young says that the man deserves a trial, and she shoots Dong-soo instead.

== Cast ==
* Soo Ae as Ko Sun-young
* Yoo Ji-tae as Han Dong-soo
* Lee Joon-ha as Ko Eun-soo, Sun-youngs daughter
* Shin Da-eun as Ko Ah-young, Sun-youngs younger sister
* Choi Hee-won as Ko Hyun-ji
* Ma Dong-seok as Son Deok-tae, Sun-youngs stalker
* Choi Song-hyun as Park Kyung-yang
* Jung Man-sik as Oh Jung-woo
* Kim Min-kyu as Yang Woo-han
* Jo Seok-hyeon as Detective Jo Kim Hyuna as side studio guest Nam Ji-hyun as side studio guest Kwak Byung-kyu as Journalist Jang

== Release ==
Midnight FM was released October 14, 2010, in South Korea. The international premiere was at the Hawaii International Film Festival ten days later. 

== Reception ==
Darcy Paquet of Screen International called it "a better-than-average Korean thriller that despite its plot holes, manages to maintain the tension all the way through."  Shelagh Rowan-Legg of Twitch Film wrote, "Midnight FM can easily be added not only to the radio film canon, but also to the slate of great thrillers being produced in recent years in South Korea."  James Mudge of Beyond Hollywood wrote, "Certainly, director Kim’s enthusiasm makes up for any lack of originality, and thanks to a fine script, fast pace and the efforts of the headlining stars, it stands as one of the best and most fun Korean thrillers of the last year or so." 

== References ==
 

== External links ==
*    
*  
*  
*  
*  
*  

 
 
 
 
 
 
 