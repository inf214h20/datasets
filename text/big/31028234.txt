Cupido pierde a Paquita
{{Infobox film
| name           =Cupido pierde a Paquita
| image          = 
| image size     =
| caption        =
| director       =
| producer       =
| writer         = 
| narrator       =
| starring       =Carlos Orellana
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1955
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1955 Mexico|Mexican film. It stars Carlos Orellana.

==Cast==
* 	 María Victoria	 ...	Paquita (as Maria Victoria Paquita)
* 	 Julio Aldama	 ...	Julio
* 	 Carlos Orellana	 ...	don Severo
* 	 Fernando Soto	 ...	Firulais; Espiridión (as Fernando Soto Mantequilla)
* 	 Malú Azcárraga	 ...	Rosita
* 	 Carlos Martínez Baena	 ...	don Feliciano
* 	 Javier de la Parra	 ...	Pepe
* 	 Paz Villegas	 ...	doña Sofía
* 	 Carmen Manzano	 ...	Juana la charrasqueada
* 	 Lola Casanova	 ...	Adela
* 	 Tito Novaro	 ...	Muñeco
* 	 José Pardavé	 ...	Juan, chofer

==External links==
*  

 
 
 


 