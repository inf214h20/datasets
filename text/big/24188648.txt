Kanyasulkam (film)
{{Infobox film
| name           = Kanyasulkam
| image          = Kanyasulkam.JPG
| image_size     =
| caption        =
| director       = P. Pullaiah
| producer       = D. L. Narayana
| writer         = Gurajada Appa Rao Vempati Sadasivabrahmam
| narrator       = Savitri C.S.R. Anjaneyulu Sowcar Janaki Govindarajula Subba Rao Vinnakota Ramanna Pantulu
| music          = Ghantasala Venkateswara Rao
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 26 August 1955
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Kanyasulkam  is a 1955 Telugu film directed by P. Pullaiah. It is based on the famous Telugu play Kanyasulkam written by Gurazada Appa Rao.

==Cast== Savitri  ...  Madhuravani
* Nandamuri Taraka Rama Rao ...  Gireesam
* Chilakalapudi Seetha Rama Anjaneyulu ...  Ramappa Pantulu
* Govindarajula Subba Rao ...  Lubdhavadhanlu
* Sowcar Janaki ...  Buchamma
* Vinnakota Ramanna Panthulu ...  Agnihotravadhanlu
* Vangara Venkata Subbaiah  ...  Karataka Sastri
* Gummadi Venkateswara Rao ...  Sowjanya Rao Pantulu
* Hemalatha ...  Venkamma Suryakantham ...  Meenakshi
* Chaya Devi ...  Putakullamma
* Chadalavada Kutumba Rao ...  Polisetti
* Subhadra ...  Subbi
* Master Kundu ...  Venkatesam
* Peketi Sivaram ...  Police Inspector
* Master Sudhakar ...  Mahesam
* Kanchi Narasimha Rao ...  Elderly Bridegroom (in the skit Puthadi Bomma Poornamma) Sharada

==External links==
*  

 

 
 
 