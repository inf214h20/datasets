Electra (1962 film)
{{Infobox film
| name           = Electra
| caption        = 
| image	         = Electra FilmPoster.jpeg
| director       = Michael Cacoyannis
| producer       = Michael Cacoyannis
| writer         = Michael Cacoyannis
| starring       = Irene Papas Giannis Fertis Aleka Katselli Manos Katrakis Notis Peryalis Fivos Razi Takis Emmanuel Theano Ioannidou Malaina Anousaki Theodoros Dimitriou Theodore Demetriou Elsie Pittas Petros Ampelas Kitty Arseni Thodoros Exarhos Elli Fotiou Afroditi Grigoriadou Kostas Kazakos
| music          = Mikis Theodorakis
| cinematography = Walter Lassally
| editing        = Leonidas Antonakis
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Greece
| language       = Greek
| budget         = 
}} The Trojan Women in 1971 and Iphigenia (film)|Iphigenia in 1977. It starred Irene Papas in the lead role as Elektra, and Yannis Fertis as Orestis. 

== Plot ==
When the King Agamemnon is murdered by his wife Clytemnestra and her lover and relative Aegisthus, the daughter Electra decides to get even, with the help of her brother Orestes. He helps his cousin Pylades to steal into Clytemnestras house, and despite the fact that she is his mother, stabs her to death, then Aegisthus, as well.

== Cast == Elektra
*Giannis Orestes
*Aleka Klytaemnistra
*Manos Katrakis as the tutor
*Notis Peryalis as Elektras husband
*Fivos Razi as Aegisthus
*Takis Emmanuel as Pylades chorus leader
*Theodoros Dimitriou (Theodore Demetriou) as Agamemnon  Elektra
*Petros Ampelas as young Orestes

==Awards==
The film was entered into the 1962 Cannes Film Festival where it won the award of Best Cinematic Transposition.    The film was also nominated for the Academy Award for Best Foreign Language Film.   

==See also==
* List of historical drama films
* Greek mythology in popular culture
* List of submissions to the 35th Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 