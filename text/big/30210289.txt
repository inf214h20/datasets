The Woman Who Obeyed
{{Infobox film
  | name           = The Woman Who Obeyed
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Sidney Morgan
  | producer       = Astra-National Productions
  | writer         = Sidney Morgan Alicia Ramsey
  | starring       = Stewart Rome Hilda Bayley Peter Dear
  | music          =
  | cinematography = Stanley Mumford
  | editing        = 
  | released       = 1923
  | runtime        = 
  | country        = United Kingdom
  | language       = Silent film English intertitles
  }} British silent film directed by Sidney Morgan.

==Plot==
Overbearing husband separates his wife from her children but is reconciled to her after he has accidentally killed their son. 

==Cast==
*Hilda Bayley as Marion Dorchester
*Stewart Rome as Dorchester
*Henri de Vries as Captain Conway
*Valia as Mrs. Bruce Carrington
*Gerald Ames as Raymond Straithmore
*Ivo Dawson as Duke of Rexford
*Peter Dear as Bobbie Dorchester
*Nancy Price as Governess

==References==
 

==External links==
* 
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 


 
 