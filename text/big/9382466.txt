Tarzan's Desert Mystery
{{Infobox Film
| name           = Tarzans Desert Mystery
| image          = Tarzans Desert Mystery (movie poster).jpg
| image_size     = 
| caption        = 
| director       = Wilhelm Thiele
| producer       = Sol Lesser
| writer         = Carroll Young (story) Edward T. Lowe Jr.
| based on       =  
| narrator       = 
| starring       = Johnny Weismuller Nancy Kelly
| music          = Paul Sawtell
| cinematography = Harry J. Wild Russell Harlan
| editing        = Ray H. Lockert	
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
}}
Tarzans Desert Mystery is a 1943 film starring Johnny Weismuller and Nancy Kelly, and directed by Wilhelm Thiele.
 Brenda Joyce, not OSullivan.)

The film revolves around Tarzans quest, at the urging of Jane, to find a rare African serum to help Allied troops. Tarzans son, Boy, manages to tag along as the apeman journeys into the Sahara, and the two are soon joined by a rambunctious horse and a female American magician, played by Nancy Kelly. The story is mostly a fantasy adventure—with "Arabian Nights"-style characters and sinister Nazi spies—but it also includes considerable comic relief and even science fiction elements. Critics complained that it was aimed more toward juvenile audiences than previous Tarzan films had been.

==Cast==
* Johnny Weissmuller as Tarzan
* Nancy Kelly as Connie Bryce
* Johnny Sheffield as Boy
* Otto Kruger as Paul Hendrix
* Joe Sawyer as Karl Straeder
* Lloyd Corrigan as Sheik Abdul El Khim Robert Lowery as Prince Selim
* Frank Puglia as Magistrate
* Philip Van Zandt as Kushmet

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 