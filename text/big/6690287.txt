Moon of the Wolf
 
{{Infobox film
| name           = Moon of the Wolf
| image          = Moonwolf.jpg
| caption        = A video cover "Moon of the Wolf."
| director       = Daniel Petrie
| producer       = Everett Chambers Peter Thomas Edward S. Feldman Richard M. Rosenbloom
| writer         = Leslie H. Whitten Alvin Sapinsley Geoffrey Lewis Royal Dano John Davis Chandler
| music          = Bernardo Segall
| cinematography = Richard C. Glouner
| editing        = Richard Halsey
| distributor    = American Broadcasting Company
| released       =  
| runtime        = 75 minutes
| country        = United States
| awards         = English
| budget         =
}} American made-for-television Geoffrey Lewis and Bradford Dillman, with a script by Alvin Sapinsley (based on Leslie H. Whittens novel of the same name). The film was directed by Daniel Petrie and filmed on location in Burnside, Louisiana. All of the downtown footage was from Clinton, Louisiana.

==Plot==
  Geoffrey Lewis) arrives at the crime scene and jumps to the conclusion that the girls lover committed the murder, a man whose very name her brother does not know. The towns Dr. Drutan (John Beradino) examines the body and pronounces the girl died of a severe blow to the head caused by a human hand.

The sheriff continues to investigate the crime and interviews people who knew the victim. Local residents have a variety of theories, including the belief she was killed by wild dogs. A posse soon forms to track down the wild dogs with little success. Burrifors continues to insist the killer to be his sisters mysterious lover while the sheriff, in turn, is suspicious of him. The girls sick and dying father Hugh Burrifors (Paul R. DeVille), interviewed by the sheriff, warns him of the "Loug Garog". The sheriff does not understand the French term and local Cajun residents are unable to interpret it.

The sheriffs investigation soon takes him to the plantation home of the wealthy Andrew Rodanthe (Bradford Dillman) and his sister Louise (Barbara Rush). They are the last of a local family dynasty with a history stretching back over a century. Andrew, who the sheriff suspects had an affair with the victim, claims to have been suffering an attack of malaria the night the girl was killed.

The sheriff, suspicious of the temperamental brother Lawrence after he assaults the town doctor (who turns out to be the mysterious lover), soon arrests him and puts him in jail. While there, the full moon rises again and Lawrence and the sheriffs deputy are killed in a vicious attack as the steel bars of the cell are torn from the wall.
 voodoo potion that gives off a vapor meant to repel the "Loug Garog". Rodanthe inhales the potion and goes into what appears to be an epileptic seizure. He is taken to the hospital.

While there, Andrews sister Louise tells the sheriff she can speak French fluently and would like to talk to Hugh Burrifor about the unexplainable term "Loug Garog". While speaking with the old man, Louise solves the puzzle. "Loug Garog" is a mispronunciation of "Loup-Garou". Translated into English the term means "werewolf". The next scene shows Andrew turning into a werewolf, revealing him to be the elusive killer.

Transformed into a werewolf, Andrew violently escapes the hospital and becomes the subject of a man-hunt. Louise talks to Sheriff Whitaker about werewolf folklore. She reveals a family secret that her grandfather used to suffer from unusual spells of sickness, implying he was also a werewolf and Andrews curse was inherited.

Louise returns to her plantation home and is alone when Andrew, still in his werewolf form, quietly enters the house. A frightened Louise attempts to corner the werewolf in a burning barn and eventually shoots the creature with what she assumes are blessed bullets. The sheriff arrives on the scene in time to see Andrew return to his human form before dying.

==See also==
* List of films in the public domain

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 