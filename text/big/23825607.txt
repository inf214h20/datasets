Meatball Machine
{{Infobox Film
| name           = Meatball Machine
| image          = Meatball Machine.jpg
| image_size     =
| caption        = Theatrical poster to Meatball Machine (2005)
| director       = Yūdai Yamaguchi Junichi Yamamoto
| producer       = Yukihiko Yamaguchi
| writer         = Junya Kato
| narrator       = 
| starring       = Issei Takahashi Aoba Kawai Kenichi Kawasaki Shōichirō Masumoto Tōru Tezuka Ayano Yamamoto
| music          = 
| cinematography = Shinji Kugimiya Shuji Momose
| editing        = 
| studio         = 
| distributor    = King Record Co. (Japan) TLA Releasing (USA)
| released       = 
| runtime        = 90 minutes
| country        = Japan Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

  is a 2005 Japanese science fiction horror and gore film directed by Yūdai Yamaguchi and Junichi Yamamoto based on a film by Yamamoto from 1999. Special effects and makeup effects were by Yoshihiro Nishimura.

==Plot==
Yōji is a young lonely factory worker who falls for an equally lonely girl co-worker, Sachiko, but is unable to tell her of his interest.  After he is assaulted in a theater by a crossdresser, Yōji finds what looks like an alien insect and hides it in his room. The next night, he comes across Sachiko being sexually attacked by another fellow worker. He attempts to come to her aid but is beaten. Sachiko feels sorry for him and returns with him to his apartment. During this encounter, Sachiko is attacked by the alien object which penetrates her and turns her into a bio-mechanical monster, a NecroBorg. These parasites take over human bodies and use their flesh to create weapons which they use to fight each other. Yōji is also infected and the plot eventually leads to a showdown fight to the death between the two would-be lovers. A side plot concerns a father who is out to kill the NecroBorgs who have also infected his daughter.

==Production==
Meatball Machine is based on a 1999 film of the same name written, produced and directed by Junichi Yamamoto. The 70 minute theatrical version was released in Japan on May 22, 1999 and starred Toshihisa Watanabe, Akiko Sasaki and Rino Sōma.  

The special effects, special makeup effects and moldmaking for the NecroBorgs were done by noted effects artist Yoshihiro Nishimura who would go on to do effects for The Machine Girl and Tokyo Gore Police.

==Cast==
*Issei Takahashi as Yōji 
*Aoba Kawai as Sachiko  
*Kenichi Kawasaki as Tanaka 
*Shōichirō Masumoto as Doi
*Tōru Tezuka as Tsujimoto
*Ayano Yamamoto as Michino

==Release==
The movie was released theatrically in Japan on September 23, 2006. The DVD was released in Japan on February 7, 2007.  The USA version of the DVD with English subtitles came out June 5, 2007. The DVD releases also contain the original 1999 version of Meatball Machine edited down to a 13 minute short, a making-of featurette and another short Reject or Death which expands on the side plot of the scientist father and his daughter.    

==Reception==
Some reviews called the film a mediocre rip-off of   but the Severed Cinema reviewer calls Meatball Machine the superior of the two movies, "a super-gory hodgepodge tongue-in-cheek ultraviolent video game monster mash misfit movie with ... charm and silly sweetness". 

Another reviewer, however, labels it "a resume reel" and "a difficult movie to defend"    although the Dreamin Demon review says it is "one of the more entertaining B-Movies I have watched in a while."    A further review praises the special effects and makeup effects but not the movie. 

Two of the reviewers mention that the films plot of doomed lovers who cannot connect can be taken as a metaphor for the difficulties and dangers inherent in expressing feelings and how emotional issues can be transformed into something monstrous.  

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 