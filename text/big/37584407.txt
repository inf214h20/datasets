Rough Diamonds (film)
 
{{Infobox film
| name           = Rough Diamonds
| image          =
| caption        =
| director       = Donald Crombie
| producer       = Damien Parer
| writer         = Donald Crombie Christopher Lee
| starring       = Jason Donovan Angie Milliken Peter Phelps Max Cullen
| music          =
| cinematography = John Stokes
| editing        =
| studio         = Australian Film Finance Corporation Beyond Films Film Australia Film Queensland Forest Home Films Nine Network Australia Southern Star Entertainment
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = Australia
| language       = English
| budget         =
| gross          =
}}
Rough Diamonds is a 1994 Australian film. It was the first lead role in a feature for Jason Donovan. 

==Plot==
Mike Tyrell drives a cattle truck and hits a car picked on the side of the road that belongs to ex-singer Chrissie.

==Production==
Donald Crombie was inspired to make the film while filming The Irishman in north Queensland in 1977. He saw a farmer, who owned 80,000 hectares of beef country and on paper was a millionaire, working on a council road gang to make money. He wrote this up as a social drama for Film Australia to star Michelle Fawdon but it was never made.

A number of years later Crombie was approached by Damien Parer who wanted to make features in Queensland and was looking for projects; Crombie showed him his treatment and they developed it into a more populist film. Andrew L. Urban, "Rough Diamonds: Romance, music and cattle theft", Cinema Papers, December 1993 p10-15, 58  The farmer who needed money became a singer who had a romance with a girl singer and saves his farm on the country and western circuit. 

Craig McLachlan was originally going to star but he dropped out to do another job and Jason Donavan was cast instead. The film was financed by Beyond Films and Southern Star Entertainment with finance from the Film Finance Corporation and Film Queensland. It was mostly shot on location in Boonah Shire, Queensland. 

==Release==
The film contained several songs and was sold by the distributors as a musical. It was bought by Rank in England, which led to it being re-cut without Crombies input. The director later claimed:
 When they   saw it, they went, "Its not a musical. Its actually a very real social realist drama with some songs." So what they did then, they cut out all the bit that mattered to me, which was the whole story about this bloke losing a property. So the whole reason for the film, the reason I got involved in it and evolved the whole thing, was taken away by the distributor. The FFC in their wisdom backed the distributors and said, "If they think its got to be like this, its got to be like this." So I said "Fine," and we parted company. Ive never seen the film and I never will, I dont think. Its terrible, I believe, because it doesnt have any heart; theres nothing there. Its never been released, thank God. But I wanted to take my name off it and I was talked out of it, and I now regret that because I realise I should have taken my name off it.   accessed 16 November 2012  

==References==
 

==External links==
* 

 

 
 
 
 
 
 