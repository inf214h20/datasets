The Physician (1928 film)
{{Infobox film
| name           = The Physician 
| image          =
| caption        =
| director       = Georg Jacoby 
| producer       = Maurice Elvey   Gareth Gundrey
| writer         = Henry Arthur Jones (play)   Edwin Greenwood Ian Hunter   Lissy Arna
| music          =
| cinematography = Gaetano di Ventimiglia
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British
| released       = May 1928
| runtime        = 8,260 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent drama Ian Hunter. The film is based on a play by Henry Arthur Jones.  

==Plot==
A former alcoholic tries to lead a crusade against drink, but is himself repeatedly tempted by alcohol. 

==Cast==
* Miles Mander - Walter Amphiel 
* Elga Brink - Edana Hinde  Ian Hunter - Dr. Carey 
* Lissy Arna - Jessie Gurdon 
* Humberston Wright - Stephen Gurdon 
* Julie Suedo - Lady Valerie Camille 
* Mary Brough - Landlady 
* Henry Vibart - Reverend Peregrine Hinde 
* Johnny Ashby - Jessies Son 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 