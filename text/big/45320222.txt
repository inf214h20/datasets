Aparadhi (1949 film)
{{Infobox film
| name           = Aparadhi
| image          = Aparadhi_1949.jpg 
| image size     = 
| caption        = Screen shot of Madhubala and Pran in Filmindia 1949
| director       = Yeshwant Pethkar
| producer       = S. Fattelal
| writer         = Yeshwant Pethkar
| narrator       = Pran Leela Pandey
| music          = Sudhir Phadke 
| cinematography = Manohar Kulkarni 
| editing        =
| studio         = Prabhat Film Company
| distributor    =
| released       = 25th February 1949
| runtime        = 101 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}} 1949 Hindi social thriller drama film directed by Yeshwant Pethkar for Prabhat Film Company.  Films story and dialogue were by Yeshwant Pethkar with music by Sudhir Phadke and lyrics by Amar Varma.    The cast included Madhubala, Ram Singh, Leela Pandey, Pran (actor)|Pran, Uma Dutt, Madhu Apte and Raj Kishore. 
 Independence days and revolves around a husband who suspects his wife of having an affair with a freedom fighter sheltering with them.

==Plot==
Sheela Rani lives with her blind father and works as a radio singer. Master Amar is a Physical Instructor who falls in love with Sheela when he hears her on the radio. He pursues her and they get married. Mohan, a freedom fighter takes shelter with them when he is on the run from the police. Soon Amar starts suspecting his wife of having an affair with Mohan. He starts ill-treating his pregnant wife and throws her out of the house. The wife lives with her father where a child is born to her. Amar then proceeds to abduct the child and thinks of getting the Rs. 5000 prize that the police have placed on ohan. Mohan is arrested and Amar repents his deeds. He realises his mistake and brings his wife home.

==Cast==
* Madhubala as Sheela Rani
* Ramsingh as Master Amar
* Pran as Mohan
* Leela Pandey as freedom fighter
* Uma Dutt as police inspector
* Srinivas Joshi
* Gauri
* Manjrekar

==Review==
Aparadhi got a poor review from the reviewer of Filmindia with the review titled "Poor story with poorer direction" and calling it "Bankrupt direction". The story was cited as being "very thin" with weak screenplay. The music lacked appeal and a song item in Zulu style was especially panned as not required in an"essentially Indian setting". Madhubala was praised and stated to be the "only silver lining".   


==Soundtrack==
The music director was Suresh Phadke with lyrics by Amar Varma. The playback singers were 
Sitara Kanpur and G. M. Durrani. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Zindagani Ka Maza Shadi Mein Hain 
| G. M. Durrani
|-
| 2
| Dil Ro Raha Hai Hum Gaa Rahe Hain 
| Sitara Kanpur
|-
| 3
| Mera Dil Churane Wale Dekho Pyar Nibhana 
| Sitara Kanpur
|-
| 4
| Jaan Pehchan Na Sahab Salam Na 
| Sitara Kanpur
|-
| 5
| Pyar Karne Walo Ke Liye Hai Duniya 
| Sitara Kanpur
|-
| 6
| Ro Na Munna Pyare Kyun Tere Naina  
| Sitara Kanpur
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 