Very Silly Songs!
 

{{Infobox film
| name = "A Very Silly Sing-Along!"
| image = Veggietales dvd silly songs.jpg
| caption = 1999 Lyrick Studios VHS cover
| writer = Mike Nawrocki Phil Vischer
| director = Mike Nawrocki Chris Olsen
| studio = Big Idea Productions
| distributor = Lyrick Studios Word Entertainment
| producer = Chris Olsen Dan Anderson
| music = Phil Vischer Mike Nawrocki
| released =  
| runtime = 30 minutes
| language = English
}}
"A Very Silly Sing-Along!" (Which was Later renamed "Very Silly Songs!") is the seventh episode in the VeggieTales animated series. Released in March 1997, it is the first of several sing-along videos. It features the four Silly Songs with Larry that had been released to date along with a half dozen others from the first five episodes. It also introduced the popular "The Pirates Who Dont Do Anything" whose Silly Song appears here for the first time.

==Plot== Larry the Cucumber, thinking this is the very first "VeggieTales workout video", begins a jumping workout in time to generic, upbeat workout music, then proceeds to trampoline exercises. However, after jumping progressively higher, he hits his head on the cupboards... again and again.

Bob then comes on-screen and explains that this is actually the very first "VeggieTales sing-along tape". To demonstrate, Bob has the French Peas type some lyrics on Qwerty, making the words appear at the bottom of the screen so people can sing along at home. However, they dont stop even after Bob stops singing, and the scene degenerates into true chaos as Bob yells "Roll tape!".

After several songs, Larry, yet again misunderstanding the point of the sing-along, introduces the kids to the very first "VeggieTales home improvement video". Larry announces his plan to replace a leaky fixture on the sink with a new one "which will be quite attractive, and last for years to come." However, Jimmy Gourd doesnt turn off the water supply for the kitchen (he thought they were working on bathrooms and decks), sending Larry flying into the air by a geyser of water when he twists the fixture off.

After even more songs, Bob comes out to introduce his favorite song, "The Bunny Song". However, when one of the French Peas tells him that theyre not supposed to sing it (it was the equivalent of Nebuchadnezzars idol in Rack, Shack and Benny), Bob clarifies by calling it "The New and Improved Bunny Song", in which the original lyrics and their negative implications have been (mostly) replaced with more positive messages. 

Larry then introduces his home-viewers to the very first "VeggieTales financial success video". Bob comes onscreen and finally explains the sing-along theme to Larry, who then asks what song theyll do first. Not wanting to explain now that theyre already almost done with the show, Bob walks off, and Larry boasts to him about having bought a chocolate factory with no money down.

The final song of the tape is a completely new "Silly Songs with Larry". Larry the Cucumber, Pa Grape and Mr. Lunt are the "infamous band of scallywags: The Pirates Who Dont Do Anything." The entire song consists of nonsensical tales of what each of the Pirates have never done. When Larry makes purely random statements of what he has never done, Pa Grape and Mr. Lunt berate him and say he should sing about "pirate-ty things." When Pa Grape asks Mr. Lunt for his opinion on whether Larry is making no sense, Mr. Lunt says Pa Grape looks like Capn Crunch. After they gainsay each other and Pa Grape sentences Mr. Lunt to walk the plank, Larry sings another verse of nonsense. Pa Grape declares Larry does not understand what they said previously about his singing; they finish the song with the refrain "And weve never been to Boston in the fall." They decide to watch Geraldo Rivera|Hiraldo, and Larry finds a quarter.

== Songs ==
# VeggieTales Theme Song (1993 original version/1998 version on some releases of the video)
# I Can Be Your Friend (from Are You My Neighbor?)
# Dance of the Cucumber (from Rack, Shack & Benny)
# The Forgiveness Song (from God Wants Me To Forgive Them!?!)
# The Water Buffalo Song (from Wheres God When Im S-Scared?)
# God Is Bigger (from Wheres God When Im S-Scared?)
# Love My Lips (from Dave and the Giant Pickle)
# Oh, No! (from Wheres God When Im S-Scared?)
# Stand Up! (from Rack, Shack & Benny)
# The Hairbrush Song (from Are You My Neighbor?)
# The New and Improved Bunny Song (from Rack, Shack & Benny)
# The Pirates Who Dont Do Anything!

==References==
*http://www1.epinions.com/kifm-review-1707-2C7C00A-388C6C2C-prod1
*http://christiananswers.net/spotlight/movies/pre2000/rvu-vegsing.html

==External links==
*  

 

 

 