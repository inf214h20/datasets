Varsha (2005 film)
{{Infobox film
 | name = Varsha
 | image = Varsha05film.jpg
 | caption = Promotional Poster
 | director = S. Narayan
 | producer = Rockline Venkatesh Siddique
 | screenplay = S. Narayan
 | based on =   Vishnuvardhan Ramesh Manya Komal Doddanna
 | music = S. A. Rajkumar
 | cinematography = P. K. H. Das
 | editing = P. R. Soundar Raj
 | released = 22 April 2005
 | runtime = 153 minutes
 | studio =  Rockline Productions Kannada
 | budget = 
 }}
 2005 Cinema Indian feature directed by Vishnuvardhan in the lead role supported by Ramesh Aravind and Manya (actress)|Manya. The film is the remake of the 1996 Cinema of Kerala|Malayalam-language film Hitler (1996 film)|Hitler starring Mammootty directed by Siddique (director)|Siddique.

==Plot==
Sathya has come up in life the hard way as he lost his mother when he was a small boy and his father was framed and arrested on criminal charges. Sathya, unaware of his fathers innocence, takes custody of his five young sisters. Sathyas father Bhadra was cheated by his brother-in-law who poisons his sisters mind to leave her husband, but she commits suicide later and her son Sathya is left with his five sisters. Sathya take care of his sisters well and educates them, but they are under his strict control and any person who plays foul with them faces his wrath, but he finds himself in trouble because of this tough character. Meanwhile, Sathyas father is released from prison and now wants to take revenge on his brother-in-law but the latter joins hands with some villains who want to settle scores with Bhadra and his son. They hatch a plan to a create rift in Sathyas family and surprising everyone succeeds as well. Will Sathya reunite his family again, and prove his fathers innocence form the climax. 

==Cast==
 Vishnuvardhan as Sathya
* Ramesh Aravind Manya
* Komal
* Doddanna
* Srinivasa Murthy
* Shobaraj

==Soundtrack==
{{Infobox album
| Name        = Varsha
| Type        = Soundtrack
| Artist      = S. A. Rajkumar
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

S. A. Rajkumar composed the films background score and music for its soundtrack, with the lyrics written by S. Narayan. The soundtrack album consists of six tracks.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Goli Maaro
| lyrics1 = S. Narayan
| extra1 = Mathangi Jagdish
| length1 = 
| title2 = Ikku Maga
| lyrics2 = S. Narayan
| extra2 = S. A. Rajkumar
| length2 = 
| title3 = Kanneerige Kanneerenu
| lyrics3 = S. Narayan Chithra
| length3 = 
| title4 = Idenidu Badukina Vesha
| lyrics4 = S. Narayan
| extra4 = S. P. Balasubrahmanyam
| length4 = 
| title5 = Thananana Thananana
| lyrics5 = S. Narayan
| extra5 = Hariharan (singer)|Hariharan, Shreya Ghoshal
| length5 = 
| title6 = Vaasanthi Vaasanthi
| lyrics6 = S. Narayan
| extra6 = Hariharan, Shreya Ghoshal
| length6 = 
}}

==References==
 

 
 
 
 
 
 


 