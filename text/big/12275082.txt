The Mangler Reborn
{{Infobox Film
| name           = The Mangler Reborn
| image          =  The Mangler Reborn.jpg|
| caption        = U.S. poster
| imdb_rating    = 
| director       = Erik Gardner Matt Cunningham
| producer       = Mark Burman Scott Pearlman
| writer         = Erik Gardner & Matt Cunningham
| based on =  
| starring       = Aimee Brooks Weston Blakesley Juliana Dever Sarah Lilly Scott Speiser Renee Dorian Rhett Giles Jeff Burr Reggie Bannister
| music          = Climax Golden Twins
| cinematography = Thaddeus Wadleigh
| editing        = Matthew Cassel
| distributor    = Lions Gate Entertainment, Baseline StudioSystems
| released       =  
| runtime        = 90 min
| country        = 
| language       = English
| budget         = 
}}
 short story by Stephen King. The movie was released straight to DVD on November 29, 2005 by Lions Gate Entertainment and Baseline StudioSystems. Directors Gardner and Cunningham intended the film to be a "rebirth" of the film franchise, with the film not requiring viewers to have seen the prior two films. 

==Synopsis==
The movie takes place after the events in the first film and follows Hadley (Weston Blakesley), a repairman that purchases the possessed machine from the first movie and becomes obsessed with the machine. Hadley awakens the machine with his blood and after being "eaten" by it, is forced to feed it new blood to stop his corpse from rotting.

==Cast==
*Aimee Brooks as Jamie
*Reggie Bannister as Rick
*Weston Blakesley as Hadley
*Scott Speiser as Mike
*Juliana Dever as Louise Watson
*Sarah Lilly as Beatrice Watson
*Renee Dorian as Gwen
*Rhett Giles as Sean
*Jeff Burr as Lawnmowing Man

==Reception==
Critical reception for The Mangler Reborn was largely negative.   DVD Talk panned the film, criticizing it as "a formless, meandering, and stunningly boring little turkey".  Reel Film also gave a negative review, writing that while "the film comes off as a masterpiece when compared with its predecessors, The Mangler Reborn ultimately just doesnt work". 

==References==
 

== External links ==
*  

 
 

 
 
 