Vice Squad (1953 film)
{{Infobox film
| name           = Vice Squad
| image          = Vice Squad 1953 poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Arnold Laven
| screenplay     = Lawrence Roman
| based on       =  
| narrator       = 
| starring       = Edward G. Robinson Paulette Goddard K.T. Stevens Porter Hall
| music          = Herschel Burke Gilbert
| cinematography = Joseph F. Biroc
| editing        = Arthur H. Nadel
| distributor    = United Artists
| released       =   
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Vice Squad is a 1953 police procedural film directed by Arnold Laven and starring Edward G. Robinson as a police captain with the Los Angeles Police Department and Paulette Goddard as one of his informants. 

==Plot==
A married undertaker having an affair, Jack Hartrampf, is a reluctant eyewitness to the shooting of a Los Angeles cop. He does not wish to testify, but captain of detectives "Barney" Barnaby is just as determined.

After a bank robbery pulled by Alan Barkis and his gang, another policeman is gunned down and a bank teller is taken hostage. Escort agency madam Mona Ross is willing to help Barnaby with the case for a fee.

Barnaby places one of Barkis partners, Marty Kusalich, under arrest until Marty implicates the real killer. Pete Monte steals a boat in an attempt to get Barkis to freedom, but Barnaby and his lieutenant, Lacey, arrive in the nick of time.

==Cast==
* Edward G. Robinson as Capt. Barnaby
* Paulette Goddard as Mona Ross
* K.T. Stevens as Ginny
* Porter Hall as Jack Hartrampf
* Edward Binns as Al Barkis Adam Williams as Marty Kusalich
* Lee Van Cleef as Pete Monte

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 