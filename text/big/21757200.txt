Palm Beach (film)
 
{{Infobox film name           = Palm Beach image          =  caption        =  producer       = Albie Thoms director       = Albie Thoms  writer         = Albie Thoms starring  Nat Young Bryan Brown music          = Terry Hannigan cinematography = Oscar Scherl editing        = Albie Thoms studio = Albie Thoms Productions distributor    =  Albie Thoms released       = 6 March 1980 runtime        = 88 minutes country        = Australia language       = English  budget         = A$100,000 
}} Sydney during 2 days. Thoms was nominated for an AFI award for Best Original Screenplay for the film. 

Thoms had made a large number of experimental films but his was his first traditional feature. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p280 

==Cast==
*Kenneth Brown as Joe Ryan	
*Nat Young as Nick Naylor
*Amanda Berry as Leilani Adams
*Bryan Brown as Paul Kite
*Julie McGregor as Kate OBrien
*John Flaus as Larry Kent
*Bronwyn Steven-Jones as Wendy Naylor
*David Lourie as Zane Green
*Peter Wright as Rupert Robert

==References==
 
* 

==External links==
* 
*  at Oz Movies
 
 
 
 
 
 


 
 