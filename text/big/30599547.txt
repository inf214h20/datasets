Frederick Douglass and the White Negro
 
{{Infobox Film
| name = Frederick Douglass and the White Negro| image = Frederick_Douglass_and_the_White_Negro.png
| director = John J Doherty
| producer = Catherine Lyons
| writer = John J Doherty
| starring = 
| narrator = John J Doherty
| music = Cyril Dunnion Gerard Meaney
| cinematography = Ronan Fox Martin Birney John J Doherty
| editing = Juris Eksts
| runtime = 52 minutes
| country = Ireland
| language = English Irish
}}

Frederick Douglass and the White Negro is a documentary film originally released in 2008 (Irish language version entitled Frederick Douglass agus na Negroes Bána).

==Synopsis==
 , Northern Ireland.]] abolition and Great Famine America where he buys his freedom with funds raised in Ireland and Britain. Fellow passengers on his return journey include the Irish escaping the famine who arrive in their millions and would go on to play a major role in the New York Draft Riot of 1863 which Douglass could only despair over. The film examines (with contributions from the author of How The Irish Became White Noel Ignatiev amongst others) the turbulent relationship between African Americans and Irish Americans during the American Civil War, what drew them together and what drove them apart and how this would shape the America of the twentieth century and beyond.

==See also==
*List of films featuring slavery

==References==
 
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 