The Devil's Rock
 
 
{{Infobox film
| name           = The Devils Rock
| image          = TheDevilsRock poster2011 2k.jpg
| alt            =  
| caption        =  Paul Campion
| producer       = Leanne Saunders Paul Campion Brett Ihaka Craig Hall Matthew Sunderland Gina Varela Karlos Drinkwater
| music          = Andrea Possee
| cinematography = Rob Marsh
| editing        = Jeffrey Hurrell
| studio         = 
| distributor    = Metrodome (UK) Vendetta (NZ) eOne (USA)
| released       =  
| runtime        = 86 minutes
| country        = New Zealand
| language       = English
| budget         = 
| gross          = 
}} New Zealand Paul Campion, Craig Hall, Matthew Sunderland, Gina Varela, and Karlos Drinkwater. It is set in the Channel Islands on the eve of Normandy Landings|D-Day and tells the story of two New Zealand commandos who discover a Nazi occult plot to unleash a demon to win World War II. The film combines elements of war films and supernatural horror films.

==Plot==
 Craig Hall) and Sergeant Joe Tane (Karlos Drinkwater), paddle in their Klepper canoe to Forau Island, landing on a beach covered in anti-personnel mines and tank traps. When they leave the beach and head inland, they begin to hear distant screaming and gunfire. They approach a German fortification and hear what they think is a man being tortured. They climb down into a large gun pit and place explosives on a large artillery gun but are disturbed when a German soldier (Luke Hawker) runs out of a tunnel pleading for help. Grogan stabs the soldier in the back of the neck and kills him. They hear a woman screaming, and Grogan decides to investigate while Tane remains outside. However, when Tane hears a gunshot he also enters the bunker to investigate. While looking for Grogan, he discovers a book of black magic and, distracted by its contents, is killed by an unseen assailant. Grogan, unharmed, later discovers Tanes body but is immediately knocked unconscious by the Nazi.

Grogan wakes and is briefly tortured by a Nazi, Colonel Meyer (Matthew Sunderland), who wants to know his mission. During the interrogation, Grogan hears a woman screaming from another room.  He eventually escapes and chases Meyer into the tunnels, shooting and injuring him. When he follows the sound of the womans screams up to a room covered in occult symbols, he discovers that the woman is his dead wife, Helena (Gina Varela). Meyer enters the room and shoots Grogan in the leg, then shoots Helena in the head, apparently killing her. Grogan attacks Meyer, who explains the woman is a demon, summoned up from a book of black magic found on the island. Meyer proves this by offering her the leg of a dead German to eat; she changes into her true demon form as she eats the leg.

After Grogan removes a bullet from his abdomen, Meyer passes out. Grogan searches him and discovers a page torn from the book of black magic in a small pouch worn as a necklace by the Nazi. Grogan keeps the page after replacing it with another from the book. Soon after, Meyer recovers and explains the demon is a shapeshifter and a weapon the Germans plan to use against the Allies. He also explains that it is confined to the island because it cannot cross moving water.  However, Meyer now realizes the demon poses too great a threat to the world. Meyer offers to give the book to Grogan if he will help him escape from Germany. Meyer then persuades Grogan to help him perform a ritual to dispel the demon back to Hell. Meyer, believing he is protected with the incantation sheet from the book, betrays Grogan at the end of the ritual.

As Meyer reveals his true intent to use the demon for the Nazis, Grogan overpowers Meyer and throws him to the demon. She brutally kills Meyer while Grogan survives, protected by the incantation he has taken from Meyer. When she tries to convince him to take her with him as Helena, he tells her that she could never replace the real Helena he knew, taking the opportunity to chain her up again.  Unable to complete the ritual alone, Grogan takes the book and leaves the demon behind, to prey on any Germans that come to investigate; he explains to the demon that he intends to come back when the war is over to finish the ritual and to banish her forever. He leaves the key to the chain within reach as he leaves the demon in the bunker.  He steps onto the beach, buries the photo of his wife Helena he kept with him, then looks up and sees planes flying overhead and an armada heading towards France.  D-Day has begun.

One Nazi does fall prey to the demon, who masquerades as a German-speaking woman, Nicole, during a coda in the closing credits.

==Cast==
 Craig Hall as Captain Ben Grogan
* Matthew Sunderland as Colonel Klaus Meyer
* Gina Varela as Helena/The Demon
* Karlos Drinkwater as Sergeant Joe Tane
* Luke Hawker as Private Muller Jonathan King as Suicide German
* Hadyn Green as Dead German
* Jessica Grace Smith as Nicole
* Geraldine Brophy as the Voice of the Demon

==Production==
 ]]

The film was produced by New Zealand producer Leanne Saunders and co-funded by the New Zealand Film Commission.

Although set in Europe, the film was shot over 15 days    in August 2010 in Wellington, New Zealand, on sets built at Island Bay Studio, on location at Breaker Bay, and at Wrights Hill Fortress, a semi-restored World War II hilltop fortification. Special makeup effects for the film were created by Weta Workshop.

==Historical references==

 

The film contains references to real historical events, and Campion has stated that he based the story on the German Occupation of the Channel Islands.  Guernseys history of witchcraft and the occult includes the existence of the "Bad Books" (books of black magic), and copies can be found in two libraries in the Channel Islands. 
 Herbert Blondie Hasler.

As Meyer ties Grogans thumbs with a piece of wire to torture him, Meyer talks about the Allied "gangster commandos, who raided these very islands and killed innocent German prisoners with their hands tied behind their backs", which is a reference to Operation Basalt, a British Commando raid on Sark during which a German prisoner was shot dead whilst his hands were tied, which in turn lead to Adolf Hitler issuing his Commando Order, upon which the torture scene in the film is based.

When Meyer is attempting to interrogate Grogan, he taunts Grogans New Zealand background: "New Zealanders, a bunch of farmers driving around the deserts of North Africa, attacking by night and fleeing to hide like cowards", which is a reference to the New Zealand section of the Long Range Desert Group.

Meyer also taunts Grogan by insulting the Maoris of New Zealand, which he describes as "the descendents of cannibals and headhunters", which is a reference to a 1940s German radio propaganda broadcast.   

==Release==
Metrodome bought the UK rights in 2010 while the film was still in post production.    The film was released in theatres and video-on-demand services on 8 July 2011 and on DVD on 11 July 2011. The film was released in 21 cinemas New Zealand on 22 September 2011 and was released on DVD and Blu-ray in December 2011. Entertainment One bought the North American rights at the 2011 Cannes Film Festival.   

===Film festival screenings===
 Fantasia International Film Festival – Montreal, Canada, August 2011
* Icon TLV Film Festival – Tel-Aviv, Israel, September 2011
* Ramskrik Film Festival – Norway, October 2011
* San Sebastian Horror & Fantasy Film Festival – San Sebastian, Spain, October 2011
* Yubari International Fantastic Film Festival – Yubari, Japan, February 2012
* A Night of Horror International Film Festival – Sydney, Australia, March 2012
* Zinema Zombie Fest – Bogotá, Columbia, November 2012

==Critical Response==

"Although its a Kiwi production, The Devils Rock feels like a more worthy successor to the Hammer Film Productions studios ethos than actual new Hammer-label product such as The Resident" 

==Awards==
The film was nominated for Best Visual Effects and Best Costume Design, and won for Best Makeup Design at the 2012 Sorta Unofficial New Zealand Film Awards.   

==References==
 

==External links==
*  
*  
*   at Paul Campions website
*   – German Fortifications of Guernsey
*   – Sorta Unofficial New Zealand Film Awards

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 