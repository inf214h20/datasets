Raktha Kanneeru
{{Infobox film
| name           = Raktha Kanneeru
| image          = Raktha Kanneeru.jpg
| caption        = Film poster
| image_size     =
| director       = Sadhu Kokila
| producer       = Munirathna
| writer         =
| narrator       =
| screenplay     = Upendra
| starring       = Upendra Ramya Krishna Sadhu Kokila
| music          = Sadhu Kokila
| cinematography = Krishna Kumar
| editing        = Lakshmana Reddy
| distributor    =
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Kannada
| budget         =
| gross          =
| website        =
}}
 Kannada film Tamil film Ratha Kanneer starring M. R. Radha   The screenplay and dilogues of the film were written by Upendra and the music was composed by Sadhu Kokila.

Upon release, the film broke all opening box office records in Karnataka. It was the highest-grossing film of the year 2003. The film was followed by a semi sequel titled Katari Veera Surasundarangi released in 2012. 

==Cast==
*Upendra
*Ramya Krishnan
*Abhirami (actress) 
*Kumar Bangarappa 
*Jyothilakshmi
*Doddanna
*Arif Ali
*Bank Janardhan, 
*Sadhu Kokila
* J. V. Somayajulu
*Kota Srinivasa Rao 
*Sudhakar
*Kallu Chidambaram

==Soundtrack==
 
The music was composed by Sadhu Kokila.

{| class="wikitable sortable"
|-
! Title !! Singers
|-
| Danger || Hemanth 
|-
| Baa Baaro Rasika || Soumya, Upendra 
|-
| Jaana Jaana || Arif Ali, Nandita
|-
| Kanneeridhu Raktha Kanneeridhu || Rajesh Krishnan 
|-
| Navila Navila || Nandita, Rajesh Krishnan
|}

===Reception===
The Soundtrack of Raktha Kanneeru was one of the best selling albums of the year. Upon its audio release, a record number of audio cassettes were sold.

==Box Office==
Raktha Kanneerus BKT distribution rights were sold at a record price of  1.6 Crore. The film went on to break many opening box office records upon release. From Bangalore alone, the film collected a share of more than  1 Crore in its first week,  a record collection for Kannada cinema at that time. The film did record collections all over Karnataka and went on to become one of the highest grossing films of its time.

==Sequel== 3D Mythology|Mythological 3D film Lord Indras Indraloka (heaven) Lord Brahma, Lord Yama Indraloka to marry Indraja.  The film was hugely successful although not as much as its predecessor. 

==See also==
* Ratha Kanneer

==References==
 

==External links==
*  
* 
*  

 

 
 
 
 