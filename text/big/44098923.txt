Hrudaya Hrudaya
{{Infobox film
| name = Hrudaya Hrudaya
| image = 
| caption =
| director = M. S. Rajashekar
| writer = KSDL Chandru   Anantha Rangachar
| starring = Shivarajkumar  Ramesh Aravind   Anu Prabhakar   Sharath Babu
| producer = Parvathamma Rajkumar
| music = Hamsalekha
| cinematography = B. C. Gowrishankar
| editing = S. Manohar
| studio = Sri Vaishnavi Combines
| released =  
| runtime = 154 minutes
| language = Kannada
| country = India
| budget = 
}} romantic drama film directed by M. S. Rajashekar and produced by Parvathamma Rajkumar. The film features Shivarajkumar, Ramesh Aravind and Anu Prabhakar, making her debut, in the lead roles whilst Sharath Babu, Avinash and Chitra Shenoy play the supporting roles. 

The film featured original score and soundtrack composed by Hamsalekha. At the 1999-00 Karnataka State Film Awards, the film was awarded in 3 categories including Best actor, Best Music director and Best Dialogue writer.

== Cast ==
* Shivarajkumar
* Ramesh Aravind 
* Anu Prabhakar 
* Sharath Babu
* Avinash
* Chitra Shenoy
* Mandeep Roy
* Jyothi
* Siddaraj Kalyankar
* Jayakumar Master Vinay Raghavendra

== Soundtrack == Rajkumar and K. S. Chitra topping the charts.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = O Premada Gangeye
| extra1 = Rajkumar (actor)|Rajkumar, K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Hey Hrudaya
| extra2 = S. P. Balasubrahmanyam
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Mithra Mithra
| extra3 = L. N. Shashtry
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Venkatesha Venkatesha
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Hogi Baa Hogi Baa
| extra5 = Rajkumar (actor)|Rajkumar, K. S. Chithra
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Ivale Nanna Rani
| extra6 = Rajesh Krishnan, Shivarajkumar, Manjula Gururaj
| lyrics6 = Hamsalekha
| length6 = 
}}

==Awards==
* Karnataka State Film Awards (1999-2000)
 Best Actor - Shivarajkumar Best Music director - Hamsalekha Best Dialogue writer - A. G. Sheshadri

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 

 