Giran (film)
{{Infobox film
| name           = Giran
| image          = 
| caption        = 
| director       = Tahani Rached Mona Assaad
| producer       = Studio Masr
| writer         = 
| starring       = 
| distributor    = 
| released       = 2009
| runtime        = 105 minutes
| country        = Egypt
| language       = 
| budget         = 
| gross          = 
| screenplay     = Tahani Rached Mona Assaad
| cinematography = Nancy Abdel-Fattah
| editing        = Mohamed Samir
| music          = Tamer Karawan
}}

Giran is an Egyptian 2009 documentary film.

== Synopsis == Garden City was a small residential area bordering on downtown Cairo, Egypt, where international political leaders had their residences. Giran walks us through this neighborhood as it is nowadays. Abandoned mansions, luxurious salons, embassies, shops or rooftops, where a whole family lives. The houses and their occupants, witnesses of the changes in History, tell tales of break-ups, hopes and survival.

== References ==
 

==External links==
*  

 
 
 
 


 
 