Kalabha Kadhalan
{{Infobox film|
| name = Kalabha Kadhalan
| image = weetgg.jpg
| director = Igore
| Cinematography = Siddharth Arya Renuka Menon Akshaya
| producer = Vishnu Talkies
| music = Niru
| lyrics =
| screenplay =
| released =  
| runtime = 165 minutes
| country = India
| language = Tamil
| budget =
}} Arya and Renuka Menon. This film was a low-budget production and the soundtrack  was composed by Niru. Akshaya also appeared in this family based psycho thriller.  It enjoyed relatively quiet success. It shared a similar storyline to a great thriller S.J. Suryah’s Vaali and another 2006 film called Uyir. The film’s censors demanded it an "Adults Only" rating.
A sister in law (Akshaya) falls in love with her brother in law Akhilan (Arya (actor)|Arya). But he refuses her because he adores his wife (Renuka Menon) and he never would cheat on her. He hides all facts to his wife in order to protect her. Finally the sister in law commits suicide after a tragedy that becomes the climax of the film.

==Cast== Arya as Akilan
*Renuka Menon as Anbarasi
*Akshaya (actress) as Kanmani
*Pawan
*Ilavarasu

==Production==
The film, produced by Vishnu Talkies, is scripted and directed by Iger, who has directed some commercials and worked with directors like Velu Prabhakaran, Narayanamurthy and Nanda Periyasami. The film has been shot at locations in Chennai, Nagercoil, Tenkasi, Kutralam and Mahabalipuram.

The film introduces a new music director in Iru, who is based in France and has released many Tamil music albums there. Wielding the camera is R Mathi, with art-work by Milan, and dialogue penned by Balakumaran. 

==Critical reception==
*Nowrunning wrote:"Kalaba Kadhalan explores relationships, marriage, romance and sex.Importantly, this lurid subject matter is handled subtly where no one gender being offended. Debutant director Igor has made this movie honestly, convincingly and believably. The plot, details, characterization and dialogue are good". 

*Thiraipadam wrote:"The most surprising thing about Kalaaba Kaadhalan is how boring it is, a poorly-paced screenplay and a weak climax make the film a surprisingly sedate affair". 

*Indiaglitz wrote:"Igor shows a skill for handling a difficult subject. He traverses the tightrope of seduction and vulgarity quite nicely. Something you dont expect a debutant to do. On the whole, Kalaba Kadhalan is watchable despite some glitches. 

==Soundtrack==
Music is composed by newcomer Niru. The soundtrack contains five songs.  The audio launch was held on December 12, 2005 at the Satyam complex. The audio was launched by Vikram and received by SJ. Suryah. Producer Council President Mr.Thiagarajan also took part in the function. Along with them the films hero Arya, director Ighor, producer Nandagopal and many other dignitaries took part in the event. Two songs were screened during the release. 

* Chellame Idhu - Karthik, Sunitha Sarathy
* Pattuselai - Krishnaraj, Sriram, Nithyasree
* Manmeethu Aangal - Haricharan
* Thogai Virithu - Chinmayee
* Urugudhe - Andrea

==References==
 

==External links==
*  

 
 
 
 