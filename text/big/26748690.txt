Die Reiter von Deutsch-Ostafrika
{{Infobox film
| name           = Die Reiter von Deutsch-Ostafrika
| image          =
| image_size     =
| caption        =
| director       = Herbert Selpin
| producer       = Walter Zeiske (executive producer)
| writer         = Marie Luise Droop (novel & screenplay) Wilhelm Stöppler (dramaturge)
| narrator       =
| starring       = See below
| music          = Herbert Windt
| cinematography = Emil Schünemann
| editing        = Lena Neumann
| distributor    =
| released       = 1934
| runtime        = 89 minutes (Germany)
| country        = Germany
| language       = German
| budget         =
| gross          =
}}

Die Reiter von Deutsch-Ostafrika is a 1934 German film directed by Herbert Selpin.

The film is also known as The Riders of German East Africa (in the USA).

== Plot summary == East African Schutztruppe command, they stop at Klixs grave. Hellhoff promises the dead boy he will come back sooner or later — an allusion to the recovery of the lost colony through German victory in the First World War.

=== Differences from novel ===
 

== Cast ==
*Sepp Rist as Deutscher Farmer Peter Hellhoff / Hauptmann der Reserve
*Ilse Stobrawa as Gerda, seine Frau
*Ludwig Gerner as Hellhoffs Assistant Lossow
*Rudolf Klicks as Volontär Wilm Klix
*Peter Voß as Englischer Farmer Robert Cresswell
*Georg H. Schnell as Colonel Black, englischer Generalstabsoffizier
*Arthur Reinhardt as Safari-Führer Charles Rallis
*Emine Zehra Zinser as Dienerin Milini
*Lewis Brody as Hellhoffs Aufseher Hamissi
*Mohamed Husen as Signalschüler Mustapha
*Gregor Kotto as Hellhofs Boy Selemani
*Vivigenz Eickstedt as Englischer Offizier

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 