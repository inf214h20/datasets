Wuthering Heights (1920 film)
 
 
{{Infobox film
| name           = Wuthering Heights
| image          = Wuthering_Heights_1920.jpg
| caption        = Theatrical release poster
| director       = A.V. Bramble
| producer       = 
| writer         = Eliot Stannard
| based on       =  
| starring       = Milton Rosmer Colette Brettel Warwick Ward   Ann Trevor
| music          =
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       =   
| runtime        = 90 minutes
| country        = United Kingdom 
| language       = Silent (English intertitles)
| budget         =
}}
Wuthering Heights is a 1920 British silent drama film directed by A.V. Bramble and starring Milton Rosmer, Colette Brettel and Warwick Ward.  It is the first film adaptation made of the novel Wuthering Heights by Emily Brontë. Its survival status is classified as unknown,  and is considered to be a lost film.

==Cast== Heathcliff
* Catherine Hareton
* Warwick Ward - Hindley Earnshaw
* Ann Trevor - Cathy
* John L. Anderson - Edgar Linton
* Cecil Morton York - Earnshaw Hareton
* Dora De Winton - Mrs Linton
* Aileen Bagot - Frances Earnshaw
* Mrs. Templeton - Nelly Dean
* George Traill - Joseph
* Alfred Bennett - Reverend Shields
* Albert Brantford - Heathcliff, as a child
* Lewis Barber - Hareton, as a child

==See also==
*List of lost films

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 

 