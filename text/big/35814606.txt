Allari (film)
 Allari}}
{{Infobox film
| name           = Allari
| image          =
| caption        =
| director       = Ravi Babu
| producer       = Ravi Babu   Suresh Movie Film Distributor   
| story          =
| screenplay     = Ravi Babu
| writer         = Nivas  (dialogues) 
| starring       = Allari Naresh Swetha Agrawal
| music          = Paul J
| cinematography = Loknath
| editing        = Marthand K. Venkatesh
| studio         = Flying Frogs
| distributor    = Suresh Movies
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Allari  is a 2002 Telugu language|Telugu-language film directed  and produced by Ravi Babu under Flying Frogs and starring Allari Naresh and Swetha Agrawal in lead roles. 

== Cast ==
* Allari Naresh ... Ravi
* Swetha Agrawal ... Aparna
* Nilambari  ... Ruchi
* Sanjay
* Chalapathi Rao  ... Visweswara Rao (Ruchis father)
* Shakeela  ... Sujatha (Ruchis mother)
* Kota Srinivasa Rao ... Mahabala Rao (Ravis father)
* Tanikella Bharani ... Secretary
* Sudha ... Aparnas mother
* Lakshmipati  ... Lift boy, Gardener
* Apoorva
* Baby Satya Gayatri
* Subhashini

==Music==
{{Infobox album
| Name        = Allari
| Tagline     =
| Type        = soundtrack
| Artist      = Paul J
| Cover       =
| Released    = 2002
| Recorded    =
| Genre       =
| Length      =
| Label       =
| Producer    =
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
The music  of the film was composed by Paul J. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
! No. !! Song !! Singers !! Length (m:ss)
|- Suresh Peters, Chinmayi, Aparna || 03:31
|- Devan ||04:14
|- Mano (singer)|Mano, Grace Karunas  ||03:26
|-
| 4|| "Nara Naram " ||Srinivas, Aparna  ||03:44
|- Ravi Varma, Lavanya||03:42
|}

==References==
 

 

 
 
 
 


 