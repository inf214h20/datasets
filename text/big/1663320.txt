Just My Luck (2006 film)
 
{{Infobox film
| name           = Just My Luck
| image          = justmyluck.jpg
| caption        = Theatrical release poster
| director       = Donald Petrie
| producer       = Arnon Milchan Arnold Rifkin Donald Petrie
| writer         = I. Marlene King Amy B. Harris Jonathan Bernstein James Greer
| starring       = Lindsay Lohan Chris Pine Faizon Love Missi Pyle McFly
| music          = Teddy Castellucci
| cinematography = Dean Semler
| editing        = Debra Neil-Fisher
| studio         = Regency Enterprises Cheyenne Enterprises Silvercup Studios
| distributor    = 20th Century Fox
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $28 million
| gross          = $38,077,373
}}
Just My Luck is a 2006 American romantic comedy film directed by Donald Petrie and written by I. Marlene King and Amy B. Harris. The film stars Lindsay Lohan and Chris Pine as the main characters.  Lohan stars as Ashley, the luckiest girl in Manhattan, New York. She loses her luck after kissing Jake, portrayed by Pine, at a masquerade bash.  

The film features supporting roles by Samaire Armstrong, Faizon Love, Missi Pyle, and McFly, the English band whom Jake discovers in the film and tries to help by finding them a producer.

==Plot==
The film follows the life of lucky and popular Ashley Albright (Lindsay Lohan) who has an extremely fortunate life and is always experiencing remarkable strokes of luck in contrast to Jake Hardin (Chris Pine), manager of McFly, who is followed by bad luck wherever he goes 

Persistent in her effort to hold onto a client, Ashley manages to sell Damon Phillips (Faizon Love) on the idea of a masquerade party.  When Ashleys boss, Peggy, learns of the plan, she is, to Ashleys surprise, impressed with the idea, leaving her with the task of organizing the party independently. Jake, hearing of the party and of the number of possible sponsors who could be there, hatches a plan to attend, in order to meet a very famous man (Phillips) who could publicise his band. He poses as a dancer for the masquerade party and gains entry.

During the party, Ashley is confronted by a  ).

Desperate to regain her luck, Ashley revisits the fortune teller, who explains that, in order for her life to return to what it was before, she must again kiss the person she kissed at the party. Ashley begins hunting down all the dancers from the company who had supplied the dancers at the party. While on her search, Ashley tries to get some food from a cafe but is rejected because she doesnt have enough money. She creates mass chaos (due to bad luck) and makes a mad dash out of the cafe with the help of Jake, who offers Ashley his previous job as a janitor/food delivery person at the bowling alley. Ashley, desperate for a job, accepts immediately.

Later, Jake takes Ashley to the place where McFly is practising. She discovers that Jake is the one who has stolen her luck. Seizing her chance, she kisses him, whereupon he gets his bad luck back; Ashleys luck has returned. That night, she thinks hard about her life and about how Jake had put his (her) good fortune to use. She decides that her life was more enjoyable without luck and goes to Jakes first concert for McFly, during which things arent running smoothly.

Ashley kisses Jake, given luck again, and the concert is saved. Ashley decides to end contact with Jake, as she doesnt want their luck to be switched again; she believes Jake put the good luck to better use than she ever did. She goes to the railway station, where Jake finds her and tries to convince her that she was all he wanted since the party. They then kiss each other relentlessly, causing their luck to swap so quickly that they no longer know who has the good luck and who has the bad. When they see Jakes little cousin Katy and both of them kiss her, one on each cheek, at the same time, she gets all the luck and wins a scratch card lottery of twenty-five dollars. Later, when they find a quarter, they decide that they still have some luck left. But only seconds later, they are drenched by a broken water line.

==Cast==
* Lindsay Lohan as Ashley Albright
* Chris Pine as Jake Hardin
* Samaire Armstrong as Maggie
* Bree Turner as Dana
* Faizon Love as Damon Phillips
* Missi Pyle as Peggy Braden
* Makenzie Vega as Katy Applee
* Carlos Ponce as Antonio
* Tovah Feldshuh as Madame Z
* Jaqueline Fleming as Tiffany
* McFly as Themselves
* Dane Rhodes as Mac
* Mikki Val as Tough Jailbird
* Chris Carmack as David Pennington (uncredited)

==Production notes==
Principal photography took place in New Orleans, Louisiana before Hurricane Katrina hit the area. In March 2005, filming continued in New York City.   

==Reception==
The film received mostly negative reviews from critics, with Rotten Tomatoes giving it a 13% rating with a very long consensus: "Just My Luck is a screwball rom-com that fails to take advantage of Lindsay Lohans considerable charm. The film (along with Robert Altmans A Prairie Home Companion) marks the end of Lohans teen queen phase as she begins playing characters that are older, but not necessarily wiser. Lohans character is supposedly cursed with bad luck, though the movie confuses misfortune with plain stupidity, leading Lohan into a long series of silly and senseless physical gags." 

Lohan earned a Razzie Award nomination as Worst Actress for her performance in the film.

===Box office===
Just My Luck opened at #4 at the North American box office with $5,692,285 in its opening weekend, May 14, 2006. In the US, it grossed $17,324,744 after showing for 12 weeks. The film has grossed over $38 million worldwide.

==Soundtrack==
{{Infobox album 
| Name       = Just My Luck
| Type       = soundtrack
| Artist     = McFly
| Cover      = Just My Luck Mcfly.jpg
| Released   = June 11, 2006
| Genre      = Pop rock
| Length     = 42:53 Island
| Jason Perry, Steve Power
| Last album = Motion in the Ocean (2006)
| This album = Just My Luck (2006)
| Next album = All the Greatest Hits  (2007)
}}

The album is made up entirely of songs from McFlys first two albums, Room on the 3rd Floor and Wonderland (McFly album)|Wonderland, with the exception of the brand new track "Just My Luck", which was recorded especially for the film. Though released as the films soundtrack, a selection of the songs featured on the album were not in the film. Reworked versions of "5 Colours in Her Hair", "Ive Got You" and "Unsaid Things", as well as the censored Single version of "I Wanna Hold You" appear on the album instead of the original versions. The European version of the album slightly muddles up the tracklisting, and adds the hidden track "Get Over You" in the pre-gap.

 
===Track listing===
{{tracklist
| writing_credits = yes
| title1   = I Wanna Hold You
| note1    = single version
| writer1  = Tom Fletcher • Danny Jones • Dougie Poynter
| length1  = 2:59
| title2   = Ive Got You
| note2    = U.S. version
| writer2  = Fletcher • Jones • Graham Gouldman
| length2  = 3:20
| title3   = Obviously
| writer3  = Fletcher • Jones • James Bourne
| length3  = 3:18 Ultraviolet
| writer4  = Fletcher • Jones 
| length4  = 3:56  Five Colours in her Hair
| note5    = U.S. version
| writer5  = Fletcher • Jones • Bourne
| length5  = 3:00
| title6   = Too Close for Comfort
| writer6  = Fletcher • Jones • Poynter
| length6  = 4:37  All About You
| writer7  = Fletcher  
| length7  = 3:06  That Girl
| writer8  = Fletcher • Jones • Bourne
| length8  = 3:17 
| title9   = Unsaid Things
| note9    = U.S. version
| writer9  = Fletcher • Jones • Bourne • Poynter • Harry Judd
| length9  = 3:45
| title10  = Ill Be OK
| writer10 = Fletcher • Jones • Poynter
| length10 = 3:24
| title11  = Just My Luck
| writer11 = Fletcher • Jones 
| length11 = 3:15 
| title12  = Memory Lane
| writer12 = Fletcher • Bourne 
| length12 = 4:40 
}}
{{tracklist
| writing_credits = yes
| headline = Alternate version
| collapsed = yes Five Colours in her Hair
| note1    = U.S. version
| writer1  = Fletcher • Jones • Bourne
| length1  = 3:00
| title2   = Obviously
| writer2  = Fletcher • Jones • Bourne
| length2  = 3:18
| title3   = I Wanna Hold You
| note3    = single version
| writer3  = Fletcher • Jones • Poynter
| length3  = 2:59
| title4   = Ive Got You
| note4    = U.S. version
| writer4  = Fletcher • Jones • Gouldman
| length4  = 3:20 Ultraviolet
| writer5  = Fletcher • Jones 
| length5  = 3:56  All About You
| writer6  = Fletcher • Jones 
| length6  = 3:06
| title7   = Too Close for Comfort
| writer7  = Fletcher • Jones • Poynter
| length7  = 4:37  That Girl
| writer8  = Fletcher • Jones • Bourne
| length8  = 3:17 
| title9   = Unsaid Things
| note9    = U.S. version
| writer9  = Fletcher • Jones • Bourne • Poynter • Judd
| length9  = 3:45
| title10  = Ill Be OK
| writer10 = Fletcher • Jones • Poynter
| length10 = 3:24
| title11  = Just My Luck
| writer11 = Fletcher • Jones 
| length11 = 3:15 
| title12  = Memory Lane
| writer12 = Fletcher • Bourne 
| length12 = 4:40 
}}

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 