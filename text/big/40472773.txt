+1 (film)
{{Infobox film
| name           = +1
| image          = File:+1MoviePoster2013.jpg
| border         = Official movie poster
| alt            = Plus One
| caption        = 
| director       = Dennis Iliadis
| producer       = Guy Botham
| screenplay     = Bill Gullo
| story          = Dennis Iliadis
| starring       = Ashley Hinshaw Rhys Wakefield Natalie Hall
| music          = Nathan Larson
| cinematography = Mihai Mălaimare, Jr.
| editing        = Yorgos Mavropsaridis
| studio         = Process Films Process Productions
| distributor    = IFC Films
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} VOD on September 20, 2013.  For the VOD release Iliadis worked on fine-tuning elements of the film, as he felt that the SXSW release was too "rushed".   

== Plot ==
 
While on the phone with his girlfriend Jill, who has moved away to attend college, David prompts her to explicitly wish that he were there to see her compete in a fencing tournament, only to surprise her with an unplanned visit. After the competition, David approaches Jills similarly attired opponent from behind and flirts with her, thinking she is his girlfriend. Intrigued, she kisses David, and Jill storms off angrily after walking in on them. Later, David meets with his friend Teddy, a sex-obsessed student who tells him that Jill will attend a house party later. Hoping that she will speak with him in person, David decides to attend the party, too.

A meteor lands near the site of the party, and electrical arcs cause a momentary blackout. The wild revelers do not notice brief, anomalous phenomena that occur during the blackout, such as a mirror image out of sync, but a drug dealers girlfriend outside the party becomes spooked. As the raucous party proceeds, David searches the house for Jill, and Teddy attempts to charm Melanie. To Teddys surprise, Melanie invites him to join her upstairs in ten minutes. Meanwhile, David spies on Jill as she flirts with a friend, and Allison, an outsider, unsuccessfully attempts to fit in. David attempts to apologize to Jill, but she becomes more angry with his fumbled apology and says that he makes her feel replaceable. The party moves outside, and the house empties except for David, Teddy, Allison, and Melanie.

When Teddy joins Melanie upstairs, he finds her naked on a bed. They proceed to have sex, and she steps into the shower afterward. During a second blackout, a duplicate of Melanie appears on the bed, surprised to find Teddy in the room. When the original Melanie exits the shower, the two Melanies come face-to-face, and Teddy flees the room in a panic. David confirms that the house has filled with duplicates of the party-goers, who repeat the actions their originals took ten minutes ago. Outside, the drug dealer and his duplicate get into a violent confrontation, and David watches as one of them murders the other. Worried that the duplicates may be hostile, the originals attempt to hide. Eventually, the duplicates disappear.

With each blackout, the duplicates momentarily reappear and reenact increasingly more recent actions. David becomes convinced that he can save his relationship if he crafts a better apology to Jills duplicate, Allison befriends her duplicate, and Teddy accidentally ruins the rendez-vous between his and Melanies duplicates. Teddy warns the others that his duplicate will now become belligerent, and he convinces the party-goers, most of whom are still skeptical, to hide from their duplicates in a pool house. When the duplicates reappear, David knocks his own duplicate unconscious and charms Jills duplicate, and Teddys duplicate attempts to convince the skeptical crowd of duplicates to attack the originals.

Worried about their safety, several of the originals sneak out and murder their duplicates, which turns the enraged duplicates hostile. The originals retreat back to the pool house, and the duplicates lay siege. The next blackout causes the duplicates to appear inside the pool house. The crowded room erupts in violence, and several people die, including Teddy. David tracks down the original Jill, who had wandered back to the house, and he murders her so that he can be with her duplicate. Allison seduces her duplicate, and they share a kiss. The final blackout causes the duplicates and originals to merge. David and Jill leave together and make their way to the pool house and have a make-out session. The confused party-goers stagger out of the pool house, and they begin to disperse. Two meteors, like the original, are shown passing through the sky and appear to be leaving upwards away.

== Cast ==
* Rhys Wakefield as David
* Ashley Hinshaw as Jill
* Natalie Hall as Melanie
* Rhoda Griffis as Mrs. Howard
* Logan Miller as Teddy
* Marla Malcolm as Heather
* Megan Hayes as Bonnie
* Hannah Kasulka as Opponent
* Colleen Dengel as Allison
* Suzanne Dengel as Allison Chrissy Chambers as Kitty
* Max Calder as Jason

==Themes== parallel dimensions, time dilation, and the doppelgänger paranormal phenomenon.

When one of the originals say "From the book of Talmud, to meet ones self is to meet God.", this is a reference in some myths, the doppelgänger is a version of the Ankou, a personification of death; in a tradition of the Talmud, to meet oneself means to meet God.  At the end of the movie, the originals fight only with their duplicates, and its not sure if the doppelgängers have killed their originals.

== Reception ==
Rotten Tomatoes reports that 67% of 12 surveyed critics gave the film a positive review; the average rating was 6.10/10.  Metacritic rated it 60/100 based on six reviews.  Jeannette Catsoulis of The New York Times made it a NYT Critics Pick and called it "a fleet and frenzied sci-fi tale with more on its mind than alien gate-crashers."  Chuck Bowen of Slant Magazine gave the film three out of four stars, commenting that Iliadis did a good job of showing the "existential despair" of narcissism and detachment.  Scott Weinberg of Fearnet gave a more mixed review, saying that the film was a "decent, uneven, well-made spin on a very standard horror story".  In a mixed review, Geoff Berkshire of Variety (magazine)|Variety called it a "trippy oddity" with "a tantalizing visual puzzle that demands full attention, even as the flavorless characters and largely so-so performances risk audience indifference." Berkshire states the films "overriding misogyny" may be intentional satire. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 