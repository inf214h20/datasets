Sipayi Ramu
{{Infobox film
| name           = Sipayi Ramu
| image          = Sipayi Ramu poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Y. R. Swamy
| producer       = 
| writer         = Nuggehalli Pankaja
| screenplay     = Basumani
| story          = 
| based on       =  
| narrator       =  Rajkumar  Leelavathi   Aarathi   K. S. Ashwath   Shivaram   Vajramuni   Thoogudeepa Srinivas
| music          = Upendra Kumar
| cinematography = R. Chittibabu
| editing        = P. Bhakthavathsalam
| studio         = Sri Bhagavathi Productions
| distributor    = 
| released       =  
| runtime        = 161 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Third Best Film and Best Actress (Leelavathi). 
 
== Cast == Rajkumar
* Leelavathi
* Aarathi
* K. S. Ashwath
* Shivaram
* Vajramuni
* Thoogudeepa Srinivas
* Shakthi Prasad
* Ashwath Narayana
* B. Jaya (actress)|B. Jaya

==Soundtrack==
The music of the film was composed by Upendra Kumar with lyrics of the soundtrack penned by R. N. Jayagopal.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s)
|-
| 1 || Thangaali Sangeetha Haadide || P. Susheela
|-
| 2 || Kokorekoko Kokorekoko Ko || S. P. Balasubrahmanyam, L. R. Eswari
|-
| 3 || Kanna Notadalle Nee Kaadabeda || P. Susheela
|-
| 4 || Nidireyu Sadaa Eko Doora || P. B. Sreenivas, P. Susheela
|-
| 5 || Ellige Payana Yaavudo Daari || P. B. Sreenivas

|}

== References ==
 

== External links ==
*  

 

 
 
 

 