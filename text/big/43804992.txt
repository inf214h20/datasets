Lost Soul (2014 film)
{{Infobox film
| name           = Lost Soul: The Doomed Journey of Richard Stanley’s Island of Dr. Moreau
| image          = File:Lost Soul The Doomed Journey of Richard Stanley’s Island of Dr. Moreau.jpg
| alt            =
| caption        = 
| film name      = 
| director       = David Gregory
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Fairuza Balk Hugh Dickson Oli Dickson
| narrator       = 
| music          = Mark Raskin
| cinematography = Jim Kunz
| editing        = Douglas Buck
| studio         = Severin Films
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} Richard Stanleys The Island of Dr. Moreau. 

==Synopsis==
In Lost Soul Gregory looks at the filming of The Island of Dr. Moreau, specifically the period during which director Richard Stanley spent on the project. Stanley was brought on to the project early but was fired only a few days after principal photography began and was replaced by John Frankenheimer. The documentary looks into Stanleys vision on for the film, as Stanley had spent years working on the movies script and had intended for Bruce Willis to star as Montgomery, a role that was later given to Val Kilmer- a move that Stanley viewed as a mistake. Lost Soul features interviews with several people involved with the movies production and focuses on various aspects of the film, including multiple changes to the script and reports that Kilmer was difficult to deal with on set.   

==Cast==
*Fairuza Balk
*Hugh Dickson
*Oli Dickson
*Peter Elliott
*Bruce Fuller
*Michael Gingold
*David Grasso Jr.
*Marco Hofschneider
*David Hudson
*Graham Humphreys
*Kier-La Janisse
*Paul Katte
*Fiona Mahl
*Rob Morrow
*Emile Nicolaou
*Edward R. Pressman
*James Sbardellati
*Robert Shaye Richard Stanley Tim Sullivan

==Reception==
Critical reception for Lost Soul has been positive and the film has received praise from Nerdly and Shock Till You Drop,  with the latter describing it as a "must see".  IGN rated Lost Soul favorably and summed it up as "a no-holds-barred making of documentary that proves that fact really is stranger than fiction." 

==Release==
The film premiered as part of the London FrightFest Film Festival on 24 August 2014.  The film screened at the IFIs Horrorthon 2014 in Dublin, Ireland   In the United States premiered in a limited screening in February 2015,  with a start in Austin, Texas on 25 February and an end on 30 March in Scottsdale, Arizona.  It is also part of Phoenix International Horror & Sci-Fi Film Festival and will screen on 30 March 2015 in the Harkins Scottsdale 101 Theatre in Arizona. 

==References==
 

==External links==
*  

 
 
 
 