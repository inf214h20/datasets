Betty Boop and the Little King
{{Infobox Hollywood cartoon|
| cartoon_name = Betty Boop and the Little King
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist = Otto Soglow (as O. Soglow)
| animator = Hicks Lockey Myron Waldman 
| voice_actor = Mae Questel 
| musician = 
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = January 31, 1936
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

Betty Boop and the Little King is a 1936 Fleischer Studio animated short film, starring Betty Boop and featuring Otto Soglows Little King.

==Plot==

A special opera performance is held for the Little King and his queen, but the diminutive monarch is soon bored by the music.  He sneaks out in search of some new entertainment, and spots a sign for Betty Boop at the local vaudeville theatre.  After some difficulties getting a pretzel from a vendor, the curtain comes up on Bettys Wild West show.  Betty performs several tricks with her horse, entrancing the monarch.  He joins Betty on stage for a song and dance number, just in time to be caught by the angry queen.  The monarchs leave in the royal carriage, with Betty (hiding on the fender) holding the Little Kings hand.

==Production notes==
The Little King had appeared in several cartoons produced by Van Beuren Studios (1933–34).  In those cartoons he was silent (as he had been in his comic strip).  This short is the only one in which the Little King speaks.

==External links==
* 
 
 

 
 
 
 
 
 
 
 


 