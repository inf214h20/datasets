Pelli Pandiri
{{Infobox film
| name           = Pelli Pandiri
| image          =
| caption        =
| writer         = Thotapalli Madhu  
| story          = Umakanth
| screenplay     = Kodi Ramakrishna
| producer       = G.Aswartha Narayana Babu M.C.Shekar S.Ramesh Babu
| director       = Kodi Ramakrishna Raasi Babloo Prithuvi Raj
| music          = Vandemataram Srinivas
| cinematography = M. Mohan Chand
| editing        = Tata Suresh
| studio         = SRS Art Movies
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Prithuvi Raj in the lead roles and music composed by Vandemataram Srinivas.       The film was a remake of Kannada film Anuraga Sangama.

==Cast==
 
*Jagapati Babu as Govind Raasi as Kasthuri Prithuvi Raj as Prakash
*Sarath Babu as Inspector Rama Rao Suhasini as Dr. Prabhavathi
*Costume Krishna Sudhakar as Goldman Mallikarjuna Rao as Bhadram
*Raghunath Reddy as Notla Raghunath Chowdary
*Pokula Narasimha Rao as Watchman
*Garimalla Viswaswara Rao
*Kallu Chidambaram as Photographer
*Mithai Chitti
*Radha Prashanthi
*Tejaswini
*Master Anand Vardhan as Young Prakash
*Master Srinivas as Young Govind
 

==Soundtrack==
{{Infobox album
| Name        = Pelli Pandiri
| Tagline     = 
| Type        = film
| Artist      = Vandemataram Srinivas
| Cover       = 
| Released    = 1997
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:04
| Label       = Supreme Music
| Producer    = Vandemataram Srinivas 
| Reviews     =
| Last album  = Priyamaina Srivaaru   (1997)  
| This album  = Pelli Pandiri   (1998)
| Next album  = Aaha   (1998)
}}

The music for the film was composed by Vandemataram Srinivas released on Supreme Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:04
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Nestaam Iddari Lokam
| lyrics1 = Guru Charan SP Balu, Chitra
| length1 = 5:15

| title2  = Choodachakkani Jinkapillara
| lyrics2 = Bhuvanachandra
| extra2  = SP Balu,Chitra
| length2 = 4:59

| title3  = Anaanaga Oka Nendu Sirivennela Sitarama Sastry
| extra3  = SP Balu,Chitra
| length3 = 6:09

| title4  = Dost Mera Dost 
| lyrics4 = Bhuvanachandra Mano
| length4 = 5:45

| title5  = Ede Manchi Roju
| lyrics5 = Sirivennela Sitarama Sastry 
| extra5  = SP Balu,Chitra
| length5 = 4:47
}}
   

==References==
 

 
 
 
 
 
 