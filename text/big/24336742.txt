Son of Ingagi
{{Infobox film
| name           = Son of Ingagi
| image          = SonOfIngagi.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Richard C. Kahn
| producer       = Richard Kahn Spencer Williams
| narrator       = 
| starring       = Zack Williams Spencer Williams
| music          =  Spencer Williams Herman Schopp Laura Bowman
| editing        = Dan Milner
| studio         = 
| distributor    = 
| released       =  
| runtime        = 70 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} American film science fiction-horror missing link monster named NGina as well as African gold. When NGina drinks the doctors potion, it puts him into a rage that makes him murder Dr.Jackson. The Lindsay family inherits Jacksons house where they soon find the presence of the monster. Son of Ingagi was written by Spencer Williams based on his own short story House of Horror. The production company was impressed with Williams script and allowed him to direct and write his own feature film The Blood of Jesus in 1941.

==Plot== Spencer Williams) missing link monster who she has taken from her previous trip to Africa. Jacksons brother leaves terrified. At the Lindsays wedding, an explosion erupts, which leads most party-goers to investigate with only Eleanor staying at home. Eleanor is then visited by Dr.Jackson, who explains that she was in love with Eleanors father and that she had fled to Africa later after he married Eleanors mother.

Later in her laboratory, Jackson works on a potion for the benefit of human race. NGina takes the potion and drinks it which causes NGina to go on a rampage which kills Jackson. The Lindsays later find that they are beneficiaries in Helens will, and due to her sudden death they are initially suspected of murdering her. Later, the Lindsays are acquitted of the crime, and move into Helens manor.

Eleanor soon discovers that food is mysteriously disappearing. Bradshaw, the executor of the will, comes to urge them to sell the house, and while rummaging through the desk, he carelessly rings the gong, which summons NGina from the hiding place in the cellar. NGina reacts to the stranger and kills Bradshaw. Detective Nelson is assigned to solve the mystery of the house and moves into the home. Zeno breaks into the couples bedroom, but escapes when Eleanor accidentally hits Bob instead of Zeno.

After seeing NGina emerge from the basement, Zeno follows NGinas path to seize Helens gold. Zeno finds the gold but is caught by NGina who drags Zeno upstairs for Nelson to find. Eleanor spots NGina and faints at the sight the creature. NGina then carries Eleanor downstairs. When Nelson finds Zenos body he awakens Bob who searches for Eleanor. NGina accidentally starts a fire, and Eleanors screams draw Bob and Nelson into the basement where Nelson fails to arrest NGina. Bob, however, succeeds in locking the beast in a cell while the house and NGina burn. Nelson emerges from the bushes outside with the bags of gold while Bob and Eleanor escape unharmed.

==Cast==
* Zack Williams as NGina
* Laura Bowman as Dr. Jackson
* Alfred Grant as Robert Lindsay
* Daisy Bufford as Eleanor Lindsay
* Arthur Ray as Zeno Jackson Spencer Williams as Nelson
* Earl J. Morris as Bradshaw
* Jesse Graves as Chief of Detectives
* The Toppers as themselves

==Production==
 
Spencer Williams screenplay for Son of Ingagi was based on his own story titled House of Horror.  Alfred N. Sack, whose Dallas, Texas-based company Sack Amusement Enterprises produced and distributed race films, was impressed with Spencer Williams screenplay for Son of Ingagi and offered him the opportunity to write and direct a feature film.     Williams resulting film was The Blood of Jesus (1941) while Son of Ingagi was directed by the white American director Richard Kahn.  At that time, the only African American filmmaker was the self-financed Oscar Micheaux.   

Cynthia Erb, author of Tracking King Kong: A Hollywood Icon in World Culture suggests that the reason the films monster does not match the title in the film was possibly for box office reasons, as to have it relate to the popular success of the exploitation film Ingagi (1930). Erb 2009, p. 193  Both Richard Gilliam of Allmovie and Erb note that NGina was probably influenced by Boris Karloffs character in Frankenstein (1931 film)|Frankenstein with NGinas outbursts of violence and tendency to show emotions of suffering and being mournful.    Erb 2009, p. 195 

==Reception==
Richard Gilliam of the online film database Allmovie wrote that the film was "One of the more interesting low-budget films of the early 40s" and "Despite what its low-budget origin and lurid subject matter might indicate, Son of Ingagi is both well-written and well-acted. Its no undiscovered classic, but its also not the bottom-of-the-barrel trash that some references sources claim that it is." 

==Notes==
 

==References==
*{{cite book
 | last= Erb
 | first= Cynthia Marie
 | title= Tracking King Kong: A Hollywood Icon in World Culture
 |publisher= Wayne State University Press
 |year= 2009
 |url=http://books.google.ca/books?id=glp97Y_eVSIC
 |isbn= 0-8143-3430-X
 |accessdate=14 September 2009
}}
*{{cite book
 | last= Balio
 | first= Tino
 | title= Grand design: Hollywood as a modern business enterprise, 1930-1939
 |publisher= University of California Press
 |year= 1996
 |url=http://books.google.ca/books?id=_J9HTLOI08wC
 |isbn= 0-520-20334-8
 |accessdate=15 September 2009
}}
*{{cite book
 | last= Weisenfeld
 | first= Judith
 | title= Hollywood be thy name: African American religion in American film, 1929-1949
 |publisher= University of California Press
 |year= 2007
 |url=http://books.google.ca/books?id=QNV9LeA4QGcC
 |isbn= 0-520-22774-3
 |accessdate=15 September 2009
}}
*{{cite book
 | last= Moon
 | first= Spencer
 | title= Reel Black talk: a sourcebook of 50 American filmmakers
 |publisher= Greenwood Publishing Group
 |year= 1997
 |url=http://books.google.ca/books?id=0YQvCbc5pBcC
 |isbn=0-313-29830-0
 |accessdate=15 September 2009
}}

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 