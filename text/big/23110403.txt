Justice Viswanathan
{{Infobox film
| name = Justice Viswanathan
| image = Justice-vishwanathan.jpg
| caption = Official DVD Box Cover
| director = G. R. Nathan Modern Theatres Ltd
| writer = A. L. Narayanan
| narrator = Ravichandran Major Sundarrajan   C.I.D.Sakunthala  Manimaala   Thengai Srinivasan Manohar Venniradai Moorthy
| music = Vedha
| cinematography = G. R. Nathan
| editing = L. Balu
| distributor =
| released = 1971
| runtime = 143 mins
| country = India Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1971 Indian Tamil language Drama film directed by G. R. Nathan starring V. Ravichandran|Ravichandran, Major Sundarrajan. The movie is a remake of 1969 Hindi movie Do Bhai(1969) starring Ashok Kumar and Jeetendra.


== Plot ==
Justice Viswanathan is a popular justice who goes on a revenge hunt for the death of Manimaala. The turn of events that follows forms the rest of the story.

== Soundtrack ==
The songs were written by Kannadasan.

* Ithu neerodu - Seerkazhi Govindarajan
* Silai Seyya - T. M. Soundararajan, P. Susheela
* Kann vazhiye Kann vazhiye
 - T. M. Soundararajan, P. Susheela
 

==References==
 

 
 
 
 


 