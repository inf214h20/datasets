Little Voice (film)
{{Infobox film
| name           = Little Voice
| image          = Little Voice.jpg
| image_size     = 
| caption        = DVD cover
| alt            =  
| director       = Mark Herman
| producer       = Elizabeth Karlsen 
| writer         = Mark Herman
| based on       =  
| starring       = Jane Horrocks  Brenda Blethyn  Michael Caine  Ewan McGregor  Jim Broadbent John Altman Andy Collins
| editing        = Michael Ellis
| distributor    = Miramax Films
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $4,611,784 
}}
Little Voice is a 1998 British musical written and directed by Mark Herman and made in Scarborough, North Yorkshire. The screenplay is based on Jim Cartwright’s play The Rise and Fall of Little Voice.

== Plot ==
Laura Hoff, who lives in a working-class home in Scarborough, is known as LV for her shyness. She flees reality by mimicking voices like Édith Piaf’s, Judy Garland’s, and Shirley Bassey’s; her love of songs is her only source of strength. Her mother Mari, a profligate woman with countless affairs, jilts a man when her passion wanes.

Billy, an installer who mends their phone, approaches LV by sending her pamphlets. Things improve when Mari is seeing Ray: He hears the girl sing, spots her gift and vows to make her a star, while Mari, who dislikes singing, still doubts her child. When LV is to sing at a nightclub, she visions her father to help perform well. 

Ray’s futile attempts to goad LV dash him. Mari, who still scorns her child, prods her against her will. When wrongly accused of arson, LV responds by blaming Mari for her father’s death and her meek nature with her domineering attitude.

Mari is left by everyone; Ray is facing his debt-collectors, and LV is saved by Billy.

== Cast ==
* Brenda Blethyn as Mari
* Jane Horrocks as LV
* Michael Caine as Ray Say, who manages third-rate acts
* Ewan McGregor as Billy
* Jim Broadbent as Mr. Boo, Ray Say’s manager Philip Jackson as George
* Annette Badland as Sadie

== Soundtrack   ==
The following songs are performed by Horrocks:
* "The Man that Got Away" by Harold Arlen and Ira Gershwin
* "Lover Man (Oh Where Can You Be)" by Jimmy Davis, Jimmy Sherman, and Roger Ramirez
* "Over the Rainbow" by Harold Arlen and E.Y. Harburg
* "Chicago" by Fred Fisher
* "Big Spender" by Cy Coleman and Dorothy Fields
* "I Wanna Be Loved By You" by Harry Ruby, Herbert Stothart, and Bert Kalmar
* "Sing As We Go" by Harry Parr Davies Falling in Love Again" by Frederick Hollander and Samuel Lerner Get Happy" by Harold Arlen and Ted Koehler

== Critical reception ==
Rotten Tomatoes gives the film an 79% rating based on 48 reviews. 

Janet Maslin wrote in her New York Times review, “Horrocks’s phenomenal mimicry of musical grande dames from Marlene Dietrich to Marilyn Monroe, lavishing special loving care on Judy Garland, makes a splendid centerpiece for the otherwise more ordinary film built around it.” 

Roger Ebert of the Chicago Sun-Times felt the story was “amusing but uneven” and that the film “seems to have all the pieces in place for another one of those whimsical, comic British slices of life. But the movie doesn’t quite deliver the way we think it will. One problem is that the Michael Caine character, sympathetic and funny in the opening and middle scenes, turns mean at the end for no good reason. Another is that the romance, and a manufactured crisis, distract from the true climax of the movie. That would be Jane Horrocks’ vocal performance . . . she is amazing. Absolutely fabulous.” 

In Variety (magazine)|Variety, Derek Elley called the film “a small picture with a big heart”, adding, “The film has almost everything going for it, with the exceptions of a somewhat lopsided structure in which the climax comes two-thirds of the way through and a romantic subplot that plays like an afterthought. Nevertheless, smooth direction by Mark Herman and juicy performances by a host of Brit character actors . . . ensure an entertaining ride . . . Horrocks, whose combo of gamin physique and big vocal talent make the title role seem unthinkable for any other actress, is a revelation, handling moments of solo emotion and onstage strutting with equal, moving panache.” 

== Awards and nominations ==
* Academy Award for Best Supporting Actress (Brenda Blethyn, nominee)
* Golden Globe Award for Best Actor - Motion Picture Musical or Comedy (Michael Caine, winner)
* Golden Globe Award for Best Actress - Motion Picture Musical or Comedy (Jane Horrocks, nominee)
* Golden Globe Award for Best Supporting Actress - Motion Picture (Brenda Blethyn, nominee) BAFTA Alexander Korda Award for Best British Film (nominee)
* BAFTA Award for Best Actor in a Leading Role (Michael Caine, nominee)
* BAFTA Award for Best Actress in a Leading Role (Jane Horrocks, nominee)
* BAFTA Award for Best Actress in a Supporting Role (Brenda Blethyn, nominee)
* BAFTA Award for Best Adapted Screenplay (nominee)
* BAFTA Award for Best Sound (nominee)
* Satellite Award for Best Picture - Musical or Comedy (nominee)
* Satellite Award for Best Adapted Screenplay (nominee)
* Satellite Award for Best Actor - Motion Picture Musical or Comedy (Michael Caine, nominee)
* Satellite Award for Best Actress - Motion Picture Musical or Comedy (Jane Horrocks, nominee) Satellite Award for Best Supporting Actress - Motion Picture Musical or Comedy (Brenda Blethyn, nominee)
* Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture (nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Leading Role - Motion Picture (Jane Horrocks, nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role - Motion Picture (Brenda Blethyn, nominee)
* London Film Critics Circle Award for British Supporting Actor of the Year (Michael Caine, winner) British Independent Film Award for Best Actor (Michael Caine, nominee)
* British Independent Film Award for Best Actress (Jane Horrocks, nominee)

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 