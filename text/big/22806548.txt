And Quiet Flows the Don (film)
 
{{Infobox film
| name           = And Quiet Flows the Don
| image          = QuietDon1958.JPG
| image_size     =
| caption        = The poster in English Sergei Gerasimov Sergei Gerasimov Mikhail Sholokhov (novel)
| narrator       = 
| starring       = Pyotr Glebov Elina Bystritskaya Zinaida Kiriyenko
| music          = Yuri Levitin
| cinematography = Vladimir Rapoport
| editing        = Nina Vasilyeva
| studio         = Gorky Film Studio
| released       = 26 October 1957 (parts 1 & 2)   Retrieved 2011-11-04  30 April 1958 (part 3)
| runtime        =  330 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}} 1958 cinema Soviet film Sergei Gerasimov the novel Mikhail Sholokhov. The first two parts of the film were released in October 1957 and the final third part in 1958. In 1958 the film won Crystal Globe award at the Karlovy Vary International Film Festival and the Best Picture Award at the All-Union Film Festival.

==Cast==
* Pyotr Glebov as Grigori "Grisha" Melekhov
* Elina Bystritskaya as Aksiniya
* Zinaida Kiriyenko as Natalya

== See also ==
*List of longest films by running time

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 