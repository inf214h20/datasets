Bird People (film)
 
{{Infobox film
| name           = Bird People
| image          = Bird People poster.jpg
| caption        = Film poster
| director       = Pascale Ferran
| producer       = Denis Freyd
| writer         = Guillaume Bréaud Pascale Ferran
| starring       = Josh Charles Anaïs Demoustier 
| music          = Béatrice Thiriet 
| cinematography = Julien Hirsch 
| editing        = Mathilde Muyard 
| studio         = Archipel 35 France 2 Cinéma Titre et Structure Production
| distributor    = Diaphana Distribution
| released       =  
| runtime        = 128 minutes
| country        = France 
| language       = French English
| budget         = € 6.91 million  
}}

Bird People is a 2014 French drama film directed by Pascale Ferran. It was screened in the Un Certain Regard section at the 2014 Cannes Film Festival.    It was also screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.   

==Plot==
Audrey, a student, works as a hotel maid. In the same hotel Gary Newman, an American business man visiting Paris deliberately misses his flight and decides to quit his job, divorce his wife and remain in Paris.

==Cast==
* Josh Charles as Gary Newman
* Anaïs Demoustier as Audrey Camuzet
* Roschdy Zem as Simon
* Camélia Jordana as Leila Geoffrey Cantor as Allan 
* Clark Johnson as McCullan
* Akela Sari as Mme Baccar
* Anne Azoulay as Melle Lhomond
* Manuel Vallade as Boris
* Genevieve Adams as Katlyn
* Taklyt Vongdara as Akira 
* Radha Mitchell as Elisabeth Newman
* Catherine Ferran as Nuala
* Hippolyte Girardot as Vengers
* Mathieu Amalric as The Narrator
* Philippe Duclos as Audrey’s father (voice)
* Kate Moran as Gary’s sister (voice)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 