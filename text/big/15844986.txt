Day and Night (2004 Chinese film)
{{Infobox film
| name           = Day and Night
| image          = Day_and_Night_Poster.jpg
| image_size     = 
| caption        = Wang Chao Fang Li Sylvain Bursztejn
| writer         = Wang Chao
| starring       = 
| music          = Qin Wenchen
| cinematography = Yihuhewula
| editing        = Zhou Xinxia
| distributor    = Worldwide Sales:  
| released       =  
| runtime        = 95 minutes
| language       = Mandarin
| budget         = 
| country        = China
}} Chinese film, Wang Chao. Luxury Car in 2006. The film is a Chinese-French co-production between Fang Lis Laurel Films and Sylvain Busztejns Rosem Pictures in association with the China Film Fourth Group and Arte France Cinema; Laurel and Rosem would later reunite to produce Luxury Car. 

==Synopsis==
Day and Night tells the story of a miner, Li Guangsheng (Liu Lei), in a fictional northern Chinese city who carries on an illicit affair with the wife of his friend and father figure (played by Orphans Sun Guilin). When his friend is killed in a cave-in, the miner is wracked with guilt and takes it upon himself to end the affair, rebuild the mine, and help his friends mentally disabled son find a wife.

==Production== Wuchuan County, Inner Mongolia, near the provincial capital of Hohhot. 

== Cast ==
* Liu Lei as Li Guangsheng, the main character, a miner in northern China.
* Sun Guilin as Zhongmin, Li Guangshengs friend and father-figure.
* Wang Lan
* Xiao Ming as A-Fu, Zhongmins son. Wang Zheng
* Zhang Guangjie

== Reception == Pusan  Hong Kong.  However, the film would ultimately fail to garner the same level of acclaim that Wangs The Orphan of Anyang was able to, though it was more technically polished than the decidedly low-budget Orphan. Critics such as Derek Elley derided the film as a "curiously lifeless item that falls short of its seemingly mythic intent."    As such, it is generally considered a less successful film than its predecessor. 
 2004 Nantes Three Continents Festival. 

==References ==
 

== External links ==
*  
*  
*   at the Chinese Movie Database

 

 
 
 
 
 
 
 