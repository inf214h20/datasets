Marriage Story
{{Infobox film name           = Marriage Story image          = Marriage Story.jpg caption        = Poster for Marriage Story (1992)
| film name      = {{Film name hangul         =   hanja          =  이야기 rr             = Gyeolhoniyagi mr             = Kyŏrhoniyagi}} director       = Kim Eui-suk  producer       = Park Sang-in Oh Jung-wan writer         = Park Heon-su starring       = Choi Min-soo Shim Hye-jin music          = Song Byeong-joon cinematography = Koo Joong-mo editing        = Park Soon-duk Park Gok-ji distributor    = Shin Cine Communications Ik Young Films Co., Ltd. released       =   runtime        =  country        = South Korea language       = Korean budget         =  gross          = 
}}
Marriage Story ( ) is a 1992 South Korean film. It was the fourth most highly attended Korean film between 1990 and 1995.   

==Synopsis==
A social-issue comedy about the romantic lives of a middle-class couple. Their relationship is followed through their wedding, break-up and reconciliation. 

==Cast==
* Choi Min-soo... Kim Tae-kyu 
* Shim Hye-jin... Choe Ji-hae
* Lee Hee-do... Yun Il-jung
* Kim Hui-ryeong... Oh Hyang-suk
* Kim Seong-su... PD Han
* Dokgo Young-jae... Park Chang-su
* Yang Taek-jo... Cha-jang
* Joo Ho-sung... Shin Seon-bae
* Yun Mun-sik... Jjik-sae
* Kim Ki-hyeon... Columbo

==Notes==
 

==Bibliography==
*  
*  
*  

 
 
 
 


 