Bashing (film)
{{Infobox film
| name           = Bashing
| image          = bashingposter.jpg
| image size     =
| caption        = Original Japanese release poster Masahiro Kobayashi
| producer       = Masahiro Kobayashi Naoko Okamura
| writer         = Masahiro Kobayashi
| narrator       =
| starring       = Fusako Urabe
| music          = Hiroshi Hayashi Koichi Saito
| editing        = Naoki Kaneko
| distributor    = 2005
| runtime        = 82 minutes
| country        = Japan
| language       = Japanese
| budget         =
| preceded by    =
| followed by    =
}} 2005 film Masahiro Kobayashi. It premiered at the 2005 Cannes Film Festival where it was nominated for the Palme dOr.    Bashing went on to win special jury award at the Fajr Film Festival and won grand prize at Tokyo FILMeX.

==Plot==
Yuko Takai and a few other Japanese political activists in the Middle East were kidnapped and used as hostages. Upon returning to Japan, Yuko is mistreated for basically "making ripples in the water;" in other words, for not committing suicide and for making the Japanese look weak. This story is based on the real affairs of the kidnapping of three Japanese political activists by Islamic extreme terrorists in Iraq in April 2004. Yuko Takai is a model of Nahoko Takato, a political activist, who was also harshly criticised by almost all Japanese.

==Cast==
* Fusako Urabe - Yuko Takai
* Nene Otsuka - Noriko Takai, Yukos step mother
* Ryûzô Tanaka - Koji Takai, Yukos father
* Takayuki Katô - Ex-boyfriend
* Kikujirô Honda - Fathers boss
* Teruyuki Kagawa - Hotel manager

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 