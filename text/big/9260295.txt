Marriage with a Fool
 
 
{{Infobox Film
| name           = Marriage with a Fool
| image          = Marriage With A Fool Poster.JPG
| caption        = DVD Cover of Marriage With A Fool
| director       = Patrick Kong (Yip Lim-Sum)
| writer         = Patrick Kong (Yip Lim-Sum)
| starring       = Alex Fong Lik-Sun Stephy Tang Pace Wu Leila Tong Philip Ng Theresa Fu
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} Alex Fong.

==Plot==
 
Ah Wah (Alex Fong Lik-Sun) and Bo (Stephy Tang) are a newly married couple living happily together. One day, Ah Wah was promoting his "Pets Funeral Service" on one of the busy streets in Causeway Bay, and he saw Josephine (Pace Wu) who was his former tutor. They met up again. Josephine was abandoned by her boyfriend, and Ah Wah was trying to help her to get through her hard times. Soon they fell in love.

Soon after, Bo wants to go to Japan to see snow and for a trip. The trip was on a special discount. Ah Wah did not want to (it was too expensive) since Bo has a debt to Ah Wahs parents. Ah Wah suggested to go to Shenzhen, but Bo was too mad. She ran out of the travel agency. It was at that moment that their once "perfect" love started to fade. 

Ah Wah followed Bo home where they had another argument. Ah Wah was chased out to the street, and he did not want to be homeless. Therefore, he went to Josephines house. 

In Josephines house, Ah Wah drank bottles and bottles of beer, while Josephine told Ah Wah how much she loved him. Then they slept with each other.

Bo was furious; she moved to one of her friends house to stay. When she was on her job, as a manager in a karaoke place, she met Philip (Philip Ng). Philip was "stealing" toilet paper &mdash; actually he was doing a study of toilet paper quality. Bo followed him to a restaurant, where she heard Philip explaining his studies over and over. Despite that, Bo and Philip became good friends.

Next, Ah Wahs parent visited the newly married couple. Ah Wah and Bo pretended they were in love, but it was too difficult for them. Finally, they had an argument. And said they want to be divorced. Everyone was shocked.

Once, the couples met in the elevator hall. Ah Wah was with Josephine, while Bo was with Philip. They had a short chat.
 taxi for her. Ah Wah comes along and promises that if there is no way he could provide taxi for Bo, then they will divorce. Ah Wah did not want to end his marriage. He recalled how he had a lot of fun and happy times with Bo. Ah Wah called for a taxi on his cell phone at least four times. Each time, he begged the taxi driver about his story, and how Bo will act if there is no taxi. For the last time, Ah Wah had tears in his eyes, but it was no use.

Bo saw how Ah Wah was serious about their marriage and burst out in tears. The couple reunited.

Bo and Ah Wah went to Japan with a discounted price for their make-up honeymoon. They took pictures and displayed them in their living room. They seemed happy.

However, at the end of the film, the director reveals that Josephine and Ah Wah, and Bo and Philip are still communicating. Josephine text messaged Ah Wah, and Bo wrote a note on a piece of purple tissue paper to Phillip. Ah Wah was afraid that Bo might see him sending SMS (cell phone message), so he deleted after replying to it. Bo was afraid that Ah Wah might see her contacting Philip, thinking theyre together, so she closed the door, and the story ends there.

==Cast==
*Alex Fong Lik-Sun
*Stephy Tang
*Pace Wu
*Theresa Fu
*Vincent Kok
*Philip Ng
*Leila Tong
*Wong Cho-lam

==Music==
The basic soundtrack of the film consists of four songs.

{| class="wikitable"
|-
! Title
! Literal translation
! Performer
! Segment featured in
! Featured in commercial album(s)
|-
| 十分．愛(合唱版)
| Perfect Love
| Alex Fong Lik-Sun, Stephy Tang
| Main Theme
| Stephy Tang - Fantasy, LOVE 06 (album) (compilation)
|-
| 愛你變成恨你
| When love turns bitter
| Kary Ng
| Used in background during Bo and Wahs mutual breakdown, realisation, and eventual reconciliation. Played once more during credits
| With a boy like you - Kary Ng (album), LOVE 06 (album) (compilation)
|-
| 领会
| Understand/comprehend
| Ronald Cheng
| Used in background during Wahs birthday at Josephines house. Both Wah and Bo text each other behind Josephines back. 
| LOVE 06 (album) (compilation)
|-
|他不准我哭 He wont let me cry Stephy Tang Used in background while Bo lamented the fate of her marriage on the streets of Hong Kong Coloring Stephy - Stephy Tang (album), All the best - Cookies (album) (compilation)
|}

==External links==
*  

 

 
 
 
 