Ears, Open. Eyeballs, Click.
{{Infobox Film |
name           = Ears Open. Eyeballs, Click. |
image          = |
director       = Canaan Brumley |
producer       = Canaan Brumley |
distributor    = Films Transit International Inc.|
released       = 2005 |
runtime        = 95 minutes | English |
budget         = |
music          = John Stutzman |
awards         = FID Marseille, le premiere prix San Diego Film Festival, San Diego Filmmaker Award Philadelphia Documentary & Fiction Festival, Best Editing Festival du Cinema de Bruxelles, Best Cinematography|
tagline        = |
}}  documentary by Marine Marine recruits during United States Marine Corps Recruit Training|bootcamp. Unlike many documentaries, this film offers no narration nor a focus on central characters, shooting from a fly on the wall|fly-on-the-wall perspective. Despite this unusual approach, the film has received very positive reviews overall, especially from film festivals, such as the Los Angeles Film Festival and the San Diego Film Festival.

Brumley began shooting of the film with four cameramen, but only a few weeks into production, they quit. Brumley ended up shooting most of the film himself.   

Brumley himself has handled distribution through DVD sales on his website.  In addition, the film has been showing on the  .

==Content summary==
The film is an observation of Platoon 1141, Company C, 1st Recruit Training Battalion, Marine Corps Recruit Depot San Diego, California. The film begins with the platoons arrival at MCRD San Diego and continues to follow their journey, offering no narration and no central characters. The recruits are shown learning proper List of United States Marine Corps acronyms and expressions|vernacular, learning drill, learning their rifles, and being confronted by their Drill Instructors. The film ends with Platoon 1141 graduating and earning the title of Marine, with the final scene showing new recruits making their phone calls to home upon arriving at boot camp.

==Explanation of title==
During Marine Corps Recruit Training, recruits in formation are prohibited from turning their head or eyes away from their direct front, even when being addressed; when a Drill Instructor (DI) speaks to a recruit, that recruit is expected to stare forward if the DI is oblique to or behind him, and through him if the DI is directly to his front. When instructing recruits, a DI may command "Ears", to which the proper response for the recruits is "Open, sir!" If commanding them to look at him, the DI may command "Eyeballs", (or some other, less formal declaration, such as, "Look here" in any case the required response is the same) to which the recruits also have a formulaic response, in this case "Click." "Snap" is also common. The proper response is determined entirely by the will of the instructors, though it is almost always standardized through the company training.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 