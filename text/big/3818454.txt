Brannigan (film)
 
 
{{Infobox film
| name           = Brannigan
| image          = Brannigan (movie poster).jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster by Robert McGinnis
| director       = Douglas Hickox
| producer       = {{Plainlist| Arthur Gardner Jules Levy
}}
| screenplay     = {{Plainlist|
* Christopher Trumbo
* Michael Butler
* William P. McGivern
* William W. Norton  
}}
| story          = {{Plainlist|
* Christopher Trumbo
* Michael Butler
}}
| starring       = {{Plainlist|
* John Wayne
* Richard Attenborough
* Judy Geeson
* Mel Ferrer
* John Vernon
* Ralph Meeker
* Daniel Pilon
}}
| music          = Dominic Frontiere
| cinematography = Gerry Fisher
| editing        = Malcolm Cooke
| studio         = {{Plainlist|
* Wellborn
* Levy-Gardner-Laven  
}}
| distributor    = United Artists
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom    
| language       = English
| budget         = 
| gross          = 
}}
 thriller film Britain to organise the extradition of an American mobster, who is soon kidnapped and held for ransom. Struggling with the restrained policing style of his British counterparts, the tough Irish-American detective uses his own brand of law enforcement to recapture the criminal.

After turning down the starring role in  . - 25 September 1995. - Retrieved: 2008-08-05  and seeing the subsequent success of that film, Wayne made two police thrillers in quick succession. After starring in McQ, he made this "cop out of water" film in the same vein as Clint Eastwoods Coogans Bluff (film)|Coogans Bluff.

==Plot== Lieutenant Jim Brannigan (John Wayne) is sent to London to extradite a notorious American gangster, Ben Larkin (John Vernon). Brannigan is assigned a local officer, Jennifer (Judy Geeson), to help while he is in London. But before Brannigan can collect his man, Larkin is kidnapped.
 British way of life, and the restrained style of policing, he employs techniques not usually seen in Britain.

In the meantime, a contract had already been put out on Brannigans life by Larkin, so hit man Gorman (Daniel Pilon) tails Brannigan in a black Jaguar, making several attempts to kill him and nearly shooting Jennifer by mistake.
 Commander Swann Metropolitan Police commander whos not afraid to get his hands dirty. There is continual conflict between Brannigan and Swann about the Americans carrying, and use of, his .38 Colt Diamondback revolver.

Permitted to go alone to deliver the ransom payment, Fields personally eliminates the kidnappers. He and Larkin celebrate having pulled off a scheme to get the money, Larkin calling the loss of a finger a small price to pay. Brannigan bursts in to foil their plans. As he and Jennifer walk away, Gorman tries to mow them down with his car, but he is shot by Brannigan, who can now return home to Chicago.

==Cast== Lieutenant James Brannigan Commander Sir Charles Swann, Baronet|Bart. 
*Mel Ferrer as Mel Fields Detective Sergeant Jennifer Thatcher
*John Vernon as Ben Larkin
*Daniel Pilon as Gorman Captain Moretti Lesley Anne Down as Luana
*Barry Dennen as Julian Detective Inspector Michael Traven
*James Booth as Charlie-the-Handle
*Arthur Batanides as Angell
*Pauline Delaney as Mrs. Cooper
*Del Henney as Drexel
*Brian Glover as Jimmy-the-Bet
*Don Henderson as Geef
*Tony Robinson as Motorcycle Courier

==Production==
The film is notable for its well-executed action sequences, including a spectacular car chase through Batterseas Shaftesbury Estate, Wandsworth and Central London featuring Brannigan jumping a yellow Ford Capri coupe across the half raised Tower Bridge. One sequence features shots of the interior and exterior of Londons famous Royal Automobile Club, which has changed little since the shooting of the film. The Capris jump was one of the last significant appearances of Tower Bridge without its now-familiar red, white, and blue paint scheme which was applied in 1977 to commemorate the Silver Jubilee of Elizabeth II.
 Raw Deal, Blue Line tracks in the median.
 Docklands in general, and Canary Wharf in particular. The location is seen as a derelict, nearly-abandoned dockside during Brannigans confrontation with the motor-scooter messenger (Tony Robinson), and has since been radically transformed.  When a hole is blown in Brannigans lavatory wall, he looks out to see the Albert Memorial, its statue still coated in thick black paint rather than gold leaf.  At the time of filming, the Trafalgar Square post office occupied not only its current footprint, but extended throughout the adjoining commercial spaces, and was marked by an unusually shaped sign extending out from the corner of the building.

It contains a unique piece of footage of the inside of the Garrick Club (known as the actors club) which traditionally does not allow cameras and was only agreed to as Richard Attenborough was a long-term member. In the scene in which Brannigan and Commander Swann are at the bar in the Garrick Club, on the wall behind them are portraits of Laurence Olivier and another of John Gielgud, both in Garrick Club ties. 
 Mayor Richard J. Daley thereafter discouraged motion picture and television location filming in the city for the rest of his administration and its aftermath. Brannigan is one of the few films – along with Cooley High, also released in 1975 – to have been approved and granted police assistance during the two-decade era.

==Home Media==

On 2 October 2001 brannigan was released on DVD for the first time ever. 

Brannigan is available  through Amazon.com in a Two Pack with Killer Force, and will be released for the first time on Blu-ray through Screen Archives Entertainment on 8 July 2014.

==See also==
*John Wayne filmography

==References==
 

==External links==
*  
*  
*  

 


 
 
   
 
 
 
   
 
   
 
 
 