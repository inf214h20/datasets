Bandhan (1998 film)
 
 

{{Infobox film
| name           = Bandhan
| image          = Bandhan1998.jpg
| image_size     =
| caption        = DVD Cover
| director       = K. Muralimohana Rao 
Rajesh Malik
| producer       = Narendra Bajaj
| writer         = Anwar Khan (dialogues) Manoj Kumar
| narrator       =
| starring       = Salman Khan Jackie Shroff Rambha Ashwini Bhave Shakti Kapoor Mukesh Rishi
| music          = Anand Raj Anand 
Himesh Reshammiya 
Surendra Singh Sodhi
| cinematography = Rajan Kinagi
| editing        = J. Narasimha Rao
| distributor    = Eros International
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         = 50&nbsp;million 
| gross          =   
| preceded_by    =
| followed_by    =
}}

Bandhan is a Bollywood drama/action film starring Salman Khan, Rambha (actress)|Rambha, Jackie Shroff and Ashwini Bhave. The film was released in 1998.  Salman Khan plays a man who gets help from his sisters husband (played by Jackie Shroff). They face many conflicts in this movie, but in the end they live happily ever after. The film was a remake of Tamil film Pandithurai.

==Plot==
Thakur Suraj Pratap (Jackie Shroff) sees a young woman performing pooja at a temple, and instantly falls in love with her. He finds out that the womans name is Pooja (Ashwini Bhave), and belongs to a poor family, consisting of her dad, Ramlal, her mom, and a brother named Raju (Salman Khan). He approaches Ramlal for the hand of Pooja, and Ramlal is delighted to accept him as his son-in-law. They are married with pomp and ceremony. Pooja is very fond of Raju, and takes him along. At the Thakurs palatial home, they are introduced to Jyoti (Rambha (actress)|Rambha) Prataps sister. Years later, all have grown up. Pooja has been unable to have any children, and Raju and Jyoti are in love. One day Suraj meets Vaishali (Shweta Menon), a stunning courtesan, and decides to make her his mistress. He disregards any advise against this decision, and even casts Raju away, and cautions him from having to do anything with Jyoti or his household. He even threatens to divorce Pooja over Vaishali. Unable to bear the shock, Ramlal passes away. Rajus attempts to convince Suraj to let go of Vaishali are all in vain, as Suraj openly weds Vaishali, and asks Pooja to accept this decision or else leave his house. Pooja will now have to decide to stay with Suraj or leave him with his new wife.

==Cast==
* Jackie Shroff as Thakur Suraj Pratap
* Salman Khan as Raju Rambha as Jyoti
* Ashwini Bhave as Pooja
* Shakti Kapoor as Billu
* Ashok Saraf as Chillu
* Mukesh Rishi as Gajendra
* Shweta Menon as Vaishali (Gajendras sister)
* Aashif Sheikh as Police Inspector
* Prashant Vishnoi

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Title !! Singer(s) !! Length
|-
| 1
| Tere Naina Mere Naino Ki 
| Kavita Krishnamurthy, Udit Narayan
| 05:43
|-
| 2
| Balle Balle
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik, Sapna Awasthi
| 05:10
|-
| 3
| Bandhan (Title Song)
| Kumar Sanu
| 05:22
|-
| 4
| Chhora Fisal Gaya
| Alka Yagnik, Udit Narayan
| 05:16
|-
| 5
| Tere Dum Se Hai Mera Dum
| Alka Yagnik, Kumar Sanu
| 05:36
|-
| 6
| Bandhan (Sad version)
| Kumar Sanu
| 03:47
|-
| 7
| Main Deewani Main Mastani
| Swetha
| 04:29
|}

== External links ==
*  

 
 
 
 

 