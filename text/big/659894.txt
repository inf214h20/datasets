Tricks (film)
{{Infobox film
| name           = Tricks Sztuczki
| image          = 
| image_size     = 
| caption        = 
| director       = Andrzej Jakimowski
| producer       = Izabel Jakimowski
| writer         = Tahila Jakimowski
| narrator       = 
| starring       = Damian Ul Ewelina Walendziak Tomasz Sapryk
| music          = Tomasz Gąssowski
| cinematography = Adam Bajerski
| editing        = Cezary Grzesiuk
| distributor    = 
| released       =   (Poland)
| runtime        = 95 min.
| country        = Poland
| language       = Polish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Polish film  written, directed and produced by Andrzej Jakimowski starring Damian Ul, Ewelina Walendziak and Tomasz Sapryk. Tricks is Andrzej Jakimowskis follow up to Squint Your Eyes ( ), his 2002 debut.

==Plot==
A charming bittersweet narrative unfolds from director Andrzej Jakimowski. This is the story of siblings Stefek, 6, and Elka, 18, along with Elkas car mechanic boyfriend Jerzy during one sun-drenched summer. The siblings live with their shopkeeper mother. Their father has left their mother for another woman, unaware of Stefeks existence. After a chance encounter at the local railway station, and despite a denial by his sister that this was his father, Stefek decides to challenge fate to engineer another meeting. He believes that the chain of events he sets in motion will help him get closer to his father who abandoned his mother. His sister Elka teaches him how to bribe fate with small sacrifices. Tricks played, coupled with a number of coincidences eventually bring the father to the mothers shop but the long awaited re-union does not immediately materialise as expected. As a last chance Stefak tries his good luck with the most risky of his tricks.

==Cast==
*Damian Ul as Stefek
*Ewelina Walendziak as Elka
*Tomasz Sapryk as Father of Stefek and Elka
*Rafal Guzniczak as Jerzy
*Iwona Fornalczyk as Mother of Stefek and Elka
*Joanna Liszowska as Violka
*Andrzej Golejewski as Homless
*Grzegorz Stelmaszewski as Turek
*Simeone Matarelli as Leone

==Distribution and response==
 Best Foreign Language Film. 

===Awards===

*Polish Film Festival
**Best Cinematography - Adam Bajerski
**Golden Lion - Andrzej Jakimowski

*São Paulo International Film Festival
**Special Jury Award - Andrzej Jakimowski

*Tokyo International Film Festival
**Best Actor Award - Damian Ul

*Venice Film Festival
**Label Europa Cinemas - Andrzej Jakimowski
**Laterna Magica Prize - Andrzej Jakimowski

===Nominations===

*São Paulo International Film Festival
**International Jury Award - Andrzej Jakimowski

*Tokyo International Film Festival
**Tokyo Grand Prix - Andrzej Jakimowski

==See also==
 The Railway Children - 1970 film

==References==
 

==External links==
*  
*  
*   (currently broken, 20 September 2009)

 
 


 