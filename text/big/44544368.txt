Moonlight on the Prairie
 
{{Infobox film
| name           = Moonlight on the Prairie
| image          = Moonlight on the Prairie.jpg
| caption        = Film poster
| director       = D. Ross Lederman
| producer       =  William Jacobs
| starring       = Dick Foran
| music          = 
| cinematography = 
| editing        = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Western film directed by D. Ross Lederman.    It was the first of a Warner Bros. singing cowboy film series with Dick Foran and his Palomino Smoke.

==Cast==
* Dick Foran as Ace Andrews (as Dick Foran the Singing Cowboy)
* Smoke as Smoke - Ace Andrews Horse (as Smoky)
* Sheila Bromley as Barbara Roberts (as Sheila Mannors)
* George E. Stone as Small Change
* Joe Sawyer as Luke Thomas Joe King as Sheriff Jim (as Joseph King)
* Robert Barrat as Buck Cantrell
* Dickie Jones as Dickie Roberts Bill Elliott as Jeff Holt (as Gordon Elliott) Herbert Heywood as Pop Powell Raymond Brown as Stage agent
* Richard Carle as Colonel Gowdy (scenes deleted)
* Milton Kibbee as Henchman Pete (as Milt Kibbee)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 