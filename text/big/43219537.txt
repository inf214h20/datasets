El hombre de los hongos
{{Infobox film
| name           = El hombre de los hongos
| image          = 
| caption        = 
| director       = Roberto Gavaldon
| producer       = 
| writer         = Sergio Galindo Roberto Gavaldón Tito Davison
| starring       = Adolfo Marsillach Isela Vega Philip Michael Thomas Sandra Mozarowsky
| music          = Raul Lavista
| cinematography = Miguel Araña Raul Perez Cubero
| editing        = 
| distributor    = CONACINE
| released       =  
| runtime        = 110 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}} Mexican drama film based on the novel of the same name by Sergio Galindo.

== Synopsis  ==
The story unfolds in a family of landowners from the colonial era, which owns a sugar plantation on the edge of the jungle. While hunting in the forest one day, the father, Don Everardo, discovers a young black orphan by the river. He leads the child to his great farm and call him Gaspar. Friendly and intelligent, the boy quickly becomes a new member of the family. He plays with his son, Sebastian and his two daughters, Emma and Lucilla, as if they were his own sisters. In a separate story, this family takes fine dining. One of his favorite mushroom dishes are prepared with an old family recipe from Spain. The family discovers if the mushroom is edible collected getting a volunteer to eat a few first. If voluntary collapses and dies, then the family does not serve mushrooms in the party.

The family has another flirtation with danger: They keep a black panther called Toy chained in the main courtyard. From time to time it is released and chases people.

Some years pass, and Gaspar grows into a handsome young man, and daughters become beautiful women. They go into the woods hiking and swimming in the river. But the play of children has become flirtations, especially among young Gaspar and Emma. Along the way, Gaspar reveals his knowledge of different species of fungi that grow in the forest.

The family unit falls apart when Emma and her mother, Elvira, compete for the attentions of Gaspar. Elviras flirtations with Gaspar provoke the jealousy of Emma, who leverages existing hostlidad between Elvira and the Panther Toy to get rid of her mother.

This situation adds a strange incestuous relationship between Sebastian and Lucila, older siblings. When Don Everardo discovers the affair of his daughter with the black servant Gaspar, he decides to make him his new "man of the mushrooms". But no one has the knowledge of Gaspar on mushrooms, and a lavish party at the farm will become a circus when Emma and Gaspar advantage the mushrooms to get rid of his own family and escape together.

== Cast ==
* Adolfo Marsillach...Don Everardo
* Isela Vega... Elvira
* Philip Michael Thomas...Gaspar
* Sandra Mozarowsky...Emma
* Ofelia Medina...Lucila
* Fernando Allende...Sebastian
* Josefina Echánove...The Nanny

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 

 