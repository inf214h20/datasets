Death Bell 2: Bloody Camp
{{Infobox film
| name           = Death Bell 2: Bloody Camp
| image          = Death-Bell-2-poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name
 | hangul         =  死  :  
 | hanja          = 고  두 째 이야기:  
 | rr             = Gosa dubeonjjae iyagi : Gyosaengsilseup
 | mr             = Kosa tupŏntchae iyagi : Kyosaengsilsŭp}}
| director       = Yoo Sun-dong
| producer       = Kim Gwang-su
| writer         = Park Hye-min, Lee Jeong-hwa, Lee Gong-ju
| starring       = Park Ji-yeon Yoon Shi-yoon Hwang Jung-eum Kim Su-ro Park Eun-bin Yoon Seung-ah Son Ho-jun
| music          = Kim Woo-geun
| cinematography = Choi Yeong-taek
| editing        = Choi Min-yeong Lee Jin Core Contents Media (now MBK Entertainment)
| distributor    = Next Entertainment World
| released       =  
| runtime        = 84&nbsp;minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =  
}}
Death Bell 2: Bloody Camp ( )   is a 2010 Korean horror film. The film was directed by Yoo Sun-dong. The film is a sequel to the 2008 film Death Bell.    The story is unrelated to the previous film.

==Synopsis==
This film is about a group of high school students and teachers who get locked in the school after the swimming instructor is murdered. In South Korea, the high school student and swimmer Jeong Tae-yeon (Yoon Seung-ah) is found dead in the pool, which is found as a suicide. Two years later, teacher Park Eun-su (Hwang Jung-eum) joins the high school where Tae-yeons stepsister Lee Se-Hee (Park Ji-yeon) is haunted by nightmarish visions and is bullied by the student Eom Ji-yun (Choi Ah-jin). Eun-su finds it difficult to get respect in the classroom and is backed up by an older teacher, Cha (Kim Su-ro). Se-Hee and her classmates are selected for an elite "study camp" held at the school during the summer break where 30 students study for their university entrance exams. The schools swimming trainer is murdered in the showers, and the words "When an innocent mother is killed, what son would not avenge her death?" found scrawled on a blackboard. A voice warns the students that theyll all be killed unless they can answer who is the murderer and why. The students and teachers find theyre locked in the school when more deaths begin to happen.

==Release==
Death Bell 2: Bloody Camp premiered at the Puchon International Fantastic Film Festival on July 23, 2010 where it was the festivals closing film.     The film received wide release in South Korea on July 28, 2010.  On wide-release, the film was very successful with over 50,000 people seeing it in Korea on its opening day. An official for the film said it was "four times what we expected". 

While writing the script, it was suggested that the characters should solve their problems in a quiz show format like they did in the first film. Director Yoo Sun-dong was against this ideas as he felt it was too much of an imitation of the first film.  Yoo was influenced by his own high school experiences, stating that "Authoritative teachers like Teacher Kang (played by Kim Byung-ok) and Teacher Cha (played by Kim Su-ro) or the competition and violence between the students were things that I saw and felt when I was in school. I tried to put such horrifying elements into the film." 

==Reception==
The Hollywood Reporter wrote that the film is an "alarmingly brainless and sloppily directed follow-up to Death Bell"  noting that the only "scene worthy of attention is when student Jang-kook is stranded on a corridor and repeatedly attacked by a motorbike outfitted with revolving blades. It has the Gothic, apocalyptic taste of Mad Max."    JoongAng Daily wrote a negative review of the film, saying that "it should have taken more chances and offered audiences more than blood...the film wont do much for viewers who are die-hard slasher film fans."    Despite negative reviews, both The Hollywood Reporter and JoongAng Daily praised the scene involving a metal-spiked motorcycle that attacks a student.   Film Business Asia gave the film a seven out of ten rating saying that the film was a "Dark, fast-moving gore feast, with less emphasis on puzzle countdowns but a richer plot than its predecessor." 

==References==
 

==External links==
*  
*  
*   at Cine 21 (Korean)

 
 
 
 
 
 