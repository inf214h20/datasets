A Cry in the Wild
 

{{Infobox film
| name           = A Cry in the Wild
| image          = A Cry in the Wild.jpg
| alt            =  
| caption        = Film poster
| director       = Mark Griffiths
| producer       = Roger Corman Julie Corman
| writer         = Gary Paulsen Catherine Cyran
| starring       = Jared Rushton Pamela Sue Martin Stephen Meadows Ned Beatty
| music          = Arthur Kempel
| cinematography = Greg Heschong
| editing        = Carol Oblath
| studio         = 
| distributor    = Concorde Pictures MGM Home Entertainment
| released       =   
| runtime        = 
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
A Cry in the Wild is a 1990 film based on the book  ;  ; and  . 

==Plot==
The movie opens up with Brian Robeson and his mother getting a package. She later gives it to Brian revealing it to be a hatchet at the airport. When Brian gets on the single engine plane with the pilot they have a short conversation.

The pilot lets Brian fly the plane and Brian enjoys it. However, when the pilot has a heart attack and dies, the plane crashes in the wilderness of Canada, leaving Brian to try to survive, all while dealing with his parents divorce.

==References==
 

==External links==
*   at Internet Movie Database

 
 
 
 
 
 
 


 