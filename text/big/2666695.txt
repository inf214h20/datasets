Wonder Man (film)
{{Infobox film
 | name           = Wonder Man
 | image          = Wonder Man original cinema poster.jpg
| border         = yes
 | caption        = Original 1946 cinema poster
 | director       = H. Bruce Humberstone
 | producer       = Samuel Goldwyn
 | story  = Arthur Sheekman
 | screenplay         = Jack Jevne   Eddie Moran
 | starring       = Danny Kaye   Virginia Mayo
 | music          = Ray Heindorf   Heinz Roemheld William E. Snyder
 | editing        = Daniel Mandell
 | studio         = Samuel Goldwyn Productions
 | distributor    = RKO Radio Pictures
 | released       =  
 | runtime        = 98 minutes
 | country        = United States
 | language       = English
 | budget         = 
 | gross          = 
 }}
 produced by directed by H. Bruce Humberstone. Mary Grant designed the films costumes.

==Plot==
Danny Kaye plays a double role as a pair of estranged "super-identical twins", with very similar looks, but very different personalities. Buster Dingle, who goes by the stage name "Buzzy Bellew", is a loud and goofy performer at a classy nightclub (the Pelican Club), while Edwin Dingle is a studious, quiet bookworm writing a history book. The two brothers have not seen each other for years.

Buster becomes the witness to a murder committed by mob boss "Ten Grand" Jackson (Steve Cochran), and is promptly murdered himself. He comes back as a ghost, calling on his long-lost brother for help to bring the killer to justice. As a result, the shy Edwin must take his brothers place until after his testimony is given.

In the meantime, he has to dodge Jacksons hitmen and fill in for Buster at the nightclub. To help him out, Buster—who cannot be seen or heard by anyone but Edwin—possesses him, with outrageously goofy results.

A famous scene features Edwin, possessed by Buzzy, performing at the Club. Under Buzzys influence, Edwin pretends to be a famous Russian singer with an allergy to flowers. A vase of flowers is nonetheless placed on a table near him, and his song, "Otchi Chornya", is frequently interrupted by his loud and goofy-sounding sneezes.

The story is further humorously complicated by the love interests of the brothers; whilst the murdered Buster was engaged to entertainer Midge Mallon (Vera-Ellen), Edwin is admired by librarian Ellen Shanley (Virginia Mayo).

In the end, Ellen marries Edwin, whilst Midge consoles herself (apparently without regret) by marrying the owner of the club where Buster was appearing.

==Awards== Best Special Best Original Best Musical Best Sound Recording.   

The film was also entered into the 1946 Cannes Film Festival.   

== Main cast ==
* Danny Kaye as Edwin Dingle and Buzzy Bellew
* Virginia Mayo as Ellen Shanley
* Vera-Ellen as Midge Mallon (singing voice was dubbed by June Hutton) Donald Woods as Monte Rossen
* S. Z. Sakall as Schmidt
* Allen Jenkins as Chimp
* Edward Brophy as Torso
* Steve Cochran as "Ten Grand" Jackson
* Otto Kruger as District Attorney Richard Lane as Asst. District Attorney
* Natalie Schafer as Mrs. Hume
* Huntz Hall as Sailor
* Virginia Gilmore as Sailors Girlfriend
* Edward Gargan as Policeman in Park 
* The Goldwyn Girls

==See also==
* List of ghost films

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 