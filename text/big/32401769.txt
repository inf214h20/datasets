Apocalypse, CA
  
{{Infobox film
| name           = Apocalypse, CA
| caption        = 
| image          = Apocalypse, CA FilmPoster.jpeg
| director       = Chad Peter
| producer       = Chad Peter Eric Branco Tierney Bray
| writer         = Chad Peter
| starring       = Nick Mathis Erin Bodine Anne McDaniels Alexander Cardinale Elizabeth Sandy Sarah Smick
| music          = Avi Ghosh
| cinematography = Chad Peter
| editing        = Kway Lars Chad Peter
| studio         = SDNP Films LLC
| distributor    = SDNP Films LLC
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Apocalypse, CA is a 2011 American comedy film.  Written and directed by Chad Peter, the film stars Nick Mathis, Erin Bodine, Anne McDaniels, Alexander Cardinale, Elizabeth Sandy, and Sarah Smick. Apocalypse, CA is an indie film featuring visual effects from artists Ryan Wieber and Teague Chrystie in this feature film directorial debut by Chad Peter.

== Plot ==
Apocalypse, CA is the story of John Parsons and his ill-fated friends as they prepare for certain death at the hands of a massive asteroid, sex-inducing drugs, a three-hundred foot giant, and a horde of other unfortunate problems.

Mysterious radio personality and apparent part-time genie, Sassy Boots (Elizabeth Sandy) takes it upon herself to grant John Parsons (Nick Mathis) a few wishes five days before the world is to be destroyed by an asteroid. Only problem is Johns wishes arent exactly calculated, or even planned - the end result of which is a situation far worse than before.

Accompanied by his brother Hank (Alexander Cardinale), Hanks friend Renee (Anne McDaniels) and Johns childhood crush Jacklyn (Erin Bodine), John and friends take the high road to Palm Springs - otherwise known as the epicenter where the asteroid will first strike Earth. With a bite the bullet approach, John & friends learn a little more about life and each other, all leading up to a wild climax of booze, sex, fantasy, and guns-a-blazin desert adventure.

== External links ==
* 
* 

 
 
 
 
  
 
 
 

 