Chandralekha (1948 film)
 
 
 
 

{{Infobox film
| name           = Chandralekha
| image          =  
| alt            = Illustrated poster showing men riding elephants and horses
| caption        = International poster
| director       = S. S. Vasan
| writer         = Veppathur Kittoo Kothamangalam Subbu K. J. Mahadevan Sangu Naina Ranjan
| producer       = S. S. Vasan
| studio         = Gemini Studios
| music          = Songs: S. Rajeswara Rao Background music: M. D. Parthasarathy
| based on       = 
| editing        = Chandru
| cinematography = Kamal Ghosh K. Ramnoth
| released       =  
| runtime        = 207–210 minutes  as does the 1999 edition of Encyclopaedia of Indian cinema by Ashish Rajadhyaksha and Paul Willemen,  while the 2011 book The Best of Tamil Cinema by G. Dhananjayan gives the runtime as 210 minutes. }}
| budget         = 
| gross          = 
| country        = India Tamil Hindi
}} Indian Tamil Tamil historical Ranjan in the lead roles, the film follows two brothers named Veerasimhan and Sasankan, who fight with each other over ruling their fathers kingdom and for marrying the village dancer Chandralekha.

The development of Chandralekha began in the early 1940s when, after two successive box office hits, Vasan announced that his next film would be titled Chandralekha. However, when he launched an advertising campaign for the film, he had only the name of the heroine from a storyline by Gemini Studios story department he had rejected. Veppathur Kittoo, one of his storyboard artists, eventually developed a story based on a chapter of George W. M. Reynolds novel Robert Macaire: or, The French bandit in England (1848), which impressed Vasan. The original director T. G. Raghavachari directed more than half of the film, then left the project because of disagreements with Vasan, who took over the film in his directorial debut.

Originally made in Tamil and later in Hindi, Chandralekha was in production for five years from 1943 to 1948. It went through a number of changes to the script, cast and production, and ultimately became the most expensive film made in India at the time; the budget for a single sequence equalled that typical for an entire film of the period. Vasan mortgaged all of his property and even sold his jewellery to complete the film. Cinematography was by Kamal Ghosh and K. Ramnoth. The music was largely inspired by both Indian and Western classical music; it was composed by S. Rajeswara Rao and by M. D. Parthasarathy, with lyrics by Papanasam Sivan and Kothamangalam Subbu.
 South Indian cinema gained prominence throughout the country with the films release. The film inspired South Indian film producers to market their Hindi films in North India. It was dubbed in English, Japanese, Danish and other foreign languages and was screened at Indian and international film festivals.

==Plot==
  abdicate in favour of Veerasimhan. This enrages the younger brother Sasankan; he forms a gang of thieves who embark on a crime spree. Chandralekhas father is injured in the ensuing chaos and dies soon after. Chandralekha, now orphaned, joins a band of travelling musicians, whose caravan is raided by Sasankans men.

Sasankan orders Chandralekha to dance for him, which she does after being flogged. She later escapes. Later, Sasankan ambushes Veerasimhan and takes him prisoner. Chandralekha watches Sasankans men trapping Veerasimhan in a cave and sealing its entrance with a boulder. She rescues him with the help of elephants from a passing circus troupe. Veerasimhan and Chandralekha join the circus to hide themselves from Sasankans men. After returning to the palace, Sasankan imprisons his parents and declares himself king. He immediately sends a spy to find Chandralekha.
 gypsy group. When Veerasimhan leaves to seek help, Sasankans men capture Chandralekha and take her to the palace. Sasankan tries to woo Chandralekha but she pretends to faint every time he approaches her. One of Chandralekhas friends from the circus comes to Sasankan disguised as a gypsy healer, claiming that she can cure Chandralekha of her "illness". Behind locked doors, the two girls secretly talk. Sasankan is pleased to find Chandralekha miraculously cured and apparently ready to accept him as a bridegroom. In return, he agrees to Chandralekhas request for a drum dance to celebrate the royal wedding.

Huge drums are arranged in rows in front of the palace. Chandralekha joins the dancers, who dance on the drums. Sasankan is impressed with Chandralekhas performance but, unknown to him, Veerasimhans soldiers are hiding inside the drums. As the dance ends, they rush out of the drums and attack Sasankans men. Veerasimhan confronts Sasankan, and they have a long sword fight, which ends with Sasankans defeat and imprisonment. Veerasimhan releases his parents and becomes the new king, while Chandralekha becomes his queen.

==Production==

===Development=== Bala Nagamma Mangamma Sapatham (1943), which netted profits of  . He wanted a film on a grand scale with no budgetary constraints.  He asked Gemini Studios story department (consisting of K. J. Mahadevan, Kothamangalam Subbu, Sangu, Naina and Veppathur Kittoo)  to write a screenplay. They saw Mangamma Sapatham and Bala Nagamma as "heroine-oriented stories", and suggested a similar story to Vasan. They told the story of Chandralekha, a tough woman who "outwits a vicious bandit, delivers the final insult by slashing off his nose and, as a finishing touch, fills the bloodied gaping hole with hot, red chilli powder". Vasan disliked their storys gruesomeness and vulgarity, and rejected it but kept the characters name, Chandralekha. 

Without waiting for the full story, Vasan announced that his next project would be titled Chandralekha, and he publicised it heavily in most major publications. Despite much work by Geminis writers, the story was still not ready three months later. Vasan grew impatient and told the writers he would be shelving Chandralekha in favour of developing Avvaiyar (film)|Avvaiyyar (1953). After he allowed them a final week,  Kittoo discovered George W. M. Reynolds novel Robert Macaire: or, The French bandit in England (1848). In the first chapter he read:

   

Vasan was impressed when Kittoo narrated a story based on this episode. He decided to continue with the film, naming the heroine Chandralekha. Although the story was still incomplete,  the rest of Geminis story department later improvised Kittoos ideas to give the story its final form. 

===Casting===
{| class="infobox" style="font-size:100%;"
|-
! Actor
! class="unsortable" | 
! Role  
|-
|  
| ...
| Chandralekha
|-
|  
| ...
| Veerasimhan
|-
|  
| ...
| Sasankan
|-
|  
| ...
| Circus performer
|-
|  
| ...
| Circus artist
|-
|  
| ...
| Circus artist
|-
|  
| ...
| Circus manager
|-
|  
| ...
| Gypsy dancer
|-
|  
| ...
| The king
|} production controller, effeminate to play a "steel-hard villain", but eventually agreed. By then, Ranjan was already committed to B. N. Raos Saalivaahanan (1945), but Kittoo persuaded him to take a screen test for Chandralekha and Rao gave Ranjan a few days off. The test was successful and Ranjan was officially cast. 

T. R. Rajakumari was chosen to play Chandralekha,  replacing Vasans first choice K. L. V. Vasantha.  Film historian Randor Guy believes Vasan chose Rajakumari over Vasantha because she was then leaving Gemini Studios permanently for Modern Theatres in Salem (India)|Salem.  In April 1947, N. S. Krishnan was released from prison;  Vasan recruited him and T. A. Madhuram to play the circus artists who help Veerasimhan rescue Chandralekha from Sasankan.  The script was rewritten; new scenes were added to showcase the comedy duo.  Actors Madurai Sriramulu Naidu and S. N. Lakshmi made their acting debut in this film; the former appears in an uncredited role as a horseman,  and the latter appears as a dancer in the climactic drum-dance scene.    

When a minor role of the heros bodyguard was yet to be cast, the then struggling stage actor Villupuram Chinniah Pillai Ganeshamurthy who later became known as Sivaji Ganesan was interested; he grew his hair long for the role. Ganeshamurthy had contacted Kittoo several times asking for a role in the film. Eventually, Kittoo took Ganeshamurthy to Vasan, who had seen him perform on-stage. To Ganeshamurthys dismay, Vasan rejected him, calling him "totally unsuited for films" and told him to choose another profession. This incident caused a permanent schism between Vasan and Ganeshamurthy.  The role of the bodyguard was eventually given to N. Seetharaman, who later became known as Javar Seetharaman. 

Kothamangalam Subbus wife M. S. Sundari Bai plays a circus performer who helps Chandralekha escape from Sasankan.  T. A. Jayalakshmi, in one of her earliest film roles, appears briefly in one scene that lasted for a few minutes, as a dancer.   L. Narayana Rao plays the circus manager.  T. E. Krishnamachari plays the king and V. N. Janaki plays a gypsy dancer who gives Chandralekha and Veerasimhan shelter in the forest.  Veppathur Kittoo plays Sasankans spy; he also worked as an assistant director in the film.  Pottai Krishnamurthy appears in the song "Naattiya Kuthirai".  Other supporting actors include Seshagiri Bhagavathar, Appanna Iyengar, T. V. Kalyani, Surabhi Kamala, Subbiah Pillai, Cocanada Rajarathnam, N. Ramamurthi, Ramakrishna Rao, Sundara Rao, V. S. Susheela, Varalakshmi and Velayutham, in addition to "100 Gemini Boys & 500 Gemini Girls".  Studio staff members, their families and passers-by were recruited as extras to play spectators in the circus scenes. 

===Filming===
{{Quote box quote = During the making (of Chandralekha), our studio looked like a small kingdom&nbsp;... horses, elephants, lions, tigers in one corner, palaces here and there, over there a German lady training nearly a hundred dancers on one studio floor, a shapely Sinhalese lady teaching another group of dancers on real marble steps adjoining a palace, a studio worker making weapons, another making period furniture using expensive rosewood, set props, headgear, and costumes, Ranjan undergoing fencing practice with our fight composer Stunt Somu, our music directors composing and rehearsing songs in a building&nbsp;... there were so many activities going on simultaneously round the clock. source =&nbsp;– Kothamangalam Subbu, on the making of the film at Gemini Studios  align = right width = 30% border = 1px
}} Raj Bhavan, Guindy),  Raghavachari left the project and Vasan took over directing, making his directorial debut. 

Originally, the film did not include any circus scenes. Vasan decided to include them when the film was halfway through production, and the screenplay was altered.  Kittoo travelled throughout South India and Ceylon (now Sri Lanka) to see over 50 circuses  before choosing the Kamala Circus Company and Parasuram Lion Circus;  Vasan employed Kamala for a month.  The circus scenes were shot by K. Ramnoth.   In retrospect, Kittoo said about Ramnoths work, "In those days, we had no zoom lenses and yet Ramnoth did it. One night, while Chandralekha is performing on the flying trapeze, she notices the villains henchman in the front row. She is on her perch high up and he is seated in a ringside chair. Shock hits her and to convey the shock the camera zooms fast from her to the man. Today, with a fast zoom shot it can be done very easily, but there was no such lens forty years ago. Ramnoth did it using the crane. He planned it well and rehearsed the shot for long. He took the shot 20 times and selected the best take."  The circus scenes lasted for 20 minutes; according to G. Dhananjayan these are "the longest footage of scenes outside the main plot that one can see". 
 name = exchange1948}} equal to the complete budget for a typical Indian film of the time. 
 editor Chandru to edit according to Ramnoth, and he was impressed with the result.  C. E. Biggs worked as the sound engineer. 
 Sreenivasa Iyengar then the editor of The Hindu and sold his jewellery to complete the film.    Adjusted for inflation, the film would have cost   in 2010.  According to film historian S. Muthiah, Chandralekha, considering the free-floating exchange rate at that time, became the first film with a budget of more than a million dollars to be made outside of the United States. 

==Themes and influences== Ruritanian period extravaganza". 
 The Tiger The Indian Coney Island. 

==Music== Hindustani music, Latin American Portuguese folk Wagner and Rimsky Korsakov (Scheherazade (Rimsky-Korsakov)|Scherezade) being used at dramatic moments". 
 The Firefly (1937).   For the Hindi soundtrack, Vasan offered most of the songs to Uma Devi, who became popularly known as Tun Tun.  She was initially hesitant, feeling that "these were beyond her capabilities", but she was supported by Rajeswara Rao who "worked hard on her".  The music of Chandralekha helped it to become one of the most successful Indian musical films of the 1940s;  it "created an atmosphere for a number of music directors influenced by Western music" in Tamil cinema.   

In his 1997 book Starlight, Starbright: The Early Tamil Cinema, Randor Guy said Parthasarathy and Rajeswara Rao "created a fine blend of lilting music of many schools".  Writing for Screen (magazine)|Screen in April 1998, film historian M. Bhaktavatsala called the songs as "distinct and standing on its own, with barely any background score attempting to interlink anything, just periods of silence".   

;Tamil tracklisting  
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Indrae Enathu Kuthukalam
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = T. R. Rajakumari
| length1         = 1:09

| title2          = Aathoram Kodikkalam
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = M. D. Parthasarathy
| length2         = 2:23

| title3          = Padathey Padathey Nee
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = M. S. Sundari Bai
| length3         = 3:29

| title4          = Naattiya Kuthirai
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = M. D. Parthasarathy
| length4         = 4:09

| title5          = Namasthey Sutho
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Chorus
| length5         = 4:10

| title6          = Group Dance
| note6           = Instrumental
| writer6         =
| lyrics6         =
| music6          =
| extra6           =&nbsp;—
| length6         = 1:25

| title7          = Aayilo Pakiriyamo
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7           = N. S. Krishnan, T. A. Mathuram
| length7         = 3:10

| title8          = Manamohana Saaranae
| note8           =
| writer8         =
| lyrics8         =
| music8          =
| extra8           = T. R. Rajakumari
| length8         = 2:30

| title9          = Murasu Aatam (Drum Dance)
| note9           = Instrumental
| writer9         =
| lyrics9         =
| music9          =
| extra9           =&nbsp;—
| length9         = 5:59
}}

Hindi tracklisting 
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Sajana Re Aaja Re
| note1           =
| writer1         =
| lyrics1         =
| music1          = Uma Devi
| length1         = 3:04

| title2          = Man Bhavan Sawan Aaya
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Uma Devi
| length2         = 3:09

| title3          = O Chand Mere
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Uma Devi
| length3         = 3:21

| title4          = Maai Re Main To Madhuban Mein
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Uma Devi
| length4         = 2:33

| title5          = Sanjh Ki Bela
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Uma Devi, Moti Bai
| length5         = 3:07

| title6          = Mera Husn Lootne Aaya Albela
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6           = Zohra Ambala, Moti Bai
| length6         = 2:41
}}

==Marketing== name = advertisement}} With Chandralekha, Gemini became the first Tamil studio to attempt to distribute a film throughout India.    According to film scholar P. K. Nair, Chandralekha was the first Indian film to have a full-page newspaper advertisement.  According to a 2010 article in Mumbai Mirror by Vishwas Kulkarni,  574,500 was spent on newspaper publicity and  642,300 on posters, banners and billboards.  G. Dhananjayans book The Best of Tamil Cinema (2011) contradicts this, saying that Vasan spent close to  500,000 on publicity alone.  Chandralekhas publicity campaign was the most expensive for an Indian film at that time; the entire publicity budget of a typical Indian film a decade earlier was around  25,000. In the 1950s, the entire publicity for a "top Indian film" cost no more than  100,000, which is substantially less than the amount spent on Chandralekha.    According to Guy, the publicity campaign "made the nation sit up and take notice". 

A. K. Shekhar designed the publicity material, which included posters, booklets and full-page newspaper advertisements. Gemini Studios, inspired by American cinema, also produced a publicity brochure for distribution to exhibitors and the press.  It contained a synopsis of the film and a step pictorial account of the key points of the narrative. It provided text for use by local theatres. The booklet also had layouts for womens pages and a pictorial account of suggested marketing activities, such as "How to drape an Indian sari: Theatre demonstrations have a big draw", and information about the films costumes which were hand-woven garments of silk and gold; one gold-embroidered riding jacket is considered "the most expensive piece of outfitting ever used in a motion picture". 

==Reception==

===Release===
Chandralekhas première was held at the theatre Wellington Talkies in Chennai on 8 April 1948,  and it had a wide release the following day.    It was released simultaneously in 40 theatres throughout South India and in another 10 within a week.  In the 1940s, a typical Tamil film would be released in about 10 towns, but Chandralekha was released simultaneously in 120 towns. 
 Hindi film Aan, which was released in Tokyo in January 1954. NCC later collapsed; no information about how Chandralekha came to be released in Japan survives. During the 1950s, when India was short of foreign currency, barter was a common means of exchange with overseas business partners. Japanese scholar Tamaki Matsuoka believes this to have been the case with Chandralekha. The pamphlet prepared by NCC for the film calls Vasan the "Cecil B. DeMille of the Indian film industry".    A Danish version of the film titled Indiens hersker ("Indias ruler"), was released on 26 April 1954.  An abridged English-language version of Chandralekha, titled Chandra, was screened in the United States and Europe during the 1950s.    

The film was a bellwether for its filming, production costs and publicity before, during and after its release. Other producers delayed releasing their films until after Chandralekha s release to avoid competition. The films entertainment value made it popular with audiences;  however, it was unable to recover its production costs, and Vasan remade it in Hindi in order to do so after being advised by film distributor Tharachand.    The Hindi version was released the same year   The date is erroneous; the Tamil version was released in April 1948. Its initially low box-office collections prompted Vasan to make a Hindi version.}} with over 600 prints   other sources say it was released with 603 prints.  }} and became a large commercial success, setting box-office records.   Vasan called the film "a pageant for our peasants"  meant for "the war-weary public that had been forced to watch insipid war propaganda pictures for years".  It was selected by the government of India for exhibition at the Fourth International Film Festival in Prague in 1949.  The films success made Madras a major production centre for Hindi films.  Five years after the films success, Gemini paid its employees a bonus, becoming one of the first studios in the world to do so. 

===Box office=== Outlook India says, "Chandralekha grossed Rs 1.55 crore with an audience of 3 crore, 60% from rural India." 

===Critical response===

====India====
 
Chandralekha received generally positive reviews from Indian critics.  On 9 April 1948, The Hindu said, "India has not witnessed a film of this magnitude in terms of making and settings so far".  On 10 April, The Indian Express said, "Chandralekha is an entertaining film for everyone with elements like animals, rope dance, circus and comedy".  The same day, the Tamil newspaper Dinamani said, "Chandralekha is not only a first rate Tamil film but also an international film". 

  for an indigenous mass audience", calling its drum-dance sequence "perhaps one of the most spectacular sequences in Indian cinema".  In his 2009 book 50 Indian Film Classics, M. K. Raghavendra said, "Indian films are rarely constructed in a way that makes undistracted viewing essential to their enjoyment and Chandralekha is arranged as a series of distractions". He concluded by saying, "Chandralekha apparently shows us that enjoyment and visual pleasure in the Indian context are not synonymous with edge-of-the-seat excitement but must permit absent-mindedness as a viewing condition". 

In May 2010, Raja Sen of Rediff.com praised the films "grandly mounted setpieces", its "memorable drum dance" and the "longest swordfight ever captured on film", calling Chandralekha "just the kind of film, in fact, that would be best appreciated now after digital restoration".  In an October 2010 review of Chandralekha, Randor Guy praised Rajakumaris performance, calling it "her career-best" and saying that she "carried the movie on her shoulders". Guy praised Radha as his "usual impressive self", saying the film would be "remembered for: the excellent onscreen narration, the magnificent sets and the immortal drum dance sequence".  In his 2011 book The Best of Tamil Cinema, 1931 to 2010: 1931–1976, G. Dhananjayan called the film "a delight to watch even after 50 years",  though he criticised the script.  In 2013, director Dhanapal Padmanabhan told K. Jeshi of The Hindu, "Chandralekha had grandeur that was at par with Hollywood standards". 

====Overseas====
Chandralekha was well received by critics overseas. Reviewing the English version, The New York Times described Rajakumari as a "buxom beauty".  When Chandralekha was screened in New York City in 1976, William K. Everson said, "Its a colorful, naive and zestful film in which the overall ingenuousness quite disarms criticism of plot absurdity or such production shortcomings as the too-obvious studio "exteriors".   Last but far from least, Busby Berkeley would surely have been delighted to see his influence extending to the climactic drum dance."   
 Daily News Sri Lanka called it "the first colossal   film I saw".  In October 2013, Malaysian author D. Devika Bai, writing for New Straits Times, praised Chandralekha for its technical aspects; she said, "at almost 68, I have not tired of watching the movie". 

== Hindi version== Agha Jani Kashmiri wrote the dialogue only for the Hindi version.  Indra was a lyricist for the Hindi version with Bharat Vyas, while Kothamangalam Subbu and Papanasam Sivan were lyricists for the Tamil version.  Rajeswara Rao, who composed the soundtrack for both versions, was assisted by Bal Krishna Kalla on the Hindi version. Parthasarathy and Vaidyanathan composed background music for the Hindi version without Das Gupta. 

Rajakumari, Radha and Ranjan reprised their roles in the Hindi version, but their characters except for Rajakumaris character Chandralekha were renamed. Radhas character Veerasimhan was known as Veer Singh in the Hindi version, and Ranjans character Sasankan was renamed Shashank.  Of the other cast members, N. S. Krishnan, T. A. Madhuram, T. E. Krishnamachari, Pottai Krishnamoorthy and N. Seetharaman appeared only in the Tamil version, whereas Yashodhara Katju and H. K. Chopra appeared only in the Hindi version.  Nearly the entire cast were credited in the Tamil version, Chandralekha (Tamil) DVD. Opening titles, from 0:45 to 1:20  but only six people Rajakumari, Radha, Ranjan, Sundari Bai, Katju and L. Narayana Rao were credited in the Hindi version. 

==Legacy==

{{centered pull quote North to films made in the South India|South. This is the story of the making of that film, Chandralekha, 
 |author=Randor Guy, film historian and columnist 
}}
 making of Apoorva Sagodharargal (1949) is often considered an unofficial sequel to Chandralekha; it was also a major commercial success.  

Chandralekha enhanced Rajakumaris and Ranjans careers; both became popular throughout India after the films release.  Its climactic sword-fight scene was well received,  and is thought to be the longest sword fight in Indian cinema.  The drum-dance sequence is often considered the films highlight;   later producers tried unsuccessfully to emulate it.  Producer-director T. Rajendar said he drew inspiration from the drum-dance for a song sequence budgeted at   in his 1999 film Monisha En Monalisa.   Film historian Firoze Rangoonwalla ranked the Hindi version eighth in his list of "the top twenty films of Indian cinema".  Chandralekha was also a major influence on Kamalakara Kameswara Raos 1953 Telugu film Chandraharam, featuring N. T. Rama Rao.  On 26 August 2004, a postage stamp featuring Vasan and the drum dance was released to commemorate the 35th anniversary of his death. 

In July 2007, S. R. Ashok Kumar of The Hindu asked eight Tamil film directors to list their all-time favourite Tamil films; two of them J. Mahendran and K. Balachander named Chandralekha.  Mahendran said, "If anybody tries to remake this black and white film, they will make a mockery of it".    Balachander said, "Just like Sivaji (film)|Sivaji today, people talked about Chandralekha in the past. Produced at a cost of Rs 30 lakhs (a huge sum at that time), it has grand sets. I have seen it 12 times."  In December 2008, S. Muthiah said, "Given how spectacular it was and the appreciation lavished on it from 1948 till well into the 1950s, which is when I caught up with it Im sure that if re-released, it would do better at the box office then most Tamil films today".  In a 2011 interview with Indo-Asian News Service (IANS), South Indian Bollywood actor Vyjayanthimala said although people consider that she "paved the way" for other South Indian female actors in Hindi cinema, "the person who really opened the doors was S.S. Vasan". She said, "When   released, it took the North by storm because by then they havent seen that kind of lavish sets, costumes and splendour. So Vasan was the person who opened the door for Hindi films in the South." 

Chandralekha was K. Ramnoths last film for Gemini Studios. Although he is often credited with shooting the drum-dance sequence, Ramnoth left the studio in August 1947, before the sequence had been conceived.  Director Singeetham Srinivasa Rao told film critic Baradwaj Rangan he disliked Chandralekha when he first saw it, realising that it was a classic only after 25 years, "a fact that the audiences realised in just two minutes".  G. Dhananjayan told The Times of India, "When you talk of black and white films, you cannot resist mentioning the 1948 epic Chandralekha".  In April 2012, Rediff.com included the film in its list "The A to Z of Tamil Cinema" and said it "boasted an ensemble cast, great production values and a story that ensured it became a blockbuster all over India, the first of its kind".   
 National Film Archive.  In 2014, Chandralekha was one of eight Indian films  that were screened at the 28th edition of the Italian film festival Il Cinema Ritrovato, as a part of "The Golden 50s: Indias Endangered Classics" the first Indian cinema retrospective at the festival. 

==Notes==
 

==References==
{{reflist|25em|refs=
   
   
   
}}

==Bibliography==
 
*  

* 

*  

*  

*  

*  

*  

*  

*  

*  

*  

*  

* 

*  

*  

*  }}

*  

*  

*  

*  

*  
 

==External links==
*  
*  
*   at Bollywood Hungama

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 