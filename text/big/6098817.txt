Scram!
 
{{Infobox film name = Scram! image = ScramtitlecardLH.jpg director = Raymond McCarey producer = Hal Roach
  | writer = H.M. Walker starring = Stan Laurel   Oliver Hardy   Richard Cramer   Arthur Housman   Vivien Oakland   Wilson Benge
  | cinematography = George Stevens
  | editing = Richard C. Currier distributor = M-G-M released =  
  | runtime = 20 37"
  | language = English 
  | country = United States
}}
Scram! is a 1932 Laurel and Hardy film directed by Ray McCarey.

== Plot ==
Ordered out of town by angry Judge Beaumont, vagrants Stanley and Oliver meet a congenial drunk who invites them to stay at his luxurious mansion. The drunk cant find his key, but the boys find a way in, sending the surprised woman inside into a faint. They revive her with what they think is water, but is actually gin, and all get tipsy in the process. Outside, the drunk realizes hes at the wrong house and stumbles off. Eventually, the real homeowner arrives, none other than Judge Beaumont.

==Controversy==
* According to the book Laurel & Hardy Compleet by Dutch author and Laurel and Hardy specialist Thomas Leeflang, this film was banned in The Netherlands in 1932. Moral crusaders thought that the scene where Laurel & Hardy lie on a bed with a woman was indecent.

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 

 