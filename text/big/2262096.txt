The Groove Tube
{{Infobox film
| name           = The Groove Tube
| image          = The Groove Tube.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Ken Shapiro
| producer       = Ken Shapiro
| writer         = Ken Shapiro Lane Sarasohn Rich Allen
| starring       = Ken Shapiro Richard Belzer Chevy Chase
| cinematography = Bob Bailin
| editing        = Gary Youngman
| distributor    = Levitt-Pickman Film Corporation
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $200,000
| gross          = $20,447,000 
}} Independent comedy film written and produced by Ken Shapiro and starring Shapiro, Richard Belzer and Chevy Chase. It features the song Move On Up by Curtis Mayfield in its opening scene.

The low budget film|low-budget movie satirizes television and the counterculture of the early 1970s. The film was originally produced to be shown at the Channel One Theater on East 60th St. in New York, a venue that featured R-rated video recordings shown on three television sets, which was a novelty to the audiences of the time. The news desk satire, including the signature line "Good night, and have a pleasant tomorrow" was later used by Chase for his signature Weekend Update piece on Saturday Night Live, although in the film he does not appear in that segment.

== Plot ==
Among the skits are:
*"The Dealers," a lengthy feature about a pair of urban drug dealers (Shapiro and Belzer) introduced by a wildly overdone, hip title segment
*"Koko the Clown," a mock childrens television show in which Shapiro, as the shows Bozo-esque host, reads erotica (specifically a page from Fanny Hill, with promises of Marquis de Sade the next day) on air during "Make Believe Time"
*a public service announcement for venereal disease that covertly (though more and more obviously as the camera zooms in, to humorous and/or shocking effect) used a real penis
*a parody of sponsored television cooking shows in which Shapiro, as a female baker seen from the shoulders down, mixes and bakes a special 4th of July "Heritage Loaf" while repeatedly using handfuls of the fictitious "Kramp Easy Lube" brand of shortening, a spoof of the "Kraft" name
*Buzzy Linhart appears in the film as an (eventually) naked hitchhiker.  He also supervised the films soundtrack.

Several spoof TV commercials are featured, including a few for the fictional Uranus Corporation (pronounced with the stress on the second and third syllables). One Uranus commercial touts the amazing properties of its space-age polymer product "Brown 25" (which looks suspiciously like human feces): "It has the strength of steel, the flexibility of rubber, and the nutritional value of beef stew."

== Release ==
The Groove Tube was originally released with an X rating.  

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 