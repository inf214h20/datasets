Something Different (film)
{{infobox film
| name           = Something Different
| image          =
| imagesize      =
| caption        =
| director       = Roy William Neill Edward Brophy(asst director)
| producer       = Realart Pictures
| writer         = Alice Duer Miller (novel: Calderons Prisoner) Kathryn Stuart (scenario)
| starring       = Constance Binney
| music          =
| cinematography = Harry Leslie Keepers Oliver T. Marsh
| editing        =
| distributor    = Realart Pictures
| released       = December 1920
| runtime        = 5 or 6 reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1920 passport photos to enter Cuba for this production are found at Flickr. 

Leo Tover, then 19 years old, later became a well known cinematographer. He took a passport photo to go to Cuba with other members of the films company. It is unclear what Tovers role in the production was.

==Plot==
As summarized in a film publication,    Alice Lea (Binney), an heiress reaching the end of her funds, was being forced into a marriage for money, so she decided to seek adventure in Central American and visit a friend. She learned that Don Luis (Wilbur), husband of her friend, was hostile to the government and secretly planning to overthrow it. At the government ball Alice met Don Mariano, head of the army. During the ball she witnessed Mariano kill a spy and hated him for his cruelty. Later Alice is taken prisoner by Mariano on suspicion that she is aiding Don Luis in his revolt. Alice was made very comfortable in Don Marianos home and, while she would not admit it, she was falling in love with her captor. Mariano was also falling in love with his captive, so much so that managed her release and also promised that of her friends husband Don Luis, who had been captured and likely faced death. Alice returned to her home, and was soon followed by Mariano, who had been exiled for allowing the prisoners to escape. He told Alice of his love, and of course they lived happily thereafter.

==Cast==
*Constance Binney - Alice Lea
*Lucy Fox - Rosa Vargas
*Ward Crane - Don Mariano Calderon
*Crane Wilbur - Don Luis Vargas
*Gertrude Hillman - Calderons Housekeeper
*Mark Smith - Richard Bidgley
*Grace Studdiford - Mrs. Evans (*as Grace Studiford)
*Riley Hatch - Mr. Stimson (*as William Riley Hatch)
*Adolph Milar - Spy (*as Adolph Millar)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 