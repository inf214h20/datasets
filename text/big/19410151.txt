Glove Taps
{{Infobox film
| name           = Glove Taps
| image          = Glove taps.JPEG
| image size     =
| caption        = Gordon Douglas
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       =
| music          = Marvin Hatley
| cinematography = Art Lloyd
| editing        = William H. Ziegler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10:28
| country        = United States
| language       = English
| budget         =
}}
 short comedy Gordon Douglas. It was the 151st Our Gang short (152nd episode, 63rd talking short, and 64th talking episode) that was released.

==Plot==
Butch explains that he clobbers every kid in school to prove that he is in charge. By a fluke, weak-kneed Alfalfa is chosen to face Butch in the barnyard boxing ring—and he has only one day to train for the big bout.

==Notes==
*After appearing as a peripheral player in several earlier Our Gang shorts, Tommy Bond made a spectacular return to the series in Glove Taps. Here and in all future appearances, Bond is cast as neighborhood bully Butch, the bane of the existence of Spanky McFarland, Carl "Alfalfa" Switzer and the rest of the Gang.
*Another Our Ganger debuts as Glove Taps Marks the first appearance of Darwood Kaye. Way Out West.   

==Cast==
===The Gang=== Eugene Lee as Porky
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat

===Additional cast===
* Tommy Bond as Butch
* Sidney Kibrick as Woim
* Darla Hood as Darla
* Darwood Kaye as Waldo

===Extra kids===
Hugh Chapman, John Collum, Rex Downing, Larry Harris, Joe Levine, Jackie Lindquist, Donald Proffitt, Hugh Sheridan, Harold Switzer, Jerry Tucker, Bobs Watson, Robert Winkler

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 