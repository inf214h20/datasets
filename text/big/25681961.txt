Dead Men Don't Make Shadows
 
{{Infobox film
| name           = Dead Men Dont Make Shadows
| image          = Djangosartana.jpg
| caption        = Wild East DVD Cover
| director       = Demofilo Fidani (as Miles Deem)
| producer       = Demofilo Fidani
| writer         = Demofilo Fidani Franco Mannocchi 
| starring       = Jack Betts (as Hunt Powers) Coriolano Gori (as Lallo Gori)
| cinematography = Joe DAmato Technicolor, Techniscope
| editing        = Piera Bruni
| distributor    =
| released       = 1970
| runtime        = 83 minutes
| country        = Italy Italian
| box office gross  =
| followed by    =
}}

Dead Men Dont Make Shadows, aka Stranger That Kneels Beside the Shadow of a Corpse (in original Italian: Inginocchiati straniero... I cadaveri non fanno ombra!) is a 1970 spaghetti western directed by Demofilo Fidani.

==Story==
A bounty hunter finds himself caught between an outlaw and an evil mine owner.

==Cast==
*Jack Betts as Hunt Powers - Lazar Peacock/Sabata
*Franco Borelli as Chet Davis - Blonde/Stranger
*Charles Pendleton as Gordon Mitchell - Roger Murdock
*Ettore Manni - Barrett/Billy Ring
*Benito Pacific as Dennis Colt - Medina
*Simone Vitelli as Simone Blondell - Maya
*Amerigo Leoni as Custer Gail - Medina henchman
*Maria Vitelli as Mary Ross - Jole
*Pietro Fumelli - Ted Stanley
*Attilio Dottesio as Dean Reese - Sanchez
*Manlio Salvatori, Eugenio Galadini, Mario Capuccio, Giglio Gigli, Aristide Massaccesi as Arizona Massachusetts

==Releases==
Wild East released a limited edition R0 NTSC DVD double feature with One Damned Day at Dawn...Django Meets Sartana!.

==External links==
* 

==References==
 

 
 
 
 
 


 
 