Eito Prem
{{Infobox film
| name            = Eito Prem
| image           = Eito Prem poster.jpg
| caption         = first look
| director        = Shohel Arman
| producer        = Shaheen Kabir
| writer          = Shohel Arman Bindu Amit Hasan Shaduzammin Selim
| music           = Songs: Habib Wahid Background score: Adith
| released        =   
| country         = Bangladesh Bengali
}} romance and Bindu as the lead pair of the film along with the Ramondo Mojomdar, Shaduzammin Selim, Afroaz Bano, Amit Hasan and Hasan Imam.The films score and soundtrack were composed by Habib Wahid. The film based on romantic love story and Liberation War of Bangladesh. 

==Plot==
A love story DURING the war of 1971. 

==Cast==
* Shakib Khan as Surjo Bindu as Madhbi
* Shohiuzzaman Seleem
* Ramendu Majumdar
* Afroaz Bano
* Syad Hasan Imam 
* Amit Hasan
* Nipun
* Masum Aziz
* Shiraz Haydar
* Khurshiduzzaman Uthpol   
* Shahanur 
* Shamol Jakaria
* Binoy Voddro
* Sarwar
* Kohinoor
* Sazzad Reza
* Pran Ray
* Sayeed Babu 
* Prithu

==Soundtrack==
{{Infobox album
| Name =  Eito Prem
| Type = soundtrack
| Cover = 
| Artist = Habib Wahid
| Released = 
| Genre = Film soundtrack
| Producer = view media
}}
The film score of the film as well as the soundtrack was scored by Habib Wahid. The soundtrack, featuring 7 tracks overall,  The lyrics were written by Shohel Arman.

The album was both critically acclaimed and gained popularity upon its release.

===Track listing===
{{Track listing
| extra_column = Artist(s)
| title1 = Moner Bhetor
| extra1 = Habib Wahid & Nancy
| title2 = Jotsna Debo
| extra2 = Nancy
| title3 = Jak Na Ure
| extra3 = Milon Mahmud
| title4 = Modhuboner Phool
| extra4 = Doli Shyantor
| title5 = Hridoye Amar Bangladesh
| extra5 = Habib Wahid, Arfin Rumey
| title6 = Hridoye Amar Bangladesh(slow)
| extra6 = Habib Wahid, Arfin Rumey, Predeep Kumar
| title7 = Moner Bhetor(slow)
| extra7 = Habib Wahid & Nancy
}}

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 