I'll Give My Life
{{Infobox film
| name           = Ill Give My Life
| image          =
| image_size     =
| caption        =
| director       =  
| producer       = Pierre Couderc (associate producer) Sam Hersh (producer) S. Roy Luby (associate producer)
| writer         = Herbert Moulton
| narrator       =
| starring       = See below
| music          =
| cinematography = Walter Strenge
| editing        = Robert Fritch
| distributor    =
| released       = 3 February 1960
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Ill Give My Life is a 1960 American film directed by William F. Claxton. The film is also known as The Unfinished Task in the USA.

== Cast == Ray Collins as John Bradford
*John Bryant  as James W. Bradford
*Angie Dickinson as Alice Greenway Bradford
*Katherine Warren as Dora Bradford Donald Woods as Pastor Goodwin
*Jon Shepodd as Bob Conners Stuart Randall as Rex Barton
*Richard Benedict as Cpl. Burr
*Sam Flint as Roy Calhoun
*Ivan Triesault as Dr. Neuman
*Jimmy Baird as Jimmy Bradford
*Mimi Gibson as Jodie Bradford
*Milton Woods as Kopa, Medical Orderly
*Virginia Wave as Miss Lane
*Vera Frances as Tem Lane, Secretary

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 