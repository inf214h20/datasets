Ariane's Thread
{{Infobox film
| name           = Arianes Thread
| image          = 
| caption        =	
| director       = Robert Guédiguian
| producer       = Marc Bordure Robert Guédiguian 
| writer      = Robert Guédiguian Serge Valletti 
| starring       = Ariane Ascaride Jacques Boudet Jean-Pierre Darroussin Anaïs Demoustier Gérard Meylan
| music          = Eduardo Makaroff Christoph Müller
| cinematography = Pierre Milon 		 
| editing        = Armelle Mahé Bernard Sasia 
| studio         = Agat Films & Cie Chaocorp
| distributor    = Diaphana Films
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Arianes Thread ( ) is a 2014 French comedy-drama film directed by Robert Guédiguian and written by Guédiguian and Serge Valletti. 

== Cast ==
* Ariane Ascaride as Ariane 
* Jacques Boudet as Jack
* Jean-Pierre Darroussin as Taxi Driver / Stage Director 
* Anaïs Demoustier as Martine / Actress 
* Youssouf Djaoro as Night Watchman 
* Adrien Jolivet as Raphaël
* Gérard Meylan as Denis 
* Lola Naymark as Lola 
* Judith Magre as The Turtle (voice) 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 