Friday the 13th Part VI: Jason Lives
 
{{Infobox film
| name           = Friday the 13th Part VI:   Jason Lives
| image          = friday6.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Tom McLoughlin
| producer       = Don Behrns
| writer         = Tom McLoughlin
| starring       = Thom Mathews Jennifer Cooke David Kagen Renée Jones Kerry Noonan Darcy DeMoss Tom Fridley C.J. Graham
| music          = Harry Manfredini
| cinematography = Jon Kranhouse
| editing        = Bruce Green
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States English
| budget         = $3 million  (estimated) 
| gross          = $19.4 million  (domestic) 
}}
 metahumor and action film elements including shootouts and car chases.   

The film was a critical success, it was the first film in the series to receive favorable reviews since the original. In the years since its release, its self-referential humor and numerous instances of  .

==Plot==
Years have passed since Crystal Lake had been renamed to Forest Green, but Tommy Jarvis, still suffering from hallucinations returns with a friend of his from the hospital; Allen Hawes, hoping to cremate Jasons body and stop his hallucinations. At the cemetery, they dig up Jasons corpse, but seeing it causes Tommy to snap and he stabs Jasons body with a metal fence post. Turning his back on Jason, two lightning bolts strike the post and revive the now-undead killer. Tommy tries to light the gasoline he soaked Jason in, but a sudden downpour prevents it. When Hawes tries to attack Jason with a shovel, his heart is punched out. Tommy runs and makes his way to the sheriffs office, where his panic and attempt to grab weapons gets him put into a jail cell. His pleas that Jason has returned goes unheeded by Sheriff Garris. On the road, two young adults, Darren and Lizbeth get lost looking for the camp and run into Jason, they fail to frighten Jason off the road and he impales them both with the metal rod that resurrected him. The next morning, Garris daughter Megan and her friends Sissy, Cort and Lizbeths sister Paula ask him to search for Darren and Lizbeth. Tommy warns them about Jason, but as he is now considered an urban legend they ignore the warnings, though Megan finds herself attracted to him.

In the woods, Jason happens upon a corporate paintball game, he kills four of them for their equipment and chases the helpless, clumsy Roy off into the woods. At Camp Forest Green, the kids arrive, and the teens do their best to run the camp without Darren and Lizbeth. Meanwhile, Garris decides to escort Tommy out of his jurisdiction due to his influence on his daughter, Tommy tries to make a run for Jasons grave, but finds that the caretaker had covered it up to deny responsibility for it being dug up, and Hawes body is buried in its place. Tommy is then escorted out of town. Cort goes out to have sex with a girl named Nikki, who borrowed an RV from her step-father, when the power is pulled suddenly, they vacate the area, unaware that Jason had snuck on board. He crushes Nikkis skull then kills Cort by ramming a bowie knife into his skull. He then happens upon the caretaker and stabs his throat out with his own whiskey bottle, then kills a nearby couple who happened to witness the murder. The sheriffs men discover several dead bodies and Garris immediately implicates Tommy in the murders, meanwhile Tommy has contacted Megan and convinced her to help him exorcise Jason back into the lake he drowned as a child. Jason makes his way to the camp and frightens one of the children, Nancy. Paula and Sissy show her theres nothing to fear and send her to bed, when Paula falls asleep, Sissy is grabbed through their cabin window and her head is twisted clean off. Nancy again sees Paula and shows her a bloodied machete. Convinced Sissy and Cort are playing pranks on each other, they go out to find them.

Meanwhile, Megan and Tommy are pulled over by Garris, and despite her alibi she was with him, he does not believe Tommy to be innocent and they go out to the camp to investigate. Tommy and Megan develop a ruse to trick the deputy and they both escape the station together. At the camp, Jason slaughters Paula after she put Nancy to bed and the police arrive to investigate, discovering the carnage, Jason quickly takes out both of Garris men. When Megan arrives, her calls to her father alert Jason who tries to go after her. Garris tries to stop him and despite taking Jason to the ground and landing blows to his face with a nearby boulder, he is bent in half backwards and is killed. Jason is about to kill Megan, but Tommy calls to him from the lake, and he goes after him instead. Tommy is attacked in the boat in the middle of the lake, and ties a boulder around Jasons neck, but he fights back, holding Tommy underwater long enough to drown him. Megan rushes out to save him and is nearly killed herself when Jason grabs her leg, she turns the boats motor into Jasons neck and he releases her. She takes Tommy back to shore and uses CPR to revive him. Tommy laments that its finally over, and that Jason is home.

Under the water, tied in a prison at the bottom of the lake, Jason stares patiently off into the water, glaring at the audience as the screen fades to black.

==Cast==
* C. J. Graham/Dan Bradley as Jason Voorhees
* Thom Mathews as Tommy Jarvis Megan Garris
* David Kagen as Sheriff Mike Garris Renee Jones as Sissy Baker
* Kerry Noonan as Paula
* Darcy DeMoss as Nikki
* Tom Fridley as Cort
* Alan Blumenfeld as Larry
* Matthew Faison as Stan
* Ann Ryerson as Katie
* Tony Goldwyn as Darren
* Nancy McLoughlin as Lizbeth
* Ron Palillo as Allen Hawes
* Vincent Guastaferro as Deputy Rick Cologne
* Michael Swan as Officer Pappas
* Courtney Vickery as Nancy
* Whitney Rydbeck as Roy
* Bob Larkin as Martin
* Wallace Merck as Burt
* Justin Nowell as Billy
* Roger Rose as Steven
* Michael Nomad as Officer Thornton
* Cynthia Kania as Annette
* Temi Epstein as Little Girl
* Justin Nowell as Billy
* Taras OHar as Little Boy
* Tommy Nowell as Tyen
* Sheri Levinsky as Bus Monitor

==Production==

===Pre-production and writing===
Although the previous film in the series, Friday the 13th: A New Beginning, had been a financial success, it had disappointed the series fans and received some of the worst reviews of any film in the franchise. In order to prevent further alienating the fans (and thus potentially endangering the series), the producers decided to take the series in a new direction, moving it away from what producer Frank Mancuso Jr. called the "coarse" nature of A New Beginning. 

To this end, Mancuso hired Tom McLoughlin, who had directed the successful horror film One Dark Night but was also known around Hollywood for shopping around various comedy scripts he had written, a dichotomy that appealed to Mancuso. McLoughlin was given free rein on how he would present the story, with the only condition being that he bring back Jason and make him the films villain. 
 retcon the events of the fifth film in order to circumvent that films cliffhanger ending, which implicated that protagonist Tommy Jarvis had become a serial killer.
 postmodern Meta-joke|metahumor; when Jason is first encountered in the woods near Crystal Lake, the character of Lizbeth comments that she and Darren should flee because she knows about proper conduct to survive a horror movie. McLoughlin would further satirize the franchise itself, as Martin the gravedigger comments on Jasons exhumation, "Whyd they have to go and dig up Jason?" before breaking the fourth wall and addressing the camera with the observation, "Some folks sure got a strange idea of entertainment." In addition to Frankenstein, McLoughlin also cited as inspiration his love of gothic horror, particularly the works of Edgar Allan Poe, and his Catholic upbringing; Jason Lives features the series only explicit references to God, and during the climax a praying girl is spared by Jason (a similar scene, in which the same girl prays for Tommy while Megan performs CPR, then mouths "Thank you" while looking skyward was deleted from the final cut of the movie, apparently against McLoughlins wishes; he recalled in the 2009 DVDs directors commentary, "Somehow it didnt stay in... probably too much sentiment").  

===Casting===
The decision to retcon the events of Part V resulted in many members of that films cast—whose characters had survived—having their contracts to return for a sequel terminated. At one point in time when Jason Lives was being considered as a direct sequel to A New Beginning rather than to The Final Chapter, the surviving characters Pam and Reggie from A New Beginning were to have died in the films opening moments.   

Although Mancuso retained control over the films casting, he deferred to McLoughlins judgment, with the only caveat being that the final girl had to be a "very attractive blonde".  To fulfill this requirement, McLoughlin chose Jennifer Cooke, based on her performance in the television series V (1984 TV series)|V. The role of Hawes, Tommys would-be sidekick who dies within the first five minutes of the movie, was given to another television veteran, Ron Palillo, famous for the role of Horshack on Welcome Back, Kotter.
 evangelical Christian who had reservations about returning to the series based on the atmosphere surrounding A New Beginning, was initially attracted to Jason Lives based on the scene in which a praying girl is spared by Jason. He ultimately decided to film the movie Caught, and shortly thereafter retired from acting to go to seminary. Thom Matthews, who would take over the mantel of Tommy Jarvis, was chosen for his work in the horror comedy Return of the Living Dead, although McLoughlin himself was unaware of Matthews horror credentials until after shooting began.  Other cast members were culled from actors whom McLoughlin had directed before (such as David Kagen, who was also an acting teacher for female lead Jennifer Cooke) and McLoughlins own family—Jasons first female victim in the film, Lizbeth, was played by McLoughlins wife, Nancy. In keeping with the series tradition, the role of Jason was given to a stuntman, Dan Bradley.

===Filming===
After the first day of filming, Mancuso decided that he disliked Bradleys appearance onscreen as Jason. Although the scenes that Bradley filmed—in which Jason kills the paintball playing executives—were kept in the completed picture, the rest of Jasons scenes were performed by C. J. Graham, an area restaurant manager and former soldier. As part of a stage show put on at the restaurant, a magician would hypnotize audience members and place them in a scenario during which they encountered Jason Voorhees; Graham, who stood 63 and weighed 250&nbsp;lbs, was asked to play Jason for the scenario. Jason Lives  special effects coordinator, Martin Becker, was in the audience for one such show, and recommended Graham to Mancuso and McLoughlin. Both men were impressed with Grahams presence, and he was hired to film the remainder of Jasons scenes.

Jason Lives would become notable for being the only film in the franchise to contain no nudity; the characters in the films sole sex scene are both fully clothed, a conscious move on McLoughlins part to distance the series from the notion that the Friday the 13th films were morality tales in which premarital sex was punished by death. McLoughlin was pressured by the films producers to have Darcy Demoss remove her shirt during the RV sex scene, but he only suggested the idea to Demoss, who refused.
 Smokey the Bear signs asking everyone to "Keep the Forests Green".

Some of the climactic moments of the film involving the primary characters in the lake were actually filmed in the swimming pool of McLoughlins father. Although McLoughlin ruined the pools filter in the process (it was jammed by gore churned into the water when Jason is hit with the boat propeller), McLoughlins father was pleased that he could now boast a Hollywood film had been shot in his backyard. 

===Post-production===
McLoughlins attempt to deliver a "different" kind of Friday the 13th film were met with skepticism from the producers. In a contrast to the series other entries, which had to be edited for violence in order to avoid an "X" rating, the films producers requested that McLoughlin add additional gore, violence, and murders to the film. The original cut of the film contained 13 killings as an in-joke; in order to appease the studio, McLoughlin had to add an additional three killings, bringing the total up to 16.  These were the killings of Martin the gravedigger, and the recently engaged couple on a nighttime picnic. The scene of Jason killing Martin would later be cited by McLoughlin as one of his favorite parts of the movie for the shot in which the picnicing man suddenly realizes that hes been spotted by Jason, which McLoughlin felt to be the films scariest moment. 

Additionally, McLoughlin was made to extend Sissys death, adding the shots of Jason dragging her to the ground and twisting her head off; as originally filmed, Sissy was simply pulled out of the cabin window, and wasnt seen again until Megan finds her head in the squad car. 

McLoughlin also found himself in contention with the producers over how the film should end. As scripted, the movie was supposed to have concluded in the graveyard, with Martin the gravedigger (who apparently wasnt killed by Jason) meeting Jasons father, Elias—a heretofore unseen character in the series—with the implication that Elias knows Jason has been resurrected and has come looking for him. The studio balked at the scene, as they did not want the responsibility of having to introduce Elias backstory in the next installment in the franchise; additionally, the added murder of Martin made the scene an impossibility to shoot. This ending would have tied up a continuity error from A New Beginning, when it is mentioned that Jason was cremated; a deleted scene from Jason Lives had Tommy asking Sheriff Garris why Jason wasnt cremated, as had been planned, at which point Garris informs him that someone paid the city to bury Jason; Elias handing Martin a wad of money would have indicated that he was the man who paid for Jasons burial. The scene was later storyboarded for inclusion on the films "Deluxe Edition" DVD release, with Bob Larkin reprising his role as Martin to provide voiceover. Elias, like Jason, was scripted to be completely silent.  

McLoughlin ultimately shot three endings, two of which, against his expectations, were not included on the films DVD release. In one ending, Jasons mask floats to the surface of Crystal Lake, having become detached during his struggle with Megan. In another, Deputy Cologne was seen trying to reach the jail cell keys after having been locked in by Tommy and Megan; the door to the police station opens and the film abruptly ends, indicating that Jason had managed to get free. The producers disliked both of these endings, as each one left Jasons survival ambiguous, and wanted it explicitly shown onscreen that he was still capable of returning for a sequel. As a result, McLoughlin shot the films current ending, showing a closeup of Jasons open, twitching eye. 

===Music===
The films music was composed by Harry Manfredini, who composed the scores to all of the series previous installments. In addition to the original score, the soundtrack also featured: Constrictor
* "Im No Animal" by Felony (band)|Felony, from their album Vigilante Constrictor
* "Hard Rock Summer" by Alice Cooper, from the box set The Life and Crimes of Alice Cooper

"Hes Back (The Man Behind the Mask)" had an accompanying music video, combining clips from the film with new footage featuring Cooper. It is not present on any home video release of the film, but can be viewed on YouTube.  

On January 13, 2012, La-La Land Records released a limited edition 6-CD boxset containing Manfredinis scores from the first six Friday the 13th films. It sold out in less than 24 hours. 

==Reception==

===Box office===
  The Fly, Psycho III, Deadly Friend, The Texas Chainsaw Massacre 2, Maximum Overdrive and Trick or Treat. Ultimately, it would go on to gross a total of $19.4 million at the U.S. box office, ranking at number 46 on the list of the years top earners.

===Critical response===
 
Popular with critics overall, Jason Lives received favorable reviews, the first time since the original Friday the 13th that an entry in the series received anything other than a negative review. The film was praised for having more of a plot than other films in the franchise, as well as its self-referential humor, which has been considered by some to be an inspiration for the style of Scream (1996 film)|Scream, as well as other horror films of the 1990s. Jeffrey M. Anderson called it "the best of the Friday the 13th series, mainly because of the winking, insider humor" and added that it was "like a precursor to Wes Cravens Scream ten years later". Gerry Shamray called it "the only good "Friday". James Kendrick called it "a funny, largely enjoyable near-spoof of slasher films in general and the Friday the 13th series in particular".

Fan reception was largely positive; as of the release of Freddy vs. Jason in 2003, it was considered a fan favorite in the franchise.   This is largely attributed to the use of humor,   though some were put off by this approach. 

==Novelization== Elias Voorhees, Jasons father who was originally meant to appear in the film, but was cut. The book also includes various flashbacks to Jasons childhood and the backstories of characters such as Tommy and Sheriff Garris are also expanded.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 