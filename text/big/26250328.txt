La visita
 
{{Infobox film
| name           = La visita
| image          = 
| caption        = 
| director       = Antonio Pietrangeli
| producer       = Moris Ergas
| writer         = Gino De Santis Ettore Scola Ruggero Maccari Antonio Pietrangeli
| starring       = Sandra Milo
| music          = Armando Trovajoli
| cinematography = Armando Nannuzzi
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

La visita is a 1963 Italian comedy film directed by Antonio Pietrangeli. It was entered into the 14th Berlin International Film Festival.   

==Cast==
* Sandra Milo - Pina
* François Périer - Adolfo Di Palma (as François Perier)
* Mario Adorf - Cucaracha
* Gastone Moschin - Renato Gusso
* Angela Minervini - Chiaretta
* Ettore Baraldi
* Giancarlo Bellagamba
* Bruno Benatti
* Paola Del Bon
* Ferdinando Gerra
* Abele Reggiani
* Carla Vivian
* Didi Perego - Nella

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 