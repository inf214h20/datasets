Don Q, Son of Zorro
 
{{Infobox film
| name           = Don Q, Son of Zorro
| image          = Don Q Son of Zorro - film poster.jpg
| caption        = Theatrical poster
| director       = Donald Crisp
| producer       = Douglas Fairbanks
| writer         = Jack Cunningham Lotta Woods
| based on       =  
| starring       = Douglas Fairbanks
| music          = Mortimer Wilson
| cinematography = Henry Sharp
| editing        = William Nolan
| distributor    = United Artists
| released       =  
| runtime        = 111 min.
| country        = United States
| language       = Silent film English intertitles
}} The Mark of Zorro. It was loosely based upon the 1909 novel Don Q.s Love Story, written by the mother-and-son duo Kate and Hesketh Hesketh-Prichard.    The story was reworked in 1925 (after Hesketh Hesketh-Prichards death) into a vehicle for the Johnston McCulley character Zorro. The film adaptation was made by screenwriters Jack Cunningham and Lotta Woods for United Artists studios.  Douglas Fairbanks both produced the film and starred as its lead character.   

The film was well-received: the New York Times rated it one of its top ten movies of 1925. 

==Cast==
*Douglas Fairbanks - Don Cesar de Vega/ Zorro
*Mary Astor - Dolores de Muro
*Jack McDonald - Gen. de Muro
*Donald Crisp - Don Sebastian
*Stella De Lanti - the Queen
*Warner Oland - The Archduke
*Jean Hersholt - Don Fabrique Borusta
*Albert MacQuarrie - Colonel Maatsado
*Lottie Pickford - Lola(billed Lottie Pickdord Forrest) Charles Stevens - Robledo
*Tote Du Crow - Bernardo
*Martha Franklin - The Duenna
*Juliette Belanger - The Danger
*Roy Coulson - Dancers Admirer
*Enrique Acosta - Ramon

==Plot==
 
Don Diego de la Vega (Zorro)s son, Cesar (Douglas Fairbanks), is in Spain finishing his education. While Cesar is showing off to friends his remarkable prowess with the whip, he accidentally clips off the feather shako on the hat of Don Sebastian (Donald Crisp) of the Palace Guard. Although Cesar apologizes immediately, Sebastian is unforgiving. Their duel is interrupted by a runaway bull. Trapped on the ground with his sword belt tangled in his boot, certain to be gored by the bull, Sebastian is saved at the last minute by Cesar. This further infuriates him. The action is observed by Queen Isabella (Stella De Lanti) and her guest, Austrian Archduke Paul (Warner Oland); she requests Cesars company immediately. Another friend of Cesar, Don Fabrique Borusta (Jean Hersholt), offers to bring him to Her Majesty.
 Jack McDonald), as she poses for a sculptor. It is love at first sight. But Sebastian, who comes from a poor family, has set his sights on Dolores and her familys wealth, and is determined to win her. Later, the Archduke invites Cesar to paint the town, with Sebastian as their "duenna." In a local tavern the Archduke offends the patrons, all seeming ruffians, by flirting with the dancer. Sebastian contrives his and the Dukes escape, but locks Cesar in the tavern to defend himself against the cutthroats. In the carriage that takes them away from what he is sure will be Cesars death, Sebastian declares he has a meeting with Dolores. The Archduke invites himself along. While Sebastian asks the General for his daughters hand, the Archduke sees Dolores serenaded by Cesar, who escaped (easily) and even acquired a guitar as a souvenir. Seeing the reactions of the young couple, the Archduke knows Cesar has won Doloress heart.

Although penniless, Don Fabrique has designs on succeeding in society. He glues together a discarded invitation to the Archduke’s Grand Ball, and crashes the party. At the ball, Cesar and Sebastian sit on either side of Dolores, both seeming frustrated in their efforts to woo her. The Archduke summons her to him. When Cesar sees the Archduke caress Doloress cheek, Cesar becomes jealous and goes to confront him. But the Archduke assures him that he is working in Cesars favor, and proves it by dragging Sebastian to another room to play cards while Cesar and Dolores dance together. Cesar pulls Dolores to a balcony for ardent lovemaking. Fabrique sees them; when the pair are interrupted by Dolores’s father, General de Muro, who recognizes Cesar and is ready to give his blessing, Fabrique believes they are about to be betrothed.

In the card room, the Archduke declares that Sebastian is as unlucky at cards as he is in love. Franque tiptoes in, and tells the Archduke that he saw Cesar and Dolores kissing: surely they will be married now. The Archduke summons Cesar to congratulate him, to the horror of Sebastian. When he enters, Cesar is offended at the impropriety of this news, and learns that the source was Fabrique. Such bad manners should not go unpunished. He informs the Archduke that someone here doesnt belong, and asks if he should remove him. Archduke Paul nods, and Cesar pulls Fabrique out of the room by tugging his nose.

The Archduke continues to taunt Sebastian, a foolish move when Sebastian, enraged by jealousy, pulls his sword and stabs the Archduke before he realizes what he has done. He hides when Cesar, hearing something, enters, then strikes Cesar unconscious. He frames him for the Archdukes murder, then casually leaves. With his last dying energy, the Archduke pulls a playing card off the table and writes on it: Sebastian assassinated me. Archduke Paul.

Fabrique enters, finds Cesar unconscious, finds the playing card and, miffed at Cesars insult, takes it. Shortly thereafter he confronts Sebastian with his demands: to be appointed Civil Governor. Both stand by while the Guard arrests Cesar for the murder and orders his immediate execution to prevent an international incident. But General de Muro offers Cesar a gentleman’s way out by giving him a dagger. Cesar pretends to stab himself and falls to the moat below the castle.

Months pass, while Cesar hides in the ruins of the old family castle. He pretends to be Don Q, for "a trick must be answered by a trick!" Fabrique has become Civil Governor, receiving regular pay-offs from Sebastian. Fabrique has even taken over Carlos servants, and maidservant Lola (Lottie Pickford), seeing how Sebastian behaves around Fabrique, runs to tell Cesar that although gossip says they are close friends, in truth Sebastian is afraid of Fabrique. This will prove the leverage Cesar needs to establish his innocence.
 Charles Stevens) for information on Cesars whereabouts, then convinces Fabrique to accompany him to the ruins  where Cesar has been living these past months. There he is determined to find what hold Fabrique has on Sebastian.
 The Mark of Zorro) has sailed from California to Spain to help. On the way to the ruins they pass Dolores and her mother along the same road. Finally, as all gather at the ruins, Zorro and Don Q battle the soldiers, Fabrique confesses, Sebastian is beaten, de Muro recognizes his old friend, the villains are arrested, and Cesar and Dolores reunited.

== References ==
 

==External links==
* 
* 
* 
*  Part 1, available for free download from  
*  Part 2, available for free download from  


 
 

 
 
 
 
 
 
 
 