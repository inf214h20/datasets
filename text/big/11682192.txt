Contraband (1980 film)
 
{{Infobox film
| name           = Contraband
| image          = Contraband-1980-poster.jpg
| alt            = 
| caption        = Italian theatrical poster
| film name      =  
| director       = Lucio Fulci
| producer       = Sandro Infascelli Curti, 2013. p. 277 
| writers        = {{plainlist|
*Ettore Sanzò
*Gianni De Chiara
*Lucio Fulci	
*Giorgio Mariuzzo Curti, 2013. p. 276 
}}
| screenplay     = 
| story          = {{plainlist|
*Ettore Sanzò
*Gianni De Chiara   }}
| starring       = {{plainlist|
*Fabio Testi
*Marcel Bozzuffi}}
| music          = Fabio Frizzi 
| cinematography = Sergio Salvati 
| editing        = 
| studio         = CMR International 
| distributor    = Cidif 
| released       =  
| runtime        = 96 minutes
| country        = Italy 
| language       = 
| budget         = 
| gross          = ₤756 million
}} 1980 poliziotteschi film directed by Lucio Fulci. The film is set in Naples, where Luca Di Angelo (Fabio Testi)  and his brother Michele use speedboats to smuggle cigarettes and find themselves between two contraband bosses after they lose a load of cigarettes.

The films story was changed to include additional scenes of violence and to better pace the plot. On the second week of a ten week shooting schedule, the films ran out of money and received funding by actual smugglers in Naples. The smugglers also made changes to the plot and title of the film.

==Plot==
Luca Di Angelo (Fabio Testi) is a smuggler, one member of an organized team trafficking cigarettes and booze up and down the coast off Naples, Italy. After a run-in with the police in which the smugglers manage to get away by faking a boat explosion resulting in the police motorboats responding to the false emergency allowing the smugglers to get away, Luca and his brother Mickey suspect Scherino (Ferdinand Murolo), the head of a rival gang of smugglers, of passing on their actives. Lucia and Mickey take their accusations to their boss Perlante (Saverio Marconi) a sleazy playboy withy numerous Mafia connections, who agrees to look into it. After a nighttime fire at Mickeys racing stables kills a valued racehorse, he and Luca drive over to inspect the damage. But on the way, they are stopped at a fake police roadblock where the assassins dressed as policemen trick Mickey into getting out of the car and machine-gun him to death over and over again (a homage to Sonny Corelones death scene in The Godfather), while Luca barely escapes injury by hiding on the floor of the car.

Afterwards, Perlante suggests that Luca leave town for a few days, but he refuses. After his brothers funeral, conducted on the gangs speedboats in the Bay of Naples, with the police surveying them, Luca vows revenge. Despite his wife Adeles (Ivana Monti) pleas, Luca goes after the prime suspect: Scherino. That night, Luca breaks into Scherinos house, but gets spotted and severely beaten up by Scherinos henchmen. However, Scherino spares Lucas life. He tells Luca that he had no part in Mickeys killing.

After Luca recovers from his injuries thanks to a local doctor named Charlie (Giordano Falzoni) who treats injuries for large bribes of cash, Luca meets with an informant who gives him a tip to who ordered the hit on Mickey. Traveling to a derelict fishing boat in the marina where a hood is making a drug pick-up, Luca tortures him for information about his boss, whom Luca learns is a Frenchman called Francois Jacios, aka: The Marsigliese. Luca calls Perlante, who tells him more about the vicious gangster, and who is muscling into Italian organized crime to deal in hard drugs. At his hideout in Naples, the Marsigliese (Marcel Bozzufi) is meeting Ingrid, a German drug courier from Frankfurt wanting to sell him some heroin. When the Marsigliese sees that the heroin is cut, he has her face horribly burned by a blowtorch while he watches with sadistic satisfaction.

Over the course of one day, the Marsigliese orders a series of shootings of all the rival Mafia Dons all over Naples as part of his plan to become the sole kingpin of Naples. Perlante barely escapes an attempt on his life when his right-hand man Alfredo (Giulio Farnese) triggers a bomb which has been hidden under Perlantes bed, killing Alfredo and Perlantes mistress. Perlante calls Luca and tells him about the series of hits. He sets up a meeting between them and the Marsigliese at the local soccer stadium where the Frenchman discusses merging their criminal concerns. Afterwards, Luca meets with his fellow smugglers and persuades them not to accept the Marsigliese demands for the inflow of drugs into their community would only escalate the number of addicts and drug-overdoses, plus they would not receive any profits since the Marsigliese would keep most of the money for himself and his close associates.

In response to the Mafia killings, the Naples police chief (Fabio Jovine) orders Captain Tarantino (Venantino Verantini) to conduct a massive sweep of the Neapolitan bay area to clean it up of crime. The dragnet has many smugglers arrested. Luca is saved from a police raid on his house by, of all people, Scherino, who suggests they form an alliance to defeat the Marsiglise. They meet that night at Perlantes house to discuss their plans with him. But Luca soon smells the tell-tale odor of the Marsiglieses personal parfum in the room. Luca realizes that Perlante is in league with the Marsgliese just as the gangster and his henchmen burst into the room and kill all of Scherinos henchmen as well as mortally wound Scherino himself. Lucas split-second reflexes of diving out a glass window and running away from the house ensures his escape. The mortally wounded Scherino manages to shoot off one shot from his gun at the treaterous Perlante, hitting him in the neck, before he drops dead himself.

The Marsigliese abducts Lucas wife, Adale, and again insists that Luca should turn over the smuggling network over to his drug operation. To help Luca make up his mind, the sound of Adale being beaten and gang-raped are relayed to Luca over the phone. Luca agrees to the Marsigliese demands. In desperation, Luca calls upon the elderly Don Morrone (Guido Alberti), the leader of the old-guard Italian Mafia who has been reading the news throughout the movie of the numerous killings. Morrone is happy to come out of semi-retirement to deal with the French sadist. Morrone relays his plans to his various middle-aged associates who swing back into action for their cause.

The following morning, a meeting between Luca and the Marsigliese in a local open square where the handover to Adele is taking place. Luca sees that it is indeed a set up to have him killed. Don Morrone and his men, using a series of hit-and-run attacks, appear and blast away all of the Marsiglieses henchmen. Luca then chases the crazed Marsigliese through the deserted streets and allyways where after the Frenchman runs down an alley which is a dead end, Luca catches up to him and shoots the Marsigliese dead who lands on a pile of garbage bags. Across down, the police raid the Marsigliese hideout where they find the tramatized Adele and a large stash of cocaine and heroin, while the rest of the Marsigliese henchmen surrender.

The final scene has Captain Tarantino meeting with Don Morrone and his housekeeper at a wharf marketplace where the policeman thanks the elderly Mafia don for his tip leading to the discovery of the Marsigliese hideout and drug shipment seizure. But when Tarantino asks Morrone about the murder of the Marsigliese and his men, Morrone claims to know nothing about it, and also not to know Luca Di Angelo. From Tarantinos sarcastic tone of voice, he knows that Morrone is lying. But out of sympathy, the policeman lets Morrone go without arresting him.

==Production==
Lucio Fulci and Giorgio Mariuzzo co-wrote the script for Contraband based on an original story by Ettore Sanzò and Gianni De Chiara with a working title of Il contrabbandiere / Violenza.   Mariuzzo has stated the story initially started with producer Sandro Infascelli, but found the story did not work and called in Mariuzzo to re-shape it.  Mariuzzo re-structured the script to change its pace and added a more scenes of violence.  

Filming began on Contraband on December 3, 1979.   The film was shot in 11 weeks within the De Paolis Studios in Rome and in Naples.  Within the first two weeks of filming at De Paolis Studios the production had run out of money.  Moving the production to Naples, local smugglers lent themselves to the production of the film as extras and let the crew use their motorboats for some scenes.  The smugglers also changed parts of the script by including lines against drug trafficking and to drop the term Violenza (Violence) from the title.  Among the smugglers was Giuseppe Grecco, the son of Sicilian Mafia member Michele Greco.  Giuseppe Grecco would ask Fulci technical questions about filming and would later direct films in the 1990s.  Filming ended in March 1980.  

==Cast==
*Fabio Testi: Luca Ajello
*Ivana Monti: Adele Ajello
*Guido Alberti: Don Morrone
*Venantino Venantini: Tarantino
*Ajita Wilson: Luisa, "la napoletana"
*Marcel Bozzuffi: Il Marsigliese
*Saverio Marconi: Luigi Perlante
*Daniele Dublino: assistente procuratore 
*Ofelia Meyer: Ingrid
*Ferdinando Murolo: Sciarrino
*Tommaso Palladino: Capece 
*Luciano Rossi: uomo del Marsigliese 
*Nello Pazzafini: killer
*Lucio Fulci: vecchio camorrista

==Style==
Contraband was director Lucio Fulcis only crime film.  The film is a poliziesco, Contraband also adopts the form of a splatter film that would be appearing in other Italian horror films of the era.  

==Release==
Contraband was released in August 8, 1980 in Italy.  The film grossed a total of 756,203,700 Italian lira on its domestic release.  

===Home Media===
The film is now released region-free, complete and UNCUT by Shameless Films   . The Shameless DVD comes with a special animated lenticular movement card   (DVD-dimension) and can be obtained on the publishers web site    as well as Amazon and good video stores.
A single for the films theme song was released as a seven-inch single by Cinevox.  The single is credited to Cricket and contains the songs "You Are Not the Same" and "New York Dash".  On July 27, 2004, Contraband was released by Blue Underground in the United States on DVD. 

==Notes==
 

===References===
*  

==See also==
* List of Italian films of 1980
* List of crime films of the 1980s

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 