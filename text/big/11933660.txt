Andrew and Jeremy Get Married
 
{{Infobox Film
| name = Andrew and Jeremy Get Married
| image = AndrewAndJeremyGetMarried.jpg
| image_size =
| caption = DVD cover
| director = Don Boyd
| producer = FirstSight Films   Clare Boyd Don Boyd
| writer = Don Boyd
| narrator =
| starring =
| music =
| cinematography = Don Boyd   Kate Boyd
| editing = Kate Spankie
| distributor = Tartan Films (UK) Gavin Films (Worldwide)
| released = 9 September 2004 (Canada) 6 May 2005 (UK)
| runtime = 75 mins.
| country = United Kingdom
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
}} commitment ceremony. BBC Storyville series, the film premiered at the 2004 Toronto International Film Festival. {{Citation
  | last = Bradshaw
  | first = Peter
  | author-link = Peter Bradshaw
  | title = Andrew and Jeremy Get Married
  | newspaper = The Guardian
  | pages = 
  | year = 
  | date = 6 May 2005
  | url =http://www.guardian.co.uk/theguardian/2005/may/06/8
  | accessdate = 13 June 2009 }}
  {{Citation
  | last = Boyd
  | first = Don
  | author-link = Don Boyd
  | title = When a man loves a man...
  | newspaper = The Observer
  | pages = 
  | year = 
  | date = 5 September 2004
  | url = http://www.guardian.co.uk/film/2004/sep/05/features.review1
  | accessdate = 13 June 2009}}
 

==People featured==
* Andrew Thomas
* Jeremy Trafford
* Hanif Kureishi

==Nominations== British Independent Film Award for best British documentary.

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 
 
 
 
 
 


 
 