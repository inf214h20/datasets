Ritu (film)
{{Infobox film
| name = Ritu
| image = ritu_poster.jpg
| caption = Seasons change. Do we?
| director = Shyamaprasad
| producer = Vachan Shetty
| writer = Joshua Newtonn Nishan Rima Kallingal Jaya Menon Vinay Forrt Rahul Raj
| cinematography = Shamdat
| editing = Vinod Sukumaran
| distributor = Playhouse Release
| released =  
| runtime = 120 minutes Malayalam 
| country = India
| budget =  
| gross =  
}}

Ritu  ( ) is a 2009 Malayalam film directed by Shyamaprasad, whose previous film Ore Kadal was highly acclaimed. The original screenplay is written by Joshua Newtonn. This is the first time Shyamaprasad has chosen to film an original screenplay. All of his previous films were adaptations of published novels or plays. 

==Plot==
Ritu is about  young people working in the IT sector. The story follows the lives of three childhood friends: Sunny Immatty (Asif Ali), Sarath Varma (Nishan K. P. Nanaiah), Varsha John (Rima Kallingal). The trio were almost inseparable as friends; they grew up in the same neighborhood in their love, bonding and innocence. They shared a dream of journeying together in life forever.

Sarath leaves for the U.S. to help his brother-in-law with his business. He also aspires to write a book some day. But still he clings on to the memories of the good old days with his friends and wishes to reunite and work with them again. So, after three years, he comes back to India and starts a small company with Varsha and Sunny to initiate an ambitious new project. But he soon finds out that their strong bond of friendship has almost vanished: Varsha and Sunnys lifestyles and mindsets have changed over the years. Varsha flirts with men over the phone while, Sunny is interested in only earning money by hook or crook. Whether their friendship will return or deteriorate even more forms the crux of the story.

In life, a time arrives when you face the ultimate question: Who are you? The three friends finally arrive at that moment when the question stares at them. Did the truth that they found help them?

==Cast==
*Asif Ali as Sunny Immatty
*Nishan K. P. Nanaiah as Sarath Varma
*Rima Kallingal as Varsha John
*K. Govindankutty as Rama Varma (Saraths father)
*M. G. Sasi as Hari Varma (Saraths Brother)
*Jaya Menon as Zarina Balu
*Prakash Menon as Balu
*Manu Jose as Jithu
*Siddharth as Pranchi
*Kalamandhalam Radhika as Saraths mother
*Vinay Forrt as Jamal
*Shyamnath Ravindranath

==Soundtrack==
 
{{Infobox album|  
| Name        = Ritu
| Type        = Soundtrack Rahul Raj
| Cover       =
| Released    = 15 July 2009|
| Recorded    = Sound Of Music, Chennai, India|
| Genre       = Soundtrack|
| Length      = 34 Minutes|
| Label       = Satyam Audios Rahul Raj
| Reviews     =
| Last album  = Crazy Gopalan   (2009)
| This album  = Ritu   (2009)
| Next album  = Chekavar (film)|Chekavar   (2010)
}}
 Kerala State Film Award for Best Original Background score, surpassing Ilaiyaraaja|Ilaiyaraajas acclaimed score for Pazhassi Raja (film)|Pazhassiraja.

{| class="wikitable"
|-
! Song title !! Singers !! Length !! Description
|- Rahul Raj|| 4:47 || A nostalgic song picturised on the lead trio.
|-
| "Pularumo" || Suchith Suresan, Gayatri Asokan || 5:53 || A romantic duet ballad picturised on the lead pair.
|- Smitha Nishant || 4:30 || An electropop song with English lyrics, picturised in a pub.
|-
| "Ku Ku Ku Theevandi"  || Jeetu || 4:03 || A party solo song with a lot of acoustic guitar.
|-
|"Chanchalam/Cindrella"  || Neha Nair, Job || 4:55 ||
|}

==Release and reception==
After four years of releasing of the movie, and an avalanche of new-gen films, critics have begun to acclaim Ritu as the pioneer of new wave in Malayalam cinema. Upon the release, Ritu received generally positive reviews. Nowrunning.com sees Ritu as a sensitive, melancholic portrayal of tumultuous emotions that ravage three young minds basking on the pinnacle of success. "It is an engrossing, intricate slice-of-life that deftly captures the nuances of growing up together and finally growing apart." Rediff and Indiaglitz reviewers termed it as a worthwhile experiment and said that it represents the changing face of Malayalam cinema. 

The acting skills of the three lead actors have been applauded: the newcomers did their part quite convincingly. Indiaglitz review said "Its high time for Mollywood film goers to change and  settle down from the regular potboilers and applaud films like Ritu which has its heart exactly at the right place". Rediff said "Ritu is a worthwhile experiment by seasoned director Shyamaprasad with a predominantly new team, and it does not disappoint".

The Telugu version of Ritu, titled New, released under two banners:  Bhargava Pictures and Innostorm entertainment.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 