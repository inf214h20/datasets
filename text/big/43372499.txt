Lai Bhaari
 
 
{{Infobox film
| name           = Lai Bhaari (2014 Movie)
| film name = 
लय भारी
| native_name_lang = mr
| image          = Lai-Bhaari-Marathi-Movie-Poster.jpg
| alt            = Lai Bhaari film poster
| caption        = Film poster 
| director       = Nishikant Kamat
| producer       = Genelia Deshmukh   Jeetendra Thackeray   Ameya Khopkar
| story          = Sajid Nadiadwala Sanjay Pawar  (dialogue) 
| screenplay     = Ritesh Shah
| starring       = Riteish Deshmukh Sharad Kelkar Uday Tikekar  Tanvi Azmi  Radhika Apte  Aaditi Pohankar
| music          = Songs: Ajay−Atul Background Score: Sameer Fatarpekar
| cinematography = Sanjay Memane
| editing        = Aarif Sheikh
| studio        = Mumbai Film Company   Cinemantra Production
| distributor  = Zee Talkies   Essel Vision
| released       =  
| country        = India Marathi
| budget         =   
| gross          =   (lifetime)   
}}
 Marathi film directed by Nishikant Kamat. The film marks the debut of Riteish Deshmukh in Marathi cinema, while Salman Khan and Genelia DSouza also feature in cameo appearances.   The film went on to become the highest grossing Marathi film of all time. 

==Plot==
Pratap Singh Nimbalkar (Uday Tikekar) and his wife Sumitra Devi (Tanvi Azmi) are known for their social work like helping poor farmers by giving them land and shelter. Even though they are blessed by everyone, Sumitra Devi is insulted for not having a child even though she has been married for many years. Her maid suggests her to pray to Lord Vithoba (generally known as Vitthala by most common people of Maharashtra) in Pandharpur, a holy place in Maharashtra. Out of eagerness, Sumitra Devi promises to give her first son to Lord Vitthala.

As fate would have it, she gets pregnant soon and confirms the good news to Pratap Singh. But when she tells him that she has promised Lord Vithhala to give him her first child, Pratap Singh, who is modern in thought, says all this is ridiculous and flies off to London. After some days the baby is born, and Sumitra Devi calls up Pratap Singh and tells him that she is now convinced of keeping the baby. He comes back as soon as possible and names the baby Abhay Singh, or Prince, as a nickname.

25 years later, Prince (Riteish Deshmukh) comes back home after studying abroad. On the other hand, there is Princes paternal cousin Sangram (Sharad Kelkar) is a crooked guy, who is trying to take all farms from the farmers by torturing them. On hearing this, Pratap Singh warns him for doing so. A few days later, Pratap Singh is killed and it is implied that Sangram was behind his death.

Seeing Prince as the only obstacle left in his plan of owning all the farms in the village, Sangram subsequently kills Prince and takes over all the property that belonged to Pratap Singh. Seeing no way left, Sumitra Devi goes to Pandharpur and angrily prays to Lord Vitthala to give back her son. Right outside the temple, Mauli (Riteish Deshmukh), a lookalike of Prince, beats up some goons who were eve teasing.

In a surprising twist, it is revealed that 25 years ago, Sumirta Devi had given birth to twins, one of which she gave to Lord Vitthala. And this son is none other than Mauli, a rowdy, as opposed to the gentleman Prince. How Mauli takes revenge from Sangram forms the crux of the story.

==Cast==
* Riteish Deshmukh as Mauli and Abhay Singh Nimbalkar Prince
* Sharad Kelkar as Sangram, Maulis first cousin
* Radhika Apte as Kavita, Maulis love interest
* Tanvi Azmi as Sumitra Devi, Mauli and Princes mother
* Uday Tikekar as Pratap Singh Nimbalkar, Mauli and Princes father
* Sanjay Khapare as Sakha, Mauli and Princes friend
* Aaditi Pohankar as Nandhini, Princes love interest 
* Salman Khan as Bhau (Cameo appearance)
* Genelia Dsouza (Cameo appearance in song "Aala Holicha)

==Production==
With a budget of  , Lai Bhaari  is one of the most expensive Marathi films till date.  It has been produced by Jeetendra Thackeray, Ameya Khopkar and Genelia Deshmukh under Cinemantra Production and Mumbai Film Company production banner and it is presented by Zee Talkies and Essel Vision.

This movie marks the third instalment of Ritesh Deshmukh in Marathi after his produced two successful Marathi films Balak-Palak and Yellow (2014 film).

==Soundtrack==
{{Infobox album 
| Name = Lai Bhaari
| Type = Soundtrack
| Artist = Ajay-Atul
| Cover =
| Released = June 2014
| Genre = Film soundtrack
| Length = 25:31 
| Label = Video Palace
| Producer = 
| Last album = Fandry Theme Song  (2014)
| This album = Lai Bhaari (2014)
| Next album = "PK (film)"  (2014)
}}

The lyrics for the film are penned by Guru Thakur & Ajay-Atul with music composed by the Ajay-Atul. The song Mauli Mauli sung by Ajay Gogavale became popular. 

===Track listing===
{{track  listing
| headline = Lai Bhaari
| extra_column = Singer(s)
| total_length = 25:31
| collapsed = no

| title1 = Mauli Mauli
| extra1 = Ajay Gogavale 
| length1 = 5:05
| title2 = Aala Holicha San 
| extra2 = Swapnil Bandodkar & Yogita Godbole 
| length2 = 5:21
| title3 = Jeev Bhulala 
| extra3 = Sonu Nigam, Shreya Ghoshal
| length3 = 4:43
| title4 = New Nava Tarana 
| extra4 = Kunal Ganjawala 
| length4 = 4:50
| title5 = Ye Na Saajna 
| extra5 = Shreya Ghoshal 
| length5 = 5:32
}}

==Reception==
The film has received highly positive reviews. Rediff gave 4 out of 5 stars and declared movie awesome.  Bollywoodlife and Koimoi have rated the movie  3 stars out of 5 .Times of India gave the movie a rating of 3.5/5.  It has been acclaimed by various Bollywood celebrities. 

==Box office== Timepass as the highest grossing Marathi film ever. http://zeenews.india.com/entertainment/regional/lai-bhaari-riteish-deshmukh-delivers-highest-grossing-marathi-film_159531.html 

==References==
 

==External links==
* 

 
 