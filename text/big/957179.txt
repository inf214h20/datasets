Bright Young Things (film)
 
 
{{Infobox film name           = Bright Young Things image          = Bright Young Things.jpg image_size     = alt            = caption        = Original poster director       = Stephen Fry producer       = Gina Carter Miranda Davis writer         = Stephen Fry Based on a novel by Evelyn Waugh starring       = Emily Mortimer Stephen Campbell Moore Fenella Woolgar Michael Sheen James McAvoy Dan Aykroyd Jim Broadbent Peter OToole music          = Anne Dudley cinematography = Henry Braham editing        = Alex Mackie studio         = distributor    = Film Four released       = 3 October 2003 runtime        = 106&nbsp;minutes country        = United Kingdom language       = English budget         = gross          = $2,682,600 
}}
Bright Young Things is a 2003 British drama film written and directed by   final film before his death in 2005.

==Plot== tabloid newspaper magnate Lord Monomark, is confiscated by HM customs officers at the port of Dover for being too racy, he finds himself in a precarious financial situation that may force him to postpone his marriage. In the lounge of the hotel where he lives, he wins £1000 by successfully performing a trick involving sleight of hand, and the Major offers to place the money on the decidedly ill-favored Indian Runner in a forthcoming horse racing|horserace. Anxious to wed Nina, Adam agrees, and the horse wins at odds of 33–1, but it takes him more than a decade to collect his winnings.
 mental institution; paparazzo who chronicles the wicked ways of the young and reckless; and Ginger Littlejohn, Ninas former beau, who ingratiates himself back into her life, much to Adams dismay. The pastimes of the young, idle rich are disrupted with the onset of World War II, which eventually overtakes their lives in often devastating ways.

==Cast==
 
*James McAvoy — Simon Balcairn
*Michael Sheen — Miles
*Emily Mortimer — Nina Blount
*Stephen Campbell Moore — Adam Fenwick-Symes
*Stockard Channing — Mrs. Melrose Ape
*Fenella Woolgar — Agatha Runcible
*Dan Aykroyd — Lord Monomark
*Julia McKenzie — Lottie Crump
*David Tennant — Ginger Littlejohn
*Jim Broadbent — The Major
*Peter OToole — Colonel Blount
*Simon Callow — King of Anatolia
*Imelda Staunton — Lady Brown Bill Paterson — Sir James Brown Guy Henry - Archie
*Simon McBurney — Sneath
*Richard E. Grant — Father Rothschild
*John Mills — Gentleman
*Harriet Walter - Lady Metroland
*Margaret Tyzack - Lady Throbbing
*Angela Thorne - Kitty Jim Carter — Customs Officer
*Stephen Fry - Chauffeur
*Nigel Planer - Taxi Driver
*Paul Popplewell – Private
 

==Production==
  directorial debut of actor Stephen Fry. Fry also makes a very brief cameo appearance in the film as a chauffeur. The assistant director was Jo Crocker, Stephen Frys sister who made her debut in television.

The film proved to be the last for John Mills, who appears briefly in the non-speaking role of an elderly party guest enthralled by the effects of cocaine.
 Lord Beaverbrook, who once employed Evelyn Waugh as a writer for his newspaper, the Sunday Express. Waughs original name for his character was "Lord Ottercreek", before his lawyers intervened.  Monomark, like Beaverbrook, a Canadian, is played by Dan Aykroyd, a Canadian.

Exteriors were shot at various locations in and around London, including the Old Royal Naval College in Greenwich and Eltham Palace. Interiors were filmed in Pinewood Studios.
 standards of the era, including "Nina," "Twentieth Century Blues," "Dance, Little Lady," and "The Partys Over Now," all performed by Noël Coward, "Mairzy Doats" by The Merry Macs, and "Hear My Song, Violetta" by Victor Silvester and His Orchestra.

The film premiered at the Cannes Film Festival in May 2003, and was shown at the Toronto Film Festival before its Royal European Charity Premiere in London on 28 September 2003. It went into theatrical release in the UK on 3 October 2003, the same day it was shown at the Dinard Festival of British Cinema in France.
 US Comedy Arts Festival in Aspen, the Cleveland International Film Festival, the Philadelphia International Film Festival, the Newport International Film Festival and the Provincetown International Film Festival before going into limited release on 20 August. It eventually grossed $931,755 in the US and £869,053 in the UK. 

==Critical reception==
 
A.O. Scott of the New York Times said, "Mr. Fry revels in the chaos of the plot, and the profusion of arch one-liners and zany set pieces gives the picture a hectic, slightly out-of-control feel. Sometimes you lose track of who is who, and where the various characters are going—but then, so do they. Subplots and tangents wander into view and then fade away, and in the end it all comes together and makes sense, more or less…Period dramas set on the eve of World War II are a Dime (United States coin)|dime—or maybe a shilling—a dozen, but what distinguishes this one is its dash and vigor. It does not seem to have been made just for the sake of the costumes and the vintage cars. The camera, rather than composing the action into a presentable pageant, plunges in, capturing the madness of the era in a swirl of colors and jolting close-ups. And Mr. Frys headlong style helps rescue the movie from the deadly trap of antiquarianism". 

Roger Ebert of the Chicago Sun-Times said the film has "a sweetness and tenderness" and observed that Stephen Fry was "the obvious choice to direct this material." He added, "He has a feel for it; to spend a little time talking with him is to hear inherited echoes from characters just like those in the story. He supplies a roll-call of supporting actors who turn up just long enough to convince us entire movies could be made about their characters". 

Carla Meyer of the San Francisco Chronicle called the film a "witty, energetic adaptation" but thought "Fry, so deft with lighthearted moments, seems uncomfortable with Waughs moralizing, and more serious scenes fall flat". She added, "Bright Young Things is like a party girl on her fourth martini. What had been fun and frothy turns irretrievably maudlin". 

Peter Travers of Rolling Stone felt Fry was "clever" for adapting Waughs novel "into a movie that would make Paris Hilton feel at home," although "By the time   lets darkness encroach on these bright young things…the fizz is gone, and so is any reason to make us give a damn". 

Derek Elley of Variety (magazine)|Variety called the film "a slick, no-nonsense adaptation…an easy-to-digest slice of literate entertainment for upscale and older auds that lacks a significant emotional undertow to make it a truly involving—rather than simply voyeuristic—experience…Frys script fillets out even the few traces of a darker underside that creep through in the second half of Waughs original. Modern auds, accustomed to more emotional payback for the characters earlier excesses, will come away empty-handed. Theres basically very little dramatic arc to the whole picture. Still, Fry and his tech team have put together a good-looking, smooth-running movie". 

Michael Wilmington of the Chicago Tribune described it as "a brilliant, giddy satiric romp with a discreetly moralistic viewpoint beneath its high-style wit," "a ball to watch," and "an incredibly entertaining film with a magnificent cast," and called Fry "a splendid director capable of visual dazzle and superb ensemble work". 

==Awards and nominations== London Film Empire Award British Independent Film Award for Most Promising Newcomer.

Stephen Fry was nominated for the Emden Film Award at International Filmfest Emden, and the production was nominated for the Empire Award for Best British Film.

==References==
 
 
 
==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 