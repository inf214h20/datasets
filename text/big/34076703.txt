Mila (film)
{{Infobox film
| name           = Mila
| image          = 
| director       = Joel Lamangan
| producer       = Charo Santos-Concio Malou Santos Elma Medua
| based on       = The life story of Anita Pamintuan.
| story          = 
| screenplay     = Ricardo Lee
| starring       = Maricel Soriano Piolo Pascual Princess Punzalan Cherry Pie Picache Kaye Abad Serena Dalrymple Jiro Manio Angelica Panganiban Luis Alandy
| music          = Jessie Lasaten
| cinematography = Monino Duque
| editing        = Tara Illenberger
| distributor    = Star Cinema
| released       =  
| runtime        = 116 minutes
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = 
}}
 2001 Cinema Filipino drama film featuring an ensemble cast starring Maricel Soriano, Piolo Pascual and Princess Punzalan. The film was directed by the acclaimed director Joel Lamangan with the screenplay written by Ricardo Lee and released by Star Cinema as a part of their 8th year anniversary. The film was based on the life story of Anita Pamintuan who died during her fight for proper wages and compensation for public school-teachers in the Philippines. 

==Cast and Characters==
*Maricel Soriano as Anita Pamintuan "Mila Cabangon"
*Piolo Pascual as Milas live-in-partner Primo
*Princess Punzalan as Milas best-friend and co-teacher Linda 
*Cherry Pie Picache as Rona
*Kaye Abad as Winowa
*Serena Dalrymple as Jenny
*Jiro Manio as Peklat
*Angelica Panganiban as Lani
*Luis Alandy as Ruel
*Noni Buencamino as Nato
*Eva Darren as Milas mother
*B.J. De Jesus as Boyet
*Kathleen Hermosa as Teresa
*Mel Kimura as Lucille
*Alfred Labatos as Momoy
*Don Laurel as Ronnel
*Tony Mabesa as Mr. De Castro
*Mario Magallona as Rambo
*Bea Nicolas as Belay
*Tom Olivar as Torres
*Jim Pebanco as Noli
*Caridad Sanchez
*Florencio A. Pili as Milas co-teacher

===Cameo===
*Noel Cabangon as himself

==Production==

 

==Release==
The film premiered on the Philippines on June 21, 2001. It had a Philippine television premiere on Cinema One and a world premiere on Cinemax.

==Recognitions==

===FAMAS=== 2002 FAMAS Awards.

===Gawad Urian=== 2002 Gawad Urian Awards.

==References==
 

==External links==
 

 
 

 