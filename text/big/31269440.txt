1911 (film)
{{Infobox film
| name = 1911
| film name = {{Film name| traditional = 辛亥革命
| simplified = 辛亥革命
| pinyin = Xīnhài Gémìng
| jyutping = San1 Hoi6 Gaap3 Ming6}}
| image = 1911_filmposter.jpg
| alt = 
| caption = Theatrical release poster
| director = Jackie Chan Zhang Li
| producer = Wang Zhebin Wang Tinyun Bi Shulin
| writer = Wang Xingdong Chen Baoguang
| starring = Jackie Chan Winston Chao Li Bingbing
| music = Ding Wei
| cinematography = Zhang Li Huang Wei
| editing = Yang Hongyu Media Asia Films Huaxia Film Distribution Media Asia Distributions   Huaxia Film Distribution East Film & TV Distribution   
| released =  
| runtime = 125 minutes
| country = China Hong Kong
| language = Mandarin English
| budget = US$30 million 
| gross = 
}}
1911, also known as Xinhai Revolution and The 1911 Revolution, is a 2011 Chinese historical drama film.  The film is a tribute to the 100th anniversary of the Xinhai Revolution. It is also Jackie Chans 100th film in his career.  Besides starring in it, Chan is also the executive producer and co-director of the film. Co-stars include Chans son Jaycee Chan, Li Bingbing, Winston Chao, Joan Chen and Hu Ge. This film was selected to open the 24th Tokyo International Film Festival. 

==Plot== deaths of Provisional Republic of China, the abdication of the last Qing dynasty emperor Puyi on 12 February 1912, and Yuan Shikai becoming the new provisional president in Beijing on 10 March 1912.

==Cast==
 

* Jackie Chan as Huang Xing
* Winston Chao as Sun Yat-sen
* Li Bingbing as Xu Zonghan
* Sun Chun as Yuan Shikai
* Jaycee Chan as Zhang Zhenwu
* Hu Ge as Lin Juemin
* Yu Shaoqun as Wang Jingwei
* Joan Chen as Empress Dowager Longyu
* Huang Zhizhong as Situ Meitang
* Jiang Wu as Li Yuanhong
* Ning Jing as Qiu Jin
* Jiang Wenli as Soong Ching-ling
* Mei Ting as Chen Yiying
* Xing Jiadong as Song Jiaoren Yikuang
* Hu Ming as Liao Zhongkai
* Iva Law as Consort Jin
* Huo Qing as Tan Renfeng
* Qi Dao as Wu Zhaolin
* Dennis To as Xiong Bingkun
* Tao Zeru as Tang Weiyong
* Wang Ziwen as Tang Manrou
* Ye Daying as Wu Tingfang
* Chen Yiheng as Xu Shichang
* Duobujie as Feng Guozhang
* Zhang Zhijian as Lin Sen
* Xie Gang as Tang Shaoyi
* Liu Zitian as Hu Hanmin
* Sun Jingji as Yu Peilun
* Michael Lacidonia as Homer Lea
* Gao Bin as Cai Yuanpei
* Wang Wang as Chen Qimei
* Zhao Yaodong as Zhang Taiyan
* Jia Hongwei as Jiang Yiwu
* Su Hanye as Puyi
* Nan Kai as Yu Zhaolong
* Tong Jun as Xiaodezhang
* Jiang Jing as Yuan Shikais concubine
* Wang Weiwei as Yuan Shikais concubine
* Wang Luyao as Yuan Shikais concubine John Jordan
* He Xiang as Fang Shengdong
* Lan Haoyu as Lin Shishuang
* Xu Ning as Chen Gengxin
* Wei Xiaojun as Red Cross Society leader
* Qin Xuan as Red Cross Society vice leader
* He Qiang as Ju Zheng
* Ma Yan as Liu Chengen
* Wang Yanan as Yuan Keding
* Zhang Xiaolin as Tieliang
* Lü Yang as Liangbi
* Tan Zengwei as Puwei
* Jack as Yubeier Zaifeng
* Liu Guohua as Qing assassin
* Wang Kan as Ruicheng
* Xu Wenguang as Zhang Mingqi
* Zuo Zhaohe as Zheng Kun
* Wang Jingfeng as Tao Qisheng
* James Lee Guy as American representative
* Maxiu as British representative
* Canwu as German representative
* Duluye as French representative
* Attarian as French representative

 

==Production==
Production started on 29 September 2010 in Fuxin, Liaoning, where a camera rolling ceremony was held.  After half a year of intense production, it wrapped up on 20 March 2011 in Sanya, Hainan. 

==Release==
The film was released on 23 September 2011 in China  and on 29 September 2011 in Hong Kong.  It opened the 24th Tokyo International Film Festival on 22 October 2011.  It was released in its original version in North American theatres on 7 October 2011.

==Reception==
1911 received generally negative reviews; it currently holds a 9% "rotten" rating on Rotten Tomatoes.  On Metacritic, which uses an average of critics reviews, it holds 37/100, indicating "generally unfavorable" reviews,  e.g. on the Opionator. 

The Economist noted that while the film was endorsed by the Chinese government officials, ticket sales have been poor. It also noted that the film avoided sensitive topics, such as the reforms which led to the revolution. 

==See also==
*Towards the Republic
*1911 Revolution (TV series)|1911 Revolution (TV series)
*72 Heroes

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   
}}

==External links==
*   (Asia)
*   (English)
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 