Peer Gynt (1934 film)
Peer German drama film directed by Fritz Wendhausen and starring Hans Albers, Lucie Höflich and Marieluise Claudius.  It is based on the play Peer Gynt by Henrik Ibsen. It was one of the most expensive productions made by Bavaria Film and involved location shooting in Norway. 

==Cast==
* Hans Albers - Peer Gynt 
* Lucie Höflich - Mutter Aase 
* Marieluise Claudius - Solveig  Ellen Frank - Ingrid 
* Olga Chekhova - Baronin 
* Lizzi Waldmüller - Tatjana 
* Zehra Achmed - Anitra 
* Richard Ryen - Gunarson 
* Hans Schultze - Schmied Aslak 
* F. W. Schröder-Schrom - Vater Solveigs 
* Leopoldine Sangora - Mutter Solveigs 
* Friedrich Kayßler - Kiensley 
* Otto Wernicke - Parker 
* Fritz Odemar - Silvan 
* Alfred Döderlein - Mats Moen 
* Mina Höcker-Behrens - Frau Rink  Philipp Veit - Landstreicher 
* Magda Lena - Eine Bäuerin 
* Armand Zäpfel - Kapitän 
* Willem Holsboer - John Bless 
* O. E. Hasse - Steuermann 
* Viktor Bell - Diener Ben

==References==
 

==Bibliography==
* Kosta, Barbara. Willing Seduction: The Blue Angel, Marlene Dietrich and Mass Culture. Berghahn Books, 2009.

==External links==
* 

 
 
 
 
 
 
 
 


 