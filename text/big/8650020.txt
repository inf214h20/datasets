South Pacific (2001 film)
 
{{Infobox television film|
  name          = South Pacific |
  image         = South Pacific.jpg|
  caption       = DVD cover| Richard Pearce|
  writer        = Oscar Hammerstein II Joshua Logan James A. Michener Lawrence D. Cohen|
  starring      = Glenn Close Harry Connick, Jr. Rade Sherbedgia Ilene Graff Natalie Mendoza|
  producer      = Christine A. Sacani|
  music         = Richard Rodgers Michael Small|
  cinematography= Stephen F. Windon| Buena Vista|
  released      = March 26, 2001|
  network      = American Broadcasting Company |
  runtime       = 135 minutes|
  country       = United States|
  language      = English |
   budget       = $15 million|
}} South Pacific. ABC production Richard Pearce, and starred Glenn Close, Harry Connick, Jr. and Rade Šerbedžija (billed in U.S. as Rade Sherbedgia). It was broadcast in 2001 and also released on DVD.

==Production==
South Pacific was filmed primarily in Australia, with some scenes shot in Moorea, an island close to Tahiti. Sixteen songs are featured in the movie.  This version omitted the well-known song Happy Talk (song)|"Happy Talk", and cut the even more popular song "Bali Hai" in half. Several new scenes, such as Nellie and Emiles very first meeting at the officers club, were added, and a new character was created to serve as Nellies best friend and confidante. The sex scenes between Liat and Lt. Cable were also dealt with more frankly than in the original.

==Cast==
*Glenn Close - Nellie Forbush
*Harry Connick, Jr. - Lt. Joseph Cable
*Rade Sherbedgia  - Emile de Becque Jack Thompson - Captain George Brackett
*Lori Tan Chinn - Bloody Mary
*Ilene Graff - Singing Ngana
*Natalie Mendoza - Liat
*Simon Burke - Harbison
*Steve Bastoni - Lt. Buzz Adams
*Kimberley Davies - Luann
*Robert Pastorelli - Luther Billis
*Craig Ball - Austin
*Damon Herriman - Professor
*Salvatore Coco - DeVito
*Peter Lamb - Bruno

==Reception==
The movie, and Close, were praised by the critic for The New York Times, who wrote, "Ms. Close, lean and more mature, hints that a touch of desperation lies in Nellies cockeyed optimism. Im stuck like a dope with a thing like hope means one thing when you are in your 20s, something else when you are not." He also noted that the movie "is beautifully produced, better than the stagy 1958 film. ... The other cast members, including Ms. Close, also sing well."  The New York Post reviewer wrote that "Notions of racism toward the islanders were glossed over in the 1958 movie, but in tonights remake, the racial themes are brought to the surface, to the productions advantage ... theres a heightened sense of drama and tension in the remake because the war is closer at hand ... the rewards are great." 

The Washington Post reviewer noted:
 
 John Kenrick  because the order of the songs was changed, and also because Rade Sherbedgia, unlike previous Emiles, did not have an operatic singing voice. Playbill reported that "Internet chat room visitors have grumbled that Close is too old for the role of Nellie Forbush, who, in the song, A Cock-Eyed Optimist, is described as immature and incurably green", but also that "  Cohen said the May–December romance plot point ... has less resonance with audiences today and it was cut. Nellie is ageless, in effect." 

In the 2008  Oxford Companion to the American Musical, Thomas Hischak wrote:

 

==DVD==
A DVD was released on August 28, 2001. Special features include deleted scenes and behind-the-scenes look at the making of the movie. In 2013, the film was reissued on DVD by Mill Creek Entertainment in a double-feature DVD set including the 1993 TV remake of Gypsy (1993 film)|Gypsy.

==Soundtrack==
{{Infobox album  
| Name        = South Pacific: Original TV Soundtrack
| Type        = soundtrack
| Artist      = Richard Rodgers
| Cover       = SouthPacificTV.jpg
| Released    = March 20, 2001
| Recorded    = Studio 301, Sydney, Australia
| Genre       = Film/Soundtrack
| Length      = Sony Music
| Producer    = Michael Gore, Paul Bogaev
}}
A soundtrack from the TV production was released on March 20, 2001.
# "Overture"
# "There Is Nothing Like a Dame"
# "A Cock-Eyed Optimist" - Glenn Close
# "Bloody Mary"
# "Bali Hai"
# "Twin Soliloquies" - Glenn Close
# "Some Enchanted Evening" - Rade Šerbedžija
# "Dites-Moi"
# "Younger Than Springtime" - Harry Connick, Jr.
# "Im Gonna Wash That Man Right Outa My Hair" - Glenn Close, Ilene Graff Some Enchanted Evening (Reprise) - Glenn Close
# "Im in Love with a Wonderful Guy" - Glenn Close, Ilene Graff
# "Youve Got to Be Carefully Taught" - Harry Connick Jr.
# "This Nearly Was Mine"
# "Honey Bun" - Glenn Close, Ilene Graff
# "Finale Ultimo" - Glenn Close
# "My Girl Back Home" - Glenn Close, Harry Connick Jr.


==See also==
List of television films produced for American Broadcasting Company

==References==
 

==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 
 