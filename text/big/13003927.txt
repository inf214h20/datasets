The Final Winter
 
 
{{Infobox Film name            = The Final Winter image           = The final winter 2007 poster.JPG caption         = Theatrical poster for The Final Winter director  Brian Andrews   Jane Forrest producer        = Anthony Coffee   Michelle Russell writer          = Matthew Nable starring        = Matthew Nable music           = Adam Gock and Dinesh Wicks cinematography  = Ian Jones editing         = Matt Villa distributor     = Paramount Pictures Australia released        = 6 September 2007 runtime         = 96 minutes {{cite web
| url = http://thecia.com.au/reviews/f/final-winter.shtml
| title = Cinematic Intelligence Agency Review and information
| publisher = Cinematic Intelligence Agency
| accessdate = 29 August 2007
}}
 
| country        = Australia
| language        = English
| budget          =
| gross           = $284,354 
|}} Brian Andrews and Jane Forrest and produced by Anthony Coffee, and Michelle Russell, while independently produced it is being distributed by Paramount Pictures. It was written by Matthew Nable who also starred as the lead role Grub Henderson. The film, which earned praise from critics, {{Cite news 
  | last = Daniel Williams
  | first = 
  | coauthors = 
  | title = Footy for Thought Time
  | place = 
  | pages = 
  | language = 
  | publisher = Time Inc.
  | date = 31 August 2007
  | url = http://www.time.com/time/magazine/article/0,9171,1657920,00.html football team in the early 1980s and his determination to stand for what rugby league traditionally stood for while dealing with his own identity crisis. {{cite web
| url = http://www.abc.net.au/atthemovies/txt/s2004682.htm
| title = At the Movies Review At the ABC
| accessdate = 29 August 2007
}}
 

==Plot==
The film explores the way in which business tore up the loyalty that was between Grubs club and family. Essentially this is a metaphor for the way in which business began to imprint the game of Rugby League during the 1980s, and saw the rise of commercialism in the game. {{Cite news 
  | last = Purcell
  | first = Charles
  | coauthors = 
  | title = Final whistle
  | work = The Sydney Morning Herald
  | place = Australia
  | pages = 
  | language = 
  | publisher = Fairfax Media
  | date = 6 September 2007
  | url = http://www.smh.com.au/news/entertainment/final-whistle/2007/09/05/1188783289309.html
  | accessdate = 2 October 2010}}  Consequently Grub must battle with an administration that wanted him gone and additionally his brother and coachs betrayal. The film also deals with the domestic issues between Grub and his wife and his children, as their husband and father has been transformed from who he was to who he has become. {{cite web
| url = http://www.abc.net.au/atthemovies/txt/s2004682.htm
| title = At the Movies Review ABC
| accessdate = 29 August 2007
}}
 

==Cast==
*Matt Nable as Mick "Grub" Henderson Bob Baines as Neddy
*Conrad Coleby as Billy
*Nathaniel Dean as Trent
*Damian De Montemas as Max
*Kevin Golsby as Chairman
*Raelee Hill as Emma
*John Jarratt as Colgate
*Matthew Johns as Jack Cooper
*Michelle Langstone as Mia
*Scott Lowe as Muddy

 Roy Masters, Noel "Ned" Kelly {{Cite news 
  | last = FitzSimons
  | first = Peter
  | coauthors = 
  | title = The Fitz Files
  | work = The Sydney Morning Herald
  | place = Australia
  | pages = 
  | language = 
  | publisher = Fairfax Media
  | date = 20 October 2007
  | url = http://www.smh.com.au/news/sport/time-to-choose-between-a-bok-and-a-hard-place/2007/10/19/1192301041887.html?page=fullpage#contentSwap3
  | accessdate = 2 October 2010}}  {{Cite news 
  | last = Douglas Kennedy and Elissa Blake
  | first = 
  | coauthors = 
  | title = League legends in The Final Winter
  | work = The Sunday Mail
  | place = Australia
  | pages = 
  | language = 
  | publisher = Queensland Newspapers
  | date = 19 August 2007
  | url = http://www.couriermail.com.au/entertainment/league-legends-in-final-winter/story-e6freqex-1111114206811
  | accessdate = 2 October 2010}}  {{Cite news 
  | last = Phil Rothfield and Rebecca Wilson
  | first = 
  | coauthors = 
  | title = Hoppa a player agent The Daily Telegraph
  | place = Australia
  | pages = 
  | language = 
  | publisher = News Limited
  | date =1 July 2007
  | url = http://www.dailytelegraph.com.au/sport/hoppa-a-player-agent/story-e6frexni-1111113861354
  | accessdate = 2 October 2010}}  and Australian Wrestling Federation ring announcer The Duke of Wrestling Kieran Burns.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 