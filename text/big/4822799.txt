Tales of Ordinary Madness
 
{{Infobox film
| name=Storie di ordinaria follia (Tales of Ordinary Madness)
| image=Tales of Ordinary Madness.jpg
| director=Marco Ferreri
| producer=Jacqueline Ferreri
| screenplay=Sergio Amidei Marco Ferreri Anthony Foutz
| starring=Ben Gazzara Ornella Muti Susan Tyrrell Tanya Lopert Patrick Hughes Wendy Welles Lepopld Stratton Anthony Pitillo Jay Julien Peter Jarvis
| art director=Dante Ferretti
| costume designes by=Nicoletta Ercole
| film editor=Ruggero Mastroianni
| director of photography=Tonino Delli Colli
| music=Philippe Sarde
| released=1981
| runtime=101 min.
| language=English
}}
Tales of Ordinary Madness (it: Storie di ordinaria follia) (fr: Contes de la folie ordinaire) is a 1981 film by Italian director Marco Ferreri. It was shot in English in the USA, featuring Ben Gazzara and Ornella Muti in the leading roles. The films title and subject matter are based on the works and the person of US poet Charles Bukowski, including the short story The Most Beautiful Woman in Town (published by City Lights Publishing in the 1972 collection Erections, Ejaculations, Exhibitions, and General Tales of Ordinary Madness).
 Post Office. 

==Plot== Charles Serking, hooker with self destructive habits. They have a stormy relationship. When Serking gets an offer from a major publishing house, Cass tries to stop him from leaving, but fails. Serking gives in to the temptation of the big bucks, but soon realises his mistake and returns to LA only to find that Cass has killed herself in his absence. Devastated he hits the bottle in a nightmarish drinking bout, but finally reaches catharsis and returns to the seaside guesthouse where he spent his happiest moments with Cass. Here he rekindles his poetry with the aid of a young admirer in one of Ferreris trademark beach scenes.

==Reception==
While successful in Europe, the film met with a lukewarm reception in the US despite its American setting. Janet Maslin of the New York Times gave the film a negative review.    
 

== Awards ==
The film won 4 David di Donatello and  2 Nastro dArgento both including Best Director.

===David di Donatello===
*Marco Ferreri: Best Director
*Sergio Amidei & Marco Ferreri: Best Script
*Tonino Delli Colli - Best Cinematography
*Ruggero Mastroianni - Best Editing

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 


 
 