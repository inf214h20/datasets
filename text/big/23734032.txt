Choices (film)
{{Infobox film
| name           = Choices
| image          = Choices 1986 DVD cover.jpg
| director       = David Lowell Rich
| producer       = Robert Halmi Sr.
| writer         = Judith Parker
| starring       = George C. Scott Jacqueline Bisset Melissa Gilbert
| music          = Charles Gross
| cinematography = Robert M. Baldwin
| editing        = Eric Albertson ABC
| released       = February 17, 1986
| runtime        = 95 minutes USA
| language       = English
}}
Choices is a 1986 television film directed by David Lowell Rich.

==Plot==
The film focuses on a 62-year-old judge who rethinks his opposition to abortion when he finds out both his 19-year-old daughter and 38-year-old wife are unwanted pregnant.  When his daughter tries to contemplate an abortion without informing her boyfriend, he immediately expresses his disapproval. However, he later changes his mind when he finds out his wife is pregnant as well. The three of them are all forced to make important choices.

==Cast==
*George C. Scott as Evan Granger
*Jacqueline Bisset as Marisa Granger
*Melissa Gilbert as Terry Granger
*Laurie Kennedy as Ellen
*Steven Flynn as Scott
*Nancy Allison as Norma
*Daliah Novak as Janet
*Lorena Gale as Clinic Staffer

==References==
 

==External links==
* 

 

 
 
 
 


 