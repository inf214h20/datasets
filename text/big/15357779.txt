13 Rue Madeleine
{{Infobox film
| name           = 13 Rue Madeleine
| image          = 13_rue_madeleine_poster.jpg
| image_size     =
| caption        = Film Poster
| director       = Henry Hathaway
| producer       = Louis De Rochemont
| writer         = John Monks, Jr. Sy Bartlett
| narrator       = Reed Hadley Annabella Richard Conte
| music          = David Buttolph
| cinematography = Norbert Brodine
| editing        = Harmon Jones
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Annabella and Richard Conte. The title refers to the Le Havre address where a Gestapo headquarters is located.

==Plot== planned Allied invasion of Europe. They intend to provide Kuncel with false information to pass along to his superiors.
 Frenchwoman Suzanne American Jeff Allied invasion transporting the group discovers that the strap to Lassiters static line was deliberately cut. Gibson and Sharkey realize that Kuncel knows that the information he was given is false and that he can identify every agent he trained with.
 collaborator who designed the V-2 depot and returning him to Great Britain. However, while intercepting Kuncel as he tries to stop the pickup airplane from taking off, Sharkey is captured. Suzanne is killed while transmitting the news to England. The Gestapo torture Sharkey, but he refuses to reveal his knowledge. Back in Great Britain, Gibson has no choice but to order a bombing raid to destroy the Gestapo headquarters and kill Sharkey before he cracks. As the bombs strike, Sharkey realizes what is happening and laughs in Kuncels face just before they both perish.

==Cast==
* James Cagney as Robert Emmett Bob Sharkey
* Richard Conte as Wilhelm Kuncel / William H. Bill OConnell Annabella as Suzanne de Beaumont
* Frank Latimore as Jeff Lassiter
* Walter Abel as Charles Gibson
* Melville Cooper as Pappy Simpson Sam Jaffe as Mayor Galimard
* Karl Malden as Operation Carpetbagger|B-24 Jumpmaster
* E. G. Marshall as Emile
* Trevor Bardette as Resistance fighter
* Red Buttons as Second Jump Master
* Arno Frey as German Officer
* Donald Randolph as La Roche
* Roland Winters as Van Duyval
* Blanche Yurka as Madame Thillot

==Production== Cloak and William Donovan and featuring Peter Ortiz as a technical advisor, Donovan raised major objections to the film, including the idea that his agency had been infiltrated by an enemy agent. p.120 Dick, Bernard F. The Star Spangled Screen University of Kentucky Press   The spy group was renamed "O77" and Cagneys character had no similarities to Donovan.

The film followed Foxs The House on 92nd Street a true story of Federal Bureau of Investigation counter espionage which shared the same director, producer, and one of the writers.
 Ursulines School in the background.

The Breen Office also objected to the Americans bombing a building solely to kill Sharkey; the film made reference to Gestapo headquarters being an acceptable military target. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 