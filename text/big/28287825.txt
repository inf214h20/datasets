The Legend of Broken Sword
 
 
{{Infobox film name = The Legend of Broken Sword image = The Legend of Broken Sword.jpg caption = DVD cover art traditional = 折劍傳奇 simplified = 折剑传奇 pinyin = Zhé Jiàn Chuán Qí}} director = Ulysses Au-yeung producer = Ching Lan-wing writer = Gu Long starring =  music =  cinematography =  editing =  studio = 東海影業公司 distributor =  released =   runtime = 84 minutes country = Hong Kong Taiwan language = Mandarin budget = gross = 
}}
The Legend of Broken Sword, also known as Dragon of the Lost Ark or Dressed to Fight, is a 1979 wuxia film directed by Ulysses Au-yeung and written by Gu Long. The lead character, Hu Tiehua, appears in Gu Longs Chu Liuxiang novel series as well.

==Cast==
*Tien Peng as Hu Tiehua
*Doris Lung as Hung
*Man Kong-lung as Golden Bell Tang
*Ling Yun
*Elsa Yeung
*Au Lap-bo
*Goo Chang
*Su Chen-ping
*Cheung Chung-kwai
*Li Jian-ping
*Yeung Gwan-gwan
*Wong Yiu
*Mo Man-sau
*Shih Ting-ken
*Cheung Hung-gei
*Woo Hon-cheung

==External links==
* 
* 

 

 
 
 
 
 
 

 