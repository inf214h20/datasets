The Sound of Fury (film)
 
{{Infobox film
| name           = The Sound of Fury
| image          = Thesoundandfury.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Cy Endfield
| producer       = Robert Stillman
| screenplay     = Jo Pagano
| based on       =  
| narrator       =
| starring       = Frank Lovejoy Kathleen Ryan
| music          = Hugo Friedhofer
| cinematography = Guy Roe
| editing        = George Amy
| studio         = Robert Stillman Productions
| distributor    = United Artists
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Sound of Fury (also known as Try and Get Me) is a 1950 black-and-white film noir directed by Cy Endfield and featuring Frank Lovejoy, Lloyd Bridges and Kathleen Ryan. The film is based on Jo Paganos novel The Condemned, who also wrote the screenplay. 

The film is based on factual events that occurred in 1933, when two men were arrested in San Jose, California, for kidnapping and murdering Brooke Hart. The suspects confessed and were lynched by a mob of locals. The Fritz Lang-directed 1936 film Fury (1936 movie)|Fury was inspired by the same incident.

==Plot==
Howard Tyler (Frank Lovejoy) is a family man, living in California, who cant seem to get by financially.  He meets up with a small-time, but charismatic, hood Jerry Slocum (Lloyd Bridges).  Soon, Slocum convinces Tyler into participating in gas station robberies to get by.  Later, they kidnap a wealthy man in hopes of getting a huge ransom. Things go wrong when the man is murdered by Slocum then thrown in a lake. Tyler reaches his limit emotionally, and he begins drinking heavily. He meets a lonely woman and confesses the crime while drunk.  The woman flees and goes to the police. 

When the two kidnappers are arrested, a local journalist (Richard Carlson) writes a series of hate-filled articles about the two prisoners which eventually lead to a brutal lynching.

==Cast==
* Frank Lovejoy as Howard Tyler
* Kathleen Ryan as Judy Tyler Richard Carlson as Gil Stanton
* Lloyd Bridges as Jerry Slocum
* Katherine Locke as Hazel Weatherwax
* Adele Jergens as Velma Art Smith as Editor Hal Clendenning

==Reception==
===Critical response===
The New York Times film critic, Bosley Crowther, panned the film, writing "Although Mr. Endfield has directed the violent climatic scenes with a great deal of sharp visualization of mass hysteria and heat, conveying a grim impression of the nastiness of a mob, he has filmed the rest of the picture in a conventional melodramatic style. Neither the script nor the numerous performances are of a distinctive quality." 

Raymond Borde and Etienne Chaumeton, in a work on American film noir, wrote that "the prison assault remains one of the most brutal sequences in postwar American cinema." 
 NY Post or Fox Cable TV). It calls attention to something about the cowboy attitude in Americans that they dont like to acknowledge about themselves, but Europeans are quite aware of how uncivilized Americans can be." 

===Accolades===
Nominations
* British Academy of Film and Television Arts Awards: Best Film from any Source; 1952. 

==References==
 

==External links==
*  
*  
*  
*   at Film Noir of the Week by Glenn Erickson
*   essay at Yahoo! Voices by Timothy Sexton

 

 
 
  
 
 
 
 
 
 