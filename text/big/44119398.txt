Anweshanam (film)
{{Infobox film 
| name           = Anweshanam.com
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = 
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Sharada Kaviyoor Ponnamma Adoor Bhasi
| music          = M. K. Arjunan
| cinematography = V Namas
| editing        = B Satheesh
| studio         = Deepak Combines
| distributor    = Deepak Combines
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar . The film stars Prem Nazir, Sharada (actress)|Sharada, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Prem Nazir Sharada
*Kaviyoor Ponnamma
*Adoor Bhasi
*Jose Prakash
*Sankaradi
*Bahadoor Sujatha
*Vijayasree

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandra rashmithan || P Susheela || Sreekumaran Thampi || 
|-
| 2 || Chandra Rasmithan (Happy) || P Susheela || Sreekumaran Thampi || 
|-
| 3 || Maanathu Ninnoru || K. J. Yesudas, S Janaki || Sreekumaran Thampi || 
|-
| 4 || Manjakkili Paadum || P Jayachandran, P. Madhuri || Sreekumaran Thampi || 
|-
| 5 || Panchami Chandrika || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 6 || Thudakkam Chiriyude || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 7 || Thulaavarsha Meghangal || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 