Dildaar
{{Infobox film
| name           = Dildaar
| image          = 
| image_size     = 
| caption        = 
| director       = K. Bapaiah
| producer       = D. Rama Naidu
| writer         = 
| narrator       = 
| starring       = Jeetendra Rekha Moushumi Chatterji
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| studio      = Suresh Productions
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Dildaar is a 1977 Hindi film. Produced by D. Rama Naidu the film is directed by K. Bapaiah. The film stars Jeetendra, Rekha, Moushumi Chatterji, Sujit Kumar, Prem Chopra, Keshto Mukherjee, Jagdeep, Deven Verma, Nazneen (who received the films only Filmfare nomination as Best Supporting Actress),  and Shashikala. The music is by Laxmikant Pyarelal.  The film did "above average" business at the box office. 
 Telugu film Soggadu starring Shobhan Babu, Jayachitra, and Jayasudha. 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dekha Na Kaise Dara Diya"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Gaon Mein Hote Hanste Rote"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "Hum Jaise To Dildaar Hote Hain"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Main Raja Tu Rani"
| Kishore Kumar, Asha Bhosle
|}

==References==
 

==External links==
*  

 
 
 
 
 

 