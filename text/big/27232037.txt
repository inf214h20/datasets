Come Out of the Pantry
Come Out of the Pantry is a 1935 British musical film directed by Jack Raymond and starring Jack Buchanan, Fay Wray, James Carew and Fred Emney. 

==Plot==
A British aristocrat goes to New York City to sell some paintings, but when the bank collapses, he finds himself stranded with no money and lots of bills.

==Songs==
The film includes the following songs: 

Everything Stops for Tea, by Al Hoffman, Al Goodhart and Maurice Sigler; Sung by Jack Buchanan

From One Minute to Another, by Al Hoffman, Al Goodhart and Maurice Sigler

==Cast==
* Jack Buchanan - Lord Robert Brent
* Fay Wray - Hilda Beach-Howard
* James Carew - Mr Beach-Howard
* Fred Emney - Lord Axminster
* Olive Blakeney - Mrs Beach-Howard
* Kate Cutler - Lady Axminster
* Ronald Squire - Eccles
* Maire ONeill - Mrs Gore
* Ethel Stewart - Rosie
* Ben Welden - Tramp
* W.T. Ellwanger - Porteous

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 