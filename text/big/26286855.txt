Battle of Blood Island
{{Infobox film
| name           = Battle of Blood Island
| image_size     =
| image	=	Battle of Blood Island FilmPoster.jpeg
| caption        =
| director       = Joel Rapp
| producer       = Stanley Bickman
| writer         = Joel Rapp
| based on = story Expect the Vandals by Philip Roth
| narrator       =
| starring       = Richard Devon
| music          = Fred Katz
| cinematography = Jacques R. Marquette
| editing        = Carlo Lodato
| distributor    = Filmgroup
| released       = 1960
| runtime        = USA:64 minutes
| country        = USA
| language       = English
| budget         = $51,579.31 Fred Olen Ray, The New Poverty Row: Independent Filmmakers as Distributors, McFarland, 1991, p 35-36 
| gross          = $28,828.12 
| preceded_by    =
| followed_by    =
| website        =
}}

Battle of Blood Island is a 1960 American   released the film as a double feature with Ski Troop Attack.

Roger Corman appears at the end of the movie as an American soldier.

==Cast==
*Richard Devon as Moe
*Ron Kennedy as Ken

==Production==
Roger Corman put up $31,129.31 of the budget with $14,000 provided by Stan Bickman and Joel Rapp. The movie was shot in Puerto Rico at the same time as two other Corman productions, Last Woman on Earth and Creature from the Haunted Sea. 

Corman later said "it turned out very nicely; it was a good little picture". Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 139 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 