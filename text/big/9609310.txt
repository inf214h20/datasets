King of the Wild
{{Infobox film
| name           = King of the Wild
| image          = King_of_the_Wild.jpg
| image_size     =
| caption        =
| director       = B. Reeves Eason Richard Thorpe Lionel Backus (asst.) Theodore Joos (asst.)
| producer       = Nat Levine
| writer         = Wyndham Gittens Ford Beebe
| narrator       = Walter Miller Nora Lane Dorothy Christy Tom Santschi Boris Karloff Arthur McLaglen
| music          = Lee Zahler
| cinematography = Benjamin H. Kline Edward A. Kull
| editing        =
| distributor    = Mascot Pictures
| released       =  
| runtime        = 12 chapters (248 min)
| country        = United States
| language       = English
| budget         =
}} Mascot Serial movie serial.

==Plot==
Robert Grant, framed for a coup in the Indian country of Ranjapur, escapes from prison to Africa in search of the real villains.  Here he meets Sheik Mustapha, who has evidence to clear him, and the location of a secret diamond mine.

==Cast== Walter Miller American escapee from Ranjapur
*Nora Lane as Muriel Armitage, Tom Armitages sister
*Dorothy Christy as Mrs LaSalle
*Tom Santschi as Harris, Villainous Animal trapper
*Boris Karloff as Mustapha, an African sheikh
*Arthur McLaglen as Bimi, Ape man
*Carroll Nye as Tom Armitage, knows the location of a secret diamond mine
*Victor Potel as Peterson
*Albert DeWinton as Cyril Wainwright
*Martha Lalande as Mrs Colby
*Mischa Auer as Dakka, escaped lunatic
*Lafe McKee as Officer
 Harry Carey MGM film Trader Horn (1931) went over-schedule, forcing Mascot to recast.  , retrieved 29/04/07 

Also appearing in the serial was explorer Albert DeWinton. He later went after explorer Percy Fawcett who had disappeared in Brazil several years earlier. DeWinton also disappeared in the Amazon in early 1934 and was presumed dead. 

==Production==
King of the Wild is sometimes misreported as an alternate title for the serial King of the Kongo, which also starred Boris Karloff. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 351
 | chapter = 14. The Villains "All Bad, All Mad"
 }} 

==Chapter titles==
# Man Eaters
# Tiger of Destiny
# The Avenging Horde
# Secret of the Volcano
# Pit of Peril
# Creeping Doom
# Sealed Lips
# Jaws of the Jungle
# Door of Dread
# Leopards Lair
# The Fire of the Gods
# Jungle Justice
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 203
 | chapter = Filmography
 }} 

==See also==
* Boris Karloff filmography
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Mascot Serial Serial 
| before=The Phantom of the West (1931 in film|1931)
| years=King of the Wild (1931 in film|1931)
| after=The Vanishing Legion (1931 in film|1931)}}
 

 
 

 
 
 
 
 
 
 
 
 


 