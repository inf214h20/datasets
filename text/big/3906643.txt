The Corpse Vanishes
{{Infobox film 
| name = The Corpse Vanishes
| image = Corpsevanishes.jpg
| image_size = 
| caption = Theatrical poster
| director = Wallace Fox
| producer = Sam Katzman Jack Dietz
| writer = Screenplay: Harvey Gates Story: Sam Robins Gerald Schnitzer  Tristram Coffin Elizabeth Russell Arthur Reed
| editing = Robert Golden
| distributor = Monogram Pictures Corporation  
| studio              = Banner Productions
| released =   
| runtime = 64 minutes 
| country = United States
| language = English
}}  mystery and Elizabeth Russell) Tristram Coffin as a small town doctor investigate and solve the disappearances of the brides.  The film bears some resemblance to the real world story of Elizabeth Báthory, a 16th-century Hungarian countess and serial killer who was said to preserve her beauty by bathing in the blood of virginal young women.

The film was later the subject of a Mystery Science Theater 3000 episode.

== Plot ==
On the day of Alice Wentworth’s wedding, mad scientist Dr. Lorenz sends the young bride a poisoned orchid, the scent of which places the young woman in a state of suspended animation. He then spirits her body away to the basement laboratory of his isolated mansion and extracts her bodily fluids to inject into his vain and aged wife in order to renew her youth and beauty.  

A young journalist, Patricia Hunter, investigates the case and discovers it involves an unusual orchid.  She is directed to Lorenz, a known expert on orchids, and visits his mansion where she meets with a chilly reception.  She is forced to spend the night when a storm breaks, and discovers the basement laboratory.  In the morning, she hurries back to her newspaper offices.

Hunter and her colleagues make an attempt to trap Lorenz but he outfoxes them, chloroforming Hunter and carrying her to his laboratory to use her bodily fluids upon his wife.  During the injection procedure, Lorenz is stabbed by an angry servant woman who holds Lorenz responsible for her sons’ deaths. He strangles her then collapses and dies.  The servant rallies weakly and stabs Lorenz‘s wife to death.   The police arrive and Hunter is rescued.

== Cast ==
* Bela Lugosi as Dr. Lorenz, a mad scientist trying to preserve his wife’s youth and beauty by injecting her with fluids removed from the bodies of young virginal brides
* Luana Walters as Patricia Hunter, a newspaper reporter investigating the disappearances of the brides Tristram Coffin as Dr. Foster, a small town physician who aids Hunter during her investigations and eventually marries her Elizabeth Russell as Dr. Lorenz’s aged wife
* Minerva Urecal as Fagah, a crone and Dr. Lorenz’s servant
* Angelo Rossitto as Toby, Fagah’s son and a dwarf
* Frank Moran as Angel, Fagah’s son and a brutish half-wit
* George Eldredge as Mike, Lorenz’s henchman and the driver of his getaway car
* Joan Barclay as Alice Wentworth, a bride who becomes Lorenz’s victim
* Gwen Kenyon as Peggy, a nightclub cigarette girl who poses as a bride in  Hunter’s scheme to trap Lorenz
* Kenneth Harlan as Keenan, a newspaper editor and Hunter’s boss
* Vince Barnett as Sandy, a newspaper photographer and Hunter’s colleague

== Release ==
=== Home media ===
In 2009, the film was available on DVD.

Mystery Science Theater 3000 showed the movie in the fifth episode of their first season. The movie was considered so bad that when Tom Servo was questioned by Joel if there was anything positive from the movie, he short circuits. The MST3K episode is available in the collectors volume 16.

== Trivia ==
Bela Lugosi also starred in another 1942 horror film, Bowery at Midnight. In a scene in that film outside a cinema, the advertising poster outside the cinema doors is for "The Corpse Vanishes" and features Lugosis image and name. 

==See also==
* Béla Lugosi filmography
* List of films in the public domain

== External links ==
*  
*  
*  
*  

=== Mystery Science Theater 3000 ===
*  
*  
 
 
 
 
 
 
 
 
 
 