Darius Goes West
{{Infobox film
| name           = Darius Goes West: The Roll of his Life
| image size     = 
| image	=	Darius Goes West FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Logan Smalley
| producer       = Julia Eisenman Greg Schenz
| writer         = 
| narrator       = 
| starring       = Darius Weems Felicity Huffman William H. Macy
| music          = Logan Smalley
| cinematography = John Hadden Dylan Wilson
| editing        = Logan Smalley Jarrard Cole Allison Firor
| studio         = Roll With Me Productions
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States English
| budget         = United States|$60,000 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 documentary by Logan Smalley about Darius Weems, a teenager living with Duchenne muscular dystrophy.  In the summer of 2005 Weems embarked on a 7,000 mile road trip across the United States from his hometown in Georgia to MTV Headquarters in Los Angeles to ask them to customize his wheelchair on Pimp My Ride, as well to promote awareness of the fatal disease Duchenne muscular dystrophy, and to raise money for research into a cure.         

== Background ==

Logan Smalley was a counselor at a Project REACH camp, a facility set up to give the experiences of being at camp to children with disabilities, when he met Darius Weems.  He had first met Darius brother Mario, who was himself suffering from Duchenne muscular dystrophy.  Mario asked Logan to "look out for my little brother," and Logan took that promise to heart.  

After reading a copy of a letter Darius had sent to MTV|MTVs show Pimp My Ride in which he asked them to consider refurbishing his wheelchair, Smalley organized a cross-country trip for Darius to Los Angeles, which became known as the Darius Goes West Project.  After having previously been to the Cannes Film Festival, Smalley recognized the opportunity and importance of documenting the trip.  The 7,000 mile, 25-day trip resulted in 300 hours of video. 

According to DVD Talk, "including Weems, over half of the documentary team was under the age of 20, including Smalleys younger brother Ben, 18.  The oldest person on the crew was Daniel Epting, 24. The group not only handled the camera and sound equipment, they cared for Weems, helping him with day-to-day tasks like using the bathroom." 

The documentary was dedicated to Dariuss brother, Mario, who also had Duchenne muscular dystrophy.   Mario died at age 19.  

== Storyline ==
 RV and America during the course of their trip.    

=== Highlights ===
 Las Vegas. Sea World.  Darius also visited the zoo in San Francisco, California. 

In the end, Pimp My Ride denied the request for Dariuss wheelchair to be "pimped out."  But near his hometown of Athens, Georgia, a local car customizing dealer created a new design for Dariuss wheelchair, which included a television, speakers, and connection for an iPod. The most symbolic part of the new wheelchair was wheel spinners; this was the one thing Darius wanted most, because they would keep spinning even if he stopped.

In 2008-2009 Darius and his crew spent an entire year going back on the road visiting middle schools, high schools, and colleges and hosting screenings all over the country. In May-June 2012, Darius took a 32-day "Believe" tour of the northeast, where he performed raps and hosted Q&As at 18 schools. In Maine, Cynthia McFadden and the ABC Nightline crew followed Darius to two schools and devoted an entire show to Darius and his continuing quest to raise awareness of DMD on Thanksgiving  of 2012.

Darius turned 23 on September 27, 2012, and wrote a rap about it that he called a tribute to all of his fans. The single, "Thank You For 23," made it to #35 on iTunes in the hip hop genre. On February 16, in honor of his late brother Marios birthday, Darius released his first album, "My Life In This Chair."

Darius continues to visit schools and Skype with students all over the country.

== Charleys Fund ==

All proceeds from the film go to Charleys Fund, named for DMD sufferer Charley Seckler, and set up as a non-profit foundation investing in scientific research to help cure DMD.     The filmmakers had originally hoped the film might raise $70,000 for DMD research, but by March 2009 they had raised $1.5 million,  and by June 2009 they had raised $2 million. 

== Participants ==
 
 

* Darius Weems
* Andrew Carson
* Daniel Epting
* John Hadden
* John Harmon
* Jason Hees
* Sam Johnson
* Collin Shepley
 
* Ben Smalley
* Logan Smalley
* Kevin Wier
* Aidan Sandor
* Charley Seckler
* Tracy Seckler
* Dr. Benjamin Seckler
* Maria McDonnell
 
* Felicity Huffman
* William H. Macy
* Ethan McDonnell
* Dylan Wilson
* Tom Sandor
* Mark Johnson
* Stephen Bennett
 

== Reception ==

Robert Kohler of Variety (magazine)|Variety wrote "Certain to stir hearts -- as proven by its aud award at the Santa Barbara Fest -- and primed to raise awareness of a debilitating form of muscular dystrophy, Darius Goes West: The Roll of His Life lovingly records 15-year-old Darius Weems odds-defying cross-country U.S. trek. A rare case of an almost purely amateur film that has a solid shot of cablecasts after a certain roll through the fest circuit, pic makes up in authentic feeling what it lacks in documentary skill." 

David Cook of The Chattanoogan made note of the compassion shared by the eleven friends toward Darius, writing "Teenage males are so often viewed as unemotional and stoic, as young Rambos in training. But this film highlights the deep love these friends had for each other, and for Darius."  Cook also commended the care and consideration of the friends: "Unable to move most of his body, he   depended on his comrades as they lifted him into bed each night, carried him into the Gulf as he touched the ocean for the first time, sat together on the edge of the Grand Canyon, held on as they wheelchaired down Lombard Street in San Fran, advocated and fought against the lack of wheelchair accessibility in the US, and wept in the face of Darius’s crippling disease and impending death."  He praised them further, "They were as graceful and compassionate caregivers as I have ever seen, and if you are looking for role models or hope in dark times, look no farther than this film and these men." 
 Santa Barbara Film Festival in 2007 that he felt like he was interviewing his own brother Tommy, who had also been born with DMD: "The shape of his head and torso were identical. The chubby cheeks and sneaky smile were the same. And the endless attempts to shift his bulbous arms, and get comfortable in his wheelchair, were an exact replica of Tommy.  But this wasnt my brother. Sadly, Tommy passed away in January 1991. This was Darius Weems from Athens and this is what Duchennes Muscular Dystrophy does to the body."  He called the documentary "remarkable" in how it "records a journey that is full of happiness and heartbreak," and, in recalling his reactions to other documentaries, concluded, "when I watched Darius Goes West something altogether more profound happened. I saw my brother again - and for that alone, I always shall be grateful." 
 Stand by Me. 

David Walker of DVD Talk called the documentary "a collective labor of love, put together by a dedicated group of people committed to changing the world," writing that "Film at its best has the power to engage us emotionally and spiritually," and that "that is what makes the documentary Darius Goes West: The Roll of His Life not only a great film, but the best film of 2007."  He concluded that "the film is an amazing celebration of life" and "one of the most life-affirming films you will ever see." 

== Recognition ==

As a Quality of Life 2008 finalist, Darius Weems received a $25,000 Volvo for Life Award,  as well as a $15,000 World of Children Founders Youth Award, and a $2,000 Gloria Barron Prize for Young Heroes.

In May 2009, Darius was selected for $10,000 award from DoSomething.org,  and in June, he and the filmmakers were honored at the Do Something Awards celebration held on June 4 at Harlem’s Apollo Theater.  Weems has donated all monies to Charleys Fund.

A supporter of the film established the Darius Weems Scholarship in December 2008, to support an incoming freshman at Yale University.

The Athens, Georgia Junior League presented Logan Smalley with the Spirit of Athens Community Service Award.

WXIA-TV presented Logan Smalley with the 11 Alive Community Service Award.  Their coverage of the film received an Emmy Award nomination.

Darius Weems and Logan Smalley were invited to speak at the 2009 TED Conference Fellowship. 

In April 2009, the Council for Exceptional Children presented Darius Weems with their 2009 Yes I Can Award.

=== Awards & nominations ===

As of June 2009, the film had won an unprecedented 28 film festival awards,   and at the Tribeca Film Festival it was the only film chosen by Robert De Niro to screen at his children’s school during the festival. Among the awards won at film festivals are:
 AFI Dallas International Film Festival
: 2007, won Audience Award for Best Feature at Atlanta Film Festival
: 2007, won Audience Award for Best Documentary at Palm Beach International Film Festival
: 2007, won Audience Choice Award for Best Feature at Santa Barbara International Film Festival
: 2007, won Best Film at Cleveland International Film Festival
: 2007, won Standing Up Film Competition at Cleveland International Film Festival
: 2007, won Audience Award for Best Feature at Independent Film Festival of Boston Oxford Film Festival Oxford Film Festival
: 2007, won Audience Choice Award for Best Feature at Jackson Hole Film Festival
: 2007, won Cowboy Award for Best Documentary at Jackson Hole Film Festival
: 2007, won Audience Award for Best Documentary at Omaha Film Festival
: 2007, won Jury Award for Best Documentary at Omaha Film Festival
: 2007, won Grand Jury Award for Best Documentary at Solstice Film Festival 
: Emmy Award nomination
: 2008, won Grand Prize at The Christophers student film competition
: Won Jury Award for Best Documentary at Real to Reel International Film Festival
: Won Audience Choice Award at East Lansing Film Festival
: Won Jury Award for Best Documentary at Longbaugh Film Festival
: Won Jury Award for Best Feature at Sunscreen Film Festival
: Won Audience Choice Award for Best Feature at Sunscreen Film Festival
: Won Jury Merit Award at Superfest International Disability Festival
: Won Audience Choice Award at Berkshire International Film Festival
: Won Jury Award for Best Documentary at Sidewalk Film Festival
: Won Audience Choice Award at Sidewalk Film Festival
: Won Audience Choice Award at Ojai Film Festival
: Won Youth Jury Award for Best Documentary at Viewfinders International Film Festival for Youth
: Won Runner Up for Best Documentary at BiNational Film Festival
: Won Jury Honorable Mention for Best Documentary at Picture This Film Festival

== References ==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

}}

== External links ==
*  
*  

 
 
 
 
 