Shattered (1921 film)
{{Infobox film
| name           = Shattered
| image          = Scherben.avi snapshot 00.56.37.jpg
| alt            = 
| caption        = Werner Krauss in Shattered
| director       = Lupu Pick
| producer       = Lupu Pick
| writer         = Carl Mayer Lupu Pick
| starring       = Werner Krauss Edith Posca Hermine Straßmann-Witt
| music          = Giuseppe Becce Alexander Schirmann
| cinematography = Friedrich Weinmann
| editing        = 
| studio         = Rex-Film GmbH
| distributor    = Film Arts Guild
| released       =  
| runtime        = 50 minutes
| country        = German
| language       = Silent film
}}
 1921 Cinema German silent silent Kammerspielfilm directed by Lupu Pick, written by Carl Mayer, and is considered to be the earliest example of the kammerspielfilm. 

== Plot ==
Set during the winter, the story tells the tale of a track checker and his family who live a monotonous and poverty-stricken life next to a railway line. They receive a telegram announcing the arrival of the section inspector, who is to live with the family. 

== Cast ==
* Werner Krauss as Bahnwärter
* Edith Posca as Tochter des Bahnwärters
* Hermine Straßmann-Witt as Frau des Bahnwärters
* Paul Otto as Bahninspektor
* Lupu Pick as Reisender

== External links==
*  
*  
*  

== References ==
 

 
 
 
 
 
 
 
 
 
 


 