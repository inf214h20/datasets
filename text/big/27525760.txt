False Servant
 
{{Infobox film
| name           = False Servant
| image          = 
| image size     = 
| caption        = 
| director       = Benoît Jacquot
| producer       = Georges Benayoun Marivaux
| starring       = Isabelle Huppert
| music          = 
| cinematography = Romain Winding
| editing        = Pascale Chavance
| distributor    = Pyramide Distribution
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

False Servant ( ) is a 2000 French comedy film directed by Benoît Jacquot and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert as La comtesse
* Sandrine Kiberlain as Le chevalier
* Pierre Arditi as Trivelin
* Mathieu Amalric as Lélio
* Alexandre Soulié as Arlequin
* Philippe Vieux as Frontin

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 