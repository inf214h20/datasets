Siruthai
{{Infobox film
| name           = Siruthai
| image          = Siruthai Karthi.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical Release Poster Siva
| producer       = K. E. Gnanavel Raja S. R. Prakash Babu S. R. Prabhu
| writer         = Siva  (Dialogue)
| screenplay     = Siva
| story          = K. V. Vijayendra Prasad
| based on       =  
| narrator       = 
| starring       =   Vidyasagar
| cinematography = Velraj
| editing        = V. T. Vijayan
| studio         = Studio Green
| distributor    = Studio Green
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Tamil
| budget         =  13 crore   
| gross          = 40 crore
}} thriller film directed by Siva (director)|Siva.  A remake of the Telugu blockbuster Vikramarkudu, directed by S. S. Rajamouli, the film stars Karthi, performing dual roles, and Tamannaah alongside comedian Santhanam (actor)|Santhanam. The film was produced by K. E. Gnanavelraja and features music by Vidyasagar (music director)|Vidyasagar. It was released during Pongal on 14 January 2011. Despite gaining mixed critical response, the film was a Blockbuster at the box office. The film was also one of the highest-grossing Tamil films of 2011.The film was also remade in Hindi as Rowdy Rathore directed by Prabhu Deva starring Akshay Kumar and Sonakshi Sinha.   

==Plot==
Rocket Raja (Karthi) is a small-time thief who steals anything he comes across. With his partner Kaatu poochi (Santhanam (actor)|Santhanam) he enjoys life to its fullest and even falls for the beautiful Swetha (Tamannaah). However, his life changes when a small girl Divya Rathinavel (Baby Rakshana)ends up in his care; he looks exactly like her father Rathniavel Pandian IPS (Karthi). As Raja discovers the girls past, it is revealed that Rathinavel Pandian is an honest policeman who is a nightmare to criminals, but that one gang of criminals in the village of Devi pattnam are intent on killing him and his daughter because he killed the son of a prominent criminal, who also sexually abused women. Rathinavel Pandians colleagues put Divya in Rocket Rajas care so that the criminals cannot track her. Rathinavel Pandian is on the verge of death following a gunshot to his head. At first Raja is angry with the little girl and breaks her tape recorder. Divya is happy and hugs him, after he repairs it. Rathinavel Pandian is intent on fighting back. Unfortunately, he dies following a battle with several criminals who had been chasing Raja and Divya. From police officers,he realizes that Pandian was an honest and courageous police officer who stood against Babuji and his family who had cruelly ruled a village. Pandyan even manages to kill his son and stops their criminal acts. Pandyan manages to fail his brother but he gets shot when he tried to save a child.Upon seeing his courage, Rocket Raja (with the help of Rathinavel Pandian’s colleagues) steps into Rathinavel Pandians shoes and finishes his unfinished work as he himself is a thug and destroys all their properties with the help of kaatu poochi in a hilarious way.He also kills his brother and also takes on the responsibility of taking care of Divya, with the help of Swetha.

==Cast==
* Karthi as Rocket Raja / Rathnavel Pandian IPS 
* Tamannaah as Shwetha Santhanam as Kaatu Poochi
* Baby Rakshana as Divya Rathinavel Pandian
* Avinash as Bavuji
* Rajiv Kanakala as Inspector Bharath
* Megha Nair as Jhansi
* Manobala as Bhoom Bhoom
* Santhana Bharathi as Minister
* Bhumika Chawla as Wife of Rathinavel Pandian (In portrait)
* Meghna Naidu in a Special appearance
* Nelson Xavier in a guest appearance

==Character map of Siruthai and its remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
|   (2012) (Cinema of West Bengal|Bengali) || Rowdy Rathore (2012) (Bollywood|Hindi)
|- Manna ||Sudeep  || Karthi || Prosenjit Chatterjee || Akshay Kumar
|- Purnima ||Ragini Dwivedi || Tamannaah || Richa Gangopadhyay || Sonakshi Sinha
|}

==Production==
In 2008, reports emerged that Vikramarkudu would be remade in Tamil with Karthi. Ruthika and Anushka, who acted in original, were considered for the female leads. Suraj of Thalainagaram was said to be the director.  VV Kathir who directed Jeeva starrer Thenavattu was also announced as director but Sivakumar advised Karthi to do strong characters before making a mark as action hero thus the project was dropped.  The project was revived in 2010 and Siva, who directed films like Souryam and Sankham, was selected as director making his debut in Tamil.  Tamannah was chosen as heroine after Paiyaa. 

==Soundtrack==

{{Infobox album 
| Name     = Siruthai
| Type     = soundtrack Vidyasagar
| Cover    = 
| Released = 2010
| Recorded = 
| Genre    = Film soundtrack
| Length   = 
| Label    = Venus  Vidyasagar
| Last album = Kaavalan (2010)
| This album = Siruthai (2010)
| Next album = Ilaignan (2010)
}}
 Vidyasagar and features five songs. It was described as "bland" by Rediff,    while Sify called it "catchy and youthful".    Behindwoods gave a 2.5/5 rating, claiming that it "emphasizes the fact that it is a commercial entertainer. This may not be Vidyasagars best album but he has given what the movie requires: A crisp album with short and sweet songs   Siruthai is a worthy listen to and must appeal to Karthis fans is a huge way". 

{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes Ranjith | lyrics1 = Na. Muthukumar
| title2 = Chellam Vada Chellam | extra2 = Udit Narayan, Roshan & Surmukhi | lyrics2 = Na. Muthukumar Priyadharshini | lyrics3 = Viveka
| title4 = Thalattu | extra4 = Srivadhini | lyrics4 = Arivumathy
| title5 = Adi Rakkamma Rakku | extra5 = Ranjith (singer)|Ranjith, Suchitra & Roshan | lyrics5 = Pa. Vijay|Pa.vijay
}}

==Reception==
The film was met with mixed to positive response from most critics and audiences. Behindwoods gave a 2/5 rating and wrote: "Watch Siruthai if you are a hardcore mass masala fan. Since the originality of the Telugu version is retained, when released in Andhra this movie might serve as Karthi’s launch pad in Tollywood. Chances are that you might like Karthi, who is trying his best to prevent the movie from its impending dive into the depths of hackneyed void."    remarked: "You can watch the film with your family because of the entertaining elements. It has a perfect mix of romance, action, fun, comedy, glamour and sentiments. It is a perfect Pongal treat for Kollywood audience. Do not miss this movie". 

==Release==
The satellite rights of the film were secured by STAR Vijay. The film was given a "U/A" certificate by the Indian Censor Board.

==References==
 

==External links==
*  
 

 
 

 
 
 
 
 
 
 
 
 
 