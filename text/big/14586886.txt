Hari-Bhari
{{Infobox film
| name           = Hari-Bhari
| image          = 
| image_size     = 
| caption        = 
| director       = Shyam Benegal NFDC
| writer         = Priya Chandrasekar (Screenplay)   Shama Zaidi (Screenplay & Dialogue)
| story  =       
| dialogue  =  
| starring       = Shabana Azmi  Rajit Kapur  Rajeshwari Sachdev Nandita Das
| music          = Vanraj Bhatia
| lyrics         = 
| cinematography =  Rajan Kothari
| editing        = Aseem Sinha
| distributor    = 
| released       = 2000
| runtime        = 134 min
| country        = India
| language       = Hindi
}}

Hari-Bhari (Fertility) is 2000 Hindi film by Shyam Benegal, starring Shabana Azmi, Rajit Kapur, Rajeshwari Sachdev, Surekha Sikri and  Nandita Das in lead roles.
 fertility rights empowerment.

The film won the 2000 National Film Award for Best Film on Family Welfare.

==Plot==

The movie tells the story of a middle aged Muslim woman gazala(shabana azmi), who is ill treated by her husband and sent back to her parents home, just because she is unable to give birth to a son.  Gazala does have a daughter salma(rajeshwari sachdev). The plot revolves around gazalas stay at her mothers home and the problems faced by the women of the house, being her mother and sisters in law. The film is a beauitiful description of the problems faced by women in the name of fertility, or in a different sense the urge to have a male child, especially in rural India. Shyam Benegal provides a masterpiece, reflecting on the sorrowful plight of rural Indian women. The irony lies in the point where the movie begins with a scene which shows a bull being brought to impregnate a buffalo. Five different women, each with a different story depicting the harsh realities of a womans life

==Cast==
* Shabana Azmi as Ghazala
* Rajit Kapur as Khurshid
* Rajeshwari Sachdev as Salma
* Surekha Sikri as Hasina
* Nandita Das as Afsana
* Seema Bhargava as Rampyari
* Lalit Tiwari as Khaleej

==References==
 

Hari Bhari is also one of the films featured in Avijit Ghoshs book, 40 Retakes: Bollywood Classics You May Have Missed

==External links==
*  
*   at Rediff.com.

 

 
 
 
 
 
 
 
 
 


 