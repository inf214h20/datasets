Mona Lisa Smile
{{Infobox film
| name        = Mona Lisa Smile
| image       = monalisasmile.jpg
| alt         = 
| caption     = Theatrical release poster Mike Newell
| producer    = Fredward Johanson
| writer      = Lawrence Konner Mark Rosenthal
| starring    = Julia Roberts Kirsten Dunst Julia Stiles Maggie Gyllenhaal Ginnifer Goodwin Dominic West Juliet Stevenson Marcia Gay Harden John Slattery Marian Seldes
| music       = Rachel Portman
| cinematography = Anastas Michos
| editing     = Mick Audsley Red Om Films Productions
| distributor = Columbia Pictures
| released    =  
| runtime     = 117 minutes
| country     = United States
| language    = English Italian
| budget      = $65 million
| gross       = $141.3 million 
}} Red Om Mike Newell, song of Seal for the movie. Julia Roberts received a record $25 million for her performance—the highest ever earned by an actress. 
{{cite news
| url = http://www.forbes.com/2007/01/17/richest-women-entertainment-tech-media-cz_lg_richwomen07_0118womenstars_slide_9.html
| title = The 20 Richest Women In Entertainment
|work=Forbes
| date = January 17, 2007
| accessdate = July 15, 2011
| first1=Lea
| last1=Goldman
| first2=Kiri
| last2=Blakeley
}}
 

==Plot==
 
 private liberal arts college in Massachusetts, because she wants to make a difference and influence the next generation of women. At her first class, Katherine discovers that the girls have already memorized the entire syllabus from the textbook, so she instead uses the classes to introduce them to Modern Art and encourages spirited classroom discussions about topics such as what makes good art and what the Mona Lisas smile means. This brings her into conflict with the conservative college president (Marian Seldes), who warns Katherine to stick to the syllabus if she wants to keep her job. Katherine comes to know many of the students in her class well and seeks to inspire them to achieve more for themselves as intelligent women than the expected convention of marriage to eligible young men. Joan Brandwyn (Julia Stiles) dreams of being a lawyer and has enrolled as pre-law, so Katherine encourages her to apply to Yale Law School, where she is accepted; Katherine finds her liberal sensibilities affronted when Joans fiancé Tommy (Topher Grace) comments Joan "will always have that", intimating his own expectations of what his wife should be. Joan eventually elopes with Tommy, and professes to Katherine she is very happy—she had decided that what she wants most is to be a wife and mother after graduation and asks Katherine to respect her choice.

Betty Warren (Kirsten Dunst) is highly opinionated and outspokenly conservative like her mother, who is also the head of the Alumnae Association. Betty doesnt understand why Katherine is not married and insists that there is a universal standard for good art. She writes editorials for the college paper, one that exposes campus nurse Amanda Armstrong (Juliet Stevenson) as a supplier of contraception, which results in the nurse being fired; Betty writes a second editorial attacking Katherine for advocating that women should seek a career instead of just being wives and mothers as intended. Betty cant wait to marry Spencer (Jordan Bridges) as their parents have arranged and expects to get the traditional exemptions from attending class because she is married, but Katherine insists she will be marked on merit, resulting in a growing conflict between the two.

Connie Baker (Ginnifer Goodwin) begins dating Bettys cousin, Charlie (Ebon Moss-Bachrach), often spending weekends together. Betty persuades her that he is only using her since it has been arranged by his parents for him to marry Deb MacIntyre, a girl more of his social standing. After a disastrous date, where Charlie and Connie very nearly cross paths with Debs parents on a weekend away at the shore, Connie ends the relationship, believing Bettys story to be true. However some weeks later, Connie and Charlie reconnect, with Charlie saying he has already decided for himself that he is not going to marry Deb, so he and Connie get back together.
 WASP student body. Giselle brazenly has affairs with a professor and a married man.

Katherine confides to the girls that she was engaged when she was younger, but that she and her fiancé were separated by the war. The relationship fizzled out, and she has since had several affairs, including one with William Holden. Katherine declines a proposal from her boyfriend (John Slattery) from California because she doesnt love him enough. She begins seeing the Wellesley Italian professor, Bill Dunbar (Dominic West), who is charming and full of stories about Europe and his heroic actions in Italy during the war. He has also had affairs with many students (including Giselle), and Katherine makes him promise that it will never happen again. When Katherine learns that Bill spent the entire war at the Army Languages Center on Long Island, she decides to break up with him because he is not trustworthy. Dunbar responds that Katherine didnt come to Wellesley to help the students find their way, but to help them find her way.

Within six months of the wedding Bettys marriage fails, as Spencer has an affair, hiding it from his wife by pretending to be away on business. Giselle sees him with another woman and comforts Betty when she lashes out due to the pain of her failure. Bettys mother, Mrs. Warren, tries to pressure Betty into remaining married to Spencer, at least for a while to avoid causing a scandal. She refuses and asks her mother if the Mona Lisas smile means she is happy. At graduation, Betty asks Katherine about an apartment, but Mrs. Warren interrupts her and asks her why. Betty tells her mother that she had filed for divorce earlier that same morning after learning how disloyal Spencer was to her and wants to have her own future. She adds that she is going to share a flat in Greenwich Village with Giselle, and that she is considering applying to Yale Law School.

Katherines course is highly popular, so the college invites her to return. But Mrs. Warren and the president impose conditions on Katherine: she must follow the syllabus, submit lesson plans for approval, keep a strictly professional relationship among all faculty members, and not talk to the girls about anything other than classes. Katherine decides to leave, exploring Europe. In the final scene, Betty dedicates her last editorial to her teacher Katherine Watson, claiming that Katherine is "an extraordinary woman who lived by example and compelled us all to see the world through new eyes." As Katherines taxi speeds up, all her students follow on their bicycles and Betty is seen increasingly struggling to keep up with the taxi as a last effort to thank Katherine for changing her life.

==Cast==
 
* Julia Roberts as Katherine Ann Watson
* Kirsten Dunst as Elizabeth "Betty" Warren (Jones)
* Julia Stiles as Joan Brandwyn (Donegal)
* Maggie Gyllenhaal as Giselle Levy
* Annika Marks as art history student
* Ginnifer Goodwin as Constance "Connie" Baker
* Dominic West as Bill Dunbar
* Juliet Stevenson as Amanda Armstrong
* Marcia Gay Harden as Nancy Abbey
* John Slattery as Paul Moore
* Marian Seldes as President Jocelyn Carr
* Ebon Moss-Bachrach as Charlie Stewart
* Topher Grace as Tommy Donegal
* Jordan Bridges as Spencer Jones
* Laura Allen as Susan Delacorte
* Emily Bauer as art history student
* Tori Amos as wedding singer
* Lisa Roberts Gillan as miss Albini
* Krysten Ritter as a student
* Lily Rabe as art history student
 

==Soundtrack==
{{Infobox album
| Name = Mona Lisa Smile
| Type = Soundtrack
| Artist = Various
| Cover =
| Released = November 21, 2003
| Length = 48:27
| Label = Sony Music
| Reviews
}}
 Mona Lisa" Seal (3:11) You Belong to Me" – Tori Amos (3:03)
# "Bewitched, Bothered and Bewildered" – Celine Dion (2:45)
# "The Heart of Every Girl" – Elton John (3:40)
# "Santa Baby" – Macy Gray (3:29)
# "Murder, He Says#Song|Murder, He Says" – Tori Amos (3:22)
# "Bésame Mucho" – Chris Isaak (2:46) Secret Love" – Mandy Moore (3:40)
# "Whatll I Do" – Alison Krauss (3:12)
# "Istanbul (Not Constantinople)" – The Trevor Horn Orchestra (2:26)
# "Sh-Boom|Sh-Boom (Life Could Be a Dream) – The Trevor Horn Orchestra (2:49)
# "Im Beginning to See the Light" – Kelly Rowland (1:47)
# "Ive Got the World on a String" – Lisa Stansfield (2:20)
# "Smile (Charlie Chaplin song)|Smile" – Barbra Streisand (4:17)
# "Suite" – Rachel Portman (5:33)

==Box office==
In its first opening weekend, Mona Lisa Smile opened at #2 at the U.S. Box office raking in $11,528,498  .  By the end of its run, the film had grossed $141,337,989 worldwide from a $65 million budget.

==Reception==
Film review website Rotten Tomatoes gives the movie a 35% "rotten" review based on 149 reviews. In a typical review, Claudia Puig of USA TODAY wrote, "its Dead Poets Society as a chick flick, without the compelling drama and inspiration... even Roberts doesnt seem convinced. She gives a rather blah performance, as if shes not fully committed to the role... Rather than being a fascinating exploration of a much more constrained time in our social history, the film simply feels anachronistic. The film deserves a solid C for mediocrity and muted appeal."  Critic Elizabeth M. Tamny of the Chicago Reader shared this negative assessment, writing "Part of the problem is simply that Mona Lisa Smile is a Hollywood film, and Hollywood isnt good at depicting the life of the mind... And Julia Roberts is no help--you either like her or you dont, but either way it has little to do with talent. Shes not so much an actor as a vessel for earnest reactions. The fact is... Its easier to take on an extremely black-and-white version of the most salient question from this film--can women bake their cake and eat it too?--than try to answer it in the present." 

==Reaction from Wellesley alumnae==
In a message to Wellesley alumnae concerning the film, Wellesley College president Diana Chapman Walsh expressed regret, given that many alumnae from the 1950s felt that the films portrayal of Wellesley was inaccurate. 

==Campus controversy== casting directors were using race to discriminate against potential extras. Producers claimed that they were merely stressing the importance of finding women that reflected the time period.

The controversy spilled over into the local media, and producers considered a compromise of hiring willing minority students to act as production assistants. The college issued a press release highlighting the realities of Wellesley in 1953 and defending their decision to allow the film to shoot on campus. 

==References==
 

==External links==
 
* John Walker. (2009).  . artdesigncafe. Retrieved August 4, 2011.
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 