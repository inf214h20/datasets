Talking Head (film)
 
  is a 1992 live action film written and directed by Mamoru Oshii. Its a surreal meta-film mystery involving the production of an anime called Talking Head.

The film has many references to the anime industry with characters being named after animators such as  Yasuo Ōtsuka and Ichirō Itano, designer Yutaka Izubuchi, writer Kazunori Itō, and composer Kenji Kawai.  The director of the film within the film is named Rei Maruwa, which was a pseudonym for Oshii on several of his projects. 

The film features a short anime segment by famous character designer Haruhiko Mikimoto (of Macross fame).

==External links==
 

 

 
 
 
 
 
 


 
 