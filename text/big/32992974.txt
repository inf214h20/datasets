True Bromance
{{Infobox film
| name           = True Bromance
| image          = OfficialposterTrueBromance.jpg
| alt            =  
| caption        = When Loves thunderbolt strikes, who do you turn to?
| director       = Sebastian Doggart
| producer       = Sebastian Doggart
| writer         = Sebastian Doggart Jim Norton Carol Connors Carol Connors Devin Ratray Sebastian Doggart Jess King
| cinematography = Matthew Woolf
| editing        = Jake Diamond
| studio         = American Princess
| distributor    = Sonar
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
True Bromance is a 2012 film by British filmmaker Sebastian Doggart which explores the absurd advice that friends and family give when someone falls in love. It follows the quest of actor Devin Ratray, who wants to win the heart of U.S. Secretary of State Condoleezza Rice. 

==Plot== Jim Norton. Carol Connors. When he arrives in Washington, D.C., he is assisted by Republican strategist Frank Luntz.

==Style==
The film is a "bromantic comedy" in the style of The Hangover. It has an original look combining live action footage with super-hero comic-book visual effects, including animated "bro cards" to introduce each character; multi-frame panels as scene transitions; thought bubbles to visualize Ratrays fears, fantasies and memories; green-screen composites of Devin interacting with his beloved Rice; and hand-painted animations of the films heroes.

==History==
The films production history is a tortuous one. In July 2007, Discovery Communications commissioned the film for $600,000. In August 2007, one week before principal photography was due to begin, Discovery suddenly announced that they had "canceled" the film. This followed pressure from Karl Rove, who warned Discovery that the movie could damage their "good relations with government".    Discovery settled with the producers, American Princess LLC, for a $150,000 "kill loan", forcing the producers to make the film on a shoestring.  Rice continued to obstruct the film, sending State Department officials to raid the producers guesthouse in Washington, D.C., and plant a bug under a coffee table in their living room—actions which were documented on camera and included in the film. After test screenings at a number of festivals, the film was finally completed in March 2012.

==Awards==
The film has won 18 awards at its festival screenings. At the  .

==Reviews==
The New York Times bestselling author Betsy Prioleau (Seductress, Swoon) described the film: "True Bromance is a tour de force, a hilarious, triple-barreled send-up—of obsessive love, the bromance “buddy” genre, and former secretary of state Condoleezza Rice. This absurdist caper, directed by Sebastian Doggart, is stylish and fall-down funny. A comic gem—not to be missed, with creative snap and a stiletto edge."   The Brooklyn Paper called the film "a bromance for all ages... the highlight of the Williamsburg International Film Festival."   The Brooklyn Eagle wrote: "Starring possibly the most surreal comedy troupe ever... True Bromance is consistently unnerving, funny and surprising and features an original comic-book style."  

==References==
 

==External links==
*  
*  
 

 
 
 
 
 