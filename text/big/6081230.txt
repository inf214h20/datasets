Feast of July
{{Infobox Film
| name           = Feast of July
| image          = Feast of july.jpg
| caption        = Promotional movie poster for the film
| director       = Christopher Menaul Henry Herbert Christopher Neame Paul Bradley (executive) Christopher Neame (from the novel by H. E. Bates) Tom Bell Kenneth Anderson Ben Chaplin
| music          = Zbigniew Preisner
| cinematography = Peter Sova
| editing        = Chris Wimble
| studio         = Touchstone Pictures Merchant Ivory Productions Buena Vista Pictures
| released       = October 13, 1995
| runtime        = 116 min
| country        = UK/USA
| awards         = English
| budget         = 
| gross          = $293,274
| preceded_by    = 
| followed_by    = 
}} 1995 UK film produced by Merchant Ivory Productions, based on the 1954 novel by H. E. Bates, starring Embeth Davidtz and Ben Chaplin.

==Plot== Tom Bell), Kenneth Anderson), Con (Ben Chaplin) and Jedd (James Purefoy). Matty, the youngest, is a shoemaker, Jedd is a soldier (cavalry) and Con does not have a specific profession. He does the household chores and helps his parents around. Throughout the movie, Con is shown to be reserved and introvert with a violent temper, although kind at heart. Bella is unable to eat any thing during the dinner, starts crying and faints when she stands up to go to bed.

Mrs. Wainwright puts Bella to bed and feeds her some soup after a while. Bella refuses to have more and falls asleep. She wakes up in the midnight and remembers the intimate moments with her lover, Arch. She falls asleep again and wakes up in the morning. From the window, she sees Mr and Mrs. Wainwright seeing off Jedd, who is going back to work. Jedd sees Bella at the window and winks at her. Jedd had already been smitten by Bella during the previous nights dinner. After Jedd is gone, Bella prepares to leave. Mrs. Wainwright gives Bella her daughters coat and tells her that she died of Rheumatic fever, 3 years ago. She asks about Bellas family and learns that she has nowhere to go. She tells her that she can stay and she will have to do her share of household work, if she chooses to stay. She also suggests she fetch leather from the factory for the shoes. Bella agrees to stay.

That evening, Bella takes some shoes (a present from Arch) to Matty in order to have a sock put in them since they are large for her feet. Matty tells her that it cant be done as the shoes are "cheap Yankee ones" and machine made. Bella is slightly surprised on hearing that the shoes are cheap. Matty tells her that he will make her a new pair. Matty is ambitious and wants to got to London where hand made shoes fetch a lot of money. The next day, Bella leaves for Aylesburgh to fetch leather. Con secretly follows Bella and when confronted by her, asks her if she would like to come out with him, sometimes. Bella tells him that she has no mind to go out with anyone. Con asks her to forget that he asked her out and leaves.

One day, Jedd arrives and is received at the station by his parents and Bella. Jedd and Bella go for a walk into the woods where Jedd flirts with her and she rejects his advances tactfully. That evening, Jedd and Ben get drunk at the pub. Arch is also seen at the pub but they dont know him. Late in the evening, Arch drops off Jedd and Ben at their house and drives away before Bella could see him. Jedd continues to flirt will Bella even at the house and Con does not like it. The next day, crop harvest begins and the entire family goes to work in the fields. During the break from work in the afternoon, Jedd insists that Bella (who is sitting under a tree) is not comfortable and takes her to a more shady spot by carrying her in his hands in spite of Bellas polite protests. Jedd also forcefully offers her water and Con, who cant take it anymore, attacks Jedd with a scythe. Jedd retaliates with a Rake (tool)|rake.
Mrs. Wainwright intervenes and disperses them.  She also scolds Bella as she was the reason for the fight. That night, the squire arranges a dinner party for all the villagers and Bella dances with each of the brothers in turn. During Cons turn, he steps on her foot as he is not a good dancer, hurting her. They get away from the crowd and Bella asks him why he fought Jedd. He tells her that Jedd did not listen to her and continued to offer her water which made him angry. She requests Con to watch his temper as she does not want any troubles in the family which took her in.

The next day, Bella sees Jedd off at the station and also finds Arch there. She follows him to his house and learns that he is married and has a child. She returns home dejected. She decides to leave Addisford and Mrs. Wainwright does not protest. When Con arrives home, his mother tells him that Bella is gone. He runs to the station and tries to convince Bella to stay. Bella tells him that she had been with another man and tells about the baby. Con replies that he loves her and her past does not matter. Bella returns home and Mrs. Wainwright receives her coldly. The next day, as they are sitting for breakfast, Mrs. Wainwright announces that Matty had left for London. During the breakfast, Con suddenly proposes to Bella before his parents. Ben is surprised but Mrs. Wainwright isnt. That day, Con and Bella attend a party where she runs into Arch. He tries to talk to her but she avoids him. The next day, Con takes Bella for boating in the river and there, they meet Arch again. Arch provokes Con by making fun of them and when the bullying gets worse, Con, in a fit of rage, kills Arch in spite of Bellas attempts to stop him.

Both Con and Bella flee Addisford to reach Selmouth. During the night they take refuge in a deserted house far from Addisford. Con is disgusted with himself and scared. Bella comforts him and they make love. The next day, they continue on their way to Selmouth where they will be taking a boat to leave for Dublin. In Selmouth, Con notices that Bella is acquainted with a lot of men and he becomes suspicious of her. At one point, when he sees Bella taking to the captain of the boat, he has visions of the captain and Bella kissing, laughing at him and he slapping Bella hard on the face. He realizes that he cant live with Bella due to his critical mentality. He tells her that he is going to turn himself in as he cant live with the guilt (the real reason is that he cant live with Bella without hurting her) and asks her to say goodbye. Bella protests violently, but he leaves and turns himself in. On the day of his execution, when Bella comes to visit him, she runs into his family. Mrs. Wainwright tries to attack her yelling "Damn you, Damn you!!". Bella sees Con for the last time, they dont talk much, but in the end are overcome with emotion and hug each other. Bella bids goodbye to him. Con is hanged as Bella is on the train to Selmouth. In the closing scene, Bella is shown standing on the boat deck, on her way to Dublin. She touches her stomach and smiles slightly to herself which suggests that she is pregnant with Cons child.

==Production==
Blists Hill Victorian Town was used as a filming location. 

==Cast==
starring
*Embeth Davidtz - Bella Ford Tom Bell - Ben Wainwright
*Gemma Jones - Mrs. Wainwright
*James Purefoy - Jedd Wainwright Kenneth Anderson - Matty Wainwright
*Greg Wise - Arch Wilson

and
*Ben Chaplin - Con Wainwright

with
*David Neal - Mitchy Mitchell
*Julian Protheroe - Bowler-Hatted Man
*Mark Neal - Clerk at Shoe Factory
*Tim Preece - Preacher
*Daphne Neville - Mrs. Mitchell
*Charles DeAth - Billy Swaine
*Colin Prockter - Man in Pub Richard Hope - Squire Wyman
*Kate Hamblyn - Harvest Girl
*Richard Hicks - Second Rowing Youth Arthur Kelly - Game Keeper
*Frederick Warder - Captain Rogers
*Colin Mayes - Seaman Tom Marshall - Prison Warder

==References==
 

==External links==
* 
* 
* 


 
 
 
 