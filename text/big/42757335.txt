She Couldn't Help It
{{infobox film
| title          = She Couldnt Help It
| image          = She Couldnt Help It (1920) - 1.jpg
| imagesize      =
| caption        = Newspaper ad
| director       = Maurice Campbell
| producer       = Realart Pictures (Adolph Zukor) Channing Pollock Douglas Bronston (film scenario)
| based on       =  
| starring       = Bebe Daniels
| music          =
| cinematography = H. Kinley Martin
| editing        =
| distributor    = Realart Pictures Corporation (Adolph Zukor)
| released       = December 1920
| runtime        = feature length
| country        = United States Silent (English intertitles)
}} lost  Channing Pollock.  

The novel and play were previously filmed in 1913 as In the Bishops Carriage starring Mary Pickford.

==Cast==
*Bebe Daniels - Young Nance Olden
*Emory Johnson - William Lattimer
*Wade Boteler - Tom Dorgan
*Vera Lewis - Mother Hogan
*Herbert Standing - Bishop Van Wagenen
*Z. Wall Covington - Mr. Ramsey
*Helen Raymond - Mrs. Ramsey
*Ruth Renick - Nellie Ramsey
*Gertrude Short - Mag Monahan
*Milla Davenport - Matron

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 