Warning Sign (film)
{{Infobox Film
| name           = Warning Sign
| image          = Warning sign poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Hal Barwood
| producer       = Hal Barwood Jim Bloom Matthew Robbins
| narrator       = 
| starring       = Sam Waterston Kathleen Quinlan Yaphet Kotto Jeffrey DeMunn
| music          = Craig Safan
| cinematography = Dean Cundey Robert Lawrence
| distributor    = 20th Century Fox
| released       = August 23, 1985
| runtime        = 99 min.
| country        = United States English
| budget         = 
| gross          = $1,918,117
| preceded_by    = 
| followed_by    = 
}}
 1985 science science fiction-horror film directed by Hal Barwood and starring Sam Waterston, Kathleen Quinlan, Yaphet Kotto, and Jeffrey DeMunn.

==Plot==
The movie tells about the outbreak of a virulent bacteria in a secret military laboratory operating under the guise of a pesticide manufacturer. During routine work a sealed tube is broken, releasing the secret biological weapon. Where upon detecting the release of the agent, one of the plants security officers activate "Protocol One", a procedure sealing all of the workers inside from the outside world and they are left to wait out the deadly effects. A local County Sheriff whose pregnant wife, the security officer, is trapped inside, and with the help of a past employee who is a known alcoholic, must fight through a government agency and the chemically affected workers to find his wife and put a stop to the spread of the lethal weapon. The former employee had started creating an antidote to the weapon, which the sheriff and his wife create and deploy.

==Reception==
 

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 