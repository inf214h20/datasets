Daughters Who Pay
{{Infobox film
| name           = Daughters Who Pay
| image          = 
| caption        = 
| director       = George Terwilliger
| producer       = 
| writer         = William B. Laub John Bowers
| music          = 
| cinematography = Edward Paul Murphy Darling Charles J. Davis
| editing        = 
| distributor    = Banner Productions
| released       =  
| runtime        = 
| country        = United States
| language       = Silent with English intertitles
| budget         = 
| gross          = 
}}
Daughters Who Pay is a 1925 American drama film directed by George Terwilliger, starring Marguerite De La Motte and featuring Béla Lugosi. A print of the film exists at George Eastman House.   

==Cast==
* Marguerite De La Motte as Sonia Borisoff / Margaret Smith John Bowers as Dick Foster
* J. Barney Sherry as Henry Foster
* Béla Lugosi as Serge Romonsky
* Marie Schaefer as Aunt Mary (as Marie Shaffer)
* Joseph Striker as Larry Smith

==See also==
* Béla Lugosi filmography

==References==
 

==External links==
* 

 
 
 
 
 
 

 