The Theatre Bizarre
 
{{Infobox film
| name = The Theatre Bizarre
| image = The Theatre Bizarre.jpg
| caption =  David Gregory Richard Stanley Jeremy Kasten
| producer = Daryl J Tucker David Gregory Richard Stanley
| starring = Kaniehtiio Horn Victoria Maurette Lena Kleine Catriona MacColl Udo Kier Virginia Newcomb
| music = Simon Boswell Marquis Howell of Hobo Jazz
| cinematography = Karim Hussain
| editing = Douglas Buck
| distributor = Severin Films 
| released =  
| runtime = 114 min.
| country = United States
| language = English
| budget = 
| gross =
}} horror anthology David Gregory, Richard Stanley.    The wraparound segments featuring Udo Kier were directed by Jeremy Kasten. 

==Plot==
The film contain six stories, each inspired by Paris’ legendary Grand Guignol theatre. 

The six stories are presented within a connecting framework, "Theatre Guignol": Enola Penny is intrigued by an abandoned theatre in her neighborhood. One night the theatre door mysteriously opens and she enters. A puppet host (or Guignol) introduces six short films. "Mother of Toads", "I Love You", "Wet Dreams", "The Accident", "Vision Stains" and "Sweets". As each is shown, the host becomes more human and Enola becomes more puppet-like.

"I Love You", "Wet Dreams" and "Sweets" match the Grand Guignol genre: physical or psychological conte cruel horror with natural explanations, cynical, amoral, ironic, sexy or gory in combinations.

"Mother of Toads" is loosely based on a supernatural horror story by Clark Ashton Smith. In "Vision Stains" a writer/serial killer injects fluid extracted from her victims eyes into her own to experience their lives for her journals. Most reviews note "The Accident" seems out of place: a mother and daughter thoughtfully discuss the nature of death after witnessing an accident.

==Cast==
*Udo Kier as Peg Poett
*Virginia Newcomb as Enola Penny James Gill as Donnie
*Kaniehtiio Horn as The Writer
*Victoria Maurette as Karina
*Shane Woodward as Martin
*Lena Kleine as Mother
*Catriona MacColl as Mere Antoinette
*Elissa Dowling as Subs
*Erin Marie Hogan as Test Tube Baby
*Erica Rhodes as Cellist
*Lorry OToole as Hot Waitress
*Lindsay Goranson as Estelle
*Jessica Remmers as Antonia
*Jeff Dylan Graham as Buzz
*Halfbreed Billy Gram as Dungeon Master
*Tom Savini as Dr. Maurey
*Cynthia Wu-Maheux as Junkie Girl
*Rachelle Glait as Homeless Woman
*Imogen Haworth as Pregnant Woman
*Tree Carr as Kissing Couple

==Production==
The film is a co-production between United States based Severin Films and France based Metaluna Productions. 

Each director was given the same budget, schedule and narrative directive.  Other than that, they were given free rein to create their 10-20 minute segments. 

Richard Stanleys segment is an adaptation of the short story "Mother of Toads" by Clark Ashton Smith. 

==Reception==
The film has received mainly mixed reviews, it currently holds a score of 43% on Rotten Tomatoes.  In an excerpt from the July 17, 2011 Fangoria review of the film, written by Michael Gingold, he says, "The many different flavors and tones in The Theatre Bizarre, courtesy of the many distinct talents who took part, means the movie ought to inspire lively debate among fans as to their favorites among the assorted stories. But regardless of your feelings about this or that individual episode, it’s guaranteed you’ll find enough to like to warrant enthusiastically recommending the movie overall." 

When the film played at the Oldenburg International Film Festival in Germany, it was reported that five people passed out at the screenings. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 