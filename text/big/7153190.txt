Puppet Master II
{{Infobox film 
|  name           = Puppet Master II: His Unholy Creations
|  image          = Puppet_Master_II.jpg
|  caption        = Film poster Dave Allen
|  producer       = Charles Band
|  writer         = David Pabian
|  starring       = Elizabeth Maclellan Collin Bernsen Gregory Webb Charlie Spradling Steve Welles Jeff Weston Ivan R. Jado Sage Allen George "Buck" Flower  and Nita Talbot
|  music          = Richard Band
|  cinematography = Thomas F. Denove
|  editing        = Bert Glatstein Peter Teschner Full Moon Entertainment Paramount Pictures
|  released       =  
|  runtime        = 88 minutes
|  country        = United States
|  language       = English
|  budget         = $780,000
}}
 Dave Allen. Puppet Master, and stars Elizabeth Maclellan, Gregory Webb, Charlie Spradling, Jeff Weston and Nita Talbot as paranormal investigators who are terrorized by the animate creations of an undead puppeteer, played by Steve Welles. Originally, Puppet Master II was intended to have the subtitle His Unholy Creations.

Puppet Master II, as well as the  ,   installments of the series, were only available in DVD format through a Full Moon Features box set that was briefly discontinued, until in 2007 when Full Moon Features reacquired the rights to the first five films. A remastered edition Blu-ray and DVD of the film was released on September 18, 2012. 

==Plot==
  Pinhead digging Andre Toulons Blade and Jester watching. After pouring the formula, the skeleton raises its arms, indicating that Andre Toulon is alive again. A few months later, a group of parapsychologists, led by Carolyn Bramwell (Elizabeth Maclellan), are sent to the hotel to investigate the strange murder of Megan Gallagher and the lunatic ravings of a now insane Alex Whitaker. It is explained that Megans brain was extracted through her nose (by Blade), and Alex, suspected of the murder, is now locked up in an asylum. While at the asylum, he begins to experience terrible seizures and premonitions.

That very evening, one of the investigators, Camille Kenney (Nita Talbot), decides to leave after spotting two of the puppets in her room. However, while packing, Pinhead and Jester attack and kidnap her. The next day, Carolyn talks to Michael (Collin Bernsen) about the disappearance of his mother, due to finding Camilles belongings and car still at the hotel. That very evening Carolyns brother Patrick (Gregory Webb) gets his head tunneled by Tunneler. Another investigator, Lance (Jeff Weston) runs in, knocks Tunneler out, and kills him by crushing him with a lamp. After dissecting Tunneler, they realize that the puppets are not remote controlled, but rather that their gears and wood are run by a chemical. From this, they deduce that the chemical must be the secret of artificial intelligence.

The next morning, while still trying understand the puppets motivation, a man named Eriquee Chaneé (Steve Welles) comes in, stating that he had inherited the hotel, and that he was in Bucharest while the investigators moved in. Afterwards, Camilles son Michael travels to the hotel, trying to figure out what happened to his mother. That very evening, Blade and Leech Woman go to a local farmers house, where Leech Woman kills the husband, Matthew (George "Buck" Flower), but gets thrown into the fireplace by the wife, Martha (Sage Allen). Just before Martha shoots Blade with her shotgun, a new puppet, Torch (Puppet Master)|Torch, walks in and burns Martha with his flame-throwing arm. It is then revealed that Eriquee is really Andre Toulon and he created Torch after being brought back to life, and he believes that Carolyn is a reincarnation of his now deceased wife, Elsa.

Toulon then has a flashback of him (played by  ), the remaining two investigators. While Wanda goes back to her room, Blade kills Lance, killing Wanda afterwards. After killing them, he uses their tissue for the formula.
 dumbwaiter opens, revealing Jester and Michaels dead mother, Camille. Toulon transfers his soul into one of the mannequins, and explains that after seeing Carolyn, he decided for them to live together forever. The puppets, upon hearing this, realize Toulon used them for his evil needs, and start torturing him. Michael then breaks into the room, saves Carolyn, and the two run out of the hotel. Up in the attic, Torch sets Toulon on fire, causing him to fall out a window and die. Afterward, Jester goes back to Camilles body with the remaining of the formula.

Several days later, it is revealed that Camilles soul has been put in the woman-sized mannequin, and is now running her own little puppet show. Blade, Pinhead, and Jester, are locked up in a cage, leaving Torch free. Camille takes them to the Bouldeston Institution for the mentally troubled tots and teens. Camille puts the puppets in the back of her car, and Torch up on the passengers seat, and drives off, leaving this movie as a cliff-hanger.

==Timeline Issue== Puppet Master, Andre Toulon killed himself on March 15, 1939. Despite the incorrect year, the month and day should remain the same.

==Cast== Andre Toulon, Eriquee Chaneé
* Elizabeth Maclellan – Carolyn Bramwell, Elsa Toulon Michael Todd – Puppet Toulon
* Julianne Mazziotti – Puppet Camille/Elsa
* Collin Bernsen – Michael Kenney
* Gregory Webb – Patrick Bramwell
* Charlie Spradling – Wanda
* Jeff Weston – Lance
* Nita Talbot – Camille Kenney
* Sage Allen – Martha
* George Buck Flower – Mathew
* Sean B. Ryan – Billy
* Ivan J. Rado – Cairo Merchant
* Taryn Band – Cairo Child
* Alex Band – Cairo Child

===Featured puppets=== Blade
* Pinhead
* Leech Woman Jester
* Tunneler Torch
* Mephisto
* Djinn

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 