In the Name of My Daughter
{{Infobox film
| name           = In the Name of My Daughter
| image          = LHomme quon aimait trop poster.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| producer       = Olivier Delbosc Marc Missonnier 
| screenplay     = Cédric Anger Jean-Charles Le Roux André Téchiné 
| based on       =  
| starring       = Catherine Deneuve Guillaume Canet Adèle Haenel
| music          = Benjamin Biolay 
| cinematography = Julien Hirsch 
| editing        = Hervé de Luze
| studio         = Fidélité Films
| distributor    = Mars Distribution 
| released       =  
| runtime        = 116 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

In the Name of My Daughter ( ; also known as French Riviera ) is a 2014 French drama film directed by André Téchiné and based on the memoir, Une femme face à la Mafia, written by Renée Le Roux and her son Jean-Charles Le Roux. The film was screened out of competition at the 2014 Cannes Film Festival.    It retraces the   which made headlines in the 1970s. 

In France, it was released in July 2014 and it attracted 300,373 audiences.  In Chennai the film was screened as the opening film of the 12th Chennai International Film Festival in December 2014.  
In January 2015, the film received two nominations at the 20th Lumières Awards.  The film will be released in US by Cohen Media Group on 8 May 2015. 

==Plot==
Agnès Le Roux, a young independent woman, returns to Nice in 1976 to have a new star in her life after a failed marriage. Her mother, Renée Le Roux, a wealthy widow, is fighting with other shareholders for control of the Palais de la Méditerranée, a casino on the French Rivera. The casino is facing difficulties and in one night, it loses five million francs to professional gamblers who very likely tampered a game.

Agnès, determined to make it on her own, opens a small bookstore where she sells African artifacts and Asian textiles. Renée, with the help of her lawyer and personal adviser, Maurice Agnelet, and her daughter’s decisive vote on her favor, takes control of the Palais de la Méditerranée. However, the mother-daughter relationship is strained. Harsh and straightforward, Renée refuses to give her daughter the share of Agnès’ inheritance from her late father. Maurice, ambitious and charming, is attracted to the beautiful, but stubborn Agnès. They become fast friends. Maurice is separated from his wife and has a small son to which he is close. He is also a ladies man. One of the women he is dating, Françoise, believing that he is having an affair with Agnès, visits her at the bookstore to warn her about Maurice. If she has not fallen for him yet, she eventually will. He beds all the young women around him.

As Maurice begins to open up to Agnès, she falls in love with him and they begin a passionate affair. Maurice’s high hopes of becoming the managing director of the casino are dashed when Renée gives the position to a more experienced manager. In revenge, Maurice decides to help Agnès get the three million francs inheritance her mother has refused to give her. The money is provided by Jean-Dominique Fratoni, an Italian  mafia boss and Renées business rival. Fratoni buys Agnès shares and she votes against her mother in the next board meeting. As a consequence, Renée loses her position undermining her wealth. Fratoni takes over the casinos operations only to have it closed. Agnès betrayal of her mother and Renée’s downfall make news headlines. Agnès guilt over the situation is eased by Maurice. Although she is aware of his faults, she opens a share bank account with him in Vevey, trusting him with her fortune. Renée is ruined. Mario, her faithful Italian chauffeur, tries to cheer her up, but he brings the subject of her daughters betrayal and she prefers not to talk about it.

Initially happy with Maurice, Agnès begins to ask from him more than he is willing to give and her love for him becomes oppressive. As she loves him more and more, Maurice pulls away from Agnès. When she appears, unexpectedly, to see him with his son, he humiliates her and forces her to apologize and smile a real smile for his forgiveness. Passing her breaking point, Agnès attempts to commit suicide with an overdosed of pills. Renée goes to the hospital to visit her, but Agnès refuses to see her mother. Maurice installs Agnès back in her home, but he is still despondent towards her. Shortly after, Agnès disappears without a trace. Her mother, having lost everything, takes on a 30-year crusade to prove that Maurice killed her daughter or had her killed by someone else.

Now elderly and frail, Renée finally succeeds in having the investigation into her daughters disappearance reopened. Maurice, who was living in Panama, returns voluntary to France to face the trial. His son, now an adult, is his main supporter. At the trial things get complicated for Maurice since Françoise testifies against him. She recounts a previous alibi she had provided for Maurice, saying that she was not with him in Switzerland at the time of Agnès disappearance. In front of the court, Renée pleads for justice for her daughter. However, Maurice is acquitted all charges. A title card informs the audience that in 2014, based on his sons testimony, Maurice was found guilty and condemned to twenty years in jail.

==Cast==
* Catherine Deneuve as Renée Le Roux 
* Guillaume Canet as Maurice Agnelet 
* Adèle Haenel as Agnès Le Roux 
* Judith Chemla as Françoise  
* Jean Corso as Fratoni  
* Laetitia Rosier as Annie 
* Mauro Conte as Mario  
* Pascal Mercier as Guérin 
* Tamara De Leener as Madeleine  
* Jean-Marie Tiercelin as Briault  
* Ali Af Shari as Reynier 
* Hubert Rollet as Palmero
* Tanya Lopert as Lydie
==Production==
In the Name of my daughter is a fictionalized account of the true story of the events surrounding the life of Agnès Le Roux, a casinos heiress, before and after her unresolved disappearance in the fall 1977. However, interviewed in May 2014, Téchiné commented: “I really have changed very little. I wanted the film to be very factual so I was very respectful of the events as they unfolded, while I was trying to depict the tragic relationship of these characters.” 

The film project started out as a commission, making a loose adaptation of the book of memoirs Une femme face à la Mafia (A woman up against the Mafia) written  by the real-life Renée Le Roux and her son Jean-Charles Le Roux. {{cite news
 | last =In The Name of My daughter presskit
 | first =
 | title =In The Name of My daughter
 | work =
 | date = 
 | url = http://www.medias.unifrance.org/medias/29/231/124701/presse/in-the-name-of-my-daughter-presskit-english.pdf | accessdate = 2015-03-28 }} 
The book tells the story of the casino wars on the French Riviera between the 1970’s – 1980’s, from the protagonist’s point of view. It includes the account of the take-over of Madame Le Roux’s Palais de le Mediterranee casino by Jean-Dominique Fratoni, with the support of Jacques Medecin, the then mayor of Nice. {{cite news
 | last =In The Name of My daughter presskit
 | first =
 | title =In The Name of My daughter
 | work =
 | date = 
 | url = http://www.medias.unifrance.org/medias/29/231/124701/presse/in-the-name-of-my-daughter-presskit-english.pdf | accessdate = 2015-03-28 }} 

Téchiné co-wrote the script with Jean-Charles Le Roux (Agnès brother) and Cédric Anger, director of Next Time I’ll Aim For The Heart (La Prochaine Fois Je Viserai Le Coeur) a film also starring Guillaume Canet. Rather than focusing in the judicial twists of Agnelets trial, Téchiné wanted to center the plot in the love and power struggle between Renée Le Roux, her daughter Agnès, and Maurice Agnelet: the iron-fisted mother, the rebellious daughter and Agnelet’s desire for recognition by society. {{cite news
 | last =In The Name of My daughter presskit
 | first =
 | title =In The Name of My daughter
 | work =
 | date = 
 | url = http://www.medias.unifrance.org/medias/29/231/124701/presse/in-the-name-of-my-daughter-presskit-english.pdf | accessdate = 2015-03-28 }} 
"It was Agnès that I was most interested in" Techine explains adding: " I wanted to paint her portrait. I agreed to make the film after reading the letters that Agnes had written to Agnelet because, quite unexpectedly, I found a surprising resemblance with another female character that I had long wanted to bring to the screen, Julie de Lespinasse. There are many parallels between the passionate love letters of this 18th century woman of letters and Agnes – heir to the Palais de la Mediterranee’s – letters. For example:“I love you how you must be loved, with excess, madness, ardor and despair.” {{cite news
 | last =In The Name of My daughter presskit
 | first =
 | title =In The Name of My daughter
 | work =
 | date = 
 | url = http://www.medias.unifrance.org/medias/29/231/124701/presse/in-the-name-of-my-daughter-presskit-english.pdf | accessdate = 2015-03-28 }} 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 