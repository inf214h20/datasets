A Tale of the Wind
{{Infobox film
| name = A Tale of the Wind
| image = A Tale of the Wind poster.jpg
| caption = Theatrical release poster
| director = Joris Ivens Marceline Loridan
| producer = Marceline Loridan
| writer = Joris Ivens Marceline Loridan
| starring = Joris Ivens
| music = Michel Portal
| cinematography = Thierry Arbogast Jacques Loiseleux
| editing = Geneviève Louveau
| studio = Capi Films La Sept Cinéma
| distributor = MK2 Diffusion
| released =  
| runtime = 77 minutes
| country = France French Mandarin Mandarin
| budget = 
| gross =
}}
A Tale of the Wind ( ) is a 1988 French film directed by Joris Ivens and Marceline Loridan. It is also known as A Wind Story. It stars Ivens as he travels in China and tries to capture winds on film, while he reflects on his life and career. The film blends real and fictional elements; it ranges from documentary footage to fantastical dream sequences and Peking opera. It was Ivens last film.

==Synopsis==
Next to a fast-spinning windmill, a young boy enters a large toy aeroplane and says he will fly to China. An old man, Joris Ivens, sits on a chair in the Gobi Desert. On the sand dunes around him a group of men are raising poles with microphones.

An old Chinese man practices martial arts with several younger men in front of a traditional Chinese building. Ivens, who is 90 years old, has been asthmatic since childhood, and asks the man how he manages to breathe so well. The literary character Sun Wukong, in a Peking opera appearance, watches from a tree. The man answers that "the secret of breathing lies in the rhythm of the autumn wind". The man begins to dance. Sun Wukong then throws a banana peel before the man, who slips and falls. Ivens helps him up.

Ivens visits the Golden Hands Buddha at the Dazu Rock Carvings. Intercut are Chinese landscapes seen from the air and footage of stormy weather. The Leshan Giant Buddha is seen. In the Gobi Desert, the team of technicians set up a camp. Ivens uses an inhaler and is swiftly examined by a doctor. A member of the group says that the wind will not appear for several days.

The following day, Ivens falls from his chair in the desert and is brought to a hospital. Sun Wukong visits him in the hospital bed. In an extended dream sequence, the spaceship from the film A Trip to the Moon brings Ivens to the Moon. There he encounters the goddess Change who tells him there is no wind on the Moon. Ivens finds that remarkable. In a stylised village where a wedding is taking place, a communist representative holds a speech about how fortunate the villagers are. Sun Wukong turns up and pulls the plug to the representatives microphone, and makes the loudspeakers play Western pop music instead. Ivens is briefly seen in Sun Wukongs make-up.

Ivens enters a cavern, where men greet him and say that he is expected. At the end of the cavern there is a mask from which a strong wind blows from the mouth. Ivens says that he wants to meet the man who made the mask, which is arranged. The man explains the masks mythological symbology and gives it to Ivens, who in return gives the man a print of his 1930 film Breakers.
 Japanese invasion of China, which Ivens filmed in 1938. Footage from the air is seen of the Great Wall of China, partially covered in desert sand. Trying to film the Terracotta Army, Ivens and Marceline Loridan only obtain permission to film for ten minutes, which would not be enough. Ivens therefore buys a large number of replicas from local tourist shops to create his own army, which he arranges and films, together with choreographed men dressed up in terracotta warrior costumes.

At Ivens camp in the Gobi Desert, and old woman approaches and says she can summon the wind by drawing a magic figure. To do this she demands two mechanical fans, which Ivens agrees to give her. Ivens explain how he for his entire career has tried to tame the wind by capturing it with the camera. The woman draws in the sand, and heavy wind begins to blow in the previously calm desert.

==Release==
The film premiered on 9 September 1988 at the 65th Venice Film Festival. It was released in France through MK2 Diffusion on 15 March 1989. A French DVD was released on 11 March 2009. 

===Critical response===
In   and Georges Méliès|Melies, between the two great early film makers who defined realism and fantasy. There is no better description of the place they map out beautifully in A Tale of the Wind. Its elliptical narrative will not appeal to every taste, but this is an eloquent last word from an important artist." 

===Accolades===
The film won the Special Award at the 1989 São Paulo International Film Festival.  It received the Documentary Film Jury Special Award at the 2nd European Film Awards. 

==References==
 

==External links==
*  
*   at Cinémathèque Française

 

 
 
 
 
 
 
 
 
 