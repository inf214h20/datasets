Dragon Lord
 
 
 
{{Infobox film name           = Dragon Lord image          = Dragon-Lord-poster.jpg caption        = Original Hong Kong poster. director       = Jackie Chan producer       = {{plainlist|
* Raymond Chow
* Leonard Ho
}} writer         = {{plainlist|
* Jackie Chan
* Edward Tang
* Barry Wong
}} starring       = {{plainlist|
* Jackie Chan Mars
* Hwang In-Shik
* Tien Feng
}} cinematography = {{plainlist|
* Chan Chung-yuen
* Chen Chin-kui
}} editing        = Peter Cheung distributor  Golden Harvest released       =   runtime        = 102 minutes (Hong Kong Version) country  Hong Kong language       = Cantonese
| gross         = HK $17,936,344
}} Police Story). 

==Synopsis==
Dragon (Jackie Chan) tries to send a love note to his girlfriend via a kite but the kite gets away and as he tries to get it back, he finds himself inside the headquarters of a gang of thieves who are planning to steal artifacts from China.

==Cast==
* Jackie Chan – Dragon Ho / Lung Mars – Cowboy Chin
* Hwang In-Shik – The Big Boss
* Tien Feng – Dragons Father
* Paul Chang – Chins Father
* Wai-Man Chan – Tiger (as Hui-Min Chen)
* Kang-Yeh Cheng – Ah Dee
* Fung Feng – The Referee
* Kang Ho – The Reteree
* Fung Hak-on – The Killer King (as Ke-An Fung)
* Kam-kwong Ho – The Commentator
* Pak-kwong Ho – Spectator
* Yeong-mun Kwon – The Hatchetman (as Kuen Wing-Man)
* Mang Hoi – Lu Chen gang member
* Lei Suet – Alice (as Sidney Yim)
* Corey Yuen – Lu Chen gang member
* Yuan-li Wu – The Matchmaker (as Yuen-Yee Ny)
*Yan Tsan Tang – Smuggler
*  Po Tai – Ah Dum Pao (as Tai Do)
* Clement Yip – Thug
* Benny Lai – Braves team player
* Johnny Cheung – Smuggler

==Production==
One of Chans complex scenes involved a Jianzi game requiring many takes for a single shot. {{cite web
  | last = Dixon
  | first = Melinda
  | title = Dragon Lord Review
  | work = DVD Bits
  | date = 2006-04-29
  | url = http://www.dvdbits.com/reviews.asp?id=3297
  | accessdate = 2007-06-09 }}   Dragon Lord went over budget and took twice as long to shoot as was originally planned due to Chans many retakes of shots to get them exactly as he wanted them.  The opening bun festival scene was originally intended to end the film but was moved as Chan wanted a spectacular opening to the film.  The final fight scene, which takes place in a barn, also featured elaborate stunts, including one where Chan does a back flip off a loft and falls to the lower ground. 

According to his book I Am Jackie Chan: My Life in Action, Chan injured his chin during a stunt, making it difficult to say his lines and direct.   

This is the first Jackie Chan film that includes outtakes (bloopers), which was inspired by Jackie Chan from The Cannonball Run. His later films all include outtakes.   

==Release==
In its original Hong Kong theatrical run, Dragon Lord grossed HK $17,936,344.   The film did not make as much as it was expected to in Hong Kong, but was a big hit in Japan. 

Hong Kong Legends released a DVD on 25 August 2003 in the United Kingdom.  Dimension Films released the film on DVD in the U.S. on 11 May 2004.   

==Reception==
Joey OBryan of The Austin Chronicle rated it 2.5/5 stars and wrote that the film, while not one of Chans best, is an early attempt to take the genre into a new direction and set the stage for many of Chans better, more-realized films.  OBryan highlighted the films climactic fight as a "worth the price of admission all by itself".   TV Guide rated it 3/5 stars and wrote, "Aside from the meandering, stop-and-go screenplay, theres much to admire about the film. "   John Sinnott of DVD Talk rated it 3.5/5 stars called it a "fun movie" that moves away from conventional martial arts films. 

===Awards and nominations===
* 1983 Hong Kong Film Awards:
** Nomination:Best Action Choreography (Jackie Chan, Fung Hak-on, Yuen Kuni)

==References==
 

==External links==
*   at Hong Kong Cinemagic
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 