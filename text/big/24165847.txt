The Girl in the Red Velvet Swing
{{Infobox film
| name           = The Girl in the Red Velvet Swing
| image          = Poster of the movie The Girl in the Red Velvet Swing.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Richard Fleischer
| producer       = Charles Brackett
| writer         = Walter Reisch Charles Brackett
| narrator       = 
| starring       = Ray Milland Joan Collins Farley Granger
| music          = Leigh Harline
| cinematography = Milton R. Krasner
| editing        = William Mace
| distributor    = 20th Century Fox
| released       = October 1, 1955
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $1.7 million 
| gross          = $1.3 million (US) 
}}
The Girl in the Red Velvet Swing is a 1955 film directed by Richard Fleischer from a screenplay by Walter Reisch and Charles Brackett, and starring Joan Collins, Ray Milland, and Farley Granger. The film was released by Twentieth Century-Fox, which had originally planned to put Marilyn Monroe in the title role, and then suspended her when she refused to do the film. 

==Plot==
The film relates the fictionalized story of Evelyn Nesbit. Nesbit was a model and actress who became embroiled in the scandal surrounding the June 1906 murder of her former lover, architect Stanford White, by her husband, rail and coal tycoon Harry Kendall Thaw. Nesbit served as a technical adviser on the film.

==Cast==
* Ray Milland as Stanford White
* Joan Collins as Evelyn Nesbit Thaw
* Farley Granger as Harry Kendall Thaw
* Luther Adler as Delphin Delmas
* Cornelia Otis Skinner as Mrs. Thaw
* Glenda Farrell as Mrs. Nesbit
* Frances Fuller as Mrs. Elizabeth White
* Phillip Reed as Robert Collier
* Gale Robbins as Gwen Arden
* James Lorimer as McCaleb
* John Hoyt as William Travers Jerome
* Robert F. Simon as Stage Manager
* Harvey Stephens as Dr. Hollingshead
* Emile Meyer as Hunchbacher

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 