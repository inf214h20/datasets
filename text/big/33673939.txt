Kerala House Udan Vilpanakku
 
{{Infobox film
| name           = Kerala House Udan Vilpanakku
| image          = 
| caption        = 
| director       = Thaha
| producer       = Ousepachan(Joseph Valakushy)
| writer         = 
| screenplay     = Kaloor Dennis
| story          = Girish Puthenchery
| based on       =  
| starring       = Jayasurya Girly Harisree Asokan Cochin Hanifa Narendra Prasad
| music          = Ousepachan
| cinematography = R. Selva
| editing        = P. C. Mohanan
| studio         = Chitrangana Films
| distributor    = Chitrangana Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Kerala House Udan Vilpanakku is a 2004 Malayalam comedy drama film directed by Thaha and starring Jayasurya, Girly, Harisree Asokan, Cochin Hanifa and Narendra Prasad. The film deals with the problems faced by the protagonist in selling his property which is located in Kerala-Tamil Nadu border in Walayar. It was moderately received by the critics as well as audience.

==Cast==
* Jayasurya as Dinesh Kondody
* Girly as Sundari
* Harisree Asokan as Vallabhan
* Cochin Hanifa as Vadival Vasu
* Narendra Prasad as Paramu Nair
* Salim Kumar as Tester Kannappan
* Geetha Salam as Ammavan
* Kovai Sarala as Parvathi Ammal
*Narayanankutty Saikumar as Mahendran Kondody
* Manian Pillai Raju as Pisharadi
* Sadiq as Idiyan Thoma

==External links==
*  

 
 
 

 
 