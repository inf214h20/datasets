Manmadhudu
{{Infobox film
| name           = Manmadhudu
| image          = manmadhudu.jpg
| writer         = Trivikram Srinivas  
| screenplay     = K. Vijaya Bhaskar Nagarjuna Akkineni
| director       = K. Vijaya Bhaskar Nagarjuna Akkineni Sonali Bendre Anshu
| music          = Devi Sri Prasad
| editing        = A. Sreekar Prasad
| cinematography = Sameer Reddy
| costume        = Asmita Marwa
| studio         = Annapurna Studios
| released       =   
| runtime        = 142 minutes
| language       = Telugu
| country        = India
| budget         =
| gross          =
}}
 Telugu romance romantic comedy Nagarjuna Akkineni Nagarjuna Akkineni, Sonali Bendre, Anshu in the lead roles and has a highly successful soundtrack composed by Devi Sri Prasad. The story and dialogues for the film were written by acclaimed writer Trivikram Srinivas. The film was released on December 20, 2002 to highly positive reviews and further went on to become one of the biggest hits in Akkineni Nagarjuna|Nagarjunas career.
The film recorded as Super Hit at box office. The film was later remade as Aishwarya (film)|Aishwarya in Kannada, marking the debut of Deepika Padukone and also remade in Bengali as Priyotoma. 

==Plot== Nagarjuna Akkineni) is a manager in his ad agency. He despises women and feels that all women are traitors. Abhirams uncle (Tanikella Bharani), who is the chairman of the ad agency, appoints Harika (Sonali Bendre) as the assistant manager. Though Abhiram does not like a girl joining his company, he has to live with it as the appointment is made by his uncle. He starts mistreating her. Harika, vexed by Abhirams acts, submits her resignation to his uncle, who then narrates Abhirams past to Harika.
 Chandra Mohan) in Abhis grandfathers company. Alarmed by this, Maheshwaris uncle takes her away to his home town and arranges her engagement with another man. Abhi travels all the way to the venue and takes Maheswari away. While returning, they meet with an accident. After 10 days, when Abhi comes out of coma, he is told that Maheswari is about to marry some other guy. Shocked by this news, Abhi develops hatred towards women. After narrating the flashback, Abhis uncle promotes Harika as the Manager and demotes Abhi to the post of Assistant Manager. Although Abhi resents this, he travels to Paris with Harika for a business venture. While there, he falls in love with her. Problems soon arise, and when they return, Harika is arranged to marry another man. However, Abhi realizes his true feelings for her at last, and they end up together. 

==Cast==
  was selected as lead heroine marking her first collaboration with Nagarjuna.]]
 
* Akkineni Nagarjuna as Abhiram
* Sonali Bendre as Harika
* Anshu as Maheswari Chandra Mohan as Maheswaris uncle
* Tanikella Bharani as Prasad
* Brahmanandam as Lavangam
* Dharmavarapu Subramanyam as Balasubramaniam Sunil as Bunk Seenu
* M. Balaiah as Abhirams Grandfather Ranganath as Father of Harika
* Jaya Prakash Reddy Melkote as Sundar
* Ananth as Subba Rao
* Rekha Vedavyas (Cameo)
* Keerthi Chawla (Cameo)
* Sudha as Lakshmi
* Siva Parvathi
* Anitha Chowdary as Anchor Swapna Madhuri as Swapna
* Swetha
* Sumalatha
* Deepika
* Devisri
* Master Tanish as Young brother to Harika
 

==Reception==

The movie received highly positive critical feedback. Particularly, Akkineni Nagarjuna, Trivikram Srinivas and Devi Sri Prasad received rave reviews for their respective works in this film. Popular reviewing website Idlebrain rated 4.5 out of 5 stating "When director and hero have latest hits behind their back, the comparison is inevitable. This film is definitely better than Santosham (Nagarjunas earlier film). Manmadhudu is as good as Nuvvu Naaku Nachav (Vijaya Bhaskars earlier film). First half of the film is extremely entertaining (total comedy + flashback romance). And second half (comedy + story) is even better than first half. 95% of the scenes in the film are comedy dialogues. There are few sentiment scenes. But these very few sentiment scenes are bound to leave impact on you. Nagarjuna dominated the show and won accolades from audiences. Its a must recommended hilarious film for all Telugu film lovers."  Another popular reviewing website Sify gave an extremely positive review and wrote "Though the story is wafer thin, the director has been able to package the film well with an innovative presentation. Nagarjuna and Sonali are excellent and have played their roles with conviction. The music of Devi Sri Prasad is hummable. The film is out to wallop some fun ‘n’ frolic.", giving a verdict that it is a "Rip-Roaring Entertainer".  Another reviewing website fullhyd.com gave a positive review stating "Nagarjuna lends significantly to the character of Abhiram in one of his slickest cinematic performances ever. He perfectly captures the liveliness, complexity and frustration of Abhiram. Unquestionably, Manmathudu proves to be an enthralling vehicle for his crowd-pleasing comeback. Sonali gives a commendable performance as a young and struggling art director who is in love with Abhiram. Devi Sri Prasads romantic and sometimes ominous musical score nicely complements the images on screen. The dialogues, penned by Trivikram, are chronically mind-boggling. Manmathudu gratifies on many levels. The direction maintains pace and intrigue, the performances are spot on, and the locations are simply sumptuous, be it Smokey Alps or a clean Hyderabad road. All things considered, if you can stomach the thirty minutes of echoing flashback, Manmathudu makes for one intense and rousing entertainment, and if youre on its quirky wavelength, it might just strike you as one of the funniest movies youve ever seen!" 

==Soundtrack==
{{Infobox album
| Name        = Manmadhudu
| Tagline     =
| Type        = film
| Artist      = Devi Sri Prasad
| Cover       =
| Released    = 2002
| Recorded    =
| Genre       = Soundtrack
| Length      = 27:04
| Label       = Sohan Music
| Producer    = Devi Sri Prasad
| Reviews     =
| Last album  = Khadgam   (2002)
| This album  = Manmadhudu   (2002)
| Next album  = Thotti Gang   (2002)
}}
 Sunitha anchored Seetharama Sastry, actors Brahmanandam and Tanikella Bharani. 

Addressing the fans, Nagarjuna said "I saw the complete movie yesterday and it came out really well. I can assure you that Manmadhudu would become a super duper hit." 

All songs are composed by Devi Sri Prasad. Five out of six songs had lyrics by Sirivennela Sitaramasastri. Music released was on SOHAN Music Company. Often being termed as one of Devi Sri Prasads finest works, the music got rave reviews and went out to become a huge success commercially.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:04
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Don`t Marry Sirivennela Sitarama Sastry
| extra1  = S. P. Balasubrahmanyam|S. P. Balu
| length1 = 5:41

| title2  = Gundello Emundho
| lyrics2 = Sirivennela Sitarama Sastry
| extra2  = Venu, Sumangali
| length2 = 4:40

| title3  = Nenu Nenuga Lene
| lyrics3 = Sirivennela Sitarama Sastry
| extra3  = S. P. B. Charan
| length3 = 3:56

| title4  = Na Manasune
| lyrics4 = Sirivennela Sitarama Sastry Chitra
| length4 = 4:09

| title5  = Cheliya Cheliya
| lyrics5 = Sirivennela Sitarama Sastry  Shaan
| length5 = 4:16

| title6  = Andhamaina Bhamalu
| lyrics6 = Bhuvanachandra
| extra6  = Devi Sri Prasad
| length6 = 4:20
}}

==Remakes==
The film was remade in Kannada as Aishwarya (film)|Aishwarya with Upendra and Deepika padukone.
It was also remade in Bengali as Priyotoma with Jeet (actor)|Jeet.

==Notes==
The film was appreciated for its slapstick comedy and Trivikram Srinivass witty dailogues. The red sports car that Abhiram drives in the movie is a 1992 Nissan 300sx. This model was discontinued in 1994 and would have been an import. The movie has references to tehelka.com.

==Box office performance==
The film collected nearly   37 crore in 50 days during its theatrical run. It was box office blockbuster hit. It was the first Telugu movie to run 50 days in USA. It ran in 30+ theaters for 100days.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 