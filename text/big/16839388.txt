Cars 2
 
 
{{Infobox film
| name           = Cars 2
| image          = Cars 2 Poster.jpg
| caption        = Theatrical poster
| director       = John Lasseter
| producer       = Denise Ream
| screenplay     = Ben Queen
| story          = John Lasseter Brad Lewis Dan Fogelman
| starring       = Owen Wilson Larry the Cable Guy Michael Caine Emily Mortimer John Turturro Eddie Izzard
| music          = Michael Giacchino
| cinematography = Jeremy Lasky Sharon Calahan
| editing        = Stephen Schaffer
| studio         = Walt Disney Pictures Pixar Animation Studios Walt Disney Studios Motion Pictures
| released       =   
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $200&nbsp;million 
| gross          =  $559.9 million   
}} Pixar Animation Mater head Grand Prix, but Mater becomes sidetracked with international espionage.           The film is directed by John Lasseter, co-directed by Brad Lewis, written by Ben Queen, and produced by Denise Ream.     

Cars 2 was released in the United States on June 24, 2011 (five years after the first film). The film was presented in Disney Digital 3D and IMAX 3D, as well as traditional two-dimensional and IMAX formats.   
The film was first announced in 2008, alongside Up (2009 film)|Up, Pixar#Product pipeline|Newt, and Brave (2012 film)|Brave, and it is the 12th animated film from the studio.       Even though the film received mixed reviews from critics, breaking the studios streak of critical success, it ranked No. 1 on its opening weekend in the U.S. and Canada with $66,135,507 and topping international success of such previous Pixar works as Toy Story, A Bugs Life, Toy Story 2, Monsters, Inc., Cars, and WALL-E. 

==Plot== spy Finn fake his death.
 Mater and oil tycoon green power formula race car Francesco Bernoulli challenges McQueen, he and Mater—along with Luigi (Cars)|Luigi, Guido (Cars)|Guido, Fillmore (Cars)|Fillmore, and Sarge (Cars)|Sarge—depart for Tokyo|Tokyo, Japan for the World Grand Prix.
 Professor Zündapp, ignite the Allinol fuel. McMissile and his partner Holley Shiftwell attempt to rendezvous with American spy car Rod "Torque" Redline at a World Grand Prix promotional event in Tokyo, to receive information about the mastermind. However, Redline is then attacked by Zündapps henchmen and passes his information to Mater before he is captured. Holley and Finn mistake Mater as their American contact. Zündapp tortures Redline and reveals that Allinol can ignite if impacted by a high electromagnetic pulse.  Zündapp demonstrates it on Redline, killing him, but not before they realize that he passed this information to Mater.

During the first race, Finn and Holley help Mater evade Zündapps henchmen. In the process, Mater inadvertently gives McQueen bad racing advice, which causes him to lose the race to Bernoulli. Meanwhile, Zündapp uses the weapon on several race cars. After McQueen falls out with Mater, Finn (who still thinks the tow truck is an real American spy) drafts him into foiling Zündapps plot. Finn and Mater escape Zündapps henchmen and climb aboard Siddeley (Cars)|Siddeley. Finn and Holley remove the tracking device on Mater and discover in it a picture of a mysterious British engine, which Mater identified by the engine and some rare parts.

Finn, Holley, and Mater all fly to Paris|Paris, France, where they go into a black-market and meet an old friend of Finns, Tomber (Cars)|Tomber. Finn and Holley show the mysterious engine to Tomber, who tells them that the car with the engine was his best customer, but he has never met him in person. Mater explains what he knows about the evil Lemons, and realizes that every Lemon involved with the plot is one of "Lemon (automobile)|historys biggest loser cars." Tomber tells Finn, Holley, and Mater that the lemons are going to have a secret meeting in Porto Corsa, Italy, where the next race in the World Grand Prix would be held.

In Italy, Mater infiltrates the criminals meeting and discovers Zündapps plan. Zündapps henchmen, meanwhile, use their weapon on several more cars during the race, eventually causing a Multi-vehicle collision|multi-car pileup on the Casino Bridge. With the Allinol fuel under suspicion, Axlerod suspends its use for the final race in England. However, McQueen chooses to continue using it. The criminals then decide to kill McQueen in the next race and upon hearing this, Mater is exposed and is captured, along with Finn and Holley, and tied up inside Big Bentleys bell tower in London|London, England.

Mater realizes how foolishly he has been acting. The criminals use the weapon on McQueen during the race, but nothing happens. Mater escapes to warn his friends of a bomb planted in McQueens pit stop|pit, but Finn and Holley find out that the bomb is actually planted Maters air-filter. They warn him about the bomb, but he flees to protect his friends. However, he is pursued by McQueen in an attempt to reconcile, unaware of the danger until they are beyond the range of Zündapps remote detonator. He sends his henchmen to kill McQueen and Mater, but they are foiled by the combined efforts of Finn, Holley, and the Radiator Springs residents. Upon his capture, Zündapp reveals that only the person who installed the bomb can deactivate it. Mater realizes that Axlerod is the mastermind behind the plot and confronts Axlerod, trapping him next to the bomb. Axlerod deactivates the bomb at the last second and he, Zündapp, and the lemons are all arrested by the police.
 Queen and returns home with his friends, where the cars from the World Grand Prix take part in the unofficial Radiator Springs race. Fillmore reveals that before the last race, Sarge replaced McQueens Allinol with Fillmores organic fuel (which prevented McQueen from being affected by the weapon).  McMissile and Shiftwell arrive and invite Mater to join them in another spy mission, but he graciously turns it down.  He does ask Shiftwell for a date when she returns which she accepts. He gets to keep the rockets they gave him earlier, which he now uses in the Radiator Springs race.

==Voice cast==
  Tony Trihull). Paul Newman (who voiced Doc Hudson) died of cancer on September 26, 2008. After Newmans death, Lasseter said they would "see how the story goes with Doc Hudson."  Doc was eventually written out,    with a few references to the character, where he is thought to have died before the events of the movie, as Mater says that he would have been proud for McQueens Piston Cups, which have been renamed after Doc; also, in the Tokyo race, one of the announcers says that Doc was one of the best dirt racers ever.

===Main characters===
* Larry the Cable Guy as Mater (Cars)|Mater, a Southern-accented tow truck from Radiator Springs.
* Owen Wilson as Lightning McQueen, a Piston Cup racecar.
* Michael Caine as Finn McMissile, a British spy car.
* Emily Mortimer as Holley Shiftwell, a fellow British intelligence agent, new to field work. 
* John Turturro as Francesco Bernoulli, McQueens main racing rival from Italy.
* Eddie Izzard as Sir Miles Axlerod, a British electric car who created Allinol.
* Thomas Kretschmann as Professor Zündapp, the doctor from Germany, Axlerods assistant.
* Joe Mantegna and Peter Jacobson as Grem and Acer (Cars)|Acer: Professor Zündapps henchmen.
* Bruce Campbell as Rod "Torque" Redline, an American spy car.

===Supporting=== Luigi
* Darrell Waltrip as Darrell Cartrip Guido
* Brent Musburger as Brent Mustangburger
* Colin Cowherd as Colin Cowling
* Jason Isaacs as Siddeley (Cars)|Siddeley/Leland Turbo David Hobbs as David Hobbscap. Jacques Villeneuve voices the character in French releases. Victor Hugo
* Lloyd Sherr as Fillmore (Cars)|Fillmore/Tony Trihull Sarge
* Tomber
* Sig Hansen as Crabby the Boat
* Franco Nero as Uncle Topolino The Queen. Sophia Loren provides the Italian dub of Topolino. 
* Bonnie Hunt as Sally Carrera Ramone
* Flo
* Sheriff
* Lizzie
* Mack
* Jeff Garlin as Otis
* Patrick Walker as Mel Dorado Lewis Hamilton
* Velibor Topic as Alexander Hugo Greg Ellis as Nigel Gearsley
* John Mainier as J. Curby Gremlin
* Brad Lewis as Tubbs Pacer Van
* Edie McClurg as Minny
* Teresa Gallagher as Maters Computer
* Jeff Gordon as Jeff Gorvette
* John Lasseter as John Lassetire

In international versions of the film, the character Jeff Gorvette is replaced with race car drivers better known in the specific countries in his dialogue scenes (however, he still appears as a competitor). 
* Mark Winterbottom as Frosty (Australian release)      
* Fernando Alonso as Fernando Alonso (Spanish release)
* Vitaly Petrov as Vitaly Petrov (Russian release)
* Jan Nilsson as Flash (Swedish release) 
* Memo Rojas (Latin American release)
* Sebastian Vettel as Sebastian Schnell (German release)

In Brazil, Gorvette is replaced by Carla Veloso in his dialogue scenes (Carla appears in all other versions of the film, but with no lines); Carla is voiced by Brazilian singer   and  . 

==Production==

===Development===
 
Cars (film)|Cars is the second Pixar film, after Toy Story, to have a sequel as well as becoming a franchise.    John Lasseter, the director of the film, said that he was convinced of the sequels story while traveling around the world promoting the first film. He said:

 

Cars 2 was originally scheduled for a summer 2012 release, but Pixar moved the release up by a year.   

In 2009, Disney registered several domain names, hinting to audiences that the title and theme of the film would be in relation to a World Grand Prix. 
 sued Disney breach of actual or statutory damages. On May 13, 2011, Disney responded to the lawsuit, denying "each and every one of Plaintiffs legal claims concerning the purported copyright infringement and substantial similarity of the parties respective works."    On July 27, 2011, the lawsuit was dismissed by a district court judge who, in her ruling, wrote that the "Defendants have sufficiently shown that the Parties respective works are not substantially similar in their protectable elements as a matter of law".   

===Casting=== David Hobbs, John Turturro, and Eddie Izzard.   

==Soundtrack==
 
{{Infobox album  
| Name        = Cars 2
| Type        = Soundtrack
| Artist      = Michael Giacchino
| Cover       =
| Caption= 
| Released    = June 14, 2011
| Recorded    = Score
| Length      = 63:24 Walt Disney
| Producer    =
| Reviews     =
| Chronology  = Michael Giacchino
| Last album  =   (2010)
| This album  = Cars 2 (2011) Super 8 (2011)
| Misc        = {{Extra chronology
| Artist      = Pixar film soundtrack
| Type        = Soundtrack Toy Story 3 (2010)
| This album  = Cars 2 (2011)
| Next album  = Brave (soundtrack)|Brave (2012)
}}
}}
{{Album ratings rev1 =   rev1score =   rev2 =   rev2score =  
}}
The Cars 2 soundtrack was released on both CD album and digital download June 14. It is the fourth Pixar film to be scored by Michael Giacchino after The Incredibles, Ratatouille (film)|Ratatouille and Up (2009 film)|Up.  It also marks the first time that Giacchino has worked with John Lasseter as a director, as Lasseter had been executive producer on Giacchinos previous three Pixar films and that Lasseter hasnt worked with Randy Newman.

{{Track listing
| total_length    =
| all_music     = Michael Giacchino, except as noted
| extra_column    = Artist
| writing_credits = yes
| title1          = You Might Think
| note1           = Cover of The Cars
| writer1         = Ric Ocasek
| extra1          = Weezer
| length1         = 3:07
| title2          = Collision of Worlds
| writer2         = Paisley, Williams
| extra2          = Brad Paisley and Robbie Williams
| length2         = 3:36
| title3          = Mon Cœur Fait Vroum  (My Heart Goes Vroom) 
| writer3         = Michael Giacchino
| extra3          = Bénabar
| length3         = 2:49
| title4          = Nobodys Fool
| writer4         = Paisley
| extra4          = Brad Paisley
| length4         = 4:17 Polyrhythm
| writer5         = Yasutaka Nakata Perfume
| length5         = 4:09
| title6          = Turbo Transmission
| extra6          =
| length6         = 0:52
| title7          = Its Finn McMissile!
| extra7          =
| length7         = 5:54
| title8          = Mater the Waiter
| extra8          =
| length8         = 0:43
| title9          = Radiator Reunion
| extra9          =
| length9         = 1:40
| title10         = Cranking Up the Heat
| extra10         =
| length10        = 1:59
| title11         = Towkyo Takeout
| extra11         =
| length11        = 5:40
| title12         = Tarmac the Magnificent
| extra12         =
| length12        = 3:27
| title13         = Whose Engine Is This?
| extra13         =
| length13        = 1:22
| title14         = Historys Biggest Loser Cars
| extra14         =
| length14        = 2:26
| title15         = Mater of Disguise
| extra15         =
| length15        = 0:48
| title16         = Porto Corsa
| extra16         =
| length16        = 2:55
| title17         = The Lemon Pledge
| extra17         =
| length17        = 2:13
| title18         = Maters Getaway
| extra18         =
| length18        = 0:59
| title19         = Mater Warns McQueen
| extra19         =
| length19        = 1:31
| title20         = Going to the Backup Plan
| extra20         =
| length20        = 2:24
| title21         = Maters the Bomb
| extra21        =
| length21        = 3:17
| title22         = Blunder and Lightning
| extra22         =
| length22        = 2:17
| title23         = The Other Shoot
| extra23         =
| length23        = 1:03
| title24         = Axlerod Exposed
| extra24         =
| length24        = 2:22
| title25         = The Radiator Springs Grand Prix
| extra25         =
| length25        = 1:30
| title26         = The Turbomater
| extra26         =
| length26        = 0:50
}}

==Release==
During the Summer of 2008, John Lasseter announced that Cars 2 would be pushed forward and released in the summer of 2011, one year earlier than its original 2012 release date.     The US release date was later confirmed to be June 24, 2011, with a UK release date set for July 22, 2011.     The world premiere of the film took place at the El Capitan Theatre in Hollywood on June 18, 2011.    Cars 2 was released in 4,115 theaters in the USA and Canada    setting a record-high for a G-rated film  and for Pixar. The latter was surpassed by Brave (2012 film)|Brave (4,164 theaters). 

===Short film===
 
The film was preceded by a short film titled Hawaiian Vacation, directed by Gary Rydstrom and starring the characters of the Toy Story franchise.

===Home media===
The film was released on  , Cars 2, and Maters Tall Tales). The film was also released as a Movie Download edition in both standard and high definition.   

The Movie Download version includes four bonus features: the new Cars Toon “Air Mater,” the Toy Story Toon “Hawaiian Vacation,” “World Tour Interactive Feature," and "Bringing Cars 2 to the World." The 1-disc DVD and 2-disc Blu-ray/DVD combo pack versions include the shorts “Air Mater” and “Hawaiian Vacation,” plus the "Director John Lasseter Commentary." The 5-disc combo pack includes all of the same bonus features as the 1-disc DVD and 2-disc Blu-ray/DVD combo pack versions, in addition to “World Tour Interactive Feature" and "Sneak Peek: The Nuts and Bolts of Cars Land." The 11-disc three movie collection comes packaged with Cars (Blu-ray, DVD, and Digital Copy), Cars 2 (Blu-ray 3D, Blu-ray, DVD, and Digital Copy), and Maters Tall Tales (Blu-ray, DVD, and Digital Copy). 

Cars 2 sold a total of 1,983,374 DVD units during its opening week,  generating $31.24 million and claiming first place.  It also finished on the top spot on the Blu-ray chart during its first week, selling 1.76 million units and generating $44.57 million. Its Blu-ray share of home media was 47%, indicating an unexpectedly major shift of sales from DVD to Blu-ray.  Blu-ray 3D contributed to this, accounting for 17% of total disc sales. 

==Reception==

===Critical response=== weighted average score out of 100 to reviews from mainstream critics, calculated an average score of 57/100 based on 38 reviews, indicating "mixed or average reviews".  "The original Cars was not greeted with exceptional warmth," said The New York Times, "but the sequel generated Pixars first truly negative response."   G rating, the focus on Mater and felt the film lacked warmth and charm, while also feeling the film was made as an exercise in target marketing.     Reviewing the film for The Wall Street Journal, Joe Morgenstern wrote, “This frenzied sequel seldom gets beyond mediocrity."    Entertainment Weekly critic Owen Gleiberman said, "Cars 2 is a movie so stuffed with "fun" that it went right off the rails. What on earth was the gifted director-mogul John Lasseter thinking – that he wanted kids to come out of this movie was   more ADD?"  Although Leonard Maltin on IndieWire claimed that he had "such high regard for Pixar and its creative team headed by John Lasseter" he said he found the plot "confusing" and felt that Tow Maters voice annoying saying that hed "rather listen to chalk on a blackboard than spend nearly two hours with Tow Mater."  Considering the low reviews given to the Pixar production, critic Kyle Smith of the New York Post said, "They said it couldnt be done. But Pixar proved the yaysayers wrong when it made its first bad movie, Cars. Now it has worsted itself with the even more awful Cars 2." 
 to drive merchandising sales.     Lasseter vehemently denied these claims, which he attributed to "people who don’t know the facts, rushing to judge."  Some theorized that the vitriol was less about the film but more about Pixars broadened focus to sequels. The New York Times reported that although one negatively reviewed film would not be enough to scratch the studio, "the commentary did dent morale at the studio, which until then had enjoyed an unbroken and perhaps unprecedented run of critical acclaim."   

===Box office===
Cars 2 grossed $191,452,396 in the USA and Canada, and $368,400,000 in other territories for a worldwide total of $559,852,396.  Worldwide on its opening weekend it grossed $109.0&nbsp;million, marking the largest opening weekend for a 2011 animated title.  Overall, Cars 2 became seventh biggest Pixar film in worldwide box office among the fourteen released.

Cars 2 made $25.7&nbsp;million on its debut Friday (June 24, 2011), marking the third-largest opening day for a Pixar film after Toy Story 3s $41.1&nbsp;million. At the time, though, it was the third least-attended opening day for a Pixar film, only ahead of Up (2009 film)|Up and Ratatouille (film)|Ratatouille.  It also scored the sixth largest opening day for an animated feature.  On its opening weekend as a whole, Cars 2 debuted at No.1 with $66.1&nbsp;million,  marking the largest opening weekend for a 2011 animated feature, the seventh largest opening for Pixar,  the eighth largest among films released in June,  and the fourth largest for a G-rated film.  In its second weekend, however, the film dropped 60.3%, the largest second weekend drop ever for a Pixar film, and grossed $26.2&nbsp;million.  It is the only Pixar film that missed the $200-million mark since A Bugs Life    and it is also the least attended Pixar film ever. 

Outside North America, it grossed $42.9&nbsp;million during its first weekend from 3,129 theaters in 18 countries, topping the box office.    It performed especially well in Russia where it grossed $9.42 million,  marking the best opening weekend for a Disney or Pixar animated feature and surpassing the entire runs of Cars and Toy Story 3.    In Mexico, it made $8.24 million during its first weekend,  while in Brazil, it topped the box office with $5.19 million ($7.08 million with previews).  It also premeiered at No.1 with $5.16 million in Australia,  where it debuted simultaneously with Kung Fu Panda 2 and out-grossed it.  It is the highest-grossing film of 2011 in Lithuania ($477,117),  Argentina ($11,996,480).  It is the highest-grossing animated film of 2011 in Estonia ($442,707),  Finland ($3,230,314),  Norway ($5,762,653). 

===Accolades=== Best Animated Feature in the history of that Award (2001–present). 

{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result

|-
|| British Academy Childrens Awards (BAFTA)
| Favorite Film
|
|rowspan=12   
|-
|| 38th Peoples Choice Awards|Peoples Choice Awards 
| Favorite Movie Animated Voice
| Owen Wilson
|-
|| 69th Golden Globe Awards
| Best Animated Film
|
|- Annie Awards
| Best Animated Feature
|
|-
| Best Animated Effects in an Animated Production Jon Reisch
|-
| Best Animated Effects in an Animated Production Eric Froemling
|-
| Character Design in an Animated Feature Jay Shuster
|-
| Production Design in a Feature Production Harley Jessup
|-
| Storyboarding in a Feature Production Scott Morse
|-
| Editing in a Feature Production Stephen Schaffer
|- Kids Choice Awards
| Favorite Animated Movie
|
|- Saturn Awards
| Best Animated Film
|- American Society ASCAP Award  Top Box Office Films Michael Giacchino
| 
|}

==Video games==
  PC and 3D gameplay.  A Nintendo 3DS version was released on November 1, 2011,  and a PSP version was released on November 8, 2011. 
 iTunes for a dollar on June 23, 2011. The Lite version was released for free that same day. The object of the game was to complete each race, unlock new levels, and get a high score. As of June 28, 2011, The app has hit No.1 on the App Store.  The game was retired on August 29, 2014. 

==Sequel and spin-offs==
  Route 99.  At the Disney shareholders meeting in March 2014, Disney CEO and chairman Bob Iger confirmed that Pixar is in preproduction on a third Cars film. 

An animated feature film spin-off called  , was later released the following year on July 18, 2014.   

==References==
 

==External links==
 
 
 <!--      

       Please be cautious adding more external links.

Wikipedia is not a collection of links and should not be used for advertising.

     Excessive or inappropriate links will be removed.

 See   and   for details.

If there are already suitable links, propose additions or replacements on
the articles talk page, or submit your link to the relevant category at
the Open Directory Project (dmoz.org) and link there using  .

-->
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 