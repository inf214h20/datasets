One Piece: The Movie
{{Infobox film
| name           = One Piece: The Movie
| film name = {{Film name| kanji= ワンピース
| romaji         = Wan Pīsu}}
| image          = One Piece - The Movie.jpg 
| caption        = Japanese DVD release cover
| director       = Junji Shimizu
| screenplay     = Michiru Shimada
| based on       =  
| starring       = Mayumi Tanaka Kazuya Nakai Akemi Okamura Kappei Yamaguchi Ikue Otani Kenji Utsumi Takeshi Aono Yuka Imai
| music          = Kohei Tanaka (composer)|Kōhei Tanaka
| editing        = Shinichi Fukumitsu  Kōichi Katagiri
| studio         = Toei Animation Fuji Television Shueisha Toei Company
| distributor    = Toei Company
| released       =  
| runtime        = 51 minutes
| country        = Japan
| language       = Japanese
| gross          = ¥2,160,000,000   
}}
  is a 2000  .

== Plot ==
In the East Blue, the pirate El Drago and his crew are searching for the treasure left behind by the legendary "Great Gold Pirate" Woonan, which is said to be a mountain of gold; El Drago eventually finds a treasure map to Woonans gold by killing Wonnans former crew. While traveling to the treasure, Wommans men rob the Straw Hat Pirates, who have run out of food and are close to starving. Luffy attacks them and accidentally smashes their small boat in the process. Woonan retaliates using his Goe-Goe devil fruit powers, sending a powerful shock-wave that completely destroys the small boat and knocks the Going Merry away. Zoro dives into the sea to save Luffy, separating them from the rest of the Straw Hats, and finds Tobio, a small boy who was forced to work for El Drago. Smelling food, Luffy, Zoro and Tobio use the remains of the boat to travel to a floating oden shop run by Tobios grandfather, Ganzo. Not having any cash, Luffy and Zoro accidentally perform an eat and run and are chained together by Ganzo.

El Drago and his crew land on Woonans island, where they encounter Usopp. Usopp convinces El Drago not to kill him by claiming to be a professional treasure hunter, and close friend of Woonan, and begins leading Woonan around aimlessly. Meanwhile Luffy and Zoro, still chained together, chase after Luffys hat onto the island when it blows away in the wind; Tobio follows them and the group becomes lost. Nami finds Woonans crew, and Usopp lies about the location of the gold, telling El Dragos men to dig in a random location for three days. However, El Drago decides to use his devil fruit powers instead, and Luffy, Zoro and Tobio follow the sound. Tobio attempts to attack El Drago, whose retaliation is blocked by Zoro. Zoro and El Dragos fight is cut short by Luffys leg getting caught on a rock and flinging them both across the island; Usopp and Nami use this distraction to escape, and regroup with Luffy and Zoro. Solving a riddle on Woonans map, the Straw Hats and Tobio find the cave in which Woonans treasure is buried, finding Ganzo ahead of them. Ganzo reveals that he and Woonan were childhood friends, and that as children Ganzo and Woonan had fought over disagreeing with each others dreams. In this fight, the two were knocked off a cliff face and onto a breaking branch, and Ganzo let himself fall in order to save Woonan. Unknown to Woonan, Ganzo was saved by a passing boat, but Woonan has already set out to sea when Ganzo woke up.

The group find an old house where the gold is hidden, but El Drago catches up to them and fights Luffy and Zoro. El Drago is defeated, and in a secret room the group find Woonans skeleton and a message left for Ganzo. Before dying, Woonan realized that his treasure could not make him happy, and returned all the gold he stole, leaving nothing but his skeleton and the flag he showed Ganzo before their fight. Ganzo decides to build a grave for Woonan and continue running his oden shop, while Nami manages to steal El Dragos treasure. Ganzo refuses to accept the money for the oden, so that the Straw Hats will still owe him and be obligated to visit him again. As the Straw hats sail away, Tobio comments that Luffy really will become King of the pirates some day.

== Cast ==
* Mayumi Tanaka as Monkey D. Luffy
* Kazuya Nakai as Roronoa Zoro Nami
* Kappei Yamaguchi as Usopp
* Kenji Utsumi as El Drago
* Takeshi Aono as Ganzo
* Yuka Imai as Tobio
* Nachi Nozawa as Woonan
* Mahito Ohba as the Narrator
* Taiki Matsuno as young Ganzo
* Takeshi Kusao as young Woonan
* Shinsuke Kasai as Danny
* Toshihiro Itō as Denny
* Tsurumaru Sakai as Donny

== Release ==
Promotional One Piece and  .   

Kazé released the film in France and Germany. The film was released on DVD and Blu-ray in France on August 24, 2011, featuring Japanese and French audio, as well as subtitles in French and Dutch subtitles; A box set of the first three One Piece films was released on 16 October 2013 on DVD and Blu-ray.    In Germany, the film was released on DVD on 26 February 2011, featuring Japanese and German audio, and German subtitles. The German release also included a poster.   
 Adventure Of Spiral Island and List of One Piece films#Choppers Kingdom on the Island of Strange Animals|Choppers Kingdom On the Strange Animal Island on 28 July 2014, featuring Japanese audio and English subtitles;    This was the first English-language One Piece release not to feature an English dub. Although Manga was praised for being the first English speaking country to release the film, some reviews criticized the release for its poor video and audio quality and inconsistent translation to the subtitles found on the TV series releases. Jitendar Canth of My Reviewer described this release as "placeholder discs", pointing out that the movies will likely by dubbed by FUNimation and re-released, but also that "you’re going to have a hell of a wait  " so "don’t miss out on the fun by waiting".   

== Soundtrack ==
The film uses the same pieces of theme music as the TV series at that time; The opening theme song is We Are! by Hiroshi Kitadani, and the films ending theme song is Memories by Maki Otsuki.

The films original soundtrack was composed by   on March 18, 2000.    This album also included the original soundtrack to the One Piece TV series and image songs sung by the Straw Hat pirates, with tracks 1 to 16 representing the soundtrack to One Piece: The Movie. This album was later re-released as part of the compilation One Piece Eizo Ongaku Kanzenban (ワンピース 映像音楽完全盤 / アニメサントラ) on January 31, 2007, with the racks re-ordered.   

;One Piece Music And Song Collection Track listing
#  
#   (Performed by Hiroshi Kitadani)
#  
#  
#  
#  
#  
#  
#  
#  
#  
#  
#  
#  
#  
#   (Performed by Maki Otsuki)
#  
#   (Performed by Mayumi Tanaka as Luffy)
#  
#   (Performed by Akemi Okamura as Nami)
#  
#  
#  
#  
#  

== Reception ==
The 2000 Spring Toei Anime Fair opened at number two position at the Japanese box office. In its second week, the double feature dropped to third place, before rising to the number one spot for its third and fourth weeks.    In total, the films earned 2,160,000,000 Japanese yen at the box office.   

The films UK release received mixed reviews from critics. Jitendar Canth, of MyReviewer.com, described the film as "  out like an extended episode", which lacked the grand visuals and storylines of later One Piece films and had a predictable story. However, Canth still recommended the film based on its fast pacing and comedic tone, awarding the film a 7/10 rating.  Andy Hanley, of UK Anime Network, suggested that the film may have been more effective as "a TV episode or two rather than a short film" and criticized the films pacing for taking "a little too long to get to its pivotal moments".  Luke Baldock, of The Hollywood News, commented that "the animation doesn’t look as good as the show here", but was satisfied with the films humor and action. 

== References ==
 

 

 
 
 
 
 
 