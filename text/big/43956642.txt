Murder in the Cathedral (film)
{{Infobox film
| name           = Murder in the Cathedral
| image          = Murder in the Cathedral (movie poster).jpg
| caption        = Theatrical release poster
| director       = George Hoellering
| producer       = George Hoellering
| screenplay     = George Hoellering
| based on       = Murder in the Cathedral by T. S. Eliot
| starring       = John Groser
| cinematography = David Kosky
| music          = László Lajtha
| editing        = Anne Allnatt
| studio         = Film Traders Limited
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} same title. Eliot himself participates as the voice of the fourth tempter.

The film competed at the 12th Venice International Film Festival and received the award for best production design, given to Peter Pendrey.  It was released in the United Kingdom in 1952. 

==Cast==
* John Groser as Thomas Becket, Archbishop of Canterbury Henry II
* David Ward as First Tempter George Woodbridge as Second Tempter
* Basil Burton as Third Tempter
* T. S. Eliot as Voice of Fourth Tempter
* Donald Bisset as First Priest
* Clement McCallin as First Knight
* Michael Aldridge as Second Knight
* Leo McKern as Third Knight Paul Rogers as Fourth Knight
* Alban Blakelock as Bishop Foliot
* Niall MacGinnis as Herald

==Reception== King Henry in a scene especially written for the film." 

==References==
 

 
 
 
 
 
 
 
 
 
 

 