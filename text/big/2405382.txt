Looking for Alibrandi (film)
 
 
{{Infobox film
| name           = Looking for Alibrandi
| image          = 
| executive producer      = Tristram Miall
| producer       = Robyn Kershaw
| writer         = Melina Marchetta
| starring       = Pia Miranda Kick Gurry Anthony LaPaglia Greta Scacchi Elena Cotta
| music          = Silverchair Killing Heidi
| cinematography = Toby Oliver Martin Connor
| runtime        = 103 minutes
| country        = Australia
| release date   = May 4, 2000 English Italian Italian
| Best Feature = None
| budget         = Australian dollar|$4.5 million 
| gross          = $8,300,000 director = Kate Woods}}
 novel of Australian Film Best Film in 2000.

==Plot==
Looking for Alibrandi begins light-heartedly, and the viewer gets a very quick understanding of Josies character through her interactions with her friends and family. As the film progresses, the glamour that is initially associated with Josie begins to fade as she struggles to cope with her final year of school (especially the racist attitude of one girl in particular, Carly Bishop (Leeanna Walsman)), the suicide of her crush, John Barton (Matthew Newton), and meeting with Michael Andretti (Anthony LaPaglia), her father, of whom has only just found out about her existence upon returning to Sydney for work. She also has continual conflict with her grandmother, Katia Alibrandi (Elena Cotta).

However, these complications are seemingly resolved quickly, in keeping with Josies brusque and forthright outlook on life. For example, in response to Carlys continuous snide remarks, she breaks her tormentors nose with a history textbook. It is this summary act that brings her father back into her life.

Another complication—the suicide of her close friend and unrequited crush, John Barton—tests her resilience. Struggling with her grief, she finds comfort to a certain extent within Jacob Coote, he was a bad boy on the outside, but he was found out to be a sincere and caring person on the inside.

The most significant complication and challenge for Josie, though, is her rocky relationship with her father, Michael Andretti. When they finally get to know each other, and recognise themselves in each other, their rift heals, and she can confide in him.

==Production== Glebe (Alibrandis Sydney Central George Street/ANZAC Village Cinema Oporto (where Josie works part-time).

==Critical acclaim==
The film, while not well known in international markets, has received critical acclaim for its insights into both the second-generation-migrant experience and the universal human condition.

Looking for Alibrandi was Kate Woods directorial debut in film; Woods was acclaimed for "giving   multicultural terrain the true respect and depth it deserves."     

==Awards==
Looking for Alibrandi won five awards at the 2000 AFI Awards: 

*Best Film- presented to producer Robyn Kershaw
*Best Lead Actress- Pia Miranda
*Best Supporting Actress- Greta Scacchi own novel)
*Best Film Editing- Martin Connor

==Box office==
Looking for Alibrandi grossed $8,300,000 at the box office in Australia. 

==See also==
* Cinema of Australia

==References==
 

==External links==
* 
*  on  
* 

 
 
 

 
 
 
 
 
 
 
 
 
 