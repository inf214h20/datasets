Elina: As If I Wasn't There
{{Infobox Film
| name           = Elina: As If I Wasnt There
| image          = Elina.jpg
| caption        = 
| director       = Klaus Härö
| producer       = Melanie Backer David Leitner Michael Yanko
| writer         = Kerstin Johansson i Backe (novel) Jimmy Karlsson (screenplay)
| narrator       = 
| starring       = Natalie Minnevik Bibi Andersson
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sonet Film
| released       = 2002
| runtime        = 77 minutes
| country        = Sweden Finland Swedish Finnish Finnish
| budget         = 
| gross          = 
}}

Elina: As If I Wasnt There ( ) is a 2002 film directed by Klaus Härö and based on a novel by Kerstin Johansson i Backe.

==Plot==
In the 1950s, nine-year-old Elina lives with her younger siblings and her mother in the Torne Valley in the north of Sweden, near the Finnish border. Her father, whom she loved dearly, died of tuberculosis a few years ago. Elina finds consolation in wandering out on the dangerous marshlands to have imaginary conversations with her dead father. Elina also contracted tuberculosis, but recovered. Because she was ill for so long, Elina is obliged to repeat a whole year at school. However, since she missed so much schooling as a result, she has been put in a new class with a different teacher, the strict Tora Holm. Tora Holm sees it as her mission to protect her pitiable charges from the pitfalls of life, believing that only those who speak perfect Swedish have any chance of a happy and successful life. Elinas family belongs to a Finnish-speaking minority frowned upon by a staunch schoolmistress who starts hounding Elina for speaking Finnish in class and questioning her authority. Elinas mother, sister, and a liberal young male teacher all try to mediate the ensuing battle of wills between Elina and Miss Holm. Especially when Elina exercises the sense of justice she learned from her father and stands up for one of her schoolmates. Although nobody is willing to take her side, little Elina proves far stronger than teacher Tora Holm reckoned. The conflict comes to a head when Elina flees to the dangerous Moorland|moor.

==Cast==
{| class=wikitable
! Actor !! Role
|-
| Natalie Minnevik || Elina
|-
| Bibi Andersson || Tora Holm
|-
| Henrik Rafaelsen || Einar Björk
|-
| Tind Soneby || Irma
|-
| Marjaana Maijala || Marta
|- Peter Rogers || Anton
|-
| Jarl Lindblad || Veikko Niemi
|-
| Zorro Svärdendahl || Isak (Elinas father)
|}

==External links==
*  
*  

 
 
 

 