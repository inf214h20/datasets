Caffeine (film)
{{Infobox film name       = Caffeine image      = Caffeine ver2.jpg caption    = Promotional movie poster for the film director  John Cosgrove producer  David Peters writer     = Dean Craig starring   = Mena Suvari,  Marsha Thomason,  Katherine Heigl,  Mike Vogel &  Breckin Meyer released   = 10 August 2006 (Rhode Island International Film Festival|RIIFF) runtime    = 92 minutes language  English
}}
Caffeine is a 2006 comedy starring Marsha Thomason, Mena Suvari, Breckin Meyer, Katherine Heigl, and Mike Vogel.  It revolves around relationships of the staff and patrons of the quirky Black Cat Cafe in London one day.

==Plot==
The staff and customers of the cafe get an extra "jolt" with their coffee when a commitment-shy man has a public fight with his angry ex-girlfriend, instigating a series of revelations about the eavesdropping couples nearby and their own "unusual" relationships; filled with eccentric personal conversations, confrontational arguments, imaginary visions, and connections between various characters.

==Story==
During one lunchtime at an offbeat London coffee house, the relationships of the quirky staff and several couples are suddenly turned upside down by revelations of supremely embarrassing secrets and idiosyncrasies, generally having to do with their rampaging sexual appetites. A neurotic young commitment-phobe runs into his ex-girlfriend while hes whacked out on killer dope. A high strung control freak finds out that her husband-to-be is a transvestite. A shy, hesitant young woman suffers the blind-date-from hell, an obnoxious bore who has been told she sleeps with men on the first date. A hyper-possessive boyfriend discovers that his girlfriend is an ex-porn actress. The managers boyfriend has a ménage à trois which he says is forgivable because the girls were identical twins. As the craziness builds to hilarious conclusions, CAFFEINE interweaves these characters hapless attempts to repair their fractured relationships while they are forced to confront issues of fidelity, betrayal, commitment and forgiveness. 

==Cast==
{| class="sortable wikitable"
|-
! Actor                          !! Role    !! Notes
|-
|     || Rachel  || Café manager & Charlies girlfriend
|-
|         || Vanessa || Café waitress & Lucys granddaughter
|-
|         || Charlie || Café chef & Rachels cheating boyfriend
|-
|     || Tom     || Café waiter, assistant cook
|-
|            || Lucy    || Vanessas delusional grandmother
|-
|    || Mike    || Dannys stoner friend
|-
|          || Danny   || Mikes stoner friend
|-
|       || Dylan   || Café waiter, aspiring writer
|-
|        || Gloria  || Marks girlfriend (and possible porn star)
|-
|       || Mark    || Glorias jealous boyfriend
|-
|     || Laura   || Steves blind date & Mikes ex-girlfriend
|-
|        || Steve   || Lauras blind date
|-
|      || John    || Davids friend (an accused pervert)
|-
|         || David   || Johns friend & Angelas fiancée
|-
|     || Dude    || a guitarist
|-
|   || &mdash; || annoying lady customer
|-
|        || Angela  || Davids fiancée
|-
|        || Mr. Davies || the owner of the Marion
|}

==References==
 

==External links==
*  
*  

 
 
 


 