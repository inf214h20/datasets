Toys in the Attic (1963 film)
{{Infobox film
| name           = Toys in the Attic
| image          = Toys in the attic.jpg
| caption        = Promotional movie poster
| director       = George Roy Hill
| producer       = Walter Mirisch
| based on       =  
| starring       = Dean Martin Geraldine Page Gene Tierney Wendy Hiller
| music          = George Duning
| cinematography = Joseph F. Biroc
| editing = Stuart Gilmore
| studio         =  Meadway-Claude Productions Company  The Mirisch Corporation 
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget = $2.1 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 171 
}} play Toys of the same name by Lillian Hellman. The original music score was composed by George Duning.

== Plot ==
Julian Berniers returns from Illinois with his young bride Lily Prine to the family in New Orleans. His spinster sisters Carrie and Anna welcome the couple, who arrive with expensive gifts. Julian tells them that while his factory went out of business, he did manage to save money.  While the sisters are skeptical, there is much talk of a long-hoped-for trip to Europe for the two sisters.
 sublimated incestuous desires for her brother, is aimed at Lily.
Carrie gets Lily to inform Charlottes husband of a rendezvous between Charlotte and Julian, at which Julian was to give Charlotte her half of the money, and Charlotte was then going to leave her husband and escape the town.  Charlottes husband sends thugs who beat up Julian and maim Charlotte.   The thugs take all the money, Charlottes and Julians halves.  Julian finds out that Carrie manipulated Lily into making the phone call to Charlottes husband.  Julian
and Anna both leave the house, Julian going to find Lily, and Anna going to Europe.  Carrie is left alone.

== Cast ==
* Dean Martin as Julian Berniers
* Geraldine Page as Carrie Berniers
* Yvette Mimieux as Lily Prine Berniers
* Gene Tierney as Albertine Prine
* Wendy Hiller as Anna Berniers
* Nan Martin as Charlotte Warkins
* Larry Gates as Cyrus Warkins
* Frank Silvera as Henry Simpson
== Reception ==
The film recorded a loss of $1.2 million. 
 Bill Thomas), and was nominated for the Best Actress Golden Globe (Geraldine Page) and the Best Supporting Actress Golden Globe (Wendy Hiller).

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 