The Iceman Ducketh
{{Infobox Hollywood cartoon
| series = Looney Tunes (Bugs Bunny/Daffy Duck)
| image = The Iceman Ducketh title card.png
| caption = 
| director = Phil Monroe Maurice Noble (co-director) Richard Thompson Virgil Ross Harry Love (effects)
| layout_artist = Bob Givens William Butler John Dunn
| voice_actor = Mel Blanc Bill Lava
| producer = David H. DePatie (uncredited)
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures
| release_date = May 16, 1964 (USA)
| color_process = Technicolor
| runtime = 6 minutes
| movie_language = English
}} John Dunn. It was the last Warner Bros. theatrical cartoon featuring Bugs and Daffy together until Box Office Bunny in 1990, and the last that Chuck Jones worked on, though he was fired at an early stage of production and replaced by Monroe (by the time it was released, Jones had already produced two cartoons at his new studio, Sib-Tower 12).

The roaring of the angry bears in the cartoon seems to be the same sound effect used for the monsters roar in the feature film The Beast from 20,000 Fathoms, which was also produced by Warner Bros.
 John Madden and Pat Summerall as the second quarter of the 2001 Cartoon Network special The Big Game XXIX: Bugs Vs. Daffy.

==Summary==
At a trading post up in the Klondike, Yukon|Klondike, many fur trappers have come in to trade in their furs for big bucks after a successful fur trapping season. When Daffy Duck sees Garcon, the manager of the trading post, giving money to the last fur trapper, he checks in to ask if trading in furs for money is true. Garcon answers its true, but Daffy decides to go out and trap some fur for money, just as Garcon warns him that fur trapping season is over due to the approaching winter.

Scoffing at the idea of winter, Daffy vows to catch a fur just as winter sets in just as suddenly.  Bugs Bunny makes a snow rabbit, telling us itll fool Daffy for a while while he makes his getaway.  Daffy comes up to the snow rabbit and warns him not to move or be pulverized. When the coal nose of the snow rabbit falls off, Daffy takes that as movement and proceeds to whack the snow rabbit to pieces until he hits a hibernating bear, whom the snow rabbit was built over. Angry at having his hibernation disturbed, the bear claws Daffy, who runs off thinking the bear missed until he falls to pieces.

Just as Bugs is tutting off Daffys misfortune, Daffy catches up and pokes his rifle at him. When Bugs asks Daffy if hes got some antipathy towards him, Daffy states hes only after the fur, of which he finds Bugs to be the softest hes felt. As Bugs starts bragging about getting his suits from "the same tailor as the Duke of Windsor," Daffy just states hes not interested in any "sales pitch" (Ken Harris animates this scene—his last work at Warners). Daffy refuses to give Bugs a sporting chance and threatens to shoot him in the neck, but Bugs kisses him, plugs up his gun with his carrot and runs away. Daffy tries to shoot Bugs, but the gun expands from the carrot and as Daffy tries to dislodge the carrot; the gun explodes in his face. Daffy then snarls: "Ooooh, I LOVE him!"

Later, Daffy ignores a sign warning him there are hibernating animals, vowing that Bugs can be the only animal thats not going to get any sleep this winter. No sooner does he say that than he comes across an alarm clock, that rings and wakes up another hibernating bear. Thinking Daffy to be responsible for the rude awakening, the bear chases him straight into a cave where Bugs directs them.  Bugs then seals up the cave entrance with a boulder, leaving the bear to pummel Daffy, who finally exits the cave looking a little worse for wear.

Further on, Bugs has come across a sign warning him that hes in avalanche territory just as Daffy comes running up. Bugs manages to inform him of where they are and instructs him to follow on tiptoe. Daffy obliges and, after a few seconds, asks Bugs if its safe to talk. Bugs pretends he didnt hear, prompting Daffy to ask again in his loudest whisper. When Bugs again pretends not to hear, Daffy gets mad enough to simply shout his question, causing snow to fall all over him.  Bugs informs him that it wasnt safe then and Daffy thanks him by shouting again and getting more snow in the face.

Back at his hole, Bugs goes down for a rest just as Daffy comes up with dynamite. As Daffy trails out the wire, Bugs moves the hole so he secretly places the dynamite next to Daffy, just as Daffy prepares a detonator. After the dynamite explodes on Daffy, Daffy states how hes going to cry.
 Colgate commercials of the early 1960s.

Up a hill, Daffy drops a pebble that rolls into a giant snowball, in the hopes it will engulf Bugs.  The snowball misses Bugs by a few inches, but engulfs three hibernating bears in a cave and falls out the other end of the cave. After a brief boulder catapult gag, similar to those from the Wile E. Coyote and Road Runner cartoons, the snowball lands on Daffy, who runs up a tree just as the angry bears threaten to massacre him.

Later that night, Bugs decides to turn in for winter hibernation and bids Daffy and the bears goodnight. Daffy, now looking blue from the cold and worse for wear from an unseen fight with the bears (that mightve taken place during the blackout between this scene and the snowball gag), simply curses over the mess hes gotten himself into while still clinging to the treetop.

==Availability==
"The Iceman Ducketh" is available, uncensored and uncut, on the Looney Tunes Superstars DVD. However, it was cropped to widescreen.

==Edited versions== Fox version of The Merrie Melodies Show, the syndicated version of The Merrie Melodies Show, and the version shown on the now defunct The WB Television Network|WB, two parts were cut:
** Daffy pulling Bugs carrot from the gun barrel after Bugs plugs it, along with Daffy getting shot and saying sarcastically: "Oooh, I LOVE him!".
** The part after Daffy gets turned into an ice statue where Daffy climbs out of the statue, tugs on the gun thats still in there, and gets shot.
* While Fox ended the scene with Daffy stuck in his own ice sculpture after Bugs line: "See you after the spring thaw, pal!" (while Daffy was still stuck in his own ice sculpture), the syndicated "Merrie Melodies" show and The The WB Television Network|WB! ended the scene after Daffy gets out of his own ice sculpture and says: "Well see who gets the last laugh around here!" right before he tugs on the gun.

==See also==
* List of cartoons featuring Daffy Duck
* List of Bugs Bunny cartoons

==External links==
*  

 
{{succession box |
before= Dr. Devil and Mr. Hare | Bugs Bunny Cartoons |
years= 1964 |
after= False Hare|}}
 

 
 
 
 
 
 
 
 