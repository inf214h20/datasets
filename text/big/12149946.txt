See Grace Fly
{{Infobox film name = See Grace Fly
| image = Seegraceflyposterb.jpg
| caption = See Grace Fly film poster
| starring = Gina Chiarelli   Paul McGillion   Tom Scholte   Benjamin Ratner   Jennifer Copping   Megan Leitch
| writer = Pete McCormack
| cinematography  = Larry Lynn
| director = Pete McCormack
| producers = Pete McCormanch, Paul McGillion, Gina Chiarelli
| executive producers = Robert French   Paul Armstrong
| co-Producer = Jena Niquidet
| film editing = Jesse James Miller
| music = Dennis Burke
| released =  
| runtime = 91 minutes
| budget = $75,000 CAD
}}
See Grace Fly is an independent film directed and written by Pete McCormack and starring Gina Chiarelli and Paul McGillion. Its dramatic and often heartwrenching plot revolve around siblings, Grace and Dominic McKinley as they struggle to cope with their mothers death and Graces mental illness.

==Plot==
Grace McKinley is a brilliant 38 year-old woman with schizophrenia. When her mother dies, Graces actions become increasingly erratic. She takes two weeks to report the death, and in that time is sent a shattering message that only she can decipher.

Graces younger brother Dominic is a repressed yet courageous missionary working in war-ravaged Sierra Leone. Called home to arrange his mothers funeral, he is at the same time forced to deal with Graces uncertain future and their forgotten past. The problem is, wanted by the police for questioning, and with her life threatened, Grace has disappeared onto the streets of Vancouver, fuelled by an indomitable will to spread her secret to the masses.

Kate Wilkens—Dominics past love interest and Graces psychiatrist—and Dominics childhood buddy James, a priest, combine with Graces fiercely loyal best friend Gigi and her punchy husband Ralph to round out those searching for Grace. But in the days leading up to the funeral the question of who actually needs to be saved becomes less and less clear, and the truth of Graces convictions threaten to shatter all Dominics beliefs, and both of their lives, to where death may be the only way to freedom…

Driven by the courage of love and the agony of mental illness, See Grace Fly slams heart first into faith, death, sex and family, offering a gripping look at the precarious balance between belief and reality.

==Cast==
* Gina Chiarelli - Grace McKinley 
* Paul McGillion - Dominic McKinley 
* Jennifer Copping - Kate Wilkins 
* Tom Scholte - Father James
* Benjamin Ratner - Ralph Devito
* Megan Leitch - Gigi Devito

==Rest of Cast in Alphabetical Order==
* Linden Banks - Principal 
* Brendan Beiser - Funeral Director 
* Frank Borg - Bubba Binski 
* Garvin Cross - Walt 
* Rick Dobran - Sammy 
* Mike Dopud - Steve 
* Marcy Goldberg - Young Connie 
* Matt Hill - School Security Guard 
* Matt Hill - Leo 
* Michael Kopsa - Mr. McKinley 
* Edith Lando - Connie 
* David Lewis - Gerry 
* David Lovgren - Terry 
* Michael P. Northey - CBS Security Guard 
* Wesley Salter - Chicken Boy 
* Jade Shaw - Cheryl 
* Christopher Shyer - Bloody-nosed Travestite 
* Jackson-Day Washington - Young Dominic
* Tatiana Stellingwerff - Young Grace

==Press==
* "On Sunday, Gina Chiarelli received two much-deserved standing ovations for her grippingly honest, tour-de-force performance as a schizophrenic who prophesizes the end of the world in Pete McCormacks See Grace Fly . This quasi-mystical story, which follows a brilliantly troubled woman and the voices that torment her through the streets of Vancouver, marks the directorial debut for author of the Stephen Leacock Medal for Humour-nominated comic novel Understanding Ken.” 
- Alexandra Gill, the Globe and Mail

* "First-time directors from Vancouver were the talk of the festival circuit this year…audiences leapt to their feet for Pete McCormack and his film about a schizophrenic woman, See Grace Fly." 
- Amy Carmichael, Ottawa Citizen

* “Gina Chiarelli is marvelous…” “Paul McGillion is excellent…”

* “…one of the most brilliant films to tackle religious themes since Jesus of Montreal…” 
- La Presse

* “…heart-wrenching…” - The Globe and Mail

* “…emotionally shattering…”

* “…roles as juicy as this are an actors recurring dream come true…”

* “…McCormacks writing is a reminder that words really do help in the lost art of communication…”

* “…Chiarellis possessed portrayal of a brilliant 38-year-old woman out to warn the world that the end is nigh is the reason awards were invented…” 
- John Griffin, The Montreal Gazette

* "...Grace McKinley is a distressingly believable character...she has the power to draw you into a world you might not come out of..." 
-  Maurie Alioff, Cine Festival

* "The best Canadian feature...was screenwriter (The Blue Butterfly, Whirlygirl) and novelist Pete McCormack’s See Grace Fly, a measured, humane portrayal of illness that most films don’t make time for. Already a double winner at last year’s Vancouver festival..., the film sketches a few days in the life of Grace, a 38-year-old schizophrenic convinced the apocalypse is about to come to pass.

* Measured and aggressive in its study of faith and illness, McCormack’s directorial debut dispenses with serial beauty shots (think Russell Crowe’s animated, frenzied manias in A Beautiful Mind) to foreground two brilliant performances from Gina Chiarelli as Grace and Paul McGillion as her brother Dominic, a lay missionary who has returned home to sort out the messy details of his mother’s death under Grace’s watch." 
-  Filmmaker Magazine

==Awards (Won)==
* Vancouver Film Festival, 2003 (Canada): Best Feature Film - Special Citation 
* Vancouver Film Festival, 2003 (Canada): Women in Film Award, Juried Prize - Gina Chiarelli
* Fantasporto 2004 (Portugal): Best Film, Audience Jury Award
* Leo Award 2004: Best Performance by a Lead Female, Gina Chiarelli (Canada)

==Awards (Nominations)==
* Leo Award 2004: Best Direction, Pete McCormack
* Leo Award 2004: Best Feature Length Drama
* Leo Award 2004: Best Lead Performance by a Male, Paul McGillion
* Leo Award 2004: Best Screenwriting, Pete McCormack
* Genie Award 2006: Best Performance by an Actress in a Leading Role, Gina Chiarelli

==External links==
*  
* 
*  
*  

 
 
 