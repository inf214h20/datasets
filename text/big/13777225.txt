Ken (film)
{{Infobox Film
| name           = Ken Huang
| image          = 
| caption        = 
| director       = Kenji Misumi 
| producer       = Hiroaki Fujii
| writer         = Kazuro Funabashi Yukio Mishima (novel)
| starring       = Raizo Ichikawa Yusuke Kawazu Hisaya Morishige
| music          = Sei Ikeno 
| cinematography = Chishi Makiura 
| editing        = 
| distributor    = Daiei Film
| released       = March 14, 1965
| runtime        = 94 minutes
| country        = Japan Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1964 Cinema Japanese film directed by Kenji Misumi. From a screenplay by Kazuro Funabashi, based upon the short story Ken (Sword) by  Yukio Mishima.

==Synopsis==
The story is centered on Kokubu Jiro (Raizo Ichikawa), a prominent member of his universitys Kendo dojo.

==Starring==
*Raizo Ichikawa - Jiro Kokubun
*Hisaya Morishige - Kagawa
*Akio Hasegawa - Mibu
*Noriko Sengoku - Mibu
*Chikako Miyagi - Kiuchi
*Keiju Kobayashi - Eri Itami
*Yuka Konno - Sheko Fujishiro
*Junko Kozakura - Sanae Mibu
*Yoshio Inaba - Seiichiro Kokubun
*Rieko Sumi - Horoko Kokubun

==External links==
*  
*   of the film at  

 

 
 
 
 
 
 
 
 
 

 
 