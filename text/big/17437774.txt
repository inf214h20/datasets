Mistress (1992 film)
 
{{Infobox Film
| name           = Mistress
| image          = Mistress 1992 poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Barry Primus
| producer       = Meir Teper Robert De Niro
| writer         = Barry Primus J.F. Lawton
| narrator       = 
| starring       = Robert Wuhl Martin Landau Jace Alexander Danny Aiello Robert De Niro Laurie Metcalf Christopher Walken Eli Wallach
| music          = Galt McDermont
| cinematography = Sven Kirsten
| editing        = Steven Weisberg
| distributor    = Rainbow Releasing
| released       = 24 July 1992 
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,102,469
| preceded_by    = 
| followed_by    = 
}}

Mistress is a 1992 comedy-drama film starring Robert De Niro, Danny Aiello,  Eli Wallach, Robert Wuhl and Martin Landau. It was written by Barry Primus and J.F. Lawton and directed by Primus.

== Plot ==
A down-and-out Hollywood screenwriter and director named Marvin Landisman (Robert Wuhl) is working on cheaply made instructional videos when his years-old script is read by Jack Roth (Martin Landau), a desperate has-been producer who offers to help Marvin find investors for his movie.

Men willing to put up the money are found, including the ruthless businessman Evan (Robert De Niro), the disturbed war veteran Carmine (Danny Aiello) and the eccentric millionaire George (Eli Wallach). But each has a mistress he insists be cast in the film in exchange for his financial backing. The women are the highly talented Beverly (Sheryl Lee Ralph), the alcoholic flight attendant Patricia (Jean Smart) and the perky blonde Peggy (Tuesday Knight).

Marvin repeatedly is asked to compromise his standards and change his script to accommodate these backers until the script becomes almost unrecognizable from its original form. The project also puts a strain on the marriage of Marvin and his long-patient wife Rachel (Laurie Metcalf).

Marvins screenplay is a bleak one about a painter who commits suicide, and was inspired by the case of an actor named Warren (Christopher Walken) who abruptly committed suicide by jumping off a building in the midst of the making of a film Marvin was directing years ago. Roth brings in young Stuart Stratland (Jace Alexander) to adapt the script for the investors mistresses, but not only does Stuart constantly enrage Marvin with his suggested changes, he falls in love with Peggy and they have an affair.

When Marvins wife demands he grow up and move with her to New York, where she is opening a restaurant, he breaks up with her instead, giving his loyalty to a film that, as she puts it, nobody wants to see. On the verge of signing contracts, everything falls apart, when Beverly discovers that the role she expected to play has been drastically reduced in Peggys favor.

Marvin is left alone, a broken man, done with Hollywood for good. Or at least until the next time Jack Roth gets in touch.

== Cast ==
{| class="wikitable"
! Actor/Actress
! Character
|-
| Robert De Niro || Even Wright
|-
| Danny Aiello ||
|-
| Robert Wuhl || Marvin Landisman
|-
| Martin Landau || Jack Roth
|-
| Eli Wallach ||
|-
| Sheryl Lee Ralph ||
|-Beverly
| Jean Smart || Patricia
|-
| Tuesday Knight || Peggy
|-
| Jace Alexander || Stuart Stratland
|}

Source: 

==References==
 

==External links==
* 

 
 
 
 