The Saturday Night Kid
{{Infobox film
| name           = The Saturday Night Kid
| image          = Thesaturdaynightkid.jpg
| image_size     =
| caption        =
| director       = A. Edward Sutherland
| producer       =
| writer         = George Abbott (play) John V. A. Weaver (play) Ethel Doherty (story) Lloyd Corrigan Edward E. Paramore, Jr. Joseph L. Mankiewicz (titles)
| narrator       = James Hall
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 James Hall, and in her first credited role, Jean Harlow. The film was based on the play Love Em and Leave Em (1926) by George Abbott and John V. A. Weaver. This movie still survives.  

==Plot== James Hall) and fight over Janies selfish and reckless behavior, such as stealing Maymes clothes and hitchhiking to work with strangers. Bill prefers Mayme over Janie and constantly shows his affection for her. This upsets Janie, who schemes to break up the couple.

One day at work, Bill is promoted to floorwalker, while Janie is made treasurer of the benefit pageant. Mayme, however, is not granted a promotion, but gets heavily criticized for constantly being late at work by the head of personnel, Miss Streeter (Edna May Oliver).

==Cast==
*Clara Bow as Mayme
*Jean Arthur as Janie James Hall as William Bill Taylor
* Edna May Oliver as Miss Streeter
*Hyman Meyer as Ginsberg
* Charles Sellon as Lem Woodruff
* Ethel Wales as Lily Woodruff
* Jean Harlow as Hazel
* Leone Lane as Pearl

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 

 