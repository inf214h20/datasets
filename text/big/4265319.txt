The Handmaid's Tale (film)
{{Infobox film
| name = The Handmaids Tale
| image = Handmaids tale.jpg
| caption = Theatrical release poster
| director = Volker Schlöndorff
| producer = Daniel Wilson
| writer = Novel:  
| starring = Natasha Richardson Faye Dunaway Robert Duvall Aidan Quinn Elizabeth McGovern
| music = Ryuichi Sakamoto
| cinematography = Igor Luther David Ray
| distributor = Cinecom Entertainment Group Metro-Goldwyn-Mayer (Current)
| released =  
| runtime = 109 minutes
| country = United States
| language = English
| budget = $13,000,000
| gross  =  $4,960,385 
}} novel of the same name, directed by Volker Schlöndorff the film stars Natasha Richardson (Kate/Offred), Faye Dunaway (Serena Joy), Robert Duvall (The Commander, Fred), Aidan Quinn (Nick), and Elizabeth McGovern (Moira).    The screenplay was written by Harold Pinter.  The original music score was composed by Ryuichi Sakamoto.  MGM Home Entertainment released an Avant-Garde Cinema DVD of the film in 2001. The film was entered into the 40th Berlin International Film Festival.   

==Plot summary==
In the near future war rages across the fictional Republic of Gilead and pollution has rendered 99% of the population sterile. Kate is a woman attempting to emigrate to Canada with her husband and daughter. While attempting to take a dirt road, they are caught by the Gilead Border Guard, who orders them to turn back or they will open fire. Kates husband uses an automatic rifle to draw the fire, telling Kate to run, but he gets shot, Kate gets captured, whilst their daughter wanders off into the backcountry confused and unaccompanied. The authorities take Kate to a training facility with several other women, where they are all trained to become a Handmaid, a concubine for one of the privileged but barren couples who rule the countrys religious fundamentalist regime. Although she resists being indoctrinated into the cult of the Handmaids, mixing Old Testament orthodoxy and misogyny with 12-step gospel and ritualized violence, Kate is soon assigned to the home of the Commander and his cold, inflexible wife, Serena Joy. There she is renamed "Offred" - "of Fred".

Her role as concubine is emotion-free, as she lies between Serena Joys legs while being penetrated by the Commander, in hopes that she will bear them a child. Kate continually longs for her earlier life, and is haunted by nightmares of her husbands death and daughters disappearance. She soon learns that many of the nations male leaders are as sterile as their wives. Serena Joy desperately wants a baby, so she convinces Kate to risk the punishment for fornication — death by hanging — in order to be fertilized by another man who will make her pregnant, and subsequently, spare her life. In exchange for agreeing to this, Serena Joy provides information to Kate that her daughter is alive, and shows a recent photograph of her as proof, living with another Commander. However, Kate is told she can never see her daughter. The Commander also tries to get closer to Kate, in the sense he feels if she enjoyed herself more she would be a better handmaid. The Commander gets Kate hard to obtain items, as well as allow her access to his private library, having realized her background as a librarian. However, during a night out, the Commander has sex with Kate in a manner that is probably not intended for pregnancy. The other man selected by Serena Joy turns out to be Nick, the Commanders sympathetic chauffer. Kate grows attached to him and eventually becomes pregnant with his child. 

Kate ultimately kills the Commander, then is taken away by a police unit.  She thinks that the men are the Eyes, the governments secret police. However, it turns out that they are soldiers from the resistance movement, which Nick, too, is a part of. Kate then flees with them, leaving Nick behind in an emotional scene.

In the closing scene, Kate is once again free and wearing her own clothes, but facing an uncertain future. She is living by herself, pregnant in a trailer whilst receiving intelligence reports from the rebels. She wonders if she will be reunited with Nick, but expresses hope that will happen, and resolves with the rebels help she will find her daughter.

==Cast==
*Natasha Richardson as Kate / Offred
*Robert Duvall as Commander
*Faye Dunaway as  Serena Joy
*Elizabeth McGovern as Moira
*Aidan Quinn as Nick
*Victoria Tennant as Aunt Lydia
*Blanche Baker as Ofglen
*Traci Lind as Janine / Ofwarren
*Reiner Schöne as Luke
*Robert D. Raiford as Dick
*Muse Watson as Guardian Bill Owen as TV Announcer #2
*David Dukes as Doctor

==Pinters unpublished filmscript==
According to Steven H. Gale, in his book Sharp Cut, "the final cut of The Handmaids Tale is less a result of Pinters script than any of his other films.  He contributed only part of the screenplay: reportedly he abandoned writing the screenplay from exhaustion. … Although he tried to have his name removed from the credits because he was so displeased with the movie (in 1994 he told me that this was due to the great divergences from his script that occur in the movie), … his name remains as screenwriter".   
 carte blanche Michael Billington that
 It became … a hotchpotch.  The whole thing fell between several shoots.  I worked with Karel Reisz on it for about a year. There are big public scenes in the story and Karel wanted to do them with thousands of people.  The film company wouldnt sanction that so he withdrew.  At which point Volker Schlondorff came into it as director.  He wanted to work with me on the script, but I said I was absolutely exhausted.  I more or less said, Do what you like. Theres the script.  Why not go back to the original author if you want to fiddle about? He did go to the original author.  And then the actors came into it.  I left my name on the film because there was enough there to warrant it—just about.  But its not mine (  304).  
 Remains of James Ivory–Ismail Merchant partnership, he refused to allow his name to be listed in the credits" (Gale, Films 125). 
 The Remains Pinter Archive at the British Library"; in this essay, which he first presented as a paper at the 10th Europe Theatre Prize symposium, Pinter: Passion, Poetry, Politics, held in Turin, Italy, in March 2006, Hudgins "examin  all three unpublished filmscripts in conjunction with one another" and "provides several interesting insights about Pinters adaptation process". 

==Richardsons perspective on the script==
In a retrospective account written after Natasha Richardsons death, for CanWest News Service, Jamie Portman cites Richardsons view of the difficulties involved with making Atwoods novel into a film script:
 Richardson recognized early on the difficulties in making a film out of a book which was "so much a one-woman interior monologue" and with the challenge of playing a woman unable to convey her feelings to the world about her, but who must make them evident to the audience watching the movie. … She thought the passages of voice-over narration in the original screenplay would solve the problem, but then Pinter changed his mind and Richardson felt she had been cast adrift. … "Harold Pinter has something specific against voice-overs," she said angrily 19 years ago. "Speaking as a member of an audience, Ive seen voice-over and narration work very well in films a number of times, and I think it would have been helpful had it been there for The Handmaids Tale. After all its HER story." 
 The Comfort The Go-Between, The Last The Remains The Trial (198–99, 234, 327, 353-54, 341, 367), as well as the voice overs that he did write for his script of The Handmaids Tale:  The novel does not include the murder of the Commander, and Kates fate is left completely unresolved—the van waits in the driveway, "and so I step up, into the darkness within; or else the light" (  295).  The escape to Canada and the reappearance of the child and Nick are Pinters inventions for the movie version.  As shot, there is a voice-over in which Kate explains (accompanied by light symphonic music that contrasts with that of the opening scene) that she is now safe in the mountains held by the rebels.  Bolstered by occasional messages from Nick, she awaits the birth of her baby while she dreams about Jill, whom she feels she is going to find eventually.  (Gale, Sharp Cut 318)  

==Filming locations==
The scene where the hanging occurred was filmed in front of Duke Chapel on the campus of Duke University in Durham, North Carolina.  , Academic Council Archive, Duke University, 18 Apr. 1996, Web, 9 May 2009. 

==References==
 

==Works cited==
 
:Michael Billington (critic)|Billington, Michael.  Harold Pinter.  London: Faber and Faber, 2007.  ISBN 978-0-571-23476-9 (13).  Updated 2nd ed. of The Life and Work of Harold Pinter.  1996.  London: Faber and Faber, 1997. ISBN 0-571-17103-6 (10). Print.

:Gale, Steven H.  Sharp Cut: Harold Pinters Screenplays and the Artistic Process.  Lexington, KY: The UP of Kentucky, 2003. ISBN 0-8131-2244-9 (10).  ISBN 978-0-8131-2244-1 (13).  Print.

:–––, ed. The Films of Harold Pinter.  Albany: SUNY P, 2001.  ISBN 0-7914-4932-7.  ISBN 978-0-7914-4932-5.  Print.   

:Hudgins, Christopher C.  "Three Unpublished Harold Pinter Filmscripts: The Handmaids Tale, The Remains of the Day, Lolita."  The Pinter Review: Nobel Prize / Europe Theatre Prize Volume: 2005–2008.  Ed. Francis Gillen with Steven H. Gale.  Tampa: U of Tampa P, 2008.  132–39.  ISBN 978-1-879852-19-8  (hardcover).  ISBN 978-1-879852-20-4 (softcover).  ISSN 08959706.  Print. 

:Johnson, Brian D. "Uphill Battle: Handmaids Hard Times." Macleans 26 Feb. 1990.  Print.

:Portman, Jamie (CanWest News Service).   . Canada.com.  CanWest News Service, 18 Mar. 2009.  World Wide Web|Web. 24 Mar. 2009.
 

==External links==
* 
* 
* 
*  (novel) – "Context" at Spark Notes.
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 