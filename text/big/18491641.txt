The Fighting Gringo (1939 film)
{{Infobox film
| name           = The Fighting Gringo
| image          =
| caption        = David Howard
| producer       = Bert Gilroy Oliver Drake
| narrator       = George OBrien Lupita Tovar
| music          =
| cinematography = Harry J. Wild
| editing        = Frederic Knudtson
| distributor    = RKO Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Western film David Howard George OBrien, Ben Johnson had a small uncredited early role as a Mexican barfly and did some work as a stuntman.

==Cast== George OBrien ...  Wade Barton 
*Lupita Tovar ...  Anita Nita del Campo 
*Lucio Villegas ...  Don Aliso del Campo 
*William Royle ...  Ben Wallace 
*Glenn Strange ...  Rance Potter 
*Slim Whitaker ...  Monty Bates (as Slim Whittaker) 
*LeRoy Mason ...  John Courtney 
*Mary Field ...  Sandra Courtney 
*Martin Garralaga ...  Pedro, ranch foreman 
*Dick Botiller ...  Jose, del Campo Vaquero  Bill Cody...  Sheriff Fred Warren (as Bill Cody Sr.) 
*Cactus Mack...  Utah Jones 
*Chris-Pin Martin ...  Felipe, barber  Ben Johnson...  Mexican barfly (uncredited) 
*Sid Jordan ...  Buck, stage driver (uncredited) 
*Forrest Taylor ...  Foreman of coroners jury (uncredited)

==See also==
*The Fighting Gringo (1917 film)

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 


 