Hurricane in the Tropics
{{Infobox film
| name =   Hurricane in the Tropics
| image =
| image_size =
| caption =
| director = Pier Luigi Faraldo   Gino Talamo
| producer = G.G. Ponzano
| writer =  Anton Giulio Majano     (novel)   Antonio Leonviola   Domenico Meccoli  
| narrator =
| starring = Fosco Giachetti   Rubi DAlma   Osvaldo Valenti   Mino Doro
| music = Ulisse Siciliani 
| cinematography = Aldo Tonti
| editing = Gino Talamo    
| studio = Ponzano Film 
| distributor = Ponzano Film 
| released = 4 November 1939 
| runtime = 74 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Hurricane in the Tropics (Italian:Uragano ai tropici) is a 1939 Italian adventure film directed by Pier Luigi Faraldo and Gino Talamo and starring Fosco Giachetti, Rubi DAlma and Osvaldo Valenti.  The film is based on a novel by Anton Giulio Majano. The film was shot at the Fert Studios in Turin, with sets designed by Ottavio Scotti.

==Cast==
*    Fosco Giachetti as Il capitano Moraes  
* Rubi DAlma as Manoela Moraes  
* Osvaldo Valenti as Il tenente Reguero  
* Mino Doro as Nichols  
* Vinicio Sofia as Il radiotelegrafista 
* Danilo Calamai 
* Aristide Garbini 
* Antimo Reyner  

== References ==
 
 
==Bibliography==
* Poppi, Roberto Dizionario Del Cinema Italiano. I film: Tutti i film italiani dal 1930 al 1944. 

== External links ==
* 

 
 
 
 
 
 

 