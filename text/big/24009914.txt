The Black Raven
{{Infobox film
| name           = The Black Raven
| image          =
| image_size     =
| caption        = Robert Livingston in the film
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| writer         = Fred Myton
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline
| editing        = Holbrook N. Todd
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Black Raven is a 1943 American film directed by Sam Newfield, produced and released by Producers Distributing Corporation.

==Cast==
*George Zucco as Amos Bradford aka The Raven
*Wanda McKay as Lee Winfield Robert Livingston as Allen Bentley
*Noel Madison as Mike Bardoni
*Byron Foulger as Horace Weatherby
*Charles B. Middleton as Sheriff
*Robert Middlemass as Tim Winfield
*Glenn Strange as Andy
*I. Stanford Jolley as Whitey Cole
*Jimmy Aubrey appears uncredited as Roadblock Watchman.

==External links==
* 
* 

 
 
 
 
 
 
 


 