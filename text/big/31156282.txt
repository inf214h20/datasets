Mayiladumkunnu
{{Infobox film 
| name           = Mayiladumkunnu
| image          =
| caption        =
| director       = S Babu
| producer       = Chithrakala Kendram
| writer         = Muttathu Varkey KT Muhammad (dialogues)
| screenplay     = KT Muhammad
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Muthukulam Raghavan Pillai
| music          = G. Devarajan
| cinematography = Asthan
| editing        = TR Sreenivasalu
| studio         = Chithrakalakendram
| distributor    = Chithrakalakendram
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by S Babu and produced by Chithrakala Kendram. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Muthukulam Raghavan Pillai in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*Sankaradi
*Sreelatha Namboothiri
*T. R. Omana
*Adoor Bhavani
*K. P. Ummer Khadeeja
*Paravoor Bharathan Sujatha
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eeso Mariyam || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Manichikkaatte || P Susheela, P. Madhuri || Vayalar Ramavarma || 
|-
| 3 || Paappi Appacha || CO Anto, Latha Raju || Vayalar Ramavarma || 
|-
| 4 || Sandhyamayangum Neram || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Thaalikkuruthola || P. Leela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 