3D Sex and Zen: Extreme Ecstasy
 
 
{{Infobox film
| name = 3D Sex and Zen: Extreme Ecstasy
| image = Extreme Ecstasy.jpg
| alt = 
| caption = Chinese release poster
| film name = {{Film name| traditional = 3D肉蒲團之極樂寶鑑
| simplified = 3D肉蒲团之极乐宝鉴
| pinyin = Sān D Ròupútuán zhī Jílè Bǎojiàn
| jyutping = Saam1 D Yuk6 Pou4tyun4 Zi1 Gik6lok6 Bou2 Gaam3}}
| director = Christopher Suen
| producer = Stephen Shiu Stephen Shiu, Jr. Ng Kin-hung
| screenplay = Stephen Shiu Stephen Shiu, Jr. Mark Wu Li Yu
| starring = Hayama Hiro Lan Yan| Crazybarby Leni Lan Yan Saori Hara Vonnie Lui Yukiko Suo Tony Ho
| music = Raymond Wong
| cinematography = Jimmy Wong
| editing = Azrael Chung Matthew Hui
| studio = One Dollar Production, Ltd. Local Production
| distributor = One Dollar Distribution
| released =  
| runtime = 110 minutes 118 min (extended cut) 129 min (directors cut)
| country = Hong Kong
| language = Cantonese Mandarin
| budget = US$3.5 million   
| gross = US$6,369,363 
}}
3D Sex and Zen: Extreme Ecstasy（3D肉蒲团之极乐宝鉴） is a 2011 Hong Kong 3-D erotic costume drama film released in Hong Kong, South Korea, Australia, and New Zealand on 14 April 2011.  It is a new instalment of the Sex and Zen series, but more dark and dramatic as well as occasionally paying homage to the humour of the original series.

==Plot==
A conceited Ming Dynasty scholar called Wei Yangsheng believes that since life is short, one should pursue the ultimate sexual pleasure as time permits. By chance, he meets Yuxiang, the daughter of the Taoist priest Tie Fei, falls in love with her on first sight, and marries her. Yuxiang is elegant and courteous under the influence of her fathers faith, though her lack of passion cannot fully satisfy Wei Yangshengs sexual needs. His disappointment is no less than his affection for her. He ventures in search of ways to increase his sex drive and performance in bed.

Meanwhile after divorcing Yuxiang, Yangshen finally understands his true love is Yuxiang and tries to get her back.

==Cast== Hayama Hiro as Wei Yangsheng
* Lan Yan| Crazybarby Leni Lan Yan as Tie Yuxiang
* Saori Hara as Ruizhu
* Vonnie Lui as the Elder of Bliss
* Yukiko Suo as Dongmei
* Irene Chen as Pandan
* Tony Ho as Prince Ning
* Kirt Kishita as Quan Laoshi
* Wong Shu-tong as Monk Budai Tenky Tin as Dique
* Justin Cheung as Mr. Lam
* Carina Chen as Xianlan, Tie Yuxiangs maid
* Jason Yiu as Shangguan Shen
* Lau Shek-yin as the Mayor
* Mark Wu as Tiancan
* Naami Hasegawa (Tomoko Kinoshita)
* Vienna Lin
* Flora Cheung
* Cliff Chen
* Jeffrey Chow
* Wah Chiu-ho

==Background==
3D Sex and Zen：Extreme Ecstasy （3D肉蒲团之极乐宝鉴）is essentially an adaptation of the erotic novel The Carnal Prayer Mat depicting the sexual exploits of a young Ming Dynasty scholar named Wei Yangsheng. It is produced by Stephen Shiu, the executive producer of its predecessor film Sex and Zen. The film was falsely promoted as "Hong Kongs first IMAX 3-D erotic film". The film was actually rejected by IMAX due to the subject matter and content.  Its cast includes Japanese AV idols.   Laughing aloud, producer Stephen Shiu described the experience of watching this film onscreen: "It is just like   voyeur near someones bed." 

==Distribution== Category III Hong Kong rating and Stephen Shiu told that screening was likely to be blocked in mainland China, a key market for Hong Kong filmmakers.  Nevertheless, producers further announced that 3D Sex and Zen will be released in various versions to bypass censorship laws in some jurisdictions and allow wider distribution. 

In Australia and New Zealand, the film was screened by Hoyts.  

==Marketing==
During the Chinese New Year in 2011, T-shirts and 3-D mouse pads with portraits of the cast were sold at the Lunar New Year Fair in Victoria Park, Hong Kong.  

==Reception==
3D Sex and Zen: Extreme Ecstasy received mostly negative reviews from critics. Review aggregator Rotten Tomatoes reports that 22% of critics gave the film positive reviews, based on 23 reviews. 
 SBS Australia suggested the film loses steam along the way. He believes the thrill of the movie dissipates as the narrative turns nasty at the final 40 minutes, caused by those multi-dimensional rape and dismemberment scenes.  Elizabeth Kerr of The Hollywood Reporter wrote, "Given Extreme Ecstasy s ultimate message that "All you need is love" and the vindication of the value of emotional connection in intercourse, the road the filmmakers take to get there is perplexing to say the least."  The Daily Telegraph named it one of the ten worst films of the year, citing "the film goes on for too long, and gets darker as it does so, veering awfully close to torture porn on occasion, before ending with some unexpectedly sentimental philosophy that will be anathema to the manufacturers of Viagra." 

However, Zoe Li of   blog that the movie heralded a new age in cinema. 

==Box office==
According to Russell Edwards of Variety, 3D Sex and Zen: Extreme Ecstasy took in US $351,000 (HK $2,790,000) on the first day alone in Hong Kong,  beating Avatar (2009 film)|Avatars HK$2.5&nbsp;million opening gross in the country in 2009.  It earned HK$13,104,982 in the first four days after opening. 

As of 15 June 2011, 3D Sex and Zen: Extreme Ecstasy earned more than HK $40&nbsp;million (over US $5M) in Hong Kong.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at Hong Kong Movie Database
*   at Hong Kong Cinemagic
*  

 

 
 
 
 
 
 
 
 
 
 