Never Trust a Woman
{{Infobox film
| name           = Never Trust a Woman
| image          = 
| image_size     = 
| caption        = 
| director       = Max Reichmann 
| producer       =
| writer         = Curt J. Braun   Anton Kuh   Walter Reisch   Werner Scheff
| narrator       = 
| starring       = Richard Tauber   Paul Hörbiger   Werner Fuetterer   Maria Matray
| music          = Paul Dessau
| editing        = 
| cinematography = Reimar Kuntze   Charles Métain
| studio         = Münchner Lichtspielkunst
| distributor    = Bavaria Film
| released       = 3 February 1930
| runtime        = 90 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German musical film directed by Max Reichmann and starring Richard Tauber, Paul Hörbiger and Werner Fuetterer. It premiered on 3 February 1930. 

==Cast==
* Richard Tauber as Stefan 
* Paul Hörbiger as Joachim 
* Werner Fuetterer as Peter 
* Maria Matray as Katja  
* Agnes Schulz-Lichterfeld as Die Mutter 
* Gustaf Gründgens as Jean 
* Edith Karin as Rote Finna 
* Sven Sandberg as Singer

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 


 
 