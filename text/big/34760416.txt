Blood Stained Shoes
{{Infobox film
| name           = Blood Stained Shoes
| image          = Blood stained shoes (2012) film.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Raymond Yip
| producer       = Manfred Wong Liang Ting
| writer         = 
| screenplay     = Manfred Wong Yang Mei Wen
| story          = 
| based on       =  
| narrator       = 
| starring       = Ruby Lin Kara Hui Monica Mok Anna Kay Michael Tong Xing Minshan
| music          = Lowell Lo
| cinematography = Zhou Shu Hao
| editing        = Shiney Yip, Derek Hui
| studio         = Star Union Skykee (Beijing) Film & Media Advertisement  Anhui Golden Bull Film & TV Investment 
| distributor    = Shenzhen Zhishang Film & TV Investment Anhui Golden Bull Film & TV Investment  Shaoxing Sheng Xia Film & TV Cultural Hefei Radio & TV Investment
| released       =  
| runtime        = 86 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = 
}}

Blood Stained Shoes ( ) is a 2012 Chinese horror film directed by Raymond Yip.    Set in the 1930s, the film tells the tale of a series of strange murders revolving around a pair of embroidered shoes.

==Cast==
* Ruby Lin as Su Er
* Kara Hui as Madame Zhen
* Monica Mok as Xu Shi
* Anna Kay as He ChuJun
* Michael Tong as Shen Xuanbai
* Xing Minshan as Wang Zhiyuan
* Jing Gangshan as Cheng Nan, Xu Shis lover
* Daniel Chan as Shen Xuanqing, Su Ers husband
* Han Zhi as Ding Dashan
* Harashima Daichi as Shen Ling
* Xiao Yuzhen Shen Yi
* Huang Yiyang as Shen He
* Cai Jifang as village elder Yuan
* Dong Jilai as village elder Huang
* Chen Jinhui as village elder Zhang

==Storyline==
This film is set in  the 1930s, and starts with a strange murder in a beautiful Jiangnan river delta. At the scene, all that is left is an exquisite pair of embroidered shoes. An innocent seamstress is suspected and put to death, but the bloody murders do not stop..

==Release==
* October 2011, Blood Stained Shoes invited Golden Rooster Award as Top 5 most anticipated movies. 
* Blood Stained Shoes had its premiere in Beijing on March 16, 2012. The film will receive wide release on March 31, 2012. 

==Reception==
*"smalltown ghost story thats more interesting for its female cast and its curious structure than anything else. In fact, its the most interesting part of the film, detailing the smalltown characters, the changing lives of its women and shifting family ties. The ghostly section of the film, with guilty parties dropping dead like flies, is relatively brief and not particularly scary "----Derek Elley, Film Business Asia 

==References==
 

==External links==
* 
*  
*  
*  

 

 
 
 
 
 
 


 
 