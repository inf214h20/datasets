Hare Ram
 
{{Infobox film
| name           = Hare Ram
| image          =
| caption        =
| director       = Harshavardhan
| producer       = Kalyan Ram
| writer         = Gowri Shankar
| narrator       = Swamiji-Vijay Ali Kota Venu Madhav Apoorva Sindhu Tolani
| music          = Mickey J Meyer
| cinematography = C. Ram Prasad
| editing        = Gowtham Raju
| studio         = N.T.R. Arts
| released       = 18 July 2008
| runtime        = 157 min.
| country        = India Telugu
| budget         =  120&nbsp;million
| Gross revenue  =
| preceded_by    =
| followed_by    =
| website        =
}} Telugu movie directed by Harshavardhan, and produced by Kalyan Ram, under N.T.R. Arts. Kalyan Ram plays the lead role while Priyamani, Ali (actor)|Ali, Brahmanandam, Kota Srinivasa Rao and Raghu Babu play supporting roles. Mickey J Meyer was the music director, C. Ram Prasad handled cinematography, and was edited by Gowtham Raju. The film was released on 18 July 2008. This movie is the next best hit for Kalyan Ram after Athanokkade which was a blockbuster.

==Plot==
The film is about an IPS officer Hari (Kalyan Ram) who is busy investigating the murders of a news reporter (Prabhakar) and a well known doctor (Rajeev Kanakala) and then discovers that these are the murders done by his twin Ram (also Kalyan Ram).

The story goes that Ram has a problem with his brain and is unable to control his hyper emotions about few things which makes him a beast at times. He would not even hesitate to kill and does not like anyone being praised or belittling him. In this process, he builds a grudge against his own brother who is good at everything and tries to kill him right during their childhood. Their mother (Seetha) is unable to tolerate all this and takes it upon herself to change Ram, so she takes him away from Hari to avoid further hatred but then not much change happens in the nature of Ram even after they grow up. Meanwhile, Hari is often chased by Anjali (Priyamani) who poses as a bank employee but in truth she is actually a CBI officer who comes to arrest Hari since she suspects his hand in the killings. Apparently, the doctor happens to be the younger brother of the health minister Siva Reddy (Kota Srinivasa Rao) and from then on the minister is closely on the heels of the killer. Anjali finally manages to arrest Hari successfully through a plan of hers and soon she realizes her folly when she chances upon his mother and understands the entire story of Hari and Ram.

Ram is initially thought to have committed the murders. Here comes a twist! Sravani (Sindhu Tolani), a journalist (Rams love interest) was jointly killed by the channel owner and the health minister when she exposes their plans of running a health mafia. Ram attacked the doctor to seek revenge. The doctor injected Ram with the deadly virus and Ram, deciding to sacrifice himself to save the people around him, fell under Haris car. He then asked Hari to shoot him to save the others. Hari shot him with no other alternative left. He then decided to kill the culprits by impersonating Ram. Hari is released from jail by the court for the purpose of catching. He then kidnaps the Health Minister in a van and takes him to a lonely place. When the policemen and the others gather there, Hari escapes from the van by means of the sewage pipe and gets into the public. He then triggers a bomb in the van. Rams mother thinks that Ram sacrificed his life for the country and feels proud of him. The films ends with Hari explaining the taxi driver (Brahmanandam), how he escaped from CBI custody.

==Cast==
* Kalyan Ram ... Hari / Ram
* Priyamani ... Anjali
* Sindhu Tolani
* Kota Srinivasa Rao ... Siva Reddy Ali ...
* Brahmanandam ...
* Raghu Babu ...

==Crew== Director ... Harshavardhan Producer ... Kalyan Ram
*Music Director ... Mickey J Meyer
*Cinematography ... C.Ram Prasad Editor ... Gowtam Raju
*Action ... Ram Lakshman, Selva

==External links==
* 
* 

 
 
 
 
 