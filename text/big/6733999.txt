Hercules and the Tyrants of Babylon
{{Infobox film
| name           = Hercules and the Tyrants of Babylon
| image          = Herculestyrantsbabylon.jpg
| caption        = A promotional film poster for "Hercules and the Tyrants of Babylon."
| director       = Domenico Paolella
| producer       = 
| writer         = Domenico Paolella Luciano Martino
| starring       = Peter Lupus (as Rock Stevens) Helga Liné Mario Petri
| music          = Angelo Francesco Lavagnino
| cinematography = Augusto Tiezzi
| editing        = 
| studio         = Romana Film
| distributor    = American International Pictures (USA)
| released       = 1964
| runtime        = 90 min
| country        = Italy
| awards         =  Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Italian film directed by Domenico Paolella and starring Peter Lupus.

== Plot ==
Asparia, Queen of the Hellenes, has been captured by the Babylonians, but she manage to hide her identity and lives as a common slave in Babylon.  Hercules, played by Peter Lupus (credited as Rock Stevens),  is sent to free her.  The Babylonian slavers begin to hear rumors and stories of a single man who can overcome any army he faces.  Asparia conspires with another slave to send a message of her whereabouts to Hercules, who soon is heading towards Babylon.

The three siblings who rule Babylon—beautiful Taneal, warlike Salman Osar and more conservative Azzur—are visited by King Phaleg of Assyria.  Phaleg showers the three with gifts, offering up untold riches in exchange for all of the slaves in Babylon.  The siblings are suspicious of Phalegs motives, thinking he means to raise an army from the slaves.  Taneal seduces and drugs the Assyrian king, discovering that he intends to find Queen Asparia and marry her, creating a powerful empire of Assyria and Hellas.  The siblings agree to stop this, and send troops to ambush the king.  Hercules discovers the plan and aids the Assyrians, as the Babylonians are his enemy, and saves the life of the king. Phaleg makes Hercules take a loyalty oath, and then sends him to Babylon, along with several of his men, to retrieve Asparia.

In Babylon, each of the siblings is conspiring against the other; Salman Osar and Azzur both wish to marry Asparia and form an empire, while Taneal intends to steal the wealth of the city and then destroy it by the means of a giant subterranean wheel which supports the foundation of all Babylon.  Hercules is able to locate Asparia, and then begins to turn the giant wheel and destroy the city.  Salman Osar kills his brother, then is crushed by falling debris while attempting to kill his sister.  As Hercules Assyrian escorts attempt to steal Asparia away to Phaleg, Taneal takes the Queen hostage herself.  Phaleg and his large contingent of cavalry ride in to claim his new bride, but they are met by Hercules as well as the freed Babylonian slaves.  Phaleg is killed by Hercules, and his soldiers routed; Taneal seemingly poisons herself rather than face the judgement of Hercules and Asparia.  In the end, Hercules leads Asparia and the Hellenes back to their homeland.

==Crew==
*Director: Domenico Paolella
*Assistant director : Giancarlo Romitelli
*Writer: Luciano Martino and Domenico Paolella
*Cinematography	: Augusto Tiezzi, Totalscope Eastmancolor
*Length: 94 min 				
*Producer: Fortunato Misiano
*Music: Angelo Francesco Lavagnino
*Distributor :	Cosmopolis Films et les Films Marbeuf
*Country of origin: Italy, Romana Film  Rome
*Editing: Jolanda Benvenuti
*Released  
*Adapted by Michel Lucklin
* Models and costumes by : Walter Patriarca
*Makeup: Massimo Giustini
*Genre : Sword-and-sandal

==Starring==
*  
* Helga Line : Thanit
* Mario Petri : Phaleg roi des Assyriens
* Anna-Maria Polani : Hesperie reine des Hellenes
*  
* Tullio Altamura : Assur
* Franco Balducci : Moksor
* Rosy De Leo : l’esclave Gilda
* Andrea Scotti : un jeune berger
* Diego Pozzetto : Behar
* Mirko Valentin : Glicon
* Diego Michelotti : Crissipo
* Eugenio Bottai : Ministre d’Assur
* Emilio Messina : lutteur
* Pietro Torrisi : lutteur
* Gilberto Galimberti : lutteur
* Amerigo Santarelli : lutteur
* Puccio Ceccarelli : lutteur

==See also==
* Sword and sandal
* List of films in the public domain

== External links ==
*  
*   

 
 
 
 
 
 
 