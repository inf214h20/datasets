The Messenger: The Story of Joan of Arc
 
{{Infobox film
| name           = The Messenger: The Story of Joan of Arc
| image          = Messenger poster.jpg
| caption        = French theatrical release poster
| director       = Luc Besson
| producer       = Patrice Ledoux
| writer         = Luc Besson Andrew Birkin
| starring       =  
| music          = Éric Serra
| cinematography = Thierry Arbogast
| editing        = Sylvie Landra Gaumont
| distributor    = Gaumont
| released       =  
| runtime        = 158 minutes   
| country        = France
| language       = English
| budget         = $60 million  
| gross          = $66,976,317 
}}

The Messenger: The Story of Joan of Arc ( ) is a 1999 French historical drama film directed by Luc Besson. The film stars Milla Jovovich, John Malkovich, Faye Dunaway and Dustin Hoffman. The screenplay was written by Besson and Andrew Birkin, and the original music score was composed by Éric Serra.

The Messenger portrays the story of St. Joan of Arc, the French war heroine and religious martyr of the 15th century. The story begins with young Joan as she witnesses the atrocities of the English against her family, and she is portrayed as having visions that inspire her to lead the French in battle against the occupying English forces. Her success in routing the English allows Charles VII to take the throne. Eventually Joan is tried and executed for heresy.

Bessons previous film, The Fifth Element, which also starred Jovovich, was a critical and financial success, and it had a positive influence on both their careers. The Messenger was intended to follow up that success and cement the status of Besson and Jovovich in film.  However, the film received mixed reviews from critics and underperformed at the box office,  earning just under $67 million on an $60 million budget.

==Plot==
 
As a child, Joan has a violent and supernatural vision. She returns home to find her village burning. Her sister Catherine tries to protect her by hiding her from the attacking English forces, part of a longstanding rivalry with France. Joan, while hiding, witnesses the brutal murder and rape of her sister. Afterward, Joan is taken in by distant relatives.
 Dauphin and Charles VII (John Malkovich), receives a message from the now adult Joan (Milla Jovovich), asking him to provide an army to lead into battle against the occupying English. After meeting him and his mother-in-law Yolande of Aragon (Faye Dunaway) she describes her visions. Desperate, he believes her prophecy.

Clad in armour, Joan leads the French army to the besieged city of Orléans. She gives the English a chance to surrender, which they refuse. The armies commanders, skeptical of Joans leadership, initiate the next mornings battle to take over the stockade at St. Loup without her. By the time she arrives on the battlefield, the French soldiers are retreating. Joan ends the retreat and leads another charge, successfully capturing the fort. They proceed to the enemy stronghold called the "Tourelles". Joan gives the English another chance to surrender, but they refuse. Joan leads the French soldiers to attack the Tourelles, though the English defenders inflict heavy casualties, also wounding Joan. Nevertheless, Joan leads a second attack the following day. As the English army regroups, the French army moves to face them across an open field. Joan rides alone toward the English and offers them a final chance to surrender and return to England. The English accept her offer and retreat.

Joan returns to Rheims to witness the coronation of Charles VII of France. Her military campaigns then continue to the walls of Paris, though she does not receive her requested reinforcements, and the siege is a failure. Joan tells King Charles VII to give her another army, but he refuses, saying he now prefers diplomacy over warfare. Believing she threatens his position, Charles conspires to get rid of Joan by allowing her to be captured by enemy forces. She is taken prisoner by the pro-English Burgundians at Compiègne, who sell her to the English.

Charged with the crime of heresy, based on her claim of visions and signs from God, she is tried in an ecclesiastical court proceeding, which is forced by the English occupation government. The English wish to quickly condemn and execute Joan, as English soldiers are afraid to fight while she remains alive. Bishop Cauchon expresses his fear of wrongfully executing someone who might have received visions from God. About to be burned for heresy, Joan is distraught that she will be executed without making a final Confession (religion)|confession. The Bishop tells her she must recant her visions before he can hear her confession. Joan signs the recantation. The relieved Bishop shows the paper to the English, saying that Joan can no longer be burned as a heretic. Whilst in her cell, Joan in confronted by an unnamed cloaked man (Dustin Hoffman), who is implied to be Joans conscience. The man makes Joan question whether she was actually receiving messages from God.

The frustrated English devise another way to have Joan executed by the church. English soldiers go into Joans cell room, rip her clothes and give her mens clothing to wear. They then state she conjured a spell to make the new clothing appear, suggesting that she is a witch who must be burned. Although suspecting the English are lying, the Bishop abandons Joan to her fate, and she is burned alive in the marketplace of Rouen.
 

==Cast==
* Milla Jovovich as Joan of Arc 
* John Malkovich as Charles VII of France
* Faye Dunaway as Yolande of Aragon
* Dustin Hoffman as The Conscience John Talbot
* Vincent Cassel as Gilles de Rais John II, Duke of Alençon
* Richard Ridings as La Hire
* Desmond Harrington as Jean dAulon
* Timothy West as Pierre Cauchon

==Themes==
Luc Besson stated that he was not interested in narrating the history of Joan of Arc; rather, he wanted to pull a message out of history that is relevant for today. Besson states that in order to achieve this he stepped away from the factual narrative of the 15th century, instead trying to get behind the "exterior envelope" and into both the emotional effect and affect of Joan. In the book The Films of Luc Besson, Susan Hayward interprets this as meaning Besson sought to follow Joan emotionally, revealing her doubts and demonstrating that one cannot return intact from the experience of war. 

As the medievalist Gwendolyn Morgan observes, Joans sanity is a continuing theme throughout the film, beginning with the priest questioning her as a child and ending with her conversations with The Conscience in the films final scenes.  Scholars view The Conscience as providing a postmodern explanation of Joans visions. At the time that Joan lived, her voices and visions would not have been doubted.  John Aberth, writing in the book A Knight at the Movies stated the filmmakers invented The Conscience to satisfy a modern audience that is aware of mental illness.    The film was also said to have "feminist undercurrents"; after Joan witnesses the rape of her sister, her crusade is said to become "a fight against male domination and the abuse of women."    Writing in Exemplaria, Nickolas Haydock also considered the witnessing of her sisters murder and rape to be an alternate psychological motivation for Joan to want to fight the English, rather than just her visions.   

Haydock also considered a theme in the film to be the inability of the church to fulfill individual spiritual needs.  This is said to be shown through many of Joans encounters with the church; as a girl she is scolded for going to confession too often, denied communion and forced to sneak into the church to take it herself, and during her trial, where she is denied confession until The Conscience confesses her instead. 

Writing in Studies in Medievalism XIII, Christa Canitz considered anti-intellectualism to be present in The Messenger; Joan admits to not knowing how to read or write and has not received any formal education, military or otherwise, yet triumphs over those who have.    Haydock commented that Joan possesses a quick wit which she uses against the unrelenting accusatory questions provided by her "intellectual superiors" during the trial. Joan also manages to triumph in battle where those with more experience and knowledge could not, made especially apparent by her use of a siege weapon backward to force open a gate. 

==Production==
Luc Besson was originally hired as executive producer for a film that was to be directed by Kathryn Bigelow. Bigelow had been developing ideas for a Joan of Arc film for about a decade. Her film was to be entitled Company of Angels, with Jay Cocks hired to write the script. The film was to be made with Bessons assistance and financial backing; in July 1996 contracts between Bigelow and Besson were exchanged, which gave Besson in addition to his personal fee, the right to be consulted on casting. According to Bigelow, eight weeks prior to filming, Besson realised that his then wife, Milla Jovovich, was not going to be cast as Joan, and he subsequently withdrew his support from the film, and with it the support of his financial backers. Bigelow threatened legal action for breach of contract and "stealing her research";  the matter was settled out of court.  After Besson left, he commenced production of his own Joan of Arc project, The Messenger, with Jovovich given the lead role; the production of Company of Angels disbanded shortly thereafter.  The Messenger was intended to follow up the success Besson and Jovovich achieved with their previous collaboration, The Fifth Element. 

Filming took place in the Czech Republic. A stuntman died in an accident during the first weeks of filming.  Besson was said to have become completely uncommunicative after the incident, only appearing on set to shout orders at people. 

==Soundtrack==
{{Infobox album
| Name        = The Messenger: The Story of Joan of Arc
| Type        = Soundtrack
| Artist      =  Éric Serra
| Cover       =
| Released    = 2 November 1999
| Recorded    =
| Genre       = Film score
| Length      =
| Label       = Sony Music Entertainment
| Producer    =
| Reviews     =  
| Last album  = The Fifth Element (1997)
| This album  = The Messenger: The Story of Joan of Arc (1999)
| Next album  = LArt (délicat) de la séduction (2001)
}}
 Carmina Burana.  Allmusic gave the album 3 out of 5 stars, stating it "combines orchestral, rock, and electronic elements for a sweeping, cinematic experience."  Dan Goldwasser from Soundtrack.net gave the soundtrack 4 out of 5, stating it was "very satisfying to listen to", though he expressed disappointment with the absence on the soundtrack of a particular piece of music present during Joans discussion with The Conscience.   

==Historical accuracy==
The scene in which Joan witnesses her sisters murder and posthumous rape by English soldiers in their village is entirely a fictional construction.    Joan and her family fled their village before it was attacked,  and it was actually attacked by the Burgundians, not the English.  In the film Joan is seen experiencing visions as a young child when the historical Joan asserted that these visions began around the age of 13. Joan is also seen finding her sword in a field as a child, whereas historically it was uncovered many years later on her journey to Chinon. 

Hayward credits Besson with showing the collaboration between the Burgundians and the English more accurately than previous filmmakers.  Many lines during scenes of Joans trial are taken verbatim from Joans real trial transcript.  Joan is shown receiving both wounds she was given in real life (an arrow above the breast and later an arrow to the leg), and the film includes some of the 15th-century accounts associated with Joan, such as being able to pick out Charles VII from among a group of his courtiers at Chinon.  The examining of Joans virginity was a real test Joan had to complete to prove her merit. 

==Release==

===Box office=== US $14,276,317 in the US, plus $52,700,000 from the rest of the world for a combined gross of $66,976,317. 

===Critical response===
The Messenger received mixed reviews. On   the film holds a score of 54 based on 33 reviews, indicating "mixed or average" reception. 

  participants who wrote, "Why does she have to die at the end?" on their evaluation cards.  In a review entitled Vivid Action Cant Save Miscast Joan , Todd McCarthy praised the films action scenes and technical aspects, including Thierry Arbogasts cinematography, though overall gave a negative review. He criticised the casting of Jovovich, stating the only thing she brought to the film was "her strikingly tall and skinny physicality, which is not exactly how one has been led to picture  ".    Ron Wells from Film Threat, however, gave the film four out of five stars. Also praising the action scenes, Wells stated the films main strength was its "adult ambiguities and relationships"; its decision not to portray Joan as a "super-hero", but rather to let the audience decide whether she was a prophet or merely Bipolar disorder|bipolar, concluding, "This film, as most things that involve religion, is better understood if you learn not to take everything so literally."   

==Accolades==
The Messenger was nominated for eight awards at the 25th César Awards of which it won two; one for Costume Design and one for Best Sound.   The film also won two Lumières Award for Best Director and Best Film.  It was nominated for Most Original trailer at the 1999 Golden Trailer Awards,  Best Costume Design and Best Production Design at the 1999 Las Vegas Film Critics Society Awards, and won the Golden Reel Award for Best Sound Editing. 

Conversely, Milla Jovovichs performance was nominated for a Golden Raspberry Award for Worst Actress. 

{| class="wikitable"
|-
! Year
! Event
! Award
! Nominee
! Result
|- 20th Golden 1999
|Golden Raspberry Awards Golden Raspberry Worst Actress Milla Jovovich
| 
|- 1999
|Golden Trailer Awards Most Original Imaginary Forces
| 
|- 1999
|rowspan=2|Las Vegas Film Critics Society Awards Best Costume Design Catherine Leterrier
| 
|- Best Production Design Hugues Tissandier
| 
|- 25th César 2000
|rowspan=8|César Award Best Costume Design Catherine Leterrier
| 
|- Best Sound
|François Groult, Bruno Tarriere, Vincent Tulli
| 
|- Best Director Luc Besson
| 
|- Best Photography Thierry Arbogast
| 
|- Best Editing Sylvie Landra
| 
|- Best Music
|Éric Serra
| 
|- Best Production Design Hugues Tissandier
| 
|- Best Film The Messenger: The Story of Joan of Arc
| 
|- 2000
|Motion Golden Reel Award Best Sound Editing: Foreign Feature Sound production team
| 
|- 2000
|rowspan=2|Lumières Award Best Director Luc Besson
| 
|- Best Film The Messenger: The Story of Joan of Arc
| 
|}

==Home media==
The Messenger was released on DVD on 4 April 2000.    The DVD version presented the film in its original 2.35:1 format, and contained several minutes of footage that did not appear in the US theatrical version. It featured English subtitles, interactive menus, talent files, a 2-page production booklet, a 24-minute  ,   and Orlando (film)|Orlando. The DVD also containted Éric Serras original score for the film, which was presented in Dolby Digital 5.1 Surround Sound, as was the film itself. Whilst criticising the film itself, Heather Picker of DVD Talk gave a favourable review of the DVD release. 

The Blu-ray disc|Blu-ray version was released on 2 December 2008.  It contains audio in English, French, Portuguese, Spanish, and Thai, and subtitles in 10 languages. Glenn Erickson of DVD Talk also criticised the film, yet praised the Blu-ray release, rating it 4½  stars out of 5 for its video quality and 4 out of 5 for its audio, though only giving it 1½ stars for its extras, noting the lack of special features.    A review from Blu-Ray.com also gave a favourable review of both the audio and visual quality, stating "I dont think that there is much here one could be dissatisfied with."  High-Def Digest, however, gave a more negative review. Whilst praising the audio quality, the lack of special features was criticised, as was the video quality, which was described as being "smothered" with edge enhancement. The reviewer concluded, "Sony is practically begging people not to buy it." 

==References==
Notes
 
Bibliography
 
* 
}}
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 