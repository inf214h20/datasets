Tiger Fangs
{{Infobox film
| name           = Tiger Fangs
| image          = fangs17.jpg
| image_size     = 190px
| caption        = Theatrical poster
| director       = Sam Newfield Jack Schwarz Fred McConnell Arthur St. Claire
| narrator       = Frank Buck Duncan Renaldo Dan Seymour Arno Frey J. Farrell MacDonald Pedro Regas
| music          = Lee Zahler
| cinematography = Ira H. Morgan
| editing        = George M. Merrick
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Frank Buck and June Duprez, directed by Sam Newfield for Producers Releasing Corporation.

==Plot== Frank Buck (right).]]
 ) and his portly accomplice Henry Gratz. Thereafter, life is safe once again in the jungle.

==Cast== Frank Buck as Frank Buck

*June Duprez as Linda McCardle

*Duncan Renaldo as Peter Jeremy

*Howard Banks as Tom Clayton

*J. Farrell MacDonald as Geoffrey MacCardle

*Alex Havier as Ali (credited as J. Alex Havier)

*Arno Frey as Dr. Lang

*Dan Seymour as Henry Gratz

*Pedro Regas as Takko

==Reception== Frank Buck actioner exciting. Its a fiction piece, and not the usual jungle travelogue…June Duprez is as attractive a biologist as one could hope to meet up with in the middle of the jungle.”  

“The animal shots are eye-filling, as usual, and especially well photographed…Theyre
convincing enough…to keep the younger generation glued to movie house seats. Sam Newfield directed with a good sense of melodramatic action, and it is Mr. Buck himself who gives the stand-out performance. The jungle fellow is a right natural actor.”  

As of 2014, Tiger Fangs holds a two and a half star rating (5.1/10) on IMDb.

==References==
 
 

==Bibliography==
* {{cite book
  | last = Lehrer 
  | first = Steven
  | title = Bring Em Back Alive: The Best of Frank Buck
  | publisher = Texas Tech University press
  | year = 2006
  | pages = 248
  | url = http://books.google.com/books?id=UNnhbq9gwTUC
  | isbn = 0-89672-582-0 
}}

==External links==
 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 


 