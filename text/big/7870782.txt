Parinayam (1994 film)
{{Infobox film
| name           = Parinayam
| image          = Parinayam_movie.jpg Hariharan
| producer       = G. P. Vijayakumar
| writer         = M. T. Vasudevan Nair Mohini  Vineeth  Manoj K. Jayan Nedumudi Venu Thilakan
| music          = {{Plainlist|
* Songs:
* Bombay Ravi
* Background score:
* Johnson
* Lyrics:
* Yusuf Ali Kechery
}}
| cinematography = S. Kumar 
| editing        = M. S. Mani
| distributor    = Seven Arts
| released       = 1994
| country        = India
| language       = Malayalam
}}
 Hariharan and written by M. T. Vasudevan Nair. The key characters are played by Mohini (actress)|Mohini, Vineeth and Manoj.K.Jayan. The film also has a cast of character artists including Thilakan, Nedumudi Venu, Oduvil Unnikrishnan, Jagathy Sreekumar, Sukumari, Jagannatha Varma, Valsala Menon, Bindu Panicker, Bahadoor, Anila and Shanthi Krishna.

Parinayam won four national awards (Best Screenplay, Best Film on Other Social Issues, Best Music Direction and Special Jury Award for Cinematography), also won several Kerala State Film Awards. This film deals with the mental and physical agony that a young Namboothiri widow had to undergo for losing her chastity. Parinayam  was loosely based on Smarthavicharam (ritualistic trial for adultery) of Kuryeddath Thathri, that created a shock among Kerala in early twentieth century that led to the reformation of Namboothiri community. It remains the finest feminist film in Malayalam. The script found the perfect director in Hariharan. 

==Plot==
Unnimaya (Mohini (actress)|Mohini), a young girl is married to Palakunnath Namboothiri ( Jagannatha Varma), a man in his sixties. She is his fourth wife. Unnimaya is an educated girl hailing from Kizhakkedath Mana, a progressive family in Kerala. Due to various social and economic factors, she is compelled to marry Palakunnath Namboothiri, a rich gentleman. Coming from a progressive household, Unnimaya finds it hard to adjust with the severe orthodox practices at her new home. The sudden death of her husband brings her face-to-face with the customary rituals practiced among Namboothiri community towards widowed women. She realizes that her widowhood makes her almost a shunned individual - one who cannot participate in any celebrations, or even attend any music/dance events or performances. Kunjunni Namboothiri (Manoj.K.Jayan), the elder son of Palakkunath, is the only person who shows compassion and support towards her. Kunjunni is actively involved in reformation among the Namboothiris and is considered as a rebel among the orthodox community. Unnimaya meets Madhavan (Vineeth), an upcoming Kathakali artist, and falls in love with him. They share some intimate moments, and later Unnimaya realizes that she is pregnant. The orthodox Namboothiri community is shocked when it learns about her  pregnancy and decides to excommunicate her through Smarthavicharam. A group of senior Namboothiris, under the leadership of Moothedath Bhattathiri (Thilakan), conducts a series of rituals, first to extract the name of the one who impregnated her, and then later, to throw her out of the community. Unnimaya expects Madhavan to come save her, but he is unable to muster the courage to rescue her. Realizing that he is a coward and that she cannot expect him to deliver her out of the situation, Unnimaya decides to stand up to the orthodox Namboothiris. She answers their questions with clarity and confidence, angering them further. Ultimately, the decision is made to excommunicate her, and all the necessary rituals are completed. Kunjunni arrives as her savior. He gives her shelter at his home. The progressive Yogakshema Sabha, that he is part of, finds his ways too bohemian and dismisses him from the group. Madhavan, realizing his mistake, arrives  to accept Unnimaya, but now she shows him the door declaring that he is not the father of her unborn child, and that the fathers are Ajuna, Bhima, Nala (the heroic characters performed by Madhavan as part of his dance performances). Unnimaya involves herself in social service and becomes a Congress volunteer deciding to do something for the downtrodden society.

==Cast== Mohini  as Unnimaya Antharjanam
*Vineeth as Madhavan
*Manoj K.Jayan as Kunjunni
* Ravi Menon as Krishnan
*Thilakan as Moothedam
*Nedumudi Venu as Aphan Namboothiri
*Jagathy Sreekumar as Mullassery
*Oduvil Unnikrishnan as Othikkan
*T. P. Madhavan
* Anoop Raja as Vasudevan
* KPAC Premachandran as Thekkumthala Govindan
*Sukumari as Kunjikavu
*Shanthi Krishna as Mathu
*Bahadoor as Kizhakkedam
*Jagannatha Varma as Palakkunnam
* Anila  Sreekumar as Nanikutty Santhakumari as Madhavans Mother
*Bindu Panicker as cheriya Athemaru
*Valsala Menon as Valiya Athemaru
*Sreejaya

==Soundtrack==
 Bombay Ravi for which the acclaimed lyrics were penned by Yusufali Kechery. All the songs of this movie were instant hits.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Raga
|- Maand
|-
| 2 || "Samaja Sancharini" || K. J. Yesudas|Dr. K. J. Yesudas || Kambhoji
|- Chorus || Mohanam
|- Kalyani
|-
| 5 || "Shanthakaram" || K. S. Chithra || Anandabhairavi
|-
| 6 || "Samaja Sancharini" || K. S. Chithra || Kambhoji
|- Kalyani
|-
|}

==Awards== National Film Awards Best Film on Other Social Issues Best Screenplay - M. T. Vasudevan Nair Best Music Bombay Ravi Special Jury Award for Cinematography - S. Kumar

; Kerala State Film Awards Best Film Best Screenplay - M. T. Vasudevan Nair Best Male Playback Singer - K. J. Yesudas|Dr. K. J. Yesudas Best Female Playback Singer - K. S. Chithra

==External links==
 
* 

==References==
 

 
 

 
 
 
 
 