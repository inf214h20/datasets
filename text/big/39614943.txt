The Little Cafe (1919 film)
{{Infobox film
| name = The Little Cafe
| image =The Little Cafe (1919) - Ad 1.jpg
| image_size =190px
| caption =Ad for the 1920 American release
| director = Raymond Bernard
| producer = 
| writer = Tristan Bernard (play)   Henri Diamant-Berger   Max Linder   Raymond Bernard
| narrator = Joffre   Wanda Lyon
| music =
| cinematography =  
| editing = 
| studio = Pathé
| distributor = Pathé 
| released = 19 December 1919
| runtime = 55 min.
| country = France French intertitles)
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} French silent silent comedy The Little Cafe by Tristan Bernard. 

==Synopsis==
After inheriting a large sum of money, a Parisian waiter has to keep working in a cafe to honour his contract to his unscrupulous employer. While working there he falls in love with his employers daughter.

==Cast==
* Max Linder as Albert 
* Armand Bernard as Bouzin  Joffre as Philibert 
* Wanda Lyon as Yvonne 
* Flavienne Merindol as Edwige  Halma as Bigredon 
* Major Heitner as Pianiste Tzigane 
* Andrée Barelly as Bérangère dAquitaine 
* Henri Debain as Le plongeur

==References==
 

==Bibliography==
* Bradley, Edwin M. The First Hollywood Musicals: A Critical Filmography Of 171 Features, 1927 Through 1932. McFarland, 2004.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 