Desamuduru
 
 

{{Infobox film name = Desamuduru image = Desamuduru_Poster.jpg writer = Puri Jagannadh starring = Pradeep Rawat Ali
|director = Puri Jagannadh producer = D.V.V. Danayya cinematography = Shyam K. Naidu country = India distributor = Universal Media editing = Marthand K. Venkatesh music = Chakri
|released = 12 January 2007 runtime = 148 minutes language = Telugu
| budget =   (citation needed)  http://www.imdb.com/title/tt0843335/business 
| gross =  (4 Weeks) 
}} Telugu action film directed by Puri Jagannadh and produced by D.V.V. Danaiah under the Universal Media banner. The film stars Allu Arjun and Hansika Motwani in the lead. The movie was released on 12 January 2007 in 400 theatres. {{cite web| title=indiaglitz.com| work= Desamuduru set for release in 400 theatres|url= http://www.indiaglitz.com/channels/telugu/article/28391.html Rambha acted in an item song alongside Allu Arjun. {{cite web| publisher=telugucinema.com| title= Rambhas item number in Desa Muduru|url= http://www.telugucinema.com/tc/news/desamuduru_dec1306.php archiveurl = archivedate = 6 January 2007}}  It was dubbed into   as Ek Jwalamukhi.

==Plot== Pradeep Rawat). Manali to shoot a travel episode instead of another team. There, Bala sees Vaishali (Hansika Motwani), who is a sannyasin. It is love at first sight for Bala. Vaishali, though initially apprehensive of Bala falls for him. The head sannyasin of the ashram Rama Prabha (Rama Prabha) unites the couple. However, before Bala is able to return to Hyderabad with Vaishali and marry her, Tambi Durais henchmen kidnap her and take her his home. Bala returns to Hyderabad and finds that she is with Tambi Durai. The person who was saved by Bala reveals that Vaishali is the daughter of Narayan Patwari (Devan (actor)|Devan). In order to grab Patwaris property, Tambi Durai killed her parents and his wife (Telangana Shakuntala) performed the marriage of Vaishali with Murugan in front of their dead bodies. However, Vaishali escapes and reaches Kulu Manali. Bala takes the help of Inspector Prasad (Ahuti Prasad) to save Vaishali from their clutches and eventually succeeds in his attempt and marries her.

==Cast==
  was selected as the lead actress opposite Allu Arjun making her debut in Telugu cinema.]]
* Allu Arjun as Bala Govind
* Hansika Motwani as Vaishali Chandra Mohan as Chandra Mohan Ali as Shankar/Himalayan Baba Pradeep Rawat as Tambi Durai Jeeva as Ponnuswamy
* Subbaraju as Murugesan
* Raghu Babu as compounder Ajay as Tambi Durais henchman
* Ahuti Prasad as A. Prasad
* G. V. Sudhakar Naidu as Tambi Durais henchman
* Rama Prabha as Rama Prabha
* Srinivasa Reddy as Ravi
* Satyam Rajesh as Rajesh
* Junior Relangi as Balas colleague
* Gundu Sudharshan as Sudarshan
* Telangana Shakuntala as Tambi Durais wife Hema as Ponnuswamys wife
* Kovai Sarala as Shivani Devan as Narayan Patwari
* Raja Ravindra
* Sony Raj as Balas friend
* Shankar Melkote as doctor
* Phani as Balas friend
* Uttej as Radio Mirchi reporter Venu Madhav as tea seller
* Gowtham Raju as apples exporter
* Narsing Yadav as police inspector Rambha in the item number "Attaantode Ittaantode"

==Crew==
* Director: Puri Jagannadh
* Story: Puri Jagannadh
* Dialogue: Puri Jagannadh
* Producer: D.V.V. Danayya Chakri
* Cinematography: Shyam K. Naidu
* Editor: Marthand K Venkatesh Vijayan

== Production == Vijay and Asin in the lead roles.  He later accepted to act in a film directed by Puri Jagannadh and produced by D. V. V. Danayya under the banner Universal Media.  The title was confirmed as Desamuduru.  Puri Jagannadh worked on the films script at Bangkok and returned to Hyderabad on 16 June 2006.  The film had its official launch at the office of Puri Jagannadhs production company Vaishno Academy on 19 June 2006. 
 Rambha was selected for an item number in the film, despite initial reports stated that Charmee Kaur would dance in the song.  

=== Filming ===
  from Kullu Valley, Himachal Pradesh.]] Hyderabad where Manali in early September 2006.  Weeks later, few scenes between the lead pair were shot in Uttaranchal and Himachal Pradesh. This schedule was planned to be wrapped up on 5 December 2006.  However the schedule was completed  on 10 October 2006.  Some crucial action sequences were shot at Aluminium Factory in Gachibowli in early December 2006. 

==Soundtrack==
 

The film has six songs composed by Chakri (music director)|Chakri. Music of the film was launched on 25 December 2006. The song Ninne Ninne was the most popular song in the soundtrack among music lovers. {{cite web| title=greatandhra.com| work= Ninne Ninne Rocking Music Lovers|url= http://www.greatandhra.com/movies/news/jan2007/desa_music.php archiveurl = archivedate = 12 January 2007}} 

==Release==
The film was released on 500 screens, including 424 in Andhra Pradesh, 32 in Karnataka, 8 in Orissa, 2 in Chennai, 3 in Mumbai and 31 overseas. 

==Box-office==
Desamuduru ran for more than 200 days. The film grossed   125.8&nbsp;million    and a share of   95&nbsp;million. {{cite web| title=nonstopcinema.com| work= Desamuduru worldwide 7-day gross – 125.8&nbsp;million|url= http://www.nonstopcinema.com/nsc/headlines/news_content.php?fileName=229 archiveurl = archivedate = 22 January 2007}}  worldwide in the first week of its release.

The film grossed   300&nbsp;million in four weeks. {{cite web| title=greatandhra.com| work= Desamuduru Collected Rs. 29,63,00,655 in 4 Weeks |url= http://www.greatandhra.com/movies/news/feb2007/desa_collect.php archiveurl = archivedate = 16 February 2007}} 

Desamuduru grossed   in 6 Weeks. later on the film ended up with a final figure of   http://www.idlebrain.com/celeb/int1/alluarjun.html  in 200 Days worldwide delivering a Share of  .  The film is one of the blockbusters in Allu Arjuns career and the highest grossing film of 2007. The film surpassed the records set by Indra (2002 film)|Indra (2002) and became the second highest grossing film after Pokiri (2006). 

==Trivia==
With this film, Allu Arjun became the first South Indian actor  and second Indian actor get a Rectus abdominis muscle|six-pack abs after Salman Khan.

==Dubbed version==

Desamuduru was dubbed into   as Ek Jwalamukhi.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 