The Sacrifice
 
{{Infobox film
| name           = The Sacrifice
| image          = sacrificebritish1.jpg
| caption        = British film poster
| director       = Andrei Tarkovsky
| producer       = Anna-Lena Wibom
| writer         = Andrei Tarkovsky
| starring       = Erland Josephson Susan Fleetwood Allan Edwall Guðrún S. Gísladóttir Sven Wollter Valérie Mairesse Filippa Franzen Tommy Kjellqvist
| music          = Johann Sebastian Bach Watazumi Doso|Watazumido-Shuso
| cinematography = Sven Nykvist
| editing        = Andrei Tarkovsky Michał Leszczyłowski Sandrew (Swedish theatrical)
| released       =  
| runtime        = 142 minutes  
| country        = Sweden United Kingdom France
| language       = Swedish English French
| budget         = 
}} Grand Prix at the Cannes Film Festival.

==Plot==
The film opens on the birthday of Alexander (Erland Josephson), an actor who gave up the stage to work as a journalist, critic, and lecturer on aesthetics. He lives in a beautiful house with his actress wife Adelaide (Susan Fleetwood), stepdaughter Marta (Filippa Franzén), and young son, "Little Man", who is temporarily mute due to a throat operation. Alexander and Little Man plant a tree by the sea-side, when Alexanders friend Otto, a part-time postman, delivers a birthday card to him. When Otto asks, Alexander mentions that his relationship with God is "nonexistent". After Otto leaves, Adelaide and Victor, a medical doctor and a close family friend who performed Little Mans operation, arrive at the scene and offer to take Alexander and Little Man home in Victors car. However, Alexander prefers to stay behind and talk to his son. In his monologue, Alexander first recounts how he and Adelaide found this lovely house near the sea by accident, and how they fell in love with the house and surroundings, but then enters a bitter tirade against the state of modern man. As Tarkovsky wrote, Alexander is weary of "the pressures of change, the discord in his family, and his instinctive sense of the threat posed by the relentless march of technology"; in fact, he has "grown to hate the emptiness of human speech".   

The family, as well as Victor and Otto, gather at Alexanders house for the celebration. Their maid Maria leaves, while nurse-maid Julia stays to help with the dinner. People comment on Marias odd appearances and behavior. The guests chat inside the house, where Otto reveals that he is a student of paranormal phenomena, a collector of "inexplicable but true incidences." Just when the dinner is almost ready, the rumbling noise of low-flying jet fighters interrupts them, and soon after, as Alexander enters, a news program announces the beginning of what appears to be all-out war, and possibly nuclear holocaust. In despair, he vows to God to sacrifice all he loves, even Little Man, if this may be undone. Otto advises him to slip away and lie with Maria, whom Otto convinces him is a witch, "in the best possible sense". Alexander takes his gun, leaves a note in his room, escapes the house, and rides his bike to where she is staying. She is bewildered when he makes his advances, but when he puts his gun to his temple ("Dont kill us, Maria"), at which point the jet-fighters rumblings return, she soothes him and they consummate while floating above her bed, though Alexanders reaction is ambiguous.

When he awakes the next morning, in his own bed, everything seems normal. Nevertheless, Alexander sets forth to give up all he loves and possesses. He tricks the family members and friends into going for a walk, and sets fire to their house when they are away. As the group rushes back, alarmed by the fire, Alexander confesses that he set the fire himself, and furiously runs around. Maria, who until then was not seen that morning, appears in the fire scene; Alexander tries to approach her, but is restrained by others. Without explanation, an ambulance appears in the area and two paramedics chase Alexander, who appears to have lost control of himself, and drive him off. Maria begins to bicycle away, but stops halfway to observe Little Man watering the tree he and Alexander planted the day before.  As Maria leaves the scene, the "mute" Little Man, lying at the foot of the tree, speaks his only line, which quotes the opening Gospel of John: "In the beginning was the Word.  Why is that, Papa?"

==Cast==
* Erland Josephson as Alexander
* Susan Fleetwood as Adelaide
* Allan Edwall as Otto
* Guðrún Gísladóttir as Maria
* Sven Wollter as Victor
* Valérie Mairesse as Julia
* Filippa Franzén as Marta
* Tommy Kjellqvist as Gossen (Little Man)
* Per Källman, Tommy Nordahl as ambulance drivers

==Production==

===Pre-production===
The Sacrifice originated as a screenplay entitled The Witch, which preserved the element of a middle-aged protagonist spending the night with a reputed witch. However, in this story, his cancer was miraculously cured, and he ran away with the woman.   |group="n"}}
In March 1982, Tarkovsky wrote in his journal that he considered this ending "weak", as the happy ending was unchallenged.  He wanted personal favorite and frequent collaborator Anatoly Solonitsyn to star in this picture, as was also his intention for Nostalghia,  but when Solonitsyn died from cancer in 1982, the director rewrote the screenplay into what would become The Sacrifice and also filmed Nostalghia with Oleg Yankovsky as the lead. 

Tarkovsky considered The Sacrifice different from his earlier films because, while he commented that his recent films had been "impressionistic in structure", in this case he not only "aimed...to develop   episodes in the light of my own experience and of the rules of dramatic structure", but also to "  the picture into a poetic whole in which all the episodes were harmoniously linked", and that because of this, it "took on the form of a poetic parable".  
 
 Sydney Pollacks Out of Africa, Nykvist later said that it was "not a difficult choice", and like Josephson, he became a co-producer when he invested his fees back into the film.    Production designer Anna Asp, who worked on Bergmans Autumn Sonata and After the Rehearsal, and had won an Academy Award alongside Susanne Lingheim for Fanny and Alexander,  also joined the project, as well as Daniel Bergman, one of Ingmars children, who worked as a camera assistant.  Many critics would comment on The Sacrifice in the context of Bergmans work. 

===Filming===
While often    erroneously claimed to have been shot on Fårö,  The Sacrifice was actually filmed on the nearby island of Gotland; the Swedish military denied Tarkovsky access to Fårö.   

Alexanders house, specially built for the production, was to be burned for the climactic scene, in which Alexander burns his house and his possessions. The shot was very difficult to achieve, and the first failed attempt was, according to Tarkovsky, the only problem during shooting. Despite Sven Nykvists protest, only one camera was used for this scene, and while shooting the burning house, the camera jammed and the footage was thus ruined.  

The scene had to be reshot, requiring a quick and very costly reconstruction of the house in two weeks. This time, two cameras were set up on tracks, running parallel to each other. The footage in the final version of the film is the second take, which lasts for six minutes (and ends abruptly because the camera had run through an entire reel). The cast and crew broke down in tears after the take was completed.  Directed by Andrei Tarkovsky. Dir. Michal Leszczylowski. Perf. Brian Cox, Erland Josephson and Andrey Tarkovskiy. Svenska Filminstitutet (SFI), 1988. 

===Post-production===
Tarkovsky and Nykvist performed significant amounts of color reduction on select scenes. According to Nykvist, almost sixty percent of the color was removed from these parts. 

==Reception== Grand Prix, FIPRESCI Prize Best Film Best Actor BAFTA Award for Best Foreign Language Film. 
 Vatican compiled Andrei Rublev. 
 Andrei Rublev, writing that, while Rublev "  the advances of an alluring pagan witch as incompatible with Christian love", The Sacrifice "juxtaposes" both sensibilities. 

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*   at nostalghia.com

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 