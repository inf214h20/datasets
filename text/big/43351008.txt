Shockwave, Darkside
 
{{Infobox film
| name           = Shockwave Darkside
| image          = 
| alt            = 
| caption        = 
| film name      = Shockwave Darkside
| director       = Jay Weisman Red Giant Media, Pipeline Entertainment
| writer         = Jay Weisman
| starring       = Mei Melançon, Bill Sage, Sonequa Martin-Green, Rich Ceraulo, Alexander Cendese
| narrator       = 
| music          = Andreas Weidinger
| cinematography = Robert Fiske, Joe Gabriel
| editing        = Eric Dow, Doug Fitch
| studio         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
 Red Giant Media. The UK Premiere occurred on August 22, 2014 at the London FrightFest Film Festival. 

==Plot==
The nano-plague that poisoned Earth’s water supply has reached its 60-year critical mass. The Unlight enemy forced the first exodus to the moon where the outlawed banished population was supposed to die. But now the Unlights have launched from Earth and are amassing on the south-west sector of the darkside of the moon for a massive ice-mining operation.

It is the last Great War and lunar troops are sent into battle for the precious resource. However, one squad is shot down and the five surviving soldiers find themselves stranded. Cut off and behind enemy lines, they start a dangerous journey through snipers and minefields back to their extraction point with only 36 hours of oxygen left. As their numbers dwindle and nerves fray, they make an amazing discovery about the moon that just might save their lives.

==Cast==
Main Cast:
*Bill Sage as Dalton
*Mei Melançon as The Machine
*Sonequa Martin-Green as Private Lang
*Rich Ceraulo as Corporal Kim
*Alexander Cendese as Private Schorr

Other cast (in alphabetical order):
* Elwaldo Baptiste
* James Barrett
* Brianne Blessing
* Nedra Gallegos
* Gus Kelley
* C.R. Marchi
* Brian Morrison
* Joe Sobalo Jr.

==Related==
A prequel web-comic of the same name was published by Keenspot in 2011. Created and written by Jay Weisman. Pencil Artwork by Weilin Yang. Finishes by Youjun Yang. Colors by Kun Song. Letters & Edits by Benny R. Powell    The 3D aspect of the comic got mixed reviews.  Crabcake Confidential stated "either my eyes are broken, or the 3D thing isnt working for me."  

==Reviews==
* Shockwave, Darkside (movie review) 
* Frightfest 2014-Shockwave Darkside 3D Review  
* Shockwave, Darkside review  
* Shockwave, Darkside 3D  
* FrightFest 2014 Day 2 

==References==
*http://www.postmagazine.com/Publications/Post-Magazine/2014/July-1-2014/Into-The-Void-completes-VFX-for-Shockwave-Darksi.aspx
 

==External links==
*  
*   List of 3D films
 
 