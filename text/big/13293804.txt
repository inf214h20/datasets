Getting Home
{{Infobox Film
| name           = Getting Home
| image          = GettingHomePoster.jpg
| image_size     = 
| caption        = Zhang Yang
| producer       = Stanley Tong Er Yong Zhang Yang Wang Yao
| narrator       =  Xia Yu Wu Ma
| music          = Dou Peng
| cinematography = Yu Lik-wai Lai Yiu-fai	 	
| editing        = Yang Hongyu
| distributor    = Worldwide: Fortissimo Films
| released       = Hong Kong: January 25, 2007 Berlin International Film Festival|Berlin: February 11, 2007
| runtime        = 90 min. PR China Hong Kong Mandarin Chinese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}	
 Chinese comedy/drama Zhang Yang and starring Chinese comedian Zhao Benshan. It is episodic and follows two workers in their 50s, Zhao (Zhao Benshan) and Liu (Hong Qiwen). The film opens when Liu unexpectedly dies after a night of drinking and Zhao decides to fulfill a promise to his friend to get him home, beginning a long odyssey from Shenzhen to Chongqing with Lius corpse on his back. Along the way, Zhao meets a variety of figures, played by several of Chinas better known character actors. 

Getting Home is Zhang Yangs fifth feature film. It was produced by Filmko Entertainment of Hong Kong and the Beijing Jinqianshengshi Culture Media company of the Peoples Republic of China. International sales and distribution was by Fortissimo Films out of Amsterdam. 

Getting Homes original title derives from a Chinese proverb meaning "A falling leaf returns to its roots."    It is apparently based on a true story.  ) 

==Cast==
*Zhao Benshan as Zhao a middle-age worker who decides to carry his deceased friend home
*Hong Qiwen as Liu
*Song Dandan as a middle-age homeless woman who sells her blood for money
*Guo Degang as a ringleader of a gang of thieves who attempts to hold up the bus Zhao first uses to transport Lius corpse
*Hu Jun as a trucker who drives Zhao and Liu a part of the way Xia Yu as a cyclist attempting to bicycle to Tibet
*Wu Ma as an elderly, wealthy, lonely man who Zhao meets along his journey
*Liu Jinshan as a thuggish restaurateur Chen Ying Guo Tao as husband and wife beekeepers who have rejected modern society

==Production==
Originally titled Air,    Getting Home was financed by Filmko Films and Fortissimo Films and produced by Peter Loehr (of Ming Productions and the Imar Film Company) and Wouter Barendrecht of Fortissimo.  This marked the first collaboration between Zhang and Filmko but the fifth with Fortissimo.  

Though the film documents Zhaos journey from Shenzhen to Chongqing, the majority of shooting took place in Yunnan, a Chinese province in the southwest. 

==Reception== Deauville  and the New York Asian Film Festival. 

Western critics, meanwhile, have embraced the film, with several noting that, while the synopsis recalls the American comedy Weekend at Bernies, Getting Home far surpasses that film in plot, cast, and drama. One critic notes that the film is "a perfectly pitched and quite heartwarming drama about friendship and promises, with a welcome drop of dark humour."  Several magazines, including Variety (magazine)|Variety and thats Beijing praised the performance of Zhao Benshan in particular as "finely calibrated" and "vivid" respectively.  

==References==
 

==External links==
* 
* 
* 
*  at the Chinese Movie Database
*  from distributor Fortissimo Films

 

 
 
 
 
 