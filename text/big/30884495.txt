Dead Cert (2010 film)
{{Infobox film
| name           = Dead Cert
| image          = Dead_Cert_(2010_film).jpg
| caption        = 
| director       = Steven Lawson
| producer       = Steven Lawson
| writer         = Ben Shillito
| starring       = 
| music          = Tim Atack
| cinematography = James Friend
| editing        = Jason de Vyea Wade Jackson
| studio         =   Raw Film Productions
| distributor    = Momentum Pictures
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = $1.5 million
}} supernatural horror film written and directed by Steven Lawson.

Dead Cert is based on an idea by Garry Charles and Steven Lawson and written by Ben Shillito. Principal photography began in October 2009 and the film had a worldwide release in 2010. 

In 2011 Shout! Factory released Dead Cert on DVD and Blu-ray in Region 1.

==Plot==
 
Freddy Frankham (Craig Fairbrass) thought he was out of the gangland world, a retired boxer, Freddy now owns a successful “gentlemen’s” nightclub. But when a gang of Romanian drug dealers, lead by the enigmatic Dante Livienko (Billy Murray), move into London, the stakes are too good to resist one last gamble. 

==Cast==
 
* Perry Benson as Magoo
* Steven Berkoff as Kenneth Mason
* Danny Dyer as Roger Kipling
* Joe Egan as Griffiths
* Craig Fairbrass as Freddy Dead Cert Frankham
* Jason Flemyng as Chelsea Steve
* Dexter Fletcher as Eddie Christian
* Ciaran Griffiths as Skender
* Dave Legeno as Yuvesky
* Lisa McAllister as Jen Christian
* Roland Manookian as Chinnery
* Janet Montgomery as Giselle Billy Murray as Dante Livenko
* Lucinda Rhodes-Flaherty as Katy
* Andrew Tiernan as Chekha
 

==References==
*  

{{Reflist|refs=
   
}}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 