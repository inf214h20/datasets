A Better Tomorrow (2010 film)
{{Infobox film
| name           = A Better Tomorrow
| image          = A Better Tomorrow (2010 film) film poster.jpg
| caption        = Promotion poster for A Better Tomorrow
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Mujeokja
 | mr             = Muchŏkcha}}
| director       = Song Hae-sung
| producer       = Kim Sang-keun Kim Jung-hwan
| story          = Jo Chang-ho
| screenplay     = {{Plainlist |
* Kim Hyo-seok
* Lee Taek-gyeong
* Choi Geun-mo
* Kim Hae-gon}}
| starring       = {{Plainlist |
* Joo Jin-mo
* Song Seung-heon
* Kim Kang-woo
* Jo Han-sun}}
| music          = Lee Jae-jin
| cinematography = Kang Seung-ki
| editing        = Park Gok-ji
| studio         = Fingerprint Pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 124 minutes
| country        = South Korea
| language       = Korean,English
| budget         = 
| gross          =   
}}
A Better Tomorrow ( ; lit. "Invincible") is a 2010 South Korean contemporary gangster drama film, starring Joo Jin-mo, Song Seung-heon, Kim Kang-woo and Jo Han-sun.   It is an official remake of the 1986 Hong Kong film A Better Tomorrow. It was directed by Song Hae-sung and produced by Fingerprint Pictures. John Woo, who directed the original 1986 version, acted as executive producer.

The film premiered as part of Special Events at the 67th Venice International Film Festival on September 2, 2010, where it was introduced by John Woo, as having "its own character and own soul, and many new elements."  It was released in theaters on September 16, 2010. 

==Cast==
* Joo Jin-mo – Kim Hyuk
* Song Seung-heon – Lee Young-choon 
* Kim Kang-woo – Kim Chul
* Jo Han-sun – Jung Tae-min 
* Lee Geung-young – Park Kyung-wi Kim Ji-young – aunt
* Kim Hae-gon – Boss Jung
* Lim Hyung-joon – Detective Lee
* Seo Tae-hwa – Public Prosecutor Jo
* Jeong Gi-seop – Detective Park
* Moon Kyung-min
* Lee Sin-seong

==Plot== National Police arms smuggler with his best friend and partner in crime, Lee Young-choon (Song Seung-heon), who also defected from the North.

Hyuk has a younger brother, Chul (Kim Kang-woo), whom he was forced to leave behind (along with their mother) during his escape. Guilt-ridden over leaving his brother behind, Hyuk has spent the past few years searching for his brother. Eventually, he finds Chul in an internment camp but Chul resents Hyuk for leaving behind the family to escape. It is then revealed that their mother was killed sometime after Hyuks escape.

Hyuk goes to Thailand to complete an arms deal, accompanied by Jung Tae-min (Jo Han-sun), a new member of the smuggling operation. However they were Double cross (betrayal)|double-crossed by Jung and the Thai gang. Jung escapes, while Hyuk is captured and sentenced to prison for three years. After reading about Hyuks capture in the newspaper, Lee finds the Thai gangster in a massage parlor and kills him and his henchmen. However, in the ensuing gunfight, he is shot in the knee and crippled.

After Hyuk is released from custody. Remorseful and determined to start a new life, he finds work as a taxi driver. Meanwhile, Chul has become an officer in the National Police and Jung has become the leader of the arms smuggling operation, while Lee does odd jobs to survive. During an emotional reunion, Lee asks Hyuk to return to the underworld to take revenge on Jung, but Hyuk refuses.

Hyuk seeks Chul out, hoping for a reconciliation, but Chul rebuffs him, seeing Hyuk as nothing but a criminal and still resentful that Hyuk left the family in North Korea. Jung finds Hyuk and presses him to rejoin the organization, offering to bring Young-choon along if Hyuk rejoins, but Hyuk refuses. Meanwhile, Chul is obsessed with arresting Jung and bringing down the arms operation. After Jung has Young-choon beaten and threatens to harm Chul, Hyuk decides to join Young-choon in taking revenge on Jung. Hyuk and Young-choon steal incriminating evidence from the smuggling business and use it to ransom Jung in exchange for money and an escape boat. However, Hyuk has given the evidence to the police. Using Jung as a hostage, Hyuk and Young-choon take the money to a pier, intending to escape in the boat. Meanwhile, having followed his brother, Chul arrives on the scene but is captured by Jungs men. Even though he is free to escape, Hyuk decides to return to save Chul and asks Young-choon to leave on his own.

Hyuk returns and offers to exchange Jung for Chul, but the trade explodes into a wild shootout. Hyuk and Chul are wounded and pinned down, but saved by Young-choon, who turned the boat around out of loyalty to Hyuk. After killing many of Jungs men, Young-choon berates Chul, telling him that he should be grateful to have a brother like Hyuk. Young-choon is in turn gunned down by Jungs men. The police arrive and begin arresting Jungs men.  Jung evades capture and escapes into a steelyard.  Hyuk and Chul chase after him, but Hyuk is shot and killed when he shields Chul from Jungs gunfire. Jung mocks Chul and prepares to surrender to the surrounding police.  Despite warnings from the police to drop his weapon, Chul shoots and kills Jung. As the police advance, Chul cradles his brothers body in his arms and tearfully laments that he missed him. He aims his gun to his head and the scene cuts to black as a single gunshot is heard.

==Differences between remake and original==
* The protagonists are arms traffickers as opposed to counterfeiters.
* Kim Hyuk is a police officer who moonlights as an illegal arms trafficker, whereas his counterpart in the original, Sung Chi-ho was not involved in law enforcement.
* Chul is hostile and resentful towards Hyuk for his perceived abandonment as a teenager whereas Ho and Kit have a close fraternal relationship until Hos arrest.
* Young-choon is shown to be suspicious of Jungs motives, and Hyuk witnesses Jungs betrayal during the deal.  In the original, Shings duplicity is not revealed until much later in the film.
* Unlike Kit, Chul does not have a girlfriend, hence no female roles.
* It is implied that Young-choon works for himself (doing odd jobs) after being crippled whereas Mark is shown to work for Shing in the original.
* Young-choon is shot and killed by a multitude of Jungs men whereas Mark was killed by Shing (and his right-hand man) directly.
* Hyuk and Chul die in the remake whereas Ho and Kit both survive the final showdown and successfully reconcile in the original.

==Reception==
In Korea, the film ranked second and grossed over   in its first week of release,  and grossed a total of   after six weeks of screening.  The film sold a total of 1,546,420 tickets nationwide.  In Japan it ranked #11 and grossed over   in its one week of release on 103 screens. 

 . Retrieved 2012-11-24.  

==Remake Anthology==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
|   (1994) (Bollywood|Hindi) || A Better Tomorrow (2010) (Korean cinema|Korean) 
|-
| John Woo || Sanjay Dutt || Joo Jin-mo
|- Kim Ji-yung
|}

==References==
 

== External links ==
*    
*   at Naver  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 