Frankie and Johnny Are Married
{{Infobox Film
| name           = Frankie and Johnny Are Married
| image          = Frankie_and_Johnny_Are_Married.jpg
| image_size     = 
| caption        = 
| director       = Michael Pressman
| producer       = Alice West
| writer         = Michael Pressman
| starring       = Michael Pressman Lisa Chess Alan Rosenberg Stephen Tobolowsky Jillian Armenante
| music          = Don Peake
| cinematography = Jacek Laskus
| editing        = Jeff Freeman Michael Rafferty
| distributor    = IFC Films   Screen Media Films  
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $22,900 
}}
Frankie and Johnny Are Married is a 2003 comedy film written and directed by Michael Pressman, starring Pressman as well as Lisa Chess and Alan Rosenberg. The film chronicles the troubles a producer has trying to mount a production of the Terrence McNally play, Frankie and Johnny in the Clair de Lune. The production is beset by one problem after another, including a hard to handle male lead (Rosenberg). This eventually leads Pressman to take on the male lead role himself.

==Cast==
* Michael Pressman as Michael Pressman
* Lisa Chess as Lisa Chess
* Alan Rosenberg as Alan Rosenberg
* Stephen Tobolowsky as Murray Mintz
* Jillian Armenante as Cynthia
* Morgan Nagler as Sally
* Maury Sterling as Roger
* Hector Elizondo as Hector Elizondo

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 