Club 60
{{Infobox film
| name           = Club 60
| image          = Club_60_Movie_Poster.jpg
| caption        =
| director       = Sanjay Tripathy
| producer       = Kavee Kumar
| writer         = Sanjay Tripathy
| starring       = Farooq Sheikh  Sarika  Satish Shah  Raghubir Yadav
| music          = Pranit Gedham
| cinematography = Shyamanand Jha
| editing        = Arunabha Mukherjee
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = Rs. 60 million  {{cite news|url= http://www.hindustantimes.com/entertainment/bollywood/farooque-sheikh-s-club-60-completes-100-days-of-run/article1-1200114.aspx|title= Farooque Sheikhs Club 60 completes 100 days of run
|publisher=Hindustan Times|date=25 March 2014}} 
| gross          = 
}}
Club 60 is a 2013 Bollywood film produced by Kavee Kumar under the Pulse Media banner and directed by Sanjay Tripathy.      The film features Farooq Sheikh, Sarika, Raghubir Yadav and  Satish Shah in key roles.  The film was Farooq Sheikh’s last release before his death on 27 December 2013.     

==Plot==
After the untimely death of their only son,Iqbaal(Ankit Bathla), neurosurgeon, Dr Tariq Sheikh (Farooq Sheikh) and his wife Dr Saira (Sarika) are unable to put the pieces of their broken life together. Tariq suffers from depression, while Saira struggles to cope with her husbands suicidal tendencies. In an attempt to make a fresh start in life and to get rid of the nothingness that haunts them, they shift to Mumbai from Pune. More than the citys distractions, its their loud neighbor Manubhai (Raghubir Yadav), who manages to kill the deafening silence that plagues them with his somewhat annoying yet adorable antics. Manubhai introduces Tariq to the jovial members of Club 60 - where life begins at 60.     

==Cast==
* Farooq Shaikh  ...  Dr. Tariq Shaikh 
* Sarika  ...  Dr. Saira  
* Ankit Bathla  ...  Iqbaal
* Raghubir Yadav  ...  Manu Bhai  
* Satish Shah  ...  Mansukhani  
* Tinnu Anand  ...  Jaffar Bhai  
* Sharat Saxena  ...  G.S. Dhillon  
* Vineet Kumar  ...  Sinha  
* Suhasini Mulay  ...  Mrs. Mansukhani
* Viju Khote  
* Zarina Wahab  ...  Mrs. Sinha  
*Himani Shivpuri  ...  Nalini Doctor  
* Harsh Chhaya  ...  Doctor  
* Mona Wasu  ...  Dolly, the lady at night club  
* Geeta Bisht  ...  Rosetta, nurse  
* Shahabb Khan  ...  Clerck  
* Mansi Sadana  ...  Junior Doctor  
* Jaanvi Sangwan  ...  Maya

==Reception==
The movie was generally appreciated  for its script, casting, performances by the lead actors (Farooq Sheikh and Sarika) editing and the music. At the same time, the movie was found wanting in certain aspects such as the background score, screenplay and was also criticized for some of the jokes with heavy sexual innuendo, a predictable second half and a clumsy and overlong climax. Overall, the movie received an average rating of 2.58/5 from critics.           
       

==Box office collections==
As per Bollywood Hungama website, the life time collections of the film was Rs. 42 lakhs.   The movie completed a run of 100 days in cinemas in April 2014. The director credited   word of mouth publicity.  

==Re-release==
PVR Cinemas decided to re-release Club 60 in cinemas again in January 2014 as a tribute to Farooq Sheikh .  

==References==
 

==External links==
*  

 
 
 