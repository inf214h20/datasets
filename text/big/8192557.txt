The Arm Behind the Army
{{Infobox film
| name = The Arm Behind the Army
| distributor = U.S. Army Signal Corps
| released = 1942
| runtime = 10 minutes
| country = United States
| language = English
}}
The Arm Behind the Army is a propaganda film produced by the US Army Signal Corps in 1942 to encourage the home front to participate in war production.

The film begins with a short outline of American military history, noting that each war has advanced military technology a little further, from the muskets of 1776, to the tanks and airplanes of the  s fist, the narrator notes, and behind it is American labor "Uncle Sams muscle" the arm behind the army.

The narrator notes "Behind the desks, behind the drawing board, behind the benches, on the assembly lines, American industry is making the greatest production effort in history to supply our armed forces with the weapons of war."
 Czech management and labor led to the ruin of both, and how French factories were left idle while France fell. It noted the terrible working conditions in Axis powers of World War II|Axis-occupied territory, the coerced labor, the ending of old-age benefits, unions and "all the advances that labor every made." The film ends with a picture of a soldier and a picture of an industrial worker superimposed on a battlefield, noting that wherever the soldier is, the worker is there too.

==See also==
* List of American films of 1942
* List of Allied Propaganda Films of World War 2
* United States home front during World War II

==External links==
* 
* 

 
 
 
 
 
 


 