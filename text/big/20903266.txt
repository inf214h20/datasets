Bundal Baaz
{{Infobox film
| name           = Bundal Baaz
| image          = 
| image_size     = 
| caption        = 
| director       = Shammi Kapoor
| producer       = 
| writer         =
| narrator       = 
| starring       =  Rajesh Khanna, Shammi Kapoor, Sulakshana Pandit
| music          = R D Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1976 Bollywood fantasy and romantic comedy film directed by Shammi Kapoor. The film stars Rajesh Khanna in the lead role and Shammi Kapoor in the supporting role.

==Plot==
Daydreamer Rajaram lives a poor lifestyle in a village in India along with his aunt and an unmarried sister, Malti, who has mortgaged their shanty house with Sukhi Lala so that she can educate Rajaram in Bombay. Rajaram pretends to be wealthy in order to get married to his sweetheart, Nisha Sharma, much to the chagrin of her dad who wants her to get married to a boxer, Ranjeet Gupta. 

Story opens at college, where an impoverished student named Raja (Rajesh Khanna) dreams of making it big and winning his true love Nisha (Sulakshana Pandit).His rival for her affections, Ranjit (Ranjeet) is the class bully and a cheat; but Ranjit’s father and Nisha’s father are close friends and want their children to get married when they graduate.Raja’s roommate and best friend Gopal (Asrani)—also poor—knows the truth of his circumstances, but Raja has fooled everyone else into believing he has a rich father.After Rajaram gets a degree in Arts he returns home and finds out about the debt, he attempts to find work, ends up working as a laborer with Sukhi, but is beaten-up when he is found slacking. Rajaram returns to Bombay, meets with Suleiman, and takes up collecting waste material (Dabha Batli) from households for a fee. When he returns to his village, he discovers that his sister Malti (Farida Jalal) and his aunt (Lalita Pawar) are being harassed by the local moneylender to repay the money lent to them for Raja’s education, or he will force Malti to marry him. Raja is overwhelmed at their sacrifices.He returns to Bombay to find work so he can send money home, and is befriended by Sulaiman (Johnny Walker), who buys bottles and boxes for recycling, and teaches Raja the trade. Raja begins earning money and sends some home to his village. He also meets Gopal again, but doesn’t contact Nisha since he is too poor to court her. When he does run into her, she is going into a New Year’s Eve masquerade party, and thinks that his bottle vendor outfit is his costume. She drags him in with her, where he is tormented again by Ranjit, and further realizes the gulf between himself and Nisha.He slips out and returns home, sad and heartbroken. One day he comes across a sack of bottles, throws it against the door; one broken bottle rolls outside where a big puff of pink smoke materializes from it.  out pops a 20000 year old Genie with magical powers. He takes his sack of bottles and As the smoke clears a genie (Shammi Kapoor) appears.he genie was imprisoned in the bottle 20,000 years ago by an uncle fed up with his naughtiness. He has sworn an oath to serve his rescuer for a full year, and is going to fulfill this oath whether Raja likes it or not. Chaos ensues!No matter how hard he tries, the genie causes problems for poor Raja. Rajaram does not want anything to do with this Genie, who insists on helping him for one year regardless. With his help Rajaram and Suleiman improve their lifestyle, albeit with a lot of problems, and also improve Rajarams chances of getting married to Nisha. Sulaiman tells the genie about Nisha, and he tries to help bring them together, but Nisha’s father is determined to get her married to Ranjit. Raja’s aunt sends Malti to him from the village to get her out of the moneylender’s way. He marries her off to Gopal.Meanwhile, Nisha and her father fight about Ranjit. Nisha wants to marry Raja no matter how poor he is. When Ranjit and some of his friends try to run down Raja (literally) he is rescued by the genie in a flying car.Pleased with himself, the genie gives Raja a ring to protect him from Ranjit.
he ring, when twisted on the finger, calls up a demon from the underworld who spouts fire and chants “Zum Zum Bah!” I find him much more funny than scary. Raja can’t handle one genie, let alone two, and he throws the ring away. Ranjit, watching, retrieves it after they go.Ranjit owes money to a bunch of goondas, who come to get their money. His plan is to marry Nisha and then repay them. He enlists the help of the demon by asking him to change him to look like Raja, and vice versa.Nisha isn’t fooled, since Ranjit hasn’t changed his voice or his manners. Raja and the genie rescue her when he assaults her. They celebrate by flying with the genie over Bombay.He transports them to a magical place where they sing another pretty song. I’m reminded of Kashmir Ki Kali with a genie thrown in.By now Nisha’s father has discovered what a scoundrel Ranjit is, and told Nisha that she can marry Raja. So Ranjit will have to resort to drastic measures! He and the demon go to Nisha’s house and kidnap her and her father.They are taken away to the underworld.The genie and Raja follow, although the genie’s power doesn’t work in the underworld. Can they overcome the demon without special powers? Will they rescue Nisha and Sharmaji? What will happen to Ranjit?

==Cast==
*Rajesh Khanna ...  Rajaram Goku Raja 
*Shammi Kapoor ...  Genie 
*Sulakshana Pandit ...  Nisha Sharma 
*Farida Jalal ...  Malti 
*Mukri ...  Gupta  Johnny Walker ...  Suleiman M.A. 
*Ranjeet ...  Ranjeet Gupta 
*Lalita Pawar ...  Rajarams Mausi  Sulochana   
*V. Gopal ...  Havaldar 
*Kamal Kapoor ...  Sukhi Lala 
*Viju Khote ...  Business / Ranjeets Creditor 
*Dev Kumar ...  Jumjumbo 
*Moolchand ...  Sindhi businessman / Ranjeets Creditor (as Mulchand) 
*Tun Tun ...  Rajarams customer

==Soundtrack==
{|class="wikitable"
|-
!Song !!Singers !!Time
|-
|"Ruk Meri Jaan" Kishore Kumar, Manna Dey
|4:12
|-
|"Bemausam Bahaar Ke"
| Kishore Kumar, Lata Mangeshkar
|6:32
|-
|"Kya Hua Yaaron" Kishore Kumar, Asha Bhosle
|6:28
|-
|"Nagma Hamara" Lata Mangeshkar, Mohd. Rafi
|4:38
|-
|}

==External links==
*  

 
 
 
 