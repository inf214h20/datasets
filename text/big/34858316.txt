Strul
{{Infobox film
| name           = Strul
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jonas Frick
| producer       = Waldemar Bergendahl Jan Marnell
| screenplay     = Bengt Palmers Björn Skifs
| based on       =  
| narrator       = 
| starring       = Björn Skifs Gunnel Fred Johan Ulveson Stefan Sauk
| music          = 
| cinematography = Stefan Kullänger
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 101 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Strul is a 1988 Swedish film directed by Jonas Frick.

==Cast==
* Björn Skifs as Conny Rundqvist
* Gunnel Fred as Susanne Lindh Magnus Nilsson as Gränges
* Gino Samil as Hjelm
* Johan Ulveson as Pege
* Mikael Druker as Norinder
* Kåre Sigurdson as Sörman
* Stefan Sauk as Alf Brinke
* Hans Rosenfeldt as Glenn
* Peter Palmér as Hans Ekelund

==External links==
*  
*  

 
 
 