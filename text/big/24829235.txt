Desperate for Love
{{Infobox film
| name           = Desperate for Love
| image          = Desperate for Love DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Michael Tuchner
| producer       = Steven R. McGlothen
| writer         = Judith Paige Mitchell
| narrator       = 
| starring       = Christian Slater Tammy Lauren Brian Bloom Charles Bernstein
| cinematography = William Wages
| editing        = Skip Schoolnik
| studio         =
| distributor    = CBS
| released       = January 17, 1989
| runtime        = 93 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Desperate for Love is a 1989 television film directed by Michael Tuchner. The film is based on a true story. 

==Plot==
Alex Cutler and Cliff Petrie are 17-year-old teens who have been close friends since they were young. Alex is the most popular guy in school, with a promising future, while Cliff is an introverted teen who has never had a girlfriend. Lily Becker, an attractive cheerleader who is very popular despite the fact that she is known for being promiscuous, has been dating Alex for a while and is planning to marry him after graduation. Alexs father, however, feels that he should go to university and points out to him that Lily comes from a different environment. Moreover, he and Lilys father, the towns notorious low life hunter, are sworn enemies. Always doing what his father tells him to, he breaks off his relationship with Lily, which shocks and upsets her.
 trash and that men desire her, until she gives in. Knowing that he thinks very highly of her, Lily seduces Cliff and even plans on marrying him. When Alex finds out, he responds furiously, immediately ending his friendship with Cliff. He soon regrets having broken up with her and they soon reconcile. When Lily breaks the news to Cliff that she and Alex are a couple again, he is crushed.

One month later, Alex and Cliff befriend each other again. Alex promised Lily to elope with her, but in the end, he is too afraid to commit himself to her and she is eventually stood up. Crushed, she returns home in tears, which angers her father, who already wasnt fond of Alex. In a mad rage, he grabs his rifle and goes into the woods. Meanwhile, Alex and Cliff are hunting in the same woods as well. The next day, Alex is reported to be missing. Crushed, Lily again finds comfort with Cliff, who she thinks killed Alex out of love for her. Always wanting to be loved by someone, she feels attracted to him, rather than being angry at him. However, she soon gives him in to the police, with the claim that he murdered Alex. After Alexs funeral, Cliff is arrested by the police.

Two years later, the murder trial against Cliff is fully in progress. Lily claims that Cliff admitted to her that he killed Alex out of love for her. Cliffs attorney interrogates her, trying to prove that she is a promiscuous and untrustworthy girl. He claims that Cliff could not kill someone out of love for Lily, because, according to him, there was no love between them. In an emotional testimony, Lily admits that she was not in love with Cliff, but that he loved her very much. Cliff feels that he no longer can co-operate with the trial and is voluntarily found guilty for murdering Alex. He is sentenced to jail for eight years, but is released on parole after four years, in October 1988.

==Cast==
*Christian Slater as Cliff Petrie
*Tammy Lauren as Lily Becker
*Brian Bloom as Alex Cutler
*Veronica Cartwright as Betty Petrie
*Scott Paulin as Lilys father
*Amy ONeill as Cindy

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 