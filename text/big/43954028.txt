Vellimoonga
{{Infobox film
| name           = Vellimoonga 250px
| caption        = Film poster
| director       = Jibu Jacob
| producer       = Shashidharan Ullattil
| writer         = Joji Thomas
| narrator       = Lal Jose
| starring       = Biju Menon Aju Varghese Nikki Galrani Asif Ali K. P. A. C. Lalitha Siddique
| music          = Bijibal
| cinematography = Vishnu Narayan
| editing        = E. S. Sooraj
| country        = India
| released       =  
| language       = Malayalam
| runtime        = 128 minutes
| distributor    = Ullattil Visual Media Release & Tricolor Entertainments
| budget         =   
| gross          =   
}}
 romantic comedy/political Siddique in other pivotal roles. Asif Ali appears in an extended cameo role in the film. The film released on September 25, 2014 in 60 centres across Kerala. It received positive reviews upon release and was a sleeper hit at the box office and went on to become the second highest grossing Malayalam film of the year, with a box office collection estimated at  .

==Plot summary==
Mamachan (Biju Menon), is a 41 year old man who is still unmarried because he had to take responsibility of his family due to his fathers early demise. His father was a politician, but could not make an earning out of it. Once Mamachan went to a police station to meet some people who are known to him, but the police mistakes him for a politician since he was wearing Khadar(Khadi attire). Mamachan understands the potential of the Khadar and becomes a politician.

Instead of joining existing political parties who has a strong foothold in Kerala, he joins a national party who hardly has any workers in Kerala and thus becomes the national leader. Later, this party makes an alliance with the main political party in Kerala and thus Mamachan becomes a candidate for them in the next election. Mamachans chances of winning are pale because he is considered to be shrewd. He has a lot of enemies in his village because of his nature. During this time, Mamachan falls in love with a girl he meets in the church, but it turns out that she is the daughter of a girl Mamachan used to like in his child hood. Because of this, his lovers father does not like Mamachan and is against the marriage. But eventually Mamachan uses his shrewd tactics to win the election and the girl he loved as well.

==Cast==
* Biju Menon as Mamachan
* Nikki Galrani as Lisa
* Aju Varghese as Paachan
* Tini Tom as VP Jose
* Asif Ali as Charlie/Josootty Pala Lena as Moly Siddique as Wareed
* K. P. A. C. Lalitha
* Sunil Sukhada as Father
* Kalabhavan Shajon as Gopi
* Sasi Kalinga
* Veena Nair as Panchayat President 
* Saju Navodaya as Mathew
* Chembil Ashokan
* Shivaji Guruvayoor
* Anu Joseph

==Reception==
Vellimoonga received positive reviews upon release.    Nowrunning.com gave 2.5/5 and called it a  "surprise winner". 

Sify reported that Vellimoonga had a good opening and called the film an "instant hit".  The film was a sleeper hit at the box office and went on to become the second highest grossing Malayalam film of the year (behind Bangalore Days), with a box office collection estimated at  .    It also earned   from satellite rights. 

==Soundtrack==
{{Infobox album
| Name = Vellimoonga
| Artist = Bijibal
| Type = Soundtrack
| caption =
| Cover =
| Released =  
| Recorded = 2014
| Genre = Film soundtrack
| Length = 
| Language = Malayalam
| Label = Manorama Music
| Producer = Bijibal
| Last album = 
| This album = 
| Next album = 
}}

{| class="wikitable"
|-
! Track !! Song Title !! Lyricist !! Singer(s)
|-
| 1 || Vellimoonga || Santhosh Varma || Lola, Daya, Thamanna, Swathi, Dev
|-
| 2 || Punchiri Kannulla || Rajeev Nair || Ganesh Sundaram
|-
| 3 || Mavelikku Shesham || Santhosh Varma || Najim Arshad
|-
| 4 || Punchiri Kannulla || Rajeev Nair || Vijay Yesudas
|}

==References==
 

 
 
 