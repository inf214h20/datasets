Gajaraja Manthram
{{Infobox film
| name = Gajaraja Manthram
| image =
| image_size =
| caption =
| director = Thaha
| producer =
| writer = Kaloor Dennis
| screenplay = Kaloor Dennis
| starring = Kalabhavan Mani Prem Kumar Charmila Jagadish
| music = Berny-Ignatius
| cinematography = Vijaya Kumar
| editing = G Murali
| studio = Cityson Films
| distributor = Cityson Films
| released =  
| country = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, directed by Thaha. The film stars Kalabhavan Mani, Prem Kumar, Charmila and Jagadish in lead roles. The film had musical score by Berny-Ignatius.   
 
==Cast==
  
*Kalabhavan Mani 
*Prem Kumar 
*Charmila 
*Jagadish 
*Machan Varghese 
 

==Soundtrack==
The music was composed by Berny-Ignatius. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Indukale || K. J. Yesudas || Gireesh Puthenchery || 
|- 
| 2 || Manjil mungum maamarathil || K. J. Yesudas, Daleema || Gireesh Puthenchery || 
|- 
| 3 || Melekkaavil || M. G. Sreekumar || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 