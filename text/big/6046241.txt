How Sweet It Is!
 
{{Infobox film name          = How Sweet It Is! director      = Jerry Paris
| image	=	How Sweet It Is! FilmPoster.jpeg
| image_size     = 225px producer      = Jerry Belson Garry Marshall writer        = Jerry Belson Garry Marshall based_on      = The Girl in the Turquoise Bikini by Muriel Resnik starring      = James Garner Debbie Reynolds Maurice Ronet Penny Marshall music  Pat Williams
|cinematography= Lucien Ballard editing       = Bud Molin studio        = Cherokee Productions distributor   = National General Pictures released      = August 1968    runtime       = 99 minutes country       = United States language      = English
| gross = $2,700,000 (US/ Canada) 
}}
How Sweet It Is! is a 1968 comedy movie starring James Garner and Debbie Reynolds, with a supporting cast including Terry-Thomas and Paul Lynde.
 Patrick Williams scored the sound track.

==Cast==
*James Garner as Grif
*Debbie Reynolds as Jenny
*Maurice Ronet as Phillipe
*Alexandra Hay as Gloria
*Terry-Thomas as Gilbert
*Paul Lynde as the Purser
*Donald Losby as Davey
*Hilary Thompson as "Bootsie"
*Marcel Dalio as Louis
*Gino Conforti as Agatzi
*Vito Scotti as the chef
*Don Diamond as the bartender
*Penny Marshall as school girl
*Erin Moran as little girl

==Production and release==
How Sweet It Is! was the first production for National General Pictures.  The title is taken from a television catchphrase popularized in the 1960s by comedian Jackie Gleason.   

Upon its release in August 1968,  the film received a mixed response with critics and audiences.    According to Howard Thompson of The New York Times, "This tired, aimlessly frisky comedy ... is about as sweet as a dill pickle." 

==See also==
*List of American films of 1968

==References==
 

==External links==
*  
*  
*  
*   at Archive of American Television

 

 
 
 
 
 
 
 


 