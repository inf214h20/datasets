Trouble Brewing (1924 film)
{{Infobox film
| name           = Trouble Brewing
| image          =
| caption        =
| director       = James D. Davis Larry Semon
| producer       =
| writer         = James D. Davis Larry Semon
| starring       = Larry Semon Carmelita Geraghty Oliver Hardy
| cinematography =
| editing        =
| studio         = Larry Semon Productions
| distributor    = Vitagraph Studios
| released       =  
| runtime        =
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 silent comedy film featuring Larry Semon, Carmelita Geraghty and Oliver Hardy. It is believed to be Lost film|lost. 

==Cast==
* Larry Semon - Government agent
* Carmelita Geraghty - The Girl
* Oliver Hardy - Bootlegger (as Babe Hardy)
* William Hauber
* Al Thompson
* Pete Gordon

==See also==
* List of American films of 1924
* Oliver Hardy filmography
*List of lost films

==References==
 

==External links==
* 
* 


 
 
 
 
 
 
 
 
 
 
 