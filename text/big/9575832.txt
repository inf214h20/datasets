Conrack
{{Infobox film name           = Conrack image          = conrack1974.jpg caption        = 1974 Promotional Poster for "Conrack" director       = Martin Ritt writer         = Pat Conroy (The Water Is Wide (book)|novel) Harriet Frank, Jr. Irving Ravetch narrator       =  starring       = Jon Voight Paul Winfield Madge Sinclair Antonio Fargas Hume Cronyn music          = John Williams cinematography = John A. Alonzo editing        = Frank Bracht distributor    = 20th Century Fox released       =   runtime        = 106 minutes country        = United States language  English
|budget         = $2.37 million  gross          = $2 million (US/ Canada) 
}}
  1974 film autobiographical book The Water 20th Century Fox on March 27, 1974.

== Synopsis == Principal (Madge Sinclair) teaching grades one through four and Conroy teaching the higher grades. Conroy discovers that the students arent taught much and will have little hope of making a life in the larger world.
 Beaufort on the mainland to go trick-or-treating, which the superintendent has forbidden. He also must overcome parental fears of "the river."  As a result, hes fired.  As he leaves the island for the last time, the children come out to see him leave, bringing along a record player on which they play the beginning movement of Beethovens Fifth Symphony as he leaves.

This film was shot in and around Brunswick, Georgia and used pupils from C.B. Greer Elementary school as the cast of students. 

== Critical reception ==
The film received a primarily positive response upon its release and currently has a "Fresh" score of 88% on reviewing site, Rotten Tomatoes. It has never been released on DVD though it remains available through other means, such as VHS as well as online streaming.  Twilight Time has announced a Blu-ray release of this title on March 10, 2014.  

== Cast ==
* Jon Voight as Pat Conroy
* Paul Winfield as Mad Billy
* Madge Sinclair as Mrs. Scott (school principal)
* Tina Andrews as Mary
* Antonio Fargas as Quickfellow
* Ruth Attaway as Edna
* James ORear as Postman
* Gracia Lee as Mrs. Sellers
* C.P. MacDonald as Mr. Ryder
* Jane Moreland as Mrs. Webster Thomas Horton as Judge
* Nancy Butler as Mrs. Ryder
* Robert W. Page as Mr. Spaulding
* Hume Cronyn as Mr. Skeffington (school superintendent)
* Mac Arthur Nelson as Mac
* Kathyrn Turner as Katt

==See also==
*White savior narrative in film
*Daufuskie Island
*Sea Islands

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 