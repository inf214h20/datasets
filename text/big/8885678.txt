G:MT – Greenwich Mean Time
{{Infobox Film
| name           = G:MT — Greenwich Mean Time
| image          = Gmtgreenwichmeantimefilm.jpg
| caption        = Promotional poster
| director       = John Strickland
| producer       = Taylor Hackford
| writer         = Simon Mirren
| starring       = Steve John Shepherd   Ben Waters   Alec Newman Chiwetel Ejiofor
| music          = Guy Sigsworth
| cinematography = Alan Almond
| editing        = Patrick Moore
| distributor    = Icon Film Distribution (UK)
| released       = 1 October 1999 (UK premiere)
| runtime        = 117 minutes
| language       = English
| budget         = 
}} British drama film.
 Jungle artists including Talvin Singh, Hinda Hicks and Imogen Heap. It was also one of the last projects of the late jazz trumpeter Lester Bowie.

==Synopsis==
Set against a backdrop of 20th century fin-de-siecle London, it focuses on a multi-racial group of South London youths who form a band called Greenwich Mean Time. Four years after college graduation, they all work out what direction their lives are headed, including girlfriend problems, an ill-fated venture into drug dealing, and sleazy record producers. As the film progresses, the narrative inches its protagonists toward a sudden bloody finale.

==Reception==
The film was not well received by critics,  with writer Mirren suggesting some of the response was directed at the fact he is Helen Mirrens nephew (and the producer was Helen Mirrens partner), and so he was being given a helping hand as a result.  Parallels with the 1996 film Trainspotting (film)|Trainspotting were also noted, in an unfavourable context. 

A limited UK release meant that the film did better business elsewhere, but it was only released on DVD in the United States, the Netherlands and Germany. Several of the films actors have gone on to become well-known, and the soundtrack to the film was well received.

==References==
 

==External links==
*  

 
 
 
 