The Busy Little Engine
{{Infobox film
| name           = The Busy Little Engine
| image          = BusyLittleEngineDVD.jpg
| image_size     =
| caption        = DVD cover image
| director       = Desmond Mullen
| producer       = Desmond Mullen Helena Mullen
| writer         = Desmond Mullen
| narrator       = Desmond Mullen
| starring       = Desmond Mullen Lorrie Guess
| music          = Jimmy Magoo
| cinematography = Desmond Mullen
| editing        = Desmond Mullen
| distributor    = Squirrel Tracks
| released       =  
| runtime        = 34 minutes
| country        = United States
| language       = English
| budget         = $52,000 (est.)
| preceded_by    = 
| followed_by    = 
}} journals Booklist,   at the American Library Association. Retrieved on 2007-03-16.  School Library Journal,   at the School Library Journal. Retrieved on 2007-03-16.  and Video Librarian.  . Retrieved on 2007-03-16.  The Busy Little Engine was picked Best DVD by Parenting Magazine in July 2006.  . Retrieved on 2007-03-16. 

==Plot==
The preschool-age DVD tells the story of a wooden toy train who pretends to be a real train. The main character, Busy Little Engine, appears alternately as a wooden toy train in a playroom and as a full-scale-size train in real-world backgrounds. With its intentionally gentle pacing and static camera work, it has been called "a young childs picture book come-to-life". 

Busy Little Engine pretends to be a real train but does not actually know what real trains do. This presents a problem which is soon solved with the help of Busy Little Engines puppet friend, Pig, and the off-screen narrator.

Through the course of the show, Busy Little Engine and Pig explore the everyday world using role-playing and imagination. Viewers learn about tangible topics such as raw goods, finished materials and basic railroad operations along with esoteric topics such as pretending, taking turns, and learning from others.

==Cast==
* Desmond Mullen as Busy Little Engine and the Narrator
* Lorrie Guess as the Prologue

==Reception==
The Busy Little Engine was selected for the 2006 San Diego International Childrens Film Festival.  It was picked Best DVD by Parenting Magazine in July 2006. 

Inspired, in part, by Richard Scarrys book, What do People do All Day? and other childrens picture books, the DVD uses static framing to its advantage. DVD Verdicts review   at DVD Verdict. Retrieved on 2007-03-16..  said:

 
Yet the three kids Ive shown it to have been rooted to the screen. Creator   shorthand are not natural. We have to learn what they mean. Kids dont intuitively understand that a jump cut means something. Pigs straightforward manner and The Busy Little Engines static framing mimic the way a child interprets the world. The proof is self evident: Kids dig this DVD.
 

==References==
 

== External links ==
*  
*  

 
 
 
 
 