Scorching Sun, Fierce Winds, Wild Fire
Scorching kung fu film starring Angela Mao, Dorian Tan, Chang Yi, and Lo Lieh.

==Plot==
Scorching Sun, Fierce Wind, Wild Fire is a story about a search for the second half of a treasure map, but its conveniently forgotten at various points. Angela Mao plays the daughter of a warlord and has a secret identity as the masked freedom fighter Violet, who rides the country righting wrongs and organizing rebels. Tien Peng plays a mysterious stranger who comes to town looking for the other half of the map. Lo Lieh and Tan Tao Liang play prison inmates who escape and end up actually being ex-comrades with Tien Ping and assisting he and Violet in apprehending Master Wu (Chang Yi), the security chief who does most of the fighting for the warlord. Master Wu turns traitor in order to get the map.

==Cast==
* Angela Mao as Violet
* Dorian Tan as Escaped Convict #1
* Lo Lieh as Escaped Convict #2
* Chang Yi as Master Wu

==Music==
In the American dubbed version, during the opening scene as well as throughout the entire film, John Williamss score from Star Wars can be heard.

==External links==
*  
*  

 
 
 
 


 
 