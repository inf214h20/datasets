Muthu (1995 film)
{{Infobox film
| name = Muthu
| image = Muthu.jpg
| director = K.S. Ravikumar
| caption =
| writer = K.S. Ravikumar
| story = Priyadarshan
| producer = Rajam Balachander Pushpa Kandaswamy Meena Sarath Babu
| producer = Rajam Balachander  Pushpa Kandaswamy
| cinematography = Ashok Rajan
| editing = K. Thanikachalam
| released =    
| runtime = 165 minutes
| distributor = Kavithalayaa Productions
| country = India
| language = Tamil
| gross =  13 crore (1995 release)  6 crore (1998 release)
| music = A. R. Rahman
}}
 Tamil musical musical drama film directed by K. S. Ravikumar. The film stars Rajinikanth, Meena Durairaj|Meena, and Sarath Babu. It was the debut film for K. S. Ravikumar with Rajinikanth.  The films score and soundtrack is composed by A. R. Rahman. It is an official remake of the Mohanlal starrer Malayalam film Thenmavin Kombath (1994).    It was the first Indian film to have a major success in Japan and earned Rajnikanth, a huge fan following in Japan.

==Plot==
A kind-hearted Zamindar (Rajinikanth) lived with his sister-in-law and cousin Rajasekhar (Raghuvaran), helping people incessantly. His sister-in-law has a son, to whom the Zamindar bequeaths a major portion of his property. At this juncture, a new baby is born to the Zamindar. His wife dies soon after. Brother Rajasekhar cheats the Zamindar as he fears that his sons property might be taken back and given to the Zamindars own son. When the cheating comes to light, the Zamindar hands over all his property and his baby to his sister-in-law and brother and goes to the Himalayas, making his sister-in-law promise him that the baby should be brought up as a servant, not as a Zamindar. This baby is named Muthu (also Rajinikanth).

According to the promise tendered to the Zamindar, the son of the Zamindars sister-in-law - Ejama becomes a Zamindar(Sarath Babu) and Muthu works as his servant. A huge fan of drama, Ejama regularly takes Muthu along with him wherever he goes. One day Ejama falls in love with a drama actress Ranganayaki (Meena Durairaj|Meena). But Ranganayakis heart lies with Muthu. Raenganayaki shows love and passion to Muthu. Amidst all this, Ejamas Uncle (Radha Ravi), tries to capture all the property by killing Ejama. In the mean time the Zamindar who was in Himalayas returns to see his sister (resembling a Begger Sage), who saves the Ejama and once for all everything about Muthu is revealed to everyone and he marries Ranganayaki, while Ejama marries his uncles daughter who was madly in love with him. The film ends by showing Muthu refusing to be Zamindar and chooses to be a worker pointing "Oruvan oruvan Muthalali" which means God is the real master and we all are his servants.

123

==Cast==
* Rajinikanth as Muthuvel (Muthu) and Zamindar, Muthus father Meena as Ranganayaki
* Raghuvaran as Zamindars cousin, Rajasekhar
* Sarath Babu as Rajasekhars son, Zamindar Malayasimman
* Jayabharathi as Malayasimmans Mother Senthil as Thennappan
* Vadivelu as Valayapathy
* Kanthimathi as Poongavanam
* Radha Ravi as Ambalarathar, Zamindars uncle
* Subhashri as Ambalarathars daughter, Padmini Ponnambalam as Kaali
* Vichithra as Rathidevi
* K. S. Ravikumar as Kerala Village President (Special Appearance)

==Production==
The film was first reported in March 1995, when it was mentioned that K. S. Ravikumar would direct Rajinikanth in a film titled Velan, to be produced by K. Balachander.  The title was later changed to Muthu. The film is characterised by location shots in Kerala and was shot in numerous palaces. The Zamindars palace in which the movie was shot is the Lalitha Mahal Palace in Mysore.

==Reception== National Diet of Japan on 14 December 2006.  

==Soundtrack==
{{Infobox album  
| Name = Muthu
| Type = soundtrack
| Artist = A. R. Rahman
| Cover = Album_muthu_cover.jpg
| Released = 1995 (India) 1998 (Japan)
| Recorded = Panchathan Record Inn
| Genre = Film soundtrack
| Length = Pyramid
| Producer = A.R. Rahman
| Reviews =
| Last album = Rangeela (film)|Rangeela  (1995)
| This album = Muthu (1995) Love Birds (1995)
}}

The soundtrack features six songs composed by A. R. Rahman, with lyrics by Vairamuthu. Muthu is the first Rajinikanth film for which Rahman wrote music. The soundtrack for this movie turned out to be a major hit and Rahman gained popularity in Japan when the movie was released in Japanese. The Hindi version is titled Muthu Maharaja and had lyrics penned by P. K. Mishra. "Omanathinkal Kidavo" portion of "Kuluvalilae" is written and tuned by the famous Malayalam poet Irayimman Thampi.
 sampled African humming in the song; French group Deep Forest had earlier sampled the same in their song Night Bird.   Thillana Thillana was later adapted by Nadeem-Shravan as Deewana Deewana for the 1996 film Jung (1996 film)|Jung. 

This soundtrack was selected as the most popular foreign soundtrack in Japan.  

===Tamil version===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Artist(s)
|-
| 1
| "Kuluvalilae"
| Udit Narayan, K. S. Chithra, Kalyani Menon
|-
| 2
| "Thilana Thilana" Sujatha
|-
| 3
| "Oruvan Oruvan"
| S. P. Balasubrahmanyam
|-
| 4
| "Kokku Saiva Kokku"
| S. P. Balasubrahmanyam, Theni Kunjaramma, Febi Mani, Ganga
|-
| 5
| "Vidu Kathaiya" Hariharan
|-
| 6
| "Theme Music"
| Instrumental
|}

===Telugu version===
The soundtrack features six songs composed by A. R. Rahman, with lyrics Penned by Bhuvanachandra. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Artist(s)
|-
| 1
| "Thilana Thilana" Sujatha
|-
| 2
| "Kalagalile Prema"
| S. P. Balasubrahmanyam, K. S. Chithra, G. V. Prakash
|-
| 3
| "Konga Chitti Konga"
| S. P. Balasubrahmanyam, Ila Arun
|-
| 4
| "Virisinada Vidhi Galam" Hariharan
|-
| 5
| "Okade Okkadu"
| S. P. Balasubrahmanyam
|-
| 6
| "Theme Music"
| Instrumental
|}

===Hindi version===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Artist(s)
|-
| 1
| "Ooperwala Malik Hai"
| S. P. Balasubrahmanyam, Chorus
|-
| 2
| "Phoolwali Ne Loota Mujhko"
| Udit Narayan, K. S. Chithra, G. V. Prakash
|-
| 3
| "Koi Samjhade"
| S. P. Balasubrahmanyam, Ila Arun
|-
| 4
| "Rangeela Rangeela" Sujatha
|-
| 5
| "Chhod Chala Nirmohi" Hariharan
|-
| 6
| "Theme Music"
| Instrumental
|}

==Awards==
;Won
* Rajinikanth - Tamil Nadu State Film Award for Best Actor in 1996

;Nominated
* Rajinikanth - Filmfare Best Actor Award (Tamil) in 1996

==Legacy==
The quote from the film "Naan eppo varuven, epdi varuvennu yarkum theriyathu. Aana vara vendiya nerathile vandidven" (English: Nobody knows when or how I will come, but I will come when the time is right) became popular.  

==In popular culture==
The scenes and songs from the film has been parodied in verious films Unnidathil Ennai Koduthen (1998), Aethiree (2004),  Thiruvanamalai (film)|Thiruvannamalai (2008),. The footage from the film has been used in French film Prete Moi Ta Main (Lend Me Your Hand). 

==References==
 

==External links==
*  

==Bibliography==
* 
* 

 
 
 

 
 
 
 
 
 
 
 