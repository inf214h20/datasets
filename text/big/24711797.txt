Ballad of Siberia
The Ballad of Siberia (in  ) from Mosfilm (1947) is Soviet Unions second color film (after The Stone Flower), directed by Ivan Pyryev, starring Vladimir Druzhnikov and Marina Ladynina.  
 musical movie Soviet style, full of songs, such as The Wanderer, describing the development of Siberia after World War II.

==Cast==
* Vladimir Druzhnikov, as Andrey
* Marina Ladynina, as Natasha
* Vladimir Zeldin, as Boris
* Vera Vacilyevna, as Nastya
* Boris Andreyev, as Yakob Burmak

==Songs==
It is a musical movie, full of songs, old and new.
* The Song of the Siberian Earth (Words by Yevgeniy Dolmatovsky, Music by Nikolai Kryukov)
* The Hymn to Siberia (Words by Yevgeniy Dolmatovsky, Music by Nikolai Kryukov) The Wanderer (in  )

==Influence==
This film was so successful that a second color musical film, Cossacks of the Kuban was made two years later by the same director and cast.

This movie also became popular in Japan, so it gave influence to the   phenomenon in the 1970s.  

==References==
 

==See also==
* Cinema of the Soviet Union
* Cinema of Russia
* Utagoe coffeehouse
* Karaoke

==External links==
*   
*  

 
 
 
 
 
 

 
 