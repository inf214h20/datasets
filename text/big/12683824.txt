Gentle Annie (film)
 
{{Infobox Film
| name           = Gentle Annie
| image          = Gentle Annie - 1945 - Poster.jpg
| image_size     =  
| caption        = 1945 theatrical poster
| director       = Andrew Marton
| producer       = Robert Sisk
| writer         = Lawrence Hazard
| narrator       = 
| starring       = James Craig Donna Reed Marjorie Main David Snell
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = New York and Los Angeles opening: 4 May 1945
| runtime        = 80-81 min.
| country        = U.S.A. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Western theme, James Craig. Marjorie Main played the role of Annie Goss. A notable actor in this film is Harry Morgan, who plays Cottonwood Goss. Morgan is best known for his role as Col. Potter in the TV show M*A*S*H (TV series)|M*A*S*H.

==Synopsis==
A frontierswoman turns her family into a band of bank robbers.

==Cast== James Craig as Lloyd Richland aka Rich Williams
* Donna Reed as Mary Lingen
* Marjorie Main as Annie Goss
* Harry Morgan as Cottonwood Goss (credited as Henry Morgan)
* Paul Langton as Violet Goss
* John Philliber as Barrow

==Production notes==
Hollywood Reporter news items and MGM publicity material provide the following information about the production: MGM purchased MacKinlay Kantors novel in February 1942. Filming began on October 6, 1942, but when director W. S. Van Dyke became ill in early November 1942, production was halted. Tay Garnett was to take over direction on November 9, 1942, but the project was shelved and not revived until June 1944.
 Robert Taylor as Lloyd Richland, Susan Peters as Mary Lingen, Spring Byington as Annie Goss, Charles Grapewin as Barrow and Morris Ankrum as the sheriff. When the project was revived in 1944, the script was rewritten and all the principal crew and cast, except Ankrum and Craig, were replaced.

Production dates: 7 August — early September 1944. Additional scenes began late September 1944.

John Philliber, who plays Barrow in the film, died on November 8, 1944, shortly after filming ended. Gentle Annie was his last picture.

== External links ==
*  
*  

 
 
 
 
 
 
 


 