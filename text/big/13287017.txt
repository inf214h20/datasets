Coqueta
{{Infobox Film
| name           = Coqueta
| image          = Coquetaor.jpg
| caption        = Poster of Coqueta
| director       = Sergio Vejar
| producer       = Daniel Galindo
| writer         = 
| narrator       =  Lucero Pedro Fernández Rodolfo Gómez Sergio Gómez
| music          = 
| cinematography = 
| editing        = 
| distributor    = Televicine 1983
| runtime        = 
| country        = Mexico Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Mexican motion picture released in 1983.

== Synopsis  ==

It tells the story of Rocío (Lucerito aka Lucero (actress)|Lucero), an optimistic adolescent, who finds true love in Pablo (Pedrito Fernández aka Pedro Fernández).  However, the innocent love of this pair is destroyed when Roció faints at her birthday party, which leads to the diagnosis of a grave heart defect.  In desperation, due to the possibility of losing his beloved Roció, Pablo tries to hide his sadness, and show great bravery for the well-being of the girl he adores.  They delight us with their charming songs.

== Cast == Lucero as Rocío
* Pedro Fernández as Pablo
* Rodolfo Gómez as Ricardo
* Sergio Gómez as Luis
* Antonio de Hud as Juan
* Lucero León as Mother of Pablo

== Promotion ==
This movie was promoted for Mexico and Latin Countries. After its release was promoted through television.

== External links ==
*  

 
 
 
 


 
 