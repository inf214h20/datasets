The Merry Widow (1925 film)
{{Infobox film
| name           = The Merry Widow
| image          = The Merry Widow (1925 film).jpg
| image_size     =
| caption        = 
| director       = Erich von Stroheim
| producer       = Erich von Stroheim Irving Thalberg (uncredited)
| writer         = Erich von Stroheim Benjamin Glazer
| based on       =   John Gilbert Roy DArcy David Mendoza (uncredited) Franz Lehár (non-original music)
| cinematography = Oliver T. Marsh William H. Daniels    
| editing        = Frank E. Hull Margaret Booth (uncredited)
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 137 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$592,000 gross = $1.5 million   accessed 19 April 2014 
}} John Gilbert opera of the same name and was the second adaptation of the opera, the first being released in Hungary in The Merry Widow (1918 film)|1918. Joan Crawford and Clark Gable also had uncredited roles in the film.

While a print of the film still survives, the end sequence shot in two-tone Technicolor is now Lost film|lost. 

==Plot==
Prince Danilo falls in love with dancer Sally OHara. His uncle, King Nikita I of Monteblanco forbids the marriage because she is a commoner. Thinking she has been jilted by her prince, Sally marries old, lecherous Baron Sadoja, whose wealth has kept the kingdom afloat. When he dies suddenly, Sally must be wooed all over again by Danilo.

==Cast==
*Mae Murray ...  Sally OHara  John Gilbert ...  Prince Danilo Petrovich 
*Roy DArcy ...  Crown Prince Mirko 
*Josephine Crowell ...  Queen Milena 
*George Fawcett ...  King Nikita I 
*Tully Marshall ...  Baron Sixtus Sadoja 
*Edward Connelly ...  Baron Popoff (ambassador)

===Uncredited===

Selected cast that were uncredited:
*Helen Howard Beaumont..  Chorus girl
*Gertrude Bennett..  Hard-Boiled Virginia 
*Bernard Berger..  Boy 
*Sidney Bracey ...  Danilos footman 
*Estelle Clark ...  French barber 
*Albert Conti ...  Danilos adjutant 
*DArcy Corrigan ...  Horatio 
*Joan Crawford ...  Extra
*Xavier Cugat ...  Orchestra leader 
*Anielka Elter ...  Blindfolded musician   Dale Fuller ...  Sadojas chambermaid
*Clark Gable ...  Ballroom dancing extra

==Production==
The film was shot over twelve weeks with a budget of United States dollar|$592,000. Filming was tense as Mae Murray and the films director, Erich von Stroheim, did not get along.

==Reception==
Upon its release, the film was both a critical and box office success. Critics praised Murrays dramatic skills while also noting that von Stroheim had "made an actress out of Miss Murray".  The film made a profit of $758,000. Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 99 

==Other adaptations== in 1934, 1962 and 1994.

==See also==
* Early Color Feature Filmography

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 