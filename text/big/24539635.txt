Blume in Love
{{Infobox film
| name = Blume in Love
| image = Blume-in-love-movie-poster-1973.jpg
| director = Paul Mazursky
| producer = Paul Mazursky
| writer = Paul Mazursky
| starring = George Segal Susan Anspach Kris Kristofferson Marsha Mason Shelley Winters
| music = Bill Conti
| cinematography = Bruce Surtees
| editing = Donn Cambern
| distributor = Warner Bros.
| released =  
| runtime = 115 min.
| country = United States
| language = English
| gross = $2,900,000 (US/ Canada rentals) 
}}
Blume in Love is a 1973 film written, produced and directed by Paul Mazursky, who also appears in it. It stars George Segal and Susan Anspach.  Others in the cast are Kris Kristofferson, Marsha Mason and Shelley Winters.

Tagline: A love story for guys who cheat on their wives.

==Plot==
Stephen Blume (Segal), a Beverly Hills divorce lawyer, tries to regain the wife (Anspach) who has divorced him.

Wandering around Venice, Italy, where they first honeymooned, Blume wonders what possessed him to betray Nina, a woman he loves, by having sex with his secretary in the bed he and Nina share at home.

Nina promptly leaves him and sets about a journey of self-discovery, trying new things like yoga and taking up with a man 12 years her junior, Elmo (Kristofferson), an unemployed musician. Blume goes to great lengths to win Nina back, complicated by the fact that he finds Elmo to be quite a nice guy.

==Critical Reaction==

The movie was nominated for a Writers Guild of America (WGA) award in the category of "Best Comedy Written Directly for the Screen."

Roger Ebert in his June 18, 1973 review in the Chicago Sun-Times gave this film four stars on a scale of four. Vincent Canby of the New York Times described it on the same date as "a restless, appealing, sometimes highly comic contemporary memoir."

In an interview with Robert K. Elder for his book The Best Film Youve Never Seen, director Neil LaBute explains his feelings on the film: “I was both intrigued and frustrated by what was happening. There’s this fractured telling of the story, several trips to Venice and the rest takes place in Venice, California. So, I think there was attraction to it by the frustration of it—like, ‘What’s happening here? What’s the story?’" 

==Cast==
*George Segal - Stephen Blume
*Susan Anspach - Nina Blume
*Kris Kristofferson - Elmo Cole
*Marsha Mason - Arlene
*Shelley Winters - Mrs. Cramer
*Donald F. Muhich - Analyst
*Paul Mazursky - Hellman
*Erin OReilly - Cindy
*Annazette Chase - Gloria
*Shelley Morrison - Mrs. Greco Mary Jackson - Louise

==Soundtrack==
*"Chester The Goat" - Music & Lyrics by Kris Kristofferson
*"Settle Down And Get Along" - Music & Lyrics by Kris Kristofferson
*"Liebestod" - From "Tristan and Isolde" by Richard Wagner, Performed by Arturo Toscanini & NBC Symphony Orchestra
*"Mr. Tambourine Man" - Music & Lyrics by Bob Dylan
*"Pickpocket" - Music by Sammy L. Greason, Music by Michael E. Utley, Music by Terry Paul, Music by Turner S. Burton & Donald R. Fritts
*"Im In Love With You" - Music & Lyrics by Dillard Crume & Rufus E. Crume
*"Ive Been Workin " - Music & Lyrics by Van Morrison
*"Ive Got Dreams To Remember" - Music & Lyrics by Zelma Redding & Otis Redding
*"Youve Got A Friend" - Music & Lyrics by Carole King
*"De Colores" - Traditional
*"Gondoli, Gondola" - Music & Lyrics by Carosone
*"Dance Of The Hours" - From "La Gioconda", By Amilcare Ponchielli
*"Largo Al Factotum" - From "The Barber of Seville", By Gioachino Rossini
*"Wien Du Stadt Meiner Traume" - By Rudolf Sieczynski
*"Eine kleine Nachtmusik" - By Wolfgang Amadeus Mozart, Performed by the Cafe Quadri Orchestra

==In popular culture==
*The film is featured briefly in Stanley Kubricks Eyes Wide Shut (1999).
*Along with Mazurskys Bob & Carol & Ted & Alice, Blume in Love served as the opening feature for the reopening of Quentin Tarantinos New Beverly Cinema in 2014.

==References==
 

==External links==
*  
*List of American films of 1973

 

 
 
 
 
 
 
 
 
 
 
 
 
 