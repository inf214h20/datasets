Scott Pilgrim vs. the World
 
{{Infobox film
| name           = Scott Pilgrim vs. the World
| image          = Scott Pilgrim vs. the World teaser.jpg
| image_size     = 215px
| alt            = A pink-haired girl named Ramona, standing back to back with a boy in a red t-shirt, Scott Pilgrim. Behind them pictures of her seven evil exes.
| caption        = Official international poster
| director       = Edgar Wright
| producer       = {{Plain list| Marc Platt
* Eric Gitter
* Nira Park
* Edgar Wright
}}
| screenplay     = Michael Bacall Edgar Wright
| based on       =  
| narrator       =  
| starring       = {{Plain list|
* Michael Cera 
* Mary Elizabeth Winstead 
* Kieran Culkin  Chris Evans 
* Anna Kendrick 
* Brie Larson
* Alison Pill 
* Aubrey Plaza
* Brandon Routh 
* Jason Schwartzman
* Johnny Simmons Mark Webber
* Mae Whitman
* Ellen Wong 
}}
| music          = Nigel Godrich
| cinematography = Bill Pope
| editing        = {{Plain list|
* Jonathan Amos
* Paul Machliss
}} Big Talk Films Universal Pictures
| released       =   
| runtime        = 112 minutes  
| country        = {{Plain list |
* United Kingdom
* Japan
* United States  
}}
| language       = English
| budget         =  $85–90 million    $60 million after tax rebates 
| gross          = $47.7 million     
}}
Scott Pilgrim vs. the World is a 2010 comedy film co-written, produced and directed by Edgar Wright, based on the graphic novel series Scott Pilgrim by Bryan Lee OMalley. It stars Michael Cera as musician Scott Pilgrim, who must battle his girlfriend Ramonas seven evil exes, who are coming to kill him.

Scott Pilgrim vs. the World was planned as a film after the first volume of the comic was released. Wright became attached to the project and filming began in March 2009 in Toronto. Scott Pilgrim vs. the World premiered after a panel discussion at the San Diego Comic-Con International on July 22, 2010. It received a wide release in North America on August 13, 2010, in 2,818 theaters.     The film finished fifth on its first weekend of release with a total of $10.5 million.       The film received generally positive reviews from critics, but it failed to recoup its production budget during its release in theaters, grossing $31.5 million in North America and $16 million internationally.   The film has fared better on home formats, becoming the top-selling Blu-ray Disc|Blu-ray on Amazon.com on its first day of sale,  and has gained a cult following. 

==Plot==
 

In Toronto, 22-year-old Scott Pilgrim, bass guitarist of floundering garage band Sex Bob-omb, is dating high schooler Knives Chau, to the disapproval of his friends. Scott meets an American Amazon.com delivery girl, Ramona Flowers, having first seen her in a dream, and loses interest in Knives. As Sex Bob-omb plays in a battle of the bands sponsored by one "G-Man Graves," Scott is attacked by Ramonas ex-boyfriend Matthew Patel. Scott defeats Patel and learns that, in order to date Ramona, he must defeat the remaining six evil exes. He had previously been made aware of this in an email that he received from Patel, warning him of the attack, but dismissed it as "boring."

Scott breaks up with Knives, who blames Ramona for taking Scott from her and swears to win him back. Scott defeats Ramonas second and third evil exes, Hollywood actor and skateboarder Lucas Lee, and vegan Todd Ingram, who is dating Scotts ex-girlfriend, Envy Adams. After he defeats Ramonas fourth ex, Roxy Richter, Scott becomes upset by Ramonas dating history.

At the next battle of the bands round, Sex Bob-omb battles Ramonas fifth and sixth evil exes, twins Kyle and Ken Katayanagi, earning Scott a 1-up. Scott sees Ramona together with her seventh evil ex, Gideon, who is sponsoring the event. Sex Bob-omb accept Gideons record deal, except for Scott, who leaves the band.

Gideon invites Scott to his venue, the Chaos Theater, where Sex Bob-omb is playing. Scott arrives and challenges Gideon to a fight for Ramonas affections, earning the Power of Love and a sword. Knives fights Ramona over Scott, and Scott accidentally reveals that he dated them concurrently. Gideon kills Scott; Ramona visits him in limbo and reveals that Gideon has implanted her with a mind control device.

Scott uses the 1-up to restore his life. He makes peace with his friends and challenges Gideon again, this time for himself. He gains the Power of Self-Respect and disarms Gideon with the sword it grants him. He apologizes to Ramona and Knives for cheating on them, and Scott and Knives team up to defeat Gideon.

Free from Gideons control, Ramona prepares to leave. Knives accepts that her relationship with Scott is over and encourages him to follow Ramona.

===Differences from source material===
To condense the six graphic novels into a single two-hour film, several major changes were made to the plot under the supervision of author Bryan Lee OMalley.

* Instead of playing various unrelated shows around Toronto, Sex Bob-Ombs performances are part of a "battle of the bands" competition. In addition to being the proprietor of the Chaos Theatre, Gideon is also cast as the owner of the record label hosting the competition; there are some added scenes later in the film where Gideons involvement in the groups career causes a rift between Scott and his bandmates (who are now playing the opening night of the Chaos Theatre instead of Envy).
* Most of Ramonas various exes are intact from the novels; though Todd Ingrams personality is unchanged, his tryst with Clash at Demonheads drummer and the fact that he two-timed Ramona and Envy is cut, and the various fight sequences involving Clash at Demonhead are condensed into a single scene after their first show.
* Most of Gideons "evil mastermind" aspects are removed, including "the glow", his invention of subspace, and his collection of frozen exes.
* Though a few major plot points relating to the characters grade school and college lives are still relevant to the film, the extended flashback sequences are cut.
* Major subplots revolving around Kim and Stills are removed. As a result, several key secondary characters from the novels, such as Joseph, Hollie, Craig, and Lisa, do not appear, and the characters of Stacy and Julie are significantly pared down. The Mr. Chau subplot is also removed.
* Several of the mundane aspects of Scotts life are removed or changed. Instead of being evicted, Wallace asks Scott to move out to make room for his boyfriend, and Scotts attempts to find and hold a job as well as the period of extended depression after Ramonas disappearance are removed. The conflict with NegaScott is moved to a brief post-climax gag.

==Cast==
 
;Main characters Scott Pilgrim, bass guitarist of the band Sex Bob-omb as well as a hyper-competent martial artist of some unknown, yet effective anime-style fighting technique. Ramona Flowers, a mysterious American delivery girl with a dating history that drives the plot of the film. Wallace Wells, Scotts 25-year-old gay best friend and roommate. Knives Chau, a 17-year-old high school girl whom Scott dates before meeting Ramona.  Kim Pine, the 23-year-old drummer of Sex Bob-omb and one of Scotts ex-girlfriends. Mark Webber Stephen Stills, the 22-year-old lead singer and "talent" of Sex Bob-omb.
* Johnny Simmons as List of Scott Pilgrim characters#Young Neil|"Young" Neil Nordegraf, a 20-year-old fan of Sex Bob-omb and Scotts replacement after he leaves the band. Stacey Pilgrim, Scotts 18-year-old   sister; she refers to Scott as her "little brother". Natalie "Envy" The Clash at Demonhead.    Julie Powers, Stephens obnoxious ex-girlfriend.

;The League of Evil Exes, in numerical order: Matthew Patel, who has mystical powers, such as levitation and the ability to throw fireballs. Chris Evans Lucas Lee, a "pretty good" skateboarder turned "pretty good" action movie star with super strength. Todd Ingram, telekinetic powers as a result of his veganism; he is the boyfriend of Scotts ex-girlfriend Envy Adams. Roxanne "Roxy" Richter , a self-conscious half-ninja with the ability to teleport. Kyle Katayanagi, twin and popular Japanese musician with the ability to summon powerful creatures like dragons. Ken Katayanagi, twin and popular Japanese musician with the ability to summon powerful creatures like dragons. Gideon Gordon Graves, owner of the Chaos Theatre and the mastermind behind the League of Evil Exes.

;Other characters
* Kjartan Hewitt as Jimmy, Staceys boyfriend; Wallace stole him and the two kiss as Ramona leaves the first round of the Battle of the Bands at the "Rockit"; from Staceys reaction, it is implied that Wallace has done this before
* Ben Lewis as Other Scott, another one of Wallaces boyfriends
* Nelson Franklin as Michael Comeau, one of Scotts friends who "knows everybody"
* Christine Watson as Matthew Patels Demon Hipster Chicks
* Chantelle Chung as Tamara Chen, Knives best friend
* Don McKellar as Director, the director of the Lucas Lee film
* Emily Kassie as Winifred Hailey, a 16-year-old actress who was due to star in a film with Lucas Lee before he was defeated by Scott; she briefly appears on the film set at the Casa Loma
* John Patrick Amedori as the Chaos Theatres bouncer
* Tennessee Thomas as Lynette Guycott, drummer for The Clash at Demonhead.   Luke "Crash" Crash and the Boys who competes in the battle of the bands. Maurie W. Kaufmann as Joel, a member of Crash and the Boys
* Abigail Chu as Trisha "Trasha" Ha, the 8-year-old drummer of Crash and the Boys
* Kristina Pesic and Ingrid Haas as Sandra and Monique, two popular girls at Julies party

Thomas Jane and Clifton Collins, Jr. appear uncredited as the Vegan Policemen. The author, Bryan Lee OMalley, and his wife, Hope Larson, also appear uncredited as Lees Palace bar patrons. Reuben Langdon (known for being the voices of Ken in Street Fighter IV and Dante in the Devil May Cry series) has a cameo as one of Lucas Lees stunt doubles. Bill Hader provides the video-game inspired voice-over.

==Production==

===Development=== Marc Platt film version.    Universal Studios contracted Edgar Wright who had just finished his last film, Shaun of the Dead, agreed to adapt the Scott Pilgrim comics.   OMalley originally had mixed feelings about a film adaptation, stating that he "expected them to turn it into a full-on action comedy with some actor that I hated"   "didnt even care. I was a starving artist, and I was like, Please, just give me some money." 

In May 2005, the studio signed Michael Bacall to write the screenplay adaptation.    Bacall said that he wanted to write the Scott Pilgrim film because he "felt strongly" about the story and "empathized" with Scott Pilgrim  s characters.  By January 2009, filmmakers rounded out its cast for the film, now titled Scott Pilgrim vs. the World.   
Edgar Wright noted that OMalley was "very involved" with the script of the film from the start, and even contributed lines to and "polished" certain scenes in the film. Likewise, due to the long development process, several lines from the various scripts written by Wright and Bacall ended up in books four and five as well.   

OMalley confirmed that no material from Scott Pilgrims Finest Hour, the sixth Scott Pilgrim volume, would appear in the film, as production had already begun. While he had given ideas and suggestions for the final act of the film, he admitted to that some of those plans might change throughout the writing process and ultimately stated that "Their ending is their ending".  OMalley gave Wright and Bacall his notes for the sixth book while filming took place. 
 Casting of the principal characters began in June 2008.     Principal photography began in March 2009 in Toronto   and wrapped as scheduled in August.   In the films original ending, written before the release of the final Scott Pilgrim book, Scott ultimately gets back together with Knives. After the final book in the series was released, in which Scott and Ramona get back together, and divided audience reaction  to the ending with Knives during testing, a new ending was filmed to match the books, with Scott and Ramona getting back together. 

The film was given a production budget of $85–90 million, an amount offset by tax rebates that resulted in a final cost around $60 million.  Universal fronted $60 million of the pre-rebate budget.   
 commentary track was recorded on August 14, 2010, one day after the films theatrical release. 

===Setting=== Goodwill location Bathurst Street is practically the cerebral cortex of Scott Pilgrim". 

===Casting=== Arrested Development.  Wright said he needed an actor that "audiences will still follow even when the character is being a bit of an ass."  Edgar Wright ran all his casting decisions by OMalley during the casting session.    Mary Elizabeth Winstead was Wrights choice for Ramona Flowers two years before filming had started, because "she has a very sunny disposition as a person, so it was interesting to get her to play a version of herself that was broken inside. Shes great in the film because she causes a lot of chaos but remains supernaturally grounded."  Ellen Wong, a Toronto actress known mostly from a role in This Is Wonderland,  auditioned for the part of Knives Chau three times. On her second audition, Wright learned that Wong has a green belt in tae kwon do, and says he found himself intrigued by this "sweet-faced young lady being a secret badass". 

===Music===
 
  David Campbell all contributed to the films soundtrack.            Beck wrote and composed the music played by Sex Bob-omb in the film, with Brian LeBarton playing drums and bass for the band on the films score and soundtrack. Two unreleased songs can also be heard in the teaser trailer. 
 Chris Murphy Sloan was Legend of Zelda video game series is used in a dream sequence in the film. To get permission to use the music, Edgar Wright sent a clip of the film and wrote a letter to Nintendo that described the music as "like nursery rhymes to a generation."   

===Title sequence===
The opening title sequence was designed by Richard Kenworthy of Shynola, and was inspired by drawn-on-film animation. According to Kenworthy:

  }}

==Release==
  at the Scott Pilgrim panel at the San Diego San Diego Comic-Con International|Comic-Con. ]]
A Scott Pilgrim vs. the World panel featured at the San Diego Comic-Con International held on July 22, 2010. After the panel, Edgar Wright invited selected members of the audience for a screening of the film which was followed by a performance by Metric.    Scott Pilgrim vs. the World was also shown at the Fantasia Festival in Montreal|Montreal, Quebec, Canada on July 27, 2010 and was also featured at the Movie-Con III in London, England on August 15, 2010.  
 premiered in Japan during the Yubari International Fantastic Film Festival on February 26, 2011 as an official selection. It was released to the rest of the country on April 29, 2011.  

===Marketing=== teaser trailer was released. 

A second trailer featuring music by The Ting Tings, LCD Soundsystem, Be Your Own Pet, Cornelius (musician)|Cornelius, Blood Red Shoes, and The Prodigy was released May 31, 2010. 
 Chris Evans. Titmouse Inc., adapts the opening prologue of the second Scott Pilgrim book and was aired on Adult Swim on August 12, 2010, later being released on their website.  Michael Cera stated that he felt the film was "a tricky one to sell. I dont know how you convey that movie in a marketing campaign. I can see it being something that people are slow to discover. In honesty, I was slow to find Shaun of the Dead". 

===Video game===
 
A video game was produced based on the series. It was released for PlayStation Network on August 10, 2010 and on Xbox Live Arcade on August 25, being met with mostly positive reviews.   The game is published by Ubisoft and developed by Ubisoft Montreal and Ubisoft Chengdu, featuring animation by Paul Robertson and original music by Anamanaguchi.  

===Home media===
Scott Pilgrim vs. the World was released on DVD and Blu-ray Disc in North America on November 9, 2010  and in the United Kingdom on December 27, 2010. 

The DVD features include four   including the original ending (where Scott ends up with Knives) with commentary, bloopers, photo galleries, and a trivia track.
 Scott Pilgrim trailers and TV spots, storyboard picture-in-picture, a DVD copy, and a digital copy. The "Ultimate Japan Version" Blu-ray Disc includes a commentary track that features Wright and Shinya Arino. It also includes footage of Wright and Michael Ceras publicity tour through Japan and a roundtable discussion with Japanese film critic Tomohiro Machiyama. It was released on September 2, 2011. 

In its first week of release, the DVD sold 190,217 copies, earning $3,422,004 in revenue, and as of 2011 earned $27,349,933 on the total US home media sales.  It reached the top of the UK Blu-ray Disc charts in its first week of release. 

==Reception==

===Box office=== widely released financial disappointment".     Universal acknowledged their disappointment at the opening weekend, saying they had "been aware of the challenges of broadening this film to a mainstream audience"; regardless, the studios spokesman said Universal was "proud of this film and our relationship with the visionary and creative filmmaker Edgar Wright.... Edgar has created a truly unique film that is both envelope pushing and genre bending and when examined down the road will be identified as an important piece of filmmaking." 

In the UK, the film opened in 408 cinemas, finishing second on its opening weekend with £1.6 million,  dropping to fifth place by the next weekend.

===Critical response===
Critical response to the film has been positive. Review aggregation website Rotten Tomatoes gives the film a score of 82% based on 245 reviews, with an average score of 7.5 out of 10. The sites consensus states: "its script may not be as dazzling as its eye-popping visuals, but Scott Pilgrim vs. the World is fast, funny, and inventive".    average score of 69, based on 38 reviews, which indicates "generally favorable reviews". 

 
At a test screening, director Kevin Smith was impressed by the film saying "That movie is great. Its spellbinding and nobody is going to understand what the fuck just hit them. I would be hard pressed to say, hes bringing a comic book to life! but he is bringing a comic book to life." Smith also said that fellow directors Quentin Tarantino and Jason Reitman were "really into it".    Singer for the band Sister and writer for Now (newspaper)|Now, Carla Gillis, also commented on the film.    Gillis was the singer of the now-disbanded Canadian group Plumtree (band)|Plumtree, and their single "Scott Pilgrim" inspired OMalley to create the character and the series.  In an interview describing the film and the song that inspired it, Gillis felt the film carried the same positive yet bittersweet tone of the song. 

After premiere screenings at the San Diego Comic-Con International, the film received positive reviews. Variety (magazine)|Variety gave the film a mixed review, referring to the film as "an example of attention deficit hyperactivity disorder|attention-deficit filmmaking at both its finest and its most frustrating" and that "anyone over 25 is likely to find director Edgar Wrights adaptation of the cult graphic novel exhausting, like playing chaperone at a party full of oversexed college kids."   

The Hollywood Reporter wrote a negative review, stating that "Whats disappointing is that this is all so juvenile. Nothing makes any real sense...  Cera doesnt give a performance that anchors the nonsense." and "Universal should have a youth hit in the domestic market when the film opens next month. A wider audience among older or international viewers seems unlikely." 
  does in the best (and most Frank Tashlin|Tashlin-like) of all the surreal martial-arts comedies, Kung Fu Hustle." 

Cindy White at IGN gave the film a positive rating of 8/10 calling the film "funny and offbeat" as well as noting that the film is "best suited for the wired generation and those of us who grew up on Nintendo and MTV. Its kinetic nature and quirky sensibilities might be a turnoff for some." 
 superhero hyperbole, and its a lot for a director to take on. Wright, who is British, has taken it on and won. Scott Pilgrim vs. the World lives and breathes the style of the original books, with animated squiggles and hearts and stars filling out the frame in many individual shots. Some of this is cute; some of it is better, weirder than cute." Phillips concludes: 
 "To enjoy the film you must enjoy the brash, satiric spirit of heros quest. Cera and his fellow ensemble members, including Kieran Culkin as Scotts roommate, Anna Kendrick as his snippy younger sister and the majestically dour Alison Pill as the bands drummer, mitigate the apocalyptic craziness with their deadpan wiles. At its best, Wrights film is raucous, impudent entertainment." 
 Tomohiko Itō, Rintaro Watanabe, and Takao Nakano. 

In June 2013, Scott Pilgrim creator Bryan Lee OMalley, who is of Korean and white Canadian parentage, stated that he regretted the fact that the films cast was predominantly white, and that there were not enough roles for minorities. 

===Accolades===
  Best Visual Effects at the 83rd Academy Awards, but did not receive a nomination.
{| class="wikitable sortable" ;  
|- style="background:#ccc; text-align:center;"
|+ style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Name
! Outcome
|- Casting Society Artios Awards Outstanding Achievement in Casting – Big Budget Feature – Comedy Robin D. Cook and Jennifer Euston
| 
|- Austin Film Austin Film Critics Association Awards Best film
| 
|- Central Ohio Film Critics Association Best Picture
| 
|- Best Overlooked Film
| 
|- Detroit Film Detroit Film Critics Society Awards Best Director Edgar Wright
| 
|- Best Ensemble Overall casting
| 
|- Empire Awards Best Film
| 
|- Best Sci-Fi/Fantasy
| 
|- Best Director Edgar Wright
| 
|- GLAAD Media Awards Outstanding Film – Wide Release
| 
|- Hugo Awards Hugo Award Best Dramatic Presentation – Long Form Michael Bacall and Edgar Wright
| 
|- Sierra Awards Best Art Direction
| 
|- Best Costume Design Laura Jean Shannon
| 
|- Best Song Beck for "We Are Sex Bob-Omb"
| 
|- Best Visual Effects
| 
|- Online Film Online Film Critics Society Awards Online Film Best Editing Jonathan Amos and Paul Machliss
| 
|- Online Film Best Adapted Screenplay Michael Bacall and Edgar Wright
| 
|- SFX Awards Best Film Director Edgar Wright
| 
|- San Diego San Diego Film Critics Society Awards San Diego Best Editing Jonathan Amos and Paul Machliss
| 
|- San Diego Best Adapted Screenplay Michael Bacall and Edgar Wright
| 
|- Satellite Awards {{cite web
| url = http://www.guardian.co.uk/film/2010/dec/20/scott-pilgrim-world-satellite-awards
| title = Take that! Twice. Scott Pilgrim Vs the World wins two Satellite awards
|work=The Guardian 
| publisher = Guardian News and Media Limited
| date =December 20, 2010
| last =Child | first =Ben
| accessdate=December 28, 2010}}  Satellite Award Best Film – Musical or Comedy
| 
|- Satellite Award Best Actor – Motion Picture Musical or Comedy Michael Cera
| 
|- Satellite Award Best Art Direction and Production Design Nigel Churcher and Marcus Rowland
| 
|- Satellite Award Best Adapted Screenplay Michael Bacall and Edgar Wright
| 
|- Saturn Awards Saturn Award Best Fantasy Film
| 
|- 2011 Scream Scream Awards The Ultimate Scream
| 
|- Best Director Edgar Wright
| 
|- Best Scream-Play
| 
|- Best Villain Satya Bhabha, Chris Evans, The League of Evil Exes
| 
|- Best Supporting Actress Ellen Wong
| 
|- Best Supporting Actor Kieran Culkin
| 
|- Fight Scene of the Year Final Battle: Scott Pilgrim and Knives vs. Gideon Graves
| 
|- Best Comic Book Movie
| 
|- Teen Choice Awards
|  Michael Cera
| 
|-
|  Mary Elizabeth Winstead
| 
|-
|colspan=2| 
| 
|- Utah Film Critics Association Awards Best Director Edgar Wright
| 
|-  Best Screenplay Michael Bacall and Edgar Wright
| 
|}

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   on Vimeo
*   from Flickr, with numerous photographs related to the film

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 