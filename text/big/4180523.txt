Kaakha Kaakha
{{Infobox film
| name           = Kaakha Kaakha
| image          = Kaakha Kaakha poster.jpg
| caption        = Poster
| director       = Gautham Menon
| writer         = Gautham Menon Suriya Jyothika Jeevan
| producer       = Kalaipuli S. Dhanu
| music          = Harris Jayaraj Anthony
| cinematography = R. D. Rajasekhar
| released by    = V. Creations
| studio         = V. Creations
| released       = 1 August 2003
| runtime        = 153 minutes
| country        = India
| language       = Tamil
| budget         =   
}}
 Tamil crime crime thriller film written and directed by Gautham Menon. Starring Suriya (actor)|Suriya, Jyothika and Jeevan (Tamil actor)|Jeevan, the film featured music composed by Harris Jayaraj and cinematography by R. D. Rajasekhar. The film released to highly positive reviews in August 2003 and went on to become the first   Biggest Blockbuster in Suriyas career,  and was considered a comeback film for producer Kalaipuli S. Dhanu.  Owing to the success, the film has been remade in several languages. 

==Plot==
A badly injured Anbuchelvan (Suriya) is lying on the bank of a stream, thinking about his wife Maya and how he needs to rescue her. The story moves quickly from this opening scene to a flashback of Anbuchelvan’s time as a young policeman.

Anbuchelvan is an honest,daring IPS officer with chennai police as ACP in crime branch. As he has no relations he had no fear on life.
Anbuchelvan and his friends, Shrikanth, Arul and Illamaran, have been recruited for part of a special unit of police officers who are battling organized crime in Chennai. Violent and laconic, Anbuchelvan finds little patience for a personal life. The unit is ruthless in its confrontation with criminals, going as far as assassinating gang members; the unit is finally disbanded by human rights authorities. Anbuchelvan is posted to Control Room Duties.

One day a schoolteacher named Maya (played by jothika) rebuffs Anbuchelvans routine questions regarding safety, not knowing that he is a police officer. He meets her again when she and her friend are questioned for driving without a license. However, Anbuchelvan lets them off with a warning. When one of Maya’s students has a problem with local kids, she asks Anbuchelvan for help. Anbuchelvan resolves this problem, a mutual respect grows between them and they begin seeing one another. When Maya gets into a road accident, Anbuchelvan helps her to recover and they fall in love. Shrikanth and his wife, Swathi, become good friends with Anbuchelvan and Maya.

In response to rising levels of crime in the city, when the son of an influential movie producer is kidnapped and killed, the special unit is reassembled by commissioner with all 4 back in crime branch. The unit tracks down and kills the head of the gang that was responsible. The brother of the gang leader, Pandya (played by Jeevan (Tamil actor)|Jeevan), returns from Mumbai and takes over the gang, promising revenge over his brother’s death. Pandya and his gang members target the families of the men in the special unit, but the police close in and a badly injured Pandya barely escapes Anbuchelvan.

Maya and Anbuchelvan get married and leave for Pondicherry (city)|Pondicherry. But the next day, Pandya and his thugs enter the cottage the honeymoon couple are staying in and torture Anbuchhelvan, leaving him for dead. They kidnap Maya. This brings the viewer back to the opening scene of the movie, in which Anbuchelvan is battling for life but thinking only about rescuing Maya.

Shrikanth and Arul arrive at the cottage, discover Anbuchelvan and take him to the hospital. Shrikanth reveals that his wife Swathi was kidnapped earlier and confesses that it was he who gave away Anbuchelvan’s location to Pandya, for the safe return of Swathi. Shrikanth feels extreme remorse over what has happened. Whilst in the hospital, they receive a message from Pandya to meet him at a particular location. When they go there, they find a package containing the severed head of Swathi. Shrikanth is distraught at seeing his wifes head and in an agony of grief and guilt at being responsible, he commits suicide by shooting himself. Anbuchelvan tracks down Pandya before he can escape from Tamil Nadu and fights with the gang. Pandya stabs Maya to distract her husband and she dies in Anbuchelvan’s arms. An enraged Anbuchelvan tracks down Pandya and, in a final encounter, kills him.

An epilogue shows that Anbuchelvan, after the death of Maya, continues his job as an IPS officer some weeks later. An alternative ending was shot and placed in the DVD version with a running commentary by Gautham Menon, in which Maya comes alive and he explains why this ending was not used in the version for cinema release.

==Cast== Suriya as Anbuchelvan
* Jyothika as Maya Anbuchelvan Jeevan as Pandian
* Daniel Balaji as Shrikanth
* Devadarshini as Swathi
* Yog Japee as Sethu
* Vivek Anand as Arul
* Rajan as Ilamaran
* Gautham Menon as Vasudeva Nair
* Cool Suresh
* Ramya Krishnan in a special appearance for "Thoodhu Varumaa" song

==Character map and remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
| Kaakha Kaakha (2003) (Tamil cinema|Tamil) ||  Gharshana (2004) (Cinema of Andhra Pradesh|Telugu) || Force (2011 film)|Force (2010) (Bollywood|Hindi) ||  Dandam Dashagunam (2011) (Cinema of Karnataka|Kannada)
|- Suriya (actor)|Suriya Venkatesh  John Abraham   || Chiranjeevi Sarja 
|-
| Jyothika ||  Asin ||  Genelia DSouza || Divya Spandana
|- Jeevan || Salim Baig || Vidyut Jamwal || P. Ravi Shankar
|}

==Production==
The film was initially tiled as Paathi (Half), before the team opted to change the title to Kaakha Kaakha.  Menon revealed that he was inspired to make the film after reading of articles on how encounter specialists shoot gangsters and how their families get threatening calls in return, and initially approached  Ajith Kumar and Vikram for the role without success.  The lead actress Jyothika asked Menon to consider Suriya for the role, and he was subsequently selected after Menon saw his portrayal in Nandha.    He did a rehearsal of the script with the actors, a costume trial with Jyothika and then enrolled Suriya in a commando training school before beginning production, which he described as a "very planned shoot". 

==Release==
The film consequently opened to very positive reviews from critics on the way to becoming another success for Menon, with critics labeling it as a "career high film".    Furthermore, the film was described as for "action lovers who believe in logical storylines and deft treatment" with Menon being praised for his linear narrative screenplay.    

===Remakes=== Venkatesh was John Abraham English version with a Chechnyan backdrop, though talks with a potential collaboration with Ashok Amritraj collapsed. 

===Awards and nominations===
In addition to the following list of awards and nominations, prominent Indian film websites named Kaakha Kaakha one of the 10 best Tamil films of 2003, with Rediff, Sify and Behindwoods all doing so. The film was, before release, in "most awaited" lists from film websites.

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- 2003 Filmfare Awards South Filmfare Best Best Actor Suriya (actor)|Suriya
| 
|- Filmfare Best Best Actress Jyothika
| 
|- Filmfare Best Best Villain Jeevan (Tamil Jeevan
| 
|- Filmfare Best Best Director Gautham Menon
| 
|- Filmfare Best Best Film Kalaipuli S. Dhanu
| 
|- Filmfare Best Best Choreography Brindha (Thoodu Varuma & Uyirin Uyire)
| 
|- Filmfare Best Best Music Director Harris Jayaraj
| 
|- Filmfare Best Best Cinematographer
|R. D. Rajasekhar
| 
|- Filmfare Best Best Editor Award Anthony (film Anthony
| 
|- 2003 Tamil Nadu State Film Awards Tamil Nadu Best Music Director Harris Jayaraj
| 
|- Tamil Nadu Best Editor Anthony (film Anthony
| 
|- International Tamil ITFA Awards  ITFA Best Best Actor  Suriya (actor)|Suriya
| 
|- International Tamil ITFA Awards  ITFA Best Best Music Director  Harris Jayaraj
| 
|}

==Soundtrack==
{{Infobox album|  
 Name = Kaakha Kaakha
| Type = soundtrack
| Artist = Harris Jayaraj Feature film soundtrack
| Label = New Music
| Last album = Kovil (film)|Kovil (2003)
| This album = Kaakha Kaakha (2003)
| Next album = Gharshana (2004) 
}} Filmfare Award ITFA Award for Best Music Director.

{| class="wikitable" style="width:50%;"
|-
! Song title !! Singers !! Notes
|- Jo asks Suriya to take her to Pondicherry.
|-
| Ondra Renda  || Bombay Jayashri || A beautiful song after the marriage of Suriya & Jyothika|Jo.
|-
| Oru Ooril || Karthik || Intro song for Jyothika|Jo, which renders her beauty.
|-
| Thoodhu Varuma || Sunitha Sarathy || An item number by Ramya Krishnan.
|-
| Uyirin Uyirae || KK (singer)|KK, Suchitra || The song takes place when Suriya thinks about his wife Jyothika|Jo.
|}

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 