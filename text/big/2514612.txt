A Cow at My Table
{{Infobox film
| name           = A Cow at My Table
| image          = A Cow at My Table (film) cover.jpg
| alt            =  
| caption        = 
| director       = Jennifer Abbott
| producer       = Flying Eye Productions
| writer         = 
| starring       =  Oh Susanna
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = Canada
| language       = 
| budget         = 
| gross          = 
}} attitudes towards farm animals and meat.

It covers the conflict between animal rights advocates and the meat industry, and their respective attempts to influence consumers. It was directed, shot and edited by Jennifer Abbott, who spent five years travelling across Canada, the United States, Australia and New Zealand to interview representatives on all sides. The film intercuts these interviews with images of farm animals and industrial farming operations. It explores what is sometimes popularly called factory farming. 

The filming of A Cow at My Table drew early criticism from the Canadian meat industry, with both the Ontario Chicken Marketing Board and the Dairy Farmers of Ontario publishing articles warning of Abbotts actions. 
 Oh Susanna. 

==References==
 

==External links==
*  on Google Video

 

 
 
 
 
 
 
 
 
 
 


 
 