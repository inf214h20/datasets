Mardi Gras: Spring Break
{{Infobox film
| name           = Mardi Gras: Spring Break
| alt            =  
| image	         = Mardi Gras- Spring Break FilmPoster.jpeg
| caption        = 
| director       = Phil Dornfield
| producer       = 
| writer         = Josh Heald
| starring       =  
| music          =  
| cinematography = 
| editing        = Mark Scheib
| studio         =  
| distributor    = Samuel Goldwyn Films   
| released       =      
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = 
}}
Mardi Gras: Spring Break is a 2011 comedy/road trip film. It stars Nicholas DAgosto, Josh Gad, Bret Harrison, Arielle Kebbel, Danneel Harris, Regina Hall, and Carmen Electra. It is directed by Phil Dornfield.  The film follows a trio of senior college students who visit New Orleans during the Mardi Gras season. 

Originally shot in 2008 as Maxs Mardi Gras,    it was scheduled for release by Sony Pictures Screen Gems division.     It was shelved until September 2011,  when Samuel Goldwyn Films released it in select cities. 

==Plot==
Three best friends Mike (Nicholas DAgosto), Bump (Josh Gad), and Scottie (Bret Harrison) in search of a booze-fueled sexcapade find their way to Mardi Gras for boobs, beads and brews along with Mikes clingy girlfriend Erica (Danneel Harris).

==Cast==
* Nicholas DAgosto as Mike
* Josh Gad as Bump
* Bret Harrison as Scottie
* Arielle Kebbel as Lucy
* Danneel Harris as Erica
* Regina Hall as Ann Marie
* Carmen Electra as herself
* Becky ODonohue as Cousin Janice
* Jessie ODonohue as Cousin Janine
* Jessica Heap as Oyster Chick
* Julin Jean as Sarah
* Gary Grubbs as Mr. Duluth
* Denise Williamson as Samantha
* J. Patrick McNamara as Professor Fleischman
* Danni Lang as Maxim Model
* Stephanie Honore as Bourbon Street Girl
* Marcelle Baer as Oyster Girl #1

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 