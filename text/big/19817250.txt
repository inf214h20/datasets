Doin' Their Bit
{{Infobox film
| name           = Doin Their Bit
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Glazer
| producer       = 
| writer         = Hal Law Robert A. McGowan Robert Blake
| music          = 
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 56"
| country        = United States 
| language       = English
| budget         = $28,306
| gross          = $24,651
}} short comedy film directed by Herbert Glazer.  This was the first short with Herbert Glazer as Our Gangs regular director. It was the 207th Our Gang short (208th episode, 119th talking short, 120th talking episode, and 39th MGM produced episode) that was released.

==Plot==
Hoping to entertain the military troops stationed in Greenpoint, Mr. Wills organizes the gang into a junior USO troupe. In addition to performing a "boot camp" sketch, the gang participates in a brace of production numbers.   

==Cast==
===The Gang===
* Billy Laughlin as Froggy
* Janet Burston as Janet Mickey Gubitosi as Mickey
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Beverly Hudson as Miss Liberty
* Walter Wills as Mr. Wills
* Freddie Chapman as Messenger boy / Union of South Africa
* Vincent Graeff as Taxi driver / Poland
* Edward Soo Hoo as China
* Valerie Lee as Luxembourg
* Lawrence Long, Jr. as Milkman / Uruguay
* Freddie Walburn as Free France

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 