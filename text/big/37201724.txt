Grave Encounters 2
{{Infobox film
| name           = Grave Encounters 2
| image          = Graveencounters2poster.jpg
| caption        = 
| director       = John Poliquin
| producer       = Twin Engine Films Pink Buffalo Films Shawn Angelski Martin Fisher
| writer         = The Vicious Brothers
| Based on       = Grave Encounters The Vicious Brothers
| starring       = Richard Harmon Leanne Lapp Sean Rogerson Dylan Playfair Stephanie Bennett  Howie Lai
| music          = Quynne Alana Paxa
| cinematography = Tony Mirza
| editing        = John Poliquin The Vicious Brothers
| studio         = 
| distributor    = Tribeca Film Festival Arclight Films
| released       =   (VOD or iTunes) October 12, 2012 (theatrical release in U.S.)
| runtime        = 100 minutes
| country        = Canada United States
| language       = English
| budget         = $1,400,000
| gross          = $8,211,000
}} found footage psychiatric hospital malevolent entities. The film was released on iTunes on October 2, 2012 and received a limited theatrical release on October 12, 2012. Grave Encounters 2 became a commercial success, but was a critical failure.

==Plot==
Film student Alex Wright and his friends want to produce a documentary about the original Grave Encounters movie. Alex believes that the first film was real. He posts an online plea for any information about the film and receives a message from someone named "DeathAwaits666." The message leads Alex to the mother of Sean Rogerson, the actor who played Lance in Grave Encounters. She believes that Sean is still alive but they discover that she has severe dementia.
 Ouija board. Using it to communicate with the spirits, they realize that their online contact is not a person, but a paranormal entity.
 electroshock machine, used to give patients seizures. He is electrocuted. While fleeing from one of the spirits, they escape the hospital and return to their hotel room. However, when they use the elevator to get to the hotel lobby, it instead opens onto the tunnels beneath the hospital, revealing that their escape was an illusion, and they are once again trapped. 

They meet the actor Sean Rogerson, and discover that he has been trapped in the hospital for over nine years, subsisting on rats and water from the hospital toilets. Rogerson has been lobotomized and driven insane, but he shows them that the buildings layout is far bigger than a city, and that he has made a map. He claims to be able to communicate with the buildings entities due to the lobotomy performed on him by Dr. Friedkin, and states that the reason the hospital is like this is due to experiments and rituals by Dr. Friedkin. Sean shows them a freestanding red door in one of the rooms and tells them it is the only way out, but it is wrapped in chains.

While they sleep for the night, an invisible entity picks up a camera and films them. Sean kills Trevor, claiming the entities made him do it. Alex and Jennifer discover Sean and their equipment have disappeared. Sean cuts the chains on the red door with the teams tools and enters, only to realize the door leads nowhere. Frenzied, he begins talking to the entities, who instruct Sean to "finish the film" so it will draw more curious thrill-seekers to the hospital. But only a single survivor will be allowed to leave.

Alex and Jennifer see Dr. Friedkins satanic altar and hide as the doctor performs a lobotomy. The nurses then present an infant to Dr. Friedkin, who sacrifices it. The couple flees and Sean demands they hand over their tapes. Alex refuses and Sean tries to kill him. During the struggle, a void opens up on the wall and sucks Sean in. Alex realizes that Sean was being honest in how to escape so he smashes Jennifers face in with the camera. He then turns the camera on himself, promising that he will finish the film.

He exits the hospital through the red door which leads him to a field on the outskirts of Los Angeles, and he is soon arrested while walking down the street at night. The last scene shows that the footage has been made into a film, with Alex and producer Jerry Hartfield claiming that everything the public sees has been staged and that it is just a movie. However, Alex tells the interviewers not to go anywhere near the hospital because its not worth it.

==Cast==
*Richard Harmon as Alex Wright, a university student and filmmaker who becomes obsessed with the previous film.
*Leanne Lapp as Jennifer Parker, a fellow student who is in love with Alex.
*Sean Rogerson as himself/Lance Preston, having survived, he has gone insane after being trapped in the hospital for nine years.
*Dylan Playfair as Trevor Thompson, Alexs best friend and cameraman.
*Stephanie Bennett as Tessa Hamill, Jennifers friend.
*Howard Lai as Jared Lee, Alexs second cameraman and Tessas boyfriend.
*Sean Tyson as the guard stationed at the hospital.
*Ben Wilkinson as Jerry Hartfield, the producer of Grave Encounters.
*Arthur Corber as Dr. Friedkin, a doctor at Collingwood Psychiatric Hospital.
*Brenda McDonald as Mrs. Rogerson.
*Collin Minihan and Stuart Ortiz as The Vicious Brothers, the directors of Grave Encounters.
*Dalila Bela as Kaitlin, a former patient at Collingwood.
*Roy Campsall as the emaciated demon.
*Melissa Valvok and Brenda Anderson as nurse demons.
*Dale Hall (IV) as operated patient.
*Maddox Valvok as baby.
*Merwin Mondesir (uncredited cameo) as himself/T.C. Gibson, the cameraman for Grave Encounters.
*Mackenzie Gray (uncredited cameo) as himself/Houston Grey, the psychic for Grave Encounters.
*Juan Riedinger (uncredited cameo) as himself/Matt White, the technical expert for Grave Encounters.
*Ashleigh Gryzcko (uncredited cameo) as herself/Sasha Parker, an occult specialist for Grave Encounters.
*Reese Alexander and Meeshelle Neal as LA Police Officers.
*Satan as Satan.

==Production== John Poliquin, and only written by the Vicious Brothers. The first trailer was released on September 4, 2012.  The films budget was $1,400,000. It was released on October 12, 2012 in a limited theatrical run, but released earlier in the month on iTunes for download.

==Critical response==
Grave Encounters 2 received negative reviews from critics. Rotten Tomatoes gave the film a score of 14% based on 7 reviews, with an average rating of 4.8 out of 10.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 