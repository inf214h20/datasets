Thoondil
 
{{Infobox film|
| name = Thoondil
| image = ThoondilDVD.jpg
| caption = Thoondil DVD Cover
| director = K. S. Adhiyaman
| writer = K. S. Adhiyaman (dialogues)
| screenplay = K. S. Adhiyaman
| story  = K. S. Adhiyaman Sandhya Divya Vivek Revathi RK
| producer = M. Rajkumar S. S. R. Thillainathan P. Kaandeepan
| music = Abhishek Ray
| camera = T. Kaviarasu
| editing = V. M. Udhayashankar
| cinematography = T. Kaviyarasu
| released =  
| runtime =
| language = Tamil
| country = India
| budget =
| website =
}} Tamil film Sandhya directed by Adhiyaman. The film opened to poor reviews.

==Plot==
The film tries to work on the old saying "hell hath no fury like a woman scorned". Divya (Divya Spandana) is an upcoming model in London who meets Sriram (Shaam) an IT guy and falls in love. They sleep together but Sriram leaves her when Divyas boss tells him to stay out of her life if he wants to see her make it big. Divya feels betrayed when she finds out but does not know that her boss is the reason behind the split. Divya does achieve her dream and becomes a top model but is on a revenge romp to wreak havoc in Sriram’s happily married life to Anjali (Sandhya (actress)|Sandhya). Sandhya, is a cheerful girl whose only sorrow in life is that she doesnt have a child. And when she finally has a baby after 4 years of marriage to Sriram, Divya comes into her life and takes the baby away, saying that that is her baby. What happens after that forms the climax of the movie.

==Cast==
*Shaam Sandhya
*Divya Spandana Vivek
*Revathi RK

==References==
 

==External links==
 

 

 
 
 
 
 


 