If I Had Known I Was a Genius
{{Infobox film
| name           = If I Had Known I Was a Genius
| image          = If I Had Known I Was A Genius Theatrical Poster.jpg
| caption        = Theatrical poster
| director       = Dominique Wirtschafter
| producer       = Mike Crawford Al Hayes Daniel Sadek Elie Samaha Andrea Sperling Dominique Wirtschafter
| writer         = Markus Redmond
| starring       = Markus Redmond Whoopi Goldberg Keith David Debra Wilson Sharon Stone Tara Reid Nadia Bjorlin Della Reese Andy Richter French Stewart
| music          = John Coda
| cinematography = Scott Kevan
| editing        = Jeff Canavan
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Sundance Film Festival.   

== Plot ==
Michael (Markus Redmond) is an African-American boy with a genius I.Q. His family refuses to encourage him and tries to bring him down, and his mother (Whoopi Goldberg) nicknames him "Ugly". Michael enrolls in a high school drama class and finds encouragement from an eccentric teacher. 

An underestimated and completely misunderstood boy genius living with a dysfunctional family goes through a series of life challenges and finds it very difficult to fit in anywhere. He finds his true calling when his path collides with his drama teacher who notices his special gift and potential to become an exceptional actor. He attempts to realize his dream of becoming an actor but not without facing disbelief and contempt from both his family and friends.

== Cast ==
*Keith David as Dad
*David Denman as Baker
*Whoopi Goldberg as Mom
*Nadia Bjorlin as Faith
*Julie Hagerty as Teacher
*Markus Redmond as Michael
*Della Reese as Nana
*Tara Reid as Stephanie
*Andy Richter
*French Stewart as Principal
*Sharon Stone as Gloria Fremont
*Debra Wilson as Teresa

==References==
 

== External links ==
* 
* 
* 

 
 
 