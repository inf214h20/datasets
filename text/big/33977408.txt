The Battle of Hearts
{{infobox film
| name           = The Battle of Hearts
| image          = The Battle of Hearts1916newspaper.jpg
| imagesize      =
| caption        = Scene from the film, from a newspaper
| director       = Oscar Apfel
| producer       = 
| writer         = Oscar Apfel (scenario)
| story          = Frances Marion
| starring       = William Farnum Hedda Hopper
| music          =
| cinematography = Alfredo Gandolfi
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 50 mins.
| country        = United States
| language       = Silent English intertitles
}} silent drama Elda Furry (later known as Hedda Hopper). The story was by Frances Marion, then still an actress herself.  This is Hoppers first motion picture.  

Hopper screened small portions of it in 1942 in her self produced short series Hedda Hoppers Hollywood. It is unknown if she had a complete print of this film at the time. These portions can be seen today in her 1942 short.

==Cast==
* William Farnum – Martin Cane
* Elda Furry – Maida Rhodes
* Wheeler Oakman – Jo Sprague
* William Burress – Captain Sprague
* Willard Louis – Captain Rhodes

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 
 
 
 
 

 