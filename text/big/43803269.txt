Don't Tell the Wife
{{Infobox film
| name           = Dont Tell the Wife
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Christy Cabanne
| producer       = Samuel J. Briskin Robert Sisk (associate)
| writer         = 
| screenplay     = Nat Perrin
| story          = 
| based on       =  
| starring       = Guy Kibbee Una Merkel Lynne Overman
| narrator       = 
| music          = 
| cinematography = Harry Wilde
| editing        = Jack Hively RKO Radio Pictures
| distributor    = 
| released       =   |ref2= }}
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Dont Tell the Wife is a 1937 American comedy film directed by Christy Cabanne using a screenplay by Nat Perrin adapted from the play, Once Over Lightly, written by George Holland. The film stars Guy Kibbee, Una Merkel, and Lynne Overman, with Lucille Ball, William Demarest, and Academy Award winner Hattie McDaniel in supporting roles.  Produced by RKO Radio Pictures, it premiered in New York City on February 18, 1937, and was released nationwide on March 5.

==References==
 

==External links==
* 

 
 
 
 
 
 


 