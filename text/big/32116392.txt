Tigers, London Zoological Gardens
{{Infobox film
| name           = Tigers, London Zoological Gardens
| image          = TigersLondonZoologicalGardens.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Alexandre Promio
| producer       = Auguste and Louis Lumière
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        =
| country        = France Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 1896 France|French short black-and-white silent actuality film, produced by Auguste and Louis Lumière and directed by Alexandre Promio, featuring two tigers reaching through the bars of its enclosure at London Zoological Gardens to get at the meat offered on a stick by their keeper. The film was part of a series, including Lion, London Zoological Gardens|Lion and Pelicans, London Zoological Gardens|Pelicans, which were one of the earliest examples of animal life on film. 

==References==
 

 
 
 
 
 
 

 