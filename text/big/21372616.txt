Mandalay (film)
{{Infobox film
| name = Mandalay
| image = Mandalay-1934.jpg
| image_size = 225px
| caption = theatrical movie poster
| director = Michael Curtiz
| producer = Robert Presnell Sr.
| screenplay = Austin Parker Charles Kenyon
| story = Paul Hervey Fox
| starring = Kay Francis Ricardo Cortez Warner Oland Lyle Talbot
| cinematography = Tony Gaudio Thomas Pratt
| music = "When Tomorrow Comes" Sammy Fain (music) Irving Kahal (lyrics)
| studio = First National Pictures
| distributor = Warner Bros.
| released =  
| runtime =  65 minutes
| country = United States
| language = English budget = $294,000   accessed 16 March 2014  gross = $629,000 
}}
 1934 American drama film directed by Michael Curtiz and written by Austin Parker and Charles Kenyon based on a story by Paul Hervey Fox. The film stars Kay Francis, Ricardo Cortez, Warner Oland and Lyle Talbot, and features Ruth Donnelly.  The film is about a world-weary woman (Francis) nicknamed Spot White at the local brothel and bar who does what she can to survive.

Director Curtiz used cutting edge wipes and opticals in the film.   Future child star Shirley Temple won a small role in the film as the daughter of the Donnelly and Littlefield characters but the role was little more than a walk-on. Originally, her name was not listed in the credits but only included years later 

==Cast==
*Kay Francis as Tanya Borodoff
*Ricardo Cortez as Tony Evans
*Warner Oland as Nick
*Lyle Talbot as Dr. Gregory Burton
*Lucien Littlefield as George Peters
*Ruth Donnelly as Mrs. George Peters
*Reginald Owen as Police Commissioner
*   

==Production==
The lead roles were initially offered to   

==Reception==
Although the critics did not see the film as anything better than a good "B-movie", it was well-received nonetheless, and was a moneymaker for the studio. 
===Box Office===
According to Warner Bros records the film made a profit of $83,462. 
==References==
Notes
 

Bibliography
*  
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 