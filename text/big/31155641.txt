Mynatharuvi Kolakase
{{Infobox film 
| name           = Mynatharuvi Kolakase
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = MC Appan
| screenplay     = Sathyan Sheela Hari
| music          = V. Dakshinamoorthy
| cinematography =
| editing        =
| studio         = Udaya
| distributor    = Udaya
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
  Sathyan
*Sheela
*Adoor Bhasi Hari
*Manavalan Joseph
*Jijo
*Adoor Pankajam
*Alummoodan
*Kaduvakulam Antony
*Kottarakkara Sreedharan Nair
*N. Govindankutty
*Nellikode Bhaskaran
*Pankajavalli
*S. P. Pillai
*Sunny
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Appanaane Ammayaane || Kamukara || Vayalar Ramavarma || 
|-
| 2 || Pallaathuruthi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Poy Varaam || P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 