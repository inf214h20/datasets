Scrooge (1935 film)
 
 
{{Infobox film
| name           = Scrooge
| image          = Scrooge1935_icon.jpg
| caption        = Film Title Frame Henry Edwards
| producer       = Julius Hagen
| writer         = H. Fowler Mear Charles Dickens (novel)
| based on       =   Sir Seymour Hicks Donald Calthrop Robert Cochran Mary Glynne Garry Marsh Oscar Asche Marie Ney C.V. France
| music          = W. L. Trytel
| cinematography = Sydney Blythe William Luff
| editing        = Ralph Kemplen
| distributor    = Twickenham Film Studios(United Kingdom)
Paramount Pictures(United States)
| released       =  
| runtime        = 78 minutes
                   63 minutes (edited version)
| country        = United Kingdom
| language       = English
| budget         =
}} British fantasy Henry Edwards Robert Cochran. silent film version. 

== Film ==
The 1935 film differs from all other versions of the story in one significant way – most of the ghosts, including that of Jacob Marley, are not actually shown onscreen, although their voices are heard. Only the Ghost of Christmas Present (Oscar Asche) is actually seen in full figure – the Ghost of Christmas Past is a mere shape with no discernible facial features, Marleys Ghost is seen only briefly as a face on the door knocker, and the Ghost of Christmas Yet to Come is just an outstretched pointing finger. 

Seymour Hicks plays both the old and young Scrooge. Albert Finney (in the 1970 film Scrooge (1970 film)|Scrooge) is the only other actor to play both young and old Scrooge in film.

The story is also severely truncated. Much time is spent at the beginning of the film – before any of the ghosts appear – setting up the atmosphere of rich and poor London. Scrooges sister Fan and Fezziwig are completely omitted from this version. 
 Tiny Tim 1999 Patrick Stewart version also contains this scene.
 Maurice Evans John Leechs illustrations of the character in the original 1843 edition of the novel.

Two versions of this film exist; each has a differently designed opening credits sequence, and one of the two versions omits the very last scenes. 

Also, copyright for this film was never renewed and therefore it is in the public domain and can be shown on multiple stations in a market. For years it was kept out of circulation, due to the extremely poor quality of most of the surviving prints.

== Cast == Sir Seymour Hicks –  Ebenezer Scrooge
* Donald Calthrop –  Bob Cratchit
* Robert Cochran –  Fred
* Mary Glynne – Belle
* Garry Marsh –  Belles husband Spirit of Christmas Present Spirit of Christmas Past Spirit of Christmas Future
* Athene Seyler –  Scrooges charwoman Maurice Evans –  Poor man Mary Lawson – Poor mans wife
* Barbara Everest –  Mrs. Cratchit
* Eve Gray –  Freds wife
* Morris Harvey –  Poulterer with Prize Turkey Tiny Tim
* D. J. Williams (actor)|D.J. Williams – Undertaker
* Margaret Yarde – Scrooges laundress
* Hugh E. Wright – Old Joe Charles Carson – Middlemark
* Hubert Harben – Worthington

==See also==
*List of ghost films

==References==

 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 