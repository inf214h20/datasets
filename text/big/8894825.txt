Nadan
 
{{Infobox film
| name               = Nadan
| image              = Nadan Free Poster.jpg
| alt                = 
| caption            =  Kamal
| producer           = Anilkumar Ambalakkara
| writer             = S. Suresh Babu
| starring           = Jayaram Remya Nambeesan Sajitha Madathil
| music              = Ouseppachan
| cinematography     = Madhu Neelakandan
| editing            = K.Rajagopal
| studio             = Ambalakkara Global Films
| released           = 
| country            = India
| language           = Malayalam
| budget             =
}}

Nadan (Malayalam : നടന്‍) is a 2013   and Remya Nambeeshan play the lead roles, while the rest of the cast are former theatre artists that include K. P. A. C. Lalitha, Joy Mathew, P. Balachandran, Hareesh Peradi and Sasi Kalinga.  It was produced by Anilkumar Ambalakkara under the banner of Ambalakkara Global Films.  Nadan features some famous scenes from the famous Kerala Peoples Arts Club (K P A C) dramas Ningalenne Communistakki and Mudiyanaya Puthran. 

==Plot==
Nadan is the story of a popular drama troupe owned by Devadas Sargavedi (Jayaram), which was previously possessed by his father and grandfather in the past, and speaks about the problems faced by the owner and his survival.  The revival of this mighty art through the sustained effort of the talented theatre artists forms the crux of the narrative.

==Cast==
* Jayaram as Devadas Sargavedi
* Remya Nambeesan as Jyothi
* Sajitha Madathil as Sudharma
* Joy Mathew as G. Krishnakumar
* K. P. A. C. Lalitha as Radhamani
* P. Balachandran as Vikraman Pilla
* Hareesh Peradi as KPAC Bharathan
* M. Mukundan as Babykuttan
* Malavika Menon as Priyamvada Devadas
* Chembil Ashokanas Abubacker
* Jayaraj Warrier as Kadavoor Manikandan
* Shankar Ramakrishnan
* Balaji Sarma
* Sudheer Karamana  
* Sasi Kalinga as Malakha Johnson
* Sunil Sukhada as Mapranam Pappachan
* Divya Prabha  as Haseena

==Soundtrack==
{{Infobox album 
| Name        = Nadan
| Type        = soundtrack
| Artist      = Ouseppachan
| Cover       =
| Background  = 
| Recorded    = Chetana Sound Studios, Thrissur
| Released    = November 2013
| Genre       = Soundtrack 
| Length      =  
| Label       = East Coast Audios
| Producer    = 
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
|}}
The songs of Nadan were composed by Ouseppachan and penned by Prabha Varma and Madhu Vasudevan.
{| class="wikitable"
|-
! Track !! Song Title !! Lyricist !! Singer(s)
|-
| 1 || Ethu Sundara || Prabha Varma || Shweta Mohan
|-
| 2 || Moolivarunna || Madhu Vasudevan || G. Sreeram, Mridula Warrier
|-
| 3 || Ottaykku Paadunna || Madhu Vasudevan || Vaikom Vijayalakshmi
|-
| 4 || Sargavedhikale || Madhu Vasudevan || Sharath, Praveen, Rahul R Nath, Lekshmi Priya
|-
| 5 || Ethu Sundara || Prabha Varma || Najim Arshad
|-
| 6 || Ottaykku Paadunna || Madhu Vasudevan || Nishad
|-
| 7 || Ottaykku Paadunna (Violin) || || Bhavya Lakshmi
|}

==Awards==
; Kerala State Film Awards Best Lyricist - Prabha Varma (for Ethu Sundara) and Madhu Vasudevan (for Ottaykku Paadunna) Best Music Director - Ouseppachan (for Ethu Sundara and Ottaykku Paadunna) Best Female Singer - Vaikom Vijayalakshmi (for Ottaykku Paadunna)

;Vayalar Film Awards 
* Best Film
* Best Actor - Jayaram
* Best Music Director - Ouseppachan
* Best Male Singer - G. Sreeram

==References==
 

==External links==
*  

 

 
 
 
 


 