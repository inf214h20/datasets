Sudigundalu
{{Infobox film
| name           = Sudigundalu
| image          =
| image_size     =
| caption        =
| director       = Adurthi Subba Rao
| producer       = Adurthi Subba Rao Akkineni Nageswara Rao
| writer         = N. R. Nandi (dialogues) Adurthi Subba Rao K. Vishwanath
| narrator       =
| starring       = Akkineni Nageswara Rao Vijayachander Ram Mohan Pushpavalli Sakshi Ranga Rao Sandhya Rani K. V. Chalam
| music          = K. V. Mahadevan
| cinematography = P. N. Selvaraj
| editing        = T. Krishna
| studio         =
| distributor    =Chakravarthi Chithra
| released       = 1968
| runtime        =
| country        = India Telugu
| budget         =
}}

Sudigundalu ( s): is a 1968 Telugu film|Telugu, drama film directed by Adurthi Subba Rao. The film has garnered the National Film Award for Best Feature Film in Telugu.     The film marked the debut of Nagarjuna Akkineni in a cameo  

==Plot==
Judge Chandrasekharam is very kind and helpful to the relatives of the culprits. His wife dies after giving birth to a baby boy. His son Raja was killed by two youth (Prasanna Rani and Vijayachander). The judge who now wears the pleaders shoes argues in the favour of the young culprits and shocks the whole audience on bringing out the real intentions of the murder.

Chandrasekharam questions the parents of both the youngsters and finally concludes the responsibility of parents in bringing up the children is the key to the future generation. Also he brings out the cultural deviations our country faces in the coming future. The film gives this message as an argument and at the end Chandrasekharam dies in the court hall.

==Credits==

===Cast===
*Akkineni Nageshwara Rao	...    Judge Chandrasekharam
* Bhanu Prakash  as Master Raja     ...     Son of Judge Chandrasekharam
* Sakshi Ranga Rao	...    Gumasta
* Gummadi Venkateswara Rao   ...   Defence lawyer
* Vijayachander (debut)
* Ram Mohan    ...    Police Inspector
* Pushpavalli
* Akkineni Nagarjuna      ... Cameo  (debut)   
* Sukanya
* Sandhya Rani	  ...    Housemaid Seetha
* K. V. Chalam
* Venkateswarlu
* P. Koteswara Rao
* Meena Kumari
* Anjaneyulu
* Prasanna Rani (debut)
* Mada    ...      Driver John Maridaiah (Uncredited)

===Crew===
* Directors: Adurthi Subba Rao and K. Viswanath
* Producer: D. Madhusudhana Rao
* Production company: Chakravarthi Chitra
* Dialogues: N. R. Nandi
* Screen adaptation: Adurthi Subba Rao and K. Vishwanath
* Original Music: K. V. Mahadevan
* Assistant Composer: Puhalendi
* Cinematography: P. N. Selvaraj
* Film Editing: T. Krishna
* Choreographer: Tara
* Playback singers: Ghantasala Venkateswara Rao and P. Susheela

==Songs==
* Vinara Sodara

==Awards== National Film Awards
*National Film Award for Best Feature Film in Telugu

;Nandi Awards
*Nandi Award for Best Feature Film

;Filmfare Awards South
*Filmfare Best Film Award (Telugu)

==References==
 

 

 

 
 
 
 
 
 
 
 
 
 
 