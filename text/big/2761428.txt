North Sea Hijack
 
 
{{Infobox film
| name           = North Sea Hijack
| image          = North_Sea_Hijack.jpg
| image size     = 
| alt            = 
| caption        = Original UK film poster
| director       = Andrew V. McLaglen
| producer       = Elliott Kastner Jack Davies
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Roger Moore James Mason Anthony Perkins Michael Parks
| music          = Michael J. Lewis
| cinematography = Tony Imi
| editing        = Alan Strachan
| sound          = Allan Sones
| studio         = Cinema Seven Productions    CIC
| released       = Autumn 1979 (UK),   Retrieved 2011-02-14   18 April 1980 (US) 
| runtime        = 100 min
| country        = United Kingdom 
| language       = English
| budget         = 
| gross          = $2,993,772 (US only) 
| preceded by    = 
| followed by    = 
}}
 action film 1979 starring Jack Davies from his novel Esther, Ruth and Jennifer. 
 playboy in The Saint, The Persuaders! and James Bond. In contrast to those parts, he is shown here as a bearded, eccentric, arrogant cat-loving, misogynist showing heroic qualities as a master Military strategy|strategist. Moore once said in an interview that he preferred this film to all the James Bond movies he had starred in while acknowledging that he had been miscast in the lead role. 

==Plot==
Misogynist freelance marine counter-terrorism consultant Rufus Excalibur ffolkes (Roger Moore) is asked by Lloyds of London to develop a contingency plan should any of the North Sea oil installations they insure be threatened.

Months later, Esther, a North Sea supply ship, takes on board a group of men posing as reporters who are visiting the oil production platform Jennifer. The leader of this group, Lou Kramer (Anthony Perkins), along with his second-in-command, Harold Shulman (Michael Parks), hijacks the ship, and two scuba diving henchmen attach limpet mines to the legs of Jennifer and its oil drilling rig, Ruth. From the bridge of Esther, Kramer issues a ransom demand for Pound sterling|£25 million or he will blow up Ruth; then, if the ransom is still not paid, he will destroy Jennifer. For good measure, he rigs Esther with explosives and has all the charges wired to a control panel that never leaves his side.

Lord Privy Seal Dennis Tipping (Jeremy Clyde) informs the British Prime Minister (Faith Brook) of the situation. The British government is opposed to yielding to terrorist blackmail, but Tipping suggests that, as a compromise, Lloyds of London|Lloyds could pay the ransom. After Lloyds is consulted, the Prime Minister is shown a video of ffolkes practising a rescue mission aboard a mock-up ship. He has anticipated that terrorists might hijack a supply ship and has worked out a plan. Flying out to Jennifer, ffolkes first proposes that, to buy time, a large explosion should light up the night sky, fooling Kramer into thinking Ruth has exploded by accident so that he wont push the button at the deadline. Ffolkes and Admiral Sir Francis Brindsen (James Mason) are to meet with Kramer on board Esther. Ffolkes makes Brindsen practice accidentally dropping cigarettes on the floor, the idea being that the admiral should distract Kramer, giving ffolkes the opportunity to kill him before he sets off the bombs; his team of commandos will in the meantime take out the guards posted on the vessel.

A sub-plot involves the imprisoned crew trying to poison their captors using the ships medicine supply. A reporter who came with Kramers men offers to do this, but the crew quickly suspects him to be a plant, so they tie him up. Unfortunately, Kramer has been spying on them, and when the food is delivered he forces one of the "conspirators" to drink the poisoned coffee; Sanna (Lea Brodie), the other main participant and the only woman on board, flees and apparently falls overboard.

Later, Kramer demands that Brinston and King (David Hedison), Jennifers manager, join him on Esther, unintentionally going along with ffolkes plan. However, Kramer doesnt trust ffolkes when he meets him and orders him to leave the ship. The reporter who got the blackmailers onto the ship gets cold feet and wants to leave, so Kramer agrees to release him. At the last moment, Kramer shoots him in the back as he is being winched aboard the helicopter.

With time running out, the Prime Minister considers paying the ransom, but ffolkes replies angrily that that would send a message that "anyone with a rowing boat and a stick of dynamite could hold this country to ransom." Ffolkes still thinks he can rescue the hostages. However, to save the lives of the 1,200 men and women aboard Jennifer, ffolkes urges that Esther be obliterated with a bomb if his team cannot rescue the hostages in time.

Ffolkes men storm Esther, bringing down the guards. Ffolkes joins them wearing a borrowed vermilion scuba suit, but is forced to throw overboard his second-in-command, who has mistaken him for a terrorist. Sanna, who had been hiding in a lifeboat, manages to take out one of the terrorists who tries to take a shot at ffolkes. Ffolkes races for the bridge as the helicopter carrying the bomb approaches. At the appointed time, Brindsen offers a cigarette to Kramer, drops them on the floor and bends down to pick them up. Ffolkes appears at the window and shoots the distracted Kramer with a spear gun, pinning him to his chair. Seeing armed men running by, Schulman races for the detonator switch, but he gets impaled at the controls with a spear in each side. Just as the Royal Navy helicopter drops the bomb down its rear loading-ramp, ffolkes fires his signal flare into the sky; the helicopter pilot desperately pulls away and the bomb narrowly misses Esther, falling harmlessly into the sea.

However, Kramer isnt quite dead, and he slowly reaches for the detonator.  Ffolkes pulls the wires out and watches him die.  Before he slumps over, Kramers last words are, "I&nbsp;— still&nbsp;— dont&nbsp;— like&nbsp;— your&nbsp;— face".

A ceremony is held at ffolkes castle to celebrate the end of the hijack. Among those present are the former hostages, the oil rig staff and the commandos. Ffolkes has expressed his disdain for medals, so the Prime Minister presents the cat-loving eccentric with a new litter of kittens, named Esther, Ruth, and Jennifer. For once moved, and a little at a loss for words, ffolkes leaves amidst a round of applause to give his new kittens a saucer of milk. 

==Cast==
* Roger Moore as Rufus Excalibur ffolkes (ffolkes is an old English name that is correctly spelled with two lower-case fs. In the book, he corrects other people several times on this point, see ffolkes baronets).
* James Mason as Admiral Brindsen
* Anthony Perkins as Lou Kramer
* Michael Parks as Harold Shulman
* David Hedison as Robert King Jack Watson as Captain Olafsen George Baker as Fletcher
* Jeremy Clyde as Lord Privy Seal Dennis Tipping David Wood as Herring
* Faith Brook as British Prime Minister
* Lea Brodie as Sanna
* Anthony Pullen Shaw as Robert F. Ackerman
* Philip OBrien as Art Webb John Westbrook as Dawnay  
* Jennifer Hilary as Sarah John Lee as Captain Harry Phillips
* Brook Williams as Helicopter Pilot

==Release==
===Titles===
The film was released as North Sea Hijack in the United Kingdom, but as Ffolkes in the United States and other English-speaking territories. In the United States it was renamed Assault Force when released on television in 1983.  

"The films had so many title changes Ive lost count," said Moore. "But everyone seems to like the character I play." MOORE--ON THE GO FROM GOA: MOORE ON THE GO
Los Angeles Times (1923-Current File)   29 Jan 1980: g1.  
===Critical===
The Guardian said "as pulp melodramas go, its quite fun". Anatomy of an American dream
The Guardian (1959-2003)   27 Mar 1980: 13.  

The Los Angeles Times called it "a vigorous but lacklustre high-seas adventure... there are lots more conferences than bravaura acts of daring... even so, it is ingenious and well crafted." FFOLKES: HHIGH SSEAS AADVENTURE
Thomas, Kevin. Los Angeles Times (1923-Current File)   18 Apr 1980: h6.  
===Box Office===
The film was a commercial disappointment. If a film chews gum, its American
The Guardian (1959-2003)   05 July 1980: 9.  

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 