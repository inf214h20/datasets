The Page Turner
{{Infobox film image              =Page turner.jpg caption            =United States theatrical poster name               =The Page Turner director           =Denis Dercourt producer           =Michel Saint-Jean writer             =Denis Dercourt starring           =Catherine Frot Déborah François Pascal Greggory music              =Jérôme Lemonnier cinematography     =Jérôme Peyrebrune editing            =François Gédigier distributor        =Tartan Films  (USA) | released           =  executive producer =Tom Dercourt runtime            =85 minutes country            =France language           =French
}}
The Page Turner ( ) is a 2006 French film directed by Denis Dercourt. It was screened in the Un Certain Regard section at the 2006 Cannes Film Festival.   

==Plot==

A young girl, Mélanie Prouvost, aspires to be a pianist and auditions in front of famous pianist Ariane Fouchécourt for a place at a conservatoire. Ariane signs an autograph for an admirer during the recital, distracting Mélanie and affecting her performance. She leaves the audition with her mother, heart broken.

Some years later Mélanie, having studied hard, finds a work experience placement at a solicitors. Perhaps coincidentally we find the husband of the famous pianist for whom she previously auditioned. The story develops as the young lady integrates herself into the life of the family, becoming a holiday carer for the young son who the family hopes will follow in the footsteps of the mother as a famous pianist. Befriending the boy, Melanie encourages him to prepare a full piano recital performance for the fathers return to the family home after a business trip. She also manages to become indispensable to Ariane, both practically and emotionally. Melanies perfectly timed page turning, combined with her composure and apparent empathy, enable Ariane to recover a confidence in performance that she thought she had lost after a traumatic car crash. 

A very close and intimate relationship is established between the two women with Mélanie becoming obsessed with Ariane in order to get revenge for the humiliation that she suffered as a child. She manages to seduce Ariane and then abandons her but twists the emotional knife by revealing the relationship to Arianes husband.

==Cast==
* Catherine Frot – Ariane Fouchécourt
* Déborah François – Mélanie Prouvost
* Pascal Greggory – Jean Fouchécourt
* Xavier De Guillebon – Laurent
* Christine Citti – madame Prouvost
* Clotilde Mollet – Virginie
* Jacques Bonnaffé – monsieur Prouvost
* Antoine Martynciow – Tristan Fouchécourt
* Julie Richalet – Mélanie Prouvost enfant
* Martine Chevallier – Jackie Onfray
* André Marcon – Werker
* Michèle Ernou – Monique

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 
 