Seven Guns for the MacGregors
{{Infobox film
| name           =Seven Guns for the MacGregors
| image          = 7-guns-for-the-macgregors-movie-poster-1967.jpg
| caption        =
| director       = Franco Giraldi
| producer       = Albert Band
| writer         = Enzo DellAquila Fernando Di Leo David Moreno Mingote Duccio Tessari Robert Woods Fernando Sancho
| music          = Ennio Morricone
| cinematography = Alejandro Ulloa
| editing        = Nino Baragli
| studio         = Estela Films Jolly Film
| distributor    = Columbia Pictures
| released       = 2 February 1966 (Italy)
| runtime        = 107 min.
| country        = Italy
| language       = English
| budget         =
}}
 1966 Cinema Italian spaghetti western. It is the directorial debut film of Franco Giraldi (here credited as Frank Garfield), who was Sergio Leones assistant in A Fistful of Dollars.    The film gained a great commercial success and generated an immediate sequel, Up the MacGregors!, again directed by Giraldi.   

==Plot== 
The MacGregors, horse ranchers of Scottish descent, are underway to the market when they are robbed of their horses by a gang under the helm of a corrupt sheriff. One of the brothers infiltrates the gang but his first attempt tries to play them backfires.  

== Cast == Robert Woods as Gregor MacGregor
* Fernando Sancho as Miguel
* Agata Flori as Rosita Carson
* Nazzareno Zamperla as Peter MacGregor
* Paolo Magalotti as Kenneth MacGregor
* Leo Anchóriz as Santillana
* Perla Cristal as Perla
* George Rigaud as Alastair MacGregor
* Manuel Zarzo as David MacGregor
* Alberto DellAcqua as Dick MacGregor (credited as Cole Kitosch)
* Julio Pérez Tabernero as Mark MacGregor
* Cris Huerta as Crawford
* Rafael Bardem as Justice Garland
* Víctor Israel as Trevor

==Bibliography==
*  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 