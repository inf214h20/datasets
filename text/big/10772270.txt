The Puppetoon Movie
{{Infobox Film |
  name           = The Puppetoon Movie|
  image          = PuppetoonMovie(2).jpg|
  caption        = DVD Cover|
  writer         = Arnold Leibovit |
  starring       = Dick Beals, Art Clokey, Paul Frees, Victor Jory, Jack Mercer, Pinto Colvig, Sam Edwards, Billy Bletcher, Dallas McKennon |
  director       = Arnold Leibovit |
  producer       = Arnold Leibovit|
  editing        = |
  movie_music    = |
  distributor    = |
  released       = June 12, 1987, DVD November 21, 2000|
  runtime        = 90 minutes|
  country        = USA|
  language       = English |
  music          = |
  budget         = |
}}
 Tubby the Tuba, Tulips Shall Grow, John Henry and the Inky Poo created by George Pal in the 1930s and 1940s, in addition to Gumby, Pokey (Gumby character)|Pokey, and Arnie the Dinosaur who host the framing story.   It stars the voices of Dick Beals, Art Clokey, Paul Frees, Victor Jory, and Dal McKennon.     as the main characters.

The original 1987 release of The Puppetoons Movie contained 11 Puppetoons. The 2000 DVD release and 2013 Blu-ray release each include the same 12 additional Puppetoons.   

==Plot==
The film opens on a film set, where Gumby and his friends are filming a dinosaur movie. A ferocious Tyrannosaurus Rex named Arnie charges on set and is about to devour a young doe named Barbara when suddenly, he removes his false teeth and lets the doe go out of sympathy. Gumby cuts the scene and questions Arnie on his hesitation to act ferocious. Although Arnie knows it is all just acting, he feels unfit for the part because it just isnt him. He explains that he once was a ferocious T-Rex, but thanks to the divine influence of George Pal, he has reformed and is now a vegetarian without a bad bone in his body. Gumby fails to understand the profound effect of George Pal on Arnies persona, so Arnie and Pokey show Gumby a set of George Pal shorts to show him the significance of the artist, thus starting The Puppetoon Movie.
 The Philips The Sleeping Tubby the Tuba.

After all the shorts, Gumby and the others meet other characters who George Pal animated, such as the Pillsbury Doughboy and the Alka-Seltzer mascot Speedy. Gumby then thanks George Pal for making all this possible, and everybody cheers. The screen pans out and shows a  Gremlins|gremlin, who looks at the audience, says "George Pal!" in a raspy voice, then climbs up a support beam while laughing hysterically.
 The Philips Broadcast of 1938, and The Ship of the Ether.

The 2013 Blu-ray also includes the following short films never released on home video: And to Think That I Saw It on Mulberry Street, 500 Hats of Bartholomew Cubbins, The Sky Princess, Rhapsody in Words, Date with Duke, Jasper and the Giant and Rhythm in the Ranks.

==Reception==

The movie received mixed reviews.          

==Awards==

George Pals Puppetoon body of work was recognized by a Special Oscar in 1943. Pal received the Special Oscar  "for the development of novel methods and techniques in the production of short subjects known as Puppetoons."    

==References==
 

==External links==
* 
*  from B2MP

 
 
 
 
 
 
 
 
 