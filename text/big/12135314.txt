Calle Mayor
{{Infobox Film
| name           = Calle Mayor
| image          = Calle mayor.JPG
| caption        = Spanish theatrical release poster
| director       = Juan Antonio Bardem
| producer       = Cesáreo González
| writer         = Juan Antonio Bardem
| starring       = Betsy Blair José Suárez
| music          = Isidro B. Maiztegui Joseph Kosma
| cinematography = Michel Kelber
| editing        = Margarita Ochoa
| distributor    = Suevia Films
| released       = 5 December 1956
| runtime        = 99 minutes
| country        = Spain
| language       = Spanish
| budget         =
}} Cuenca and Logroño. The film won the FRIPESCI Award at Venice Film Festival, and was an international success.
  of Calle Mayor]]

==Plot==
Isabel (Betsy Blair) is a good-natured and sensible spinster who lives in a small town with her widowed mother. At the age of 35, she is losing all hope of getting married and having children.

A bunch of bored middle-aged friends decides to play a trick on Isabel: Juan (José Suárez), the youngest and most handsome of them, will pretend to fall in love with her. As Isabel lives the courtship, full of hope and joy, Juan realizes too late the cruelty of the situation, but, pushed by his buddies, doesnt dare tell Isabel the truth.

When the day of the gala dance at the towns club comes, Isabel is still living her dream of love. She expects her engagement to be publicly announced from the stage, but Juan, desperate, tries to do anything to shy away from the muddle.

==Cast==
*Betsy Blair (dubbed into Spanish by Elsa Fábregas) as Isabel
*José Suárez as Juan
*Yves Massard as Federico
*Luis Peña as Luis
*Dora Doll as Toña
*Alfonso Godá as José María, Pepe el Calvo
*Manuel Alexandre as Amigote 1
*José Calvo as Amigote 2
*Matilde Muñoz Sampedro as Chacha
*René Blancard as Editor
*Lila Kedrova

==Additional remarks==
Similarities between Calle Mayor story and environment and Federico Fellinis I Vitelloni have been pointed out. 
Calle Mayor was Blairs first performance outside the US, and she played her role brilliantly (which bore a rather close resemblance to her character in her previous success, Marty (film)|Marty). For Suárez, this was his most dramatically profound role, and it shot him momentarily to fame all across Europe.
 Communist Party of Spain (PCE); Bardem was a well-known member of PCE.
 Best Foreign Language Film at the 30th Academy Awards, but was not accepted as a nominee. 

==Sequel==
Seven years after Calle Mayor, Bardem wrote and directed Nunca pasa nada (Nothing ever happens), which depicts an environment and characters similar to those in Calle Mayor, to the point that some critics nicknamed it disdainfully Calle Menor (Minor street).

==See also==
* List of submissions to the 30th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 