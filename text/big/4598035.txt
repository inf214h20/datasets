Year of the Dogs
 
{{Infobox film |
 name = Year of the Dogs |
 image = YearoftheDogs.jpg|
 director = Michael Cordell, Stewart Young |
 producer = Michael Cordell, Chris Hilton |
 writer = documentary |
 starring = Alan Joyce Tony Liberatore Stephen Wallis Terry Wallace Chris Grant Pat Hodgson Jenny Hodgson Peter Gordon |
 distributor =  |
 released = 1997 (Australia) |
 country = Australia |
 runtime = 86 minutes |
 language = English |
 budget =  |
}}
 documentary detailing Western Bulldogs).

==History==
Filmed during the turbulent 1996 season where they finished second-last (15th) in the Australian Football League, the documentary follows the club and two dedicated fans, Pat and Jenny Hogson. The Bulldogs have a horror run of losses; senior coach Alan Joyce is fired and replaced by Terry Wallace. The Dogs continue to struggle under this change in coaching administration, and continue to lose games.
 AFL CEO Ross Oakley is insistent that smaller AFL clubs must merge and the Dogs are under pressure to amalgamate with other clubs. After the Melbourne Hawks merger failure, the clubs members vote strongly against any proposed merger. In the end a taskforce of businessmen and former players take over the club to ensure its future. The club president (Peter Gordon) resigns.

In addition to this, youngest player Shaun Baxter is fighting cancer and the club veteran, Steven Wallis, knows his playing days are numbered.

For the last game for the season, Footscray play Essendon Football Club|Essendon. Wanting to give their retiring player, Wallis a good send off, they play hard and put up a good performance, but end up losing narrowly in a closely played game.
 Fitzroy (although Fitzroy were the club that did suffer that fate, being merged with Brisbane Bears|Brisbane). After Wallaces performance as caretaker, he is signed on for the 1997 season.

==Miscellanea== Grand Final, losing to eventual premiers   by just two points. AFI award for "Best Editing in a Non-Feature Film" (Stewart Young).

==Box Office==
Year of the Dogs grossed $199,191 at the box office in Australia. 

==See also==
*Aussie Rules the World
*Australian rules football
*Western Bulldogs

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 