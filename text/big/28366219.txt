Drömkåken
{{Infobox film
| name           = Drömkåken
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Peter Dalle
| producer       = Christer Abrahamsen
| writer         = Peter Dalle Bengt Palmers
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Björn Skifs Suzanne Reuter Pierre Lindstedt
| music          = Bengt Palmers
| cinematography = Esa Vuorinen
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Swedish comedy film directed by Peter Dalle.

==Cast==
*Björn Skifs as Göran
*Suzanne Reuter as Tina
*Zara Zetterqvist as Petra
*Mikael Håck as Anton
*Lena Nyman as Sanna
*Jan Malmsjö as The major
*Pierre Lindstedt as Ernst
*Gunnel Fred as Karin
*Pontus Gustafsson as Robert
*Johan Ulveson as Fille
*Claes Månsson as Mats
*Peter Dalle as Thomas
*Anders Ekborg as Plumber
*Johan Paulsen as Building worker in bathroom
*Sven-Åke Wahlström as Carpenter in kitchen
*Johan Rabaeus as Martin
*Anna-Lena Hemström as Malou
*Magnus Mark as Janne
*Robert Gustafsson as Asthma-patient
*Gunvor Pontén as Parrots voice
*Hans Alfredson as Himself
*Tommy Körberg as Himself

==External links==
* 
* 

 
 
 
 


 
 