Country Music: The Spirit of America
{{Infobox film
| name           = Country Music: The Spirit of America
| image          = OurCountryPoster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Steven Goldmann Keith Melton Tom Neff
| producer       = Randy Scruggs Tom Neff
| writer         = Tom Neff
| narrator       = Hal Holbrook
| starring       = Emily Lalande
| music          = Randy Scruggs
| cinematography = Steven D. Smith Rodney Taylor
| editing        = Barry Rubinow
| distributor    = IMAX Giant Screen Films
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Country Music: The Spirit of America is a 2003 documentary film, in the IMAX format, written and co-produced by Tom Neff and co-directed by Neff, Steven Goldmann and Keith Melton. Randy Scruggs was also a producer on the film and wrote the music score. The film traces the history of the United States in the 20th Century through country music, and is also known as Our Country. 

==Cast==
* Emily Lalande as Time Traveling Sprite
* Austin Stout as Austin
* Hannah Swanson as Time Traveling Sprite
* Benton Jennings as Comic Old West Cowboy
* Tommy Barnes as Stage Manager
* Terry Ike Clanton as Crazed Prisoner
* Tony Nudo as Joe, man at the train station
* Jaclynn Tiffany Brown as Fresh Faced Teen

===Interviews and music performers===
 
 
* Trace Adkins Alabama
* Jessica Andrews
* Béla Fleck
* Guy Clark
* Charlie Daniels
* Joe Diffie
* Vince Gill
* Billy Gilman
* Hal Holbrook as narrator
* Alan Jackson
* Alison Krauss
* Lyle Lovett
* Loretta Lynn
* Kathy Mattea
* Martina McBride
 
* Roger McGuinn
* Leigh Nash
* Randy Owen
* Dolly Parton
* Minnie Pearl - archival
* Earl Scruggs
* Marty Stuart
* Pam Tillis
* Ernest Tubb - archival
* Porter Wagoner
* Lee Ann Womack
 

==Reception==
===Critical response===
When the film was released, Jane Sumner, film critic for  , the documentary celebrates country music as a mirror of the American experience across 90 years ... Vintage photos, archival news footage (including a shot of O.J. Simpson trying on that pesky glove) and Mr. Neffs intelligent, lyrical commentary, narrated by Hal "Deep Throat" Holbrook, trace the history of country music as it parallels the nations." 

==See also==
*  
* List of IMAX films

==References==
 

==External links==
*   official web site
*  
*   interview at Big Movie Zone

 

 
 
 
 
 
 
 
 
 
 
 