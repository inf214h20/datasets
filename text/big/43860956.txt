Cool Breeze (film)
{{Infobox film | name = Cool Breeze
 | image = File:Cool_Breeze_Film_Poster.jpg
 | caption = Original Theatrical Poster
 | director = Barry Pollack
 | producer = Gene Corman
 | screenplay = Barry Pollack
 | based on =   Jim Watkins Judy Pace Lincoln Kilpatrick Raymond St. Jacques
 | music = Solomon Burke
 | cinematography =Andy David
 | editing =Morton Tubor
 | distributor = Metro-Goldwyn-Mayer
 | released = July 2, 1972   
 | runtime = 101 minutes   
 | country = United States
 | budget = 
 | language = English
 | gross = 
}}
Cool Breeze is a 1972 American blaxploitation film directed by Barry Pollack, and released by Metro-Goldwyn-Mayer.  Cool Breeze is loosely based on W. R. Burnetts 1949 novel, and is a remake of the 1950 film The Asphalt Jungle with a predominately all-black cast. http://www.tcm.com/tcmdb/title/3219/Cool-Breeze/ 

==Plot== convicted felon San Quentin. Pacific Coast. 
 launder illegal the Box Christian minister, the Driver’) an honest business man and half brother#half|half-brother of Travis. Unfortunately, after the successful robbery, the group finds themselves caught up in a string of unhappy accidents and double crosses.

==Cast==
* Thalmus Rasulala as Sidney Lord Jones Jim Watkins as Travis Battle
* Judy Pace as Obalese Eaton
* Lincoln Kilpatrick as Lt. Brian Knowles
* Sam Laws as "Stretch" Finian
*Raymond St. Jacques as Bill Mercer
* Margaret Avery as Lark Pamela Grier as Mona Paula Kelly as Martha Harris
* Wally Taylor as John Battle
* Rudy Challenger as Reverend Roy Harris
* Stewart Bradley as Cpt. Lloyd Harmon
* Ed Cambridge as The Bus Driver
* Royce Wallace as Emma Mercer
* Stack Pierce as Tinker
* Biff Elliot as Lt. Carl Mager

==See also==
* The Asphalt Jungle Hit Man - subsequent 1972 heist film released by MGM
* The Slams - subsequent 1973 heist film released by MGM
* List of blaxploitation films

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 