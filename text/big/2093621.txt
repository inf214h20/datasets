Road House (1948 film)
For the 1989 film, see Road House (1989 film).
{{Infobox film
| name           = Road House
| image          = roadhouse1948.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jean Negulesco
| producer       = Edward Chodorov
| screenplay     = Edward Chodorov
| story          = Margaret Gruen Oscar Saul
| starring       = Ida Lupino Cornel Wilde Celeste Holm Richard Widmark O.Z. Whitehead
| music          = Cyril J. Mockridge
| cinematography = Joseph LaShelle Norbert Brodine James B. Clark
| distributor    = 20th Century Fox
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Road House is a 1948 film noir drama directed by Jean Negulesco, with cinematography by Joseph LaShelle.  The picture features Ida Lupino, Cornel Wilde, Celeste Holm, Richard Widmark, among others. 

The drama tells the story of Lily Stevens (Lupino) who takes a job as a singer at a roadhouse—complete with bowling alley. When Lily dumps the owner Jefty (Widmark) for his boyhood friend Pete Morgan (Wilde), problems begin. They only get worse when Jefty is rejected after proposing to Lily, causing Jefty to go on a murderous rage.

Lupino sings the classic Johnny Mercer song "One for My Baby (and One More for the Road)" in the film.  The song "Again (1949 song)|Again", written by Dorcas Cochran (words) and Lionel Newman (music), debuted in this film, and was also sung by her.

==Plot==
Pete Morgan manages Jeftys Road House for his longtime friend, Jefferson "Jefty" Robbins, who inherited the place from his father. Jefty is attracted to Lily Stevens, his new singer from Chicago, but Pete thinks she is just another in a long string of girls he will eventually have to send on her way. Jefty, however, is convinced that Lily is different, even though she is playing hard-to-get.

Although Pete tries to pay Lily off and put her on a train, she is not about to leave and makes a successful debut at the club, accompanying herself on piano. Jefty asks Pete to teach Lily how to bowl in the roadhouses alley but she shows little interest in the sport and quite a bit more in Pete.

Susie Smith, the clubs cashier who is fond of Pete, becomes jealous of Lily. Before Jefty leaves on a hunting trip, he tells Lily that she is not like any other girl he has ever met. Lily tries to join Pete for a boat ride on a lake, but he refuses as she is Jeftys girl. Lily contradicts that notion, so Pete arranges to pick her up later. Susie also goes along, although the womens friendship is decidedly frosty. Later, Pete comes to Lilys rescue when a drunk causes a scene at the club.

Lily and Pete share a passionate kiss. Pete loves her, and it is obvious she feels the same way. Their idyll is interrupted when Jefty returns and shows Pete a marriage license he has obtained in his and Lilys names. When Jefty is told Pete and Lily are planning to be married, Jefty throws him out. Pete leaves a note stating that he and Lily are leaving and that he has taken $600 owed to him.

At the railroad station, two policemen detain Pete and Lili. Jefty claims that the entire weeks receipts have been taken from the roadhouses safe, but Pete insists he took only $600. After Susie states that the receipts totaled $2,600, Pete is held for trial and Lily accuses Jefty of framing him.

Pete is tried and found guilty of grand larceny. Before sentencing, Jefty talks to the judge in private and persuades him to parole Pete into his custody. The judge announces that Pete will be on probation for two years, but will have his job back and will be obligated to repay Jefty from his paycheck. Pete and Lily realize that Jefty has them trapped.

Jefty plans a trip to his hunting cabin. Pete wants to cross the Canadian border, which is only fifteen miles from the cabin, but Lily refuses to go along. At his cabin, Jefty taunts Pete and Lily while fooling around with a rifle. Lily accuses Jefty of taking the missing money, so Jefty hits her. Pete retaliates by knocking him out. Lily decides that she will go with Pete to Canada, and they set off on foot through the woods. Susie, meanwhile, discovers a deposit envelope for the receipts in Jeftys coat pocket, proof of Petes innocence and Jeftys false testimony. She gives the envelope to Pete, but is shot in the leg by a pursuing Jefty.

In the fog-enshrouded lakeside, Pete cranks up the motor on a boat and sends it off empty. After Jefty wastes bullets shooting at the boat, Pete tries to grab his gun. Lily gets possession of it and shoots Jefty when he threatens to hit her with a rock. As Jefty dies, he reminds Pete that he once told him that Lily was different. Dawn breaks as Pete, Lily and Susie (in Petes arms) head out of the woods and back to civilization, leaving the audience with no idea whether Pete is devoted to Lily or to Susie, as the camera holds on Lily, alone in both close and long shots, while Pete and Susie depart out of the cameras frame, to a subdued arrangement of Lionel Newmans "Again".

==Cast==
* Ida Lupino as Lily Stevens
* Cornel Wilde as Pete Morgan
* Celeste Holm as Susie Smith
* Richard Widmark as Jefty Robbins
* O.Z. Whitehead as Arthur
* Robert Karnes as Mike
* George Beranger as Lefty Ian MacDonald as Police captain

==Reception==

===Critical response===
Writer Spencer Selby calls the film an "interesting melodrama that has a crisp forties look and slowly builds to a noirish climax." 

Film critic Blake Lucas says the film "impresses first of all with its sharp dialogue exchanges between the characters and the bizarre look of the interiors" referring to the at once modern and rustic road house. 

==References==
 

==External links==
*  
*  
*  
*   film clip at Turner Classic Movies (Ida Lupino sings)
*  

 

 
 
 
 
 
 
 
 
 