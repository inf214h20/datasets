Joanna Francesa
{{Infobox film
| name           = Joanna Francesa
| image          = Joanna Francesa.jpg
| caption        = Thetrical release poster
| director       = Carlos Diegues
| producer       = Carlos Alberto Prates Correia
| writer         = Carlos Diegues
| starring       = Jeanne Moreau Eliezer Gomes Carlos Kroeber  Ney Santanna
| cinematography = Dib Lutfi
| editing        = Eduardo Escorel
| music          = Chico Buarque Roberto Menescal
| studio         = Zoom Cinematográfica
| distributor    = Ipanema Filmes
| released       =  
| runtime        = 110 minutes
| country        = France   Brazil
| language       = Portuguese
| budget         = 
}}

Joanna Francesa is a 1973 Cinema of France|French-Brazilian romantic drama film directed by Carlos Diegues and starring Jeanne Moreau, Eliezer Gomes and Carlos Kroeber.  In the 1930s, Joanna, the owner of a brothel in São Paulo, goes to Alagoas and falls in love with a customer. 

==Cast==
* Jeanne Moreau - Joanna
* Eliezer Gomes - Gismundo
* Carlos Kroeber - Aureliano
* Ney Santanna - Honório
* Tetê Maciel - Dorinha
* Helber Rangel - Lianinho
* Beto Leão - Ricardo
* Lélia Abramo - Olímpia
* Leina Krespi - Das Dores
* Pierre Cardin - Pierre

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 