Tanganyika (film)
{{Infobox film
| name           = Tanganyika
| image          = Tanganyika (movie poster).jpg
| image_size     =
| caption        = Poster
| director       = André De Toth
| producer       = Albert J. Cohen
| writer         = William Robert Cox (story) William Sackeim (writer)
| starring       = Van Heflin
| cinematography = Maury Gertsman Al Clark	 	
| distributor    = Universal-International
| released       =  
| runtime        = 91 mins.
| country        = United States English
}} 1954 action action adventure film directed by André De Toth and starring Van Heflin.

==Plot synopsis==
In 1903  ), former teacher Peggy (Ruth Roman), and two kids. Gale’s motives however are nothing to do with justice or even the charms of Peggy; he hopes to stake a claim on a valuable piece of land. Dan also has hidden motives for coming along; and the Nukumbi are lying in wait. Eventually Gale and McCracken meet in man-to-man combat.

==Cast==
*Van Heflin — John Gale
*Ruth Roman — Peggy Merion
*Howard Duff — Dan Harder McCrecken
*Jeff Morrow — Abel McCracken
*Joe Comadore — Andolo
*Noreen Corcoran — Sally Marion
*Gregory Marshall — Andy Marion
*Naaman Brown — Mukumbi prisoner
*Edward C. Short — Head porter

==External links==
* 
* 

 

 
 
 
 
 
 
 

 