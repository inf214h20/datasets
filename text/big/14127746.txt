Luke Rides Roughshod
 
{{Infobox film
| name           = Luke Rides Roughshod
| image          = Luke Rides Roughshod.jpg
| caption        = Film poster
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd - Lonesome Luke
* Snub Pollard - (as Harry Pollard)
* Bebe Daniels
* Billy Fay
* Fred C. Newmeyer
* A.H. Frahlich
* Sammy Brooks
* Eva Thatcher - (as Evelyn Thatcher)
* Rose Mendel
* Ben Corday
* Dee Lampton
* C.A. Self
* L.A. Gregor
* Ray Wyatt
* C. Spikeman
* Minna Browne
* F. Ward
* L. McCormack
* H. Minneshaw
* Clifford Silsby
* H. Granger Charles Stevenson - (as Charles E. Stevenson)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 