Angachamayam
{{Infobox film 
| name           = Angachamayam
| image          =
| caption        =
| director       = Rajaji Babu
| producer       = Babu Jose
| writer         = Babu Jose Mankombu Gopalakrishnan (dialogues)
| screenplay     = Rajaji Babu Swapna Jose Jose T. R. Omana
| music          = G. Devarajan
| cinematography = Lekshman Gore
| editing        = K Sankunni
| studio         = Jose International
| distributor   = Jose International
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Jose and T. R. Omana in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir as Public Prosecutor Jayadevan Swapna as Malla Jose
*T. R. Omana as Jayadevans Mother
*Prathapachandran as Tribal Leader
*Sathaar as Baby
*Anjali Naidu as Gayathri
*Balan K Nair as Robert
*Jaffer Khan as Chandrahassan Ravikumar as Forest Officer

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ilam Pennin || P Jayachandran || Mankombu Gopalakrishnan || 
|-
| 2 || Manjurukum || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 3 || Then churathi || P. Madhuri || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 