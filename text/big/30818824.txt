Lawless (film)
{{Infobox film
| name           = Lawless
| image          = Lawless ver4.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = John Hillcoat
| producer       = {{Plainlist|
*Lucy Fisher
*Douglas Wick
*Megan Ellison
*Michael Benaroya }}
| screenplay     = Nick Cave
| based on       =  
| starring       = {{Plainlist|
*Shia LaBeouf
*Tom Hardy
*Gary Oldman
*Mia Wasikowska
*Jessica Chastain Jason Clarke
*Guy Pearce
}}
| music          = {{Plainlist|
*Nick Cave Warren Ellis }}
| cinematography = Benoît Delhomme
| editing        = Dylan Tichenor Red Wagon Revolt Films BlumHansonAllen Films
| distributor    = The Weinstein Company FilmNation Entertainment
| released       =   }}
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $26 million
| gross          = $53,676,580 
}} crime drama Jason Clarke, and Guy Pearce.

Lawless explores the actions of three brothers: Forrest (Hardy), Howard (Clarke), and Jack Bondurant (LaBeouf), who made and sold moonshine in Franklin County, Virginia, during Prohibition in the United States|Prohibition. The film was in development for about three years before it was produced. It screened at the 2012 Cannes Film Festival and was theatrically released on August 29, 2012.

==Plot== Franklin County, in the Virginia Piedmont. Their friend Cricket Pate assists them in their endeavors. They use their gas station as a front for their illegal activities. One day, Jack witnesses mobster Floyd Banner shooting a competitor, and they exchange looks.

Jack returns to the station, where Forrest hires Maggie Beauford, a dancer from Chicago, to be their new waitress (for liquor sales). Shortly afterward, the gas station is visited by newly arrived Special Deputy Charley Rakes, on behalf of the Virginia Commonwealth Attorney Mason Wardell. He tells Forrest that he wants a cut of all profit made by the countys bootleggers.  Forrest refuses and threatens to kill Rakes if he returns. Forrest later meets with the other bootleggers and convinces them to stand up to Rakes as well, though they eventually give in to the deputys intimidation.
 Brethren preacher. sacred harp hymns) and makes a fool of himself, causing Berthas father to forbid her from seeing him, but this makes her more interested in Jack. When he later finds Rakes raiding Crickets house in search of his distillation equipment, the deputy brutally beats Jack to send a message to his brothers and other bootleggers. Forrest tells Jack that he needs to learn how to fight for himself. Forrest and Howard arrange to meet with potential "clients" from Chicago, but Howard gets drunk with a friend and misses the appointment. When the two mobsters harass Maggie, Forrest and Cricket beat them up. Later, after Cricket leaves, the men return, slash Forrests throat, and rape Maggie.

While Forrest recovers at a hospital, Jack decides to travel across the county line with Cricket to sell their remaining liquor. There, they are double-crossed by their clients but are rescued by Floyd Banner, who recognizes Jack from Virginia. Banner already knows of the attack on Forrest, and the identities of his two assailants; he provides Jack with their address and advises him that the pair are working for Rakes.

Forrest and Howard later find, torture, and kill the men, both in retaliation and to send a piece of them as a message to Rakes. Banner becomes a regular client of the brothers, who move their distillation equipment to the woods and earn great profits. Jack continues to court Bertha. Maggie initially decides to leave, but Forrest convinces her to stay, letting her use a spare room. They soon begin a romantic relationship. Eventually Jack decides to show Bertha the distillation center, but they are followed and ambushed by Rakes and his men. Howard and Jack flee from Rakess men with Bertha and Cricket. Cricket is later captured and murdered by Rakes.

After Crickets funeral, the sheriff of Franklin County comes to the Bondurant house to warn them that Rakes and his men are blockading the bridge out of the town. Jack takes off in Crickets old car to confront Rakes. Howard alerts Forrest of this and persuades him to get in the car and follow him to provide back up for Jack, though Maggie tries to dissuade him, saying that she found him when wounded and took him to the hospital. Forrest realizes then that she was also attacked that night, though Maggie attempts to deny it. The bootleggers engage Rakes men in a firefight, during which Rakes shoots Forrest several times before being shot in the leg and attempting to escape.  Jack and Howard confront Rakes, shooting him in the chest with a gun, and fatally stabbing him in the back, avenging Crickets death.

With Rakes and his men dead, the Bondurants decide to save their money and retire after Prohibition ends. By November 1940, Jack has married Bertha, Forrest has married Maggie, and Howard has married a local woman.  During a reunion at Jacks house, Forrest walks to a frozen lake and falls into the freezing water, and later dies from pneumonia.

== Cast ==
 
 
*Shia LaBeouf as Jack Bondurant
*Tom Hardy as Forrest Bondurant Jason Clarke as Howard Bondurant
*Jessica Chastain as Maggie Beauford
*Guy Pearce as Charlie Rakes
*Mia Wasikowska as Bertha Minnix
*Dane DeHaan as Cricket Pate
*Gary Oldman as Floyd Banner
*Lew Temple as Deputy Henry Abshire
*Chris McGarry as Danny
*Tim Tolin as Mason Wardell
*Marcus Hester as Deputy Jeff Richards
*Bill Camp as Sheriff Hodges
*Alex Van as Tizwell Minnix
*Noah Taylor as Gummy Walsh
 

== Production == bootlegging activities of his grandfather Jack Bondurant and his grand-uncles Forrest and Howard.  Producers Douglas Wick and Lucy Fisher optioned the book in 2008 and sent it to director John Hillcoat.     Hillcoat later commented, 
 "  sort of drew   into this crazy kind of world of corruption and lawlessness ironically, but then mostly they survived, they got through it all and actually went on to have businesses and children. And traditionally the gangster film teaches us that weve got to pay for our sins. Usually the gangster is shot down in a blaze of glory and doesnt get up again."  Hillcoat and screenwriter Nick Cave, who had worked together on the Western film The Proposition (2005), were attracted to the story by the success of the Bondurants.  Hillcoat also said, "we also loved the idea that it sort of touched on the whole immortality that a lot of these guys start to feel when they do survive so many strange experiences."   

The first actor to be cast was Shia LaBeouf as Jack, the youngest Bondurant brother.  James Franco was attached to play Howard and Ryan Gosling was attached to play Forrest; Amy Adams and Scarlett Johansson were also attached to the project.   Originally titled The Wettest County in The World like the book, the films title was changed to The Promised Land.    Although Hillcoat intended to begin shooting in February 2010,  in January the project was reported to have fallen apart due to financing problems.   Only LaBeouf remained with the project.  He said that after he saw Bronson (film)|Bronson (2008), "I went home and wrote Tom   a letter saying I was a fan. He sent me a script, and I sent him Lawless. He called me back and said, This is fucking amazing.    Cinematographer Benoît Delhomme recommended Jessica Chastain to Hillcoat.  Chastain said, "I am a big fan of The Proposition. I hadnt even read the script, but I told  , If you cast me, Ill do it. I approach every role in terms of: Have I done this before? Is it something Im repeating? Lawless offered a new opportunity." 
 Jason Clarke and Dane DeHaan were cast in January 2011.    Guy Pearce, Gary Oldman, and Mia Wasikowska joined the cast in February 2011. 

According to Cave, "a lot of the truly brutal stuff did not make it through into the film. In the book, you get lulled by the beautiful lyricism of the writing, then suddenly you are slapped in the face by a graphic description of a killing. I tried to be true to that as much as I could."     He also said the filmmakers "tried to stay as true to the original story as possible", adding "we kind of changed aspects of the personality and temperament of Rakes to get   involved."     Before Pearces casting, "Rakes, the character Rakes, was very much like the character in the book. He was a nasty country cop. We made him a city cop, gave him his disturbed sexuality and all the rest of it," Cave said.     Pearce created the hairstyle worn by Rakes in the film. 
 Carroll Countys Peachtree City for three months during production,  and Hillcoat screened dailies for the cast every weekend.     Hillcoat and Delhomme consulted with cinematographers Roger Deakins and Harris Savides on digital cinematography.  They chose to use the Arri Alexa digital camera system for Lawless, and Delhomme always used two cameras during filming. 

In March 2011, Momentum Pictures and its parent company Alliance Films acquired the U.K. and Canadian distribution rights.  In May 2011, the Weinstein Company bought the U.S. distribution rights, with plans for a wide release.  In March 2012, the title was changed to Lawless. 
 Warren Ellis.    Cave said
 

==Release==
Lawless screened In Competition at the 2012 Cannes Film Festival on May 19 and received a nearly 10-minute standing ovation.   The film was theatrically released in the U.S. on Wednesday, August 29, 2012, as The Weinstein Company hoped that good word of mouth would be built up for the upcoming Labor Day weekend.     Audiences polled by the market research firm CinemaScore gave Lawless a B+ grade on average. 

==Reception==

===Critical response===
Reviews of Lawless have been mostly positive. Rotten Tomatoes shows a "fresh" approval rating of 67% based on reviews from 201 critics, with the critical consensus "Grim, bloody, and utterly flawed, Lawless doesnt quite achieve the epic status it strains for, but its too beautifully filmed and powerfully acted to dismiss," and reports a rating average of 6.5 out of 10.   At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 58 based on 38 reviews. 

====Cannes====
David Rooney of   gave the film a B− grade, calling it "a thoroughly familiar—but flavorful and rousing—shoot-em-up set among Prohibition bootleggers.   If youve seen even a handful of Tommy-gun movies, however, everything that happens here will feel preordained".   Richard Corliss of   gave the film 2 stars out of 5, writing: "its basically a smug, empty exercise in macho-sentimental violence in which we are apparently expected to root for the lovable good ol boys, as they mumble, shoot, punch and stab. Our heroes manage to ensnare the affections of preposterously exquisite young women, and the final flurry of self-adoring nostalgia is borderline-nauseating." 

====Theatrical release====
Owen Gleiberman of   also praised Hardys performance, and concluded, "The filmmakers detail a long-gone conflict from a long-lost era and end up showing how the dreams and longings that motivate Americans never really change."   Peter Travers of   also gave the film 2.5 stars out of 4, writing:  "I can only admire this films craftsmanship and acting, and regret its failure to rise above them. Its characters live by a barbaric code that countenances murder. They live or die in a relentless hail of gunfire. Its not so much that the movie is too long, as that too many people must be killed before it can end."  

Claudia Puig of   felt that the film was clichéd, writing that it "turns the Virginia hills of the early 1930s into just another backdrop for a clockwork succession of perfunctorily filmed showdowns and shootouts."   A. O. Scott of The New York Times similarly wrote:  "There are too many action-movie clichés without enough dramatic purpose, and interesting themes and anecdotes are scattered around without being fully explored. This is weak and cloudy moonshine: it doesnt burn or intoxicate."  

===Accolades===
{| class="wikitable" style="font-size: 100%;"
|-
! Award
! Category
! Recipients and nominees
! Outcome
|- 65th Cannes Film Festival       Palme dOr John Hillcoat
| 
|- Georgia Film Critics Association Best Original Song
|"Cosmonaut" by Nick Cave & Warren Ellis
| 
|- Best Original Song
|"Fire in the Blood" by Nick Cave & Warren Ellis
| 
|- Oglethorpe Award for Excellence in Georgia Cinema Nick Cave & John Hillcoat
| 
|}

==Soundtrack==
A soundtrack for the film was released on August 28, 2012: 

{{Track listing
| extra_column    = Artist
| total_length    = 41:36

| title1          = Fire and Brimstone Fred Lincoln Wray Jr.
| extra1          = The Bootleggers feat. Mark Lanegan
| length1         = 4:27

| title2          = Burnin Hell
| writer2         = Bernard Besman / John Lee Hooker
| extra2          = The Bootleggers feat. Nick Cave
| length2         = 1:56

| title3          = Sure Nuff n Yes I Do
| writer3         = Herb Bermann / Don Van Vliet
| extra3          = Ralph Stanley
| length3         = 1:27

| title4          = Fire in the Blood
| writer4         = Nick Cave & Warren Ellis (musician)
| extra4          = The Bootleggers feat. Emmylou Harris
| length4         = 1:10
 White Light / White Heat
| extra5          = The Bootleggers feat. Mark Lanegan
| length5         = 4:24

| title6          = Cosmonaut
| extra6          = The Bootleggers feat. Emmylou Harris
| length6         = 3:42
 Snake Song
| writer7         = Nick Cave & Warren Ellis / Townes Van Zandt  Warren Ellis
| length7         = 4:25

| title8          = So Youll Aim toward the Sky
| extra8          = The Bootleggers feat. Emmylou Harris
| length8         = 5:57

| title9          = Fire in the Blood
| writer9         = Nick Cave & Warren Ellis
| extra9          = The Bootleggers feat. Emmylou Harris
| length9         = 1:06

| title10         = Fire and Brimstone
| extra10         = Ralph Stanley
| length10        = 2:12

| title11         = Sure Nuff n Yes I Do
| extra11         = The Bootleggers feat. Mark Lanegan
| length11        = 2:35

| title12         = White Light / White Heat
| extra12         = Ralph Stanley
| length12        = 1:38

| title13         = End Crawl
| extra13         = Nick Cave & Warren Ellis
| length13        = 4:00

| title14         = Midnight Run
| writer14        = Marc Copely / James Bernard Dolan / Adam Stuart Levy 
| extra14         = Willie Nelson
| length14        = 2:37
}}
<!--		
5 	White Light/White Heat Lou Reed The Bootleggers feat. Mark Lanegan 4:24
6 	Cosmonaut Nick Cave / Warren Ellis The Bootleggers feat. Emmylou Harris 3:42
7 	Fire In the Blood/Snake Song Nick Cave / Warren Ellis / Townes Van Zandt The Bootleggers feat. Emmylou Harris, Ralph Stanley 4:25
8 	So Youll Aim Toward the Sky Jason Lytle The Bootleggers feat. Emmylou Harris, Leila Moss 5:57
9 	Fire In the Blood Nick Cave / Warren Ellis The Bootleggers feat. Emmylou Harris 1:06
10 	Fire and Brimstone Fred Lincoln Wray Jr. The Bootleggers feat. Ralph Stanley 2:12
11 	Sure Nuff N Yes I Do Herb Bermann / Don Van Vliet The Bootleggers feat. Mark Lanegan 2:35
12 	White Light/White Heat Lou Reed Ralph Stanley 1:38
13 	End Crawl Nick Cave / Warren Ellis The Bootleggers 4:02
14 	Midnight Run Marc Copely / James Bernard Dolan / Adam Stuart Levy Willie Nelson 2:37
-->

== References ==
{{Reflist|2|refs=

   

   

   

   

   

   

   

   

    -->

   

   

   

   

   

<!-- Unused citation
   
-->
}}

==External links==
*  
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 