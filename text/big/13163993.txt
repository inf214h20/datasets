An Average Little Man
{{Infobox film
| name = An Average Little Man (Un borghese piccolo piccolo)
| image = Un borghese piccolo piccolo.jpg
| image size =
| caption = Italian film poster
| director = Mario Monicelli
| producer = Aurelio De Laurentiis Luigi De Laurentiis
| writer = Vincenzo Cerami Sergio Amidei Mario Monicelli
| narrator =
| starring = Alberto Sordi Shelley Winters Romolo Valli Vincenzo Crocitti
| music = Giancarlo Chiaramello
| cinematography = Mario Vulpiani
| editing = Ruggero Mastroianni
| distributor = Cineriz
| released =  
| runtime = 118 minutes
| country = Italy
| language = Italian
| budget =
}}
An Average Little Man ( , literally meaning a petty petty bourgeois, also known in English as A Very Little Man) is a 1977 Italian drama film directed by Mario Monicelli. It is based on the novel of the same name written by Vincenzo Cerami. The first hour is a fine example of commedia allitaliana but the second part is a psychological drama and a tragedy. The film was an entrant in the 1977 Cannes Film Festival.   

==Plot==
Giovanni Vivaldi (Alberto Sordi) is a petty bourgeois, modest white collar worker nearing retirement in a public office in the capital. His life is divided between work and family. With his wife (Shelley Winters) he shares high hopes for his son, Mario (Vincenzo Crocitti), a newly qualified accountant, not a particularly bright boy who willingly assists in the efforts which his father employs to make it in the same office.

The father, in an attempt to guide his son, emphasizes the point of practicing humility in the presence of his superiors at work, and he enrolled himself in a Masonic lodge to help him gain friendships and favoritisms that, at first, he would never hope to have.

Just as the attempts of Giovanni Vivaldi seems to turn to success, his son Mario is killed, hit by a stray bullet during a shootout that erupts following a robbery in which the father and son are accidentally involved.

Misfortune and sufferings consequently distort the lives, beliefs and morality of the Vivaldis. The wife of Giovanni becomes ill, loses her voice and becomes seriously handicapped. Giovanni, now blinded by grief and hatred, throws himself headlong into an isolated and desperate quest. He identifies his sons murderer, abducts him, takes him to a secluded cabin and submits him to torture and violence, eventually bringing the killer of his child to a slow death.

Then, for Giovanni arrives  - at his set date - his retirement and, only a day later, the  death of his wife who had by now been overcome by her disability.

Giovanni is now prepared with serenity and resignation to live into old age, but a spontaneous verbal confrontation with a young idler revives in him the role of an executioner who will, presumably, kill again.

==Cast==
* Alberto Sordi - Giovanni Vivaldi
* Shelley Winters - Amalia Vivaldi
* Vincenzo Crocitti - Mario Vivaldi
* Romolo Valli - Dr. Spaziani
* Renzo Carboni - Robber
* Enrico Beruschi - Toti
* Marcello Di Martire
* Francesco DAdda - (as Francesca DAdda Salvaterra)
* Edoardo Florio
* Ettore Garofolo - Young Man on Street

==Awards==
* 3 David di Donatello : Best Film, Best Director and Best Actor (Alberto Sordi).
* 4 Nastro dArgento :Best Actor, Best Script, Best New Actor (Vincenzo Crocitti), Best Supporting Actor (Romolo Valli) .

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 