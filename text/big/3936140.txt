Smokin' Aces
{{Infobox film
| name           = Smokin Aces
| image          = Smokin-poster2.jpg
| caption        = Theatrical release poster
| director       = Joe Carnahan
| producer       = Tim Bevan Eric Fellner
| writer         = Joe Carnahan
| starring       = Ben Affleck Andy García Alicia Keys Ray Liotta Jeremy Piven Ryan Reynolds 
| music          = Clint Mansell
| cinematography = Mauro Fiore
| editing        = Robert Frazen
| studio         = StudioCanal Relativity Media Blinding Edge Pictures Working Title Films Scion Films
| distributor    = Universal Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States United Kingdom France
| language       = English
| budget         = $17 million
| gross          = $57,103,895
}} Common as Matthew Fox. The film is set in Lake Tahoe and was mainly filmed at the MontBleu casino, called the "Nomad Casino" in the film.

==Plot==
  Las Vegas Agents Richard mob boss bounty on Tommy Flanagan), who specializes in disguises and impersonations; Sharice Watters (Taraji P. Henson) and Georgia Sykes (Alicia Keys), two hitwomen hired by Sparazzas underboss, Victor "Buzz" Padiche (David Proval); Pasquale Acosta (Nestor Carbonell), a calm torture expert and mercenary; and the psychotic neo-Nazi Tremor brothers, Darwin (Chris Pine), Jeeves (Kevin Durand), and Lester (Maury Sterling).
 bail bondsmen, Jack Dupree (Ben Affleck) and his partners, "Pistol" Pete Deeks (Peter Berg) and Hollis Elmore (Martin Henderson), have been hired by a sleazy lawyer that posted Israels bail, Rupert "Rip" Reed (Jason Bateman), to bring him into custody. The bondsmen are gunned down by the Tremors, but Elmore survives. Messner is dispatched to the murder scene while Carruthers proceeds to Israel. At the same time, each of the assassins gain access to the hotel in their own various ways.
 riot cuffs in the hallway. Georgia finds Carruthers and Acosta, both riddled with bullets and bleeding to death, in the elevator, but assumes Acosta is Soot. In Los Angeles Locke abruptly withdraws from the deal with Israel and orders that Messner and Carruthers are not told. The Tremor brothers reach the penthouse floor, where they engage in a shootout with the security team and Ivy, who manages to kill Jeeves and Lester. Israel, learning of the FBIs new position, attempts suicide by gunshot but passes out before he can.

Messner arrives at the hotel and sets up a position around Georgias elevator. Sharice provides cover from another high-rise hotel with a Barrett M82 anti-materiel rifle, outgunning the FBI agents. Acosta, still alive, shoots Georgia, but is shot by Carruthers. Sharice, thinking Georgia is dead, refuses to escape and keeps shooting at the FBI team. Georgia escapes to the penthouse where she stops Darwin Tremor before he can kill Ivy. Darwin Tremor escapes by posing as an FBI agent in stolen clothes, and Messner, distraught over the death of Carruthers, stops Ivy and Georgia on the stairwell, but decides to let them escape. Sharice, after seeing the pair alive and free through her rifle scope, is gunned down by the FBI from behind.

Locke and a team of FBI agents descend on the penthouse and take Israel to the hospital, while Soot escapes by dressing as a member of hotel security. Acosta, carted away on a gurney, is also shown to be alive. Darwin Tremor almost manages to escape, but is gunned down by Hollis Elmore on the roof of the casinos parking garage.

Messner arrives at the hospital and learns the truth about the days events from Locke at gunpoint. It transpires that the mysterious Swede is actually a prominent heart surgeon from the University of Stockholm and that Soot was hired by Sparazza to get Israels heart so it could be transplanted into the body of Sparazza. Sparazza is further revealed to be Freeman Heller (Mike Falkow), an FBI agent who went undercover and was thought to have been killed by the mob. The FBI had attempted to kill Heller, after they thought his assignment had blurred the lines between being a mobster or an FBI agent. But Heller miraculously survived and ended up taking on the role as Sparazza full-time after his mind snapped. The mobster has agreed to expose the mobs operations in exchange for Israels heart as he is in fact Sparazzas son, and thus, the most compatible donor.

Messner, furious over the unnecessary deaths, especially Carruthers, protests and is ordered by Locke to either resign on the spot or return to Washington, D.C., and forget about the case. Realizing that the FBI will never admit what they did, he walks into the emergency room, locks the door and pulls the plug on both men. He then lays his gun and badge on the floor while Locke and his men desperately try to break in, apparently resigning as an FBI agent.

==Cast==
*Jeremy Piven as Buddy "Aces" Israel
*Ryan Reynolds as Richard Messner
*Martin Henderson as Hollis Elmore: A former Las Vegas vice cop and the partner of Pistol Pete. 
*Ray Liotta as Donald Carruthers
*Andy Garcia as Stanley Locke, the FBI Deputy-Director.
*  and master of disguise who wears realistic latex masks. He agrees to kill Israels crew for a $1 million bounty, not Israel.
*Alicia Keys as Georgia Sykes: A female contract killer. She and her partner, Sharice Watters, are hired by Serna to abduct Israel from his hotel before Soot can kill him. Common as Sir Ivy: Israels head of security. crush on her partner. SAS custody to avoid identification by Interpol. He learns of the contract on Israel and tries to collect on it himself.
* , methamphetamine|speed-freak, Nazi punk|neo-Nazi punk hitmen with a penchant for scorched earth tactics. They get wind of the hit on Israel and decide to horn in on the action.
*Kevin Durand as Jeeves Tremor: The largest and youngest of the Tremor brothers.
*Maury Sterling as Lester Tremor: The smallest of the Tremor brothers.
* s and Substance dependence|addictions. His law firm bailed Israel out of jail, so he hires three bounty hunters to get him back before he officially jumps and they forfeit the bond.
*  who is hired to bring Buddy Israel back from Lake Tahoe and see that he makes his next court appearance.
*  . His real name is Sven Ingstrom. 
*  fired from the force for police corruption|corruption. He and his partner Hollis are hired by Dupree to help him recover Israel.
* an bodyguard and hanger-on. 
* . It is revealed in   that "Beanie" is actually an alias. His real name is Malcolm Little, and he is an undercover FBI agent.
*  at the Lake Tahoe hotel where much of the action occurs.

Wayne Newton cameos as himself along with Joe Carnahan, the films writer-director, who cameos as an armed robber at the beginning of the film.

==Production==

===Title sequence===
During the making on the film Joe Carnahans on set photographer captured thousands of stills. These stills (over 3000) were given to the London based studio VooDooDog who found sequential photographs that could be animated into title sequences. The images were then manipulated using After Effects giving control of camera movement and depth of field. The sequence takes inspirations from Butch Cassidy and the Sundance Kid and other 1970s movies. To give the rostrum type hand made feel, ink textures were filmed using a Canon 5D stills camera.

Originally two sequences were produced, an opening sequence and end sequence. However, only the end sequence was used.

"Yes, Joe liked the opening credits we did but after their edit they felt it slowed the momentum of the introduction. That seems to be a big concern for filmmakers now – they’re aware of the short attention span of audiences and don’t want to delay the story. As a designer, I am not sure I would agree, of course. I think that if credit sequences are good and entertaining, they can hold an audience’s attention." 

==Soundtrack==
The movie itself contains 18 songs,  leaving only one out of the official soundtrack which was "Spottieottiedopaliscious" by Outkast. The score music was composed by Clint Mansell who has also scored such movies as The Fountain and Requiem for a Dream.

===Track listing===
{{tracklist
| extra_column    = Artist
| title1          = First Warning
| extra1          = The Prodigy
| length1         = 4:21
| title2          = Big White Cloud
| extra2          = John Cale (U.S. release only)
| length2         = 4:05
| title3          = Ace of Spades
| extra3          = Motörhead
| length3         = 2:48
| title4          = Down on the Street
| extra4          = The Stooges
| length4         = 3:45
| title5          = Play Your Cards Right Common feat. Bilal
| length5         = 3:09
| title6          = Trespassing
| extra6          = Skull Snaps
| length6         = 4:00
| title7          = Segura o Sambura
| extra7          = Nilton Castro
| length7         = 2:55
| title8          = Touch Me Again
| extra8          = Bernard "Pretty" Purdie
| length8         = 4:23
| title9          = Under the Street Lamp
| extra9          = Joe Bataan
| length9         = 2:52
| title10         = I Gotcha Back
| extra10         = GZA
| length10        = 5:00
| title11         = I Love You
| extra11         = The Bees
| length11        = 4:33
| title12         = Morte di un Soldato
| extra12         = Ennio Morricone
| length12        = 3:12
| title13         = Save Yourself
| extra13         = The Make-Up
| length13        = 3:22
| title14         = Like Light to the Flies Trivium
| length14        = 5:43
| title15         = FBI
| extra15         = Clint Mansell
| length15        = 3:00
| title16         = Shell Shock
| extra16         = Clint Mansell
| length16        = 3:09
| title17         = Dead Reckoning
| extra17         = Clint Mansell
| length17        = 3:16
}}

==Release and reception==

===Box office===
According to Box Office Mojo, the movie grossed $14,638,755 on its opening weekend (2,218 theaters, averaging $6,599 per theater).
The movie grossed a total of $35,662,731 in the North American market and $18,878,474 outside the United States|U.S, making a total worldwide gross of $54,541,205.

===Critical reception=== Quentin Tarantinos style but not much of his wit or humor". Despite this, the film has gain some cult following with its viewers.

===Home media===
Smokin Aces was released on DVD on April 17, 2007 and sold 1,853,397 DVD units which produced a revenue of $35,714,831, or more than double the movies budget. 

==Prequel  ==
 

On July 17, 2007, director Joe Carnahan announced that production had been approved by Universal Pictures for a second Smokin Aces film, which he would not direct. The film is a prequel to the original and was released straight to DVD on January 19, 2010.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*   by Brian Holcomb at Cinema Blend
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 