Ankur (film)
{{Infobox film
| name           = Ankur अंकुर
| image          = Ankur film poster.gif
| caption        = Film poster
| director       = Shyam Benegal
| producer       = Lalit M. Bijlani, Freni Variava; Blaze Film Enterprises
| writer         = Shyam Benegal (Screenplay) Satyadev Dubey (Dialogue)
| starring       = Shabana Azmi Anant Nag Sadhu Meher Priya Tendulkar Kader Ali Beg Dalip Tahil
| music          = Vanraj Bhatia
| cinematography = Govind Nihalani Kamath Ghanekar 
| released       = 1974
| runtime        = 125&nbsp;minutes
| country        = India National Film Awards, 43 prizes in India and elsewhere Dakhani
}}
Ankur ( : اَنکُر,   of 1974. It was the first feature film directed by Shyam Benegal and the debut of Indian actors Shabana Azmi and Anant Nag. Anant Nag was introduced in Ankur by Shyam Benegal after his higher education in Mumbai. Though Shabana Azmi had acted in other films as well, Ankur was her first release. 

Like many of Benegals other films, Ankur belongs to the genre of Indian art films, or more precisely, Indian Parallel cinema. The plot is based on a true story that occurred in Hyderabad district (India)|Hyderabad, apparently in the 1950s. {{cite web
| publisher=Red Hot Country
| work=Bollywood Films
|url=http://www.redhotcurry.com/entertainment/films/bollywood/ankur.htm
| title=Ankur
}}
  It was filmed almost entirely on location.
 National Film Awards and 43 other prizes, both in India and abroad. It was nominated for the Golden Bear at the 24th Berlin International Film Festival.
 whipping scene and more profanity than is usually found in Indian films.

==Plot==
Ankur is a film that analyzes human behavior in general and heavily stresses characterization (though the story is not fictional). The story revolves around two characters, Lakshmi and Surya. Ankur is also there.
 alcoholic pottery|potter who communicates using gestures. At the beginning of the film, during a village festival, she claims (in a prayer to the village goddess) that her only desire is to have a child.
 mistress named illegitimate son sexually frustrated.

He is then forced to administer his share of land in the village. He is to stay in an old house, and Lakshmi and Kishtayya are to act as his servants. Not long after his arrival, he begins introducing a number of measures (often controversial). For example, on his second day in the village, Surya (who already finds Lakshmi attractive) has Lakshmi cook his meals and make tea. This disappoints the village priest, who is accustomed to delivering food to the landowner, though at a higher price than Lakshmi asks.
 flirt with Lakshmi but fails. In the meantime, the villagers gossip, and many (most notably the overseer, Police Patel Sheikh Chand) believe that Surya has already slept with Lakshmi and will treat her the same way the landlord treated Kaushalya: try to conceal the scandal by giving the mistress a plot of land.
 sacks her, claiming that she is too sick to work.

Many days later, Kishtayya returns, having cured himself of his alcoholism and made some money. Lakshmi is overwhelmed with a feeling of guilt (emotion)|guilt, because she believes that she has betrayed her husband. On discovering Lakshmis pregnancy, he salutes the village goddess at her temple. He then decides to try to ride the bullock cart again but carries a stick as he approaches Surya. Surya sees Kishtayya and mistakenly believes that Kishtayya is seeking revenge from him.
 whip him with a rope used for lynching. The commotion attracts others (including Sheikh Chand and Pratap) to the scene, and Lakshmi rushes to defend her husband. She angrily curses Surya, then slowly returns home with Kishtayya. In the final scene, after the others have left, a child throws a stone at Suryas glass window and runs away, and this is the Seedling. 

==Characters==
The plot is portrayed through the perspectives of Surya and Lakshmi. However, several other major characters and relatively minor characters also enhance the plot, each in his own way.

===Surya===
During his wedding, Surya glances first at Kaushalya and Pratap (who are sitting together), then at his own mother. Kaushalya is smiling because her sons wedding is to be held with Suryas, but Suryas mother appears relatively resigned. This is how he always saw marriage: the legitimate wife who suffers and the mistress who prospers.

Formerly an inhabitant of the city, he is not accustomed to the ways of the village. When Sheikh Chand expresses his hopes that the village will improve with Suryas presence, Suryas only reply is: "Yeah, well, they better." It is for this reason that Surya begins making changes as he deems fit.

In addition, Suryas father gives Pratap the "best" land in the village while Surya lives in an old house. As a result, Suryas second change in the village is to stop water from flowing into Prataps and Kaushalyas fields. Kaushalya requests a reason from Surya, who points out that he is not her son. She responds, "I think of you that way" - a remark that makes Surya even more angry.

At least two days later, Pratap asks Surya to restore the water flow into his fields. When Surya refuses, Pratap threatens to report Suryas actions to their father. Surya initially dismisses this warning but is surprised when his father eventually does show up. Surya tries to defend his actions but is unsuccessful in doing so.
 abort the child and refuses to take responsibility over the baby. She refuses because she always wanted a child, so Surya tells her to leave.

When Saru announces to Surya that she has caught Lakshmi stealing rice, he warns Lakshmi that he would have whipped her until she began bleeding if she were not a woman. Then he forbids her to come near his house again. Sheikh Chand requests Surya to consider Lakshmis situation more thoroughly. Surya refuses while nervously wiping his hands.

===Lakshmi===
Lakshmi readily serves Surya as a servant, albeit at a low wage. That is what she is expected to do. However, the villagers did not realize that Surya would find her attractive and ask her to do additional chores.

She often appears to be worried: first about Kishtayyas alcoholism, then about where he has gone and how to live without him, and finally, about her loyalty to him.

It is clear that Lakshmi had an extended affair with Surya which seems to have started when she places her head on Suryas shoulder and the scene ends abruptly. After that there are several occasions when we see her dressing up or her lying next to Surya in bed.

===Rajamma===
Two days after Kishtayyas departure, Lakshmi witnesses (from a distance) two men dragging a village woman to the panchayat. The woman, Rajamma, refuses to go while the men accuse her of "dishonouring our brother." At the panchayat, the men reveal that she has committed adultery.

Her brothers-in-law argue that they have "two wells" and "two crops a year," maintaining that Rajamma could not want more. Rajamma explains that she wants a child (Lakshmi immediately realizes the similarity to her own desires). "My not having a child," she claims, "is because of him, my husband." She threatens suicide if she is forced to live with her husband, Yadgiri. When asked what his argument is, Yadgiri simply allows the judges to make a decision.

Before Rajamma expresses her views, the judges reprimand her for disgracing "your house, your family, and your village" by living with another man. At this point, Surya looks at Lakshmi, who already lives with him. The final verdict is that Rajamma must live with her husband and that Yadgiris brothers should compensate if she is dissatisfied. After the trial, Rajamma commits suicide.

Lakshmi can identify closely with Rajamma. After Rajammas suicide, she tells Surya that she wishes to return to Kishtayya. However, she realizes that this is impossible, and Surya promises to look after her.

===Saru===
As soon as Saru arrives, she bows before Surya, and Lakshmi garlands her. She immediately begins adding decorations to the house, namely a frame with the message "Good Luck" (in English) and a picture from her wedding. She also takes off her garland, puts it around a framed picture of two Hindu deities, and prays to them. She knows about Suryas affair with Lakshmi, but she is puzzled when she sees Lakshmi cursing Surya since she does not know about Kishtayya.

Saru is rarely shown smiling until she sacks Lakshmi. In fact, the very first day, just as Surya is hanging up the wedding picture and Lakshmi is walking into her room, she points out Lakshmis presence by silently staring at him and nodding slightly towards Lakshmis room. When Surya tries to avoid discussion of the affair by criticizing how he looks in the picture, Saru does not speak to him and does not even allow him to touch her. That same night, she suggests that he sack Lakshmi; an unconcerned Surya tells her to do as she wishes.

Saru is a supporter of the caste system. When Lakshmi makes tea for the new couple, Surya accepts his glass. Saru, on the other hand, refuses hers before expressing her surprise at Surya (not realizing that he was the one who had Lakshmi make tea in the first place). She then refuses to have "anything she   has touched."

===Suryas Father===
If not for Suryas father, Surya would have been far less likely to meet Lakshmi, change the villages rhythm of life, and cause a revolt. However, he insists that Surya control his share of land. He does not believe in hiring others to administer the land for him because he thinks they will "get rich at our cost" and seize the land for themselves.

===Kaushalya===
Though she always smiles when speaking to Surya, Kaushalya does not like him very much. In his absence, she calls him a "spoiled brat" and a "mere boy." She always calls him her "son," as if the legitimate wife were nonexistent. She has become rich at Suryas mothers cost. Many, if not all, of these facts provoke Surya to try to put an end to her progress by cutting off the water supply.

===Pratap===
Pratap does not seem to consider Suryas feelings about Kaushalya very thoroughly. He also does not hesitate to enter his half-brothers house without permission. However, he is perhaps more enraged than any of the other villagers when he observes the injured Kishtayya at the end of the film. In that scene, he appears to stay and stare at Suryas window longer than any of the other villagers.

===Sheikh Chand===
Sheikh Chand is the Muslim overseer of the landlords property. When Surya arrives in the village, he is initially hopeful that the new arrival might improve the village. These hopes seem to disappear within a day: Surya monopolizes the toddy trade and demands that Sheikh Chand guard the toddy. Sheikh Chand promises to punish anyone who steals the toddy severely, but once Surya leaves, the expression on his face changes from a smile to a straight face.

===Kishtayya===
Kishtayya is strong both physically and mentally. It is because of his physical strength that Surya is afraid of him, and his mental strength is demonstrated by his ability to overcome alcoholism (facilities for alcoholics were generally not available in India during the 1950s, particularly not in villages). Surya, however, exploits Kishtayyas weaknesses (i.e. the fact that Kishtayya is deaf-mute, alcoholic, poor, and a Dalit). Kishtayyas alcoholism allows Surya to humiliate him so that Surya can be closer to Lakshmi. Kishtayyas physical handicap prevents him from understanding what is happening when Surya proceeds to beat him in the end, and his poverty and caste do not permit him to complain about Suryas actions.

In spite of his weaknesses, Kishtayya uses his job as a cart-driver to drive willing schoolchildren to their homes at the end of the day.

===The Boy===
One of the characters is an anonymous boy who first appears in the scene in which Kishtayya steals toddy. The boy is the one who reports the theft to Surya; from that day until the end of the film, they seem to be on friendly terms (just before Kishtayya is beaten, the boy is shown flying kites with Surya). However, at the end of the film, he suddenly turns against Surya by breaking his window.

==Motif of the seedling==
In addition to being the title of the film, the seedling makes various appearances (both physically and metaphorically) in the film and is used as a motif (literature)|motif.
* In the first scene, a village woman appears to offer the seedling of a fruit to the goddess while Lakshmi prays for a child. (As she offers the seedling, it seems that the first word she utters is pandlu which means "fruits" in Telugu.)
* The seedling may also represent the child that Lakshmi desires.
* Not long before Kishtayya is caught stealing toddy, there is a scene in which Lakshmi is cooking dinner. Suddenly, near the doorstep, she notices a pot containing a seedling. The implication is that Kishtayya has gone out to drink again and has left Lakshmi the seedling as compensation. She steps outside the doorstep, finds Kishtayya staggering home, and breaks the pot in front of him before returning inside.
* Metaphorically speaking, the seedling of popular rebellion sprouts at the end of the film (the villagers begin to protest the villages social hierarchy).

==Social issues==
Many reviewers suggest that Ankur makes a statement concerning one particular social issue. In reality, it addresses several, including (but not necessarily limited to) those listed below:
* Alcoholism: Kishtayya used to be a "good potter," Lakshmi tells Surya. However, demand for his clay pots became weak since aluminium vessels were becoming increasingly popular. As he could not sell many pots, he began to drown his sorrows in alcohol. Lakshmi claims that Kishtayya is a "good man" whose "only fault is drinking." Two scenes show Kishtayya returning home after a night of drinking while Lakshmi cooks dinner. In both scenes, a worried Lakshmi scolds her husband, trying to discourage him from drinking. Kishtayyas only response is to go to bed on a hungry stomach. He does not overcome his alcoholism until he abandons Lakshmi.
* Casteism: The film provides a deeper insight into the ugliness of Indian caste system, particularly visible in the rural areas.  The villagers expect Lakshmi to work as Suryas servant. However, being a Dalit, tradition forbids that she cook meals for Surya. Thus, when Surya asks Lakshmi to cook his meals, the villagers (particularly the Hindu greengrocer) begin to disapprove of him. When Saru moves to Suryas house, she refuses to touch "anything that she (Lakshmi) has touched."
* Rich vs. Poor: The first time Lakshmi is shown scolding Kishtayya, she claims that she is obliged to commit petty theft in order to care for herself and her husband. She initially steals no more than three handfuls a day of rice from Surya. Some time after she is dismissed from her job, she returns to Suryas house to look for work again. Saru offers Lakshmi food instead of work, and Lakshmi attempts to steal a little more rice than usual (as she is pregnant). Saru catches her red-handed as she brings a meal, then forces her to put back the rice, saying, "You people starve because you steal." In the end, Lakshmi refuses "your   jobs, your money, anything of yours!" thus suggesting that poverty does not concern her in this context.
* Parent vs. Child: The relationship between Surya and his father appears to be rather unsteady; neither of them smiles when they are together. Surya tries to spend more time with his friends by asking his father for permission to study for a Bachelor of Arts degree. However, his father (who already knows what Surya is trying to do) refuses him permission and forces him to marry Saru. Little does Suryas father initially know what the consequences of these actions of his will be.
* Sexual drive: Surya, Lakshmi, and Rajamma have all engaged in adultery. Each has his own reasons. Surya is sexually frustrated, and Rajamma wants a child. Lakshmis reasons are unclear, for Ankur does not reveal when her affair with Surya began. (See the Unanswered Questions section of this article.)
* Changing loyalties: Saru is perhaps the only character who does not change her loyalty to something (in her case, tradition). Surya pretends to be loyal to Lakshmi but abandons her once she becomes pregnant. Lakshmi is loyal to her husband until she sleeps with Surya. We do not know whether Kishtayya has remained loyal to his wife in his absence (though it seems improbable considering his general respect for Lakshmi, adultery on his part might explain why he forgives her). Certainly, he has abandoned his loyalty to alcohol but remains supportive of his wife.
* Religious differences: This less predominant issue characterizes the relationship between two of the minor characters, namely Sheikh Chand and the greengrocer. Their religious differences encourages them to play nonviolent practical jokes on one another. The greengrocer tricks Sheikh Chand into getting Suryas car out of the mud; later, Sheikh Chand reciprocates by stealing a few betel leaves from the greengrocer.
* Dowry: This issue is a relatively minor one in the film. It is addressed only in one quote, when Lakshmi explains why she married Kishtayya. After Surya asks why Lakshmi chose to marry a "drunken deaf-mute," she answers that no one else would marry her as she could not afford to pay dowry. She then points out that Kishtayya was not yet a drunkard.

==Irony==
* The attitudes of Suryas mother and Lakshmi towards their husbands are ironic in different ways. Both are fiercely defensive of their husbands and forbid Surya to say anything negative about them. Yet, Suryas mother has suffered as the landlord prefers Kaushalya to his legitimate wife, and Lakshmi continues to defend Kishtayya after having an affair with Surya.
* Surya repeatedly criticizes Kishtayya for drinking. However, on the night of   does not succeed in carrying away Swamis wife).

==Foreshadowing== foreshadow parts of the main plot. Examples are provided below.
* After his unsuccessful attempt to flirt with Lakshmi, Surya walks to his water well when suddenly he notices a snake approaching him. He is so scared that instead of running away, he stands still out of shock while calling Lakshmi for help, thus making it easier for the snake to bite him. Lakshmi saves his life by chasing the snake back to its hole, but Surya shows no gratitude. This act of cowardice in a small scene foreshadows the final scene, in which Surya runs back into his house on seeing Lakshmi, emotionally unable to confront her.
* The first time that Kishtayya is shown returning home drunk, he attempts to engage in sexual intercourse with Lakshmi after she scolds him. She resists at first but soon gives in. The same happens, though on a larger scale, between Lakshmi and Surya after Kishtayya leaves the village.

==Unanswered questions==
Although the plot of Ankur is very detailed, it does withhold enough information to leave some questions unanswered.
* Did Surya always oppose the caste system, or did he simply adopt this belief to be closer to Lakshmi? 
* When did Lakshmi and Surya begin their affair? Was it before or after Kishtayya left the village?
* Sheikh Chand discovers Lakshmi stealing his maize but tries to dissuade her, saying that Surya will grant land to her as his father did to Kaushalya. In the next scene, he tries unsuccessfully to convince Surya to look after Lakshmi. How much time elapsed between these two scenes? Did Sheikh Chand truly believe that Lakshmi would benefit, or was he simply trying to console her?
* Why, in fact, did Lakshmi decide to have an affair? Was it for fear of losing her job and only source of income? (Satyajit Ray, on whom Benegal later made a documentary film, had directed a film called Pratidwandi four years before the making of this film. In Pratidwandi, the main characters sister "Topu" or Sutapa has an affair with her boss for this reason.)

==Production==
The characters in Ankur often speak the Dakhani language, a variant of Standard Hindi-Urdu spoken in Southern India (particularly in the Hyderabad area). For example, when Surya asks Lakshmi where Kishtayya is, she responds, "Mereku naheeN maaluum" in Dakhani instead of "Mujhe naheeN maaluum" (I dont know) in Standard Hindi. (See Muslim culture of Hyderabad for more examples of Dakhani).

Shabana Azmi, a fresh graduate from Film and Television Institute of India, Pune (FTII), wasnt the first choice for the role of Lakshmi, Benegal had earlier approached, actress, Waheeda Rehman, Anju Mahendru and Sharada (actress)|Sharada, all of whom had refused his offer. Thereafter, he chose Shabana Azmi, there again, he had to alter the script a bit to suit, the younger looking Lakshmi. 

Benegal was initially reluctant to hire Shabana Azmi, thinking she was a model and perhaps unsuitable for the role of a humble villager.

==Music==
Being an Indian art film, Ankur is a "straight" feature without musical sequences. However, Surya plays parts of two records over the course of the film. The first recording consists of the third stanza of the song "Yahii To Hai Woh" from Solvan Saal (1958). The fourth stanza is then played in the background while Surya talks to Lakshmi.

The film also includes several scenes in which villagers sing folk songs, mostly in Telugu.

==Cast==
* Anant Nag - Surya
* Mirza Qadir Ali Baig - Suryas Father
* Prafullata Natu - Suryas Mother
* Priya Tendulkar - Saru
* Shabana Azmi - Lakshmi
* Sadhu Meher - Kishtaya
* Dalip Tahil

==Reception==
The film was both commercial and critical success, as films producer, Lalit M. Bijlani, who produced the film for just five lakhs rupees, went on to make one crore with its release. 
 Time Out reviewer, the film, "recalled the modest realism of Satyajit Ray,  and as a recent reviewer, put "Shyam Benegal creates a sublime and provocative examination of hypocrisy, economic disparity, and the social status of women in Ankur. 

==Awards==
* 1975  
* 1975  
* 1975  
* 1974:  : Nominated 

==See also==
* Shyam Benegal
* Muslim culture of Hyderabad (for more examples of Dakhani)

==References==
 

==External links==

*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 