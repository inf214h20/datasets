Alleycat Rock: Female Boss
{{Infobox Film
| name = Alleycat Rock: Female Boss
| image = Alleycat Rock Female Boss.jpg
| image_size = 
| caption = Theatrical poster for Alleycat Rock: Female Boss (1970)
| director = Yasuharu Hasebe   
| producer = 
| writer = Hideichi Nagahara
| narrator = 
| starring = Akiko Wada Meiko Kaji
| music = Kunihiko Suzuki
| cinematography = Muneo Ueda
| editing = Akira Suzuki
| distributor = Nikkatsu
| released =   
| runtime = 80 minutes 
| country = Japan
| language = Japanese
| budget = 
| gross =  Hori Kikaku 
}}

  aka Stray Cat Rock: Delinquent Girl Boss, Female Juvenile Delinquent Leader: Alleycat Rock, Wildcat Rock  is a 1970 Japanese  ,  ,   and Alleycat Rock: Crazy Riders 71. 

==Plot==
Tough girl biker Ako (pop singer Akiko Wada) comes across Mei (Meiko Kaji) and her girl gang (the Alleycats/Stray Cats) as they are about to have a knife fight in Shinjuku, Tokyo with another gang of girls. When the second gang calls in their boyfriends for help, Ako joins in and turns the tide for Mei and her gang and becomes a leader figure for the girls. Meanwhile, Meis boyfriend Michio (Kōji Wada) wants to join some right-wing nationalists, the Seiyu Group. To prove himself, he induces an old friend Kelly (Ken Sanders) to throw a boxing match so the Seiyu Group can cash in betting against him. But when the boxer, encouraged by Ako and Mei, wins the fight, the Seiyu Group takes their anger out on Michio until Mei and the Alleycats rescue him. But Mei and the girls are now on the run from the powerful group. Mei is eventually killed and Ako leaves Shinjuku, roaring away on her bike. 

==Cast==
* Akiko Wada as Ako 
* Meiko Kaji as Mei
* Kōji Wada ( ) as Michio Yagami
* Bunjaku Han as Yuriko
* Yuka Kemari ( ) as Mari
* Hanako Tokachi ( ) as Hanako
* Yūko Shimazu ( ) as Yūko
* Yuka Ōhashi (  as Yuka
* Miki Yanagi ( ) as Miki
* Toshimitsu Shima (  as Maabō
* George Fujita ( ) as Hiroshi 
* Ken Sanders ( ) as Kelly Fujiyama
* Tatsuya Fuji as Katsuya
* Yōsui Inoue  (as Andre Candre) ( )

== Background ==
Alleycat Rock: Female Boss was designed by Nikkatsu to compete with Toei Company|Toeis Delinquent Boss series, which, in turn, had been inspired by Roger Cormans early outlaw biker film, The Wild Angels (1966). Weisser p. 41  Nikkatsu also meant the film to showcase the popular singer Akiko Wada, and to appeal to her young audience. Co-star Meiko Kaji, however, attracted the most audience attention, and she became the star of the remaining episodes in the Alleycat Rock series. Hasebe, Yasuharu. (1998). Interviewed by Thomas and Yuko Mihara Weisser in Tokyo, 1999, in Asian Cult Cinema, #25, 4th Quarter, 1999, p.34.  Nikkatsu regarded Alleycat Rock: Female Boss as a prototype for a new direction for the studio and its success ensured the studios move towards youth-oriented action films. 

Director Hasebe and cult screenwriter-director Atsushi Yamatoya wrote the script to Alleycat Rock: Female Boss. Because of the films low budget, the studio gave Hasebe and Yamatoya more creative freedom than was generally the case for Nikkatsus staff at this time.  Of the distinctive look of Alleycat Rock: Female Boss, Hasebe recalled, "I tried to infuse those movies with the culture of the time. I spent a lot of time visiting places where people hung out. At the time, protest songs were popular, so I included them in the soundtrack. I remember, one day I noticed a big fuss near the west entrance of Shinjuku station. Activists were gathering and protesting against the US-Japanese Security Treaty. These people were like the hippies in the States. I found them interesting. Cinematic. I wanted my film to be this modern." 

==Critical appraisal==
The Weissers, in their Japanese Cinema Encyclopedia: The Sex Films, judge Alleycat Rock: Female Boss to be better than Toei Company|Toeis Delinquent Boss series, with which it was meant to compete, and call the series, "a prime example of sexually oriented-action movies, five excellent entries over a two year period".  The style of the series, according to the Weissers, is "Ultra-chic, yet surprisingly grim".  Allmovie writes that Alleycat Rock: Female Boss is "Good-looking and fast-paced".   

==Availability==
Alleycat Rock: Female Boss was released theatrically in Japan on May 2, 1970.  It was released on DVD on December 8, 2006. 

==Bibliography==

===English===
*  
* Hasebe, Yasuharu. (1998). Interviewed by Thomas and Yuko Mihara Weisser in Tokyo, 1999, in Asian Cult Cinema, #25, 4th Quarter, 1999, p.&nbsp;32-42.
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 
 

 
 
 
 
 
 