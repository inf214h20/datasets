Two Flags West
{{Infobox film
| name           = Two Flags West
| image          = Two Flags West VideoCover.png
| caption        =
| director       = Robert Wise
| producer       = Casey Robinson
| screenplay     = Casey Robinson
| story          = Frank S. Nugent Curtis Kenyon Jeff Chandler Cornel Wilde Alfred Newman Orchestration Earle Hagen Maurice de Packh Composer  Julia Ward Howe Daniel Decatur Emmett William Steffe
| cinematography = Leon Shamroy
| editing        = Louis Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 min.
| country        = United States
| awards         = English
| budget         =
}}
 1950 Western Western drama Jeff Chandler, Linda Darnell, and Cornell Wilde. The opening credits contain the following statement: 

 On December 8th, 1863, President Abraham Lincoln issued a Special Proclamation, whereby Confederate Prisoners of War might gain their freedom, provided they would join the Union Army to defend the frontier West against the Indians.     
 Georgia veterans desert at the first opportunity. The fulfillment of that expectation is challenged by an attack on the fort itself by Kiowa.
 Rocky Mountain The Last Outpost (1951), Escape from Fort Bravo (1953), and Revolt at Fort Laramie 
(1957). 

==Synopsis== Confederate 5th Union prison western frontier, undermanned because its able-bodied regulars have been sent east, leaving only "greenhorns or casualties"  like Bradford to fight Indians. Although promised that they will not be compelled to fight against their own, many of the Georgians resist the offer. Putting the decision to a vote, the issue is deadlocked when the last soldier dies before he can choose. Compassion for his men, and Bradfords sincerity, compels their reluctant commander, Col. Clay Tucker (Joseph Cotten) to break the tie by agreeing to the conditions offered.
 3rd Cavalry. Jeff Chandler), is stern and provocative. The bitter Kenniston has a limp from a wound that relegated him to Fort Thorn early in the war. Tucker, now a lieutenant in the Union Army, dines with Kenniston, his widowed sister-in-law Elena (Linda Darnell) and civilian guests, and is put on edge by their patronizing comments. Tension becomes high when Tucker reveals that he led the cavalry charge that killed Elenas husband. Elena has been stranded for months at the fort on her way home, and is uneasy with her brother-in-laws protectiveness, suspecting that he believes himself to be his late brothers surrogate.

Friction quickly develops between the Northern and Southern soldiers. When Tuckers men try to pursue a band of Indians but are ordered to stop, they mock the order as Yankee irresoluteness. Kenniston rebukes them, warning them they had been riding into an ambush. He assigns the Georgians to execute two civilians convicted of gunrunning, but when informed that they were actually Confederate agents, Tucker objects as a violation of their enlistment agreement, to no avail. He begins plotting to desert the command, which Kenniston shrewdly deduces. On the rationalization that he does not want "enemies" in his ranks, he assigns Tuckers troop to escort a wagon train across hostile territory, knowing Tucker will deliver it safely before deserting. Elena escapes by concealing herself in a wagon, which Tucker discovers but allows to continue. Ephraim Strong (Harry Von Zell), a civilian in the train, reveals himself as a Confederate agent and enlists Tucker in a plan to link California with the South. He persuades Tucker to return to Fort Thorn and to take back Elena to gain Kennistons confidence. While surprised by Tuckers actions, Kenniston continues to be wary of him. 
 march to the sea, spelling doom for the Confederacy. Elena tries to comfort a despairing Tucker with the hope that things will seem better tomorrow.

==Cast==
* Joseph Cotten as Col. Clay Tucker
* Linda Darnell as Elena Kenniston Jeff Chandler as Maj. Henry Kenniston
* Cornel Wilde as Capt. Mark Bradford
* Dale Robertson as Lem
* Jay C. Flippen as Sgt. Duffy
* Noah Beery Jr. as Corp. Cy Davis (as Noah Beery)
* Harry von Zell as Ephraim Strong
* Johnny Sands as Lt. Adams (as John Sands)
* Arthur Hunnicutt as Sgt. Pickens

===Casting notes===
Fox had originally intended the role of "Col. Clay Tucker" to be played by either Victor Mature or Richard Basehart, but Joseph Cotten was cast at the last minute, loaned to Fox by Selznick International Pictures.

==Production==
===Locations=== Black Mesa, Tewa inhabitants agreed to use of their community, some of whose buildings dated back 400 years, when director Robert Wise promised that filming would remain clear of the tribal kiva (underground council room), cemetery, and sacred shrines. 

===Historical basis=== Dee Brown Fort Laramie, Wyoming in 1864.  They were also the only former Confederate cavalrymen (originally part of Morgans Raiders) to see service as "Galvanized Yankees" on the Western frontier.

The historical Fort Thorn was built in December 1853 on the west bank of the Rio Grande,   north of Las Cruces, New Mexico (near present-day Hatch, New Mexico|Hatch) to defend local settlements against raids by Apache Indians, primarily those of the Mescalero band. Fort Thorn became the eastern terminus of a road built in 1856 across Arizona from Fort Yuma until 1860, when the post closed as a permanent garrison. 
 Arizona Territory. Mesilla at the other end of the valley. Confederate forces occupied the site in January 1862 to stage for an advance north, but in April were forced to withdraw from New Mexico. Fort Thorn again became a Union post on July 4, 1862. Keleher, William A. (1951, 1982). Turmoil in New Mexico. Rydal Press, ISBN 0-8263-0632-2, p. 271, note 58.  
 3rd Infantry Regiment of 3rd Cavalry 5th Infantry.  In September 1861, Captain Robert M. Morris defeated a force of Texans neat Fort Thorn with Companies C, G and K of the 3rd Cavalry. Detachments of the 3rd Cavalry continued to operate from Fort Thorn, as depicted, until September 1862, when that regiment was sent east to fight against the Confederacy. Although Fort Thorn was likely not occupied after that time, the 5th Infantry remained in New Mexico throughout the Civil War, and in theory its forces could have been augmented by "Galvanized Yankees".
 breakout at the Camp Douglas prison camp.  A detachment of Company H of the 5th U.S.V.I. escorting a wagon train in June 1865 is the only known unit of Galvanized Yankees to venture into New Mexico. 

==Reception==
Two Flags West opened October 14, 1950, at the Rivoli Theater in New York City, to a favorable review from New York Times critic Bosley Crowther.     

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 