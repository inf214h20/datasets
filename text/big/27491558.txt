Country Town
 
 
{{Infobox film name           = Country Town image          =  caption        =  producer       = Fenton Rosewarne director       = Peter Maxwell writer         = Barbara Vernon starring       = Terry McDermott music          = Bruce Clarke cinematography = Bruce McNaughton editing        = Raymond Daley studio = Outback Films Avargo Productions distributor    =  Gary Gray Terry McDermott released       = 19 June 1971 runtime        = 106 minutes country        = Australia language       = English  budget         = $85,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p257 
}} Gary Gray and Lynette Curran.  It was a film version of the Australian television series Bellbird (television series)|Bellbird, written by Barbara Vernon. 

==Plot==
A severe drought strikes the town of Bellbird. Young reporter Philip Henderson arrives and stirs old tensions. The locals rally together and hold a fund-raising gymkhana.

==Cast==
* Fenton Rosewarne (Producer)
* Terry McDermott  – Max Pearson Gary Gray – David Emerson
* Lynette Curran – Rhoda Wilson
* Gerard Maguire – Philip Henderson
* Sue Parsons – Jean Fowler
* Carl Bleazby – Jim Emerson
* Maurie Fields – John Quinney
* Carmel Millhouse – Marge Bacon
* Brian Anderson – Stan Bacon
* Margaret Cruickshank – Doctor Liz
* Mark Albiston – Bob Wright
* Kirsty Child – Julie
* Frank Rich  – Giorgio Lini
* Rosie Sturgess – Anna Maria Lini
* Kurt Ludescher – Grossark

==Production==
The film was made in January 1971 during a break in production from filming the TV series. Although most of the regular cast were involved and the script was written by Barbara Vernon, who was one of the main writers on the show,  the ABC was not formally involved in production. The movie was the idea of two regular cast members, Gary Gray and Terry McDermott who formed a production company, Avargo, with Fenton Rosewarne, an ABC film editor, and Rod Barnett, a chartered accountant. English director Peter Maxwell, who had extensive experience of working in Australia, was hired to direct. 
 Yea in Australian Film Development Corporation provided $15,000 to help prepare 35&nbsp;mm prints.

==Release==
The film was distributed by Gray and McDermott themselves, released first in country areas then reaching Sydney in 1973. 

==References==
 

==External links==
* 
*  at Oz Movies

 
 
 
 
 


 