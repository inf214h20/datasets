You Must Get Married
{{Infobox film
| name           = You Must Get Married
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = A. Leslie Pearce
| producer       = Basil Humphreys
| writer         = Richard Fisher F. McGrew Willis
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{flatlist|
* Frances Day Neil Hamilton
* Robertson Hare
}}
| music          = 
| cinematography = Claude Friese-Greene
| editing        = 
| studio         = Walton Studios
| distributor    = City Film Corporation
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy Neil Hamilton and Robertson Hare.  In order to be able to work in Britain an American actress marries a British sailor. It was based on a novel by David Evans.

==Cast==
* Frances Day - Fenella Dane Neil Hamilton - Michael Brown
* Robertson Hare - Percy Phut
* Wally Patch - Chief Blow
* Gus McNaughton - Bosun
* Fred Duprez - Cyrus P. Hankin
* Dennis Wyndham - Albert Gull
* C. Denier Warren - Mr Wurtsell
* James Carew - Mr Schillinger

==References==
 

==External links==
* 

 
 
 
 
 


 