September Dawn
 
{{Infobox film
| name           = September Dawn
| image          = Septemberdawnmp.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christopher Cain
| producer       = Christopher Cain Scott Duthie Kevin Matossian
| writer         = Christopher Cain Carole Whang Schutter
| starring       = Jon Voight Trent Ford Tamara Hope  William Ross
| cinematography = Juan Ruiz-Anchia
| editing        = Jack Hofstra
| studio         = Voice Pictures
| distributor    = Slow Hand Releasing Black Diamond Pictures
| released       =  
| runtime        = 111 minutes
| country        = Canada United States
| language       = English
| budget         = $11 million 
| gross          = $1,066,555 
|}} Western drama film directed by Christopher Cain, telling a fictional love story against a controversial  historical interpretation of the 1857 Mountain Meadows massacre. Written by Cain and Carole Whang Schutter, the film was a critical failure and box office disappointment.

==Plot== Mormon bishop, Fancher party persecuting Mormons. He instructs his sons Jonathan and Micah to keep an eye on them.
 gentiles (non-Mormons) federal government, Paiute Indians. By pointing to a rival Indian tribe as their mutual enemy, John D. Lee, the adopted son of Brigham Young, convinces the Paiutes that it is Gods will to kill the migrants. Jonathan objects to the plan, which his father has just conveyed to the local Mormons, and is imprisoned by his father. Jonathan has become disillusioned by the Mormon faith not only because of the planned massacre, but because of what he allowed to happen to his mother. In a flashback earlier in the film, Jonathan remembers that his mother was ordered away by a senior religious leader who took her as is his wife; she returned to get her children, for which she was executed in full view of Jonathan and his father.

The Fancher party repels the Indian attack, and the local Mormons are forced to complete the mission themselves. The Mormon militia under the command of John D. Lee is ordered to kill anyone who is old enough to talk. John D. Lee offers to lead the Fancher party to safety; however, they lead them instead to an ambush where they are all killed. Escaping his imprisonment, Jonathan arrives too late to save them and his lover, Emily, who is killed by his father. John Lee is executed for his role in the massacre in 1877 and Brigham Young denies any knowledge or involvement.

==Cast==
 
* Jon Voight as Jacob Samuelson
* Trent Ford as Jonathan Samuelson
* Tamara Hope as Emily Hudson
* Terence Stamp as Brigham Young
* Dean Cain as Joseph Smith
* Jon Gries as John D. Lee
* Taylor Handley as Micah Samuelson
* Lolita Davidovich as Nancy Dunlap
* Shaun Johnston as Captain Fancher
* Huntley Ritter as Robert Humphries
 

==Production==
 , Townhall.com, 3 May 2007; also found at  , 3 May 2007.  Officially, the LDS Church "is not commenting about this particular depiction"  of the massacre but has published an article marking 150 years since the tragedy occurred. 

Screenplay writer Carole Whang Schutter said: "Creating likeable  characters that take part in unimaginably atrocious acts is a chilling reminder that terrorists can be anyone who chooses to blindly follow fanatical, charismatic leaders.   Our fight is not against certain religions   powers of darkness which are prejudice, hate, ignorance, and fear perpetuated by leaders who history will surely judge by their deeds."  Schutter claims that she was inspired by God to write the story. "I got this crazy idea to write a story about a pioneer woman going in a wagon train to the California gold rush, and the train gets attacked by Mormons dressed as Indians   The idea wouldnt leave me. I believe it was from God."  : September Dawn to hit theaters in August, by Pete Fowler, Aspen Times, 9 July 2007  She also states that she finds the coincidental date of the massacre – September 11 – to be "very odd" and "strange," but that "people can draw their own conclusions" about the date. 

==Reception==
The film has received generally negative reviews and is considered to be controversial. Based on 54 reviews, the film currently holds a 13% rating on review aggregator website  ,   by Roger Ebert, Chicago Sun-Times, August 24, 2007, as found at rogerebert.com  who described it as "a strange, confused, unpleasant movie" unworthy of Voights talents. The New York Post gave the film an unusual 0/4. Justin Changs review for Variety (magazine)|Variety described it as, "not torture porn; its massacre porn." Though he realized that the film was meant to draw parallels to the September 11 attacks, Chang remarked that the film does not "convey any insights into the psychology of extremism, aside from some choice moments in Voights persuasively complex performance" and that it was "ultimately less interested in understanding its Mormon characters than in demonizing them"; the only praise he offered for the film went to the photography and location scouting done for the film. 

However, the film did receive some positive notices. Ken Fox of TV Guide gave the film 2.5/4 stars saying the film "sheds some much-needed light on a 150-year-old crime."  William Arnold of the Seattle Post-Intelligencer praised Jon Voights portrayal of Bishop Samuelson stating the character had "a soft brutality that is all the more terrifying for its compassionate veneer."  Ted Fry of The Seattle Times stated, "Religious and thematic issues aside, September Dawn is well-crafted as a revisionist Western with a message. If the message is muddled, theres plenty of literature to clear the facts — or to make the matter even more bewildering for those seeking truth." 

===Box office===
September Dawn opened in wide release on August 24, 2007 and made $601,857 in its opening weekend, ranking number 24 at the domestic box office.  By the end of its run two weeks later, it had grossed $1,066,555. Based on an $11 million budget, the film is a box office bomb.   

===Accolades===
Voight was nominated for a  , and  ).

==See also==
* Anti-Mormonism
* Criticism of The Church of Jesus Christ of Latter-day Saints
* Latter Day Saints in popular culture
* Mormonism and violence
* Revisionist Western

==References==
 
*  
*  

;News coverage
*   by Jeff Vice, Deseret News, 2 September 2007 David Brody, Christian Broadcasting Network News, 23 August 2007
*   (John Voigt on September Dawn), Adam Laukhuf, Radar Magazine, April 2007
*  , Carrie Sheffield, The Politico, 27 March 2007
*  , Laura Hancock, Deseret News, 17 February  2007
*  , Carrie A. Moore, Deseret News, 26 August 2005

;Reviews
*   by  Janos Gereben, San Francisco Examiner, August 24, 2007
*   by Sean P. Means, The Salt Lake Tribune, August 23, 2007
*   by Justin Chang, Variety (magazine)|Variety, August 21, 2007
*   by Michael Medved, USA Today, August 13, 2007
*   by Martin Grove, The Hollywood Reporter, April 25, 2007

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 