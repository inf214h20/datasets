Night of the Day of the Dawn
{{Infobox Film
| name = Night of the Day of the Dawn of the Son of the Bride of the Return of the Revenge of the Terror of the Attack of the Evil, Mutant, Alien, Flesh Eating, Hellbound, Zombified Living Dead Part 2
| image    = Night-of-the-day-of-the-dawn-part-2.jpg
| director = Lowell Mason (an alias for James Riffel)
| producer = James Riffel w/ special accolades provided by Adam Christopher Janowski
| writer = James Riffel
| starring = James Riffel (voice)
| music = 
| cinematography = 
| editing = 
| distributor =  
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = 
| italic title = force
}}

Night of the Day of the Dawn is a series of parody films written by James Riffel as spoofs adding his own scripts on already known films and television footage after deleting the original scripts from the films.

==Part 1==
Part 1 remains obscure and as yet unreleased as it was done at a public access station where Riffel took a bunch of student films he had made at New York University and some video footage and super 8 home movies and some other materials and edited them in a few days. It was never released and is available according to Riffel "back at my parents house at the bottom of one of the closets". 

==Part 2== horror Parody spoof written and directed by James Riffel.  It is also known as NOTDOT. Although dubbed Part 2, it is the first part actually released publicly. The movie where he used an alias name, Lowell Mason, was created by redubbing the 1968 horror classic Night of the Living Dead with comedic dialog, and by adding new clips. Although it was originally distributed to only 500 video stores in the United States, the film has since achieved cult status. 

NOTDOT was screened at the New York City Horror Film Festival in October 2005. 

===Title===
With 41 words in its title, 168 characters without spaces, it holds the distinction of being cited as the movie with the longest English language title,  but of the title, Maitland McDonagh of TV Guide wrote, "Most people cite Night of the Day of the Dawn of the Son of the Bride of the Return of the Revenge of the Terror of the Attack of the Evil, Mutant, Alien, Flesh Eating, Hellbound, Zombified Living Dead Part 2: In Shocking 2-D (1991) as the longest English-language title of all time, but its clearly a gimmicky joke."   

==Part 3== War of the Worlds that cost $200 million. 

==Part 4== The Most King Kong modern version in 2005.

==Part 5== spoof written by James Riffel and released in 2011. 

The movie is only fifty minutes long and is a parody of the golden age of television comparing what was considered appropriate television in the 1950s and 1960s and what is considered appropriate TV today. Riffel took an episode of The Andy Griffith Show and Bonanza replacing the dialogue with what Riffel believes are words and music that are more along the lines of what todays TV viewers are used to.

This was the first movie that Riffel wrote to be used for charity. Despite being Part 5, it is actually the fourth movie in the series. The title contains 40 words and contains 219 characters with no spaces, making it one of the longest movie titles ever made.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 