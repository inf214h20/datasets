Chickens Come Home
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Chickens Come Home
| image          = Chickenscomehometitlecard.jpg
| caption        = UK title card
| director       = James W. Horne
| producer       = Hal Roach
| writer         = Hal Roach (story) H.M. Walker (dialogue)
| narrator       =
| starring       = Stan Laurel Oliver Hardy
| music          = Marvin Hatley Leroy Shield Jack Stevens Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 30 26" (English) 56 11" (Spanish)
| country        = United States
| language       = English
| budget         =
}}
Chickens Come Home is a 1931 short film starring Laurel and Hardy, directed by James W. Horne and produced by Hal Roach. It was shot in January, 1931 and released on February 21, 1931. It is a remake of the 1927 silent film Love em and Weep in which Jimmy Finlayson played Hardys role and Hardy played a party guest.

==Plot== threatening to publish an old photograph of the woman and Ollie riding piggyback on the beach if she is not paid off. Ollie agrees to meet her that evening to make a settlement, but Mrs. Hardy (Thelma Todd) arrives to remind him of a dinner party taking place at that same moment. Ollie recruits employee Stan to go to the womans apartment while Ollie attends the party, a maneuver which does not please the woman, who demands Ollies telephone number, touching off a variety of misunderstandings and suspicions of unfaithfulness between the boys, their wives, Ollies butler and Mrs. Laurels gossipy friend.

The woman eventually makes it to Ollies house, despite Stans efforts; Ollie passes her off as Mrs. Laurel to avoid suspicion. After the guests leave, Ollie threatens to kill the woman and then himself, causing her to faint. The boys attempt to get her out of the house, but Mrs. Hardy and Mrs. Laurel, the latter armed with a hatchet, easily catch on, and the boys flee.

== Cast ==
* Stan Laurel
* Oliver Hardy
* Mae Busch
* Thelma Todd
* Jimmy Finlayson
* Norma Drew
* Patsy OByrne

== Spanish version ==
A Spanish-language version of this film was completely re-shot with the stars delivering their lines in phonetic Spanish. It was expanded to one hour by adding scenes of Abraham J. Cantu, a magician and of vaudeville professional regurgitator|regurgitator, Hadji Ali, performing at the Hardy dinner party. Titled Politiquerias, the film was released in Latin American and Spanish markets as a feature.

Joining headliners Laurel and Hardy was a supporting cast of native Spanish speakers: Linda Loredo played Mrs. Hardy, Carmen Granada was Mrs. Laurel and Rina De Liguro was the burr under everyones saddle in the Mae Busch role. James Finlayson absorbed the abuse — and more — of the magician and the regurgitator in the added scenes, reprising his role as the Hardy butler.

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 