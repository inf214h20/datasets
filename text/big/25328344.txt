Le Boulet
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Le Boulet
| image          =Le Boulet.jpg
| image size     =
| alt            =
| caption        = Alain Berbérian
| producer       = Thomas Langmann Jacques-Eric Strauss Fabienne Tsaï Jean-Louis Monthieux
| writer         =
| narrator       =
| starring       = Gérard Lanvin Benoît Poelvoorde José Garcia (actor)|José Garcia Djimon Hounsou Rossy de Palma
| music          = Robert Basarte François Forestier Krishna Levy Jean-Louis Viale
| cinematography = Jean-Pierre Sauvaire
| editing        = Philippe Bourgueil
| studio         =
| distributor    =
| released       =  
| runtime        = 107 min.
| country        = France
| language       = French
| budget         = $24,150,000
| gross          = $38,443,010 
}} French film directed by Alain Berbérian and Frédéric Forestier, released in 2002.

==Plot==
Moltès, a killer in prison, plays the lottery every week and sends the tickets with Reggio, a guard, so that the latters wife, Pauline, can have them validated. One day the ticket is a winner, but Pauline is at a party rally in Africa, carrying the ticket with her. Moltès wanting to recover his due, escapes and forces Reggio (the ball) to accompany him. However, he becomes the target of his nemesis, another gangster nicknamed "The Turk" (whose brother was killed by Moltès), and his bodyguard named Requin, a giant with teeth of steel.

==Cast==
*Gérard Lanvin as Gérard Moltès
*Benoît Poelvoorde as Francis Reggio
*José Garcia (actor)|José Garcia as Mustapha Amel AKA "The Turk"
*Djimon Hounsou as Detective Youssouf
*Rossy de Palma as Pauline Reggio
*Jean Benguigui as Saddam, the store keeper
*Gary Tiplady as Requin The Giant
*Gérard Darmon as Kowalski
*Stomy Bugsy as Malian guy #1
*Marco Prince as Malian guy #2
*Omar Sy as Malian guy #3
*Nicolas Anelka as Nicolas, the football player
*Lionel Chamoulaud as Journalist of Paris-Dakar
*Nikola Koretzky as Jean Monthieux
*Jamel Debbouze as Desert Prison Guard

==Production==
Shooting took place in Paris and North Africa.

The man who reads a newspaper with Moltès photo on the back is Jean-Marc Deschamps, the production manager. There are numerous cameo appearances; Nicolas Anelka appears as a football player, Jamel Debbouze as a prison guard in Mali, and musicians Stomy Bugsy (former Ministère AMER) and Marco Prince (singer of the FFF) and comedian Omar Sy (the duo Omar et Fred) as killer brothers.
The scriptwriter and producer Thomas Langmann made a small cameo in the role of the Turks brother. 
 The Spy Who Loved Me (1977) and Moonraker (film)|Moonraker (1979).

==References==
 

==External links==
*   in Internet Movie Database

 
 
 
 
 
 