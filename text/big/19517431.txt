Men Against the Sky
{{Infobox film
| name = Men Against the Sky
| image = Men Against the Sky-poster.jpg
| caption = Theatrical film poster
| director = Leslie Goodwins
| producer = Howard Benedict
| writer = John Twist (story) Nathanael West Richard Dix Kent Taylor, Edmund Lowe
| music = Frank Tours Roy Webb
| cinematography = Frank Redman
| editing = Desmond Marquette
| studio =RKO Radio Pictures
| distributor = RKO Radio Pictures
| released =  
| runtime = 75 minutes
| country = United States English
| budget =
}} Richard Dix and Edmund Lowe. Based on a story by John Twist, and screenplay by novelist Nathanael West, the film deals with aircraft development and the dangers of flying in the period before World War II. 

==Plot==
Phil Mercedes (Richard Dix), once a record-setting pilot, is now an aging alcoholic. As an air show performer, while inebriated, he crashes his stunt aircraft into a barn and is grounded for a year. His sister Kay (Wendy Barrie), his only means of support, hopes to land a job in the drafting department for Martin Ames (Kent Taylor), the chief engineer for Dan McLean (Edmund Lowe), an aircraft manufacturer. 

Given the war in Europe, the McLean company hopes to win a big contract with the government. Although Kay is not very skilled, she shows Ames some drawings Phil made. The creative designs interest Dan, who approves the construction of a high-speed fighter aircraft. Preliminary tests of the aircraft prove disastrous, with test pilot Dick Allerton (Donald Briggs) contending that the aircraft is too dangerous to fly. 

With a redesign of the wings, Dick flies the experimental aircraft again, but the landing gear will not extend fully, leaving the test pilot circling the airport. If the aircraft cannot complete its tests, the company will be in danger of bankruptcy. Phil again takes over, successfully wrenching the gear down, but losing his life in the process. With the successful completion of the flight tests, the company is saved. 

==Cast==
  Richard Dix as Phil Mercedes
* Kent Taylor as Martin Ames
* Edmund Lowe as Daniel M. "Dan" McLean
* Wendy Barrie as Kay Mercedes, aka Kay Green
* Granville Bates as Mr. Burdett
* Grant Withers as Mr. Grant
* Donald Briggs as Dick Allerton
* Charles Quigley as Flynn
* Selmer Jackson as Capt. Sanders
* Lee Bonnell as Capt. Wallen  
 

==Production==
  B films in this period, including Men Against the Sky, but recognition of his work was short-lived. Along with his wife, Eileen, he was killed on December 22, 1940, when their car was hit by a train. Miller, Frank.   Turner Classic Movies. Retrieved: October 11, 2014. Principal photography for Men Against the Sky took place from late May to June 15, 1940. 

According to The Hollywood Reporter, Lucille Ball, considered Cinema of the United States|Hollywoods B movie queen, was to play the female lead before Wendy Barrie came on board.  Paul Mantz, noted movie stunt pilot, was the aviation consultant. Models were mostly used, but also notable is the use of footage of the experimental Hughes H-1 Racer during its trials. 

==Reception== Test Pilot (1938) or Men With Wings (1938), which explored the same subject. Bosley Crowther of The New York Times in a contemporary review, characterized Men Against the Sky as a "generally entertaining little action picture," although he criticized its "maudlin heroics," the storyline which was "routine and obvious" and the performances that were no better than "stock and pedestrian." 

==References==
Notes
 

Citations
 

Bibliography
 
* Cowin, Hugh W. The Risk Takers, A Unique Pictorial Record 1908-1972: Racing & Record-setting Aircraft (Aviation Pioneer 2).  London: Osprey Aviation, 1999. ISBN 1-85532-904-2.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 