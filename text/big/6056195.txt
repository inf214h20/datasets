A Fighter's Blues
 
 
{{Infobox film
| name           = A Fighters Blues
| image          = Fighter2000.jpg
| caption        = Daniel Lee
| producer       = Andy Lau Derek Yee Daniel Lee
| narrator       =
| starring       = Andy Lau
| music          = Henry Lai
| cinematography = Kwok-Man Keung
| editing        = Wai Chiu Chung
| studio         = Teamwork Motion Pictures
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 105 min. Hong Kong English Thai Thai Japanese Japanese
| budget         =
| gross          = HK$22,002,055
| preceded by    =
| followed by    =
}} 2000 Cinema Hong Kong Daniel Lee and starring Andy Lau. On another note, A Fighters Blues is also Laus 100th film role. 

==Plot==
After spending 13 years in jail for killing one of his opponents Chat Chai in the dressing room after breaking up the fight because of a quarrel with his girlfriend Pim, Mong Fu (Andy Lau), a washed up Muay Thai kickboxer returns to Thailand to look for his old love. Upon arrival in Bangkok, he finds out that she died and that he has a 14 year old daughter. He finds the orphanage and meets his daughter and starts a relationship with sister Mioko who runs the orphanage. To clean his past, he challenges  the current and more than 15 years younger champion, who wants to avenge Chai.

==Cast==
* Andy Lau as Mong Fu
* Takako Tokiwa as Sister Mioko
* Intira Jaroenpura as Pim Nathasiri
* Apichaya Thanatthanapong as Ploy
* Reila Aphrodite

 {{Cite web |url=http://www.imdb.com/title/tt0277558/ |title=A Fighters Blues 
 |accessdate=2 July 2010 |publisher=imdb.com}}    

==Overview==
 
The film received mixed critic by reviewers and audiences and has a score of 56 out of 100 on Rottentomatoes, but was praised for the beautiful locations in Thailand, use of flashbacks.the acting of the main cast, and the editing of the fight scenes.

Andy Lau trained extensively in Muay Thai for his role as Mong Fu.

==Soundtrack==
The majority of the songs in the film was made and sung by Andy Lau including Smile and When I Met You.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 