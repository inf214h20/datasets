The County Chairman (1914 film)
{{infobox film
| name           = The Country Chairman
| image          = The County Chairman - scene - 1914.jpg
| imagesize      =
| caption        = Scene published in a contemporary newspaper
| director       = Allan Dwan
| producer       = Adolph Zukor
| writer         = Allan Dwan (screenplay/scenario)
| based on       =  
| starring       = Maclyn Arbuckle Harold Lockwood
| music          =
| cinematography = H. Lyman Broening
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1914 Fox in 1935 with Will Rogers.   

==Cast==
*Maclyn Arbuckle - The Honorable Jim Hackler,The County Chairman (*Arbuckles role in 1903 play)
*Harold Lockwood - Tillford Wheeler
*Willis P. Sweatnam - Sassafras Livingston (*Sweatnams role in 1903 play)
*William Lloyd - Elias Rigby
*Daisy Jefferson - Lucy Rigby
*Helen Aubrey - Mrs. Rigby
*Mabel Wilbur - Lorena Watkins
*Wellington A. Playter - Joseph Whitaker

unbilled
*Amy Summers - Chick

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 