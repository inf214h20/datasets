Dazed and Confused (film)
{{Infobox film
| name           = Dazed and Confused
| image          = DazedConfused.jpg
| caption        = Theatrical release poster
| director       = Richard Linklater
| producer       = Richard Linklater Sean Daniel James Jacks
| writer         = Richard Linklater
| starring       = Jason London Rory Cochrane Wiley Wiggins Adam Goldberg Matthew McConaughey Cole Hauser Parker Posey Milla Jovovich Ben Affleck
| music          = 
| cinematography = Lee Daniel
| editing        = Sandra Adair
| studio         = Alphaville
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 102 minutes    
| country        = United States
| language       = English
| budget         = $6,900,000  
| gross          = $7,993,039
}} coming of age comedy film written and directed by Richard Linklater. The film features a large ensemble cast of actors who would later become stars, including Matthew McConaughey, Jason London, Ben Affleck, Milla Jovovich, Cole Hauser, Parker Posey, Adam Goldberg, Joey Lauren Adams, Nicky Katt, and Rory Cochrane. The plot follows various groups of Texas teenagers during the last day of school in the summer of 1976.

The film grossed less than $8 million at the U.S. box office but later achieved cult film status. In 2002, Quentin Tarantino listed it as the 10th best film of all time in a Sight and Sound poll.  It also ranked third on Entertainment Weekly magazines list of the 50 Best High School Movies.  The magazine also ranked it 10th on their "Funniest Movies of the Past 25 Years" list. 
 song of Rock and John Paul Jones agreed, Robert Plant refused. 

==Plot==
It is May 28, 1976, the last day of school at Lee High School in the suburbs of Austin, Texas. The next years group of seniors are preparing for the annual hazing of incoming freshmen. Randall "Pink" Floyd, the schools star football player, is asked to sign a pledge promising not to take drugs during the summer or do anything that would "jeopardize the goal of a championship season". When classes end, the incoming freshman boys are hunted down by the seniors and Paddling (punishment)|paddled. The incoming freshman girls are also hazed; they are rounded up in the school parking lot by senior girls, covered in mustard, ketchup, flour and raw eggs, and forced to propose to senior boys.
 cruising with friends that night. Plans for the evening are ruined when Kevin Pickfords parents discover he planned to host a keg party. Elsewhere, the intellectual trio of Cynthia Dunn, Tony Olson and Mike Newhouse decide to participate in the evenings activities. Pink and his friend David Wooderson, a man in his early 20s who still socializes with high school students, pick up Mitch and head for the Emporium, a pool hall  frequented by teenagers.
 hamburger drive-in. Mitch is introduced to sophomore Julie Simms, with whom he shares a mutual attraction. While cruising again with Pink, Pickford, and Don Dawson, Mitch drinks beer and smokes marijuana for the first time. After a game of mailbox baseball, a neighborhood resident brandishing a gun threatens to call the police. They barely escape after the resident fires at their car. After returning to the Emporium, Mitch runs into his middle school friends. They hatch a plan to get revenge on OBannion. The plan culminates with them dumping paint on OBannion, who leaves in a fit of rage.
 make out. Tony gives Sabrina a ride home and they kiss good night.

As night turns to dawn, Pink, Wooderson, Don, Ron Slater and several other friends decide to smoke marijuana on the 50-yard line of the football field. The police arrive, so they ditch the drugs. Recognizing Pink, the police call Coach Conrad, his football coach. Conrad lectures Pink about hanging out with "losers" and insists that he sign the pledge. Pink says that he might play football, but he is not going to sign the pledge. Pink leaves with his friends to get tickets to an Aerosmith concert. Mitch arrives home after sunrise to find his mother has waited up for him. She decides against punishment but warns him about coming home late again. Mitch goes to his bedroom, puts on headphones and listens to "Slow Ride" by Foghat as Pink, Wooderson, Slater and Simone travel down a highway to purchase their tickets.

==Cast==
 
 
* Jason London as Randall "Pink" Floyd
* Wiley Wiggins as Mitch Kramer
* Rory Cochrane as Ron Slater
* Matthew McConaughey as David Wooderson
* Sasha Jenson as Don Dawson
* Michelle Burke as Jodi Kramer
* Christine Harnos as Kaye Faulkner
* Adam Goldberg as Mike Newhouse
* Anthony Rapp as Tony Olson
* Marissa Ribisi as Cynthia Dunn
* Catherine Avril Morris as Julie Simms Shawn Andrews as Kevin Pickford
* Cole Hauser as Benny ODonnell
* Milla Jovovich as Michelle Burroughs
* Joey Lauren Adams as Simone Kerr
* Christin Hinojosa as Sabrina Davis
* Ben Affleck as Fred OBannion
* Parker Posey as Darla Marks
* Deena Martin as Shavonne Wright
* Nicky Katt as Clint Bruno
* Esteban Powell as Carl Burnett
* Jason O. Smith as Melvin Spivey
* Mark Vandermeulen as Tommy Houston
* Renée Zellweger as Nesi White
 

==Reception==
Dazed and Confused was released on September 24, 1993 in 183 theaters, grossing $918,127 on its opening weekend. It went on to make $7.9 million in North America.   

The film received favorable reviews from critics. It garnered a 94% approval rating from 50 critics&nbsp;&ndash; an   provides a score of 78 out of 100 from 18 critics, indicating "generally favorable" reviews.   

Film critic Roger Ebert awarded the film three stars out of four, praising the film as "art crossed with anthropology" with a "painful underside".    In her review for The New York Times, Janet Maslin wrote, "Dazed and Confused has an enjoyably playful spirit, one that amply compensates for its lack of structure".    Desson Howe, in his review for The Washington Post, wrote, "Dazed succeeds on its own terms and reflects American culture so well, it becomes part of it".    In her review for The Austin Chronicle, Marjorie Baumgarten praised Matthew McConaugheys performance: "He is a character were all too familiar with in the movies but McConaughey nails this guy without a hint of condescension or whimsy, claiming this character for all time as his own".   

Rolling Stones Peter Travers praised Linklater as a "sly and formidable talent, bringing an anthropologists eye to this spectacularly funny celebration of the rites of stupidity. His shitfaced American Graffiti is the ultimate party movie – loud, crude, socially irresponsible and totally irresistible".    In his review for Time (magazine)|Time, Richard Corliss wrote, "Linklater is surely no ham-fisted moralist, and his film has lots of attitude to shake a finger at. But it also has enough buoyant 70s music to shake anybodys tail feather, and a kind of easy jubilance of narrative and character".    Entertainment Weekly gave the film an "A" rating, and Owen Gleiberman wrote, "Yet if Linklater captures the comic goofiness of the time, he also evokes its liberating spirit. The film finds its meaning in the subtle clash between the older, sadistic macho-jock ethos and the follow-your-impulse hedonism that was the lingering legacy of the 60s".   

==Legacy==
Quentin Tarantino included it on his list of the 10 greatest films of all time in the 2002 Sight and Sound poll.     In 2003, Entertainment Weekly ranked the film #17 on their list of "The Top 50 Cult Films",    third on their list of the 50 Best High School Movies,    10th on their "Funniest Movies of the Past 25 Years" list,    and ranked it #6 on their "The Cult 25: The Essential Left-Field Movie Hits Since 83" list.   

In October 2004, three of Linklaters former classmates from Huntsville High School, whose surnames are Wooderson, Slater, and Floyd, filed a defamation lawsuit against Linklater, claiming to be the basis for the similarly named characters on the film. The lawsuit was filed in New Mexico rather than Texas because New Mexico has a longer statute of limitations.  The suit was subsequently dismissed. Linklater, Richard. Directors commentary, Dazed and Confused, Criterion Collection DVD. 

In 2005, a quote from Dazed and Confused "Thats what I love about these high school girls, man. I get older, they stay the same age."  was nominated for AFIs 100 Years...100 Movie Quotes; it failed to make the final list. 
 Butch Walker John Hughes film: "The drama is so low-key in  . I don’t remember teenage being that dramatic. I remember just trying to go with the flow, socialize, fit in and be cool. The stakes were really low. To get Aerosmith tickets or not? That’s a big thing. It was really rare when the star-crossed lovers from the opposite side of the tracks and the girl gets pregnant and there’s a car crash and somebody dies. That didn’t really happen much. But riding around and trying to look for something to do with the music cranked up, now that happened a lot!"      

After Boyhood (film)|Boyhood was released Linklater announced that his next film would be a "spiritual sequel" to the 1993 movie titled Thats What Im Talking About (film)|Thats What Im Talking About.    The new film will take place in College during the 1980s as opposed to Dazes 70s highschool setting. 

The line "alright, alright, alright" became a catchphrase for Matthew McConaughey. 

==Home video==
MCA/Universal released Dazed and Confused on laserdisc in January 1994,  followed by a VHS release two months later. 

The film was released on HD DVD in 2006.  The Criterion Collection released a two-disc boxed-set edition of the film on June 6, 2006 in the U.S. and Canada. Features included an audio commentary by Richard Linklater, deleted scenes, the original trailer, the 50 minute "Making Dazed" documentary that aired on the American Movie Classics channel on September 18, 2005, on-set interviews, behind-the-scenes footage, cast auditions and footage from the ten-year anniversary celebration. Also included is a 72-page book featuring new essays by Kent Jones, Jim DeRogatis, and Chuck Klosterman as well as memories from the cast and crew, character profiles and a mini reproduction of the original film poster designed by Frank Kozik. Entertainment Weekly gave it an "A" rating and called it a "fine edition grants this enduring cult classic the DVD treatment it deserves".   
 AVC MPEG-4 transfer, with a DTS-HD Master Audio 5.1 surround sound|5.1 soundtrack. 

==Soundtracks== The Medicine Label. The songs Hurricane (Bob Dylan song)|"Hurricane" by Bob Dylan, Hey Baby (Ted Nugent song)|"Hey Baby" by Ted Nugent, and "Sweet Emotion" by Aerosmith were also included in the film, but not on the commercial soundtracks.

{{Infobox album
| Name        = Dazed and Confused
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = September 28, 1993
| Recorded    =  heavy metal, glam rock, Southern rock, funk
| Length      = 56:56 Medicine
| Producer    = 
}}

{{Track listing
| collapsed       = no
| headline         = Dazed and Confused (1993)
| writing_credits = no
| extra_column = Artist
| total_length = 56:56
| title1 = Rock and Roll, Hoochie Koo
| writer1 = 
| extra1 = Rick Derringer
| length1 = 3:44
| title2 = Slow Ride
| writer2 = 
| extra2 = Foghat
| length2 = 3:58
| title3 = Schools Out (song)|Schools Out
| writer3 = 
| extra3 = Alice Cooper
| length3 = 3:29 Jim Dandy
| writer4 = 
| extra4 = Black Oak Arkansas
| length4 = 2:42 Tush
| writer5 = 
| extra5 = ZZ Top
| length5 = 2:17
| title6 = Love Hurts
| writer6 =  Nazareth
| length6 = 3:53 Stranglehold
| writer7 = 
| extra7 = Ted Nugent
| length7 = 8:24 Cherry Bomb
| writer8 = 
| extra8 = The Runaways
| length8 = 2:19 Fox on the Run
| writer9 =  Sweet
| length9 = 3:26 Low Rider
| writer10 =  War
| length10 = 3:13
| title11 = Tuesdays Gone
| writer11 = 
| extra11 = Lynyrd Skynyrd
| length11 = 7:32 Highway Star
| writer12 = 
| extra12 = Deep Purple
| length12 =6:08
| title13 = Rock and Roll All Nite
| writer13 =  KISS
| length13 =2:57 Paranoid
| writer14 = 
| extra14 = Black Sabbath
| length14 =2:47
}}

{{Track listing
| collapsed       = yes
| headline         = Even More Dazed and Confused (1994)
| writing_credits = no
| extra_column = Artist
| total_length = 50:52 Free Ride
| writer1 = 
| extra1 =Edgar Winter Group
| length1 = 3:08 No More Mr. Nice Guy
| writer2 = 
| extra2 = Alice Cooper
| length2 = 3:07
| title3 = Livin in the USA
| writer3 = 
| extra3 = The Steve Miller Band
| length3 = 4:05
| title4 = Never Been Any Reason
| writer4 = 
| extra4 = Head East
| length4 =5:12 Why Cant We Be Friends?
| writer5 = 
| extra5 =  War
| length5 = 3:51 Summer Breeze
| writer6 = 
| extra6 = Seals and Crofts
| length6 = 3:25
| title7 = Right Place, Wrong Time
| writer7 = 
| extra7 = Dr. John
| length7 = 2:54
| title8 = Balinese
| writer8 = 
| extra8 =  ZZ Top
| length8 = 2:39
| title9 = Lord Have Mercy On My Soul
| writer9 = 
| extra9 = Black Oak Arkansas
| length9 = 6:14
| title10 = I Just Want to Make Love to You
| writer10 = 
| extra10 = Foghat
| length10 = 4:19 Show Me the Way
| writer11 = 
| extra11 = Peter Frampton
| length11 = 4:41
| title12 =Do You Feel Like We Do
| writer12 = 
| extra12 = Peter Frampton
| length12 = 7:13
}}

==Book==
In September 1993, St. Martins Press published a 127-page, softcover book (ISBN 0-312-09466-3) inspired by Richard Linklaters screenplay. It was compiled by Linklater, Denise Montgomery, and others, and designed by Erik Josowitz. It was presented as a kind of yearbook, with character profiles, essays by characters, a time-line focusing on the years 1973 to 1977, and various 1970s pop culture charts and quizzes. It also featured dozens of black-and-white photos from the film.

==Notes==
 

==References==
{{reflist|colwidth=32em|refs=
   
}}

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 