Rechipo
{{Infobox film
| name           = Rechipo
| image          = Rechipo poster Hd.jpg
| caption        =
| director       = Parachuri Murali
| producer       = G. V. Ramana
| writer         = Parachuri Murali
| narrator       = Nitin Ileana DCruz Ahuti Prasad
| music          = Mani Sharma
| cinematography =
| editing        = Kotagiri Venkateswara Rao
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Telugu film, Nitin and Ileana DCruz in the lead roles. The film released on September 25, 2009. Later, it was dubbed into Tamil as Dhana and Hindi as Aaj ka Naya Khiladi. 

==Plot==
Siva (Nitin Kumar Reddy|Nitin) is a thief. He robs the Peter to pay Paul on the lines of Robin hood. He always robs the black money from rich persons and spends it for the education of orphans and for a good cause. He is claimed to be an intelligent criminal. Once the police crack a major cricket betting racket mafia and get vital information about Rs 500 crores through one of the gang members. However, the Home Minister (Ahuti Prasad) transfers the commissioner (Bhanu Chander) and grabs the money. He conceals the money in the overhead water tank of his house. On information, Siva robs the money and escapes. However, he lands in a marriage party along with an old woman (Ramaprabha).

While on their way to the venue, Siva had an accidental meet with Krishnaveni (Ileana DCruz|Ileana) near a petrol pump. Soon, both fall in love with each other. Honesty, Siva decides to confess that he is a thief, but unfortunately everyone comes to know about it by the time he reaches the venue. Krishna leaves the place in a huff and decides to go to Dubai to stay with her friend, only to be kidnapped by the mafia gang. Later the Home Minister gets a call from the mafia don Shakur Ahmed demanding the release of four of his group members besides the Rs 500 crore cash. Following a suggestion by the Commissioner, the home minister agrees to send Siva to Dubai as who only could bring back Krishna safely. What all the problems faced by Siva in getting her released? How he meets Shakur Ahmed? How in what conditions Siva and Krishna travel in a desert to escape from the don? Did the love between Siva and Krishna become fruitful? Answers to all these questions form part of the climax. 

==Cast== Nitin
* Ileana DCruz
* Bhanu Chander
* Ramaprabha
* Ahuti Prasad

==Soundtrack==
{{Infobox album
| Name = Rechipo
| Longtype = to Rechipo
| Type = Soundtrack
| Artist = Mani Sharma
| Cover =
| Border = yes
| Alt =
| Caption =
| Released =  
| Recorded = 2009 Feature film soundtrack
| Length = 26:25 Telugu
| Label = Supreme Music
| Producer = Mani Sharma
| Reviews =
| Last album = Ek Niranjan (2009)
| This album = Rechipo (2009)
| Next album = Baanam (2009)
}}
The Songs were composed by Mani Sharma. The Cassettes and Cds were released directly into the market directly by Supreme Music on 10 September 2009. 
{{tracklist
| headline        =
| extra_column    = Artist(s)
| total_length    = 26:25
| all_lyrics      = Bhaskarabhatla
| title1          = Bhayam
| extra1          = Rahul Nambiar
| length1         = 03:36
| title2          = O Rori
| extra2          = Venu, Geetha Madhuri
| length2         = 04:51
| title3          = Tholi Tholiga
| extra3          = Ranjith (singer)|Ranjith, Swetha
| length3         = 04:26
| title4          = Gaalaina Malavika
| length4         = 05:17
| title5          = Ettuko
| extra5          = Geetha Madhuri
| length5         = 04:07
| title6          = Pathikella
| extra6          = Ranjith, Saindhavi
| length6         = 04:08
}}

==References==
 

==External links==
*  
 
 
 