Oruththi
{{Infobox film
| name = Oruththi
| border = yes
| caption =
| director = Amshan Kumar
| based on =  
| starring = Poorvaja Bharati Mani Thomas Ober
| music = L. Vaidyanathan
| cinematography = P.S. Dharan
| editing = S. Satish & J.N. Harsha
| art = A.C.Pillai
| runtime = 91 minutes
| language = Tamil
| country = India
}}
 Tamil film directed by Amshan Kumar.
 International Film Festival of India and was screened in the Indian Panorama Section.   It also won a special award from the Pondicherry government.   

== Cast ==

* Poorvaja
* Ganesh Babu
* Raju
* Balasingh
* Bharati Mani
* Thomas Ober

== Crew ==

* Director: Amshan Kumar
* Screenplay: Amshan Kumar
* Camera: P.S. Dharan
* Editor: S. Satish & J.N. Harsha
* Art: A.C. Pillai
* Music: L. Vaidyanathan|L.Vaidyanathan

== Reception ==

Oruththi received critical acclaim, with all the reviewers praising the film for its natural treatment.   The music by L. Vaidyanathan|L.Vaidyanathan also received positive reception. 

== References ==
 

== External links ==

*  

 
 
 


 