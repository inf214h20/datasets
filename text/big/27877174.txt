Love in Japan
{{Infobox film
| name           = Love in Japan
| image          = loveinjapan1.jpg
| caption        = Official Movie Poster
| director       = Akram Shaikh
| producer       = Akram Shaikh
| writer         = Faiz Saleem
| narrator       = 
| starring       = Rajpal Yadav Shakti Kapoor Ketaki Dave Tiku Talsania Raju Shrivastava Upasana Singh Yukta Mookhey
| music          = Sikander Khan
| cinematography = Sushil Chopra
| editing        = Vinod Pathak
| distributor    = Sony Entertainment Television
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 2006 Bollywood action comedy directed by Akram Shaikh. The film stars Rajpal Yadav, Shakti Kapoor, Ketaki Dave, Tiku Talsania, Raju Shrivastava, Upasana Singh and Yukta Mookhey. 

==Plot==
Chakra Dhari (Rajpal Yadav) wants to be a famous film director. But hes so broke hes even borrowed money from his landlady, Mrs. Mehta (Ketaki Dave). Opportunity knocks when Tiku Sheth (Tiku Talsania), an NRI from Japan, enters Chakra Dharis life. He learns that Tiku Sheth is a huge fan of a popular actress known as Barkha Rani (Upasana Singh). In order to get financial assistance from Tiku seth Chakra Dhari asks Barkha Rani to help him. They go to Japan to shoot the film, Mrs. Mehtas son Rocky (Mohsin Khan) is selected as the hero.

Bakha Ranis list of admirers grows as the shooting progresses. Babu Bhai (Mushtaq Khan) the cameraman and choreographer Pawan (Raju Srivastav) fall for Barkha Rani. The situation gets more complicated when Rocky gets involved with a drug lord called Banga (Shakti Kapoor).

==Cast==
*Rajpal Yadav...... Chakra Dhari
*Shakti Kapoor...... Banga
*Ketaki Dave...... Mrs. Mehta
*Tiku Talsania...... Tiku Seth
*Raju Shrivastava...... Choreographer Pawan
*Upasana Singh...... Barkha Rani
*Yukta Mookhey...... special appearance

==References==
 

==External links==
*  

 
 
 

 