Lass of the Lumberlands
{{Infobox film
| name           = A Lass of the Lumberlands
| image          = File:Lass of the Lumberlands lobby card 3.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Lobby card
| director       = Paul Hurst J. P. McGowan
| producer       = Samuel S. Hutchinson J. P. McGowan
| writer         = E. Alexander Powell Ford Beebe
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Helen Holmes Leo D. Maloney Thomas G. Lingham
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Mutual Film
| released       =  
| runtime        = 15 two-reel episodes
| country        = United States
| language       = Silent
| budget         = 
| gross          = £2,000,000
}}
A Lass of the Lumberlands is a 1916 silent film serial directed by Paul Hurst and J. P. McGowan, and starring Helen Holmes.  The serial is considered to be Lost film|lost. 

==Cast==
* Helen Holmes - Helen Holmes / Helen Dawson  
* Leo D. Maloney - Tom Dawson 
* Thomas G. Lingham - Dollar Holmes
* William N. Chapman 
* Paul Hurst 
* Katherine Goodrich
* Frank Hemphill
* William Behrens

==Chapter titles==
# The Lumber Pirates
# The Wreck in the Fog
# First Blood
# A Deed of Daring
# The Burned Record
# The Spiked Switch
# The Runaway Car
# The Fight in Camp I
# The Double Fight
# The Gold Rush
# The Ace High Loses
# The Main Line Wreck
# (Title Unknown)
# The Indians Head
# Retribution

==See also==
*List of lost films

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 


 