Yamada: The Samurai of Ayothaya
{{Infobox film
| name           = Yamada: The Samurai of Ayothaya
| image          = YamadaTheSamuraiOfAyothaya2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Nopporn Watin
| producer       = Nopporn Watin
| writer         = 
| screenplay     = 
| story          = 
| starring       = Seigi Ozeki Sorapong Chatree Winai Kraibutr
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Thailand
| language       = Thai Japanese Burmese
| budget         = 
| gross          = 
}}
Yamada: The Samurai of Ayothaya ( ) is a 2010 Thai action movie directed by Nopporn Watin. The film features renowned Muay Thai boxers Buakaw Banchamek, Saenchai Sor. Kingstar, Yodsanklai Fairtex, and Anuwat Kaewsamrit along with the main cast of actors.

==Plot==
Attacked and wounded by a group of traitorous Japanese ninja, a mercenary samurai named Yamada (Ozeki) is rescued and nursed back to health by a group of Siamese warriors, in service to the King of Ayothaya. Confused by the mysterious identity of his assailants, Yamada stays with the warriors, befriending them, learning their art, and eventually pledging his loyalty and life to their cause and kingdom.

==Historical basis==
The lead character in the film is based on Yamada Nagamasa, a Japanese adventurer who later became a governor in the Ayutthaya Kingdom.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 