Eames: The Architect and the Painter
{{Infobox film
| name           = Eames: The Architect and the Painter
| image_size     =
| image	         = Eames- The Architect and the Painter FilmPoster.jpeg
| caption        =
| director       = Jason Cohn Bill Jersey
| producer       = Jason Cohn Bill Jersey
| writer         = Jason Cohn
| narrator       = James Franco
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Quest Productions Bread & Butter Films American Masters Productions
| distributor    = First Run Features
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Eames: The Architect and the Painter is a 2011 documentary film about American designers Charles and Ray Eames and the Eames Office. It was produced and written by Jason Cohn, and coproduced by Bill Jersey. 
 narration by James Franco.

The film uses extensive interviews to frame the story. There are eight subjects, including  , Gordon Ashby, and Deborah Sussman.

==Reception==
The New York Times reviewer A. O. Scott called it "a lively new documentary" and "appropriately busy and abundant: full of objects, information, stories and people, organized with hectic elegance."    He praised it for showing, "in marvelous detail, how their work was an extension of themselves and how their distinct personalities melded into a unique and protean force." 

Tom Keogh of the Seattle Times wrote that "Much like the creations of its subjects, Eames is itself a dazzling, sensory adventure" and that "the film is an extraordinary and enjoyable history of how two people influenced so much of our thinking and surroundings today." 

Los Angeles Times critic Kenneth Turan described the film as "a thorough and vibrant examination of the master Modernists." 

==Awards==
The film won a Peabody Award in 2012.

==Home media==
The film is available on DVD. 

==References==
 

==External links==
* 
* 
*  
* 

 

 
 
 
 
 