Quebracho (film)
  1974 film Santa Fe. The wood and its main product, tannin, were highly coveted between 1918 and 1945 and became a focal point of political and social struggles connected with the evolution of trade unionism, as well as the emergence of the Radical Civic Union and Peronism. It is considered an iconic film of its period, if now somewhat dated. {{cite web
 |url=http://www.nodo50.org/rebeldemule/foro/viewtopic.php?f=3&t=2358
 |title=Quebracho (Ricardo Wullicher 1972 Argentina)
 |work=RebelDeMule
 |language=Spanish
 |accessdate=2010-11-09}} 

== Cast ==
* Lautaro Murúa
* Juan Carlos Gené
* Héctor Alterio
* Luis Aranda

== References ==
 

== External links ==
  

 
 
 

 