Genghis Khan (1992 film)
{{Infobox Film  name =Genghis Khan image =GenghisKhan1992film.jpg director = Ken Annakin writer = Andrzej Krakowski language = English
|budget = U.S. Dollar|$55,000,000 (estimated)
}} Kyrgyz director Tolomush Okeyev but, after several weeks of much expense in project development, preparation, and pre-production, the film was halted, since it became evident that it was unsuitable for an international audience.

The project was restored and given to British director, Peter Duffell (director of The Far Pavilions#Film, TV or theatrical adaptations|The Far Pavilions, an epic film similar in scale), with a new script which begins with the story of Khans childhood. Soon after shooting began, Ken Annakin took over as director. Unfortunately, because of the August Coup, actors were ordered by their Embassies to return home immediately. As a result, production ended prematurely.

==Cast==
*Bekim Fehmiu
*Rodney A. Grant  ...  Jamuga Daniel Greene  ...  Mikuli/Jebel Togrul
*James Hong
*Richard Lee-Sung
*James Mitchum  ...  Inalchuk
*Pat Morita  ...  Emperor Wang
*Julia Nickson  ...  Bortei (as Julia Nickson-Soul)
*Tricia ONeil  ...  Hoelun John Saxon  ...  Chiledu
*Nigel Terry  ...  Mulwick
*Richard Tyson  ...  Genghis Khan
*Hal Yamanouchi

==See also==
* The Conqueror (film)|The Conqueror (film)
* Genghis Khan (1965 film)|Genghis Khan (1965 film)
* Genghis Khan (1998 film)|Genghis Khan (1998 film)
*  
* Mongol (film)|Mongol (film)
* No Right to Die – Chinggis Khaan
* Genghis Khan (TVB)|Genghis Khan (TVB)
* Genghis Khan (ATV)|Genghis Khan (ATV)
* Genghis Khan (2004 TV series)|Genghis Khan (2004 TV series)

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 

 