Alias Ladyfingers
{{Infobox film
| name           = Alias Ladyfingers
| image          = Alias Ladyfingers (1921) - 2.jpg
| alt            = 
| caption        = Newspaper ad
| film name      = 
| director       = Bayard Veiller
| producer       = 
| writer         =  Lenore J. Coffee
| story          = 
| based on       =    Frank Elliot
| narrator       = 
| music          = 
| cinematography = 
| editing        =  Metro Pictures Corporation Metro Pictures Corporation
| released       =  
| runtime        = 
| country        = United States Silent (English intertitles)
| budget         = 
| gross          = 
}} mystery novel Frank Elliot, Metro Pictures Corporation.

==Synopsis== disowns her. string of pearls. The pearls disappear and the police suspect Ladyfingers, but the pearls are found and the case is dismissed. Rachel notices the resemblance between him and her son-in-law, and soon realizes that he is her grandson. When his past is revealed, Ladyfingers confesses his crimes and agrees to go to jail for two years to pay his debt to society. After his release from his prison term, he takes up farming and is happily married to Enid.  

==Cast==
*Bert Lytell - Robert Ashe (Ladyfingers)  
*Ora Carew - Enid Camden   Frank Elliott - Justin Haddon  
*Edythe Chapman - Rachel Stetherill  
*DeWitt Jennings - Lieutenant Ambrose  
*Stanley Goethals - Robert Ashe, (age 4)

==Reviews and reception==
A review in the Exhibitors Trade Review for the film when it played at The Rialto Theater in Allentown, Pennsylvania, said it was a "first-rate crook drama, with star well known and liked in Allentown. Sequence was good run, with attendance better than for some time".  When the film played at the Kings Theatre in St. Louis, Missouri|St. Louis, Lytell appeared in person in conjunction with the film for three days and "proved to be a real drawing card", according to the Exhibitors Trade Review.  Lytell also appeared in person, three times daily, at the Strand Theater in Milwaukee, Wisconsin|Milwaukee, and his appearance "boosted the box office receipts way up...and   was given a most generous welcome by his audiences".  A review in the Motion Picture Herald|Exhibitors Herald from 1923, said "this is a mighty good crook picture, be sure and let your people know what you have, as the name hurts business".
  
 stationer used pictures of scenes from the film for a window display. Blue cards were placed beside the items for sale, with advertising slogans that read: "We carry Check Protective Ink to guard your signature against the clever Alias Ladyfingers", and another slogan read, "Even on your desk your valuable papers should be kept in our Steel Bond Boxes for fast is the work of the visiting Alias Ladyfingers". 

==References==
 

==External links==
 
* 


 
 
 