What Have I Done to Deserve This? (film)
 
{{Infobox film
| name = What Have I Done to Deserve This?
| image = Que hecho yo para merecher esto.jpg
| image_size = 
| alt = 
| border = yes
| caption = Theatrical release poster
| director = Pedro Almodóvar
| producer = Hervé Hachuel
| writer = Pedro Almodóvar Gonzalo Suarez
| music = Bernardo Bonezzi
| cinematography = Angel L. Fernandez
| editing = José Salcedo
| studio = Kaktus Producciones Cinematográficas Tesauro S.A.
| distributor = Cinevista  
| released =  
| runtime = 101 minutes  
| country = Spain
| language = Spanish German English French
| budget =  ESP 116,730,182  (United States dollar|USD$901,508)
}}
What Have I Done to Deserve This? ( ) is a 1984 Spanish comedy-drama film by Pedro Almodóvar. The title is sometimes given with an exclamation mark at the end rather than a question mark.
 telekinetic child. The film, set in the tower blocks around Madrid, depicts female frustration and family breakdown, echoing Jean-Luc Godards Two or Three Things I Know About Her but with Almodóvars unique approach to filmmaking. Carmen Maura has often worked for Almodóvar, she became a synonym for a strong woman and also a gay icon for her role as a transsexual in Law of Desire.

==Plot==
The film is a slice of a housewifes life in 1980s post-Franco Madrid: Gloria lives in a small and cheap apartment with husband and two teenage children: the declared homosexual Miguel, whom she sells to the dentist, and the drug-dealing Toni. Her husband, Antonio, longs for a German woman for whom he was a chauffeur in Germany before he married Gloria.

Glorias mother-in-law, a frugal and unhappy woman who hides her Magdalena cakes and desperately wants to move back to her native countryside, is another addition to Glorias household. Cristal, a prostitute who lives next door, is Glorias best friend.

When Glorias husband assaults her in the kitchen, she accidentally kills him after hitting him with a ham leg. Her mother-in-laws lizard, Money, is the only witness to the crime. Gloria successfully evades the investigators and is never tied to the murder. There are many subplots intertwined with the main narrative with characters including a writer, his beauty-obsessed wife and an impotent policeman frustrated by his lack of sexual power.

==Cast==
* Carmen Maura as Gloria
* Ángel de Andrés López as Antonio
* Verónica Forqué as Cristal Gonzalo Suarez as Lucas Villalba
* Miguel Ángel Herranz as Miguel
* Juan Martínez as Toni
* Cecilia Roth as Chica anuncio
* Ryo Hiruma as Professor Kendo
* Pedro Almodóvar as Playback La bien pagá Tinin Almodovar as Cajero

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 