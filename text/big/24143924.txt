Uno (film)
Uno Norwegian drama Amanda Award for Best Director.

==Plot==
The film centers around a group of young men whom reside in an area of Oslo that is predominantly inhabited by immigrants. Best friends David and Morten work as gym instructors at Jarles gym. Jarle is a sadistic small-time criminal, who, together with his son Lars, purchases and distributes anabolic steroids. Lars has ties with a notorious criminal Pakistani gang led by Khuram. The climax of the film takes place after Lars, Morten and David are arrested for possession of illegal drugs. David chooses to "snitch" on his friends in order to visit his dying father. The story escalates when Lars uses his influence on the Pakistani gang to retaliate. Lars also informs Khuram about Mortens alleged sexual intercourse with Khurams sister, viewed as dishonourable by the Sharia law. The plot leaves the two best friends in a series of events that force them to run for their lives.

==Cast==
*Aksel Hennie as David
*Nicolai Cleve Broch as Morten
*Bjørn Floberg as Jarle
*Espen Juul Kristiansen as Kjetil
*Ahmed Zeyan as Khuram
*Martin Skaug as Lars

==References==
 

==External links==
 

 
 
 
 


 