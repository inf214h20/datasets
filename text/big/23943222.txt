Evil Things
{{Infobox film
| name           = Evil Things
| image          = 
| caption        = UK DVD Cover
| director       = Dominic Perez
| producer       = Dominic Perez Mario Valdez Steckler
| writer         = Dominic Perez
| editor         = Dominic Perez
| starring       = Laurel Casillo Morgan Hooper Ryan Maslyn Elyssa Mersdorf Torrey Weiss Gail Cadden
| music          = 
| cinematography = Laurel Casillo Moran Hooper Ryan Maslyn Elyssa Mersdorf Dominic Perez Mario Valdez Steckler Torrey Weiss
| editing        = 
| studio         = Go Show Media
| distributor    = Plum County Pictures
| released       = 2009
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| followed_by    =
}} American horror film written and directed by Dominic Perez  as his feature film debut.  

==Plot==
Five college students leave New York City for a weekend in the country, and 48&nbsp;hours later they vanished without a trace.

To celebrate Miriams (Elyssa Mersdorf) birthday, Miriams Aunt Gail (Gail Cadden) gives her use of a large country home in the Catskills for the weekend. Miriam invites her friends Cassy (Laurel Casillo), Mark (Morgan Hooper), Tanya (Torrey Weiss) and Leo (Ryan Maslyn) to join her to celebrate.

As an aspiring filmmaker, Leo brings his new video camera, hoping to create a short documentary of the weekend getaway. The five begin driving to the house. Whilst looking for a place to pull over because Tanya is carsick, the group notice they are being bothered by a dark red van whose driver incessantly honks his horn at them and overtakes them only to slow down in front of them. They pass the van and continue on. They stop at a small gas station where Cassy notices a dark red van pull in and slow down.

Spooked, the gang leave. As they are driving away, a girl from inside the gas station stops them to hand over a phone Cassy left in the bathroom. The group continue to Aunt Gails home and are again tailed by the van. They stop at a diner and while eating, the van pulls into the diner parking lot and drives slowly by the window. Furious, Mark storms outside to confront the driver but he drives away when Mark gets too close.

The group eventually make it to the house and Aunt Gail comes to turn the power on and wish them a good night. The five surprise Miriam with a birthday cake and then party and drink with Leo filming the whole occasion.

The next day, the group take a hike to the snowy woods which they soon get lost in the dark. They hear noises that they cant identify, crackling sounds on their two-way-radio and branches snapping which scares them all into a run. They do eventually make it back to the house without further incident. Later when eating dinner, they receive phone calls with no one answering. A knock at the door is heard and Mark finds a video tape wrapped in brown paper on the front step.

The tape reveals that the group have been being secretly video taped since they were on the road, and are being stalked by the same person in the maroon van. There is footage of them at the gas station with the girl running out to give Cassy back her phone, and them at the diner when Mark attempted to confront the driver of the van, proving it is the same van who has been following them the whole time. Then the footage follows them to Aunt Gails house and shows film taken through the windows of the group laughing and having fun, surprising Miriam with her birthday cake, and to their horror, the stalker inside the house filming them all as they slept.

The phone rings again with no answer, and then the line is cut. No one can get a signal on their cell phones. The group decide to leave but when they run out to the car they see that it is missing. A van pulls up in the driveway, scaring the group back into the house. As everyone tries to get a signal on their phones again, all the power in the house goes out. Miriam finally gets a signal on her phone and dials 9-1-1, but the call drops out.

Down the hall, the group hear a noise. Mark gets a knife and goes to investigate. He finds a two way radio that is on and crackling. The door suddenly shuts from the inside and Marks cries of pain can be heard. Cassy bangs on the door and tries to open it, and then it opens just a crack which scares the group into running upstairs to hide.

Upstairs, the group see the van driving away from the house so they come back downstairs to leave. Leo gives the camera to Tanya and leads the way out, but once outside he sees something that makes him scream at the others to run back inside. Once inside, Tanya falls breaking the camera and all left is the audio sound of Tanya screaming and her and Leos fate is left unknown.

On the video footage of the van outside, Miriam is shown to be running from the house. The van turns on its lights and creeps along to follow her. The van stops and the stalker gets out and chases a screaming Miriam into the woods.

Back inside the house, the other stalker is looking around the house for Cassy with his nightvision on the camera. He finds Cassy hiding behind a couch and, believing she is alone, creeping out wandering blindly around the home. The stalker follows her watching how far she will get. Cassy gets to the door but, it is pulled shut from the outside, and the camera man makes the same sound the group heard in the woods alerting Cassy of his presence. She screams as he then lunges out at her, and the camera freezes on Cassys screaming face.

The view pulls back and shows to a dark room were the stalker is watching several videos on many monitors. As well as the stalkers footage, there is also the footage Leo shot implying the stalkers stole his camera.

The last piece of film shows the stalker with the camera in a park, surveying groups of friends. While looking around he spots another group of friends filming. He then follows them on their trip.

During the end credits the video of the stalkers movements are shown from the moment he first spotted the group on the highway, following them to the house, filming them through the windows and while they sleep. Many of the close up shots focus solely on Cassy.

==Background==
Shot in found footage mockumentary style in the valleys surrounding New York City, the film is about a group of twenty-somethings in a relatives vacation home, captured as real footage on a video camera, with the actors themselves serving as cinematographers. The film took three months to write, seven days to shoot, and one month to edit. 

==Release== British premiere on August 29, 2009 in Leicester Square as part of Channel 4s London FrightFest Film Festival.   There are plans for worldwide release of the film on DVD and Blu-ray Disc in 2011.

==Critical reception==
  wrote that the film was meandering, aiming for authenticity in its scares but missing the mark, commenting that it was the "free-form dialogue from each of the five victims forces the authenticity; raising and lowering the tension immediately and in the process spiralling as far from reality as possible."  However, they did note the film was well shot: "for a handheld piece the frozen vistas are quite remarkable and the winter holiday home is an excellent set piece for that unmistakable feeling of middle of nowhere," but concluded that despite the location, once the actors begin their dialog, "the film loses its gravitas." The website concluded that the film "offers an interesting end in the form of the killers point of view, but the nifty convention is ultimately lost in the mediocrity that came before it."  JoBlo.com compared the film to both Blair Witch and Lord of the Rings "because for the better part of the movie, nothing much happens. But when the shit does hit the fan, it does so in a grand fashion that leaves you retarded on the floor and cowering in fear/excitement". They noted that while much of the films early dialog was tedious and boring, the final 30&nbsp;minutes when the reviewer felt that as a found footage|hand-held film, it finally delivered the excitement that was anticipated. They wrote that the actors did a great job of leading up to the final moments, doing "great job of carrying the film, and keeping the audience intrigued right to the final screams," offering that "the horror of the payoff here feels personal, like it is actually happening, which is hard to accomplish in any movie". The reviewer also appreciated how the film was put together, as if edited by the killer himself after his deeds were done, with additional footage seemingly shot by the kiler himself edited into that taken by his victims.  Action Flick Chick wrote that the film was a "slow starter", but that the "ending does give you a little bit of a creep out factor, though."  Horror review site Life After Undeath panned the movie stating that "Evil Things is every mock documentary ever made" and that they "can only recommend Evil Things for people who think The Ils Witch Activity would make a good movie, and then only if you liked the worst parts of each of those movies." 

==References==
{{reflist|refs=
   
   
   
   
   
   
   
   
   
}}

==External links==
*  
*   at the Internet Movie Database
*   at the Rotten Tomatoes
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 