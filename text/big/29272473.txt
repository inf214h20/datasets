The Untitled Kartik Krishnan Project
{{Infobox film
| name = The Untitled Kartik Krishnan Project
| image = The Untitled Kartik Krishnan Project Poster1.jpg
| caption =  The Untitled Kartik Krishnan Project Poster
| studio = Enter Guerrilla Productions
| director = Srinivas Sunderrajan
| cinematography = Hashim Badani
| editing = Srinivas Sunderrajan
| writer = Srinivas Sunderrajan, Vijesh Rajan
| music = Sujil Sukumaran
| released =  
| runtime = 75 minutes
| country = India
| language = Hindi
| budget =   40,000  (1000 USD$)
}} independent filmmaker English drama-thriller Mumble Core film.    The film was shot entirely on location in Mumbai, India. It was screened at the 2010 Mumbai International Film Festival in the New Faces of Indian Cinema Section on October 26, 2010  and on June 5, 2011 at Transylvania International Film Festival in competition 

==Synopsis==
In a bustling office in Mumbai, Kartik Krishnan (Kartik Krishnan) sits behind his desk coding HTML websites when he chances upon a blog on cinema featuring Independent filmmakers. This sparks his intentions to make his own short film, thus leading him to contact Srinivas Sunderrajan (Vishwesh K), an independent filmmaker who had met Kartiks idol and famed director Quentin Tarantino at an international film festival, who in turn agrees to guide Kartik through the process. 

As Kartik begins piecing the story, cast and screenplay together, several inexplicable phenomena start emerging in his life including a sinister stalker (D Santosh) dressed in official government clothing; and the sudden appearance of a strange future-telling antique toy. 

Troubled with these bizarre developments and the unresolved feelings that he harbours for Swara Bhaskar (Swara Bhaskar), his office colleague, Kartik embarks on an extraordinary journey that transcends love, life and logic itself.

==Cast==
* Kartik Krishnan as Kartik Krishnan
* Vishwesh K as Srinivas Sunderrajan
* Swara Bhaskar as Swara Bhaskar & Maya
* D Santosh as System

==Production==
The film was shot on weekends over nearly a year since most of the actors had day jobs and they could only afford to spare the weekend. Also, since there were no location permits, the cast and crew used to sometimes end up without a location for a week, which meant the shoot had to be shifted to the next week.  Since the cast were all good friends of Sunderrajan, they agreed to work for free.  A lot of the time stretch happened because of location unavailability. And when the location was ready, the actors would be unavailable due to other commitments. True to guerrilla filmmaking, many times the extras and the crowd in the frames did not even know that shooting was being done.

At any given point of time, only the director, the cinematographer and the lead actor, and depending on which location was being filmed - the secondary character was present on the sets. Mid-way through shooting the original script, the main location (the protagonists house) collapsed. One day the building was there, and the next day the third floor caved in on the second. And so the location was cordoned off, even though 60 percent of shooting was still left to be done at that location. 

The script was outlined, yet completely improvised by the actors with the director often giving the actors a free hand. In keeping with its meta fiction genre, the films characters were named after the actors who played them and many instances in the film have been based on real events which transpired between the cast and crew.
 Macbook (in-between shooting days) and later the post production, VFX & Sound Design (Vijesh Rajan) of the film was done in a one month time-span on a home computer so that it could be sent out within several film festivals deadlines. After seeing an initial screening, director Anurag Kashyap helped to promote the film. 

==Festivals==
* In Competition, Berlin Asian Hot Shots Film Festival 2010 
* New Faces, Mumbai International Film Festival 2010 
* Dramatic Competition, South Asian International Film Festival, New York 2010 
* In Competition, Transylvania International Film Festival, Cluj-Napoca, Romania 2011 

==References==
 
16. ^ Shalini Seth (February 12, 2011). " ". Yuva. Retrieved March 28, 2011.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 