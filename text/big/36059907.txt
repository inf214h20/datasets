Mohabat Ka Paigham
{{Infobox film
| name           = Mohabat Ka Paigham
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Padmini
| producer       = Mrs. Rani S. Kapila	
| writer         = 
| screenplay     = Vikas Anand	
| story          = Vikas Anand	
| based on       =  
| narrator       = 
| starring       = Shammi Kapoor Raj Babbar Aditya Pancholi Meenakshi Seshadri
| music          = Bappi Lahiri
| cinematography = Ashok Gunjal
| editing        = Omkarnath Bhakri	 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Padmini and produced by Mrs. Rani S. Kapila. It stars Shammi Kapoor, Raj Babbar, Aditya Pancholi and Meenakshi Seshadri in pivotal roles. 

==Cast==
* Shammi Kapoor...Chaudhary Abdul Rehman
* Raj Babbar...Nadeem Ur Rehman
* Aditya Pancholi...Naeem
* Meenakshi Seshadri...Zeenat Banu
* Ranjeet...Raja
* Satyendra Kapoor...Dinu
* Shammi...Chand Bibi
* Yunus Parvez...Manzoor

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Do Jahan Wale Tujhe"
| Mohammed Aziz
|-
| 2
| "Pyar To Pyar Hai"
| Mohammed Aziz
|-
| 3
| "Mehfil Ho Dil Walon Ki"
| Kumar Sanu, Sudesh Bhosle, Johny Whisky
|-
| 4
| "Ishq Hai Aisa Deewanapan (Sad)"
| Mohammed Aziz
|-
| 5
| "Nache More Man Mandir Mein"
| S. Janaki, Anoop Jalota
|}
==External links==
* 

 
 
 

 