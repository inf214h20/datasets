Taza, Son of Cochise
{{Infobox film
| name           = Taza, Son of Cochise
| image          =Taza, Son of Cochise (movie poster).jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Douglas Sirk
| producer       = Ross Hunter
| writer         = Gerald Drayson Adams (story) George Zuckerman (screenplay)
| starring       = Rock Hudson Barbara Rush.
| cinematography = Russell Metty
| editing        = Milton Carruth	 	
| distributor    = Universal Pictures
| released       =  
| runtime        = 79 mins.
| country        = United States English
}} 1954 western western film directed by Douglas Sirk and starring Rock Hudson and Barbara Rush. The film was shot in 3-D film|3D, but only released in 2D at the time.

==Plot synopsis== Taza (Rock Chiricahua Apaches on a reservation, where they are soon joined by the captured renegade Geronimo, who is all it takes to start a war.

==Cast==
*Rock Hudson as Taza
*Barbara Rush as Oona
*Gregg Palmer as Capt. Burnett
*Rex Reason as Naiche
*Morris Ankrum as Grey Eagle
*Eugene Iglesias as Chato
*Richard H. Cutting as Cy Hegen Ian MacDonald as Geronimo Robert Burton as Gen. George Crook
*Joe Sawyer as Sgt. Hamma
*Lance Fuller as Lt. Willis Bradford Jackson as Lt. Richards
*James Van Horn as Skinya Charles Horvath as Kocha Robert Hoy as Lobo Jeff Chandler as Cochise

==External links==
* 

 
 


 
 
 
 
 
 
 
 


 