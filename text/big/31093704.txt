Cavedweller (film)
{{Infobox film
| name           = Cavedweller
| image          = 
| image_size     = 
| caption        = 
| director       = Lisa Cholodenko
| writer         = 
| narrator       = 
| starring       = Kyra Sedgwick
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2004
| runtime        = 101 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} novel of the same name by Dorothy Allison. It stars Kyra Sedgwick and Aidan Quinn. It was nominated for numerous awards in 2004 and 2005. 

==Cast==
*Kyra Sedgwick as Delia Byrd
*Aidan Quinn as Clint Windsor
*Sherilyn Fenn as MT
*Jill Scott as Rosemary 
*Vanessa Zima as Amanda Windsor

==References==
 

==External links==
* 

 

 
 