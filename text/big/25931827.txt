Sun in Your Eyes
{{Infobox Film
| name           = Sun in Your Eyes
| image          = 
| image size     = 
| caption        = 
| director       = Jacques Bourdon
| producer       = Eric Schlumberger
| writer         = Jacques Bourdon Dominique Aury Michèle Perrein Eric Schlumberger
| narrator       = 
| starring       = Anna Karina
| music          = 
| cinematography = Lucien Joulin
| editing        = 
| distributor    = 
| released       = 22 June 1962
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Sun in Your Eyes ( ) is a 1962 French romance film directed by Jacques Bourdon and starring Anna Karina.   

==Cast==
* Anna Karina - Dagmar
* Georges Descrières - Denis
* Jacques Perrin - Frédéric
* Nadine Alari
* Charles Blavette
* Jean-Luc Godard
* Jean Rochefort

==References==
 

==External links==
* 

 
 
 
 
 
 