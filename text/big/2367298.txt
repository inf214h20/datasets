If You Could See What I Hear
{{Infobox film 
| name           = If You Could See What I Hear
| image          = If You Could See What I Hear.jpg
| caption        = 
| director       = Eric Till
| writer         = Derek Gill Stuart Gillard
| producer       = Eric Till Gene Corman (executive producer)
| starring       = {{plainlist|
* Marc Singer
* R. H. Thomson
* Sarah Torgov Shari Belafonte Harper
}} Michael Lloyd Eric N. Tom Sullivan
| cinematography = Harry Makin
| editing        = Eric Wrate Cypress Grove Shelter Films
| distributor    = Ciné 360 Inc. Jensen Farley Pictures
| released       =     (Japan)     (United States) 
| runtime        = 103 minutes
| budget         = CAD $5,600,000 (estimated)
| preceded_by    =
| followed_by    =
| awards         =
| country        = Canada
| language       = English
}} Tom Sullivan, starring Marc Singer and Shari Belafonte, directed by Eric Till.

Tagline: The true story of a born winner!

==Plot summary==
Tom Sullivan is a blind college student who wants to be normal. When not in class, Tom hangs out with his friend, Sly, who does not treat him like a blind person. In fact, he goes out of his way to challenge Tom.  Tom likes to go jogging while Sly leads him on his bicycle.  Sly leads him past obstacles such as park benches, shouting out "Bench!" at the last moment so Tom has to jump over it.

On campus, Tom meets a black woman named Heather with whom he falls in love, but she breaks off the relationship because "the black and white thing" coupled with Toms blindness is too complicated for her. Crushed by Heathers abandonment and experiencing loneliness, Tom continues to struggle with himself, still denying that his blindness affects his "normalcy". Then he meets his future wife, Patti, and his life changes forever.

==Cast== Tom Sullivan
*R. H. Thomson ... Will Sly
*Shari Belafonte ... Heather Johnson
*Harvey Atkin ... Bert
*Helen Burns ... Mrs. Ruxton Douglas Campbell ... Porky Sullivan David Gardner ... Jack Steffen
*Nonnie Griffin ... Mrs. Steffen Sharon Lewis ... Helga
*Adrienne Pocock ... Blythe Steffen
*Sarah Torgov ... Patti Steffen 
*Greer Forward ... Stunt Double for Blythe Steffen (uncredited)

==Reception== Animal House. His idea of overcoming his handicap is to party all night." He and Gene Siskel selected the film as one of the worst of the year in a 1982 episode of Sneak Previews. 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 