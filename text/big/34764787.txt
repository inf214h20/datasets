The Song and Dance Man
 
{{infobox_film
| name           = The Song and Dance Man
| image          =File:Song and Dance Man poster.jpg
| imagesize      =
| caption        =Film poster
| director       = Herbert Brenon
| producer       = Adolph Zukor Jesse Lasky
| based on       =   Paul Schofield Tom Moore Bessie Love
| cinematography = James Wong Howe
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 mins.
| country        = United States Silent (English intertitles)
}}
The Song and Dance Man is a 1926 American silent film comedy-drama produced by Famous Players-Lasky and released through Paramount Pictures. It is based on a play by George M. Cohan and was directed by Herbert Brenon. A copy of the film is housed in the Library of Congress collection. It is missing reels 1 and 2, but reels 3–7 of the seven-reel film survive.      

==Plot==
"Happy" Farrell (Tom Moore (actor)|Moore) wants to be a famous song and dance man. He befriends a young dancer, Leola (Bessie Love|Love), who accompanies him to an audition. Leola is given a contract, but Happy is not. Happy changes careers and becomes successful, but still yearns to be a song and dance man. After three years, Leola plans to retire from dancing so that she can get married, but Happy returns to pursue his original dream. Leola is inspired by this, and her fiancé agrees to let her continue her dancing career.   

==Cast== Tom Moore as Happy Farrell
*Bessie Love as Leola Lane Harrison Ford as Joseph Murdock
*Norman Trevor as Charles Nelson
*Bobby Watson as Fred Carroll
*Josephine Drake as Jane Rosemond
*George Nash as Inspector Craig
*William B. Mack as Tom Crosby
*Helen Lindroth as Marsha Lane
*Jane Jennings as Ma Carroll

==Reception==
The film received mixed reviews. Although the actors were praised, the plot was criticized because the two leads were not reunited at the end of the film.  

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 