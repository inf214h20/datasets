Virus Undead
{{Infobox film
| name           = Virus Undead
| image          =
| alt            =
| caption        =
| film name      =
| director       = Wolf Wolff Ohmuthi
| producer       = Jie Lin
| writer         = Wolf Jahnke
| starring       = Philipp Danne Birthe Wolter Anna Breuer Nikolas Jürgens Marvin Gronen
| music          = Dominik Schultes Max Würden
| cinematography = Heiko Rahnenführer
| editing        = Robert Kummer
| studio         = Legendary Units
| distributor    = Barnholtz Entertainment
| released       =   }}
| runtime        = 95 minutes
| country        = Germany
| language       = English
| budget         =
| gross          =
}}
Virus Undead (also known as Beast Within) is a 2008 German horror film directed by Wolf Wolff and Ohmuthi, written by Wolf Jahnke, and starring Philipp Danne, Birthe Wolter, Anna Breuer, Nikolas Jürgens, and Marvin Gronen.  A group of medical students and locals in a small town suffer an outbreak of a mutated H5N1-like virus that causes people to turn into zombies.

== Plot ==
Professor Bergen, a famous medical researcher, discovers a vaccine to an H5N1-like viral pandemic.  However, his research indicates that the virus has mutated and caused increased aggression in the local birds.  While researching this phenomenon, he is attacked and killed by his test subjects, a flock of violent birds.  After missing the funeral, Robert, a medical student, returns to his childhood home to settle his grandfathers affairs.  He takes two friends along with him, fellow medical students  Eugen and Patrick, who have an antagonistic relationship.  When they arrive, Roberts ex-girlfriend, Marlene, confronts him and demands to know why he dumped her and left town.  As Robert and Marlene talk, Eugen and Patrick flirt with Marlenes friend, Vanessa.  Patrick invites the women to a party later that night despite Roberts protestations.  Later, the trio are harassed in turn by Bollman, the town bully, and Lieutenant Lehmann, an unfriendly cop.  When they finally arrive at Professor Bergens mansion, they discover the place is in near ruins.  Robert gives his friends free rein, and they raid the wine cellar.

When the women arrive at the mansion, Robert and Marlene reconcile.  Vanessa and Patrick continue their flirting, and leave the others to have sex.  Eugen, drunk on absinthe and depressed, attempts to commit suicide, but Robert stops him.  Bollman, who has been using roadkill in his local fast food delivery service, becomes infected with the mutated virus.  Bollman develops large boils, becomes increasingly savage and animalistic, and goes on a violent rampage at Professor Bergens mansion that culminates in his biting Patrick.  Patrick immediately falls ill, and Eugen taunts him with grim medical diagnoses, such as amputation.  Bollman, thought dead after the struggle, suddenly reappears and resumes his attacks.  Robert is forced to shoot him dead with a pistol.  Shaken, Robert volunteers to seek help, and Marlene joins him.  Eugen and Vanessa burn Bollmans body and watch over Patrick, though Eugen secretly continues his taunts and threatens to kill Patrick.  When Robert and Marlene see Lieutenant Lehmann murder and attempt to cannibalize a local, they retreat back to the mansion.

Robert and Marlene, who is a biology student, use Professor Bergens lab to analyze the virus.  Robert urges Eugen to help, but Eugen dismisses their research as pointless and outright advocates killing Patrick.  When the semiconscious Patrick saves them from a stray villager who has entered the mansion, the others refuse to kill him; instead, they bind him upstairs and continue their research.  Eugen angrily leaves when Robert suggests that he, too, is infected, and Vanessa flees into the nearby woods.  As Eugen is in the process of turning, he helps Vanessa defeat several infected villagers, but he turns hostile.  In a moment of clarity, he commits suicide.  Robert and Marlene flee to the top of the mansion, where they realize theres no escape but to jump.  Robert survives the fall, but Marlene does not.  As dawn breaks, soldiers storm the property, kill the infected townspeople, and take Robert in for testing.  Convinced that Robert is uninfected, a rival scientist to Professor Bergen confidently asserts that they will have the situation under control soon, but Robert disputes that the virus can be stopped.

== Cast ==
* Philipp Danne as Robert Hansen
* Birthe Wolter as Marlene Vogt
* Anna Breuer as Vanessa Lux
* Nikolas Jürgens as Eugen Friedrich
* Marvin Gronen as Patrick Schubert 
* Ski as Bollmann
* Axel Strothmann as Polizist Lehmann
* Joost Siedhoff as Professor Bergen

== Production ==
Virus Undead was shot in Berlin and Wandlitz, Brandenburg.  

== Release ==
The film premiered at the Shanghai International Film Festival on June 14, 2008.   Lionsgate Home Entertainment released it on video November 3, 2009. 

== Reception ==
Drive-in movie critic Joe Bob Briggs rated it 3/4 stars and said that Nikolas Jürgens "steals the movie".   Jeremy Blitz of DVD Talk rated it 3/5 stars and wrote, "The film doesnt break any new ground or provide us with a startling new take on the zombie genre, but it does its job admirably, and serves up an hour and a half of creepiness."   Annie Riordan of Brutal As Hell called it "one of the stupider and more boring zombie films I’ve sat through".   Corey Danna of HorrorNews.net called it "a solid way to spend a boring evening."   Mike Catalano of JoBlo.com rated it 1.5/4 stars and wrote, "It seemed to have a small collection of cool, if not original, ideas for a zombie movie, but never really developed anything to its full potential."   Peter Dendle wrote that it is "a cut above your usual low-budget zombie affair", but "the characters are over-written and unconvincing". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 