Amazing (film)
{{Infobox film
| name           = Amazing
| image          = Amazing film poster.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Sherwood Hu
| producer       = Shanghai Film Group Corporation  
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Huang Xiaoming   Amber Kuo   Carmelo Anthony   Scottie Pippen
| music          = Frank Fitzpatrick
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 110 minutes
| country        = China
| language       = Mandarin
| budget         = US$10,000,000 
| gross          = US$6,990,000 
}}

Amazing (Chinese: 神奇) is a Chinese sci-fi basketball film directed by Sherwood Hu released in 2013. The film is a sci-fi drama set in modern day Shanghai about an unexpected series of events that unfold when Chinas top programmer (Huang Xiao Ming) designs the worlds first thought-controlled virtual reality basketball game. It premiered at the Shanghai International Film Festival in June 2013  and released in China on September 30, 2013. 

==Cast==
* Huang Xiaoming
* Amber Kuo
* Carmelo Anthony
* Scottie Pippen
* Dwight Howard
* Eric Mabius
* Kim Ah-joong Huang Yi
* Stephen Fung
* Chen Chien-chou
* Yi Jianlian
* Wang Zhizhi

==Production==
Shanghai Film Group approached the NBA with the idea for a film centered around basketball and video games. The production company sent director Sherwood Hu to New York to make the sales pitch. NBA officials were enthused about the idea and arranged for Anthony, Howard and Pippen to participate. Filming began in fall of 2010 in Shanghai with the scenes with the NBA players, who were available only for a few days each because of training schedules. production was halted for half a year while Hu polished the script and waited for some of his other actors to finish other films. Shooting resumed the following spring and wrapped in October 2011. Language was a key issue during the production because of the cast that included actors from the United States, China, South Korea, Taiwan and Hong Kong. 

==Music==
Amazing features musical performances by the China National Symphony Orchestra and lead soprano Ying Huang. The original score was composed by Frank Fitzpatrick. 

==References==
 

==External links==
*  

 
 
 
 
 
 


 