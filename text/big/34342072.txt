The Dancing Girl (film)
{{infobox film
| name           = The Dancing Girl
| image          =
| imagesize      =
| caption        =
| director       = Allan Dwan
| producer       = Adolph Zukor  Daniel Frohman
| writer         = 
| based on       =  
| starring       = Florence Reed
| music          =
| cinematography = H. Lyman Broening
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1915 silent film drama produced by the Famous Players Film Company and distributed by Paramount Pictures. It is based on an 1890s Broadway play by Henry Arthur Jones. The film was directed by Allan Dwan and starred stage actress Florence Reed in her film debut. Reeds husband, Malcolm Williams, also appears in the film.  

==Cast==
*Florence Reed - Drusilla Ives
*Fuller Mellish - David Ives
*Lorraine Huling - Faith Ives Malcolm Williams - A Quaker William Russell - John Christison
*Eugene Ormonde - Duke of Guiseberry
*William Lloyd - Mr. Crake
*Minna Gale - Lady Bawtry

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 