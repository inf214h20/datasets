Three Bewildered People in the Night
{{Infobox film
| name = Three Bewildered People in the Night
| image = 
| image_size = 
| alt = 
| caption = 
| director = Gregg Araki
| producer = Gregg Araki
| writer = Gregg Araki
| starring = Darcy Marta John Lacques Mark Howell
| cinematography = Gregg Araki
| editing = Gregg Araki
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = $5,000
}}
Three Bewildered People in the Night is a 1987 American drama film directed by Gregg Araki. The film follows three characters through the dissolution of a heterosexual relationship and the possible beginning of a gay one.

==Plot==
The film revolves around Alicia, a video artist, her live-in boyfriend Craig, a journalist and frustrated actor, and David, Alicias best friend and a gay performance artist. Through a series of telephone calls and coffee shop conversations, Craig and Alicia split up and Craig and David take tentative steps toward a relationship. 

==Cast==
* Darcy Marta as Alicia
* John Lacques as Craig
* Mark Howell as David

==Production==
Gregg Araki shot Three Bewildered People in the Night on a budget of $5,000. He shot in black and white using a spring-wound Bolex camera. The film is an example of guerrilla filmmaking, with Araki shooting in unauthorized locations without permits. 

==Reception==
Three Bewildered People in the Night won the Ernest Artaria Award at the 1989 Locarno International Film Festival.

==Notes==
 

==References==
* Levy, Emanuel (2001). Cinema of Outsiders: The Rise of American Independent Film. NYU Press. ISBN 0-8147-5124-5.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 