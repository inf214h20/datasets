Haunted Honeymoon
{{Infobox film
| name        = Haunted Honeymoon
| image       = Haunted_honeymoon.jpg
| caption     = Haunted Honeymoon promotional movie poster
| director    = Gene Wilder
| producer    = Susan Ruskin
| writer      = Gene Wilder Terence Marsh
| starring    = Gene Wilder Gilda Radner Dom DeLuise Jonathan Pryce Paul L. Smith John Morris
| editing     = Christopher Greenbury
| distributor = Orion Pictures
| released    =  
| runtime     = 82 min.
| country     = United States
| language    = English
| budget      = $13 million
| gross       = $8,033,397
}}

Haunted Honeymoon is a 1986 comedy movie starring Gene Wilder, Gilda Radner, Dom DeLuise, and Jonathan Pryce. Wilder also served as the films writer and director. The film also marked Radners final appearance prior to her death of ovarian cancer in 1989.
(The title Haunted Honeymoon was previously used for the 1940 U.S. release of Busmans Honeymoon (film)|Busmans Honeymoon based on the stageplay by Dorothy L. Sayers.)
==Plot==
Larry Abbot (Wilder) and Vickie Pearle (Radner) are performers on radios "Manhattan Mystery Theater" who decide to get married. Larry has been plagued with on-air panic attacks and speech impediments since proposing marriage. Vickie thinks its just pre-wedding jitters, but his affliction could get them both fired.

Larrys uncle, Dr. Paul Abbot, decides that Larry needs to be cured.  Paul decides to treat him with a form of shock therapy to "scare him to death" in much the same way someone might try to startle someone out of hiccups.

Larry chooses a castle-like mansion in which he grew up as the site for their wedding. Vickie gets to meet Larrys eccentric family: great-aunt Kate (DeLuise), who plans to leave all her money to Larry; his uncle, Francis; and Larrys cousins, Charles, Nora, Susan, and the cross-dressing Francis Jr. Also present are the butler Pfister and wife Rachel, the maid; Larrys old girlfriend Sylvia, who is now dating Charles; and Susans magician husband, Montego the Magnificent.

Paul begins his "treatment" of Larry and lets others in on the plan.  Unfortunately for all, something more sinister and unexpected is lurking at the Abbot Estates mansion. The pre-wedding party becomes a real-life version of Larry and Vickies radio murder mysteries, werewolves and all.

==Cast==
*Gene Wilder as Larry Abbot
*Gilda Radner as Vickie Pearle 
*Dom DeLuise as Aunt Katherine "Kate" Abbot
*Jonathan Pryce as Charles "Charlie" Abbot
*Bryan Pringle as Pfister, the Butler
*Peter Vaughan as Uncle Francis Abbot Sr.
*Paul L. Smith as Dr. Paul Abbot, uncle Jim Carter as Montego, the Magician
*Roger Ashton-Griffiths as Cousin Francis Jr.
*Ann Way as Rachel, Pfisters wife
*Sally Osborne as Mrs. Abbot (Larrys mother)
*Knebworth House as the castle

==Reception==

The film received negative reviews.   Dom DeLuise won the Razzie Award for Worst Supporting Actress for his performance in drag.

===Box office===

The movie was a financial flop, grossing only $8,000,000 in America, entering the box office at number 8, then slipping to 14 the following week. While Gilda Radner was struggling with cancer, she wrote the following about the film: "On July 26  , Haunted Honeymoon opened nationwide. It was a bomb. One month of publicity and the movie was only in the theaters for a week – a box-office disaster." 

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 