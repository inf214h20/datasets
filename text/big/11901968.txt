Big Money Rustlas
{{Infobox film
| name           = Big Money Rustlas
| image          = Big Money Rustlas.jpg
| image_size     = 175px
| caption        = 
| director       = Paul Andresen
| producer       = Chris Kraft
| story          = Joseph Bruce
| screenplay     = Joseph Bruce Paul Andresen Studebaker Duchamp Violent J Shaggy 2 Jamie Madrox Monoxide Jason Mewes Mark Jury
| music          = Jim Manzie Edgar Rothermich
| cinematography = Paul Andresen
| editing        = Pascal Leister Psychopathic Fontana Fontana Vivendi Vivendi
| released       =     
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $1.5 million 
| gross          = 
}}
 Western comedy Warner Bros. cartoons, and the film Blazing Saddles.

Gambling tycoon Big Baby Chips (Joseph Bruce), along with his assistants Raw Stank (Jamie Spaniolo) and Dusty Poot (Paul Methric|Monoxide), run the downtrodden town of Mud Bug through extortion and violence. Sheriff Sugar Wolf (Joseph Utsler) arrives in town to confront Big Baby Chips, redeem his family name, and save the town. The films tagline is "The Good, the Bad, and the ... Outrageous," a parody of the film The Good, the Bad and the Ugly. Big Money Rustlas was released direct-to-video on August 17, 2010. A theatrical tour to screen the film around the country is currently planned.

==Plot==
Sheriff Sugar Wolf (Utsler) returns to his hometown after many years to find that it has been taken over by Big Baby Chips (Bruce), a ruthless gambling tycoon who have run the downtrodden town of Mud Bug with his gang of thugs, which include Raw Stank (Jamie Madrox) and Dusty Poot (Monoxide), since killing Sugar Wolfs father, Grizzly Wolf (Ron Jeremy), and Sugars brothers. Sugar decides to take over the position his father once held, leading Big Baby Chips to pit Sugar against a series of deadly assassins.
 Bridget Powerz), little person and takes on a deputy, Bucky (Mewes). After Sugar Wolf jails Raw Stank and Dusty Poot, Big Baby Chips calls in his deadliest assassin, which turns out to be Tink, who, in reality, is a bearded man in drag, Tank (Jody Sadler), who cripples Sugar Wolfs shooting hand. Dirty Sanchez (Mark Jury), a former rival of Big Baby Chips, whose hands had been crippled by Big Baby Chips, trains Sugar Wolf to fight with his other hand. Sugar Wolf challenges Big Baby Chips to a showdown, and guns down the gambler, who reveals himself to be Grizzly Wolf.

==Cast==
*Joseph Bruce as Big Baby Chips
*Joseph Utsler as Sugar Wolf
*Jamie Spaniolo as Raw Stank Monoxide as Dusty Poot
*Jason Mewes as Bucky
*Mark Jury as Dirty Sanchez
*Boondox as The Ghost
*2 Tuff Tony as The Foot Bridget Powerz as Tink
*Brigitte Nielsen as Lady
*Eric Geller as Sheriff Fred Freckles
*Cindie Haynie as Mama Wolf
*Ron Jeremy as Grizzly Wolf
*Jody Sadler as Tank Scott Hall as Sign guy Jumpsteady as Hack Benjamin

==Production==
  }}

The films director, Paul Andresen, had previously worked with Joseph Bruce and Joseph Utsler on the long form music video Bowling Balls.  Andresen was enthusiastic to work on the film because of previous experiences with Psychopathic Records, and Bruces sense of humor.  Paul Methric described the production as being more organized than that of Big Money Hustlas.  According to Methric, "The first one was, lets shoot it 45 times, then shoot it another 45 times, where this one the guys knew exactly what we needed, and we got in there and got it." 
 Pro Wrestling Psychopathic Video department was still trying to learn how to work in the film industry. To better improve the quality of the film, Bruce said that Psychopathic Video had hired people from within the film industry to help with production. 

===Origins===
Bruce and Utsler had a negative experience with the New York style of production and "asshole art film crews" while filming of Big Money Hustlas.    Following the release of the movie, the two made plans to produce a Western prequel, which would be filmed in California in order to avoid the previous issues.  In the packaging of their 2000 album Bizaar, Insane Clown Posse released an image of what would later be the characters Raw Stank and Dusty Poot.

Bruce and Utsler continued to talk about the movie over the next eight years, though no actions were taken to start pre-production of the film. The two were encouraged to move forward with the production of Big Money Rustlas after starring in the 2008 film Death Racers. According to Bruce, "We knew we could do better and wed have a way bigger budget. And we thought, We can pull this off."   

===Story and setting=== Western films Warner Bros. Man with No Name Trilogy, as well as Mel Brooks Blazing Saddles, and that the latter was a strong influence on Big Money Rustlas, due to its comedic anachronisms.  Andresen and Bruce wanted the film to place more emphasis on humor than the over-the-top violence that Insane Clown Posses music is known for. 

===Casting===
  portrays Sugar Wolfs deputy sheriff Bucky.]] Corporal Robinson, Terry Brunk, Scott Hall, Scott DAmore, Jimmy Hart, and Joe Doering. Established actors who appear include Jason Mewes, Dustin Diamond, Tom Sizemore, Todd Bridges, Jimmie Walker, and Brigitte Nielsen.
 Violent J, Shaggy 2 Jamie Madrox, Jumpsteady were pornographic actress, Bridget Powerz, and his sidekick was played by an established actor, Jason Mewes.

The role of Sugar Wolf offered Shaggy 2 Dope much more room for improvisation than there had been in playing Sugar Bear in Big Money Hustlas, where all of his dialogue had to be performed in Dolemite-style rhymes; Shaggy 2 Dope was unsatisfied that he was unable to improvise in that film for this reason.  

===Filming=== Paramount Ranch in the Santa Monica Mountains, California, and concluded on February 24, 2009.       The films budget was $1.5 million.  During filming, Joseph Bruce was harassed by a police officer due to local law enforcements classification of Juggalos as a gang. 

==Release==
The films trailer premiered at the 10th annual Gathering of the Juggalos, where it was screened twice.  Bruce and Utsler plan to tour theaters around the country to screen the film.   Bruce also announced that the film would be sent in to multiple film festivals.  The film premiered at The Fillmore Detroit on January 23, 2010.    The DVD was released locally at the 11th annual Gathering of the Juggalos, and nationally on August 17, 2010. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 