Half a Hero
{{Infobox film
| name           = Half a Hero
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Don Weis
| producer       = Matthew Rapf
| writer         = Max Shulman
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Red Skelton Jean Hagen
| music          = Paul Sawtell
| cinematography = Paul Vogel
| editing        = Newell P. Kimlin
| studio         = MGM
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 mins.
| country        = United States
| language       = English
| budget         = $486,000  . 
| gross          = $891,000 
}}

Half a Hero is a 1953 American comedy film starring Red Skelton and Jean Hagen. Directed by Don Weis, the film was written by Max Shulman and released by Metro-Goldwyn-Mayer.

==Synopsis==
Freelance writer Ben Dobson (Skelton) who lands his first full-time writing job at a national magazine, tasked with rewriting other authors work.  His wife Martha (Hagen) uses this as the perfect time to start their family, and four years later pressuring Ben into moving from New York to the suburbs, where hes swiftly living beyond his means.  His boss then wants him to write a story on those suburbs, titled "slums of tomorrow." While Martha happily embraces her new environment and friendly neighbors, Ben is cynical about their life there, and decides they should return to the city.  However, while showing their home to another prospective buyer, he realizes he would miss the homes personal touches, and they should stay.

==Cast==
* Red Skelton as Ben Dobson
* Jean Hagen as Martha Dobson
* Charles Dingle as Mr. Bascomb
* Willard Waterman as Charles McEstway
* Mary Wickes as Mrs. Watts
* Frank Cady as Mr. Watts
* Hugh Corcoran as Pete Dobson
* Dorothy Patrick as Edna Radwell
* King Donovan as Sam Radwell
* Billie Bird as Ernestine
* Dabbs Greer as George Payson
* Kathleen Freeman as Welcomer
* Polly Bergen as Herself
==Reception==
According to MGM records the film earned $661,000 in the US and Canada and $230,000 elsewhere, resulting in a profit of $68,000. 
==References==
 
==External links==
* 
*  

 
 
 
 
 
 
 


 