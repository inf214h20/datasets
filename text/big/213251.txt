Alien 3
 
{{Infobox film
| name           = Alien 3
| image          = Alien3 poster.jpg
| caption        = Theatrical release poster
| director       = David Fincher
| producer       = {{Plain list|
* Gordon Carroll
* David Giler Walter Hill
}}
| screenplay     = {{Plain list|
* David Giler
* Walter Hill Larry Ferguson
}} Vincent Ward
| based on       =  
| starring       = {{Plain list|
* Sigourney Weaver
* Charles Dance
* Charles S. Dutton
* Lance Henriksen
 
}}
| music          = Elliot Goldenthal Alex Thomson
| editing        = Terry Rawlings
| studio         = Brandywine Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 115 minutes  145 minutes   
| country        = United States 
| language       = English
| budget         = $50–55 million       
| gross          = $158.5–159.8 million  
}} debut feature Alien organism was also aboard the escape pod. The Alien organism begins a killing spree in the prison.
 Vincent Ward at the helm was cancelled well into pre-production. Fincher had little time to prepare, and the experience of making the film proved agonizing for him. Besides the need to shoot and rewrite the script simultaneously while fitting in sets that had already been built, filming was also plagued by incessant creative interference from studio executives, who overruled many of Finchers decisions and dictated a large part of production. Fincher has since disowned the film, citing the aforementioned reasons. In 2003, a revised version of the film, known as the Assembly Cut, was released without Finchers involvement.

While under-performing at the United States box office, it earned over $100 million outside of North America.  The film received mixed reviews upon release, and was regarded as inferior to the previous installments.

==Plot==
  android Bishop. cryonic stasis cryotubes show millenarian version of Christianity. Ripley is warned by the prison warden, Harold Andrews (Brian Glover), that her presence among them may have extremely disruptive effects.

Suspicious of what caused the escape pod to jettison and what killed her companions, Ripley requests that Clemens perform an autopsy on Newt. She fears that Newt may be carrying an Alien embryo in her body, though she does not share this information. Despite protests from the warden and his assistant, Aaron (Ralph Brown), the autopsy is conducted. No embryo is found in Newts body, and Clemens proclaims she drowned when seawater flooded her cryotube. Meanwhile, Ripleys unusual behavior begins to frustrate the warden and is agitating the prisoners.

A funeral is performed for Newt and Hicks, during which their bodies are cremated in the facilitys enormous furnace. In another section of the facility, the prison dog   enters convulsions, and a larger than normal Alien bursts from its body. The Alien soon begins to attack members of the colony, killing several and returning outcast prisoner Golic (Paul McGann) to his former deranged state. To get answers, Ripley recovers and reactivates the damaged android Bishop, who confirms that there was an Alien on the Sulaco and it came with them to Fiorina in the escape pod. She then informs Andrews of her previous encounters with the Aliens and suggests everyone work together to hunt it down and kill it. Andrews does not believe her story and explains that the facility has no weapons. Their only hope of protection is the rescue ship being sent for Ripley by the Weyland-Yutani Corporation.
 Danny Webb) and Dillon. Dillon is the last victim; he fearlessly remains in the mold to distract the Alien, allowing it to tear him apart, as Morse pours the molten lead onto both of them. The Alien, covered in molten metal, escapes the mold but is quickly killed by Ripley when she turns on fire sprinklers and sprays the beast with water, causing its exoskeleton to cool rapidly and shatter via thermal shock.

While Ripley battles the Alien, the Weyland–Yutani team arrives, including a man who looks identical to the Bishop android, claiming to be its creator. He tries to persuade Ripley to undergo surgery to remove the Queen embryo, which he claims will be destroyed. Ripley refuses and steps back onto a mobile platform, which Morse positions over the furnace. The Weyland–Yutani team shoot Morse in the leg, prompting Aaron to pick up a large wrench and strike Bishop II over the head with it. Aaron is then shot dead, and Bishop II and his men show their true intentions, begging Ripley to let them have the "magnificent specimen". Ripley defies them by throwing herself into the gigantic furnace. The alien suddenly erupts from Ripleys chest. Ripley holds on to the alien so it cannot escape the inferno, and Ripley and the alien queen both fall into the fire to their deaths.

The facility is closed down and the last surviving inmate, Morse, is led away. A sound recording of Ripley (her final lines from the original Alien (film)|Alien) is heard from the Sulaco escape pod, "This is Ripley, last survivor of the Nostromo, signing off."

==Cast==
 
 
* Sigourney Weaver as Ellen Ripley, reprising her role from the previous two Alien films. Ripley crash-lands on Fiorina 161 and is once again burdened with the task of destroying another of the alien creatures.
* Charles S. Dutton as Dillon, one of Fiorinas inmates who functions as the spiritual and de facto leader amongst the prisoners and attempts to keep the peace in the facility.
* Charles Dance as Jonathan Clemens, a former inmate who now serves as the facilitys doctor. He treats Ripley after her escape pod crashes at the start of the film and forms a special bond with her. Before he is killed, Clemens laments to Ripley why he was originally sent to Fiorina, describing it as "more than a little melodramatic." Fincher initially offered the role to Richard E. Grant, hoping to reunite him with Withnail and I co-stars Ralph Brown and Paul McGann. 
* Brian Glover as Harold Andrews, the prison warden. He believes Ripleys presence will cause disruption amongst the inmates and attempts to control the rumors surrounding her and the creature. He rejects her claims about the existence of such a creature, only to be killed by it.
* Ralph Brown as Aaron, the assistant of Superintendent Andrews. The prisoners refer to him by the nickname "85", after his IQ score, which annoys him. He opposes Ripleys insistence that the prisoners must try to fight the alien, and repudiates her claim that Weyland-Yutani will collect the alien instead of them.
* Paul McGann as Golic. A mass-murderer and outcast amongst the prison population, Golic becomes very disturbed after being assaulted by the alien in the prisons underground network of tunnels, gradually becoming more and more obsessed with the alien. In the Assembly Cut of the film, his obsession with and defense of the creature lead to murder, and his actions jeopardize the entire plan. Danny Webb as Morse, an acerbic, self-centered, and cynical prisoner. Although he is wounded by a company guard, Morse is the only survivor of the entire incident.
* Lance Henriksen playing an unnamed character but credited as Bishop II, he appears in the films final scenes, claiming to be the human designer of the Bishop android. He wants the alien Queen that is growing inside Ripley for use in Weyland-Yutanis bioweapons division. He also reprises the role of Bishop from the previous movie, albeit in animatronic form.
* Tom Woodruff, Jr. as the Alien (creature in Alien franchise)#Dog Alien|Alien. Alien 3  audio commentary, Alien Quadrilogy boxset  This Alien is different from the ones in previous installments due to its host being quadrupedal (a dog in the theatrical cut, an ox in the assembly cut). Initially a visual effects supervisor, Woodruff decided to take the role of the creature after his company, Amalgamated Dynamics, was hired by Fox.    Woodruff said that, following Sigourney Weavers advice, he approaches the role as an actor instead of a stuntman, trying to make his performance more than "just a guy in a suit." He considered the acting process "as much physical as it is mental."   
* Pete Postlethwaite as David, an inmate smarter than most who is killed by the creature in the bait-and-chase sequence.
* Holt McCallany as Junior, the leader of the group of inmates who attempt to rape Ripley. He has a tattoo of a tear drop underneath his left eye. In the Assembly Cut, he sacrifices himself to trap the alien as redemption. Peter Guinness as Gregor, one of the inmates who attempts to rape Ripley, he is bitten in the neck and killed by the Alien during the bait-and-chase sequence.
* Danielle Edmond as Rebecca "Newt" Jorden, the child Ripley forms a maternal bond with in the previous film who briefly returns as a corpse being autopsied. Carrie Henn was unable to reprise her role as Newt as she was too old for the part so Danielle Edmond took over the role in this installment for the brief autopsy scene with Newts corpse.
* Christopher Fairbank as Murphy  Phil Davis as Kevin 
* Vincenzo Nicoli as Jude 
* Leon Herbert as Boggs 
* Christopher John Fields as Rains 
* Niall Buggy as Eric
* Hi Ching as Company Man 
* Carl Chase as Frank 
* Clive Mantle as William 
* DeObia Oparei as Arthur
* Paul Brennen as Troy
 

==Development==
 
 
 Walter Hill Aliens as biological weapons. treatment for the third film featuring "the underhanded Weyland-Yutani Corporation facing off with a militarily aggressive culture of humans whose rigid socialist ideology has caused them to separate from Earths society."  Michael Biehns Corporal Hicks would be promoted to protagonist in the third film, with Sigourney Weavers character of Ellen Ripley reduced  to a cameo appearance before returning in the fourth installment,  "an epic battle with alien warriors mass-produced by the expatriated Earthlings." Weaver liked the Cold War metaphor, and agreed to a smaller role,    particularly due to a dissatisfaction with Fox, who removed scenes from Aliens revealing Ripleys backstory that the actress considered crucial.   

 
Although 20th Century Fox were skeptical about the idea, they agreed to finance the development of the story, but asked that Hill and Giler attempt to get Ridley Scott, director of Alien (film)|Alien, to make Alien 3. They also asked that the two films be shot back to back to lessen the production costs. While Scott was interested in returning to the franchise, it did not work out due to the directors busy schedule. 

===1987-89===
In September 1987, Giler and Hill approached  .  Harlin wanted to go in different directions from the first two movies, having interest in both visiting the Alien home planet or having the Aliens invading Earth. 

Gibsons script was mockingly summed up by him as "Space commies hijack alien eggs - big problem in Mallworld".  The story picked up after Aliens, with the Sulaco drifting into an area of space claimed by the "Union of Progressive Peoples". The ship is boarded by people from the U.P.P, who are attacked by a facehugger hiding in the entrails of Bishops mangled body. The soldiers blast the facehugger into space and take Bishop with them for further study. The Sulaco then arrives at a space station-shopping mall hybrid named Anchorpoint. With Ripley put in a coma, Hicks explores the station and discovers Weyland-Yutani are developing an Alien army. In the meantime, the U.P.P. are doing their own research, which led them to repair Bishop. Eventually Anchorpoint and the U.P.P stations are overrun with the Aliens, and Hicks must team up with the survivors to destroy the parasites. The film ends with a teaser for a fourth movie, where Bishop suggests to Hicks that humans are united against a common enemy, and they must track the Aliens to their source and destroy them.  The screenplay was very action oriented, featuring an extended cast, and is considered in some circles as superior to the final film and has a considerable following on the Internet.    The producers were on the whole unsatisfied with the screenplay, which Giler described as "a perfectly executed script that wasnt all that interesting",  particularly for not taking new directions with the initial pitch. They still liked certain parts, such as the subtext making the Alien a metaphor for the HIV, but felt it lacking the human element present in Aliens and Gibsons trademark cyberpunk aesthetic. Following the end of the WGA strike, Gibson was asked to make rewrites with Harlin, but declined, citing various other commitments and "foot dragging on the producers part." 
 The Hitcher fall of Communism made the Cold War analogies outdated, Twohy changed his setting to a prison planet, which was being used for illegal experiments on the aliens for a Biological Warfare division.  Harlin felt this approach was too similar to the previous movies, and tired of the development hell, walked out on the project, which lead Fox to offer Harlin The Adventures of Ford Fairlane. 

Twohys script was delivered to Fox president Joe Roth, who did not like the idea of Ripley being removed, declaring that "Sigourney Weaver is the centerpiece of the series" and Ripley was "really the only female warrior we have in our movie mythology."  Weaver was then called, with a reported $5 million salary, plus a share of the box office receipts.  She also requested the story to be suitably impressive, original and non-dependent on guns. Twohy duly set about writing Ripley into his screenplay. 

===Start-up with Vincent Ward=== Vincent Ward. Ward, who was in London developing Map of the Human Heart,   only accepted the project on the third call as he at first was uninterested in doing a sequel. Twohys script was met with Wards derision, so he had another idea, involving Ripleys escape pod crash landing on a monastery-like satellite. Developing this pitch on his flight to Los Angeles, once Ward got with the studio executives he saw his idea approved by the studio. Ward was hired to direct Alien 3, and writer John Fasano was hired to expand his story into a screenplay.  Once Twohy discovered through a journalist friend that another script was being written concurrently to his, he went after Fox  and eventually left the project. 

Ward envisioned a planet whose interior was both wooden and archaic in design, where Luddite-like monks would take refuge. The story begins with a monk who sees a "star in the East” (Ripleys escape pod) and at first believes this to be a good omen. Upon arrival of Ripley, and with increasing suggestions of the Alien presence, the monk inhabitants believe it to be some sort of religious trial for their misdemeanors, punishable by the creature that haunts them. By having a woman in their monastery, they wonder if their trial is partially caused by sexual temptation, as Ripley is the only woman to be amongst the all-male community in ten years. To avoid this belief and (hopefully) the much grimmer reality of what she has brought with her, the Monks of the "wooden satellite" lock Ripley into a dungeon-like sewer and ignore her advice on the true nature of the beast.  The monks believe that the Alien is in fact the Devil. Primarily though, this story was about Ripleys own soul-searching complicated by the seeding of the Alien within her and further hampered by her largely solo attempts to defeat it. Eventually Ripley decides to sacrifice herself to kill the Alien. Fox asked an alternate ending where the character survived, but eventually Sigourney Weaver said she would only do the movie if Ripley died. 

Empire (magazine)|Empire magazine described Wards Wooden Planet concept as undeniably attractive – it would have been visually arresting and at the very least, could have made for some astonishing action sequences. In the same article, Norman Reynolds – Production Designer originally hired by Ward – remembers an early design idea for "a wooden library shaft. You looked at the books on this wooden platform that went up and down". Imagine the kind of vertical jeopardy sequence that could have been staged here – the Alien clambering up these impossibly high bookshelves as desperate monks work the platform.      Sigourney Weaver described Ward’s overall concept as "very original and arresting."   Former The Times|Times journalist David Hughes included Wards version of Alien 3 amongst "The Greatest Sci-Fi Movies Never Made" in his book of this title. 
 Jon Landau considered Wards vision to be "more bent on the artsy-fartsy side than the big commercial one" that Ridley Scott and James Cameron employed. Ward managed to dissuade the producers of their idea of turning the planet into an ore refinery and the monks into prisoners, but eventually Fox asked a meeting with the director imposing a list of changes to be made. Refusing to do so, Ward was fired. The main plot of the finished film still follows Wards basic structure. 

===Walter Hill and David Gilers script=== Larry Ferguson as a script doctor. Fergusons work was not well received in the production, particularly by Sigourney Weaver, who felt Ferguson made Ripley sound like "a pissed-off gym teacher". Short on time before filming was due to commence, producers Walter Hill and David Giler took control of the screenplay themselves, melding aspects of the Ward/Fasano script with Twohys earlier prison planet screenplay to create the basis of the final film.   Sigourney Weaver had also had a clause written into her contract stating the final draft should be written by Hill and Giler, believing that they were the only writers (besides James Cameron) to write the character of Ripley effectively.   Fox approached music video director David Fincher to replace Ward.  Fincher did further work on the screenplay with author Rex Pickett, and despite Pickett being fired and Hill and Giler writing the final draft of the screenplay, he revised most of the work done by the previous authors.

==Production==

===Filming===
  Alex Thomson replaced Cronenweth. 

===Visual effects===
Stan Winston, responsible for creature effects in Aliens, was approached but was not available. Winston instead recommended Tom Woodruff, Jr. and Alec Gills, two former workers of his studio who had just started their own company, Amalgamated Dynamics.  Even before principal photography begun, the practical effects crew was developing models of the Alien and the corpses of the Sulaco victims. Richard Edlunds Boss Film Studios was hired for compositing and other post-production effects.  A small number of shots contain computer-generated imagery, most notably the cracking alien head once the sprinklers cause thermal shock. Other CGI elements include shadows cast by the rod puppet alien, and airborne debris in outdoor scenes. 

David Fincher wanted the alien to be, “more of a puma, or a beast” as opposed to the upright, humanoid posture of the previous films, so the designer of the original alien, H. R. Giger, was contacted to generate new sketch ideas. His revisions included longer, thinner legs, the removal of “pipes” around the spine, and an idea for a sharp alien “tongue” in place of the secondary jaws. Working from his studio in Zurich, Giger produced these new sketches which he faxed to Cornelius de Fries who then created their model counterparts out of plasticine.    The only one of Gigers designs that wound up in the final project was a “Bambi Burster” Alien that had long legs and walked on all fours. ADI also built a full-scale Bunraku-style puppet of this design which was operated on-set as an in-camera effect. Scenes using this approach were cut from the final release due to the limitations of chemical compositing techniques -making it exceedingly difficult to remove the puppeteers from the background plate- but can be seen in “Assembly Cut” of the film. 
 rod puppet bluescreen and optically composited coverage of the quadrupedal alien, but the visual effects team was dissatisfied with the comical result and the idea was dropped in favor of the puppet. 

The rod-puppet approach was chosen for the production rather than stop motion|stop-motion animation which did not provide the required smoothness to appear realistic. As a result, the rod-puppet allowed for a fast alien that could move across surfaces of any orientation and be shot from any angle.     This was particularly effective as it was able to accomplish movements not feasible by an actor in a suit. The 1/3 scale puppet was 40 inches long and cast in foam rubber over a bicycle chain armature for flexibility.    For moving camera shots, the on-set cameras were equipped with digital recorders to track, pan, tilt, and dolly values. The data output was then taken back to the studio and fed into the motion control cameras with the linear dimensions scaled down to match the puppet. 

To make syncing the puppets actions with the live-action shots easier, the effects team developed an instant compositing system using LaserDisc. This allowed takes to be quickly overlaid on the background plate so the crew could observe whether any spatial adjustments were required. 
 motion control camera.  Depending on the complexity of the shot, the puppet was operated by 4-6 people.  Sparse sets were created as they provided freedom of motion for the puppeteers as well as large, solid surfaces for the puppet to act within a three dimensional space. 

The crew were pushed to make the movements of the Alien as quick as possible to the point where they were barely in control, and this led to, according to Edlund, “the occasional serendipitous action that made the alien have a character.” The ease of this setup allowed the crew to film 60-70 takes of a single scene. 

Hoping to give the destroyed Bishop a more complex look that could not be done by simple make-up, the final product was done entirely through animatronics, while a playback of Lance Henriksens voice played to guide Sigourney Weaver. 
 miniature against a blue-screen and composited onto large scale traditional matte paintings of the planets surface. To make the clouds glow from within as the EEV entered the atmosphere, the paintings values were digitally reversed and animated frame-by-frame. The scene in which the EEV is moved by a crane-arm (also a miniature) was created by projecting a video of actors onto pieces of cardboard and then compositing them into the scene as silhouettes against the matte painted background. 

===Music===
 
The films composer, Elliot Goldenthal, spent a year composing the score by working closely with Fincher to create music based primarily on the surroundings and atmosphere of the film itself. The score was recorded during the Los Angeles riots of 1992, which Goldenthal later claimed contributed to the scores disturbing nature. 

==Reception==

===Box office===
The film was released in the United States on May 22, 1992. The film debuted at number two of the box office, behind Lethal Weapon 3, with a Memorial Day weekend gross of $23.1 million. It screened in 2,227 theaters, for an average gross of $8,733 per theater. {{cite web
|title=Alien 3
|publisher=Box Office Mojo
|url=http://www.boxofficemojo.com/movies/?page=weekend&id=alien3.htm
|accessdate=2008-02-07}}  The film was considered a flop in North America with a total of $55.4 million, although it grossed $104.3 million internationally {{cite web
|title=Beauties and the Beast
|author=Hochman, David
|publisher=Entertainment Weekly
|date=December 5, 1997
|url=http://www.ew.com/ew/article/0,,290562,00.html
|accessdate=2008-10-12| archiveurl= http://web.archive.org/web/20081001002042/http://www.ew.com/ew/article/0,,290562,00.html| archivedate= October 1, 2008  | deadurl= no}}  for a total of $159.7 million. It is the second highest earning Alien film, excluding the effect of inflation, and had the 28th highest domestic gross in 1992. {{cite web
|title=1992 Domestic Gross
|publisher=Box Office Mojo
|url=http://www.boxofficemojo.com/yearly/chart/?yr=1992&p=.htm
|accessdate=2008-02-06}} 

===Critical reception===
The film initially received mixed reviews from critics, generally being negatively compared to the preceding two films in the franchise.  Review aggregation website Rotten Tomatoes gives the film a score of 44% "Rotten" based on 41 reviews. 
 Temple of Doom slap in the face" to him and to fans of the previous film. Biehn, upon learning of Hicks demise, demanded and received almost as much money for the use of his likeness in one scene as he had been paid for his role in Aliens. 

===Awards===
 

The films visual effects were nominated for an Academy Award, losing to Death Becomes Her. The film was also nominated for seven Saturn Awards and a Hugo Award.

The film was also nominated for an MTV Movie Award for Best Action Sequence. 

==Adaptations==
A novelization of the film was authored by  ." 

Dark Horse Comics also released a three-issue comic book adaptation of the film. 
 Super Nintendo/Super Mega Drive/Genesis, and Sega Master System. Rather than being a faithful adaptation of the film, it took the form of a basic platform action game where the player controlled Ripley using the weapons from the film Aliens in a green-dark ambient environment.  The Game Boy Alien 3 (handheld game)|version, developed by Bits Studios, was different from the console game, being a top-down adventure game. Sega also developed an arcade rail shooter loosely based on the films events, Alien 3: The Gun which was released in 1993.

==Home media== boxed sets throughout the 1990s. A VHS boxed set titled The Alien Trilogy containing Alien 3 along with Alien and Aliens was released in facehugger-shaped carrying cases, and included some of the deleted scenes from the Laserdisc editions. When Alien Resurrection premiered in theaters in 1997, another boxed set of the first three films was released titled The Alien Saga, which included a Making of Alien Resurrection tape. A few months later this set was re-released with the Alien Resurrection film taking the place of the making-of video. In 1999, Alien 3 was released on DVD, both singly and packaged with the other three Alien films as The Alien Legacy boxed set. This set was also released in a VHS version and would be the last VHS release of the film. In 2003, Alien 3 would be included in the 9-disc Alien Quadrilogy DVD set which contained two versions of the film (see below). The first three films were also later packaged as the Alien Triple Pack DVD set (this release was identical to the 1999 Alien Legacy set but excluding Alien Resurrection). Alien 3 was first released on Blu-ray in 2010, as part of the 6-disc Alien Anthology boxed set which included all of the features from the Alien Quadrilogy DVD set and more. The film was also released as a single Blu-ray disc in 2011.

The bonus disc for Alien 3 in the 2003 Quadrilogy set includes a documentary of the films production,  that lacks Finchers participation as clips where the director openly expresses anger and frustration with the studio were cut.  The documentary was originally named "Wreckage and Rape" after one of the tracks of Goldenthals soundtrack, but Fox renamed it simply "The Making of Alien 3". These clips were restored for the 2010 Blu-ray release of the Anthology set, with the integral documentary having a slightly altered version of the intended name, "Wreckage and Rage". 

===2003 Assembly Cut===
An alternate version of Alien 3 (officially titled the "Assembly Cut")  with over 30 minutes of additional footage was released on the 9-disc Alien Quadrilogy box-set in 2003, and later in the Alien Anthology Blu-ray set in 2010. The films extended footage includes alternate key plot elements, extended footage and deleted scenes. Director David Fincher was the only director from the franchise who declined to participate in the box-set releases.   

The "Assembly Cut" has several key plot elements that differ from the theatrical release. The alien gestates in an ox rather than a dog, and one of the inmates discovers a dead facehugger. Ripleys unconscious body washes up on the shore of the planet in the Assembly Cut instead of being found in the ships wreckage as in the theatrical cut. Some scenes are extended to focus more on the religious views of the inmates. The inmates succeed in their attempt to trap the alien, but it is later released by the disturbed inmate Golic. In this version, the alien Queen does not burst from Ripleys chest as she falls into the furnace. There is also a scene in the prisons assembly hall in which one of the inmates suggests to Dillon that they lead the creature to the furnace so that they can incinerate it. One notable scene that was not restored for the DVD or Blu-ray extended versions was the full autopsy scene. Greg Cannom, who worked on the special make-up effects, said in the Alien Quadrilogy special features that, "I saw the rough cut of the film, uncut, and there were some scenes in there that were pretty gross. There was an autopsy scene on the girl   and I like certain gore in the films. I do it  , and it made me sick. It really grossed me out and I remember people got up and left, walked out of the theatre and I was just thinking, This will never be in the film. They cant show this stuff. It was just too much I thought. And when the film came out, it wasnt in the film."
 ADR by the original actors was not recorded for this footage, since it had been cut from the film by the time the film was being dubbed.  For the 2010 Alien Anthology release, this dialogue was re-recorded by the original castmembers, making it on par with the original theatrical footage. 

==Further reading==
Gallardo C., Ximena; and C. Jason Smith (2004).  . Continuum. ISBN 0-8264-1569-5

==See also==
*List of monster movies
 

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 