Work Weather Wife
{{Infobox film
| name           = Work Weather Wife
| image          = Official_poster_Work_Weather_Wife.jpg
| caption        = Official poster Harpreet Sandhu
| producer       = Harpreet Sandhu, Reema Nagra
| writer         = Harpreet Sandhu
| starring       = Harpreet Sandhu Reema Nagra Dilbag Brar Kirat Bhatthal
| music          = Harpreet Sandhu, Sukhjinder Alfaaz
| distributor    = StarGauge Films Inc.
| released       =  
| country        =  
| language       = Punjabi
}}
 Harpreet Sandhu Harpreet Sandhu.         

==Plot==
Vick tricks his way into Dimple and CJs fragile family. The film outlines the major community issue of honour killings. CJ, who belongs to the working class, doesnt have time for the daily household routine, and Vick outsmarts CJ on a scam.   
==Cast== Harpreet Sandhu NRI living in Vancouver, BC
* Reema Nagra as Dimple, an NRI living in Vancouver, BC
* Dilbag Brar as CJ, Dimples husband
* B K Singh Rakhra as Vicks father
* Mani Kahlon as Bobby, Vicks friend
* Eline Mets as Amber
* Abi Centrik as Boy
* Kirat Bhatthal as Gugni, CJs daughter
* Jagmohan Bhandari as Psychiatrist
* Monsoon Sondhi as Vicks sister
* Sukhi Waraich as Vicks sisters boyfriend
* Alicia Karl as Monique

==Soundtrack==
{{Infobox album
| Name = Work Weather Wife
| Type = Soundtrack Harpreet Sandhu
| Cover = Official poster WWW.jpg
| Released =  
| Recorded = ArtGauge Films Inc. Hindi Film Soundtrack
| Label = ArtGauge Films Inc. Harpreet Sandhu | Reema Nagra Harpreet Sandhu | Sukhjinder Alfaaz
}}
 Harpreet Sandhu, Harpreet Sandhu.   

{{Track listing
| extra_column = Singers
| title1 = Wahgan Hawaawan Harpreet Sandhu,Dilbag Brar
| length1 = 4:00
| title2 = Bewafa
| extra2 = Sukhjinder Alfaaz
| length2 = 4:55
| title3 = Chann, (English title "Moon")
| extra3 = Alka Yagnik, Arsh Avtar
| length3 = 7:20
| title4 = Jugni
| extra4 = Dilbag Brar
| length4 = 5:57
| title5 = Jugni Sad
| extra5 = Dilbag Brar
| length5 = 1:50
| title6 = Pyar De 1
| extra6 = Dilbag Brar
| length6 = 3:41
| title7 = Pyar De 2
| extra7 = Dilbag Brar
| length7 = 2:57
| title8 = Zamane Musical Harpreet Sandhu
| length8 = 3:05
| title9 = Lambi Gutt (English title "Long Braid")
| extra9 = Dilbag Brar
| length9 = 3:22
| title10 = Dil
| extra10 = Sukhjinder Alfaaz
| length10 = 3:54
}}

==References==
 

== External links ==
*  
*  
*  
*  
* 
* 
*  
 
 
 