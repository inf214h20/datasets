Hood of the Living Dead

{{Infobox film
| name           = Hood of the Living Dead
| image          = 
| caption        = 
| director       = Eduardo Quiroz   Jose Quiroz
| producer       = Quiroz Bros.
| writer         = Eduardo Quiroz   Jose Quiroz
| screenplay     = 
| story          = 
| based on       =  
| starring       = Carl Washington   Brandon Daniels   Jose Rosete
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hood of the Living Dead is a 2005 direct-to-video zombie movie about a regenerative formula bringing the dead back to life. 

==Synopsis==

Ricky (Carl Washington) is a young scientist living in a poor and violent section of Oakland with his brother Jermaine (Brandon Daniels). Since his parents died, Ricky has been working hard at a laboratory trying to perfect a regenerative formula that could heal dead cells in animals.  
Ricky disapproves of Jermaines friends Marco (Raul Martinez) and Kevin (Derek Taylor II) and tells his brother that they will be moving to a safer community at the end of the month. While out with his friends one night, Jermaine is threatened by a group of drug dealers that claim that his brother Ricky reported their illegal activities to the police.

That night Jermaine is killed in a drive by shooting. Ricky uses his regenerative formula on Jermaines corpse. When nothing happens, Ricky enlists Marco and Kevin to exact his revenge against his brothers killers. Subsequently, Jermaine comes back to life and also pursues his killers.

The zombie infection then spreads through flesh wounds, turning the dealers, Rickys girlfriend, and an ambulance crew into zombies. Once Ricky realizes the situation is out of control, he calls his boss, Dr. Richards (Victor Zaragoza) and tells him of the disaster.  Dr. Richards, not wanting the virus to spread out of control, calls in a mercenary to deal with the problem. The group hunts down the remaining zombies, but the mercenary gets turned into a zombie in the process.  Ricky returns home with Dr. Richards, who has been infected.  The now-undead Dr. Richards attacks Ricky, but the also-undead Jermaine comes to Rickys rescue by killing Richards. Jermaines reanimated corpse then turns on Ricky, who is forced to shoot his own brother.

As the movie ends, a lone driver (Co-Director Eduardo Quiroz in a brief cameo) sees the abandoned ambulance and pulls over to help. He is attacked by the undead hospital crew, leaving the potential for a sequel open.

==Cast==
* Ricky (Carl Washington)
* Jermaine (Brandon Daniels)
* Scott (Chris Angelo)
* Romero (Jose Rosete)
* Dr. Richards (Victor Zargoza)
* Marco (Raul Martinez)
* Kevin (Derek Taylor II)

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 