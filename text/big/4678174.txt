Grill Point
 
{{Infobox_Film |
  name           = Grill Point |
  image          = HalbeTreppeDVDCover.jpg |
    director       = Andreas Dresen |
  writer         = Andreas Dresen|
  starring       = Steffi Kühnert  Gabriela Maria Schmeide  Thorsten Merten  Axel Prahl |
  producer       = Peter Rommel |
  music          = 17 Hippies |
  cinematography = Michael Hammon |
  editing        = Jörg Hauschild | Universal |
  released       = 3 October 2002 |
  runtime        = 111 minutes |
  country        = Germany |
  language       = German |
  budget         = |
}}
Grill Point is a 2001 German drama film directed by Andreas Dresen. Its original German title is Halbe Treppe, which means "Halfway up the Stairs" in English, and was the real-life name of the snack bar shown in the film.

Grill Point was heavily influenced by the Dogme 95 manifest (although it does not follow all the Dogme rules; it uses some music not performed in-scene, the director is credited, and it is presented in widescreen format). It was shot almost entirely on location in the eastern German city of Frankfurt (Oder) using handheld digital video cameras. Its performance was improvised.

==Plot ==
Uwe, owner of a snack bar, and his wife Ellen, who works in a perfume store, are friends with Katrin, who works in a trucking agency, and her husband Chris, a radio DJ. Neither marriage is holding together well; soon, Chris begins an affair with Ellen. When Katrin discovers this, they try, and fail to solve the problem as a foursome; when this fails, the two couples respond to the situation in different ways. 

==Cast==
Only four professional actors were used in the film.

*Axel Prahl as Uwe Kukowski
*Steffi Kühnert as Ellen Kukowski
*Thorsten Merten as Chris Düring
*Gabriela Maria Schmeide as Katrin Düring

The band 17 Hippies who wrote and performed the music appear throughout the film as "the musicians".

==Reception==
*The film won the Silver Bear at the 2002 Berlinale (Grand Prix of the Jury)
*2002 Deutscher Filmpreis in silver
*Silver Hugo for Best Direction and Best Ensemble at the 2002 Chicago International Film Festival

==External links==
* 
*  
*  

 
 
 
 
 
 