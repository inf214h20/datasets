Doughboys in Ireland
{{Infobox film
| name =  Doughboys in Ireland
| image =
| image_size =
| caption =
| director = Lew Landers
| producer = Jack Fier
| writer = Howard J. Green   Monte Brice
| narrator = Kenny Baker   Jeff Donnell   Lynn Merrick   Robert Mitchum
| music = John Leipold 
| cinematography = L. William OConnell
| editing = Mel Thorsen 
| studio = Columbia Pictures
| distributor = Columbia Pictures
| released =  October 7, 1942
| runtime = 61 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical war Kenny Baker, Jeff Donnell, Lynn Merrick.  The film offered an early role for future star Robert Mitchum, who appeared in a large number of films that year. A group of American troops are stationed in Ireland, where they come into conflict with the locals.

==Cast== Kenny Baker as Danny OKeefe  
* Jeff Donnell as Molly Callahan  
* Lynn Merrick as Gloria Gold  
* Robert Mitchum as Ernie Jones  
* George Tyne as Jimmy Martin   Harry Shannon as Michael Callahan  
* Dorothy Vaughan as Mrs. Callahan   Larry Thompson as Captain  
* Syd Saylor as Sergeant 
* Herbert Rawlinson as Larry Hunt 
* Neil Reagan as Medical Captain  
* The Jesters as Singing Group

==References==
 

==Bibliography==
* Biskupski, M.B.B. Hollywoods War with Poland, 1939-1945. University Press of Kentucky, 2011. 
* Krutnik, Frank. In a Lonely Street: Film Noir, Genre, Masculinity. Routledge, 2006.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 