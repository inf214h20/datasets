The Honor of the Press
{{Infobox film
| name           = The Honor of the Press
| image          = 
| image_size     = 
| caption        = 
| director       = B. Reeves Eason
| producer       = Fanchon Royer (producer)
| writer         = John T. Neville (story and scenario) J.K. Foster (story) Michael L. Simmons (story)
| narrator       = 
| starring       = See below
| music          =  Ernest Miller
| editing        = Frank Ware
| studio         = 
| distributor    = 
| released       = 1932
| runtime        = 58 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Honor of the Press is a 1932 American film directed by B. Reeves Eason.

==Plot summary==
 

==Cast==
*Edward J. Nugent as Daniel E. Greely, Cub Reporter
*Rita La Roy as Daisy Tellem, Gossip Columnist
*Dorothy Gulliver as June Bonner, the Girlfriend
*Wheeler Oakman as Roger Bradley, Crooked Newspaper Owner Russell Simpson as City Editor Dan Perkins John Ince as Police Commissioner Drake
*Charles K. French as Dodson, Editorial Writer
*Reginald Simpson as Larry Grayson, Reporter
*Franklin Parker as Sorrell "Sorry" Simpson, Photographer
*Vivian Fields as Blond Coat-check Girl
*Franklyn Farnum as Mr. Sampson, publisher of "The Herald"

==Soundtrack==
 

==External links==
* 
* 

 
 
 
 
 
 


 