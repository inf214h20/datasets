I'll Remember April (film)
{{Infobox film
| name           = Ill Remember April
| image_size     = 
| image	=	Ill Remember April FilmPoster.jpeg
| caption        = 
| director       = Bob Clark David Forrest Mark R. Harris Mark Headley Sam Irvin
| writer         = Mark Sanderson
| narrator       =  Trevor Morgan Pam Dawber Haley Joel Osment Mark Harmon
| music          = Paul Zaza
| cinematography = Stephen M. Katz
| editing        =  Regent Entertainment Pioneer Entertainment
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Ill Remember April is a 1999 film starring Pat Morita, Pam Dawber, Haley Joel Osment, Mark Harmon and Yuki Tokuhiro.  It was directed by Bob Clark.

==Plot==
  Second World War.

Although the horrors of WWII are far removed from the Pacific Coast community where adolescent Duke Cooper (Trevor Morgan) and his three best chums play soldier, experiment with swearing, and earnestly patrol the beach for Japanese submarines, the realities of the war are about to come crashing down around them. Not when a Japanese soldier, stranded and wounded when his sub quickly dived, washes ashore; his capture by the foursome merely allows for more playtime and thoughts of becoming heroes. Its coming because Dukes older brother is on some island awaiting combat and the black sedans with military tags have already begun rolling through town to deliver their grim announcements. And Dukes Japanese American pal Willie Tanaka (Yuki Tokuhiro), all three feet and 55 pounds of him, has suddenly become a threat to national security, so he, his mother, and grandfather are soon to be shipped away to an internment camp.

==External links==
* 

 

 
 
 
 
 
 


 