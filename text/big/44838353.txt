Jhansi Ki Rani (1953 film)
 
 
{{Infobox film
| name           = Jhansi Ki Rani 
| image          = Jhansi_Ki_Rani_(1953).jpg
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Sohrab Modi
| writer         = Pandit S. R. Dubey Pandit Girish  
| narrator       =  Mehtab Sapru Sapru Mubarak
| music          = Vasant Desai Pandit Radheshyam (lyrics)
| cinematography = Ernest Haller  assisted by M. Malhotra and Y. D. Sarpotdar   Russell Lloyd
| distributor    =
| studio         = Minerva Movietone
| released       = 1953
| runtime        = 148 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1953 Hindi Mehtab and Sohrab Modi included Mubarak (actor)|Mubarak, Ulhas (actor)|Ulhas, Sapru (actor)|Sapru, Ram Singh, Baby Shikha, Marconi, and Shakila.   

Set in the 19th century against the backdrop of the Mutiny of 1857, the film is about the brave queen Lakshmibai, Rani of Jhansi, who took up arms and led her army against the British. She was one of the first Indians to do so.   

==Plot==
Rajguru (Sohrab Modi) decides that Jhansi should get its proper recognition in history. He comes across a young girl Manu (the young Rani Lakshmibai) played by Baby Shikha. Her father has been hit by a carriage driven by an English driver. She gathers a few children to confront the driver. This and her confrontation with an elephant impresses the Rajguru who takes her in hand, shaping her into becoming a determined leader. He arranges for her, at the age of nine, to marry the much-older ruler of Jhansi, Gangadhar Rao (Mubarak), who is about fifty years, and become Queen.

Manu grows up under the expert tutelage of the Rajguru, learning physical combat and political administration. The older Manu now called Lakshmibai gives birth to a boy who dies. She adopts another boy Damodar Rao who the English refuse to accept as the rightful heir. This further sets her against the British. During the uprising of 1857 (1857 Mutiny) she fights against them succumbing to her injuries in the end.

==Cast== Mehtab as Rani Lakshmibai
* Sohrab Modi as Rajguru Mubarak as Gangadhar Rao Sapru as General Sir Hugh Rose Ulhas as Ghulam Ghaus Khan
* Ram Singh as Sadashiv Rao
* Baby Shikha as Manu
* Anil Kishore as Lieut. Henry Dowker
* Kamlakant as Moropant
* S.B. Nayampalli as Panditji
* Michael Shea as Major Eliss
* Gloria Gasper as Doris Dowker
* Marconi as Colonel Sleeman
* Shakila as Kashi
* Dar Kashmiri

==Production== Gone with Russell Lloyd from England.     The film deviated from the fictionalised accounts and stuck to the extracts from the novel Jhansi Ki Rani (1946) by Vrindavan Lal Verma   

==Sohrab Modi And Historicals== Pukar (1939), Mirza Ghalib 1954), which are "considered milestones of the genre".   

==Box-office==
The press praised the film lauding Modis use of colour and direction.  However, in spite of having spent lavishly on technicians, sets, war scenes and making it in colour, the film was a big box-office disaster causing Modi great financial losses.       Modi was blamed for casting his wife Mehtab, in the title role of Lakshmibai, who looked too old at 35 years to portray the young queen half her age.    

==Crew==
Cast and crew {{cite web|url=http://www.gomolo.com/jhansi-ki-rani-movie-cast-crew/2353 
|title=Cast and crew Jhansi Ki Rani (1953)|publisher=Gomolo.com|accessdate=25 December 2014}} 
* Producer: Minerva Movietone
* Director: Sohrab Modi
* Writer: Pandit S. R. Dubey, Pandit Girish 
* Dialogue:L. Bijlani and Dialogue director was William DeLane Lea 
* Screenplay: Geza Herceg, Adi F. Keeka and Sudarshan 
* Editor:  Russell Lloyd
* Audiographer: M. Eduljee
* Music: Vasant Desai
* Lyricist: Pandit Radheshyam

==Soundtrack==
While the English version (1956, dubbed)    had no songs, the Hindi version had music by Vasant Desai and lyrics by Pandit Radheshyam. The playback singers were Mohammed Rafi, Sulochana Kadam, Suman Purohit, Parshuram, P. Ramakant.  Two songs in Mohammed Rafis voice remain notable: "Amar Hai Jhansi Ki Rani" and "Rajguru Ne Jhansi Chhodi".   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer
|-
| 1
| Amar Hai Jhansi Ki Rani
| Mohammed Rafi
|-
| 2
| Rajguru Ne Jhansi Chhodi
| Mohammed Rafi
|-
| 3
| Humara Pyara Hindustan 
| Mohammed Rafi
|-
| 4
| Har Har Mahadev Ka Nara 
| Sulochana Kadam, Suman Purohit, Parshuram, Ramakant
|-
| 5
| Azadi Ki Ye Aag Hai Lajawab 
| Mohammed Rafi
|-
| 6
| Kahan Baje Kishan Teri Bansuriya 
|
|-
| 7
| Badhe Chalo Bahaduro 
|
|-
| 8
| Nari Jee Jee Re Jee Jee Re 
|
|}

==Trivia== Razia Sultan (1983), Modi wanted to remake Jhansi Ki Rani with her in the lead.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 