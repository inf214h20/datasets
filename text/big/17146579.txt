The Art of Amália
The Art of Amália is a documentary by Bruno de Almeida on the artistic career of Amália Rodrigues (1920-1999), the celebrated Fado singer from Portugal.  This 90-minute film is based on a previous five-hour made-for-television mini-series on the singer that dealt with her life and career; it was also de Almeida’s fourth cinematic project celebrating Rodrigues’ career.   
  David Byrne, April in Portugal). 

The Art of Amália also includes the last filmed interview by Rodrigues, who died one week before production was completed.

The film had its U.S. premiere in March 2000 at a United Nations benefit, and its theatrical premiere in New York in December 2000.  The DVD is now available to the public.

== References ==
 

== External links ==
*  
* http://www.arcofilms.com/ - production company
* http://www.filmthreat.com/interviews/22/ - interview

 
 
 
 
 
 
 
 


 
 