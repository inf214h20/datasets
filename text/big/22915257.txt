The Wooden Man's Bride
{{Infobox film
| name           = The Wooden Mans Bride
| image          = Wooden_Mans_Bride.jpg
| caption        = Region 1 DVD Cover
| director       = Huang Jianxin
| producer       = Wang Ying Hsiang
| writer         = Yang Zhengguang Novel: Jia Pingwa Wang Lan Ku Paoming Wang Yumei Wang Fuli Kao Mingjun
| music          = Zhang Dalong
| cinematography = Zhang Xiaoguang
| editing        = Lei Qin
| distributor    = 
| released       =  
| runtime        = 113 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 五魁
| fanti          = 五魁
| pinyin         = Wǔ kuí}}
}}
The Wooden Mans Bride ( ) is a 1994 Chinese film directed by the fifth generation filmmaker Huang Jianxin. The film is Huangs fifth feature and the first of his films to be released in the United States.    The film was also the first film shot in Mainland China to be entirely financed by Taiwanese producers.    The film stars both Taiwanese and Mainland Chinese actors, and was based on the novel by Jia Pingwa.

The Wooden Mans Bride was something of a departure for the director, Huang Jianxin, whose earlier works were modern day satires on bureaucracy as in the black comedy, The Black Cannon Incident, and the science-fiction dystopia in Dislocation (film)|Dislocation. 

== Plot == Wang Lan), is on her way to meet her future husband for the first time when her procession is attacked by sword-wielding bandits in the desert. A servant, Kui (Chang Shih) recklessly takes off after the bandits who have kidnapped the young mistress and taken her back to their lair. When he arrives, Kui impresses the chief of the bandits (Kao Mingjun), who allows Kui to take the Young Mistress back. 

Meanwhile, word has reached the Young Mistresss fiancé of her capture. Preparing to engage in a thrilling rescue, the hapless young man accidentally sets off an explosion, killing him. When Young Mistress finally arrives, she faces her fiancés imperious mother (Wang Yumei), who forces the young woman to undergo arcane tests of purity to determine whether she is worthy to marry the (now dead) bridegroom. When Young Mistress passes these tests, she is forced to marry the titular "wooden man," a wood-carved statue of her late fiancé. 

Forced into a life she does not want, Young Mistress tries and fails to escape from her new home. She finds solace in her growing friendship with her one-time hero, Kui, a friendship that soon blossoms into an illicit love affair. When the affair is discovered, Kui is banished from the mill, and Madame Liu has the Young Mistresss legs broken to prevent escape. Kui, however, is determined to rescue his love once more. He returns to the bandits lair to find the chief dead, and becomes the bandits new leader.  

A year after he leaves, Kui returns with the bandits to claim Young Mistress. He allows Madame Liu to commit suicide by hanging herself, then burns the house down, taking Young Mistress away with him.

== Cast == Wang Lan as Young Mistress, the films headstrong heroine and the titular "Wooden Mans Bride".
* Chang Shih as Kui, a peasant-servant who first saves the Young Mistress from the bandits and later becomes her lover.
* Kao Mingjun as Chief Tang, the charismatic leader of the bandits who kidnap Young Mistress.
* Wang Yumei as Madame Liu, the imperious mother of Young Mistresss fiancé. Madame Lius husband died twenty years earlier, leaving her in charge of a tofu mill.
* Wang Fuli as Sister Ma, Madame Lius devoted housemaid. 

== International reception ==
Western critics warmly received Huangs shift into the historical drama film. One early review during its premiere at the International Film Festival Rotterdam found the film to be a "ravishing spectacle," though not necessarily groundbreaking,  especially in light of similar films such as Zhang Yimous 1990 drama, Ju Dou. The San Francisco Chronicles Edward Guthmann, however, found the comparison to Zhangs earlier films as diminishing The Wooden Mans Bride. While he found the film to be an "entertaining melodrama," it nevertheless lacked the "technical mastery or historical reverberations" of those other auteurs.  Other critics felt that Huang had more than matched his contemporaries, in the process creating the "most visually stunning, emotionally powerful western since Clint Eastwoods "Unforgiven."" 

At least one critic praised not only the films sense of spectacle, but also what he saw as the films underlying cultural criticism. Stephen Holden of The New York Times noted that the film was a "methodical, cool-headed expose of an oppressive sexual code that treats women as chattel and metes out brutal punishment to violators." 

== See also ==
* Ju Dou, Zhang Yimous 1990 film also about a young woman forced into a loveless marriage, though with a living human being.
== References ==
 

== External links ==
*  
*  
*  
*   at the Chinese Movie Database

 
 
 
 
 
 
 
 
 