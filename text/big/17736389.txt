Ramona (1928 film)
 
{{Infobox film
| name           = Ramona
| image          = Poster of Ramona (1928 film).jpg
| caption        = Film poster
| director       = Edwin Carewe
| producer       = 
| writer         = Helen Hunt Jackson (novel) Finis Fox
| starring       = Dolores del Río Warner Baxter
| music          = "Ramona (song)|Ramona" by Mabel Wayne and L. Wolfe Gilbert
| cinematography = Robert Kurrle
| editing        = Jeanne Spencer
| studio         = Inspiration Pictures
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}} silent drama film directed by Edwin Carewe,    based on Helen Hunt Jacksons 1884 novel Ramona, and starring Dolores del Rio and Warner Baxter. 

Mordaunt Hall of the New York Times found much to praise in what he called "an Indian love lyric": "This current offering is an extraordinarily beautiful production, intelligently directed and, with the exception of a few instances, splendidly acted. The scenic effects are charming....  The different episodes are told discreetly and with a good measure of suspense and sympathy. Some of the characters have been changed to enhance the dramatic worth of the picture, but this is pardonable, especially when one considers this subject as a whole."  

This was the first United Artists film with a synchronized score and sound effects , but no dialogue, and so was not a talking picture.

The story had been filmed by D. W. Griffith in 1910 with Mary Pickford, remade in 1916 with Adda Gleason, and again in 1936 with Loretta Young.

==Preservation status==
For decades, Ramona was thought to be lost until archivists rediscovered it in the Národní Filmový Archiv in Prague. The Motion Picture, Broadcasting and Recorded Sound Division of the Library of Congress later transferred Ramona’s highly flammable original nitrate film to acetate safety stock. 

The restored version of the 1928 film had its world premiere in the Billy Wilder Theater at the University of California, Los Angeles on March 29, 2014.  Carewes older brother Finis Fox had written Ramonas screenplay and created its intertitles. 

==Cast==
* Dolores del Río - Ramona
* Warner Baxter - Alessandro
* Roland Drew - Felipe
* Vera Lewis - Señora Moreno
* Michael Visaroff - Juan Canito
* John T. Prince - Father Salvierderra
* Mathilde Comont - Marda
* Carlos Amor - Sheepherder
* Jess Cavin - Bandit Leader
* Rita Carewe - Baby

==See also==
* List of rediscovered films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 