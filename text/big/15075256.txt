Crossed (film)
{{Infobox Film
| name = Crossed
| image =
| caption = Teaser poster for Crossed
| director = Stolis Hadjicharalambous
| producer = Stolis Hadjicharalambous
| writer = Stolis Hadjicharalambous Christopher J. Otis
| starring = Christopher J. Otis Javier Rodriguez Henry Boriello
| music = Billy Archiello
| cinematography =Bart Mastronardi Dominic Sivilli
| editing = Stolis Hadjicharalambous
| distributor = Hilltop Studios
| released =  
| runtime =
| country = United States
| awards = English
| budget =
| gross =
}}

Crossed is independent film maker Stolis Hadjicharalambouss feature length action/thriller film. It is a story of truth, power, and revenge in a dangerous crime underworld of assassins.

== Plot ==
A young hitman named Frank Archer rises through the ranks of a seedy underworld dominated by upstart crime boss Borriello. Still living up to the legacy of his father, Frank Archer searches for the truth about his haunted past, eventually crossing him with a sadistic assassin known only as The Ripper. 

== Cast ==
{| class="wikitable"
|-
! Cast
! Character
|-
| Christopher J. Otis
| Frank Archer
|-
| Henry Borriello
| Borriello
|-
| Javier Rodriguez
| The Ripper
|-
| Jerry Murdock
| Frank Archer Sr.
|-
| Ashley Bernardes
| Samantha Thompson
|-
| Alan Rowe Kelly
| Ed
|-
| Talia Morero
| Nina
|- Miguel Lopez
| Dr. Gregorio Casa
|-
| Dominick Sivilli
| Bartolino
|-
| Richard Wenzel 
| Charlie
|- Michael Coyne  Parkman
|- Jessie May Laurhman Tatum
|-
|}

== Production ==
Hadjicharalambous has been working on Crossed since 2005. A teaser trailer was posted on the official Myspace of the film. On June 30, 2007, that trailer was replaced with an exclusive teaser trailer shown at the Fangoria Convention.  There have been two screenings of Crossed thus far, the first being on November 22, 2008. The second was the following week on November 29, 2008.

==References==
 

==External links==
*  
*  

 
 


 