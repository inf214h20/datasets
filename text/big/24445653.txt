Ekangi
{{Infobox film
| name           = Ekangi
| image          = Ekangi poster.jpg
| image_size     = 175px
| caption        =  Ravichandran
| producer       = Ravichandran
| writer         = Ravichandran
| screenplay     = Ravichandran
| narrator       =
| starring       = Ravichandran Ramya Krishnan Prakash Rai
| music          = Ravichandran
| cinematography = G. S. V. Seetharam
| editing        = Shyam
| studio         = Eshwari Enterprises
| distributor    =
| released       =  
| runtime        = 130 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kannada cinema during the time.  It was also the first in Kannada and second Indian film to employ SFX sound system.
 Second Best Best Actor (V. Ravichandran|Ravichandran).  It was an ambitious project, but failed to succeed at the box-office, even though following the audiences hostile response after its theatrical release, almost 20 scenes were re-shot and re-edited before re-releasing it with censor acceptance. Ravichandran was said to have suffered a loss of   2 crore owing to the films commercial failure. 

==Cast== Ravichandran
* Ramya Krishnan
* Prakash Rai
* Vanitha Vasu Sridhar
* Soori
* Master Vinay
* M. N. Suresh
* Prakash
* Ramesh Bhat
* Sadhu Kokila
* Mandya Ramesh
* Master Santhosh

==Production== Ravichandran at the time, considering that a similar house was erected even for Ravichandrans previous film O Nanna Nalle. Close to 80% of the films scenes were filmed in the house.   EFX sound system was employed in the film for the first time in Kannada cinema and second overall.

Following the films first theatrical release and a hostile response from the audience, close to 20 scenes of the film were re-shot and re-edit before giving it a re-release, a first of its kind in Kannada cinema. 

==Soundtrack==
{{Infobox album
| Name        = Ekangi
| Type        = Soundtrack Ravichandran
| Cover       = Kannada film Ekangi album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 2001
| Recorded    = Feature film soundtrack
| Length      = 48:21
| Label       = Jhankar Music
| Producer    = 
| Last album  = O Nanna Nalle 2002
| This album  = Ekangi 2002
| Next album  = Kodandarama 2002
}}
 Ravichandran composed the films background score, soundtracks and also wrote the lyrics for the tracks. The album consists of nine soundtracks.  The album was released in late 2001, in Davangere, at an event.   

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 48:21
| lyrics_credits = yes
| title1 = Banna Bannada Loka Ravichandran
| extra1 = Shankar Mahadevan
| length1 = 5:08
| title2 = Ee Chitte Thara Banna
| lyrics2 = Ravichandran
| extra2 = L. N. Shastry
| length2 = 5:54
| title3 = Hudugi Superamma
| lyrics3 = Ravichandran
| extra3 = Suresh Peters, Anupama (singer)|Anupama, Rajesh Krishnan
| length3 = 4:54
| title4 = Nannane Kele Nanna Pranave
| lyrics4 = Ravichandran Hariharan
| length4 = 5:50
| title5 = Nee Madid Thappa
| lyrics5 = Ravichandran
| extra5 = Rajesh Krishnan
| length5 = 11:18
| title6 = Nee Yekangiyagamma
| lyrics6 = Ravichandran
| extra6 = Madhu Balakrishnan
| length6 = 4:40
| title7 = Once Upon a Time
| lyrics7 = Ravichandran
| extra7 = Sonu Nigam
| length7 = 4:15
| title8 = Ondu Nimisha
| lyrics8 = Ravichandran
| extra8 = S. P. Balasubrahmanyam, Anuradha Sriram
| length8 = 4:47
| title9 = Ekangi Theme
| lyrics9 = Ravichandran
| extra9 = Instrumental
| length9 = 1:35
}}

===Critical reception===
The album was received well by critics upon release. But, for the lyrics, the album received appreciation. Amritamati S. of the The Music Magazine reviewed the album and called it, "Symphonic grandeur on dumb lyrics". She added writing credits to the solo violin, guitar bits and the piano play among other highlights of the album. She concluded writing, "The quality of recording is excellent. Full marks to the string ensemble, and to the other instrumentalists. But you will be disappointed if you look for poetry, or even the street-smart variety of verse that Hamsalekha specialises in." 

==Awards==
; 2001–02 Karnataka State Film Awards Best Film Best Actor Ravichandran
* Best Music Ravichandran
* Best Sound Recording - L. Sathish
* Karnataka State Film Award for Best Male Playback Singer - Rajesh Krishnan

==Aftermath==
Having suffered from huge losses after the film failed commercially, Ravichandran took the blame for the films failure. In an interview with Deccan Herald in December 2004, speaking of the films failure, he said "it shattered him mentally and physically."  The failure of the film influenced him hugely and his style of filmmaking. 

==References==
 

 

 
 
 
 