Ganga Gowri (1997 film)
{{Infobox film
| name           = Ganga Gowri
| image          = Ganga Gowri DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Madheswaran
| producer       = N. Vishnuram
| writer         = Madheswaran Vasantharajan  (dialogues) 
| starring       =  
| music          = Sirpy
| cinematography = Kichaas
| editing        = B. S. Vasu - Saleem
| distributor    =
| studio         = Ganga Gowri Production
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil Comedy-drama|comedy-drama Mantra in lead roles. The film had musical score by Sirpy and was released on 5 May 1997.   

==Plot==

Shiva (Arun Vijay) and his brother Vichu (Vadivelu) are carefree youth while their father Pandiyan (Dindigul I. Leoni) is a miser. Shiva falls in love with Ganga (Raasi (actress)|Mantra) at first sight and tries to seduce her with his brothers help. Ultimately, Ganga also falls in love with him. Gowri (Sangita) comes from her village and begins to work in Pandiyans house as a maid.

Few months ago, Shiva was bitten by a snake and Muthu (Siva) saved him. Muthu cannot marry Gowri because of their horoscope. According to tradition, Gowri must get married with another man for one week and then she can marry Muthu. Shiva, grateful to Muthu for saving his life, got married with her and left the village. Thereafter, Gowri didnt want to marry Muthu anymore, Muthu was understandable and he advised her to rejoin Shiva.

Finally, Pandiyan accepts Shiva and Gangas marriage. What transpires later forms the crux of the story.

==Cast==

*Arun Vijay as Shiva
*Sangita as Gowri Mantra as Ganga
*Vadivelu as Vichu
*Dindigul I. Leoni as Pandiyan
*Siva as Muthu
*Paandu
*Viswanath
*Peeli Sivam
*Sirpy in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Ganga Gowri
| Type        = soundtrack
| Artist      = Sirpy
| Cover       = 
| Released    = 1997
| Recorded    = 1997 Feature film soundtrack |
| Length      = 29:19
| Label       = 
| Producer    = Sirpy
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sirpy. The soundtrack, released in 1997, features 6 tracks with lyrics written by Pazhani Bharathi.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Mano || 4:53
|- 2 || Kadhal Solla Vandhen || P. Unni Krishnan, Sujatha Mohan || 4:51
|- 3 || Kadhalare Kadhalare || S. P. Balasubrahmanyam || 4:55
|- 4 || Margazhi Neerada || Mano || 5:14
|- 5 || Poonthendrale Velaiyadu || Mano, Sujatha Mohan || 5:00
|- 6 || Poovukkul Puthaiyal || Mano || 4:26
|}

==References==
 

 
 
 
 