The Stunt Man
{{Infobox film
| name           = The Stunt Man
| image          = Stuntmanposter.jpg
| caption        = Theatrical poster Richard Rush
| producer       = Richard Rush
| writer         = Novel:   Lawrence B. Marcus
| starring       = Peter OToole Steve Railsback Barbara Hershey
| music          = Dominic Frontiere
| cinematography = Mario Tosi
| editing        = Caroline Biggerstaff Jack Hofstra Melvin Simon Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 131 minutes
| country        = United States English
| budget         = $3,500,000
| gross          = $7,068,886
}} Richard Rush, starring Peter OToole, Steve Railsback, and Barbara Hershey.  The movie was adapted by Lawrence B. Marcus and Rush from the novel by Paul Brodeur. It tells the story of a young fugitive who hides as a stunt double on the set of an anti-war movie whose charismatic director will do seemingly anything for the sake of his art.
 Best Director Best Writing, Screenplay Based on Material from Another Medium. However, due to its limited release, it never earned much attention from US audiences at large.  As OToole remarked in a DVD audio commentary, "The film wasnt released, it escaped." 

==Plot==
Cameron (Steve Railsback) is a young veteran running from the police. He stumbles onto the set of a World War I movie and isnt sure if he accidentally caused the death of one of the films stunt men. The eccentric and autocratic director, Eli Cross (Peter OToole), agrees to hide Cameron from the police if he will take the dead mans place. Cameron soon begins to suspect that Cross is putting him in excessive danger. At a bar one night, another member of the production gets drunk and tells Cameron that Eli almost killed the helicopter pilot during the fatal accident Cameron caused, because he insisted he keep flying in order to get the shot. Cameron falls in love with Nina Franklin (Barbara Hershey), the films star, and is devastated to find out that she and Eli slept together before he met her.

The boundaries between reality and fiction become increasingly blurred as Cross exercises godlike control over the production. During a screening of some footage for Ninas parents who are visiting the production, a nude sex scene with Nina is shown. Eli appears to be mortified, but allows the footage to play anyway. He waits until Nina is just about to shoot a traumatic scene the next day to tell her that her parents have seen the footage of her naked. It causes her to cry, which seems to be the exact emotion Eli needed from her in the scene.

The final day of filming involves a complicated stunt where Cameron has to drive a vintage Duesenberg off a bridge. Nina has two other scenes to shoot as well, but Cameron is convinced Eli will rig the stunt so he will die. He persuades Nina to run away with him, but they are unable to leave the set, which is kept sealed from the neighboring town by the police on Elis orders. Nina hides in the trunk of the Duesenberg, promising to slip away with Cameron during the stunt.

Before the scene is shot, Eli points to the Duesenberg and explains it is the only copy of the vintage car that the production has. He therefore orders that no one interrupt the filming of the scene once it begins. Cameron is beside himself with anxiety. He keeps trying to check the trunk to see if Nina is there, but finally decides that he will find out once he has escaped in the car. When he gets behind the wheel, the police chief asks if the in-car camera is on. Cameron mishears the question "Camera on?" the stunt man starts the car and speeds away before anyone is expecting it. The entire crew springs into action. Eli screams at them to start shooting.
 flips off the camera, but a crew member triggers a charge which causes a blowout in a front tire. The car swerves off the side of the bridge and into the water. As it sinks, Cameron climbs into the back seat to free Nina from the trunk. Then he sees her standing next to Eli on the bridge, looking down on him. He swims to the bank. Eli descends behind him on a crane, and helps Cameron come to the realization his life was never in danger. Cameron says that the stunt is the hardest $1,000 he has ever made. Eli corrects Cameron, saying the pay for the stunt is only $650. Cameron becomes enraged, insisting he was promised $1,000. Eli laughs at him and offers to split the difference at $750. He flies off in the helicopter, leaving Cameron screaming at him.

==Production== Coronado near San Diego, California.
 Lawrence of Arabia.

==Reception==
Of The Stunt Man, Roger Ebert wrote "there was a great deal in it that I admired...   there were times when I felt cheated".    He gave the film only two stars but noted that others had "highly recommended" it. In an October 17, 1980, review in The New York Times, Janet Maslin noted "the films cleverness is aggressive and cool," but concluded that although "the gamesmanship of The Stunt Man is fast and furious... gamesmanship is almost all it manages to be".  However, critic Pauline Kael considered it "a virtuoso piece of kinetic moviemaking" and rated it one of years best films.  She called OTooles comic performance "peerless". The film currently holds a 92% "fresh" rating on Rotten Tomatoes based on 26 reviews.

== Awards ==
*Montreal World Film Festival – "Grand Prix des Amériques" (Best Film) for Richard Rush
*Golden Globe awards – "Best Original Score" for Dominic Frontiere
*National Society of Film Critics Awards – "Best Actor" for Peter OToole

=== Nominations ===
The Stunt Man received three Academy Award nominations:
* Best Actor – Peter OToole
* Best Director – Richard Rush
* Best Adapted Screenplay – Lawrence B. Marcus, Richard Rush

==Home media==
The Stunt Man was released on DVD on November 20, 2001 in two versions by Anchor Bay Entertainment. The first version is a standard release featuring two deleted scenes and a commentary by director Richard Rush and stars Peter OToole, Steve Railsback, Barbara Hershey, Alex Rocco, Sharon Farrell and Chuck Bail. The second version is a limited edition (100,000 copies) containing everything from the standard release as well as including the 2001 documentary The Sinister Saga of Making "The Stunt Man".

The films theme song "Bits & Pieces" is sung by Dusty Springfield.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 