The Game (1984 film)
 
{{Infobox film
  | name = The Game
  | image = 
  | caption = 
  | director = Bill Rebane
  | producer = Barbara Rebane Alan Rainer
  | writer = William Arthur Larry Dreyfus
  | starring = Tom Blair Jim Iaquinta Carol Perry Lori Minnetti
  | music = Bruce Malm
  | cinematography = Bill Rebane
  | editing = Bill Rebane
  | distributor = Trans World Entertainment (TWE)
  | released = 1984
  | runtime = 84 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}

The Game also known as The Cold is a low budget 1984 horror film directed by Bill Rebane. The film was made in Wisconsin and released theatrically by Trans World Entertainment.

==Plot==
 
Three millionaires gather nine people in an old mansion and give them a proposition: If they can conquer their biggest fears, they will get one million dollars in cash.

==Cast==
 

==Production==
 

==Release==
 

==Reception==
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 