E a Vida Continua...
{{Infobox film
| name           = E a Vida Continua...
| image          = E a Vida Continua.jpg
| caption        = Brazilian theatrical poster
| director       = Paulo Figueiredo
| producer       = 
| writer         = Paulo Figueiredo
| starring       = Amanda Acosta  Lima Duarte  Ana Rosa  Luiz Baccelli  Ana Lúcia Torre
| music          = 
| cinematography = 
| editing        = 
| studio         = Versátil Filmes
| distributor    = Paris Filmes
| released       =  
| runtime        = 98 minutes
| country        = Brazil
| language       = Portuguese
| budget         = $2 million 
}}

E a Vida Continua... is a 2012 Brazilian drama film directed by Paulo Figueiredo, based on the book of the same name by the medium Chico Xavier.

==Plot==
When the car of young Evelina (Amanda Costa) breaks on the road, she has no idea how her path will be deeply changed forever. Bailed out by Ernesto (Luiz Baccelli), Evelina soon discovers that they are going exactly to the same hotel.   

Immediately they develop a friendship so solid that will persist when both leave to another dimension. 

==Cast==
*Lima Duarte
*Amanda Acosta
*Ana Lúcia Torre
*Ana Rosa
*Arlete Montenegro
*Carla Fioroni
*Cesar Pezzuolli
*Claudia Mello
*Luiz Baccelli
*Luiz Carlos Felix

==References==
 

==External links==
*    
*  

 
 
 
 


 
 