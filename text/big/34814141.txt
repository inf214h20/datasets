Pension Schöller (1930 film)
Pension German comedy Pension Schöller by Wilhelm Jacoby and Carl Laufs. Georg Jacoby was Wilhems son, and made three film adaptation of his fathers best known play in 1930, 1952 and 1960.

==Cast==
* Paul Henckels - Direktor Schöller 
* Elga Brink - Friedel - ihre Tochter 
* Jakob Tiedtke - Philipp Klapproth 
* Josefine Dora - Ulrike - seine Frau 
* Truus Van Aalten - Grete - beider Tochter 
* Paul Heidemann - Dr. Alfred Klapproth 
* Kurt Vespermann - Ernst Kissling 
* Else Reval - Frau Pfeiffer 
* Viktor de Kowa - Bernhardy 
* Fritz Kampers - Gröber 
* Hedwig Wangel - Fräulein Krüger  Fritz Schulz - Jallings 
* Trude Berliner - Fiffi 
* Carl Geppert - Der Bürgermeister

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 


 