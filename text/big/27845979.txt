Spy Sorge
 
Spy Sorge is a Japanese film directed by Masahiro Shinoda in 2003 in film|2003, about the Soviet spy Richard Sorge. A long and lavish production which became only a modest critical and commercial success, Shinoda intended the film to be his final feature.

==Plot==
The life of Richard Sorge.

==Cast==
*Iain Glen : Richard Sorge
*Masahiro Motoki : Hidemi Ozaki
*Kippei Shiina : Mitsusada Yoshikawa
*Takaya Kamikawa : Tokko T
*Toshiya Nagasawa : Miyagi
*Riona Hazuki : Hanako Miyake
*Koyuki : Yoshiko Yamazaki
* Armin Marewski  : Branko Vukelic
*Yui Natsukawa : Hideko Ozaki
*Takaaki Enoki : Duke Fumimaro Konoye
*Hideji Otaki : Duke Kinmochi Sai-onji
*Shima Iwashita : Mme Konoe

==Technical details==
*Writers: Robert Mandy & Masahiro Shinoda
*Producers : Masato Hara, Masaru Koibuchi & Peter Rawley for Asmik Ace Entertainment & Manfred Durniok Filmproduktion
*Music : Shinichirô Ikebe Tatsuo Suzuki
*Length: Japan : 182 min
*Country: Japan / Germany
*Language: Japanese
*Colour: Colour
*Sound: Dolby Digital

==Honours== 2004
* Also nominated at the same awards in the following categories: 
**Best Cinematography for Tatsuo Suzuki
**Best Director for Masahiro Shinoda
**Best Editing for Hiroshi Okuda
**Best Film
**Best Lighting for Hideshi Mikami
**Best Music Score for Shinichirô Ikebe
**Best Screenplay for Masahiro Shinoda and Robert Mandy
**Best Sound for Tetsuo Segawa

==External links==
* 

 
 
 
 
 


 
 