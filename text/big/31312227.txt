Redwood Curtain
 
{{Infobox film
| name           = Redwood Curtain
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = John Korty
| producer       = Rick Rosenberg Brent Shields
| screenplay     = Ed Namzug
| based on       =  
| starring       = Jeff Daniels Lea Salonga Catherine Hicks John Lithgow
| music          = Lawrence Shragge
| cinematography = Ronnie Taylor
| editing        = Scott Vickrey
| studio         = Chris/Rose Productions Hallmark Hall of Fame Productions ABC Republic Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 1995 United American drama dramatic TV of the ABC on April 23, 1995.      

==Background==
Redwood Curtain was filmed in Northern California.  The film was adapted by Ed Namzug and based upon Lanford Wilsons 1993 play of the same name.  Hallmark brought Debra Monk, who was part of the original stage production, to the telefilm to reprise her role of Geneva Riordan. 

==Synopsis== prodigy comes to Californias redwood forests to an area populated by Vietnam veterans unable to reintegrate into society.

==Cast==
* Jeff Daniels as Lyman Fellers
* Lea Salonga as Geri Riordan
* Catherine Hicks as Julia Riordan
* John Lithgow as Laird Riordan
* Debra Monk as Geneva Riordan
* Shirley Douglas as Schyler Noyes
* Vilma Silva as Zenaida
* Joy Carlin as Mrs. Cole
* Steven Anthony Jones as Nate Stone
* Jarion Monroe as Leon Shea
* Cab Covay as Mad John
* R. Blakeslee Colby as Reverend Grant
* Jonathan Korty as Dennis McCaw

==Reception==
Variety (magazine)|Variety noted that the original stage play was a "spookily amorphous affair", and that it included an "edgy, funny performance by Debra Monk."  Of the television film, they called it a "ponderous, cliche-riddled adaptation", with a performance by Monk that suffered in her character having her "spirit drained".  Conversely, they commended director John Korty in his drawing "a nicely restrained performance out of John Lithgow". 

==Awards and nominations==
* 1995, Cinema Audio Society Award nomination for Outstanding Achievement in Sound Mixing for a Movie of the Week, Mini-Series or Special
* 1995, Emmy Award nomination for Outstanding Individual Achievement in Sound Mixing for a Drama Miniseries or a Special

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 
 
 
 
 