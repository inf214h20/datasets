Ningalenne Communistakki (film)
{{Infobox film 
| name           = Ningalenne Communistakki
| image          =Ningalenne Commistaki.jpg
| caption        =
| director       = Thoppil Bhasi
| producer       = M Kunchacko
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi Sathyan Sheela Jayabharathi
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by Thoppil Bhasi and produced by M Kunchacko. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Jayabharathi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Gopalan Sathyan as Paramu Pilla
*Sheela as Sumam
*Jayabharathi as Mala
*KPAC Lalitha as Meenakshi
*Alummoodan as Velu
*K. P. Ummer as Mathew
*Kottayam Chellappan as Keshavan Nair
*Kundara Bhasi as Karumban
*Rajamma
*S. P. Pillai as Pappu
*Thoppil Krishna Pillai
*Vijaya Kumari as Kalyani
*Adoor Pankajam as Kamalamma
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aikya Munnani || K. J. Yesudas, P. Madhuri, Chorus || Vayalar Ramavarma || 
|-
| 2 || Ambalapparambile || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Ellaarum Paadathu || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Kothumbuvallam Thuzhanjuvarum || K. J. Yesudas, P. Leela, P. Madhuri, B Vasantha || Vayalar Ramavarma || 
|-
| 5 || Neelakkadambin Poovo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Pallanayaarin Theerathil || P Susheela, MG Radhakrishnan || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 