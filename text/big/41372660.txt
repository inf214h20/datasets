How I Learned to Love Women
 
{{Infobox film
| name           = How I Learned to Love Women
| image          = How I Learned to Love Women.jpg
| caption        = Theatrical release poster
| director       = Luciano Salce
| producer       = Enrico Chroscicki Alfonso Sansone
| writer         =
| starring       = Michèle Mercier Nadja Tiller Elsa Martinelli
| music          = Ennio Morricone
| cinematography =  Erico Menczer
| editing        = 
| released       = October 2, 1966  (Italy) 
}}
How I Learned to Love Women ( ,  ,  , also known as Love Parade) is a 1966 Italian-French-German comedy film directed by Luciano Salce.    

==Cast==
* Michèle Mercier as Franziska
* Nadja Tiller as Baroness Laura
* Elsa Martinelli as Rallye driver
* Anita Ekberg as Margaret Joyce
* Robert Hoffmann as Robert
* Zarah Leander as Olga
* Sandra Milo as Ilde
* Romina Power as Irene
* Orchidea De Santis as Agnes
* Vittorio Caprioli as Playboy
* Sonja Romanoff as Monika
* Erica Schramm as Betty
* Gigi Ballista as Sir Archibald
* Heinz Erhardt as Schluessel
* Chantal Cachin as Wilma
* Gianrico Tedeschi as Director 
* Mita Medici 
* Carlo Croccolo 
* Mariangela Giordano

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 