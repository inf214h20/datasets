Torrid Zone
 
{{Infobox film
| name           = Torrid Zone
| image          = Poster - Torrid Zone 01.jpg
| image_size     =
| caption        = Theatrical poster
| director       = William Keighley
| producer       = William Cagney (uncredited)
| writer         = Richard Macauley Jerry Wald
| narrator       = Pat OBrien
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1940 adventure Pat OBrien.  

==Plot summary==
Steve Case (Pat OBrien) has to deal with trouble at his tropical fruit companys Central American banana plantation. A revolutionary, Rosario La Mata (George Tobias), is stirring up unrest among the workers, and the only man who can handle the situation, foreman Nick Butler (James Cagney), has just quit. Steve manages to persuade Nick to stick around (for a big bonus). Adding to the complications is Lee Donley (Ann Sheridan), a woman whom Steve has ordered out of the region for causing a different kind of trouble among the men.

The film borrowed plot elements from The Front Page and Red Dust and ended with Cagney saying to Sheridan "You and that 14-carat oomph", a studio in-joke in reference to Sheridans title as the Oomph Girl.

==Cast==
*James Cagney as Nick Butler
*Ann Sheridan as Lee Donley Pat OBrien as Steve Case
*Andy Devine as Wally Davis
*Helen Vinson as Mrs. Gloria Anderson
*Jerome Cowan as Bob Anderson
*George Tobias as Rosario La Mata
*George Reeves as Sancho
*Victor Kilian as Carlos
*Frank Puglia as Police Chief Juan Rodriguez

==Notes==
 

==External links==
* 
*  

 

 
 
 
 
 
 

 