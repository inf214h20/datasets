Manual of Love 2
{{Infobox film
| name           = Manual of Love 2 
| image          = Manual of Love 2.jpg
| image size     =
| caption        =  
| director       = Giovanni Veronesi
| producer       = Aurelio De Laurentiis Luigi De Laurentiis
| writer         =  Giovanni Veronesi Andrea Agnello Ugo Chiti
| narrator       = Claudio Bisio
| starring       = Carlo Verdone Monica Bellucci Riccardo Scamarcio Sergio Rubini Fabio Volo
| music          = Paolo Buonvino
| cinematography = Giovanni Canevari
| editing        =  Cludio Di Mauro
| country = Italy
| language       = Italian
| released = 2006
| runtime = 125 min
| budget         =
}}
 2007  Italian romantic comedy film in four quartets directed by Giovanni Veronesi.     It is the sequel of Manual of Love and was followed by Manual of Love 3.

==Plot==
The film is divided into four episodes, centered on the theme of love.

===First segment===
Dario and Nicola, two men from Apulia, are involved in a car accident. Both are taken to hospital suffering from paralysis of the legs, and there Nicola meets a beautiful nurse named Lucia. While Nicola continues his rehabilitation therapy, he finds that he loves Lucia more and more, and she, too, in time, falls in love with him.

===Second segment===
Franco and Manuela have been married for a short time, and they want a child. However, Franco has a low semen count, and this creates many complications for the couple. Given that in Italy the two cannot use artificial insemination, because the law prevents it, Franco and Manuela travel to Spain, where Manuela is admitted to a specialist clinic. Franco manages to make Manuela pregnant, and he hopes they will have a son. However, their unborn child proves to be a girl.

===Third segment===
 , and disowns him. Fosco and Filippo, as well as having this problem, know that in Italy they cannot get married, or even take on the upbringing of a child, because the laws do not permit such things. After they have been fighting over these issues, Fosco is beaten up by homophobes, and is soon visited in hospital by Filippo, who promises to go to Spain where he will marry him.

===Fourth segment===
Ernesto is a highly regarded waiter in an expensive restaurant in Rome. He has a wife and children, although his life lacks passion. One day Ernesto meets Cecilia, an Argentinian girl who works in the restaurant as a dishwasher, and they fall in love. Ernesto is very fond of Cecilia, because he knows that she is suffering because she has been abandoned as a child by her father. Cecilia, delighted by their love affair, involves Ernesto in wild late-night parties, but he has a heart attack, and he realizes that the youthful life of Cecilia no longer suits him. So the two decide to end their affair, while agreeing to remain friends.

==Cast==
* Carlo Verdone: Ernesto
* Monica Bellucci: Lucia
* Riccardo Scamarcio: Nicola
* Antonio Albanese: Filippo
* Sergio Rubini: Fosco
* Fabio Volo: Franco Barbora Bobulova: Manuela
* Claudio Bisio: Fulvio (DJ)
* Elsa Pataky: Cecilia
* Dario Bandiera: Dario
* Eugenia Costantini: Maura
* Gea Lionello: Elena
* Rosario Fiorello|Fiorello: male nurse

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 
 