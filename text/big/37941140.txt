List of Malayalam films of 1964
 

The following is a list of Malayalam films released in 1964.
{| class="wikitable"
|- style="background:#000;"
! colspan=2 | Opening !! Sl. No. !!Film !! Cast !! Director !! Music Director !! Notes
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 31
|valign="center" |  1  Thacholi Othenan Madhu || National Film Award for Best Feature Film
|-
| rowspan="1" style="text-align:center; background:#dcc7df; textcolor:#000;"| F E B 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 22
|valign="center" |  2  Madhu || M. Krishnan Nair (director)|M. Krishnan Nair || Baburaj|M.S.Baburaj ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| M A R 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 5
|valign="center" |  3  Anna || Ragini || KS Sethumadhavan || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 20
|valign="center" |  4 
| Devaalayam || Prem Nazir, Adoor Bhasi || NS Muthukumaran, Ramanathan || V. Dakshinamoorthy ||
|-
| rowspan="4" style="text-align:center; background:#ffa07a; textcolor:#000;"| A P R 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 3
|valign="center" |  5  School Master || Prem Nazir, K. Balaji || Puttanna Kanagal|S. R. Puttanna || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 10
|valign="center" |  6  Madhu || KS Sethumadhavan || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 18
|valign="center" |  7  Atom Bomb || K. Balaji, Kaviyoor Ponnamma || P. Subramaniam || Br Lakshmanan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 30
|valign="center" |  8 
| Kalyana Photo || Madhu (actor)|Madhu, Adoor Bhasi || JD Thottan || K. Raghavan ||
|-
| rowspan="1" style="text-align:center; background:#dcc7df; textcolor:#000;"| M A Y 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 7
|valign="center" |  9 
| Oral Koodi Kallanayi || Prem Nazir, Sheela || P. A. Thomas || KV Job ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| A U G 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 14
|valign="center" |  10 
| Karutha Kai || Prem Nazir, Sheela || M. Krishnan Nair (director)|M. Krishnan Nair || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 21
|valign="center" |  11  Pazhassi Raja || Kottarakkara Sreedharan Nair|Kottarakkara, Prem Nazir || Kunchako || R. K. Shekhar ||
|-
| rowspan="2" style="text-align:center; background:#ffa07a; textcolor:#000;"| S E P 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 11
|valign="center" |  12  Sree Guruvayoorappan Thikkurissi || S. Ramanathan || V. Dakshinamoorthy ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 30
|valign="center" |  13  Madhu || P. Bhaskaran || K. Raghavan ||
|-
| rowspan="1" style="text-align:center; background:#dcc7df; textcolor:#000;"| O C T 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 8
|valign="center" |  14 
| Omanakuttan || Sathyan (actor)|Sathyan, Sukumari || KS Sethumadhavan || G. Devarajan ||
|-
| rowspan="3" style="text-align:center; background:#d0f0c0; textcolor:#000;"| N O V 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 22
|valign="center" |  15  Madhu || A. Vincent || M.S. Babu Raj ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 23
|valign="center" |  16 
| Bharthavu || Sheela, Kaviyoor Ponnamma || M. Krishnan Nair (director)|M. Krishnan Nair || V. Dakshinamoorthy ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 27
|valign="center" |  17  Ambika || SR Puttanna || G. Devarajan ||
|-
| rowspan="3" style="text-align:center; background:#ffa07a; textcolor:#000;"| D E C 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 5
|valign="center" |  18  Sathyan || Kunchacko || R. K. Shekhar ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 22
|valign="center" |  19 
| Kudumbini || Prem Nazir, Sheela || J. Sasikumar || LPR Varma ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 24
|valign="center" |  20 
| Althaara || Prem Nazir, Sheela || P. Subramaniam || MB Sreenivasan ||
|}

 
 
 

 
 
 
 
 