Hamsa Geetham
{{Infobox film 
| name           = Hamsa Geetham
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         = T. Damodaran
| screenplay     = T. Damodaran
| starring       = Sukumari Ratheesh Balan K Nair Kuthiravattam Pappu Shyam
| cinematography = CE Babu Chandramohan
| editing        = K Narayanan
| studio         = Sreemookambika Creations
| distributor    = Sreemookambika Creations
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by IV Sasi. The film stars Sukumari, Ratheesh, Balan K Nair and Kuthiravattam Pappu in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Sukumari
*Ratheesh
*Balan K Nair
*Kuthiravattam Pappu
*P. K. Abraham Seema

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chanchala noopura thaalam || S Janaki || Bichu Thirumala || 
|-
| 2 || Devi ninte || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Ee Swaram || S Janaki, Kalyani Menon || Bichu Thirumala || 
|-
| 4 || Kannil Naanam || S Janaki, Chorus || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 