Their Compact
{{infobox film
| name           = Their Compact
| image          =
| imagesize      =
| caption        =
| director       = Edwin Carewe
| producer       = Metro Pictures
| writer         = Charles A. Logue (story) Albert S. Le Vino (scenario)
| starring       = Francis X. Bushman Beverly Bayne
| music          =
| cinematography = R. J. Bergquist
| editing        =
| distributor    = Metro Pictures
| released       = September 17, 1917
| runtime        = 7 reels
| country        = United States
| language       = Silent (English intertitles)
}} western film produced and distributed by Metro Pictures and directed by Edwin Carewe. The film stars Francis X. Bushman and Beverly Bayne, a popular romantic screen duo at the time. This film is lost film|lost.  

==Cast==
*Francis X. Bushman - James Van Dyke Moore
*Beverly Bayne - Mollie Anderson
*Henry Mortimer - Robert Forrest
*Harry S. Northrup - Ace High Norton
*Mildred Adams - Verda Forrest
*Robert Chandler - Pop Anderson
*John Smiley - Peters
*Thomas Delmar - Pay Dirt Thompson

==References==
 

==External links==
* 
* 
*Lobby cards for the film:  ,  

 
 
 
 
 
 
 
 
 


 