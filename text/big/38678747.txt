Krishna Aur Kans
{{Infobox film
| name=Krishna Aur Kans   कृष्णा और कंस    கிருஷ்ணா அவுர் கன்ஸ்    కృష్ణా ఔర్ కంస్
| image=
| image_size = 
| alt=
| caption=Theatrical release poster
| director=Vikram Veturi
| producer=
| story=
| based on=
| screenplay=Kamlesh Pandey
| starring=Om Puri Juhi Chawla Prachi Save
| music=Shantanu Moitra Varaprasad
| cinematography=
| editing=
| studio=Reliance Entertainment
| distributor=
| released=  
| runtime=
| country=India Tamil  Telugu
}}
 2012 Hindi computer animation|Flash-animated film produced and distributed by Reliance Entertainment. It was directed by Vikram Veturi.  It was tax free in six states at the time of his release  and released in Hindi,Tamil,Telugu and English.  A new mobile game was also launched on the occasion of Janmashtami after being inspired from film.  It was the widest ever released animation movie in India. 

== Voice Cast (Original Hindi version) == Prachi Saathi as Krishna
*Meghana Erande as Baby Krishna
*Om Puri as Kans
*Rajshree Nath as Radha
*Juhi Chawla as Mother Yashoda
*Manoj Bajpai as Nand
*Anupam Kher as Gargacharya
*Vinod Kulkarni as Trinavert
*Sachin Pilgaonkar as Vasudev
*Supriya Pilgaonkar as Devaki
*Meena Gokuldas as Gwalan  (she was credited as Meena Goculdas.) 
*Harish Bhimani as Subala
*Ninad Kamat as Narad
*Namrata Sawhney as Doruga
*A K Hangal as King Ugrasen 

==Reception==
Akanksha Naval - Shetye of DNA said "The film is more of an engaging fare for children, but even the adults accompanying their kids will find themselves enjoying the film just as much, so go for it!".  
Bavesh Bhatia in his review on Imdb said "Not only an entertainer for your children, Krishna Aur Kans will keep you glued to your seat as you will definitely recall the bygone days of your childhood!" 

==Music==

The music of the film is composed by Shantanu Moitra and lyrics are penned by Swanand Kirkire and Vedavyasa Rangabhattacharya.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Advent Of Krishna (Ayega Koi Ayega)"
| Sonu Nigam, Hamsika Iyer,Swanand Kirkire, Amitabh Bhattacharya
|-
| 2
| "Enchanting Flute"
| Raj Mohan Sinha , Rakesh Chaurasiya
|-
| 3
| "Hey Krishna (Hey Krishna Hey Krishna)"
| Sonu Nigam
|-
| 4
| "Holi (Holi Hain)"
|  Amitabh Bhattacharya,Abhijeet Ghoshal,Hamsika Iyer
|-
| 5
| "Krishna Leaving Vrindavan (Suno Suno Sawaren Ki)"
| Shreya Ghoshal,Pranab Kumar
|-
| 6
| "Krishna Past Time (Gokul Ki Galiyon Me)"
|  Shravan Suresh
|-
| 7
| "Natkhat Natkhat"
|  Shravan Suresh, Swanand Kirkire
|-
| 8
| "Nukkad Wale"
| Shravan Suresh
|-
| 9
| "Putana (Baccho Jara Dur Dur Rahena Re)"
|  Sunidhi Chauhan,Shantanu Moitra
|-
| 10
| "Rasa (Roon Ghoona Re)"
|  Shreya Ghoshal, Babul Supriyo
|-
| 11
| "Shloka"
|  Varaprasad
|}

==References==
 

==External links==
* 

 
 
 
 
 