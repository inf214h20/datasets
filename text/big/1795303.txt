Water (1985 film)
 
 
 
{{Infobox film
| name           = Water
| image          = Waterposter1985.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Dick Clement Denis OBrien
| writer         = Dick Clement Ian La Frenais
| narrator       = 
| starring = {{Plainlist|
* Michael Caine
* Valerie Perrine
* Brenda Vaccaro
* Leonard Rossiter
* Billy Connolly
* Eric Clapton
* George Harrison
* Ringo Starr
}}  Mike Moran Eric Clapton Eddy Grant George Harrison
| cinematography = Douglas Slocombe
| editing        = 
| studio         = HandMade Films
| distributor    = Atlantic Releasing
| released       = 1985
| runtime        = 115 min.
| country        = United Kingdom English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1985 comedy film scripted by Dick Clement and Ian Le Frenais, directed by Clement, and starring Michael Caine. This HandMade Films production was released in U.S. theatres in April 1986 by Atlantic Releasing.

==Plot summary==
The story is set in the fictional Caribbean British colony of Cascara. Widely ignored by the British Government, media and general public, local Governor Baxter Thwaites is having an easy life in his small and peaceful colony. That peace is disturbed when an abandoned oil rig starts delivering water - at the standard of the finest table water brands. Different parties, including Downing Street, the Cascara Liberation Front, the White House and the Cubans take interest in the future of the island and threaten to destroy the cosy way of life enjoyed by the islands inhabitants.

==Other information== Falkland Islands and Invasion of Grenada|Grenada.
 The Honorary Consul) and Billy Connolly as local biracial activist Delgado, supported by the last performance of Leonard Rossiter as Sir Malcolm Leveridge and one of the last performances of Fulton Mackay.

Most of the movie was filmed in and around Soufrière, Saint Lucia, with some scenes shot in Devon, England and at Lee International Studios.

The BBC television presenter Paul Heiney had a small part in the film as part of the In at the Deep End series.
 Paramount Home Video on 1 February 1987. The film received its first DVD edition in North America in 2006, courtesy of Anchor Bay Entertainment.

==Cascara==
The film is largely set on the fictional island of Cascara. In the film an oil well is re-opened and discovered to have mineral water with a slight laxative effect. The islands name itself is a play on this as Cascara is the name of a plant (scientific name Rhamnus purshiana) which has laxative properties.

==Cast==
* Michael Caine as Governor Baxter Thwaites
* Valerie Perrine as Pamela Weintraub
* Brenda Vaccaro as Dolores Thwaites
* Leonard Rossiter as Sir Malcolm Leveridge
* Billy Connolly as Delgado Fitzhugh
* Chris Tummings as Garfield Cooper
* Dennis Dugan as Rob Waring
* Fulton Mackay as Reverend Eric McNab
* Jimmie Walker as Jay Jay
* Dick Shawn as Deke Halliday
* Fred Gwynne as Franklin Spender
* Trevor Laird as Pepito
* Alan Igbon as Cuban
* Maureen Lipman as Margaret Thatcher

==Soundtrack==
The soundtrack principally featured reggae music by Eddy Grant and was released by Ariston Records.

{{Track listing
| extra_column    = Artist
| writing_credits = yes
| headline  = Side 1
| title1          = Water
| note1           = 
| writer1         = Eddy Grant
| extra1          = Eddy Grant
| length1         = 3:56

| title2          = Walking On Sunshine
| note2           = 
| writer2         = Eddy Grant
| extra2          = Eddy Grant
| length2         = 3:50

| title3          = All As One
| note3           = 
| writer3         = Ian La Frenais, Mike Moran
| extra3          = Lance Ellington
| length3         = 3:50

| title4          = The Cascaran National Anthem
| note4           = 
| writer4         = Bill Persky, Dick Clement, Ian La Frenais, Mike Moran
| extra4          = 
| length4         = 0:59

| title5          = Instrumental
| note5           = 
| writer5         = Mighty Gabby
| extra5          = Mighty Gabby
| length5         = 2:55

| title6          = Focus Of Attention
| note6           = 
| writer6         = Dick Clement, George Harrison, Mike Moran
| extra6          = Jimmy Helms
| length6         = 2:10
}}
{{Tracklist
| extra_column    = Artist
| writing_credits = yes
| headline  = Side 2
| title1          = Living On The Frontline
| note1           = 
| writer1         = Eddy Grant
| extra1          = Eddy Grant
| length1         = 3:40

| title2          = Cascara
| note2           = 
| writer2         = Mike Moran
| extra2          = Lance Ellington
| length2         = 3:48

| title3          = Jack
| note3           = 
| writer3         = Mighty Gabby
| extra3          = Mighty Gabby
| length3         = 4:45

| title4          = Celebration
| note4           = 
| writer4         = George Harrison, Mike Moran
| extra4          = Jimmy Helms
| length4         = 3:46

| title5          = Freedom
| note5           = 
| writer5         = Eric Clapton, Ian La Frenais
| extra5          = Billy Connolly, Chris Tummings, The Singing Rebels Band
| length5         = 4:40

| title6          = Water (Instrumental)
| note6           = 
| writer6         = Eddy Grant
| extra6          = Eddy Grant
| length6         = 2:18
}}

The Singing Rebels Band consists of Eric Clapton, George Harrison, Ray Cooper, Jon Lord, Mike Moran, Chris Stainton and Ringo Starr, with backing singers Jenny Bogle and Anastasia Rodriguez.  It spoofs The Concert for Bangladesh organised by Harrison in 1971.

== Reception ==

The movie received a mixed review in the New York Times, which read in part "The folks who packaged this put-on operated on the theory that a lot of eccentric people doing nutty things produce hilarity. The ingredient missing from the fitfully amusing conglomeration of characters is a character for the whole. In kidding everything, the movie leaves us uncertain about whether anything is being seriously kidded." 

== References ==
 

==External links==
* 
*  

 

 

 
 
 
 
 
 
 
 
 
 
 