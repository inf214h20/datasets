Jönssonligan och Dynamit-Harry
{{Infobox film
| name           = Jönssonligan & Dynamit-Harry
|originaltitel= The Johnson Gang & Dynamite-Harry
| image          = Jönssonligan och Dynamit-Harry.jpg
| image_size     = 
| caption        = 
| director       = Mikael Ekman
| producer       = Ingemar Ejve
| writer         = Henning Bahs, Erik Balling, William Aldridge, Sven Melander
| narrator       = 
| starring       = Gösta Ekman, Ulf Brunnberg, Nils Brandt, Björn Gustafson
| music          = Ragnar Grippe
| cinematography = 
| editing        = 
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 102 min
| country        = Sweden Swedish
| budget         = 
| gross          = 
}}
Jönssonligan och Dynamit-Harry (1982) (The Johnson Gang & Dynamite-Harry - International: English title) is a Swedish film about the gang Jönssonligan.

==Cast==
*Gösta Ekman d.y.|Gösta Ekman - Charles-Ingvar "Sickan" Jönsson
*Ulf Brunnberg - Ragnar Vanheden
*Nils Brandt - Rocky
*Björn Gustafson - Dynamit-Harry
*Carl Billquist - Persson
*Dan Ekborg - Gren
*Sten Ardenstam - Appelgren
*Mona Seilitz - Katrin Appelgren
*Weiron Holmberg - Biffen
*Jarl Borssén - Night guard
*Lena Söderblom - Mrs. Lundberg
*Per Grundén - Wall-Enberg
*Peder Ivarsson - Bill
*Peter Harryson - Polis
*Jan Waldekranz - Polis
*Gösta Krantz - Driver

== External links ==
* 
* 

 

 
 
 
 


 