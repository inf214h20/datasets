Road to Salina
{{Infobox film
| name           = Road to Salina
| image          = Road to Salina.jpg
| image_size     =
| alt            = 
| caption        = Theatrical release poster
| director       = Georges Lautner
| producer       = Robert Dorfmann   Yvon Guézel  
| writer         = Georges Lautner   Jack Miller   Pascal Jardin
| narrator       = 
| starring       = Robert Walker, Jr.  Mimsy Farmer Rita Hayworth Marc Porel Christophe   Philip Brigham
| cinematography = Maurice Fellous
| editing        = Michelle David
| studio         = Films Corona
| distributor    = Embassy Pictures|Avco-Embassy
| released       =  
| runtime        = 96 min 
| country        = France   Italy   USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1970 France|French English in the Canary Islands.

==Plot==
Jonas, a young drifter, is wandering in a deserted area on the road to Salina. He stops to drink some water at a desolate roadside service station when Mara, the owner, identifies him as her son Rocky, who disappeared four years ago. Jonas is overwhelmed by the awkward situation, but tired and hungry, he accepts Mara’s offer of room and board. Feeling sorry for Mara, he pretends to be her beloved son Rocky. Jonas is soon comfortably assuming Rocky’s identity. Initially he believes that Mara is just delusional, but when Warren, Mara’s old friend and neighbor, arrives for a visit, surprisingly Warren also acts as if Jonas were Rocky.

As Billie, his alleged sister, comes home, Jonas thinks that the game is over and that he will be finally unmasked as an impostor, but even Billie recognizes him as her brother. The attractive and carefree Billie soon takes him under her wing; they spend all the time together, go skinny dipping in the ocean and an erotic relationship starts between Jonas and his alleged sister. Everything seem to be fine and Jonas begins to relax in his role as Rocky. Mara, Billie and Warren enjoy a dinner party together celebrating Rocky’s come back.

Realizing that Billie is having a passionate affair with her alleged brother, Mara and  Warren  are afraid that the harmony so recently reached would soon be broken. Uneasy, Jonas becomes increasingly interested in finding the reasons for the disappearance of the true Rocky. He has the first clues in his search from Warren, who never expressed any doubt that he is Rocky mentions the past and brings to a conversation the name of Rocky’s girlfriend, Linda. In search for answers, Jonas goes to Salina and finds Linda who runs a local restaurant as Warren told him. He drops a glass at the bar to draw attention to himself and Linda is the first person who fails to recognize him as Rocky. Back at the house, Jonas steals from Billie’s bedroom some old photographs and confirms his suspicions that he does not even resemble the real Rocky. When he confronts Billie with the truth she tells him that she has lied to protect Mara and that she loves Rocky and wanted him back.

In an attempt to resolve his mounting confusion, Jonas visits Linda again. She tells him that she was going to elope with Rocky the day he disappeared. The surprising visit of Charlie, an old friend of Jonas, once again threatens to completely destroy the façade that he is really Rocky, but not only do Mara and Billie seem totally undisturbed when Charlie calls Jonas by his real name, but Charlie and his companions have a good time with Mara, Billie and the bewildered Jonas. Instead of accepting his friends offer of leaving with him, Jonas stays with Billie and Mara.

Jonas finally learns the shocking truth about the nature of his "sisters" behavior.  When Rocky wanted to leave the incestuous relationship with his sister, Billie unintentionally killed him with a rock while trying to stop him. The revelation marks a turning point in Jonas relationship with Billie, and from then on she avoids him. Rebuked by her, Jonas explodes during an argument with Billie. Shaking her against a wall, he accidentally kills her. He runs away in the middle of a rainstorm in spite of Mara’s protestations. Mara begs him to stay and offers him to help him hide Billie’s body under the station as Billie had done with Rocky but instead Jonas goes to Salina and tells the sheriff what has happened. The story is told in flashback.

==Cast== Robert Walker and Jennifer Jones. It was the last film of Ed Begley who died in April, 1970 and the second to last film for legendary screen siren Rita Hayworth.  
* Robert Walker, Jr. – Jonas
* Mimsy Farmer - Billie  
* Rita Hayworth - Mara
* Marc Porel - Rocky
* Ed Begley  - Warren
* Sophie Hardy - Linda
* Bruce Pecheur -Charlie
* David Sachs - Sheriff
* Ivano Staccioli - Lindas Husband

==Soundtrack==
The soundtrack was composed by Bernard Gerard. The main theme of the film, named "Sunny road to Salina" was used by Quentin Tarantino in Kill Bill V 2. 
It also features songs and music from popular French singer Christophe and French/Canadian/British band Clinic (consisting of Philip Brigham, Phil Trainer and Alan Reeves). 

==Reception==
In the U.S.A Road to Salina had a modest and brief release. Its play-date was usually on the lower half of a double feature. Ringgold, The Films Of Rita Hayworth, p. 242  Kevin Thomas in the Los Angeles Times called it "  An admirable ambitious film of strictly sophisticated appeal." He described the film as " A fable showing how tragedy can occur when reality intrudes upon the lonely lives of those who live in a world of fantasy".  The review in Newsday called Road to Salina  " A very strange film... More perversely compelling than it has a right to be". Ringgold, The Films Of Rita Hayworth, p. 244  In the Village Voice, Robert Colaciello said: " If your taste runs to 70s actors having 60s sex in a 50s film so that a 40s star can suffer, then Road to Salina is for you " 

==References==
 

==Bibliography==
*Ringgold, Gene:  The Films of Rita Hayworth: The legend and Career of a Love Goddess, The Citadel Press, Seacacus, N.J, 1974, ISBN 0-8065-0439-0

==External links==
* 

 
 
 
 
 
 
 