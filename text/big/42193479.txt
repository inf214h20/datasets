Late Phases
{{Infobox film
| name           = Late Phases
| image          = File:LATE-PHASES-SXSW-Poster-11x17-195x300.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Adrián García Bogliano
| producer       = 
| writer         = Eric Stolze
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Nick Damici, Ethan Embry, Lance Guest
| music          = Wojciech Golczewski
| cinematography = Ernesto Herrera
| editing        = Aaron Crozier
| studio         = Dark Sky Films, Glass Eye Pix, Site B
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Late Phases is a 2014 horror drama film by Adrián García Bogliano and his first feature film in the English language. The film had its world premiere on March 9, 2014 at South by Southwest and stars Nick Damici as a blind war veteran that becomes the victim of a werewolf attack.

==Synopsis==
Ambrose (Nick Damici) is a blind Vietnam War veteran that moves into a retirement community with his seeing eye dog upon the prompting of his son Will (Ethan Embry). Hes shocked when he narrowly survives an attack by what he believes to be a werewolf. The community has been the focus of several brutal dog attacks that have killed several residents, but Ambrose now believes that it is werewolves and not dogs that have been doing the slaughtering. Now Ambrose is preparing himself for the next full moon, when he will make his strike against his lupine would-be aggressors. 
 
==Cast==
*Nick Damici as Ambrose
*Ethan Embry as Will
*Lance Guest as Griffin
*Tina Louise as Clarissa
*Rutanya Alda as Gloria B.
*Caitlin OHeaney as Emma
*Erin Cummings as Anne
*Tom Noonan as Father Roger
*Larry Fessenden as OBrien
*Al Sapienza as Bennet
*Bernardo Cubria as Officer Lang
*Karen Lynn Gorney as Delores
*Karron Graves as Victoria Kaye
*Haythem Noor as Nelson
*Kareem Savinon as Edward

==Production==
Bogliano first announced his intention to film Late Phases in 2012, as he had been impressed with the script that Stolze had written.  Nick Damici was confirmed to be leading the film in February 2013,  and filming began in Hudson Valley, New York in June of the same year.  Damici had some difficulty portraying a blind man, as he stated that you "cant just blindfold yourself to learn because it doesn’t work that way. Even when you’re blindfold you’re seeing more than a blind person sees. I just had to do it looking peripherally and trusting Adrian that it came off okay."    The films biggest challenge came from the werewolf transformation scene, as Bogliano noted that "  the producers knew if it was going to work until we finally finished." 

==Reception==
The film received mixed reviews. Shock Till You Drop gave Late Phases a lukewarm review, commenting that while it was "occasionally thrilling" it was "obviously going for the Bubba Ho-Tep vibe" but didnt have the "ominpresent quirkiness and energy Bubba Ho-Tep carried."  Twitch Film panned the film overall, as they felt that it was "a concept that looks like it absolutely should have worked on paper, but ends up becoming a cautionary tale about what happens when you put too much stock in your wolf rather than your sheep."   Patrick Cooper of   gave the film five skulls out of five, stating that the films is "a tale of hardcore werewolf violence, a tangible father/son relationship, redemption, and a whole lotta heart. It’s funny, brash, and exciting, but knows when to pull back and let the emotion sink in. Simply put, it’s a masterpiece of the werewolf genre because of what it accomplishes on top of the scares, which is deliver a truly emotional, heartfelt story of a father and son."  

==Release==
The film was selected to screen In April of 2014 at the Stanley Film Festival. The home release is set for March 10, 2015 on Digital Download and on Blu-ray & DVD over Dark Sky Films. 

==References==
 

==External links==
*  

 
 
 
 
 