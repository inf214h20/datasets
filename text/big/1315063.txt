Dragon Ball Z: The Return of Cooler
 
{{Infobox film
| name            = Dragon Ball Z: The Return of Cooler
| image           = Coolerreturn12345.jpg
| caption         = FUNimation DVD cover
| director        = Daisuke Nishio
| producer        = Chiaki Imada Rikizô Kayano
| writer          = Story: Akira Toriyama Screenplay: Takao Koyama
| starring        = See  
| music           = Shunsuke Kikuchi
| cinematography  =
| editing         =
| distributor     = 
| released        =   August 13, 2002 in North America and Europe
| runtime         = 46 minutes gross           = ¥2.05 billion ($14.9 million)
 
}}
Dragon Ball Z: The Return of Cooler, known in Japan as  , is the sixth   and the third   on March 17, 2006. These movies are the first Dragon Ball-related movies to receive a theatrical release in the United States. This film, alongside Coolers Revenge, was re-released to DVD and Blu-ray in a double feature on November 11, 2008. It was later re-released to DVD again on December 6, 2011 in a remastered 4-pack with the next three films in the series. 

==Plot== Super Saiyan Goku. The peaceful Namekians found themselves without a home for months. Eventually they had a new planet created for them with the help of the Dragon Balls. For three years or so they lived in peace on their new planet. One day however, a strange metal planet began to absorb New Namek. The silver planet wrapped around New Namek, absorbing its energy. Dende (Dragon Ball)|Dende, who is now the Earths guardian, senses the plight of his people and calls Goku to ask for help.
 Master Roshi arrive and encounter an army of strange, large, silent robots. They soon learn, to their horror, that the mastermind behind the invasion is Cooler, Friezas older brother, who Goku was thought to have killed a few years earlier and who now plans to use the Namekians as biological fuel for his ship, the Big Gete Star. Goku fights Cooler alone, while the other Z-Fighters battle Coolers robots.

At first the Z-Warriors have trouble penetrating the robots armor, but Piccolo instructs them to concentrate their energy to one point, and doing so allows them to destroy a few of the robots. However, there are far too many to handle and all the warriors, except Piccolo, are captured along with a village of Namekians. Piccolo is left behind, and destroys all the robots. He then makes his way to rescue those who were captured.

Elsewhere, it becomes apparent that Goku is no match for Coolers new metallic form which gives him the ability to regenerate himself. Cooler also reveals his ability to use the Instantaneous Movement technique, which Goku also uses. Goku continues to struggle even after becoming a Super Saiyan. Cooler reveals that the Big Gete Star constantly monitors his body, and fixes any flaws that may occur, in this case Coolers arm being torn off by Goku.

Just before Goku is choked to death by Cooler, Vegeta, who had arrived in a separate pod, shows up just in time to help Goku. The two Super Saiyans attack Cooler and they soon are able to kill him. However, the Big Gete Star once again corrects a flaw in Coolers design, in this case, giving Cooler a thousand bodies rather than just one. Outnumbered and exhausted, Goku and Vegeta are captured and transported to the Big Gete Stars core to be used as energy.

As Cooler is leeching their Saiyan strength via strange wires, he snidely explains his manner of survival: not long after his defeat at Gokus hands, a nearby computer chip floated among a debris of spacecraft until it gained sentience and absorbed everything in its proximity and formed a spaceship, and at one point, Coolers remains, including pieces of his brain, were fused with the main computer and Cooler subsequently took control, effectively meaning that Cooler is the Big Gete Star. Goku and Vegeta regain consciousness and release all their Super Saiyan energy to the wires, overloading the system. They then come face to face with the Big Gete Stars core, the true Cooler. Cooler attempts to crush Goku, but his hand is severed by Vegeta, giving Goku enough time to blast him with the last of his energy, causing his ultimate demise. Meanwhile, Piccolo arrives in the Big Gete Star, and meets up with a clone of Cooler, which explodes. Eventually, all the other clones of Cooler and every other robot in the Big Gete Star explode. Piccolo and the rest escape before the Big Gete Star leaves New Nameks orbit and explodes.

Goku and Vegeta fall from the sky near the rest of the Z-Fighters, and everyone rejoices. Vegeta is nowhere to be found, but Goku still compliments his role in defeating Cooler. Vegeta is shown in his spacepod holding the computer chip that created the Big Gete Star. He then crushes it, ensuring that it will never create another monstrosity like the Big Gete Star ever again.

==Cast==
{| class="wikitable"
|-
! Character Names

! Voice Actor (Japanese)
! Voice Actor (English)
|-
| Goku|| Masako Nozawa|| Sean Schemmel
|- Gohan || Masako Nozawa || Stephanie Nadolny
|- Piccolo || Toshio Furukawa || Christopher Sabat
|-
| Krillin || Mayumi Tanaka || Sonny Strait
|-
| Vegeta|| Ryo Horikawa || Christopher Sabat
|-
| Master Roshi || Kôhei Miyauchi || Mike McFarland
|- Oolong || Naoki Tatsuta|| Bradford Jackson
|-
| Yajirobe || Mayumi Tanaka|| Mike McFarland
|- Dende || Laura Bailey
|-
| List of supernatural beings in Dragon Ball#Deities|Mr. Popo || Toku Nishio|| Christopher Sabat
|- Moori || Kinpei Azusa|| Christopher Sabat
|- Meta Cooler Andrew Chandler
|-
| Narrator || Joji Yanami || Kyle Hebert
|}

==Music==
*Opening Theme
*# "Cha-La Head-Cha-La"
*#* Lyrics:  , Vocals: Hironobu Kageyama

*Ending Theme
*#   Yuka

The score for the English-language version was composed by Mark Menza; however the remastered release contains an alternate audio track containing the English dialogue and Japanese background music.

==References==
 

==External links==
*   of Toei Animation
*  
*  

 

 
 
 
 
 
 