Nyay Anyay
{{Infobox film
| name           = Nyay Anyay
| image          = 
| image_size     = 
| caption        = 
| director       = Lawrence DSouza	 
| producer       = Sudhakar Bokade
| writer         = 
| narrator       = 
| starring       = Jeetendra Jaya Prada Sumeet Saigal Shilpa Shirodkar
| music          = Anand-Milind
| lyrics         = Majrooh Sultanpuri
| editing        = 
| distributor    = 
| released       = 28 September 1990
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

 Nyay Anyay  is a 1990 Bollywood film directed by Lawrence DSouza and starring Jeetendra, Jaya Prada, Sumeet Saigal, Shilpa Shirodkar and Anupam Kher. This was the first movie of Lawrence DSouza and Sudhakar Bokade as director and producer. The duo had later collaborated in making of movies Saajan and Sapne Sajan Ke. The movie was disaster at box office.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title
|-
| 1
| "Aaj Teri Baahon Mein"
|-
| 2
| "Raat Jaanejaa"
|-
| 3
| "Jeeyo Ki Ek Saal"
|}

==External links==
*  

 
 
 
 