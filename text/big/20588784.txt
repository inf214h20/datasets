Thodasa Roomani Ho Jaayen
Thodasa Roomani Ho Jayen is a Hindi movie directed by Amol Palekar and released in 1990. It features Anita Kanwar, Nana Patekar and Vikram Gokhale as major actors. This movie has become a part of management courses and study pertaining to human behaviour.

The movie has a lot of poetry within itself. Anita Kanwar is an unconventional girl lacking confidence. All people around her including her father and brothers keep advising her how she should be. Then, a magician who can bring rain comes to their lives. He helps her to realize the potential of believing. He brings back beauty and confidence to Anita Kanwar. Now, she realizes altogether a different life, just because of believing or changing perception.

==Songs and Dialogues==
(Bim: Do raato ke bich ek chhota sa din bechara hai...kya kare?
Barishkar: Ummh...Do dino ke bich ek chhoti si raat. Raat ke idhar ek din, raat ke udhar ek din. 
Din to hai do, bas raat ek hai. Karlo jo chaho, ban lo jo chaho!
Aaj hai tumhara, aur Kal bhi tumyara hai!

Bim: Do ghaato ke bhic ek patli si dhaara hai....kya kare?
Barishkar: Ghaat nahi chalte, dhara chalti hai. 
Patli si dhara chal ke samandar se milti hai. 
Samandar se milti hai, milke kho jaati hai. 
Ghaat "Ghaat" hi rehte hai...wo "Samandar" ho jaati hai!)

(Song)
Samandar ko baandhe aisa koi ghaat nahi, 
Kadmo ko thame aisi koi baat nahi.

(Barishkar: Karlo jo chaho, ban lo jo chaho!
Tum kar nahi sakte aisa kahi kuchh bhi nahi.
Isliye ye na kehna kabhi...kya karu...kaise karu.
Mein kar sakta hoon, Mein karta hoon, Muze karna hi hai, Mein karunga.

Bim: Mein kar sakta hoon, Mein karta hoon, Muze karna hi hai, Mein karunga. (2)
Yea! Main kar nahi sakta nahi aisa kahi kuch bhi nahi.)

(Song)
Samandar ko baandhe aisa koi ghaat nahi, 
Kadmo ko thame aisi koi baat nahi.
Patli si dhaara, jaake samandar se milti hai, 
Samandar se milti hai, Aur milke kho jaati hai

Ghaat "Ghaat" hi rehte hai...wo "Samandar" ho jaati hai!

==Details==
*Released in: 1990
*Language: Hindi
*Actor: Anita Kanwar, Nana Patekar, Vikram Gokhale
* 
* 
* 
*Duration: 160 minutes

The movie was shot in Panchmarhi in Madhya Pradesh

== External links ==
*  

Movie was released on Aug.23 1990.Date can be verified from the certificate shown in the beginning of film.

 
 
 


 