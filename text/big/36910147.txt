When Day Breaks
 
{{Infobox film
| name           = When Day Breaks
| image          = When Day Breaks.jpg
| caption        = Film poster
| director       = Goran Paskaljević
| producer       = Goran Paskaljević
| writer         = Filip David Goran Paskaljević
| starring       = Mustafa Nadarević
| music          = 
| cinematography = Milan Spasić
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Serbia
| language       = Serbian
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Mustafa Nadarević as Professor Miša Brankov
* Mira Banjac as Ana Brankov
* Zafir Hadžimanov as Marko Popović
* Predrag Ejdus as Rabbi
* Meto Jovanovski as Mitar
* Toma Jovanović as Najfeld
* Rade Kojadinović as Kosta Brankov
* Olga Odanović as a Refugee
* Nada Sargin as Marija
* Damir Todorovic as German Officer

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Serbian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 