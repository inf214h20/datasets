Where the Money Is
{{Infobox film
| name = Where the Money Is
| image = Where the Money Is.jpg
| caption =
| director = Marek Kanievska
| producer = Chris Dorr Ridley Scott Charles Weinstock Chris Zarpas
| story = E. Max Frye
| screenplay = E. Max Frye Topper Lilien Carroll Cartwright  Susan Barnes
| music = Mark Isham
| cinematography = Thomas Burstyn
| editing = Garth Craven Samuel Craven Dan Lebental
| studio = PolyGram Filmed Entertainment Gramercy Pictures
| distributor = USA Films
| released =  
| runtime = 89 minutes
| country = United States
| language = English
| budget = $18 million
| gross = $5,661,798
| preceded by =
| followed by =
}}
Where the Money Is is a 2000 film directed by Marek Kanievska, written by E. Max Frye, and starring Paul Newman, Linda Fiorentino, and Dermot Mulroney.

==Plot==
Legendary bank robber Henry Manning pushes his luck too far and ends up in prison, where he suffers a massive stroke. He is transferred to a nursing home, in the care of Carol Ann McKay, a high school prom queen who married her boyfriend Wayne, the star of her schools football team, and whose glamour days are well behind her. Carol Ann starts to suspect that Henry isnt as sick as he seems, and she and Wayne are soon working with Henry to plan his last and greatest score.

==Cast==
* Paul Newman as Henry Manning
* Linda Fiorentino as Carol Ann McKay
* Dermot Mulroney as Wayne
* Susan Barnes as Mrs. Foster
* Anne Pitoniak as Mrs. Tetlow
* Bruce MacVittie as Karl
* Irma St. Paule as Mrs. Galer
* Michel Perron as Guard
* Dorothy Gordon as Mrs. Norton
* Rita Tuckett as Mrs. Weiler
* Diane Amos as Kitty
* Dawn Ford as Cheryl, Wife #2
* T.J. Kenneally as Farwell Welk
* Rod McLachlan as Lloyd the Cop
* Bill Corday as Grounds Worker
* Gordon McCall as Handyman
* Charles S. Doucet as Tom
* Arthur Holden as Bob
* Frank Fontaine as Cop
* Richard Jutras as Manager
* Frankie Faison as Security Guard
* Vlasta Vrána as Jewelry Store Employee
* Heather Hiscox as TV Announcer
* Michael Brockman as FBI Agent

==External links==
*  
*  
*  
* Please note, the website   has nothing to do with the film "Where the Money Is"

 

 
 
 
 
 
 
 
 
 
 


 
 