Kallukkul Eeram
{{Infobox film name           = Kallukkul Eeram image          = Kallukkul Eeram.jpg caption        = Official DVD Box Cover director       = P. S. Nivas writer         = Rangarajan (dialogues) screenplay     = P. Bharathiraja story          = Chandrabose starring  Sudhakar Aruna Aruna Vijayashanti Janagaraj Chandrasekhar Chandrasekhar
|producer       = Neelima music          = Ilaiyaraaja cinematography = P. S. Nivas editing        = Chandy studio         = Neelima Movie Makers distributor    = Neelima Movie Makers released       = 29 February 1980 runtime        = 120 minutes language  Tamil 
}} Aruna and Vijayashanti. 

== Synopsis ==
This is  a story of two innocent village girls who fall in love with the Director and Hero of the cinema crew visiting their village for a shoot. Aruna falls  in love with Bharathiraaja and Vijayashanthi with Sudhagar.

Bharathiraajas movie crew lands on the picturesque and primeval village. The villagers are enthralled by the visiting crew. Vijaya Shanthi, a witty girl and Aruna, who perpetually wears an expression of shock and surprise combined, her marble-eyes not letting any other emotions through, are constant on lookers. Aruna steps into a frame of the movie and is admonished by Bharathiraaja. Vijay Shanthi and Aruna stage a role play song with the village children with V Shanthi as the heroine and Aruna as the director. Bharathi Raaja and Sudhakar walk by and catch them in the act. They appreciate the villagers amateur attempt and praise them. Aruna returns some money she finds, which belongs to Bharathiraaja and he further appreciates her honesty.

Gounda Mani, who is Arunas dad is the village launder. Aruna delivers the laundered clothes to the crew. As she visits Bharathiraaja each time, she also secretly sends him a flower or a note.

Vijaya Shanthi develops romantic feelings towards Sudhakar, only to be told by him that he meets several girls like her and she mistook his friendly gestures. In a moment when a supporting artiste did not show up, Bharathiraaja grabs Aruna from the crowd and makes her play a one line part.

Karuppan who slew the arm of another villager who once teased Aruna, returns from jail. He finds out about Bharathiraaja grabbing Arunas arm and tries to kill him by rolling rocks on him. However, Aruna finds out and saves Bharathiraaja. He notices later that the cloth tied around his injured arm belongs to Arunas saree. 
During the village temple festival, Vijaya Shanthi dances in the play. However, as she was jilted by Sudhagar, she commits suicide. Karuppan tries to molest Aruna in the groves, but ChandraShekar, the village madcap kills him.

The Crew leaves the village. As they leave, Aruna stops Bharathiraaja, but doesnt tell him anything. Hounded by memories of her, he returns and they unite on the river banks. As he holds her hand, chandra Shekar kills him from behind.

==Cast==
* P. Bharathiraja Sudhakar
* Aruna
* Vijayashanti
* Vennira Aadai Nirmala
* Goundamani Janagaraj
* Ramanathan
* Senadhipathi
* Chandrasekhar
* Krishnamoorthy
* Ambalavaanan

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Gangai Amaran || 4:19
|-
| 2 || "Ennatthil Yedho" || S. Janaki || 4:08
|-
| 3 || "Kothamalli Poove" || Malaysia Vasudevan, S. Janaki || 4:10
|-
| 4 || "Siru Ponmani" || Ilaiyaraaja, Malaysia Vasudevan, S. Janaki || 3:59
|-
| 5 || "Thoppiloru Natakam" || Ilaiyaraaja, Malaysia Vasudevan, S. P. Sailaja || Muthuvendhan || 4:29
|}

==References==
 
* 
* 

==External links==
*  

 

 
 
 
 
 