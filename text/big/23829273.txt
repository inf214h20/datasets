Due Date
 
{{Infobox film
| name           = Due Date
| image          = Due Date Poster.jpg
| caption        = Theatrical release poster
| alt            = A man in a ragged blue shirt with his wrist in a cast, and his arm around a smiling bearded man holding a bulldog that is wearing a protective cone around its neck.
| director       = Todd Phillips
| producer       = Todd Phillips Daniel Goldberg Susan Downey
| screenplay     = Alan R. Cohen Alan Freedland Adam Sztykiel Todd Phillips
| story          = Alan R. Cohen Alan Freedland Robert Downey Jr. Zach Galifianakis Michelle Monaghan Juliette Lewis Jamie Foxx
| music          = Christophe Beck
| cinematography = Lawrence Sher
| editing        = Debra Neil-Fisher Green Hat Films Warner Bros. Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $65 million   
| gross = $211.8 million
}}
 road film directed by Todd Phillips, co-written by Alan R. Cohen, Alan Freedland, and Adam Sztykiel, and starring Robert Downey, Jr. and Zach Galifianakis. The film was released on November 5, 2010.  The film was shot in Las Cruces, New Mexico, Atlanta, Georgia (U.S. state)|Georgia, and Tuscaloosa, Alabama|Tuscaloosa, Alabama (U.S. state)|Alabama.

==Plot==
Peter Highman (Robert Downey, Jr.) is on a plane, flying home to be with his wife Sarah (Michelle Monaghan), who is due to give birth. When an actor sitting behind Peter, Ethan Tremblay (Zach Galifianakis), misuses the words "terrorism|terrorist" and "bomb", Peter and Ethan are both escorted off the plane. This is only the first of a series of misadventures caused by the drastically dysfunctional Ethan. Peter, now on the No Fly List and missing his wallet, agrees to drive with Ethan to Los Angeles.
 Danny McBride) refuses to accept Ethans stage name I.D., leading to a violent altercation.

After a night at a rest stop, Peter decides to drive off and leave Ethan there, but returns after wrestling with his conscience. Peter gets Ethan to drive so Peter can get some rest, but Ethan falls asleep at the wheel and crashes the car. After Peter and Ethan are picked up by Peters friend Darryl (Jamie Foxx), Peter again decides to part company with Ethan. Darryl initially persuades him otherwise, but then throws Ethan out after mistakenly drinking Ethans fathers ashes.

When Darryl lets them use his Range Rover to make the rest of the trip, Ethan mistakenly drives to the Mexico–United States border. Despite assuring Peter that hell handle the situation, Ethan flees, and Peter is arrested for possession of marijuana. The Mexican Federal Police lock Peter up, but Ethan steals a truck and breaks him out, causing several car crashes in the process.

When they stop at the Grand Canyon, Peter confesses that he tried to leave Ethan at the rest area. Ethan makes a confession of his own: he has Peters wallet. Peter and Ethan leave for California. When Ethan finds a gun in the truck, he accidentally shoots Peter. Arriving at the hospital where Sarah is in labor, Peter passes out from loss of blood.

Sarah delivers the baby safely, and Peter expresses his discomfort at his new daughter being named Rosie Highman. Ethan leaves while telling Peter to call him. At the end, Ethan guest stars on an episode of his favorite television program, Two and a Half Men with Peter and Sarah watching it in bed with their daughter.

==Cast==
* Robert Downey, Jr. as Peter Highman
* Zach Galifianakis as Ethan Tremblay / Ethan Chase
* Michelle Monaghan as Sarah Highman Old School) 
* Jamie Foxx as Darryl Johnson Matt Walsh as TSA Agent RZA as Airline Screener Danny McBride as Western Union Employee Lonnie
* Todd Phillips as Barry
* Mimi Kennedy as Sarahs Mom
* Keegan-Michael Key as New Father
* Aaron Lustig as Dr. Greene Marco Rodríguez as Federal Agent
* Brody Stevens as Chauffeur Charlie Harper   Alan Harper  
 Jake Harper, but the scene was cut short, and his cameo was not shown in the original run. Alan Arkin filmed scenes as Peters long lost father, but they were not left in the final cut of the film.

==Marketing== The Town, Life As We Know It.

Previews for the film feature the songs "New Moon Rising" by Wolfmother and the original version of "Check Yo Self" by Ice Cube.

In the film, Ethan is a huge fan of the sitcom Two and a Half Men. He also mentions that he started a website called itsrainingtwoandahalfmen.com. As a joke, a website was actually launched with the same name. 

==Soundtrack==
Due Date (Original Motion Picture Soundtrack) was released on November 2, 2010 by WaterTower Music.
 
{{Infobox album  
| Name = Due Date (Original Motion Picture Soundtrack)
| Type = Soundtrack
| Artist = Various Artists
| Cover =
| Recorded = 2010
| Released = November 2, 2010
| Length =
| Label = WaterTower Music
| Reviews =
}}

===iTunes version===
{{Track listing
| extra_column = Artist Hold On, Im Comin
| extra1 = Sam & Dave
| length1 = 2:31 New Moon Rising
| extra2 = Wolfmother
| length2 = 3:45
| title3 = Is There a Ghost
| extra3 = Band of Horses
| length3 = 2:59
| title4 = People Are Crazy
| extra4 = Billy Currington
| length4 = 3:51
| title5 = White Room Cream
| length5 = 4:58
| title6 = This Is Why Im Hot MIMS
| length6 = 4:17
| title7 = Sweet Jane
| extra7 = Cowboy Junkies
| length7 = 3:35
| title8 = Amazing Grace
| extra8 = Rod Stewart
| length8 = 2:02 Check Ya Self 2010 (feat. Chuck D with Lisa Kekaula)
| extra9 = Ice Cube
| length9 = 3:25
| title10 = Glaucoma
| extra10 = Christophe Beck
| length10 = 2:13
| title11 = A Good Sign
| extra11 = Christophe Beck
| length11 = 1:36
| title12 = Ethans Theme
| extra12 = Christophe Beck
| length12 = 1:19
}}

; Additional songs

The following songs are not included in the soundtrack, but they appear in some parts of the film:
 Danny McBride Mykonos - Fleet Foxes Old Man Live at Massey Hall) - Neil Young Hey You - Pink Floyd
* Theme from Two and a Half Men

==Reception==

=== Box office ===
Despite mixed critical reviews Due Date was commercially successful, it earned $12,216,515 at the North American domestic box office on its release day and $43,478,266 on its first week, placing behind  , Tangled, Burlesque, Megamind, Love and Other Drugs, Unstoppable (2010 film)|Unstoppable and Faster (2010 film)|Faster. It closed in theaters on January 27, 2011. As of April 2011 Due Date grossed over $100 million in the U.S.A. and Canada as well as $200 million worldwide, against a production budget of $65 million. {{cite web 
| title= Due Date (2010) 
| url= http://www.boxofficemojo.com/movies/?id=duedate.htm 
| work= Box Office Mojo 
| publisher= Amazon.com 
| accessdate= January 22, 2011
}}
 

===Critical response ===
 
Due Date received average reviews. Rotten Tomatoes gives the film a score of 40% "rotten", or a 5.2/10 rating, based on 185 reviews. The sites consensus is: "Shamelessly derivative and only sporadically funny, Due Date doesnt live up to the possibilities suggested by its talented director and marvelously mismatched stars." {{cite web
| title=Due Date Movie Reviews, Pictures
| url=http://www.rottentomatoes.com/m/due-date
| work=Rotten Tomatoes
| publisher=Flixster
| accessdate=September 18, 2011
}} 
Metacritic gives the film a "mixed or average" score of 51, based on reviews from 39 critics. {{cite web
| title=Due Date Reviews, Ratings, Credits
| url=http://www.metacritic.com/movie/due-date
| work=Metacritic
| publisher=CBS
| accessdate=September 18, 2013
}} 

Roger Ebert of the Chicago Sun-Times awarded the film two and a half stars out of a possible four, noting "The movie probably contains enough laughs to satisfy the weekend audience. Where it falls short is in the characters and relationships." Ebert compares Due Date to the 1987 film Planes, Trains and Automobiles but bemoans that Due Date could have learned and offered more. {{cite web
| author=Roger Ebert
| url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20101103/REVIEWS/101109991
| title=Due Date :: rogerebert.com :: Reviews
| publisher=Chicago Sun Times
| accessdate=December 11, 2010
}} 

=== Home media ===
 
Due Date was  released on DVD and Blu-ray Disc|Blu-ray on February 22, 2011. 

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 