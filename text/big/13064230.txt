Children of Pleasure
{{Infobox film
| name           = Children of Pleasure
| image          = Children of Pleasure.jpg
| image_size     =
| caption        = Poster
| director       = Harry Beaumont
| producer       = 
| writer         = Richard Schayer Crane Wilbur
| narrator       = 
| starring       = Lawrence Gray Wynne Gibson
| music          = 
| cinematography = Percy Hilburn   Technicolor sequences 
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Sound All-Talking
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Children of Pleasure  is a 1930 American MGM musical comedy film directed by Harry Beaumont originally released with Technicolor sequences. It was adapted from Crane Wilburs stage success of 1929 The Song Writer.

==Plot==
Danny, an acclaimed singer and songwriter, falls in love with a socialite girl who is just playing around. He doesnt realize that his girl-Friday is the one he really loves until it is almost too late. Although he is awestruck by high society, he overhears the girls admission that she is stringing him along just in time to avoid marriage.  Danny is notably Jewish, and among the issues the movie raises is his temptation to assimilate into the larger culture.  

The film is an adaptation of a play that riffed on the real-life relationship between songwriter Irving Berlin and Long Island socialite Ellin Mackay, which was all over the gossip columns in the late 1920s.  Mackays millionaire father cut her off and did not speak to her for years because, after a long courtship, she married Berlin, who was Jewish. (Unlike the fickle debutante in the film, Mackay stayed with Berlin, and their marriage lasted over sixty years.)

The film is played against a theatrical backdrop, and contains many songs and production numbers.

==Cast==
*Lawrence Gray ...  Danny Regan 
*Wynne Gibson ...  Emma Em Gray 
*Judith Wood ...  Patricia Pat Thayer (as Helen Johnson)  Kenneth Thomson ...  Rod Peck (as Kenneth Thompson) 
*Lee Kohlmar ...  Bernie (as Lee Kolmar) 
*May Boley ...  Fanny Kaye 
*Benny Rubin ...  Andy Little 
*Jack Benny ...  Himself, Cameo Appearance (uncredited) 
*Sidney Bracey ...  Miles (butler) (uncredited) 
*Mary Carlisle ...  Secretary (uncredited) 
*Carrie Daumery ...  Dowager (uncredited) 
*Ann Dvorak ...  Chorus girl (uncredited) 
*Jay Eaton ...  Eddie Brown (uncredited)

==Production==
The movie was originally premiered and released with Technicolor sequences in the summer of 1930.  One reviewer noted that "the revue scenes filmed in Technicolor being particularly lavish."    These color sequences were later replaced with a black-and-white version that had been filmed simultaneously because the backlash against musicals (which occurred in the autumn of 1930) made the expense of printing color prints superfluous and frivolous. Only this black-and-white general release version currently exists. The same fate was shared by another of MGMs major musicals, Cecil B. DeMilles Madam Satan which was released during the same time period. A segment of one of the Technicolor sequences survives in an MGM short subject in color titled Roast Beef and Movies (1934).

==Soundtrack==
Lawrence Gray recorded two of his songs from the picture for Brunswick Records. His rendition of the songs Leave It That Way and The Whole Darned Things For You were released on Brunswicks popular ten inch series on record number 4775.

==See also==
* List of American films of 1930
* List of early color feature films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 