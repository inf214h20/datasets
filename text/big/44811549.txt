Yare Nee Abhimani
{{Infobox film
| name           = Yare Nee Abhimani
| image          =
| caption        =
| director       = D. Rajendra Babu
| producer       = Lakshman Pramod Kumar
| writer         = Remake
| screenplay     = D. Rajendra Babu
| starring       = Shiva Rajkumar Ramya Krishnan Sangita Srinath
| music          = Hamsalekha
| cinematography = P K H Das
| editing        = Shashikumar
| studio         = Sri Jwalamalini Devi Productions
| distributor    =
| released       =  
| country        = India Kannada
}}
 2000 Cinema Indian Kannada Kannada film,  directed by  D. Rajendra Babu and produced by Lakshman Pramod Kumar. The film stars Shiva Rajkumar, Ramya Krishnan, Sangita and Srinath in lead roles. The film had musical score by Hamsalekha.   The film was based on Telugu film Aayanaki Iddaru and Hindi film Aaina (1993 film)|Aaina.

==Cast==
 
*Shiva Rajkumar
*Ramya Krishnan
*Sangita
*Srinath
*Dwarakish
*Doddanna
*Mandeep Roy
*Vijayasarathi Rekha
*M S Karanth
 

==Soundtrack==
The music was composed by Hamsalekha. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Chithra || Hamsalekha || 05.06
|-
| 2 || Mysore Seeme || S. P. Balasubrahmanyam || Hamsalekha || 05.12
|-
| 3 || Shrungara Kavyavo || S. P. Balasubrahmanyam, Rathnamala Prakash || Hamsalekha || 05.05
|- Chithra || Hamsalekha || 05.16
|- Chithra || Hamsalekha || 05.04
|- Chithra || Hamsalekha || 04.51
|}

==References==
 

==External links==
*  

 
 
 
 
 


 