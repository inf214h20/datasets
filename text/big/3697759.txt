Details (film)
{{Infobox film
| name           = Details
| image          = Details (film).jpg
| caption        = Swedish cover
| director       = Kristian Petri
| producer       = Christer Nilson   Nik Hedman   Lars Kolvig   Enrico Molé
| writer         = Jonas Frykberg   Lars Norén (play)
| starring       = Rebecka Hemse   Michael Nyqvist   Jonas Karlsson
| music          = David Österberg   Johan Söderberg
| cinematography = Göran Hallberg
| editing        = Johan Söderberg
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = Sweden Denmark
| language       = Swedish
| budget         = 
}}

Details ( ) is a 2003 Swedish drama film directed by Kristian Petri about a young author and her various relationships over ten years. It was entered into the 26th Moscow International Film Festival.   

==Cast==
Rebecka Hemse       .... 	Emma 
Michael Nyqvist	.... 	Erik 
Jonas Karlsson	.... 	Stefan 
Pernilla August	.... 	Ann 
Gunnel Fred 	.... 	Eva 
Valter Skarsgård    .... 	Daniel (young) 
Gustaf Skarsgård    .... 	Daniel (old) 
Ingela Olsson	.... 	Actress 
Leif Andrée 	.... 	Actor 
Ebba Hultkvist	.... 	Rineke

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 