Quo Vadis (2001 film)
{{Infobox film name = Quo Vadis image =  caption =  director = Jerzy Kawalerowicz producer = Mirosław Słowiński   Jerzy Kajetan Frykowski screenplay = Jerzy Kawalerowicz based on =   starring = Paweł Deląg Magdalena Mielcarz Bogusław Linda music = Jan Kaczmarek editing = Cezary Grzesiuk Chuck Bush studio = Zespół Filmowy Kadr distributor = Zespół Filmowy Kadr released = 14 September 2001 runtime = 170 minutes country = Poland language = Polish budget = $ 18 mln (ca.76 140 000 PLN)  gross =
 }} 2001 Cinema Polish film directed by book of the same title by Henryk Sienkiewicz. It was Polands submission to the 74th Academy Awards for the Academy Award for Best Foreign Language Film, but was not nominated.        

== Plot ==
The central plot in the movie revolves around the love of a Roman patrician, Marcus Vinicius, towards a Christian girl (coming from the territory of modern-day Poland) set against the backdrop of the persecutions against Christians during the reign of Nero.

In the beginning, Lygia, a Christian and hostage of Rome, becomes the object of Vinicius love but she refuses his advances. Vinicius friend Petronius tries to manipulate Nero, who has authority over all Roman hostages, to give Lygia to Vinicius, but Lygia is taken into hiding by Christians. Marcus Vinicius decides to find her and force her to be his wife. He goes to a Christian meeting along with Croton, a gladiator, to find her. After following her from the meeting, Marcus tries to take her, but Ursus, a strong man and friend of Lygia, kills Croton. Marcus himself is wounded in the fight, but is taken care of by Lygia and the Christians. Seeing their kindness he begins to convert to Christianity, and Lygia accepts him.

Rome catches fire while the emperor, Nero, is away. Nero returns and sings to the crowd, but they become angry. At the suggestion of Neros wife, the Christians are blamed for the fire, providing a long series of cruel spectacles to appease the crowd. In one of the spectacles, Ursus faces a bull carrying Lygia on its back. Ursus wins and, with the crowd and guards in approval, Nero lets them live.

Nero kills himself, and Vinicius and Lygia leave Rome.

==Cast==
*Paweł Deląg – Marcus Vinicius
*Magdalena Mielcarz – Lygie Callina
*Bogusław Linda – Petronius
*Michał Bajor – Nero
*Jerzy Trela – Chilon Chilonides
*Danuta Stenka – Pomponia Graecina
*Franciszek Pieczka – Saint Peter
*Krzysztof Majchrzak – Tigellinus
*Rafał Kubacki – Ursus

== See also ==
*Cinema of Poland
*List of submissions to the 74th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 