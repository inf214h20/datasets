Back in the Day (2005 film)
{{Infobox Film
| name           = Back in the Day
| image          = Back in the Day 2005.jpg
| caption        = 
| director       = James Hunter
| producer       = Stephen Baldwin Donald A. Barton
| writer         = James Hunter Michael Raffanello
| starring       = Ja Rule Ving Rhames
| music          = Robert Folk
| cinematography = Donald M. Morgan
| editing        = Chris Holmes
| distributor    = First Look Home Entertainment
| released       = May 24, 2005
| runtime        = 103 min.
| country        = U.S.A. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Back in the Day is a 2005 Crime drama starring Ja Rule and Ving Rhames and directed by James Hunter. The film was introduced on May 15, 2004 at the Cannes Film Festival and was released a year later in the U.S. on May 24, 2005.

==Plot==
Reggie Cooper (Ja Rule) is a young man who lives with his father (Giancarlo Esposito) in order to avoid the violent gang activity that almost claimed his life when he was a teenager. However, when his recently paroled mentor, J-Bone (Ving Rhames) reconnects with Reggie, and when his father is murdered, Reggie slips back into a life of crime. Reggie murders a local preacher (Joe Morton), whose daughter (Tatyana Ali) later develops a relationship with him.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Ja Rule || Reggie Cooper 
|-
| Ving Rhames || J-Bone 
|-
| Tatyana Ali || Alicia Packer
|-
| Giancarlo Esposito || Benson Cooper
|-
| Joe Morton || Rev. James Packer
|-
| Pam Grier || Mrs. Cooper
|-
| Frank Langella || Lt. Bill Hudson
|-
| Lahmard Tate || Jamal
|-
| Tia Carrere || Loot
|-
| Al Sapienza || Det. Kline
|-
| Davetta Sherwood || Tasha
|}

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 