Misunderstood (1966 film)
 
{{Infobox film
| name           = Misunderstood
| image          = Incompreso.jpg
| caption        = Film poster
| director       = Luigi Comencini
| producer       = Angelo Rizzoli
| writer         = Leonardo Benvenuti Piero De Bernardi Lucia Drudi Demby Giuseppe Mangione Florence Montgomery
| starring       = Anthony Quayle
| music          = Fiorenzo Carpi
| cinematography = Armando Nannuzzi
| editing        = Nino Baragli
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Misunderstood ( ) is a 1966 Italian drama film directed by Luigi Comencini. It was entered into the 1967 Cannes Film Festival.   

==Plot==
Duncombe is the UK Consul General in Florence, Italy.  He becomes a widower when his two sons, Andrew and Miles, are still young kids.  Andrew, the eldest, apparently reacts with adult maturity to the loss of his mother, looking after little Miles, an attempt to find a way out of such premature heart-crushing loss.  Miles constantly blames Andrew for his mischievous behavior but his brother valiantly takes said blame as his personality is that of a grown up, or at least that is what he tries to be.  The father, given his mandate, is often absent, both physically and emotionally, especially toward Andrew.  It will be at the end that Duncombe will acknowledge his mistakes when finding himself at a fathers point of no return.

==Cast== Sir Anthony Quayle as John Duncombe
* Stefano Colagrande as Andrew
* Simone Giannozzi as Miles John Sharp as Uncle William
* Adriana Facchetti
* Anna Maria Nardini
* Silla Bettini
* Rino Benini
* Giorgia Moll
* Graziella Granata

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 