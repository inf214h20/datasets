The Final Girls
{{Infobox film
| name           = The Final Girls
| image          = 
| alt            = 
| caption        = 
| director       = Todd Strauss-Schulson
| producer       = {{Plainlist|
*Michael London
*Janice Williams}}
| writer         = {{Plainlist|
*M.A. Fortin
*Joshua John Miller}}
| starring       = {{Plainlist|
*Taissa Farmiga
*Malin Åkerman
*Alexander Ludwig
*Nina Dobrev
*Thomas Middleditch
*Adam DeVine
*Alia Shawkat
*Chloe Bridges}}
| music          = Gregory James Jenkins
| cinematography = Elie Smolkin
| editing        = Debbie Berman
| studio         = {{Plainlist|
* Groundswell Productions
* Studio Solutions}}
| distributor    = Sony Pictures Worldwide Acquisitions
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Final Girls is a 2015 American horror comedy film, directed by Todd Strauss-Schulson and written by M.A. Fortin and Joshua John Miller. It stars Taissa Farmiga and Malin Åkerman in leading roles, with supporting performances from Alexander Ludwig, Thomas Middleditch, Alia Shawkat, Nina Dobrev, Chloe Bridges, and Adam DeVine.
 Sony Pictures in October 2015. 

==Plot==
Max (Farmiga), a high school senior, is mysteriously transported with her friends into a 1980s horror film that starred Maxs mother (Åkerman), a celebrated scream queen. Trapped inside the movie, Max finds herself reunited with her mom, who she lost in real life. Together with Maxs friends, they must fend off the camp counselors raging hormones, battle a deranged machete-wielding killer, and find a way to escape the movie and make it back home.   

==Cast==
*Taissa Farmiga as Max Cartwright
*Malin Åkerman as Amanda Cartwright
*Alexander Ludwig as Chris
*Nina Dobrev as Vicki
*Adam DeVine as Kurt
*Thomas Middleditch as Duncan
*Chloe Bridges as Paula
*Alia Shawkat as Gertie
*Angela Trimbur as Tina
*Tory N. Thompson as Blake
*Dan B. Norris as Billy Murphy

==Production==

===Pre-production===
 
In February 2014, it was reported that Sony Pictures Worldwide Acquisitions had bought the rights to the film, with Michael London and his company Groundswell Productions producing the feature. The film was originally picked up by New Line Cinema but was shopped elsewhere when production did not get off the ground.  The screenplay was written by Joshua John Miller and M.A. Fortin, and was directed by Todd Strauss-Schulson.  Miller and Fortin also served as executive producers, alongside Darren M. Demetre. 

It was announced on February 27, 2014, that Malin Åkerman and Taissa Farmiga had been cast in the two main roles for the film, playing mother and daughter, respectively.      On April 10, 2014, it was revealed that Thomas Middleditch, Nina Dobrev, Adam DeVine and Alexander Ludwig had joined the cast in supporting roles.  Middleditch stars as Duncan, the "film geek"; Dobrev plays Vicki, the "alpha ex-best friend"; DeVine stars as Kurt, the movie-within-a-movies "teen seducer"; and Ludwig portrays Chris, the love interest to Farmigas character.  Alia Shawkat and Chloe Bridges also joined the cast of the film, in the supporting roles of Gertie and Paula, respectively.

===Filming=== Baton Rouge and St. Francisville, Louisiana.    Production for the film began on April 22, 2014.   On April 23, 2014, a photo from the set was revealed by one of the cast.   Over 200 extras were sought out for large scenes.  Principal photography for the film concluded on May 25, 2014.  In late October 2014, some of the cast and crew returned for Pick-up (filmmaking)|pick-ups and scene re-shoots following a test screening earlier that month.   

===Post-production=== Film composer Gregory James Jenkins was hired to score the music for the film, having previously composed the music for two of Strauss-Schulsons short films, as well as his 2011 feature A Very Harold & Kumar 3D Christmas.  On December 22, 2014, Strauss-Schulson stated that post-production had been completed for the film. 

==Release== The Paramount Theatre.    It screened at the Stanley Film Festival as the closing night film on May 2, 2015.    The Final Girls will have a gala premiere at the Los Angeles Film Festival on June 16, 2015.    At the Stanley Film Festival, it was announced that the film would be released to United States theaters by Sony Pictures Worldwide Acquisitions in October 2015.   

==Reception==

===Critical response===
The Final Girls received mostly positive reviews from film critics. Eric Eisenberg of Cinema Blend wrote: "The Final Girls boils down to being a strange cross between   gave a positive review, writing: "Though not quite as inspired or consistent as the similarly self-mocking likes of The Cabin in the Woods, Tucker & Dale vs. Evil or the first two Scream (film series)|Scream pics, this is good fun that should delight genre fans. Directing M.A. Fortin and Joshua John Millers clever screenplay, Todd Strauss-Schulson delivers an accessible in-joke that should sell nicely to various territories in all formats." 

===Accolades===
{|class="wikitable"
! Year
! Award
! Category
! Nominee(s)
! Result
! Ref
|-
| 2015
| Stanley Film Festival
| Audience Award for Feature Film
| The Final Girls (Todd Strauss-Schulson)
|  
|  
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 