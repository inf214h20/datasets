The Moon Riders (serial)
 
{{Infobox film
| name           = The Moon Riders
| image          = The Moon Riders 1920 lobby card.jpg
| caption        = Lobby card for the series.
| director       = B. Reeves Eason Theodore Wharton
| producer       =  Albert Russell Theodore Wharton
| story          = Karl R. Coolidge George Hively William Pigott Albert Russell  Theodore Wharton
| starring       = Art Acord Mildred Moore
| cinematography = William M. Edmond Alfred H. Lathem Howard Oswald
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 360 minutes (18 episodes)
| country        = United States  Silent English intertitles
| budget         = 
}}
 Western film serial directed by B. Reeves Eason and Theodore Wharton. The serial is considered lost film|lost.   

==Cast==
* Art Acord as Buck Ravelle, a Ranger
* Mildred Moore as Anna Baldwin, Arizonas Daughter Charles Newton as Arizona Baldwin George Field as Egbert, Leader of Moon Riders
* Beatrice Dominguez as Rosa, Housekeepers Daughter
* Tote Du Crow as Warpee, Indian Chief
* Albert MacQuarrie as Gant, Crooked Attorney

==Chapter titles==
# Over the Precipice 
# The Masked Marauders
# The Red Rage of Jealousy
# Vultures of the Hills
# The Death Trap
# Caves of Mystery
# The Menacing Monster
# The Moon Riders Bride
# Deaths Door
# The Pit of Fire
# The House of Doom
# Unmasked
# His Hour of Torture
# The Flaming Peril
# Rushing Waters
# Clearing Skies

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 