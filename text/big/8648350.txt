The Battle of Britain
{{Infobox Film
| name = Why We Fight: The Battle of Britain
| image =File:Why We Fight.4.Battle of Britain.webm
| caption =Film
| director = Frank Capra Anthony Veiller
| producer = Office of War Information Julius Epstein; Philip Epstein
| narrator = Walter Huston
| starring =
| music =
| cinematography = Robert Flaherty
| editing = William Hornbeck
| distributor = War Activities Committee of the Motion Pictures Industry
| released = 1943
| runtime = 54 minutes
| country = United States
| language = English
| budget =
}}

:For the 1969 film, see Battle of Britain (film). For the World War II battle, see Battle of Britain.

The Battle of Britain was the fourth of Frank Capras Why We Fight series of seven propaganda films, which made the case for fighting and winning the Second World War. It was released in 1943 and concentrated on the German bombardment of the United Kingdom in anticipation of Operation Sea Lion, the planned Nazi invasion of Great Britain.

==Plot==
The narrator describes the fall of France, leaving Britain almost defenceless. British forces are vastly outnumbered, but the British people are calm. The narrator explains that this is because in a democracy the people as a whole are involved in the decision to fight. operation Sealion|Hitlers masterplan to subjugate Britain is described. Hitler begins by attacking convoys and ports, but fails to destroy them. The RAF are outnumbered "6 - 8 - 10 to one", but knock out far more planes than the Germans do. Bailed out British pilots are also able to return to the air, but German pilots are lost. Unlike the Dutch and Polish airforce Britain does not "make the mistake of bunching its planes on the runways".
 a massive attack on September 15, to which the British respond with "everything they had". In the battle the Germans suffer severe losses.
 destroying Coventry. never in the field of human conflict has so much been owed by so many to so few".

== Allegations of anti-Polish bias == Soviet annexation of Polish territories following Soviet invasion of Poland), repeats the false Nazi propaganda claims that the Polish Air Force was destroyed on the ground (contrasting it with the correct fact that the RAF was not destroyed), and ignores the significant Polish participation in the Battle of Britain. Participation from Polish pilots from No. 303 Polish Fighter Squadron and other units was widely publicized in Britain at the time this propaganda piece was filmed. 

==See also==
* Propaganda in the United States
* List of Allied propaganda films of World War II

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 