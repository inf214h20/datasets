Woh Lamhe
 
{{Infobox film
| name           = Woh Lamhe
| image          = Wohlahme.jpg
| caption        =
| director       = Mohit Suri
| producer       = Mahesh Bhatt    Mukesh Bhatt
| writer         = Mahesh Bhatt (Story)
| narrator       =
| starring       = Kangna Ranaut Shiney Ahuja
| music          = Pritam  Roop Kumar Rathod Jawad Ahmed Peter Pan Glenn John Bobby Singh
| editing        =
| distributor    =
| released       = 29 September 2006
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          = Rs. 16.79 crores  
| preceded by    =
| followed by    =
}}

Woh Lamhe...( ) is a 2006 film starring  Kangna Ranaut andShiney Ahuja and directed by Mohit Suri. The film is supposedly based on Parveen Babis life, her battle with schizophrenia and her relationship with Mahesh Bhatt to whom she was a lover as well as a mentor in his struggling days.

Bhatt said that it is his tribute to the actress and time he spent with her, hence the name Woh Lamhe ("Those Moments"). Parveen Babis character is played by Kangna Ranaut who is named Sana Azim in the film to avoid direct reference to the actress. Woh Lamhe was  critically acclaimed for its screenplay, direction, and a noteworthy performance by Kangna.

The film was an average commercial success, recovering most of its cost from DVD and satellite television circuit.

== Cast ==
{| class="wikitable"
|-
! Actor/Actress !! Role
|- Kangana Ranaut || Sana Azim
|- Shiney Ahuja || Aditya Garewal
|- Masumi Makhija || Rani
|- Shaad Randhawa || Nikhil Rai
|-
|-Anita Wahi || Sanas Mother
|}

== Synopsis ==
In the glitzy entertainment capital of Mumbai as dusk descends, actress Sana Azim (Kangana Ranaut) slits her wrists in a hotel room, in an attempt to kill herself. When this news reaches film-maker Aditya Garewal (Shiney Ahuja), he is devastated. Aditya has been searching for Sana, who was intensely involved with him and who had mysteriously disappeared from his life without any explanation, three years ago, only to surface now in what could be the last moments of her life.

As Aditya waits outside the ICU, praying to be reunited with her, he is hurled back into the perfumed days and champagne nights of his memory, when Sana played the role of a lover and mentor to a struggling Aditya.

Everything was perfect, except for an enemy which lurked in the shadows, waiting to destroy their love. When Aditya realizes that the only way he can save Sana from total devastation is to take her away from Bollywood and the vested interests that threaten to destroy her completely, he runs away with Sana putting his career on the line. Those moments lived in the sanctuary of their love are like an oasis in the desert.

Until one day, suddenly, she disappears, leaving him with unanswered questions.

Aditya tries his best to save Sana from her mental illness but fails.

== Music ==
The soundtrack was composed by Jawad Ahmed , Pritam , Roop Kumar Rathod, Glenn John & Peter Pan. Lyrics were penned by Sayeed Quadri. The music of the film was badly plagiarised by Pritam by copying the tunes from various sources however later the credits were given to the original artists some of the tunes were recreated in terms of musical arrangements. Singers like James, KK (singer)|KK, Shreya Ghoshal, Jawad Ahmed, Kunal Ganjawala, Glenn John lent their voices for the album. Songs like Kya Mujhe Pyar Hai & Bin Tere were popular among the masses while other songs also managed to get a good response. Planet Bollywood gave a rating of 8/10 to the soundtrack.

{| class="wikitable"
|-
! Track No !! Song !! Singer !! Composer
|- KK || Pritam & Peter Pan (Original)
|-
| 2 || Chal Chale || James || Pritam
|-
| 3 || Tu Jo Nahin I || Glenn John || Pritam
|-
| 4 || So Jaoon Main (Female) || Shreya Ghoshal || Roop Kumar Rathod
|-
| 5 || Tu Jo Nahin II || Glenn John || Pritam
|-
| 6 || So Jaoon Main (Male) || Kunal Ganjawala || Roop Kumar Rathod
|-
| 7 || Bin Tere || Jawad Ahmed || Jawad Ahmed (Tune Recreated by Pritam)
|-
| 8 || Kya Mujhe Pyar Hai (Remix) || KK || Pritam & Peter Pan (Original)
|}

== References ==
 

== External links ==
*  
* http://www.dawn.com/weekly/images/archive/061231/images2.htm
* http://www.apunkachoice.com/scoop/bollywood/20060929-5.html
* http://www.bollywoodhungama.com/movies/review/12755/index.html

 
 

 
 
 
 
 
 