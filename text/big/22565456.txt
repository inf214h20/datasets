Spring Fever (2009 film)
 
{{Infobox film
| name           = Spring Fever
| image          = Spring Fever (2009 film).jpg
| caption        = 
| director       = Lou Ye
| producer       = Nai An Sylvain Bursztejn Lou Ye
| writer         = Mei Feng Wu Wei Jiang Jiaqi
| music          = Peyman Yazdanian
| cinematography = Zeng Jian
| editing        = Robin Weng Zeng Jian Florence Bresson
| studio         = Dream Factory HK Rosem Films
| distributor    = Le Pacte
| released       =  
| runtime        = 116 minutes
| country        = Hong Kong France China
| language       = Mandarin Chinese
| budget         = 
}} Summer Palace.     Filmed in Nanjing, the film was described to be about a young threesome overcome with erotic longings.  

By the time of the films premiere at the Cannes Festival on 13 May 2009, it was known that Lou had circumvented the five-year ban imposed upon him after Summer Palace by having Spring Fever registered as a Hong Kong/French co-production (filmmaking)|co-production.   

== Plot ==
China, 2007. Spring. The protagonist is a private investigator hired to spy on a man who is having an affair with another man. However, the investigator becomes entangled in a love triangle with the boyfriend of the man hes investigating and his own girlfriend.

==Cast==
*Qin Hao ( ) as Jiang Cheng ( )	
*Chen Sicheng ( ) as Luo Haitao ( )
*Tan Zhuo ( ) as Li Jing ( ) Wu Wei ( ) as Wang Ping ( )
*Jiang Jiaqi ( ) as Lin Xue ( )

== Release == editing the film in Paris.   Like Summer Palace, Spring Fever was to be screened without government approval. 

== Reception == Suzhou River.  
 Best Screenplay at the 2009 Cannes Film Festival for its writer Mei Feng. 

== References ==
 

== External links ==
*    
*  
*  
*   at Cannes Film Festival
*   at the Chinese Movie Database
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 