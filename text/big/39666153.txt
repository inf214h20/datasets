Jallikattu (film)
{{Infobox film
| name           = Jallikattu
| image          = 
| image_size     =
| caption        = 
| director       = Manivannan
| producer       = Chitra Ramu Chitra Lakshmanan
| writer         = Vietnam Veedu Sundaram  (dialogues) 
| screenplay     = Manivannan
| story          = Vietnam Veedu Sundaram
| starring       =  
| music          = Ilaiyaraaja
| cinematography = A. Sabapathy
| editing        = Chandran
| distributor    = Seetha Lakshmi Art Films
| studio         = Seetha Lakshmi Art Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1987 Tamil Tamil crime Radha in lead roles. The film, produced by Chitra Ramu and Chitra Lakshmanan, had musical score by Ilaiyaraaja and was released on 28 August 1987.   

==Plot==

The story begins with Arjun (Sathyaraj) being arrested for multiple crimes. In the past, Arjun was a happy-go-lucky young man. His brother (Delhi Ganesh), a factorys union leader, clashed with his superiors (M. N. Nambiar, Malaysia Vasudevan and Chitra Lakshmanan) for bonus. In the meantime, Arjun fell in love with Radha (Radha (actress)|Radha). To help the labourer, Arjun cheated the factorys owners as a fake income tax officer, he managed to take all their black money and he gave it to the labourers. Later, Arjuns brother, sister-in-law and his niece were killed. In anger, he tried to kill the culprit and failed. Siva Prasad (Captain Raju), Radhas brother, advised him to surrender but, as an innocent, he refused. However, Siva Prasad arrested him in surprise. Arjun was tortured and his head was shaved. Therefore, a bald Arjun comes to the court. Despite everything was against Arjun, the judge Ram Prakash (Sivaji Ganesan) feels that he is innocent. So Ram Prakash sentences that Arjun will be under house arrest in his isolated island. Along with Ram Prakash how Arjun kills the culprits forms the rest of the story.

==Cast==
*Sivaji Ganesan as Ram Prakash
*Sathyaraj as Arjun (Half Boiled Oliver/Chinnappadass/Kunguma Pottu Gounder/Rajaratnam) Radha as Radha
*M. N. Nambiar as Dheenadalayan Janagaraj as Janagaraj
*Venniradai Moorthy as Moorthy
*Malaysia Vasudevan as Numerology
*Captain Raju as Siva Prasad
*Chitra Lakshmanan as Dharmaraj
*A. R. S as Srinivasan
*Rajasekhar
*Charle Thyagu
*Veeraraghavan
*Pramila
*John Amirtharaj
*Kutty Padmini as Kutty Amma
*Delhi Ganesh in a guest appearance
*Kokila as Karpagam (guest appearance)
*Prathap K. Pothan in a guest appearance
*Ramya Krishnan as TV Anchor (Interviewing Dharmaraj)in a guest appearance

==Soundtrack==

{{Infobox album |  
| Name        = Jallikattu
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1987
| Recorded    = 1987 Feature film soundtrack |
| Length      = 22:35
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1987, features 5 tracks with lyrics written by Gangai Amaran.    The track, Kathi chandai podaamalae is reused from the movie Kadalora Kavithaigal also composed by Ilaiyaraaja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Mano || 4:31
|- 2 || Kaadhal Kizhiye || Mano, S. Janaki || 4:53
|- 3 || Kathi Chandai Poadaamale || K. S. Chithra || 4:22
|- 4 || Yeriyil Oru || Malaysia Vasudevan|| 4:25
|- 5 || Yethanaiyo || Mano, S. Janaki || 4:24
|}

==References==
 

 
 
 
 
 
 