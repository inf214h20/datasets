Bleeke Bet (1923 film)
 
{{Infobox film
| name           = Bleeke Bet
| image          = 
| image_size     = 
| caption        = 
| director       = Alex Benno
| producer       = 
| writer         = Alex Benno Herman Bouber
| narrator       = 
| starring       = 
| music          =   
| cinematography = H. W. Metman H.W. Metman
| editing        = 
| distributor    = 
| released       = 24 August 1923
| runtime        = 
| country        = Netherlands
| language       = Silent
| budget         = 
}} 1923 Netherlands|Dutch silent film directed by Alex Benno.

==Cast==
* Alida van Gijtenbeek - Bleeke Bet
* Jan van Dommelen - Tinus, haar man
* Beppie De Vries - Jans
* Riek Kloppenburg - Trui
* Piet Urban - Van Zanten
* Heintje Davids
* Herman Bouber - Sally Matteman
* Harry Boda - Ko Monjé
* Johan Elsensohn - Lucas
* Henriette Blazer - Meid (as Henriëtte Blazer)
* Gerardus van Weerdenburg - Hannes (as Kees van Dam)
* Kees Pruis - Eerste student
* Frans Bogaert - Tweede student
* John Timrott
* Emile Timrott

== External links ==
*  

 
 
 
 
 


 