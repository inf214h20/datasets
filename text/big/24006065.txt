Cheer Boys Cheer
{{Infobox film
| name = Cheer Boys Cheer
| image =
| image size =
| border =
| alt =
| caption =
| director = Walter Forde
| producer = Michael Balcon
| writer = Roger MacDougall Allan MacKinnon
| screenplay =
| story = Donald Bull Ian Dalrymple
| based on =  
| narrator =
| starring = Nova Pilbeam Edmund Gwenn Jimmy ODea Graham Moffatt Moore Marriott
| music = Van Phillips   Alfred Ralston
| cinematography = Ronald Neame
| editing = Ray Pitt
| studio = Ealing Studios Associated British
| released =  
| runtime = 84 minutes
| country = United Kingdom English
| budget =
| gross =
}}

Cheer Boys Cheer is a 1939 British comedy film directed by Walter Forde and starring Nova Pilbeam, Edmund Gwenn, Jimmy ODea, Graham Moffatt and Moore Marriott.

==Production==
The film was made by Ealing Studios, roughly a year after Michael Balcon had taken over from Basil Dean as head of production. It was released shortly before the outbreak of the Second World War, the last Ealing film to be released in peacetime.

==Synopsis==
The film depicts the rivalry between two firms of brewers. Ironsides, a modern company led by the ruthless Edward Ironside and his son John, seeks territorial expansion to crush its rivals and seize control of their business. They are faced by the smaller, more gently run Greenleaf brewery which is about to celebrate its 150th anniversary.

==Reception==
The films representation of the differing management styles of the Ironside and Greenleaf companies has traditionally been seen as an analogy for Nazi Germany and the United Kingdom in the lead-up to the outbreak of war. 
 Hue and Cry in 1947.

==Cast==
* Nova Pilbeam as Margaret Greenleaf
* Edmund Gwenn as Edward Ironside
* Jimmy ODea as Matt Boyle
* Moore Marriott as Geordie
* Graham Moffatt as Albert Baldwin
* C.V. France as Tom Greenleaf
* Peter Coke as John Ironside
* Alexander Knox as Saunders
* Ivor Barnard as Naseby
* Walter Forde as Pianist at Wedding James Knight as Ironsides Chauffeur
* Hay Plumb as Greenleaf Employee
* Charles Rolfe as Ironside Thug
* Harry Terry as Brewery Worker
* Jean Webster-Brough as Maggie

==References==
 

==Bibliography==
* Perry, George. Forever Ealing: A Celebration of the Great British Film Studio. Pavilion, 1981.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 