The Screaming Shadow
 
{{Infobox film
| name           = The Screaming Shadow
| image          = Ben Wilson in The Screaming Shadow by Ben F. Wilson and Duke Worne Film Daily 1920.png
| caption        = Poster for film serial
| director       = Ben F. Wilson Duke Worne
| producer       = 
| writer         = J. Grubb Alexander (scenario) Harvey Gates (scenario)
| starring       = Ben F. Wilson Neva Gerber
| cinematography = 
| editing        = 
| distributor    = Hallmark Pictures Corporation
| released       =  
| runtime        = 15 episodes 
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 action film serial directed by Ben F. Wilson and Duke Worne. The film is considered to be lost film|lost.   

==Cast==
* Ben F. Wilson as John Rand (as Ben Wilson)
* Neva Gerber as Mary Landers
* Frances Terry as Nadia
* Howard Crampton as J.W. Russell
* Joseph W. Girard as Baron Pulska William Dyer as Jake Williams
* William A. Carroll as Harry Malone Fred Gamble as Fred Wilson
* Pansy Porter as Young maiden
* Claire Mille as Young maiden
* Joseph Manning as The butler

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 