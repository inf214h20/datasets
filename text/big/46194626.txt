Dil-E-Nadaan (1953 film)
{{Infobox film
| name           = Dil-E-Nadaan 
| image          = Dil-E-Nadaan_1953.jpg
| image size     =
| caption        = 
| director       = A. R. Kardar
| producer       = A. R. Kardar
| writer         = Kardar Productions story department
| narrator       =
| starring       = Talat Mehmood Shyama Peace Kanwal Diwan Sharar
| music          = 
| cinematography = Dwarka Divecha 
| editing        = M. S. Hajee 
| studio         = Kardar Productions Ltd.
| distributor    =
| released       =  
| runtime        = 142 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}} 1953 Hindi Ghulam Mohammed, with lyrics written by Shakeel Badayuni.    It was Talat Mehmoods debut acting role, and he was introduced as the "Singing Star Talat Mehmood".    He co-starred with the new actress Peace Kanwal, who was introduced in this film, and with Shyama. The film failed at the box-office in spite of several popular songs.    The other actors were Diwan Sharar, S. N. Bannerji, Ramesh and Master Romi.   

The story was a love triangle, with two sisters played by Shyama and Peace Kanwal, in love with the same man (Talat Mehmood).

==plot==
Kamini (Peace Kanwal) and Asha (Shyama) are two sisters living with their wealthy father, Seth Hirachand (Diwan Sharar). Mohan (Talat Mehmood), is striving to become a musician, but is hindered by his father who is a petition-writer. Mohan leaves home and meets up with Kamini and her father. Seth Hirachand helps Mohan to make a name for himself as a music director. Kamini and Mohan fall in love, but Asha lets Kamini know that she loves Mohan. Having spoilt Asha, as an older sister Kamini gets Asha and Mohan married. However things dont work out between the two. Asha is disturbed by Mohans music and finds it boring. She finds out about Kamini and Mohans affair and consumed with rage a pregnant Asha runs after Mohan but trips down the stairs. Badly injured, she gives birth and dies handing the baby to Kamini. After a few years, Kamini and Mohan are brought together by Mohans son, who insists on calling Kamini, mother.

==Cast==
* Shyama
* Peace Kanwal
* Talat Mehmood
* Diwan Sharar
* Shyam Kumar 
* S. N. Bannerji
* Master Romi
* A.L. Ramesh
* Agha Mehraj
* Phiroza
* Miss Maya Dass

==Soundtrack==
Ghulam Mohammed composed the music for Dil-E-Nadaan and gave a "standout score" for the film. Bharatan states that Dil-E-Nadan marked a downward trend for him as far as the box-office success was concerned.   

Ghulam Mohammeds compositions were well appreciated by the public. One of the notable songs from the film was Talat Mehmoods expressive "Zindagi Dene Wale Sun", the start of which employs western stringed instruments before the tune blends into Raga Bhoop.    The second song was  Mehmoods "Mohabbat Ki Dhun Beqararon Se Poochho" with co-singers Sudha Malhotra and Jagjit Kaur.    The other popular numbers were Jagjit Kaurs "Khamosh Zindagi Ko Ek Afsana Mil Gaya" and Talats "Jo Khushi Se Chot Khaye".

The lyricist was Shakeel Badayuni and the singers were Talat Mehmood, Asha Bhosle, Shamshad Begum and Sudha Malhotra.

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Zindagi Denewale Sun, Teri Duniya Se Dil Bhar Gaya 
| Talat Mehmood
|-
| 2
| Jo Khushi Se Chot Khaye, Woh Jigar Kaha Se Laaun 
| Talat Mehmood
|-
| 3
| Ye Raat Suhani Raat Nahin Ae Chand Sitaron So Jao 
| Talat Mehmood
|-
| 4
| Muhabbat Ki Dhun Bekararo Se Poochho 
| Talat Mahmood, Sudha Malhotra, Jagjit Kaur
|-
| 5
| Teri Khaatir Sitam Dil Par 
| Talat Mahmood
|-
| 6
| Chanda Gaye Ragni Cham Cham Barse 
| Jagjit Kaur
|-
| 7
| Khamosh Zindagi Ko Ek Afsana Mil Gaya 
| Jagjit Kaur
|-
| 8
| Na Wo Humare Na Dil Humara 
| Sudha Malhotra
|-
| 9
| Lijo Babul Hamara Salaam Re 
| Asha Bhosle
|}

==References==
 

==External links==
*  
*   songs

 

 
 
 
 