The Creeping Terror
 
{{Infobox film
| name = The Creeping Terror
| image = Creepingterror.jpg
| caption = Title screen
| director = Vic Savage  (as A.J. Nelson) 
| producer = Vic Savage  (as A.J. Nelson) 
| writer = Robert Silliphant
| narrator = Larry Burrell
| starring = Vic Savage Shannon ONeil William Thourlby John Caresio
| music = Frederick Kopp
| cinematography = Andrew Janczak
| editing = Vic Savage  (as A.J. Nelson) 
| distributor = Crown International Television
| released = 1964
| runtime = 75 min USA
| English
}}
 worst films riffing on the satirical television series Mystery Science Theater 3000.

== Plot == alien spacecraft omnivorous monster emerges from the side of an impacted spaceship. A second one, still tethered inside, kills a forest ranger and the sheriff (Byrd Holland) when they independently enter the craft to investigate.

Martin, now temporary sheriff, joins his wife Brett (Shannon ONeil); Dr. Bradford (William Thourlby, the original Marlboro Man), a renowned scientist; and Col. James Caldwell, a military commander and his men to fight the creature. Meanwhile the monster stalks the countryside, devouring a girl in a bikini, picnickers at a "hootenanny", Grandpa Brown (Jack King) and his grandson while fishing, a housewife hanging the laundry, the patrons at a community dance hall, and couples in their cars at lovers lane.

The protagonists ultimately deduce that the monsters are mindless biological-sample eaters. The bio-analysis data is microwaved back to the probes home planet through the spaceship.

Caldwell decides that the creatures must be killed, despite Bradfords objections.  He orders his men to fire at the creature, which they do while standing close to one another as it moves towards them.  Their gunfire proves ineffective, and all of the troops are devoured.  Paradoxically, Caldwell decides a moment later to throw a grenade, and the creature dies instantly.

At the end of the film, both creatures are destroyed, but not before the signal is sent. The dying Bradford suggests that this bodes ill for the human race, but observes that since the galaxy to which the transmission was aimed is a million light years away, the threat may not manifest for millennia.

==Cast==

* Vic Savage as Martin Gordon
* Shannon ONeil as Brett Gordon
* William Thourlby as Dr. Bradford
* John Caresio as Col. James Caldwell
* Brendon Boone as Barney the Deputy
* Byrd Holland as Sheriff
* Jack King as Grandpa Brown
* Pierre Kopp as Bobby

== Production ==
The Creeping Terror was directed, produced, and edited by Vic Savage under the alias A.J. Nelson. Although Robert Silliphant is the credited writer, the original story was written by his younger brother, Allan Silliphant, who went on to produce, write and direct the 3-D film|3-D adult feature film The Stewardesses (1969) (under the name Al Silliman Jr.), the only micro-budget film of the 1960s or 1970s to become the #1 film on the weekly Variety (magazine)|Variety box-office chart (it finally grossed over $140,000,000 in 2011 United States dollar|U.S. dollars).
 Naked City Route 66. In the The Poseidon The Towering Inferno, among about 40 other films. Allan Silliphant was therefore famous by association, a fact used by Savage to draw in potential investors. The younger Silliphant brother had no idea that the family name was being used to influence potential investors. Savage reportedly offered many of the investors a small part in the film for a few hundred dollars each, in exchange for a part of the profits. However, just before the films release, Savage was sued repeatedly, even possibly facing indictment on charges of fraud, and vanished. He was apparently never heard from again in the context of film production, and reportedly died of liver failure in 1975, aged 41.

Savage paid Allan Silliphant $1500, according to comments by Silliphant when interviewed for "Creep", although far less was implied by a book about "bad movies."  Forthwith, the 22-year-old Silliphant returned in three days with the original nine-page film treatment that he had "made up" face-to-face with Savage, based only on a vague earlier story idea. Later in the production there was conflict between writer and director, with Silliphant growing frustrated that Savage did not seem to share his vision that the story was "supposed" to be over the top. Furthermore, instead of shooting at scenic Lake Tahoe as Silliphant had intended, a muddy pond at Spahn Ranch had to do. The assistant director was stuntman Randy Starr, who later achieved notoriety by providing Charles Manson with the gun used in the Sharon Tate murders.  Silliphant saw that the direction the film was taking would harm his family, especially the reputation of half-brother Stirling Silliphant, rather than enhance it, so he bowed out after the studio scenes were done. The production became a weekend affair for several more months, with Savage raising the money by selling small parts to star-struck plumbers, etc. One story says Savage checked into a motel with a silent picture-only Moviola to do a quick assembly of the film.
 35mm mag film stock|stock. Having insufficient money to pay for basic sound transfers, he finally hired a local radio news reader to narrate the entire movie in post-production.

== Sequels ==
In 1980 and 1981, a group of students at the University of Michigan produced two sequels to The Creeping Terror entitled Return of the Creeping Terror and The Creeping Terror Strikes Back . Several shots include crewmembers holding the lights and microphones, and the Terrors victims can be seen rolling offscreen after the carpet smothers them. 
== See also ==
* List of films considered the worst

== Notes ==
 

== External links ==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 