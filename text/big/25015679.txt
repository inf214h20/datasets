La Cage aux Folles II
 
{{Infobox film
| name = La Cage aux Folles II
| image	= La Cage aux Folles II FilmPoster.jpeg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Édouard Molinaro
| producer = Marcello Danon
| writer = Jean Poiret Francis Veber Marcello Danon
| story = Jean Poiret Francis Veber  
| based on =  
| starring = Michel Serrault Ugo Tognazzi Marcel Bozzuffi 
| music = Ennio Morricone
| cinematography = Armando Nannuzzi
| editing = Monique Isnardon Robert Isnardon
| studio = Da Ma Produzione Les Productions Artistes Associés
| distributor = United Artists
| released =  
| runtime = 101 minutes
| country = France Italy
| language = French
| budget = 
| gross = $10,989,331
}} La Cage aux Folles. It is directed by Édouard Molinaro and stars Michel Serrault as Albin, (stage name ZaZa), the female impersonator star of a gay night-club revue, and Ugo Tognazzi as Renato, his partner of over twenty years.

==Plot==
A spy plants a capsule of microfilm on Albin and from then on spies and government agents pursue him. Albin and Renato travel to Italy to hide at Renatos mothers farm. At each point along the way we see the straight worlds reaction to Albin.

==Cast==
* Michel Serrault as Albin Mougeotte/ZaZa Napoli
* Ugo Tognazzi as Renato Baldi
* Marcel Bozzuffi as Broca, chief of the government agents
* Michel Galabru as Simon Charrier
* Paola Borboni as Mrs. Baldi, Renatos mother
* Benny Luke as Jacob, Renato and Albins housekeeper
* Giovanni Vettorazzo as Milan
* Glauco Onorato as Luigi
* Roberto Bisacco as Ralph
* Gianrico Tondinelli as Walter
* Giorgio Cerioni as Gunther
* Nazzareno Natale as Demis
* Antonio Francioni as Michaux
* Stelio Candelli as Hans
* Mark Bodin as Caramel, Albins would-be replacement
* Tom Felleghy as Andrew Manderstam

==Critical response==
The film was favorably reviewed by the critic Pauline Kael in The New Yorker: " La Cage aux Folles II has nothing to do with the art of movies, but it has a great deal to do with the craft and art of acting, and the pleasures of farce. Serrault gives a superb comic performance - his Albin is a wildly fanciful creation. Theres a grandeur about Albins inability to see himself as he is. And maybe its only in this exaggerated form that a movie about the ridiculousness and the tenderness of married love can be widely accepted now. At the end, after Albin has been kidnapped by the spies, Renato, who is nearby with the police, cant think of anything but his beloved Albin. And, suddenly, forgetting the danger, each starts running toward the other, and they meet between the two armed groups like lovers in an opera. One of the policemen watching their embrace is weeping. "Its beautiful," he says."  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
   
 
 
   
 
 
 
 
 
 
 
 
 