Broken Ways
 
{{Infobox film
| name           = Broken Ways
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = F. P. Bayer
| starring       = Henry B. Walthall Blanche Sweet
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States Silent English English intertitles
| budget         =
}}
 Western film directed by  D. W. Griffith, starring Henry B. Walthall and Blanche Sweet. A print of the film survives.   

==Cast==
* Henry B. Walthall as The Road Agent
* Blanche Sweet as The Road Agents Wife Harry Carey as The Sheriff Charles Gorman as HoltasUp Victim
* Frank Opperman as Road Agents Gang Member Joseph McDermott as Road Agents Gang Member
* Gertrude Bambrick as InTelegraph Office/On Street
* William A. Carroll as In Posse Edward Dillon
* Dorothy Gish as In Telegraph Office
* Robert Harron as In Telegraph Office
* Adolph Lestina as In Telegraph Office
* Mae Marsh Walter Miller as In Town
* Alfred Paget as In Posse

==See also==
* List of American films of 1913
* Harry Carey filmography
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 