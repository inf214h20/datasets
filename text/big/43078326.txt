Countess Walewska
{{Infobox film
| name           = Countess Walewska
| image          =
| caption        =
| director       = Aleksander Hertz  
| producer       = 
| writer         = Aleksander Hertz 
| starring       = Stefan Jaracz   Maria Duleba   Bronislaw Oranowski
| music          = 
| cinematography = Czeslaw Jakubowicz
| editing        = 
| studio         = Pathé Frères   Sokól
| distributor    = 
| released       = 5 May 1914
| runtime        = 
| country        = Poland
| awards         =
| language       = Silent   Polish intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
Countess Walewska is a 1914 Polish historical film directed by Aleksander Hertz and starring Stefan Jaracz, Maria Duleba and Bronislaw Oranowski.  The film was made as a co-production with the French company Pathé Frères. It portrays the life of Maria Walewska the Polish-born mistress of the French Emperor Napoleon.

==Cast==
*  Stefan Jaracz as Napoleon Bonaparte
* Maria Duleba as Maria Walewska 
* Bronislaw Oranowski as Duke Józef Poniatowski  

== References ==
 

==Bibliography==
* Liehm, Mira & Liehm, Antonín J. The Most Important Art: Eastern European Film After 1945. University of California Press, 1977. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 