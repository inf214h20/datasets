Running Turtle
{{Infobox film
| name           = Running Turtle
| image          = Running Turtle film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Geobuki dalinda
| mr             = Kŏbuki tallinta}}
| director       = Lee Yeon-woo
| producer       = Lee Choon-yeon Lee Mi-yeong
| writer         = Lee Yeon-woo
| starring       = Kim Yoon-seok Jung Kyung-ho
| music          = Lee Byung-hoon Jang Young-gyu
| cinematography = Jo Yong-gyu
| editing        = Steve M. Choe 
| studio         = 
| distributor    = Showbox
| released       =  
| runtime        = 117 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2009 South Korean film about a countryside detective trying to capture a legendary prison breaker. Released on June 11, 2009, it was directed by Lee Yeon-woo and starred Kim Yoon-seok and Jung Kyung-ho.

== Plot == scratching off lottery tickets in his office, while his wife and children work in a manwha shop. Pil-seong secretly takes his wifes emergency money of   and tells his friend to bet it on a bull name Gomi in a bullfight. Gomi is declared as a winner, and Pil-seongs friends celebrate their victory later that night as they wait for him to arrive.

Meanwhile, an escaped prisoner named Song Gi-tae intervenes in the celebration and steals Pil-seongs money. When Pil-seong finds out what has happened he confronts the criminal, but suffers a humiliating defeat.

Pil-seong reports to his colleagues and his boss that he encountered the infamous Gi-tae, but none of them believed in him. In an attempt to recapture Gi-tae, Pil-seong decides to recruit his friends (who were beaten up by Gi-tae earlier) and found him inside a house with his girlfriend, Kyeong-joo. Formulating a plan, Pil-seong tells his friends to stand by the outside window of where Gi-tae was, while Pil-seong himself sneaks into the house, armed with pepper spray. Gi-tae, however, was aware of Pil-seongs presence and once escaped before the police arrived. Before leaving, Gi-tae takes out his knife, stabs Pil-seongs right hand, and warns him that the next time hell kill him if he tries to capture him again.

Suffering from public humiliation as well as being kicked out of the house by his wife (because of her money), Pil-seong decides to train himself in fighting Gi-tae. When taking taekwondo lessons one day, Pil-seong learns that the upper part of the human rib cage is the vulnerable area of taking his opponents down. He later buys himself his own handgun as his own personal defense weapon.

In the next encounter of Gi-tae, Pil-seong got him surrounded by keeping Kyeong-joo hostage. As he forced Gi-tae to keep moving on, Pil-seongs friends intervened, causing Gi-tae to escape once again. Pil-seongs friends begged him to give up capturing Gi-tae for his own sake, but he refuses.

Meanwhile, one of Gi-taes accomplice, Pyo Jae-seok, was arrested by Pil-seong. He confiscates Jae-seoks handphone and orders him to bring back the money that Gi-tae stole from him. Learning that Gi-tae was hiding somewhere in a fishing village, Pil-seong uses a bullhorn and asks Gi-tae to reveal himself.

Realizing that Pil-seong has the money and Jaeseoks cell phone, Gi-tae calls him and tells him that he is next to the manwha shop where his family works. Holding a gasoline tank, he gives Pil-seong one hour to meet him and bring the money, or else hell destroy the manwha shop with his family inside.
 fair fight. The battle then ensures as both Pil-seong and Gi-tae beat each other up to the pulp.

Finally cornering Pil-seong, Gi-tae asks him for the money and threatens to kill him if he doesnt. Gi-tae finds the money and walks off. However, Pil-seong regained consciousness and eventually defeats his opponent by accurately striking him in the upper rib cage. Pil-seong is barely conscious from the fight, but is satisfied on his efforts of capturing Gi-tae. Pil-seongs colleagues later finds him in the cell sleeping with Gi-tae handcuffed.

Few days later, Pil-seong and his colleagues was awarded for a higher position as officers and led a ceremonial parade to Pil-seongs daughters school. The film ends as Pil-seong and the fellow officers gave a salute.

== Cast ==
* Kim Yoon-seok ... Jo Pil-seong
* Jung Kyung-ho ... Song Gi-tae
* Kyeon Mi-ri ... Detective Jos wife
* Sunwoo Sun ... Kyeong-joo, Gi-taes girlfriend
* Kim Ji-na ... Ok-soon
* Shin Jung-geun ... Yong-bae
* Choi Kwon ... Pyo Jae-seok
* Kim Hye-ji ... Joo-rang
* Ju Jin-mo ... Team leader Yang
* Lee Moo-saeng ... Detective Lee

== Release and box office == topped the box office over its first weekend with a total of 480,293 admissions.  It led the box office for a second consecutive weekend,  and as of August 2 had accumulated a total of 3,051,136 admissions and grossed  .  

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 