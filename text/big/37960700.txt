The Somme (film)
{{Infobox film
| name           = The Somme
| image          =
| caption        =
| director       = M.A. Wetherell
| producer       = E. Gordon Craig
| writer         =  Geoffrey Barkas   Boyd Cable
| starring       = 
| music          =
| cinematography = Sydney Blythe   Freddie Young
| editing        = Geoffrey Barkas
| studio         = New Era Films
| distributor    = New Era Films
| released       = September 1927 
| runtime        = 8,100 feet  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}}
The Somme is a 1927 British documentary film directed by M.A. Wetherell. It re-examined the 1916  Battle of the Somme during the First World War.

==Production==
It was made at Isleworth Studios using a docudrama format. It involved a number of the personnel who had previously worked on a successful series of documentary reconstructions of First World War battles by British Instructional Films released between 1921 and 1927. British Instructional Films had finished their series with The Battles of Coronel and Falkland Islands, and Geoffrey Barkas moved to the newly established New Era films to carry on the cycle. When Barkas fell ill, Wetherell was brought in to take over the project. Although Wetherell received the directors credit, much of the film was made by Barkas and Boyd Cable. 

The following year the company released another docudrama, Q-Ships (film)|Q-Ships.

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 