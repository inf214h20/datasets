Fiddlers Three (1948 film)
{{Infobox Film |
  | name           = Fiddlers Three|
  | image          = FiddlersthreesTITLE.jpg|
  | caption        =  |
  | director       = Jules White | Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Vernon Dent Virginia Hunter Philip Van Zandt Cy Schindell Al Thompson|
  | cinematography = Allen G. Siegler | 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       = May 6, 1948 |
  | runtime        = 17 06" |
  | country        = United States
  | language       = English
}}
Fiddlers Three is the 107th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are fiddlers at the court of Old King Cole (Vernon Dent). They are forbidden by the king to marry their sweethearts until Princess Alicia (Virginia Hunter) weds Prince Gallant III of Rhododendron "when the flowers bloom in the Spring." Evil magician Murgatroyd (Philip Van Zandt) has his own plans to marry Alicia, and promptly abducts her. While the Stooges are trying to apply new horseshoes to their horses, they accidentally fall through the floor where they find the Princess bound and gagged. They ultimately and rescue her by luring the guards out one by one where they knock them out.
 

==Production notes==
Like Squareheads of the Round Table and The Hot Scots, Fiddlers Three was filmed on the existing set of the feature film The Bandit of Sherwood Forest.    

This is the thirteenth of sixteen Stooge shorts with the word "three" in the title.
 remade in 1954 as Musty Musketeers, using ample stock footage. 

==References==
  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 