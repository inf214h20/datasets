Rabbit Romeo
{{Infobox Hollywood cartoon|
| cartoon_name = Rabbit Romeo
| series = Merrie Melodies (Bugs Bunny) 250px
| caption = title card
| director = Robert McKimson
| story_artist = Michael Maltese
| animator = Ted Bonnicksen   George Grandpre
| voice_actor = Mel Blanc   June Foray (uncredited))  Arthur Q. Bryan (uncredited)
| musician = Milt Franklyn 
| producer = Eddie Selzer Warner Bros. Pictures
| release_date = December 14, 1957 (USA)
| color_process = Technicolor
| runtime = 6:54
| movie_language = English
}}

Rabbit Romeo is a Merrie Melodies cartoon starring Bugs Bunny and Elmer Fudd. The film is one of the few pairings of Bugs and Elmer in which Bugs is not hunted throughout the entire picture (despite Elmer using his hunting rifle every time Bugs tries to escape throughout the entire cartoon), and also notable as a cartoon in which Bugs has a romantic encounter.

== Plot == Slobbovian rabbit (named Millicent) until he arrives, and is promised $500 for his efforts. When he opens the box he discovers that Millicent is a huge, unattractive, female rabbit with an Eastern European/Slavic accent. When Elmer shows Millicent her room, she trashes the room and cries uncontrollably on a couch.

Elmer calls a doctor who says that Slobovian rabbits get lonely and need another rabbit to talk to. He goes out to lure a rabbit with a carrot, and catches Bugs Bunny|Bugs.

When Elmer introduces Bugs to Millicent, her demeanor quickly switches from melancholic to amorous; she asks for a "laaarge keess", as she calls it. Most of the rest of the plot deals with Bugs humorous attempts to evade Millicents romantic advances; Bugs is often thwarted by a gun-wielding Elmer. At one point, Bugs declares that they should elope. Bugs takes a rolled up sheet and holds it out the window for Milly to slide down, but lets go of the sheet as she is doing so ("Butterfingers!").

As Millicent pounds on the door, Bugs goes and tells Elmer that Uncle Judd is at the door. Bugs offers a "bathrobe" for Elmer to slip into; as he steps into the hall, the "bathrobe" is revealed to be a bunny costume. When Elmer opens the door, Millicent becomes interested in Elmer instead (thinking hes a rabbit), and chases after him off into the countryside. The cartoon concludes with Bugs at the door,(in a cupid costume) saying: "Aint I the little matchmaker?"

 
{{succession box |
before= Show Biz Bugs | Bugs Bunny Cartoons |
years= 1957 |
after= Hare-Less Wolf|}}
 

 
 
 
 
 
 


 