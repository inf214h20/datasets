House of Wax (2005 film)
{{Infobox film
| name = House of Wax
| image = House Of Wax movie poster.jpg
| image_size = 220px
| alt =
| caption = Theatrical release poster
| director = Jaume Collet-Serra Susan Levin Chad Hayes Carey Hayes
| starring = Chad Michael Murray Elisha Cuthbert Brian Van Holt Paris Hilton Jared Padalecki Jon Abrahams Robert Richard
| music = John Ottman
| cinematography = Stephen F. Windon
| editing = Joel Negron
| studio = Village Roadshow Pictures Dark Castle Entertainment Warner Bros. Pictures
| released =  
| runtime = 113 minutes  
| country = United States Australia
| language = English
| budget = $40 million   
| gross = $70,064,000 
}} the 1953 film of the same name, which was itself a remake of the 1933 film Mystery of the Wax Museum, but the 2005 films plot is completely different from the story told by the two earlier films. It earned mixed reviews, but critics praised Murrays performance.

==Plot==
  football game.

The two arrive at Ambrose, which is practically a ghost town. Unable to find an attendant at the auto mechanics shop, they wander into the church, disrupting a funeral. There they meet a mechanic named Bo (Brian Van Holt), who offers to sell them a fan belt after the funeral. While waiting for the service to end, the two visit the wax museum, which itself is made of wax and is the central feature of the town. Afterward, they follow Bo to his house to find a proper fan belt. Wade follows Bo inside, and is attacked by a mysterious figure in the dark. Outside, Carly discovers that Bos car is the same one that visited them the night before, after noticing the broken headlight. She is attacked by Bo and runs to the church, only to find that the ongoing funeral is populated only by wax sculptures. She is captured by Bo and imprisoned in a cellar, where she is taped to a wheel chair with her lips glued shut. Meanwhile, Wade is brought to a room, where the mysterious figure strips off his clothes and shaves him. The figure then straps Wade to a chair and sprays his naked body in wax.

Due to standstill traffic, Blake, Paige, Nick and Dalton realize theyre not going to make it to the game in time, and return to the campsite. Nick and Dalton offer to head to Ambrose to fetch Carly and Wade while Paige and Blake remain at the campsite.

Nick and Dalton arrive at Ambrose, also finding the town completely vacant. They decide to split up, and Dalton heads to the wax museum, where he encounters a wax-covered Wade sitting at a piano. After noticing that he is still alive, Dalton tries to remove the wax, but ends up peeling off a piece of his cheek in the process. The mysterious figure appears and cuts off half of Wades mouth, before chasing Dalton. After falling down stairs leading to the basement, Dalton is Decapitation|beheaded, and his body is dragged off. Meanwhile, Nick finds Bo and asks him about Carlys whereabouts. While attempting to draw her brothers attention, Carly gets her finger chopped off by Bo. She manages to tears her lips apart and screams for help. After Nick finds and saves her, Bo disappears. Soon after, they realize that the only inhabitants of the town are the sons of the wax museums proprietors, who have been luring in victims to create wax sculptures.

Back at the campsite, Paige and Blake are about to have sex when the radio outside the tent stops. While Blake goes to investigate, the killer, who Paige at first mistakes for Blake, enters the tent. When Paige flees the tent she trips over Blake, who has been stabbed in the neck. She gets up and flees, while the killer shoves the knife deeper into Blakes neck, killing him. Paige arrives at an abandoned sugar mill, but the killer stabs her in the foot from underneath. Paige hides in a car using a pole as a weapon. The killer finds her, but she escapes again, dropping the pole and hiding behind another car. Thinking that the killer has left, Paige stares through the car window only for the killer to throw the pole she dropped earlier through the window, penetrating her forehead and killing her instantly.

Bo soon finds Carly and Nick, and chases them to a movie theatre, where he is shot twice with an arrow by Nick. Nick and Carly return to Bos house to search for Wade and Dalton. While searching the house they discover that Bo, and the mysterious figure Vincent Sinclair were conjoined twins separated at birth by their father, Dr. Sinclair; however, the surgery left Vincent horribly disfigured, and his mother Trudy, a renowned sculptor, tried to hide his deformities by making him a mask made of wax. When Bo and Vincent return, Carly sees Paige and Blakes bodys in the back of their truck. While hiding, Carly overhears that Bo has manipulated his brother into continuing his mothers legacy using real people. The duo flees to the basement, where they find Dalton covered in wax. They are discovered by Vincent, and Nick sets fire to the basement to cut off their attackers. While running from Bo and Vincent, Nick and Carly reach the wax museum, where they find Wade. The fire spreads throughout the museum, slowly melting it down. Carly bludgeons Bo with a baseball bat after he stabs Nick in the leg with a screwdriver. She runs upstairs, pursued by Vincent, and Nick attempts to follow them, but his leg gets stuck in melting stairs. He manages to free himself and heads upstairs, while the first floor melts down. Nick arrives to save Carly, and Carly kills Vincent. The duo rush to escape from the wax museum as it melts to the ground, burying the tragic artists in their work.

The next morning, the smoke from the fire has drawn help from the outside and police and rescue workers sift through evidence throughout the town. The sheriff informs the group that the town has been abandoned for a decade, since its sugar mill closed down, and that it doesnt even appear on maps anymore. Over the radio, police reveal that the Sinclairs had three sons. The film ends with Lester, who had driven them to the town earlier, waving goodbye, implying that he is the third son.

==Cast==
*Elisha Cuthbert as Carly Jones
*Chad Michael Murray as Niko "Nick" Jones
*Brian Van Holt as Beauregard "Bo" Sinclair / Vincent Sinclair
**Thomas Adamson as Young Bo
**Sam Harkess as Young Vincent
*Paris Hilton as Paige Edwards
*Jared Padalecki as Waderick "Wade" 
*Jon Abrahams as Dalton Chapman
*Robert Richard as Blake
*Damon Herriman as Lester Sinclair Andy Anderson as Sheriff
*Dragitsa Debert as Trudy Sinclair
*Murray Smith as Dr. Victor Sinclair
*Emma Lung as Jennifer

==Production==
 
The film was originally titled Wax House, Baby before Warner Bros. realized they had permission to use the title House of Wax.  Posters and advertising banners were printed with the Wax House, Baby title.
Principal photography of House of Wax took place in Queensland, Australia in 2004. 

==Lawsuit==
In January 2006, it was announced by Warner Roadshow studio owners Village Theme Park Management and Warner Brothers Movie World Australia that they were suing special effects expert David Fletcher and Wax Productions because of a fire on the set during production.
 Gold Coasts Warner Bros. Movie World studios. The alleged grounds of negligence included not having firefighters on stand-by and using timber props near a naked flame. The set where the fire broke out has now been demolished and a field kept for Movie World for future projects. 

==Release==

===Box office===
Opening in 3,111 theaters, the film grossed $12 million in its first three days. Though most critics did not recommend the film, many of them acknowledged that it was well made and/or better than other recent similar films. House of Wax earned $70,064,800 worldwide. 46.6% of that total came from domestic receipts. House of Wax also earned $42,000,000 in VHS/DVD rentals. 

===Reception===
 
The film received negative reviews from critics despite being considered better than other teen flicks, with Rotten Tomatoes giving it a 25% "rotten" rating, with the general consensus being "Bearing little resemblance to the 1953 original, House of Wax is a formulaic but better-than-average teen slasher flick." Chicago Sun-Times film critic Roger Ebert gave the film two out of four stars, writing "House of Wax is not a good movie but it is an efficient one, and will deliver most of what anyone attending House of Wax could reasonably expect, assuming it would be unreasonable to expect very much." He also defended Hiltons performance, saying that "she is no better or worse than the typical Dead Post-Teenager, and does exactly what she is required to do in a movie like this, with all the skill, admittedly finite, that is required".

===Awards and nominations===
{|class="wikitable"
|-
!Award
!Category
!Subject
!Result
|- Golden Raspberry Award Golden Raspberry Worst Supporting Actress Paris Hilton
| 
|- Golden Raspberry Worst Picture Joel Silver Robert Zemeckis Susan Levin
| 
|- Golden Raspberry Worst Remake or Sequel Joel Silver Robert Zemeckis Susan Levin
| 
|-
|}

==Soundtrack==
{{Infobox album|
| Name = House of Wax: Music from the Motion Picture
| Type = Soundtrack
| Artist = Various, John Ottman
| Cover = House of Wax soundtrack cover.jpg
| Caption = Commercial soundtrack
| Released = May 3, 2005 (commercial), May 10, 2005 (score)
| Recorded =
| Genre = Soundtracks Film scores Alternative metal Gothic rock |
| Length = 50:41 (commercial), 41:46 (score)
| Label = Varese Sarabande
| Producer =
| Misc = {{Extra album cover
| Upper caption = Alternative cover
| Type = Soundtrack
| Cover = House_Of_Wax_Score_Soundtrack.gif
| Lower caption = Score soundtrack
}}
}}
House of Wax: Music from the Motion Picture is the title of a publicly released soundtrack used for House of Wax, consisting of commercially recorded songs.  A second album, simply titled House of Wax, was released containing the film score, composed by John Ottman. 

{{Track listing
| headline     = House of Wax: Music from the Motion Picture
| extra_column = Performer
| total_length = 50:41
 Spitfire
| extra1       = The Prodigy featuring Juliette Lewis
| length1      = 5:08
| title2       = I Never Told You What I Do For A Living
| extra2       = My Chemical Romance
| length2      = 3:52 Minerva
| extra3       = Deftones
| length3      = 4:17
| title4       = Gun in Hand
| extra4       = Stutterfly
| length4      = 3:29
| title5       = Prayer Disturbed
| length5      = 3:38
| title6       = Path to Prevail
| extra6       = Bloodsimple
| length6      = 3:17
| title7       = Dried Up, Tied and Dead to the World Marilyn Manson
| length7      = 4:15
| title8       = Dirt
| extra8       = The Stooges
| length8      = 7:00
| title9       = Not That Social
| extra9       = The Von Bondies
| length9      = 3:00
| title10      = Cut Me Up
| extra10      = Har Mar Superstar
| length10     = 3:10
| title11      = New Dawn Fades
| extra11      = Joy Division
| length11     = 4:46
| title12      = Taking Me Alive
| extra12      = Dark New Day
| length12     = 4:43
}}

{{Track listing
| headline     = Original Motion Picture Score
| total_length = 41:21

| title1       = Opening/Tantrum
| length1      = 3:28
| title2       = Ritual/Escape the Church
| length2      = 4:15
| title3       = Story of the Town
| length3      = 1:39
| title4       = Up in Flames
| length4      = 3:42
| title5       = They Look So Real
| length5      = 2:16
| title6       = Sealed Lips
| length6      =  3:56
| title7       = Brotherly Love
| length7      = 2:28
| title8       = Hanging with Baby Jane
| length8      = 3:36
| title9       = Paris Gets It
| length9      = 3:07
| title10      = Curiosity Kills
| length10     = 2:33
| title11      = Bringing Down the House
| length11     = 5:08
| title12      = Three Sons
| length12     = 2:28
| title13      = Endless Service
| length13     = 2:45
}}
There is a song appearing in the film which is not integrated in the Soundtrack. It is "Roland" by Interpol (band)|Interpol, and appears in the scene when the group decides to camp over the night at the beginning of the film.

 

==See also==
*2005 in film
*Mystery of the Wax Museum
*House of Wax (1953 film)|House of Wax (1953 film)

==References==
 

==External links==
* 
* 
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 