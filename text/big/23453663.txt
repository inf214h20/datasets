Hong Kong 97 (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = Hong Kong 97
| image =
| image_size =
| caption =
| director = Albert Pyun
| writer = Randall Fontana
| distributor = Trimark Pictures
| starring = Robert Patrick
| released = 9 November 1994
| runtime = 91 min
| country = USA
| language = English
}}

Hong Kong 97 is a 1994 film by Albert Pyun starring Robert Patrick, Brion James and Tim Thomerson. The story revolves around the transfer of sovereignty over Hong Kong from the United Kingdom to the Peoples Republic of China. An assassin kills several high-ranking Chinese officials and must get out of the country quickly before he himself is murdered.

The movie was released directly to videocassette on November 9, 1994.

==Cast==
* Robert Patrick - Reginald Cameron
* Brion James - Simon Alexander
* Tim Thomerson - Jack McGraw
* Ming-Na - Katie Chun
* Michael Lee - Chun
* Andrew Divoff - Malcolm Goodchild

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 


 