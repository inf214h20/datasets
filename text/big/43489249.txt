The Candy House
{{Infobox Hollywood cartoon|
| cartoon_name = The Candy House
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Manuel Moreno Lester Kline Fred Kopietz George Grandpre Ernest Smyth
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = January 15, 1934
| color_process = Black and white
| runtime = 8:37
| movie_language = English
| preceded_by = Chicken Reel The County Fair
}}

The Candy House is a 1934 short animated film featuring Oswald the Lucky Rabbit, one of the few in which he plays a character other than himself. The film is an adaptation of the fairy tale Hansel and Gretel by the Brothers Grimm.

==Plot==
Hansel (Oswald the Lucky Rabbit) and Gretel are two simple children who live with their father and stepmother. Although they have little resources, they are happy. One day the stepmother is irritated by the food shortage, and makes the children go to bed without dinner. The stepmother then talks to the father about taking the children deep in the forest to be abandoned. The father is too timid to argue. Hansel and Gretel overhear their conversation.

The next day, the stepmother takes the children further in the forest. Hansel picks up a sunflower, and drops its seeds on the way to mark their path. When they reach a certain distance, the stepmother tells them to cover their eyes, pretending she has a surprise for them. When she makes the run, Hansel is optimistic that he and his sister would have no trouble getting home. But to their dismay, birds have eaten the sunflower seeds.

With their trail cleaned off, Hansel and Gretel are left wondering how they can get back. After walking around for a few moments, they come across a cottage made of candy. They then start eating pieces of the house. While they are still eating, a voice invites them to come in. Upon entry, the two little rabbits are met by a hag who grabs them.

The hag appears to be more interested in Hansel as she has him placed in a cage to be fed. Gretel, however, is hung onto a column, reserved for a later purpose. The hag offers Hansel a whole roasted turkey. She then discloses she would make a meal out of him if Hansel gains some weight. Hansel, who dislikes her plans, lets a rat eat his food. The hag then picks up Hansel again, and tries to stuff him in the oven. Hansel, however, is able to get out of her grasp and run. He then helps bring down his sister. After a few chases, the little rabbits manage to trick the hag into running into the oven.

Back at home, the father laments for the lost children but the stepmother shows no regret. In no time, the parents hear singing voices coming from outside. To the parents surprise, Hansel and Gretel return home somehow. Hansel and Gretel also brought stuff to eat as they even brought with them the entire candy house. The stepmother also comes forward to apologise for her deed.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 
 


 