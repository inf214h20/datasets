Soldier's Girl
{{Infobox film
| name           = Soldiers Girl
| image          = 111.Soldiers.Girl.jpg
| caption        = 
| director       = Frank Pierson
| producer       = 
| writer         = Ron Nyswaner
| starring       = Troy Garity Lee Pace
| music          = Jan A.P. Kaczmarek
| cinematography = Paul Sarossy
| editing        = Katina Zinner
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = United States, Canada
| language       = English
| budget         = 
}}
Soldiers Girl is a 2003 Canadian-American drama film produced by   and Calpernia Addams and the events that led up to Barrys murder by fellow soldiers. It was written by Ron Nyswaner and directed by Frank R. Pierson, with Troy Garity starring as Barry and Lee Pace starring as Calpernia.

== Plot ==
Barry is a private with the 101st Airborne Division of the United States Army, stationed at Fort Campbell, Kentucky. Calpernia works as a showgirl at a transgender revue in Nashville, Tennessee when the two met in 1999. Barrys roommate Justin Fisher (Shawn Hatosy) brings Barry to the club where she performs. When Barry and Calpernia begin seeing each other regularly, Fisher begins spreading rumors on base about their relationship, which appeared to be a violation of the militarys  "dont ask, dont tell" policy about discussing the sexual orientation of military personnel. Barry faces increasing harassment and pressure, which explode into violence over Fourth of July weekend. While Calpernia performs in a pageant in Nashville, Barry is beaten to death in his sleep with a baseball bat by Calvin Glover, who had been goaded by Fisher into committing the crime. The film ends with a discussion of the aftermath.

==Cast==
* Troy Garity as Barry Winchell
* Lee Pace as Calpernia Addams
* Andre Braugher as Carlos Diaz
* Shawn Hatosy as Justin Fisher
* Philip Eddolls as Calvin Glover
* Merwin Mondesir as Henry Millens
* Dan Petronijevic as Collin Baker (as Daniel Petronijevic)

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Group !! Award Nomination !! Resulted 
|-
| rowspan="3" | 2003
| rowspan="2" | Emmy
| Outstanding Directing for a Miniseries, Movie or a Dramatic Special (Frank Pierson)
|  
|-
| Outstanding Makeup for a Miniseries, Movie or a Special (Prosthetic) (Raymond Mackintosh and Russell Cate)
|  
|-
| Gotham Awards
| Breakthrough Award (Lee Pace)
|  
|-
| rowspan="12" | 2004 GLAAD
| Outstanding Television Movie or Mini-Series
|  
|-
| rowspan="3" | Golden Globe
| Best Mini-Series or Motion Picture made for Television
|  
|-
| Best Performance by an Actor in a Mini-Series or a Motion Picture Made for Television (Troy Garity)
|  
|-
| Best Performance by an Actor in a Supporting Role in a Series, Mini-Series or Motion Picture Made for Television (Lee Pace)
|  
|-
| rowspan="2" | Independent Spirit Awards
| Best Male Lead (Lee Pace)
|  
|- Best Supporting Male (Troy Garity)
|  
|-
| Peabody Award
|
|  
|-
| rowspan="4" | Satellite Awards
| Best Motion Picture Made for Television
|  
|-
| Best Performance by an Actor in a Miniseries or a Motion Picture Made for Television (Troy Garity)
|  
|-
| Best Performance by an Actor in a Miniseries or a Motion Picture Made for Television (Lee Pace)
|  
|- Best Performance by an Actor in a Supporting Role in a Series, Miniseries or a Motion Picture Made for Television (Shawn Hatosy)
|  
|-
| Television Critics Association Awards
| Outstanding Movie, Miniseries or Special
|  
|}
Soldiers Girl was also hailed as one of the ten best Television Programs of the Year (2003) by the American Film Institute.

==See also==
* Transgender characters in film and television

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 