Walking Too Fast
{{Infobox film
| name           = Walking Too Fast
| image          = WalkingTooFast2010Poster.jpg
| alt            = 
| caption        = Film poster
| director       = Radim Špaček
| producer       = 
| writer         = Ondřej Štindl
| starring       = Ondřej Malý
| music          = Tomáš Vtípil
| cinematography = Jaromír Kačer
| editing        = Anna Ryndová
| studio         = 
| distributor    = Bontonfilm
| released       =  
| runtime        = 146 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
}} Czech Thriller Thriller and communist Czechoslovakia in the 1980s. The film was likened to the German film The Lives of Others.

Walking Too Fast is described as a dark thriller with unpredictable protagonist who becomes dangerous to all people around including himself. His way to reach the goal is self-destructive.

== Plot ==
The story revolves around Antonín Rusnák who is an StB agent. He feels rage towards the world. He feels sick and bored from his family and work. His psychological issues cause him breathing problems and his doctor advises him to breathe into a bag if he gets a fit.

Antonín and his partner Martin surveil Tomáš and Pavel. They are both dissidents but Pavel is also collaborating with Antonín and Martin. Antonín finds out that Tomáš, who is married, has a lover, Klára. Antonín becomes interested in her. He secretly follows her and uses his authority to protect her. He beats up a guy who bothers her or prevents her boss from firing her because of her links to the dissidents. He also pushes Tomáš to leave the country to get rid of him. He beats him up during interrogations and reveals his affair to his family and Tomáš finally decides to leave Czechoslovakia.

Antonín forces Pavel to arrange a meeting with Klára in exchange for seeing his file. Pavel then meets Antonín’s partner Martin in order to reveal the agreement he made with Antonín. Martin states that Antonín is done at StB but doesn’t know that Antonín is aware of their meeting and lures Martin to the woods where he attacks him and threatens him with a gun. Martin begs him for his life and Antonín handcuffs him to a tree.

Martin and Klára meet in Pavel’s apartment. The meeting doesn’t end well and Antonín gets a fit so he breathes into the bag which makes Klára realize that Antonín is the agent who terrorized Tomáš. He tells her that he followed her and she admits that she felt as if somebody was watching her for a long time. Antonín reveals his name and she says that he has to stop watching her.

Klára leaves the apartment but writes a message for Pavel on the Wall - "Svině" (Swine). Klára goes Home and meets the ambulance that takes her pregnant Friend Darina to the Hospital. She gets on the ambulance but looks around if anybody watches her. The film ends with Antonín waking up at the lakeside. The audience can now hear his thoughts as he enters the lake and goes deeper and deeper. He thinks about who he was and hopes that Klára will remember his name. In the end he states "It is beautiful here" as he kills himself.

==Cast== State Security agent. 
* Martin Finger - Tomáš Sýkora, a member of Dissident.  
* Kristína Farkašová - Klára Kadlecová, Tomáš’s lover. 
* Luboš Veselý - Pavel Veselý, an important figure in dissident movement and a close friend to Tomáš. 
* Lukáš Latinák - Martin Husár, Antonín’s partner
* Barbora Milotová - Silvie Sýkorová – Tomáš’s wife 
* Ivana Uhlířová – Darina, Klára’s friend.
* Oldřich Kaiser – Janeček, Antonín and Martin’s boss.

== Awards ==
The film gained 13 nominations for Czech Lion. It received 5 of them - Best film, Best Director, Best Screenplay, Best Cinematography and Best Actor in Leading Role. Film also received The Film Critics Award.

Walking Too Fast also succeeded at Czech Film Critics Awards. The film was awarded in categories Best film, Best director, Best Screenplay, Best Actor in Leading Role and RWE award.

== Reception ==
The film has won universal acclaim from critics in the Czech Republic. Out of all Czech films Walking too fast is number one on Czech movie aggregator Kinobox.cz where it holds 87% from critics.  František Fuka considers it to be the best Czech film in a few last years if not the best since the Velvet revolution. The film also gained positive reception from audience.it currently holds 74% on Czech-Slovak Film Database. 

=== Reviews===
*Jaroslav Sedláček, Kinobox.cz, February 4, 2010 85%  
*Kamil Fila, Aktuálně.cz, February 5, 2010 75%  
*Kamila Boháčková, A2 (časopis)|A2 3/2010  
*Jan Gregor, Respekt (týdeník)|Respekt 5/2010, February 1, 2010  
*Vít Schmarc, MovieZone.cz, January 26, 2010 90%  
*František Fuka, FFFilm, January 11, 2010 100%  

== International festivals ==
Film was shown at Karlovy Vary International Film Festival, Warsaw International Film Festival, Busan International Film Festival, East European Film Festival and Berlin International Film Festival.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 