Next Stop, Greenwich Village
{{Infobox film
| name = Next Stop, Greenwich Village
| director = Paul Mazursky
| image	= Next Stop, Greenwich Village FilmPoster.jpeg
| producer = Paul Mazursky Anthony Ray
| writer = Paul Mazursky
| starring = Lenny Baker Shelley Winters Ellen Greene Lois Smith Christopher Walken
| music = Bill Conti Dave Brubeck Quartet
| cinematography = Arthur J. Ornitz
| editing = Richard Halsey
| distributor = 20th Century Fox
| released =  
| runtime = 111 min.
| country = United States
| language = English
| gross = $1,060,000 (US/ Canada) 
}} 1976 comedy-drama|drama film, set in the early 1950s, written and directed by Paul Mazursky, featuring, amongst others, Lenny Baker, Shelley Winters, Ellen Greene, Lois Smith, and Christopher Walken. The film was generally well received by critics. Film review aggregate website Rotten Tomatoes gave the film a "fresh" score of 80% based on 10 reviews.  Filmmaker Mazursky had made his acting debut in Stanley Kubricks 1953 film Fear and Desire (shot in New York) and Next Stop, Greenwich Village is a semiautobiographical account of Mazurskys early life as an actor in that city. The film was entered into the 1976 Cannes Film Festival.    This film is also notable for being Bill Murrays first film.

==Plot== WASP who fancies himself a poet; and Bernstein, a gay man. All the while, he tries to maintain a stormy relationship with Sarah, his girlfriend. This band of outsiders becomes Larrys new family as he struggles as an actor and works toward a break in Hollywood.

==Cast==
*Lenny Baker as Larry Lapinsky
*Shelley Winters as Fay Lapinsky
*Ellen Greene as Sarah
*Lois Smith as Anita
*Christopher Walken as Robert (as Chris Walken)
*Antonio Fargas as Bernstein
*Jeff Goldblum as Clyde Baxter
*Bill Murray (uncredited) as Nick Kessler
*Stuart Pankin (uncredited) as Man at Party
*Vincent Schiavelli (uncredited) as Man at Rent Party

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 