The New One-Armed Swordsman
 
 
{{Infobox film name = The New One-Armed Swordsman image =  alt =  caption =  traditional = 新獨臂刀 simplified = 新独臂刀 pinyin = Xīn Dú Bì Dāo}} director = Chang Cheh producer = Runme Shaw writer = Ni Kuang starring = David Chiang Ti Lung Lee Ching music = Frankie Chan cinematography = Kung Mu-to editing = Kwok Ting-hung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 102 minutes country = Hong Kong language = Mandarin budget =  gross = 
}} Jimmy Wang, the star of the two preceding films in the series, The One-Armed Swordsman and Return of the One-Armed Swordsman. The film features Chang Chehs usual brand of violent swordplay and bloody effects.

==Cast==
*David Chiang as Lei Li of han dynasty
*Ti Lung as Feng Junjie / Hero Fung
*Lee Ching as Ba Jiao
*Ku Feng as Long Yizhi / Lung Er Zi
*Chan Sing as Chief Chan Chun Nam
*Wong Chung as Tiger Mansion Leader Jin Fen
*Lau Gong as Tiger Mansion Leader Jin Yi
*Wong Pau-gei as Tiger Mansion Leader Chen Jie
*Wang Kuang-yu as Tiger Mansion Leader Fan Yun He
*Wong Ching-ho as Boss Li
*Shum Lo as Blacksmith Ba
*Cheng Lui as Chieftain Ho Wai
*Yau Ming as Lungs disciple
*Cheng Kang-yeh as Lungs disciple
*Bruce Tong as Lungs disciple
*Wu Chi-chin as Lungs disciple
*Woo Wai as man at eatery
*Nam Wai-lit as man at eatery
*Leung Seung-wan as man at eatery
*Sham Chin-bo as man at eatery
*Lo Wai as wounded swordsman in flashback
*Yen Shi-kwan as Chieftain Hos man
*Tam Bo as Chieftain Hos man
*Chui Fat as Chieftain Hos man
*Hsu Hsia as Chieftain Hos man / Lungs disciple
*Huang Ha as Lungs disciple
*Yuen Cheung-yan as Lungs disciple
*Yuen Shun-yi as Lungs disciple
*Lee Chiu-jun as Lungs disciple
*Pao Chia-wen as Lungs disciple
*Fung Hap-so as Lungs disciple
*Yeung Jan-sing as Lungs disciple
*Wong Ching as Tiger Mansion member
*Wong Mei as Tiger Mansion member
*Lee Chiu as Tiger Mansion member
*Fung Hak-on as Tiger Mansion member
*Ko Hung as Tiger Mansion member
*Tung Choi-bo as Tiger Mansion member
*Chin Chun as Chieftain Lin Shing
*Lee Hang as martial artist at meeting
*Yeung Pak-chan as martial artist at meeting
*Chai Lam as martial artist at meeting
*Cheung Sek-au as martial artist at meeting
*Gam Tin-chue as martial artist at meeting
*Lam Yuen as martial artist at meeting
*Ling Hon as martial artist at meeting
*Lau Jun-fai
*Wong Shu-tong
*Yuen Woo-ping
*Lai Yan
*Lau Kar-wing
*Chow Siu-loi
*Law Keung

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 

 