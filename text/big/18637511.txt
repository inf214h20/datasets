In Rwanda We Say…The Family That Does Not Speak Dies
{{Infobox Film
| name = In Rwanda We Say…The Family That Does Not Speak Dies
| image = In Rwanda screenshot.jpg
| caption = Screenshot depicting two Rwandans
| director = Anne Aghion
| producer = Laurent Bocahut Anne Aghion
| writer = 
| starring = 
| music = 
| cinematography = 
| editing = Nadia Ben Rachid
| distributor = Gacaca Productions and Incarus Films
| released = 2004
| runtime = 54 min.
| MPAA = 
| country = Rwanda English subtitles
| budget = 
| gross = 
}} English subtitles. 

==Plot==
Set in Rwanda, Anne Aghion, the director, interviews a genocide offender who has been released back into his community, and the victims of the genocide. The film follows how at first, the coexistence between the people who instigated the genocide and the victimized people is unbearable. Many of the victims feel rage toward their former oppressors. But gradually, the victims and oppressors start talking to the camera, and then to each other as they start the difficult task of living with each other. The documentary portrays how the peoples spirits cannot be crushed by the Rwandan Genocide, the 1994 mass killing of hundreds of thousands of Rwandas minority Tutsis and the moderates of its Hutu majority by the Interahamwe and the Impuzamugambi.

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 

 