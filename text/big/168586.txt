Dark Eyes (film)
{{Infobox film name = Dark Eyes image = Dark Eyes.jpg caption = Film poster director = Nikita Mikhalkov producer = Carlo Cucchi Silvia DAmico Bendico writer = Aleksandr Adabashyan Nikita Mikhalkov Suso Cecchi dAmico Anton Chekhov (stories) starring = Marcello Mastroianni Silvana Mangano Marthe Keller Yelena Safonova Marthe Keller Vsevolod Larionov music = Francis Lai cinematography = Franco Di Giacomo editing = Enzo Meniconi distributor = RUSCICO released = September 9, 1987 runtime = 118 minutes country = Italy Soviet Union language = Italian / Russian / French
|budget =
}} 1987 Italian Italian and Russian language film which tells the story of a 19th-century married Italian who falls in love with a married Russian woman. It stars Marcello Mastroianni and Yelena Safonova.   

== Source material == famous Russian art song.

==Cast==
* Marcello Mastroianni - Romano
* Marthe Keller - Tina, Romanos Mistress
* Yelena Safonova - Anna Sergeyevna, Governors Wife (as Elena Sofonova)
* Pina Cei - Elisas Mother
* Vsevolod Larionov - Pavel (Russian Ship Passenger)
* Innokenti Smoktunovsky - the Governor of Sysoyev (as Innochentij Smoktunovskj)
* Roberto Herlitzka - Lawyer
* Paolo Baroni - Manlio
* Oleg Tabakov - His Grace
* Yuri Bogatyryov - Marshall (as Jury Bogatiriov)
* Dmitri Zolotukhin - Konstantin (as Dimitri Zolothuchin)
* Silvana Mangano - Elisa (Romanos Wife)
* Jean-Pierre Bardos - Laying guest (as J. Pierre Bardos)
* Nino Bignamini - Buyer
* Maria Grazia Bon - His wife

==Location==
Principal shooting took place at the Montecatini Terme in Tuscany, in the Volga town of Kostroma, and in Leningrad (Vladimir Palace, Peter and Paul Fortress). A few of the actors had previously appeared together in A Cruel Romance, a 1984 Russian film starring Mikhalkov and shot on location in Kostroma.

== Awards == Best Actor at the 1987 Cannes Film Festival    and was nominated for the Academy Award for Best Actor. Safonova was awarded the David di Donatello as Best Actress.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 