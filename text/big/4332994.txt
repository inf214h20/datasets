Beef III
{{Infobox film
| name           = Beef III
| image          = Beef3.jpg
| image size     = 180px
| caption        = Beef III Cover
| director       = Peter Spirer
| producer       = Joshua Krause, Peter Spirer
| writer         = Peter Spirer Kay Slay
| starring       =
| music          = Nu Jerzey Devil
| cinematography =
| editing        = Gabriel Reed
| distributor    = Image Entertainment
| released       =  
| runtime        = 85 minutes
| country        =
| language       = English
| budget         =
| gross          =
}}

Beef III is the third installment of the Beef (film)|Beef series. It is a documentary about Hip hop rivalries and beefs (arguments). It was released on DVD on November 15, 2005. It was directed and produced by Peter Spirer and lasts approximately 85 minutes. It was narrated by DJ Kay Slay and scored by Nu Jerzey Devil. The next film in the series is called Beef IV.

==Beefs==
===Twista vs. Bone Thugs-N-Harmony=== Cleveland and Columbus, Ohio). They both became very popular for their quick, rapid-fire style of rapping delivery. The beef started with a dispute over who originated this style. Bone Thugs say that Twista did not come out with the rapid-fire style. Layzie Bone elaborated and said "yeah he came spittin it a hundred miles an hour but he wasnt adding the harmony like us". Twista attacked back with "Crook County" feat. Psychodrama, calling Bone "Hoes of the Harmony." He also threatened that they were going to see Eazy-E soon. However, Layzie Bone and Twista became good friends and they dropped "Mid- West Invasion," soon thereafter the beef ended. Krayzie Bone and Wish Bone still had beef with Twista, they put out subliminal messages dissing Twista in Krayzie Bones "Gemini" album and "Leatherface: The Legend" underground album. It was until the "Spit Your Game" video shoot that Krayzie, Wish and Twista ended their Beef.

===Chingy vs. Nelly===
St. Louis, Missouri|St. Louis own Chingy was an emerging artist mainly from the help he received from Nelly, the biggest premiere rap artist to so far come out of the Gateway City. Nelly decided to take Chingy with him on tour, but Chingy felt that Nelly was trying to take all the fame and keep him under his wings, to Chingys dissatisfaction. Because of lack of recognition, Chingy became disenchanted. Then Ludacris and his Disturbing tha Peace record label offered Chingy a record deal, and he then traded shots at Nelly. Nelly also felt disrespected by Chingy, stating to the press that Chingy never gave credit to Nelly because of the success he was having. Then it escalated when Nelly Released his 2004 album "Sweat" with the song "Another One" which was a back handed form of flattery to remind Chingy who came out first.   So Chingy struck back on "We Got". At the Radio Music Awards in Las Vegas, Chingy approached Nelly to squash the beef. But to no avail. Chingy also challenges Nellys claim of the word "Derrrty" saying hes a liar and the people have been saying "Derrrty" since back in the day. As tension began to heat up through St. Louis between Derrty Ent. and the Chingy camp, Ching knew there was one man who could reason with both men. That man would be M.J., who is Nellys cousin and has raised Chingy since he was six. Chingy got MJ to talk to Nelly to resolve the problem. In 2006, Chingy appeared in Ali & Gipps video "Go Head", therefore squashing the beef.

===Lil Scrappy vs. Orlando Police Department=== local police stepped in. They gave Scrappy a warning, because he took his shirt off (which made female fans even more aggressive). The reasoning for this was because the police were afraid of a potential riot. They stated that if Scrappy were to do something like this they would cancel the concert. Then, Scrappy did a stagedive, which made the police react and they entered the stage and stopped the music. An officer then bumped into Lil Scrappys manager making him angry. The same officer then rushed over to Lil Scrappy and pushes him off  stage.

===Ludacris vs. T.I.=== Game on the album version. Ludacris and T.I. sat down and talked about it and are now on good terms.

The beef reignited since the film when T.I. made a disrespectful comment on his single "You Know What It Is", about Ludacris winning his grammy for rap album of the year ("Release Therapy") which he and T.I. were both nominees ("King"). The comment made was T.I. saying he felt that Ludacris didnt deserve the award and that T.I. actually had the rap album of the year.  He also dissed Ludacris on his verse to Rockos song "Umma Do Me."  However, the two are once again on good terms, and were featured on each others albums in 2008.

===Lil Flip vs. T.I.=== The Source. Producer Nick Fury asked T.I. to do a verse on Lil Flips "Game Over" remix, so T.I. discussed this with Lil Flip and his record label, and decided to do it. each rappers entourages) until it eventually died out. They both made amends shortly after The Game and 50 Cent did so in similar fashion (although Lil Flips and T.I.s beef has stayed settled).

===G-Unit vs. The Game===
For further information on this beef, see G-Unit vs. The Game.
 The Game was thought to be over (or everted from higher levels). Beef III ended with the truce the two rappers had held in New York City, where they each donated over $200,000 dollars to charity. This documentary ended with the feeling that for now it looked like an East vs. West rivalry was avoided. There was somewhat of a cliffhanger when in the end the DVD stated that at least for now a coast vs. coast beef was gone. Since Beef III was released, the rivalry had actually intensified. In an extended interview found in the extras portion of this DVD, The Game further explained his dissatisfaction with 50 Cent and G-Unit, including footage of a concert where Game performed at Giants Stadium in East Rutherford, New Jersey (just eight miles across the Hudson River from New York City), in which he threw his G-Unit chain into the back row of the crowd.

===The Game vs. Yukmouth===
Yukmouth first met The Game at a club, at the time Yukmouth was engaged in a feud with 50 Cent and G-Unit. The Game released a diss track aimed at the rapper over the "I Got 5 on It" beat, a song which Yukmouth recorded when he was a part of Luniz. Yukmouth responded with a track that mocked The Games appearance on Change of Heart. The two later tried to bury the hatchet, due to a personal friend and even recorded a song together with C-Bos protégé Speedy, named "Peace". However the rivalry continued afterward, since Game dissed Yukmouth on the "Peace" song (they recorded their verses separately).   Since then, Yukmouth responded by releasing the song and music video "Game Over Part 2" over Fabolous "Breathe" instrumental in which it parodied Game. In the video there is a lookalike of the rapper getting robbed and beaten up. Yukmouth claimed on the song that Game had a tongue ring and was slapped by mogul Suge Knight. Since the West Coast Peace Conference both rappers ended the feud.

== Production information ==
*Narrator: DJ Kay Slay
*Writer: Peter Spirer
*Editor: Gabriel Reed
*Producers: Joshua Krause, Peter Spirer
*Associate Producer: Julia Beverly
*Executive Producer: Quincy D. Jones III (QD3)
*Director: Peter Spirer
*Studios/Distribution Companies: Image Entertainment, QD3 Entertainment, Open Road Films, Aslan Productions
*Release Date: November 15, 2005
*MPAA Rating: R for pervasive language, some drug content and sexual references.
*Running Time: 85 minutes

==See also==
*Beef (film)
*Beef II
*Beef IV
* 

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 
 