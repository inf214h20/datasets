Road (film)
 
{{Infobox film
| name           = Road
| image          = Road poster.jpg
| caption        = Movie poster for Road 
| director       = Rajat Mukherjee
| producer       = Ram Gopal Varma
| writer         = Rajnish Thakur
| starring       = Vivek Oberoi Manoj Bajpai Antara Mali
| music          = Sandesh Shandilya
| cinematography = Sudeep Chatterjee
| editing        = Chandan Arora  
| distributor    = Varma Corporation Ltd 
| released       = 27 September 2002 
| runtime        = 134 minutes 
| country        = India
| language       = Hindi
}} road thriller The Hitcher.

==Synopsis==
On their way from Delhi to Jodhpur, India, in an SUV, on a deserted highway, an eloped couple Arvind (Vivek Oberoi) and Lakshmi (Antara Mali), encounter a mad wayfarer (Vijay Raaz), Babu (Manoj Bajpai) a Hitchhiker who turns out to be a serial psychopath killer on road, Inderpal (Makrand Deshpande) a happy go lucky, intelligent, responsible truck driver and an irresponsible, eccentric cop (Sayaji Shinde). 

==Plot==
Arvind (Vivek Oberoi) and Lakshmi (Antara Mali) are in love and want to get married. However, Lakshmis dad, a cop, is against their affair. Hence the two decide to elope from Delhi and travel to get married at Arvinds ancestral haveli in Rajgarh, Alwar, Rajasthan, by road, passing by a desert, in a Tata Safari.

After an escape from an aggravated assault, by a mad wayfarer, they bump into a smooth-talking hitchhiker Babu (Manoj Bajpai) who is stranded in the middle of no where. Babu convinces the young couple to give him a lift. Travelling with Babu proves a nightmare for Arvind and Lakshmi, Babu turns out to be a psychopath. Soon, Lakshmi finds herself hostage of an armed Babu. Thanks to the timely intervention of a truck driver Inderpal (Makrand Deshpande) and the highway petrol bunk owner, an aspiring actor Bhanwar Singh (Rajpal Yadav), Arvind rescues Lakshmi from Babu.

After a while, Babu again finds a way, after attacking Inderpal, and re-attacks the couple by haunting them on the road, via Inderpals truck. But, this time, Babu fails to get hold of the couple. After dodging an FIR, in the nearest police station, the couple finds a motel, the couple recuperate. The next day Babu again attacks the couple and elopes with Lakshmi.

When the car breaks down on the way, Babu kills his another victim, a traveler (Snehal Dabi) attracted to Lakshmi, and elopes in the travelers vehicle. As the cops are on their way to catch hold of him, he manages to attack them.

On the other hand, the cops suspect Arvind as the serial killer, as he first eloped with Lakshmi, who is D.C.Ps daughter. This irresponsible intervention of the cop (Sayaji Shinde), who fail to trust Arvind, makes it impossible to chase Lakshmi. Finally, a frustrated Arvind, manages to escape with the cops jeep, gets a bike and chases Babu to death. The couple finally find their way out of the desert.

==Cast==
*Vivek Oberoi as Arvind Chauhan
*Antara Mali as Lakshmi
*Manoj Bajpai as Babu
*Sayaji Shinde as Inspector Singh
*Vijay Raaz as Villager on the road
*Makrand Deshpande as Inderpal
*Ganesh Yadav as Bungalow watchman
*Rajpal Yadav as Bhanwar Singh
*Raj Zutshi as Kishan bhai
*Koena Mitra as special appearance in item number "Khullam Khulla"

==Soundtrack==
*Road Ke Har Mod Pe: Gary Lawyer & Tannishtha Sanjeevani & Sonu Nigam
*Raste Raste: Sunidhi Chauhan & Vinod Rathod
*Khullam Khulla Pyaar: Sonu Nigam & Sunidhi Chauhan KK
*Pehli Nazar Mein: Mohit Chauhan & Sunidhi Chauhan
*Road Rage: Instrumental

==References==
 

==External links==
*  

 

 
 
 
 
 