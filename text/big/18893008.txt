Impasse (1969 film)
 
{{Infobox film
| name           = Impasse
| image          = Impasse 1969 movie poster.jpg
| image size     = 298px 
| alt            = Movie poster featuring a man in a white suit kicking another man in the face. In the background, two men carry a large trunk and behind them an Asian woman in a black dress is running while holding a gun in her right hand. On top, a man stands on top of a large trunk while being hoisted out of a hole.
| caption        = Theatrical release poster
| director       = Richard Benedict
| producer       = Hal Klein Aubrey Schenck
| writer         = John C. Higgins
| based on       =  
| starring       = Burt Reynolds Anne Francis Vic Diaz Lyle Bettger Rodolfo Acosta 
| music          = Philip Springer
| cinematography = Mars B. Rasca
| editing        = John F. Schreyer
| distributor    = United Artists
| released       = 1969
| runtime        = 100 minutes
| country        = United States
| language       = English
}}
Impasse is a 1969 American film about a group of adventurers trying to recover gold lost in the Philippines during World War II. It stars Burt Reynolds, Anne Francis, Vic Diaz, Lyle Bettger and Rodolfo Acosta 

== Plot ==
Pat Morrison (Reynolds) runs a shady salvage operation in Manila. His latest scheme involves finding $3 million worth of gold bars hidden by the military during World War II. To this end, he needs the help of several former soldiers who were present when the gold was hidden. The first is Jesus (Vic Diaz), a Filipino muslim and Morrisons business associate. The second is Draco (Rodolfo Acosta), a hard-drinking, hot-tempered Apache living on an Indian reservation who answers Morrisons telegram with the promise of finding a wartime lover named Maria Bonita.

The trio then breaks the third man, the bigoted Hansen (Lyle Bettger), out of a Filipino jail. Draco eventually manages to find Maria in a local bar, but discovers that she has grown older and gained weight. Meanwhile, Morrison rescues the captured Trev Jones (Clarke Gordon), a veteran with a heart condition who has been abducted by Wombat (Jeff Corey). With Trevs help, Morrison and company are able to figure out the golds exact location in the Malinta Tunnel on the island of Corregidor. Along the way, Morrison falls in love with Joness daughter Bobby (Anne Francis), a tennis champion. This complicates matters as Morrison is sleeping with Jesuss Japanese wife Mariko (Miko Mayama), a fact that Jesus discovers and confronts Morrison with.

Despite their differences, the four men are able to successfully locate and retrieve the gold. However, they meet resistance in the Philippine military. A gunfight ensues that leaves Hansen dead, Draco wounded and Jesus captured. After a brief chase, Morrison finds his escape route blocked and it is revealed that a jealous Mariko had tipped authorities off to their plan. The film ends at the airport with Bobby returning to the United States and a smiling Morrison being led away in handcuffs.

== Reception == Howard Thompson of the New York Times described the film as "a good one that may get away all to soon", a reference to its limited release. While he cited a couple of glaring plot holes, he praised the exotic setting, the performances, script and direction. Thompson, Howard. " ". 9 May 1969. New York Times. 

== References ==
 

==External links==
* 

 
 
 
 
 
 
 