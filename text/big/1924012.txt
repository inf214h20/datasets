Who Dares Wins (film)
 
{{Infobox film
| name           = Who Dares Wins
| image          = Who Dares Wins - uk film poster.jpg
| caption        = Poster for the films UK cinema release
| director       = Ian Sharp
| producer       = Chris Chrisafis Euan Lloyd Raymond Menmuir
| writer         = Reginald Rose
| starring = {{Plainlist|
* Lewis Collins
* Judy Davis
* Richard Widmark Tony Doyle
* John Duttine
* Kenneth Griffith
* Rosalind Lloyd
* Ingrid Pitt
* Norman Rodway
* Edward Woodward
* Robert Webber
}}
| music          = Roy Budd
| cinematography = Phil Meheux
| editing        = John Grover
| studio         = Richmond Light Horse Productions / Varius Rank Film Distributors (UK) MGM|MGM/UA Entertainment Company (US)
| released       =   
| runtime        = 125 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} motto of the elite Special Air Service (SAS).

The plot is largely inspired by the Iranian Embassy siege of 1980, where the Unitied Kingdoms SAS special forces dramatically stormed the building to rescue those being held hostage inside. Euan Lloyd, the films producer, got the idea for the film after watching live television coverage of the event, but he had to move quickly to prevent the idea being taken by somebody else. An initial synopsis was created by George Markstein. This was then turned into a novel by James Follett as The Tiptoe Boys, in thirty days. Meanwhile, chapter-by-chapter as the novel was completed, it was posted to Reginald Rose in Los Angeles, who wrote the final screenplay. 

==Plot== London is militant group attached to CND has been planning a significant act of terrorism for the near future. The person killed during the protest demonstration was an undercover intelligence officer who had infiltrated the terrorists. The Commanding Officer of the Special Air Service, Colonel Hadley, suggests a new line of inquiry for the investigation.
 US Army Rangers and Captain Freund is a member of the German GSG9. They are taken to the Close Quarter Battle house and witness an SAS room entry assault. Colonel Hadley introduces them to the SAS man playing the hostage, Captain Peter Skellen, and informs them they will be with Skellens troop, consisting of Baker, Dennis and Williamson. During an exercise in Brecon Beacons, Hadley and Major Steele discover Skellens troop torturing Hagen and Freund. Skellen is dismissed from the Regiment. The torture and dismissal are a ruse to repaint Skellen as a disgraced former SAS operative, and Hagen and Freund are innocent victims in the scheme.

Skellens intelligence contact, Ryan, advises him to meet Frankie Leith and Rod Walker, the two people who lead The Peoples Lobby, the militant group believed to be planning the act of terrorism. Skellen tells his wife that he will be going away for a while on a mission. A foreign man, Malik, arranges with a city banker for the distribution of large sums of money to various left organisations, including the PL. Skellen arranges to meet Leith at a bar frequented by PL members and initiates a romantic relationship with her, to the annoyance of Walker and his cohorts Helga and Mac. Leith takes Skellen to the organizations offices and introduces him to the group. Leith appreciates Skellens SAS background, and offers him a job as security consultant to the PL; she also allows him to move in with her. To strengthen Skellens cover story, Hadley informs Hagen and Freund of Skellens location; the wronged men attack Skellen at Leiths home and inflict a severe beating. As a result Leiths few remaining doubts about Skellen vapourise, but Walker and his cronies still are not fully convinced. Helga observes Skellen meeting the same unknown individual (Ryan) in various locations. Their scrutiny intensifies when Walker and his associates witness Skellens meeting with his wife and daughter. They use photos from their surveillance to convince Leith that Skellen is not all he seems. Walker orders Helga to kill Ryan, cutting Skellens link to Hadley. Hadley has no choice but to trust Skellens abilities to uncover the groups plans and escape alive. He orders police protection for Skellens family.

Despite his official advisory capacity, Skellen is denied details about the upcoming PL operation.  On the day of the operation, Leith and Walker instruct Helga and Mac to take Skellens family hostage. Leith uses this to blackmail Skellen into unconditional co-operation.

The terrorists and Skellen arrive at the US Embassy in a hijacked coach. Wearing stolen   is launched at Holy Loch Naval Base, all the hostages will be killed. Currie questions Leiths motivations, and Leith responds that her ultimate goal is the disarmament of the whole world. This opens a debate about method and political philosophy that only antagonizes the terrorists.

Meanwhile, Dennis and three fellow SAS troopers arrive at Skellens home. They set up in the attached house next door, using sensing devices to covertly observe Helga, Mac and their captives through the wall.

Skellen manages to separate himself from the group by feigning a need for the toilet. He uses a shaving mirror to heliograph floodlights and signal Hadley via Morse code, telling him to attack at 10 a.m. while Skellen creates a diversion. Hadley cannot get permission for an SAS attack because the British Home Secretary insists that Powell resolve the situation through negotiation. As the tension mounts inside the embassy, a mistake by one of the terrorists causes the death of the SAC C-in-C. This enables Powell and Hadley to get the permission for their assault.

The SAS operatives in the house adjoining Skellens remove a large area of the bricks separating the attached houses. Meanwhile, Helgas temper at the Skellens crying baby escalates into a fight with Skellens wife. The operatives work fast to attach a charge to the exposed wall, cut the lights and blow the wall so two SAS soldiers can shoot through the gap and kill both Helga and Mac.

As the SAS mount an assault on the embassy, the terrorists panic. Skellen overpowers and kills two terrorists. The SAS, deploying from helicopters, force open doors and enter through windows. As they methodically clear the embassy, Skellen kills more terrorists including Walker. Skellen joins with Baker and his troop to search for Leith, as the other troopers bundle the hostages to safety. When Skellen hesitates to kill Leith on sight, Major Steele kills her before she can kill Skellen.

The Ambassador thanks the troopers as they leave the embassy. Skellen and his troop apologise to Hagen and Freund, explain the reason for their actions, and make peace. Skellen departs on one of the helicopters with his colleagues.

In a government building a politician complains to a colleague about the violent end to the siege. He then meets the financier Malik, and they discuss future similar actions.

An on-screen list of notable terrorist incidents appears over the closing credits, accompanied by a rendition of The Red Flag.

==Cast==
* Lewis Collins as Captain Peter Skellen
* Judy Davis as Frankie Leith
* Richard Widmark as Secretary of State Arthur Currie
* Edward Woodward as Commander Powell
* Robert Webber as General Ira Potter Tony Doyle as Colonel J. Hadley
* John Duttine as Rod Walker
* Kenneth Griffith as Bishop Crick
* Rosalind Lloyd as Jenny Skellen
* Ingrid Pitt as Helga
* Norman Rodway as Ryan
* Maurice Roëves as Maj. Steele Bob Sherman as Hagen Mark Ryan as Mac
* Patrick Allen as Police Commissioner
* Zig Byfield as SAS trooper Steve Baker
* Nick Brimble as SAS trooper Williamson
* Anna Ford as Newsreader Paul Freeman as Sir Richard
* Oz Clarke as Special Branch Man

==Production== The Professionals, was invited to SAS headquarters at Stirling Lines where he met with some of the troops who assaulted the Iranian Embassy. With the co-operation of the SAS achieved, production moved ahead swiftly.

An SAS trainer was used to train the actors portraying SAS troopers. However, Sharp says Collins required no training and impressed the SAS instructor with his skills.
 Fijian trooper who had a mishap during the Iranian Embassy assault. The trooper told how he got caught up in his descent and his uniform caught fire due to the explosives used for their forced entry. Inspired by this, Sharp had a similar scene inserted.
 The Union Chapel in Islington.

Others were shot in Kynance Mews, W2. 

When it came time to shoot the SAS assault on the US Embassy, the crew had prepared the helicopters and stuntmen but the SAS offered to do the scene instead. Sharp accepted as he thought the look they gave could not be replicated by the crew.

Filming wrapped after 7 weeks. Lloyd started to organise the publicity campaign, but like his previous film The Wild Geese, word had already started to spread that the film was a right-wing propaganda film attacking the Soviet-backed CND organisation.  

==Critical reception==
Film critic Roger Ebert of the Chicago Sun-Times said: "There are so many errors of judgment, strategy, behavior and simple plausibility in this movie that we just give up and wait for it to end. You know youre in trouble when the movies audience knows more about terrorism than the terrorists do." 
 
Who Dares Wins was also panned by some critics as being right-wing. Sight and Sound described the film as "hawkish". 

Film critic Alan Patterson had a favourable review and summary of the film in 2010. He concluded, "All in all this is an entertaining British action picture that beats many of todays offerings into a cocked hat. Theres the necessary hint of menace to make it work and to involve the audience. A great cast with an excellent score. In short, a well made movie. Movie score : 8" (out of 10). 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 