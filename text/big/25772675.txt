La Cucina (film)
 
{{Infobox film
| name           = La Cucina
| image          = La Cucina cover.png
| caption        = la Cucina film cover
| director       = Allison R. Hebble Zed Starkovich
| producer       = A. W. Gryphon Zachary Kahn Jackie Olson Crystal Santos Starlotte Dawn Smith
| writer         = A. W. Gryphon
| starring       =
| music          = Ian Ball
| cinematography = Alan Caudillo
| editing        = Zed Starkovich
| studio         =
| distributor    = Anthem Pictures
| released       =  
| runtime        =
| country        = United States
| language       =
| budget         =
| gross          =
}} American Beauty, this was his first attempt at scoring an entire film. 
 Showtime on December 4, 2009. Anthem Pictures released La Cucina on DVD and Blu-ray on January 12, 2010.

A.W. Gryphon also wrote the supernatural novel Blood Moon.   Allison Hebble, Zed Starkovich and Starlotte Dawn Smith were all neighbors in West Hollywood. They got to talking one day over a glass of wine and decided to make a movie. They called some friends and less than a year later went into production on La Cucina.

==Plot==
La Cucina is a brief slice of life that is set in present day Los Angeles, on a hot summer evening in a West Hollywood apartment building. An intimate character study, it focuses on 3 couples and their very different relationships over food & wine. A young writer, Lily (Hendricks) goes on an emotional rollercoaster with the much older (17 years) and sophisticated Michael (Almeida). They navigate through a web of exploration on marriage, cheating, babies, and entrapment. In the kitchen above, the very pregnant Shelly (Hailey) is terrified that becoming a mother will drive away her husband and has turned for advice to her lesbian friend Jude (Hunter), sure that this magnificent cook who has been in a long-term relationship has it all figured out. It is revealed that Shelly has her own secrets that fuel her fear and Judes relationship may not be exactly as it appears. They struggle through—fighting, laughing, crying, cooking & eating—trying to work out what really makes relationships work.

==Cast==
* Christina Hendricks as Lily
* Joaquim de Almeida as Michael
* Leisha Hailey as Shelly
* Rachel Hunter as Jude
* Kala Savage as Raven
* Oz Perkins as Chris
* Michael Cornacchia as Andy
* Clare Carey as Celia

==Reviews==
"Move over Julia Child, Christina Hendricks is in the kitchen!" 

“  intriguing introspective look at. . .relationships.”  

"4 stars. “La Cucina” made me want to grab a bottle of Chianti and start cooking!"  

==References==
 

==External links==
* http://www.curvemag.com/Curve-Magazine/July-August-2008/Talking-it-Out
*  
*  
*  
* http://www.diariodominho.pt
* http://archive.is/20130102015838/http://origin.www.afterellen.com/blwe/05-25-2007
* http://kittenlounge.onsugar.com/2379386
* http://kittenlounge.onsugar.com/2387311
* http://hollywoodawards.com/finalists/films2007.html
* http://www.thefrontpageonline.com/articles1-6829/BacklotFilmFestivalWinnerComestoTelevisionThisWeekend
* http://www.thefrontpageonline.com/articles1-4530/AGrandTimeWasEnjoyedbyAlloftheStarsattheBacklotFestival
* http://www.backlotfilmfestival.com/thefilm.htm
* http://www.lasplash.com/publish/Entertainment/cat_index_nyc_events/La_Cucina_An_Alison_Hebble_Zed_B_Starkovich_Film.php
* http://popwatch.ew.com/2009/12/10/move-over-julia-child-christina-hendricks-is-in-the-kitchen/
* http://www.lifeinitaly.com/decor/lacucina.asp
* http://archive.is/20130116085038/http://www.afterellen.com/blog/dorothysnarker/leisha-hailey-and-christina-hendricks-gather-in-la-cucina
* http://www.gmax.co.za/feel/film/film07/070418-deepinhllywd.html
* http://pro.imdb.com/title/tt0978800/
* http://uhhuhherfan.com/2009/12/leisha-hailey-la-cucina-on-showtime/
* http://www.sho.com
* http://www.anthemdvd.com/catalog/la_cucina/main.htm
* http://www.flixster.com/movie/la-cucina
*  

 
 
 