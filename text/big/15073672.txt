Charlie Chan at the Circus
{{Infobox film
| image          = Charlie_Chan_ATC.jpg
| image size     = 190px
| director       = Harry Lachman John Stone
| writer         = Robert Ellis
| starring       = Warner Oland
| cinematography = Daniel B. Clark
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 72 minutes
}}
Charlie Chan at the Circus is the 11th film produced by Fox starring Warner Oland as Charlie Chan. A seemingly harmless family outing drags a vacationing Chan into a murder investigation.                        

== Plot ==
Charlie Chan takes his wife and twelve children on an outing to a circus after receiving a free pass from one of the owners, Joe Kinney. Kinney wants Chan to find out who is sending him anonymous threatening letters. Nearly all of the circus workers are suspects, since Kinney is very unpopular. However, when Chan goes to meet him during the nights performance, he finds the man dead, seemingly killed by a rampaging gorilla who somehow escaped from his cage. 

Lieutenant Macy takes charge of the investigation, assisted by Chan and his overzealous eldest son Lee, who also takes the opportunity to (unsuccessfully) romance Su Toy (Toshia Mori, credited as Shia Jung), the contortionist. On Chans advice, Macy lets the circus continue on to its next stop, with the trio tagging along. During the train ride, an attempt is made to murder Chan with a poisonous cobra. 

Then someone tries to break into the circuss safe, but nothing is missing. Macy finds a marriage certificate inside, showing that Kinney supposedly married circus wardrobe lady Nellie Farrell in Mexico. However, Kinneys fiance Marie Norman claims that she can prove Kinney was not in Mexico the day indicated on the certificate. Before she can prove it, during her act, someone shoots one of the ropes of her trapeze swing and she falls to the ground, seriously injured, but still alive. 

A doctor is summoned. Chan states that Marie is too badly hurt to move, so the doctor must operate on the spot. Chan asks everyone to keep quiet and clear the area, so as not to cause a potentially fatal distraction for the medical staff during the delicate operation. 

Meanwhile, Chan has noticed a newspaper article about a crime committed at a casino the day of Kinneys alleged marriage. He sends his son to phone for a description of the crooks involved from the police. When Lee returns, he sees a man slug the policeman guarding the gorillas cage and let the ape out again. He struggles with the man, but is knocked out.
 snake charmer Tom Holt in a costume, trying to pin a second death on the escaped animal. He and Kinney had robbed the casino and hidden out at the circus. However they had had a falling out over the division of the money, leading to Kinneys murder. Nellie Farrell and her brother Dan are also arrested for trying to use a forgery to gain half interest in the circus. Charlie Chan agrees to obtain a lifetime pass to the circus for his family. He sees Lee Chan and Su Toy having some romance together wondering if any future grandchildren will be able to see the circus, too.

== Cast ==
* Warner Oland as Charlie Chan
* Keye Luke as Charlies Number One Son, Lee Chan
* George Brasno as Colonel Tim, a midget performer at the circus
* Olive Brasno as Lady Tiny, Colonel Tims midget wife Francis Ford as John Gaines, half owner of the circus 
* Maxine Reiner as Marie Norman, an aerialist and fiancée of Joe Kinney John McGuire as Hal Blake
* Shirley Deane as Louise Norman, Maries sister  Paul Stanton as Joe Kinney, the other half owner
* J. Carrol Naish as Tom Holt
* Booth Howard as Dan Farrell
* Drue Leyton as Nellie Farrell
* Wade Boteler as Lieutenant Macy
* Toshia Mori | Shia Jung as Su Toy, a contortionist (love interest to Lee Chan)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 