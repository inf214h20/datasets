Anata e
 
{{Infobox film
| name           = Anata e
| image          = 
| alt            =
| caption        =
| director       = Yasuo Furuhata
| producer       =
| writer         =
| starring       =  Ken Takakura   Yuko Tanaka   Koichi Sato   Hideji Otaki   Kimiko Yo   Haruka Ayase
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
  is a 2012 Japanese film directed by Yasuo Furuhata. 

==Plot== Shimonoseki City, the Kanmon Bridge, Moji-ku, Kitakyūshū, and Sasebo, Nagasaki), recalling experiences with his wife along the way.  He also befriends and is assisted by numerous strangers.

==Cast==
* Ken Takakura as Eiji Kurashima
* Yuko Tanaka as Yoko, his wife
* Kitano Takeshi as Sugino Teruo, van-driving Japanese-language teacher
* Tadanobu Asano as cop
* Tsuyoshi Kusanagi as Tamiya Yuji, food-stall cook
* Kōichi Satō as "Nambara", food-stall cook and former fisherman
* Haruka Ayase as Naoko Hamazaki, young waitress and Nambaras daughter
* Kimiko Yo as Tamiko Hamazaki, her mother, eatery owner, and Nambaras widow/wife
* Hideji Ōtaki as Ohura Goro, elderly boat owner, and estranged father of Nambara

==Production==
Around the time of filming, Takakura was 80 years of age - much older than the probable working age of the prison carpentry instructor he was portraying (given that the common retirement age in Japan is usually 60). Hideji Ōtaki, who played the cranky old skipper, died a few months after the films release.

==References==
 

==External links==
*  

 

 
 
 


 