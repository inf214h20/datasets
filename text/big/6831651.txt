Attentato ai tre grandi
{{Infobox film
| name           = Attentato ai tre grandi
| image          = Descompos.jpg
| caption        = Original film poster
| writer         = Umberto Lenzi Ken Clark Horst Frank
| director       = Umberto Lenzi
| producer       = Alberto Grimaldi
| released       = 1 September 1967
| runtime        = 96 minutes
| language       =
| music          = Angelo Francesco Lavagnino
| budget         =
}}
 Roosevelt and de Gaulle Tuareg nomads, and French and American soldiers.

The film was released as Les Chiens verts du désert (The green desert dogs) in France, as Fünf gegen Casablanca (Five against Casablanca) in Germany and as Desert Commandos in the USA. Some commenters consider the film to be Umberto Lenzis best and have remarked the film is notable for having Germans as the heroes and with mixed multi-dimensional characters. 

==Cast==
 Ken Clark: Captain Fritz Schoeller
* Horst Frank: Lt. Roland Wolf 
* Jeanne Valérie: Faddja Hassen  Carlo Hinterman: Sgt. Erich Huber  Howard Ross: Willy Mainz 
* Franco Fantasia: Major Dalio 
* Hardy Reichelt: Corporal Hans Ludwig 
* Fabienne Dali: Simone 
* John Stacy: Sir Bryan  
* Tom Felleghy: Colonel Ross 
* Gianni Rizzo: Perrier 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 
 