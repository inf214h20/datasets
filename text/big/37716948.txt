Tokumei Sentai Go-Busters vs. Kaizoku Sentai Gokaiger: The Movie
 
{{Infobox film
| name           = Tokumei Sentai Go-Busters vs. Kaizoku Sentai Gokaiger: The Movie
| film name = {{Film name| kanji = 特命戦隊ゴーバスターズVS海賊戦隊ゴーカイジャー THE MOVIE
| romaji = Tokumei Sentai Gōbasutāzu Tai Kaizoku Sentai Gōkaijā Za Mūbī}}
| image          = Go-Busters vs Gokaiger.jpg
| caption        = Theatrical poster for Tokumei Sentai Go-Busters vs. Kaizoku Sentai Gokaiger: The Movie
| director       = Takayuki Shibasaki
| producer       = 
| writer         = Kento Shimoyama
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| distributor    = Toei Company
| released       =  
| runtime        = 74 minutes
| country        = Japan
| language       = Japanese
| italic title   = force
}}
  is the latest entry in the Super Sentai VS film series, which features the meeting of casts and characters of Tokumei Sentai Go-Busters and Kaizoku Sentai Gokaiger. The film was released on January 19, 2013. This is the first appearance of the 37th Super Sentai called Zyuden Sentai Kyoryuger.

==Plot== their attack on the Zangyack home world ended with them being defeated by Bacchus in conjunction with his alliance with the Vaglass.

After Navi reveals the five phantom Ranger Keys grant unlimited power to whoever possesses them, the Go-Busters receive a visit from Kaoru Shiba of the Shinkengers who brings them a scroll from Yoko that reveals she, Jay, and Gai ended up in 18th century Tokyo. Learning that Gokaigers are in different times in Europe and Gorisaki in Laurasia, the Go-Busters learn they can retrieve them by using GozyuDrills time travel ability on their Megazords within a 45 minute time limit. As Masato gets the green Ranger Key ahead of Aihm and Doc in England 1805, Blue Buster battles Gokai Blue and Gokai Yellow in 1557 Mediterran for the blue Ranger Key before they suddenly attack Bacchuss subordinate Waredonaier as he takes the blue ranger key. Confronting Captain Marvelous at the Dino Curry in 2005, taking their fight outside, Hiromu learns the Gokaigers sided with the Zangyack after their defeat to bide their time until they get the phantom Ranger Keys. However, Bacchus is on to the deception and steals the green Ranger Key from Masato, Ahim, and Doc once they returned to the present while his forces find the pink Ranger Key. Back in 18th century Tokyo, after they encounter Jerashid and get the yellow Ranger Key from him, Yokos group are found by the RH-03 after Usada retrieved Gorisaki. However, the RH-03 is attacked by Enter in Megazord Epsilion while Yokos group is attacked by the Zangyack with their allies coming to their aid. Unfortunately, besides losing the ranger Key to Bacchus, the GoBusters mecha lack Enetron needed to return to their time before Nick, Gorisaki and Usada offer their energy for the process. Though it succeeds, it resulted with the Buddyloidss anti-Metavirus program being damaged as they are rendered into mindless drones with no way of undoing the damage.
 Kyoryu Red who offers to hold off the Zangyack for them. Joined by his team mates, the Zyuden Sentai Kyoryuger team formally introduce themselves before defeating Waredonaier. Despite the Buddyloids state, the Go-Busters call in their Buster Machines to destroy the Black Galleon with Enter standing in their way in Megazord Omega. Explaining to him that they only fight those who get in their way as Earths protection entrusted to the Go-Busters, the Gokaigers use red Ranger Keys to overpower Bacchus before defeating him with a Gokai Legend Dream/Gokai Galleon Buster combo. Refusing to give up, Bacchus enlarges with Gokaioh and Gozyujin formed to fight him.

Though both giant battles are in the villains favor, the Go-Busters refusal to abandon their Buddyloids enable them to restore their partners personalities. As Go-Buster Lioh is formed, the Phantom Keys transform into Buddyloid-like forms that the Gokaigers realize is the Greater Power of the Go-Busters, representing the bond between them and their robotic friends, before the Ranger Keys transform into Megazord Keys. Using them, Go-Buster Ace becomes Gekitohja while Gokaioh becomes Daibouken, Buster Hercules becomes MagiKing, Gozyujin into Daizyujin, and Go-Buster Lioh into GaoKing to fight not only Bacchus and Megazord Omega, but also the various Megazord models Enter summons. The fight ultimately ends with Bacchuss death, and the destruction of both Megazord Omega and the Black Galleon. After the Gokaigers bid farewell to the Go-Busters to find another treasure, Marvelous commenting them to be a worthy Super Sentai team, the Buddyloids end up getting into another argument with their partners over what occurred while they were mindless.

==Cast==
* Hiromu Sakurada/Red Buster:  
* Ryuji Iwasaki/Blue Buster:  
* Yoko Usami/Yellow Buster:  
* Cheeda Nick:  
* Gorisaki Banana:  
* Usada Lettuce:  
* Masato Jin/Beet Buster:  
* Beet J. Stag/Stag Buster, Waredonaier:  
* Captain Marvelous/Gokai Red:  
* Joe Gibken/Gokai Blue:  
* Luka Millfy/Gokai Yellow:  
* Don "Doc" Dogoier/Gokai Green:  
* Ahim de Famille/Gokai Pink:  
* Gai Ikari/Gokai Silver:  
* Navi:  
* Takeshi Kuroki:  
* Toru Morishita:  
* Miho Nakamura:  
* Kaoru Shiba:  
* Jerashid:  
* Enter:  
* Escape:  
* Basco Ta Jolokia:  
* Bacchus Gill:  
* Kyoryu Red (Daigo Kiryu):  
* Kyoryu Black (Ian Yorkland):  
* Kyoryu Blue (Nobuharu Udo):  
* Kyoryu Green (Souji Rippukan):  
* Kyoryu Pink (Amy Yuuzuki):  
* Automatic Program Voice:  
* Mobilate Voice, Gokai Sabre Voice, Gokai Gun Voice, Gokai Cellular Voice, Gokai Spear Voice, Gokai Galleon Buster Voice:  
* GB Custom Visor Voice, Lio Blaster Voice:  
* Gaburevolver Voice:  
* Narration:  

==Theme song==
* 
**Artist:  

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 