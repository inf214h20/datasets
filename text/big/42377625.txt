Scary or Die
{{Infobox film
| name           = Scary or Die
| image          = Scary or Die DVD Cover.jpg
| alt            = 
| caption        = 
| director       = 
| producer       = Bob Badway, Michael Emanuel, Igor Meglic
| writer         = Bob Badway, Michael Emanuel
| starring       = Domiziano Arcangeli, Corbin Bleu, Bill Oberst Jr. 
| music          = Shawn K. Clement, Claude Foisy, Hanna Lim, Christopher Young
| cinematography = Bruce Douglas Johnson, Igor Meglic, James Lawrence Spencer, Byron Werner
| editing        = Masayoshi Matsuda
| studio         = Canal Street Films, Bleuman
| distributor    = Phase 4 Films
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Scary or Die is a 2012 American horror anthology film that was directed by Bob Badway, Michael Emanuel, and Igor Meglic.    The film was released on video on demand on May 1, 2012 and on DVD on September 11, 2012.  Initially titled Terror Bytes, the films name was later changed to coincide with a horror website by the same name that Emanuel ran with his co-director Igor Meglic and two other filmmakers. 

The anthology stars Domiziano Arcangeli, Corbin Bleu, and Bill Oberst Jr., and is composed of five interlocking stories set within the city of Los Angeles.

==Synopsis==
===The Crossing===
Buck (Bill Oberst Jr.) and his friends have made a past time of trolling the Mexico-United States border in search of anyone they believe to be an illegal immigrant. Upon catching someone, the group murders them and buries the body in the hopes of sending out a personal message to anyone else that wants to enter. However what they couldnt predict is that their victims might rise from the grave to take their revenge.

===TaeJungs Lament===
A lonely widower finds himself unable to cope with the loss of his wife and begins to follow around various women that resemble her. On one such occasion he ends up becoming a witness to a womans kidnapping. The man rescues her and in gratitude, the woman urges him to meet her one night. Unbeknownst to him, the woman and her friends are all vampires that are being hunted by Van Helsing himself.

===Re-Membered===
A dirty cop (Christopher Darga) has been asked to serve as a hitman for a mans murder, which he does. He dismembers the body and stows it in his trunk for later disposal, but is shocked when he begins to find evidence that his victim is somehow still alive. 

===Clowned===
When Emmett (Corbin Bleu), a wayward drug dealer, gets bitten by Fucko (Domiziano Arcangeli), a flesh-eating clown at a family members birthday party, the last thing he expects is for that bite to begin a horrifying transformation. As his transformation proceeds, Emmett is horrified to find that he has a newly acquired taste for human flesh.

===Lover Come Back===
A woman returns from the grave in order to seek revenge on the cheating husband that caused her death. As she moves towards her final task, she begins to recollect their relationship and how everything went wrong.

==Cast==
*Domiziano Arcangeli as Fucko
*Bill Oberst Jr. as Buck
*Corbin Bleu as Emmett
*Shannon Bobo as The Walking Woman
*Bob Bouchard as Fucko 2
*Andrew Caldwell as Bill Blotto
*Alexandra Choi as Min-ah
*Charles Rahi Chun as TaeJung
*Erik Contreras as Gonzalez Jr.
*Christopher Darga as Detective Franks
*Xavier Davis as Andy
*Elizabeth Di Prinzio as Kelly
*Hali Lula Hudson as Connie
*Azion Iemekeve as Van Helsing
*David Reivers as Gran Pere

==Reception==
Aint It Cool News gave Scary or Die a positive review, stating that "Reminiscent of old school films like Creepshow with its practical effects and unflinchingly wicked tales, this is one indie film worth seeing by as many folks as possible."  Dread Central gave a more mixed review and remarked that while they enjoyed the segments Clowned and Re-Membered, they found most of the segments disappointing and could really only recommend it for Clowned.  HorrorNews.net criticized the films segments, commenting that some would serve well as excellent student films but that "this is a commercial release, and the bar is higher." 

==References==
 

==External links==
*  
*  

 
 
 