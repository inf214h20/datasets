Milk Money (film)
{{Infobox film
| name           = Milk Money
| image          = Milk Money Poster.jpg
| caption        = Promotional poster
| director       = Richard Benjamin Kathleen Kennedy Frank Marshall
| writer         = John Mattson
| starring       = Melanie Griffith Ed Harris Michael Patrick Carter Brian Christopher Adam LaVorgna Malcolm McDowell
| music          = Michael Convertino David Watkin
| studio         = The Kennedy/Marshall Company
| distributor    = Paramount Pictures
| released       = August 31, 1994
| runtime        = 110 minutes
| language       = English
| country        = United States
| budget         = $20 million
| gross          = $18.1 million (domestic)
}}
Milk Money is a 1994 American romantic comedy film directed by Richard Benjamin and starring Melanie Griffith and Ed Harris. It is about three suburban 11-year-old boys who find themselves behind in "the battle of the sexes," believing they would regain the upper hand if they could just see a real, live naked lady.
 Cincinnati and Lebanon, OH|Lebanon.  The story is set in a fictitious 
Ohio suburb named "Middletown", outside of an unnamed city (for which Pittsburgh was used).  The screenplay was sold to Paramount Pictures by John Mattson in 1992 for $1.1 million, then a record for a romantic comedy spec script. 

==Plot==
Three boys &mdash; Brad, Frank and Kevin &mdash; go from their bedroom suburb of Middletown to the city, bringing money with hopes of seeing a naked woman. They find a bemused hooker named "V" willing to show her breasts for that amount. However, when the boys go to head home, their bikes have been stolen. Theyre broke and stuck in the city.

V speaks with her friend Cash and another hooker, Betty. Bag-man Cash has been skimming money that he sends to mob boss Waltzer, who in turn steals from his own boss, Jerry. V notices the boys outside in the rain and offers them a ride home.

After they arrive at Franks house, the car breaks down. With no other option, V accepts Franks offer to stay in his tree house. Tom, Franks father, is surprised to find a hooker in his sons tree house but offers to repair the car in a few days when he is free from his science classes at school. Frank tells his dad that V is a math tutor and that shes giving lessons to his friend Brad. Tom doesnt realize she is staying in the tree house.

Frank begins to romanticize her, hoping to get widowed Tom to become attracted. The boy tells her Tom has no problem with her "job," meaning the tutoring ruse, but V thinks he means her prostitution. 

V learns from television that Cash has been murdered by Waltzer. She phones Betty only to discover that Waltzer is looking for her - Cash told him that V stole the money. V realizes that he is overhearing the conversation and hangs up.

With the car still broken down, she gets Toms old bike from the garage and rushes to find him. He is on a field trip to the towns wetlands, undeveloped natural land that he is attempting to save from development. Tom is unable to repair the car any sooner but V realizes that she is probably safer in Middletown, since Waltzer doesnt know where she is.

At school, Frank flunks a biology test about sex education and must give the class an oral presentation. He decides to use V as a mannequin and through a ruse distracts his teacher long enough to draw a relatively accurate female reproductive system on her skin-colored suit. This leads to much comic gossip among the adults. Soon Tom finds out the truth about Vs vocation. He is angry, confused, and hurt and turns to a box of memorabilia that was his wifes before she died.

V explains herself to Tom, and their relationship grows. She reveals that her real name is Eve. She thought the name Eve was too biblical so she removed the “e”s.  Kevins father learns the truth about her as well, and in an attempt to purchase her services, unwittingly calls her home phone number. Waltzer learns from Betty about the trip to Middletown, thus finding out where V is hiding.

Tom and V begin a relationship, attending a school dance, then enjoying a night on the town. Waltzer shows up to spoil their fun. A chase ensues, with him finally being eliminated. Anxious about her status and afraid to return to her old job, V goes to Waltzers boss and relates how he has been cheating him. She asks to be forgotten by them. The older crime boss succumbs to her charms and tells her hell take care of things, that she doesnt need to be afraid any more.

V finds the stolen money in Cashs car and uses it to preserve the wetlands. To thank her, Tom names it after V and they carry on with their new relationship.

==Cast==
*Melanie Griffith as V
*Ed Harris as Tom Wheeler
*Michael Patrick Carter as Frank Wheeler
*Malcolm McDowell as Waltzer
*Anne Heche as Betty
*Philip Bosco as Jerry the Pope
*Casey Siemaszko as Cash
*Brian Christopher as Kevin Clean
*Adam LaVorgna as Brad
*Margaret Nagle as Mrs. Fetch
*Kevin Scannell as Mr. Clean
*Katie Powell as Mrs. Clean
Future Major League Baseball star Kevin Youkilis appears as a 14-year-old extra, and even has a line in the film. 

==Reception==
The film received mostly negative reviews. Siskel & Ebert gave Milk Money a "thumbs down" and speculated mockingly that it may have been made by Hollywood executives with an affinity for hookers and their desire to make films about them, for lack of knowing women in any other profession.  In print, Roger Ebert opted not for a conventional negative review, but to portray it as the result of a fictional conversation between two studio executives. 
 Worst Screenplay, The Flintstones.

== Release==
It was released on  .

*Subtitles: English

*Audio:
**ENGLISH: Dolby Digital 5.1  
**ENGLISH: Dolby Digital Surround  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 