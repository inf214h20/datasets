Marked Men (1919 film)
 
{{Infobox film
| name           = Marked Men
| image          = Marked Men (1919 film).jpg
| caption        = Theatrical poster to Marked Men (1919)
| director       = John Ford Pat Powers
| writer         = Peter B. Kyne H. Tipton Steck Harry Carey
| music          =
| cinematography = John W. Brown
| editing        = Frank Atkinson Frank Lawrence
| distributor    = Universal Studios
| released       =  
| runtime        = 50 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 Western film Harry Carey. It is a remake of the 1916 film The Three Godfathers, which also starred Carey. The film is considered to be a lost film.   

==Cast== Harry Carey as Cheyenne Harry
* Joe Harris
* Ted Brooks Charles Le Moyne
* J. Farrell MacDonald
* Winifred Westover
* David Kirby (uncredited)

==See also==
* Harry Carey filmography
* List of lost films

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 