Salvation Boulevard
{{Infobox film
| name           = Salvation Boulevard
| image          = Salvation boulevard poster.jpg
| caption        = Theatrical release poster
| director       = George Ratliff
| producer       = Mark Victor Peter Fruchtman Celine Rattray
| writer         = Cathy Schulman George Ratliff Larry Beinhart (novel)
| screenplay     = 
| story          = 
| based on       =  
| starring       = Pierce Brosnan Jennifer Connelly Ed Harris Greg Kinnear Marisa Tomei Jim Gaffigan
| music          = George S. Clinton
| cinematography = Tim Orr
| editing        = Michael LaHaie
| studio         = 
| distributor    = IFC Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $5.5 million
| gross          = $28,468
}}
Salvation Boulevard is a 2011 comedy-drama-thriller-action film with religious satire undertones. It is based on the novel of the same name by Larry Beinhart.

==Synopsis==
While discussing a new book idea, a modern-day evangelical Pastor, Dan Day (Pierce Brosnan|Brosnan), accidentally shoots atheist Dr. Paul Blaylock (Ed Harris|Harris) in the head. Pastor Day tries to make it look like a suicide, fearing the damage to his reputation if word were to get out. However, reborn church-goer and ex-deadhead Carl (Greg Kinnear|Kinnear) witnesses the act. Carl has to endure attacks by fellow church-goers at Days request (although under false pretense), his own familys skepticism about his story, and a Mexican crime lord who kidnaps him looking to blackmail both Carl and Pastor Day with footage of the accident.

==Plot==
Carl Vandermeer is a former Deadhead turned evangelical. He, his wife Gwen and his stepdaughter Angie attend a megachurch, the Church of the Third Millenium, run by the charismatic Pastor Dan Day, who is planning to expand his church into a large Christian community, "City on a Hill". At the start of the film, the Vandermeers attend a debate between Pastor Dan and Professor Paul Blaylock, an outspoken atheist. At the end of the debate, Pastor Dan is invited to Blaylocks college for a nightcap. He brings along Carl. At Blaylocks office, he reveals that he invited Pastor Dan over to propose that the two of them, both bestselling authors, collaborate on a book about their opposing worldviews and views on religion. During a discussion about morality without religion, Pastor Dan picks up an antique pistol and pulls the trigger on Blaylock, unaware that the gun is loaded and functional. The bullet hits Blaylock in the skull, apparently killing him. In a state of shock, Pastor Dan places the gun in Blaylocks hand to make it look like a suicide. He and Carl then drive home in silence, neither of them telling anyone what happened.

After getting a call from a stranger claiming to be his "friend", Pastor Dan approaches Carls neighbor, videographer Jerry Hobson, and appears to confess to what happened. At the sermon the next day, Pastor Dan informs the congregation that Blaylock survived his "suicide", the bullet was removed and he is in a coma. After the sermon, Jerry takes Carl aside and gets into his car. During the drive, Jerry turns out to have been told by Pastor Dan that Carl was the one who pulled the trigger and tried to cover it up. Jerry drives Carl to a quarry and tries to execute him. He is distracted when Angie, who was asleep in the backseat of the car, interrupts the act, allowing Carl to knock Jim out with a rock. Carl then meets with Gwen and her father, Jim Hunt, and tells them what happened with Pastor Dan and Jerry, but neither believes him, thinking it was a dream or an "acid flashback". Realizing they will tell Pastor Dan, Carl goes to Blaylocks college, where he runs into Honey Foster, a friend from his Deadhead days who became campus security and met him at the debate. He asks her to turn in the footage from a security camera that would have seen him and Pastor Day enter, buts finds that the footage has been altered to erase them - apparently by Jerry, who worked at the campus as a video technician.

Meeting with Gwen and Jim, Pastor Day convinces them that Carl may be unstable and will react violently if he is caught by the police. Meanwhile, since most of the police is loyal to Pastor Dan, Honey lets Carl hide at her house until things blow over. While they are out driving, Carls car is stopped by a group of armed men who kidnap him. He wakes up in the house of Jorge Guzman de Vaca, a Mexican crime lord who claims he is in Mexico. Guzman shows that he has a secret recording of Pastor Dan shooting Professor Blaylock. He makes Carl write down and sign two confessions. One tells the truth of what happened, but in the other Carl backs up Pastor Dans story. Guzman, who has a large business in construction, plans to use the recording to blackmail Pastor Dan into giving him the contract to build City on a Hill, promising to keep Carl in the house until the project is done.
 stun gun, they bungle it and get set on fire themselves, allowing Carl to escape. Meanwhile, Pastor Dan meets with Guzman at the construction site of City on a Hill. Guzman tries to play the DVD of the shooting, but instead plays a sex tape he made with his wife. Offended, Pastor Dan starts hitting him, but is stopped when Guzman stabs him in the abdomen and leaves him in the trunk of a car. After a while, Carl arrives by foot and finds the bleeding Pastor Dan, who is hallucinating because of the blood loss and thinks he is at the Pearly Gates, and calls for help on a cell phone. Thinking Carl is an angel, Pastor Dan asks him if he is going to heaven. Out of sympathy, Carl tells him he is. As Carl waits for help to arrive, a bolt of lightning strikes the ground behind him, severing his handcuffs.

The credits reveal that Pastor Day survived, spent two years in jail, where he founded a new ministry inspired by his near-death experience, and became a real-estate broker in Arizona. Gwen divorced Carl citing irreconcilable differences, though Carl still has a good relationship with Angie. Jim started his own private investigation firm and went back to his old Episcopal church. Professor Blaylock continued his teachings against religion. Carl, on the other hand, left the church, began a relationship with Honey and became an "ex-ex-Deadhead."

==Cast==
*Pierce Brosnan as Dan Day, the powerful leader of a church
*Greg Kinnear as Carl Vandermeer, an ex-Deadhead and member of Pastor Dans church
*Jennifer Connelly as Gwen Vandermeer, Carls wife
*Marisa Tomei as Honey Foster, a pot-smoking campus security guard
*Ed Harris as Dr. Paul Blaylock, an atheist professor who doesnt buy Dans theories
*Isabelle Fuhrman as Angie Vandermeer, Carls stepdaughter
*Jim Gaffigan as Jerry Hobson, Pastor Days cameraman
*Ciarán Hinds as Jim Hunt, Gwens ex-Marine father
*Yul Vazquez as Jorge Guzman De Vaca, a Mexican crime lord

==Production== Ann Arbor Dearborn in April and June 2010. Salvation Boulevard premiered at the Sundance Film Festival in January 2011, and was given a limited release theatrically by IFC Films on July 15, 2011 in the United States.

==Reception==
The film was only released in a small number of theaters and quickly released to video on demand. It was released on DVD on September 18, 2012. It received poor reviews with the New York Times saying "there is the inkling of a strong, interesting idea here, about how some versions of modern religion are predicated on the systematic denial of reality, but Salvation Boulevard is itself too loosely tethered to the actual world to make the point with the necessary vigor or acuity".  Rotten Tomatoes reported only a 21% favorable rating. 

==References==
 

==External links==
*  
* http://www.mrmovietimes.com/movie-news/salvation-boulevard/

 
 
 
 
 
 
 
 