Nakhangal (2013 film)
{{Infobox film
| name = Nakhangal
| image =
| caption =
| director = Suresh Krisshnan 
| writer =  R. Prem Nath
| producer = G. P. Vijayakumar
| starring = Rakendu Madan Mohan Megha Vivek Gopan
| music = M. G. Sreekumar
| cinematography = Bijoy Varghese
| editing = M. S. Ayyappan Nair
| studio = Seven Arts Productions
| distributor = Seven Arts Productions
| released =  
| runtime = 140 minutes
| country = India
| language = Malayalam
| budget =
| gross =
}}
Nakhangal is a Malayalam crime thriller film directed by Suresh Krisshnan, produced by G. P. Vijayakumar and written by R. Prem Nath.  It casts Rakendu, Madan Mohan and Megha in lead roles. 

==Plot==
Nakhangal tells the tale of three youngsters sharing a rented house. The house belongs to an NRI, whose friend Jeevan (Arun) arranges for the rental. Giri (Rakendu), a driver in a courier company, Jackson (Madan Mohan), a tea estate accountant and Sherin (Megha), a nurse are the youngsters who stay together. A fourth person enters the home and their lives when the rent is increased. He introduces himself as a writer with the pen name Manjila. But is fishy about Manjila who carries a gun with him. The plot thickens when he is found dead from drug overdose and Giri finds a bag of money in the room. There are takers for the money who wants it returned to them. But the three friends decide to keep it for themselves. Meanwhile Sherin is being persuaded by Vinodh, to accept his love for her. The money orchestrates the rest of the story with broken trust, gruesome murders and a suspense ending.

==Cast==
* Rakendu as Giri
* Madan Mohan as Jackson
* Megha 
* Arun Benny
* Santhosh Sleeba as Vinod Kumar
* Nandhu Augustine as a neighbor
* Vivek Gopan as Gunda 1
* Sumesh as Gunda 2
* Poojappura Ravi
* Solomon

==References==
 

 
 
 


 