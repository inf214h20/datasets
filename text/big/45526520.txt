Love Goes Up and Down
{{Infobox film
| name=Love Goes Up and Down
| image= 
| caption= 
| director=Rolf Husberg 
| producer=
| writer=Sven Björkman, Hasse Ekman 
| starring=Sture Lagerwall Eva Dahlbeck Thor Modéen Agneta Lagerfeldt Sigge Fürst Hjördis Petterson John Botvid
| music=Rune Molander, Jerry Högstedt  
| released=1946
| runtime=91 min
| country=Sweden Swedish
}}
 Swedish comedy comedy film directed by Rolf Husberg. 

==Plot summary==
Sixten is a screenwriter who reluctantly travels to a ski resort in Åre to prepare a love story in a ski environment for a film.  tabloid newspaper, Vivi Bostrom, calls him to ask him about his plans. They end up immediately quarrelling. She then takes the train to Åre and does her best to get back at him. Revenge will be sweet, and snowy. 

==Cast==
*Sture Lagerwall as Sixten Kennebeck, screenwriter
*Eva Dahlbeck as Vivi Boström, journalist at Kvällsexpressen
*Thor Modéen as Sture Nylén, Wholesaler 
*Agneta Lagerfeldt as Anne-Sofie 
*Sigge Fürst as Larsson
*Hjördis Petterson as "Scorpion"
*John Botvid as Fingal Andersson 
*Bullan Weijden as Agda Nylén
*Douglas Håge as District Police Superintendent 
*Holger Höglund as Holmström, Reception Clerk 
*Magnus Kesster as Film Producer
*Kenne Fant as Actor in Studio
*Mimi Nelson as Actress in Studio
*Börje Mellvig as Director in Studio
*Nils Hultgren as Film Producer
*Stig Johanson as Printer at Kvällsexpressen
*Albin Erlandzon as Printer at Kvällspressen
*Leif Hedenberg as Hotel Porter
*Karin Miller as Hotel Waitress
*Gunnar Hedberg as Hotel Guest

==External links==
* 

 
 

 
 
 
 