The Berliner (film)
The Berliner (original title Berliner Ballade, also known as The Ballad of Berlin) was a 1948 motion picture, adapted by Günter Neumann from his cabaret, directed by Robert A. Stemmle, and starring Gert Fröbe in his first leading role.    It offers a satirical portrayal of life in Berlin in the aftermath of World War II.

==Plot==
The film has a framing narrative set in 2050 where viewers are offered the chance to look back at "The Ancients", which introduces the main narrative set in 1948.  The film reflects the struggles of Otto Normalverbraucher (Otto Average-Consumer, played by Fröbe), a former German soldier returning to civilian life in Berlin after World War Two.  After many travails, struggling to find food, shelter, and work, he eventually falls in love and ends up happily with his dream woman. 

==Production== Soviet blockade, Joseph Burstyn Inc. distributed the film in the U.S.

==Critical reception==
The Darmstädter Echo praised it for its lack of spite and viciousness and its humor and humanity.  Angelica Fenner compares the film to Bertolt Brecht with devices such as the omniscient narrator, prototypical characters, and satirical tone.  Sabine Hake points out that although within the genre of post-war Trümmerfilme (rubble film) it offers a refreshing change from the majority of those films through its use of satirical humor.  However, Stephen Brockmann has criticised the film for portraying an optimistic message about the survival of the human spirit after World War II while ignoring the causes of the war. 

==Awards==
It was nominated for a BAFTA for Best Film from any Source in the 1950 ceremony, when it was beaten by Bicycle Thieves.   It won an International Prize at the 10th Venice International Film Festival in 1949. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 