Aatank
 
{{Infobox film
| name           =Aatank
| image          =Aatank poster.jpg
| image_size     = 
| caption        = 
| director       =
| producer       =
| writer         =
| starring       =Dharmendra Hema Malini Ravi Kissen Amjad Khan Vinod Mehra
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       = 09 February 1996
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Aatank  is a 1996 Bollywood film starring Dharmendra, Vinod Mehra, Hema Malini, Amjad Khan, Ravi Kissen and Nafisa Ali. It is inspired by the Hollywood film Jaws (film)|Jaws as it has a sub-plot with a killer shark. The film began production in the mid 70s and was delayed for several years and released in 1996. By the time of its release, two of its cast members Vinod Mehra and Amjad Khan had died.

==Plot==
Jesu and Peter are childhood friends who live in a coastal village in India and depend on fishing as their livelihood. The community is oppressed by a powerful gangster named Alphonso. Jesu is an orphan, while Peter is brought up by his aunt and uncle, after the death of his mother. The community is all thrilled when Phillips finds black pearls off the coast. But then so does Alphonso, who asks his divers to get all the pearls, thus disturbing the ocean. Peter meets with Suzy DSilva and they fall in love with each other, and get married. While enjoying a quiet swim on the sea-shore, Suzy disappears. A search is carried out, and a number of human body parts are recovered. With shock and horror, this community finds that their livelihood is being threatened by a gigantic, virtually indestructible man-eating shark.

==External links==
*  

 
 
 
 
 


 