The Saphead
 
{{Infobox film
| name           = The Saphead
| image	         = The Saphead FilmPoster.jpeg
| caption        = Film poster
| director       = Herbert Blaché Winchell Smith
| producer       = John Golden Marcus Loew Winchell Smith
| writer         = Buster Keaton Bronson Howard Victor Mapes June Mathis Winchell Smith
| starring       = Buster Keaton Beulah Booker
| cinematography = Harold Wenstrom
| editing        = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}

The Saphead is a 1920 American comedy film featuring Buster Keaton.    It was the actors first starring role, on the recommendation of Douglas Fairbanks, in a full-length feature and the film that launched his career.
 Bronson Howards play The Henrietta and the novel The New Henrietta by Victor Mapes and Winchell Smith, which was meant to be an adaption of Howards play.

==Cast==
* Buster Keaton as Bertie The Lamb Van Alstyne
* Beulah Booker as Agnes Gates
* Edward Connelly as Musgrave Edward Jobson as Rev. Murray Hilton
* Edward Alexander as Watson Flint
* Odette Taylor as Mrs. Cornelia Opdyke
* Carol Holloway as Rose Turner
* Irving Cummings as Mark Turner
* Jack Livingston as Dr. George Wainright
* William H. Crane as Nicholas Van Alstyne
* Katherine Albert as Hattie (uncredited)
* Henry Clauss as Valet (uncredited)
* Alfred Hollingsworth as Hathaway (uncredited)
* Helen Holte as Henrietta Reynolds (uncredited)
* Jeffrey Williams as Hutchins (uncredited)

==Plot== New York, but he is very disappointed in the behavior of his son, Bertie, who stays out all night gambling and partying, and who seems to show no talent or interest in work. In fact, Bertie is feigning this behavior because he believes it will help to impress the girl of his dreams, his adopted sister Agnes. Unfortunately, it helps him to do nothing more than get disowned by his father.
 mistress named illegitimate child with her. When Henrietta dies after a long illness, a letter is sent to him informing him about the present circumstances. Mark manages to claim the letter is actually Berties, breaking Agnes heart and ensuring Van Alstyne never wants to speak to his son again.

Soon after, when Van Alstyne goes away on business he leaves Mark in charge of running the familys finances, but Mark plots to claim the family fortunes himself by selling off all their shares of stock. Bertie inadvertently saves the day by buying back all of the stock without realizing what he is doing. When Van Alstyne sees what has happened he forgives Bertie and allows him to marry Agnes. Mark, meanwhile, conveniently dies of a heart attack when he realizes that his scheme has failed. The film ends a year later, with the birth of Bertie and Agnes twin children.

==Production==
Douglas Fairbanks had played the role of Bertie in the original stage production, but was already committed to his own film project when he was approached about the film adaptation.  Instead, Fairbanks recommended Keaton for the role instead with his confident reassurances that the comedian would be appropriate. 

==Reception==
Buster Keatons performance in the film was singled out as having an unusual stillness for his character that gave him a distinctive presence on screen. For instance, Variety (magazine)|Variety noted "His quiet work in this picture is a revelation." As such, Keatons work on such a prestigious film gained him credibility as an actor. 

==Gallery==

 
File:Buster Keaton Saphead Poster without Buster Keaton.JPG|The Saphead   Alternate Poster (1920) 
File:Buster Keaton in The Saphead by Winchell Smith 1920.png|Wids Film Daily    (1920) 
 

==See also==
* Buster Keaton filmography

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 