Kadathanadan Ambadi
 
{{Infobox film
| name     = Kadathanadan Ambadi
| director = Priyadarshan
| producer = Sajan Vargeese
| screenplay = Cochin Haneefa P. K. Sarangapani (Dialogues)
| starring = Mohanlal Prem Nazir Cochin Haneefa
| released = April 14, 1990
| runtime  = 149 minutes
| country  = India
| language = Malayalam
}}
 1990 Cinema Indian feature directed by Raadhu and Prem Nazir. The film was in production from 1985 and was stalled due to financial crisis faced by the producer Sajan Vargeese. The film was released after the death of Prem Nazir. 

== Plot == Thacholi Manikoth Othenan Kuruppus death and retrieve the sacred sword Bhadravaal and Sacred Thread, which were missing since the death of Thacholi Othenan.

Kadathanadan Ambadi along with his Mentor-Marital arts Guru sets out searching mysterious disappearance of the sword. They suspect a Muslim Manager of their household who went missing since the day. While searching, they reached Kingdom, whose ruler was a mysterious Black Magician. The Magician Ruler used to send his eagle to search for eachs day beautiful lady, to be sacrificed each day for strengthening the King. In that process, the eagle settled s house whose ladys marriage was fixed on the day. The soldiers dragged the girl and brought to sacrifice pit, only to saved by Ambadi and killed the King. It later found that, the girl and her brother were sons of . Moyideen narrates the story, who within the Thacholi family plotted against to stage a coup and get hold of the sword, making thief, who was later sold as slave to Tulunads King. The King also holds the key to secret magical cave where the sword was kept. Ambadi sets to Tulunadu, where he lures the princess and get to know about whereabouts of the Key. Ambadi fights against King and his soldiers and grabbed the key from his necklace and proceeds to Magical Cave. He had to fight against Demoness who guards the sword as well as a Water Demoness.

After getting back the Sacred Sword - "Bhadravaal" he returns to Kadathanadu with his Guru, Payyappilly Chanthu. But they were not aware of the fact that his fathers sister- Unichala (Sukumaari), her two children (Ganesh Kumar and an unknown actress) and Moosakkuutti (Srinivasan) were captured by Kathiroor Chandrappan ( Kochin Haneefa). Payyappilly Chanthus ( Prem Nazeer) daughter Kunjilakshmi feels betrayed as she knew that Ambadi was not alone but with a Princess.

==High Court Order== Navodaya release, Supreme Court judgement of 15, February 1989, SLP (CRL) 1127-28/88 & 1148-49/88 in the division bench of Honourable chief justice R.S.Pathak and Honourable S.Natarajan supervised by the Honourable courts commissioners Adv. Siby Mathew & Adv. Koshy George. 

==Snippets== Navodaya productions to take care of the release and give report to the Court.

==Box Office==
The film then collected a record Rs.35 lakhs gross in the first week itself. But the film dropped collections due to the stale look of the film as it was made in 1985 - 86 season. In 1990, Director Priyadarshan canned a fight scene between Mohanlal and Disco Shanti in a cave, was much appreciated for its technical brilliance.

=== Cast ===
*Prem Nazir - Payyappilly Chanthu Gurukkal
*Mohanlal - Kadathanadan Ambadi Swapna  Raadhu
*Sreenivasan Sreenivasan - Moosakkutty
*Jagathy Sreekumar - Karkodakan
*Sukumari - Unnicharu
*Captain Raju - Maharajah Kavillya
*Cochin Haneefa - Kathiroor Chandrappan
*Kuttyedathi Vilasini Santhosh
*Kunjandi Priya
*K. P. A. C. Sunny
*Disco Shanti

==Dubbing==
Malayalam actor Shammi Thilakan dubbed for 20 characters including late Prem Nazir.

==References==
 

== External links ==
*  

 

 
 
 
 