A Life in the Death of Joe Meek
{{Infobox film
| name           =A Life in the Death of Joe Meek
| image          =
| image_size     =
| caption        =
| director       =Howard S. Berger and Susan Stahman
| producer       =Howard S. Berger and Susan Stahman
| writer         =
| narrator       =
| starring       =
| music          =Joe Meek and others
| cinematography =
| editing        =
| distributor    =
| released       = 
| runtime        = 
| country        =United States
| language       =English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

A Life in the Death of Joe Meek is an independent American documentary about the British record producer Joe Meek, made by Howard S. Berger and Susan Stahman.
==Plot==
Joe Meek was one of Britains premier independent record producers of the late fifties and early sixties, renowned for his pioneering recording techniques and for the futuristic sound of the records he produced, but notorious for his eccentric personality. His biggest success was the production of The Tornados 1962 worldwide #1 hit "Telstar (song)|Telstar". After a long struggle with debt, paranoia and depression, he killed his landlady Violet Shenton and shot himself on 3 February 1967.

The documentary was shown as a work-in-progress on the opening night of the 2008 Sensoria Music & Film Festival in Sheffield, on 12 April 2008.  Later in 2008 it was shown at the Cambridge Film Festival  and the Raindance Film Festival in London.  A North American premiere of the film opened the Chattanooga Film Festival on 3 April 2014. 

The documentary contains over 60 interviews with Meeks family, close friends, associates, musicians and pop culture movers and shakers.

==Interviewees==
 
*Jimmy Page Steve Howe
*Keith Strickland
*Edwyn Collins
*Alex Kapranos
*Huw Bunford (Super Furry Animals)
*Marc Evans
*Jake Arnott
*Mike Stax (Ugly Things) Liam Watson
*Humphrey Lyttelton The Outlaws)
*Simon Napier-Bell
*Big Jim Sullivan
*John Leyton Dave Adams
*Joy Adams
*Doug Collins (The Blue Men)
*Dave Golding (The Blue Men)
*Clem Cattini (The Tornados)
*Roger LaVern (The Tornados)
*Pete Holder (The Tornados)
*Roger Holder (The Tornados)
*Robb Huxley (The Tornados)
*Dave Watts (The Tornados)
*Dennis DEll (The Honeycombs)
*Honey Lantree	(The Honeycombs)
*John Lantree (The Honeycombs) Bobby Graham The Outlaws) The Outlaws) The Outlaws)
*Gary Leport (The Moontrekkers)
*Ritchie Routledge (The Cryin Shames) The Saints)
*Guy Fletcher (songwriter)
*Ted Fletcher Mike Berry
*Keith Grant
*Adrian Kerridge
*Jimmy Lock
*Allen Stagg
*Tony Dangerfield
*David John
*Charles Blackwell
*Jason Eddie
*Robbie Duke aka Patrick Pink
*Kim Pavey
*John Repsch
*Barry Cleveland
*John Cavanaugh
*John Beecher
*Mark Newson
*John OKill
*Eric Meek
*Marlene Meek
*Sandra Meek-Williams
 

==References==
 

==External links==
*  
*  Retrieved 2012-08-11
*   Retrieved 20014-05-20

 
 
 
 
 
 
 
 
 
 


 