Game of Death II
 
 
 
{{Infobox film
| name           = Tower of Death
| image          = GameofDeath2FOXDVDcover.jpg
| caption        = UK cover for the H.K.L DVD containing the original theatrical release.
| film name = {{Film name| traditional    = 死亡塔
 | simplified     = 死亡塔
 | pinyin         = Sǐwáng Tǎ
 | jyutping       = Sei2 Mong4 Taap3}}
| director       = See-Yuen Ng 
| producer       = Raymond Chow
| writer         = Ting Chak-luen Ho Ting-sing Tong Lung Huong Cheng Li Roy Chiao To Wai Wo Lee Hoi San
| music          = Frankie Chan
| cinematography = Leung Hei-man Cheung Hoi Danny Lee Yau-tong Hoh Tin-shing
| editing        = Peter Cheung Golden Harvest
| released       =  
| runtime        = 86 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$1,950,391
}} Tong Lung, Huong Cheng Li and Roy Horan. This film was marketed as a sequel to Bruce Lees last and only partially completed film Game of Death. Bruce Lee died some years before the production of Game of Death II and most of his scenes are taken from Lees older films; mostly from Enter the Dragon.  Aside from the International English dub giving the "Bruce Lee" character the name "Billy Lo", this movie would seem to have no connection with Robert Clouses film. 

Bruce Lee, famed martial arts star is murdered. His brother, Bobby goes to Korea, and in an underground pagoda finds his brothers killers and exacts revenge. Huong Cheng Tong Lung), who is studying with Billys former teacher, and leaves him a book on Jeet Kune Do. Chin is soon killed, and Billy goes to Japan to find his stepdaughter, May. May tells him that Chin had visited just before his death, and left a film for her. They are suddenly attacked, but Billy manages to escape with the film.

A few days later Billy attends Chins funeral, where he is turned away from viewing the body. A helicopter arrives during the burial and steals the coffin away. Trying to prevent the theft, Billy is carried up with the casket but falls to his death. Bobby Lo is told of Billys death by their father, who tells him to find a man named Sherman Lan and avenge his brother. Sherman gives him the film, which shows Chin Ku at the Palace of Death. The Palace of Death is run by a crazed martial arts expert by the name of Lewis (Roy Horan). Any challenger who fails to defeat Lewis is fed to his pack of lions. Bobby decides to meet Lewis, who is impressed with Bobbys abilities. While investigating the Palace, Bobby is attacked by a masked man. Then, He informs Lewis that someone is trying to kill him. Later that night, a woman is sent to Bobbys room to seduce and assassinate him. When she fails, one of Lewis lions attacks Bobby. During the fight, the masked man appears and kills Lewis.

Suspecting Lewis valet, Bobby seeks him out at the Fan Yu temple, where the underground Tower of Death is rumored to be. After defeating the valet, Bobby spies the secret entrance into the tower. Battling his way through the tower he eventually confronts the operator, Chin Ku. Chin is actually the head of a global drug trafficking organization and staged his own death to throw off Interpol investigators. He tried to frame Lewis for his death and arranged for the coffin to be stolen to prevent it from being searched. Realizing the only way to defeat Chins sword skills is with Billys Jeet Kune Do. Bobby manages to kill Chin and stop his drug operation.

==Deleted scene==
Deleted scenes are available on DVD in both the International and English version as a special feature in Hong Kong Legends.
One particular scene in the Chinese version of Game of Death, directed by Sammo Hung, was intended for his Asian audiences. In the Chinese version, Hung replaced the action scene in the opera house with another scene from the glasshouse at night featuring Casanova Wong in his karate clothes. The new fight scene, particularly the flips between Tong Lung and Yuen Biao, is reminiscent of Bruce Lees fighting style. 
      

==Cast==
* Bruce Lee as Lee Chen-chiang (李振強) / Billy Lo (盧比利) in English dub (Stock footage)	 Tong Lung as Lee Chen-kwok (李振國) / Bobby Lo (盧博比) in English dub (Billy Lo doubling for Bruce Lee) Huong Cheng Li as Chin Ku
* Roy Horan as Lewis
* Roy Chiao as The Abbot
* Ho Lee Yan as Old Man
* Casanova Wong as the Korean Martial Artist
* To Wai Wo as Lewis Servant	
* Lee Hoi San as Bald monk Mars as Guard in the cave	t	
* Miranda Austin as Angel
* Tiger Yang as Strongman in Leopard suit	
* Yuen Biao as Blue Staff Monk
* Bolo Yeung as the Guard

==Soundtracks==
* Dancer - Performed by Gino Soccio (Only in Cantonese/Mandarin versions)
* Jealousy - Performed by Amii Stewart (Only in Korean theatrical cut)

==DVD releases==

Universe (Hong Kong)

* Aspect Ratio: Widescreen (2:35:1) letterboxed
* Sound: Cantonese (Dolby Digital 5.1), Mandarin (Dolby Digital 5.1)
* Subtitles: Traditional, Simplified Chinese, English, Japanese, Indonesian, Malaysian, Thai, Korean, Vietnamese
* Supplements: Stars files, Trailer, Trailers for The Big Boss, Enter the Dragon, Game of Death and Legacy of Rage
* All regions, NTSC

Fortune Star – Bruce Lee Ultimate DVD Collection (Hong Kong)

* Released: 29 April 2004
* Aspect Ratio: Widescreen (2:35:1) anamorphic
* Sound: English (DTS 5.1), English (Dolby Digital 5.1), English (2.0 Mono)
* Subtitles: Traditional, Simplified Chinese, English
* Supplements: Original trailer, New trailer, Celebrity interviews, Still photos, Slideshow of photos, Game of Death outtakes, Unseen footage, Enter the Dragon alternate opening credits, 32-page booklet
* Region 3, NTSC

Fox (America)

* Released: 25 May 2005
* Aspect Ratio: Widescreen (2:35:1) anamorphic
* Sound: English (DTS 5.1), English (Dolby Digital 5.1)
* Subtitles: English
* Supplements: Original trailer, New trailer, Bonus trailers
* Region 1, NTSC

Fox – Bruce Lee Ultimate Collection (America)

* Released: 18 October 2005
* Aspect Ratio: Widescreen (2:35:1) anamorphic
* Sound: English (DTS 5.1), English (Dolby Digital 5.1), English (Dolby Digital 2.0 Mono)
* Subtitles: English
* Supplements: Original trailer, New trailer, Game of Death outtakes, Bonus trailers, Still photos, Slideshow of photos
* Region 1, NTSC

Hong Kong Legends (United Kingdom)

* Released: 5 November 2001
* Aspect Ratio: Widescreen (2:35:1) anamorphic
* Sound: Cantonese (Dolby Digital 2.0 Surround), English (Dolby Digital 2.0 Surround)
* Subtitles: English, Dutch
* Supplements: Commentary by Bey Logan and Roy Horan, Original trailer, HKL promo trailer, Biography showcases, Bonus trailers, Interviews with actors Casanova Wong and Roy Horan, Deleted scenes (in English)
* Regions 2/4, PAL

==See also==
* Bruce Lee
* Bruceploitation
* Game of Death
* List of Hong Kong films

==References==
 

==External links==

*  

 

 

 
 
 
 
 
 
 
 
 
 