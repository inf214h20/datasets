Kamen Rider Den-O: I'm Born!
{{Infobox film 
| name           = Kamen Rider Den-O: Im Born!
| film name      = {{Film name
| kanji          = 劇場版 仮面ライダー電王 俺、誕生!
| romaji         = Gekijōban Kamen Raidā Denō Ore, Tanjō!}}
| image          = DenGeki Poster.jpg
| caption        = Film poster for both Kamen Rider Den-O: I&#39;m Born! and  
| director       = Takao Nagaishi
| writer         = Yasuko Kobayashi
| producer       =   
| starring       =  
| music          = Toshihiko Sahashi
| cinematography = Masao Inokuma
| editing        = Naoki Osada Toei
| Toei Co. Ltd 
| released       =  
| runtime        = 69 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
  is the theatrical film adaptation of the Kamen Rider Den-O TV series directed by Takao Nagaishi and written by Yasuko Kobayashi. The catchphrase for the movie is  . The movie takes place between episodes 27 and 28 of the series, featuring the DenLiner and its passengers travel as far back as the Edo period of Japan.

The film is produced by Ishimori Productions and  , both of which premiered on August 4, 2007. The films title is translated into English as both Masked Rider Den-O The Movie: Im Born! and Ill be born! on the initial collectors pack DVD release.

During its first week at the theaters, the movie came in 4th place and was the highest selling Japanese production of the week. 

==Plot== Ryotaro pursues Gaoh who they aided earlier. Momotaros arrives after escaping the DenLiner, but he is only able to possess Kotaro while Siegs special talents allow him to still possess Ryotaro. Yuto shows up, taking everyone on the ZeroLiner to pursue Gaoh, with Kotaro joining them. Gaoh uses the DenLiner to damage the ZeroLiner before transforming into his Kamen Rider Form to send the gang into the Mesozoic Era before they are able to reach the Edo Period. While Yuto fixes the ZeroLiner, Ryotaro and Kyotaro talk, revealing that the event Ryotaro most regrets is losing the only remaining photograph of his parents, who died when he was young, because he cannot remember their faces without the photograph.
 the past Ryotaros he borrowed for the "Climax Scene".  After the long battle, the Den-Os and Zeronos emerge victorious as Gaohs followers are wiped out and the GaohLiner is destroyed by the DenLiner and ZeroLiner. Surviving the GaohLiners destruction, Gaoh dies in a final duel with Den-O Sword Form, reduced to sand. On the return trip to his own time, the DenLiner comes across Ryotaros old house, allowing Ryotaro and Kotaro to see the faces of their parents. Sieg and Kotaro also return to their respective timelines as well, resuming their places in history. When Ryotaro returns to the present, he finds a drawing of his parents in the picture frame where the original photo once was.   

==Characters==
===Characters from the TV series===
* : Once again gets access to  . It was lost when Sieg had to be sent back in time to 1997, but he is retrieved to aid Ryotaro in the Tarōs absence.     
* 
* 
* : In the trailer, he reveals the existence of Gods Line, a railway in time that allows travelers with a Master Pass to venture on it to go to any time period.
* 

===Movie-exclusive characters===
* : He is a king of bandits from the Warring States era and leads a troop of Imagin with which he plans on using to take over the DenLiner. On his journey, he also obtains Yukimuras help in unearthing the GaohLiner. His main goal is to take over all of time and space.   
* : A Japanese historical figure, the daughter of shogun Tokugawa Hidetada. Ninja Corps.
* : Yukimuras subordinate, sent after Ryotaro on Yukimuras orders from Gaoh.
* : Yukimuras subordinate, he escorts Senhime to Toyotomi Hideyoris place.
* : Ryotaros younger self at age 11 that joins the DenLiner group in fighting Gaoh and his Imagin underlings. The Tarōs can also possess him due to their contracts with Ryotaro which extend into the past. Momotaros does so and allows Kotaro to become Mini Den-O Sword Form.    Kotaros transformation earns him the title of being the youngest Rider in history at age 11.
* : The father of Airi and Ryotaro. 33 years old.
* : The mother of Airi and Ryotaro. 31 years old.

==Imagin==
Since Ryotaro succumbed to temporary memory loss, the Tarōs obtained physical form temporarily. They do so to fight off the Ninja Corps and the Sanada Ten Braves.

===Imagin from the TV series===
* 
* 
* 
* 
* 
* : An Imagin who was on DenLiner for a while before he was taken to June 1, 1997. When Ryotaro and Hana were stranded in 2000, they meet up with Sieg who aids Ryotaro in the Tarōs absence as repayment for preventing his disappearance. He once again provides Ryotaro with the ability to become Wing Form.

===Gaohs subordinates===
*  who is based on the legend of  . He fights Den-O Sword Form and is defeated in episode 27, serving as a distraction for Gaoh.
*  who is based on the legend of  . He fights Den-O Wing Form in the Edo period.
*  who is based on the legend of  . He fights Den-O Rod Form on December 26, 1988.
*  who is based on   as the axolotl salamander. He fights Den-O Gun Form on December 26, 1988.
*  who is based on the legend of "The Mystery of the Newt". He fights Den-O Ax Form on December 26, 1988.

==Momotaross Summer Vacation== short film that debuted between the Gekiranger movie and Im Born!. Using live action photographs, 3D computer graphics, and traditional animation (all of which is termed a   by the production team), it has Momotaros gloating of his strength until Sha-Fu dared him to proved it by swimming. After being made fun of by Urataros and Ryutaros, and bashed by Kintaros, Momotaros is talked into believing he can swim by Deneb. But as the "mind over matter" influence wears off, the Imagin and Sha-Fu tell the audience the movies coming up soon.  

==Cast==
* :  
* :  
* :  
* :  
* :  
* :   of Run&Gun
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  

===Voice actors===
* :  
* :  
* :  
* :  
* :  
* :  
* :       
* :    
* :   
* :   

==Songs==
;Theme song
* 
**Lyrics & Composition: Shogo (musician)|Shogo.k
**Arrangement: Seiji Kameda
**Artist: 175R 

;Insert song
*"Climax Jump HIPHOP ver."
**Lyrics: Shoko Fujibayashi
**Composition & Arrangement: Shuhei Naruse AAA Den-O form
;Other songs
*"Double-Action GAOH form"
**Lyrics: Shoko Fujibayashi
**Composition: LOVE+HATE
**Arrangement: Shuhei Naruse
**Artist: Gaoh (Hiroyuki Watanabe) 
*:This song was not utilized in the theatrical release of the film but was placed on its soundtrack. It was also placed in the Final Cut Version DVD release.
*"Double-Action Wing form"
**Lyrics: Shoko Fujibayashi
**Composition: LOVE+HATE, Shuhei Naruse
**Arrangement: Shuhei Naruse
**Artist: Ryotaro Nogami & Sieg (Takeru Satoh & Shin-ichiro Miki)
*:This song was not utilized in the theatrical release but was used in the Final Cut Version.  It also plays during Siegs appearance in the final episode of the TV series.

==DVD releases==
The movie was initially released on DVD in Japan on January 28, 2008, in a normal edition and a   that came with a bonus disc. On May 21, 2008, the   version was released which included 12 minutes of unreleased footage, the Momotaross Summer Vacation short, and a special release CD with "Double-Action Wing form" and "Double-Action Wing form   Ver." on it.

==References==
 

==External links==
*  - Official website for Kamen Rider Den-O: Im Born!   (Inactive)
*   

 

 
 
 
 
 
 
 