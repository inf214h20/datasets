Darby's Rangers (1958 film)
{{Infobox film
| name           = Darbys Rangers
| image          = Darbysrangers.jpg
| image_size     = 200px
| caption        = Original film poster
| director       = William Wellman
| producer       = Martin Rackin
| writer         = Guy Trosper
| based on       = Darbys Rangers: An Illustrated Portrayal of the Original Rangers
| screenplay     =
| narrator       = Jack Warden
| starring       = James Garner Jack Warden Stuart Whitman
| music          = Max Steiner
| cinematography = William H. Clothier
| editing        = Owen Marks
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 121 min.
| country        = United States
| language       = English
}}

Darbys Rangers (released in the UK as The Young Invaders ) is a 1958 war film starring James Garner as William Orlando Darby, who organized and led the first units of United States Army Rangers during World War II. The movie was shot by Warner Brothers Studios in black and white to match wartime stock footage included in the production and was directed by William Wellman. The film was based on the 1945 book Darbys Rangers: An Illustrated Portrayal of the Original Rangers by Major James J. Altieri, himself a veteran of Darbys force. 

==Plot==
The  ) with Peggy McTavish (Venetia Stevenson), the daughter of the fearsome but humorous Scottish Commando instructor, Sergeant McTavish (Torin Thatcher), and vagabond Hank Bishop (Stuart Whitman) with the proper Wendy Hollister (Joan Elan). 

The Rangers prove their worth in  ).

Darby confides to Rosen a recurring dream of being run over by an oncoming train, foreshadowing the tragic climax. During the Battle of Anzio, the 1st and 3rd Ranger Battalions are sent on a dangerous mission; they are ambushed and wiped out by the Germans in the Battle of Cisterna. Of the 767 men who go in, only seven come back, the majority being captured. Burns is among the dead. Darby leads his 4th Ranger Battalion in an unsuccessful rescue attempt.

After the heavy losses at Cisterna, the Ranger units are disbanded. Brief vignettes show Bishop on leave with Wendy and her family, and Dittman with Angelina. Darby leaves the Anzio beachhead to report to Army HQ, taking salutes from newly arrived troops as he walks alone down the beach to board a landing craft.

==Cast==
* James Garner as William Orlando Darby
*   as Angelina De Lotta
* Jack Warden as Saul Rosen
* Edd Byrnes as Arnold Dittmann (billed as Edward Byrnes)
* Venetia Stevenson as Peggy McTavish
* Torin Thatcher as Sergeant McTavish Peter Brown as Rollo Burns
* Joan Elan as Wendy Hollister
* Corey Allen as Pittsburgh Tony Sutherland
* Stuart Whitman as Hank Bishop
* Murray Hamilton as Sims Delancey
* William Wellman, Jr. as Eli Clatworthy (billed as Bill Wellman, Jr.)
* Andrea King as Sheilah Andrews Adam Williams as Heavy Hall
* Frieda Inescort as Lady Hollister
* Reginald Owen as Sir Arthur Hollister
* Sean Garrison as Young Soldier (uncredited), his first film role

==Production== Battle Cry, Force of Foreign Legion air squadron. Warner Brothers emphasised the romantic pairings of most of the leads, as a feature of the film, to emulate the success of Battle Cry; problems arose.
 Marine Corps had enthusiastically lent bases, Marine extras, and film of its campaigns to Hollywood films to boost its public image (see The United States Marine Corps on film). But the Army was not so keen on this project. The Army felt that Ranger operations led to heavy losses of excellent soldiers that the Army thought would be better employed leading regular infantry units. By the 1950s, rather than the separate Ranger units shown in the movie, the Army preferred training individual officers and NCOs at the Ranger School, who then returned to their parent units and trained them in Ranger tactics and military values. Thus, the U.S. Armys co-operation was limited to training the actors and providing black-and-white stock footage.

Originally, Charlton Heston was cast as William O. Darby. He was enthusiastic about portraying a recent historical figure; he could interview people who knew Darby in creating his characterization. However, he asked for five percent of the profits. Jack Warner thought he was joking, until just before filming.  Warner looked to his studios contracted actors, and, fortuitously, replaced Heston with thirty-year-old James Garner. Garner had the proper appearance and age to play William O. Darby, who died at age thirty-four. Garner, a Korean War veteran, had just broken through as a leading man with the Maverick (TV series)|Maverick television series. Garners first leading film role demonstrated the thespian range that made him believable as a Ranger infantry officer. Garners original role in the film was taken by Stuart Whitman. 

Originally,   and hip "Kookie" of 77 Sunset Strip.
 Francis De Sales had an uncredited role as a captain. The film emphasizes romantic subplots, as Darby counsels his soldiers about their personal problems. William H. Clothiers black and white cinematography blends with the stock footage, and renders the sets believable.

==Promotion==
The premiere showing in several major US cities was preceded by a banquet where James Garner and the highest-ranking Darby Ranger in that city still in the service sat side by side at the head table.  In Philadelphia, that Darby Ranger was Captain Edward Haywood, US Army retired, 1941 to 1961, deceased 1990.

==References==
 

==External links==
*  
*  
*  
*  
*   at Archive of American Television

 

 
 
 
 
 
 
 
 