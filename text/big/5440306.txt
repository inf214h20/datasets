Beau Geste (1966 film)
{{Infobox Film
| name           = Beau Geste
| image          = beaugeste.jpg
| image_size     = Frank McCarthy
| director       = Douglas Heyes
| producer       = Walter Seltzer
| writer         = P. C. Wren Douglas Heyes
| narrator       = 
| starring       = Guy Stockwell Doug McClure Leslie Nielsen Telly Savalas
| music          = Hans J. Salter
| cinematography = Bud Thackery
| editing        = Russell F. Schoengarth Universal Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 novel by P. C. Wren filmed by Universal Pictures in Technicolor and Techniscope near Yuma, Arizona and directed by Douglas Heyes. This is the least faithful of the various film adaptations of the original novel. In this version, there are only two brothers, rather than three, and there are no sequences showing Beaus life prior to his joining the Legion.

==Plot== Tuareg attacks and American Beau Graves (Guy Stockwell) is the only survivor.  After his badly injured arm is amputated, he is asked what has happened and his story is revealed in Flashback (narrative)|flashback.

Beaus column had been serving under Lieutenant De Ruse (Leslie Nielsen) and Sergeant Major Dagineau (Telly Savalas), the latter of whom is notorious for his harsh treatment of the men under his command. He is especially tortuous on Beaus class of recruits, hoping this will get them to reveal to him which of the men is the author of an anonymous letter Dagineau has received threatening his life. Although he has no proof, he suspects Beau, which earns Beau particularly brutal treatment.

Beaus background leads De Ruse to nickname him Beau "Geste".  Specifically, Beau had run away and joined the Legion after having falsely confessed to an embezzlement actually committed by his business partner. Beau had taken the blame for the sake of his partners wife, whom Beau also loved. His noble gesture (French: beau geste) had proven futile, however, as the partner confessed and committed suicide just a few months later. That development prompts the suggestion that Beau might reclaim his lost love upon returning home. But he deems it unfair to ask her to wait for him, as he is now committed to a five-year enlistment, with no guarantee hell survive it.

De Ruse and his men are assigned to relieve Fort Zinderneuf, but on the way there, De Ruse is mortally wounded and is infirmed upon the groups arrival.  With Dagineau back in charge, the brutality returns and it isnt long before the legionnaires mutiny, with everyone except Beau and his brother John (Doug McClure) set upon executing the sergeant. Just as they are about to do so, the Tuareg attack. Despite their personal hatred for Dagineau, no one doubts his excellence as a battle commander, so Beau convinces the men to release him that he may lead them in defending the fort.

As legionnaires are killed in relentless waves of Tuareg attacks, Dagineau props up the bodies of the dead men on the forts ramparts with their rifles pointing at their attackers. Between attacks, De Ruse speaks privately with Beau and confesses to being the author of the letter. He had hoped to frighten Dagineau into showing more humanity toward his troops. As he dies, De Ruse laments that in fact, it only caused Dagineau to treat the men even more harshly.

The legionnaires try to hold out against the attacks, with Dagineau confidently proclaiming that relief is on the way to them.  But eventually, after John was shot by a wounded Tuareg, the only ones left alive are Dagineau and Beau, who has a seriously wounded arm.  When the predicted relief column arrives, Dagineau delays their entry so he can settle things with Beau once and for all.  He tells Beau that the Legion is need of heroes, and that all of the dead men around them can be presented as those heroes, so long as no one ever knows that they had mutinied. Therefore, he cannot leave Beau alive to reveal the truth of what had occurred at Fort Zinderneuf. Beau and Dagineau fight for their side of the history, with Beau finally gaining the upper hand and shooting and killing Dagineau. As promised to your brother John, Beau arranges the promised Viking funeral, placing the dead body of Dagineau as the brother´s dog.

Beaus flashback ends to reveal that he has not actually been relating this story to the relief commander who had questioned him about what happened at the fort.  Still awaiting Beaus response, the commander repeats his query, and Beau tells him only that the men laid down their lives protecting the fort. With no mention of Dagineaus brutality, nor of the mutiny, Beau presents the entire group as heroes, and not mutineers as Dagineau had wanted.

The commander informs Beau that the Legions high command has decided that Fort Zinderneuf is no longer worth protecting and they will now abandon it. Having lost his arm, Beau will be discharged, and the commander offers his hope that Beau has someone to whom to return. Beau smiles pensively and replies that he indeed does.

==Cast==
*Guy Stockwell as Beau Geste
*Doug McClure as John
*Leslie Nielsen as Lieutenant De Ruse
*Telly Savalas as Sergeant Major Dagineau David Mauro as Boldini
*Robert Wolders as Fouchet
*Leo Gordon as Krauss
*Michael Constantine as Rostov
*Malachi Throne as Kerjacki
*Joe De Santis as Beaujolais
*X. Brands as Vallejo
*Michael Carr as Sergeant
*George Keymas as Platoon sergeant
*Patrick Whyte as Surgeon
*Victor Lundin as Vachiaro

==Production==
Gene Kelly was originally set to produce but he left Universal to make a movie at 20th Century Fox. Walter Seltzer to Make Beau Geste: MOVIE CALL SHEET
Martin, Betty. Los Angeles Times (1923-Current File)   15 June 1965: C14. 
==See also==
*Beau Geste for a list of adaptations of the novel
==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 