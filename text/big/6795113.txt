Rettai Jadai Vayasu
{{Infobox film
| name =Rettai Jadai Vayasu
| image =
| image_size =
| caption =
| director = C. Sivakumar
| producer = N. Pazhaniswamy
| writer = Mantra Goundamani Senthil
| Deva
| cinematography = A. Karthik Raja
| editing = K. Mohan
| distributor =
| released = 12 December 1997
| runtime = 134 mins
| studio = Bhagyam Cine Combines
| country =   Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Tamil romantic Manthra in Senthil and Ponvannan among others play other pivotal roles in the film, which has music composed by Deva (music director)|Deva. It was released on 12 December 1997 and was success at the box office.

==Cast==
*Ajith Kumar as Vijay
*Manthra as Anjali                                               
*Goundamani as Vijay´s Uncle Senthil
*Latha as Anjali´s Mother
*Apoorva
*Ponvannan as Vijay´s brother in law and Anjali´s Brother
* Ajay Rathinam as Jeeva

==Production==
The film saw the return of C. Sivakumar, who had previously directed the film Ayudha Poojai, and Bhagyam Cine Arts offered him another chance. During the launch of the film, veteran comedian Goundamani was given a large green garland. 

The film super hit at box office, which made 5 classical hit  starring Ajith Kumar in 1997 after the biggest success of Kadhal Kottai. 

==Reception==
The family theme and comical scenes from Goundamani and Ajith. very will it a box office success, which was indicative of the Tamil audiences when it was broadcast on television. 

==Soundtrack==
{{Infobox album|  
  Name        = Rettai Jadai Vayasu
|  Type        = Soundtrack Deva
| Kavingar VASAN
|  Cover       =
|  Released    = 1997
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Five Star Audio
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Rettai Jadai Vayasu (1997)
|  Next album  = 
}}
The soundtrack of the film was composed by Deva (music director)|Deva.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Kanchipattu Chellakatti  Hariharan
| length1         = 5:15
| title2          = Angel Engae
| extra2          = Adithyan, Swarnalatha 
| length2         = 5:14
| title3          = Thaam Thaketa Theemi  Mano
| length3         = 5:03
| title4          = Idlikku Mavu Attaiyle 
| extra4          = Krishnaraj, Sabesh
| length4         = 5:31
| title5          = Mattikittan Mattikittan 
| extra5          = Deva (music director)|Deva, Anuradha Sriram
| length5         = 2:03
| title6          = Vellipani Malarae 
| extra6          = Unnikrishnan
| length6         = 1:31
| title7          = Kummunu Erukkalam Kushpoo
| extra7          = Krishnaraj, G. V. Prakash Kumar
| length7         = 5:01
}}

==References==
 

 
 
 
 


 