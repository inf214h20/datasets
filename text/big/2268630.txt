The Missouri Breaks
 
{{Infobox film
| name           = The Missouri Breaks
| image          = missouri_breaks_movie_poster.jpg
| caption        =
| director       = Arthur Penn
| producer       = Elliott Kastner Robert M. Sherman
| writer         = Thomas McGuane
| starring       = Marlon Brando Jack Nicholson Randy Quaid Kathleen Lloyd Frederic Forrest Harry Dean Stanton
| music          = John Williams
| cinematography = Michael Butler
| editing        = Dede Allen Gerald B. Greenberg  Steven A. Rotter
| distributor    = United Artists
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         =
}} western film starring Marlon Brando and Jack Nicholson. The film was directed by Arthur Penn, with supporting performances by Randy Quaid, Harry Dean Stanton, Frederic Forrest, John McLiam and Kathleen Lloyd.  The score was composed by John Williams.

The title of the movie refers to a forlorn and very rugged area of north central Montana, where over eons the Missouri River has made countless deep cuts or "breaks" in the land.

==Plot== rustler experiencing hard times. He and his gang are particularly upset by the hanging of a friend by Braxton, a land baron who takes the law into his own hands.

Logans men pull off a daring train robbery, only to lose much of the money. They decide to seek vengeance against Braxton by killing his foreman Pete Marker and by buying a small property close to Braxtons ranch, then rustling his stock. First the gang, without Logan, rides off across the Missouri River and north of the border to steal horses belonging to the Royal Canadian Mounted Police. In their absence, Logan plants crops and enters into a relationship with Braxtons virginal daughter, Jane.
 Irish brogue and a Creedmoor rifle (a Model 1859 Sharps rifle) with which he is deadly accurate from a very long distance.

Quickly suspicious of Logan, who doesnt strike him as a farmer, Clayton dons a variety of disguises and begins to pick off Logans gang, one by one. Identifying himself as "Jim Ferguson," he kills Logans young friend Little Tod by dragging him with a rope through the raging Missouri.

Clayton spies on Logan with binoculars and taunts Braxton about his daughters affair with a horse thief. Braxton attempts to discharge him but Clayton is determined to finish what he starts. He amuses himself by shooting two more of Logans partners, Cary and Cy, from a distance and then by wearing a "granny" dress while brutally killing Logans closest friend, Cal, with a handmade weapon.

Logan knows its kill or be killed. He also wants vengeance against Braxton for having hired the regulator in the first place, despite his feelings for Jane. One night after a campfire goes dark with Clayton serenading his horse, Logan slits his throat. He then comes after Braxton, who has lost his mind — perhaps having suffered a stroke — as well as losing his daughter. Braxton pulls a weapon on Logan, but is himself shot in the chest.

Logan abandons his farm and packs up to leave. He acknowledges to Jane the possibility that they can renew their relationship another time, another place.

==Production==
In a May 24, 1976 Time (magazine)|Time magazine interview  it was revealed that Brando "changed the entire flavor of his character — a bounty hunter called Robert E. Lee Clayton — by inventing a deadly hand weapon resembling both a harpoon and a mace that he uses to kill. He said, "I always wondered why in the history of lethal weapons no one invented that particular one. It appealed to me because I used to be very expert at knife throwing." 

After one horse drowned and several others were injured, including one by American Humane Association (AHA)-prohibited tripwire, this film was placed on the AHAs "unacceptable" list. 
 Nevada City, Red Lodge, Virginia City.

== Cast ==

* Marlon Brando as Robert E. Lee Clayton
* Jack Nicholson as Tom Logan
* Randy Quaid as Little Tod
* Kathleen Lloyd as Jane Braxton
* Frederic Forrest as Cary
* Harry Dean Stanton as Cal
* John McLiam as David Braxton John P. Ryan as Cy (as John Ryan)
* Sam Gilman as Hank Rate
* Steve Franken as Lonesome Kid Richard Bradford as Pete Marker
* James Greene as Hellsgate rancher
* Luana Anders as Ranchers wife
* Danny Goldman as Baggage clerk
* Hunter von Leer as Sandy

==Reaction== One Flew Over the Cuckoos Nest, the film was highly anticipated, but become a notorious critical and commercial flop.

Vincent Canbys review in the May 20, 1976 New York Times cited "an out-of-control performance" by Brando. Marlon Brando agreed to accept $1 million for five weeks work plus 11.3% of gross receipts in excess of $10 million. Jack Nicholson agreed $1.25 million for ten weeks work, plus 10% of the gross receipts in excess of $12.5 million. Despite its two stars, Missouri Breaks reportedly earned a domestic box-office gross of a mere $14 million.

Xan Brooks of The Guardian sees the film as having ripened over the years: "Time has worked wonders on The Missouri Breaks. On first release, Arthur Penns 1976 western found itself derided as an addled, self-indulgent folly. Today, its quieter passages resonate more satisfyingly, while its lunatic take on a decadent, dying frontier seems oddly appropriate. ...Perhaps for the last time, there is a whiff of method to (Brandos) madness. He plays his hired gun as a kind of cowboy Charles Manson, serene and demonic".  

==References==
 

== External links ==
*  
*  

 

 

 
 
 
 
 
 
 
 
 