Don't (1925 film)
{{Infobox film
| name           = Dont
| image          =
| caption        =
| director       = Alfred J. Goulding
| producer       =
| writer         = Agnes Christine Johnston (screenplay) Rupert Hughes (story)
| narrator       = John Patrick Bert Roach Ethel Wales
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}} John Patrick, Bert Roach, and Ethel Wales, and released by Metro-Goldwyn-Mayer. The film is one of the B pictures the studio produced to keep the Loews circuit and other cinemas supplied.

The screenplay by Agnes Christine Johnston is based on the story Dont You Care! by Rupert Hughes. This film is considered a lost film.  

==Synopsis==
Tracey Moffat (Sally ONeil) is a parent-defying flapper.

==Cast==
* Sally ONeil - Tracey Moffat John Patrick - Gilbert Jenkins
* Bert Roach - Uncle Nat
* James Morrison - Abel
* Estelle Clark - Jane
* DeWitt Jennings - Mr. Moffat
* Ethel Wales - Mrs. Moffat

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 