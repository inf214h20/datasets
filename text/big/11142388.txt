Appooppan
{{Infobox film
| name           = Appooppan
| image          = Appooppanfilm.jpg
| image_size     =
| caption        = Promotional Poster
| director       = P. Bhaskaran
| producer       = Evershine Productions Murugan Movies
| writer         =
| narrator       = Sumithra
| music          = M. S. Baburaj
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| website        =
}}
 1976 Cinema Indian feature directed by Sumithra and Kamal Haasan.   

==Cast==

*Thikkurissi Sukumaran Nair
*Kamal Haasan
*Jayabharathi Sumithra

==Trivia==

The film Appooppan was announced as Charitram Aavarthikkunnilla and the films posters and LP records covers also carried the same name. The title change to Appooppan happened just within 1 week before the theatrical release of the film. There is a misconception that Appooppan and Charitram Aavarthikkunnilla are two different films.

==Soundtrack ==

{{Infobox album Name     = Appooppan Type     = film Cover    = Charitramavarthikkunnilla.jpg Released =  Music    = M. S. Baburaj Genre  Feature film soundtrack Length   =  Label    = EMI The Gramaphone Company of India Limited
}}

The music composed by M. S. Baburaj.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Edavappathiyil... || K. J. Yesudas, L.R.Anjali || rowspan=4|P. Bhaskaran 

|-
| 2 || Anandakkuttan... || S. Janaki  
|-
| 3 || Aatirambile Sundari... || Jayachandran, L.R.Anjali 
|-
| 4 || Uttharam Kittatha... || K. J. Yesudas 
|-
|}

==References==
 

==External links==
 

 
 


 