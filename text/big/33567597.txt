For Better, for Worse (1959 film)
 
 
{{Infobox film
| name           = For Better, for Worse
| image          = 
| caption        = 
| director       = Yueh Feng Robert Chung
| writer         = Yueh Feng
| starring       = Connie Chan
| music          = 
| cinematography = Jie Fan
| editing        = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Film at the 32nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Connie Chan as Damei
* Yang Chang as Gao Yongsheng
* Xiaoyu Deng as Xiaohu (as Peter Dunn) Xiang Gao as Landlords Wife
* Helen Li Mei as Meijuan
* Yunzhong Li as Yongshengs Brother-in-law
* Enjia Liu as Mr. Zhao - The Landlord
* Qianmeng Liu as Yongshengs Sister
* Feng Su as Xiujuan
* Ching Tien as Li Zixin

==See also==
* List of submissions to the 32nd Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 