Pink String and Sealing Wax
{{Infobox film
| name           = Pink String and Sealing Wax
| caption        =
| image          = Pink String and Sealing Wax FilmPoster.jpeg
| director       = Robert Hamer
| producer       = Michael Balcon
| based on       =   Diana Morgan Gordon Jackson
| music          = Norman Demuth
| cinematography = Stanley Pavey
| editing        = Michael Truman
| studio         = Ealing Studios
| distributor    = Eagle-Lion Films|Eagle-Lion Distrib. Ltd  
| released       =  
| runtime        = 95 minutes    Linked 2015-04-25 
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1945 British Gordon Jackson. It is based on a play with the same name by Roland Pertwee. It was the first feature film Robert Hamer directed on his own. 

==Plot==
The wife of a pub landlord in Victorian Brighton, who is having an affair, wants to rid herself of her abusive husband. To accomplish this she befriends a young man who works in his fathers pharmacy. and thus has access to poison.

==Main cast==
* Mervyn Johns as Edward Sutton 
* Googie Withers as Pearl Bond  Gordon Jackson as David Sutton 
* Jean Ireland as Victoria Sutton 
* Sally Ann Howes as Peggy Sutton 
* Mary Merrall as Ellen Sutton
* David Walbridge as Nicholas Sutton 
* Catherine Lacey as Miss Porter 
* Garry Marsh as Joe Bond
* Maudie Edwards as Mrs. Webster
* Frederick Piper as Dr. Pepper

==Release== The Strand and the Marble Arch Pavilion. The critic in The Times praised Googie Withers and Gordon Jackson for their roles, and concluded that Robert Hamer, "has made, in spite of occasional lapses and longueurs, a promising beginning as a director."   Linked 2015-04-25 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 