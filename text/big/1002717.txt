Foul Play (1978 film)
{{Infobox film
| name           = Foul Play
| image          = Foul Play1978.jpg
| caption        = Promotional poster
| alt            = 
| director       = Colin Higgins
| producer       = Edward K. Milkis Thomas L. Miller
| writer         = Colin Higgins Rachel Roberts Eugene Roche Dudley Moore Charles Fox
| cinematography = David M. Walsh
| editing        = Pembroke J. Herring
| studio         = 
| distributor    = Paramount Pictures
| released       = July 14, 1978
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $44,999,621 
}} comic mystery Rachel Roberts, Brian Dennehy and Billy Barty. In it, a recently divorced librarian is drawn into a mystery when a stranger hides a roll of film in a pack of cigarettes and gives it to her for safekeeping.
 ABC television series starring Barry Bostwick and Deborah Raffin that aired in early 1981 and was cancelled after six episodes. 

==Plot== Highway 1. She impulsively accepts Scottys invitation to join him at the movies that evening, and before they part ways he asks her to take his pack of cigarettes in order to help him curb his smoking. Unbeknownst to her, Scotty has secreted a roll of film in the pack. That evening, a seriously wounded Scotty meets Gloria in the theater and warns her to "beware of the Dwarfism|dwarf" before dying. When his body mysteriously disappears while Gloria seeks help from the theater manager, she is unable to convince anyone of what has transpired.  

At the end of the following work day, Gloria is attacked in the library by   who assumes she is picking him up to have sex. Shocked by his misunderstanding, she flees and returns to her apartment, where she is attacked by a man with a scar who demands the cigarette pack Scotty had given her. When he attempts to strangle her with a scarf, Gloria stabs him in the stomach with a pair of knitting needles and calls the police for help. When her attacker tries to stop her, he is killed by Whitey through the kitchen window, and Gloria faints. When she awakens, all traces of what has happened have disappeared, and she is unable to convince two San Francisco policeman, Lt. Tony Carlson (Chevy Chase) and his partner Inspector "Fergie" Ferguson (Brian Dennehy), or even her landlord Mr. Hennessy (Burgess Meredith) that she was attacked.
 Mace and brass knuckles given to her by her friend and fellow library employee, Stella (Marilyn Sokol). Later, Tony takes her to his Sausalito houseboat, where the two become involved romantically. Upon further investigation, Tony discovers that a contract killer named Rupert Stiltskin (alias "the Dwarf") was under investigation by an undercover detective named Bob "Scotty" Scott, who had received a tip that a major assassination would take place in the city on a certain night. Tony is now assigned to protect Gloria from her would-be killers.
 Archbishop Thorncrest (Eugene Roche), unaware that the man theyre interviewing is in fact the Archbishops twin brother Charlie, who is involved in a plot to assassinate Pope Pius XIII (played by prominent San Francisco businessman Cyril Magnin) during his upcoming visit to San Francisco. Charlie has murdered his twin in order to impersonate him. The following day, Rupert kidnaps Fergie and uses him to lure Gloria into a trap. She manages to hide in a massage parlor, where she encounters Stanley yet again, but then is found and abducted by Jackson and Stiltskin.
 San Francisco Rachel Roberts), who is really Delia Darrow.

Darrow then details her "contingency plan" to eliminate the Pope: If His Holiness is not yet terminated at the end of Act I, Whitey Jackson will open fire from one of the auditoriums two organ bays ("He will also open fire should the Pope unexpectedly leave his seat, or if the police arrive in the auditorium," Darrow explains). Mr. Hennessy knocks out Charlie and defeats Delia in a martial arts duel, and Tony and Gloria race to the Opera House, having some unusual problems along the way (such as crashing into an Italian restaurant and commandeering a limo carrying a pair of Japanese emigres). After making it backstage, Gloria is grabbed by Jackson, who kills one of several security guards who have joined the pursuit. An enraged Gloria attempts to attack Jackson, who simply shoves her to the floor. This gives Tony the room he needs to shoot the albino, thus thwarting the plan to kill the Pope.  As the performance ends, Gloria and Tony are revealed onstage along with the now-dead bodies of Jackson and the guard, but the Pope, who seems not to have noticed anything unusual, leads the audience in applause for the cast, the orchestra, and the conductor - Stanley Tibbets.

==Cast==
* Goldie Hawn as Gloria Mundy
* Chevy Chase as Lieutenant Tony Carlson
* Burgess Meredith as Mr. Hennessey
* Brian Dennehy as Inspector "Fergie" Ferguson
* Dudley Moore as Stanley Tibbets Rachel Roberts as Gerda Casswell/Delia Darrow
* Eugene Roche as Archbishop Thorncrest/Charlie Thorncrest
* William Frankfather as Whitey Jackson
* Marc Lawrence as Rupert Stiltskin, "The Dwarf"
* Marilyn Sokol as Stella
* Billy Barty as J.J. MacKuen
* Bruce Solomon as Bob "Scotty" Scott
* Don Calfa as Scarface
* Cyril Magnin as Pope Pius XIII
* Chuck McCann as Nuart theatre manager

==Production== The Man Who Knew Too Much, which inspired the opera house sequence in Foul Play.  When Gloria is attacked in her home, she reaches inside her knitting basket and chooses a pair of scissors to defend herself -- a reference to Dial M for Murder.  Other Hitchcock films which receive a nod from screenwriter/director Colin Higgins include, Notorious (1946 film)|Notorious, Vertigo (film)|Vertigo, and Psycho (1960 film)|Psycho. In addition, the plot includes a MacGuffin—an object that initially is the central focus of the film but declines in importance until it is forgotten and unexplained by the end—in the form of the roll of film concealed in the pack of cigarettes. Hitchcock popularized the term MacGuffin and used the technique in many of his films.

"Audiences love to be scared and at the same time they love to laugh," said Higgins. "It is tongue in cheek realism. The audience is in on the joke but the actors must carry on as if they were unaware." FILM CLIPS: Hawn On Deck for Foul Play
Kilday, Gregg. Los Angeles Times (1923-Current File)   13 Aug 1977: b6.  

The script was originally written under the name Killing Lydia with Goldie Hawn in mind for the lead. Higgins had met Hawn through their mutual friend, Hal Ashby. However the project did not take off. After Silver Streak came out Higgins rewrote the script. He and the producers took the project to Paramount who hoped to star Farrah Fawcett. However Fawcett was in the middle of a legal battle with the producers of Charlies Angels so in the end it was decided to go with Hawn. 

The name Gloria Mundy is a reference to "Sic transit gloria mundi," a Latin phrase meaning "Thus passes the glory of the world." It was included in the ritual of papal coronation ceremonies until 1963.

Higgins had written the role of Stanley Tibbets for Tim Conway, but when the actor turned it down he offered it to Dudley Moore instead. It was Moores American film debut and led to his being cast in 10 (film)|10 by Blake Edwards the following year. 

Higgins says when he sold the script he wanted to direct it so badly he did not care who was going to play the lead roles. He met with Farrah Fawcett to play the female lead before going with Goldie Hawn. His first choice for the male lead was Harrison Ford (who had been Higgins carpenter) who turned it down. Steve Martin was also offered the role but did not end up playing it. Higgins says he offered the part to another actor who wanted to play the cop and Stanley Tibbets. Eventually Chevy Chase was cast. HIGGINS: WRITER-DIRECTOR ON HOT STREAK
Goldstein, Patrick. Los Angeles Times (1923-Current File)   24 Jan 1981: b15. 
 Noe Valley, Mission District, Telegraph Hill, Hayes Valley, Nob Hill, Pacific Heights, Marina District, Potrero Hill, City Hall art house located on Santa Monica Boulevard in West Los Angeles. The houseboat, "Galatea", was located at 15 Yellow Ferry Harbor in Sausalito.  
 Charles Fox, with lyrics by Foxs writing partner, Norman Gimbel and performed by Barry Manilow, who conceived and supervised the songs recording in partnership with Ron Dante. The soundtrack also includes Copacabana (song)|"Copacabana" written by Manilow, Jack Feldman and Bruce Sussman, and performed by Manilow; "I Feel the Earth Move" by Carole King,  and "Stayin Alive," written and performed by the Bee Gees. Excerpts from Act I of Gilbert & Sullivans The Mikado, conducted by Julius Rudel, are performed by members of the New York City Opera. 

==Critical reception== House Calls Silver Streak, and this time hes much more successful . . . Still, Mr. Higgins isnt a facile enough juggler to keep the films diverse elements from colliding at times." 

Variety (magazine)|Variety observed, "Writer Colin Higgins makes a good directorial bow." 

Time Out London stated, "Unsatisfactory as a whole, the film is hilarious and tense in bits" and noted "while writer/director Higgins uses almost every stock thriller device . . . he approaches this semi-parody with more zest and originality than is common." 

Channel 4 called the film "a finely tuned and fast-paced offering which is chock-full of black comic twists and perfect casting." 

==Awards and nominations==
Charles Fox and Norman Gimbel were nominated for the Academy Award for Best Song but lost to Paul Jabara for Last Dance (song)|"Last Dance" from Thank God Its Friday. 
 Heaven Can Best Actress Best Actor Best Supporting Best Screenplay, Best Original Song (Charles Fox and Norman Gimbel).
The film tied Who’s Afraid of Virginia Woolf? and The Godfather Part III for most nominations without a win at the Globes.

==Trivia==
In Sweden, the connection to  : Tjejen som föll överbord ("The girl who fell overboard").

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
  
 
 
 
 
 