Tony Manero (film)
Tony 2008 Chilean film directed by Pablo Larraín about a 52 year old man in Santiago in 1978 who is obsessed with John Travoltas character in Saturday Night Fever. It won the top prize at the 2008 Torino Film Festival and was Chiles submission to the 81st Academy Awards for the Academy Award for Best Foreign Language Film.   In 2009 it won the Golden Tulip at the Istanbul International Film Festival.

==Cast== Alfredo Castro - Raúl Peralta
*Paola Lattus - Pauli
*Héctor Morales (actor)|Héctor Morales - Goyo
*Amparo Noguera - Cony
*Elsa Poblete - Wilma

==Reception==
The film currently holds an 85% score from the film critics site Rotten Tomatoes, with the  average rating calculated as 6.6/10. It summarised the critical consensus as: "Deliberately provocative, Tony Manero is as challenging and compelling as it is difficult to describe.". 

==DVD releases==
The film has been released on DVD in several countries.  Region 2 DVDs were released in 2009 by Network DVD in the U.K. and Ripleys Home Video in Italy, and an unrated region 1 DVD was released in 2010 by Kino International.

==See also==
 
*List of submissions to the 81st Academy Awards for Best Foreign Language Film
* Cinema of Chile

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 