Hansie
{{Infobox Film
| name           = Hansie
| image          = Hansie main poster sml.jpg
| image_size     = 
| caption        = Promotional Poster
| director       = Regardt van den Bergh
| producer       = Frans Cronje
| writer         = 
| narrator       =  Sarah Thompson
| music          = 
| cinematography = 
| editing        = 
| distributor    = Global Creative Studios
| released       = 24 September 2008
| runtime        = 
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Hansie is a feature film, produced in South Africa by Global Creative Studios and directed by Regardt van den Bergh. It is based on the true story of cricketer Hansie Cronje. The movie was released on 24 September 2008 in South African cinemas and the Collectors Edition DVD on 24 November 2008.
"How do you start over once you have betrayed a nations trust?" The news of Hansie Cronjés involvement with Indian bookmakers and his resulting public confession rocked the international sporting community. An unprecedented rise to glory was followed by the most horrific fall. A tarnished hero fueled the nations fury. Hansie, once South African crickets golden boy, had been stripped of everything he had held dear: a glorious captaincy, the support of his former team mates and the respect of a nation. In its place the stinging rejection of cricket administrators and the humiliating dissection of his life on international television, made his retreat into depression inevitable. Hansies bravest moment in finally confessing his involvement with bookies had suddenly become a tightening noose around his neck.

Lead actors: Frank Rautenbach as Hansie and Sarah Thompson as Bertha

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 


 