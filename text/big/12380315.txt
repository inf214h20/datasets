That's My Boy (1932 film)
 
{{Infobox film
| name           = Thats My Boy
| image          = Thats My Boy FilmPoster.jpeg
| caption        = Film poster
| director       = Roy William Neill
| producer       = Roy William Neill Francis Wallace
| narrator       = Richard Cromwell Dorothy Jordan
| music          =
| cinematography = Joseph H. August
| editing        = Jack Dennis
| distributor    =
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
}}
 Richard Cromwell Dorothy Jordan. It features John Wayne in an uncredited role as a football player.

==Cast== Richard Cromwell as Tommy Jefferson Scott Dorothy Jordan as Dorothy Whitney
* Mae Marsh as Mom Scott Arthur Stone as Pop Scott
* Douglass Dumbrille as Coach "Daisy" Adams
* Lucien Littlefield as Uncle Louie Leon Ames as Al Williams 
* Russell Saunders as Pinkie
* Sumner Getchell as Carl
* Otis Harlan as Mayor
* Oscar Dutch Hendrian as Hap
* Elbridge Anderson as 1st Student
* Crilly Butler as 2nd Student Douglas Haig as Tommy, as a young boy
* John Wayne as Football Player (uncredited)

==See also==
* John Wayne filmography

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 