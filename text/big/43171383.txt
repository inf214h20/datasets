Kudumba Thalaivan
{{Infobox film
| name           = Kudumba Thalaivan
| image          = 
| caption        = 
| director       = M. A. Thirumugam
| producer       = Sandow M. M. A. Chinnappa Thevar
| writer         = Aarur Das
| story          = Aarur Das
| starring       = M. G. Ramachandran M. R. Radha B. Saroja Devi S. A. Ashokan Sandow M. M. A. Chinnappa Thevar M. V. Rajamma
| music          = K. V. Mahadevan
| cinematography = C. V. Murthy
| editing        = M. A. Thirumugam M. G. Balu Rao M. A. Mariappan
| studio         = Devar Films
| distributor    = Devar Films
| released       =   
| runtime        = 139 mins
| country        =   India Tamil
}}
 1962 Tamil language drama film directed by M. A. Thirumugam.  The film features M. G. Ramachandran, M. R. Radha and B. Saroja Devi in lead roles. 
The film, produced by Sandow M. M. A. Chinnappa Thevar, had musical score by K. V. Mahadevan and was released on 15 August 1962. 
The film was a big hit at box-office and ran for 120 days. 

==Cast==
* M. G. Ramachandran
* M. R. Radha
* B. Saroja Devi
* S. A. Ashokan
* V. K. Ramasamy (actor)|V. K. Ramasamy
* Sandow M. M. A. Chinnappa Thevar
* M. V. Rajamma Lakshmi Rajyam

==Crew==
*Producer: Sandow M. M. A. Chinnappa Thevar
*Production Company: Devar Films
*Director: M. A. Thirumugam
*Music: K. V. Mahadevan
*Lyrics: Kannadasan
*Story: Aarur Das
*Dialogues: Aarur Das
*Art Direction: A. K. Ponnusamy
*Editing: M. A. Thirumugam, M. G. Balu Rao & M. A. Mariappan
*Choreography: S. M. Rajkumar
*Cinematography: C. V. Murthy
*Stunt: Shyam Sundar
*Songs Recording & Re-Recording: T. S. Rangasamy
*Audiography: W. Narasimha Murthy & T. Sarangan
*Dance: None

==Soundtrack==

The music composed by K. V. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:23
|-
| 2 || Kattana Kattalagazhu || T. M. Soundararajan, P. Susheela || 03:22
|-
| 3 || Kuruvi Koottam Pola || T. M. Soundararajan || 03:41
|-
| 4 || Maarathayya Maarathu || T. M. Soundararajan || 03:26
|-
| 5 || Mazhai Pozhindhu || P. Susheela || 02:58
|-
| 6 || Thirumanamam || T. M. Soundararajan || 03:12
|-
| 7 || Yetho Yetho || T. M. Soundararajan, P. Susheela || 03:46
|}

==References==
 

==External links==
 

 
 
 
 
 
 


 