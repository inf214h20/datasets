Ekhon Nedekha Nodir Xhipare
{{Infobox film
| name           = Ekhon Nedekha Nodir Xhipare
| image          = Ekhon Nedekha Nodir Xhipare Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Bidyut Kotoky
| producer       = National Film Development Corporation of India
| writer         = Bidyut Kotoky
| starring       = Sanjay Suri  Bidita Bag Victor Banerjee Nakul Vaid Raj Zutshi Naved Aslam Preeti Jhangiani
| music          = Zubeen Garg 
| cinematography = Madhu Ambat
| editing        = Rajesh Parmar
| studio         =  
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = India Assamese Hindi Hindi
| budget         = 
| gross          = 
}}
 Assamese Assamese language Social political thriller film,    with some dialogue in Hindi language|Hindi, starring Sanjay Suri and Bidita Bag in the lead roles. The film was directed by Bidyut Kotoky and produced by National Film Development Corporation of India. The film was simultaneously made in Hindi and was named As the river flows.
 best Assamese film for being “not Assamese”.      

==Plot==
Sridhar Ranjan, a social activist, has gone missing from Majuli, Assam. After almost 7 years, when as per law he is about to be officially declared dead, his old journalist friend Abhijit Shandilya (Sanjay Suri) goes in search of Sridhar from Mumbai to Majuli. In Majuli, Abhijit finds himself to be a stranger of sorts in a place that’s infested with terrorists and where both the police and the terrorists seem to be suspicious of him. He befriends with his local guide Sudakshina ( Bidita Bag). Sudakshina apparently also was the guide for Sridhar when he came to Majuli initially. She however seems to be living with her own fears and stops short of getting close to Abhijit whenever an occasion comes up. The rest of the story revoles around Abhijit trying solve the mystery behind the disappearance of Sridhar.

==Cast==
* Sanjay Suri as Abhijit Shandilya, an old friend of Sridhar Ranjan
* Bidita Bag as Sudakshina, a local guide in Majuli
* Victor Banerjee as grandfather of Sudakshina 
* Nakul Vaid as Jyotiraj, brother of Sudakshina
* Raj Zutshi as Jayanta Doley, an ex-militant
* Naved Aslam as Sub-Divisional Police Officer Sameer Agarwal
* Preeti Jhangiani
* Hadorsa Daimari as " Anything "

==Production==
Bidyut Kotoky, the director of Ekhon Nedekha Nodir Xhipare, said that the film has nothing to do with the disappearance of Sanjay Ghose and it is not based on his life. According to him, “if a person like Sanjay Ghose existed, then somebody like Sridhar Ranjan – the protagonist of my film – could also have existed in Majuli. True, the film is inspired by the unfortunate disappearance of Sanjay Ghose – but inspiration ends there. The entire story is totally fictitious...”   

The film was shoot in Assams Majuli river island, Jorhat and Mumbai.   

The film also features a  poem recitation by Bhupen Hazarika|Dr. Bhupen Hazarika.   It was recorded on February 10, 2010 and featured on the starting of the film.  In Bidyut Kotokys words, “The poem represents the voice of the river Brahmaputra through the voice of the music maestro. Late Hazarika is known for his passion for the mighty river and is called the Bard of Brahmaputra. So, when I needed someone to represent the voice of the river in my film, I immediately decided to approach Dr. Hazarika. ... He was ailing and not well when I approached him for recording the recitation for the film. However, he agreed to lend his voice and we finally recorded the recitation in February 2010.”

The music of the film was composed by Zubeen Garg  and cinematography was done by veteran cinematographer Madhu Ambat. 

==Release== theatres  Maharashtra government on September 8, 2012 in Mumbai to mark Bhupen Hazarika’s birth anniversary. 

==Reception==

===Critical reception===
The film received generally positive reviews. Utpal Borpujari praising the film said that Bidyut Kotoky made a “promising debut.”    Film writer Arunlochan Das rated Ekhon Nedekha Nodir Xhipare to be a “good film based on reality.”     Seven Sisters Post praised the acting of Raj Zutshi and said that the film is “an honest attempt to tell the truth.”    Madaboutmoviez.com complimented the film to be “victory of Bidyut Kotoky.” 

==Awards and Accolades==
Bidyut Kotoky won Best Screenplay award for this film in the Assam State Film Awards for the period 2010-2012, which was presented on March 10, 2013 at Machkhowa, Guwahati. Jeevan Initiative, a voluntary trust from Assam, included the film in its list of Special 10 of the year 2012 for Assam and cited, "this cinema aesthetically unveils an almost forgotten story of our recent political history."   

Ekhon Nedekha Nodir Xhipare was selected for screening in the second edition of the Washington DC South Asian Film Festival held from May 10, 2013.    The film was awarded for the Best Script and Best Actor at the same festival.   

==Controversy== best Assamese official censorship Ministry of Information and Broadcasting, the Directorate of Film Festivals, the Central Board of Film Certification, Rohini Hattangadi—chairperson of Feature Film section, and Hiren Bora. 

After examining the documents submitted for the selection of regional films, the High Court dismissed the plea. The proceedings revealed that Ekhon Nedekha Nodir Xhipare, and other films from the eastern region, were previewed and rejected by the jury. The court imposed costs of   on the petitioner for moving to the court without ascertaining the facts of the case. 

An online campaign was also created to support the film.   Many film personalities, such as Nandita Das, Amol Gupte, Rajesh Parmar, Adil Hussain etc. have pledged support to the campaign. 

==References==
 

 
 
 