Gandugali
{{Infobox film|
| name = Gandugali
| image = 
| caption =
| director = C. H. Balaji Singh Babu
| writer =  J. G. Krishna
| starring = Shiva Rajkumar  Nirosha  Pournami 
| producer = J G Krishna   G Govindu   K C Subramani Raju   Raghavendra
| music = Sadhu Kokila
| cinematography = J. G. Krishna
| editing = R. Janardhan
| studio = Jyothi Chitra
| released =  
| runtime = 113 minutes
| language = Kannada
| country = India
| budgeBold text =
}}
 Kannada drama action drama film directed by C. H. Balaji Singh Babu and written, co-produced by J. G. Krishna. The film features Shiva Rajkumar and Nirosha in the lead roles.  The films successful soundtrack and score is composed by Sadhu Kokila.

== Cast ==
* Shiva Rajkumar 
* Nirosha
* Pournami
* Rajeev
* Ravikiran
* Meghana
* Bank Janardhan
* Sihi Kahi Chandru
* Disco Shanti
* Jai Jagadish...guest appearance

== Soundtrack ==
The soundtrack of the film was composed by Sadhu Kokila. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Something Special
| extra1 = S. P. Balasubrahmanyam& K. S. Chithra
| lyrics1 = 
| length1 = 
| title2 = Ee Jodi Nodi
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = Sriranga
| length2 = 
| title3 = Thampu Gali Maiya Soki
| extra3 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics3 = 
| length3 = 
| title4 = Thaala Mela 
| extra4 = S. P. Balasubrahmanyam, Sadhu Kokila & Mangala
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Ondu Mathanaadade Rajkumar
| lyrics5 = Rudramurthy Shastry
| length5 = 
| title6 = Ee Age Baadi
| extra6 = Suresh Peters
| lyrics6 = Ramesh Rao
| length6 = 
| title7 = Mysore Dasara
| extra7 = Sadhu Kokila & Mangala
| lyrics7 = 
| length7 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 

 