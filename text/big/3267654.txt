Sökarna
{{Infobox film
| name           = Sökarna
| image          =
| caption        =
| director       = Daniel Fridell Peter Cartriers
| producer       = Kaśka Krosny
| writer         = Daniel Fridell Leon Flamholc
| narrator       =
| starring       = Liam Norberg Ray Jones IV Thorsten Flinck Jonas Karlsson
| music          = Elia Cmiral
| cinematography = Sten Holmberg Yngvar Lande Peter Mokrosinski
| editing        = Leon Flamholc
| distributor    =
| released       =  
| runtime        = 108 min.
| country        = Sweden Swedish
| budget         =
| gross          =
}}

Sökarna (literally "The Searchers") is a 1993 Swedish   was produced in 2006.

A month before the premiere, Norberg was arrested for a robbery he had committed in 1990, and sentenced to five years in jail.  Later it was also revealed that Norberg had used money from his robberies to help finance the production.  The film is considered a major cult classic in Sweden.

The Swedish hip-hop group Infinite Mass gained public attention after their appearance in the movie with their controversial song "Area Turns Red", also known as "Shoot the Racist".

==Plot== Nazi skinheads, a subculture whose movement had a renaissance in Sweden in the early 1990s.

After participating in a raid of a clothing warehouse, Joakim Wahlåås (Liam Norberg) gets arrested and sentenced to a few years in a Swedish penitentiary. While in jail, Joakim gets exposed to inmate brutality, and associates with the heavily criminal Tony (Thorsten Flinck), who introduces Joakim (also called Jocke, or Jocke-pojken) to cocaine.

Shortly after being released from Jail Joakim and Tony team up with Joakims old friends and begin to commit more violent crimes: bank robberies, and drug distribution. The friends quickly become rich, and spend thousands of dollars on Versace clothes, champagne, drugs, and women.

A few years, many women, and plenty of Freebase (chemistry)|free-base pipes later, their lavish lifestyle begins to take its toll. The friends become dependent on cocaine and heroin. They also begin to distrust each other, and subsequent to an argument about the division of profit from a drug trade, Tony kidnaps Joakims longtime girlfriend Helen. As Joakim becomes aware of the kidnapping, he begins searching for Tony.

The film ends in a deadly confrontation, between Tony and Joakim. Joakim survives the confrontation with Tony, but a few minutes later Joakim is arrested by the police.

==Cast==
* Liam Norberg - Joakim "Jocke" Wahlåås
* Ray Jones IV - Ray Lopez
* Thorsten Flinck - Kola-Tony
* Jonas Karlsson - Gurra
* Musse Hasselvall - Andy
* Malou Bergman - Helen
* Per-Gunnar Hylén - Mike
* Örjan Ramberg - Assistant principal
* Per Sandborgh - Porr-Bengt
* Jan Nygren - Principal
* Paolo Roberto - Prisoner
* Caroline af Ugglas - Singer in the street
* E-Type (musician)|E-Type - Himself
* Anna Book - Herself

==References==
 

==External links==
*  

 
 
 
 
 