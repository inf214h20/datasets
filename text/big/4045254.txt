Vampire's Kiss
 Kiss of the Vampire}}
{{Infobox Film
 | name           = Vampires Kiss
 | image          = Vampires kiss.jpg
 | caption        = Theatrical release poster
 | director       = Robert Bierman
 | producer       = {{plainlist|
* John Daly
* Derek Gibson
* Barry Shils
* Barbara Zitwer
}}
 | writer         = Joseph Minion
 | starring       = {{plainlist|
* Nicolas Cage
* María Conchita Alonso
* Jennifer Beals
* Elizabeth Ashley
}}
 | music          = Colin Towns
 | cinematography = Stefan Czapsky
 | editing        = Angus Newton
 | distributor    = Hemdale Film Corporation
 | released       =  
 | runtime        = 103 minutes
 | country        = United States
 | language       = English German
 | budget         = $2 million  
 | gross          = $725,131 (US)   
}}
Vampires Kiss is a 1989 American black comedy horror film, directed by Robert Bierman, written by Joseph Minion, and stars Nicolas Cage, María Conchita Alonso, Jennifer Beals, and Elizabeth Ashley. The film tells the story of a mentally-ill literary agent, whose condition turns even worse when he gets bitten by a vampire. It was a box office failure but went on to become a cult film.

== Plot summary ==
Peter Loew (Nicolas Cage) is a driven literary agent, who is slowly going insanity|insane. He works all day, and club hops at night, with little in his life but one night stands and the pursuit of money and prestige. He sees a therapist (Ashley) frequently, and during these sessions, his declining mental health becomes clear through a series of increasingly bizarre rants that eventually begin to scare even his psychiatrist. After taking a girl he met in a club named Jackie (Kasi Lemmons) back to his place, a bat flies in through his window, scaring them both.

At his next session he mentions to his therapist that the bat aroused him. After visiting an art museum with Jackie the next day, he ditches her and she leaves an angry message on his phone.
 
Loew meets Rachel (Beals) at a night club, and takes her home. She pins him down, reveals fangs, and feeds on him. He soon begins to believe that he is changing into a vampire. He stares into a bathroom mirror and fails to see his reflection; he wears dark sunglasses during the day; and, when his "fangs" fail to develop, he purchases a pair of cheap plastic vampire teeth. All the while, Rachel visits him nightly to feed on his blood.

He experiences mood swings and calls Jackie back apologetically, asking to meet her at a bar. As he is about to leave, a jealous Rachel appears and beckons him back inside. A dejected Jackie eventually leaves the bar and leaves an angry note on his door asking him to leave her alone.

A subplot concerns a secretary working at Loews office, Alva Restrepo (Alonso). Loew torments her by forcing her to search through an enormous file for a 1963 contract. When she fails to find the contract, he at first browbeats and humiliates her, then visits her at home, and finally attacks and attempts to bite her at the place they both work.  She mistakes the attempt to drink her blood as a rape attempt, causing her to pull out a gun, and Loew begs her to shoot him. Since it is only loaded with blanks, she fires at the floor to scare him off. He eventually overpowers her and mocks her rape-assumption by ripping her shirt open and knocking her down. He then takes the gun and attempts to fire it in his mouth, but after doing it twice, the blanks do not kill him.

He goes out to a club wearing his vampire teeth, and begins to seduce a woman, but when he gets too grabby she slaps him off, but he then overpowers her and bites her neck, having taken out the fangs and using his real teeth. He then puts the plastic fangs back in. Leaving the club, Loew has a brief, ambiguous encounter with Rachel: she admits to knowing him, but gives the impression that they have not been in contact for a long period. He accuses her of being a vampire, and is expelled from the club.

Alva wakes up with her shirt ripped open, possibly thinking she was raped, and eventually tells her brother about the sexual assault, and he goes after Loew to seek revenge. 
Loew is wandering the streets in a blood-spattered business suit, talking to himself. In a hallucinatory exchange, he tells his therapist that he raped someone and also murdered someone else. Based on a newspaper, the latter appears to be true, as the girl he bit in the club is announced dead. As Loew returns to his now-disastrous apartment (which hed been using as a sort of vampire cave) Alva points out Loew to her brother, who pursues him inside his home with a tire iron.

In the midst of argument with an imaginary romantic interest (supposedly a patient of his psychiatrist) he begins to retch again from the blood he had swallowed, and crawls under an upturned sofa. Alvas brother finds him and upturns the sofa, and Loew holds a large broken shard of wood to his chest as a makeshift stake, repeating the gesture he had made earlier to strangers on the street when he had asked them to stake them. Alvas brother, in a rage, pushes down on the stake and it pierces Loews chest. The movie ends with Loew envisioning the vampire-Rachel one last time.

==Cast==
*Nicolas Cage as Peter Loew
*María Conchita Alonso as Alva Restrepo
*Jennifer Beals as Rachel
*Elizabeth Ashley as Dr. Glaser
*Kasi Lemmons as Jackie
*Bob Lujan as Emilio
*Jessica Lundy as Sharon
*Johnny Walker as Donald
*Boris Leskin as Fantasy Cabbie Michael Knowles as Andrew
*John Michael Higgins as Ed
*Jodie Markell as Joke Girl Marc Coppola as Joke Guy
*David Hyde Pierce as Theater Guy (as David Pierce)
*Amy Stiller as Theater Girl

==Release==
Vampires Kiss was released June 2, 1989.  It grossed $725,131 in the US.   It was released on home video in August 1990.   MGM released it on DVD in August 2002,  and Scream Factory released it on Blu-Ray in February 2015. 

==Reception== cult following Kevin Thomas of the Los Angeles Times called it "a sleek, outrageous dark comedy thats all the funnier for constantly teetering on the brink of sheer tastelessness and silliness."   Hal Hinson of The Washington Post called the film "stone-dead bad, incoherently bad", but said that Cages overacting must be seen to be believed.   Carrie Rickey of The Philadelphia Inquirer called it an "imaginative, if warped, black comedy" that "succeeds as a wicked allegory of What Men Want".   Reviewing the film on Blu-Ray, Anthony Arrigo of Bloody Disgusting wrote, "The film may not work very well as a comedy, but theres enough of a dark derangement present to make it almost unsettling." 

==See also==
*Vampire film

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 