Absurd (film)
 
{{Infobox film
| name           = Absurd (Rosso Sangue)
| image          = Absurdposter.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Joe DAmato
| producer       = Joe DAmato   Donatella Donati
| writer         =  George Eastman
| story          = George Eastman
| based on       = 
| narrator       = 
| starring       = Ian Danby   Ted Rusoff   Annie Belle   Katya Berger   Kasimir Berger   Edmund Purdom   George Eastman   Hanja Kochansky   Charles Borromel
| music          = Carlo Maria Cordio
| cinematography = Joe DAmato
| editing        = George Morley
| studio         = Filmirage   Metaxa Corporation
| distributor    = Medusa Pictures
| released       =  
| runtime        = 96 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
 George Eastman. The film is a Spiritual successor|follow-up to Antropophagus.
 Video Nasties of the United Kingdom, and became one of 39 titles to be successfully prosecuted under the Obscene Publications Acts in 1984. It was originally released in both a cut and uncut version with identical sleeve design by Medusa Home Video in 1981. The original tape is sought-after and is an expensive collectable among fans.
 gore films right wing politics and committed murder in 1993. 

The film was considered, at the time of its release, as a "sequel" to the  s for a period of time.

== Plot ==
 disembowel him, but he is revived later in a local hospital. The madman escapes after brutally murdering a nurse, and goes on a killing spree. The priest informs the hospital and authorities that the only way to kill Mikos is to destroy the cerebral mass.

While attacking a motorcyclist after escaping from the hospital, Mikos is struck by a hit-and-run driver. The driver of the car, Mr. Bennett, and his wife are going to a friends house to watch a football game, leaving their two children at home with a babysitter. Their daughter Katia is confined to her bed because of a problem with her spine, while her younger brother believes that the Bogeyman is coming to get him.
 drawing compasses. She then stumbles down the hallway as the blinded killer staggers after her. He stalks her through the house, but Katia manages to elude him. The priest arrives and struggles with Mikos, and Katia grabs an axe from a decorative suit of armor and decapitates Mikos with it. The police and the rest of the family arrive to discover Katia standing in the doorway, covered in blood holding Mikoss severed head.

== Background ==

Absurd is in many ways a non-sequel to Anthropophagus, as the only real connections between the two films – besides George Eastman and Joe DAmato – is the presence of a homicidal man (played by George Eastman in effectively the same role as the one he played in the first film) who is disemboweled in both films, and who comes from a Greek island.
 a babysitter a silent and seemingly indestructible killer. Director DAmato also attempted to make the film more attractive to the American market by setting it in the States, even though it was shot in Italy.

== Release ==

Absurd was released in October 1981 in Italy.
 video nasties in 1983 in its uncut state in the UK, but a version was released theatrically with two minutes and 23 seconds of cuts to it that same year.

An uncut DVD version of the film was released under the French title, Horrible, via Mya Communication on July 28, 2009. Also, an uncut DVD version including a long version of the film was released under the German title Absurd via XT-Video on December 15, 2010. An old VHS release exists under the title Monster Hunter.

== Cast ==
 George Eastman as Mikos Stenopolis
* Annie Belle as Emily
* Charles Borromel as Sergeant Ben Engleman
* Katya Berger as Katia Bennett
* Kasimir Berger as Willy Bennett
* Hanja Kochansky as Carol Bennett
* Ian Danby as Ian Bennett
* Ted Rusoff as Doctor Kramer
* Edmund Purdom as Father
* Cindy Leadbetter as Peggy
* Lucia Ramirez as Angela
* Mark Shannon as Man on TV
* Michele Soavi as Biker
* Martin Sorrentino as Deputy
* Goffredo Unger as Machine Shop Owner
* James Sampson as Black cop (uncredited)

 

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 