Alma (film)
 
{{Infobox film
| name = Alma
| image = Alma_(2009_short_film)_poster.jpg
| alt = Alma poster
| director = Rodrigo Blaas
| producer = Cecile Hokes Nina Rowan
| writer = Rodrigo Blaas
| starring = Mastretta
| editing =
| studio =
| distributor =
| released =  
| runtime = 5:29
| country = Spain
| language = None
}}
Alma is a 2009 Spanish  " in Spanish means "soul".

==Plot==
On a snowy day in Barcelona, a girl named Alma is wandering a street. Encountering a chalkboard inscribed with the countless given names of various children, she includes her own name. Before continuing her stroll, where she is suddenly captivated by a toy store overflowing with dolls across from the chalkboard. Taking notice of a doll physically identical to her through a window that resembles the mouth of a monster, she tries to enter the vacant, deserted, silent shop to retrieve the toy for herself. But the front door is locked. After giving up trying to open it, Alma begins to walk away, only for the door to mysteriously open. She returns and enters the store. 

Alma tries to retrieve the doll but nearly steps on a small toy boy riding a bike who tries to escape but the door closes in front of him. As Alma starts to climb a shelf the doll somehow moved to, the eyes of the other children dolls suddenly start moving and watching her. While trying to snatch it off of the shelf, the moment she touches it a fast sequence of various brief, quick clips of petrifying footage flashes across the screen before suddenly taking focus on the entire shop from Almas perspective on the shelf-top. 

Afterwards, it is revealed that Almas inner consciousness has somehow been withdrawn from her body into that of the doll resembling her. As all of the other dolls sitting atop the shelf suddenly roll their eyes, focusing on her (in revelation of the dark, true intentions of that particular store), a different doll is raised to the shop window bearing resemblance to another child, implying that the cycle will continue on another victim.

==Film adaptation==
In October 2010, it was announced that DreamWorks Animation is developing an animated feature film based on Alma. Shorts director Rodrigo Blaas is again set to direct the feature, with Guillermo del Toro executive producing it.  In November 2011, it was reported that the studio has hired Megan Holley, a writer of Sunshine Cleaning, to write a script.  Del Toro, who is also helping with the story and the design work, said in June 2012 that the film was in visual development. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 
 