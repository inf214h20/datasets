The Life and Loves of Tschaikovsky
{{Infobox film
| name =  The Life and Loves of Tschaikovsky
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer =  Carl Froelich 
| writer =  Géza von Cziffra  (novel + screenplay)   Frank Thieß    Georg Wittuhn   Jean Victor
| narrator =
| starring = Zarah Leander   Aribert Wäscher   Hans Stüwe   Marika Rökk
| music = Theo Mackeben    
| cinematography = Franz Weihmayr   
| editing =  Gustav Lohse        UFA
| distributor = UFA
| released = 13 August 1939
| runtime = 94 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Carl Froelich and starring Zarah Leander, Aribert Wäscher and Hans Stüwe.  The film portrays the fictional relationship between the Russian composer Pjotr Iljitsch Tschaikowsky and an aristocratic woman who, unhappily married, falls in love with him and decides to secretly support his work financially. It premiered on 13 August 1939 at the Venice Film Festival.

==Cast==
* Zarah Leander as Katharina Alexandrowna Murakina
* Aribert Wäscher as Michael Iwanowitsch Murakin  
* Hans Stüwe as Pjotr Iljitsch Tschaikowsky  
* Marika Rökk as Nastassja Petrowna Jarowa, Dancer  
* Leo Slezak as Prof. Otto Hunsinger  
* Fritz Rasp as Porphyr Philippowitsch Kruglikow, Kritiker  
* Paul Dahlke as Iwan Casarowitsch Glykow, Music publisher 
* Hugo Froelich as Vater Jarow  
* Karl Haubenreißer as Gruda, Konzertagent  
* Karl Hellmer as Stepan, Butler  
* Wolfgang von Schwindt as Onkel Jarow 
* Kurt Vespermann as Ferdyschtschenko  
* Traute Bengen as Mother waiting at Station  
* Eduard Bornträger as Schwager Jarow 
* Ernst Dumcke as Dmitri Pawlowitsch Miljukin  
* Max Harry Ernst as Guest at Concert  
* Claire Glib as Eine dicke Dame  
* Lotte Goebel as Sonja, Katharinas Maid  
* Grete Greef-Fabri as Mutter Jarrow  
* Karl Hannemann as Pjotr, Murakins Butler  
* Eva Immermann as Eine junge Fürstin  
* Gerda Kuhlmey as Katharinas Guest  
* Maria Loja as Elsa Siebeneiner  
* Benno Mueller as Gast beim Konzert  
* Maria Reisenhofer as Eine alte Fürstin  
* Ferdinand Robert as Guest at Masked Ball  
* Franz Stein as Dr. Ossorgin  
* Arnim Suessenguth as Großfürst Konstantin Konstantinowitsch  
* Leopold von Ledebur as Ein General  
* Eva Wegener as Katharinas Guest  

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.
* Reimer, Robert C. & Reimer, Carol J. The A to Z of German Cinema. Scarecrow Press, 2010.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 