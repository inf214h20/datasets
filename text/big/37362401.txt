Ducks and Drakes
{{infobox film
| name           = Ducks and Drakes
| image          =Ducks and Drakes (1921) - Ad 1.jpg
| image_size      =190px
| caption        =Ad for film
| director       = Maurice Campbell
| producer       = Realart Pictures Elmer Harris (screen story & scenario)
| cinematography = H. Kinley Martin
| editing        =
| distributor    = Paramount Pictures
| released       = February 1921 reels
| country        = United States
| language       = Silent (English intertitles)
}} silent comedy Jack Holt. Elmer Harris provided the story and screenplay. A copy is held at the Library of Congress.    

==Plot==
Based upon a summary in a film publication,    Teddy Simpson (Daniels) is a wealthy young orphan who, instead of marrying Rob Winslow (Holt), whom her Aunty Weeks (Kelso) has selected for her, is bent upon getting into trouble by seeking adventure and through her flirtatious ways. Robs friends, victims of her telephone flirtations, offer to help him cure her. Part of the cure involves Teddy taking a ride with Tom Hazzard (Lawrence) to an exclusive gun club, with the other conspirators making things so warm for her that she is cured for all time. When Rob calls her the next day, he finds her ready to consent to a speedy wedding.

==Cast==
*Bebe Daniels - Teddy Simpson Jack Holt - Rob Winslow
*Mayme Kelso - Aunty Weeks
*Edward Martindel - Dick Chiltern
*W. E. Lawrence - Tom Hazzard
*Wade Boteler - Colonel Tweed
*Maurie Newell - Cissy
*Elsie Andrean - Mina

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 