Vanamala
 

{{Infobox film
| name           = Vanamala
| image          = Vanamala.jpg
| image_size     =
| caption        = Promotional Poster
| director       = G Viswanath
| producer       = V&C Productions
| writer         = G Viswanath
| screenplay     = G Viswanath
| music          = PS Divakar
| released       =  
| cinematography = Arumukham
| editing        =
| studio         =
| lyrics         =
| distributor    = Central Pictures Release, Kottayam
| starring       = P. A. Thomas, Ammini
| country        = India Malayalam
}}
 Indian Malayalam Malayalam film, directed by G Viswanath and produced by V&C Productions.  The film stars P. A. Thomas, Ammini in lead roles. The film had musical score by PS Divakar. It is the first jungle movie in Malayalam. Also the debut film of Neyyattinkara Komalam, director G .Viswanath, lyricist P. Kunjukrishna Menon, singer Jikki and cameraman Arumugham. 

==Cast==
* P. A. Thomas
* Ammini
* Muthukulam Raghavan Pilla
* Baby Lakshmi
* Karthikeyan Nair
* Sumathikkutty Amma
* Kamala Bhargavan
* Neyyaattinkara Komalam
* Kanchana
* Raju
* Ambalappuzha Krishnamoorthy
* Kandiyoor Parameshwaran Pillai
* SP Pillai

==References==
 

==External links==
*  

 
 
 


 