Lilla Jönssonligan och cornflakeskuppen
{{Infobox film
| name           = Lilla Jönssonligan och cornflakeskuppen
| image          = Lillajonssonligancornflakes.jpg
| caption        =
| director       = Christjan Wegner
| producer       = Mikael Hylin Maria Nordenberg
| writer         = Mikael Hylin Christjan Wegner
| starring       = Kalle Eriksson Jonathan Flumee Fredrik Glimskär
| music          = Michael B. Tretow AB
| released       =  
| runtime        = 83 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
Lilla Jönssonligan och cornflakeskuppen ( ) is a Swedish film and the first out of four films in the Lilla Jönssonligan film series. It was released on November 29, 1996, in Sweden and was directed by Christjan Wegner. http://www.imdb.com/title/tt0116883/ 
 TV3 and TV4 (Sweden)|TV4.  )  It was released in Germany on September 7, 2000, as  Die Jönnson Bande & der Cornflakes-Raub. 

==Synopsis==
Charles-Ingvar "Sickan" Jönsson is a new student in Dynamit-Harrys and Ragnarss school. He instantly gets bullied by Junior and Biffen for his nerdy look. The year is 1953 and small cards called filmisar, that are found in cornflakes boxes, are popular at the school. Sickans first coup will be to get as many filmisar as he can by sneaking in to the cornflakes factory with his newly made friends Harry and Ragnar.

==Cast==
*Kalle Eriksson - Charles-Ingvar "Sickan" Jönsson 
*Jonathan Flumée - Ragnar Vanheden
*Fredrik Glimskär - Dynamit-Harry
*Jonna Sohlmér - Doris
*Anders Öström - Junior Wall-Enberg
*Mats Wennberg - Biffen 
*Peter Rangmar - Sigvard Jönsson Cecilia Nilsson - Tora Jönsson
*Isak Ekblom - Sven-Ingvar Jönsson 
*Loa Falkman - Oscar Wall-Enberg 
*Lena T. Hansson - Lilian Wall-Enberg 
*Micke Dubois - Loket Jerry Williams - Einar Vanheden
*Mona Seilitz - Rut Vanheden 
*Cecilia Häll - Vivi Vanheden

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 

 