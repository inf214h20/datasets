Watchers of the Sky
{{Infobox film
| name           = Watchers of the Sky
| image          = Watchers of the Sky poster.jpg
| caption        = Promotional poster
| director       = Edet Belzberg
| producer       = Edet Belzberg Amelia Green-Dove Kerry Propper
| writer         = 
| starring       =
| music          = Dougie Bowne
| cinematography = Mai Iskander Jerry Risius Nelson Walker III Sam Cullman Edet Belzberg
| editing        = Jenny Golden Karen Sim
| studio         = Propeller Films The Unofficial Man
| distributor    = Music Box Films
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Watchers of the Sky is a 2014 American documentary film directed by Edet Belzberg.   The film premiered at the 2014 Sundance Film Festival on January 20, 2014 in the U.S. Documentary Competition.  It won the two awards at the festival.       It went on to win the Jonathan Daniels Award at the Monadnock International Film Festival  and the Ostrovsky Award for Best Documentary Film at the Jerusalem Film Festival. 

After its premiere at the Sundance Film Festival, Music Box Films acquired the US distribution rights. The film was released in October 2014.    Films We Like released it in Canada  and Madman Entertainment released it in New Zealand and Australia. 

==Plot== Prosecutor of the International Criminal Court; and Emmanuel Uwurukundo, head of operations for refugee camps in Chad set up by the United Nations High Commissioner for Refugees in the War in Darfur. The film is based on Powers Pulitzer Prize-winning book, A Problem from Hell.

==Reception== Variety said that "Edet Belzbergs sweeping survey of global genocide is an impressive and artful cinematic thesis of palpable substance."  Duane Byrge of The Hollywood Reporter gave the film positive review and said that "An exhaustive, complex look at genocide with a sobering and historically predictable prognosis. "  Steve Greene from Indiewire in his review said that "As a documentary, "Watchers of the Sky” shows how even capturing that progress is a monumental task in itself."  Amber Wilkinson in her review for Eye for Film gave the film four star out of four and said that "There is quite a lot of onscreen reading to be done as well as watching, but the passages of Lemkin that Belzberg chooses are always well-illustrated either by the animation or archival footage."  Entertainment Weekly called it "fiery" and "essential" and Joshua Brunsting of Criterioncast.com named it one of 2014s best and said it was "a tour-de-force."  

==Accolades==
 
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3"| 2014 Sundance Film Festival
| U.S. Grand Jury Prize: Documentary
| Edet Belzberg
|  
|-
| Editing Award: U.S. Documentary
| Jenny Golden and Karen Sim
|    
|-
| U.S. Documentary Special Jury Award for Achievement for Use of Animation
| Edet Belzberg
|    
|}
 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 