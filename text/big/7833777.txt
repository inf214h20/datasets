Such a Gorgeous Kid Like Me
{{Infobox film name            = Such a Gorgeous Kid Like Me  image           = Belle_moi.jpg image_size      = 225px caption         = theatrical release poster director        = François Truffaut producer        = Marcel Berbert based on        = Such a Gorgeous Kid Like Me by Henry Farrell writer          = Jean-Loup Dabadie François Truffaut starring        = Bernadette Lafont music           = Georges Delerue cinematography  = Pierre-William Glenn editing         = Yann Dedet studio          = Les Films du Carrosse distributor     = Columbia Pictures  released        = 12 September 1972 (France) 26 June 1973 (UK) March 1973 (US) runtime         = 100 minutes country         = France language  French
|budget          = gross = 684,919 admissions (France) 
}}
 1972 Cinema French film 1967 novel of the same name.

==Plot==
Stanislas Previne is a young sociologist, preparing a thesis on criminal women. He meets Camille Bliss in prison to interview her. Camille is accused of having murdered her lover Arthur and her father. She tells Stanislas about her life and her love affairs.

Stanislas, much to the frustration of his secretary, who also has a crush on him, soon falls in love with Camille and works to find the evidence to prove her innocent. His secretary tries to convince the sociologist that Camille is a manipulative slut but he cannot be convinced. Through investigation the sociologist and his secretary find a young boy, an amateur filmmaker, who has captured the evidence they need on film to secure Camilles release from prison.

Once free, Camille, who has always loved music and has seduced the cabaret singer Sam Golden earlier in the film, becomes a cause célèbre and a singing star. Stanislas meets her after a performance and she seduces him at her home; But her husband (who is cuckolded many times during the film) discovers them and beats him up. Camille kills her husband and then plants the gun on her passed out paramour.

When Stanislas is imprisoned for murder, Camille will do nothing to help the man who once freed her. As he cleans up the prison in the films final segment, the camera pans to show Stanislas secretary typing a manuscript in a nearby balcony, presumable the thesis that Stanislas began, but this time preparing one that will expose Camille as the manipulative seductress that Stanislas has discovered her to truly be.

==Cast==
  
* Bernadette Lafont as Camille Bliss
* Claude Brasseur as Maître Murene
* Charles Denner as Arthur
* Guy Marchand as Roger aka Sam Golden
* André Dussollier as Stanislas Prévine
* Anne Kreis as Hélène
* Philippe Léotard as Clovis Bliss
* Gilberte Géniat as Isobel Bliss
* Michel Delahaye as Marchal
* Danièle Girard as Florence Golden
 
* Jérôme Zucca as the child amateur filmmaker
* Gaston Ouvrard as old prison warder
* Martine Ferrière as prison secretary
* Jacob Weizbluth as Alphonse, the mute
* Marcel Berbert as the bookseller (uncredited)
* Jean-Loup Dabadie as a photograph (uncredited)
* Annick Fougery as teacher (uncredited)
* Jean-François Stévenin as the newspapers seller (uncredited)
* François Truffaut as a journalist (voice) (uncredited) 
 

==References==
Notes
 

== External links ==
*  

 

 
 
 
 
 


 