A Dog's Will
{{Infobox film
| image          = O Auto da Compadecida.jpg
| caption        = Theatrical release poster
| director       = Guel Arraes
| producer       = André Cômodo
| writer         = Adriana Falcão Guel Arraes João Falcão
| based on       = Auto da Compadecida by Ariano Suassuna
| starring       = Matheus Nachtergaele Selton Mello Fernanda Montenegro
| music          = Grupo Sá Grama Sérgio Campelo
| cinematography = Felix Monti
| studio         = Globo Filmes Lereby Produções
| distributor    = Columbia Tri Star
| released       =  
| runtime        = 104 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$11,496,994 ($4,903,192)
}}
 play of almost the same name by Ariano Suassuna, with elements of other Suassunas play, The Ghost and the Sow, and Torture of a Heart.

==Plot==
The plot concerns the adventures of João Grilo (Matheus Nachtergaele) and Chicó (Selton Mello), the most cowardly of men.  Both struggle for daily bread in a telling representation of the life of the poor in North-East Brazil (O Nordeste) and gull a series of comical stereotypes - baker, landowner, and priest - in a series of interrelated episodes united by the passion of the adulterous bakers wife for her little dog who dies from eating the food she supplies to them as occasional workers, and the daughter of the landowner Antônio Morais (Paulo Goulart) - a magnificent comic character who represents the colonial pretensions of the erstwhile "colonial" class who owned the great estates (or "fazendas") of the region in which sugar production was central to a once-booming economy. 

The Catholic Church gets a particularly lively ragging for its combination of simony and superstition as represented by the put-on parish priest (Rogério Cardoso) and a domineering bishop (Lima Duarte) no less self-interested than him - while a larger than life bandit Severino de Aracaju (Marco Nanini), who attacks the town and slaughters the inhabitants, is ultimately forgiven by Jesus in a posthumous denouément set in Heaven.  There the fate of the main characters is mercifully arbitrated by in a court-room contest between Satan and a black Jesus, with the Virgin Mary (Fernanda Montenegro) interceding as the Virgin in keeping with her prayer-book promise. Odd and extravagant as this tacked-on scene is, it conveys the morality of the film better than the antecedent scenes since every character manages to reveal a saving grace as well as demonstrating the unforgiving harshness of the Nordeste environment which they all share and endure in different ways. 

Last to die in the bandits onslaught is João himself - given as "Jack" in the English subtitles. An intensely witty and ingenious rogue in the best picaresque tradition, he contrives to spin everyone around his finger throughout the narrative and ends by getting Severino to order his side-kick to shoot him dead so that he can meet his revered saint in heaven for some minutes on the understanding that a miraculous harmonica which João has ingeniously convinced him possesses the power of bringing the dead back to life - in this case Chicó rigged up with a little balloon of blood - will effect his speedy resurrection. In heaven with the rest, João is more or less master of his fate and manages to down-face the Devil himself (Luis Melo) in the little matter of eternal damnation. With becoming modesty he refuses to claim any personal virtues and turns down the Virgins offer of a purgatorial sentence but accepts instead the compassionate offer of a return to earth to sin no more - but do not expect a pious conversion! 

His resurrection coincides with the moment when Chicó is digging him a sandy grave, triggering a comical mixture of dismay and joy as he rises from the wagon on which he has been laid out in death. Together the two donate their ill-gotten gains from the other deceased characters to the Virgin, to whom  Chicó promised such a recompense if his friend came back to life  - hardly expecting that he would. Now they proceed with Plan B: to get Chicó married to Rosinha, the daughter of the landlord. Here a marriage bargain involving a lump-sum payment or the skin off Chicós back is foiled by reference to the legal contrivance familiar from The Merchant of Venice of William Shakespeare - that is, the skin may be owing but not a drop of blood must be taken with it. João, Chicó and the bride now make their escape and enter gleefully into a life of penury on the dusty roads of the region, only to meet with a beggar of dark complexion whom we know to be Jesus. It is the bride - now reduced to penury for the first time in her existence - who breaks bread with him while the others philosophise about Jesuss propensity to test the faithful in just such a way. At the same time they playfully doubt that Jesus could have be so brown - as João puts it while still in Heaven - reiterating the anti-racist message of the script and its original. 

The film wanders in and out of realism, commedia dellarte and Morality Play but consistently holds the focus on the realities of Brazilian life in the 1950s period when the original text was written. When the exoneration of the desperate bandit Severino is delivered by Jesus at the behest of his Holy Mother, a series of black-and-white stills of rural poverty in the Nordeste  
Brazil are screened, giving the whole a sense of social feet-on-ground which belies its comic brio. In spite of plot variations, the film is extremely faithful to the comic spirit and moral ethos of its literary source, and remains a classic of Brazilian cinema with a faithful audience in the region where it is set - an audience that knows every line, every jape and every twist of the busy plot. It deserves to be better known world-wide if only for the performances (without exception) but requires, to some extent, a specifically Brazilian sense of context for its sympathetic appreciation. Even in that context, the Nordeste sentiment is markedly in the ascendant.

==Cast==
* Matheus Nachtergaele as João Grilo (Jack the Cricket)
* Selton Mello as Chicó
* Fernanda Montenegro as Virgin Mary Cangaceiro Severino de Aracaju
* Denise Fraga as Dora
* Lima Duarte as Bishop
* Rogério Cardoso as priest João
* Diogo Vilela as Eurico
* Maurício Gonçalves as Jesus Christ
* Virginia Cavendish as Rosinha
* Paulo Goulart as major Antônio Morais
* Luís Melo as the Devil Bruno Garcia as Vicentão
* Enrique Diaz as Severinos henchman
* Aramis Trindade as corporal Setenta

==Reception==
The film was critical and commercial success in Brazil—receiving four awards at the 2nd Grande Prêmio Cinema Brasil  and grossing Brazilian real|R$11,496,994 ($4,903,192) with a 2,157,166 viewership —,and in some South American countries like Chile and Venezuela.  The film was not as well received in English-speaking countries. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 