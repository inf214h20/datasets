Ek Chitthi Pyar Bhari
 
{{Infobox film
| name           = Ek Chitthi Pyar Bhari
| image          = Ek Chitthi Pyar Bhari.jpg
| caption        = Ek Chitthi Pyar Bhari Film Poster
| director       = Vijay Sadanah
| producer       = Savitri M, Mrs Raj Geol
| Lyrics         = Indeevar, Verma Malik
| starring       = Raj Babbar Reena Roy Baby Bulbul Sulochana Latkar
| music          = Kalyanji Anandji
| cinematography = Keki Mistry
| released       = 1 January 1985
| Run Time       = 139 Minutes
| country        = India
| language       = Hindi
}}
 Hindi Drama film, starring Raj Babbar and Reena Roy and directed by Vijay Sadanah.

==Cast==

* Raj Babbar as Dr. Sunil Sharma
* Reena Roy as Aarti Saxena
* Sulochana Latkar as Mrs. Sharma (Sunils mom)
* Chaman Puri as Aartis father-in-law
* Baby Bulbul as Bulbul
* Jayshree T. as Sadhurams daughter
* Lalita Kumari as Aartis mother-in-law
* Jagdeep as Murugan
* C.S. Dubey as Bhiku
* Seema Deo as Hostel Manager
* Ramesh Deo as Kamal Nath
* Birbal as Kaanha
* Yunus Parvez as Khan
* Mohan Choti
* Manmohan Krishan  
* Polson

==Sound Track ==
* Ek Chitthi Pyar Bhari
* Ek Nanhi Si Ye
* O Tune Di Aawaaz
* Shadi Karke Bhi Me
* Yeh Hamari Tumari

==External links==
* 

 
 
 

 