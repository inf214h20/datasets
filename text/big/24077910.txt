Barney's Version (film)
 
{{Infobox film
| name           = Barneys Version
| image          = Barneys version.jpg
| border         = yes
| caption        = Film poster
| director       = Richard J. Lewis
| producer       = 
| writer         = Michael Konyves
| based on     =  
| starring       =  
| music          = 
| cinematography = Guy Dufaux
| editing        = Susan Shipton Entertainment One
| released       =  
| runtime        = 134 minutes
| country        = Canada
| language       = English
| budget         = $30 million
| gross          = $8.5 million 
}} same name by Mordecai Richler.  The film was nominated for the Golden Lion at the 67th Venice International Film Festival.  

==Plot==
Barney Panofsky is living with his best friend Boogie (Speedman) in Rome. He marries the mentally disturbed and unfaithful Clara Charnofsky (Lefevre) after she tells him she is pregnant with his child. Barney later finds out the child is not his, and he demands they separate. Clara commits suicide, and a devastated Barney decides to return home to Montreal.
 detox therapy, for a few days at Barneys lake house. He eventually finds Boogie in bed with his wife. Barney is at first overjoyed that he has an excuse to divorce her and pursue Miriam, but questions Boogies integrity. The two argue, firing rounds from Barneys gun into the air before Barney collapses onto his dock and passes out, and a drunk Boogie falls back into the lake. When Barney awakens, it appears that he has shot Boogie. An abusive detective (Mark Addy) tries to beat a confession out of Barney until Barneys father, Izzy (Hoffman), intervenes.  Barney is let go when they cannot find a body.  Barney continues to believe that Boogie ran away and throughout the movie waits for him to reappear.

With his divorce finalized, Barney asks Miriam out on a date. He travels to New York City to meet her, and they finally begin a relationship. They marry and have two children as Barney gets a job producing a television series. Izzy later dies in a brothel, causing Barney to laugh and cry and call his father a "King". Barney and Miriam live happily until, on another vacation to the lake house, Barney meets Blair (Bruce Greenwood), who is trying to get a job in Miriams line of work. Both Blair and Miriam travel to the city to try to use Miriams contacts. Barney gets drunk at a bar and ends up having sex with a former actress on his show. Barney tells Miriam about his infidelity and the two divorce. Miriam later marries Blair, which contributes to Barneys mental deterioration.

Years later, Boogies body is discovered in the mountains near the lake house, apparently dead from injuries suggestive of a sky diving accident. Barney continues his slide into progressive dementia, which causes his daughter to look after him. He realizes he is losing his memory and, while they lunch at their favorite restaurant, begs Miriam to agree to be buried next to him. She agrees to think about it, and begins crying in the restroom. When she returns to the table, Barney is gone. She finds him wandering, whereupon he starts rambling about their life together. Barneys condition worsens until his death.  While his children are helping settle some of his affairs at the lake house, they observe a "water bomber" plane scoop up water from the lake and dump it on the mountain side fire—showing the children (and Barney) what had probably happened to Boogie. (He had been scooped up by a water bomber plane by accident and dropped in the mountains.) The final scene shows Miriam visiting Barneys grave, leaving roses at a tombstone bearing both of their names.

==Cast==
* Paul Giamatti as Barney Panofsky
* Dustin Hoffman as Israel Izzy Panofsky, the father
* Rosamund Pike as Miriam Grant, the third wife
* Minnie Driver as the second wife
* Rachelle Lefevre as Clara Charnofsky, the first wife
* Anna Hopkins as Kate Panofsky, the daughter
* Jake Hoffman as Michael Panofsky, the son
* Bruce Greenwood as Blair
* Mark Addy as Detective OHearne
* Paula Jean Hixson as Grumpys Bartender
* Scott Speedman as Bernard Boogie Moscovitch
* Thomas Trabacchi as Leo
* Clé Bennett as Cedric
* Saul Rubinek as Mr. Charnofsky, the first father in-law
* Harvey Atkin as the second father in-law
* Macha Grenon as Solange, the soap opera actress

There were also cameos by Canadian directors Atom Egoyan (early director of Barneys soap opera Constable OMalley of the North), David Cronenberg (later director of Barneys soap), Paul Gross (star in Barneys soap), Denys Arcand (Jean, the maître d at both of Barney & Miriams luncheons beside the duck pond at Montreals Ritz-Carlton), & Ted Kotcheff (train conductor).

==Production==
 
After being in development for 12 years, the film was released in September 2010 with Paul Giamatti in the title role. It was directed by Richard J. Lewis and produced by Robert Lantos from a screenplay by Michael Konyves. Filming took place in Montreal, Lake Memphremagog, Rome and New York. Special effects were produced by Modus FX in Montreal.

==Reception==
===Critical reception===
Barneys Version has received mostly positive reviews. Review aggregation website Rotten Tomatoes reports that 80% of critics have given the film a positive review based on 125 reviews, with an average score of 6.7/10. 

===Box office===
The film grossed $472,892 in Canada over its first few weeks.  As of April 17, 2011, the film had grossed $4,337,300 in the United States and a total of $8 million worldwide. Most of the worldwide box office was in Italy 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
|- Academy Awards 
| February 27, 2011 Best Makeup
| Adrien Morot
|  
|- Genie Awards 
|rowspan="11" | March 10, 2011 Best Picture
|
|  
|- Best Actor
| Paul Giamatti
|  
|- Best Actress
| Rosamund Pike
|  
|- Best Supporting Actor
| Dustin Hoffman
|  
|- Best Supporting Actress
| Minnie Driver
|  
|- Best Director
| Richard J. Lewis
|  
|- Art Direction/Production Design
| Claude Paré and Élise de Blois
|  
|- Costume Design
| Nicoletta Massone
|  
|- Adapted Screenplay
| Michael Konyves
|  
|- Original Score
| Pasquale Catalano
|  
|-
| Make-Up
| Adrien Morot and Micheline Trépanier
|  
|- Golden Globes 
| January 16, 2011 Best Actor - Comedy/Musical Film
| Paul Giamatti
|  
|- London Film London Film Critics Circle Awards  February 10, 2011
| Best British Actress
| Rosamund Pike
|  
|-
| Best British Supporting Actress
| Minnie Driver
|  
|- Satellite Awards 
| December 19, 2010 Best Supporting Actress
| Rosamund Pike
|  
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 