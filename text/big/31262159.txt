The Old Wives' Tale (film)
{{Infobox film
| name           = The Old Wives Tale 
| image          =
| caption        =
| director       = Denison Clift
| producer       = 
| writer         = Arnold Bennett (novel)   Denison Clift
| starring       = Fay Compton Florence Turner Henry Victor   Mary Brough
| music          = 
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = December 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Denison Clift and starring Fay Compton, Florence Turner and Henry Victor.  It is based on the 1908 novel The Old Wives Tale by Arnold Bennett.

==Cast==
* Fay Compton - Sophie Barnes
* Florence Turner - Constance Barnes
* Henry Victor - Gerald
* Francis Lister - Cyril Povey
* Mary Brough - Mrs Barnes
* Joseph R. Tozer - Chirac
* Norman Page - Samuel Povey
* Drusilla Wills - Maggie
* Tamara Karsavina - Dancer

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 