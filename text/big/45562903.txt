Johnny (1993 film)
 
{{Infobox film		
| name = Johnny
| image = 
| caption = 
| director = 	Sangeeth Sivan
| producer = Sivan
| writer = Beeyar Prasad
| story = Sangeeth Sivan
| narrator =
| starring = 	Tarun Kumar  Santhi Krishna	
| music = Mohan Sithara
| cinematography =Santosh Sivan
| editing = Bose
| distributor = 
| released =	 1993
| runtime = 
| country =	 India Malayalam
| budget = 
| gross = 
| preceded_by = 
| followed_by =	
}}			

Johnny is 1993 period film directed by Sangeeth Sivan.  

==Reception==
Meera John Chakrabarthy of Sunday Times said "It’s hard to pick holes in this film. The technique is brilliant and the story, wholesome and clean. Little wonder then that the Malayalam film ‘Johnny’ is coasting along, having picked up two Kerala state awards for the ‘Best children’s film’ and the ‘Film critics’ award, and has been entered in several International film festivals, among them, Finland, Chicago and Iran". 

==Awards==
*Kerala State Film Award for Best Childrens Film

==References==
 

 
 
 


 