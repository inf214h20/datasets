A Summer You Will Never Forget
{{Infobox film
| name           = A Summer You Will Never Forget
| image          = A Summer You Will Never Forget Cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = A Summer You Will Never Forget cover
| director       = Werner Jacobs
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       =  Claus Biederstaedt
*Antje Geerk
*Karin Dor
 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 German drama film directed by Werner Jacobs and starring Claus Biederstaedt, Antje Geerk and Karin Dor. Its German title is Ein Sommer, den man nie vergißt. It was based on a novel by Marion Jahn.

==Cast==
* Claus Biederstaedt – Ernst Leuchtenthal 
* Antje Geerk – Marianne 
* Karin Dor – Christine von Auffenberg 
* Fita Benkhoff – Therese Leuchtenthal 
* Heli Finkenzeller – Mrs. Dr.Manning 
* Carl Wery – Fürst Aufenberger 
* Alexander Golling – Konsul Leuchtenthal 
* Eddi Arent – Ruprecht 
* Helga Martin – Anni 
* Benno Kusche – Dr. Bachmeier Tierarzt 
* Sascha Hehn – Peter Bachmeier

==External links==
* 

 

 
 
 
 
 
 


 