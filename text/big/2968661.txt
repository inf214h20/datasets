To Die For (1994 film)
 
 
 
{{Infobox film
| name = To Die For
| caption =
| image	= To Die For FilmPoster.jpeg Peter Mackenzie Litten
| producer = Johnny Byrne Peter Mackinzie Litten Paul McEvoy Ian Williams John Altman
| music = Roger Bolton John Ward
| editing = Jeffrey Arsenault
| distributor =
| released =  
| runtime = 101 minutes
| country = United Kingdom English
}}
 British comedy Peter Mackenzie Ian Williams, John Altman. Johnny Byrne, Roger Bolton.

==Plot summary==
Set in London in the early nineties, the film portrays the bitter-sweet lifestyles of a young gay couple in a fiery open relationship.   Mark is an acerbic drag queen with a sharp tongue, who finds it difficult to accept his much better-looking partners highly-promiscuous lifestyle of non-stop clubbing and cruising.  Worse still, Mark is HIV positive and his partner is negative.

Both of them are struggling to come to terms with Marks deteriorating condition.  Nowadays, Mark prefers to stay at home when not performing - working on his own panel of embroidery for an AIDS quilt memorial project.  Simon however, prefers to turn a blind eye to the situation and continues to cruise Londons gay bars at night looking for action.

Mark dies early on in the story and Simon becomes the focus of the story as he buries his feelings and continues his torrid sex life.  At first, It seems that hes totally unaffected by his lovers death.  But when Mark comes back to haunt him his life suddenly gets a lot more complex!  Especially as hes the only one who can see him.

It turns out that Mark has actually come back to help his partner to accept his true feelings and to encourage him to reassess his reckless lifestyle.  A lifestyle that he is sure will never bring him the happiness he seeks.  Eventually, Mark gets through to him and Simon breaks down and weeps for the very first time.

Marks work is done and he can leave his one-time lover to move on with his life.

==Main cast==
* Thomas Arklie - Simon Ian Williams - Mark
* Tony Slattery - Terry
* Dillie Keane - Siobbah John Altman - Dogger
* Jean Boht - Mrs. Downs
* Caroline Munro - Mrs. Pignon
* Ian McKellen - Quilt Documentary Narrator

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 