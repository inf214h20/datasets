Rentun Ruusu
{{Infobox film
| name           = Rentun Ruusu
| image          = RentunRuusu.jpg
| caption        = DVD cover
| writer         = Timo Koivusalo Vexi Salmi
| starring       = Martti Suosalo, Ilkka Koivula
| director       = Timo Koivusalo
| producer       = Timo Heinänen
| distributor    = Artista Filmi Oy Buena Vista Pictures
| released       = 12 January 2001 
| runtime        = 100 minutes Finnish
}} 2001 Finland|Finnish biographical film drama directed and written by Timo Koivusalo.
The film won an award and nomination at the Jussi Awards.

The film is based on the life of Antti Yrjö Hammarberg (Irwin Goodman) who is still today a very popular singer in Finland.

The film stars Martti Suosalo and Ilkka Koivula with Vexi Salmi as narrator.

The film premiered in Helsinki on 12 January 2001.

By 5 April 2001 it took $2,236,169 at the box office.

==Overview==
Martti Suosalo transforms himself into the character of Irwin Goodman, a Finnish singer and takes us through the singers life in the 1960s and 1970s. Written by his longtime friend and manager Vexi Salmi, the movie is apparently as close to the real life of Irwin as we can get. In addition to being the story of Irwin, the movie also looks at the transformation of Finnish society from the post-war years to the 70s and 80s.

==Main cast==
*Martti Suosalo ...  Irwin Goodman
*Ilkka Koivula ...  Vexi Salmi
*Vexi Salmi ...  Narrator
*Esko Nikkari ...  Väiski
*Eeva-Maija Haukinen ...  Irwins mom, Kirsti Hammarberg
*Hannu Kivioja ...  Kaspar
*Riitta Salminen ...  Riitta Feirikki-Hammarberg
*Matti Mäntylä ...  Toivo Kärki
*Tom Lindholm ...  Eikka
*Kunto Ojansivu ...  Alpo
*Raimo Grönberg ...  Honkanen
 
*Mikko Kivinen ...  Tappi Suojanen
*Seppo Sallinen ...  Keihänen
*Erkki Ruokokoski ...  Ilmari Kianto
*Timo Julkunen ...  Mape
*Maarit Niiniluoto ...  Radiohaastattelija
*Harri Ekonen ...  Lentäjä 1
*Aimo Santaoja ...  Lentäjä 2
*Matti Nurminen ...  Taksikuski torilla
*Kai Tanner ...  Taksikuski lentokentällä
*Raimo Viitanen ...  Nimismies
*Seppo Helenius ...  Virkamies
*Risto Luukko ...  Tuomari
*Sonja Saarinen ...  Kaija
*Tapani Mäki ...  Vartija 2
*Johanna Jokela ...  Nuori ihailijatyttö

==Minor cast==
 
*Jevgeni Haukka ...  Venäläinen tullimies 1
*Martti-Mikael Järvinen ...  Otto
*Timo Nissi ...  Eki
*Martti Palo ...  Erik Lindström
*Alexander Pritoup ...  Venäläinen tullimies 2
*Taneli Rinne ...  Verotarkastaja
*Raino Rissanen ...  Vartija 1
*Kielo Tommila ...  Aila
*Andrei Tsumak ...  Venäläinen tullimies 3
*Harry Viita ...  Ralliperuna

==Trivia==
*The film was the most watched Finnish movie of 2001.
*The name Rentun ruusu is from Antti Yrjö Hammarbergs song. Rentun ruusu album was his biggest and sold 125,000 copies.

==External links==
* 
*http://www.yle.fi/tv2draama/rentunruusu/index.html
*http://www.artistafilmi.fi/RentunRuusu_sivut/vexinmuistelu.html

 
 
 
 


 
 