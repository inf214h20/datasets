Aces Go Places 5: The Terracotta Hit
 
 
{{Infobox film
| name           = Aces Go Places 5: The Terracotta Hit
| image          = AcesGoPlaces5.jpg
| alt            = 
| caption        = DVD cover
| film name = {{Film name| traditional    = 新最佳拍檔
| simplified     = 新最佳拍档
| pinyin         = Xīn Zuì Jiā Pāi Dǎng
| jyutping       = San1 Zeoi4 Gaai1 Paak3 Dong3}}
| director       = Lau Kar-leung
| producer       = Karl Maka
| writer         = Chang Kwok Tse Nina Li Chi
| music          = Richard Lo Teddy Robin
| cinematography = Paul Chan Joe Chan Hung Hin Sing
| editing        = Wong Ming Lam
| studio         =  Cinema City Company
| released       =  
| runtime        = 103 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$20,032,206
}} action choreography by Lau Kar-wing. This is the fifth and final installment in the Aces Go Places (film series)|Aces Go Places film series. It was released in the United States as Mad Mission 5: The Terracotta Hit.

==Plot==
King Kong and Baldy, the "Aces," part ways in 1986 after a mission in Thailand to kidnap a woman on her way to marry her boyfriend (a rich man claiming to be her husband enlisted the Aces services) goes sour. Three years later, figures from the famous Terracotta Army and a Qing Dynasty bronze sword called the "Chinese Excalibur" is stolen during their transport to an exhibition in Hong Kong. Based on pictures that appear in the media, the two men are accused of the heist. By this time, King Kong is running an investment company that has long since been in the red, and Baldy - who sent his wife and son to Canada - is hiding in a Sai Kung houseboat from creditors who lent him money to invest in the stock market.
 MSS operative called the Chinese Rambo separately visits Baldy and King Kong (with indirect approval from the Hong Kong Police Force command, who have long since disowned them), both men decide to find those who framed them to clear their names. 

They discover that a brother-sister tandem calling themselves the "New Aces" took the pictures during the heist and wore face masks of the two mens likenesses while getting away with stealing the Chinese Excalibur. They interrogate them inside Baldys houseboat, and as the siblings try to escape, they plunge into the water and go back to the house during which a Chinese ship tows the houseboat. All four of them are sent to Beijing and imprisoned to answer for the crime. They are forced to undergo a staged execution until the Chinese Rambo offers them a chance to get out of prison in exchange for helping the Chinese government recover the figures. The four Aces agree to help recover the figures from the White Gloves syndicate. They begin training in martial arts because Beijing specifically orders that the figures must not be damaged by any means.  However, the Chinese Rambo calls off the training, explaining that the Chinese government will try to get the figures back through diplomatic means.

Despite the turn of events, the four Aces band together and proceed with the mission. A furious battle inside the White Gloves hideout, which even involves the use of the Chinese Excalibur, results in the quartet recovering the figures. King Kong, Baldy, and the New Aces join the Hong Kong police in sending off the Chinese Rambo, who is safeguarding the shipment back to China.

==Cast==
*Samuel Hui as King Kong
*Karl Maka as Baldy
*Leslie Cheung as Brother thief Nina Li Chi as Sister thief
*Conan Lee as the Chinese Rambo
*Melvin Wong as Bosss aide
*Ellen Chan as Ellen Danny Lee as Prisoner
*Fennie Yuen as Baldys niece
*Roy Cheung as Murderer King
*Maria Cordero as Woman in window
*Brad Kerner as Caucasian Boss
*Cho Tat-wah as Uncle Wah
*Ha Chi Jan as Rambos assistant
*Wayne Archer as Bosss thug
*Mark Houghton as Bosss thug
*Billy Chong as Bosss thug
*James Ha as Bosss thug
*Lau Kar-wing as Thai horse rider
*Hung Yan-yan as Thai horse rider
*Montatip Keawprasert as May Deborah Grant as Deborah
*Lu Yan as Lui Yin
*Jim James as police officer
*Ernest Mauser as police officer

==Box office==
This film grossed HK$20,032,206 during its theatrical run from 28 January to 22 February 1989 in Hong Kong.

==See also==
*Aces Go Places (film series)

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 