Whatever Lola Wants (film)

{{Infobox film
| name           = Whatever Lola Wants
| image          = Whatever Lola Wants (film).jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Nabil Ayouch
| producer       = Jake Eberts
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = Krishna Levy
| cinematography = Vincent Mathias
| editing        = Hervé de Luze
| studio         =  
| distributor    = Pathé|Pathé Distribution
| released       =  
| runtime        = 115 minutes
| country        =  
| language       =  
| budget         = $8,000,000 (apx)
| gross          = 
}} 2007 Cinema Canadian romantic drama film directed by Nabil Ayouch.  The film had its world premiere on December 11, 2007 at the Dubai International Film Festival and stars Laura Ramsey as an American postal worker who travels to Egypt to seek out a legendary belly dancer.  

In his book Film in the Middle East and North Africa Josef Gugler commented that at the time of its release, the film had a larger budget than any of the previous Moroccan films and was one of the highest grossing Moroccan films for 2008. 

==Plot==
Lola (Laura Ramsey) is a female postal worker that dreams of becoming an oriental dancer. After a friend encourages her to perform at a local restaurant, Lola captures the attentions of the handsome Zack (Assaad Bouab). Lola follows after him, but is crushed to find that Zack is to marry someone of his familys choosing. Lola decides to turn all of her energy into making her dreams a reality and tracks down Ismahan (Carmen Lebbos), a reclusive dancing star that retired due to a scandal involving a mysterious lover. Although reluctant, Ismahan is persuaded into giving Lola lessons and a friendship blossoms as a result. In no time Lola becomes a professional level dancer and attracts the interest of Nasser Radi (Hichem Rostom), a famous impresario. He takes her under his wing and under his tutelage Lola gets to dance at the prestigious Nile Tower. During this time Lola discovers that Nasser was Ismahans lover and that the two were kept apart because of social conventions but also because of their own pride. As Lolas career takes off, she manages to help reunite the two former lovers before returning to New York in order to take the art she loves to her fellow Americans.

==Cast==
 
* Laura Ramsey as Lola
* Assaad Bouab as Zack
* Carmen Lebbos as Ismahan
* Hichem Rostom as Nasser
* Achmed Akkabi as Yussef
* Nadim Sawalha as Adham
* Mariam Fakhr Eddine as Mrs. Aida
* Abdelkader Lofti as Choukri
* Roz Ryan as Postal Worker
* Hend Sabri as Fayza
* Milia Ayache as Yasmine
 

==Reception==
Critical reception for Whatever Lola Wants has been mixed,   and Variety (magazine)|Variety wrote that "despite a considerable number of cringe-inducing moments early on, the sheer brio of the heroines cross-cultural antics will win over auds by the end."  The Screen Daily wrote a mixed review, stating "Shot brightly in artsy-melodrama mode and garnished with an Arab-fusion soundtrack by Krishna Levy and TransGlobal Underground, Whatever Lola Wants is a polished film, but in the end it never really decides whether it wants to be a prettily packaged star-is-born tale or an all-guns-blazing social critique."  The Hollywood Reporter was also mixed, stating that while the films music and dancing was a highlight, the movie ultimately "fails to engage". 

==Release==
The films premiere was in United Arab Emirates December 11, 2007 at the Dubai International Film Festival.  In 2008, it screened in France and Norway.  In 2009, it screened in South Korea and in 2010 in Taiwan. It had its Television premiere on HBO Hungary in 2011 as Amit csak Lola akar. 
In 2009, it has DVD premieres in France, Netherlands, Sweden, and Finland.

==References==
 

==External links==
*   as archived March 28, 2008

 
 
 
 
   
 
 
 
 
 
 
 
 