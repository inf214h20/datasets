Victor (2009 film)
Victor 2009 French French feature film, a comedy. It was directed by Thomas Gilou.    It is an adaptation of a novel by French author Michèle Fitoussi,    who has also been a magazine editor.

The story involves the elderly Victor, a contest organized by a magazine, and a family who "adopts" the ailing old man. Complications ensue, often to comedic effect.

==Cast ==
* Pierre Richard as Victor Corbin 
* Clémentine Célarié as Sylvie Saillard
* Lambert Wilson as Jérôme Courcelle
* Antoine Duléry as Guillaume Saillard
* Sara Forestier as Alice
* Marie-France Mignal as Sylvies mother
* Eric Haldezos as Paco

==Notes==
 

==External links==
* 
*    at allocine.fr 

 
 
 
 