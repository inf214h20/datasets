Age 13
{{Infobox film
| name           = Age 13
| image          =
| caption        = 
| director       = Arthur Swerdloff
| producer       = Sid Davis
| writer         = Arthur Swerdloff
| screenplay     = 
| story          = 
| based on       =  
| starring       = Michael Keslin
| music          = Kevin Moore
| cinematography = Sid Davis
| editing        = Arthur Swerdloff
| studio         = 
| distributor    = 
| released       =  
| runtime        = 27 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Age 13 is an educational film by Sid Davis released in 1955. It is property of the public domain.
The film centers on Andrew, a thirteen-year-old boy stricken with grief over the recent death of his mother. On the day of her death her radio stops working, and Andrew believes that if he can repair it his mother will return. He is left with a cold, emotionally distant stepfather. He is also teased relentlessly in school, which leads him to bring a gun with him. During an altercation with another student in a physical education class, he fires the gun, injuring no one. Following the incident he receives counseling, is administered a Rorschach inkblot test and is encouraged to open up emotionally. However, his stepfather becomes increasingly brutal. Andrew commits a virtual murder by destroying a photograph of his stepfather, whom he blames for his mothers death; afraid his feelings will lead him to actual homicide, he runs away. By films end he has recovered, and is adopted by his aunt and her husband.

Musician Kevin Moore selected this film as inspiration for the Chroma Key album Graveyard Mountain Home. The film is included on DVD in a special edition of the album, playing at half speed and featuring the albums music soundtrack as opposed to the original.

== Availability ==
The film is in public domain and can be downloaded freely from www.archive.org.
* 
* 

==External links==
* 

 
 


 