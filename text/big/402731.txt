It's All About Love
{{Infobox film
| name     = Its All About Love
| image    = Its All About Love_poster.JPG
| director = Thomas Vinterberg
| writer   = Thomas Vinterberg Mogens Rukov
| starring = Joaquin Phoenix Sean Penn Claire Danes
| music    = Zbigniew Preisner
| editing  = Valdís Óskarsdóttir
| studio   = Nimbus Film
| released =  
| runtime  = 105 minutes
| country  = Denmark
| language = English
| budget   = $10,000,000   
}} drama film apocalyptic science fiction, but Vinterberg prefers to call it "a dream".  Unlike the directors earlier Danish-language films, Its All About Love is entirely in English and stars Joaquin Phoenix, Claire Danes and Sean Penn. The production was led by Denmarks Nimbus Film, but the film was largely an international co-production, with involvement of companies from nine different countries in total. It was very poorly received by film critics.

==Cast==
* Joaquin Phoenix as John
* Claire Danes as Elena
* Sean Penn as Marciello
* Douglas Henshall as Michael
* Alun Armstrong as David
* Margo Martindale as Betsy
* Mark Strong as Arthur
* Geoffrey Hutchings as Mr. Morrison

==Production==
 
 
The film was written, directed, and produced by Thomas Vinterberg over a period of five years. It was produced through Nimbus Film.

At a certain point during production Vinterberg called up Ingmar Bergman and asked him to come and help him finish the film, as he felt he could not complete it himself. Vinterberg recalled that:

:He roared with laughter and said I had to be out of my mind. There was nothing he was less interested in. And he also said I was an idiot that had not decided fast enough what to do after The Celebration (Festen), which he, by the way, called a masterpiece. It was a very contended conversation.

(From an interview in Berlingske Tidende.)

==Reception== Kubrick with a talent-ectomy" and Jack Matthews of the New York Daily News declared that "Surely, Vinterberg was high on some inert gas when he embarked on it".  Dennis Lim of the Village Voice, however found something to like in the film:  Sundance 03 premier, supposedly disowned by its stars (rumor has it Claire Danes burst into tears upon seeing the end result) and jettisoned by original distributor Focus Features|Focus. But this $10 million Danish-British-French-U.S.-Japanese-Swedish-Norwegian-German-Dutch co-production is a film maudit for the ages &mdash; rapturous and inexplicable in equal measure. 

== References ==
 

== External links ==
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 