The Adversary (film)
{{Infobox film
| name           = LAdversaire
| image          =The Adversary (film).jpg
| image_size     = 
| caption        = 
| director       = Nicole Garcia
| producer       = Alain Sarde
| writer         = Frederic Belier-Garcia and Jacques Fieschi, from the book by Emmanuel Carrère
| narrator       = 
| starring       = Daniel Auteuil   Géraldine Pailhas
| music          = Angelo Badalamenti
| cinematography = Jean-Marc Fabre
| editing        = Emmanuelle Castro
| distributor    = 
| released       = 28 August 2002
| runtime        = 130 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 9,062,658 €
| preceded_by    = 
| followed_by    = 
}} 2002 Cinema French drama film directed by Nicole Garcia, starring Daniel Auteuil and Géraldine Pailhas. 

Based on the 2000 book of the same name by Emmanuel Carrère, it is inspired by the real-life story of Jean-Claude Romand. LAdversaires protagonist Jean-Marc Faure (Auteuil) pursues an imaginary career as a doctor of medicine in a plot more closely based on Romands life and Carrères book than was Laurent Cantets 2001 film LEmploi du Temps.
 Best actor, Best supporting Best supporting actress.

== Cast ==
*Daniel Auteuil ...  Jean-Marc Faure 
*Géraldine Pailhas ...  Christine Faure 
*François Cluzet ...  Luc 
*Emmanuelle Devos ...  Marianne 
*Alice Fauvet ...  Alice 
*Martin Jobert ...  Vincent 
*Michel Cassagne ...  Le père de Jean-Marc 
*Joséphine Derenne ...  La mère de Jean-Marc 
*Anne Loiret ...  Cécile 
*Olivier Cruveiller ...  Jean-Jacques 
*Nadine Alari ...  La mère de Christine  Nicolas Abraham ...  Xavier 
*Bernard Fresson ...  Le père de Christine 
*François Berléand ...  Rémi
*Sibylle Blanc ... waitress

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 