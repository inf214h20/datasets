Madame Aema 11
{{Infobox film 
| name           = Madame Aema 11
| image          = Madame Aema 11.jpg
| caption        = Theatrical poster for Madame Aema 11 (1995)
| film name = {{Film name
 | hangul         =   11
 | hanja          =   11
 | rr             = Aemabuin 11
 | mr             = Aemapuin 11}}
| director       = Joe Moung-hwa 
| producer       = Choe Chun-ji
| writer         = Min U
| starring       = Lee Da-yeon
| music          = Kim Nam-yun
| cinematography = Jung Pil-si
| editing        = Hyeon Dong-chun
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 102 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Madame Aema 11 (  - Aema Buin 11) is a 1995 South Korean film directed by Joe Moung-hwa. It was the eleventh and final entry in the Madame Aema series, the longest-running film series in Korean cinema. 

==Plot==
In this episode in the Madame Aema series, Aema is married to a respected scholar who is preoccupied with his research and unable to satisfy her sex drive. Aemas husband becomes the target of a Japanese businessman with ties to the yakuza. Seeking to take his research, the Japanese businessman blackmail Aemas husband by taping him in a compromising position with a young woman he has sent to seduce him. Meanwhile Aema is indulging in an affairs of her own.   

==Cast==
* Lee Da-yeon: Madame Aema 
* Lee Joo-cheol
* Joen Hae-yoeng
* Han Eun-jeong
* Cha Ryong
* Yu Ga-hui
* Kook Jong-hwan
* Na Dong-geun
* Choe Myeong-ho
* Jo Seok-hyeon

==Bibliography==

===English===
*  
*  
*  

===Korean===
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 
 


 
 