The Chinese Parrot (film)
{{Infobox film
| name           = The Chinese Parrot
| image          =
| image_size     =
| caption        =
| director       = Paul Leni
| writer         = J. Grubb Alexander (scenario) Walter Anthony (intertitles)
| based on       =  
| starring       = Marian Nixon Florence Turner Hobart Bosworth
| music          =
| cinematography = Benjamin H. Kline
| distributor    = Universal Pictures
| released       =  
| runtime        = 7 reel#Motion picture terminology|reels; 7,304 feet
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
The Chinese Parrot (1927) is a silent film, the second in the Charlie Chan series. It was directed by Paul Leni and starred Japanese actor Kamiyama Sojin as Chan.  The film is an adaptation of the 1926 Earl Derr Biggers novel The Chinese Parrot. It is considered a lost film.  

Another version of the story was filmed in 1934, entitled The Courage of Charlie Chan.

==Cast==
*Marian Nixon - Sally Phillmore
*Florence Turner - Mrs. Phillmore
*Hobart Bosworth - P.J. Madden
*Edmund Burns - Robert Eden
*Albert Conti - Martin Thorne
*Sojin - Charlie Chan
*Fred Esmelton - Alexander Eden
*Edgar Kennedy - Maydorf
*George Kuwa - Louis Wong
*Slim Summerville - Prospector
*Dan Mason - Prospector
*Anna May Wong - Nautch Dancer
*Etta Lee - Girl in Gambling Den
*Jack Trent - Jordan
*Frank Toy - number one son

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 