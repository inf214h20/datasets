And Women Shall Weep
{{Infobox film
| name           = And Women Shall Weep
| image          = 
| caption        = 
| director       = John Lemont
| producer       = Norman Williams
| writer         = John Lemont Leigh Vance
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ruth Dunning Max Butterfield Gillian Vaughan Richard OSullivan
| music          = 
| cinematography = 
| editing        = Bernard Gribble
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

And Women Shall Weep is a 1960 British drama film directed by John Lemont and starring Ruth Dunning, Max Butterfield and Richard OSullivan.  Its plot follows a mother who tries to prevent her younger son being led astray by his delinquent elder brother.

==Cast==
* Ruth Dunning – Mrs Lumsden 
* Max Butterfield – Terry Lumsden 
* Richard OSullivan – Godfrey Lumsden 
* Gillian Vaughan – Brenda Wilkes 
* Claire Gordon – Sadie MacDougall  David Gregory – Desmond Peel  David Rose – Woody Forrest 
* León García – Ossie Green
* Prudence Bury - Jenny Owens

==References==
 

==External links==
*  

 
 
 
 
 
 
 