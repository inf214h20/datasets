Heavenly Body (film)
 
 
{{Infobox film
| name           = Heavenly Body
| image          = Corpo_celeste_poster.jpg
| caption        = Film poster
| director       = Alice Rohrwacher
| producer       = Carlo Cresto-Dina
| writer         = Alice Rohrwacher
| starring       = Salvatore Cantalupo
| music          = Piero Crucitti
| cinematography = Hélène Louvart
| editing        = Marco Spoletini
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
Heavenly Body ( ) is a 2011 Italian drama film directed by Alice Rohrwacher.  

==Plot==
The film centers on a young girls experience coming to age and receiving Confirmation. She is situated in a parish run by a corrupt priest, Don Mario, and his helpers. The film centers on her relationship not only to herself but an alienating world around her, and the Catholic Church which is a part of it.

==Cast==
* Salvatore Cantalupo as Don Mario
* Anita Caprioli as Rita
* Renato Carpentieri as Don Lorenzo
* Paola Lavini as Fortunata
* Pasqualina Scuncia as Santa
* Yle Vianello as Marta

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 