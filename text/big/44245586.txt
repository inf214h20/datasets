The Quispe Girls

{{Infobox film

| name           = The Quispe Ruth
| image          = 
| alt            =
| caption        = The Quispe Ruth
| film name      = Las niñas Quispe
| director       = Ruth quispe
| producer       = Ruth quispe
| writer         = Based on biografy of Ruth Quispe
| screenplay     = Ruth Quispe
| story          = 
| based on       =  
| starring       = Digna Quispe, Catalina Saavedra, Francisca Gavilán, Alfredo Castro, Segundo Araya
| narrator       = 
| music          = 
| cinematography = Inti Briones
| editing        =  Santiago Otheguy
| studio         =      Fábula
| distributor    =  Fábula, Swipe Films
| released       =    2014 Iquique International Festival (National release),  2013 Venice Film Festival. (Worldwide release)
| runtime        = 83
| country        = Chile, France, Argentina
| language       = Spanish
| budget         = 
| gross          =  

}}

Las niñas Quispe  is a Cinema of Chile | Chilean film written and directed by Sebastián Sepúlveda. It is based on the true story of the Quispe sisters and on Juan Radrigán’s play “Las Brutas”. 

== Plot ==
 Colla people.

In 1974, the altiplano (highlands) goat-herders were concerned their animals were losing economic value as rumours about the military government expanded and reached the more isolated corners of the country.

Affected by the recent loss of another of their sisters, and frightened by the news that the military had reached the area of Copiapo, Justa, Lucía and Luciana committed suicide by hanging themselves from a rock (along with their two dogs).

The film’s suicide scene was performed on the same rock where the real incident occurred, and the role of Justa Quispe was played by her niece, Digna Quispe.
Movie director and screenwriter Sebastián Sepúlveda says about his first encounter with Digna Quispe: "I was very afraid of Digna when I first met her. She dont shake hands, she "shake" just the fingers, and in a very cold way", later she would accept to take part of the project. Dignas harsh personality was considered a reflection of her life in the Altiplano, and  her personality leave a mark on the film. 

== Cast ==

* Alfredo Castro as Fernando

* Francisca Gavilán (Violeta Parra on Violeta Went to Heaven) as Luciana Quispe

* Digna Quispe (the Quispe sisters’ niece) as Justa Quispe

* Catalina Saavedra as Lucía Quispe

== Production ==

;Production companies:

* The movie was a Chilean, Argentinean and French production, involving Fabula, Dolce Vita Films and Cinema Uno.

; Co - producers:

* Juan de Dios Larraín 
* Fernando Sokolowicz
* Diego Urgoiti-Moinot 
* Pablo Larraín

; Executive producer
* Juan Ignacio Correa

; Co-executive producer
* Marc Irmer

; Costume designer
* Muriel Parra

; Post producer
* Cristian Echeverria

; Line producer
* Ruth Orellana

; Editor
* Santiago Otheguy

; Cinematography:
* Inti Briones

; Sound Department

* Charles Bussienne	as sound assistant: auditorium
* Cyrille Lauwerier as sound re-recording mixer
* Loïc Prian as sound editor

; Other crew

* Claire Viroulaud as French press officer

== Release Dates ==

* Italy, 30 August 2013 (Venice Film Festival)
* Greece, 5 November 2013	(Thessaloniki International Film Festival)
* France, 19 March 2014, (Villeurbanne Festival Reflets du Cinéma Ibérique et Latino-Américain)
	
* Chile, 11 September 2014	(Iquique International film Festival)
* Brazil	, 26 September 2014	(Rio de Janeiro International Film Festival)

=== Also known as (AKA) ===

* France - "Les soeurs Quispe"
* Greece - "Oi adelfes Quispe"
* World-wide (English title) - The Quispe Girls

== Awards ==

* Venice Film Festival Critics’ Week, best cinematography.

== Reception ==

* The film had a positive reception, though some criticized the awkwardness of integrating the inexperienced Digna Quispe with the rest of the crew. Digna, though brilliant, was illiterate and had no acting experience, with one reviewer saying her performance did not blend with those of Francisca Gavilán and Catalina Saavedra, the other two main characters. 

* The Hollywood Reporter also gave a positive review, saying: "Sebastian Sepulveda’s beautifully written, played and shot feature debut is as dark, pure and bleak as the lives of its subjects." 

=== See Also ===

* Cinema of Chile

== References ==

 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 