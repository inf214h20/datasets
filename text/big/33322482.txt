Freerunner (film)
{{Infobox film
| name           = Freerunner
| image          = Freerunner film poster.jpg
| caption        = Movie poster
| director       = Lawrence Silverstein
| producer       = Warren Ostergard Lawrence Silverstein
| writer         = Matthew Chadwick Raimund Huber Jeremy Sklar
| screenplay     = 
| story          = Jeremy Sklar Raimund Huber
| based on       =  
| starring       = Sean Faris Danny Dyer Tamer Hassan
| music          = 
| cinematography = Claudio Chea
| editing        = Marc Jakubowicz
| studio         = Vitamin A Films Strategic Film Partners
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Freerunner is a 2011 film by Lawrence Silverstein starring Sean Faris (Never Back Down), Danny Dyer and Tamer Hassan in lead roles.    

==Plot==
Freerunner sees eight freerunning | freerunners racing against time, to scan a collar on three check points across the city area within an hour. Each collar is fitted with an explosive which will detonate if they leave the green race zone, are the last runner to scan a checkpoint more than three seconds after the previous racer, or if the race owner, Mr. Frank (Dyer), manually detonates via remote device. The winner of the race is the first to make it to the final checkpoint within 60 minutes and will receive a prize of million dollars. The losers will all die.

The runners are chosen after performing in the local, non-lethal, races. Ryan (Faris), Kid Elvis, Mitch, Decks, Turk, West, Freebo and Finch are the eight runners chosen against their will to participate. International gangsters and businessmen place bets on the runners, before and throughout the race. Race activity is monitored by cameras throughout the race area and on each racers collar.

==Cast==
*Sean Faris as Ryan
*Danny Dyer as Mr. Frank
*Tamer Hassan as Reese
*Amanda Fuller as Dalores
*Seymour Cassel as Grampa
*Mariah Bonner as Deedee
*Casey Durkin as Stacey
*Ryan Doyle as Finch 
*Dylan W. Baker as West
*Rebecca Da Costa as Chelsea

==Reception==
Moviemavericks gave the film a 3 star rating out of 5 saying, "  hits all the expected marks for a movie like this, doesn’t disappoint and the freerunning is neat to watch. Check it out if you like low budget actioners" comparing it with French made District B13.  Haley Harris of The Film Judge  gave the movie a 2.5 star rating calling it entertaining but over at the same time. He praised some of its cool stunts but panned the script and acting calling it cheesy.   

==Release==
Frerunners was released in Germany on 15 September 2011 at Oldenburg International Film Festival. The film became available on DVD and Blu-ray in October 18, 2011. 

===DVD===
DVD extra includes behind-the-scenes footage, a making-of featurette, a look at the stunts and fights, and some on-set B-roll featuring the actors playing a game called ninja and engaging in random Parkour. 

==Soundtrack==
# Let Us In! (Come On) – The Lions Rampant
# Panther 1 – Slowride
# At The Edgewater – Johnny Douglas
# Caught Up In The Chase – Billy Livesay
# Are You Ready For This – Kritical
# Aloha vey – The Code
# Final Hour Groove – Steve Kornicki
# Oily Rags – Grant Fitch
# Fast Lane – Freak i.v.
# Back To My Old Habits – Rhiann Holly
# Last Stand of Cornholio the Wicked – Grant Fitch
# That’s The Way It’s Got To Be – Martin Guigui
#  Psycho Creep – Mark Cook
# Bein’ Blue – Cassidy Cooper
# Rocket Lab – Andrew Jed
# Get A Real Woman – Amber (Church Of Disco mix)
# Counter – Peter Groenwald
# Lover Tonight – Joe Wolfe
# End Run – Peter DiStefano
# Harp Angel – CT Sox
# Engergise – Earodynamics
# Bullets – Protillus

==References==
 

==External links==
*  

 

 
 
 
 
 