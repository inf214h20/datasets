A Long Return
 

{{Infobox film
| name           = A Long Return
| image          =
| caption        =
| director       = Pedro Lazaga Manuel Perez
| writer         = Juan Cobos Miguel Rubio Mark Burns Lynne Frederick Charo Lopez Ricardo Merino
| music          = Anton Garcia Abril
| cinematography = Francisco Sempere
| editing        = Alfonso Santacana
| distributor    =
| released       = May 26, 1975
| runtime        = 116 min
| country        = Spain
| language       = English
| budget         =
}}

A Long Return, also known by the title A Largo Retorno in Spain, is a 1975 Spanish romantic film, directed by Pedro Lazaga.

==Plot==
A young vibrant college student, Anna (Lynne Frederick), catches eye of architect, David Ortega (Mark Burns), at an orchestra show. She quickly falls in love with him and eventually wins his love. The two become inseparable and fall madly in love with each other and marry. Soon after, Anna begins acting strange and begins experiencing sudden fatigue and memory loss (even forgetting her best friend). It is then her husband learns that she has a rare and deadly disease of the nervous system that is quickly killing her. While in the hospital, with her husband by her bed side, Anna suddenly goes into a state of a coma. Doctors tell David she has less than days to live. He pleads to the Doctor for help of any kind and they inform David that Annas only chance of survival is to put her in a state of perpetual deep sedation, which would shut down her body and practically freeze her in time and prevent her from aging, until a cure can be found for her disease. With great optimism, David accepts and his wife is put in a state of sedation. For forty agonizing and heart acing years, David remains faithful to Anna and waits for a cure to be found while Anna’s friends and family move on with their lives, even as going as far as wanting to pull the plug on her and bury her, as she remains sedated. A cure is finally found in 2014, forty years later, and she is successfully taken out of sedation and fully cured. When Anna awakens she is kept at the hospital for observations. She repetitively asks to see her husband, not knowing he is now middle aged and that she has been in sedation for forty years, but the doctor keeps denying her request. The ice is broken when her best friend, Irene (now well past her 60s), visits her and informs her of what has happened in the last forty years (such events like Anna’s parents dying and the changes David has succumb to with age). When she is released from the hospital Irene takes her to David, and much to his delight, Anna greets him with overflowing love and affection as he welcomes her back with open arms. Later that night David expresses his trepidation and guilt of the age difference in their relationship. Anna reassures David that her love for him is still in there and tells him she loves him even more for him remaining faithful to her all those years. The couple has only a short time together as David dies (possibly of heart failure or old age) a few days later. After David’s death, Anna’s doctor comes by the house, asking the grief-stricken widow, where she will go from here. She replies, in a sorrowful manner, “Go on living.” Anna then has a flash back to her first date with David when he gave Anna her motto (“Today is the first day of the rest of your life”). She then tearfully glances at the doctor and utters to him “Today is the first day of the rest of my life,” knowing David would want her to go on living her life.

==Cast== Mark Burns	... 	David Ortega
*Lynne Frederick	... 	Anna Ortega
*Charo Lopez	... 	Irene
*Ricardo Merino	... 	Carlos
*George Rigaud	... 	Doctor Valls
*Andrés Mejuto	... 	Father of Anna
*Mayrata OWiesiedo	... 	Mother of Anna
* Adriano Dominguez ...     Martin
* Cocha Cuetos      ...     Adela
* Juan Diego        ...     Docteur Aguirre
* Fernando Hilbeck  ...     Doctor Armayor

==Filming==
Lynne Frederick got the part of Anna Ortega largely because had the ability to cry out of either eye on cue and she ad libbed the motto "Today is the first day of the rest of your life," which was a phrase she commonly used in real life. Mark Burns enjoyed working with Frederick recalling her being "just an overall sweet person and a very professional actress." Raul Julia was first approached for the role of David Ortega, he agreed to take the part but only on the condition that the movie be filmed in Puerto Rico. But the request was rejected by director Pedro Lazaga and Burns was cast as Ortega. Frederick herself did all her own singing for the film. The film was recorded in Madrid, Segovia, Venice and Mallorca, between 1974-1975. It was released in Spain on May 26, 1975, and then was only available in the mid 80’s on PAL Betamax. It was released in English, Spanish, Italian, and English with Greek Subtitles. It was never available in the US on DVD or VHS. Today the film is out of print and physical copies are near impossible to find anywhere. With the increase of Lynne Fredericks new found cult following in recent years, the movie has become a fan favorite and enjoyed a second life on the internet with fans phrasing Fredericks acting and beauty in the film. Many noting the film as her best work.

==External links==
* 

 
 
 