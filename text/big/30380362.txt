Fugly (film)
 
{{Infobox film
| name = Fugly
| image = Fugly_Hindi_Film_Poster.jpg 
| director =Kabir Sadanand
| producer = Ashvini Yardi Alka Bhatia Akshay Kumar
| writer = Rahul Handa
| starring =Jimmy Shergill Mohit Marwah Kiara Advani Vijender Singh Arfi Lamba
| music = Yo Yo Honey Singh Prashant Vadhyar  Raftaar & badshah
| cinematography = Milind Jog
| editing = Shounok Ghosh
| studio =  Grazing Goat Pictures Pvt. Ltd 
| distributor =
| released =  
| runtime =
| country = India
| language = Hindi
}}

Fugly is a Bollywood drama film directed by Kabir Sadanand released on 13 June 2014. The film  features Jimmy Shergill as one of many lead characters, including debut appearances from Mohit Marwah, Vijender Singh, Arfi Lamba and Kiara Advani.   

==Cast==
 
* Jimmy Shergill as Inspector R.S. Chautala
* Mohit Marwah as Dev
* Kiara Advani as Devi
* Vijender Singh as Gaurav
* Arfi Lamba as Aditya
* Anshuman Jha as Cheeni (special appearance)
* Sana Saeed as dancer (special appearance in song Lovely) 
 

== Plot ==
Dev, Devi, Gaurav and Aditya are close friends. Gaurav is the son of CM of Haryana, while the rest of his friends are commoners. Gaurav is isolated from the politics his family is into and is the only educated person in his family. Gaurav is nice hearted, but always high on adrenaline because of his influential father. He has a knack of getting into trouble with police because of this attitude, but the police always ignore his pranks. 
A local grocer or Nunu has an evil eye on Devi and one fine day gropes her in his shop. The four of them decide to teach him a lesson. At night they break into his shop and trash him warning him not to see her again. Although outnumbered and in bad state Nunu challenges them to leave Devi behind for him. Dev is very angry and decides to teach him a lesson. They put him in their cars trunk and drive him to an isolated location. They are intercepted by Inspector R.S. Chautala who is patrolling the highway. Gaurav as usual tries to act smart and gets involved in a scuffle with Chautala. This enrages him and he suspects foul play. When he checks the car he discovers Nunu inside the trunk. Chautala kills Nunu in order to frame them in his murder case. In the morning after severe threat he puts up a demand of Rs. 61 lakhs of ransom money, which they have to arrange in 24 hours. Gaurav is afraid to tell his father about the incident because he was actually involved in kidnapping of Nunu, since this would cost his father his CM chair. 
Somehow everyone is able to arrange 24 lakhs.

Chautala further blackmails them by recording them handing over the money to him, as an evidence of attempting to bribe  a policeman. He gives them 3 more days to arrange for the money with additional Rs. 10 lakhs as penalty. The four of them are further forced to arrange a rave party with a drug dealer, which is again raided by Chautala. He forces them into drug dealing as well to extract more juice out of them. 
The four of them decide that they had enough and want to expose him in a sting operation. They almost succeed in extracting confession out of him on camera, but are busted and Chautala also kidnaps Devi. Chautala outsmarts them on every step and four of slip deep into trouble with every step they try. Finally the are arrested on false charges and meanwhile out on bail.

Dev in the end decides that he has to sacrifice his life to expose truth. The next scene we see Dev in hospital and his narration is interrupted by Chautala once again. He clears up the ICU room of everyone to have a private moment with Dev. Dev tricks him by cutting off his own life support and getting into a scuffle with Chautala. As the medics and reporters rush into the room, they find Dev dead and it appears as if Chautala has killed him. This is telecast live on the news. Next we see Chautala meets Gauravs father in an isolated region. Chautala true to his colour again attempting to blackmail the CM and he shot dead by his colleague.

==Production==
Plans for Fugly were announced in 2013,   and actors such as Salman Khan and Akshay Kumar were confirmed to be performing in the film.  Filming for Fugly was initially intended to begin in September 2013, but was delayed until October of the same year.    Filming took place in Delhi and Mumbai,   and the key song was shot in March 2014. 

==Critical reception==
Critic Subhash K. Jha gave it 4 Stars and said that this film is thoughtful and at times brilliant. Moreover, it possesses a very rare virtue, a conscience. 

==Music==
{{Infobox album 
| Name = Fugly
| Longtype = to Fugly (2014)
| Type = Soundtrack
| Artist = Yo Yo Honey Singh
| Caption = Soundtrack album cover 
| Released =  
| Genre = Bollywood Music
| Length = 22:37 Hindi
| Label = T-Series
}}

===Soundtrack===
The soundtrack is composed by Prashant Vadhyar, Yo Yo Honey Singh & Raftaar. All lyrics written by Yo Yo Honey Singh, Raftaar, Arshia Nahid, Sumit Aroraa, Niren Bhatt & Rajveer Ahuja

== References ==
 

==External links==
*  

 
 
 
 
 