Pearl of the Army
{{Infobox film
| name = Pearl of the Army
| image = Pearl of the Army ad.jpg
| caption = Theatrical poster 
| director = Edward José
| producer = 
| writer = Guy McConnell
| starring = Pearl White Ralph Kellard
| cinematography = 
| editing =  Astra Films
| released =  
| runtime = 15 episodes 
| country = United States 
| language = Silent with English intertitles
| budget = 
}}
 silent film Hudson River Fort Lee.      

==Cast==
* Pearl White as Pearl Date
* Ralph Kellard as Captain Ralph Payne
* Marie Wayne as Bertha Bonn
* Theodore Friebus as Major Brent
* William T. Carleton as Colonel Dare (as W.T. Carleton)
* Floyd Buckley
* Joe Cuny

==Chapter titles==
# The Traitor
# Found Guilty
# The Silent Menace
# War Clouds
# Somewhere In Grenada
# Major Brents Perfidy
# For The Stars and Stripes
# International Diplomacy
# The Monroe Doctrine
# The Silent Army
# A Million Volunteers
# The Foreign Alliance
# Modern Buccaneers
# The Flag Despoiler
# The Colonels Orderly

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 


 