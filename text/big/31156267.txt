Preethi
{{Infobox film 
| name           = Preethi
| image          =
| caption        =
| director       = William Thomas
| producer       =
| writer         = Hamrabi N. Govindankutty (dialogues)
| screenplay     = N. Govindankutty Madhu Sheela Prem Navas Baby Shabnam
| music          = A. T. Ummer
| cinematography = U Rajagopal
| editing        = VP Krishnan
| studio         = KK Films Combines
| distributor    = KK Films Combines
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by William Thomas . The film stars Madhu (actor)|Madhu, Sheela, Prem Navas and Baby Shabnam in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Madhu
*Sheela
*Prem Navas
*Baby Shabnam
*Bahadoor
*Kaduvakulam Antony
*N. Govindankutty
*Philomina
*S. P. Pillai

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Dr Pavithran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Adharam Madhuchashakam || K. J. Yesudas || Dr Pavithran || 
|-
| 2 || Kanna Kaarvarnna   || S Janaki || Dr Pavithran || 
|-
| 3 || Kannuneeril || K. J. Yesudas || Dr Pavithran || 
|-
| 4 || Kizhakku Ponmalayil || P Jayachandran || Dr Pavithran || 
|-
| 5 || Naadha Varoo Prananaadha || LR Eeswari || Dr Pavithran || 
|-
| 6 || Umma Tharumo || S Janaki, KC Varghese Kunnamkulam, Latha Raju || Dr Pavithran || 
|}

==References==
 

==External links==
*  

 
 
 

 