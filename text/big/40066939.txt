Persecuted (film)
 
 
{{Infobox film
| name           = Persecuted
| image          = Persecuted-2014-poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Daniel Lusko
| producer       = Daniel Lusko James R. Higgins Gray Frederickson Jerry D. Simmons
| writer         = Daniel Lusko
| starring       = James Remar Bruce Davison
| music          = Chris Ridenhour
| cinematography = Richard J. Vialet
| editing        = Brian Brinkman
| studio         = One Media
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.6 million   
}}
Persecuted is a 2014 American drama film written and directed by Daniel Lusko and stars James Remar, Bruce Davison, Dean Stockwell, Raoul Trujillo, Fred Thompson, Brad Stine, David House, and Tabatha Shaun.

==Plot==
Reformed drug addict and Americas leading evangelist John Luther opposes Senator Donald Harrisons Faith and Fairness Act which would not allow Christians to state that they have the whole/only truth. To destroy Luthers credibility and ensure passage of the bill Luther is framed for the rape and murder of a teenage girl.

==Cast==
* James Remar as John Luther
* Bruce Davison as Senator Donald Harrison
* Dean Stockwell as Dave Wilson
* Raoul Trujillo as Mr. Gray
* Fred Thompson as Fr. Charles Luther
* Natalie Grant as Monica Luther
* Gretchen Carlson as Diana Lucas
* Brad Stine as Pastor Ryan Morris

==Production==
Principal photography took place in Albuquerque, New Mexico.   The movies production is finished and it was screened at the February 2014 National Religious Broadcasters convention in Nashville, Tennessee and March 2014 at the Conservative Political Action Conference in Washington, D.C. 

It has been turned into a book by Robin Parrish.

==Release==
Persecuted was released theatrically on July 18, 2014.

===Critical reception===
The film was widely panned by critics and is one of the worst-reviewed films of 2014. The review aggregator website Rotten Tomatoes reported a 0% approval rating with an average rating of 2.5/10 based on 13 reviews.  Metacritic, which uses a weighted average, assigned a score of 11 out of 100 based on 9 reviews, indicating "overwhelming dislike". 

In a review for the New York Post, Kyle Smith wrote, "The Lord works in mysterious ways but Persecuted works in blundering, obvious ways, straining a Christianity-under-attack theme through a dopey thriller."   
 New York Times film critic Neil Genzlinger stated, "This terrible attempt at a political thriller for the religious right is aimed not at Christians in general but at a certain breed of them, the kind who feel as if the rest of the world were engaged in a giant conspiracy against their interpretation of good and truth."   

Justin Chang from Variety (magazine)|Variety concluded his scathing review with, "At a time when the world offers us no shortage of examples of what actual religious persecution looks like, for a film to indulge in this particular brand of self-righteous fearmongering isn’t just clueless or reckless; it’s an act of contemptible irresponsibility."   

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 