Berlin Calling (2014 film)
 
{{Infobox film
| name = Berlin Calling
| image = 
| writer = Nigel Dick Kastle Waserman
| starring = Kastle Waserman Benjamin Waserman
| director = Nigel Dick
| producer = Nigel Dick
| studio = Dickfilms
| distributor = 
| released =  
| runtime = 81 minutes
| language = English
| gross = 
| music = Nigel Dick
}}
Berlin Calling is a 2014 documentary film starring Kastle Waserman and Benjamin Waserman. Written by Nigel Dick and Kastle Waserman, and directed by Nigel Dick.

==Plot==

The film follows American punk fan Kastle Waserman as she journeys to Berlin, Prague, Paris, Houston and Theresienstadt to unearth the details of her fathers time in Hitlers Germany and his time in Theresienstadt concentration camp. The film concludes with a return visit to Berlin during which Benjamin Waserman meets Berlin mayor Klaus Wowereit. 

==Cast==

*Kastle Waserman as herself
*Benjamin Waserman as himself
*John Waserman as himself
*Klaus Wowereit as himself

==Production==

The film was made over a period of seven years and was shot in California, Texas, France, Germany and the Czech Republic. The film was officially released on DVD on April 7th 2015 and its digital release was on April 17th 2015.

==Awards==

The film was awarded a Gold REMI award at the WorldFest Houston International Films Awards in April 2014 and was an Official Selection at the Hartford Jewish Film Festival in March 2015 and the Pioneer Valley Jewish Film Festival in April 2015.

== External links ==
*  
*  

 