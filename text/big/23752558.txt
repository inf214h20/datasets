Family Album (miniseries)
{{Infobox television film
| name         = Family Album
| image        = Family Album DVD cover.jpg
| caption      = DVD cover
| genre        = Drama
| runtime      = 240 minutes
| director     = Jack Bender
| producer     = Douglas S. Cramer
| screenplay   = Karol Ann Hoeffner
| story        = Danielle Steel Family Album (1985) by Danielle Steel
| starring     = {{Plainlist|
*Jaclyn Smith
*Michael Ontkean
*Joe Flanigan
*Joel Gretsch
*Brian Krause
*Kristin Minter
}}
| editing        = Mark Melnick
| music          = Lee Holdridge Michael Watkins
| budget         = 
| country        = United States
| language       = English
| released       = 1994
| first_aired    = October 23, 1994
| last_aired     = October 24, 1994
| network        = NBC
| num_episodes   = 2
}}
 1985 novel of the same name by Danielle Steel. Directed by Jack Bender, it was broadcast in two parts on October 23 and 24, 1994. The drama centers on the life chronology of a Hollywood actress who becomes a successful film director in an era when directing was dominated by men.

==Plot== Zane Grey Theater. Meanwhile, her home life is less fortunate. Ward has been depressed since going bankrupt and he has started an affair. When Faye finds out, she immediately kicks him out of the house. However, she soon agrees to give him another chance and they decide to work together on films as director and producer. Their film debut becomes a blockbuster success with positive reviews and she is contracted by Universal Pictures|Universal.
 football player, run away. Faye is devastated, but cant afford to quit her job and look for her. Soon, Greg announces he will serve for the army in Vietnam War|Vietnam. The entire family comes together to say goodbye, but Ward refuses to speak to Lionel. Later, Faye receives a phone call from the police, informing her that Anne has been arrested for drug possession. Faye picks her up and is shocked to find out she is pregnant. Ward convinces her to try to make her give up her child. Anne is in tears after giving birth, but reluctantly agrees to give up the baby for adoption.
 UCLA for the lead role in a cheap horror film, for which she is required to go nude. The Oscar nomination takes all of Fayes time. This makes Anne feel very neglected and she starts to hang out at her friends place a lot. During this time, she bonds with the father of this family, Bill OHara. Faye finally decides to forgive Ward and they reunite. Soon, the Thayer family deal with a second tragedy, when Lionel and his boyfriend John get into a car accident. Lionel survives, but John dies. He has trouble dealing with his loss and spends all his time working as a photographer.

Later, Anne upsets her parents by admitting she is dating the much older Bill. Ward is furious and confronts Bill with the fact he is seeing a 17-year-old girl. However, Anne and Bill are still determined to marry and it doesnt take long before she gets pregnant again. Meanwhile, Valerie finally gets her big break, when she is given the second lead role in her mothers newest film project. The lead player, George Waterson, at first treats her badly, because he thinks she is a horrible actress. However, she eventually wins his heart and they start a secret relationship. Lionel finds love as well, with Paul Steel, a drug addicted actor who was recently fired. In the end, the movie directed by Faye and starring Valerie becomes a great success and Val enjoys her overnight stardom. Later, Anne gives birth to a son. The labor and baby reminds her of her first pregnancy and she blames her mother for having given up her first baby. Ten years later, Val makes Anne realize that Faye was actually a great mother, but didnt have a lot of time. Anne tries to apologize, but she is too afraid. Suddenly, Faye dies. Anne feels guilty for not having apologized, but Ward assures her that Faye knew how much she loved her.

==Differences between the film and the novel.==
* In the novel, Valerie is one of a set of twins. In the film, she is a singleton.
* In the novel, Lionels sweetheart John dies in a Christmas tree fire. In the film, he dies in a car crash.
* In the novel, Paul is Lionels much-older first beau, with whom he ends up parting. Then, he falls in love with John. Then, sometime after Johns death, he starts a relationship with a nice gentleman whose name is not given. In the film, John is Lionels first sweetheart, then he meets and falls for Paul.
* In the novel, Bills last name is Stein. In the film, it is OHara.

==Cast==
* Jaclyn Smith as Faye Price Thayer
* Michael Ontkean as Ward Thayer
* Joe Flanigan as Lionel Thayer
* Kristin Minter as Valerie Val Thayer
* Leslie Horan as Anne Thayer (later OHara)
* Brian Krause as Greg Thayer Tom Mason as Bill OHara
* Joel Gretsch as John
* Paul Satterfield as Paul Steel John Waters as Vincent

==Production and broadcast== 1985 novel of the same name by Danielle Steel. Produced by Douglas S. Cramer and directed by Jack Bender, the four-hour miniseries was broadcast on NBC in two parts on October 23 and 24, 1994.         

==Critical reception==
David Hiltbrand of People (magazine)|People wrote, "This hackneyed attempt to blend Hollywood glamor with family values is cosmetically appealing, but underneath that veneer crawl the worms of artifice, predictability and bad acting." 
 Michael Watkins was nominated for a Primetime Emmy Award for Outstanding Cinematography for a Miniseries or Movie in 1995 for Family Album.   

==References==
 

==External links==
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 