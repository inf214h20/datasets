Pumpkinhead: Ashes to Ashes
{{Infobox television film
| name           = Pumpkinhead 3: Ashes To Ashes
| caption        = 
| image          =  
| director       = Jake West
| producer       = Brad Krevoy   Pierre Spengler Donald Kushner
| writer         = Jake West Barbara Werner Sci Fi Channel
| starring       = Doug Bradley Lance Henriksen Rob Lord
| cinematography = Erik Alexander Wilson
| editing        = Justin Rogers MPCA
| distributor    = Sony Pictures Home Entertainment
| released       =    }}
| runtime        = 91 minutes
| country        = United Kingdom United States Romania
| language       = English
| budget         = 
| preceded_by    =  
| followed_by    =  
}}

Pumpkinhead 3: Ashes to Ashes is a 2006  .

== Plot == cremating them. Pumpkinhead through the mummified body of Ed Harley (played by Lance Henriksen, who reprises his role from the first film). Pumpkinhead then proceeds to go on his killing rampage murdering all those responsible for the desecration, while Doc Frasier (played by Hellraisers Doug Bradley) hurries to murder those who summoned Pumpkinhead, which will effectively kill the demon in the process.

== Cast ==
* Doug Bradley as Doc Fraser
* Douglas Roberts as Bunt Wallace
* Lisa McAllister as Dahlia Wallace
* Tess Panzer as Molly Sue Allen
* Emanuel Parvu as Oliver Allen
* Ioana Ginghina as Ellie Johnson
* Lance Henriksen as Ed Harley
* Radu Iacobian as Richie
* Catalin Paraschiv as Ronnie Johnson
* Dan Astileanu as Sheriff Bullock
* Aurel Dicu as Tiny Wallace
* Iulian Glita as Junior Wallace
* Lynne Verrall as Haggis
* Emil Hostina as Lenny
* Philip Bowen as Reverend McGee
* Valentin Vasilescu as Deputy Ben
* Bart Sidles as Fred
* Mircea Stoian as Agent Bensen
* Radu Banzaru as Agent Black
* Liviu Gherghe as Stan

== Production ==
Initially announced as Pumpkinhead 3, it was filmed back-to-back with another sequel titled Pumpkinhead 4 in Bucharest, Romania. The films were renamed Pumpkinhead: Ashes To Ashes and  , respectively, before their release. 

== Release == SCI FI on October 28, 2006. 

== Reception ==
Ian Jane of DVD Talk rated it 2/5 stars and criticized its "cheap, shoddy filmmaking".   Dread Central rated it 2/5 stars and wrote, "Despite the cast and crew’s obvious devotion to the original, Ashes to Ashes hits every pitfall in the low-budget realm."   Virginia Heffernan of The New York Times called it unmemorable and "rote slasher stuff".  In a recent interview, Lance Henriksen described his experiences on the film as "a nightmare". 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 