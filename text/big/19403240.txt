La Possibilité d'une île (film)
{{Infobox Film
| image          = La Possibilité dune île.jpg
| name           = La Possibilité dune île
| director       = Michel Houellebecq
| producer       = Eric Altmeyer Nicolas Altmeyer	
| writer         = Michel Houellebecq
| starring       = Benoît Magimel Ramata Koite Patrick Bauchau Andrzej Seweryn Jean-Pierre Malo Serge Larivière	
| cinematography = Eric Guichard Jeanne Lapoirie
| editing        = Camille Cotte
| music          = Mathis Nitschke
| distributor    = 
| released       = 10 September 2008
| runtime        =  
| country        = France French
| budget         = 
| gross          = 	
}}

La Possibilité dune île is a 2008 film directed by Michel Houellebecq, loosely based on his 2005 novel The Possibility of an Island.

== Plot ==
Without much response and only accompanied by his not very supportive son Daniel and a technical assistant, a man calling himself The Prophet travels around Belgium and lectures on eternal life and how it is possible to surpass the limitations of death. Only three years later he has achieved a remarkable status with numerous followers. Believing in the so-called Elohim, extraterrestrials who bring eternal life, the sect he founded is doing research on an optimised technique of cloning that allows one to reproduce an adult person within a few minutes including a copy of his complete memory storage. Daniel leaves his fathers cult but returns years later in order to become The Prophets successor. The story continues thousands of years from this moment. Daniel reappears as a clone of himself, heir to a long line of clones called neo-humans. Living in a cave, he reads about the past of humankind and his own ancestors. When an undefined longing urges him to leave his cave, he wanders a lonely world. After a dog has become his companion, Daniel feels like he approaches the concept of love which neo-humans have lost over the centuries. At the same time another loner is getting lost in the wastelands, a woman whose original human version once had an encounter, maybe a love affair with Daniel.

== Reception ==
The film received negative reviews.

== External links ==
*  
*  
*  

 

 
 
 
 
 

 