Climbing to Spring
{{Infobox film
| name           = Climbing to Spring
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Daisaku Kimura
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Kaoru Kobayashi
| music          = Shinichiro Ikebe
| cinematography = Daisaku Kimura
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 116 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = ¥95.2 million (Japan)
}}

  is a 2014 Japanese drama film directed by Daisaku Kimura. It was released on 14 June 2014. 

==Plot==
Tōru, a struggling securities trader in Tokyo, learns that his father Isao has died in a mountain rescue operation. He returns to his home town in the mountains of central Japan for the wake. He spontaneously decides to quit his prestigious job and take over the remote mountain hut that Isao operated during the summer season. Helping him are Ai, a young woman whos an excellent cook, and Goro, a somewhat mysterious friend of Isaos.

Tōru struggles at first, but soon comes to appreciate the magnificent environment and the camaraderie among the mountain enthusiasts who come to visit. His newfound solace is put to a test when Goro suffers a stroke and needs to be carried down the mountain quickly or risk lasting damage.

==Cast==
*Kenichi Matsuyama as Tōru Nagamine
*Yu Aoi as Ai Takazawa
*Etsushi Toyokawa as Goro Tada Kaoru Kobayashi as Isao Nagamine

==Reception==
The film has grossed ¥95.2 million in Japan. 

==References==
 

==External links==
*   
* 

 
 

 
 
 
 