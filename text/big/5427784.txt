How to Commit Marriage
{{Infobox film
| name = How to Commit Marriage
| image = 
| image_size = 
| border = 
| alt = 
| caption = 
| director = Norman Panama
| producer = Bill Lawrence Ben Starr
| screenplay = 
| story = 
| based on = 
| narrator = 
| starring = (See article)
| music = Joseph J. Lilley
| cinematography = Charles Lang
| editing = Ronald Sinclair
| studio = Naho Productions
| distributor = Cinerama Releasing Corporation
| released =  
| runtime = 95 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
 comedy film directed by Norman Panama, featuring Bob Hope and Jackie Gleason in their only movie together.  The film also stars Jane Wyman as Hopes wife, Tina Louise as record-producer Gleasons love interest, Leslie Nielsen as the straight man, and Irwin Corey as a middle eastern guru named "The Baba Zeba." Also in the cast is young Tim Matheson.

This was Jane Wymans final film appearance.
 
The rock band The Comfortable Chair appears as a hippie rock group, performing their song "A Childs Garden."

==Plot==
Nancy, the 19-year-old daughter of Frank and Elaine Benson, wants to marry David, the 20-year-old son of Oliver Poe. What the bride doesnt know is that her parents are about to get a divorce.

Poe, a music promoter, has a hunch something is amiss. He doesnt want the kids to get married, so before the wedding he exposes the Bensons secret. Nancy and David decide marriage isnt necessary. They will live together instead, travel around the country with a rock band and heed the advice and wisdom of a Hindu mystic called The Baba Zeba.

Frank and Elaine are seeing other people. He is involved with a divorcee, Lois Grey, while she is developing an interest in Phil Fletcher, who also is recently divorced. Poe, meanwhile, continues to see his mistress, LaVerne Baker.

Then one day, Nancy finds out she is pregnant. The Baba Zeba persuades her to put up the baby for adoption. Frank and Elaine conspire behind their daughters back to adopt their own grandchild.

Complications arise, resulting in Frank trying to bribe the guru and even disguising himself as one of the Baba Zebas robed followers. By the end, all ends well for everybody, the Bensons getting back together and Poe proposing to LaVerne.

==Cast==
* Bob Hope as Frank Benson
* Jackie Gleason as Oliver Poe
* Jane Wyman as Elaine Benson
* Leslie Nielsen as Phil Fletcher
* Maureen Arthur as Lois Grey
* Tina Louise as LaVerne Baker
* Tim Matheson as David Paul Stewart as Willoughby
* Irwin Corey as the Baba Zeba
* JoAnna Cameron as Nancy

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 