Reptilicus
{{Infobox film
| name           = Reptilicus
| image          = reptilicus-danish.jpg
| image_size     =
| caption        = Danish theatrical poster
| director       = Danish version:  
| producer       =
| writer         = Ib Melchior Sidney W. Pink
| narrator       =
| starring       = Carl Ottosen Ann Smyrner Mimi Heinrich
| music          = Sven Gyldmark
| cinematography = Aage Wiltrup
| editing        = Sven Methling Edith Nisted Nielsen
| studio         = Saga Studios
| distributor    = American International Pictures (USA) Saga Studios (Denmark)
| released       =  
| runtime        = 81 min.
| country        = Denmark United States Danish English English
| budget         = $100,000 Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p168-169 
}}

Reptilicus, a giant monster film about a fictional prehistoric reptile, is a Danish-American co-production, produced by American International Pictures and Saga Studio, and is—upon close examination—two distinctly different films helmed by two different directors.

The original version, which was shot in Danish, was directed by Danish director Poul Bang and released in Denmark on February 25, 1961.

The American version, which was in English with a nearly identical cast, was directed by the films American producer-director Sidney W. Pink; this version was initially deemed virtually unreleasable by American International Pictures and had to be extensively reworked by the films Danish-American screenwriter, Ib Melchior, before being finally released in America in 1962. Pink was angry at the changes and wound up in a legal dispute with AIP.  After Pink and others viewed the English-language version, the lawsuit was dropped.   

==Plot==
Danish miners dig up a section of a giant reptiles tail from the frozen grounds in Sápmi (area)|Lapland, where they are drilling.  The section is flown to the Danish Aquarium in Copenhagen, where it is preserved in a cold room for scientific study. But due to careless mishandling, the room is left open and the section begins to thaw, only for scientists to find that it is starting to regenerate.
 regeneration abilities to that of other animals like earthworms and starfish.

Once fully regenerated from the tail section, Reptilicus goes on an unstoppable rampage from the Danish countryside to the panic-stricken streets of Copenhagen (including one of its famous landmarks, Langebro Bridge), before finally being killed with poison by ingenious scientists and military officers.

However, the monsters foot is not destroyed and sinks to the bottom of the sea. The movie is left open-ended, with the possibility that the foot could regenerate.

==Cast==
* Carl Ottosen as General Mark Grayson (Military)
* Ann Smyrner as Lise Martens (Martens Daughters)
* Mimi Heinrich as Karen Martens (Martens Daughters)
* Asbjorn Andersen as Professor Otto Martens (Scientist)
* Marla Behrens as Connie Miller (Scientist)
* Bent Mejding as Svend Viltorft (Drilling Crew Chief)
* Povl Woldike as Dr. Peter Dalby (Scientist)
* Dirch Passer as Peterson (Night Watchman) Overalls
* Ole Wisborg as Captain Brandt (Royal Danish Guard)
* Birthe Wilke as Herself (Nightclub Singer)
* Borge Moller Grimstrup as Danish Farmer
* Claus Toksvig as Himself / Narrator
* Mogens Brandt as Police Chief Hassing
* Kjeld Petersen as Police Officer Olsen
* Dirk Melchior as Farmer Eaten By Reptilicus
* Alex Suhr as Alex (Drilling Crew)
* Jens Due as Journalist
* Mai Reimers as Badegæst
  theatrical poster for the U.S release of Reptilicus. Artwork by Reynold Brown]]


==Production==
 	
Filming took place in several locations in Denmark, including Copenhagen, Sjællandl, and Jylland.  Several versions were filmed, the original film was filmed using the native Danish language and the second was filmed using the English language. Each version of the film featured the same actors speaking in English with the exception of Bodil Miller who was replace by actress Marlies Behrens since the Danish actress could not speak English. However the English version of the film was heavily edited and the actors voices dubbed over by American International Pictures for its release in the United States.   

==Reception==
  Godzilla in 1998, before his death in 2002.

The film received mostly negative reviews from American critics, it currently has a 29% "Rotten" rating on the film review website Rotten Tomatoes based on 7 critics and a 37% from audience members. 

==Novel and Comic==
 

A novelization of the film was released in paperback at the time of its original release (Reptilicus by Dean Owen (Monarch, 1961)). 

In 1961, Charlton Comics produced a comic book based on the film. Reptilicus lasted two issues.  After the copyright had lapsed, Charlton modified the creatures look and renamed it Reptisaurus. The series was now renamed Reptisaurus the Terrible and would continue from issue #3 before being cancelled with issue #8 in 1962.  This was followed by a one-shot called Reptisaurus Special Edition in 1963. 

In 2012, Scary Monsters magazine reprinted the Reptisaurus the Terrible series as a black and white collection called Scarysaurus the Scary. 

==DVD==
The American version of Reptilicus was released on DVD April 1, 2003 by MGM Home Entertainment under the Midnight Movies banner.  The Danish version was released on DVD from Sandrew Metronome (Denmark) in 2002.

==References==
 

==Further reading==
* Dean Owen: Reptilicus (Monarch Books, 1961)
* Sidney W. Pink: So You Want to Make Movies (Pineapple Press, 1989)
* Robert Skotak: Ib Melchior - Man of Imagination (Midnight Marquee Press, 2000)
*   #96 (2003)

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 