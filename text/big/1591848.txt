Pete Kelly's Blues (film)
{{Infobox film
| name = Pete Kellys Blues
| image = Pkblues.jpg
| caption = Videocassette poster for Pete Kellys Blues
| writer = Richard L. Breen
| starring = Jack Webb Janet Leigh Edmond OBrien
| director = Jack Webb
| producer = Jack Webb
| music = Arthur Hamilton Ray Heindorf David Buttolph Matty Matlock
| cinematography = Harold Rosson
| editing = Robert M. Leeds
| studio = Mark VII Limited
| distributor = Warner Bros.
| released =  
| runtime = 95 min.
| country = United States
| language = English
| budget = $2,000,000
| gross = $5 million (US)  Defiant Ones Is Festival Favorite: Kramer and Stars in Berlin; Webb Readies Newspaper Tale
Scheuer, Philip K. Los Angeles Times (1923-Current File)   02 July 1958: B7. 
}}
 original radio series. It was directed by and starred Jack Webb in the title role of a bandleader and musician. Janet Leigh is featured as party girl Ivy Conrad, and Edmond OBrien as a gangster who applies pressure to Kelly. Best Actress in a Supporting Role).  Ella Fitzgerald makes a cameo as singer Maggie Jackson (a character played by a white actress in the radio series). Lee Marvin, Martin Milner and a very young Jayne Mansfield also make early career appearances.

Much of the dialogue was written by writers who wrote the radio series Pat Novak for Hire,  (1946-1949) and the radio version of "Pete Kellys Blues"  (1951),  both in which Webb starred for a time before creating Dragnet (series)|Dragnet.

==Plot== Kansas City in 1927 during Prohibition. New local crime boss Fran McCarg (Edmond OBrien) wants a percentage of the bands meager earnings. Kelly wisely gives in, despite the rest of the bands opposition.

Before the night ends, Rudy, the manager of the club, orders Kelly and the band to go to the house of wealthy Ivy Conrad (Janet Leigh), a woman with a reputation for hosting rowdy parties and who has designs on Kelly. Reluctantly, Kelly arrives at the party and leaves a message for McCarg to call him there. When the call comes through, it is intercepted by Kelly’s drunk, hot-tempered drummer, Joey Firestone (Martin Milner), who turns McCarg down. Kelly and his band are run off the road as they drive back to Kansas City.
  
The following night, Firestone roughs up Guy Bettenhauser, McCarg’s right-hand man. Kelly desperately tries to patch things up, but to no avail. As the band finishes its last number, two gunmen burst through the front door of the club. Kelly tries to save Firestone by sending him out the back, but Firestone is shot to death in the alleyway. 

Later, all the local band leaders meet secretly to decide how to respond to McCargs pressure. When Kelly tells them he will put up no resistance, the rest go along as well.

Tired and frustrated by his drummer’s murder and the subsequent departure of Al (Lee Marvin), his clarinetist and longtime friend, Kelly returns to his apartment to find Ivy waiting for him. Although he initially resists her advances, the two soon strike up a relationship, which turns into an engagement.

McCarg tries to befriend Kelly, telling him that Bettenhauser acted alone in Firestone’s murder. He also presents Kelly with a new band member: his moll Rose Hopkins (Peggy Lee). Rose, celebrating Pete and Ivy’s engagement, has a little too much to drink, and due to an inattentive crowd, cannot bring herself to sing. An enraged McCarg chases her to her dressing room and beats her senseless.

Ivy, feeling left out by Kelly’s secrecy and devotion to his work, decides to go her own way. Al drops in to see Kelly. The two come to blows over Kellys handling of the McCarg situation, but quickly patch things up, and Al rejoins the band.

Realizing that Al is right, Kelly tries to fix things by going to McCarg to pay him off, but McCarg will not release the band. Kelly then turns to Detective George Tennel (Andy Devine), who is trying to take McCarg down. Tennel has news for Kelly: Bettenhauser has skipped town.

Kelly gets a message to meet someone. That person turns out to be Bettenhauser. He first tells Kelly that it was McCarg who had Firestone killed. Second, if Kelly can come up with $1,200 by daybreak, Bettenhauser will help him get McCarg. Kelly agrees. Bettenhauser tells him he can find cancelled checks and papers in McCarg’s office at the Everglade Ballroom.

Back at the club, Kelly arms himself, but is stopped by Ivy, who wants a last dance with him. He insists he does not have the time. Kelly finds the papers he needs, but before he can get out, a loud orchestrion begins playing. Ivy had followed Kelly to the ballroom, started the music and turned all the lights on. Kelly fearfully agrees to a last dance, but soon finds himself surrounded by McCarg and two of his men, one of them being Bettenhauser. Kelly has been set up.

A shootout ensues. Bettenhauser climbs up into the ceiling to get a better shot, but Kelly shoots him first. McCarg’s other man tries to shoot Kelly, but Kelly throws a chair at him, causing him to hit and mortally wound McCarg instead. Seeing this, the gunman gives up.

Back at the 17 Club, it is business as usual — the band playing, Ivy and Pete back together again, and Rudy still cutting corners wherever he can.

==See also==
*Pete Kellys Blues (radio series)|Pete Kellys Blues (radio series)
*Pete Kellys Blues (song)
*Pete Kellys Blues (TV series)|Pete Kellys Blues (TV series)
*Songs from Pete Kellys Blues

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 