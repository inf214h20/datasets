Oru Thira Pinneyum Thira
{{Infobox film 
| name           = Oru Thira Pinneyum Thira
| image          = Otpthira.png
| image_size     =
| caption        = LP Records Cover
| director       = PG Vishwambharan
| producer       = M Mani
| writer         = Sunitha Dr Pavithran (dialogues)
| screenplay     = Dr Pavithran
| starring       = Prem Nazir Mammootty Ratheesh Premji Shyam MG Radhakrishnan
| cinematography = DD Prasad
| editing        = VP Krishnan
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Shyam and MG Radhakrishnan.   

==Cast==
*Prem Nazir
*Mammootty
*Ratheesh
*Premji
*Sathyakala
*Swapna

==Soundtrack== Shyam and MG Radhakrishnan and lyrics was written by Chunakkara Ramankutty and Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Devi nin roopam shishiramaasa || K. J. Yesudas || Chunakkara Ramankutty || 
|-
| 2 || Devi nin roopam shishiramaasa (sad) || K. J. Yesudas || Chunakkara Ramankutty || 
|-
| 3 || Muthiyamman Kovilile || Vani Jairam, Chorus || Bichu Thirumala || 
|-
| 4 || Oru Thira   || K. J. Yesudas, Chorus || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 