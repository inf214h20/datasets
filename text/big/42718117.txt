Hara (2014 film)
{{Infobox film
| name           = Hara
| image          = Hara audio.jpg
| alt            = 
| caption        = Ready to Strike
| director       = Devaraj Palan
| producer       = Janaki Tulasiram
| writer         = 
| starring       = Vasanth Pragna
| music          = Jassie Gift
| cinematography = K.M. Vishnuvardhana
| editing        = Suresh Urs
| studio         = Dhanalakshmiinarayanaa Combiinnes
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kannada drama Telugu film Chinnodu, where actors Sumanth and Charmy Kaur played the lead roles.       The trailer of the film was unveiled on December 13, 2013.

== Cast ==
* Vasanth
* Pragna
* Dharma Sharan
* Sadhu Kokila
* Tennis Krishna
* Vinaya Prasad
* Avinash
* Rahul Dev
* Satyajit

== Music ==
{{Infobox album  
| Name       = Hara
| Type       = soundtrack
| Artist     = Jassie Gift
| Cover      = 
| Size       = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Anand Audio
| Producer   = 
| Last album = Aryan (2014 film)|Aryan (2014)
| This album = Hara (2014)
| Next album = 
}}
The soundtrack album of the film has been composed by Jassie Gift, while lyrics have been penned by Jayant Kaikini, Keshavaaditya and Kaviraj (lyricist)|Kaviraj.       The audio launch of the album took place on December 31, 2013.   

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = Hara
| extra_column    = Singer(s)
| total_length    = 28:57 Kaviraj
| all_music       = Jassie Gift
| lyrics_credits  = yes
| title1          = Birugaali
| lyrics1         = Jayant Kaikini
| extra1          = Shreya Ghoshal, Haricharan
| length1         = 4:40
| title2          = A Bramha Bareda
| lyrics2         = Keshavaaditya
| extra2          = Anuradha Bhat
| length2         = 4:05
| title3          = Hare Rama Kaviraj
| extra3          = Sooraj Santhosh
| length3         = 4:04
| title4          = Lava Lavike
| lyrics4         = Kaviraj
| extra4          = Tippu (singer)|Tippu, Nanditha
| length4         = 4:40
| title5          = Yaare Nee Sundari
| lyrics5         = Jayant Kaikini
| extra5          = Santhosh, Jyotsna Radhakrishnan
| length5         = 4:22
| title6          = A Brahma Bareda (Male)
| lyrics6         = Keshavaaditya
| extra6          = Santhosh Venky
| length6         = 2:29
| title7          = Birugaali (Female)
| lyrics7         = Jayant Kaikini
| extra7          = Shreya Ghoshal
| length7         = 4:37
}}

== References ==
 

== External links ==
*  

 
 