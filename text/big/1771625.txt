Easy Come, Easy Go (film)
 
{{Infobox film
| name           = Easy Come, Easy Go
| image          = Easy-come-easy-go-movie-poster-1967-1020427150.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster John Rich
| producer       = Hal B. Wallis
| writer         = {{Plainlist|
* Allan Weiss
* Anthony Lawrence
}}
| starring       = {{Plainlist|
*Elvis Presley Pat Priest
}}
| music          = Joseph J. Lilley
| cinematography = William Margulies
| editing        = Archie Marshek
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $2,000,000
| gross          = $1,950,000(US/ Canada)   
}}
Easy Come, Easy Go is a 1967 American musical film comedy starring Elvis Presley. Hal Wallis produced the film for Paramount Pictures, and it was his final movie for Elvis Presley. The film co-starred Dodie Marshall, Pat Harrington, Jr., Pat Priest, Elsa Lanchester and Frank McHugh. (It was McHughs last feature film.) The movie reached #50 on the Variety magazine national box office list in 1967. 
 Double Trouble .

==Plot==
Lieutenant Ted Jackson (Elvis Presley) is a former U.S. Navy frogman who divides his time between twin careers as a deep sea diver and nightclub singer. Ted discovers what he believes could be a fortune in Spanish gold aboard a sunken ship and sets out to rescue it with the help of go-go dancing yoga expert Jo Symington (Dodie Marshall) and friend Judd Whitman (Pat Harrington, Jr.). Gil Carey (Skip Ward), however, is also after the treasure and uses his girlfriend Dina Bishop (Pat Priest) to foil Teds plans.

Elvis sings six songs in the movie: the title song, "Ill Take Love", "Sing You Children", "You Gotta Stop", "Yoga Is as Yoga Does" in a duet with Elsa Lanchester, and "The Love Machine".

==Cast==
* Elvis Presley as Ted Jackson
* Dodie Marshall as Jo Symington Pat Priest as Dina Bishop
* Pat Harrington, Jr. as Judd Whitman
* Skip Ward as Gil Carey
* Sandy Kenyon as Schwartz
* Frank McHugh as Captain Jack
* Ed Griffith as Cooper 
* Read Morgan as Ensign Tompkins
* Mickey Elley as Ensign Whitehead 
* Elaine Beckett as Vicki 
* Shari Nims as Mary 
* Diki Lerner as Zoltan
* Robert Isenberg as Artist
* Elsa Lanchester as Madame Neherina

==Production==
Paramount originally intended to make a movie called Easy Come Easy Go starring Jan and Dean with director Barry Shear but it was cancelled when the stars and several crew were injured in a train crash. MOVIE CALL SHEET: Train Wreck Derails Film
Martin, Betty. Los Angeles Times (1923-Current File)   11 Aug 1965: d12. 

==Soundtrack==
{{Infobox album
| Name        = Easy Come, Easy Go
| Type        = ep
| Artist      = Elvis Presley
| Cover       = Elviseasycomeeasygo.jpg
| Released    = March 1967
| Recorded    = September 1966
| Genre       = Soundtrack
| Length      = 13:41
| Label       = RCA Records
| Producer    = Joseph Lilley
| Reviews     =  Tickle Me (1965)
| This album  = Easy Come, Easy Go (1967)
| Next EP  = 
}} Nashville sessions How Great Thou Art and other songs more to his taste, Presley was reportedly unhappy with the quality of the songs selected for the film, allegedly referring to the selections as "shit" during the recording session.  It is often reported   that Presley recorded "Leave My Woman Alone" for the film, but only an instrumental backing was ever recorded; Presley never recorded a vocal for the song.   Seven selections were recorded for the film; the song "Shes A Machine" was not used in the movie, but would be released on Elvis Sings Flaming Star the following year. 
 extended play single released to coincide with the March 1967 premiere of the film. It failed to chart on the Billboard Hot 100|Billboard Hot 100, and sold fewer than 30,000 units total. Jorgensen, Elvis Presley A Life in Music: The Complete Recording Sessions. New York: St. Martins Press, 1998;  p. 224.  Given that the EP format was no longer a viable marketing medium, and the poor performance of Easy Come, Easy Go, it was the final release of new material by Presley in the EP format. 

===Personnel===
* Elvis Presley - vocals
* The Jordanaires - background vocals
* Anthony Terran, Mike Henderson Jerry Scheff - trumpets
* Butch Parker - trombone
* Meredith Flory, William Hood - saxophones
* Scotty Moore - lead guitar
* Tiny Timbrell - rhythm guitar
* Charlie McCoy - acoustic guitar, organ (music)|organ, harmonica
* Michael Rubini - harpsichord
* Bob Moore,  - double bass drums
* Emil Radocchia, Curry Tjader, Larry Bunker- percussion

===Track listing===
{{Track listing
|headline=Side one
|writing_credits=yes
|extra_column=Recording date
|title1=Easy Come, Easy Go
|writer1=Sid Wayne and Ben Weisman
|extra1=September 28, 1966
|length1=2:07
|title2=The Love Machine
|writer2=Chuck Taylor, Fred Burch, Gerald Nelson
|extra2=September 29, 1966
|length2=2:10
|title3=Yoga Is As Yoga Does
|writer3=Fred Burch and Gerald Nelson
|extra3=September 29, 1966
|length3=2:51
}}
{{Track listing
|headline=Side two
|writing_credits=yes
|extra_column=Recording date
|title1=You Gotta Stop
|writer1=Bernie Baum, Bill Giant, Florence Kaye
|extra1=September 29, 1966
|length1=2:20
|title2=Sing You Children
|writer2=Fred Burch and Gerald Nelson
|extra2=September 28, 1966
|length2=2:00
|title3=Ill Take Love
|writer3=Dolores Fuller and Mark Barker
|extra3=September 28, 1966
|length3=2:13
}}

==References==
 

==External links==
*  
*  

;DVD Reviews
*  by Paul Mavis at  , August 6, 2007.
*  by Jeff Rosado at  , March 4, 2003.

 

 
 
 
 
 
 
 