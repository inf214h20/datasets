The Bunny Game
 
{{Infobox film
 | name = The Bunny Game
 | image =   The Bunny Game.jpg
 | director = Adam Rehmeier
 | writer =  
 | starring =   Rodleen Getsic
 | music =  
 | cinematography = Adam Rehmeier
 | editing =  Adam Rehmeier
 | producer =  
 | distributor =  
 | released =   
 | runtime =  
 | awards = 
 | country =  United States
 | language = English 
 | budget = 
 }}
The Bunny Game  is a 2011 American independent horror film directed by Adam Rehmeier who also was co-author of the script together with the lead actress Rodleen Getsic.
 limited theatrical release, then was released on DVD and Blu-ray Disc on July 31, 2012.  

==Plot==
Drug addicted prostitute "Bunny" propositions a truck driver, who has a fetish for asphyxiation. He kidnaps her and subjects her to various brutal, humiliating and extreme forms of physical and sexual abuse.

== Cast ==
* Rodleen Getsic as Bunny
* Jeff Renfro as Hog
* Gregg Gilmore as Jonas
* Drettie Page as Martyr

==Production==
Filming took place on October 2008 for a period of thirteen days. 

==Release==
The film entered the 2011 PollyGrind Film Festival,  in which it won several awards including "Best Cinematography", "Best Editing" and "Best Overall Individual Performance in a Film" (to Rodleen Getsic). 
 
==Controversy==
The film raised some controversies and was banned in the UK by the British Board of Film Classification due to its graphic scenes of sexual and physical abuse. 

==Reception==
The film received negative reviews. IMDb gave at the film 3.7/10 positives reviews.

==See also==
*List of black-and-white films produced since 1970

==References==
 

==External links==
* 

 
 
 
 
 
 

 