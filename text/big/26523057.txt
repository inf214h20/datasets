The Red Shoes (2010 film)
{{Infobox film
| name           = The Red Shoes
| image          = The_Red_Shoes_poster_2010_film.jpg
| alt            =  
| caption        = Original poster
| director       = Raul Jorolan
| producer       = Tony Gloria Tito Velasco James Ladioray
| writer         = James Ladioray
| starring       = Marvin Agustin Nikki Gil Liza Lorena Tessie Tomas Tirso Cruz III Tetchie Agbayani Iwa Moto Monica Llamas Irene Celebre
| music          = Jessie Lasaten
| cinematography = Ike Avellana
| editing        = Ike Veneracion
| studio         = 
| distributor    = Unitel Productions
| released       =  
| runtime        = 97 minutes
| country        = Philippines
| language       = Filipino Tagalog English
| budget         = 
| gross          = 
}}
The Red Shoes is a 2010 Filipino film produced by Unitel, known for the box-office and critical hits Crying Ladies and Santa Santita. The movie is unusual in Philippine cinema because of its non-linear narrative style and use of history to frame a tale of lost love and redemption spanning close to three decades, starting in 1981 with the tragic accident in the construction of the Manila Film Center, through to the 1986 People Power Revolution, which ended the 21-year Presidency of Ferdinand Marcos, and finally concluding in 2009, which finds the major characters of the film facing the consequences of the theft of one of 3,000 pairs of shoes of the First Lady Imelda Marcos. The film won the Special Jury Prize in the 2010 Bogota International Film Festival. It participated in the 2010 Vietnam International Film Festival in Hanoi and the 2010 China Golden Rooster and Hundred Flowers Film Festival. The film was rated "A" by the Cinema Evaluation Board of the Philippines and was also cited as Best Picture in the 2010 Catholic Mass Media Awards in the Philippines.

==Plot== People Power Revolution in the Philippines in February 1986, 10-year-old Lucas Munozca (Marvin Agustin) finds himself swept up with a euphoric crowd that has entered the grounds of the Malacañan Palace|Malacañang Presidential Palace. Lost in the tumult, Lucas stumbles into a room where the presidents wife, Imelda Marcos, kept her infamous shoe collection. He plans to filch two pairs but only manages to steal one pair—for poetic justice and for love.

Lucas gives the pair of red shoes from the countrys First Lady to the first ladies of his life. The right shoe he gives to his mother, Chat (Liza Lorena), who ekes out a living giving manicures and pedicures while seemingly unable to recover from the loss of her husband, Domingo (Tirso Cruz III), the father of her only son, who was among the 169 workers said to have been buried alive when the upper levels of the Manila Film Center collapsed under construction in 1981 for the Manila International Film Festival project of the First Lady. The left shoe Lucas gifts to the love of his life, Bettina (Nikki Gil), the daughter of one of her mothers wealthy manicure home service customers.

Lucas and Bettina eventually become a couple years later and go steady for 13 years until a betrayal ends the relationship. At this point, the red shoes again become crucial for both mother and son in their attempt to overcome their respective loneliness: Chat, in trying to contact her husband from beyond the grave through Madame Vange (Tessie Tomas), a spiritist whose main job is being an Imelda impersonator; and Lucas, in trying to redeem himself for having lost his true love in a moment of weakness.

==Cast==
 
*Marvin Agustin as Lucas
*Nikki Gil as Bettina
*Liza Lorena as Chat, Lucas mother
*Tessie Tomas as Madame Vange, spiritist/Imelda impersonator
*Tirso Cruz III as Domingo, Lucas father
*Tetchie Agbayani as Bettinas mother
*Iwa Moto as Gidget, Bettinas colleague
*Monica Llamas as Sabs, Lucas current girlfriend
*Irene Celebre as Maita, woman from Lucas past
*Renzo Renacido as the kid swimmer 
==Production==
 Cinemalaya Philippine Independent Film Festival in 2007, a screenplay entitled 2,999, which told the story of a missing pair of shoes from Imelda Marcoss collection of 3,000 pairs of shoes, was one of the finalists that year. Unfortunately, the filmmakers were unable to finish the entry on time so they decided to pull out. 
Marvin Agustin liked the story so much that he called writer James Ladioray to present to Unitel President and CEO Tony Gloria, who was instantly drawn to the story and wasted no time in greenlighting the project.   

"The story unfolds through three decades as we tell Lucas’ story," Jorolan lets on. "Young, resolute and in love, there is nothing that will stop him from pursuing his goals – not even committing an act amidst the chaos and confusion of the 1986 People Power Revolution – all in the name of love." 

Jorolan, a TV commercial director, faced other challenges. Lead stars Agustin and Gil, contract actors from rival TV stations GMA Network and ABS-CBN Broadcasting Corporation, had to squeeze in shooting in their competing work schedules, stretching principal photography, which took only 20 days, over 10 months. Jorolan and Ladioray, an advertising creative director, had to shuttle in between their regular work and the movie set to complete the shoot.   

Ladioray adds, "I didnt want to make an outrightly political movie. That part in our history was very decisive, I wanted to make a story that can use that turning point as a point of redemption for everyone. The shoes are a metaphor — I wanted to ask the question, would you steal for love?"     

Jorolan speaks of another metaphor that was crucial to the story, the bridge. "A bridge connotes relationship, whether its been cut short, it still holds its purpose, similar to a relationship thats been broken off. (It still holds significance)." 

==Awards and Recognition==
*27th Bogota International Film Festival, 2010: Special Jury Recognition Award (Mencion de Honor)
*32nd Catholic Mass Media Awards Philippines, 2010 : Best Picture
*1st Vietnam International Film Festival, 2010: Official Selection and Best Actress Nomination
*19th China Golden Rooster and Hundred Flowers Film Festival, 2010 : Exhibition
*Cinema Evaluation Board of the Philippines, 2010 : Rated "A"
*27th Philippine Movie Press Club Star Awards for Movies, 2011 : Nominations for the Digital Movie Category - Best Movie, Best Director, Best Screenplay, Best Cinematographer, Best Musical Scorer, Best Sound Engineer, Best Original Movie Theme Song for "Bawat Hakbang"
*29th Luna Awards by the Film Academy of the Philippines, 2011 : Nomination for Best Sound

==References==
 

==External links==
* 
* 
*  The Red Shoes is rated "A" by Cinema Evaluation Board
*  Imeldific in a Good Way - Manila Times Review
*  Love in the Form of 8  Stilettos
*  The Tale of Imeldas Stolen Pair of Red Shoes
*  Thepoc.net Review
*  Telebisyon.net
*  Telebisyon.net
*  Pep.ph Review
* 
*  The Red Shoes Wins Big in South America Filmfest
*  Pinoy Film Honored in Bogota
*  Colombia Honors RP Film
*  Best Picture 2010 CMMA
*  27th PMPC Star Awards Nominations for The Red Shoes
*  The Red Shoes 29th Luna Awards Nomination for Best Sound


 
 
 
 
 