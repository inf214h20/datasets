We Are All Murderers
{{Infobox film
| name           = Nous sommes tous des assassins
| image          = We Are All Murderers.jpg
| image size     =
| caption        =
| director       = André Cayatte
| producer       =
| writer         =
| narrator       =
| starring       = Marcel Mouloudji
| music          = Raymond Legrand
| cinematography = Jean Bourgoin
| editing        = Paul Cayatte
| distributor    =
| released       = 21 May 1952 (France) 8 January 1957 (USA)
| runtime        = 115 mins
| country        = France/Italy
| language       = French
| budget         =
| gross          =
}}
We Are All Murderers ( , also known as Are We All Murderers?) is a 1952 French film written and directed by André Cayatte, a former attorney. It tells the story of René, a young man from the slums, trained by the French Resistance in World War II to kill Germans. He continues to kill long after the war has ended, as it is all he knows. 
 Special Jury Prize.   

==Plot==
René Le Guen (Marcel Mouloudji) is a former resistance fighter trained as a young man as a professional killer. After World War II, he has no qualms in applying these skills and is arrested for murder. Convicted and condemned to death, he is held in a prison cell with other murderers sentenced to death. Men to be guillotined are taken out at night, so they wait in fear and only sleep after dawn. While Le Guens lawyer (Claude Laydu) tries to achieve a pardon for his client, three of Le Guens fellow inmates are executed, one by one, in the course of the film.

Cayatte used his films to reveal the inequities and injustice of the French system, and protested against capital punishment.  

==Cast==
* Marcel Mouloudji - René Le Guen
* Raymond Pellegrin - Gino Bollini
* Antoine Balpêtré - Dr. Albert Dutoit
* Julien Verdier - Bauchet
* Claude Laydu - Philippe Arnaud
* Georges Poujouly - Michel Le Guen
* Jacqueline Pierreux - Yvonne Le Guen (version française)
* Lucien Nat - Lavocat général
* Louis Arbessier - Lavocat du tribunal pour enfants
* René Blancard - Albert Pichon
* Léonce Corne - Le colonel instructeur
* Henri Crémieux - Lavocat de Bauchet
* Jean Daurand - Girard, lhomme dans la cabine téléphonique
* Yvonne de Bray - La chiffonnière
* Guy Decomble - Un inspecteur
* Liliane Maigné – Rachel

==Honors== Special Jury Prize at the Cannes Film Festival in 1952.

==References==
 

==External links==
* 

 

 
 
 
 
 

 