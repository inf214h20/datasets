Striptease (film)
{{Infobox film
| name           = Striptease
| image          = Striptease_movie_poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Bergman
| producer       = Andrew Bergman Mike Lobell
| screenplay     = Andrew Bergman
| based on       =  
| starring       = Demi Moore Armand Assante Ving Rhames Robert Patrick Burt Reynolds
| music          = Howard Shore
| cinematography = Stephen Goldblatt
| editing        = Anne V. Coates
| studio         = Castle Rock Entertainment Lobell/Bergman Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 minutes  
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $113,309,743 
}} novel of the same name by Carl Hiaasen; it is about a stripper who becomes involved in both a child-custody dispute and corrupt politics.
 Award for Worst Picture of 1996.

==Plot==
  FBI secretary Congressman named David Dilbeck visits the club and immediately begins to adore Grant. Aware of Dilbecks embarrassing indulgences, another Eager Beaver patron approaches Erin with a plan to manipulate the Congressman to settle the custody dispute in Erins favor. However, Dilbeck has powerful business connections who want to ensure he remains in office. Consequently, those who can embarrass him in an election are murdered. Meanwhile, Erin retrieves her daughter from her negligent husband.

Dilbecks personal interest in Erin persists, and she is invited to perform privately for him. He asks her to become his lover and later his wife, despite his staffs concerns that she knows too much. A debate occurs as to whether to kill Erin or simply keep her quiet by threatening to take away her daughter (Rumer Willis). However, Erin and a police officer (Armand Assante) begin to suspect the Congressmans guilt in the murders, and Erin concocts a plan to bring the Congressman to justice. She tricks him into confessing on tape, and he is soon after arrested. Thus, Erin regains full custody of Angela, and Darrell returns to prison.

==Cast==
* Demi Moore as Erin Grant
* Armand Assante as Lt. Al Garcia
* Ving Rhames as Shad
* Robert Patrick as Darrell Grant
* Burt Reynolds as Congressman David Dilbeck
* Siobhan Fallon Hogan as Rita Grant
* Rumer Willis as Angela Grant
* Marc Chaykin as the Bachelor
* Paul Guilfoyle as Malcolm Moldowsky
* Jerry Grayson as Orly
* Robert Stanton as Erb Crandal
* William Hill as Jerry Killian
* Stuart Pankin as Alan Mordecai
* PaSean Wilson as Sabrina Hepburn
* Dina Spybey as Monique, Jr.
* Barbara Alyn Woods as Lorelei
* Pandora Peaks as Urbana Sprawl
* Kimberly Flynn as Ariel Sharon
* Rena Riffel as Tiffany

==Production== Strip Tease Floridian crime writer Carl Hiaasen. It was published in 1993 and was a bestseller. The screenplay itself was written by Andrew Bergman, who also directed. According to one critic, the novels plot is "quite faithfully followed" by the screenplay, but in bringing the complicated story to the screen, "Bergman forgets to explain persuasively what a nice girl like Erin — smart, spunky and a former FBI employee — is doing in a dump called the Eager Beaver." 

Concerns that the ending of the film was not comical enough resulted in rewrites and reshoots, causing a one-month delay. Chris Nashawaty, " ." Entertainment Weekly 04/26/96, URL accessed 16 August 2006.   Part of these concerns owed to test screenings, where audiences objected to a scene where Dilbeck becomes violent. Later test screenings also turned up less than favorable reactions. 

===Casting===
  plays Erin Grant and received a record salary for the film.]]
Moore played the main female character, Erin Grant. For the film, she was paid $12.5 million, which was at the time a record for an actress. To prepare for her role, Moore visited strip clubs in  ), URL accessed August 13, 2006.  though this was the sixth time she showed her breasts on film. Gregory Cerio and Carolyn Ramsay, "Eye of the tiger,"   05/19/95, URL accessed 16 August 2006.  In the first attempt at filming Moore stripping, two hundred actors were used to portray the audience. Although their salaries were small, many accepted the role to see Moore nude. After waiting for a while, when Moore finally appeared and started dancing the crowd turned so loud and wild that the shooting had to temporarily cease. As Moore said, "After my experience, I felt very confident." 

The cast included some notable real-world strippers such as Pandora Peaks. Rhames plays a bouncer named Shad. The filmmakers, in trying actors out for Shads part, looked for someone "at least 62 and physically massive...any ethnicity."  (Rhames is African American). Reynolds played Congressman Dilbeck, and he based his performance after politicians he knew in his early life, through his father, a police chief. Barbara Cramer, "Film reviews," Films in Review, September/October 1996, Vol. 47 Issue 9/10, page 67-68.  Reynolds was not an actor that the filmmakers originally had in mind for the part, but Reynolds wanted it, contacted Castle Rock head Rob Reiner, and traveled to Miami to audition.  He accepted a salary lower than what he had made in his earlier career.  Moores own daughter Rumer Willis played Erins daughter Angela. As Moore explained, "she   wanted it so badly" that Moore asked that Willis be considered for the part. In reality this required Willis to see Moore dancing topless, for a scene in which Angela sees Erin performing.  However, Moore said that this was acceptable, as "We dont shame the body, we encourage the body as something beautiful and natural, and my children bathe with me, and I walk around naked." 

==Soundtrack==
{{Infobox album  
| Name        = Striptease: Music from the Motion Picture Soundtrack
| Type        = film
| Artist      = Various Artists
| Released    =  
| Label       = Capitol Records
}} Little Bird" Missionary Man", which was played during the end credits. Furthermore, it excluded the song "(Pussy, Pussy, Pussy) Whose Kitty Cat Are You?" by the Light Crust Doughboys which won the Golden Raspberry Award for Worst Original Song.
{{Track listing
| collapsed       = no
| extra_column    = Artist(s)
| headline        = Striptease: Music from the Motion Picture Soundtrack 
| title1          = Gimme Some Lovin
| extra1          = The Spencer Davis Group
| length1         = 2:58
| title2          = Get Outta My Dreams, Get into My Car
| extra2          = Billy Ocean
| length2         = 5:33 The Tide Is High Blondie
| length3         = 4:42
| title4          = Expressway to Your Heart
| extra4          = The Soul Survivors
| length4         = 2:16
| title5          = Green Onions
| extra5          = Booker T. & the M.G.s
| length5         = 2:51 Love Child (Halaila)
| extra6          = Laladin
| length6         = 3:18
| title7          = I Live for You
| extra7          = Chynna Phillips
| length7         = 3:45
| title8          = Youve Really Got a Hold on Me
| extra8          = The Miracles
| length8         = 2:59 Mony Mony
| extra9          = Billy Idol
| length9         = 5:03
| title10         = If I Was Your Girlfriend Prince
| length10        = 3:46
| title11         = I Hate Myself for Loving You
| extra11         = Joan Jett and the Blackhearts
| length11        = 4:12
| title12         = Sweet Dreams (Are Made of This)
| extra12         = Eurythmics
| length12        = 3:36
| title13         = Return to Me
| extra13         = Dean Martin
| length13        = 2:24
| title14         = Missionary Man (Eurythmics song)
| length14        = 3:50
| extra14         = Eurythmics
}}

==Release==
Striptease was distributed by Sony and was finally released in the United States on June 28, 1996, after a June 23 premiere in New York City. It opened in Australia, France and Germany in August, and Argentina, Italy, Bolivia, South Africa, the United Kingdom, Brazil and Japan in September. 

 , June 28, 1996.  The Motion Picture Association of America raised concerns regarding a posters which it felt revealed too much of Moores naked body. A Castle Rock employee argued: "There are racier perfume ads." 

The previous years film about nude dancers, Showgirls, was generally disliked, so filmmakers feared audiences would pre-judge Striptease on this basis. To avoid any association, advertisements were designed to make Striptease look more comedic than Showgirls, which was a drama.  Besides the subject matter, Striptease and Showgirls did have two notable connections. The choreography in these films was by the same person, Marguerite Derricks.  Both also featured performances by Rena Riffel, who plays a dancer in each. To promote the film, Moore appeared on the Late Show with David Letterman and a Barbara Walters special. In both cases, she danced or otherwise exhibited her body. 

==Reception==

===Critical response===
Striptease received generally negative reviews from film critics. Roger Ebert of the Chicago Sun-Times complimented some of the characters, but ultimately concluded the film failed because "all of the characters are hilarious except for Demi Moores." He felt the drama surrounding the main character "throws a wetblanket over the rest of the party." Ebert also found the nudity not too sexy.  Leonard Maltin was harsher, writing in his book that the film was too depressing, and "Not funny enough, or dramatic enough, or sexy enough, or bad enough, to qualify as entertainment in any category."  Barbara Cramer concurred with Ebert that Moores character was written too dramatically, compared to other characters. She said the film was predictable and would appeal mostly to "post-pubescent schoolboys or closet voyeurs." However, Cramer also cited Reynolds "his best role in years," and that Rhames was "worth the price of admission."  Brian D. Johnson of Macleans, who thought Moores acting was terrible, predicted that despite Moores financial success, her career depended on the success of this film and the film was "tacky, pretentious-and boring."  This critic described Striptease as displaying Moores vanity.  Dave Ansen of Newsweek, sharing Eberts view on Moores character, also claimed Striptease failed as a drama because it had no Mystery fiction|mystery, revealing the identity of its villains early. Moreover, the "damsel in distress|damsel-in-distress angle generates zero tension."  Daniel P. Franklin,
in his book Politics and Film: The Political Culture of Film in the United States went so
far as to call Striptease "the worst film ever made"  Daniel P. Franklin, 
Politics and Film: The Political Culture of Film in the United States.  Rowman and Littlefield, 2006, 
ISBN 0742538095,  (p. 203).  and stated "The film pays homage to Moores 
surgical breast enhancement".  Nathan Rabin, reviewing the film
for his series "My Year of Flops", described the film thus: " Moores dour lead performance sabotages the film from the get-go. Its as if director Andrew Bergman told Moore she was acting in a serious drama about a struggling single mother...and then told everyone else in the cast that they were making a zany crime comedy filled with kooky characters, sleazy hustlers, dumbass opportunists, and outsized caricatures."  Nathan
Rabin,  . The Onion AV Club.June 7, 2007  Retrieved 19 July 2014.  Striptease currently holds a 12% rating on Rotten Tomatoes based on 68 reviews.

===Accolades=== Golden Raspberry The Island Barb Wire, The Stupids, and Ed (film)|Ed. Moore won for Worst Actress while she and Reynolds shared for Worst Screen Couple.

===Box office=== The Nutty The Hunchback of Notre Dame, in which Moore voiced one of the main characters.  Ultimately, Striptease made $33,109,743 in the United States, and domestically it was the 47th highest grossing film of 1996. It made $113,309,743 internationally,  , Box Office Mojo, URL accessed 13 August 2006.  having grossed £2,104,480 in the UK and ¥102,419,500 in Japan. 

===Legacy===
In 1997, Striptease made news again when it was shown in a fourth-grade class in Chicago, Illinois. The teacher claimed the students chose the film, but he drew criticism since the film was risqué. (The violent 1996 film Scream (1996 film)|Scream was shown in the same school on the same day, causing further controversy.)  In 2000 in Ireland, some viewers criticized the Radio Telefís Éireann for running Striptease. These viewers questioned the films appropriateness and some considered it demeaning to women. However, the station felt it was not pornography and it was aired at night. 

In 2003, Radioactive Films used a scene from Striptease featuring Moore nude in a video called Hollywoods Hottest. This raised a dispute as to whether use of the scene qualified as fair use. A lawsuit was launched as a consequence. 

==References==
 
 Please do not type footnotes here. Instead insert the footnote in its proper spot in the body of this article using the    tags. See   for an explanation of how to generate footnotes using the tags. -->

==External links==
 
*  
*  
*  
*  

   
{{Succession box
| title=Golden Raspberry Award for Worst Picture
| years=17th Golden Raspberry Awards
| before=Showgirls The Postman
}}
 
 
 
 
 
 

 
 
   
   
   
 
 
 
 
 
 
 
 
 