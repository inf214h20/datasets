While the Sun Shines
{{Infobox film
| name           = While the Sun Shines
| image          = 
| caption        = 
| director       = Anthony Asquith     
| producer       = Anatole de Grunwald
| writer         = Terence Rattigan Anatole de Grunwald
| based on       =   Ronald Howard Philip Green
| cinematography = Jack Hildyard
| editing        = Frederick Wilson ABPC
| Associated British-Pathé Stratford Pictures Corporation
| released       =  
| runtime        = 82 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = £146,173 (UK) 
| preceded_by    = 
| followed_by    =
}}
 1947 Cinema British comedy film directed by Anthony Asquith. It was based on Terence Rattigans 1943 play of the same name. 

==Plot==
Lady Elisabeth Randall is an English Air Force corporal during World War II. She is on her way to marry her fiancé when she finds herself being romanced by two different men. The first man is Colbert, a Frenchman residing in England. The second man is Joe Mulvaney, an American lieutenant. Difficulties ensue as Randall finds that due to these romances both her military career and her impending marriage are in danger.

==Cast==
*Barbara White.....Lady Elisabeth Randall
*Michael Allan.....Colbert
*Bonar Colleano.....Joe Mulvaney
*Ronald Howard.....Earl of Harpenden
*Ronald Squire - Duke of Ayre
*Brenda Bruce - Mabel Crum
*Miles Malleson - Horton
*Margaret Rutherford - Doctor Winifred Frye
*Cyril Maude - Admiral
*Garry Marsh - Mr Jordan
*Joyce Grenfell - Daphne
*Charles Victor - Tube Train Conductor
*Wilfrid Hyde-White - Receptionist
*Aubrey Mallalieu - Night Porter

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 
 