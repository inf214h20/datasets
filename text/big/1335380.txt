Exodus (1960 film)
 
{{Infobox film
| name           = Exodus
| image          = Exodus poster.jpg
| caption        = Theatrical release film poster by Saul Bass
| director       = Otto Preminger
| producer       = Otto Preminger
| writer         = Dalton Trumbo
| based on       =  
| starring       = Paul Newman Eva Marie Saint Ralph Richardson Peter Lawford Sal Mineo Jill Haworth Lee J. Cobb John Derek Ernest Gold
| cinematography = Sam Leavitt, ASC
| editing        = Louis R. Loeffler
| distributor    = United Artists MGM (DVD)
| released       =  
| runtime        = 208 minutes
| country        = United States
| language       = English
| budget         = $4.5 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 133 
| gross =  $8,700,000 (US/ Canada)  $20 million (worldwide) 
}}
 soundtrack music Ernest Gold.

Widely characterized as a "Zionist epic",     the film has been identified by many commentators as having been enormously influential in stimulating Zionism and support for Israel in the United States.    Although the Preminger film softened the anti-British and anti-Arab sentiment of the novel, the film remains controversial for its depiction of the Arab-Israeli conflict, and for what some scholars perceive to be its lasting impact on American views of the regional turmoil.   It would also be famous for the hiring by Preminger of screenwriter Dalton Trumbo, who was blacklisted for being a Communist: he was hired and was later sought for other scripts by other 
studios.

==Plot summary==
The film is based on the events that happened on the ship SS Exodus|Exodus in 1947 as well as events dealing with the founding of the modern state of Israel in 1948.
 Nurse Katherine internment camp the Second illegal voyage Mandate Palestine before being discovered by military authorities. When the British find out that the refugees are in a ship in the harbor of Famagusta, they blockade it. The refugees stage a hunger strike, during which the camps doctor dies, and Ari threatens to blow up the ship and the refugees. The British relent and allow the Exodus safe passage.

Meanwhile, Kitty has grown very fond of Karen Hansen (Jill Haworth), a young History of the Jews in Denmark|Danish-Jewish girl searching for her father, from whom she was separated during the war. She has taken up the Zionist cause, much to the chagrin of Kitty, who had hoped to take young Karen to America so that she can begin a new life there.

During this time, opposition to the partition of Palestine into Arab and Jewish states is heating up, and Karens young beau Dov Landau (Sal Mineo) proclaims his desire to join the Irgun, a radical Zionist underground network. Dov goes to an Irgun address, only to get caught in a police trap. After he is freed, he is contacted by members of the Irgun and is interviewed by Ari Ben Canaans uncle Akiva (David Opatoshu). Before swearing Dov in, Akiva forces the boy to confess that he was a Sonderkommando in Auschwitz and that he was raped by Nazis.  Due to his activities, Akiva has been disowned by Aris father, Barak (Lee J. Cobb), who heads the mainstream Jewish Agency trying to create a Jewish state through political and diplomatic means. He fears that the Irgun will damage his efforts, especially since the British have put a price on Akivas head.

Karen has gone to live at Gan Dafna, a fictional Jewish kibbutz near Mount Tabor at which Ari was raised.  Kitty and Ari have fallen in love but Kitty pulls back, feeling like an outsider after meeting Aris family and learning of his previous love interest, Dafna, a young woman tortured and murdered by Arabs, who is the namesake of the Gan Dafna kibbutz. Leaving Kitty, Ari promises to help find Karens father, who is eventually found ill in hospital in Jerusalem and does not recognize Karen.
 bombs the King David Hotel in an act of terrorism, leading to dozens of fatalities, Akiva is arrested, imprisoned in Acre fortress, and sentenced to hang. Seeking to save Akivas life, as well as to free the Haganah and Irgun fighters imprisoned by the British, Ari organizes an escape plan for the prisoners.

Dov, who had managed to elude the arresting soldiers, turns himself in so that he can use his knowledge of explosives to facilitate the Acre Prison break. All goes according to plan; hundreds of prisoners, including Akiva, manage to escape.  Akiva is fatally shot by British soldiers while evading a roadblock set up to catch the escaped prisoners. Ari is also badly wounded. He makes his way to Abu Yesha, an Arab village near Gan Dafna, where his lifelong friend, Taha, (John Derek) is the mukhtar. Kitty is brought there and treats his wound and Ari and Kittys romance is rekindled.

An independent Israel is now in plain view, but Arab nationals commanded by Mohammad Amin al-Husayni, the Grand Mufti of Jerusalem, plot to attack Gan Dafna and kill its villagers. Ari receives prior warning of this attack from Taha, and he manages to get the younger children of the town out in a mass overnight escape. Karen, ecstatic over the prospect of a new nation, finds Dov (who was out on patrol outside the town) and proclaims her love for him; Dov assures her that they will marry someday. As Karen returns to Gan Dafna, she is ambushed and killed by a gang of Arab militiamen. Dov discovers her lifeless body the following morning. That same day, the body of Taha is found hanging in his village, killed by Arab extremists with a Star of David symbol carved on his body. Karen and Taha are buried together in one grave. At the Jewish burial ceremony, Ari swears on their bodies that someday, Jews and Arabs will live together and share the land in peace, not only in death but also in life. The movie then ends with Ari, Kitty, and a Palmach contingent entering trucks and heading toward battle.

==Cast==
*Paul Newman as Ari Ben Canaan
*Eva Marie Saint as Kitty Fremont
*Ralph Richardson as Gen. Sutherland
*Peter Lawford as Maj. Caldwell
*Lee J. Cobb as Barak Ben Canaan
*Sal Mineo as Dov Landau
*John Derek as Taha
*Hugh Griffith as Mandria
*Gregory Ratoff as Lakavitch
*Felix Aylmer as Dr. Lieberman
*David Opatoshu as Akiva Ben-Canaan
*Jill Haworth as Karen  Hansen Clement
*Marius Goring as Von Storch
*Alexandra Stewart - Jordana Ben Canaan 
*Michael Wager as David Martin Benson - Mordekai Paul Stevens - Reuben
*Victor Maddern as Sergeant
*George Maharis as Yoav
*Esther Ofarim as Mrs. Hirschberg

==Awards and nominations==
;Academy Awards Ernest Gold Best Original 1960 Oscars.
 Best Supporting Best Cinematography (Sam Leavitt).

;Golden Globe Best Supporting Actor Award

;Grammy Award Best Soundtrack Song of the Year at the Grammy Awards of 1961 for the soundtrack and theme to Exodus respectively. It is the only instrumental song ever to receive that award to date.

;Cannes Film Festival
The film was screened at the 1961 Cannes Film Festival, but was not entered into the competition for the Golden Palm.   

==Soundtrack==
  The Eagles, Gospel pianist Gaither Vocal classical pianist samples of the Exodus theme have been used in several Hip hop music|hip-hop songs, including Ice-T´s song "Ices Exodus" from the album The Seventh Deadly Sin, Nass song "Youre Da Man" from the album Stillmatic, and T.I. (rapper)|T.I.s song "Bankhead" from the album King (T.I. album)|King. A portion of the main title was included in a montage arranged by composer John Williams and performed at the 2002 Academy Awards ceremony. The artist Nina Paley used the entire theme song to satirical effect in her animated short, titled after the lyrics, "This Land is Mine" (2012). 
   Although not in an official film soundtrack, Nocturnes, Op. 9 (Chopin)|Chopins Nocturne was played while General Sutherland and Kitty Fremont discussed the future of Jews and Palestine. 

==See also==
* 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 