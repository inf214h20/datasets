Flight (1929 film)
{{Infobox film
| name           = Flight
| image          = Flight (1929 film).jpg
| image size     = 
| caption        = 
| director       = Frank Capra
| producer       = Harry Cohn Frank Capra
| writer         = Ralph Graves (story) Frank Capra (dialogue) Howard J. Green (uncredited) Jack Holt Ralph Graves Lila Lee Joseph Walker Paul Perry Elmer Dyer (aerial)  
| editing        = Ben Pivar Maurice Wright Gene Milford
| studio         = Columbia Pictures
| distributor    = Columbia Pictures (US) Woolf & Freedman Film Service (UK)
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} adventure and Jack Holt, Lila Lee and Ralph Graves, who also came up with the story, for which Capra wrote the dialogue.  Dedicated to the United States Marine Corps, the production was greatly aided by their full cooperation. 

==Plot== runs the Jack Holt), Harold Goodwin), and cannot take off, resulting in a crash. Panama rescues Lefty from the burning aircraft, suffering burns to his hands. Lefty is "washed out" by his squadron commander, Major Rowell (Alan Roscoe).

Lefty is taken to the base hospital, where he falls for Navy nurse Elinor Murray (Lila Lee). When the "Flying Devils" squadron is sent to quell bandit attacks by the notorious General Lobo in Nicaragua, Panama arranges for Lefty to accompany him as his mechanic. Panama shows Lefty a photograph of Elinor, the love of his life, not knowing Lefty is in love with her too. When Elinor is sent to Nicaragua, she does not understand the guilt-stricken Leftys cool reception. When the girl-shy Panama asks Lefty to propose to Elinor on his behalf, Elinor confesses her love for him instead, after which Panama accuses Lefty of betrayal. 

An urgent call for help by a Marine outpost under bandit attack stops any confrontation. Lefty flies as gunner for Steve Roberts, who makes fun of him about shooting in the right direction. During the mission, their aircraft is shot down in a swamp. Unwilling to join in the rescue, Panama reports in sick, but once Elinor convinces him that Lefty never betrayed him, he flies his own solo rescue mission. At the crash site, Roberts dies of his injuries and is cremated by Lefty using their aircraft as a funeral pyre. Panama finds Lefty but is wounded by bandits led by General Lobo, after his landing. Lefty kills the attacking bandits, takes off, and brings the pair back, putting on an impressive flying display over the base that includes safely landing the aircraft after it loses a wheel. Sometime later, Lefty has won his wings and is now an instructor at the school, married to Elinor. When his wife arrives in their new car, Lefty accidentally pulls away in reverse.

==Cast==
  Jack Holt as Gunnery Sergeant "Panama" Williams    
*Lila Lee as Elinor Murray
*Ralph Graves as Corporal "Lefty" Phelps 
*Eddy Chandler* as Marine Sergeant, Panamas buddy
*Edgar Dearing* as Football Coach
*Jimmy De La Cruze* as "Lobo" Sandino, the bandit leader Harold Goodwin* as Corporal Steve Roberts George Irving* as Marine Colonel in Nicaragua
*Alan Roscoe* as Major James D. Rowell
 
*&nbsp;uncredited
 s in flight, c. 1929]]
==Production== Consolidated NY-1B Curtiss OC-2 aircraft from Marine Attack Squadron 231 (VMA-231) were featured in the aerial combat sequences.  The squadron, along with VO-10M (Marine Observation Squadron 10), also prominently appeared in the Devil Dogs of the Air (1935). McBride 1992, p. 205. 

A total of 28 aircraft were at Capras disposal and with the benefit of using actual aircraft, Capra did not have to rely on "process shots" or special effects which was the standard of the day, although dangerous crash scenes and a mass night takeoff were staged using studio miniatures. Along with principal aerial photographer, Elmer Dyer, who filmed from a camera-equipped aircraft, Capra flew alongside in a directors aircraft to coordinate the aerial scenes. Jack Holt who was an accomplished pilot, flew in the film but crashed during one scene. Capra pushed for aerial close-ups and in one scene, wanted Holt to stand up in the cockpit but his parachute had deployed, and he remained seated, causing the scene to be abandoned.  Noted Marine Corps exhibition pilots Lts. Bill Williams and Jerry Jerome were also involved in the production. Capra 1971, p. 109.  

===Historical accuracy=== Sandinista rebels.  Importing fire ants for the swamp scene became controversial as the ants were capable of biting through the actors clothing. 
 Columbia studio boss Harry Cohn, during the 1929 Rose Bowl when Roy Riegels was tackled by his own team after picking up a fumble and running toward his own goal line. Footage from the actual game is used. 

==Reception==
Flight garnered a lukewarm response from critics and did well at the box office.  Typical of the reviews was the one appearing in  s of the silent era that depicted US involvement in Mexican and Latin American conflicts. Evans 2000, p. 71. 

Largely forgotten today, Flight is representative of Capras early period and fits in well with the silent Submarine (1928 film)|Submarine (1928) and later Dirigible (film)|Dirigible (1931) as a trio of military-themed productions. Now available in home video, the film is rarely broadcast as it is considered a minor work in the Capra filmology. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Capra, Frank. Frank Capra, The Name Above the Title: An Autobiography. New York: The Macmillan Company, 1971. ISBN 0-306-80771-8.
* Dolan Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Harwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Joseph McBride (writer)|McBride, Joseph. Frank Capra: The Catastrophe of Success. New York: Touchstone Books, 1992. ISBN 0-671-79788-3.
* Scherle, Victor and William Levy. The Films of Frank Capra. Secaucus, New Jersey: The Citadel Press, 1977. ISBN 0-8065-0430-7.
 

==External links==
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 