Before the Rain (2010 film)
 
 
{{Infobox film
| name           = Before the Rain
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Nick Clifford Craig Boreham Stephen de Villiers CJ Johnson
| producer       = Sandra Levy Graham Thorburn Christopher Lee
| screenplay     = 
| story          = 
| based on       = 
| starring       = Shai Alexander Jacinta Acevski Ryan Corr Kimberley Hews
| music          = David Barber Paul Kopetko
| cinematography = Joel Froome Adam Howden
| editing        = Adrian Chiarella Amarnath Jones Gwen Sputore
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}

Before the Rain is a 2010 Australian drama film, directed by Craig Boreham, Nick Clifford and CJ Johnson and written by Shirley Barrett.

==Plot==
 

==Cast==
* Jacinta Acevski ... Nicole
* Shai Alexander ... Danny
* Cooper George Amai ... Hugh
* Laurence Brewer ... Harley
* Ryan Corr ... Max
* Kenji Fitzgerald ... Jamie
* Lisa Gormley ... Karin
* Kimberley Hews ... Naomi

==External links==
 

 
 
 