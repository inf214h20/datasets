Mated in the Wilds
 
 
{{Infobox film
| name           = Mated in the Wilds
| image          = 
| image_size     =
| caption        = 
| director       = P.J. Ramster
| producer       = P.J. Ramster
| writer         = 
| based on       = 
| narrator       =
| starring       = 
| music          =
| cinematography = Reg Fuz
| editing        = 
| studio = PJ. Ramster Photoplays
| distributor    = 
| released       = 13 August 1921
| runtime        = 5,000 feet
| country        = Australia English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Mated in the Wilds is a 1921 Australian silent film directed by P.J. Ramster. It is a melodrama about a love triangle among members of Sydney society.

The movie is considered a lost film.

==Plot==
Two men, flying ace Justin Strong (Fred Oppey) and foreigner Montgomery Lyle (Anthony Aroney) are both in love with sportswoman Elsa Hope (Elsa Granger). Justin and Monty leave by motorcycle on a surveying trip and Monty leads his rival in the desert to die. He tells Elsa that Justin was killed by aboriginals. Elsa insists on seeing Justins grave and drives out to the desert with Monty and her mother. Monty ties Mrs Hopes to a tree and is about to abduct Elsa but she pulls a gun on him and eventually finds Justin living with some friendly aborigines. 

==Cast==
*Elsa Granger as Elsa Hope
*Anthony Aroney as Montgomery Lyle
*Fred Oppey as Justin Strong
*Maud de Grange
*William Shepherd
*Lydia Rich
*David Edelsten
*Louis Witts
*Albert Germain
*S.P. Woodford
*Phillip Raftus
*Kathleen Ramster
*Winifred Law
*Dorothy Shepherd
*Nancy Simpson

==Production==
The cast was unpaid and could only work weekends so the film took over a year to complete. Actors were drawn from P.J. Ramsters acting school. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 107. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 

 
 
 
 
 
 


 