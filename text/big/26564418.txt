Lovelorn (film)
{{Infobox film
| name           = Lovelorn
| image          = Gonul Yarasi.jpg
| image size     = 
| caption        = Theatrical poster
| director       = Yavuz Turgul
| producer       = Ömer Vargı Mustafa Oğuz Mine Vargı
| writer         = Yavuz Turgul
| narrator       = 
| starring       = Şener Şen Meltem Cumbul Timuçin Esen
| music          = Tamer Çıray
| cinematography = Soykut Turan
| editing        = 
| distributor    = Warner Bros
| released       =  
| runtime        = 138 mins.
| country        = Turkey
| language       = Turkish
| budget         = 
| gross         = 
| preceded by    = 
| followed by    = 
}}
Lovelorn ( ) is a 2005 Turkish drama film, written and directed by Yavuz Turgul, starring Şener Şen as a teacher recently returned to Istanbul who must protect a nightclub singer from her violent ex-husband. The film, which went on nationwide general release across Turkey on  , won awards at film festivals in Antalya, Palm Springs and New York.

==Plot==
Nazım, a primary school teacher, returns to Istanbul from his posting in the east . He temporarily takes on a job as a taxi driver with the help of his friend Atakan. One night he picks up Dünya, a singer at a club and after striking up a friendship, becomes her driver. While Nazım is waiting for Dünya to finish her performance one night, she is attacked by a man. Dünya is injured, and is rushed to the hospital by Nazım.

Dünya reveals that he is her ex-husband Halil, who wants to take away their daughter Melek. Nazım takes Dünya and Melek into his home but Halil continues to stalk them. Halil reveals to Nazım that he still loves Dünya and wants to remarry her to start over a new life.  After he gives his word, Dünya and Melek leave with them.  Meanwhile, Nazım faces up to his failed obligations to his own children Mehmet and Piraye.

One day, Nazım gets a phone call from Dünya asking for protection from Halil who has gone back to his old ways. Nazım travels to meet Dünya at the otobus terminal, but Halil gets wind of this and confronts them there. Halil asks Dünya to sing a folk song to him for the last time, before he would let her go. Halil is euphoric when he hears Dunya singing passionately, but then realises that it isnt directed at him, but at Nazım. Halil becomes enraged and shoots Dünya. He then turns the gun on himself, after leaving Melek in Nazıms care.

==Cast==
*Şener Şen as Nazım
*Meltem Cumbul as Dünya
*Timuçin Esen as Halil
*Güven Kıraç as Mehmet
*Sümer Tilmaç as Takoz
*Ece Naz Kızıltan as Melek
*Devin Özgür Çınar as Piraye
*Erdal Tosun as Haşmet
*Mübeccel Vardar as Berrin

==Awards==
The film won three awards at the 42nd  .   

==References==
 

==External links==
* 

 
 
 
 
 

 
 