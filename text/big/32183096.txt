Conan O'Brien Can't Stop
{{Infobox film
| name           = Conan OBrien Cant Stop
| image          = Conan OBrien Cant Stop.jpg
| caption        = 
| director       = Rodman Flender
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =   Mike Merritt James Wormworth Jerry Vivino Mark Pender Richie Rosenberg Rachael L. Hollingsworth Fredericka Meek
| music          = 
| cinematography = 
| editing        = 
| studio         = Pariah
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $267,965 (US)   
}}
 scheduling dispute.

==Synopsis==
  convention]]
The film opens with a short segment explaining the events which transpired to culminate in Conans launch of the Legally Prohibited Tour. It focuses on the rift with NBC and the outpouring of support shown by his fans while the fiasco played out.

The documentary continues with Conan explaining that the idea for the tour came to them while thinking of things they could do in the 6 months they were legally prohibited from appearing on television. The following segments show glimpses of the thought process for the tour, and then an extremely nervous Conan and team members as they wait in realtime to see the results of whether or not their show would sell. All the venues begin to sell out at a fast pace and the stage is set for them to proceed with the planning of the tour.
 Jack White, with White participating in the show itself. During the film there is little interviewing between Conan and director Rodman Flender, though the few questions that are captured are answered in great detail. A constant companion during the entire documentary is Conan OBriens personal assistant, with whom he shares humorous (yet occasionally passive aggressive) banter till the close of the film.

The documentary ends with a title card displaying the end date of the tour and that Conan had a few weeks off before starting his next assignment as the host of his cable talk show at TBS (TV channel)|TBS, a shot of Conan walking out onto the studio stage after the Conan theme has been played by the band is shown after the title card. A recorded performance at Jack Whites studio is played while the credits roll.

==Reception==
The documentary was given a limited release, and it was generally well received by film critics. At movie review website Rotten Tomatoes, the film has received an aggregated score of 80% from 70 reviews.    Film critic Roger Ebert gave the documentary three out of four stars stating "To be sure, NBC paid him $40 million in a send-off package, but the Conan OBrien we see in the film wasnt in it for the money. He was in it because he cant stop."  The New York Times movie review by Stephen Holden was critical of the films subject and its direction which left much of the tours performances off camera stating "The film spends too much time with Mr. O’Brien and his team backstage, where he is the needy focus of attention at all times."    Holden did state though, that as a whole, the documentary is "consistently watchable". 

==References==
 
* Conan Obrien as Himself.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 