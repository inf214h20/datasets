Jabardasth (film)
 
 
{{Infobox film
| name           = Jabardasth
| image          = Jabardasth poster.jpg
| alt            =  
| caption        =
| director       = B.V. Nandini Reddy
| producer       = Bellamkonda Suresh
| writer         = Veligonda Srinivas  (Dialogues)  Ramajogayya Sastry, Lakshmi Bhoopal, Sreshta  (Lyrics) 
| screenplay     = B.V. Nandini Reddy
| story          = Maneesh Sharma
| starring       =  
| music          = S. Thaman
| cinematography = Mahesh Muthuswamy Sanjay Loknath
| editing        = Kotagiri Venkateswara Rao
| studio         = Sri Sai Ganesh Productions
| distributor    =7SEAS Inc    (Overseas)   
| released       =  
| runtime        = 150 min
| country        = India
| language       = Telugu
| budget         =  
| gross          =   (Worldwide collection)(Nett)
}}
 Telugu romantic Siddharth and Samantha in the lead roles and Nithya Menen in an important role.    The film released on 22 February 2013 with a U/A Certificate from the Censor Board and was commercially successful at the box office.   The film is an unofficial remake of 2011 Hindi film Band Baaja Baaraat. Yash Raj Films, the producers of the Hindi original, are planning to take legal action against the makers of Jabardasth.  Jabardasth satellite and audio telecast rights were sold for  5.25 crores.  

==Cast== Siddharth as Bairraju Samantha as Shreya
* Srihari as Javed Ibrahim
* Nithya Menen as Saraswati
* Sayaji Shinde as Bihar Yadav
* Thagubothu Ramesh

==Crew==
* Direction : B.V. Nandini Reddy
* Story :  B.V. Nandini Reddy
* Screenplay : Maneesh Sharma
* Music : S. Thaman
* Cinematography : Mahesh Muthuswamy & Sanjay Loknath
* Dialogues: Veligonda Srinivas
* Lyrics : Ramajogayya Sastry, Lakshmi Bhoopal & Sreshta
* Editor : Kotagiri Venkateswara Rao
* Art : A. S. Prakash
* Additional screenplay : Kona Venkat
* Costumes : Roopa Vaitla
* Fights : Ganesh & Satish
* Executive Producer : B Mahendra Babu
* Producer : Bellamkonda Suresh & Bellamkonda Ganesh Babu

==Soundtrack==
{{Infobox album
| Name = Jabardasth
| Longtype = to Jabardasth
| Type = Soundtrack
| Artist = S. Thaman
| Cover =
| Released = 1 February 2013
| Recorded = 2012–2013 Feature film soundtrack
| Length = 21:06 Telugu
| Label = Aditya Music
| Producer =
| Last album = Settai (2013)
| This album = Jabardasth (2013)
| Next album = Shadow (2013 film)|Shadow (2013)
}}

The films audio was launched on 1 February 2013. It was attended by Surender, Samantha, Thaman, Siddharth, Srihari and Sunil with V. V. Vinayak being the chief guest. 

{{Tracklist
| extra_column    = Artist(s)
| total_length    = 21:06
| title1          = Arere Arere
| extra1          = Nithya Menon
| length1         = 05:11
| title2          = Allah Allah
| extra2          = Shreya Ghoshal, Ranjith (singer)|Ranjith, Naveen Madhav
| length2         = 04:25
| title3          = Meghamala
| extra3          = Muralidhar, Rahul, Vandana, Rita, Megha
| length3         = 05:34
| title4          = Tees Maar Khan
| extra4          = Naveen Madhu
| length4         = 03:12
| title5          = Lashkar Pori
| extra5          = Muralidhar
| length5         = 03:30
}}

==Critical reception==

===Reception===
Idlebrain.com jeevi gave a rating of 3/5 for the film.  Rediff wrote:"First half was fun but second half was dragged". 

==References==
 

==External links==
*  

 
 
 
 