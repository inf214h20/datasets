Anwar (2010 film)
{{Infobox film
| name=Anwar
| image = Anwar_malayalamfilm.jpg
| caption = Theatrical release poster
| director = Amal Neerad
| writer = Amal Neerad
| narrator = Mammootty  Prithviraj Mamta Lal
| producer = Raj Zacharias 
| cinematography = Satheesh Kurup
| editing = Vivek Harshan
| music = Gopi Sundar
| Rafeeq Ahmed
| distributor = Red Carpet Movies Limited
| released =  
| runtime =
| country = India
| budget = 
| gross = 
| language = Malayalam
}} Prithviraj in Tamil dubbed Hindi as spy thriller film Traitor (film)|Traitor. 

== Plot ==
The movie is about the journey of a  ), and falls for her. His family knows about this and proposes to her. She accepts. But when they go out for shopping to Coimbatore, Anwar goes out of the cloth shop for refreshing and a sudden blast happens, killing the whole family except him. Meanwhile, Ayesha is arrested & then Anwar goes out to Stalin for her bail. But he refuses and tell it could not be done owing to lack of proof. He then tells Stalin that he will assist in the discovery of the terrorists, which lead to the present part of the story. Both of them have a secret meeting about the bomb blasts planned by Babu Sait & Co. Stalin brings Ayesha so that they could meet after a long time. When Babu Saits Men arrives, Stalin tells Anwar to act as if they were having a fight, and one of the Babu Saits men shoot Stalin, hereby killing, thinking that Anwar was having a fight. But after that, Anwar was given the big responsibility of transporting the deadly terrorists to Mumbai, through a ship containing full of explosives & weapons. Anwar at first made them think that he was on their side, but soon he blasts the ship by firing on it and killing all of the terrorists. Babu Sait becomes furious, that Anwar cheated them. When Anwar says about his parents, their flashbacks, which Babu Sait commits suicide. Anwar then calls the phone of Stalin which is under the police custody. He tells that the mission is over. After that it is narrated that Stalin & his colleague got bravery awards but no one has recognised the near-death battle Anwar has played. The film ends when both Anwar & Ayesha are in a quiet place, living together in a small house. The end-credits rolls with the "Njan" song.

==Cast==
* Prithviraj Sukumaran as Anwar
* Mamta Mohandas as Ayesha
* Prakash Raj as Stalin Manimaran Lal as Babu Sait
* Sampath Raj as Basheer Bhai Saikumar as Anwars father Geetha as Anwars mother
* Nithya Menen as Anwars sister
* Salim Kumar as Ashraf
* Rajeev Pillai as a Terrorist (cameo)
* "Mahanadi" Sankar

== Production ==
 
Shoot for the film started in March, 2010. The main location was Kochi, India|Kochi.     

== Awards == Filmfare Awards
;; won: Best Music Director – Gopi Sundar Best Female Playback Singer – Shreya Ghoshal – "Kizhakk Pookkum" Best Lyricist – Rafeeque Ahmed – "Kizhakku Pookkum"
;; Nominated: Best Actor – Prithviraj Sukumaran Best Supporting Lal

== Soundtrack ==
{{Infobox album
| Name = Anwar
| Type = soundtrack
| Artist = Gopi Sundar
| Cover = Anwar Malayalam OST cover.jpg
| Released =  
| Recorded =
| Genre = World Music
| Label =
| Producer =
}}
The soundtrack of the film, composed by Gopi Sundar, was released on 3 September 2010. The album features seven songs with lyrics by Rafeeq Ahmed. 

In the 2011 Vanitha Film Awards, the song Kizhakku Pookkum won three awards: Best Song, Best Female Playback (Shreya Ghoshal) and Best Lyrics (Rafeeq Ahamed). 

{{Track listing
| extra_column = Performer(s)
| title1 = Kizhakku Pookkum | extra1 = Shreya Ghoshal, Navin Iyer, Sabari Brothers and Raqueeb Alam | length1 = 5:08
| title2 = Njan | extra2 = Prithviraj Sukumaran|Prithviraj, Mamta Mohandas, Reshmi, Priya and Asif Akbar | length2 = 3:44
| title3 = Kanninima Neele | extra3 = Shreya Ghoshal and Naresh Iyer | length3 = 3:53
| title4 = Vijanatheeram | extra4 = Sukhwinder Singh, Blaaze | length4 = 3:46
| title5 = Kavitha Pol | extra5 = Gopi Sunder and A V Uma | length5 = 4:30
| title6 = Kanninima Neele | extra6 = Suchitra Karthik and Gopi Sunder | length6 = 3:33
| title7 = A Hero Will Rise | extra7 = Gopi Sunder, Navin Iyer, Sethu Thankachan and Sree Charan | length7 = 3:30
}}

==Box Office==
The film was declared as a flop by box office india  

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 