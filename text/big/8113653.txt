Yawm Said
{{Infobox film
| name           = Yawm Said
| image          = ModernEgypt, Poster of Yom said, COV 321.jpg
| image size     = 200px
| caption        = Movie poster
| director       = Mohammed Karim
| producer       = Mohammed Karim
| writer         =
| starring       = Faten Hamama Samiha Samih Mohamed Abdel Wahab
| music          =
| cinematography = 1939
| runtime        =
| country        = Egypt
| language       = Arabic
}}
 1939 Egyptian drama film directed by Mohammed Karim and starring Egyptian actor and musician Mohamed Abdel Wahab. This was also the first movie that Faten Hamama, who was only eight years old then, had acted in.

== Plot ==

Abdel Wahab plays the role of a young man who is unlucky to be fired from his job. He meets a beautiful lady and falls in love with her. The ladys parents do not approve of the man. The young man earns a good reputation and becomes famous and a rich lady, secretly having a crush for him, asks him for music lessons, only to get closer to him. The rich lady discovers that he is in love with another woman and tries to ruin their relationship, but fails and the lovers remain together. After what happens, the ladys parents are convinced of the mans loyalty and love so they accept him as her husband.

== Cast ==
*
* Mohamed Abdel Wahab
* Faten Hamama
* Samiha Samih

== References ==
*  

==External links==
*  

 
 
 
 
 
 

 
 