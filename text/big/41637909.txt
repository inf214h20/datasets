Hellcats (film)
{{Infobox film
| name           = Hellcats
| image          = File:Hellcats_poster.jpg
| director       = Kwon Chil-in
| producer       = Kang Woo-suk   Kim Eun-young
| writer         = Kim Hyun-soo   Kim Soo-ah   Park Hye-ryeon Ahn So-hee 
| music          =  
| cinematography = Jo Yong-gyu
| editing        = Kim Sun-min
| distributor    = Cinema Service
| released       =  
| runtime        = 114 min.
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| admission      = 590,409
| film name      = {{Film name
| hangul         = 뜨거운것이 좋아 
| rr             = Ddeugeoun Geosi Joa}}
}} Ahn So-hee) -- who are all engaged in dilemmas regarding love and sex.  

==Plot==
27-year-old Ah-mi is a freelance screenwriter, and shes on her 17th rewrite for a screenplay that has been in the works for over a year. For the past 3 years, shes been living with her older sister Young-mi because she cant afford rent on her own. Ah-mi dreams of success and independence, but these seem far-off. She has a boyfriend named Won-suk, a member of a struggling rock band, who is mostly broke. Then one day, she goes out on a blind date and a new guy, Seung-won, enters her life. Seung-won is a successful accountant, and is very different from Won-suk.

Meanwhile, 40-year-old Young-mi is a happy, independent single mother and interior designer. She begins working with a theatre company where she meets a much younger actor named Kyoung-soo, who takes a romantic interest in her. But even her sexy, confident self gets insecure when on her next doctors visit, she learns that shes undergoing menopause.

There is also Young-mis teenage daughter, Kang-ae. She is a bright, optimistic high school student, whose current goal in life is to figure out a way to get a kiss from her boyfriend of three years, Ho-jae. Kang-aes best friend, Mi-ran, a self-proclaimed dating expert, coaches Kang-ae in matters of love. Mi-ran helps Kang-ae plan a strategy for her first kiss, but their scheme goes haywire, and Kang-aes first kiss ends up being with Mi-ran.

Although different in age, attitude about life, and dating preferences, the three women each learn to find their own unique way to happiness.

==Cast==
* Lee Mi-sook as Kim Young-mi
* Kim Min-hee as Kim Ah-mi Ahn So-hee as Kim Kang-ae Kim Sung-soo as Oh Seung-won Kim Heung-soo as Na Won-suk
* Yoon Hee-seok as Choi Kyoung-soo
* Kang Hae-in as Yoo Mi-ran
* Kim Bum as Lee Ho-jae
* Jang Hang-joon as Director Ahn
* Park Kwang-jung as Gynecologist
* Jung In-gi as Cafe owner 
* Oh Yeon-ah as Won-suks workshop woman 
* Lee Myeong-haeng as Travel agency friend 
* Kim Gyeong-hyeong as Customer 
* Moon Se-yoon as Ah-mis hairstylist
* Jun-seong Kim as Young-mis nail artist
* Lee Eun-sung as Kang-aes cosmetics staff member

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
| rowspan=5| 2008 || rowspan=2| Baeksang Arts Awards 
| Best Actress || Kim Min-hee ||  
|- Kim Heung-soo ||  
|-
| Busan Film Critics Awards || Best Actress || Kim Min-hee ||  
|- Ahn So-hee ||  
|-
| Korean Film Awards || Best Actress || Kim Min-hee ||  
|}

==References==
 

==External links==
*    
*  
*  
*  

 
 
 