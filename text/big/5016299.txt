Honkytonk Man
{{Infobox film
| name           = Honkytonk Man
| image          = Honkytonk man.jpg
| caption        = Promotional movie poster for the film
| director       = Clint Eastwood
| producer       = Clint Eastwood
| writer         = Clancy Carlile
| starring = {{plainlist|
* Clint Eastwood
* Kyle Eastwood
}}
| music          = Steve Dorff
| cinematography = Bruce Surtees
| editing        = Ferris Webster Michael Kelly Joel Cox
| studio         = The Malpaso Company
| distributor    = Warner Bros.
| released       = December 15, 1982
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $2 million 
}}
:For other uses, see Honky Tonk Man (disambiguation).
Honkytonk Man is a 1982 American drama film set in the Great Depression. Clint Eastwood, who produced and directed, stars with his son, Kyle Eastwood. Clancy Carliles screenplay is based on his novel of the same name. This was Marty Robbins last appearance before he died.

==Plot==
Itinerant western singer Red Stovall (Clint Eastwood), suffers from tuberculosis but has  been given an opportunity to make it big at the Grand Ole Opry. He is accompanied by his young nephew Whit (Kyle Eastwood), to Nashville, Tennessee|Nashville, Tennessee. 

After a series of adventures which include the nephews first sexual encounter in a brothel, he and uncle Red finally arrive at Nashville.  A fit of coughing in the middle of his audition at the Grand Ole Opry ruins his chance and his dream.  But talent scouts for a record company are impressed enough to arrange a recording session, realizing that he has only days to live. The tuberculosis reaches a critical stage in the middle of this session, where Reds lines are filled in by a side guitarist. Red finally succumbs while Whit vows to tell the story of his uncle.  Reds vintage Lincoln Model K touring car, prevalent throughout the movie, finally dies at the cemetery where Red is laid to rest.

==Production== Calaveras County, Mountain Ranch; Main Street, Sheepranch, California|Sheepranch; and the Pioneer Hotel in Sheepranch. 
The famous jail break scene was filmed in Dayton, Nevada at the corner of Pike Street (the Lincoln Highway) and W Main Street. The vintage brick building the movie-built jail was attached to is the Odeon Hall, where Marilyn Monroes paddle ball and bar interior scenes were shot in The Misfits (1961). Extras were locally hired and many of the towns residents are seen in the movie.

==Reception==
Honkytonk Man received critical acclaim, and has a score of 93% on Rotten Tomatoes.  The New York Post wrote, "The pace is slow, very country, but it rises to touching moments...not all perfect by any means, but ultimately a story of occasional awkward truths." Hughes, p.138  The film grossed $4.5 million at the United States box office. Hughes, p.137  The film was nominated for a Razzie Awards for Worst Original Song for No Sweeter Cheater than You. 

== References ==
 

==Bibliography==
* 
* 

== External links ==
*  
*  

 

 
 
 
 
 
 