The Perfect Weapon
 
{{Infobox film
| name = The Perfect Weapon
| image = Perfect weapon poster.jpg
| director = Mark DiSalle Ralph Winter
| writer = David C. Wilson Mako Mariska Hargitay Charles Kalani, Jr.
| music = Gary Chang
| cinematography = Russell Carpenter 
| editing = Andrew Cohen Wayne Wahrman
| distributor = Paramount Pictures
| released = March 15, 1991
| runtime = 87 minutes English
| budget = $10,000,000 
| gross = $14,061,361
}}

The Perfect Weapon is a 1991 action-martial arts film starring acting newcomer Jeff Speakman.  

Set in Los Angeles, the film relates the story of a young man, Jeff, who is trained in the martial art of Kenpō, and his fight against the Korean Mafia families.

Speakman was a student of and was advised closely by Ed Parker in the making of this film.   

The films taglines included "No gun.  No knife.  No equal." and "Just try him." and is the only well-known Hollywood depiction of Kenpo techniques on-screen. 
 The Power rap group Snap! is featured extensively in the movies soundtrack. 

==Plot==
Jeff Sanders (Jeff Speakman) leads a double life: by day, he is a simple, unassuming construction worker, and by night, an expert Kenpo student and master of his craft.

When Jeff lost his mother as a teenager, he became an outcast and frequently lashed out at his family and society in an attempt to assuage his anger. His father, Captain Sanders (Beau Starr), gained the idea from a mutual friend, Kim (Mako (actor)|Mako), to enroll Jeff in a Kenpō school to better manage his rage and feelings. But one day, he got into a fight with a bully who beat up his younger brother, and severely injured him with his martial arts. Displeased with his lack of self-control, Jeffs father forced him to move out of home. Jeff, now estranged from his family and living alone, continued with his courses in Kenpo and eventually gained Kim as a mentor and father figure.
 Korean mafia families, due to his refusal to pay them off and use his antique store to peddle drugs. Jeff helps out Kim by beating up the people who attacked his store, but only ends up doing more harm than good to Kims reputation with the mafia, and this ultimately ends up having Kim murdered by an anonymous hit-man, Tanaka (Professor Toru Tanaka).

Jeff vows to avenge Kims death, and uses all his resources and fighting skills to go against the mafia and find out who ordered Kims murder. He remembers a boy named Jimmy (Dante Basco) who lived with Kim, and tries to find him to ask if he knows about the murder. However, Jeffs estranged younger brother Adam (John Dye), now also a cop, is investigating the case, and warns Jeff against trying to settle matters in his own hands. In his hunt to avenge Kims murder, Jeff is approached by a mafia boss named Yung (James Hong) who claims to be Kims friend and knew of a possible lead to Kims killer. Jeff is directed to Sam, one of the mafia bosses in Korea town, who was believed to be the one who ordered Kims death. However, upon breaking into Sams residence and attempting to kill him, Jimmy appears to reveal that Sam was Kims friend and was the one who took him in for protection. Jimmy also explains that Yung is the one responsible for Kims death, and he was merely attempting to use Jeff as a weapon to kill his rival boss Sam. 

Jeff now plans to kill Yung, but Jimmy warns him that Yung is always protected by his hit-man Tanaka. In order to eliminate Tanaka, Jeff asks Jimmy to falsely testify (to Adam) that he witnessed Tanaka murdering Kim. The plan is have Adam arrest Tanaka so that Jeff can get Yung alone to kill him. Adam and the police eventually capture Tanaka after a long car chase, but to Jeffs dismay Yung was not in the car with him. Tanaka later manages to escape from the police by severely injuring Adam and breaking out of the police car. 

Jimmy overhears that Yung plans to escape the country by boat, and tells Jeff about Yungs drug factory. Now further fueled for vengeance, Jeff sets out to attack Yungs drug factory, using his martial art skills and various weapons to defeat guards and hitmen protecting Yung. He eventually subdues Yung, but is attacked by Tanaka. Although Tanaka gains the upper hand during their fight, Jeff manages to kill Tanaka by setting fire to a gas tank he was standing next to. Despite initially wanting to kill Yung, in the end Jeff decides to capture him alive (showing he has learned self-control) and turns Yung in to his father, Captain Sanders.

The film ends with Jeff entering the kenpo dojo to visit his former master.

==Cast==
* Jeff Speakman as Jeff Sanders 
* John Dye as Adam Mako as Kim
* James Hong as Yung
* Mariska Hargitay as Jennifer
* Dante Basco as Jimmy Ho
* Professor Toru Tanaka as Tanaka
* Seth Sakai as Master Lo
* Beau Starr as Capt. Carl Sanders
* Clyde Kusatsu as Detective Wong
* Cary-Hiroyuki Tagawa as Kai

==Box office and reception==
The Perfect Weapon debuted at the box office at number six with a three-day box-office take of $3.9 million and had a total domestic box office gross of $14,061,361. 

The film was met with mixed to negative reviews,    with a 33% approval rating on Rotten Tomatoes, based on nine reviews. 

==Home media==
The Perfect Weapon was released on February 14, 2012 on DVD and Blu-ray Disc.

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 