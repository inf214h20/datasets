A Tremendously Rich Man
{{Infobox film
| name           =  A Tremendously Rich Man
| image          = 
| image_size     = 
| caption        = 
| director       = Steve Sekely    
| producer       = Joe Pasternak
| writer         = Eugen Szatmari   Ernst Wolff
| narrator       = 
| starring       = Catherine Hessling   Alexander Murski   Amy Wells   Berthold Bartosch
| music          = Max Kolpé   Theo Mackeben
| editing        = Andrew Marton
| cinematography = Reimar Kuntze
| studio         = Deutsche Universal   Tobis Film 
| distributor    = Deutsche Universal
| released       = 13 February 1932
| runtime        = 73 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy film directed by Steve Sekely and starring Curt Bois, Dolly Haas and Adele Sandrock. It premiered on 13 February 1932.  The film was a co-production (filmmaking)|co-production between the German subsidiary of Universal Pictures and the German firm Tobis Film.

==Cast==
*  Curt Bois as Curt 
* Dolly Haas as Dolly 
* Adele Sandrock as Adele 
* Liselotte Schaak as Ulla 
* Egon Brosig as Fürst 
* Fritz Ley as Notar 
* Paul Hörbiger as Linkerton 
* Willi Schur as Emil 
* Paul Biensfeldt as Ferdinand 
* Margarete Kupfer as Bella da Vasco 
* Friedrich Ettel as Arzt 
* Annie Ann   
* Eduard Rothauser   
* Josef Dahmen   
* Peter Ihle   
* Hermann Picha   
* Hermann Pittschau   
* Walter Steinbeck   
* Michael von Newlinsky 

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 