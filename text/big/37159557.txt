Gulf Stream Under the Iceberg
 
{{Infobox film
| name           = Gulf Stream Under the Iceberg
| image          = 
| caption        = 
| director       = Jevgeņijs Paškevičs
| producer       = 
| writer         = Jevgeņijs Paškevičs
| based on       =  
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =   
| runtime        = 125 minutes
| country        = Latvia Russia
| language       = Russian
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Olga Shepitskaya
* Rēzija Kalniņa
* Ģirts Ķesteris
* Jānis Reinis
* Uldis Dumpis
* Pēteris Liepiņš
* Vigo Roga
* Regnars Vaivars Aleksey Serebryakov
* Ksenia Rappoport
* Danila Kozlovskiy
* Ville Haapasalo
* Yuriy Tsurilo
* Ekaterina Vilkova
* Anna Azarova Elena Morozova
* Tatyana Lyutaeva

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Latvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 