Silent Night, Deadly Night
:Not to be confused with the 1974 horror film Silent Night, Bloody Night.
{{Infobox film
| name = Silent Night, Deadly Night
| image = Silentnightdeadlynight.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Charles Sellier
| producer = Ira Richard Barmak Scott J. Schneid Dennis Whitehead
| screenplay = Michael Hickey
| story = Paul Caimi
| starring = {{plainlist|
* Lilyan Chauvin
* Gilmer McCormick
* Toni Nero
* Robert Brian Wilson
}} Perry Botkin
| cinematography = Henning Schellerup
| editing = Michael Spence
| studio = Slayride
| distributor = TriStar Pictures
| released =  
| runtime = 79 minutes 85 minutes  
| country = United States
| language = English
| budget = $750,000
| gross = $2,491,460
}} slasher film Charles E. Sellier, Jr. and starring Robert Brian Wilson, Lilyan Chauvin, Gilmer McCormick, Toni Nero, Linnea Quigley, Britt Leach and Leo Geter.

The film focuses on a young boy who, after witnessing his parents brutal murder at the hands of a man clad in a Santa suit on Christmas, grows up tumultuously in a Catholic orphanage and slowly emerges into a spree killer himself. The film caused an uproar when released in 1984 during the holiday season, and has developed a cult following.

==Plot== rapes and murders their Mother Superior, a strict disciplinarian, who persistently strikes children who are misbehaving, considering punishment for their wicked actions as a good thing. Sister Margaret seems to be the only one who sympathises with the children, trying to get Billy to open up and play with the children, but they are constantly under Superiors scrutinizing eye and they regularly end up getting punished.

Ten years later, Billy leaves the orphanage in hopes of finding a normal life. He gets a job as a stock boy at a local toy store. He has a crush on his coworker, Pamela, but his thoughts of them having sex are often interrupted by the visions of the criminal imposter in a Santa Claus outfit who ends up slashing him in the torso with the same switch blade that ends up slashing his mothers throat thirteen years ago. Steadily growing unstable, Billy is volunteered by force to dress up as Santa Claus for the store on Christmas Eve. When he happens upon Pamela being raped by another coworker named Andy, Billy snaps and hangs him with a string of Christmas lights. He then "punishes" Pamela by killing her with a box cutter. His boss, Mr. Sims, comes in to check on them and is killed by a hammer to the skull.  Mrs. Randall comes in and discovers the bodies, but she is killed by an arrow when she attempts to flee. Sister Margaret discovers the carnage and runs runs back to the orphanage screaming to search for help on the telephone. Meanwhile, Billy breaks into a house and kills two teenagers having sex by impaling Denise on a set of deer antlers, and throwing Tommy through a window. A little girl interrupts him and he asks her if shes been naughty or nice. When she says shes been good, he smiles and warmly gives her the same box cutter that hed used to kill Pamela with earlier. Witnessing the bullies picking on two teenage boys on a nearby hill while they are sledding, Billy decapitates the bullys head and his body with his head were discovered by the other bully who runs away screaming. Meanwhile, the authorities are still investigating with the aid of Sister Margaret, who ends up assuming that Billy is the killer and postulates that he is making his way back to the orphanage.

The next morning, a man dressed in a Santa Claus outfit approaches the orphanage. Officer Barnes warns the Santa to stop, but he does not respond and he is forced to shoot him. He discovers that this Santa was Father OBrien, a deaf pastor on his way to the orphanage. Billy then suddenly appears and axes Barnes in the chest, killing him. Later, Billy arrives at the orphanage and is still allowed to come in, the children were still, believing him to be Santa. The now-wheelchair bounding Mother Superior tells Billy that "Theres no such thing as Santa Claus" and, furiously, Billy raises the axe to kill her.  Mother Superior closes her eyes, but at that moment, Captain Richards bursts through the door and mortally shoots Billy in the back. Billy collapses and looks at the children, saying "Youre safe now, Santa Claus is gone" before falling to the floor and dying. As the children gather around on the scene; Ricky, who still lives at the orphanage stares up at Mother Superior with a cold stare before uttering "Naughty."

==Cast==
* Robert Brian Wilson as Billy Chapman (age 18)
* Danny Wagner as 8-year-old Billy
*Jonathan Best as 5-year-old Billy
* Lilyan Chauvin as Mother Superior
* Gilmer McCormick as Sister Margaret
* Toni Nero as Pamela
* Britt Leach as Mr. Sims
* Nancy Borgenicht as Mrs. Randall
* H.E.D. Redford as Captain Richards
* Linnea Quigley as Denise
* Leo Geter as Tommy
* Randy Stumpf as Andy
* Will Hare as Grandpa Chapman
* Tara Buckman as Ellie Chapman
* Jeff Hansen as Jim Chapman
* Charles Dierkop as the criminal imposter in a Santa Clause outfit
* Eric Hart as Mr. Levitt
* A. Madeline Smith as Sister Ellen
* Amy Stuyvesant as Cindy
* Max Robinson as Officer Barnes

==Release==
The film was released theatrically in the United States by TriStar Pictures on November 9, 1984.   On its opening weekend, the film outgrossed Wes Cravens landmark slasher A Nightmare on Elm Street, which opened the same day. Before being pulled from theaters, it grossed $2,491,460 at the box office, still making the film a success against its $750,000 budget. 

===Home media===
The film was released three times on DVD in the United States by Anchor Bay Entertainment. The first release was a double feature disc alongside sequel Silent Night, Deadly Night Part 2 in 2003.   The second release was in 2007.   

The first two region 1 releases are currently out of print.

The film was released on DVD in the United Kingdom in 2009 by Arrow Video; this set includes an audio interview with director Charles E. Sellier Jr., poster, booklet including "Deadly Director: Charles Sellier Interviewed by Calum Waddell" and "Silent Night, Sex Night: The Slice and Times of Linnea Quigley".  
 Black Christmas. 
 Part 2 as a two-disc "Christmas Survival Double Feature", containing the same archival bonus features as the 2003 release. 

On September 16, 2014, the film was released on Blu-ray by Anchor Bay/Starz Entertainment as a 30th Anniversary Edition. No new special features were included, with the exception of a few new commentaries, none of which any of the actors participated in. The Blu-ray contains exactly the same release as previous DVD editions with the extended scenes edited back into the film with noticeable picture quality changes. There has yet to be a release of the full, uncut print from a single source.

==Controversy and reception== PTA fought earlier film with a similar premise had gone unnoticed.

Upon its original release in 1984, the film received a negative reception.   as a child molester?" Large crowds (mostly angry families) formed at theaters and malls around the nation to protest the film.  TriStar Pictures, its original distributor, pulled all ads for the film six days after its release (November 15). The film itself was also withdrawn shortly thereafter, due to the controversy.    

The film was later re-released by an independent distributor,  Aquarius Films, in May of 1985, with an ad campaign replacing the original "Twas the night before Christmas"-theme with a new one that centered on the controversy surrounding the film and edited out all close-up shots of Billy, in the Santa suit, with weapons. The print ad material also replaced the original Chimney picture with one that talked about the controversy. 

In the United Kingdom, the movie was never submitted for certification to the British Board of Film Classification|BBFC, and its sequel was denied a video certificate in 1987 after the distributors refused to make the cuts required for an 18 certificate. However, in 2009, Arrow Films submitted the film to the BBFC for classification, who passed the film uncut with an 18 certificate.  The UK DVD was released on November 23, 2009.

==Sequels==
Silent Night, Deadly Night spawned four sequels.
* Silent Night, Deadly Night Part 2 (1987)
*   (1989)
*   (1990)
*   (1991)

==Remake== Silent Night Lisa Marie, Mike OBrien, Cortney Palm, John B. Lowe, Curtis Moore and stuntman Rick Skene as Ronald Jones Jr., The Killer Santa. 

==Re-release==
In November 2013, it was announced that Fangoria in association with Brainstorm Media and Screenvision would be re-releasing the film to theaters in the United States throughout December 2013. 

==References==
  Psychotronic Video Guide (1996) - Michael J. Weldon

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 