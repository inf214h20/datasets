Char Dervesh
{{Infobox film
| name           = Char Dervesh
| image          = Char Dervesh.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Wadia Brothers Production
| writer         = C. L. Cavish 
| screenplay     = JBH Wadia
| story          = C. L. Cavish 
| based on       = 
| narrator       =  Naaz
| music          = G. S. Kohli
| cinematography = Agha Hasham
| editing        = Shaikh Ismail
| studio         = Basant Studios
| distributor    = 
| released       = 1964
| runtime        = 155 min
| country        = India Hindi
| budget         = 
| gross          = 
}} 1964 Hindi/Urdu action fantasy film directed by Homi Wadia for Basant Pictures.  The film was produced by Wadia Brothers and its music composer was G. S. Kohli.  Feroz Khan acted in several "small-budge" costume films like Homi Wadias Char Dervesh as hero, before he became popular as second lead and later as hero, producer and director in mainstream cinema.    The film starred Feroz Khan, Sayeeda Khan, Naaz, B. M. Vyas, Mukri and Sunder (actor)|Sunder. 

The fantasy film revolves around Qamar who is in love with the princess Nargis Banu and his adventures that follow in an attempt to rescue the princess sister Hamida.

==Plot==
Three Derveshes are praying at a shrine, each has a wish to fulfil but that can’t happen till a fourth one arrives. A white horse appears with a rider, and it is the fourth Dervesh who is seeking to reedeem himself. His name is Qamar (Feroz Khan). Qamar has been a care-free person getting into trouble for his innocent misdeeds and basically a source of worry for his two covetous brothers. He sees the beautiful princess Nargis Bano (Sayeeda Khan) and falls in love with her. However he’s caught by the palace guards and whipped and sent to exile. His brothers throw him off the ship they are travelling in and he lands in an underwater sea kingdom. He sees a Woman turned to stone up to her neck. There is another princess, Hamida, imprisoned there by an evil magician. The stone woman turns out to be the mother of the two princesses and she wants Qamar to marry the imprisoned princess. This makes the magician angry and Qamar’s skin is turned black and he meets the other three derveshes. The problem with the skin gets better. He’s now in a dilemma as he has to decide which princess to marry. Several action and chase scenes follow with sword fighting and flying on magic carpets to rescue the princess and finally marry the one he loves.

==Cast==
* Feroz Khan
* Sayeeda Khan
* Naaz
* Mukri
* B. M. Vyas
* Amarnath
* Ratnamala
* Jeevankala Sunder
* W. M. Khan
* Paul Sharma

==Production==
Babubhai Mistry was once again the art and special effects director for the Wadia film. He had made a name for himself as a special effects artist in mythology and fantasy films.    Char Dervesh had plenty of special effects in the form of underwater sea kingdom, two-headed monsters and flying carpets. The film was a ‘classic B-movie’ with many fight and ‘stunt’ scenes.   

==Music==
The music was composed by G. S. Kohli and the lyricists were Anjaan (lyricist)|Anjaan, Saba Fazli, Raja Mehdi Ali Khan. The playback singing was given by Mohammed Rafi, Asha Bhosle, Usha Mangeshkar, and Jani Babu Qawwal. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|- 
| 1
| Pyar Ke Daman Se Lipte Ham Kahan Tak Aa Gaye 
| Mohammed Rafi Anjaan
|-
| 2
| Gusse Me Tum Aur Bhi Achhi Lagti Ho 
| Mohammed Rafi
| Anjaan
|-
| 3
| Tere Karam Ki Dhoom Jahan Mein Kahan Nahi 
| Mohammed Rafi,Jaani Babu Qawwal
|
|-
| 4
| Tadpa Le Jitna Chahe 
| Asha Bhosle, Usha Mangeshkar                 
| Raja Mehdi Ali Khan
|-
| 5
| Kali Kali Ankhon Mein Chamak Gayi Bijli 
| Asha Bhosle
| Anjaan
|-
| 6
| Tu Hai Ek Shama To 
| 
| Saba Fazli 
|-
| 7
| Le Liya Dil Mera Le Liya 
| Asha Bhosle
| Raja Mehdi Ali Khan
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 