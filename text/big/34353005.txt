Carnival Capers
{{Infobox Hollywood cartoon|
| cartoon_name = Carnival Capers
| series = Oswald the Lucky Rabbit
| image = OswaldCarnivalCapers.jpg
| caption = Screenshot Bill Nolan
| story_artist = Walter Lantz Bill Nolan
| animator = Ray Abrams Fred Avery Bill Weber Jack Carr Charles Hastings
| voice_actor =
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = October 10, 1932
| color_process = Black and white
| runtime = 7:14 English
| preceded_by = The Busy Barber Wild and Woolly
}}

Carnival Capers is a 1932 animated short film featuring Oswald the Lucky Rabbit. It is the 65th Oswald cartoon by Walter Lantz Productions and the 117th in the entire series.

==Plot==
Oswald and the girl beagle (making her debut in this film) are at the fair, dancing on a wooden platform. All of a sudden, a large oppressive pit bull pulls it right under their feet, much to their surprise. The pit bull pretends to apologize in giving Oswald a handshake but snaps a rat trap at the rabbits hand instead. When the large dog flirts with the girl beagle, the annoyed Oswald quickly takes her and walks out of the scene.

Oswald and his date go to a refreshment stand to order ice cream sodas. Upon receiving their beverages, the pit bull shows up again, takes Oswalds drink, and consumes it. The girl beagle doesnt drink hers but instead pours the stuff in the pit bulls hat without the latter noticing. As the big dog puts on the hat and gets covered in a creamy mess, the two little tourists immediately move to another location.

After spending a few moments at the punch pad game, Oswald and the girl beagle decide to have a ride. They then select a bizarre one operated by a marsupial. The girl beagle was first to go as the marsupial launches her high in the air where she lands and slides onto a series of animals. In the end, however, she was nabbed by the pit bull. Oswald comes to her assistance by picking up a bow and shooting arrows at the pit bulls back. In this, the large dog drops the girl beagle and sets sights on the rabbit.

Running from the pit bull, Oswald climbs and crawls through a small hole in the fence. The large dog attempts to get through too, only to get stuck half-way. The rabbit then draws a picture of a rat on the pit bulls rear, attracting a stray cat and then a hound. Both of which went in a mauling fashion. Eventually, the pit bull was able to get out of the situation and continues pursuing Oswald.

Oswald, still on the run, enters a gate to the backyard of a house. Instead of going in also, the pit bull peeks through one of the windows. While his pursuer is still looking in, Oswald appears in front of the house from a distance and throws a stick at the other window, breaking the glass. The disturbed homeowner then opens the door in the blink of an eye, therefore smashing the pit bull between it and the front wall. Oswald comes to and writes a "welcome" message on the flattened dog before placing the latter in front of the door. The girl beagle then shows up in the vicinity, happily calling Oswald. She and the rabbit go to celebrate things with a kiss.

==Availability==
The short is available on   DVD box set. {{cite web
|url=http://lantz.goldenagecartoons.com/1932.html
|title=The Walter Lantz Cartune Encyclopedia: 1932
|accessdate=2012-01-11
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 