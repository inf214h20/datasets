Tora-san's Bluebird Fantasy
{{Infobox film
| name = Tora-sans Bluebird Fantasy
| image = Tora-sans Bluebird Fantasy.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = Kiyoshi Shimizu Shigehiro Nakagawa
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Etsuko Shihomi
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 102 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Tora-san, Bluebird of Happiness  is a 1986 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Etsuko Shihomi as his love interest or "Madonna".  Tora-sans Bluebird Fantasy is the thirty-seventh entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
During his travels, Tora-san comes across a traditional theater he used to visit, and discovers that one of his old friends has died. Tora-san and his family help the friends daughter, who becomes romantically involved with an aspiring artist.      

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Etsuko Shihomi as Miho Shimazaki
* Tsuyoshi Nagabuchi as Kengo Kurata
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama
* Jun Miho as Akemi
* Issei Ogata

==Critical appraisal==
Stuart Galbraith IV judges this to be one of the weaker entries in the Otoko wa Tsurai yo series, but still recommended. Perhaps the most notable aspect of the film is that the co-star, Etsuko Shihomi, was the star of the Sister Street Fighter series. It was on the set of Tora-sans Bluebird Fantasy that she met Tsuyoshi Nagabuchi, whom she married the following year.  The German-language site molodezhnaja gives Tora-sans Bluebird Fantasy three and a half out of five stars.   

==Availability==
Tora-sans Bluebird Fantasy was released theatrically on December 20, 1986.  In Japan, the film has been released on videotape in 1988 and 1996, and in DVD format in 2005 and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 