Frk. Møllers jubilæum
{{Infobox film
| name           = Frøken Møllers jubilæum
| image          = FrøkenMøllersjubilæum.jpg
| image_size     =
| caption        = Poster
| director       = Lau Lauritzen Jr. Alice OFredericks 
| producer       = Henning Karmark
| writer         = Lau Lauritzen Jr. Alice OFredericks 
| narrator       = 
| starring       = Liva Weel
| music          = Victor Cornelius Karen Jønsson  Karl Andersson
| editing        = Marie Ejlersen    
| distributor    = ASA Film
| released       = 30 August 1937
| runtime        = 95 minutes
| country        = Denmark
| language       = Danish language
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1937 Denmark|Danish comedy film  directed and written by Lau Lauritzen Jr. and Alice OFredericks. The film stars Liva Weel and Victor Borge .

==Cast==
*Liva Weel ...  Frk. Møller 
*Victor Borge ...  Klaverstemmer Asmussen ... credited as Victor Borge|Børge Rosenbaum
*Karen Jønsson ...  Grete Holm 
*Lau Lauritzen Jr. ...  Ingeniør Peter Juhl 
*Poul Reichhardt ...  Peters ven 
*Per Gundmann ...  Peters ven 
*Jon Iversen...  Direktør Smith 
*Thorkil Lauritzen ...  Sekretær 
*Paul Rohde ...  Fuldmægtig Petersen 
*Holger Strøm ...  Hotelrotte 
*Olaf Ussing ...  Hotelrotte 
*Alex Suhr ...  Konduktør 
*Erik Olsson ...  Hotelkarl

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 