The Heartbreak Kid (1972 film)
 

{{Infobox film
| name = The Heartbreak Kid
| image = The Heartbreak Kid (1972 film).jpg
| border = yes
| caption = Theatrical release poster
| director = Elaine May
| producer = Edgar J. Scherick
| writer = Neil Simon Bruce Jay Friedman (story)
| starring = Charles Grodin Cybill Shepherd Jeannie Berlin Eddie Albert Audra Lindley
| cinematography = Owen Roizman
| studio = Palomar Pictures International
| distributor = 20th Century Fox
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| gross = $5,600,000  (rentals)  
}}

The Heartbreak Kid is a 1972 dark romantic comedy film directed by Elaine May, written by Neil Simon, and starring Charles Grodin, Jeannie Berlin, Eddie Albert, Audra Lindley, Doris Roberts and Cybill Shepherd. It is based on the short story "A Change of Plan", written by Bruce Jay Friedman.
 Best Supporting Best Supporting Actor.

It is #91 on AFIs 100 Years... 100 Laughs, a list of the funniest American movies ever made.
 The Heartbreak Kid starring Ben Stiller and Malin Åkerman.
==Plot and theme==
A black comedy examination of love and hypocrisy loosely based on Theodore Dreisers classic novel, An American Tragedy (and thereby recalling an earlier, well-regarded film of Dreisers novel, A Place in the Sun), the satire begins with the New York City traditional Jewish marriage of emotionally shallow, self-absorbed, "nebbish"-man-boy, Lenny Cantrow, who is a sporting goods salesman (Charles Grodin). While on honeymoon at the Doral Hotel on Miami Beach, he meets and pursues a tall, blonde, Midwestern, seductively bitchy, but sarcastically witty and gorgeous, student named Kelly Corcoran (Cybill Shepherd).  His unsophisticated and emotionally needy bride, Lila (Jeannie Berlin, daughter of director, Elaine May), refuses to use sunscreen and consequently develops a severe sunburn, which quarantines her in their hotel room. Lenny begins a rendezvous with Kelly, lying to his wife as to his whereabouts.  Lenny recklessly and impulsively decides to dump Lila, ending his ephemeral marriage, in order to pursue unloving Kelly, his false ideal, and ultimate fantasy shiksa-goddess. (The girl he was "waiting for all of his life".  He just "timed it wrong".)  She is attending college in Minnesota, where her somewhat bigoted, suspicious and overprotective, hostile father (Eddie Albert) is a relentless obstacle.

==Cast==
*Charles Grodin as Lenny Cantrow
*Cybill Shepherd as Kelly Corcoran
*Jeannie Berlin as Lila Kolodny
*Audra Lindley as Mrs. Corcoran
*Eddie Albert as Mr. Corcoran
*Doris Roberts as Mrs. Cantrow
*Martin Sherman as Mr. Johnson

==Reception==
The film has received almost universal praise from critics, with a 90% rating on Rotten Tomatoes.  Notably, The New York Times declared it to be "a first-class American comedy, as startling in its way as was The Graduate." 

==Awards and honors==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #91
Academy Awards
* 1972: nominated for Academy Award for Best Supporting Actress (Jeannie Berlin) and Academy Award for Best Supporting Actor (Eddie Albert)
Golden Globe Awards
* 1972: nominated for Golden Globe Award for Best Actor - Motion Picture Musical or Comedy (Charles Grodin), Golden Globe Award for Best Supporting Actress - Motion Picture (Jeannie Berlin), and Golden Globe Award for Best Screenplay (Neil Simon)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 