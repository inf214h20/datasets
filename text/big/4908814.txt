Evil Under the Sun (1982 film)
 
 
{{Infobox film
| name           = Evil Under the Sun
| image          = EvilSun01.jpg
| caption        = Original film poster
| director       = Guy Hamilton
| producer       = John Brabourne   Richard B. Goodwin Anthony Shaffer Barry Sandler Agatha Christie (novel)
| starring       = {{plainlist|
* Peter Ustinov
* Jane Birkin
* Colin Blakely
* Nicholas Clay
* James Mason
* Roddy McDowall
* Sylvia Miles
* Denis Quilley
* Diana Rigg
* Maggie Smith
}}
| music          = Cole Porter
| cinematography = Christopher Challis
| editing        = Richard Marden
| studio = EMI Films Titan Productions Mersham Productions Warner Universal Pictures
| released       =  
| runtime        = 117 minutes
| country        = United Kingdom
| language       = English
| gross          =
}}
 novel of the same name by Agatha Christie.   

==Plot==
In the prologue, a hiker reports to local police that he has found dead woman on the Yorkshire moors. The police identify the victim, who has been strangled, as Alice Ruber.

Shortly afterwards, Belgian detective Hercule Poirot (Peter Ustinov) is asked to examine a diamond belonging to Sir Horace Blatt (Colin Blakely), a millionaire industrialist. Poirot declares the diamond a fake. Blatt paid United States dollar|US$100,000 in New York as a gift to his mistress, now on holiday at an exclusive island resort. Poirot agrees to confront her, and enjoy a holiday of his own, at the former summer palace of the reigning King of Tyrania, now owned by Daphne Castle (Maggie Smith), who had received the palace "for services rendered."

Arlena emotionally abuses her stepdaughter, Linda, and flirts with Patrick. Everyone feels sorry for his meek wife, Christine. Patrick is there only because Arlena arranged it. Ken turns to his old friend, Daphne, who in turn reviles the way that Arlena treats both Ken and Linda. Arlena has caused the Gardeners financial problems by walking out of a major play, and refusing another. Brewster has already spent the royalties advanced to him for a tell-all biography of Arlena.

==Act of Murder==
Early one morning, Arlena takes a paddle-boat to Ladder Bay. Patrick and Myra go for a boat trip around the island and see a body laying motionless on the beach. Patrick approaches the body and recognizes Arlena, announcing that she has been strangled. There are nine suspects who may have had a motive to murder Arlena.

==Alibis==
Kenneth was in his room (heard typing by Daphne). Christine was with Linda at Gull Cove and did not leave until 11:55 for a 12:30 tennis match.  Sir Horace argued with Arlena about the diamond he gave her at Ladder Bay at 11:30 (confirmed by his yacht crew, and Daphne who saw the argument from the clifftop). Arlena kept the diamond, promising an explanation that evening. Poirot finds the jewel nearby in a grotto. Patrick left at 11:30 with Myra, seeing the Blatt yacht coming, and hearing the noon cannon.

Rex Brewster, on pedalo, met Linda entering Gull Cove at 12:00. He reports a bottle flung from the top of a cliff nearly hit him. Odell was seen reading by Daphne and her staff. He claims low water pressure hindered his 12:15 wash before tennis, but nobody admits to bathing at such a time.

==Solution==
Assembling the suspects together, Poirot accuses Christine and Patrick of the crime: Christine knocked out Arlena and hid her in the nearby grotto. Patrick strangled the helpless Arlena later. Christine posed as Arlena with self-tanning lotion, Arlenas swimsuit and large red hat, to be mis-identified by Patrick. But in the grotto, Poirot smelled Arlenas perfume. 

Christine set Lindas watch twenty minutes fast, suggested a swim cap to muffle the noon cannon, and corrected the watch afterward. She tossed out the lotion bottle and bathed off her tan, thus depriving the leaky hotel water system of pressure.

==Truth==
Poirot has no proof until he sets a trap for Patrick. He suspects Patrick switched Blatts jewel with a paste one, which Arlena returned to Blatt. Patrick and Christine killed her to protect the theft.

On leaving the hotel, Patrick pays by cheque, signing the "R" in "Redfern" exactly as he did when he wrote the name "Felix Ruber", husband of the prologues Yorkshire moor victim, on an insurance form, claiming he was observed smoking in a non-smoking train compartment at the time of the Yorkshire murder. The hiker that found the body had been Christine, establishing his alibi. Patrick had earlier heard an opera aria by Giuseppe Verdi, and translated the composers name from Italian to "Joe Green," triggering Poirot to remember that "Felix Ruber" is Latin for "Red Fern." Poirot knows photos from the British police will show Patrick to be grieving husband, Felix.

Lastly, Patrick puts a pipe in his mouth that has never been lit during his stay. When Poirot empties the pipe bowl to reveal the diamond, Patrick punches him. Poirot, recovering, learns he will be decorated by the King of Tyrania.

==Production==
  Anthony Shaffer (who had worked on previous Christie adaptations) and an uncredited Barry Sandler. The adaptation stayed fairly close to Christies work but truncated scenes for time constraints, removed minor characters, and added certain humorous elements that were not present in the novel. Additionally, the novel is set in Devon, but the film is set on an Adriatic island in the fictional kingdom of "Tyrania" (based on Albania). The characters of Rosamund Darnley and Mrs. Castle are merged, the characters of Major Barry and Reverend Stephen Lane are omitted, and the female character of Emily Brewster is now a man named Rex Brewster, played by Roddy McDowall.
 Anthony Powell, Death on the Nile. Ustinov designed his own bathing suit used in one scene of the film. 
 Death on Murder on the Orient Express, with Albert Finney in the starring role. Guy Hamilton had previously directed another Agatha Christie story, The Mirror Crackd, in 1980.

==Filming locations==
  on location in Majorca, Spain. 
The actual island used is Sa Dragonera, (an uninhabited islet with Natural Park status, located just off the west coast of Majorca near Sant Elm), but only for the aerial shots. Other locations used were Cala Blanca
( ) as Ladder Bay, and offshore at Sant Elm ( ) for the South of France, (Sir Horaces boat scenes). Sa Dragonera can be seen in the background from the boat. Cala den Monjo ( ) was used for Daphnes Cove and Hotel; the scenes in which the hotel can be seen as being beside the sea. (The hotel was a private estate owned by a wealthy German, but was bought by the Island Council of Mallorca,   to create a Natural Park, and was demolished to its foundations, most of which can still be seen.) Gull Cove is the remote Cala en Feliu on the Formentor Peninsula. ( ). The other hotel exterior shots were filmed at the   Estate, ( ) a large Italian style villa, surrounded by Lazzarini-designed gardens, once owned by the German designer Jil Sander, and subsequently purchased by the Island Council of Mallorca. It is located in Buñola, north of Palma. Finally, Poirot boards his boat to the Island from Cala de Deiá, the costal area of the exclusive town. The locations were well stitched together to give the appearance of a few locations near each other on a small island, when in reality they are spread across Majorca. It made full use of its location to adequately convey the intricacies of Christies plot, in which the hotel guests all appear to be at different parts of the island at the time of the murder. The scenes of the finding of the murdered hiker on moors at the beginning of the film were shot in the Yorkshire Dales, England, with the exterior of the Police Station being the former Literary Institute in Muker, Swaledale. 

==References==
 

==External links==
* 
* 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 