Invincible (2001 drama film)
 
{{Infobox film
| name = Invincible
| image= Invincible VideoCover.png
| caption =
| director = Werner Herzog Paul Webster
| writer = Werner Herzog
| starring =  
| music = Klaus Badelt Hans Zimmer
| cinematography = Peter Zeitlinger
| editing = Joe Bini
| distributor = Channel Four Films (UK) Fine Line Features (USA)
| released =  
| runtime = 133 minutes
| country = United Kingdom Germany Ireland United States English
| budget =
}}
Invincible ( ) is a 2001 drama film written and directed by Werner Herzog. The film stars Tim Roth, Jouko Ahola, Anna Gourari, and Max Raabe. The film tells the story of a Jewish strongman in Germany. While basing his story on the real-life figure, Zishe Breitbart (aka Siegmund Breitbart), Herzog uses the bare facts of Breitbarts life to weave fact and fiction (e.g., the story is set in 1932 Berlin, a full seven years after Breitbarts death in 1925) to create an allegory of human strength, knowing oneself with honesty, and also pride in ones heritage.
 German film The Pledge feature film industry, and one of several collaborations with Herzog as well.

== Plot ==
Zishe Breitbart (Jouko Ahola) is the son of an Orthodox Jewish blacksmith in rural Poland. He is fantastically strong, largely from working at hard labor all day. A talent agent sees how strong Breitbart is in his Jewish shtetl home and convinces him to move to Berlin, where he can find work as a strongman.
 Nordic helmet and calls him "Ziegfried" so as to identify him with the Aryan notion of physical superiority. This appeals to the largely Nazi clientele, and he is a big hit.

This is a dark comedy but is as much so a deeply dramatic story, involving the mainly secular Jews of Berlin.  Included is interaction between Breitbart, an attractive stage musician Marta, their boss Hanussen, who abuses her, and some very top level Nazis.  Ultimately Breitbart becomes disgusted and dismayed.  

A visit from Breitbarts young brother, Benjamin (Jacob Benjamin Wein), convinces Breitbart to be proud of his Jewish heritage, and so, without warning, he takes off the blonde wig in the middle of his act to announce that he is not an "Aryan", and calls himself a new Jewish Samson. This has the effect of making him a hero to the local Jews, who flock to the cabaret to see their new Samson. The Nazis arent as pleased, and Hanussen tries to discredit Breitbart. He tries to make it seem that it was his mystic powers that were the true strength behind the strongman, and makes it look as though even his frail female pianist Marta can break chains and lift weights if under his power.

Hanussen knows the Nazis dabble in the occult and hopes to become a part of Hitlers future government. He therefore hobnobs with the likes of Himmler and Goebbels. In the end, however, he is exposed as a Czech Jewish con artist named Herschel Steinschneider. As a result, Hanussen is kidnapped and murdered by the Brownshirts. Breitbart foresees what will be known as the Holocaust and returns to Poland to warn the Jewish people of its coming. Unfortunately, no one believes him and he accidentally dies from an infected wound, according to the final titles, two days before Hitler takes power in 1933. In the final scene he is in a delirium as a result of the infection. In a moving vision he sees his younger brother Benjamin flying safely away from the looming Holocaust.

==Cast==
* Jouko Ahola -  Zishe Breitbart.  A Jewish strongman who works in a Berlin occult cabaret. The character is based loosely on Zishe Breitbart.
* Tim Roth - Hanussen. The owner and star attraction of the cabaret. He is based on Erik Jan Hanussen. 
* Anna Gourari - Marta Farra . A pianist and Hanussens mistress.
*   - Mrs. Holle

==Critical reception==
Invincible received mixed reviews during its North American theatrical run. On one end of the spectrum,  , October 4, 2002. Retrieved Feb 22, 2010.  
 Ebert & Roeper, Eberts co-host Richard Roeper was also enthusiastic, calling the film, "A tremendous piece of work." 

David Stratton described it as an uninteresting and overly-long take on a fascinating period of 20th century history. However he did appreciate the production values, which were solid, and the film had a predictably rich music soundtrack. 

As of 24 August 2010, the film has a score of 53% on Rotten Tomatoes.  , RottenTomatoes.com. Retrieved 24 August 2010. 

==Box office==
Invincible opened in North America on September 20, 2002 on 4 theatres, grossing $14,293 USD ($3,573 per screen) in its opening weekend, ranking 85th for the weekend. At its widest point, it played at only 9 theatres, and its total gross is $81,954 USD. It was only in theatrical release for 35 days.

==References==
 

==External links==
* 
* , by his sister Judy Bart Kancigor
* , Written by Mr. Gary Bart of Forty-Three Productions and Siegmunds Great-Nephew"
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 