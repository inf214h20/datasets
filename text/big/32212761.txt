Pups (film)
{{Infobox film
| name           = Pups
| image size     = 
| image	=	Pups FilmPoster.jpeg
| alt            = 
| caption        =  Ash
| producer       = Daniel M Berger 
| writer         = Ash Baron-Cohen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Mischa Barton Burt Reynolds Cameron Van Hoy
| music          = Erran Baron Cohen
| cinematography = Carlos Arguello
| editing        = Michael Schultz
| studio         = 
| distributor    = Allied Entertainment Group
| released       = 18 April 1999
| runtime        = 103 mins
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Pups is a 1999 American independent crime drama written and directed by Ash (director)|Ash. The film stars Mischa Barton, Burt Reynolds and Cameron Van Hoy. The film centres on two young adolescents that embark on a bank robbery on their way to school. The film premiered at the Los Angeles Independent Film Festival on 18 April 1999. The film, although well received critically received a limited release that has been attributed as sensitivity to the Columbine High School massacre that occurred two days after the premiere.   Entertainment Weekly. 3 March 2000 

==Cast==
*Mischa Barton as Rocky
*Burt Reynolds as Daniel Bender
*Cameron Van Hoy as Stevie
*Ed Metzger as Mr. Edwards
*Kurt Loder as himself
*David Alan Graf as Bank manager
*Darling Narita as Joy
*Adam Farrar as War Vet paraplegic

==Plot==
Stevie (Van Hoy) finds a gun in his mothers closet, and on his way to school with his girlfriend, Rocky (Barton), decides to use the gun to rob a bank. The police come and surround the building. One FBI agent, Daniel Bender (Reynolds), tries to free the hostages. Initially he is calm and cooperative, giving food, condoms, beer and an MTV reporter by request. As the situation furthers, Bender gets frustrated. After several failed attempts at impossible requests, Stevie decides to surrender. Rocky and he leave the guns, money, and hostages in the bank. As the two walk away, Stevie reaches for a flower in his pocket, and a sniper shoots him dead. Bender asks to get "that mother fucker down from that roof".

==Production== Jonesboro shootings; "The film was a reflection of what was going on in Jonesboro and around the world,". As well as the controversy of the Columbine killings, the release of the film was complicated by the arrest of one of its co-stars, Adam Farrar. Farrar had been arrested in March 1999 on suspicion of attempted murder and making terrorist threats against his girlfriend.  

==Critical reception==
The film was well received by critics, it currently holds a 90% fresh rating on Rotten Tomatoes.  Robert Koehler of Variety (magazine)|Variety praised the execution of the film "Applying all the assets of seat-of-your-pants indie filmmaking with few of its deficits, Ash has delivered a sinewy, disturbing sophomore work.. "Pups" gives off the energy of a movie shot on the run with few of the rough edges that typically dog such rapid-fire filmmaking." Koehler continued to single out the picture as a "perceptive spin on the teen pic". Koehler also praised the casting "Van Hoy leads the way, with a startling, haunting film debut that matches pics sense of impulse, rage and humanism. Barton quietly suggests a smart girl who knows shes in trouble but might find a way out. Besieged on all sides, Reynolds works against his characters cliches and indicates that "Boogie Nights" was no fluke."   Varierty. 25 April 1999 

Roger Ebert of the Chicago Sun-Times gave the film three out of four stars, congratulating Van Hoy and Barton for "two of the most natural and freed performances I have seen by actors of any age...Often Van Hoy and Barton waltz through long takes, working without the net of editing...So much depends on the performances. If instead of Van Hoy and Barton the movie had starred safer or more circumspect actors, the energy would have flagged and the flaws of the quick production would have been more of a problem."   Chicago Sun-Times. 10 December 2000 

Lawrence Van Gelder of the New York Times described the protagonists, Stevie and Rocky as "Bonnie and Clyde for the MTV generation." He praised Ash for "taking a knowing look at adolescents informed but not educated by television and movies in a less than perfect United States." Van Gelder also praised Van Hoy for his portrayal of  "a volatile, fast-talking compendium of pop culture, childish rage, adolescent mischief and adult stupidity and remorse." And Barton also, "in a layered performance, combines loyal girlfriend and voice of reason with deep cynicism toward the world she was born into and now is shaping." 
 Sundance had it not been for the recent Columbine tragedy. 

===Awards and honours===
{| class="wikitable"
|-
! Year !! Award !! Category !! Result 
|- 1999
|rowspan="1"|Chicago International Film Festival
|"Gold Hugo award for New Directors Competition" - Ash
|  
|- Stockholm International Film Festival
|"Bronze Horse" - Ash
|  
|- Festival du Film Policier de Cognac
|"New Blood Award" - Ash
|  
|- 2000
|rowspan="1"|Yubari International Fantastic Film Festival
|"Grand Jury Prize-Feature" - Ash
|  
|-
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 