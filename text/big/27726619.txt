Aching Wives: Continuous Adultery
{{Infobox film
| name = Aching Wives: Continuous Adultery
| image = Aching Wives - Continuous Adultery.jpg
| image_size =
| caption = Theatrical poster for Aching Wives: Continuous Adultery (2006)
| director = Akira Fukuhara
| producer = Akira Fukamachi
| writer = Akira Fukuhara
| narrator = 
| starring = Tomohiro Okada Mayuko Sasaki Yōko Satomi
| music = Ichimi Ōba
| cinematography = Shōji Shimizu
| editing = Shōji Sakai
| studio = Haikyū
| distributor = Shintōhō
| released = December 26, 2006
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}

  is a   ceremony. Other awards won by the films personnel were Mayuko Sasaki, who won a Best Actress award, Tomohiro Okada, who won a Best Actor award, and Shōji Shimizu for Best Cinematography. The film itself was named the Fourth Best Film of the pink film genre for its year of release. 

==Synopsis==
Toshio, an aspiring novelist, has an affair with Mitsue, a married woman. Mitsue leaves her husband for a rendezvous with Toshio in a hot springs town. While with Toshio, Mitsue learns that her daughter died in an automobile accident. A decade passes, and Toshio is married to Miki, an office lady. While suffering from writers block, Toshio returns to the hot springs inn he had stayed in with Mitsue. There he meets her by chance.    

==Cast==
* Tomohiro Okada ( ) as Toshio Miyashita 
* Mayuko Sasaki ( ) as Mitsue Takayanagi
* Yōko Satomi as Miki Miyashita
* Hōryū Nakamura ( ) as Keiichi Takayanagi
* Yūko Mizuki ( ) as Nobuko Saeki
* Seiji Nakamitsu ( ) as Kōji Okamoto
* Yutaka Ikejima as Innkeeper

==Bibliography==
*  
*  

==External links==
*  

==Notes==
 

 
 
 
 
 

 

 
 
 
 


 
 