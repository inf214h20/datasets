Filmistaan
 
{{Infobox film
| name           = Filmistaan
| image          =01 filmistan 30x40 poster2.jpg
| alt            = 
| caption        = Official film poster
| director       = Nitin Kakkar
| producer       = Shyam Shroff Balkrishna Shroff Subhash Chaudary Shaila Tanna Siddharth Roy Kapur
| writer         =
| screenplay     = Nitin Kakkar Sharib Hashmi(dialogues)
| story          = Nitin Kakkar
| starring       = Sharib Hashmi  Inaamulhaq Kumud Mishra   Gopal Dutt
Kavita Thapliyal
| music          = Arijit Datta
| cinematography = Subhransu Das
| editing        = Sachindra Vats
| studio         = Satellite Pictures Pvt. Ltd
| released       =  
| runtime        = 117 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}}
Filmistaan is a 2013 Indian comedy film written & directed by Nitin Kakkar. 
 Best Feature 60th National Film Awards 2012.  The film was also screened internationally at the 24th Palm Springs International Film Festival, 23rd Tromsø International Film Festival, 36th Göteborg International Film Festival, and 5th Jaipur International Film Festival. Filmistaan was released nationwide across India on June 6, 2014. 

==Plot==
In Mumbai, affable Bollywood buff and wanna-be-actor Sunny, who works as an assistant director, fantasizes on becoming a heart-throb star. However, at every audition he is summarily thrown out. Undeterred, he goes with an American crew to remote areas in Rajasthan to work on a documentary. One day terrorist group kidnaps him for the American crew-member. Sunny finds himself on enemy border amidst guns and pathani-clad guards, who decide to keep him hostage until they locate their original target.  The house in which he is confined belongs to a Pakistani, whose trade stems from pirated Hindi films, which he brings back every time he crosses the border. Soon, the two factions realize that they share a human and cultural bond. The film shows how cinema can be the universal panacea for co-existence.

==Cast==
*Sharib Hashmi As Sunny
*Inaamulhaq As Aftaab
*Kumud Mishra As Mehmood
*Gopal Dutt As Jawwad

==Production==
The Pakistani village depicted is the Beermanna village  in Rajasthan, India. The film was declared tax free in Maharashtra due to the idea of binding India and Pakistan.   

==Awards==
 National Film Award for Best Feature Film in Hindi.
 

==Critical reception==

 

==References==
 

==External links==
* 
*  

 

 
 
 
 