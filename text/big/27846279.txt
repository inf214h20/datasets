Carnivel
{{Infobox film
| name           = Carnivel
| image          = Carnivalfilm.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = P. G. Viswambharan
| producer       = Shyney Films
| writer         = S. N. Swamy Parvathy Sukumaran Babu Antony Shyam
| cinematography = J. Williams
| editing        = K. Sankunni
| studio         = Shyney Films
| distributor    = Shyney Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
Carnivel is a 1989 Malayalam film written by S. N. Swamy and directed by P. G. Viswambharan. The film centres on a murder held in the backdrops of a carnival. It stars Mammootty, Parvathy Jayaram|Parvathy, Sukumaran and Babu Antony in pivotal roles.

==Plot== Siddique and Bobby Kottarakkara) earn money by gambling in the streets. But Bharathan is forced to close down his business when a carnival is organised in the village. Bharathan manages to get a job in the carnival company and soon becomes the most trusted person of its owner Chandrappan, who is fondly called as Bhai. James, another worker at the company is envious about the growth of Bharathan. He attempts to kill Bharathan by removing the brakes of Bharathans circus bike. Unfortunately, Bhai is killed in that accident. Bharathan, who once happened to witness James having sex with Bhais wife, is doubtful about Jamess involvement in the murder. Meanwhile, James, with the help of a police officer, casts Bharathan for the murder. In the climax, Bharathan proves his innocence and brings James in front of justice. On the parallel, a romance story between Bharathan and Gowri is shown. Gowri is a girl who had to sell herself to earn bread for her family.

==Cast==
* Mammootty as Bharathan Parvathy as Gowri (Bharathans lover)
* Sukumaran as Chandrappan aka Bhai (the owner of the "Kings Carnivel")
* Babu Antony as James (worker at the carnival company)
* Mala Aravindan as Bharathans friend Siddique as Bharathans friend
* Bobby Kottarakkara as Bharathans friend
* V. K. Sriraman as Syriac Kureekkadan (police officer)
* Usha as Vanaja (Bharathans sister)
* Valsala Menon as Kamalamma (the prostitute)
* Kozhikode Santha Devi as Gowris mother
* Kunjikkuttan Thampuran (actor)|R. V. Kunjikkuttan Thampuran as Panicker (marriage broker)
* Kundara Johny as Pocker (a goon)

==External links==
*  

 
 

 