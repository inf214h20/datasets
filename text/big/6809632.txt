Kaadhal Mannan
{{Infobox film
| name           = Kaadhal Mannan
| image          = Kaadhal Mannan VCD Cover.jpg
| caption        = VCD Cover Saran
| producer       = Sudhir Kumar
| writer         = Saran  Vivek Karan Karan
| music          = Bharathwaj
| cinematography = T. Vijayakumar
| editing        = Ganesh Kumar
| distributor    =
| released       = March 6, 1998
| runtime        =
136 mins| country        = India Tamil
| box office = 15 crs
}} Karan and Girish Karnad also played other roles. The film, which had music composed by Bharathwaj, was released in March 1998 to positive reviews and box office hit.

==Plot==
Rudra (Girish Karnad), a very strict disciplinarian father of two daughters, hates the word Love. The very mention of this word makes him punish himself to unimaginable heights. He disowns his elder daughter Menaka because she elopes with her lover. The strictness is doubled for the younger daughter Thilottama (Maanu) and a marriage alliance is fixed for her. She stoically accepts her fathers decision till she meets a local mechanic Shiva (Ajith). Both hopelessly fall in love with each other. Tilottama is unable to reveal her love to Shiva and to her father, as she fears the consequences. Are the lovers able to declare their love for each other and get united? Does Rudra take to it kindly?

==Cast==
*Ajith Kumar as Shiva
*Maanu as Thilothama
*M. S. Viswanathan as Mess Viswanthan Vivek as Oyya Karan as Ranjan
*Girish Karnad as Black Dog security service MD

==Production==
Saran describes that he "was wondering what would happen if a girl, who is engaged to a particular person, falls in love with someone else" and this formed the basis of his plot for the film.

The film saw veteran music composer M. S. Viswanathan make his acting debut in a supporting role, whilst the lead actress Maanu from Assam and music composer Bharathwaj also debuted.  The films soundtrack gained rave review prior to release.  Viswanathan had initially waded away the approach but actor Vivek later convinced him to partake in the film. 

==Release==
The film released in 108 screens worldwide. It won positive reviews from critics, with a reviewer praising the film for tackling a taboo subject. The critic claimed that Ajith Kumar "was back at his best", whilst also crediting success to Bharathwajs soundtrack.   Despite winning plaudits for her portrayal, Maanu quit the film industry for over a decade before resurfacing as a promoter for the 2010 Singaporean film Gurushetram - 24 Hours of Anger and later as an aide to Rajinikanth during his health-related visit to Singapore in 2011. 

The film was commercial success at the box office as it released during a period of crisis in the film industry where the FEFSI strikes were ongoing and thus the distributors refused to pick the film up outright and insisted on distribution only. The first copy was worth Rs 22 million, but was only sold for Rs 16 million. Still, it ran for 200 days and re-established Ajith Kumars market after a string of failures. 

==Soundtrack==
{{Infobox album|  
  Name        = Kaadhal Mannan
|  Type        = Soundtrack
|  Artist      = Bharathwaj
|  Cover       =
|  Released    = 1998
|  Recorded    = 1998 Feature film soundtrack
|  Length      = 22:20
|  Label       = Magna Sound Sa Re Ga Ma
|  Producer    = Bharathwaj
|  Reviews     =
|  Last album  = Pooveli (1998)
|  This album  = Kaadhal Mannan (1998)
|  Next album  = Amarakallam (1999) 
}}
The soundtrack of the film was composed by Bharathwaj, was well received by the audience.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Unnai Paartha
| extra1          = S. P. Balasubrahmanyam
| length1         = 
| title2          = Vaannum Mannum
| extra2          = Hariharan (singer)|Hariharan, K. S. Chithra 
| length2         = 
| title3          = Thilothamma
| extra3          = Bharathwaj, Annupamaa
| length3         = 
| title4          = Mettu Thedi
| extra4          = M. S. Viswanathan  
| length4         = 
| title5          = Marimuthu Marimuthu Deva
| length5         = 
| title6          = Kanni Pengal
| extra6          = Febi, Ada Ali Azad  
| length6         = 
}}

==References==
 

 

 
 
 
 
 
 