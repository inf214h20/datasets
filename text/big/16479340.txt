The Cloud Mystery
The documentary by Danish director Danish scientist solar activity may affect cloud cover, and how this might influence global warming.  Also known as Klimamysteriet in Danish language|Danish.

This documentary presents the viewpoint of scientists who do not consider global warming is caused by anthropogenic production of carbon dioxide. It argues that cloud cover change caused by variations in cosmic rays is a major contributor to global temperature increase, and it claims that human influence and the effect of greenhouse gases have been exaggerated.

==Where to watch it==

The Cloud Mystery aired on TV2 (Denmark) in early 2008. It was also showed on Norways NRK and on TV4 Fakta, (which can be viewed in Sweden and Finland. ) and on Arte (April 2. 2010, "Das Geheimnis der Wolken") in Germany.

==Opinions== Danish engineering documentary gave a sober overview of Henrik Svensmarks theory, though it lacked scientific criticism. 
 Galactic Cosmic Rays vs Global Temperature). {{Cite journal Mike Lockwood & Claus Fröhlich
| title = Recent oppositely directed trends in solar climate forcings and the global mean surface air temperature
| journal = Proceedings of the Royal Society A
| doi = 10.1098/rspa.2007.1880
| year = 2007
| volume = 463
| pages = 2447
| bibcode=2007RSPSA.463.2447L
}}  

==See also== Cosmic rays – Postulated role in climate change
* Milky Way
* Barred spiral galaxy
* Cloud condensation nuclei
* The Chilling Stars
* The Great Global Warming Swindle

==References==
 

==External links==
 
*   Danish press.
*   - review of The Cloud Mystery in Danish engineering trade weekly Ingenøren.
*  
*  
* Other documentary movies of Lars Oxfeldt Mortensen disputing the anthropic Global Warming (AGW): 
** 
** 
*   - official website for Simon Ravn, who composed the music for The Cloud Mystery
*  
*   for Niels Ostenfeld, film editor, The Cloud Mystery

 
 
 
 
 
 
 