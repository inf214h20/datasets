The Expulsion from Paradise
 
{{Infobox film
| name           = The Expulsion from Paradise
| image          = 
| caption        = 
| director       = Niklaus Schilling
| producer       =   Manfred Korytowski
| writer         = Niklaus Schilling
| starring       = Herb Andress
| music          = 
| cinematography = Ingo Hamer
| editing        = Niklaus Schilling
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Expulsion from Paradise ( ) is a 1977 West German comedy film directed by Niklaus Schilling. It was entered into the 27th Berlin International Film Festival.   

==Cast==
* Herb Andress – Andy Paulisch
*   – Astrid Paulisch
* Ksenija Protic – Gräfin
* Jochen Busse – Berens
* Andrea Rau – Evi Hollauer
* Herbert Fux – Kameramann
* Elisabeth Bertram – Mutter
* Georg Tryphon – Produzent
* Trude Breitschopf – Kundin
* Harry Raymon
* Werner Abrolat – Regisseur im Aufzug
* Dieter Brammer – Besetzungschef
* Hans-Jürgen Leuthen
* Gert Wiedenhofen – Pfarrer
* Heinz Baues von der Forst – Grenzbeamter

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 