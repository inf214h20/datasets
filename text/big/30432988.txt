Sahara Jaluchi
{{Infobox film
 | name = Sahara Jaluchi
 | image = 
 | caption = DVD Cover
 | director = Sudhanshu Sahu
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Uttam Mohanty Siddhanta Mahapatra Jyoti Misra Mihir Das Aparajita Mohanty Mithun Chakraborty
 | music = Swarup Nayak
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1998
 | runtime = 125 min. Oriya
 | budget =  
 | preceded_by = 
 | followed_by = 
 }}
 1998 Oriya Indian feature directed by Sudhanshu Sahu, starring  Uttam Mohanty, Siddhanta Mahapatra, Jyoti Misra, Mihir Das, Aparajita Mohanty and Mithun Chakraborty

==Plot==
Biraj Das, a teacher lives with his wife Malati, son Priya and daughter Seema. Briaj stands for his honesty & integrity. One day he witnessed Pappu Raulas  goons murdering a person in the street. Birajs family insist him to forget the incident, but he finally decides to inform the police. Papu Raulss goons threaten him to keep mum. At last Biraj get killed by the goons. His son Priya, his journalist girlfriend Tanu and Raja, who is also intends to kill Papu Raula, as Rajss brother also killed by Pappu  unite. The team  ends up finally killing Pappu Raula and take revenge.

==Snippets==

Sahara Jaluchi is Mithuns first Oriya movie and he does a special appearance. Later in 2003, Mithun Chakraborty acted with Uttam Mohanty in Oriya film Ae Jugara Krushna Sudama.

==Cast==

*Uttam Mohanty	... 	Biraj Das
*Siddhanta Mahapatra	 ... 	Priyabrata Das
*Mihir Das	... 	Raju
*Rekha Jain		
*Jyoti Misra	... 	Tanu Mahapatra
*Tandra Roy	... 	Malati
*Bijay Mohanty	... 	Pappu Raula
*Mithun Chakraborty

==References==
*http://www.imdb.com/title/tt1449247/
*http://www.moviesplanet.com/movies/328299/sahara-jaluchi
*http://www.subs.to/movie-details/2dmPA1d7CV

==External links==
*  

 
 
 


 