Beyond Justice
{{Infobox film
| name     = Beyond Justice
| image    = Beyond Justice.jpg
| director = Duccio Tessari
| writer   = Adriano Bolzoni Sergio Donati Luigi Montefiori
| starring = Ruger Hauer Carol Alt Omar Sharif
| producer = Pio Angeletti Adriano De Micheli Guido Lombardo
| cinematography = Giorgio Di Battista
| editing  = Mario Morra
| studio   = Titanus Films
| distributor = 
| released =  1992 
| music    = Ennio Morricone
| runtime  = 113 minutes
| country  = Italy
| language = 
| budget   =
}} 1992 cinema Italian film directed by Duccio Tessari that was shot in Morocco. It was a feature film edited from the 300 minute 1989 Italian Canale 5 television miniseries Il principe del deserto. 

==Plot==
When the estranged Moroccan husband of wealthy corporate head Christine Sanders takes their son Robert to Morocco, she hires two operatives who specialise in rescuing hostages from terrorists to bring her son back. Robert is brought to his grandfather who wishes the boy to succeed him as the ruler of his tribe.

==Cast==
*Rutger Hauer  ...  Tom Burton  
*Carol Alt  ...  Christine Sanders  
*Omar Sharif  ...  Emir Beni-Zair  
*Elliott Gould  ...  Red Merchantson  
*Kabir Bedi  ...  Moulet Beni-Zair 
*Stewart Bick  ...  
*David Flosi  ...  Robert Sanders  
*Brett Halsey  ...  Sal Cuomo  
*Peter Sands  ...  James Ross  
*Christopher Ahrens  ...  Bodyguard  
*Larry Dolgin  ...  Duncan  
*D.R. Nanayakkara  ...  El Mahadi  
*David Thompson  ...  Headmaster Bligh

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 
 



 

 