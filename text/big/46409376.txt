Fairground People
{{Infobox film
| name = Fairground People
| image =
| image_size =
| caption =
| director = Carl Lamac
| producer =  
| writer =  Charlie Roellinghoff   Václav Wasserman   Hans H. Zerlett
| narrator =
| starring = Anny Ondra   Sig Arno   Margarete Kupfer
| music = Jára Benes    Erich Giese   Otto Heller  
| editing =       
| studio = Ondra-Lamac-Film 
| distributor = 
| released =14 August 1930 
| runtime = 98 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Fairground People (German:Die vom Rummelplatz) is a 1930 German comedy film directed by Carl Lamac and starring Anny Ondra, Sig Arno and Margarete Kupfer. The film was made shortly after the sound revolution, which had damaged Ondras career in British films and led her to return to Germany. It showcased Ondras talents as a musical comedy star who sings and dances.  Ondras character dresses up as Mickey Mouse for her performances, and the film was known by the alternative title of Mickey Mouse Girl (Das Micky-Maus-Girl).

==Cast==
*  Anny Ondra as Anny Flock  
* Sig Arno as Hannes, Ausrufer  
* Margarete Kupfer as Annys Mutter  
* Viktor Schwannecke as Annys Vater  
* Toni Girardi as Ordini  
* Max Ehrlich as Horbes, Agent 
* Kurt Gerron as Schaubudenbesitzer 
* Gretl Basch as Mimi 
* Yvette Rodin as Lily 
* Julius Falkenstein    Paul Morgan   
* Walter Norbert  
* Paul Rehkopf  
* Fritz Spira  
* Bruno Arno

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 
 
 
 
 
 

 