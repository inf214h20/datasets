Dance of the Dead (film)
{{Infobox film
| name           = Dance of the Dead
| image          = dance_of_the_dead.jpg
| caption        = Film poster
| director       = Gregg Bishop
| producer       = {{plainlist|
* Gregg Bishop
* Ehud Bleiberg
}}
| writer         = Joe Ballarini
| starring       = {{plainlist|
* Jared Kusnitz
* Greyson Chadwick
* Chandler Darby
* Carissa Capobianco
* Randy McDowell
* Blair Redford
* Mark Oliver
* Justin Welborn
}} 
| music          = Kristopher Carter
| cinematography = George Feucht
| editing        = Gregg Bishop
| distributor    = Bleiberg Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| gross          = 
}} independent zombie comedy, directed by Gregg Bishop and written by Joe Ballarini. The film featured Jared Kusnitz, Greyson Chadwick, Chandler Darby, and Carissa Capobianco. The plot revolves around the mysterious reanimation of the dead and the efforts of several students to save their high school prom from attack.

The film had a limited theatrical release at Graumans Chinese Theatre|Manns Chinese 6 Theatres in Los Angeles on 13 October 2008, one day before being released on DVD.  Originally finished in 2007, the film premiered at a number of film festivals throughout 2008, including the South by Southwest Film Festival and the Atlanta Film Festival, to mostly positive reviews.

==Plot==
  Georgia is unexpectedly interrupted when a graveyard, next to a power plant, becomes the sudden source of resuscitated cadavers. As zombies march on the high school, a motley group of dateless teenage outcasts, among them Jimmy (Jared Kusnitz), Lindsey (Greyson Chadwick), Steven (Chandler Darby) and Kyle (Justin Welborn), take on the zombies and save the day.

Lindsey breaks up with Jimmy the day before the prom and starts going out with Mitch. On the way to the prom, Mitch takes Lindsey to the cemetery to "loosen  up" and makes out with her. Mitch is drawn out of the car and killed by a zombie then turns into one himself. Meanwhile, members of the schools SciFi club, Jules, Steven, Rod and George, are investigating the cemetery when they are attacked. Rod is killed and the remaining three are rescued by a grave digger who reveals that he was aware of the  occurrences but kept it quiet to keep his job. He instructs them on how to kill the zombies before running off. The three get surrounded again, but Lindsey comes to the rescue and they all escape in Mitchs car.

After making a pizza delivery and discovering that a family are now zombies, Jimmy finds a crashed truck with his enemy Kyle as the only survivor. Jimmy and Kyle run into a cheerleader named Gwen, and the three are attacked. They manage to kill all of the zombies with a bat they find and a gun Kyle carries in his truck. Unfortunately Jimmys truck is stolen by two zombies, leaving the three without a getaway vehicle. In another neighborhood, Nash Rambler (Blair Redford), Jensen and Dave the Drummer are in the middle of writing a new song for their band. Nash asks Jensen to open the garage door to let in some air because Jensen has been smoking pot, and they discover zombies when Jensen opens the garage door. The zombies attack, but the band members also accidentally find that the zombies like music, and they keep playing to keep the zombies at bay, but also inadvertently attracting more.

Mitchs car breaks down, and Lindsey, Jules, George and Steven take refuge in a nearby house. Jimmy calls Lindsey to find her. He, Kyle and Gwen then make their way to the house through the town sewers. On the way, they discover a substance that the power plant dumped that they believe caused the re-animation of the dead. The house in which their friends are hiding turns out to be a funeral home. Fumes from the power plant re-animate the corpses stored there. Kyle and Jimmy kill them all, but not before Kyle is bitten in the neck and turns into a zombie and the others are forced to beat him to death. Gwen manages to retrieve the funeral homes hearse and the group escapes with the intent to rescue their fellow students from the prom. On the way to the school, the hearses tires are shot out. The teens also discover their coach is still alive, and after telling him what has happened, he helps arm them. The group heads for the prom in the coachs Hummer. On the way they find and rescue the besieged band. The band then joins the group to stop the zombies.

At the school they find all of the towns zombies have gathered there and that theyve arrived too late to save everybody. The coach decides to use his explosives to blow up the school. While the coach starts setting up around the school, the schoolkids block all the doors so the zombies cant escape. While looking through the school, the SciFi club members find a small group of survivors, including the prom queen, and start to lead them out to safety.

The coach accidentally drops the detonator into a chip bowl. Jimmy, however, is forced to go back for it, and Lindsey follows him. They sneak through the gym to get to the detonator but are noticed and attacked. Before they can be killed, the onstage band starts playing to distract the zombies. Jimmy and Lindsey dance while looking for the remote.

On the way out the others are attacked but manage to kill their attackers, while Gwen hides the fact that she was bitten. She pulls Steven, for whom she has developed feelings, into the bathroom. There she reveals the truth and the two kiss, but she turns into a zombie and bites Stevens tongue off. She kills him, which turns Steven into a zombie as well. The two zombies kiss again and then start to eat each other.

In the gym, Jimmy and Lindsey are still looking for the detonator when a zombie accidentally pulls the plug on the bands instruments, stopping the music and causing the zombies to attack again. While the two hold off the zombies, Jenson tries to replace the plug. Jimmy and Lindsey are forced to hide beneath the bleachers, where they are attacked by Mitch, now a zombie. The two escape with Lindsey killing Mitch. The band manages to plug in their instruments and begin playing, allowing Jimmy to get the detonator. As the group attempt to make their escape, Jensen is caught and killed by the horde. The surviving four make their way out of the school through a window. The zombies are unable to escape out the blocked doors, and Jimmy blows up the school, killing all of the zombies, while kissing Lindsey at the same time rekindling their relationship.

The survivors, including a group that hid at the prom, takes a bus to a pancake house for breakfast courtesy of the coach with a plan to eat and plan an attack to shut down the power plant and prevent the zombie plague from spreading. The grave digger also survives and complains about the fact he has to clean up the mess.

==Cast==
* Jared Kusnitz, as James "Jimmy", a pizza deliverer
* Greyson Chadwick, as Lindsey, Jimmys lovely girlfriend and vice-president of the student council
* Chandler Darby, as Steven, the class nerd who harbors a secret crush on Gwen
* Carissa Capobianco, as Gwendoline "Gwen", a popular cheerleader
* Randy McDowell, as Jules, president of the Sci-Fi Club
* Blair Redford, as Nash Rambler, leader of a band of drug-addled teenagers
* Mark Oliver, as Coach Keel, the school coach with some kind of military background
* Justin Welborn, as Kylerick "Kyle" Grubin
* James Jarrett, as gravedigger
* Lucas Till, as Jensen, another member of the band

==Production==

The Film itself was based on a script written in the late 1990s.    It was filmed in Rome, Georgia and North Georgia. 

The cast of Dance of the Dead marked the debut of relatively unknown Georgian natives—including Greyson Chadwick, Chandler Darby, and Carissa Capobianco—and the return of sophomore actors Jared Kusnitz (Doll Graveyard) and Randy McDowell (Good Intentions).  According to director-producer Gregg Bishop, he and the producers "searched for months for the right kids, ones who were good at improv, and ended up casting more kids in Georgia ... They were more natural.   casting director would throw them curveballs and the ones who could hang and go along ended up in the movie." 

==Distribution==

Dance of the Dead was distributed on October 14, 2008 on DVD by Lions Gate Entertainment in a deal with Sam Raimis new partnership Ghost House Underground. The film is expected to be part of Ghost Houses inaugural slate of offerings dubbed "October Horror" .   

Following the Cannes Film Festival, German distributor Splendid Film bought the rights for German release.  

==Release==

===Film festivals===

Dance of the Dead premiered at the 15th South by Southwest Film Conference and Festival in Austin, Texas, March 7–15, 2008 alongside a lineup of 113 feature films, including sixty-four world premieres, high school-themed features and rock documentaries.  It was a Round Midnight selection.  
 Film4 FrightFest in London, UK, on 23 August 2008,  and the Rome International Film Festival in North Georgia, US, on 7 September 2008. 

===Critic response===
Rotten Tomatoes, a review aggregator, reports that 80% of five surveyed critics gave the film a positive review; the average rating is 7.2/10. 

Jason Whyte of eFilmCritic.com called it a "triumph in nearly every way; not only is an independent film that takes its small budget and runs with it, it is everything you want out of a horror comedy and more."  Scott Weinberg of FEARnet was surprised that "character development" was possible, and called it "fast and funny, sick and sweet, nerdy and gory. Basically, a whole lot of fun."  Expecting a "cheesy zombie flick", reviewer Brian Gibson instead described it as a "zombie/prom weekend film that ended up being one of the best surprises of the   festival."  Anton Bitel, who writes for UK film magazine Little White Lies, was tired of the zombie comedy genre in general yet still saw a film distinguished by "sharp writing, amusing characters, and a real affection for nerd culture."   Collin Armstrong of Twitch Film wrote, "Owing a sizable debt to Dan OBannons classic punk gut-muncher Return of the Living Dead, Dance manages to offer a few new ideas while mostly just taking those weve seen time and again to gonzo levels of execution."   Steve Barton of Dread Central rated it five out of five stars and wrote that the film "is rife with everything we could want in a living dead flick — great characters, good-looking zombies, and tons of gore!"   Tim Anderson of Bloody Disgusting rated it five out of five stars and called it "the best horror comedy of this or any other year".   David Harley, also writing at Bloody Disgusting, rated it four-and-a-half stars out of five and wrote, "Out of all the films I’ve seen at SXSW this year, regardless of genre, Dance of the Dead stands out as the most impressive and entertaining." 

==DVD release==
 
Dance of the Dead was released on DVD in North America on 14 October 2008. The DVD contains an audio commentary by the films director, Gregg Bishop, and writer Joe Ballarini, as well as deleted and extended scenes with optional audio commentary with Bishop. The DVD also contained several featurettes, including, "Making of Dance of the Dead," with extended commentary from cast and crew members. The second, "Blood, Guts and Rock n Roll: Effects and Stunts of Dance of the Dead", discusses the films technical production. Several additional features included a trailer gallery and Voodoo, a five-and-a-half minute short film by director Gregg Bishop.    

==See also==

* List of American films of 2008
* List of zombie films
* Living Dead

==References==
 

==External links==
 
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 