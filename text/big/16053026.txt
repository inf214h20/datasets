Silver Wings (film)
 
{{Infobox film
| name           = Silver Wings
| image          = Sliver wings newspaper ad.png
| caption        = Newspaper ad for the film
| director       = Edwin Carewe John Ford William Fox
| writer         = Paul Sloane
| starring       = Mary Carr Lynn Hammond
| cinematography = Robert Kurrle Joseph Ruttenberg
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}

Silver Wings is a 1922 American drama film directed by Edwin Carewe and John Ford. Ford directed only the prologue of the film. The film is now considered to be a lost film.      

==Cast==
* Mary Carr as Anna Webb (prologue / play)
* Lynn Hammond as John Webb (prologue)
* Knox Kincaid as John (prologue)
* Joseph Monahan as Harry (prologue)
* Maybeth Carr as Ruth (prologue)
* Claude Brooke as Uncle Andrews (prologue / play)
* Robert Hazelton as The Minister (prologue)
* Florence Short as Widow Martin (prologue)
* May Kaiser as Her Child
* Percy Helton as John (play)
* Joseph Striker as Harry (play)
* Jane Thomas as Ruth (play)
* Roy Gordon as George Mills (play)
* Florence Haas as Little Anna (play)
* L. Rogers Lytton as Bank President (play) (as Roger Lytton)
* Ernest Hilliard as Jerry Gibbs (play)

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 