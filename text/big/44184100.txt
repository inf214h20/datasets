Iniyum Kurukshetrum
{{Infobox film
| name           = Iniyum Kurukshetrum
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Sasikumar
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Mohanlal Jagathy Sreekumar Shobhana Kaviyoor Ponnamma
| music          = M. K. Arjunan
| cinematography = Vipin Das
| editing        = G Venkittaraman
| studio         = Sneha Films
| distributor    = Sneha Films
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by Sasikumar. The film stars Mohanlal, Jagathy Sreekumar, Shobhana and Kaviyoor Ponnamma in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Mohanlal
*Jagathy Sreekumar
*Shobhana
*Kaviyoor Ponnamma
*Adoor Bhasi
*Nahas
*C. I. Paul
*K. P. Ummer
*MG Soman Meena
*Rahman Rahman
*Thodupuzha Vasanthi
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by K Jayakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ethra nilaathiri || K. J. Yesudas || K Jayakumar || 
|-
| 2 || Marathakakkootil || K. J. Yesudas, Lathika || K Jayakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 