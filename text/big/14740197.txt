Gents in a Jam
{{Infobox Film |
  | name           = Gents in a Jam
  | image          = Gents Jam 1sht.jpg
  | image size     = 190px
  | caption        = 
  | director       = Edward Bernds
  | writer         = Edward Bernds Dani Sue Nolan Mickey Simpson
  | cinematography = Fayte Browne  Edwin Bryant
  | producer       = Hugh McCollum
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 12"
  | country        = United States
  | language       = English
}}

Gents in a Jam is the 141st short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges offer to repaint their landladys apartment in order to avoid being evicted. Landlady Mrs. MacGruder (Kitty McHugh) warns them that the "furnishings cost a pretty penny," so the Stooges destroy her place without fail. Just as they are packing a trunk to leave, Shemp receives a telegram from his Uncle Phineas Bowman (Emil Sitka) who is coming to visit. When Mrs. MacGruder hears the wealthy bachelor Uncle Phineas is worth $6 million, with Shemp his sole heir, she allows the Stooges to stay.
 Dani Sue Nolan) comes by to borrow a cup of sugar. While making casual conversation, Shemp keeps repeating, "Duggan, Duggan...I know that name from somewhere." Gertie confirms Shemps curiosity when she says her husband is tough, professional strongman Rocky Duggan (Mickey Simpson), known for easily tearing thick telephone books for kicks. Within seconds, Gertie takes a fall in the boys kitchen, leading to Shemp accidentally tearing off her skirt when trying to help her up. Realizing that all three Stooges will be beaten into submission if husband Rocky gets wind of this, they frantically hide Gertie. Just then, Uncle Phineas arrives, and all seems fine until Gertie, now resplendent in Shemps bathrobe, makes a dash for her apartment. Rocky sees this, and comes barging into the Stooges apartment, knocking Phineas to the floor.

Just when the towering Rocky is grinding Shemp into powder, Mrs. MacGruder appears and demands Rocky let go of Shemp. He threatens  "Beat it, lady. No dame is gonna tell me what to do." Without missing a beat, MacGruder knocks Rocky to the ground with a right hook to the jaw. As Gertie comes running to her husband, she pleads, "Honey, this whole thing was a mistake." "Mistake?" he moans, and then proceeds to spit out his teeth, griping "Look at my choppers!" Mrs. MacGruder then enters the Stooges apartment to find a weary Uncle Phineas, who turns out to be her childhood sweetheart. While Shemp is promising Rocky new teeth, since he is his uncles sole heir, Phineas and MacGruder rekindle their romance, and decide to get married, leaving Shemp and the Stooges without an inheritance and Rocky without teeth. Incensed, the strongman chases after the Stooges once more. As they round the corner of the apartment complex, each Stooge plus Rocky races past Uncle Phineas, knocking him down. As the haymaker, Gertie runs right over the fallen uncle, and squarely kicks him in the jaw.

Semi-conscious, Phineas receives a hug from the caring MacGruder, and moans, "All I wanted was a nice, quiet visit."

==Decline==
Gents in a Jam was the last short directed by Edward Bernds, long considered the Stooges finest director. Producer Hugh McCollum was discharged and, as a result, Bernds resigned out of loyalty to McCollum, leaving only director/short subject head Jules White to both produce and direct the Stooges remaining Columbia comedies. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 