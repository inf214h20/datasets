Jee Aayan Nu
{{Infobox film
| name           = Jee Ayan Nu
| image          = Jee_Aayan_Nu_Poster.jpg
| caption        = Jee Aayan Nu Movie Poster
| director       = Manmohan Singh
| producer       = Bhushan Kumar Krishan Kumar
| writer         = Baldev Gill
| starring       = Harbhajan Mann Priya Gill Kimi Verma Navneet Nishan
| music          = Jaidev Kumar
| cinematography = Harmeet Singh
| distributor    = T-Series
| released       =  
| country        = India
| awards         = Zee Punjabi Music Award for best movie Punjabi
| budget         = Rs. 90,80,000
| art            = R. Verman
| choreographer  = Vaibhavi Merchant
}}

Jee Ayan Nu ( : جی آیاں نوں,  ) is a Punjabi feature film, released in 2002. 

It stars Harbhajan Mann in the lead role with Priya Gill. It was directed by Manmohan Singh. It is one of the most successful movie in Punjabi cinema. The movies songs are quite popular and were sung by Harbhajan Mann, Alka Yagnik and others. Jee Aayan Nu is the first film in the history of Punjabi cinema to be made on such a lavish scale and with renowned technicians who have contributed their best efforts and dedication.

== Plot ==

Grewal (Kanwaljeet), a business tycoon of the media industry, is a Punjabi settled in Vancouver, Canada with his wife and daughter. Simar (Priya Gill), the elder daughter, was three years old when she was brought to Canada from her birthplace, Punjab. Living a luxurious life in Canada, both daughters are highly influenced by the local culture. Grewal, with his family, returns to Punjab after many years to attend a college function. Grewal meets Inder (Harbhajan Mann) who happens to be Grewals childhood friends son. Grewal asks Inder to take Simar around and show her the beauty of Punjab.

While sightseeing Inder makes Simar realize how loving and good-natured Punjabis are and how it is beautifully blended with their culture. Though Simar is impressed, her mother (Navnit Nishan) is not comfortable in her own country and its lifestyle. Inder and Simar soon fall in love, and the families decide to get them married. However, at their engagement, Inder realizes that Simars family expects Inder to settle in Canada with them after their marriage. Inder refuse to leave Punjab. Annoyed at Inder, the Grewal family returns to Canada.

Time passes and soon Inders parents, seeing his pain without Simar, insist that he should go to Canada and get his lost love back. Inder leaves and is determined to get Simar back.

==References==
 

== External links ==
*  

 
 
 


 