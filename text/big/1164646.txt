Iron Jawed Angels
{{Infobox film
| name = Iron Jawed Angels
| image = Iron Jawed Angels.jpg
| caption = DVD cover
| director = Katja von Garnier
| producer = Len Amato Lydia Dean Pilcher Robin Forman Paula Weinstein
| writer = Sally Robinson Eugenia Bostwick-Singer Raymond Singer Jennifer Friedes
| starring = Hilary Swank Frances OConnor Julia Ormond Anjelica Huston
| music = Reinhold Heil Johnny Klimek
| cinematography = Robbie Greenberg
| editing = Hans Funck
| distributor = HBO Films
| released =  
| runtime = 125 minutes
| country = United States
| language = English
}} American womens suffrage movement during the 1910s. The film received acclaim at the 2004 Sundance Film Festival.  Much of the principal photography was done in Richmond, Virginia.
 nonviolent strategies, tactics, and dialogues to revolutionize the American feminist movement to grant women the right to vote. This film is unrated by the MPAA.

==Plot== National American Womans Suffrage Association (NAWSA) to push directly in Washington for womens rights to vote nationally. They see that their ideas were much too forceful for the established leaders, particularly Carrie Chapman Catt (Anjelica Huston), but are allowed to lead the NAWSA Congressional Committee in DC.  They start by organizing the 1913 Womens Suffrage Parade, on the eve of President Woodrow Wilsons inauguration.  While soliciting donations at an art gallery, Paul convinces labor lawyer Inez Milholland (Julia Ormond) to serve as a figurehead for the parade and meets a Washington newspaper political cartoonist, Ben Weissman (a fictional character, played by Patrick Dempsey), causing romantic fire to start. Paul tries to explain to Ida B. Wells why she wants African American women to march in the back of the parade in order to not anger southern Democrats in the movement, but Wells refuses, and joins a white group during the middle of the parade.  After disagreements over fundraising, Paul and Burns are pushed out of the NAWSA, and found the National Womans Party (NWP) to support their approach. Alice Paul briefly explores a romantic relationship with Ben Weissman. He has a child (also a fictional character.)

Further conflicts within the movement are portrayed as NAWSA leaders criticize NWP tactics, such as protesting against Wilson and picketing outside the White House in the Silent Sentinels action. Relations between the American government and the NWP protesters also intensify, as many women are arrested for their actions, charged with "obstructing traffic."
 Occoquan Workhouse for 60-day terms. Despite abusive treatment, Paul and other women undertake a hunger strike, during which paid guards Force-feeding|force-feed them milk and raw eggs. The suffragists are blocked from seeing visitors or lawyers, until a U.S. Senator (the fictional Tom Leighton) manages to visit his wife Emily (also fictional), one of the imprisoned women. News of their treatment leaks to the media after she secretly passes a letter to him during the visit. Paul, Burns, and all of the other women are released.
 nineteenth amendment to the Constitution. Finally he accedes to the pressure, rather than be called out in the international press for fighting for democracy in Europe while denying its benefits to half of the US population. Ratification follows in the states, reaching a climax when Harry T. Burn, a member of the Tennessee legislature, receives a telegram from his mother at the last minute and changes his vote, such that the amendment is ratified.

==Fictional characters==
Fictional characters in the movie are Ben Weissman, his child, Emily Leighton, and Tom Leighton.   

==Origin of title== Joseph Walsh, who in 1917 opposed the creation of a committee to deal with womens suffrage. Walsh thought the creation of a committee would be yielding to "the nagging of iron-jawed angels" and referred to the Silent Sentinels as "bewildered, deluded creatures with short skirts and short hair." 

==Cast==
*Hilary Swank as Alice Paul
*Brett Boseman as Ben Weissmans child
*Frances OConnor as Lucy Burns
*Molly Parker as Emily Leighton   
*Laura Fraser as Doris Stevens
*Lois Smith as Rev. Dr. Anna Howard Shaw
*Vera Farmiga as Ruza Wenclawska, aka Rose Winslow Brooke Smith as Mabel Vernon
*Patrick Dempsey as Ben Weissman DVD Verdict: In this movie, Alice is given a fledgling romance with political cartoonist Ben Weissman. According to the audio commentary, he is another completely fictional character, created to give Alice a (sort of) love interest. 
*Julia Ormond as Inez Milholland
*Adilah Barnes as Ida Wells-Barnett
*Anjelica Huston as Carrie Chapman Catt
*Margo Martindale as Harriot Eaton Stanton Blatch
*Bob Gunton as Woodrow Wilson
*Vinny Genna as Fiorello La Guardia
*Joseph Adams as Senator Thomas "Tom" Leighton

==Reception==

===Critical response===
Film critic   gave the film a negative review, writing: "HBOs starry suffragette drama, Iron Jawed Angels, latches on to a worthy historical subject and then hopes noble intentions will be enough to carry the day. Alas, theres no such luck in this talky, melodramatic overview of the dawn of equal rights for women in America. Gussied up with a comically anachronistic use of period music on the soundtrack and flashy, MTV-style montage sequences, pic misguidedly strives – but ultimately fails – to belie its instincts as an assembly-line movie-of-the-week." 

Robert Pardi of   tour; it sometimes feels as though she and her writers conceived the fight for womens suffrage as a 1912 version of Sex and the City. Only when the anachronisms finally subside in the films final third is the moving core is allowed to shine." 

===Accolades===
The film was nominated for five awards at 56th Primetime Emmy Awards, though none of which were won; three awards at the 62nd Golden Globe Awards, winning one; and two awards at the 9th Golden Satellite Awards, winning one. Anjelica Huston won the Golden Globe Award for Best Supporting Actress – Series, Miniseries or Television Film and the Satellite Award for Best Supporting Actress – Series, Miniseries or Television Film for her performance in the film.
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2004
| Primetime Emmy Award Outstanding Casting for a Miniseries, Movie, or a Special
| Janet Hirshenson, Jane Jenkins, Liz Marks, Kathleen Chopin
|  
|- Outstanding Cinematography for a Miniseries or a Movie
| Robbie Greenberg
|  
|- Outstanding Costumes for a Miniseries, Movie, or a Special
| Caroline Harris, Eric Van Wagoner, Carl Curnutte III
|  
|- Outstanding Supporting Actress in a Miniseries or a Movie
| Anjelica Huston
|  
|- Outstanding Writing for a Miniseries, Movie, or a Dramatic Special
| Sally Robinson, Eugenia Bostwick-Singer, Raymond Singer, Jennifer Friedes
|  
|-
| Casting Society of America
| Best Casting for TV Movie of the Week
| Janet Hirshenson, Jane Jenkins, Liz Marks
|  
|-
| Humanitas Prize 90 Minute or Longer Category
| Sally Robinson, Eugenia Bostwick-Singer, Raymond Singer, Jennifer Friedes
|  
|- OFTA Television Award
| Best Supporting Actress in a Motion Picture or Miniseries
| Anjelica Huston
|  
|-
| Best Supporting Actress in a Motion Picture or Miniseries
| Brooke Smith
|  
|-
| Best Actress in a Motion Picture or Miniseries
| Hilary Swank
|  
|-
| Best Motion Picture Made for Television
| Iron Jawed Angels
|  
|- 2005
| Golden Globe Award Best Supporting Actress – Series, Miniseries or Television Film
| Anjelica Huston
|  
|- Best Miniseries or Television Film
| Iron Jawed Angels
|  
|- Best Actress – Miniseries or Television Film
| Hilary Swank
|  
|-
| American Society of Cinematographers
| Outstanding Achievement in Cinematography in Movies of the Week/Mini-Series/Pilot (Basic or Pay)
| Robbie Greenberg
|  
|-
| Screen Actors Guild Award Outstanding Performance by a Female Actor in a Miniseries or Television Movie
| Hilary Swank 
|  
|- Satellite Award Best Supporting Actress – Series, Miniseries or Television Film
| Anjelica Huston
|  
|- Best Miniseries or Television Film
| Iron Jawed Angels
|  
|- PEN Center USA West Literary Award
| Teleplay
| Sally Robinson, Eugenia Bostwick-Singer, Raymond Singer, Jennifer Friedes
|  
|- Costume Designers Guild Award Outstanding Period/Fantasy Television Series
| Caroline Harris 
|  
|}

==References==
 

==External links==
 
 
*  
*  

 

 
 
 
 
 
 
 