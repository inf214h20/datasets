Al Hilal (film)
{{Infobox film
| name           = Al Hilal
| image          = 
| image_size     = 
| caption        = 
| director       = Mehboob Khan
| producer       = Sagar Movietone
| writer         = Wajahat Mirza 
| narrator       =  Kumar Indira Yakub Kayam Ali
| music          = Pransukh Nayak
| cinematography = Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1935
| runtime        = 158 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1935 Urdu/Hindi Cecil B. DeMille’s The Sign of the Cross (film)|The Sign of the Cross.  The film was produced by  Sagar Movietone. The director of photography was Faredoon Irani. The music composer was Pransukh Nayak with lyrics by Munshi Ehsan Lucknavi.    It starred Kumar, Indira, Yakub (actor)|Yakub, Sitara Devi, Kayam Ali and Mehboob Khan.    The film depicted fictionalised history in the form of a Roman-Arab conflict, with the son of the Ottoman Empire being captured by the Roman army and his escape from them.

==Plot==
Set in the Ottoman Empire it deals with the Caesar’s (Pande) army and their skirmishes with the local Muslim rulers. The Sultan’s (Asooji) son Ziyad (Kumar) is arrested by the Roman army. The Roman princess Rahil falls in love with him. A Muslim maid Leela (Sitara Devi) and the princess help him escape. What follows are long chase scenes and fights which ultimately lead to success for Ziyad and his people.

==Cast== Kumar
*Indira Yakub
*Sitara Devi
*Pande
*Mehboob Khan
*Wallace
*Asooji
*Razak
*Kayam Ali
*Azurie

==Production==
Sets were lavish and extensive use of variations made in camera technique like "tight close-ups" and skilfully captured battle scenes. Through the film Mehboob Khan also showed his expertise in the field of editing.   

==Reception==
The film was a "commercial success" and Mehboob Khan earned "critical acclaim" as a director. According to Rauf Ahmed Baburao Patel of FilmIndia declared after watching the debut direction that "He will go far". 

==Songs==
Songlist 
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| "Ae Mohabbat Tere Hathon Kaisi Ruswai Hui"
|-
| 2
| "Bekhabar Veh Hain Khabar Kuchh Bhi Nahin"
|-
| 3
| "Ho Gayi Dil Sadchak"
|-
| 4 
| "Koi Umeed Bar Nahin Aati"
|- 5
| "Lab Pe Naam Jab Aapka Ahmed-E-Mukhtar Aaya"
|-
| 6 
| "Main Toh Khanjar Hun Balma Kataari Nahin"
|-
| 7
|  "Tujhe Dhoondata Tha Main"
|-
| 8
| "Veerana Dil Kisi Se Basata Na Jaayega"
|-
| 9
| "Duniya Ke Inquilab Ka Sadma Fazool Hai"
|-
|}

==References==
 

==External links==
* 

 

 
 
 