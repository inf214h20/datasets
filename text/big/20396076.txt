A Lady of Chance
{{Infobox film
 | name = A Lady of Chance
 | image = A Lady of Chance.jpg
 | caption = Theatrical poster
 | director = Robert Z. Leonard
 | writer = Edmund Goulding A. P. Younger
 | starring = Norma Shearer
 | producer = Robert Z. Leonard
 | studio = Metro-Goldwyn-Mayer 
 | distributor =
 | budget =
 | released =  
 | country = United States
 | language = Silent film English intertitles
 | runtime = 78 minutes 
 | }}

A Lady of Chance is a 1928 silent film directed by Robert Z. Leonard. The film is based upon the story "Little Angel" by Leroy Scott and is  Norma Shearers last silent film. Although the film was released with added dialogue scenes, Shearer cant be heard.  

==Plot==
Dolly "Angel Face" Morgan is a parolee out to fleece any wealthy man who is taken in by her looks.  She is recognized by two fellow con artists, Gwen and Brad.  Since she needs some help, she allows them to help "pull a job," shaking down a wealthy man for $10,000 after her outraged "husband" (Brad) breaks in and finds them in a compromising situation.  But when Brad has Gwen hide the money, and tells Dolly that their victim stopped payment on his check, Dolly takes all of the money and makes a quick getaway.

Soon after, Dolly meets a young man named Steve Crandall in Atlantic City for a cement convention. Believing that he is a wealthy plantation owner, she flirts with him. When he proposes they get married that very night, Dolly is shocked, but accepts. She is packing to leave with Steve when Brad shows up, demanding his share of the $10,000. Once again, Dolly uses her wits to escape.

Dolly and Steve take the train south to his home town of Winthrop, Alabama. There Dolly is rudely surprised to discover that Steve is far from rich, nor does he own a plantation (though he lives next door to one).  He is certain his invention, Enduro cement, will make his fortune, but his new wife is not so sure.  Dolly has grown fond of Steve, but cannot hide her disappointment from him. That evening, she has him take her to a train for New York.  The next morning, however, Steve returns to his room to find Dolly curled up in a chair. She is in love with him and has decided to reform, though she keeps her past a secret.

Brad and Gwen track her down, certain she has landed yet another rich sucker. They are surprised to find her living in modest circumstances. Dolly tells them that she has fallen for a poor man, but they do not believe her. To get rid of them, she gives them the $10,000. However, Steve receives a telegram informing him that a company has bought his cement formula for $100,000. Overjoyed, he rushes home and tells Dolly, his mother, and "cousins" Brad and Gwen.

Brad and Gwen blackmail Dolly into a scheme to part Steve from his new-found riches. Brad invites the couple to stay with him in New York City.  Just as Steve is about to sign Brads contract, Dolly cannot take it anymore. She telephones the police, then tells Steve that the contract is nothing but a scam; she then confesses to Steve that she herself is a crook and that she only married him in order to fleece him. Steve is devastated.

The cops show up and take her away. Steve begs Dolly to come back to him, but she says that he would be better off without her.  Dolly is taken to prison.  Steve, however, manages to get the warden to parole her into his custody.

==Cast==
* Norma Shearer as Dolly "Angel Face" Morgan Crandall
* Lowell Sherman as Bradley / "Brad"
* Gwen Lee as Gwen
* Johnny Mack Brown as Steve Crandall
* Eugenie Besserer as Ma Crandall
* Buddy Messinger as Hank Crandall

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 