Three Women (1952 film)
 
{{Infobox film
| name           = Three Women
| image          = 
| caption        = 
| director       = André Michel Pierre Lévy
| writer         = Claude Accursi Guy de Maupassant Jean Ferry
| starring       = Marcelle Arnold
| music          = 
| cinematography = Henri Alekan André Bac Maurice Barry
| editing        = Victoria Mercanton
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         = 
}}

Three Women ( ) is a 1952 French film directed by André Michel. It was entered into the 1952 Cannes Film Festival.     

==Cast==
* Marcelle Arnold
* Michel Bouquet - M. Lesable (segment "Héritage, L")
* Betty Daussmond
* Agnès Delahaie - Coralie Cachelin (segment "Héritage, L")
* Blanche Denège
* Moune de Rivel - Zora (segment "Boitelle")
* Jacques Duby - Antoine Boitelle
* Jacqueline Duc
* Catherine Erard - Mouche (segment "Mouche")
* Jacques Fabbri - Albert (segment "Mouche")
* Florelle
* Jacques François - Horace (segment "Mouche")
* René Lefèvre (actor)|René Lefèvre - M. Cachelin (segment "Héritage, L")
* Marcel Lupovici
* Marcel Mouloudji
* Bernard Noël - M. Maze (segment "Héritage, L")
* Pierre Olaf - Ptit bleu (segment "Mouche")
* Palau - M. Torcheboeuf (segment "Héritage, L")
* Raymond Pellegrin - Julien (segment "Mouche")
* Germaine Stainval
* Rosy Varte
* Yvonne Yma

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 