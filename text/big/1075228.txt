Oliver's Story (film)
 
{{Infobox film
| name           = Olivers Story (film)
| image          = Olivers Story -1978 - poster.png
| caption        = Theatrical release poster
| producer       =  David V. Picker	 
| director       =  John Korty	 	 
| writer         =  Erich Segal	and John Korty
| starring       =  Ryan ONeal   Candice Bergen
| music          =  Lee Holdridge   Francis Lai
| cinematography =  Arthur J. Ornitz
| editing        =  Stuart H. Pappé	 	 
| studio         =  Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $6 million 
| gross          =   
| followed by    =  
}} Love Story, is a 1978 film based on an Erich Segal novel published a year earlier. It was directed by John Korty and again starred Ryan ONeal, this time opposite Candice Bergen. The original music score was composed by Lee Holdridge and Francis Lai.

This films tagline is: "It takes someone very special to help you forget someone very special."

==Plot summary==
Oliver Barrett IV is emotionally devastated by the death of his young wife Jenny. As he tries to lose himself in his work as a lawyer, the long hours dont ease his pain, especially when he finds that his leftist views conflict with those of the senior partners at the firm.

Olivers inconsolable grief begins to alienate those around him, at least until he finds new love with Marcie Bonwit, the wealthy and beautiful heiress to the Bonwit Teller fortune. Despite his affection for her, Oliver finds it difficult to leave the memory of Jenny behind, which causes many problems in their relationship.

==Location==
A number of scenes were filmed in Massachusetts and Rhode Island. The Stanley Woolen Mill in Uxbridge, Massachusetts, and other locations in that community were used for this film. Olivers law offices were those occupied at the time by the New York firm of Davis Polk & Wardwell.

==Cast==
* Ryan ONeal &ndash; Oliver Barrett IV
* Candice Bergen &ndash; Marcie Bonwit
* Nicola Pagett &ndash; Joanna Stone
* Ed Binns &ndash; Phil Cavilleri
* Benson Fong &ndash; John Hsiang
* Charles Haid &ndash; Stephen Simpson Kenneth McMillan &ndash; Jamie Francis
* Ray Milland &ndash; Oliver Barrett III
* Josef Sommer &ndash; Dr. Dienhart
* Sully Boyar &ndash; Mr. Gentilano
* Swoosie Kurtz &ndash; Gwen Simpson
* Meg Mundy &ndash; Mrs. Barrett
* Beatrice Winde &ndash; Waltereen

==Production==
John Marley did not reprise his role as Ali MacGraws father from the original. He and Paramount had come to terms on money but not billing; he was replaced by Edward Binns. FILM CLIPS: Harvey: Movies Back to Back
KILDAY, GREEGG. Los Angeles Times (1923-Current File)   12 Apr 1978: f8. 

==Critical Reception==
Unlike the original film, Olivers Story was poorly reviewed and was not successful at the box office. The film currently holds a 20% "Rotten" rating with an average score of 4.1/10 at Rotten Tomatoes.

==References==
 

== External links ==
* 
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 