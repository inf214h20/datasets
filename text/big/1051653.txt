Rollerball (1975 film)
 
 
{{Infobox film
| name           = Rollerball
| image          = RollerballPoster.jpg
| caption        = Theatrical release poster
| director       = Norman Jewison
| producer       = Norman Jewison William Harrison John Beck Moses Gunn Ralph Richardson
| music          = André Previn
| cinematography = Douglas Slocombe
| editing        = Antony Gibbs
| distributor    = United Artists
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $30,000,000 
}}
 sports Action action science William Harrison,  who adapted his own short story, "Roller Ball Murder", which had first appeared in the September 1973 issue of Esquire (magazine)|Esquire.  Although it had an American cast, a Canadian director, and was released by the American company United Artists,  it was produced in London and Munich.  {{citation
| first1=Stephen | last1=Vaughn | year=2006| title=Freedom and Entertainment: Rating the Movies in an Age of New Media| publisher=Cambridge University Press| isbn=0-521-85258-7 | page=55| url=http://books.google.com/books?id=B7MkaxKGTGAC&pg=PA55}} 

==Premise==
In the film, the world of 2018 (referred to in the tagline as "the not too distant future") is a global corporate state, containing entities such as the Energy Corporation, a global energy monopoly based in Houston which deals with nominally-peer corporations controlling access to all transport, luxury, housing, communication, and food on a global basis. According to the tagline, in this world, "wars will no longer exist. But there will be... Rollerball."

The films title is the name of a violent, globally popular sport around which the events of the film take place. It is similar to Roller Derby in that two teams clad in body armor skate on roller skates (some instead ride on motorcycles) around a banked, circular track. There, however, the similarity ends. The object of the game is to score points by the offensive team (the team in possession of the ball) throwing a softball-sized steel ball into the goal, which is a magnetic, cone-shaped area inset into the wall of the arena. The team without possession of the ball is defensive and acts to prevent scoring. It is a full-contact sport in which players have considerable leeway to attack opposing players in order to take or maintain possession of the ball and to score points. In addition, each team has three players who ride motorcycles to which teammates can latch on and be towed. The player in possession of the ball must hold it in plain view at all times.
 global corporations. Energy Corporation sponsors the Houston team. The game is a substitute for all current team sports and for warfare. While its ostensible purpose is entertainment, Mr. Bartholomew, a high-level executive of the Energy Corporation, describes it as a sport designed to show the futility of individual effort.

==Plot==
The film follows Jonathan E. (James Caan), the veteran star of the Houston rollerball team.  By virtue of his stellar performance over the years, Jonathan has become the most recognizable Rollerballer in history; everyone recognizes him on sight. This is problematic for the hegemonic corporations. After another impressive performance in Houstons victory over the Madrid team, Energy Corporation chairman Mr. Bartholomew (John Houseman) announces that the corporation, running out of ways to reward its champion, will feature Jonathan in a "multivision" special devoted to his career.

Mr. Bartholomew later tells Jonathan that they want him to retire. He offers Jonathan a lavish retirement package, including special "privileges", if he announces his retirement on his televised special. Mr. Bartholomew emphasizes the benefits of corporate-run society and the importance of respecting executive decisions, but does not explain why they want Jonathan to retire. It is revealed that Jonathan was married to Ella (Maud Adams), which ended when she was promised to an executive.

Jonathan struggles to understand why he needs to retire while relaxing at his ranch with his corporation-provided concubine. He gives advice to a group of up-and-coming Rollerball players, emphasizing the importance of skill and technique. Later, he tries to access some books from a library, but to his disappointment, he finds that the books have been classified, transcribed, and stored in one of the major corporate computer banks. Jonathan comforts himself at his ranch by watching a video of his ex-wife, and finds that the corporation has sent him another concubine, Daphne.
 John Beck) in a persistent vegetative state and brain-dead. Jonathan defies a doctor in the Tokyo hospital, and insists on keeping Moonpie on life support and transporting him elsewhere to receive medical care.
 New York team will be played without penalties, player substitutions, or a time-limit, in the hope that Jonathan, if he decides to participate, will be killed during the course of the game. The executivess meeting reveals why they are demanding Jonathans retirement: Rollerball was conceived not merely to satisfy mans bloodlust, but to demonstrate the futility of individualism. Jonathans singular talent and longevity in the sport defeats the intended purpose of Rollerball. Before the match, Jonathan is visited by his ex-wife, Ella, who reveals that she has a son with her new husband and that the corporations had told her to visit him in an effort to convince him to retire.

After much personal introspection, and further delving into the true nature of the corporations that run the world, Jonathan decides he is going to play in the game despite the obvious dangers. Naturally, the final game quickly loses all semblance of order as players are incapacitated or killed in short order. The crowd, raucous and energetic at the games beginning, gradually become more and more subdued as the carnage builds and degrades to a gladiatorial "last man standing" event.

In the end, Jonathan is the last player on the Houston team. Two players remain from New York. After a violent struggle, Jonathan dispatches one of the players, gets the ball and grabs the last, helpless New York player. He looks like he is about to kill the final player as the world watches in complete silence.

With a moments pause, Jonathan releases his opponent, slowly gets to his feet, and painfully makes his way to the goal, scoring the only point of the game, leaving the final score Houston 1, New York 0.

Immediately following this Jonathan then starts to freely skate around the track in silent victory, and the coaches and fans of both teams start chanting, "Jon-a-than!" They do so first in a whisper, and then their voices gradually grow louder and louder as Jonathan continues to circle the track.

Seeing his worst fears unfolding, Mr. Bartholomew hurries to exit the arena in blind panic, with the realization that Jonathan has essentially defeated the purpose of the game itself. As the cheering reaches a climax, the movie cuts to a sudden still of Jonathan, against the same music that opened the film, the Toccata from Bachs iconic Toccata and Fugue in D minor.

==Cast==
*James Caan as Jonathan E.
*John Houseman as Mr. Bartholomew
*Maud Adams as Ella John Beck as Moonpie
*Moses Gunn as Cletus
*Pamela Hensley as Mackie
*Barbara Trentham as Daphne
*John Normington as Executive
*Shane Rimmer as Rusty, Team Executive
*Burt Kwouk as Japanese Doctor
*Nancy Bleier as Girl In Library
*Richard LeParmentier as Bartholomews Aide
*Robert Ito as Strategy Coach for Houston Team
*Ralph Richardson as Librarian

==Filming locations==
 
Among the filming locations used was the Rudi-Sedlmayer-Halle as arena, the then-new BMW Headquarters and Museum buildings in Munich, Germany, appearing as the headquarters buildings of Energy Corporation and at the Olympiapark, Munich.
A number of scenes were also filmed at Fawley Power Station, near Southampton (United Kingdom|UK).

==Music==
  Largo from Symphony No. 5.

==Reception==
Variety (magazine)|Variety praised the film, calling the lead performances "uniformly tops." 

By contrast, Vincent Canby was unimpressed, and his review sneered: 
 "All science-fiction can be roughly divided into two types of nightmares. In the first the world has gone through a nuclear holocaust and civilization has reverted to a neo-Stone Age. In the second, of which  Rollerball  is an elaborate and very silly example, all of mankinds problems have been solved but at the terrible price of individual freedom.... The only way science-fiction of this sort makes sense is as a comment on the society for which its intended, and the only way  Rollerball  would have made sense is a satire of our national preoccupation with televised professional sports, particularly weekend football.  Yet  Rollerball  isnt a satire. Its not funny at all and, not being funny, it becomes, instead, frivolous." 

TV Guide gave the film 3 out of 4 stars, saying that "the performances of Caan and Richardson are excellent, and the rollerball sequences are fast-paced and interesting."  James Rocchi of Netflix said in his review that "the combination of Roman Empire-styled decadence and violence mixed with a vision of a bizarre, loveless corporate future is evocative and unsettling." 
 Time Magazine posted a negative review of the film, saying that Caan looked "unconvinced and uncomfortable" as Jonathan E. 

The film currently has a 68% approval rating on Rotten Tomatoes.

American Film Institute lists
*AFIs 100 Years...100 Thrills – Nominated 
*AFIs 100 Years...100 Cheers – Nominated 
*AFIs 10 Top 10 – Nominated Science Fiction Film 
In 1977, James Caan rated the film 8 out of 10 saying he "couldnt do much with the character." James Caans career hitting tough times
Siskel, Gene. Chicago Tribune (1963-Current file)   November 27, 1977: e6. 

==Historical significance==
Recognizing their contribution to the films many crucial action sequences, Rollerball was the first major Hollywood production to give screen credit to its stunt performers. 

==In popular culture==
* IJK Software based its Commodore 64 game Rocketball (1985) on Rollerball.
* In the cyberpunk manga Battle Angel Alita, "Motorball" is a popular, bloody sport based on Rollerball.

==Remake== 2002 remake Chris Klein, Jean Reno, and LL Cool J. The remake received universally negative responses from critics; its approval rating on Rotten Tomatoes is 3%. 

==See also==
* Death Race 2000, a dystopian science-fiction sports film released two months before Rollerball. 
* Futuresport, a 1998 TV-movie with a similar premise.
* Rollerball (2002 film)|Rollerball, a 2002 remake.

==References==
 

==External links==
*  
*  
*  
*   at japenet.net &ndash; A Summary of Rules of Rollerball from Book to Films.

 
 
{{succession box |
  | before = Soylent Green
  | after = Logans Run (1976 film)|Logans Run
  | title = Saturn Award for Best Science Fiction Film
  | years = 1974/75
|}}
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 