Pained
{{Infobox film name           = Pained  image          = Pained2011Poster.jpg caption        = Film poster director       = Kwak Kyung-taek  producer       = Lee Ji-seung screenplay     = Han Soo-ryun  story          = Kang Full starring       = Kwon Sang-woo Jung Ryeo-won  music          = Kim Jun-seong cinematography = Hwang Ki-seok  editing        = Kim Jae-beom distributor    = Lotte Entertainment  released       =   runtime        = 104 minutes country        = South Korea language       = Korean budget         =  gross          =    film name      = {{Film name hangul         =   hanja          =   rr             = Tong jeung  mr             = T‘ong jŭng}}
}}
Pained ( ; lit. "Pain") is a 2011 South Korean film directed by Kwak Kyung-taek.    Kwaks first feature in three years, it is a romantic melodrama set in Seoul, which is a departure from the directors previous Busan-based masculine thrillers and gangster movies. It is adapted from an original story by popular webcomic artist Kang Full. 

==Plot==
Debt collector Nam-soon (Kwon Sang-woo) lost his sense of pain after a traumatic accident during his youth, and now regularly takes beatings for his job. Street vendor Dong-hyun (Jung Ryeo-won) suffers from severe hemophilia, a disorder that impedes the bodys ability to stop bleeding. For Dong-hyun, even the most minor of injuries could be deadly. Shes left homeless after Nam-soon collects the last of her money, so he decides to take her in. As the two grow closer, Nam-soon suddenly begins to lose his lifelong insensitivity to pain and the hurt of a lifetime washes over him. Together, these two lonely souls learn to hurt and hope again...

==Cast==
*Kwon Sang-woo - Nam-soon
*Jung Ryeo-won - Dong-hyun
*Ma Dong-seok - Bum-No
*Jang Young-nam - Kye-Jung
*Kim Hyeong-jong - jobless guy
*Keum Dong-hyun - Young-Bae
*Lee Mi-do - street vendor
*Mahbub Alam - street vendor
*Song Bong-geun - bucktooth
*Oh Joo-hee - Nam-Soons aunt
*Kwak Min-seok - newlywed husband
*Kim Yeon-ah - newlywed wife
*Kim Jin-goo - grandmother owner
*Sa-hee - actress
*Kwon Oh-jin - trader representative
*Kim Min-joon - male actor (cameo)

==References==
 

==External links==
*  
*   at HanCinema

 
 
 
 
 
 
 
 
 
 
 
 


 
 