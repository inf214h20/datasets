Heads I Win, Tails You Lose
 
{{Infobox film
| name = Heads I Win, Tails You Lose
| image = Testa o croce lrg.jpg
| caption =
| director = Nanni Loy
| writer =
| starring = Nino Manfredi Renato Pozzetto
| music = Carlo Rustichelli Paolo Rustichelli 
| cinematography = Claudio Cirillo
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 1982 Cinema Italian comedy film written and directed by Nanni Loy. 

The film consists in two back-to-back stories that deals with two "taboo" themes, the celibacy of the clergy in the episode of Renato Pozzetto and the homosexuality in the one with Nino Manfredi.  

== Cast == 
===La pecorella smarrita===
*Renato Pozzetto: Father Remigio
*Mara Venier: Teresa
*Mario Cei: Don Ugo

===Il figlio del Beduino===
*Nino Manfredi: Beduino
*Paolo Stoppa: The grandfather
*Leo Gullotta: Walter
*Ida Di Benedetto: Stefania
*Maurizio Micheli: Doctor 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 