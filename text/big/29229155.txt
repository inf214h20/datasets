Parkway Drive: The DVD
 
 

 

{{Infobox Film |
  name          = Parkway Drive: The DVD |
  image         =  |
  distributor   = Epitaph Records |
  director      = Ben Gordon, Dylan Etherington |
  producer      = Ben Gordon |
  editing       = Ben Gordon |
  released      = 22 September 2009 |
  runtime       = 77 min. |
}}

Parkway Drive: The DVD is a documentary film about the Australian metalcore group, Parkway Drive. It was released on 22 September 2009. It is centered on the formation and beginnings of the band, their record label signings and how they managed to continuously tour from country to country with little time in between. The DVD went platinum in Australia and earned numerous awards in their home country.

==Content==

===The DVD===
* In the Beginning
* Coming Together
* First Show
* Making Progress
* The Truth
* KWAS
* Uncharted Waters
* Other Side of the World
* The Chode
* (Ten) Things Get Ugly
* Only the Best Hotels
* California Dreamin
* On the Road U$A
* Epitaph
* Ups and Downs
* Horizons
* The New Europe
* Around the World
* Sweatfest
* A Different Point of View
* 12/12/08
* Home Sweet Home
* Rolling Dice

===Live Set===
# "Boneyards"
# "Gimme A D"
# "Idols and Anchors"
# "Carrion"
# "Guns for Show, Knives for a Pro"
# "The Siren Song"
# "Mutiny"
# "Feed Them to the Pigs"
# "Dead Mans Chest"
# "Smoke Em If Ya Got Em"
# "Romance Is Dead"

===Bonus Content===
*Bonus footage

 

 
 
 
 
 
 


 

 