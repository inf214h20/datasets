Mean Tricks
 
{{Infobox film
| name           = Mean Tricks
| image          = 
| image_size     = 
| caption        = 
| director       = Umberto Lenzi
| producer       = Ezio Palaggi
| story          = Vittorio M. Testa
| screenplay     = Vittorio M. Tesa Steven Luotto Antonio Miglietta Charles Napier Stefano Sabelli
| music          = Franco Micalizzi
| cinematography = Marco Onorato	 
| editing        = Alberto Moriani
| distributor    = Rete Italia Trinidad Film
| released       =  
| runtime        = 
| country        = Italy
| language       = English Italian
| budget         = 
| gross          = 
}}
 Charles Napier and Stefano Sabelli.

A recently retired FBI agent (Hornsby) goes to South America to find his old partner because rumors are that the old partner has become a criminal.  The former partner is killed. Hornsby alters the scene to make it appear his partner killed the gunman himself.

The rest of the movie follows Hornbys character as he tries to uncover the man and motive behind the killing of his partner.

==Cast== Charles Napier as Brian Hornsby 
* Stefano Sabelli as Rodriguez 
* Iris Peynado David Brandon as Jimmy Gandelman / Cobra 
* Bettina Giovannini  
* Stelio Candelli
* Salvatore Lago 
* David Warbeck as Frank Mendoza 
* Italo Clemente  
* Marco Felicioli 
* Enzo Frattari
* Marco Onorato 
* Riccardo Petrazzi 
* Elena Wiedermann 
* Roberto Ricci  
* Marcello Tallone
* Emy Valentino
* Doris Susanna Vonthury

==External links==
*  

 
 
 


 
 