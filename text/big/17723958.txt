Beauty for Sale
{{Infobox film
| name           = Beauty for Sale
| image          =
| image_size     =
| caption        =
| director       = Richard Boleslawski
| producer       = Lucien Hubbard
| writer         = Faith Baldwin (novel) Eve Greene Zelda Sears
| starring       = Madge Evans Alice Brady Otto Kruger
| music          =
| cinematography =
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 87 minutes
| country        = United States English
| budget         =
| gross          =
}}
Beauty for Sale is a 1933 film about the romantic entanglements of three beauty salon employees. It stars Madge Evans, Alice Brady, and Otto Kruger. It was based on the novel Beauty by Faith Baldwin.   

==Plot==
Small town girl Letty Lawson (Madge Evans) moves to New York City and lives in a boarding house run by Mrs. Merrick (May Robson). Eventually she asks her friend and Mrs. Merricks daughter, Carol (Una Merkel), to get her a job at her workplace, an exclusive beauty salon owned by Madame Sonia Barton (Hedda Hopper). Though both Carol and her brother Bill (Edward J. Nugent), who is in love with her, warn her that it is not a fit place for a young woman of good character, Letty insists she knows what she is getting into.

After proving herself, Letty is sent on a house call to attend to spoiled, scatterbrained, chatty Mrs. Smallwood (Alice Brady). When she leaves, she discovers her hat has been chewed up by Mrs. Smallwoods Pekingese. Lawyer Mr. Smallwood (Otto Kruger) returns home and buys her an expensive replacement. By chance, she meets him again when they both seek shelter from a rainstorm in the same place. Smallwood is delighted when a fear of lightning makes Letty reflexively seek the comfort of his arms several times. They start seeing each other, though nothing very improper occurs.

Meanwhile, Carol has a rich, older, indulgent boyfriend, Freddy Gordon (Charley Grapewin), while Jane, another salon employee, is secretly seeing Burt (Phillips Holmes), Madame Sonias mining engineer son.

Finally, Sherwood asks Letty to take the next step in their relationship. She asks for a week to think it over.

Carol convinces Freddy to take her along on his business trip to Paris. While seeing her off aboard the ocean liner, Letty runs into the Bartons. When Letty later mentions that Burt is leaving on the same ship as Carol, Jane becomes very upset. It turns out that Burt had promised to marry her the next day after she told him she was pregnant. Though Letty tries to comfort her, late that night Jane leaps from her window to her death.

Influenced by the examples of both Jane and Carol (after her first and only love turned out to be a married man who eventually went back to his wife, she became calculating and cynical), Letty turns Sherwood down. Then, she reluctantly agrees to marry Bill.

Specifically requested by Mrs. Sherwood, Letty is forced by Madame Sonia to go to her home. When her client notices her engagement ring, she reveals that she is getting married soon. Mr. Sherwood coolly congratulates her. However, on the wedding day, she cannot go through with it.

The next day, Mrs. Sherwood asks her husband for a divorce so she can marry Robert Abbott, the architect of the new country mansion she had commissioned. She tells him that she will ask for no alimony, as she is independently wealthy. Sherwood is furious, as it is after Lettys supposed wedding, but is quite willing to let his wife go.

Carol, having finally gotten Freddy to propose, goes house hunting. The real estate agent takes them to see the Sherwood mansion. When he reveals that it is being sold because the couple are divorcing, Letty rushes over to the real estate office to stop the sale and be reunited with her love.

==Cast==
*Madge Evans as Letty Lawson
*Alice Brady as Mrs. Henrietta Sherwood
*Otto Kruger as Mr. Sherwood
*Una Merkel as Carol Merrick
*May Robson as Mrs. Merrick
*Phillips Holmes as Burt Barton
*Edward J. Nugent as Bill Merrick
*Hedda Hopper as Madame Sonia Barton
*Florine McKinney as Jane
*Isabel Jewell as Hortense, the salon manager
*Louise Carter as Mrs. Lawson John Roche as Robert Abbott
*Charley Grapewin as Freddy Gordon

==Reception==
The New York Times reviewer had a mixed reaction, calling Beauty for Sale "a strange composite of good and bad."  "The story is reminiscent of so many others", but "the cast works miracles and Richard Boleslavsky, the director, has displayed considerable acumen" so that "at times, therefore, one is happily deluded into the feeling that the picture has freshness and a certain originality." 

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 