We're in the Money (film)
{{Infobox film
| name           = Were in the Money
| caption        = Theatrical poster
| starring       = Joan Blondell Glenda Farrell
| director       = Ray Enright
| writer         = F. Hugh Herbert Brown Holmes Erwin S. Gelsey (adaptation)
| story          = George Bilson
| distributor    = Warner Brothers
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
}} 

Were in the Money is a romantic comedy film released by Warner Bros. Pictures  in 1935. The movie stars Joan Blondell and Glenda Farrell, one of the six movies in which they were paired informally as an comedy team.  Here they play two tricky women who serve legal papers to a playboy, a racketeer, a wrestler, and a singer.

==Plot==
Ginger Stewart (Joan Blondell) and Dixie Tilton (Glenda Farrell) are offered $1000 by ditsy lawyer Homer Bronson (Hugh Herbert) to serve subpoenas on reluctant witnesses for a breach of promise lawsuit brought by Claire LeClaire (Anita Kelly) against wealthy C. Richard Courtney (Ross Alexander). They have a deadline, as a new state law will take effect in a few weeks banning such suits. Unbeknownst to Ginger, she already knows the defendant; she and Courtney, masquerading as a chauffeur named Carter, have fallen in love. Courtney himself does not know that Ginger is a process server.
 Phil Regan), gangster "Butch" Gonzola (Lionel Stander), and professional wrestler Man Mountain Dean (playing himself), the last in the middle of a bout with Chief Pontiac. Courtney, on the advice of his lawyer, Stephen Dinsmore (Henry ONeill), prepares to sail away to safety on his yacht. However, Ginger jumps out of a motorboat piloted by the erratic Bronson and pretends to be in distress. She is rescued by Courtneys crewmen. She and Courtney finally learn each others true identity, but eventually admit they love each other and decide to get married. Ginger sends a message to Dixie, asking her to bring a few things she will need for the honeymoon. However, Dixie assumes her partner is merely luring Courtney in, and when the couple set foot on the dock, Dixie serves the last subpoena. Courtney also assumes Ginger was merely acting and angrily breaks up with her.

At the trial, Bronson produces a photograph showing LeClaire cosily nestled in Courtneys lap. Courtney agrees to marry LeClaire. Later, however, Bronson confides to Ginger and Dixie that he faked the picture by combining two others. Ginger rushes over and stops the wedding ceremony just in time. She and Courtney then reconcile.

==Cast==
*Joan Blondell as Ginger Stewart
*Glenda Farrell as Dixie Tilton
*Hugh Herbert as Homer Bronson
*Ross Alexander as C. Richard Courtney, aka Carter
*Hobart Cavanaugh as Max Phil Regan as Phil Logan
*Anita Kerry as Claire LeClaire
*Henry ONeill as Stephen Dinsmore
*E. E. Clive as Jevons, Courtneys butler
*Edward Gargan as Clancy ORourke, a policeman whom Dixie uses to fend off Gonzolas wrath
*Lionel Stander as Leonidus Giovanni "Butch" Gonzola
*Man Mountain Dean as Man Mountain Dean
*Chief Little Wolf as Chief Pontiac (as Myron Cox)

== Songs == 
* The Gold Diggers Song (Were in the Money) (1933, background and sometimes hummed by cast)  
* Its So Nice Seeing You Again (1935)  

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 