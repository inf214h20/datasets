Petőfi '73
{{Infobox film
| name           = Petőfi 73
| image          = 
| caption        = 
| director       = Ferenc Kardos
| producer       = István Nemeskürty
| writer         = Ferenc Kardos István Kardos Sándor Petőfi
| starring       = Mihály Kovács
| music          = 
| cinematography = János Kende
| editing        = János Rózsa
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Petőfi 73 is a 1973 Hungarian drama film directed by Ferenc Kardos. It was entered into the 1973 Cannes Film Festival.   

== Cast ==

* Mihály Kovács - Petõfi Sándor
* Nóra Kovács - Szendrey júlia
* Can Togay - Kossuth Lajos
* Tibor Csizmadia - Vasvári Pál
* Csaba Oszkay - Madarász László
* Péter Szuhay - Görgey Artúr
* Attila Köhalmi - Jókai Mór
* Miklós Donik - Orlay Petrik Soma
* Tibor Spáda - Bem József
* Péter Blaskó - Klapka György
* János Marosvölgyi - Dembinszky
* Zoltán Fábián - Damjanich János
* Kata Kánya
* Péter Fried

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 