Little England (film)
 
{{Infobox film
| name           = Little England
| image          = Mikra Anglia poster.jpg
| caption        = Theatrical release poster
| film name      = Μικρά Αγγλία
| director       = Pantelis Voulgaris
| producer       = Giannis Iakovidis
| screenplay     = Ioanna Karystiani
| based on       =  
| starring       = Pinelopi Tsilika Sofia Kokkali Aneza Papadopoulou Andreas Konstantinou Maximos Moumouris Vasilis Vasilakis Christos Kalavrouzos
| music          = Katerina Polemi
| cinematography = Simos Sarketzis
| editing        = Takis Giannopoulos
| studio         = Mikra Anglia Black Orange OTE TV
| distributor    = Feelgood Entertainment
| released       =  
| runtime        = 160 minutes
| country        = Greece
| language       = Greek
| budget         = 
| gross          = $3,078,029
}} period Drama romance film directed by Pantelis Voulgaris. The film is based on the novel of the same name by Ioanna Karystiani, Voulgaris wife, and stars Pinelopi Tsilika, Sofia Kokkali, Aneza Papadopoulou and Andreas Konstantinou. The plot revolves around two sisters, Orsa and Moscha from the island of Andros, dubbed Little England because of its affluence, who are both in love with Spyros; it starts in the interwar period and ends in the 1950s.
 Best Feature Best Director Best Actress, Best Foreign Best Foreign Language Film at the 87th Academy Awards, but it was not nominated.

==Plot==
The action takes place on the island of Andros, where Orsa, 20 years old, and her younger sister, Moscha, live. Orsa is deeply in love with Spyros Matabes, a lieutenant, but she has never revealed her secret to anybody. On the other hand, Moscha dreams of leaving Andros and escaping womens fate of marrying sailors, who are usually away from their families. Their mother, Mina, who is married to a captain, considers love to be a trouble and, overriding their emotions, wants her daughters to make wealthy marriages. As a result, Orsa gets married to captain Nikos Vatokouzis, and Moscha to Spyros Matabes, a captain now with whom her sister is in love. The two women live in the familys duplex home and the forbidden love will harm their lives.

==Cast==
* Pinelopi Tsilika as Orsa Saltaferou
* Sofia Kokkali as Moscha Saltaferou
* Aneza Papadopoulou as Mina Saltaferou 
* Andreas Konstantinou as Spyros Maltabes
* Maximos Moumouris as Nikos Vatokouzis
* Vasilis Vasilakis as Savvas Saltaferos
* Christos Kalavrouzos as uncle Aimilios

==Release==
The film was released in Greece on 5 December 2013. It was also screened twice as closing film at the 36th Cairo International Film Festival on 17 and 18 November 2014. 

==Reception==
===Box office===
Little England was a major hit in Greece, where it grossed $3,078,029, making it the second-highest grossing film of 2013 in the country behind only  . The film grossed $537,314 in its opening weekend and topped the box office, while it stayed in top four in the following six weekends.

===Critical response===
Boyd van Hoeij of The Hollywood Reporter describes the film as "A womans picture in the best sense of the word" and a "handsomely mounted and impeccably acted film." 

===Accolades=== Best Feature Greek entry Best Foreign Language Film at the 87th Academy Awards.   

 
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| rowspan="13" | Hellenic Film Academy Awards  
| rowspan="13" | 14 April 2014
| Best Film
| Little England
|  
|-
| Best Screenplay
| Ioanna Karystiani
|  
|- Best Actress
| Pinelopi Tsilika
|  
|-
| Sofia Kokkali
|  
|-
| Best Supporting Actor
| Christos Kalavrouzos 
|  
|-
| Best Cinematography
| Simos Sarketzis 
|  
|-
| Best Editing
| Takis Giannopoulos
|  
|-
| Best Music
| Katerina Polemi
|  
|-
| Best Scenography
| Antonis Daglidis 
|  
|-
| Best Costume Design
| Gioula Zoiopoulou
|  
|-
| Best Sound
| Stefanos Efthymiou, Kostas Varybobiotis and Takis Giannopoulos
|  
|-
| Best Make-up
| Evi Zafiropoulou
|  
|-
| Best Special Effects and Cinematic Innovation
| Antonis Kotzias and Antonis Nikolaou
|  
|-
| Satellite Awards  15 February 2015 Best Foreign Language Film
| Little England
|  
|-
| rowspan="3" | Shanghai International Film Festival  22 June 2014 Best Feature Film
| Little England
|  
|- Best Director
| Pantelis Voulgaris
|  
|- Best Actress
| Pinelopi Tsilika
|  
|}

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 
 