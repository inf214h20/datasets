Scarecrows (1988 film)
 
{{Infobox Film
| name           = Scarecrows
| image          = Scarecrows.jpg
| image_size     = 
| caption        = 
| director       = William Wesley
| producer       = Ted Vernon William Wesley Cami Winikoff 
| writer         = William Wesley Richard Jefferies
| story         = William Wesley       
| narrator       = 
| starring       = Ted Vernon Michael David Simms Richard Vidan Kristina Sanborn Victoria Christian David James Campbell B.J. Turner
| music          = Terry Plumeri 
| cinematography = Peter Deming
| editing        = William Wesley 
| distributor    = Manson International Pictures 
| released       = September 28, 1988
| runtime        = 83 min.
| country        = U.S.A. English
| budget         =  $425,000 (estimated)
| preceded_by    = 
| followed_by    = 
}} 1988 horror hijacked a plane. After one of their number escapes with the money, they land near a farmhouse surrounded by scarecrows. As the group searches the scarecrow-infested grounds and are picked off one by one by an unseen assailant, they come to realize that something evil lurks  on the grounds... 

==Plot==
 
Scarecrows is about five bank robbing paramilitary mercenaries and war criminals who steal three million dollars from Camp Pendleton and take two hostages, a pilot and his daughter. As the robbers fly towards Southern waters, one of the robbers steals the loot and parachutes into a dark field. The remaining robbers land the plane and head for a broken-down house. The house has a demonic history, which causes scarecrows guarding surrounding graves to resurrect and slaughter all trespassers, dooming their victims to live on as scarecrows for all eternity.
==Cast==
* Ted Vernon as Corbin

* Michael David Simms as Curry

* Richard Vidan as Jack

* Kristina Sanborn as Roxanne

* Victoria Christian as Kellie

* David James Campbell as Al

* B.J. Turner as Bert

==Production==
 
Filming took place in Davie, Florida. 
Cinematography was done by Peter Deming, Deming would later go on to work on films like Hellraiser and Evil Dead 2.   

==Release==
The film was released on VHS by Forum Home Video in 1988.  The company released the film in both R-rated and unrated versions.  

The film was released on DVD on September 11, 2007 by MGM/Fox.   The DVD is unrated and uncut in a 1.85 anamorphic widescreen presentation with no special features. On 2 June 2015 Scream Factory set the Blu-ray Disc premiere of Scarecrows. 

==Reception==
Critical reception for the film has been mixed.

Allmovie gave the film a positive review stating, "Tightly paced and consistently tense, this low-budget film has slick production values (making it appear more expensive than it probably was) and only falters in some laughably overwrought performances".  Time Out gave the film a mostly positive stating in its review for the film, "Although a little slow to get started, this better-than-average horror movie makes excellent use of its creepily-lit monsters, is reasonably well put together, and features some stomach-turning grisliness". 
Steve Barton from Dread Central awarded the film a score of 3.5 / 5 stating, "As fun as it is over-the-top violent, Scarecrows is the perfect fit for viewers looking to strap in for some good old fashioned mindless mayhem". 

Ian Jane from DVD Talk gave the film a mixed review saying, "While the movie looks and sounds okay and is presented here in its uncut form, it hasnt aged all that well and the barebones presentation doesnt help anything. Scarecrows has got some solid gore and will definitely provide children of the 80s with a fun sense of nostalgia, but its not a great film". 

==References==
 

==External links==
*  
*  
*  
*  
 
 
 
 
 
 
 
 
 
 



 