Jeevikkan Marannupoya Sthree
{{Infobox film 
| name           = Jeevikkan Marannupoya Sthree
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = KSR Moorthy
| writer         = Vettor Raman Nair Thoppil Bhasi (dialogues)
| screenplay     = KS Sethumadhavan
| starring       = Sheela KPAC Lalitha Mohan Sharma Sankaradi
| music          = M. S. Viswanathan
| cinematography = Balu Mahendra
| editing        = TR Sreenivasalu
| studio         = Chithrakalakendram
| distributor    = Chithrakalakendram
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan and produced by KSR Moorthy. The film stars Sheela, KPAC Lalitha, Mohan Sharma and Sankaradi in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Sheela
*KPAC Lalitha
*Mohan Sharma
*Sankaradi Baby Sumathi
*Bahadoor Kanchana
*MG Soman
*Vijayasree

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ashtapadiyile || P Jayachandran || Vayalar Ramavarma || 
|-
| 2 || Brahmanandini || K. J. Yesudas, B Vasantha || Vayalar Ramavarma || 
|-
| 3 || Maalineethadame || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Shilpi Devashilpi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Veena poove || S Janaki || Vayalar Ramavarma || 
|-
| 6 || Veena poove || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 