Kawashima Yoshiko (film)
 
 
{{Infobox film
| name           = Kawashima Yoshiko
| image          = KawashimaYoshiko.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 川島芳子
| simplified     = 川岛芳子
| pinyin         = Chuān Dǎo Fāng Zǐ
| jyutping       = Cyun1 Dou2 Fong1 Zi2 }}
| director       = Eddie Fong
| producer       = Teddy Robin
| writer         = 
| screenplay     = Lilian Lee
| story          = 
| based on       = 
| narrator       = 
| starring       = Anita Mui Andy Lau Patrick Tse Derek Yee
| music          = Jim Sam
| cinematography = Jingle Ma
| editing        = Henry Cheung
| studio         = Paragon Films Golden Harvest
| released       =  
| runtime        = 105 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$11,798,844
}}
 1990 Cinema Hong Kong historical drama film directed by Eddie Fong based on the life of Yoshiko Kawashima, a Manchu princess who was brought up as a Japanese and served as a spy in the service of the Japanese Kwantung Army and Manchukuo during the Second World War. Film stars Anita Mui as Kawashima, Andy Lau, Patrick Tse and Derek Yee.

==Plot==
Yoshiko Kawashima is the 14th princess of Prince Su of the Qing Dynasty. In order to revive the Qing culture, Su sent Yoshiko to Japan to be trained as a spy by Naniwa Kawashima, who also deprived Yoshiko of her virginity. Yoshiko was ordered to marry a Mongolian prince, although the marriage was a failure. After breaking with Naniwa, Yoshiko went to Shanghai and, with her beauty and leverage, she attaches herself to Japanese general Tanaka Takayoshi and helps Emperor Puyi establish Manchukuo in Changchun. She becomes a commander of a Manchukuo Army unit and a Japanese spy, seeking revenge on revolutionaries. But upon seeing her former lover Cloud, who became a revolutionary, being arrested for assassinating Takayoshi, Yoshiko has a change of mind. But a crisis will ensue.

==Cast==
*Anita Mui as Yoshiko Kawashima / Kam Pik Fai
*Andy Lau as Fook / Cloud
*Patrick Tse as Commander Tanaka Takayoshi
*Derek Yee as Amakasu Masahiko/ Wong Ka Hung
*Idy Chan as Empress Wan Jung
*Lawrence Ng as Lam
*Matthew Wong as Ganzhuerzhabu
*Ken Lo as Tanakaa aide
*Pau Fong as Advocate Lee (Yoshikos lawyer)
*Kam Piu as Prosecutor at Yoshikos trial
*Tin Ching as Judge at Yoshikos trial
*Sze Mei Yee as Emperor Puyi
*Blackie Ko
*Wai Ching
*Chow Suk Yee as Chizuko (Yoshikos aide)
*Ho Chi Moon as Yoshikos party guest

==Box office==
The film grossed HK$11,798,844 at the Hong Kong box office during its theatrical run from 28 July to 23 August 1990 in Hong Kong.

==Award nominations==
*12th Hong Kong Film Awards Best Supporting Actor (Derek Yee) Best Cinematography (Jingle Ma)
**Nominated: Best Art Direction (Eddie Mok, Fang Ying)
 Golden Horse Awards
**Nominated: Best Supporting Actor (Andy Lau)

==See also==
*Andy Lau filmography
*Anita Mui filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at sogoodreviews.com

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 