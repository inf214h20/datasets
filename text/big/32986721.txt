The Woman of Ahhs: A Self-Portrait by Victoria Fleming
 
{{Infobox film
| name           = The Woman of Ahhs: A Self-Portrait by Victoria Fleming
| image          = 
| image size     = 
| caption        = 
| director       =   B. P. Paquette
| producer       =   B. P. Paquette Co-Produced by
Jason Ross Jallet
| writer         =   B. P. Paquette
| narrator       =
| starring       =   Darryl Hunter Holly O-Brien Stephanie Dixon
| music          =   Andrew David
| cinematography =   Ivan Gekoff with Etienne Boilard Giulia Frati Alain Julfayan J.R. Tellaj
| editing        = Andrew David
| studio         =   Ourson Films
| distributor    =   Ourson Films
| released       = 2008
| runtime        =   125 minutes
| country        =   Canada
| language       =   English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}	
 The Wizard of Oz, The Woman of Ahhs features the performance of original songs wherein the lyrics comment upon and propel the narrative, as do a triad of contemporary dance performances by Cirque du Soleil choreographer Debra Brown. The Woman of Ahhs is the second panel in Paquettes triptych on "the psychology of romantic love," preceded by A Year in the Death of Jack Richards (2004), and followed by The Anonymous Rudy S.. 

==Plot==
A struggling musician in Toronto, Jude Garland, 33, has developed feelings for a beautiful young woman whom he has never met, but only observed via the Internet. Disheartened, Jude is encouraged to track her down by his friend Victoria Fleming, a sly documentary filmmaker whose ulterior motives are only gradually revealed as the film progresses. His quest eventually takes him to Montreal where his friend Billy (who also has ulterior motives) introduces him to Raymonde, Jacklyn and Bertie, three musicians (who also have ulterior motives) with whom he will have three conversations about romantic love. Although clues abound that the beautiful young woman from the Internet is within his reach, Judes frustration mounts as he discovers that his reality is anything but what it seems.  . Cinema.bg. Retrieved 12 November 2011. 

==Production==
Montreal-born and L.A.-based, lead actor Darryl Hunter had to gain and lose 30 pounds during the course of the shoot. "Its a really difficult performance because it goes from being very sombre and dramatic   to charming and comedic," according to director B. P. Paquette. Hunter concurs, "It really does the extremes and everything in between. And obviously, as an actor, its a ball you get to show it all." 
 http://thesudburystar.com/PrintArticle.aspx?e=1763513  
 Concordia University. Hunter had a role in Paquettes first film A Year in the Death of Jack Richards, which picked up numerous prizes at festivals around the globe when it was released in 2004. 

==Interpretations and allusions==
The Woman of Ahhs has been described as "a weirdly insightful film about romantic love and the nature of media and consciousness itself, a kind of mockumentary with the added complication that the documentarian on-screen is not the director.  Helen Faradji, film critic, and editor-in-chief of  24 images, wrote that The Woman of Ahhs is “mysterious and bizarre   Mixing reality, documentary and fiction but also having fun scrambling all reference points to time, the film takes as its starting point the classic The Wizard of Oz, and then layers in stylistic effects, mirror-within-mirror constructions and fantastical sequences to better puzzle us while we wander through this loveably bizarre dream world.”

==Festival recognition==
Premiering at Festival du Nouveau Cinéma where it was nominated for the Grand Prix Focus, The Woman of Ahhs was an official selection at numerous film festivals around the world, including  Cinefest, the Sofia International Film Festival in Bulgaria, the European Independent Film Festival in Paris, France, where it was presented at the Bibliothèque nationale de France, and the Mexico International Film Festival where Paquette received the Bronze Palm Award.            

==Theatrical release==
A commercial release in theatres across Canada is scheduled for 2012.

==Awards==
*2005 B. P. Paquette won the Bronze Palm Award at the Mexico International Film Festival.

==References==
 

==External links==
* 

 
 
 
 
 
 