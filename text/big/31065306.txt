Heaven with a Barbed Wire Fence
{{Infobox film name       = Heaven with a Barbed Wire Fence image      = caption    =  director   = Ricardo Cortez producer   =  writer     = Dalton Trumbo music =  Samuel Kaylin  cinematography  =  editing =  starring   = Glenn Ford Ward Bond Jean Rogers
|distributor=  Columbia Pictures released   =   runtime    =  62 minutes country   = United States language   = English
|}}
Heaven with a Barbed Wire Fence is a 1939 drama film starring Glenn Ford and directed by Ricardo Cortez

==Plot==
Joe Riley (Glenn Ford) has worked six long years in New York City to save enough money to buy a piece of land in Arizona. Unable to buy a car (because he spent all his money on the property), he has to travel to his new home by hitchhiking. Riley ends up traveling to Arizona along with a homeless man and a local female immigrant.

==Cast==
* Glenn Ford as Joe Riley
* Jean Rogers as Anita Santos
* Ward Bond as Hunk
* Marjorie Rambeau as Mamie
* Irving Bacon as Sheriff Clem Diggers
* Richard Conte as Tony Casselli Eddie Collins as Bill
* Kay Linaker as Nurse

==References==
* Motion picture review digest, Vol 4-5, by H.W. Wilson Co.,

==External links==
*  
*  

 
 
 
 
 
 
 

 