Top Gun (1955 film)
{{Infobox film
| name           = Top Gun
| image size     = 
| image	=	Top Gun FilmPoster.jpeg
| caption        = 
| director       = Ray Nazarro
| producer       = Edward Small
| writer         = 
| based on       =  William Bishop
| music          = 
| cinematography = 
| editing        = 
| distributor    = United Artists
| studio         = Fame
| released       = 1955
| runtime        = 
| country        = USA
| language       = English
| budget         = 
}}
 Western about an ex-gunslinger (Sterling Hayden) who arrives in a small town warning of an impending attack by his old gang. The film features Rod Taylor in one of his first American roles. 
==Plot==
Rick Martin arrives in a small town warning of an impending attack by his old gang.

==Cast==
*Sterling Hayden as Rick Martin William Bishop as Canby Judd
*Karin Booth as Laura Mead
*James Millican as Marshal Bat Davis
*Regis Toomey as Jim OHara
*Hugh Sanders as Ed Marsh
*John Dehner as Tom Quentin
*Rod Taylor as Lem Sutter
*Denver Pyle as Hank Spencer

==Production==
Filming started June 1955. Hayden Next Will Star in Top Gun: Cole Porter Wonderland Readying
Schallert, Edwin. Los Angeles Times (1923-Current File)   21 Mar 1955: B9.  

Female lead Karin Booth was married to the grandson of Allan Pinkerton and Small announced plans to star her as a female Pinkerton detective but no film seems to have resulted. Pinkerton Production on Hot Slate; Hired Guns Speeds Project
Schallert, Edwin. Los Angeles Times (1923-Current File)   21 June 1955: 19.  
==References==
 

==External links==
*  TCM
 
 
 
 
 
 

 