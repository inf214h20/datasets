History of the World, Part I
 
 
{{Infobox film
| name = History of the World, Part I
| image = History of the World poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Mel Brooks
| producer = Mel Brooks
| writer = Mel Brooks
| narrator = Orson Welles
| starring = Mel Brooks Dom DeLuise Madeline Kahn Harvey Korman Cloris Leachman John Morris
| cinematography = Woody Omens
| editing = John C. Howard
| distributor = 20th Century Fox
| released =  
| runtime = 92 minutes
| country = United States
| language = English Latin French
| budget = $10 million 
| gross = $31.6 million
}} King Louis XVI, and Jacques, le garçon de pisse. The large ensemble cast also features Sid Caesar, Shecky Greene, Gregory Hines (in his film debut), Charlie Callas; and Brooks regulars Dom DeLuise, Madeline Kahn, Harvey Korman, Cloris Leachman, Andreas Voutsinas and Spike Milligan.

The film also has cameo appearances by Royce D. Applegate, Bea Arthur, Hugh Hefner, John Hurt (as Jesus Christ), Barry Levinson, Jackie Mason, Paul Mazursky, Andrew Sachs and Henny Youngman, among others. Orson Welles narrates each story.
 detailed below. 

==Plot== period costume drama subgenres. The four main segments consist of stories set during the Stone Age, the Roman Empire, the Spanish Inquisition, and the French Revolution. Other intermediate skits include reenactments of the giving of the Ten Commandments and the Last Supper.

===The Stone Age===
Cavemen (including Sid Caesar) depict the invention of fire, the first marriages (the first “Homo sapiens”  marriage which was swiftly followed by the first same-sex marriage|"homosexual marriage"; note that the movie preceded the first legal recognition of same-sex marriage (in Denmark) by almost a decade), the first artist (which in turn gives rise to the first critic), and early attempts at comedy and music, by smashing each others feet with rocks and thus creating an orchestra of screams.

===The Old Testament=== TEN Commandments! For all to obey!”

===The Roman Empire=== Swiftus (Ron Ron Carey) conscripted into Las Vegas.
 abundant body corrupt ways. Josephus absentmindedly pours a jug of wine into Neros lap and is ordered to fight Comicus to the death in a gladiatorial manner. They fight their way out of the palace, assisted in their escape by Miriam, Empress Nympho and a horse named Miracle.
 marijuana which is growing alongside the road and rolls it into the papyrus, forming a device he calls Mighty Joint (cannabis)|Joint, sets fire to it and mounts it to the back of their chariot, trailing smoke into the chasing army. 

The resulting smoke confuses and incapacitates the trailing Roman army. The escaping group then sets sail from the port to Judea. While waiting tables at a restaurant, Comicus blunders into a private room where the Last Supper is taking place, as Jesus is telling the apostles "One of you has betrayed me tonight". The Apostles are asking "Who?".  Comicus says "JUDAS." Judas, startled, almost jumps out of his seat as Comicus replies "Do you want a beverage?", and interrupting 
Jesus (John Hurt) repeatedly (using his name as an expression for dismay or concern, right in front of him). Eventually, Leonardo da Vinci (Art Metrano) arrives to paint the The Last Supper (Leonardo da Vinci)|group’s portrait. Dissatisfied that he can only see the backs of half of their heads, he has them move to one side of the table and paints them with Comicus behind Jesus, holding a silver plate which doubles as aureola.   

===The Spanish Inquisition=== iron maiden and "water torture" re-imagined as an Esther Williams-style aquatic ballet with nuns. Jackie Mason and Ronny Graham have cameos  in this scene as Jewish torture victims.

===The French Revolution=== clay pigeons in a murderous (and humorous) game of Skeet shooting|skeet. A beautiful woman, Mademoiselle Rimbaud (Pamela Stephenson), asks King Louis to free her father, who has been imprisoned in the Bastille for 10 years because he said "the poor aint so bad." He agrees to the pardon under the condition that she have sex with him that night, while threatening that should she refuse, her father will die. He then gives her 10 seconds to decide between "hump or death" and at the last second she agrees to "hump".
 senile father (Spike Milligan) return from the prison, the peasants burst into the room and capture the piss-boy “king” and M lle  Rimbaud. They are taken to the guillotine for the crimes committed by the crown. When asked if he would like a blindfold or any last words, Jacques declines. However, when they test the guillotine, Jacques make a final request for Novocain. The executioner declares "there is no such thing known to medical science", to which Jacques replies "Ill wait". Just as Jacques is about to be beheaded M lle  Rimbaud muses that "only a miracle can save him now", and Josephus arrives in a cart pulled by Miracle, the horse from the films Roman Empire segment. They all escape Paris, riding away in the cart. The last shot is of the party approaching a mountain carved with the words “THE END.”

===Previews of coming attractions=== Brooks parody Sir Walter Raleigh while prisoner in the Tower of London; he had only managed to complete the first volume before being beheaded). 

==Cast==
* Orson Welles - Narrator
* Mel Brooks - Moses, Comicus, Tomas de Torquemada, Louis XVI of France and Jacques le Garçon de Pisse
* Dom DeLuise - Emperor Nero
* Madeline Kahn - Empress Nympho
* Harvey Korman - Count de Monet
* Cloris Leachman - Madame Defarge Ron Carey - Swiftus Lazarus
* Gregory Hines - Josephus
* Pamela Stephenson - Mademoiselle Rimbaud
* Spike Milligan - Monsieur Rimbaud
* Andreas Voutsinas - Béarnaise sauce|Béarnaise 
* Shecky Greene - Marcus Vindictus
* Sid Caesar - Chief Caveman
* Bea Arthur - Unemployment Insurance Clerk
* Johnny Silver - Small Liar
* Mary-Margaret Humes - Miriam
* Paul Mazursky - Roman officer
* Hugh Hefner - Roman outside Temple of Eros
* Barry Levinson - Column Salesman
* Charlie Callas - Soothsayer
* John Hurt - Jesus Christ
* Andrew Sachs - Gerard
* Diane Day - Caladonia
* Cleo Rocos - French Girl (uncredited)

==Production==
 

==Release==
===Box office===
History of the World, Part I was a box office success, taking in $31,672,907 domestically from an $11 million budget. 

===Critical reception===
 
The film currently holds a 62% Fresh rating on Rotten Tomatoes. 

==Legacy== King Richard The Producers, as a lyric in the song "The King of Broadway".

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 