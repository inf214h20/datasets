I'll Be Home for Christmas (1998 film)
{{Infobox film
| name           = Ill Be Home for Christmas
| image          = Ill be home for christmas poster.jpg  
| image_size     = 
| caption        = Theatrical release poster 
| alt            = 
| director       = Arlene Sanford
| producer       = Robin French Justis Greene David Hoberman Tracey Trench
| writer         = Michael Allin Tom Nursall Harris Goldberg
| narrator       = 
| starring       = Jonathan Taylor Thomas Jessica Biel Adam LaVorgna Gary Cole Eve Gordon Sean OBryan Andrew Lauer
| music          = John Debney
| cinematography = Hiro Narita
| editing        = Anita Brandt-Burgoyne
| studio         = Walt Disney Pictures Mandeville Films Buena Vista Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $12.2 million 
| preceded_by    = 
| followed_by    = 
}}
 Christmas Family family comedy film. It stars Jonathan Taylor Thomas, Jessica Biel, Adam LaVorgna, Gary Cole, Eve Gordon, Sean OBryan, and Andrew Lauer.

==Plot==
 
Jake Wilkinson (  to take his girlfriend Allie (Jessica Biel), who is also from New York. She considers the act selfish as Jake knows Allie always returns home for Christmas. Jake reconsiders and trades the tickets back in for New York, and she forgives him.

As Jake is leaving, he is taken captive by nemesis Eddie Taffet (Adam LaVorgna) and his trio of goons in the desert dressed as Santa Claus. The group do this as punishment for not getting an exam cheat sheet from Jake, unaware that it was Eddie who withheld the cheat sheet so he might get Jake out of the way and claim Allie for himself. While Jake is stuck in the California desert, Eddie ends up giving a reluctant Allie a ride to New York after she thinks Jake has bailed on her once again. Jake has only three days to make it to Larchmont if he wants the car and Allie.
 Mexican child reminds Jake, Nolan and Max about the importance of family. Nolan decides to return west, give up crime and return to his girlfriend, therefore abandoning Jake. Max, recently estranged from his wife, asks Jake to accompany him to North Platte, Nebraska and help him win his wife Marjorie (Lesley Boone) back. Afterwards, Max and Marjorie buy Jake a bus ticket to New York in return for helping them understand their differences.

Meanwhile, Allie and Eddie continue to head for New York. Instead of staying at a cheap roadside motel, Allie convinces Eddie to stay the night at novelty hotel in a Bavarian village, the Amana Colonies in Iowa City, Iowa|Amana, Iowa. The village is celebrating the holidays, and a television news reporter is reporting live from the village. Allie and Eddie are unknowingly standing beneath a bunch of mistletoe while the news anchor is reporting live. She asks them to kiss for the camera and as they do, Jake happens to be watching the news report and the kiss between Allie and Eddie whilst waiting at the bus station in Nebraska.

The driver on the bus is determined to stick to the schedule as it is a non-stop bus to New York. Jake wants to stop at the Bavarian village and develops a scheme. He obtains a lunch-cooler, a slab of meat from another passengers sandwich, writes Allies name and hospital address on the cooler, and then convinces the whole bus that the cooler contains a liver that needs to be delivered to a little girl at the hospital in the Bavarian Village. The bus passengers convince the driver to speed up and detour to the Bavarian village. Jake finds the village, and locates Allie and Eddies room. After Allie lets Jake in to the room, Eddie walks out of the shower in a towel and Jake assumes he had slept with Allie (he didnt, Allie made him sleep in a sleeping bag and almost all of his clothes). Jake and Allie eventually make up, until Jake blurts out that Eddie prevented him from getting home to get his dads Porsche. Upset that Jake had come to get the car and not herself (which he actually did), Allie storms onto the bus and takes Jakes seat. The driver asks is shes the girl who the organ transplant was for to which she agrees. Upset with Jakes tangle of lies, she tells him off and boards the bus.

Jake and Eddie drive off. Jake tells Eddie that he will go home, win back Allie, and get the car. Eddie, unwilling to be a nice guy (it would be bad for his reputation) and jealous that Jake would not only get Allie but also the Porsche, ends up throwing Jake out of his car somewhere in Wisconsin. Jake decides to enter a Santa Claus race for a chance to win a $1,000 prize to buy an airline ticket to New York. Eddie is arrested after being rude to two cops policing the race dressed as Christmas trees ("Hey, Jingle Balls, move your candy canes.") While registering for the race, Jake meets a nice man, Jeff Wilson, who Jake barely beats in the race. Jake wins the prize, but on his way to the airport the taxi driver informs Jake that Jeff is actually the mayor of the town. Mayor Wilson usually wins the race every year and uses the prize money to buy turkeys for the needy. Jake feels bad, and asks the cab driver to turn back. Jake leaves the money in the Mayors mailbox, but is caught in the middle of this good deed by the Mayor. Jeff offers to set an extra place for Jake, but Jake declines, saying he has somewhere to be that evening, and calls home.

Jake talks to his sister, who arranges for an airline ticket for Jake from Madison, Wisconsin. The airline refuses to allow Jake to board the aircraft because he has no photographic identification. Jake decides to stow away in a dog kennel on a cargo aircraft. From the airport, he stows away on a train, tries to hitch a ride in a car (and ends up riding on the roof), then steals a one-horse open sleigh (on wheels) from the local parade. When he reaches his street, he apologizes to Allie, and they make up. Jake and Allie ride the one-horse open sleigh to Jakes family home, and arrive at 5:59pm. Jake intentionally waits until after 6:00pm to go inside, so that he wont arrive in time to get the Porsche. When Jakes father offers him the Porsche, explaining that hes only a few seconds late, he still refuses, insisting that their time spent fixing it up as father and son isnt done yet. Jake also finally accepts his stepmother. The Wilkinsons and Allie get in the sleigh just as the parade arrives. They let what Jake did slide, based on his Santa suit.

==Cast==
* Jonathan Taylor Thomas as Jake Wilkinson
* Jessica Biel as Allie Henderson
* Adam LaVorgna as Eddie Taffet
* Gary Cole as Jakes dad
* Eve Gordon as Carolyn Wilkinson
* Lauren Maltby as Tracey Wilkinson
* Andrew Lauer as Nolan
* Sean OBryan as Officer Max
* Lesley Boone as Marjorie
== Release    ==
;U.S. TV airings ABC a Encore premium channels. Disney Channel originally aired it in 2002, but due to unresolved conflicts, as of 2007, it hasnt been shown again on that station. In 2009, the movie premiered on Hallmark Channel. In 2011, it moved to ABC Familys 25 Days of Christmas block.

;Canadian TV airings CBC every Christmas season. It is typically aired the Sunday before Christmas.

=== Box office === box office dump against its $30 million budget.  {{cite web 
| title = Ill Be Home for Christmas
| url = http://boxofficemojo.com/movies/?id=illbehomeforchristmas.htm 
| work = Box Office Mojo 
| publisher = Amazon.com 
}} 
 

=== Critical response    ===
The film received negative reviews from film critics. Rotten Tomatoes gave the film a score of 23% based on 43 reviews,  with an average rating of 4/10. The critical consensus is: "Neither parent nor child will find any merriment in this mess."

Roger Ebert described the film as Pleasantville (film)|Pleasantville made from "anti-matter", saying the film is about "People who seem to be removed from a 50s sit-com so they can spread cliches, ancient jokes, dumb plotting and empty cheerful sanitized gimmicks into our world and time." He gave the film 1/4 stars.

In one of the few positive reviews of the film, Christopher Null called the film "surprisingly engaging" and gave it 3 out of 5 stars. 

== References ==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 