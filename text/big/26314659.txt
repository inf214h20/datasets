Swing It, Sailor!
{{Infobox film
| name           = Swing It, Sailor!
| image          = Swing it, Sailor.(1938).Isabel Jewel.Wallace Ford,and,friend.jpg
| image_size     = thumb
| caption        = Swing it, Sailor.(1938).Isabel Jewell, Wallace Ford, and friend. Raymond Cannon David Diamond (producer) Gaston Glass (associate producer)  (uncredited) David Diamond (original story) Clarence Marks (screenplay) and David Diamond (screenplay)
| narrator       =
| starring       = See below
| music          = Richard Fryer
| editing        = Aaron Nibley Gene Milford
| distributor    =
| released       =
| runtime        = 57 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Raymond Cannon.

== Plot summary ==
Sailor Husky proposes marriage to every girl, in every port. He cant swim, and is ready to be discharged, from the Navy, but hes always helping his pal Pete. Pete would hate to see Husky leave, as Pete have to do all the work himself. Huskys new girl is Myrtle, who goes through men like Kleenex, trolling for a   catch. Pete tries to save his friend from her; but, is it only for himself? It looks like they are in for a fight.

== Cast ==
*Wallace Ford as Pete Kelly
*Ray Mayer as Husky Stone
*Isabel Jewell as Myrtle Montrose
*Mary Treen as Gertie Burns
*Max Hoffman Jr. as Bosn Hardy
*Cully Richards as Shamus OShay
*George Humbert as Pet Shop Proprietor Tom Kennedy as Policeman
*Alexander Leftwich as Captain
*Kenneth Harlan as First Officer
*Archie Robbins as Second Officer
*Kernan Cripps as Doctor
*Rex Lease as Interne

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 