Far skal giftes
{{Infobox film
| name           = Far skal giftes
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr.
| producer       = Henning Karmark
| writer         = Axel Frische Børge Müller
| starring       = Helge Kjærulff-Schmidt
| music          = 
| cinematography = Rudolf Frederiksen Alf Schnéevoigt	
| editing        = Marie Ejlersen
| distributor    = 
| released       = 1 September 1941
| runtime        = 92 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Far skal giftes (Father is Going to Get Married) is a 1941 Danish comedy film directed by Lau Lauritzen, Jr. and starring Helge Kjærulff-Schmidt.

==Cast==
* Helge Kjærulff-Schmidt - Professor Jacob Jensen
* Ellen Gottschalch - Husbestyrerinde Karen Frederiksen
* Berthe Qvistgaard - Else Margrethe Jensen
* Bodil Kjer - Birthe Jensen
* Poul Reichhardt - Oskar Jensen
* Maria Garland - Tante Rikke Holm
* Eigil Reimers - Harry Holm
* Edvin Tiemroth - Peter Larsen
* Ib Schønberg - Gartner Ørsager

==External links==
* 

 

 
 
 
 
 
 
 


 
 