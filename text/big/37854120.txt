Mosquita y Mari
 
{{Infobox film
| name = Mosquita y Mari
| image = 
| caption = 
| director = Aurora Guerrero
| producer = 
| writer = Aurora Guerrero
| starring = Fenessa Pineda Venecia Troncoso
| music = 
| cinematography = Magela Crosignani
| editing = 
| studio = 
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = 
}}
Mosquita y Mari is a 2012 coming-of-age film written and directed by Aurora Guerrero.   

==Plot==
When Yolanda Olveros meets her new neighbor Mari Rodriguez, all they see in each other are their differences. An only child, sheltered Yolandas sole concern is fulfilling her parents dream of a college-bound future. With her fathers recent death, street-wise Mari, the elder of two, carries the weight of her sister as their mother works to keep them above water. But despite their contrasting realities, Yolanda and Mari are soon brought together when Mari is threatened with expulsion after saving Yolanda from an incident at school. The girls forge a friendship that soon proves more complex than anticipated when the girls unexpectedly experience a sexually charged moment between them. At a loss for words, the girls ignore their moment and move on to become best friends, unaware they have set in motion an unstoppable journey of self-discovery.

As Yolanda and Maris feelings reach new depths, their inability to put words to their emotions leads to a web of unspoken jealousy, confusion, and a sudden betrayal that ultimately rattles them at their core.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 