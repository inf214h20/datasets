The Surprise of a Knight
{{Infobox film
| name           = The Surprise of a Knight
| image          = 
| caption        = 
| director       = 
| producer       = 
| writer         = "Oscar Wild" (pseudonym; the writers real name is unknown)
| starring       = Two uncredited male sexual performers, and one female non-sexual performer
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1929
| runtime        = 10 minutes
| country        = United States
| language       = Silent film
| budget         = 
| gross          = 
}}
 hardcore pornographic earliest known American pornographic film to depict exclusively homosexual intercourse. Burger, John R. One-Handed Histories: The Eroto-Politics of Gay Male Video Pornography. New York: Harrington Park Press, 1995. ISBN 1-56023-852-6 

==Background==
The first known motion pictures depicting nude men were made by Eadweard Muybridge in the 1880s and 1890s as part of his studies of human locomotion. Williams, Linda. Hard Core: Power, Pleasure, and the "Frenzy of the Visible." Expanded ed. Berkeley, Calif.: University of California Press, 1999. ISBN 0-520-21943-0 

Hardcore pornographic films (at the time, also called "stag films") first appeared in Europe in 1908. By 1920, the first hardcore homosexual sex acts were depicted in the film Le ménage moderne du Madame Butterfly. Waugh, Thomas. Hard To Imagine. New York: Columbia University Press, 1996. ISBN 0-231-09998-3  Slade, Joseph. "Bernard Natan: Frances Legendary Pornographer." Journal of Film and Video. 45:2-3 (Summer-Fall 1993). 

However, like most of the films which came after it, Le ménage moderne du Madame Butterfly only shows male-male sex acts as deviant, firmly establishes the heterosexuality of the characters, and usually depicts gay sex acts as essentially bisexual (e.g., male-male sexual contact occurs while the men are also having heterosexual intercourse).  

Most historians consider the first genuine American stag film to be A Free Ride,  produced and released in 1915.  

The illegality of hardcore pornographic films in the United States in the 1920s and 1930s means that no producer, performer or other credits for The Surprise of a Knight are identifiable. However, researchers have dated the film to at least 1929 Waugh, Thomas. "Homosociality in the Classical American Stag Film: Off-Screen, On-Screen." Porn Studies. Linda Williams, ed. Durham, N.C.: Duke University Press, 2004. ISBN 0-8223-3312-0  (although earlier estimates placed its production in 1930).   This makes The Surprise of a Knight the first known hardcore gay pornographic film in American cinematic history.  

==Plot==
The film opens with an elegantly attired "woman" with short hair as she finishes dressing for a visitor. As the "lady" completes her boudoir, she lifts her skirts to reveal a thick patch of pubic hair. At this point, an intertitle reveals that the screenwriter is "Oscar Wild" (clearly a pseudonym). 
 fellating her partner.

The "lady" then lies face-down on the sofa with her buttocks in the air. It is revealed that she has no underwear on. The gentleman caller then copulates the "lady" anally (although no penetration is actually shown). After a minute or so, the gentleman withdraws and sits back on the sofa. The "lady" gyrates her buttocks in the air. This induces him to mount her anally again. Both individuals reach orgasm, and the gentleman caller walks off-camera.
 drag then dances about briefly, making sure that his penis bobs up and down in the air. The gentleman caller re-enters the cameras view, and helps the other man remove his skirt and most of his other clothing. The gentleman caller (now completely clothed again) dances briefly with the nude young man. After a jump cut, the "lady"—now dressed completely in business attire—walks back on screen, winks at the audience, and walks off screen.

==Assessment== Caucasian man without the need for drag.  The appearance of gay sexual contact on film would soon end, however, and not reappear until the advent of legal gay hardcore pornography after 1970. 
 Linda Williams have argued The Surprise of a Knight is a film fraught with interpretational difficulties. Williams notes the lead character (the "lady") is in costume, yet costumes are the antithesis of the hardcore pornographic film (in which nudity and the display of genitalia and penetration during intercourse are key).  "The costume spectacle either steals the show..." as film historian Thomas Waugh put it, "...or becomes a grotesque distraction..."  The revelation of the "ladys" penis is not real surprise, Waugh concludes, as audiences knew what sort of film they were getting (e.g., homosexual porn).  

The use of drag in The Surprise of a Knight also distances the audience from the performers on screen, Waugh argues. The main character of the film is a drag queen, and yet nearly all the audience members could say they were not drag queens.  Waugh see the film not depicting gay men on screen, but reaffirming heteronormativity and negative stereotypes of gay men and gay sex.  John Robert Burger writes it is unclear from the film whether the visitor knows of the drag queens gender before the encounter, and hiding the gender of the drag queen makes it "faux homosexuality".    Burger also writes,The Surprise of a Knight is an exception to the norm of stag films, in which the receptive partner in same-sex anal sex is typically perceived to be victimized or punished. 

==References==
 

==External links==
*  

 
 
 
 
 
 