Nanda Loves Nanditha
{{Infobox film
| name           = Nanda Loves Nanditha
| image          = NandalovesNanditha.JPG
| caption        = Film poster
| director       = B. N. Vijayakumar
| producer       = Ramesh Kashyap
| writer         = Ajay Kumar
| screenplay     = Ajay Kumar Yogesh Nandita
| narrator       = 
| music          = Emil
| cinematography = Mathew Rajan
| editing        = K. M. Prakash
| studio         = Simhadri Productions
| distributor    = 
| released       =  
| runtime        = 141 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}

Nanda Loves Nanditha ( ) is a 2008 Indian Kannada language film directed by B. N. Vijayakumar starring Yogesh and Nandita in the lead roles.

==Cast== Yogesh as Nanda
* Nandita as Nanditha
* Avinash
* Suresh Chandra
* Suneetha Shetty
* Shobha
* Jayabalu
* Girija Lokesh

==Crew==
*Director - B.N. Vijaya Kumar
*Producer - Ramesh Kashyap
*Banner - Simhadri Productions
*Cinematography - Mathew Rajan
*Lyrics - Anand Ram, V Nagendra Prasad, Kaviraj, Tangalli Nagaraj
*Music - Emil
*Singers - 
*Art - Kumar
*Editor – Prakash
*Co-Directors – C Jagadish
*Stunts – Different Danny
*Choreography - Prasad, Rajesh

== External links ==
*  

 
 
 


 