The Boys of St. Vincent
 
{{Infobox Film
| name           = The Boys of St. Vincent
| director       = John N. Smith
| producer       = Claudio Luca
| writer         = Sam Grana John N. Smith Des Walsh
| narrator       = 
| starring       = Henry Czerny
| music          = Neil Smolar
| cinematography = Pierre Letarte
| editing        = Werner Nold
| distributor    = 
| released       = 1992
| runtime        = 90 min. (Germ.) / 186 min. (US)
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Roman Catholic Church.

The first film, The Boys of St. Vincent, covers the sexual and physical abuse of a number of orphans by Brothers headed by Brother Peter Lavin (Henry Czerny). The second film, The Boys of St. Vincent: 15 Years Later, covers the trial of the Brothers.
 cut when the film was first shown in the United States.
 takes his own life with an overdose of drugs. His death finally prompts Reevey to give evidence against Lavin.

Lavin remains in denial, even to his wife. His fate is left unanswered as is the question, posed by his wife at the end of the second film, as to whether he ever molested his own two young sons.

==See also==
*Roman Catholic sex abuse cases

==External links==
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 