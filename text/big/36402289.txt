Njai Dasima (1929 film)
{{Infobox film
| name      = Njai Dasima
| image     = Njai Dasima p103.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = Ad in Doenia Film
| director    = Lie Tek Swie
| producer    =Tan Khoen Yauw
| writer     = Tan Khoen Yauw
| starring    = {{plain list|
*Nurhani
*Anah
*Wim Lender
*Momo
}}
| music     = 
| cinematography = 
| editing    = 
| studio     = Tans Film
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Silent
| budget     = 
| gross     = 
}}
Njai Dasima (  from the Dutch East Indies (modern day Indonesia). It details the fall of a rich mistress at the hands of a greedy delman driver. The first film released by Tans Film, the film&nbsp;– adapted from an 1896 novel&nbsp;– was a critical and commercial success. It was released in two parts, followed by a sequel, and remade another two times by 1940.

==Plot==

===Part 1=== Gambir Square take a second wife as she wants Dasimas money, which she will use for gambling.

===Part 2=== story of sentenced to hang.

==Production== ethnic Chinese. native viewers, Njai Dasima was filmed from September to October 1929.  

The story was based on a novel written by G. Francis in 1896, which was advertised as based on a true story from 1813 Batavia. The story had previously been adapted by Toneel Melayu troupe. Tans often adapted the troupes work, and Njai Dasima was among their most successful.   Like most native adaptations of the novel, Njai Dasima avoided the anti-Muslim undertones present in the original work. 

Njai Dasima was directed by Lie Tek Swie and produced by Tan Khoen Yauw.  Nurhani, who played Dasima, was cast from the general populace.  Other stars included Anah, Wim Lender, and Momo.  The cinematography was handled by Andre Lupias. 

==Release and reception==
Njai Dasima was released in November 1929; it was released in two parts; the conclusion of the story was released in 1930.  It was a commercial success, to the point that the Indonesian film historian Misbach Yusa Biran writes that a cinema could make up several days losses with a single showing of the film.  Despite critical acclaim for her acting, Nurhani never played another film role. 

The film critic Kwee Tek Hoay, known for his scathing reviews, praised the visuals in Njai Dasima and the acting of Nurhani and the girl who played Nancy. However, he criticised the other actors, describing the one who played Williams as too young, the one who played Samiun as looking like a bellboy, and the one who played Mak Buyung as looking half crazy.  The magazine Doenia Film praised the filmography and acting by the lead characters.  The magazine Panorama later wrote that Njai Dasima was an important film for the Indies, as it showed that locally produced films could compete with foreign, especially American, ones. 

The film spawned a sequel, Nancy Bikin Pembalesan (Nancy Takes Revenge; 1930). The critically acclaimed sequel followed an adult Nancy in a quest to avenge her mothers death, leading to Samiun falling down a well and Hayati killing herself.  Tans Film made another adaptation of Njai Dasima (1932 film)|Njai Dasima in 1932, which Armijn Pane described as the first talkie in the Indies with good sound quality.  Yet another adaptation, entitled Dasima, was directed by Tan Tjoei Hock in 1940. 

==See also==
*List of films of the Dutch East Indies

==References==
;Footnotes
 

;Bibliography
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite web
 |title=Njai Dasima (I)
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-n011-29-352520_njai-dasima-i#.T_6yCVLE_Mw
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=12 July 2012
 |archiveurl=http://www.webcitation.org/696HNBTw0
 |archivedate=12 July 2012
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=JOqVuLrIGycC
 |title=Fetish, Recognition, Revolution
 |isbn=978-0-691-02652-7
 |author1=Siegel
 |first1=James T
 |date=1997
 |ref=harv
 |publisher=Princeton University Press
 |location=Princeton
}}
 

==External links==
* 

 

 
 
 
 
 