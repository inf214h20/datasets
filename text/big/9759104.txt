Kon-Tiki (1950 film)
 
 
{{Infobox Film
| name           = Kon-Tiki
| image          = KonTiki.jpg
| image_size     = 
| caption        = The DVD cover of the documentary
| director       = Thor Heyerdahl
| producer       = Olle Nordemar
| writer         = Thor Heyerdahl
| narrator       = 
| starring       = Thor Heyerdahl
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
 
| runtime        = 77 minutes
| country        = Norway Norwegian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Norwegian Documentary documentary about Norwegian explorer and writer Thor Heyerdahl in 1947, released in Sweden, Norway, Finland, and Denmark in 1950, followed by the United States in 1951. The movie, which was directed by Thor Heyerdahl and edited by Olle Nordemar, received the Academy Award for Best Documentary Feature for 1951 at the 24th Academy Awards. The Oscar officially went to Olle Nordemar. It is currently the only feature film from Norway to have won an Academy Award.   

== Content ==
 
The movie has an introduction explaining Heyerdahls theory, then shows diagrams and images explaining the building of the raft and its launch from Peru.  Thereafter it is film of the crew on board, shot by themselves, with commentary written by Heyerdahl and translated.  The whole film is black and white, shot on a single 16mm camera.

A small amount of color footage of Kon-Tiki does exist. 

==See also==
 
* Kon-Tiki (2012 film)|Kon-Tiki (2012 film)

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 


 
 