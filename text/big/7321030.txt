Visiting Hours
{{Infobox Film
| name           = Visiting Hours
| image          = Visiting hours.jpg
| caption        = 
| director       = Jean-Claude Lord
| producer       = Victor Solnicki Claude Héroux Pierre David
| writer         = Brian Taggert
| starring       = {{plainlist|
* Lee Grant
* William Shatner
* Michael Ironside
* Linda Purl
}} Jonathan Goldsmith
| cinematography = René Verzier
| editing        = Jean-Claude Lord
| distributor    = 20th Century Fox
| released       =  
| runtime        = 105 min
| country        = Canada English
| budget         = CAD 5,500,000 (estimated)
| gross = $6.4 million (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}}
 slasher film directed by Jean-Claude Lord and starring Michael Ironside, Lee Grant, Linda Purl, William Shatner and Lenore Zann. The plot focuses on a feminist journalist who becomes the target of a serial killer who follows her to the hospital after attacking her in her home.

==Plot==
Deborah Ballin (Grant), a feminist activist, inspires the wrath of misogynistic psychopath and serial killer Colt Hawker (Ironside) on a TV talk show. He attacks her, but she survives and is sent to County General Hospital.

Hawker begins stalking her. Deborah befriends nurse Sheila Munroe (Purl), who admires her devotion to womens rights.  Hawker murders an elderly patient and a nurse. He overhears Sheilas opinions on Deborah and "that bastard" who attacked her. Hawker decides to focus his attention on Sheila, stalking her and her children at home.

Hawker courts a young punk girl named Lisa (Zann), then brutally beats, tortures, and rapes her. The next day, Deborah discovers that the patient and nurse have been killed, so she suspects her attacker is back to finish the job.  She tries to convince her boss, Gary Baylor (Shatner), and Sheila that she is not safe, but both think she is simply paranoid.

Hawker visits his father, who was disfigured years ago by his mother, explaining his hatred for self-defending women. Soon he tries again to kill Deborah, but is thwarted by her security. A frantic Sheila is paged and finds Lisa (whose wounds she had treated) waiting for her.  Lisa says she knows the identity of Deborahs attacker, and where he lives.

Before she can alert anyone, Sheila gets an ominous phone call from Hawker, warning her that he is in her house with her young daughter and babysitter.  She sends Lisa to warn Deborah, then rushes home, only to find her daughter and babysitter safe in bed. She places a call to Deborah, but a hidden Hawker springs forth, stabs Sheila in the stomach and pushes her to the ground, phone to her ear, torturing her for Deborah to hear. He moves toward Sheilas young daughter. Sheila can only scream in terror. At the last second, he walks out, leaving Sheila to die.

Hawker goes home, where he devises one last plan to get to Deborah.  He busts a beer bottle underneath his arm, wounding himself badly. Gary and Deborah have an ambulance sent to Sheilas house.  Still alive, but badly wounded, she is rushed to the hospital.  Gary accompanies the police to Hawkers apartment, where they discover photos of Hawkers previous victims, as well as of Deborah and Sheila.  They also learn that the wounded Hawker has been taken to County General.

Just as Sheila is taken into the emergency room, Hawker is wheeled in.  After being bandaged and medicated, Hawker sneaks away to find Deborah and attacks her. She flees to an elevator. In the basement, she goes into a radiography room, finding a helpless Sheila, all alone, waiting for X-rays.

Realizing she must lure Hawker away to protect Sheila, Deborah leaves and deliberately gives her location away. As Hawker approaches the curtain she is hiding behind, Deborah stabs hims with a switchblade, killing him.  Sheila is wheeled to safety, while Gary comforts Deborah, who faints at the sight of what she has done.

===Cast===
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Michael Ironside
| Colt Hawker
|-
| Lee Grant
| Deborah Ballin
|-
| Linda Purl
| Sheila Munroe
|-
| William Shatner
| Gary Baylor
|-
| Lenore Zann
| Lisa

|-
| Harvey Atkin
| Vinnie Bradshaw
|- Michael J. Reynolds
| Porter Halstrom
|-
| Len Watt
| Clement Pine
|-
| Kirsten Bishop
| Denise
|- Robby Robinson (as Robbie Robinson)
| Matthew
|-
| Lorena Gale
| Nurse 1
|}

==Release==
The film was released theatrically in the United States by 20th Century Fox in May 1982. 
 Video Nasty" ITV in 1989,  however this resulted in censure by the Broadcasting Standards Council as this version had not been cleared for video release.
 Bad Dreams as a double feature DVD on September 13, 2011.

==External links==
* 
* 
* 
* 

==References==
 

 
 
 
 
 
 
 
 
 
 