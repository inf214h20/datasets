Ennu Swantham Janakikutty
{{Infobox film
| name           = Ennu Swantham Janakikutty
| image          = Ennu Swantham Janakikutty.jpg
| image_size     =
| caption        = Hariharan
| producer       = P. V. Gangadharan
| writer         = M. T. Vasudevan Nair
| narrator       =
| starring       = Jomol  Chanchal
| music          = Songs:   Antara Chowdhury
| cinematography = Hari Nair
| editing        = M. S. Mani
| distributor    = Kalpaka Films
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Ennu Swantham Janakikutty is a 1998 Malayalam movie written by M. T. Vasudevan Nair and directed by Hariharan (director)|Hariharan. The movie features debutant Jomol in the lead role.

The movie was produced by P. V. Gangadharan under the banner of Gruhalakshmi Productions and was distributed by Kalpaka Release
==Plot==
The movie is narrated by Janakikkutty, a ninth standard student who feels lonely and neglected by her family members, especially her elder sister Devu and cousin sister Sarojini. The elder girls spend time by grooming themselves, going to temple wearing fashionable clothes, etc. Janaki wants to be friends with them, but they always shoo her away telling to study for exams. Janaki gets a slight relief from having no one to talk with the arrival of Muthassi(Grandmother). Janaki has a crush on Bhaskaran, who is a college student and Janakis neighbor. Bhaskaran is friendly to Janaki, gives her chocolates and comic books that he brought from town. Janakis heart shatters on finding that Sarojini and Bhaskaran are in love with each other. She trips and falls near the woods which is believed to be haunted by Kunjathol, the Yakshi(ghost) of young bride killed by her unfaithful husband. She wakes up to meet Kunjathol, and her associate Karineeli, another Yakshi. The bonding of Janaki with Kunjathol and how it affects her life forms the rest of the story.

==Cast==
* Jomol as Janakikkutty
* Chanchal as Kunjathol
* Sharath as Bhaskaran
* Anoop as Kuttan, Janakis elder brother
* Rashmi Soman as Sarojini
* Chakyar Rajan
*Valsala Menon as grandmother

==Crew==
* Cinematography: Hari Nair
* Editing: M. S. Mani
* Art: Radhakrishnan
* Makeup: P. Mani
* Costumes: Natarajan
* Choreography: Shobha Geethanandan
* Lab: Prasad Colour Lab
* Stills: Ansari
* Effects: Murukesh
* Production controller: Kannan
* Outdoor: Jubilee Cini Unit

==Songs== BGM was provided by Anantha Chaudhari and Sanjay Choudhari.

;Songs list
# Paarvana paalmazha: K. S. Chithra
# Ambilipoovattam ponnuruli: K. J. Yesudas
# Chempakapoo mottinullil vasantham vannu: K. S. Chithra
# Ambilipoovattam ponnuruli: Sangeetha
# Innalathe poonilavil: Biju Narayanan
# Ambili poovum: Sangeetha
# Champakappoo mottinullil: K. J. Yesudas
# Then thulumpumormayay:

==Awards==
* National Film Award for Best Audiography - Janakikutty Sampath
* National Film Award – Special Jury Award / Special Mention (Feature Film) - Jomol
* Kerala State Film Award for Best Actress - Jomol
* Kerala State Film Award for Best Photography - Hari Nair
* Kerala State Film Award for Best Processing Lab - Prasad Colour Lab

==External links==
*  

 
 
 