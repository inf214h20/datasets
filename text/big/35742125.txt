Jeff (film)
{{Infobox film
| name           = Jeff
| image          = Jeff The Movie Poster.jpg
| alt            = 
| caption        = Official festival poster
| director       = Chris James Thompson Chris Smith Barry Poltermann Jack Turner Chris James Thompson
| writer         = Chris James Thompson Andrew Swant Joe Riepenhoff
| starring       = Andrew Swant Jeffrey Jentzen Pat Kennedy Pamela Bass
| music          = Robert Mulrennan The Knife The Books
| cinematography = Michael T. Vollmann
| editing        = Michael T. Vollmann, Chris James Thompson
| studio         = 
| distributor    = IFC Films
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Jeff (also called The Jeffrey Dahmer Files) is an independent documentary film about serial killer Jeffrey Dahmer during the summer of his arrest. The film was directed by Chris James 
Thompson and stars Andrew Swant as Dahmer in fictionalized re-enactment segments which are interwoven with interviews of the medical examiner assigned to the 
case (Jeffrey Jentzen), the lead detective (Pat Kennedy), and Dahmers next door neighbor (Pamela Bass).

The film premiered at the 2012 SXSW film festival where it received positive reviews and obtained sales representation from Josh Braun at Submarine 
Entertainment. The film also played at the Hot Docs Canadian International Documentary Festival in Toronto, the Independent Film Festival of Boston, and the Maryland Film Festival in Baltimore.

The documentary was picked up by   through IFC on February 15, 2013, with a television and DVD release to follow. 

Jeff was shot in Milwaukee, Wisconsin on Super 16 mm film over the course of three years. It was executive produced by [[Chris Smith (filmmaker)|
Chris Smith]] (director of American Movie), Barry Poltermann, and Jack Turner.

==Plot synopsis==

In 1991 Jeffrey Dahmer was arrested in Milwaukee and sentenced to 957 years in prison for murdering 17 men and boys and dismembering their bodies. Jeff explores 
the city of Milwaukee by meeting those surrounding Dahmer during and after his hidden spree. Recollections from Milwaukee Medical Examiner Jeffrey Jentzen, 
Police Detective Patrick Kennedy, and neighbor Pamela Bass are interwoven with archival footage and everyday scenes from Dahmer’s life, working collectively to 
disassemble the facade of an ordinary man leading an ordinary existence.

==Critical response==

The film received generally positive reviews, many of them praising the directors restraint. Jeannette Catsoulis of The New York Times called the film "a meditation on perversion as hypnotic as it is repulsive" and labeled it a "Critics Pick".  Mark Olson of the Los Angeles Times called the film "quietly unnerving."  

John DeFore of The Hollywood Reporter said that “Jeff stands apart from the true-crime pack" and called Swant’s portrayal of Dahmer "eerily convincing.”  John Gholson of Movies.com said “I’d go so far as to say that Jeff is one of the greatest serial killer movies ever made.” 

Owen Gleiberman, writing in a warning tone for the general audience, said it was "for hardcore Dahmer obsessives only. Through a mix of documentary footage and staged scenes, director Chris James Thompson explores the days during which Dahmer’s crimes were first discovered. Interviews with the medical examiner on the scene and the officer who first interrogated Dahmer bring us into a newly queasy communion with the horror of his crimes." 

The film won the Milwaukee Film Festivals Cream City Cinema Grand Jury Award for 2012. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 