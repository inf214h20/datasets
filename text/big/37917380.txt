Suddilage Kathaawa
{{Infobox film
| name           = Suddilage Kathaawa
| director       = Dharmasiri Bandaranayake
| producer       = Bandula Gunawardena
| writer         = Simon Navagattegama
| starring       = Swarna Mallawarachchi, Sommie Rathnayake, Joe Abeywickrama
| music          = Premasiri Khemadasa
| cinematography = Parakrama De Silva, Andrew Jayamanne
| editing        = S. De Alwis, Newton Weerasinghe
| released       = 1984
| runtime        = 145 minutes
| country        = Sri Lanka
| language       = Sinhala
}}

Suddilage Kathaawa ( ) is a 1984   Sri Lankan film directed by Dharmasiri Bandaranayake. The film was based on the novel by reputed writer Simon Navagattegama, who later adapted the novel for the movie.

==Plot==
Set in 1958, Suddhi (Swarna Mallawarachchi) is married to a notorious criminal in the village, Romial (Cyril Wickramage), who has been taken into the local jail on suspicion of murder. In the meantime, Suddi depends on various men in the village to provide her provisions by sleeping with them. Some time before Romial comes home, Suddi starts sleeping with Mudalali (Sommie Rathnayake), a shrewd businessman from town, and he becomes her main lover and provider.

Romial had been hired by Arachchila (Joe Abeywickrama), the local ex-headman, to kill an unnamed villager. Mudalali is married to  Arachchilas only sister, mainly for the respect and the wealth she had inherited. The movie is set in the era of the Grama Niladhari system, which undermines Arachchilas village position, and Mudilalis connection with the new grama  niladhari provokes Arachchila. 

Muralali informs Avusadaya (Ananda Wijesinghe),  a tenet farmer who works on Arachchilas land, of the Paddy Lands Act passed through Parliament in Colombo; this new law would force landowners to share the excess land with the landless. This conversation sets forth the conflict between Arachchila and Avusadaya.

Romial returns from jail to the village. Arachchila handed him over the contract to destroy Aushadaya by killing Peter (Salaman Fonseka), the brother of Aushadaya with whom he had regular quarrels after he gets drunk. When Romial hesitates to get back to violence, Suddi encourages him to go ahead with the killings. The murder trap ends with both Peter and Arachchilas deaths. 

However Romial who commits the murder regrets this. He decided to help Peters wife (Nilanthi Wijesinghe), who is left helpless after Peters death, and remains unaware about the murder. Soon, Peters wife falls in love with Romial, while Mudalali and Suddi continue with their affair. Peters wife informs Romial about the affair between Mudalali and Suddi, and the film ends with an enraged Romial killing Suddi.

==Cast==
* Swarna Mallawarachchi as Suddi
* Cyril Wickramage as Romial
* Sommie Rathnayake as Mudalali
* Joe Abeywickrama as Arachchila 
* Ananda Wijesinghe as Avusadaya
* Salaman Fonseka as Peter
* Nilanthi Wijesinghe as Peters wife

==References==
 

 
 