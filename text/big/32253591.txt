Nayee Padosan
 
 
 
{{Infobox film
| name = Nayee Padosan
| image = Nayee_Padosan.jpg
| caption = 
| director = B.H. Tharun Kumar
| producer = Nitin Manmohan
| writer = Ikram Akhtar
| narrator = 
| starring = Mahek Chahal Anuj Sawhney Vikas Kalantri Aslam Khan
| music = Shankar–Ehsaan–Loy
| cinematography = Raj| editing = 
| distributor = 
| released =  
| runtime = 
| country = India
| language = Hindi
| budget =  
}} Tamil film Indru Poi Naalai Vaa which is a blockbuster in 1981 and directed by K. Bhagyaraj

==Plot==
The story revolves around the protagonist or the Nayee Padosan (Mahek Chahal) who has shifted into a new locality. Her simplicity, righteousness and no-nonsense attitude are qualities any man would want his life partner to possess.

Three different boys Raju (Anuj Sawhney), Raja (Aslam Khan) and Ram (Vikas Kalantri) from different backgrounds, characteristics and eventually modus operandi are swept head over heals in love with the girl.

What follows next is a laugh riot, in the way the three eligible bachelors try and get themselves acquainted and eventually outwit one another to get the girl.

The film takes a sudden turn when a new entry Prabhu (Rahul Bhat)  that happens to be a favourite with both the girl and her family shatters the hopes of the three eligible bachelors. Nothing seems to stop the inevitable. The battle seems lost for the three hopeless boys.

The film goes through various emotional graphs where the three boys have their individual shares of gains and losses vis-a-vis their competitors. The girl doesnt reveal her feelings until a certain change of events makes her realise whom she really loves and would want to spend the rest of her life with.

==Cast==
* Mahek Chahal – Pooja Iyengar
* Anuj Sawhney – Raju
* Vikas Kalantri – Ram
* Aslam Khan – Raja
* Rahul Bhat – Prabhu
* Vijay Kashyap – Shastri Iyengar
* Yusuf Hussain
* Shabnam Kapoor

== Music ==
{{Infobox Album |  
  Name        = Nayee Padosan
|  Type        = soundtrack
|  Artist      = Shankar–Ehsaan–Loy
|  Cover       =
|  Released    =  4 April 2003 (India)
|  Recorded    =  Feature film Fusion music
|  Length      =  Saregama
|  Producer    = Nitin Manmohan
|  Reviews     = 
|  Last album  = Armaan (2003 film)|Armaan (2003)
|  This album  = Nayee Padosan (2003)
|  Next album  = Kuch Naa Kaho (2003)
}}
The movie soundtrack contains 7 songs composed by the award-winning trio Shankar–Ehsaan–Loy. Lyrics by Sameer (lyricist)|Sameer.

{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)!! Duration
|- Sari Sari Shaan (singer)|Shaan & Shweta Pandit ||4:42
|- Mera Man Mahalakshmi Iyer ||6:12
|- Chori Nahi Mahalakshmi Iyer & Shankar Mahadevan ||5:03
|- Rang De Babul Supriyo, Shaan ||4:39
|- Dil Mein, Shaan & Shankar Mahadevan ||5:53
|- Ek Bechainee Neha Rajpal, Shankar Mahadevan & Vijay Prakash ||5:38
|- Nayee Padosan (Fusion Music) ||Shankar–Ehsaan–Loy ||1:55
|-
|}

== External links ==
*  
*  

 
 
 
 