The Power of Good: Nicholas Winton
{{Infobox film
| name           = The Power of Good: Nicholas Winton
| image          = 
| image size     = 
| caption        = 
| director       = Matej Mináč
| producer       = Partik Pass Matej Mináč   
| writer         = 
| narrator       = 
| starring       = 
| music          = Janusz Stoklosa 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 64 min 
| country        = 
| language       = 
| budget         = 
}}
The Power of Good: Nicholas Winton ( ) is a 2002 documentary about Nicholas Winton, the man who organized the Kindertransport rescue mission of 669 children from German-occupied Czechoslovakia on the eve of the Second World War. Director Matej Mináč decided to make the documentary after meeting Winton while developing the film treatment for All My Loved Ones.   

==See also==
* 
*The Children Who Cheated the Nazis

==Awards==
*Best Documentary - International Emmy Awards (2002)
*Trilobit Prize Czech Republic (2002)
*Slovak Film Critics Prize IGRIC (2002)
*Christopher Award for Film that Affirms the Highest Values of the Human Spirit (2006) 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 


 