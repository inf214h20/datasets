New Best Friend
 
{{Infobox film
| name           = New Best Friend
| image          = New best friend.jpg
| caption        = Theatrical release poster
| director       = Zoe Clarke-Williams
| producer       = Frank Mancuso Jr.
| writer         = Victoria Strouse
| starring       = Mia Kirshner Meredith Monroe Dominique Swain John Murphy
| cinematography = Tom Priestley Jr.
| editing        = Norman Buckley Leo Trombetta
| distributor    = TriStar Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $46,375 
}}
New Best Friend is a 2002 American film based on a story by author James Edwards. The film was originally owned by MGM, which eventually let this film go.  Since then, TriStar Pictures acquired the rights to distribute this film in the United States and some other territories, primarily for home video market;       still, TriStar Pictures gave this film a limited theatrical release in the United States on April 12, 2002.   

==Summary==
A North Carolina sheriff (Taye Diggs) investigates the near-fatal drug overdose of a working class college girl (Mia Kirshner) and discovers many sordid details of her life before and during her descent into drugs and debauchery. 

==Cast credits==
*Mia Kirshner: Alicia Campbell
*Meredith Monroe: Hadley Ashton
*Dominique Swain: Sidney Barrett
*Rachel True: Julianne Livingstone
*Eric Michael Cole: Warren
*Scott Bairstow: Trevor
*Taye Diggs: Sheriff Artie Bonner
*Oliver Hudson: Josh
*Joanna Canton: Sarah
*Dean James: Max
*Glynnis OConnor: Connie Campbell
*Ralph Price: Eddie

==Notes==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 