Bandido (1956 film)
{{Infobox film
| name           = Bandido
| image size     =
| image	         = Bandido FilmPoster.jpeg
| caption        = Film poster
| director       = Richard Fleischer
| producer       = Robert L. Jacks John E. Burch 
| writer         = Earl Felton
| narrator       =
| starring       = Robert Mitchum Ursula Thiess Gilbert Roland Zachary Scott
| music          = Max Steiner
| cinematography = Ernest Laszlo
| editing        = Robert Golden
| distributor    = United Artists
| released       = 1956
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross = $1.65 million (US)  
| preceded by    =
| followed by    =
}}
 1956 Western movie starring Robert Mitchum.  The supporting cast includes Ursula Thiess, Gilbert Roland, and Zachary Scott.  The film, set in the Mexican Revolution and filmed on location around Acapulco, was written by Earl Felton and directed by Richard Fleischer. Robert Mitchum also co-produced the film through his DRM Productions company. 

==Plot==
In the 1916 Mexican  Revolution, American mercenary Wilson (Robert Mitchum)  checks into a hotel in Northern Mexico during a battle.  Equipped with a suitcase full of Mk 2 grenades; he throws a few "samples" at the Federal troops in the square, turning the tide of the battle. Revolutionary Col. Escobar (Gilbert Roland) and his men praise Wilsons efforts, calling him "El Alacran" (the  scorpion) for the sting of his grenades. Col. Escobar is still  a bit wary about why an American would want to help them. Wilson informs Escobar of a plot by an American gun runner, Kennedy (Zachary Scott) to sell a large shipment  arms and explosives  to Federal forces—but hes not sure where they are.

Encouraged by Wilson to capture  Kennedy, they attack his train and stop it. Kennedy lies; saying the arms are at his fishing lodge. Wilson make time with Kennedys flirtatious wife, Lisa (Ursula Thiess).  After the capture of Kennedy, Wilson takes his leave of Escobar. He goes to the fishing lodge to find nothing but Lisa, whom he saves from Escobars firing squad and encourages to flee north to the U.S.

Angered by what he perceived as treachery, Gen. Escobar imprisons Wilson in with Kennedy, who is now held in an old mission awaiting the firing squad. Wilson, inexplicably, still has two hand grenades, which he uses to blow their way out of the mission. With Escobar  chasing them through the jungle and over a waterfall, they both escape, but Kennedy is shot and later dies.

Taking the secret of the arms location  the dying Kennedy has given him, Wilson, and Escobar, ride to the coast and Kennedys fishing villa. The arms were to be delivered by barge in a day or two. They then encounter federal troops who are going to ambush Escobars rebel force which is approaching by the beach. Wilson then offers Escobar a "compromise"; no more "deals" by getting a M1917 Browning machine gun out of the arms barge and offering to blow up the explosives barge to save his troops. Raking the troops to force them back, the explosives barge is  now cut free of its moorings to drift towards the troops on shore. A few rounds into this barge by Wilson and the Federals are blown up.
 
After his men then load up with  the arms, Escobar releases Wilson, saying; "Adios, El Alacran!" Wilson heads North to rejoin Lisa.

==Cast==

* Robert Mitchum as  Wilson
* Ursula Thiess as  Lisa Kennedy
* Gilbert Roland as  Colonel Escobar
* Zachary Scott as  Kennedy
* Rodolfo Acosta as  Sebastian
* José Torvay as  Gonzales Henry Brandon as  Gunther
* Douglas Fowley as  McGhee

==Production==
This film was shot on location in Mexico at Cuernavaca, Tepetlán, Palo Balero in Xochitepec, Yautepec de Zaragoza, Acapulco, Iguala and the Hotel Hacienda in Cocoyoc, Morelos. 

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 