Barely Dead
{{Infobox film
| name           = Barely Dead
| image          = Barely Dead.jpg
| image size     =
| alt            = 
| caption        = The Barely Dead logo
| director       = Doug Urquhart  (credited as JDU)
| producer       = Misled Media
| writer         = 
| screenplay     = 
| story          = 
| narrator       = Justin Eisinger
| starring       = See list of participants The Boxer Rebellion
| cinematography = JDU
| editing        = JDU
| studio         = Misled Lab
| distributor    = 
| released       = 2006  
| runtime        = 50 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 documentary about the underground culture of Inline skating|Rollerblading.

Credited are: Azikiwee Anderson, Julian Bah, Erik Bailey, Michael "Gumby" Braud, Alex Broskow, Cameron Card, Shane Coburn, Jess Dyrenforth, Arlo Eisenberg, Jon Elliot, Chris Farmer, Rob Guerrero, Chris Haffey, Chiaki Ito, Jon Julio, Ramelle Knight, Dave Kollash, Pat Lennon, Montre Livingston, Franky Morales, Jeromy Morris, Mike Opalek, Dre Powel, Brian Shima, Oli Short, David Sizemore and Jeff Stockwell.

The artwork on the packaging was done by Andrew Tunney. 

==Production==
The documentary was produced by Misled Media in 2006. Misled Media also made Black Market (2005), another documentary about rollerblading (not to be confused with the 2007 documentary about the illegal trade in wildlife and associated products). Both videos were directed by Doug Urquhart.  The film was shot on Super16mm motion picture film and then transferred to video.

    

Rollerblading was originally known as aggressive inline skating. This documentary,   contains a short history of the inline skate, the rollerskate and the skateboard. It also covers the use of the inline skate as a training tool and the boom of companies like Senate. For the rest the documentary is a compilation of a lot of skating footage and chronicles skating from the time it became popular in the nineties to around 2005. Rollerblading had a tremendous impact as a result of the exposure of the X games. The sport had to fight for a place between BMX (which was dwindling at that time) and skateboarding. The popularity of rollerblading dwindled after that, the documentary tries to explain why. Critics will say that the documentary is more like a rock video promoting inline skating. The video contains many shots of professional skaters doing what they are best at. The documentary also contains old footage and TV broadcasts. There is also mention of the IMYTA (I match your trick association), an informal competition among skaters. The film was shot in different places of the globe such as USA or Europe.

Because rollerblading has now, more or less, gone underground, it no longer appears alive, hence the title "Barely Dead".

There have been a number of "skate documentaries", Black Market being one of them. Before that there were "Hoax" and "Hoax 2". The skaters that made these "documentaries" claim that they were actually performing professionally, while claiming that they were making a documentary. Hence the name hoax. And because the first video was successful, another was made: Hoax 2.

==Awards==
 
Winner of the 2005 London Freesports Film festival.

==Alternate titles==
* Barely Dead: The Saga of Modern Rollerblading
* Black Market II

==References==
 

==External links==
* 

 
 
 
 
 
 
 