From Nurse to Worse (1940 film)
{{Infobox film
 | name           = From Nurse To Worse 
 | image          = Fromnurseworse.jpg
 | caption        = 
 | director       = Jules White 
 | writer         = Clyde Bruckman Charles L. Kimball  John Tyrrell Al Thompson 
 | cinematography = Benjamin H. Kline 
 | editing        = Mel Thorsen 
 | producer       = Jules White
 | distributor    = Columbia Pictures 
 | released       = August 23, 1940
 | runtime        = 16 43" 
 | country        = United States
 | language       = English
}}

From Nurse To Worse is the 49th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot ==
The boys are painters who run into their old friend Jerry, an insurance salesman (Lynton Brent). He promises them that if they take out a policy on Curly proving that he has gone insane, they can collect $500 a month. Moe and Larry bring Curly on a leash to the office of Dr. D. Lerious (Vernon Dent). Curlys pretending to be a hound is so over the top that the doctor declares he must operate. The Stooges flee, and hide out in the back of a dog catchers truck and are soon infested with fleas. Dr. Lerious eventually catches up with the Stooges, and Curly is sent straight for the operating room. Eventually, the trio get away on a gurney, bump into their pal Jerry, and give him the works.

==Production notes==
From Nurse to Worse was filmed on May 15-18, 1940.   

The footage of the Stooges sailing through the city streets was lifted from Dizzy Doctors.    The voice on the police scanner is Moes. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 