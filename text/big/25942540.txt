Avan Ivan
 
 
{{Infobox film
| name           = Avan Ivan
| image          = AvanIvanPoster.jpg
| caption        = Bala
| producer       =  
| story          = Bala
| screenplay     = Bala
| writer         = S. Ramakrishnan (dialogue)
| starring       =  
| music          = Yuvan Shankar Raja
| cinematography = Arthur A. Wilson
| editing        = Suresh Urs
| studio         = AGS Entertainment FiveStar  Ayngaran International  (worldwide)    }}
| released       =  
| country        = India
| runtime        = 130 minutes
| language       = Tamil
| gross          = 
}}
Avan Ivan ( ) is a 2011 Tamil comedy-drama film written and directed by Bala (director)|Bala, who with this project directs his fifth feature film. The film stars Arya (actor)|Arya, Vishal Krishna, Janani Iyer and Madhu Shalini in the lead roles.    The film, produced by Kalpathi S. Agorams AGS Entertainment, features music by Yuvan Shankar Raja, cinematography by Arthur A. Wilson and editing by Suresh Urs.  Set in the backdrops of Theni, Avan Ivan illustrates the relationship between two boisterously playful half-brothers. The film, which was dubbed in Telugu as Vaadu Veedu,    released worldwide on 17 June 2011,  to mixed critical response. According to Eros International, it was successful at the box office. 

==Plot==
Walter Vanangamudi (Vishal Krishna) and Kumbudren Saamy (Arya (actor)|Arya) are half-brothers, who constantly fight and try to outdo each other. Both brothers are pickpockets and get encouragement from their respective mothers. Walters mother Maryamma (Ambika (actress)|Ambika) encourages her son to steal and continue their "family tradition". However, Walter, an aspiring actor with an effeminate touch, is rather interested in arts than committing crimes. The Zamindar (G. M. Kumar), referred to as Highness by the community, takes an affinity towards Saamy and Walter and treats them as his own family. He constantly encourages Walter to take up acting seriously and to be friendly towards his brother. Walter is smitten by a Police Constable Baby (Janani Iyer) from whom he attempts to steal after being dared by his brother to prove himself. She finds him completely amusing and eventually falls for him. He returns several stolen goods from his home and from Kumbudren Samy to rescue her from being dismissed and goes to great lengths to impress her. Kumbudren Samy falls for a college student Thenmozhi (Madhu Shalini) who is intimidated by his rough ways at first but reciprocates his love later on.

One day, a police inspector (Chevvalai Rasu) who had insulted the Zamindar is tracked down and punished by Kumbudren Samy and Walter. While Walter takes the police truck and dumps it in the forest, Kumbudren Samy is caught by the police inspector. He acts like he swallowed a blade so he can see Thenmozhi once before going to jail. He is rushed to the hospital sees her and fools the entire police force even though Baby is quite suspicious. Kumbudren Samy tries to bribe the doctor to lie but she tells the police constable who only pleads with the him but later gives up and releases him. Actor Suriya attends a school function in the town to promote educational awareness through his Agaram Foundation. Just as he is about to leave the Zamindar requests him to stay and witness Walters acting skills, who shows off his depictions of the nine emotions (Navarasas) and impresses everyone, including Kumbedren Samy. The drunk Kumbredren Samy then reveals to the drunk Zamindar that he actually does love his brother and that all the anger and hate it is just an act.

Meanwhile, the Zamindar exposes the illegal activities of a cattle smuggler (R. K. (actor)|R. K.). The smuggler loses his animal farm and is taken into custody by the police amid media fare. Kumbudren Samy brings Thenmozhi to the Zamindars house to introduce them to each other. Thenmozhi is identified to be the Zamindars enemys daughter, which Kumbudren Samy was unaware of. When the Zamindar tells him to break up with her, Saamy refuses and attacks him verbally, telling that he doesnt have a family and no one loves him so he would never understand. An angered Zaminder throws out Kumbudren Samy and Walter as well who tried to defend his brother and gets extremely drunk. Later that evening, both brothers make up with the Zamindar and invite him to their home. The Zamindar even signs over his land to Thenmozhis father who was trying to get a hold of the land and organises for their marriage. Few days later, the smuggler returns, kidnaps the Zamindar and flogs him into unconsciousness, before hanging him to death from a tree. Walter and Kumbudren Samy are devastated and almost collapse with grief. While Kumbudren Samy fails in his attempt to take revenge, getting flogged and injured severely, Walter manages to bash up the smuggler and his men. The smuggler has been tied down under the platform with the Zamindars body and is burnt to death along with the Zamindar, while both brothers dance madly.

==Cast==
 Arya as Kumbudren Saamy
* Vishal Krishna as Walter Vanangamudi
* G. M. Kumar as Zamindar Thirthapathi (Highness)
* R. K. (actor)|R. K. as the smuggler
* Madhu Shalini as Thenmozhi
* Janani Iyer as Constable Baby Ambika as Maryamma
* Prabha Ramesh as Kumbudren Saamys mother
* Ramaraj as Police Inspector
* Ananth Vaidyanathan as Srikanth
* Vimalraj Ganesan as Kannan
* Chevvalai Rasu as Inspector who insults Highness Suriya as himself (guest appearance) Bala as auto driver (cameo)

==Production==

===Development===
After finishing and releasing his magnum opus Naan Kadavul in February 2009, Bala (director)|Bala, whose previous feature films had all been tragedy drama films dealing with serious and dark subjects,    announced that for his next directorial, he would be moving away from such films and make a full-length light-hearted comedy film.    He was working on its script in the following months, whilst declaring that it will be a double hero subject. Allegedly Bala had come to this decision, since his earlier films, despite receiving critical acclaim, garnered poor or only average box office returns.    During the post-production phase, Bala disclosed that the film was "fun till the last 15 minutes, after which it turns serious", adding that he decided to "change tracks", after several people including his mentor Balu Mahendra advised him to do so.   

Though initially Soundarya Rajinikanths Ocher Studios were reported to be the producer of the film,  Kalpathi S. Agoram finally took up the project and decided to produce it under the banner of AGS Entertainment. On 25 January 2010, an official press meet was held, where the films official title was finally revealed and the films lead female actress as well as the technicians were announced, with which the project official commenced.    During the launch, Bala told that unlike his earlier films, Avan Ivan would have an "extra dose of comedy, besides action and family sentiments."  He had also disclosed that, unlike his earlier films, he will complete Avan Ivan within eight months of time and be ready for a release in late 2010.    According to sources, Bala intended to name the film as Avana Ivan first, but as it was already registered by director Bharath, who was not willing to give away the title, he changed the title to Avan Ivan. 

===Casting=== Surya and Jeeva and squint and braces throughout the film.  He reportedly became the first ever actor to attempt a squint look in a feature film,  which was considered for an entry in The Guinness Book of World Records.    
 dub for Suriya was signed to appear in a guest role as himself.   Reemma Sen was also reported to perform a cameo role,  however shortly before release, this was revealed to be a false news. 

Furthermore, Yuvan Shankar Raja was announced as the music director of the film, joining Bala again after the successful Nandha in 2001.  About replacing his usual music director Ilaiyaraaja by his son, Bala said that since it was a youth-centric film, Yuvan Shankar Raja can "bring the right feel".  Suresh Urs, who had worked on all Bala films since Nandha, was roped in as the films editor, while Arthur A. Wilson remained the cinematographer. S. Ramakrishnan was assigned to write the dialogues, after J. S. Ragavan and professor Gnanasambanthan were approached. 

===Filming=== dubbing and post-production works commenced. 

==Soundtrack==
{{Infobox album 
| Name = Avan Ivan
| Longtype = to Avan Ivan
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Avan Ivan.jpg
| Border = yes
| Alt = 
| Caption = Original CD Cover
| Released = 18 April 2011
| Recorded = 2010 - 2011  Prasad Studios  (Chennai)  S.A.S.I Studios  (Chennai)  Feature film soundtrack
| Length = 23:56 Tamil
| Label = Sony Music
| Producer = Yuvan Shankar Raja
| Last album = Jolly Boy (2011)
| This album = Avan Ivan (2011)
| Next album = Mankatha (soundtrack)|Mankatha (2011)
}}
{{Album ratings
| rev1 = Behindwoods
| rev1Score =     
| rev2 = Rediff
| rev2Score =     
}} Western musical synths and recorded live. Indian folk drums with 40 people.     Yuvan revealed that he was given over two months time for each song,    while Bala later commented that Yuvans job for Avan Ivan was of "international standard".  The Master recording was handed over by Yuvan Shankar Raja on 18 March 2011. 

The soundtrack album was released exactly one month later, on 18 April 2011 by Balas mentor Balu Mahendra in a grand event held at the Residency Towers, Chennai.   Controversially, the songs had been leaked to the Internet few days before the official release, after Sony Music had sent the master copies to abroad earlier.   The album consists of 5 tracks, four songs and an instrumental,  with lyrics penned by Na. Muthukumar. The lyrics for the Telugu version of the soundtrack, which was released on 1 June 2011 at Taj Banjara Hotel in Hyderabad, India|Hyderabad, were written by Ananth Sreeram and Chandrabose (lyricist)|Chandrabose.  The song "Oru Malayoram" featured vocals by children Priyanka, Srinisha and Nithyashree, who were participants in the second season of the reality-based singing-competition Airtel Super Singer Junior.  Only one songs from the soundtrack, "Dia Dia Dole", was used in its entirety, along with an altered shorter version of "Rasathi" and parts of "Mudhal Murai", while "Avanapathi" and "Oru Malayoram" were completely left out.     The film however featured two additional tracks, not included in the soundtrack, which would be released in a second edition. 

The album received positive reviews from music critics. Richard Mahesh from Behindwoods gave a 2.5/5 rating and said "Yuvan Shankar Raja has experimented with a new-dimensional music that sounds good on the whole. While ‘Avanapathi’ and ‘Rasathi’ turning us irresistibly addictive to its tunes, ‘Oru Malayoram’ will be a melodic hit of this season." describing the album as "different but a laudable show by Yuvan"  Pavithra Srinivasan from Rediff gave a 3/5 rating and said "When it comes to Avan Ivan, it looks like Yuvan has voluntarily tried to move out of his comfort zone, given up on his template and experimented, particularly with the instrumental arrangement and most times, it works. Go for it."  Indiaglitz said "Songs from Balas films never failed to disappoint us. The director has repeated the magic this time too and the credit goes to Yuvan Shankar Raja." 

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 23:56
| all_lyrics      = Na. Muthukumar
| title1          = Rasathi
| extra1          = Haricharan
| length1         = 4:53
| title2          = Dia Dia Dole
| extra2          = Suchitra
| length2         = 4:02
| title3          = Oru Malayoram	
| extra3          = Vijay Yesudas, Priyanka, Srinisha, Nithyashree
| length3         = 5:43
| title4          = Mudhal Murai
| extra4          = Vijay Prakash
| length4         = 3:37
| title5          = Avanapathi Sathyan
| length5         = 5:41
}}

==Release== Telugu as Vaadu Veedu and release it under his GK Films Corporation banner simultaneously with the Tamil version.

==Reception==

===Critical response=== Arya and Vishal Krishna saying "Unlike Vishal, Arya doesn’t have much scope over performance, but manages to remain under spotlights with his rib-tickling comedy tracks and dialogue delivery. Although we have the signature as ‘A Film By Bala’ during final credits, it’s worth mentioning that the film completely belongs to Vishal."  A reviewer from in.com gave three and half out of five and said that the movie is a "definite watch for Balas followers and to watch the new Vishal emerge as a performer". Further praising Vishal, the critic stats, "there is no doubt that from the beginning of the making of the film, there has been so much riding on Vishals character as he has been playing the role of a squint and has done a fab job."  Rohit Ramachandran of nowrunning.com gave it two out of five stars stating that "Avan Ivan, in the end, is Vishal and Arya battling for recognition in the acting arena."  Supergoodmovies also gave three and a half stars and concluded that "Avan Ivan will not let down the audience. Though not a typical Bala film, a feel of joy creeps as we watch the film. It is one of the best technically made movies by Bala so far." 

  wrote: "Overall Avan Ivan suffers from a sloppy script despite having some fine performances. The lack of balance between Balas emphatic portrayal of different kind of life and his effort to provide fun is the major problem of the movie. The fun becomes farce and the seriousness turns out to be ineffective." 

===Box-office===
Avan Ivan had a solo release on 17 June 2011 in almost 700 screens worldwide.   The film opened across 350 screens in Tamil Nadu and collected  8.9&nbsp;million in the opening weekend at the Chennai box office.  According to Sify, the film had earned a distributor share of  8.3&nbsp;million from 18 Chennai screens.  At the end of the third weekend, the film had earned  46.5&nbsp;million in Chennai.  Its dubbed Telugu version, Vaadu Veedu released simultaneously with the Tamil version,  and grossed an estimated  15&nbsp;million in the opening weekend in Andhra Pradesh,  while collecting around  5&nbsp;million in Nizam area only, which was considered a remarkable figure for a dubbed release.  The dubbed version had reportedly earned  40&nbsp;million in Andhra Pradesh,  outclassing original Telugu ventures.   In the United Kingdom, the film was released by Ayngaran International across 14 screens and collected $49,921 in the first three days, opening at 15th place.  At the end of the second weekend, the film had earned $80,933 overall in UK. FiveStar distributed the film in Malaysia in 32 theatres, where it grossed $232,781 in the first weekend, opening at fourth. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 