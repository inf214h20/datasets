Ek Main Aur Ekk Tu
 
 

 

{{Infobox film
| name           = Ek Main Aur Ekk Tu
| image          = Ek Main Aur Ekk Tu.jpg
| caption        = Theatrical release poster
| director       = Shakun Batra
| producer       = Hiroo Yash Johar Karan Johar Ronnie Screwvala
| writer         = Ayesha Devitre Shakun Batra Imran Khan Kareena Kapoor Ratna Pathak Shah Boman Irani Ram Kapoor
| music          = Songs:   
| cinematography = David McDonald
| editing        = Asif Ali Shaikh
| studio         = Dharma Productions
| distributor    = UTV Motion Pictures
| runtime        = 110 minutes    
| released       =  
| country        = India
| language       = Hindi
| budget         =    
| gross          =     
}}
 Indian romantic Imran Khan Las Vegas, Nevada, who loses his job and following a night of debauchery, accidentally marries a free-spirited hairstylist named Riana Braganza. After mutually deciding to annul the marriage, Rahul begins a one-sided attraction for Riana, which threatens to ruin their newly found friendship.

Development of the film began in 2010, when Johar signed Batra and Khan for a film to be made under his banner. Inspired by the Woody Allen style of film-making, Ayesha Devitre and Batra worked on the script, with principal photography taking place in Vegas, Los Angeles, Pataudi and Mumbai. The music of the film was composed by Amit Trivedi with lyrics written by Amitabh Bhattacharya. Originally slated to release during the fall of 2011, Ek Main Aur Ekk Tu eventually released on 10 February 2012, to positive critical notice, with major praise directed to Khan and Kapoors performance, and proved a moderate commercial success.    

==Plot== Imran Khan) has always been on the road to perfection, as per the wishes of his domineering father (Boman Irani) and socialite mother (Ratna Pathak Shah). After suddenly losing his job as an architect in Vegas, he decides to hide the truth from his parents and find another job. Unable to cope with unemployment, he decides to go to a psychologist and runs into another patient, Riana Braganza (Kareena Kapoor). A series of events lead Riana to believe that he is sent by her ex-boyfriend to stalk her, and she ends up taking his file by mistake.

Riana, who later realizes her mistake, calls him and apologizes for her behavior. They meet up during Christmas Eve and Riana invites Rahul to have a few drinks with her. A few drinks turn into a night of intoxicated revelry as they end up marrying each other. Horrified, they both decide to get it annulled as soon as possible. Meanwhile, Riana, who is also unemployed and has not paid her rent, loses her house and an initially hesitant Rahul finally lets her stay with him till her problem gets resolved. During these few days, they get to know each other better. Rahul is revealed to be interested in photography while Riana recounts her ambition to be a ballet dancer, which was destroyed after she broke her ankle in her childhood. Their friendship develops as Riana gives Rahul a long break he always wanted. As they grow closer, Rahul develops feelings for Riana, who fails to reciprocate them.

As New Years Eve approaches, Riana plans to visit her family in India, and asks Rahul to accompany her. He disagrees initially, as his parents also live in Mumbai, but later succumbs to her argument. Upon arrival, she introduces him to her family, making his feelings towards her grow. The next day, Riana takes him to visit her school, and amidst reflecting over her past relationships, he unsuccessfully tries to kiss her. Realizing that she does not feel the way he does, he angrily leaves the school, only to almost run into a car revealed to be his mothers. He lies and tells his mother that he is in India for a research trip, and that Riana is a colleague. They proceed to have lunch with Rahuls parents, where he spins a web of intricate lies to avoid telling his father the truth. He leaves Rianas house and moves in with his parents, trying to tell them the truth, but once again is overpowered by his father.

Two days later, during a family dinner, Rahul angrily divulges the complete truth to his in-denial parents and their business partners (including Mr. Bulani), after realizing the meaninglessness of his pretense he indulged in showing all these years. As he rushes back to Riana to apologize for his behavior, Riana apologises she never intended to lead him on. After a night of reflection and deep conversation, both decide to maintain their friendship. The film ends with both returning to Las Vegas and finalizing their annulment with Rahul getting a new job, while keeping time out for photography. He remains hopeful that one day he will convince Riana to go back to that chapel with him. Both remain good friends, with an ending note from Rahul claiming that he is finally happy with his life, as the credits roll.

==Cast== Imran Khan as Rahul Kapoor, an uptight architect living in Las Vegas. Following the loss of his job, he ends up marrying Riana Braganza due to alcohol-resulting drunkenness. Khan describes his role by saying, "Rahul has had a very restricted life. His parents have always told him what to do, how to behave, what to study, what career to pursue and so on and so forth. Even his hobbies have been decided by his parents. He has never had a spontaneous moment in his life where he has done something for himself. He is actually a very sad, lonely and tragic guy but he doesnt realize it."   
* Kareena Kapoor as Riana Braganza, a hairstylist who tries to add zing to Rahul Kapoors monotonous life following their marriage. In an interview with The Times of India, Kapoor describes her character and explains, "Riana knows what she is doing. Even though she does not have a house or a job, she is a positive person... very similar to the way I am." 
* Ratna Pathak Shah as Rahuls mother, a fiercely dominating woman.
* Boman Irani as Rahuls father, a strict disciplinarian.
* Ram Kapoor as Mr. Bulani, a friend of Rahuls father, who tries to help Rahul overcome his low self-esteem.
* Dana Lewis as Mrs. Bulani, who makes sexual advances at Rahul.
* Soniya Mehra as Anusha, Rahuls ex-girlfriend.
* Rajesh Khattar as Mr. Shah.
* Nikhil Kapoor as Phil Braganza, Rianas father.
* Zenobia Shroff as Nicole Braganza, Rianas mother.
* Manasi Scott as Rianas sister.
* Mukul Chaddha as Rianas brother-in-law.
* Avantika Malik in a special appearance during the song Auntyji.

==Production==
  
Pre-production work on the film began in the year 2010 when Karan Johar announced his plans of making a new project with debutant Shakun Batra.  Following the success of their previous collaboration I Hate Luv Storys (2010), Khan was contracted by Johar to play the films male lead. In an attempt to sign in an actress who had never worked with Khan before, Kapoor was confirmed to play the female lead several days later.  She had to cut her hair short and sport red streaks for her role.  On casting Khan and Kapoor for the film, Johar commented, "They could get their real life personality onto the film. Imran is meant to be this very perfectionist young man. So while he is a very stuffy kind of a character, he is also very focused and doesnt let himself go. On the other hand Kareena plays this woman who is like a modern version of Geet from Jab We Met."   

Regarding the film, Batra mentioned, "Its a slice of life film, driven by characters. The plot is not primary, emotions of the characters are at the forefront. It is set on two weeks of the life of two characters, played by Imran and Kareena."    On the other hand, Johar said that the film was inspired by the Woody Allen style of film-making. He added that there would be "lot more conversation happening here" and there would be "these walk-talk sequences where it is just you and the camera in motion". 
 Indian state social networking site of Twitter. According to him, "Short Term Shaadi was just a working title that attached itself to this film through the making. Neither the director nor I were happy with it from the start."  The name Ek Main Aur Ekk Tu was derived from a song from the Rishi Kapoor and Neetu Singh starer Khel Khel Mein (1975).  On deciding to set the film in Vegas, Batra said that, "the character of the place is very well defined and we need to show it as Las Vegas. It cant be shown as any other city. Since most films are not set there, it gets difficult. In case of  Ek Main Aur Ekk Tu, our film is set in Las Vegas and Mumbai".  As filming took place during the winter season in Vegas, the production team shot for double shifts in order to be cost effective. 

==Soundtrack==
{{Infobox album
| Name        = Ek Main Aur Ekk Tu
| Type        = Soundtrack
| Artist      = Amit Trivedi
| Cover       = 
| Released    =  
| Recorded    = 2011 Feature film soundtrack
| Length      = 30:42
| Label       = T-Series
| Producer    = Karan Johar
| Last album  = Trishna (2011 film)|Trishna  (2011)
| This album  = Ek Main Aur Ekk Tu (2012)
| Next album  = Ishaqzaade (2012)
}}
The music rights of Ek Main Aur Ekk Tu were sold to T-Series for  .  The soundtrack, which released on 26 December 2011, featured music composed by Amit Trivedi with lyrics penned by Amitabh Bhattacharya.  Batra mentioned that Trivedi was his first choice for music director and added that while the track Gubbare was the first to be recorded, the title track took the longest to compose. 

{{track listing
| headline  = Tracklist
| extra_column    = Singer(s)
| total_length    = 30:42
| title1          = Ek Main Aur Ekk Tu
| lyrics1         = Amitabh Bhattacharya
| extra1          = Benny Dayal, Anushka Manchanda
| length1         = 4:22
| title2          = Gubbare
| lyrics2         = Amitabh Bhattacharya
| extra2          = Amit Trivedi, Shilpa Rao, Nikhil DSouza
| length2         = 4:31
| title3          = Aunty Ji
| lyrics3         = Amitabh Bhattacharya
| extra3          = Ash King, Bianca Gomes, Neuman Pinto, Fionas Pinto, Piyush Kapoor
| length3         = 4:34
| title4          = Aahatein
| lyrics4         = Amitabh Bhattacharya
| extra4          = Karthik (singer)|Karthik, Shilpa Rao
| length4         = 4:27
| title5          = Kar Chalna Shuru Tu
| lyrics5         = Amitabh Bhattacharya Vishal Dadlani, Shilpa Rao
| length5         = 4:56
| title6          = Ek Main Aur Ekk Tu (Remix)
| length6         = Amitabh Bhattacharya
| extra6          = Benny Dayal, Anushka Manchanda, Shefali Alvares
| length6         = 4:08
| title7          = Aahatein (Remix)
| lyrics7         = Amitabh Bhattacharya Shekhar Ravjiani, Shilpa Rao
| length7         = 3:37
}}

===Reception===
 
The soundtrack met with a positive response, with Avijit Ghosh of The Times of India mentioning, "Composer Amit Trivedi and lyricist Amitabh Bhattacharya are among the finest musical team in Bollywood. In Ek Main Aur Ekk Tu, they are again at their best. The seductive Auntyji and the racy title track have a delightful breeziness. And the more evocative tracks such as Gubbare Gubbare and Aahatein stay with you for long".  While praising the compositions, Joginder Tuteja of Bollywood Hungama commented that the album belonged to a "niche audience". He added that the songs Ek Main Aur Ekk Tu and Kar Chalna Shuru Tu would make the most impression.  Mitesh Saraf of Planet Bollywood, while summarizing his review wrote, "The album has some cool songs to chill out to and they should sound even better on screen." 

==Release==
The film released worldwide on 10 February 2012, coinciding with Valentines Day celebrations. Upon release, Ek Main Aur Ekk Tu received positive reviews and was a moderate box office success. 
 trailers of Ek Main Aur Ekk Tu faced trouble with the censor board before release. Initially, the word "sex" was objected by the board and asked to be removed. Later, a scene depicting Kapoor pinching Khans buttocks was considered inappropriate for television promos and had to be removed. 

===Critical reception=== DNA India highly recommended the film and gave 4 stars out of 5 concluding, "All in all, Ek Main Aur Ekk Tu is a delightful film that keeps getting better along the way, and ends fabulously. Clocking under two hours, the film is a breezy watch that will leave you with a smile."  MiD DAY gave 3.5 stars out of 5, commenting, "The climax is brave, unpredictable and real. Go watch! Ek Main Aur Ekk Tu is a small packet of joy."  India Today too gave 3.5 stars out of 5 saying, "Ek Main Aur Ekk Tu is a gentle little film, where even the regulation song with Americans dancing to Bollywood music in the middle of Vegas dont look so bad."  A review carried by Birmingham Mail mentioned,
 Daily Bhaskar gave the film 3 out of 5 stars, and concluded saying, "Overall, Ek Main Aur Ekk Tu is a typical popcorn entertainment with super-amazing climax which puts it in a different league altogether."  Sukanya Verma for Rediff too gave the film 3 stars out of 5, and commented, "Ek Main Aur Ekk Tu is neither on the epic side like Dharma Productions great, grand ancestors nor weighed down by an overload of pop culture references of those that define the genre."    Komal Nahta for Koimoi.com too gave the film 3 out of 5 stars, saying Ek Main Aur Ekk Tu is an entertaining and enjoyable fare for the city audience.  Rajeev Masand of CNN-IBN gave it 3 out of 5 stars and said "Ek Main Aur Ekk Tu is a narrative that unfolds mainly through dialogue, and the lighter moments come at you as the odd couple gets to know each other. The humor hits the mark many times and falls flat occasionally, but the movie doesnt grate because the characters arent trying too hard to be cute."  The Economic Times gave the film 2.5 stars out of 5, and said "Its perfect in whatever it offers. But what it offers is quite average in volume."  Shubhra Gupta of The Indian Express gave 2.5 stars out of 5 sand said "The film passes by pleasantly enough past all its expected roadsigns, providing a smile and an occasional chuckle but making you wish for more newness." 

On the contrary, Kunal Guha of Yahoo! gave 2 stars out of 5, saying "this film doesnt subscribe to every cliché associated with this auspicious day for getting Main and Tu to become Hum." 

===Box office=== Delhi NCR, Punjab and Mysore.  It collected   nett on Saturday, while   on Sunday, hence totaling its first weekend collection around  . 

On Monday, the film netted around  , with a 50% drop in collections as compared to first day.  On Tuesday, Ek Main Aur Ekk Tu had a jump and earned around   nett, owing to Valentines Day.  At the end of its first week, the film earned a total of  .     The film added   and   during its second and third week respectively, taking its total to around   nett.  It eventually emerged as a moderate commercial success. 

The film also had a decent opening at the international box office. In the UK, in its opening weekend, the film collected £  in 53 screens. The film fared even better in the United States, collecting $1.7 million from 111 screens in its opening weekend. 

==References==
 

==External links==
  
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 