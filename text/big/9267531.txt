Dracula vs. Frankenstein
(not to be confused with the 1971 Jesus Franco film Dracula Contra Frankenstein).... 
 
{{Infobox film
| name = Dracula vs. Frankenstein
| image = Draculavsfrankposter.jpg
| caption =
| director = Al Adamson
| producer = Al Adamson Mardi Rustam Mohammed Rustam Samuel M. Sherman John Van Horne
| writer = William Pugsley Samuel M. Sherman 
| starring = J. Carrol Naish Lon Chaney, Jr. Anthony Eisley Regina Carrol Greydon Clark
| music = William Lava
| cinematography = Paul Glickman Gary Graver
| editing = Irwin Cadden
| distributor = Troma Entertainment
| released =  
| runtime = 90 minutes
| language = English
| country = United States
| budget =
}}

Dracula vs. Frankenstein is a 1971 United States horror film directed by Al Adamson. It was theatrically released as Blood of Frankenstein in the UK, and was retitled Revenge of Dracula on early VHS releases.

==Plot==
 
A mad scientist (J. Carrol Naish) descended from  Dracula (played by Roger Engel under the pseudonym "Zandor Vorkov") comes to the scientist promising to revive Frankensteins monster in return for a serum which will grant him immortality.

==Cast==
*Lon Chaney, Jr. as Groton
*J. Carrol Naish as Dr. Frankenstein
*Zandor Vorkov (Roger Engel) as Count Dracula John Bloom as Frankensteins monster Jim Davis as Sheriff Brown
*Regina Carrol as Judith Fontaine
*Russ Tamblyn as Rico
*Anthony Eisley as Mike Howard
*Anne Morrell as Samantha
*Maria Lease as Joan
*Angelo Rossitto as Grazbo the Evil Dwarf
*Forrest J Ackerman as Dr. Beaumont
*Gary Graver, the future cinematographer for Orson Welles, is an extra in the film.

==Production==
 
This was Lon Chaney, Jr.s final horror film role and J. Carrol Naishs last film. Chaney filmed his part in mid-1969 when the film was titled Blood Seekers. Naish filmed additional footage in 1970 when Dracula and the Frankenstein Monster were added to the story (in his confrontation scene with Dracula, he appears noticeably older).  Director Adamsons wife, Regina Carrol, appears in the film as one of the people who discover the two title monsters. The film was released on DVD by Troma Entertainment.  

Two other films titled Dracula vs Frankenstein were made around the same time as Adamsons film. In 1969, Spanish horror film icon Paul Naschy starred in Los Monstruos del Terror which was later released on VHS as Dracula vs Frankenstein. Meanwhile in 1971, famed Spanish schlock film director Jesus Franco turned out his Dracula vs Frankenstein, apparently unaware that Al Adamson was already using that title.

==Reception==
 
The film was met with negative reception from critics. 

==Notes==
 

==External links==
 
* 

 
 
 

 
 
 
 
 
 
 
 
 