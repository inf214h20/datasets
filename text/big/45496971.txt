Legend of a Rabbit: The Martial of Fire
{{Infobox film
| name           = Legend of a Rabbit: The Martial of Fire
| image          = Legend of a Rabbit The Martial of Fire poster.jpg
| alt            = 
| caption        = 
| film name      = 兔侠之青黎传说 
| directors      = Ma Yuan Dong Dake
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| production companies = TianJin BeiFang Film Group Co., LTD Beijing Century Butterfly Animation Production Co.,Ltd Beijing Luyi Jiuying Pictures Media Co.,Ltd China Beijing Television Station Toonmax Media Co.,Ltd Tianjin Radio and television
| distributors   = Beijing Wanzhong Canlan Entertainment Co.,Ltd Beijing Xinghe Lianmeng Entertainment Co.,Ltd YL Pictures American Aurora Pictures International Ltd
| released       =   
| runtime        = 90 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          =   (China)   
}} Chinese animated action adventure film directed by Ma Yuan and Dong Dake. It was released on February 21, 2015. 

==Voice cast==
*Huang Lei
*Yang Zishan
*He Yunwei
*Wang Jinsong
*Wang Yuebo
*Ma Yuan
*Zhao Huishan
*Zhou Qixun
*Wang Di
*Li Xiaochen
*Zhang Yixin
*Li Zhou

==Reception==
By February 23, the film had earned   at the Chinese box office. 

==See also==
*Legend of a Rabbit

==References==
 

 
 
 
 
 
 
 
 
 


 
 
 
 