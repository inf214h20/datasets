A Time for Defiance
{{Infobox film
| name           = La hora de los valientes
| image          =
| caption        =
| director       = Antonio Mercero
| producer       = Enrique Cerezo
| writer         = Horacio Varcarcel Antonio Mercero
| starring       = Gabino Diego Leonor Watling Adriana Ozores Luis Cuenca Héctor Colomé
| music          = Bingen Mendizábal
| cinematography = Jaume Peracaula
| editing        = José María Biurrún
| distributor    = Sogecine film
| released       =  
| runtime        = 124 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
A Time for Defiance ( , literally The Hour of the Brave) is a 1998 Spanish war drama film directed by Antonio Mercero about the Spanish Civil War. Adriana Ozores won a Goya Award as Best Supporting Actress. It was also entered into the 21st Moscow International Film Festival where it won the Special Silver St. George.   

==Plot==
In Madrid, during the bombing of November 1936, in the Spanish Civil War, the Republican Government decided on the evacuation of paintings from the Prado Museum. Manuel (played by Gabino Diego), a 28 year old security guard, finds a self-portrait of Goya abandoned in one corner.  He hides the painting and flees the bombing of his house.

==Cast==
* Gabino Diego as Manuel
* Leonor Watling as Carmen
* Adriana Ozores as Flora
* Luis Cuenca as Melquíades
* Héctor Colomé as Lucas
* Ramón Agirre as Portero
* Aten Soria as Filo
* Juan José Otegui as Professor Miralles
* Josep Maria Pou as Heliodoro (as José María Pou)
* Txema Blasco as Cuñado Professor
* Ramón Langa as Director Bellas Artes

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 