Mortal Kombat (film)
{{Infobox film
| name = Mortal Kombat
| image = Mortal Kombat poster.jpg
| caption = Theatrical release poster
| writer = Kevin Droney
| starring = Robin Shou Linden Ashby Bridgette Wilson Christopher Lambert Cary-Hiroyuki Tagawa Talisa Soto
| director = Paul W. S. Anderson
| producer = Lauri Apelian Lawrence Kasanoff
| based on =  
| music = George S. Clinton
| cinematography = John R. Leonetti
| editing = Martin Hunter
| studio = Threshold Entertainment
| distributor = New Line Cinema
| released =   (United States)
| budget = $18 million
| runtime = 101 minutes
| country = United States
| language = English
| gross = $122.1 million   
}}
 fantasy martial arts film written by Kevin Droney, directed by Paul W. S. Anderson, and starring Robin Shou, Linden Ashby, Bridgette Wilson, Christopher Lambert, Cary-Hiroyuki Tagawa and Talisa Soto. It is a loose adaptation of the early entries in the fighting game series Mortal Kombat.
 original game of the same title, but it was also inspired by and incorporates elements of 1993s follow-up game Mortal Kombat II.

Mortal Kombat, a co-production between   and  , and a prequel animated film The Journey Begins.

Despite receiving mixed reviews by critics, Mortal Kombat spent three weeks as the number-one film at the U.S. box office, earning over $122 million worldwide. Due to its commercial success, Threshold Entertainment followed with a 1997 sequel film   and created two spin-off television series,   and  . The Mortal Kombat film reboot was announced by New Line Cinema in 2011, but as of early 2015 it remains in development hell.

==Plot== Elder Gods to limit invasions between the realms of the universe. If the realm of Outworld wins Mortal Kombat ten consecutive times, its Emperor Shao Kahn will be able to invade and conquer the realm containing the Earth.
 Australian crime lord Kano (Mortal Kombat)|Kano; and Cage, having been branded as a fake by the media, seeks to prove otherwise.
 Princess Kitana, Reptile to spy on her. Liu defeats his first opponent and Sonya gets her revenge on Kano by snapping his neck. Cage encounters and barely beats Scorpion (Mortal Kombat)|Scorpion. Liu engages in a brief duel with Kitana, who secretly offers him cryptic advice for his next battle. Lius next opponent is Sub-Zero (Mortal Kombat)|Sub-Zero, whose defense seems impregnable because of his freezing abilities, until Liu recalls Kitanas advice and uses it to kill Sub-Zero.
 Goro enters the tournament and mercilessly crushes every opponent he faces. One of Cages peers, Art Lean, is defeated by Goro as well and has his soul taken by Shang Tsung. Sonya worries that they may not win against Goro, but Raiden disagrees. He reveals their own fears and egos are preventing them from winning the tournament.

Despite Sonyas warning, Cage comes to Tsung to request a fight with Goro. The sorcerer accepts on the condition that he be allowed to challenge any opponent of his choosing, anytime and anywhere he chooses. Raiden tries to intervene, but the conditions are agreed upon before he can do so. After Shang Tsung leaves, Raiden confronts Cage for what he has done in challenging Goro, but is impressed when Cage shows his awareness of the gravity of the tournament. Cage faces Goro and uses guile and the element of surprise to defeat the defending champion. Now desperate, Tsung takes Sonya hostage and takes her to Outworld, intending to fight her as his opponent. Knowing that his powers are ineffective there and that Sonya cannot defeat Tsung by herself, Raiden sends Liu and Cage into Outworld in order to rescue Sonya and challenge Tsung. In Outworld, Liu is attacked by Reptile, but eventually gains the upper hand and defeats him. Afterward, Kitana meets up with Cage and Liu, revealing to the pair the origins of both herself and Outworld. Kitana allies with them and helps them to infiltrate Tsungs castle.

Inside the castle tower, Shang Tsung challenges Sonya to fight him, claiming that her refusal to accept will result in the Earth realm forfeiting Mortal Kombat (this is, in fact, a lie on Shangs part). All seems lost for Earth realm until Kitana, Liu, and Cage appear. Kitana berates Tsung for his treachery to the Emperor as Sonya is set free. Tsung challenges Cage, but is counter-challenged by Liu. During the lengthy battle, Liu faces not only Tsung, but the souls that Tsung had forcibly taken in past tournaments. In a last-ditch attempt to take advantage, Tsung morphs into Chan. Seeing through the charade, Liu renews his determination and ultimately fires an energy bolt at the sorcerer, knocking him down and impaling him on a row of spikes. Tsungs death releases all of the captive souls, including Chans. Before ascending to the afterlife, Chan tells Liu that he will remain with him in spirit until they are once again reunited.

The warriors return to Earth realm, where a victory celebration is taking place at the Shaolin temple. The jubilation abruptly stops, however, when Shao Kahns giant figure suddenly appears in the skies. When the Emperor declares that he has come for everyones souls, the warriors take up fighting stances.

==Cast==
* Robin Shou as Liu Kang, a former Shaolin monk, who enters the tournament to avenge his brothers death. As in most of the games in the Mortal Kombat series, Liu Kang is the main protagonist. 
* Linden Ashby as Johnny Cage, a Hollywood superstar that enters the tournament to prove to the world that his skills are for real. Ashby trained in karate and tae kwon do especially for this film. Despite the intensity of the fight scenes coupled with the actors performing most of their own stunts, on-set injuries were surprisingly minimal; the only notable occurrence was a mildly bruised kidney Ashby suffered while shooting Cages fight scene with Scorpion.  Special Forces officer from Austin (Texas)|Austin, Texas whos in hot pursuit of Kano, the criminal who killed her partner. Cameron Diaz was originally set to play Sonya, but she broke her wrist during a martial arts training prior to shooting and was replaced by Bridgette Wilson, who was jokingly nicknamed "RoboBabe" during production by director Paul W. S. Anderson.  Wilson performed all her own stunts, including fight scenes.
* Christopher Lambert as Raiden (Mortal Kombat)|Raiden, god of thunder and protector of Earthrealm who guides the warriors on their journey. He desires to aid the heroes in defending Earthrealm, but as he himself is not mortal, he is not permitted to participate in the tournament and may only advise them and act to prevent cheating.
* Trevor Goddard as Kano (Mortal Kombat)|Kano, a mercenary that joins forces with Shang Tsung. Princess Kitana, the Outworld emperors stepdaughter who decides to help the Earth warriors. Soto had previously appeared alongside Tagawa in Licence to Kill (1989).
* Cary-Hiroyuki Tagawa as Shang Tsung, a powerful, sadistic and treacherous sorcerer, he is the films main antagonist who killed Liu Kangs brother Chan. Tagawa was the filmmakers first and only choice for the role; he was instantly selected after he came to his audition in costume, and read his lines while standing on a chair. 
* François Petit as Sub-Zero (Mortal Kombat)|Sub-Zero, one of Shang Tsungs warriors who, as his name implies, possesses the ability to freeze. The rivalry between Sub-Zero and Scorpion is only briefly mentioned by Shang Tsung at the beginning of the movie.
* Chris Casamassa as Scorpion (Mortal Kombat)|Scorpion, one of Shang Tsungs warriors whose trademark spear from the games was changed to a snake-like entity that shot from a slit in his palm. The character was voiced by the Mortal Kombat games co-creator Ed Boon.
* Keith Cooke as Reptile (Mortal Kombat)|Reptile, a creature who is serving Shang Tsung. Reptiles lizard form was rendered with the use of computer-generated imagery (CGI), while the characters human form was portrayed by Keith Cooke.  Reptiles vocal effects were provided by Frank Welker. Steve James was originally cast to play Jax, but the actor died a year before production on the film began.  Goro
** Kevin Richardson voices Goro (Mortal Kombat)|Goro, the undefeated Mortal Kombat champion. His vocal effects are provided by Frank Welker.
* Kenneth Edwards as Art Lean, a colleague of Johnny Cage who competes, and is killed, in the Mortal Kombat tournament.  Steven Ho as Liu Kangs younger brother Chan, who is killed by Shang Tsung in the films opening scene. 
* Peter Jason as Johnny Cages sensei, Master Boyd. 
* Hakim Alston as a fighting monk on the beach.
* Frank Welker voices the unnamed Emperor of Outworld (Shao Kahn) who appears at the end of the film. Welker also voiced Reptile and Goro.

Sandy Helberg is briefly seen in the beginning of the film as the director of Cages latest movie. Originally, this part was to be a cameo appearance by Steven Spielberg, but scheduling conflicts forced him to back out; the "director" character in this scene still resembles Spielberg. 

==Production== Kung Lao. Originally not included in the movie, Reptile was added in response to focus groups being unimpressed with the original fights in the film.    Robin Shou and Paul W. S. Anderson noted that neither knew what Reptiles lizard form would look like until after filming, making the pre-fight sequence difficult to shoot. 
 Friendship move in MKII. When Reptile assumes his human form, the voice of Shao Kahn - sampled directly from the second game - can be heard announcing "Reptile". The Shadow Priests, seen before the final battle, were first seen in the second game as part of two of the backgrounds.
 Kaiser steel bows of the boats were fitted with ornamental dragon-head carvings and used in the movie as the fighters secondary transport to Shang Tsungs island from his Junk (ship)|junk.  The film was originally scheduled for a May 1995 U.S. release, but was pushed back to August.    According to co-producer Larry Kasanoff, this was because New Line Cinemas executives felt the film had the potential to be a summer hit.  It was released on October 20 in the United Kingdom, and on December 26 in Australia.

==Other media==

===The Journey Begins=== CGI to explain the origins behind some of the movies main characters, as well as a 15-minute behind-the scenes documentary of the theatrical release. The film was included on the Mortal Kombat Blu-ray released in April 2011.

The plot follows Liu Kang, Johnny Cage and Sonya Blade—also the three main characters in the live action movie—traveling on a mysterious boat en route to the Mortal Kombat tournament. On the way they meet Raiden, who provides them with some hints about how to survive the tournament and defeat Shang Tsung and his army of Tarkatan minions. Upon arriving at the island where the battles takes place, Raiden retells the origins of Shang Tsung, Goro, Scorpion, Sub-Zero and the Great Kung Lao in between fight scenes.

===Mortal Kombat: A Novel===
A novelization of the movie by Martin Delrio was released through Tor Books. It is based on the early version of the films script and such it includes several deleted or unfilmed scenes, such as a fight between Sonya Blade and Jade (Mortal Kombat)|Jade.

===Music===
  is the instrumental score album with the music by   is the compilation album released by  .

The hit Mortal Kombat theme was composed by  .

==Reception==

===Box office===
Mortal Kombat opened on August 18, 1995, and cruised into the top box-office spot with $23.2 million, nearly eight times the opening amount of the only other new release that weekend,  ,  , and  . 

===Critical response===
The film holds a score of 33% on Rotten Tomatoes, with the consensus "despite an effective otherwordly atmosphere and appropriately cheesy visuals, Mortal Kombat suffers from its poorly constructed plot, laughable dialogue, and subpar acting."  As of 2014, the film holds a "mixed or average" rating of 58/100 on Metacritic. 
 Siskel & Hong Kong Kevin Thomas of Los Angeles Times gave the film a much more positive review, writing that "as impressive as the special effects are at every turn, even more crucial is Jonathan Carlsons superb, imaginative production design, which combines Thailand exteriors with vast sets that recall the barbaric grandeur of exotic old movie palaces and campy Maria Montez epics. John R. Leonettis glorious, shadowy camera work and George S. Clintons driving, hard-edged score complete the task of bringing alive the perilous  ." 

==Legacy==

===Sequel===
The sequel   was released in 1997, directed by John R. Leonetti (cinematographer of the first Mortal Kombat) and starring the returning Robin Shou and Talisa Soto as well as Brian Thompson, Sandra Hess, Lynn "Red" Williams, Irina Pantaeva, Marjean Holden and James Remar. Its storyline is largely an adaptation of Mortal Kombat 3, following a band of warriors as they attempt to save Earth from Shao Kahn himself. Although the story picks up where the last film left off, most of the lead roles were recast.

In contrast to its predecessor, which was a box office success and marginally well received, Annihilation was critically panned and underperformed at the box office. As a result, development of the planned third installment halted and never progressed beyond pre-production.

===Spin-off TV series===
Two television series, the cartoon sequel   and the live-action prequel  , were produced by Threshold Entertainment between 1996 and 1999.

===Reboot===
In September 2011, it was reported that  , but was not involved in  , is returning to pen the story, while no actors, nor other crew have been confirmed. Story details known state that the film will not be an extension of the game, nor of Legacy. 
 Comic Con with Uziel and Mortal Kombat creator Ed Boon and to expect a very big origin story with the sensibility and realism of Rebirth and Legacy as opposed to the traditional Mortal Kombat mythology.    He said, "Ive always been a fan of properties like Batman where you can expand the universe in different directions. Mortal Kombat is big enough that you can allow for multiple different kinds of storytelling."   

But as for its continuity with the web series, Tancharoen said "you wont have to have seen all ten episodes previously - or have played the videogame - to understand the movie."  Shooting was expected to begin in March 2012  with a budget of well under $100 million and a release date of 2013, coordinated with the next installment of the video game series being produced by the same studios.  It was later delayed due to budget constraints and the director started working on the second season of Legacy until problems with the movie had been sorted out. In late 2012, Warner Bros executive Lance Sloan revealed that the Mortal Kombat movie will have a budget of between $40–50 million.  In October 2013, Tancharoen announced that he will not be directing the film. 

==References==
 

== External links ==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 