Major Chandrakanth (1993 film)
 
{{Infobox film
| name           = Major Chandrakanth
| image          = Major Chandrakanth.jpg
| image_size     =
| caption        =
| director       = Kovelamudi Raghavendra Rao|K. Raghavendra Rao
| producer       = Mohan Babu
| writer         = Paruchuri Brothers
| narrator       = Sharada Mohan Babu Ramya Krishna Nagma Brahmanandam  Amrish Puri Srihari
| music          = M. M. Keeravani
| cinematography = Ajay Vincent
| editing        =
| studio         = Sree Lakshmi Prasanna Pictures
| distributor    =
| released       = 23 April 1993
| runtime        = 
| country        = India
| language       = Telugu
| budget         =
}} Telugu patriotism|patriotic-drama Paruchuri brothers, lyrics composed by Jaladi-Gurucharan-Rasaraju, cinematography by Ajay Vincent and art by Bhaskara Raju. The film was released on 23 April 1993. The film was declared as a blockbuster film|blockbuster, and later went on to celebrate its silver jubilee. This film is perhaps a last glimpse in Sri N. T. Rama Raos prolonged film career.

==Songs==
* "Bunga Moothi" (Lyricist: Gurucharan; Singers: S. P. Balasubramanyam, K. S. Chitra|Chitra)
* "Lappam Tappam Gallaki" (Lyricist: Gurucharan; Singers: S. P. Balasubramanyam, Chitra)
* "Muddulato Onamalu" (Lyricist: M.M. Keeravani; Singers: Yesudas, Chitra)
* "Neekkavalsindi" (Lyricist: Gurucharan; Singers: Mano (singer)|Mano, Chitra)
* "Punyabhoomi Naadesam" (Lyricist: Jaladi Raja Rao|Jaladi; Singer: SP.Balasubramanyam)
* "Sukhibhava Sumangali" (Lyricist: Jaladi; Singers: S. P. Balasubramanyam, Chitra)
* "Uliki Padaku Allari Moguda" (Lyricist: Rasaraju; Singers: S. P. Balasubramanyam, Chitra)

==External links==
*  
*  

 
 
 
 


 