Delicious (film)
{{Infobox film
| name           = Delicious
| image          = Delicious (film).jpg
| image_size     =
| caption        = original film poster David Butler
| writer         = Guy Bolton Sonya Levien
| starring       = Janet Gaynor Charles Farrell Virginia Cherrill
| music          = George Gershwin Ernest Palmer
| editing        = Irene Morra Fox Film Corporation
| released       =  
| runtime        = 106 minutes
| country        = United States English
| budget         =
}} musical romantic David Butler,  with color sequences in Multicolor (now lost).

==Production background==
  ]] George and Ira Gershwin, including the introduction of Rhapsody in Rivets, later expanded into the Second Rhapsody by Gershwin, an imaginative and elaborate set piece. Gershwin also contributed other sequences for the score, but only a five-minute dream sequence called The Melting Pot and the six-minute Rhapsody in Rivets made the final cut. Fox Film Corporation rejected the rest of the score.
 Scottish girl America who Seventh Heaven Street Angel Lucky Star (1929). Gaynor won the first Academy Award for Best Actress for the first two and F. W. Murnaus Sunrise (film)|Sunrise.

==Cast==
{| width="67%"
|-
| width="50%" valign="top" |
*Janet Gaynor as Heather Gordon
*Charles Farrell as Larry Beaumont
*El Brendel as Chris Jansen
*Raul Roulien as Sascha
*Lawrence OSullivan as Detective OFlynn
*Manya Roberti as Olga
*Olive Tell as Mrs. Van Bergh
*Virginia Cherrill as Diana Van Bergh
*Mischa Auer as Mischa
*Marvine Maazel as Toscha
| width="20%" valign="top" |
 
|}

==Preservation status==

On December 14, 2011, Turner Classic Movies aired a print of the film restored by George Eastman House.

On September 22, 2013, it was announced that a musicological critical edition of the full orchestral score will be eventually released. The Gershwin family, working in conjunction with the Library of Congress and the University of Michigan, are working to make scores available to the public that represent Gershwins true intent.  

The score to Delicious is currently scheduled to be the seventh in a series of scores to be released. The project may take 30 to 40 years to complete, which means the edition may not be available until 2035 or later. It will be the first time this score has ever been published.
  

==References==
 

==See also==
*List of early color feature films

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 