Abhayam (1970 film)
{{Infobox film 
| name           = Abhayam
| image          =
| caption        =
| director       = Ramu Kariat
| producer       = Sobhana Parameswaran Nair
| writer         = Perumpadavam Sreedharan S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Madhu Sheela Raghavan Jose Prakash
| music          = Original Songs:  
| cinematography = EN Balakrishnan U Rajagopal
| editing        = K Narayanan
| studio         = Roopavani Films
| distributor    = Roopavani Films
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film, Raghavan and Jose Prakash in lead roles. The film featured original songs composed by V. Dakshinamoorthy and original musical score composed by Salil Chowdhury.    

==Cast==
  Madhu as Balakrishnan
*Sheela as Sethulakshmi Raghavan as Murali
*Jose Prakash as Vikraman
*Kottayam Santha as Devaki Amma
*Omanakuttan as Editor Prema as Sathi
*Sankaradi as Comrade Sankaran
*KP Pillai as Rikshawwala
*Krishnan Nair as Kunjunni Nair
*Devasya as Supplier Kuttappan
*Edie as Bhaskara Menon
*Kottayam Chellappan as Karunan
*Kshema as Student
*Mavelikkara Ponnamma as Saraswathi Amma
*Philomina as Sr. Briggitte
*Radhamani as Student
*S. P. Pillai as Pachu Pilla
*Sherin Sheela as Mini
*Sukumaran as Lecturer
*Veeran as Kunjukrishna Menon
*Vijayan Nair as Lecturer
*Hema  as Student
*Unni Menon as Lecturer
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Balamaniyamma, Changampuzha, Sreekumaran Thampi, G Sankara Kurup, Vayalar Ramavarma, Kumaranasan, Vallathol, Sugathakumari and P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ammathan Nenjil || B Vasantha || Balamaniyamma || 
|-
| 2 || Chumbanangalanumaathram || P Jayachandran || Changampuzha || 
|-
| 3 || Enteyeka dhanamangu || B Vasantha || Sreekumaran Thampi || 
|-
| 4 || Eriyum Snehaardramaam || P. Leela || G Sankara Kurup || 
|-
| 5 || Kaama Krodha Lobha || P Jayachandran, P. Leela, C Soman, CO Anto, T Soman, Varghese || Vayalar Ramavarma || 
|-
| 6 || Maattuvin Chattangale || MG Radhakrishnan || Kumaranasan || 
|-
| 7 || Nammude Mathaavu || Latha Raju || Vallathol || 
|-
| 8 || Neeradalathaagriham || S Janaki || G Sankara Kurup || 
|-
| 9 || Paarasparyashoonyamaakum || B Vasantha || Changampuzha || 
|-
| 10 || Paavam Maanavahridayam || P Susheela || Sugathakumari || 
|-
| 11 || Raavupoyathariyaathe || P Susheela || P. Bhaskaran || 
|-
| 12 || Shraanthamambaram || K. J. Yesudas || G Sankara Kurup || 
|-
| 13 || Thaarathilum Tharuvilum || V. Dakshinamoorthy || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 