The White Diamond
 
{{Infobox film
| name           = The White Diamond
| image          = The white diamond dvd.jpg
| caption        = The White Diamond DVD cover
| director       = Werner Herzog
| producer       = Marco Polo Film AG
| writer         = Annette Scheurich Rudolph Herzog
| narrator       = Werner Herzog
| starring       = Graham Dorrington Werner Herzog Dieter Plage
| music          = Ernst Reijseger
| released       = 2004
| runtime        = 90 min. English
}}
 forest canopies of Guyana. It features music composed by Ernst Reijseger, which was re-used in Herzogs 2005 film The Wild Blue Yonder.

Most of the film focuses on Dorringtons flights near Kaieteur Falls, in Guyana. Dorrington discusses the mechanics of his flight, as well as his own struggles with uncertainty and the "heaviness" he feels after the death of the cinematographer Dieter Plage. The film also explores the Kaieteur Falls themselves, a local man named Marc Anthony Yhap, a local diamond miner, and the White-tipped Swift|white-tipped swifts (Aeronautes montivagus) which roost in an inaccessible cave behind the falls.

The film holds ratings of 83% (based on 12 reviews) on the film review aggregator websites Metacritic and 94% (based on 18 reviews) on Rotten Tomatoes.  
==Soundtrack==
 
== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 