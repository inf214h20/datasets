Anokhi Ada
 
{{Infobox film
| name           = Anokhi Ada
| image          = Anokhi Ada.jpg
| caption        = 
| director       = Kundan Kumar
| producer       = Kundan Kumar
| writer         = Rafi Ajmeri Mushtaq Jalili Vishwanath Pande K.K. Shukla Mehmood Nazir Hussain
| music          = Laxmikant-Pyarelal
| cinematography = V. Durga Prasad
| editing        = Kamlakar Karkhanis
| studio         = Asha Studios Ranjit Studios Roop Tara Studios Shree Sound Studios
| distributor    = Kundan Films B4U Entertainment Eros Entertainment
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1973 Bollywood romance film directed by Kundan Kumar. The film stars Jeetendra, Rekha, Vinod Khanna.

==Cast==
*Jeetendra...  Rakesh
*Rekha ...  Neeta Gupta
*Vinod Khanna ...  Gopal
*Padma Khanna ...  Radhika Mehmood ...  Dr. Captain Bhushan
*Nazir Hussain ...  Lalaji (as Nazir Hussain)
*Manmohan Krishna ...  Mr. Gupta
*Kanhaiyalal ...  Ram Prasad Ramu (as Kanhayalal)
*Brahmachari ...  Sewakram (Dr. Bhushans compounder)
*Praveen Paul ...  Mrs. Kamla Gupta
*Keshav Rana ...  Govind
*Sanjana ...  Rosie (Gopals girlfriend)
*M.B. Shetty ...  Birju (as Shetty)
*Manmohan ...  Shambhu
*Jeevan ...  Khushiram

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Haal Kya Hai Dilon Ka"
| Kishore Kumar
|-
| 2
| "Tere Dil Mein Zara Si Jagah"
| Mohammed Rafi, Lata Mangeshkar
|-
| 3
| "Sundari Ae Hae Sundari"
| Kishore Kumar
|-
| 4
| "Jawani Tera Bol Bala"
| Manna Dey, Asha Bhosle
|-
| 5
| "Gum Gai Gum Gai"
| Asha Bhosle
|-
| 6
| "Title Music (Anokhi Ada)"
|
|}

==External links==
*  

 
 
 
 
 


 
 