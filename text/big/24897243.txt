Halloween Hall o' Fame
{{multiple issues|
 
 
}}

Halloween Hall o Fame is a television special that first aired on October 30, 1977 as part of The Wonderful World of Disney. The special featured Jonathan Winters as a security guard working late at the Walt Disney Productions studio on Halloween night. He is accompanied by his dog, Peanuts. Winters, bitter about working on Halloween night, stumbles upon the prop room at the studio and begins acting out scenes with various props. Eventually he finds a crystal ball containing a talking pumpkin, Jack o Lantern (also played by Winters). Jack o Lantern is hiding out from Halloween because it is no longer scary like it was back in "the old days". The dialogue between the security guard and the pumpkin is interspersed with Halloween-related Disney cartoons, that Jack o shows the security guard in his crystal ball and which the audience gets to enjoy.

List of cartoons: Trick or Donald plays a mean trick on them and the boys seek revenge on him with the help of a witch.
 Pluto on trial for tormenting cats featuring a jury of felines who instantly proclaim hes guilty. Thankfully, just as hes given the penalty, he wakes up, revealing it all to be a nightmare.

The third and final cartoon portrays the story of "The Legend of Sleepy Hollow", originally released as part of The Adventures of Ichabod and Mr. Toad and narrated by Bing Crosby. This cartoon takes up the rest and most of the special.

==References==
 
 

==External links==

 
 
 
 
 
 


 