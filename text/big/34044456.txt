Ice Age (2002 film)
 
 
{{Infobox film name = Ice Age image = Ice Age.jpg caption = Theatrical release poster border = yes director = Chris Wedge   Carlos Saldanha (co-director) producer = Lori Forte screenplay = Peter Ackerman story = Michael J. Wilson starring =  Ray Romano John Leguizamo Denis Leary music = David Newman editing = John Carnochan studio = Blue Sky Studios distributor = 20th Century Fox released =   runtime = 81 minutes country = United States language = English budget = $59 million gross = $383.3 million 
}} comedy adventure film directed by Carlos Saldanha and Chris Wedge from a story by Michael J. Wilson. It was produced by Blue Sky Studios and distributed by 20th Century Fox. The film features the voices of Ray Romano, John Leguizamo, Denis Leary and Chris Wedge and was nominated at the 75th Academy Awards for best animated feature. It shows the adventures of a sloth named Sid, a mammoth named Manny and a few other animals when the Earth was being flooded with glaciers.

This film was met with mostly positive reviews and was a   in 2006,   in 2009, and   in 2012. A fifth film, temporary called Ice Age 5, is scheduled for release on July 15, 2016.

==Plot== Manfred ("Manny"), an agitated mammoth who fights them off and is heading north. Not wanting to be alone and unprotected, Sid follows Manny. Meanwhile, Soto, the leader of a Smilodon pride, wants revenge on a group of humans by eating the chiefs baby son, Roshan, alive. Soto leads a raid on the human camp, during which Roshans mother is separated from the rest and jumps down a waterfall when cornered by Sotos lieutenant, Diego (Ice Age)|Diego. For his failure, Diego is sent to find and retrieve the baby.

Sid and Manny spot Roshan and his mother near the lake, having survived her plunge. The mother only has enough strength to trust her baby to Manny before she disappears. After much persuasion by Sid, they decide to return Roshan, but when they reach the human settlement, they find it deserted. They meet up with Diego, who convinces the pair to let him help by tracking the humans. The four travel on, with Diego secretly leading them to his pack for an ambush.

While having small adventures on their way, they reach a cave with several drawings made by humans. There Sid and Diego learn about Mannys past and his previous interactions with the human hunters, in which his wife and son were killed, leaving Manny a cynical loner. Later, Manny, Sid, Diego and Roshan almost reach Half-Peak but encounter a river of lava. Manny and Sid, along with Roshan, make it safely, but Diego struggles, about to fall into the lava. Manny rescues him, narrowly missing a fall into the lava himself. The herd takes a break for the night, and Roshan takes his first walking steps to Diego.

The next day, the herd approach the ambush, causing Diego to confess to Manny and Sid about the ambush, and he tells them to trust him. The herd battles Sotos pack, and a short fight ensues. As Soto closes in for the kill on Manny, Diego leaps and stops Soto, who wounds Diego in the process. Manny knocks Soto into a rock wall, causing several sharp icicles to fall on Soto, killing him. The rest of the pack then retreats. The group then mourns for the injury caused to Diego. Soon, Manny and Sid manage to return the baby to his tribe, and Diego rejoins them, as the group begins to head off to warmer climates.

20,000 years later, Scrat, frozen in the ice, ends up on the shores of a tropical island. When the ice slowly melts, the acorn is then washed away. He mistakenly triggers a volcanic eruption, after stomping the coconut on the ground due to frustration.

==Cast==
  prehistoric animals.  The animals can talk to and understand each other and are voiced by a variety of famous actors.  Like many films of prehistoric life, the rules of time periods apply very loosely, as many of the species shown in the film never actually lived in the same time periods or the same geographic regions.
 Manfred "Manny", a woolly mammoth, is voiced by Ray Romano
* Diego (Ice Age)|Diego, a Smilodon, is voiced by Denis Leary giant ground sloth, is voiced by John Leguizamo
* Scrat, a Cronopio (mammal)|"saber-toothed" squirrel, is voiced by Chris Wedge
* Soto, a Smilodon, is voiced by Goran Višnjić
* Zeke, a Smilodon, is voiced by Jack Black
* Oscar, a Smilodon, is voiced by Diedrich Bader
* Lenny, a Homotherium, is voiced by Alan Tudyk
* Carl, a Brontops, is voiced by Cedric the Entertainer
* Frank, a Brontops, is voiced by Stephen Root
* Rachel, a female giant ground sloth, is voiced by Jane Krakowski
* Jennifer, a female giant ground sloth, is voiced by Lorri Bagley

==Production==
 

===Development===
Ice Age was originally intended to be a dramatic, non-comedic hand-drawn animated film directed by Don Bluth and Gary Goldman and produced by Fox Animation Studios, However in 2000 Fox Animation Studios shut down due to the financial failure of Titan A.E., Don Bluth and Gary Goldman turned down the opportunity to direct the film. Blue Sky Studios got the opportunity with the Ice Age script to turn it into a computer animated comedy, Chris Wedge and Carlos Saldanha took over as the directors. Supposedly the reason Don Bluth refused to make the film is when 20th Century Fox said they wanted it to be CGI after the failure of 2D animation, Bluth refused due to his personal hate for fully CG animation and angrily walked away from the project. The drama was also dropped from the film because 20th Century Fox would only except it as a comedy. 

===Writing===
Writer Michael J. Wilson has stated on his blog that his daughter Flora came up with the idea for an animal that was a mixture of both squirrel and rat, naming it Scrat, and that the animal was obsessed with pursuing his acorn. Chris Wedge, director, is the voice of Scrat, but has no intelligible dialogue; the plan to have Scrat talk was quickly dropped, as he worked better as a silent character for comedic effect. The name Scrat is a combination of the words squirrel and rat, as Scrat has characteristics of both species; Wedge has also called him "saber-toothed squirrel." Scrats opening adventure was inserted because, without it, the first real snow and ice sequence wouldnt take place until about 37 minutes into the film. This was the only role intended for Scrat, but he proved to be such a popular character with test audiences that he was given more scenes, and has appeared in other movies. 

According to an interview with Jay Leno on July 12, 2012, Denis Learys character Diego originally died near the end of the film, which caused a negative reaction such as the test audience of children bursting into tears, so it was re-done. 

Originally, Sid was supposed to be a con-sloth and a hustler, and there were even two finished scenes of the character conning some aardvark kids and a very suggestive scene with two female sloths later in the movie. Sid was also supposed to have a female sloth named Sylvia chasing after him, whom he despised and kept ditching, however all of her scenes were removed. Some scenes of her were removed, while many scenes, which were finished, were retooled and re-animated for the final film. All the removed scenes of her can be seen on the "Super Cool Edition" DVD. 

===Casting===
For mammoth Manny, the studio was initially looking at people with big voices.    James Earl Jones and Ving Rhames were considered, but they sounded too obvious and Wedge wanted more comedy.     Instead, the role was given to Ray Romano because they thought his voice sounded very elephant-like. Wedge described Romanos voice as "deep and his delivery is kind of slow, but hes also got a sarcastic wit behind it."   

John Leguizamo was cast as Sid, he tried 30 different voices for Sid. After viewing a documentary about sloths, he learned that they store food in their mouths; this led to him wondering what he would sound like with food in his mouth. After attempting to speak as if he had food in his mouth, he decided that it was the perfect voice for Sid. 

All the actors were encouraged to improvise as much as possible to help keep the animation spontaneous. 

===Animation===
Blue Sky Studios has engineers on its staff who understand the physics of sound and light and how these elements will affect movement in characters. 

The responsibility for animating Sids snowboard sequence was given to animators who went snowboarding in real life. 

==Release==
The film was released on March 15, 2002.

===Box office===
The film had a $46.3 million opening weekend, a large number not usually seen until the summer season, and way ahead of Foxs most optimistic projection of about $30 million. Ice Age broke the record for a March opening (first surpassed in 2006 by its sequel,  ) and was the then-third-best opening ever for an animated feature—after Monsters, Inc. ($62.6 million) and Toy Story 2 ($57.4 million).  Ice Age finished its domestic box office run with $176,387,405 and grossed $383,257,136 worldwide, being the 9th highest gross of 2002 in North America and the 8th best worldwide at the time. 

===Critical reaction===
Ice Age was released into theaters on March 15, 2002 and was met with generally positive reviews from critics (making it the best reviewed film in its later-existing franchise).   had a score of 60% out of 31 reviews.  The film was nominated an Academy Award for Best Animated Feature, but lost to Spirited Away.    Roger Ebert of the Chicago Sun-Times gave the film 3 stars out of 4 and wrote "I came to scoff and stayed to smile".

CinemaScore polls conducted during the opening weekend, cinema audiences gave Ice Age an average grade of "A" on an A+ to F scale.   

The film was nominated for AFIs 10 Top 10 in the "Animation" genre. 

===Home media===
Ice Age was released on DVD and VHS on November 26, 2002. Both releases included a short film Gone Nutty, featuring Scrat from the film.  The film was released on Blu-ray on March 4, 2008, and beside Gone Nutty, it included 9 minutes of deleted scenes. 

==Video game==
 
A video game tie-in was published by Ubisoft for the Game Boy Advance, and received poor reviews.  

==Sequels==
 
*   was released on March 31, 2006. The film focuses on the melting of a dam (due to, as Sid puts it at the end of the first film, global warming) and the impending flood.
*   was released on July 1, 2009. The film focuses on dinosaurs being discovered underground.
*   was released in July 13, 2012. The film focuses on the continental drift on Earth.
* Ice Age 5 is set to be released on July 15, 2016.   

==See also==
* List of animated feature-length films
* List of computer-animated films

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 