If You Build It
{{Infobox film
| name           = If You Build It
| image          = If_You_Build_It_(film)_poster.jpg
| caption        = film poster
| director       = Patrick Creadon
| producer       = Neal Baer, Christine OMalley
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Emily Pilloton, Matt Miller
| starring       = 
| music          = Peter Golub
| cinematography = George Desort
| editing        = Nick Andert 	
Douglas Blush 	
Daniel J. Clark
| studio         = OMalley Creadon Productions
| distributor    = Long Shot Factory
| released       =     
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
If You Build It is a 2013 documentary directed by Patrick Creadon, {{cite news
  | title = Film Review: If You Build It
  | publisher = Architectural Record Windsor and surrounding Bertie County, North Carolina, the states poorest county. {{cite news
  | title = Emily Pilloton Talks Inspiring Students Through Design in ‘If You Build It’
  | publisher = Way Too Indie, February 25, 2014, Bernard Boo
  | url = http://waytooindie.com/interview/emily-pilloton-talks-inspiring-students-through-design-in-if-you-build-it/}} 

The documentary follows a year in the life of an innovative, design-based high school program, culminating with the design and sixteen-week construction of a farmers market pavilion, the only farmers market pavilion in the U.S. designed and built by high school students. {{cite news
  | title = Windsor Super Market
  | publisher = Project H
  | url = http://www.projecthdesign.org/projects/windsor-super-market/}} 

The films title is a truncated reference to the catchphrase "if you build it, they will come," from the 1989 film, Field of Dreams. {{cite news
  | title = IF YOU BUILD IT WILL THEY COME? CAN A HIGH SCHOOL CLASS IN RURAL AMERICA TRANSFORM THEIR TOWN WITH ARCHITECTURE?
  | publisher = Architectural Review, 16 January 2014, Phineas Harper
  | url = http://www.architectural-review.com/comment-and-opinion/if-you-build-it-will-they-come/8657683.article}} 

==Synopsis==
In 2010, Superintendent of Public Schools for Bertie County, Chip Zullinger (1951-2014) had been hired to address the school districts serious shortcomings.   After completing four large architectural projects with designer Emily Pilloton (author of the 2009 book Design Revolution) and architect Matt Miller,  Zullinger invited the two to create a high school curriculum.
 shop class for the 21st century, where the design process could address the communitys problems and its own sense of possibility. Named after its focus on "humanity, habitats, health and happiness,” {{cite news
  | title = Film Review: ‘If You Build It’
  | publisher = Variety, JANUARY 9, 2014, Geoff Berkshire
  | url = http://variety.com/2014/film/reviews/film-review-if-you-build-it-1201042309/}}  the curriculum "empowered young people to become creative problem solvers and at the same time encourage them to become more active citizens." {{cite news
  | title =‘If You Build It’
  | publisher = Sundance, Film Forward Variety quoted Pilloton, saying the projects purpose was to "plant small seeds in our students that years from now could result in a new kind of resource." 

Pilloton and partner Miller, worked with 10 (initially 13)  high school students at Bertie Early College High School through the year-long,  full-scale design/build curriculum &mdash; following six design rules: there is no design without critical action; we work with’ not for; we start locally and scale globally; create systems, not stuff; document, share and measure; and finally build.  The students began the school year  {{cite news
  | title = Review, If You Build It
  | quote = Day one, Matt and Emily have them getting their hands dirty — very dirty, making water filtration systems out of mud and cow patties. 
  | publisher = Beliefnet.com, Nell Minow
  | url = http://www.beliefnet.com/columnists/moviemom/2014/02/if-you-build-it.html}}  {{cite news
  | title = KNOT A PROBLEM: ACTIVATING THE HAND AND MIND
  | quote = Last year, the first day of Studio H consisted of dirt, coffee grounds, cow dung, and fire (we made DIY water filters). 
  | publisher = Studio H, January 10th, 2012 cow pies. {{cite news
  | title = Film Review: ‘If You Build It’
  | quote = The Studio H curriculum starts the school year with simple projects, designed to build skills and fill the kids with a sense of wonder. Water purifiers are made from clay and cow pies. 
  | publisher = Film Journal International, January 9, 2014, Sara Sluis
  | url = http://www.filmjournal.com/filmjournal/content_display/reviews/specialty-releases/e3iaf88d7e3fce6c4334acff569f0bf42a0}}  After designing, constructing and selling corn hole boards, the students follow with full-scale chicken coops of their own design and culminate the year with the design and construction of an open-air farmers market, {{cite news
  | title = Film Review: ‘If You Build It’
  | quote = Miller and Pilloton, who were co-workers and romantic partners, crafted a highly supportive, hands-on workshop in which their increasingly motivated students designed and built a series of community-oriented projects, culminating in a farmers market pavilion. 
  | publisher = Chicago Tribune,February 06, 2014, Gary Goldstein
  | url = http://articles.chicagotribune.com/2014-02-06/entertainment/ct-if-you-build-it-20140206_1_happiness-review-matthew-miller}}  which subsequently became known as the Windsor Super Market (depicted on the movie poster above, right).

:::See: Geolocation of the Windsor Super Market:  

The project team and the community had identified a new farmers market as a viable and desirable community project, and under the leadership of Pilloton and Miller the students focused on developing design and construction skills: critical thinking, research (e.g., analyzing Buckminster Fullers Dymaxion House), sketching, and drafting, along with welding and real-world construction trades. Pilloton and Miller in turn completed construction documents and specifications, worked with a structural engineer and filed for construction permits &mdash; while also preparing and conducting three hours of daily classroom instruction. {{cite news
  | title =Interview with Matthew Miller: Revisiting the "If You Build It" Documentary
  | publisher = Association of Architecture Organizations, MARCH 28, 2014
  | url = http://www.aaonetwork.org/news/articles/interview-matthew-miller-revisiting-if-you-build-it-documentary}} 

The film backtracks to tell the story of Matt Miller and his thesis project from the Cranbrook Academy, where he designed and constructed along with classmate Thomas Gardner a 900 sf, two-story infill house. {{cite news
  | title = Cranbrook grads build distinctive single-family home in Poletown
  | publisher = Model Media, OCTOBER 23, 2007
  | url = http://www.modeldmedia.com/devnews/cranbrook11607.aspx}}   The house was ultimately abandoned, {{cite news
  | title = If You Build It, About the Subjects
  | publisher = If You Build It, Official Site
  | url = http://www.ifyoubuilditmovie.com/about/about-the-subjects.html}}  leading Miller to conclude the project failed because the end-user was insufficiently engaged and invested in process.

 

Together the Project H team identified student Stevie Mizelles preliminary concept model to develop further, subsequently completing construction of the farmers market to approximately 75% before the school year ended.  The design features a large wood platform for vendor booths with vertical wood trusses supporting a rectilinear roofed canopy with wood slats designed to filter and control solar glare; a main floor at the same height above grade as a pickup truck bed, allowing vendors to back up to the "loading dock" at each vendor stall and walk their goods directly off their truck into their booth; and a long ramp at the front to allow pedestrian accessibility and to provide a symbolic front porch for visitors. {{cite news
  | title =RECAP OF GRAND OPENING CEREMONY
  | publisher = Studio H, October 4, 2011
  | url = http://www.studio-h.org/recap-of-grand-opening-ceremony}} 

Early in the school year, Pilloton and Miller chose to continue with the project after Zullinger, their prime backer, was dismissed by the school board, and the board became unwilling to underwrite the curriculum and withdrew their offered salaries.  Pilloton and Miller continued to work with the students and subsequently completed construction of the farmers market after the school year ended.  The Windsor Super Market opened in October 2011.

Students featured in Studio H were Erick Bowen, Rodecoe Dunlow, Kerron Hayes, Anthony Johnson, Stevie Mizelle, Cameron Perry, Cj Robertson, Jamesha Thompson, Colin White and Alexia Williams.  The film also features County Commissioner Ron Wesson as well as volunteer Eric Wandmacher (friend of Pilloton and Miller) and Superintendent of Public Schools, Chip Zullinger. {{cite news
  | title =If You Build It, IMDB, Full Cast
  | publisher = IMDB
  | url = http://www.imdb.com/title/tt2322482/fullcredits?ref_=tt_ov_st_sm}} 

==Background==

{{rquote|right|
My goal in going in to Bertie County was not to go in and just design for them, but to really teach their kids about the design process and do something for the community with their high school students. That, in a way, transcends a lot of this social design movement stuff... and hopefully   the next phase of what that movement can be. &ndash; Matthew Miller  }}
Prior to one-year Studio H curriculum in Bertie County, Pilloton and Miller had completed four architectural projects with the school board, as backed by Superintendent Zullinger: four playgrounds, three computer labs, a weight room for the football team, and a county-wide graphic campaign.  Friction on the school board developed with another Zullinger-backed project, "Connect Bertie," a project to bring free county-wide internet connectivity by paying an outside entity, Century Link, to provide the internet services at a cost of $2 million annually.  The tension culminated with Zullwingers dismissal, followed a reversal of the dismissal and then acceptance of his resignation.  Without his support and the offered salaries, Zullwingers departure directly threatened the Studio H project. {{cite news
  | title = Zullinger Resigns as School Superintendent – Aug. 4, 2010
  | publisher = Bertie Ledger Advance, Barry Ward, January 24, 2011
  | url = http://www.ifyoubuilditmovie.com/about/about-the-subjects.html}} 

Other tensions arose, and in a 2013 interview, Pilloton identified a racially and politically charged context that surrounded the curriculum and the project &mdash; describing Bertie County as a place where "hundreds of years of slavery and institutionalized racism is very much still alive and kicking."  On release of the film, Pilloton and Miller said "they said they still believe it was fear of change – not lack of money in a budget that topped $30 million – that ultimately led to the boards decision." {{cite news
  | title = Community Spotlight: Bertie County Schools featured in new documentary
  | publisher = WNCT.com, Feb 03, 2014
  | url = http://www.wnct.com/story/24437206/community-spotlight-bertie-county-schools-featured-in-new-documentary}} 

Pilloton had grown up near Mt. Tamalpais, California, attributing the German toy Quadro with inspiring her interest in design.   She later studied at the College of Environmental Design, UC Berkeley and the Art Institute of Chicago, during a period when there was no curriculum but when there was a 100,000 sf shop class. 

Matt Miller was born in West Virginia and has architecture degrees from the University of Tennessee and Cranbrook Academy of Art.  He has taught architecture and design at the Rhode Island School of Design, the College of Creative Studies, and UC Berkeley &mdash; and worked with Architecture for Humanity.  Miller was the first student at the Cranbrook Academy to design and also build his thesis project (along with classmate Thomas Gardner): a 900 sf, two-story block infill house {{cite news
  | title = Cranbrook grads build distinctive single-family home in Poletown
  | publisher = Model Media, OCTOBER 23, 2007
  | url = http://www.modeldmedia.com/devnews/cranbrook11607.aspx}}  at 2126 Pierce Street in Poletown East, Detroit|Poletown, Detroit.  The house, ultimately abandoned, {{cite news
  | title = If You Build It, About the Subjects
  | publisher = If You Build It, Official Site
  | url = http://www.ifyoubuilditmovie.com/about/about-the-subjects.html}}  had a construction budget of $60,000 and used all-volunteer labor along with carefully selected materials (e.g., concrete block and rubber membrane roofing) to keep costs down. 

==Reception==
As of mid-2014, the film had received a rating on Rotten Tomatoes of 81% favorable critic response and 100% favorable audience response. {{cite news
  | title = If You Build It
  | publisher = Rottentomatoes.com
  | url = http://www.rottentomatoes.com/m/if_you_build_it/}} 

Architectural Record called the film "A document of America at its most vulnerable."  The New York Times reviewer Nicolas Rapold said "the movie’s biggest weakness comes with its tendency to film people telling us what’s going on rather than having us observe." {{cite news
  | title = Today, a Chicken Coop; Tomorrow, the World
  | publisher = The New York Times, Nicolas Rapold, Jan 9, 2014
  | url = http://www.nytimes.com/2014/01/10/movies/if-you-build-it-by-patrick-creadon-on-student-projects.html?smid=tw-nytmovies&seid=auto&_r=0}} 

==Follow up==
Pilloton and Miller, who had been a couple, split up  some time after the period depicted in the documentary.  Matthew Miller is now the lead design/build instructor at (co)studio, a public high school program in Carbondale, Colorado.  Pilloton continues to direct Studio H, which is now in its fourth year, at the at REALM Charter School in Berkeley, California. 

Studio H has grown to include more than 200 students, {{cite news
  | title = Shop Class, Redesigned
  | publisher = Slate, JAN. 7 2014, Kristin Hohenadel
  | url =http://www.slate.com/blogs/the_eye/2014/01/07/emily_pilloton_matthew_miller_project_h_design_if_you_build_it_directed.html}}  a staff of seven (paid) and 24 built projects around the country (as of mid 2014).   In Bertie County as of 2012, the Studio H program had completed 15 architectural and design projects, worked with 26 students and (in addition to the farmers market pavilion) constructed two roadside farmstands for important county intersections, and organized a town-managed association for local farmers. Students have worked on these projects for high school and college credits as well as stipends. {{cite news
  | title = FROM BERTIE TO BERKELEY: THE NEXT GENERATION OF STUDIO H
  | publisher = Studio H, une 4th, 2012
  | url = http://www.studio-h.org/from-bertie-to-berkeley-the-next-generation-of-studio-h}} 

At a point early in the film, student Stevie Mizelle &mdash; whose preliminary design for the farmers market was ultimately selected by the team for design development &mdash; joked: "school, I hate it, my dad hated it, my granddaddy hated it. Im carrying on a tradition."  He currently studies animal behavior at North Carolina State University in Raleigh, NC, making Dean’s List his freshman year.   He was the first male relative in his immediate family to graduate from high school, attributing his graduation to his work with Studio H. 
 
Chip Zullinger died suddenly in April 2014. {{cite news
  | title = Zullinger succumbs
  | publisher = Roanoke-Chowan News Herald, April 3, 2014, Gene Motley
  | url = http://www.roanoke-chowannewsherald.com/2014/04/03/zullinger-succumbs/}}  Zullinger, widely described as ahead of his time, {{cite news
  | title = Chip Zullinger, who changed local schools, is still remembered
  | publisher = The Post and Courier, Apr 11 2014, Brian Hicks
  | url = http://www.postandcourier.com/article/20140411/PC16/140419916}}  {{cite news
  | title = HISD mourning death of Elementary School Chief ‘Chip’ Zullinger
  | publisher = Houston Independent School District
  | url = http://blogs.houstonisd.org/news/2014/04/02/hisd-mourning-death-of-elementary-school-chief-chip-zullinger/}}  told Patrick Creadon, director of "If you Build it," he had been fired at least eight times and wore that as a badge of honor. {{cite news
  | title = Cutting Room: We talk with Patrick Creadon, director of "If You Build It"
  | publisher = Archinect.com, Amelia Taylor-Hochberg, May 14 14
  | url = http://archinect.com/features/article/99663613/cutting-room-we-talk-with-patrick-creadon-director-of-if-you-build-it-about-the-documentary-and-the-power-of-design-education-in-post-recession-u-s}}  Creadon described Zullinger as a "renegade school superintendent, a great thinker and a steamroller when it came to getting kids the education they deserve.” {{cite news
  | title = Dr. Sidney "Chip" Zullinger, III
  | publisher = Our Local Community Online
  | url = http://www.ourlocalcommunityonline.com/Dr-Sidney-Chip-Zullinger-III-Details-/18764166}} 

==References==
 

==External links==
*  
*  
*   at YouTube
*  

 
 
 
 
 
 
 
 
 
 
 