The Rogues Tavern
{{Infobox film
| name           = The Rogues Tavern
| image_size     =
| image	=	The Rogues Tavern FilmPoster.jpeg
| caption        =
| director       = Robert F. Hill
| producer       = Sam Katzman Al Martin (original screenplay)
| narrator       =
| starring       = Wallace Ford Barbara Pepper Joan Woodbury
| music          =
| cinematography = William Hyer
| editing        = Dan Milner
| distributor    =
| released       = 1936
| runtime        = 70 minutes (Australia, uncut version) 70 minutes (U.S.)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Rogues Tavern is a 1936 American horror film directed by Robert F. Hill.

== Plot summary ==
It is a bleak and windy night when Jimmy Kelly (Wallace Ford) and Marjorie Burns (Barbara Pepper) check into the Red Rock Tavern with plans to marry as soon as possible. Everyone in the tavern is shocked when a wild dog breaks in through an open window, attacks and kills two of the guests.  When they discover that the dog is not the real killer all of the guests begin to panic and suddenly find themselves trapped inside the tavern by locked doors and barred windows. Everyone frantically tries to determine who among them is the killer before another one of them is murdered.

== Cast ==
*Wallace Ford as Jimmy Kelly
*Barbara Pepper as Marjorie Burns
*Joan Woodbury as Gloria Robloff
*Clara Kimball Young as Mrs. Jamison
*Jack Mulhall as Bill John Elliott as Mr. Jamison
*Earl Dwire as Morgan John Cowell as Hughes
*Vincent Dennis as Bert
*Arthur Loft as Wentworth
*Ivo Henderson as Harrison Ed Cassidy as Mason Silver Wolf as Silver Wolf

== Soundtrack ==
 

== External links ==
 
* 
* 

 
 
 
 
 
 


 