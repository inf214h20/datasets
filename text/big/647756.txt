Dragon: The Bruce Lee Story
{{Infobox film
| name           = Dragon: The Bruce Lee Story
| image          = Storyofdragon.JPG
| image_size     = 215px
| caption        = Theatrical release poster
| director       = Rob Cohen
| producer       = Raffaella De Laurentiis Rick Nathanson John Badham
| screenplay     = Rob Cohen John Raffo Edward Khmara
| based on       =    
| starring       = Jason Scott Lee Lauren Holly Robert Wagner
| music          = Randy Edelman
| cinematography = David Eggby
| editing        = Peter Amundson Universal Pictures
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $14 million   
| gross          = $63.5 million   
}} biographical drama film written and directed by Rob Cohen, and starring Jason Scott Lee (no relation), Lauren Holly and Robert Wagner. The film was released in the United States on May 7, 1993.
 The Crow. 

==Plot== Bruce Lees San Francisco when his father was on a tour there and has a US birth certificate.  His father asks Bruce to become a success, so big a success that his name will be famous even back in Hong Kong.
 Linda Emery (Lauren Holly), and the two begin dating.  They eventually marry in defiance of Lindas racist mother.  Linda suggests that Bruce open up a martial arts training school, which he does, but some other Chinese martial arts schools feel that he should not train non-Chinese.   They challenge him to fight Johnny Sun (John Cheung) to resolve the issue. Bruce wins the fight but Sun attacks Bruce from behind after the fight is over, resulting in a serious back injury.

While immobilized and recovering Bruce and Linda quarrel of why he did not tell her about this duel, but she furiously rejects his despairing assumption that she will abandon him because of this injury.  To give his recovery time purpose, Linda coaxes him to examine the weaknesses of his combat technique, which leads to him developing the fight philosophy of Jeet Kune Do while she helps him write "The Tao of Jeet Kune Do." During this period Linda gives birth to their first child, Brandon Lee|Brandon, and he is the key for the couple to reconcile with Lindas mother (Michael Learned).  Later at Ed Parkers martial arts tournament, Bruce has been challenged by Johnny Sun, this time in a 60-second demonstration of his new fighting style. Johnny Sun appears to have the upper hand in the first half of the match but then Bruce recovers and ends up kicking Sun over the top rope.
 The Green Kung Fu television series.  At a cast party, Linda says she is now pregnant with their second child, Shannon Lee|Shannon. Shortly afterwards, there is an announcement for the cancellation of The Green Hornet. Kung Fu makes it onto television, but starring David Carradine and not Bruce.

Bruce travels back home for his fathers funeral and whilst in the country is approached by Philip Tan (Kay Tong Lim), a Hong Kong film producer.  He says that Bruce is well known in Hong Kong and that The Green Hornet show is called The Kato Show there.  Bruce begins work on the feature film The Big Boss. In the filming of the final scene, set in an ice factory, the brother of Johnny Sun attacks Bruce, wanting revenge.  Bruce successfully defends himself against the attack. The Big Boss is a success and Bruce makes several more films – working as actor, director and editor.  This causes a rift between Bruce and Linda, as she wishes to return to the States.  Bill Krieger shows up, and although he knows that Bruce is still angry with him, he offers him a chance to work on a big-budget Hollywood movie, which Bruce decides to do, particularly as Linda wishes to return to the States.

On the 32nd day of shooting Enter the Dragon, Bruce has a vision of himself fighting and defeating the phantom that haunted his childhood.  At the end, he decides to walk off of the set and go home to see his family.  The film ends during a shot of the final scene of Enter the Dragon, with a voice-over by Linda informing the audience of his death before the movies release, and her preferred choice to discuss his life, not his death.

==Cast==
* Jason Scott Lee as Bruce Lee 
** Sam Hau as Young Bruce Lee Linda Lee 
* Robert Wagner as Bill Krieger
* Michael Learned as Vivian Emery
* Nancy Kwan as Gussie Yang
* Alicia Tao as April Chun
* Ed Parker Jr. as Ed Parker
* Lim Kay Tong as Philip Tan
* Ric Young as Lee Hoi-chuen|Bruces Father
* Wang Luoyong as Yip Man Jerome Sprout
* John Cheung as Johnny Sun
* Sven-Ole Thorsen as The Demon
* Eric Bruskotter as Joe Henderson
* Aki Aleong as Principal Elder The Green Hornet   
* Lala Sloatman as Sherry Schnell
* Rob Cohen as Robert Clouse, Enter the Dragon director
* Iain M. Parker as Brandon Lee
* Michelle Tennant as Shannon Lee

==Production==
The film is based upon the biography  , written by Linda Lee Cadwell, Bruce Lees widow. 
 The Crow in March 1993, less than two months before this films release.  The film is dedicated to his memory at the end credits. In the film Brandon was portrayed as a child by Iain M. Parker.

To prepare for their roles both lead actors Jason Scott Lee and Lauren Holly trained in Bruce Lees Martial Art Jeet Kune Do for months under former Lee student-turned-instructor Jerry Poteet.   Jason Scott Lee has continued to train in Jeet Kune Do under Poteet since 1993.
 1960s TV series of the same name appears as the director of The Green Hornet in this film.  Bruce Lees daughter, Shannon Lee, has a cameo appearance as a singer in the party scene (singing "California Dreamin"), at which Linda tells Bruce she is pregnant for the second time – carrying Shannon.

The tombstone that Bruce is forced to see when confronting his demon towards the end of film is the actual tombstone of Bruce Lee.  The picture on it is different from the one that is actually on the real one but the date of birth, date of death and the epitaph are the same.

===Deleted scenes (UK)===
Scenes from Dragon: The Bruce Lee Story were deleted by British authorities, ostensibly to meet age-rating prior to its release in the United Kingdom, in particular the scenes in the Lantern Festival dance party in Hong Kong where a British sailor who abuses a woman, is confronted by Bruce Lee,    and the fight between Bruce Lee and his inner demon near the end of the film was shortened in a way that viewers can not see Bruce Lee fighting with the nunchaku weapon.

==Reception==
The film received generally positive reviews,    with a rating of 83% on RottenTomatoes, based on 18 reviews counted. 

===Box office===
The film debuted at #1 at the box office.   The film had a domestic gross of $35,113,743, with an additional $28,400,000 earned in foreign territories. The film grossed $63,513,743 worldwide. 

==Soundtrack==
{{Infobox album   
| Name        = Dragon: The Bruce Lee Story
| Type        = Soundtrack
| Artist      = Randy Edelman 
| Cover       = 
| Released    = 
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = 
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings rev1 = Allmusic rev1score =   
}}
The soundtrack for Dragon: The Bruce Lee Story was composed by Randy Edelman. Randy Edelmans soundtrack is best known for its use in film trailers, particularly the love theme Bruce and Linda and The Premiere of the Big Boss.  The uncredited song playing during the kitchen fight scene at the beginning, is "Green Onions" by Booker T. and the MGs. 

==Legacy==
A video game   was released in the mid 1990s to various consoles.
 The Fast and the Furious, Dragon was on Dominic Torettos television during a scene in his house. Both films are directed by Rob Cohen.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 