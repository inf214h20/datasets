Deceit (1923 film)
{{Infobox film
| name           = Deceit
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Oscar Micheaux
| producer       = 
| writer         = 
| narrator       = 
| starring       = Evelyn Preer William Fountaine Norman Johnstone A.B. DeComathiere Cleo Desmond
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Micheaux Film Corporation
| released       =  
| runtime        = 
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =  
}} silent black-and-white conventional melodrama directed by Oscar Micheaux.  Like many of Micheauxs films, Deceit casts clerics in a negative light.  Although the film was shot in 1921, it was not released until 1923.  Its survival status is classified as unknown,  which suggests that it is a lost film.

==Cast==
*Evelyn Preer - Doris Rutledge / Evelyn Bently
*William Fountaine - unknown role
*Norman Johnstone - Alfred DuBois / Gregory Wainwright
*A.B. DeComathiere - Reverend Bently
*Cleo Desmond - Charlotte Chesbro
*Louis De Bulger - Mr. Chesbro
*Mabel Young - Mrs. Levine
*Cornelius Watkins - Gregory Wainwright, as a child
*Mrs. Irvin C. Miller - Mrs. Wainwright
*Ira O. McGowan - Mr. Wainwright

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 