The Battle of Shaker Heights
{{Infobox film
| name           = The Battle of Shaker Heights
| image          = Poster of the movie The Battle of Shaker Heights.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Efram Potelle Kyle Rankin
| producer       = Ben Affleck Matt Damon
| writer         = Erica Beeney Shia La William Sadler Richard Marvin
| cinematography = Thomas E. Ackerman
| editing        = Richard Nord
| studio         = LivePlanet
| distributor    = Miramax Films
| released       =  
| runtime        = 77 minutes  
| country        = United States
| language       = English German
| budget         = $1 million
| gross          = $280,351 
}} Shia La Beouf, Elden Henson, Kathleen Quinlan, Amy Smart, and Shiri Appleby. The film was the winning script for the second season of Project Greenlight.

==Plot== war reenactment drug addicts, being clean himself for over 5 years. His mother, Eve, is a commercial artist. 

As Kelly is leaving, Bart sees him and is furious as he knew he was in the workshop. The next day, as Kelly is on a set for a war program that Bart got them in, Bart hasnt turned up. Kelly is cast in a role to be a jeep driver and when his cue is called, Bart turns up out of the bushes and confronts him about Tabby. Kelly denies it at first and then becomes speechless. As he is about to drive for his cue, Bart attacks him in the jeep and they make a mess of the set.

The next day, Abe is taken into care again for relapsing and Kelly is emotionless. He goes to the wedding the same day, only to have an annoyed Bart tell him that he cant let him in. He sneaks into Tabbys limo before it pulls in and he talks to her, and it is a moment in which he finally realizes he must grow up. He gets out of the car and everyone at the wedding sees him and are all confused. He goes round the back of the church to get his bike and leave and meets Minor again, who has no idea of what happened with Kelly and Tabby, and is civil towards him. Kelly cycles to the clinic where his father is staying and is surprised. They begin to watch television and his mother walks in and is surprised and happy to see Kelly.

In the end, it shows the voicemail message where Kelly apologizes to the Bowlands for his behavior and asks Bart if he will meet him where he will be selling all his old memorabilia, and gives a hat Bart acquired for him back. He is shown walking down the street with Sarah and they hold hands. Kelly sees Lance again on his lawn where they faked the invasion, and goes over to him and confesses, only to be punched in the jaw.

==Cast== Shia La Beouf as Kelly Ernswiler
* Elden Henson as Bart Bowland
* Amy Smart as Tabitha "Tabby" Bowland 
* Shiri Appleby as Sarah
* Kathleen Quinlan as Eve Ernswiler William Sadler as Abraham "Abe" Ernswiler
* Ray Wise as Harrison Bowland Billy Kay as Lance Norway Michael McShane as Mr. Norway
* Anson Mount as Miner Weber
* Hattie Wilson as Principal Holmstead

==References==
 

;Sources
* "Shaker lends only its name; weather gets blame", The Plain Dealer, August 24, 2003.

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 