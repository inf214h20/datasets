Paranoia 1.0
{{Infobox film name           = Paranoia 1.0 (One Point O) image          = Paranoia 1point0 poster.jpg image_size     = caption        = theatrical poster director       = Jeff Renfroe Marteinn Thorsson producer       = Chris Sievernich R.D. Robb Kyle Gates Thomas Mai writer         = Jeff Renfroe Marteinn Thorsson narrator       = starring       = Jeremy Sisto Deborah Kara Unger Bruce Payne music          = Terry Huud cinematography = Christopher Soos editing        = Daniel Sadler Troy Takaki studio         = distributor    = Armada Pictures International released       =   runtime        = 92 minutes country  Iceland
|language       = English budget         = gross          =
}}
Paranoia: 1.0 (originally One Point O, also known as 1.0, One Point Zero, Version 1.0, and Virus 1.0) is a 2004 cyberpunk science fiction film written and directed by Jeff Renfroe and Marteinn Thorsson. The film is a Kafkaesque nightmare in which a young computer programmer is an unwitting guinea pig in a corporate experiment to test a new advertising scheme.    The film stars Jeremy Sisto and Deborah Unger and features Lance Henriksen, Eugene Byrd, Bruce Payne and Udo Kier.

== Plot ==
When network engineer Simon J. (Jeremy Sisto) begins finding empty plain brown paper packages in his apartment, he goes to great length to try and secure his boundaries, but the packages keep appearing. Simon attempts to find out who is leaving the packages, and his investigation exposes him to his eccentric neighbors, an artificial intelligence robot head, created by Derrick (Udo Kier) who occupies an apartment across the hall from Simon, a virtual reality sex-game, created by the Neighbour (Bruce Payne) who occupies the apartment next to Simons, and the possibility of a corporate conspiracy. At the same time Simon, who works in IT, is under pressure to complete a programming project and develops a romantic relationship with another of the apartment blocks residents, Trish (Deborah Kara Unger).

With security cameras watching him constantly, Simon begins to lose his grip on reality, and develops an inexplicable craving for Nature Fresh brand milk.  Simon becomes increasingly puzzled as murders begin happening in the building. Simon eventually finds out that everyone else is receiving packages too but were keeping quiet. These people also got addicted to consuming other products like fresh juice, cola 500 and farm fresh meat. One of Simons friends, a courier named Nile (Eugene Byrd), tells Simon about a corporate experiment to ‘infuse’ Nanomites into peoples brains to make them addicted to certain products. He first dismisses it as a lie but eventually understands that it is indeed what is happening.

== Cast ==
* Jeremy Sisto as Simon
* Udo Kier as Derrick
* Deborah Kara Unger as Trish
* Bruce Payne as Neighbor
* Constantin Florescu as Tall Man
* Ana Maria Popa as Alice
* Matt Devlen as Cashier
* Lance Henriksen as Howard
* Eugene Byrd as Nile
* Emil Hostina as Landlord
* Constantin Cotimanis as Detective Polanski
* Sebastian Knapp as Detective Harris
* Udo Kier and Jeremy Sisto both voiced the robot head

== Production ==
Paranoia 1.0 was an international co-production, and was shot entirely in Bucharest, Romania. It premiered in competition at the 2004 Sundance Film Festival under its original title (One Point O).

The programming code seen in the film is from Viralator 0.9. 

== Awards and honors ==
; 2004
:; Sundance Film Festival (Park City, Utah, USA)
:: Grand Jury Prize, Dramatic Category; Nominated: Jeff Renfroe, Marteinn Thorsson.
:; Catalonian International Film Festival (Sitges, Spain)
:: Nominated Best Film; Recipients: Marteinn Thorsson, Jeff Renfroe.
:; Fant-Asia Film Festival (Montreal, Canada)
:: AQCC Award-Mention: Jeff Renfroe, Marteinn Thorsson; Best International Film (Jury Prize): Jeff Renfroe, Marteinn Thorsson; Most Ground-Breaking Film (Jury Prize): Jeff Renfroe, Marteinn Thorsson.
; 2005
:; Edda Awards (Iceland)
:: Edda Award Category: Best Picture (nominated); Best Production Design: Eggert Ketilsson (nominated); Best Sound or Music: Bradley L. North, Byron Wilson, Ann Scibelli (nominated); Director of the Year: Marteinn Thorsson, Jeff Renfroe (nominated).
:; Málaga International Week of Fantastic Cinema (Spain)
:: Best Actress: Deborah Kara Unger; Best Film: Jeff Renfroe, Marteinn Thorsson; Youth Jury Award (Best Feature Film): Jeff Renfroe, Marteinn Thorsson.
; 2007
: In 2007, the film was listed as one of the “Top 50 Dystopian Movies of All Time” by Snarkerati, a popular movie web-magazine.

== External links ==
*  
*  
*  
*   at Cyberpunk Review
* One Point O on  

== Notes ==
 

 
 
 
 
 
 
 
 