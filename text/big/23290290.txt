Zoeken naar Eileen (film)
{{Infobox film
| name           = Zoeken naar Eileen
| image          = Zoeken naar Eileen.jpg
| image_size     = 
| caption        = 
| director       = Rudolf van den Berg
| producer       = 
| writer         = Leon de Winter
| narrator       =
| starring       = Thom Hoffman Lysette Anthony Garry Whelan Kenneth Hardgein John van Dreelen Hans Kemna
| music          =   
| cinematography = Theo van de Sande
| editing        = 
| distributor    = 
| released       = 24 September 1987
| runtime        = 98 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1987 Netherlands|Dutch Zoeken naar Eileen W., written by Leon de Winter. 

The voice of the British actress Lysette Anthony was dubbed by Marijke Vleugelers. 

==Plot== 

A young man has just lost his young girlfriend, and becomes depressed. But then he meets a woman which resembles his late girlfriend a lot. Their meeting is brief, but the main character knows enough to know that he only wants her from that moment on. The only thing he knows of her, is that she speaks English, that she is from Northern Ireland and that she is called Eileen.

==Cast== 

* Thom Hoffman - Philip de Wit 
* Lysette Anthony - Marian Faber / Eileen 
* Garry Whelan - Mark Nolan 
* Kenneth Hardgein - Geoffrey 
* John van Dreelen - Philips father 
* Hans Kemna - Henk Faber

== External links ==
*   IMDB

 
 
 

 