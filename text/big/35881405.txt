Apache Rifles
{{Infobox film
| name           = Apache Rifles
| image          = Apacherifpos.jpg
| caption        = Film poster
| director       = William Witney
| producer       = Grant Whytock Robert E. Kent
| writer         = Charles B. Smith Kenneth Gamet (story) Richard Schayer (story)  
| narrator       = Linda Lawson
| music          = Richard LaSalle
| cinematography = Archie R. Dalzell
| editing        =
| distributor    = Twentieth Century Fox
| released       = November 1964
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}} Western film Red Rock Canyon State Park, California.

==Plot==
Captain Stanton is a renown Indian Fighter with his animosity stirred by the fact that his career officer father was forced to resign after trusting a group of Indians who broke their word to him.  Stanton is sent to the field to relieve the commander of a troop unsuccessfully hunting Apaches who have fled their reservation.  Stanton quickly meets with success by having his new troop drop their excess equipment to move faster and longer.  He defeats a war party of Apache by luring them with an unescorted wagon loaded with hidden soldiers who engage the enemy until Stantons mounted troop surround and defeat them.  Stanton captures Red Hawk, the son of Chief Victorio and uses him to negoitate a truce with the Apaches promising to stay on their reservation in return for white miners not trespassing on their land to mine gold.

Though the territory remains peaceful, the economy of the settlers and miners faces a serious economic threat when the miners are unable to pay the business interests that advance them money for their mines in return for a percentage of the profits.

Captain Stantons sympathies gradually change when the miners continue their trespassing.  He also is attracted to Dawn Gillis, a courageous missionary teacher who is half Indian herself that teaches in an Indian school.  Stanton ruins his military career by beating up a local troublemaker who makes racist taunts to Dawn, then later engages a group of whites who massacre a group of Indians at the school that leads Stanton to open fire on the whites killing several of them.

A delegation complains about Stantons behaviour and the economic disaster to the Federal Government in Washington D.C. who send out a new commander named Colonel Perry.  The Colonel not only relieves Stanton but dismantles his line of outposts to monitor white entry into the reservation that leads the miners to swarm in and hostilites to resume.

Victorios Apaches lure the inexperienced Colonel and his men into an ambush but a messenger escapes to the remnants of the Colonels command that Stanton takes charge of to straighten out the mess. 

==Cast==
* Audie Murphy as Captain Jeff Stanton
* Michael Dante as Red Hawk
* L. Q. Jones as Mike Greer Linda Lawson as Dawn Gillis   
* Ken Lynch as Hodges
* Joseph A. Vitale as Victorio 
*Robert Brubaker as Sergeant Cobb
* Eugene Iglesias as Corporal Ramirez
*J. Pat OMalley as Captain Thatcher John Archer as Colonel Perry
* Charles Watts as Crawford Owens
* Howard Wright as Thompson

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 
 
 