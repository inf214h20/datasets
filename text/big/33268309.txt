Back to Your Arms
 
{{Infobox film
| name           = Back to Your Arms
| image          = BackToYourArms2010Poster.jpg
| caption        = Lithuanian poster
| director       = Kristijonas Vildžiūnas
| producer       = Uljana Kim
| writer         = Kristijonas Vildžiūnas
| starring       = Giedrius Arbaciauskas
| music          = 
| cinematography = Vladas Naudzius
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Lithuania
| language       = Lithuanian
| budget         = 
}} Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Cast==
* Giedrius Arbaciauskas
* Andrius Bialobzeskis as Vladas
* Margarita Broich as Bettina
* Franz Broich-Wuttke
* Jurga Jutaite as Aukse
* Aleksas Kazanavicius
* Elzbieta Latenaite as Ruta
* Sandra Maren Schneider as Renate
* Sabin Tambrea

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Lithuanian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 

 


 