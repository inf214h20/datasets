Pelicanman
{{Infobox film
| name          = Pelikaanimies
| image         = Pelikaanimies poster.jpg
| caption       = Film poster for Pelikaanimies.
| year          = 2004
| director      = Liisa Helminen
| writer        = William Alridge,  Liisa Helminen
| starring      = Kari Ketonen, Roni Haarakangas, Inka Nuorgam
| cinematography= Timo Salminen
| producer      = Hanna Hemilä
| distributor   = Buena Vista International (Finland)
| released      = 2004
| runtime       = 1h 30min
| language      = Finnish
}}
Pelicanman (  Finnish fantasy film.

==Plot==
A pelican magically changes his appearance into that of a young man. He walks and acts somewhat oddly compared to real humans, and at first he does not know much about humans, but he learns fast. He rents an apartment and gets a job. The 10-year old boy Emil finds out that he is a pelican, and they become friends.

The pelican man is sent to a zoo, but Emil helps him escape. Then the pelican man changes back to pelican appearance.

==Trivia== Helsinki Market Square, the Norrmén house by Theodor Höijer is seen in the place of the Enso-Gutzeit main office by Alvar Aalto that is actually situated there. This is because the director is among those Finns who dislike Aaltos building, and would have preferred Höijers building to remain in its place.

==External links==
* 

 
 
 
 
 


 
 