After.Life
 
{{Infobox film
| name           = After.Life
| image          = After.Life.jpg
| caption        = Theatrical release poster Agnieszka Wójtowicz-Vosloo
| producer       = Bill Perkins Brad Michael Gilbert Celine Rattray
| writer         = Agnieszka Wojtowicz-Vosloo Paul Vosloo Jakub Korolczuk
| starring       = Christina Ricci Liam Neeson Justin Long
| music          = Paul Haslinger
| cinematography = Anastas N. Michos
| editing        = Niven Howie
| studio         = Lleju Productions Harbor Light Entertainment Plum Pictures
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2,002,618 
}}
 Agnieszka Wójtowicz-Vosloo from her original screenplay.   

==Plot== fictional drug called hydronium bromide to "relax the muscles and keep rigor mortis from setting in."

Anna unsuccessfully attempts to escape several times; Eliot tells her she must let go of life as she had not really been living anyway.  Eventually, Anna escapes and finds a room with a phone where she reaches Paul, who hangs up thinking its a prank.  Anna comes to believe she has actually died when Eliot allows her to see her corpse-like self in a mirror.  One of Annas students sees her and alerts Paul, who becomes suspicious that she is still alive. Paul requests to see Annas body but Elliot does not allow it.

During the final preparation for the funeral, Anna asks to see herself one last time.  Eliot holds up a small mirror, and while she stares at herself she notices her breath condensing on the glass and begins to believe she has been alive all along.  Eliot injects her one last time to make her numb.  At the funeral, as Paul views Annas body, she twitches her eyes but is unable to get his attention.  Paul places the engagement ring he intended to give her the night of the crash on her finger and gets surprised as Annas body was cold and then kisses her.    

After the funeral, Paul drinks heavily telling Eliot he knew Anna was not dead.  Anna is shown awakening to the sound of earth being shoveled onto her coffin.  She cries out and desperately scratches the satin lining of her coffin lid.  As she slowly dies of suffocation, Eliot suggests Paul should find out whether Anna is actually dead or not before it is too late.  Driving under the influence of alcohol, Paul rushes to the cemetery. The two embrace and Anna tells Paul she has always loved him.  As they hug, Paul is curious about odd sounds he hears; Anna explains it is the sound of Eliots gloves and scissors on the table as he prepares Pauls body.  Paul sees Anna  disappearing, then a bright flash of headlights.  A moment later, he finds himself in the morgue with Eliot standing over him preparing his body.  Paul says he saw Anna, but Eliot tells him that he never made it to the cemetery due to a car accident which killed him.  Paul pleads that he is alive until the moment Eliot inserts a trocar deep into his torso.

==Cast==
* Christina Ricci as Anna Taylor
* Liam Neeson as Eliot Deacon
* Justin Long as Paul Coleman
* Celia Weston as Beatrice Taylor
* Chandler Canterbury as Jack
* Rosemary Murphy as Mrs Whitehall

==Production==
After.Life completed filming in New York at the end of December 2008 with Bill Perkins and Celine Rattray as producers. Galt Niederhoffer and Pam Hirsch are executive producing for Plum Pictures with Edwin Marshall and James Swisher executive producing for Harbor Light.  Scenes were filmed in Lynbrook, New York|Lynbrook, New York in early December 2008. 

==Release== premiered at AFI Film Festival in Los Angeles on November 7, 2009.  Anchor Bay Entertainment, a division of Overture Films, has acquired theatrical rights for the U.S. and the U.K.  The film received an R-rating for the multiple nude scenes with Christina Ricci and was released on 9 April 2010 in a limited release.  Anchor Bay released the DVD and Blu-ray on 3 August 2010.   

==Critical reception==

After.Life has received negative reviews. Review aggregate Rotten Tomatoes reports that 29% of 49 critics have given the film a positive review, holding an average score of 4.6/10.    According to the website, the films critical consensus is, "It has an interesting premise and admirable ambitions, but After.Life fails to deliver enough twists or thrills to sustain its creepy atmosphere." Review aggregate Metacritic has given the film a weighted score of 36/100, based on 21 reviews, indicating "Generally unfavorable reviews".     DreadCentral rated the film 2.5/5 stars and called it "a competent but disappointing thriller."   Bloody Disgusting rated it 3/5 stars and described it as "a plodding reflection on mortality disguised as a psychological thriller".   In a negative review for The New York Times, Manohla Dargis wrote that "the few good ideas are inevitably thwarted by the filmmaking".   James Berardinelli rated the film 3/4.  Berardinelli wrote that the film "has flaws aplenty" but it is arresting and unsettling. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 