When a Killer Calls
{{multiple issues|
 
 
}}

{{Infobox film
| name           = When a Killer Calls
| image          = When a Killer Calls.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| director       = Peter Mervis
| producer       = Sherri Strain   David Rimawi   David Michael Latt
| writer         = Peter Mervis   Steve Bevilacqua
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Sarah Hall   Mark Irvingsen   Robert Buckley   Derek Osedach   Rebekah Kochan
| music          = Mel Lewis
| cinematography = Mark Atkins
| editing        = Peter Mervis
| studio         = The Asylum
| distributor    = The Asylum
| released       = February, 2006
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 2006 remake When a Stranger Calls, as both movies have near-identical plots. 

== Plot ==
A killer goes into a house, killing a mother and her two children. In the process, he takes pictures on his cell phone of the attack. Elsewhere, Trisha (Rebekah Kochan) is babysitting a girl, Molly, while her parents are out.  She needs this to go well because her last babysitting job with the Hewitts did not end on a good note. On the way to a speaking engagement, Mollys parents are killed by an off-screen attacker that they obviously knew. Meanwhile, Trisha begins getting increasingly threatening phone calls and texts. After, she puts Molly to bed, the caller escalates his taunting.  He sends her pictures of the previous murders claiming he did it for her.  Although she believes it is her boyfriend Matt calling her, she calls the police who inform her that they will trace the call. 

Matt (Robert Buckley), along with his two reckless friends Frank and Chrissy arrive saying they have been chased by cops after Frank brandished a gun at a bowling alley during an argument.  They ask to stay until it is safe to leave.  Matt assures Trisha that he did not make that call. Going into the basement to make out, Chrissy hears a strange noise. Frank is killed when he investigates and Chrissy is beaten unconscious when she goes to find him. Worried that Frank is getting into trouble again, Trisha has Matt find out what he is doing. When he goes to the basement, he is knocked unconscious and tied up by the killer. 

Watching the news of a triple murder earlier that night, Trisha realizes the pictures sent to her are real and that they are also of the Hewitts. The killer calls Trisha and informs her that she needs to check on Molly. After the police call and inform her that the calls came from inside the house, she discovers a camera in the living room. She checks on Molly and finds her dead with Richard Hewitt (the murdered familys husband and father) in the bedroom. She tries to run from him but is quickly tripped and knocked unconscious.

Awakening in the basement, Trisha discovers she is gagged and tied to a rafter, Matt tied up on the floor, Chrissy tied up and gagged on the couch, and Frank dead on the floor. Richard tortures the group and kills Chrissy before he goes upstairs to kill some investigating police officers. Richard also explains the reason for the murders, revealing he had become obsessed with Trisha after raping her one night. Using this time to free himself, Matt stuns Richard and frees Trisha. However, Matt is killed doing this. Trisha manages run out of the house and retrieves Franks gun which he stashed in Matts truck. She shoots Richard several times, ensuring he is dead, and then walks away from the house into the surrounding woods.

== Cast ==
* Rebekah Kochan - Trisha Glass
* Robert Buckley - Matt
* Mark Irvingsen - Richard Hewitt   
* Sarah Hall - Chrissy
* Derek Osedach - Frank
* Carissa Bodner - Molly Walker
* Chriss Anglin - Mr. Walker
* Tara Clark - Mrs. Walker
* Louis Graham - Charlie
* Isabella Bodnar - Linda Hewitt
* Cheyenne Watts - Holly Hewitt
* Christian Hutcherson - Ryan Hewitt
* Justin Jones - Trent Rockport
* Kassidy Oridian - Shark

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 