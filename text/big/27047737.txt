Suno Sasurjee
{{Infobox film
| name           = Suno Sasurjee
| image          = Suno_Sasurjee.jpg
| director       = Vimal Kumar
| producer       =
| writer         = Rajeev Kaul
| narrator       = Aftab Ameesha Patel
| music          = Sanjeev Darshan
| cinematography = Pravin Bhatt
| editing        = Amit Saxena
| distributor    =
| released       =  
| runtime        = 169 mins
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 romantic Comedy film directed by Vimal Kumar. The film stars Aftab Shivdasani and Ameesha Patel in lead roles. Kader Khan, Gulshan Grover and Asrani made supporting roles in the film.

==Synopsis==
Mr. Raj K. Saxena (Kader Khan) is known for his parsimony. Everything he does and thinks is valued with money, and ways he can accumulate it. His daughter, Kiran (Ameesha Patel), is the opposite, a spendthrift. Mr. Saxena borrows money from elderly people, assuring them of returning the loan with a handsome rate of interest after about 20 years, knowing fully well that none of them will survive 20 years. Then he meets with the son of one of such lender, aptly named after him viz. Raj K. Saxena (Aftab Shivdasani|Aftab). Mr. Saxena refuses to repay the amount, swallows the proof, the only evidence, and asks Raj to get out, which he does so. Raj is determined to get his money, and wants Kiran to fall in love with him. Kiran does so, and brings him over to introduce him to her shocked and speechless dad. Things change when Raj inherits a large amount of money, ironically left by Mr. Saxena maternal grandmother, and it is Mr. Saxena who is now anxious to get in the good books of Raj, with hilarious results.

==Cast==
*Aftab Shivdasani  as  Raj Saxena
*Ameesha Patel  as  Kiran Saxena
*Gulshan Grover  as  Shera Aflatoon
*Kader Khan  as  Raj K. Saxena
*Asrani  as  Murli
*Shakti Kapoor  as  Kirans Brother-in-Law
*Kiradaas  as  Dannys Cousin-in-Law

==Soundtrack==
{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_lyrics      = 
| all_music       = Sanjeev Darshan
| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1 = Aa Jaa
| extra1 = Jaspinder Narula

| title2 = Aap Kaha Rehte Hain
| extra2 =Abhijeet Bhattacharya|Abhijeet, Alka Yagnik

| title3 = Jab Dil Dhadakta
| extra3 = Kumar Sanu, Alka Yagnik

| title4 = Kardo Kardo Shaadi Sasurjee
| extra4 = Sonu Nigam, Vinod Rathod, Sapna Mukherjee, Arshad Warsi

| title5 = Mera Dil Chura Ke
| extra5 = Kumar Sanu, Prabha

| title6 = Saason Se Saasein
| extra6 = Babul Supriyo, Alka Yagnik, Sunidhi Chauhan

| title7 = Tota Mirchi
| extra7 = Sonu Nigam, Alka Yagnik
}}

==External links==
*  

 
 
 