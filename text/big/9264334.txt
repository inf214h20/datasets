4 Devils
{{Infobox film
| name           = 4 Devils
| image          = 4devils.jpg
| caption        = Janet Gaynor and Charles Morton
| director       = F. W. Murnau William Fox
| writer         = Carl Mayer
| based on       =    Charles Morton Mary Duncan Barry Norton
| music          = Ernö Rapée Lew Pollack
| cinematography =
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} silent drama film directed by German film director F. W. Murnau and starring Janet Gaynor.

==Plot== Charles Morton) who become a high wire act, and centers around sinister goings-on at a circus.

==Cast==
*Janet Gaynor - Marion
*Anders Randolf - Cecchi
*Barry Norton - Adolf Charles Morton - Charles
*Andre Cheron - Old roue George Davis - Mean clown
*Philippe De Lacy - Adolf as a child
*Nancy Drexel - Louise
*Mary Duncan - The lady
*Wesley Lake - Old clown
*Anita Louise - Louise as a child
*J. Farrell MacDonald - The clown
*Claire McDowell - Woman
*Jack Parker - Charles as a child
*Anne Shirley - Marion as a girl (billed as Dawn ODay)

==Production== William Fox, who had hired Murnau to come to the United States.  A sound version, incorporating "synchronised sound effects, music and dialogue sequences", was made without Murnaus cooperation.   

==Preservation status==
No copies of either version of the film are known to exist, and 4 Devils remains among the most sought after  ,  released by Fox as part of their 20th Century Fox Studio Classics collection. 

Film historian and collector William K. Everson stated that the only surviving print was lost by actress Mary Duncan who had borrowed it from Fox Studios.  Martin Koerber, curator of Deutsche Kinemathek, is more hopeful, writing that the print was given to Duncan, and that her heirs, if any, may yet have it. 

==References==
 

==External links==
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 