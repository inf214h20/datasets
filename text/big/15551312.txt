Great Guy
{{Infobox film
| name           = Great Guy
| image          = Great Guy FilmPoster.jpeg
| caption        = 
| director       = John G. Blystone
| producer       = Douglas MacLean Henry McCarty Henry Johnson Harry Ruskin (addl. dialogue)
| screenplay     = 
| story          = 
| based on       =  
| starring       = James Cagney Mae Clarke
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Grand National Pictures
| released       =   
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Great Guy (1936) is a crime film starring James Cagney and Mae Clarke. In the film, an honest inspector for the New York Department of Weights and Measures takes on corrupt merchants and politicians.

==Cast==
*James Cagney as Johnny Cave
*Mae Clarke as Janet Henry James Burke as Patrick James Aloysius Haley
*Edward Brophy as Pete Reilly
*Henry Kolker as Abel Canning
*Bernadene Hayes as Hazel Scott
*Edward McNamara as Captain Pat Hanlon
*Robert Gleckler as Marty Cavanaugh
*Joe Sawyer as Joe Burton

==External links==
* 
*  
*  
*  

 
 
 
 
 
 
 


 