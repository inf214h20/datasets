Patton (film)
{{Infobox film
| name           = Patton
| image          = 70_patton.jpg
| image_size     =
| border         = yes
| caption        = film poster
| director       = Franklin J. Schaffner Frank McCarthy
| screenplay     = Francis Ford Coppola Edmund H. North
| based on       =  
  Michael Bates Karl Michael Vogler
| music          = Jerry Goldsmith
| cinematography = Fred J. Koenekamp
| editing        = Hugh S. Fowler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 170 minutes
| country        = United States
| language       = English
| budget         = $12,625,000 
| gross          = $61,749,765(US) 
}} Dimension 150 by cinematographer Fred J. Koenekamp and has a music score by Jerry Goldsmith.
 Best Picture.
 opening monologue, delivered by George C. Scott as General Patton with an enormous American flag behind him, remains an iconic and often quoted image in film. The film was a success and has become an American classic. 

In 2003, Patton was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically or aesthetically significant".

==Plot== his speech Lieutenant Colonel Codman assures Patton that, though Rommel was absent, that if Patton defeated Rommels plan, then he defeated Rommel.

Patton is shown to believe in reincarnation, while remaining a devout Christian. At one point during the North Africa campaign, he takes his staff on an unexpected detour to the site of the ancient Battle of Zama. There he reminisces about the battle, insisting to his second in command, General Omar Bradley (Karl Malden) that he was there.
 British General Bernard Law slapping and threatening to shoot a Combat stress reaction|shell-shocked soldier, whom he accuses of cowardice, in an Army hospital.
 Richard Münch) invasion of Europe.
 Third Army and distinguishes himself by rapidly sweeping across France until his tanks are halted by lack of fuel. He later relieves the vital town of Bastogne during the Battle of the Bulge. He then smashes through the Siegfried Line and drives into Germany itself.
 British crowd Great Britain would dominate the post-war world, which is viewed as a slight to the Soviet Union|Russians. After the Germans capitulate, he insults a Russian officer at a celebration; fortunately, the Russian insults Patton right back, defusing the situation. Patton then makes an offhand remark comparing the Nazi Party to the political parties in the US. In the end, Pattons outspokenness loses him his command once again, though he is kept on to see to the rebuilding of Germany, with the disconcerting incident of a runaway ox-cart narrowly missing Patton alluding to the generals ignominious actual death in a car accident in December 1945.

The film ends with Patton walking his dog, a bull terrier named Willie, and Scott relating in a voice over that a returning hero of ancient Rome was honored with a triumph, a victory parade in which "a slave stood behind the conqueror, holding a golden crown, and whispering in his ear a warning: that all glory ... is fleeting."

==Cast==
*George C. Scott as Major General (later General) George S. Patton. (Rod Steiger had first turned down the role, later admitting that it was the worst decision of his career. ) Omar N. Bradley Michael Bates General (later Field Marshal) Sir Bernard Montgomery
*Edward Binns as Lieutenant General (later General) Walter Bedell Smith
*Lawrence Dobkin as Colonel Gaston Bell
*John Doucette as Major General Lucian Truscott James Edwards as Sergeant William George Meeks 
*Frank Latimore as Lieutenant Colonel Henry Davenport Richard Münch as Colonel General Alfred Jodl
*Morgan Paull as Captain Richard N. Jenson
*Siegfried Rauch as Captain (later Major) Oskar Steiger Paul Stevens as Lieutenant Colonel (later Colonel) Charles R. Codman Hobart Carver
*Karl Michael Vogler as Field Marshal Erwin Rommel Stephen Young as Captain Chester B. Hansen
*Peter Barkworth as Colonel John Welkin Sir Arthur Coningham David Bauer as Lieutenant General Harry Buford
*Tim Considine as Private First Class Charles Kuhl
*Albert Dumortier as Moroccan minister (voiced by Paul Frees) Sir Arthur Tedder Field Marshal) Sir Harold Alexander David Healy as Clergyman
*Bill Hickman as Pattons driver
*Sandy McPeak as War correspondent (credited as Sandy Kevin)
*Cary Loftin as Bradleys driver
*Alan MacNaughtan as British briefing officer
*Lionel Murton as Chaplain James Hugh ONeill
*Clint Ritchie as Tank captain
*Douglas Wilmer as Major-General Freddie de Guingand
*Patrick J. Zurica as First Lieutenant Alexander Stiller
*Abraxas Aaran as Willy
*Florencio Ararilla as Soldier (uncredited)
*Brandon Brady as Lieutenant Young (uncredited) 
*Charles Dennis as Soldier (uncredited)
*Paul Frees as Reporter #2 (voice/uncredited)
*Dolores Judson as Knutsford Welcome Club Dignitary (uncredited)  
*Lowell Thomas as Himself-Movietone News Narrator (voice/uncredited)

==Production==
=== Script preparation ===
Attempts to make a film about the life of Patton had been ongoing for over fifteen years, commencing in 1953.  Eventually, the Patton family was approached by the producers for help in making the film. The filmmakers desired access to Pattons diaries, as well as input from family members. However, by unfortunate coincidence, the producers contacted the family the day after Beatrice Ayer Patton, the generals widow, was laid to rest. After this encounter, the family refused to provide any assistance to the films producers.
 General of the Army Omar Bradley.
 Brigadier General General Dwight Eisenhower throughout the film, he is not portrayed on screen. At the time of production, Eisenhower was still alive, but died in March, 1969, before the film was released.

===The opening===
  American flag. R rating; in the opening monologue, the word "fornicating" replaced "fucking" when criticizing The Saturday Evening Post. Also, Scotts gravelly and scratchy voice is the complete opposite of Pattons high-pitched, nasal and somewhat squeaky voice, a point noted by historian S.L.A. Marshall.  Yet Marshall also points out that the film contains "too much cursing and obscenity  . Patton was not habitually foul-mouthed. He used dirty words when he thought they were needed to impress." 

When Scott learned that the speech would open the film, he refused to do it, as he believed that it would overshadow the rest of his performance. Director Franklin J. Schaffner assured him that it would be shown at the end. The scene was shot in one afternoon at Sevilla Studios in Madrid, with the flag having been painted on the back of the stage wall. 

All the medals and decorations shown on Pattons uniform in the monologue are authentic replicas of those actually awarded to Patton. However, the general never wore all of them in public and was in any case not a four-star general at the time he made the famous speeches on which the opening is based. He wore them all on only one occasion, in his backyard in Virginia at the request of his wife, who wanted a picture of him with all his medals. The producers used a copy of this photo to help recreate this "look" for the opening scene.

===Locations===
Most of the film was shot in Spain. One scene, which depicts Patton driving up to an ancient city that is implied to be Carthage, was shot in the ancient Roman city of Volubilis, located in Morocco. The early scene, wherein Patton and Muhammed V are reviewing Moroccan troops including the Goumiers, was shot at the Royal Palace in Rabat. One unannounced battle scene was shot the night before, which raised fears in the Royal Palace neighborhood of a coup détat. One paratrooper was electrocuted in power lines, but none of this battle footage appears in the film.  Also a scene at the dedication of the welcome center in Knutsford in Cheshire, England, was filmed at the actual site. The scenes set in Africa and Sicily were shot in the south of Spain (Almeria ), while the winter scenes in Belgium were shot near Segovia (to which the production crew rushed when they were informed that snow had fallen). 
 Spanish Protectorate from 1912 to 1956).

===Use of footage===
A sizeable amount of battle scene footage was left out of the final cut of Patton, but a use was soon found for it. Outtakes from Patton were used to provide battle-scenes in the made-for-TV film Fireball Forward which was first broadcast in 1972. The film was produced by Patton producer Frank McCarthy and Edmund North wrote the screenplay. One of the cast-members of Patton&nbsp;— Morgan Paull&nbsp;— appeared in this production alongside Eddie Albert, Ben Gazzara and Ricardo Montalban. The plot featured a general taking command of a U.S. infantry division with a high casualty rate, a reputation as a hard-luck outfit and a suspected traitor hiding in its midst. 

===Music=== Best Original top twenty-five American film scores.   The original soundtrack has been released three times on disc and once on LP; through Twentieth-Century Fox Records in 1970; through Tsunami Records in 1992, through Film Score Monthly in 1999, and a two-disc extended version through Intrada Records in 2010.  

====2010 Intrada Records Album====

=====Disc One=====
{{Tracklist
| collapsed = yes
| headline  = Original Motion Picture Soundtrack
| title1    = Patton Salute (Solo Bugle)
| length1   = 0:44
| title2    = Main Title
| length2   = 3:08
| title3    = The Battleground
| length3   = 2:14
| title4    = The Cemetery
| length4   = 2:42
| title5    = The First Battle
| length5   = 2:50
| title6    = The Funeral
| length6   = 1:54
| title7    = The Hospital
| length7   = 3:36
| title8    = The Prayer
| length8   = 1:11
| title9    = No Assignment 
| length9   = 2:23
| title10   = Patton March
| length10  = 1:53
| title11   = Attack
| length11  = 3:15
| title12   = German Advance
| length12  = 2:32
| title13   = An Eloquent Man
| length13  = 1:43
| title14   = The Payoff
| length14  = 2:26
| title15   = A Change Of Weather
| length15  = 1:23
| title16   = Pensive Patton
| length16  = 0:16
| title17   = End Title
| length17  = 2:20
| title18   = Echoplex Session
| length18  = 5:29
}}

=====Disc Two=====
{{Tracklist
| collapsed = yes
| headline  = Original 1970 Score Album
| title1    = Patton Speech (spoken by George C. Scott)
| length1   = 4:54
| title2    = Main Title
| length2   = 2:17
| title3    = The Battleground
| length3   = 2:19
| title4    = The First Battle
| length4   = 2:48
| title5    = Attack
| length5   = 3:14
| title6    = The Funeral
| length6   = 1:53
| title7    = Winter March
| length7   = 1:55
| title8    = Patton March
| length8   = 2:04
| title9    = No Assignment
| length9   = 1:59
| title10   = German Advance
| length10  = 2:31
| title11   = The Hospital
| length11  = 3:18
| title12   = The Payoff
| length12  = 2:22
| title13   = End Title & Speech (spoken by George C. Scott)
| length13  = 1:01
| title14   = End Title (sans dialogue)
| length14  = 1:11
}}

==Distribution==

===First telecast===
Patton was first telecast by American Broadcasting Company|ABC-TV as a three hours-plus color film special in the fall of 1972, only two years after its theatrical release. This was highly unusual at the time, especially for a roadshow theatrical release which had played in theatres for many months. Most theatrical films at that time had to wait at least five years for their first telecast. Another unusual element of the telecast was that very, very little of Pattons profanity-laced dialogue was cut (only two sentences, one of which contained no profanity, were cut from the famous opening speech in front of the giant U.S. flag).

===Home media===
Patton was first released on DVD in 1999 featuring a partial audio commentary by a Patton historian. Then again in 2006 with a commentary by screenwriter Francis Ford Coppola and extra bonus features.

The film made its Region A (locked) Blu-ray debut in 2008 to much criticism for its excessive use of digital noise reduction on the picture quality. In 2012, a remaster was released with much improved picture quality.  In June 2013 Fox UK released the film on Region B Blu-ray, but reverted to the 2008 transfer.

==Reaction==

===Critical response===
Roger Ebert said of George C. Scott, "It is one of those sublime performances in which the personalities of the actor and the character are fulfilled in one another."  Online film critic James Berardinelli has called Patton his favorite film of all time  and "...to this day one of Hollywoods most compelling biographical war pictures." 

According to Bob Woodward and Carl Bernsteins book The Final Days, it was also Richard Nixons favorite film. He screened it several times at the White House and during a cruise on the Presidential yacht. Before the 1972 Nixon visit to China, then Chinese Premier Zhou Enlai specially watched this film in preparation for his meeting with Nixon.

Review aggregate website Rotten Tomatoes reported that 98% of critics gave the film a positive review based on 41 reviews, with an average score of 8.4/10. The film is currently No. 100 on Rotten Tomatoes list of best rated films.  Rotten Tomatoes summarizes the critical consensus as, "George C. Scotts sympathetic, unflinching portrayal of the titular general in this sprawling epic is as definitive as any performance in the history of American biopics." 

===Accolades===
Scotts performance won him an Academy Award for Best Actor in 43rd Academy Awards|1971. He famously refused to accept it, citing a dislike of the voting and even the actual concept of acting competitions.  He was the first actor to do so.
 Best Picture, Best Director, Best Original Best Film Best Sound Douglas Williams, Best Art Frank McCarthy.
 Best Cinematography, Best Visual Best Music, Original Score.   

In 2006, the Writers Guild of America selected Francis Ford Coppola and Edmund Norths adapted screenplay as the 94th best screenplay of all time.

American Film Institute Lists
*AFIs 100 Years...100 Movies - #89
*AFIs 100 Years...100 Heroes and Villains: General George S. Patton - #29 Hero
*AFIs 100 Years...100 Movie Quotes:
**"Now, I want you to remember that no bastard ever won a war by dying for his country. He won it by making the other poor dumb bastard die for his country." - Nominated
*AFIs 100 Years of Film Scores - Nominated
*AFIs 100 Years...100 Cheers - Nominated
*AFIs_100_Years...100_Movies#2007_update|AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated
*AFIs 10 Top 10 - Nominated Epic film

==Sequel==
A television movie|made-for-television sequel, The Last Days of Patton, was produced in 1986. Scott reprised his title role. The film was based on Pattons final weeks after being mortally injured in a car accident, with flashbacks of Pattons life.

==Notes==
 

==Further reading==
 
*In 2005, Pattons wifes "Button Box" manuscript was finally released by his family, with the posthumous release of Ruth Ellen Patton Tottens book, The Button Box: A Daughters Loving Memoir of Mrs. George S. Patton. 
*  Suids book contains an extended discussion of the production of Patton and of public and critical response to the film; the discussion occupies most of the chapter, "13. John Wayne, The Green Berets, and Other Heroes".

==External links==
 
* 
* 
*  
*   from AmericanRhetoric.com

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 