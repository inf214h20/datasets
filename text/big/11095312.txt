Frankenstein 1970
{{Infobox Film | name = Frankenstein 1970
  | image = frankenstein1970poster.jpg
  | caption = 
  | director = Howard W. Koch
  | producer = Aubrey Schenck
  | writer = Mary Shelley George Worthing Yates Aubrey Schenck Charles A. Moses Richard H. Landau
  | starring = Boris Karloff Don "Red" Barry
  | music = Paul Dunlap
  | based on       =  
  | cinematography = Carl E. Guthrie
  | editing = John A. Bushelman
  | distributor = Aubrey Schenck Productions
  | released = United States &ndash; July 20, 1958 
  | runtime = 83 minutes
  | country = United States
  | language = English
  | budget = $110,000 Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 418  Tom Weaver, It Came from Horrorwood: Interviews with Moviemakers in the SF and Horror Tradition McFarland, 2000 p 279 
  }}
 Allied Artists.

For several years, only a pan and scan VHS tape of the film was available. In October 2009, Warner Brothers released the DVD "Karloff & Lugosi Horror Classics," which includes Frankenstein 1970 as one of the four films, and features an audio commentary by one the films co-star, Charlotte Austin, and fan historians Tom Weaver and Bob Burns.

== Plot == Baron Victor von Frankenstein, who suffered at the hands of the Nazis as punishment for not cooperating with them during World War II. Horribly disfigured, he nevertheless continues his work as a scientist. Needing funds to support his experiments, the Baron allows a television crew to shoot a made-for-television horror film about his monster-making family at his castle in Germany.

This arrangement gives the Baron enough money to buy an atomic reactor, which he uses to create a living being, modeled after his own likeness before he had been tortured by the Nazis. When the Baron runs out of body parts for his work, however, he proceeds to kill off members of the crew, and even his faithful butler, for more spare parts.  Finally, however, the monster turns on the Baron, and they are both killed in a blast of radioactive steam from the reactor.  After the reactor is shut down and the radiation falls to safe levels, the monsters bandages are removed, revealing the Barons own face prior to his being disfigured by the Nazis, and an audio tape is played back, in which the Baron reveals that he had intended for the monster to be a perpetuation of himself, as he was the last of the Frankenstein family line.

== Cast ==
* Boris Karloff – Baron Victor von Frankenstein
* Tom Duggan – Mike Shaw
* Jana Lund – Carolyn Hayes Donald Barry – Douglas Row
* Charlotte Austin – Judy Stevens
* Irwin Berke – Inspector Raab
* Rudolph Anders – Wilhelm Gottfried
* Norbert Schiller – Schutter, Frankensteins butler John Dennis – Morgan Haley The Monster

==Production==
The films main set was borrowed from the movie Too Much, Too Soon (1958). 
==Reception==
The film was sold to Allied Artists for $250,000. 

==References==
 

== External links ==
* 
*  

 

 
 
 
 
 
 
 


 
 