Bottles (film)
{{Infobox Hollywood cartoon
| cartoon_name = Bottles
| series = Happy Harmonies
| image =
| caption =
| director = Hugh Harman
| story_artist =
| animator =
| voice_actor =
| musician = Scott Bradley (unc.)
| producer = Hugh Harman Rudolf Ising
| studio =
| distributor = Metro-Goldwyn-Mayer
| release_date = January 11, 1936
| color_process = Technicolor
| runtime = 10 minutes
| preceded_by =
| followed_by =
| movie_language = English
}}
 MGM Happy Harmonies animated cartoon directed by Hugh Harman and produced by Rudolf Ising.

==Plot==
Once upon a time in an old apothecary building on a dark, rainy and stormy night, there was an old, ancient pharmacist working on chemicals when hes sitting on his stool. When he falls asleep, a victim to a drop of skrinking poison of his own devising his body is shrunk to the dimensions of thousands of bottles that resides from shoulder to shoulder on the towering shelves. The bottles and containers come to life in a frantic parade of singing and dancing in a manner that are good, clean, fun and friendly.

But there is a sinister presence in this fantasy land: a bottle of poison becomes an evil, maniacal skeleton bent on tormenting our hero. He dispatches spirits of ammonia to seize the poor man and hurls him into a witchs brew of colorful potions. In this sequence, the animators use a dazzling variety of perspectives to show the little guys journey through a maze of glass tubing filled with colorful liquids bubbling away.

The pharmacist wakes to find that he has been reduced to pint size, and that his bottles have turned into people and ceases to exist in his wild, and horrible nightmare.

==Characters==
The Pharmacist is an old and ancient man who is working late making some magical chemicals, liquids and potions around the interior of the old, and vintage apothecary building with windows, top and glass bottom shelves, and a table with a stool.

 
 
 
 
 
 


 