The Last Journey
{{Infobox film
| name           = The Last Journey
| image          = 
| image_size     = 
| caption        = 
| director       = Bernard Vorhaus
| producer       = Julius Hagen
| writer         = John Soutar H. Fowler Mear Joseph Jefferson Farjeon
| narrator       = 
| starring       = Godfrey Tearle Hugh Williams Judy Gunn Mickey Brantford
| music          = W.L. Trytel
| cinematography = William Luff Percy Strong
| editing        = Lister Laurance
| studio         = Twickenham Studios
| distributor    = Twickenham Film Distributors Ltd. (UK) Atlantic Pictures Corporation (US)
| released       = 1936
| runtime        = 66 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Last Journey is a 1936 British thriller film directed by Bernard Vorhaus and starring Godfrey Tearle, Hugh Williams and Judy Gunn.

==Production==
The film was made at Twickenham Studios and is considered a quota quickie. 

==Synopsis==
A train driver (Julien Mitchell) on his last journey before retirement thinks his fireman is having an affair with his wife.  The driver intends to kill himself and his passengers by crashing the train.  The train is filled with colourful characters, including a psychoanalyst who persuades the driver not to do it.

==Cast==
* Godfrey Tearle as Sir Wilfred Rhodes
* Hugh Williams as Gerald Winter
* Judy Gunn as Diana Gregory
* Mickey Brantford as Tom
* Julien Mitchell as Bob Holt
* Olga Lindo as Mrs. Holt Michael Hogan as Charlie
* Frank Pettingell as Goddard
* Eliot Makeham as Pip
* Eve Gray as Daisy

==References==
 

==Bibliography==
* Richards, Jeffrey (ed.). The Unknown 1930s: An Alternative History of the British Cinema, 1929- 1939. I.B. Tauris & Co, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 