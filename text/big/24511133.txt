Anjaana Anjaani
 
 
{{Infobox film
| name                = Anjaana Anjaani
| image               = Anjaanaanjaani3.jpg
| caption             = Theatrical release poster
| director            = Siddharth Anand
| producer            = Sajid Nadiadwala
| story               = Mamta Anand
| screenplay          = Advaita Kala Siddharth Anand
| starring            = Ranbir Kapoor Priyanka Chopra Zayed Khan
| music               = Songs:  
| studio              = Nadiadwala Grandson Entertainment
| distributor         = Eros Entertainment
| cinematography      = Ravi K. Chandran
| editing             = Rameshwar S. Bhagat
| released            =  
| runtime             = 151 minutes
| country             = India
| language            = Hindi
| budget              =     
| gross               =   
}}
Anjaana Anjaani (pronounced Anjānā Anjānī; Strangers) is a 2010 Hindi romance film directed by Siddharth Anand, starring Ranbir Kapoor, Priyanka Chopra and Zayed Khan. It was produced by Sajid Nadiadwala under his Nadiadwala Grandson Entertainment Pvt. Ltd banner. The music was composed by Vishal-Shekhar, who have composed the music for all of Siddharths past three films. The cinematography is handled by Ravi K. Chandran. Anjaana Anjaani was scheduled to release on 24 September, then it was postponed to 1 October 2010. 

== Plot ==
Anjaana Anjaani is the story of Kiara (Priyanka Chopra) and Akash (Ranbir Kapoor). Kiara is based in San Francisco, while Akash is a New York City boy. Akash has to clear a loan of $12 million, which he is unable to partly due to the stock market crash. Unable to find any means, suicide seems the only option. He decides to jump off the George Washington Bridge. This is when he meets Kiara, who is also bound to commit suicide because she caught her fiancé Kunal (Zayed Khan) cheating on her. They both try to put an end to their lives but are deterred by the coastguards. Still keen on ending their lives, Akash deliberately gets hit by a car and Kiara falls on the bridge and breaks her neck. This proves to be yet another failed attempt and they end up in the hospital together. Kiara takes Akash to her house as his house was seized by the bank.

Continuing their attempts at suicide, they try to kill themselves five times but in vain. They make a pact to end their lives on 31 December 2009. With 20 days to go, they decide to fulfill their unfinished wishes and thus begin their journey together. Kiara helps Akash find a date and shares with him how Kunal cheated on her. The next day, Akash cleans Kiaras messy apartment. Akash, who cant swim is forced to fulfill Kiaras wish of swimming in the cold Atlantic Ocean. At sea, Kiara falls overboard and Akash rescues her which in turn causes their yacht to drift away from them. As the two slowly succumb to hypothermia, Kiara continues telling her story to Akash. The coast guard that saw them at the bridge earlier, comes to their rescue.
 Las Vegas on a holiday as Akash never went on a holiday and they end up together in bed. Realization dawns on Akash when Kiara tells him that she still loves Kunal. On Akashs insistence, she moves back with her parents and gives Kunal a second chance, while Akash moves in with his friend and colleague (Vishal Malhotra) and decides to go back to India on the night of 31 December to start afresh.

Akash attends the bank settlement and makes up with his friends, with whom he was at loggerheads. He also reconciles with his father. Meanwhile, Kiara cant stop thinking about Akash. On the 31st, Kiara realises that she has fallen for Akash. Kunal realises this and drops her at the airport. Kiara reaches the bridge but finds herself alone and believes she will never see Akash again. Just then, Akash arrives. Then they go to the sea to die. At sea, Akash throws a beer bottle with a note in it. Kiara reads the paper inside the bottle and Akash proposes to her. Surprised, Kiara accepts the proposal and the couple share a kiss while the coast guard rescues them. As credits roll, it is shown that Akash and Kiara got married two years later and had a baby boy.

== Cast ==
* Ranbir Kapoor as Akash
* Priyanka Chopra as Kiara
* Zayed Khan as Kunal
* Pooja Kumar as Peshto
* Tanvi Azmi as the Doctor
* Kumar Pallana as the Coast Guard
* Vishal Malhotra as Akashs friend
* Nakul Kamte as Kiaras dad

== Production ==
Filming began on 12 December 2009 and was completed early July 2010. Anjaana Anjaani was shot in New York,  Los Angeles, Las Vegas and San Francisco, while some parts of the film were shot in Malaysia and Thailand. The film was slated for release on 24 September 2010, but the release date was postponed to 1 October 2010 due to its clash with the Babri Masjid verdict. 

== Reception ==

=== Critical reception === IBN rated it 1/5: "Anjaana Anjaani, unfortunately, is not only painfully predictable but also deathly boring."  Sanjukta Sharma of Mint said, "This is a film of a few memorable moments. Overall, the script doesn’t hold up and no amount of external finesse can redeem it."  A positive review came from Taran Adarsh from Bollywood Hungama, who gave the film 4 stars out of 5: "On the whole, Anjaana Anjaani is an unpretentious romantic saga that revels in the exuberance of newly found love. The film has everything going for it: top notch performances by two super-stars, good music, a sensitive director with a good track record, a simple yet absorbing story and of course, terrific moments (emotional as well as humorous). Dont miss this one." 

=== Box office ===
The films opening weekend at the box office in India earned approximately  257.5&nbsp;million.  Made on a budget of approximately  43 crore, Anjaana Anjaani immediately recovered  22 crore through the sale of satellite rights to Star India,  8 crore from music rights sold to T-Series, and  130&nbsp;million from theatrical distribution rights sold in a few territories. The movie earned $854,757 in the United States and earned 475,336 from the UK.  Box Office India declared the movie as above average grosser. 

== Awards and nominations ==
;Filmfare Awards Best Music Director – Vishal-Shekhar
; Apsara Film & Television Producers Guild Awards Ahmed Khan for "Anjaana Anjaani"  2011 Zee Cine Awards Best Music Director – Vishal-Shekhar 

== Soundtrack ==
{{Infobox album
| Name        = Anjaana Anjaani
| Type        = soundtrack
| Artist      = Vishal-Shekhar
| Cover       = Anjaanaanjaani_Album.jpg
| Released    = 19 August 2010
| Recorded    =
| Genre       = Film soundtrack
| Length      = 42:33
| Label       = T-Series
| Producer    = Vishal-Shekhar
| Chronology = Vishal Shekhar
| Last album = I Hate Luv Storys (2010)
| This album = Anjaana Anjaani (2010)
| Next album = Break Ke Baad (2010)
}}

=== Track listing ===
{{Track listing
| extra_column = Performer(s)
| title1 = Anjaana Anjaani Ki Kahani | extra1 = Nikhil DSouza, Monali Thakur | length1 = 4:47
| title2 = Hairat | extra2 = Lucky Ali | length2 = 4:09
| title3 = Aas Paas Khuda | extra3 = Rahat Fateh Ali Khan | length3 = 5:21
| title4 = Tumse Hi Tumse | extra4 = Shekhar Ravjiani, Caralisa Monteiro | length4 = 4:23
| title5 = Tujhe Bhula Diya | extra5 =Mohit Chauhan, Shekhar Ravjiani, Shruti Pathak | length5 = 4:41
| title6 = I Feel Good | extra6 = Vishal Dadlani, Shilpa Rao | length6 = 5:23
| title7 = Anjaana Anjaani | extra7 = Vishal Dadlani, Shilpa Rao | length7 = 5:55
| title8 = Tujhe Bhula Diya | note8 = Remix | extra8 = Mohit Chauhan, Shekhar Ravjiani, Shruti Pathak | length8 = 4:32 extra9 = Rahat Fateh Ali Khan, Shruti Pathak | length9 = 3:23
}}

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 