Blue Steel (1989 film)
{{Infobox film|
  name           = Blue Steel |
  image          = Blue Steel (1990 film).jpg|
  caption        = Theatrical release poster|
  director       = Kathryn Bigelow |
  producer       = Edward R. Pressman Oliver Stone|
  writer         = Kathryn Bigelow Eric Red |
  starring       = Jamie Lee Curtis   Ron Silver   Clancy Brown   Elizabeth Peña   Louise Fletcher|
  distributor    = Metro-Goldwyn-Mayer |
  editing        = Lee Percy|
  cinematography = Amir Mokri|
  released       = May 1989  (Cannes Film Festival|Cannes) , March 16, 1990  (USA)  |
  runtime        = 102 minutes |
  studio         = Lightning Pictures Precision Films Mack-Taylor Productions|
  country        = United States |
  language       = English |
  music          = Brad Fiedel|
  awards         = |
  budget         = |
  gross          = $8,217,997 (USA) |
}} action thriller MGM due to Vestrons financial problems and eventual bankruptcy at the time.

==Plot==
Megan Turner (Curtis) is a rookie New York City policewoman who shoots and kills a suspect (Tom Sizemore) with her police-issue .38 Special Smith & Wesson Model 10 revolver while hes holding up a neighborhood market.  The suspects .44 Magnum Smith & Wesson Model 29 revolver lands on the floor of the market in the shopping area as the suspect is blown backward through the front window.

As she continues to the checkout area, Turner nearly steps on the suspects gun directly in front of Eugene Hunt (Silver), a commodities trader, who is also a latent psychopath.  Hunt takes the gun and slips away, using it to commit several bloody and brutal murders over the next few days. Because the robbers weapon was not found at the scene, Turner is accused of killing an unarmed man.

While the officer attempts to clear her name with Chief Hoyt and her superiors, Hunt begins to romance the suspended Turner in a twisted love fetish. Turner arrests him but he is freed by his attorney, Mel Dawson. He begins to stalk Turner at her family home, an uncomfortable place where Turner remembers her mother being physically abused by her dad.

Turner fights to keep her badge and solve the murders with the help of Detective Nick Mann (Clancy Brown), while trying to figure out her relationship with a killer. Hunt turns up at her apartment, injures Turner and kills her best friend, Tracy. This causes Turner to have an emotional breakdown.

She spends the night with Mann, her fellow officer. Mann is ambushed by Hunt when he goes to the bathroom. Turner doesnt hear the shot because it was muffled. Hunt attacks and rapes her, and she shoots him, but he runs off. Mann is unconscious and taken to the hospital.

Determined to find Hunt and finish him off, Turner finally shoots and kills him after a long and violent confrontation and a bullet wound to her shoulder. She is then taken away by fellow police officers.

==Cast==
* Jamie Lee Curtis as Megan Turner
* Ron Silver as Eugene Hunt
* Clancy Brown as Nick Mann
* Louise Fletcher as Shirley Turner
* Philip Bosco as Frank Turner
* Richard Jenkins as Dawson
* Elizabeth Peña as Tracy
* Kevin Dunn as Asst. Chief Stanley Hoyt
* Tom Sizemore as Robber

==Reception==
The movie gained positive reviews,    garnering a "fresh" 70% rating on rotten tomatoes based on 20 reviews.

===Box Office===
The film was not a box office success. 

==References==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 