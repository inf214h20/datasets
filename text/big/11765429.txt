Irina Palm
 
 
 
{{Infobox film
| name        = Irina Palm
| image       = Irinapalmposter.JPG
| caption     = Movie poster
| writer      = Martin Herron   Philippe Blasband   Sam Garbarski
| starring    = Marianne Faithfull   Miki Manojlović   Kevin Bishop
| director    = Sam Garbarski
| producer    = Sébastien Delloye   Diana Elbaum   Georges van Brueghel
| music       = Ghinzu
| distributor = Strand Releasing (US)
| released    = 13 February 2007 (Premiere in Berlin)
| runtime     = 103 minutes English
|}}

Irina Palm (2007) is a tragicomedy film starring Marianne Faithfull and Miki Manojlović. It is a co-production of five countries (Belgium, Luxembourg, Great Britain, Germany and France). The film premiered at the 2007 Berlin International Film Festival. The film has earned over US $10 million worldwide.

== Plot ==
The 50-year-old  ), the owner of the shop, explains to her frankly that "hostess" is a  ), and she, at the other side, gives a handjob (however, the film does not show any penis). Her colleague, Luisa, shows her how to do it, and after the first hesitation, she quickly develops good skills. However, she keeps her work secret from friends and family, which leads to uncomfortable situations.  After a while, she tells some friends. They are quite interested and ask various details.

Under the pseudonym Irina Palm, she becomes increasingly more successful and well-paid, 600 to 800 pounds a week. However, the health of her grandson deteriorates quickly, therefore she asks and receives an advance payment of 6000 pounds for 10 weeks of work. She gives the money to her son Tom (Kevin Bishop) without telling how she got it. He follows her to learn about the source of the money, and is furious when he discovers what she does. He wants her to never go there again and says he will himself return the money and not go to Australia. However, his wife, Sarah, is thankful to Maggie for her sacrifice to save the boy. Tom and Maggie reconcile and Tom, Sarah, and Olly go to Australia.

Luisa is fired due to Maggies success. She is very angry at Maggie. A competitor of Mikis offers Maggie a better job, as supervisor of prostitutes in a room with multiple glory holes. She would get 15% of their earnings. She hesitates but eventually declines the offer; becoming a "madam" is a step too far for her. Maggie and Miki eventually fall in love with each other.

When Maggie is walking on Shaftesbury Avenue, the poster of the acclaimed play "The Night of the Iguana", staged at the Lyric Theatre in 2006, is clearly visible.

== Cast == 	
 
* Marianne Faithfull as Maggie: the films protagonist who starts working in a sex shop to pay for her grandsons treatment
* Miki Manojlović as Miklos: owner of the sex shop
* Kevin Bishop as Tom: Maggies son
* Siobhan Hewlett as Sarah: Toms wife; Maggies daughter-in-law
* Corey Burke as Olly: Tom and Sarahs son; Maggies grandson
* Dorka Gryllus as Luisa: One of Maggies colleagues in the sexshop
* Jenny Agutter as Jane: One of Maggies friends
* Meg Wynn Owen as Julia: One of Maggies friends
* Susan Hitch as Beth: One of Maggies friends
* Flip Webster as Edith: the local shopkeeper

== Awards == Golden Bear for Best Movie at the 2007 Berlin International Film Festival

* Miki Manojlovic is nominated for Best European Actor of 2007 by European Film Academy for his role in Irina Palm
 The Queen.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 