Mylanji (film)
{{Infobox film 
| name           = Mylanji
| image          =
| image_size     = |
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       =
| writer         =
| screenplay     =
| starring       =
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         =
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair.  The film had musical score by A. T. Ummer.   

==Cast==

 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by P. Bhaskaran and .

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Alankaara chamayathaal || Chorus, Laila Razzak || P. Bhaskaran || 
|-
| 2 || College laila || K. J. Yesudas, Ambili || P. Bhaskaran || 
|-
| 3 || Ithuvare ithuvare || K. J. Yesudas, Ambili || P. Bhaskaran || 
|-
| 4 || Kaalu manniluraykkaatha || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Kokkara kokkara || Vilayil Valsala, VM Kutty || P. Bhaskaran || 
|-
| 6 || Maamalayile || K. J. Yesudas || P. Bhaskaran || 
|-
| 7 || Malarvaaka poomaaran || Chorus, Laila Razzak ||  || 
|}

==References==
 

==External links==
*  

 
 
 


 