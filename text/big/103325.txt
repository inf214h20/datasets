Royal Space Force: The Wings of Honnêamise
{{Infobox film
| name            = Royal Space Force: The Wings of Honnêamise
| image           = HonneamiseHD.jpg
| caption         = Cover art of the 2007 HD DVD release by Bandai Visual
| film name       = {{Film name
 | kanji        = 王立宇宙軍 オネアミスの翼
 | romaji       = Ōritsu Uchūgun: Oneamisu no Tsubasa}}
| director     = Hiroyuki Yamaga
| writer         = Hiroyuki Yamaga
| producer     = Makoto Yamashina
| starring       = Leo Morimoto Mitsuki Yayoi
| music         = Ryuichi Sakamoto Yuji Nomi Koji Ueno Haruo Kubota
| studio          = Gainax   Bandai Visual Toho Towa
| released      =  
| runtime        = 119 minutes
| country        = Japan
| language      = Japanese
| budget         = ¥800 million
}}
 animated science fiction film and the first film produced by Gainax and Bandai Visual. It was directed and written by Hiroyuki Yamaga. The film was released on March 14, 1987 and grossed only modestly in the box office.  Since then, it has received very positive reviews.     A sequel was intended to be released set 50 years later, but due to lack of funds, Gainax abandoned it part way through production ; former president of Gainax, Toshio Okada cited a fundamental dissatisfaction with the script and plot.  However, it was announced in March 2013 that the sequel is in production once again. 

==Plot==
 
On an alternate Earth, an industrial civilization is flourishing amid an impending war between two bordering nations: the Kingdom of Honneamise and "The Republic".
 religious woman named Riquinni Nonderaiko. Seeing Lhadatt as a prime example of what mankind is capable of, and understanding the godliness and ground-breaking nature of his work, she inspires him to become the first man in space.

His training as an astronaut parallels his coming of age, and he and the rest of the members of the space project overcome technological difficulties, doubt, the machinations of their political masters, and a botched assassination attempt by the enemy nation. Amidst the debacle, Lhadatt becomes worn out by the overbearing publicity surrounding his space mission, prompting him to stay with Riquinni for a while; he then comes close to raping her one night while catching her undressing, causing a temporary rift between them that is later mended thanks to Riquinnis kindness. 
 fighter planes duel high above an armored advance towards a defensive trench network. Despite calls to pull out, Lhadatt, already in the space capsule and determined to finish what he started, convinces the frightened and vulnerable ground crew to complete the launch. The spectacular launch stuns both sides into inaction as Lhadatt goes into orbit. With no more reference to the world below (beyond a slight suggestion that both nations plans for war have been foiled), Lhadatt prays for humanitys forgiveness.
 montage of his own life and his worlds history and achievements are shown. Meanwhile on the planets surface, Riquinni witnesses the first snow fall and gazes into the sky, thinking of Lhadatt.

==Cast==
{| class="wikitable"
|-
! Character!! Japanese!! English dub
|- John Hurt
|-
| Riquinni Nonderaiko || Mitsuki Yayoi || Patricia Ja Lee
|-
| Manna Nonderaiko || Aya Murata || Wendee Lee
|-
| Marty Tohn || Kazuyuki Sogabe || Bryan Cranston
|-
| General Khaidenn || Minoru Uchida || Steve Bulen
|-
| Dr. Gnomm || Chikao Ōtsuka || Michael Forest
|- Masato Hirano || Tom Konkle
|-
| Yanalan || Bin Shimada || 
|-
| Darigan || Hiroshi Izawa || 
|-
| Domorhot || Hirotaka Suzuoki || Jan Rabson
|-
| Tchallichammi || Kouji Totani || 
|-
| Majaho || Masahiro Anzai || Tony Pope
|-
| Nekkerout || Yoshito Yasuhara || Dan Woren
|-
| Prof. Ronta || Ryūji Saikachi || Kevin Seymour
|-
|}

==Production==
Honneamise was based on a story by Hiroyuki Yamaga. A Four minute short The Royal Space Force was funded by Gainax and presented to Bandai Visual who were planning to enter the film making industry.  Bandai president Makoto Yamashina approved an 8 billion Yen budget for a full length film, the record for an anime film the time. The key staff travelled to the United States for aviation and spacecraft research, including the launch of Space Shuttle Discovery. Production began on the movie after their return in September 1985.   

All the characters were designed by Yoshiyuki Sadamoto. Hideaki Anno worked as special effects artist and as part of the animation design staff. Both Anno and Sadamoto would gain notoriety after working together in Neon Genesis Evangelion years later. Famous musician and actor Ryuichi Sakamoto created most of the film score.

==Release== Maiden Japan re-released the movie separately on Blu-ray and DVD on October 15, 2013.  Anime Limited announced that they intend to release Wings of Honneamise on DVD and Blu-ray on March 23, 2015 in the United Kingdom.  

==Reception==
Wings has received universal acclaim from film critics since its release in 1987. Roger Ebert of the Chicago Sun-Times called it "a visually sensational two-hour extravaganza".    DVD Times said that the animation "gives the storyteller complete control over what you see and ultimately how the viewer interprets the directors dream...the astounding animation, the attention to detail seen in every frame is quite daunting, but allows the viewer to discover new subtleties on repeated viewing."  DVD Vision Japan praised the Japanese voice actors performances for their range and emotion, as well as the "very distinctive" characters.  Helen McCarthy in 500 Essential Anime Movies called the film "marvellous" and added that nothing "can disguise the sheer quality of this film." She praised backgrounds and designs, noting that they "show a level of commitment that borders on fanatical".  In a discussion of original versus adapted anime,  Brian Hanson of Anime News Network goes on to say:

 "Its set on a quasi-alternate-reality version of Earth, but without reams of bogus sci-fi hokum, and the screen is littered with wonderful art direction of a world thats sort of familiar, but wonderfully alien. Just look at, for example, the guns the characters use - theyre not laser pistols or handguns, but these strange little things that still look like guns, but cleverly modified to fit within the rest of this wonderful universe that Gainax created. 
And the story! Oh, man! Its not some spunky kid out to save the universe. Its not a band of merry adventurers battling an onslaught of demons. None of that. Its a tender story of a bored and broken man seeking redemption, pouring himself into a cause that he finds important that everyone else thinks is a laughable joke, and finding spiritual enlightenment in spite of his entire endeavor being twisted by corrupt politicians as an excuse to wage war. Its about one man coming to terms with himself. Its a story thats mature, resonant, and authentic. Every single thing in that movie is wholly idiosyncratic. Its wonderful. And not only is it unique, it is exceptionally well done. Every frame is teeming with detail." 

Former president of Gainax, Toshio Okada felt in retrospect that it needed a stronger screenplay, that it was too niche and like an "art" film to be really mainstream and successful - the film did not recoup its budget at the box office.

==Sequel==
In March 1992, Gainax had begun planning and production of an anime movie called  , which was to be a sequel to Royal Space Force: The Wings of Honnêamise set 50 years later (so as to be easier to pitch to investors ) which, like Oritsu, would follow a group of fighter pilots. Production would eventually cease in July 1993: a full-length anime movie was just beyond Gainaxs financial ability – many of its core businesses were shutting down or producing minimal amounts of money: Akai had seen to that – but there wasnt a lot of work tossed our way. With mere pennies coming in, we were having a hard enough time just paying everyones salaries. Finally the order came down for us to halt production on Aoki Uru. We were simply incapable of taking the project any further."  
 King Records Neon Genesis Evangelion:
 "One of the key themes in Aoki Uru had been "not running away." In the story, the main character is faced with the daunting task of saving the heroine … He ran away from something in the past, so he decides that this time he will stand his ground. The same theme was carried over into Evangelion, but I think it was something more than just transposing one shows theme onto another …"  

Gainax has periodically attempted to restart Aoki Uru,  such as releasing a 1998 CD with storyboards, a script, and several hundred pieces of art,  and a 2000 release of a mod to Microsoft Flight Simulator. 

At the 2013 Tokyo Anime Fair, Gainax announced that they are finally producing the Blue Uru film with Honneamise veterans Hiroyuki Yamaga as the director and screenwriter and Yoshiyuki Sadamoto as the character designer, but without Hideaki Annos involvement in the project (given his present work completing the Rebuild of Evangelion for Studio Khara).   

==See also==
* List of animated feature films

==References==
 

==Further reading==
*  
*  
*  
*  
*  

==External links==
*     ( )
*  
*  
*  
* http://replay.waybackmachine.org/20010220212913/http://www.gainax.co.jp/special/rsf/comment-e.html
* http://replay.waybackmachine.org/20010220212937/http://www.gainax.co.jp/special/rsf/report-e.html
* http://replay.waybackmachine.org/20010222005346/http://www.gainax.co.jp/special/rsf/report2-e.html
* http://replay.waybackmachine.org/20010222004532/http://www.gainax.co.jp/special/rsf/report3-e.html
*  in the Encyclopedia of Science Fiction

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 