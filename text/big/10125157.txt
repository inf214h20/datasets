Gunman (film)
{{Infobox film
| name           = Gunman
| image          = Gunman DVD cover.jpg
| caption        = Thai DVD release cover
| director       = Chatrichalerm Yukol
| producer       = 
| writer         = 
| narrator       = 
| starring       = Sorapong Chatree
| music          = 
| cinematography = 
| editing        = 
| distributor    = Mangpong
| released       =  
| runtime        = 140 min.
| country        = Thailand Thai
| budget         = 
}}

  Thai crime amputee Assassination|assassin.

==Plot==

As the opening credits roll, the scene is from the point of a man on a motorcycle at night. The man gets off the bike, walks into a coffee shop, shoots one of the patrons with a pistol and then gets back on the motorcycle.

The scene then cuts to various bystanders being interviewed about what the man looked like. The descriptions vary wildly, except for one thing – the man walked with a limp.

The police are then on the lookout for anyone with a limp. They arrest one man walking down the street carrying a briefcase. In the struggle to apprehend the man, his briefcase comes open, spilling the contents – some sex toys – onto the sidewalk. Because such items are illegal in Thailand, the man is arrested.
 Secret War in Laos, Sommai now works as a barber in a small shop in a canal community of suburban Bangkok.

One day Sommai is cutting hair, when his young son is attacked on a nearby footbridge by some neighborhood boys. They are taunting the boy for having a one-legged father and no mother. Sommai must leave the shop, making a customer angry, to attend to the boy.

After the neighborhood bullies are chased away, Sommais son collapses and starts having some sort of seizure. A local doctor is ill-equipped to treat the boy and recommends he be looked after at home. Sommai has a problem, being a single father, and needing to work, he must find someone else to look after the boy. He asks his vain, selfish ex-wife, who left Sommai after he lost his leg. She refuses. But Nid, the younger sister of his ex-wife, readily agrees. The two, with the boy, then bond as family unit.

Meanwhile, the Bangkok press is abuzz with a police hero, Special Branch Inspector Thanu, known as the Black Hand, for the trademark black leather glove he wears on his gun hand. He cultivated a fearsome reputation, even though the kills he claims credit for were often the work of his subordinates. Thanu also has difficulties at home, with a dissatisfied wife.

One of Thanus subordinates, Officer Chalam, independently investigates the shooting by the one-legged gunman, and through methodical casework, he determines that the gunman is likely an ex-military man, which narrows the suspects down considerably. Chalam immediately suspects Sommai, however Thanu tries to divert attention to other cases.

It turns out that Thanu was Sommais lieutenant in the Secret War, and it was through Thanus cowardly actions that Sommai was left on the field with a leg blown off, to be captured by the Pathet Lao while Thanu and the rest of platoon escape to safety on a helicopter. At all costs, Thanu would rather not confront his old sergeant again. Thanu goes as far as visiting Sommai at his barbershop, telling Sommai to go into hiding.
 mute motorcyclist named Khan, and the two plan more jobs.

Chalam has disobeyed orders by Thanu to stay away from Sommai, and has informants following Sommais every move. After one shooting is narrowly averted, leaving the intended target only injured, Thanu can no longer ignore Sommai.

Thanu is then given the job of watching over an important government minister. Sommai has taken on the job of killing the minister, and succeeds. His mute motorcyclist friend Khan betrays him, but Sommai is able to free himself from Khan and kill him. He then visits his handler, just as his handler and another man are plotting Sommais demise, obtains the payment, and then kills the two men.

Sommai then goes to the hospital to retrieve his son. Thanus team is waiting for him, but Sommai takes Thanu hostage. Holding an automatic pistol to Thanus head, he and is able to obtain a promise from the prime minister that Nid and the boy will be allowed to leave the country. After Nid arrives at the airport and assures Sommai via two-way radio that she will be okay, Sommai surrenders, and hands his pistol to Thanu, who takes it and aims at Sommais back, but then puts the gun down. However, Chalam raises his pistol, and against the orders of Thanu, who shouts "No!", fires, most likely killing Sommai.

==Cast==
* Sorapong Chatree as Sergeant Sommai Moungthup
* Ron Rittichai as Inspector Thanu Adharn

==Production==
The opening shot was filmed by Chatrichalerm Yukol himself, who used a hand-held camera while riding on the back of a motorcycle. The shot was done in three takes, with the third take being used. "After the third one I wouldnt do it again. It was pretty risky. Youre practically blind, when you get off the bike you have to walk into the coffee shop shoot the man and walk back, back on the motorbike with your eyes on the viewfinder, you have to do all the Focus puller|focus-pulling yourself, it was pretty painful. But that was still an easy shot", he told interviewer Thomas Richardson in 1993.   

Another scene involving the helicopter sequence in the Secret War flashback scene has mistakenly thought to be copied from Oliver Stones Platoon (film)|Platoon, which in fact was made in 1986, three years after the release of Gunman. The shot focused on Sommai on the ground as the helicopter rose into the air. "Pretty painful shot, I think. We have a lot of painful shots in that film", Chatrichalerm said in the 1993 interview. "Its difficult because you have to center him the whole time, zoom out at the right time, pull the focus". 

==Gunman series== SLORC colonel, the stories and characters of the two films have no relation, so Salween is a sequel to Gunman in name only.

==DVD==
The DVD of the film, with English language subtitles, was released in Thailand on an all-region, PAL disc.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 