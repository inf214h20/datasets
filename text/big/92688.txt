Room at the Top (1959 film)
 
 
 
{{Infobox film
| name           = Room at the Top
| image          = Room at the Top poster 2.jpg
| image_size     = quad size film poster
| director       = Jack Clayton
| producer       = John and James Woolf Neil Paterson Mordecai Richler (uncredited)
| based on  =  
| starring       = Laurence Harvey Simone Signoret Heather Sears Donald Wolfit Hermione Baddeley
| music          = Mario Nascimbene
| cinematography = Freddie Francis
| editing        = Ralph Kemplen
| studio         = Romulus Films Continental Distributing  
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom
| language       = English
| budget         = £280,000 
}} novel of Neil Paterson with uncredited work by Mordecai Richler. It was directed by Jack Clayton and produced by John and James Woolf. The film stars Laurence Harvey, Simone Signoret, Heather Sears, Donald Wolfit, Donald Houston and Hermione Baddeley. 
 Best Picture, Best Director Best Actor Best Supporting Best Actress Best Adapted Screenplay for Paterson. Baddeleys performance became the shortest ever to be nominated for an acting Oscar (she had 2 minutes and 20 seconds of screen time).

==Plot==
In late 1940s Yorkshire, England, Joe Lampton (Laurence Harvey), an ambitious young man who has just moved from the dreary factory town of Dufton, arrives in Warnley, to assume a secure, but poorly paid, post in the Borough Treasurers Department. Determined to succeed, and ignoring the warnings of a colleague, Soames (Donald Houston), he is drawn to Susan Brown (Heather Sears), daughter of the local industrial magnate, Mr. Brown (Donald Wolfit). He deals with Joes social climbing by sending Susan abroad; Joe turns for solace to Alice Aisgill (Simone Signoret), an unhappily married older woman who falls in love with him.

When Susan returns from her holiday, shortly after the lovers (Joe and Alice) have quarrelled, Joe seduces Susan, and then returns to Alice. Discovering that Susan is pregnant, Mr. Brown, after failing to buy off Joe, coerces him to give up Alice and marry his daughter. Deserted and heartbroken, Alice launches on a drinking bout that culminates in her car-accident death. Distraught, Joe disappears, and after being beaten unconscious by a gang of thugs for making a drunken pass at one of their women, he is rescued by his colleague Soames in time to marry Susan.

==Main cast==
 
* Laurence Harvey as Joe Lampton
* Simone Signoret as Alice Aisgill
* Heather Sears as Susan Brown
* Donald Wolfit as Mr Brown
* Donald Houston as Charlie Soames
* Hermione Baddeley as Elspeth
* Allan Cuthbertson as George Aisgill
* Raymond Huntley as Mr Hoylake John Westbrook as Jack Wales
* Ambrosine Phillpotts as Mrs Brown
* Richard Pasco as Teddy
* Beatrice Varley as Aunt
* Delena Kidd as Eva
* Ian Hendry as Cyril
* April Olrich as Mavis
* Mary Peach as June Samson
* Anthony Newlands as Bernard
* Avril Elgar as Miss Gilchrist
* Thelma Ruby as Miss Breith
* Paul Whitsun-Jones as Laughing Man at Bar
* Derren Nesbitt as Thug in Fight on Tow Path
* Derek Benfield as Man in bar
* Richard Caldicot as Taxi driver
* Wendy Craig as Joan
* Basil Dignam as Priest
* Jack Hedley as Architect
* Miriam Karlin as Gertrude Wilfrid Lawson as Uncle Nat
* Prunella Scales as Council Office Worker John Welsh as Mayor
* Sheila Raynor as Vera
* John Moulder Brown as Urchin (uncredited) Andrew Irvine as Office boy (uncredited)
* Kenneth Waller    as Reggie (uncredited)
 

==Adaptation==
There are some differences from Braines novel. His friend Charles, whom he meets at Warnley in the film, is a friend from his hometown Dufton in the novel. Warnley is called Warley in the novel. More emphasis is paid to his lodging at Mrs Thompsons, which in the novel he has arranged beforehand (in the film, his friend Charles arranges it soon after they meet). In the book, the room is itself significant, and is strongly emphasised early in the story; Mrs Thompsons room is noted as being at "the top" of Warley geographically, and higher up socially than he has previously experienced, and serves as a metaphor for Lamptons ambition to rise in the world.

==Production==
Producer James Woolf bought the film rights to the novel, originally intending to cast Stewart Granger and Jean Simmons. Vivien Leigh was originally offered the part of Alice, in which Simone Signoret was eventually cast.  He hired Jack Clayton as director after seeing The Bespoke Overcoat,  a short which John Woolf had worked (uncredited) and their film company had produced.
 location work in Halifax, West Yorkshire|Halifax, Yorkshire, which stood in for the fictional towns of Warnley and Dufton. Some scenes were also filmed in Bradford, notably with Joe travelling on a bus and spotting Susan in a lingerie shop and the outside of the amateur dramatics theatre. Greystones, a large mansion in the Savile Park area of Halifax, was used for location filming of the outside scenes of the Brown family mansion. Halifax railway station doubled as Warnley Station in the film, and Halifax Town Hall was used for the Warnley Town Hall filming.
 Life at the Top.

==Reception==
The film was critically acclaimed and marked the beginning of Jack Claytons career as an important director. It became the third most popular film at the British box office in 1959 after Carry On Nurse and Inn of the Sixth Happiness. 

==Awards and nominations==
===Academy Awards===
Wins Best Actress in a Leading Role (Simone Signoret) Best Writing, Screenplay Based on Material from Another Medium.
Nominations Best Picture Best Actor in a Leading Role, (Laurence Harvey) Best Actress in a Supporting Role (Hermione Baddeley) Best Director (Jack Clayton)

===BAFTA Awards===
Wins Best British Film Best Film from any Source
* BAFTA Award for Best Actress in a Leading Role (Simone Signoret)
Nominations Best British Actor (Laurence Harvey) Best British Actor (Donald Wolfit)
* Best British Actress (Hermione Baddeley)
* Most Promising Newcomer (Mary Peach)

===Golden Globe Awards===
Win
* Samuel Goldwyn Award
Nomination
* Best Motion Picture Actress – Drama (Simone Signoret)

===Cannes Film Festival===
Win Best Actress (Simone Signoret)   
Nomination Golden Palm (Jack Clayton)

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 