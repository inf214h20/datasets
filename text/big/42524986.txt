The Salt of the Earth (2014 film)
 
{{Infobox film
| name           = The Salt of the Earth
| image          = TheSaltoftheEarth2014.jpg
| alt            = 
| caption        = Film poster
| director       = {{Plainlist|
* Wim Wenders
* Juliano Ribeiro Salgado}}
| producer       = {{Plainlist|
* Wim Wenders
* Lélia Wanick Salgado
* David Rosier
* Julia de Abreu
* Fakhrya Fakhry
* Andrea Gambetta
* Christine Ponelle}}
| writer         = {{Plainlist|
* Wim Wenders
* Juliano Ribeiro Salgado
* David Rosier}}
| starring       = 
| music          = Laurent Petitgand
| cinematography = {{Plainlist|
* Hugo Barbier
* Juliano Ribeiro Salgado}}
| editing        = {{Plainlist|
* Maxine Goedicke
* Rob Myers}}
| studio         = Decia Films
| distributor    = Le Pacte
| released       =  
| runtime        = 110 minutes  
| country        = {{Plainlist|
* France
* Brazil}}
| language       = {{Plainlist|
* French
* Portuguese
* English}}
| budget         = 
| gross          = $3.6 million   
}} biographical documentary film directed by Wim Wenders and Juliano Ribeiro Salgado.    It was selected to compete in the Un Certain Regard section at the 2014 Cannes Film Festival    where it won the Special Prize.  The film was nominated for the Academy Award for Best Documentary at the 87th Academy Awards.  It won the 2014 Audience Award at the San Sebastián International Film Festival and the 2015 Audience Award at the Tromsø International Film Festival.   It also won the César Award for Best Documentary Film at the 40th César Awards.   

The film portrays the works of the Brazilian photographer Sebastião Salgado.

==Reception==
The Salt of the Earth received largely positive reviews from critics. On  , the film has an 82/100 rating based on 22 critics, indicating "universal acclaim". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 