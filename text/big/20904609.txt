Heera-Moti
{{Infobox film
| name           = Heera-Moti
| image          = Heera-Moti.jpg
| image_size     = 
| caption        = 
| director       = Chand
| producer       = Ratan Mohan
| writer         =
| narrator       = 
| starring       =Shatrughan Sinha Reena Roy
| music          = O. P. Nayyar
| Lyrics         = Ahmed Wasi
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1979
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood film directed by Chand and produced by Ratan Mohan. It stars Shatrughan Sinha and Reena Roy. 

==Cast==
*Shatrughan Sinha ...  Vijay / Heeralal P. Srivastav / Heera 
*Reena Roy ...  Neelam 
*Danny Denzongpa ...  Ajay / Moti  Bindu ...  Julie 
*Ajit ...  Pratap Singh 
*Om Shivpuri ...  Trustee  Anwar Hussain ...  Johnny 
*Dev Kumar ...  Sheikh Bin Sheikh 
*Narendra Nath ...  Dracula

==Soundtrack==
Heera Moti Had music by o.p.nayyar & Lyrics by Ahmed Wasi
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Honth Hain Tere Do Lal Heere"
| Mohammed Rafi, Dilraj Kaur
|-
| 2
| "Jai Krishna Hare"
| Mohammed Rafi, Manna Dey
|-
| 3
| "Main Tujh Ko Maut De Doon"
| Dilraj Kaur
|-
| 4
| "Sau Saal Jiyo Tum Jaan Meri"
| Dilraj Kaur
|-
| 5
| "Tum Khud Ko Dekhte Ho Sada"
| Dilraj Kaur
|-
| 6
| "Zindagi Lekar Hatheli Par"
| Mohammed Rafi, Manna Dey, Dilraj Kaur
|}
==External links==
*  

 
 
 
 