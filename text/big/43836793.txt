Jungle Ka Jawahar
{{Infobox film
| name           = Jungle Ka Jawahar 
| image          = Jungle Ka Jawahar.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Basant Pictures
| writer         = 
| screenplay     = 
| story          = Boman Shroff
| based on       = 
| narrator       = 
| starring       = Fearless Nadia John Cawas Leela Kumari Dalpat
| music          = Master Madhavlal
| cinematography = Anant Wadadeker
| editing        = Kamlaker
| studio         = Basant Studios
| distributor    = 
| released       = 1953
| runtime        = 140 min
| country        = India Hindi
| budget         = 
| gross          = 
}} 1953 Hindi action adventure film directed by Homi Wadia and produced by Basant Pictures.    The story was written by Boman Shroff and the special effects were by Babubhai Mistry. Having played daredevil roles starting in the 1930s, Fearless Nadia was still playing lead action roles in the 1950s and continued to act till 1968.    The film starred Fearless Nadia, John Cawas, Leela Kumari, Shyam Sunder and Shapoor Aga.   

The story revolves around a doctor living in the jungle with his daughter, surrounded by wild tribal people who are ruled by the jungle queen, Sheena. 

==Plot==
Mala (Fearless Nadia) lives with her father (Aga Shapoor) who is a doctor, in the jungle. Mala enjoys the jungle life swinging on vines and riding elephants. She also has a horse called Rajput which is able to perform several tricks. Sheena (Leela Kumari) is chief of her tribe and wants a special sceptre called the Rajnishan that the doctor owns, given to him by Sheena’s father. One day a plane lands in the jungle with three men, Kiran (Dalpat), Deepak (Shyam Sunder) and the plane’s pilot Vijay (John Cawas). They are caught by the tribals along with Mala who helped save Vijays life. They are tied to totem poles, ready to be cooked in boiling pots. The doctor arrives in time to save them. Now Kiran and Sheena are both after the Rajnishan which seems to cast a hypnotic spell over the tribals and allows the doctor to access the treasure. Kiran insists the doctor go back with him to the city saying that the doctor’s brother Ramesh is ill. Once there, Kiran kills the doctor and gets Ramesh, a look-alike, to take his place. Kiran, and Ramesh now as the fake doctor, go back to the jungle. The film moves on to action scenes with Mala swinging and tossing people over her head. Good finally triumphs and Mala leaves the jungle with Vijay in his plane.

==Cast==
* Fearless Nadia as Mala
* John Cawas as Vijay
* Shyam Sunder as Deepak
* Leela Kumari as Sheena
* Mehru
* Goldstein as Jingo
* Dalpat as Kiran
* Shapoor Aga as Doctor/Ramesh
* Abdulla as Nimbo
* Raja Sandow
* Rajani as Chulbuli

==Music==
The music director was Master Madhavlal with lyrics written by Firoz Jalandhari and Saraswati Kumar Deepak. The  singers were Sulochana Kadam, Tun Tun and Shyam Sunder. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Shrimati Shrimati Aao Aao 
| Shyam Sunder,Sulochana Kadam
| Saraswati Kumar Deepak
|-
| 2
| Haye Bhaiyya Kaisi Ye Khatpat 
| 
|
|-
| 3
| Pyare Pappu 
| Shyam Sunder,Sulochana Kadam
| Saraswati Kumar Deepak
|-
| 4
| Yeh Dil Mein Do Baatein Chhupa Rakhi Hai 
| Uma Devi
| Firoz Jalandhari
|-
| 5
| Chand Ko Kehte Mohabbatwale 
| Sulochana Kadam
| Firoz Jalandhari
|}


==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 