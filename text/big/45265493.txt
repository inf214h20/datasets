A Song for Martin
 
{{Infobox film
| name           = A Song for Martin
| image          = 
| alt            = 
| caption        = 
| director       = Bille August
| producer       = {{Plainlist|
* Bille August
* Lars Kolvig
* Michael Obel
* Michael Lundberg
}}
| screenplay     = Bille August
| based on       =  
| starring       = {{Plainlist|
* Sven Wollter
* Viveka Seldahl
}}
| music          = Stefan Nilsson
| cinematography = Jörgen Persson
| editing        =  
| studio         = {{Plainlist|
* Moonlight Filmproduction
* Svenska Filmkompaniet
}}
| distributor    = {{Plainlist| Nordisk Film Biografdistribution  
* First Look  
}}
| released       =  
| runtime        = 118 minutes
| country        = {{Plainlist|
* Sweden
* Denmark
}}
| language       = Swedish
| budget         = 
| gross          = $27,983  
}} drama film Danish Bille August and starring Sven Wollter and Viveka Seldahl. Based on the 1994 autobiographic novel Boken om E (The Book about E) by Swedish author Ulla Isaksson, the films themes are loss and Alzheimers disease.      

==Plot==
Martin Fischer is successful composer. During a rehearsal for an upcoming concert he makes contact with Concertmaster|first-violinist Barbara Hartman. During the concert, and Barbara and Martin flirts a lot with each other, although they are both married and have adult children. After the concert, they meet out back. She feels ill, and he says that he will follow her home. They stop at his hotel and goes inside, kisses, and Barbara says she loves him. Somewhat later, she tells her two adult children that she is going to separate from the childrens father. Phillip, the son becomes very angry, while her daughter Karin is more indulgent. The film jumps to Barbaras honeymoon with Martin in Morocco. After they return home, Martin experiences amnesia, and is diagnosed with Alzheimers disease.

The doctor says they should try to live as they always have lived. They go to a restaurant, but Martin will have to pay twice, and it destroys the good mood as he feels humiliated. His condition worsens. He is working on an opera, soon to be completed. After working all night, he claims that he is finally finished. He asks Barbara to go to the post office and send the score to his manager. On the way to the post office Barbara opens the envelope to look it through, and she realizes that its a mess, and that several of the pages are completely blank. Barbara and the manager Biederman agree to pretend that the opera will be performed. Martin and Barbara again go on vacation to Morocco, but Martin is now too affected by the disease. One morning he is gone, and Barbara finds him sitting at a café with no pants on. Later they are out swimming and he forgets how to swim. Barbara grabs him, but hes too heavy for her, he pulls her under the water, but they are saved from drowning by some lifeguards. When they return home to Sweden, they attend Mozarts opera The Magic Flute. Suddenly Martin stands up and begins to sing along. Barbara gets him out of the theatre. At night, he gets out of bed, goes to the living room and smashes Barbaras precious violin. She hits him while she cries.

Martin basically needs help with everything now. His birthday is celebrated at a restaurant. Martin begins to urinate in a plant, and when Barbara tries to get him to stop, he becomes angry. Martin is and eventually he just lies quiet in bed and only reacts when he hears that Barbara opens a box of chocolates. The film ends with Martins concert with Barbara playing the violin.

==Cast==
 
* Sven Wollter as Martin Fischer
* Viveka Seldahl as Barbara Hartman
* Reine Brynolfsson as Biederman
*   as Karin
* Lisa Werlinder as Elisabeth
* Peter Engman as Philip
*   as Erik
*   as Susanne
*   as Dr. Gierlich
 

==Reception==
===Critical response===
In his review Roger Ebert gave the film three-and-a-half stars out of four, and described A Song for Martin as an honest film about Alzheimers disease that "starts at the beginning and goes straight through to the inevitable end, unblinkingly." 

The Rotten Tomatoes web site reports that the film received an aggregate 85% positive rating from critics based on 27 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 