Patriots (film)
{{Infobox film
| name           = Patriots
| image          =
| image_size     =
| caption        = Karl Ritter Karl Ritter (line producer) Karl Ritter (writer)
| narrator       =
| starring       = See below
| music          = Theo Mackeben
| cinematography = Günther Anders (cinematographer)|Günther Anders
| editing        = Gottfried Ritter
| distributor    =
| released       = 1937
| runtime        = 96 minutes
| country        = Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Karl Ritter.

== Plot summary ==
 

== Cast ==
*Mathias Wieman as Peter Thomann, called Pierre
*Lída Baarová as Thérèse, called Jou-Jou
*Bruno Hübner as Jules Martin, director of a front-line theatre
*Hilde Körber as Suzanne
*Paul Dahlke as Charles
*Nicolas Koline as Nikita
*Kurt Seifert as Alphonse
*Edwin Juergenssen as commandant
*Willi Rose as desk officer
*Ewald Wenck as police officer
*Otz Tollen as judge in wartime court
*Ernst Karchow as prosecutor
*André Saint-Germain as defending counsel
*Lutz Götz as German POW
*Paul Schwed as German POW
*Karl Hannemann as janitor
*Gustav Mahnke as sergeant

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 