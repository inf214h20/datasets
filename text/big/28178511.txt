Muddu Bidda
{{Infobox film
| name           = Muddu Bidda
| image          = Muddu Bidda.jpg
| image_size     =
| caption        =
| director       = K. B. Tilak
| producer       = K. B. Tilak
| writer         = Arudra Tapi Dharma Rao
| narrator       = Jamuna Kongara Jaggaiah Chittor V. Nagaiah G. Varalakshmi Lakshmirajyam C. S. R. Anjaneyulu Ramana Reddy Suryakantham
| music          = Pendyala Nageshwara Rao
| cinematography = 
| editing        = M. V. Rajan
| studio         =
| distributor    =
| released       = 1956
| runtime        =
| country        = India Telugu
| budget         =
}}
Muddu Bidda ( ) is a 1956 Telugu drama film directed and produced by K. B. Tilak. It is a debut film for Tilak under the banner Anupama Films. 

It is remade into the Hindi language as Chhoti Bahu in 1971, directed by K. B. Tilak himself.

==Cast== Jamuna - Radha, wife of Madhu		
* Kongara Jaggaiah	- Doctor Madhu, younger brother of Seshayya	
* Chittor V. Nagaiah - Seshayya
* G. Varalakshmi
* Lakshmirajyam - Seetha, wife of Seshayya
* C. S. R. Anjaneyulu - Zamindar
* Ramana Reddy
* Suryakantham

==References==
 

==External links==
*  

 
 
 
 

 