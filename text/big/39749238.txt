Uruvam
{{Infobox film
| name           = Uruvam
| image          = 
| image_size     =
| caption        = 
| director       = G. M. Kumar
| producer       = D. P. Singh Tarun Jalan
| writer         = G. M. Kumar R. P. Viswam  (dialogues) 
| starring       =  
| music          = Ilaiyaraaja
| cinematography = K. Rajpreeth Ilavarasan Dhayal
| editing        = A. P. Manivannan
| distributor    = Prathik Pictures
| studio         = Prathik Pictures
| released       =  
| runtime        = 90 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil horror Jaimala in lead roles. The film, produced by D. P. Singh and Tarun Jalan, had musical score by Ilaiyaraaja and was released on 15 March 1991.     

==Plot==

The illegitimate son of a rich man loses the battle of court over the palatial house which he has been living. So he appeals Bangaru Muni (Sathyajith) and he sets of a devastating black magic attack on the legitimate son Mohan (Mohan (actor)|Mohan) and his family. Mohan lives happily with his wife (Jaimala (actress)|Jaimala), his two children, his sister Raasi (Pallavi (entertainer)|Pallavi), his brother-in-law Ashok (Veera Pandiyan) and his wifes sister Meena (Roshini). They all move to the palatial house. Mohan is an atheist who doesnt believe in the supernatural. Soon, the family are disturbed by a supernatural force. Finally, Jolna Swamy (R. P. Viswam) comes to their rescue and fights against the evil who is in Mohans body.

==Cast==
 Mohan as Mohan Pallavi as Raasi
*R. P. Viswam as Jolna Swamy
*Veera Pandiyan as Ashok Jaimala as Mohans wife
*Sathyajith as Bangaru Muni
*Moorthy
*Roshini as Meena
*Master Krishnaprasad as Mohans son
*Baby Swarnalatha as Mohans daughter
*Crazy Venkatesh as Venkatesh
*Vaithyanathan
*R. T. Madurai Mani
*Sottai Mani
*Pandian

==Production==

===Development=== Pick Pocket Radha and Karthik and Pallavi (entertainer)|Pallavi, G. M. Kumar began work on his next film, also his first horror film.  The film was produced by Pallavis brother and another partner.  

===Casting=== Mohan accepted Pallavi signed Jaimala would play as Mohans wife. Three different cinematographers : K. Rajpreeth, Ilavarasan and Dhayal handle the camera, K. A. Balan was signed up as the art director, while A. P. Manivannan took up the post of the editor. 

===Filming===
The film made on low budget,   provided mainly the background music and composed only one song. The film was censored by the Indian Central Board of Film Certification, which gave the film an "A" certificate, an adult rating, due to its "mix of soft porn and hard horror". 

==Soundtrack==
{{Infobox album   Name        = Uruvam Type        = soundtrack Artist      = Ilaiyaraaja Cover       =  Released    = 1991 Recorded    = 1991 Genre  Feature film soundtrack Length      = 1:24 Producer    = Ilaiyaraaja
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1991, features 1 track with lyrics written by himself. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration 
|-  1 || "Puthiya Varusam" || Chorus || 1:24
|}

==Reception==

The film ultimately bombed at the box-office but the film has grown a strong cult film.It destroyed the career of Mohan, He should have not chosen this film at any cost. Mohan phenomenon came to an end with this film.

==References==
 

==External links==
* 

 
 
 
 
 
 