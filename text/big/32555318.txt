The Royal Bed
{{infobox film
| name           = The Royal Bed
| image          = TheRoyalBedPoster.jpg
| image size     = 
| caption        = Film Poster
| director       = Lowell Sherman  Harmon Weight (assistant)  Henry Hobart (assoc.) 
| writer         = J. Walter Ruben   
| based on       =   
| starring       = Lowell Sherman Mary Astor Anthony Bushell
| music          =                                               
| cinematography = Leo Tover 
| editing        = Arthur Roberts 
| distributor    = RKO Pictures
| released       =   }}
| runtime        = 72-73 minutes 
| country        = United States
| language       = English 
}}
The Royal Bed is a 1931 Pre-Code Hollywood|pre-Code American satirical comedy film produced by William LeBaron and distributed through RKO Pictures|RKO. The film was directed by and starred Lowell Sherman, along with Mary Astor and Anthony Bushell. The screenplay was adapted by  J. Walter Ruben based on the 1928 play by Robert E. Sherwood titled The Queens Husband. It would be one of a handful of RKO pictures which was produced in both English and French language versions.

==Plot==
 
 
Princess Anne (Mary Astor) plans to run away with Freddie Granton (Anthony Bushell), the commoner secretary of her father, King Eric VIII (Lowell Sherman), once her domineering mother, Queen Martha (Nance ONeil), has left for a vacation in America. Anne is therefore aghast when the Marquis of Birten (Alan Roscoe) brings news that he has negotiated her political marriage to Prince William of Grec (Hugh Trevor), a man she has never even met. Dismissing Annes vehement protests, the Queen is delighted, a feeling not shared by Annes loving but ineffectual father.

Meanwhile, the Premier and General Northrup (Robert Warwick) warn that a revolution is brewing. He wishes to execute large numbers of political prisoners, but cannot without the Kings signature. The Queen wholeheartedly approves of these stern measures. The King promises to attend to it, but after Northrup and the Queen leave, he orders his secretary to misplace the death warrants.
 Carrol Naish), the rebels rise up after Northrup gets Parliament to grant him dictatorial powers. Anne seizes the opportunity to try to flee with Granton, with her fathers approval. However, when she believes that the King is in real danger, she refuses to leave him.

Doctor Fellman (Frederick Burt), a moderate rebel leader, comes to see the King to demand his abdication, but agrees to stop the fighting in favor of negotiation. Then Northrup insists he is in charge now and laughs in derision when the King claims the people are stronger than Northrups army and navy. Next to arrive is Prince William. Despite his admission that he dislikes Anne, he is prepared to do his duty and go through with the wedding. Then Fellman and Laker show up. The King surprises Northrup by dismissing him from his service and putting Fellman in charge, ordering him to set up general elections as soon as possible.

The Queen, newly returned from America with a much-needed loan, tells her husband in private that she knew the whole revolution was a bluff to force Northrup from power. The King has one last deception planned (of which she is unaware). After she leaves for the wedding, he has Granton brought to him. He speedily marries Anne and Granton and sends them on their way to "exile" in France.

==Cast==
 
 
*Lowell Sherman as King Eric VIII
*Mary Astor as Princess Anne
*Anthony Bushell as Freddie Granton
*Hugh Trevor as Crown Prince William of Grec
*Nance ONeil as Queen Martha
*Robert Warwick as Premier and General Northrup
*Gilbert Emery as Phipps
*Alan Roscoe as Marquis of Birten
*Frederick Burt as Doctor Fellman Carrol Naish as Laker
*Desmond Roberts as Major Blent
*Nancy Lee Blaine as Lady in Waiting
*Lita Chevret as Lady in Waiting
 
(Cast list as per AFI database) 

==Production==
  Broadway play by Robert E. Sherwood (although The Film Daily incorrectly attributed the play to Noel Coward), and disclosed that Sherman would direct and star in the project.  William LeBaron, head of production for RKO Radio Pictures, had purchased the rights to Sherwoods play during his trip to New York in early September.  Later in the month, Emerys involvement in the film was announced in "an important role". 

In early October, Nance ONeils attachment in the role of the Queen was reported,  with Mary Astors involvement as the Princess announced the week later.  Robert Warwick was loaned from Fox, who pulled him out of the cast of Once a Sinner prior to production, to play the role of Northrup on October 15. 

In mid-October, it was reported that United Artists would handle the distribution of the French version of the film, in French-speaking countries. Using an all-French cast, that version was scheduled for release on November 30.  The complete cast for the French version, being shot at the RKO lot in Hollywood, was announced at the beginning of November. 

On October 21 the final cast was announced, as well as J. Walter Ruben as the writer in charge of the screenplay adaptation, and Arthur Roberts signed on as the editor.  The film was already in production by this time at RKOs studio in Hollywood. 

In November 1930 it trade paper accounts announced that the name of the film was being changed from The Queens Husband to The Royal Bed. 

==Reception==
Photoplay complimented the plot, dialogue and acting, calling it a "fine talkie,"   while The Modern Screen Magazine said it was "... an amusing story ...".  The Film Daily lauded the directing and the acting, stating that the film was an "Excellent satirical comedy", and said "The subtle but sure-fire humor of Lowell Sherman and the superb acting of Nance ONeil are the outstanding features of this fine production".  The magazine also complimented the French version of the play.  Motion Picture Magazine also liked the picture, calling it "Clever Satire - Cleverly Done", and they especially enjoyed Shermans acting. 

Jack Grant of Motion Picture News gave the film high praise, titling it "Delightful Sophistry".  He complimented the entire cast saying that it "... could hardly be improved upon", singling out the performances of Astor, Sherman, ONeil, Warwick, and Emery.  Grant also enjoyed Shermans direction.  Overall he described the picture "As simon-pure entertainment for intelligent audiences this talker has few equals in this class." 

==Notes== public domain in the United States due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

The Royal Bed was also produced by LeBaron in a French-language version entitled Echec Au Roi, directed by Léon DUsseau and Henri de la Falaise, with a translation of Rubens screenplay into the French language by Robert Harari.  Leo Tover was the cinematographer on this film as well, which starred Françoise Rosay, Pauline Garon and Emile Chautard.  At both the Los Angeles and Chicago screenings, the film was titled, Le Roi sEnnuie.     

As reported in The Hollywood Reporter, a radio version of this film aired on December 30, 1935. 
 Playhouse Theatre John Cromwell. Gyles Isham as Freddie Granton. 

==References==
 

==External links==
* 
* 
* 
*  available for free download at  

 
 

 
 
 
 
 
 
 
 
 
 
 