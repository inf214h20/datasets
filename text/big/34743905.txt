Salon Kitty (film)
{{Infobox film
| name = Salon Kitty
| image = Salon Kitty (film).jpg
| director = Tinto Brass
| producer = Ermanno Donati Giulio Sbarigia
| writer = Tinto Brass Ennio De Concini Maria Pia Fusco Antonio Colantuoni
| starring = Helmut Berger Ingrid Thulin
| music = Fiorenzo Carpi
| cinematography = Silvano Ippoliti
| editing = Tinto Brass
| distributor =
| released =  
| runtime =
| awards =
| country = Italy West Germany France
| language = Italian
| budget =
}} Salon Kitty wire tapped and all the prostitutes replaced with trained spies in order to gather data on various members of the Nazi party and foreign dignitaries.

It is considered among the progenitors of Nazisploitation genre.  

In the U.S., the film was edited to lighten the political overtones for an easier marketing as a sexploitation film and released under the title Madam Kitty with an "X" rating.  Blue Underground Video, for the uncut version, has surrendered the "X" rating for an unrated DVD and Blu-ray release.
  
== Cast ==
* Helmut Berger: Helmut Wallenberg
* Ingrid Thulin: Kitty Kellermann
* Teresa Ann Savoy: Margherita
* John Steiner: Biondo
* Bekim Fehmiu: Hans Reiter
* Stefano Satta Flores: Dino
* Sara Sperati: Helga, the dominatrix
* Maria Michi: Ilde
* Rosemarie Lindt: Susan John Ireland: Cliff
* Paola Senatore: Marika
* Tina Aumont: Herta Wallenberg
* Dan van Husen: Rauss
* Luciano Rossi: Dr. Schwab
* Giancarlo Badessi: German Officer with Projector
* Malisa Longo: New Kitty Girl
* Aldo Valletti: Dart Throwing Client
* Salvatore Baccaro: Neanderthal Prison Inmate (uncredited)

==Production==
Salon Kitty was filmed mostly at Dear Studios in Rome, with some additional location filming in Germany. Production designer Ken Adam had recently suffered a nervous breakdown while working on Barry Lyndon, and he described his participation in this film as a creatively regenerative one. He has stated that the production was an enjoyable one, and that he feels Salon Kitty is "underrated."  Adam based his design of Wallenbergs apartment on his own memories of his familys apartment in World War II-era Berlin. Wallenbergs enormous office, though a set, allegedly features a real marble floor, as it was cheaper to use real marble than create a mock-up version. 

Costumes and uniforms for the film were designed by Ugo Pericoli and Jost Jacob, and were constructed by Tirelli Costumi of Rome. Adam credited Jacob with the design of the kinky uniforms that Wallenberg wears throughout the film. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 