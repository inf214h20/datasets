Samna (film)
 
{{Infobox film
| name           = Samna
| image          = 
| caption        = 
| director       = Jabbar Patel
| producer       = Ramdas Phutane Madhav Ganbote
| writer         = Jabbar Patel Vijay Tendulkar
| starring       = Mohan Agashe Nilu Phule Shriram Lagoo Smita Patil Vilas Rakate
| music          = 
| cinematography = Suryakant Lavande
| editing        = N.S. Ved
| distributor    = 
| released       =  
| runtime        = 151 minutes
| country        = India Marathi
| budget         = 
}} Marathi language crime drama directed by Jabbar Patel. It was entered into the 25th Berlin International Film Festival.   

==Cast==
* Mohan Agashe as Maruti Kamble
* Anant Audkar
* Shivaji Bhosle
* Sanjivani Bidkar as Mrs. Patil
* Rajni Chauhan
* Jayant Dharmadhikari
* Aswel Guruji
* Bhalchandra Kulkarni
* Shreeram Lagoo as Master
* Uday Lagoo
* Gulab Latkar
* Salim Latkar
* Usha Naik as Suhini
* Nandu Paranjpe
* Smita Patil as Kamley
* Nilu Phule as Hindurao Dhonde Patil
* Nandu Poley
* Vilas Rakte as Sarjerao
* Shriram Ranade
* Bhagwan Rao
* Asha Rukdikar
* Lalan Sarang
* Surekha Shah
* Chandrakant Shinde
* Hem Suvarna
* Ramesh Tilekar
* Shahji Varey

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Category !! Cast/Crew member !! Status
|-
| colspan="4" |
*25th Berlin International Film Festival
|-
| 1975 || Golden Berlin Bear || Jabbar Patel ||  
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 