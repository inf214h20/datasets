Noel (film)
 
{{Infobox film name       = Noel image      = Noel poster.JPG caption    = Noel film poster director   = Chazz Palminteri writer     = David Hubbard starring  Marcus Thomas and Robin Williams producer   = Al Corley Howard Rosenman music      = Alan Menken
|distributor= Convex Group, Redbus Film Distribution (UK) budget     = country    = United States released   =   runtime    = 96 minutes language   = English gross      = $2,280,924 
}}

Noel is a 2004 Christmas-themed drama film written by David Hubbard and directed by Chazz Palminteri. It stars Susan Sarandon, Penélope Cruz, Paul Walker, Alan Arkin, Daniel Sunjata and an uncredited Robin Williams. It was filmed in Montreal, Canada.

==Plot summary==
The film centers on five strangers who are linked together&nbsp;– and who meet each other at separate times&nbsp;– by a series of events that take place on Christmas Eve in New York.
 Marcus Thomas) is a young man who deliberately damages his hand so he can attend a Christmas party in the emergency room, as that was the only happy memory of his childhood. In addition to the five main characters, the mysterious Charlie (Robin Williams) is introduced as the person who may be able to help Rose finally realize that she must look after herself more, rather than worrying about everyone else.

==Main cast==
*Susan Sarandon&nbsp;– Rose Collins
*Penélope Cruz&nbsp;– Nina Vasquez
*Paul Walker&nbsp;– Michael (Mike) Riley
*Alan Arkin&nbsp;– Artie Venizelos Marcus Thomas&nbsp;– Jules
*Chazz Palminteri&nbsp;– Arizona
*Robin Williams&nbsp;– Charles (Charlie) Boyd
*Sonny Marinelli&nbsp;– Dennis
*Daniel Sunjata&nbsp;– Marco
*Rob Daly&nbsp;– Paul
*John Doman&nbsp;– Dr. Baron Billy Porter&nbsp;– Randy
*Carmen Ejogo&nbsp;– Dr. Batiste
*Donna Hanover&nbsp;– Debbie Carmichael
*Merwin Mondesir&nbsp;– Glenn

==See also==
*List of Christmas films

==External links==
* 
* 

==Reference list==
 

 
 
 
 
 
 
 
 .
 
 