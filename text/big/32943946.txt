The Bells (1911 film)
 
 
{{Infobox film
  | name     = The Bells
  | image    = 
  | caption  = 
  | director = W. J. Lincoln		 William Gibson Millard Johnson John Tait Nevin Tait
  | writer   = W. J. Lincoln The Bells by Erckmann-Chatrian adapted by Leopold Lewis and W. J. Lincoln
  | starring = Arthur Styan Nellie Bramley
  | music    = 
  | cinematography = Orrie Perry
  | editing  = 
  | distributor = 
  | studio = Amalgamated Pictures
  | released = 7 October 1911 (Melbourne) 
  | runtime  = 4,000 feet
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 stage melodrama by Erckmann-Chatrian adapted by Leopold Lewis, which had been adapted for the Australian stage by Lincoln.  

It is considered a lost film.

==Plot==
Mathias (Arthur Styan) is an innkeeper in a village in Alsace, happily married to Catherine (Miss Grist) and with a daughter Annette (Nellie Bramley). However he is greatly in debt so on Christmas Day 1833 murders a Polish Jew (Mr Cullenane) who visits the inn for his gold. He uses this to pay off his debts and rise in society, becoming the burgomeister of the town – however he is always tormented by guilt.

Fifteen years later on Christmas Day, Mathias becomes delirious and hears the sound of the Jews sleigh bells. He dreams he is being tried for the murder and is found guilty. He awakes and dies, leaving his family none the wiser.

==Cast==
*Arthur Styan as Mathias
*Nellie Bramley as Annette
*Miss Grist as Catherine
*J Ennis as Walter
*Ward Lyons as Hans
*Charles Lawrence as Christian
*Mr Johns as mesmerist
*Mr Ebbsmith as Dr Zimmer
*George Kensington as notary
*Mr Devon as Tony
*Mr Devine as Fritz
*Mr Cullenane as the Polish Jew
*Mr Coleridge as judge
*Mr Sinclair as clerk
*Marion Willis as Sozel

==Production==
The film was an adaptation of a well known play and featured the only known screen appearance of stage actor Nellie Bramley.  It was shot partly on location of Mount Donna Buang in Victoria. 

==Release==
Screenings of the film were often accompanied by a lectured from J Ennis, who was in the film.

==References==
 

==External links==
*  
*  at AustLit
*   at National Film and Sound Archive
* 

 

 
 
 
 
 


 