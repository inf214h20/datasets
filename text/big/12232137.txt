Aaj Aur Kal (1963 film)
{{Infobox film
| name           = Aaj Aur Kal
| image          = Aajaurkal.jpg
| image_size     =
| caption        = DVD Cover
| director       = Vasant Joglekar
| producer       = Vasant Joglekar   Panchadeep Chitra
| writer         = P L Deshpande   Akhtar-Ul-Iman (dialogues)
| narrator       = 
| starring       = Sunil Dutt Raaj Kumar Nanda (actress)| Nanda
| music          = Ravi (music director)| Ravi
| cinematography = Bal M. Jogalekar
| editing        = Chintamani Borkar
| distributor    = Shemaroo Video Pvt. Ltd. (2004) DVD 
| released       = 1963
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
Aaj Aur Kal is a 1963 Hindi movie produced and directed by Vasant Joglekar. The film stars Sunil Dutt, Nanda (actress)| Nanda, Raaj Kumar, Tanuja, Ashok Kumar and Deven Verma. 

==Plot==
Its the story of a fastidious king Balbir Singh (Ashok Kumar) of Himmatpur whose stern, high handed behaviour leads to a breakdown in communication with his four children; eldest daughter Hemalata Nanda (actress)| (Nanda), younger daughter Ashalata (Tanuja) and sons Pratap (Rohit Kumar) and Rajendra (Deven Verma). His intimidating commands lead to a partial paralysis of Hemlatas lower limbs as also a simmering discontent amongst other descendants. After numerous attempts fail to cure Hemalata, the king hires a new doctor Sanjay (Sunil Dutt) who, contrary to expectations, is not just young and handsome but also against silly protocols that hamper laughter, fun and frolic. Dr. Sanjays experimentations provide greater mobility and joy to youngsters, giving them fresh lease of life and a much needed voice of rebellion. Transcending normal barriers of doctor-client confidentiality, Sanjay and Hemalata fall in love and the romance enables her to walk in double quick time on her feet!
Initially reluctant, the king awakens to a new dawn of freedom and humbly accepts not just their relationship but also Ashalatas betrothal to social activist (Soodesh Kumar), who defeats him in a general election.

== Cast ==
*Sunil Dutt as Dr. Sanjay
*Nanda (actress)| Nanda as Hemalata
*Raaj Kumar 
*Tanuja as Ashalata
*Ashok Kumar as Balbir Singh Agha  Dhumal
*Deven Verma as Rajendra

==Music==
{{Track listing
| headig = Songs
| extra_column = Singer(s)
| all_lyrics = Sahir Ludhianvi Ravi
| title1 = Mujhe Gale Se Laga Lo | extra1 = Mohammad Rafi and Asha Bhosle | length1 = 3:14
| title2 = Raja Saheb Ghar Nahin | extra2 = - | length2 = 3:23
| title3 = Maut Kitni Bhi Sangdil Ho | extra3 = - | length3 = 3:24
| title4 = Taqat Na Hoga Taj Na Hoga | extra4 = - | length4 = 3:15
| title5 = Yeh Wadiyan Yeh Fazayen | extra5 = Mohammad Rafi | length5 = 3:13
| title6 = Mujhe Chhedo Na Kanha | extra6 = - | length6 = 3:25
| title7 = Itni haseen itni jawaan raat kya karen | extra7 = Mohammad Rafi | length7 = 
| title8 = Zindagi ke rang kayi re | extra8 = Asha Bhosale | length8 = 
}}

==External links==
*  

 
 
 