Tortoise Wins by a Hare
 

{{Infobox Hollywood cartoon|
| cartoon_name = Tortoise Wins By a Hare
| series = Merrie Melodies/Bugs Bunny
| image = Tortoise Wins by a Hare title card.png
| caption = 
| director = Robert Clampett
| story_artist = Warren Foster
| animator = Bob McKimson   Rod Scribner
| voice_actor = Mel Blanc
| musician = Carl W. Stalling
| producer = Leon Schlesinger
| distributor = Warner Bros. Pictures   The Vitaphone Corporation
| release_date =   (USA)
| color_process = Technicolor
| runtime = 7 minutes 44 seconds
| movie_language = English
}}
 Chicago Sunday Tribunk) shown in this cartoon accurately predicts Adolf Hitlers suicide two years later. 

==Plot== Bill Thompsons "Old Timer" character from Fibber McGee and Molly) to ask the turtle his secret. Cecil, not in the least bit fooled by the disguise, so tells him he remarks that his streamlined shell lets him win, and produces a set of blueprints for his "air-flow chassis". He also adds that in contrast, the long ears of a rabbit only serve as "wind resistors", which in turn would slow the rabbit down. The turtle ends the conversation with the comment, "Oh, and another thing... Rabbits arent very bright, either!" just before slamming the door in the enraged bunnys face. Not getting the hint that the turtles story is a humbug, Bugs builds the device and prepares for the race.

 
Meanwhile, the bunny mob learns of the upcoming match-up and places all its bets on Bugs. ("In fact, we dont even tink dat de toitle will finish... Do we, boys?" "Duh, no, Boss, no!") The race begins, and Bugs easily outpaces his reptilian rival. However, in his new get-up, the dim-witted gangsters mistake him for the turtle. Cecil reinforces this misconception by dressing in a gray rabbit suit and munching on a carrot. The mobsters thus make the shelled Bugs run a nightmare, ultimately giving the race to Cecil (in an aside to the audience, as the rabbits cheer him, Cecil remarks, "I told you rabbits arent very bright!") When Bugs removes the chassis and sobbingly reveals that hes the rabbit, the rabbit gangsters remark, in mock-Bugsy style, "Ehhh, now he tells us!" and commit suicide by shooting themselves with a single bullet that goes through the sides of all of their apparently soft heads (for more information, see "Censorship" below).

==Analysis==
This animated short contains wartime references. Bugs displays "A" and "C" ration cards. He claims he has a secret weapon. A Japanese cruiser is mentioned in a newspaper headline. A chorus of turtles sing "He did it before and he can do it again". Shull, Wilt (2004), p. 159 

Nichola Dobson mentions the short as an example of both Bob Clampetts attention to detail and of the fast pace of his work. Dobson (2010), p. 44 

==Censorship== Turner channels (i.e. Cartoon Network  , TBS (TV Channel)|TBS, and Turner Network Television|TNT) and the former The WB|WB! Network. This is because of suicide. The edited version ends with an abrupt fake blackout immediately after the gambling ring members say "Eh, NOW he tells us!".
*The TBS version (in addition to editing the suicide ending) also shortens the part where the rabbits pounce on Bugs before he can reach the finish line. 
*On Turner Classic Moviess Cartoon Alley, the suicide ending is intact, but the cartoon ends with a fade out instead of an iris-out.
*The short is aired uncensored (as recently as 2015) on the Canadian cable channel Teletoon Retro.

==Availability==
* This short can be found (uncut and uncensored) on the  .

==Sources==
*  
*  

==See also==
* Rabbit Transit (film)

==References==
 

== External links ==
* 
*  at the Big Cartoon Database

 
{{succession box 
| before= Case of the Missing Hare  Bugs Bunny Cartoons 
| years=1943 
| after= Super-Rabbit
}}
 

 
 
 
 
 
 
 
 
 