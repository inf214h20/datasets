The Three Must-Get-Theres
{{Infobox film
| name           = The Three Must-Get-Theres
| image          = The Three Must get Theres.jpg
| image_size     =
| caption        =
| director       = Max Linder
| producer       = Max Linder
| writer         = Max Linder
| narrator       =
| starring       = See below
| music          = 
| cinematography = Max Dupont Enrique Juan Vallejo	
| editing        =
| distributor    = United Artists
| released       = August 27, 1922
| runtime        = 55 min
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 
The Three Must-Get-Theres is a 1922 American silent film directed by Max Linder.

A young man from Gascony travels to Paris hoping to become one of the Kings musketeers.
 The Three Musketeers, starring Douglas Fairbanks.

This is Linders final American film before returning to France.

== Cast ==
*Max Linder as Dart-In-Again
*Bull Montana as Lil Cardinal Richie-Loo
*Frank Cooke as King Louis XIII
*Jobyna Ralston as Constance Bonne-aux-Fieux

== External links ==
 
*  
*  
* http://www.europafilmtreasures.eu/fiche_technique.htm?ID=312 The full film.

 
 

 
 
 
 
 
 
 
 
 


 