Every Night at Eight
{{Infobox film
| name           = Every Night at Eight
| image          = 
| image_size     = 
| caption        = 
| director       = Raoul Walsh
| producer       = Walter Wanger
| writer         = C. Graham Barker Stanley Garvey (story)
| narrator       = 
| starring       = George Raft Alice Faye Frances Langford
| music          = Frederick Hollander Paul Mertz Clifford Vaughan
| cinematography = 
| editing        = W. Donn Hayes
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $266,956 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p435 
| gross          = $507,117 
| preceded_by    = 
| followed_by    = 
}}
Every Night at Eight is a 1935 American comedy musical film starring George Raft and Alice Faye and made by Walter Wanger Productions Inc. and Paramount Pictures.  It was directed by Raoul Walsh and produced by Walter Wanger from a screenplay by C. Graham Baker, Bert Hanlon and Gene Towne based on the  story Three On a Mike by Stanley Garvey. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 62-63 

The film involves a trio of young female singers trying to break into show business.

The song "Im in the Mood for Love" was introduced in this film by Frances Langford.

==Cast==
* George Raft as Tops Cardona
* Alice Faye as Dixie Foley
* Frances Langford as Susan Moore
* Patsy Kelly as Daphne OConnor
* Walter Catlett as the Master of Ceremonies (M.C.)
* Harry Barris as Harry

==Reception==
The film made a profit of $148,782. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 


 