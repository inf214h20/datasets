Hellbound?
  
{{Infobox film
| name           = Hellbound?
| image          = 
| alt            = 
| caption        = 
| director       = Kevin Miller
| producer       = Kevin Miller David Rempel
| writer         = Kevin Miller
| starring       = 
| music          = Marcus Zuhr
| cinematography = 
| editing        = Simon Tondeur
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Hellbound? is a 2012 Canadian documentary film which details the current debate regarding various views about the existence and nature of hell.

==Premise==
The film features interviews of theologians and commentators who discuss various views whether Hell exists and if so, who would go there after death.

Interview subjects include:

{{columns-list|4|
* Glen Benton Mike Bickle Gregory A. Boyd
* Ray Comfort
* Ron Dart Mark Driscoll
* Hank Hanegraaff
* Peter Kreeft
* Bob Larson
* Robert McKee
* Brian McLaren
* Necrobutcher
* Robin Parry Jonathan and Margie Phelps
*  
* Frank Schaeffer
* Oderus Urungus
* David Vincent
* William P. Young
}}

==Production==
Producer Kevin Miller began active work on Hellbound? in January 2011. During the production work, discussion and controversy over the subject of Hell was raised by the release of Rob Bells book, Love Wins.   

==Release==
The first public screening of Hellbound was on 12 September 2012 in Nashville, Tennessee, followed by showings in other North American cities.    The film will release on DVD and VOD on 28 May 2013.

==Reception==
 , the review aggregator website Rotten Tomatoes indicated a 67% approval rating from critics. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 
 