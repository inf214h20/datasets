Shapath
{{Infobox film
 | name = Shapath
 | image = Shapatmithun.jpg
 | caption = DVD Cover
 | director = Rajiv Babbar
 | producer = Rajiv Babbar
 | writer = 
 | dialogue =  Harish  Ramya Krishna  Kareena Grover  Vineeta Salim Ghouse
 | music = Anand-Milind
 | lyrics =
 | associate director = 
 | art director = 
 | choreographer = 
 | released =   
 | runtime = 140 min.
 | country = India
 | language = Hindi
 | budget =  3 Crores
 | preceded_by = 
 | followed_by = 
}} 1997 Hindi Indian feature directed by Rajiv Babbar, starring Mithun Chakraborty, Jackie Shroff, Harish Kumar|Harish, Ramya Krishna, Kareena Grover, Vineeta and Salim Ghouse

==Plot==

Shapath  is an Action flick from Rajiv Babbar, who has produced several successful films with Mithun. The film had Jackie Shroff in an important role. The film also has Singer Altaf Raja in Special appearance.

==Cast==
* Mithun Chakraborty  as  Commando Arjun / Surya
* Jackie Shroff  as  Inspector Kishan
* Salim Ghouse  as  Lankeshwar
* Ramya Krishna  as  Kavita D. Kallu
* Harish Kumar  as  Rahul Rami Reddy  as  Mantri Dindayal Kallu
* Gulshan Grover  as  Rajeshwar
* Shakti Kapoor  as  Havaldar Shakti Singh
* Kader Khan  as  Chaurasia
* Achyut Potdar  as  Satyajeet Satya Babu
* Altaf Raja  as  Himself
* Ranjeet  as  Dr. Subramaniam Swami
* Deepak Shirke  as  Assistant Commissioner of Police
* Guddi Maruti  as  Sharmili S. Singh
* Kareena Grover  as  Neena / Shalu
* Tina Ghai  as  Chameli
* Amrit Patel
* Manmauji
* Vineeta
* Yunus Parvez

==References==
* http://www.imdb.com/title/tt0251395/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Shapath+%281997%29

==External links==
*  

 
 
 
 
 
 


 