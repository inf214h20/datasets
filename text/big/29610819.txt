Dei svarte hestane
{{Infobox film
| name           = Dei svarte hestane
| image          = 
| image size     =
| caption        = 
| director       = Hans Jacob Nilsen   Sigval Maartmann-Moe
| producer       = 
| writer         = Tarjei Vesaas (novel)   Eiliv Odde Hauge   Kåre Bergstrøm
| narrator       =
| starring       = Hans Jacob Nilsen   Eva Sletto
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 24 September 1951
| runtime        = 85 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Hans Jacob Nilsen and Sigval Maartmann-Moe, based on a book by Tarjei Vesaas, and starring Hans Jacob Nilsen, Eva Sletto and Ottar Vicklund.
 coaching house. On his farm he has four fit and shiny black horses. These horses eventually become the centre of his life, because his young and beautiful wife, Lisle (Sletto), is unable to love him like he loves her. She can not forget Bjørneskinn (Vicklund), the blonde boy she was once in love with. He wanted to be a poet, but decided on a life on the road when he could not have her.

When the old boyfriend one day returns, it leads to a dramatic showdown. The family is about to fall apart, which will be particularly hard for Kjell, Ambros and Lisles young son. He end up in a conflict of loyalty between his parents, and has great difficulties dealing with the situation.

==External links==
*  
*  

 
 
 
 