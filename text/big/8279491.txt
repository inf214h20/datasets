The Purple Monster Strikes
 
{{Infobox film
| name           = The Purple Monster Strikes
| image          = The Purple Monster Strikes.jpg
| director       = Spencer Gordon Bennet Fred C. Brannon
| producer       = Ronald Davidson
| writer         = Royal Cole Albert DeMond Basil Dickey Lynn Perkins Joseph Poland Barney Sarecky Dennis Moore James Craven John Davidson
| cinematography = Bud Thackery
| distributor    = Republic Pictures
| released       =   |1957|03|25|U.S. re-release|ref2= |1966| | |U.S. TV|ref3= }}
| runtime         = 15 chapters (209 minutes (serial)  100 minutes (TV) 
| country         = United States English
| budget          = $160,057 (negative cost: $183,803) 
| awards          =
}} Republic Movie serial.  It was also released as a Century 66 television film under the title D-Day on Mars (1966).

The original production title for the serial was The Purple Shadow Strikes. The sequel to this serial was the 1950 Flying Disc Man from Mars, which used much of the footage from the original. 

==Plot==
Astronomer Cyrus Layton is working late one night on his new airplane design in his observatory. He witnesses what he believes is a meteorite landing in the far distance. He contacts his niece Sheila and asks her to bring Craig Foster to the observatory to help analyze his discovery; he then sets out to search for the meteorite crater. Layton instead discovers a crashed spaceship; the ships pilot emerges and explains that he is from the planet Mars.

Mistakenly thinking the alien is friendly, Layton takes him back to the observatory. Once there the Martian, calling himself "The Purple Monster," wishes to see Laytons designs for the new airplane/spaceship. He proudly shows the alien his designs until the alien explains that he is now stealing them, to build a spaceship for himself to fly back to Mars, where a fleet of the ships will then be used invade the Earth. When Dr. Layton objects, the Martian murders him with a weapon that emits a "carbo-oxide" gas, which kills instantly. The alien then transforms into a ghost and takes over Dr Laytons body. Doing so fools the astronomers niece Sheila and criminologist Craig Foster, both of whom work with Dr. Laytons foundation, which is responsible for commissioning the spaceship project.

Inhabiting Dr. Layton allows the Martian to witness the unrelated theft of the plans by a gangster named Garrett. The Martian convinces Garrett and his gang to aid in the invasion plot. With the criminals help the alien begins building the spaceship. Eventually, however, the Martians efforts at pretending to be Dr Layton fall apart, and Foster and Sheila realize what is happening. A series of action scenes show the pair trying to figure out and stop whatever the alien is doing on Earth. Craig and Sheila constantly battle the Purple Monsters henchmen, who use mind-control poisons, carjackings, and even a booby-trapped vacant lot to dispose of Craig and Sheila.

The closest the criminals come to succeeding is in Chapter 7 ("The Evil Eye"), when Sheila is lured into a trap at the gangs hideout. Foster gets the information out of a captured gang member and speeds to the house to save Sheila, who has been tied up and gagged inside a room filled with explosives set to detonate after an electric eye is tripped.

At the end of Chapter 7, Foster steps into the electric eye, triggering the explosives and detonating the building. However, at the beginning of Chapter 8, Shelia manages to remove her gag and alert Foster about the eye, allowing him to jump over it. Once safely out of the building, Foster shoots a henchman, causing him to fall into the electric eye, triggering the bomb.

In the last chapter Craig and Sheila realize that the Purple Monster is using Professor Laytons body; they devise a plan to uncover the truth. While Sheila gets the supposed Doctor Layton to come downtown to sign some papers needed for funding, Craig slips into Laytons office and secretly installs a movie camera which will be remotely activated when the telephone is used. Foster then escapes and calls the office to advise him that he will be bringing reinforcements to search the observatory, which he has discovered is the Purple Monsters hideout. Craig and Sheila arrive to find the observatory deserted. Sheila goes to the basement where she stumbles upon Purple Monsters subterranean lair and is kidnapped. Foster goes to check on Sheila and finds the basement empty. He then discovers the secret lair where Sheila has been bound and gagged. The Purple Monster orders his henchmen to dispose of her and destroy the observatory once he escapes.

The story ends with Craig Foster using a part of the spaceship, a sonic pulse cannon used to shatter meteors. He destroys the alien spaceship with the Purple Monster inside as he attempts to fly back to Mars to lead an invasion fleet against Earth.

==Cast== Dennis Moore as Craig Foster. The hero was known as Carry Foster even as far into production as the final shooting script. The name was pencilled out for filming.   
* Linda Stirling as Sheila Layton
* Roy Barcroft as The Purple Monster. The villain was originally called "The Purple Shadow", which was also pencilled out for filming.  James Craven as Dr Cyrus Layton
* Bud Geary as Hodge Garrett, Henchman
* Mary Moore as Marcia John Davidson as Emperor of Mars
* Joe Whitehead as Carl Stewart
* Anthony Warde as Tony

==Production==
The Purple Monster Strikes was budgeted at $160,057 although the final negative cost was $183,803 (a $23,746, or 14.8%, overspend).  It was the most expensive Republic serial of 1945.   It was filmed between 17 April and 19 May 1945 under the working title The Purple Shadow Strikes.   The serials production number was 416.   This was the first post-war science fiction serial.   

Roy Barcroft lived close (about a mile) to Republic Studios and jogged to work to keep fit for this job.  When told that the costume included tights he decided to lose weight and lost about 30&nbsp;lb in 30 days.  He called the serial "The Jerk in Tights from Boyle Heights". 

The Purple Monster costume was re-used in   (the agent played by Stanley Waxman).  Stock footage from The Purple Monster Strikes was also re-used in these serials.  The rocket crash footage was re-used for Flying Disc Man from Mars.  

This was the last Republic serial with 15-chapters.  The remaining serials were either 12- or 13-chapters in length. 

===Special effects===
The special effects were created by the Lydecker brothers.  A problem occurred with the effects sequence of the rocketship crash in the first chapter. In the first attempt, the rocket struck an underground water pipe causing a geyser and forcing a retake. 
 Flash Gordon had the copyright on the word "rocketship" for use in serials and their featurizations. 

===Stunts===
* Dale Van Sickel as Craig Foster (doubling Dennis Moore)
* Babe DeFreest as Sheila Layton (doubling Linda Stirling) Fred Graham as The Purple Monster (doubling Roy Barcroft) Tom Steele as Hodge Garrett (doubling Bud Geary)
* Polly Burson as Marcia (doubling Mary Moore)
* John Daheim
* Bud Geary
* Carey Loftin Cliff Lyons Henry Wills

==Release==

===Theatrical===
The Purple Monster Strikes official release date is 6 October 1945, although this is actually the date Chapter 7 was made available to film exchanges. 

The serial was re-released on 25 March 1957 between the similar re-releases of Dangers of the Canadian Mounted and Zorros Black Whip. The last original Republic serial release was King of the Carnival in 1955. 

===Television===
The Purple Monster Strikes was one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to D-Day on Mars.  This version was cut down to 100-minutes in length. 

==Chapter titles==
# The Man in the Meteor (22min 20s)
# The Time Trap (13min 20s)
# Flaming Avalanche (13min 20s)
# The Lethal Pit (13min 20s)
# Death on the Beam (13min 20s)
# The Demon Killer (13min 20s)
# The Evil Eye (13min 20s)
# Descending Doom (13min 20s)
# The Living Dead (13min 20s) recap chapter
# Menace from Mars (13min 20s)
# Perilous Plunge (13min 20s)
# Fiery Shroud (13min 20s)
# The Fatal Trial (13min 20s)
# Take-off to Destruction (13min 20s)
 Source:   

==See also==
* List of film serials
* List of film serials by studio

==References==
 
* Republic Pictures Home video VHS tape, The Purple Monster Strikes
* Science Fiction: The Illustrated Encyclopedia by John Clute; DK Publications copyright 1995; pg 259
* The Encyclopedia of Super Villains by Jeff Rovin; Facts on file copyright 1987; pg 282

==External links==
*  

 
{{succession box Republic Serial Serial
| before=Federal Operator 99 (1945)
| years=The Purple Monster Strikes (1945) The Phantom Rider (1946)}}
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 