Green Lantern: Emerald Knights
{{Infobox film
| name           = Green Lantern: Emerald Knights
| image          = GreenLantern-Emerald-Knights.jpg
| alt            =  
| caption        = Cover of the U.S. Blu-ray
| director       = {{Plainlist|
* Christopher Berkeley
* Lauren Montgomery
* Jay Oliva }}
| producer       = Bruce Timm
| writer         = {{Plainlist|
* Eddie Berganza
* Alan Burnett
* Todd Casey
* Dave Gibbons Michael Green
* Marc Guggenheim
* Geoff Johns
* Peter Tomasi }}
| starring       = {{Plainlist|
* Nathan Fillion
* Elisabeth Moss
* Jason Isaacs
* Kelly Hu
* Roddy Piper
* Arnold Vosloo }}
| music          = Christopher Drake
| cinematography = 
| editing        = 
| studio         = {{Plainlist|
* Warner Bros. Animation
* Warner Premiere
* DC Comics }}
| distributor    = Warner Home Video
| released       =  
| runtime        = 84 minutes
| country        =  
| language       = English
| budget         = 
| gross          = $4,890,485 
}}
Green Lantern: Emerald Knights   is a  , the film uses the same character designs and includes a cameo by Chp, who had a speaking role in the previous film.

It is the eleventh film released under the   to feature an  .

Two of the stories ("Mogo Doesnt Socialize" and "Abin Sur") were based on comic stories written by Alan Moore, who has a standing policy of not allowing his name to be used in the credits when his stories are adapted to other media. Although it uses the same name, it has no relation with the 1998 comic book Emerald Knights. 

==Plot==
The sun of the Green Lantern homeworld, Oa, is becoming a gateway for Krona (comics)|Krona, an evil anti-matter alien tyrant that once sought to destroy all life. As precaution the Guardians of the Universe decide to evacuate Oa of all valuables, such as the Central Battery. While in line to charge their rings before the Battery is taken away, Green Lantern Corps rookie Arisia Rrab converses with Hal Jordan and expresses her self doubts as the newest Green Lantern. In response, Hal tells her the story of the first Green Lantern:

===The First Lantern===
Avra, a  , who is then succeeded by Hal Jordan.
 boot camp. Hal tells Arisia not to fear Kilowog, and recounts the story of Kilowogs own trainer.

===Kilowog=== authority to Kilowog, who completes the mission.

Hal and Arisia arrive at a border patrol of the sun and await Kronas return. Arisia and other Lanterns hear a Delphic prophecy from a Lantern named Laira who is levitating in the Lotus position (about which she remarks "That was strange."), whereupon  Hal shares List of Green Lanterns#Laira|Lairas own story:

===Laira=== ritual suicide to maintain his honor.

Back at the border patrol of Oas sun, every Lantern extant has been called to await Kronas return, with the notable exception of Mogo. Hal explains who Mogo is and why he is not present:

===Mogo Doesnt Socialize===
Bolphunga the Unrelenting seeks to fight and destroy all the most powerful warriors in the universe. He is told, however, by his latest opponent (a volcanic being whom he dismembers) that he will never defeat the Green Lantern Mogo. Bolphunga’s computer contains no data on Mogo save for his whereabouts on a mysterious green planet. Bolphunga spends weeks there tracking Mogos plethora of power signatures, but never finds the elusive Lantern. He then sets explosives all over the planet in order to flush Mogo out, but is horrified when the planet extinguishes all the bombs and Mogo is revealed to be the entire planet itself. He attempts to escape, but Mogo easily captures him.

At Oas sun Hal and Arisia are attacked by Kronas Shadow Demons and rescued by Sinestro. Sinestro then speaks of the prophecy that Oa will be destroyed and relates a story of Abin Sur and the Lantern view on destiny:

===Abin Sur=== own lantern corps built on the power of fear. Abin Sur however refuses to believe his friend would betray his Green Lantern duties.
 Mogo the Living Planet arrives and uses its own mass and Lantern Power to assist his comrades. Oa and Krona are forced into the sun and both are annihilated. The prophecy of Oas destruction is fulfilled, but Krona is destroyed and the Corps is saved.

Mogo volunteers to be the Corps temporary base as they build a new Oa. Arisia is honored with an official entry into the Book of Oa for her heroic ingenuity, although she still has to report for Kilowogs training.

==Cast==
* Nathan Fillion as Hal Jordan / Green Lantern
* Elisabeth Moss as Arisia Rrab 
* Jason Isaacs as Sinestro Laira
* Roddy Piper as Bolphunga
* Arnold Vosloo as Abin Sur
* Tony Amendola as Kentor, Appa Ali Apsa (uncredited)
* Steven Blum as Kloba Vud, Palaqua (uncredited), Ranakar (uncredited), GHu (uncredited), Additional Voices
* Grey DeLisle as ReeYu, Ardakian Trawl (uncredited), Boodikka (uncredited) Michael Jackson as Ganthet
* Peter Jessop as Salaak David Kaufman as Rubyn
* Sunil Malhotra as Bolphungas Ship
* Henry Rollins as Kilowog 
* Andrea Romano as Abin Surs Ring, Deegans Ring (uncredited)
* Jane Singer as Wachet
* James Arnold Taylor as Tomar-Re Bruce Thomas as Atrocitus
* Bruce Timm as Galius Zed (uncredited)
* Mitchell Whitfield as Avra
* Wade Williams as Deegan
* Gwendoline Yeo as Blu

==Related media== Young Justice, John Stewart.

==Reception==
The movie has received a mostly mixed to positive reception. Joey Esposito of IGN praised the films visuals, voice acting, and action sequences, but criticized its thin plot and characterization. Esposito felt that it was difficult to connect emotionally with the characters, and asserted that Emerald Knights was best watched by established fans of the Green Lantern comics series.  In a review for DVD Talk, Jamie Rich also praised the animation and action sequences, while panning the films storyline. Rich also recommended the film to "true Lantern fans" in particular.  On the other hand, Alan Kistler of Newsarama called it "a solid feature   should be enjoyed by any Green Lantern fan, as well as anyone who knows nothing about the comic and wants to learn."  Joseph Szadkowski of The Washington Times praised the design decisions made in the making of the movie and, specifically, called the fight between Laira and her father one of the best animated hand-to-hand combat sequences that he had ever seen. 

==References==
 

==External links==
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 