All Roads Lead to Calvary (film)
{{Infobox film
| name           = All Roads Lead to Calvary 
| image          =
| caption        =
| director       = Kenelm Foss
| producer       = H.W. Thompson
| writer         = Jerome K. Jerome  (novel)   Kenelm Foss 
| starring       = Minna Grey   Bertram Burleigh   Mary Odette  Jack Parker
| editing        = 
| studio         = Astra Films 
| distributor    = Astra Films
| released       = August 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama novel of the same name by Jerome K. Jerome.  A fisherman becomes a Member of Parliament, but is torn between his career, mistress and wife.

==Cast==
*  Minna Grey as Nan Phillips  
* Bertram Burleigh as Bob Phillips  
* Mary Odette as Joan Allway  
* Roy Travers as Preacher  
* Julie Kean as Hilda Phillips  
* J. Nelson Ramsay as Mr. Alway   David Hallett as Arthur Allway   George Travers as Editor  
* Lorna Rathbone as Editors Wife  
* Kate Gurney as Landlady

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
  
 

 