Bad Company (1999 film)
 
{{Infobox film
| name           = Bad Company
| image          =
| image size     = 
| caption        = Mauvaises fréquentations 1999
| director       = Jean-Pierre Améris
| producer       = Philippe Godeau Alain Sarde
| writer         = Alain Layrac
| narrator       = 
| starring       = Maud Forget Lou Doillon
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}
Bad Company (Fr. Mauvaises fréquentations) is a 1999 French film starring Maud Forget and Lou Doillon. It is a romantic drama about two young students falling in love. The movie is written by Alain Layrac, and directed by Jean-Pierre Améris. A few scenes from this movie are featured in a music video by Lene Marlin for her song, Where Im Headed.

== Cast ==
* Maud Forget : Delphine 
* Lou Doillon : Olivia
* Robinson Stévenin : Laurent
* François Berleand : René
* Maxime Mansion : Alain
* Cyril Cagnat : Justin
* Delphine Rich : Claire

==External links==
* 

 

 

 
 
 
 
 
 


 
 