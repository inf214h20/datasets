Pay It Forward (film)
 
{{Infobox film
| name           = Pay It Forward
| image          = Pay it forward ver1.jpg
| alt            = 
| image_size     = 
| caption        = Theatrical release poster
| director       = Mimi Leder
| producer       = Mary McLaglen Jonathan Treisman Steven Reuther Peter Abrams Robert L. Levy (II) Paddy Carson
| screenplay     = Leslie Dixon
| based on       =  
| starring       = Kevin Spacey Helen Hunt Haley Joel Osment Jay Mohr Jim Caviezel Jon Bon Jovi Angie Dickinson
| music          = Thomas Newman
| cinematography = Oliver Stapleton
| editing        = David Rosenbloom
| studio        = Bel Air Entertainment Tapestry Films Pathé Warner Bros. Pictures
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $55,707,411 
}} novel of the same name by Catherine Ryan Hyde.  It was directed by Mimi Leder and written by Leslie Dixon.  It stars Haley Joel Osment as a boy who launches a good-will movement, Helen Hunt as his single mother, and Kevin Spacey as his social-studies teacher.

==Plot==
Trevor McKinney (Haley Joel Osment) is a seventh grade student with a life already full of worries. His mother, Arlene (Helen Hunt), works in a Las Vegas casino and is an alcoholic. His father, Ricky (Jon Bon Jovi), is a deadbeat who shows up only occasionally and stays just long enough to present some promise of reform and then beat up Arlene and terrify Trevor. His grandmother, Grace (Angie Dickinson), lives in her station wagon. His best friend, Adam (Marc Donato), is repeatedly the victim of a school bully, a situation which Trevor finds increasingly difficult to tolerate. Living in lower-middle-class suburban Nevada, Trevor looks out his  bedroom window and sees a bleak horizon.

One day, however, Trevors life changes. His social studies teacher for the new school year, Eugene Simonet (Kevin Spacey), gives the class a project: do something to change the world. Trevors expectant classmates have various reactions: some are mystified, some begin to be bored, and some roll their eyes. Trevors reaction is different, and it is clear that he is inspired by his teachers words. As Mr. Simonets  voice lays out his premise and expectations, Trevor is visibly moved. A few days later, he comes up with an idea: he decides to do "something big" for three people who "really need it," with the understanding that each will do the same for three more. In Trevors vision, the whole world will be populated by do-gooders, all working toward the end of worry.

==Cast==
* Kevin Spacey as Eugene Simonet: Trevors social studies teacher who assigns the goodwill assignment to his class. He later dates Arlene.
* Helen Hunt as Arlene McKinney: Trevors alcoholic, single mother who works in both a casino and a strip club. She later dates Eugene.
*   and fathers abuse and absence.  He starts the movement of "Pay It Forward". Trevor dies from a stab wound in his abdomen at the end of the movie.
* Jay Mohr as Chris Chandler, a reporter who interviewed Trevor.
* Jim Caviezel as Jerry, a homeless drug addict.
* Angie Dickinson as Grace, Arlenes mother. 
* Jon Bon Jovi as Ricky McKinney: Trevors abusive and alcoholic father who has since left the family.
* Marc Donato as Adam: Trevors friend who has been bullied his whole life at school. He was saved by Trevor from the bullies at the end of the movie
* Gary Werntz as Mr. Thorsen
* Loren D. Baum as Bully No. 1. He pays tribute to Trevor in redemption at the end of the movie.
* Nico Matinata as Bully No. 2. He pays tribute to Trevor in redemption at the end of the movie.
* Zack Duhame  as Bully No. 3. He pays tribute to Trevor in redemption at the end of the movie.
* Irving Leyson as Bully No. 4. He pays tribute to Trevor in redemption at the end of the movie.

==Production== book of the same name by Catherine Ryan Hyde, which was available as an open writing assignment. {{cite book
 | last =Cohen
 | first =David S
 | title =SCREENPLAYS: HOW 25 SCRIPTS MADE IT TO A THEATER NEAR YOU-FOR BETTER OR WORSE
 | publisher =HarperEntertainment
 | edition =First
 | year =2008
 | location =New York
 | pages =115 }}  Dixon struggled with the adaptation of the book in part because of multiple narrative voices within it. Specifically in that the reporter, the central character in the film, does not show up until halfway through the novel. Stuck, Dixon considered returning the money she was paid for the assignment. Cohen 117   She eventually hit upon the idea to start with the reporter and trace the events backwards.  Dixon presented the idea to Hyde who in turn liked it so much that she decided to change the then unpublished novels plot structure to mirror the films.  In the novel, the character of Eugene was originally a black man by the name of Reuben St. Clair. The role was offered to Denzel Washington, but he turned it down. Kevin Spacey was contacted next and accepted the role.

==Reception==
The film received mixed reviews, although Spacey, Hunt, and Osments performances in the film were universally praised.   s  .

==Box office==
The film opened at #4 in the North American box office making $9,631,359 USD in its opening weekend, behind Remember The Titans, Bedazzled (2000 film)|Bedazzled and Meet The Parents, which was on its third week at number one.

==See also==
* Pay it forward
* Magnificent Obsession, a 1954 film in which a similar concept was portrayed.

==Remakes & Character Map==

{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Stalin (2006) Jai Ho (2014) (Bollywood|Hindi)
|-
| Haley Joel Osment || Chiranjeevi || Salman Khan 
|- Trisha || Daisy Shah 
|}

==References ==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   fundraising program enabling giving to others through your everyday spending

 

 
 
 
 
 
 
 
 
 
 
 