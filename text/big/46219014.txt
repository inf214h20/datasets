Dr. Monica
{{Infobox film
| name           = Dr. Monica
| image          = 
| image_size     = 
| caption        = 
| director       = William Keighley William Dieterle (uncredited)
| producer       = Henry Blanke (uncredited)
| writer         = Charles Kenyon Laura Walker (as Laura Walker Mayer)
| story          = 
| based on       =  
| narrator       =  Jean Muir
| music          = Heinz Roemheld (uncredited)
| cinematography = Sol Polito William Clemens
| studio         = Warner Bros.
| distributor    = Warner Bros. / The Vitaphone Corp.
| released       =  
| runtime        = 61-65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jean Muir. A woman doctor who is unable to have children discovers that her husband has gotten another woman pregnant.

==Cast==
* Kay Francis as Dr. Monica
* Warren William as John
* Jean Muir as Mary
* Verree Teasdale as Anna
* Emma Dunn as Mrs. Monahan
* Phillip Reed as "Bunny" Burton
* Herbert Bunston as Mr. Pettinghill
* Ann Shoemaker as Mrs. Hazlitt
* Virginia Hammond as Mrs. Chandor
* Hale Hamilton as Dr. Brent
* Virginia Pine as Louise 

==Reception==
Mordaunt Hall, critic for The New York Times, wrote that Dr. Monica is "not especially suspenseful", but it "moves apace and the acting is excellent." 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 

 