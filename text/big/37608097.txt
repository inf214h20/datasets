Thotti Gang
{{multiple issues|
 
 
}}

{{Infobox film
| name = Thotti Gang
| image = Thotti Gang.jpg
| writer = E. V. V. Satyanarayana Sunil Gajala
| director = E. V. V. Satyanarayana
| cinematography = Loknath
| producer = E. V. V. Satyanarayana
| editing = Marthand K. Venkatesh
| studio = E. V .V. Cinema
| country = India
| released = 6 December 2002
| runtime =
| language = Telugu
| music = Devi Sri Prasad
| awards =
| budget =
| gross =
}}

Thotti Gang is a 2002 Telugu comedy film produced and directed by E. V. V. Satyanarayana starring Prabhu Deva, Allari Naresh,  Sunil (actor)|Sunil, Gajala and Anita Hassanandani in the lead roles. The movie was released on 6 December 2002 to positive reviews and was declared a super hit at the box office. The movie was inspired from the Hollywood movie Saving Silverman.

==Plot==
Achi Babu (Allari Naresh), Sathi Babu (Sunil (actor)|Sunil) and Suri Babu (Prabhu Deva) are the best buddies since childhood. They form a band that does multi purpose jobs like acting as band of boys for marriages, professional catchers of dogs, and cremation service guys. Karate Malliswari (Gajala), another childhood nightmarish karate-skill-flaunting damsel, lures Achi Babu into love. Then she snatches Achi Babu away from Suri Babu and Sathi Babu to make him her live-in boy friend. They also get engaged to each other. Irritated by their act, Suri Babu and Sathi Babu kidnap Malliswari. They tie her to a chair and then hatch another plan to make Achi Babu believe that Malliswari is dead. In that process, they dig a fresh dead body from a crematorium, pack her in the car of Malliswari and throw that car into a valley. Police confirms that Malliswari is dead and Achi Babus heart breaks again. Then appears Venkata Lakshmi (Anita Hassanandani), the childhood sweetheart of Achi Babu. Venkata Lakshmi has another flashback and she was sexually abused by the husband of her elder sister. She was in a hurry to join an Ashram run by Matasri (Shakeela). Sathi Babu and Suri Babu hatch another plan to unite this couple. At this moment, Malliswari escapes from the den. The rest of the story is all about how these confused souls are paired to give a happy ending.

==Cast==
* Prabhu Deva as Suri Babu
* Allari Naresh as Achi Babu Sunil as Sathi Babu
* Anita Hassanandani as Venkata Lakshmi
* Shakeela as Matasri
* L.B. Sriram as Alexander
* M. S. Narayana and Chalapathi Rao as Bongu Brothers
* Jaya Prakash Reddy in a Special Appearance
* Brahmanandam as Galigottam Govinda Sastry

==Soundtrack==
{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers !! Length
|-
| "Nuvve Kaavaali" || S. P. B. Charan, Sumangali || 04:28
|- Ranjith || 05:26
|- Chitra || 05:22
|-
| "Orinayano" || Sandeep, Kalpana || 05:15
|-
| "Gundello Nuvve" || Devi Sri Prasad, Febi || 05:23
|- Mathangi || 05:28
|}

==External links==
*  
 

 
 
 
 
 
 
 