The Making of a Martyr
{{Infobox Film
| name           =  The Making of a Martyr
| image          =  
| image_size     = 
| caption        =  
| director       =   
| producer       =   
| writer         =   
| narrator       =   
| starring       =   
| music          =   
| cinematography =   
| editing        =   
| studio         =   
| distributor    =   
| released       =   
| runtime        =   
| country        =  
| language       =   
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Canadian film directors Brooke Goldstein and Alistair Leyland.

==Story== Palestinian Intifada Islamic Jihad Summer Academy in Tulkarem, and more. Their interviews are a window into the causes behind the recent phenomenon of child suicide bombers, and why Palestinian children are being recruited to violently kill themselves and others for the sake of Jihad.
 

==Purpose==
First-time director and human rights attorney Brooke Goldstein made this film with the intent of facilitating the enforcement of Palestinian children’s human rights, namely their right to life and to an education free of incitement to kill themselves.  Director/Producer Alistair Leyland has been an advocate of providing exposure to human rights abuses for some time. Having spent time covering the "one child policy" in China and its effects on infant girls, Leyland knew the story of a 16 year old Palestinian suicide bomber was both horrifying and complex.

{{quote
 |text=It was after shooting tens of hours of interviews in the region that I finally realized these children are being preyed upon by higher powers. This issue needed to be addressed, his (Abdos) story needed to be told.
 |sign= The Making of a Martyrs Director/Producer Alistair Leyland
 }}

==Awards==
In April 2006, The Second Annual United Nations Documentary Film Festival honored The Making of  a Martyr with the Audience Choice Award for Best Film.  Brooke Goldstein and Alistair Leyland were on-site to accept their first award for this troubling film. 

Official selections include: Brooklyn International Film Festival (2007); Malibu International Film Festival (2007);  Shoot-Me Film Festival (2007); Liberty Film Festival (2007); Whistler Film Festival (2006); Anchorage International Film Festival (2006); United Nations Documentary Film Exposition, London, England (2006); Shoot-Me Film Festival The Hague (2007).

==External links==
=== Online media - videos ===
* Making of a Martyr -  .
* Making of a Martyr Bonus Features - Walid Shoebat:  ,  .

=== Other ===
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 