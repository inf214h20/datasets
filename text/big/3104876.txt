Pooh's Grand Adventure: The Search for Christopher Robin
 
 
{{Infobox film
| name        = Poohs Grand Adventure: The Search for Christopher Robin
| image       = PGATSFCR.jpg
| caption     = 2006 edition DVD cover art
| director    = Karl Geurs
| producer    = Karl Geurs Gina Shay
| writer      = Carter Crocker Karl Geurs
| based on    =   David Warner
| starring    = Jim Cummings John Fiedler Ken Sansom Paul Winchell Peter Cullen Brady Bluhm Andre Stojka
| music       = Carl Johnson
| studio      = DisneyToon Studios Walt Disney Pictures
| distributor = Walt Disney Home Video
| released    =  
| runtime     = 75 minutes
| country     = United States Japan
| language    = English
}}
 animated direct-to-video film directed by Karl Geurs. The film follows Pooh and his friends on a journey to find and rescue their friend Christopher Robin from the "School|Skull". Along the way, the group confront their own insecurities throughout the search, facing and conquering them in a series of events where theyre forced to act beyond their own known limits, thus discovering their true potential. Unlike the films predecessors, this film is an entirely original story, not based on any of A. A. Milnes Pooh stories (although some elements derive from In Which Rabbit Has a Busy Day and We Learn What Christopher Robin Does in the Mornings from The House at Pooh Corner).

The film received generally negative reviews due to its dark themes and imagery, which also resulted in its release as a List of Disney direct-to-video films|direct-to-video feature film. However, it is also the first Winnie the Pooh film ever to have its own special edition.

==Plot==
The story begins on the last day of summer. Christopher Robin is unable to tell his friend Winnie-the-Pooh some sad news, and leaves him with the advice, "Youre braver than you believe, and stronger than you seem, and smarter than you think," but Pooh doesnt clearly understand. The next morning, Pooh discovers a honey pot with an attached note - however, he cannot read it himself after getting honey all over it. He goes around to his friends Piglet, Tigger, Rabbit and Eeyore, and none of them are able to read it, so they ask Owl for help. From misinterpreting the note and his own romantic imagination of adventure, Owl deduces that Christopher Robin has been taken to a distant, mysterious and dangerous place called "Skull" against his will, to a cave where the monstrous "Skullasaurus" resides. Owl equips the group with a map and sends them into the "Great Unknown" of the Hundred Acre Wood.

During their journey through the Great Unknown, as they are seemingly hunted by the Skullasaurus, the group slowly begins to realize just how helpless they are without Christopher Robin in the outside world. Piglet, Tigger, and Rabbit come to believe they dont have the courage, strength, or intelligence respectively to go on; Piglet is abducted by a swarm of butterflies in a tranquil field, leaving him feeling weak and helpless; Tigger plummets into a deep gorge and is unable to bounce out to safety, causing his friends to fall with him, and Rabbit continuously makes poor leadership decisions following Owls inaccurate map. Pooh tries to comfort them each with the advice Christopher Robin had given him, but fails due to his inability to remember exactly what he said. When Rabbit finally breaks down, admitting he has no idea where they are going, the group comes to terms with the fact that they are lost and helpless without Christopher Robin, and take shelter in a nearby cave. While everyone is asleep, Pooh laments on getting no closer to finding Christopher Robin.

In the morning, the five realize theyd spent the night in their destination, Skull Cave. The five enter and split up to search for Christopher Robin on their own after coming across multiple paths. Pooh gets stuck in a small gap in the caves crystals, and the four others tumble about to a ledge before finding the "Eye of the Skull" where Christopher Robin supposedly is trapped. Believing Pooh to have been killed by the Skullasaurus, they rise past their fears and doubts and make their way to the Eye of the Skull. Upon seeing his friends bravery, Pooh excitedly frees himself from the crevasse, only to be trapped in a deep pit where he can find no way out. While there, he realizes that Christopher Robin is still with him in his heart, even when they are not together, just as Christopher had promised. After Piglet, Rabbit, Tigger and Eeyore enter the Eye, they are found by Christopher Robin who has been searching for them as well. He explains he was only at school, and the roars of the Skullasaurus they have been plagued by are actually the noises of Poohs stomach.

Christopher Robin then rescues Pooh from the deep pit. The six exit the Skull Cave, only to discover that from the outside, it and all the other locations on the map werent nearly as big, nor as scary as they seemed. They return home, and that evening, Christopher Robin says he will return to school the next day. Pooh declares that he will always be waiting for him, and the two happily watch the sunset, knowing they will always have each other in the sanctuary of the Hundred Acre Wood.

==Songs==

* "Forever and Ever", Performed by Jim Cummings and Frankie J. Galasso
* "Adventure is a Wonderful Thing", Performed by Andre Stojka
* "If It Says So", Performed by Ken Sansom
* "Wherever You Are", Performed by Jim Cummings
* "Everything is Right", Performed by Jim Cummings, Dylan Watson, Ken Sansom, Steve Schatzberg, Andre Stojka, and Frankie J. Galasso.
* "Wherever You Are"   (End Title) Performed by Barry Coffing and Vonda Shepard

==Cast==
*Jim Cummings - Winnie-the-Pooh/Skullasaurus/Tigger (Singing voice, uncredited) Piglet
**Steve Piglet (Singing voice, Uncredited) Rabbit
*Paul Winchell - Tigger
*Peter Cullen - Eeyore
*Brady Bluhm - Christopher Robin
**Frankie J. Galasso - Christopher Robin (singing voice) Owl
*David David Warner - Narrator
*Dylan Watson - Eeyore Singing Voice

==Reception==
Poohs Grand Adventure has garnered much less praise than its predecessors. It has received generally mixed to negative reviews from critics. Most criticism was geared toward the films dark imagery and subject matter, which was deemed too frightening for younger viewers.  The review aggregator website Rotten Tomatoes reported that the film received a "Rotten" rating of 38%, based on 8 reviews (with an average rating of 4.5/10), making it unique in that it became the first and only Pooh film to earn a "rotten" certification, and also the only direct-to-video Pooh film to have a score at all.  George Blooston of Entertainment Weekly gave the film a C grade, calling it "treacly" and criticised its lack of "grown up-wit   child psychology" that made The Many Adventures of Winnie the Pooh a classic.  Reviewer Ellen Rosen criticised the films plot as being "meaningless", and she commented that "eighty percent of its scenes are scary." David Nusair of Reel Film Reviews called the film "tedious", and Alex Sandell of Juicy Cerebellum felt that Disney "sucked with  ." 
 Siskel and Ebert gave the film "Two thumbs up" in their review.  Jane Louise Boursaw of Kaboose gave the film a positive review, praising the films songwriting and animation.  John J. Puccio of Movie Metropolis was also positive; while he admitted that it is more "adventurous" than Milnes stories, he felt that this was compensated by the films visual appearance and "unaffected charm." 

==References==
 

==External links==
 
* 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 