Det gælder os alle
{{Infobox film
| name           = Det gælder os alle
| image          = Det gælder os alle.jpg
| caption        = Film poster
| director       = Alice OFredericks
| producer       = Svend Nielsen
| writer         = Svend Rindom
| starring       = Poul Reichhardt
| music          = Sven Gyldmark
| cinematography = Einar Olsen
| editing        = Edith Schlüssel
| distributor    = 
| released       = 16 February 1949
| runtime        = 94 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Det gælder os alle is a 1949 Danish drama film directed by Alice OFredericks.

==Cast==
* Poul Reichhardt - Jørgen Vedel
* Lisbeth Movin - Edith
* Ib Schønberg - Direktør Lassen
* Agnes Rehni - Fru Lassen
* Lily Broberg - Karen
* Ilselil Larsen - Leni Rosner
* Preben Lerdorff Rye - Chauffør Olsen
* Preben Mahrt - Kurt
* Helga Frier - Grethe
* Tom Rindom Thomsen - Hugo
* Grete Bendix - Søster Erika
* Signi Grenness - Anna
* Karen Meyer - Ninas mor
* Per Buckhøj
* Alma Olander Dam Willumsen - Fru Gormsen
* Sigurd Langberg
* Bente Hansen - Tove
* Hanne Clement - Jette
* Else Marie Jørgensen - Else
* Verna Olesen - Nina
* Wenche Klouman - Gerd

==External links==
* 

 

 
 
 
 
 
 
 
 


 