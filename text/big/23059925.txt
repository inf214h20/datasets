Breath Control: The History of the Human Beat Box
 
{{Infobox film
| name           = Breath Control: The History of the Human Beat Box
| image_size     = 
| image	=	Breath Control- The History of the Human Beat Box FilmPoster.jpeg
| caption        = 
| director       = Joey Garfield
| producer       = Joey Garfield Jacob Craycroft Zachary Mortensen
| writer         = 
| narrator       =  Skratch Wise Wise Daddy-O (musician)|Daddy-O 4Zone
| music          = 
| cinematography = 
| editing        = Jacob Craycroft
| distributor    = Ghost Robot Tribeca Film Festival 2002
| runtime        = 74 min.
| country        = United States English
| budget         = 
| gross          = 
}} Hip Hop culture, alongside Disc jockey|Dj-ing, Graffiti, Breakdancing, and Rapping|MC-ing. Unfortunately, its contribution has been largely overlooked, as has the fun, expressive, human, and spontaneous dimension of Hip Hop that it represents. As the first documentary of its kind, Breath Control: The History of the Human Beat Box uses interviews, live performances, archival footage, and animation to bring to light this important and neglected ingredient of Hip Hops identity .

With the help of Beat Box pioneers Doug E. Fresh, Wise, Biz Markie, and The Fat Boys, Breath Control traces this art form from its basic beat beginnings in the Eighties to its present day multi-layered, polyrhythmatic figureheads Rahzel and Skratch of the Hip Hop group The Roots. But Breath Control isnt limited to Hip Hop. Musician Zap Mama opens up the idea that human beat boxing is an art form practiced all over the world and has been refined by many different cultures. Breath Control is a half historical, half tutorial look at humans as actual instruments.

==External links==
* 
* 
* 
* 
* 
*  at Eye For Film
*  at the Yahoo!Xtra|Yahoo!Xtra Movies
*  at Answers.com

 
 

 
 
 
 
 
 


 