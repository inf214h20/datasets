Gopurangal Saivathillai
{{Infobox film
| name           = Gopurangal Saivathillai
| image          = 
| caption        = 
| director       = Manivannan
| producer       = P. Kalaimani
| writer         = Manivannan
| screenplay     = Manivannan
| story          = P. Kalaimani Mohan Suhasini Suhasini Radha Radha S. Ve. Shekher
| music          = Ilaiyaraaja
| cinematography = A. Sabapathy
| editing        = L. Kesavan
| studio         = Everest Films
| distributor    = Everest Films
| released       =  
| runtime        = 135 minutes
| country        = India Tamil
}} 1982 Tamil Suhasini and Radha in lead roles. Naseeb Apna Apna (1986) with Rishi Kapoor and Radhika. It was remade in Telugu as Mukku Pudaka with Bhanu Chander reprising Mohans role, while Suhasini (as in original) and Vijayashanti (for Radha in original) were part of main cast.

==Plot==

Arukkani (Suhasini Maniratnam) is an illiterate village girl and her father is upset to not find her a groom. One day, her father meets his old friend Bhoothalingam and shares his sadness, Bhoothalingam proposes his son to marry her and Arukkanis father accepts. Murali (Mohan (actor)|Mohan), Bhoothalingams son, is an handsome man and also a successful sales manager. He doesnt see his future wife yet and he is excited to see her. At his marriages day, Murali is shocked to see his future wife Arukkani who is ugly and gets married with her anyway. Murali feels very disappointed and cannot bear her. He takes a transfer to Bangalore leaving his father and his wife. In Bangalore, he gets friendly with his colleague Julie (Radha (actress)|Radha), who is modern and good-looking. They fall in love with each other and they get married. What transpires later forms the crux of the story.

==Cast==
 Mohan as Murali
*Suhasini Maniratnam as Arukkani Radha as Julie
*S. Ve. Shekher as Stanley (Julies brother)
*Vinu Chakravarthy as Bhoothalingam
*T.K.S. Chandran
*Sundar
*Manobala
*Kamala Kamesh
*Nalini
*N. Mohan
*Vellai Subaiah Senthil
*Loose Mohan
*Haja Shareef
*Sabitha Anand

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || En Purushanthan || S. P. Sailaja, Sasi Rekha || Muthulingham || 04:05
|-
| 2 || Oorengum || Ilaiyaraaja || Gangai Amaran || 03:58
|-
| 3 || Poo Vaadaikatru || Ilaiyaraaja, S. Janaki, Krishnachandran || Vairamuthu || 03:57
|-
| 4 || Pudhichalum Pudichen || Krishnachandran || Avinashi Mani || 04:14
|- Vaali || 04:08
|}

==References==
 

 
 
 
 
 
 
 
 
 


 