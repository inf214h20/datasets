His Ward's Love
{{Infobox film
| name           = His Wards Love
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = Arthur V. Johnson
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 3 minutes (16 Frame rate|frame/s)
| country        = United States
| language       = Silent
| budget         = 
}}

His Wards Love is a 1909 American Short film directed by D. W. Griffith and the film was made by the American Mutoscope and Biograph Company 
==Cast==
* Arthur V. Johnson as Reverend Howson
* Florence Lawrence as The Reverends Ward
* Owen Moore as General Winthrop
* Linda Arvidson as The Maid

==Storyline==
Reverend Howson loves his young ward, but urges her to marry someone else. She accepts the proposal, but then sees the Reverend kissing an object she has dropped, and realizes he loves her.

==See also==
* List of American films of 1909
* D. W. Griffith filmography

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 

 