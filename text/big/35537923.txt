Accidental Meeting
{{Infobox film
| name           = Accidental Meeting
| image          = 
| image_size     =
| caption        =
| director       = Michael Zinberg
| producer       = Walter Klenhard Christopher Horner Pete Best
| narrator       = 
| starring       = Linda Gray Linda Purl Patrick Williams
| cinematography = Michael W. Watkins
| editing        = Robert L. Sinise David L. Bertman
| studio         =
| distributor    = USA Network
| released       = March 17, 1994
| runtime        = 92 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1994 television Strangers on a Train (1951). 

== Plot ==
Jennifers husband is committing adultery. Maryannes boss treats her like dirt. After an accidental meeting, the women decide to kill each others men. Jennifer goes through with the deal, but Maryanne backs out. Jennifer, enraged, will not rest until Maryanne lives up to her part of the deal.

==Cast==
*Linda Gray as Jennifer Parris
*Linda Purl as Maryanne Bellmann
*Leigh McCloskey as Richard
*Ernie Lively as Obrenski
*David Hayward as Jonathan Holtman
*Kent McCord as Jack Parris
*Lorna Scott as Lynn
*Nancy Hochman as Julie
*Bethany Richards as Zoe
*Steve Tom as Hallman
*Duke Stroud as Palmer

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 