Bharosa (1940 film)
{{Infobox film
| name           = Bharosa 
| image          = Bharosa_1940.jpg
| image_size     = 
| caption        = Chandra Mohan and Sardar Akhtar in Bharosa (1940)
| director       = Sohrab Modi
| producer       = Sohrab Modi
| writer         = Lalchand Bismil 
| narrator       = 
| starring       = Chandra Mohan Sardar Akhtar Majid Khan Sheela
| music          = G. P. Kapoor
| cinematography = Y. D. Sarpotdar
| editing        = 
| distributor    = 
| studio         = Minerva Movietone
| released       = 15 August 1940
| runtime        = 147 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1940 Hindi/Urdu Chandra Mohan, Sardar Akhtar, Mazhar Khan, Sheela, Maya Devi and Eruch Tarapore.  

The film revolves around an incestuous relationship that develops unwittingly between a brother and sister. The theme was "considered to be quite revolutionary" with a "daring" "thematic climax".   

==Plot== Chandra Mohan) are good friends and when Gyan has to go to Africa for work, he leaves his wife Shobha (Sardar Akhtar) in care of Rasik and his wife Rambha (Maya Devi). Rasik has always liked Shobha but kept silent about it. Rambha goes to her parents home, leaving Rasik and Shobha alone in the house. Rasik gives in to his feelings with Shobha being a willing participant. Soon Shobha gives birth to a daughter (Indira) whom Gyan believes to be his child. Rasik and Shobha are shocked when Gyan decides on an alliance between Indira and Rasiks son Madan seeing the close relationship they share.

==Cast== Chandra Mohan as Rasik
* Sardar Akhtar as Roshan
* Mazhar Khan as Gyan
* Sheela as Indira
* Maya Devi
* Naval as Madan
* Eruch Tarapore
* Gulab
* Menaka
* Abu Bakar
* Ram Apte

==Review==
The Filmindia editor, Baburao Patel, not known to have an amiable relationship with Modi, normally panned his films. However for Bharosa, as cited by Amrit Gangar in his book, Patels review carried the headline "Sohrab Modi Directs His First Good Picture" going to the extent of claiming that Bharosa was a better picture than Modis Pukar (1939 film)|Pukar.    

==Soundtrack==
The music director was G. P. Kapoor with lyrics by Lalchand Bismil. The singers were Khan Mastana, Menka, Sheela, Paresh Bannerji and Sardar Akhtar. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Bagiya Seencha Jaane
| Khan Mastana, Menka
|-
| 2
| Chhum Chhum Chhanan Naach Rahi Hai Maya
| Menka
|-
| 3
| Saanjh Bhai Samajh Na Paye
| Sheela
|-
| 4
| Man Murkh Samajh NaPaye
| Menka
|-
| 5
| More Naina Bhar Aaye Khushi Se
| Sheela
|-
| 6
| Ghaat Se Bandhan Khol Re Sajan
| Sheela, Presh Bannerji
|-
| 7
| Bairi Milan Na De Sajan
| Sheela, Presh Bannerji
|-
| 8
| Bhor Bhai Uth Jaag Re Manwa
| Leela
|-
| 9
| Kaahe Roki Baat Hamari Krishna Murari
| Sardar Akhtar
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 