Rocket Gibraltar
{{Infobox film
| name           = Rocket Gibraltar
| image          = Rocket Gibraltar.jpg
| image_size     =
| caption        =
| director       = Daniel Petrie Robert Fisher
| writer         = Amos Poe
| narrator       =
| starring       = Burt Lancaster Patricia Clarkson Macaulay Culkin
| music          = Andrew Powell
| cinematography = Jost Vacano
| editing        = Melody London
| distributor    = Columbia Pictures
| released       =  
| runtime        = 99 min.
| country        = United States English
| budget         =
| gross          = $187,349
}}

Rocket Gibraltar is a 1988 American drama film released directed by Daniel Petrie, and stars Burt Lancaster, Patricia Clarkson and Macaulay Culkin in his film debut.

== Plot ==
Levi Rockwell is a retired, widowed Hollywood screenwriter and patriarch who reunites his entire family at his Long Island estate for his 77th birthday, but personal and social problems abound. His four children, son Rolo, and daughters Ruby, Rose and Aggie arrive, along with their spouses and children, to help him celebrate his 77th birthday. During the course of the family reunion, Levis health begins to fail and he passes on a sentimental request that he be given a Viking Funeral after his death. With his adult children consumed by their own personal worries, the grandchildren honors Levis last wishes.

== Cast ==
* Burt Lancaster as Levi Rockwell
* Suzy Amis as Aggie Rockwell
* Patricia Clarkson as Rose Black
* Macaulay Culkin as Cy Blue Black
* Angela Goethals as Dawn Black
* Frances Conroy as Ruby Hanson John Glover as Rolo Rockwell
* Sinéad Cusack as Amanda "Billi" Rockwell
* Sara Rue as Jessica Hanson
* Bill Pullman as Crow Black
* Kevin Spacey as Dwayne Hanson
* David Hyde Pierce as Monsieur Henri
* James McDaniel as Policeman George Martin as Doctor

==External links==
* 
* 

 

 
 
 
 
 

 