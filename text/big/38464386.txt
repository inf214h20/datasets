River Patrol (film)
 
 
{{Infobox film
| name           = River Patrol
| image          =
| caption        =
| director       = Ben R. Hart 
| producer       = Hal Wilson
| writer         = James Corbett
| starring       = John Blythe   Lorna Dean   Wally Patch   Stan Paskin
| music          = 
| cinematography = Bertram Brooks-Carrington
| editing        = James Corbett
| studio         = Hammer Film Productions   Knightsbridge Films
| distributor    = Exclusive Films 
| released       = 28 January 1948
| runtime        = 46 minutes 
| country        = United Kingdom 
| awards         =
| language       =  English 
| budget         =
| preceded_by    =
| followed_by    =
}} earliest films made by Hammer following its relaunch after the Second World War.

==Synopsis==

Two British policemen, Robby and Jean, go undercover, pretending to be husband and wife in order to smash a ring of smugglers along the Thames. During the investigation they visit the most shady places of London, including a night club with the worst imaginable reputation in the city. The two fake-spouses befriend and dupe the night club owner and find evidence leading to the top of the smugller ring. However, their identities are revealed by the criminals before they can report back to their superiors, and they have to fight alone against the thugs until they finally manage to get them all arrested.  



==Cast==
* John Blythe as Robby
* Lorna Dean as Jean
* Wally Patch as The Guy
* Stan Paskin
* Cyril Collins
* George Crowther
* Andrew Sterne
* Wilton West
* Tony Merrett
* George Kane 
* Johnny Doherty
* Iris Keen
* Dolly Gwynne

==References==
 

==Bibliography==
* Meikle, Denis. A History of Horrors: The Rise and Fall of the House of Hammer. Scarecrow Press, 2010.
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2009.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 