Murder at Site 3
 
 
{{Infobox film
| name           = Murder at Site 3
| director       = Francis Searle
| producer       = Charles Leeds, Francis Searle
| writer         = Paddy Manning OBrine, Peter Saxon (original novel, Crime is my Business)
| starring       = Geoffrey Toone, Barbara Shelley
| music          = Don Banks
| cinematography = Bert Mason
| studio         = Hammer Film Productions
| released       = 1958 
| runtime        = 67 minutes
| country        = United Kingdom
| language       = English
}}
 Hammer movie from 1958, featuring the character of Sexton Blake.   It stars Barbara Shelley, Geoffrey Toone and John Warwick. 

==Synopsis==

Sexton Blake tracks down a gang who have stolen secrets from a rocket site. 

==Cast==

* Geoffrey Toone as Sexton Blake
* Barbara Shelley as Susan
* John Warwick as Commander Chambers
* Richard Burrell as Tinker
* Jill Melford as Paula Dane

==References==

 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 

 