Rikas tyttö
{{Infobox film
| name           = Rikas tyttö
| director       = Valentin Vaala
| image          =
| producer       =
| story          = Kersti Bergroth
| screenplay     = Nisse Hirn
| starring       = Sirkka Sari Olavi Reimas Lea Joutseno
| distributor    = Suomi-Filmi
| released       = 17 September 1939
| runtime        = 80 minutes
| country        = Finland
| language       = Finnish
| budget         =
| gross          =
}}
 Finnish film directed by Valentin Vaala.  The film is remembered today for being the last film starring Sirkka Sari, due to her death at a party for the cast and crew of the film. Due to Saris death, another actress replaced her for the few remaining scenes that needed to be filmed. Scenes were shot from further away to hide the fact her character was being played by another actress.

==Full credited cast==
*Sirkka Sari as Anni Hall
*Olavi Reimas as Vilhelm Vinter
*Lea Joutseno as Lea
*Hannes Häyrinen as Markus Hall
*Irma Seikkula as Irja Rantanen
*Turo Kartto as Baron Allan Ahlfeldt
*Anni Aitto as Mrs. Hall
*Arvi Tuomi as Alfred Hall
*Elsa Rantalainen as Mrs. Karila
*Eija Karipää as Edla Lundström (as Eija Londén)
*Uolevi Räsänen as Lasse
*Tuulikki Schreck as Auroora Rantanen

==References==
 

==External links==
*  

 
 
 


 