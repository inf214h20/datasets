A Holiday Romance
{{Infobox film
| name           = A Holiday Romance
| image          = 
| alt            =  
| caption        = 
| director       = Bobby Roth
| producer       = Randi Richmond
| writer         = Darrah Cloud
| starring       = Naomi Judd Andy Griffith Gerald McRaney Alison Pill
| music          = Christopher Franke
| cinematography = Eric Van Haren Noman
| editing        = Armen Minasian
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Holiday Romance (released in the UK under the title A Song for the season) is a 1999 film directed by Bobby Roth and starring Naomi Judd, Andy Griffith, and Gerald McRaney. This film has been released on DVD.

==Plot==

During the holiday season, Bethlehem School goes under review by a stern school administrator Cal Peterson (Gerald McRaney), sent to eliminate programs to save money and the school itself. Following long review, Cal decides to eliminate the music program led by music teacher Lily Waite (Naomi Judd). Cal is unaware that his teenage niece Fern (Alison Pill), whom he has been taking care of, has been changed by the music program. At the final concert, before Lily must leave her job, the music program is saved by generous donations, and Cal sees Fern perform. There are many sub-plots, including the hijinks of Jake Peterson (Andy Griffith). With a musical back ground, this film is unique in itself, it reaches out to the viewer not only through acting but through the joy of Christmas music, showing tradition in a new light, and showing that you are only as old as you feel.

==Cast==
*Naomi Judd - Lily Waite
*Andy Griffith - Jake Peterson
*Gerald McRaney - Cal Peterson
*Alison Pill - Fern
*Jayne Eastwood - Margie
*Taborah Johnson - Anne Hutchinson
*Jackie Richardson - Bea Buskins
*Brian Heighton - Todd
*Jack Duffy - Irwin
*Sumela Kay - Clarissa
*Adam Dolson - Del Nathan Carter - Hal
*Martha Gibson - Donna
*Andrea Lewis - Autumn
*Aron Tager - Joseph
*Ken Wickes - Pete The Essentials with Paula MacNeill

==External links==
* 

 

 
 
 
 
 
 


 