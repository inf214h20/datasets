Yol
 
{{Infobox film 
| name           = Yol 
| image          = Yol (1982 film).jpg
| caption        = Poster of Yol 
| writer         = Yılmaz Güney 
| starring       = Anita P. Ness Halil Ergün Şerif Sezer Meral Orhonsay
| director       = Yılmaz Güney  Şerif Gören
| producer       = Edi Hubschmid Yılmaz Güney 
| music          = Sebastian Argol Zülfü Livaneli 
| released       =  
| runtime        = 124 minutes
| country        = Turkey  Turkish Kurdish Kurdish
| awards         = 
}} Turkish for Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

The film is a portrait of Turkey in the aftermath of the 1980 Turkish coup détat: its people and its authorities are shown via the stories of five prisoners given a weeks home leave. The film has caused much controversy in Turkey, and was banned until 1999 due to Yılmaz Güneys involvement rather than its content.

==Plot==
Yol tells the story of several prisoners on furlough in Turkey. Seyit Ali (Tarık Akan) travels to his house and finds that his wife (Şerif Sezer) has betrayed him and works as a prostitute. She was caught by her family and held captive for Seyit Ali to end her life in an honor killing. Though apparently determined at first, he changes his mind when his wife starts to freeze while travelling in the snow. Despite his efforts to keep her alive, he eventually fails. His wifes death relieves Seyit Ali from family pressure and he is saved from justice since she freezes but he has an internal struggle and must return to jail.

Mehmet Salih (Halil Ergün) has been arrested for his role in a heist with his brother-in-law, whom he abandoned as he was being shot by police. His in-laws want nothing to do with him, and he is finally forced to tell his wife Emine (Meral Orhonsay) the truth. Emine and Mehmet Salih decide to run away and get on a train. On the train, they get caught in the toilet while having long-awaited sex with each other. They are saved from an angry mob by the trains officers and held in a cabin before being handed over to officials. There, a young boy from Emines family who boarded the train shoots both Mehmet Salih and Emine.

Ömer (Necmettin Çobanoğlu) returns to his village. Being a border village, it has a struggle with the army due to smuggling. Ömer visits and arranges to cross the border to escape prison. Though Ömer is clearly determined, he gives up after his brother is shot dead while smuggling. Through his brothers death, Ömer has inherited the responsibilities of his brothers wife and children as dictated by tradition.

Each prisoner in the film suffers from a conflict that threatens his freedom, with tradition also imprisoning him.

== Rights dispute ==
The rights to Yol were disputed for a long time. Even during Yilmaz Güneys lifetime, there were major conflicts about the ownership of the film between Güney and Donat Keusch, the head of a Switzerland|Swiss-based service company called Cactus Film AG, who claimed to own the entire rights of the film. After Güneys death, the dispute escalated between Keusch and Güneys widow.

When Keusch filed for bankruptcy with his Cactus Film AG     in 1999, the situation became even more complicated and resulted in numerous lawsuits in both Switzerland and France. There still are numerous sellers in the market claiming to be the sole owner of the world rights to Yol, and the film is offered in different versions through different distribution channels.      
 CNC (film number 2010.2922), Donat Keusch acquired the exclusive rights for Yol on March 4, 2010.

== Awards and nominations ==
*1982 Cannes Film Festival, won FIPRESCI Prize   
*1982 Cannes Film Festival, won Palme dOr 
*1983 César Awards, nominated for César
*1983 French Syndicate of Cinema Critics, won Critics Award
*1983 Golden Globes, nominated for Golden Globe for Best Foreign Film
*1984 London Critics Circle Film Awards, won ALFS Award

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*   page for the film

 
 

 
 
 
 
 
 
 
 
 
 