The Pillow Book (film)
 
  name            = The Pillow Book image           = The_Pillow_Book_poster.jpg caption         = Theatrical poster. director        = Peter Greenaway producer        = Kees Kasander writer          = Peter Greenaway starring        = Vivian Wu Ewan McGregor Ken Ogata Yoshi Oida Hideko Yoshida Judy Ongg music           = Brian Eno cinematography  = Sacha Vierny editing  Chris Wyatt studio          =   distributor  Lions Gate Films released        =   runtime         = 126 minutes country         = France Netherlands United Kingdom language        = English budget          = box office      = $2,372,744 
|}}
The Pillow Book is a 1996 film by British director Peter Greenaway, which stars Vivian Wu as Nagiko, a Japanese model in search of pleasure and new cultural experience from various lovers. The film is a rich and artistic melding of dark modern drama with idealised Chinese and Japanese cultural themes and settings, and centres on body painting. The film features full-frontal male nudity. 

The film co-stars Ewan McGregor as Jerome, an English translator who becomes Nagikos favourite lover. Greenaway also wrote the screenplay, in addition to directing.

==Plot == the book of observations by  , actual name believed to be  , from whence the protagonists name in the film.
 characters of the book of observations. Nagikos aunt tells her that when she is twenty-eight years old, the official book of observations will be officially 1000 years old, and that she, Nagiko, will be the same age as Sei Shōnagon when she had written the book (in addition to sharing her first name). Nagiko also learns around this time that her father is in thrall to his publisher, "Yaji-san" (Yoshi Oida), who demands sexual favours from her father in exchange for publishing his work.

===Early chapters===
The publisher arranges Nagikos wedding to his young apprentice. Her husband (Ken Mitsuishi), an expert archery|archer, resents Nagikos love for books and her desire to read, in spite of his apprenticeship. He also refuses to indulge in her desires for pleasure, refusing to write on her body. When he discovers and reads Nagikos pillow book, he is extremely resentful, setting it on fire and thus setting fire to their marital home, an event which Nagiko describes to be the first major fire of   life. Insulted and enraged, Nagiko leaves him for good.
 type to depletion of forests due to the need to make paper.

After working as a secretary in the office of a Japanese fashion designer for a while, Nagikos employer takes a liking to her and makes her one of his models. As a successful fashion model, Nagiko hires a maid (  and are lousy lovers, or vice versa.

One day, at the Cafe Typo, Nagikos favourite haunt, she meets Jerome (Ewan McGregor), a British translator. Intrigued by his knowledge, they go to a private space where she has Jerome write on her body in various languages. In spite of her interest, Nagiko dislikes Jeromes handwriting and orders him out. Jerome totally shocks Nagiko, however, when he asks her to teach him, offering her to write on his body. Opening his shirt, he offers Nagiko to "Use my body like the pages of a book. Of your book!". Nagiko has never considered this aspect in her desires before: her lovers always write on her body. When she backs out and runs, Jerome laughs at her.

Frightened but very intrigued by Jeromes suggestion, Nagiko has several one-night stands in which she experiments writing on their bodies. One of the activists, admirer Hoki (Yutaka Honda), a Japanese photographer who adores her, begs Nagiko to take him as a lover. She explains she cant, as his skins no good for writing: whenever she writes on him, the ink smears and runs. Hoki, not wanting Nagiko to keep carrying on like she is, suggests she try writing a book, offering to take it to a renowned publisher he freelances for. Nagiko likes this idea and writes her first book.

Nagikos book is returned, being told the book is "not worth the paper its written on!". Insulted, Nagiko follows the address on the envelope to confront the publisher. Nagiko is shocked to discover that the publisher who rejected her work is in fact Yaji-san, her fathers old publisher. Whats more, the publisher has a young lover: Jerome.

Devising a plan, Nagiko decides that she will get to the publisher through Jerome. Meeting up with Jerome again, Nagiko discovers he has learned a few more languages, and his penmanship has greatly improved. Nagiko and Jerome spend several weeks exploring this, writing on each other and making love. Nagiko soon realises that, in Jerome, she has found the perfect lover she has been searching for: the partner with whom she can share her physical and her poetic passion, using each others bodies as tablets for their art.

===Writing of books 1 – 6===
Nagiko tells Jerome the truth and the whole story with the publisher. Jerome comes up with an idea: Nagiko will write her book on Jeromes body and Jerome will take it to the publisher. Nagiko loves the idea, and writes Book 1: The Book of The Agenda, in intricate characters of black, red, and gold, on Jerome, keeping her identity  s copy down the text.

After telling Nagiko of the plans success, Jerome tells Nagiko that hell return to her as soon as the publisher, who was extremely aroused by the experience, lets him go. However, during his time with the publisher, Jerome appears to lose track of time and doesnt return to Nagiko. Nagiko, jealous, impatient, and angry, searches for Jerome, eventually finding him making love with the publisher. Nagiko takes this as rejection and betrayal of the worst kind, and immediately plots revenge.

On two   and Book 3: The Book of the  /  is delivered by a boorish, fat, hyperactive American (Tom Kane; who was actually more interested in Hoki than Nagiko).

Nagikos revenge is a success. Jerome is furiously jealous, and comes to Nagikos home to confront her. Nagiko refuses to meet him, however, and wont let Jerome in. Jeromes outrage soon turns to desperation as he begs her to talk to him, but she wont.

Jerome sinks into deep depression and meets with Hoki at the Cafe Typo, desperate to find a way to get Nagiko to forgive him. Hoki suggests that he "scare" Nagiko by faking suicide, similar to the fake death scene in Romeo and Juliet and gives Jerome some pills.
 the book of observations.

The plan is a success: when Nagiko returns home and finds Jerome, she rushes to him, eager to renew their relationship and continue their plans. However, the plan has worked too well: Jerome has overdosed on the pills and is dead. Nagiko is devastated, and realises how much she loved him. On his dead body, Nagiko writes Book 6: The Book of the Lovers.

At Jeromes funeral, his mother (Barbara Lott), a snobbish, upper-class woman, tells Nagiko that Jerome always loved things that were "fashionable". When she suggests that was probably why Jerome loved Nagiko, Nagiko strikes her.

After the funeral, the publisher secretly exhumes Jeromes body from the tomb and has Jeromes skin, still bearing the writing, flayed and made into a grotesque pillow book of his own. Nagiko, now back in Japan, learns of the publishers actions and becomes distraught and outraged. She sends a letter to the publisher, still keeping her identity a secret, demanding that particular book from the publishers hands in exchange for the remaining books. The publisher, now obsessed with his mysterious writer and her work, agrees.

===Writing of books 7 – 13===
Nagiko, now pregnant with Jeromes child, writes Book 7: The Book of The   is delivered as a series of photographs. A young  s written on all his "secret" spots: in between his fingers and toes, the insides of his thighs, etc.; the book is presented in the form of riddles. When the next messenger ( .

The activists protests come to an end when their truck hits a young wrestler ( ed, right outside the publishers office. The next messenger (Masaru Matsuda) simply drives by the office, giving little time to copy down Book 12: The Book of False Starts.

Finally, Book 13: The Book of the Dead arrives on the body of a Sumo wrestler (Wataru Murofushi). In the book writing on the body of the messenger, which the publisher carefully reads, Nagiko finally reveals her identity, confronting the publisher with his crimes: blackmailing and disgracing her father, "corrupting" her husband, as well as Jerome, and what hes done to Jeromes corpse. The publisher, greatly shamed and humbled by being confronted with his guilt, hands the pillow book made of Jeromes skin to the messenger, then has the messenger slit his throat.  
      
Upon recovering the book made out of Jeromes skin, Nagiko buries it under a Bonsai tree and life goes on. She has given birth to Jeromes child (Hikari Abe), and is shown in the epilogue writing on her childs face, like her father used to do when she was young, and quoting from her own pillow book. It is now Nagikos 28th birthday.

Nagikos bi-cultural heritage plays a key role in this film. As a half-Chinese and half-Japanese woman, Nagiko navigates her dual cultures through physical and psychological exploration. Greenaway portrays this exploration subtly by mixing and switching Asian iconography.

==Cast==

*Vivian Wu ...  Nagiko
*Yoshi Oida ...  The Publisher
*Ken Ogata ...  The Father
*Hideko Yoshida ...  The Aunt / The Maid
*Ewan McGregor ...  Jerome
*Judy Ongg ...  The Mother
*Ken Mitsuishi ...  The Husband
*Yutaka Honda ...  Hoki
*Barbara Lott ...  Jeromes Mother
*Miwako Kawai ...  Young Nagiko
*Lynne Langdon ...  Jeromes sister (as Lynne Frances Wachendorfer)
*Chizuru Ohnishi ...  Young Nagiko
*Shiho Takamatsu ...  Young Nagiko
*Aki Ishimaru ...  Young Nagiko

==Reception==

The film was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.   Andrew Johnston (critic) stated: "Most of Greenaways signature visual devices (elaborate title cards, superimposed images) are employed here; but accompanied by U2 songs and traditional Asian music, instead of a Michael Nyman score, they seem fresher and more dynamic than before. The actors are required to submit completely to Greenaways mechanics, but there isnt one bad performance. McGregor and Oida (as a venal publisher) are especially fine." 

==Soundtrack==
*Autopsia-Colonia CD Including main theme from The Pillow Book film, Staalplaat 

*"Offering to the Saviour Gompo", Performed by Buddhist Lamas & Monks of the Four Great Orders, Courtesy of Lyrichord Disks New York
 
*"A Buddhist Prayer", Performed by Buddhist Lamas & Monks of the Four Great Orders, Courtesy of Lyrichord Disks New York

*"Invocations of Gompo", Performed by Buddhist Lamas & Monks of the Four Great Orders, Courtesy of Lyrichord Disks New York

*"Ranryo Ou", Court music of Japan, Performed by Tokyo Gakuso, Courtesy of Victor Entertainment

*"Nasori", Court music of Japan, Performed by Tokyo Gakuso, Courtesy of Victor Entertainment

*"Manzairaku", Court music of Japan, Performed by Tokyo Gakuso, Courtesy of Victor Entertainment

*"Wedding Song", Performed by A Village Ensemble, Aqcha, Afghanistan, Courtesy of Topic Records Lrd

*"Blonde", Performed by Guesch Patti & E. Daho, Courtesy of EMI Music Publishing France SA

*"La Marquise", Performed by Guesch Patti & Dimitri Tikovoi, Courtesy of EMI Music Publishing France SA

*"La Chinoise", Performed by Guesch Patti & Dimitri Tikovoi, Courtesy of EMI Music Publishing France SA

*"Taimu-Mashin no nai Jidai", Performed by Cawai Miwako, Courtesy of Fun house Publishers, Inc

*"Daddys Gonna Pay For Your Crashed Car", Written by U2, Performed by U2, Courtesy of Polygram International

*"Sinfonia Concertante in A Fur Violine, Viola, Violoncello und Orchester", Written by Wolfgang Amadeus Mozart, reconstructor Shigeaki Saegusa, Performed by Mozarteum Orchester Salzburg, conductor Hans Guraf, Courtesy of May Music, Japan

*"Valse", extract from The Frist String Quartet "La Théorie", Written by Walter Hus, Performed by Quadro Quartet, Courtesy of Het Gerucht / Uncle Dans

*"Je suis la resurrection", Performed by Autopsia, Courtesy of Hypnobeat Records

*"Ai no Meguriai", Performed by Judy Ongg, Courtesy of Nichion

*"Qui Tolis", Extract from "Rome", Written by Patrick Mimran, Performed by James Bowman, Courtesy of Wisteria Publishing, Amsterdam

*"Rose, Rose, I Love You", Performed by Yao Lee, Courtesy of EMI SE Asia Ltd

*"Teki Wa Ikuman", Performed by Ichjiro Wakahar, Courtesy of King Records

*"Aiba Shingun-ka", Performed by Hachiro Kasuga, Courtesy of King Records

*"Chicken Bandit-The-Blistered-Corn", Performed by Lam Man Chun and Eric Tsang, Courtesy of New Melody Publishing/Bird and Child Ltd

*"Suiren", Performed by Yasuaki Shimizu, Courtesy of Nippon Columbia Berlin

== See also ==
* Body painting Nudity in film (East Asian cinema)
* Peter Greenaway

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 