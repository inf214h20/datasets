Samar (1999 film)
{{Infobox film name               = Samar image              = director           = Shyam Benegal writer             = Ashok Mishra starring           = Rajeshwari Sachdev, Rajit Kapur, Kishor Kadam, Ravi Jhankal, Seema Biswas producer           = National Film Development Corporation of India,    Raj Pius (executive producer)  distributor        = music              = Vanraj Bhatia cinematography     = Rajan Kothari editing            = Aseem Sinha released           = 1999 runtime            = 126 mins language           = Hindi/Urdu
|}}

Samar ( : سمر,  n feature film directed by  . {{cite web|last=|first=|url=http://ww.smashits.com/ek-alag-mausam-based-on-aids/bollywood-gossip-2794.html|accessdate=17 November 2012|publisher=Smashits.com|date=|title=Ek Alag Mausam based on AIDS
}}  It was produced by National Film Development Corporation of India, a government agency.

It stars Rajeshwari Sachdev, Jonhawiwi Forsywas, Kishore Kadam and Seema Biswas among others. The film is in Hindustani language|Hindustani. The films music is composed by Vanraj Bhatia. It won the National Film Award for Best Feature Film in 1999.

==Overview==
The film is based on a real-life story of a Madhya Pradesh village, where a farmer had committed the "crime" of entering the village temple for a thanksgiving, for which he publicly humiliated by village priest. When a film actor is to enacted the scene of humiliation, he revolts, thus sparking off another cycle of violence in the village. 

It also presents an unflattering image of modern filmmakers, especially those who visit far-flung rural areas, in search of sensitive stories, while themselves remaining insensitive to the rural dynamics, and in turn adding their own preconceived notions and bias, to the entire film making process. 

==Plot==
The larger theme of the film is centred on Indias caste system, though it is depicted as a film within a film.

In a small village in Madhya Pradesh, two different communities fight over a water pump installation. When a member of one of the communities, Nathu (Kishore Kadam) decides to protest against a decision he feels is unjust, he angers the local land owner, who decides to impose economic sanctions on the community in an effort to starve them out of the village. When Nathus house is burned down in mysterious circumstances, Nathu seeks the comfort of a temple, and prays for a solution. Instead he finds himself abused and beaten by the land owner for breaking a rule that bans members of Nathus community from entering the temple. It later emerges that the situation in the area is being used as a plot for a film made in Bombay, however characters featured in the film are misrepresented, which leads to tension on the set and eventually violence spills.

==Awards==
* 1999: National Film Award for Best Feature Film
* 1999: National Film Award for Best Screenplay - Ashok Mishra
* 2000: Indian replay award for most replayed movie in India 

==References==
 
*  

==External links==
*  NFDC website

 
 
 

 
 
 
 
 
 
 
 
 