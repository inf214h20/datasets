Pratheeksha
{{Infobox film
| name = Pratheeksha
| image =
| caption =
| director = Chandrahasan
| producer = Sreekumar Vijayakumar
| writer = Sandya Chandrahasan (dialogues)
| screenplay = Chandrahasan Madhu Mohan Ambika
| music = Salil Chowdhary
| cinematography = Jayanan Vincent
| editing = K Venkattaraman
| studio = Thakshasila Films
| distributor = Thakshasila Films
| released =  
| country = India Malayalam
}} 
 1979 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by Salil Chowdhary.    

==Cast== 
   Madhu 
*Mohan Sharma 
*Adoor Bhavani  Ambika 
*MG Soman 
*Vidhubala 
 
 
==Soundtrack== 
The music was composed by Salil Chowdhary and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aathirappoo || K. J. Yesudas || ONV Kurup || 
|- 
| 2 || Kochu swapnangal || S Janaki || ONV Kurup || 
|- 
| 3 || Nerukayil nee || S Janaki || ONV Kurup || 
|- 
| 4 || Ormakale || K. J. Yesudas || ONV Kurup || 
|} 

==References== 
  

==External links== 
*   

  
  
 
 