Desert Dancer
{{Infobox film
| name           = Desert Dancer
| image          = Desert Dancer.jpg
| caption        = film poster
| director       = Richard Raymond
| producer       = {{Plainlist|
* Izabela Miko
}}
| writer         = Jon Croker
| starring       = {{Plainlist|
* Reece Ritchie
* Freida Pinto
* Nazanin Boniadi
* Tom Cullen
* Marama Corlett
* Akin Gazi
}}
| music          = Benjamin Wallfisch
| cinematography = Carlos Catalan
| editing        = 
| studio         = 
| distributor    = Relativity Media
| released       =  
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = $6 million
| gross          = $326,718 
}}
Desert Dancer is a 2014 biographical film directed by Richard Raymond. It tells a true story of  a young, self-taught dancer in Iran, Afshin Ghaffarian, who risked his life for his dream to become a dancer despite a nationwide dancing ban. 

==Plot==
Set in Iran, the story follows the ambition of Afshin Ghaffarian. During the volatile climate of the 2009 presidential election, Afshin and some friends (including Elaheh played by Freida Pinto) risk their lives and form an underground dance company. The group learned the dancing from videos of  Michael Jackson, Gene Kelly and Rudolf Nureyev even though the online videos are banned. Afhsin and Elaheh also learn much from each other and learned how to embrace their passion for dance and for one another. 

==Cast==
{| class="wikitable sortable"
|-
! Actor !! Role
|-
| Reece Ritchie || Afshin Ghaffarian
|-
| Freida Pinto || Elaheh
|-
| Nazanin Boniadi|| Parisa Ghaffarian
|-
| Tom Cullen|| Ardavan
|-
| Marama Corlett || Mona
|-
| Simon Kassianides|| Sattar
|-
| Akin Gazi || Farid Ghaffarian
|-
| Tolga Safer || Stephano
|-
| Makram Khoury|| Mehdi
|-
| Joshan Ertan|| Voice of Young Afshin
|}

==Filming== Akram Khan, whose work featured in the Olympic opening ceremony at London 2012. Richard Raymond is producing the film through his May 13 Films banner. Freida Pinto undertook an intensive training schedule for the role that comprised eight hours of dance rehearsals every day for 14 weeks.  Photo call for the film was held at the Palais des Festivals during the third day of the 65th annual Cannes Film Festival in Cannes, France on Friday May 18, 2012.   Relativity Media has acquired North America distribution rights to the film   at 2013 American Film Market. 

==Release==
Desert Dancer premiered in Germany on July 3, 2014.  It opened the Ischia Global Film and Music Fest in Ischia, Italy on July 13, 2014.  The film premiered in Hong Kong on August 28, 2014 and in the United States on April 10, 2015. 

==See also==
*List of biographical films

==References==
 

==External links==
*  
*  

 
 
 