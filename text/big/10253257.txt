Lani Loa – The Passage
{{Infobox film 
| name = Lani Loa - The Passage
| image = Laniloa.jpg
| image_size =
| caption = Italian DVD release poster (MEDUSA)
| director = Sherwood Hu
| producer = John P. Marsh Sherwood Hu Francis Ford Coppola (Exec. Producer) Tom Luddy (Exec. Producer)
| writer = John P. Marsh
| starring = Angus Macfadyen Ray Bumatai Carlotta Chang Chris Tashima
| narrator =
| music = Frank Fitzpatrick Hou Yong
| editing = Nicholas C. Smith
| production company =
| released = 1998
| runtime = 89 min.
| country = United States, China English
| preceded_by =
| followed_by =
}}

("Lani Loa - The Passage" is the correct title to this film.  It has been mis-identified on IMDb.com as "Lanai-Loa")

 1998 film directed by Sherwood Hu, executive produced by Francis Ford Coppola, about a woman murdered on her wedding day in Hawaii who comes back to haunt her murderers. The film stars Angus Macfadyen, Ray Bumatai, Carlotta Chang and Chris Tashima.  It was the first film from Francis Ford Coppola|Coppolas and Wayne Wangs Chrome Dragon Films, a short-lived film company that was to specialize in utilizing Asian talent on American-financed projects.  Set in Hawaii, the film was shot in Hilo, Hawaii and in China, in Hainan and Shanghai.

It screened at the  , as Lani-Loa (Hawaiian Ghost Story).

==Cast==
(Main players)
*Angus Macfadyen as Turner
*Ray Bumatai as Hawaiian Kenny
*Carlotta Chang as Jenny
*Chris Tashima as Bong
*Leonelle Akana as Auntie Wana

==See also==
*List of ghost films

==External links==
*  

 

 
 
 
 
 
 
 
 


 