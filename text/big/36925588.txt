Johnny Allegro
 
{{Infobox film
| name           = Johnny Allegro
| image          = Johnny Allegro poster.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Ted Tetzlaff
| producer       =
| writer         = 
| narrator       =
| starring       = George Raft
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = 1949
| runtime        = 
| country        = United States
| language       = English}}
 
Johnny Allegro is a black and white 1949 American film noir, starring George Raft, Nina Foch, Will Geer, and George Macready. An ex-gangster (Raft) working as a federal agent runs afoul of a crime lord (Macready) who enjoys hunting humans for sport. The picture was directed by Ted Tetzlaff and produced by Columbia Pictures. Everett Aaker. The Films of George Raft, McFarland & Company, 2013, pg 138.  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 

 