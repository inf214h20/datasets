Ça ira - Il fiume della rivolta
{{Infobox film
 | name = Ça ira - Il fiume della rivolta
 | image = Cairailfiumedellarivolta.jpg
 | caption =
 | director = Tinto Brass
 | writer = Tinto Brass Gian Carlo Fusco Franco Arcalli
 | starring = 
 | music =  Romolo Grano
 | cinematography = Bruno Barcarol
 | editing =  
 | producer = Zebra Film
 | distributor =
 | released = 3 December 1964	
 | runtime = 85 mins
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} Italian collage film of documentary film and drama film genres directed by Tinto Brass. Taking its name from the popular revolutionary song Ça ira, the film is a critical narrative of 20th century revolutions from 1900 to 1962 and their legacy.

The first film directed by Brass, Ça ira - Il fiume della rivolta was produced in 1962 but it could be premiered at the Venice Film Festival in September 1964, to see theatrical release in December. 

==Narration/Voice==
*Enrico Maria Salerno
*Sandra Milo
*Tino Buazzelli

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 