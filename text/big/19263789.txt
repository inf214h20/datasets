Lion's Den (1988 film)
{{Infobox film
| name           = Lions Den
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Bryan Singer John Ottman
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Ethan Hawke Brandon Boyce Dylan Kussman David Conhaim
| music          = 
| cinematography = 
| editing        = John Ottman
| studio         = 
| distributor    = 
| released       =  
| runtime        = 25 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Lions Den is a 1988 short film directed by Bryan Singer (X-Men) and editor/composer John Ottman. It marks Singers first film as a director.  The film is about five men who meet up at their old hang-out spot after finishing their first semester of college. The film is 25 minutes in length. Ethan Hawke, who had known Bryan Singer as a child in New Jersey, agreed to star in it at the same time he was filming Dad (film)|Dad with Jack Lemmon. As well as co-directing, John Ottman.  also edited the film.

==Cast==
*Ethan Hawke
*Brandon Boyce
*Dylan Kussman  
*David Conhaim

==References==
 

==External links==
* 

 

 
 
 
 