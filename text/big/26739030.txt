Utah (film)
{{Infobox film
| name           = Utah
| image_size     =
| image	=	Utah FilmPoster.jpeg
| caption        = John English
| producer       = Donald H. Brown (associate producer)
| writer         = Betty Burbridge (story) John K. Butler (writer) Jack Townley (writer) Gilbert Wright (story)
| narrator       =
| starring       = See below
| music          = William Bradford
| editing        = Harry Keller
| distributor    =
| released       = 1945
| runtime        = 77 minutes (original version) 54 minutes (edited version)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 John English.

==Plot==
Misunderstanding what her ranch is worth, Dorothy Bryant sells the land for far less than its value, so its up to Roy to somehow get it back.

== Cast ==
*Roy Rogers as Roy Rogers Trigger as Trigger, Roys Horse
*George Gabby Hayes as Gabby Wittaker
*Dale Evans as Dorothy Bryant Peggy Stewart as Jackie (Dorothys friend)
*Beverly Lloyd as Wanda - Bobs girl friend
*Jill Browning as Babe (Dorothys showgirl friend)
*Vivien Oakland as Stella Mason
*Grant Withers as Ben Bowman
*Hal Taliaferro as Steve Lacy Jack Rutherford as Sheriff McBride
*Emmett Vogan as Chicago Police Chief
*Bob Nolan as Bob
*Sons of the Pioneers as Cowhands / Musicians

== Soundtrack == Tim Spencer)
* Sons of the Pioneers - "Five Little Miles" (Written by Bob Nolan) Charles Henderson)
* Dale Evans - "Thank Dixie For Me" (Written by Dave Franklin)
* Roy Rogers and the Sons of the Pioneers - "Utah Trail" (Written by Bob Palmer) Glenn Spencer)
* The Sons of the Pioneers - "Welcome Home Miss Bryant" (Written by Ken Carson)
* Roy Rogers - "Wild and Wooly Gals From Out Chicago Way" (Written by Tim Spencer)

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 