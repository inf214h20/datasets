The Man from U.N.C.L.E. (film)
 
{{Infobox film
| name           = The Man from U.N.C.L.E.
| image          = The Man from U.N.C.L.E. poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Guy Ritchie
| producer       = {{Plainlist|
* Steve Clark-Hall
* Jeff Kleeman John Davis Lionel Wigram
}}
| screenplay     = {{Plainlist|
* Lionel Wigram
* Guy Ritchie
}}
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Henry Cavill
* Armie Hammer
* Alicia Vikander
* Elizabeth Debicki
* Hugh Grant
}}
| music          = Daniel Pemberton John Mathieson
| editing        = {{Plainlist|
* James Herbert
}}
| studio         = {{Plainlist|
* Davis Entertainment Wigram Productions
}}
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $75 million   
| gross          = 
}}
 action comedy Lionel Wigram, MGM The television series of the same name created by Sam Rolfe. The film stars Henry Cavill, Armie Hammer, Elizabeth Debicki, Alicia Vikander, and Hugh Grant. The film will be released on August 14, 2015.

== Synopsis ==
Set against the backdrop of the early 1960s period of the Cold War, The Man from U.N.C.L.E. centers on U.N.C.L.E. agents Napoleon Solo and Illya Kuryakin. The two team up on a joint mission to stop a mysterious international criminal organization, which is bent on destabilizing the fragile balance of power through the proliferation of nuclear weapons and technology. The duo’s only lead is the daughter of a vanished German scientist, who is the key to infiltrating the criminal organization, and they must race against time to find her and prevent a worldwide catastrophe.

== Cast ==
* Henry Cavill as Napoleon Solo 
* Armie Hammer as Illya Kuryakin 
* Hugh Grant  as Alexander Waverly|Mr. Waverly
* Elizabeth Debicki as Victoria Vinciguerra
* Alicia Vikander  as Gaby Teller
* Jared Harris  as Saunders
* Luca Calvani  as Alexander
* Simona Caparrini  as Contessa

== Production ==

=== Development === John Davis Jim and John Thomas, Jackie Brown instead. The Man from U.N.C.L.E. continued to labor in development hell with directors Matthew Vaughn and David Dobkin.  Steven Soderbergh was attached to direct Scott Z. Burnss screenplay, with production slated to begin in March 2012. Executives from Warner Bros. wanted the budget to stay below $60 million, but Soderbergh felt that amount would not be adequate to fund the 1960s-era sets and props and international settings required in the film.  Emily Blunt was nearly cast as the female lead,  but Soderbergh departed in November 2011. 

Guy Ritchie signed on in March 2013.    On July 31, 2013, it was announced that Ritchies adaptation would start filming in September 2013 in London and Italy.  

=== Casting ===
In November 2010,  .   British actor   was cast as the films main villain, Alexander.    Simona Caparrini was cast to play Contessa.   

=== Filming === Goodwood Motor Racing Circuit in West Sussex, UK.

=== Music ===
On July 17, 2014, Daniel Pemberton was hired to score the music for the film. 

== Release ==
The film was previously set for a January 16, 2015, release,  but on August 12, 2014, Warner Bros moved back the films release date to August 14, 2015. 

According to the trailer, which Released on February 2015, the year is 1963. An unknown criminal organization with ties to former Nazis” is said to have built an atomic bomb. This forces the United States and Soviet Union to cooperate — even to the point of assigning Solo and Kuryakin, shown here as being foes, to work together. 
===Marketing===
According to  , the films trailer was supposed to be shown before  .  However, the trailer premiered online several days after, on February 11th.  

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 