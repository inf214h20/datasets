Ambush at Cimarron Pass
{{Infobox Film
| name           = Ambush at Cimarron Pass
| image          = ambush_at_cimarron_pass_poster.jpg
| image_size     = 
| caption        = Movie poster
| director       = Jodie Copelan
| producer       = Herbert E. Mendelson	 
| writer         = Robert A. Reeds (Story) Robert E. Woods (Story) John K. Butler Richard G. Taylor
| starring       = Scott Brady Margia Dean Clint Eastwood Irving Bacon  Frank Gerstle Baynes Barron William Vaughn
| music          = Paul Sawtell Bert Shefter John M. Nickolaus, Jr.
| distributor    = 20th Century Fox
| released       = March 1958 (United States) December 8, 1961 (Finland) October 30, 1972 (Sweden)
| runtime        = 73 minutes
| country        = United States
| language       = English
}} Western film directed by Jodie Copelan, starring Scott Brady and Clint Eastwood (third billed, later first billed upon reissue). The film also features Margia Dean, Irving Bacon, Frank Gerstle, Baynes Barron, and William Vaughn. It was released on DVD and Blu-ray on September 24, 2013. It is the first time it has been released on any home video format, and is the only feature film ever directed by Copelan, who was primarily a film editor.

==Plot==
Eastwood appears as a Southern cowboy Keith Williams who is upset over having to join up with a group of Yankees who have been attacked by the same group of Indians.  Most film guides include in their entry for this film a quote attributed to Eastwood, "probably the lousiest Western ever made."  This film is also notable for a scene in which Brady beats Eastwood in a fistfight.

==Cast==
*Scott Brady	 ... 	Sergeant Matt Blake
*Margia Dean	... 	Teresa Santos
*Clint Eastwood	... 	Keith Williams
*Irving Bacon	... 	Judge Stanfield
*Frank Gerstle	... 	Capt. Sam Prescott
*Ray Boyle	... 	Johnny Willows (billed as Dirk London)
*Baynes Barron	... 	Corbin the Gunrunner
*William Vaughn	... 	Henry the Scout
*Ken Mayer	... 	Corporal Schwitzer
*John Damler	... 	Private Zach
*Keith Richards	... 	Private Lasky
*John Frederick	... 	Private Nathan (billed as John Merrick)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 