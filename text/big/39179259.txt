Kadavul
{{Infobox film
| name           = Kadavul
| image          = 
| image_size     =
| caption        = 
| director       = Velu Prabhakaran
| producer       = H. Anraaj
| writer         = Velu Prabhakaran
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Velu Prabhakaran
| editing        = Nandamuri Haribabu
| distributor    =
| studio         = Deccan Pictures PVT. LTD.
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil anti-theist Arun Pandiyan, Mansoor Ali Roja in lead roles. The film, produced by H. Anraaj, had musical score by Ilaiyaraaja and was released on 5 December 1997. Despite having controversial themes, the film was well received.    {{cite web|url=http://www.behindwoods.com/tamil-movie-news/july-07-03/19-07-07-velu-prabhakaran.html|title=
‘Manaivikku nadanda thirumanam’|date=2007-07-19|accessdate=2013-04-21|publisher=behindwoods.com}} 

==Plot==

Parvathi (Roja (actress)|Roja), a young woman, is a goddess believer while her father (V. Gopalakrishnan) is the priest of a temple. After a quarrel between two castes during a Hindu festival, the temple was closed off. Five years later, the temple reopens.

Muthu (Rahul) and Thenmozhi (Nandhini) are in love but they are not from the same caste. Velusamy (Karikalan), Thenmozhis uncle, finds out their love and forces her to marry him.
 Mansoor Ali Arun Pandian). Thamizharasan fights against injustice and is a revolutionary. Angry, Thamizharasan tries to kill him but fails and eventually steals his money. In the process, Thamizharasan is injured and the prostitute Shenbagam (Rupasri) takes care of him.

Rajapandi (Velu Prabhakaran), an atheist, propagates the non-existence of God. One day, he saves Parvathi from her uncle K. Shanmugam and gets married with her accidentally. Soon, Rajapandi gets beaten by the devotees. In angry, Rajapandi challenges God to become a human and understand the humans, and God (Manivannan) appears as a human.

==Cast==

*Velu Prabhakaran as Rajapandi
*Manivannan as God Arun Pandian as Thamizharasan Mansoor Ali Khan as K. Shanmugam Roja as Parvathi
*Rahul as Muthu
*Nandhini as Thenmozhi
*V. Gopalakrishnan as Parvathis father
*Karikalan as Velusamy
*Naga Kannan
*Roopa Sri as Shenbagam
*J. Lalitha as Thenmozhis mother
*Shakeela
*Vincent Roy
*Suryakanth
*Meesai Murugesan as Naidu

==Soundtrack==

{{Infobox album |  
| Name        = Kadavul
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1997
| Recorded    = 1997 Feature film soundtrack |
| Length      = 30:58
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1997, features 6 tracks with lyrics written by Vaali (poet)|Vaali, Pulamaipithan and Ponnadiyan.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aadi Sivan Tholil Irukkum || Bhavatharini || 5:40
|- 2 || Ariviruntha Konjam || Malaysia Vasudevan || 4:51
|- 3 || Enakku Oru Raasa || Amudha || 5:08
|- 4 || Kaathalai || P. Unni Krishnan, Sujatha Mohan || 5:07
|- 5 || Kovilukku Irukkum || K. S. Chithra || 4:52
|- 6 || Poovarasan Poove || Arunmozhi, Sujatha Mohan || 5:20
|}

==References==
 

==External links==
* 

 
 
 
 
 