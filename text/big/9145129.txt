Thunder in the Sun
{{Infobox film
| name           = Thunder in the Sun
| image_size     = 
| image	=	Thunder in the Sun FilmPoster.jpeg
| caption        = 
| director       = Russell Rouse
| producer       = Clarence Greene
| writer         = Russell Rouse Stewart Stern
| based on = story by James Hill Guy Trosper
| narrator       =  Jeff Chandler
| music          = Cyril J. Mockridge
| cinematography = Stanley Cortez Chester W. Schaeffer
| studio = Seven Arts Productions Carrolton Inc
| distributor    = Paramount Pictures
| released       = April 8, 1959
| runtime        = 81 min.
| country        = United States
| language       = English
| budget         = 
| gross = $1.8 million (est. US/ Canada rentals) 
| preceded_by    = 
| followed_by    = 
}} 1959 Western western film Jeff Chandler.

==Plot== Basque immigrants pioneering into the Wild West while carrying their ancestral vines. Hard drinking trail driver Lon Bennett is hired to lead them and he falls for the spirited Gabrielle Dauphin.

The film is infamous among Basques for its misunderstandings of Basque customs, such as the use of the xistera (a device of the jai alai sport) as a weapon or shouting irrintzi ululations as meaningful communication. 
Other commentators, though, have noted the well staged action scenes, the absorbing story, and the excellent cinematography.

==Cast==
*Susan Hayward as Gabrielle Dauphin Jeff Chandler as Lon Bennett
*Jacques Bergerac as Pepe Dauphin
*Blanche Yurka as Louise Dauphin
*Carl Esmond as Andre Dauphin
*Fortunio Bonanova as Fernando Christophe
*Bertrand Castelli as Edmond Duquette
*Albert Carrier
*Felix Locher as Danielle
*Michele Marly
*Albert Villasainte
*Veda Ann Borg as Marie

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 