Paris Is Always Paris
 
{{Infobox film
| name           = Paris Is Always Paris
| image          = 
| caption        = 
| director       = Luciano Emmer
| producer       = Giuseppe Amato
| writer         = Luciano Emmer
| starring       = Aldo Fabrizi
| music          = 
| cinematography = Henri Alekan
| editing        = Jacques Poitrenaud Gabriele Varriale
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy France
| language       = Italian French
| budget         = 
}}

Paris Is Always Paris ( ,  ) is a 1951 Italian-French comedy film directed by Luciano Emmer.   

==Cast==
* Aldo Fabrizi - Andrea De Angelis
* Henri Guisol - Monsieur Morand
* Ave Ninchi - Elvira de Angelis
* Jeannette Batti - Claudia
* Hélène Rémy - Christine
* Henri Génès - Paul Gremier
* Marcello Mastroianni - Marcello Venturi
* Lucia Bosé - Mimi de Angelis
* Carlo Sposito - Toto Mancuso (credited as Carletto Sposito)
* Giuseppe Porelli - Raffaele DAmore
* Janine Marsay - Praline
* Galeazzo Benti - Gianni Forlivesi
* Paolo Panelli - Nicolino Percuoco
* Franco Interlenghi - Franco Martini
* Yves Montand - as himself
* Eartha Kitt - uncredited cameo as herself / cabaret singer  
* Roland Lesaffre
* Lisette Lebon
* María Riquelme

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 