The Invasion (film)
{{Infobox film
| name           = The Invasion
| image          = The Invasion film poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Oliver Hirschbiegel James McTeigue   
| producer       = Joel Silver Wachowski brothers   
| based on       =   Jeffrey Wright Veronica Cartwright
| music          = John Ottman
| cinematography = Rainer Klausmann
| editing        = Joel Negron Hans Funck Vertigo Entertainment Warner Bros. Pictures
| released       =  
| runtime        = 99 minutes
| country        = United states 
| language       = English Russian
| budget         = $65 million 
| gross          = $40,170,568   
}} science fiction thriller film starring Nicole Kidman and Daniel Craig, directed by Oliver Hirschbiegel, with additional scenes written by The Wachowskis and directed by James McTeigue.
 of the Body Snatchers.

==Plot== REM sleep. CDC director investigating the crash.

Tuckers ex-wife, psychiatrist Carol Bennell, begins to feel something is amiss when people seem to have "changed". Her patient Wendy Lenk describes how her husband "is not her husband", and one of her sons friends acts detached and emotionless. At a neighbourhood kids party, Carols son Oliver discovers a strange lifeform. The mothers speculate if the organism is in any way connected to the reports of a fast-spreading flu. Carol takes the organism to her doctor friend Ben Driscoll to have it checked. Meanwhile, Tucker uses the CDC to spread the disease further, disguising the spores as flu inoculations.

Ben and Dr. Stephen Galeano, a biologist, discover how the spore takes over the brain during REM sleep. They also find that people who had brain affecting illnesses, such as encephalitis or Acute disseminated encephalomyelitis|ADEM, are immune to the spore because their previous illnesses prevents the spore from "latching on" to the brain matter. Carols son, Oliver, is immune to the spore because of the ADEM he had as a young child. Carol decides to get her son, who might show a way to a cure, back from Tucker. Before she drives to Tuckers house, she joins Bens team who are called to the house of the Belicecs, the Czech ambassador and his wife, in a case of emergency. There they witness the transformation of Yorish, the Russian ambassador and the Belicecs friend.

When Carol arrives at Tuckers house, he and several colleagues close in on her. He explains that the changed humans, devoid of irrational emotions, are offering a better world, and asks her to join them. When Carol resists, he holds her to the ground and infects her by spurting his saliva on her. She escapes and returns to Ben at the Belicecs house. They flee when Belicec returns with more transformed people intent on infecting anyone in the house. Galaneo and one of his assistants head to a base outside Baltimore where they and other scientists attempt to find a cure for the alien virus. Carol and Ben separate to find Oliver, who texts his location, the apartment of Tuckers mother, to Carol.

Finally Ben arrives, but Carol realizes that he too has become one of the infected. He tries to seduce her to give in to the new society, but also frankly states that there is no room for people like Oliver who are immune. Carol shoots him in the leg with a handgun she stole earlier from a transforming policeman, and flees with her son. With the infected closing in on them, Galeano picks them up with an army helicopter at the last second. They head back to the base, where scientists use Olivers blood to create a vaccine.

One year later, most victims of the infection have been cured, having no memory of the events which took place during their illness. Asked by a reporter if he considers the virus to be under control, Galeano replies that a look at the newspaper headlines should be proof enough that humanity acted human again. At her home, Carol helps her son to get ready for school, while Ben, now apparently her partner, reads the morning newspaper. He expresses his dismay about the violence in the world. Carol remembers Yorishs remark that a world without violence would be a world where human beings ceased to be human.

==Cast==
* Nicole Kidman as Carol Bennell
* Daniel Craig as Ben Driscoll
* Jeremy Northam as Tucker Kaufman
* Jackson Bond as Oliver Jeffrey Wright as Dr. Stephen Galeano
* Veronica Cartwright as Wendy Lenk
* Josef Sommer as Dr. Henryk Belicec
* Celia Weston as Ludmilla Belicec
* Roger Rees as Yorish
* Eric Benjamin as Gene
* Susan Floyd as Pam
* Stephanie Berry as Carly
* Alexis Raben as Jill
* Adam LeFevre as Richard Lenk
* Joanna Merlin as Joan Kaufman
* Malin Åkerman as Autumn (Uncredited)
* Jeff Wincott as Transit Cop

==Production== Invasion of the Body Snatchers.  In July 2005, director Oliver Hirschbiegel was attached to helm the project, with production to begin in Edgemere, MD.  The following August, Nicole Kidman was cast to star in the film then titled Invasion, receiving a salary of close to $17 million. Invasion was based on the script by Kajganich, originally intended as a remake of Invasion of the Body Snatchers, but Kajganich crafted a different enough story for the studio to see the project as an original conception.    Kajganich described the story to reflect contemporary times, saying, "You just have to look around our world today to see that power inspires nothing more than the desire to retain it and to eliminate anything that threatens it." The screenwriter said that the story was set in Washington, D.C. to reflect the theme.  In August, Daniel Craig was cast opposite Kidman in the lead.     The film, whose original title Invasion of the Body Snatchers was shortened to Invasion due to Kajganichs different concept, was changed once more to The Visiting so it would not be confused with American Broadcasting Company|ABCs TV series Invasion (TV series)|Invasion. 
 Jaguar that was being towed by a stunt driver and was taken to a hospital briefly.  Kidman broke several ribs, but she was able to get back to work soon after being hospitalized. 
 postmodern style, with atmospheric and thrilling action elements. 

The Invasion was originally intended to be released in June 2006,  but it was postponed to 2007.  The film was released on August 17, 2007 in the United States and Canada in 2,776 theaters. The film grossed $5,951,409 over the opening weekend. The Invasion has grossed $15,074,191 in the United States and Canada and $24,727,542 in other territories for a worldwide gross of $40,170,558 as of March 9, 2008. 

The music in the trailer is called "Untitled 8 (a.k.a. "Popplagið")" by Sigur Rós.

==Reception==
 
The film received generally negative reviews. On review aggregator Rotten Tomatoes, The Invasion rates 19%.  On review aggregator Metacritic, The Invasion received an average score of 45 out of 100. 

Roger Ebert of the Chicago Sun-Times called it "the fourth, and the least, of the movies made from Jack Finneys classic science fiction novel."  Owen Gleiberman of Entertainment Weekly wrote that it was "a soulless rehash...The movie isnt terrible; its just low-rent and reductive."  Joanne Kaufman of The Wall Street Journal added, "With all the shoot-outs, the screaming, the chases, collisions and fireballs, there isnt much time for storytelling." 


== See also ==
*The Puppet Masters

==References==
{{Reflist|2|refs=
   
}}

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 