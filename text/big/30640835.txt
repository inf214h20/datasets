Raspberry Magic
 
{{Infobox film
| name           = Raspberry Magic
| image          =
| alt            = 
| caption        = 
| director       = Leena Pendharkar
| producer       = Megha Kadakia
| writer         = Leena Penharkar
| starring       = Lily Javherpour Meera Simhan Ravi Kapoor Bella Thorne Maulik Pancholy
| music          = Jesse Clark
| cinematography = Jeffrey Chu
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
}} drama film nature or nurture that can make them grow.  Directed by Leena Pendharkar, produced by Megha Kadakia, the movie premiered at the Cinequest Film Festival 
{{cite web
|url=http://www.cinequest.org/media/CQ20Guide.pdf
|title=Mavericks: Official Guide to Cinequest Film Festival 20
|date=7 March 2010
|accessdate=26 January 2011
|page=46}}  and the San Francisco International Asian American Film Festival 
{{cite web
|url=http://filmguide.festival.asianamericanmedia.org/tixSYS/2010/filmguide/Director/PP
|title=Events by director: detail view
|work=2010 San Francisco Asian American Film Festival
|accessdate=26 January 2011}}  in 2010.

==Cast==
* Lily Javaherpour 
* Bella Thorne
* Meera Simhan
* Ravi Kapoor
* Keya Shah James Morrison
* Alison Brie
* Maulik Pancholy
* Randall Batinkoff

==Reception==
Raspberry Magic received generally positive reviews during its festival screenings.  Clinton Stark says that "Raspberry Magic delivers an inspired message about pursuing your dreams, recognizing and appreciating who you are, despite the day-to-day curve balls life might pitch at you," 
{{cite web
|first=Clinton|last=Stark
|url=http://www.starkinsider.com/2010/02/cinequest-review-raspberry-magic.html
|title=Film Review: ‘Raspberry Magic’
|date=12 February 2010 News & Observer called it "a sweet little film that without adornment in style or storytelling evokes the nature of family drama and family love." 
{{cite news
|title=Sweet look at family love
|work=The News & Observer
|first=Adrienne|last=Johnson Martin
|date=1 October 2010}} 

==Awards==
* Audience Award, Best Narrative Feature, Philadelphia Asian American Film Festival 
{{cite web
|url=http://www.phillyasianfilmfest.org/
|title=Philadelphia Asian American Film Festival
|accessdate=26 January 2011}} 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 