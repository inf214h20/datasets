The Ghost Train (1941 film)
 
{{Infobox film
| name           = The Ghost Train
| image          = "The_Ghost_Train"_(1941).jpg
| image_size     =
| caption        = Detail Spanish theatrical poster
| director       = Walter Forde Edward Black
| writer         = Marriott Edgar (dialogue) Val Guest (dialogue) J.O.C. Orton (writer)
| based on       =  
| narrator       =
| starring       = Arthur Askey Richard Murdoch
| music          = Walter Goehr
| cinematography = Jack E. Cox
| editing        = R.E. Dearing
| studio         = Gainsborough Pictures
| distributor    = General Film Distributors
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1923 play of the same name written by Arnold Ridley.   

==Plot== communication cord GWR express train, bringing it to a stop so he can retrieve his hat. Returning to the train, he escapes an angry conductor by ducking into a compartment occupied by attractive blonde Jackie Winthrop (Carole Lynne), whom Gander flirts with. Another passenger, Teddy (Richard Murdoch), has his eye on Jackie as well, but her companion Richard Winthrop (Peter Murray-Hill) ejects both of them from the compartment.
 Herbert Lomas) tells them the last Truro-bound train has gone, and that they cannot remain at the station, as he is locking up for the night. The passengers insist on staying, as it is raining heavily and the nearest village is four miles away.

Hodgkin warns them the station is haunted. A branch line once crossed the river on a swing bridge close to the station. One night 43 years ago, then stationmaster Ted Holmes had a fatal heart attack while attempting to close the bridge, causing a train to plunge into the river. Ever since, a phantom train has been heard periodically on the abandoned track. It is said to kill anyone who looks upon it.

With that, he reluctantly leaves them. As the passengers make themselves as comfortable as they can, they hear footsteps outside. Richard opens the door, and  Hodgkin collapses into the room. Dr. Sterling pronounces him dead. Later, a terrified young woman in black (Linden Travers) appears. She pleads for help, saying that someone is pursuing her. A car spins off the road and crashes into a tree. The driver is unhurt, but his car is damaged. Back in the waiting room, he introduces himself as John Price (Raymond Huntley) and explains that he is searching for his sister Julia, who he says suffers from delusions. Julia protests that he is lying. Price further explains that she thought she had seen the ghost train, and became obsessed with it ever since. The passengers tell him that Hodgkin has died. When Price insists on seeing the body, they discover it has mysteriously vanished.

Price leaves to arrange transportation. Then an approaching train is heard. As it thunders past, Julia smashes a window to look at it, then screams and faints. They hear singing from the nearby railway tunnel mouth. Julia claims that Ben Isaacs (D. J. Williams (actor)|D. J. Williams), the sole survivor of the accident, is coming back. Teddy shoots at the "ghost", causing it to flee back into the tunnel, leaving behind a bloodstained cloth.

Teddy shows the others the cloth and orders the others, at gunpoint, to stay put until the police arrive, but Richard punches him, knocking him out. The passengers carry him to the bus Price has obtained. When Teddy comes to, he is furious with Richard, as now there will be no one to intercept the train on its return journey. When Gander remarks that he had returned the bridge to the open position, Dr. Sterling suddenly orders the bus driver to stop, while his confederate, Price, produces his own gun. Sterling orders the driver to turn back so they can warn the train.

Meanwhile, guns are being loaded aboard the "ghost train"; a very much alive Hodgkin flags the train off and climbs aboard. Teddy explains that the train is really being used by Nazi Fifth Columnists to secretly transport arms. While Price heads down the embankment with Julia and the driver to try to stop the train, Teddy knocks Sterling out and gains control of the situation. The train plunges into the river.

==Cast==
*Arthur Askey as Tommy Gander
*Richard Murdoch as Teddy Deakin
*Kathleen Harrison as Miss Bourne
*Peter Murray-Hill as R. G. Winthrop
*Carole Lynne as Jackie Winthrop
*Morland Graham as Dr. Sterling
*Betty Jardine as Edna
*Stuart Latham as Herbert Herbert Lomas as Saul Hodgkin
*Raymond Huntley as Price
*Linden Travers as Julia Price
*D. J. Williams (actor)|D.J. Williams as Ben Isaacs

==Critical reception==
TV Guide noted, "good for a few laughs and a couple of chilling surprises." 

==Soundtrack==
*Arthur Askey – "The Seaside Band" (Written by Kenneth Blain)  –  (UK DECCA F 9944 10" 78 rpm shellac PICTURE LABELS) 

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 