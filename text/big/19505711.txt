Father of Four and Uncle Sofus
 
{{Infobox film
| name           = Far til fire og onkel Sofus
| image          = Far til fire og onkel Sofus.jpg
| caption        = Film poster
| director       = Alice OFredericks Robert Saaskin
| producer       = Henning Karmark
| writer         = Grete Frische Jon Iversen Alice OFredericks
| narrator       = 
| starring       = Karl Stegger
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = ASA Film
| released       =  
| runtime        = 89 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Father of Four and Uncle Sofus ( ) is a 1957 Danish family film directed by Alice OFredericks and Robert Saaskin.

==Cast==
* Karl Stegger - Far
* Birgitte Bruun - Søs
* Otto Møller Jensen - Ole
* Rudi Hansen - Mie
* Ole Neumann - Lille Per
* Peter Malberg - Onkel Anders / Sofus
* Ib Mossin - Peter
* Agnes Rehni - Naboen Agnes Sejersen
* Preben Kaas - Peters ven
* Einar Juhl - Rektor
* Kirsten Passer - Lærerinde Ludvigsen
* Holger Juul Hansen - Lærer Henry Nielsen - Flypassager
* Grethe Kausland - Lille Grete (as Grete Nielsen)
* Poul Erik Møller Pedersen - Torben
* Bente Svendsen - Birthe
* Jens Thygesen - Bent
* Jan Herdahl - Tage
* Tommy Larsen - Tom
* Mantza Rasmussen - Opsynsmand
* Ole Ishøj - Student
* Erling Dalsborg - Betjent
* Hjalmar Madsen - Tjener
* Asta Hjelm - Lille Gretes mor
* Povl Wøldike - Lille Gretes far

==External links==
* 

 

 
 
 
 
 
 


 