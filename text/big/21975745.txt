Boy Meets Dog
{{Infobox film name = Boy Meets Dog image = Boymeetsdog-title.jpg caption = Title card for Boy Meets Dog| producer = Walter Lantz writer = Gene Byrnes (comic strip) starring = Billy Bletcher released = March 10, 1938 distributor = Universal Pictures (Original)   Castle Films (Home Release) runtime = 9 min.  language = English
}}

Boy Meets Dog is an American animated commercial short made in 1938 for Ipana Toothpaste. It was produced by Walter Lantz as a Technicolor cartoon for theatrical release by Universal Pictures.   However, it did not see theatrical release, but  Castle Films purchased it, and released it to the home movie market.

==Plot==
The story begins with young Bobby and his friends having a sundae at a restaurant, but he refuses to because of his extremely strict and mean father (Billy Bletcher). When returning home, Bobby hears a bark and realizes that a beagle dog is lost. Although he decides to keep him, his father wont let him. Bobby cleans the dog to not make his father disappointed. However, his father is. Bobbys father kicks out his lost dog and then Bobby cries in bed. His father goes back downstairs but then trips over a roller skate and accidentally breaks the vase. He walks back upstairs, realizing Bobby has vanished. He then tries to find him, thinking that he is hiding. Gnomes come to life from the wallpaper and knock him Unconsciousness|unconscious. Then he wakes up seeing that he is in the court. The gnomes tell him that he is a "mental giant" and find him guilty. However, according to this, Bobby has not disappeared but disguised himself as a judge and sentences his father into the youth machine which turns him into an infant, making his life start all over again and be locked up and treated as a baby in the machine forever. To cover up the illusion of his nightmare, the machine feeds him with a bottle and showing that it was the poor lost beagle kissing him. When he wakes up, the dog runs away and then Bobbys father realizes that his son is right. He then starts to lose anger and strictness of his son and joins a gang of kids (including Bobby) and he is now a "Reglar Feller".

==Trivia==

* The part where the three lawyer gnomes sing about how the father is a "mental giant" would later on be used in (If not inspiring) Tech N9nes song "Hes a mental Giant" in his album All 6s and 7s.
*It was originally a commercial for "Ipana Toothpaste" but the scene of the toothpaste ad after Bobbys father wakes up was removed. As of today, the scene was leaked in one of the videos online but the sound was muted for unknown reasons.

==See also==
* List of films in the public domain

==External links==
*   at Internet Movie Database
*  

 
 
 

 