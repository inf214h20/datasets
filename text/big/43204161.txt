Geleya
{{Infobox film
| name = Geleya
| image =
| caption =  Harsha
| writer = Preetham Gubbi
| based on = 
| producer = B Basavaraju  B K Gangadhar A Venkatesh Kadur Umesh 
| starring = Prajwal Devaraj   Tarun Chandra   Kirat Bhattal  Duniya Vijay
| music = Mano Murthy
| cinematography = Krishna (cinematographer)|S. Krishna
| editing = Deepu S. Kumar
| studio  = Sri Mookambika Combines
| released =  
| runtime = 179&nbsp;minutes
| language = Kannada
| country = India
}}
 2007 Indian Kannada language crime romance film directed by Harsha (director)|Harsha, a popular choreographer making his debut in direction. The script is written by Preetham Gubbi and the cinematography is by Krishna (cinematographer)|Krishna; all of whom had worked previously together for the blockbuster film Mungaaru Male.  The film stars Prajwal Devaraj, Tarun Chandra and Kirat Bhattal in the lead roles with Duniya Vijay and Pooja Gandhi appearing in cameo roles. 

The film released on 19 October 2007 across Karnataka and set high expectations for its storyline and the team. However upon release, the film generally met with average reviews from the critics and audience. 

==Plot==
Guru (Prajwal) and Vishwa (Tarun) are the best friends from the same village. They migrate to Bangalore city in search of a job and better living. Out of greed to make quick money, they join the anti-social gang who are threatening the common lives of Bangalore. Coincidently, Guru and Vishwa join the two rival gangs who are constantly against each other. In due course, Guru kills Vishwas boss and this enrages Vishwa who sets vengeance against Gurus boss. Vishwa becomes the leader of the gang and kills Gurus boss eventually. This leads to a massive clash between the two groups and Vishwas wife (Kirat) tries to match up between the old friends but to no avail. A tough cop (Duniya Vijay) gets deployed to handle the case and what happens next forms the crux of the story.

== Cast ==
* Prajwal Devaraj as Guru
* Tarun Chandra as Vishwa
* Kirat Bhattal as Nandini
* Rangayana Raghu as Jayanna Kishore as Don Bhandari
* Bullet Prakash
* Sithara Vaidya
* Ninasam Ashwath
* Mico Nagaraj
* Duniya Vijay in a guest appearance
* Pooja Gandhi in a guest appearance
* Rakhi Sawant in a special appearance

== Soundtrack ==
{{Infobox album  
| Name        = Geleya
| Type        = Soundtrack
| Artist      = Mano Murthy
| Cover       = Geleya audio cover.jpg
| Released    = 11 October 2007
| Recorded    = 
| Caption     = Soundtrack cover Feature film soundtrack
| Length      = 28:33
| Label       = Anand Audio Kannada
| Producer    = Mano Murthy
| Last album  = 
| This album  = 
| Next album  = 
}}

Mano Murthy composed the music for the film and the soundtracks. The album consists of six soundtracks. 

{{tracklist
| headline = Track listing
| lyrics_credits = yes
| total_length = 28:33
| extra_column = Singer(s)
| title1 = Ee Sanje Yaakagide
| lyrics1 = Jayant Kaikini
| extra1 = Sonu Nigam
| length1 = 5:11
| title2 = Nanna Stylu Berene Kaviraj
| extra2 = Rajesh Krishnan, Inchara
| length2 = 5:14
| title3 = Hudugi Malebillu
| lyrics3 = Jayant Kaikini
| extra3 = Karthik (singer)|Karthik, Priya Himesh
| length3 = 4:26
| title4 = Putagala Naduvina
| lyrics4 = Jayant Kaikini
| extra4 = Praveen Dutt Stephen
| length4 = 3:56
| title5 = Chaangu Balaa Changure
| lyrics5 = V. Nagendra Prasad
| extra5 = Shankar Mahadevan
| length5 = 4:31
| title6 = Kanasalle Mathaduve
| lyrics6 = Jayant Kaikini
| extra6 = Shreya Ghoshal
| length6 = 5:15
}}

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 