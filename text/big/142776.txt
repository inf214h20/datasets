The Patent Leather Kid
{{Infobox film
| name           = The Patent Leather Kid
| image          = Poster of the movie The Patent Leather Kid.jpg
| caption        =
| director       = Alfred Santell
| producer       = Alfred Santell ?Richard A. Rowland
| writer         = Rupert Hughes (story)  Winifred Dunn (scenario)
| starring       = Richard Barthelmess
| music          = ?Cecil Copping(*at opening)
| cinematography = Arthur Edeson Ralph Hammeras Alvin Knechtel
| editing        =
| distributor    = First National Pictures
| released       =  
| runtime        = ?130-150 minutes(per IMDb) / 12 reels
| country        = United States
| language       = Silent film (English Intertitles) gross = $1.2 million   accessed 19 April 2014 
}}
 1927 silent Arthur Stone.
 Gerald C. Duffy (intertitle|titles), Winifred Dunn, Casey Robinson (uncredited) and Adela Rogers St. Johns from the story by Rupert Hughes.  It was directed by Alfred Santell.   

It was nominated for the Academy Award for Best Actor (Richard Barthelmess).

==Cast==
* Richard Barthelmess - The Patent Leather Kid
* Molly ODay - Curley Boyle
* Lawford Davidson - Lt. Hugo Breen
* Matthew Betz - Jake Stuke Arthur Stone - Jimmy Kinch
* Ray Turner - Mabile Molasses
* Hank Mann - Sergeant Walter James - Officer Riley
* Lucien Prival - The German Officer
* Nigel De Brulier - The French Doctor
* Fred OBeck - Tank Crew
* Clifford Salam - Tank Crew
* Henry Murdock - Tank Crew
* Charles Sullivan - Tank Crew
* John Kolb - Tank Crew
* Al Alleborn - Tank Crew

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 