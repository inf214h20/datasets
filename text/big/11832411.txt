Magic Fire
{{Infobox film
| name           = Magic Fire
| image          = 
| image size     =
| caption        = 
| director       = 
| producer       =
| writer         =
| narrator       =
| starring       =Yvonne de Carlo
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       = 1955
| runtime        =
| country        = United States English
| budget         =
| gross          = 
}}
  of cast members (from left) Valentina Cortese, Carlos Thompson, Yvonne De Carlo, Alan Badel, and Rita Gam.]] 1955 biographical film about the life of composer Richard Wagner, released by Republic Pictures.

==Overview==
Directed by William Dieterle, the film made extensive use of Wagners actual music, which was arranged by Erich Wolfgang Korngold.  Dieterle worked with Korngold on several Warner Bros. films, including A Midsummer Nights Dream and Juarez (film)|Juarez.  It was one of the final films Republic made in the two-strip color process known as Trucolor. 
 Ludwig IIs patronage of Wagner, without going into much detail about the kings controversial personality.

The film used a very large cast, opulent sets, and lavish costumes.  Since Republic was known primarily for westerns and adventure serials, Magic Fire was one of the rare "prestige" films to be produced by studio chief Herbert Yates.  Nevertheless, critical response was mixed and box office receipts in the U.S. were disappointing. 

==Cast==
*Alan Badel ...  Richard Wagner
*Yvonne De Carlo ...  Minna Planer
*Carlos Thompson ...  Franz Liszt Cosima Liszt
*Valentina Cortese ...  Mathilde Wesendonck
*Peter Cushing ...  Otto Wesendonck
*Frederick Valk ...  Minister von Moll King Ludwig II
*Erik Schumann ...  Hans von Bülow
*Robert Freitag ...  August Roeckel
*Heinz Klingenberg ...  King of Saxonia
*Charles Régnier ...  Giacomo Meyerbeer
*Kurt Großkurth ...  Magdeburg Theatre Manager (as Kurt Grosskurth)
*Fritz Rasp ...  Pfistermeister
*Hans Quest ...  Robert Hubner
*Jan Hendriks ...  Mikhail Bakunin Hans Richter (uncredited)

==Production==
The film was shot in Italy and Germany over 12 weeks. MOVIELAND BRIEFS: Director Finishing Lavish Wagner Film
Los Angeles Times (1923-Current File)   27 Nov 1954: 13.   Yvonne De Carlo Will Narrate Foreign Film; Gobel Manager Irked
Ames, Walter. Los Angeles Times (1923-Current File)   30 Dec 1954: 22.  

==Crew==
*Directed by William Dieterle
*Writing credits: David T. Chantler, screenplay; Ewald André Dupont, screenplay (as E. A. Dupont);
*Bertita Harding, screenplay; based on the novel Magic Fire: The Story of Wagners Life and Music (1954) 
*Produced by William Dieterle
*Original Music by Erich Wolfgang Korngold
*Cinematography by Ernest Haller
*Art Direction by Robert Herlth
*Costume Design by Ursula Maes
*Virgil Hart ....  assistant director
*Frank T. Dyke ....  sound
*Walter Rühland ....  sound (as Walter Ruehland)
*Léo L. Fuchs ....  still photographer (uncredited)
*Stanley E. Johnson ....  supervising editor (as Stanley Johnson)
*Tatjana Gsovsky ....  choreographer
*Rudolf Hartmann ....  opera stager (as Professor Rudolf Hartmann)
*Erich Wolfgang Korngold ....  music supervisor

==Release dates==
It was released in   the United Kingdom on July 15, 1955, and in the United States on March 29, 1956. Other European releases were in Denmark on November 21, 1956, and in Finland on October 18, 1957.

==Footnotes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 