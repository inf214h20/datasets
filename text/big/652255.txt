Mr. Nanny
{{Infobox film
| name           = Mr. Nanny
| image          = MrNanny.jpg
| caption        = DVD cover Michael Gottlieb
| producer       = Robert Engelman
| writer         = Michael Gottlieb Edward Rugoff Robert Gorman Mother Love David Johansen
| music          = David Johansen Brian Koonin Peter Stein
| editing        = Earl Ghaffari Michael Ripps
| studio         = 
| distributor    = New Line Cinema (US)
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = $4.3 million
}}

Mr. Nanny is a 1993 romantic comedy film starring professional wrestler Hulk Hogan.  The working title of the film was Rough Stuff, and David Johansen (who plays the villain) also recorded a song by that name for the film.  

==Plot==
Set in  ) and Kate (Madeline Zima).

As it turns out, Alex and Kate are two highly mischievous brats who vie for their often-too-absent fathers attention by wreaking havoc in the household via elaborate and rather vicious pranks and booby-traps, with their specialty targets being the nannies he has assigned to take care of them (Alex Sr. is a widower). Sean witnesses the last nanny jumping into the fountain in front of the house to extinguish the fire in her hair. However, their father proves to be either too distracted or too lenient, which causes the children to continue their schemes. Thinking that he is a new (albeit unusual) replacement, they find a new target in Sean. But after one prank too many, which involves a swimming pool full of red dye ("the Pit of Blood"), Sean finally exerts his authority and not only gets to quiet Alex and Kate down, he also manages to open the eyes of their father to his family problems.

However, the real trouble has just started. The unscrupulous and vain Tommy Thanatos (  of which he was so proud.

Thanatos kidnaps Alex Sr. with the help of Frank Olsen (Raymond OConnor), the corrupt security chief of Mason Systems (who is disposed of en route to the hideout), and demands of him to hand over the chip. When Alex Sr. (who stowed the chip in Kates doll) refuses, Thanatos has Alex and Kate kidnapped in order to force him to comply. Despite a valiant effort, Sean is overpowered and Burt is taken as well, giving Thanatos an unexpected revenge bonus. But Sean manages to track down Thanatos, and with the help of his friends is able to beat the villains. As Thanatos prepares to charge Sean, Alex Sr. and the children activate an improvised electromagnet to launch him into space, leaving only his skullplate.

The movie ends with Sean preparing to take a leave of absence from the Masons. But Alex and Kate intend to have him back much sooner - and therefore Sean falls victim to yet another prank.

==Cast==

*Hulk Hogan - Sean Armstrong
*Sherman Hemsley - Burt Wilson
*Austin Pendleton - Alex Mason, Sr.
*Madeline Zima - Kate Mason Robert Gorman - Alex Mason, Jr. 
*Raymond OConnor - Frank Olsen
*Afa Anoaʻi - Himself
*Brutus Beefcake - Himself George "The Animal" Steele - Himself
*Mother Love  - Corinne
*David Johansen - Tommy Thanatos

==Reception==
The film grossed $4.3 million at the box office.
The film has been poorly received by critics, it currently holds a 7% score on Rotten Tomatoes.    

==Other information==
*Madeline Zima went on to star in the sitcom, The Nanny.
*The movie The Pacifier starring Vin Diesel has a similar storyline. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 