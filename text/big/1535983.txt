Eight Men Out
{{Infobox film
| name           = Eight Men Out
| image          = Eight_Men_Out_DVD_cover.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = John Sayles
| producer       = Sarah Pillsbury
| screenplay     = John Sayles
| based on       =   Michael Lerner Christopher Lloyd Charlie Sheen David Strathairn D.B. Sweeney
| music          = Mason Daring Robert Richardson
| editing        = John Tintori MGM (2003 and 2013, DVD)
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $6.6 million Gerry Molyneaux, "John Sayles", Renaissance Books, 2000 p 179. 
| gross          = $5.7 million  
}}
Eight Men Out is a drama film based on Eliot Asinofs 1963 book 8 Men Out. It was written and directed by John Sayles. The film is a dramatization of Major League Baseballs Black Sox scandal, in which eight members of the Chicago White Sox conspired with gamblers to intentionally lose the 1919 World Series. Much of the movie was filmed at the old Bush Stadium in Indianapolis, Indiana. 

==Plot== 1919 Chicago White Sox are considered the greatest team in baseball and, in fact, one of the greatest ever assembled to that point. However, the teams owner, Charles Comiskey, is a skinflint with little inclination to reward his players for a spectacular season.
 Cincinnati Reds.

A number of players, including Chick Gandil, Swede Risberg, and Lefty Williams, go along with the scheme. The teams greatest star, Shoeless Joe Jackson, is depicted as being not very bright and not entirely sure what is going on. Buck Weaver, meanwhile, is included with the seven others but insists that he wants nothing to do with the fix.

When the best-of-nine series begins, Cicotte deliberately pitches poorly to lose the first game. Williams does likewise in Game 2, while Gandil and Hap Felsch make glaring mistakes on the field. Several of the players become upset, however, when the various gamblers involved fail to pay their promised money up front.

Chicago journalists Ring Lardner and Hugh Fullerton grow increasingly suspicious. Meanwhile, the teams manager, Kid Gleason, continues to hear rumors of a fix, but he remains confident that his boys will come through in the end.

A third pitcher not in on the scam, Dickey Kerr, wins Game 3 for the Sox, making both gamblers and teammates uncomfortable. Other teammates such as Ray Schalk continue to play hard, while Weaver and Jackson show no visible signs of taking a dive. Cicotte, who won 29 games during the season, loses again in Game 4. With the championship now in jeopardy, Gleason intends to bench him from his next start, but Cicotte begs for another chance. The manager reluctantly agrees and is rewarded with a victory in Game 7. Unpaid by the gamblers, Williams also intends to do his best, but when his wifes life is threatened, he purposely pitches badly to lose the final game.

Cincinnati wins the World Series (5 games to 3) to the shock of Sox fans. Even worse, sportswriter Fullerton exposes the strong possibility that this series was not on the level. His findings cause Comiskey and the other owners to appoint a new commissioner of baseball, Kenesaw Mountain Landis, and give him complete authority over the sport.

Eight players are indicted and brought to trial. Cicotte, Williams, and Jackson even sign confessions. But in court, while Weaver maintains his innocence, the confessions are mysteriously found to be stolen, and the popular Chicago players are found not guilty. While they celebrate, however, Judge Landis bans all eight from professional baseball for life, citing their failure to reveal being approached by gambling interests in the first place.

Weaver is among those exiled from the game. The final scene shows him in the bleachers of a New Jersey minor league ballpark in 1925, watching Jackson play under an assumed name.

==Cast==
 
* Jace Alexander as Dickey Kerr John Anderson as Kenesaw Mountain Landis
* Gordon Clapp as Ray Schalk
* John Cusack as Buck Weaver (banned)
* Richard Edson as Billy Maharg Don Harvey as Swede Risberg (banned)
* Bill Irwin as Eddie Collins
* Clifton James as Charles Comiskey
* Perry Lang as Fred McMullin (banned) Michael Lerner as Arnold Rothstein Bill Burns
* John Mahoney as Kid Gleason
* Barbara Garrick as Helen Weaver
* Wendy Makkena as Kate Jackson
* Michael Mantell as Abe Attell
* James Read as Lefty Williams (banned)
* Maggie Renzi as Rose Cicotte
* Michael Rooker as Chick Gandil (banned)
* John Sayles as Ring Lardner
* Charlie Sheen as Happy Felsch (banned)
* David Strathairn as Eddie Cicotte (banned)
* D. B. Sweeney as Shoeless Joe Jackson (banned)
* Studs Terkel as Hugh Fullerton
* Kevin Tighe as Sport Sullivan
* Nancy Travis as Williams Wife
 

==Background== Charlie in center field." 

Several people involved in this film would go on to be involved with Ken Burns 1994 film miniseries Baseball (TV series)|Baseball. Cusack, Lloyd, and Sweeney did several voice-overs, reading recorded reminiscences of various personalities connected with the game. Sayles and Terkel were interviewed on the subject of the 1919 World Series. Sayles also contributed to the section on Roberto Clemente, and Terkel, a historian and a former labor leader, spoke about the movement toward labor freedom in baseball. Terkel also "reprised his role" by reading Hugh Fullertons columns during the section on the Black Sox. 

==Production==
During the late summer and early fall of 1987, news media in Indianapolis reported sightings of the films actors, including Sheen and Cusack. Sayles told the Chicago Tribune that he hired them not because they were rising stars, but because of their ball-playing talent. 

Sweeney remarked on the chilly Indiana temperatures in an interview with Elle magazine|Elle magazine. "It got down to 30, 40 degrees, but John   would stand there in running shorts, tank tops, sneakers—sometimes without socks—and never look cold." The young actor said Sayles appeared to be focused on an "agenda, and thats all he cared about. Looking at him we thought, Well, if hes not cold, then we certainly shouldnt be." 

Reports from the set location at Bush Stadium indicated that cast members were letting off steam between scenes. "Actors kidded around, rubbing dirt on each other", the Tribune reported. "... Actors trade jokes, smokes and candy" in the dugout. "Some of them chewed tobacco at first, but, noted Bill Irwin, Even the guys who were really into it started to chew apricots after a while."  Sheen made his reasons for taking the role clear. "Im not in this for cash or my career or my performance", Sheen told the Tribune. "I wanted to take part in this film because I love baseball."

When cloud cover would suddenly change the light during the shooting of a particular baseball scene, Sayles showed "inspirational decisiveness", according to Elle, by changing the scripted game they would be shooting—switching from Game Two of the series to Game Four, for example. "The second assistant director knew nothing about baseball", Sayles told Elle, "and she had to keep track of who was on base. Suddenly wed change from Game Two to Game Four, and shed have to shuffle through her papers to learn who was on second, then track the right guys down all over the ballpark." Salzberg, Charles. Elle (magazine)|Elle, feature article "Sayles Pitch", p. 84. September 1988. 

Right-handed Sweeney told Elle that producers considered using an old Hollywood trick to create the illusion that he was hitting lefty. "We could have done it from the right side, then run to third and switched the negative, like they did in The Pride of the Yankees, but we didnt really have enough money for that", Sweeney said. 

Ring Lardner, Jr., Oscar-winning screenwriter of such films as Woman of the Year and M*A*S*H, came to Bush Stadium to visit the set. Lardners article in American Film magazine reported that Sayles script depicted much of the story accurately, based on what he knew from his father. But the audience, Lardner wrote, "wont have the satisfaction of knowing exactly why everything worked out the way it did." 

Lardner also witnessed how the production crew had to make "a few hundred extras look like a World Series crowd of thousands", which were hampered by the productions inability to entice a substantial number of Indianapolis residents to come to the stadium to act as film extras. Lardner stated, "The producers offer free entertainment, Bingo with cash prizes, and as much of a stipend ($20 a day) as the budget permits ..." 

==Reception==
 
The review aggregator website Rotten Tomatoes gave the film a rating of 85%, based on 48 reviews, with an average rating of 7.2/10. The sites critical consensus reads, "Perhaps less than absorbing for non-baseball fans, but nevertheless underpinned by strong performances from the cast and John Sayles solid direction." 

When the film was first released, the film industry staff at Variety (magazine)|Variety magazine wrote:

 "Perhaps the saddest chapter in the annals of professional American sports is recounted in absorbing fashion in Eight Men Out...The most compelling figures here are pitcher Eddie Cicotte (David Strathairn), a man nearing the end of his career who feels the twin needs to ensure a financial future for his family and take revenge on his boss, and Buck Weaver (John Cusack), an innocent enthusiast who took no cash for the fix but, like the others, was forever banned from baseball."  

Film critic Roger Ebert was underwhelmed, writing,

 "Eight Men Out is an oddly unfocused movie made of earth tones, sidelong glances and eliptic    conversations. It tells the story of how the stars of the 1919 Chicago White Sox team took payoffs from gamblers to throw the World Series, but if you are not already familiar with that story youre unlikely to understand it after seeing this film."  

Eberts television colleague Gene Siskel said, "Eight Men Out is fascinating if you are a baseball nut ... the portrayal of the recruiting of the ball players and the tight fisted rule of Comiskey is fascinating ... thumbs up." 

Critic Janet Maslin spoke well of the actors, writing,
 "Notable in the large and excellent cast of Eight Men Out are D. B. Sweeney, who gives Shoeless Joe Jackson the slow, voluptuous Southern naivete of the young Elvis; Michael Lerner, who plays the formidable gangster Arnold Rothstein with the quietest aplomb; Gordon Clapp as the teams firecracker of a catcher; John Mahoney as the worried manager who senses much more about his players plans than he would like to, and Michael Rooker as the quintessential bad apple. Charlie Sheen is also good as the teams most suggestible player, the good-natured fellow who isnt sure whether its worse to be corrupt or be a fool. The storys delightfully colorful villains are played by Christopher Lloyd and Richard Edson (as the halfway-comic duo who make the first assault on the players), Michael Mantell as the chief gangsters extremely undependable right-hand man, and Kevin Tighe as the Bostonian smoothie who coolly declares: You know what you feed a dray horse in the morning if you want a days work out of him? Just enough so he knows hes hungry. For Mr. Sayles, whose idealism has never been more affecting or apparent than it is in this story of boyish enthusiam gone bad in an all too grown-up world, Eight Men Out represents a home run."  

==DVD==
Eight Men Out was released on DVD by MGM (successor-in-interest to Orion Pictures) in two editions; a standard one on April 1, 2003, and a special edition on May 7, 2013.

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*   original trailer at Dailymotion

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 