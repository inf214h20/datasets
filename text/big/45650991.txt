Ghosts of Port Arthur
 
{{Infobox film
| name           = Ghosts of Port Arthur
| image          = 
| image_size     = 
| caption        = 
| director       = Ken G. Hall
| producer       = Ken G. Hall
| writer         = 
| based on      = 
| narrator       = Bert Bailey
| starring       = 
| music          = 
| cinematography = Frank Hurley 
| editing        = 
| studio         = Cinesound Productions
| released       = March 1933   
| runtime        = 10 mins
| country        = Australia
| language       = English
| budget         = 
}}
Ghosts of Port Arthur is a 1933 Australian short documentary directed by Ken G. Hall. It was described as a "travel fantasy"  which focuses on the history of the penal settlement at Port Arthur.
 Port Arthur, Derwent River district.

==Reception==
The film was released as a support item. The Adelaide News called it a "fine travel talk". 

==References==
 

==External links==
*  at Australian Screen Online
*  at IMDB
 

 
 
 


 