Americans at the Black Sea
{{Infobox Film
| name           = Americans at the Black Sea
| image          = AmericansBlackSeaPoster.jpg
| image size     = 
| caption        = Theatrical poster
| director       = Kartal Tibet
| producer       = Selay Tozkoparan
| writer         = Kubilay Tunçer
| narrator       = 
| starring       = Metin Akpınar Peker Açıkalın Kadir Çöpdemir Kıvanç Tatlıtuğ Melis Birkan
| music          = 
| cinematography = 
| editing        = 
| studio         = Energy Medya
| distributor    = Kenda Film
| released       =  
| runtime        = 100 min
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = US$2,243,249
| preceded by    = 
| followed by    = 
}}

Americans at the Black Sea ( ) is a 2006 Turkish comedy film, directed by Kartal Tibet, about a U.S. military recovery operation on Turkeys Black Sea coast. The film, which went on nationwide general release across Turkey on  , was one of the highest-grossing Turkish films of 2007.

==Production==
The film was shot on location in Akçakoca, Turkey.   

== Plot ==
A rocket the United States has located on the Black Seas seabed as a precaution against a potential threat from Tehran launches accidentally. Officers, quickly realizing the incident, alter the coordinates of the rocket and manage to land it—without exploding—on Turkeys Black Sea coast. All thats left is to retrieve the rocket from the village before anybody notices.

==Release==

 

==Reception==
The film was one of the highest grossing Turkish films of 2007 with a total gross of US$2,243,249.   

== See also ==
*Stereotypes of Americans

==References==
 

==External links==
*  

 
 
 
 


 
 