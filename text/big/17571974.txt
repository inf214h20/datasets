Ball Don't Lie
 
{{Infobox film
| name           = Ball Dont Lie
| image          = Ball Dont Lie FilmPoster.jpeg
| caption        = 
| director       = Brin Hill
| producer       = Brin Hill Mark G. Mathis Brigitte Mueller Michael Roiff Jeffrey Smith
| writer         = Brin Hill Matt de la Peña
| starring       = Grayson Boucher Kim Hidalgo Rosanna Arquette Emilie de Ravin Nick Cannon Ludacris
| music          = Tony Morales
| cinematography = Matthew Jensen
| editing        = Steven Pilgrim
| studio         = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} AND1 Mixtape team member Grayson Boucher, a.k.a. The Professor. The film premiered at the 2008 Tribeca Film Festival.

==Plot==
Ball Dont Lie plays out over one day in the life of Sticky (Boucher), a skinny high school sophomore and basketball prodigy from Venice, California. Burdened with emotional scars from a traumatic childhood, a callous foster care system, and obsessive-compulsive personality disorder, Sticky manages to transcend his limitations whenever he has a ball in his hands.

==Cast==
* Grayson Boucher - Sticky
* Kim Hidalgo - Annie
* Emilie de Ravin - Baby
* Rosanna Arquette - Francine
* Ludacris - Julius
* Cress Williams - Dante
* Nick Cannon - Mico
* Dania Ramirez - Carmen
* Melissa Leo - Georgia
* Harold Perrineau - Jimmy
* Mykelti Williamson - Dallas
* Robert Wisdom - Perkins Steve Harris - Rob
* Allen Maldonado - Sin
* John Bryant Davila - Venice Hardcore Teen
* Mathew St. Patrick - Louis Accord
* Michael Shamus Wiles - Coach Reynolds

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 

 