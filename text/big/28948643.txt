Dorothy and the Witches of Oz
{{Infobox television film
| image          =  
| caption        = 
| director       = Leigh Scott
| producer       =  
| screenplay     = Leigh Scott
| story          =  
| starring       =  
| music          = Eliza Swenson
| cinematography = Leigh Scott
| editing        =      
| studio         = Palace/Imaginarium
| distributor    = IFI Studios  
| released       = February 17, 2012
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $5,000,000
}}
Dorothy and the Witches of Oz is a 2012 film directed by Leigh Scott, based on the novels The Wonderful Wizard of Oz, Ozma of Oz, The Road to Oz and The Magic of Oz by L. Frank Baum. A longer version of the film was originally released as a TV miniseries in 2011 called The Witches of Oz, distributed by Marvista Entertainment.  The miniseries was over an hour longer and had earlier versions of the special effects. The miniseries was originally released in 2011 in Europe, though its United Kingdom premiere wasnt until July 5, 2012 on the Syfy|Sci-Fi Channel.

Development of the miniseries/film began while director Leigh Scott was making direct-to-video films for The Asylum. Production began in December 2009 and filming took place throughout Connecticut and New York City. 

==Plot==
 
Dorothy and the Witches of Oz, and its miniseries predecessor, follows the exploits of the grown Dorothy Gale, now a successful childrens book author, as she moves from Kansas to present day New York City. Dorothy quickly learns that her popular books are based on repressed childhood memories, and that the wonders of Oz are very, very real. When the Wicked Witch of the West shows up in Times Square, Dorothy must find the inner courage to stop her.  

==Cast==
* Paulie Rojas as Dorothy Gale, a successful childrens book author from Kansas. 
* Eliza Swenson as Billie Westbrook, Dorothys agent and friend who is actually the Earth-based form of the Wicked Witch of the West.  Billy Boyd as Nick Chopper, Dorothys boyfriend. Wizard of Oz, the ruler of the Emerald City.
* Ari Zagaris as Allen Denslow, the illustrator of Dorothys books who is actually the Earth-based form of Scarecrow (Oz)|Scarecrow.
* Ross Edgar as Rick, a man who only appears in the original version of the film.
** Jordan Turnage as Tin Woodman, the true form of Rick.
* Barry Ratcliffe as Bryan Jennings, Dorothys lawyer who is actually the Earth-based form of Cowardly Lion. witch who is an ally to the Wicked Witch of the West and has many heads that she changes.
** Sasha Jackson as Ilsa Lang, a popular Hollywood actress who is one of the thirty-one different heads of Princess Langwidere.
** Jessica Sonneborn as Ev Locast, one of Princess Langwideres thirty-one different heads.
** Elizabeth Masucci as Jennifer Mombi, a New York citizen whose head is claimed by Princess Langwidere.
* Sean Astin as Frack Muckadoo,  a servant of Princess Langwidere.
* Ethan Embry as Frick Muckadoo, a servant of Princess Langwidere. Henry Gale, Dorothys old-fashioned uncle who lives in rural Kansas.
* Jeffrey Combs as Frank, author of the original Oz books and Dorothys real father. His full name is L. Frank Baum. 
* Noel Thurman as Glinda the Good Witch|Glinda, the Good Witch of the South and ruler of the Quadling Country. 
* Brionne Davis as Simon, Ilsas ill-tempered mysterious assistant who is actually an unknown creature that works for Princess Langwidere. The Wicked Witch of the West
* Al Snow as the Nome King, a cruel king set on revenge on the Tin Woodman. 
* Liz Douglas as Aunt Em, Henrys wife.
* Brooke Taylor as Locasta, the Good Witch of the North and younger sister to Glinda.
* Sarah Lieving as the Wicked Witch of the East, the sister of the Wicked Witch of the West.
* Chanel Ryan as Pinney Pinney
* Rajah as Toto (Oz)|Toto, Dorothys beloved dog and loyal companion.

==Release==
Dorothy and the Witches of Oz was released theatrically in the United States on February 17, 2012.  The film opened in select   throughout the course of the year. 

The original version of the film, in the form of the miniseries The Witches of Oz, was released on home video in France on November 9, 2011, in Germany on December 8, 2011, and in the United States on April 10, 2012. 

===Reception===
Despite an overall negative response from critics and audiences to the original miniseries version, the later film version Dorothy and the Witches of Oz received better, but still mixed, reviews during its theatrical run. Bob Fischbach of the Omaha World-Herald praised the film for its contemporary twist on the story, but stated that it was "cheesy, but fun for family."  Patrick Luce of Monsters & Critics gave the film a positive review, and stated that "hopefully this wont be the only trip to Oz well get to take". 

==Soundtrack==
{{Infobox album  
| Name        = Dorothy and the Witches of Oz
| Type        = Soundtrack
| Artist      = Eliza Swenson
| Cover       = 
| Released    = 2012
| Recorded    = Classical
| Length      =
| Label       = Imaginarium
| Producer    = Eliza Swenson
}}

The soundtrack to the film, composed by Eliza Swenson, was released on iTunes, and then on a soundtrack CD in February 2012. 

===Track listing===
# "From the Beginning"
# "Billie Westbrook" 
# "A Place Called Oz"
# "The Changing Word"
# "Friend and Foe" 
# "This Rides On Me"
# "The Emerald Key"
# "She Doesnt Like Surrender"
# "Memory Dust"
# "Good Witch?"
# "Something Wicked This Way Comes"
# "Kansas 1889"
# "Good Witch of Manhattan" 
# "One Way Ticket to Oz"
# "Oz Suite"

==See also==
* The Wonderful Wizard of Oz
* The Wizard of Oz (1939 film)|The Wizard of Oz (1939 film) Adaptations of The Wizard of Oz

==References==
 

==External links==
*  
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 