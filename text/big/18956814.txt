Destroyer (1943 film)
 
{{Infobox film
| name           = Destroyer
| image          = Destroyer 1943 poster.jpg
| caption        = Theatrical Film Poster
| director       = William A. Seiter
| producer       = Louis F. Edelman
| writer         = Borden Chase Lewis Meltzer Frank Wead (also story)
| starring       = Edward G. Robinson Glenn Ford Marguerite Chapman Edgar Buchanan Anthony Collins
| cinematography = Franz Planer
| editing        = Gene Havlick
| distributor    = Columbia Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States English
| budget         =
| gross          =
}}

Destroyer is a 1943 Columbia Pictures war film starring Edward G. Robinson and Glenn Ford as U. S. Navy sailors in World War II.

==Plot==
In 1943 retired Navy Chief Bosuns Mate Steve "Boley" Boleslavski (Edward G. Robinson) helps build the destroyer John Paul Jones, the namesake of the ship he served on in World War I, sunk in combat while saving an aircraft carrier from being torpedoed. When he finds out that an old shipmate, Lieutenant Commander Clark (Regis Toomey), is the ships new captain, he returns to the Navy and wrangles a berth as the ships leading chief bosuns mate.

However, Boley soon alienates the rest of the destroyers crew with his perfectionist attitude and ignorance of the ships modern equipment, particularly Mickey Donohue (Glenn Ford), whom he replaced as leading chief. As a result, the ships crew and equipment perform poorly on the Joness shakedown cruise. Boley is demoted for striking Donohue when the latter goads him by insulting the Jones. Donahue becomes leading chief again. To further complicate matters, Donohue falls in love and secretly marries Boleys daughter Mary (Marguerite Chapman).
 Revolutionary War HMS Serapis.
 garrison on Kiska Island, and the crew of the Jones eagerly answers the general order to converge on the enemy. Higher headquarters contemptuously changes their orders and sends them to the safe port of Sitka, Alaska|Sitka. En route, the Jones is attacked by six enemy airplanes, shooting down all of them but struck by a torpedo during the fight. When a hostile submarine is detected, the crippled ship has to flee at its best speed. It manages to evade attack until night falls, but the list of the ship extinguishes its boilers, stopping the engines. The captain has no choice but to give the order to abandon ship. Boley is given a chance to stay aboard with a few volunteers, including Donohue, to try to weld shut the hole. He succeeds just before dawn, when the submarine attacks. The destroyer drops depth charges, forcing the submarine to the surface, then rams and sinks it.

When the ship docks to much fanfare, Boley learns that Donohue has married his daughter. He willingly returns "to the beach" after the crew expresses its reluctance to see him go.

==Cast==
* Edward G. Robinson as Steve Boleslavski
* Glenn Ford as Mickey Donohue
* Marguerite Chapman as Mary Boleslavski
* Edgar Buchanan as Kansas Jackson
* Leo Gorcey as Sarecky
* Regis Toomey as Lieutenant Commander Clark
* Edward Brophy as Casey
* Warren Ashe as Lieutenant Morton
* Benson Fong as Japanese sonar man
* Richard Loo as Japanese submarine captain
* Lloyd Bridges as Second Fireman (uncredited)
* Larry Parks as Ensign (uncredited)

==References==
 	

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 