Pranayakalam
{{Infobox Film
| name           = Pranayakalam
| image          = Pranayakalam.jpg
| image_size     = 
| caption        = 
| director       = Uday Ananthan
| producer       = AVA Productions
| writer         = Uday Ananthan
| narrator       = 
| starring       = Ajmal Ameer Vimala Raman
| music          = Ouseppachan
| cinematography = Jibu Jacob
| editing        = L. Bhoominathan
| distributor    = Century Release
| released       = 8 June 2007
| runtime        = 
| country        = India Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2007 Malayalam Malayalam romantic film directed by débutant Uday Ananthan.   

== Cast ==
* Ajmal Ameer as Renjith
* Vimala Raman as Maria
* Jithu Johny Panjikkaran as Deepu Murali as Varghese Seema
* Balachandra Menon
* Thilakan
* Madhu Warrier
* Atlee
* Baburaj
* Lekshmi Ramakrishnan


==Music==
The music for the film was written by Ouseppachan. All the songs from the movie became popular and hit the markets with good response. 

Lyrics: Rafeeque Ahmed
Cinematography: Jibu Jacob, Lokanathan
Editing: L. Bhoominathan
Art Direction: A.V. Gokuldas
Screenplay: Uday Ananthan, K. Gireesh Kumar
Story/Writer: Uday Ananthan

==References==
 

 
 
 

 