The Suspect
 
{{Infobox film
| name           = The Suspect
| image          = The-suspect-1944.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Robert Siodmak		
| producer       = Islin Auster
| screenplay     = Bertram Millhauser
| based on       =  
| starring       = Charles Laughton Ella Raines Dean Harens Stanley Ridges Frank Skinner
| cinematography = Paul Ivano
| editing        = Arthur Hilton
| distributor    = Universal Pictures
| released       =   
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Edwardian times. It is based on the novel This Way Out, by James Ronald, and was released by Universal Pictures. It stars Charles Laughton, Ella Raines, Dean Harens, and Stanley Ridges. 

==Plot==
Philip Marshall (Charles Laughton) is a kind but henpecked accountant who strikes up an innocent friendship with a young stenographer (played by Ella Raines) who had approached him looking for work. He gradually finds himself falling in love with her, but keeps the relationship platonic.

His wife Cora (Rosalind Ivan), who has also alienated their son with her shrewish ways, suspects the worst and threatens a scandal. Cora dies after a fall down the stairs at home, and it is left to the viewer to decide whether Marshall arranged the fall or not. It is strongly hinted that he did, although the death appears accidental.

A Scotland Yard inspector (Stanley Ridges) suspects that Marshall murdered his wife but is unable to prove anything. An impoverished,  drunken, wife-beating neighbor (Henry Daniell) is very interested to learn of the inspectors suspicions, and relishes the chance to blackmail Marshall, whose respectability he envies. He threatens to invent a story about an argument between Marshall and his wife, which would appear to prove that Marshall had killed his wife.

Marshall kills his neighbor. When the Inspector hears of this second death, he sets a trap in which he pretends to frame his neighbours long-suffering innocent wife for the murder. The success of the trap depends on Marshalls coming forward, rather than letting the innocent woman hang. The inspector believes that, in spite of everything, Marshall has never lost his innate decency.

==Cast==
* Charles Laughton as Philip Marshall
* Ella Raines as Mary
* Dean Harens as John 
* Stanley Ridges as Inspector Huxley
* Henry Daniell as Simmons
* Rosalind Ivan as Cora
* Molly Lamont as Mrs. Simmons
* Raymond Severn as Merridew
* Eve Amber as Sybil
* Maude Eburne as Mrs. Packer
* Clifford Brooke as Mr. Packer

==Reception==

===Critical response===
Film critic Bosley Crowther wrote, "Another study of an amiable, middle-aged fellow who commits murder out of desperation and his attempts at concealing his connection is being offered in The Suspect, ... The Suspect is by no means a dull picture, but it seems to lack that quality of excitement which in good melodrama keeps one on edge. In a word, it is too genteel. Henry Daniell, as the blackmailer, and Rosalind Ivan, as the exasperating spouse, are each splendid, and Ella Raines is most appealing as the second wife. It might be remarked, however, that her choice of a husband of Mr. Laughtons dimensions is a bit strange." 

The staff at Variety (magazine)|Variety magazine praised the film.  They wrote, "In Charles Laughton’s accomplished hands, this character becomes fascinating ... There is less of the bluster and none of the villainy of Laughton’s previous vehicles. He gives an impeccable performance as the kindly, law-abiding citizen. Matching his deft portrayal is Ella Raines as the youthful steno he weds after his wife’s demise." 
 The Spiral The Killers/The File on Thelma Jordon) competently directs this theatrical studio-bound minor film noir, and keeps it more as a character study than as a whodunit. It builds on suspense, but never becomes that exciting or interesting. But its well-acted, though the dreary story is never totally convincing or compelling." 

==Adaptation==
In 1955, The Suspect was dramatized for television on Lux Video Theatre, and starred Robert Newton.

==References==
 

==External links==
*  
*  
*  
*  
*  

===Streaming audio===
*   on Lux Radio Theater: April 9, 1945

 

 
 
 
 
 
 
 
 
 
 
 