Daredevils of the Clouds
{{Infobox film
| name           =Daredevils of the Clouds
| image          =Daredevils.jpg
| image size     =150px
| caption        =Theatrical poster George Blair
| producer       =Stephen Auer
| based on       =
| writer         =Ronald Davidson (story) Norman S. Hall (screenplay)
| narrator       = Robert Livingston James Cardwell
| music          =Morton Scott
| cinematography =John MacBurnie
| editing        =Richard L. Van Enger Republic Pictures Corp.
| distributor    = Republic Pictures Corp.
| released       =   
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 George Blair Robert Livingston, James Cardwell. Daredevils of the Clouds depicts bush pilot flying in northern Canada. 

==Plot==
Trans-Global Airlines president Douglas Harrison (Pierre Watkin) wants to force  Terry ORourke (Robert Livingston), and his rival Polar Airways out of business. When Kay Cameron (Mae Clarke) asks for a job, Harrison connives to have her infiltrate ORourkes operation in his Edmonton, Alberta headquarters. She is assigned to work on the official documents required by Jimmy Travis (Russell Arms), a Canadian customs official. Soon after, a Polar Airways flight encounters trouble when an oil line breaks. Radio operator, Eddy Clark (Jimmie Dodd) talks ORourke down through a thick fog. ORourke decides not to report the accident and the damage to his aircraft, afraid his insurance company will cancel the policy, jeopardizing his lucrative Canadian government contract to haul gold ore.
 Canadian Air ripcord to his parachute had been cut. ORourke recovers some Trans-Global Airlines pay stubs with Kay admitting she and Martin were involved in a scheme to ruin him, and that he has no insurance to cover his losses.

ORourke becomes angry and fires Kay. Conroy asks her to fly with him to deliver some documents but a microphone hidden aboard the aircraft reveals to ORourke that Conroy is the real murderer who killed Martin partly to complete his plan to steal the gold ore and partly in jealousy of anyone who is interested in Kay. When ORourke follows the pair to Caribou Flats, Conroy tries to use Kay as a hostage but is killed in the ensuing fight. Finally seeing that Kay is his true love, they return home together and marry.

==Cast==
   Robert Livingston as Terry ORourke
* Mae Clarke as Kay Cameron James Cardwell as Johnny Martin
* Grant Withers as  Matt Conroy
* Edward Gargan as "Tap-It" Bowers
* Ray Teal as Jim Mitchell
* Jimmie Dodd as Eddy Clark
  
* Pierre Watkin as Douglas Harrison
* Jayne Hazard as Mollie
* Robert J. Wilke as Joe (credited as Bob Wilke)
* Frank Melton as Frank
* Russell Arms as Jimmy Travis
* Hugh Prosser as RCMP Sergeant Dixon Charles Sullivan as Bartender Charlie
 

==Production==
 
Under the working title, Daredevils of the Sky, principal photography began in mid-February 1948 at the Republic Pictures Corp. studio and backlots, Los Angeles, California.  
 The Flying Thomas Mitchell and Maureen OHara. Grounded by the studios insurance company, the XC-12 was primarily seen on the ground with flying scenes of an XC-12 model. In RKOs Dick Tracys Dilemma (1947) the XC-12 was shown somewhat dismantled, but not scrapped, apparently its fate, shortly after the making of Daredevils of the Clouds. 

==Reception==
Daredevils of the Clouds, was primarily a B film.  Aviation film historian Stephen Pendo characterized the film as "tedious" with the flying scenes, "routine". Pendo 1985, p. 24. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Hughes, Howard. When Eagles Dared: The Filmgoers History of World War II. London: I. B. Tauris, 2012. ISBN 978-1-84885-650-9.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  

 
 

 
 
 