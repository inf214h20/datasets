The Master and Margaret (1972 film)
{{infobox film
| name = The Master and Margarita
| image = The Master and Margaret.jpg
| caption = The Master and Margareth (1972) Aleksandar Petrović
| producer       = Arrigo Colombo
| writer         = Barbara Alberti Amedeo Pagani Aleksandar Petrović Roman Wingarten Mikhail Bulgakov  
| starring       = Ugo Tognazzi Mimsy Farmer Alain Cuny Bata Živojinović Pavle Vuisić Fabijan Šovagović Ljuba Tadić Taško Načić Danilo Stojković
| music          = Ennio Morricone
| cinematography = Roberto Gerardi
| editing        = Mihailo Ilić
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy / Yugoslavia Italian
| budget         = 
| gross          = 
}}
 directed by Aleksandar Petrović. 

The film is an adaptation of Mikhail Bulgakovs 1940 novel The Master and Margarita, although it mainly focuses on the parts of the novel set in 1920s Moscow.   
 Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Ugo Tognazzi - The Master (Nikolaj Afanasijevič Maksudov)
* Mimsy Farmer - Margarita 
* Alain Cuny - Woland
* Bata Živojinović - Koroviev
* Pavle Vuisić - Azazello
* Fabijan Šovagović - Berlioz
* Ljuba Tadić - Pontius Pilate
* Taško Načić - Rimsky
* Danilo Stojković

==Other screen adaptations of The Master and Margarita==
*Giovanni Brancale - Il Maestro e Margherita - 2008 (film) Master i Margarita - 2005 (TV series)
*Ibolya Fekete - A Mester és Margarita - 2005 (film)
*Sergei Desnitsky - Master i Margarita - 1996 (film) Master i Margarita - 1994 (film) Incident in Judea - 1992 (film)
*Andras Szirtes - Forradalom Után - 1990 (film) Mistrz i Małgorzata - 1988 (TV series) Pilatus und Andere - 1972 (film)

;To be expected: 

* Scott Steindorff - The Master and Margarita - 2012 (film)
* Rinat Timerkaev - Master i Margarita - 2012 (animation film)

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
*          on the Master & Margarita website

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 