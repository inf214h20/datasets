Smokey Joe's Cafe: Direct from Broadway
{{Infobox film
| name           = Smokey Joes Cafe: Direct from Broadway
| image          = SmokeyJoesCafeDVDCover.jpg
| caption        = DVD Cover Art
| director       = Don Roy King
| based on       =  
| starring       = B.J. Crosby Adrian Bailey Victor Trent Cook Brenda Braxton DeLee Lively
| music          = Jerry Leiber Mike Stoller
| cinematography = 
| editing        = 
| distributor    = Broadway Worldwide
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
}} 2000 film Broadway production of the musical revue Smokey Joes Cafe as captured live in performance on Broadway featuring the shows final Broadway cast. The show was captured at Broadway’s Virginia Theatre in New York City during the shows final Broadway performance utilizing multiple high definition cameras by Broadway Worldwide.

The film was released September 10, 2000 on cable and satellite pay-per-view channels in the U.S., Canada, and Latin America. {{cite web 
|url= http://www.playbill.com/news/article/55308-Broadway-Television-Network-Is-First-to-Market-With-PPV-Smokey-Joes-Sept-10
|title= Broadway Television Network Is First to Market With PPV Smokey Joes, Sept. 10
|author= Playbill News
|accessdate=
|publisher= playbill.com
|date= }}  The program was released on DVD and VHS November 6, 2001 by Good Times Video, with a DVD re-release January 16, 2007 by Image Entertainment. {{cite web 
|url= http://www.amazon.com/Smokey-Joes-Cafe-Leiber-Stoller/dp/B000JJSKZ4/ref=sr_1_5?ie=UTF8&qid=1303388284&sr=8-5
|title= Amazon listing for Smokey Joes Cafe
|author= 
|accessdate=2006
|publisher= amazon.com
|date= }}  HBO bought the program in December 2002 for a two-year contract on the network. {{cite web 
|url= http://www.playbill.com/news/article/75302-HBO-To-Offer-Jekyll-Hyde-Smokey-Joes-Cafe-Putting-It-Together
|title= HBO To Offer Jekyll & Hyde, Smokey Joes Cafe & Putting It Together
|author= 
|accessdate= 2 December 2002 
|publisher= playbill.com
|date= }} 

==Cast==
Ken Ard, Adrian Bailey, Matt Bogart, Brenda Braxton, Victor Trent Cook, B.J. Crosby, DeLee Lively, Deb Lyons, Dredrick B. Owens, Virginia Woodruff

==References==
 

==External links==
*  
* 
*  


 
 
 

 