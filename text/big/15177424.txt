The Savage Eye
 
 
{{Infobox Film  
| name           = The Savage Eye
| image          = Savage_Eye_poster.jpg
| caption        = Film Poster for The Savage Eye (1959)
| director       = Ben Maddow Sidney Meyers Joseph Strick
| producer       = Ben Maddow Sidney Meyers Joseph Strick
| writer         = Ben Maddow Sidney Meyers Joseph Strick
| starring       = Barbara Baxley Herschel Bernardi Jean Hidey Elizabeth Zemach Gary Merrill
| music          = Leonard Rosenman
| cinematography = Jack Couffer Helen Levitt Haskell Wexler
| editing        = Ben Maddow Sidney Meyers Joseph Strick
| distributor    = Trans-Lux Distributing-Kingsley International
| released       = June 6, 1960
| runtime        = 68 minutes
| country        = USA 
| awards         = 
| budget         = $65,000
}}
The Savage Eye is a 1959 "dramatized documentary" film that superposes a dramatic narration of the life of a divorced woman with documentary camera footage of an unspecified 1950s city. In a 1960 review, A. H. Weiler characterized the film:  
 
 Muscle Beach BAFTA Flaherty Documentary Award as well as several film festival prizes. Reviewing its debut at the 1959 Edinburgh Film Festival, the art critic David Sylvester called its imagery "sharp, intense, spectacular, and imaginative". 
 Red Desert, and Juliet of the Spirits."

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 