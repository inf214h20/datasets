Dr. Jekyll and Sister Hyde
 
 
 
 
{{Infobox film
| name           = Dr. Jekyll and Sister Hyde
| image          = Dr. Jekyll and Sister Hyde.jpg
| image_size     =
| caption        = U.S. film poster
| director       = Roy Ward Baker
| producer       = Brian Clemens Albert Fennell
| writer         = Brian Clemens
| starring       = Ralph Bates Martine Beswick David Whitaker
| cinematography = Norman Warwick
| editing        = James Needs
| studio         = Hammer Film Productions American International (USA)
| released       = 17 October 1971
| runtime        = 97 minutes
| country        = United Kingdom English
}} The Ugly Duckling and The Two Faces of Dr. Jekyll.  The film is notable for showing Jekyll transform into a female Hyde; it also incorporates into the plot aspects of the historical Jack the Ripper and Burke and Hare cases.  The two characters were played by the films stars, Ralph Bates and Martine Beswick.

A remake of the film was reportedly under consideration as of 2011. 

==Plot==
Dr. Henry Jekyll dedicates his life to the curing of all known illnesses, however when his lecherous friend, Professor Robertson, remarks that Jekylls experiments take so long to actually be discovered, he will no doubt be dead by the time he is able to achieve anything. Haunted by this remark, Jekyll abandons his studies and obsessively begins searching for an elixir of life, using female hormones taken from fresh cadavers supplied by murderers  . Susan becomes jealous when she discovers this mysterious woman, but when she confronts Jekyll, to explain the sudden appearance of his female alter ego, he calls her Mrs. Hyde, saying she is his widowed sister who has come to live with him. Howard, on the other hand, develops a lust for Mrs. Hyde.

Dr. Jekyll soon finds that his serum requires a regular supply of female hormones to maintain its effect, necessitating the killing of young girls. Burke and Hare supply his needs but their criminal activities are uncovered. Burke is lynched by a mob and Hare blinded. The doctor decides to take the matters into his own hands and commits the murders attributed to Jack the Ripper. Dr. Jekyll abhors this, but Mrs. Hyde relishes the killings as she begins to take control, even seducing and then killing Professor Robertson when he attempts to question her about the murders.
 Ripper murders. As Dr. Jekyll tries to escape by climbing along the outside of a building, he transforms into Mrs. Hyde, who, lacking his strength, falls to the ground, dying as a twisted amalgamation of male and female.

==Cast==
* Ralph Bates as Dr. Henry Jekyll
* Martine Beswick as Sister Hyde
* Gerald Sim as Professor Robertson
* Lewis Fiander as Howard Spencer
* Susan Brodrick as Susan Spencer
* Dorothy Alison as Mrs. Spencer William Burke William Hare
* Philip Madoc as Byker
* Paul Whitsun-Jones as Sergeant Danvers 
* Virginia Wetherell as Betsy
==Production==
 
Hammer Horror actress, Caroline Munro was the first choice to play Sister Hyde, but she declined because the role required nudity.

==Release==
 

== Critical reception ==
  Time Out called the film "enormous fun" and an "admirably successful attempt to ring new changes on an old theme". 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 