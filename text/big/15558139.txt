Welcome to Sajjanpur
 
 
{{Infobox film
| name = Welcome to Sajjanpur
| image = mahadevkasajjanpur.jpg
| caption = Movie Poster for Welcome to Sajjanpur
| director       = Shyam Benegal
| producer       = Ronnie Screwvala Chetan Motiwalla
| writer         = Shyam Benegal Ashok Mishra
| starring       = Shreyas Talpade Amrita Rao Kunal Kapoor Ravi Kishan   Ravi Jhankal
| music          = Shantanu Moitra
| lyrics         = Ashok Mishra Swanand Kirkire
| cinematography = Rajan Kothari
| editing        = Aseem Sinha
| studio         = IX Faces Pictures
| distributor    = UTV Spotboy Motion Pictures
| released       = 19 September 2008
| runtime        = 134 mins
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}

Welcome to Sajjanpur is a 2008 Hindi comedy film directed by Shyam Benegal and starring Shreyas Talpade and Amrita Rao in the lead roles. The film is by noted Parallel cinema director, Shyam Benegal, marking his return to comedy after filming Charandas Chor (1975). Even though Benegal is responsible for other films in the parallel cinema genre, this film was one of his mainstream Bollywood films. It was a remake of the 1977 film Palkon Ki Chhaon Mein.

== Plot ==

Mahadev (Shreyas Talpade) is an unemployed graduate with a Bachelor of Arts from Satna college, who is forced to make a living writing letters for the uneducated people of his village. His real ambition is to become a novel writer. Through his humble occupation, Mahadev has the potential to impact numerous lives. The movie is a satirical, but warm-hearted portrait of life in rural India.

Among Mahadevs customers are:
* Mahadevs childhood crush Kamla (Amrita Rao) is desperate for communication from her husband Bansi Ram (Kunal Kapoor), who works as a labourer at a dockyard in Mumbai. In the letters to her husband, a jealous Mahadev writes the opposite of the loving messages Kamla wants to convey, while faking what her husband has written to her.
* A hurried mother (Ila Arun) who wants to get her manglik daughter, Vindhya (Divya Dutta) married.
* A landlord whose wife is a candidate for the village Sarpanch, and who wants all her political rivals eliminated from the race.
* A eunuch Munni who is contesting the elections for the village Sarpanch but fears the threats from the landlord.
* A love-lorn compounder, Ram Kumar (Ravi Kishan), who is crazy about the widowed daughter-in-law Shobha Rani (Rajeshwari Sachdev) of a retired army soldier.

Mahadev manages to get his friend engaged, police protection for Munni, and almost kisses Kamla before they are interrupted. However Munni is seriously injured in the head, and he learns a shocking truth about Kamlas husband.It soon turns out that the story was a fictional novel written by the real Mahadev, but it is mostly based on his own experiences. Though it turns out that some of the villagers didnt exactly have happy endings, Mahadev sorts out his mistakes and accomplishes his long-held dream of writing a novel.

As Mahadev, under pseudo name Sukhdev, reveals that Munnibai became a successful politician, becoming an MLA, or a Member of the State Legislature, with high connections and powerful people surrounding her. It is also revealed that Kamla and Bansi are happy in small house in Mumbai, who come to visit Sajjanpur every Diwali. In midst of all these good news, Mahadev notes that Ram Kumar and Shobha Rani were lynched because members of their community opposed a widow getting re-married. Mahadev also notes that he got married to Vindhya, the manglik, after wooing her by writing 40 letters. While most people consider a manglik to be a great misfortune, Mahadev notes that he became successful due to his marriage, as he paid down his farm land mortgage, built a wonderful house and realised his dream of writing a novel.

== Cast ==
* Shreyas Talpade as Mahadev Kushwaha
* Amrita Rao as Kamla Kumharan
* Ravi Kishan as Ram Kumar
* Kunal Kapoor as Bansi
* Ravi Jhankal as Munnibai Mukhanni Yashpal Sharma as Ramsingh
* Rajeshwari Sachdev as Shobha Rani
* Divya Dutta as Vindhya
* Ila Arun as Ramsakhi Pannawali
* Lalit Mohan Tiwari as Subedar Singh
* Rajit Kapur as Collector
* Vineeta Malik (Kamlas mother-in-law)
* Daya Shankar Pandey as Chidamiram Naga Sapera
* Sri Vallabh Vyas as Ramavtar Tyagi (Masterji)

== Soundtrack ==
Music of the film is by Shantanu Moitra. The music was released on 5 September 2008.,  there are a total of seven songs in the soundtrack including one remix.
 KK
* Ek Meetha Marz De Ke - Madhushree, Mohit Chauhan
* Bheeni Bheeni Mehki Mehki - Krishnakumar Kunnath|KK, Shreya Ghoshal
* Dildara Dildara - Sunidhi Chauhan, Sonu Nigam
* Aadmi Azaad Hai - Kailash Kher
* Munni Ki Baari - Ajay Jhingran KK

== Reception ==
Upon its release, an Indiatimes review said, "Shyam Benegal has always been accredited as a mesmerizing storyteller known for making meaningful cinema. This time he also caters to commercial consumers, coming up with his most entertaining attempt, by far." 

== Awards and Nominations ==
* Amrita Rao - Winner, Stardust Best Actress Award

== References ==
 

== External links ==
*  
*  
* Sen, Meheli (2011) "Vernacular Modernities and Fitful Globalities in Shyam Benegal’s Cinematic Provinces", in: manycinemas 1, 8-22,  ,  

 

 
 
 
 
 