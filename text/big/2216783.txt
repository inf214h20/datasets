Bounce (film)
{{Infobox film
| name           = Bounce
| image          = Bounce ver3.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Don Roos
| producer       = Michael Besman Steve Golin
| writer         = Don Roos
| narrator       =
| starring       = Ben Affleck Gwyneth Paltrow Tony Goldwyn Natasha Henstridge Jennifer Grey
| music          = Mychael Danna
| cinematography = Robert Elswit
| editing        = David Codron
| distributor    = Miramax Films
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $53,425,292
}}

Bounce is a 2000 American romantic drama film starring Ben Affleck and Gwyneth Paltrow and directed by Don Roos.  

==Plot==
In Chicagos OHare International Airport|OHare airport, advertising executive Buddy Amaral (Ben Affleck) is delayed by a snow storm for a return flight to Los Angeles, on the same airline he has just signed as a big client. He meets writer Greg Janello (Tony Goldwyn), and when his flight resumes boarding, Buddy gives his ticket to Greg so he can get home to his sons, Scott (Alex D. Linz) and Joey (David Dorfman). Buddy convinces his friend and airline employee Janice Guerrero (Jennifer Grey) to allow Greg to takes his place on the flight. While spending the night with fellow stranded passenger Mimi (Natasha Henstridge), Buddy sees on television that the flight crashed. He has Janice check into the computer system to remove his name from the passenger manifest and add Gregs name.  

Gregs wife Abby (Gwyneth Paltrow) is woken up by news of the crash, and for many hours is torn between hope and despair, clinging to the belief that Greg would still arrive on the later flight on which he was originally booked, until his death is confirmed.
 Clio award. Crushed by guilt, Buddy drunkenly (and publicly) begins a stint in Alcoholics Anonymous. One of the steps in recovery is to make up for past deeds, with Buddy seeking out Abby, a budding realtor. Giving her a tip on a commercial office building that Jim (Joe Morton), Buddys partner and boss, has put a bid on, in return, Abby treats Buddy to a night at Dodger Stadium. Their relationship blossoms, although both have secrets. 

Buddy gets close to the whole family, and Abby reveals that she is not divorced; her husband died in an aircraft crash.  When the airline settles with Gregs estate, she next wants to put her boys on an aircraft to Palm Springs to get over their fear of flying. Buddy asks to go along with them, and soon develops a strong bond with the two boys. On the return trip, Buddy says he has a secret he will reveal the next day. 

It all comes apart when Mimi shows up, with a video of Greg and Buddy having a drink in the airport bar. Abby is devastated by Buddy lying to her and demands that he leave her home and her life - though also demanding that he say goodbye to the boys. This does not sit well with anyone. Buddy comes back the next day and talks to Scott, who feels that his father died trying to get home for a Scout Christmas tree outing. 

The relatives sue the airline for damages, and Janices role is revealed when Buddy is called to testify. As Abby watches on television, Buddy explains that gave his ticket to Greg and did not take Gregs in exchange. In coercing Janice to change the roster, the airlines security procedures were compromised, getting her fired. Buddy remembered that Greg was not the good flier he once was, having had "too many people in his wallet". Buddy is excused by the judge, but does not feel "excused". Abby had harbored the same guilt as her son in pressuring Greg to come home on the fateful flight. 

In being honest and facilitating the suit against the airline, Buddy realizes that he has to resign from his firm. Abby comes by to tell him that his talk with Scott had helped them both. Buddy, sensing that Abby is about to leave, asks her to help him rent his beachfront home or put it up for sale. As Buddy starts to talk about his plans, Abby realizes she can forgive him.

==Cast==
*Ben Affleck as Buddy Amaral
*Gwyneth Paltrow as Abby Janello
*Natasha Henstridge as Mimi Prager Edward Edwards as Ron Wachter
*Jennifer Grey as Janice Guerrero
*Tony Goldwyn as Greg Janello
*Lisa Joyner as T.V. Announcer
*Caroline Aaron as Donna
*Alex D. Linz as Scott Janello
*David Dorfman as Joey Janello
*Juan Garcia as Kevin Walters
*Joe Morton as Jim Willer
*Johnny Galecki as Seth

==Production==
Bounce  was a project previously in development with Polygram Filmed Entertainment. Principal photography began on August 30, 1999, with shooting completed on November 7, 1999.

The film marks the first major motion picture to be delivered via satellite, with AMC Empire playing Bounce exclusively in its digital format/digital production. The film was later released in United States on video on April 10, 2001. 
==Music==

===Soundtrack===
{{Infobox album
| Name = Bounce: Music from and Inspired by the Miramax Motion Picture
| Type = soundtrack Digital download)/Audio CD
| Artist = Various
| Cover = 
| Released = November 7, 2000 
| Length =  Arista
| Reviews =
}}
# Need to Be Next to You - Leigh Nash
# Central Reservation (The Then Again Version) - Beth Orton Dido
# Divided - Tara MacLean
# Silence - Delerium
# Im No Ordinary Girl - Anika Parks
# Lose Your Way - Sophie B. Hawkins
# My Baby And Me - Nick Garrisi
# Rome Wasnt Built In A Day - Morcheeba
# Hush - Angie Aparo
# Our Affair (Remix) - Carly Simon
# Love (Remix) - Sixpence None The Richer
# The Only Thing Thats Real - Sister Seven BT

===Score===
{{Infobox album
| Name = Bounce: Original Motion Picture Score
| Type = Film score Digital download)/Audio CD
| Artist = Mychael Danna
| Cover = 
| Released = November 21, 2000  
| Length = 30:23
| Label = Varèse Sarabande
| Reviews =
}}
#Weather  
#Bed Time  
#Boarding Pass  
#Moving Day  
#Hangover  
#Crash  
#Nice To Meet You  
#Now I Am  
#So Brave  
#Seven Steps  
#Christmas Trees  
#Award  
#Kiss  
#Deception  
#Say Goodbye  
#Testimony  
#Youre Excused  
#Can We Try?  

==Reception==
Bounce received generally mixed reviews from film critics. As of January 2012, review aggregator Rotten Tomatoes has given it a 52% rating, with an average rating of 5.5 out of 10, based on 106 reviews.  The film opened at #4 at the North American box office making $11.4 million its opening weekend.

Roger Ebert noted that the plot was a familiar one. "Lovers with untold secrets are a familiar movie situation ..." yet, he liked the film because the characters, from lead actors to secondary roles, were honest and endearing.  
 
==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Milano, Valerie. Gwyneth Paltrow. Toronto, Ontario, Canada: ECW Press, 2000. ISBN 978-1-55022-407-8.
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 