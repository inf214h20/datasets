Mr. Soft Touch
{{Infobox film
| name           = Mr. Soft Touch
| image          =
| image_size     =
| caption        = Gordon Douglas Henry Levin
| producer       = Milton Holmes
| writer         = Orin Jannings Milton Holmes (story)
| narrator       =
| starring       = Glenn Ford Evelyn Keyes
| music          =
| cinematography =
| editing        =
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English}}
 1949 film about a man on the run from the Mafia|Mob. It stars Glenn Ford and Evelyn Keyes.

==Plot==
Polish American Joe Miracle (Glenn Ford) returns from fighting in World War II, only to find his San Francisco nightclub under the control of the Mob, and his friend and partner Leo missing and presumed murdered. To get even, he robs $100,000 from his former business, planning to leave the country as soon as possible. He goes to the apartment of Victor Christopher (Ray Meyer), Leos brother, where he picks up a ticket Victor and his wife Clara (Angela Clarke) had purchased for him. However, he discovers to his dismay that they could only book him on a ship that sails for Yokohama on Christmas Eve, the next night. He has to hide until then. When the police come to stop Victor from ringing a bell and disturbing the neighbors, Joe pretends to be him in order to spend the night safely in jail. However, Jenny Jones (Evelyn Keyes), a kind-hearted social worker, gets him remanded into her custody instead. 
 John Ireland).

Byrd tries to find out from Jenny if Joe is staying at the settlement house, but she refuses to divulge anything. From Byrds description, Jenny realizes that Joe is not Victor. Then, when she finds out Joe also has a pistol, she insists he leave. Byrd returns and tries to get Joe to tell him the name of the man providing protection to the crooks, but Joe refuses to talk. When he collects his money, Jenny pleads with him to give it back so they can start a life together. He counters by asking her to leave the country with him. Neither accepts the others proposal. Meanwhile, the mobsters force Clara to tell them where Joe is hiding and start a fire to smoke him out. They recover the money but the settlement house is left in smoldering ruins.

Joe enters the nightclub through a secret passageway and takes the money again from the new boss, Barney Teener (Roman Bohnen). Then he hires some men to dress up as Santa Claus to distribute presents to the children at a fundraiser at the settlement house. Joe slips in as another Santa and leaves the money to pay for the rebuilding. As he slips away, Jenny realizes what is going on and chases him out into the street, calling his name. Hearing this, the waiting mobsters shoot Joe in the back. The film ends at this point, leaving it unclear whether he will live or die, or what the future holds for the couple.

==Cast==
*Glenn Ford as Joe Miracle
*Evelyn Keyes as Jenny Jones John Ireland as Henry "Early" Byrd
*Beulah Bondi as Mrs. Hangale, one of the settlement house workers
*Percy Kilbride as Rickle
*Helene Stanley as Donna
*Clara Blandick as Susan Balmuss, another of Jennys co-workers
*Ted de Corsia as Rainey, the main thug
*Stanley Clements as Yonzi, leader of the teens who lose to Joe
*Roman Bohnen as Barney Teener Harry Shannon as Police Sergeant Garrett Gordon Jones as Muggles, a gangster
*Ray Mayer as Victor Christopher
*Angela Clarke as Clara Christopher

==External links==
* 
* 

 
 

 
 
 
 
 
 
 