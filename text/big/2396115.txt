The Garden (1990 film)
{{Infobox film

 | name = The Garden
 | image_size = 
 | caption = 
 | director = Derek Jarman
 | producer = James Mackay
 | writer = Derek Jarman
 | narrator = Michael Gough Spencer Leigh Spring - Mark Adley COIL Miranda Sex Garden
 | cinematography = Derek Jarman, Christopher Hughes, Richard Heslop
 | editing = Derek Jarman, Peter Cartwright, Kevin Collins
 | distributor = Basilisk Communications
 | released = 1990
 | runtime = 95 min.
 | country = United Kingdom
 | language = English
 | budget = GBP£380,000
 | gross = 
 | preceded_by = 
 | followed_by = 
 | website = 
 | image = The Garden VideoCover.jpeg
}} British art arthouse film Dungeness in Kent,  and his garden and the nearby landscape surrounding a nuclear power station, a setting Jarman compares to the Garden of Eden. The film was entered into the 17th Moscow International Film Festival.   

==Overview==
Lacking almost any dialogue the film is shown as Jarmans own subjective musings, which are tempered by the reality of his own mortality&mdash;when HIV-positive Jarman made the film he was facing death from AIDS.
 Madonna (Tilda Judas who Cypriot and other types of music and sound.  The film has a soundtrack by Simon Fisher-Turner and production design by Derek Brown.
 Roger Cook, Spencer Leigh, Kevin Collins, Jodie Graber and Jarman himself.

It is currently the only one of Jarmans feature-length films not available on Region 1 DVD.

==Cast==
* Tilda Swinton as Madonna
* Johnny Mills as Lover
* Philip MacDonald as Joseph
* Pete Lee-Wilson as Devil Spencer Leigh as Mary Magdalene / Adam

==References==
 

==External links==
* 
* 
* 
*  at the Internet Archive

 

 
 
 
 
 
 
 
 


 