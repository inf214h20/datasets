Schubert's Dream of Spring
{{Infobox film
| name           = Schuberts Dream of Spring
| image          =
| image_size     =
| caption        =
| director       = Richard Oswald 
| producer       = Erich Morawsky   Richard Oswald
| writer         =  Léo Lasko  Arthur Rebner
| narrator       =
| starring       = Carl Jöken  Gretl Theimer   Alfred Läutner   Willy Stettner
| music          = Felix Günther
| cinematography = Willy Goldberger Paul Falkenberg
| studio         = Richard-Oswald-Produktion
| distributor    = Atlas-Filmverleih
| released       = 1931
| runtime        = 90 minutes
| country        = Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} German musical film directed by Richard Oswald and starring Carl Jöken, Gretl Theimer and Alfred Läutner. It is a biopic of the early Nineteenth century Austrian composer Franz Schubert. It was one of two films along with Vienna, City of Song (1930) with which the director paid musical tribute to his native city Vienna. 

==Cast==
* Carl Jöken as Franz Schubert 
* Gretl Theimer as Maria Esterhazy 
* Alfred Läutner as Graf Esterhazy 
* Willy Stettner as Von Fekete, sein Schwiegersohn in spe 
* Lucie Englisch as Therese, Wirtin der Höldrichsmühle 
* Sig Arno as Ferdi Klebinder
* Oskar Sima as Sepp, Küfer der Höldrichsmühle 
* Gustl Gstettenbaur as Schani, Piccolo in der Höldrichsmühle 
* Fritz Kampers    Max Hansen    Paul Morgan

==References==
 

==Bibliography==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 