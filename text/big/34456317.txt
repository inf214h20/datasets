Miss Robin Crusoe
 
{{Infobox film
| name           = Miss Robin Crusoe
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Eugene Frenke
| producer       = Eugene Frenke
| writer         = Richard Yriondo Harold Nebenzal Al Zimbalist (story)
| based on       = 
| screenplay     =
| narrator       = Amanda Blake
| starring       = Amanda Blake George Nader Rosalind Hayes
| music          = Elmer Bernstein
| cinematography = Virgil Miller Thomas Pratt
| studio         = Eastern Productions 
| distributor    = Twentieth Century Fox Film Corporation
| released       = 1954
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Miss Robin Crusoe is a 1954 Eastmancolor adventure film directed by Eugene Frenke, starring Amanda Blake, George Nader and Rosalind Hayes. One of many film variations of Robinson Crusoe, it features a female castaway. The film was shot on location in Palos Verdes, California. 

==Plot==
On September 28, 1659, a ship founders. The captains daughter and cabin boy, Robin Crusoe (Amanda Blake), and a sailor named Sykes reach a deserted island. When Sykes tries to force Robin to show her appreciation for his efforts, she flees up a hill. In the ensuing struggle, he falls over a cliff and is killed. 

She soon settles in, building herself a tree house. 

When a party of savages shows up with two women captives, she watches from hiding as they execute one in gruesome fashion. She then rescues the other (Rosalind Hayes), and the two fight off the men with the aid of her flintlock. She names her new companion Friday, as that was the day of her rescue. The two women become friends.

In December, Royal Navy officer Jonathan (George Nader) washes ashore. Robins experiences with lecherous sailors and her cruel father have embittered her against men, and she is hostile and suspicious at first. When Jonathan learns that she is repairing a longboat that can hold only two, he suggests that the "fittest" take it, and send help back for Friday. Robin, however, insists she and Friday will use the boat. Eventually, Robin overcomes her prejudice against him, and they spend the night together. The next morning, she awakens to find he has stolen the longboat and is sailing away.

When he returns, she assumes he is a coward, and sets out to kill him. He informs her that he turned back for her. Before she can shoot him, however, the savages return and capture Friday. Robin and Jonathan rescue her, but are surrounded. When all seems lost, Robin admits she wants to marry Jonathan. Just then, a warship appears and bombards the attackers, enabling the trio to steal an outrigger canoe and reach the safety of the ship.

==Cast==
* Amanda Blake as Miss Robin Crusoe
* George Nader as Jonathan  
* Rosalind Hayes as Friday  

==References==
 	

== External links ==
*  
*  
*  

 

 
 
 
 
 

 