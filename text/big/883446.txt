The Angry Red Planet
{{Infobox film
| name           = The Angry Red Planet
| image          = Angry Red Planet.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = Ib Melchior
| producer       = Sidney W. Pink Norman Maurer
| screenplay     = Sidney W. Pink Ib Melchior
| based on           = an original story by Sidney W. Pink
| starring       = Gerald Mohr Naura Hayden Jack Kruschen Les Tremayne
| music          = Paul Dunlap
| cinematography = Stanley Cortez
| editing        = Ivan J. Hoffman
| studio = Sino Productions
| distributor    = Sino Productions (originally) American International Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = United States dollar|US$ 190,000
}} CineMagic technique, which involved using hand-drawn animations together with live action footage, and it was used for all scenes on the surface of Mars. Although this process was largely unsuccessful, producer Norman Maurer would attempt the same technique again in The Three Stooges in Orbit. 

==Plot==
The rocketship MR-1 (for "Mars Rocket 1"), returns to Earth after the first manned flight to Mars. At first thought to have been lost in space, the rocket reappears but mission control cannot raise the crew by  ) and Col. Tom OBannion (Gerald Mohr), the latters arm covered by a strange alien growth. The mission report is recounted by Dr. Ryan as she attempts to find a cure for Col. OBannions arm.
 force field. OBannion leads the crew to a Martian lake with a city visible on the other side. They cross in an inflatable raft, only to be stopped by a giant amoeba-like creature with a single spinning eye. The creature kills Jacobs and infects OBannons arm. The survivors escape to the MR-1 and commence liftoff. The survivors then return to Earth, where OBannons infected arm is cured using electric shocks.

When the mission scientists attempt to examine the expeditions data recorders, all they find is a recorded message. An alien voice announces that the MR-1 crew were allowed to leave so they can deliver this message to Earth. The Martians have been watching human development throughout history, believe our technology has outpaced cultural advancement, and accuse mankind of invading their world. They warn humanity to never return to Mars or Earth will be destroyed in retaliation.

==Cast==
* Gerald Mohr as Col. Thomas OBannion
* Naura Hayden as Dr. Iris Irish Ryan
* Les Tremayne as Prof. Theodore Gettell
* Jack Kruschen as CWO Sam Jacobs
* Paul Hahn as Maj. Gen. George Treegar
* J. Edward McKinley as Prof. Paul Weiner
* Tom Daly as Dr. Frank Gordon
* Don Lamond as TV Newscaster/Martian Voice
* Edward Innes as Brig. Gen. Alan Prescott
* Gordon Barnes as Maj. Lyman Ross
* Jack Haddock a Lt. Col. Davis
* Brandy Bryan as Nurse Hayes
* Joan Fitzpatrick as Nurse Dixon
* Arline Hunter as Joan
* Alean Hamilton as Joans Friend

==Production==
Sidney W. Pink originally wrote a treatment called The Planet Mars, which told the story of an Earth trip to the planet Mars. 

"It was written on my kitchen table," said Pink later. "My kids were my critics, theyd tell me what was good and what just fell flat!" 

Pink gave his treatment to Melchior, whom hed met at a party; Melchior agreed to write the script if Pink allowed him to direct. While writing the script, Pink met Maurer, who was developing a new cinematic technique, CineMagic, which attempted to make drawings look like photographic images. However it soon became apparent the technique would not be able to deliver what had been promised. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures McFarland, p 161-168 

"The damn Cinemagic didnt work like it should," said Pink later. "It was supposed to be sort of a 3-D effect. What we came up with was great anyway!"   accessed 15 April 2014 
 
Filming started on September 9, 1959, a month after Melchior completed his final draft. 

==Release==
American International Pictures released the film as a double feature with Circus of Horrors. It was the first of several movies Melchior made for the studio. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 14 

"Arkoff and I had a working relationship," said Pink. "Neither of us trusted the other… which worked out well because I wouldnt touch him with a ten foot pole. Jimmy Nicholson was the brains of that operation. With Arkoff, you never got a straight count." 

===Box office===
The film was popular at the box office and enjoyed a long life on television. 

===Critical response===
When the film was released, Eugene Archer, film critic for The New York Times, critiqued the films special effects, writing:
 The Angry Red Planet, solemnly warns its audiences not to go to Mars. Stubborn patrons who ignore the advice will discover that the planet looks like a cardboard illustration from Flash Gordon and is inhabited by carnivorous plants, a giant amoeba and a species resembling a three-eyed green ant."  

Film critic Bruce Eder retrospectively praised the film, writing:
 The effects are a combination of costuming, model work, and puppets, with Bob Bakers giant (puppet) bat-rat-spider moving off in the distance perhaps the best shot in the movie. Danish-born director/screenwriter Ib Melchior brings a surprisingly light, deft touch to the proceedings, allowing the actors a chance to have fun with their roles -- especially Gerald Mohr, still looking and sounding a bit like Humphrey Bogart, as the stalwart mission commander, and Jack Kruschen as the good-humored technician in the crew -- without losing sight of the adventure and the story line, and meshing it all seamlessly with the special effects-driven sequences."  

===Home media===
The Angry Red Planet was released by MGM on Region 1 DVD on April 1, 2003.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 