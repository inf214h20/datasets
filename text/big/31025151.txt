Los hijos de María Morales
{{Infobox film
| name           =Los hijos de María Morales
| image          = 
| image size     =
| caption        =
| director       =Fernando de Fuentes
| producer       = Fernando de Fuentes, Antonio Matouk
| writer         =  Ernesto Cortázar, Paulino Masip
| narrator       =
| starring       =  Pedro Infante, Antonio Badú, Emma Roldán
| music          = Manuel Esperón Ignacio Torres
| editing        = José W. Bustos
| distributor    = 
| released       = 14 August 1952 (Mexico)
| runtime        = 114 min 
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1952 Mexico|Mexican film. It was directed by 
Fernando de Fuentes.

==Cast==
* 	 Pedro Infante	 ...	José Morales
* 	 Antonio Badú	 ...	Luis Morales
* 	 Emma Roldán	 ...	María Morales
* 	 Carmelita González	 ...	Gloria Magaña
* 	 Irma Dorantes	 ...	María
* 	 Josefina Leiner	 ...	Lupe
* 	 Verónica Loyo	 ...	Lola Gómez, La Torcasa
* 	 Andrés Soler	 ...	Carlos Salvatierra
* 	 Tito Novaro	 ...	Tomás Gutiérrez
* 	 José Muñoz	 ...	Bandit
* 	 Salvador Quiroz	 ...	Don Tacho, the barman
* 	 Lupe Inclán	 ...	Chencha
* 	 Pepe Nava

==External links==
*  

 
 
 
 
 


 