Bail Enforcers
{{Infobox film
| name           = Bounty Hunters
| image          = Bail Enforcers poster.jpg
| caption        = Teaser poster
| director       = Patrick McBrearty
| producer       = Chad Archibald Cody Calahan Michael Paszt
| writer         = Reese Eveneshen
| starring       = Trish Stratus Christian Bako Boomer Phillips
| music          = Norman Orenstein
| cinematography = Justin G. Dyck
| editing        = 
| studio         = Black Fawn Films
| distributor    = Star Entertainment (India)
| released       =  
| runtime        = 80 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Bail Enforcers is a 2011 film starring Trish Stratus, Christian Bako and Boomer Phillips directed by Patrick McBrearty. It marks the acting debut of former WWE wrestler Trish Stratus in a feature length film.  The film was retitled "Bounty Hunters" for its DVD release.

==Synopsis==
A group of people go around catching people who have skipped out on paying bail or have a bounty on their heads. One night the group, consisting of Jules (Trish Stratus), Chase (Boomer Phillips), and Ridley (Frank J. Zupancic), catches an informant with a $100,000 bounty. A mobster who wants this man more, offers the group a million dollars to turn the informant over to him.

==Cast==
*Trish Stratus as Jules 
*Christian Bako as Francis
*Boomer Phillips as Chase Thomson
*Frank J. Zupancic as Ridley
*Emily Alatalo as Hals Girl
*Enrico DiFede as Mario Antonio

==Awards==

Boomer Phillips who played Chase Thomson was nominated for a Canadian Comedy Award for Best Male Performance. 

==References==
 

==External links==
*  
*  

 
 
 
 