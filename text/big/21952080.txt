Zor (film)
 
{{Infobox Film
| name           = Zor
| image          = Zor (film).jpg
| image_size     = 
| caption        = 
| director       = Sangeeth Sivan
| producer       = Vivek Kumar
| writer         = 
| narrator       = 
| starring       = Sunny Deol Sushmita Sen Milind Gunaji Om Puri Vidyasagar
| cinematography = 
| editing        = 
| distributor    = Eros Entertainment 
| released       = February 13, 1998 (India)
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Zor is a Bollywood action film released in 1998. Zor was produced by Vivek Kumar and directed by Sangeeth Sivan.

== Plot ==

Zor is a film about how innocent people are targeted by mafia and politicians to turn them into terrorist for their benefits. Arjun (Sunny Deol) a Photographer is only a son of Police Commissioner (Anupam Kher) and happy with his Mother, younger sister and grandmother. He meets Aarti (Sushmita Sen) a low profile reporter in a small daily who wants to make name as a famous reporter and both fall in love. Both decides to marry but there is problem.

Arjun tells Nisha that they need to wait for an approval from his close friend Iqbal (Milind Gunaji) who is former Army captain (Expert in explosive) before they could get married. Meanwhile Arjuns father is blamed for letting off an unknown terrorist and declared as traitor. Iqbal also present at Arjuns home during Arjuns sister engagement while police arrest Commissioner. Arjun knows it is a conspiracy against his dad and sets off to find the vanished terrorist and catch the people behind the conspiracy to restore his fathers dignity. Meanwhile Iqbal leaves Arjuns home without any clue.

This makes Arjun suspicious about Iqbal and goes to Kashmir, he survived a terrorist attack there and found Iqbal is the leader of the group which attacked him. Iqbal tells Arjun why he became a terrorist from a patriotic army captain. Iqbal had a family and wife whom he loved more than anything however he lost them in a communal riot in his village and a communal leader Shah Alam (Om Puri) took him along hundreds of youths to prepare them to fight against the people who destroyed their village. Gradually Iqbal is turned into anti-national and eventually a leader of terrorist group.

Upon hearing Iqbals story Arjun explained him that it is all a conspiracy to make Iqbal a terrorist as he was an expert in explosives serving in Indian Army and to utilise his skills to produce explosive to destroy his own country and his own people. Iqbal finally understands and accept to offer help to Arjun.

After hearing that Iqbal has left him Shah Alam inform his crime partner Swamiji. Swamiji goes to Ajays home and threatens him to finish his family if he does not keep quiet. Swami and Alam also looking for Iqbal to kill him as he is going to be an evidence against them.

Finally when Commissioner is about to be pronounced as culprit Arjun brings Iqbal to court after going through lot of bloodshed. Arjun clarify to judge that its people like Swamiji and Alam who are the real traitors who create terrorist out of innocent people like Iqbal. Iqbal gives all evidence against Alam and Swamiji thus court declaring Iqbal and Commissioner as innocent and releasing them. Alam is arrested however Swamiji refused to give in saying that his followers will create a ruckus if he is arrested however Arjun with help of his media friends have live telecast entire proceedings of court on a national channel hence revealing Swamijis real nature to his followers. His own followers beats him to death. Arjun has accomplished his mission after restoring his Fathers pride.

== Cast ==
*Sunny Deol – Arjun Singh
*Sushmita Sen – Aarti
*Milind Gunaji – Iqbal Khan
*Om Puri – Shah Alam
*Anupam Kher - Police Commissioner Uday Singh

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mein Kudi Anjaani"
| Hema Sardesai
|- 
| 2
| "Tere Pyar Mein"
| Shankar Mahadevan, Hema Sardesai
|-
| 3
| "Pehli Pehli Baar"
| Anuradha Sriram
|-  
| 4
| "Koi Dekh Raha"
| Udit Narayan, Kavita Krishnamurthy
|- 
| 5
| "Zor Dekho"
| Gopal Rao 
|-  
| 6
| "Gum Na Karo"
| Sonu Nigam, Kavita Krishnamurthy
|}

== References ==
  imdb 
http://www.imdb.com/title/tt0264210/

==External links==
*  

 
 
 
 