Let Go (film)
{{Infobox film
| name           = Let Go
| director       = Brian Jett
| producer       = Spencer R. Stouffer, Ed Asner
| writer         = Brian Jett
| starring       = David Denman Gillian Jacobs
| released       =  
| studio         = Thousand Miles Entertainment
| runtime        = 95 minutes
| country        = United States
| language       = English
}}
Let Go is a 2011 comedy-drama film starring David Denman. It was written and directed by Brian Jett. 

==Plot==
The film centers around the interlocking lives of a bored parole officer and three eccentric ex-convicts recently placed under his supervision.

==Cast==
*David Denman as Walter Dishman
*Gillian Jacobs as Darla DeMint Kevin Hart as Kris Styles
*Edward Asner as Artie Satz  
*Simon Helberg as Frank
*Maria Thayer as Beth
*Ogy Durham as Vanessa

==Production== The Office and Jacobs from Community (TV series)|Community, worked for SAG independent film scale.   Writer-director Brian Jett cited influences including Dustin Hoffman drama Straight Time.   

==Release==
The film was released October 25, 2011 at the Austin Film Festival, October 31, 2011 at the Savannah International Film Festival before being released in the United States in August 21, 2012. 

==Critical reaction==
CultureMap considered it a flop: they criticised it for poor technical quality, citing bad lighting, sound, and cinematography, and also felt the script didnt give a talented cast much to do. 

Magdalena Bresson praised the performances from a variety of actors known for TV comedy, and the films visual qualities, particularly its use of color. 

==References==
 

==External links==
*  

 
 