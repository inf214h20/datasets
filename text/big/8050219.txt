Beloved Rogues
{{Infobox film
| name           = Beloved Rogues
| image          = Beloved Rogues.jpg
| image size     =
| caption        = Al Santel
| producer       =
| writer         = Aaron Hoffman (Story) Al Santel (Scenario)
| narrator       =
| starring       = Clarence Kolb|C. William Kolb
| music          =
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       = January 15, 1917
| runtime        = 5 reels
| country        = United States English intertitles
}}
 1917 American silent comedy-drama film directed and written by Alfred Santell with the storyline by Aaron Hoffman. Starring Clarence Kolb|C. William Kolb.  

A surviving fragment, 1 reel, exists at the Library of Congress. 

==Plot==
The film is about a business deal involving a personal endowment.

==Cast==
*Clarence Kolb|C. William Kolb ....  Louie Vanderiff Max M. Dill ....  Mike Amsterdammer
*May Cloy ....  Madge
*Clarence Burton ....  Jack Kennedy
*Harry von Meter ....  Andrews
*Tom Chatterton

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 