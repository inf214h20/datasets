The Ring (1927 film)
 
 
{{Infobox film
| name           = The Ring
| image          = Thering1.jpg
| border         = yes
| alt            =
| caption        = Theatrical release poster
| director       = Alfred Hitchcock
| producer       = John Maxwell
| writer         = Alfred Hitchcock
| starring       = {{Plainlist|
* Carl Brisson
* Lillian Hall-Davis Ian Hunter
* Forrester Harvey
}}
| music          = Xavier Berthelot  
| cinematography = Jack E. Cox
| editing        =
| studio         = British International Pictures
| distributor    = Wardour Films
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = Silent film with English intertitles
| budget         =
| gross          =
}} silent sports Ian Hunter. It is one of Hitchcocks nine surviving silent films. The Ring is Hitchcocks only original screenplay although he worked extensively alongside other writers throughout his career.

==Production==
The film was made by at Elstree Studios by the newly established British International Pictures who emerged as one of the two British major studios during the late 1920s and began hiring leading directors from Britain and abroad. It was Hitchcocks first film for the company, after joining from Gainsborough Pictures. It was also the first ever film to be released by the company. 

===Inception===
Hitchcock was only 28 years old when he directed The Ring, but this was already the young filmmakers fourth film. Hitchcock regularly attended boxing matches in London where he lived and he was struck by the fact that a good number of the spectators appear from good backgrounds and dressed in white. He also noticed that fighters were sprinkled with champagne at the end of each round. It was these two details that persuaded the young Hitchcock to start work on The Ring.

===Screenplay=== Easy Virtue, Gainsborough company, Hitchcock was frustrated and jumped at the chance to develop an idea of his own. Surprisingly, The Ring is Hitchcocks one and only original screenplay, although he worked extensively alongside other writers throughout his career. Colleagues at the studio were impressed by the neatness of his script and its writers grasp of structure. Whats more, writing for silent films came naturally to a director who already thought in visual terms. He was much less comfortable with dialogue, which goes some way to explain why he took no sole writing credit in any later films.

===Directing=== The Man Who Knew Too Much, most notably during the climactic boxing sequences.

==Synopsis== Ian Hunter).

==Reception==
The film was a major critical success on its release.  However, when it went on general release it was considered a box office failure. 

==Film restoration==
The restoration was done by the BFI National Archive in association with media company Canal+ in 2005. Principal restoration funding was provided by the Hollywood Foreign Press Association and The Film Foundation. Additional funding provided by Deluxe 142 and The Mohammed S. Farsi Foundation. A restored and remastered print of the film was released on DVD by Lionsgate Home Entertainment in 2007.

==Cast==
* Carl Brisson as One-Round Jack Sander
* Lillian Hall-Davis as Mabel (as Lilian Hall Davis) Ian Hunter as Bob Corby
* Forrester Harvey as James Ware
* Harry Terry as Showman
* Gordon Harker as Jacks Trainer Charles Farrell as Second  
* Clare Greet as Fortune Teller  
* Tom Helmore  
* Minnie Rayner as Boxing Contestants Wife  
* Brandy Walker as Spectator  
* Billy Wells as Boxer  

==Film + music live staging== Alto saxophonist and rapper Soweto Kinch composed a background score for the film. It was screened with the live performance by Kinch and Team. 
 17th International Film Festival of Kerala on the inaugural day 

==References==
 

==Bibliography==
* Ryall, Tom. Alfred Hitchcock and the British Cinema. Athlone Press, 1996.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 