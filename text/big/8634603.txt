The Plank (1979 film)
 Plank}}
{{Infobox film
| name     = The Plank
| image    =
| caption  =
| director = Eric Sykes
| producer = Dennis Kirkland
| writer   = Eric Sykes
| starring = Eric Sykes Arthur Lowe
| music    = Alan Braden
| cinematography =
| editing  =
| distributor =
| released =  
| runtime  = 30 minutes
| country  = United Kingdom
| language = English
| budget   =
}} 1967 film, ITV network. Like the original, it has an all-star cast of British comedians and other celebrities; although only Sykes, Jimmy Edwards and Kenny Lynch reprise their previous roles.

Although not literally a silent film, it has little spoken dialogue. Instead the film is punctuated by grunts, other vocal noises and sound effects.

==Outline==
When two builders find that a floorboard is missing, they buy a replacement floorboard and return with it through the streets, causing unexpected  .
 Festival Rose dOr, held in Montreux, Switzerland.

==Cast==
{{columns-list|2|
* Eric Sykes
* Arthur Lowe
* Jimmy Edwards
* Lionel Blair
* Henry Cooper
* Harry H. Corbett
* Bernard Cribbins
* Robert Dorning
* Diana Dors
* Charlie Drake
* Liza Goddard
* Deryck Guyler Charles Hawtrey
* Frankie Howerd
* James Hunt
* Wilfrid Hyde-White
* Joanna Lumley
* Kenny Lynch Brian Murphy
* Kate OMara
* Ann Sidney
* Reg Varney
* Frank Windsor
}}

==See also== The Big Freeze (1993).

==External links==
*  at BBC Online
* 
* 

 
 
 
 
 
 
 
 

 