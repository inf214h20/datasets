Perfume (2001 film)
 
{{Infobox film
| name           = Perfume
| image          = Perfume rymer dvd cover.jpg
| caption        = DVD cover
| border         = yes
| director       = Michael Rymer Hunter Carson
| producer       = L. M. Kit Carson Cynthia Hargrave Nadia Leonelli Fredrik SundwallSundwall
| writer         = Michael Rymer L. M. Kit Carson
| starring       = Estella Warren Jeff Goldblum Mariel Hemingway Carmen Electra Omar Epps Peter Gallagher
| music          = Adam Plack
| cinematography = Rex Nicholson
| editing        = Dany Cooper Lions Gate Films
| released       = January 26, 2001
| runtime        = 106 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 2001 film about the fashion industry in New York City.

==Plot==
Prior to a major show, the people involved find themselves embroiled in their own personal battles.

Lorenzo Mancini is a famous designer who learns that he is dying. With this knowledge, he attempts to make amends with his former wife, Irene, and his companion Guido. In addition, he uses his time to try his best to convince his son Mario to not merge their family business with the hip-hop fashion industry.

==Cast==
* Estella Warren.....Arrianne
* Jeff Goldblum.....Jamie
* Mariel Hemingway.....Leese Hotton
* Carmen Electra.....Simone
* Omar Epps.....J.B.
* Peter Gallagher.....Guido
* Paul Sorvino.....Lorenzo Mancini
* Sônia Braga.....Irene Mancini
* Michael Sorvino.....Mario Mancini
* Jared Harris.....Anthony
* Rita Wilson.....Roberta
* Michelle Forbes.....Francene
* Leslie Mann.....Camille
* Harris Yulin.....Phillip
* Joanne Baron.....Janice Michelle Williams.....Halley

==Awards and nominations==
Independent Spirit Awards
* Nominated—Producers Award: Nadia Leonelli

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 