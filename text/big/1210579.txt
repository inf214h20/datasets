The Winter Guest
{{Infobox film
| name           = The Winter Guest
| image          = Winter guest.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Alan Rickman
| producer       = Steve Clark-Hall
| writer         = Sharman Macdonald Alan Rickman
| narrator       =
| starring       = Emma Thompson Phyllida Law Sean Biggerstaff Arlene Cockburn Gary Hollywood Douglas Murphy Sheila Reid Sandra Voe
| music          = Michael Kamen
| cinematography = Seamus McGarvey
| editing        = Scott Thomas
| studio         = Capitol Films
| distributor    = Fine Line Features
| released       = December 24, 1997
| runtime        = 108 minutes
| country        = United Kingdom
| language       = English
}}

The Winter Guest (1997) was British actor Alan Rickmans debut as a director, and stars Phyllida Law and Emma Thompson.

==Plot==
Set in Scotland on one wintry day, the film focuses on eight  people; a mother and daughter, Elspeth (Phyllida Law) and Frances (Emma Thompson); two young boys skipping school, Sam (Douglas Murphy) and Tom (Sean Biggerstaff); two old women who frequently attend strangers funerals, Chloe (Sandra Voe) and Lily (Sheila Reid); and two teenagers Nita (Arlene Cockburn) and Alex (Gary Hollywood). The film consists primarily of the 
interactions between the characters.

==History==
The film is based on Sharman MacDonalds play, premiered at the West Yorkshire Playhouse (in the Quarry studio theatre, 23 January to 18 February 1995) before transferring to the Almeida Theatre in London (14 March to 15 April 1995).

Like the film it was also directed by Rickman, starring Law, Reid, Voe and John Wark, with Sian Thomas|Siân Thomas in the role of Frances, played in the film by Emma Thompson.

==Reception==
The film was met warmly by critics, with Thompson winning an award at the   in 1995.

===Awards and nominations===
*British Independent Film Awards (UK)
**Nominated: Best British Actress (Emma Thompson)

*Brussels International Film Festival (Belgium)
**Won: Audience Award (Alan Rickman)
 Chicago Film Festival (USA)
**Won: Gold Hugo &ndash; Best Film (Alan Rickman)

*Czech Lion (Czech Republic)
**Nominated: Best Foreign Language Film (Alan Rickman)

*European Film Awards
**Nominated: Best Actress (Emma Thompson)

*Venice Film Festival (Italy)
**Won: CinemAvvenire Award  (Alan Rickman; tied with A Ostra e o Vento and Giro di lune tra terra e mare).
**Won: OCIC Award (Alan Rickman)
**Won: Pasinetti Award &ndash; Best Actress (Emma Thompson)
**Nominated: Golden Lion (Alan Rickman)

==External links==
*  
*  

 
 
 
 
 
 
 
 

 