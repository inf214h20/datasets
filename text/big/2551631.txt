Underclassman
 
{{Infobox film
| name           = Underclassman
| image          = Underclassman film.jpg
| caption        = Theatrical release poster
| director       = Marcos Siega Robert L. Levy Andrew Panay David Wagner
| starring       = Nick Cannon Shawn Ashmore Roselyn Sánchez Kelly Hu Hugh Bonneville Cheech Marin BT
| cinematography = David Hennings
| editing        = Nicholas C. Smith
| studio         = Miramax Films
| distributor    = Miramax Films
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $5,655,459
}}
 action comedy directed by Marcos Siega, and stars Nick Cannon, Shawn Ashmore, Roselyn Sánchez, Kelly Hu, Hugh Bonneville, and Cheech Marin. It was released on September 2, 2005, had been originally set for a release in 2004.

==Plot==
Tracy (Trey) Stokes (Nick Cannon|Cannon) is a 23-year old undercover cop and goes to the wealthy private Westbury School to figure out the death of a student. He becomes friends with Rob Donovan (Shawn Ashmore|Ashmore) and soon figures out that Donovan has been stealing the cars at parties. Near the end, it is revealed that the principal (Hugh Bonneville|Bonneville) of the school had been blackmailing Donovan, thus he is the ringmaster of the crimes. After being kicked off the force by his father-figure police chief (Cheech Marin|Marin), Trey solves the case (with the help from Donovan) and is reinstated.

==Cast==
*Nick Cannon as Tracy (Tre) Stokes
*Roselyn Sánchez as Karen Lopez
*Kelly Hu as Lisa Brooks
*Shawn Ashmore as Rob Donovan
*Angelo Spizzirri as David Boscoe
*Hugh Bonneville as Headmaster Felix Powers
*Cheech Marin as Captain Victor Delgado Sarah Jane Morris as Jamie (uncredited)

==Reception==
Underclassman received negative reviews from critics. On Rotten Tomatoes, only 6% of the critics gave it positive write-ups. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 