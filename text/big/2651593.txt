Water (2005 film)
 
 
 
{{Infobox film
| name           = Water
| image          = Water (2005 film) cover art.jpg
| caption        = Film poster
| director       = Deepa Mehta David Hamilton
| screenplay     = Anurag Kashyap 
| story          = Deepa Mehta John Abraham Manorama
| music          = A. R. Rahman   Mychael Danna (Background Score)
| cinematography = Giles Nuttgens
| editing        = Colin Monie Fox Searchlight Pictures  (US)   Mongrel Media  (Canada)   B.R. Films  (India) 
| released       =   David Hamilton Productions 
| runtime        = 114 minutes
| country        = Canada India
| language       = English Hindi 
| budget         = 
| gross          =  $10,422,387  .   
}}
Water ( ), is a 2005 Canadian film written and directed by  , published by Milkweed Press. Sidhwas earlier novel, Cracking India was the basis for Earth (1998 film)|Earth, the second film in the trilogy. Water is a dark introspect into the tales of rural Indian widows in the 1940s and covers controversial subjects such as misogyny and ostracism. The film premiered at the 2005 Toronto International Film Festival, where it was honoured with the Opening Night Gala, and was released across Canada in November of that year. It was first released in India on 9 March 2007. 
 John Abraham, and Sarala Kariyawasam in pivotal roles and Kulbhushan Kharbanda, Waheeda Rehman, Raghuvir Yadav, and Vinay Pathak in supporting roles. Featured songs for the film were composed by A. R. Rahman, with lyrics by Sukhwinder Singh and Raqeeb Alam while the background score was composed by Mychael Danna. Cinematography is by Giles Nuttgens, who has worked with Deepa Mehta on several of her films.

In 2008, inspired by the film, Dilip Mehta directed a documentary, The Forgotten Woman about widows in India. The film was also written by Deepa Mehta. 

== Plot ==

The film is set in the year 1938, when India was still under British rule. Child marriage was common practice back then. Widows had a diminished position in society, and were expected to spend their lives in poverty and worship of God. Widow remarriages were legalised by the colonial laws, but in practice, they were largely considered taboo.

When Chuyia ( ), the now second-youngest of the widows, by taking her across the water to the customers. Kalyani was forced into prostitution as a child to support the ashram.

Shakuntala (Seema Biswas) is perhaps the most enigmatic of the widows. She is attractive, witty and sharp. She is also one of the few widows who can read. She exudes enough anger that even Madhumati leaves her alone. Quiet and reserved, Shakuntala is caught between her hatred of being a widow and her fear of not being a sincere, dedicated widow. Shakuntala is a very devout Hindu who seeks the counsel of Sadananda (Kulbhushan Kharbanda), a gentle-looking priest in his late forties who recites the scriptures to the pilgrims who throng the ghats of the holy city. It is he who makes Shakuntala aware of her situation, eventually giving her the necessary intellectual input to separate true faith from the hypocrisy and superstition that makes her and the other widows lives a misery. Shes attached to Chuyia, because deprived from her liberties and freedom of choices from a young age, she sees herself reflected in Chuyia; and strives to give her what she lacked.
 John Abraham), a young and charming upper-class follower of Mahatma Gandhi and of Gandhism. Despite her initial reluctance, Kalyani feels attracted to the young man and eventually buys into his dream of marriage and a fresh life in Kolkata|Calcutta. She eventually agrees to go away with him.

Her plan is disrupted when Chuyia, in her innocence, inadvertently blurts about the secret affair with Narayan while massaging Madhumati one evening. Enraged at losing a source of income and afraid of the imminent social disgrace, Madhumati locks Kalyani up. Much to everyones surprise, Shakuntala, the usually God-fearing widow, unlocks the door of the hovel and lets Kalyani out to go meet Narayan for the planned rendezvous, and he ferries her across the river to take her home. The journey however, does not culminate in the happy ending that Kalyani had hoped for, as she recognises Narayans bungalow as that of one of her former clients, and it turns out that Narayan is the son of one of the men she had slept with. In the shock of realisation, she demands that he turn around the boat and take her back. A confrontation with his father reveals to Narayan the reason of Kalyanis sudden change of heart. Disgusted to know the truth, he decides to walk out on his father and join Mahatma Gandhi (Mohan Jhangiani, actor; Zul Vilani, voice). He arrives at the ashram to take Kalyani with him, only to find out that Kalyani has drowned herself in humiliation and grief.

Meanwhile, Madhumati sends Chuyia away with Gulabi, to be prostituted as a replacement for Kalyani for a waiting client (presumably Narayans friends father). Shakuntala finds out and runs out to prevent the worst, but she only arrives at the shore in time for Chuyias return. As a result of being raped, the child is deeply traumatised and practically catatonic. Cradling Chuyia, Shakuntala spends the night sitting at the shore. Walking through town with Chuyia in her arms she hears about Gandhi being at the train station, ready to leave town. Intuitively, she follows the crowd to receive his blessing before his departure. As the train is departing, in an act of despair, Shakuntala runs along the train, asking people to take Chuyia with them, and to put her under the care of Gandhi. She spots Narayan on the train and in a last effort gives Chuyia to him. The train departs leaving teary eyed Shakuntala behind, taking Chuyia into a brighter future.

== Cast ==
* Seema Biswas as Shakuntala
* Lisa Ray as Kalyani John Abraham as Narayan
* Waheeda Rehman as Bhagavati, Narayans Mother
* Sarala Kariyawasam as Chuyia
* Buddhi Wickrama as Baba
* Ronica Sajnani as Kunti Manorama as Madhumati
* Rishma Malik as Snehalata
* Seema Biswas as Gyanvati
* Vidula Javalgekar as Patiraji (auntie)
* Daya Alwis as Saduram
* Raghuvir Yadav as Gulabi
* Vinay Pathak as Rabindra
* Kulbhushan Kharbanda as Sadananda
* Gerson Da Cunha as Seth Dwarkanath
* Mohan Jhangiani as Mahatma Gandhi
* Zul Vilani as Mahatma Gandhi (voice)

== Release ==
The film debuted on 8 September 2005 at the Toronto International Film Festival and opened in other theatres at the dates given below. After several controversies surrounding the film in India, the Indian censor boards cleared the film with a "U" certificate. It was released in India on 9 March 2007. 

{| border=1 align=left cellpadding=4 cellspacing=0 width=300 style="margin: 0 0 1em 1em; background: #ccccff; border: 1px #8888cc solid; border-collapse: collapse; font-size: 95%;"
|- style=background:#ccccff
! Region
! Release date
! Festival or Distributor
|- Canada
| 8 September 2005 Mongrel Media
|- United States|USA 2 October 2005 South Asian Literary and Theater Arts Festival
|- Spain
| 2 October 2005 Valladolid International Film Festival
|- Canada
| 4 November 2005
| style="background: #f7f8ff; white-space: nowrap;" |
|- Australia
| 13 April 2006 Dendy Films
|- United States|USA 19 April 2006 Indian Film Festival of Los Angeles
|- United States|USA 26 April 2006 Indianapolis International Film Festival
|- United States|USA 28 April 2006 Fox Searchlight Pictures
|- Switzerland
| 15 August 2006 Filmcoopi Zurich AG
|- India
| 9 March 2007
| style="background: #f7f8ff; white-space: nowrap;" |B.R. Films
|}
 

== Reception ==

=== Awards and nominations===
 
{|class="wikitable plainrowheaders sortable"
|-
!scope="col" | Date of ceremony
!scope="col" | Award
!scope="col" | Category
!scope="col" | Recipients and nominees
!scope="col" | Result
|- 25 February Best Foreign Language Film || Deepa Mehta ||  
|- 26th Genie 13 March 2006 Genie Awards or Canadian Screen Awards    Best Motion David Hamilton ||  
|- Best Director || Deepa Mehta ||  
|- Best Actress || Seema Biswas    ||  
|- Best Art and Production Design || Dilip Mehta ||  
|- Best Cinematography || Giles Nuttgens || 
|- Best Screenplay || Deepa Mehta ||  
|- Best Film Editing || Colin Monie ||  
|- Best Original Score || Mychael Danna ||   
|- 17 February Best Golden Kinnaree Film    || Deepa Mehta ||  
|- 20 January Broadcast Film Critics || Best Foreign Language Film || Deepa Mehta || 
|- December 17, Best Foreign Language Film || Deepa Mehta ||  
|- Best Non-European Director || Deepa Mehta ||  
|- January 9, 2007
| rowspan="2"| National Board of Review Top 5 Foreign Language Films
| Deepa Mehta  ||  
|- NBR Freedom of Expression Awards  World Trade Center) || 
|- December 11, 2006
|  rowspan="2"| New York Film Critics Online
| Top 10 Films 
| Deepa Mehta ||  
|-
| NYFCO Humanitarian Award 
| Deepa Mehta ||  
|- 14 October Best Silver Mirror Feature Film || Deepa Mehta ||    
|-
| 20 March 2006|| San Francisco International Asian American Film Festival || Best Narrative Audience Award || Deepa Mehta ||  
|- 17 December Best Foreign Deepa Mehta ||  
|- 21 October 2005
| rowspan="2"| Valladolid International Film Festival
| Best Youth Jury Film – In Competition 
| Deepa Mehta ||   
|-
| Best Golden Spike Film – In Competition 
| Deepa Mehta ||  
|- February 7, 2006
| rowspan="2"| Vancouver Film Critics Circle Best Canadian director || Deepa Mehta ||  
|- Best Canadian Actress || Lisa Ray ||  
|- 10 March 2007
| rowspan="2"| Young Artist Awards Best Leading Young Actress in a Feature Film || Sarala Kariyawasam ||  
|- Best International Family Feature Film || Deepa Mehta ||  
|}

=== Critical response ===
The film received high praise from Kevin Thomas, writing in the Los Angeles Times:
 
For all her impassioned commitment as a filmmaker, Mehta never preaches but instead tells a story of intertwining strands in a wholly compelling manner. "Water," set in the British colonial India of 1938, is as beautiful as it is harrowing, its idyllic setting beside the sacred Ganges River contrasting with the widows oppressive existence as outcasts. The film seethes with anger over their plight yet never judges, and possesses a lyrical, poetical quality. Just like the Ganges, life goes on flowing, no matter what. Mehta sees her people in the round, entrapped and blinded by a cruel and outmoded custom dictated by ancient religious texts but sustained more often by a familys desire to relieve itself of the economic burden of supporting widows. As a result, she is able to inject considerable humour in her stunningly perceptive and beautifully structured narrative. "Water" emerges as a film of extraordinary richness and complexity. 
 

Jeannette Catsoulis of The New York Times selected the film as NYT Critics Pick, calling it "exquisite"..."Serene on the surface yet roiling underneath, the film neatly parallels the plight of widows under Hindu fundamentalism to that of India under British colonialism." 
 Orientalist and racist stereotypes about the "exotic" and "strange" nature of Indian culture. 

The film received mostly positive reviews. Review aggregator Rotten Tomatoes reports that 91% of 90 professional critics have given the film a positive review, with a rating average of 7.6 out of 10.  The sites consensus is that "This compassionate work of social criticism is also luminous, due to both its lyrical imagery and cast." On Metacritic which assigns a weighted mean rating out of 100 reviews from critic, the film has a "universal acclaim" rating score of 77 based on 25 critics reviews. On IMDB it has a user ratings of 7.8 out of 10 by 10, 801 users.

Roger Ebert of Chicago Sun-Times called the film "The film is lovely in the way Satyajit Rays films are lovely and the best elements of Water involve the young girl and the experiences seen through her eyes. I would have been content if the entire film had been her story" and gave it three stars out of four.  Carrie Rickey of The Philadelphia Inquirer also praises Mehtas work on trilogy saying that "Profound, passionate and overflowing with incomparable beauty, Water, like the prior two films in director Deepa Mehtas "Elements" trilogy, celebrates the lives of women who resist marginalisation by Indian society."

== Soundtrack ==
 

== Controversies ==
Mehta had originally intended to direct Water in February 2000, with the actors Shabana Azmi, Nandita Das and Akshay Kumar. Her earlier film, Fire (1996 film)|Fire, however, had previously attracted hostility from the Hindutva|right-wing elements of the Indian polity, which objected to her subject matter and portrayal of conservative households in a negative light. Protestors organised protests and attacks on cinemas that screened that film. The day before filming of Water was due to begin, the crew was informed that there were complications with their location permits for filming. The following day, they learned that 2,000 protesters had stormed the ghats, destroying and burning the main film set and throwing the remnants into the Ganges in protest of what ultimately were revealed to be false accusations regarding the subject matter of the film.  Activist Arun Pathak also organised a suicide protest to stop the film production. 

The resulting tensions and economic setbacks led to several years of struggle as Mehta was eventually forced to film Water in Sri Lanka, rather than in India.  Finally Mehta was able to make the film, but with a new cast and under a false title (River Moon) in 2003. The struggle to make the film was detailed by Mehtas daughter, Devyani Saltzman, in a non-fiction book, Shooting Water: A Mother-Daughter Journey and the Making of the Film.   

== See also ==
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

== Notes and references ==
 

==Bibliography==
*  
*  

== External links ==
* 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 