Gamer (film)
{{Infobox film
| name           = Gamer 
| image          = Gamermovie.jpg
| caption        = Theatrical poster
| director       = Neveldine/Taylor
| producer       = {{Plainlist|
* Tom Rosenberg
* Gary Lucchesi
* Richard Wright
* Skip Williamson }}
| writer         = Neveldine & Taylor
| starring       = {{Plainlist|
* Gerard Butler
* Michael C. Hall
* Amber Valletta
* Logan Lerman
* Terry Crews Chris Ludacris Bridges
* Kyra Sedgwick }}
| music          = {{Plainlist|
* Robert Williamson
* Geoff Zanelli }}
| cinematography = Ekkehart Pollack
| editing        = {{Plainlist|
* Peter Amundson
* Fernando Villena }}
| studio         = Lakeshore Entertainment Lionsgate
| released       =  
| runtime        = 95 minutes 
| country        = United States
| language       = English
| budget         = $50 million 
| gross          = $40,828,540   
}} science fiction Brian Taylor. control human beings as players, and Logan Lerman as the player who controls him. Gamer was released in North America on September 4, 2009, and the United Kingdom on September 16, 2009.

==Plot==
In 2024, inventor and professional computer programmer, Ken Castle (Michael C. Hall), has revolutionized the gaming industry with his self-replicating nanites that replace brain cells and allow full control of all motor functions by a third party. Castles first application of this technology is a game called Society, which allows gamers to control a real person in a pseudo community (much like The Sims or Second Life). This allows players to engage in all manner of debauchery, such as deliberately injuring their "characters" and engaging in rough sex with random people. As a result, those who work as "characters" in Society are paid very well in compensation.
 life imprisoned lag problem in the game and no communication between the inmates and players, so there is a dangerous delay between the players control and inmates responses. Although the players control the inmates during movement, the inmate himself decides when he will shoot.

In Slayers, John "Kable" Tillman (Gerard Butler) is the most recognizable face and the best soldier due to having survived 27 matches, far more than any other participant (no other inmate than Kable has survived more than 10 matches). This marks him as one of the hardest targets and also as the crowds favorite. Simon (Logan Lerman), a seventeen-year-old superstar gamer from a wealthy family, is the player who exclusively controls him, also gaining fame for playing Kable, but he is mostly a spoiled brat.

While Castle is being interviewed by popular TV host Gina Parker Smith (Kyra Sedgwick), an activist organization called "Humanz" hijacks the broadcast and claims that Castle can use the nanite technology to control people against their will, but it is soon shut down. A faceless female visits Tillman, as he sits in his prison cell, giving him a picture of his family and taking some of his blood (he is told the purpose is to validate the authenticity of the autograph he gives her). Another inmate, Hackman (Terry Crews), taunts Tillman in the prison, threatening his family. Hackman comments that he has "no strings on him", meaning he is not controlled by an outside player, and thus has no dangerous lag, allowing him to fully control his movements and gain an advantage, since he is allowed to think and move for himself.

A file appears on Simons computer screen (walkietalkie.exe), an illegal mod created by the Humanz that will allow Simon to illegally speak with Tillman within the Slayers arena. After Hackman nearly kills Tillman, Tillman asks Simon to relinquish control during the next match, their 30th. The Humanz intervene again, providing this ability. The stranger visits Tillman in his cell again, warning him that Castle has no intention of letting him live, and that escape is the only option. Tillman asks for alcohol, which she provides. Tillman uses his free will in the 30th match to escape by successfully driving out of the deathmatch arena; the vehicle is fueled by ethanol from the vomit and urine containing the alcohol he has chugged before the match.

After his escape, news outlets report that he has been officially listed as  ) apartment by the female stranger, Trace (Alison Lohman), whose voice he recognizes as the girl who visited him in prison, and they speed away before Castles men arrive.

She takes him to the Humanz leader named Brother (Ludacris) and Dude (Aaron Yoo) who explains that the mind control technology can potentially be unwillingly used on anyone, leading to the "extinction of independent thought". The Humanz have created a cure, but it must be individualized for each person, hence the need to take Tillmans blood in prison. Tillman searches for Angie, who has been working as an avatar (known as Nika) for a heinous and obese Society player. After a violent confrontation with security he manages to enter the Society isolated world to break her out. He returns to the Humanz who are able to deactivate the nanite cells in Angies brain.

When the Dude and Brother examine Tillmans memories, Tillman reveals that he was part of the original experiment to use nanites in the brain. The first person to successfully be implanted with nanites was Tillmans close friend Scotch (Johnny Whitworth). In an experimental session, Tillman (controlled by Castle through the nanites) shot Scotch in the head, which landed Tillman on death row. When he heard about the Slayers game, Tillman volunteered so he would have a chance to be set free.

When Tillman discovers Castle adopted his young daughter, Delia (Brighid Fleming), Tillman infiltrates Castles mansion to get her back. He locates Castle who performs a song-and-dance number using mind-controlled Slayers as backup dancers, while a screen above plays footage of Castles men murdering Trace, Brother and Dude. The "dancers" then attack Tillman, who kills all of them one by one. Castle then leads Tillmann to a room with a basketball court where Castle reveals that, whereas the nanites implanted in other peoples brains are designed to receive commands, his nanites are designed for sending commands. Tillman fights Hackman on the court, this time defeating him easily by breaking his neck, since Hackman is under Castles control and unable to fight for himself.

To demonstrate his control, Castle beats Tillman savagely while stopping him from fighting back. Angie and Delia are brought onto the court after Castle reveals that the Humanz have been found and killed. Castle then makes Tillman crawl to his family, trying to force him to kill his own daughter, though Tillman is able to resist. However, Trace and Gina have escaped Castles slaughter, and broadcast the confrontation on every video screen across the country, exposing Castle and his plans for control. Seeing the broadcast, Simon is able to use his previous link with Tillman to aid him in resisting Castles control. The interference distracts Castle, and the pair wrestle for control of Tillmans mind.

Now with his control back, Tillman outwits Castle by suggesting that he imagines Tillman plunging the knife into his stomach, allowing him to do so (since Castles thoughts, no matter how small, create actions), and he stabs Castle. After Castle dies, Tillman turns to Castles technicians, who release everyone from nanite control upon Tillmans request. They walk away, after one states, "Well played, Kable".  The film closes with the Tillman family taking a trip down a country road, ending with the words "Game Over".

==Cast==
* Gerard Butler as John "Kable" Tillman, the highest-ranked warrior in the game Slayers. 
* Amber Valletta as Angie "Nika" Roth Tillman, Kables wife, an avatar in Society. 
* Michael C. Hall as Ken Castle, creator of Society and Slayers. The main antagonist of the movie. 
* Logan Lerman as Simon Silverton, the 17-year-old gamer "playing" Kable. 
* Kyra Sedgwick as Gina Parker Smith, a famous talk show host who meets the Humanz and investigates them.
* Ludacris as Brother, the spokesperson and leader of the Humanz.
* Aaron Yoo as Dude, a member of the Humanz and a hacker.
* Alison Lohman as Trace, a member of the Humanz.
* John Leguizamo as Freek, an inmate who befriends Kable. 
* Terry Crews as Hackman, a psychopathic inmate sent to murder Kable.
* Zoë Bell as Sandra, an inmate.
* Mimi Michaels as Stikkimuffin, another teenage gamer. 
* Milo Ventimiglia as Rick Rape Jonathan Chase as Geek Leader, the head of Castles technical team.
* Keith David as Agent Keith, a CIA agent.

==Production== Brian Taylor, the creators of Crank (film)|Crank (2006), to produce a "high-concept futuristic thriller" called Game. Neveldine and Taylor wrote the script for Game and were slated to direct the film, while actor Gerard Butler was cast into the lead role. 
 Red One digital cameras, which allowed the special effects team to begin work normally done in post-production after each days shooting. 

In March 2009, the films working title was changed from Game to Citizen Game.   In May 2009, another name change was announced, the new name being Gamer.   

==Reception==
Critical reception has been primarily negative. The film holds a 28% "Rotten" rating from 75 reviews on Rotten Tomatoes; the sites consensus being "with all of the hyperkinetic action and none of the flair of Mark Neveldine and Brian Taylors earlier work, Gamer has little replay value." 
 New York Xerox of a Xerox" and citing a number of films it supposedly takes elements from, including The Matrix and Rollerball (1975 film)|Rollerball.  RVA Magazine noted that Gamer s plot was overly similar to The Condemned and commented that Gamer "hates its primary audience" and "tries to criticize the commercialization of violence, even though it itself is commercialized violence". 

Cultural critic Steven Shaviro authored a 10,000 word defense and analysis of the film that he posted online, and eventually re-worked into the penultimate chapter of his book, Post-Cinematic Affect (Zer0 Books, 2010).   

===Box office===
Gamer was not a box office success. It had an opening day gross of $3.3 million and ranked fourth at the box office. In total, the film earned $9,156,057 in its opening weekend. Overall, the film grossed $20,534,907 in the United States and Canadian box office with a worldwide cumulative of $40,828,540. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 