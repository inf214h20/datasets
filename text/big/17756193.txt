Le bon roi Dagobert (1984 film)
{{Infobox film
| name = Dagobert
| image = 
| alt =
| caption =
| director = Dino Risi
| producer = Renzo Rossellini
| story = Age & Scarpelli
| writer = Dino Risi Age & Scarpelli Gérard Brach
| starring = Ugo Tognazzi Coluche Michel Serrault
| music = Guido De Angelis & Maurizio De Angelis
| cinematography = Dante Ferretti
| editing = 
| studio =  Gaumont
| released = 1984
| runtime = 118 min
| country = Italy-France
| language = Italian-French
| budget =
| gross =
}}

Le Bon Roi Dagobert (Good King Dagobert, in  .

==Plot==
The film is inspired by a popular song against the French monarchy, created during the French Revolution.

During the 7th century, the lazy and messy King Dagobert I goes to Rome to ask Pope Honorius I for forgiveness of his sins of revelry and fornication. But Dagobert does not know that the pope, while he is still traveling, was replaced in a conspiracy by a perfect double. The replacement is a crude and rude man, even more stupid than Dagobert.

==Cast and roles==
* Coluche - Dagobert I
* Michel Serrault - Otarius Pope Honorius and his look-alike
* Carole Bouquet - Héméré
* Isabella Ferrari - Chrodilde
* Michael Lonsdale - Saint Eligius
* Venantino Venantini - Demetrius, merchant
* Karin Mai - Nanthild, the Queen
* Francesco Scali - Landek
* Antonio Vezza - Rutilius
* Sabrina Siani - Berthilde
* Marcello Bonini Olas - Heraclius, the Emperor of Byzance
* Isabella Dandolo - Alpaide
* Federica Paccosi - Ragnetrude
* Gea Martire - Philliria

==External links==
*  
 

 

 
 
 
 
 
 
 
 
 