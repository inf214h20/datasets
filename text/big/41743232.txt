That Demon Within
 
 
{{Infobox film
| name           = That Demon Within
| image          = That Demon Within poster.jpg
| border         = 
| alt            = 
| caption        = Official poster
| director       = Dante Lam
| producer       = Candy Leung Albert Lee Ren Yue
| writer         = 
| screenplay     = Jack Ng Dante Lam
| story          = Dante Lam
| based on       =  
| narrator       = 
| starring       = Daniel Wu Nick Cheung
| music          = Leon Ko
| cinematography = Kenny Tse Patrick Tam
| studio         = Emperor Motion Pictures Sil-Metropole Organisation
| distributor    = Emperor Motion Pictures
| released       =   
| runtime        = 111 mins
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = US$20,425,934 
}}

That Demon Within (Mo Ging,   psychological thriller film directed by Dante Lam and starring Daniel Wu and Nick Cheung. It premiered at the 64th Berlin International Film Festival in February 2014 and also included on the 16 edition of Far East Film Festival in Udine .  The film was released on 18 April 2014. 

The movie was presented by Dante Lam at the 16 Far East Film Festival 2014 in Udine with The Unbeatable (2013).

==Plot==
Reclusive cop Dave (Daniel Wu) unwittingly saves the life of criminal gang leader Hon Kong (Nick Cheung) by donating his blood, thus symbolizing that despite diametrically opposed outer appearances they are essentially made of the same stuff. The gang members hide their faces behind traditional demon masks when committing their violent crimes. During psychotic episodes Dave experiences his own demons within as he sets out to play off the gang members against each other, resulting in everyones annihilation. It is then revealed that Dave was brought up by a high expressed emotion father in a socio-economically disadvantaged living environment. He witnessed his fathers death inadvertently caused by the responsible policeman who resembled Hon Kong. As a young, innocent soul who had lost the father, he impulsively went on to take revenge, resulting in excessive guilt that predisposed the onset of psychosis later in his life. The event whereby he rescued Hon Kong reminded him of his suppressed memories as a child who had done wrong but trying the hardest to make reparation. Before his inevitable death, there was an opportunity for him to resolve his subconscious intrapersonal conflicts which was to have the courage to fix a mistake done.

==Cast==
*Daniel Wu as Dave Wong
*Nick Cheung as Hon Kong / Riot police officer
*Christie Chen as Liz Kwok
*Andy On as Ben Chan
*Liu Kai-chi as Broker
*Dominic Lam as Inspector Mok
*Joseph Lee as Effigy
*Stephen Au as MC
*Chi Kuan-chun as Daves father
*Ken Wong as Smart Ass
*Deep Ng as Kwong
*Samuel Leung as Rookie
*Lam Tsing as Chanter
*Philip Keung as Station Sergeant
*Astrid Chan as Stephanie
*Fung So-bor as Granny
*Wu Ying-man as Private nurse

==References==
 

==External links==
* 

 
 

 
 
 