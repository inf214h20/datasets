Ekka Raja Rani
{{Infobox film
| name           = Ekka Raja Rani
| image          = Ekka Raja Rani.jpg
| image_size     =
| caption        = Promotional Poster
| director       = 
| producer       = Xavier Marquis
| writer         =  
| narrator       =  Govinda  Vinod Khanna Paresh Rawal
| music          = Nadeem-Shravan
| cinematography = 
| editing        = 
| distributor    = Mark Films
| released       = 24 June 1994
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}}
 1994 Bollywood comedy drama film starring Govinda (actor)|Govinda. 

==Cast==
*Vinod Khanna as  Vishal Vicky Kapoor Govinda  as  Sagar
*Ayesha Jhulka as  Barkha
*Ashwini Bhave as  Asha
*Paresh Rawal as  Nageshwar Rao
*Tinnu Anand
*Anil Dhawan as Pasha
*Bharat Kapoor as  Police Commissioner
*Johnny Lever as  Guruji (Dancemaster)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1 
| "Ishq Karoge To Dard Milega"
| Kumar Sanu, Udit Narayan & Alka Yagnik
|- 
| 2
| "Yeh Neeli Peeli Chudiyaan"
| Udit Narayan, Alka Yagnik
|- 
| 3
| "Pyar Karo To Aise"
| Kumar Sanu
|- 
| 4
| "Dil Ko Zara Sa Aaram Denge (Duet)"
| Kumar Sanu, Alka Yagnik
|- 
| 5
| "Dil Ko Zara Sa"
| Babul Supriyo
|- 
| 6
| "Mere Savre Savaria"
| Bali Brahmbhatt, Sapna Awasthi
|- 
| 7
| "Dil Milne Ko Tarasta Hai"
| Vinod Rathod, Alka Yagnik
|-
| 8
| "Tadpun Ya Pyar Karun"
| Alka Yagnik
|- 
| 9
| "Pyar Karo To (Duet)"
| Kumar Sanu, Udit Narayan & Alka Yagnik
|}

==References==
 

== External links ==
*  

 
 
 
 
 


 
 