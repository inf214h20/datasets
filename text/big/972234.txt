Blue Desert (film)
{{Infobox film
| name           = Blue Desert
| image          = Blue Desert.jpg
| caption        = DVD cover
| director       = Bradley Battersby
| producer       =
| writer         = Arthur Collis Bradley Battersby
| narrator       =
| starring       = Courteney Cox D. B. Sweeney Craig Sheffer Sandy Ward
| music          = Joel Goldsmith
| cinematography = Paul Murphy (cinematographer)
| editing        = Debra Bard
| distributor    = First Look Pictures
| released       =  
| runtime        = 98 min.
| country        = United States English
| budget         =
}}

Blue Desert (1991 in film|1991) is a psychological thriller film starring Courteney Cox and D. B. Sweeney, directed by Bradley Battersby. The original music score is composed by Jerry Goldsmith and Joel Goldsmith. The filming locations were Inyokern, California and Red Rock Canyon State Park, Cantil, California.

==Plot summary==
A rape victim, comic book artist Lisa Roberts is given the runaround by the New York police. Tired with city life, she heads for the wide open spaces of Arizona. Not long afterward, she is propositioned by lowlife Randall Atkins. She reports this to sympathetic local policeman Steve Smith, who replies matter-of-factly that this is not the first time that Atkins has been accused of a sexual offense. To her amazement, Roberts is later visited by Atkins, who agitatedly warns her not to trust the sweet-natured policeman. Someone is lying about something, and Roberts plainly does not know what to believe. When she finds out, it is nearly too late.

==Main cast==
* Courteney Cox – Lisa Roberts
* D. B. Sweeney – Steve Smith
* Craig Sheffer – Randall Atkins
* Steve Ward – Walter
* Philip Baker Hall – Joe

==External links==
*  
*  
*  , focusing on Courteney Cox as the lead character

 
 
 
 
 

 