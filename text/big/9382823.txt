Below (film)
 
{{Infobox film
| name           = Below
| image          = Belowposter2002.jpg
| caption        = Theatrical release poster
| director       = David Twohy
| producer       = Darren Aronofsky Sue Baden-Powell Eric Watson
| writer         = Lucas Sussman Darren Aronofsky David Twohy	
| starring       = Bruce Greenwood Matthew Davis Olivia Williams Holt McCallany Scott Foley Zach Galifianakis Jason Flemyng Dexter Fletcher
| music          = Graeme Revell Tim Simonec Ian Wilson
| editing        = Martin Hunter
| studio         = Dimension Films Protozoa Pictures
| distributor    = Miramax Films Dimension Films
| released       =  
| runtime        = 105 minutes
| country        = United States English
| gross          = $605,562
}}
Below is a 2002 World War II horror film directed by David Twohy. It was written by Lucas Sussman, Darren Aronofsky and David Twohy, and stars Bruce Greenwood, Olivia Williams, Matthew Davis, Holt McCallany, Scott Foley, Zach Galifianakis, Jason Flemyng and Dexter Fletcher. The film tells the story of a United States Navy submarine that experiences a series of supernatural events while on patrol in the Atlantic Ocean in 1943.

Below was filmed on location in Lake Michigan for exteriors (using the World War II-era U.S. Navy submarine  ) and at Pinewood Studios.

==Plot== PBY Catalina depth charges in the process. Later, the commanding officer of the Tiger Shark, Lieutenant Brice (Bruce Greenwood), discovers that the wounded survivor is actually a German prisoner-of-war, Bernard Schillings (Jonathan Hartman). Brice confronts him because he thinks Schillings has been making noises to betray the Tiger Shark s position to the German warship.  Brice shoots Schillings dead when the German panics and grabs a scalpel to defend himself.

Brice reveals to Paige that the Tiger Shark had sunk a German submarine tender recently and that her previous commanding officer, Lieutenant Commander Winters (Nick Hobbs), had died after the Tiger Shark surfaced to confirm the sinking. According to Brice, Winters was using a boathook to try to obtain a souvenir from the flotsam left behind by the sunken German ship when the Tiger Shark struck a submerged object, causing Winters to hit his head while reaching for his souvenir, fall overboard, and drown before he could be rescued. Brice then assumed command of the Tiger Shark.

Soon after the death of the German prisoner, those aboard the Tiger Shark begin to hear disembodied voices and experience other eerie events. While working in one of the Tiger Shark s ballast tanks, two of the submarines officers, Lieutenant Steven Coors (Scott Foley) and Ensign Douglas Odell (Matthew Davis) have a conversation in which Odell questions the story about Winters hitting his head and falling overboard after the Tiger Shark struck a submerged object, saying that he had felt no such impact. Coors tells Odell that the real story of Winters death is darker. Winters, on deck with only Brice, Coors, and Lieutenant Paul Loomis (Holt McCallany), had ordered them to have a gunnery party come on deck to machine-gu] the survivors of the sunken German ship in the water. When Brice, Loomis, and Coors objected, a heated argument had broken out and escalated into a physical altercation during which Winters hit his head and fell overboard. In order to protect Winters reputation, Coors asks Odell not to tell anyone. Before they leave the ballast tank, Coors dies in a mysterious accident. Soon afterwards, Loomis sees Winters ghost aboard the Tiger Shark, escapes from the submarine via an escape trunk while she is underwater, and dies when he is impaled on an outside railing. 

A series of bizarre mechanical problems causes the crew of the Tiger Shark to lose control of the submarine, and she turns back towards the site of her sinking of the German ship, apparently of her own volition. Meanwhile, crewmen die in accidents at an alarming rate.  The eerie phenomena seem related to the death of Winters, and the crew begins to suspect a supernatural influence is behind all of the Tiger Shark s mishaps and to question Brices version of how and why Winters died.

Paige and Odell discover that the Tiger Shark mistook the Fort James for the German submarine tender and sank the Fort James instead; they also learn that Brice, Loomis, and Coors believed that they could not afford this drastic mistake to appear on their records and conspired to suppress the story, killing Winters on the deck of theTiger Shark as he tried to save the survivors of the Fort James. 

The Tiger Shark ultimately is crippled by a mounting number of accidents, and only five of those aboard remain alive: Brice, Odell, Paige, Stumbo (Jason Flemyng), and "Weird" Wally (Zach Galifianakis). Wally concludes that the Tiger Shark is haunted by a "malediction" that must be satisfied in order to escape its netherworld between heaven and hell. After the Tiger Shark arrives at the location of the sinking of the Fort James, she surfaces in a disabled condition and those aboard her detect a surface ship nearby. Brice prevents the surviving crew of the Tiger Shark from radioing the nearby ship, but Paige sneaks out on deck and tries to signal the ship with a flashlight. Brice confronts her and holds her at gunpoint. His remorse over the accident overcomes him; he admits the entire cover-up to Paige and then shoots himself in the head, falling dead into the ocean.

The ship Paige is signaling turns out to be British and picks up the four survivors of the Tiger Shark. As they look on, the Tiger Shark sinks, coming to rest on the ocean floor next to the wreck of the Fort James.

==Cast==
*Bruce Greenwood as Lieutenant Brice
*Matthew Davis as Ensign Douglas Odell
*Olivia Williams as Claire Paige
*Holt McCallany as Lieutenant Paul Loomis
*Scott Foley as Lieutenant, Junior Grade Steven Coors
*Zach Galifianakis as "Weird" Wally
*Jason Flemyng as Stumbo
*Dexter Fletcher as Kingsley
*Nick Chinlund as Chief
*Andrew Howard as Hoag
*Christopher Fairbank as Pappy
*Nick Hobbs as Lieutenant Commander Winters

==Production==
 
 
The producers of Below used the  , a retired World War II-era U.S. Navy   that is now a museum ship in Muskegon, Michigan|Muskegon, Michigan, for exteriors of the fictional USS Tiger Shark. The submarine was towed out into Lake Michigan for filming.

== Reception ==
 
The film gained an approval rating of 63% on Rotten Tomatoes with 43 out of 68 reviews calling it fresh.  Reviewers on Metacritic gave a mixed response with a 55% approval rating based on 20 reviews.  Entertainment Weekly gave the film a B+ rating, calling it a "handsome, haunting submarine thriller".  Edward Guthmann from the San Francisco Chronicle gave a mainly negative review stating that the dialogue was "heavy on sarcasm and puncturing insults, never captures the World War II period but sounds ridiculously anachronistic".  Variety Magazine gave the movie a mixed review stating that "the strenuous seriousness the film applies to an idea that is finally silly at its core steadily increases the impression of overwrought artificiality as matters progress". 

==References==
 

==External links==
* 
*  review at RogerEbert.com
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 