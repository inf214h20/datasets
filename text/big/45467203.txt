Kindermädchen für Papa gesucht
{{Infobox film
| name           = Kindermädchen für Papa gesucht
| image          = File:Kindermädchen für Papa gesucht poster.jpg
| alt            = Film poster
| caption        = Film poster
| film name      = 
| director       = Hans Quest
| producer       = Artur Brauner, Horst Wendlandt 
| writer         = Curth Flatow, Eckart Hachfeld, Jochen Kuhlmey 
| screenplay     = 
| story          = 
| based on       =  
| starring       =  Claus Biederstaedt, Susanne Cramer, and Gunther Philipp
| narrator       = 
| music          =Martin Böttcher, Erwin Halletz
| cinematography = Fritz Arno Wagner
| editing        = Kurt Zeunert 
| studio         = Central Cinema Company Film
| distributor    = 
| released       =26 July 1957 
| runtime        = 88 minutes
| country        = West Germany
| language       =German
| budget         = 
| gross          = 
}}

Kindermädchen für Papa gesucht is a 1957 West German comedy film directed by Hans Quest and written by Curth Flatow and Eckart Hachfeld. It stars Claus Biederstaedt, Susanne Cramer, and Gunther Philipp.   

==Plot==
Peter and Kurt Jäger are cousins, though extremely different from one another. While Peter has a reputation as a womanizer in the familys chocolate concern, Kurt is a teetotaler, a hypochondriac and a complete novice with women. He has never noticed that Inge, his secretary, has been in love with him for years.

One day, shortly after his birthday, Kurt confides to his cousin that he has fallen in love with Sabine, a customer. She has been working as a nanny but has just resigned from her job. When she is about to respond to a newspaper employment ad for a nanny, Peter secretly connects her call to Kurt who invites Sabine home for an interview. He is so uptight that he does not realize Sabine wants to work as a nanny until she enquires about his child. Kurt explains that there is in fact a child in the house but it belongs to his cousin Peter. The next day they "borrow" Heinerle, the son of one of Peters friends. He soon turns the house upside down, driving the housekeeper Frau Stadelmeier to despair.

Peter pretends he is Heinerles biological father, explaining that the boy has been brought up by his stepfather. So as to bring Sabine and Kurt together, Peter intends to go out with her and tell her what a wonderful cousin he has. Kurt should come and join the two of them in a restaurant. Peter should then leave, explaining he has a girlfriend he has recently hardly had time to see.

Kurts secretary Inge decides to take action. From a plain, unobtrusively clad secretary, she turns herself into a fashionably dressed woman with dazzling make-up. Kurt is so overcome that he can no longer dictate letters to her. As the two are still planning how Sabine can impress Kurt that evening, they come closer together, have a few cognacs and end up tipsy at Kurts. They go on drinking and Inge takes Kurt, now completely drunk, up to bed and the two become engaged. At the restaurant, Peter is in trouble. He had expected Kurt to turn up and does not have enough money to pay the bill. Fortunately, at that very moment there is an announcement about a dancing competition aimed at finding the "most sensitive couple". The prize is just enough to pay for the meal and so the two of them dance through to victory.

Next day: Peter has fallen in love with Sabine but she now suspects Heinerle is not Peters child. Heinerles real father appears and, mad with jealousy, turns on Kurt believing him to be his wifes secret lover. Sabine learns from Peter the truth about the "borrowed child" just as she is about to leave. She tells Heinerles father how it all happened, now looking quite contrite. Sabine plans to stall Peter and reveals she has fallen in love with him just as Peter had planned it all for their evening together. It leads to a row between Peter and Kurt and even Inge is unhappy until a staged fight between Peter and Kurt comes to an end. Finally Peter and Sabine as well as Kurt and Inge end up as happy couples.

==Cast==
*Claus Biederstaedt as Peter Jäger
*Susanne Cramer as Sabine
*Gunther Philipp as Kurt Jäger
*Carla Hagen as Inge Wernicke
*Erica Beer as Monika Baerwaldt
*Bum Krüger as Ernst Baerwaldt
*Margarete Haagen as Frau Stadelmeier Peter Fischer as Heinerle
*Ingrid Lutz as Evelyn
*Kurt Pratsch-Kaufmann as Ansager
*Dinah Hinz as Ulla
*Ruth Scheerbarth as Ella
*Eve Dietrich 	Eve Dietrich 		
*Ada Witzke as secretary

==Production==
Shooting took place from March to April 1957 in Berlin and in the CCC-Studios in Berlin-Spandau. The working title of the film was Mein Vater, der Schürzenjäger (My Father, the Womanizer).

The premiere was on 26 July 1957 in the Metro im Schwan at Frankfurt am Main. Along with Die Unschuld vom Lande and  Einmal eine große Dame sein,   Kindermädchen für Papa gesucht was one of CCC-Films three comedies for the 1957/58 season.   The films hit song "Andrea" was sung by the Montecarlos.

==Reception==
The journal film-dienst found that Hans Quest had "significantly cut back on refined humour ... in favour of blatant slapstick in almost every scene. This is a pity as the film offers material not just for a colourful farce but for a pleasant comedy. Yet hardly with the present cast (Biederstaedt-Philipp-Cramer)!"

The 1990 Lexikon des Internationalen Films published by film-dienst qualifies Kindermädchen für Papa gesucht as an "exaggerated, expressionless farce built on proven mishmash criteria." 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 