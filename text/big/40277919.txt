How to Lose Your Virginity
{{Infobox film
| name           = How to Lose Your Virginity
| image          = How to Lose Your Virginity, Official DOC NYC Poster, Nov 2013.jpg
| border         = yes
| alt            = 
| caption        = 
| film name      = 
| director       = Therese Shechter
| producer       = Therese Shechter and Lisa Esselstein
| writer         = Therese Shechter
| based on       =  
| narrator       = 
| starring       = 
| music          = Stephen Thomas Cavit
| cinematography = 
| editing        = Dina Guttman, Marin Sander-Holzman
| distributor    = Women Make Movies
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       =  English
}}

How to Lose Your Virginity is an American documentary film directed by Therese Shechter and distributed by Women Make Movies. The film examines how the concept of virginity shapes the sexual lives of young women and men through the intersecting forces of history, politics, religion and popular culture. It is set to premiere at DOC NYC, a New York City documentary festival, on November 17, 2013. {{cite web
| title =DOC NYC: How to Lose Your Virginity
| url =http://www.docnyc.net/film/how-to-lose-your-virginity/#.UmmV05Tk-Yr
| date = 
| accessdate = 24 October 2013}} 

== Synopsis ==
 Natalie Dylans Barely Legal and discusses the success of the "virginity porn" genre. How to Lose Your Virginity questions the effectiveness of the Abstinence-only sex education| Abstinence-only sex education movement and observes how sexuality continues to define a young woman’s morality and self-worth.  
The meaning and necessity of virginity as a  , "Scarleteen" {{cite web
| title =Scarleteen
| url =http://www.scarleteen.com/about_scarleteen
| date =  comprehensive sex education advocate Shelby Knox.

== Production ==

The film was directed by Therese Shechter, whose production company Trixie Films is based in Brooklyn. Working with Producer Lisa Esselstein, How to Lose Your Virginity was shot over several years in the U.S. and Canada. Other films produced by Trixie Films include the documentary feature I Was A Teenage Feminist and the documentary shorts "How I Learned to Speak Turkish" and "#slutwalknyc". {{cite web
| title = How to Lose Your Virginity: the filmmakers
| url =http://virginitymovie.com/crew
| date = 
| accessdate = 25 September 2013}}  {{cite web
| title =Trixie Films Official Site
| url =http://www.trixiefilms.com
| date = 
| accessdate = 27 September 2013}}  {{cite web
| title =Lisa Esselstein  
| url =http://www.imdb.com/name/nm2423448/
| date = 
| accessdate = 29 September 2013}}  {{cite web
| title = How to Lose Your Virginity: films & writing
| url =http://www.virginitymovie.com/other-films/
| date = 
| accessdate = 23 September 2013}} 

Shechter was inspired to make the film because of the growing abstinence until marriage movement and her own experiences as an older virgin. While making the film, Shechter became engaged and incorporated trying on white wedding dresses into the film as a way of looking at how the wedding industry sells virginity. {{cite web
| last = Hills
| first = Rachel
| title = How to lose your virginity
| url =http://www.dailylife.com.au/news-and-views/dl-opinion/how-to-lose-your-virginity-20120507-1y8ei.html
| date = 8 May 2012
| accessdate = 19 September 2013}}  {{cite web
| title = Interview: Therese Shechter, filmmaker
| url =http://her-film.com/2010/06/21/interview-therese-shechter-filmmaker
| date = 21 June 2010
| accessdate = 15 September 2013}}/ 

Over the course of the films production, its transmedia companion, The V-Card Diaries has crowd-sourced over 200 stories about what the site calls "sexual debuts and deferrals." It was exhibited at The Kinsey Institutes 8th Annual Juried Art Show, the exhibits first interactive piece. {{cite web
| last = Crane
| first = Hannah
| title =Kinsey Art Show features thought-provoking art
| publisher = Indiana Daily Student
| url =http://idsnews.com/news/story.aspx?id=92889
| date = 19 May 2013
| accessdate = 24 October 2013}}  {{cite web
| title =Juried Art Show 2013
| publisher = The Kinsey Institute
| url =http://www.kinseyinstitute.org/services/gallery/jeas/2013/index.php
| date = 
| accessdate = 24 October 2013}} 

== Critical reception ==
 
Soraya Chemaly wrote in the Huffington Post, "Virginity is a powerful and malleable concept, as evidenced by the teenagers in Therese Shechters smart, funny and provoking documentary." {{cite web
| last = Chemaly
| first = Soraya
| title =Virgins, Bondage, and a Shameful Media Fail
| publisher = The Huffington Post
| url =http://www.huffingtonpost.com/soraya-chemaly/50-shades-of-grey_b_1441008.html?ref=tw
| date = 20 April 2012
| accessdate = 30 September 2013}}  Leigh Kolb of Bitch Flicks said that "Theres no anger, theres no judgment…Shechter’s ability to teach, dismantle, expose and explore is remarkable. The audience is left with newfound knowledge with which they can criticize myths of virginity in our culture. However, the audience is also left with respect for everyone’s stories. When a documentary can do that, it succeeds in a big way." {{cite web
| last = Kolb
| first = Leigh
| title =How to Lose Your Virginty or: How We Need to Rethink Sex
| publisher = Bitch Flicks
| url =http://www.btchflcks.com/2013/08/how-to-lose-your-virginity-or-how-we-need-to-rethink-sex.html#.Ukow7Ba0wts
| date = 9 August 2013
| accessdate = 30 September 2013}} 

In the Jakarta Globe, Paul Freelend wrote that "her work to highlight what she calls the virginity culture and the misconceptions surrounding it may resonate as loudly in Indonesia and other developing countries as in the United States." {{cite web
| last = Freeland
| first = Paul
| title =Wherever Youre From, A Call to Talk About Sex
| publisher = Jakarta Globe
| url =http://www.thejakartaglobe.com/archive/wherever-youre-from-a-call-to-talk-about-sex/515933/
| date = 5 May 2012
| accessdate = 30 September 2013}}  Basil Tsoikos, programmer for the Sundance Film Festival and DOC NYC, in What (not) to doc remarked that "Shechter seems like the perfect filmmaker to tackle the complexities around virginity. It’s a topic that far too many people are obsessed about – probably for all the wrong reasons – so the film is sure to stimulate interest and provoke heated debate." {{cite web
| last = Tsoikos
| first =Basil
| title = In the Works: HOW TO LOSE YOUR VIRGINITY
| publisher = What (not) to doc
| url =http://whatnottodoc.com/2012/04/20/in-the-works-how-to-lose-your-virginity/
| date = 20 April 2012
| accessdate = 30 September 2013}} 

J. Maureen Henderson of Forbes.com said that Shechters work "tackles one of the last taboos in our culture’s discussion of sex – the deliberate decision not to participate in it.” {{cite web
| last = Henderson
| first =J. Maureen
| title = Lets (Not) Talk About Sex: Meet The Filmmaker Whos Exploring Modern Virginity
| publisher = Forbes.com
| url =http://www.forbes.com/sites/jmaureenhenderson/2012/05/01/lets-not-talk-about-sex-meet-the-filmmaker-whos-exploring-modern-virginity/
| date = 1 May 2012
| accessdate = 24 October 2013}}  Lena Corner of The Guardian wrote that "It’s refreshing to hear such forthright voices in a world where any debate about virginity is often so conflicting or one-sided." {{cite web
| last = Corner
| first =Lena
| title = Virginity: how was it for you?
| publisher = The Guardian
| url =http://www.theguardian.com/lifeandstyle/2010/jul/18/virginity-project-blogging-sex
| date = 17 July 2010
| accessdate = 24 October 2013}}  Jennifer Wadsworth of SFGate.com remarked that the project is "More than just a narrative about virginity. It’s about the connection of storytelling and how hearing about other people’s experience can make anyone else feel less alone in theirs." {{cite web
| last = Wadsworth
| first =Jennifer
| title = Virginity obsession is focus of new documentary
| publisher = SFGate.com
| url =http://blog.sfgate.com/hottopics/2013/10/08/virginity-obsession-is-focus-of-new-documentary/
| date = 8 October 2013
| accessdate = 24 October 2013}} 

== References ==

 

== External links ==

*  
*  
*  

 
 
 