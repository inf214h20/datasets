Tous les Matins du Monde
{{Infobox film
| name           = Tous les matins du monde
| image          = Tous les matins du monde-film.jpg 
| caption        = Film poster
| director       = Alain Corneau
| producer       = Jean-Louis Livi
| writer         = Pascal Quignard Alain Corneau
| starring       = Jean-Pierre Marielle Gérard Depardieu   Anne Brochet   Guillaume Depardieu
| music          = Jordi Savall Sainte-Colombe Marin Marais
| cinematography = Yves Angelo
| editing        = Marie-Josephe Yoyotte
| distributor    = Koch-Lorber Films (1992)
| released       =  
| runtime        = 115 minutes
| country        = France
| language       = French
| gross          = $3,089,497
}}
 

Tous les matins du monde (English translation: All the Mornings of the World) is a 1991  . Viol Bodies. Film of the week: Tous les Matins de Monde. The Guardian, 30 December 1992.  The title of the film is explained towards the end of the film; « Tous les matins du monde sont sans retour » ("all the mornings of the world never return") spoken by Marais in chapter XXVI of Quignards novel when he learns of the death of Madeleine.

==Background==

In the same year as the books release, Pascal Quignard, together with director Alain Corneau, adapted the novel to the film that starred Jean-Pierre Marielle, Gérard Depardieu, Anne Brochet, and Guillaume Depardieu. The film is currently distributed by Koch-Lorber Films.

The film revolves around the late-17th / early-18th-century composer Marin Marais life as a musician, his mentor Monsieur de Sainte-Colombe, and Sainte-Colombes daughters. The aging Marais, played by Gérard Depardieu, narrates the story, while Depardieus son Guillaume Depardieu plays the young Marais. The haunting sound of his instrument, the viol (viola da gamba), here played by Jordi Savall, is heard throughout the movie and plays a major role in setting the mood. Though fictional, the story is based on historical characters, and what little is known about their lives is generally accurately portrayed. 

The film credits the scenes set in the salon of Louis XV as filmed in the golden gallery of the Banque de France.

Described as a "crossover movie" with the music integrated into the story-line, Derek Malcolm saw Marielles performance as "matching the music note for note". 

==Synopsis== jansenist Monsieur de Sainte Colombe. Sainte Colombe buried himself in his music after the death of his wife bringing up his two daughters on his own, and teaching them to be musicians, and playing in a consort with them for local noble audiences. His reputation reaching the court of Louis XIV, the king sent an envoy, Caignet, to request him to play at court. But Sainte Colombe sent the envoy away as well as the abbé Mathieu, and shut himself away in a cabin in his garden in order to perfect the art of viol playing.

After some years, a 17-year-old man, Marin Marais, visits him to request that he be taught by the older master, who however sees no merit in the playing of the young man and sends him packing. The elder daughter, Madeleine is saddened as she has fallen in love with Marais, and helps him to secretly listen to her father playing.

She becomes pregnant, but the child is still-born, and she falls gravely ill. She is abandoned by Marais, who on the death of Caignet has assumed a position as a court musician. Sainte Colombe calls him to his house, as the ailing Madeleine wants to hear her lover play a piece he wrote for her "La rêveuse". When Marais leaves hurriedly Madeleine hangs herself. The old Marais realises his faults and vanity, while Sainte Colombe recognises finally his musicianship.

==Cast==
* Jean-Pierre Marielle as Monsieur de Sainte Colombe
* Gérard Depardieu as Marin Marais
* Anne Brochet as Madeleine
* Guillaume Depardieu as Young Marin Marais
* Carole Richert as Toinette
* Michel Bouquet as Baugin
* Jean-Claude Dreyfus as Abbe Mathieu
* Yves Gasc as Caignet
* Yves Lambrecht as Charbonnières
* Jean-Marie Poirier as Monsieur de Bures
* Myriam Boyer as Guignotte

==Music==
As listed in the films credits, the music heard includes the following:
*Sainte Colombe: Les pleurs; Gavotte du tendre; "Le retour"
*Marin Marais: Improvisation sur les Folies dEspagne; Larabesque; Le Badinage; Le rêveuse
*Lully: Marche pour le cérémonie des Turcs
*François Couperin: Troisième leçon de Ténèbres
*Savall: Prélude pour Monsieur Vauquelin; Une jeune fillette, d’après une mélodie populaire; Fantaisie en mi mineur, d’après un anonyme du XVIIème

Apart from Savall, the musicians are Monserrat Figueras and Mari-Cristina Kiehr (sopranos), Christophe Coin and Jérôme Hantaï (viola da gamba), Rolf Lislevand (theorbo) and Pierre Hantaï (harpsichord and organ).

==Awards and nominations==
*César Awards (France) Best Actress &ndash; Supporting Role (Anne Brochet)  Best Cinematography (Yves Angelo) Best Costume Design (Corinne Jorry) Best Director (Alain Corneau)  Best Film Best Music (Jordi Savall) Best Sound (Anne Le Campion, Pierre Gamet, Gérard Lamps and Pierre Verany) Best Actor &ndash; Leading Role (Jean-Pierre Marielle) Best Editing (Marie-Josèphe Yoyotte)  Best Writing (Alain Corneau and Pascal Quignard) Most Promising Actor (Guillaume Depardieu)

*42nd Berlin International Film Festival (Germany)
**Official selection: Golden Bear (Alain Corneau)   

*Golden Globe Awards (USA) Best Foreign Language Film

*Louis Delluc Prize (France)
**Won: Best Film 

==References==
 

== External links ==
* 
* http://www.medieval.org/emfaq/misc/tlmdm.htm For more details of the films background

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 