Amira & Sam
Amira & Sam is a 2014 American film written and directed by  . Penske Business Media, LLC. August 28, 2014. Retrieved on January 28, 2015. 

==Plot==
Sam, a soldier who had served in Afghanistan and Iraq, meets Amira when he visits her uncle, Bassam, who had served as Sams Iraqi translator. Initially Amira does not trust him because he was an American soldier. Amira, a vendor of pirated films, is forced to stay with Sam after immigration officials go in pursuit of her. As the film progresses, Sam and Amira fall in love.

==Cast==
* Amira Jafari (Dina Shihabi)
** Andrew OHehir of  . January 28, 2015. Retrieved on January 29, 2015.  Jeannette Catsoulis of  . January 29, 2015. Print: January 30, 2015, p. C9, New York Edition. Retrieved on February 11, 2015. 
* Sam Seneca (Martin Starr) American Sniper.  OHehir also described Sam as "somewhat the black sheep" in his family. 
* Bassam Jafari (Laith Nakli)
* Charlie (Paul Wesley)
** Charlie, Sams cousin and a hedge fund manager, asks Sam to encourage veterans to invest in his financial schemes. Tom Keogh of the Seattle Times describes him as "manipulative."  As time passes Sam learns the true nature of Charlies operations. 
* Jack (David Rasche)
* Greg (Ross Marquand)
* Claire (Taylor Wilcox)

==Production==
Much of the film was shot on Staten Island. 

==Release==
The release was scheduled for early 2015. A limited theatrical release was planned,  scheduled for January 30, 2015. A video on demand release was scheduled for the same day.  In addition the film was to be available on home video and digital formats. 

==Reception==
 
Catsoulis wrote that the film is "is more successful as a portrait of veteran alienation than as a romance." Catsoulis, Jeanette. " ." The New York Times. January 29, 2015. Print: January 30, 2015, p. C9, New York Edition. Retrieved on February 11, 2015. 

OHehir stated that despite implausibilities in the film, its romance "worked on me, or at least it made me wish that the world of Sam and Amira’s wonderful and unlikely love affair really existed. It’s a better world than ours."  OHehir added that even though the film had good intentions and any cultural mistakes "are exceptionally mild", "If enough people see this movie, it’s possible that Mullin will come under attack for eroticizing or exoticizing an Arab woman, and/or disrespecting Islam." 

John DeFore of the Hollywood Reporter wrote that the romantic chemistry between Amira and Sam was "just right". 

Alan Scherstuhl of the Village Voice wrote that the film had "implausibilities" but that the romance was overall "stellar".
 

==References==
 

==Further reading==
* Beggs, Scott. " " ( ). Film School Rejects. January 28, 2015.
* Oakley, Josh. " " ( ). Cut Print Film. January 27, 2015.
* Oakley, Josh. " " ( ). Cut Print Film. January 30, 2015.
* Wardrope, Todd. " " ( ). Twin Cities Daily Planet. January 26, 2015.
* Leuck, Tristan. " " ( ). District, Student paper of the Savannah College of Art and Design. October 27, 2014.

==External links==
*   - Official website
*  
*   at Rotten Tomatoes
* " ." Alamo Drafthouse Cinema.

 
 
 
 
 