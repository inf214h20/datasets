Spontaneous Combustion (film)
{{Infobox film
| name           = Spontaneous Combustion
| image          = Poster of the movie Spontaneous Combustion.jpg
| caption        = Theatrical release poster
| director       = Tobe Hooper
| producer       = Henry Bushkin Sanford Hampton Jerrold W. Lambert Jim Rogers Arthur M. Sarkissian
| screenplay     = Tobe Hooper Howard Goldberg
| story          = Tobe Hooper
| starring       = Brad Dourif
| music          = Graeme Revell 
| cinematography = Levie Isaacks
| editing        = David Kern
| studio         = Black Owl Productions Project Samson VOSC
| distributor    = Taurus Entertainment
| released       =  
| runtime        = 97 min.
| country        = United States
| language       = English
}}
 science fiction horror film, directed by Tobe Hooper. It was written by Tobe Hooper and Howard Goldberg, based on a story by Hooper, and is a co-production between Henry Bushkin, Sanford Hampton, Jerrold W. Lambert, Jim Rogers and Arthur M. Sarkissian.

It was nominated for best film in the 1991 Fantasporto International Fantasy Film Awards. 

== Plot ==
 
Brad Dourif plays the role of Sam, who learns that his parents were part of an atomic bomb experiment. As an adult, Sam discovers he has the power of pyrokinesis. He is able to control fire and electricity but with terrible consequences to his body afterwards. The cast also includes actors Melinda Dillon and Cynthia Bain.
==Cast==
* Brad Dourif as Sam

* Cynthia Bain as Cynthia Bain

* Jon Cypher as Dr. Marsh
 William Prince as Lew Orlander 

* Melinda Dillon as Nina

* Dey Young as Rachel

== Critical reception ==
 
Spin (magazine)|Spin magazine, while writing, "no one makes bad movies as deliriously entertaining as Tobe Hooper, whose career continues its spectacular downward slide with Spontaneous Combustion", gave the film an overall favorable review.  John Kenneth Muir, in his book Horror Films of the 1980s, wrote, "Spontaneous Combustion commences on a high note of creativity and wit, but then promptly goes down in flames." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 