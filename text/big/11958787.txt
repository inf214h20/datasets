Me, Natalie
{{Infobox film
| name           = Me, Natalie
| image          = Me Natalie (1967).jpg
| caption        = Original poster
| director       = Fred Coe
| producer       = Stanley Shapiro
| writer         = A. Martin Zweiback
| narrator       = 
| starring       = Patty Duke James Farentino Deborah Winters
| music          = Henry Mancini Rod McKuen
| cinematography = Arthur J. Ornitz
| editing        = Sheila Bakerman John McSweeney Jr.
| studio         = Cinema Center Films
| distributor    = National General Pictures
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.9 million  (US/ Canada rentals) 
| preceded_by    = 
| followed_by    = 
}}
 Pillow Talk, Lover Come Back, and That Touch of Mink for Doris Day.  

Al Pacino began his film career with this movie.

==Synopsis== optimistic outlook, myopic optometry|optometrist Morris to marry her, hoping his nearsightedness will prevent him from seeing shes no beauty.
 Bohemian lifestyle. She finds employment as a cocktail waitress at the Topless Bottom Club and befriends Drug addiction|drug-addicted Go-Go dancing|go-go dancer Shirley Norton. Although she contemplates suicide after discovering her aspiring artist lover David Harris is married, he finally convinces her shes a worthwhile human being and not the ugly duckling she imagines herself to be.

==Principal cast==
*Patty Duke as Natalie Miller
*James Farentino as David Harris
*Elsa Lanchester as Miss Dennison
*Deborah Winters as Betty
*Salome Jens as Shirley Norton
*Nancy Marchand as Mrs. Miller
*Philip Sterling as Mr. Miller
*Bob Balaban as Morris
*Martin Balsam as Uncle Harold
*Al Pacino (Film debut) as Tony
*Catherine Burns as Hester Robyn Morgan as Natalie, Age 7

==Critical reception==
In his review in the New York Times, Vincent Canby called the film "an artificial mess of wisecracks and sentimentality" and added, "Locales and a gummy musical score by Henry Mancini and Rod McKuen are among the things constantly impinging on Me, Natalie. Another is Coes apparent indecision as to whether the movie is a character study or a gag comedy. Mostly its just gags, delivered abrasively by Miss Duke, who is even less effective when registering pathos."  

Roger Ebert of the Chicago Sun-Times found it to be "as conventional and corny as warmed-over Young at Heart (1954 film)|"Young at Heart" . . . a pleasant film, very funny at times . . . Patty Duke, as Natalie, supplies a wonderful performance."  

TV Guide considers the film "somewhat bland" but calls Duke "a wonder" and adds, "Handled by a lesser actress, the results might have seemed more stereotypical, but Duke is convincing."  

==Awards and nominations==
*Golden Globe Award for Best Actress - Motion Picture Musical or Comedy (Patty Duke, winner) Grammy Award for Best Original Score Written for a Motion Picture or Television Show (nominee)
*Writers Guild of America Award for Best Drama Written Directly for the Screen (nominee)

==See also==
* List of American films of 1969

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 