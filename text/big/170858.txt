The Hudsucker Proxy
{{Infobox film
| name           = The Hudsucker Proxy
| image          = The Hudsucker Proxy Movie.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster Joel Coen  Ethan Coen 
| writer         = Joel Coen Ethan Coen Sam Raimi
| starring       = Tim Robbins Jennifer Jason Leigh Paul Newman
| music          = Carter Burwell
| cinematography = Roger Deakins
| editing        = Thom Noble
| studio         = Silver Pictures Working Title Films PolyGram Filmed Entertainment
| distributor    = Warner Bros. Pictures   Universal Pictures  
| released       =   
| runtime        = 111 minutes
| country        = United Kingdom United States  
| language       = English
| budget         = $25 million Bergan, pp. 24, 36 
| gross          = $2.8 million
}} Joel and Ethan Coen. Sam Raimi co-wrote the script and served as second unit director. The film stars Tim Robbins as a naïve business-school graduate who is installed as president of a manufacturing company, Jennifer Jason Leigh as a newspaper reporter, and Paul Newman as a company director who hires the young man as part of a stock scam.
 The Computer box office flop.

==Plot== jumping out of a top-floor window.  Afterwards, Sidney J. Mussburger (Paul Newman), a ruthless member of the board of directors, learns Hudsuckers stock shares will be soon sold to the public, he mounts a scheme to buy the controlling interest in the company by temporarily depressing the stock price from hiring an incompetent president to replace Hudsucker.
 pitch an invention hes been working on which turns out to be a simple drawing of a circle. Believing Norville to be an idiot, Mussburger selects him as a proxy for Hudsucker. Across town, Amy Archer (Jennifer Jason Leigh), a brassy Pulitzer Prize-winning reporter for the Manhattan Argus, is assigned to write a story about Norville and find out what kind of man he really is. She gets a job at Hudsucker Industries as his personal secretary, pretending to be yet another desperate graduate from Muncie. One night, Amy searches the building to find clues and meets Moses, a man who operates the towers giant clock and knows "just about anything if it concerns Hudsucker". He tells her Mussburgers plot, and she takes the story back to her Chief (John Mahoney), but he does not believe a word of it.

The other executives decide to accept Norvilles invention of the  . Norville dismisses it and fires Buzz. Meanwhile Aloysius (Harry Bugin), a Hudsucker janitor, discovers Amys true identity and informs Mussburger. Mussburger reveals Amys secret identity to Norville and tells him he will be dismissed as president after the new year. Mussburger also convinces the board that Norville is insane and must be sent to the local psychiatric hospital.

On New Years Eve, Amy finds Norville drunk at a beatnik bar. She apologizes, but he storms out and is chased by an angry mob led by Buzz, whom Mussburger had convinced that Norville had stolen the hula hoop idea. Norville escapes to the top floor of the Hudsucker skyscraper and changes back into his mailroom uniform. He climbs out on the ledge, where Aloysius locks him out and watches as he slips and falls off the building at the stroke of midnight. All of a sudden, Moses stops the clock and time freezes. Waring Hudsucker appears to Norville as an angel and tells him the Blue Letter that was supposed to be delivered to Mussburger contains a legal document indicating that Hudsuckers shares would go to his immediate successor, who is now Norville. Moses fights and defeats Aloysius inside the tower, allowing Norville to fall safely to the ground. Norville and Amy reconcile. As 1959 progresses, it is Mussburger who is sent to the asylum while Norville develops a new invention "for kids", an enigmatic circle on a folded sheet of paper that will ultimately turn out to be a frisbee.

==Cast==
 
 
* Tim Robbins as Norville Barnes
* Jennifer Jason Leigh as Amy Archer
* Paul Newman as Sidney J. Mussburger Jim True as Buzz the elevator operator
* Bill Cobbs as Moses the clock man
* Harry Bugin, as Aloysius the janitor
* Bruce Campbell as Smitty, Argus reporter
* John Mahoney as Argus chief editor
 
* Charles Durning as Waring Hudsucker
* Patrick Cranshaw as Ancient sorter
* Anna Nicole Smith as Za-Za
* Steve Buscemi as Beatnik bartender
* Sam Raimi as Hudsucker brainstormer
* Jon Polito as Mr. Bumstead
* John Goodman as Rockwell newsreel announcer 
* Ian McKellen as Mob crowd member (uncredited)
 

==Development==

===Writing=== Christmas in homage to Howard Hawks His Girl Friday (1940), while Jennifer Jason Leighs performance as fast-talking reporter Amy Archer is reminiscent of Rosalind Russell and Katharine Hepburn, in both the physical and vocal mannerisms. Levin, pp. 103–118  Other movies that observers found references to include Executive Suite (1954) and Sweet Smell of Success (1957). 
 visual motifs concerning the shape of circles. This includes Moses monologue at the beginning, the Hudsucker Clock, Mussburgers wristwatch, the inventions of both the hula hoop and frisbee, as well as Norville and Amys conversation about Karma. 

The first image the Coens and Raimi conceived was of Norville Barnes about to jump from the window of a skyscraper and then they had to figure out how he got there and how to save him. Bergan, pp. 148–162  The inclusion of the  . The characters talk fast and wear sharp clothes." Woods, pp. 125–135 
 pitched the project at Warner Bros. Pictures. Silver also allowed the Coens complete artistic control. 

===Production===
Joel Silvers first choice for Norville Barnes was Tom Cruise, but the Coens persisted in a desire to cast Tim Robbins.  Winona Ryder and Bridget Fonda were in competition for the role of Amy Archer before Jennifer Jason Leigh was cast. Mottram, pp. 93–113  Leigh had previously auditioned for a role in the Coenss Millers Crossing and Barton Fink. Her failed auditions prompted the Coens to cast her in The Hudsucker Proxy.  When casting the role of Sidney Mussburger, "Warner Bros. suggested all sorts of names," remembered Joel. "A lot of them were comedians who were clearly wrong. Mussburger is the bad guy and Paul Newman brought that character to life."  However, the Coens first offered the role to Clint Eastwood, but he was forced to turn it down due to scheduling conflicts. 

Once Newman and Robbins signed on, PolyGram Filmed Entertainment and Working Title Films agreed to co-finance the film with Warner Bros. and Silver Pictures.  The film was shot on sound stages at Carolco Studios in Wilmington, North Carolina beginning on November 30, 1992. Raimi served as second unit director, shooting the hula hoop sequence and Waring Hudsuckers suicide.  Production designer Dennis Gassner was influenced by fascist architecture, particularly the work of Albert Speer,  as well as Terry Gilliams Brazil (1985 film)|Brazil (1985),  Frank Lloyd Wright and the Art Deco movement. Robson, pp. 139–142  Principal photography ended on March 18, 1993. 

In addition, numerous sequences were filmed in downtown Chicago, particularly in the Merchandise Mart building for the entrance and lobby to Hudsucker Industries and the Hilton Chicago Christmas ballroom. 
 presses in the News & Observer building in downtown Raleigh, North Carolina appeared in the movie. 

===Visual effects===
The  s, shot separately and merged in post-production. To lengthen the sequence, the model of the Hudsucker building was the equivalent of 90 stories, not 45. 
 Aon Center. The Computer CGI snow and composites of the falling sequences. 

To create the two suicide falls, the miniature New York set was hung sideways to allow full movement along the heights of the buildings. McAlister calculated that such a drop would take seven seconds, but for dramatic purposes it was extended to around thirty. Problems occurred when the Coens and cinematographer Roger Deakins decided that these shots would be more effective with a wide-angle lens. "The buildings had been designed for an 18 mm lens, but as we tried a 14 mm lens, and then a 10 mm, we liked the shots more and more."  However, the wider amount of vision meant that the edges of the frame went beyond the fringes of the model city, leaving empty spaces with no buildings. In the end, extra buildings were created from putting the one-sided buildings together and placing them at the edges. Charles Durnings fall was shot conventionally, but because Tim Robbins had to stop abruptly at the camera, his was shot in reverse as he was pulled away from the camera. 

According to Cinefex Magazine # 74 (July 1998), the skyscraper models created for The Hudsucker Proxy were re-used for the 1998 Godzilla movie.

==Soundtrack==
{{Infobox album  
| Name        = Original Motion Picture Soundtrack: The Hudsucker Proxy
| Type        = Soundtrack
| Artist      = Carter Burwell
| Cover       =
| Cover size  = 0
| Released    = March 15, 1994
| Recorded    =
| Genre       = Film score
| Length      = 29:28
| Label       = Varèse Sarabande
| Producer    = 
| Chronology  = Coen Brothers film soundtracks Barton Fink (1991)
| This album  = The Hudsucker Proxy (1994)
| Next album  = Fargo (film)#Soundtrack|Fargo (1996)
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =    
| noprose = yes
}} score to The Hudsucker Proxy was written by Carter Burwell, the fifth of his collaborations with the Coen Brothers. "Adagio of Spartacus and Phrygia" from the ballet Spartacus (ballet)|Spartacus by Khachaturian is the basis of the main theme and additional music from the ballet runs under the Hula-Hoop sequence. The popular music of the time is also reflected in the character of Vic Tenetta, played by Peter Gallagher and modeled after Dean Martin, who sings "Memories Are Made of This." Additional inspiration comes from Aram Khachaturians Gayane (ballet)|Gayane suite. A section from the ballet is used by Burwell for the scene in which Norville and Amy meet for the first time.  The composers "Sabre Dance" (from Gayane) is also used when the boy is the first to try the hula hoop. 

# "Prologue" (Khachaturian) – 3:20
# "Norville Suite" – 3:53
# "Warings Descent" – 0:27
# "The Hud Sleeps" – 2:13
# "Light Lunch" (Khachaturian) – 1:38
# "The Wheel Turns" – 0:52
# "The Hula Hoop" (Khachaturian) – 4:10
# "Useful" – 0:40
# "Walk Of Shame" – 1:22
# "Blue Letter" – 0:43
# "A Long Way Down" – 1:46
# "The Chase" – 1:02
# "Norvilles End" – 3:52
# "Epilogue" (Khachaturian) – 2:08
# "Norvilles Reprise" – 1:22

Other songs used in the film but not on the soundtrack album include:
* "Memories Are Made of This", performed by Peter Gallagher as Vic Tenetta, the party singer
* "In a Sentimental Mood", performed by Duke Ellington
* "Flying Home", performed by Duke Ellington
* "Carmen", performed by Grace Bumbry; used in dream dance sequence.

The classical music used was:
# Georges Bizet, Habanera from Carmen
# Luigi Boccherini, Minuet (3rd movt) from String Quintet in E, Op.11 No.5
# Frédéric Chopin Chopin Waltz (Waltz No.1 in E-flat "Grande valse brillante", Op.18 B62) from Les Sylphides Spartacus and Phrygia from Spartacus Suite No.2 Gayane Suite No.3
# Peter Tchaikovsky, Waltz from Swan Lake

==Release==
Warner Bros. held test screenings and audience comments were largely mixed. The studio suggested re-shoots, but the Coens, who held final cut privilege, refused because they were very nervous working with their biggest budget to date and were eager for mainstream success. The producers eventually added footage that had been cut and also shot minor Pick-up (filmmaking)|pick-ups for the ending.  Variety (magazine)|Variety magazine claimed that the pick-ups were done to try to save the film because Warner feared it was going to be a box office bomb. Joel Coen addressed the issue in an interview: "First of all, they werent reshoots. They were a little bit of additional footage. We wanted to shoot a fight scene at the end of the movie. It was the product of something we discovered editing the movie, not previewing it. Weve done additional shooting on every movie, so its normal." 
 Pulp Fiction.  The Hudsucker Proxy was released on March 11, 1994, and only grossed $2,816,518 in the United States.  The production budget was officially set at United States dollar|$25 million,  although, it was reported to have increased to $40 million for marketing and promotion purposes. Nonetheless, the film was a box office bomb.  Response from critics were also mixed. Based on 40 reviews collected by Rotten Tomatoes, 58% of the reviewers enjoyed the film. 

Roger Ebert praised the production design, scale model work, matte paintings, cinematography, and characters. "But the problem with the movie is that its all surface and no substance," Ebert wrote. "Not even the slightest attempt is made to suggest that the film takes its own story seriously. Everything is style. The performances seem deliberately angled as satire." {{cite news | author = Roger Ebert |authorlink=Roger Ebert | title = The Hudsucker Proxy | url =
http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19940325/REVIEWS/403250301/1023 | work = Chicago Sun-Times | date = 1994-05-25 | accessdate = 2008-11-20}}  Desson Thomson of The Washington Post described The Hudsucker Proxy as being "pointlessly flashy and compulsively overloaded with references to films of the 1930s. Missing in this films performances is a sense of humanity, the crucial ingredient in the movies Hudsucker is clearly trying to evoke. Hudsucker isnt the real thing at all. Its just a proxy." 
 old Hollywood The Player. From the Brazil (1985 film)|Brazil-like scenes in the cavernous mail room to the convoluted machinations in the board room, this film is pure satire of the nastiest and most enjoyable sort. In this surreal world of 1958 can be found many of the issues confronting large corporations in the 1990s, all twisted to match the filmmakers vision." 

Warner Home Video released The Hudsucker Proxy on DVD in May 1999. No featurettes were included.  It was announced in November 2012 that it will be one of the first Blu-Ray Disc titles released through the Warner Archive Collection. 

==References==

===Notes===
 

===Bibliography===
*  
*  
*  
*  
*  
*  
*  

==Further reading==
*  

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 