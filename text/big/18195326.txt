Life Is Cool (film)
{{Infobox film
| name           = Life Is Cool
| image          = Life is Cool film poster.jpg
| caption        = Theatrical poster
| director       = Choi Equan Choe Seung-won
| producer       = Lee Tae-hun
| writer         = Choi Equan
| starring       = Kim Su-ro Kang Seong-jin Kim Jin-soo Park Ye-jin
| music          = Jang Min-seung Jeong Jae-il
| cinematography = Sin Gyeong-won Jeong Yeong-sam Choe Byeong-hun
| editing        = Wang Sang-ik
| distributor    = CJ Entertainment
| released       =  
| runtime        = 98 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = United States dollar|US$28,100 
}} A Scanner Darkly (2006). 

== Plot == thirtysomething best friends—a heartbroken Romeo, a hopeless romantic, and a goofy playboy—meet up for the first time in ten years. However, things get complicated when they all fall for the same woman.

== Cast ==
* Kim Su-ro ... Baek Il-kwon
* Kang Seong-jin ... Kim Tae-yeong
* Kim Jin-soo ... Seong-hoon
* Park Ye-jin ... Kang Yeon-woo
* Kim Roi-ha ... Basketball manager
* Lee Won ... Samsung scouter
* David Joseph Anselmo ... Foreign professor
* Goo Bon-im ... Barbershop woman
* Kim Choon-gi ... University adulterer
* Yeo Ji ... High school girl
* Yoon Joo-hee ... Joo-hee
* Jo Young-gyu ... Policeman
* Park Jin-taek ... Naked man
* Park Yoo-mil ... Pregnant woman
* Lee Sang-hong ... Tango gangster

== Production ==
Life Is Cool was one of four films produced by CJ Entertainment to receive investment from Keyeast, a media contents company co-established by actor Bae Yong-joon.  Although actual shooting only lasted for one month, it then took almost two years and 140 artists to complete the rotoscoping, a technique in which animators traced over live action footage frame by frame. Visual effects were created by local production company DNA, who had previously worked on The Animatrix. 

Director Choi Equan has said that he was inspired by the Richard Linklaters film Waking Life, and that the films plot was based on a real life story of one of his friends. 

== Release ==
Life Is Cool was released in South Korea on 12 June 2008. The film accumulated a total of 3,951 admissions at the domestic box office, and grossed  . Retrieved 2009-05-28. 

== References ==
 

== External links ==
*   at Naver
*  
*  
*   at AnimationInsider.net
*  

 
 
 
 
 
 
 
 
 
 