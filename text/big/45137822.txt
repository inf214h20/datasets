Batman vs. Robin
{{Multiple issues|
 
}}
 
{{Infobox film
| name        = Batman vs. Robin
| image       = Batman vs Robin 3D box art.jpg
| caption     =
| director = Jay Oliva
| producer = James Tucker
| screenplay = J. M. DeMatteis
| based on =  
| starring ={{Plainlist|
* Jason OMara
* Stuart Allan
* Sean Maher
* Jeremy Sisto
* "Weird Al" Yankovic
* David McCallum
* Grey DeLisle
* Sean Maher
* Kevin Conroy
}}
| music = Frederik Wiedmann
| cinematography =
| editing = Al Bretienbach
| studio = Warner Bros. Animation DC Entertainment
| distributor = Warner Home Video
| released = April 3, 2015  (WonderCon)   April 14, 2015  (United States)  
| runtime = 76 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Batman vs. Robin is a   story arc written by Scott Snyder and illustrated by Greg Capullo and Jonathan Glapion, and serves as a sequel to 2014s Son of Batman.

Jason OMara, Stuart Allan, David McCallum and Sean Maher reprise their respective roles from Son of Batman.

==Plot== Anton Schott, Bruce Wayne, a.k.a. Batman, arrives to aid Damian in the resulting battle, with Damian chasing after the Dollmaker while Batman knocks out the Dolls with gas and frees the remaining children. Tempted to kill him, Damian chooses to spare the Dollmaker, only for an unknown owl-masked assassin to rip his heart out, telling Damian to never doubt his instincts. The assassin leaves Damian and the body of the Dollmaker behind, leading Batman to accuse Damian of murdering him when he arrives. Damian insists he spared the Dollmaker out of respect for Batman, and after inspecting the body further, Batman discovers an owl feather.
 Dick Grayson, a.k.a. Nightwing to look after him, as he investigates the owl feather he found, leading him to the Gotham Museum of Natural Historys Hall of Owls. In a flashback, Batman is reminded of a story his father, Thomas Wayne, told him when he was a child; a story about the Court of Owls, a secret society of rich men who ruled Gotham from the shadows and killed any who opposed them by sending agents called "talons". Although Thomas insisted it was just a story, the night Thomas and Bruces mother Martha were murdered, Bruce saw an owl clutching a bat flying away from the scene of his parents murder, and became convinced the story was true and that the Court of Owls was to blame. Bruce swears vengeance, and takes his frustrations out by killing an owl nesting in the attic of Wayne Manor, but after finding no evidence of the Court of Owls from his search of a high society club, Bruce conceded and dismissed the legend.

In the present, Batman is attacked by three owl-masked assailants, all seemingly inhuman and impervious to harm. Although he manages to destroy one with explosives, the other two nearly kill Batman before they suddenly liquefy into black ooze, and Batman sends out a distress beacon to the Batcave before losing consciousness. Damian, having escaped Nightwing and the mansion, stops a mugging and is again approached by the assassin he met, who introduces himself as "Court of Owls#The Talons|Talon". Talon encourages Damians choice of punishing criminals, and even offers Damian a chance to join him. Damian is unsure of this decision, and returns home where Bruce is waiting, disappointed that Damian snuck out again. Bruce warns Damian that hell send him away to a school in Switzerland unless he learns to discipline himself, yet the next night, Damian sneaks out again.

On the way to a date with Samantha, Bruce is attacked by white owl-masked people and brought before the Court of Owls, in the flesh, who now offer Bruce a chance to join them as a member of one of Gothams wealthiest families. After Bruce leaves, given time to consider the offer, its revealed that Talon is working for the Court of Owls, who are secretly raising an army of the inhuman warriors, also known as "talons", through a process similar to the Lazarus Pit, so that they can destroy Gotham City and allow the Court to rebuild it as they see fit. Unfortunately, the serum theyve developed to awaken the talons only lets them last for 24 hours at a time outside of their storage tanks before they rot and dissolve. Talon will soon be forced by the Court to undergo the process himself and become one of these monsters, but not before bringing Damian over as his apprentice and replacement.

Damian, having contacted Talon, has gone out on missions with him to take out some criminals. Damian has constantly hesitated to kill due to Batmans code, which frustrates Talon, who tells Damian hell find another apprentice if he wont cross that line. Sensing how Damian looks up to Batman as a father, Talon tells his story about how he and his own father were thieves; Talon looked up to his father and wanted to be just like him, but couldnt, and was always punished for it. One night, Talon gave his father up to the police, who shot him, and afterwards, Talon was recruited by the Court of Owls. Batman shows up, having tracked Damians activity, but is blocked by Damian from capturing Talon. Batman tries to convince Damian that Talon and the Court of Owls are using Damian to get to him, but Damian believes Batman is selfish, trying to hold him back from his potential. A confrontation breaks out between father and son, resulting in Damian almost killing Batman, but sparing him instead. Damian leaves Batman.

Talon is revealed to be in a relationship with Samantha, who is secretly the Grandmaster of the Court of Owls. Despite their social class divide, Samantha wants to spare Talon from the immortality ritual and have him stand by her side, as they plan to destroy Batman with Robin. Batman sneaks into the Court of Owls through the sewers, and when he arrives, is revealed to have been drugged by the Court with a drug similar to the Scarecrows fear gas, causing Batman to hallucinate the night his parents were murdered. There, he sees the shooter is revealed to be not Joe Chill, but a grown up Damian as Batman. Realizing that his identity as Batman will cause Damian the same rage and hatred as the Court of Owls, Bruce asks the grown up Damian for forgiveness, who then reverts to the child Damian.

Bruce awakens from his hallucination, finding himself back at the Batcave with Alfred Pennyworth and Nightwing. Bruce now accepts Damian as his son, determined to find him before the Court turns him into a monster. Meanwhile, Talon introduces Damian to the Court of Owls, but when Damian reveals himself to the Grandmaster, Samantha realizes that if Robin is Bruce Waynes son, then Bruce must be Batman. Ordering Talon to kill Damian, Talon instead turns against the Court of Owls, murdering every member, including Samantha. Now in control of the Courts army of resurrected "talons", Talon leaves Damian behind at the Court, offering him another chance to join him once he has killed Batman.

Talon and his undead soldiers attack the mansion, breaking into the Batcave as Batman, Nightwing, and Alfred fight them off. Batman and Talon ultimately face off, with Talon gaining the upper hand, before Damian, who escaped from the Court, comes in to take over for a badly wounded Batman. Damian eventually defeats Talon, holding a sai to his throat. After telling Damian one last time never to doubt his instincts, Talon commits suicide by pulling Damians sai through his neck, leaving Damian stunned and confused.

Bruce tries to welcome Damian back home, but Damian refuses claiming he needs to leave and sort out who he is after this whole ordeal. Damian accepts Bruce as his father, then leaves for a monastery in the Himalayas. Bruce is confident that eventually Damian will return... not because Bruce wants him to, but because hell want to.

==Cast== Bruce Wayne / Batman  Damian Wayne / Robin   
* David McCallum - Alfred Pennyworth 
* Troy Baker - Court of Owls Lieutenant   
* Kevin Conroy - Thomas Wayne 
* Trevor Devall - Jack 
* Robin Atkin Downes - Court of Owls Grandmaster 
* Griffin Gluck - Young Bruce Wayne  Grey Griffin - Samantha Vanaver  Dick Grayson / Nightwing 
* Peter Onorati - Draco 
* Andrea Romano - Jill  Talon  Anton Schott / The Dollmaker 

==Crew==
* Andrea Romano - Casting and Voice Director

==Reception==
 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 

 
 