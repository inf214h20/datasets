I Know Where I'm Going!
 I Know Where Im Going}}
 
 
{{Infobox film
| name           = I Know Where Im Going!
| image          = I know where im going.jpg
| image_size     =
| caption        = theatrical poster
| director       = Michael Powell Emeric Pressburger
| producer       = Michael Powell Emeric Pressburger  George R. Busby (associate producer)
| writer         = Michael Powell Emeric Pressburger
| starring       = Wendy Hiller Roger Livesey Allan Gray
| cinematography = Erwin Hillier
| editing        = John Seabourne Sr.
| distributor    = General Film Distributors
| released       = 16 November 1945 (UK) 9 August 1947 (US)
| runtime        = 88 minutes
| country        = United Kingdom Gaelic
| budget         = £200,000 (est.)
| gross          =
}} Michael Powell Pamela Brown, Finlay Currie and Petula Clark in her fourth film appearance.

==Plot==
Joan Webster (Wendy Hiller) is a young middle class Englishwoman with an ambitious, independent spirit. She knows where shes going, or at least she thinks she does. She travels from her home in Manchester to the Hebrides to marry Sir Robert Bellinger, a very wealthy, much older industrialist, on the (fictitious) Isle of Kiloran.
 Pamela Brown).

The next day, on their way to catch a bus into town, they come upon the ruins of Moy Castle. Joan wants to take a look inside, but Torquil refuses to go in. When she reminds him that the terrible curse only applies to the Laird of Kiloran, Torquil introduces himself: he is the laird, and Bellinger has only leased his island. As the bad weather worsens into a full-scale gale, Torquil takes advantage of the delay to woo Joan, who becomes increasingly torn between her ambition and her growing attraction to him.
 Corryvreckan whirlpool, but Torquil is able to restart the motor just in time and they return safely to Mull.

At last, the weather clears. Joan asks Torquil for a parting kiss before they go their separate ways. Torquil enters Moy Castle, and the curse takes effect almost immediately. A narrator relates that, centuries earlier, Torquils ancestor had stormed the castle to capture his unfaithful wife and her lover. He had them bound together and cast into a water-filled dungeon with only a small stone to stand upon. When their strength gave out, they dragged each other into the water, but not before she placed a curse on the Lairds of Kiloran. Any who dared to step over the threshold would be chained to a woman to the end of his days. From the battlements, Torquil sees Joan with three pipers marching resolutely toward him. They embrace.

==Cast==

* Wendy Hiller as Joan Webster
* Roger Livesey as Torquil MacNeil Pamela Brown as Catriona
* Finlay Currie as Ruairidh Mhór
* George Carney as Mr. Webster
* Nancy Price as Mrs. Crozier
* Catherine Lacey as Mrs. Robinson, a chatterbox friend of Bellingers who is on holiday in the area
* Jean Cadell as the Postmistress diamond anniversary céilidh Torquil and Joan attend
* Valentine Dyall as Mr. Robinson, a business associate of Bellingers and Mrs. Robinsons husband
* Norman Shelley as Sir Robert Bellinger (voice)
* Margot Fitzsimons as Bridie
* Murdo Morrison as Kenny falconer and friend of Torquil and Catriona
* Walter Hudd as Hunter, one of Bellingers employees

Cast notes:
*Petula Clark played Cheril, the precocious daughter of the Robinsons, in her fourth film appearance. A Matter of Life and Death, also known as Stairway to Heaven (1946). 

==Production==
The original story and the whole screenplay were written in less than a week.  Pressburger said it just flowed naturally. 

The film was shot in black and white while Powell and Pressburger were waiting for Technicolor film to begin making their next film, A Matter of Life and Death (colour film was in short supply in wartime Britain).  It was the second and last collaboration between the co-directors and cinematographer Erwin Hillier (who shot the entire film without using a light meter). In the documentary I Know Where Im Going Revisited (1994) on the Criterion DVD 

From various topographical references and a map briefly shown in the film, it is clear that the Isle of Kiloran is based on Colonsay.  The name Kiloran was borrowed from one of Colonsays bays, Kiloran Bay. The heroine of the film is trying to get to Kiloran (Colonsay), but nobody ever gets there. No footage was shot on Colonsay.
 Jura and the Gray Dogs (Bealach aChoin Ghlais) between Scarba and Lunga, Firth of Lorn|Lunga. 
*There are some long distance shots looking down over the area, shot from one of the islands.
*There are some middle distance and close-up shots that were made from a small boat with a hand-held camera.
*There were some model shots, done in the tank at the studio.  These had gelatin added to the water so that it would hold its shape better and would look better when scaled up. Usually the way that waves break and the size of water drops is a give-away for model shots done in a tank.
*Then there were also the close-up shots of the people in the boat.  These were all done in the studio, with a boat on gimbals being rocked in all directions by some hefty studio hands while other studio hands threw buckets of water at them. These were filmed with the shots made from the boat with the hand-held camera projected behind them.
*Even then, there was further trickery where they joined together some of the long and middle distance shots with those made in the tank in a single frame.   
 Denham and a double was used in all of his scenes shot in Scotland.  These shots were then mixed so that the same scene would often have a middle distance shot of the double and then a closeup of Livesey or a shot of the doubles back and then a shot showing Liveseys face.
 Glasgow Orpheus Choir. 
 Allan Gray.

==Locations==
* On the Isle of Mull
** Carsaig Bay: Carsaig Pier and boathouses, Carsaig House (Erraig), telephone box next to the waterfall.
** Moy Castle - Castle of Moy
** Duart Castle - Castle of Sorne
** Torosay Castle - Achnacroish
** Gulf of Corryvreckan - the whirlpool

==Reception==
The film has received accolades from many critics:
*"Ive never seen a picture which smelled of the wind and rain in quite this way nor one which so beautifully exploited the kind of scenery people actually live with, rather than the kind which is commercialised as a show place." – Raymond Chandler, Letters. 
*"The cast makes the best possible use of some natural, unforced dialogue, and there is some glorious outdoor photography." – The Times, 14 November 1945
*"  has interest and integrity.  It deserves to have successors." – The Guardian, 16 November 1945
*"I reached the point of thinking there were no more masterpieces to discover, until I saw I Know Where Im Going!" – Martin Scorsese 
*The film critic Barry Norman included it among his 100 greatest films of all time.
*The film critic Molly Haskell included it among her 10 greatest films of all time, in the 2012 Sight & Sound poll.

==References==

===Notes===
 

===Bibliography===
 
* {{cite book
|last=Cook
|first=Pam
|authorlink=Pam Cook
|title=I know where Im going!
|date=4 November 2002
|publisher=British Film Institute
|location=London
|isbn=978-0-85170-814-0}}
*{{cite book
|last=Powell
|first=Michael
|authorlink=Michael Powell
|title=A Life in Movies
|type=autobiography
|year=1986
|publisher=Heinemann
|location=London
|isbn=0-434-59945-X}}
*{{cite book
|last=Powell
|first=Michael
|authorlink=Michael Powell
|title=Million Dollar Movie
|type=The second volume of his autobiography
|year=1992
|publisher=Heinemann
|location=London
|isbn=0-434-59947-6}}
 

==External links==
*  
*  
*  
*  . Full synopsis and film stills (and clips viewable from UK libraries).
*   at the  
*  
*  . A documentary about the people and places in the film.
*   resource page.

===DVD reviews===
; Region 1 
*  by DVD Savant
*  by Megan Ratner at Bright Lights

; Region 2 
*  by Noel Megahey at DVD Times (UK)
*  (in French) at DVD Classik (France)

 

 
 
 
 
 
 
 
 
 
 
 