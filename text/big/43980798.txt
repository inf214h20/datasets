April 19 (film)
{{Infobox film
| name = April 19
| image =
| caption =
| director = Balachandra Menon
| producer = Mohan Vettathu
| writer = Balachandra Menon
| screenplay = Balachandra Menon Nandini Shanthi Krishna
| music = Raveendran
| cinematography = Anandakkuttan
| editing = G Murali
| studio = GVJ Films
| distributor = GVJ Films
| released =  
| country = India Malayalam
}}
 1996 Cinema Indian Malayalam Malayalam film, Nandini and Shanthi Krishna in lead roles. The film had musical score by Raveendran. 
 

==Cast==
  
*Jagathy Sreekumar 
*Balachandra Menon  Nandini 
*Shanthi Krishna 
 

==Soundtrack==
The music was composed by Raveendran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Arivinum Arulinum || Raveendran, Chorus, Roshni || S Ramesan Nair || 
|-  Janaki || S Ramesan Nair || 
|- 
| 3 || Mazha Peythaal   || K. J. Yesudas || S Ramesan Nair || 
|-  Janaki || S Ramesan Nair || 
|-  Janaki || S Ramesan Nair || 
|}

==References==
 

==External links==

 

 
 
 
 


 