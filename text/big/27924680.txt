Deeper and Deeper (film)
{{Infobox film
| name           = Deeper and Deeper
| image          = Deeper and deeper cover 4.jpg
| caption        =
| director       = Mariusz Kotowski 
| producer       = Mariusz Kotowksi Gilda Longoria
| writer         = Cyndi Williams
| story          = Mariusz Kotowksi
| starring       = David Lago Cyndi Williams LisaMarie Lamendola
| music          = Rick DeJonge 
| cinematography = Leon Rodriguez 
| editing        = Brian Burrowes
| studio         = Bright Shining City Productions
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
}}
Deeper and Deeper is an 2010 American erotic psychological thriller written by Cyndi Williams and directed by Polish-American filmmaker Mariusz Kotowski. 

==Plot==
When bank teller Ryan (David Lago) meets beautiful but icy business executive Angelica (LisaMarie Lamendola) he becomes instantly infatuated with her.  He stumbles upon the perfect opportunity to spy on her when he is hired by Dolores (Cyndi Williams) as a part-time maintenance man and moves into an apartment across the street from Angelica.  He uses his position and his apartments view to start spying on her, and slowly inserts himself into Angelicas life.  Angelica turns on him and Ryan slits his wrists in the bathtub in despair. When items from Ryans apartment begin explicitly appearing in Angelicas apartment, Dolores becomes suspicious of Angelica, especially when Angelica visits the apartment building and produces a memento from Dolores late son.

==Cast==
 
*David Lago as Ryan
*Cyndi Williams as Dolores
*LisaMarie Lamendola as Angelica
*Drew Waters as John
*Jennifer Finley as Jenny
*Ashley Rene-Hallford as Devin
*John Finan as Mr. Collins
*Sydney Barrosse as Robyn
*Kristi Jennings as Elena
*Ben Leffler as Matthew
*Mamie Meek as Mrs. Jenkins
 

==Background== 7th Heaven. 

Kotowskis previous directorial credits include the narrative  , a biographical documentary about the life of silent film star Pola Negri. 

==Release and recognition==
Deeper and Deeper premiered on April 26, 2010 at the Eleventh Annual Polish Film Festival of Los Angeles, California.  The film received its debut screening in Austin, Texas at the Violet Crown Cinema  on June 20, 2011. 

The film was nominated for Best Director (Mariusz Kotowski), Best Actor (David Lago, Best Screenplay (Cyndi Williams), and Best Actress (LisaMarie Lamendola) at The World Music & Independent Film Festival,  won Best Actor (David Lago) and received Honorable Mention for best Narrative Film at the Los Angeles Reel Film Festival,  won First Place for best Suspense Thriller at the International Indie Gathering,  and won bronze medal for Dramatic Feature in the 2010 JamFest Indie Film Festival Awards. 

In 2010, Deeper and Deeper also won an Accolade Competition Award of Merit,  an Indie Fest Award of Merit,  and a Platinum EMPixx Award.  The film since also been screened at the Bare Bones Festival  and the 16th Annual Temecula Valley International Film and Music Festival in Temecula, CA. 

===Awards and nominations===
* 2010, Won Best Actor for David Lago at Los Angeles Reel Film Festival  
* 2010, Won Honorable Mention for Best Narrative Film at Los Angeles Reel Film Festival 
* 2010, Won First Place for Best Suspense Thriller at International Indie Gathering 
* 2010, Won bronze medal for Dramatic Feature at JamFest Indie Film Festival 
* 2010, Won Award of Merit at Accolade Competition 
* 2010, Won Award of Merit at Indie Fest 
* 2010, Won Platinum Award at EMPixx Awards 
* 2010, Nomination for Best Director for Mariusz Kotowski at World Music & Independent Film Festival   
* 2010, Nomination for Best Actor for David Lago at World Music & Independent Film Festival   
* 2010, Nomination for Best Screenplay for Cyndi Williams at World Music & Independent Film Festival   
* 2010, Nomination for Best Actress for LisaMarie Lamendola at World Music & Independent Film Festival 

==Soundtrack== Temptation Island and on MTVs Total Request Live.  The other two, "I Am a Dangerous Woman" and a vocal version of the Deeper and Deeper theme song, are performed by Sydney Barrosse,  who appears in the music video for the vocal version of the Deeper and Deeper theme.  Barrosse also appears as an actress in the film, playing Robyn, Angelicas business partner.

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

  . Accessed 3 Oct 2010. 

 Deeper and Deeper soundtrack CD liner notes. 

}}

== External links ==
*  
*  
*  

 
 
 