Detective K: Secret of the Lost Island
{{Infobox film name           = Detective K: Secret of the Lost Island image          =  director       = Kim Sok-yun producer       = Kim Jho Gwangsoo    Lee Sun-mi writer         = Lee Nam-kyu   Kim Su-jin starring       = Kim Myung-min   Oh Dal-su   Lee Yeon-hee music          = Kim Han-jo  cinematography = Jang Nam-cheol editing        = Im Sun-kyong studio         = Generation Blue Films  distributor    = Showbox/Mediaplex  released       =   runtime        = 124 minutes country        = South Korea language       = Korean   Japanese budget         =  gross          =  
}}
Detective K: Secret of the Lost Island ( ; lit. "Joseon Detective: The Disappearance of the Laborers Daughter") is a 2015 South Korean  .  

==Plot==
Set in Joseon in the year 1795, nobleman, inventor and master sleuth Kim Min and his sidekick Seo-pil investigate a silver counterfeiting ring, which may be linked to the disappearance of hundreds of girls.

==Cast==
*Kim Myung-min as Kim Min, "Detective K" 
*Oh Dal-su as Han Seo-pil
*Lee Yeon-hee as Hisako 
*Jo Kwan-woo as Musician Jo 
*Jung Won-joong as Senior colleague
*Lee Chae-eun as Da-hae 
*Hwang Chae-won as Do-hae 
*Hwang Jeong-min as Sakura 
*Woo Hyun as Mr. Bang 
*Choi Moo-sung as Boss 
*Park Soo-young as Official request man (cameo appearance|cameo)
*Kim Won-hae as Sato (cameo)
*Hyun Woo as Vampire (cameo)

==Box office==
The film was released on February 11, 2015. It topped the box office on its opening weekend, earning   ( ) from 960,000 admissions over five days.   By the end of its theatrical run, it recorded 3,872,025 admissions, grossing  . 

==References==
 

==External links==
*   
*  at Showbox
* 
* 
* 

 
 
 
 
 