When Will I Be Loved (film)
{{Infobox film
| name = When Will I Be Loved
| image = When Will I Be Loved (film poster).jpg
| caption = theatrical poster
| director = James Toback
| writer = James Toback
| starring = Neve Campbell Fred Weller Alex Feldma
| producer = Ron Rotholz
| distributor = IFC Films
| country = United States
| released =  
| runtime = 81 minutes
| language = English
| budget =
| gross =
}}

When Will I Be Loved is a 2004 American drama film written and directed by James Toback and starring Neve Campbell. The film had a 35-page script and was mostly improvised  throughout its 12-day shoot. 

== Plot ==
The movie begins with a five minute treatment of a nude Neve Campbell masturbating with a shower nozzle, setting the tone for the movie.

Vera (  who begins exploring the limits of her sexual and intellectual power. She picks up men on the street and has sex with them in her apartment. She also videotapes a sexual romp with a female lover, and has sexually frank discussions with her potential employer. As the daughter of wealthy, indulgent parents, Vera seems to be improvising her way through the beginning of her life as an adult.

Her boyfriend, Ford (Frederick Weller), is a fast-talking hustler prepared to do anything to make a buck. Aware of Veras promiscuity, Ford sees a chance to make big money when he meets an aging Italian media mogul, named Count Tommaso (Dominic Chianese), who is enamored of Vera because of her sexuality, her intelligence, and what he perceives as her naiveté. Ford cooks up an idea to pimp Vera out to the Count for $100,000, easy money, if he can only talk Vera into it. Incredibly, she agrees. Everything appears to be going even better than planned.

But both men have gravely underestimated Vera, who has an agenda of her own. Ford and the Count unwittingly play right into her hands, and when her plan of deception and manipulation comes to fruition, the results are staggering.

== Cast ==
* Neve Campbell as Vera Barrie
* Frederick Weller as Ford Welles
* Dominic Chianese as Count Tommaso Lupo
* Ashley Shelton as Ashley
* James Toback as Professor Hassan Al-Ibrahim Ben Rabinowitz
* Oliver "Power" Grant as Power
* Mike Tyson as himself

==Reviews==
When Will I Be Loved received generally negative reviews.   gave the film a 39/100 indicating "generally unfavorable reviews.  

Despite being reviled by most critics, film critic Roger Ebert gave the film a perfect score of 4 out of 4 Stars saying that "When Will I Be Loved is like a jazz solo that touches familiar themes on its way to a triumphant and unexpected conclusion."  

== References ==
Notes
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 