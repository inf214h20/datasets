Police Story (1979 film)
{{Infobox film
| name           = Police Story
| image          = Police Story (1978).jpg
| caption        = Theatrical poster for Police Story (1979)
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Gyeongchalgwan
 | mr             = Kyŏngch‘algwan}}
| director       = Lee Doo-yong 
| producer       = Kwak Jeong-hwan
| writer         = Lee Moon-woong
| starring       = Jang Dong-he
| music          = Kim Hee-kap
| cinematography = Son Hyun-chae
| editing        = Lee Kyung-ja
| distributor    = HapDong Films
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}  1979 South Korean film directed by Lee Doo-yong. It was chosen as Best Film at the Grand Bell Awards.     

==Synopsis==
A melodrama about a man who chooses the career of a police officer in spite of his girlfriends objection and social stigma. After he is injured in the line of duty, he and his girlfriend get married. 

==Cast==
* Jang Dong-he 
* Han So-ryong
* Yu Ji-in
* Moon Jung-suk
* Do Kum-bong
* Bang Su-il
* Sin Mu-il
* Kim Young-in
* Han Kug-nam
* Choe Jae-ho

==Bibliography==

===English===
*   
*  
*  

===Korean===
*  
*  

==Notes==
 

== External links ==
*  

 
 
 
 
 
 

 
 
 
 
 
 
 


 