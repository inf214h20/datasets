Tom and Jerry: Santa's Little Helpers
{{Infobox film
| name           = Tom and Jerry: Santas Little Helpers
| image          =  
| Caption        =
| director       = Darrel Van Citters
| producer       = 
| writer         = Jim Praytor & Robert Zappia
| screenplay     =
| story          =
| based on       = Tom and Jerry by William Hanna & Joseph Barbera
| cast           = 
| starring       = 
| music          = David Ricard   John Van Tongeren
| cinematography =
| editing        = Michael D Ambrosio
| studio         = Turner Entertainment Warner Bros. Animation Renegade Animation
| distributor    = Warner Home Video
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Tom and Jerry: Santas Little Helpers is a 2014 animated Direct-to-DVD special starring Tom and Jerry, produced by Warner Bros. Animation.  It was made available as part of a 2-disc DVD set of the same name, which also contains 29 other Tom and Jerry cartoons and episodes from Tom and Jerry Tales, on October 7, 2014. 

==DVD Synopsis== Tom and Jerry (joined Nibbles Mouse, who is known as Tuffy again) return in this new 2-Disc collection of their most wonderful wintertime adventures. Included in this compilation is the all-new holiday special, Santa’s Little Helpers, where Jerry and Tuffy are living the good life in Santa’s workshop, until the unfortunate day on which Tom is rescued by the Clause family. With Tom in the house, merry mayhem ensues at the North Pole, but when the dust settles, the destructive duo now have to work together to save Christmas and learn the true meaning of friendship. 

In addition to the special, the DVD collection contains episodes The Abominable Snowmouse, Snow Brawl and Polar Peril from Tom and Jerry Tales, as well as 26 other cartoons.

==Trivia== The Tom and Jerry Show, taking into account being animated with Flash like the show was.

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 