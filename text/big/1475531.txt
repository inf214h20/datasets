Turtles Can Fly
 
{{Infobox film
| name = Turtles Can Fly
| image = Turtles Can Fly poster.jpg
| image_size = 
| alt = 
| caption = US theatrical release poster
| director = Bahman Ghobadi
| producer = Babak Amini Hamid Ghobadi Hamid Ghavami Bahman Ghobadi
| writer = Bahman Ghobadi
| starring = Soran Ebrahim Avaz Latif
| music = Hossein Alizadeh
| cinematography = Shahriah Assadi
| editing = Mostafa Kherghehpoosh Hayedeh Safiyari
| studio = Mij Film Co. Bac Film
| distributor = IFC Films  
| released =  
| runtime = 97 minutes  
| country = Iran, France, Iraq
| language = Kurdish
| budget = 
| gross = $1,075,553
}}
Turtles Can Fly ( : Kûsî Jî Dikarin Bifirin   drama film written, produced, and directed by Bahman Ghobadi, with notable theme music composed by Hossein Alizadeh. It was the first film to be made in Iraq after the fall of Saddam Hussein.

==Plot== Kurdish refugee Turkish border US invasion antennae (for manipulative leader of the children, organizing the dangerous but necessary sweeping and clearing of the land mine|minefields. Many of these children are injured one way or the other, yet still maintain a boisterous prattle whenever possible, devoted to their work in spite of the vagaries of their life.

The industrious Satellite arranges trade-ins for undetonated mines. He falls for an orphan named Agrin, assisting her whenever possible in order to win her over. She is a perpetual dour-faced girl who is part bitter, part lost in thought, unable to escape the demons of the past. Traveling with her is her disabled, but very caring brother Hengov, who appears to have the gift of clairvoyance, though he seems to have a bad reputation for it. The siblings stay with a blind toddler named Riga. On course, we come to know that Agrin gave birth to him after she was gang raped by soldiers (In Agrin and Hengovs village, the girls had been killed after being raped, while the boys and their families were plain butchered. Hengovs arms had been shot as the soldiers attempted to drown both children). Throughout the film, we see how Agrin has been unable to accept Riga (who according to Agrin mysteriously unties his tethered leg and walks at night and returns to the camp safely despite his claims of being blind) as anything besides a taint, a continuous reminder of a brutal past.

Agrin tries to abandon the child on multiple occasions, until finally she ties him to a rock and throws him to the bottom of the lake, latter going in for suicide herself from a cliff. When her brother sees a vision of his loved ones drowning, he hurries out of the tent crying and tries to salvage whatever can be. However, his nightmare is late & indeed all is lost by then. On his way to the lake, a flashback of Agrin in her final moments haunts Hengov standing on the abandoned tanks, Hengov eventually finds his nephew s body at the bottom of the lake but cant cut it loose from the rock partly due to his inability. At last he is seen grieving on the cliff from where Agrin jumped to her death, where he ultimately collects the shoes left behind by Agrin before the fatal leap.
By the turn of the events including a disabled Satellite, the same protagonist loses any charm he had about the American intervention and looks away when the American soldiers finally pass close by him.

==Cast==
* Soran Ebrahim as Satellite
* Avaz Latif as Agrin
* Hiresh Feysal Rahman as Hengov
* Abdol Rahman Karim as Riga
* Ajil Zibari as Shirkooh

==Reception==
Turtles Can Fly received generally positive reviews, currently holding a 90% "fresh" rating on Rotten Tomatoes  and an 85/100 rating on Metacritic, signifying "universal acclaim". 

Significantly, the film is silent about what happens to Satellite after the Americans finally land in their refugee camp. Some critics   believe that the film reflects the true sentiment of Kurds, many of whom suffered greatly under the dictatorship of Saddam Hussein and strongly supported the US military invasion and occupation of Iraq.

==Awards==
# Glass Bear, Best Feature Film and Peace Film Award, Berlin International Film Festival, 2005
# Golden Seashell, Best Film, San Sebastián International Film Festival, 2004
# Special Jury Award, Chicago International Film Festival, 2004
# International Jury and Audience Awards, São Paulo International Film Festival, 2004
# La Pieza Award, Best Film, Mexico City International Contemporary Film Festival, 2005
# Audience Award, Rotterdam International Film Festival, 2005
# Golden Prometheus, Best Film, Tbilisi International Film Festival, 2005
# Aurora Award, Tromsø International Film Festival, 2005
# Golden Butterfly, Isfahan International Festival of Films for Children, 2004
# Gold Dolphin, Festróia - Tróia International Film Festival, 2005
# Sundance Selection, 2005
# Silver Skeleton Award Harvest Moonlight Festival 2007

==In popular culture==
The film had an influence on the 2007-2009 Gundam anime series Mobile Suit Gundam 00. The animes main protagonist Setsuna F. Seiei is a war orphan of Kurdish origins and his real name is Soran Ibrahim, a reference to the child actor portraying the protagonist of Turtles Can Fly. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 