Irada Pakka
{{Infobox film
| name           = Irada Pakka
| image          = Irada Pakka.jpg
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Kedar Shinde
| producer       = Smita Meghe
| writer         = 
| starring       = Siddharth Jadhav Sonalee Kulkarni Mohan Joshi
| music          = Nilesh Mohril, Pankaj and Pushkar Mahabal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Irada Pakka is a Marathi film released on 23 April 2010.  Produced by Smita Meghe and directed by Kedar Shinde. The movie is based on happily married couple who are so bored with their daily routine life that they decide to take an uncharted path in their relationship and see where it lands. A poignant and gripping human drama takes the audience on a journey into introspection.

== Synopsis ==
Rohit and Adhya are happily married and in order to break the monotony in life, they decide to play a game; little knowing that destiny had something else in store for them. A game in which unprecedented events take a different turn, a turn for the worst and threaten their very existence.

== Cast ==

The cast include
*Siddharth Jadhav as Rohit
*Sonalee Kulkarni as Adhya
*Mohan Joshi as Adhya’s father 
*Smita Jaykar as Adhya’s mother
*Shalaka Pawar as Savitri & others

==Soundtrack==
The music is provided by Nilesh Mohril, Pankaj and Pushkar Mahabal. It was launched worldwide at Ravindra Natya Mandir Dadar on 29th Jan 2010.
  The album Contained 6 original and 3 remix songs. Album witnessed huge success with Bhijun Gela Wara and Kadhi Kadhi appeales masses. Nilesh Mohril composed majority of album and Pankaj Pushkar was roped in for title track. 
 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s) !! Duration !! Lyrics !! Composer
|-
| Aata Maage Na Jane
| Avadhoot Gupte, Vaishali Made
| 4:47
| Sumthi Vangede
| Nilesh Mohrir
|-
| Bhijun Gela Wara
| Kshitij Tarey,  Nihira Joshi
| 4:41
|  Ashwini Shende
| Nilesh Mohrir
|-
| Banda Padaliye Aapli Gaadi
| Avadhoot Gupte, Kshitij Tarey, Nilesh Mohrir, Poulami Pete, Pushkar Mahabal
| 4:41
| Ashwini Shende
| Nilesh Mohrir
|-
| Kadhi Kadhi
| Javed Ali
| 4:34
| Ashwini Shende
| Nilesh Mohrir
|-
| Kadhi Kadhi (Lounge Mix)
| Javed Ali
| 4:38
| Ashwini Shende
| Nilesh Mohrir
|-
| Banda Padaliye Aapli Gaadi (remix)
| Avadhoot Gupte
| 4:41
| Ashwini Shende
| Nilesh Mohrir
|-
| Ek Kolha Bahu Bhukela
| Jaanvee Prabhu Arora
| 1:38
| Nimish Dath
| Pankaj - Pushkar
|-
| Irada Pakka
| Hindol Pendse, Pushkar Mahabal
| 1:38
| Nimish Dath
| Pankaj - Pushkar
|-
| Irada Pakka (Hindi)
| Hindol Pendse, Pushkar Mahabal
| 2:31
| Tanveer Ghazi
| Pankaj - Pushkar

|}

== References ==
 
 

== External links ==
*  
*  
*  
*  

 
 
 


 