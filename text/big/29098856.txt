Soul-Fire
{{Infobox film
| name           = Soul-Fire
| image          =
| caption        =
| director       = John S. Robertson
| producer       = Inspiration Pictures (aka Richard Barthelmess)
| writer         = Martin Brown (play; Great Music) Josephine Lovett (scenario)
| starring       = Richard Barthelmess Bessie Love
| music          =
| cinematography = Roy Overbaugh William Hamilton
| distributor    = First National Pictures
| released       =  
| runtime        = 9 reels at 8,262 feet
| country        = United States
| language       = Silent film (English intertitles)
}}
 1925 silent silent drama starring Richard Barthelmess and Bessie Love; directed by John S. Robertson; and is based on the Broadway production Great Music (1924) by Martin Brown. 

The film was funded by Barthelmess through his Inspiration Pictures and released by the   and Harriet Sterling (also known on Broadway as Harriet Steeling). 

==Cast==
* Richard Barthelmess as Eric Fane
* Bessie Love as Teita
* Percy Ames as Critic
* Charles Esdale as Critic
* Effie Shannon as Mrs. Howard Fane, Erics mother
* Lee Baker as Howard Fane, Erics father
* Carlotta Monterey as Princess Rhea
* Gus Weinberg as The Old Musician
* Ann Brody as Princess Rheas maid
* Helen Ware as San Francisco Sal Walter Long as Herbert Jones Sailor
* Rita Rossi as The Prima Donna
* Edward LaRoche as The Orchestra Leader
* Harriet Sterling (billed as Harriet Steeling) as Ruau Richard Harlan as NuKu
* Ellalee Ruby as Dancer in a Music Hall
* Arthur Metcalfe as Dr. Travers, of Leper Island
* George Pauncefort as Simpson, an Attorney
* Aline Berry as Fleurette, the Mannequin

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 