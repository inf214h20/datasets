The Expendables (2000 film)
 
The Expendables is a 2000 film directed by Janet Myers. It was written by Patricia Resnick, Peter Geiger and Bruno Heller. It stars Brett Cullen, Robin Givens, Idalis DeLeón and Tempestt Bledsoe. It aired April 25, 2000 on the USA Network.  

== Plot ==

A group of female convicts volunteers for a mission to rescue a woman from a Cuban prison.

== Cast ==

* Brett Cullen as Deacon
* Robin Givens as Randy
* Idalis DeLeon as Ver (as Idalis de León)
* Tempestt Bledsoe as Tanika
* Jenica Bergere as Sue
* Cristi Conaway as Nicoline
* Jennifer Blanc as Christine
* Julie Carmen as Jackie
* Megan Cavanagh as Prison Warden (as Megan Cavenagh)
* Thom Barry as Tyler
* Annette Helde as Rosa
* Eileen Weisinger as Anna (as Ayleen Weisinger)
* Wil Cesares as Jorge
* David Norona as Ramone
* Omar Ynigo as Maquito

== References ==
 

==External links==
*  

 
 
 
 

 