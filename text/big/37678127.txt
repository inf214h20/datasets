Clear Skies (film)
 
{{Infobox film
| name           = Clear Skies
| image          = 
| caption        = 
| director       = Grigori Chukhrai
| producer       = Vladimir Kantorovich
| writer         = Daniil Khrabrovitsky
| starring       = Yevgeni Urbansky
| music          = Mikhail Ziv
| cinematography = Sergei Poluyanov
| editing        = Maria Timofeeva
| studio         = Mosfilm
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Clear Skies ( , Transliteration|translit.&nbsp;Chistoe nebo) is a 1961 Soviet romance film directed by Grigori Chukhrai. It won the Grand Prix (in a tie with Kaneto Shindos The Naked Island) at the 2nd Moscow International Film Festival.   

==Cast==
* Yevgeni Urbansky as Aleksei Astakhov
* Nina Drobysheva as Sasha Lvova
* Natalya Kuzmina as Lyusya (as N. Kuzmina)
* Vitali Konyayev as Petya (as V. Konyayev) Georgi Kulikov as Mitya (as G. Kulikov)
* Leonid Knyazev as Ivan Ilyich (as L. Knyazev)
* Georgi Georgiu as Nikolai Avdeyevich (as G. Georgiu)
* Oleg Tabakov as Seryozhka (as O. Tabakov)
* Alik Krylov as Sergey Vitali Bondarev as Yegorka (as Vitalik Bondarev)

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 