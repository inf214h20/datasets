Mere Genie Uncle
 

{{Infobox film
| name           = Mere Genie Uncle
| image          = Theatrical release poster.png
| caption        = Theatrical release poster
| director       = Ashish Bhavsar
| producer       = Raju Gada   Ashish Bhavsar.
| story          = Ashish Bhavsar
| screenplay     = Ashish Bhavsar
| Dialogue       = Ashish Bhavsar
| editing        = Rajesh Khanchi
| starring       = Tiku Talsania Shakti Kapoor Swati Kapoor
| music          = Vandana Vadhera
| cinematography = Nazir Khan   Palash Bose 
| studio         = Aar Motion Pictures
| released       =  
| country        = India
| language       = Hindi
| gross          = 
}}

Mere Genie Uncle (  3D movie directed by Ashish Bhavsar and produced by Raju Gada. The film stars Tiku Talsania, Swati Kapoor   Shakti Kapoor, Ehsaan Qureshi among many others. The film shoot is complete and is slated to release on 5 Jun 2015.

==Cast==
* Tiku Talsania as Genie & I M Patel
* Swati Kapoor as Ria Bannerjee
* Anuj Sikri as Sidd Patel
* Shakti Kapoor as Jr. Jaffar
* Ehsaan Qureshi as Sher Khan
* Navina Bole as Haseena Mallik
* Mushtaq Khan as ATS officer Salunkhe
* Asheish Roy as Guru Ganguly
* Pankaj Kalra as Dhondu Bhai

==Child Artist==

* Pratham Kalra as Montu
* Yash Acharya as Don
* Ayyan Mallik as Happy Singh
* Jannat Khan as Ronnie studies at Avalon Heights International School
* Hetvi Charla as Beena

==Synopsis==
Mere Genie Uncle is a 3D Movie for the Entire Indian Family with special emphasis on kids.

==Soundtrack==
The album is arranged and sequenced by Nyzel Dlima. The songs are written and composed by Vandana Vadehra.

{{Infobox album
| Name = Mere Genie Uncle
| Type = Soundtrack
| Artist = Vandana Vadhera
| Genre = Film soundtrack
}}

===Track listing===

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = Vandana Vadehra
| title1  = Oh Tere Ki 
| extra1  = Vandana Vadhera
| lyrics1 = Vandana Vadhera
| title2  = Christmas Day  
| extra2  = Thomas & Kids
| lyrics2 = Vandana Vadhera
| title3  = Naa Jaane Kyun
| extra3  = Rahul Pandey   Gewn Mathais
| lyrics3 = Vandana Vadhera
| title4  = Lori 
| extra4  = Pappon
| lyrics4 = Vandana Vadhera
}}

==References==
 

==External links==

 