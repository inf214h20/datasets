The Black Butterfly
{{infobox film
| title          = The Black Butterfly
| image          = Theblackbutterfly-newspaperad-1916.jpg
| caption        = Newspaper advertisement.
| director       = Burton L. King
| producer       = Popular Plays and Players
| writer         = Olga Petrova (story)  L. Case Russell(story) Wallace C. Clifton(scenario)
| starring       = Olga Petrova
| music          =
| cinematography = Andre Barlatier -  )
| editing        =
| distributor    = Metro Pictures
| released       = December 4, 1916
| runtime        = 5 reels
| country        = USA
| language       = Silent film(English intertitles)
}}
  lost  1916 silent film drama starring Olga Petrova and released by Metro Pictures.  The last known copy was destroyed in the 1967 MGM Vault fire.

==Cast==
*Olga Petrova - Sonia Smirnov/Marie, the convent girl
*Mahlon Hamilton - Alan Hall
*Anthony Merlo - Girard
*Count Lewenhaupt - Lachaise
*Edward Brennan - Lord Braislin
*Violet B. Reed - Lady Constance Braislin
*John Hopkins - Don Luis Maredo
*Morgan Jones - Peter, father of Sonia
*Norman Kaiser - Vladimir
*Roy Pilcher - Gaston Duval
*Evelyn Dumo - Ciel, Sonias maid

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 