Sailor of the King
{{Infobox Film
| name           = Single-Handed
| image          = Sailor of the king.jpg
| image_size     =
| caption        = US release poster with overseas market name, Sailor of the King.
| director       = Roy Boulting Frank McCarthy
| writer         = C. S. Forester (novel) Valentine Davies
| starring       = Jeffrey Hunter Michael Rennie Peter van Eyck Wendy Hiller
| music          = Clifton Parker
| cinematography = 
| editing        = 
| distributor    = 
| released       = 11 June 1953 (UK)
| runtime        = 83 minutes
| country        = United Kingdom English
| budget         = $1,220,000 
| gross          = 
}}
Single-Handed is a 1953 war film based on the novel Brown on Resolution by C. S. Forester and (despite being largely set in the Pacific) filmed in the Mediterranean Sea. Jeffrey Hunter stars as a Canadian sailor serving on a British warship who battles single-handedly to delay a German World War II warship long enough for the Royal Navy to bring it to battle. The film was released in the United States as Sailor of the King. 

==Plot==
During the First World War, Lieutenant Richard Saville, a young British naval officer on five days leave, and Miss Lucinda Bentley, a merchants daughter from Portsmouth, get talking on the train up to London.  Halfway along their journey, they miss their rail connection and spend a romantic holiday in the countryside of southern England.  When Saville proposes to her, she accepts, but on the day they are due to go back to Portsmouth, she changes her mind, asking Saville to realise that neither he nor she could bear being parted for the long periods he would be at sea.  They part, seemingly forever.
 raider Essen, but HMS Stratford, the flagship of Savilles squadron is too low on fuel for pursuit and the convoy cannot be left unguarded.  Saville decides to remain with the convoy while his other two ships - HMS Amesbury and HMS Cambridge - chase after the raider.  Cambridge then has to stop to pick up survivors from the merchantman, leaving the Amesbury on her own.  Amesbury finds and attacks the Essen, scoring a major torpedo hit on the Essen’s bow, but is sunk with the loss of all but two hands, Petty Officer Wheatley and Signalman Andrew Canada Brown.  Brown is the son of a mother keen on the navy and thus knows more about naval tactics, strategy and gunnery than most of his rank.
 AA guns big guns in vain attempts to dislodge Brown.  Finally he sends a party of marines out to hunt Brown down, but just as they are about to kill him, they are recalled and the Essen departs.  Brown collapses, seriously wounded.

As the Essen leaves the lagoon, she is caught and sunk by Savilles force.  One of her survivors informs the British of Browns exploits, which delayed repairs for 18 hours, thus enabling the British to catch up with them. A landing party is sent ashore from Savilles force to find Brown.

===Alternate endings===
The film is unusual for its period in that, for the American version, two different endings were filmed, one in which Brown survives and another in which he is killed.  These were both shown in cinemas and audiences were asked to choose their favourite one.  Both endings are also shown when the film is broadcast on British television (e.g. FilmFour).  
====First ending====
The landing party searches the island for Brown, as the camera pans to his apparently dead body.  The action then cuts to London and an honours investiture, where Saville receives a knighthood for his actions.  Brown was found dead by the landing party and is awarded a Victoria Cross posthumously, presented to his mother. She is revealed to be the former Lucinda Bentley, who had moved to Canada after her tryst with Saville.  She and Saville meet before she goes to accept her sons medal.

====Second ending==== national anthem plays.

==Differences from the novel==
In the novel, Brown is unknowingly Savilles and Lucinda Browns illegitimate son.  In the film, Lucinda initially has a different surname, Bentley. It is thus implied that she later married a man named Brown. Andrew states "my father died before I was born."  Thus, it is possible either that Andrew is Lucindas legitimate son by her marriage, or (though this is never explicitly stated) that he may in fact be Savilles son. In the novel Brown dies without knowing that his actions have secured the destruction of the German ship.
 Brown on Graf Spee, Admiral Hipper in order to allow their convoys to scatter and escape.

==Cast==
*Jeffrey Hunter as Signalman Andrew Canada Brown
*Michael Rennie as Lieutenant / Captain Richard Saville
*Peter van Eyck as Kapitan Ludwig von Falk
*Wendy Hiller as Lucinda Bentley
*Bernard Lee as Petty Officer Wheatley
*Victor Maddern as Signalman Willy Earnshaw John Horsley as Commander John Willis 
*Patrick Barr as Captain Tom Ashley, HMS Amesbury 
*Robin Bailey as Lieutenant John Stafford, HMS Stratford James Copeland as Chief Engineer  
*Sam Kydd as second signalman  
*Lowell Gilmore as Emissary of the King  
*The officers and men of  ,  , and  .

===Ships used=== Admiral Vians Italian battleship Littorio and two powerful heavy cruisers in one of the best cruiser actions of the war. The films battle sequences depict this light-cruiser firing her guns and torpedoes in some detail.

The raider "Essen" is portrayed by  , a fast minelaying cruiser, fitted for the film with large mock-up gun turrets over her 4" guns. The torpedo damage which forces her delay at Resolution Island is simply painted on the side of her port bow. With three enlarged raked funnels, she looks more like a First World War vessel than any of the German Second World War surface raiders, but she is sufficiently different in appearance not to be mistaken for any of the British ships. The scenes when she is holed up for repairs were filmed in the semi-circular Dwejra bay, guarded by Fungus Rock on the west coast of Gozo Island in Malta. The ships Captain, John Trevor Lean D.S.O., played the part of the Navigator in the film as he had to be on the bridge at all times.  Several of the ships company also took part as extras, dressed in German uniforms. 

The crew of   are credited in the opening titles; she herself is seen in a few distant shots in company with two Dido-class cruisers, and her triple 6" guns are depicted when the "Essen" fires her main armament at the island in an attempt to dislodge Brown.

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 