Hijack (2008 film)
{{Infobox film
| name           = Hijack
| image          = Hijack poster.jpg
| caption        = Movie poster for Hijack
| director       = Kunal Shivdasani
| producer       = Dinesh Vijan  Kunal Shivdasani
| writer         = Kunal Shivdasani  Gavendra Agarwal
| starring       = Shiney Ahuja  Esha Deol  K K Raina  Mona Ambegaonkar
| music          = Justin Yesudas, Uday Kumar Ninjoor
| cinematography = 
| editing        = 
| distributor    = Eros International
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} IC 814.

==Plot==
Vikram Madan (Shiney Ahuja) is a ground maintenance engineer at the Chandigarh Airport. His social life is limited to one friend, Rajeev, the security chief of the same airport. Vikrams daughter is traveling with her teacher for a debate contest to Amritsar from Delhi. That flight gets hijacked by a group of six terrorists working for a man named Rasheed (KK Raina) who has been captured by the Indian police. The flight is forced to land at the Chandigarh Airport. These terrorists demand the release of Rasheed from the Indian Government or they threaten to kill the hostages inside the aircraft. Now Vikram is faced with a dilemma of being the only man who can sneak inside the aircraft and try to save the life of his daughter Priya. Once he breaches into the aircraft with the help of air hostess Saira (Esha Deol), he starts plotting and planning and killing the terrorists one by one. Some innocent passengers become the victims of the terrorists and die. However somehow Vikram and Saira together saved the day.

==Cast==
* Shiney Ahuja as Vikram Madan
* Esha Deol as Saira
* Kush Sharma 
* Mona Ambegaonkar as Simone
* KK Raina as Rasheed
* Kaveri Jha as Pooja Madan
* Rasika Dugal as Neha

==Music==
The soundtrack was scored by debutante duo Justin-Uday. KK
*Dekh Joi
*Koi Na Jaane - KK, Shilpa Rao
*Theme Of Hijack - Suraj Jagan, Uday Shaan
*Dekh Dekh (Club Mix) - Sunidhi Chauhan, Joi
*Yaad Mein Aksar (Remix) - KK, Joi
*Yaad Mein Aksar (Sad) - Shaan

==External links==
*  
*  

 
 
 
 
 
 

 