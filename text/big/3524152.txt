The Daytrippers
{{Infobox Film
| name  = The Daytrippers
| image =Film poster for the 1996 film The Daytrippers.jpg
| caption = Theatrical release poster
| director = Greg Mottola
| producer = Nancy Tenenbaum Steven Soderbergh Larry Kamerman David Heyman Campbell Scott
| writer = Greg Mottola
| starring = Hope Davis Parker Posey Liev Schreiber Anne Meara Pat McNamara Stanley Tucci Campbell Scott
| music = Richard Martinez
| cinematography = John Inwood
| editing = Anne McCabe
| distributor = Columbia TriStar
| released =  
| runtime = 87 minutes
| country = Canada United States
| language = English
| budget = 
| gross = $2,084,559
}} independent drama film written and directed by Greg Mottola. It stars Hope Davis, Stanley Tucci, Parker Posey and Liev Schreiber. 

==Plot== Long Island.

==Cast==
*Stanley Tucci as Louis DAmico
*Hope Davis as Eliza Malone DAmico
*Pat McNamara as Jim Malone
*Anne Meara as Rita Malone
*Parker Posey as Jo Malone
*Liev Schreiber as Carl Petrovic
*Campbell Scott as Eddie Masler
*Marcia Gay Harden as Libby
*Douglas McGrath as Chap
*Peter Askin as Nick Woodman

==Release and reception==
The Daytrippers was released on March 5, 1997. The film opened to 52 theaters and grossed $35,988 in its opening weekend. Overall, the film grossed $2,099,677 domestically.    The movie received positive reception and the movie review site Rotten Tomatoes currently gives the film a 73% "Fresh" rating.

==References==
 

==External links==
* 
*  
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 

 