A Killer in the Family
 
{{Infobox television film
| name           = A Killer in the Family
| image          = 
| image_size     =
| caption        =
| genre          = Crime Drama Thriller
| director       = Richard T. Heffron
| producer       = Robert Aller
| writer         = Robert Aller Sue Grafton Steve Humphrey
| narrator       = 
| starring       = Robert Mitchum James Spader Lance Kerwin Eric Stoltz
| music          = Gerald Fried
| cinematography = Hanania Baer
| editing        = Michael Eliot
| studio         = Stan Margulies Productions Sunn Classic Pictures ABC
| network        = ABC
| released       = October 30, 1983
| runtime        = 90 minutes
| country        = US
| language       = English
| budget         =
| gross          = 
}} American television ABC and was released on DVD in 2010 by Warner Home Video.

== Plot ==
Gary Tison (Robert Mitchum) is serving two consecutive life sentences for murder, while his three young adolescent sons try to move on with their life. The oldest, Donny (James Spader) is the only son making something of his life. He is a pre-law student and is in a loving relationship with his high school sweetheart Carol (Catherine Mary Stewart). His younger brothers Ray (Lance Kerwin) and Ricky (Eric Stoltz) spend their days doing nothing and decide - convinced that he is innocent - to break their father out of jail. Donny, morally challenged, threatens to give them up the police. Noticing their determine, he agrees to go along, convinced that they can never break Gary out without his help. The three young men drive up to prison and Donny and Ricky use their guns to keep everyone hostage, while Ray breaks out Gary and his cellmate Randy Greenawalt (Stuart Margolin).

Once outside, the gang grabs the car and drives off in the Arizona landscape. An immediate police record is sent out, warning the citizens that the men are armed and dangerous. Donny laughs off the situation, not believing that any of the fugitives could do anyone harm. Things changes, however, when one night Gary decides to change cars to keep away the cops. He holds up a car, and forces the people inside to step out. The driver John Lyons (Arliss Howard), his wife Dannelda (Amanda Wyss), their young niece Teresa (Susan Swift) and young baby son are terrified of Gary, even though Donny tries to calm them down by promising they will not harm them. Despite promises of Gary that he will not call the cops, Gary fears that the man will betray him and decides to kill him and his family. Realizing that his sons will not allow him, he sends them away to get water for the couple, and then shoots all four of them. The sons are shocked and appalled, and realize their father may not be as innocent and loving as they thought.

The next morning, while in a gas station shop, Ray tells Donny that they could step to the police, since they were not involved with the murders. Donny, however, silences him and explains that Gary will never let them go away. They go back to their father and go into hiding with a female friend of Randy. The gas station shop owner, meanwhile, has recognized the gang from the paper, and warns the police.  They have already fled, though, when they arrive to arrest them, and leave the spot with a newly bought car, heading out to Mexico.

In the end, they are ambushed by the cops. Donny is shot to death, and the other sons are arrested and sent to death row. Gary was able to flee the crime, but his body is found eleven days later.

==Cast==
*Robert Mitchum as Gary Tison
*James Spader as Donald Donny Tison
*Lance Kerwin as Ray Tison
*Eric Stoltz as Ricky Tison
*Salome Jens as Alice Johansen
*Lynn Carlin as Dorothy Tison
*Stuart Margolin as Randy Greenawalt
*Arliss Howard as John Lyons
*Amanda Wyss as Dannelda Lyons
*Susan Swift as Teresa
*Catherine Mary Stewart as Carol
*Liam Duque as Alfan

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 