Hello, Hello, Carnival!
{{Infobox film
| name =  Hello, Hello, Carnival!
| image = Carmen Miranda e Aurora Miranda em Alô, Alô Carnaval (1936).jpg
| caption = Aurora Miranda (left) and Carmen Miranda (right) during their routine.
| director = Adhemar Gonzaga
| producer =Adhemar Gonzaga   Wallace Downey 
| writer = Ruy Costa   Alberto Ribeiro   João de Barro   Adhemar Gonzaga 
| starring =
| music = 
| cinematography = Edgar Brasil    Victor Ciacchi   Antonio Medeiros  
| editing = Afrodísio de Castro   Ruy Costa    Moacyr Fenelon   Adhemar Gonzaga    
| studio = Cinédia 
| distributor = Distribuidora de Filmes Brasileiros 
| released = 20 January 1936 
| runtime = 75 minutes
| country = Brazil Portuguese
| budget =
| gross =
| website =
}}
Hello, Hello, Carnival! (Portuguese:Alô Alô Carnaval) is a 1936 Brazilian musical comedy film directed by Adhemar Gonzaga and starring Jaime Costa, Barbosa Júnior and Pinto Filho. The film is best known for its early performance by Carmen Miranda who performed a routine with her sister Aurora Miranda

The film was produced by the Rio de Janeiro-based Cinédia, Brazils leading studio of the era which had been founded by Gonzaga. The film was in the popular tradition of chanchadas, essentially revue shows featuring stars of the musical stage. Shooting began in October 1935 and it was originally titled The Grand Casino (O Grande Cassino).  Much of the filming took place early in the morning because the stars performed on radio shows during the daytime. 
 Banana of the Land (1939). 

==Partial cast==
*  Jaime Costa as Empresário  
* Barbosa Júnior as Autor 1  
* Pinto Filho as Autor 2 
* Oscarito as No cassino  
* Lelita Rosa as Morena
* Aurora Miranda 
* Carmen Miranda
* Henrique Chaves as Crupiê  
* Paulo de Oliveira Gonçalves as Barman  
* Jorge Murad as Contador de piadas

==References==
 

==Bibliography==
* Shaw, Lisa & Dennison, Stephanie. Popular Cinema in Brazil. Manchester University Press, 2004. 

==External links==
* 

 
 
 
 
 
 
 
 

 