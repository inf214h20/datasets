You Were Meant for Me (film)
{{Infobox film
| name           = You Were Meant for Me
| image size     =
| image	         = You Were Meant for Me FilmPoster.jpeg
| caption        =
| director       = Lloyd Bacon
| producer       = Fred Kohlmar
| writer         = Valentine Davies Elick Moll
| starring       = Dan Dailey Jeanne Crain Oscar Levant Herbert Anderson Barbara Lawrence Alfred Newman  Lionel Newman
| cinematography = Victor Milner
| editing        = William H. Reynolds
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 92 min. English
| country        = United States
| box office	 = $2.00 million (US) 
}}
 1948 musical film, released by 20th Century Fox, directed by Lloyd Bacon, starring Dan Dailey and Jeanne Crain as a bandleader and his wife.   Marilyn Monroe worked on the film as an uncredited extra.   The film includes performances of You Were Meant for Me (1929 song)|"You Were Meant for Me", Ill Get By |"Ill Get By (As Long As I Have You)", and Aint Misbehavin (song) | "Aint Misbehavin".

==Plot summary==
 
Chuck Arnold (Dan Dailey) is a bandleader during the 1920s. He meets hometown girl Peggy Mayhew (Jeanne Crain), a flapper script girl, at one of the bands presentations and the next day they get married. Though she loves him, life on the road becomes increasingly difficult for her, and eventually, with the rise of the Great Depression, in 1929, she tires of it and returns to her country home. Unable to find new bookings, he soon joins her and brings with him Oscar Hoffman (Oscar Levant) his acerbic, cynical manager. The bandleader finds the pastoral life a crashing bore and so heads for the big city to find fortune. Fortunately, this time, he succeeds and happiness is the result. 

==Cast==
* Jeanne Crain as Peggy Mayhew  
* Dan Dailey as Chuck Arnold  
* Oscar Levant as Oscar Hoffman  
* Barbara Lawrence as Louise Crane  
* Selena Royle as Mrs. Cora Mayhew  
* Percy Kilbride as Mr. Andrew Mayhew  
* Herbert Anderson as Eddie
* Marilyn Monroe as Lady-in-waiting (uncredited)

==Soundtracks== Concerto in F
** Music by George Gershwin

* Happy Days Are Here Again
** Music by Milton Ager
** Lyrics by Jack Yellen

* Lilacs in the Rain
** Music by Peter De Rose
 You Were Meant for Me
** Music by Nacio Herb Brown
** Lyrics by Arthur Freed
 If I Had You Jimmy Campbell and Reginald Connelly

* Cant Sleep a Wink
** Written by Charles Henderson

* Crazy Rhythm Joseph Meyer and Roger Wolfe Kahn
** Lyrics by Irving Caesar

* Ill Get By (As Long as I Have You)|Ill Get By
** Music by Fred E. Ahlert
** Lyrics by Roy Turk

* Good Night, Sweetheart
** Written by Ray Noble, Jimmy Campbell and Reginald Connelly

* Aint Misbehavin (song)|Aint Misbehavin Harry Brooks
** Lyrics by Andy Razaf

* Aint She Sweet?
** Music by Milton Ager
** Lyrics by Jack Yellen

==See also==
* You Were Meant for Me (1929 song)|"You Were Meant for Me" (1929 song), a pop standard written by Arthur Freed and Nacio Herb Brown.

* Ill Get By |"Ill Get By (As Long As I Have You)" (1928 song), a pop standard written by Fred E. Ahlert and Roy Turk.

* Aint Misbehavin (song) | "Aint Misbehavin" (1929 song), a slide/jazz standard written by Fats Waller, Harry Brooks and Andy Razaf. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 