A Woman at Her Window
{{Infobox film
| name           = A Woman at Her Window
| image          = A Woman at Her Window.jpg
| caption        = 
| director       = Pierre Granier-Deferre
| producer       = Albina du Boisrouvray Hans Pflüger
| screenplay     = Jorge Semprún Pierre Granier-Deferre
| based on       = Hotel Acropolis by Pierre Drieu La Rochelle
| starring       = Romy Schneider Philippe Noiret Victor Lanoux Umberto Orsini
| cinematography = Aldo Tonti
| music          = Carlo Rustichelli
| editing        = Jean Ravel
| studio         = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
}}
A Woman at Her Window ( ) is a 1976 French drama film directed by Pierre Granier-Deferre, starring Romy Schneider, Philippe Noiret, Victor Lanoux and Umberto Orsini. It tells the story of a woman who helps a union leader sought by the police in Greece in the 1930s. The film is based on the 1929 novel Hotel Acropolis by Pierre Drieu La Rochelle.
 Best Actress Best Editing. 

==Cast==
* Romy Schneider as Margot (Santorini)
* Philippe Noiret as Raoul Malfosse
* Victor Lanoux as Michel Boutros
* Umberto Orsini as Rico (Santori)
* Gastone Moschin as Primoukis
* Delia Boccardo as Dora Cooper
* Martine Brochard as Avghi
* Neli Riga as Amalia Joachim Hansen as Stahlbaum
* Carl Möhner as Von Pahlen
* Vasilis Kolovos as Andréas Paul Muller as Le Directeur
* Camille Piton as Le Gardien
* Aldo Farina as LAméricain

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 