Ma and Pa Kettle at Waikiki
{{Infobox film
| name           = Ma and Pa Kettle at Waikiki
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Lee Sholem
| producer       = Leonard Goldstein
| writer         = 
| screenplay     = Jack Henley Harry Clork Elwood Ullman
| story          = Connie Lee Bennett
| based on       =  
| narrator       = 
| starring       = Marjorie Main Percy Kilbride
| music          = Joseph Gershenson
| cinematography = Clifford Stine
| editing        = Virgil Vogel
| studio         = Universal Studios
| distributor    = Universal-International
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,500,000 (U.S.) 
}}
Ma and Pa Kettle at Waikiki is a 1955 American comedy film directed by Lee Sholem. It is the seventh installment of Universal Studios|Universal-Internationals Ma and Pa Kettle franchise starring Marjorie Main and Percy Kilbride.

==Synopsis==
The Kettles help out cousin Rodney Kettle in Hawaii with his Pineapple industry. Ma and Pa get acquainted with blue-blooded Mrs. Andrews who thinks the Kettles are the "lowliest" people she has met. This is Percy Kilbrides last appearance as Pa Kettle, and his final movie as well.

==Cast==
*Marjorie Main as Ma Kettle
*Percy Kilbride as Pa Kettle
*Lori Nelson as Rosie Kettle
*Byron Palmer as Bob Baxter
*Russell Johnson as Eddie Nelson
*Hilo Hattie as Mama Lotus
*Loring Smith as Rodney Kettle
*Lowell Gilmore as Robert Coates
*Mabel Albertson as Teresa Andrews
*Fay Roope as Fulton Andrews
*Oliver Blake as Geoduck
*Teddy Hart as Crowbar
*Esther Dale as Birdie Hicks
*Claudette Thornton as Rodney Kettles Secretary
*George Arglen as Willie Kettle

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 