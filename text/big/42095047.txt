The Prince and the Pauper (1915 film)
{{infobox film
| title          = The Prince and the Pauper
| image          =
| caption        = Hugh Ford
| producer       = Famous Players Film Company Daniel Frohman
| writer         = Mark Twain(novel)  Hugh Ford (scenario)
| starring       = Marguerite Clark
| cinematography =
| editing        =
| distributor    = Famous Players-Lasky Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent film (English titles)
}} lost  Hugh Ford.  

==Plot==
In this Mark Twain story of mistaken identity, Marguerite Clark plays two different young men, the well to do Prince of Wales and a poor boy of the streets.

==Cast==
*Marguerite Clark - Prince Edward/Tom Canty
*Robert Broderick - The King
*William Barrows - Earl of Hertford
*William Sorelle - Miles Hendon
*William Frederic - Tom Cantys Father
*Alfred Fisher - Father Andrew
*Nathaniel Sack -
*Edwin Mordant -

==See also==
* The House That Shadows Built (1931 promotional film by Paramount); a possibility that the unnamed Marguerite Clark clip is from The Prince and the Pauper.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 