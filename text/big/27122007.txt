Fang and Claw
 
{{Infobox Film
| name           = Fang and Claw
| image          = fangclaw6.jpg
| image_size     = 190px
| caption        = Theatrical poster Frank Buck Amedee J. Van Beuren
| writer         = Frank Buck, Ferrin Fraser (uncredited)
| narrator       = Frank Buck
| starring       = Frank Buck
| music          = Winston Sharples
| cinematography = Harry E. Squire, Nicholas Cavaliere    
| editing        = Horace Woodard, Stacy Woodard
| distributor    = RKO
| released       = 1935
| runtime        = 68 or 73-74 mins. 
| country        = United States
| language       = English
| budget         =  
| gross          = 
}} Frank Buck. Buck continues his demonstration of the ingenious methods by which he traps wild birds, mammals and reptiles in Johore. {{cite book
  | last = Lehrer
  | first = Steven
  | title = Bring Em Back Alive: The Best of Frank Buck
  | publisher = Texas Tech University press
  | year = 2006
  | pages = xi
  | url = http://books.google.com/books?id=UNnhbq9gwTUC
  | isbn = 0-89672-582-0}} 

==Scenes==
Among the scenes in the film: 
* Buck shoots a tiger attacking a young rhino and captures the rhino.
* Buck captures a bird of paradise
*Buck captures a    python by shooting off the tree limb supporting the snake
*Buck captures a large group of monkeys by luring them with tapioca.” 

==Behind the camera==
The film took nine months to make. A   python cinematographer Harry E. Squire was helping Buck to force into a box left a    wound on Squire’s right arm. 

==Reception==
“The intrepid Mr. Buck displays his ingenuity and courage…Fang and Claw will be welcomed by the youngsters." 

The film made a profit of $46,000 for RKO. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p91 

==References==
 

==External links==
* 

 
 
 
 
 
 
 