2 Champions of Shaolin
 
 
 
{{Infobox film
| name           = 2 Champions Of Shaolin
| image          = 2ChampionsOfShaolinDVD.jpg
| caption        = DVD Cover
| director       = Chang Cheh
| producer       = Mona Fong
| writer         = 
| narrator       =  Meng Lo Sheng Chiang Li Wang Feng Lu
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
2 Champions of Shaolin (少林與武當 Shàolín yǔ wǔ dāng) is a 1980 Shaw Brothers film directed by Chang Cheh. It is one of the Shaolin Temple themed martial arts films and feuds with the Wu Tang Clan starring the Venoms. Rumor mills suggest that a rift was occurring between Kuo Chui and Lu Feng over choreography credits in previous films thus they came to an agreement that Kuo Chui would sit out 2 Champions of Shaolin and Lu Feng will sit out a later film, thus giving the role usually filled by Kuo to Lo Mang.  The film was digitally enhanced by Joseph Kahn for the Chemical Brothers music video "Get Yourself High".

== Plot ==
Two young warriors from the rebel Shaolin Clan are engaged in a deadly secret mission that could bring down the empire. Tung Chien-Chen is the "Shaolin Hercules", who is sent from Shaolin to take revenge on the local Wu Tang experts. Yu Tai-Ping and Wang Li are the Wu Tang chiefs who attempt to kill Tung using Yu Tais throwing knives technique after meeting him in a restaurant. Tung barely escapes and meets up with Sun Chien and his sister who teach him a special kung fu to counter knives. He eventually meets up with Hu Wei-Chen who is another Shaolin student. Wei Sing-Hung is the son of a Ming general who was adopted by the Wu Tang, in his heart he supports Shaolin and he frequently questions why the Wu Tang support the Qing but is usually hushed quickly by his masters. His love interest is Wang Lis daughter played by Wen Hsueh-Erh.

Wang Li sends Yu to kill Tung but Tung, Sun Chien and Hu Wei-Chen end up killing him forcing the Wu Tang to take revenge. Tung plans to marry Sun Chiens sister but after getting drunk and the wedding raided by the Wu Tang, Sun and his sister are killed, Tung is kidnapped and Hu Wei-Chen is too inebriated to help defend. Wei Sing secretly meets up with Hu Wei-Chen, then frees Tung and allows him to escape but is caught by Wen Hsueh Erh when he leaves his knife behind forcing a rift between them. Tung, realizing his wife is dead is down in the dumps and seems to have lost his will to fight until his brothers bring him back to reality. Tung and the other Shaolin men visit his wifes grave and meet up with a "wandering scholar" and his three servants "who look like monkeys", they immediately befriend Tung and Hu Wei-Chen occasionally bringing them food, gifts and wanting to discuss kung fu. Lu Feng is secretly a government official for the Qing who uses the monkey sword style and his servants use the monkey poles, they are planning to ambush the Shaolin men.

Wei Sing finds out about the Wu Tang and Lu Fengs plan and sides with Tung and Hu Wei-Chen. All the men end up in a battle at a local tea house. Tung and Hu Wei-Chen kill Lu Feng and his servants (being killed themselves in the process) and Wei Sing kills Wang Li. Hsueh Erh arrives just as Wei kills her father, forcing Wei to kill himself.

==Cast==
*Lu Feng as Kow Hsueh Wen/Kow Chin Chong (Wandering Scholar)
*Lo Mang as Tung Chien-Chen
*Chiang Sheng as Hu Wei-Chen
*Chin Siu-Hou as Wei Sing-Hung Wang Li as Wu Tang boss
*Yu Tai-Ping as Wang Li De Tong (Wu Tang knife expert)
*Sun Chien as Chin Tai Lei
*Ching Li 
*Wen Hsueh Erh as Ehr Wang

== External links ==
*  

 

 
 
 
 