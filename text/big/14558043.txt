Dead Season
{{Infobox Film
| name           = Dead Season (Мёртвый сезон)
| image          = 
| image_size     = 
| caption        = 
| director       = Savva Kulish
| producer       = 
| writer         = Aleksandr Shlepyanov Vladimir Vajnshtok
| narrator       = 
| starring       = Donatas Banionis Rolan Bykov Gennadi Yukhtin
| music          = Andrei Volkonsky
| cinematography = Aleksandr Chechulin
| editing        = 
| studio         = Lenfilm
| distributor    = 
| released       = 20 December 1968
| runtime        = 
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} Soviet spy film directed by Savva Kulish based on a screenplay by Aleksandr Shlepyanov and Vladimir Vajnshtok and featuring Donatas Banionis and Rolan Bykov.

==Cast==
* Donatas Banionis as Ladeynikov (voiced by Alexander Demyanenko)
* Rolan Bykov as Savushkin
* Sergei Kurilov as Bocharov, the KGB General
* Gennadi Yukhtin as Muravyov
* Bruno Freindlich as Valery Petrovich
* Svetlana Korkoshko as Ellis
* Jüri Järvet as Professor OReilly
* Laimonas Noreika as Nicholls
* Stepan Krylov
* Ants Eskola as Smith
* Leonhard Merzin as Father Mortimer
* Einari Koppel as Drayton
* Mairi Raus as Greban
* Vladimir Erenberg as Professor Born / Hass
* Anda Zajtse as Catrine

==Trivia==
* Film features a scene of exchange of the spies on the Glienicke Bridge.

==External links==
* 

 
 
 
 
 
 
 
 