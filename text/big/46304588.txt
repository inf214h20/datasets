A Texas Steer
{{Infobox film
| name           = A Texas Steer
| image          =
| caption        = Richard Wallace
| producer       = Sam E. Rork
| writer         =
| starring       =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Richard Wallace. It was a cinematic adaptation from an eponymous play by Charles Hale Hoyt. Mordaunt Hall,  , The New York Times, January 2, 1928 

==Summary==
Maverick Brander, a newly elected Congressman from the fictional town of Red Dog, Texas, moves to Washington, D.C. to serve in the United States House of Representatives.  He supports the Eagle Rock Dam bill.  Meanwhile, he flirts with a woman. 

==Cast==
*Will Rogers
*Louise Fazenda Sam Hardy
*Ann Rork
*Douglas Fairbanks, Jr.
*Lilyan Tashman
*George F. Marion
*Bud Jamison
*Arthur Hoyt
*Mack Swain
*William Orlamond
*Lucien Littlefield

==Critical reception==
The film was reviewed in The New York Times by film critic Mordaunt Hall in 1928.  He noted, "There are passages in this film that are rowdy, but there are also a good many witty episodes." 

==References==
 

==External link==
*  
* 

 
 
 
 
 
 
 

 