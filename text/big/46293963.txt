Maman (2012 film)
{{Infobox film
| name           = Maman
| image          = 
| caption        = 
| director       = Alexandra Leclère
| producer       = Cyril Colbeau-Justin Jean-Baptiste Dupont Sylvie Pialat Jeremy Burdek Nadia Khamlichi Adrian Politowski Gilles Waterkeyn
| writer         = Alexandra Leclère
| based on       = 
| starring       = Josiane Balasko Mathilde Seigner Marina Foïs
| music          = Grégoire Hetzel
| cinematography = Laurent Brunet	
| editing        =  Wild Bunch
| studio         = Les Films du Worso France 2 Cinéma
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French
| budget         = $7,9 million
| gross          = $3,673,240 
}}
Maman is a 2012 French drama directed by Alexandra Leclère. 

==Plot==
Alice and Sandrine are sisters. One is married to Serge, realtor, and offers piano lessons to occupy his time. The second has two son, Thomas and Nicolas, works in an advertising agency and occasionally sleeping with the director, Erwan. These two women with established routine will suddenly be confronted with a violent imponderable: their mother arrived from Lyon, with the intention of moving to Paris after a stormy divorce. This same mother who had leaked 20 years ago, this mother not love that had never occupied them and had never tried to contact them. The reunion, tumultuous, pushing Alice and Sandrine to strike a blow: they are absorbed sleeping pills to their mother they kidnap and sequester the house of Erwan, Brittany. With a very clear objective: to settle their accounts, and require that mother unworthy and odious to love them.

==Cast==
 
* Josiane Balasko as Paulette
* Mathilde Seigner as Sandrine
* Marina Foïs as Alice
* Michel Vuillermoz as Erwan de Kerdoec
* Serge Hazanavicius as Serge
* Thomas Gérard as Thomas
* Mathieu Rousseau as Nicolas
* Kojiro Okada as The pianist
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 