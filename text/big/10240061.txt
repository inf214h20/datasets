Lil' Pimp
{{Infobox Film
 | name = Lil Pimp
 | image = Lil_Pimp_DVD_Cover.jpg
 | caption =  Mark Brooks   Peter Gilstrap
 | producer = Amy Pell
 | writer = Mark Brooks  Peter Gilstrap
 | starring = Mark Brooks Lil Kim
 | distributor = Lionsgate Home Entertainment
 | released = January 11, 2005 
 | runtime = 80 minutes
 | country = United States |
 | language = English
 | rated = R
}}
 animated film Mark Brooks and Peter Gilstrap, based upon an episodic web animation by the same name.  The film was released straight to DVD on January 11, 2005 and starred Lil Kim and Bernie Mac as voice artists.   

==Plot== foul mouthed gerbil and faces constant rejection by his peers. He accidentally meets a prostitute under the name of Sweet Chiffon, who takes him to her working place, a bar named "the Playground", where he befriends the pimp "Fruit Juice", who gives him a small amount of "pimp glitter". He decides he wants to become a pimp. 

The following day at school, during show and tell he is scorned by his classmates for not having a living male relative and decides to use the pimp glitter to summon Fruit Juice, who consequently impresses the whole class. When he visits the Playground again, Fruit Juice alters the boys style and dresses him as a pimp, too. Meanwhile, mayor Tony Gold threatens to close Fruit Juices bar, unless he is given 90% of the profits. After this incident the boys mother goes in search of him, first directed to a gay bar and informed by Sweet Chiffon of a "nasty midget" closely resembling her son and then to the Playground. The boy refuses to return home to his mother, of which mayor Tony is informed directly and takes advantage, accusing Fruit Juice of keeping the boy against his will.  He is promptly arrested and his bar is closed down. Afterwards, mayor Tony Gold kidnaps Fruit Juices prostitutes, in order to exploit them, while assigning two policemen to plant a bomb in the closed Playground. 

Meanwhile, Fruit Juice believes that the boy betrayed him, but upon being visited and helped to escape by the boy, he changes his attitude towards him. After the narrow escape, the boys friends meet secretly in his room in order to concoct a plan to foil the Mayors scheme. His mother discovers them and agrees to disguise herself as a prostitute in order to lure the two policemen into giving her the keys to the Town Hall. The boy and his friends enter the Town Hall secretly and unveil mayor Tonys wide range of crimes, while the boy sets the prostitutes free. Then, after the gang moves the explosives, mayor Tony, unaware of the situation, presses the button on the remote controlling the bomb, devouring the Town Hall. 

In the end, Fruit Juice turns his bar into a theme park also named "the Playground" but less sexually explicit. Mayor Tony and the two policemen are then shown to be working at the park as costumed mascots.

==Cast== Mark Brooks as Lil Pimp
* Bernie Mac as Fruit Juice
* Lil Kim as Sweet Chiffon
* Ludacris as Weathers
* William Shatner as Tony Gold  
* Danny Bonaduce as Nasty Midget
* Kevin Michael Richardson as Smokey
* Jill Talley as Mom/Old lady/Mary
* David Spade as Principal Nixon
* Peter Gilstrap as Skinny Peeps/Kevin/Bonny Big Boy as Nag Champa
* John C. McGinley as Man Cub Master
* Mystikal as Geoffrey
* Jack Shih as Cabbie
* Jennifer Tilly as Miss De La Croix
* Carmen Electra as Honeysack
* Tom Kenny as Hans Dribbler Announcer/Billy/Clancey/Adam 12 Cop

==Reception==
In their negative review for the DVD, DVD Verdict commented that several people had walked out of the 2003 test screenings held by Sony (who had initially held the rights to the film) and that they (the reviewer) recommended that people stay away from the 2005 DVD release.  In contrast, the Metro Times gave a positive review for the DVD, stating "In a perfect world, this would’ve came out with an accompanying sound track and DVD extras like the 48 “Webisodic” episodes, but as a stand-alone item, Lil Pimp works its odd little corner of the world nicely." 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 