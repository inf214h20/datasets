Detroit 9000
{{Infobox film
| name           = Detroit 9000
| image          = Detroit9000.jpg
| image_size     =
| caption        = Film poster
| director       = Arthur Marks
| producer       = Arthur Marks Chuck Stroud
| writer         = Orville H. Hampton
| narrator       =
| starring       = Hari Rhodes Alex Rocco Rudy Challenger Scatman Crothers
| music          = Luchi de Jesus
| cinematography = Harry May
| editing        =
| distributor    = General Film Corporation (original release) Rolling Thunder Pictures (1998 re-release)
| released       =  
| runtime        = 106 minutes
| country        = United States English
| budget         =
| gross          = $3,179 (1998 re-release)   $1,200,000 (US/ Canada rentals) 
}}
 1973 United American cult film directed by Arthur Marks from a screenplay by Orville H. Hampton. Originally marketed as a blaxploitation film, it had a resurgence on video 25 years later.

==Plot==
Street-smart white detective Danny Bassett (  

==Filming==
Actor Alex Rocco was cast as a result of director Arthur Marks positive experience working with him on their 1973 film Bonnies Kids. {{cite AV media
 | people = Elijah Drenner (director)
 | title = Arthurs Kids: A Conversation with Arthur Marks 
 | medium = short documentary
 | publisher =
 | location =
 | date = June 29, 2010
 | url = http://vimeo.com/27964463}} 
 Fort Street Terminal train station.  Fort Street Station was already closed when filming was taking place and the approach tracks to the station were used for a chase scene.  The now restored Book Cadillac Hotel was used in the reception scenes, including the hotels famed crystal ballroom.  Although the hotel closed in 1983 and sat dormant for over 20 years, it was restored and reopened by the Westin Hotel group in 2009. Although Detroit suffered from race rioting in July 1967, and the riots are referred to in the movie, the film avoided showing areas that still showed signs of heavy damage from the rioting.
 Elmwood Cemetery. Sacred Heart Seminary stands in for the "Longview Sanitarium," where Bassett goes to visit his institutionalized wife. The hospital is Detroit Memorial Hospital on St. Antoine St. (The building was torn down in 1987.) Detroit Police headquarters at 1300 Beaubien Street is also shown.

A number of local Detroit celebrities appeared in the film, such as disc jockey Dick Purtan, who plays the police detective who converses with Alex Roccos character just prior to his boarding a police helicopter. Then-Detroit Police Chief John Nichols played himself in the TV station scene, and Detroit radio personality Martha Jean "The Queen" Steinberg played the host.

==Re-Release==
 Jackie Brown.    
 
The re-release met with generally favorable reviews. The New York Times critic Lawrence Van Gelder claimed "In general release, Detroit 9000 illustrates the wisdom of the adage "better late than never," and praised the films complex racial politics,  while the The A.V. Club|A.V. Clubs Nathan Rabin opined that, while the film was flawed, it was also an "interesting, thoroughly watchable film, and considering its genre and origins, thats something of an achievement."    Reviewing the films 2013 re-issue by Lionsgate Films as part of a Rolling Thunder Picture triple-pack (with The Mighty Peking Man and Switchblade Sisters), DVD Talks Ian Jane called it a "top notch cops and robbers urban crime thriller" which is "Not content to just titillate the audience with the more exploitative elements inherent in the genre...   film addresses head on the issues of racial tension, marital infidelity, and the difficulties of trying to make ends meet while still playing the part of an honest cop." {{cite web |url=http://www.dvdtalk.com/reviews/60450/quentin-tarantinos-rolling-thunder-pictures/ | title=Quentin Tarantinos Rolling Thunder Pictures Triple Feature DVD
 |last1=Jane |first1=Ian |date=16 April 2013 |website=DVD Talk |accessdate=15 May 2014}} 

==Cast==
*Hari Rhodes as Sergeant Jesse Williams
*Alex Rocco as Lieutenant Danny Bassett
*Vonetta McGee as Ruby Harris
*Ella Edwards as Helen Durbin
*Scatman Crothers as Reverend Markham
*Herb Jefferson, Jr. as Ferdy Robert Phillips as Captain Chalmers
*Rudy Challenger as Aubrey Hale Clayton
*Council Cargle as Drew Sheppard
*James Gates as Detective in the background of one of the police station scenes

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 