Only When I Larf (film)
{{Infobox film name           = Only When I Larf image          = "Only_When_I_Larf"_(1968).jpg caption        = director       = Basil Dearden  producer  Brian Duffy screenplay     = John Salmon based on       =   starring       = Richard Attenborough David Hemmings Alexandra Stewart Nicholas Pennell music          = Ron Grainer   cinematography = Anthony B. Richmond editing        = Fergus McDonell distributor  Paramount
|released       = 1968 country        = United Kingdom  runtime        = 104 min language       = English 
}} British comedy film directed by Basil Dearden and starring Richard Attenborough, David Hemmings and Alexandra Stewart.  It is adapted from the novel Only When I Larf by Len Deighton, and features Attenborough as an ex-brigadier con man in a variety of guises.   

==Plot==
A trio of confidence tricksters (Attenborough, Hemmings and Stewart), attempt to swindle an African state into buying crates full of scrap metal instead of anti-tank guns. 

==Cast==
* Richard Attenborough as Silas Lowther
* David Hemmings as Bob
* Alexandra Stewart as Liz Mason
* Nicholas Pennell as Spencer
* Melissa Stribling as Diana
* Terence Alexander as Gee Gee Gray
* Edric Connor as Awana
* Clifton Jones as General Sakut
* Calvin Lockhart as Ali Lin
* Brian Grellis as Spider David Healy as Jones
* Alan Gifford as Poster

==Reviews==
It received mixed reviews,   including "a plodding adaptation";  and praise for its "sound, unfussy direction and witty, observed thesping." 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 


 
 