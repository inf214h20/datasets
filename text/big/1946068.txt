Anytown, USA (film)
{{Infobox film
| name           = Anytown, USA
| image size     = 
| image	=	Anytown, USA FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Kristian Fraga
| producer       = Michael Bridenstine Juan Dominguez John L. Sikes
| writer         = 
| narrator       = 
| starring       = Steve Lonegan Fred Pesce Dave Musikant Doug Friedline Bill Palatucci Robert Miller Jonathan Wolff Robert Greene Sandy Patch
| studio         = XMark Monkey 4th Row Films Sirk Productions
| distributor    = Film Movement
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Republican Steve Democrat Fred Pesce and independent Dave Musikant.

The film is described as a candid documentary on a small town that "serves as a perfect backdrop which provides a compromising look at our nations political climate and proves that all politics is truly local". 

It was screened at the Minneapolis-St. Paul International Film Festival on April 9, 2005, where it won the award for Best Documentary.

==Plot==
In August 2003, cuts to funding by the Bogota Borough Council threaten the Bogota High School football team, enraging many residents. With a mayoral election ahead, the stage is set for a close election between the incumbent, Republican Steve Lonegan, and former Borough Council Democrat Fred Pesce. Then, in late September, former high school athlete Dave Musikant announces he will run for mayor as a write-in candidate. The three-way race garners national attention, particularly because both Lonegan and Musikant are legally blind. As election day draws near, Musikant scores a coup by hiring Doug Friedline, Jesse Venturas former campaign manager. In the week before election day, a rumor develops that Pesce is ill and will drop out of the race. However, Pesce continues to the end.

As election day dawns, poor weather worries Lonegan, who fears low turnout may cause him to lose. However, Lonegan wins with 1,097 votes, while Pesce has 728, and Musikant has 200. The Republicans also win the City Council. That January, Lonegan is inaugurated as Mayor for his third term. The football team goes on to the state championship, but loses in that game. Pesce also announces his retirement from politics. In September 2004, Musikant succumbs to his eight-year battle with brain cancer. Lonegan goes on to run for the Republican nomination for governor that year, after Jim McGreeveys resignation, but loses the primary.

==Results==
{{Election box begin no change title = Bogota, New Jersey 2003 mayoral election
}}
{{Election box winning candidate with party link no change party = Republican Party (US) candidate = Steve Lonegan (incumbent) votes = 1097 percentage = 54.2
}}
{{Election box candidate with party link no change party = Democratic Party (US) candidate = Fred Pesce votes = 728 percentage = 36.0
}}
{{Election box candidate no change party = Write-in candidate = Dave Musikant votes = 200 percentage = 9.9
}}
{{Election box total no change votes = 2225 percentage = 100
}}
{{Election box turnout no change votes = 2025 percentage = 55.6
}}
{{Election box hold with party link without swing winner = Republican Party (US)
}}
 

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 
 