The Fighting Eagle
{{infobox film
| title          = The Fighting Eagle
| image          =
| imagesize      =
| caption        =
| director       = Donald Crisp
| producer       = Cecil B. DeMille
| writer         = Douglas Z. Doty(adaptation, screenplay) John W. Krafft(intertitles)
| based on       =  
| cinematography = Arthur C. Miller and/or J. Peverell Marley
| editing        = Barbara Hunter
| distributor    = Pathé Exchange
| released       =  
| runtime        = 54 minutes
| country        = USA
| language       = Silent film (English intertitles)
}}
The Fighting Eagle is a 1927 silent film adventure and romance drama starring Rod La Rocque. It was directed by Donald Crisp and produced by Cecil B. DeMille.    The film was set during the Napoleonic Era.

A surviving film in several collections and out on DVD. 

==Cast==
*Rod La Rocque - Etienne Gerard
*Phyllis Haver - Countess de Launay Talleyrand
*Max Barwyn - Napoleon
*Julia Faye - Josephine
*Sally Rand - Fraulein Hertz
*Clarence Burton - Col. Neville
*Alphonse Ethier - Major Oliver

;unbilled
*Emile Drain - Napoleon
*Carole Lombard -

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 