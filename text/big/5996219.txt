Murder at the Vanities
{{Infobox film
| name = Murder at the Vanities
| image = Poster from 1934s Murder at the Vanities.jpg
| image_size =
| director = Mitchell Leisen Carey Wilson Jack Cunningham Rufus King (play)
| producer = E. Lloyd Sheldon (uncredited) Howard Jackson (uncredited)   William E. Lynch (uncredited) Milan Roder (uncredited)
| cinematography = Leo Tover
| editing = William Shea (uncredited)
| runtime = 89 min
| language = English
| studio = Paramount Pictures
| released =  
| country = USA
}}

Murder at the Vanities (1934 in film|1934) is a musical film based on the 1933 Broadway musical with music by Victor Young, made in the pre-Code era, and released by Paramount Pictures. It was directed by Mitchell Leisen, stars Victor McLaglen, Carl Brisson, Jack Oakie, Kitty Carlisle, Gertrude Michael, Toby Wing, and Jessie Ralph. Duke Ellington and his Orchestra are featured in the elaborate finale number.
 Broadway revue Arthur Johnston and Sam Coslow include "Cocktails for Two" sung by Brisson, "Marahuana" sung by Michael, "Where Do They Come From (and Where Do They Go)" sung by Carlisle, and "Ebony Rhapsody" by Ellington. In the film, Lucille Ball, Ann Sheridan, and Virginia Davis had small roles as chorines.

It was released on DVD (as part of a six disc set entitled "Pre-Code Hollywood Collection") on April 7, 2009. 

==Plot==
Jack Ellery (Oakie) is staging a lavish musical revue, starring Eric Lander (Brisson), Ann Ware (Carlisle),
and Rita Ross (Michael), supported by a cast of a hundred background singers/dancers (almost all women, and many scantily clad) and two full orchestras. On opening night, just before the show, somebody tries to kill Ware several times. Ellery calls in police lieutenant Murdock (McLaglen) of the homicide squad to investigate. During the show a private detective and then Rita are murdered. Ellery hides this from the rest of the performers, claiming the victims are just sick, and talks Murdock into investigating while the revue continues on, otherwise Ellery will go broke.

Several twists and turns follow, but finally the murders are solved just after the show ends. In the last scene, Nancy (Wing), a squeaky pretty blonde showgirl, finally gets to tell Ellery and Murdock what she has attempted to tell Ellery several times throughout the show, but was always being put off by him until now, as he thought she was just trying to gain his attention, and he was too busy staging the show. Actually, she had a vital piece of information that would have solved the first murder much sooner, and may have prevented the second. Now that the show is over and a success, Ellerys attention is now all on her, and theyre going out for the night to celebrate. She giggles once again and moves off stage left in front of him, and then Oakie breaks the fourth wall just momentarily, looking into the camera with a devilish grin, before he follows her.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 