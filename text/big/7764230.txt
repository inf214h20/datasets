Black Beauty (1994 film)
{{Infobox film
| name           = Black Beauty
| image          = Black Beauty, a 1994 film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Caroline Thompson Robert Shapiro
| screenplay     = Caroline Thompson
| based on       =  
| narrator       = Alan Cumming Jim Carter Peter Davison Alan Cumming Docs Keepin Time
| music          = Danny Elfman Alex Thomson
| editing        = Claire Simpson
| distributor    = Warner Bros. Family Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United Kingdom/United states
| language       = English
| budget         = 
| gross          = $4,630,377
}}
 same name directed by Caroline Thompson in her directorial debut.  The film stars Andrew Knott, Sean Bean and David Thewlis. The film is also treated as an autobiography of the horse Black Beauty as in the original novel, and is narrated by Alan Cumming as the voice of the Black Beauty.  This is the fifth feature film adaptation of the 1877 classic novel by Anna Sewell. 

==Plot==
Black Beauty (voiced by Alan Cumming; played by Docs Keepin Time) narrates his story. He is born on a farm in the English countryside and remains by his mothers side until he is sent to Birtwick Park to serve Squire Gordon and his family.

Lady Gordon, the squires ill wife, is pleased by the beautiful horse and gives him his trademark name, Black Beauty. Beauty is smitten with the squires bitter chestnut mare, Ginger, who rebuffs his attempts to be friendly. However, Beauty also befriends Merrylegs, a pony who gives rides to the squires daughters, Jessica and Molly.

On a stormy night, Beauty is pulling a carriage holding the squire and his caretaker, John Manly, home from town, but he refuses to cross a partially flooded bridge that he senses is dangerous. When John tries to get him to move, Beauty refuses. John slips and falls into the river, but hangs onto Beautys bridle. Beauty and the squire save John, and they head home.

Young Joe Green, who works in the stable, looks after Beauty that night. His lack of knowledge about horses causes him to give Beauty cold water and neglecting to cover him with a rug, which makes Beauty ill. John, Joe, and the squire treat Beauty, and he recovers.

Lady Gordons illness gets worse, and she is taken to a doctor in a carriage pulled by Beauty and Ginger. When they stop at an inn for the night, the barn where the horses are staying catches on fire due to a dropped pipe. However, Joe rescues the horses.

Lady Gordons doctor orders her to leave England for a warmer place because her illness is so advanced. The squire and his family bid goodbye to John, Joe, and the horses. Merrylegs is given to the vicar, who promises to never sell the pony.

Beauty and Ginger are taken to Earlshall Park, home of the Lord and Lady of Wexmire, and Joe bids a tearful goodbye to Beauty. Beauty and Ginger are paired up to pull Lady Wexmires carriage, but she demands that the horses wear uncomfortable bearing reins to raise their heads high, which angers Ginger. One day, Ginger breaks away from the carriage in rage.

Reuben Smith, the horses caretaker, rides to town with Beauty to take a carriage to be repainted. He becomes drunk at the tavern and rides Beauty roughly home during the night. Beauty throws a shoe and stumbles to the ground, throwing Reuben off and suffering disfiguring injuries to his knees. Reuben is dismissed from his job, and Beauty is later sold by Lord Wexmire.

He is bought by a man who keeps horses for rent but treats them terribly. Beauty is eventually taken to a fair, where he spots Joe, now a grown-up, but Joe doesnt notice him. Beautys whinnies catch the attention of Jerry Barker, a taxi carriage driver from London, and he buys Beauty for 17 guineas.

Jerry introduces Beauty to his family, who name him Black Jack. Beauty likes his job as a taxi cab horse, and Jerry treats him better than his last owner did. One day, Beauty spots Ginger, now a cab horse, but she is very weak from being abused by her owner. Beauty begs for her not to give up, but her owner leads her away. Some time later, Beauty sees her dead body on a wagon.

One snowy night, Jerry has a dreadful cough that worsens as he waits outdoors for hours for his passengers to leave a party. His condition then  worsens, and a doctor advises him to quit his job and move to the countryside. Beauty is sold to a grain dealer and pulls heavy loads of flour for two years until he collapses from exhaustion.

He is taken to a fair to be sold, but he is so weak that no one wants to buy him. Then Farmer Thoroughgood and his grandson spot Beauty, and a young man sees him, too. Beauty realizes that the young man is Joe, and he whinnies for his friend. Joe recognizes him, and the two are reunited.

Beauty lives the remainder of his life at Thoroughgoods farm with Joe, who promises that he will never sell Beauty.

==Cast==
* Docs Keepin Time as Black Beauty (horse)
* Alan Cumming as Black Beauty (voice)
* Andrew Knott (Ian Kelsey, older) as Joe Green
* Sean Bean as Farmer Grey
* David Thewlis as Jerry Barker Jim Carter as John Manly
* Peter Davison as Squire Gordon Alun Armstrong as Reuben Smith
* John McEnery as Mr. York
* Eleanor Bron as Lady Wexmire
* Peter Cook as Lord Wexmire
* Keeley Flanders as Dolly Barker Niall OBrien as Farmer Thoroughgood

==Release==

===Critical reception===
Despite commercial failure, Black Beauty received mixed to positive reviews upon its release. Review aggregator Rotten Tomatoes reports that 78% of 9 critics have given the film a positive review, with a rating average of 6.6 out of 10. 

Roger Ebert of the Chicago Sun-Times gave the film a mediocre review reacting negatively towards the horses voice over stating that "it plays like a cross between New Age mysticism and anthropomorphism run amok."  Similarly, Lisa Schwarzbaum of Entertainment Weekly found the narration to have "the effect of making a basically charming story go drippy." However, she concluded her review on a positive note, saying that "girls will inevitably love all this." 

===Box office===
The film did poorly in the box office,   grossing only $4,630,377 domestically. 

==Soundtrack==
{{Infobox album 
| Name        = Black Beauty
| Type        = Film score
| Artist      = Danny Elfman
| Cover       = 
| Released    = July 19, 1994
| Genre       = Soundtrack
| Length      =  Giant
| The Nightmare Before Christmas (1993)
| This album  = Black Beauty (1994) The Frighteners (1996)
}} Giant Records label.

;Track Listings
#Main Titles
#Baby Beauty
#Gang on the Run
#Mommy
#Jump for Joy
#Kicking up a Storm
#The Dance/ Bye Merrylegs
#Sick
#Hes Back (Revival)
#Frolic
#Ginger Snaps
#Goodbye Joe
#Wild Ride/ Dream
#Is it Joe?
#In the Country
#Poor Ginger
#Bye Jerry/ Hard times
#Memories 
#End Credits

==See also== Black Beauty (1921) Black Beauty (1946) Black Beauty (1971) Black Beauty (1978)

==References==
 

==External links==
 
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 