Loft (2005 film)
{{Infobox film
| name           = Loft
| image          = Loft_(film).gif 
| director       = Kiyoshi Kurosawa
| producer       = 
| writer         = Kiyoshi Kurosawa
| starring       = Miki Nakatani Etsushi Toyokawa
| cinematography = Akiko Ashizawa
| editing        = Masahiro Onaga
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Japan
| language       = Japanese
}}
Loft is a 2005 Japanese horror film directed by Kiyoshi Kurosawa, starring Miki Nakatani and Etsushi Toyokawa.

==Plot==
Reiko Haruna, a prize-winning writer, moves to a quiet suburban house to finish up her new novel. One night she sees a man in a storage room transporting an object wrapped in cloth. She finds out that he is Makoto Yoshioka, an archaeologist researching ancient mummies, and that object was a recently discovered mummy. Working late on her book, she sees a ghost and finds out that her room once belonged to a woman who disappeared.

==Cast==
* Miki Nakatani - Reiko Haruna
* Etsushi Toyokawa - Makoto Yoshioka Hidetoshi Nishijima - Koichi Kijima
* Yumi Adachi - Aya
* Sawa Suzuki - Megumi Nonomura
* Haruhiko Kato - Murakami
* Ren Osugi - Hino

==Reception==
Sky Hirschkron of Stylus Magazine gave Loft a "C" grade. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 
 