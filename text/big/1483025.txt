Gunfight at the O.K. Corral (film)
 
{{Infobox film
| name           = Gunfight at the O.K. Corral
| image          = Gunfight at the O.K. Corral film poster.jpeg
| caption        = Gunfight at the O.K. Corral/Last Train from Gun Hill film poster
| director       = John Sturges
| producer       = Hal B. Wallis
| writer         = Leon Uris from a story by George Scullin John Ireland
| music          = Dimitri Tiomkin
| cinematography = Charles B. Lang Jr.
| editing        = Warren Low
| distributor    = Paramount Pictures
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $2 million Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p151-153 
| gross = $10.7 million 
}}
Gunfight at the O.K. Corral is a 1957 American film starring Burt Lancaster as Wyatt Earp and Kirk Douglas as Doc Holliday, based on a real event which took place on October 26, 1881. The picture was directed by John Sturges from a screenplay written by novelist Leon Uris. 
 The historical event itself lasted only about 30 seconds, and was fought at close range with only a few firearms.

==Plot== John Ireland) into custody, but instead finds out that the local sheriff, Cotton Wilson (Frank Faylen), released them despite the outstanding warrants for their arrest. Holliday refuses to help the lawman, holding a grudge against Wyatts brother, Morgan. Holliday kills Bailey with a knife-throw when Bailey attempts to shoot him in the back. Holliday is arrested for murder, though Wyatt and Kate allow him to escape from a lynch mob.

In Dodge City, Kansas, Wyatt finds out that Holliday and Kate are in town.  Holliday tells him he has no money, so Wyatt allows him to stay if he promises to not fight while he is in town.  Meanwhile, gorgeous gambler Laura Denbow (Rhonda Fleming) is arrested for playing cards since women are not allowed to gamble.  She is released and allowed to play in the side rooms of the saloon. Wyatt is forced to deputize Holliday because a bank robber kills a cashier and Wyatts other deputies are out in a posse catching another outlaw. The bank robbers attempt to ambush Wyatt outside of town, but are instead killed by Wyatt and Holliday.

Back in Dodge City, Holliday learns Kate has left him for Ringo, who taunts Holliday to a shootout and throws liquor on him. Holliday steadfastly refuses to fight him.  Shanghai Pierce (Ted de Corsia) and his henchmen ride into town, wound deputy Charlie Bassett (Earl Holliman) and attack a dancehall, but Wyatt and Holliday hold the men and defuse the situation.  As Ringo attempts to intervene, Holliday shoots him in the arm.  Holliday returns to his room and Kate is waiting for him, but he refuses to take her back. By now, Wyatt and Laura have fallen in love, but when he receives a letter from his brother, Virgil, asking him to come clean up Tombstone, Arizona, she refuses to go with him and Holliday.

In Tombstone, Wyatt finds out that Ike Clanton is trying to herd thousands of heads of Mexican cattle but cannot as long as the Earps control Tombstones railway station. Morgan Earp (DeForest Kelley) criticizes his brothers association with Holliday, but Wyatt insists the gunslinger is welcome in Tombstone as long as he stays out of trouble. Cotton, the cowardly county sheriff from Fort Griffin, offers Wyatt a $20,000 bribe ($468,655 in 2013 dollars)  if he allows the stolen cattle to be shipped, but Wyatt refuses. He rides out to the Clanton ranch, returning young Billy Clanton (Dennis Hopper) to his mother after finding Billy drunk. Wyatt informs Ike that he has been made a U.S. Marshal and has legal authority in every county in the United States.  Finding no recourse, the Clantons decide to ambush Wyatt, but kill kid brother James Earp (Martin Milner) by mistake. 

The next morning, Ike and five of his henchman go to Tombstone to face off against the Earps at the O.K. Corral.  Holliday, who is sick from tuberculosis, joins them.  Though Virgil and Morgan are wounded in the gunfight, all six in Clantons gang are killed, including Billy, who was given a chance to surrender but refuses. After the fight is over, Wyatt joins Holliday for a final drink before heading off to California to meet Laura, as promised.

==Cast==
  
* Burt Lancaster - Marshal Wyatt Earp
* Kirk Douglas - Doc Holliday
* Rhonda Fleming - Laura Denbow Kate Fisher John Ireland - Johnny Ringo
* Lyle Bettger - Ike Clanton
* Frank Faylen - Sheriff Cotton Wilson
* Earl Holliman - Deputy Sheriff Charlie Bassett Shanghai Pierce
 
* Dennis Hopper - Billy Clanton John P. Clum George Mathews - John Shanssey
* John Hudson - Virgil Earp
* DeForest Kelley - Morgan Earp James Jimmy Earp
* Lee Van Cleef - Ed Bailey Tom McLowery
* Peter Lawman - Jack Morgan
* Brian G. Hutton as Rick
 

==Historical inaccuracies==
 
There are historical inaccuracies contained in the  film depiction of the Gunfight at O.K. Corral:
*Virgil Earp was already a deputy U.S. Marshal when he arrived in Tombstone, while Wyatt had little, if any, legal authority.  
*Wyatt came to Tombstone with a common-law wife, whom he later sent away to stay with his family—in order to get her away from opiates. 
*The real gunfight was a 30-second long, face-to-face affair with only a few firearms, not a medium-range, heavily armed shootout as in the film. 
*Johnny Ringo was not present at the OK Corral gunfight. He later killed himself.   Spicer hearing, the coroner and witnesses presented conflicting evidence about whether the Cowboys had their hands in the air or guns in their hands or were trying to draw their weapon when the fighting started. 
*Morgan and Virgil Earp were wounded and Holliday was grazed by a bullet. Wyatt was unhurt. 
*Judge Wells Spicer ruled that the lawmen acted within their authority.

==Shooting== Paramount Movie Ranch.

==Reception==
The film was a big hit and earned $4.7 million on its first run and $6 million on re-release.  Its Dimitri Tiomkin score, featuring the song "Gunfight at the O.K. Corral" with lyrics by Ned Washington, sung by Frankie Laine, pushes the movies momentum relentlessly throughout.    

Members of the Western Writers of America chose the song "Gunfight at the O.K. Corral" as one of the Top 100 Western songs of all time.   

==Sturges Sequel==
Sturges revisited the same material a decade later when he directed a more historically accurate sequel of sorts, Hour of the Gun, starring James Garner as Wyatt Earp, Jason Robards as Doc Holliday, and Robert Ryan as Ike Clanton.  That film begins with a more accurate version of the O.K. Corral gun battle then moves forward into the aftermath for the balance of the movie.

==Awards==
The film was nominated for two Academy Awards.   
* Film Editing Sound Recording (George Dutton)

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 