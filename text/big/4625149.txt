Trust the Man
{{Infobox film
| name           = Trust The Man
| image          = Trust The Man.jpg
| director       = Bart Freundlich
| producer       = Sidney Kimmel Tim Perell
| writer         = Bart Freundlich
| starring       = David Duchovny Billy Crudup Julianne Moore Maggie Gyllenhaal Eva Mendes James LeGros
| music          = Clint Mansell
| cinematography = Tim Orr John Gilroy Fox Searchlight
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
}}
Trust the Man is a 2005 romantic comedy film starring David Duchovny, Billy Crudup, Julianne Moore, and Maggie Gyllenhaal. It was directed and written by Bart Freundlich. The film primarily deals with three relationships, and a realization of just how important those relationships are. It had a limited release on August 18, 2006.  

==Plot== Lincoln Center. Her husband, Tom gave up a lucrative job in advertising to take care of their young daughter. Occasionally, Tom and Rebecca have sex; once a year, they meet with their therapist, Dr. Beekman.
 biological clock.

Tom and Rebecca each face extramarital temptations. Tom responds to a personal crisis that stems from his decision to choose kids over career by watching pornography and having an affair with a divorced mother from his sons school. Rebecca is pursued on the set by a young costar, Jasper, who would like to be able to claim that hes bedded a famous actress.

==Cast==
* David Duchovny as Tom
* Julianne Moore as Rebecca
* Billy Crudup as Tobey
* Maggie Gyllenhaal as Elaine
* Justin Bartha as Jasper
* Garry Shandling as Dr. Beekman
* Ellen Barkin as Norah
* Eva Mendes as Faith
* James LeGros as Dante

==Reception==
Based on 100 reviews collected by Rotten Tomatoes, the film has a 27% approval rating. The site reported in a consensus that "What aspires to be a sophisticated, unconventional romantic comedy turns out to be a contrivance-filled pretender to other, better films of its genre."    Another review aggregator, Metacritic, gives the film a 43/100 approval rating based on 30 critics reviews. 

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 


 