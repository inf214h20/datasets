If a Tree Falls: A Story of the Earth Liberation Front
 
 
{{Infobox film
| name           = If a Tree Falls: A Story of the Earth Liberation Front
| image          = IATF poster.jpg
| caption        = Promotional poster for If a Tree Falls
| director       = Marshall Curry (director), Sam Cullman (co-director)
| producer       = Marshall Curry, Sam Cullman
| writer         = Marshall Curry, Matthew Hamacheck
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = 
| cinematography = 
| editing        = Matthew Hamacheck, Marshall Curry 
| studio         = 
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 85 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}} Department of Daniel McGowan and his co-conspirators in a nationwide sweep in December 2005 and raises questions as to whether or not they deserved to be sentenced as "terrorists."

The film premiered at the 2011 Sundance Film Festival, where it also won an award for editing, and went on to screen within such festivals as Maryland Film Festival. On January 24, 2011, the film was announced as one of the five nominees for the Academy Award for Best Documentary Feature.

==References==
 
*http://www.ifatreefallsfilm.com/If%20A%20Tree%20Falls-Press%20Notes.pdf

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 