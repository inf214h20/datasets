Siudo Ko Sindoor
 
{{Infobox film name           = Siudo Ko Sindoor director       = Manju Kumar Shrestha producer       = Manju Kumar Shrestha writer         = Surendra Nakarmi Music composer         = Sambhujit Baskota released       = 12 October 2001 (Nepal) language  Nepali
|country        = Nepal
}}

Siudo ko Sindor ( ) is a 2001 Nepali film directed by Manju Kumar Shrestha. {{cite web
| url        = http://xnepali.net/movies/nepali-movie-siudoko-sindoor/
| title      = Nepali Movie – Siudoko Sindoor
| date       =  
| accessdate =  
| publisher  = xnepali.net
| author     = Anand Nepal
}} 

== Cast ==
* Rajesh Hamal
* Niruta Singh
* Jharana Bajracharya
* Neer Shah
* Ratan Subedi
* Basundhara Bhushal

== Music ==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Writer !!Duration
|-
|Na ta maya thaha Chha
|
| Karun Thapa
| 4:40
|-
|Kohi Neelo Jeans ma
|
| Karun Thapa
| 3:59
|-
|}

==See also==
 
*Cinema of Nepal
*List of Nepalese films

==References==
 

==External links==
*  

 
 
 
 


 
 

 