Doctor at Large (film)
{{Infobox film
| name           = Doctor at Large
| image          = Doctor at Large poster.jpg
| image_size     = 
| caption        = Original British cinema poster
| director       = Ralph Thomas
| producer       = Betty Box
| screenplay         = Nicholas Phipps 
| based on       =  
| starring       = Dirk Bogarde Muriel Pavlow Donald Sinden James Robertson Justice Bruce Montgomery
| cinematography = Ernest Steward
| editing        = Frederick Wilson
| distributor    = Rank Film Distributors (UK) Universal-International (US)
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| website        = 
}}
Doctor at Large is a 1957 British comedy film, the third of the seven films in the Doctor (film series)|Doctor series.  It stars Dirk Bogarde, Muriel Pavlow, Donald Sinden, and James Robertson Justice.

==Main cast==
 
* Dirk Bogarde as Dr. Simon Sparrow
* Muriel Pavlow as Joy Gibson
* Donald Sinden as Benskin
* James Robertson Justice as Sir Lancelot Spratt
* Shirley Eaton as Nan
* Derek Farr as Dr. Potter-Shine
* Michael Medwin as Bingham Martin Benson as Maharajah John Chandos as OMalley Edward Chapman as Wilkins
* George Coulouris as Pascoe
* Judith Furse as Mrs. Digby
* Gladys Henson as Mrs. Wilkins
* Anne Heywood as Emerald
* Ernest Jay as Charles Hopcroft
* Lionel Jeffries as Dr. Hatchet
* Mervyn Johns as Smith
* Geoffrey Keen as Second Examiner
* Dilys Laye as Mrs. Jasmine Hatchet
* Harry Locke as Porter
* Terence Longdon as George - House Surgeon
* A. E. Matthews as Duke of Skye and Lewes
* Guy Middleton as Major Porter
* Barbara Murray as Kitty
* Dandy Nichols as Lady in Outpatients Dept.
* Nicholas Phipps as Mr. Wayland - Solicitor
* Wensley Pithey as Sam - Poacher
* Maureen Pryor as Mrs. Dalton Noel Purcell as "Padre", pub landlord
* George Relph as Dr. Farquarson
* Athene Seyler as Lady Hawkins Ronnie Stevens as Waiter at hotel
* Ernest Thesiger as First Examiner
* Michael Trubshawe as Colonel Graves
 

==Plot==
Back at St Swithin’s, Dr Simon Sparrow loses out to the self-important Dr Bingham for a job as senior house surgeon. Feeling that he has no future as a surgeon, he takes a general practice job in an industrial town. He finds that he has to do most of the work, including night calls, and is also the target of his partner’s flirty wife. 

He then takes a locum job with Dr Potter-Shine’s Harley Street practice, where most of the patients are dotty aristocrats and neurotic society women. Leaving after three months, he moves to a rural practice where patients pay in kind, ranging from home-grown raspberries to poached salmon. 

Meanwhile, Tony Benskin fails his finals – again – and travels to Ireland where he buys a very dubious medical degree. This leads to a post as private physician to a rich elderly aristocratic lady in Wiltshire. 

Sparrow and Benskin take a short holiday in France, where they save Dr Hopcroft, a governor at St Swithin’s, from an embarrassing incident. In return, he arranges for Sparrow and Benskin to return to St Swithin’s. Sparrow commences advanced surgical training with Sir Lancelot Spratt, whilst Benskin becomes personal physician to a rich Maharajah.

==Reception==
The film was the second most popular movie of the year at the British box office. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 
 