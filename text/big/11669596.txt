The Lurking Fear (film)
{{Infobox Film
| name           = Lurking Fear
| image          =Poster of the movie The Lurking Fear.jpg
| image_size     = 
| caption        = US Poster Artwork
| director       = C. Courtney Joyner
| producer       = Charles Band
| writer         = C. Courtney Joyner Blake Bailey Ashley Laurence Jon Finch Jeffrey Combs Vincent Schiavelli
| cinematography = Adolfo Bartoli	 
| editing        = 	 	  Paramount Home Video Full Moon Entertainment 1994
| runtime        = 76 min.
| country        =   English
| budget         = $1,000,000 est.
}} 1994 horror film, loosely based on the H. P. Lovecraft short story The Lurking Fear.  It was produced by Charles Bands Full Moon Entertainment and written and directed by C. Courtney Joyner.

==Plot== Blake Adams) returns to his childhood home of Lefferts Corner after serving time for a crime he didnt commit.  Martense visits family friend Knaggs (Vincent Schiavelli), a mortician who has been holding half of a map for him.  The map leads to a graveyard where Martenses father hid the money from his last heist.  Arriving at an abandoned church, Martense is confronted by Cathryn (Ashley Laurence), a young woman seeking revenge for the murder of her sister, and town doctor Dr. Haggis (Jeffrey Combs).  This group is quickly joined by a trio of criminals who are looking to find the money Johns father stole from them.  What everyone is not aware of are the humanoid creatures lurking underneath the holy grounds.

==Trivia==
*Stuart Gordon was originally hoping to make this film for Bands Empire Pictures.
The film includes some in-jokes such as a car license plate on which the wording Arkham Imports appears.

==Critical reaction==

In their book  ,   films, Lurking Fear s  trailer is better than the feature it promotes. Thats unfortunate because Joyner had a wonderful professional cast who, with a cleaned-up script and a hands-on producer, could have made the film a standout among   adaptations." 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 

 