Attracta (film)
Attracta Kate Thompson and Joe McPartland. It was based on a short story by William Trevor. 

==Plot summary==
An elderly schooteacher reflects on her past.

==Cast==
* Wendy Hiller as Attracta  Kate Thompson as The young Attracta 
* Joe McPartland as Mr. Purce  John Kavanagh as Mr. Devereux 
* Kate Flynn as Aunt Emmeline 
* Cathleen Delaney as Sarah Crookham 
* Deirdre Donnelly as Geraldine Carey 
* Christopher Casson as Mr. Jameson 
* Emma McGrane as Attracta at 11 years 
* Aiden Grennell as Archdeacon Flower 
* Seamus Forde as Mr. Ayrie 
* Jane Brennan as Penelope Vade 
* Martina Stanley as Maisie
* Alan Stanford as Doctor Friendman

==References==
 

==External links==
*  
*   at Irish Film & TV Research online

 

 
 
 