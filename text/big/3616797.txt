Gladiator (2000 film)
{{Infobox film
| name           = Gladiator
| image          = Gladiator ver1.jpg
| alt            = A man standing at the center of the image is wearing armor and is holding a sword in his right hand. In the background is the top of the Colosseum with a barely visible crowd standing in it. The poster includes the films title, cast credits and release date.
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = Douglas Wick David Franzoni Branko Lustig John Logan William Nicholson
| story          = David Franzoni
| starring       = Russell Crowe Joaquin Phoenix Connie Nielsen Oliver Reed Derek Jacobi Djimon Hounsou Richard Harris
| music          = Hans Zimmer Lisa Gerrard  John Mathieson
| editing        = Pietro Scalia Red Wagon Entertainment    Universal Pictures  
| released       =  
| runtime        = 155 minutes     164 minutes    
| country        = United States   United Kingdom 
| language       = English
| budget         = $103 million      
| gross          = $457.6 million 
}} epic Historical historical drama Roman general Maximus Decimus Meridius, who is betrayed when Commodus, the ambitious son of Emperor Marcus Aurelius, murders his father and seizes the throne. Reduced to slavery, Maximus rises through the ranks of the gladiatorial arena to avenge the murders of his family and his emperor.
 Best Picture Best Actor for Crowe.

==Plot== AD 180, Germanic tribes long war on the Roman frontier and winning the favor of Emperor Marcus Aurelius. The emperor is already old and dying, and although he has a son, Commodus, he asks Maximus to succeed him as a regent and turn Rome back into a republic. The emperor speaks with Commodus afterwards and attempts to explain his decision. A weeping Commodus retorts that Marcus Aurelius never valued his son, and he kills the emperor.
 Roman province province of Mauretania. He is sold to a man named Proximo, who uses him as a gladiator.
 German named Hagen. Maximus successes allow Proximo to bring the team to the Roman Colosseum, where now-Emperor Commodus has organized 150 days of games to honor his late father. Proximo explains to Maximus that he was himself a gladiator who fought well enough in the Colosseum to win his freedom, granted to him by Marcus Aurelius himself. Maximus realizes that if he fights well enough in the Colosseum he may have a chance to personally meet the Emperor, providing a chance to kill Commodus.

Having arrived at the Colosseum, Proximos team is put in a match meant to reenact the Battle of Zama. Maximus and his teammates are on foot, armed with spears and shields, against a cohesive and well-equipped force of mounted fighters and archers in chariots. Through Maximus leadership, however, the team is able to destroy their opponents, despite having to play the role of the defeated in the reenactment. Commodus comes into the arena to personally congratulate "the Spaniard" (Maximus) on his victory. Maximus braces himself to kill Commodus, but at the last moment decides against it because of the presence of Commodus young nephew Lucius Verus. At this point, Maximus removes his helmet and reveals himself to the startled Commodus, and promises to exact vengeance against him. While a stunned Commodus longs to execute the former general on the spot, he cannot as the vast arena crowd chants "Live!" repeatedly, demonstrating their support for Maximus.

Commodus tries to have Maximus killed by paying Tigris of Gaul, a former gladiator who earned his freedom by never being defeated, to come back and fight Maximus. During the match, Colosseum staff approach Maximus from behind, holding tigers by the leash, in order to put Maximus at a disadvantage. Against all expectations, Maximus wins, but spares Tigris life despite the crowd and Commodus urging he kill his opponent. He is then declared by the crowd to be "Maximus the Merciful", further angering Commodus. Face to face, the emperor taunts Maximus with harrowing details of how his family died. Maximus responds by turning his back on Commodus and walking away.

As Maximus is being escorted back to the gladiators quarters, his former orderly Cicero approaches him and says that Maximus still has the loyalty of the Legion, encamped near Rome. Commodus sister Lucilla and the senator Gracchus secure a meeting with Maximus, and Maximus obtains their promise to help him escape Rome, rejoin his soldiers, topple Commodus by force, and hand over power to the Senate. Commodus, however, suspects a plot and forces Lucilla to confess it by threatening to kill her son Lucius. Praetorians close in upon the gladiator quarters before Maximus can leave. Proximo refuses to open the gate, enabling Maximus to escape. When the Praetorians break through, Proximos gladiators assault them in order to give Maximus more time. The Praetorians kill Hagen and execute Proximo. Maximus reaches the rendezvous but more Praetorians are waiting. Cicero is killed and Maximus is captured.

Desperate to kill Maximus and to restore his own standing, Commodus arranges a public duel between them. Before the fight begins, Commodus stabs the chained Maximus in the side, leaving him severely weakened, but also admits obliquely to killing his own father, in Quintus presence. During the fight, Maximus manages to evade Commodus blows and disarm him. Commodus asks the Praetorians to give him a sword, but Quintus orders the guards to sheathe their weapons. Commodus produces a hidden stiletto, but Maximus turns the blade back into Commodus throat, killing him.

Maximus succumbs to the stab wound, asking with his last words that reforms be made, his gladiator allies freed, and that Senator Gracchus be reinstated. As he dies, he has a vision of walking through a gate into a field of grain and of being finally reunited with his wife and son in the afterlife. At the behest of Lucilla, the gladiators, Lucius, Quintus, and Gracchus carry out Maximus body for an honorable funeral as a "soldier of Rome", leaving Commodus behind in the dirt. Some time later, Juba revisits the Colosseum at night, and he buries Maximus small figurines of his wife and son at the spot where he died. Juba promises that he will see Maximus again, "but not yet".

==Cast== Trujillo  in todays Province of Cáceres, Spain. After the murder of his family he vows vengeance. Maximus is a fictional character partly inspired by Marcus Nonius Macrinus, Narcissus (wrestler)|Narcissus, Cincinnatus, and Maximus of Hispania. Mel Gibson was first offered the role, but declined as he felt he was too old to play the character. Antonio Banderas and Hugh Jackman were also considered.  
* Joaquin Phoenix as Commodus: The corrupted, twisted and immoral son of Marcus Aurelius, he murders his father when he learns that Maximus will hold the emperors powers in trust until a new republic can be formed. 
*  uous advances for she hates him, while also having to be careful to protect her son, Lucius, from her brothers corruption and wrath. 
* Oliver Reed as Antonius Proximo: An old, gruff gladiator trainer who buys Maximus in North Africa. A former gladiator himself, he was freed by Marcus Aurelius, and gives Maximus his own armor and eventually a chance at freedom. This was Reeds final film; he died during filming. In the original script, Proximo was supposed to live.
* Derek Jacobi as Senator Gracchus: A member of the Roman Senate who opposes Commoduss rule and an ally of Lucilla and Maximus. 
*   tribesman who was taken from his home and family by slave traders. He becomes Maximuss closest ally and friend.
*  , a senator opposed to Gracchus. He helps Commodus to consolidate his power.
* John Shrapnel as Senator Gaius: Another Roman senator allied with Gracchus, Lucilla, and Maximus against Commodus. 
*  , who served under and was a friend to Maximus. Made commander of the Praetorian Guard by Commodus, after betraying Maximus. In the extended version, Quintus sees the mad side of the Emperor when he is forced to execute two innocent men. Quintus later redeems himself by refusing to allow Commodus a sword during his duel with Maximus.
*   warrior and Proximos chief gladiator who later befriends Maximus and Juba during their battles in Rome.
*  . He is also the grandson of Marcus Aurelius.
* David Hemmings as Cassius: the master of ceremonies for the gladiatorial games in the Colosseum.
*  , and Lucilla. He is used as bait for the escaping Maximus and eventually killed by the Praetorian Guard. 
* Sven-Ole Thorsen as Tigris of Gaul: An undefeated champion gladiator who is called out of retirement by Commodus to kill Maximus but is defeated by Maximus. Commodus then ordered Maximus to kill Tigris, but he spared him, which caused Commodus to hate Maximus even more.
*  . He is murdered by his son Commodus before his wish can be fulfilled.
* Omid Djalili  as a slave trader.
* Giannina Facio as Maximuss wife.
* Giorgio Cantarini as Maximuss son who is the same age as Lucillas son Lucius.

==Production==

===Screenplay=== Daniel P. Mannix’s 1958 novel Those About to Die, and he chose to base his story on Commodus after reading the Augustan History. In Franzonis first draft, dated April 4, 1998, he named his protagonist Narcissus (wrestler)|Narcissus, a wrestler who, according to the ancient sources Herodian and Cassius Dio, strangled Emperor Commodus to death.   

  by Jean-Léon Gérôme, the 19th-century painting that inspired Ridley Scott to tackle the project.]]
 Pollice Verso John Logan to rewrite the script to his liking. Logan rewrote much of the first act, and made the decision to kill off Maximuss family to increase the characters motivation.   

Russell Crowe describes being eager for the role as pitched by Walter F. Parkes, in his interview for Inside the Actors Studio: "They said, Its a 100-million-dollar film. Youre being directed by Ridley Scott. You play a Roman General. Ive always been a big fan of Ridleys."   
 William Nicholson was brought to Shepperton Studios to make Maximus a more sensitive character, reworking his friendship with Juba and developed the afterlife thread in the film, saying "he did not want to see a film about a man who wanted to kill somebody." 

The screenplay faced many rewrites and revisions. Crowe allegedly questioned every aspect of the evolving script and strode off the set when he did not get answers. According to a DreamWorks executive, "(Russell Crowe) tried to rewrite the entire script on the spot. You know the big line in the trailer, In this life or the next, I will have my vengeance? At first he absolutely refused to say it."    Nicholson, the third and final screenwriter, says Crowe told him, "Your lines are garbage but Im the greatest actor in the world, and I can make even garbage sound good." Nicholson goes on to say that "probably my lines were garbage, so he was just talking straight."   

Russell Crowe described the script situation: "I read the script and it was substantially underdone. Even the character didnt exist on the pages. And that set about a long process, thats probably the first time that Ive been in a situation where the script wasnt a complete done deal. We actually started shooting with about 32 pages and went through them in the first couple of weeks." 

Of the writing/filming process, Crowe added, "Possibly, a lot of the stuff that I have to deal with now in terms of my quote unquote volatility has to do with that experience. Here was a situation where we got to Morocco with a crew of 200 and a cast of a 100 or whatever, and I didnt have anything to learn. I actually didnt know what the scenes were gonna be. We had, I think, one American writer working on it, one English writer working on it, and of course a group of producers who were also adding their ideas, and then Ridley himself; and then, on the occasion where Ridley would say, Look, this is the structure for it -- what are you gonna say in that? So then Id be doing my own stuff, as well. And this is how things like, Strength and honor, came up. This is how things like, At my signal, unleash hell, came up. The name Maximus Decimus Meridius, it just flowed well." 

===Pre-production===
In preparation for filming, Scott spent several months developing storyboards to develop the framework of the plot.    Over six weeks, production members scouted various locations within the extent of the Roman Empire before its collapse, including Italy, France, North Africa, and England.    All of the films props, sets, and costumes were manufactured by crew members due to high costs and unavailability of the items.    One hundred suits of steel armour and 550 suits in polyurethane were made by Rod Vass and his company Armordillo. The unique sprayed-polyurethane system was developed by Armordillo and pioneered for this production. Over a three-month period, 27,500 component pieces of armor were made. 

===Filming=== remove the Morocco just south of the Atlas Mountains over a further three weeks.    To construct the arena where Maximus has his first fights, the crew used basic materials and local building techniques to manufacture the 30,000-seat mud brick arena.    Finally, the scenes of Ancient Rome were shot over a period of nineteen weeks in Fort Ricasoli, Malta.      
 Flame and Inferno software.   

===Post-production===
  The Mill bluescreen into the fight sequences, and adding smoke trails and extending the flight paths of the opening scenes salvo of flaming arrows to get around regulations on how far they could be shot during filming. They also used 2,000 live actors to create a computer-generated crowd of 35,000 virtual actors that had to look believable and react to fight scenes.    The Mill accomplished this by shooting live actors at different angles giving various performances, and then mapping them onto cards, with motion capture|motion-capture tools used to track their movements for three-dimensional compositing.  The Mill created over 90 visual effects shots, comprising approximately nine minutes of the films running time.   
 heart attack John Nelson reflected on the decision to include the additional footage: "What we did was small compared to our other tasks on the film. What Oliver did was much greater. He gave an inspiring, moving performance. All we did was help him finish it."  The film is dedicated to Reeds memory. Schwartz, p.142 

==Historical authenticity== Berber origin, instead of Sub-saharan origin.]]

===Development=== product endorsements in the arena; while this would have been historically accurate, it was not filmed for fear that audiences would think it anachronistic. 

At least one historical advisor resigned due to these changes. Another asked not to be mentioned in the credits (though it was stated in the directors commentary that he constantly asked, "where is the proof that certain things were exactly like they say?"). Historian Allen Ward of the University of Connecticut believed that historical accuracy would not have made Gladiator less interesting or exciting because "creative artists need to be granted some poetic license, but that should not be a permit for the wholesale disregard of facts in historical fiction".      

===Fictionalization===
Marcus Aurelius died at Vindobona (a Roman camp on the site of modern-day Vienna in Austria) in 180 AD; he was not murdered by his son Commodus following the final battle of the Marcomannic Wars. In reality Marcus Aurelius gave succession to his immoral son. In doing so the great philosopher emperor ended the beneficent tradition of previous Adoptive Emperors.
 Narcissus (Commoduss Consul in 154 AD, and friend of Marcus Aurelius).          Although Commodus engaged in show combat in the Colosseum he was not killed in the arena, he was strangled in his bath by the wrestler Narcissus. Commodus reigned for over twelve years unlike the shorter period portrayed in the film.      
 Caesar in preference to Commodus but was turned down. Pompeianus had no part in any of the many plots against Commodus. He was not depicted in the film. 

In the film the character Antonius Proximo claims "the wise" Marcus Aurelius banned gladiatorial games in Rome forcing him to move to the Mauretania colonies in North Africa. The real Marcus did ban the games but only in Antioch as punishment for the citys support of the rebel Avidius Cassius. No games were ever banned in Rome. However when the Emperor started conscripting gladiators into the legions, the resulting shortage in fighters allowed Lanistas such as Proximo to make "windfall" profits through increased charges for their services. 

===Anachronisms===
Costumes within the film are almost never completely historically correct. Some of the soldiers wear  .   The Germanic tribes are dressed in clothes from the stone-age period. 

The Roman cavalry are shown attacking using stirrups. This is anachronistic as the horse-mounted forces of the Roman army used a two-horned saddle. Stirrups were only employed for safety reasons because of the additional training and skill required to ride with a Roman saddle.     Catapults and ballistae would not have been used in a forest. They were reserved primarily for sieges and were rarely used in open battles. 
 Basilica of Maxentius and Constantine is quite prominent; however, it was not completed until 312 AD.

==Influences== The Fall of the Roman Empire and Spartacus (1960 film)|Spartacus,  and shares several plot points with The Fall of the Roman Empire, which tells the story of Livius, who, like Maximus in Gladiator, is Marcus Aureliuss intended successor. Livius is in love with Lucilla and seeks to marry her while Maximus, who is happily married, was formerly in love with her. Both films portray the death of Marcus Aurelius as an assassination. In Fall of the Roman Empire a group of conspirators independent of Commodus, hoping to profit from Commoduss accession, arrange for Marcus Aurelius to be poisoned; in Gladiator Commodus himself murders his father by smothering him. In the course of Fall of the Roman Empire Commodus unsuccessfully seeks to win Livius over to his vision of empire in contrast to that of his father, but continues to employ him notwithstanding; in Gladiator, when Commodus fails to secure Maximuss allegiance, he executes Maximuss wife and son and tries unsuccessfully to execute him. Livius in Fall of the Roman Empire and Maximus in Gladiator kill Commodus in single combat, Livius to save Lucilla and Maximus to avenge the murder of his wife and son, and both do it for the greater good of Rome.

Scott attributed Spartacus and Ben-Hur (1959 film)|Ben-Hur as influences on the film: "These movies were part of my cinema-going youth. But at the dawn of the new millennium, I thought this might be the ideal time to revisit what may have been the most important period of the last two thousand years&nbsp;– if not all recorded history&nbsp;– the apex and beginning of the decline of the greatest military and political power the world has ever known."   

Spartacus provides the films gladiatorial motif, as well as the character of Senator Gracchus, a fictitious senator (bearing the name of a pair of revolutionary   (John Gavin) in Spartacus.
 Nazi propaganda film Triumph of the Will (1935), although Scott has pointed out that the iconography of Nazi rallies was itself inspired by the Roman Empire. Gladiator reflects back on the film by duplicating similar events that occurred in Adolf Hitlers procession. The Nazi film opens with an aerial view of Hitler arriving in a plane, while Scott shows an aerial view of Rome, quickly followed by a shot of the large crowd of people watching Commodus pass them in a procession with his chariot. Winkler, p.114  The first thing to appear in Triumph of the Will is a Nazi eagle, which is alluded to when a statue of an eagle sits atop one of the arches (and then is shortly followed by several more decorative eagles throughout the rest of the scene) leading up to the procession of Commodus. At one point in the Nazi film, a little girl gives flowers to Hitler, while Commodus is met by several girls who all give him bundles of flowers. Winkler, p.115 

==Music==
 
{{listen
|filename=Gladiator - Battle.ogg
|title=Gladiator&nbsp;– "Battle"
|description=A clip from the score of the 2000 film Gladiator.
}}
{{listen
|filename=Now We Are Free.ogg
|title=Hans Zimmer and Lisa Gerrard&nbsp;– "Now We Are Free"
|description=listen to a clip from the score of Gladiator.
}}
The Oscar-nominated score was composed by  ", and in June 2006, the Holst Foundation sued Hans Zimmer for allegedly copying the late Holsts work.       Another close musical resemblance occurs in the scene of Commoduss triumphal entry into Rome, accompanied by music clearly evocative of two sections&nbsp;– the Prelude to  . Then, on September 5, 2005,   in January 2003 before commercial breaks and before and after half-time. Winkler, p.141  In 2003, Luciano Pavarotti released a recording of himself singing a song from the film and said he regretted turning down an offer to perform on the soundtrack.    The soundtrack is one of the best selling film scores of all time .

==Reception== normalized rating Film 2000, taking 40% of the votes.  In 2002, a Channel 4 (UK TV) poll named it as the sixth greatest film of all time.     Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Are you not entertained?". Geier, Thom; Jensen, Jeff; Jordan, Tina; Lyons, Margaret; Markovitz, Adam; Nashawaty, Chris; Pastorek, Whitney; Rice, Lynette; Rottenberg, Josh; Schwartz, Missy; Slezak, Michael; Snierson, Dan; Stack, Tim; Stroup, Kate; Tucker, Ken; Vary, Adam B.; Vozick-Levinson, Simon; Ward, Kate (December 11, 2009), "The 100 Greatest Movies, TV shows, Albums, Books, Characters, Scenes, Episodes, Songs, Dresses, Music vidos, and Trends that entertained us over the past". Entertainment Weekly. (1079/1080):74-84 

It was not without its deriders. Roger Ebert gave the film 2 out of 4 stars, and criticized the look of the film as "muddy, fuzzy, and indistinct." He also derided the writing, claiming it "employs depression as a substitute for personality, and believes that if characters are bitter and morose enough, we wont notice how dull they are."    Camille Paglia called the film "boring, badly shot and suffused with sentimental p.c. rubbish." 

The film earned US$34.83 million on its opening weekend at 2,938 U.S. theaters. Schwartz, p.141  Within two weeks, the films box office gross surpassed its US $103 million budget.  The film continued on to become one of the highest earning films of 2000 and made a worldwide box office gross of US$457,640,427, with over US$187 million in American theaters and more than the equivalent of US$269 million in non-US markets.   

===Accolades===
  BAFTA Awards, and the Golden Globe Awards. Of 119 award nominations, the film won 48 prizes.   
 Best Original Best Supporting Best Director All the Best Music, Academy rules, at the time. However, the pair did win the Golden Globe Award for Best Original Score as co-composers.
* 73rd Academy Awards    Best Picture Best Actor in a Leading Role (Russell Crowe) Best Visual Effects Best Costume Design Best Sound Mixing (Bob Beemer, Scott Millan and Ken Weston)
* BAFTA Awards
** Best Cinematography
** Best Editing
** Best Film
** Best Production Design
* 58th Golden Globe Awards
** Best Motion Picture&nbsp;– Drama
** Best Original Score&nbsp;– Motion Picture

;American Film Institute Lists
* AFIs 100 Years...100 Heroes and Villains:
** General Maximus Decimus Meridius&nbsp;– #50 Hero 
* AFIs 100 Years...100 Movie Quotes:
** "Father to a murdered son. Husband to a murdered wife. And I will have my vengeance, in this life or the next."&nbsp;– Nominated 
  
* AFIs 100 Years of Film Scores&nbsp;– Nominated 
* AFIs 100 Years...100 Cheers&nbsp;– Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition)&nbsp;– Nominated 
* AFIs 10 Top 10&nbsp;– Nominated Epic Film 

==Impact==
The films mainstream success is responsible for an increased interest in Roman and classical history in the United States. According to The New York Times, this has been dubbed the "Gladiator Effect".

 
 The Alamo, King Arthur, Kingdom of Robin Hood c "Australian Legends" postage stamp series.    Russell Crowe attended a ceremony to mark the creation of the stamps. 

==Home media== Easter eggs, cast auditions. The film was released on Blu-ray in September 2009, in a 2-disc edition containing both the theatrical and extended cuts of the film, as part of Paramounts "Sapphire Series" (Paramount bought the DreamWorks library in 2006).    Initial reviews of the Blu-ray Disc release criticized poor image quality, leading many to call for it to be remastered, as Sony did with The Fifth Element in 2007.    A remastered version was later released in 2010.

The DVD editions that have been released since the original two-disc version, include a film only single-disc edition as well as a three-disc "extended edition" DVD which was released in August 2005. The extended edition DVD features approximately fifteen minutes of additional scenes, most of which appear in the previous release as deleted scenes. The original cut, which Scott still calls his directors cut, is also select-able via seamless branching (which is not included on the UK edition). The DVD is also notable for having a new commentary track featuring director Scott and star Crowe. The film is on the first disc, the second one has a three-hour documentary into the making of the film by DVD producer Charles de Lauzirika, and the third disc contains supplements. Discs one and two of the three-disc extended edition were also repackaged and sold as a two-disc "special edition" in the EU in 2005.

==Cancelled sequel== John Logan Scott expressed easter egg contained on disc 2 of the extended edition / special edition DVD releases includes a discussion of possible scenarios for a follow-up. This includes a suggestion by Walter F. Parkes that, in order to enable Russell Crowe to return to play Maximus, who dies at the end of the original movie, a sequel could involve a "multi-generational drama about Maximus and the Aureleans and this chapter of Rome", similar in concept to The Godfather Part II.

In 2006, Scott stated he and Crowe approached  , the Vietnam War, and finally being a general in the modern-day Pentagon (building)|Pentagon. This script for a sequel, however, was rejected as being too far-fetched, and not in keeping with the spirit and theme of the original film.      

==See also==
 
* List of films set in ancient Rome
* List of historical drama films
* List of films featuring slavery

==Notes==
 

==References==
 
*  
 

== Further reading ==
 
*  
* Schwartz, Richard (2001). The Films of Ridley Scott. Westport, CT: Praeger. ISBN 0-275-96976-2
*  
* Stephens, William (2012). "Appendix: Marcus, Maximus, and Stoicism in Gladiator (2000)", in  . London: Continuum. ISBN 978-1-4411-0810-4
*  
* Winkler, Martin (2004). Gladiator Film and History. Malden, MA: Blackwell Publishing. ISBN 1-4051-1042-2
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 