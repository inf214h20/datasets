The Butcher (2006 film)
{{Infobox film
| name           = The Butcher
| image          = The Butcher poster.jpg
| image_size     =
| border         =
| alt            =
| caption        = DVD release poster
| film name      =
| director       = Edward Gorsuch
| producer       =
| writer         = Michael Hurst
| screenplay     =
| story          =
| based on       =
| narrator       =
| starring       = Myiea Coy Hazel Dean Ashley Rebecca Hawkins
| music          =
| cinematography =
| editing        =
| studio         = Lionsgate
| distributor    = Lionsgate
| released       = 2006
| runtime        = 90 minutes
| country        = United States
| language       = English
| gross          =
}}
The Butcher is a 2006 horror film directed by Edward Gorsuch. It centers on a group of teenagers who discover a murderer living in the middle of nowhere after crashing their car.

==Plot==
6 college friends embark on a road trip to Las Vegas, and during a foolish and unfamiliar shortcut, become involved in a serious car accident in a small, unknown town. One of the girls in the vehicle dies, and is accompanied by a friend while the others go looking for help in the small town. The find an old house in the woods, which is inhabited by a family of psychopaths, who proceed to chase and hunt down the group of kids.

==Cast==
*Myiea Coy as Sophie
*Hazell Dean as Sarah
*Ashley Hawkins as Atlanta
*Bill Jacobson as Franklin Mayhew
*Tiffany Kristensen as Liz
*April Lang as Mrs. Mayhew
*Tom Nagel as Adam
*Alan Ritchson as Mark

==Reception==
Mitchell Hattaway, writing for   wrote that: "Originally entitled The Harvest, Lionsgate realized that was just too generic and changed it to the almost nearly as generic The Butcher. Considering the film doesnt seem to have an original idea in its body or any intention of even trying to come up with an original idea or the ability to do anything even remotely creative with its recycled material, a more fitting title would have been The Nevada Wrong Turn Massacre." 

==References==
 

==External links==
* 

 
 
 
 


 