Annapolis (film)
 
{{Infobox film
| name = Annapolis
| image = Annapolis.jpg
| caption = Theatrical release poster
| director = Justin Lin
| producer = Damien Saccani Mark Vahradian
| writer = Dave Collard
| starring = James Franco Tyrese Gibson Jordana Brewster Roger Fan Donnie Wahlberg Vicellous Reon Shannon Brian Tyler
| cinematography = Phil Abraham
| editing = Fred Raskin
| studio = Touchstone Pictures Buena Vista Pictures
| released = January 27, 2006 
| runtime = 117 minutes
| country = United States
| language = English
| budget = $26 million
| gross = $17,496,992
}}
Annapolis is a 2006 drama film directed by Justin Lin and starring James Franco, Tyrese Gibson, Jordana Brewster, Donnie Wahlberg, Roger Fan, and Chi McBride. The film revolves around Jake Huard, a young man who dreams of one day attending the United States Naval Academy in Annapolis, Maryland. It was released January 27, 2006 in the United States.

As of February 12, 2006, the film grossed an approximate total of US Dollar|US$17.2 million in the United States, and was produced for a $26-million budget. Annapolis scored mostly negative reviews from critics but found an audience on DVD selling over four million copies and staying on top 10 rental lists around the U.S.

==Summary==
The movie begins with Jake Huard (Franco) waking up after being knocked down during a boxing match. After returning to his feet and beating his opponent, he returns to the home he shares with his distant father (Brian Goodman), who is also his employer at a naval shipyard, building vessels for the United States Navy|Navy. It is revealed that his mother died an unspecified time ago, and his father doubts his ability to be anything.
 LCDR Burton (Wahlberg), who reveals that his application to the United States Naval Academy|U.S. Naval Academy at Annapolis, Maryland has been recently accepted. At a bar where Jake and his friends are celebrating his acceptance, his friends introduce him to a young woman named Ali (Brewster), who they claim is a prostitute hired as a going-away present. Huards attempts to seduce Ali are unsuccessful, and his farewell with his father leaves him frustrated.
 Midshipman 2nd company commanding Marine prior to joining the Academy, and announces his intent to run out any midshipman he deems unfit to be an officer. Jakes roommates include Nance (Shannon) (nicknamed Twins because he is too overweight to complete the obstacle course), Loo (Fan), and Estrada (Calderon), who is being singled out by Whitaker because of his ethnicity.

As time goes on, Estrada is kicked out for lying to Whitaker and attempting to assault Loo when he informs on Estrada to Cole. Huards class becomes increasingly frustrated from suffering due to his failures, as evidenced by Loo moving out of the room. Nance explains his refusal to leave by noting that the instructors are so focused on Huard that they are leaving him alone. Returning home on winter leave, he intends not to return until he discovers that his father made a wager, expecting him to fail. Upon his return, the class begins instruction in boxing, and it is announced that a midshipman tournament, the Brigades, will occur at the end of the year. After Huard angers the boxing instructor with some unsportsmanlike conduct toward Cole in the ring, he is forced to train by himself.
 weight class. In his match with Loo, he endures some taunting and returns with a single-punch knockout, earning him Loos respect and support, along with the rest of his class when he proves capable of performing academically. He progresses through the tournament and defeats Whitaker in the semifinals, leaving Huard and Cole for the final match.
 by decision, Jakes boxing ability earns him the respect of the entire Academy, as well as his father (who has come to see the fight despite being behind schedule on the current ship he is building).
 commissioned Second Second Lieutenant Cole to ask who would have won if the fight had continued, to which Cole challenges Jake to join the Marines to find out.

==Cast==
*James Franco as Midshipman 4th Class Jake Huard
*Tyrese Gibson as Midshipman 1st Class Matthew Cole
*Macka Foley as Ref
*Jim Parrack as A.J.
*Donnie Wahlberg as Lieutenant Commander Burton
*Brian Goodman as Bill Huard
*Billy Finnigan as Kevin
*Jordana Brewster as Midshipman 2nd Class Ali
*Katie Hein as Risa
*Jimmy Yi Fu Li as Midshipman Lin Charles Napier as Supt. Carter
*Heather Henderson as Daniels
*Vicellous Reon Shannon as Twins
*Roger Fan as Loo
*McCaleb Burnett as Whitaker
*Wilmer Calderon as Estrada
*Chi McBride as McNally
*Matt Myers as Mr. Nance
*John Fahy as Midshipman
*Samuel Winder as Flaming Midshipman
*Lisa Crilley as Jen
*Zachery Ty Bryan as Johnson (uncredited)

==Soundtrack==
The score by Brian Tyler was released, but the soundtrack was not.

# "Nowhere Ride" - The Chelsea Smiles White Zombie
# "When Im Gone" - No Address Disturbed
# "Different Stars" - Trespassers William
# "Somersault" - Zero 7
# "Born Too Slow" - The Crystal Method
# "Hero of the Day" - Metallica
# "Start Something" - Lostprophets

==Reception==
The film was considered a box office bomb, as it failed to break even with production costs.  It received mostly negative reviews from critics, and holds a 10% rating on Rotten Tomatoes based on 111 reviews.

==Controversy==
Upon reviewing the script, the Chief of Naval Information commented: {{cite journal | author = Captain Sherman G. Alexander ’56, USN (Ret.) |date=August 2006 | title = Navy on the Big Screen: an Historical Perspective | journal = Shipmate archiveurl = archivedate = 2006-11-13}}  Department of Department of Defense ... the story depicted in the script did not accurately portray the Academy, its standards for training, and its methods of shaping midshipmen mentally, morally and physically for service in the U.S. Navy. Based on this, the producers were not allowed further access to the Academy grounds or provided with any other support for the filming.

:Navy personnel should avoid the appearance of support to the film as members of the Department of the Navy. Anyone attending a screening or promotional activity for the film should not attend in uniform.

Because of the lack of access to Annapolis Naval Academy, the movie was filmed in Philadelphia, Pennsylvania at Girard College at the decommissioned Philadelphia Naval Shipyard.

==References==
 

==External links==
*   
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 