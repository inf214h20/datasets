Krishna Gopalakrishna
{{Infobox film
| name = Krishna Gopalakrishna
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = Krishnan Nair
| writer = Balachandra Menon
| screenplay = Balachandra Menon Ashokan Balachandra Kalpana
| music = Balachandra Menon
| cinematography = Alagappan
| editing = Balachandra Menon
| studio = KSFDC & V&V
| distributor = KSFDC & V&V
| released =  
| country = India Malayalam
}}
 2002 Cinema Indian Malayalam Malayalam film, Kalpana in lead roles. The film had musical score by Balachandra Menon.   

==Cast==
  
*Manoj K Jayan  Ashokan 
*Balachandra Menon  Kalpana  Santhosh  Sreenivasan  Lal  Siddique 
*Geetha Nair
*Geethu Mohandas 
*Indraja 
*Master Vignesh
*Mini Nair
*Poojappura Radhakrishnan
*Valsala Menon 
 

==Soundtrack==
The music was composed by Balachandra Menon. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kallum Mannum Vaarithinnum (F) || KS Chithra || Bichu Thirumala || 
|- 
| 2 || Choodulla Kaattin || Sujatha Mohan, Balachandra Menon || Bichu Thirumala || 
|- 
| 3 || Kallum Mannum Vaarithinnum (M) || K. J. Yesudas || Bichu Thirumala || 
|- 
| 4 || Raamayoora Nadana || K. J. Yesudas || Bichu Thirumala || 
|- 
| 5 || Thathakkapithaka (Neeradum Pennungalude) || Bichu Thirumala, Chorus, Prathibha || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 


 