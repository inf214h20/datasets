Basant Bahar (film)
 Basant Bahar}}
{{Infobox film
| name = Basant Bahar
| image = 
| director = Raja Nawathe
| producer = R. Chandra
| writer = Rajinder Singh Bedi (dialogue) T. R. Subba Rao|Ta-Ra-Su (story)
| starring = Bharat Bhushan Nimmi
| music = Shankar Jaikishan
| cinematography = M. Rajaram
| editing = P. S. Khochikar G. G. Mayekar
| released = 1956 Hindi
}} Shailendra and Hasrat Jaipuri; and music composition by Shankar-Jaikishan. The story is based on the Kannada novel "Hamsageethe" by legendary novelist T. R. Subba Rao|Tarasu. "Hamasa" means swan and "Geethe" means song. It is believed that before a swan dies,it will sing without opening its mouth. That mutter of melody is believed to be unmatched since any scene of lyricism falls short of its reach.

==Plot==
The only son of the Royal Astrologer, Narsin (Om Prakash) namely Gopal (Bharat Bhushan) is not interested in astrology but in music and songs, much to the dismay of Narsin. Gopals talents are well-known and he could become the Emperors Chief Musician if he wins in the contest. His rivals live next door, and in order to win the approval of the emperor (Chandrashekhar), they poison Gopals drink, and Gopal loses his voice completely. His mom (Leela Chitnis) is sorry at the loss of his voice. A young dancing girl, Gopi (Nimmi) takes interest in Gopal, and through her help and a hard knock on the head, he recovers his voice. But Gopals challenges are far from over, and he will be called upon to prove his worth, as well as his association with Gopi.

==Cast==
* Bharat Bhushan        	 ...	Gopal
* Nimmi                  	 ...	Gopi Kumkum      	 ...	Radhika (as Kum Kum)
* Manmohan Krishna      	 ...	Lehri Baba
* Parsuram            	                (as Parashram)
* Chand Burke          	 ...	Leelabai (as Chand Burque)
* Shyam Kumar (actor)		
* S. K. Prem		
* Babu Raje		
* S. B. Nayampalli	                (as Nayam Pally)
* Indira		
* Chandrashekhar (actor)	 ...	Emperor (as Chandra Shekhar)
* Leela Chitnis	         ...	Gopals mom
* Om Prakash        	 ...	Narsin

==Awards== National Film Awards    1956 - Certificate of Merit for Best Feature Film in Hindi

==Soundtrack==
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)
|-
| 1
| Badi Der Bhai
| Mohd. Rafi
|-
| 2
| Bhay Bhajana Vandana Sun
| Manna Dey
|-
| 3
| Duniya Na Bhaye
| Mohd. Rafi
|-
| 4
| Ja Ja Re Ja Balama
| Lata Mangeshkar
|-
| 5
| Kar Gaya Re
| Lata Mangeshkar, Asha Bhosle
|-
| 6
| Ketaki Gulab Juhi
| Manna Dey, Bhimsen Joshi
|-
| 7
| Main Piya Teri
| Lata Mangeshkar
|-
| 8
| Nain Mile Chain Kahan
| Lata Mangeshkar, Manna Dey
|-
| 9
| Sur Na Saje
| Manna Dey
|}

==References==
 

==External links==
*  

 

 
 
 
 