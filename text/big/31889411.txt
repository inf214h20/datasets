Subhodayam
{{Infobox film
| name           = Subhodayam
| image          =
| image_size     =
| caption        =
| director       = K. Viswanath
| producer       = Ch. Narasimha Rao Jandhyala (dialogues)
| narrator       = Chandramohan Sulakshana Sulakshana Manorama Manorama Sakshi Ranga Rao Charu Haasan
| music          = K. V. Mahadevan Kasturi
| editing        =
| studio         =
| distributor    =
| released       = 1980
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Subhodayam is a 1980 Telugu film directed by K. Viswanath. It is remade in Hindi language as Kaamchor in 1982 starring Rakesh Roshan and Jaya Prada.

==Cast==
{| class="wikitable"
|-
! Actor / Actress !! Character
|- Chandramohan || Chandram
|- Sulakshana || Geeta
|- Manorama || Sivakamini
|-
| J. V. Ramana Murthi ||
|-
| Sakshi Ranga Rao ||
|- Annapoorna ||
|-
| Charu Haasan ||
|}

==Soundtrack==
* "Asatomaa Sadgamayaa" (Lyrics: Veturi; Singer: Singer: S. P. Balasubrahmanyam)
* "Gandhamu Pooyarugaa" (Lyrics: Veturi; Singers: S. P. Balasubrahmanyam and P. Susheela)
* "Kanchiki Potavaa Krishnamma" (Lyrics: Veturi; Singers: S. P. Balasubrahmanyam and P. Susheela)
* "Mandara Makaranda" (Lyrics: Pothana; Singer: P. Susheela)
* "Natanam Aadene" (Lyrics: Veturi; Singers: S. P. Balasubrahmanyam and P. Susheela)
* "Raayaitenemiraa Devudu Haayigauntadu Jeevudu" (Lyrics: Veturi: Singer: S. P. Balasubrahmanyam and P. Susheela)
* "Kasthoori Ranga Ranga (slokam) ( Singer : P.Susheela)

==External links==
*  

 
 
 
 
 
 
 


 