Super Sucker
{{Infobox film
| name = Super Sucker
| image = Super Sucker movieposter.JPG
| caption = Film poster
| director = Jeff Daniels
| producer = Bob Brown Jeff Daniels Tom Spiroff
| writer = Jeff Daniels Guy Sanville
| starring = Jeff Daniels Dawn Wells Harve Presnell Matt Letscher
| editing  = 
| distributor = Purple Rose Films
| released = February 24, 2002 (Jackson, MI premiere) January 24, 2003 (US release)
| runtime = 95 minutes
| country = United States
| language = English
| budget =
}}

Super Sucker is a 2003 film featuring Jeff Daniels, Harve Presnell, and Kate Peckham.

Filmed in Jackson, Michigan, Daniels and Presnell play Fred Barlow and Winslow Schnaebelt, the heads of two different groups of door-to-door vacuum cleaner salesmen who are competing for the same territory.  Their rivalry becomes so fierce that the president of the manufacturer of the product, Mr. Suckerton, decides that for the good of the company, the town will have only one group of sales representatives.  Desperate, and always the underdog, Barlow suggests a winner-take-all sales contest to determine who gets the territory.  Well behind Schnaebelt from the very start, Barlows sales surge when he learns of his wifes non-traditional use of a long forgotten vacuum attachment.  

The movie, written by Daniels, won the 2002 U.S. Comedy Arts Festival audience award for Best Feature but never obtained national distribution.

==Cast==
*Fred Barlow &ndash; Jeff Daniels
*Howard Butterworth &ndash; Matt Letscher
*Winslow Schnaebelt &ndash; Harve Presnell
*Bunny Barlow &ndash; Michelle Mountain
*Darlene &ndash; Kate Peckham
*Dawn Wells &ndash; herself
*Jill &ndash; Suzi Regan
*Rhonda &ndash; Sandra Birch
*Tamara Thompson &ndash; Sarab Kamoo

==External links==
*  
*  
*  

 
 
 
 
 
 
 

 