Special 26
 
 
{{Infobox film
| name           = Special 26
| image          = Special 26 poster.jpg
| caption        = Theatrical release poster
| director       = Neeraj Pandey
| producer       = Shital Bhatia   Kumar Mangat
| writer         = Neeraj Pandey
| starring       =  
| music          = Songs: M. M. Keeravani|M. M. Kreem   Himesh Reshammiya Background Score: Surender Sodhi
| lyricist       = Irshad Kamil Bobby Singh
| editing        = Shree Narayan Singh
| studio         = Friday Filmworks
| distributor    = Viacom 18 Motion Pictures
| based on = 1987 Opera House heist
| released       =  
| runtime        = 144 minutes   
| country        = India Hindi
| budget         =   
| gross          =   (worldwide gross) http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6308&nCat= 
}}
 thriller heist executed an Opera House branch of Tribhovandas Bhimji Zaveri in Mumbai. Special 26 released on February 5, 2013 to widespread critical acclaim and was regarded as  one of the best films of 2013
.   

==Plot==
The story opens on 18 March 1987, where a walk-in CBI interview is taking place, held by the characters of Akshay Kumar and Anupam Kher. The film then goes into flashback.
 Rajesh Sharma), and Iqbal (Kishor Kadam), meet the support officers and conduct the raid.

Following the successful raid at a ministers house, its revealed that Ajay and Sharma are fake CBI officers, along with their two accomplices. They then move to different parts of the country where theyre from and merge into their everyday lifestyles. Ajays love interest, Priya (Kajal Aggarwal), an about-to-be-married teacher, is introduced here. The crew meets again in Chandigarh at the behest of Sharma for his daughters wedding.

Ranveer with his senior officer meet the minister of the earlier raid, who reveals that he doesnt want the news to appear in the public domain because he wants to protect his image. The senior officer suspends Ranveer with his colleague Shanti (Divya Dutta) for being irresponsible.
 Neetu Singh) and child in New Delhi. He apprehends a criminal named Gupta, which gives an insight into his character. A disgraced Ranveer meets Khan, and they join hands to apprehend Ajay and his fellow criminals.
 Bara Bazar in Kolkata posing as officers from the Income Tax Department. Following the more difficult but successful raid, Khan insists that this should be reported in the newspaper, despite nobody coming forward to report it themselves, as black money is involved. Upon seeing this in the newspaper, Ajay and Sharma decide to conduct their "big job," a final raid in Mumbai.

After the second income tax raid, the story briefly focuses on Ajay and Priya, with the latters marriage being planned. She insists to go with Ajay for the final job, but he tells her to meet him at the airport at 4 pm on the day.

Meanwhile, Ranveer finds information about Sharma, and Khan orders Telephone tapping|wire-tapping his phone. When Sharma is talking to Ajay on the phone, Khan procures several details about them, including the names Joginder and Iqbal and Ajju (Ajays nickname). As a result, under Khans orders, officers track the crew to Mumbai to a hotel where theyre staying. They plan to raid and hence rob a big jewellery store.

Ajay, followed by a CBI officer, goes to a newspaper to advertise for "50 dynamic graduates" with details of an interview. Khan embeds his officers among the candidates and they are selected. Khan finds out the details of the training process, which includes a mock raid. It is stated that, on the day, the candidates will be trained and then led out for the mock raid in the afternoon. However, no details about the raid have come to light.

To find out more, Khan and Ranveer go to the hotel and force their way into Sharmas room. He divulges the information about the raid, following a threat of violence. He also mentions that Ajay is taking revenge on the CBI for not appointing him. Khan orders Sharma to ensure everything proceeds as normal.

On the day of the raid, Khan takes charge of the jewellery store, and replaces the goods with fake jewellery, with the originals being moved to a nearby workshop. He is given constant information about the activity of the crew and recruits. Ajay informs the recruits that he will come in a different vehicle to them. Sharma leaves with the recruits in a bus, but leaves the bus at police headquarters, saying that he will arrive with Ajay. He also states that nobody should leave the bus until they arrive and that he is going to verify the paperwork for the raid. In reality, he goes to meet Ranveer Singh. With Ajay, a raid of the workshop is carried out. Ajay then goes and meets Priya at the airport.

Meanwhile, at the jewellery store, Khan is informed that the raid was conducted at the workshop and all jewellery in the workshop along with the jewellery that were moved were stolen. Khan works it all out for himself, realising that Ranveer was part of the gang and he wasnt a real police officer, and they were set up right from the beginning. Khan starts laughing loudly and applauding the entire robbery. Later, Khan receives a money order for Rs. 100 that Ajay had taken from him with the message that he could not steal the honest earnings of an officer. The film ends with Khan being told over the phone that the CBI has information about the perpetrators. At the same time we see Ajay Singh and P.K. Sharma enjoying a cricket match happily, at the Sharjah Cricket Association Stadium with their wives, Joginder, and Iqbal.

==Cast==
* Akshay Kumar as Ajay "Ajju" Singh / A. K. Vardhan
* Kajal Aggarwal as Priya
* Manoj Bajpai as C.B.I. Officer Waseem Khan
* Jimmy Shergill as Sub-Inspector Ranveer Singh
* Divya Dutta as Constable Shanti
* Anupam Kher as P. K. Sharma
* Kharaj Mukherjee as CBI Officer Mukherjee Rajesh Sharma as Joginder
* Kishor Kadam as Iqbal
* Neeru Bajwa cameo in song "Gore Mukhde Pe"
* Deepraj Rana as Rahul
* Tiku Talsania as jewellery shop owner
* Sachin Nayak as Bela Boy Neetu Singh as Waseems wife 
* Abha Parmar as ministers wife

==Development== robbery of Opera House, Mumbai.

Manoj Bajpai was signed to play the CBI officer,    and Anupam Kher plays a vital role.  Kajal Aggarwal plays a specially-written role of Mon Singhs love interest. 

Ajay Devgn and Abhishek Bachchan were reported to have been considered for the lead role. It went to Akshay Kumar as he was Pandeys first choice.  Kajal Aggarwal is playing the role of a school teacher.   Akshay Kumar slashed his fee by half for Special Chabbis.  

==Filming== Jama Masjid. 

==Marketing==
  Chashme Baddoor CBI officials on 6 February 2013. 

==Release==
Special Chabbis released on for around  .     The producers have   investment on Special Chabbis, as the film has fetched around   from sale of the territory rights across India.       Special 26 was made on a budget of around  , with a publicity and advertising budget of around  .   

==Critical reception==
Special 26 has received much critical acclaim.

{{Album ratings

|rev1=Bollywood Hungama
|rev1Score=  
|rev2=CNN-IBN
|rev2Score=  
|rev3=Hindustan Times
|rev3Score=  
|rev4=NDTV
|rev4Score=  
|rev5=Rediff
|rev5Score=  
|rev6=The Times of India
|rev6Score=  
|rev7=Zee News
|rev7Score=  
|rev8=Planet Bollywood
|rev8Score=  
}}

Taran Adarsh of Bollywood Hungama gave the score of 4 out of 5, and said "Special Chabbis is an intelligently woven, slick and smart period thriller with its subject matter as its USP. Its sure to get listed as one of the most gripping heist dramas based on real life occurrences. "    

Anupama Chopra, writing for Hindustan Times, gave 3.5 stars out of 5, calling it one of the best films of the year and praising its attention to detail, its character building, the plots steady but sure sense of immediacy and urgency, and the elaborate cat-and-mouse chase between the conmen and the police. She criticises the unnecessary addition of the love angle in an otherwise gripping script, along with the unconvincing nature of its climax.   

Saibal Chatterjee of NDTV gave it 4 out of 5 stars, while and wrote, "Special 26 is an intelligently scripted, superbly acted, enthralling and believable heist film that is more than just that."   

Kaushik Ramesh of Planet Bollywood gave the film 8 out of 10 and summarised, "The unfolding of each scene brings with it certain unforeseeable surprises, course changing and effective. The adrenalizing screenplay makes you spontaneously acknowledge the genius in the director Neeraj Pandey who clearly emerges as the hero to watch out for."   

Resham Sengar of Zee News rated the film 4 out of 5, stating, "Very rarely would you come across a logically scripted and intelligently-directed drama thriller churned out from the good ol Hindi cine factory these days. And here’s a promising Neeraj Pandey for you who is back with a bang (and bang on target) with another awe inspiring film Special 26."   

Rajeev Masand of CNN-IBN gave 3.5 out of 5, saying "Special Chabbis works on account of its meaty, realistic plot and nicely fleshed out characters. This is solid, assured filmmaking, evident in the meticulous detailing of its 80s production design. The film charms you with its subtle humor, through characters played by Jimmy Shergill and Divya Dutta."   

Madhureeta Mukherjee of The Times of India gave the movie 3.5 out of 5 stars, while adding, "Special 26 doesnt stun you with a social message like Pandeys A Wednesday!, but it grips, excites and ahh...climaxes too! And no ... you cant fake this one! Catch it for pure cinematic orgasm."    

Sukanya Verma for Rediff.com gave 3.5 stars out of 5 and said, "Special Chabbis is one of the finest films of the year so far."    

Newstrackindia stated that Special 26 is a no-nonsense film from Akshay. 

==Boxoffice==

===India===
Special 26 opened at 30% occupancy on Friday morning but it improved steadily till the evening shows to 60% due to mostly positive reviews. 
The film netted   on its first day    and further grow to make   in its first weekend.    the film collected around   nett on Monday.  It netted around   in its first week.    In its second weekend it has a collection of around   to bring its ten-day figure to around   nett. 
In its second week, the film netted   taking its total business to nearly   in India.  It eventually netted around   with a distributor share of   in domestic market.     

The worldwide gross of Special 26 stands at  . 

===Overseas===
Special 26 had low opening overseas with a collection of $1.25 million its first weekend.    It further netted around $2.35 million in ten days.  Its final overseas collections was US$2.75 million. 

==Soundtrack==
{{Infobox album
| Name = Special 26
| Cover =
| Type = Soundtrack
| Artist = M. M. Kreem and Himesh Reshammiya Prasad Film Laboratories Hyderabad, India Spectral Harmony Mumbai, India
| Released =  
| Length =   Feature Film Soundtrack
| Label = T-Series
| Chronology       = M. M. Kreem
 | Last album       = Shirdi Sai (2012)
 | This album       = Special 26 (2013)
 | Next album       = Anaamika (2014 film)| Anaamika (2014)
 {{Extra chronology
 | Artist       = Himesh Reshammiya
 | Type       = Soundtrack
 | Last album      = Khiladi 786 (2012)
 | This album      = Special 26 (2013)
 | Next album      = Shortcut Romeo (2013)
 }}
}} Shabbir Ahmed. T-Series acquired the rights of the album. 
{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| total_length =  
| title1 = Tujh Sang Lagee
| extra1 = Krishnakumar Kunnath|KK, M M Kreem
| lyrics1 = Irshad Kamil
| music1 = M M Kreem
| length1 = 4:13
| title2 = Gore Mukhde Pe Zulfa Di Chaava
| extra2 = Aman Trikha, Shreya Ghoshal, Shabab Sabri
| lyrics2 = Shabbir Ahmed
| music2 = Himesh Reshammiya
| length2 = 4:06
| title3 = Kaun Mera (Female)
| extra3 = Chaitra Ambadipudi
| lyrics3 = Irshad Kamil
| music3 = M M Kreem
| length3 = 2:57
| title4 =Mujh Mein Tu
| extra4 = Keerthi Sagathia
| lyrics4 = Irshad Kamil
| music4 = M M Kreem
| length4 = 4:24
| title5 = Kaun Mera (Male)
| extra5 = Angaraag Mahanta
| lyrics5 = Irshad Kamil
| music5 = M M Kreem
| length5 = 2:54
| title6 = Dharpakad
| extra6 = Bappi Lahri
| lyrics6 = Irshad Kamil
| music6 = M M Kreem
| length6 = 2:22
| title7 = Kaun Mera
| extra7 = Sunidhi Chauhan
| lyrics7 = Irshad Kamil
| music7 = M M Kreem
| length7 = 2:32
| title8 = Mujh Mein Tu - Version 2
| extra8 = M M Kreem
| lyrics8 = Irshad Kamil
| music8 = M M Kreem
| length8 = 3:20
}}

==See also==
* 1987 Opera House heist

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 