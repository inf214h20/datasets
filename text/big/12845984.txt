The Trap (1922 film)
 
{{Infobox film
| name           = The Trap
| image          = Thetrap1920-newspaperad.jpg
| image_size     =
| caption        = Newspaper advertisement.
| director       = Robert Thornby
| producer       = Lon Chaney (story) Lucien Hubbard (story) George C. Hull (writer) Irving Thalberg (story) Lon Chaney Alan Hale
| cinematography = Virgil Miller
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
| website        =
}}
 1922 American Lon Chaney, directed by Robert Thornby, and released by Universal Pictures. The movie was released in the United Kingdom under the title Heart of a Wolf.
 Alan Hale of the same name in 1913.

== Plot summary ==
The film features an angry miner who sets out for revenge on a man who stole his mine.

== Cast == Lon Chaney as Gaspard the Good Alan Hale as Benson
*Dagmar Godowsky as Thalie
*Stanley Goethals as The Boy
*Irene Rich as The Teacher
*Spottiswoode Aitken as The Factor
*Herbert Standing as The Priest
*Frank Campeau as The Police Sergeant

== Production ==
It was filmed in Yosemite National Park in California.

==Survival status==
Several prints of the film exist and it is available on DVD. 

==References==
 

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 
 
 


 