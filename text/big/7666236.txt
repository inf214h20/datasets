Arunachalam
 

{{Infobox film
| name           = Arunachalam
| image          = Arunachalam poster.jpg
| alt            =
| caption        = Film poster
| image size     =
| director       = Sundar C
| producer       =  
| writer         =  
| screenplay     = Sundar C
| story          =
| starring       =   Deva
| cinematography = U. K. Senthil Kumar
| editing        = P. Saisuresh
| studio         = Annamalai Cine Combines
| distributor    =
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
 Tamil drama Rambha in Ravichandran in other pivotal roles. The soundtrack and background score for the film was composed by Deva (music director)|Deva, while U. K. Senthil Kumar handled the cinematography. The basic plot has been inspired from the 1985 Hollywood film Brewsters Millions (1985 film)|Brewsters Millions and the 1988 Naseeruddin Shah starrer Hindi film Maalamaal.
 Best Film. 

==Plot== Rambha is Visus daughter and she is appointed as the accountant for Rajni for 30 days.

==Cast==
 
*Rajinikanth as Arunachalam and Vethachalam
*Soundarya as Vedhavalli Rambha as Nandhini
*Jaishankar as Aathikesavan Ravichandran as Ammayappa
*Raghuvaran as Vishvanath
*Visu as Rangachari
*V. K. Ramaswamy (actor)|V. K. Ramasamy as Kalaiperumal
*Nizhalgal Ravi as Prathap Kitty as Kurian
*Senthil as Arunachalams mama
*Janakaraj as Kaathavaraayan Raja
*Anju Aravind
*Shakthi Kumar as Shakthi
*Thevaraj Balakrishnan as Arivu Manorama
*Ponnambalam Ponnambalam as Ponnambalam
*Veeraraagavan
*Vadivukkarasi as Vedhavalli
*Vinu Chakravarthy
*Thevaraj Krishnan as Chokkalingam
*Crazy Mohan as Ayyasaamy Ambika as Meenatshi
*Sundar C in a cameo appearance MRK
 

==Soundtrack==
{{Infobox album
| Name = Arunachalam
| Longtype = to Arunachalam
| Type = Soundtrack Deva
| Cover =
| Released = 
| Recorded = 1996-1997 Feature film soundtrack
| Length = Star Music Tamil
| Label = Big B Vega Music Deva
| Reviews =
| Last album = Abhimanyu (1997)
| This album = Arunachalam (1997)
| Next album = Bharathi Kannamma (1997)
}}

The music was Composed by Deva (music director)|Deva. The song "Nagumo" had two versions, Hariharan version was included in soundtrack only, while another version with vocals of Krishnaraj was included in the film only. Audio was released under the music label "ABCL Music" of Amitabh Bachchan.

{| class="wikitable" style="width:50%;"
|-
! Song Title !! Singers !! Lyricist
|-
| Alli Alli Anarkili || Mano (singer)|Mano, Swarnalatha || Palani Bharathi
|-
| Athanda Ethanda|| S. P. Balasubrahmanyam || Vairamuthu
|- Chitra || Palani Bharathi
|- Chitra || Vairamuthu
|-
| Singam Ondru || Malaysia Vasudevan || Vairamuthu
|-
| Thalai Maganae || S. P. Balasubrahmanyam || Kalidasan
|}

==Production==
 
After the success of  s novel Brewsters Millions.
 Meena who Simran  Rambha were Manorama was also surprisingly added to the cast after causing controversy the previous year by lashing out at Rajinikanths political motives.  

Rajinikanth lost his sentimental Rudraksh bead during the shooting of the film and was shocked and upset to note that his Rudraksh was missing. He ordered a search at the shooting spot at midnight and with the help of the giant lights used for shooting, he later found the divine bead after a while. 

==Release==
Indolink.com gave the film a mixed review citing that "the saving grace of the film is Rajini himself. He has the exceptional ability to convincingly portray the same utopian role of anger and love, innocence and impetuousness...and whats more...he does it with remarkable freshness time and again". 
 Best Film. Best Stunt Best Art Director. The film was later dubbed and released in Telugu under the same name. Rajinis dance with Rambha was a highlight in this movie.

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 