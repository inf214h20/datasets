Banning (film)
{{Infobox film
| name           = Banning
| image          = 
| alt            =  
| caption        = 
| director       = Ron Winston
| producer       = Dick Berg David Hammond-Williams
| writer         = Hamilton Maule James Lee
| starring       = Robert Wagner Anjanette Comer Jill St. John Guy Stockwell Sean Garrison
| music          = Quincy Jones
| cinematography = Loyal Griggs
| editing        = J. Terry Williams
| studio         = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Bob Russell were nominated for an Academy Award for the song, "The Eyes of Love." 

==Plot==
Mike McDermot is a rising golf star on the PGA Tour until he is accused of cheating.  He supposedly has offered to split a winners purse with a competitor, Jonathan Linus, if his opponent deliberately misses a final putt. In fact, the competitor is the one who approached him.  McDermot refuses, so Jonathan Linus goes to another pro, Tommy Del Gaddo, whose glory days were behind him. Then they turn in McDermot, accusing him of their crime.

Linus retires from the PGA Tour and marries into a rich family.  His wife is the daughter of a wealthy businessman in Arizona; through their money, they are now principals (President and General Manager) of an exclusive golf club, the El Presidente. Del Gaddo becomes the head pro at the club as reward for supporting Linus in winning the Eastern Open.

McDermot, now calling himself Mike Banning, arrives and threatens to expose them, so he is given the assistant pros job to quiet him.  Banning proceeds to lure the clubs high rollers to stage a high-wager golf tournament, a Calcutta, in which two-man teams are auctioned off. All money is then put in a pot and split three ways between the teammates and their bidder.

Banning must win this tournament to make enough money, $21,000, to pay off the mob, which had bankrolled his trial on the PGA Tour.  He is literally playing for his life (and that of his dentist who actually took out the loan).

Banning knows the club pro cheats; he plays high-stakes poker, appearing drunk on whiskey while actually drinking iced tea.  Another local aristocrat, Angela Barr, wins the highly competitive bidding for Bannings team over the Presidents daughter.  The president knew of Bannings background and had his daughter bid for him for what he was sure would be the winning team.

Before play proceeds, Chris Patton tells the organizing committee that Banning is a former pro. Banning ends up giving up his handicap of 5 to play at scratch.  He is informed that Patton provided this information and gets into a fight with him, almost killing him.  Patton withdraws from the tournament and is replaced by Linus, now playing with his father-in-law, J. Pallister Young.

The tournament comes down to a sudden-death playoff, just as had happened in the ill-fated attempt to bribe Banning on the tour.  On the 17th hole, with a life-saving shot, Banning makes a near impossible shot over a tall stand of trees.

== Cast ==
* Robert Wagner as Mike Banning
* Anjanette Comer as Carol Lindquist
* Jill St. John as Angela Barr
* Guy Stockwell as Jonathan Linus
* James Farentino as Chris Patton
* Susan Clark as Cynthia Linus
* Howard St. John as J. Pallister Young
* Gene Hackman as Tommy Del Gaddo

== Reception == Howard Thompson of The New York Times called it "unusual, but noticeably unimpressive". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 