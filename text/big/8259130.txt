What's Good for the Goose
{{Infobox film
| name           = Whats Good for the Goose
| image          = Whats Good for the Goose.jpg
| image_size     = 
| caption        = 
| director       = Menahem Golan
| producer       = Tony Tenser
| writer         = Norman Wisdom  Menahem Golan
| music          = Ken Howard
| cinematography = William Brayne
| editing        = Dennis Lanning
| narrator       =  David Lodge
| distributor    = Tigon British Film Productions March 1969 (UK)
| runtime        = 105 minutes (UK)  98 minutes (Edited)
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Whats Good For The Goose, also known as Girl Trouble, is a 1969 British comedy film, and was Norman Wisdoms final starring role in a film. John Hamilton, Beasts in the Cellar: The Exploitation Film Career of Tony Tenser, Fab Press, 2005 p 126-127   Simon Sheridan, Keeping the British End Up: Four Decades of Saucy Cinema, Titan Books 2011 p 61-63 

It was written and directed by Menahem Golan. The film features pop music by Electric Banana otherwise known as the Pretty Things. The film uses locations around the Southport area, including the Birkdale Palace Hotel.

==Plot==
Norman Wisdom plays a 50-something assistant bank manager called Timothy Bartlett whose marriage has become lacklustre.  When he goes to a bankers conference in a coastal town he meets a fun-loving female student called Nikki (Sally Geeson) with whom he has a brief affair and abandons his work responsibilities to have a perfect day with her having fun taking in all the seaside attractions and recapturing his youthful energy.  

He thinks he has fallen in love with Nikki only to find out he was just a one-day novelty for her and she has already moved on to someone her own age.

The next day he invites his wife to join him at the resort and replicates his perfect day with her with his newfound youthful spirit and finds he can have just as much enjoyment with her now that he has rediscovered how to have fun again.

==Production==
There was also a German dubbed version of the film which bears the title Öfter Mal Was Junges!!.  This version is actually 27 minutes shorter than the UK version running to 75 minutes instead of 102 minutes.  It contains alternate identically-staged versions of the hotel bedroom scenes in which Sally Geeson is topless rather than remaining in her bra as she does in the UK print (which is the generally available version).  The text in the opening credits is completely redone in German over the same unfettered film sequence as in the UK version meaning it must have been prepared concurrently.

The main impact on the story caused by the much shorter running time is that Timothy is not seen replicating his perfect day with his wife and the film ends when she arrives at his hotel on a note that makes it seem he is dismayed by her appearance, thus losing the well-rounded charm of the full film.

==Cast==
* Norman Wisdom as Timothy Bartlett
* Sally Geeson as Nikki
* Sarah Atkinson as Meg
* Sally Bazely as Margaret Bartlett
* Stuart Nichol as Bank Manager
* Derek Francis as Harrington
* Terence Alexander as Frisby
* Paul Whitsun-Jones as Clark David Lodge as Porter
* Karl Lanchbury as Pete
* Hilary Pritchard as Cashier in Discotheque

==External links==
 
* 
 

 
 
 
 
 

 