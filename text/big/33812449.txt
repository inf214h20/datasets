Der letzte Fußgänger
{{Infobox film
| name           = Der letzte Fußgänger
| image          = 
| image_size     = 
| caption        = 
| director       = Wilhelm Thiele
| producer       = Otto Meissner (executive producer) Otto Meissner (producer)
| writer         = Eckart Hachfeld (idea) Wilhelm Thiele
| narrator       = 
| starring       = See below
| music          = Franz Grothe
| cinematography = Kurt Grigoleit
| editing        = Martha Dübber
| studio         = 
| distributor    = 
| released       = 1960
| runtime        = 87 minutes
| country        = West Germany
| language       = German English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Der letzte Fußgänger is a 1960 West German film directed by Wilhelm Thiele.

== Plot summary ==
 

== Cast ==
*Heinz Erhardt as Gottlieb Sänger
*Christine Kaufmann as Christine Cornelius
*Käthe Haack as Frau von Hartwig
*Ernst Waldow as Dr. Zollhöfer
*Hans Hessling as Chefredakteur Kleinert
*Michael Lenz as Rudi
*Peter Wegen as Max
*Willy Reichert as Schützenwirt Weber
*Werner Finck as Redakteur Hiss
*Lucie Englisch as Frau Huppert
*Blandine Ebinger as Henriette
*Trude Herr as Frau im Zug
*Günther Ungeheuer as Fotoreporter Pit
*Käte Jaenicke as Italien-Urlauberin
*Harry Tagore as Indischer Prinz
*Margarethe Andersen
*Marianne Prenzel
*Katharina Schmitt
*Dieter Cartini
*Klaus Hellmold
*Fritz Schollmeier

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 
 