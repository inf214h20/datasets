Aa Dekhen Zara
 
 

{{Infobox film
| name           = Aa Dekhen Zara
| image          = Aadekhezaraposter.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Jehangir Surti
| producer       = Viki Rajani
| screenplay     = Sheershak Anand Shantanu Ray Chhibber
| story          = Sheershak Anand Shantanu Ray Chhibber
| starring       = Bipasha Basu Neil Nitin Mukesh Rahul Dev Sophie Choudry
| music          = Pritam Chakraborty Gourov Dasgupta
| cinematography = Jehangir Chowdhury
| editing        = Bunty Nagi
| studio         = Next Gen Films
| distributor    = Eros Entertainment
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = Indian rupee|Rs. 52.5&nbsp;millions. 
}}
Aa Dekhen Zara ( ;  ) is a 2009 romantic Science fiction|sci-fi action thriller film starring Neil Nitin Mukesh, who plays a photo journalist, and Bipasha Basu, as a disc jockey. The film is the directorial debut of Jehangir Surti.

==Plot==
Ray Acharya (Neil Nitin Mukesh), a struggling photographer has nothing going for him… until he inherits a very special camera from his grandfather who was a scientist. Then his life changes in a way that he could not have imagined in his wildest dreams.

The photographs produced by the camera predict the future. Ray uses the camera to obtain winning lottery numbers, winning horses, and also stock prices. His life becomes one big roller coaster ride that takes him from rags to riches and also helps him meet the love of his life, Simi (Bipasha Basu), a DJ with a mind of her own.

However Captain (Rahul Dev) finds out and chases Ray to get the camera for himself. Security authorities also chase Ray as they are aware that Rays grandfather was trying to create a camera that can predict the future. The chase leads them to Bangkok where the climax unfolds. 

==Cast==
{| class="wikitable"
|-
! Actor || Role
|-
| Neil Nitin Mukesh || Ray Acharya
|-
| Bipasha Basu || Simi Chatterjee
|-
| Rahul Dev || Captain
|-
| Sophie Choudry || Bindiya Avasti
|-
| Rajan Korgaonkar || Recovery Agent
|-
| Ashwin Nayak || Bike Rider
|-
| Bobby Vatsa || Inspector Puri
|}(AKB)

==Production==
The movie was initially known as Freeze. But in an interview, Vikram Rajani, the executive director of Eros Entertainment said, "What is Freeze? The movie was always called Aa Dekhen Zara. I do not know how people called the movie Freeze. That was just a working title we had in mind. We have the title Aa Dekhen Zara registered long time back". 

Bipasha Basu performed opposite Neil Mukesh for the first time with this film.  Recent reports say that Neil convinced Bipasha to sing with him the title track, "Aa Dekhe Zara".  Stylist Rakhi Parekh Patil worked on Bipasha Basu’s look for the movie. 

Neil got hurt twice while doing the stunts for the film. 

==Release==

===Promotion===
The first official trailer was released on 7 February 2009. 

Both Bipasha Basu and Neil Nitin Mukesh guest appeared on 14 February 2009 episode of Indian Idol to promote the film.  A trailer for the film was shown midway through the episode.

Both stars also performed to the soundtrack at the 54th Filmfare Awards.

The film was released on 27 March 2009.

==Box office==
The film did moderate business because of a feud between multiplexes owners and the producers. It had an average opening in the first week. The collections started to drop in the second week because of other films with similar themes releasing at the same time (8 x 10 Tasveer, Kal Kissne Dekha). The movie grossed Rs   worldwide.     The film was declared an average grosser.

==Music==
{{Infobox album |  
  Name        =  Aa Dekhen Zara|
  Type        = Soundtrack |
  Artist      = Pritam |
  Released    =  15 February 2009 (India) |
  Recorded    = | Feature film soundtrack |
  Length      = | Eros Music |
  Producer    = Eros Entertainment  |  
  Reviews     = |
  Last album  = Billu  (2009) |
  This album  = Aa Dekhen Zara (2009) |
  Next album  = Luck (2009 film)|Luck (2009)|
}} The Power" by Snap!.

===Tracks===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Track !! Singer(s) !! Composer !! Writer
|-
| 1.
| "Gazab" Shaan
| Pritam
| Irshad Kamil
|-
| 2.
| "Rock The Party"
| Sunidhi Chauhan & Shweta Vijay
| Gourav Dasgupta
| Sheershak Anand
|-
| 3.
| "Power"
| Dibyendu Mukharjee
| Gourav Dasgupta
| Syed Gulrez & Prashant Pandey
|-
| 4.
| "Aa Dekhen Zara (Lounge Mix)"
| Dibyendu Mukherji & Shweta Vijay
| Gourav Dasgupta
| Sheershak Anand
|-
| 5.
| "Mohabbat Aapse"
| Akruti Kakkar
| Pritam
| Irshad Kamil
|-
| 6.
| "Power (Club Mix)"
| Dibyendu Mukharjee
| Gourav Dasgupta
| Syed Gulrez & Prashant Pandey
|-
| 7.
| "Gazab (Club Mix)" Shaan
| Pritam
| Irshad Kamil
|-
| 8.
| "Aa Dekhen Zara"
| Neil Nitin Mukesh & Sunaina
| Gourav Dasgupta
| Sheershak Anand
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 