The Island of Dr. Moreau (1996 film)
{{Infobox film
| name           = The Island of Dr. Moreau
| image          = Island of dr moreau ver2.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = John Frankenheimer
| producer       = Edward R. Pressman Richard Stanley Ron Hutchinson
| based on       =  
| starring       = Marlon Brando Val Kilmer David Thewlis Fairuza Balk
| music          = Gary Chang
| cinematography = William A. Fraker
| editing        = Paul Rubell
| studio         = 
| distributor    = New Line Cinema
| released       =    
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $49,627,779
}} science fiction Richard Stanley Ron Hutchinson.

==Plot==
In 2010, United Nations negotiator Edward Douglas (David Thewlis) survives a plane crash in the Java Sea and is eventually rescued by a passing boat. Aboard, a man called Montgomery (Val Kilmer) tends to him, and after telling him the boat has no radio, he promises Douglas the captain will take him to Timor after dropping him off. However, when they arrive at Montgomerys destination referred to as "Moreaus Island", he instead advises Douglas to stay with him, ostensibly so he can use the radio on the island.

Montgomery unloads a shipment of rabbits at a pen, and one runs away while he slaughters another for Douglas meal. They then head on to the Main House where Douglas is warned not to wander. There, he meets a daughter of Dr. Moreaus called Aissa (Fairuza Balk), but Montgomery turns him away from her and leads him to his room. On the way, they discuss how Moreau vanished after becoming obsessed with his animal research. Montgomery locks Douglas in his room, but he manages to escape that night. He then comes across a laboratory where he witnesses the birth of a mutant baby, belonging to and delivered by human-animal Hybrid (biology)|hybrids. He is noticed, escapes, and runs into Aissa who leads him to the village of the mutants. On the way, they find the partially eaten corpse of a rabbit, not far from a leopard hybrid called Lo-Mai (Mark Dacascos). At the village, they find the Sayer of the Law (Ron Perlman) whose Law preaches "being human" in terms of restraint and discipline. Dr. Moreau (Marlon Brando), referred to as "The Father" by the mutants, appears. He controls the villagers by using a remote control that causes pain through an implant under the creatures skin. Moreau forces the village to hand over Douglas only to peacefully take him to the House to discuss the situation.

Douglas, Montgomery and Moreau gather and he introduces his hybrid "children" and later dine. Then, he explains his creations: he introduced human DNA into animals in search of a higher being, incapable of harm. The existing Beast Folk are imperfect, but Moreau claims to be "closer than   could possibly imagine" in his quest. Moreaus son Azazello (Temuera Morrison) comes in with the rabbit, to the disgust of Moreau who abhors killing. When he learns of the eaten rabbit, he promises that there will be a trial the next day. Douglas tries to escape by boat, but finds it overrun with rat-like creatures and gives up.

At the trial, Azazello unexpectedly shoots Lo-Mai. His body is burned, and a mutant called Hyena-Swine (Daniel Rigney) comes, notices the pain implant among his remains, then removes his own. Montgomery reveals to Douglas that in addition to the pain, the animals are controlled through regular drugging to prevent them from "retrogressing". Hyena-Swine shows off the removed implant, and so Montgomery sets the other beasts after him. Meanwhile, Douglas tries to contact the outside world, but Montgomery sabotages the radio and Aissa reveals to Moreau that she is regressing as it shows her with cat-like eyes.

Hyena-Swine and his trackers (now on his side and also free of implants) break into the House and confront their Father. Angry over their hybrid nature and no longer under his control, they reject humanity and the Law and kill Moreau. His children grieve, except for Azazello who steals Montgomerys gun and goes to join the savages. Aissa informs Douglas that he can stop her regression with a serum from the lab. However, it turns out Montgomery has gone insane and destroyed it. Douglas also finds samples and a file with his name on them, and finds out that Moreau was planning to use his DNA to stop Aissas regression permanently, completing his experiments. Meanwhile, Azazello leads the savages to the armory.

The savages have now taken over the island: Azazello shoots Montgomery at the village and Hyena-Swines faction rampages around the island. Azazello hangs Aissa, but is shot dead by Hyena-Swine after bringing Douglas to him. Douglas manages to survive by telling Hyena-Swine to impose his leadership and be "God Number One" among the others of his faction. During the battle, Douglas escapes and Hyena-Swine is killed in the burning building.

The Sayer of the Law and Assassimon see off Douglas as he leaves. The Sayer of the Law tells Douglas that the hybrids want to return to their natural state of being. In closing narration, Douglas reflects on the savagery that also emerges in humans and claims that he leaves the island "in fear".

==Cast==
* Marlon Brando as Dr. Moreau, a mad scientist who created the Beast Folk.
* David Thewlis as Edward Douglas, a U.N. agent who gets left to die in the middle of the ocean and comes to the island.
* Val Kilmer as Dr. Montgomery, a former neurosurgeon who is a vet on Dr. Moreaus island.
* Fairuza Balk as Aissa, a beautiful cat hybrid and Moreaus "daughter" who looks more human than the other hybrids.
* Daniel Rigney as Hyena-Swine, a vicious hyena and pig hybrid.
* Temuera Morrison as Azazello, a dog-like hybrid and Moreaus "son" who is assigned to find the hybrids.
* Nelson de la Rosa as Majai, a miniature version of Dr. Moreau who doesnt speak. Peter Elliott as Assassimon, a baboon-like hybrid who is the only primate hybrid and Five Finger Man.
* Mark Dacascos as Lo-Mai, a leopard hybrid who is accused of breaking the laws (suckling water from a stream, walking on all fours, and eating flesh).
* Ron Perlman as Sayer of the Law, a blind goat-like hybrid who is the priest-like figure among the hybrids.
* Marco Hofschneider as MLing
* Miguel López as Waggdi
* William Hootkins as Kiril

==Production== Richard Stanley spent four years developing the project before getting the green-light from New Line Cinema. The first sign of trouble appeared when Kilmer suddenly decided - for reasons of his own - that he wanted his role cut by 40%. Stanley knew that it was impossible to cut the role of UN diplomat Edward Prendick (later changed to Edward Douglas) by such a drastic amount, but he wanted to keep Kilmer on board, so he hit on the idea of switching him to the role of Dr. Montgomery, Moreaus assistant on the island. Kilmer agreed to this proposal, so the part of Prendick was given to Rob Morrow. The chosen location for the film were the rain forests outside Cairns in North Queensland, Australia. 

Three days into filming, New Line fired Stanley. The reasons for Stanleys dismissal were not made clear, but it is likely because of difficulties with Kilmer, who was known to be difficult on sets and was going through a divorce at the time. During the first days shooting, Kilmer would not deliver the dialogue as scripted, and the footage was deemed unusable. The studio seemed to have blamed the director for not getting Kilmer under control.   
 Richard Stanley, Ron Hutchinson. When Morrow also decided to leave the production, Frankenheimer needed to find a new lead actor and brought in David Thewlis to play Douglas. The whole production was shut down for a week and a half while these changes were implemented. 
 Will Rogers never met Val Kilmer." Frankenheimer also reportedly clashed with Brando and the studio, as they were concerned with the direction he was taking the film.

The making of the film proved so disastrous that Fairuza Balk, who played the female lead, actually tried to escape production, but was caught at the airport and promptly returned to the set.

According to Thewlis, "we all had different ideas of where it should go. I even ended up improvising some of the main scenes with Marlon." Thewlis went on to rewrite his character personally. The constant rewrites also got to Brandos nerves and having no motivation to keep rehearsing new lines, he was equipped with a small radio receiver - a technique hed used on earlier films. Thewlis recollects: "  be in the middle of a scene and suddenly hed be picking up police messages and would repeat, Theres a robbery at Woolworths." Even Brando clashed with Kilmer over the latters continuing erratic behavior. According to Film Threat magazine, Brando pointed out to him: "Youre confusing your talents with the size of your paycheck".
 Cheyenne as the atoll he owned. Upon completion of Kilmers final scene, Frankenheimer is reported to have said to the crew, "Cut, Now get that bastard off my set."

As a joke, Stanley reportedly told the production designer to burn the set down; security was tightened in case he was actually trying to sabotage the project.    One rumor surfaced (promoted by Stanley)  that he did however manage to sneak back on the set in full costume as one of the many human-animal hybrids.  Another reports that he showed up at the films wrap party where he ran into Kilmer, who was said to have apologized profusely for Stanleys removal from the film.

Thewlis chose to skip the films premiere.

==Directors cut==
Eventually, a directors cut was released on DVD, extending the 96-minute film to 100 minutes.

==Reception==
The film was met with negative reviews; Rotten Tomatoes currently rates the film with a 23% "Rotten" based on thirty-one reviews.  The film grossed only $49 million worldwide on a $40 million budget, which, with marketing and other expenses, lost money for the studio   
 Razzie Awards Worst Picture Worst Director, Worst Supporting Actor for Marlon Brando (Val Kilmer was also a nominee in this category). The film also got nominations for two Saturn Awards: Best Make-Up and Science Fiction Film.

==See also==
The two earlier film versions of the story: Island of Lost Souls starring Charles Laughton and Bela Lugosi The Island Michael York
*  , a 2014 documentary on the filming of The Island of Dr. Moreau

==Notes==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 