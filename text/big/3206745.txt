The Battle of Russia
{{Infobox film
| name           = Why We Fight: The Battle of Russia
| image          = Poster of The Battle of Russia.jpg
| caption        = 
| director       = Frank Capra; Anatole Litvak
| producer       = Office of War Information Julius Epstein; Philip Epstein
| narrator       = Walter Huston
| starring       = 
| music          = 
| cinematography = Robert Flaherty
| editing        = William Hornbeck
| distributor    = War Activities Committee of the Motion Pictures Industry
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Battle of Russia (1943) is the fifth film of  . The score was done by Russian-born Hollywood composer, Dimitri Tiomkin, and drew heavily on Tchaikovsky along with traditional Russian folk songs and ballads. 

Film historian Christopher Meir notes that the films popularity "extended beyond the military audience for it was initially intended, and was the second in the series to be nominated for an Academy Award for Best Documentary Feature. 

==Story== Germany in World War II.
 Tchaikovsky and Leo Tolstoys book War and Peace are also mentioned. Communism is never mentioned at any point in the film; instead, the Russian Orthodox Church is described as a force opposing Nazism. The start of the film includes a quote from U.S. Gen. Douglas MacArthur, who commended the Russian peoples defense of their nation as one of the most courageous feats in military history.

The film then covers the Nazi conquests in the Balkans, described as a preliminary to close off possible Allied counter-invasion routes, before the war against Russia was launched on June 22, 1941.  The narration describes the German "keil und kessel" tactics for offensive warfare, and the Soviet "defense in depth" used to counter this.  The scorched earth Soviet tactics, the room-to-room urban warfare in Soviet cities, and the guerilla warfare behind enemy lines are also used to underline the Soviet resolve for victory against the Germans. The Siege of Leningrad and the Battle of Stalingrad conclude the film.

In order to justify the Western Allies alliance with the Soviet Union, the episode, like the entire Why We Fight series, misportrayed or simply omitted many facts, which could have cast doubts on the "good guy" status of the Soviets, such as the Molotov-Ribbentrop Pact|Nazi-Soviet non-aggression pact, Soviet invasion of Poland; Soviet occupation of the Baltic States, Winter War, and others.    Nor did the film mention the word "Communism." 

Virtually in line with the Soviet propaganda, the series was not only screened but widely acclaimed in the Soviet Union.    To exonerate the Soviets, the series casts even less important Allies, like the Poles, in a bad light.   The episode has been described as "a blatant pro-Soviet propaganda posing as factual analysis"  and was withdrawn from circulation during the Cold War.  Capra commented about why certain material was left out:

 }}

==Awards==
*Academy Awards, 1944: Nominated,  Best Documentary, Features   

*National Board of Review, 1943: Won, NBR Award, Best Documentary 

*National Film Preservation Board, 2000: Won, National Film Registry, as part of the Why We Fight series (1943-1945) 

*New York Film Critics Circle Awards, 1943: Won, Special Award 

==See Also==

* Propaganda in the United States

==References==
 

==External links==
 
*  
*  
*  
*  
*  


 

 
 
 
 
 
 
 
 