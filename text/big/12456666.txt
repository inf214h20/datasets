Nice Girls Don't Explode
{{Infobox Film
| name           = Nice Girls Dont Explode
| image          = Nice girls dont explode.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Chuck Martinez
| producer       = Douglas Curtis Paul Harris
| narrator       =  Barbara Harris William OLeary Wallace Shawn James Nardini
| music          = Brian Banks Anthony Marinelli
| cinematography = Stephen M. Katz	
| editing        = Wende Phifer Mate	 New World Pictures 1987
| runtime        = 92 min
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American comedy Barbara Harris, William OLeary, New World Pictures. 

==Plot== Barbara Harris) because flames have a tendency to spontaneously erupt whenever her hormones are aroused; for April, "protection" on a dinner date is carrying a fire extinguisher. As her mother explains, April is a "fire girl," whose very unstable body chemistry causes spontaneous combustion when she is aroused. As such, the only men April meets more than once are firefighters.
 William OLeary), a former neighbor who has returned to Aprils life, he challenges Aprils and her mothers assumption and presses his luck to prove to her that her hormones are not, in fact, explosive. Hijinks result; as Andy tries to prove his point and get the girl, he is thwarted at every turn by Aprils mother. Further complications ensue when April befriends a lonely, obsessive pyromaniac named "Ellen" (Wallace Shawn), who becomes incensed at the constant mishearing of his real name "Ellen" for "Helen," after which he throws BIC lighter-flicking snits, trying to set his tormentors ablaze.

==Box office==
"Nice Girls Dont Explode" had a domestic box-office total of only $65,000. 

==Production==
At least part of the film was shot or produced in the cities of Lawrence, Kansas, Ottawa, Kansas, and Overland Park, Kansas. 

==References==
 
 
==External links==
*  

 
 
 
 
 
 