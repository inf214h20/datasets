Our Last Spring
 
{{Infobox film
| name           = Our Last Spring
| image          =
| caption        =
| director       = Michael Cacoyannis
| producer       = Michael Cacoyannis
| writer         = Michael Cacoyannis Jane Cobb Kosmas Politis (novel)
| narrator       =
| starring       = Alexandros Mamatis
| music          =
| cinematography = Walter Lassally
| editing        =
| distributor    =
| released       =  
| runtime        = 120 minutes
| country        = Greece
| language       = Greek
| budget         =
}}

Our Last Spring ( , Transliteration|translit.&nbsp;Eroica) is a 1960 Greek drama film directed by Michael Cacoyannis and based on the 1938 novel "Eroica" by Greek writer Kosmas Politis. It was entered into the 10th Berlin International Film Festival.   

==Cast==
* Alexandros Mamatis - Alekos
* Jenny Russell - Monika
* Nikiforos Naneris - Dimitris
* Panos Goumas - Loizos Patrick OBrian - Sebastian
* Marie Ney
* Tasso Kavadia
* Lydia Vasileiadou
* Jane Cobb
* Robin Fife
* Nikos Ignatiadis
* Giannis Voglis
* Nikos Pilavios - Nestor
* Faidon Georgitsis
* Michalis Nikolinakos
* Paris Pappis
* Nana Gatsi

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 