Torment (1950 film)
{{Infobox film
| name           = Torment
| image_size     =
| image	         = Torment FilmPoster.jpeg
| caption        =
| director       = John Guillermin
| writer         = John Guillermin
| narrator       = John Bentley
| music          = John Wooldridge Gerald Gibbs
| editing        = Robert Jordan Hill
| studio         = Advance Productions
| distributor    = Adelphi Films
| released       = 3 February 1950 (London) (UK)  November 10, 1950 (US)
| runtime        = 68 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} British thriller John Bentley. 

==Plot==
Brothers Cliff and Jim Brandon are a successful writing team specializing in murder mysteries, but Cliff and Jim are almost as disturbed as some of the characters theyve created. Whilst researching their latest novel, one of the brothers commits murder, simply to experience the thrill. He then attempts to frame his secretary Joan for the crime. His reason this time is personal: both brothers are in love with Joan, but she prefers one over the other. The saner of the two brothers races against time to save Joan from the gallows and to bring his sibling to justice. 

==Cast==
* Dermot Walsh as Cliff Brandon  
* Rona Anderson as Joan   John Bentley as Jim Brandon  
* Michael Martin Harvey as Curley Wilson  
* Valentine Dunn as Mrs. Crier  
* Dilys Laye as Violet Crier

==Critical reception==
TV Guide wrote, "the story here is nothing new, but direction is fresh and original. Taking this simplistic plot line, Guillermin manages to inject some good suspense into a modestly budgeted feature. Walsh, Bentley, and Anderson play their roles well and play against one another with skill."   

==External links==
* 

==References==
 

 

 
 
 