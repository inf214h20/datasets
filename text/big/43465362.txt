Knock, Knock (2015 film)
{{Infobox film
| name           = Knock, Knock
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Eli Roth
| producer       = Miguel Asensio Colleen Camp John T. Degraye Cassian Elwes Nicolás López Eli Roth 
| writer         = Guillermo Amoedo Nicolás López Eli Roth 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Keanu Reeves
| narrator       = 
| music          = 
| cinematography = Antonio Quercia 
| editing        = 
| studio         = Camp Grey Dragonfly Entertainment Sobras International Pictures Lionsgate
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 horror thriller film directed by Eli Roth, who also co-wrote the script with Guillermo Amoedo and Nicolás López. The film stars Keanu Reeves.

== Cast ==
* Keanu Reeves  as Evan Webber
* Ignacia Allamand  as Karen Alvarado
* Lorenza Izzo    as Genesis
* Ana de Armas  as Bel
* Aaron Burns  as Louis
* Colleen Camp  as Vivian

== Production == Lionsgate acquired the distribution rights to the film. 

== Origins ==
Though not stated in the press, Knock Knock is an update of Peter Traynors Death Game (1977) .


=== Filming ===
The filming began on April 14, 2014, in Santiago, Chile,  and finished on May 11, 2014. 

==Release==
The film premiered at the Sundance Film Festival on January 23, 2015. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 