Bachelor's Paradise
{{Infobox film
| name = Bachelors Paradise
| image =
| image_size =
| caption =
| director = Kurt Hoffmann
| producer =  Heinz Rühmann
| writer =  Johannes Boldt  (novel)   Karl Peter Gillmann    Günter Neumann 
| narrator =
| starring = Heinz Rühmann   Josef Sieber   Hans Brausewetter   Trude Marlen
| music = Michael Jary  
| cinematography = Carl Drews  
| editing =  Arnfried Heyne      
| studio = Terra Film
| distributor = Terra Film
| released = 1 August 1939
| runtime = 91 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Bachelors Paradise (German:Paradies der Junggesellen) is a 1939 German comedy film directed by Kurt Hoffmann and starring Heinz Rühmann, Josef Sieber and Hans Brausewetter.  It was based on a novel by Johannes Boldt. The film featured the popular song Das kann doch einen Seemann nicht erschüttern.

==Synopsis==
After getting his second divorce, Hugo Bartels and his two ex-military comrades agree a pact to form a "paradise for bachelors" club in which all are pledged never to get married again. However, when Hugo meets and falls in love with an attractive woman he faces as a quandary. He is eventually able to marry her after introducing his friends to his two ex-wives who also fall in love.

==Cast==
*   Heinz Rühmann as Hugo Bartels, Standesbeamter 
* Josef Sieber as Caesar Spreckelsen, Apotheker  
* Hans Brausewetter as Dr. Balduin Hannemann, Studienrat  
* Trude Marlen as Frau Platen, Hausbesitzerin  
* Hilde Schneider as Hermine, Hugos zweite geschiedene Frau  
* Gerda Maria Terno as Eva, Hugos erste geschiedene Frau 
* Lotte Rausch as Frau Wagenlenker, Hugos Vermieterin 
* Maly Delschaft as Amalie Bernau  
* Armin Schweizer as Emil Bernau, Mieter in Frau Platens Haus  
* Albert Florath as Landgerichtsdirektor  
* Paul Bildt as Stadtrat Krüger  
* Werner Schott as Kapitän  
* Irene Andor as Wirtschafterin der Apotheke  
* Charly Berger as Beisitzender Richter 
* Otto Braml as Wilhelm, Möbelpacker  
* Gerhard Dammann as Wirt 
* Jac Diehl as Gast im Pavillon 
* Aribert Grimmer as August, Möbelpacker   Knut Hartwig as Hugos Verteidiger 
* Clemens Hasse as Ein Matrose 
* Carl Iban as 3. Möbelpacker  
* Antonie Jaeckel as Mieterin  
* Melitta Klefer as Frau beim Kameradschaftstreffen  
* Erwin Laurenz as Normann, der junge Ehemann  
* Lucia Lumera as Frau Liesegang  
* Hellmuth Passarge as Gast beim Kameradschaftstreffen 
* Wera Schultz as Anna, Mädchen bei Frau Platen 
* Rudolf Schündler as Rechtsanwalt 
* Albert Venohr as Portier  
* Egon Vogel as Gast beim Kameradschaftstreffen  
* Eduard Wenck as Auktionator 
* Willi Witte as Trauzeuge

== References ==
 

== Bibliography ==
* Etlin, Richard A. Art, Culture, and Media Under the Third Reich. University of Chicago Press, 2002. 
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 