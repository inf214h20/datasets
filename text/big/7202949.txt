A Home of Our Own
 
{{Infobox film
| name           = A Home of Our Own
| image          = A Home of Our Own (1993 film).jpg
| caption        = Theatrical release poster
| writer         =
| starring       = Kathy Bates Edward Furlong Soon-Tek Oh Tony Campisi Clarissa Lassig
| director       = Tony Bill
| producer       =
| editing        =
| cinematography =
| distributor    = Gramercy Pictures Metro Goldwyn Mayer
| released       = November 5, 1993
| runtime        = 103 mins.
| language       = English
| rating         =
| music          = Michael Convertino
| budget         =
| gross          = $1,677,807
}}

A Home of Our Own is a 1993 drama film directed by Tony Bill, starring Kathy Bates and Edward Furlong. It is the story of a mother and her six children trying to establish a home in the small town of Hankston, Idaho in 1962.  

==Synopsis==
Frances Lacey, a widow, works at a factory.  She is fired when one of the men gropes her, and she hits him in return.  The same day, her son is brought home by the police, for stealing change from payphones, but they dont press charges.  Shortly after this, Frances decides that LA is not the place to raise a family.  She packs the kids up, sells everything they can carry, and starts driving.  She figures shell know where shes going when she sees it.  

The family is picking through the charred remains of the house when Frances finds their meager savings in a blackened jar. Hope is reborn for Frances but Shayne angrily demands a reality check. When rebuilding seems impossible, Mr. Munimura arrives with professional town folk and supplies to rebuild. Whether stubborn independent Frances likes it or not, rebuilding has started as Mr. Munimura gives her a comforting hug.  Frances relents, but true to character, she states that all will be paid back.  Toys, clothes, and blankets are also provided for the children.  Frances only lets them build the house as far as it was before the fire.  Shayne, narrating, says that it took them six months to finish the rest of the house, and four years to pay everyone back, but that it brought them all closer together as a family.  Even though he hated Idaho at first, he still lives there, and has never been back to LA.

==Cast==
* Kathy Bates as Frances Lacey
* Edward Furlong as Shayne Lacey
* Clarissa Lassig as Lynn Lacey
* Sarah Schaub as Faye Lacey
* Miles Feulner as Murray Lacey
* Amy Sakasitz as Annie Lacey
* T.J. Lowther as Craig Lacey   
* Soon-Tek Oh as Mr. Munimura
* Tony Campisi as Norman

==External links==
*  

 

 
 
 
 
 
 