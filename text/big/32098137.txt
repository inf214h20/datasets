Jagir (1984 film)
{{Infobox film
 | name = Jagir
 | image = 
 | caption = DVD Cover
 | director = Pramod Chakravorty
 | producer = Pramod Chakravorty
 | writer = 
 | dialogue =  Pran Danny Denzongpa Amrish Puri Shoma Anand
 | music = R D Burman
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = October 26, 1984
 | runtime = 135 min.
 | language = Hindi Rs 3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1984 Hindi Indian feature directed by Pramod Chakravorty, starring Dharmendra, Mithun Chakraborty, Zeenat Aman,  Pran (actor)|Pran, Danny Denzongpa, Shoma Anand and Amrish Puri.

==Plot==

Jagir is an action film in starring Dharmendra  and Mithun Chakraborty in lead roles.

==Cast==

*Dharmendra 
*Mithun Chakraborty 
*Zeenat Aman 
*Pran 
*Shoma Anand 
*Amrish Puri 
*Danny Denzongpa 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaj Ki Raat"
| Asha Bhosle
|-
| 2
| "Chor Tera Naam Hai"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Hum Dilwale"
| Shailender Singh, Kishore Kumar, Shakti
|-
| 4
| "Naya Naya Hota Hai"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Sab Ko Salaam Karte Hain"
| R D Burman, Asha Bhosle
|-
| 6
| "Shaharon Mein Se Shahar"
| Kishore Kumar
|}
==References==
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Jaagir

==External links==
*  

 
 
 
 
 

 