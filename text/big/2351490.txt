Attack of the Crab Monsters
{{Infobox film
| name           = Attack of the Crab Monsters
| image          = Attack of the Crab Monsters 1957.jpg Theatrical release poster
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Charles B. Griffith Pamela Duncan Russell Johnson
| music          = Ronald Stein
| cinematography = Floyd Crosby
| editing        = Charles Gross Allied Artists
| released       =  
| runtime        = 62 min
| country        = United States
| awards         =
| language       = English
| budget         = $70,000 Frank, 1998 p. 38. 
| gross = $1 million 
}}
 American black-and-white Pamela Duncan and Russell Johnson. 

Attack of the Crab Monsters concerns a second scientific expedition that is sent to a remote Pacific island to discover what happened to the scientists of the first. Unknown to them when they arrive, the island is inhabited by a mating pair of two radiation-mutated intelligent giant crabs that consumed the first expedition. The giants are also slowly undermining the geology of the island, causing it to fall away, piece-by-piece, into the ocean.

==Plot== the Bikini Atoll nuclear tests on the islands plant and sea life. They learn to their horror that the earlier group have been killed and eaten by two mutated, intelligent giant crabs, who have also absorbed the minds of their victims and can speak telepathically in the voices of their victims.

Members of the current expedition are then systematically attacked and killed by the monsters, which are now invulnerable to most standard weaponry because of the mutations to their cell structures. The remaining scientists finally discover that both giant crabs are the cause of the ongoing earthquakes and landslides on the island; they are slowly destroying the island, reducing its size. The scientists turn their attention to a way to stop the mating pair of monsters from reproducing.

As the island continues to fall away into the Pacific, and after barely escaping from a laboratory that is about to collapse, the surviving trio witness up close one of the intelligent giant crabs for the first time. This is Hoolar who speaks to them via telepathy and vows to go to the mainland with her fertilized eggs when the island is gone (and the three scientists are dead) to feed upon on even more humans, absorbing those minds in the process. One of the remaining three then sacrifices himself in order to kill the monster and her unhatched brood.

Following the giants destruction, the two in-love survivors embrace on the small portion of what remains of the island.

==Cast==
 
* Richard Garland as Dale Drewer Pamela Duncan as Dr. Martha Hunter
* Russell Johnson as Hank Chapman
* Leslie Bradley as Dr. Karl Weigand
* Mel Welles as Jules Deveroux
* Richard H. Cutting as Dr. James Carson
* Beach Dickerson as Ron Fellows
* David Arvedon as Hoolar The Giant Crab
* Tony Miller as Jack Sommers
* Ed Nelson as Ensign Quinlan
* Charles B. Griffith as Tate
* Robin Riley
 

==Production==
===Script===
Writer Charles B. Griffith later described the scripting process:  Roger came to me and said, "I want to make a picture called Attack of the Giant Crabs, and I asked, "Does it have to be atomic radiation?" He responded, "Yes." He said it was an experiment. "I want suspense or action in every scene. No kind of scene without suspense or action." His trick was saying it was an experiment, which it wasnt. He just didnt want to bother cutting out the other scenes, which he would do.  
===Underwater photography===
Griffith directed some underwater sequences (and also appeared in a small role). Griffith said:
 I had just read The Silent World by Jacques Cousteau and found it to be new and exciting. So when that picture came along, I wrote all the underwater stuff and went to Roger and told him I’d direct all the underwater parts for $100. He said, “Okay.” If I had just asked, he would have said, “No.” I had to put it in a way that he would jump at. So I directed all that stuff and it was rather funny. I’d be down at the bottom of the tank at Marineland trying to get actors to do something while   Floyd Crosby was hammering at the glass window trying to get them to do something else.   It was all pretty silly.  

==Reception==
===Theatrical release=== Not of This Earth. Attack of the Crab Monsters was Cormans most profitable production up to that time, which he attributed to the "wildness of the title," the construction of the storyline,  the structuring of every scene for horror and suspense, and editing for pace. di Franco 1979 p. 78.  Corman has stated that the success of the film convinced him that horror and humor was an effective combination.  

According to Tim Dirks, the film was one of a wave of "cheap teen movies" released for the Drive-in theater|drive-in market. They consisted of "exploitative, cheap fare created especially for them   in a newly-established teen/drive-in genre."  

Film reviewer Glenn Erickson, writing in DVD Savant, noted that for Corman, Attack of the Crab Monsters was "... (a) more ambitious production, it covers the methodical destruction and inundation of an entire island – all of which occurs off-screen. Charles B. Griffiths screenplay keeps the story hopping for just over an hour but limits the show to a minimum of locations." Erickson, Glenn.   DVD Savant, December 28, 2010. Retrieved: January 9, 2015. 

==References==
Notes
 
Bibliography
 
* Corman, Roger and Jim Jerome. How I Made A Hundred Movies In Hollywood And Never Lost A Dime. New York: Random House, 1990. ISBN 978-0-306-80874-6.
* di Franco, J. Philip, ed. The Movie World of Roger Corman. London: Chelsea House Publishers, 1979. ISBN 978-0-87754-122-6.
* Frank, Alan. The Films of Roger Corman: Shooting My Way Out of Trouble. Bath, UK: Bath Press, 1998. 978-0-71348-272-0.
* Warren, Bill. Keep Watching the Skies: American Science Fiction Films of the 1950s, 21st Century Edition. Jefferson, North Carolina" McFarland & Company, 2009. ISBN 978-0-78644-230-0.
* Wingrove, David. Science Fiction Film Source Book. London: Longman Group, Limited, 1985. ISBN 978-0-58289-239-2.
 

==See also==
* List of American films of 1957
*The Macra Terror - A Doctor Who serial that follows a similar plot.

==External links==
* 
*  at Eccentric-cinema.com
*  
*  at Internet Archive
 
 

 
 
 
 
 
 
 
 
 
 
 
 