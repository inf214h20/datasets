Space Battleship Yamato (1977 film)
{{Infobox Film
| name =  Space Battleship Yamato
| image = 
| caption =  Toshio Masuda
| producer = Yoshinobu Nishizaki	
| screenplay = Eiichi Yamamoto Keisuke Fujikawa
| story = Leiji Matsumoto Yoshinobu Nishizaki
| starring = Kei Tomiyama Yoko Asagami Shusei Nakamura
| studio = Office Academy
| music = Hiroshi Miyagawa
| distributor =
| released =  
| runtime = 145 minutes
| country = Japan
| language = Japanese
| budget = ¥ 200,000,000
| gross = ¥ 2,100,000,000
}}

  (also rendered as Space Cruiser Yamato) is the first theatrical movie based on the classic anime series (known as Star Blazers in the United States). Unlike the later films that would follow it, this is a compilation film consisting of various television episodes edited from the "Iscandar" arc of the television series. It originally had a new ending created for the theatrical release in which Starsha had died before the Yamato reaching Iscandar. This ending was removed for the television broadcast and was lost until the DVD release. In English speaking countries, it was known by the title Space Cruiser. {{cite news|title=Flying off to Iscandare for the Cosmo DNX! Can we defeat the Gorgons?
|work=StarBlazers.com|date=|url=http://web.archive.org/web/20120319164824/http://www.starblazers.com/html.php?page_id=236|accessdate=2008-09-10}} 

== Plot ==
In the distant future, the war between the human race and the aliens known as the Gamilons has destroyed the Earth. Radioactive asteroids have devastated the planet making its atmosphere uninhabitable. In an effort to assist the Earth, Queen Starsha of the planet Iscandar offers the Earth Forces a device that can completely neutralize the radiation.

In order to get this device, the space battleship Yamato is launched from the remains of its World War II ancestor on a 148,000 light-year journey. The crew of the Space Battleship Yamato has only one Earth year to travel to Iscandar and back, or the human race will become extinct. 

== Japanese cast == Aruga Kōsaku
* Kei Tomiyama - Kodai Susumu
* Shūsei Nakamura - Shima Daisuke
* Yōko Asagami - Mori Yuki
* Ichirō Nagai - Dr. Sado Sakezō / Tokugawa Hikozaemon
* Taichirō Hirokawa - Kodai Mamoru
* Takeshi Aono - Sanada Shirō
* Masatō Ibu - Desler/Tōdō Heikurō Osamu Kobayashi - Domel
* Michiko Hirai - Starsha
* Akira Kamiya - Katō Saburō Kenichi Ogata - Analyzer / Yabu Sukeharu
* Keisuke Yamashita - Hiss / Sugiyama Kazuhiko / Jirō Nōmura
* Takeshi Ōbayashi - Schultz
* Akira Kimura - Narration

==Revivals== Space Battleship Yamato opened in Japan, followed by several sequels. Also resulting from this franchise is Space Battleship Yamato 2199, an anime reimagining of the classic story. A live-action revival of Space Battleship Yamato is set to reach theaters in 2017 under the tentative title of Star Blazers. It will be produced by Skydance Productions with Shoji Nishizaki as the executive producer and Christopher McQuarrie as the director. 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 