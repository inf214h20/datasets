Onionhead
 
{{Infobox film
| name           = Onionhead
| caption        = theatrical release poster
| image_size     = 215px
| image	         = Onionhead FilmPoster.jpeg
| director       = Norman Taurog
| producer       = Jules Schermer
| based_on       = Onionhead (novel) by Weldon Hill
| writer         = Nelson Gidding
| starring       = Andy Griffith
| music          = David Buttolph
| cinematography = Harold Hal Rosson
| editing        = William H. Ziegler
| studio         = Warner Bros.
| released       =  
| runtime        = 111 minutes
| country        = United States English
| budget         = $960,000 Michael A. Hoey, Elvis Favorite Director: The Amazing 52-Film Career of Norman Taurog, Bear Manor Media 2013 
| gross          =
}}
 Erin OBrien, James Gregory, Coast Guard.

Griffith had experienced success with his previous service comedy, No Time for Sergeants, and Onionhead was an attempt to cash in on that success. It was marketed as an uproarious comedy but is actually a comedy-drama with some fairly dark themes. Onionhead was such a notorious flop that it drove Griffith into television, according to Griffiths videotaped interview in the Archive of American Television.

==Plot==
In the spring of 1941, Al Woods quits an Oklahoma college to join the armed forces after a quarrel with his co-ed sweetheart, Jo. He joins the Coast Guard, partly by chance due to the flip of a coin.  After boot training, Al is assigned to a buoy tender in Boston, the Periwinkle, as a ships cook. He encounters immediate hostility from the chief of the galley, Red Wildoe, from new crew mates and cooks helpers Gutsell and Poznicki, and from his arrogant department head, Lieutenant (junior grade) Higgins.
 Pearl Harbor is attacked and war declared.  Wildoe abruptly proposes to Stella and they marry. A free-for-all breaks out at their wedding celebration, a jealous Al instigating a fight with soldiers who are clearly familiar with Stella already.  Wildoe is assigned to another vessel performing convoy duty at sea. During this time, Stella begins seeing other men. Al tries to prevent this on Wildoes behalf, but cant resist Stella himself.

Aboard the Periwinkle, Al becomes the new chief cook. Higgins, promoted to executive officer, is entering lesser amounts than they pay for the cost of officers meals into the ledger of the ships mess and pocketing the difference.  He purchases substandard food for the crew to keep the mess budget from showing a deficit. Higgins also objects to finding Als hair in his food, so Al shaves his scalp bald, earning the nickname "Onionhead."  Assuming erroneously that all the officers are in on the scam, Al bypasses channels to report the theft to the District Office.  During leave to attend his fathers funeral, Al reconnects with Jo, realizing that she is the one he loves. In port again, Wildoe asks Al to take Stella home from the bar one night when he is recalled to his ship. Stella tries to seduce Al, who calls her a tramp. She replies: "I cant help what I am."
 rating and reassignment to Greenland, but also informs Higgins that he will have to repay every embezzled dollar before his court-martial.  He gently chastises Al for not having come to him with the proof earlier, but gives him leave to marry Jo before he ships out for Greenland.

==Cast==
  
*Andy Griffith as Al Woods
*Felicia Farr as Stella
*Walter Matthau as Red Wildoe   Erin OBrien as Jo Hill James Gregory as the Skipper
*Joey Bishop as Sidney Gutsell
 
*Roscoe Karns as Windy Woods
*Claude Akins as Poznicki
*Ray Danton as Higgins
*Sean Garrison as Yeoman Kaffhamp
*Dan Barton as Ensign Fineberg
 

==Production== Coast Guard station in Alameda, California and on the USS Heather in Long Beach, California. 

==References==
Notes
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 