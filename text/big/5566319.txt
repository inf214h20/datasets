The Virginian (1929 film)
 
{{Infobox film
| name           = The Virginian
| image          = Poster - Virginian, The (1929) 01.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Victor Fleming
| producer       = Louis D. Lighton
| writer         = {{Plainlist|
* Kirk LaShelle
* Grover Jones
* Howard Estabrook
* Keene Thompson
* Edward E. Paramore Jr.
}}
| based on       =  
| starring       = {{Plainlist|
* Gary Cooper
* Walter Huston
* Richard Arlen
}}
| music          = Karl Hajos
| cinematography = {{Plainlist|
* J. Roy Hunt
* Edward Cronjager
}} William Shea
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}} Western film The Virginian by Owen Wister and adapted from the popular 1904 theatrical play Wister had collaborated on with playwright Kirke La Shelle. 
The Virginian is about a good-natured cowboy who romances the new schoolmarm and has a crisis of conscience when he learns his best friend is involved in cattle rustling. The film is well known for Coopers line, "If you wanna call me that—smile," in response to a cuss by the antagonist. 

==Plot== the Virginian (Gary Cooper) is ranch foreman at Box H Ranch near Medicine Bow, Wyoming. At a saloon in Medicine Bow, he and the cattle rustler Trampas (Walter Huston) vie for the attentions of a barmaid; when Trampas insults him, the Virginian pulls a gun and tells him to smile. Soon afterwards, Molly Wood (Mary Brian), a new schoolteacher from Vermont, arrives in town. The Virginian and a drifter named Steve (Richard Arlen) vie for her attentions, but she ultimately chooses the latter. However, as Steve was his childhood friend, the Virginian gives him a job at the ranch.

Unhappy with the Virginians violent nature, Molly tries to change him but is unsuccessful. hang all involved, including Steve. The Virginian vows revenge on Trampas for forcing him to do so.

Disgusted by The Virginians callousness, Molly leaves him. However, after he is shot in the back by Trampas, she decides to treat him, and they fall deeper in love; they eventually decide to marry. On their wedding day, Trampas comes back to town for revenge and challenges the Virginian to a shoot-out. The Virginian quickly draws his six-shooter and kills the bandit in the streets. He then marries Molly, and the two prepare to open their own ranch.

==Cast==
* Gary Cooper as the Virginian
* Walter Huston as Trampas
* Richard Arlen as Steven
* Mary Brian as Molly Stark Wood
* Chester Conklin as Uncle "Pa" Hughey
* Eugene Pallette as "Honey" Wiggin
* Victor Potel as Nebrasky
* E.H. Calvert as Judge Henry
* Helen Ware as Mrs. Ma Taylor
* Randolph Scott as Rider (uncredited)   

==Production== novel of 1914 and The Virginian (1923 film)|1923.  The film was not entirely faithful to the book. 

The film was directed by Victor Fleming; it was his first sound film.  Gary Cooper, who had previously appeared in several silent films, was cast as the Virginian; it was his first leading role in a western, and his first talkie.   He was coached in the Virginians accent by Randolph Scott. 
 Sonora  and Lone Pine, California.    The train station scenes were filmed in Jamestown, California.  There was little studio shooting.  To shoot outdoor scenes, the filmmakers used blimped cameras (cameras with internal soundproofing), which were a recent innovation. 

The film featured the traditional song "Bury Me Not on the Lone Prairie", hummed and sung by Richard Arlen.   

==Techniques and style==
Rather than synchronize every sound on screen with a shown action, The Virginian treated sound as at times being independent of the action; this allowed for greater symbolism. The film also heavily used natural sounds, such as cattle.  This was facilitated by the outdoor shooting locations. 

==Reception==
  typecast as a man of few words, described by film historian Lee Clark Mitchell as a "yup and nope" actor.  Cooper later called it his favorite film. 

The Virginian has been well received, with a 100% "fresh" rating on Rotten Tomatoes as of March 2012, based on five reviews.  The review for Variety (magazine)|Variety noted that the film mixed various aspects of previous Westerns. The review described the scene where The Virginian must send his comrades to certain death "one of the most harrowing and vivid sequences ever before the lenses".  Eder praised the characterizations and use of sound, summarizing that the film was "a most worthwhile viewing experience".  Film historian Colin Shindler notes that The Virginian, along with Cimarron (1931 film)|Cimarron, was one of the first Westerns to handle sound well.  Film critic Emanuel Levy gives the film a B+, noting that Cooper showed moral conflict similar to his role in the later film High Noon (1952), which won an Academy Award. 
 1946 adaptation. film series, ran 9 seasons beginning in 1962.  The film also shaped the view of cowboys as chivalrous, slow-talking yet tough characters. 

==References==
;Notes
 

;Bibliography
 
* {{cite book
  | last = Crafton
  | first = Donald
  | title = The Talkies : American Cinemas Transition to Sound, 1926-1931
  | ref = harv
  | year = 1997
  | url = http://books.google.ca/books?id=KFB_oT-jupQC
  | isbn = 978-0-684-19585-8
  | publisher = Scribner
  | location = New York
  }}
* {{cite web
  | last = Eder
  | first = Bruce
  | url = http://www.allmovie.com/movie/the-virginian-v52866/review
  | publisher = Rovi
  | title = The Virginian
  | date = 
  | accessdate = March 15, 2012
  | archiveurl = http://www.webcitation.org/66BCyAjW1
  | archivedate = March 15, 2012
  | ref =  
  }}
* {{cite web
  | last = Levy
  | first = David
  | url = http://www.emanuellevy.com/review/virginian-the-1929-7/
  | work = davidlevy.com
  | title = Virginian, The (1929)
  | date = 
  | accessdate = March 16, 2012
  | archiveurl = http://www.webcitation.org/66CWTMUeL
  | archivedate = March 16, 2012
  | ref =  
  }}
* {{cite book
  | last = Mitchell
  | first = Lee Clark
  | title = Westerns: Making the Man in Fiction and Film
  | ref = harv
  | year = 1996
  | url = http://books.google.ca/books?id=SdS2ViQG1DEC
  | isbn = 978-0-226-53234-9
  | publisher = University of Chicago Press
  | location = Chicago
  }}
* {{cite book
  | last = Ferguson
  | first = Michael
  | title = Idol Worship : A Shameless Celebration of Male Beauty in the Movies
  | ref = harv
  | year = 2004
  | url = http://books.google.ca/books?id=R4f--vTO9LIC
  | isbn = 978-1-891855-48-1
  | publisher = STARbooks Press
  | location = Sarasota
  }}
* {{cite book
  | last = Shindler
  | first = Colin
  | title = Hollywood in Crisis: Cinema and American Society, 1929-1939
  | ref = harv
  | year = 1996
  | url = http://books.google.ca/books?id=qwXSBEz0fBAC
  | isbn = 978-0-415-10313-8
  | publisher = Routledge
  | location = New York
  }}
* {{cite web
  | last = 
  | first = 
  | url = http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=13034
  | publisher = American Film Institute
  | title = The Virginian
  | date = 
  | archiveurl = http://www.webcitation.org/66BDiy3HT
  | archivedate = March 15, 2012
  | accessdate = March 15, 2012
  | ref =  
  }}
* {{cite web
  | last = 
  | first = 
  | url = http://www.rottentomatoes.com/m/1022869-virginian/reviews/
  | publisher = Rotten Tomatoes
  | title = The Virginian
  | date = 
  | accessdate = March 15, 2012
  | ref =  
  }}
* {{cite web
  | last = 
  | first = 
  | url = http://www.allmovie.com/movie/women-love-diamonds-v117642/
  | publisher = Variety
  | title = The Virginian
  | date = 
  | accessdate = March 15, 2012
  | archiveurl = http://www.webcitation.org/66BBun9DO
  | archivedate = March 15, 2012
  | ref =  
  }}
* {{cite web
  | last = 
  | first = 
  | url = http://ahc.uwyo.edu/onlinecollections/exhibits/virginian/impact.htm
  | work = American Heritage Center
  | publisher = University of Wyoming
  | title = The Virginian Virtual Exhibit
  | date = 
  | archiveurl = http://www.webcitation.org/66BECrIBI
  | archivedate = March 15, 2012
  | accessdate = March 15, 2012
  | ref =  
  }}
* {{cite book
  | last = Tibbetts
  | first = John C
  | title = The American Theatrical Film : Stages in Development
  | ref = harv
  | year = 1985
  | url = http://books.google.ca/books?id=aZtHtxVVWZgC
  | isbn = 978-0-415-10313-8
  | publisher = Bowling Green State University Popular Press
  | location = Bowling Green
  }}
 

==External links==
 
*  
*  
*  
*   on Lux Radio Theater: November 2, 1936

 
 

 
 
 
 
 
 
 
 
 
 
 