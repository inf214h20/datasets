Arthur and the Revenge of Maltazard
{{Infobox film
| name           = Arthur and the Revenge of Maltazard
| image          = Arthur_and_the_vengeance_of_maltazard_ver4.jpg
| caption        = Teaser poster
| director       = Luc Besson
| producer       = Luc Besson Emmanuel Prévost
| writer         = Luc Besson Céline Garcia Stacy Ferguson Jimmy Fallon Snoop Dogg will.i.am Logan Miller Robert Stanton Lou Reed Penny Balfour Jean-Paul Rouve Ron Crawford
| music          = Éric Serra
| cinematography = Thierry Arbogast
| editing        = Julien Rey Narfia Entertainment Group
| distributor    = EuropaCorp (France) 20th Century Fox/The Weinstein Company (USA) Entertainment Film Distributors (UK)
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = English
| budget         = $63 million
| gross          = $78,520,547  
}} feature Films film with Arthur series, following Arthur and the Invisibles.

==Plot==
In 1963, protagonist Arthur stays with his grandparents for the holidays, during which the Bogo Matassalai (a fictitious African society) assign Arthur a series of tests including camouflage and environmental nonviolence. Having passed these tests, Arthur prepares to see the Minimoys to celebrate, until his father decides to take him and his mother back to the metropolis. When a spider gives Arthur a grain of rice containing a distress-call, which he believes has come from the Minimoys, he returns to his grandparents house, where the Bogo Matassalais attempt to give him Minimoy stature through a telescope fails, and they instead wrap him in vines of increasing tightness until he falls in a drop of sap into the Minimoy Maxs bar. En route to investigate the Minimoys condition, Arthur and Max rescue Betameche, who leads Arthur to the King. He then learns that Selenia is held by Maltazard, who is inspired to invade the human world by increasing his own size. Maltazard tricks the Minimoys into completing this design. The telescope itself is destroyed in the process, leaving Arthur trapped at his Minimoy size.

==Cast==
* Freddie Highmore as Arthur
* Mia Farrow as Daisy
* Ron Crawford as Archibald
* Robert Stanton as Armand Penelope Ann Balfour as Rose
* Jean Betote Njamba as Bogo Chief
* Bogos -
** Valery Koko Kingue
** Abdou Djire
** Bienvenu Kindoki
** Laurent Mendy
** Ibrahima Traore
** Aba Koita
* David Gasman as Mechanic
* Alan Fairbanks as Pump Attendant

===Voices===
* Freddie Highmore as Arthur
* Selena Gomez as Selenia. Gomez replaces Madonna (entertainer)|Madonna, who voiced the character in the original.The character was dubbed by French singer Mylène Farmer in the French version 
* Logan Miller as Jake
* Lou Reed as Maltazard. Reed replaces David Bowie, who voiced the character in the original.
* Jimmy Fallon as Betameche
* Snoop Dogg as Max
* will.i.am as Snow Fergie as Replay
* David Gasman as King/Bogo Chief
* Leslie Clack as Ferryman
* Alan Wenger as Mono Cyclop/DaVinci
* Barbara Weber Scaff as Miss Perlanapple
* Jerry di Giacomo as Proscuitto/Guard
* Paul Bandey as Miro/Unicorn Chief

==Home media==
On March 22, 2011   was a huge complete box office failure at the North American box office. However, the film was still released theatrically in Europe and also in the UK as Arthur and the Great Adventure.

==UK version==
Shortly after the third film   was released, the two films were edited into a single feature titled Arthur and the Great Adventure, which was given a theatrical release in the UK on December 24, 2010.

==Reception==
Both the original and UK versions received mixed to poor reviews. Arthur and the Revenge of Maltazard is widely thought of as the weakest film in the Arthur series. http://www.rottentomatoes.com/m/arthur_and_the_vengeance_of_maltazard/ 

==Video game==
To promote the film, a video game was released for PlayStation 3, Xbox 360, Nintendo Wii and Microsoft Windows. The game composes of mostly minigames and cutscenes which are loosely related to the plot of the film.

==Sequel==
In 2010, a sequel titled   was released.

==Notes==
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 