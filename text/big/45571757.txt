India's Daughter
{{Infobox film
| name           = Indias Daughter
| image          =
| image_size     =
| caption        =
| director       = Leslee Udwin
| producer       = Leslee Udwin
| writer         = Leslee Udwin
| based on       = 2012 Delhi gang rape
| narrator       =
| starring       =
| music          = Krsna Solo
| cinematography =
| editing        = Anuradha Singh
| distributor    = Berta Film
| studio         = {{plainlist| *Assassin Films
*Tathagat Films }}
| released       = 4 March 2015   
| runtime        = 58 minutes (58 min 18 sec)
| country        = United Kingdom
| language       = English language|English, Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Storyville series.    The film is based on the 2012 Delhi gang rape and murder of a 23-year-old woman who was a physiotherapy student.    
 viral with various shares on social media. On 5 March, the Indian government directed YouTube to block the video in India.      

== Background ==
 
 , Rajpath, December 2012]] Delhi gang emergency treatment surgeries in injuries she sustained in the assault.   
 Indian government for not providing enough protection to women. International media covered the incident only after persistent public protests.   
 sentenced to Juvenile Justice Act.   

== The film ==
 Storyville series. media outlets. viral with various shares on social media. On 5 March, the Indian government directed YouTube to block the video in India.    YouTube complied with the orders.     The film has generated a great deal of controversy in both India and worldwide.

== Interview ==
One of the convicted rapists serving life imprisonment, Mukesh Singh, was interviewed for the documentary. He said in the interview "When being raped, she shouldn’t fight back. She should just be silent and allow the rape. Then they’d have dropped her off after ‘doing her’, and only hit the boy."     He later added, "A girl is far more responsible for rape than a boy … A decent girl won’t roam around at nine o’clock at night … Housework and housekeeping is for girls, not roaming in discos and bars at night doing wrong things, wearing wrong clothes."   A report by the Navbharat Times has suggested that Mukesh Singh was paid   (about  ) to do the interview. According to the report, initially he had asked for  , but the amount was negotiated down and the sum was given to his family.     However, the filmmakers deny that he was paid for the interview.   

A. P. Singh, a defence lawyer in the case, was shown saying, “If my daughter or sister engaged in pre-marital activities and disgraced herself and allowed herself to lose face and character by doing such things, I would most certainly take this sort of sister or daughter to my farmhouse, and in front of my entire family, I would put petrol on her and set her alight.”  Asked later if he stood by those comments, he insisted that he did.   

== Reception ==
The film received positive reviews in the British press, from Sonia Faleiro in The Guardian  and Yasmin Alibhai-Brown in The Independent. 
 patriarchal attitudes that Indian women are expected to "behave themselves". She  says that the film is part of a wider White savior narrative in film|"white saviour" mentality.  She also said the film failed to profile Indian men who are "on the side of law and order and morality"  
 Because I Am a Girl, expressed their support to the director and film.   On 9 March, the documentary film screened during an event at Baruch College, New York City, which was attended by Meryl Streep, Chris Martin, Dakota Fanning and Frieda Pinto. 

The U.N. Secretary-General Ban Ki-moons spokesman Stéphane Dujarric stated on 5 March, "Im not going to comment on the unspeakable comments that were made by the person accused of raping this girl, but I think the secretary-general has spoken very clearly on the need to halt violence against women and on the need for men to get involved in halting violence against women and decrying it loud and clear every time it occurs."   

In an interview on 5 March 2015, the parents of the victim said that everybody should watch the documentary.    Bollywood figures including Anushka Sharma, Abhishek Kapoor, Sonal Chauhan, Twinkle Khanna and Punit Malhotra condemned the ban on the same day.  

Meenakshi Lekhi, a Bharatiya Janata Party spokesperson, opined that documentary makers didnt stick to the stated objective and are trying to benefit commercially by creating controversies. 

Avanindra Pandey, the friend of the victim who was injured in the attack, said, "The facts are hidden and the content is fake. Only Jyoti and I know what happened on that night and the documentary is far from truth." 

== Ban in India == media outlets soon afterwards.   The statements made by the convict created public outcry and screening issues in India.   

The Delhi Police filed a First Information Report (FIR) on 3 March against the filmmakers under Sections 505 (Statements conducing to public mischief), 504 (Intentional insult with intent to provoke breach of the peace), 505(1)(b) (With intent to cause, or which is likely to cause, fear or alarm to the public), 509 (Word, gesture or act intended to insult the modesty of a woman) of the Indian Penal Code and Section 66A of the Information Technology Act, 2000 (Punishment for sending offensive messages through communication service).    The Deputy Commissioner of Police (Economic Offences Wing) of New Delhi, Rajneesh Garg  said, "These excerpts of the interview as published are highly offensive and have already created a situation of tension and fear among women in society. Therefore, in the interest of justice and maintenance of public order, an application was made in court seeking restraining order from publishing, transmitting, uploading and broadcasting the interview."   
 viral with lamp on a black screen during the scheduled time slot. 
 Home Minister Minister of Parliamentary Affairs M. Venkaiah Naidu said in the same debate in the Parliament, "We can ban the documentary in India but there is a conspiracy to defame India and the documentary can be telecast outside. We will also be examining what should be done."    Rajya Sabha member Javed Akhtar said, "Its good that this documentary has been made. Crores of men in India have now come to know that they think like a rapist. If it is sounding dirty, they have to think."    (A crore is equal to ten million.)

The films director, Leslee Udwin, appealed to the Indian Prime Minister, Narendra Modi, to lift the ban in India on 4 March 2015.   

On 5 March, Tihar Jail authorities sent a legal notice to the filmmakers. They claimed that the filmmakers had violated the conditions under which they were given permission to film inside the prison. They claimed they had been screened a shorter version of the documentary and also said that they asked the producers to delete the interview with the convict.     Udwin denied this saying that she had submitted 16 hours of “raw, unedited footage”, but the review committee told her after watching three hours of it, “We can’t sit through all this, it’s too long.”  She said that she then submitted an edited version that was cleared.  On 5 March Udwin left India, presumably to avoid legal action.   
 show cause notice to the two lawyers,  M. L. Sharma and A. P. Singh, who had made misogynistic statements in the documentary.   
 Minority Affairs UPA government for allowing the film to be made. She said she fully supports Home Minister Rajnath Singhs stance on the subject.   
 rejecting an Indian male students internship because of the "rape problem in India", went viral phenomenon|viral.    The professor later apologised to the student.    The German Ambassador to India, Michael Steiner tried to contain damage and criticised the professor in an open letter.     Leipzig University released a statement saying that the internship was rejected due to lack of vacancies and the professors statements were taken out of context.   

A Public Interest Litigation was filed in the Delhi High Court to lift the stay order on the broadcast of the documentary. The petitioners claimed the ban violated freedom of expression under Article 19 of the Indian Constitution.    On 12 March 2015, the Court said the ban cannot be lifted as the appeals of the convicts are under trial in the Supreme Court of India.    However, it forwarded the case the case to the bench of the Chief Justice.   

== See also ==

* Rape in India
* Nirbhaya Fund, projects to utilize on dignity and ensuring safety of women in India
* Nirbheek, Indias first revolver for women
* Geeta and Sanjay Chopra kidnapping case, a 1978 case in which the death-row convicts were allowed to be interviewed
* Censorship in India

== References ==
 
== Further reading ==
*  
* 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 