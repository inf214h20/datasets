SEAL Team 8: Behind Enemy Lines
 
{{Infobox film
| name           = SEAL Team 8: Behind Enemy Lines
| image          = SEAL Team 8 Behind Enemy Lines.jpg
| caption        = Release poster
| director       = Roel Reiné
| producer       = David Wicht
| screenplay     = Brendan Cowles Shane Kuhn
| story          = Roel Reiné
| starring       = Tom Sizemore Langley Kirkwood Tanya van Graan Lex Shrapnel Bonnie Lee Bouman
| music          = Mark Kilian
| cinematography = Roel Reiné
| editing        = Radu Ion
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes
| country        = United States English
}}

SEAL Team 8: Behind Enemy Lines is a 2014   and  . The film was released on direct-to-video and Blu-ray on April 1, 2014. 

==Plot==
SEAL Team 8 is sent on an unsanctioned mission in Africa to find a secret mining operation and prevent weapons-grade uranium from falling into the hands of terrorists. While on an unsanctioned covert mission in Africa, a small team of S.E.A.L.s uncovers intelligence pointing to the imminent sale of a massive quantity of weapons grade uranium. Now, with no mission prep and zero support, they have less than 36 hours to fight their way past a ruthless warlords army guarding this secret mining operation, hidden deep in the treacherous Congo, in order to secure the yellowcake and expose the unknown buyer. As the story moves along, the asset that the S.E.A.L team was asked to secure, turns out to be the warlord. With three of the team members lost and one taken by the adversaries, Case fights his way into their nest to save his team-mate and give the enemy a withering end.

==Cast==
*Tom Sizemore as Ricks
*Langley Kirkwood as Lieutenant Parker
*Tanya van Graan as Collins
*Lex Shrapnel as Case
*Anthony Oseyemi as Jay
*Aurelie Meriel as Zoe Jelani
*Bonnie Lee Bouman as Officer
*Eugene Khumbanyiwa as Arms Cache Manager

==Production==
The film takes place and was shot in South Africa. 

==Release==
The film was released on April 1, 2014. 

==See also==

*List of films featuring the United States Navy SEALs

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 