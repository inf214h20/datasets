Chattambikkavala
{{Infobox film 
| name           = Chattambikkavala
| image          =
| caption        =
| director       = N Sankaran Nair
| producer       =
| writer         = Muttathu Varkey
| screenplay     = Muttathu Varkey Sathyan Srividya Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = BA Chidambaranath
| cinematography = ENC Nair
| editing        = Gopalakrishnan
| studio         = Sreekumar Productions
| distributor    = Sreekumar Productions
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed by N Sankaran Nair . The film stars Sathyan (actor)|Sathyan, Srividya, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by BA Chidambaranath.   

==Cast==
  Sathyan
*Srividya
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Jose Prakash
*Mani
*Shanthi
*Bahadoor
*Fareed
*K. P. Ummer
*Kalaikkal Kumaran Meena
*S. P. Pillai
*Sarasamma
*Vasantha
 

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anjanakkulir neela || K. J. Yesudas, S Janaki || ONV Kurup || 
|-
| 2 || Anthimalarkkili koodananju || K. J. Yesudas, S Janaki || ONV Kurup || 
|-
| 3 || Mayilppeeli Mizhikalil || K. J. Yesudas, S Janaki || ONV Kurup || 
|-
| 4 || Oru Hridayathalikayil || P Jayachandran, P. Leela || ONV Kurup || 
|-
| 5 || Oru Muri Meeshakkaaran (Kuruviyilla Kuruvikkooday) || LR Eeswari, Gnanaskandan || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 