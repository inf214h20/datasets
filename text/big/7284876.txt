The Taming of Sunnybrook Nell
{{Infobox film
| name           = The Taming of Sunnybrook Nell
| image          =
| caption        =
| director       = Sydney Ayres
| producer       =
| writer         = Harry Wulze (Story)
| starring       = William Garwood Louise Lester
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States English intertitles
}}
 1914 United American silent silent short short drama film directed by Sydney Ayres, written by Harry Wulze and starring  William Garwood, Louise Lester and Vivian Rich.

==Cast==
*William Garwood as Steve, a woodcutter
*Louise Lester as Mrs. Durkin
*Vivian Rich as Sunnybrook Nell, his daughter Jack Richardson as Clifford Durkin
*Harry von Meter as Old Clon, a mountaineer

==External links==
* 
*{{cite journal
 | last = Hunt
 | first = Elaine
 | title = The Taming of Sunnybrook Nell
 | journal = Photoplay
 | volume = VI
 | issue = 6
 | pages = 115ff
 | publisher = Cloud Publishing Co.
 | location = Chicago, Ill
 | date = November 1914
 | url = https://archive.org/stream/PhotoplayMagazineNov.1914/Photoplay1114#page/n113/mode/2up
 | accessdate = 25 December 2013}}

 
 
 
 
 
 
 
 
 


 