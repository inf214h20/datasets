My Favorite Blonde
{{Infobox film
| name           = My Favorite Blonde
| image          = My_Favorite_Blonde_1942_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Sidney Lanfield Paul Jones
| screenplay     = {{Plainlist|
* Don Hartman Frank Butler
}}
| story          = {{Plainlist|
* Melvin Frank
* Norman Panama
}}
| starring       = {{Plainlist|
* Bob Hope
* Madeleine Carroll
}}
| music          = David Buttolph
| cinematography = William C. Mellor
| editing        = William Shea
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
My Favorite Blonde is a 1942 American comedy film directed by Sidney Lanfield and starring Bob Hope and Madeleine Carroll.    Based on a story by Melvin Frank and Norman Panama, the film is about a vaudeville performer who gets mixed up with British and German secret agents in the days just before Americas entry into World War II. The film features an uncredited cameo appearance by Bing Crosby.

==Plot Summary==

When a British secret agent is murdered in the line of duty, agent Karen Bentley inherits the mission from her partner. The mission is to deliver a flight plan for a hundred American bomber planes to a British agent in Chicago. The plans are hidden in a small medallion of a scorpion that Karen wears.

Karen arrives to New York City from Europe by ship and escapes the clutches of enemy agents by hiding in a variety theatre. To improve her chances of getting away and get a good cover, she charms an actor named Larry Haines, who performs a small act called "Percy" involving his penguin. Larry tells her that he and his penguin are heading west to Hollywood to appear in a film. They have a contract paying $500 a week for Percy and $30 for him as his trainer.

Karen accompanies Larry to the train and plants the medallion on him before he boards the train. Unaware of what he is now carrying, Larry leaves New York, and the German agents, Mme. Stephanie Runick and Dr. Hugo Streger, are also on board the same train, keeping a close eye on Larry. The agents manage to scare up Larry with their odd behavior, and in Albany, Karen boards the train.

Larry meets Karen and finds her too a little odd, since she didnt board the train back in New York with him. When the train stops for three hours in Chicago, Karen manages to steal Larrys suitcase, which now contains the jacket where the medallion is hidden. Larry follows Karen and the suitcase to an address where she is supposed to meet an agent, but Karen finds the agent murdered and has to change her plans. She is instructed to continue to Los Angeles instead.

Since Larry has already seen her and the dead body, Karen reveals her true identity to him and asks his help. When the German agents are watching, they fake a domestic argument between the two of them, where Larry is violent against Karen. The police arrive to the scene and they are both arrested for disturbing the peace. While under arrest, the two of them are transported safely out of the building and past the German agents.

Karen and Larry are released after they make up in the police patrol car. They dont get very far until the police chase them again, because the German agents anonymously tell the police they are responsible for murdering the British agent. The murder is on the news and Larry is named the "love slayer".

During their frantic escape from the police, Karen and Larry fall in love. They find a place to hide at the top of Union Hall for the night, but the morning after they get on a bus headed out of the city on a sightseeing tour. They hijack the bus and then steal a plane to fly to Los Angeles. When the plane is out of fuel they land in the middle of nowhere and are arrested again, this time for stealing food on a farm.

In jail they are recognized as the couple killing the British operative, but they escape and are chased again. They manage to jump a freight train to Los Angeles. The address Karen was supposed to visit turns out to be a funeral parlor, but the Germans have beaten them to it and taken the British agent hostage. Karen and Larry manage not to get caught though and flee the scene to a U.S. Air base nearby, where they take refuge and deliver the secret bomber plans. 

==Cast==
* Bob Hope as Larry Haines
* Madeleine Carroll as Karen Bentley
* Gale Sondergaard as Madame Stephanie Runick
* George Zucco as Dr. Hugo Streger
* Lionel Royce as Karl
* Walter Kingsford as Dr. Wallace Faber
* Victor Varconi as Miller
* Otto Reichow as Lanz
* Esther Howard as Mrs. Topley
* Edward Gargan as Mulrooney James Burke as Union Secretary
* Charles Cane as Turk OFlaherty
* Crane Whitley as Ulrich, the Henchman
* Dooley Wilson as Porter
* Milton Parsons as Mortician
* Bing Crosby in cameo appearance as Man Outside Union Hall (uncredited)   

==See also==
*My Favorite Brunette (1947) with Bob Hope and Dorothy Lamour
*My Favorite Spy (1951) with Bob Hope and Hedy Lamarr

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 