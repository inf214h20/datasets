The Dragon Tamers
 
 
{{Infobox film
| name           = The Dragon Tamers
| image          = https://en.wikipedia.org/wiki/File:The_Dragon_Tamers_poster.jpg
| image_size     = 638px × 813px
| caption        = 
| Phonetic Title = Nu zi tai quan qun ying hui
| simplified     = 女子跆拳群英會
| director       = John Woo   Chin Wei Chun (Assistant Director) 
| producer       = Raymond Chow	
| writer         = John Woo
| starring       =  	James Tin Jun, Carter Wong Ka-Tat, Yeung Wai, Gam Kei-Chu, Chin Chang-Shou, Ji Han-Jae, Chan Chuen, Hsu Hsia, Yuen Wah, Chik Ngai-Hung, Lee Ye-Min, Martin Chui Man-Fooi, Jang Jeong-Kuk, Ina Ryoko, Kobayashi Chie, Saijo Nami, Hara Keiko Park Seong-Jae, Lee Dae-Yeob, Woo Yeon-Jeong, Tam Bo, Kim Wang-Kuk, Lee Joo-Keun, Ho Choi-Yat
| music          = Joseph Koo
| cinematography = Li Cheng-Chun
| Stunt Director = Chan Chuen
| editing        = Peter Cheung Yiu-Chung and Chin Chang-Chun 	
| distributor    = Golden Harvest Company
| released       =  
| runtime        = 107 min
| country        = 
| language       = Mandarin Chinese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Dragon Tamers, also known as Belles of Taekwondo ( ), is a 1975 Hong Kong action film.

Long before his development of the "heroic bloodshed" gangster films genre, John Woo was a young director struggling to make it in the Hong Kong film industry. The Dragon Tamers was his second film as a director (his debut film was The Young Dragons (1974)).

This action picture from Golden Harvest was filmed in Korea. Stunt choreographer, Chuan Chen had been a recognized talent at Golden Harvest, having worked on many of their films. His assistant on this particular production was a very young and hungry, Jackie Chan.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 
 