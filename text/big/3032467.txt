Vampyros Lesbos
{{Infobox film
| name           = Vampyros Lesbos
| image          = Vampyros-lesbos-poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = German film poster
| director       = Jesús Franco
| producer       = Artur Brauner Karl Heinz Mannchen 
| writer         = Jaime Chávarri Jesús Franco  Paul Müller
| music          = Jesús Franco Manfred Hübler Siegfried Schwab
| cinematography = Manuel Merino 
| editing        = Clarissa Ambach 
| studio         = Fénix Films CCC Telecine Film 
| distributor    = 
| released       =     
| runtime        = 89 minutes 
| country        = West Germany Spain Browning, 2010. p.183 
| language       = German 
}}

Vampyros Lesbos ( ) is a 1971 West German-Spanish horror film directed and co-written by Jesús Franco. The film stars Ewa Stroemberg  as Linda Westinghouse, an American who works in a Turkish legal firm. Westinghouse has a series of erotic dreams that involve a mysterious vampire woman who seduces her before feeding on her blood. When she travels to an island to settle an inheritance, Linda recognizes a woman as the vampire from her dreams.

The film was shot in 1970 in Turkey. It was a popular success in theaters in Europe on its release and was the first film to have a more psychedelic score for a Franco film and the first to have a lesbian theme as a prominent feature of the film. The films score became popular in the mid-1990s when it was included on the compilation Vampyros Lesbos: Sexadelic Dance Party, an album that became a top ten hit on the British Alternative charts.

== Plot == Count Dracula. After Linda begins to feel dizzy from drinking wine, Nadine takes her to a room where the two have sex and Nadine draws blood from Lindas neck. Linda later and finds Nadine motionless in a swimming pool and faints. 

The next day, Arga (Heidrun Kussin) appears in mental distress at a hospital, where she claims to have visions of Nadine. She is under the care of Dr. Seward (Dennis Price) who then treats her new patient, Linda, who does have any memory of what she encountered with Nadine. At Nadines home, she appears alive and recounts to her servant Morpho (José Martínez Blanco) about how she became a vampire and her obsession with Linda who she wishes to become a vampire. Nadine uses her powers to contact Linda to return to her island where the two drink blood and have sex. On her return to hospital, Dr. Seward informs Nadine that to remove herself from the vampires curse she must split the vampires head with an axe or pierce it with a pole.

Linda is then kidnapped by Memmet. Lindas boyfriend Omar (Andrés Monales) begins to search for her. Nadine later arrives at the asylum to have Linda return with her where she meets Dr. Seward. Dr. Seward admits that he only attempted to help Linda in order to draw Nadine to him so he can become a vampire. Nadine refuses and has Morpho kill him. As Omar is searches for Linda, she is told by Memmet that all woman, including his wife Arga, who return from the island become insane which has driven him to kill various women around the island. Linda manages to kill Memmet with a saw and escapes to find Nadine. She finds Nadine at her home near death in desperate need of Blood to survive. Linda ignores Nadines plea and bites her neck and stabs her with a pole through her left eye. Morpho commits suicide and Linda is found by Omar who tries to convince Linda the whole experience was a dream.

== Cast ==
 
*Ewa Strömberg as Linda Westinghouse
*Soledad Miranda as Countess Nadine Carody
*Andrés Monales as Omar
*Dennis Price as Dr. Alwin Seward Paul Müller as Dr. Steiner
*Heidrun Kussin as Agra
*Michael Berling as Dr. Sewards assistant
*Beni Cardoso as Dead woman (uncredited)
*Jesús Franco as Memmet (uncredited)
*José Martínez Blanco as Morpho (uncredited)
 

==Production==
Vampyros Lesbos was filmed in Turkey between June 1, 1970 and July 10, 1970.  Shipka 2011, p. 203.  Franco applied film devices that were used in his previous film such as long strip club sequences and female protagonists while the lesbian subtext was more prominent in this film than any previous work.  The music score also differs from the jazz soundtracks of his previous films with a more psychedelic music influenced soundtrack.  The soundtrack was composed by Manfred Hübler, Siegfried Schwab and Jesús Franco who credited himself under the alias of David Khune.  The film went under several titles before being released as Vampyros Lesbos including Das Mal des Vampirs (Evil of the Vampires) and Im Zeichen der Vampire (Mark of the Vampire).  Less than a month after finishing production on Vampyros Lesbos, Franco began working on his next film She Killed in Ecstasy (1971). 

==Release==
Vampyros Lesbos was released in July 15, 1971 in Germany and in Spain in 1973 where it was a popular with audiences in Europe.  Shipka 2011, p. 205.  The film was released on DVD by Synapse Video on January 4, 2000.    Image Entertainment released the film on December 27, 2000 on DVD. 

A remake of Vampyos Lesbos directed by Matthew Saliba was released in 2008. The film follows the story of Francos film. 

==Reception==
 
Total Film gave the film three stars out of five, noting that "Despite (or perhaps because of) the hilariously leaden acting, dull script and amateurish direction, this film still exerts a certain fascination."  Jonathan Rosenbaum of The Chicago Reader gave the film a negative review, comparing director Jesús Franco to Ed Wood.  Slant Magazine gave the film a positive review of three and half stars out of four, finding the film "effortlessly dreamlike" as well as praising the soundtrack.  Film 4 gave the film a mixed review, noting that "you never come to Francos films (over 150 of them) for the plots, but his dreamy, unsettling direction does develop the central tragedy of Carodys love for Westinghouse." as well as praising the films soundtrack. 

In his 2009 book The Pleasure and Pain of Cult Horror Films: An Historical Survey, Bartomiej Paszylk took umbrage with some of the high-brow critics of the film, though ultimately acquiescing to its shortcomings, "Truth be told, Francos vampyros are far more interested in being lesbos than in drinking human blood, but the movie is so mesmerizing and so outright sexy that you really shouldnt mind that.   

==Soundtrack==
{{Infobox album  
| Italic title  = 
| Name          = Vampyros Lesbos: Sexadelic Dance Party 
| Type          = Soundtrack
| Longtype      = 
| Artist        = Vampires Sound Incorporation
| Cover         = Vampyros-soundtrack.jpg
| Border        = 
| Alt           = 
| Caption       = 
| Released      = 1995
| Recorded      = 
| Genre         = Film music, psychedelic
| Length        = 48:46
| Language      = 
| Label         = Motel
}}

The soundtrack to Vampyros Lesbos was released as Vampyros Lesbos: Sexadelic Dance Party on  .   
The album was released during a period where there was a resurgence of interest in   by American director Quentin Tarantino.  The album is dedicated to actress Soledad Miranda.   

The soundtrack was a top 10 hit on the British Alternative charts on its release over 20 years after the film was released.  On September 29, 1997, a remix album titled The Spirit of Vampyros Lesbos was released. The album was a collection of remixes from various electronic artists including Two Lone Swordsmen, Cristian Vogel and Alec Empire who released their own mixes of the films soundtrack. 

Allmusic gave the album a negative of three stars out of five referring to the albums music as "excruciating" as well noting that a track on the album is "built on a shameless ripoff of the "(I Cant Get No) Satisfaction|Satisfaction" guitar riff".  Entertainment Weekly gave the album a B+ rating, opining that it was "not for cheese lovers only." 

===Track listing===
{{tracklist
| all_writing     =Manfred Hübler and Siegfried Schwab 
| title1          = Droge CX 9
| length1         = 5:11
| title2          = The Lions and the Cucumber
| length2         = 5:10
| title3          = Theres No Satisfaction
| length3         = 3:10
| title4          = Dedicated to Love
| length4         = 2:32
| title5          = Peoples Playground Version A
| length5         = 0:50
| title6             = We Dont Care
| length6         = 5:20
| title7          = Peoples Playground Version B
| length7         = 1:17

| title8          = The Ballad of a Fair Singer
| length8         = 4:35

| title9          = Necronomania
| length9         = 2:09

| title10         =Kama Sutra
| length10         = 4:03

| title11          = The Message
| length11         = 3:21

| title12          = Shindai Lovers
| length12         = 4:21

| title13          = The Six Wisdoms of Aspasia
| length13        = 4:20

| title14          = Countdown to Nowhere
| length14         = 2:27
}}

==Notes==
 

== See also ==
 
*List of horror films of 1971
* 
*List of Spanish films of 1971
*Vampire film

==References==
*{{cite book
 | last= Browning
 | first= John Edgar
 |author2=Picart, Caroline Joan 
  | title= Dracula in Visual Media:Film, Television, Comic Book and Electronic Game Appearances, 1921-2010 McFarland
 |year= 2010
 |isbn= 0786433655
}}
*  

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 