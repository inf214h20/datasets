Gopi Kishan
{{Infobox film
| name           = Gopi Kishan 
| image          = Gopi Kishan DVD cover.png
| caption        = Promotional Poster
| director       = Mukesh Duggal
| producer       = Mukesh Duggal
| writer         = Anees Bazmee
| starring       = Sunil Shetty Shilpa Shirodkar Karishma Kapoor
| music          = Anand-Milind
| cinematography = Akram Khan
| editing        = A. Muthu
| distributor    = 
| released       =  2 December 1994
| runtime        = 161 mins
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Gopi Kishan is 1994 Hindi Movie directed by Mukesh Duggal and starring Sunil Shetty in a double role, supported by Shilpa Shirodkar and Karishma Kapoor. Other cast include Suresh Oberoi, Aruna Irani, Mohan Joshi, Shammi, Satyendra Kagoor, Mushtaq Khan. The film is the remake of K. Bhagyarajs hit Tamil film Avasara Police 100.

== Summary ==

Kishan (Sunil Shetty) is a hardened criminal who is in search of his father, after knowing that his father is still alive. Suraj Malhotra (Suresh Oberoi), a dreaded gangster, is supposedly the father of Kishan, whom Kishen wants to avenge for deserting his mother.

Gopi, on the other hand, is a cop who cannot even shoot a bullet straight. Other than their faces(and the fact that none of them knows about the existence of other), he & Kishan have nothing in common. But when Kishan learns about Gopi, he decides to use Gopis identity to his advantage. This results in Gopi suddenly becoming promoted & famous. But the stories of Gopi & Kishen are going to merge in a way that none of them  would have ever imagined.

== Plot ==

Kishan, is a hardened criminal who returns home after completing 14 years imprisonment for murder. Kishan had killed a man in his childhood, when the latter tried to molest his mother. Kishan thinks his father is dead, but he soon learns that his mother has kept truth under wraps. He learns that his father Suraj Malhotra was a jeweller who killed his partner & ran away with some precious jewels. She tells Kishan that she never saw Suraj after that & now he is a dreaded gangster in the underworld.

Kishan decides to avenge his father for all his wrongdoings. He singles out Sawant, a powerful man of Suraj & decides to strike on him. Meanwhile, Gopi, a doppelgänger of Kishan, works as a constable in police force. Gopi has a doting mother, a wife and a kid, but isnt taken seriously as he lacks the guts to become anything worthwhile. Kishan spots Gopi and decides to use him for his purposes. Kishan starts bumping off the goons while Gopi starts getting the credit.

Soon, Gopis fortune changes, while Kishan succeeds in furthering his motives. Sadly, Seema, the Commissioners daughter, falls in love with Gopi, unaware of his marital status. One day, Kishan manages to sneak into Malhotras lair. He holds Malhotra on gunpoint & shows him the photo of his mother. He learns that Sawant is the kingpin and Suraj the pawn, rather than other way around. Suraj tells Kishan that Sawants men killed Surajs partner for the jewels. Suraj somehow scooted off with the money & hid it in the basement of a construction site.

Sawant caught Suraj & told him that the latter has been convicted for the formers crimes. Sawant also tells him that his wife has committed suicide & his son would be killed too, if Suraj does not tell the location of the jewels. Suraj does not tell the location, but to save the infant from death, takes the blame on himself. Upon this, Kishan reveals his true identity. Both realize that Sawant passed some other child as Surajs child. Kishan learns that the place the money was hidden is the police headquarters now.

Kishan decides to retrieve the loot, but both Gopi & Seema realize the truth. Now, Gopi starts hunting for Kishan. Meanwhile, Kishan has infuriated Sawant too by killing some of his goons. Kishans mother, whom Kishan had left to find his father, comes to find him. She runs into Gopi, mistaking him for Kishan. Gopi tricks her & abducts her. But Gopi is in for a surprise when Kishans mother & Gopis mother turn out to be acquaintances.

Gopis mother reveals that when Kishans mother was in labour, she delivered twins. Gopis mother was actually childless, so she took one of the kids. Gopi is in a quandary as he cannot arrest his newfound brother, but decides that he has to do this, one way or other. Meanwhile, Kishan has revealed the truth to Seema, who agrees to help him in retrieving the score. Sawant succeeds in nabbing Kishan. Unable to see his son tortured, Suraj spills the beans.

Now, Gopi enters the den and pretends to be Kishan, claiming that Sawant has actually kidnapped the gullible Gopi. One by one, all the villains are unmasked. Suraj is delighted to meet his wife, while both Gopi and Kishan start bringing Sawants empire to dust. After killing Sawants son, which ironically provides poetic justice in this case, all the goons are rounded off. Suraj is finally united with his extended family in the end.

==Soundtrack==

Anand-Milind and Sameer (lyricist) teamed up with director Deepak Shivdasani after Baaghi and Pehchaan, to deliver another hit musical score. Songs like Hai huku and Batti Na Bujha topped the countdowns and the music topped the charts when released. It was among the best selling albums of 1994. 

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yeh Ishq Hai Kya"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Haye Hukku Haye Hukku Haaye Haaye"
| Kumar Sanu, Poornima
|-
| 3
| "Chhatri Na Khol Barsaat Mein"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Mera Mehboob Aayega" Poornima
|-
| 5
| "I Love You"
| Kumar Sanu, Alka Yagnik
|-
| 6
| "Batti Na Bujha"
| Poornima
|}

==Snippets==

* This is the first double role project of Sunil Shetty, one an action role and the other, a comic role.
* This is the first time Sunil Shetty done a comic role before his teaming with Priyadarshan.
* The film was a box - office success of 1994

==References==
 

== External links ==
*  

 
 
 
 
 
 