Deaf Jam
 
{{Infobox film
| name           = 
| image          = Deaf Jam Documentary Theatrical Release Poster.png
| alt            = 
| caption        = Theatrical Release Poster
| director       = Judy Lieff
| producer       = {{Plainlist | Judy Lieff
*Steve Zeitlin }}
| writer         = 
| starring       = {{Plainlist | Aneta Brodski
*Tahani Salah }} 
| music          = 
| cinematography = {{Plainlist | Melissa Donovan 
* Claudia Raschke-Robinson}}
| editing        = Keiko Deguchi
| studio         = 
| distributor    = CINEPHIL
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = {{Plainlist | English
*American Sign Language }} 
| budget         = 
| gross          = 
}} American Sign Language poetry. When Aneta, an Israel-born, ASL poet, eventually meets Tahani, a Palestinian, spoken word poet, the two begin to collaborate, creating a new form of poetry that gains recognition in Deaf and hearing communities alike. 

Deaf Jam premiered on   for the Best Work of the Youth Category. 
"Deaf Jam" is a co-production of Made-By-Hand, LLC and the Independent Television Service (ITVS).

== Plot ==

Aneta Brodski is first exposed to American Sign Language (ASL) poetry through an after-school program at the Lexington School for the Deaf in Queens. Aneta is an Israeli immigrant, and unlike many of her classmates, was born to an all-deaf family. She is dedicated to the study ASL poetry, and by the end of the first year, has begun to master the three-dimensional form and cultivate a strong poetic voice.
 slam team, an unprecedented move for a member of the deaf community. http://cdn.itvs.org/deaf_jam-discussion.pdf  Although Aneta is proud of her deafness, she explains that she does not wish to be defined by it, but would like to have the opportunity to express herself in spheres beyond the deaf community. http://www.pbs.org/independentlens/deaf-jam/film.html  

Through her activity at Urban Word, Aneta eventually meets Tahani Salah, a Palestinian spoken word poet, and the two young women begin to collaborate, creating a new form of slam poetry that transcends the politics of their respective national origins. After an arduous process of synthesis, Aneta and Tahani are invited to present their poetry at Bob Holman’s Bowery Poetry Club.  

== Release ==

Deaf Jam premiered on PBSs Independent Lens program, in the 2011-2012 season,  and has broadcast internationally on the German BRs magazine "Sehen statt Hören,"  Koreas EBS, http://www.visitseoul.net/en/article/article.do?_method=view&art_id=57227&lang=en&m=0003001006003&p=06  Swiss Television and the Taiwanese Ski Digi Entertainment Co. http://www.d-word.com/documentary/509-Deaf-Jam 
 Documentary Edge Film Festival New Zealand,  35th Goteborg International Film Festival in Sweden,  San Sebastopol Documentary Film Festival,  San Diego Jewish Film Festival,  Atlanta Jewish Film Festival,  New York Jewish Film Festival,  San Francisco Independent Film Festival,  Houston Jewish Film Festival,  Pittsburg Jewish Film Festival,  Westchester Jewish Film Festival at the Jacob Burns Film Center,  Arts in Action Film Festival in Australia,  EBS International Documentary Festival in Korea,  Maine Deaf Film Festival,  Project Youth View in Oakland, California,  One World Arts Festival in Canada,  Festival CIneSordo in Ecuador,  CineDeaf Roma in Italy,  and the Toronto Jewish Film Festival. 

== Reception ==
 Japan Prize for the Best Work of the Youth Category, in 2012. It was also awarded the prize for Best Documentary at the Greenpoint Film Festival  and The Greater Reading Film Festival,  as well as Best Film at The Irish Deaf Festival in Dublin.  In 2013, Deaf Jam was invited to participate in the American Film Showcase, an initiative by the State Departments Bureau of Educational and Cultural Affairs that sends filmmakers to selected countries to represent an independent view of American culture and society. 

The film has received positive reviews in the critical press. Reporting on the Thessaloniki Documentary Film Festival in the Huffington Post, Karin Badt called Deaf Jam a "riveting documentary" and remarks that "the deaf Israeli teenager at the center of the film, Aneta Brodski, is so charmingly expressive."  Reporting for Variety, Boyd Van Hoeij wrote, "ASL poetry relies heavily on visuals and movement, and Lieffs film follows suit, with lensing on a variety of digital formats, colorful tech wizardry and fast-paced cutting. A hip soundtrack further adds to the pics street cred for hearing auds." 

== References ==

 

== External links ==
* 
*  at PBS
* 

 
 
 
 