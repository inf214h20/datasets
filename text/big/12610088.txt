Jallaad
{{Infobox film
| name           = Jallaad
| image          = Jallaad.jpg
| image_size     = 
| caption        = DVD cover
| director       = T L V Prasad
| producer       = Rajiv Babbar
| writer         = 
| narrator       =  Rambha Madhoo Kader Khan Shakti Kapoor Sonu Walia Prem Chopra Yunus Parvez Sameer   (Lyrics)  
| cinematography = 
| editing        = 
| distributor    = 
| released       = 15 September 1995
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
Jallad (English:Executioner) is a 1995 Indian action film directed by T L V Prasad released on 15 September 1995. The film stars Mithun Chakraborty in a double role, which also won him the various awards under the Best Villain category. The film was the remake of Tamil film Amaithipadai starring Satyaraj.

==Cast==
*Mithun Chakraborty ... Amavas a.k.a. Vijay Bahadur/ Inspector Kranti
*Moushumi Chatterji ... Tara
*Madhoo ... Gayatri 
*Kader Khan ... K.K. 
*Shakti Kapoor ... Shakti Jackson 
*Sonu Walia
*Prem Chopra
*Yunus Parvez
*Rambha ... Krantis Lover
*Tiku Talsania ... Police Inspector

==Plot==

A young, dashing, and honest Police Inspector, who lives with Mr. and Mrs. Prasad, goes to a small town in Southern India to be introduced to a prospective bride, Koyal, the daughter of Gopinath. The Police Inspector falls head over heels in love with Koyal and wants to marry her. Koyal too falls in love with him. When the time comes for finalizing their marriage, a man comes forward alleging that the Police Inspector is not who he claims to be, but a man who is a bastard, born out of an unknown biological father. This leads to the postponement of the marriage, and the Inspector questioning Prasad and his wife as to who he really is and who his father was. Prasad starts recounting his mother, Gayetris, life, the fact that Amavas drugged her(Gayetri) and had sex with her but refuses to acknowledge her after he winning a local election, a man who was selected by a shady politician to lure the innocent public to vote for him - so that he can plunder the nation to no end. Watch as this story takes you back in time, showing the greed and avarice that can encompass even the simplest of men - making him turn into a demon.

==Reception==

The film was a Success at the Box-Office establishing Rajeev Babbar as a Producer. The film was the remake of Tamil Hit Amaithipadai starring Satyaraj

==Awards==
* Filmfare Best Villain Award - Mithun Chakraborty
* Star Screen Award Best Villain - Mithun Chakraborty

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Jallaad

==External links==
* 

 
 
 
 
 

 
 