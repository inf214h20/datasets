Masks (1929 film)
{{Infobox film
| name           = Masks
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Meinert
| producer       = 
| writer         = Guido Kreutzer (novel)   Rudolf Meinert
| narrator       = 
| starring       = Karl Ludwig Diehl   Trude Berliner  Marcella Albani   Charles Willy Kayser
| music          = 
| editing        = 
| cinematography =  Günther Krampf
| studio         = Omnia-Film
| distributor    = Deutsche Lichtspiel-Syndikat 
| released       =  
| runtime        = 79 minutes
| country        = Weimar Republic Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} silent crime film directed by Rudolf Meinert and starring Karl Ludwig Diehl, Trude Berliner and Marcella Albani. It was the second film made by Meinert featuring the detective hero Stuart Webbs following The Green Monocle (1929). 

==Cast==
* Karl Ludwig Diehl – Stuart Webbs 
* Trude Berliner – Mary 
* Marcella Albani – Elyane 
* Charles Willy Kayser – Bankier Clifford 
* Hans Schickler – Jankins 
* Jean Murat – Jonny 
* Betty Astor – Goldelse 
* Borwin Walth – Kommissar Black 
* Gerhard Dammann – Wirt Pitt 
* Oskar Homolka – Breitkopf 
* Robert Klein-Lörk – Charly

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2007.

==External links==
* 

 

 
 
 
 
 
 
 


 
 