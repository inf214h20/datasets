Poor No More
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Poor No More
| image          =
| caption        =
| director       = Bert Deveaux Suzanne Babin
| producer       = David Langille Suzanne Babin Mary Walsh
| music          =
| cinematography =
| editing        =
| studio         = Deveaux Babin Productions
| distributor    =
| released       =  
| runtime        = 53 minutes
| country        = Canada
| language       = English
| budget         =  550,000
| gross          =
}} Mary Walsh, late 2000s recession and looks at solutions for Canadas working poor. The film follows two working Canadians and Mary Walsh to Ireland and then to Sweden, where they take a closer look at how the Nordic Model has affected living standards for the Swedish.

The characters are Durval Terceira and Liquor Control Board of Ontario employee Vicki Baier. Each compares their standard of living in Canada with the standard of living for the Swedes and Irish. The film discusses Canadas need for better social services in regards to unemployment insurance, universal health care, pensions, and full-time union-protected jobs. Poor No More shows how, in Sweden, the labour movement works hand-in-hand with corporate entities to ensure a stronger social safety net offering universal child care, and income and training systems for the unemployed.

A rough cut sneak preview of Poor No More was screened in several provinces as part of the first Canadian Labour International Film Festival (CLIFF), held in November 2009.

==External links==
*  
*http://www.rabble.ca/news/2009/11/poorbut-not-broken
*http://www.theglobeandmail.com/news/arts/television/mary-walsh-is-sitting-pretty/article1390875/
*http://www.thesudburystar.com/ArticleDisplay.aspx?e=2196227
*http://www.orilliapacket.com/ArticleDisplay.aspx?e=2180919
*http://www.bclocalnews.com/vancouver_island_central/cowichannewsleader/entertainment/75208232.html?period=W&mpStartDate=12-03-2009&
*http://www.canada.com/Labour+film+fest+makes+history/2276939/story.html
*http://www.harbourliving.ca/event/canadian-labour-international-film-festival-nanaimo
*http://www.opseu.org/lbed/2009bargaining/bargainingbulletin/2009-bargaining-bulletin-june-12.htm
*http://www.ccsd.ca/events/2008/agm.pdf
*http://hamiltonlabour.ca/doc.php?document_id=129
*http://www.nationtalk.ca/modules/news/article.php?storyid=26550
*http://www.georgianc.on.ca/news/events/orillia-hosts-inaugural-labour-relations-film-festival-on-nov-28
*http://www.rnao.org/Page.asp?PageID=122&ContentID=3121&SiteNodeID=398
*http://webcache.googleusercontent.com/search?q=cache:C5LHPvKfPH8J:www.skyworksfoundation.org/current_issues/cf_download.cfm%3Ffile%3DPoverty%2520Jan-Feb%252009.pdf%26path%3D%255C+%22poor+no+more%22+film&cd=194&hl=en&ct=clnk&gl=ca

 
 
 
 
 
 
 
 
 
 