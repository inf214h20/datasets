The Salamander (1981 film)
{{Infobox film
| name = The Salamander
| image = The Salamander (1981 film).jpg
| director = Peter Zinner
| writer =
| starring = Franco Nero Anthony Quinn
| music = Jerry Goldsmith
| cinematography = Marcello Gatti
| editing =
| producer =
| distributor =  
| released =  
| runtime =
| awards =
| country =
| language = English
| budget =
}}
The Salamander  (also known as Morris Wests The Salamander) is a 1981 thriller film directed by Peter Zinner, at his directional debut.  The film is based on a novel with the same name by Morris West. 

==Plot==
Policeman Dante Matucci investigates a series of murders involving people in prominent positions. Left behind at each murder scene is a drawing of a salamander. As the body count grows he sees a pattern that might point to a conspiracy to take over the Italian government.

== Cast ==
*Franco Nero: Carabinieri Colonel Dante Matucci 
*Anthony Quinn: Bruno Manzini 
*Martin Balsam: Captain Steffanelli 
*Sybil Danning: Lili Anders 
*Christopher Lee: Prince Baldasar, the Director of Counterintelligence 
*Cleavon Little: Major Carl Malinowski, USMC 
*Paul L. Smith: The Surgeon
*John Steiner: Captain Roditi 
*Claudia Cardinale: Elena Leporello 
*Eli Wallach: General Leporello 
*Renzo Palmer: Carabinieri Major Giorgione 
*Anita Strindberg: Princess Faubiani 
*Marino Masé: Captain Rigoli 
*Jacques Herlin: Woodpecker 
*Fortunato Arena: General Pantaleone 
*John Stacy: Concierge  Gitte Lee: Princess Baldasar 
*Nello Pazzafini: Manzinis Bodyguard

==References==
 

==External links==
* 

 
 
 
 
 
 


 