Kattu Vannu Vilichappol
{{Infobox film
| name           =Kattu Vannu Vilichappol
| image          =Kattu Vannu Vilichappol.jpg
| image size     =
| caption        =
| director       = Sasi Paravoor
| producer       = Krishna Sasidharan, T Haridas
| writer         = Sasi Paravoor
| narrator       = Chippy Vijayaraghavan Vijayaraghavan Krishnakumar
| music          = M. G. Radhakrishnan
| cinematography = K. G. Jayan
| editing        = Venugopal
| distributor    = 
| released       = 2000
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  30 lakhs   
| gross          =  15 lakhs 
}} Vijayaraghavan and Krishnakumar in pivotal roles. The film deals with the issue of AIDS.

==Plot==
Seetha, born in an orthodox Hindu family, falls in love with the lower-caste Unni and later elopes with him to Bombay. Four years later, one day, Unni goes missing at Bombay. Seetha is pregnant and had to return to her native place. Through a newspaper report, Seetha comes to know that Unni has committed suicide after knowing he was infected with HIV. The heart-broken Seetha is isolated by the society and even by her parents who believe that she is also infected. Abu, a kind-hearted Muslim ferryman is her only help. Hated and isolated by the society, Abu and Seetha has to face lot of troubles. One day, filmmaker Lohithadas, a friend of Unni, visits Seetha and informs her that Unni was killed by the Bombay underworld and the AIDS-news was all crafted by the wicked media. Meanwhile, some people try to kick Seetha out of the village and burn her house. The film ends with Seetha, her new-born baby and Abu fleeing to another village.

==Cast== Chippy as Seetha Vijayaraghavan as Abu
* Krishnakumar as Unni
* Madambu Kunjikkuttan as Raghavan Nair
* A. K. Lohithadas as himself
* T. V. Chandran as Achuthan Nair
* Jose Pellissery as Shankara Pillai
* Priyadarsini
* Saraswathi Amma
* Harishree Ashokan as Velappan
* Kozhikode Shantha Devi as Umma
* Dimple Rose

==Soundtrack==

The films soundtrack is composed by M. G. Radhakrishnan. The lyrics are by O. N. V. Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)!! Duration
|-
| 1
| "Katte Nee"
| K. S. Chithra
| 03:29
|-
| 2
| "Poomakal"
| M. G. Sreekumar
| 05:18
|}

==References==
 

 
 
 
 
 

 