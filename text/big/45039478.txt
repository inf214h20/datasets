L'Affaire SK1
{{Infobox film
| name           = LAffaire SK1
| image          = 
| caption        =
| director       = Frédéric Tellier Julien Leclercq   Julien Madon   Julien Deris   Franck Elbase   David Gauquie  Nicolas Lesage Etienne Mallet  
| writer         = David Oelhoffen Frédéric Tellier 
| starring       = Raphaël Personnaz   Nathalie Baye   Olivier Gourmet   Michel Vuillermoz
| music          = Christophe La Pinta Frédéric Tellier 
| cinematography = Mathias Boucard 	 
| editing        = Mickael Dumontier 	
| studio         = Labyrinthe Films   Movie Pictures   France 3 Cinéma   Cinéfrance 1888   SND Films   Mondatta Films   Bethsabée Mucho
| distributor    = SND
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = $5 millions
| gross          = $2,867,670 
}}

LAffaire SK1 is a 2014 French thriller drama film directed by Frédéric Tellier. The film chronicles the hunt and trial of a serial killer, dubbed "The Beast of the Bastille", in the 1990s. The term, "SK1", refers to "Serial Killer 1", a codename given by the police to the first serial killer who was identified and arrested via DNA analysis in France. 

== Cast ==
* Raphaël Personnaz as Franck Magne (Charlie)
* Nathalie Baye as Frédérique Pons
* Olivier Gourmet as Bougon  
* Michel Vuillermoz as Carbonnel  
* Adama Niane as Guy Georges
* Christa Théret as Elisabeth Ortega 
* Thierry Neuvic as Jensen 
* William Nadylam as Lavocat de Guy Georges 
* Marianne Denicourt as La chef de la Crim 
* Chloë Stéfani as Corinne

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 