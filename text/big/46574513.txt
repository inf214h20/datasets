Carmen's Veranda
{{Infobox Hollywood cartoon|
| cartoon_name = Carmens Veranda
| series = Terrytoons
| image = 
| caption = 
| director = Mannie Davis
| story_artist = John Foster
| animator = 
| voice_actor = 
| musician = Philip A. Scheib Paul Terry
| studio = Terrytoons
| distributor = 20th Century Fox
| release_date = July 28, 1944
| color_process = Technicolor
| runtime = 6:25 English
| preceded_by = The Green Line
| followed_by = The Cat Came Back
}}

Carmens Veranda is a short animated musical film produced by Terrytoons and distributed by 20th Century Fox. The film is one of the studios early films in color.

==Plot== high heels.

In the opera, Carmens mother, a brown ogress, urges Carmen to marry the count, a generous and wealthy but unattractive nobleman. Carmen, however, does not adore the count, and will therefore only marry whom she has affection for.

The count comes to Carmens place with various gifts. But Carmen remains strong to her marital preference. Every time the count approaches the weasel at her balcony, Carmen would either bash him back down or drop a heavy object. Carmens mother then resorts to sending armored guards to Carmens room, in an attempt to push Carmen to take the count. Carmen locks her room and writes a distress letter to some guy named Tyrone. Carmen sends the message via bird.

In another scene in the opera, Tyrone, who is a spoonbill, receives the message carried by the bird. He then sets off in a flying horse.

Tyrone arrives just outside of Carmens home. The guards charge at him but Tyrone is able to overcome them. Eventually with his flying horse, Tyrone comes to the elevated window of Carmens room where he picks up Carmen before flying away.

The count, however, isnt left loveless as he finds a lover in Carmens mother.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 

 