Autumn in March
 
{{Infobox film name = Autumn in March image = caption = director = Huang Yiliang producer = Huang Yiliang story = Huang Yiliang Sun Lin Yu Tong writer = Chui Chi Kin Ting Soo Yun starring = Sheila Sim Phyllis Quek Nathaniel Ho Bernard Tan music = Mo Juli cinematography = Alan Yap Lim Chin Leong editing = Yim Mum Chong Gu Yi Huang Yiliang distributor = Red Group Film released =   runtime = 87 minutes country = Singapore language = Mandarin Hokkien English budget = Singapore dollar|S$1,000,000 preceded by = followed by =
}} directed by produced by Red Group Film. It stars Sheila Sim, Phyllis Quek, Nathaniel Ho and Bernard Tan and co-stars Huang Yiliang and Joey Swee.

It was released in DVD format on 8 September 2009 nationwide in Bluemax stores.

==Plot==
Xinjie (Sheila Sim) is a mysterious girl who lives on her own in a big bungalow house. No one knows much about the girl except that she is leasing out rooms to strangers at an incredibly cheap rate turning away many prospective renters who believe that the house is haunted. Xinjie rented out 3 of her rooms at $200 per month each, and the only condition is that she wants her room mates to have breakfast with her every morning and dinner for at least 3 times a week.

Her criteria for rental also leaves some with little doubt that the girl has mental problems. After a long search for tenants, she finally decides to rent out her place to an ex convict with a history of domestic violence (Bernard Tan), an older lady who is on the run from loan sharks (Phyllis Quek) and a young aspiring pianist who has fled from home after a dispute with his father (Nathaniel Ho).

As their lives start to intertwine, Xinjies horrific past starts to unravel as well accumulating to a climax of an ending that leaves everyone stumped!

==Cast==
The main characters of the movie were played by the following actors:
{| class="wikitable"
|+
! Character !! Played by
|- Xinjie
|Sheila Sim
|- Lee Siqin Phyllis Quek
|-
| Nathaniel Ho
|-
| Bernard Tan
|}
Co-stars include Huang Yiliang, Joey Swee etc.

==Production==
Autumn in March is Huang Yiliangs first produced movie. Filming had taken place for the $1million budget film on September 2008 and planned to be released by first quarter 2009 in cinemas. However, the film was released in DVD instead, on 8 September 2009.

==External links==
*  

 
 
 
 
 
 
 

 