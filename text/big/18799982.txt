Once Around
 
{{Infobox film
| name           = Once Around
| image          = Once_around_poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Lasse Hallström
| producer       = Amy Robinson Griffin Dunne
| writer         = Malia Scotch Marmo
| starring       = Richard Dreyfuss Holly Hunter Danny Aiello Laura San Giacomo Gena Rowlands
| music          = James Horner
| cinematography = Theo van de Sande
| editing        = Andrew Mondshein Cinecom Entertainment Group Double Play Productions Dreyfuss/James Productions Universal Pictures
| released       = January 18, 1991
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $16 million
| gross          = $14,851,083
| preceded_by    = 
| followed_by    = 
}}
Once Around is a 1991 romantic comedy-drama film about a young woman who falls for and eventually marries an overbearing older man who proceeds to rub her close-knit family the wrong way, while exposing the dynamics of other family members along the way. It stars Richard Dreyfuss, Holly Hunter, Danny Aiello, Laura San Giacomo and Gena Rowlands and was directed by Lasse Hallström.

==Plot==
The Bellas are a close-knit family of Italian-Americans living in Boston, Massachusetts. Joe, the head of the family, owns a construction company. Hes been married to Marilyn for 34 years and they have 3 children—Tony, Renata and Jan. Jan is about to get married, leading Renata to wonder why boyfriend Rob has not yet proposed to her. Once Rob reveals that he never plans on marrying her, she leaves him and moves back in with her parents.

Renata travels to the Caribbean, where she takes a course on selling condominiums. She meets Sam Sharpe, a highly successful salesman who makes a speech at a training seminar. They become instantly attracted and Sam accompanies her back to Boston, where Renata introduces him to her family. A chain-smoking, abrasive Sam is overly eager to please. While the majority of the Bellas give Sam a chance, Jan seems to have a particular dislike of him. This upsets Renata and the siblings rapport becomes strained. Jan eventually apologizes and gives Renata her blessing.

Sam and Renata get married, with Sam relocating his business from New York to Boston so he can spend as much time with Renata as possible. At a memorial for Joes late mother, Sam attempts to sing a song in her honor, but the Bellas, especially Marilyn, tell him it is highly inappropriate. Renata tells Sam hes tearing her family apart. They reconcile, and the next day Renata gives birth to their child. At the baptism, Sam suffers a heart attack and is rushed to the hospital.

Now in a wheelchair, Sam is welcomed home to the Bella residence to celebrate Christmas as a family. During dinner, he lights up a cigarette, which an irritated Renata throws into a glass of wine. On a frozen lake, Renata goes skating while Sam and their daughter watch from a distance. Sam passes away while still holding his child. After the funeral, Renata mourns but is grateful for the time they spent together and for Sam changing her life for the better. Renatas father directs the funeral procession through several rotations on a traffic round-about, something Sam greatly enjoyed during his life.

==Cast==
* Richard Dreyfuss as Sam Sharpe
* Holly Hunter as Renata Bella
* Danny Aiello as Joe Bella
* Laura San Giacomo as Jan Bella
* Gena Rowlands as Marilyn Bella
* Roxanne Hart as Gail Bella
* Danton Stone as Tony Bella
* Tim Guinee as Peter Hedges
* Greg Germann as Jim Redstone
* Griffin Dunne as Rob
* Cullen O. Johnson as Sonny

==Reception==

The movie had a mixed reception.   

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 