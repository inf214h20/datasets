The Firefly of France
{{Infobox film
| name           = The Firefly of France
| image          =
| alt            = 
| caption        = 
| director       = Donald Crisp
| producer       = Jesse L. Lasky Margaret Turnbull
| starring       = Wallace Reid Ann Little Charles Ogle Raymond Hatton Winter Hall Ernest Joy
| music          = 
| cinematography = Henry Kotani
| editor         =
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Margaret Turnbull. The film stars Wallace Reid, Ann Little, Charles Ogle, Raymond Hatton, Winter Hall and Ernest Joy. The film was released on July 7, 1918, by Paramount Pictures.  

==Plot==
 

==Cast==
*Wallace Reid as Devereux Bayne
*Ann Little as Esme Falconer
*Charles Ogle as Von Blenheim (*Charles Stanton Ogle)
*Raymond Hatton as The Firefly
*Winter Hall as Dunham
*Ernest Joy as Aide to Von Blenheim
*William Elmer as Aide to Von Blenheim
*Clarence Geldart as Aide to Von Blenheim
*Henry Woodward as Georges
*Jane Wolfe as Marie-Jeanne

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 