Skinned Deep
  
 
{{Infobox film
| name           = Skinned Deep
| image          = Skinned Deep poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = DVD Cover
| director       = Gabriel Bartalos
| producer       = Gabriel Bartalos
| writer         = Gabriel Bartalos
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Les Pollack Aaron Sims Kurt Carley Linda Weinrib Forrest J Ackerman Eric Bennett Warwick Davis David Davidson Captain Sensible
| cinematography = Peter Strietmann
| editing        = Christopher Seguine
| studio         = Center Ring Entertainment
| distributor    = Bedford Entertainment
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $ 600,000 USD
| gross          = 
}}

Skinned Deep is a 2004 horror film, directed and written by Gabriel Bartalos.

==Plot==
 

An American family is lost on the highway while taking a family trip. After their car gets a flat, the father, Phil Rockwell, goes to a convenience store to find help and a strange old woman invites them to stay with her while one of her sons can fix their car. There they meet her strange sons, one whom the woman calls the "Surgeon General". After, they are introduced to Plates (Warwick Davis) and Brain. After the mom takes a picture of Surgeon General, he kills her and Plates starts throwing plates at the dad who is then murdered by Surgeon General. The daughter, Tina, and son, Matthew, escape through a window and are pursued by Surgeon General and Plates. Matthew taunts Surgeon General who swipes at him and splits him in two. Tina is captured and knocked out. When she wakes up shes in a room covered in newspapers where she escapes out of a trap door. She finds a couple old bikers from part of a gang at the convience store being served coffee by the old woman and begs for help from the family. She gets recaptured and its assumed the gang are killed in a cutscene. Brain takes Tina to a park and shows her how to ride a motorcycle. In the end Tina manages to kill and escape the strange family, where she finds help from a police who turns out to be similar to the strange family.

==Cast==
* Forrest J Ackerman as Forrey
* Eric Bennett as Phil Rockwell
* Jason Dugre as Brain
* Warwick Davis as Plates
* Karoline Brandt as Tina Rockwell
* Peter Iasillo, Jr. as Petey Kurt Carley as Surgeon General #2
* Bill Butts as Graine
* Neil Dooley as Pig Pen
* Joel Harlow as Octobaby

==Reception==
 
Skinned Deep receive negative reviews.  Website, Bloody Good Horror, gave the film a 5/10 for its terrible performance and weak storyline, explaining that its "just the average Texas Chainsaw Massacre knock-off".  Another website, Eat Horror, explained the film is "horribly bad but never scares or thrills and the attempts at comedy are incredibly childish".  

==References==
 

==External links==
*  
*  
*  

 
 
 
 