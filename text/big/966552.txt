Billy Jack
  wrestler of a similar name, see Billy Jack Haynes.

{{Infobox film
| name           = Billy Jack
| image          = Billy Jack poster.jpg
| caption        = Theatrical release poster.
| director       = Tom Laughlin  as T.C. Frank 
| producer       = Tom Laughlin  as Mary Rose Solti 
| writer         = Tom Laughlin  (as Frank Christina)  Delores Taylor  (as Theresa Christina) 
| narrator       =
| starring       = Tom Laughlin Delores Taylor
| music          = Mundell Lowe, Dennis Lambert, Brian Potter Fred Koenekamp John M. Stephens
| editing        = Larry Heath Marion Rothman
| studio         = National Student Film Corporation
| distributor    = Warner Bros.
| released       =  
| runtime        = 114 min.
| country        = United States
| language       = English
| budget         = $800,000
| gross          = $32.5 million   
}}
 independent film; 20th Century-Fox came forward and filming eventually resumed but when that studio refused to distribute the film, Warner Bros. stepped forward.

Still, the film lacked distribution, so Laughlin booked it in to theaters himself in 1971.  The film died at the box office in its initial run, but eventually took in more than $40 million in its 1973 re-release, with distribution supervised by Laughlin.

== Plot == American Navajo Green Beret biker film" about a motorcycle gang terrorizing a California town. Billy Jack rises to the occasion to defeat the gang when defending a college student with evidence against them for gang rape.
 counterculture students. The school is organized by Jean Roberts (Delores Taylor).

In one scene, a group of Indian children from the school go to town for ice cream and are refused service and then abused and humiliated by Bernard Posner and his gang. This prompts a violent outburst by Billy. Later, Billys girlfriend Jean is raped and an Indian student is murdered by Bernard (David Roya), the son of the countys corrupt political boss (Bert Freed). Billy confronts Bernard and sustains a gunshot wound before killing him with a hand strike to the throat, after Bernard was having sex with a 13-year-old girl. After a climactic shootout with the police, and pleading from Jean, Billy Jack surrenders to the authorities and is arrested. As he is driven away, a large crowd of supporters raise their fists as a show of defiance and support. The plot continues in the sequel, The Trial of Billy Jack.

== Cast ==
*Tom Laughlin as Billy Jack
*Delores Taylor as Jean Roberts
*Clark Howat as Sheriff Cole
*Victor Izay as Doctor
*Julie Webb as Barbara
*Debbie Schock as Kit
*Teresa Kelly as Carol
*Lynn Baker as Sarah
*Stan Rice as Martin
*David Roya as Bernard Posner
*John McClure as Dinosaur
*Susan Foster as Cindy
*Susan Sosa as Sunshine

== Box-office and critical reception ==
The film was re-released in 1973 and earned an estimated $8,275,000 in North American rentals. 

Billy Jack holds a "Fresh" rating of 62% at Rotten Tomatoes.  As of February 2014 it has a score of 6.1 on Internet Movie Database|IMDB.

In his Movie and Video Guide, film critic Leonard Maltin writes: "Seen today, its politics are highly questionable, and its message of peace looks ridiculous, considering the amount of violence in the film."

 , that a gun is better than a constitution in the enforcement of justice." 

Delores Taylor received a Golden Globe nomination as Most Promising Newcoming Actress. Tom Laughlin won the grand prize for the film at the 1971 Taormina International Film Festival in Italy.

==Soundtrack==
{{Infobox album |  
| Name        = Billy Jack
| Type        = Soundtrack
| Artist      = Mundell Lowe
| Cover       = 
| Released    = 1972
| Recorded    = 1971
| Genre       = Film score
| Length      =  Warner Bros.  WS 1926 
| Producer    = Mundell Lowe
| Chronology  = Mundell Lowe Satan in High Heels (1961)
| This album  = Billy Jack (1971)
| Next album  = California Guitar (1974)
}}
 Warner Bros. label.   accessed August 23, 2012 
 
===Reception===
The  " by the band Coven (band)|Coven, became a Top 40 hit in 1971, and featured the chorus:

 
Go ahead and hate your neighbor; go ahead and cheat a friend.
Do it in the name of heaven; you can justify it in the end.
There wont be any trumpets blowin come the judgment day
On the bloody morning after, one tin soldier rides away
 

{{Album ratings
| rev1 = Allmusic
| rev1Score =    
}}

===Track listing===
All compositions by Mundell Lowe, except as indicated. Brian Potter)&nbsp;– 3:18
# "Hello Billy Jack"&nbsp;– 0:45
# "Old and the New"&nbsp;– 1:00
# "Johnnie" (Teresa Kelly)&nbsp;– 2:35
# "Look, Look to the Mountain" (Kelly)&nbsp;– 1:40
# "When Will Billy Love Me" (Lynn Baker)&nbsp;– 3:24
# "Freedom Over Me" (Gwen Smith)&nbsp;– 0:35
# "All Forked Tongue Talk Alike"&nbsp;– 2:54
# "Challenge"&nbsp;– 2:20
# "Rainbow Made of Children" (Baker)&nbsp;– 3:50
# "Most Beautiful Day"&nbsp;– 0:30
# "An Indian Dance"&nbsp;– 1:15
# "Ceremonial Dance"&nbsp;– 1:59
# "Flick of the Wrist"&nbsp;– 2:15
# "Its All She Left Me"&nbsp;– 1:56
# "You Shouldnt Do That"&nbsp;– 3:21
# "Ring Song" (Katy Moffatt)&nbsp;– 4:25
# "Thy Loving Hand"&nbsp;– 1:35
# "Say Goodbye Cause Youre Leavin"&nbsp;– 2:36
# "The Theme from Billy Jack"&nbsp;– 2:21
# "One Tin Soldier (End Title)" (Lambert, Potter)&nbsp;– 1:06

===Personnel===
*  , conductor Coven featuring Jinx Dawson (tracks 1 & 21), Teresa Kelly (tracks 4 & 5), Lynn Baker (tracks 6 & 10), Gwen Smith (track 7), Katy Moffatt (track 17): vocals 
* Other unidentified musicians

== Influence ==
 civil rights kung fu movie trend that followed.  The centerpiece of the film features Billy Jack, enraged over the mistreatment of his Indian friends, fighting racist thugs using hapkido techniques.
 blue jeans, and a black hat with a beadwork band) would become nearly as iconic as the character .
 MASH being the first). A black student says the words "fucked up" during the scene where the Freedom school students are talking about the "Second Coming".

== Billy Jack in popular culture ==
* In 1975 (release date 12/30/1974), Firesign Theater, an American comedy group, made reference to Billy Jack on their album, "In The Next World, Youre on Your Own," in the form of "Billy Jack Dog Food", and "Im not Billy Jacking you," among other thematic references.
* In 1975, musician Curtis Mayfield recorded and released a song titled, "Billy Jack" on his album Theres No Place Like America Today.
* In 1976 musician Paul Simon played "Billy Paul" (a parody of Billy Jack, unrelated  to musician Billy Paul) in a sketch on the second season of the NBC comedy show Saturday Night Live, after the film Billy Jack aired earlier that evening on NBC.
* In 1982, a professional wrestler, Billy Jack Haynes, debuted as "Billy Jack" wearing a hat like Billy Jack. He changed his wrestling name from "Billy Jack" to "Billy Jack Haynes" after Tom Laughlin threatened to sue.
* In the series  , after a fight breaks out between a racist dig supervisor and his Indian help, Tom Servo says, "This is where Billy Jack should come riding up."; on the episode Track of the Moon Beast, after the Native American professor finishes telling a story, Crow says, "Uh huh...do you know Billy Jack?"
* In an episode of The Simpsons ("Bart of War"), Bart joins a Boy Scouts of America-like group called the "Pre-Teen Braves", and they engage in a rivalry with "the Cavalry Kids". A montage of the two groups fighting each other is set to Covens version of One Tin Soldier.
* The song "Kooler than Jesus" by My Life with the Thrill Kill Kult features samples from the film.
* Billy Jack is referenced in an episode of Gilmore Girls ("Red Light on the Wedding Night") while Lorelai and Rory are watching the movie in their living room. At the line "Billy Jack, Im gonna kill you if its the last thing I do!", Lorelai responds, "Ugh, he so jinxed himself with that one." Rory replies, "Yeah, he shouldve said Billy Jack, Im gonna kill you or buy myself a lovely chenille sweater."
* Upon meeting serial killer Cary Stayner—then considered a possible material witness to a 1999 murder in Yosemite National Park—FBI Agent Jeff Rinek asked if Stayner had ever seen the movie Billy Jack, noting Stayners resemblance to the films hero. Initially, Stayner denied seeing the movie.  However, 90 minutes later, after building rapport during the drive to the FBI headquarters in Sacramento from the nudist resort where he was picked up, Stayner surprised Rinek by reciting several of Billy Jacks lines. 
* In the motion picture Major Payne, Damon Wayans as the title character references the iconic fight scene quote "Now, what Im goin do is take this right foot and Im a put it cross the left side your face." Sabrina The Teenage Witch, principal Mr. Kraft reveals that Billy Jack is his favorite film. Yes Man.
* Metal band Goblin Cock have a song entitled "Ode to Billy Jack" on their 2009 album Come With Me if You Want to Live, which is a tribute to him.
* In the movie Drillbit Taylor, actor Owen Wilson references Billy Jack by saying to a cast mate "I am gonna Billy Jack your ass."
* In the episode of the animated show Pinky and the Brain, titled "Brainy Jack," Brain assumes the role of the titular Brainy Jack to trick a commune of hippies into helping him take over the world. Brains wardrobe is a direct reference to Billy Jack, especially the hat with a beaded hat-band. Likewise, the song Pinky sings in the episode is a parody of "One Tin Soldier."
* British electro band Relaxed Muscle (fronted by Jarvis Cocker, from Pulp (band)|Pulp) released a song called "Billy Jack" on their only album A Heavy Nite With... in 2003. It was released as a single with a music video that featured Cocker (as alter ego, Darren Spooner) in Western garb reminiscent of Billy Jacks trademark outfit. Season 2 Hugo Miller shouts "Billy Jack!" excitedly after Myka Bering kicks a gas station attendant who had pulled a gun.
* In the book "The Berlin Blues", a play by Drew Hayden Taylor, the character named Trailer references Billy Jack when he says on page 92, "No Cirque du Billy Jack?" when the plan for Ojibway World which was supposed to be opening on the reserve falls through. fundamentalist Mormons.

== References ==

 

== External links ==
*  
*  
*   Interview with Billy Jack co-star David "Bernard Posner" Roya
*  
*   Village Voice review August 19, 1971

 

 
 
 
 
 
 
 
 
 
 
 
 