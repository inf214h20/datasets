Aanakkorumma
{{Infobox film 
| name           = Aanakkorumma
| image          =
| caption        =
| director       = M. Mani
| producer       = M Mani Vijayan
| screenplay     = Menaka
| Shyam V. D. Rajappan
| cinematography = CE Babu
| editing        = VP Krishnan
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Menaka in Shyam and V. D. Rajappan.   

==Cast==
*Adoor Bhasi as Raman Nair
*Ratheesh as Devan
*Sankaradi as Potti Menaka as Devi Baby Shalini as Bindu Santhosh as Vikraman
*MG Soman as Swami/Police Officer
*VD Rajappan as Balan Kunchan as Govindan
*Thrissur Elsy as Meenakshi
*Poojappura Ravi as Narayana Pilla
*Paravoor Bharathan as Police officer
*Thikkurussy Sukumaran Nair as Minister
*KPAC Azeez as Police Inspector Anuradha as Anitha

==Soundtrack== Shyam and V. D. Rajappan and lyrics was written by V. D. Rajappan and Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanna Kaarmukil Varnna   || Ambili, V. D. Rajappan || Chunakkara Ramankutty, V. D. Rajappan || 
|-
| 2 || Manikanda Manikanda || Vani Jairam || Chunakkara Ramankutty || 
|-
| 3 || Muthaninja Therirangi || P Susheela || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 