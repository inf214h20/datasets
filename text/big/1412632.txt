Head-On (film)
 
{{Infobox film
| name = Head-On
| image = Gegen die Wand (2004).jpg
| image_size = 215px
| alt = 
| caption = Theatrical film poster
| director = Fatih Akın
| producer = Stefan Schubert Ralph Schwingel Andreas Schreitmüller Mehmet Kurtuluş Fatih Akın
| writer = Fatih Akın
| starring = Birol Ünel Sibel Kekilli Catrin Striebeck Meltem Cumbul
| music = Alexander Hacke Maceo Parker
| cinematography = Rainer Klausmann Andrew Bird Bavaria Film International
| distributor = Timebandits Films   R Film   Strand Releasing  
| released =  
| runtime = 121 minutes
| country = Germany Turkey
| language = German Turkish English
| budget = 
| gross = $11,030,861 
}}
Head-On ( , literally Against the Wall;  ) is a 2004 German-Turkish drama film written and directed by Fatih Akın.

==Plot== Turkish German in his 40s. He has given up on life after the death of his wife and seeks solace in cocaine and alcohol. One night, he intentionally drives his car head-on into a wall and barely survives. At the psychiatric clinic he is taken to, Sibel Güner, another Turkish German who has tried to commit suicide, approaches him. She asks Cahit to carry out a formal marriage with her so that she can break out of the strict rules of her conservative family. Cahit is initially turned off by the idea, but then he agrees to take part in the plan.

After Sibel tells him that she prefers an independent sex life, they live as roommates with separate private lives. They eventually fall in love, but things take a turn for the worse when Cahit kills one of Sibels former lovers out of anger and is sent to prison. While Cahit is in prison, Sibel flees her family and goes to Istanbul to stay with her cousin Selma (Meltem Cumbul)—a divorced woman who manages a hotel. Sibel accepts a job as a maid in Selmas hotel, but finds her new life to be as restrictive as prison. She leaves Selmas apartment to live with a bartender who provides drugs and alcohol. Eventually, he rapes her during her drunk sleep and throws her out. Roaming the streets that night, she baits three men into beating her up. One of them stabs her and they leave her for dead.

Several years later, Cahit travels to Istanbul upon his release from prison, hoping to find Sibel. Initially, Selma refuses to tell Cahit where Sibel is, but later informs him that Sibel is in a long-term relationship and now has a daughter. Cahit waits in a hotel for Sibels call. It eventually comes, and they meet and make love. He asks her to run away with him and she agrees, but when she goes home to pack, she hears her husband return and squeals of delight from her daughter. She never shows up. The film ends with Cahit on a bus, presumably traveling to Mersin, the city where he was born.

==Cast==
* Birol Ünel as Cahit Tomruk
* Sibel Kekilli as Sibel Güner
* Catrin Striebeck as Maren
* Meltem Cumbul as Selma
* Hermann Lause: Psychotherapist Dr. Schiller
* Demir Gökgöl: Yunus Güner
* Cem Akın: Yilmaz Güner
* Mona Mur: Customer Zoe Bar
* Adam Bousdoukos: Barman 1
* Mehmet Kurtuluş: Barman 2
* Tim Seyfi: Taxidriver
* Fanfare Ciocărlia: Musicians
* Stefan Gebelhoff as Nico
* Francesco Fiannaca as Mann am Tresen

==Reception==
Head-On received generally positive reviews, and has a 90 percent rating at Rotten Tomatoes, based on 83 reviews. The critical consensus states that the film is "A raw, provocative drama about star-crossed love and the lives of immigrants caught between the traditional and modern." At Metacritic, the film has a 78/100 rating, indicating "generally favorable reviews". 
 John Waters, annual selection for the 2006 Maryland Film Festival.

==Accolades==
* "Best Film" and the "Audience Award" at the 2004 European Film Awards on December 11 in Barcelona, Spain. 
* The Golden Bear for Best Film at 54th Berlin International Film Festival) on February 14, 2004.
* The Golden Prize for Best Actress at the Deutscher Filmpreis on June 18, 2004.
* The Quadriga Prize on October 3, 2004 in Berlin.
* The Silver Mirror Award for the Best Movie from the South at the Oslo Film Festival on October 16, 2004.
* The Audience Prize at the 9th Festival de Cine on November 6–13 in Sevilla, Spain. Golden Bambi for the best shooting star at the 56th Bambi-Verleihung on November 19 in Hamburg, Germany
* The Golden Gilde prize for the best German film of 2003-2004 at the Leipzig Film Fair.
* The Goya Award for Best European Film of 2004, on January 30, 2005 in Madrid, Spain.

==References==
 

==External links==
*     
*  
*  
*  
*  
*  

 
 
{{succession box European Film Award for Best European Film
| years=2004
| before=Good Bye Lenin!   Hidden (Caché)}}
{{succession box
| title=Goya Award for Best European Film
| years=2004
| before=Good Bye Lenin!
| after=Match Point}}
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 