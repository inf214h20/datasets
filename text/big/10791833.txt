Aadu Puli Attam (2006 film)
{{Infobox film|
| name = Aadu Puli Attam
| image =
| caption =
| director = Sanjay Ram
| writer = Vennila Sudhakar Vasanth
| producer =
| music = Praveen Mani
| editor = R.T Annadurai
| released = 2006
| runtime =
| country = India Tamil
| budget =
}}
 Venilla and Sudhakar Vasanth. The film tells the story of an orphan in Tirunelveli who is forced to become a gangster.    The music for the film was composed by Praveen Mani and lyrics were provided by Ilaya Kamban.  
 film of the same name starring Rajnikanth and Kamal Haasan released in 1977. However the films share no resemblance otherwise. 

==Plot==
Mandahiran is an orphan turned gangster living in Tirunelveli. He works for a local underworld boss named Sadayan, whose gang kills and loots for money. Along the way, Mandhiran meets a Brahmin girl named Gayathri and the two fall in love. Mandhiran successfully helps Gayathris family with a property dispute and, thus, wins the favor of her father. A special police task force headed by Vellaithurai has been assigned to track the gang and bring it to justice. The movie revolves on Sadayans gang trying to stay one step ahead of the police and avoid capture, and whether Mandhiran and Gayathris relationship will eventually lead to marriage.

==Cast==
* Mani Prakash as " Mandhiran"
* Vennila as "Gayathri"
* Sudhakar Vasanth as "Vellaithurai"
* Damodhara Raju as "Sadayan"
* Poovilangu Mohan as "Gayathris father"

==Soundtrack==
The soundtrack consists of eight songs composed by Pravin Mani.  

{| border="2" cellpadding="4" cellspacing="0" style="margin:1em 1em 1em 0; background:#f9f9f9; border:1px #aaa solid; border-collapse:collapse; font-size:95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss) || Notes
|-
| 1 || Arariro || Pradeep || (04:40) ||
|-
| 2 || Nila Nila || Haricharan, Anuradha Sriram || (05:24) ||
|-
| 3 || Devadhaya || Srinivas, Swetha || (04:55) ||
|-
| 4 || Maya Kanna || Sujatha || (04:50) ||
|-
| 5 || Theme Music || Haricharan || (01:05) ||
|-
| 6 || Sms Lady || Swetha || (04:48) ||
|-
| 7 || Mayapenne || Pradeep || (04:53) ||
|-
| 8 || Gnanapazham || Anuradha Sriram || (04:33) ||
|}

==References==
 

==External links==

 
 
 
 
 


 