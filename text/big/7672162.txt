My Life in Ruins
{{Infobox film
| name = My Life in Ruins
| image = MyLifeInRuinsPoster.jpg
| caption = Theatrical release poster
| director = Donald Petrie
| producer = Michelle Chydzik Nathalie Marciano 
| writer = Mike Reiss Nia Vardalos  (uncredited) 
| narrator =
| starring = Nia Vardalos Richard Dreyfuss Alexis Georgoulis Harland Williams David Newman David Mullen
| cinematography = José Luis Alcaine
| editing = Patrick J. Don Vito
| studio = 26 Films   Playtone
| distributor = Fox Searchlight Pictures  Echo Bridge Entertainment Hollywood Entertainment
| released =    (Greece)     (United States) 
| runtime = 95 minutes 
| country = United States Greece Spain
| language = English, Greek
| budget = United States dollar|$17 million 
| gross = $20,455,276   
}} 2009 romantic comedy film set amongst the ruins of ancient Greece, starring Nia Vardalos, Richard Dreyfuss, Alexis Georgoulis, Rachel Dratch, Harland Williams and British comedy actor and impressionist Alistair McGowan. The film is about a tour guide whose life takes a personal detour, while her group gets entangled in comic situations among the ruins, with a series of unexpected stops along the way. The film was released on June 5, 2009 in the United States,  and May 7, 2009 in Greece.   Hollywood Entertainment. Retrieved on March 19, 2009 

==Synopsis==
Georgia (Nia Vardalos) is a Greek American tour guide who is leading a tour around Greece with an assorted group of misfit tourists who would rather buy a T-shirt than learn about history and culture. In a clash of personalities and cultures, everything seems to go wrong, until one day when an older traveller named Irv Gideon (Richard Dreyfuss), shows her how to have fun, and to take a good look at the last person she would ever expect to find love with, her Greek bus driver (Alexis Georgoulis).    Echo Bridge Entertainment. Retrieved on May 12, 2008 

 

==Cast==
* Nia Vardalos as Georgia Ianakopolis
* Richard Dreyfuss as Irv Gideon
* Alexis Georgoulis as Poupi Kakas
* Rachel Dratch as Kim Sawchuck
* Harland Williams as Big Al Sawchuck
* Caroline Goodall as Dr. Elizabeth Tullen
* Ian Ogilvy as Mr. Stewart Tullen
* Alistair McGowan as Nico
* Jareb Dauplaise as Gator
* Sheila Bernette as Dorcas
* Ralph Nossek as Barnaby
* Bernice Stegers as Maria
* María Botto as Lala, Spanish divorcée
* María Adánez as Lena, Spanish divorcée
* Nacho Perez as Doudi Kakas, Prokopi Kakas nephew
* Sophie Stuckey as Caitlin Tullen
* Brian Palermo as Marc Mallard
* Simon Gleeson as Ken, the Australians
* Natalie ODonnell as Sue, the Australians

==Production==

===Development===
The script is originally by Mike Reiss (The Simpsons, The Simpsons Movie), based on his travel experiences, but it was later re-written by Vardalos (My Big Fat Greek Wedding) after she became involved. The film was co-produced by Gary Goetzman and Tom Hanks, and is directed by Donald Petrie. Vardalos has stated that the film was a lifelong dream of hers for she had always wanted to do a film in her familys ancestral homeland. 

===Filming===
The film is set on location in Greece and Alicante, in Spain, including Guadalest and Javea. This was the first time that an American film studio was allowed to film on location at the Acropolis of Athens|Acropolis; the Greek government gave the studio its approval after Vardalos sought permission to film several scenes there.  Other Greek filming locations include  Olympia, Greece|Olympia, Delphi,  and Epidaurus. 

==Release==
The official US trailer by Fox Searchlight Pictures was released on January 7, 2009. The film premiered on June 5, 2009, and took in $3.2 million, placing it ninth of ten for American box office income in its three-day opening weekend.  After 17 days of its release, My Life In Ruins took in gross sales of $8,500,270 in the United States.  In Greece, the film grossed $1,549,303 1st in its three-day opening weekend, placing it first in sales for the weekend.  After a month, sales stood at $1,871,896 placing it 8th in the yearly Greek box office.  Canadian ticket sales for My Life In Ruins came in at $777,290. 

The film was released simultaneously on DVD and Blu-ray on October 6, 2009, in the United States.  As of April 12, 2010 the domestic DVD sales for My Life In Ruins are $5,718,459.

===Distribution===
The film is distributed by Fox Searchlight Pictures    in the United States, Australia, and New Zealand, while it will be distributed in all other locations by Echo Bridge Entertainment.

==Reception==
 
According to Fox Searchlight Pictures President Peter Rice in the companys press release for signing a distribution deal for the film, the film gained positive feedback in early screenings: 

 "Given the wildly enthusiastic audience response, we are thrilled to be distributing the film. Nias enormous box office appeal  along with her gift of perfect comedic timing make for a winning combination. Producers Michelle Chydzik Sowa, Nathalie Marciano, Gary Goetzman, Tom Hanks, Rita Wilson and Peter Safran have proven that films with broad audience appeal can still be made independently." 

Critics were not so kind, as the film received just a 9% approval rating on Rotten Tomatoes from 119 reviews, with the consensus: "With stereotypical characters and a shopworn plot, My Life in Ruins is a charmless romantic comedy." 
Roger Ebert was unequivocal, in his review of June 3, 2009:

 Rarely has a film centered on a character so superficial and unconvincing,   played with such unrelenting sameness. I didnt hate it so much as feel sorry for it ... The central question posed by "My Life in Ruins" is, what happened to the Nia Vardalos who wrote and starred in My Big Fat Greek Wedding? She was lovable, earthy, sassy, plumper, more of a mess, and the movie grossed more than $300 million. Here shes thinner, blonder, better dressed, looks younger and knows it. Shes like the winner of a beauty makeover at a Hollywood studio. She has that dont touch my makeup! look ... Now she is rich, famous and perhaps taking herself seriously after being worked over for one too many magazine covers ... There is, in short, nothing I liked about "My Life in Ruins," except some of the ruins.  

Scott Foundas, writing in The Village Voice (June 2, 2009), agreed:
 Grumpy Old Mens Donald Petrie, is a strangely self-loathing affair that paints Vardaloss tour group as a uniformly ill-mannered, culturally illiterate bunch, while rendering Greece itself as a badly plumbed third-world hellhole run by lazy, Zorba -dancing louts.  

Lou Lumenick, of the New York Post, also wrote on June 5, 2009:
 Grumpy Old Men).  
 At the Movies, gave the film 1 out of 5 stars and said:

 ...maybe they   read the script and they thought, This is a shocker. And it is so bad, this film... the Australian characters are unbelievable throwbacks to another era...everybody is awful. And, unfortunately, shes   disconcertingly like Sarah Palin, and this really threw me during the film. But its just such a crass   - and all these jokes about Poupis name. Thats supposed to be funny.  

===Accolades===
  Teen Choice Awards 2009. 

==References==
 

==External links==
 
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 