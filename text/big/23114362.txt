L'arc-en-ciel (film)
{{Infobox film
| name           = Larc-en-ciel
| image          =
| caption        =
| director       = David Bonneville
| producer       = David Bonneville Bárbara Valentina Fernando Vendrell
| writer         = David Bonneville
| starring       = Rafael Morais Ana Moreira Carloto Cotta Jaime Freitas
| music          = Black Bambi Ruth Chan
| cinematography = Inês Carvalho
| editing        = Paulo Rebelo
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Portugal
| language       = Portuguese
| budget         =
| gross          =
}}
Larc-en-ciel is a short feature film directed by David Bonneville. It is a co-production by the Calouste Gulbenkian Foundation / David & Golias / RTP2. 

The film tells us the story of Quitterie, a 40-year-old European woman, who relives the great love for her deceased 18-year-old Japanese partner by engaging young male strangers into sordid sexual adventures. 

==Cast==
*Sofia Ferrão
*Carloto Cotta
*Nuno Casanovas
*Rafael Morais
*Jaime Freitas
*Takuya Oshima
*Ana Moreira


==Awards==
* Best Actress award at the International Portuguese Language Film Festival MOSTRALÍNGUA (Winner)
* Honourable Mention at the Austrian Film Festival of Nations (Winner)
* Portobello London Film Festival (Nomination)
* Milan ICF International Film Festival (Nomination) 
* Fantasporto International Film Festival (Panorama)
* Bilbao International Film Festival Zinegoak (Nomination)
* Hong Kong Indpanda International Film Festival (Nomination)



== External links ==
*  
*  

 
 
 
 
 

 