Bora Bora (2011 film)
 
{{Infobox film
| name           = Bora Bora
| image          = BoraBora2011Poster.jpg
| caption        = Film poster
| director       = Hans Fabian Wullenweber
| producer       = Stine Spang-Hansen
| writer         = Hans Fabian Wullenweber
| starring       = Sarah-Sofie Boussnina
| music          = 
| cinematography = Jacob Kusk
| editing        = 
| distributor    = 
| released       =  
| runtime        = 76 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Bora Bora is a 2011 Danish drama film written and directed by Hans Fabian Wullenweber.    

==Cast==
* Sarah-Sofie Boussnina as Mia
* Janus Dissing Rathke as Zack
* Mette Gregersen as Silja
* Adnan Al-Adhami as Azim
* Iben Dorner as Birthe
* Jimmy Jørgensen as Simon
* Johan Philip Asbæk as Jim
* Christian Grønvall as Betjent
* Malene Nielsen as Shopper / dancer

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 