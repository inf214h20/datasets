Nils Karlsson Pyssling
{{Infobox film
| name           = Nils Karlsson Pyssling
| image          = 
| alt            = 
| caption        = 
| director       = Staffan Götestam
| producer       = Ingrid Dalunde
| writer         = Astrid Lindgren
| starring       = Oskar Löfkvist Jonatan Lindoff Britta Pettersson Charlie Elvegård Ulla Sallert
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 75 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Nils Karlsson Pyssling is a 1990 Swedish family film based on the novel with the same name by Astrid Lindgren.

== Cast ==
*Oskar Löfkvist as Bertil
*Jonatan Lindoff as Nils
*Britta Pettersson as Mother
*Charlie Elvegård as Father
*Ulla Sallert as Hulda

== External links ==
* 
* 

 
 
 
 
 


 