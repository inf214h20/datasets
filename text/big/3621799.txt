Platinum Blonde (film)
{{Infobox film
| name           = Platinum Blonde
| image          = PlatBlonde.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster Frank R. Capra
| producer       = Harry Cohn  Frank R. Capra
| writer         = Robert Riskin (dialogue) Jo Swerling (adaptation) Dorothy Howell (continuity)
| story          = Harry E. Chandlee Douglas W. Churchill	 Robert Williams Jean Harlow
| music          = David Broekman (uncredited) Joseph Walker
| editing        = Gene Milford
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 89 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 1931 American Robert Williams, and Loretta Young. The film was written by Jo Swerling and directed by Frank Capra. Platinum Blonde was Robert Williams last screen appearance; he died of peritonitis three days after the films October 31 release.  

Though not as well known as Capras later 1930s movies, the films reputation has grown over the years. It is occasionally aired in the United States on Turner Classic Movies – and has been introduced on that channel by Sharon Stone.

==Plot== Robert Williams), ace reporter for the Post, is assigned to get the story about the latest escapade of playboy Michael Schuyler (Donald Dillaway), a breach of promise suit by chorus girl Gloria Golden, who has been paid to drop it.  Unlike rival Daily Tribune reporter Bingy Baker (Walter Catlett), he turns down a $50 bribe from Dexter Grayson (Reginald Owen), the Schuylers lawyer, to not write anything. He does pretend to be swayed by the pleas of Anne (Jean Harlow), Michaels sister, but then brazenly calls his editor with the scoop, appalling the Schuylers.
 Conrad he had taken from the Schuylers library.  The butler, Smythe (Halliwell Hobbes), tries to make him leave, but Anne sees him. Stew surprises Anne by presenting her with Michaels love letters to Gloria, who had intended to use them to extort more money from the Schuylers.  Anne offers Stew a $5,000 check, which he refuses.  She asks why he reported the suit, but not the love notes.  Stew explains that one was news, the other, blackmail. He later tells her he is writing a play. Intrigued, Anne wonders if she can turn him into a gentleman. She invites him to a party at the house.

They fall in love and soon elope, horrifying Annes widowed mother, Mrs. Schuyler (Louise Closser Hale), an imperious dowager who looks down on Stews lower-class background. Michael takes it in stride, telling Stew hes not as bad as everyone thinks. The wedding is scooped by the rival Daily Tribune, enraging his editor, Conroy (Edmund Breese). Even more upset is Stews best friend Gallagher (Loretta Young), a sob sister columnist secretly pining for him. Conroy taunts Stew as "a bird in a gilded cage."  Despite his bravado, Stew is upset by the implication he is no longer his own man, vowing not to live on Annes money. However, she cajoles him into moving into the mansion and starts to make him over, buying him garters (despite his objections) and hiring a valet, Dawson (Claud Allister).

When the Schuylers hold a reception for the Spanish ambassador, Gallagher substitutes for the society reporter and chats with Stew.  Anne is surprised to learn that her husbands best friend (whom she had assumed was a man) is actually a lovely young woman and treats Gallagher icily. Then, Bingy tells Stew the Tribune will give him a column if he signs it "Anne Schuylers husband."  Insulted, Stew punches Bingy when he calls him Cinderella Man. The next morning, Mrs. Schuyler is aghast to find Stews brawl has made the front page.

Wrestling with his play, Stew invites Gallagher and another friend, Hank (Eddy Chandler) from Joes.  They arrive with Joe and several bar patrons in tow and even Bingy shows up to apologize.  A raucous party ensues.  Meanwhile, Stew and Gallagher ponder the play, deciding to base it on Stews marriage. Anne, Mrs. Schuyler, and Grayson return as the party is in full swing.  Stew apologizes for letting the party get out of control, but protests that he can invite friends to "my house."  Anne replies, "Your house?"

Stew returns with Gallagher to his own apartment.  Along the way, he gives a homeless man his expensive garters. Grayson stops by to say Anne will pay him alimony, whereupon Stew punches him (earlier, Stew had warned Grayson that his twentieth insult would earn him a "sock to the nose"). Stew tells Gallagher the play could end with the protagonist divorcing his rich wife and marrying the woman whom he had always loved without ever realizing it.  Overwhelmed, Gallagher hugs him.

==Cast (in credits order)==
*Loretta Young as Gallagher Robert Williams as Stew Smith
*Jean Harlow as Ann Schuyler
*Halliwell Hobbes as Butler
*Reginald Owen as Grayson
*Edmund Breese as Conroy – The Editor
*Don Dillaway as Michael Schuyler
*Walter Catlett as Binji
*Claud Allister as Dawson – The Valet
*Louise Closser Hale as Mrs. Schuyler

== Reception and legacy == New York American expressed some disappointment, writing, "For all her top billing, Jean Harlow has very little to do, and Loretta Young even less. To say they are competent to the pictures requirements is only a mild compliment."   

Despite the films measure of positive reviews and star power, it wasnt much of a hit at the box office, with returns around the country reported as "just fair" and "a bit disappointing".     The historical significance of the picture would only become apparent in later years as Capras reputation grew, as did that of Harlow, who, like co-star Williams, died young.     Roger Ebert called the film "central to the Jean Harlow legend". 

== References ==
 

== External links ==
 
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 