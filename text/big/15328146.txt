The Brave Bulls
{{Infobox book
| name          = The Brave Bulls
| title_orig    = 
| translator    = 
| image         = 
| image_caption =  Tom Lea Tom Lea
| cover_artist  = 
| country       = 
| language      = 
| series        = 
| subject       = Raising fighting bulls Bullfighting Mexico
| genre         = Western (genre)|Western, Southwestern
| publisher     = Little, Brown and Company (Boston)
| pub_date      = April 20, 1949
| english_pub_date = 
| media_type    = 
| pages         = 270
| isbn          = 0-292-74733-0  (2002 reprint) 
| dewey= 813/.54 21
| congress= PS3523.E1142 B73 2002
| oclc          = 4622973
| preceded_by   = 
| followed_by   = The Wonderful Country
}}
 Western novel Tom Lea (his first) about the raising of bulls, on the ranch Las Astas, for bullfighting in Mexico.  

Las Astas is based on the real "La Punta", a 15,000 hectare (about 37,000 acre) ranch in eastern Jalisco,  near Lagos de Moreno, at one time the largest fighting-bull ranch in the world. 

Lea, also an artist and muralist, did illustrations throughout the book and on the end papers and dust jacket. Prior to, during World War II, and after, Lea was an artist, and not an author. He went to Mexico to get a better idea about bullfighting, but forgot to take a sketchbook or paintbox, so he found himself using words to describe what he would have as a visual artist. 

==Plot==
The Brave Bulls is the story of Luis Bello, "The Swordsman of Guerreras", the greatest matador in Mexico, who is at the top of his profession, with everything that comes with it, money, a mistress, family and friends, bravado, the crowds are infatuated with him. But one day fear changes everything, he suddenly feels a fear that previously he had not felt in the invincibility that comes with healthy-macho-youth. His best friend and manager, Raul Fuentes, is killed in a car crash along with Luiss mistress, Linda de Calderon, after Linda and Raul had spent a romantic weekend together. This betrayal shakes Luiss beliefs about what has been real and what is real now. Now Luis must deal with these new found feelings while at the same time facing the most feared bulls in all of Mexico, "the brave bulls". In his first fight after the auto accident he is gored by a bull because of the doubt and guilt that has come into the ring with him. In addition, while under the influence of Tequila, and some pressure from ring promoter Eladio Gomez, he agreed to let his younger brother Pepe fight these top bulls with him. Luis must now examine his life to find out where the courage comes from and if he can get it back.

{{Quote box
|width=300px
|align=left
|quote=...Leas knowledgeable explanation of the mystique of bull fighting develops the theme of the fear of death. The torero Luis Bello conquers this fear when he accepts death as inevitable.  
|source=Lou Rodenberge ~ A Literary History of the American West  
|title=A Literary History of the American West
|date=}}

==Reception==
In Time (magazine)|TIMEs review, the magazine said about it: "The writing is clumsy in places, but it is also direct, penetrating and sustained; it makes the slicker sorts of professionalism look pointless. And the book is, finally, both religious in its treatment of ultimates and morally eloquent in its strong rebuke for those who scorn any culture but their own". And that it "qualifies as a work of art".  Additionally TIME called it "The best first novel of the year..."  

The book won the Carr P. Collins Award of the Texas Institute of Letters for best book by a Texan. 

It is widely considered a classic of Southwestern American literature. 

==Adaptations== John Bright. 

==References==
 

==External links==
*   - University of Texas Press

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 