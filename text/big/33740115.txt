One Fall
{{Infobox film
| name = One Fall
| image = One_Fall_movie_poster.jpg
| caption = Film poster
| director = Marcus Dean Fuller
| producer = Dean Silvers  Marlen Hecht Richard K. Smucker Marcus Dean Fuller Forrest Silvers Julie S. Fuller
| writer = Marcus Dean Fuller Richard Greenberg
| starring = Marcus Dean Fuller  Zoe McLellan   Seamus Mulcahy   James McCaffrey   Mark Margolis   Dominic Fumusa  Phyllis Somerville
| music = Ben Toth
| cinematography = Alice Brooks
| editing = Marlen Hecht William Henry
| distributor =
| released =  
| runtime =
| country = United States
| language = English
| budget = 
| gross = 
}}
One Fall is a 2011 American fantasy drama film directed by Marcus Dean Fuller, and produced by Dean Silvers and Marlen Hecht. Filming took place in New York.

==Plot==
Set in the rustic Midwestern U.S. town of One Fall, the film tells the story of a man, James (Marcus Dean Fuller), who miraculously survived a horrific fall from a spectacular 200-foot-high cliff and was never heard from again. However, three years after vanishing James chooses to return to his hometown of One Fall—but he returns a changed man. For an incomprehensible reason, James has developed supernatural healing abilities. He must decide whether to use his abilities to help the ones he once turned his back on or to continue running from his mysterious past.

==Cast==
* Marcus Dean Fuller as James Bond/The Janitor 
* Zoe McLellan as Julie Gardner
* Seamus Mulcahy as Tab Barrows/Repeller Boy
* James McCaffrey as Werber Bond
* Mark La Mura as Cliff Bond
* Dominic Fumusa as Tom Schmidt
* Mark Margolis as Walter Grigg Sr.
* Phyllis Somerville as Mrs.Barrows

==References==
 
http://www.filmcritic.com/reviews/2011/one-fall/

http://www.slantmagazine.com/film/review/one-fall/5755

==External links==
*  

 
 
 
 
 
 


 