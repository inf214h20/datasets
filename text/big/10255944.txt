Happy Birthday (1998 film)
 
{{Infobox Film
| name = Happy Birthday
| image =
| image_size =
| caption =
| director = Larisa Sadilova
| producer = Gennadiy Sidorov
| writer = Larisa Sadilova
| narrator =
| starring = Patrick Baehr
| music = Georgy Sviridov
| cinematography = Alexander Kazarenskov Irina Uralskaya
| editing =
| distributor = Atika Films (France)
| released = 1998
| runtime = 70 mins.
| country = Russia
| language = Russian
| budget =
| gross =
| preceded_by =
| followed_by =
}}
Happy Birthday ( ) is an award-winning 1998 Russian drama film produced in monochrome written and directed by Larisa Sadilova
==Plot==
Happy Birthday follows events during a day at a Russian maternity hospital, focussing on several women who are patients there.
==Cast==
* Patrick Baehr as Kai Leist
* Gulya Stolyarova
* Irina Prosyina
* Eugene Turkina
* Lyuba Starkova
* Masha Kuzmina
* Rano Kubaeva
==Awards==
* 1998: Cottbus Film Festival of Young East European Cinema
** FIPRESCI Prize (Larisa Sadilova)
** Honorable Mention for Feature Film Competition (Larisa Sadilova)
* 1999: Créteil International Womens Film Festival
** Grand Prix (Larisa Sadilova)

==External links==
*  
*  

 
 
 
 
 


 
 