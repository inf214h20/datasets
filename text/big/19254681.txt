María Candelaria
{{Infobox film
  | name = María Candelaria (Xochimilco)
  | image = María Candelaria.jpg
  | caption =
  | director = Emilio Fernández
  | producer = Agustin J. Fink
  | writer = Emilio Fernández
  | starring = Dolores del Río Pedro Armendáriz Alberto Galán Miguel Inclán Julio Ahuet
  | music = Francisco Domínguez
  | cinematography = Gabriel Figueroa
  | editing = Jorge Bustos
  | distributor = Films Mundiales
  | released =
  | runtime = 76 minutes
  | country = Mexico
  | language = Spanish
  | budget =
}} Grand Prix (now known as the Palme dOr) becoming the first Latin American film to do so.  María Candelaria would later win a Silver Ariel award for Best Cinematography.

The film came to be regarded as one of Fernándezs best works in which he portrays the indigenous people of Mexico with innocence and dignity. Fernández has said that he wrote an original version of the plot on 13 napkins while sitting in a restaurant. He was anxious because he was dating Dolores del Río and could not afford to buy her a birthday present. The film was first entitled Xochimilco and the protagonist was named María del Refugio. 

Major themes in the film include melodrama, indigenousness, nationalism, and the beauty of Mexico.    María Candelaria is one of Mexicos most beloved films of all time, and was ranked thirty-seventh among the top 100 films of Mexican cinema.   

==Plot==
A young journalist presses an old artist (Alberto Galán) to show a portrait of a naked indigenous woman that he has in his study. The body of the movie is a flashback to Xochimilco, Mexico, in 1909. The film is set right before the Mexican Revolution, and Xochimilco is an area with beautiful landscapes inhabited mostly by indigenous people. 

The viewer learns that the woman in the painting is María Candelaria (Dolores del Río), a young Indian woman who is constantly rejected by her own people for being the daughter of a prostitute. She and her lover, Lorenzo Rafael (Pedro Armendáriz), face constant struggles throughout the film. They are honest and hardworking, yet nothing ever goes right for them. Don Damian (Miguel Inclán), a jealous Mestizo store owner who wants María for himself, prevents them from getting married. He kills a piglet that María and Lorenzo plan to sell for profit and he refuses to buy vegetables from them. When María falls ill with malaria, Don Damian refuses to give the couple the quinine medicine necessary to fight the disease. Lorenzo breaks into his shop to steal the medicine, and he also takes a wedding dress for María. Lorenzo goes to prison for stealing, and María agrees to model for the painter to pay for his release. The artist begins painting a portrait of María, but when he asks her to pose nude she refuses.

The artist finishes the painting with the nude body of another woman. When the people of Xochimilco see the painting, they assume it is María Candelaria and stone her to death. 

Finally, Lorenzo escapes from prison to carry Marías lifeless body through Xochimilcos canal of the dead. 

==Cast==
* Dolores del Río as María Candelaria: A beautiful, indigenous Mexican woman who has many misfortunes befall her throughout the film.
* Pedro Armendáriz as Lorenzo Rafael: María Candelarias lover and only consistent supporter.
* Alberto Galán as Painter: The narrator of the story and creator of the painting that ultimately leads to Marías death. The character is based on muralist Diego Rivera.     
* Margarita Cortés as Lupe: A young woman in the community who is jealous of María because she wants to be with Lorenzo Rafael. She is instrumental in the mob of townspeople who eventually stone María to death.
* Miguel Inclán as don Damián: A store owner who exploits indigenous people and wants María for himself.

Other characters
 
* Beatriz Ramos as Journalist
* Rafael Icardo as Priest
* Julio Ahuet as José Alfonso
* Lupe Inclán as Gossip
* Salvador Quiroz as Judge
* Nieves as Model
* Elda Loza as Model
* Lupe Garnica as Model
* Arturo Soto Rangel as Doctor
* David Valle González as Court secretary
* José Torvay as Police
* Enrique Zambrano as Doctor
* Alfonso Jiménez "Kilómetro"
* Irma Torres 
* Lupe del Castillo 

==Production== Mexican film star system. 
 Flor silvestre (1943). Emilios "bronco" temperament had surfaced on several occasions, and the actress had nearly left the film. The pleas of their co-workers, and her high sense of professionalism, convinced del Río to return. However, her relationship with the director had become distant. On Good Friday 1943, del Ríos birthday, was the occasion chosen by the filmmaker to find the desired reconciliation. In addition to needing her as an actress, Fernández began to love her as a woman. In his biographical account of the actress, writer David Ramón relates: 

 "When it was Emilio Fernándezs turn to give her his gift, he got close up to Dolores and took a bunch of napkins with writings, and he practically threw them to her and said: This is your birthday present, a history of cinema. I hope youll like it, its your next film, its called Xochimilco. Its yours, its your property, if somebody wants to buy it, theyll buy it from you."  

 With the generous gift and all, Dolores had her doubts. She said: "First a rural woman ... And now, an Indian woman, you want me to play an indian? I ... barefooted?"   

==Awards==
{| class="wikitable"
|-bgcolor="#CCCCCC"
! Year
! Ceremony
! Award
! Result
! Winner
|-
| 1946
| Festival de Cannes  Grand Prix for "Best feature film"   
|  
|
|-
|-
| 1946
| Premios Ariel Special Silver Award for "Best Cinematography"
|  
| Gabriel Figueroa
|-
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 