We Cellar Children
{{Infobox film
| name           = We Cellar Children
| image          = 
| image_size     = 
| caption        = 
| director       = Hans-Joachim Wiedermann
| producer       = Hans Oppenheimer (executive producer)
| writer         = Thomas Keck (dialogue) Herbert Kundler (dialogue) Wolfgang Neuss (screenplay)
| narrator       = 
| starring       = See below
| music          = Peter Sandloff
| cinematography = Werner M. Lenz
| editing        = Walter von Bonhorst
| studio         = 
| distributor    = 
| released       = 1960
| runtime        = 86 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

We Cellar Children (originally Wir Kellerkinder) is a 1960 West German film directed by Hans-Joachim Wiedermann.

== Plot summary ==
 

== Cast ==
*Wolfgang Neuss as Macke Prinz
*Karin Baal as Nenne Briehl, 18 Jahre
*Ingrid van Bergen as Mackes Schwester Almuth Prinz
*Jo Herbst as Adalbert
*Wolfgang Gruner as Arthur
*Willi Rose as Vater Prinz
*Hilde Sessak as Mutter Prinz
*Ralf Wolter as Kameramann Keschke der "Neuen Deutschen Schau"
*Klaus Becker
*Dietmar Behnke
*Horst Dieter Braun
*Eckart Dux as Reporter Kemskorn der "Neuen Deutschen Schau"
*Inge Egger as Frau Briehl
*Wilfried Fraß
*Joe Furtner
*Wolfgang Gerhus
*Friedrich Hartau
*Heinz Holl
*Thomas Keck
*Kurt Lauermann
*Bruno W. Pantel as Herms Niel
*Ethel Reschke
*Joachim Röcker
*Rudi Schmitt as Herr Briehl
*Jochen Schröder
*Günther Schwerkolt
*Achim Strietzel as Knösel
*Rolf Ulrich as Glaubke
*Herbert Weissbach as Amtsgerichtsrat Bleiber
*Paul Westermeier as Portier
*Helmut Käutner as Chauffeur
*Erik Ode as Aufgebrachter Vater
*Erik Schumann as Ulrikes Freund

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 
 