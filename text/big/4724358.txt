Gangster Wars
  
{{Infobox film name        = Gangster Wars image       = caption     =   5.5/10 (33 votes) writer      = Richard DeKoker starring    = Michael Nouri Joe Penny Brian Benben Kathleen Lloyd Madeleine Stowe Markie Post Allan Arbus Louis Giambalvo James Andronica Robert Davi Joseph Mascolo Kenneth Tigar Richard S. Castellano Jon Polito Jonathan Banks George DiCenzo Robert F. Lyons Richard Foronjy  Karen Kondazian director    = Richard C. Sarafian producer    = Stuart Cohen distributor = CIC
|released    = April 9, 1981 runtime     = 121 min. language  English
|music       = John Cacavas awards      = budget      =
}}

Gangster Wars is a 1981 crime drama directed by Richard C. Sarafian.

==Synopsis== Charles "Lucky" Benjamin "Bugsy" Siegel (Joe Penny) and Michael Lasker (Brian Benben) (a fictional character who was most likely modeled after Meyer Lansky), growing up in New Yorks ghettos during the early 1900s to their rise though organized crime.

==Adaptation==
This movie is based on an original telecast from 1981 called The Gangster Chronicles. It was a three-hour opener for the subsequent miniseries. In addition to the characters above Brian Benbens character is a fictional composite of several mobsters (here named "Michael Lasker"). While the miniseries covered nearly four decades, the opener takes us from 1907 to the Prohibition era of the 1920s. After its initial run, the entire Gangster Chronicles saga was boiled down to 121&nbsp;minutes and released to videocassette as Gangster Wars.

==External links==
*  

 

 
 
 


 