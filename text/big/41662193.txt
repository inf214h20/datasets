Feral (2012 film)
{{Infobox film
| name           = Feral
| image          = 
| caption        = 
| director       = Daniel Sousa
| producer       = Daniel Sousa
| writer         = Daniel Sousa
| starring       =  Dan Golden
| cinematography =
| editing        = 
| studio         = 
| distributor    = Creative Capital
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
}}
Feral is a 2012 black-and-white animated short film by Daniel Sousa.

==Plot== feral boy is found in the woods and brought back to live in society. Uncomfortable in this new environment, the boy tries to adapt by using the same strategies and tactics that kept him safe in the wild.

==Release==
Feral premiered January 19, 2013 at the Sundance Film Festival in Park City, Utah

===Accolades===
{| class="wikitable" width="95%"
! colspan="5" style="background: LightSteelBlue;" | Awards
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Award 
| March 2, 2014 Best Animated Short Film Dan Golden
|  
|-
|}

==References==
 

==External links==
*   at Daniel Sousa
*  

 
 
 

 