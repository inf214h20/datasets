The Harlem Globetrotters (film)
{{Multiple issues
| 
 
}}
{{Infobox film
| name           = The Harlem Globetrotters
| image          = HarlemGlobetrottersFilmPoster.jpg
| image_size     = 220
| caption        = Dorothy Dandridge and Bill Walker in 1951 theatrical poster.
| starring       = Thomas Gomez Bill Walker Dorothy Dandridge
| music          =
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
}} Drama film about the famous African American basketball team The Harlem Globetrotters released by Columbia Pictures. The film stars Thomas Gomez, Bill Walker, Dorothy Dandridge, Angela Clarke, and Peter M. Thompson.

==Trivia==
The film has not been released on U.S. home video. It premiered on television on Turner Classic Movies (TCM) on May 29, 2012.

==Cast==
 
*Thomas Gomez as Coach Abe Saperstein
* Bill Walker as Prof. Turner
*Dorothy Dandridge as Ann Carpenter
*Angela Clarke as Sylvia Saperman
* Peter M. Thompson as  Martin 
*Billy Brown as Billy Townsend (Globetrotter)
*Roscoe Cumberland as Roscoe  (Globetrotter)
*William Pop Gates as Pop Gates (Globetrotter)
*Marques Haynes	as Marques (Globetrotter)
*Louis Babe Pressley as Babe Pressley (Globetrotter)
*Ermer Robinson as Elmer Robinson (Globetrotter)
*Ted Strong as Ted Strong (Globetrotter)
*Reece Goose Tatum as Goose Tatum (Globetrotter)
*Frank Washington as Frank Washington (Globetrotter)
*Clarence Wilson as Clarence Wilson  (Globetrotter)
 

==External links==
* 
* 

 
 
 
 
 
 
 


 