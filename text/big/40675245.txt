A Gun for Jennifer
{{Infobox film
| name           = A Gun for Jennifer
| image          = A_Gun_for_Jennifer_Poster.jpg
| caption        =
| director       = Todd Morris
| producer       = Deborah Twiss
| writer         = Todd Morris Deborah Twiss
| starring       = Deborah Twiss
| music          = JF Coleman
| cinematography = Joe di Gennaro Eliot Rockett David Tumblety
| editing        = Todd Morris Rachel Warden
| studio         = Independent Partners
| distributor    = Action Gitanes Conspiracy Films Inc.
| released       = 
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
}} feminist vigilante castrate suspected rapists and batterers while a female police officer attempts to stop them.  It is shot in a retro grindhouse style and premiered at the Fantasia Festival in July 1997,  where it was sold-out an hour before screening and received a standing ovation. The film has not left the festival circuit; Fangoria reported that it had been picked up for distribution by Mondo Macabro, but the release never came to fruition.  

The movie was covered in the documentary In the Belly of the Beast,  which detailed Morris and Twisss discovery that their financier (whom Twiss had met while working as an exotic dancer)  had been embezzling money.    

==Cast==
*Deborah Twiss as Jennifer / Allison
*Benja Kay as Dt. Billie Perez
*Rene Alberta as Becky
*Tracy Dillon as Grace
*Freida Hoops as Jesse
*Veronica Cruz as Priscilla
*Sheila Schmidt as Trish
*Beth Dodye Bass as Annie
*Joe Pallister as Grady (as Joseph Pallister)
*Arthur J. Nascarella as Lt. Rizzo (as Arthur Nascarella)
*Carl Jasper as Carl Varna
*James ODonoghue as Det. Cahill
*Douglas Gorenstein as Snake
*Lord Kayson as T-Bone
*Fatmir Haskaj as Josh

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 