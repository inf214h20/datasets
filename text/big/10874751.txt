The Secret of Treasure Island
{{Infobox film
| name           = The Secret of Treasure Island
| image          = The Secret of Treasure Island.JPG
| image_size     =
| caption        = Title card shown at the beginning of Chapter 10 of the serial
| director       = Elmer Clifton Louis Weiss
| writer         = Elmer Clifton L. Ron Hubbard George M. Merrick George Rosener
| narrator       = Walter Miller Grant Withers George Rosener Hobart Bosworth
| music          = Abe Meyer
| cinematography = Edward Linden Herman Schopp Earl Turner
| distributor    = Columbia Pictures
| released       =   
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Columbia Serial movie serial based on Robert Louis Stevensons novel Treasure Island.     The serial is broken into fifteen chapters.  Reporter Larry Kent travels to an island in the Caribbean to investigate the disappearance of his colleague, and discovers that the island contains a lost treasure trove of gold. Kent meets Toni Morrell, the daughter of a shipmate whose partner knew the location of the treasure, who helps him in his investigation and they search for the treasure together. During their investigation they are opposed by a villain named Collins and Dr. X., who attempts to kill Kent. Kent defeats Dr. X. in the final installment of the serial.

The story was written by  .

==Plot== Walter Miller) who uses his henchmen and supply of weapons and land mines in an attempt to stop them. Kents enemy in the film is Dr. X. (Hobart Bosworth) who wears a skull mask and pirate clothing, and is in the process of developing a powerful explosive. Due to the actions of Dr. X., Kent is nearly buried alive, killed with dynamite and slashed in a sword battle. He defeats Dr. X. in the last installment of the serial titled "Justice".   

==Production== 1941 Columbia movie serial, The Spider Returns. 

Producer Jack Fier brought in Louis Weiss as associate producer, and hired Elmer Clifton to direct.  Yakima Canutt worked on the action sequences in this serial and in The Mysterious Pilot, which William C. Cline, author of In the Nick of Time: Motion Picture Sound Serials referred to as "outstanding" work.  Earl Bun and Ken Peach handled special effects for the serial, including work on the appearance of the mysterious pirate villain. 

==Cast==
*Don Terry as Larry Kent, a reporter investigating the disappearance of a colleague in the area
*Gwen Gaze as Toni Morrell Walter Miller as Carter The Shark Collins, owner of the island
*Grant Withers as Roderick Gridley
*George Rosener as Capt. Samuel Cuttle
*Hobart Bosworth as Dr X.
*Sandra Karina as Nurse Zanya
*Patrick J. Kelly as Professor Gault
*Yakima Canutt as Dreer, Leader of the Mole Men, Collins slave diggers

==Reception==
The serial was well received by fans, and helped to solidify Columbias presence in movie serials.  Hal Erickson of Allmovie gave the serial a rating of one and a half stars.    In his book In the Nick of Time: Motion Picture Sound Serials, William C. Cline described the action in the serial as "well paced and lively".  Alan G. Barbours book Days of Thrills and Adventure: History of the American motion picture serial from 1930-50 called The Secret of Treasure Island "one of Columbias few good serials", and described actor Don Terrys performance as "excellent".   
 Batman and Robin, Mysterious Doctor Satan and The Adventures of Rex and Rinty.   The serial was shown along with the 1922 Down to the Sea in Ships at the 2006 Memphis Film Festival, and in an article about the festival John Beifuss of The Commercial Appeal called the two serials "Elmer Clifton classics". 

==Chapter titles==
Source 
# The Isle of Fear
# The Ghost Talks
# The Phantom Duel
# Buried Alive
# The Girl Who Vanished
# Trapped by the Flood
# The Cannon Roars
# The Circle of Death
# The Pirates Revenge
# The Crash
# Dynamite
# The Bridge of Doom
# The Mad Flight
# The Jaws of Destruction
# Justice

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 