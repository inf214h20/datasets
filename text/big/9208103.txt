Condemned to Live
{{Infobox film
| name           = Condemned to Live
| image          = Condemned-to-Live-poster.jpg
| caption        =
| director       = Frank R. Strayer
| producer       = Maury Cohen
| writer         = Karen DeWolf
| starring       = Ralph Morgan Maxine Doyle Russell Gleason
| music          = Abe Meyer
| cinematography =
| editing        = Invincible Pictures
| released       =  
| runtime        = 65 min
| country        = United States English
| budget         =
}} American horror film starring Ralph Morgan and Maxine Doyle, and directed by Frank R. Strayer.  The film is unusual for its time, as it approaches the topic of vampirism from a sympathetic standpoint. 

==Plot==

The film opens with a trio of explorers in Africa who are hiding in a cave.  One of the explorers, a pregnant woman, is bitten by a vampire bat.
 mob form, with torches, at the house of Professor Kristan (Ralph Morgan) after every murder.  The villagers suspect that a giant bat is to blame for the murders.  Kristan gives the villagers advice on staying safe, and assures them a scientific explanation exists.

However, in subsequent scenes, Kristan himself is revealed to be the murderer.  He is seized by attacks (triggered by darkness) which transform him into a trance-like state of murderousness.  After he commits a murder, he awakens from the trance with no memory of the deed, believing himself merely to have fainted.  Kristans obliviousness is further enabled by the intervention of his loyal hunchback Zan, the only person aware of Kristans condition.  Zan follows Kristan when he is in his trances, ensuring the professor is not discovered.

An old friend of Kristans named Dr. Bizet arrives to visit, and soon suspects what is happening.  Bizet discloses to Kristan that his mother was bitten by a vampire bat, and that traits of vampirism have likely been passed down to him per Lamarckism.  (The audience now understands the pregnant explorer in the opening flashback to have been Kristans mother.)

After Kristans fiance (Maxine Doyle) is attacked by an entranced Kristan, the mob of villagers assumes Zan is culpable and chases him to the edge of a cliff inside a cave.  Kristan arrives and confesses to the murders, despite Zans protestations (aimed at saving the professor) that he, the hunchback, is in fact the murderer.  As the mob watches, Kristan throws himself over the edge of the cliff and Zan follows.

==Cast==
* Ralph Morgan
* Pedro de Cordoba
* Maxine Doyle
* Russell Gleason
* Mischa Auer
* Lucy Beaumont
* Carl Stockdale Barbara Bedford
* Robert Frazer
* Ferdinand Schumann-Heink
* Heidi Shope
* Marilyn Knowlden

==Memorable Quotes==
"What good can there be in a hunchback?"  - Mob member

==Notes==
 

==References==
*http://www.missinglinkclassichorror.co.uk/demned.htm
*http://www.imdb.com/title/tt0026230/
*http://www.scifilm.org/musings/musing55.html

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 