Alice Doesn't Live Here Anymore
 
{{Infobox film
| name           = Alice Doesnt Live Here Anymore
| image          = Alice Doesnt Live Here Anymore.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| producer       = Audrey Maas David Susskind
| writer         = Robert Getchell
| starring       = Ellen Burstyn Kris Kristofferson 
| cinematography = Kent L. Wakeford
| editing        = Marcia Lucas
| distributor    = Warner Bros.
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $1.8 million   The Wrap. Retrieved April 4, 2013. 
| gross          = $21 million 
}}
Alice Doesnt Live Here Anymore is a 1974 American comedy-drama film directed by Martin Scorsese and written by Robert Getchell.  It stars Ellen Burstyn as a widow who travels with her preteen son across the Southwestern United States in search of a better life, along with Alfred Lutter as her son and Kris Kristofferson as a man they meet along the way. This is Martin Scorseses fourth film. The film co-stars Billy "Green" Bush, Diane Ladd, Valerie Curtin, Lelia Goldoni, Lane Bradbury, Vic Tayback, Jodie Foster (in one of her earliest film appearances), and Harvey Keitel.

Ellen Burstyn won the Academy Award for Best Actress and the BAFTA Award for Best Actress in a Leading Role for her performance, and the film won the BAFTA Award for Best Film.

==Plot==
When Socorro, New Mexico, housewife Alice Hyatts uncaring husband Donald is killed in an accident, she decides to have a garage sale, pack whats left of her meager belongings and take her precocious son Tommy to her childhood hometown of Monterey, California, where she hopes to pursue the singing career shed abandoned when she married.

Their financial situation forces them to take temporary lodgings in Phoenix, Arizona, where she finds work as a lounge singer in a seedy bar. There she meets the considerably younger and seemingly available Ben, who uses his charm to lure her into a sexual relationship that comes to a sudden end when his wife Rita confronts Alice. Ben breaks into Alices apartment while Rita is there and physically assaults her for interfering with his extramarital affair.  When Alice tells Ben to calm down, he threatens her also and further smashes up the apartment. Fearing for their safety, Alice and Tommy quickly leave town.
 Tucson so she can accumulate more cash. At the local diner owned by Mel, she eventually bonds with her fellow servers—independent, no-nonsense, outspoken Flo and quiet, timid, incompetent Vera and meets divorced local rancher David, who soon realizes the way to Alices heart is through Tommy.

Still emotionally wounded from the difficult relationship she had with her uncommunicative husband and the frightening encounter she had with Ben, Alice is hesitant to get involved with another man so quickly. However, she finds out that David is a good influence on Tommy, who has befriended wisecracking, shoplifting, wine-guzzling Audrey, a slightly older girl forced to fend for herself while her mother makes a living as a prostitute.

Alice and David warily fall in love, but their relationship is threatened when Alice objects to his discipline of the perpetually bratty Tommy. The two reconcile, and David offers to sell his ranch and move to Monterey so Alice can try to fulfill her childhood dream of becoming another Alice Faye. In the end, Alice decides to stay in Tucson, coming to the conclusion that she can become a singer anywhere.

==Cast==
*Ellen Burstyn as Alice Hyatt, a woman in her thirties who once worked as a singer
*Alfred Lutter as Tommy, Alices talkative preteen son
*Kris Kristofferson as David, a regular customer of Mels diner
*Billy "Green" Bush as Donald, a truck driver, Alices husband
*Diane Ladd as Florence Jean Castleberry|Flo, a hardened, sharp-tongued waitress
*Lelia Goldoni as Bea, Alices friend and neighbor in Socorro.
*Lane Bradbury as Rita
*Vic Tayback as Mel, a short-order cook who owns his own diner
*Jodie Foster as Audrey, a tomboyish girl with delinquent tendencies
*Harvey Keitel as Ben, a hot-tempered man who assembles gun ammunition for a living
*Valerie Curtin as Vera Louise Gorman-Novak|Vera, a shy, high-strung waitress
*Murray Moston as Jacobs
*Harry Northup as Joe & Jims Bartender

Director Martin Scorsese cameoed as a customer while Diane Ladds daughter, future actress Laura Dern, appears as the little girl eating ice cream in the diner.

==Production== The Exorcist Francis Coppola and asked who was young and exciting and he said Go look at a movie called Mean Streets and see what you think. It hadnt been released yet, so I booked a screening to look at it and I felt that it was exactly what . . . Alice needed, because   was a wonderful script and well written, but for my taste it was a little slick. You know – in a good way, in a kind of Doris Day-Rock Hudson kind of way. I wanted something a bit more gritty."     

Burstyn described her collaboration with director Martin Scorsese, making his first Hollywood studio production,  as "one of the best experiences Ive ever had." The director agreed with his star that the film should have a message. "Its a picture about emotions and feelings and relationships and people in chaos," he said. "We felt like charting all that and showing the differences and showing people making terrible mistakes ruining their lives and then realizing it and trying to push back when everything is crumbling – without getting into soap opera. We opened ourselves up to a lot of experimentation." 

Scorseses casting director auditioned three hundred boys for the role of Tommy before they discovered Alfred Lutter. "I met the kid in my hotel room and he was kind of quiet and shy," Scorsese said. But when he paired him with Burstyn and suggested she deviate from the script, he held his own. "Usually, when we were improvising with the kids, they would either freeze and look down or go right back to the script. But this kid, you couldnt shut him up." 

The film was shot on location in Amado, Arizona|Amado, Tucson, and Phoenix. A Mels Diner still exists in Phoenix.  
 George and Coney Island, Betty Grable is heard singing "Cuddle Up A Little Closer, Lovey Mine" by Otto A. Harbach and Karl Hoschna; and in a film clip from Hello Frisco, Hello, Alice Faye performs "Youll Never Know" by Harry Warren and Mack Gordon.

==Reaction==
===Critical response===
Upon its 1974 release, the film was near-unanimously praised by the critics, and grossed $21,044,810 worldwide.  On Rotten Tomatoes, the film has a 95% "Fresh" rating. 

Vincent Canby of The New York Times called it a "fine, moving, frequently hilarious tale" and observed it "is an American comedy of the sort of vitality that dazzles European film critics and we take for granted. Its full of attachments and associations to very particular times and places, even in the various regional accents of its characters. Its beautifully written . . . and acted, but its not especially neatly tailored . . . At the center of the movie and giving it a visible sensibility is Miss Burstyn, one of the few actresses at work today . . . who is able to seem appealing, tough, intelligent, funny, and bereft, all at approximately the same moment. Its Miss Burstyns movie and part of the enjoyment of the film is in the directors apparent awareness of this fact . . . Two other performances must be noted, those of Diane Ladd and Valerie Curtin . . . Their marvelous contributions in small roles are a measure of the films quality and of Mr. Scorseses fully realized talents as one of the best of the new American film-makers." 

Roger Ebert of the Chicago Sun-Times called the film "one of the most perceptive, funny, occasionally painful portraits of an American woman Ive seen" and commented, "The movie has been both attacked and defended on feminist grounds, but I think it belongs somewhere outside ideology, maybe in the area of contemporary myth and romance."  Ebert put the film at #3 of his list of the best films of 1975 (even though the film came out in 74). 

The film did not go without its detractors, however. Variety (magazine)|Variety thought the film was "a distended bore," saying it "takes a group of wellcast film players and largely wastes them on a smaller-than-life film - one of those little people dramas that makes one despise little people." 

TV Guide rated the film three out of four stars, calling it an "effective but uneven work" with performances that "cannot conceal the storylines shortcomings." 

===Accolades=== Murder on the Orient Express, and Robert Getchell was nominated for the Academy Award for Best Original Screenplay but lost to Robert Towne for Chinatown (1974 film)|Chinatown.
 Best Actress Best Actress Best Screenplay. Best Direction but lost to Stanley Kubrick for Barry Lyndon.
 Best Actress Best Supporting Actress in a Motion Picture, respectively, and Scorsese was nominated for the Palme DOr at the 1975 Cannes Film Festival. 

==References in popular culture== Howard Jones song "Look Mama" features a section of dialogue from this film.

==Television adaptation== pilot episode but was replaced by Philip McKeon for the series. Diane Ladd joined the show later in its run, but in a role different from that she had played in the film.

==Home media==
Warner Home Video released the film on Region 1 DVD on August 17, 2004. It is in anamorphic widescreen format with audio tracks in English and French and subtitles in English, French, and Spanish. Bonus features include commentary by Martin Scorsese, Ellen Burstyn, and Kris Kristofferson and Second Chances, a background look at the making of the film.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 