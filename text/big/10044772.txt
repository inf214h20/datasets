Parapalos
{{Infobox film
| name           = Parapalos
| image          = Parapalos poster small.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Ana Poliak
| producer       = Ana Poliak
| screenplay     = Ana Poliak Santiago Loza Adrián Suárez
| narrator       = 
| starring       = Adrián Suárez
| music          = Fulanen de Talen
| cinematography = Alejandro Fernández Mouján Víctor González
| editing        = Ana Poliak
| studio         = Viada Producciones
| distributor    = Cinemagroup
| released       =  
| runtime        = 90 minutes
| country        = Argentina Belgium Spanish
| budget         = 
| gross          = 
}} Argentine and Belgian film, directed by Ana Poliak, and written by Poliak and Santiago Loza. 

==Plot==
The film tells of a Ringo (Adrian Suarez), a young man who moves from the city to the country, and moves in with his cousin Nancy (Nancy Torres).  He takes a job as a Pin Boy at the local bowling alley.

Ringo has to go through to a rather physical and extensive training session, and is warned that the job is physically demanding and hazardous and he is given no health insurance.

But Ringo takes the job with enthusiasm and seems content doing the physical work.  He listens to the folktales of his older, more experienced co-workers, particularly the well-traveled former hippie who calls himself Nippur.

Ringo comes home from work as Nancy goes out to her job, and they share breakfast before she leaves.  They establish a good relationship that develops into a good friendship.

==Cast==
* Adrián Suárez
* Roque Chappay
* Armando Quiroga
* José Luis Seytón Guzmán
* Nancy Torres
* Dorian Waldemar

==Background==
In an interview with a social justice journalist David Walsh at the Buenos Aires 6th International Festival of Independent Cinema, director and producer Ana Poliak discussed why she made the film.  She said, "It was the first time that I had the feeling that we were not all equal.... I could see behind the back walls of the alley, where I saw kids my age, naked from the waist up, who were working very, very hard. I couldnt quite understand the situation.... During the match I would concentrate on the boys feet and hands, and I felt that on the other side there was another world, parallel to mine, which I couldnt comprehend. I started from this idea to make the film," she added, "This is connected, in some way, to the differences in social classes that I discovered when I was little, and I guess thats why Im so interested in this type of character. I cant find answers for these questions. I think that my social class doesn’t have that capacity, that light." 

==Reception==
 
===Critical response===
Film critic Doug Cummings liked the film and wrote, "Poliaks DV camera maintains a steady gaze, intensifying the subtleties of the workers conversations in their cramped and shadowy confines with tight compositions. The films careful sound design emphasizes the ambient noises and shapes them to reflect Adri·n’s subjective experience. In many ways, the film’s formal claustrophobia is reminiscent of Lucrecia Martels The Holy Girl, but Pin Boy is far less lush, emphasizing its austere and potentially dangerous environment with flat lighting and compelling, matter-of-fact realism."  

===Awards===
Wins
* Buenos Aires International Festival of Independent Cinema: Best Film; Ana Poliak; 2004.
* Entrevues Film Festival, France: Grand Prix Foreign Film, Ana Poliak; 2004.

==Film Festivals==
* Belfort Entrevues Film Festival
* Buenos Aires International Festival of Independent Cinema
* Cinemanila International Film Festival
* La Rochelle Film Festival
* London Film Festival
* Oslo International Film Festival
* Rio de Janeiro International Film Festival
* San Francisco International Film Festival
* Thessaloniki International Film Festival
* Toulouse Latin America Film Festival
* Wisconsin Film Festiva

==References==
 

==External links==
*  
*  
*   at the cinenacional.com  

 
 
 
 
 
 