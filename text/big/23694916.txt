Passport to Destiny
{{Infobox Film
| name           = Passport to Destiny
| other titles   = Passport to Adventure Dangerous Journey
| image          = Pptdestpos.jpg
| image_size     = 200px
| caption        = Original film poster
| director       = Ray McCarey
| producer       = Herman Schlom
| writer         = Muriel Roy Bolton Val Burton
| starring       = Elsa Lanchester Lenore Aubert Fritz Feld
| music          = Roy Webb
| cinematography = Jack MacKenzie
| editing        = Robert Swink
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 assassinate Adolf Hitler.

==Plot== India that protected him from all harm. Whilst cleaning her attic, she goes through her husbands effects and finds the charm that she absent-mindedly puts in the pocket of her skirt.
 air raid warden tells her to run, another to lay down. She does the latter and survives the explosion, though she is helped to the shelter in a daze.  As she recovers, she is convinced that her husbands "Magic Eye" charm has protected her.  She asks a friend what she would do if she were totally invulnerable. Looking up to the street being bombed, her friend replies that she would go to Germany and "give that Mr. Hitler what for". Ella leaves the shelter, unconcerned about the bombs exploding around her, as she sets out to do just that.
 Stowing away on a British merchant ship, Ella is discovered by the crew, who think having a woman aboard is bad luck; subsequently, a German bomber sinks the ship. Ella reaches France in a lifeboat, where the other survivors are quickly captured by the Germans. Ella works her way across France and Germany, pretending to a deaf-mute cleaning woman. She shares a train compartment with German Captain Franz Von Weber (Gordon Oliver). Franzs fiancees uncle, Frederick Walthers (Lloyd Corrigan) arrives, and she is asked to leave the compartment.  Both men are members of the anti-Hitler German resistance. Walthers informs Franz that Grete (Lenore Aubert), his fiancee, has been arrested. Franz is determined to rescue her.
 Herr Joyce Gavin Muir), Lord Haw", comes to complain about his treatment.  Dietrich is unconcerned, as Joyces usefulness is rapidly diminishing. On his way out, Joyce slips on a bar of soap Ella has carefully placed. Ella also overhears that Grete is being held in Mobit Prison.

When Franz tries to see Dietrich, Ella writes the message "Grete Mobit" on the floor. Noticing Ellas brush says "Champion: Made in England", Franz later hears the supposedly deaf and dumb woman singing in English, and realizes Ella is not who she seems. Outside, she lends him her Magic Eye to rescue Grete. Franz is able to have Greta released, but it is actually a ploy by Dietrich; he has the couple followed in hopes they will lead him to other members of the German Resistance. 

 
Inside Hitlers private office, Ella rehearses what she will say to him, but Dietrich is eavesdropping on the intercom. Lord Haw-Haw enters and begs Ella to help him escape from Germany. Both are arrested, as are Frederick, Franz and Grete. After Dietrich gives Ella back the eye, the Royal Air Force bombs the Chancellery.  Frederick is killed, but Ella, Franz and Grete take advantage of the confusion to escape to an airfield, where Franz steals a bomber. They fly to England and land by parachute. 

Feted as a heroine, Ella shows a reporter her husbands chest where she found the amulet, but discovers many more in a box labeled as souvenirs of a glass blowers exhibition.

==Cast==
As credited, with screen roles identified: 
* Elsa Lanchester as Ella Muggins  
* Gordon Oliver as Capt. Franz von Weber  
* Lenore Aubert as Grete Neumann  
* Lionel Royce as Sturmfuehrer Karl Dietrich  
* Fritz Feld as Chief Janitor  
* Joseph Vitale as Lt. Bosch   Gavin Muir as Herr Joyce / Lord Haw  
* Lloyd Corrigan as Prof. Frederick Walthers  
* Anita Sharp-Bolster as Agnes (as Anita Bolster)  
* Lydia Bilbrook as Millie  
* Lumsden Hare as Freighter Captain Mack  
* Hans Schumm as Miniger, Dietrichs Aide  

==Production==
In August 1943, no less than six Hollywood films about Adolf Hitler or with Hitler as a title (i.e. Hitlers Children (film)|Hitlers Children, Hitler – Dead or Alive, The Strange Death of Adolf Hitler, The Hitler Gang) were in production. Principal photography took place from late August to mid-September 1943.  Originally filmed as Dangerous Journey, the title was changed to Passport to Adventure perhaps due to the similarity to Warners Desperate Journey then to Passport to Destiny. The production was rushed through "before some soldier beats Elsa to Hitler".   Turner Classic Movies. Retrieved: May 19, 2013. 

Two actors were "borrowed" from other studios: Gordon Oliver came from David O. Selznick Productions and Lenore Aubert from Samuel Goldwyns company to appear in the film. Ray McCarey, the brother of Leo McCarey, was signed on a contract to Republic Pictures after the film. 

For the climatic escape scene, the studio again used the Capelis XC-12 transport that was on the backlot, this time painted to resemble a Luftwaffe bomber.  

==Reception==
Passport to Destiny was intended to be a comedy but despite the best efforts of its star, the contrived scenario of a Hitler assassination are described as doomed by a "screenplay  (that) was quintessential tommyrot, undiluted by even the smallest amount of intelligence or common sense." Jewell and Harbin 1982, p. 193.  Only Lanchesters charm "made it bearable with her unique histrionic abilities."  A more recent critique notes, "The movie is indeed charming and amusing; at least, it is for the length of time that it plays its story for silly comedy. Unfortunately, but not unexpectedly, the movie starts to take itself seriously at the half-way mark ..."   Film critic Leonard Maltin described it as a "tidy programmer". 

==References==
===Notes===
 

===Citations===
 
===Bibliography===
 
* Farmer, James H. Broken Wings: Hollywoods Air Crashes. Missoula, Montana: Pictorial Histories Pub. Co., 1984. ISBN 978-9-999926-515.
* Jewell, Richard and Vernon Harbin. The RKO Story. New Rochelle, New York: Arlington House, 1982. ISBN 978-0-70641-285-7.
* Kenny, Mary.Germany Calling: A Personal Biography of William Joyce, Lord Haw-Haw. Dublin: New Island Books, 2013. ISBN 978-1-84840-007-8.
* Lanchester, Elsa. Elsa Lanchester, Myself. New York: St. Martins Press, 1984. ISBN 978-0-31224-377-7.
* Nesbit, Roy Conyers and Georges van Acker. The Flight of Rudolf Hess: Myths and Reality. Stroud, UK: History Press, 2011. ISBN 978-0-7509-4757-2.
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 