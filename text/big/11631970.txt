Buddies Thicker Than Water
 
{{Infobox Hollywood cartoon
|cartoon_name=Buddies Thicker Than Water
|series=Tom and Jerry
|image=Buddies_Thicker_Than_Water.jpg
|director=Gene Deitch   
|producer=William L. Snyder
|story_artist=Larz Bourne
|musician=Steven Konichek
 Rembrandt Films
|distributor=Metro-Goldwyn-Mayer
|release_date=November 1, 1962
|color_process=Metrocolor
|runtime=8:00
|country=Czechoslovakia/Czech Republic United States
|language=English
|preceded_by=Sorry Safari
|followed_by=Carmen Get It!
}}
Buddies Thicker Than Water is a 1962 short film, originally released as part of the Tom and Jerry series on November 1. It was the 12th and penultimate cartoon in the series to be directed by Prague-based animator Gene Deitch in then-Communist Czechoslovakia (now the Czech Republic) and produced by William L. Snyder. The name is a pun on the phrase "Blood is thicker than water".
 psychedelic and experimental nature. It was chosen for the eighth volume of the Tom & Jerry Bumper Collection VHS series.

==Plot==
On a snowy night in New York City, Jerry is comfortably asleep in his hole inside a penthouse, while Tom tries to keep from freezing to death in the alley below, after being thrown out by his owner. He writes a note, slips it into a bottle, and throws it up to hit the penthouse window. Jerry, awakened by the noise, goes out to the balcony and finds both this note and a second one sent up by Tom:

 

Rushing to the alley, Jerry finds the frozen-solid Tom and drags him back upstairs on a trash can lid. He then sets Tom inside the hot-air vent, thaws him out with an electric blanket,and provides him with an "Instant Gourmet" dehydrated meal.

Tom and Jerry lounge about the penthouse, listening to music and drinking everything in the owners liquor cabinet. However, the owner returns and startles the inebriated pair. Jerry dives into his hole as she grabs Tom and prepares to throw him out again. Tom grabs Jerry and shows him to the woman, throwing her into a panic until he throws Jerry off the balcony into the alley. Whilst Tom is being stroked and pampered by the owner, Jerry is furious at the cats betrayal and wants revenge.

Upon entering the house, Jerry sneaks past Tom and goes over to the womans make-up table, where he applies her face powder all over his body to look like a ghost. Putting on an album of spooky sound effects, Jerry chases the terrified Tom outside, but some of the powder washes off as Jerry advances through the snow

Tom is furious and unsheathes his claws, wanting to kill Jerry for his trick, but as he runs towards him, he slips in the snow and falls down to the alley below. Tom sends Jerry another note: "Help! Its freezing down here! Your old pal, Tom." Jerry responds by throwing a pair of ice skates and an ice hockey stick down to him, then happily returns to his hole and goes to sleep.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 