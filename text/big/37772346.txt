Red Wine (film)
{{Infobox film
| name           = Red Wine
| image          = Red Wine (film).jpg
| caption        = Theatrical release poster
| director       = Salam Bappu
| producer       = A.S. Gireesh Lal
| story          = Noufal Blathoor 
| screenplay     = Mammen K. Rajan Mia Meghana Satheesh Menon
| music          = Bijibal
| cinematography = Manoj Pillai
| editing        = Ranjan Abraham 
| studio         = Gowri Meenakshi Movies
| distributor    = Reelax Eveents & Tricolor Entertainments.
| released       =   
| runtime        = 146 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} nonlinear investigation film directed by Salam Bappu. The film is produced by Gireesh Lal under the banner Gowri Meenakshi Movies and distributed by Reelax Eveents. It stars Mohanlal with Fahad Fazil and Asif Ali in lead roles.  The film is scripted by newcomer Mammen K. Rajan based on a story by Noufal Blathoor. The films soundtrack and background score were composed by Bijibal, with lyrics penned by Rafeeq Ahammed and Santhosh Varma. The film released on 21 March 2013. 

==Plot Summary==
Assistant Commissioner of Police Ratheesh Vasudevan (Mohanlal), tries to solve the murder of Anoop C V (Fahadh Faasil), a part-time actor in local plays. During the investigation, Vasudevan learns that the victim was involved in Leftist activities and opted for helping the local population rather than using his engineering degree to get a well paying job, Ramesh (Asif Ali) is a local vendor who is in debt and Navas (Saiju Kurup) is a friend of Anoop who makes films for advertisements.

==Cast==
* Mohanlal as ACP Ratheesh Vasudevan
* Fahadh Faasil as C.V. Anoop
* Asif Ali as K.P.Ramesh Kumar
* Saiju Kurup as Navas Paramban
* Jayaprakash as Dr.Paul Alexander
* Mia George as Deepthi
* Anusree as Sreelakshmi
* Maria John as Jasmin
* Meghana Raj as R.D.O Ann Mary
* Meera Nandan as Jasna
* Suraj Venjaramoodu as Joe Sebastian
* T. G. Ravi as Narayanettan
* Priyanandanan as Unniyettan
* Sunil Sugutha as Rathnakaran Pillai
* Jayakrishnan as Venugopal. S Satheesh Menon as Philipose Koshy
*Sudheer Karamana  as SI Rafi
* Roshan Basheer as Alfy Kailash as Fasaludheen
* Ambika Mohan as Elsey
* Sreejith as Raghu
* Anoop Chandran as Abhilash Irshad as Dinesh
* Nandan Unni as Kunjikannan
* Sundara Pandian as Shibu Mon
* Shivaji Guruvayoor as Commissioner
* Chithara Bhanu as Chandrettan 
* Favour Francis as Father
* Moideen Koya as Principal
* Nandhu Poduval as Xavier
* Dr. Venugopal as Doctor
* Manraj as Vinay
* Mithun as Sooraj
* Navaneeth as Shyam
* Naslim as Gautham
* Kozhikode Vilasini as Party Member
* Kozhikode Ramadevi as Ammini Chechi
* Navamy as Jincy
* Mahima as Lalitha Chechi 
* Musthafa as Villager
* Milesh as Villager
* Gopan as Villager
* Koshy as S.I
* Abhilash as S.I.
* Master Eric as Ebel 
* Baby Anjana as Adheena Navas

==Production==
The film was shot on locations in Kozhikode and Wayanad. The film began its shoot and held its pooja or launch on November 30, 2012.

==Marketing==
The films first teaser was released on December 22, 2012,  and a theatrical trailer was released on March 5, 2013.  The Promo Song was released on March 18, 2013. 

==Release==
Red Wine was awarded with U certificate by the Central Board of Film Certification.

===Reception===
The film has received mixed reviews from film critics

* Aswin of The Times of India says that "Red Wine makes it too easy, too effortless and the thin element of suspense wanes no sooner than it even appears". 

* Sify|Sify.com rated the film as "Above Average", stating that "Red Wine is not bad, but it is not great either. If you are not looking forward to some modern pattern, the film may be a fine option as well". 

* Paresh of Rediff.com says that "Red Wine is worth watching for its approach and its lead actors". 

* Nirmal of Oneindia.in says that "Red Wine is an interesting film by the super combo of Mohanlal, Fahad Fazal and Asif Ali, a routine murder mystery, but a watchable one". 

* Indiaglitz.com says that "Red Wine is a clean bit of film which can definitely be prescribed for families. A one time watch for others though it is quite predictable to the core". 

The film was below average by box office.
==Controversy==
The storyline has an eerie similarity with the case involving the murder of Muthoot Finance Group scion, Paul M George in 2009. However, there is a twist in the end of the film where instead of the resort developer, the activist against the land dealing gets murdered. In true life, the developer gets murdered.

==Soundtrack==
{{Infobox album 
| Name = Red Wine
| Artist = Bijibal
| Type = Soundtrack
| caption = Original CD cover
| Cover =
| Released =  
| Recorded = 2012
| Genre = Film soundtrack
| Length = 12:32
| Language = Malayalam Mathrubhumi Music
| Producer = A.S. Gireesh Lal
| Last album = Da Thadiya (2012)
| This album = Red Wine (2013) Thank You (2013)
}}
 Mathrubhumi Music.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 12:32
| lyrics_credits = yes
| title1 = Ilam Veyil
| lyrics1 = Santhosh Varma
| extra1 = Najeem Arshad, Veetraag, Prashanth
| length1 = 4:49
| title2 = Neelaakaasham
| lyrics2 = Santhosh Varma
| extra2 = Aditi Gopinathan
| length2 = 3:25
| title3 = Akaluvathenthino
| lyrics3 = Rafeeq Ahammed
| extra3 = Job Kurian
| length3 = 4:20
}}

==References==
{{Reflist|refs=
   The Times of India   
   Sify  
   Rediff   
   Oneindia.in|OneIndia   
    
}}

==External links==
*  
*  

 
 
 
 
 
 

 