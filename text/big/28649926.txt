Milka – A Film About Taboos
 
{{Infobox film
| name           = Milka – A Film About Taboos
| image          = 
| caption        = 
| director       = Rauni Mollberg
| producer       = Rauni Mollberg
| writer         = Rauni Mollberg Timo K. Mukka
| starring       = Irma Huntus
| music          = 
| cinematography = Markku Lehmuskallio
| editing        = Tuula Mehtonen
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Milka – A Film About Taboos ( ) is a 1980 Finnish drama film directed by Rauni Mollberg. It was entered into the 31st Berlin International Film Festival.   

==Plot==
Milka is an 14 years old still childlike girl, who lives with her mother in a tiny isolated community in Northern Finland. The girl misses her dead father and prays God to show her what love is. The mother employs a man nicknamed Christ-Devil to help in the hay making. He stays in the house and courts both the mother and daughter. Later he vanishes unexpectedly and the mother, who wished to marry him, notices that Milka is pregnant by him.

==Cast==
* Irma Huntus - Milka Sierkkiniemi
* Leena Suomu - Anna Sierkkiniemi
* Matti Turunen - Ojanen, Kristus-Perkele
* Eikka Lehtonen - Cantor Malmström
* Esa Niemelä - Auno Laanila
* Hellin Auvinen-Salmi - Villager
* Sirkka Metsäsaari - Mother Laanila
* Ulla Tapaninen - Ojanens woman friend
* Toivo Tuomainen - Villager
* Tauno Lehtihalmes - Dean

==References==
 

==External links==
* 

 
 
 
 
 
 
 