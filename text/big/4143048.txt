36 Fillette
{{Infobox film
| name           = 36 Fillette
| image          = 36fillette.jpg
| caption        = DVD cover
| director       = Catherine Breillat
| producer       = Emmanuel Schlumberger Valérie Seydoux
| writer         = Catherine Breillat Roger Salloch
| narrator       = 
| starring       = Delphine Zentout Oliver Parniere
| music          = Maxime Schmitt
| cinematography = Laurent Dailland
| editing        = Yann Dedet
| distributor    = Société des Etablissements L. Gaumont
| released       =  
| runtime        = 88 min.
| country        = France
| language       = French
| budget         = 
}} French film starring Delphine Zentout and Oliver Parniere, directed by Catherine Breillat. It is the story of a sexually curious and rebellious 14-year-old (played by 16-year-old Zentout) who has an emotionally charged and dually manipulative relationship with an aging playboy. 
Breillat is known for films focusing on sexuality, intimacy, gender conflict and sibling rivalry. Breillat has been the subject of controversy for her explicit depictions of sexuality.

==Plot==
Lili, a pouty and voluptuous 14-year-old, is caravan camping with her family in Biarritz. Shes self-aware and holds her own in a café conversation with a concert pianist she meets, but she has a wild streak and shes testing her powers over men, finding that she doesnt always control her moods or actions, and shes impatient with being a virgin. She sets off with her brother to a disco, latching onto an aging playboy who is himself hot and cold to her. She is ambivalent about losing her virginity that night, willing the next, and determined by the third. The playboys mix of depression and misogyny ends their unconsummated affair, so Lili has to hunt elsewhere at the campground, eventually finding an awkward teen her own age who clumsily deflowers her.

==Cast==
* Delphine Zentout as Lili
* Etienne Chicot as Maurice
* Olivier Parnière as Bertrand
* Jean-Pierre Léaud as Boris Golovine
* Berta Domínguez D. as Anne-Marie
* Jean-François Stévenin as The father
* Diane Bellego as Georgia
* Adrienne Bonnet as The mother
* Stephane Moquet as Ca-Pe
* Cécile Henry as Maetitia
* Michel Scotto di Carlo as Stéphane
* Anny Chasson as Mme Weber
* Jean-Claude Binoc as M. Weber
* Christian Lafitte as Le conducteur
* Christian Andia as Doorman Opium

==References==
 

==External links==
* 
 

 
 
 


 