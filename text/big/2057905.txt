Earth vs. the Flying Saucers
{{Infobox film
| name           = Earth vs. the Flying Saucers
| image          = Earth vs the Flying Saucers DVD.jpg Theatrical release poster
| director       = Fred F. Sears
| producer       = Charles H. Schneer Sam Katzman Bernard Gordon Joan Taylor 
| music          = Mischa Bakaleinikoff
| cinematography = Fred Jackman Jr. 
| editing        = Danny B. Landres
| studio         = Clover Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States English
| budget         = 
| gross = $1,250,000    
}}
 American science Joan Taylor. 

The films storyline was suggested by the bestselling, non-fiction book Flying Saucers from Outer Space by Maj. Donald Keyhoe. Jacobs  .  

The stop-motion animation special effects in the film were created by Ray Harryhausen.
 
==Plot== alien involvement. The Marvins then witness the 11th falling from the sky shortly after launch.

When a saucer lands at Skyhook the next day, soldiers open fire, killing one exposed alien, while others and the saucer itself are being protected by a  , to negotiate an alien occupation.
 auditory perception. From other observations, Marvin develops a counter-weapon against their flying saucers, which he then later successfully tests against a single saucer. After doing so, as they escape, the aliens jettison Gen. Hanley and the motor cycle cop; both fall to their deaths. Groups of alien saucers then attack Washington, Paris, London, and Moscow but are destroyed by Dr. Marvins sonic weapon. The defenders also discover that the aliens can be easily killed by simple small arms gunfire once they are outside the force fields of their saucers. 

With the alien threat eliminated, Dr. Marvin and Carol quietly celebrate the victory by going back to their favorite beach, resuming their lives as a newly wed couple.

==Cast==
 
* Hugh Marlowe as Dr. Russell A. Marvin Joan Taylor as Carol Marvin
* Donald Curtis as Major Huglin, the liaison officer
* Morris Ankrum as Brig. Gen. John Hanley
* John Zaremba as Prof. Kanter
* Thomas Browne Henry as Vice-Admiral Enright
* Grandon Rhodes as General Edmunds
* Larry J. Blake as a motorcycle policeman Charles Evans as Dr. Alberts
* Paul Frees as Alien (voice)
* Harry Lauter as Cutting - Generator Technician
 

==Production==
===Visual effects=== HMS Barham during  World War II was used for the U. S. Navy destroyer that is sunk by a flying saucer. Satellite launch depictions made use of stock film images from a Viking rocket launch and a failure of a German V-2 rocket. Wood, Bret.   Turner Classic Movies. Retrieved: January 6, 2015. 

The voice of the aliens was produced from a recording made by Paul Frees (uncredited) reading their lines and then hand-jiggling the speed control of an analog reel-to-reel audio tape recording|reel-to-reel tape recorder, so that it continually wavered from a slow bass voice to one that is high and fast. 
 Jason and the Argonauts held in Sydney, Australia, Harryhausen said he sought advice from noted 1950s UFO "contactee" George Adamski on the depiction of the flying saucers used in the film. He also noted that Adamski appeared to have grown increasingly paranoid by that time. The films iconic flying saucer design (a static central cabin with an outer rotating ring with slotted vanes) matches descriptions given to Maj. Donald Keyhoe of flying disc sightings in his best-selling flying saucer book. 

==Reception==
Earth vs. the Flying Saucers was well received by audiences and critics alike, with Variety (magazine)|Variety noting that the special effects were the real stars of the film. "This exploitation programmer does a satisfactory job of entertaining in the science-fiction class. The technical effects created by Ray Harryhausen come off excellently in the Charles H. Schneer production, adding the required out-of-this-world visual touch to the screenplay, taken from a screen story by Curt Siodmak, suggested by Major Donald E. Keyhoe’s Flying Saucers from Outer Space."   

Earth vs. the Flying Saucers has reached an iconic status in that many films in the "flying saucer" subgenre that followed, imitated and incorporated many of the elements established by Ray Harryhausen.  In an article for The New York Times film reviewer Hal Erickson noted that, "Anyone whos seen the 1996 science-fiction lampoon Mars Attacks may have trouble watching Earth vs. the Flying Saucers with a straight face." The later campy film could be seen as an homage to the era and especially, to the contributions made by Earth vs. the Flying Saucers. 

==Legacy==
Several films have recycled stock footage from the film, including The Giant Claw and The 27th Day (1957), an episode of the Twilight Zone (1985) and the short Flying Saucer Daffy  (1958).

==References==
Notes
 

Bibliography
 
* Hagerty, Jack and Jon Rogers. The Saucer Fleet. Burlington, Ontario, Canada: Apogee Books, 2008. ISBN 978-1894959-70-4.
* Hankin, Mike. Ray Harryhausen: Master of the Majicks, Vol. 2: The American Films. Los Angeles, California: Archive Editions, 2008. ISBN 978-0-98178-290-4.
* Jacobs, David. "Flying Saucers from Outer Space – The inspiration behind Earth vs. the Flying Saucers". in Wilson, S. Michael. Monster Rally: Mutants, Monsters, Madness. West Orange, New Jersey: Idea Men Production, 2008. ISBN 978-1-4392-1519-7.
 

==External links==
*  
*  
*  


 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 