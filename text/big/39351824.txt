The Return of Monte Cristo
{{Infobox film
| name           = The Return of Monte Cristo
| image          = 
| caption        = 
| director       = Henry Levin
| producer       = Edward Small Grant Whytock
| screenplay     = George Bruce Alfred Neumann
| based on       = story by Curt Siodmak  
| starring       = Louis Hayward George Macready
| music          =
| cinematography =
| editing        = 
| studio         = Columbia
| distributor    = Columbia Pictures
| released       = 1946
| runtime        = 92 mins
| country        = United States
| language       = English
| budget         = $658,284 Bernard Dick, The Merchant Prince of Poverty Row : Harry Cohn of Columbia Pictures Lexington, Ky. : University Press of Kentucky, c1993. p 135 
| gross          = 
}} The Count of Monte Cristo (1934).
 

==Plot==
The grandson of the Count of Monte Cristo is falsely accused of a crime and imprisoned on Devils Island. He escapes and seeks revenge against those responsible for his imprisonment.

==Production==
Edward Small made the film in collaboration with Columbia Studios, using an old commitment he had with Louis Hayward. PARAMOUNT BUYS HARVESTING STORY: Studio Will Produce Houston Branchs The Big Haircut --Lead to Alan Ladd
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   11 May 1946: 34.  Hayward was paid $35,000. 

==Reception==
Reviews were positive. Return of Monte Cristo Exciting Drama
Scott, John L. Los Angeles Times (1923-Current File)   15 Jan 1947: A2.  Edward Small announced plans to star Louis Hayward in The Treasure of Monte Cristo but no film resulted. Again, Monte Cristo! Beery Jr. Joins Scout
Schallert, Edwin. Los Angeles Times (1923-Current File)   15 June 1948: 22.  SMALL PLANS FILM ON MONTE CRISTO: Seeks Louis Hayward for Lead in Movie on Dumas Hero -- Beloin Doing Hope Script 1949 and 1961 but not from Small.)

==References==
 

==External links==
*  at IMDB
*  
 
 
 

 
 
 
 
 
 
 
 
 


 