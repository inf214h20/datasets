It's a Big Country
{{Infobox film
| name           = Its a Big Country
| image_size     =
| image	         = Its a Big Country FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Clarence Brown John Sturges Richard Thorpe Charles Vidor Don Weis William A. Wellman
| producer       = Robert Sisk John McNulty George Wells
| narrator       =
| starring       = Ethel Barrymore Gary Cooper Van Johnson Gene Kelly Janet Leigh Marjorie Main Fredric March George Murphy William Powell James Whitmore
| cinematography = John Alton Ray June William C. Mellor Joseph Ruttenberg
| editing        = Ben Lewis Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $1,013,000  .  Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008, p. 60  
| gross          = $655,000 }}

Its a Big Country is a 1951 anthology film directed by 6 directors: Clarence Brown, John Sturges, Richard Thorpe, Charles Vidor, Don Weis, and William A. Wellman.

==Plot==
A professor traveling on a train overhears a fellow passenger make a comment about "America". The professor then asks: "Which America?" This provides a lead-in for multiple tales of American life. There is the tale of Mrs. Riordan, an elderly lady from Boston. She is upset about not having being counted in the 1950 census. She asks a newspaper editor named Callaghan to intervene on her behalf, and he makes the mistake of not taking her seriously.

There is the story of a Hungarian immigrant named Stefan Szabo who is in the business of selling paprika. He has several daughters and does not want them to marry men of other nationalities. Rosa falls in love with Icarus, who is Greek, and must overcome her fathers objections. There is the tale of Maxie Klein, a young Jewish man. He looks up the mother of a young man who died in the war. The mother is not sure what to make of Maxie because her son mentioned no Jewish friend, but ends up touched by his visit. So many tall tales about Texas exist that a tall Texan man takes it upon himself to separate the fact from the fiction.

Adam Burch, a minister in Washington, D.C., whose parishioners include the President of the United States, sometimes tailors his sermons specifically for the President, only to learn later that the President was unable to attend services that day. Scolded to speak for all rather than to one, Rev. Burch gives the sermon of his life, then learns to his surprise that the President was present on that day and heard every word. Miss Coleman, a school teacher in San Francisco, discovers that her pupil Joey needs glasses. However, Joeys father, Mr. Esposito, believes they are not necessary and will only bring Joey ridicule from his peers. In the end, it is the father who learns an important lesson.

==Cast==
* William Powell as the Professor
* James Whitmore as the Passenger
* Ethel Barrymore as Mrs. Riordan
* George Murphy as Callaghan
* Gene Kelly as Icarus
* Janet Leigh as Rosa
* S.Z. Sakall as Mr. Szabo
* Keefe Brasselle as Maxie Klein
* Gary Cooper as the Texan
* Van Johnson as Rev. Burch Nancy Davis as Miss Coleman
* Fredric March as Papa Esposito

==Reception==
According to MGM records the film earned $526,000 in the US and Canada and $129,000 elsewhere, resulting in a loss to the studio of $677,000. 

==References==
 
 

==External links==
*  
*   
*  

 
 
 

 
 
 
 
 
 
 
 
 
 


 