Parsifal (1904 film)
  Edwin S. Porter. It is based on the 1882 opera Parsifal by Richard Wagner, and stars Adelaide Fitz-Allen as Kundry and Robert Whittier as Percival|Parsifal. Niver, Kemp (1967). Motion Pictures From The Library of Congress Paper Print Collection 1894-1912. University of California Press, ISBN 978-0520009479 

== Production and release ==
In 1903, Wagners widow unsuccessfully attempted to stop a performance of Parsifal by the Metropolitan Opera in New York, causing great scandal. In 1904, Harley Merry acquired the motion picture rights and brought on Porter and Edison. Edison had been experimenting with ways to combine silent films with recorded music. Porters version of Parsifal employed Edisons Kinetophone, "a primitive, synchronized sound-mix device, but this machine does not represent the era of silent film, since it had a short life with no great success." Olsen, Solveig (2007). Opera Quarterly, Volume 22, Issue 2, pp. 369-375, doi: 10.1093/oq/kbl004 

Despite a strong advertising campaign, sales were modest. Musser, Charles (1991). Before the Nickelodeon: Edwin S. Porter and the Edison Manufacturing Company. University of California Press, ISBN 9780520060807 

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 