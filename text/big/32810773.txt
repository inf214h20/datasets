A Man and His Dog
{{Infobox film
| name      = A Man and His Dog
| director  = Francis Huster
| producer  = Jean-Louis Livi 
| writer    = Francis Huster Murielle Magellan Cesare Zavattini
| starring  = Jean-Paul Belmondo Hafsia Herzi Julika Jenkins Francis Huster Max von Sydow Jean Dujardin
| music     = Philippe Rombi
| cinematography = Vincent Jeannot
| released  =  
| runtime   = 109 minutes
| country   = France
| language  = French
| budget    = $7,342,756
| gross     = $3,016,932  203,872 admissions (France) 
}}
A Man and His Dog (Un Homme et Son Chien) is a 2008 French film directed by French director Francis Huster, starring Jean-Paul Belmondo, based on the 1952 film Umberto D. directed by Vittorio De Sica, and written by Cesare Zavattini.

It was the first film in seven years featuring Jean-Paul Belmondo due to his having had a stroke. 

== Plot ==
Charles is an old retiree who lives in a maids room in the house of his lover, a rich widow. He is forced out onto the street with his dog after the widow breaks off the relationship, as she decides to marry again. With no home nor way to make money, they wander the streets of Paris.

== Cast ==
* Jean-Paul Belmondo - Charles
* Hafsia Herzi - Leïla
* Julika Jenkins - Jeanne
* Francis Huster - Robert
* Max von Sydow - Commandier
* Jean Dujardin - Worker
* José Garcia (actor)|José Garcia - Passenger
* Caroline Silhol 
* Michèle Bernier 
* Daniel Prévost
* Françoise Fabian 
* Cristiana Reali
* Tcheky Karyo 
* Pierre Mondy 
* Antoine Duléry
* Charles Gérard 
* Patrick Bosso
* Jean-Luc Lemoine 
* Dolores Chaplin
* Barbara Schulz 
* Sarah Biasini
* Bruno Lochet 
* Rachida Brakni 
* Daniel Olbrychski 
* Aurélien Wiik
* François Perrot
* Nicole Calfan
* Steve Suissa 
* Jean-Marc Thibault 
* Robert Hossein 
* Jean-Pierre Bernard 
* Micheline Presle
* Emmanuelle Riva
* Jacques Spiesser
* Pierre Cassignard 
* Carlo Nell
* Linda Hardy

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 