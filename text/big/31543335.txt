Puthukkottayile Puthumanavalan
 
{{ infobox film
| name           = Puthukottyile Puthu Manavalan
| image          = 
| image size     =
| alt            =
| caption        = 
| director       = Rafi Mecartin
| producer       = New Saga Films
| writer         = Rafi Mecartin Annie  Innocent  Jagathy Sreekumar  Kanakalatha  Janardanan  Captain Raju  Rajan P Dev  Vinduja Menon  Spadikam George Mala Aravindan V. D. Rajappan Krishnakumar
| music          = S. P. Venkatesh
| cinematography = Anandakuttan
| editing        = Harihara Puthran 
| studio         =
green
| distributor    = New Saga Films
| released       = 1995
| runtime        = 130 minutes 
| country        = India
| language       = Malayalam
| budget         = 
28 lakhs
| gross          = 
3 crore
}}
Puthukottyile Puthumanavalan ( ) is a Malayalam comedy film released in 1995.Directed by Rafi Mecartin.


==Plot==
Story revolves around two energetic men who lives in a city with their unsuccessful Kadhaprasangam troupe (story telling performance).

==Cast==
*Jayaram as Ganabhooshanam Satheesh Cochin
*Prem Kumar as Ganabhooshanam Gireesh Cochin
*Jagathy Sreekumar as Kochu Thampi
*Janardhanan (actor) as Palunny
*Innocent (actor)
*Spadikam George as Sreedharanunni
*Captain Raju as Madassery Thampi
*Rajan P Dev as Adv Govindan
*Annie (actress) as Geethu
*Vinduja Menon
*Aranmula Ponnamma
*Kanakalatha
*Krishna Kumar (actor) as Anandhan/John Sacaria
*V. D. Rajappan
*Kunchan (actor)
*Mala Aravindan
*Machan Varghese
*Sainuddin (actor)
*Praseetha Menon

==Songs==
*Oruvellithambalam
*Thankakkolussil
*Mangalam mangalam

==External links==
+ 

 
 
 


 