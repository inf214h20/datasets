The Miracle (1959 film)
{{Infobox film
| name           = The Miracle
| writer         = Karl Vollmöller Frank Butler Jean Rouverol
| starring       = Carroll Baker Roger Moore
| music          = Elmer Bernstein
| cinematography = Ernest Haller
| editing        = Frank Bracht
| director       = Irving Rapper
| producer       = Henry Blanke
| distributor    = Warner Bros.
| released       =  
| runtime        = 121 minutes
| language       = English
| budget         =
}}
 The Miracle, The Miracle written by Karl Vollmöller and directed by Max Reinhardt.

The 1959 film version for Warner Bros. was shot in Technirama and Technicolor, with an original score by Elmer Bernstein.
 
==Plot==
Teresa (Carroll Baker), a postulant at the convent of Miraflores in Salamanca, Spain, is an orphan taken in by the sisters there. She enjoys the convent life, despite being a handful for her superiors. She sings worldly love songs to the other postulants and reads secular stories and plays such as Romeo and Juliet. Still, she has a lively devotion to Christ and to His Blessed Mother. A statue of the Madonna, in fact, is held in high regard by Teresa as she goes about her duties.

When the British march through the town on their way to battle Napoleon, Teresa is drawn to a handsome captain (Roger Moore) she sees while he waters his horse. After a defeat at the Battle of Salamanca the British regiment limps back to the convent which the Mother Superior offers as a hospital for the wounded. Here Teresa learns more about the young captain who had attracted her interest. He is Michael Stuart. He finds Teresa fascinating, and before long he and Teresa find themselves falling in love.

Recovered, the soldiers march out of the convent grounds to be billeted in the nearby town of Miraflores. The seventeen-year-old Teresa is filled with desire for Michael and begins to question her calling. Returning to duty, Michael asks Teresa to marry him; she hesitates, but runs after him. They kiss and Michael proposes that they meet at the towns inn if she wants to leave the religious life and marry him.
 Virgin Mary comes to life, dons the discarded habit and secretly takes Teresas place at the convent.

A thunderstorm roars up as the statue of the Holy Mother steps off its pedestal, but it is the last rain the people of the valley will see for several years. A period of drought begins in the surrounding countryside, seriously damaging the local crops. (The townspeople are convinced that the beneficent intercession of the Virgin Mary has caused the area to flourish, and their belief seems to contain an element of truth for the drough began with the disappearance of the Blessed Mothers statue.)
 Carlos Rivas), necklace with a crucifix and throws it to the ground, screaming that she is no Christian.

What Teresa doesnt know is that Michael has been captured and taken to a prison camp. After some time he escapes and returns to the convent to take Teresa with him to England to marry her. Hes too late, the Mother Superior informs him, "Teresa is now the Bride of Christ," meaning that she has taken her final vows and is now a fully professed nun. Michael pushes past her only to find "Teresa" in full habit - actually the Virgin Mary impersonating her - in a procession singing "Ave Regina Coelarum" ("Hail, Queen of Heaven").  Disillusioned, he leaves to return to duty.

Meanwhile, Teresa, believing Michael dead, falls in love with Guido. The resentful Carlitos, is eaten up by envy and jealousy. On the eve of their wedding, Guido is betrayed to the French by Carlitos. A detachment of soldiers sweeps the gypsy camp pushing Carlitos before them to show them the way. The soldiers shoot a number of men, including Guido.

After the French captain tosses a bag of gold to Carlitos, he is in turn shot to death by La Roca, the two mens mother (Katina Paxinou) for betraying his brother. In agony La Roca turns on the despondent Teresa as the cause of this disaster and banishes her from the camp. Flaco decides to act as Teresas protector as they begin to wander Spain together.
 Dennis King) who finances her career as a singer. The bullfighter is gored in the bullring while smiling at her, deepening Teresas belief that she the cause of his death as, she believes, she was for Michael and the two gypsy brothers, "Im bad luck to anyone who shows me any kindness or affection," as she once told Flaco. She also abandons the portrait Casimir had commissioned from "my friend, Goya," leaving him in despair when he discovers her sudden departure.

During the next four years Teresa travels the Continent becoming a celebrated singer. In Belgium on a concert tour, a special ball is being prepared for the British officers stationed there before they again meet the armies of Napoleon, now escaped from Elba. In her carriage, Teresa catches sight of a British colonel - it is Michael.

The two lovers attend the ball. On the terrace Teresa asks Michael why, after his escape, he did not come back for her. But he did, Michael tells her. In fact he is surprised to see her, considering that he had seen her in nuns habit after taking final vows. She persuades him that he must have hallucinated this while he was lying ill at the prison camp. Michael agrees. At the same time he does recall that the statue of the Madonna had disappeared. This news distresses Teresa even more; because she had so loved the statue. Just then word comes to Colonel Stuart that Michaels uncle, the Duke of Wellington, has called all officers to join their ranks. The ball had been allowed to go on as a ruse to fool all the spies infesting Belgium (this is an actual historical event). Michael asks Teresa to pray for him.

Teresa has now come to a crossroad. Because she believes herself cursed she is terrified that Michael will die in battle for having loved her. She goes to a church to pray. There she makes her peace with God, asking Him to keep Michael safe so that he may return to his own people, and not to her. Leaving word with the parish priest, she decides to return to the convent and leaves with Flaco in a coach.

The next day, Michael leads the infantry charge that finally breaks the ranks of Napoleons soldiers. A cannonball explodes near him. Wellington sees Michael fall from his horse. Bodies litter the field but Michael comes to his senses. Picking up his helmet, he sees where shrapnel has torn a slice across it. It appears that Michael has been saved through divine intervention.

Michael returns to Teresas flat. She has sent the priest to tell him of her decision and to deliver a letter to him in which Teresa begs Michael not to follow her. She must return to her true vocation. In anguish Michael asks the priest for his spiritual guidance, knowing that he must respect Teresas choice and do what is right by letting her go.

Back in Salamanca Teresa finds the region suffering a drought "for four years now," as a woman tells her - ever since the statue of Mary disappeared.  Bidding farewell to Flaco, Teresa enters the chapel shed left so long ago and prays. Weeping, she prostrates herself on the floor as the Blessed Virgin enters, pauses to bless her, and then returns to the pedestal that had been for so long vacant. When she looks up, Teresa finds the statue returned to its pedestal and gazes on it in awe.
 Ave verum corpus.

==Cast==
 
* Carroll Baker as Teresa
* Roger Moore as Michael
* Walter Slezak as Flaco
* Vittorio Gassman as Guido
* Torin Thatcher as Duke of Wellington
* Isobel Elsom as Mother Superior
* Lester Matthews as Capt. Boulting
* Gustavo Rojo as Cordoba
* Katina Paxinou as La Roca Dennis King as Casimir
 
==Production== Wolfgang Reinhardt, son of Max Reinhardt;  however, the project was shelved until 1959.

While the original play and film had been set in medieval times, this version was set during the Napoleonic era in Spain – its climax involved the Battle of Waterloo, with Torin Thatcher making a cameo appearance as Arthur Wellesley, 1st Duke of Wellington (Napoleon is never seen in the film).

It is possible that Carroll Baker was selected to star in the production as a way for Warner Brothers to make peace with the Catholic Church in the United States and "rehabilitate" her reputation in the eyes of the Church. The Legion of Decency, a Catholic film review board, had denounced Bakers suggestive performance in Baby Doll four years earlier, placing that film on its "Condemned" list. Such a drastic classification had seldom been given to any domestic releases due to the strict enforcement of the Motion Picture Production Code in the film industry. Francis Cardinal Spellman, then archbishop of New York, had even gone so far as to threaten excommunication to any Catholic going to see Baby Doll. The films poor box office reflected the Catholic boycott.

"Our director was a humorless jerk," recalled Baker later. "Roger   took so much abuse from Irving Rapper that I was appalled, but he took it like a man and went on to do a very professional job."  
==Reception== widescreen version The Nuns Oscar nominations.  Both that film and The Miracle were produced by Henry Blanke, and most likely, one drew unfavorable comparisons with the other.

===Home video===
The film, which was originally shot in wide screen Technirama and Technicolor and projected with a 2.35:1 aspect ratio was issued on videocassette in a heavily cropped 4 x 3 Pan and Scan transfer in 1997, appearing on DVD in a Korean subtitled version.

== See also ==
* Das Mirakel (play)
* Das Mirakel (1912 film)
* The Miracle (1912 film)
* List of Technirama films

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 