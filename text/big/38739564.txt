The Employer
{{Infobox film
| name           = The Employer
| image          = The Employer (2013) key art poster.jpg
| image_size     =
| border         =
| alt            = Malcolm McDowell as The Employer
| caption        =
| director       = Frank Merle
| producer       = {{Plainlist|
* Frank Merle
* Tiago Mesquita
* Ross Otterman
}}
| writer         = Frank Merle
| starring       = {{Plainlist|
* Malcolm McDowell
* David Dastmalchian
* Paige Howard
* Billy Zane
}}
| music          = Jonathan M. Hartman
| cinematography = Tiago Mesquita
| editing        = {{Plainlist|
| Casting        = Carmen Aiello 
* Sean Ludan
* Jon Mendenhall
}}
| studio         = Hyrax Entertainment
| distributor    = Vision Films
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Employer is an award-winning psychological thriller written and directed by Frank Merle. The film stars Malcolm McDowell as the title character who interviews five candidates for a job at a mysterious, powerful corporation. After a film festival circuit in early 2013, the film was released by Vision Films in the U.S. on June 7, 2013 as an On Demand premiere title, followed by a DVD release on July 2, 2013. 

==Plot==
Five highly qualified applicants for a position with the mysterious, powerful Carcharias Corporation wake up trapped together in a locked room without any hope of escape. They receive a phone call from their interviewer, known only as The Employer, who informs them that they are about to experience the final interview, and its not at all what they were expecting.

==Cast==
* Malcolm McDowell as The Employer
* David Dastmalchian as James
* Paige Howard as Sandra
* Michael DeLorenzo as Keith
* Matthew Willig as Mike
* Katerina Mikailenko as Billie
* Nicki Aycox as Maggie
* Billy Zane as Alan
* Daniel Aldema as Ted
* Jennifer Grace Farmer as Ms. Anderson
* James Cooney as Pete
* Eli Goodman as Greg
* Mark Alexander Herz as Dom
* Bryan Hanna as Max
* Nicholas Vukasovich as Ray
* Erin Killean as Christina
* Tony Casale as Police Officer
* Rebecca Jordan as Dispatcher

==Release==
In October 2012, a near-completed version of film was screened at Shriekfest in Los Angeles, as an Official Opening Night Selection.  

In December 2012, the film won the award for Best Thriller at the Illinois International Film Festival.  In March 2013, the film won a Special Jury Award at the Geneva Film Festival. 

Other festival appearances include the LA Indie Film Festival,  and the Big Bear Horro-Fi Film Festival as a Special Invitation screening.  

The film was released in the United States by Vision Films, a worldwide distributor of independent films, across all major On Demand platforms on June 7, 2013. A DVD release followed on July 2, 2013.

==Reception==
Early festival screenings received positive reviews. Hunter Johnson from LAHorror.com praised the films "terrific performances," especially Malcolm McDowell, who he noted is "even more dementedly charming than usual" in a film thats "modern, violent and full of nasty twists."  Leo Brady from AMovieGuy.com called the film "an intriguing concept and thrilling to watch," stating that McDowell is "the perfect actor for this film. He has mystery behind his eyes and his portrayal of power is calm and cool."  Sean Brickley from Horror News Network gave the film five stars and called McDowells title character "the most sociopathic Human Relations director the corporate world has ever seen."  Christopher M. Jimenez from Sinful Celluloid highly recommended the film, writing "Frank Merle has created a minor masterpiece with spot on performances, great manipulation, and good backstory."  Sufi Mohamed from IndieJudge.com gave the film five stars, declaring in his review "this is literally one of the best independent movies I’ve ever seen," as he drew a real-world comparison between the films themes and the Milgram Experiment.  

On May 25, 2013, The Employer won eight honors at the Los Angeles Movie Awards, including Best Narrative Feature, Best Director, Best Actor (Malcolm McDowell), Best Supporting Actor (Michael DeLorenzo), Best Supporting Actress (Paige Howard), Best Original Score, Best Special Effects and the Audience Choice Award. 

==References==
  

==External links==
*  
*   at the Internet Movie Database

 

 