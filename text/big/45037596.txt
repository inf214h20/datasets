Voice of Silence (2013 film)
{{Multiple issues|
 
 
}}
 Iranian film written and directed by Mohammad Hadi Naeiji.  The movie has won the Crystal Simorgh for Best Screenplay at the 32nd Fajr International Film Festival. It was produced by the award-winning writer and director Reza Mirkarimi the director of A Cube of Sugar.

== Plot ==
Mir-Hashem is a young cleric whose wife has left him because of his angry creditors and his fertility problems. One of the creditors persuades Mir-Hashem to go for propagation and get the money from people.

== Cast & Crew ==

=== Cast ===
* Hesam Mahmoudi as Mir-Hashem
* Mohammad Asgari as the Creditor
* Houtan Shakiba as Sed-Hasan
* Shirin Esmaeeli as Fereshte
* Mahdi Farizeh as Ahmad

=== Crew ===
* Writer and Director Mohammad Hadi Naeiji
* Assistant Director Mohammad Reza Ahmadi
* Producer Reza Mirkarimi
* Director of Photography Morteza Hodai
* Edited by Mohsen Gharai
* Music by Reza Mortazavi
* Costume Designer Mahdi Mousavi
* Sound Saeed Bojnordi
* Character Makeup Mahmoud Dehghani
* Production Manager Mohammad Reza Mansouri
* Photographer Masoud Gharai

==References==
 

== External links ==
*  

 
 
 
 
 

 