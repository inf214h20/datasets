Bad for Each Other
{{Infobox film
| name           = Bad for Each Other
| image          = 
| image_size     = 
| caption        = 
| director       = Irving Rapper
| writer         = 
| narrator       = 
| starring       = Charlton Heston Lizabeth Scott
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Columbia Pictures
| released       = December 24, 1953
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Bad for Each Other is a 1953 film directed by Irving Rapper, and starring Charlton Heston and Lizabeth Scott.

==Plot==
Army colonel and doctor Tom Owen returns home to Coalville, Pennsylvania, on leave. He learns  from wealthy mine owner Dan Reasonover that his brother Floyd, a mine safety engineer killed in an explosion, had betrayed Reasonovers trust; he had been embezzling and stealing even more money by purchasing cheap, substandard equipment. Floyd was also heavily in debt. Tom wants to pay it all back, though Dan Reasonover tries to talk him out of it.

Twice divorced socialite Helen Curtis, Dans daughter, meets Tom at a party and likes what she sees. Helen asks him for a date, during which she informs him she has arranged for him to meet Dr. Homer Gleeson. Gleeson runs a fancy Pittsburgh clinic catering to the upper class. Gleesons associate has quit to start his own practice, so he offers Tom the vacancy. Knowing that Tom has vowed to pay his late brothers debts, she talks him into accepting, despite Toms contentment in the Army and his mothers disapproval. Toms first task is hiring a nurse; he selects Joan Lasher, an attractive and idealistic young woman who intends to become a doctor herself.

Tom and Helen begin dating. Eventually, Tom proposes to her, and she accepts. Her own father warns him that her wealth had poisoned her first two marriages, but Tom remains adamant.

Lasher becomes disappointed that Tom treats wealthy society patients for minor ailments when he could be doing more good elsewhere. Dr. Jim Crowley, a former sergeant who was inspired by Toms example to resume his medical studies, comes to Tom to ask for a job, not for the money (though he is poor) but for the experience of working and learning from such a superb surgeon. Tom sends him to see Dr. Lester Scobee, who cares for the miners of Coalville.

Emergency surgery is needed for Mrs. Roger Nelson, Helens rich and influential aunt. Her personal physician, Gleeson, pleads with Tom to perform the operation for him, as he himself has not done any surgery in ten years. When Tom does so, unethically, Lasher quits. Gleeson decides to charge Mrs. Nelson an exorbitant fee, which he splits with Tom. Helens aunt eventually finds out who actually operated on her. Though she is grateful, she questions his ethics. Before Tom can defend himself, he is called away by an emergency: a mine explosion at Coalville.

Tom enters the mine and joins Jim treating and rescuing miners. They get out before the chamber they were in collapses, but Jim is fatally injured and dies in the ambulance. Tom tells Helen he is quitting the clinic to work in Coalville. Helen tells him she cannot live there, so they reluctantly part. Tom arrives at his new office, to find Joan already at work.

==Cast==
*Charlton Heston as Dr. Tom Owen
*Lizabeth Scott as Helen Curtis
*Dianne Foster as Joan Lasher
*Mildred Dunnock as Mrs. Mary Owen, Toms mother
*Arthur Franz as Dr. Jim Crowley Ray Collins as Dan Reasonover
*Marjorie Rambeau as Mrs. Roger Nelson
*Lester Matthews as Dr. Homer Gleeson Rhys Williams as Dr. Leslie M. Scobee

==Miscellaneous== The Citadel, was reshuffled and simplified for Bad for Each Other. In The Citadel, Robert Donat plays a doctor who forgoes treating miners in Wales in order to become a highly paid society doctor in London. The mine-rescue sequence in Bad for Each Other is near-identical to the equivalent sequence in The Citadel.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 