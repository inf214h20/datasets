Exit 67
{{Infobox film
| name           = Exit 67
| image          = Sortie-67.jpg
| caption        = Theatrical release poster
| director       = Jephté Bastien
| producer       = Jephté Bastien
| writer         = Jephté Bastien
| starring       = Henri Pardo Natacha Noël Benz Antoine
| music          = 
| cinematography = 
| editing        = 
| studio         = Ajoupa Atopia
| released       =  
| runtime        = 93 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          =
}}
Exit 67 ( ) is a 2010 French-Canadian (Quebec) film written and directed by Jephté Bastien.
 La Presse columnist Rima Elkouri, director Jephté Bastien states that the death of his 16 years old nephew inspired him to make the film. 

The film had its world premiere at the 2010 edition of Montreals Fantasia Festival. It was named the winner of the Claude Jutra Award for the best feature film by a first-time film director, at the 31st Genie Awards. 

==Synopsis==
The following description, was written by Simon Laperrière, a Director and Programmer at the festival, and translated by Rupert Bottenberg, a Montreal-based journalist, was featured in the festivals program catalogue:

"A life of crime seems inevitable for Jecko. A mixed-race Québécois with a Haitian background, he witnessed his mother’s death at his fathers hands when he was eight years old, an event that scarred him for life. Tossed from one foster family to another, Jecko finally finds the sense of belonging he’s lacked when he starts hanging out with the young hoodlums of the Villeray–Saint-Michel–Parc-Extension|St-Michel neighbourhood. They invite him to join their gang, promising him money and power, everything he’s dreamed of but that he knows polite society will never offer him. For a teenager with few hopes for the future, one highly susceptible to influence, it’s not an offer to refuse. Even if it means bending to the will of a violent and domineering gang leader. Even if his initiation is to kill a stranger. Several years later, crime and violence have brought Jecko to the pinnacle of an underworld empire. In this world where riches and respect are earned with a gun, he feels perfectly at home. A number of events, however, have him reconsidering the path before him. With the option of pulling one final crime, his father due out of prison shortly and hopes of building his own family, it’s time for Jecko to grab the steering wheel of his destiny. But leaving St-Michel wont be so easy.

==Cast==
*Henri Pardo as Ronald Paquet / Jecko
*Benz Antoine as Brooklyn
*Jacquy Bidjeck as Sonia
*Alain Lino Mic Eli Bastien as Pakko
*Edouard Fontaine as Zophe
*Sylvio Archambault as Jocelyn Paquet
*Natacha Noël as Candy
*Anthony Clerveaux as Ronald - 15 ans
*Lansana Kourouma as Pakko - 17 ans
*Scott Jimmy Beaubrun as Zophe
*Fabienne Colas as Magalie
*Lynne Adams as Social worker
*Franck Sylvestre as Ti-Boss
*Yardly Kavanagh as Ginette
*Stéphane Moraille as Jack
*Danny Blanco Hall as Blade
*Sophie Desmarais as Lola
*Anatoly Zinoviev as Trafficker #1
*Philippe Racine as Kaya
*Miguel Mendoza as Latino
*Florence Situ as Caissiere
*Ralph Prosper as Max
*Patricia Stasiak as Marie
*Kwasi Songui as Max #2
*Vanessa Uguru John as Naima
*Jude Ferus as Fuyard
*Jessica B. Hill as Sara
*Marcia Leblanc as Barmaid
*Le Voyou as Rappeur #1
*Calvin Philips as Jeune Client
*Shevon Jeremy Noël as Ronald

== References ==
 

== External links ==
*  
*  
*   at Fantasia Festival.com

 
 
 
 
 
 
 