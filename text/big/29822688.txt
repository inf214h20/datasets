Sicilian Uprising
{{Infobox film
| name = Sicilian Uprising
| image =
| image_size =
| caption =
| director = Giorgio Pastina  Giuseppe DAngelo
| writer =  Eugène Scribe (libretto)   Charles Duveyrier (libretto)   Fulvio Palmieri
| narrator = Paul Muller 
| music = Enzo Masetti  
| cinematography = 
| editing = 
| studio =   Epica   Safir 
| distributor = Fincine 
| released = 1949
| runtime = 91 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama earlier play written by Eugène Scribe and Charles Duveyrier.

==Cast==
*   Marina Berti as Laura 
* Clara Calamai as Elena Di Caltabellotta 
* Roldano Lupi as Giovanni Da Procida  Steve Barclay as Cap. Droet   Paul Muller as Carlo Dangio  
* Ermanno Randi as Ruggero  
* Aldo Silvani as Tommaso  
* Carlo Tamberlani as Abate Di Santo Spirito  
* Aroldo Tieri as Governatore Di Palermo
* Gabriele Ferzetti
* Gianni Glori
* Felice Minotti

== References ==
 
 
==Bibliography==
* Gesù, Sebastiano. La Sicilia e il cinema. Giuseppe Maimone, 1993. 
* Aroldo Tieri e il Cinema. Pellegrini Editore, 2007.

== External links ==
* 

 
 
 
 
 
 
 
 
 
 

 

 