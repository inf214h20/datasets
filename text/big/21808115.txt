Heartstopper (film)
 
 
Heartstopper (also known as Heart Stopper) is a 2006 straight-to-DVD horror film directed by Bob Keen and starring Robert Englund.

==Plot==
Sara Wexler is a lonely teenager who attempts to commit suicide by running in front of a car. However, she is only injured before being discovered by Sheriff Berger. He takes her to hospital, where the notorious serial killer Jonathan Chambers, whom Berger captured, is being detained. Chambers is then executed in the electric chair, but the police do not know that he survived by making a deal with the devil. Chambers now has supernatural powers and begins to slaughter everyone in the hospital, including Berger. Meanwhile, Sara and another teenager called Walter, who was sent to hospital after accidentally being impaled on his own rake, try to escape from the hospital but find that all exits are locked. Chambers then confronts Sara and explains that he needs her to help him because she has a power which will make him immortal. She declines the offer and flees from him. Eventually she defeats Chambers by opening a portal to hell and sending him through it. In the final scene, however, it is revealed that Chamberss personality has passed into her.

{| class="wikitable"
|+Cast
!Actor
!Character
|- Meredith Henderson Sara Wexler
|- Nathan Stephenson Walter
|- Robert Englund Sheriff Berger
|- James Binkly Jonathan Chambers
|}

==Production==
Filming lasted from 9 September 2005 to 13 November 2005, on a $3,000,000 budget.

==Release==
The film was released on DVD in the USA on 31 October 2006 and in the UK on 18 December 2006.

==External links==
*  

 
 

 