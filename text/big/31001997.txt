The King of Paris (1917 film)
{{Infobox film
| name           = The King of Paris
| image          = The King of Paris poster.jpg
| alt            = Film poster
| caption        =
| film name      = Король Парижа
| director       = Yevgeni Bauer
| producer       =
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Russia
| language       = Silent film
| budget         =
| gross          =
}}
The King of Paris ( :Korol Parizha) is a 1917 Russian silent feature film directed by Yevgeni Bauer.

The film was shot in the cinema factory of Aleksandr Khanzhonkov in Yalta.  This was the last work of Yevgeni Bauer. As he worked on his previous film, For Happiness (ru: «За счастьем») Bauer broke his leg, and he shot this film while in a bathchair, but soon fell ill with pneumonia. He began shooting in early summer of 1917.    But he was soon placed in Yalta hospital and on 9 June 1917 he died. Actress Olga Rakhmanova finished the film.

==Plot==
 
In Paris, Raskol takes young Bremond under his protection. He tells him that if he treats women as slaves, men as enemies and businessmen as puppets, he will become King of Paris. The two men, now passing as Count de Saint-Venkov and Marquis de Predamond apply the recipe very successfully. However, when Bremond wants to marry the Duchess von Dorstein for her money, he will have to confront her son, who has changed his name to Jean Hiénard and lives a simple life as a sculptor. 

==Cast==
*Vjacheslav Svoboda as Roger "King of Paris"
*Nikolai Radin as Rascol Venkov
*Emma Bauer as Furstin Von Dorstein
*Mikhail Stalski as Jean Hiénard
*Lydia Koreneva as Lucienne Marechal
*Marija Boldireva as Juliette
*Vera Karalli (cameo)

==Release==
The premiere was held one month after the October Revolution, 6 December 1917. Preoccupied by the revolution, the film was unnoticed by audiences and critics. The only known copy of the film has lain for almost 100 years in the Russian State archives.    A screening of the film took place in Yalta at the 130th anniversary of Aleksandr Khanzhonkov in September 2007. 

==References==
 

==External links==
*  

 
 
 
 
 


 