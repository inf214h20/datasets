The Tree (2010 film)
 
 
{{Infobox film
| name = The Tree
| image = The_tree_poster.jpg
| caption = Film Poster
| director = Julie Bertuccelli
| writer = Julie Bertuccelli
| starring = Charlotte Gainsbourg Marton Csokas Morgana Davies Aden Young Kent Smith Sue Taylor
| released =  
| studio = Taylor Media Les Films du Poisson
| distributor = Transmission Films (Australia) Le Pacte (France) Zeitgeist Films (US)
| country = Australia France 
| language = English
| runtime = 100 minutes
| gross = $2,219,182 
}} Cannes Film Festival   on 23 May 2010 following the Awards Ceremony and received a seven-minute standing ovation.  As well as this, The Tree premiered at the 2010 Sydney Film Festival.  The film is distributed in the U.S. by Zeitgeist Films, opening on 15 July 2011 in New York, on 22 July in Los Angeles, Boston and Washington, D.C., and throughout the country over the summer.

== Plot ==
Dawn and Peter O’Neil live together with their children (three boys and a girl), on the outskirts of a small country town. Next to their rambling house stands the kids’ favorite playground: a giant Moreton Bay Fig tree (now known in real-life as the Teviotville Tree), whose branches reach high towards the sky and roots stretch far into the ground.

Everything seems perfect until Peter suffers a heart attack, crashing his car into the tree’s trunk. Dawn is devastated, left alone with her grief and four children to raise. Until one day, 8-year-old Simone, reveals a secret to her mother. She’s convinced her father whispers to her through the leaves of the tree and he’s come back to protect them. Dawn takes comfort from Simone’s imagination, and starts to believe in it herself; just like Simone, Dawn also likes to spend time in the tree. It starts to dominate their physical and emotional landscape. But the close bond between mother and Simone forged through a mutual sorrow and shared secret, is threatened by the arrival of George, the plumber, called in to remove the tree’s troublesome roots. 
As the relationship between Dawn and George blossoms, the tree continues to grow, with its branches infiltrating the house, its roots destroy the foundations. Dawn decides the tree has to go. George and some other workmen arrive, but Simone climbs in the tree to defend it. Dawn and George try to convince her, but she refuses to come down. George argues to Dawn that the girl is only 8 years old, Dawn should not allow her to stop the necessary removal of the tree. This irritates Dawn, and she cancels the operation, and tells George she does not want to see him again. 

In a big storm the house is demolished by the tree, and the family leaves the area, planning to start living somewhere else, perhaps in a tent.

== Production ==
The Tree was written and directed by Julie Bertuccelli, it is based on the screenplay by Elizabeth J Mars, and produced by Sue Taylor of Taylor Media, Yael Fogiel and Laetitia Gonzalez of Les Films du Poisson, the film is a co-production between Australia and France.  It came to be a co-production when Julie Bertuccelli was given the book Our Father Who Art in The Tree  from a close friend. When she looked into getting the rights for the film she found that Australian producer Sue Taylor already had them, however she did not have a director. It just so happened that Julie is a director, and from there the co-production was born. The tree used in the film is Teviotville Tree, located in the small town of Teviotville in the state of Queensland. It has a 34m spread, 20m height and 2.31m diameter at 1m above ground which is the narrowest point. The tree has low branches which have not been pruned off, and when they are laden with fruit they reach the ground. It is very rare to find this in a Moreton Bay Fig Tree. It is estimated that it was planted in 1880. 

* Charlotte Gainsbourg as Dawn
* Marton Csokas as George
* Aden Young as Peter
* Morgana Davies as Simone
* Christian Byers as Tim
* Tom Russell as Lou
* Gabriel Gotting as Charlie
* Gillian Jones as Vonnie
* Penne Hackforth-Jones as Mrs Johnson

== Soundtrack ==
Original film music was written and composed by Grégoire Hetzel. The soundtrack features other songs of the film: 

# "Weak" - Asaf Avidan & The Mojos (3:34)
# "Wake" - Grégoire Hetzel (2:58)
# "The Tree" - Main Theme - Grégoire Hetzel (3:55)
# Chorus of "Die Kriegsknechte aber" from "The Passion according to St. John" - Scholars Baroque Ensemble (1:26)
# "Flying Foxes" - The Slippers (2:47)
# "Speak to Me" - Grégoire Hetzel (2:55)
# "Simones Theme" - Grégoire Hetzel (3:18)
# "The Roots" - Grégoire Hetzel (3:27)
# "Shiver Shiver" - The Slippers (3:48)
# "Under the Branches" - Grégoire Hetzel (3:55)
# "Wounded Tree" - Grégoire Hetzel (3:13)
# "To Build a Home" - The Cinematic Orchestra (6:12)
# "Daydream" (Extra Track) - Grégoire Hetzel (8:45)

==References==
 

== External links ==
*  
*  
*  
*   on indieWiRE
*  
*  
*  
*  
*  


 
 
 
 
 
 
 
 
 
 