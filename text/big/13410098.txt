Noora
{{Infobox film
| name           = Noora
| image          = Noora film.jpg
| director       = Mahmoud Shoolizadeh
| producer       = Javad Zahiri  (I.R.I.B, Ch1)
| writer         = Mahmoud Shoolizadeh
| starring       = Mohammad Abassi Masoomeh Yousefi Farhad Aeesh Maedeh Tahmasebi
| music          = Majid Entezami
| cinematography = Farzad Jodat
| sound          = Karim Kashani
| editing        = Jila Ipakchi
| released       = 2002
| runtime        = 84 min.
| country        = Iran English subtitles
}}
Noora (The Kiss of Life) is the first fiction film directed by Mahmoud Shoolizadeh, it has participated in major international film festivals and has won many awards.
This film is the story of a ten-year-old boy who lives with his father, mother, little sister and grandmother in a cottage in one of the jungles in the north of Iran. His little sister became paralyzed in an accident. All the family members start an endeavour to solve the problem, but the boy selects a special way and shows his iron will facing the difficulties.
The two children can recognize a new meaning for life in the process of the story. They reach tranquility through their spiritual ascension.

== Director’s View ==
 
 (left) directing an actor along with other film crew]]Noora is simple and without any mysterious complexity. The story, characters and their relations are all simple. It starts with a childish play of a brother (Niaz) and sister (Noora) and then continues in huge developments in their lives. Through passing too many difficult barriers, these lovely children, reach the peak of perfection. Niaz, the ten-year-old child experiences the pain of being kind. He grows like a grain and blossoms.
I tried my best to utilize the visual and technical elements that are suitable for the contents of the story. Using the minimum dialogue and transferring the sense and condition by image, utilizing the nature for transferring the concepts, producing a natural, real and live invironment are parts of my goals in making this film in order to create a real belief for the viewers.

== Awards ==
  in the Taormina International Film Festival 2003, on stage being introduced to the audience.]]

*1. Best Script award and special appreciation in the 16th International Children and youth film festival Isfahan - October 2001

*2. Best Child actress award in the 16th International Children and youth film festival Isfahan - October 2001

*3. Best Set design in 6th Iran Cinema Film Festival - Tehran - September 2002 {{cite web
| url = http://www.khanehcinema.ir/news/view.asp?newsid=55
| title = In Persian language: official website of Iran Cinema Film Festival (Numbers 10 and 11)
| accessdate=2008-09-12
| date = 
| author = 
| publisher = 
}}   

*4. Best Music award in 6th Iran Cinema Film Festival - Tehran - September 2002 

*5. Best Director award in the 4th Cima Festival (Iran TV) - Tehran - October 2003 {{cite web
| url = http://cmi.irib.ir/pages/synopsis.htm
| title = A synopsis of the film by Cima (Iran TV) (Noora is the 3rd film down) 
| accessdate=2008-09-12
| date = 
| author = 
| publisher = 
}} 

*6. Best Film award in Azra Film Festival  - Tehran - October 2003

*7. Best Film award of the audience in 49th International Film Festival of Taormina in Italy  June 2003  

== Participation in film festivals ==
=== International ===

*Arizona International Film Festival in USA, April 2003,

*Taormina International Film Festival in Italy, prize winner as the best film for the audience, June 2003, 

*Durham International Film Festival in Britain, June 2003,

*Karlovy Vary International film festival in Czech republic, July 2003, 

*Open Air International Film Festival in Germany, Aug 2003,

*Cairo International Film Festival in Egypt, Oct 2003,

*Flanders International Film Festival in Belgium, Oct 2003,

*Golden Elephant International Film Festival in India, Nov 2003,

*Cork International Film Festival in Ireland, Oct 2003, 

*Foil International Film Festival in Britain, Nov 2003,

*Olou International Film Festival in Finland, Nov 2003,

*Olympia International Film Festival in Greece, Dec 2003,

*International Film Festival in Turkey, Dec 2003,

*International Film Festival in Tunisia, Dec 2003,

*Auburn Film Festival in Australia, 2003, (left) and Cinematographer Farzad Jodat(right) during filming for the film Noora]]

*International Children and Young Film Festival in Argentina, 2003,

*Shtootgart International Film Festival in Germany, Jan 2004,

*East Lansing International Film Festival in USA, March 2004,

*Zlin International Film Festival in Czech republic, June 2004, 

===National (in Iran)===

*International youth film festival in Isfahan, 2002,

*Fajr International film festival in Tehran, 2002,

*Great Festival of Iranian cinema in Tehran, 2002,

*Azra film festival in Tehran, 2003,

*The fourth Cima film festival (Iran TV), 2003, 

== References ==
 

== External links ==
*  

 
 