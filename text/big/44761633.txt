Mutthanna
{{Infobox film|
| name = Mutthanna
| image = 
| caption =
| director = M. S. Rajashekar
| writer = Manmohan Desai
| starring = Shivrajkumar  Shashikumar  Supriya   Sneha
| producer = L. Somanna Gowda
| music = Hamsalekha
| cinematography = R. Madhusudhan
| editing = S. Manohar
| studio = Nirupama Art Combines
| released =  
| runtime = 146 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada drama romance drama film directed by M. S. Rajashekar and produced by L. Somanna Gowda. The film features Shivarajkumar in dual roles, Shashikumar, Supriya and Sneha in the lead roles. 
 Mumtaz and Vinod Khanna in the lead roles. The film also had its Tamil version released in 1975 as Ninaithadhai Mudippavan starring M. G. Ramachandran and M. N. Nambiar. 
 
== Cast ==
* Shivarajkumar as Mutthanna / Diamond Kiran
* Supriya
* Shashikumar
* Sneha
* Thoogudeepa Srinivas
* Doddanna
* M.N Lakshmi Devi
* Bhavyashri Rai
* Girija Lokesh
* Mynavathi
* Pruthviraj
* Ashwath Narayan

== Soundtrack ==
The soundtrack of the film was composed by Hamsalekha. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Mutthanna Peepi Uduva
| extra1 = S. P. Balasubrahmanyam& K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Hrudaya Bagila
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Nooru Nooru Kohinooru
| extra3 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Mukha Nodi Mola Hakabeda
| extra4 = S. P. Balasubrahmanyam 
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Nangoo Aase
| extra5 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics5 = Hamsalekha
| length5 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 

 