Pete Smalls Is Dead
{{Infobox film
| name           = Pete Smalls Is Dead
| image          = Pete Smalls Is Dead poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Alexandre Rockwell
| producer       = Floyd Byars Brandon Cole Peter Dinklage Paul Hudson Alexandre Rockwell Sean-Michael Smith 	
| writer         = Brandon Cole Alexandre Rockwell 
| starring       = Mark Boone Junior Peter Dinklage Seymour Cassel Todd Barry Steve Buscemi Rosie Perez Tim Roth
| music          = Mader 
| cinematography = Kai Orion 
| editing        = Jarrah Gurrie Aldolpho Rollo Josiah Signor 
| studio         = Ms. Tangerine Productions Pantry Films
| distributor    = W2 Media 
| released       =   
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Pete Smalls Is Dead is a 2010 American comedy film directed by Alexandre Rockwell and written by Brandon Cole and Alexandre Rockwell. The film stars Mark Boone Junior, Peter Dinklage, Seymour Cassel, Todd Barry, Steve Buscemi, Rosie Perez and Tim Roth. The film was released on April 14, 2011.  

== Cast ==
*Mark Boone Junior as Jack Gomes
*Peter Dinklage as K.C. Munk
*Seymour Cassel as Saco
*Todd Barry as Bob Withers
*Steve Buscemi as Bernie Lake
*Rosie Perez as Julia
*Tim Roth as Pete Smalls
*Ritchie Coster as Hal Lazar
*Lena Headey as Shannah
*Michael Hitchcock as Sly 
*Carol Kane as Landlady
*Artin Kashini as Aram Michael Lerner as Leonard Proval
*David Proval as Nimmo
*Steven Randazzo as Larson
*Emily Rios as Xan
*Theresa Wayman as Saskia
*Cástulo Guerra as El Patron
*Tony Longo as Joey Sausa
*Tatyana Ali as Cocktail Waitress
*Peter OLeary as Bill & Fat Monk Coati Mundi as Security Guard & Pool Band Leader
*Lana Rockwell as Airport Butterfly
*Luc Bell as Armenian Kid
*Robert Crobar Bilus as Hals Bodyguard
*Jon Morgan Woodward as Diner #10

== References ==
 

== External links ==
*  

 
 
 
 
 


 