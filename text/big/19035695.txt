Further Up the Creek
 
 
{{Infobox film
| name           = Further up the Creek
| image          = "Further_up_the_Creek"_(1958).jpg
| image_size     = 
| caption        = UK theatrical poster
| director       = Val Guest
| producer       = Henry Halstead
| writer         = Val Guest Len Heath John Warren
| narrator       = 
| starring       = David Tomlinson  Frankie Howerd  Shirley Eaton
| music          = Stanley Black Gerald Gibbs Len Harris
| editing        = Bill Lenny
| studio         = Hammer Columbia
| released       = 28 October 1958 London UK
| runtime        = 91 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Up the The Mouse That Roared.  Frankie Howerd was drafted in to replace him.

==Plot==
Navy frigate the "Aristotle" is sold to a Middle Eastern power, and against regulations the ships bosn tries to make a profit by selling tickets to passengers seeking a luxury cruise. When the Captain discovers what is going on, he attempts to straighten things out.   

==Cast==
* David Tomlinson as Lieutenant Fairweather
* Frankie Howerd as Bosn
* Shirley Eaton as Jane
* Thora Hird as Mrs. Galloway
* Lionel Jeffries as Steady Barker
* Lionel Murton as Perkins
* Sam Kydd as Bates
* John Warren as Cooky David Lodge as Scouse
* Ian Whittaker as Lofty
* Esma Cannon as Maudie Tom Gill as Philippe
* Jack Le White as Kentoni Brother
* Max Day as Kentoni Brother
* Eric Pohlmann as President
* Michael Goodliffe as Lieutenant Commander

==Critical reception==
TV Guide wrote, "less rather than more, as most follow-ups are."  

==References==
 

 
 

 
 
 
 
 
 
 



 
 