Omen IV: The Awakening
{{multiple issues|
 
 
}}

{{Infobox television
| name = Omen IV: The Awakening
| image =  
| image_size = 
| caption  = 
| director = Jorge Montesi Dominique Othenin-Girard
| producer = Harvey Bernhard Mace Neufeld
| writer   = David Seltzer (characters) Harvey Bernhard Brian Taggert Michael Woods Asia Vieira
| music    = Jonathan Sheffer Jerry Goldsmith
| cinematography = Martin Fuhrer
| editing  = Bruce Giesbrecht Frank Irvine
| distributor = Fox Video 20th Century Fox
| released = May 20, 1991
| runtime  = 97 minutes
| country  = United States
| language = English
| preceded_by =  
}}

Omen IV: The Awakening is a 1991 Television film|made-for-television film that serves as the fourth and final addition to the original The Omen (film series)|The Omen series, directed by Jorge Montesi and Dominique Othenin-Girard.  This was intended to be the first of many televisual sequels to Twentieth Century Foxs film history of popular titles. Producer Harvey Bernhard, who produced the last three films, felt there could be more done to the series. This was the last film he has produced to date.  He previously wrote the story for the second film but this is the only film that he co-wrote. 

==Plot==
Virginian congressman Gene York (Michael Woods) and his attorney wife Karen (Faye Grant), after numerous failed attempts to have children, go to a nun-owned orphanage where they adopt a child that they name Delia from a nun named Sister Yvonne. At first, other than Delia scratching Karen and the later heart attack of the preacher overseeing her baptism, all seems normal. But seven years later, after the Yorks adopt a rottweiler that they name Ryder, Delia (Asia Vieira) starts to display the traits and personality of an increasingly violent and manipulative sociopath. Furthermore, as the family doctor Hastings reveals that she is going through puberty, strange events begin to occur around Delia, including the death of the father of a boy whom she terrorized. Jo Thueson, a New Age practitioner hired by the Yorks as a nanny to help while Gene runs for the Senate, senses something suspicious about Delia after finding her healing crystals blackened by the girls touch.

At the advice of her friend, an aura reader named Noah, Jo takes Delia to a fair, where all psychics present sense a feeling of unease brought on by the girls presence. Jo manages to get Noah to take an Aura photograph of Delia before she storms off. But as Noah sees her photograph showing very dark colors, Delia causes a fire that sets the entire fairground ablaze. Though Noah warns her to leave after showing her the photo, Jo attempts to find out why her young charge is so full of negative energy. It is during this investigation that Jo learns of Delias true identity, but before she can share this information with anyone she is sent plummeting out of a window by Ryder. Karen, who witnesses the fall, faints from shock and is taken to the hospital, where she learns that she is pregnant.

Becoming increasingly alarmed and suspicious of her adoptive daughter, Karen turns to her preacher, Father James Mattson, for help in understanding what Jo learned of Delia, and is told of the Antichrist. Eight months later, after learning that Sister Yvonne mysteriously left the orphanage when she and Gene adopted Delia, Karen hires detective Earl Knight to find Delias biological parents. Knights search takes to him Carolina, where Sister Yvonne now goes by the name Felicity. He finds her taking part in a bizarre religious ceremony during which Felicity stands in a circle surrounded by rattlesnakes. Earl shows Felicity a recent photograph of Delia, and unintentionally causes her suffer an envenomation overdose during a Snake handling ritual. After speaking to her before she dies, Knight finds clippings in Felicitys trailer relating to Gene. Unable to return to Virginia, Knight sends Karen a letter of his findings in the mail prior to being killed in a bizarre construction accident.
 666 symbol, clearly displayed on the palm of her brothers hand. Be it by the childrens power or by her own inability to kill the infant, Karen ends up taking her own life, with Gene, Delia and Alexander attending her funeral.

==Cast==
* Faye Grant as Karen York Michael Woods as Gene York
* Asia Vieira as Delia York
* Brianne Harrett as Delia York (3 years old)
* Rebecca Cynader as Delia York (2 years old)
* Shelby Adams as Delia York (baby) Michael Lerner as Earl Knight
* Madison Mason as Dr. Hastings
* Ann Hearn as Jo Thueson Jim Byrnes as Noah
* Don S. Davis as Jake Madison
* Megan Leitch as Sister Yvonne / Felicity 
* Joy Coghill as Sister Francesca  David Cameron as Father Hayes
* Duncan Fraser as Father Mattson 
* Susan Chapple as Mother Superior 
* Dana Still  as Revival Preacher
* Andrea Mann as Miss Roselli 
* Camille Mitchell as Madge Milligan 
* Brenda Crichlow as Hildy Riggs 
* William S. Taylor as Forrest Riggs
* Serge Houde as Morris Creighton 
* Wendy Van Riesen as Lily Creighton 
* James Sherry as Jerome 
* Mikal Dughi as Miss Norris

==Production==

===Music===
The score was composed by Jonathan Sheffer who wrote original music and referenced themes written by Jerry Goldsmith for the previous films.

==Reception==
The movie had an overwhelmingly negative reception from critics.    

== References ==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 