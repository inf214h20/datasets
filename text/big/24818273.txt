Full Circle (1977 film)
{{Infobox film
| name           = Full Circle (aka The Haunting of Julia)
| image          = TheHauntingofJulia.jpg
| image_size     = 
| caption        = Poster for The Haunting of Julia
| director       = Richard Loncraine
| writer          = Dave Humphries (screenplay) Harry Bromley Davenport (story adaptation)
| based on     =  
| narrator       = 
| starring       = Mia Farrow
| music          = Colin Towns
| cinematography = Peter Hannan
| editing        = Ron Wisman
| distributor    = Cinema International Corporation
| released       = September 11, 1977  (San Sebastián International Film Festival)  September 11, 1981  (USA) 
| runtime        = 98 minutes
| country        = Canada/UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 ghost of a vengeful little girl.

==Plot==
 
As Julia Lofting (Mia Farrow) eats breakfast with her family, her daughter Kate (Sophie Ward) begins to choke on her food. Unable to dislodge the food, Julia attempts to save her by performing a tracheotomy which results in the childs bleeding to death.

Julia leaves her husband Magnus (Keir Dullea) and moves to a new house with an upstairs room containing a childs possessions, including a mechanical clown toy with sharp cymbals on which she cuts her finger. In the park, Julia sees a young girl that she believes is Kate but the child disappears. Odd events begin to take place in the house such as strange noises and appliances turning on by themselves. Later, Julia again sees the girl in the park and finds a mutilated turtle and knife where she stood. 

Julia lets Magnus sister Lily use her house to conduct a seance. Mrs. Flood, the medium, explains that spirits need to control someone to carry out physical acts. During the seance, the medium becomes frightened and tells Julia to leave the house immediately. Moments later, one of Lilys friends falls down the stairs before Mrs. Flood can explain what she saw. 

While Julia is out, Magnus breaks into her house. He sees something and follows it to the basement where he falls from the staircase, fatally cutting his throat on a broken bottle.

A neighbor tells Julia the house once belonged to Heather Rudge, who moved away after her daughter Olivia died. Julia visits Mrs. Flood who tells her that during the séance she had a vision of a boy in the park, bleeding to death. Julia finds an article about Geoffrey Braden, a young boy who was murdered in the park. Julia visits Geoffreys mother, Greta Braden, who says a vagrant was executed for the crime but other children in the park murdered her son. She says she and her companion have been following the lives of the children. She asks Julia to visit the remaining two, now adults: Captain Paul Winter and David Swift. 

Julia visits Swift, an alcoholic, who confesses that Olivia had power over him and the other children. She made them each kill an animal, and watch as she murdered Geoffrey. He says he told only Mrs. Rudge. Later, as Swift leaves his apartment, he slips on a broken bottle in the stairwell and he falls to his death.

Julia tells her friend, Mark (Tom Conti) what she has discovered but he doesnt believe her. That evening, he is electrocuted by a lamp falling into his bath. 

Julia visits Mrs. Rudge in a psychiatric home. Mrs. Rudge confesses that she killed Olivia after learning of the murder. As Julia leaves she looks over her shoulder at Mrs. Rudge, who sees Olivias eyes and dies of a fright-induced heart attack. 

At home, Julia sees Olivia, first in the bathroom mirror and then in the living room playing with the clown toy. She takes it from her, offers her a hug and asks her to stay. Julia is then seen bleeding to death, her throat apparently cut on the sharp edges of the clowns cymbals.

==Cast==
*Mia Farrow as Julia Lofting
*Keir Dullea as Magnus Lofting
*Tom Conti as Mark Berkeley Jill Bennett as Lily Lofting
*Robin Gammell as David Swift
*Cathleen Nesbitt as Heather Rudge
*Anna Wing as Rosa Flood
*Edward Hardwicke as Captain Paul Winter
*Mary Morris as Greta Braden
*Pauline Jameson as Claudia Branscombe
*Arthur Howard as Piggott
*Peter Sallis as Jeffrey Branscombe
*Damaris Hayman as Miss Pinner
*Sophie Ward as Kate Lofting
*Hilda Fenemore as Katherine
*Nigel Havers as Estate Agent
*Samantha Gates as Olivia Rudge
*Denis Lill as Doctor
*Julian Fellowes as Library Assistant
*Michael Bilton as Salesman
*Yvonne Edgell as Mrs. Floods Niece
*Robert Farrant as Receptionist
*Oliver Maguire as Nurse
*Susan Porrett as Mrs. Ward
*John A. Tinn as Customer
*Elizabeth Weaver as Mother in Showroom 

==Reception and Release History==
The film was originally released in England as Full Circle but fared poorly at the box office. It was released in the United States under the title  The Haunting of Julia in 1981, but still failed to find its audience. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 