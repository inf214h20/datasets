Invasion of the Body Snatchers (1978 film)
{{Infobox film 
| name           = Invasion of the Body Snatchers 
| image          = invasion_of_the_body_snatchers_movie_poster_1978.jpg 
| caption        = Theatrical release poster by Bill Gold
| director       = Philip Kaufman 
| screenplay     = W. D. Richter 
| based on       =  
| producer       = Robert H. Solo 
| starring       =  
| music          = Denny Zeitlin  Michael Chapman Douglas Stewart
| studio         = Allied Artists Pictures
| distributor    = United Artists 
| released       =  
| runtime        = 115 minutes 
| country        = United States
| language       = English
| rating         = PG
| budget         = $3.5 million 
| gross          = $24,946,533  (North America)  
}}
 thriller  Brooke Adams, Veronica Cartwright, Jeff Goldblum and Leonard Nimoy. Released on December 20, 1978, it is a remake of the 1956 film Invasion of the Body Snatchers, which is based on the novel The Body Snatchers by Jack Finney. The plot involves a San Francisco health inspector and his colleague who discover humans are being replaced by duplicate aliens who appear to be perfect copies of the persons replaced, but devoid of any human emotion.

A box office success, Invasion of the Body Snatchers was very well received by critics, and is considered by some to be among the greatest film remakes.   

== Plot ==
In deep space, a race of gelatinous creatures abandon their dying world. Pushed through space by the solar wind, they make their way to Earth and land in San Francisco. Some fall on plant leaves, assimilating them and forming small pods with pink flowers. Elizabeth Driscoll (Brooke Adams), an employee at the San Francisco health department, is one of several people who bring the flowers home. The next morning, Elizabeths boyfriend, Geoffrey Howell (Art Hindle), suddenly becomes distant, and she senses that something is wrong. Her colleague, health inspector Matthew Bennell (Donald Sutherland), suggests that she see his friend, psychiatrist Dr. David Kibner (Leonard Nemoy). While driving to a book party Kibner is attending, they are accosted by a hysterical man (Kevin McCarthy, in a direct homage to the original film).  The man runs off, and is soon seen dead, surrounded by a crowd of emotionless onlookers. At the party, Matthew calls the police about the incident, and finds them strangely indifferent. An agitated party attendee starts declaring that her husband is not her real husband. Kibner works to reconcile them.  He also suggests that Elizabeth wants to believe that Geoffrey has changed because she is looking for an excuse to get out of their relationship.

Meanwhile, Matthews friend Jack Bellicec (Jeff Goldblum), a struggling writer who owns a bathhouse with his wife Nancy (Veronica Cartwright) discovers a deformed body on one of the beds and calls Matthew to investigate. Noticing that the body (which is adult sized but lacks distinguishing characteristics) bears a slight resemblance to Jack, Matthew breaks into Elizabeths home and finds a semi-formed double of her in the bedroom garden. He is able to get the sleeping Elizabeth to safety, but the duplicate body has disappeared by the time he returns with the police. The body at the bathhouse has also disappeared when Matthew returns there.

Matthew realizes that what is happening is extraterrestrial, and that people are being replaced by copies while they sleep. Matthew calls several state and federal agencies, but they all tell him not to worry. In addition, people who had earlier claimed that their loved ones had changed seem to have been converted as well, including (unbeknownst to him) Dr. Kibner, and repudiate their earlier claims of their loved ones being imposters. 

That night, Matthew and his friends are nearly duplicated by the pods while they sleep. The pod people try to raid Matthews house, but he and his friends are able to escape. During this, they discover that the pod people emit a shrill scream once they learn someone is still human among them.

Jack and Nancy create a diversion within a crowd of pursuing pod people to give Matthew and Elizabeth time to escape. Matthew and Elizabeth are chased across San Francisco. They are eventually found by the doubles of Jack and Dr. Kibner at the Health Department. Kibners double tells them that what the alien species is doing is purely for survival and that they are even doing humanity a favor by ridding them of emotion. Matthew and Elizabeth are injected with a sedative to make them sleep. However, having already taken a large dose of methamphetamine|speed, the couple overpower them and escape the building.

In the stairwell, they find Nancy, who has learned to evade the pod people by hiding all emotion. Outside, Matthew and Elizabeth are exposed as human when Elizabeth screams after seeing a mutant dog with a mans face. They flee, and discover a giant warehouse at the docks where the pods are grown. After Matthew and Elizabeth profess their love for each other, Matthew goes out to investigate, only to discover a cargo ship being loaded with hundreds of pods.

Matthew returns to find that Elizabeth has fallen asleep. He tries to wake her, but her body crumbles to dust and her naked double arises, telling him to embrace his fate and sleep. Matthew returns to the warehouse and sets it on fire, destroying many pods. He hides from the pod people under a pier, but they know he will have to fall asleep eventually.
 City Hall, he is spotted by Nancy, who has avoided conversion into a pod person. She calls his name, to which Matthew responds by pointing to her and emitting the piercing pod scream. Realizing that Matthew is now a pod person, Nancy, now the only human left in the city, screams in helpless terror.

==Cast==
* Donald Sutherland as Matthew Bennell Brooke Adams as Elizabeth Driscoll
* Leonard Nimoy as Dr. David Kibner
* Jeff Goldblum as Jack Bellicec
* Veronica Cartwright as Nancy Bellicec
* Art Hindle as Dr. Geoffrey Howell, DDS
* Lelia Goldoni as Katherine Hendley
* Don Siegel as Taxicab Driver Kevin McCarthy as Running man

==Background==
  Kevin McCarthy, Michael Chapman appears twice as a janitor in the health department.

The film score by Denny Zeitlin was released on Perseverance Records. Despite its popularity and critical praise, it is the only film score Zeitlin has composed.      
 Star Wars, Mark Berger at American Zoetrope in the four-channel Dolby Stereo process, which was not yet standard exhibition equipment in most theaters.

Philip Kaufman said of the casting of Leonard Nimoy, "Leonard had got typecast and this   was an attempt to break him out of that," referring to the similar perks Dr. Kibner (and his pod double) had with Spock.  According to Kaufman, it was Mike Medavoy, head of production at United Artists, who suggested the casting of Donald Sutherland.  Sutherlands character had a similar curly hairstyle as that of his character from Dont Look Now (1973).  "They would have to set his hair with pink rollers every day," recalled co-star Veronica Cartwright.    According to Zeitlin, Sutherlands character was originally written as an "avocational jazz player" early in development.  

==Release==
Invasion of the Body Snatchers earned nearly $25 million in box office revenue in the United States. 

===Critical reception===
Reviews for Invasion of the Body Snatchers have been nearly unanimously positive. It maintains a 95% approval rating on Rotten Tomatoes,  the consensus reading "Employing gritty camerawork and evocative sound effects, Invasion of the Body Snatchers is a powerful remake that expands upon themes and ideas only lightly explored in the original," and is regarded as one of the best films of 1978,   as well as one of the greatest film remakes ever made. 

The New Yorker  s Pauline Kael was a particular fan of the film, writing that it "may be the best film of its kind ever made".  Variety (magazine)|Variety wrote that it "validates the entire concept of remakes. This new version of Don Siegels 1956 cult classic not only matches the original in horrific tone and effect, but exceeds it in both conception and execution."  The New York Times   Janet Maslin wrote "The creepiness   generates is so crazily ubiquitous it becomes funny." 
 Phil Hardys Aurum Film Encyclopedia called Kaufmans direction "less sure" than the screenplay. 
 Best Drama Academy of Best Director, Best Science Fiction Film. Donald Sutherland, Brooke Adams and Leonard Nimoy received additional nominations for their performances. 

===Home video===
Invasion of the Body Snatchers was released on DVD in the United States, Australia and many European countries. The film was released on Blu-ray Disc in the United States in 2010 and in the United Kingdom in 2013.

==Legacy==
The Chicago Film Critics Association named it the 59th scariest film ever made. 

==References==
 

==External links==
 

*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 