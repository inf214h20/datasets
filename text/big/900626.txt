Dear Jesse
 
 
{{Infobox film
| name           = Dear Jesse
| image          = DJ dvd.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Cover art of 2006 DVD of Dear Jesse
| director       = Tim Kirkman
| producer       = 
| writer         = Tim Kirkman
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Joe Klotz
| studio         = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American documentary film by Tim Kirkman that was released theatrically by Cowboy Pictures in 1998.
 Senator Jesse openly gay Lee Smith Mike Nelson, and people in the street, including a brief interview with Matthew Shepard, then a student at Catawba College.

==Production== John Crooke and cinematography by Norwood Cheek.

==Awards and nominations==
In 1998, the film won the Audience Award at Frameline, the San Francisco Gay and Lesbian Film Festival, and was nominated for an Independent Spirit Award and was named Best Documentary of the Year (Runner-Up) by the Boston Society of Film Critics. After the film aired on the HBO/Cinemax “Reel Life” series, Kirkman was nominated for an Emmy Award (Documentary Writing Category).

==External links==
*  

 
 
 
 
 
 
 
 


 
 