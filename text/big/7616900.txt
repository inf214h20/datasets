Old Khottabych
{{Infobox film name = Old Khottabych image = Starik Khottabych DVD cover.jpg caption = Modern DVD cover of the film director = Gennadiy Kazansky Lev Mahtin|L. Mahtin producer = writer = Lazar Lagin screenplay = based on = Lazar Lagin starring = Nikolai Volkov Alexey Litvinov Gennady Khudyakov music = Nadejda Simonyan cinematography = M. Shurukov editing = studio = Lenfilm distributor = released = 1956 runtime = 86 min. country = Soviet Union language = Russian
}} fantasy comedy film produced in the USSR by Lenfilm in 1956 based on a childrens book of the same name by Lazar Lagin. In the USA, the book was published under the title The Flying Carpet.

The film is also known as Starik Khottabych (Soviet original title) and Old Man Khottabych.

== Plot summary == Young Pioneer, ancient vessel at the bottom of a river. When he opens it, a genie emerges. He calls himself Hassan Abdul-rahman ibn Khattab, but Volka renames him Khottabych. The grateful Khottabych is ready to fulfill any of Volkas wishes, but it becomes clear that Volka should use the powers of the genie carefully, for they can have some unforeseen undesirable results.

== History ==
The novel is obviously influenced by the tale of Aladdin and his magical lamp, and it was quite popular with Soviet kids.

There were two major versions of the novel - the original was published in 1938, and a revised version followed in 1955. This later version was the basis of the 1956 film. Revisions to the novel were made by Lagin himself in order to incorporate the changes taking place in the USSR and the rest of the world into the narrative. The newer version also includes some ideological anti-capitalistic elements. The original edition has been republished in the Post-Soviet era.

In 2006, a modern film remake was made. It was called Khottabych. This remake has little in common with the first film, except for the central plot point of finding a genie in a clay vessel, and the central theme of how times have changed, as is emphasized by this very lack of commonality (as well as various anachronism-based humor, like wished-for hundred dollar bills appearing printed on ancient papyrus, a makeover that puts the venerable djinn inside a flashy tracksuit, or the so-old-its-hip-again luxury Soviet vehicle driven by the protagonists).

== Crew ==
* Screenplay by — Lazar Lagin
* Directed by — Gennadiy Kazansky
* Operator — Muzakir Shuruckov
* Art Directors — Isaak Kaplan, Bella Manevich
* Director — Lev Mahtin
* Composer — Nadejda Simonyan
* Sound Operator — Grigory Albert
* Combined photography:
*: Main Operator: Mihail Shamkovich
*: Camera Operators: B. Durov, M. Pocrovsky
*: Art Directors: A. Alekseev, Mihail Krotkin, Maria Kandat
* Director of Picture: Tamara Samoznaeva

== Awards ==
*1958 Moscow International Film Festival award
*1958 Vancouver International Film Festival award

== Trivia == Russian patronymic suffix -ych, yielding a Russian equivalent of ibn-Khattab (son of Khattab).
* Volka (Willie) is a diminutive form of the name Volya which also was a diminutive name of Vladimir.  
* Khottabych later claim to be 3,732 years and 5 months old.

== Video ==
The Film Released on DVD in Russia to 2004. The disc contains 3x Menus Languages, 4x Spoken Languages to Dolby Digital 5.1: Russian Original, English Voice-over, French and Arabic Dubbed Languages; Subtitles on a Russian, English, French, Spanish, Italian, Dutch, Japanese, Swedish, German, Portuguese, Hebrew, Arabic and Chinese languages: also Special Features: "Monologue in the Intermission", "Another Genie", Filmography and Photo Album.

== See also == The Brass Bottle

== External links ==
* 
* 
*   at Maksim Moshkows Library
*  

== References ==
 

 
 
 
 
 
 
 