Ranam
 
{{Infobox Film | 
  name     = Ranam |
  image          = ranam_dvdcover.jpg |
    writer         = Amma Rajasekhar |
  starring       = T. Gopichand Kamna Jethmalani Biju Menon |
  director       = Amma Rajasekhar |
  producer       = Pokuri Babu Rao |
  distributor    = |
  released       = 10 February 2006 |
  runtime        = |
  editing        = Gowtham Raju |
  cinematography = Srinivasa Raju |
  country        = India |
  language       = Telugu  |
  music          = Mani Sharma |
  awards         = |
  budget         = |
}}
Ranam is a Telugu film ( Dubbed in Malayalam as the same title ) which stars T. Gopichand and Kamna Jethmalani. Choreographer-turned-director Amma Rajasekhar directed this film. The film was a success at the box office. This is T. Gopichands third consecutive hit as a lead actor. The film is remade into Oriya as Mahanayak and in Kannada as Bhadra. The film was dubbed into Tamil as Stalin and in Hindi as Suryaa Jalta Nahi Jalata Hai.

== Plot ==

Maheswari (Kamna Jethmalani) is sister of Bhagawati (Biju Menon), a mafia don in Hyderabad. Chinna (T. Gopichand), classmate of Maheshwari, inadvertently enters into a row with the gang members of Bhagavati, do falls in love with Maheswari. The rest of the story is all about the how Chinna resorts to a mind game on Bhagavati to win his sister.

== Cast ==
* T. Gopichand ... Chinna
* Kamna Jethmalani ... Maheswari
* Biju Menon ... Bhagawati

== Music ==
The film has six songs composed by Mani Sharma:

* Cheli Jabili - Naveen & Suchitra

* Varevva - Mahalakshmi & Mallikarjun
 Tippu

* Bulligownu - Jassie Gift

* Nallanimabbu - Vardhini

* Gana Gana - Kay Kay & Sangeetha

==Box Office performance==
*The film had a 100-day run in 24 centres. 

== External links ==
*  

 
 
 
 


 