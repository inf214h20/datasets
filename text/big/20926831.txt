Vengeance (2009 film)
 
 
{{Infobox film
| name           = Vengeance
| image          = Vengeance (2009 film).jpg
| caption        = Theatrical release poster
| alt            = Several neon lights and buildings are seen in the background of a rainy setting. A man walks on a street, wielding a handgun in his right hand. Passerbys are seen in the background behind the man, walking in different directions and holding umbrellas.  The top of the poster lists the Festival de Cannes logo, the films lead actor, the title in both English and Chinese, and the films director; the bottom right of the poster lists the production credits.
| director       = Johnnie To
| producer       =  
| screenplay     = Wai Ka-Fai
| starring       =    Lo Tayu
| cinematography = Cheng Siu-Keung
| editing        = David M. Richarson
| studio         = Milkyway Image
| distributor    =  
| released       =  
| runtime        = 109 minutes
| country        =  
| language       =  
| gross          = $1,346,952   
}}
 Media Asia Films in Hong Kong.

The idea of Johnnie To directing an English-language film originated with the ARP co-founders and French producers Michèle and Laurent Pétin, who had Alain Delon in mind for the lead role.  In 2006, after Delon turned down the role, the Pétins recommended Johnny Hallyday, who was cast in the lead role after meeting with To in early 2008. Principal photography for Vengeance began in November 2008, and concluded in February 2009; filming took place on location in Hong Kong and Macau, with a crew mainly based in Hong Kong.
 62nd Cannes Film Festival, and was released theatrically in France on 20 May 2009. The film was later released in Hong Kong on 20 August 2009. It premiered in North America at the 2009 Toronto International Film Festival. Vengeance was met with positive reviews, with several critics praising Tos direction, Hallydays performance, the cinematography and editing. During its theatrical run, the film grossed over US$1.3 million worldwide, having been released in Asia and parts of Europe. In the United States, the film was distributed by IFC Films, which made it available as a video on demand|video-on-demand selection on pay television formats.

==Plot== Anthony Wong), Lam Ka-Tung) and Fat Lok (Lam Suet), a trio of hitmen who are hired to murder the unfaithful wife of Triad crime boss George Fung (Simon Yam). After overhearing the murder in a hotel room, Costello reaches an unspoken agreement to walk away. Costello later tracks down Kwais syndicate, hands them a stack of Euros and his watch, and asks them to help him avenge his daughters familys deaths. Before doing so, though, he takes a Polaroid picture of each of the hitmen, and writes down their names, so that he will not forget what they look like. Together, the four make their way to the apartment where the shooting occurred and surmise the weapons used.

The four men visit a landfill to meet Kwais cousin Tony, who supplies illegal arms. Tony mentions that he sold matching weapons to a trio of Hong Kong hitmen who work on Seafood Street. As the four men reach the street, they see a man with a bandage over his ear. However, they see only two men. Kwais syndicate decide to wait until the men lead them to the third attacker. The two suspects head towards the countryside as Kwais syndicate follows.

The two men later reach a Nature reserve as the third killer shows up. The trio meet their wives, their kids and Kwais syndicate during the barbecue. After a tense standoff, the trio explain that they killed Costellos grandchildren because the children had seen their faces. Not wanting to start a shootout in front of the three attackers wives and children, everyone waits until they have left. A moonlit gun battle ensues, with both sides suffering injuries. The three attackers flee, while Costello and Kwais syndicate take refuge at a run-down apartment in Hong Kong to perform surgeries by themselves to remove bullets from their bodies. Costello reveals that he is a former assassin and has a bullet in his head for twenty years which is affecting his memories.

Shortly after they have tended to their wounds, Kwai receives a phone call from George Fung, who informs him that three of his men need help, and are with an underground non-registered doctor known as "Old Five". Fung tells Kwai that his men were attacked by three Chinese men and one white man. The four men realize that Fung ordered the hit on Costellos daughter and her family. They head to the dwelling and kill the three attackers and leave Old Five alive, who then makes a call to Fung, reporting what had happened. Fung then contacts Kwai, explaining that Costellos son-in-law was handing over Fungs financial reports to the police. Fung also says he only ordered his men to kill the parents. Kwai replies that his syndicate has switched their loyalty from Fung to Costello as a matter of honor.

Later that night, Fung sends more killers to Kwais refuge in order to eliminate Kwais syndicate. A gunfight ensues, with the four men emerging victorious. As they escape, Kwais syndicate lose track of Costello, who is walking through a crowded street, seemingly lost in the night rain. He pulls out the Polaroid pictures that he took of the hitmen earlier, and tries to find them in the crowd. They meet again, but it becomes evident that Costello has a problem with his memory, and has rapidly forgotten who his friends are, but also what he is doing. The hitmen try to explain to Costello that he is seeking revenge for his daughter, but Costello does not recognize her, nor does he understand the concept of revenge. The three men give up in despair, and take him to a beach where they meet a pregnant woman (Michelle Ye) with a group of children. Kwai gives her a stack of money and ask her to take care of Costello. Kwais syndicate return to Macau because they lost contact with Kwais cousin Tony who supplies their ammunition. When they reach the landfill where Tony is living, they find him and his partner dying as they were tortured by Fung and his gang. Soon, a large group of assassins sent by Fung show up and a gunfight ensues. Kwai, Chu and Fat Lok are killed. Costello and the pregnant woman learn of the shootout during a news report.

The next day, Fung is having tea in a public square surrounded by his subordinates. He sees a beautiful woman (Kwais pregnant friend) having tea in the same square. As he tries to get her attention, a large group of children come asking him to buy charity stickers. The children place stickers on Fung and his men. The pregnant woman leaves the square and meets up with Costello, then tells him that the man back in the square with the most number of stickers is Fung. Costello thanks her, makes his way to the square and begins to shoot a great many of the guards as well as Fung as chaos ensues. However, Fung turns out to be wearing a bulletproof vest. His subordinates help him run away from Costello, who gives chase on foot. As Costello walks, he checks the bystanders for stickers, which tips Fung off to the fact that Costello does not actually recognize him, apart from the stickers he is wearing. Fung removes the stickers from his coat, and puts them on the jacket of a subordinate instead. This causes Costello to shoot Fungs subordinate. Fung then removes his coat with all the stickers and runs off.

Costello picks up the trenchcoat and shoots more subordinates who get in his way. Fung is finally the last surviving member, so he attempts to walk nonchalantly by Costello, relying on the Frenchmans inability to recognize him without the coat, but as he is walking by, Costello sees a sticker on Fungs tie. Both men reach for their guns fire at each other. Fung is hit and Costello demands that he put the coat on, which Fung refuses, so Costello shoots him in the leg. Fung then puts the coat on and Costello checks the bullet holes in the coat and sees that they align with the bullets stuck in the bullet-proof vest he was wearing. Costello then stands up and fires a round into Fungs head killing him.

==Cast==
 
* Johnny Hallyday plays Francis Costello, a retired assassin turned chef. The character is named after Jef Costello, the lead character played by Alain Delon in Jean-Pierre Melvilles 1967 film Le Samouraï.    Anthony Wong plays Kwai, a Hong Kong hitman who agrees to assist Costello during his visit to Macau. 
* Lam Suet plays Fay Lok, Kwais partner in crime.  Lams voice is dubbed by Conroy Chan.  Lam Ka-Tung plays Chu, another one of Kwais partners in crime. Lams voice is dubbed by Terence Yin.
* Simon Yam plays George Fung, the films antagonist. He is a crime boss of Lok, Chu and Kwai. 
Other cast members include Sylvie Testud as Irene Thompson, Costellos daughter; Michelle Ye as a pregnant woman who aids Costello in his fight for revenge; and Vincent Sze as Mr. Thompson, Irenes husband and the father of her two children.  Cheung Siu-Fai, Berg Ng, and Felix Wong appear as a trio of hitmen hired to kill Irene and her family. Maggie Shiu plays Inspector Wong, a Hong Kong police inspector. 

==Production==
{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#F0F8FF; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"...I’ve had this hope to do movies with people outside of Hong Kong, because I feel this exchange is good for me. Also, I wanted to bring more people to Hong Kong to make movies. I think that’s very interesting. This project is from France so it gave me the opportunity to do what I wanted to do."
|-
| style="text-align: left;" |—Director Johnnie To on making Vengeance. 
|}

===Crew=== Lo Tayu The Big action choreographer.

===Development===
Having distributed several of Tos films in France, Michèle Pétin and her husband Laurent discussed the idea of having To direct an English-language feature film.  In March 2006, the Pétins met To in Sai Kung Town, Hong Kong, where they expressed their idea to him. The couple mentioned Alain Delon as a possibility for the lead role.   
 60th Cannes Film Festival.  He met with Delon, who had expressed his interest in working with To, and To promised to meet with Delon once a film treatment was written.  To returned to France in March 2007, where he handed the Pétins a step outline of what he and screenwriter Wai Ka-Fai had envisioned.  Delon, however, was no longer interested in the project.   In July 2007, the Pétins met with French musician and actor Johnny Hallyday, who was interested in making a new film.  After meeting with Hallyday, who had expressed his love for world cinema, the Pétins decided that he would be perfect in the lead role.

{| class="toccolours" style="float: right; margin-right: 1em; margin-right: 2em; font-size: 85%; background:#F0F8FF; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"Hes alone and lost in Hong Kong. I was just like him...Its impossible on your own because people in the street dont speak any English. I really felt out of the loop, like Costello in the film...For this movie, I made use of the present. Everything I was experiencing and feeling in Hong Kong went into my role."
|-
| style="text-align: left;" |—Johnny Hallyday comparing his first visit to Asia to that of his lead character Costello. 
|}
In February 2008, while promoting his previous film, Sparrow (2008 film)|Sparrow, at the Berlin International Film Festival, To was approached by the Pétins, who had promised him that they would set up a meeting with Hallyday.  While Hallyday was a fan of his films, To had never heard of Hallyday. The producers gave To footage from Hallydays concert performances along with a copy of the 2002 French film The Man on the Train, in which Hallyday co-starred. After viewing the given footage, To had expressed that he had enjoyed the film, but was more impressed with Hallydays concert performances.   In March 2008, To finally met with Hallyday over a dinner, expressing their love for music.  Upon their first meeting, To decided that Hallyday would be perfect in the lead role. 

Production plans were nearly put to a halt when To was hired to remake the 1970 French crime film Le Cercle rouge, meaning that Vengeance would not be made until 2010.  However, in June and July 2008, Ding Yuin-Shan, a long-time production assistant and English translator for To, contacted Hallyday and the Pétins, and told them that production plans for The Red Circle were falling behind schedule and that To would be ready to film Vengeance by the end of October 2008.  Hallyday was ecstactic about meeting with the director once again, and prepared for his role by viewing several of Tos previous films, before making his first visit to Hong Kong, where he would get the chance to meet with his co-stars.  Before principal photography began, Hallyday met his co-stars, Anthony Wong, Lam Ka-Tung, Simon Yam, and Lam Suet, during a dinner on 7 November 2008. 

===Filming===
Principal photography for Vengeance took place in Hong Kong and Macau from 15 November 2008 to 31 January 2009. The cast and crew of the film began with a celebration on the rooftop of the Milkyway Image studio in Kwun Tong, Hong Kong.  While the story is set in Macau, several scenes were shot in Hong Kong.

For To, filming Vengeance differed from his usual style of filmmaking.  While he is used to improvising his scenes as a director, Vengeance marked the first time that To had to work with a complete shooting script since the producers demanded that the story and dialogue already be written. Hallyday was the only actor to have read the script, while Anthony Wong had knowledge of the story. To explained that his purpose was to "keep the actors natural and spontaneous. They dont have time to create something. Theyre given a situation and they act it right away." After filming was complete, the cast and crew celebrated by having a final dinner at a restaurant located near the Milkyway Image studio, hoping to meet again at the forthcoming Cannes Film Festival. 

==Release==

===Theatrical run===
  attending the premiere of Vengeance at the Ryerson Theatre during the 2009 Toronto International Film Festival.]]
Vengeance was first released in France on 20 May 2009.   It was later released in Belgium on 27 May 2009.  On 5 August 2009, the film premiered in Asia at the 2009 Hong Kong Summer International Film Festival.  It was released theatrically in Hong Kong on 20 August 2009. Vengeance was also released in other Asian countries, including Malaysia on 27 August 2009; Taiwan on 31 October 2009; and Singapore on 5 November 2009. 

===Home media=== VCD format in Hong Kong.  Releases for the French version include a single-disc edition DVD;  a two-disc special edition DVD;  and a special edition Blu-ray Disc.  In France, the home video formats, along with its special features, do not include English subtitles.

IFC Films currently serves as a North American distributor.  In the United States, the film was released as a video on demand option on pay television formats, beginning on 4 August 2010. 

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, the film received a score of 76 out of 100 based on 5 reviews. 
{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"The films plot serves the fabled Hong Kong director Johnnie To as an excuse to devise uncanny and beautiful visual action set pieces. Hes not much concerned with how we got into them and how we get out of him. Since his hero is losing his memory, he doesnt much care, either."
|-
| style="text-align: left;" |—Roger Ebert, writing for the Chicago Sun-Times 
|}

Vengeance first received praise from several critics who attended the screening at the   wrote, "Vengeance is not top-flight Johnnie To...But the To poetry keeps breaking through: a gun battle in a city park stops and starts as clouds pass before the moon."   
 Breaking News or PTU (film)|PTU out of the Hong Kong crime genre box and turned them into arthouse crossover items."  Perry Lam of Muse (Hong Kong magazine)|Muse Magazine wrote, "Overall, the movie lacks the flashes of life and brilliance that mark the best works in the genre." 

===Box office===
In France, Vengeance was released to 280 theatres and in its first week, opened at eleventh place in the box office, grossing only US$539,809, and selling 63,240 tickets.  The films revenues decreased by 66.9% in its second weekend, moving down to fourteenth place and earning $178,459 at the box office.  After only two weeks of release, Vengeance grossed a total of $744,881 in France alone. 

Vengeance was released in Belgium seven days after its release in France. On the weekend of 27 May 2009 to 31 May, the film opened at 22nd place in the box office, earning $10,397 on the opening weekend, with a total gross of $11,439.  The film dropped down to the 29th spot in the box office, grossing only $6,886 in its second week.   At the end of its three weeks of theatrical release in Belgium, Vengeance grossed a total $27,377. 

Vengeance was later released in Hong Kong, where it opened at sixth place, grossing $121,837 on its opening weekend.  The film dropped down to tenth place in its second week, grossing $37,629 for a total gross of $208,976.  During the next three weeks of its release, Vengeance continued a decrease in revenue as well as the number of theatres screening the film.  The film dropped down to 21st spot on 3 to 6 September 2009 weekend, grossing only $1,951.  By the end of its theatrical run in Hong Kong, the film grossed $236,027.  In total, Vengeance has grossed $1,346,952 worldwide, despite not being released in other parts of Europe. 

===Accolades=== 62nd Cannes Film Festival.  In North America, the film premiered as a "Special Presentations" feature at the Ryerson Theatre during the 2009 Toronto International Film Festival. 
  with his wife Laeticia, promoting the film at the 2009 Cannes Film Festival.]]
{| cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;" 
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center" 
! Award
! Category
! Name
! Result
|- 29th Hong Kong Film Awards  Best Original Film Score Lo Ta-yu|Lo Tayu Nominated
|- 4th Asian Film Awards  Best Composer Lo Tayu Won
|- Best Cinematography Cheng Siu-Keung Nominated
|}

==Remake==
Israeli directors Nevot Papushdo and Aaron Kashels were hired by Sony Pictures to direct an American version of the film.

==See also==
 
* French films of 2009
* Hong Kong films of 2009 Triad

==References==
;Footnotes
 

==External links==
*   
*  
*  
*  
*  
*   at the Movie Review Query Engine
*   at Box Office Mojo

 
 

 

 
 
   
   
 
 
 
 
 
 
 
 
 
   
 
 
 
 
 