Crossing the Line (2006 film)
 
{{Infobox film
| name           = Crossing the Line
| image          = Crossing the line region 1 dvd 2006-07.jpg
| alt            = 
| caption        = Region 1 DVD Daniel Gordon Nicholas Bonner Daniel Gordon Daniel Gordon
| starring       = 
| music          = Heather Fenoughty
| cinematography =Nick Bennet
| editing        = Peter Haddon
| studio         =
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English Korean
| budget         = 
| gross          = $9,258 (USA)
}}
 Daniel Gordon and Nicholas Bonner.

==Synopsis== James J. North Korea Daniel Gordon and Nicholas Bonner, and was shown at the 2007 Sundance Film Festival. Crossing the Line, which was narrated by actor Christian Slater, was nominated for the Grand Jury Prize at the festival.

==Production==
It was first screened in 2007 on the BBC. The film centred on Dresnoks history, highlighting his unhappiness in United States|America, and particularly his desertion from the United States Army in 1962 to the North Korea|DPRK. It also showed Dresnok in the present day in Pyongyang (where he now lives), interacting with his North Korean family and friends. Dresnok spoke exclusively to the filmmakers about his childhood, his desertion, his life in a country completely foreign and quite hostile to his own, his fellow defectors, and his wife and children. 

Dresnok is also shown with fellow defectors, including Charles Robert Jenkins, who returned to Japan to be with his wife, Hitomi Soga (a victim of kidnap by the North Koreans), while filming was taking place. Dresnok felt hurt by Jenkins allegations of physical abuse by Dresnok and the North Korean regime and angrily denied them. Towards the end of Crossing the Line, a North Korean doctor discloses to the BBC that Dresnok is in failing health, mainly due to heavy drinking and smoking.   

==Cast==
*Christian Slater as narrator
*Charles Robert Jenkins
*James Joseph Dresnok

==Reception==
The movie had mostly positive  reception.    

==See also==
* List of documentary films about North Korea

== References ==
 

==External links==
* 

 
 
 
 
 
 