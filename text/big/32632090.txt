Cry Baby Lane
{{Infobox television film
| name           = Cry Baby Lane
| image          =
| caption        = 
| genre          = Horror
| director       = Peter Lauer
| producer       = Albie Hecht   Jerry Kupfer
| writers        = Peter Lauer Robert Mittenthal Jase Blankfort Trey Rogers Frank Langella
| music          = Andrew Barrett
| photography    = John Inwood
| editing        = Doug Able
| studio         = Centre Street Productions   Constant Communications   Flying Mallet, Inc.
| network        = Nickelodeon
| released       = October 28, 2000
| runtime        = 96 minutes  (approximately)    70 minutes  (without commercials) 
| country        = United States
| language       = English
| budget         = $800,000 
}}
Cry Baby Lane (original teaser title: Someone Wants to Meet You) is a 2000   and the ensuing reaction prompted TeenNick to re-air the film on October 31, 2011.

In its original air date, this TV movie was originally rated TV Parental Guidelines|TV-Y7 when it first premiered, on United States Television. It was re-rated TV Parental Guidelines|TV-PG-V for moderate violence on its rerelease.

==Plot== Jase Blankfort) and his older brother Carl (Trey Rogers) enjoy listening to ghost stories that the local undertaker, Mr. Bennett (Frank Langella), tells them. One night Bennett tells the tale of a local farmer whose wife gave birth to conjoined twins, one being good-natured while the other was clearly evil. The farmer, ashamed of them, kept the twins locked in their room. Eventually the twins got sick from a liver disease and died together, so the farmer sawed them in half and buried the good twin in a cemetery and the bad twin in a shallow grave near the house, at the end of an old dirt road called Cry Baby Lane, as whoever is caught out there at night will hear the cries of the deceased twin. Later, Andrew, Carl, and a group of friends decide to hold a séance in the cemetery where the good twin is buried, but soon after the seance, creepy phenomenon occurs around the town. When Andrew and Carl consult Mr. Bennett about it, he confesses that when the twins were unjoined, the farmer mixed up the twins and tossed the good one in the field and that the good twin is crying for help, not vengeance, and the bad twin possesses nearly everyone in town, and it is up to Andrew to stop him. However, during the time Andrew and Carl journey into Cry Baby Lane, the bad twin intervenes and possesses Carl who then tries to attack his brother, the evil twin speaking for Carl telling Andrew he cant stop his doing as the cries of the good twin become louder and more desperate. Andrew eventually escapes the car and journeys into the good twins grave where he must cut a root that was wrapped the good twins skeletal in order to regain peace. The evil twin showers dirt onto the grave, attempting to bury Andrew alive, but Andrew manages to cut the root and saves the good twin, the evil twin disappearing in a flash of light. 
The next day, Andrew wakes up outside of the grave where he finds the good twins grave with flowers, alluding that the twin is now at peace. Andrew then picks a flower from the grave and gives it to Ann as they leave Cry Baby Lane.    

== Cast and crew ==
Cry Baby Lane was filmed in an abandoned town in New Jersey, with a few scenes in Tontogany, Ohio. It was produced by Albie Hecht and Jerry Kupfer.  The editor was Doug Able, and John Inwood served as the director of photography.  The supporting cast included:   , The New York Times Movies 
 Jase Blankfort as Andrew
* Trey Rogers as Carl
* Larc Spies as Kenneth
* Frank Langella as Bennett
* Anne Lange as Ann Weber 
* Marc John Jefferies as Hall
* Allison Siko as Louise
* Jessica Brooks Grant as Megan
* Sheri Drach as Kathy
* Gary Perez as Gary Steve Mellor as Dick Weber
* Bernadette Quigley as Mrs. Hunt
* Carl Burrows as Evil Sheriff
* Jim Gaffigan as Bob
* Rob Newton as Evil Twin
* Andie K. Taylor as Becky

== Production ==
 The film was originally envisioned as $10 million theatrical release for Nickelodeon, but it was instead ordered to be a made-for-television movie with a budget of $800,000. The film was shot in a condemned neighborhood in New Jersey in a little over twenty days with an extra day of shooting in Ohio for shots of the town. The director originally wanted Tom Waits for the role of the caretaker, but Nickelodeon insisted on Frank Langella in hopes that it would garner extra publicity.   

==Availability==
Nickelodeon never released the film or re-aired it, supposedly due to controversy involving several complaints from parents, who claimed the film was too frightening and inappropriate for children. Cry Baby Lane was thought to be a lost film for over a decade, and gained a large cult following due to its obscurity. A search began for the lost film, and in August 2011, the user Firesaladpeach from the website Reddit recovered a VHS copy recorded from the films original broadcast and made it available online. Director Peter Lauer was interviewed soon after and said that he was surprised and flattered by the attention his film had gotten 11 years after its original release, being unaware of its supposed banning by Nickelodeon: "I just assumed they didnt show it again because they didnt like it! I did it, I thought it failed, and I moved on."  A Nickelodeon representative later claimed that the film was never banned; it was just forgotten.   

The reaction from Reddit, as well as the success of The 90s Are All That, prompted TeenNick to re-air Cry Baby Lane on the night of October 31, 2011. It reran again on March 31, 2015, as part of a week of specials on The 90s Are All That.  It was promoted as being banned from television. 

==Reception==
The reviews were mixed. IMDB gave it a 5.8. 

== See also ==
* List of films broadcast by Nickelodeon

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 