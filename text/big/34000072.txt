Buddy the Dentist
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image = 
| caption = 
| director = Ben Hardaway
| story_artist =  Jack King Jack Carr Bernice Hansen Billy Bletcher (all uncredited) Norman Spencer
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = December 15, 1934 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| preceded_by = Buddys Adventures (1934)
| followed_by = Buddys Theatre (1935)
}}
 animated short film, released December 15, 1934 (though one source gives March 5 as a date of copyright. ) It is a Looney Tunes cartoon, featuring Buddy (Looney Tunes)|Buddy, the second star of the series. It was supervised by Ben Hardaway; musical direction was by Norman Spencer.

==Summary== stove for a moment, Bozo the dog burns his tongue attempting to taste the mixture. Buddy returns with a tube to squirt the fudge onto a pan, disregarding the odd behavior of his pet; as he arranges the fudge on a pan, he tosses some into Bozos mouth while lecturing the dog on how harmful candy might be to a dogs teeth.

Buddy orders the dog to sit and be quiet as he   (which Buddy always uses for fudge.) Bozo wanders off, back to the kitchen, and spills the fudge all about the floor, then eating and, true to Buddys admonition, causing a sharp spike to hurt his teeth. The dog yelps in pain, Buddy yells, "shut up!" (which Cookie misinterprets as directed towards her; she hangs up.)

Buddy walks into the kitchen, finds his fudge ruined and Bozo hiding under a table; Buddy pulls the dog out from under the table and reprimands him with a promise that "candy would hurt your teeth". Buddy attempts to remove his dogs loose tooth with  .  Buddy attaches a gas pipe with a mouthpiece to Bozos mouth, and the dog inflates, floats upward: Buddy gets him down with a vacuum cleaner, which then explodes, trapping Buddy in an ironing board compartment. Buddy next decides to tie one end of a string to a dog toy, the other to Bozos damaged tooth; when that only serves to amuse the animal, Buddy then decides to tie up the tooth and tie the other end of the string to a doorknob, the closing action of the door then serving to force the loose tooth from the dogs gums.

Bozo is trepidatious; but as Buddy prepares to demonstrate how little the method hurts, a  . The cat gets away, as Buddy and his dog become trapped in an hammock occupied by Cookie. All three emerge from the fallen hammock: Buddy discovers that he has got the troubled tooth, but Cookie, only mildly disgusted, finds a tooth clearly missing from Buddys mouth.

==Millar and Frisby==
Early in the cartoon, Buddy is seen to be making his fudge with "Millars Cocoa," a reference to Melvin Millar. Later on, as Bozo chases the cat (and drags Buddy along), they pass a billboard that advertises "Frisby", a reference to Friz Freleng; a similar reference to Freleng occurs in High Diving Hare.

==Dating discrepancy== relevant section of the article on Buddys Circus.

==References==
 
 

==External links==
*  

 

 
 
 
 
 