Tarzan and the Trappers
{{Infobox Film | name = Tarzan and the Trappers
  | image = Tarzan and the Trappers (movie poster).jpg
  | caption = 
  | director = Charles F. Haas Sandy Howard H. Bruce Humberstone (uncedited)		
  | producer = Sol Lesser
  | writer = Frederick Schlick, Robert Leach
  | based on =   Eve Brent Rickie Sorensen Lesley Bradley
  | music = Audrey Granville William Snyder Alan Stensvold
  | editing = George Gittens	
  | distributor = Sol Lesser Productions 
  | released = 
  | runtime = 70-74 min.
  | country = United States
  | language = English 
  | budget = 
}}
Tarzan and the Trappers (1958) is an action adventure film featuring Edgar Rice Burroughs famous jungle hero Tarzan and starring Gordon Scott, Eve Brent, Rickie Sorensen and Lesley Bradley. The movie was filmed as three pilot episodes for a television series which were edited into a feature film when the project was abandoned, and so was released in black and white rather than color, like other contemporary Tarzan films. The film did finally appear on television, but only in 1966. It was shot in Chatsworth, Los Angeles|Chatsworth, California. 

==Plot== Jane (Eve Saul Gorse) Sherman Crothers), and trap the ape man when he tries to free him. Tyanas tribe rescues the two. Finally, the hunters reach Zarbo, but find it empty of both people and treasure. In a final conflict, Tarzan overcomes the villains, who are then turned over to the authorities by the natives.

==External links==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 


 