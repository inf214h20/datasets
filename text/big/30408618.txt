Rebecca H. (Return to the Dogs)
 
{{Infobox film
| italic title   = force
| name           = Rebecca H. (Return to the Dogs)
| image          = 
| caption        = 
| director       = Lodge Kerrigan
| producer       = Lodge Kerrigan
| writer         = Lodge Kerrigan
| starring       = Géraldine Pailhas
| music          = 
| cinematography = Laurent Brunet Josée Deshaies
| editing        = Marion Monnier
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = France United States
| language       = French English
| budget         = 
}}
Rebecca H. (Return to the Dogs) is a 2010 French-American drama film directed by Lodge Kerrigan. It was entered into the Un Certain Regard section of the 2010 Cannes Film Festival.   

==Cast==
* Géraldine Pailhas as Rebecca Herry / Géraldine Pailhas
* Pascal Greggory as Jérôme Herry / Pascal Greggory
* Magali Woch as Passerby (as Magalie Woch)
* Lodge Kerrigan as Director
* Ilan Cohen as Translator
* Paco Boublard as Neighborhood Teenager
* Jérémy Lornac as Neighborhood Teenager
* Louise Szpindel as Neighborhood Teenager Nicolas Moreau as Angry Neighbor
* Antoine Régent as Travel Agent
* Aurélien Recoing as Internet Stranger
* Gérard Kuhnl as Murderer (as Gérard Kuhln)
* Valérie Dréville as Detective
* Gérard Watkins as Detective

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 