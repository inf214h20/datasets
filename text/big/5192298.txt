Who Killed Who?
 
{{Infobox film
| name           = Who Killed Who?
| image          = Who Killed Who.jpg
| image size     =
| alt            =
| caption        = The original release poster
| director       = Tex Avery
| producer       = Fred Quimby (unc.) Joseph Barbera (unc.)
| writer         =
| narrator       =
| starring       = Robert Emmett OConnor (Uncredited live action appearance) Billy Bletcher (Voice of police officer, Uncredited) Tex Avery (Uncredited, Voice of Santa Claus) Kent Rogers (Uncredited, Voice of the victim with an impression of Richard Haydn)
| music          = Scott Bradley (unc.)
| cinematography = Bob Clampett
| editing        =
| studio         = Metro-Goldwyn-Mayer cartoon studio
| distributor    = Metro-Goldwyn-Mayer 
| released       =  
| runtime        = 8 minutes 7"55 minutes (edit)
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Who Killed Who? is a 1943 Metro-Goldwyn-Mayer cartoon studio|Metro-Goldwyn-Mayer animated short directed by Tex Avery.  The cartoon is a parody of whodunit stories and employs many clichés of the genre for humor.

==Plot==
A live-action host (Robert Emmett OConnor) opens with a disclaimer about the nature of the cartoon, namely, that the short is meant to "prove beyond the shadow of a doubt that crime does not pay."
 bumped off." Someone throws a dagger with a letter attached, telling the master that he will die at 11:30. When he objects, another letter informs him that the time has been moved to midnight.

True to form, on the final stroke of midnight a mysterious killer in a heavy black cloak and hood shoots him dead with a rather large pistol (how dead he is, though, is a matter of question), and a police officer (voiced by Billy Bletcher, modeled on characters portrayed in film by Fred Kelsey) immediately begins to investigate. After investigating the premises and the staff, the officer gives a lengthy chase to the real killer, finding the mansion to be filled with many surreal pitfalls, strange characters---including a red skeleton, a parody of Red Skelton---and booby traps that slow and obstruct him. He eventually traps the killer and unmasks him, revealing him to be the opening-sequence host, who confesses "I dood it"---one of Skeltons catchphrases---before bursting into tears.

==External links==
* 

 
 
 
 
 
 
 


 