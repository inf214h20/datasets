Los Amantes del Señor de la Noche
{{Infobox Film
| name           = Los Amantes del Señor de la Noche
| image          = 
| caption        = 
| director       = Isela Vega
| producer       = 
| writer         = Hugo Arguelles Isela Vega
| narrator       = 
| starring       = Isela Vega Irma Serrano Emilio Fernández Lilia Prado
| music          = Pedro Plasencia
| cinematography = Angel Bilvatua
| editing        = Federico Landeros
| distributor    = Producciones Fénix 1986
| runtime        = 90 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Los Amantes del Señor de la Noche (The Lovers of the Lord of the Night) is a Mexican motion picture categorized as Horror film|horror. It was filmed in 1986.

== Synopsis  ==
Venusita (Elena de Haro) falls in love with the son of a wealthy family whose mother sends her son off to the United States in order to keep the two apart. Not to be rejected so easily, Venusita visits Saurina (Irma Serrano) the sorceress who comes up with a spell that kills off the merchant and zaps the son back home, but Venusitas problems are far from over.

== Cast ==
* Isela Vega... Nana Amparo
* Irma Serrano... Saurina
* Elena de Haro...Venusita
* Emilio Fernández... Don Venustiano
* Lilia Prado... Celina
* Arturo Vázquez... Pedro
* Andres García... Amparo´s Lover

==Production Notes==
The movie is about witchcraft and a passion thriller around it. It was written by a very known Mexican writer Hugo Argüelles in conjunction with Isela Vega. Great actors appear in the movie including Emilio Fernández and the always enigmatic Irma Serrano who is already celebrity of its own. This film passed unnoticed in Mexico in part thanks to the lousy distribution system of that time, which used to privilege the foreign movies. Nowadays there are no longer Mexican movies so the distributors no longer have the concern of programming Mexican movies.The movie could had turn into a cult movie. It has a good plot, good intentions, interesting actors and low budget.

== External links ==
*  

 
 
 
 
 
 
 


 
 