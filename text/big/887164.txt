The Assassination Bureau
 
 
{{Infobox film
| name           = The Assassination Bureau Limited
| image          = Assassinationbureau poster.jpeg
| caption        = Theatrical release poster
| writer         = Screenplay:   Robert L. Fish
| starring       = Oliver Reed Diana Rigg Telly Savalas Curt Jürgens
| director       = Basil Dearden
| producer       = Michael Relph
| cinematography = Geoffrey Unsworth
| distributor    = Paramount Pictures
| released       = 10 March 1969
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| music          = Ron Grainer
| budget         =
}}

The Assassination Bureau Limited (released in North America as The Assassination Bureau) is a black comedy film made in 1969 based on an unfinished novel, The Assassination Bureau, Ltd by Jack London. It stars Oliver Reed, Diana Rigg, Telly Savalas, and Curt Jürgens and was directed by Basil Dearden. Unlike Londons original novel, which is set in the United States, the film is set in Europe.

==Plot==
In London, during the early 1900s, aspiring journalist and womens rights campaigner Sonia Winter (Rigg) uncovers an organisation that specialises in killing for money, the Assassination Bureau, Limited. To bring about its destruction, she commissions the assassination of the bureaus own chairman, Ivan Dragomiloff (Reed).

Far from being outraged or angry, Dragomiloff is amused and delighted and decides to put it to his own advantage. The guiding principle of his bureau, founded by his father, has always been that there was a moral reason why their victims should be killed – these have included despots and tyrants. More recently though, his elder colleagues have tended to kill more for financial gain than for moral reasons. Dragomiloff, therefore, decides to accept the commission of his own death and challenge the other board members: Kill him or he will kill them.

With Miss Winter in tow, Dragomiloff sets off on a tour of Edwardian Europe, challenging and systematically purging the bureaus senior members. Little do they realise that this is a plot by Miss Winters sponsor, newspaper publisher Lord Bostwick (Savalas), to take over the bureau and plunge Europe into war&nbsp;— Bostwick is the bureaus vice-chairman and is bitter for having been passed over in favour of the founders son.

Bostwick and the other members of the Bureau plan to get rich quick by the "biggest killing" of them all&nbsp;— buying stocks in arms factories and then propelling Europe into war by assassinating all the heads of state of Europe while they attend a secret peace conference.

Dragomiloff and Miss Winter uncover the plot&nbsp;— dropping a bomb from a Zeppelin airship on to the castle in Ruthenia where the kings, emperors and presidents of Europe are trying to avoid a possible war caused by the death of a Balkan prince who was killed by a bomb intended for Dragomiloff.

Dragomiloff steals aboard the airship and destroys it, killing the remaining members of his board of directors. He is then decorated by the heads of state he has saved. It is implied that Dragomiloff may wed Miss Winter as well.

==Cast==
* Oliver Reed as Ivan Dragomiloff 
* Diana Rigg as Sonya Winter 
* Telly Savalas as Lord Bostwick 
* Curd Jürgens as General von Pinck 
* Philippe Noiret as Monsieur Lucoville 
* Warren Mitchell as Herr Weiss 
* Beryl Reid as Madame Otero 
* Clive Revill as Cesare Spado 
* Kenneth Griffith as Monsieur Popescu 
* Vernon Dobtcheff as Baron Muntzof 
* Annabella Incontrera as Eleanora Spado 
* Jess Conrad as Angelo 
* George Coulouris as Swiss Peasant
* Felix Felton as Cellar Proprietor (uncredited)

==Video releases==
This film has been released both as a VHS video and as a Region 1 DVD.

==See also== Assassinations in fiction

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 