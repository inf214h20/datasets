Four (2011 film)
 
{{Infobox film
| name           = Four
| image          = four2011.jpg
| caption        = 
| writer         = Paul Chronnell Craig Conway Kierston Wareing Sean Pertwee
| director       = John Langridge Craig Conway
| music          = Raiomond Mirza
| cinematography = Adrian Brown
| editing        = John Langridge Ben King
| studio         = Oh My! Productions Ltd
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = £500,000
| gross          = 
}}

Four is a British independent film directed by John Langridge and released in 2011.

==Plot==
A jealous husband hires a movie-obsessed detective to kidnap his wifes lover and bring him to a derelict factory to administer some rough justice. Once there, the husband discovers the detective has a revelation of his own. He has kidnapped the husbands wife as well. 

==Cast==
* Martin Compston as Lover Craig Conway as Husband
* Kierston Wareing as Wife
* Sean Pertwee as Detective
* George Morris as Sergeant Walker

==Reviews== Paul Bradshaw Tarantino comparisons Pinterish dialogue" and ending up "much like a horror movie." 

The Guardian called it "confusing and boring",”  while The Independent wrote that the script "meditates on male insecurity and possessiveness", acknowledging however that "the attempt at menace unwisely borrows quotations from Hollywood movies,   that make   sound rather wannabe in consequence.” 
 Time Out cockernee slang and an initially amusing, increasingly wearying overuse of the fuck|F-word. 

==References==
 

==External links==
*  
*  

 
 