The Mango Tree (film)
 
{{Infobox Film
| name           = The Mango Tree
| image          = 
| image_size     = 
| caption        = 
| director       = Kevin Dobson
| producer       = Michael Pate 
| writer         = Michael Pate
| based on = novel by Ronald McKie
| starring       = Christopher Pate Geraldine Fitzgerald
| music          = Marc Wilkinson
| cinematography = Brian Probyn
| editing        = John Scott
| studio = Pisces Productions
| distributor    = Greater Union
| released       = 13 December 1973
| runtime        = 104 mins (Australia) 95 mins (international)
| country        = Australia
| language       = English
| budget         = A$800,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p320 
| gross          = A$1,028,000 (Australia)
}} Australian drama film directed by Kevin Dobson and starring Geraldine Fitzgerald and Sir Robert Helpmann. Lead actor Christopher Pate is the son of actor Michael Pate who also produced the film.

==Plot==
The film is about Jamie, a young man in his formative teen years, growing up in rural subtropical town of Bundaberg, Queensland, Australia, set around World War I.  Jamie, raised by this grandmother, enjoys his life in "Bundy", until the towns reaction to the insanity of a local preacher leads him to leave his hometown for life in the city.

==Cast==
*Christopher Pate as Jamie Carr
*Geraldine Fitzgerald as Grandma Carr
*Robert Helpmann as the Professor Gerard Kennedy as Preacher Jones
*Gloria Dawn as Pearl
*Carol Burns as Maudie Plover
*Barry Pierce as Agnus McDonald
*Diane Craig as Miss Pringle
*Ben Gabriel as Wilkenshaw
*Gerry Duggan as Scanlon
*Jonathan Atherton as Stinker Hatch
*Tony Bonner as Captain Hinkler
*Tony Foley as Private Davis
*Tony Barry as Tommy Smith
*Terry McDermott as Somers

==Production==
Michael Pate was a neighbour of Robert McKie, who wrote the novel. Pate opted it and tried to find a writer to adapt it into a screenplay but ended up doing it himself. The budget was raised from the Australian Film Commission, GUO Film Distributors and the Bundaberg Sugar Company; the latter invested a third of the total sum. The budget was originally $650,000 but production of the film was delayed by 12 months by which time inflation meant it had risen to $800,000. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p258 

Michael Pate originally wanted to direct the film himself but the AFC did not want him to write, produce and direct, so insisted he find a director. Pate proposed Michael Lindsay-Hogg, son of Geraldine Fitzgerald, who was unable to do it. Bruce Beresford almost directed until the South Australian Film Corporation, who had the director under contract, intervened and prohibited him from making it. Then in February 1977, two months before shooting was to begin, Pate hired Kevin Dobson. 

Filming took place in the town of Gayndah, Mount Perry and Cordalba as well as Bundaberg. The shoot went for seven weeks starting April and ending in June. 

==Release==
The Mango Tree enjoyed reasonable success, grossing $1,028,000 at the box office in Australia,  which is equivalent to $4,728,800 in 2009 dollars.

Critical reaction was muted, with much criticism falling on the performance of Christopher Pate. However film writer Brian McFarlane later wrote that Geraldine Fitzgerald gave "one of the most luminous performances by an actress in Australian film."  The movie achieved only limited sales overseas. Peter Beilby & Scott Murray, "Michael Pate", Cinema Papers, May–June 1979 p401 

Kevin Dobson later claimed he felt that the script was never quite right and "one or two performances were a little shonky" but that he has great affection for the movie. Scott Murray, "Kevin Dobson", Cinema Papers, Jan-Feb 1982 p14  There are several different edits of the film available, one by Dobson, one by Pate. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
*  at the National Film and Sound Archive
*  at SBS Films
*  at Oz Films
 
 
 
 
 
 