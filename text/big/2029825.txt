X-Men: The Last Stand
 
 
{{Infobox film
| name           = X-Men: The Last Stand
| image          = X-Men The Last Stand.jpg
| caption        = Theatrical release poster
| director       = Brett Ratner
| alt            = The poster shows Wolverines claws unsheathed in front of a big X representing "X3". At the middle is the title while at the bottom are the production credits and rating.
| producer       = {{Plainlist|
* Lauren Shuler Donner Ralph Winter
* Avi Arad}}
| writer         = {{Plainlist|
* Simon Kinberg
* Zak Penn}}
| based on       =  
| starring       = {{Plainlist| 
* Hugh Jackman
* Halle Berry
* Ian McKellen
* Famke Janssen
* Anna Paquin
* Kelsey Grammer
* James Marsden
* Rebecca Romijn
* Shawn Ashmore
* Aaron Stanford
* Vinnie Jones
* Patrick Stewart}}
| studio         = {{Plainlist|
* Marvel Entertainment
* Donners Company
* Ingenious Film Partners }}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = United States United Kingdom   John Powell
| cinematography = Dante Spinotti
| editing        = {{Plainlist| Mark Helfrich
* Mark Goldblatt Julia Wong}}
| language       = English
| budget         = $210 million 
| gross          = $459.3 million 
}} John Byrne, mutants and humans, and on the resurrection of Jean Grey.
 most expensive film at the time of its release. It had extensive visual effects created by 11 different companies.

X-Men: The Last Stand was released on May 26, 2006, to commercial success. It grossed approximately $459 million worldwide, becoming the seventh-highest grossing film of 2006 and the highest grossing film in the series, until it was surpassed by   in 2014. Critical reception was mixed, with the acting and the action scenes receiving positive notice, and criticism directed at the screenplay, overuse of characters and style.

==Plot==
 
 Professor Charles mutant as the boy tries to cut off his wings.
 mutants their abilities, offering the "cure" to any mutant who wants it. The cure is created from the genome of a young mutant named Leech (comics)|Jimmy, who lives at the Worthington facility on Alcatraz Island. While some mutants are interested in the cure, including the X-Mens Rogue (comics)|Rogue, many others are horrified by the announcement. In response to the news, Lensherr, now known as the X-Mens adversary Magneto (comics)|Magneto, reestablishes his Brotherhood of Mutants with mutants who oppose the cure, warning his followers that the cure will be forcefully used to exterminate the mutant race.
 Mystique and the loss Wolverine and Storm to telekinetically floating rocks, Cyclops glasses, and an unconscious Jean. Cyclops himself is nowhere to be found.

When they return to the X-Mansion, Xavier explains to Wolverine that, when Jean sacrificed herself, she unleashed the powerful alternate personality she calls "Phoenix Force (comics)|Phoenix", which Xavier had telepathically repressed, fearing the Phoenixs destructive potential. Wolverine is disgusted to learn of this psychic tampering with Jeans mind but, once she awakens, Wolverine realizes she killed Cyclops and is not the Jean Grey he once knew. The Phoenix awakens, knocks out Wolverine, and escapes to her childhood home.

Magneto learns of Jeans resurrection through Callisto, and the X-Men arrive at the Grey home at the same time as the Brotherhood.  Magneto and Xavier vie for Jeans loyalty until the Phoenix resurfaces. She destroys the house and disintegrates Xavier before leaving with Magneto. The Brotherhood decides to strike Worthington Labs, and Magneto uses his powers to move the Golden Gate Bridge so he can connect Alcatraz to the San Francisco mainland and facilitate the attack. The X-Men confront the Brotherhood, despite being significantly outnumbered, and arrive just as the military troops, who thus far had been neutralizing the attacking mutants, are overwhelmed by the Brotherhood.
 Beast injects Magneto with the cure, nullifying his powers. Army reinforcements arrive and attack Jean, awakening the Phoenix, who uses her powers to disintegrate the troops. As the Phoenix begins to destroy the facilities and mutants at Alcatraz, Wolverine realizes that, due to his healing factor, only he can approach the Phoenix. Wolverine approaches her, and Jean momentarily gains control, begging him to save her. Wolverine fatally stabs Jean, stopping the devastating force, but mourns her death.
 Bobby Drake that she has taken the cure, while Magneto sits alone at a chessboard in a San Francisco park. As he gestures toward a metal chess piece, it wobbles slightly, suggesting that his powers are returning.

In a post-credits scene, Dr. Moira MacTaggert checks on a comatose patient who greets her with Xaviers voice, leaving her startled.

==Cast== Logan / Wolverine
:A Canadian mutant born with hyper-acute senses, claws on his hands, and an accelerated healing factor that made possible to implant a coating of the indestructible metal alloy adamantium on his skeleton. Jackman was pleased to see that the script allowed Wolverine to expand his character choices, as instead of  questioning whether he would remain a loner or join the X-Men, Logan now is asked if he will play a leadership role in the X-Men. 
 Ororo Munroe / Storm
:One of Xaviers earlier students and the leader of the X-Men in Cyclops absence, Storm is a woman with the ability to manipulate the weather.  Berry had stated during interviews for X2 that she would not return unless the character had a significant presence comparable to the comic book version, leading to a larger role in The Last Stand s script.    Berry declared that her ethnicity made the actress identify with the cure plot: "When I was a child, I felt that if only I could change myself, my life would be better. As I’ve gotten older, I’ve come to terms with what utter nonsense that is."  The character was given a more modern haircut, and costume designer Judianna Makovsky opted to give Storm more black clothes, a color she only wore in the leather costume for previous films, to make her "tougher and sexier". "Clothing vs. Costume". X-Men: The Last Stand DVD, 2006, 20th Century Fox Home Entertainment 
 Erik Lehnsherr / Magneto
:Leader and founder of the Brotherhood, Magneto is a   and the London theatre, going as far as filming the actor in England to later superimpose into the Vancouver plates. 
 Jean Grey / Phoenix Class 5 telekinetic powers. Her mutant powers rival those of Xavier. Haley Ramm plays Jean as a child. The writers described the multiple personalities was "an Oedipal drama played out", where the Phoenix was "someone embodying   Greek goddess" , while Jean Grey kept the character as "a human, grounded in Freudian terms, a victim, a schizophrenic.    To mark the change from Jean Grey to Phoenix, her wardrobe focused on red colors, and everyday fabric in contrast to the leather costumes of the X-Men.  Digital make-up also made Jeans face darken her skin, show some veins and have her eyes go black. 
 Marie / Rogue
:A young mutant woman from the Deep South whose power causes her to temporarily take on the powers of anyone she touches, leaving her victims (mutant or human) unconscious, Rogues lack of control over her power causes a great deal of strain on her relationship with Iceman. Paquin declared that while Rogue did not have "a large physical component in this movie", the "adult decisions" the character was forced to do made for more intensity on the emotional side. 

* Kelsey Grammer as Beast (comics)|Dr. Hank McCoy / Beast
:A former student of Xaviers Institute for Gifted Mutants who is now a member of the Cabinet of the United States|U.S. Cabinet as the Secretary of Mutant Affairs, Beast is a brilliant scientist and statesman.  He is covered in blue fur and has heightened strength and agility, as well as pointed fangs and a lion-like roar. Grammers make-up took three hours to apply, it involved applying latex prosthetics before painting his eye area and lips blue, applying various hair pieces and wigs, and a muscle suit covered with a hand·punched fur suit. 
 Scott Summers / Cyclops
:The X-Mens field leader, Cyclops emits powerful energy blasts from his eyes, and must wear specially made glasses to prevent the destruction of anything he looks at. Although he is in a committed relationship with Jean Grey, her Phoenix persona kills him early in the film. Marsden saw no problem in having a smaller role, as the films opted to feature Wolverine as the standpoint character, and feeling that "its difficult when you have however many new characters that youre trying to introduce to an audience in 90 to 120 minutes, to give everyone their due." 
 Raven Darkholme / Mystique
:Magnetos blue-skinned right-hand woman possesses the ability to Shapeshifting|shape-shift to mimic anyones appearance, as well as fight with incredible agility and strength. She jumps in front of cure darts intended for Magneto and, after she loses her mutant abilities as a result, Magneto abandons her. Romijn described this story as "a traumatic experience" for Mystique, given that the previous movies implied that she and Magneto had "a deep-seated bond", and becoming "a frail mortal would be her worst nightmare". 
 Bobby Drake / Iceman
:A young mutant, Iceman can create constructs of ice or blasts of cold. Ashmores commitments to X-Men made him decline Bryan Singers invitation to play Jimmy Olsen in Superman Returns. The actor was content with his bigger role after Bobby joined the X-Men main team in X2, as during the predecessors production he wondered  "When do I get to freeze something or get into a fight?" 
 John Allerdyce / Pyro The Hills Moroccan desert, while Pyro was nowhere as physically demanding - "My characters pretty much stand-and-deliver, stand there and throw fire at people. Theres no acrobatics." 
 Cain Marko / Juggernaut
:A criminal recruited by the Brotherhood in a prison truck, Juggernaut is incredibly strong, fast and, once he gains momentum, he is nearly unstoppable. Matthew Vaughn cast Jones, who he met producing the Guy Ritchie gangster movies where Jones begun his acting career.  The actor had to go through a four-hour make-up process to portray Juggernaut, which included a muscle suit and a prostethic chin. "Make-Up Chair Confessions". X-Men: The Last Stand DVD, 2006, 20th Century Fox Home Entertainment  The costume tried to retain the bullet-shaped helmet of the comics without going excessively over the top. 
 Charles Xavier / Professor X telepathic powers, genetic mutation Eleventh Hour, but eventually Brett Ratner called to introduce himself as the new director.   
 Kitty Pryde / Shadowcat Hard Candy. The actress initially declined, not wanting to yet jump to Hollywood filmmaking, but accepted after reading the script.  Page said part of her motivation was having a new experience: "I thought, well, when else am I going to have a chance to wear a leather suit and run through exploding things? Why not be a superhero for a change?" 
 Warren Worthington III / Angel
:The mutant son of an industrialist, who has feathered wings which allow him to fly. He is played as a child by Cayden Boyd. The static wings were models with a   wingspan and   height glued to Fosters back, replaced with computer-generated ones when movement was required. 
 Callisto
:The leader of The Omegas, Callisto is a mutant with enhanced superhumanly acute senses, who senses mutants and their powers, and possesses superhuman speed. The character combined the powers of the comics Callisto with another of the Morlocks (comics)|Morlocks, Caliban (comics)|Caliban, and was written as someone who could be "beautiful, but with a tough persona".  Ramirez had originally auditioned to play the mutant prostitute Stacy X, and impressed Brett Ratner so much the director decided to bring her to play Callisto. 
 Peter Rasputin / Colossus
:A mutant with the ability to transform his skin into an organic steel, Colossus powers grant him superhuman strength and a resistance to physical damage and extreme temperatures. Cudmore wore a foam latex muscle suit covered with a chrome·plated plastic plus a hard plastic head to have the metal skin on the set, with some digital augmentation being used to enhance the facial expressions. A digital double was used only for stunts that could not be achieved practically, such as the Fastball Special where Colossus throws Wolverine. 
 Michael Murphy as Warren Worthington II
:The head of Worthington Labs, the corporation developing the "cure", Worthington expects to rid his son of his mutant abilities. The addition of the character allowed to integrate Angel into the cure plot,  which also added a parallel between Warrens discovery of his sons mutation with a father finding out about his sons homosexuality. 

* Shohreh Aghdashloo as Dr. Kavita Rao
:A scientist who works at Worthington Labs on the mutant cure, she is killed by Kid Omega. Aghdashloo signed without a completed script, and erroneously said her character would be mutant doctor Cecilia Reyes. 

* Josef Sommer as the President of the United States
:The President of the United States is tolerant of mutants, but fearful of the Brotherhoods threats. While creating the role, the producers felt that  a "different" president, like an African American or a woman, had become a cliché in itself and went for a traditional route with an elder man.  Sommer was invited by Ratner following their collaboration in The Family Man. 
 James Madrox / Multiple Man
:A mutant and thief recruited by the Brotherhood in a prison truck, Madrox has the ability to create a very large number of copies of himself. The writers considered Danes performance memorable despite being featured in only two scenes.  Madroxs wardrobe invoked the symbols worn in his comics costume.   
 
* Bill Duke as Secretary Trask 
:The head of the  . 
 Jimmy / Mark Helfrich, Spike (Lance cameos at Jubilee respectively, and three identical girls in the background in one scene are a reference to the Stepford Cuckoos. X-Men co-creator Stan Lee and writer Chris Claremont have cameos in the films opening scene as the neighbors of young Jean Grey.       The sergeant directing defensive preparations before the Brotherhood assaults Alcatraz Island is played by R. Lee Ermey. 

==Production==
{{multiple image
| footer    =  , was Foxs first choice to replace Bryan Singer. Once he left, Brett Ratner (right) took over directing The Last Stand.
|align=right

| image1    = Matthew Vaughn.JPG
| alt1      = Matthew Vaughn on the red carpet.
|width1=90

| image2    = Brett Ratner 2012 Shankbone.JPG
| alt2      = Brett Ratner at a convention.
|width2=103
}}

===Development===
 . 
 Wonder Woman Rob Bowman  and Alex Proyas  were also rumored to be up for consideration, though Proyas personally turned it down, citing feuds with 20th Century Fox president Thomas Rothman while producing I, Robot (film)|I, Robot.  Zack Snyder was also approached, but he was already committed to 300 (film)|300.  In February 2005, with still no director hired, Fox announced a May 5, 2006, release date, with filming to start in July 2005 in Vancouver.    One month later, the studio signed Matthew Vaughn to direct, and pushed the release date three weeks to May 26, Memorial Day weekend.  Vaughn cast Kelsey Grammer as Beast, Dania Ramirez as Callisto, and Vinnie Jones as Juggernaut, but family issues led him to withdraw before filming began.   Vaughn was also cautious of the tight deadlines imposed by Fox, stating that he "didnt have the time to make the movie that I wanted to make".   

 .    With a limited knowledge of the X-Men mythos, Ratner trusted his writers on doing something faithful to the comics, having the script drawing all of its scenes from the original Marvel publications. 

===Writing=== Fantastic Four and Elektra (2005 film)|Elektra,  was hired as writer for X-Men 3 in August 2004. X2 co-writer Zak Penn was separately working on his own draft, and the two joined forces for a combined screenplay in January 2005. Kinberg wanted "The Dark Phoenix Saga" to be the emotional plot of the film, while "Gifted (comics)|Gifted" would serve as the political focus.    The duo had seven months to complete The Last Stands script, and during the first week of work completed the first eighty pages, consisting of the first two-thirds of the plot. This incomplete draft was leaked to Aint It Cool News, who proceeded to do a negative review.  

The writers had to fight Foxs executives to retain the Phoenix plot, as the studio only wanted the cure story as it provided a reason for Magnetos conflict with the X-Men. Still the disputes made them not add much for Jean Grey to do in most of the films second half, as the executives considered the tone of the Phoenix story too dark for a mainstream summer movie, and that its appeal would be limited to hardcore fans rather than a general audience.  Penn defended the divergences from the original Dark Phoenix stories, stating that the Phoenix was not a  , as Fox felt the script called for a dramatic turning point. Kinberg and Penn were originally cautious, but grew to like the idea of killing off Xavier. They decided to write a post-credits scene suggesting the characters return for a sequel.   

As the studio was simultaneously developing  , limitations were set on which mutants could be used for cameo appearances in X-Men 3 in an attempt to avoid risking character development for Wolverine.   .  The introdutory scenes tried to emulate the Auschwitz opener for the first film, going with different scenes that resonated later in the plot instead of an action scene like in most blockbuster (entertainment)|blockbusters. Afterwards came a scene in the Danger Room, which was considered for the previous X-Men films but never included for budget and writing concerns. The writers tried to make the simulation not feel extraneous by showcasing some of the character conflicts and abilities in a "Days of Future Past"-inspired battle with a Sentinel. Another repurposed scene was Magneto attacking the convoy to free Mystique, Madrox and Juggernaut, which Penn had previously envisioned for X2. 
 Planet of the Apes and X2 itself, had their ending in Washington, and the Golden Gate sequence "would be the biggest sequence in my entire career", and suggested to instead put the Worthington laboratory in Alcatraz, along with "creating a face for the cure", which became the character of Jimmy/Leech. Kinberg agreed, as he previously argued with Penn about "blowing so many things early in the movie".       

===Filming=== Julia Wong with effects-heavy footage.   
 call sheets did not reveal all the characters, and many scenes were shot in varied ways.   Both of the ending scenes were not included on the shooting script, with Ratner taking a small crew during one days lunch time to film the post-credits scene with Xavier, and later going to London to film Magneto in the park.  

===Visual effects=== CG when we had to." For instance, complex wirework rigs were employed  which enabled the actors to do some stunts without resorting to digital doubles,    including a computer controlled flying rig from Cirque Du Soleil for Angels flight,  and one for Halle Berrys flying spins.  

Bruno estimates one-sixth of the effects budget was spent on the Golden Gate Bridge scene, which employed both a miniature of the bridge and computer graphics.    The effects team had to work without reference footage due to the city of San Francisco vetting any filming in the actual bridge, including aerial shooting as the area has restrictions on flying helicopters.  Framestore had further challenges in matching the varied weather conditions across the films plates. As compositing supervisor Matt Twyford detailed, "the elements consisted of cold, rainy night live-action footage from Vancouver, sunny day miniature elements, traditional misty day background plates of San Francisco, and of course the CG bridge and fx elements."  Another miniature was for the Grey home, which had a destructable equivalent matched the Canadian location and also had a digital equivalent.  A notable effect was the "digital skin-grafting", which rejuvenated the faces of senior actors Patrick Stewart and Ian McKellen, made by the Brothers Strauses Lola Visual Effects.  Bruno made sure to ask the atomization made by Phoenix was not too vivid and gruesome, instead resembling oatmeal. 

===Music===
 
 Requiem Mass for the choir parts.  A soundtrack album was released on May 23, 2006. 

==Marketing== King Kong Diamond Select Fox Broadcasting two weeks before the films theatrical release.  

 s tie-in video game,  , doing the script along with screenwriter Zak Penn. The games story bridges the events between X2 and The Last Stand,   featuring Wolverine, Iceman and Nightcrawler as playable characters, voiced by their film portrayers Hugh Jackman, Shawn Ashmore, and Alan Cumming. Patrick Stewart also appears as Professor X.  The game was released to negative reviews and eventually underperformed commercially. 

==Release and reception==
  for an advance screening.|alt=Hugh Jackman, Halle Berry and Kelsey Grammer hold a flag with the X-Men: The Last Stand logo and the inscription "We Salute Our Troops" in a ships deck.]]
 USS Kearsarge (LHD-3), as the ship was en route to New York City for Fleet Week.  X-Men: The Last Stand was released in the United States on May 26, 2006, in 3,690 theaters,   while also opening in 95 international markets that same weekend. 

===Critical response===
X-Men: The Last Stand received mixed reviews from critics. At Rotten Tomatoes, the film has a score of 58%, based on 231 reviews, with an average rating of 5.9/10. The sites consensus reads, "Director Brett Ratner has replaced the heart and emotion (and character development) of the previous X-Men films with more action and explosions. The film should still provide ample entertainment, but viewers may truly wish this to be the Last Stand."  At Metacritic, it has a score of 58 out of 100, based on 38 critics, indicating "mixed or average reviews". 
 Ebert and Roeper gave the film a "two thumbs up" rating, with Roger Ebert saying, "I liked the action, I liked the absurdity, I liked the incongruous use and misuse of mutant powers, and I especially liked the way it introduces all of those political issues and lets them fight it out with the special effects."  Stephanie Zacharek of Salon.com gave it a mixed review, noting that it was "only half a mess", and that Ratner "could have stuck a bit more closely to the Dark Phoenix narrative than he did." However, Zacharek did note that that third act captured some of the original storys "majesty".  
Famke Janssens performance was praised by critics and audiences. Also impressed with Janssens performance was Total Film, who said, "Playing the super-freaky mind-control goddess like GoldenEye’s Xenia Onatopp’s all-powerful psycho sister, her scenes&nbsp;– particularly that one with the house&nbsp;– crackle with energy and tragedy. If only the rest of X3 had followed suit." 

Justin Chang of  , Peter Travers of Rolling Stone said, "Last stand? My ass. Billed as the climax of a trilogy, the third and weakest chapter in the X-Men series is a blatant attempt to prove there is still life in the franchise. And there is: just enough to pull a Star Trek and spawn a Next Generation saga." 

Writer Simon Kinberg would later state that "there are a lot of things about X3 that I love and there are a lot of things that I regret", detailing that he would have preferred the Dark Phoenix as the main plotline and "I would have fought harder" for that, considering that at the period "the darkness of her story was a little bit daunting on a huge $200 million studio movie" leading Fox to ask for rewrites.    Previous X-Men director Bryan Singer declared that The Last Stand "isnt what I would have done" and he was dissatisfied with the busy plot and excessive character deaths, but Singer still liked some parts of the movie, such as Ellen Pages casting - leading Singer to bring her back as Kitty Pride in   - and the scenes with Leech, which he described as "really sweet moments".  Matthew Vaughn, who was attached as director before dropping out, criticized Ratners direction: "I could have done something with far more emotion and heart. Im probably going to be told off for saying that, but I genuinely believe it."  While promoting his own installment of the franchise, 2011s X-Men: First Class, Vaughn would say regarding The Last Stand that “I storyboarded the whole bloody film, did the script. My X3 would have been 40 minutes longer. They didnt let the emotions and the drama play in that film. It became wall-to-wall noise and drama. I would have let it breathe and given far more dramatic elements to it." 

===Box office=== The Da Vinci Code, which retained the top spot in most markets, and beat The Last Stand in international gross that weekend with $91 million.     The films second weekend dropped 67 percent to $34 million, which was the steepest post-Memorial Day opening drop on record.  X-Men: The Last Stand eventually grossed $234,362,462 in the domestic box office and $224,997,093 internationally, for a worldwide total of $459,359,555,    the fourth-highest in domestic grosses    and seventh-highest worldwide for 2006.  X-Men: The Last Stand was also the highest-grossing film in the franchise, until it was surpassed by X-Men: Days of Future Past eight years later. 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- 12th Empire Empire Awards   Best Sci-Fi / Fantasy
|
| 
|- Scene Of The Year The Phoenix and Professor X showdown
| 
|-
| Costume Designers Guild Awards 
| Excellence in Costume Design for Film – Fantasy
| Judianna Makovsky
|  
|- The 4th Irish Film & Television Award  Best International Actor Ian McKellen
| 
|- 33rd Peoples Choice Awards|Peoples Choice Award  Favorite Movie Drama
|
| 
|- Favorite Movie
|
| 
|- Favorite Female Action Star Halle Berry
| 
|- Satellite Award    Satellite Award Best Editing Mark Helfrich Mark Helfrich, Julia Wong
| 
|- Saturn Award  Saturn Award Best Supporting Actress Famke Janssen
| 
|- Best Science Fiction Film
|
| 
|- Best Supporting Actor Kelsey Grammer
| 
|- Best Music John Powell John Powell
| 
|- Best Costume Judianna Makovsky 
| 
|- Best Special Effects John Bruno, Eric Saindon, Craig Lyn 
| 
|- Teen Choice Teen Choice Award    Teen Choice Best Choice Sleazebag Ian McKellen
| 
|- Best Choice Liplock Hugh Jackman and Famke Janssen
| 
|-
| 
| 
| 
|- Young Artist Awarda  Best Supporting Young Actor in a Feature Film Cameron Bright
| 
|}

==Home media== Easter eggs. The two-disc edition came with a 100-page commemorative comic book with a new story written by X-Men co-creator Stan Lee, his first original Marvel comic book in five years.    The DVD sold 2.6 million units in its first day, exceeding Foxs expectations,  and sold a total 5 million in its first week.  A Blu-ray edition of the film was issued in November 2006. 

==Sequels== The Wolverine, released on July 26, 2013. A stand-alone sequel,  The Wolverine  shows Logan heading for Japan to escape the memories of what occurred during The Last Stand. Hugh Jackman and Famke Janssen reprised their roles, while Ian McKellen and Patrick Stewart appear in a post-credits scene|mid-credits scene. 
 sent back into his 1973 self so he could guide the past Xavier and Magneto into preventing the catastrophe. The events of the film end up applying retroactive continuity to The Last Stand, with the ending set in a newer timeline where Jean and Cyclops are still alive.  

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
}}

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 