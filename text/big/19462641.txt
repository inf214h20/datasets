The Murder of Fred Hampton
{{Infobox Film
| name           = The Murder of Fred Hampton
| image          = DVD cover of the movie The Murder of Fred Hampton.jpg
| caption        = 
| director       = Howard Alk
| producer       = Mike Gray
| writer         = 
| narrator       = 
| starring       = Fred Hampton  Rennie Davis  Edward Hanrahan Bobby Rush
| music          = 
| cinematography = Howard Alk Mike Gray
| editing        = Howard Alk John Mason
| distributor    = Facets Multi-Media Chicago Film Group, MGA Inc
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1971 documentary film which began with the intention of portraying Fred Hampton and the Illinois Black Panther Party. During the films production, Hampton was killed by the Chicago Police Department. 

== Plot ==
 
The documentary is split into two parts: a portrait of Fred Hampton and an investigative report in his death.  Through re-enactments, evidence from the scene, and interviews, the documentary alleges that Hamptons death was murder by the Chicago police.
 

== Cast ==
* Fred Hampton
* Rennie Davis
* Edward Hanrahan
* Don Matuson
* Bobby Rush

== Release ==
The film was released in Chicago, Illinois in May 1971, but it failed to attract much attention.  However, it had a successful festival run in Europe and opened in New York in October 1971.   

== Reception ==
In a retrospective, Roger Ebert called it "less compelling as investigative journalism than as an archive of political vernacular."   A. H. Weiler of The New York Times called it "a disturbingly somber illustration of some of the ills that beset us and our social system."   Spencer Parsons of the Austin Chronicle wrote that the films coverage of Hampton is riveting and does not shy away from controversy.   Noel Murray of The A.V. Club rated it B+ and called it an immersive experience and "more satisfying portrait of activism" than American Revolution 2 (1969).   David Walker of DVD Talk rated it 4.5/5 stars and wrote, "As a documentary, The Murder of Fred Hampton serves as a lasting memorial to Hamptons great legacy and tragic killing. Equally important, the film is an example of the power of independent media in providing the truth, when much of the mainstream media simply chooses to recycle the information they are given without digging beneath the surface." 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 
 