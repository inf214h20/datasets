Aftermath: The Remnants of War
{{Infobox film
| name           = Aftermath: The Remnants of War
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Daniel Sekulich
| producer       = Ed Barreveld Michael Kot Peter Starr
| writer         = Allen Abel Daniel Sekulich John Jarvis
| starring       =
| music          =
| cinematography = Michael Grippo
| editing        = Deborah Palloway
| studio         = National Film Board of Canada
| distributor    =
| released       = 2001
| runtime        = 73 min 37 s 
| country        = Canada
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Aftermath: The Remnants of War is a 2001 Canadian documentary film directed by Daniel Sekulich about the painful legacy of war, based on the Lionel Gelber Prize winning book of the same name by Donovan Webster. The film is co-written by Sekulich and Allen Abel, and co-produced by the National Film Board of Canada and Aftermath Pictures.
 trauma of military conflict which remain after the fighting stops and the troops go home. The program features interviews with individuals involved with the reparation of the residual devastation - people who destroy unexploded munitions at Verdun and in Sarajevo, recover and identify skeletons of battlefield casualties at Stalingrad, and help victims of Agent Orange in the Aluoi Valley, Vietnam. Archival footage sets each segment in its historical context.
 dioxin used in the Vietnam War. 

==Reception==
Aftermath: The Remnants of War garnered multiple awards at film festivals around the world, including a Gold Medal for Best International Affairs Documentary at the New York Festivals Television Competition and a Gold Special Jury Award at the WorldFest-Houston International Film Festival.   

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 


 
 