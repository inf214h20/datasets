The Cove (film)
 
{{Infobox film
| name           = The Cove
| image          = The Cove 2009 promo image.jpg
| image_size     = 250px
| alt            =
| caption        = Canadian free-diving world champion Mandy-Rae Cruickshank swimming with dolphins in a photograph used for the films movie poster 
| director       = Louie Psihoyos
| producer       = Fisher Stevens Paula DuPre Pesmen
| writer         = Mark Monroe
| starring       = Ric OBarry
| music          = J. Ralph
| cinematography = Brook Aitken
| editing        = Geoffrey Richman
| studio         = Participant Media Lionsgate Roadside Attractions
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English Japanese
| budget         =
| gross          = $1,140,043
| Notable Awards         = Oscar
}} Academy Award Best Documentary Feature in 2010. The film is a call to action to halt mass dolphin kills, change Japanese fishing practices, and to inform and educate the public about the risks, and increasing hazard, of mercury poisoning from dolphin meat. The film is told from an ocean conservationists point of view.   The film highlights the fact that the number of dolphins killed in the Taiji dolphin drive hunting is several times greater than the number of whales killed in the Antarctic, and claims that 23,000 dolphins and porpoises are killed in Japan every year by the Whaling in Japan|countrys whaling industry. The migrating dolphins are herded into a cove where they are netted and killed by means of spears and knives over the side of small fishing boats.  The film argues that dolphin hunting as practiced in Japan is unnecessary and cruel.

Since the films release, The Cove has drawn controversy over neutrality, secret filming, and its portrayal of the Japanese people.

The film was directed by former  . (January 27, 2009). Retrieved on January 27, 2009.  
 25th annual Sundance Film Festival in January 2009. It was selected out of the 879 submissions in the category.  

==Synopsis== hit television series of the same name. The show, very popular, fueled widespread public adoration of dolphins, influencing the development of marine parks that included dolphins in their attractions. After one of the dolphins, in OBarrys opinion, committed a form of suicide in his arms by closing her blowhole voluntarily in order to suffocate, OBarry came to see the dolphins captivity and the dolphin capture industry as a curse, not a blessing. Days later, he was arrested off the island of Bimini, attempting to cut a hole in the sea pen in order to set free a captured dolphin.  Since then, according to the film, OBarry has dedicated himself full-time as an advocate on behalf of dolphins around the world.
 mercury and interviews two local politicians, Taiji City Councilmen, who have, for that reason, advocated the removal of dolphin meat from local school lunches.

Attempts to view or film the dolphin killing in the cove are physically blocked by local police and the Japanese local government who treat the visitors with open intimidation, derision, and anger. Foreigners who come to Taiji, including The Coves film crew, are shadowed and questioned by local police. In response, together with the Oceanic Preservation Society, Psihoyos, OBarry, and the crew utilize special tactics and technology to covertly film what is taking place in the cove.  The film also reports on Japans alleged "buying" of votes of poor nations in the International Whaling Commission. The film indicates that while Dominica has withdrawn from the IWC, Japan has recruited the following nations to its whaling agenda: Cambodia, Ecuador, Eritrea, Guinea-Bissau, Kiribati, Laos, and the Republic of the Marshall Islands. This is not entirely accurate, however, as Ecuador has been a strong opponent of whaling. {{cite web|url= http://www.mmrree.gob.ec/2011/bol147.asp|title=Ecuador ECUADOR PROPONE PONER FIN A LA "CAZA CIENTÍFICA" QUE JAPÓN REALIZARÁ EN SANTUARIO AUSTRAL
|publisher=Ministerio de Relaciones Exteriores del Ecuador|date=February 17, 2011 |accessdate=September 21, 2010}}   At the end of the film, OBarry marches into a meeting of the Commission carrying a TV showing footage of the Taiji dolphin slaughter. OBarry walks around the crowded meeting room displaying the images until he is escorted from the room.

Geographical location of some of the filming and killing of the dolphins is here:  

==Cast==
 
 
* Ric OBarry Scott Baker 
* Hayden Panettiere
* Joe Chisholm
* Mandy-Rae Cruickshank
* Charles Hambleton
* Simon Hutchins
*Hardy Jones
* Kirk Krack
* Isabel Lucas
* Roger Payne
* John Potter
* Louie Psihoyos
* Dave Rastovich
* Paul Watson
 

==Production==
In the film, Ric OBarry states, "Today they would kill me, if they could. And Im not exaggerating, if these fisherman could catch me and kill me, they would."  The film shows KernerFXs (previously part of Industrial Light & Magic|ILM) contribution of specialized camouflaged high-definition cameras that were designed to look like rocks. These hidden cameras helped capture footage and were so well camouflaged that, according to director Louie Psihoyos, the crew had a hard time finding them again. 

==Reception==
===Japanese media===
Some media in Japan have questioned whether one scene was manufactured for the camera, discussed whether the movie should properly be called a documentary and sought to discredit it. 『クローズアップ現代 問われる表現 イルカ漁映画』NHK2010年7月6日   Louie Psihoyos, the documentary’s director, argues that such allegations are fabricated to protect the local whaling industry and that none of the scenes in the film were staged. 

An NHK TV program alleged that various techniques were used by anti-hunt activists in the film to irritate local people by saying nasty words both in Japanese and English, and then using violence or aggressive tactics with local fishermen until local police arrived. NHK concluded that the activists did so in order to capture angry and wild expressions by the local fishermen in the film and in photos. 

===Critics=== normalized rating average score of 82, based on 26 reviews.   
 Gayatri Spivak, Minister of Agriculture and Fisheries, said "it is regrettable that this movie is made as a message that brutal Japanese are killing cute dolphins".  According to Michelle Orange of Movie Line "How much of this (The Cove) should we believe? As a piece of propaganda, The Cove is brilliant; as a story of ingenuity and triumph over what seems like senseless brutality, it is exceptionally well-told; but as a conscientious overview of a complex and deeply fraught, layered issue, it invokes the same phrase as even the most well-intentioned, impassioned activist docs: Buyer beware."  There has been some controversy over the depiction of the Japanese people in the film. However, upon questioning, director Louie Psihoyos said of his sympathy for the Japanese people, many of whom are unaware of the situation at the cove, "To me, its a love letter. Im giving you the information your government wont give you." 

===Reactions in Western Australia=== sister city relationship with the Japanese whaling port town of Taiji, as long as the latter continues its dolphin slaughter.    The decision was reversed in October 2009. 

===Reactions in Taiji, Japan===
The whale and dolphin hunting season in Japan usually begins on September 1 each year; in 2009, the hunting began on September 9. Although activists tend to believe that it was because of the publicity generated by the film,  it has been reported that the delay was due to the weather and rough seas.  According to campaigners, out of the 100 dolphins captured on September 9, some were taken to be sold to marine museums and the rest were released, while 50 pilot whales were killed and sold for meat on the same day. While campaigners claim that it has become apparent that The Cove is having an impact on the way in which Japanese fisherman normally conduct the dolphin hunt,   Associated Press. September 15, 2009.  on March 23, 2010 the Japanese government stated "The dolphin hunting is a part of traditional fishery of this country and it has been lawfully carried out."  

Upon the film winning the Oscar, the town mayor of Taiji and the chief of Taiji Fishery Union said "The hunt is performed legally and properly with the permission of Wakayama Prefecture  ."   Several people who appear in the film, including Taiji assemblyman Hisato Ryono and Tetsuya Endo, an associate professor at Health Sciences University of Hokkaido, say that they were lied to by the documentarys producers about what the film would contain and Endo has pressed charges against the Japanese film publisher. 

Since the release of the film, a much larger number of activists, mainly non-Japanese, have visited Taiji to protest or film the dolphin hunts. The Taiji fishermen responded by constructing an elaborate structure of tarps to better conceal the drive-hunting activities in and around the cove. 

===Release in Japan===
The film was initially screened only at two small venues in Japan: at the  , March 9, 2010, p. 1. ( .)   , April 23, 2010, p. 2. 

As of June 2010, the controversy over the film and the films subject had received little press attention in Japanese-language media in Japan.  Boyd Harnell of the Japan Times stated on May 23, 2010, that Japanese news editors had told him that the topic was "too sensitive" for them to cover. Harnell, Boyd, "Experts fear Taiji mercury tests are fatally flawed", Japan Times, May 23, 2010, p. 12. 

In April 2010, Colonel Frank Eppich, the United States Air Force commander of Yokota Air Base, located near Tokyo, banned screenings of the film at the base theater. A base spokesman said that The Cove was banned because using a base venue to display the film could be seen as an endorsement of the film. The spokesman added, "We have a lot of issues with Japan ... and anything done on an American base would be seen as an approval of that event."   In response, Louie Psihoyos said that he would give away 100 DVD copies of the film for free to Yokota base personnel. 
 Shibuya was harassed by right-wing protesters. Unplugged stated that it was in negotiations with other theaters to screen the film.   Another theater in Tokyo and one in Osaka subsequently declined to screen the film. In response, a group of 61 media figures, including journalist Akihiro Ōtani and filmmaker Yoichi Sai, released a statement expressing concern over the threat to freedom of speech by the intimidation of right-wing groups.  The Directors Guild of Japan also asked theaters not to stop showing the film, arguing that "such moves would limit opportunities to express thoughts and beliefs, which are the core of democracy." 

On June 9, 2010, Tsukuru Publishing Co. sponsored a screening of the film and panel discussion at Nakano Zero theater in Nakano, Tokyo. The panelists included five who had signed the statement above. Afterwards, panel member Kunio Suzuki, former head of Issuikai, an Uyoku dantai (rightist) group, condemned the right-wingers threats against theaters and urged that the film be shown. "Not letting people watch the movie is anti-Japanese," said Suzuki. 

In response to the cancellation of screenings of the film in Japan, Japanese video sharing site Nico Nico Douga screened the film for free on June 18, 2010. The same week, Ric OBarry was invited to speak at several universities in Japan about the film. OBarry stated that he was planning on bringing several Hollywood stars to Taiji in September 2010 in an attempt to halt that years hunt. 
 Hachinohe began screening the film. Uyoku dantai|Right-wing nationalists protested outside four of the theaters, but close police supervision prevented any disruption to the viewing schedules and ensured free access for viewers to the theaters.  The two in Tokyo and Yokohama were successful in obtaining prior court injunctions prohibiting protests outside their venues. 

A local Taiji activist group, called People Concerned for the Ocean, announced that they would distribute DVDs of the film, dubbed in Japanese, to all 3,500 residents of Taiji.  The DVDs were to be distributed to the residents on March 5–6, 2011. 

The assistant chief of the whaling division at Japans Fisheries Agency Hideki Moronuki is portrayed as having been fired in the movie..  Close-up Gendai, a Japanese social affairs television program, showed a video-conference in English with Psihoyos and asked how he came to think Moronuki was fired. Psihoyos stated that he met Akira Nakamae, the Deputy Minister of Fisheries, on an airplane going to the 2008 IWC meeting in Santiago  and was told then, but Nakamae denied ever having such a meeting. 

===Lawsuit over alleged inaccuracies===
Tetsuya Endō, an associate professor of the Health Sciences University of Hokkaido who is shown in the film discussing the high mercury content of dolphin meat, is suing the Japanese rights-holder, Medallion Media, and the distributor, Unplugged, for ¥11 million over what Endō said were misleading edits of his comments in the film which have damaged his reputation. In reference to the scene in which Endo was holding in his hand the meat of a dolphin from Taiji, Endo argued that the director arbitrarily inserted into the scene a caption saying mercury was detected in the meat, even though he was explaining about another dolphin. The director, who is a photographer and founder of a marine life conservation group, alleges in the film that a large quantity of mercury is contained in dolphin meat.The litigation opened in Tokyo District Court on December 1, 2010. 

===Reactions in Seaworld===
SeaWorld spokesperson Fred Jacobs has responded by saying that, "We think were being unfairly criticized for something were opposed to." {{Citation
  | last = Mieszkowski
  | first = Katharine
  | title = Dolphins Are Dying to Amuse Us
  | newspaper = Salon
  | date = August 7, 2009
  | url = http://www.salon.com/news/environment/feature/2009/08/07/the_cove_dolphins
  | accessdate = June 7, 2011}}
  He adds that, "SeaWorld opposes the dolphin hunts documented in The Cove. We do not purchase any animals from these hunts. More than 80 percent of the marine mammals in our care were born in our parks. We havent collected a dolphin from the wild in decades." {{Citation
  | title = The Coves Shocking Discovery
  | newspaper = The Oprah Winfrey Show
  | date = April 22, 2010
  | url = http://www.oprah.com/oprahshow/Filmmakers-Reveal-Dolphin-Slaughter-in-The-Cove/5
  | accessdate = June 7, 2011}}
  However, Jacobs does not condemn those who purchase from the Taiji dolphin hunt. {{Citation
  | last = Alexander
  | first = Brian
  | title = Dolphin hunt film sparks dilemma for tourists
  | publisher = msnbc.msn.com
  | date = August 6, 2009
  | url = http://www.msnbc.msn.com/id/32274599/ns/travel-news/t/dolphin-hunt-film-sparks-dilemma-tourists/
  | accessdate = January 9, 2012}}
 
OBarry has thus been criticized for emphasizing that dolphinariums are a large contributing factor to the economic success of the dolphin slaughter in Taiji and for encouraging boycotts of dolphin shows to protest the dolphin slaughter. The scene in The Cove that displays a map consisting of arrows emanating from Taiji and pointing to countries with dolphinariums has been said to be misleading since the majority of those countries do not currently have dolphins of Japanese origin. In the United States it is currently illegal to import dolphins obtained from a drive, including the drive hunt at Taiji, as it is considered an inhumane method.  Since 1993 there have been no permits issued to facilities in the United States to import dolphins acquired through drive hunt methods. {{cite web
  | last = Rose
  | first = Naomi A.
  | coauthors = E.C.M. Parsons, and Richard Farinato
  | title = The Case Against Marine Mammals in Captivity
  | publisher = The Humane Society of the United States and the World Society for the Protection of Animals
  | year = 2009
  | url = http://www.wspa-international.org/Images/159_the_case_against_marine_mammals_in_captivity_english_2009_tcm25-8409.pdf
  | accessdate = June 7, 2011}}  Marilee Menard, the executive director of the Alliance of Marine Mammal Parks and Aquariums, has also stated that she believes that the filmmakers are "misrepresenting that the majority of zoos and aquariums with dolphins around the world are taking these animals." 

==Noteworthy achievements==
The Cove won over 25 film awards. Some notable awards include "Best Documentary" from the Environmental Media Awards,  Three Cinema Eye Honors  for "Outstanding Achievement", and the Academy Award for Best Documentary Feature on the 82nd Annual Academy Awards. 

==Awards and nominations==
The Cove has been nominated for or received numerous awards, including the following:
*   cameras abruptly cut away to the crowd when OBarry raised a banner urging the audience to "Text DOLPHIN to 44144".   March 7, 2010    TV Guide labeled the moment as "Fastest Cutaway",  and film critic Sean Means wrote it showed that the Oscar ceremony was "studiously devoid of genuine excitement". 
* Genesis Awards (2010) - Best Documentary Feature (won) 62nd Writers Guild Awards (2009) – Best Documentary Feature Screenplay (February 20, 2010) Directors Guild Awards (2009) – Outstanding Directorial Achievement in Documentary, Directors Guild of America (January 31, 2010)  National Board of Review – Best Documentary,  (December 3, 2009)  15th BFCA Critics Choice Awards (2009) – Best Documentary Feature, Critics Choice Awards in Los Angeles (January 15, 2010) 
* Los Angeles Film Critics Association – Best Documentary  Toronto Film Critics Association Awards (2009) – Allan King Documentary Award (December 16, 2009)    Toronto Film Critics Association Awards (2009) – Best Documentary Feature (December 16, 2009) 
* Newport Beach Film Festival (2009) – Audience Award for Best Documentary 
* New York Film Critics Online (NYFCO) – Best Documentary (December 13, 2009) 
* Sheffield Doc/Fest (2009) – The Sheffield Green Award (November 8, 2009) 
* Cinema Eye Honors (2009) – (Nominated) Outstanding Achievement In Original Music Score – J. Ralph (November 5, 2009) 

Traveling through film festivals and social events all around the United States, The Cove has also received the best documentary nod from many critics organizations, including The Boston Society of Film Critics,  San Diego Film Critics Society,  Dallas/Ft. Worth Film Critics Association,  Utah Film Critics Association,  Florida Film Critics Association,  Houston Film Critics Association,  and the Denver Film Critics Society.  As the film has received more and more recognition, the Oceanic Preservation Society translated their website into multiple languages to cater to interest from around the world. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
* Oscar-Winning Doc The Cove – video report by Democracy Now!
* The making of The Cove Director Louie Psihoyos technical interview on Momentum about the making of "The Cove"

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 