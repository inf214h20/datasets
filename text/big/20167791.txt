Doppio Petto
  Sicilian dialect. There are Italian subtitles for people who do not understand the sicilian dialect. It was filmed in 1997 and was releashed shortly after. It runs about 35 minutes long.

==Synopsis==
Pasqualino, on the day of his birthday, hears the voice of his father telling: "Now that you are thirteen and are a man, you finally can stay with the animals". And so it will be; from that day Pasqualino will spend weeks in the country to tend oxen and goats. The games with its friends along the roads of Burgio are a distant memory, and what he most suffers is the separation from 12-year-old Saro (his brother) and from grandfather Pasquale whose name he inherited. 
Saro, born a paraplegic, needs of a lot of cares and expenses that worsen a very difficult economical situation. 
Pasqualino seems to accept this destiny but in his heart hopes to emigrate and to build a worthier existence. The present condition that makes him a sort of guard (to the sister when she is with her fiancé, to the sick brother because and sick, to the animals) becomes unbearable, and he wishes to go away. 
He often meets with Vito, a young shepherd, and shares with him peaceful or awkward moments of his life in the countryside. One day Vito, for carelessness, loses a sheep, the owners infuriated hits the boy, who has a hemorrhage and dies. the body is thrown in a lake to escape justice simulating a death by accident. Pasqualino, eyewitness of the facts, is scared and plans to flee. Driven by this desire of escape he begins to think about the difficulty of the plan. 
First of all, he needs new clothing, different from the simple shepherd dress he wears. He remembers that his father had promised him a suit and he had chosen a "doppio petto" (double chest suit). Now this garment represents the freedom and he cannot wait to wear it. 
When the father discovers the boy and his brother playing with one of his old suits, he sends them back to the country for the next three months. Now work appears harder than ever because the dream of escape has almost vanished. But a surprise restores hope: Pasqualinos grandfather has persuaded the tailor to go up there, to measure on the incredulous boy his new double chest suit. It will be ready in two weeks. The moment of freedom is close, the last days seem never to go by, the time seems to have stopped. 
On the eve of his last work day, an event upsets forever the life of Pasqualino: a peasant warns him to return urgently home. He runs towards the house, meeting people that express by gestures a distressing message of death. When he arrives inside his house, he sees his brother Saro, dead, wearing a double-breasted suit.

== Cast ==
* Pasqualino: Giuseppe Piazza
* Saro: Saverio Miceli
* Paolo: Benedetto Baiamonte
* Luciano: Giuseppe Restivo
* Father: Calogero Latino
* Mother: Maria Concetta Aquilino
* Grandfather: Salvatore Musso Pantaleo

== Awards ==
* National Competition Fedic 1998
* World Competition Sole 1998

== External links ==
* 
*  Watch The Actual Movie

 
 