Christmas Mountain
{{Infobox film
| name           = Christmas Mountain
| image          = ChristmasMountain.jpg
| image_size     =
| caption        = 2008 DVD cover
| director       = Pierre de Moro Giafferi
| producer       = Laurette de Moro Giafferi Mark Miller Mark Miller Slim Pickens Barbara Stanger
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1981
| runtime        = 90 minutes
| country        =   English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1981 western Mark Miller of Savannah Smiles and Slim Pickens in one of his final roles, the film was originally produced and released in 1981. The original 16MM master was then lost for 20 years. Jack Evans, a partner and financier of the original film, eventually regained possession of the master. It was digitally remastered by Victory Studios of Los Angeles in 2008, and is now available on  DVD.

A New York Times review of the original release said, "This heartwarming Christmas tale contains a western twist as it tells the tale of a heavenly cowpoke who rides down to earth to ride herd on a few people in need of some miracles."

==Cast== Mark Miller as Gabe Sweet
*Slim Pickens as the cowboy angel
*Barbara Stanger as Teresa

==External links==
* 
*  

 
 
 
 
 
 


 