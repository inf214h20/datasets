Bad (2007 film)
{{Infobox film
| name           = Bad
| image          = 
| caption        = 
| director       = Vincenzo Giammanco
| producer       = Vincenzo Giammanco
| writer         = Vincenzo Giammanco
| starring       =Remy Thorne Jennifer Davis Nancy Cronig Kyle Dietz Kenlyn Kanouse
| music          =David Tichy
| cinematography = Ryan Moyer
| editing        = Michael Lynn Sturm
| distributor    = Chartwell School
| released       =  
| runtime        = 30 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bad, stylized as bAd, is a 2007 drama film directed and written by Vincenzo Giammanco about a young boy who is dyslexic.

John Reed (Remy Thorne) is in the fifth grade and is failing in school as "his teacher thinks that hes just too lazy to study, and the local bully takes every opportunity to humiliate him." After the intervention of his mother, she and John work to overcome the difficulties he faces due to dyslexia.

==See also==
*List of artistic depictions of dyslexia

==External links==
*  
*  

 
 
 
 


 