Chandrahasa (film)
{{Infobox film
| name           = Chandrahasa
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sagar Movietone
| writer         =  
| narrator       = 
| starring       = Noor Mohammed Charlie Gulzar Master Bachchu
| music          = S. P. Rane
| cinematography = Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1933
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1933 Hindi mythological costume drama film directed by Sarvottam Badami.    It was the first Hindi film directed by Badami bringing him into prominence, even though he did not know Hindi.    It was also one of the first Talkie versions of the film which was remade several times.    Produced by Sagar Movietone, it had music composed by S. P. Rane.  The film starred Noor Mohammed Charlie, Gulzar, Kamala, Master Bachchu, Dinkar, Mehboob Khan and Baburao Sansare. 

The film was not a comedy in spite of Charlie, a well-known comedian acting in this film, Badami went on to be recognised later for his satirical comedies.   

The story is about a young man Chandrahasa, whom an evil Minister is trying to kill. A princess helps Chandrahasa, falling in love with him in the process.   

==Plot==
An evil minister wants to kill the young boy Chandrahasa who escapes the minister’s killers. Several years later the minister finds Chandrahasa and sends him as a messenger to the king with a letter. The letter asks the king to poison the messenger. On the way a young princess finds him sleeping and reads the letter. She rewords the letter and changes Vish (poison) to Vishya, which is her name. The king on receiving the letter gets Chandrahasa married to the princess.

==Cast==
* Noor Mohammed Charlie
* Gulzar
* Master Bachchu
* Kamala
* Baburao Sansare
* Khatoon
*Mehboob Khan

==Remakes==
The Film has been made several times since 1921.    

* Chandrahasa (1921) directed by Kanjibhai Rathod
* Chandrahasa (1928) directed by Kanjibhai Rathod
* Chandrahasa (1929) directed by Dadasaheb Phalke
* Chandrahasa (1933) Hindi, directed by Sarvottam Badami
* Chandrahasan (1936) Tamil, directed by Prafulla Ghosh
* Chandrahasa (1941) Telugu, directed by M. L. Rangaiah
* Chandrahasa (1947) Hindi, directed by Gunjal
* Chandrahasa (1947) directed by S. Patil
* Chandrahasa (1965) Telugu, directed by B. S. Ranga

==Music==
The music direction was by S. P. Rane. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| aao Aao Tanik Mere Saath Chalo
|-
| 2
| Ahankaar Mein Man Kyun Biraaje
|-
| 3
| Dhyan Dharoon Jai Gori Mata
|-
| 4
| Ati Vyakul Hoon Tav Darshan Ko
|-
| 5
| Hey Tu Sumate Dhir Kyun Taje
|-
| 6
| In Nainon Ke Raste Leke Dil
|-
| 7
| Jai Shripati Jag Trata
|-
| 8
| Kaise Mere Durbhagya Yeh Nikas Chale More Pal Mein Pran
|-
| 9
| Jo Jo Re Taane Mere Ro Nahin So Ja Pyare
|-
| 10
| Nath Itna Hi Prem Main Pichhanoo
|-
| 11
| Raakhe Tujh Bin Kon Laaj Prabhu
|-
| 12
| Taras Rahein Hain Nain Daras Bin
|-
| 13
| Tu Amrut Bhar Antar Mein Sakhi
|-
| 14
| Tumhari Adh Khuli Aankhein Madhur Amrut Pilati Hain
|}

==References==
 

==External links==
* 

 

 
 
 