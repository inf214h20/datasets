The Face Reader
{{Infobox film
| name           = The Face Reader
| image          = The_Face_Reader_poster.jpg
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Gwansang
| mr             = Kwansang}}
| director       = Han Jae-rim 
| writer         = Kim Dong-hyuk
| producer       = Kim Woo-jae    Yu Jeong-hun   Ju Pil-ho
| starring       = Song Kang-ho Lee Jung-jae Baek Yoon-sik Jo Jung-suk Lee Jong-suk Kim Hye-soo 
| music          = Lee Byung-woo
| cinematography = Go Nak-seon
| editing        = Kim Chang-ju Showbox Mediaplex  (South Korea)  Pan Media and Entertainment  (United States)  Dreamwest Pictures  (United States) 
| released       =  
| runtime        = 139 minutes 
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =     . Box Office Mojo. Retrieved 2014-09-16. 
}} Grand Prince King Munjong.   

The Face Reader became one of the highest-grossing films in South Korea in 2013, with 9.1 million admissions. It won six awards at the 50th Grand Bell Awards, including Best Film, Best Director for Han Jae-rim, and Best Actor for Song Kang-ho.  

==Plot== King Munjong Grand Prince Suyang who yearns to become King himself by killing the young successor Danjong of Joseon|Danjong. Nae-gyeong decides to keep his loyalty to the late King and help Kim Jongseo protect the young King which forces him into the biggest power struggle in the history of the Joseon dynasty.

==Cast==
*Song Kang-ho as Nae-gyeong Grand Prince Suyang
*Baek Yoon-sik as Kim Jongseo 
*Jo Jung-suk as Paeng-heon, Nae-gyeongs brother-in-law and assistant
*Lee Jong-suk as Jin-hyeong, Nae-gyeongs son
*Kim Hye-soo as Yeon-hong
*Kim Eui-sung as Han Myung-hoi
*Jung Gyu-soo as Park Cheom-ji Danjong
*Lee Yoon-geon as Jo Sang-yong
*Lee Do-yeob as Kim Seung-kyu
*Yoo Sang-jae as Hong Yoon-seong
*Lee Ae-rin as Hong-dan
*Lee Yong-gwan as Yang-jeong
*Yoon Kyeong-ho as Im-woon
*Seo Hyeon-woo as Jin-moo
*Lee Chang-jik as Hwang Bo-in Kim Tae-woo King Munjong
*Ko Chang-seok as His Excellency Choi
*Kim Kang-hyeon as Suspect 3

==Production==
Kim Dong-hyuks screenplay won the grand prize at the 2010 Korean Scenario Contest held by the Korean Film Council. 

==Awards and nominations==
2013 50th Grand Bell Awards
*Best Film
*Best Director: Han Jae-rim 
*Best Actor: Song Kang-ho
*Best Supporting Actor: Jo Jung-suk
*Best Costume Design: Shim Hyun-sub
*Popularity Award: Lee Jung-jae
*Nomination - Best Actor: Lee Jung-jae
*Nomination - Best Supporting Actor: Baek Yoon-sik 
*Nomination - Best Screenplay: Kim Dong-hyuk
*Nomination - Best Cinematography: Go Nak-seon
*Nomination - Best Art Direction: Lee Ha-jun
 34th Blue Dragon Film Awards
*Best Supporting Actor: Lee Jung-jae
*Nomination - Best Film
*Nomination - Best Director: Han Jae-rim
*Nomination - Best Actor: Song Kang-ho
*Nomination - Best Supporting Actor: Jo Jung-suk
*Nomination - Best Supporting Actress: Kim Hye-soo
*Nomination - Best Screenplay: Kim Dong-hyuk
*Nomination - Best Cinematography: Go Nak-seon
*Nomination - Best Art Direction: Lee Ha-jun
*Nomination - Best Lighting: Shin Kyung-man, Lee Chul-oh

2013 33rd Korean Association of Film Critics Awards
*Best Actor: Song Kang-ho
*Best Supporting Actor: Jo Jung-suk
*Best Music: Lee Byung-woo
* 

2014 5th KOFRA   Film Awards
*Best Supporting Actor: Lee Jung-jae

2014 9th Max Movie Awards
*Best Poster 
*Nomination: Best Actress: Kim Hye-soo
*Nomination: Best Supporting Actor: Lee Jung-jae
*Nomination: Best Supporting Actor: Jo Jung-suk
*Nomination: Best Supporting Actress: Kim Hye-soo
*Nomination: Best Preview 

2014 19th Chunsa Film Art Awards
*Nomination - Best Director: Han Jae-rim
*Nomination - Best Actor: Lee Jung-jae
*Nomination - Best Screenplay: Kim Dong-hyuk
 8th Asian Film Awards
*Nomination - Best Costume Design: Shim Hyun-sub
 50th Baeksang Arts Awards
*Best Supporting Actor: Lee Jung-jae
*Nomination - Best Film
*Nomination - Best Supporting Actor: Kim Eui-sung

2014 23rd Buil Film Awards
*Nomination - Best Supporting Actor: Lee Jung-jae
*Nomination - Best Art Direction: Lee Ha-jun

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 
 

 

* Watch Online Streaming  

 
 
 
 
 
 