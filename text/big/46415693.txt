The Guvnors
 
{{Infobox film
| name           = The Guvnors
| image          = The Guvnors.jpg
| caption        = 
| director       = Gabe Turner
| producer       = 
| writer         = Gabe Turner Harley Sylvestre Doug Allen
| music          = Paul Arnold Andrew Barnabas Pascal Bideau
| cinematography = James Friend
| editing        = 
| studio         = 
| distributor    = Metrodome Distribution
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Guvnors is a 2014 film  directed by Gabe Turner.   

==Plot==
Mitch, the ex-leader of a London firm known as The Guvnors has walked away from his life of violence and more than 20 years later is happily married. He becomes concerned when his son starts to show violent tendencies through his behavior at school. He is challenged by Shanko a local gangster after Shanko learns of the reputation of The Guvnors. Shanko is then humiliated at the hands of another former Guvnor, Mickey. This leads to brutal retaliation and a reuniting of The Guvnors which reignites gangland warfare spanning two generations of families. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 