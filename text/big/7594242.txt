Thumbsucker (film)
: Beaverwood redirects here. For the tree, see Celtis occidentalis, more commonly known as the common hackberry.
{{Infobox film
| name = Thumbsucker
| image = Thumbsucker film.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Mike Mills
| producer = Anthony Bregman Bob Stephenson
| screenplay = Mike Mills
| based on =   Lou Pucci Tilda Swinton Vincent DOnofrio Keanu Reeves
| music = The Polyphonic Spree Elliott Smith
| cinematography = Joaquin Baca-Asay
| editing = Haines Hall Angus Wall Bob Yari This Is That Cinema Go-Go Good Machine
| distributor = Sony Pictures Classics
| released =  
| runtime = 95 minutes  
| country = United States
| language = English
| budget = $4 million
| gross = $1,919,197 
}} American independent independent comedy-drama Mike Mills novel of the same name. It stars Lou Taylor Pucci, Tilda Swinton, Vincent DOnofrio, Kelli Garner, Benjamin Bratt, Vince Vaughn, and Keanu Reeves. The movie focuses on teenager Justin Cobb as he copes with his thumb-sucking problem, and on his experiments with hypnosis, sex, and drugs.

==Plot==
Justin Cobb is a shy 17-year-old in a family of four in suburban Oregon. He has a persistent thumb-sucking habit his father disapproves of, which has led to major orthodontic repair. He addresses his parents by their first names, Mike and Audrey, so as not to make his father feel old. Audrey, a registered nurse, is idly fascinated by actor Matt Schramm, entering a contest to win a date with him. She insists it is "innocent fun", but is inordinately concerned with looking attractive for the contest.

Justin struggles on his schools debate team, led by Mr. Geary which he joined to get closer to his environmentalist classmate Rebecca. He tries to start a relationship with her, but she rejects him after he cannot open up to her about his thumb-sucking habit.
 suggesting that his thumb will taste like echinacea. This works, and Justin finds his thumb distasteful, but falls deeper into frustration without the crutch. After Justin conspires with his brother to disrupt Dr. Lyman in a bicycle race with Justins father, his school counselor prods the Cobbs to give him Ritalin. While his parents wring their hands over the idea, Justin insists that he needs the help.
 stoners crowd. neuroses of the adults around him, especially their struggles with aging. With a somewhat deceitful cover letter, he applies to New York University|NYU, in spite of his mothers urging that he go to college closer to home.

After rambling incoherently at the state debate championship, Justin quits the debate team, throws away the pills, and seeks out Rebecca to hook him up with Cannabis (drug)|pot. During their smoking sessions, Rebecca blindfolds him and engages with him in kissing and other sexual activity, which Justin interprets as a relationship. But when he broaches the subject, Rebecca tells him otherwise, calling their meetings an "experiment." He quits both her and the drugs.
 rehab facility where Schramm has been committed. Attempting to catch his mother in the act, he instead meets Schramm sneaking a smoke in the bushes, and learns the unromantic truth. The next day, he receives an acceptance letter from NYU.
 sleeptalking to find his thumb in his mouth and an attractive girl smiling at him. Slightly embarrassed but self-confident, he introduces himself.

==Cast== Lou Pucci as Justin Cobb
** Colton Tanner as 10-year-old Justin
* Tilda Swinton as Audrey Cobb
* Vincent DOnofrio as Mike Cobb
* Keanu Reeves as Dr. Perry Lyman
* Kelli Garner as Rebecca
* Benjamin Bratt as Matt Schramm
* Vince Vaughn as Mr. Geary
* Chase Offerle as Joel Cobb
* Kit Koenig as Principal
* Nancy ODell as herself
* Walter Kirn (Cameo appearance|cameo) as Debate judge

==Reception==
Review aggregator Rotten Tomatoes gives Thumbsucker a score of 71% based on 108 reviews, with an average rating of 6.7/10. The sites consensus is: "Though quirky coming-of-age themes are common in indie films, this one boasts a smart script and a great cast." 

Steven Rea of the Philadelphia Inquirer gave it three-and-a-half stars out of four, calling it a "quiet, quirky gem" and "terrific". 

Roger Ebert of the Chicago Sun-Times gave it three stars out of four, writing "I have focused on Justin, but really the movie is equally about the adult characters, who all seem to have lacked adequate parenting themselves. We talk about the tragedy of children giving birth to children; maybe that can happen at any age." 

==Location==
   Beaverton and Sherwood, Oregon|Sherwood, Oregon, Portland International Airport, Tualatin High School, and the Living Enrichment Center in Wilsonville, Oregon.   

==Soundtrack==
 
The soundtrack to the film was originally to consist of a number of cover songs performed by Elliott Smith, but he died before the projects completion. Tim DeLaughter and The Polyphonic Spree were then chosen to compose an original soundtrack after Mills attended one of their shows and was impressed. Three of Smiths songs remain on the soundtrack.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 