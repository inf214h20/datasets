The Day of the Beast (film)
{{Infobox film
| name           = The Day of the Beast
| image          = Dayofthebeast.jpg
| caption        = Theatrical release poster by Oscar Mariné
| director       = Álex de la Iglesia
| producer       = Andrés Vicente Gómez
| writer         = Jorge Guerricaechevarría Álex de la Iglesia
| starring       = Álex Angulo Armando De Razza Santiago Segura Maria Grazia Cucinotta
| music          = Battista Lena
| cinematography = Flavio Martínez Labiano
| editing        = Teresa Font
| studio         = Canal+ (Spanish satellite broadcasting company)|Canal+ España Iberoamericana Films Producción Sociedad General de Televisión (Sogetel)
| distributor    = Trimark Pictures (USA)
| released       =  
| runtime        = 103 minutes
| country        = Spain Italy
| language       = Spanish
| budget         = $1,500,000
}} Spanish black black comedy horror action film co-written and directed by Álex de la Iglesia and starring Álex Angulo, Armando De Razza and Santiago Segura. Both Maria Grazia Cucinotta and El Gran Wyoming have small roles in the film as well.

== Plot synopsis ==
  towers are said to be shaped as a diabolical signature and the place of birth of the Antichrist.]] Basque Priesthood Roman Catholic priest dedicated to committing as many sins as possible (Angulo), a death metal salesman from Carabanchel (Segura), and the Italian host of a TV show on the occult (De Razza). By committing multiple sins, the priest hopes to sell his soul, so that he can be at the birth of the Antichrist and kill it before it can destroy the world.
 acid trip", the devil manifests and, not being fooled by the priests actions, he does not reveal the location of the Antichrist.

The trio eventually track down the birthplace of the Antichrist. The salesman is killed by the Antichrist and the devils minions attack and badly burn the host, but the priest is able to kill them and the Antichrist. After the events, the priest and the host become homeless drifters.

== Cast ==
* Álex Angulo as Father Ángel Berriartúa
* Armando De Razza as Professor Cavan
* Santiago Segura as José María
* Terele Pávez as Rosario
* Nathalie Seseña as Mina
* Maria Grazia Cucinotta as Susana
* Jaime Blanch
* Antonio Dechent
* Saturnino Garcia
* Ramon Agirre
* El Gran Wyoming

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 