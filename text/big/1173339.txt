Jaws 3-D
 
{{Infobox film
| name           = Jaws 3-D
| image          = jaws_3d.jpg
| caption        = Theatrical release poster
| based on       = Characters: Peter Benchley
| story          = Guerdon Trueblood
| screenplay     = Carl Gottlieb Richard Matheson
| starring = {{plainlist|
* Dennis Quaid
* Bess Armstrong
* Simon MacCorkindale
* Louis Gossett, Jr.
}}
| director       = Joe Alves
| producer       = Rupert Hitzig
| studio         = Alan Landsburg Productions Universal Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| music          =  
| cinematography = James A. Contner Chris Condon Austin McKinney
| editing        = Corky Ehlers Randy Roberts
| budget         = $20.5 million
| gross          = $87.9 million 
}} horror thriller film directed by Joe Alves and starring Dennis Quaid, Bess Armstrong, Lea Thompson and Louis Gossett, Jr.  novel by Peter Benchley and is the third installment in the Jaws (franchise)|Jaws franchise. It is also the only one in the series not made directly by Universal Studios|Universal, and one of the few theatrical films produced by Alan Landsburg Productions (the film is copyrighted to "MCA Theatricals Ltd").

The film is notable for making use of 3D film during the revived interest in the technology in the 1980s, amongst other horror films such as Friday the 13th Part III and Amityville 3D. Cinema audiences could wear disposable cardboard polarized 3D glasses to create the illusion that elements penetrate the screen.    Several shots and sequences were designed to utilise the effect, such as the sharks destruction. Since 3D was ineffective in home viewing until the advent of 3D televisions in the early 2000s, the alternative title Jaws III is used for television broadcasts, VHS and DVD.   

Jaws 3-D was followed by   in 1987.

==Plot== SeaWorld park and throws the gate off its rails while it is closing. Meanwhile, Florida announces the opening of SeaWorlds new glass covered underwater tunnels that allow close up views of the parks animals.

Kathryn "Kay" Morgan (Bess Armstrong), SeaWorlds senior marine biologist, wonders why dolphins Cindy and Sandy are so afraid of leaving their dolphin pen. When Shelby Overman dives into the water to repair and secure the gates, he is killed by a shark. That night, two men in diving equipment sneak into the park in a small inflatable boat to steal coral they intend to sell. Both are killed by the shark, which also bites and sinks the inflatable.
 Jaws film) are informed of Overmans disappearance. They use a small submarine to search through an underwater Spanish Galleon fixture, despite dolphins Cindy and Sandys attempting to keep them out. As they search the Spanish Galleon they encounter a small great white shark. The dolphins rescue Kay and Mike.

SeaWorld park manager Calvin Bouchard (Louis Gossett, Jr.) does not believe the shark story until a first hand examination of the fence damage that connects to cindy and sandys pen. The news is exciting to his underwater film maker and photographer friend, Phillip FitzRoyce (Simon MacCorkindale), who states his intention to kill the shark on network television. Kay argues that killing the shark would be good for one headline, but capturing and keeping a great white shark in captivity would guarantee TV crews and money constantly rolling into SeaWorld. The young shark is captured and Kay and her staff nurse it to health. Calvin, desperate to start the money rolling in immediately, prematurely orders it moved to a small tank for exhibit, where the young shark soon dies.

At the underwater tunnels, a girl is terrified when she sees part of Overmans corpse float up to a window. Examining the bite marks on Overmans corpse, Kay realizes that the shark that killed him must be the small sharks mother, and that since Overman was killed inside the park, the mother shark must also be inside the park. Based on the shape of the bite, Kay concludes that the sharks mouth must be about three feet wide and thus the shark about 35 feet long. This captures the attention of FitzRoyce, but she cannot convince Calvin until the enormous shark shows herself at the window of the parks underwater cafe, terrifying the customers.

Flushed out from her refuge inside the filtration pipe, where fast moving water flowed across her gills, the shark wreaks havoc throughout the park.  The shark attacks water skier Kelly Ann Bukowski (Lea Thompson) and Mikes brother Sean (John Putch), injuring Kellys leg before causing a leak that nearly drowns everyone in the underwater tunnels.

FitzRoyce and his assistant Jack (P. H. Moriarty) lure the shark into the filtration pipe in an attempt to kill it. When FitzRoyce is attacked by the shark he attempts to use a grenade to kill it, but the shark devours him before he can pull the safety pin.  With the shark safely in the pipe, Mike and Kay dive down to repair the underwater tunnel so the technicians can restore air pressure and save the customers. Calvin orders the filtration pump shut down to suffocate the shark, but the shark breaks free from the pipe and attacks Mike and Kay. They escape thanks to help from dolphins Cindy and Sandy, who attack the shark to distract her briefly.

They join Calvin in the control room, but the shark appears in front of the window and smashes its way through the glass and floods the room, knocking out a technician. Calvin manages to swim out and rescue the unconscious technician, but another technician is eaten by the shark in the process. Mike notices FitzRoyces corpse still in the sharks throat with the grenade in his hand, and uses a bent pole to pull the grenades pin, killing the shark.

==Cast==
* Dennis Quaid as Mike Brody
* Bess Armstrong as Kathryn Morgan
* Simon MacCorkindale as Philip FitzRoyce
* Louis Gossett, Jr. as Calvin Bouchard
* John Putch as Sean Brody
* Lea Thompson as Kelly Ann Bukowski
* Harry Grant as Shelby Overman
* P. H. Moriarty as Jack Tate
* Dan Glasko as Danny
* Elizabeth Morris as Elizabeth

==Production== David Brown spoof named National Lampoon John Hughes and Todd Carroll for a script.    Joe Dante was briefly pursued as a director.    The project was shut down due to conflicts with Universal Studios.  David Brown later said that the studio attitude was that a spoof would have been a mistake and that it would be like "fouling in your own nest. We should have fouled the nest. It would have been golden, maybe even platinum." 
 Marineland theme park in Florida had seen his 1978 3D film Sea Dream. Lerner said that his "heart sank" when he was sent the first script of Jaws 3-D, saying "I cant really get involved in this". As the production already had an art director, Lerner declined to be involved in the film. 

The film was directed by Joe Alves, who was the production designer for the first two films and was the second unit director for Jaws 2. It had been suggested that Alves co-direct the first sequel with Verna Fields when first director John D. Hancock left the project.  It was filmed at SeaWorld Orlando, a landlocked water park; and Navarre, Florida, a community in the Florida Panhandle near Pensacola, Florida|Pensacola. 

As with the first two films in the series, many people were involved in writing the film. Richard Matheson, who had written the script for Steven Spielbergs celebrated 1971 television film Duel (1971 film)|Duel, says that he wrote a "very interesting" outline, although the story is credited to "some other writer".    Universal forced Matheson to include Brodys two sons, which the writer "thought was dumb". They also wanted it to be the same shark that was electrocuted in Jaws 2.  Matheson was also requested to write a custom-role for Mickey Rooney, "which I did so successfully that when Mickey Rooney turned out not to be available, the whole part was pointless".  The writer was unhappy with the finished film.

 Im a good storyteller and I wrote a good outline and a good script. And if they had done it right and if it had been directed by somebody who knew how to direct, I think it would have been an excellent movie. Jaws 3-D was the only thing Joe Alves ever directed; the man is a very skilled production designer, but as a director, no. And the so-called 3D just made the film look murky  – it had no effect whatsoever. It was a waste of time.  

Guerdon Trueblood is credited for the story; a reviewer for the website SciFilm says that the screenplay was based upon Truebloods story about a white shark swimming upstream and becoming trapped in a lake.    Carl Gottlieb, who had also revised the screenplays for the first two Jaws films, was credited for the script alongside Richard Matheson.      Matheson has reported in interviews that the screenplay was revised by script doctors. 

The film did not use any actors from the first two Jaws films. Roy Scheider, who played Police Chief Martin Brody in the first two films, laughed at the thought of Jaws 3, saying that "Mephistopheles ... couldnt talk me into doing   ... They knew better than to even ask".    He agreed to do Blue Thunder to ensure his unavailability for Jaws 3-D. 

===3D=== 3D at this time, with many films using the technique. Jaws second sequel integrated the technology into its title, as did Amityville 3D. Friday the 13th Part III could also make dual use of the number three.    The gimmick was also advertised in the tagline "the third dimension is terror."  As it was Joe Alves first film as director, he thought that 3D would "give him an edge". 

 
Cinema audiences could wear disposable polarized glasses to view the film, creating the illusion that elements from the film were penetrating the screen to come towards the viewers. The opening sequence makes obvious use of the technique, with the titles flying to the forefront of the screen, leaving a Doppler effect|trail. There are more subtle instances in the film where props are meant to leave the screen. The more obvious examples are in the climactic sequence of the shark attacking the control room and its subsequent destruction. The glass as the shark smashes into the room uses 3D, as does the shot where the shark explodes, with fragmented parts of it apparently bursting through the screen, ending with its jaws. There were many difficulties in making the green screen compositing work in 3D, and a lot of material had to be reshot. 

Jaws 3-D had two 3D consultants: the production started with   35–3 camera with Arrivision 18&nbsp;mm over/under 3D lens. 

This kind of 3D effect does not work on television without special electronic hardware, and so with two exceptions, the home video and broadcast TV versions of Jaws 3-D were created using just the left-eye image, and with the title changed to "Jaws 3" or "Jaws III". Because the left-eye image only takes up half the 35&nbsp;mm film frame, the picture resolution is noticeably poorer than would normally be expected of a film shot on 35&nbsp;mm.
 VHD video disc system (not to be confused with LaserDisc). This required a special 3D VHD player, or a standard VHD player with a hardware 3D adapter, and a set of LCD glasses that shuttered the viewers eyes according to control signals sent by the player, allowing the polarised 3D effect to work.  The other exception was the Sensio 3D DVD of Jaws 3-D released in February 2008. The Sensio 3D Processor is needed for 3D home viewing. 
 anaglyph glasses to fully enjoy the movie; this was an anaglyph 3D version of the film created from the Arrivision original.

==Music==
{{Infobox album
| Name        = Jaws 3-D
| Type        = soundtrack Alan Parker
| Cover       = Jaws3 cd.gif
| Released    = 1983
| Recorded    = Angel Studios, London
| Genre       = Orchestral
| Length      = 35:43
| Label       = MCA Records
| Producer    = Graham Walker
| Chronology  = Jaws Jaws 2 Jaws 3-D
| Next album  =  
}} Alan Parker, Van der American Gothic.  John Williams famous shark motif is, however, integrated into the score. The soundtrack album was released by MCA Records which was absorbed by Geffen Records. The soundtrack was later released on CD by Intrada and was limited to only 3000 copies. 

===Track listing===
# "Jaws 3-D Main Title" (2:59)
# "Kay and Mikes Love Theme" (2:18)
# "Panic at Seaworld" (2:07)
# "Underwater Kingdom and Shark Chase" (4:20)
# "Shark Chase and Dolphin Rescue" (1:22)
# "Saved by the Dolphins" (2:05)
# "The Sharks Gonna Hit Us!" (2:42)
# "Its Alive/Seaworld Opening Day/Silver Bullet" (2:34)
# "Overmans Last Dive" (1:18)
# "Philips Demise" (4:59)
# "Night Capture" (4:53)
# "Jaws 3-D End Titles" (4:06)

==Reception==
The film opened in more than a thousand screens across the U.S. There were many promotions to accompany the release of the film. As with Jaws 2,  s 1983 documentary film The Sharks in the featurette without authorization. 

===Box office===
The film grossed $13,422,500 on its opening weekend,    playing to 1,311 theaters at its widest release. This was 29.5% of its total gross. It has achieved total lifetime worldwide gross of $87,987,055.  Despite being No. 1 at the box office, this illustrates the series diminishing returns, since Jaws 3-D has earned $120 million less than the total lifetime gross of its predecessor  and almost $400 million less than the original film  which is even less when taking inflation into account. The final sequel would attract an even lower income, with around two thirds of Jaws 3-Ds total lifetime gross. 

===Critical response===
Reception for the movie was generally negative. Variety (magazine)|Variety calls it "tepid" and suggests that Alves "fails to linger long enough on the Great White."  It has an 11% rotten rating at Rotten Tomatoes.  The 3D was criticized as being a gimmick to attract audiences to the aging series    and for being ineffective.  Allmovie, however, says that "the suspense sequences were made somewhat more memorable during the films original release with 3D photography, an attribute lost on video, thereby removing the most distinctive element of an otherwise run-of-the-mill sequel."  Derek Winnert says that "with Richard Mathesons name on the script youd expect a better yarn" although he continues to say that the film "is entirely watchable with a big pack of popcorn."  Others are disappointed that Matheson and Gottlieb produced this script given their previous success.  One critic summed up the general consensus:

 Campy performances, cheesy special effects, and downright awful dialogue all contribute to making Jaws 3 a truly dismal experience for just about everyone. Its not only hard to believe that a sequel this downright abominable didnt kill the franchise, but that it actually would be followed by a movie that was arguably worse—Jaws: the Revenge.    

Amongst some flaws, some critics describe the film as "marginally entertaining."  The sound design has been commended, however. The moment when an infants cry is heard when the baby shark dies in the pool is particularly praised by one reviewer.  Gossett, Jet magazine says, was the "only cast member to survive the generally negative reviews". 

In her screenwriting textbook, Linda Aronson suggests that its protagonist, played by Quaid, is a major problem with the film. She says that after taking too long for him to be introduced, the character is "essentially a passive onlooker." There is no hunt until the climax when the shark is terrorizing the people in the aquarium; only then does Mike Brody become center of the action. She also highlights inaccuracies in the plot. For instance, she rejects the idea of a "mother shark protecting her offspring   sharks do not mother their young," and points out that dolphins can attack sharks. 

Leonard Maltin calls the film a "road-company Irwin Allen type-disaster film" and notes that its premise is similar to that of Revenge of the Creature, the 1955 sequel to Creature from the Black Lagoon. 

Jaws 3-D was nominated for five 1983 Golden Raspberry Awards, including Worst Picture, Director, Supporting Actor (Lou Gossett, Jr.), Screenplay, and Newcomer (Cindy and Sandy, "The Shrieking Dolphins"), but received none. 

==Home Media== Universal on June 3, 2003 under the title Jaws 3. With the exception of one theatrical trailer, no bonus features were included.

==See also==
*Revenge of the Creature, a 1955 3D sequel film, featuring an attack on a Floridian marine mammal park  .
*List of killer shark films

==References==

===Notes===
 

===Bibliography===
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
 
* 
* 
* 
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
         
 
 
 
 
 
 
 