Lissy (film)
{{Infobox film
| name           =Lissy
| image          = 
| image size     =
| caption        =
| director       =Konrad Wolf
| producer       =
| writer         = Konrad Wolf, Alex Wedding
| narrator       = 
| starring       = Sonja Sutter, Horst Drinda, Hans-Peter Minetti
| music          = Joachim Werzlau	 	
| cinematography = Werner Bergmann
| editing        = Lena Neumann
| studio    = DEFA 
| distributor    =
| released       = 30 May 1957
| runtime        = 89 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German film, directed by Konrad Wolf, based on a novel by Franz Carl Weiskopf|F.C. Weiskopf.

==Plot==
Lissy Schroeder, a working class girl in Berlin, marries Alfred, a clerk.  In 1932, Alfred is fired by his Jewish boss.  Despite having ties to the Communist party through Lissys brother Paul, the previously apolitical Alfred joins the Nazi party.  After Hitler gains power, Paul is shot by the Nazis, causing Lissy to question the countrys and her husbands politics and where her loyalties truly lie.

==Cast==
* Sonja Sutter as Lissy Schröder/Frohmeyer
* Horst Drinda as Alfred Frohmeyer
* Hans-Peter Minetti as Paul Schröder
* Kurt Oligmüller as Kaczmierczik
* Gerhard Bienert as Vater Schröder
* Else Wolz as Mutter Schröder
* Raimund Schelcher as Max Franke
* Christa Gottschalk as Toni Franke

==External links==
*   at the Internet Movie Database

 

 
 
 
 
 

 