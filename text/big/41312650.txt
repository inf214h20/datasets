Chaos (2008 film)
 
 
{{Infobox film
| name           = Chaos
| image          = Chaos2008.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 三不管
| simplified     = 三不管
| pinyin         = Sān Bú Guǎn 
| jyutping       = Saam1 Bat1 Gwun2 }}
| director       = Herman Yau
| producer       = Teddy Chan
| writer         = Lau Ho Leung
| starring       = Gordon Lam Andrew Lin Kristal Tin Charmaine Fong
| music          = Patrick Lo
| cinematography = Ngai Man Yin
| editing        = Yau Chi Wai
| studio         = Fortune Star Entertainment Sum-Wood Productions
| distributor    = Fortune Star Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}

Chaos is a 2008 Hong Kong action thriller film directed by Herman Yau and starring Gordon Lam, Andrew Lin, Kristal Tin and Charmaine Fong.

==Plot==
In the Walled City, the inhabitants are either cornered or have a shady past. They commit prostitution, gambling, drugs and other lawless acts. They have their own order and law. The Walled City is akin to a "limbo" zone. One day, Cheung Tai Hoi (Andrew Lin), while escorting criminal Mickey Szeto (Gordon Lam), accidentally loses control of the police van and breaks into the Walled City and is detained by its inhabitants. Walled Citys overlord Crow (Alexander Chan) hate the police and believes one of them is a cop and prepares to execute one of them. At this time, a brothel owner in Walled City Ling (Kristal Tin) falsely accuses Mickey to be the cop. Therefore, Hoi was released while Mickey was continued to be detained. It turns out that Mickey is Lings long lost lover and she wants him to suffer since she believes he abandoned her and her daughter Yan (Charmaine Fong) years ago. At the same time, she also wants Mickey to help her leave Walled City because Crow has been eying for Yan. Yan then sees the detained Mickey, while she does not know he is her father, she had a feeling which prompt her to decide to help him escape. At the same time, a fatal plague was discovered in Walled City and the government uses this as an excuse to start a massacre that would prevent the spreading of the plague. Will Mickey, Tai Hoi, Ling and Yan survive the massacre? Will Mickey and Yan be reunited as father and mother?

==Cast==
*Gordon Lam as Mickey Szeto
*Andrew Lin as Tai Hoi
*Kristal Tin as Ling
*Charmaine Fong as Yan
*Alexander Chan as Crow
*Wong Shu Tong as Mr. Kim
*Sam Wong as Inmate 2378
*Wong Man Shing as Crows Thug
*Chan Man Ching as Crows Thug
*Lui Siu Ming as Crows Thug
*Chin Yiu Wing as Crows Thug

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 ]
 
 
 