Casanova (1918 film)
{{Infobox film
| name           = Casanova
| image          = 
| caption        = 
| director       = Alfréd Deésy
| producer       = 
| writer         = Izsó Barna László Békeffi Jenö Faragó József Pakots
| starring       = Alfréd Deésy
| music          = 
| cinematography = Károly Vass
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hungary Silent
| budget         = 
| gross          = 
}}
Casanova is a 1918 Hungarian film directed by Alfréd Deésy and featuring Béla Lugosi.

==Cast==
* Péter Andorffy - Hilmer,gyáros
* Viktor Costa
* Norbert Dán - Roland herceg
* Alfréd Deésy - Casanova
* László Faludi - Molnárlegény
* Tessza Fodor
* Annie Góth - Felsenburg hercegnõ,Marie
* Sandy Igolits - Clara
* Richard Kornai - Külügyminiszter
* Ila Lóth
* Béla Lugosi - (as Olt Arisztid)
* Gyula Margittai - Waldenstein gróf
* Marcell Rolla
* Sári Sólyom - Denisse
* Margaretta Tímár - Margaretta Hilmer,a gyáros lánya
* Gusztáv Turán
* Miklós Ujvári - Az apa
* Camilla von Hollay - Ninette (as Hollay Kamilla)
* Lucy Wett - Suzanne Hilmer, a gyáros lánya

==See also==
* Béla Lugosi filmography

==External links==
* 

 

 
 
 
 
 
 
 
 