Endendigu
{{Infobox film
| name           = Endendigu
| image          = 2015 film Endendigu poster.jpg
| caption        = Theatrical release poster
| director       = Imran Sardhariya
| producer       = S. V. Babu
| writer         = 
| screenplay     = Jayatheertha Imran Sardhariya
| story          = 
| based on       = 
| starring       = Ajay Rao Radhika Pandit
| narrator       = 
| music          = V. Harikrishna Venkatesh Anguraj
| editing        = Deepu S. Kumar
| studio         = S. V. Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India Kannada
| budget         = 
| gross          = 
}}

Endendigu ( ) is an upcoming Indian Kannada language romance film directed by Imran Sardhariya. It stars Ajay Rao and Radhika Pandit in the lead roles.

==Cast==
* Ajay Rao as Krishna
* Radhika Pandit as Jyothi
* H. G. Dattatreya Ashok
* Chandrashekhar
* Raveendranath
* Kalyani Raju
* Tabla Nani Sumithra
* Pavitra Lokesh
* Shantamma
* B. Suresha
* Anmol (credited as Master Anmol)

==Production== Breaking News (2012).  Filming began in November 2013 in Sweden, where song sequences were shot.  

==Soundtrack==
{{Infobox album
| Name        = Endendigu
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = 2015 Kannada film Endendigu album cover.JPG
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 27 March 2015
| Recorded    =  Feature film soundtrack
| Length      = 20:42
| Label       = D Beats
| Producer    = V. Harikrishna
| Last album  = 
| This album  = 
| Next album  = 
}}

V. Harikrishna composed the films background score and music for its soundtrack. The soundtrack album was released on 27 March 2015.  It consists of five tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 20:42
| lyrics_credits = yes
| title1 = Ninnalle
| lyrics1 = K. Kalyan
| extra1 = Sonu Nigam, Shreya Goshal
| length1 = 4:42
| title2 = Iduvarege
| lyrics2 = Yogaraj Bhat
| extra2 = Sonu Nigam
| length2 = 4:17
| title3 = Friendu Maduve
| lyrics3 = Yogaraj Bhat
| extra3 = V. Harikrishna
| length3 = 3:57
| title4 = Kanna Kajal
| lyrics4 = A. P. Arjun
| extra4 = Sonu Nigam, Anuradha Bhat
| length4 = 4:13
| title5 = Endendigu
| lyrics5 = K. Kalyan Kartik
| length5 = 3:33
}}

===Reception===
Upon release, the album was received well by critics. Indiaglitz in its review called the tracks "top class in melody and lyrical strength."  Cienloka.com, reviewed the album and wrote, "ENDENDIGU definitely has class written all over. Audience will surely appreciate the efforts of HK   for churning out chartbusters with ease. Album guarantees Melody coupled with wholesome entertainment in its lavish package." 

==References==
 

 
 
 
 