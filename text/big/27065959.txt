Vindhyarani
{{Infobox film
| name           = Vindhyarani  (Telugu:     )
| image          = 
| image_size     =
| caption        =
| director       = C. Pullayya
| producer       =
| writer         = Pingali Nagendra Rao
| narrator       =
| starring       = Akkineni Nageswara Rao G. Varalakshmi Relangi Venkata Ramaiah Pushpavalli D. V. Subba Rao A. V. Subba Rao B. Padmanabham
| music          = Emani Sankara Sastry Saluri Rajeswara Rao
| cinematography = C. V. Ramakrishnan
| editing        = K. R. Krishna Swamy
| studio         =
| distributor    =
| released       = January 14, 1948
| runtime        =
| country        = India Telugu
| budget         =
}}
Vindhyarani (English title: Queen of the Vindhyas;  .  Pushpavalli played the title role of Vindhyarani.

==Plot==
Durjaya kills his elder brother Maharajah Jayaveera of Vindhya Kingdom and occupies the throne. Shatamitra secretly takes care of the Prince Shivasri and waiting for an opportunity to take revenge. Avanti is daughter of a subordinate king. She hates men. Durjaya connivingly brings her to the Kingdom and announces her as the Queen. Shivasri and Avanti started loving each other. Shatamitra sends message to Shivasri to kill Durjaya. Thinking that his love is a hindrance to his revenge, he told Avanti to forget him and his love. Avanti plans to kill Durjaya and plans to marry Shivasri. Shivasri attacks Durjaya but pardons him. Avanti kills him and escapes. The drama continued. At the end Shivasri and Avanti get married and rules the Vindya Kingdom.

==Credits==

===Cast===
. Ramana rao
* G. Varalakshmi
* Relangi Venkataramaiah
* Pushpavalli	as   Vindhyarani
* D. V. Subba Rao
* Sreevatsava
* A. V. Subba Rao
* B. Padmanabham
* Kumari Sakkubai

===Crew===
* Director: Chittajallu Pullayya
* Story and Dialogues: Pingali Nagendra Rao
* Production Company: Vyjayanthi Pictures
* Original Music: Eemani Sankara Sastri and Saluri Rajeshwara Rao
* Cinematography: C. V. Ramakrishnan
* Film Editing: K. R. Krishnaswamy
* Art Direction: K. R. Sarma
* Sound Engineer: S. C. Gandhi
* Playback singer: Ghantasala Venkateswara Rao

==References==
 

==External links==
*  

 
 
 
 


 