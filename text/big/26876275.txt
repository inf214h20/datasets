The Proud Youth
 
 
{{Infobox film name = The Proud Youth image = caption = traditional = 笑傲江湖 simplified = 笑傲江湖 pinyin = Xiào Ào Jiāng Hú}} director = Sun Chung producer = Runme Shaw story = Louis Cha screenplay = Ni Kuang starring = Michael Chan Ling Yun music = Frankie Chan Wang Fu-ling cinematography = Lam Ngai Kai editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 92 minutes country = Hong Kong language = Mandarin budget = gross = 
}} Louis Chas Michael Chan and Ling Yun.

==Cast==
*Wong Yue as Nangong Song (renamed from Linghu Chong) Michael Chan as Hao Jieying (renamed from Tian Boguang)
*Shih Szu as Bai Yingying (renamed from Ren Yingying)
*Stanley Fung as Luo Chaojun (renamed from Yue Buqun)
*Wong Chung as Guardian Shi (renamed from Xiang Wentian)
*Ling Yun as Shi Zhongying (renamed from Liu Zhengfeng)
*Yau Chui-ling as Lan Fenghuang
*Cheng Miu as Leng Ruojun (renamed from Zuo Lengshan)
*Lau Wai-ling as Luo Shouyi (renamed from Yue Lingshan)
*Chan Shen as Priest Zhishan (renamed from Taoist Tianmen)
*Teresa Ha as Abbess Yixin (renamed from Abbess Dingyi)
*Wong Ching-ho as Cui Lin
*Ng Hong-sang as Lao Denuo
*Chan Wai-ying as Huizhi
*Chong Lee as Luo Yingzhi
*Ku Feng as Master Bai (renamed from Ren Woxing)
*Ding Ying as Guardian Shis wife
*Yue Wing as Gao Yun (renamed from Qu Yang)
*Tin Ching as Sima Wuji (renamed from Dongfang Bubai)
*Yeung Chi-hing as Bingji
*Wang Han-chen as Shaolin abbot
*Hung Ling-ling as Huixin
*Chin Yuet-sang
*Tang Tak-cheung
*Alan Chui
*Yuen Wah
*Tsang Choh-lam
*Gam Yam
*Yuen Bun
*Wong Chi-ming
*Hsu Hsia
*Ban Yun-sang
*Wong Pau-gei
*Siu Yuk-lung
*Chow Kin-ping
*Lau Fong-sai
*Wong Chi-keung
*Yuen Cheung-yan
*Lo Wai
*Alan Chan
*Law Keung
*Yuen Shun-yi
*Lee Hang
*Kong Chuen
*Ha Kwok-wing
*Lai Yau-hing
*Gam Tin-chue
*Lam Ching-ying

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 