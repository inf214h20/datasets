Vasantha Maligai
 
 

 

{{Infobox film
| name= Vasantha Maligai
| image= Vasantha Maligai.jpg
| director= Kovelamudi Surya Prakash Rao|K. S. Prakash Rao
| starring= Sivaji Ganesan   Vanisri   K. Balaji   Santha Kumari   Nagesh   Pandari Bai   Major Sundarrajan   V. K. Ramaswamy (actor)|V. K. Ramasamy
| producer=D. Ramanaidu
| writer= Bala Murugan
| story = Kousalya Dev
| music= K. V. Mahadevan
| cinematography = A. Vincent
| editing = K. A. Marthand Narasimha Rao
| studio = Vijaya & Suresh Productions
| distributor = Vijaya & Suresh Combines
| released= 29 September 1972
| runtime= 162 mins
| country = India
| language= Tamil
| budget=
}}
 Tamil film, Telugu film, Prem Nagar (1971 film)|Prem Nagar. (Its producer, D. Ramanaidu, made a third version of the film in Hindi, titled like the 1971 film Prem Nagar (1974 film)|Prem Nagar, which was released in 1974.)

Vasantha Maligai was a blockbuster, running in theatres for nearly 750 days, and Ganesan and Vanisri were both well-received for their acting. In the Telugu version, the lead roles were played by Akkineni Nageswara Rao and Vanisree, and in the Hindi by Rajesh Khanna and Hema Malini. A digitally restored version of the film was released in March 2013. 

==Plot==
Anand (Sivaji Ganesan) is a rich playboy and who loves alcohol. He has an older brother, Vijay (K. Balaji). Anand, who has been abroad, boards a plane home, on a shift that air stewardess, Latha (Vanisri), is working. After the plane lands, the scene changes to Lathas home where we meet her father (Major Sundarrajan), mother (Pandari Bai), two brothers, and a sister. Her elder brother resides at home with his wife, but being the eldest, Latha is the highest earning member of the family. A struggle begins when Lathas mother objects to her being an air hostess: She begs her to change profession so that she can be home at more decent hours.

The scene changes to Anand celebrating his birthday in a pub near his home. Vanisri arrives at the same pub for a job interview with the manager. However, the manager, a lustful man, in the guise of interviewing her, shuts the door and tries to rape her. While leaving the pub, Anand hears Latha screaming and pushes open the door. He fights the manager, rescues Latha, then drives her home. The next day, Latha goes to Anands house to return the coat he had lent her the night before. She then asks him for a job, to which he agrees, hiring her as his caretaker.

The next day he shows her around his house, and she meets his mother, brother, and sister-in-law. Latha soon notices that Anand is an alcoholic and, therefore, wishes to resign, but a servant begs her not to because Anands behaviour has changed for the better since meeting her.

Anands fiancée comes to his home and starts ranting that she no longer wishes to marry him because he is an alcoholic. Meanwhile, his sister-in-law, who concludes that Latha has come to steal Anands affections, disturbs his mother with these comments, as well as by the comments of his fiancée. Latha, however, assures the mother, after everyone has left the table, that she will in fact try and stop Anand from drinking. Later on she catches him drinking with his servant (Nagesh). His servant runs away upon seeing her, but Anand continues to drink. Latha throws the glass after arguing with him, so infuriating Anand that he throws a glass bottle onto Lathas forehead.

When he realises what he has done, he destroys all his bottles, promising Latha that he will never drink again. He confides in her the anguish of his soul, how when he was young both his father and his Ayya died. After this transformative incident, he announces that he is going to build a new palace for himself and the girl that he truly loves. He will call the palace "Vasantha Maligai."

Anand brings Latha to this new house; everyone in his family goads Latha to find out who this mysterious woman is that Anand loves. Latha, too, is curious to meet the girl of Anands heart. He then shows her her own reflection in a separate room revealing that she is the girl of his affections. However, Vijay witnesses this, and runs to tell his mother. He conjures a story about Latha having stolen his wifes jewellery. Hearing this, Anand becomes suspicious of Latha. He asks Latha about it, but she runs away utterly dejected that he could suspect her of such wrongdoing. Fortunately, Anand overhears his servant whisper to another about Vijays malicious plan.

Anand confesses his ignorance and apologises. But Latha will not forgive him. Anand loses his composure and becomes seriously ill. Meanwhile, Latha receives a marriage proposal. Anands mother goes to apologise to Latha while Latha hands her an invitation to her wedding. Anands mother shows this to her son, who then decides to attend the wedding and Latha is shocked to see him. She meets him privately to reconcile their differences but, unfortunately, her sister-in-law spots them and announces it to the guests. All depart, leaving Latha with her family. Then, all of a sudden, Anands mother enters the room and declares that Latha should marry Anand. When Latha arrives at the palace, she is shocked to see Anands condition. She does not know that out of desperation and lovesickness he had poisoned himself. As soon as Latha enters the room he faints. The scene changes to the hospital where we see a recovered Anand. And upon seeing Latha and his family, he is revived. The film ends on the blissful note of Latha hugging Anand.

==Cast==
* Sivaji Ganesan as Anand
* Vanisri as Latha
* K. Balaji as Vijay
* Santha Kumari as Anand and Vijays mother
* Nagesh as the servant
* Major Sundarrajan as Lathas father
* Pandari Bai as Lathas mother
* Sreekanth as Lathas elder brother
* V. K. Ramaswamy (actor)|V. K. Ramasamy

==Soundtrack==
The music composed by K. V. Mahadevan.  The song "Oru Kinnathai" was remixed by Yathish Mahadev in Indira Vizha (2009).  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:15
|-
| 2 || Irandu Manam || T. M. Soundararajan || 04:10
|-
| 3 || Kalai Magal || P. Susheela || 03:27
|-
| 4 || Kudimagane || T. M. Soundararajan, L. R. Eswari || 03:17
|-
| 5 || Mayakkam Enna || T. M. Soundararajan, P. Susheela || 03:30
|-
| 6 || O Manida || T. M. Soundararajan || 02:02
|-
| 7 || Oru Kinnathai || T. M. Soundararajan, Vasantha || 03:16
|-
| 8 || Yaarukkaga || T. M. Soundararajan || 03:28
|}

==Reception==

===Reviews===
M. Suganth of The Times of India rated the film 4.5 out of 5 stars saying, "To be frank, the opening 20 minutes are as choppy a ride as that experienced by the characters in the introductory scene." He continues: 
::"But forget K V Mahadevans songs, forget Krishnaraos dazzling sets and forget the leading man. This film still holds up so well – 40 years after its release – because of the writing and characterization. Yes, for a film that is dismissed as melodramatic romance, the writing (Balamurugan) is quite nuanced." 
He concludes that the film is "further proof that old is indeed gold." 

===Box office===
Vasantha Maligai was one of the biggest blockbusters for Sivaji Ganesan, running for over 750 days in theatres. It held the record of running the highest continuous full-house showings in Madras. The film had a remarkable 271 continuous full-house screenings in all the three theatres it was released, namely, Shanthi, Crown, and Bhuvaneswari.  This movie was also successful in Sri Lanka where it ran more than 250 days. 

==Re-releases==

===Theatrical===
A digitally restored version of Vasantha Maligai was due to be released in early December 2012,  but was ultimately released on March 2013.  The restoration was done by P. Srinivasan of Sai Ganesh Films at a cost of INR 10 million, consuming five months of work.  The film had a poor opening, grossing only   in the opening week. 

===Home media===
Vasantha Maligai is included alongside various other hit Sivaji Ganesan films in the compilation DVD 8 Ulaga Adhisayam Sivaji. 

==Legacy== same name. In Sivaji (film)|Sivaji (2007), Rajini and Shriya imitates the song "Mayakkam Enna". 

==References==
 

==External links==
*  

 
 
 
 
 
 