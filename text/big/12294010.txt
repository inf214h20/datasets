The Anarchist's Wife
{{Infobox film
| name = The Anarchists Wife
| director = Marie Noelle   Peter Sehr   
| starring = Juan Diego Botto   María Valverde    Ivana Baquero
| writer =   Marie Noelle   Ray Loriga
| music = Zacarías M. de la Riva
| producer = Marie Noelle    Norbert Llaras   Phillippe Planells    Peter Sehr   Jordi Rediu French
| released =  2008
}}
The Anarchists Wife ( ) is a 2008 Franco-German film directed by Maria Noelle and Peter Sehr. 
 Madrid against Nationalists during the Spanish Civil War. He was deported to a concentration camp, then fought for the French Resistance. His wife, with two young children, never gave up hope of seeing him again.

The film premiered at the Valladolid Film Festival on October 25, 2008.   The film was released in Spain under the name La Mujer Del Anarquista and in Germany as Die Frau des Anarchisten.

== Principal cast ==
* Juan Diego Botto as Justo
* Maria Valverde as Manuela
* Ivana Baquero as Paloma - age 15

== References ==
 

== External links ==
* 
*  at AK Press

 
 
 
 
 
 
 
 
 


 