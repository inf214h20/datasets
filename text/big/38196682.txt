Sassi Punnu (1958 film)
{{Infobox film
| name           = Sassi Punnu
| image          =
| image_size     =
| caption        =
| director       = Akbar Ali
| producer       = Syed A. Haroon (Eastern Studios)
| writer         =
| lyrics         = Rashid Lashari
| starring       = Nighat Sultana
| music          = Ghulam Nabi Abdul Latif
| cinematography =
| editing        =
| released       = 30 May 1958
| runtime        =
| country        = Pakistan Sindhi
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}

Sassi Punnu ( ) is a Pakistani film adapted from a popular Sindhi folk tale, produced by Syed A. Haroon, directed by Akbar Ali. It was released on 30 May 1958 and starred Nighat Sultana.

==See also==
* Sassui Punnhun
* Sindhi folklore
* Sindhi cinema
* List of Sindhi-language films

==Further reading==
* Gazdar, Mushtaq. 1997. Pakistan Cinema, 1947-1997. Karachi: Oxford University Press.

==External links==
*  

 
 
 

 