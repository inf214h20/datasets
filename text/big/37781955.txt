No Sail
{{Infobox Hollywood cartoon
| name           = No Sail
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Jack Hannah
| producer       = Walt Disney
| writer         = Dick Kinney Ralph Wright
| narrator       = 
| starring       = Pinto Colvig Clarence Nash
| music          = Oliver Wallace
| animator          = Bob Carlson Hugh Fraser John Reed Judge Whitaker
| layout artist     = Yale Gracey
| background artist = Thelma Whitmer
| cinematography = 
| editing        =  Walt Disney Productions RKO Radio Pictures
| release date   =   (USA)
| color process  = Technicolor
| runtime        = 7:30 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
No Sail is a cartoon produced by The Walt Disney Company in 1945, featuring Donald Duck and Goofy. It follows Donald and Goofy after finding themselves stranded at sea and the crazy ways they try to survive.

==Plot==
Goofy and Donald are at a marina where they hire a "U-Drive Sail Boat". After Donald boards, Goofy follows but stops partway along to untie the boat. After Goofy gets on board, Donald inspects the sail mechanism.

Aware of Goofys intentions, the seagull migrates from Donalds head to Goofys just as Goofy brings the club down on Donalds head. Upon realising where the bird is now, Goofy hands a dazed and angry Donald the club and is promptly hit on the head when the seagull changes sides again. Donald then grabs the club and begins repeatedly hitting Goofy on the head, however Goofy is distracted by the bird flying away. Donald stops to inspect the damage but is surprised when he sees his beating Goofy has left lumps on the club.
 On Ice, and Goofy complies, however he accidentally drops him bill-first into the sails coin slot, which sends the mast shooting out and folding into place.

==External links==
*  

 

 
 
 
 