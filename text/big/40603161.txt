The Hell Cat
 
{{infobox film
| title          =
| image          =
| imagesize      =
| caption        =
| director       = Reginald Barker
| producer       = Diva Pictures Samuel Goldwyn
| writer         = Willard Mack
| starring       = Geraldine Farrar Tom Santschi Milton Sills
| music          =
| cinematography = Percy Hilburn William Laub
| editing        =
| distributor    = Goldwyn Pictures
| released       =   reels
| country        = US
| language       = Silent film English intertitles
}}
The Hell Cat is a 1918 silent film western produced and distributed by Goldwyn Pictures. Reginald Barker directed and Geraldine Farrar starred.  

==Cast==
*Geraldine Farrar - Pancha OBrien
*Tom Santchi - Jim Dyke
*Milton Sills - Sheriff Jack Webb
*William Black - Panchas Father
*Evelyn Axzell - Wan-o-mee
*Clarence Williams
*George James Hopkins -
*Clarence Snyder -
*Raymond Wallace -
*Monte Jarrett - 
*Pete Nordquist -
*Jimmy Tuff -
*Dudley Smith -
*Charlie Black - 
*Bryan Wangoman -

uncredited
*Texas Guinan -

==References==
 

==External links==
* 
* 
* (Univ. of Washington, Sayre collection)

 
 
 
 
 

 