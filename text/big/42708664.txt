The Buchholz Family
{{Infobox film
| name = The Buchholz Family
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer = Carl Froelich   
| writer =  Julius Stinde (novel)   Jochen Kuhlmey
| narrator =
| starring = Henny Porten   Paul Westermeier   Käthe Dyckhoff   Marianne Simson
| music = Hans-Otto Borgmann 
| cinematography = Robert Baberske 
| editing =  Wolfgang Schleif     UFA
| distributor = Deutsche Filmvertriebs 
| released = 3 March 1944 
| runtime = 92 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Buchholz Family (German:Familie Buchholz) is a 1944 German drama film directed by Carl Froelich and starring Henny Porten, Paul Westermeier and Käthe Dyckhoff. It is a family chronicle set in late nineteenth century Berlin.  It is based on an 1884 novel by Julius Stinde.

==Cast==
*Henny Porten as: Wilhelmine Buchholz
*Paul Westermeier as Carl Buchholz, ihr Mann
*Käthe Dyckhoff as Betti, beider Tochter
*Marianne Simson as Emmi, beider Tochter
*Hans Zesch-Ballot as Fritz Fabian, Wilhelmines Bruder
*Gustav Fröhlich as Frauenarzt Dr. Franz Wrenzchen
*Grethe Weiser as Jette, Dienstmädchen bei Buchholzens
*Elisabeth Flickenschildt as Kathinka Bergfeldt
*Hans Herrmann Schaufuß as Rechnungsrat Bergfeldt, ihr Mann
*Erich Fiedler as Emil, beider Sohn
*Sigrid Becker as Auguste, beider Tochter
*Werner Stock as Franz Weigel, ihr Mann
*Jakob Tiedtke as sein Vater
*Albert Hehn as Friedrich Wilhelm Holle, Kunstmaler
*Kurt Vespermann as  Julius Stinde, Verleger
*Maria Loja as Frau Posener
*Hellmut Helsig as Gardefüsilier Gottfried
*Vera Achilles as Cilly Posener
*Oscar Sabo as August Butsch, Wirt
*Carl Heinrich Worth as Prof. Hampel
*Renée Stobrawa as Adelheid Hampel, seine Frau
*Irmingard Schreiter as Erika von Rüdnitz, ihre Nichte Charles Francois as Kellner
*Illo Gutschwager as junger Kellner
*Max Hiller as  Diener bei Poseners
*Alfred Karen as  Gast im Kegellokal

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 