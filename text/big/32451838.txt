Cat Run
{{Infobox film
| name           = Cat Run
| alt            = 
| image          = Cat Run FilmPoster.jpeg
| caption        =  John Stockwell
| producer       = Bill Perkins Ram Bergman Derrick Borte
| writer         = Nick Ball John Niven
| starring       = Paz Vega Janet McTeer Alphonso McAuley Scott Mechlowicz Christopher McDonald Karel Roden D. L. Hughley Tony Curran Michelle Lombardo
| music          = Devin Powers
| cinematography = Jean-François Hensgens
| editing        = Ben Callahan
| studio         = Lleju Productions
| distributor    = Eagle Films
| released       =   }}
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $30,000 (US)   
}}
 John Stockwell.

== Plot == assassin Helen Bingham is hired to retrieve the HDD and kill Cat. Meanwhile, Americans Julian Simms and Anthony Hester decide to open a detective agency to raise money. When they read in the newspaper that Cat is wanted by the police, they decide to seek her out, expecting to receive a reward. However, they cross the path of Helen and they end protecting Cat from the killer. When Helen is betrayed by those who hired her, she decides to help Anthony, Julian, and Cat retrieve the HDD and their freedom.

== Cast ==
* Paz Vega as Catalina Rona
* Janet McTeer as Helen Bingham
* Alphonso McAuley as Julian Simms 
* Scott Mechlowicz as Anthony Hester
* Christopher McDonald as Bill Krebb
* Karel Roden as Carver
* D. L. Hughley as Dexter
* Tony Curran as Sean Moody
* Michelle Lombardo as Stephanie
* Heather Chasen as Binghams mum
* Branko Đurić as himself

== Production ==
Cat Run was shot in Serbia and Montenegro. 

== Release ==
Cat Run was released April 1, 2011.  It grossed $30,000 domestically.  Universal released it on home video on June 19, 2012.   

== Reception ==

Rotten Tomatoes, a review aggregator, reports that 14% of 14 surveyed critics gave the film a positive review; the average rating was 3.6/10.  Metacritic rated it 33/100 based on nine reviews.  John Anderson of Variety (magazine)|Variety called it "an often stylish but wearying action thriller that fails even to be convincingly tongue-in-cheek."  Todd McCarthy of The Hollywood Reporter called it "a self-consciously sleazy comic crime saga composed of facetious elements whose shelf life has long since passed."  Mark Olsen of the Los Angeles Times called it a return to "late-90s post-Tarantino crime thrillers" that are "cut-rate knockoffs" of Quentin Tarantinos style.  Scott Tobias of The A.V. Club rated it C+ and called it a "generic hip thriller" that is well-suited to late-night cable.  Gerard Iribe of DVD Talk rated it 1.5/5 stars and called it "the poor mans version of Smokin Aces".  Paul Pritchard of DVD Verdict wrote, "It lacks a unique voice, and though it does entertain in bursts, it is overlong and frequently guilty of lacking direction." 

==Sequel==

Cat Run was followed by a sequel in 2014, Cat Run 2, with Scott Mechlowicz and Alphonso McAuley reprising their roles as Anthony and Julian.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 