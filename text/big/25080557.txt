Scream for Help
 
{{Infobox film
| name           = Scream for Help
| image          = Scream for Help.jpg
| caption        =
| director       = Michael Winner Tom Holland
| narrator       =
| starring       = Rachael Kelly Corey Parker John Paul Jones
| cinematography = Robert Paynter
| editing        = Arnold Ross Michael Winner
| distributor    = Miracle Films
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Scream for Help is a 1984 film directed by Michael Winner.

==Plot==
A teenage girl discovers that her stepfather is trying to murder her and her mother, but when she tries to tell other people about it, no one will believe her.

==Reception==
The Monthly Film Bulletin criticized Scream for Help for what it described as the films "moronic characterization, daft dialogue, inept performances and opportunistic camerawork."   

==Soundtrack== John Paul Jones and released as a Scream for Help (album)|Soundtrack.

==Video release== Max Headroom and Maximum Overdrive. To this day, the film has never been released on either DVD or Blu-ray.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 


 