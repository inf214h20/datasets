Indonesia Calling
 
{{Infobox Film name           = Indonesia Calling image          =  caption        =  producer  Waterside Workers Federation director       = Joris Ivens  writer         = Joris Ivens (script) Catherine Duncan (commentary) narrator       = Peter Finch music          =  cinematography = Marion Michelle editing        = Joris Ivens distributor    =   released       = 1946 runtime        = 22 minutes country        = Australia language       = English  budget         = 
}} Waterside Workers Dutch ships independence movement. 
Ivens filming of the events taking place gradually became a symbol even for those who had not seen the film and had a growing following in the Netherlands, long before the film had an audience. Joris Ivens suffered persecution from his stance about the Dutch and Indonesia. Ivens had his Dutch passport seized by the Dutch authorities for a few months at a time in order to monitor his whereabouts. Finally in 1985 The Dutch government presented Ivens with a Golden Calf. At the ceremony the Dutch minister gave a speech and in his words, “Shortly after the war, your support for Indonesia’s right to self-determination and your film Indonesia Calling brought you into conflict with the Dutch government (…) I can now say that history has come down more on your side than on the side of your adversaries”. 

==References==
 
* 
*Hans Schoots, Living Dangerously: A Biography of Joris Ivens (Amsterdam: Amsterdam University Press, 2000)
*Doolan, Paul. "Joris Ivens and the Legend of Indonesia Calling." Thinkshop. N.p.. Web. 18 Apr 2014.  .

==External links==
* 
*  : From Colonial Film Commissioner to Political Pariah: Joris Ivens and the Making of Indonesia Calling by Drew Cottle and Angela Keys
* 
*   at National Film and Sound Archive
* 
* 
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 