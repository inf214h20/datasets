McTeague (film)
{{infobox film
| name           = McTeague (Lifes a Whirlpool)
| image          =
| imagesize      =
| caption        =
| director       = Barry ONeil
| producer       = William A. Brady (as William A. Brady Picture Plays)
| based_on       = Frank Norris (novel)
| writer         = Barry ONeil (scenario)  E. Magnus Ingleton (scenario)
| starring       = Holbrook Blinn Fania Marinoff
| distributor    = World Film Company
| released       = January 10, 1916
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}}

McTeague (aka Lifes a Whirlpool) is a 1916 American silent film drama directed by Barry ONeil, the first adaptation of Frank Norriss 1899 novel McTeague. The stars of the film were Holbrook Blinn and Fania Marinoff whose roles were played seven years later by Gibson Gowland and Zasu Pitts in the remake, Eric von Stroheims opus Greed (film)|Greed (1923). Blinn was already famous for playing brutal characters on the stage such as in Salvation Nell (1908). 

The film is now considered a lost film. 

==Cast==
*Holbrook Blinn as McTeague
*Fania Marinoff as Trina
*Walter Green as Marcus Schuller
*Philip Robson as Mr. Sieppe
*Julia Stuart as Mrs. Sieppe
*Rosemary Dean as Selina Sieppe
*Eleanor Blanchard as Maria Cappa

==See also==
*List of lost films

==References==
Notes
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 

 