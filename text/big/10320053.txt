Kaathala Kaathala
{{Infobox film
| name = Kaathala Kaathala
| image = Kaathala Kaathala DVD Cover.jpg
| caption = DVD Cover Rambha
| director =  Singeetam Srinivasa Rao
| writer = Crazy Mohan Kamal Haasan
| producer = P. L. Thenappan
| cinematography = Tirru
| editing = N P Satish
| studio = Saraswathi Films
| distributor = Rajkamal International
| released = 10 April 1998 Tamil
| music =  Karthik Raja
| budget = 
| awards =
}}
 1998 Kollywood|Indian Rambha in the lead roles with an ensemble cast in supporting roles. The film released on 10 April 1998, to positive reviews and became a commercial success.

== Plot ==
Ramalingam (Kamal Haasan) & Sundaralingam (Prabhu Deva) are simpletons who are always in search of ways to earn a quick buck. Sundaralingam is an artist with good talent while Ramalingam is the business brain who brings in money, mostly by fraudulent means. They fall in love  - Sundaralingam with Janaki (Rambha (actress)|Rambha) and Ramalingam with Sundari (Soundarya). Both are students of art.

Janakis father, played by the music director M. S. Viswanathan, rejects Sundaralingam. Sundari convinces Janaki to write letters falsifying Sundaralingams rise to fame and big money, eventually lying that Janaki & Sundaralingam now have a son, in order to win over the parents with sentiment. Janakis dad, worried that his grandson might be brought up poorly, arrives at a mansion that has been set up to look like Sundaralingams new property, along with her mom (Srividya). Meanwhile, Sundaris dad (Moulee) also makes an unexpected stop there to check on his daughter.

A small misunderstanding during introductions and progressively bigger lies to cover the initial lies lead to a side-splitting laugh riot. How those four eventually overcome their problems and find happiness forms the rest of the story.

==Cast==
*Kamal Haasan as Ramalingam
*Prabhu Deva as Sundaralingam
*Soundarya as Sundari Rambha as Janaki
*Vadivelu as Security Singaram
*M. S. Viswanathan as Paramasivam
*T. S. B. K. Moulee (Moulee) as Balamurugan
*Kovai Sarala as A Parambara Pichakaari
*Cho Ramaswamy as Varadachaari
*Srividya as Parvathy
*S. N. Lakshmi as Noorjahan
*Delhi Ganesh as Ramalingams and Sundaralingams landlord
*Ajay Rathnam as Williamson
*Cochin Haneefa as Vikadananda (V Anand)
*Crazy Mohan as Mohan
*Nagesh as Chokkalingam (Special Appearance)
*Vaiyapuri

==Production== Meena and Rambha being cast in a lead role.  Nagma also opted against signing the film fearing that a potential clash may arise with actress Rambha, after the pairs alleged fall out on the sets of Janakiraman.  After Soundaryas death in 2004, Kamal Haasan paid tribute by revealing that "she came forward to do the movie, when the rest of the industry was unwilling to work with me (Kamal Haasan)". 

==Release==
Critics from Indolink.com gave the film a positive review, citing that "if one can put the inspirations behind, the movie is quite enjoyable"  and that "the going gets very complicated and absolutely hilarious".  The film was later dubbed into Telugu as Navvandi Lavvandi and released in early 1999. 

In 2010, the producer of the film P. L. Thenappan threatened legal action against the makers of the Hindi film, Housefull (2010 film)|Housefull for remaking scenes from the film without permission. Thenappan revealed he had dubbed the film into Hindi in the late 1990s as Mirchi Masala, but the version did not release.  

==Soundtrack==
*Kaasumela - Kamal Haasan, Udit Narayan
*Laila Laila - Hariharan (singer)|Hariharan, Bhavatharini
*Madonna Paadala Nee - Kamal Haasan
*Thakida Thathom Annachi - Karthik Raja
*Saravana Bhava - Kamal Haasan, Karthik Raja, Sripriya, Sujatha
*Anaconda - Kamal Haasan, Bhavatharini
*Anaconda II - Hariharan (singer)|Hariharan, Bhavatharini

==References==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 