The Gamblers (2007 film)
{{Infobox film
| name = The Gamblers
| image = The Gamblers.jpg
| caption = Theatrical release poster
| director = Sebastian Bieniek
| producer = Sebastian Bieniek and Hartmut Bitomsky
| writer = Sebastian Bieniek
|´starring = Fritzi Malve Voss (Frederike Nass), Thomas B. Hoffmann, Sebastian Bieniek
| music = Zarko Jovasevic
| cinematography = Camilo Sottolichio and Vojtech Pokorny
| editing = Sebastian Bieniek
| released =  
| runtime = 86 minutes
| country = Germany
| language = German
| budget = 2000 €
}}
The Gamblers ( ) is a 2007 German  .
 The Gambler by Fyodor Mikhailovich Dostoevsky, although the film follows more a love story plot than a gambling addiction plot.

== Production ==
The Gamblers was made as a no-budget production in the Deutsche Film- und Fernsehakademie Berlin (DFFB, German Film and Television Academy Berlin) in a seminar taught by Hungarian director Béla Tarr.

It was shot on the Teufelsberg (German for Devils Mountain), a hill in Berlin that was built from the rubble of Berlin (400,000 buildings) after the Second World War. This is the only location in the film.

== Story ==
In the midst of a world — to an outsider, impenetrable — the house tutor of the deserted General, Alexej Iwanovitsch, once ruled by
an uncontrollable addiction to gambling, falls in love with his employer’s stepdaughter Polina.
Alexej understands very quickly that he has entered a world ruled by monetary worries and dreams and that, offhand, he is not Polina’s perfect suitor.
To him it seems that not only his social position, but also his self-imposed ban from the casino, stand in his way.

== Festivals ==
2007
*10th Shanghai International Film Festival (international competition)
*German film week Nanjing (official selection)
*31st Cairo International Film Festival (festival of festivals)
2008
*10th Mumbai International Film Festival (international competition)
*10th Int.Short and Ind. Film Festi. Dhaka  (international Panorama)
*XXVI Uruguay International Film Festival (international competition)
*6th Non-Budget International Film Festival (6to. Festival Internacional Del Cine Pobre) (Gibana, Cuba) (international competition)
*Miskolc Cinefest International Film Festival (international competition).
*Valdivia International Film Festival (international competition).
*5th Abuja International Film Festival (international competition).
*III Didor Dushanbe Film Festival (international competition).
*Start International Student Film Festival Baku (international competition).

== References ==
10th Shanghai International Film Festival

==External links==
*  
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 