Queen of Hearts (1936 film)
{{Infobox film
  | name = Queen of Hearts
  | image =
  | caption =
  | director = Monty Banks
  | producer = Basil Dean
  | writer = Douglas Furber Gordon Wellesley Clifford Grey Anthony Kimmins H.F. Maltby John Loder Enid Stamp-Taylor   Fred Duprez
  | music = Ernest Irving
  | cinematography = John W. Boyle
  | editing =
  | studio = Associated Talking Pictures  Associated British
  | released = 5 October 1936
  | country  = United Kingdom
  | runtime = 78 minutes
  | language = English
  | budget =
  }} 1936 Cinema British musical musical comedy John Loder and Enid Stamp-Taylor. It was made at Ealing Studios. 

==Plot summary==
Grace Perkins (Gracie Fields) is an ordinary working class seamstress who is mistaken as a rich patron of the arts. When shes asked to back a new show she plays along with the charade, hoping that she can become the productions leading lady. When the show opens Grace is a huge hit and goes on to become a glamorous star.

==Cast==
* Gracie Fields as Grace Perkins John Loder as Derek Cooper
* Enid Stamp-Taylor as Yvonne
* Fred Duprez as Zulenberg
* Edward Rigby as Perkins
* Julie Suedo as Rita Dow
* Jean Lester as Mrs. Perkins
* Hal Gordon as Stage Manager
* Syd Crossley as Constable
* Madeline Seymour as Mrs. Vandeleur
* H.F. Maltby as Solicitor
* Margaret Yarde as Mrs. Porter

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
 

 

 
 
 
 
 
 
 
 


 