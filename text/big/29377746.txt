Loved (film)
{{Infobox film
| name           = Loved
| director       = Erin Dignam
| writer         = Erin Dignam Robin Wright Penn  William Hurt  Amy Madigan  Paul Dooley  Lucinda Jenney  Joanna Cassidy
| released       = April 1997
| runtime        = 109 minutes
| country        = United States
| language       = English
}}
 Robin Wright Penn and William Hurt

== Plot ==
After a husband is accused of droving his third wife to suicide, his first wife Hedda, a troubled woman who cant hate or hurt others even if they had wronged her, is subpoenaed to testify on his abusive behavior during their marriage.

== Cast ==
 
  Robin Wright Penn as Hedda Amerson
* William Hurt as K.D. Dietrickson
* Amy Madigan as Brett Amerson
* Anthony Lucero as The Defendant
* Paul Dooley as Leo Amerson
* Lucinda Jenney as Kate Amerson
* Joanna Cassidy as Elenore Amerson
 

== See also ==
 
 
*1997 in film
*List of American films of 1997
*List of drama films
*List of thriller films of the 1990s
 
 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 


 
 