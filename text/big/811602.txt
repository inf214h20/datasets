He's a Woman, She's a Man
 
 
 
{{Infobox film
|image=Hes a Woman, Shes a Man.jpg
|caption=Hes a Woman, Shes a Man DVD cover
|traditional=金枝玉葉
|simplified=金枝玉叶
|pinyin=Jīn Zhī Yù Yè
|jyutping=Gam1 Zi1 Juk6 Jip6
|director=Peter Chan
|producer=Eric Tsang
|starring=Leslie Cheung Anita Yuen Carina Lau Eric Tsang Jordan Chan
|music=Clarence Hui Tsui Tsang-hei Henry Chan
|editing=Chan Ki-hop
|studio=United Filmmakers Organisation
|distributor=Mandarin Films Distribution Co. Ltd.
|released= 
|runtime=107 minutes
|country=Hong Kong
|language=Cantonese
|gross=HK$29,131,628
}}

Hes a Woman, Shes a Man (金枝玉葉 or Gam Chi Yuk Yip) is a 1994 film directed by   in 1996. The theme song is Chase (追) sung by Leslie Cheung (Most Beloved album, 1995).

==Plot==

Wing (Anita Yuen) is a sassy girl who deeply idolizes Rose (Carina Lau), a pop singer, and Roses boyfriend, top record producer and song-writer Sam (Leslie Cheung). Rose has been groomed by Sam, achieving stardom and international acclaim. With her success, Sam decides to try his hand at bringing up a male singer.  He decides to announce a country-wide, males-only talent search, much to his former protegées chagrin.

Wing, desperate to meet her idols, seizes this opportunity and enters the contest disguised as a male. Her childhood friend (Jordan Chan) trains her to perfect this whimsical idea.

As fate would have it, Wing succeeds in the audition thanks to the poor quality of those auditioning and in part to Roses taunts. Rose challenges Sam to see to the idea of him bringing up a (relatively) talentless Wing.

Wing is later invited to stay at Sam and Roses home for  her/"his" musical education.  Wing tries unsuccessfully to reconcile the two lovers difference. Trouble and comedy ensue as she finds herself falling for Sam and vice versa, despite him thinking she is a he.

==Cast==
* Leslie Cheung - Sam Koo Gai Ming
* Carina Lau - Rose
* Anita Yuen - Lam Chi Wing
* Eric Tsang - Auntie
* Jordan Chan - Yu Lo
* Lawrence Cheng - Charles Chow Chu
* Law Kar-ying - Joseph (as Ka-ying Lo)
* Jerry Lamb - Sams Assistant
* Pete Spurrier - Wine bar patron (uncredited)

==Awards and nominations==
* Won: Hong Kong Film Award for Best Actress (Anita Yuen)
* Won: Hong Kong Film Award for Best Original Film Song (Lesile Cheung)
* Nominated: Hong Kong Film Award for Best Actor (Lesile Cheung)
* Nominated: Hong Kong Film Award for Best Art Direction (Chung Man-kei)
* Nominated: Hong Kong Film Award for Best Costume & Make Up Design (Dora Ng)
* Nominated: Hong Kong Film Award for Best Director (Peter Chan)
* Nominated: Hong Kong Film Award for Best Film (Eric Tsang)
* Nominated: Hong Kong Film Award for Best Screenplay (Lee Chi-ngai, James Yuen)
* Nominated: Hong Kong Film Award for Best Supporting Actor (Jordan Chan)
* Nominated: Hong Kong Film Award for Best New Performer (Jordan Chan)
==See also==
* Cross-dressing in film and television

==External links==
*  
*  

 

 
 
 
 