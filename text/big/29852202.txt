The Flying Doctor
 
 
 
{{Infobox film
| name           = The Flying Doctor
| image          =
| image_size     =
| caption        =
| director       = Miles Mander
| producer       =
| writer         = J.O.C. Orton Miles Mander
| based on = novel by Robert Waldron
| narrator       =
| starring       = Charles Farrell Mary Maguire
| music          = Willy Redsone Alf. J. Lawrence
| cinematography = Derick Williams
| editing        = J.O.C. Orton R. Maslyn Williams Edna Turner
| studio         = National Productions Gaumont British|Gaumont-British Pictures
| distributor    =20th Century-Fox (Aust)
| released       = 23 September 1936 (Aust) Sept 1937 (UK)
| runtime        = 92 min. (Aust) 67 min. (UK)
| country        = Australia
| language       = English
| budget         = £45,000 
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Flying Doctor is a 1936 Australian-British drama film directed by Miles Mander and starring Charles Farrell, Mary Maguire and James Raglan.  The Royal Flying Doctor Service of Australia operate in the Australian Outback. Australian cricketer Don Bradman appears as himself during the film.

==Plot==
On his wedding night, Sandy Nelson decides to abandon his young bride, Jenny and to go work in Sydney as a painter on the Habour Bridge. He befriends a doctor, John Vaughan, who is in love with a married woman. Vaughan decides to accept a job as flying doctor in the outback.

Sandy gets in a brawl at a cricket match, serves time in prison, then heads for the outback and discovers gold. He is shot in a bar room fight and loses his eyesight. He then discovers Vaughan has fallen in love with Jenny, his former bride. When he realises Jenny loves Vaughan, Sandy decides to commit suicide, leaving his fortune to the Flying Doctors.

==Cast==
* Charles Farrell – Sandy Nelson
* Mary Maguire – Jenny Rutherford
* James Raglan – Doctor John Vaughan
* Joe Valli – Dodger Green
* Margaret Vyner – Betty Webb
* Eric Colman – Geoffrey Webb
* Tom Lurich – Blotch Burns
* Maudie Edwards – Phyllis
* Katie Towers – Mrs. OToole
* Phillip Lytton – Doctor Gordon Rutherford
* Andrew Beresford – John Rutherford
* Jack Clarke – Pop Schnitzel
* Phil Smith – Barman Joe
* Donald Bradman – Himself

==Production==

===National Productions===
The movie was the first and only production from National Productions, a new Australian film production company which was formed in the 1930s under the management of Frederick Daniell, a promoter involved with radio and newspaper companies in Sydney. Amongst its directors were Sir Hugh Denison, Sir Samuel Walder and Sir James Murdoch. 

The company was closely associated with National Studios Ltd, which built a large studio complex in Pagewood Studios|Pagewood, Sydney. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 172.  It was incorporated in September 1935 with capital of £50,000. 

===Pre-production===
National Productions had links to the British company, Gaumont British, which had been interested in making a film in Australia for a long time, with Robert Flaherty intending to shoot one.  Gaumont provided technical and financial support for the company.

Gaumont British provided several personnel for the film, including the director, writer, cinematographer, unit manager and sound recordist.  National Productions also hired Englishman Errol Hinds to be head of the camera department for two years. 

The British unit arrived in November 1935.   In December, American star Charles Farrell was signed to play the lead.  He did not arrive until late January 1936. 

Cricket star Don Bradman was signed to make a cameo. 

===Shooting===
Shooting began in 1936 with bad weather helping the budget increase. The film was shot at National Studios Pagewood facility.

Director Miles Mander left for Hollywood in March 1936, leaving J.O.C. Orton to finish the film. 

==Reception==
Mary Maguire lived in Brisbane, so it was decided to hold the films international premiere there.  20th Century-Fox agreed to distribute the film free of charge.  Box office receipts were poor but the release of the film led to a flood of donations to the flying doctors.  Reviews were patchy. 

Gaumont British decided not to distribute the film in the UK and it was done by General Film Distributors. The movie was never released in the USA.

National Productions had prior to shooting announced intention to make three more movies but none of these eventuated. 

==Preservation status== lost until oxy torch – somehow avoiding igniting the highly flammable nitrate film inside – and loaded a truck with the contents to take away for disposal.    An office worker saw the truck drive by, loaded with film cans, gave chase in his car, and rescued the film, which included the first eight of nine reels of The Flying Doctor.  Two years later, the National Film Archive in London found a copy of the shortened, re-edited British release of the film, also eight reels long, in the possession of a large film company.  Despite this print having been "totally rearranged", the last reel was found to take up exactly where the Australian one left off. 

==References==
 

==External links==
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 
 
 