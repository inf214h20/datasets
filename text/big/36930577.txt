Sleepy Lagoon (film)
 Sleepy Lagoon}}
 
{{Infobox film
| name           = Sleepy Lagoon
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Joseph Santley
| producer       = 
| writer         = 
| screenplay     = Frank Gill. Jr. George Carleton Brown
| story          = Prescott Chaplin
| based on       = 
| narrator       = 
| starring       = Judy Canova Dennis Day Ruth Donnelly
| music          = Mort Glickman (uncredited) Walter Scharf (uncredited) Marlin Skiles (uncredited)
| cinematography = Bud Thackery Richard Van Enger
| studio         = 
| distributor    = Republic Pictures
| released       =  
| runtime        = 65 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 musical comedy film directed by Joseph Santley and featuring comedienne Judy Canova and singer Dennis Day. The film was written by Prescott Chaplin, while Frank Gill, Jr. and George Carleton Brow wrote the screenplay.  
 song which lent its name to that incident.

==Overview==
The story deals with the travails of a newly elected female mayor, Judy Joyner (Canova), in a growing and mildly corrupt small town.  Horror film actor Rondo Hatton had a bit part as "Hunchback."

==Cast==
* Judy Canova as Judy Joyner
* Dennis Day as Lancelot Hillie
* Ruth Donnelly as Sarah Rogers
* Joe Sawyer as Lumpy
* Ernest Truex as Dudley Joyner
* Douglas Fowley as J. "The Brain" Lucarno Will Wright as Cyrus Coates
* Forrest Taylor as Samuel
* Kitty McHugh as Mrs. Small Eddie Chandler as Ticket Seller
* Herbert Corthell as Sheriff Bates
* Ellen Lowe as Mrs. Simms
* Jack Raymond as Joe, the Clown
* Margaret Reid as Mrs. Crumm
* Mike Riley as Bandleader
* Rondo Hatton as Hunchback (uncredited)

== External links ==
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 

 