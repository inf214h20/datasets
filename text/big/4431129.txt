Butterfly on a Wheel
 
{{Infobox film
| name           = Shattered: Butterfly on a Wheel
| image          = Butterflyonawheel1.jpg
| image_size     =
| caption        = Mike Barker William Morrissey Pierce Brosnan
| writer         = William Morrissey
| narrator       =
| starring       = Pierce Brosnan Gerard Butler Maria Bello Callum Keith Rennie Robert Duncan
| cinematography = Ashley Rowe Bill Sheppard
| distributor    = Lions Gate Entertainment (USA) Icon Entertainment (non-USA) Freestyle Releasing 
| released       =  
| runtime        =
| country        = Canada United Kingdom
| language       = English
| budget         = $20 million   
}}
Butterfly on a Wheel (US: Shattered, Europe: Desperate Hours) is a 2007  ” The line is usually interpreted as questioning why someone would put great effort into achieving something minor or unimportant, or who would punish a minor offender with a disproportional punishment. 

==Plot==
Chicago residents Neil Randall (Gerard Butler) and his wife, Abby Randall (Maria Bello) have the perfect life and a perfect marriage. With their beautiful young daughter, Sophie, they are living the American dream... until today. When Sophie is suddenly kidnapped, they have  no choice but to comply with the abductors demands. The kidnapper, Tom Ryan (Pierce Brosnan), a cold and calculating Aspd|sociopath, takes over their lives with the brutal efficiency of someone who has nothing to lose. 

In the blink of an eye, Neil and Abbys safe and secure existence is turned upside down. Over the next twenty-four hours they are at the mercy of a man who wants only one thing: that they do his bidding. It soon becomes clear that Ryans demands are all the more terrifying because he doesnt want their money. What he wants is Neil and Abbys life, the life they have built over 10 years, to be systematically dismantled and destroyed, piece by piece. 

With time running out on their little girl, Neil and Abby realize their nightmare is just about to begin. They will have to submit to Ryans challenges over the next 24 hour period.  How far will they go to save the life of their child? They are asked to withdraw money from the bank which Tom burns and throws along with their wallets out of the car. They have to get $300 from nowhere in a part of the town where they dont have any friends. Abby pledges her bracelet and Neil his watch which gets them the $300. They are then asked to deliver a courier to some place within 20 min, and Tom reveals to Neil that the cover contains dirty details of Neils job, which if leaked will ruin Neil. This goes on, with efforts by them to rescue Sophie from the Hotel, only to get caught by Tom, who makes Abby strip and change in front of them, etc.

The denouement is revealed only in the last few minutes. Neil and Toms wife (Judy) are colleagues at work and have been having an affair for a while: and Tom has come to know of it. On that day, Neil and Judy were planning to meet for a rendezvous. Tom wants to put Neil through the pain he had undergone and surprisingly Abby plays along with his plan. 

Neil lies to Abby when they are returning home that someone else was having an affair with Judy and Tom mistook Neil to be that person, which is why Tom had tormented them the whole day. Abby reveals the details at the very end to Neil, that in truth their daughter has not been kidnapped, she knows of the affair between Neil and Judy, the cover delivered contained only blank sheets, etc. Abby is paying back Neil the pain she went through on learning of the affair and played the game along with Tom.

==Cast==
*Pierce Brosnan - Tom Ryan
*Gerard Butler - Neil Randall
*Maria Bello - Abby Randall
*Callum Keith Rennie - Detective McGrath
*Dustin Milligan - Matt Ryan
*Claudette Mink - Judy Ryan

==Production==
Pierce Brosnan joined the films cast in late 2005.  Maria Bello and Gerard Butler joined production on 19 January 2006. 

Filming began in February 2006 and finished the following May.  Vancouver stands in for Chicago but the production shot in the latter city for landscapes before moving to the United Kingdom for post-production.  Filming also took place in Los Angeles. 

==Remake==
The 2010 Indian Malayalam film Cocktail (2010 film)|Cocktail is an uncredited remake of Butterfly on a Wheel.  

The 2014 Indian Tamil film Athithi (2014 film)|Athithi is also an uncredited remake of Butterfly on a Wheel. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 