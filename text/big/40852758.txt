Hackers (film)
{{Infobox film
| name           = Hackers
| image          = Hackersposter.jpg
| caption        = Poster design by Dawn Patrol 
| director       = Iain Softley
| producer       = {{plainlist|
* Michael Peyser
* Ralph Winter }}
| writer         = Rafael Moreu
| starring       = Angelina Jolie  Jonny Lee Miller 
| music          = Simon Boswell
| cinematography = Andrzej Sekuła
| editing        = {{plainlist|
* Chris Blunden Martin Walsh }}
| distributor    = United Artists 
| released       = September 15, 1995
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         =  
| gross          = $7,563,728 (US) 
}}
 science fiction thriller film hackers and their involvement in a corporate extortion conspiracy. Made in the 1990s when the internet was unfamiliar to the general public, it reflects the ideals laid out in the Hacker Manifesto quoted in the film, "This is our world now... the world of the electron and the switch   We exist without skin color, without nationality, without religious bias... and you call us criminals.   Yes, I am a criminal. My crime is that of curiosity." Hackers has achieved cult classic status. 

==Plot==
In 1988, 11-year old Dade "Zero Cool" Murphy is arrested and charged with crashing 1,507 computer systems in a single day and causing a single-day 7-point drop in the New York Stock Exchange. Upon conviction, his family is fined $45,000 for the events, and although he is not incarcerated, he is banned from owning or operating computers or touch-tone telephones until he is 18 years old.
 social engineering, The Outer handle "Acid Burn") on the same network, and they briefly converse. During the exchange, Dade identifies himself by a new alias, Crash Override, rather than reveal himself as "Zero Cool".

Dade enrolls in a fictional local high school (for which Stuyvesant High School is used as the filming location), where he meets Kate Libby (Angelina Jolie), who is assigned to take him on a tour of the school. After pranking Dade by claiming theres a pool on the roof of the school, only to lock him outside with several other students during a rainstorm, a rift develops between Dade and Kate. Ramon "The Phantom Phreak" Sanchez (Renoly Santiago) observes Dade accessing the school mainframe during computer class to put himself in the same English class as Kate, and invites him to a hacker nightclub, Cyberdelia, where the rivalry between Dade and Kate intensifies as Dade beats Kates high score in the Wipeout (video game)|Wipeout arcade game.

Soon after, Dade exacts revenge for the earlier prank by scheduling a test of the schools sprinkler system the next day. Dade begins integrating himself into Phreaks circle of hacker friends: Emmanuel "Cereal Killer" Goldstein (Matthew Lillard), Paul "Lord Nikon" Cook (Laurence Mason) (so named for his photographic memory), and Joey Pardella (Jesse Bradford), an aspiring novice hacker without an alias. At a party, Dade learns that Kate is "Acid Burn", the hacker that earlier kicked him out of the TV network.
 fragmented file. IT employee Hal (Penn Jillette) detects this unauthorized entry and summons computer security officer Eugene "The Plague" Belford (Fisher Stevens), a former hacker, to deal with the problem.
 US Secret Service to recover the file, claiming it is the code to a second computer virus (named "Da Vinci" for an image of the Vitruvian Man that accompanies it) that Plague inserted as a red herring that will capsize the companys oil tanker fleet.

Soon after, Joey is arrested and his computer is confiscated and searched, but the Secret Service finds nothing in the software, as Joey has hidden the disk containing the files. In response, Dade and Kate decide to settle their disagreements regarding the others computing supremacy with a bet in the form of a hacking duel, with Dade choosing a date with Kate as his prize; Kate electing to have Dade perform menial computing tasks. The hacking duel focuses on harassing Secret Service Agent Richard Gill (Wendell Pierce), "Hacker enemy number one", who was involved in Joeys arrest. Phreak, Cereal, and Nikon act as judges. After various pranks including canceling Gills credit cards, creating a fake embarrassing personal ad in Gills name, fabricating a criminal record, and changing his payroll status to "deceased", the duel remains in a tie status.

After being released on parole, Joey reveals the disk to Phreak in a public park; but they quickly realize that they are being followed by the Secret Service. The next day, Phreak is arrested. He uses his phone call to inform Kate that he hid the disk in a boys bathroom at school. That evening, Kate and Cereal Killer ask Dade for his help; but, stating he has "a record" before, declines. Kate then asks Dade to copy the disk so that, if anyone else is arrested, they have the disk as un-tampered evidence. After determining that Dade is not the one who hacked into Ellingson, The Plague attempts to enlist Dades help to find the one who did. First, he sends Dade a high-powered laptop, more powerful than his old computer, that upon start-up displays a video message from The Plague encouraging Dade to join forces with him. Later, he threatens to have Dades mother incarcerated with a manufactured criminal record, which would then send him on the street. At this, Dade agrees to deliver Kates copy of the disk.

Meanwhile, Kate, Lord Nikon, and Cereal Killer attempt to discern the contents of the disk. Dade joins them; and, after working all night, they learn the purpose of its code—a worm designed to Salami slicing|salami-slice $25 million from Ellingson transactions. Dade confesses that he knows Plague is behind this scheme, because he was the one who wanted Kates copy of the disk. He admits he gave Plague the disk. When they confront him about his "record", he reveals his hacking history as "Zero Cool". Lord Nikon and Cereal display great reverence and excitement finding out that Dade is Zero Cool, since he became "a hacker legend" after his 1988 hacking. Kate is nonplussed, as their futures are still at risk. Determined to stop the scheme, the assembled hackers plan to hack the Gibson again. Kate and Dade go dumpster-diving for employee memos with passwords; Cereal Killer installs a phone tap in the Ellingson offices; and Nikon poses as a delivery boy wandering the Ellingson cubicles, memorizing employee passwords as they enter them into their terminals.

Reading the memos, they discover that the Da Vinci virus is set to capsize the oil fleet the next day, which would provide the perfect cover to distract from the salami-slicing worm. In need of help, Dade and Kate seek out Razor and Blade, the producers of a hacker-themed pirate TV show, "Hack the Planet." Razor and Blade are at a club where Urban Dance Squad is performing and after Dade and Kate gain access to their space, they manage to convince Razor and Blade to join them in disrupting The Gibson enough that the garbage file can be located and copied. Lord Nikon and Cereal Killer learn through their Ellingson phone tap that warrants for their arrest are to be executed at 9AM the next day, and have to hurry up before that time.
 paged by Grand Central station, where they use payphones and acoustic couplers to begin their assault on the Gibson. At first, their attempts are easily rebuffed by Plague, who calls Dade to taunt him to escape before he is arrested. However, Razor and Blade have contacted hackers around the world, who lend their support with virus attacks, hampering the Gibson and distracting Plague long enough for Dade to download the incriminating file to a floppy disk.

Shortly after crashing the Gibson, Dade and company are arrested. As theyre being led away, Dade surreptitiously informs Cereal Killer, hiding in the crowd, that hes tossed the disk in a trashcan. As Dade and Kate are being interrogated, Razor and Blade jam the local television signals and broadcast live video of Cereal Killer, revealing the plot and Plagues complicity, along with the account number with the stolen funds. Plague is arrested while attempting to flee to Japan under the alias "Mr. Babbage" (itself an obscure reference to Charles Babbage).  Their names cleared, Dade and Kate go on a date at a swimming pool on the roof of a building, their friends showing off their latest hack—the lights in several adjacent office buildings spelling out "CRASH AND BURN." Confirming to each other that they have had the same weird dream, they begin to make out.

==Cast==
*Jonny Lee Miller as Dade Murphy (a.k.a. Zero Cool, a.k.a. Crash Override)
*   (1995), she had to turn it down.  . IMDb. Retrieved 2010-09-06. 
*Renoly Santiago as Ramόn Sánchez (a.k.a. The Phantom Phreak) pen name of) the publisher of 2600 magazine, which is in turn a homage to Emmanuel Goldstein from the novel Nineteen Eighty-Four.
*Laurence Mason as Paul Cook (a.k.a. Lord Nikon)
*Jesse Bradford as Joey Pardella
*Fisher Stevens as Eugene Belford (a.k.a. The Plague)
*Lorraine Bracco as Margo Wallace
*Alberta Watson as Lauren Murphy
*Penn Jillette as Hal
*Wendell Pierce as U.S. Secret Service Special Agent Richard Gill
*Marc Anthony as U.S. Secret Service Special Agent Ray
*Michael Gaston as U.S. Secret Service Special Agent Bob
*Felicity Huffman as Prosecuting Attorney
*Darren Lee as Razor
*Peter Y. Kim as Blade
*Max Ligosh as Young Dade Murphy

==Production==

===Screenplay===
The  . There, he met Phiber Optik, a.k.a. Mark Abene, a 22-year-old hacker who spent most of 1994 in prison on hacking charges.  Moreu also hung out with other young hackers being harassed by the government and began to figure out how it would translate into a film. He remembered, "One guy was talking about how hed done some really interesting stuff with a laptop and payphones and that cracked it for me, because it made it cinematic".  The character Eugene Belford uses Babbage as a pseudonym at the end of the film, a reference to Charles Babbage, an inventor of an early form of the computer. The fictional computer mainframe named the "Gibson" is a homage to cyberpunk author William Gibson and originator of the term "Cyberspace", first in his 1982 short story Burning Chrome and later in his 1984 book Neuromancer.

===Pre-production===
The cast spent three long weeks getting to know each other and learning how to type and rollerblade. They studied computers and met with actual computer hackers.    Actor Jonny Lee Miller even attended a hackers convention.   

===Shooting=== East Village neighborhoods of Manhattan in November 1994. Many scenes included real school seniors as extras.  

===Post-production=== CGI for motion control, computer graphics alone can sometimes lend a more flat, sterile image."  Sonys SCE Studio Liverpool created the CGI for the Wipeout (video game)|Wipeout arcade game sequence. 

Shortly after the filming ended, Jonny Lee Miller and Angelina Jolie were married, and after divorcing, remain good friends.

===Marketing===
MGM/UA set up a website for Hackers that soon afterwards was allegedly hacked by a group called the "Internet Liberation Front." A photograph of the films stars Angelina Jolie and Jonny Lee Miller were doodled upon, and the words "this is going to be an entertaining fun promotional site for a movie," were replaced with "this is going to be a lame, cheesy promotional site for a movie!" The studio maintained the site during the theatrical run of the movie in its altered form. Hackers MGM DVD 8-page booklet featuring trivia, production notes and a revealing look at the making of the film.   

The movie poster shows Acid Burn and Crash Override with various words and ASCII symbols transposed on their faces, with the words:
* Hacker names from the movie, including Lord Nikon, Acid Burn, and Crash Override.
* Most commonly used passwords, noted by Plague, such as God, Sex, Love, and Secret.
* Phreak, a "phone freak" hacker whose specialty is telephone systems, with the main Phreaker in the hacker group Phantom Phreak.

==Soundtrack== tribal rhythms Underworld and Orbital (band)|Orbital. Acclaimed with 4.4 of 5 stars from 54 reviewers,  it was released in 3 separate volumes over three years. The first volume was composed entirely of music featured in the film (with the exception of Carl Coxs "Phoebus Apollo"), while the second and third are a mix of music "inspired by the film" as well as music actually in the film. The most featured song in the movie is "Voodoo People" by The Prodigy.

Most of the music in the film, however, including much of the techno and electronic music, was composed and performed by UK film composer Simon Boswell. 

===Hackers===
# "Original Bedroom Rockers" – Kruder & Dorfmeister Underworld
# "Voodoo People" – The Prodigy Open Up" – Leftfield (featuring John Lydon)
# "Phoebus Apollo" – Carl Cox Josh Abrahams Halcyon and Orbital
# "Communicate" (Headquake Hazy Cloud Mix) – Plastico One Love" – The Prodigy
# "Connected (Stereo MCs song)|Connected" – Stereo MCs
# "Eyes, Lips, Body" (Mekon Vocal Mix) – Ramshackle
# "Good Grief" – Urban Dance Squad
# "Richest Junkie Still Alive" (Sank Remix) – Machines of Loving Grace Heaven Knows" Squeeze

===Hackers 2: Music from and Inspired by the Original Motion Picture Hackers===
# "Firestarter (The Prodigy song)|Firestarter" (Empirion mix) – The Prodigy
# "Toxygene" – The Orb Little Wonder" (Danny Saber Dance Mix) – David Bowie Scooter
# "Narcotic Influence 2" – Empirion BT
# "Go (Moby song)|Go" – Moby
# "Inspection" (Check One) – Leftfield Underworld
# "To Be Loved" (Disco Citizens R&D Edit)   – Luce Drayton Orbital
# "Get Ready to Bounce" (Radio Attack) – Brooklyn Bounce Off Shore" Chicane
# "Original (Leftfield song)|Original" – Leftfield

===Hackers 3===
# "Why Cant It Stop" – Moby BT
# Fluke
# "Quiet Then" – Cloak Monkey Mafia
# "Phuture 2000" (radio edit) – Carl Cox Orbital
# "Fashion" (Ian Pooley Mix) – Phunky Data
# "Psychopath" (Leftfield Mix) – John Lydon Cirrus
# Chicane
# "Hack the Planet" – Brooklyn Bounce
# "Diskette" – Simon Boswell
# "Launch Divinci" – Simon Boswell

===Additional information===
Songs featured in the film but not appearing on any soundtracks:

* "Connection (Elastica song)|Connection" – performed by Elastica Real Wild Child" – written by Johnny OKeefe, Johnny Greenan and Dave Owen (VIII) (as Dave Owens)
* "Protection (Massive Attack song)|Protection" – performed by Massive Attack
* "Combination" – performed by Guy Pratt
* "Grand Central Station" – performed by Deep Cover

==Reception== Indiana Jones".    On the show Siskel & Ebert, Ebert gave the film thumbs up while Gene Siskel gave the film thumbs down, saying, "I didnt find the characters that interesting and I really didnt like the villain in this piece. I thought Fisher Stevens was not very threatening... The writing is so arch". 

In his review for the San Francisco Chronicle, Peter Stack wrote, "Want a believable plot or acting? Forget it. But if you just want knockout images, unabashed eye candy and a riveting look at a complex world that seems both real and fake at the same time, Hackers is one of the most intriguing movies of the year".   
 Pump Up the Volume, as well as its casting prowess".    In his review for the Toronto Star, Peter Goddard wrote, "Hackers joy-rides down the same back streets Marlon Brando did in The Wild One, or Bruce Springsteen does in Born To Run. It gives all the classic kicks of the classic B-flicks, with more action than brains, cool hair and hot clothes, and all the latest tech revved to the max".   

Chicago Reader critic Jonathan Rosenbaum noted that, "Without being any sort of miracle, this is an engaging and lively exploitation fantasy-thriller about computer hackers, anarchistic in spirit, that succeeds at just about everything "The Net" failed to—especially in representing computer operations with some visual flair." 

The Los Angeles Times David Kronke wrote, "imagination of Rafael Moreu, making his feature screenwriting debut, and director Iain Softley...piles on the attitude and stylized visuals, no one will notice just how empty and uninvolving the story really is".      In his review for the Washington Post, Hal Hinson wrote, "As its stars, Miller and Jolie seem just as one-dimensional—except that, in their case, the effect is intentional".    Entertainment Weekly gave the film a "D" rating and Owen Gleiberman wrote, "the movie buys in to the computer-kid-as-elite-rebel mystique currently being peddled by magazines like Wired (magazine)|Wired".   

The film has a metascore of 46 by critics on Metacritic  and a 34% rating on Rotten Tomatoes based on 41 reviews. 

==DVD==
Hackers was released to DVD by MGM Home Video on August 25th, 1998 as a Region 1 widescreen DVD and on October 5th, 2010 as part of the 4 movie, 2-disc set 4 Sci-Fi Movies. 

==See also==
*List of films featuring surveillance
*Cyberpunk

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 