Islands (film)
 
{{Infobox film
| name           = Islands  Isole 
| image          = Islands (film).jpg
| caption        =
| director       = Stefano Chiantini
| producer       = Sada Selvaggia
| writer         = Stefano Chiantini
| starring       = Asia Argento Giorgio Colangeli Ivan Franek
| music          = Piernicola Di Muro
| cinematography = Vladan Radovic
| editing        = Luca Benedetti
| distributor    = Ellipsis Media International, Italy
| released       =  
| runtime        = 92 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Islands (  released in 2011, directed by Stefano Chiantini and starring Asia Argento.

The film was presented in world première at the Toronto International Film Festival 2011,  then at the Festival de Cine Italiano in Madrid,  at the London Italian Film Festival 2012  and at the Lavazza Italian Film Festival 2012 in Sydney.

==Plot==
The first scenes of the film are set in the historical center of the town of Termoli. A young illegal immigrant called Ivan is looking for a job. He is a bricklayer and lives in a small flat with his old and sick father. Precisely in with the intention to find a job he sets off to the Isole Tremiti, Apulia, but cannot not return as planned and had to extend his sojourn. There he meets two people who are going to help him. One is Don Enzo, an uncommon priest who lives in a small house and takes care of his bees. The other is Martina, a young woman who, since her daughters death, has closed herself in silence, and lives in Don Enzos house, her former tutor. Martina and Ivan become friends, and even don Enzo starts to feel some sympathy for the youngster.
 characters the director underscores the difference that often generates loneliness but may also bring people closer to one another. In a time where migration is a worldwide phenomenon Chiantinis analysis is interesting.

That description of the social relationship is completed by a last character, Father Enzos sister. She is a selfish and greedy woman who, from the first instant, distrusts the young foreigner mainly because she is interested in the property of the Father. She symbolises the world around which is united against Ivan and Martina.

==Cast==
*Asia Argento as Martina
*Giorgio Colangeli asDon Enzo
*Anna Ferruzzo as Vilma, Don Enzos sister
*Ivan Franek as Ivan
*Pascal Zullino: Rocco, Vilmas friend
*Eugenio Krauss as the Violent Man

==References==
 

==External links==
* 

 
 
 
 