Keizoku
Keizoku ("Unsolved cases") is a Japanese mystery thriller created first as a TV drama and later as a film. It is about Detective Jun Shibata, who handles unsolved cases with her hardened partner Tōru Mayama.

The television series was broadcast in eleven episodes between 8 January and 19 March 1999.    A two-hour "special drama" was then broadcast on 24 December 1999.  The series has been called "epoch-making" in the police procedural genre on Japanese television.   

==Television==

===Cast===
* Miki Nakatani - Shibata Jun
* Atsuro Watabe - Mayama Tôru
* Sarina Suzuki - Kido Aya
* Yuu Tokui - Kondo Akio
* Hidekazu Nagae - Taniguchi Tsuyoshi
* Kenichi Yajima - Hayashida Seiichi
* Masashi Arifuku - Nagao Noboru
* Mari Nishio - Osawa Maiko
* Goro Noguchi - Saotome Jin
* Raita Ryu - Nonomura Koutarou
* Shigeru Izumiya -Tsubosaka Kunio

===Episode Titles===
*01: Phone Call from the Dead Man
*02: Punishment Table of Ice
*03: The Wiretapped Murderer
*04: The Room of Certain Death
*05: The Man Who Saw the Future
*06: The Wickedest Bombing-Demon
*07: Death Curse of the Oil Painting
*08: Farewell, Lovely Cutthroat
*09: Future Revenge of the Past
*10: Your Own Two Eyes
*11: The Kiss of Deaths Flavor

===Production===
* Screenwriter: Nishiogi Yumie
* Producer: Ueda Hiroki
* Directors: Tsutsumi Yukihiko, Kaneko Fuminori, Imai Natsuki, Isano Hideki
* Music: Mitake Akira

==Film (2000)==

===Cast===
*Shibata Jun: Nakatani Miki
*Mayama Tôru: Watabe Atsuro
*Hideyo Amamoto		
*Inuko Inuyama	
*David Itô	
*Izumi Pinko	
*Kunio Tsubosaka: Shigeru Izumiya
*Hairi Katagiri		
*Kera
*Koyuki
*Katsuyuki Murai	
*Katsuhisa Namase
*Nanako Ookôchi: Nanako Ôkôchi
*Kôtarô Nonomura: Raita Ryu
*Toshiya Sakai
*Aya Kido Sarina Suzuki
*Tomorowo Taguchi	
*Masahiro Takaki
*Akio Kondô Yu Tokui
*Kenichi Yajima

===Production===
*Director: Yukihiko Tsutsumi
*Music: Akira Mitake 	
*Cinematography: Satoru Karasawa
*Editing: Soichi Ueno
*Visual effects supervisor: Fumihiko Sori
*Visual effects(Renderman): Bernard Edlington

==References==
 

==External links==
* 
* 

 
 
 
 
 


 