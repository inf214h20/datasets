Half Way to Heaven
{{Infobox film
| name           = Half Way to Heaven
| image          = Half Way to Heaven poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Abbott
| producer       = 
| screenplay     = George Abbott Henry Leyford Gates Gerald Geraghty
| starring       = Charles Buddy Rogers Jean Arthur Paul Lukas Helen Ware Oscar Apfel Edna West Irving Bacon
| music          = Gene Lucas 
| cinematography = Alfred Gilks Charles Lang
| editing        = William Shea 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Half Way to Heaven is a 1929 American drama film directed by George Abbott and written by George Abbott, Henry Leyford Gates and Gerald Geraghty. The film stars Charles Buddy Rogers, Jean Arthur, Paul Lukas, Helen Ware, Oscar Apfel, Edna West and Irving Bacon. The film was released on December 14, 1929, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Charles Buddy Rogers as Ned Lee
*Jean Arthur as Greta Nelson
*Paul Lukas	as Nick Pogli
*Helen Ware as Madame Elsie
*Oscar Apfel as Circus Manager
*Edna West as Mrs. Lee
*Irving Bacon as Slim
*Michael Stuart as Eric Lee  Al Hill as Blackie
*Lucille Williams as Doris
*Richard K. French as Klein
*Freddy Anderson as Tony
*Ford West as Stationmaster
*Guy Oliver as Farmer at Railroad Station

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 