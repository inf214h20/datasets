Wrong Turn 4: Bloody Beginnings
 
{{Infobox film
| name = Wrong Turn 4: Bloody Beginnings
| image = WrongTurn4Poster.jpg
| image_size = 215px
| alt =
| caption = Promotional poster
| director = Declan OBrien
| producer = Kim Todd
| writer = Declan OBrien
| based on =  
| starring = Jenny Pudavick Tenika Davis Kaitlyn Wong Terra Vnesa
| music = Claude Foisy
| cinematography = Michael Marshall
| editing = Stein Myhrstad
| studio = Summit Entertainment Constantin Film
| distributor = 20th Century Fox Home Entertainment
| released =  
| runtime = 93 minutes
| country = United States Canada
| language = English
| gross   = $2,267,824
}}
 prequel to the first three films. The film was released on DVD and Blu-ray on October 25, 2011. 

== Plot ==
In 1974, at the Glensville Sanatorium, West Virginia, Dr. Brendan Ryan (Arne MacPherson) shows local psychiatrist Dr. Ann McQuaid (Kristen Harris) around the psychiatric hospital. He shows his three deformed patients, Three-Finger, One-Eye and Saw-Tooth, who all have the ability to sustain injury and not feel it. While Dr. Ryan and Dr. McQuaid move on, one of the mental patients grabs Dr. McQuaids hair and rips out her hair clip, which she does not notice. The patient gives the hair clip to Saw-Tooth. The three deformed patients use the clip to pick the lock and escape their cells. They release the other patients and together they kill the orderlies and the doctors including McQuaid and Ryan.

Twenty nine years later, nine Weston University students: Kenia (Jennifer Pudavick), Jenna (Terra Vnesa), Vincent (Sean Skene), Bridget (Kaitlyn Wong), Sara (Tenika Davis), Claire (Samantha Kendrick), Kyle (Victor Zinck, Jr.). Daniel (Dean Armstrong) and Lauren (Ali Tataryn) are snowmobiling, on their way to their friend Porters cabin in the mountains. However, they get lost in a snowstorm and are forced to take shelter in the Glensville Sanatorium, where the surviving cannibals (Three-Finger, Saw-Tooth and One-Eye) settled. The teens explore the asylum and decide to wait out the storm. Lauren remembers her brothers stories about the sanatorium and the cannibals, but her friends do not believe her. After the group go to bed, Vincent, suffering from insomnia, begins strolling around the asylum. After he finds Porters corpse, he is murdered by Saw-Tooth before he could alert the others. The next day as the storm continues, the teens notice that Vincent is missing and begin to look for him. While searching for him, Jenna witnesses the cannibals butchering Porters body, and she runs back to warn the others. After they find Porters severed head, Claire is caught around the throat with a strip of barbed wire and lifted up to a nearby balcony. Kyle attempts to save her, but the cannibals pull the wire tight, decapitating Claire. The group flees, but the cannibals have stripped off the spark plug wires on their snowmobiles. Lauren skis down the mountain to retrieve help, while the others barricade themselves in a doctors office.

Later, Sara, Daniel and Kyle go to the basement to find weapons. After discovering an arsenal of knives and other tools, they return to the others, but the cannibals captured Daniel. The rest of the group overhears his screams and runs back to save him, but they arrive too late when Kyle finds the cannibals eating Daniels body alive. The teens chase the cannibals, successfully trapping them in a cell. They begin to kill the cannibals, but Kenia refuses, saying that they would be just like the cannibals if they resorted to execution now that they had them at bay. The girls leave Kyle to guard the cannibals as they set off to find the spark plug wires. While Kyle is asleep, the cannibals use Dr. McQuaids hair clip to escape and attack him. Giving up their search for the spark plug wires, the girls return to the doctors office. During the night they mistake Kyle for a cannibal and accidentally stab him to death.

Realizing that they are locked in the building, Sara breaks a window and digs a tunnel through the snow. The girls escape, but Jenna is killed before she can get out. As Kenia, Sara and Bridget attempt to get away, the cannibals chase them on snowmobiles. The girls are separated while the cannibals attack and kill Bridget.

As the day dawns, Lauren has frozen to death in the storm. Kenia is still searching for the road when One-Eye reappears on a snowmobile. Sara arrives and saves Kenia, knocking One-Eye off the snowmobile and allowing the pair to steal it. As Kenia and Sara drive away, they accidentally run into a strip of barbed wire and are decapitated which was a trap of Three-Finger. Three-Finger picks up their heads and puts them in a tow truck, before driving away with the other cannibals.

== Cast ==
* Sean Skene as Three Finger/Vincent
* Blane Cypurda as Young Three Finger
* Daniel Skene as One Eye
* Tristan Carlucci as Young One Eye
* Scott Johnson as Saw Tooth
* Bryan Verot as Young Saw Tooth
* Jenny Pudavick as Kenia 
* Tenika Davis as Sara
* Kaitlyn Wong as Bridget
* Terra Vnesa as Jenna
* Victor Zinck Jr as Kyle
* Dean Armstrong as Daniel
* Ali Tataryn as Lauren
* Samantha Kendrick as Claire
* Arne MacPherson as Doctor Brendan Ryan
* Kristen Harris as Doctor Ann Marie McQuaid

== Release ==

Wrong Turn 4: Bloody Beginnings was released to DVD and Blu-ray on October 25, 2011. The film entered the DVD chart at number 13, selling 45,928 copies in its first week. To date the film has sold 143,000 units in America.  The film received mixed to negative reviews upon its release. The film holds a 20% on Rotten Tomatoes.

== References ==

 

== External links ==

*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 