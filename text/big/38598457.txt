Haadsa
{{Infobox film
| name           = 
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Akbar Khan
| producer       = Akbar Khan
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Akbar Khan Ranjeeta Naseeruddin Shah
| music          = Kalyanji-Anandji
| cinematography = Pramod Moutho
| editing        = Sunil
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}} 1983 Hindi Akbar Khan himself, Ranjeeta, Naseeruddin Shah, Amrish Puri as main characters.   

==Story==

 

==Cast==
* Akbar Khan  as  Jaikumar Jai Sharma
* Ranjeeta Kaur  as  Robby V. Kapoor
* Smita Patil  as  Asha
* Ashok Kumar  as  Dr. Ved Kapoor
* Amrish Puri  as  R.K. Chakravarty
* Naseeruddin Shah  as  Gangster
* Jalal Agha  as  Lorry Driver
* Bob Christo  as  Robbys Abductor
* Helen  as  Martha
* Iftekhar  as  Police Inspector
* Jagdeep  as  Anthony Gonsalves
* Mac Mohan  as  Gangsters goon
* Master Ravi  as  Guddu 
* Rajendra Nath
* Yogeeta Bali
* Amrish Puri  as  Ravi
* Rahul Dev  as  Bakhtawar
* Akash Khurana  as  Bade Miyan

==Music==
{{Track listing
| extra_column = Singer(s)
| all_lyrics   = 
| all_music    = Kalyanji-Anandji
| title1       = Haiyo Rabba | extra1 = Kanchan
| title2       = Pyaar Ka Haadsaa  | extra2 = Amit Kumar
| title3       = Tu Kya Jaane  | extra3 = Kishore Kumar
| title4       = Yeh Bombay Shehar | extra4 = Amit Kumar
| title5       = Yeh Vaada Karo | extra5 = Kishore Kumar, Asha Bhosle
| title6       = Y.O.G.A | extra6 = Amit Kumar, Chorus
| title7       = Zindagi Yeh Zindagi | extra7 = Anand Kumar
}}

==References==
 

==External links==
* 

 
 
 
 

 