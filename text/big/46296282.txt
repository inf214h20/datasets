Milap (1937 film)
 
 
{{Infobox film
| name           = Milap
| image          = Milap_(1937).jpg
| image_size     =
| caption        = Rampyari and Prithviraj Kapoor in Milap
| director       = Abdul Rashid Kardar
| producer       = Moti Mahal Pictures 
| writer         = 
| narrator       =
| starring       = Prithviraj Kapoor Bimla Kumari Rampyari M. Ismail
| music          = K. C. Dey
| cinematography = 
| editing        = 
| studio         = Moti Mahal Pictures 
| distributor    =
| released       = 1937
| runtime        = 
| country        = British India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1937 Hindi/Urdu social drama film directed by A. R. Kardar.    Produced under the Moti Mahal Pictures banner, it had music composed by K. C. Dey.    Milap was a big success for the actress Rampyari.    
 Nadira in his film Shree 420 (1955) for the song "Mud Mud Ke Na Dekh".   

The co-stars included M. Ismail, Yakub, Bimla Kumari, Dev Bala and Anees Khatoon.    

==Cast==
* Prithviraj Kapoor
* Rampyari
* Indira Devi
* Mazhar Khan
* M. Ismail
* Yakub
* Bimla Kumari
* Anees Khatoon
* Devbala

==Soundtrack==
Akbar Khan (Durrani) Peshawri sang the popular number "Pila Raha Hai Toh Kuchh Lutf-e-Mai Badha Ke Pila". The music director was K. C. Dey.    

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| "Chhoti Jaan Ke Na Chhod Jaiyo Baalma"
|-
| 2
| "Jamna Ka Kinara Ho Har Mauj Ke Hothon Par Afsana Hamara"
|-
| 3
| "Kahun Ri Sakhi Ik Maze Ki Baat"
|-
| 4
| "Koyaliya Madhur Bain Bole"
|-
| 5
| "Pila Raha Hai Toh Kuchh Lutf-e-Mai Badha Ke Pila"
|-
| 6
| "Sunoji Balam Ab Na Banegi Mori Tori"
|-
| 7
| "Dil Paraye Bas Mein Beet Gaya Din"
|-
| 8
| "Jagat Ka Rakhwala Hai Ram"
|-
| 9
| "Aa Prem Nagar Mein Aa Dil Ke Mandir"
|}

==References==
 

==External links==
*  

 

 
 
 
 

 