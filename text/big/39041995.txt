Daddy (2004 film)
 
{{Infobox film
| name           = Daddy
| image          = 
| caption        = 
| director       = Vladimir Mashkov
| producer       =  Aleksandr Galich
| starring       = Vladimir Mashkov
| music          = 
| cinematography = Oleg Dobronravov
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Russia
| language       = Russian
| budget         = 
}}

Daddy ( , Transliteration|translit.&nbsp;Papa) is a 2004 Russian drama film directed by and starring Vladimir Mashkov. It was entered into the 26th Moscow International Film Festival.   

==Cast==
* Vladimir Mashkov as Abraham Schwartz
* Andrei Rozendent as David Schwartz, 12 Years
* Olga Krasko as Tanya
* Lidiya Pakhomova as Tanya, 12 Years (as Pakhomova Lida)
* Olga Miroshnikova as Hannah
* Kseniya Bespalova as Hannah, 12 Years
* Sergey Dreyden as Meyer Wolf
* Kseniya Lavrova-Glinka as Lyudmila Shutova (as Kseniya Glinka)
* Anatoliy Vasilev as Chernyshov Ivan Kuzmich

==Soundtrack==
The background music includes a number of songs, one of the being "A song from the film  A Girl Hurrying to a Date" (composed by Isaak Dunaevsky, lyrics by Vasily Lebedev-Kumach|Lebedev-Kumach), performed by Efrem Flaks with an all-girl ensemble and a jazz orchestra.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 