Aarodum Parayaruthu
{{Infobox film
| name           = Aarodum Parayaruthu
| image          =
| caption        =
| director       = AJ Rojas
| producer       = Vijayan
| Vijayan
| Rohini Shankar Shankar Siddique Siddique Sukumaran
| music          = PC Susi
| cinematography = Divakara Menon
| editing        = Ayyappan
| studio         = Giri Manju Productions
| distributor    = Giri Manju Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Siddique and Sukumaran in lead roles. The film had musical score by PC Susi.   

==Cast==
  Rohini
*Shankar Shankar
*Siddique Siddique
*Sukumaran
*Unnimary Vijayan
*Babitha Justin
*Balan K Nair
*Bhagyalakshmi
*Kothuku Nanappan
*Kuthiravattam Pappu
*Lalithasree
*Sreenath
 

==Soundtrack==
The music was composed by PC Susi and lyrics was written by Poovachal Khader and Malloor Ramakrishnan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enthe Sreepadmanaabha || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Manavaalan || Latha Parassala || Poovachal Khader || 
|-
| 3 || Sindoorasandhyayil Aaraadi || K. J. Yesudas, Latha Parassala || Malloor Ramakrishnan Nair || 
|-
| 4 || Vinnil Theliyum || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 