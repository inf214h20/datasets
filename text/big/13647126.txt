Castle of Sand
{{Infobox film name           = Castle of Sand image          = Caste of Sand Poster.jpg caption        = Castle of Sand - Original Japanese Poster Art director       = Yoshitarō Nomura producer       = Shinobu Hashimoto Yoshihara Mishima Masayuki Sato writer         = Seicho Matsumoto (novel) Yoshitaro Nomura Shinobu Hashimoto Yôji Yamada starring       = music          = Mitsuaki Kanno Kosuke Sugano cinematography = Takashi Kawamata editing        =  distributor    = Shochiku released       =   runtime        = 143 minutes country        = Japan language       = Japanese budget         = 
}}
Castle of Sand (砂の器 Suna no utsuwa) is a 1974 Japanese police procedural directed by Yoshitarō Nomura, based on the novel "Inspector Imanishi Investigates" by Seicho Matsumoto.

==Plot==
Yoshitaro Nomura’s 1974 film of Seicho Matsumoto’s immensely popular detective story tells the tale of two detectives, Imanishi (Tetsuro Tamba) and Yoshimura (Kensaku Morita), tasked with tracking down the murderer of an old man, found bludgeoned to death in a rail yard.

==Cast==
*Tetsuro Tamba - Detective Eitaro Imanishi
*Go Kato - Eiryo Waga/Hideo Motoura
*Kensaku Morita - Detective Hiroshi Yoshimura
*Yoko Shimada|Yôko Shimada - Rieko Naruse
*Karin Yamaguchi - Sachiko Tadokoro
*Ken Ogata - Kenichi Miki Seiji Matsuyama - Shokichi Miki
*Yoshi Kato - Chiyokichi Motoura
*Chishū Ryū - Kojuro Kirihara

==Awards==
*1975 Kinema Junpo Award  . 
**Best Screenplay (Shinobu Hashimoto and Yôji Yamada)
*Readers Choice Award
**Best Japanese Film Director (Yoshitaro Nomura)
*1975 Mainichi Film Concours
**Best Director (Yoshitaro Nomura)
**Best Film (Yoshitaro Nomura)
**Best Film Score (Kosuke Sugano )
**Best Screenplay (Shinobu Hashimoto and Yôji Yamada)
*9th Moscow International Film Festival   
**Diploma (Yoshitaro Nomura)
**Nominated for Golden Prize (Yoshitaro Nomura).

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 