State Department: File 649
{{Infobox film
| name           = State Department: File 649
| image          =
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| writer         = Milton Raison
| narrator       =
| starring       = See below
| music          = Lucien Cailliet
| cinematography = Jack Greenhalgh
| editing        =
| distributor    = Film Classics, Inc
| released       = 11 February 1949
| runtime        = 87 minutes
| country        = USA English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

State Department: File 649 is a 1949 Cinecolor American film directed by Sam Newfield (using the pseudonym "Peter Stewart").

The film is also known as Assignment in China in the United Kingdom.

== Plot summary ==
Kenneth Seeley (William Lundigan), member of the U. S. State Departments Foreign Service Bureau, and Marge Weldon (Virginia Bruce ), a morale worker with the bureau, are assigned to an area in Mongolia dominated by an outlaw warlord. The latter captures the village where they reside and they´ll have to make a plan to escape.

== Cast ==
*William Lundigan as Kenneth R. Seeley
*Virginia Bruce as Margaret Marge Weldon
*Jonathan Hale as Director General of Foreign Service
*Frank Ferguson as Consul-General Reither
*Richard Loo as Marshal Yun Usu
*Philip Ahn as Col. Aram Raymond Bond as Consul Howard Brown
*Milton Kibbee as Bill Sneed, white trader
*Victor Sen Yung as Johnny Hon, houseboy
*Lora Lee Michel as Jessica Morse John Holland as Wilfred Ballinger
*Harlan Warde as Rev. Dr. Morse
*Carole Donne as Mrs. Sarah Morse
*Barbara Woodell as Carrie Willoughby, pianist Lee Bennett as Agent Don Logan Robert Stevenson as Mongolian Spy
*H.T. Tsiang as Mayor Wan-too
*Joseph Crehan as Junior Official
*Ray Bennett as Fur Trader
*Nana Bryant as Mrs. Peggy Brown

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 