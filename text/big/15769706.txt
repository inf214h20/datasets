Ontari (film)
 
 
{{Infobox film| 
  name     = Ontari  |
  image          =  |
    writer         = B.V. Ramana| Gopichand Bhavana Bhavana Ali Ali Sunil Sunil Ashish Vidyarthi Sayaji Shinde |
  director       = B.V. Ramana |
  producer       = Pokuri Babu Rao |
  distributor    = Ee Taram Films |
  released       = February 14, 2008 |
  runtime        = |
  editing        = Goutam Raju
 |
  cinematography = Sarvesh Murari |
  country        = India | Telugu |
  music          = Mani Sharma |
  awards         = |
  budget         = |
}}
 2008 Telugu Telugu film Gopichand and Bhavana Balachandran|Bhavana. Choreographer-turned-director B.V. Ramana directed this film. the movie was made under reputed banner Eetharam films. The film has been dubbed into Tamil as Subramaniam and in Hindi as Ek Aur Khal Nayak. The Film was remade Twice in Dhallywood as Boss Number One and Premik Number One with Shakib Khan.

==Plot==
Vamsi (Gopichand) is son of an affluent sari mandir owner Muddu Krishna Rao (Paruchuri Venkateshwara Rao). He happens to fall in love with a girl Bujji (Bhavana) who is an orphan and stays in a hostel. With the consent of his parents, Vamsi tries to get engaged with Bujji. But she was kidnapped by a group of gangsters.Bujji is attempted to molest by one of gangster brother (Ajay) who is roughed up by Vamsi. The gangster {Ashish Vidyarthi} vows to take revenge and on the day of Gopichand marriage with Bujji. He kidnaps and rapes her in front of hero. All this mishap happens because of support of MLA {Rajiv Kanakala} who is close friend of Vamsi as he is promised with minister berth by the gangster,and stabs Vamsi.Vamsi arrives Hyderabad and tries to trace the gangsters. Because of his betrayal, Vamsi kills his MLA friend (Rajiv Kanakala). Later in the movie, Sayyaji Shinde interviews Vamsis parents and family members because vamsi is in a state of hallucination and he didnt know until he told him. The cop Sayyaji Shinde investigates the case though at the end he catches the hero but releases him and gives his support for killing gangsters. The hero kills Lal Mahankali (Asish Vidyarthi) and rips his brothers arm off. Vamsis dad tells him that bujji dies the day of the engagement because she jumped  off a rock building .

== Cast == Gopichand .... Vamsi Bhavana .... Kanaka Mahalakshmi / Bujji
* Ashish Vidyarthi .... Lal mahankali, the main villain Ajay .... Lals brother, 2nd villain
* Sayaji Shinde IPS Officer, positive character Ali
* Sunil .... Vamsis friend Subbu
* Raghu Babu
* Supreeth .... Lals henchman Rajiv .... Vamsis family-friend Raghava

==Crew==
*Music:Mani Sharma
*Cinematography: Sarvesh Murari
*Action: Vijay
*Editing: Gowtam Raju
*Dialogues: Marudhuri Raja & Bhasha Sri
*Story: Eetaram unit
*Screenplay - direction: BV Ramana
*Producer: Pokuri Babu Rao

== External links ==
*  

 
 
 