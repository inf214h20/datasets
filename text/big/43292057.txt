The Outskirts (2015 film)
{{Infobox film
| name           = The Outskirts
| image          = 
| alt            = 
| caption        = 
| director       = Peter Hutchings
| producer       = Brice Dal Farra   Claude Dal Farra
| writer         = Dominique Ferrari   Suzanne Wrubel Peyton List   Harry Katzman
| music          = 
| cinematography = John Thomas
| editing        = Jeffrey Wolf
| studio         = BCDF Pictures
| distributor    = Clarius Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Peyton List and William Peltz. Principal photography on the film began on July 2, 2014 in Great Neck, New York. The film will be released on June 26, 2015.

== Plot ==
Two best friends, Mindy (Eden Sher) and Jodi (Victoria Justice), plan to unite the outcasts of the school and start a social revolution to get their revenge from the high schools alpha female Whitney (Claudia Lee).

== Cast ==
* Claudia Lee as Whitney
* Eden Sher as Mindy
* Victoria Justice as Jodi
* Avan Jogia as Dave
* Ashley Rickards as Virginia Peyton List as Mackenzie
* Katie Chang as Claire
* Frank Whaley as Herb
* William Peltz as Colin
* Ted McGinley as Principal Whitmore
* Harry Katzman as Louis
* Nick Bailey as Rick

== Production == Peyton List, Katie Chang and William Peltz.  On August 8, Frank Whaley joined the film to play Herb, the father of Jodi and a postal worker who agrees to start dating once his daughter takes charge of her own life. Victoria Justice and Avan Jogia starred together in the Nickelodeon television show Victorious. 

=== Filming === Great Neck, Port Washington. 

== Marketing ==
On July 29, 2014, two images from the film were revealed by Entertainment Weekly. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 