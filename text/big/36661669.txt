Vamshoddharakudu
{{Infobox film
| name           = Vamshoddharakudu
| image          = Imageof_Vamsoddarakudu.png
| alt            =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = Sarath
| producer       = M. S. Reddy T. Subbarami Reddy  
| director       = Sarath Ramya Krishna Raadhika
| Koti
| cinematography = K S R Swamy|V. S. R. Swamy
| editing        = K. V. Krishna Reddy
| studio         = Gayatri Films
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}  
 
Vamshoddharakudu ( , drama film directed by Sarath and produced by M. S. Reddy under Gayatri Films, . Starring Nandamuri Balakrishna, Ramya Krishna, Sakshi Shivanand and Krishnam Raju in the lead roles and music composed by Saluri Koteswara Rao|Koti.  

==Plot==
Suryam (Balaiah) is a physical director in a local college. His mother Varalakshmi (Radhika) is a widow. The college in which Suryam work belongs to an Industrialist called Sudarshan Rao (Charan Raj). Sudarshan Rao is the brother-in-law of the business magnate Raja (Krishnam Raju). Raja has two sons, Brahmaji and Ravi Babu.

Sudarshan Rao has a son, Srihari and a daughter, Saakshi. Saakshi is a student in the college. Satyabhama (Ramya) works as a junior PD (Physical Director) in the same college. Suryam and Saakshi used to have regular fights (amir heroine gareeb hero). After a few days they fall in love. Satyabhama too loves Suryam.

When they visit Khajuraho on a college tour, Satyabhama implicates Suryam that he tried to rape her. After a couple of scenes we come to know that Sudarshan Rao inducted Satyambhama into the college to malign the image of Suryam so that his daughter (Saakshi) hates Suryam in that process. Later on we come to know that Satya is the daughter of a conspirator Giribabu. She is asked to play the trick on Suryam in order to save her ailing father. Sudarshan Rao agrees for the marriage of Suryam and Sakshi. But he wants to know why the mother of Suryam is appearing as widow even though her husband is alive. Suryam caught unawares with this question, as he is not aware that his father is alive.

Later on Suryam comes to know that Raja is his father. Suryams mother used to work as a maid in Rajas house. When Raja gets involved in a murder of a thief, she takes up the responsibility and gets ready to go to the jail. Moved by the unconditional sacrifice done by his maid, Raja married his maid. Sudarshan Rao, brother in law of Raja, makes sure that the maid is thrown out of the house by doing manipulations. The maid comes out of the house and raises her kid (Suryam). After knowing the flashback, Suryam decides to go back to his father and win his laurels and there by set the house right.

== Cast ==
 
*Nandamuri Balakrishna as Suryam Ramya Krishna as Satya
*Sakshi Shivanand as Surekha
*Krishnam Raju as Rajagaru
*Charan Raj as Sudarshan Rao Srihari as Srikanth
*Brahmaji as Anand
*Ravi Babu as Ashok 
*Brahmanandam as Hanumanthu & Anjibabu (Duel role)
*Babu Mohan as Dalapati
*M. S. Narayana as History Lecturer
*Chalapathi Rao as Pandus Father
*Giri Babu as Satyas Father
*Surya as Pandu
*Ramaraju as Police Inspector Raadhika as Varalakshmi
*Rama Prabha as Surekhas Bamma
*Jayalalita as Ramulamma
*Varsha as Suryams Sister  Priya as Sudarshan Raos Sister Srilakshmi as Mangamma
*Kalpana Rai as Warden
*Y.Vijaya as Gangamma
 

==Soundtrack==
{{Infobox album
| Name        = Vamshoddharakudu
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 2000
| Recorded    = 
| Genre       = Soundtrack
| Length      = 26:51
| Label       = Supreme Music Koti
| Reviews     = Krishna Babu   (1999)  
| This album  =  Vamshoddharakudu   (2000)
| Next album  = Goppinti Alludu   (2000)
}}

Music composed by Saluri Koteswara Rao|Koti. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:51
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Kondapalli Bomma
| lyrics1 = Ghantadi Krishna SP Balu, Chitra
| length1 = 4:09

| title2  = Andala Prayam Sirivennela Sitarama Sastry
| extra2  = SP Balu,Chitra
| length2 = 4:21

| title3  = Gudi Gantalu 
| lyrics3 = Mallemala
| extra3  = Hariharan (singer)|Hariharan,Chitra
| length3 = 5:22

| title4  = Budi Budi Chinukula
| lyrics4 = Bhuvanachandra
| extra4  = Ramu,Chitra
| length4 = 3:50

| title5  = Dole Dole
| lyrics5 = Bhuvanachandra Sujatha
| length5 = 3:47

| title6  = Nee Choopu Bhale  
| lyrics6 = Suddala Ashok Teja  
| extra6  = SP Balu,Chitra
| length6 = 4:23
}}

==External links==
 

== References ==
 

 
 
 