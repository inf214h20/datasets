Major League II
 
{{Infobox film
| name           = Major League II
| image          = Major league ii.jpg
| caption        = Film poster 
| writer         = David S. Ward R.J. Stewart Tom S. Parker Jim Jennewein
| starring       = Charlie Sheen Tom Berenger Corbin Bernsen Dennis Haysbert Omar Epps David Keith Margaret Whitton Bob Uecker 
| director       = David S. Ward
| cinematography = Victor Hammer
| music          = Michel Colombier
| editing        = Donn Cambern Kimberly Ray Paul Seydor Frederick Wardell
| studio         = Morgan Creek Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 105 minutes
| language       = English
| producer       = Gary Barber James G. Robinson Susan Vanderbeek David S. Ward Ted Winterer Julia Miller
| budget         = $25 million
| gross          = $30,626,182
}}
 Major League. Major League II stars most of the same cast from the original, including Charlie Sheen, Tom Berenger, and Corbin Bernsen. Absent from this film is Wesley Snipes, who played Willie Mays Hayes in the first film and who by 1994 had become a film star in his own right. Omar Epps took over his role.
 MLB life.

==Plot== ALCS by the Chicago White Sox.
 Buddhist and adopted a more placid, carefree style as opposed to the angry and aggressive player he was before. Centerfielder Willie Mays Hayes is still as fast as ever but is more concerned with hitting home runs and his movie career, which saw him star in an action film that was a flop and resulted in him coming to spring training with a sprained knee. Again catcher Jake Taylor has also returned, and conceited third baseman Roger Dorn has retired and purchased the team from its previous owner, Rachel Phelps. One of his first acts as owner is to sign Oakland Athletics all-star catcher Jack Parkman, which forces Jake to compete for his old position. To further complicate things, minor-league catcher Rube Baker has also been invited to camp despite his inability to throw the ball back to the pitcher with any consistency.

As the team breaks camp, Taylor discovers that although he has made the team, he believes there is no way that manager Lou Brown is going to carry three catchers as Parkman and Rube have also made the team. Lou, after being confronted by Jake, informs Taylor that he is not going to carry him as a player but as a coach. While initially upset over being forced into retirement, Jake elects to take Lous offer and join the coaching staff.

Once again, the Indians start slow as Cerranos religious conversion causes him to struggle, Hayes refusal to play even with the slightest injury, Vaughns lack of control over his pitches, and Parkmans ego poisoning the clubhouse. To make matters worse, Dorn has been unable to keep up with the franchises finances and is forced to do strange things to bring in money such as putting advertising on the outfield walls. Eventually Lou reaches the end of his tolerance regarding Parkman and decides to suspend him after Parkman criticized the team in the local papers. Parkman then informs Lou that the suspension is moot as he has been traded to the White Sox. Lou confronts Dorn for not consulting him about the trade. Dorn explains that he could no longer afford to pay Parkmans salary and had no choice but to trade him. In return, Japanese import Isuro "Kamikaze" Tanaka, a gifted left fielder with a penchant for crashing into the fence, is sent to the Indians.

Finally out of options, Dorn sells the Indians back to Rachel Phelps, who is still angry over the fact that the teams success the year before prevented her from breaking the teams lease and moving it to Miami. Making matters worse, Lou suffers a heart attack in the clubhouse due to his frustration over the teams performance and Jake is given the reins of the team.

Things come to a head during a doubleheader against the Boston Red Sox. Late in the first game, Rube is hit by a pitch in his ankle and Hayes is called upon to run for him. Hayes refuses to do it, which angers Jake. Vaughn chimes in, which causes Hayes to bring up his own struggles and the two begin fighting. Soon, the entire team gets involved and begins fighting each other, resulting in everyone being ejected. After the game, Tanaka criticizes Cerrano for not having any "marbles" due to his struggles and Hayes makes a wisecrack at Baker about his injury, while Rube lashes out at Hayes and the rest of the team for their lack of passion. Inspired by the speech, Hayes volunteers to run for the injured Baker in the bottom of the ninth inning of the second game and promptly steals second, third, and home to tie the score. Cerrano, also inspired, demands that Jake insert him into the game to pinch hit and he responds by hitting the game-winning home run.

The win sparks a hot streak that the Indians ride all the way to a second straight division title, clinched by beating the Toronto Blue Jays on the last day of the season. Despite the teams hot streak, Vaughn continues to slump as his ineffective breaking pitches have caused him to lose confidence in his best pitch, his fastball. To make matters worse, he refuses to finish games he starts and has allowed the fans to get into his head.

In the ALCS, the Indians square off in a rematch with the White Sox and win the first three games of the series. This inspires Rachel to give the team a phony pep talk before Game 4, which is purposely designed to get in the heads of the players and distract them. It works, as a still struggling Vaughn gives up a game-winning home run to Parkman in the bottom of the ninth. The White Sox then defeat the Indians in the next two games, forcing a seventh game played in Cleveland.

The night before the game, Jake goes to visit Vaughn at his home and tells him that he might be called on to pitch in relief in Game 7. Vaughn nonchalantly tells Taylor he will be ready, which infuriates Jake to the point where he lashes out at Vaughn. He calls Vaughn out for having lost his edge and not having his head in the game, and tells him that he is of no use to the team if he continues playing the way he is. Before Taylor leaves, he tells Vaughn to find his edge if it has not already escaped him.

The White Sox jump out to an early lead in Game 7 and lead 2-1 after Parkman bowls over Rube on a play at the plate. With the game tied, Hayes reaches base on a walk and taunts Parkman by saying he is going to score on the play without sliding. Sure enough, he does this by hurdling Parkman as he comes around and touches home. Parkman responds, however, by hitting a three-run home run in the seventh inning and the White Sox carry a 5-3 lead into the bottom of the eighth.

Although the Indians get a runner to reach base, two outs are recorded and Jake is forced to make a strategic move. He calls upon Roger Dorn, who had signed himself to a contract after selling the team. Recognizing the White Sox pitcher as one who routinely pitched Dorn inside, Taylor encourages Dorn to "take one for the team." Dorn obliges and is hit by the first pitch, then is pulled off the field immediately afterward for a pinch runner. With two runners on, Cerrano hits a towering home run to give the Indians a 6-5 lead.

With two outs and two runners on in the top of the ninth, Jake decides to take the ball from his starter and call on Vaughn. To everyones surprise, especially Rachels, Vaughn comes out of the bullpen with a rediscovered sense of self. Taylor gives Vaughn a scouting report on the batter he is called in to face, but Vaughn does not want to pitch to him. Instead Vaughn wants to walk the hitter and risk loading the bases for a chance at redemption against his old nemesis Parkman. Jake obliges him, and Vaughn walks the bases loaded.

Vaughn throws a fastball that Parkman takes for the first strike, then follows with another fastball that Parkman manages to foul off. With two strikes on him, an impressed Parkman dares Vaughn to throw it a third time. Vaughn fearlessly complies, and Parkman swings and misses for the final out, sending the Indians to the World Series. Its unknown if the Indians won the World Series, but it can be inferred that they may have failed as in the third film in the trilogy, Rube, Cerrano and Tanaka are playing minor league baseball, while Dorn owns the Minnesota Twins while Cerrano apparently retired from baseball in the 4-year gap between films only to make a comeback.

==Cast==
* Charlie Sheen as Rick Wild Thing Vaughn 
* Tom Berenger as Jake Taylor 
* Corbin Bernsen as Roger Dorn
* Dennis Haysbert as Pedro Cerrano
* James Gammon as Lou Brown
* Omar Epps as Willie Mays Hayes
* Bob Uecker as Harry Doyle
* David Keith as Jack Parkman
* Takaaki Ishibashi as Isuru Tanaka
* Margaret Whitton as Rachel Phelps
* Eric Bruskotter as Rube Baker
* Alison Doody as Rebecca Flannery
* Michelle Burke as Nicki Reese
* Rene Russo as Lynn Wells 
* Jay Leno as himself
* Randy Quaid as an extremely loyal fan
* Richard Schiff as a commercial director
* Jesse Ventura as himself

==Box office reception==
The movie debuted at No. 1, knocking out  , another sports comedy featuring Major League star Charlie Sheens brother, Emilio Estevez.  In the United States, the movie grossed a total of $30,626,182 at the box office. 

Major League II received extremely negative reviews from critics, leaving the film with just a 5% rating on Rotten Tomatoes based on 21 reviews. 

==Notes==
Cleveland Stadium was not used, as it was not in the first film. Oriole Park at Camden Yards replaced Milwaukee County Stadium as the stand-in for the teams home. Although Oriole Park bore a stronger resemblance to the stadium that the Indians were playing in when Major League II was released (the now-Progressive Field), like County Stadium it was designed to represent Cleveland Stadium in the film as the new ballpark was not yet named at the time of the filming.  The outfield scoreboard at Oriole Park reads "Welcome to Cleveland Stadium" at various points and scenes in the outfield are played in front of a blue wall, which Cleveland Stadium had (Oriole Park and Progressive Field both have dark green outfield walls).
 Cleveland Indians Atlanta Braves. In the lead-up to Game 3, the first World Series game played in Cleveland in 41 years, the PA system played "The House Is Rockin", the song from the end of Major League II. In another coincidence, Bob Uecker served as a commentator for the 1995 World Series television coverage on Major League Baseball on NBC|NBC.

==Sequel==
 , was released in 1998.

On April 6, 2011 in Cleveland Ohio, Charlie Sheen during his "violent torpedoes of truth" tour announced to the audience that hes working on a third sequel, titled Major League 3, and said "We are gonna shoot it right here in Cleveland!" He opened the show wearing a "Rick Vaughn" #99 Cleveland Indians jersey.

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 