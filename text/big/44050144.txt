Inaye thedi
{{Infobox film 
| name           = Inaye thedi
| image          =
| caption        =
| director       = Antony Eastman
| producer       = Antony Eastman
| writer         =
| screenplay     =
| starring       = Kalasala Babu Sankaradi Achankunju Silk Smitha Johnson
| cinematography = Vipin Das
| editing        = NP Suresh
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed and produced by Antony Eastman. The film stars Kalasala Babu, Sankaradi, Achankunju and Silk Smitha in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
*Kalasala Babu
*Sankaradi
*Achankunju
*Silk Smitha
*TM Abraham

==Soundtrack== Johnson and lyrics was written by RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Vipina vaadika || P Jayachandran || RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 