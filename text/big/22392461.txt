His Brother (film)
{{Infobox film
| name           = His Brother   (Son frère)
| image          = His-brother-film.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Patrice Chéreau
| writer         = 
| starring       = Bruno Todeschini Eric Caravaca  Nathalie Boutefeu Pierre Chevalier (producer) Joseph Strub (exec. producer)
| cinematography = Eric Gautier
| music          = 
| editor         = François Gédigier
| distributor    = 
| released       = 2003
| runtime        = 
| country        = France
| language       = French
}}
His Brother ( ) is a 2003 French film directed by Patrice Chéreau. The screenplay, based on the Philippe Besson 2001 book Son frère (English title His Brother) was written by Chéreau and Anne-Louise Trividic.

==Synopsis==
Brothers Thomas (Bruno Todeschini) and Luc (Eric Caravaca) have an estranged relationship.  One day, Thomas comes to Lucs apartment explaining that he is ill and asks if Luc will accompany him to the hospital.  Thomas has an undetermined platelet disorder which is treated with cortisone and splenectomy—neither help. Luc, who happens to be homosexuality|gay, and is in a relationship with Vincent (Sylvain Jacques), is viewed by their father as the stronger of the two and wishes it was Luc that were ill, as he could beat the illness.

Thomass girlfriend, Claire (Nathalie Boutefeu), leaves Thomas when she realizes he only wants his brother Luc around. Lucs relationship with Vincent is also strained as Luc takes Thomas to the family home in Brittany. The film jumps forward in time to allow about 18 months to be covered during the course of the film.  By the end of the film, the brothers reconcile their relationship.

==Cast==
*Bruno Todeschini as Thomas
*Eric Caravaca as Luc
*Nathalie Boutefeu as Claire
*Sylvain Jacques as Vincent
*Maurice Garrel as the old man
*Catherine Ferran as Head Doctor
*Antoinette Moya as the mother
*Fred Ulysse as the father
*Robinson Stévenin as Manuel

==Awards and nominations== Berlin Film Festival (Germany) Silver Bear - Best Director (Patrice Chéreau)
**Nominated: Golden Bear (Patrice Chéreau)

*César Awards (France) Best Actor &ndash; Leading Role (Bruno Todeschini)

*European Film Awards Best Actor &ndash; Leading Role (Bruno Todeschini)

*Lumiere Awards (France)
**Won: Best Actor &ndash; Leading Role (Bruno Todeschini)

 

 
 
 
 
 
 


 