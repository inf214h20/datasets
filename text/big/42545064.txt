Olavina Udugore
{{Infobox film	
| name = Olavina Udugore
| image =
| caption =
| director = D. Rajendra Babu
| producer = M Raman, D Rajendra Babu, N R Shashikala, Padma, D V Sudheendra, Vasantha, N Ramamurthy
| writer = D. Rajendra Babu
| narrator = Ramakrishna Leelavathi Leelavathi
| music = M. Ranga Rao
| cinematography = Kulashekar
| editing = K. Balu
| studio=  Sri Ashtalakshmi Chithralaya
| released =  
| runtime = 142 minutes
| country = India Kannada
| budget =
| gross =
}}	
 Ramakrishna in the lead roles.  The music was composed by M. Ranga Rao and the script was written by B. L. Venu. The film was declared a musical hit at the box-office upon its release.  Actor Ambareesh was awarded with the Filmfare Award for Best Actor - Kannada for the year 1988. 

==Cast==
* Ambareesh
* Manjula Sharma Leelavathi
* Ramakrishna
* Balakrishna
* Dinesh
* N. S. Rao
* Umashree
* Shanthamma

==Soundtrack==
{{Infobox album
| Name        = Olavina Udugore
| Type        = Soundtrack
| Artist      = M. Ranga Rao
| Cover       = Olavina Udugore album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1986
| Recorded    =  Feature film soundtrack
| Length      = 22:04
| Label       = Sangeetha
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

All songs were composed by M. Ranga Rao, with lyrics by R. N. Jayagopal and Shyamasundara Kulkarni. The album consists of five soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 22:04
| lyrics_credits = yes
| title1 = Olavina Udugore
| lyrics1 = R. N. Jayagopal
| extra1 = Jayachandran
| length1 = 3:58
| title2 = Olavina Udugore (Duet Version)
| lyrics2 = R. N. Jayagopal
| extra2 = Jayachandran, P. Susheela
| length2 = 4:00
| title3 = Kannige Kaanada
| lyrics3 = R. N. Jayagopal
| extra3 = S. P. Balasubramanyam
| length3 = 4:12
| title4 = Ninnantha Cheluveyanu
| lyrics4 = Shyamasundara Kulkarni
| extra4 = S. P. Balasubramanyam
| length4 = 5:04
| title5 = Hrudaya Miditha
| lyrics5 = Shyamasundara Kulkarni
| extra5 = S. P. Balasubramanyam, Vani Jairam
| length5 = 4:50
}}

==Awards==
* Filmfare Award for Best Actor - Kannada - Ambareesh

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 


 