Magic in Town
 
{{Infobox film
| name           = Magic in Town
| image          =
| caption        =
| director       = Annelise Reenberg Erik Larsen
| writer         = Annelise Reenberg
| starring       = Jeanne Darville
| music          = Sven Gyldmark
| cinematography = Mikael Salomon
| editing        = Maj Soya
| studio         = Saga Studios
| released       =  
| runtime        = 95 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Magic in Town ( ) is a 1968 Danish comedy film directed by Annelise Reenberg and starring Jeanne Darville.

==Cast==
* Jeanne Darville - Else Berg William Rosenberg - Peter Berg
* Pusle Helmuth - Pulse Berg
* Jan Priiskorn-Schmidt - Jan Berg
* Vibeke Houlberg - Lotte Berg Michael Rosenberg - Michael Berg
* Sonja Oppenhagen - Rikke Berg Lars Madsen - Blop Berg
* Sigrid Horne-Rasmussen - Fru Jensen
* Karen Berg - Tante Alma
* Dirch Passer - Dr. Mogensen
* Ove Sprogøe - Politiassistent Møller
* Bjørn Puggaard-Müller - Overbetjent Pedersen
* Karl Stegger - Advokat Andersen
* Thecla Boesen - Fru Edel Andersen

==External links==
* 

 
 
 
 
 
 


 
 