Sleeping Beauty (2011 film)
 
 
{{Infobox film name          = Sleeping Beauty image         = Sleeping Beauty film.jpg caption       = Theatrical poster director      = Julia Leigh producer      =   screenplay    = Julia Leigh starring      = {{Plainlist|
* Emily Browning
* Rachael Blake
* Ewen Leslie
* Michael Dorman
* Mirrah Foulkes
* Henry Nixon
}} music  Ben Frost
|cinematography= Geoffrey Simpson editing       = Nick Meyers studio        =   distributor   = Paramount Pictures released      =   runtime       = 102 minutes country       = Australia language      = English budget        = $3 million  gross         = {{Plainlist|
* $36,578 (USA) 
* $A300,888 (Australia) }}
}} Nobel laureate Yasunari Kawabata.   

The film premiered in May at the 2011 Cannes Film Festival as the first Competition entry to be screened. It was the first Australian film In Competition at Cannes since Moulin Rouge! (2001). Sleeping Beauty was released in Australia on 23 June 2011. It premiered in US cinemas on 2 December 2011 on limited release. Critical reaction to the film was mixed. 

==Plot==
Lucy ( ), who is attracted to her. Although she does not return his affection, she appears to be happier when with him than at any other time.

Lucy responds to an ad and is invited to meet Clara (  in lingerie. Lucy agrees, and Clara tells her that she will never be penetrated during these encounters. Clara says she will call Lucy by the name Sarah. Lucy gets beauty treatments before arriving for the event. She is the only girl dressed in white lingerie; the other women seem to be much older, wear severe makeup, and have black lingerie designed to reveal much more than to conceal. The event is a formal dinner party at an elegant home. Lucy serves drinks for the party and goes home with the money she made from it.

After one other session as a serving girl, Lucy gets a call from Claras assistant Thomas (Eden Falk) for a different request. Lucy is driven to a country mansion, where Clara and Thomas inspect her body.  Clara then offers Lucy a new role with the clients, wherein she will drink some tea and then fall into a deep sleep.

Lucy is seen lying in a large bed, sedated, as Clara leads in the man who hosted the first dinner party. After Clara reminds the man of the no-penetration rule, he strips, caresses Lucys body, and cuddles up next to her.

Lucy is evicted from her room by her landlords. She instead rents a much more expensive apartment. After two more sleeping sessions at Claras house, Birdmann calls her; he has overdosed on drugs, and she visits him as he dies. She takes off her shirt and gets in bed with him, sobbing but making no effort to help him. At his funeral, Lucy blandly asks a former acquaintance if he will marry her. Dumbfounded, he refuses, citing his new relationship, and several character flaws in Lucy.
 hung over much larger dose of the drug.

The morning after, Clara comes in and checks the mans pulse, showing no surprise when he cannot be awakened. She then tries to wake Lucy but is at first unable to do so, eventually having to use Artificial respiration|mouth-to-mouth resuscitation. Lucy awakes and, discovering that the naked man lying beside her is dead, screams. Throughout the whole film, Lucy was quiet, passive, and stoic, now when she sees the situation, she finally releases emotions—the sleeping beauty, now awake.

The film ends with the scene captured by the camera that Lucy had installed: the dead old man and the sleeping girl both lying on the bed.

==Cast==
*Emily Browning as Lucy
*Rachael Blake as Clara
*Ewen Leslie as Birdmann
*Michael Dorman as Cook
*Mirrah Foulkes as Sophie
*Henry Nixon as Mark Peter Carroll as Man 1
*Chris Haywood as Man 2
*Hugh Keays-Byrne as Man 3

==Production==
Writer and director Julia Leigh, primarily a novelist, said in an interview with Filmmaker Magazine that she initially wrote the film without the intention of directing it.  In writing the script, Leigh drew from several literary inspirations—the Japanese novel The House of the Sleeping Beauties by Yasunari Kawabata; Memories of My Melancholy Whores by Gabriel Garcia Marquez;  a story in the Bible in which King David as an old man spends the evening alongside sleeping virgins; and the eponymous fairytales by Charles Perrault and The Brothers Grimm.   She also noted the phenomenon of images of sleeping girls on the internet, presumably in somnophilia pornography. 
 Black List Jane Eyre. 

Sleeping Beauty is notable for its extremes of production style. Nearly every scene is shot in one long take; sound design is minutely controlled and makes very sparing use of original electronic music; colour palette is extremely carefully designed to fit dramatic situations; set design is scrupulously detailed and timing of fades is  calculated to create a dreamlike, surrealist feeling. This surrealism is added to by some judicious use of magic effects. 

==Reception==
A trailer was released on the same day that the film was announced for the main Competition of the 2011 Cannes Film Festival. The film premiered at the festival on 12 May as the first Competition entry to be screened. An early-morning press preview drew a mixed response but the official red carpet evening premiere received a prolonged standing ovation. All subsequent screenings at Cannes were packed and the film became one of the most talked about at the festival. Actor Jude Law, a Cannes jury member, stated in a press conference that Sleeping Beauty had just missed out on one of the  major awards.       

In a review from the festival,   in every sense", with the reservation: "Cannes audiences tend to be more forgiving in sections geared to emerging talent, like Un Certain Regard or Directors Fortnight. Outside the glare of Competition, even this pretentious exercise might have earned some appreciation for its rigorously cold aesthetic".  Ian Buckwalter of NPR noted the films "sexless and sterile" approach to its erotic material, saying, "This Sleeping Beauty is no fairy tale; its stark, dispassionate and noticeably short on happily ever afters". 

Other reviewers have been intrigued: "Titillates, terrifies and haunts in equal measure", said Sukhdev Sandhu in the UKs   in The New York Times found the film "seductive and unnerving in equal measure" while also observing that "the tone is quiet and the pacing serenely unhurried" and that "Sleeping Beauty is at times almost screamingly funny, a pointed, deadpan surrealist sex farce that Luis Buñuel might have admired". 

Comparing Leighs literary style to her film imagery, Ryan Gilbey of the British New Statesman noted that "the taut prose of the Australian novelist Julia Leigh (who wrote The Hunter and Disquiet) has been preserved in her first film,Sleeping Beauty. Leigh uses language parsimoniously on the page, with each word weighted for maximum tension or ambiguity, and she demonstrates the same approach to images." He concluded that Sleeping Beauty was "a convincing and original debut."  The New Statesman subsequently awarded Julia Leigh a 2011 Cultural Capital Award for Best Newcomer Director. 

Slant Magazine gave two reviews. For the 2011 Santa Fe Film Festival, Glenn Heath Jr. commented that "Julia Leighs striking debut film, Sleeping Beauty, is a treasure trove of formal artistry and psychological abstraction."   Michael Nordine, while awarding Sleeping Beauty three stars out of four, noted that "Leighs take on the   story is a study in detachment and unspoken dissatisfaction, traits that imbue the proceedings with a barely-contained sexual energy lurking beneath a thin veneer of calm" and that the film was "a mood piece whose primary goal is to immerse both protagonist and viewer in a strange environment and discover how all involved react." 
 SBS gave the film two stars out of five, arguing that "A character whose inner life is so affected by events about which she has no memory is inherently intriguing, but the early promise of Leighs idea loses momentum quickly".  Interestingly, when Sleeping Beauty was first shown on the SBS World Movies pay TV channel in September 2012, it was the highest-rating film.  It subsequently became the highest-rating film ever shown on the World Movies Channel. 

Sleeping Beauty was a film festival fixture, showing in over 50 major festivals worldwide in 2011–12. Emily Browning won best actress awards at The Hamptons (USA) and Kiel (Germany).  Julia Leigh received a Special Mention at the Stockholm International Film Festival 2011 for Sleeping Beauty, "For its ability to provoke and at the same time start an intellectual discussion about the things that it hurts to talk about", and in July 2012 won Best Director in a First Feature Film at the Durban International Film Festival. 

In addition to festival screenings, Sleeping Beauty was distributed commercially into 45 territories (over 65 countries). 

In May 2012, on the anniversary of her Cannes debut, Julia Leigh received the top award of the Australian Directors Guild, Best Direction in a Feature Film.

Sleeping Beauty has attracted attention from academic film journals. Dan Sallitt in Notebook has analysed the literary devices employed throughout the film, and has also recognised the intentional humour noted above;  Genevieve Yue in Film Quarterly has compared in detail the Catherine Breillat film The Sleeping Beauty with Julia Leighs.  Emma Deleva in Cahiers du Cinéma has discussed the controversy over the French R16 classification and the distributors unsuccessful appeal against this.  Lesley Chow in Bright Lights Film Journal has dealt with other literary aspects, including the symbolic use of sleep as a metaphor.  This line was extended by Meredith Jones (UTS Sydney, Australia) in a paper in the International Journal of Cultural Studies where Sleeping Beauty was seen as representing sleep as the "anti-matter to the neo-liberal imperative of Just Do It.".   In a paper in Studies in Australian Cinema, Kyra Clarke (UWA, Perth, Australia) has shown how Sleeping Beauty  highlights the importance of placing aside conventional expectations of girls produced in the media  and accepting the challenge of confused and imperfect representations and the heteronormative constraints that structure society.  In a paper in the Journal of International Womens Studies, Kendra Reynolds concludes that "through her anti-tale Leigh provides the resuscitation needed to revive feminism from its premature bed in order to ensure that the real Sleeping Beauty, the true female identity, will not sleep forever"  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 