Oki's Movie
{{Infobox film
| name           = Okis Movie
| image          = File:Okis_Movie_poster.jpg
| alt            = 
| caption        = 
| film name      = {{Film name
| hangul         =  
| hanja          = 옥희의  
| rr             = Ok-hi-eui Yeonghwa
| mr             = Ok-hi-ŭi Yŏnghwa}}
| director       = Hong Sang-soo
| producer       = Kim Kyeong-hee
| writer         = Hong Sang-soo Jung Yu-mi Moon Sung-keun
| music          = We Zong-Yun
| cinematography = Park Hong-yeol Ji Yoon-jeong
| editing        = Hahm Seong-won
| studio         = Jeonwonsa Films
| distributor    = Sponge Entertainment
| released       =  
| runtime        = 80 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Okis Movie ( ) is a 2010 South Korean drama film written and directed by Hong Sang-soo.

In a multipart narrative divided into four chapters, Hong fashions a new kind of love triangle. Oki is a young and beautiful college student majoring in film production and torn between the affections of two men: an older cinema professor and a former student/budding filmmaker. As the story shifts perspectives and timelines, Hong depicts each relationship with the authentically awkward rhythms of real life.  

== Plot == shorts director Nam Jin-gu (Lee Sun-kyun) is nagged by his wife Jang Su-yang (Seo Yeong-hwa) about his drinking, and he wonders if she is having an affair with a guy called Yeong-su. Nams onetime professor at film school, Song (Moon Sung-keun), tells him that filmmaking as an art is now dead. Nam remembers his first meeting with his wife, then an impressionable amateur photographer, on a park bench. At a dinner with film-school staff, Nam gets drunk and into a quarrel with Song, about whom hes heard a disquieting rumor. Afterwards, at the Q&A for his film, Nam is asked by a member of the audience (Lee Chae-eun) whether its true he was dating the actress at the time and is therefore responsible for ruining her life. Nam says he has quit directing.
 Jung Yu-mi) at film school and tries to go out with her, claiming hes never dated a woman before. When they smooch in a greenhouse, she says hes a good kisser. Shes still getting over a relationship with an older man but finally gives in to Nams persistence, and they sleep together and date.

After the Snowstorm ( ): Following a heavy bout of snow, only Nam and Jung turn up one day for Prof. Songs class, and the three end up talking about relationships. Song has already decided to quit teaching.

Okis Movie ( ): Jung narrates her own short movie based on her relationships with two guys, an "older man" and a "younger man", with whom she separately went walking with one winter on Mt. Acha, south of Seoul. 

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 