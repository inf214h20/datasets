Gajapathi Garvabhanga
{{Infobox film
| name           = Gajapathi Garvabhanga
| image          = 
| image_size     = 
| caption        = Film poster
| director       = M. S. Rajashekar
| producer       = Parvathamma Rajkumar
| writer         = 
| screenplay     = 
| narrator       = 
| starring       = Raghavendra Rajkumar  Malashri
| music          = Upendra Kumar
| lyrics         = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
}}
Gajapathi Garvabhanga is a 1989 Indian comedy film in the Kannada language. It was directed by M. S. Rajashekar and produced by the actor Rajkumar (actor)|Rajkumars production company Poornima Enterprises. The movie starred his second son Raghavendra Rajkumar and Malashri. The cast included  Dheerendra Gopal, Srinath and Honnavalli Krishna.  The film ran in theatres for 365 days. Raghavendra Rajkumars previous movie Nanjundi Kalyana was also a comedy with involving many of the same cast and crew.

==Cast==
* Raghavendra Rajkumar
* Malashri
* Dheerendra Gopal

==Songs==
{| class=wikitable sortable
|-
! Title !! Singer(s) !! Lyricist 
|-
|  Hosa Ragavidu ||  Raghavendra Rajkumar, Manjula Gururaj || Sri Ranga 
|-
|  Jataka Kudure || Manjula Gururaj || Sri Ranga 
|-
|  Tharakari Thayamma || Raghavendra Rajkumar || Chi. Udayashankar 
|-
|  Maduveya Vayasu || Raghavendra Rajkumar, Manjula Gururaj || Chi. Udayashankar 
|-
|  Olida Swaragalu || Raghavendra Rajkumar, Kusuma, Madhusudan || Chi. Udayashankar 
|}

==References==
 
#http://popcorn.oneindia.in/movie-cast/7876/gajapathi-garvabhanga.html
#http://www.raaga.com/channels/kannada/music/Upendra_Kumar.html

== External links ==

 
 
 


 