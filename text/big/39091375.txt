5 Sundarikal
{{Infobox film
| name           = 5 Sundarikal
| image          = 5 Sundarikal Poster.jpg
| caption        = Promotional poster of the film.
| alt            =
| director       = Shyju Khalid Sameer Thahir Aashiq Abu Amal Neerad Anwar Rasheed
| producer       = Amal Neerad
| story          = M. Mukundan Sijoy Varghese Amal Neerad Feng Jicai Hashir Mohamed
| screenplay     = Shyam Pushkar Muneer Ali Siddharth Bharathan Sijoy Varghese Abhilash Kumar Unni R. Hashir Mohamed
| starring       = Anikha Chethan Isha Sharvani Nivin Pauly Kavya Madhavan Biju Menon Dulquer Salmaan Reenu Mathews Jinu Ben Fahadh Faasil Asmita Sood
| music          = Gopi Sundar Bijibal Prashant Pillai Yakzan Gary Pereira Black Letters (Promo)
| cinematography = Alby Shyju Khalid Rajeev Ravi Ranadive Amal Neerad
| editing        = Vivek Harshan   Praveen Prabhakar Amal Neerad Productions
| released       =  
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
5 Sundarikal ( ) is a 2013  -based rock band Black Letters was also released.   

The featurette Sethulakshmi is an adaptation of M. Mukundans short story Photo while Kullande Bharya is based on a Chinese story, The Tall Woman and Her Short Husband by Feng Jicai.

==List of short films==
{| style="background:#d3d3d3;" class="wikitable"
|-
! Title!! Director !! Story !! Screenplay !! Cinematographer !! Music
|-
| Sethulakshmi || Shyju Khalid || M. Mukundan || Shyam Pushkar Muneer Ali || Alby || Yakzan Gary Pereira
|-
| Isha || Sameer Thahir || Sijoy varghese || Siddharth Bharathan Sijoy varghese ||  Shyju Khalid || Prashant Pillai
|-
| Gauri || Aashiq Abu || Amal Neerad  (story idea)  || Abhilash Kumar || Rajeev Ravi || Bijibal
|-
| Kullande Bharya || Amal Neerad || Feng Jicai || Unni R. ||  Ranadive || Gopi Sundar
|-
| Aami || Anwar Rasheed || Hashir Mohamed || Hashir Mohamed ||  Amal Neerad || Yakzan Gary Pereira
|}

==Plots==
===Segment 1: Sethulakshmi===
Sethulakshmi (Baby Anika), a school-going girl, has a hobby of collecting the photos of newly married couples from newspaper and pasting them in a note-book. She has a close friend (Master Chethan) and he comes to know about her keen interest in taking photos. Once they decide to go to a studio nearby for taking their photos. The decision changes their destiny...

===Segment 2: Isha===
Two strangers, Isha (Isha Sharvani) and Jinu (Nivin Pauly) meet in a house for a similar cause in a New Year eve. The two have a conversation and then a series of twists and turns happen.

===Segment 3: Gauri===
The short film tells the love story of a married couple living in a hill station. After materializing their love affair through register marriage, Gauri (Kavya Madhavan) and Jonathan Antony (Biju Menon) choose the place to begin their married life. Jo is crazy about trekking while Gauri is a dancer and teaches the art to her students. On the eve of their wedding anniversary, a tragedy occurs in their life and that changes their life.

===Segment 4: Kullante Bharya=== Alfred Hitchcock Rear Window. 

===Segment 5: Aami===
A businessman, Ajmal (Fahadh Faasil), faces a row of events during his journey from Malappuram to Kochi. His affectionate wife (Asmitha Sood) tries to keep him awake  during the drive by asking a few tricky questions. The night journey becomes an eventful one and changes the course of his life.

==Cast==
{| class=wikitable
|-
! style="background:#EEDD82; width:20%" | Sethulakshmi
! style="background:#EEDD82; width:20%" | Isha
! style="background:#EEDD82; width:20%" | Gauri
! style="background:#EEDD82; width:20%" | Kullante Bharya
! style="background:#EEDD82; width:20%" | Aami
|-
| valign="top" |
* Baby Anikha as Sethulakshmi
* Master Chethan as Abhilash
* Guru Somasundaram as Photographer
* Gopalakrishnan as Achan
* Anjali Upasana as Amma
* Unnimaya as Teacher
| valign="top" |
* Isha Sharvani as Isha
* Nivin Pauly as Jinu/Santa
* Deepak Sharaf as Teresas father
* Sujatha as Teresas mother
* Pradeep as Police Constable
* Rajasekhar V. Das as Dealer
* Ajay Natrajan as Driver
| valign="top" |
* Kavya Madhavan as Gauri
* Biju Menon as Jonathan Antony
* Shine Tom Chacko as Servant
* Tini Tom as Husband
* Rimi Tomy as Wife
* Jayasurya as Buyer
| valign="top" |
* Dulquer Salmaan as Photographer/Storyteller
* Reenu Mathews as Kullante Bharya
* Jinu Ben as Kullan
* Pouly Valsan as Molly Chechi
* Dileesh Pothan as Security
* Mano Jose as Sugunan
* Soubin Shaher as Poovalan
* Muthumani as Sicily
* Shirly Somasundaran as Annamma Chacko
* Jayaraj as Chacko Mash
* Sankar T. K. as Sicilys husband
* Ajith Vijayan as Ambiswamy
* Shanthi Sunil as Ambiswamys wife
* Karan Rahul as Schoolboy
| valign="top" |
* Fahadh Faasil as Ajmal
* Asmita Sood as Aami
* Honey Rose as Nancy
* Chemban Vinod Jose as Joshy
* Vinayakan as Chandran
|}

==Production==
The first film Sethulakshmi was by cinematographer Shyju debuting as a director in the film.  The film was an adaptation of the story Photo by   was signed to play a photographer in the film, setting foot into Malayalam films. 

Sameer Thahir cast Isha Sharvani opposite Nivin Pauly in his segment.  Isha, who will be making her Malayalam debut with this film, said that Sameers story - written by Sijoy Varghese - explores the love between two strangers.  This segment was completed in January 2013. 

Aashiq Abus featurette Gauri (earlier titled Naayika) would be a period film about a group of people going in search of a promised land.    Biju Menon and Kavya Madhavan were signed to play the lead. Singer Rimi Tomy stated that she will be acting opposite Tini Tom, making her acting debut.  In March 2013, Jayasurya was also signed up for a role.  The shoot of the portion was held in Munnar and other locales. 

Amal Neerad cast Dulquer Salmaan and Reenu Mathews, who acted in Lal Joses Immanuel (film)|Immanuel, as the lead pair for his portion. A debutant Jinu Ben was given a prominent role. Neerad said that the story was loosely based on an ancient Chinese short story, The Tall Woman and Her Short Husband. 

Fahadh Faasil stated that he and Rima Kallingal would pair up in Anwar Rasheeds segment of the anthology.  Rima Kallingal was replaced and Honey Rose and Asmitha Sood play the lead female roles in this film. Anwar said that the story unfolds on an eventful night and can be called a road movie. 

The directors pitched in for each other as well. Anwar Rasheed’s portion was filmed by Amal Neerad, Shyju Kahlid handled the camera of Sameer Thahir’s film, Aashiq Abu’s film was shot by Rajeev Ravi and Amal Neerad chose his assistant Ranadive to crank the camera. 

==Soundtrack==
{{Infobox album
| Name = 5 Sudarikal
| Type = Soundtrack
| Artist = Gopi Sundar,Yakzan Gary Pereira,Bijibal,Prashant Pillai
| Cover = 
| Caption = 
| Released = 2013
| Recorded =  Feature film soundtrack
| Length =
| Online Promotion =
| Label = Mathrubhumi Music 
| Producer = Amal Neerad
| Last album1 =   (2013)
| This album1 = 5 Sundharikal   (Title, Kullande Bharya)
| Next album1 =Buddy (2013 film)|Buddy   (2013)
}} 
The list of songs is as follows:
{| class="wikitable"
|-
! S.No !!Song !!Segment !!Singer !!Music Director !!Notes
|- Anna Katharina Gopi Sunder||
|- Kunal Ganjawala,Sreya Gopi Sunder||Theme song
|- Neha Nair||Yakzan Gary Pereira||
|- Neha Nair||Yakzan Gary Pereira||
|- Gayatri Asokan||Bijibal||
|- Preeti Pillai||Prashant Pillai||
|- Preeti Pillai||Prashant Pillai||
|}

== Awards ==
* Kerala State Film Award for Best Child Artist - Baby Anikha

==References==
 

==External links==
*  
*   (The Hindu)
*   (The New Indian Express)
*   (Malayala Manorama)
*   (Sify.com)
*   (Rediff.com) ( )
*   (Nowrunning.com) ( )

 

 
 
 
 
 
 
 