Swing Parade of 1946
{{Infobox film
| name = Swing Parade of 1946
| image = File:Swing Parade of 1946 poster.jpg
| caption = Film poster
| director = Phil Karlson
| producer = Lindsley Parsons Harry A. Romm Tim Ryan Phil Regan Moe Howard Larry Fine Curly Howard
| music = Edward J. Kay
| cinematography = Harry Neumann Richard Currier
| distributor = Monogram Pictures Corporation (1946) Legend Films (2007)
| released =  
| runtime = 74 01"
| country = United States
| language = English
| budget =
}} musical comedy Phil Regan, Will Osborne Stormy Weather" and "Caldonia".

==Plot==
 
The Three Stooges. as dishwashers, help an aspiring singer, Carol Lawrence (Storm), and a nightclub owner, Danny Warren (Regan), find love.

==Three Stooges appearance==
The Stooges rework several bits they performed with   and some of the waiter gags are borrowed from Beer and Pretzels.

Swing Parade of 1946 was filmed near the end of Curly Howards career. The 42-year-old comedian had suffered a series of minor strokes several months prior to filming, and his performances in their Columbia shorts at that time were often halting and slow. By the time of Swing Parade of 1946, he had lost a considerable amount of weight, and lines had creased his face. The scene where he recreates the maze of pipes from A Plumbing We Will Go (1940) has been described as sluggish and lethargic. 

Director Nicholas Ray worked on the screenplay. His work was uncredited. This was Rays only collaboration with the Three Stooges.

==See also==
*The Three Stooges filmography

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 
 