The Straw Hat
{{Infobox Film
| name           = The Straw Hat
| image          = 
| image_size     = 
| caption        = 
| director       = Leonid Kvinikhidze
| producer       = 
| writer         = Leonid Kvinikhidze
| narrator       = 
| starring       =  Isaac Schwartz
| cinematography = Yevgeni Shapiro
| editing        = Aleksandra Borovskaya
| studio         = Lenfilm
| distributor    = 
| released       = 1974
| runtime        = 127 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} play by Isaac Schwartz.

==Plot summary==
The extravagant life of a charming rentier who thought nothing of consequences. But living on credit finally wakes him up, forcing to end his bachelors life by marrying the daughter of a wealthy earl for her money. All goes well until a horse eats a certain straw hat, triggering a series of farcical vents interwoven with the marriage.

==Cast== Andrei Mironov
* Vladislav Strzhelchik
* Zinovi Gerdt
* Yefim Kopelyan
* Lyudmila Gurchenko
* Yekaterina Vasilyeva
* Alisa Freindlich
* Mikhail Kozakov
* Igor Kvasha
* Aleksandr Benyaminov
* Vladimir Tatosov
* Irina Maguto
* Marina Starykh
* Sergei Migitsko
* Mikhail Boyarsky
* Yelena Yefimova

== Songs from the film ==
* Im getting married, Im getting married...
* Song about lovers
* Song about the straw hat
* Song about a cornet
* Song about a cheated husband
See   for the text of the songs  

==External links==
*  

 
 
 
 
 
 
 


 
 