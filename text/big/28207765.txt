Any Rags?
{{Infobox Film
| name           = Any Rags?
| image          = 
| image_size     = 
| caption        = 
| director       = Dave Fleischer
| producer       = Max Fleischer
| animator       = Willard Bowsky Thomas Goodson
| writer         = 
| narrator       = 
| starring       = Little Ann Little
| music          = Sammy Timberg Lou Fleischer
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = 2 January 1932
| runtime        = 6 minutes
| country        =    English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Any Rags? is a 1932 Fleischer Studios Talkartoon animated short film starring  Betty Boop, Bimbo (Fleischer)|Bimbo, and Koko the Clown.

==Synopsis==
Bimbo the garbage man walks the streets asking townsfolk "Any Rags?" (during which he strips peoples clothes off and takes other things that are not really garbage as trash). He comes across Betty Boop who throws her garbage to him from her window. Bimbo then auctions all the garbage he has collected from his cart to a crowd which includes Koko the Clown, who purchases a bowtie. When Bimbo opens Bettys garbage bag, Betty Boop leaps out and kisses Bimbo. The cart then rolls down the hill and turns into a home for Betty and Bimbo.

==See also==
*Talkartoon

==External links==
* 
* 

 
 
 
 
 


 