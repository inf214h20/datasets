Bhagavad Gita (film)
{{Infobox film
| name           = Bhagavad Gita
| image          = Vishnuvishvarupa.jpg
| director       = G. V. Iyer
| based on       = Bhagavad Gita
| writer         = Bannanje Govindacharya
| producer       = T. Subbarami Reddy
| starring       = Neena Gupta Gopi Manohar G. V. Ragahvendra Govindh Rao 
| released       = 1993
| runtime        = 140
| cinematography = Madhu Ambat
| cinematography = N. Swamy
| country        = India
| language       = Sanskrit Telugu
| music          = Mangalampalli Balamuralikrishna
}}
Bhagavad Gita or Bhagvad Gita: Song of the Lord is a 1993 Sanskrit film produced by T. Subbarami Reddy, and directed by G. V. Iyer. The film is based on Hindu religious book Bhagavad Gita, which is part of the epic Mahabharata. The film was premiered at the Andhra Pradesh Film Chamber of Commerce in Hyderabad, India, and International Film Festival of India.  The film has garnered the National Film Award for Best Feature Film in 1993. 

==Awards==
;40th National Film Awards
*National Film Award for Best Feature Film      

==See also==
*G. V. Iyer
*Adi Shankaracharya (film)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 

 