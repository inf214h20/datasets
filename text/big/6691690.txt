The Tooth Fairy (film)
 
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = The Tooth Fairy
| image = The Tooth Fairy 2006 poster.jpg
| caption = Promotional poster
| director       = Chuck Bowman
| producer       = Stephen J. Cannell
| writer         = Stephen J. Cannell  Peter Wong  Ben Cotton  Carrie Fleming 
| music          = Jon Lee, Richard John Baker
| cinematography = David Pelletier Anchor Bay
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
}}
The Tooth Fairy is an English-language horror film released on August 8, 2006 on DVD in the USA and Canada. The runtime is 89 minutes.

==Plot==
When 12-year-old Pamela goes on vacation with her family to a bed and breakfast, the girl who lives next door tells her the "true story" of the Tooth Fairy: Many years earlier, the evil Tooth Fairy slaughtered a countless number of children to take their teeth, and now she has returned to kill Pamela and anyone else who gets in her way. The "tooth fairy" pursues the victims unrelentingly, which leads to a gruesome collection of events. 

==Cast==
*Lochlyn Munro as Peter Campbell
*Chandra West as Darcy Wagner
*Steve Bacic as Cole
*Nicole Muñoz as Pamela Wagner
*Jianna Ballard as Emma
*Carrie Fleming as Star Roberts
*Peter New as Chuck
*Jesse Hutch as Bobby Boulet
*Ben Cotton as Henry
*Sonya Salomaa as Cherise
*Karin Konoval as Elisabeth Craven
*Sam Laird as Austin Carter
*P.J. Soles as Mrs. MacDonald Peter Wong as a Tooth Fairy
*Peng Zhang Li as a Tooth Fairy
*Ming Jian Huang as a Tooth Fairy
*Aaron Au as a Tooth Fairy
*John Hambridge as a Tooth Fairy
*Micki Maunsell as Maude Hammond
*Brent Chapman as Chief Claude
*Madison J. Loos as Donnie Carter
*Kurt Max Runte as Cabbie

==External links==
* 

 
 
 
 
 
 
 


 