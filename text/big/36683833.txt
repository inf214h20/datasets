Io e mia sorella
 
{{Infobox film
 | name = Io e mia sorella (My sister and I)
 | image =  Io e mia sorella.jpg
 | caption =
 | director = Carlo Verdone
 | writer = Leo Benvenuti, Piero De Bernardi, Carlo Verdone
 | starring = Carlo Verdone, Ornella Muti, Elena Sofia Ricci
 | music =  Fabio Liberatori
 | cinematography = Danilo Desideri
 | editing =  	 Antonio Siciliano Mario and Vittorio Cecchi Gori
 | distributor = Cecchi Gori Group
 | released = 1987
 | runtime = 110 min
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }}
 1987 Cinema Italian comedy Best Supporting the same best actress (Ornella Muti) and a David di Donatello for Best Script. 

==Plot==
Serena and Carlo are happily married in Italy, but the arrival of his sister Silvia, disrupts the peaceful family balance. In fact the woman, when was a young girl, went in Hungary and married an athlete with whom she had a son. But now the couple, when comes to Hungary for a promise made to dying mother of Carlo, discovers that Silvias husband had a serious accident and now is paralyzed. So its up to Carlo and Serena helping the poor Silvia to look after the baby.

==Cast==
*Carlo Verdone: Carlo Piergentili
*Ornella Muti: Silvia Piergentili
*Elena Sofia Ricci: Serena
*Mariangela Giordano: Nadia
*Galeazzo Benti: Lawyer Sironi
*Tomas Arana: Gábor
*Veronica Lazar: Judge

==References==
 

==External links==
* 

 
 
 
 


 