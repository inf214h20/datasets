I Travel Alone
{{Infobox film
| name     = I Travel Alone
| image    =
| director = Stian Kristiansen
| writer = 
| producer = 
| Script = 
| starring = Rolf Kristian Larsen Gustaf Hammarsten Ingrid Bolsø Berdal
| cinematography = 
| music = 
| country = Norway
| language = Norwegian
| runtime = 94 minutes
| released =  
}}
I Travel Alone ( ) is a 2011 Norwegian drama film directed by Stian Kristiansen. It is a sequel to The Man Who Loved Yngve from 2008 and was followed by the prequel The Orheim Company in 2012.

== Plot ==
Jarle Klepp is 25 year old literature student (onomastic, Marcel Proust) at the university of Bergen in Norway who suddenly finds out he has a 7 year old daughter, because the mother (with whom he once had a one night stand) sends her to him to take care for her for a week. He struggles with his new role, loses his girlfriend to his teacher and gets again close to the mother during a costume party for the children...

== External links ==
* 

 
 
 
 
 

 