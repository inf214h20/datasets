Gloria Victoria
{{Infobox film
| name = Victoria Gloria
| image= Gloria Victoria poster.jpg
| caption = Film poster
| director = Theodore Ushev
| producer = Marc Bertrand
| studio = National Film Board of Canada
| released =  
| country = Canada
| runtime = 6 m 56 sec
}}
Victoria Gloria is a 2013 3D film|3-D anti-war film|anti-war animated short by Theodore Ushev, produced in Montreal by the National Film Board of Canada (NFB). A film without words set to the music of Shostakovichs Leningrad Symphony, Victoria Gloria is final film in a trilogy of NFB animated shorts by Ushev on art, ideology and power, following Tower Bawher (2005) and Drux Flux (2008).      

Gloria Victoria uses mixed media techniques to create images that evoke bloody battlefields and the horrors of war, with imagery of combat and massacres from the bombing of Dresden in World War II to Guernica (painting)|Guernica, from the Spanish Civil War to Star Wars, in a style inspired by expressionism, Constructivism (art)|constructivism, cubism as well as surrealism. Ushev has stated that the film is "less geometric" in its animation than his previous works and that he sought to flatten the 3-D effect as the film progressed towards its climax, so as convey the emotion of a human being at war.    

The seven-minute film took Ushev two years to create, using traditional animation and computer animation tools. 

==Release and reception== FIPRESCI critics prize at the 2013 Annecy International Animated Film Festival in June 2013. Gloria Victoria was screened as part of the Short Cuts Canada program at the 2013 Toronto International Film Festival.  It was named as Most Well Liked Animated Short of 2013 in a survey of fifteen festival programmers and critics, selected by eleven out of fifteen people surveyed. The film also got a nomination at 2014 Hollywoods Annie Awards.  

==References==
 

==External links==
* Watch   at the National Film Board of Canada
* 

 
 
 
 
 
 
 
 
 
 
 


 
 