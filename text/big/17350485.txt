Jenny and the Soldier
{{Infobox film
| name           = Jenny and the Soldier
| image          = Soldaten og Jenny.jpg
| caption        = DVD Cover for the film Soldaten og Jenny
| director       = Johan Jacobsen
| producer       = John Olsen
| writers        = Johan Jacobsen C.E. Soya
| starring       = Poul Reichhardt Bodil Kjer
| music          = Kai Møller
| cinematography = Aage Wiltrup
| editing        = Anker Sørensen
| studio         = Saga Studios
| distributor    = 
| released       = 30 October 1947 (Denmark) 1948 (USA)
| runtime        = 92 min.
| country        = Denmark Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
| tv_rating      = 
}}
 1947 Denmark|Danish Danish playwright Bodils for their leading roles. Jenny and the Soldier is one of the ten films listed in Denmarks cultural canon by the Danish Ministry of Culture. 

==Synopsis==

Two common working-class people, Robert and Jenny, meet one day in a bar. After Robert defends Jenny against her date, a district attorneys chauffeur, who is trying to get her drunk, they begin a relationship. She soon discovers that she will be tried in court for an abortion she needed a few years previously. Her parents disown her and she loses her job as a saleswoman. Robert stays with her, admitting his own responsibility for killing someone when he was 17 years old. Distraught at the thought of going to prison, Jenny convinces Robert to commit suicide with her. Just as she is planning the suicide, her lawyer comes and announces that the case has been dropped—the case had been delayed too long while the district attorney was in the hospital after his chauffeur has caused an automobile accident.

==Cast==
* Poul Reichhardt as Robert Olsen, Soldier
* Bodil Kjer as Jenny Christensen
* Elith Pio as District Attorney
* Karin Nellemose as District Attorneys Wife
* Johannes Meyer as Jennys father
* Maria Garland as Jennys mother
* Sigfred Johansen as Prosecutor
* Gunnar Lauring as Gustav, District Attorneys Chauffeur
* Svend Methling as Sveistrup, "The Crazy Lawyer"
* Jessie Rindom as Gartnerkonen (as Jessie Lauring)
* Per Buckhøj as Bartender
* Kirsten Borch as District Attorneys Maid
* Birgitte Reimer as Prosecutors Secretary

 
==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 