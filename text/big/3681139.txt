Necessary Roughness (film)
{{Infobox film
| name = Necessary Roughness  
| image =  Necessary roughness poster.jpg 
| caption = Theatrical release poster
| writer = Rick Natkin David Fuller
| starring = Scott Bakula Héctor Elizondo Robert Loggia Harley Jane Kozak
| director = Stan Dragoti
| music = Bill Conti  
| cinematography = Peter Stein John Wright
| distributor = Paramount Pictures
| released = September 27, 1991
| runtime = 108 minutes
| language = English
| producer = Howard Koch Jr. Lis Kern Mace Neufeld Robert Rehme
| budget = 
| gross =  $26,255,594 (USA) 
}} Larry Miller, Sinbad (entertainer)|Sinbad, Jason Bateman, Kathy Ireland, Rob Schneider, and Fred Dalton Thompson.
 season at football team Texas State Southern Methodist University football team by the NCAA in 1987 for team violations very similar to the ones that the fictional Texas State is accused of.
 Roger Craig, Randy White.  The film also has some cameo appearances from Chris Berman and Evander Holyfield.
 Fort Worth, Denton were the primary locations used for filming.  The University of North Texas in Denton was a major location for filming football and college scenes. Texas States green and white uniforms in the movie are exactly the same colors worn by North Texas.

== Plot ==
The Texas State University Fightin Armadillos were once one of the most powerful teams in college football. After winning consecutive national championships, massive NCAA violations resulted in the program having to forfeit years worth of victories. All of the previous players and coaches are banned from returning except Charlie Banks, the only "clean" player, who never got to play despite having "heart".

The new coaching staff, led by Ed "Straight Arrow" Gennero (Elizondo), is forced to build an almost entirely new team with little assistance.  No athletic scholarships are available, forcing them to hold tryouts. Along with this, they must worry about Dean Elias (Miller), who wants the team to fail so he can scrap it. The coaches soon have a makeshift team in place.
 offensive and defensive squads.  The Armadillos are thus forced to play ironman football.  The team lacks experience and talent in all areas, especially at quarterback, placekicker, and the defensive line.  Assistant coach Wally "Rig" Riggendorf (Loggia) finds Paul Blake (Bakula), a 34-year-old high school star who never attended college due to his fathers death.  Rig convinces him to enroll and become the Armadillos quarterback.

Blake arrives on campus and catches everyones attention due to his age, especially Professor Carter (Kozak). Blake then recruits a graduate student teaching assistant named Andre Krimm (Sinbad), who is also enrolled at the school and still has some eligibility remaining. Blake convinces him to join, and he is positioned on the defensive line, where he excelled years earlier. Even with the new members, the team is unable to win. Things get so bad that, at one point during the film, announcer Chuck Neiderman (Schneider) covers his microphone with his hands and screams, "SHIT!!!!" at the top of his lungs.

Carter tells Blake that she knows him from years earlier.  Carters ex-boyfriend was a high school football star, but Blake and his team humiliated him and his team in a championship game. This episode actually caused Carter to become infatuated with Blake. Now, years after the fact, the two begin a romantic relationship which Dean Elias opposes, due to the fact that Blake is a student and Carter is a teacher.
 Kansas (in real life, Kansas holds the all-time NCAA Division I-A record for number of tie games with 57 ). After this game, Blake quits the team after arguments with Gennero and Carter, but convinces himself to come back after a teammate, who is also quitting, inadvertently changes his mind and both come back.
 point after attempt and pass for two.  Blake scrambles and finally finds Banks in the end zone to win the game.

== Characters ==

===The Team===
* Scott Bakula - Paul Blake
* Héctor Elizondo - Coach Ed Gennero
* Robert Loggia - Coach Wally Riggendorf Sinbad - Andre Krimm
* Jason Bateman - Jarvis Edison
* Andrew Bryniarski - Wyatt Beaudry
* Duane Davis - Featherstone
* Michael Dolan - Eric Samurai Hansen
* Marcus Giamatti - Sargie Fumblina Wilkinson
* Kathy Ireland - Lucy Draper
* Andrew Lauer - Charlie Banks
* Louis Mandylor - McKenzie Peter Tuiasosopo - Manumana "the Slender"

===The University===
* Harley Jane Kozak - Dr. Suzanne Carter Larry Miller -	Dean Phillip Elias
* Fred Dalton Thompson - University President Carver Purcell
* Rob Schneider - Chuck Neiderman

== Reception ==
Necessary Roughness received mixed to negative reviews by critics. The film earned a "Rotten" rating of 32% on  ." Sports Illustrated, in a criticism of sports movies in general, stated that "Kathy Ireland is probably the best thing in this laugh-a-quarter film", while the Los Angeles Times called it "A by-the-numbers, but enjoyable sports comedy". 
 rocks for jocks", and alumni giving payoffs to college players. He further said it was good to see a college being forced to use genuine students for its football team and anticorruption practices which prevent the aforementioned favoritism and big money. 

==Texas State Irony==
One of the teams that Texas State plays in the film is the Southwest Texas State University Bobcats. A few years after the release of the film, Southwest Texas State became Texas State University after an official name change and change of the Universitys status. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 