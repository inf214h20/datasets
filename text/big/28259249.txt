Stir (film)
 
 
{{Infobox film
| name           = Stir 
| image          = Stir film.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Stephen Wallace
| producer       = Richard Brennan
| writer         = Bob Jewson
| narrator       = 
| starring       = Bryan Brown Max Phipps
| music          = Cameron Allan
| cinematography = Geoff Burton
| editing        = Henry Dangar
| studio         = New South Wales Film Corp. Smiley Productions
| distributor    = Hoyts Distribution
| released       = 23 October 1980 (AUS)
| runtime        = 101 minutes 
| country        = Australia
| language       = English
| budget         = AU$485,000 David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p330 
| gross          =
}} 1980 Cinema Australian film Gladstone and the Flinders Ranges in South Australia.  It premiered at the 1980 Cannes Film Festival. 

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Bryan Brown || China Jackson
|-
| Max Phipps|| Norton
|-
| Garry Waddell || Dave
|-
| Phil Motherwell  || Alby
|- Robert Noble || Riley
|}

==Production==
Bob Jewson was a former prisoner who had been in Bathurst Gaol at the time of the riot. He wrote a script based on it originally called Bathurst. Martha Ansara who was working for the Prison Action Group read it and introduced Jewson to Stephen Wallace. (Other accounts say it was Tony Green who did this. Barbara Alysen, "Stephen Wallace", Cinema Papers, Oct-Nov 1980 p341-343 ) Wallace decided to make the film David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p226-228 

The New South Wales Film Corporation were looking at investing in some films made by directors who had made successful short films. Wallace had just made the acclaimed on hour drama The Love Letters from Teralba Road and they asked him if he had any projects. He told them about his prison drama. 

As part of his preparations, Wallace had the actors do a clown workshop for four days and a longer workshop of three weeks. Most of them disliked the clown workshop and Wallace later admitted was a mistake. 

The original drafts of the script had some female characters, such as a social worker and a girlfriend of Bryan Browns character, but these were dropped. 
 
The film was shot over five weeks in October and November 1979 in South Australia at an abandoned prison in Gladstone.  They had trouble sourcing enough extras and had to fly them in from Adelaide; some of the actors who did appear had been to prison. Wallace tried to get more aboriginal extras but was unable to. 

During filming the movie was known as The Promotion of Mr Smith until Jewson suggested the shorter title Stir.  Wallace:
 Bob Jewson said one thing - and I think this is what we tried to make the theme of the film, although it was very hidden - that riots dont happen out of the blue. The prison authorities make you believe that all these criminals that are incarcerated are at all times dangerous and theyre trying to get out. But Bob said thats never true; most of them have accepted their lot and theyre trying to serve their time. They only get into a riot situation when theyre treated badly and unfairly over a long period. He said most people dont want a riot; they know what its going to mean, longer in jail.
   accessed 21 November 2012  

==Reception==
The film was reasonably popular and according to Wallace it made a profit. 

===Critical reception===
David OConnell of In Film Australia:
  by Bob Jewson’s screenplay, which carries with it a certifiable authenticity, being based on his own time inside during the Bathurst prison riots of February 1974... Brutal and profane, Stir is a no-holds-barred, revelatory glimpse into life inside and rates as an Australian classic. The dialogue is loose and jagged and these men, as part of a uniformly excellent ensemble, look every bit the part of desperate beaten-down prisoners. }}

==DVD availability==
The DVD is available from Umbrella Entertainment and contains a new 16:9 aspect ratio transfer, a 50 minute interview feature with key cast and crew, as well as the original theatrical trailer. 

==See also==
*Cinema of Australia

==References==
 

== External links ==
* 
* 
*  at Australian Screen Online
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 