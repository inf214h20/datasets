Native New Yorker (film)
{{Infobox film
| name           = Native New Yorker
| image          = Native_New_Yorker_DVD.jpg
| caption        = 
| director       = Steve Bilich
| producer       = Steve Bilich and William Susman
| writer         = Steve Bilich
| music          = William Susman
| cinematography = Steve Bilich
| editing        = Steve Bilich
| distributor    = Amazon and TFI Reframe Collection
| runtime        = 13 minutes
| country        = United States
| budget         = United States dollar|US$ $2500
}}

Native New Yorker (2005) is the title of the 2006 Tribeca Film Festival Best Documentary Short    by Steve Bilich. 

Filmed with a 1924 hand-crank Cine-Kodak camera,  Shaman Trail Scout Coyote takes a journey which transcends time, from Inwood Park (where the island was traded for beads and booze), down a native trail (now Broadway), into lower Manhattan (sacred burial ground, now including the newest natives of this island empire).

Shot before, during and after 9/11, Native New Yorker took several years of filming, with a running length of 13 minutes. This is a film by Steve Bilich with an original score composed by William Susman.

New Internationalist calls Native New Yorker   "...a conventionally unclassifiable short... In 13 minutes it brilliantly encapsulates aeons." 

"...the stuff dreams - and nightmares - are made of."  -The Austin Chronicle

In 2009, the film score to Native New Yorker was released on a CD entitled Music for Moving Pictures. 

In 2011, the Tribeca Film Institute selected Native New Yorker for inclusion   in their Reframe Collection which "shares the best of our visual heritage."  

In 2013, the Sound of Silent Film Festival  screened Native New Yorker with a live orchestra
  at the Anthology Film Archives in New York City.

In 2014, The 50th Pesaro Film Festival in Pesaro Italy featured Native New Yorker in Panorama U.S.A. – Il cinema sperimentale-narrativo nel nuovo millennio 



==Festivals==

*8th Athens Avant-Garde Film Festival - Athens, Greece
*50th Pesaro Film Festival - Pesaro, Italy
*Sound of Silent Film Festival - New York, NY
*Native Spirit Film Festival - London, UK
*Rooftop Film Festival - New York, NY
*Cinestrat Film Festival - Alicante, Spain
*Austin Jewish Film Festival - Austin,Texas
*WILDsound Film Festival - Toronto, Canada
*Sebastopol Documentary Film Festival - Lincoln, Nebraska
*Moondance International Film Festival - Los Angeles, California
*Expresion en Corto International Film Festival - Guanajuato, Mexico
*Global Voices/UNAFF - Harvard Cambridge, Mass.
*Tribeca Film Festival - New York, NY
*Cinequest Film Festival - San Jose, California
*Tiburon International Film Festival - Tiburon, California
*Park City Film Music Festival - Park City, Utah
*Raindance Film Festival - London, United Kingdom
*United Nations Association Film Festival - Palo Alto, California
*LA Shorts Fest - Los Angeles, California
*Rome International Film Festival - Rome, Georgia
*Vancouver International Film Festival Vancouver, Canada
*Action/Cut Short Film Competition - Los Angeles, California
*Avignon Film Festival - Avignon, France
*Moondance International Film Festival - Boulder, Colorado

==Awards==

* Tribeca Film Festival - WINNER Best Documentary Short ( 2006)
* Park City Film Music Festival - WINNER Audience Choice – Best Impact of Music (2006)
* Park City Film Music Festival - WINNER Gold Medal Jury’s Choice - Artistic Excellence (2006)
* Moondance International Film Festival - WINNER Columbine Award  (2005)

==Broadcasts==

* Aboriginal Peoples Television Network (APTN) (throughout Canada)
* PBS / WNET – Reel New York (New York)
* Comcast (throughout the United States)

== References ==
 
 

==External links==
*  - Official Website
*  - 2006 Tribeca Film Festival Awards
*  - Native New Yorker (2005)
*  - Native New Yorker Film Festival Awards
*  - Tribeca Film Institute Online Portal for Independent Film
*  - Reel New York Film Festival
*  - Available at Amazon in 2011
*  - Released on album entitled Music For Moving Pictures in 2009

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 