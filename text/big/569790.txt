Prime Cut
{{Infobox film
| name           = Prime Cut
| image          = Primecut.jpg
| image_size     = 
| caption        = Theatrical release poster by Tom Jung Michael Ritchie
| producer       = Joe Wizan
| writer         = Robert Dillon
| starring       = Lee Marvin Gene Hackman Sissy Spacek
| music          = Lalo Schifrin
| cinematography = Gene Polito
| editing        = Carl Pingitore
| studio         = Cinema Center Films
| distributor    = National General Pictures
| released       = United States: June 28, 1972
| runtime        = 88 minutes
| country        = United States|U.S.A. English
| budget         = 
}} 1972 United American film Michael Ritchie, with a screenplay written by Robert Dillon.
 mob enforcer from Chicago sent to Kansas to collect a debt from a meatpacker boss played by Gene Hackman. The female Lead, Golden Globe nominated actress, Angel Tompkins, plays the wife of Gene Hackman which was once romantically involved with Lee Marvin., Jack Sowards, author of "The Wrath of Khan" declared that one scene between Ms. Tompkins and Marvin is the "hottest" moment in a film with two actors wearing clothes  .  Sissy Spacek appears in her first credited on-screen role as a young orphan being sold into prostitution; she appears naked in a pen in one scene.
 female slavery, combine in an open field; In that scene, Angel Tompkins and Lee Marvin are being pursued and the combine actually eats their car, a Cadillac limousine and bales it .

== Plot ==
The movie opens with a credit sequence that follows a slaughterhouse process in from the unloading of the cattle to the making of sausages. A short cut to a human body at the beginning, as well one in which a wristwatch and a shoe appear on a conveyor line make it clear that a human cadaver is processed among the cattle. The woman operating the sausage machine is interrupted by Weenie (Gregory Walcott), who has timed the machine using his watch. He stops the machine, wraps up a string of sausages, and marks the package with an address in Chicago.

It is later revealed that Weenie is the brother of Mary Ann (Gene Hackman), the crooked operator of the slaughterhouse in Kansas City, Kansas. The particular sausages that Weenie was wrapping were made from the remains of an enforcer from the Chicago Italian mob sent to Kansas City to collect $500,000 from Mary Ann.

After the head of the Irish mob in Chicago receives the package, he contacts Nick Devlin (Lee Marvin), an enforcer with whom he has worked previously, to ask him to go to Kansas City to collect the debt. He tells Devlin about the sausages and that another enforcer sent to Kansas City was found floating in the Missouri River.

Devlin agrees to the fee of $50,000 and asks for some additional muscle. He gets a driver and three other younger members of the Irish mob as help, including the young OBrien (Les Lannom), who makes Devlin meet his mother as he leaves Chicago.

It is later revealed that Devlin and Mary Ann have a shared history involving Mary Anns wife Clarabelle (Angel Tompkins), who previously had an affair with Devlin and is now married to Gene Hackmans character. .

Devlin and his men drive to Kansas City. They stop at a flophouse where Devlin finds Weenie in an upstairs room. He beats him up and tells him to inform Mary Ann that he is in town to collect the debt.

The next day Devlin and his men drive out into the prairie and find Mary Ann in a barn where he is entertaining guests at a white slave auction. He demands the money from Mary Ann, who tells him to come to the county fair the next day to get it. Mary Ann tells Devlin that Chicago is "an old sow, begging for cream" that should be melted down.

As they are standing next to a cattle pen with naked young women offered for auction, one of them, who is later revealed to have the name Poppy (Sissy Spacek), begs Devlin for help. Devlin takes her with him "on account". Back at the hotel he puts Poppy in a large bed in their suite to recuperate. When she wakes up, she tells Devlin about her history of growing up at an orphanage in Missouri with her close friend, Violet (Janit Baldwin), before they were both brought to the slave auction. Devlin promises to take her to the fair the next day.

The next day at the county fair, in the midst of a livestock judging competition, Mary Ann gives Devlin a box that supposedly contains the money. When Devlin cracks the box open on the spot, he finds it contains only beef hearts. Betrayed by Mary Ann, whose men are about to take him away, Devlin is able to escape with Poppy after Violet distracts Weenie, who claimed her after the auction.

Mary Anns men chase Devlin, his men and Clarabelle through the fair. OBrien is killed underneath a viewing stand. Devlin and Angel Tompkins as Clarabelle run off into a nearby wheat field, where they escape detection. When they try to leave the field, however, they are chased by a combine harvester operator. Clarabelle falls while running and they are nearly sliced up by the machines blades.

Devlin and Clarabelle are saved by the arrival of Devlins men in their car, which they abandon and let ram into the front of the combine. Devlins driver shoots the combine operator. The entire car is demolished by the threshing apparatus and turned into bales of hay and metal.

With their car destroyed, Devlin and his men hitch a ride back into Kansas City on a truck. Devlin jumps off the truck near the river and sends the rest of them with Poppy back into town. He enters a houseboat, the luxurious accommodation of Clarabelle, purchased for her by Mary Ann; she is there alone. He gets information from her on the whereabouts of Mary Ann. Clarabelle attempts to seduce him, but he rebuffs her. Clarabelle tells him she would be perfectly happy being a widow and joining Devlin again.

When he returns to the hotel with a new car for his crew, Devlin finds an ambulance out front, with one of his men being hauled away. He learns that Mary Anns men ambushed them and took Poppy.  When he returns to Weenies hotel to look for him, he finds that Violet has been gang-raped, apparently as a warning to what will happen to Poppy.

He and his two remaining men drive out to Mary Anns farm to finally take care of business. On the way, Devlin takes out a Smith & Wesson M76 submachine gun from a case.

Devlin stops the car on the edge of a field of sunflowers near Mary Anns farm. They approach the farm through the field and engage in a long gun battle with Mary Anns men. Both of Devlins men are hit. He tells them to stay behind while he advances with the submachine gun. Unable to get past Mary Anns men, he stops a truck hauling livestock, commandeers it and uses it to ram the gate and smash into the greenhouse on the farm, demolishing it.

Devlin kills several of Mary Anns men then advances into the barn where Mary Ann and his brother are holding Poppy. From behind a bale of hay, he hits Mary Ann, who falls injured down into a pig pen. Enraged at seeing his brother shot, Weenie runs toward Devlin, who kills him. As he dies, Weenie tries to stab Devlin with a sausage.

Devlin carries Poppy out of the barn. They pass the wounded Mary Ann, flat on his back, next to a sow pen. Mary Ann taunts Devlin to kill him, telling him to finish him off, like he would an animal. Devlin tells him that since Mary Ann is a man, not an animal, he wont do that. He walks away, leaving Mary Ann to die on his back.

In a little known intro to the film, an unusual sexual relationship between the two brothers is hinted and later clarified during a fight scene, which is where the brother, "Weenie," earned his nickname  ....   Naturally, at the time, it was impossible to put this piece of the plot into an American film because of the mores of the American viewing audience  

==Credits==

===Production===
*Mickey Borofsky - associate producer  
*Kenneth L. Evans - executive producer  
*Joe Wizan - producer
*Michael Ritchie - director
*Robert Dillon - screenplay
*Carl Pingitore - editor

===Cast===
*Lee Marvin - Nick Devlin
*Gene Hackman - Mary Ann
*Angel Tompkins - Clarabelle
*Gregory Walcott - Weenie
*Sissy Spacek - Poppy
*Janit Baldwin - Violet
*William Morey - Shay
*Clint Ellison - Delaney
*Howard Platt - Shaughnessy
*Les Lannom - OBrien
*Eddie Egan - Jake
*Therese Reinsch - Jakes Girl
*Bob Wilson - Reaper Driver
*Gordon Signer - Brockman
*Gladys Watson - Milk Lady
*Wayne Savagne - Freckle Face

== Trivia ==
*The scene involving Mary Ann at the end was adapted somewhat in the 1992 movie Unforgiven, written by David Webb Peoples and directed by Clint Eastwood, in which Hackman plays a villain who taunts his killer while lying wounded on his back after a gun battle.
* In 2009 Empire Magazine named it #14 in a poll of the 20 Greatest Gangster Movies Youve Never Seen* (*Probably).

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 