Max Hell Frog Warrior
{{Infobox film
| name           = Max Hell Comes to Frogtown
| image          = Max_Hell_Frog_Warrior.jpg
| caption        =
| director       = Donald G. Jackson Scott Shaw
| producer       = Donald G. Jackson Scott Shaw
| writer         = Donald G. Jackson Scott Shaw Jill Kelly
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 min.
| rating         =
| country        = United States
| language       = English
| budget         = $375,000  	  	
}} Jill Kelly.

==Plot==
This film follows the lead character, Max Hell, played by Scott Shaw, who goes on a mission to rescue Dr. Trixi T from the clutches of the evil Mickey OMalley, played by Joe Estevez.  According to Donald G. Jackson, Max Hell Frog Warrior is not so much a sequel as it is a standalone film inspired by the original concept for Hell Comes to Frogtown.

==Zen Filmmaking==

This film is considered a Zen Film in that it was created in the distinct style of filmmaking formulated by Scott Shaw known as Zen Filmmaking.  In this style of filmmaking no scripts are used; instead a rough plot is outlined including the basic scenes and locations and then the crew and actors improvise the rest, all dialogue and action is spontaneous and entire plot points, scenes and setpieces are formulated on the spot. Shaw and Jackson have claimed the technique offers freedom of creativity allowing for very natural performances from actors and a unique artistic outcome.

==Versions==

* An early cut  (under the name Toad Warriors) received an unofficial release from distributors who obtained a master copy of the film.

* A filmmaker-approved version appeared in 2002 under the name Max Hell Frog Warrior.

* A third version, truncated to 30 minutes, was assembled by Scott Shaw and released as Max Hell in Frogtown: A Zen Speed Flick.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 
 