Burning Annie
{{Infobox film
| name           = Burning Annie
| image          = BurningAnnie dvdcover.jpg
| caption        = WB/LightYear DVD
| director       = Van Flesher Randy Mack (uncredited)
| producer       = Randy Mack
| writer         = Randy Mack (uncredited) Zack Ordynans
| starring       = Gary Lundy Sara Downing Kim Murphy Brian Klugman Jay Paulson Rini Bell Todd Duffey Kathleen Rose Perkins
| music          = Dean Harada
| cinematography = Stephan Schultze   
| editing        = Randy Mack 
| distributor  =   Warner Brothers Lightyear Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
}}
Burning Annie is an American comedy film starring Gary Lundy, Sara Downing, Kim Murphy, Brian Klugman, Jay Paulson, Rini Bell, Todd Duffey, and Kathleen Rose Perkins. It is the feature directorial debut of Van Flesher.  The film is produced by Randy Mack of Armak Productions and distributed by Warner Brothers and Lightyear Entertainment.

==Plot==
A dark slacker comedy about a college student obsessed with Woody Allens film Annie Hall.  His world is then turned upside down by a young woman who might be the modern day equivalent of Annie Hall herself.

==Cast==
* Gary Lundy as Max  
* Sara Downing as Julie  
* Kim Murphy as Beth (as Kim Murphy Zandell)  
* Brian Klugman as Charles  
* Jay Paulson as Sam  
* Rini Bell as Amanda  
* Todd Duffey as Tommy  
* Kathleen Rose Perkins as Jen   David Hall as Andy 
* Jason Risner as Scott  
* Carrie Freedle as Sara  
* Keith Page as Mark

==Release==
After 3 years and 6 awards on the film festival circuit, the film went through several false starts with distribution deals and was ultimately acquired by LightYear Entertainment. The films theatrical run began February 7, 2007 at the Two Boots/Pioneer Theater in East Village, Manhattan, New York City. The film was released on DVD and Video-on-Demand by Warner Bros on March 20, 2007. It is available for streaming through Amazon Instant Video, in addition to traditional outlets like Best Buy and Netflix. No sequel is currently planned.

== Awards and Honors ==
2004 MassBay Film Festival (Worcester, MA.)
* Won - Best Feature 

2004 West Virginia Filmmakers Film Festival
* Won - Best Feature 

2004 Strictly Midwestern Movies and Short Hits (SMMASH)
* Won - Best Director
* Won - Best Screenplay
* Won - Best Actor
* Nominated - Best Supporting Actor
* Nominated - Best Actress
* Nominated - Best Feature
* Nominated - Audience Award 

2005 Seattles True Independent Film Festival (STIFF)|Seattles True Independent Film Festival (STIFF)
* Won - Best Debut Feature
* Won - Best Use of a Donnie Darko Cast Member 

==References==
 

== Sources ==
*  
*  
*  
* http://movies.nytimes.com/2007/02/07/movies/07burn.html
* http://www.variety.com/review/VE1117922407.html?categoryid=31&cs=1&p=0
* http://nymag.com/movies/listings/rv_46896.htm
* http://movies.tvguide.com/burning-annie/review/286227
* http://www.villagevoice.com/2007-01-30/film/burning-annie/

 
 
 
 
 
 


 