Swapna Sundari (film)
 
{{Infobox film
| name           = Swapna Sundari
| image          =
| caption        =
| director       = Ghantasala Balaramaiah
| producer       = Ghantasala Balaramaiah
| writer         = Samudrala Raghavacharya
| starring       = Akkineni Nageswara Rao Anjali Devi S. Varalakshmi Kasturi Siva Rao Mukkamala G. Varalakshmi Surabhi Balasaraswati
| music          = C. R. Subburaman
| cinematography = P. Sridhar
| editing        =
| studio         = Vauhini Studios
| distributor    =
| released       =  
| runtime        = 173 minutes
| country        = India
| language       = Telugu
| budget         =
}} Tamil with the same name.

==Plot==
Prabhu (ANR) dreamt of a lovely lady, his Dream Girl or Swapna Sundari. He encounters a tribal queen (G. Varalakshmi) while on tour, who falls in love with him, but he rejects her. He finally meets his Swapna Sundari (Anjali Devi) who is actually a Deva Kanya. The love is mutual. She leaves her Godly abode to stay with Prabhu. But an evil Mantrik (Mukkamala) finds out that with her help, he can conquer the world and become invincible so he kidnaps her. Prabhu, with the help of the tribal queen vanquishes the evil Mantrik, but the Rani dies. Swapna Sundari and Prabhu live happily ever after.

==Credits==
===Cast===
* Akkineni Nageshwara Rao	... Prabhu
* Anjali Devi	... Swapna Sundari
* G.Varalakshmi
* Kasturi Siva Rao
* Mukkamala
* G. Varalakshmi
* Surabhi Balasaraswati

===Crew===
* Director: Ghantasala Balaramaiah
* Screenplay, Lyrics and Dialogues: Samudrala Raghavacharya
* Producer: Ghantasala Balaramaiah
* Production Company: Pratibha Films
* Music Director: C. R. Subburaman
* Cinematography: P. Sridhar
* Choreography: Vedantam Raghavaiah
* Playback singers: Raavu Balasaraswathi, Ghantasala Venkateswara Rao and S. Varalakshmi

==Songs==
* Aagani Vegame Jeevitam (Singer: Ghantasala)
* Ee Seema Velasina Haayi (Singers: R. Bala Saraswathi Devi and Ghantasala)
* Natanalu Thelusune (Singer: R. Bala Saraswathi Devi; Composer: C. A. Subbaraman; Lyrics: Samudrala)
* Nee Sari Neevele (Singer: G. Varalakshmi; Composer: C. R. Subburaman; Lyrics: Samudrala Venkata Raghavacharya)
* Nijamaaye Kala Nijamaaye (Singer: Ghantasala)
* O Paradesi Mare Jaadala (Singers: Ghantasala, G. Varalakshmi)
* O Swapna Sundari.. Ninu Kaanaga Naitinigaa (Singer: Ghantasala)

==External links==
*  
*  
*  

 
 
 
 
 