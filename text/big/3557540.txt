The Hunt for Red October (film)
{{Infobox film
| name           = The Hunt for Red October
| image          = The Hunt for Red October movie poster.png
| caption        = Theatrical release poster
| director       = John McTiernan
| producer       = Mace Neufeld
| screenplay     = {{Plainlist| Larry Ferguson
*Donald E. Stewart
}}
| based on       =  
| starring       = {{Plainlist|
*Sean Connery
*Alec Baldwin
*Scott Glenn
*James Earl Jones
*Sam Neill
}}
| music          = Basil Poledouris
| cinematography = Jan de Bont
| editing        = {{Plainlist|
*Dennis Virkler John Wright
}}
| studio           = {{Plainlist|
*Mace Neufeld Productions
*Nina Saxon Film Design
}}
| distributor    = Paramount Pictures
| released       =  
| runtime        = 134 minutes
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $200.5 million
}}
 action thriller thriller film same name. Jack Ryan. CIA analyst who correctly deduces that circumstance, and must prove his theory to the United States Navy|U.S. Navy to avoid a violent confrontation between the two nations. The ensemble cast features Scott Glenn, James Earl Jones and Sam Neill.
 Best Sound Editing in 1991. On June 12, 1990, the original motion picture soundtrack was released by the MCA Records music label. The soundtrack was composed and orchestrated by musician Basil Poledouris.
 sequels throughout Jack Ryan. However, for the role of Ryan, a number of different actors were cast for the subsequent reincarnations of the film saga, including Harrison Ford, Ben Affleck, and Chris Pine.

==Plot== Soviet ballistic submarine Captain Red October, caterpillar drive, political officer Ivan Putin (Firth), and commands the crew to head toward Americas east coast to conduct missile drills. 
 CIA analyst Jack Ryan (Baldwin) briefs government officials on the departure of Red October and the threat it poses. Officials in the briefing learn that the bulk of the Soviet Navy has been deployed to sink Red October, fearing Ramius may plan an unauthorized strike against the United States. Ryan however, hypothesizes that Ramius instead plans to defection|defect, and leaves to meet up with the American submarine USS Dallas to prove his theory. Meanwhile, Tupolev, though unable to track Red October, guesses his former mentors route and sets a new course across the Atlantic.

 
Red October s caterpillar drive fails during risky maneuvers through an undersea canyon. Petty Officer Jones (Vance), a sonar technician aboard Dallas who has discovered a way to detect Red October through underwater acoustics, plots an intercept course. Ryan arranges a hazardous mid-ocean rendezvous to get aboard Dallas, where he attempts to persuade its captain, Commander Bart Mancuso (Glenn), to contact Ramius and determine his intentions.
 rescue sub, at which point Ramius formally requests asylum in the United States for himself and his officers.

The Red October is suddenly attacked by Konovalov, which has followed them across the Atlantic. As the two Soviet subs maneuver, one of Red Octobers cooks, Loginov (Arana), an undercover GRU agent who has hidden himself on board, opens fire on the crew. He fatally wounds Ramiuss first officer, Vasily Borodin (Neill) before retreating into the missile launch area. Loginov later shoots Ramius, wounding him, but Ryan shoots Loginov before he can detonate a missile and destroy the sub. Meanwhile, Red October makes evasive maneuvers with help from Dallas, causing Konovalov to be destroyed by one of its own torpedoes. Ryan and Ramius, their subterfuge complete, sail Red October to the Penobscot River in Maine.

==Cast==
 , who portrayed commanding officer Tupolev of the Russian submarine V.K. Konovalov.]]
 
* Sean Connery as Captain 1st Rank Marko Aleksandrovich Ramius, Commanding Officer of Red October Jack Ryan, CIA intelligence analyst, author, Professor of Naval History at the United States Naval Academy
* Scott Glenn as Commander Bart Mancuso, Commanding Officer of the USS Dallas
* Sam Neill as Captain 2nd Rank Vasily Borodin, Executive Officer of Red October
* James Earl Jones as Vice Admiral James Greer, CIA Deputy Director of Intelligence (DDI)
* Joss Ackland as Ambassador Andrei Lysenko, Soviet Ambassador to the United States National Security Advisor
* Peter Firth as Senior Lieutenant Ivan Putin, Political Officer of Red October
* Tim Curry as Senior Lieutenant/Dr. Yevgeniy Petrov, Chief Medical Officer of Red October
* Ronald Guttman as Senior Lieutenant Melekhin, Chief Engineer of Red October
* Michael Welden as Captain-Lieutenant Gregoriy Kamarov, Navigator of Red October
* Boris Lee Krutonog as Senior Lieutenant Victor Slavin, Chief Helmsman of Red October
* Courtney Vance as Sonar Technician 2nd Class Ronald "Jonesy" Jones, Sonar Technician of the USS Dallas
* Stellan Skarsgård as Captain 2nd Rank Viktor Tupolev, Commanding Officer of the V.K. Konovalov
* Jeffrey Jones as Dr. Skip Tyler, a U.S. Naval Academy Instructor and Naval Sea Systems Command consultant DSRV DSRV-1 Mystic|Mystic Anthony Peck as Lieutenant Commander "Tommy" Thompson, Executive Officer of the USS Dallas Larry Ferguson as Master Chief Petty Officer Watson, Chief of the Boat (COB) of the USS Dallas
* Fred Dalton Thompson as Rear Admiral (Lower Half) Joshua Painter, Commander of the Enterprise Carrier Battle Group USS Enterprise
* Sven-Ole Thorsen as Chief of the Boat of Red October
* Gates McFadden as Dr. Caroline Ryan, Jack Ryans wife
* Tomas Arana as Cooks Assistant Igor Loginov, Red Octobers cook, also a GRU agent
* Ned Vaughn as Seaman Beaumont, Sonar Technician (Jones apprentice), USS Dallas
* Peter Zinner as Admiral Yuri Ilyich Padorin, Chief Political Officer of the Soviet Navy USS Reuben James (uncredited)
 

==Production==
===Development===
Producer Mace Neufeld optioned Tom Clancys novel after reading galley proofs in February 1985. Despite the book becoming a best seller, no Hollywood studio was interested because of its content. Neufeld said, "I read some of the reports from the other studios, and the story was too complicated to understand."    After a year and a half he finally got a high-level executive at Paramount Pictures to read Clancys novel and agree to develop it into a film.

Screenwriters Larry Ferguson and Donald Stewart worked on the screenplay while Neufeld approached the United States Navy|U.S. Navy for approval. They feared top secret information or technology might be revealed. However, several admirals liked Clancys book and reasoned that the film could do for submariners what Top Gun did for the Navys jet fighter pilots.  Captain Michael Sherman, director of the Navys western regional information office in Los Angeles, suggested changes to the script that would present the Navy in a positive light.   
 USS Salt Lake City. Glenn, who played the commander of USS Dallas (SSN-700)|Dallas, trained by assuming the identity of a submarine captain on board the USS Houston (SSN-713)|Houston (which portrayed Dallas in most scenes).  The subs crew all took orders from Glenn, who was being prompted by the actual commanding officer. 

===Casting=== first actor Jack Ryan film series. ]] USS La Jolla, including Lt Mark Draxton, took leave to participate in filming. According to an article in Sea Classics, at least two sailors from the Atlantic Fleet-based Dallas took leave and participated in the Pacific Fleet-supported filming. The crew of Houston called their month-long filming schedule the "Hunt for Red Ops." Houston made over 40 emergency surfacing "blows" for rehearsal and for the cameras. 

Baldwin was approached in December 1988, but was not told for what role. Klaus Maria Brandauer was cast as Soviet sub commander Marko Ramius but two weeks into filming he quit due to a prior commitment.  The producers faxed the script to Sean Connery who, at first, declined because it didnt make sense. He was indeed missing the first page. He arrived in L.A. on a Friday and was supposed to start filming on Monday but he requested a day to rehearse.  Principal photography began on April 3, 1989 with a $30 million budget.    The Navy lent the film crew the Houston, the USS Enterprise (CVN-65)|Enterprise, two frigates (USS Wadsworth (FFG-9)|Wadsworth and USS Reuben James (FFG-57)|Reuben James), helicopters, and a dry-dock crew. 

Filmmaker John Milius revised some of the films script, writing a few speeches for Sean Connery and all of his Russian dialogue.    He was asked to rewrite the whole film but was only required to do the Russian sequences.  Rather than choosing between the realism of Russian dialog with subtitles, or the audience-friendly use of English (with or without Russian accents), the filmmakers compromised with a deliberate conceit. The film begins with the actors speaking Russian with English subtitles. But in an early scene, actor Peter Firth casually switches in mid-sentence to speaking in English on the word "Armageddon", which is the same spoken word in both languages. After that point, all the Soviets dialogue is communicated in English. Connery continued using the heavy Russian accent he perfected for the motion picture. Only towards the end of the film, once the Russian and American submariners are interacting together, do some of the actors speak in Russian again.

===Filming===
Filming in submarines was impractical. Instead, five soundstages on the Paramount backlot were used. Two 50-foot square platforms housing mock-ups of Red October and Dallas were built, standing on hydraulic gimbals that simulated the subs movements. Connery recalled, "It was very claustrophobic. There were 62 people in a very confined space, 45 feet above the stage floor. It got very hot on the sets, and Im also prone to sea sickness. The set would tilt to 45 degrees. Very disturbing."  The veteran actor shot for four weeks and the rest of the production shot for additional months on location in Port Angeles, Washington and the waters off Los Angeles. 

 , where a number of flight deck scenes were filmed.]]
Made before sophisticated   in the area of the Juan de Fuca Strait and Puget Sound in March 1989. The ship operated out of United States Coast Guard|U.S. Coast Guard Station Port Angeles. The SH-60B detachment from the Battlecats of HSL-43 operated out of NAS Whidbey Island, after being displaced by the filmcrew. Most underwater scenes were filmed using smoke with a model sub connected to 12 cables, giving precise, smooth control for turns. Computer effects, in their infancy, created bubbles and other effects such as particulates in the water.
 Communist Party would give up its monopoly of power, effectively ending the Cold War. Set during this period, there were concerns that with its end, the film would be irrelevant but Neufeld felt that it "never really represented a major problem".  To compensate for the change in Russias political climate, an on-screen crawl appears at the beginning of the film stating that it takes place in 1984 during the Cold War.  Tony Seiniger designed the films poster and drew inspiration from Soviet poster art, utilizing bold red, white and black graphics. According to him, the whole ad campaign was designed to have a "techno-suspense quality to it". The idea was to play up the thriller aspects and downplay the political elements. 
 milligal anomalies". gravity gradiometer by Bell Aerospace was a classified technology at the time. It was thought to be deployed on only a few   after it was first developed in 1973. Bell Aerospace later sold the technology to Bell Geospace for oil exploration purposes.    The last Typhoon-class submarine#Units|Typhoon-class submarine was officially laid down in 1986, under the name TK-210, but according to sources was never finished and scrapped in 1990. 

===Music===
 
{{Infobox album
| Name = The Hunt for Red October (Music from the Motion Picture)
| Type = film score
| Artist = Basil Poledouris
| Released = June 12, 1990
| Length = 29:48
| Label = MCA Records Jack Ryan soundtrack
| Last album = 
| This album = The Hunt for Red October (1990) Patriot Games (1992)
}}

The musical score of The Hunt for Red October was composed by Basil Poledouris. A soundtrack album composed of ten melodies was released on June 12, 1990.  The album is missing some of the musical moments present in the film, including the scene where the crew of Red October sings the Soviet national hymn. The soundtrack is limited due to the fact that it was originally compiled to fit the Compact Cassette. Later, it was remastered for the Compact Disc|CD. An expanded version was released in late 2013 by Intrada Records. It features 40 additional minutes of the score, including the so far-unreleased end titles. 

==Reception==
===Box office===
The Hunt for Red October opened in 1,225 theaters on March 2, 1990, grossing $17&nbsp;million on its opening weekend, more than half its budget.    The film went on to gross $122,012,643 in North America with a worldwide total of $200,512,643. 

===Critical response=== weighted average out of 100 to critics reviews, The Hunt for Red October received a score of 58 based on 17 reviews. 

Roger Ebert called it "a skillful, efficient film that involves us in the clever and deceptive game being played", {{cite news
 | last = Ebert
 | first = Roger
 | title = The Hunt for Red October
 | work = Chicago Sun-Times
 | publisher = 
 | date = March 2, 1990
 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19900302/REVIEWS/3020301/1023
 | accessdate = 2007-10-25 }}  while Gene Siskel commented on the films technical achievement and Baldwins convincing portrayal of Jack Ryan. Nick Schager, for Slant magazines review, noted, "The Hunt for Red October is a thrilling edge-of-your-seat trifle that has admirably withstood the test of time." {{cite news
 | last = Schager
 | first = Nick
 | title = The Hunt for Red October
 | work = Slant
 | publisher = 
 | year = 2003
 | url = http://www.slantmagazine.com/film/film_review.asp?ID=696
 | accessdate = 2007-10-25 }} 
In contrast however,
Newsweek s David Ansen wrote, "But its at the gut level that Red October disappoints. This smoother, impressively mounted machine is curiously ungripping. Like an overfilled kettle, it takes far too long to come to a boil." {{cite news
 | last = Ansen
 | first = David
 | title = The Hunt for Red October
 | work = Newsweek
 | publisher = 
 | date = March 2, 1990
 | url = 
 | accessdate = }} 
Vincent Canby, writing for The New York Times, opined that "the characters, like the lethal hardware, are simply functions of the plot, which in this case seems to be a lot more complex than it really is." 

===Accolades===
The Hunt for Red October was nominated and won several awards in 1991. In addition, the film was also nominated for AFIs 100 Years...100 Thrills. 

{|class="wikitable" border="1"
|-
! Award
! Category
! Nominee
! Result
|- 1991 63rd Academy Awards    Academy Award Best Sound Editing Cecelia Hall, George Watters II
| 
|- Academy Award Best Sound Mixing Richard Bryce Goodman, Richard Overton, Kevin F. Cleary, Don J. Bassman
| 
|- Academy Award Best Film Editing Dennis Virkler, John Wright
| 
|- 1991 44th British Academy Film Awards Best Actor Sean Connery
| 
|- Best Production Design Terence Marsh
| 
|- Best Sound Cecelia Hall, George Watters II, Richard Bryce Goodman, Don J. Bassman
| 
|- 1991 Broadcast BMI Film Music Awards BMI Film Music Award Basil Poledouris
| 
|- 1991 Motion Motion Picture Sound Editors Awards Best Sound Editing&nbsp;— ADR
|align="center" |————
| 
|-
|}

==See also==
 
* 1990 in film Video games based on the motion picture

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 