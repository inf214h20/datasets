Spy Game
 
 
 
{{Infobox film
| name           = Spy Game
| image          = Spy Game poster.jpg
| caption        = Theatrical release poster
| director       = Tony Scott
| producer       = Douglas Wick Marc Abraham
| screenplay     = Michael Frost Beckner David Arata
| story          = Michael Frost Beckner
| starring       = Robert Redford Brad Pitt
| music          = Harry Gregson-Williams Dan Mindel
| editing        = Christian Wagner
| studio         = Beacon Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = $115 million   
| gross          = $143 million
}}

Spy Game is a 2001 American spy film directed by Tony Scott and starring Robert Redford and Brad Pitt. The film grossed United States dollar|$62 million in the United States and $143 million worldwide and received mostly positive reviews from film critics.

==Plot== CIA learns Su Chou claims him. If the CIA claims Bishop as an agent, they risk jeopardizing the trade agreement. Exacerbating Bishops situation is the fact that he was operating without permission from the Agency.
 the CIAs headquarters, by fellow CIA veteran Harry Duncan (David Hemmings), for whom Bishop had been working an operation in Hong Kong before going rogue. Muir first attempts to save Bishop by leaking the story to CNN through a contact in Hong Kong, believing that public pressure would force the CIA to rescue Bishop. The tactic only stalls them, however, and is stymied when a phone call to the U.S. Federal Communications Commission from CIA Deputy Director Charles Harker (Stephen Dillane) results in CNN retracting the story.
 East German Chinese embassy in Britain, causing her to flee the country. Fearing that Bishops feelings for Hadley might compromise his cover and the mission, Muir tips off the Chinese to Hadleys location in return for freeing an arrested U.S. diplomat. Chinese agents kidnap Hadley, and Bishop cuts all ties to Muir when he discovers his involvement. After learning Hadley was the target of Bishops rescue attempt, Muir finally realizes that he has greatly underestimated Bishops feelings for her.

Running out of time, Muir secretly creates a forged urgent operational directive from the CIA director to commence Operation Dinner Out, a rescue mission to be spearheaded by Commander Wileys (Dale Dye) United States Navy SEALs|U.S. Navy SEAL team, for which Bishop had laid the groundwork as a "Plan B" for his own rescue attempt. Using United States Dollar|US$282,000 of his life savings and a misappropriated file on Chinese coastline satellite imagery, Muir enlists Duncans help in bribing a Chinese energy official to cut power to the prison for 30 minutes, during which time the SEAL rescue team retrieves Bishop and Hadley.

Bishop, who is rescued at the end of the film 15 minutes before his scheduled execution, realizes Muir was behind his rescue when he recognizes the name of the plan to rescue him, Operation Dinner Out: a reference to a birthday gift that Bishop gave Muir while they were in Lebanon. When the CIA officials are belatedly informed of the rescue, Muir has already left the building and is seen driving off into the countryside.

==Cast==
 
* Robert Redford as Nathan D. Muir
* Brad Pitt as Tom Bishop
* Catherine McCormack as Elizabeth Hadley
* Stephen Dillane as Charles Harker
* Larry Bryggman as Troy Folger
* Marianne Jean-Baptiste as Gladys Jennip
* Ken Leung as Li
* David Hemmings as Harry Duncan
* Michael Paul Chan as Vincent Vy Ngo
* Garrick Hagon as Cy Wilson, the CIA Director
* Andrew Grainger as Andrew Unger
* Shane Rimmer as the Estate Agent
* Ho Yi as the Prison Warden
* Benedict Wong as Tran
* Adrian Pang as Jiang
* Omid Djalili as Doumet
* Dale Dye as Commander Wiley, USN SEAL
* Charlotte Rampling as Anna Cathcart James Aubrey as Mitch Alford
* Colin Stinton as Henry Pollard
* Andrea Osvárt as a cousin of Muir in Berlin
 

==Production==
 
Filming locations included:
* HSBC Hong Kong headquarters building, presented as the American embassy. (There is actually a U.S. consulate general in Hong Kong, but no embassy.) The embassys interior was filmed at the Lloyds building in London.
* Budapest, Hungary served as Cold War Berlin in the film. The film was shot there to save money, and also because Berlin has changed a lot since the fall of the Berlin Wall. The helicopter landing in the films climax was filmed at an airfield near Budapest.
* Casablanca, Morocco became 1980s Beirut in the film. The crew started off in Haifa, Israel, but had to shift to Morocco because of the Al-Aqsa Intifada which broke out in late 2000. The Vietnam War segment was also filmed in Morocco.
*  , which closed in 1996, was used as the Chinese prison set in Su Chou (Suzhou). (It has since been converted into a luxury hotel.) Shots of the ambulance approaching the prison were also filmed in Queens Lane in Oxford.
* The GlaxoSmithKline research centre in Stevenage, England was used for exterior and some interior scenes at the Central Intelligence Agency|C.I.A. headquarters. (Aerial shots of the real headquarters were also included.)
* The C.I.A. lobby location was in the Senate House of the University of London. Close-ups of Robert Redford as Muir driving from his home to the C.I.A. headquarters were filmed in Regents Park, standing in for Washington, D.C..
* A second unit filmed footage in Washington, D.C. and Virginia for the scenes of Muir driving to and from the C.I.A. headquarters.
* The boat used in the filming was an original Russian-made hydrofoil and was provided by Stephen Canning of Iron Wolf Imports at the direction of Jonathan Frost. The boat was purchased in Lithuania and shipped to Morocco by truck, and then was unfortunately destroyed along with other props in Morocco.

==Reception==
Spy Game opened at number three at the box office in its first weekend in the United States.  The film grossed $62,362,785 in the United States and $143,049,560 worldwide. 

The film received generally positive reviews from critics. Aggregate site Rotten Tomatoes gave the movie a score of 65% based on 130 reviews.  Metacritic gave the film a metascore of 63 out of 100 based upon reviews by 29 critics.  Roger Ebert of the Chicago Sun-Times gave the film two and a half stars out of four and said, "It is not a bad movie, mind you; its clever and shows great control of craft, but it doesnt care, and so its hard for us to care about." 

==Home media==
  State of Play on March 22, 2011 and A Blu-ray/DVD/Digital Copy combo pack on June 28, 2011. Since the VHS and HD DVD formats have been discontinued, the DVD and Blu-ray has remained available.

==References==
 

==External links==
 
*  
*  
*   at Script-o-Rama

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 