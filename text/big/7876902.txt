Abhilasha
{{Infobox film
| name           = Abhilasha
| image          = Abhilasha.jpg
| caption        =
| director       = A. Kodandarami Reddy
| producer       = K.S. Rama Rao
| writer         = A. Kodandarami Reddy
| starring       = Chiranjeevi Raadhika Sarathkumar Rao Gopal Rao
| music          = Ilayaraja
| cinematography =
| editing        = Kotagiri Venkateswara Rao
| studio        =Creative Commercials
| distributor    = Creative Commercials
| released       = March 11, 1983
| runtime        = Telugu
| budget         =
| preceded by    =
}} Telugu film, directed by A. Kodandarami Reddy and was produced by K.S. Rama Rao. It is the film adaptation of Yandamuri Veerendranaths novel of the same name. Chiranjeevi, Raadhika Sarathkumar, Rao Gopal Rao, and Gollapudi Maruthi Rao played vital roles. Music was scored by Ilayaraja.

== Plot ==
Struggling lawyer Chiranjeevi (Chiranjeevi) lives with his roommate (Allu Aravind) and strives for removing IPC Section 302. The reason behind his determination is his fathers death sentence under 302 for rape and murder, which he didnt commit. 

Sarvothama Rao (Rao Gopal Rao), a famous criminal lawyer in that city, sends an invitation to every famous lawyer for a party at his place. By his secretarys mistake, one invitation reaches Chiranjeevi and he gets a chance to meet Sarvothama Rao and express his idea about IPC 302. He also meets Sarvothama Raos niece Archana (Raadhika Sarathkumar) at the party and falls in love with her. When their love starts blossoming, Archana has to attend a meeting in another country and she leaves for few days.

During this time Chiranjeevi comes up with an idea: to plan a fake murder, in which he will implicate himself and receive a death sentence. According to his plan, Sarvothama Rao will show original pictures of the dead body to the governor at the last moment and rescue Chiranjeevi. Then, both intend to file a petition with the Supreme Court against IPC 302. They manage to frame him with the body of a woman, and Chiranjeevi is sentenced to death, just as he planned. In jail, guard Vishnu Sharma (Rallapalli) is surprised to see Chiranjeevi happy before his death. Archana returns from abroad and finds out that Chiranjeevi will be hanged the next day. She goes to meet Chiranjeevi in prison, and he acts like he actually did commit the crime out of lust. Archana does not know Chiranjeevis plan and leaves in disgust. On the night before Chiranjeevis hanging, Sarvothama Rao, who is on his way to meet governor to rescue Chiranjeevi, has an accident.

A tense Chiranjeevi reveals to Vishnu Sharma that this was all fake and that he did not kill the woman, pleading for his help. Vishnu Sharma runs to Archanas house to tell her the truth. After a night-long struggle, they manage to get in touch with Sarvothama Rao, who is alive in hospital. They get the original negatives and Chiranjeevi is saved from being hanged. 

Sarvothama Rao throws a party to celebrate Chiranjeevis success and announces Chiranjeevi and Archanas engagement. A drunk (Gollapudi) drags Chiranjeevi out of the party and Archana gets suspicious about their secret talk. She follows him. Meanwhile Chiranjeevi believes that the dead body of the woman he used did not die a natural death. A postmortem report shows that she was killed by a man with four fingers on one hand. Archana remembers a goon who attacked her on the night she saved Chiranjeevi. Chiranjeevi reveals that he bought the body from that four-fingered goon. It slowly unfolds that the dead woman was Sarvothama Raos daughter from his extramarital relationship. Sarvothama Rao is arrested and in court the judge sentences him to death. But Chiranjeevi objects. He finds that the death penalty is ridiculous and that it is not a punishment to kill someone. To punish, one must suffer for his/her actions. After a short speech, the judge agrees and instead gives him seven years to prison.

Two American films, The Man Who Dared and Life of David Gale, share a similar plot.

==Cast==

*Chiranjeevi
*Radhika
*Rao Gopal Rao
*Gollapudi Maruthi Rao

==External links==
* 
*   At Basthi.com

 
 
 
 
 
 