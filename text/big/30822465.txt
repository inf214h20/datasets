Children of God (film)
{{Infobox film
| name           = Children of God
| image          = Children-of-god-2010.jpg
| director       = Kareem Mortimer
| producer       = Trevite Willis   Richard Lemay
| writer         = Kareem Mortimer
| starring       = Johnny Ferro   Margaret Laurena Kemp   Stephen Tyrone Williams   Van Brown   Mark Richard Ford   Craig Pinder
| music          = Nathan Matthew David
| cinematography = Ian Bloom
| editing        = Maria Cataldo
| released       =  
| runtime        = 104 minutes
| country        = Bahamas
| language       = English
}}
Children of God is a 2010 Bahamian romantic drama film by director and screenwriter Kareem Mortimer. It tells the story of two young Bahamian men who fall in love with each other and portrays the homophobia of the Bahamian society. The film also deals with themes of bisexuality, romantic drama film.

It is one of the first feature narratives from the Caribbean to address homosexuality.  It was the opening night film of the Bahamas International Film Festival. The Bahamas had banned the film Brokeback Mountain in 2006. The film made its International Premiere at the  Miami International Film Festival and have premiered at over 100 film festivals around the world winning 17 awards. Children of God was named one of the top ten films of 2010 on BET.COM.

==Plot==
Jonny (Johnny Ferro), an awkward painting student, is “banished” by his instructor to the remote island of Eleuthera to focus on his work and find his voice. But first he finds Romeo (Stephen Tyrone Williams), a handsome, self-confident guy who shows Jonny the scenic spots, and a bit more. Romeo’s got a girlfriend, however, as well as a blustery mother who willfully ignores any clue or hint he drops to set her straight.

Meanwhile, Lena (Margaret Laurena Kemp), a pastor’s wife, has also made her way to this distant spot to contemplate her future in relative calm. Tired of her husband spouting high-and-mighty, anti-gay rhetoric at rallies while refusing to own up to the cruelty and contradictions in his private life, Lena has a decision to make.
 cruise ship. Actual documentary footage details the mass hysteria that divides the Caribbean as some fundamentalists lead widespread rallies. Jonny, a young obsessive-compulsive white Bahamian artist, faces losing his scholarship at a local university if he does not live up to the potential his professors believe he has. Jonny escapes from his gritty inner-city life in Nassau to the under-populated and dramatic Bahamian island of Eleuthera. Here he decides to paint his masterpiece and then gets killed by a closeted homosexual when Johnny was en route to meet Romeo. It could have had a happy ending. However, for bigots like in this film, life does not have happy endings for many Gay and lesbians. Lena Mackey is an extremely conservative forty-year-old anti-gay activist who finds out that her husband is not who he represents himself to be. She believes that the only way to fix problems in her life is to limit the rights of homosexuals. She heads to Eleuthera for the purpose of galvanizing the community to oppose gay rights. When Jonny arrives, his and Lenas paths cross and together these two embark on a series of physical and emotional adventures that not only inspire Jonny to paint but give him a new zest for life.

==Cast==
* Johnny Ferro as Jonny Roberts
* Margaret Laurena Kemp as Lena Mackey
* Stephen Tyrone Williams as Romeo Fernander
* Van Brown as Reverend Clyde Ritchie
* Mark Richard Ford as Ralph Mackey
* Craig Pinder as Mike Roberts
* Sylvia Adams as Grammy Rose
* Aijalon Coley as Omar Mackey
* Jay Lance Gotlieb as Angry Driver
* Jason Elwood Hanna as Purple
* Christopher Herrod as Dr. Mark Wells
* Juanita Kelly as Lonnette Adderley
* Adela Osterloh as Romeos Mother
* Leslie Vanderpool as Rhoda Mackey
* Christine Wilson as Anna Ross
* Conrad Knowles, Romeos true friend

==Release== Showtime television network on June 2 and the DVD was released on June 7 through TLA Releasing. 
Johnny Ferro won Best Actor at the Out on Film festival in Atlanta against runner-up James Franco (for Howl (film)|Howl).

== See also ==
*LGBT rights in the Bahamas

==External links==
*  

 
 
 
 
 
 