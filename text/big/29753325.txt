Doing Hard Time
 
Doing Hard Time is a 2004 drama film starring Boris Kodjoe, Michael K. Williams, Steven Bauer, David Banks, and Giancarlo Esposito. Michael (Boris Kodjoe) was a good man and a loving father, until one day, his seven-year-old son was caught in the crossfire of a drug deal gone bad. Michaels mourning becomes outrage when his childs killers get only a slap on the wrist for drug possession. He launches a crusade of vengeance, getting arrested himself so that he can go behind bars and deal out his own brand of justice to the two shooters. But in a place where there are no rules, revenge will no longer be Michaels only concern.

==Plot==
When Michael Mitchells (Boris Kodjoe) seven-year-old son, Chase, is accidentally shot during an aborted drug deal, no one knows for sure which of the two gunmen, Curtis "Durty Curt From Detroit" Craig (Michael K. Williams) or Raymond "Razor" Carver (Michael Kimbrew), pulled the trigger. Because both men refuse to testify against the other, a jury finds them guilty only of drug possession, meaning they could be back on the street in two years. 

Having already lost Chases mother to cancer, Michael cant live with the idea of his only childs murder going unpunished. As part of a methodical plan to exact his revenge, he commits a brutal crime to get himself sentenced to the same institution as Durty Curt and Razor. Meanwhile, Durty Curt is determined to settle the score with Razor, who has fallen in with prison kingpin Eddie Mathematic (Sticky Fingaz). 

As a showdown approaches, a cast of players, from prison guard Capt. Pierce (Giancarlo Esposito) to gay bookie Clever (William L. Johnson) to fast-talking druggy (David Banks) to Michaels illiterate cellmate, Smalls (Marcello Thedford), take sides and place bets on the outcome.

==Cast==
* Boris Kodjoe as Michael Mitchell
* Michael K. Williams as Curtis "Durty Curt From Detroit" Craig
* Sticky Fingaz as Eddie Mathematic
* David Banks as Chris
* Reagan Gomez-Preston as Rayvon Jones
* William L. Johnson as Clever
* Marcello Thedford as Toure Smalls
* Emilio Rivera as Officer David Teal
* Chenoa Maxwell as Elise
* Charles Malik Whitfield as Armand
* Patrice Fisher as Lt. Elaine Lodge
* Steven Bauer as Det. Anthony Wade
* Giancarlo Esposito as Captain Pierce
* Michael Kimbrew as Raymond "Razor" Carver

== External links ==
*  

 
 
 