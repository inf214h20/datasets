Wallace & Gromit: The Curse of the Were-Rabbit
 
 
{{Infobox film
| name        = Wallace & Gromit:  The Curse of the Were-Rabbit
| image       = wallace_gromit_were_rabbit_poster.jpg
| border      = yes
| alt         = 
| caption     = British theatrical release poster
| director    = Nick Park Steve Box
| producer    = Nick Park Claire Jennings Peter Lord Carla Shelley David Sproxton Bob Baker Mark Burton
| based on    =    Nicholas Smith Liz Smith
| music       = Julian Nott  
| editing     = David McCormick Gregory Perler
| studio      = DreamWorks Animation Aardman Animations DreamWorks Pictures  (United States)  United International Pictures  (United Kingdom)  
| released    =  
| runtime     = 85 minutes
| country     = United Kingdom
| language    = English
| budget      = $30 million
| gross       = $192,610,372  
}}  comedy film. DreamWorks Pictures. It was directed by Nick Park and Steve Box as the second feature-length film by Aardman after Chicken Run.

The Curse of the Were-Rabbit is based on the Wallace and Gromit short film series, created by Park. The film follows eccentric inventor Wallace (voiced by Peter Sallis) and his mute and intelligent dog, Gromit, as they come to the rescue of the residents of a village which is being plagued by a mutant rabbit before an annual vegetable competition.
 Academy Award for Best Animated Feature, making it the second film from DreamWorks Animation to win (after Shrek), as well as both the second non-American animated film and second non-computer animated film to have received this achievement (after Spirited Away). It is also the first stop-motion film to win the award.

==Plot==
  Giant Vegetable Competition is approaching. The winner of the competition will win the coveted Golden Carrot Award. All are eager to protect their vegetables from damage and thievery by rabbits until the contest, and Wallace and Gromit are cashing in by running a vegetable security and humane pest control business, "Anti-Pesto".
 brainwash the rabbits. While performing the operation, Wallace accidentally kicks a lever and a rabbit gets fused to Wallaces head, causing the machine to malfunction, and Gromit is forced to destroy the Mind-O-Matic to save Wallace. The resulting failure somehow leaves them with a semi-intelligent rabbit who no longer has an appetite for vegetables, whom Wallace dubs "Hutch". That night, all the vegetable gardens in the town are raided by the "Were-Rabbit", a giant rabbit-like monster which eats vegetables of any size. During a chaotic town meeting in the church, Anti-Pesto enters into a rivalry with Lord Victor Quartermaine, who seeks to court Lady Tottington and who believes that it is better to be rid of the rabbits via the use of guns. With Lady Tottingtons persuasion, the townspeople agree to let Anti-Pesto front the capture attempt.
 gold bullets – supposedly, the only things capable of killing a Were-Rabbit.
 marrow he had been growing for the competition as bait for Wallace who has burst in upon the vegetable contest. Victor exhausts his supply of gold bullets and takes the Golden Carrot award from Lady Tottington, as it is the only golden bullet-like object left to him. Ascending to the rooftops, Wallace takes Lady Tottington with him and indirectly reveals his identity to her, but Victor interrupts them and, after accidentally revealing his knowledge of the Were-Rabbits identity, confesses that he only wanted to court Lady Tottington for her money.
 bouncy castle. On the roof of Tottington Hall, Victor wields the Golden Carrot trophy inside an blunderbuss and tries one last time to shoot Wallace, but Gromit saves him by grabbing onto a rope from a flagpole and swinging his plane into the path of the improvised bullet. However, the toy plane rapidly descends when Gromit accidentally lets go of the rope. Wallace sacrifices himself to save Gromit, breaking Gromits fall into the cheese tent below. Victor gloats victoriously, but Lady Tottington knocks him into the tent, where Wallace is dying of his injuries. Using the marionette to protect Wallace from the angry mob outside, Gromit dresses Victor up as the monster and the angry mob chases Victor away.

Gromit and Tottington tend to Wallace who seemingly dies, but morphs back into his normal, naked form. Gromit, however, is able to revive Wallace with the scent of a slice of Stinking Bishop cheese. For his and his Squash (plant)|marrows bravery, Gromit is awarded the slightly dented Golden Carrot trophy, and Lady Tottington turns Tottington Halls front garden into a wildlife sanctuary, where Hutch and the rest of the rabbits can live in peace.

==Cast==
 
  at the films North American premiere at the 2005 Toronto International Film Festival ]] 
* Peter Sallis as Wallace, an eccentric and absentminded inventor with an obsession with cheese.
**Gromit is Wallaces silent and intelligent dog, who saves his master whenever something goes horribly wrong bounder who high calibre bolt-action model. It soon becomes clear in the film that Victors only interest in Lady Tottington is her vast fortune which he is eager to get his hands on. After Lady Tottington discovers that Victor knew that the were-rabbit was Wallace all along, he reveals that all he wants is her money. His surname is similar to Allan Quatermain, the British novelists H. Rider Haggards big-game hunter character. 
** Philip is Victors vicious but cowardly hunting dog who resembles a Miniature Bull Terrier. He and his master will do anything to stop the Were-Rabbit, although Philip is bright enough to know that the Were-Rabbit is beyond his hunting skills, and that Gromit, closer to his own size, is a better prospect as the target of premeditated violence. He also owns a ladys purse decorated with flowers for spare change.
* Helena Bonham Carter as Lady Campanula "Totty" Tottington, a wealthy aristocratic spinster with a keen interest in both vegetable-growing and fluffy animals. For 517 years, her family has hosted an annual vegetable competition. Lady Tottington asks Wallace to call her "Totty" (which is a British term for attractive upper class women) and develops a romantic interest in him. Her given name, Campanula is also the name of a bellflower. bobby who judges the Giant Vegetable Contest, although, with the havoc it creates every year he would rather it did not happen at all. Nicholas Smith vicar and the first person in the village to witness the Were-Rabbit. He describes the full horror of his encounter with the beast, but Victor refuses to believe him. However, when Victor discovers the true identity of the beast, he turns to the vicar for advice on how to kill it. Reverend Hedges appears to have a wide range of knowledge on the habits and the slayings of supernatural animals, and has a whole cupboard filled with the weapons to defeat them. Although his name appears in the credits, it is never said inside the film. Liz Smith as Mr. and Mrs. Mulch, clients of Wallace and Gromits Anti-Pesto. Mrs. Mulch is a prominent woman that has a fixation on her gigantic pumpkin. Mr. Mulch speaks little and has a pair of dentures, which he briefly used to knock out a thieving rabbit.  
* Edward Kelsey as Mr. Growbag, an elderly resident of Wallace and Gromits neighbourhood and a founding member of the towns veg growers council. He constantly recalls memories of incidents from previous Vegetable Competitions – comparing them to what may happen to the forthcoming one. Two of the "disasters" he mentions are The Great Slug Riot of 32, "when there were slugs the size of pigs", and the Great Duck Plague of 53.
* Peter Sallis (with a sped-up voice) as Hutch, originally just another captive rabbit, but receives special treatment, and his name, after an attempt to brainwash him and his fellows goes wrong. He was the first to be suspected of being the Were-Rabbit. Everything that Hutch says is a quotation from Wallace (though, surprisingly, some of the lines were originally spoken by Wallace after the incident with the Mind-Manipulation-O-Matic). Hutch wears clothes like Wallaces, including his slippers and tank top.

==Production==
  at the films premiere]] Nicholas Smith Liz Smith (as Mrs. Mulch). Keeping with the tradition of the original short films, Gromit remains silent, communicating only through body language.

Park told an interviewer that after separate test screenings with British and American children, the film was altered to "tone down some of the British accents and make them speak more clearly so the American audiences could understand it all better."  Park was often sent notes from DreamWorks, which irritated him. He recalled one note that Wallaces car should be trendier, which he disagreed with because he felt making things look old-fashioned made it look more ironic. 

The vehicle Wallace drives in the film is an Austin A35 van. In collaboration with Aardman in the spring of 2005, a road-going replica of the model was created by brothers Mark and David Armé, founders of the International Austin A30/A35 Register, for promotional purposes. In a 500-man-hour customisation, an original 1964 van received a full body restoration before being dented and distressed to perfectly replicate the model van used in the film. The official colour of the van is Preston Green, named in honour of Nick Parks home town. The name was chosen by the Art Director and Mark Armé.
 marrow as a "melon". Because the word "marrow" is not well known in the US, Jeffrey Katzenberg insisted it be changed. Park explained "Because its the only appropriate word we could find that would fit with the mouth shape for marrow. Melon apparently works over there. So we have Wallace saying, Hows your prize melon?".  The US version is also heard in the UK bootleg DVD release.

==Release and reaction==
The film was released in the United Kingdom, Hong Kong, and the United States on 14 October 2005 to critical acclaim, including "A" ratings from Roger Ebert and Ty Burr. The DVD edition of the film was released on 7 February 2006 (USA) and 20 February 2006 (UK). On the Rotten Tomatoes website, the film won 2 Golden Tomato awards for "Best Wide Overall Release" and "Best Animation", the film received critical acclaim and has a 95% "Certified Fresh" rating from the website. However, Peter Sallis was dissatisfied with the film, saying that he preferred the half-hour films to the feature,  and Richard Roeper gave a "thumbs down" to the film on At the Movies with Ebert & Roeper. Roeper said that, "Its slightly amusing and Id say when it comes out on video or if you catch it on cable, but to rush out to theaters...".
 

===Box office performance=== The Fog.  It remained number one worldwide for three weeks in a row.  Despite the big difference between the production budget and the overall gross, DreamWorks considered its returns low in comparison to Chicken Run, which made a slightly larger amount (US$224,834,564) worldwide, but nearly twice as much (US$106,834,564) within the United States.  When it is factored in that Chicken Run also cost US$15 million more to make, the overall profits for both films end up looking similar. Nevertheless, it was reported on 3 October 2006  and confirmed on 30 January 2007  that the partnership between DreamWorks and Aardman has ended due to "creative differences" about Aardmans CG feature, Flushed Away. But, given the films US$30 million budget, Aardman have judged it successful enough for a new Wallace & Gromit film to be made. 

===Awards===
{| class="wikitable"
|-
! Group !! Award !! Recipients !! Result
|-
| 78th Academy Awards Best Animated Feature Film
| Nick Park Steve Box
|  
|- 33rd Annie Awards
| Best Animated Effects
| Jason Wen
|  
|- Best Animated Feature
| 
|  
|-
| Best Character Animation
| Claire Billet
|  
|-
| Best Character Design in an Animated Feature Production 
| Nick Park
|  
|-
| Best Directing in an Animated Feature Production 
| Nick Park Steve Box
|  
|-
| Best Music in an Animated Feature Production 
| Julian Nott
|  
|-
| Best Production Design in an Animated Feature Production 
| Phil Lewis
|  
|-
| Best Storyboarding in an Animated Feature Production 
| Bob Persichetti
|  
|-
| Best Voice Acting in an Animated Feature Production 
| Peter Sallis as the voice of Wallace
|  
|-
| Best Writing in an Animated Feature Production 
| Steve Box Nick Park Mark Burton Bob Baker
|  
|-
| Best Character Animation
| Jay Grace Christopher Sadler
|  
|-
| Best Storyboarding in an Animated Feature Production 
| Michael Salter
|  
|-
| Best Voice Acting in an Animated Feature Production 
| Helena Bonham Carter as the voice of Lady Campanula Tottington
|  
|-
| Best Voice Acting in an Animated Feature Production 
| Ralph Fiennes as the voice of Victor Quartermaine
|  
|-
| Best Voice Acting in an Animated Feature Production  Nicholas Smith as the voice of Reverend Clement Hedges
|  
|-
| Bodil Awards
| Best Non-American Film
| 
|  
|-
| 59th British Academy Film Awards Best British Film
| Claire Jennings David Sproxton Nick Park Steve Box Mark Burton Bob Baker
|  
|-
| British Comedy Awards
| Best Comedy Film
| Nick Park
|  
|-
| Broadcast Film Critics Association
| Best Animated Feature
|
|  
|- Cine Awards 
| Best voice actress
| Helena Bonham Carter
|  
|-
| Dallas-Fort Worth Film Critics Association
| Best Animated Feature
|
|  
|- Empire Awards
| Best Director
| Nick Park Steve Box
|  
|-
| Best British Film
| 
|  
|-
| Best Comedy
| 
|  
|-
| Scene of the Year
| 
|  
|-
| Florida Film Critics Circle
| Best Animated Film
| 
|  
|- 50th Hugo Awards Best Dramatic Presentation – Long Form
| 
|  
|-
| Kansas City Film Critics Circle
| Best Animated Film
| 
|  
|-
| Las Vegas Film Critics Society
| Best Animated Film
| 
|  
|-
| London Film Critics Circle
| British Film of the Year
| 
|  
|-
| Los Angeles Film Critics Association
| Best Animated Film
|
|  
|- 53rd Motion Picture Sound Editors Golden Reel Awards
| Best Sound Editing in Feature Film – Animated
| 
|  
|-
| New York Film Critics Online
| Best Animated Film
| 
|  
|-
| Kids Choice Awards
| Best Animated Film
| 
|  
|-
| Online Film Critics Society
| Best Animated Feature
|
|  
|-
| 17th Producers Guild of America Awards
| Producer of the Year Award in Animated Theatrical Motion Pictures 
| Claire Jennings Nick Park
|  
|- 10th Satellite Awards Outstanding Motion Picture, Animated or Mixed Media
| 
|  
|- 32nd Saturn Awards Best Animated Film
| 
|  
|-
| Southeastern Film Critics Association
| Best Animated Film
| 
|  
|-
| Toronto Film Critics Association
| Best Animated Film
| 
|  
|-
| Ursa Major Awards 
| Best Anthropomorphic Motion Picture
|
|  
|-
| Visual Effects Society
| Outstanding Animated Character in an Animated Motion Picture
| Lloyd Price for "Gromit"
|  
|-
| Washington D.C. Area Film Critics Association
| Best Animated Film
| 
|  
|}

Awards shown here are those detailed by the Internet Movie Database. 

==Home media==
In Region 2, the film was released in a two-disc special including Cracking Contraptions, plus a number of other extras. In Region 1, the film was released on DVD in Widescreen and Fullscreen versions and VHS on 7 February 2006. Wal-Mart stores carried a special version with an additional DVD, "Gromits Tail-Waggin DVD" which included the test shorts made for this production.

A companion game, also titled  , had a coinciding release with the film. A novelisation, Wallace and Gromit: The Curse of the Were-Rabbit: The Movie Novelization by Penny Worms (ISBN 0-8431-1667-6), was also produced.

It was the last DreamWorks Animation movie to be released on VHS. It was re-released on DVD on May 13, 2014, as part of a triple film set, along with DreamWorks Animations Chicken Run and Flushed Away. 

==Soundtrack==
{{Track listing
| total_length    = 48:11
| all_music     = Julian Nott, except as noted 
| extra_column    = Artist
| title1   = A Grand Day Out
| length1  = 1:54
| title2   = Anti-Pesto to the Rescue
| length2  = 3:18
| title3   = Bless You, Anti-Pesto
| length3  = 1:56
| title4   = Lady Tottington and Victor
| length4  = 2:03
| title5   = Fire Up the Bun-Vac
| length5  = 1:47
| title6   = Your Ladyship
| length6  = 1:07
| title7   = Brainwash and Go
| length7  = 2:28
| title8   = Harvest Offering
| length8  = 2:30
| title9   = Arson Around
| length9  = 2:23
| title10  = A Big Trap
| length10 = 3:27
| title11  = The Morning After
| length11 = 1:44
| title12  = Transformation
| length12 = 4:05
| title13  = Ravaged in the Night
| length13 = 1:45
| title14  = Fluffy Lover Boy
| length14 = 4:36
| title15  = Kiss My Artichoke
| length15 = 4:31
| title16  = Dogfight
| length16 = 3:39
| title17  = Every Dog Has His Day
| length17 = 2:43
| title18  = All Things Fluffy
| length18 = 1:07
| title19  = Wallace and Gromit
| length19 = 1:08
}}

==References==
 
* The Art of Wallace and Gromit: The Curse of the Were-Rabbit by Andy Lane & Paul Simpson. ISBN 1-84576-215-0

==External links==
 
 
*   (UK)
*   at the Official Wallace & Gromit website
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
{{Navboxes|title=Awards for Wallace & Gromit: The Curse of the Were-Rabbit|state=collapsed|list1=
 
 
 
 
}}

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 