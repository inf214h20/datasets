Chandanachola
{{Infobox film
| name           = Chandanachola
| image          =
| caption        =
| director       = Jeasy
| producer       = Dr. Balakrishnan
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Jose Prakash Manavalan Joseph Pattom Sadan Sankaradi
| music          = KJ Joy
| cinematography = Kanniyappan
| editing        = G Venkittaraman
| studio         = Rekha Cine Arts
| distributor    = Rekha Cine Arts
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by Jeasy and produced by Dr. Balakrishnan. The film stars Jose Prakash, Manavalan Joseph, Pattom Sadan and Sankaradi in lead roles. The film had musical score by KJ Joy.   

==Cast==
 
*Jose Prakash
*Manavalan Joseph
*Pattom Sadan
*Sankaradi
*Sunil
*Nilambur Balan
*KR Kumaran Nair
*Kuthiravattam Pappu
*Paravoor Bharathan Reena
*Sadhana Sadhana
*Sudheer Sudheer
*Sukumaran Nair
*T. P. Madhavan
*Victory Janardanan
*Vidhubala Vincent
 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Dr. Balakrishnan, Muppathu Ramachandran, Konniyoor Bhas and Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bindu Neeyananda || P Susheela || Dr. Balakrishnan ||
|-
| 2 || Bindu Neeyen Jeeva || P Susheela || Dr. Balakrishnan ||
|-
| 3 || Hridayam Marannu || K. J. Yesudas || Muppathu Ramachandran ||
|-
| 4 || Lovely Evening || Vani Jairam || Konniyoor Bhas ||
|-
| 5 || Maniyaanchettikku || K. J. Yesudas, Pattom Sadan || Dr. Balakrishnan ||
|-
| 6 || Mukhasree Kunkumam || K. J. Yesudas || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 