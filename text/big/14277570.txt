Luke and the Bang-Tails
 
{{Infobox film
| name           = Luke and the Bang-Tails
| image          =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film starring Harold Lloyd. A print of the film survives in the film archive of the Museum of Modern Art.   

==Cast==
* Harold Lloyd - Luke
* Bebe Daniels
* Snub Pollard Charles Stevenson - (as Charles E. Stevenson)
* Billy Fay
* Fred C. Newmeyer
* Sammy Brooks
* Harry Todd
* Bud Jamison
* Margaret Joslin - (as Mrs. Harry Todd)
* Dee Lampton
* May Cloy
* Mrs. Halliburton
* Clarke Irvine

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 