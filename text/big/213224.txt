Shadow of the Vampire
 
 
{{Infobox film
| name = Shadow of the Vampire
| image = ShadowoftheVampireposter.jpg
| alt = 
| caption = Promotional poster
| director = E. Elias Merhige
| producer = Nicolas Cage Jeff Levine Steven Katz John Aden Gillet Eddie Izzard Udo Kier Catherine McCormack Ronan Vibert Dan Jones
| cinematography = Lou Bogue
| editing = Chris Wyatt Madman Films Saturn Films Lions Gate Films
| released =  
| runtime = 92 minutes  
| country = Luxembourg United Kingdom United States
| language = English German Luxembourgish
| budget = $8 million 
| gross = $11,155,214
}} Steven Katz, elided action iris lenses. 

==Plot== Frederich Wilhelm German theater performer named Max Schreck, who is a character actor. To involve himself fully in his role, Schreck will only appear amongst the cast and crew in makeup, and will never break character. 

After filming scenes in a studio with leading actress Greta Schroeder, who is displeased about leaving Berlin, Murnaus team travels to the remote inn where they will be staying and shooting further scenes. The landlady becomes distressed at Murnau removing crucifixes around the inn, and the cameraman, Wolfgang Muller, falls into a strange, hypnotic state. Gustav discovers a bottle of blood amongst the teams food supplies, and Murnau delivers a caged ferret to a cellar in the middle of the night. 
 Slovak castle for the first scene with the vampire. Schreck appears for the first time, and his appearance and behavior impress and disturb them. The films producer, Albin Grau, suspects that Schreck is not a German theater actor, and is confused when Murnau tells him that he found Schreck in the castle. Soon after the completion of the scene, Wolf is found collapsed in a dark tunnel. Upon returning to the inn, the landlady appears frightened by his pale, weak appearance, and mutters "Nosferatu" while clutching at a rosary.
 generator powering the lights fails. When the lights return, Schreck has pinned Wolf to the floor, apparently draining his blood. Albin orders filming ended for the night, and the crew rushes from the castle, leaving Schreck behind. Schreck examines the camera equipment, fascinated by footage of a sunrise.  

Schreck is in fact an actual vampire, and Murnau has struck a deal with him in order to create the most realistic vampire film possible. He has been promised Greta as a prize for completing the film, but remains difficult and uncooperative until the entire production is at his mercy. With Wolf near death, Murnau is forced to bring in another cinematographer, Fritz Arno Wagner. During Murnaus absence, Albin and the films scriptwriter, Henrik Galeen, share a drink by a campfire, when Schreck approaches them. They invite him to join them, and question Schreck, believing he is still in character. They ask him when he became a vampire; Schreck replies that he cannot remember. Albin and Galeen reply that Dracula would not reply so, then ask Schreck what he thought of the novel. Schreck points out Draculas loneliness, and the flaw of Dracula remembering how to do everyday activities that he has not performed in centuries. Albin and Henrik suggest creating more vampires, but Schreck replies he is too old, and he seems to remember he could not anyway. When they ask how he became a vampire, Schreck starts to mention a tryst he had. A bat flies by and Schreck catches it, sucking its blood ecstatically. The others are impressed by what they assume is talented acting.

The production moves to Heligoland to film the final scenes, and Murnau, in a laudanum-induced stupor, admits Schrecks true nature to Albin and Fritz. The two realise that they are trapped, leaving them no choice but to complete the film and give Greta to the vampire if they wish to survive. Greta becomes hysterical after noticing Schreck casts no reflection. Murnau, Albin and Fritz drug her, and film the scene as Schreck feeds on Greta, resulting in her death. However, the laudanum in her blood puts Schreck to sleep. At dawn, the remaining three attempt to open a door and let in sunlight to destroy Schreck. However, Schreck previously cut the chain, having learned of their trickery. Schreck kills Fritz and Albin while Murnau continues filming. The rest of the crew arrives in time to lift up the door and flood the set with sunlight, destroying Schreck while Murnau films his death.

Murnau completes the filming and calmly states "I think we have it."

==Cast== Frederich Wilhelm Murnau, the director of Nosferatu
* Willem Dafoe as Max Schreck, who plays Count Orlok
* Udo Kier as Albin Grau, the producer, art director, and costume designer
* Cary Elwes as Fritz Arno Wagner, the cinematographer Greta Schroeder, who plays Ellen Hutter/Mina Harker
* Eddie Izzard as Gustav von Wangenheim, who plays Thomas Hutter/Jonathan Harker John Aden Gillet as Henrik Galeen, the screenwriter
* Nicholas Elliott as Paul
* Ronan Vibert as Wolfgang Muller
* Sophie Langevin as Elke
* Myriam Muller as Maria

The film depicts several of the major characters as being killed by the vampire; however, historically these individuals continued to live long lives after the films production.  Fritz Wagner and Albin Grau, who are shown having their necks snapped by Count Orlok, lived to the 1950s and 70s respectively.  Greta Schroeder, who also did not actually die, continued to have a successful film career until the 1950s.  Of all the characters, it is Max Schreck - the real actor who actually played the vampire - who died in the 1930s thus placing his death the closest to the films production.

==Production==
 
The films working title was Burned to Light, but the director E. Elias Merhige decided to change the name of the film when Willem Dafoe asked, "Whos Ed?"; the actor thought the title was Burn Ed to Light. 

The film was produced by Nicolas Cages Saturn Films. Members of the online community "The HollyWood Stock Exchange" were able to donate a small sum towards the films production, in exchange for listing their name on the DVD release of the film as "Virtual producers".
 The Hunger (1983).

==Reception==
 
Critical reaction to Shadow of the Vampire has been mostly positive, with the film earning a rating of 82% on   gave the film 3½ stars out of 4, writing that "director E. Elias Merhige and his writer, Steven Katz, do two things at the same time. They make a vampire movie of their own, and they tell a backstage story about the measures that a director will take to realize his vision", and that Dafoe "embodies the Schreck of Nosferatu so uncannily that when real scenes from the silent classic are slipped into the frame, we dont notice a difference."  Ebert later awarded the film his Special Jury Prize on his list of "The Best 10 Movies of 2000", writing of Dafoes "astonishing performance" and of the film, "Avoiding the pitfall of irony; it plays the material straight, which is truly scary." 

==Awards==
Shadow of the Vampire won several awards:
* the Prix Tournage
* the Saturn Award
* the Gran Angular Award
* the International Fantasy Film Award
* the President Award
* the Golden Satellite Award
* the Independent Spirit Award
* the LAFCA Award for Willem Dafoe Steven Katz

Willem Dafoe was nominated for the Academy Award for Best Supporting Actor.

==See also==
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 