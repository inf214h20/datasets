Hitchcock (film)
 
{{Infobox film
| name           = Hitchcock
| image          = Hitchcock film poster.jpg
| caption        = Theatrical film poster
| director       = Sacha Gervasi
| producer       = Ivan Reitman Tom Pollock Joe Medjuck Tom Thayer Alan Barnette
| screenplay     = John J. McLaughlin
| based on       =  
| starring       = Anthony Hopkins Helen Mirren Scarlett Johansson Toni Collette Danny Huston Jessica Biel James DArcy Michael Wincott 
| music          = Danny Elfman
| cinematography = Jeff Cronenweth Pamela Martin
| studio         = The Montecito Picture Company Cold Spring Pictures 
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 98 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $27,586,114 
}}
 biographical drama film directed by Sacha Gervasi and based on Stephen Rebellos non-fiction book Alfred Hitchcock and the Making of Psycho. The film was released in selected cities on November 23, 2012,  with a worldwide release on December 14, 2012.

Hitchcock centers on the relationship between director Alfred Hitchcock (Anthony Hopkins) and his wife Alma Reville (Helen Mirren) during the making of Psycho (1960 film)|Psycho, a controversial horror film that became one of the most acclaimed and influential works in the filmmakers career.

==Plot summary== Casino Royale in favor of a horror novel called Psycho (novel)|Psycho by Robert Bloch, which is based on the crimes of murderer Ed Gein. Gein appears in sequences throughout the film in which he seems to prompt Hitchcocks imagination regarding the Psycho story, or act as some function of Hitchcocks unconscious mind (for instance, drawing Hitchcocks attention to sand on his bathroom floor, the quantity of which reveals how much time his wife Alma has been spending at the beachhouse with Whitfield Cook).
 Paramount prove more difficult to persuade, forcing Hitchcock to finance the film personally and use his Alfred Hitchcock Presents television crew to produce the film.

However, the pressures of the production, such as dealing with Geoffrey Shurlock of the Motion Picture Production Code, and Hitchcocks lecherous habits, such as when they confer with the female lead, Janet Leigh, annoy Alma. She begins a personal writing collaboration with Whitfield Cook on his screenplay at his beach house without Hitchcocks knowledge. Hitchcock eventually discovers what she has been doing and suspects her of having an affair.  This concern affects Hitchcocks work on Psycho.

Alma takes over production of the film when Hitchcock is temporarily bedridden after collapsing from overwork. Hitchcock eventually confronts Alma and asks her if she is having an affair. Alma angrily denies it. Meanwhile, Hitchcock expresses his disappointment to Vera Miles at how she didnt follow through on his plan to make her the next biggest star after Grace Kelly; but Miles says she is happy with her family life.

Hitchcocks cut of Psycho is poorly received by the studio executives, while Alma discovers Whitfield having sex with a younger woman at his beach house. Hitchcock and Alma reconcile and set to work on improving the film. Their renewed collaboration yields results, culminating in Alma convincing Hitchcock to accept their composers suggestion for adding Bernard Herrmanns harsh strings score to the shower scene.

After maneuvering Shurlock into leaving the films content largely intact, Hitchcock learns the studio is only going to exhibit the film in two theaters. Hitchcock arranges for special theater instructions to pique the publics interest such as forbidding admittance after the film begins. At the films premiere, Hitchcock first views the audience from the projection booth, looking out through its small window at the audience (a scene which recalls his spying on his leading actresses undressing earlier in the film, by looking through a hole cut in the dressing room wall - which itself is a voyeuristic motif included in the film of Psycho). Hitchcock then waits in the lobby for the audiences reaction, conducting to their reactions as they scream on cue. The film is rewarded with an enthusiastic reception.
 raven lands The Birds, before turning to meet with his wife.

The final title cards say that Hitchcock directed six more films after Psycho, none of which would eclipse its commercial success, and although he never won an Academy Award|Oscar, the American Film Institute awarded him its Life Achievement Award in 1979 - an award he claimed he shared, as he had his life, with his wife, Alma.

==Cast==
*Anthony Hopkins as Alfred Hitchcock
*Helen Mirren as Alma Reville
*Scarlett Johansson as Janet Leigh
*Danny Huston as Whitfield Cook
*Toni Collette as Peggy Robertson
*Michael Stuhlbarg as Lew Wasserman
*Michael Wincott as Ed Gein
*Jessica Biel as Vera Miles
*James DArcy as Anthony Perkins
*Richard Portnow as Barney Balaban
*Kurtwood Smith as Geoffrey Shurlock
*Ralph Macchio as Joseph Stefano
*Wallace Langham as Saul Bass
*Paul Schackman as Bernard Herrmann
*Currie Graham as PR Flack
*Richard Chassler as Martin Balsam Spencer Leigh as Nunzio
*Josh Yeo as John Gavin

==Production==

===Development===
In 2005, it was reported that A&E (TV channel)|A&E would produce a television film or miniseries based on Stephen Rebellos book Alfred Hitchcock and the Making of Psycho.  Subsequently, the book was optioned as a major motion picture. In 2007, The Montecito Picture Company, owned by Ivan Reitman and Tom Pollock, set up a first look deal with Paramount Pictures, the original distributor of Psycho (1960 film)|Psycho. However, after four years of development at Paramount, production moved to Fox Searchlight Pictures.   
 Black Swan co-writer John J. McLaughlin wrote the first screenplay drafts; subsequently, Stephen Rebello wrote additional uncredited drafts that shifted the storys focus away from Ed Gein and instead toward the complex personal and professional relationship of Hitchcock and his wife, Alma Reville, during the filming of Psycho.

===Casting===
Much of the films casting was announced in March 2012. On March 1, 2012, Scarlett Johansson and James DArcy were announced to play the stars of Psycho, Janet Leigh and Anthony Perkins.  Later that month Jessica Biel was cast as Vera Miles.  Following, additional cast members were announced that included Toni Collette as the directors trusted assistant, Danny Huston as screenwriter-playwright Whitfield Cook, Michael Stuhlbarg as powerful agent and studio boss Lew Wasserman, Michael Wincott as serial killer Ed Gein, Ralph Macchio as screenwriter Joseph Stefano, Richard Portnow as Paramount Pictures boss Barney Balaban, and Wallace Langham as graphic designer Saul Bass.  

===Filming===
Principal photography for the film began on April 13 in Los Angeles, with the film retitled as Hitchcock.  Filming was wrapped up on May 31 after the completion of a scene set during Psycho s New York City premiere on June 16, 1960. 

===Music=== 1998 shot-for-shot remake. 

The soundtrack album to the film was released by Sony Classical on December 14, 2012. 

==Release== Oscar season.    The film had its world premiere as the opening film of AFI Fest 2012 on November 1 with a gala at Graumans Chinese Theatre in Hollywood.  The first trailer for the film was released on October 10, 2012. 

Hitchcock was released onto DVD and Blu-ray on March 12, 2013 by 20th Century Fox Home Entertainment. The home media releases contain several making-of featurettes as well as commentary between director Sacha Gervasi and author Stephen Rebello, a deleted scene, and the films theatrical trailer. 

==Reaction==

===Box office===
Hitchcock has earned an estimated $23,570,541 worldwide.    During its opening on Thanksgiving weekend, the film debuted in 17 theaters and grossed an average of $16,924 per theater. 

===Critical response===
The film received mixed to positive reviews from the critics. Review aggregator Rotten Tomatoes gives a score of 62% based on reviews from 195 critics, with a rating average of 6.2 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 55% based on 40 reviews. 
 Best Picture, lead actor, lead actress, adapted screenplay, music score, art direction."  John Patterson of The Guardian called the film "clever and witty"; "the making of Psycho is depicted in detail without our seeing one frame of the completed movie" and concluding "it lives and breathes through Hopkins and Mirren." 

Upon its theatrical release, Mary Pols of Time (magazine)|Time called the film "a feel-good frolic, which is fine for anyone who prefers their Hitchcock history tidied up, absent the megalomania, the condescending cruelty and tendency to sexual harassment that caused his post-Psycho blonde discovery Tippi Hedren to declare him a mean, mean man."  Roger Ebert of the Chicago Sun-Times gave the film a positive review and felt that the film depended most on Helen Mirrens portrayal of Alma Reville, which he found to be "warm and effective." 

The Atlantic s Govindini Murty called the film "smart and entertaining" and also provided a cultural guide to the themes, personalities, and cinematic references in the film, from German Expressionism to the paintings of Edward Hopper. 
 The Girl, The Birds and Marnie (film)|Marnie. Justin Chang of Variety (magazine)|Variety wrote that "the comparatively frothy Hitchcock offers a more sympathetic, even comedic assessment of the man behind the portly silhouette."  Todd McCarthy of The Hollywood Reporter also made note that the film "brings a measure of authenticity entirely missing from The Girl." When writing about the film as a whole, McCarthy said, "Hitchcock might be a work of fantasy and speculation as much as it is history and biography, but as an interpretation of a major talents inner life and imagination, its undeniably lively and provocative." 

===Accolades===

{| class="wikitable"  
! Award
! Category
! Recipients and nominees
! Result
|-
|  Academy Awards  (85th Academy Awards|85th) Academy Award Best Makeup and Hairstyling Gregory Nicotero, Howard Berger, Peter Montagna and Julie Hewitt
|  
|- Alliance of Women Film Journalists Actress Defying Age and Ageism Helen Mirren
| 
|- British Academy Film Awards BAFTA Award Best Actress Helen Mirren
| 
|- BAFTA Award Best Makeup and Hair
|
| 
|-
|Dallas-Fort Worth Film Critics Association Best Actress Helen Mirren
| 
|- Golden Globe Awards Golden Globe Best Motion Picture Actress – Drama Helen Mirren
| 
|- Houston Film Critics Society Best Score
|
| 
|- London Film Critics Circle British Actress of the Year Helen Mirren
| 
|- Phoenix Film Critics Society Best Actor Anthony Hopkins
| 
|- Best Original Score Danny Elfman
| 
|- Saturn Awards
| Best Independent Film
|
| 
|- Saturn Award Best Actress Helen Mirren
| 
|- Saturn Award Best Make-Up Gregory Nicotero, Howard Berger, Peter Montagna and Julie Hewitt
| 
|- Screen Actors Guild Awards    Best Actress
| Helen Mirren
|  
|-
|rowspan=2|St. Louis Gateway Film Critics Association Best Actress Helen Mirren
| 
|- Best Scene Anthony Hopkins in lobby conducting to music/audience’s reaction during “Psycho” shower scene
| 
|- Washington DC Area Film Critics Association Best Actress Helen Mirren
| 
|-
|}

==See also== The Girl, The Birds and Marnie (film)|Marnie.

==References==
 

== Further reading ==
*  

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 