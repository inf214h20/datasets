Anjaam
 
 {{Infobox film
| name = Anjaam अंजाम انجام
| image = Anjaam (movie poster).jpg
| image_size =
| caption = Film poster 
| director       = Rahul Rawail
| producer       = Maharukh Johki Rita Rawail
| writer         = Sutanu Gupta Rumi Jaffrey Gautam Rajadhyaksha
| narrator       = 
| starring       = Shahrukh Khan Madhuri Dixit Johnny Lever Himani Shivpuri Deepak Tijori
| music          = Anand-Milind
| cinematography = Sameer Arya
| editing        = Suresh Chaturvedi
| studio         = 
| distributor    = 
| released       = 22 April 1994
| runtime        = 171 mins
| country        = India
| language       = Hindi
| budget =
}}
 thriller drama movie released on 22 April 1994. Directed by Rahul Rawail, it stars Madhuri Dixit, Shahrukh Khan, Deepak Tijori, Himani Shivpuri, Tinnu Anand, Kalpana Iyer and Kiran Kumar. The films music was composed by Anand-Milind with lyrics written by Sameer. The film is about the consequence / result (Anjaam) of the slightest mistake and how it can ruin your whole life. It also focuses on the atrocities committed against women.

This was the first time that Madhuri Dixit and Shahrukh Khan were paired together.  Khan and Dixit were praised for their respective performances. Madhuri Dixit earned a Filmfare Best Actress nomination for her role but won the award for her performance in Hum Aapke Hain Koun..!. Khan was again featured in a negative role - he received the Filmfare Best Villain Award for his performance. He was nominated for the same award the previous year for his performance in Yash Chopras Darr.

== Plot ==
Vijay Agnihotri (Shah Rukh Khan) comes from a wealthy family and is very spoiled as well as psychopathic. He meets Shivani (Madhuri Dixit) in an airplane who is a flight attendant with whom he instantly falls in love  but she shows no interest in him – this does not stop Vijay and he continues to pursue her, only to be rejected every time. Vijay informs his mother that he intends to marry Shivani. When they approach Shivani’s family with a proposal, they witness that Shivani had married another man named Ashok. Vijay is now heartbroken. Shivani and Ashok decide to move to United States|America.

Four years later, Vijay still cannot forget Shivani and repeatedly turns down marriage proposals brought by his mother. He comes across Shivani again and Ashok who have a daughter named Pinky. Vijay befriends Ashok with fake airline project as the hope of getting closer to Shivani. Ashok is totally oblivious to Vijays real intentions, to the extent that he does not believe Shivani when she tries to convince him of what Vijay is planning against them. One day, Ashok kicks Shivani out of their house after they have an argument. Vijay witnesses this and severely beats Ashok, leaving him unconscious. When Ashok is being treated in the hospital, Vijay removes the oxygen mask keeping Ashok without oxygen supply, thereby killing him. Shivani attempts to convince the police that Vijay is responsible for Ashok’s death. However, Vijay bribes his friend, Inspector Arjun Singh, to provide an alibi, meaning Shivani could not be believed and Vijay is released without charge. Vijay then approaches Shivanis house and begs her to say she loves him. When she refuses, he frames Shivani for his attempted murder and she is sentenced to three years in prison whilst Pinky is placed under the care of Shivanis sister and drunkard brother-in-law (Tinu Anand). Her brother-in-law treats Pinky extremely badly which eventually causes her to run away with the help of her aunt. Vijay accidentally kills Shivani’s sister and daughter by running his car over them. Shivani learns about their deaths and realizes Vijay is the one who killed them. Shivani decides go to any lengths to seek revenge. In an attempt to escape, she makes a complaint about the brutality of her prison guard. Again her plea is ignored. In prison she comes to know that she is pregnant with Ashoks child . When the prison guard learns that Shivani tried to complain, she gives her a severe beating which causes her to have a miscarriage. Shivani soon kills the prison guard by dragging her to the gallows and hanging her by her neck. But as there is no evidence, she is not convicted for that.

Three years later, Shivani is released from prison. First, she goes to her brother-in-laws house and kills him by choking him with rupee notes and chewing off a significant amount of flesh from his arm. Inspector Singh learns about the murder and suspects Shivani. Singh misbehaves with her in a barn, but she sets the barn on fire, killing Inspector Singh.  She then searches for Vijay where she comes to know that he has moved to Tikamgarh. She then goes to work at a hospital for the mentally ill and finds Vijay, who had become paralyzed in the car accident after running over Shivanis family. She volunteers to look after him and rehabilitate him. When cured, Vijay begs Shivani to say she loves him. She opens her arms to him. Whilst they embrace, she stabs him and then starts attacking him for everything he did. She confesses that she made him better for one purpose: to kill him. (She says its a sin to kill a handicapped person, who cannot defend himself.) Eventually, they both dangle from a cliff (with Vijay holding onto Shivani’s foot). Vijay says that if he falls to his death hell take Shivani with him. Shivani then says that it is not as necessary for her to live as it is for Vijay to die. So she lets go off the cliff and they both fall to their deaths.

== Cast ==
{| class="wikitable"
|- 
! Actor/Actress !! Role 
|-
| Shah Rukh Khan || Vijay Agnihotri 
|-  Madhuri Dixit || Shivani Chopra
|- Deepak Tijori || Ashok Chopra
|- Johnny Lever || Champa Bose (Eunuch)
|- Himani Shivpuri || Nisha 
|- Sudha Chandran || Shivanis sister
|- Beena || Padma Agnihotri
|- Kiran Kumar || Arjun Singh 
|- Baby Gazala || Pinky Chopra
|- Tinnu Anand || Mohanlal
|}

==Soundtrack==
{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =      
}} Abhijeet in this album "Badi Mushkil hai" is considered one of the most melodious song till date ". 
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Badi Mushkil Hai" Abhijeet
| 05:30
|- 
| 2
| "Chane Ke Khet Mein" Poornima
| 05:50
|-  
| 3
| "Tu Samne Jab Aata Hai"
| Udit Narayan, Alka Yagnik
| 05:55
|- 
| 4
| "Barson Ke Baad" 
| Alka Yagnik
| 04:13
|- 
| 5
| "Sun Meri Bano"
| Alka Yagnik
| 05:56
|- 
| 6
| "Kolhapur Se Aaye Jhumke" 
| Sadhana Sargam
| 05:04
|- 
| 7
| "Partighat Ki Jwala" 	
| Sapna Awasthi
| 01:49
|}

==Awards==
* Nomination Filmfare Award for Best Actress - Madhuri Dixit
* Filmfare Award for Best Performance in a Negative Role - Shah Rukh Khan

==References==
 

==External links==
* 

 
 
 
 
 
 
 