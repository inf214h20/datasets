Casanova's Big Night
 
{{Infobox film
| name           = Casanovas Big Night
| image          = Casanovas Big Night 1954 poster.jpg
| image_size     = 220
| caption        = 1954 US Theatrical Poster
| director       = Norman Z. McLeod Paul Jones
| writer         = Aubrey Wisberg
| based on       =
| screenplay     = Edmund Hartmann Hal Kanter
| narrator       =
| starring       = Bob Hope Joan Fontaine Basil Rathbone John Carradine Lon Chaney, Jr. Raymond Burr
| music          = Lyn Murray
| cinematography = Lionel Lindon
| editing        = Ellsworth Hoagland
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = United States English
| budget         =
}}
 spoof of swashbuckling historical adventure films. It was directed by Norman Z. McLeod.

Hope plays a tailor who impersonates Giacomo Casanova, the great lover. The film also stars Audrey Dalton, Basil Rathbone, Hugh Marlowe, John Carradine, Hope Emerson, Lon Chaney, Jr., Raymond Burr, Natalie Schafer, and Vincent Price (in a cameo appearance as the real Casanova).

==Plot summary==
Pippo, a tailor, impersonates Casanova to woo the girls, particularly the widow Bruni. Casanova has left town, pursued by creditors who persuade Pippo to impersonate Casanova at the behest of a Genoan family that will pay "Casanova" to test the fidelity of the sons betrothed. 

Pippo, the widow Bruni and Casanovas valet Lucio travel to Venice.  The Doge of Venice, "a snake with whiskers," to use Pippos description, intends to use the intended seduction as an excuse to wage war against Genoa. After many humorous adventures, exploiting Pippos traits of vanity, arrogance and cowardice, the heroine so impresses Pippo with her dignity that he refuses to cooperate in the plot to ruin her character. He is arrested by the Doge and sentenced to death on the guillotine. A desperate Pippo turns to strangers for help, but is shocked when they prefer that he lose his head.

==Cast==
 
* Bob Hope as Pippo Popolino
* Joan Fontaine as Francesca Bruni
* Audrey Dalton as Elena Di Gambetta
* Basil Rathbone as Lucio / Narrator
* Hugh Marlowe as Stefano Di Gambetta
* Arnold Moss as The Doge
* John Carradine as Foressi
* John Hoyt as Maggiorin
* Hope Emerson as Duchess of Castelbello
* Robert Hutton as Raphael, Duc of Castelbello
* Lon Chaney Jr. as Emo the Murderer (as Lon Chaney)
* Raymond Burr as Bragadin
* Frieda Inescort as Signora Di Gambetta
* Primo Carnera as Corfa
* Frank Puglia as Carabaccio
* Paul Cavanagh as Signor Alberto Di Gambetta
* Romo Vincent as Giovanni
* Harry Brandon as Captain Rugello
* Natalie Schafer as Signora Foressi
* Douglas Fowley as Second Prisoner
* Nestor Paiva as Gnocchi
* Lucien Littlefield as First Prisoner
* Vincent Price as Casanova (cameo)
 

==References==
 	

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 