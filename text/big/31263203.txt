Dragon Eyes
 
{{Infobox film
| name           = Dragon Eyes
| image          = Dragon Eyes FilmPoster.jpeg
| caption        = Official film poster
| director       = John Hyams
| producer       = {{plainlist|
* Alan Amiel
* Joel Silver
* Moshe Diamant
* Steve Richards
* Courtney Solomon
}}
| writer         = Tim Tori
| starring       = {{plainlist|
* Cung Le
* Jean-Claude Van Damme
}}
| music          = Michael Krassner
| cinematography = Stephen Schlueter
| editing        = {{plainlist|
* Andrew Bentler
* Andrew Drazek
* Jon Greenhalgh
}}
| studio         = {{plainlist|
* After Dark Films
* Dark Castle Entertainment
* Silver Pictures
}}
| distributor    = After Dark Films
| released       =   }}
| runtime        = 91 minutes
| country        = United States
| language       = English
}}
Dragon Eyes is a 2012 American martial arts film starring Cung Le and Jean-Claude Van Damme. It was directed by John Hyams.   In New Orleans, a mysterious man who looks to unite two warring gangs against the lawmen who have been using them to advance their corrupt agenda.

==Plot==
St. Jude Square is a neighborhood living in fear and despair. The dueling gangs of local kingpins, Dash and Antoine, terrorize the streets, and the citizens live without a shred of hope until mysterious stranger, Ryan Hong (Cung Le) arrives. He begins to play one gang against the other, by calling on the teachings of his mentor, Tiano (Jean-Claude Van Damme), to find the strength to battle back. However, just as he begins to bring the community under control, Hong is confronted by Mr. V (Peter Weller), the towns corrupt police chief. At first Mr. V is impressed by Hongs skill, but soon sees Hong as a threat to his regime and the two are locked in a head to head battle, pitting the fear and corruption of Mr. Vs regime against the new beginning Hong represents for the people of St. Jude Square.

==Cast==
*Cung Le as Ryan Hong
*Jean-Claude Van Damme as Tiano
*Peter Weller as Mr. V
*Crystal Mantecon as Rosanna
*Danny Mora as Grandpa George
*Kristopher Van Varenberg as Sgt. Feldman
*Luis Da Silva as Dash
*Dan Henderson as Beating Police Officer
*Rich Clementi as Devil Dog Gangster #4
*Trevor Prangley as Lord

==Home media== Region 2. A limited number of the DVD slipcovers in Australia were for sale from sanity.com.au and autographed by Jean-Claude Van Damme.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 