Dressed to Kill (1941 film)
{{Infobox film
| name           = Dressed to Kill
| image          = Dressed_to_kill_poster.jpg
| caption        = Original film poster Eugene J. Forde
| producer       = Sol M. Wurtzel
| writer         = Novel:  Richard Burke "Death Takes no Bows" Screenplay: Brett Halliday Manning OConner Stanley Ruth
| starring       = Lloyd Nolan Mary Beth Hughes Sheila Ryan William Demarest
| music          = Cyril J. Mockridge
| cinematography = Glen MacWilliams Fred Allen
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}

Dressed to Kill is a 1941 crime mystery starring Lloyd Nolan, Mary Beth Hughes and Sheila Ryan.  The film was based on Death Takes No Bows, a mystery novel  by Richard Burke.

==Plot Summary==
 Hotel du Nord where she is staying, but as they are about to leave together, Michael hears a woman screaming from one of the rooms.

It turns out that the hotel maid Emily (Virginia Brissac) has discovered two dead people: producer Louis Lathrop, owner of the hotel and the adjoining theater, and Desiree Vance, one of Lathrops actresses. Both are dressed up in medieval costumes. Lathrop also has the head from a dog costume on him.

Police Investigator Pierson (William Demarest) arrives at the scene and learns from the hotel manager, Hal Brennon (Charles Amt), that the costumes seem to be from Lathrops only successful show, Sweethearts of Paris, from many years before. On the cast of the show was Desiree as the leading lady, and Carlo Ralph (Erwin Kalser) as Beppo the Dog. Immediately Shayne suspects Carlo of being the killer, since there is a symbolism in placing the dog costume head on Lathrop.

On the cast of that show were also actors David Earle (Charles Trowbridge) and Julian Davis (Henry Daniell). Earle comes to the hotel and tells the police that Lathrop had hosted a private party for the entire cast of the show to celebrate its anniversary. Shayne examines the list of people involved in the show production and discovers that the musical director was Max Allaron (Milton Parsons), an alcoholic who also lives at the hotel.

As the investigation proceeds, Shayne learns that Lathrop had another woman besides Desiree, and that the apartment has many entrances and exits. From Earles daughter he also learns that Davis stole money from Lathrop, and he decides to pay Davis a visit. Shayne finds Davis with Phyllis Lathrop, Louis wife. They confess to embezzling money from Louis, but claim to be innocent of his murder. They hire Shayne to help them prove their innocence.

Shayne continues his investigation and talks to Max Allaron. He learns that Carlo died in the First World War in France. In Desirees room, Shayne finds a metal box containing a letter from Carlo written after the war in 1920, where he claims to have returned to the U.S after being held hostage for months. Clearly he has survived the war.

Shayne brings Davis to the Lathrop apartment and they discover a hidden passage to the maid Emilys room downstairs. When they enter Emilys apartment, they find her dead body and a note explaining that she was the one who killed Lathrop because he betrayed her years before for another woman. It also turns out Emily was once known as actress Lynn Evans.

However, Shayne does not believe that Emily has killed herself, so he continues searching for the real killer. When Shayne is back in Lathrops apartment, Investigator Pierson is knocked out in the next room by Allaron. Shayne rushes to see what happened and Otto Kahn, the theatre doorman, arrives and confesses that he is the one who killed Lathrop and Desiree. He is really Carlo, and was married to Desiree before she left him for Lathrop. He also killed Emily since she found out too much about him. Allaron has been blackmailing Carlo since he saw him leave the apartment right after the killings.

While they are talking, Pierson comes back to life and together with Shayne he overpowers Otto and Allaron. Shayne appoints Pierson to be best man at the wedding later in the day, but when Shayne arrives to Joannes apartment, she has eloped with her ex-boyfriend because she has grown tired of waiting for him. 

==Cast==
*Lloyd Nolan as Michael Shayne
*Mary Beth Hughes as Joanne La Marr
*Sheila Ryan as Connie Earle
*William Demarest as Inspector Pierson
*Virginia Brissac as Lynne Evans, aka Emily, the maid   
*Milton Parsons as Max Allaron
*Erwin Kalser as Otto Kahn/Carlo Ralph
*Henry Daniell as Julian Davis
*Charles Arnt as Hal Brennon
*Charles Trowbridge as David Earle

==Production== Producers Releasing Corporation (PRC) and starred Hugh Beaumont.  There were also three radio shows (1944–1953) and a television series (1960–1961) based on the Michael Shayne character.

==Critical reaction to DVD release==

When the film was released on DVD in 2005,   called the film "one of the most enjoyable B movies Ive ever seen."    writes the film "benefits from a powerhouse supporting cast and the effectively moody cinematography of Glenn MacWilliams."

==External links==
*  
*  

==References==
 

 
 
 
 
 
 
 
 