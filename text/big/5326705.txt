Four Adventures of Reinette and Mirabelle
{{Infobox film
| name           = Four Adventures of Reinette and Mirabelle
| image          = 4_aventures_de_Reinette_et_Mirabelle.jpg
| caption        = 
| director       = Eric Rohmer
| producer       = 
| writer         = Eric Rohmer Joëlle Miquel
| narrator       = 
| starring       = Joëlle Miquel Jessica Forde Philippe Laudenbach
| music          = Jean-Louis Valéro
| cinematography = Sophie Maintigneux
| editing        = María Luisa García
| distributor    = Les Films du Losange
| released       = 1987
| runtime        = 102 min.
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Four Adventures of Reinette and Mirabelle ( ) is a 1987 French film directed by Éric Rohmer, lasting 102 minutes and starring Joëlle Miquel, Jessica Forde, Philippe Laudenbach. 

==Synopsis==
Four episodes (Rohmer titles them ‘Aventures,’ ‘Adventures’) in the relationship of two young women, Reinette, a country girl, and Mirabelle, a Parisian: city girl and country girl. The first is entitled ‘The Blue Hour’ and recounts their meeting. The second centers on a café and a waiter, a difficult man ‘with attitude.’ In the third they interact with and discuss their different views on society’s ‘margins’: beggars, thieves, swindlers. And in the fourth, on a bet, Reinette, an art student, and Mirabelle succeed in selling one of Reinette’s paintings to an art dealer while Reinette pretends to be mute and Mirabelle, who effects not to know her, does all the talking.

==External links==
*  
 

 
 
 
 