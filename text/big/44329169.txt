Safelight (film)
 
{{Infobox film
| name=Safelight
| image = 
| image_size =
| caption = 
| director = Tony Aloupis
| producer = {{Plainlist|
*Cory Neal
*Bernie Gewissler
*Tony Aloupis}}
| writer = Tony Aloupis
| starring = {{Plainlist|
*Juno Temple
*Evan Peters
*Kevin Alejandro
*Meaghan Martin
*Christine Lahti
*Ariel Winter
*Jason Beghe}}
| music = Joel P. West
| cinematography = Gavin Kelly
| editing = {{Plainlist|
*Ed Marx
*John Wesley Whitton}}
| studio = {{Plainlist|
*Aloupis Productions
*Hacienda Film Co.}}
| released =  
| runtime = 84 minutes
| country = United States
| language = English
}}
Safelight is a 2015 American drama film, written and directed by Tony Aloupis, and starring Juno Temple, Evan Peters, Kevin Alejandro, Jason Beghe, Ariel Winter, and Christine Lahti. The film had its world premiere on April 17, 2015 at the Nashville Film Festival. It will also screen at the Newport Beach Film Festival on April 25, 2015.

==Plot==
Set in the 1970s, a teenage boy and girl discover a renewed sense of possibility as they go on a road trip to photograph lighthouses along the California coast.

==Cast==
*Evan Peters as Charles
*Juno Temple as Vicki
*Kevin Alejandro as Skid
*Meaghan Martin as Sharon
*Jason Beghe as Eric
*Ariel Winter as Kate
*Matthew Ziff as Kyle
*Don Stark as Jack Campbell
*Ever Carradine as Lois
*Christine Lahti as Peg
*Joel Gretsch as Mr. Sullivan
*Roma Maffia as Rose
*Gigi Rice as Lillian
*Will Peltz as Jason

==Production==
In March 2012, Evan Peters and Juno Temple were cast in the two leading roles.  The film was originally titled Truck Stop. In April 2012, Christine Lahti joined the cast as a character named Peg, Matthew Ziff joined the cast as Kyle, and Meaghan Martin was cast as Sharon.  

==Marketing and release==
On February 17, 2015, the first theatrical trailer was released for the film.  Safelight had its world premiere at the Nashville Film Festival on April 17, 2015,  and will screen at the Newport Beach Film Festival on April 25, 2015. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 