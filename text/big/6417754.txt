This Boy's Life (film)
{{Infobox film
| name           = This Boys Life
| image          = This Boys Life.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Caton-Jones
| producer       = Fitch Cady Art Linson
| screenplay     = Robert Getchell
| based on       =  
| starring       = Leonardo DiCaprio Robert De Niro Ellen Barkin
| music          = Carter Burwell David Watkin Jim Clark Knickerbocker Films Warner Bros. Pictures
| released       =      
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,104,962   
}}
 of the same name by American author Tobias Wolff. It is directed by Michael Caton-Jones and stars Leonardo DiCaprio as Tobias Wolff, Robert De Niro as stepfather Dwight Hansen, and Ellen Barkin as Tobys mother, Caroline. The film also features Chris Cooper, Carla Gugino, Eliza Dushku and Tobey Maguire in his feature film debut. 

==Plot==
Nomadic, flaky Caroline Wolff (Ellen Barkin) just wants to settle down in one place, find a decent man, and provide a better home for her and her son, Tobias "Toby" Wolff (Leonardo DiCaprio). When she moves to Seattle, Washington, and meets the seemingly respectable Dwight Hansen (Robert De Niro) (in real life Dwight Thompson worked as a mechanic for Seattle City Light at the time), she thinks she has it made. Toby, however, comes to feel differently; Dwights true personality is revealed after Toby spends a few months separated from his mother with Dwight and his children. The boys stepfather-to-be seems to want to mold Toby into a better person, but his method includes emotionally, verbally and physically abusing the boy.
 Concrete and live with his older brother Gregory, Toby decides to apply for East Coast prep-school scholarships. Realizing his grades are not good enough to be accepted, Toby devises a plan to submit falsified grade reports. Meanwhile, the friendship between Arthur and Toby becomes strained when Arthur confronts Toby, saying he is behaving more and more like Dwight, and asks, "Why should you be the one who gets to leave?". Even so, Arthur helps his friend get the papers he needs to falsify his grade records and Toby sends off his prep-school applications. When Toby assures Arthur he too can leave Concrete and have a better life, Arthur replies he will most likely stay. After numerous rejections, Toby is finally accepted by the Hill School in Pottstown, Pennsylvania, with a full scholarship.

At the end of the film, following two years of marriage, Caroline defends Toby from Dwight during a physically violent argument and they are both seen leaving Dwight and the town of Concrete. The real Dwight died in 1992. Caroline remarried and moved to Florida. Arthur left Concrete after all, and became a successful businessman in Italy. Dwights children all married and stayed in Seattle.

==Cast==
* Leonardo DiCaprio as Tobias "Toby" Wolff
* Robert De Niro as Dwight Hansen
* Ellen Barkin as Caroline Wolff Hansen
* Jonah Blechman  as Arthur Gayle
* Eliza Dushku as Pearl Hansen
* Chris Cooper as Roy
* Carla Gugino as Norma Hansen Zack Ansley as Skipper Hansen
* Tracey Ellis as Kathy
* Kathy Kinney as Marian
* Tobey Maguire as Chuck Bolger Sean Murray as Jimmy Voorhees
* Lee Wilkof as Principal Skippy
* Bill Dow as Vice Principal
* Deanna Milligan and Morgan Brayton as Silver Sisters

==Production==
Largely filmed in the state of Washington (U.S. state)|Washington, the town of Concrete, Washington (where Tobias Wolffs teen years were spent with his mother and stepfather, Dwight), was transformed to its 1950s appearance for a realistic feel. Many of the towns citizens were used as extras, and all external scenes in Concrete (and some internal scenes, as well) were shot in and around the town, including the former elementary school buildings and the still-active Concrete High School building.

==Release==
===Box office===
The film was released in limited release on April 9, 1993, and earned $74,425 that weekend;  upon its wide release on April 23, the film opened at #10 at the box office and grossed $1,519,678.  The film would end with a domestic gross of $4,104,962. 

===Critical reception===
The film gained mostly positive reviews; review aggregator website Rotten Tomatoes gave the film a 75% Fresh rating from 36 critics.  On Metacritic, where they give a normalized score, the film has a 60/100. 

===Home media===
This Boys Life was released on DVD May 13, 2003. 

==Soundtrack== Come Fly South Pacific. However, most of the music reflects Tobys fondness for rock and roll and doo wop, including songs by Eddie Cochran, Frankie Lymon and the Teenagers, and Link Wray. Carter Burwell composed the films pensive score, which featured New York guitarist Frederic Hand. 

==References==
 

==External links==
*  
*  
*  
*  
*   film trailer at YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 