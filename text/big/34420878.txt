Inaindha Kaigal
{{Infobox film
| name           = Inaindha Kaigal
| image          = Inaindha Kaigal DVD cover.svg
| image_size     =
| caption        = DVD cover
| director       = N. K. Viswanathan
| producer       = Abhavanan
| writer         = Abhavanan
| starring       =   Gyan Varma
| cinematography = N. K. Viswanathan
| editing        = S. Ashok Mehtha
| distributor    =
| studio         = Thiraichirpi
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Tamil
}}
 1990 Cinema Indian Tamil Tamil film, Sindhu in Gyan Varma Hindi as Telugu as Sahasa Ghattam.

==Plot==
Dr. Chandralekha (Srividya), a grieving mother, asks an ex-army man David Kumar (C. Arunpandian) to rescue her son Gunasekaran (Prabhakaran) who is locked-up in a military prison. But at the same time, the hideous criminal P. K. Roy (Nassar) appoints the fugitive Pratap (Ramki) to kidnap Gunasekharan from the prison. Both leave for their missions respectively, not knowing what surrender holds in his past and the fugitive takes this personal.

==Cast==
*C. Arunpandian as David Kumar
*Ramki as Pratap
*Nirosha as Julie Sindhu as Geetha
*Nassar as P. K. Roy Senthil as Mani
*Srividya as Chandralekha
*Murali Kumar as Ashok
*Prabhakaran as Gunasekaran
*A. R. S as Lawyer Shanmugam
*Sivaraman as Vellai Subbaiah
*K. S. Selvaraj
*Charle as a singer (guest appearance)
*Veera Raghavan in a guest appearance
*Kullamani in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Inaindha Kaigal
| Type        = soundtrack Gyan Varma
| Cover       = 
| Released    = 1990
| Recorded    = 1990 Feature film soundtrack
| Length      = 30:31
| Label       =  Gyan Varma
| Reviews     = 
}}
 Gyan Varma. The soundtrack, released in 1990, features eight tracks with lyrics written by Abhavanan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics
|- 1 || Aadi Maasam || Gangai Amaran || 2:23
|- 2 || Andhinera Thendral || S. P. Balasubrahmanyam, Jayachandran || 4:29
|- 3 || Chinnapoove Chinnapoove || Deepan Chakravarthy, Vidhya || 4:31
|- 4 || Gangai Karaiyil || Malaysia Vasudevan || 4:59
|- 5 || Malaiyorum Kuyil || Deepan Chakravarthy, Vidhya || 4:28
|- 6 || Oracha Manjala || Abhavanan || 2:48
|- 7 || Mella Mella || Vidhya || 3:06
|- 8 || Kudukuduppai || S. P. Balasubrahmanyam, B. S. Sasirekha || 3:47
|}

==Reception==
Indian express called it "ambitious, vast, brash, blantantly loud and empty" 

==Legacy==
In 2013 Tamil film Sutta Kadhai, lead heroes of that film Balaji and Venkatesh appeared as fans of Ramki and Arun Pandian. In one scene, Balaji utter the films title referring to their collaboration on mission.

==Trivia==

Inaindha Kaigal had an extra ordinary opening and it created Stampede in some theatres killing 2 persons. This news which appeared in tamil daily Dina Thanthi was advertised as Poster for its Kerala release.

==References==
 

==External links==
*  

 
 
 
 