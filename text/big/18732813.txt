Illustrious Corpses
 
{{Infobox film
| name           = Illustrious Corpses (Cadaveri eccellenti)
| image          = Cadaveri-eccellenti-poster.jpg
| caption        = Italian film poster
| director       = Francesco Rosi
| producer       = Alberto Grimaldi
| writer         = Tonino Guerra Lino Iannuzzi Francesco Rosi Leonardo Sciascia
| starring       = Lino Ventura
| music          = Piero Piccioni
| cinematography = Pasqualino De Santis
| editing        = Ruggero Mastroianni
| studio         = Produzioni Europee Associati Les Productions Artistes Associés
| distributor    = United Artists
| released       =  
| runtime        = 127 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Cadavre Exquis, invented by André Breton, in which the participants draw consecutive sections of a figure without seeing what the previous person has drawn, leading to unpredictable results, and is meant to describe the meandering nature of the film with its unpredictable foray into the world of political manipulations, as well as the ("illustrous") corpses of the murdered judges.
 100 Italian films to be saved. Massimo Bertarelli, Il cinema italiano in 100 film: i 100 film da salvare, Gremese Editore, 2004, ISBN 88-8440-340-5.
   

==Plot==
The film starts with the murder of District Attorney Vargas in Palermo, amongst a climate of demonstrations, strikes and political tension between the Left and the government. The subsequent investigation failing, the police assign the protagonist Inspector Rogas (Lino Ventura) to solve the case. While he is starting his investigation, two judges are killed. All victims turn out to have worked together on several cases. After Rogas discovers evidence of corruption surrounding the three government officials, he is encouraged by superiors "not to forage after gossip," but to trail the "crazy lunatic who for no reason whatever is going about murdering judges." This near admission of guilt drives Rogas to seek out three men wrongfully convicted by the murdered judges. He is joined by a journalist friend working for a far-left newspaper, Cusan.

Rogas finds his likely suspect in Cres, a man who was convicted of attempting to kill his wife. Mrs. Cres accused her husband of trying to kill her by poisoning her rice pudding, which she escaped only because she fed a small portion first to her cat, who died. Rogas concludes that he was probably framed by his wife, and seeks him out, only to find that he has disappeared from his house. Meanwhile another district attorney is killed, and eyewitnesses see two young revolutionaries running away from the scene. Rogas, close to finding his man, is demoted, and told to work with the political division to pin the crimes on the revolutionary Leftist terrorist groups.

Rogas discovers that his phone is tapped. He seeks out the Supreme Courts president (Max von Sydow) in order to warn him that he is most likely the next victim. The president details a philosophy of justice wherein the court is incapable of error by definition. Music from a party in the same building leads to Rogas discovering the Minister of Justice (Fernando Rey) at the party with many revolutionary leaders, amongst them the editor of the revolutionary paper Cusan is working for, Galano, and Mrs. Cres. He and the Minister have a discussion, where the Minister reveals that sooner or later, his party will have to form a coalition with the Communist Party, and that it will be their task to prosecute the far-leftist groups. The murder of the judges as well as Rogass investigations help raise the tension and justify the prosecution of the far-left groups. Rogas also discovers that his suspect, Cres, is present at the party. Rogas meets with the Secretary-General of the Communist Party in a museum. Both of them are killed. Amongst raising tensions between revolutionaries and the government, who mobilize the army, the murder of the Secretary-General is blamed on Rogas by the chief of police. The film ends with a discussion between Cusan and the vice-secretary of the Communist Party, who claims that the time is not yet ready for the revolution and the party will not react to the governments actions. "But then the people must never know the truth?", asks Cusan. The vice-secretary answers: "The truth is not always revolutionary." It is a sardonic concluding comment on the strategy at the time of the historic compromise with Christian Democracy adopted by the Communist party, referring back to the motto To tell the truth is revolutionary adopted from Ferdinand Lassalle by Antonio Gramsci, the partys most famous former leader and author of the Prison Notebooks.

==Cast==
* Lino Ventura as Inspector Amerigo Rogas
* Tino Carraro as Chief of Police
* Marcel Bozzuffi as The lazy
* Paolo Bonacelli as Dr. Maxia
* Alain Cuny as Judge Rasto
* Maria Carta as Madame Cres
* Luigi Pistilli as Cusan
* Tina Aumont as The prostitute
* Renato Salvatori as Police commisary
* Paolo Graziosi as Galano
* Anna Proclemer as Nocios wife
* Fernando Rey as Security Minister
* Max von Sydow as Supreme Courts president

==See also==
*Equal Danger, the novel by Leonardo Sciascia on which this film is based.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 