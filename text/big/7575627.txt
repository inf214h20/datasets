Asterix Conquers America
{{Infobox film name = Asterix in Amerika image = Asterix conquers america cover.jpg caption = VHS cover writer = Thomas Platt and Rhett&nbsp;Rooster, adapted from René Goscinny and Albert Uderzo starring = narrator = Harald Juhnke (German)   John Rye (English) director = Gerhard Hahn producer = Gerhard Hahn and Jürgen&nbsp;Wohlrabe music = Harold Faltermeyer distributor = 20th Century Fox (Europe) Judgenfilm (Germany) released =1994 runtime = 85 minutes country = France, Germany language = German / English / French budget = DEM 19.000.000 gross = $768,488 (USA)}} 1994 animated movie directed by Gerhard Hahn. It was produced in Germany as Asterix in Amerika by Gerhard Hahn and Jürgen Wohlrabe, and is the first Asterix film produced outside of France (where it is known as Astérix et les Indiens). It is loosely based on the comic Asterix and the Great Crossing but has numerous significant variations on the plot. It featured the voice talents of Craig Charles as Asterix and Howard Lew Lewis as Obelix.

==Plot== Caesar sends Getafix and edge of Unhygienix lacks fresh fish (as always), which starts a fight, spilling Getafixs magic potion. Because fresh fish is a vital ingredient thereof, Asterix and Obelix go fishing while Getafix gathers herbs, accompanied by Dogmatix. In the forest, Getafix and Dogmatix are captured by Lucullus. Aboard a Roman galley bound for the "worlds edge", they pass Asterix and Obelix, who pursue them, using Obelix as an outboard motor until he is diverted by the aroma of food from a passing pirate ship, and then blown off course by a violent storm. After a long pursuit, a dolphin brings Dogmatix (whom Lucullus had dropped overboard) and guides them to the Romans. Seeing the Gauls and mistaking the North American coast for the worlds edge, the Romans launch Getafix from a catapult, then return to Rome, while Asterix and Obelix make landfall. In Gaul, Caesar lays siege to their village and waits for the last of the magic potion to run out.
 Native American whom he mistakes for a disguised Roman. When he finds Asterix gone and his helmet lying on the ground, he has Dogmatix follow the scent. On the way he saves a Native American chiefs daughter from a stampede of bison. In the Native American village, Asterix is tied with Getafix to a pole, menaced by the natives. Obelix and the girl arrive to save them, and the chief frees them; thus enraging the medicine man, who is insulted again when Getafix proves himself the superior magician. That night, the medicine man pretends to offer peace, but uses the peace pipe to drug the visitors.
 Cacofonix reveals that Caesar has captured the villagers, whereupon Asterix and Obelix — disguised as legionaries — take gourds of fresh potion to their friends imprisoned at the nearby camp of Compendium. Thus enabled, the Gauls free themselves, destroy the camp, and return victorious to their village. During the battle, Caesar escapes disguised as a wine barrel, while Lucullus is consumed alive by Caesars pet Panthera|panther.

==Cast==

===Original German version===
*Peer Augustinski - Asterix
*Ottfried Fischer - Obelix
*Harald Juhnke -  Narrator
*Ralf Wolter - Getafix
*Jochen Busse - Cacofonix Caesar
*Michael Habeck - Lucullus
*Andreas Mannkopff - The Centurion

===French version===
*Roger Carel - Asterix
*Pierre Tornade - Obelix
*Henri Labussière - Getafix Chief Vitalstatistix
*Michel Tugot-Doris - Cacofonix Unhygienix
*Robert Caesar
*Jean-Luc Galmiche - Lucullus
*Olivier Jankovic - Stupidus
*Nathalie Spitzer - Falbala
*Yves Pignot - The Centurion
*Claude Chantal -Impedimenta
*Joël Zaffarano - The Captain

===English version===
*Craig Charles -  Asterix
*Howard Lew Lewis -  Obelix 
*Geoffrey Bayldon - Getafix 
*Christopher Biggins - Lucullus  
*Henry McGee -  Caesar 
*John Rye -  Narrator
*Rupert Degas - Medicine Man (uncredited)
*Julie Gibbs - Impedimenta

==See also==
*List of animated feature films

==External links==
* 
*  at Asterix NZ

 

 
 
 
 
 
 
 