The Holy Mountain (1926 film)
{{Infobox film
| name           = The Holy Mountain
| image          = Theholymountain1926.jpg
| border         = yes
| alt            = 
| caption        = German theatrical release poster
| director       = Arnold Fanck
| producer       = Harry R. Sokal
| writer         = {{Plainlist|
* Arnold Fanck
* Hans Schneeberger
}}
| starring       = {{Plainlist|
* Leni Riefenstahl
* Luis Trenker
* Frida Richard
}}
| music          = {{Plainlist|
* Edmund Meisel
* Edmund Reisch
}}
| cinematography = {{Plainlist|
* Arnold Fanck
* Hans Schneeberger
* Sepp Allgeier
* Helmar Lerski
}}
| editing        = Arnold Fanck UFA
| distributor    = UFA
| released       =  
| runtime        = 106 minutes
| country        = Germany
| language       = Silent film, German intertitles
| budget         = 
| gross          = 
}}
 German mountain film directed by Arnold Fanck and starring Leni Riefenstahl, Luis Trenker and Frida Richard. It was the future filmmaker Riefenstahls first screen appearance as an actress. Written by Arnold Fanck and Hans Schneeberger, the film is about a dancer who meets and falls in love with an engineer at his cottage in the mountains. After she gives her scarf to one of his friends, the infatuated friend mistakenly believes that she loves him. When the engineer sees her innocently comforting his friend, he mistakenly believes she is betraying him.

==Cast==
* Leni Riefenstahl as Diotima 
* Luis Trenker as Karl 
* Frida Richard as Mother 
* Ernst Petersen as Vigo  Friedrich Schneider as Colli 
* Hannes Schneider as Mountain Guide

==Production==
The film began production in January 1925, but then was delayed due to weather and hospitalization of three actors.   The film cost 1.5 million reichsmarks to produce, and was released during the 1926 Christmas season. 

==Reception== UFA in a year which was otherwise marked by a policy of retrenchment and the departure of respected studio head Erich Pommer. The film was compared unfavourably with the much less costly Madame Wants No Children directed by Alexander Korda. 

==References==
;Notes
 
;Bibliography
 
* Hinton, David B. (2000)   Scarecrow Press ISBN 9781461635062 pg 4-6
* Hardt, Ursula. From Caligari to California: Erich Pommers life in the International Film Wars. Berghahn Books, 1996.
* Riefenstahl, Leni (1995)   NY Picador-USA ISBN 0-312-11926-7 pg 45-59.
* Fanck, Arnold (1973) Er führte Regie mit Gletschern, Stürmen und Lawinen. Ein Filmpionier erzählt München: Nymphenburger Verlagshandlung ISBN 3-485-01756-6
 

==External links==
*  
*  
*   at Silent Era

 

 
 
 
 
 
 
 
 


 