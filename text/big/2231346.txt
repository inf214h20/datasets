Monterey Pop
{{Infobox film
| name        = Monterey Pop
| image       = MontereyPopPoster.jpg
| caption     = Monterey Pop movie poster
| director    = D. A. Pennebaker John Phillips   Lou Adler
| starring    = The Mamas & the Papas Canned Heat Simon & Garfunkel Hugh Masekela Jefferson Airplane Big Brother and the Holding Company The Animals The Who Country Joe and the Fish Otis Redding The Jimi Hendrix Experience Ravi Shankar
| editing     = Nina Schulman
| distributor = Leacock Pennebaker
| released    =  
| runtime     = 79 minutes
| country     = United States English
| budget      =
| gross       =
}}
 1968 concert Albert Maysles. namesake set Wild Thing".

==Performers and songs==
Songs featured in the film, in order of appearance:
#Scott McKenzie—"San Francisco (Be Sure to Wear Flowers in Your Hair)"*
#The Mamas & The Papas—"Creeque Alley"* and "California Dreamin"
#Canned Heat—"Rollin and Tumblin"
#Simon & Garfunkel—"The 59th Street Bridge Song (Feelin Groovy)" Bajabula Bonke (The Healing Song)"
#Jefferson Airplane—"High Flyin Bird" and "Today (Jefferson Airplane song)|Today"
#Big Brother & The Holding Company—"Ball n Chain"
#Eric Burdon & The Animals—"Paint It, Black"
#The Who—"My Generation"
#Country Joe & The Fish—"Section 43"
#Otis Redding—"Shake (Sam Cooke song)|Shake" and "Ive Been Loving You Too Long" Wild Thing"
#The Mamas & The Papas—"Got a Feelin" Raga Bhimpalasi") 
 *  = Studio version, played over film footage of pre-concert activity.

The order of performances in the film was rearranged from the order of appearance at the festival. Additionally many artists who appeared at the festival were not included in the original cut of the film.

==Production== 35mm for theatrical release.    Director D. A. Pennebaker said he recorded the audio on a professional Multitrack recording|8-channel Reel-to-reel audio tape recording|reel-to-reel recorder borrowed from The Beach Boys.  The movies theatrical released used a four-channel soundtrack that included two to three minutes of rudimentary surround sound. Dolby noise reduction was added in 1978 when fresh prints of the film were struck. 

After the original production company, Leacock-Pennebaker, was dissolved in 1970, Pennebaker Associates acquired rights to the film. 

==Home video==
When Sony Video released Monterey Pop on videocassette in 1986, Pennebaker created three one-inch tape masters struck from a 16mm negative he had "wet-gated", a process in which sponges remove particles and also place a fast-drying chemical on the film that fills in scratches.  In a digital remix for the video, Pennebaker eliminated the surround track of the theatrical release and mixed the center dialog track into the left and right stereo channels. No Dolby was used, although Sonys initial video release inadvertently said otherwise on the packaging. 
  Criterion Collection box set, The Complete Monterey Pop Festival, that also includes Pennebakers short films Jimi Plays Monterey (1986) and Shake! Otis at Monterey (1986), as well as two hours of outtake performances, including some by bands not seen in the original film.  The box set was re-released in 2009 on Blu-ray.  For this edition, the soundtracks were remixed in 5.1 Surround Sound by Eddie Kramer. 

==Influence==
 ]]
 counterculture that by then seemed both blessedly inevitable and dangerously embattled." 
 One A.M. (for "One American Movie") in collaboration with Pennebaker and Leacock. Godard shot a sequence of the Airplane, (included on the 2004 "Fly Jefferson Airplane" DVD), playing at high noon on a business day on the roof of a New York hotel across the street from the Leacock-Pennebaker offices, with the tower of Rockefeller Center in the background.  Attracted by the extremely high volume of the music, the police arrived and put an end to the shooting. 

The screening of Monterey Pop in theaters nationwide helped raise the festival to mythic status, rapidly swelled the ranks of would-be festival-goers looking for the next festival, and inspired new entrepreneurs to stage more and more of them around the country. 

In 1969,  .

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 