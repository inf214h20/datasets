Al Capone (film)

{{Infobox film
| name           = Al Capone
| image          = Al Capone (1959 film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Richard Wilson
| producer       = Leonard J. Ackerman, John H. Burrows
| writer         = 
| screenplay     = Malvin Wald, Henry F. Greenberg
| story          = 
| based on       =  
| narrator       = James Gregory
| starring       = Rod Steiger
| music          = David Raksin
| cinematography = Lucien Ballard
| editing        = Walter Hannemann
| studio         =  Allied Artists
| released       =  
| runtime        = 104 minutes
| country        = 
| language       = 
| budget         = 
| gross          = $2.5 million (est. US/ Canada rentals) 
}} Richard Wilson. Allied Artists. It starred Rod Steiger as Al Capone. 

Steiger reportedly refused the producers first offer to star in this film because he thought the initial screenplay inappropriately romanticized Capone and criminality. In an interview Steiger said, "I turned the picture down three times." According to Turner Classic Movies|TCM, he agreed to play the role only after the producers agreed to rewrites.  The finished film was noted for its deglamorized portrayal of the subject. 

==Plot== Johnny Torrio. He meets Torrios top man, Jim Colosimo|"Big Jim" Colosimo, who runs business and politics in the First Ward while secretly on Torrios payroll.
 Earl "Hymie" Weiss to compete for profits in bootleg liquor and beer. Torrios rivals conspire to have Colosimo and his henchmen killed.

A reform mayor is elected, so Torrio and Capone change their base of operations to Cicero, Illinois|Cicero, a few miles away. Capone also has OBanion killed and makes a play for Maureen Flannery, the widow of one of Colosimos men.

Weiss and Moran return the favor by ordering Torrio to be shot. Capone retaliates by killing Weiss and forcing merchants throughout the city to pay for "protection." A sergeant in the Chicago police, Schaefer, is promoted to captain and vows to end the bloodshed and extortion and put Capone behind bars.

With the heat on from the cops, a crooked newspaperman named Keely tries to bribe Schaefer but fails. He persuades Capone to move to Florida until things cool down. From a safe distance, Capone masterminds the St. Valentines Day Massacre, with several of Morans men gunned down in a Chicago garage.

Capone and Moran call a truce, but when he learns Keely has been helping a Moran plot to kill him, Capone ends the reporters life instead. Maureen finally has her fill of Capones corruption and violence, while Schaefer and the feds find a way to finally put Capone away—on charges of tax evasion, earning him 11 years at Alcatraz.

==Cast==
* Rod Steiger as Al Capone
* Nehemiah Persoff as Johnny Torrio
* Fay Spain as Maureen Flannery
* Joe DeSantis as Big Jim Colosimo
* Martin Balsam as Mac Keely James Gregory as Schaefer
* Murvyn Vye as Bugs Moran
* Clegg Hoyt as Lefty (uncredited)

==Reception==
Bosley Crowther in The New York Times commented that with so many old movies about Capone, it was uncertain whether a new one was needed, but that this film had the "modest justification" that "it has a strong documentary flavor and Rod Steiger is an odious skunk in the title role."   Variety (magazine)|Variety called it "a tough, ruthless and generally unsentimental account" and "also a very well-made picture."   The film won a Laurel Award as 1959s "Sleeper of the Year". 

==References==
 

==External links==
*   TCM

 
 
 
 
 
 
 