Nancy Drew and the Hidden Staircase (film)
{{Infobox Film
| name           = Nancy Drew and the Hidden Staircase
| image_size     = 
| image	=	Nancy Drew and the Hidden Staircase FilmPoster.jpeg
| caption        =  William Clemens
| producer       = Bryan Foy Hal B. Wallis Jack L. Warner
| writer         = Mildred Wirt Benson (novel) Kenneth Garnet
| starring       = Bonita Granville Frankie Thomas John Litel
| music          = Heinz Roemheld
| cinematography = L. William OConnell
| editing        = Louis Hesse
| distributor    = Warner Bros.
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} novel of the same name by Mildred Wirt Benson.

==Plot==
The elderly Turnbull sisters want to donate their mansion for a childrens hospital. However, their fathers will states that at least one of them has to stay in the house every night for twenty years before they can inherit the estate; there are two weeks left to go. 

Then some strange things start occurring. A stranger forces his way past Nancy Drew (Bonita Granville) and brazenly searches the Drew house for related affidavits her lawyer father Carson (John Litel) has obtained. Then, the Turnbulls chauffeur Phillips (Don Rowan) dies, though it is uncertain if it was a murder or a suicide. The frightened old ladies consider leaving their home. When Nancy recognizes the dead man as the trespasser, she begins investigating, dragging her boyfriend Ted Nickerson (Frankie Thomas) into one predicament after another, eventually getting him fired and jailed. 

When police Captain Tweedy (Frank Orth) arrests the two sisters for Phillips murder, their ownership is endangered. Just in time, Nancy and Ted discover a secret passageway in the basement linking it to the neighboring house, owned by Daniel Talbert (William Gould). Talbert would make a lot of money if a racetrack were to be built on the two properties, but the Turnbulls had turned down an offer to buy their place.

==Cast==
*Bonita Granville as Nancy Drew
*Frankie Thomas as Ned Nickerson
*John Litel as Carson Drew
*Frank Orth as Captain Tweedy
*Renie Riano as Effie Schneider
*Vera Lewis as Rosemary Turnbull
*Louise Carter as Floretta Turnbull William Gould as Daniel Talbert
*George Guhl as Smitty, a bumbling policeman
*Don Rowan as Phillips

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 