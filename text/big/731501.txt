Deep Cover
 
{{Infobox film
| name           = Deep Cover
| image          = Deepcoverposter.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Bill Duke
| producer       = Henry Bean Pierre David
| writer         = Michael Tolkin Henry Bean
| narrator       = Larry Fishburne
| starring       = Larry Fishburne Jeff Goldblum
| music          = Michel Colombier
| cinematography = Bojan Bazelli
| editing        = John Carter
| distributor    = New Line Cinema
| released       =  
| runtime        = 107 minutes
| country        = United States English
| budget         =
| gross          = $16,639,799 
}} crime Thriller thriller film theme song Snoop Doggy Dogg.

==Plot==
In Cleveland, 1972, Russel Stevens Jr. is the son of a drug addicted, alcoholic man (Glynn Turman). His father tells his son never to be like him. Stevens then witnesses his father getting killed while robbing a liquor store. He swears that he will never end up the way he has.
 Cincinnati Police. Officer Stevens is recruited by DEA Special Agent Gerald Carver (Charles Martin Smith) to go undercover on a major sting operation in Los Angeles. Stevens poses as drug dealer John Hull in order to infiltrate and work his way up the network of the west coasts largest drug importer, Anton Gallegos (Arthur Mendoza) and his uncle Hector Gúzman, (René Assa) a South American politician. Stevens relocates to a cheap hotel in LA and begins dealing cocaine. His room is opposite that of crack cocaine addict Belinda Chacón (Kamala Lopez-Dawson), a struggling single mother who tries to sell her son James to Stevens for money to buy drugs.

Stevens is arrested by the devoutly religious L.A.P.D. Narcotics Detective Taft (Clarence Williams III) and his corrupt partner Hernández (Julio Oscar Mechoso) when he buys a kilogram in a set up by Gallegos low-level street supplier Eddie Dudley (Roger Guenveur Smith). At his arraignment, Stevens discovers that he was sold "baby laxative" (mannitol) instead of cocaine and his case is dismissed. Stevens self-appointed attorney David Jason (Jeff Goldblum), who is also a drug trafficker in Gallegos network, rewards Stevens with more cocaine and introduces Stevens to Felix Barbossa (Gregory Sierra), the underboss to Gallegos. Felix realized that Eddie was working with the LAPD, which results in Felix killing him and enlisting Stevens as Eddies replacement.
 launder Jasons police informant which results in him being killed by Jason.

Gallegos comes to personally meet with Jason and Stevens and informs them that they have inherited Felixs $1.8 million debt. Later that same day, Stevens meets with Carver to tell him about his meeting with Gallegos, but Carver tells him that the DEA has pulled the plug on Stevens operation. After Stevens angrily protests, Carver pulls a gun on Stevens and orders him to surrender his weapon and get in his car. Stevens disarms Carver and forces him to reveal whats happening behind the scenes. Carver admits that the State Department is leaving Gallegos and Guzman alone because Guzman may someday be useful as a political asset to them. Stevens becomes disillusioned and abandons his undercover status vowing to take down Gallegos and Guzman alone.
 diplomatic status. Guzman flees the scene before Tafts backup arrives. Taft orders Stevens to surrender, but is shot and wounded by Jason. Stevens reveals to Jason that he is a police officer but Jason ignores it and orders him to leave with him. Stevens refuses and Jason kills Taft, despite Stevens pleas to let him go. Stevens then reaffirms himself as a police officer and attempts to arrest Jason, but is forced to kill him when Jason draws his gun.
 House Judiciary Subcommittee, he produces a video tape of the incriminating conversation with Guzman at the shipyard, thus potentially ruining Guzmans career. Carver gets mad at Stevens for the videotape and punches Carver in the stomach. Later, Stevens accompanies James to his mother Belindas grave (she died of a drug overdose earlier) where he contemplates what to do with the $11 million of Gallegos money that he secretly kept.

==Cast==
*Larry Fishburne as Russell Stevens/Johnny "John" Hull
*Jeff Goldblum as David Jason
*Charles Martin Smith as DEA Agent Gerald Carver
*Victoria Dillard as Betty McCutcheon
*Gregory Sierra as Felix Barbossa
*Glynn Turman as Russell Stevens Sr.
*Clarence Williams III as Taft
*Kamala Lopez-Dawson as Belinda Chacon

==Reception==
Deep Cover was released on April 17, 1992 in 901 theaters grossing $3.4 million on its opening weekend.   It went on to make $16.6 million in North America.   
 Night and the City has left some critics lamenting that film noir is dead in the water, but Deep Cover displays many hallmarks of the genre, down to the diffuse paranoia (perhaps the entire operation is a high-level Washington cover-up). It was the most unexpected pleasure to arrive here in many a month".   

==Soundtrack== Snoop Doggy Dogg (a newcomer at the time) and Dr. Dre from 1991-1992.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 