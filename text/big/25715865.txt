High Life (film)
 
 
{{Infobox film
| name = High Life
| image = High_Life_poster_small.jpg
| producer =   Gary Yates
| writer = Lee MacDougall Jonathan Goldsmith
| starring =  
| distributor = Union Pictures
| released =  
| runtime = 80 minutes
| language = English
| country = Canada
}} Gary Yates. Joe Anderson and Rossif Sutherland, High Life is a comedic heist movie from the flip-side of the 80’s consumer dream. 

==Plot==
Set in 1983, just after the birth of the Automated Teller Machine, High Life is a story of kinship, loyalty and honour amongst thieves. In a busy downtown hospital, a visit from his former socio-pathic cellmate Bug (Stephen Eric McIntyre) has led to Dick (Timothy Olyphant) being fired from his job as a hospital janitor. Unemployed and in need of fast cash Dick gets the idea to rob one of the brand new ATMs, to "buy a little self-respect", announces Dick to Bug and the team. Enter the charismatic, criminally-minded Donnie, (Joe Anderson) and the front-man, the sexy, sleepy-eyed charmer Billy, (Rossif Sutherland) and all of the pieces are in place.  "It’s a precision job," says Dick the night before the heist: "No violence."

Things do not go according to plan and the unfolding catalogue of disasters that confronts Dick is enough to test any friend’s loyalties as they bungle their way toward a pipe-dream of quick riches. Alternately tragic and hysterical, High Life’s perfect plan ends up anything but when one of the bank’s employees double-crosses them all. Set against the nostalgic back-beat of Three Dog Night, Creedence Clearwater Revival and a raft of April Wine,  High Life’s highwire tension unfolds with calamitous results.

==Cast==
* Timothy Olyphant as Dick
* Stephen Eric McIntyre as Bug Joe Anderson as Donnie
* Rossif Sutherland as Billy

==Awards==
*Calgary International Film Festival (win) Best Canadian Feature

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 