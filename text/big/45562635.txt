Narada Vijaya
{{Infobox film|
| name = Narada Vijaya
| image = 
| caption =
| director = Siddalingaiah
| writer = M. D. Sundar
| starring = Ananth Nag  Padmapriya   M. P. Shankar
| producer = Siddalingaiah  N. Veeraswamy  S. P. Varadaraj Chandulal Jain Vaidi
| cinematography = R. Madhusudhan
| editing = P. Bhaktavatsalam
| studio = Jain Combines
| released = 1980
| runtime = 133 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada film directed and co-produced by Siddalingaiah. The story is written by M. D. Sundar.  The film starred Ananth Nag, Padmapriya and M. P. Shankar in lead roles.
 Vaidi considered evergreen hits.

==Plot==
Ananth Nag plays a dual role of Narada in the celestial world and a normal human on earth. It so happens that Narada is forced to descend on earth and interact with human. A series of humorous incidents occur.
  

== Cast ==
* Anant Nag 
* Padmapriya
* M. P. Shankar
* K. S. Ashwath 
* Loknath
* Thoogudeepa Srinivas
* Sudheer
* Hema Chowdhary
* Prathima Devi

== Soundtrack == Ashwath - Vaidi duo with lyrics by Chi. Udaya Shankar.  All the songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Idhu Entha Lokavayya
| extra1 = K. J. Yesudas
| music1 = 
| length1 = 03:26
| title2 = Ninnantha Hennu 
| extra2 = S. P. Balasubramanyam, S. Janaki
| music2 = 
| length2 = 04:37
| title3 = Ee Vesha Nodabeda Ammayya
| extra3 = K. J. Yesudas, S. Janaki
| music3 =
| length3 = 04:13
| title4 = Naa Bale Beesuve
| extra4 = S. Janaki
| music4 =
| length4 = 04:52
}}


== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 