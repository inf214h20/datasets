Tango Tangles
{{Infobox film
  | name = Tango Tangles
  | image = The Tango Tangle poster.jpg
  | imagesize=
  | caption = Theatrical poster to Tango Tangles
  | director =  Mack Sennett
  | producer = Mack Sennett
  | writer = Mack Sennett
  | starring = Charles Chaplin   Ford Sterling   Roscoe Fatty Arbuckle   Chester Conklin   Minta Durfee
  | music = Frank D. Williams
  | editing =
  | distributor = Keystone Studios
  | released = March 9, 1914
  | runtime = 12 minutes English (Original titles)
  | country        = USA
  | budget =
  | preceded by =
  | followed by =
  }}
 
  1914 United States|American-made film comedy short starring Charles Chaplin and Roscoe Arbuckle.  Chaplin appears with no moustache.  The action takes place in a dance hall, with a drunken Chaplin, Ford Sterling, and the huge, menacing, and acrobatic Arbuckle fighting over a girl. The supporting cast also features Chester Conklin and Minta Durfee.  The picture was written, directed and produced by Mack Sennett and distributed by Keystone Studios.

==Cast==
* Charles Chaplin - Tipsy Dancer
* Roscoe Arbuckle - Clarinettist
* Ford Sterling - Band leader
* Chester Conklin - Guest in Police Costume
* Minta Durfee - Guest
* Charles Avery	... Guest in Straw Hat (uncredited)
* Glen Cavender	... Drummer in band / Guest in Cone Hat (uncredited)
* Alice Davenport	... Guest with Man in Overalls (uncredited)
* Billy Gilbert	... Guest in cowboy hat (uncredited)
* William Hauber	... Flutist (uncredited)
* Bert Hunn	... Guest (uncredited)
* George Jeske	... Cornet Player / Guest with Bow Tie (uncredited)
* Edgar Kennedy	... Dance Hall Manager (uncredited)
* Sadie Lampe	... Hat Check Girl (uncredited)
* Hank Mann	... Guest in Overalls (uncredited)
* Harry McCoy	... Piano Player (uncredited)
* Rube Miller	... Guest Pushed Away (uncredited) Dave Morris	... Dance Organizer (uncredited)
* Eva Nelson	... Guest with Man in Cone Hat (uncredited)
* Frank Opperman	... Clarinetist / Guest (uncredited)
* Peggy Pearce	... Guest (uncredited)
* Al St. John	... Guest in Convict Costume (uncredited)

==See also==
* List of American films of 1914
* Charlie Chaplin filmography
* Fatty Arbuckle filmography

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 


 