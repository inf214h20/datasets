Johnny, You're Wanted
{{Infobox film
| name           = Johnny, Youre Wanted
| image          = "Johnny,_Youre_Wanted"_(1956).jpg
| image_size     =
| caption        = Original Australian daybill 
| director       = Vernon Sewell
| producer       = George Maynard
| writer         = Frank Driscoll Michael Leighton John Slater Alfred Marks Garry Marsh
| music          = Robert Sharples
| cinematography = Basil Emmott
| editing        = Peter Rolfe Johnson
| distributor    = Anglo-Amalgamated
| released       = January 1956
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
}}
 John Slater.   The film features famous strongwoman Joan Rhodes performing her stage act.

==Plot==
Johnny (Slater) is a long-distance lorry driver returning to London from a provincial delivery, after having taken in a show by Joan Rhodes on the way.  Late at night he stops to give a lift to an attractive female hitchhiker whose car has broken down and who is in a hurry to get to back to London.  Later, Johnny pulls in to a transport café to make a telephone call and buy a coffee.  When he returns to his truck, the woman is gone.  Assuming that in her hurry she has picked up a lift with another driver, he goes on his way, and a few miles down the road is flagged down by another driver to help with a woman who has been found laid at the roadside.  It turns out that the woman is Johnnys hitchhiker, and that she is dead.  

The police soon establish that Johnny was the last person to see the woman alive, and consider him the prime suspect in her murder.  Johnny goes on the run, and tries to find out as much as he can about the woman and why anyone should have wanted her dead, while trying to elude the police.  He soon finds himself caught up in the shady world of drug smuggling and has to use all his wits to bring the real killers to justice.
  
==Cast== John Slater as Johnny
* Alfred Marks as Marks
* Garry Marsh as Balsamo
* Joan Rhodes as Herself
* Chris Halward as Julie Jack Stewart as Inspector Bennett John Stuart as Surgeon
* Ann Lynn as Chorine

==Critical reception==
TV Guide called the film a "well-conceived thriller...The situations and performances are a bit forced, but otherwise interest is easily maintained." 

== External links ==
*  
* 

==References==
 

 

 
 
 
 
 
 
 
 

 
 