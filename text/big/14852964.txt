Fully Flared
 
{{Infobox film
| name           = Fully Flared
| image          = Fully Flared.jpg
| caption        = 
| image size       =
| director       = Ty Evans   Spike Jonze   Cory Weincheque
| producer       =
| writer         =
| narrator       = Mike Carroll Marc Johnson
| music          = Sam Spiegel (original)
| cinematography =
| editing        = Ty Evans
| distributor    = Lakai
| released       = November 16, 2007
| runtime        = 90 mins.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Fully Flared is a street skateboarding video by the Lakai footwear company, featuring video parts from its team riders. The film is directed by Ty Evans, Spike Jonze, and Cory Weincheque. In 2007, it won "Best Video of the Year" at the Transworld Skateboarding Awards, while Guy Mariano won both the "Best Street" and "Best Video Part" awards at the same event. It was one of the first skateboarding videos to use high-definition cameras and it was the first skateboarding video to be regulated on the internet. The film has become a skateboarding culture icon as the apex of professional skateboarding.

==Introduction feature==
The introduction features the skateboarders performing tricks in a vacant urban space, consisting of obstacles, blocks, and stair sets, while explosions occur.  Presented in slow motion, the introduction feature is accompanied by a soundtrack from electronic music group, M83 (band)|M83. 

Originally, Evans, Jonze, and Howard played with different ideas that were significantly more dangerous than what was eventually featured. The introduction was eventually filmed three weeks prior to the premiere. 

==Production==
Overall the film took about four years to make.   , Kenny, Niall, Tacky Skateboard. Retrieved on 2007-12-26. 

Some team members had a lot of leftover footage and are now planning to use it for other videos.   , Sidewalk Magazine. Retrieved on 2007-12-26.  Most of the video was filmed with Sony DCR-VX1000 cameras and Panasonic HVX200 (which Ty Evans gained access to towards the end of filming) High-definition video|high-definition footage is used for second angles.  , Skate Perception. Retrieved on 2007-12-26. 

==Filming==
The main group filming was Aaron Meza and Chris Ray. In Europe, an Italian filmer named Fedrico Vitetta - who had been living with Oliver Barton in Spain for a year - took on the role. Then was conceptual help from Rick Howard and Spike Jonze. Finally, Johannes Gamble helped with all the effects work.

==Release==
The videos release was postponed for about two years, mainly due to important team additions.  , Finch, Greg, Kingpin Skateboarding Europa. Retrieved on 2007-12-26.   , Carleton, Curtis, Transworld Skateboarding. Retrieved on 2007-12-26. 

The high budget of the film and the long production time made the company determined to not have the video be a short lived craze, causing them remove all Fully Flared videos from the internet for several months after the films first release.

==References==
 

==External links==
*  - Lakais official website
*  - Fully Flared soundtrack list

 

 
 