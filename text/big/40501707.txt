Voodoo Tiger
{{Infobox film
| name           = Voodoo Tiger
| image          = Voodoo Tiger poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Spencer G. Bennet
| producer       = Sam Katzman
| writer         = 
| screenplay     = Samuel Newman
| story          = 
| based on       =   
| narrator       = 
| starring       = Johnny Weissmuller
| music          = 
| cinematography = William Whitley
| editing        = Gene Havlick
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = November 1952
| runtime        = 68 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Voodoo Tiger (1952) is the ninth Jungle Jim film produced by Columbia Pictures. It features Johnny Weissmuller in his ninth performance as the protagonist adventurer Jungle Jim, and James Seay as the films antagonist. The film was directed by Spencer G. Bennet and written by Samuel Newman.

The film centres on Jungle Jim battling a team of art thieves in an African jungle inhabited by tiger god worshippers. The film was theatrically released in the United States in November 1952.

==Plot==
Adventurer Jungle Jim (Johnny Weissmuller) and Sergeant Bono (Rick Vallin) are taking the British Museums Phyllis Bruce (Jean Byron) on a tour around an African jungle. They come to witness a sacrificial ritual about to take place. Jungle Jim hurriedly prevents chief religious official Wombulu (Charles Horvath) from slaughtering an African native as an offering to the tiger god Tambura. The furious voodoo practitioner lunges for Jungle Jim with a knife, only to be shot by hunter Abel Peterson (James Seay). Peterson invites Jim, Bono, and Bruce to stay at his lodging. However, Jim receives a last-minute notification that Major Bill Green of the United States (Robert Bray) has come with Commissioner Kingston (Richard Kipling) to meet businessman Karl Werner (Michael Fox). The two army personnel are looking to recover a stolen collection of artworks. Werner, who operates in the jungle, is believed to possess knowledge of the artworks whereabouts. Jim is assigned to lead the way to Werner. 

Werner claims to not know anything about the looted art pieces; Green rebuts him by labelling him as a "Nazi"  who aided the looters. Just then, Green spots Peterson and his men, who happen to be passing by. The army major informs Jim that they are actually notorious thieves specialising in artworks. The two camps battle and at last it is Petersons team which prevails. Werner, Green, Kingston, and Jim are locked up in a cupboard. Werner somehow breaks loose and makes his way to the airfield. He dashes up the plane Peterson and his henchmen are planning to escape on. After overpowering the pilot, he captains the plane himself and flies back to the jungle. The plane catches fire just as it is reaching  the destination. Werner courageously jumps out, along with an exotic dancer who happens to be on board too. Her tiger also lands in the jungle. The natives, who are about to massacre Jim, see the tiger and kneel on their knees, believing it to be Tambura.

Meanwhile, Wombulu and Peterson have started an alliance. They hatch a plan to kidnap Werner. At the same time, Jungle Jim and his acquaintances have escaped. Jims eagle-eyed pet chimpanzee, Tamba, spots an obscure dynamite trap laid out for them; Jim defuses it. From afar they see the dancers tiger on a wild rampage. As more natives get killed by the tiger, Peterson grabs Werner and runs away. Jungle Jim stops Peterson in his tracks and rescues Werner from the hunters clutches. The enraged African natives run towards Peterson and his men, killing them. Jim and the rest barely manage to escape. The jungle explorer quick-wittedly resets the dynamite trap. It explodes and kills the natives. With the voodoo tribe dissolved, Major Green professes his love for Bruce.

==Production== Bring Em Back Alive (1932) for scenes involving tigers. 

==Release and reception==
The film was officially released in North American cinemas in November 1952.  The Hollywood Reporter found the film to be "  movement and suspense in spades", while Variety (magazine)|Variety opined that it "stacks up okay as a program filler".  The Columbia Story described the film as "moribund". 

==References==
 

==Bibliography==
*  }}
*  }}

==External links==
*  
*  
*  
 
 
 
 
 
 
 