The Lost Empire (1983 film)
{{Infobox film
| name           = The Lost Empire
| image size     =
| image	=	The Lost Empire FilmPoster.jpeg
| caption        =
| director       = Jim Wynorski
| producer       = Jim Wynorski
| writer         = Jim Wynorski
| starring       = Melanie Vincz Raven De La Croix Angela Aames Paul Coufos Robert Tessier Angus Scrimm Alan Howarth
| cinematography = Jacques Haitkin
| editing        = Larry Bock
| distributor    = JGM Enterprises
| released       = 1983
| runtime        = 83 mins
| country        = United States English
}}The Lost Empire is a 1983 fantasy adventure film directed by Jim Wynorski. It stars Melanie Vincz, Raven De La Croix, Angela Aames and Angus Scrimm

==Plot==
The film opens at a jewelry shop in Chinatown, which contains a statue with one glowing red eye. Three masked figures kill the store’s owner, then try to pry the eye free. The police arrive, and after a bloody fight, all of the intruders and all but one of the policemen are dead, with the lone survivor being seriously wounded.

The next day, children are being held hostage in an elementary school. A black-clad figure enters and takes on the terrorists, killing all three before revealing that she is Inspector Angel Wolfe (Melanie Vincz) of the L.A.P.D. A man enters the school room and Angel strikes him, breaking his nose and knocking him down, before realizing that the newcomer is Federal Agent Rick Stanton (Paul Coufos), an old friend. The two spend the night together.

The next morning, Angel receives a phone call telling her that her brother Rob (Bill Thornbury), is in the hospital after the jewelry shop confrontation. Angel and Rick rush to his side, and Rob gives Angel a strange star-shaped object and a cryptic message that "the Devil exists, and the Eye knows where". Rick recognises the star and tells Angel about the legend of Lee Chuck (Angus Scrimm), who gained immortality at the price of giving the devil a new soul every day.

Angel pays a visit to the crime scene. As she gazes sadly at the spot on which her brother was wounded, the glowing red eye drops, unnoticed, from the statue into her handbag. Angel is startled by the sudden appearance of a mysterious Chinese man, who turns out to be Inspector Charles Chang (Art Hern). Chang tells Angel and Rick about the Eyes of Avatar, into which the Dragon-God placed enough power to allow anyone possessing them both to rule the world; and about his belief that Lee Chuck is both real, and in possession of one of the Eyes. He further tells them that Dr Sin Do (Angus Scrimm, in a dual role), the leader of a religious cult, is somehow involved with Lee Chuck.

After learning that Rob has died from his injuries, a grief-stricken Angel forces Rick to tell her more about Sin Do, who is recruiting women for an army of terrorists, luring them to his island by promising them fabulous wealth. When she hears that Sin Do only accepts women in trios, Angel travels to an Indian reservation to see Whitestar (Raven De La Croix), an old friend, and asks her to join the mission, to which Whitestar agrees. The third recruit is Heather McClure (Angela Aames), a criminal who Angel promises a parole in exchange for her help. The three sign on, and are flown by plane to Sin Dos mysterious island fortress of Golgatha.

==Cast==
Melanie Vincz as Angel Wolfe
 Raven De La Croix as Whitestar
 Angela Aames as Heather McClure
 Paul Coufos	as Rick Stanton
 Robert Tessier as Koro
 Angus Scrimm as Dr. Sin Do/Lee Chuck
 Blackie Dammett as Prager
 Linda Shayne as Cindy Blake
 Kenneth Tobey as Capt. Hendry
 Tom Rettig as Officer Robinson
 Angelique Pettyjohn as Whiplash
 Art Hern as Charles Chang
 Anne Gaybis as Prison Referee
 Bill Thornbury as Rob Wolfe

==Production==

===Production Crew===
*Director … Jim Wynorski
*Producer … Jim Wynorski
*Original story and screenplay … Jim Wynorski
*Director of Photography
*Original music … Alan Howarth
*Film Editor … Larry Bock

==Reception==
-->

==Home video==
The film was released on VHS and Laserdisc by Lightning Video in 1984. Some thirty years later, after being out of print for more than two decades, it was released on DVD in Region 1 by Polyscope Media in 2014, with Pegasus releasing the film in Region 2. Bonus features included an isolated soundtrack, still photo gallery, and commentary track by director Jim Wynorski.

==Notes and references==
 

==External links==
*  
*   at Bad Movie Planet

 
 
 
 
 
 
 
 