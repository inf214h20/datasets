A Question of Taste
{{Infobox film
| name           = A Question of Taste
| image          = A Question of Taste Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = United States DVD Cover
| director       = Bernard Rapp
| producer       = 
| writer         = Bernard Rapp and Gilles Taurand
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       =  Bernard Giraudeau
* Florence Thomassin
 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 French film directed by Bernard Rapp. Rapp and Gilles Taurand wrote the screenplay which was based on the book "Affaires de goût" by Philippe Balland. The film received 5 César Award nominations, including the nomination for Best Film.

==Awards and nominations==
*César Awards (France) Best Actor &ndash; Leading Role (Bernard Giraudeau) Best Actress &ndash; Supporting Role (Florence Thomassin) Best Film Best Writing (Bernard Rapp and Gilles Taurand) Most Promising Actor (Jean-Pierre Lorit)
 Karlovy Vary Film Festival (Czech Republic)
**Won: Special Mention (Bernard Rapp)
**Nominated: Crystal Globe (Bernard Rapp)

==External links==
*  

 
 
 
 
 

 