Vandicholai Chinraasu
{{Infobox Film
| name           = Vandicholai Chinraasu
| image          = Album_vandicholaichinraasu_cover.jpg
| image_size     = 
| caption        =  Manoj Kumar
| producer       = Thirupur Mani
| writer         = Manoj Kumar Sivaram Gandhi (dialogues)
| narrator       =  Sukanya Goundamani
| music          = A. R. Rahman
| cinematography = Lakshmi Balan
| editing        = M. N. Raja
| studio         = Vivekananda Films
| distributor    = Vivekananda Films
| released       = 15 April 1994  
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Manoj Kumar. This remains the only Sathyaraj movie as hero where A. R. Rahman composed the music. It was released on Tamil New Year Day. The film received mixed reviews from the box office.

==Cast==
*Sathyaraj
*Sukanya
*Sivaranjani (actress)
*Goundamani

==Story==
The title of the story is the name of the hero Sathyaraj (Chinnrasu) and Vandicholai is the name of the town where he lives with his father (Delhi Ganesh) and sister. Delhi Ganesh works in the forest office as a driver. Chinnrasu has an aim to become a district forest officer. Due to some problem Delhi Ganesh gets a transfer order and the family shifts in Vandicholai town. The first plot of the movie Showcases the current situation of Chinnrasu. The movie starts from a scene where Chinnrasu is in the jail and will be hanged till death in a couple of month. Hearing this Sathyaraj remembers his past life and how he gets such a punishment.

==Critical Reception==
The film received mixed reviews from the critics and had a below average run at the box office. The soundtrack however was praised and fetched good reviews. The song - Barota became very popular in all the military hotels.

==Soundtrack==
{{Infobox album  
| Name        = Vandicholai Chinraasu
| Type        = film
| Artist      = A. R. Rahman
| Cover       =  1994
| Recorded    = Panchathan Record Inn
| Genre       = Film soundtrack
| Length      =  AVM Audio
| Producer    = 
| Reviews     = 
| Last album  = Thiruda Thiruda  (1993)
| This album  = Vandicholai Chinraasu (1994)
| Next album  = Super Police (1994)
}}

{{tracklist
| headline = Tamil Track listing
| extra_column = Singer(s)
| all_music = A. R. Rahman
| all_lyrics = Vairamuthu

| title1 = Senthamizh Naatu Thamizhachiye
| extra1 = Shahul Hameed
| length1 = 4:17

| title2 = Chitthirai Nilavu
| extra2 = Jayachandran, Minmini
| length2 = 4:51

| title3 = Kaatu Panamaram Pola
| extra3 = Swarnalatha & Malgudi Subha
| length3 = 5:01

| title4 = Idhu Sugam
| extra4 = Vani Jayaram & S. P. Balasubrahmanyam
| length4 = 4:31

| title5 = Barota Barota
| extra5 = S. Janaki & S. P. Balasubrahmanyam
| length5 = 4:34
| note5 = Lyrics: N. A. Kamarasan

}}

 Telugu (Bobili Paparayudu) 
 Telugu as Bobili Paparayudu. Lyrics were by Vennelekanti.

{{tracklist
| headline = Telugu Track listing
| extra_column = Singer(s)
| all_music = A. R. Rahman
| all_lyrics =

| title1 = Achu Telugu
| extra1 = S. P. Balasubrahmanyam
| length1 = 4:17

| title2 = Sukham Sukham Sujata
| length2 = 4:51

| title3 = Oh Mudhu Mama
| extra3 = Swarnalatha, Malgudi Subha
| length3 = 5:01

| title4 = Tharaka Cheluvu
| extra4 = Swarnalatha, S. P. Balasubrahmanyam
| length4 = 4:31

| title5 = Singaraaya Konda
| extra5 = S. P. Balasubrahmanyam, Malgudi Subha
| length5 = 4:34

}}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 