Playground (film)
{{Infobox film
| name = Playground
| image = Yashimoto-nara.jpg
| director = Libby Spears
| producer =  
| music =  
| editing = Oreet Rees
| released =  
| runtime = 78 minutes
| country = United States
| language = English
}}
Playground is 2009 documentary directed by Libby Spears. The film focuses on the child sex trade in the United States. 

Playground challenges the notion that the sexual exploitation of children is limited to back-alley brothels in developing countries and traces the epidemic of exploitation to its disparate, and decidedly American, roots — among them the way children are educated about sex, and the problem of raising awareness about a crime that inherently cannot be shown. 

The film includes interviews with Ernie Allen, President of the National Center for Missing and Exploited Children and Judge Sanford "Sammy" Jones, Former Chief Judge of the Fulton County Juvenile Court and unfolds as a search for Michelle, an everyday American girl who was lost to the underbelly of sexual exploitation as a child and has yet to resurface a decade later.

Playground features original artwork by Japanese pop artist, Yoshitomo Nara, and animation by Heather Bursch.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 

 