Midida Shruthi
 

{{Infobox film|
| name = Midida Shruthi
| image = 
| caption =
| director = M. S. Rajashekar
| writer = Sai Suthe
| starring = Shivrajkumar  Sudharani  Srinath   Vinaya Prasad
| producer = S. A. Govindaraj
| music = Upendra Kumar
| cinematography = V. K. Kannan
| editing = S. Manohar
| studio = Nirupama Art Combines
| released =  
| runtime = 150 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada drama romance drama film directed by M. S. Rajashekar and produced by S. A. Govindaraj. The film features Shivarajkumar, Sudharani, Srinath and Vinaya Prasad in the lead roles.  The films plot is based on the novel written by Sai Suthe.

== Cast ==
* Shivarajkumar 
* Sudharani
* Vinaya Prasad
* Srinath
* Devaraj
* Sundar Krishna Urs
* Shimoga Venkatesh
* Sundarashri
* Hema Choudhary

== Soundtrack ==
The soundtrack of the film was composed by Upendra Kumar. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Aa Surya Chandra
| extra1 = S. P. Balasubrahmanyam& Manjula Gururaj
| lyrics1 = M. N. Vyasa Rao
| length1 = 
| title2 = Nanna Ninna Aase
| extra2 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics2 = Geethapriya
| length2 = 
| title3 = Bannada Okuli
| extra3 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics3 = Sri Ranga
| length3 = 
| title4 = Nenedodane America
| extra4 = S. P. Balasubrahmanyam 
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Yaavudu Preethi
| extra5 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics5 = M. N. Vyasa Rao
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 