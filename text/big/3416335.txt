Death Takes a Holiday
 
{{Infobox film name           =Death Takes a Holiday image          =DeathTakesAHolidayposter.jpg image_size     = caption        =Theatrical release poster director       =Mitchell Leisen producer       =E. Lloyd Sheldon Emanuel Cohen screenplay     =Maxwell Anderson Gladys Lehman based on       = Death Takes a Holiday (play) by Alberto Casella starring       =Fredric March Evelyn Venable Guy Standing Katharine Alexander Gail Patrick Kent Taylor Helen Westley Henry Travers Kathleen Howard cinematography =Charles Lang editing        =  distributor    =Paramount Pictures released       =  runtime        =79 min. country        =United States language  English
}}
Death Takes a Holiday is a 1934 romantic drama starring Fredric March, Evelyn Venable and Guy Standing, based on the Italian play La Morte in Vacanza by Alberto Casella.

==Synopsis== Death takes on human form (March) for three days so that he can mingle among the mortals and find an answer. He finds a host in Duke Lambert (Standing) after revealing himself and his intentions to the Duke, and takes up temporary residence in the Dukes villa. However, events soon spiral out of control as he falls in love with the beautiful young Grazia (Venable), the only woman unafraid of him. As he falls in love with her, Duke Lambert, who is also the father of Grazias mortal lover Corrado (Taylor), begs him to give Grazia up and leave her among the living. Death must decide whether to seek his own happiness, or sacrifice it so that Grazia may live.

==Releases==
The theatrical release of the film was on March 30, 1934. The home video releases have been:
* 
*  (As part of the Meet Joe Black Ultimate Edition.)
* 

==Reception==
The film was an enormous critical and commercial success.  Time Magazine|Time called it "thoughtful and delicately morbid", while Mordaunt Hall for the New York Times wrote that "it is an impressive picture, each scene of which calls for close attention".  Richard Watts, Jr, for the New York Herald Tribune, described Marchs performance as one of the films "chief virtues".

==Remakes and adaptations==
It aired as the drama of the week on Cecil B. DeMilles Lux Radio Theatre on March 22, 1937, starring Fredric March as Death and his wife, actress Florence Eldridge, as Grazia.
 owners MCA Inc.|MCA, made a 1971 television production featuring Yvette Mimieux, Monte Markham, Myrna Loy, Melvyn Douglas and Bert Convy. Loy related in her biography that the production was marred by a decline in filming production standards; she described a frustrated Douglas storming off the set and returning to his home in New York when a tour guide interrupted the filming of one of his dramatic scenes to point out Rock Hudsons dressing room. 
 1998 as Meet Joe Black starring Brad Pitt, Claire Forlani and Anthony Hopkins.
 Thomas Meehan. It began previews Off-Broadway on June 10, and officially opened on July 21, 2011, in a limited engagement through September 4, in the Laura Pels Theatre at the Harold & Miriam Steinberg Center for Theatre in a production by Roundabout Theatre Company. 

==Further reading==
*Loy, Myrna and Kotsilibis-Davies, James — Being and Becoming, Alfred A. Kopf, Inc. 1987, ISBN 1-55611-101-0
*Lawrence J. Quirk|Quirk, Lawrence J. — The Films of Fredric March, The Citadel Press, 1971, ISBN 0-8065-0413-7

==References==
 

==External links==
*  
* 
* 
*  on Lux Radio Theater: March 22, 1937

 

 
 
 
 
 
 
 
 
 
 
 