Chhatrapati (film)
 
 

{{Infobox film
| name  = Chhatrapati
| image = Chatrapathi.jpg
| writer = K. V. Vijayendra Prasad Pradeep Rawat
| director = S. S. Rajamouli
| producer = B. V. S. N. Prasad
| distributor =Sri Venkateswara Cine Chitra Dil Raju
| editing = Kotagiri Venkateswara Rao
| studio = Sri Venkateswara Cine Chitra
| cinematography = K. K. Senthil Kumar
| released =  
| runtime = 165 minutes
| music = M. M. Keeravani
| country = India
| language = Telugu
| budget =   (citation needed)  
| gross =  Gross 
}} Action drama Telugu film Pradeep Rawat appear in other roles. The film released on 29 September 2005, it was dubbed into Hindi as Hukumat Ki Jung. The film was remade in Kannada with the name Chatrapathi (2012 film)|Chhatrapati with Siddhanth and Priyadarshini in lead roles and in Bengali as Refugee (2006 film)|Refugee.

==Plot==
Shivaji (Prabhas) and Ashok (Shafi) are the sons of Parvati (Bhanupriya). Shivaji is her stepson, but Parvati showers equal affection on both of them and this is not liked by Ashok, who is her biological son. They are one of the families living on the coast of Sri Lanka. One day, this villagere are forced to evacuate and Shivaji is separated from the family. He ends up in a different boat and lands in Vizag port. He is brought up in the port itself. But his search for his mother never ceases. In that process he comes across Neelu (Shriya Saran|Shriya).
 Pradeep Rawat) comes down to Vizag in the hunt for Shivaji since he killed his brother Baji Rao. Ashok, too, lands in the same place. Realizing that Shivaji is his brother, he joins hands with the bad guys. The story is then of Chhatrapati who is searching for his mother and has a bunch of bad guys including his brother on his back while the entire port looks up to him. The plot is loosely based on Scarface (1983 film)|Scarface.

==Cast==
  was selected as lead heroine marking her first collaboration with Prabhas.]]
* Prabhas ... Chhatrapati
* Shriya Saran ... Neelu
* Bhanupriya ... Shivajis mother
* Shafi ... Ashok/Akash Pradeep Rawat ... Raj Bihari
* Kota Srinivasa Rao ... Appala Nayudu Ajay ... Ajay
* Kamal Kamaraju ... Shivajis friend
* Sekhar as Shivajis friend
* L.B. Sriram
* Narendra Jha ... Baji Rao
* Supreet .... Katraju Venu Madhav ... Mahesh Nanda
* Jaya Prakash Reddy ... Commissioner
* Subbaraya Sharma ... Commissioners assistant
* Srinivas Reddy ... Appala Nayudus assistant Surya ... Syed Jaffar Khan
* Raghava ... Dr. Vihari
* Karate Kalyani
* Y. Vijaya
* Aarthi Agarwal ... in item number
* Mumaith Khan ... in item number

==Music==
The film has seven songs composed by M. M. Keeravani. The track "Gundusoodi" has been reused from the song "Kambangaadu" from the 1992 tamil movie Vaaname Ellai also composed by M. M. Keeravani.
* "A Vachhi B Pai Valli" - M. M. Keeravani, Mathangi
* "Agni Skalani" - M. M. Keeravani, Mathangi, Mangari
* "Summama Suruyaaa" - Kalyani Malik, Sunitha Upadrashta|Sunitha, Smitha, Niraj Pandit
* "Nallnivanee" - K. S. Chithra|K. S. Chitra
* "Manela Tintivira" - Tippu (singer)|Tippu, Smitha, Kalyani
* "Gundusoodi" - M. M. Keeravani, Sunitha
* "Gala Galagala" - K. S. Chitra, Neerippal, Jassie Gift

==Box office==
The film had a 100-day run in 54 centers. The film grossed   in 100 days and became the highest grossing Telugu film of 2005.  

==Awards==
* Nandi Award for Best Supporting Actress - Bhanupriya
* Nandi Award for Best Music Director - M. M. Keeravani 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 