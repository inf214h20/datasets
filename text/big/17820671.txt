Tarzan's Deadly Silence
{{Infobox Film |
name      = Tarzans Deadly Silence |
image= Tarzans Deadly Silence (movie poster).jpg| John Considine Tim Considine |
based on =   |
starring    = Ron Ely Jock Mahoney Woody Strode Manuel Padilla, Jr. |
director    = Robert L. Friend |
producer    = Sy Weintraub Leon Benson |
music     = Walter Greene|
distributor  = National General Pictures |
released    =   |
runtime    = 88 mins. |
language    = English |
}}    
 John Considine and Tim Considine (based on the character created by Edgar Rice Burroughs) and directed by Robert L. Friend.

==Synopsis==
 loses his hearing after a bomb blast, and is hunted through the jungle by the ruthless Colonel.

==Selected Cast==

*Ron Ely as Tarzan
*Jock Mahoney as The Colonel, a villain
*Woody Strode as Marshak
*Manuel Padilla, Jr. as Jai, Tarzans youthful ward
*Nichelle Nichols as Ruana

==Production notes==

The film consists of The Deadly Silence, a two-part episode of Elys Tarzan (NBC series)|Tarzan series.

*The Deadly Silence, Part I, aired on October 28, 1966.  It was written by Lee Erwin and Jack H. Robinson, and directed by Robert L. Friend.

*The Deadly Silence, Part II, aired on November 4, 1966.  It was written by John Considine and Tim Considine, and directed by Lawrence Dobkin, who was not credited in the theatrical release of the film.

Jock Mahoneys first appearance in Tarzan films was as Coy Banton, a villain opposite Gordon Scotts Tarzan in the 1960film Tarzan the Magnificent.  He took over the role of the Ape Man in 1962s Tarzan Goes to India.  This was followed by his final turn as Tarzan in Tarzans Three Challenges (1963).

Woody Strode portrayed Ramo in Tarzans Fight for Life (1958 in film|1958), and Khan in Tarzans Three Challenges (1963).

==External links==
*  

 

 
 
 
 
 
 