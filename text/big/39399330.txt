The Harry Hill Movie
 
{{Infobox film
| name           = The Harry Hill Movie
| image          = The Harry Hill Movie poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = UK release poster
| director       = Steve Bendelack
| producer       = Robert Jones
| writer         = Harry Hill Jon Foster James Lamont
| story          = Harry Hill
| starring       = Harry Hill Julie Walters Matt Lucas Simon Bird Johnny Vegas
| cinematography = Baz Irvine Michael Parker
| casting        = Karen Lindsay-Stewart  
| studio         = 
| distributor    = Entertainment Film Distributors
| released       =  
| runtime        = 88 minutes  
| country        = United Kingdom
| language       = English
| budget         = £2 million 
| gross          = £2,548,314 
}} musical comedy film directed by Steve Bendelack and starring Harry Hill. It was written by Hill along with Jon Foster and James Lamont. It revolves around a fictional version of Harry Hills adventures with his diesel-drinking nan and misdiagnosed hamster. The film was released on 20 December 2013 in the United Kingdom and Republic of Ireland|Ireland. 

==Plot==
The movie begins with an electric scooter chase between Harry and his nan because she didnt know it was him. Afterwards, Harry is sent to get a Fried chicken | chicken for lunch, but they fire a machine gun at him and throw a grenade, which Harry throws into the chicken shed, blowing them up. They then discover that their beloved pet hamster Abu is ill so they take him to the vet. He is almost put down until Harry takes him back home. Ed the vet and his assistant, Kisko, are working for Harrys neo-Nazi twin brother Otto who was abandoned by his nan in the 1970s, claiming it was because she couldnt look after them both, and raised by dogs. After another failed attempt to capture Abu (by disguising as a priest and a nun), Harry and his nan decide to take him on a trip in their Rover P6 to Blackpool for a week before he dies. Ed and his assistant pursue them on the road, until they arrive in "Blackpole" by mistake.

The next day, Harry and his nan take Abu on a personal guided tour around the nuclear power plant by the cleaner. Ed and Kisko attempt to capture him again only for him to end up turned into a destructive giant caused by radiation which wears off shortly. While walking on the beach they encounter Barney Cull, a member of the Shell People. He asks Harry and the others to save his peoples children from a gift shop. They succeed and they are invited back to the Shell Peoples cave where Harry falls for the Shell Kings daughter, Michelle. He leaves after being unable to cope living under water. They continue their road trip only to end up in a boxing match where Harry has to fight Kisko to keep Abu. He successfully wins with a free stick of rock.

Later on, the car runs out of petrol in the middle of the woods and Harry and his nan leave Abu behind while they search for a petrol station. Hes almost kidnapped again by Ed and Kisko only to leave the car in pieces.

Meanwhile, Otto teams them up with a master of disguise fox. Harry, his Nan and Abu hitch a ride with Justin Bieber and Selena Gomez (actually Ed and Kisko in disguise) and they arrive in Blackpool to see a show.

After they finally arrive there, Harry is reunited with Michelle (much to his Nans dismay) and Abu is finally kidnapped and replaced with the fox. Harry later finds out and they go looking for Abu. They follow a trail of steak barbecue hulahoops (which is what Otto was left with to eat when he was abandoned) to his hideout where he reveals his plan to turn Abu into a model figurine for his collection as an act of revenge for being deserted. During a fight between the two, Harrys nan reveals that she got rid of Otto because she kept getting him and Harry mixed up. After being chased away by killer brains, Harry and his nan are saved by the Shell People, to which his Nan accepts his and Michelles love. Harry peruses his brother to the top of the Blackpool tower. His Nan rescues him and Abu in a helicopter when Otto falls to the ground after taking a punch from Kisko after he and Ed thought they were working for the wrong brother. After defeating Otto, Abu coughs up a green felt tip pen which turned out to be the cause of his illness. Ed explains that hamsters like sucking on pens and he gave it to Abu so he would be sick and start this whole plan. Abu lives and the movie ends with a big end song with everyone in the film. Just before the final credits, a hamster appears on screen, riding on a model train. He gets off at the model station when the train stops. The hamster can only be thought to belong to Harry Hill, but there is no evidence for this.

==Critical reception==
Critical reception for the movie were mixed to negative. Ryan Gilbey wrote a review on The Guardian website saying "Whats missing is any persuasive comic force or vision to justify the films place in cinemas rather than in petrol station bargain bins".  The Independent were also critical saying its screenplay wasnt "so much offbeat as utterly feeble" and "it is very hard to keep patience with a story which hinges on the health of a toy hamster".  However Graham Young for the Birmingham Mail, on a more positive note said, "Uniquely British and deliriously silly, Harry Hill offers up the laughs".  It received a 33% rating on the review aggregator Rotten Tomatoes.  Tom Huddleston from Time Out gave it three stars saying " this has bigger laughs" and "the set-piece gags are memorable" however admitted "There’s not enough here to sustain 88 minutes, too many of the jokes fall flat" but also said "There will be those who find ‘The Harry Hill Movie’ about as amusing as a trip to the dentist. They’re wrong." 

==Filming==
Set in Blackpool and London, the Blackpool scenes in the film were shot in the town itself. Greatstone-on-Sea, Kent doubled as "Blackpole".   The fuel station scene was shot at Millbrook Proving Ground.

==Cast==
* Harry Hill as a fictional version of himself.
* Julie Walters as Harrys Nan.
* Matt Lucas as Otto Hill, Harrys neo-nazi twin brother who got separated from Harry when they were younger and was raised by German Shepherd|Alsatians.
* Simon Bird as Ed, a vet who works for Otto. 
* Sheridan Smith as Michelle, the daughter of the Shell King
* Johnny Vegas as Abu, Harrys pet hamster. 
* Julian Barratt as Conch, the King of the Shell People.
* Marc Wootton as Barney Cull, a member of the Shell People. 
* Jim Broadbent as Bill the Cleaner.
* Guillaume Delaunay as Kisko, Eds partner in crime.
* The Magic Numbers as themselves.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 