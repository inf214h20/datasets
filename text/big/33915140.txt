Prince Jack
{{Infobox film
| name           = Prince Jack
| image          = Prince Jack 1985 VHS.jpg
| image_size     = 150
| caption        = 1985 VHS Home Video box image
| director       = Bert Lovitt
| writer         = Bert Lovitt
| narrator       = 
| starring       = Robert J. Hogan James F. Kelly Lloyd Nolan Kenneth Mars
| music          = Elmer Bernstein
| cinematography = Hiro Narita
| editing        = 
| studio         = 
| distributor    = Castle Hill Productions
| released       = December 1985
| runtime        = 100 min 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 Attorney General civil rights. Although primarily a dramatic narrative, Prince Jack also uses satire and black humor, especially with regard to the Kennedy brothers complicated relationship with Lyndon B. Johnson.
 Democratic National Convention in July 1960 to the autumn of 1963, just prior to the assassination of John F. Kennedy.

The film was written and directed by Bert Lovitt.

==Cast==
* Robert J. Hogan - John F. Kennedy
* James F. Kelly - Robert F. Kennedy
* Lloyd Nolan - Joseph P. Kennedy, Sr.
* Kenneth Mars - Lyndon B. Johnson
* Dana Andrews - The Cardinal
* Robert Guillaume - Martin Luther King, Jr. Cameron Mitchell - Edwin Walker Ted Dealey William Windom - Ferguson ("Fergie")
* Theodore Bikel - Georgi Bolshakov

==Notes==
* Jim Backus last live project.
* James F. Kelly portrayed Robert F. Kennedy a total of seven times in different productions between 1981 and 1997. He also portrayed John F. Kennedy once. 
* Prince Jack was available on VHS but does not appear to have been released in DVD format.

==See also==
*Kennedy (TV miniseries)|Kennedy (TV miniseries)
*Hoover vs. The Kennedys
*The Kennedys (TV miniseries)|The Kennedys (TV miniseries)
*Cultural depictions of John F. Kennedy

==External links==
* 

 
 

 
 
 
 
 
 