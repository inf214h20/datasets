Fleshtone
{{Infobox film
| name           = Fleshtone
| image          = Fleshtone.jpg
| image_size     = 
| alt            = 
| caption        = UK DVD Cover
| director       = Harry Hurwitz
| producer       = David Charles Sheldon
| writer         = Screenplay: Harry Hurwitz Idea: Harry Hurwitz Mark Stock
| narrator       =  Martin Kemp Tim Thomerson
| music          = Zane Cronje
| cinematography = Victor Petrashevic
| editing        = Emily Paine
| studio         = 
| distributor    =  
| released       = 
| runtime        = 91 minutes
| country        =   English
| budget         = 
| gross          =  
| preceded_by    = 
| followed_by    = 
}}

Fleshtone is a 1994 film written and directed by Harry Hurwitz. 

==Plot==
A painter plays erotic games over the telephone with a woman.  Her body is found mutilated but it may not be hers after all.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Martin Kemp || Matthew Greco
|-
| Tim Thomerson|| Buddy Fields
|-
| Lise Cutter || Jennifer Womak
|-
| Graham Armitage || Dr. Sydney Frye
|-
| Suanne Braun || Wendy Pollin
|}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 