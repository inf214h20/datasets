Ilanjippookkal
{{Infobox film
| name           = Ilanjippookkal
| image          =
| caption        =
| director       = Sandhya Mohan
| producer       =
| writer         = Ignetius Kalayanthani
| screenplay     = Ignetius Kalayanthani Innocent Mukesh Mukesh Rajalakshmi Ratheesh
| music          = Kannur Rajan
| cinematography = Saroj Padi
| editing        = VP Krishnan
| studio         = Pushpa & Pushpa
| distributor    = Pushpa & Pushpa
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by Sandhya Mohan. The film stars Innocent (actor)|Innocent, Mukesh (actor)|Mukesh, Rajalakshmi and Ratheesh in lead roles. The film had musical score by Kannur Rajan.   

==Cast==
  Innocent
*Mukesh Mukesh
*Rajalakshmi
*Ratheesh Shankar
*Lissy Lissy
*Achankunju
*Master Vimal
*Santhakumari Shivaji
*Soorya
*Thodupuzha Vasanthi
*Sandhya
 

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Madhu Alappuzha. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Udayagiriyirangivarum || KS Chithra || Madhu Alappuzha || 
|-
| 2 || Vishuppakshi chilachu || K. J. Yesudas || Madhu Alappuzha || 
|}

==References==
 

==External links==
*  

 
 
 

 