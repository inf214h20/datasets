Adventures of Zatoichi
{{Infobox film
| name = Adventures of Zatoichi
| image =
| director = Kimiyoshi Yasuda
| producer = Shozaburo Asai
| writer = Shozaburo Asai Kan Shimozawa (story)
| starring = Shintaro Katsu Eiko Taki Miwa Takada
| music = Taichirô Kosugi
| cinematography = Shôzô Honda
| released =  
| runtime = 86 minutes
| country = Japan
| language = Japanese
}}
{{Nihongo|Adventures of Zatoichi| Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Adventures of Zatoichi is the ninth episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
  New Years festival, Zatoichi is asked by a man to deliver a message to a woman. Upon delivery, he rooms with a young women who is searching for her missing father.

==Cast==
* Shintaro Katsu as Zatoichi
* Eiko Taki as Osen
* Miwa Takada as Saki
* Mikijiro Hira as Gounosuke
* Kichijiro Ueda as Boss Jinbei
* Akitake Kono as Gorota Kajima
* Koichi Mizuhara as Kamazo
* Ikuko Mori as Hanokama 

==Reception==
===Critical response===
Adventures of Zatoichi currently has three positive reviews, and no negative reviews at Rotten Tomatoes. 

==References==
 

==External links==
*  
*  review by Brian McKay for eFilmCritic.com (20 April 2004)
* , review by J. Doyle Wallis for Home Vision Entertainment (19 August 2003)
* " by Thomas Raven for freakengine (27 November 2011)

 

 
 
 
 
 
 
 
 

 