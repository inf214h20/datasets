Time, the Comedian
{{Infobox film
| name           = Time, the Comedian
| image          =
| caption        =
| director       = Robert Z. Leonard
| producer       =
| writer         = Fanny Hatton and Frederic Hatton
| narrator       =
| starring       = Mae Busch Lew Cody
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 50 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 1925 film directed by Robert Z. Leonard. The film stars Mae Busch and Lew Cody. The film was a hit. 

==Plot==
Singer Nora (Mae Busch) left her husband for new flame Larry (Lew Cody); her husbands suicide cools the affair, and the pair meets again when, years later, Larry meets and falls in love with Noras daughter.

==Cast==
* Mae Busch - Nora Dakon 
* Lew Cody - Larry Brundage 
* Gertrude Olmstead - Ruth Dakon 
* Rae Ethelyn - Ruth Dakon, as a child 
* Roy Stewart - Michael Lawler 
* Paulette Duval - Mrs. St. Germaine 
* Creighton Hale - Tom Cautley 
* Nellie Parker Spaulding - Aunt Abbey 
* Robert Ober - Anthony Dakon 
* David Mir - Count de Brissac 
* Templar Saxe - Prince Strotoff 
* Mildred Vincent - Swedish Maid

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 