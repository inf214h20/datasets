Kannur Deluxe
{{Infobox film
| name           = Kannoor Deluxe
| image          =
| caption        =
| director       = AB Raj
| producer       = TE Vasudevan
| writer         = V  Devan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Sheela Adoor Bhasi Ammini
| music          = V. Dakshinamoorthy
| cinematography = TN Krishnankutty Nair
| editing        = TR Sreenivasalu
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed by AB Raj and produced by TE Vasudevan. The film stars Prem Nazir, Sheela, Adoor Bhasi and Ammini in lead roles. The film had musical score by V. Dakshinamoorthy.    The majority of the film was shot inside the KSRTC bus Kannur Deluxe. The film was well received.

==Cast==
 
*Prem Nazir as Thirumeni /CID Officer
*Adoor Bhasi as Chanthu,Thirumenis Assistant/Assi.CID Officer
*K. P. Ummer as Venu,K.B.Pillais Son
*Sankaradi as Kammath,a Passenger
*Sheela as Lady in the Bus /Central CID Officer
*N. Govindan Kutty as KGS Nair
*Jose Prakash as Gopalakrishnan,the thief
*T. R. Omana 
*Nellikkodu Bhaskaran as Bus Conductor
*G. K. Pillai (actor)|G. K. Pillai as K.B.Pillai
* P. Sreekumar
*Ammini
*Abbas
*Kottayam Chellappan
*Paappi
*K Radhakrishnan
*M Radhakrishnan
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Muhabathenthoru || K. J. Yesudas, S Janaki, PB Sreenivas || Sreekumaran Thampi || 
|-
| 2 || Ethra Chirichaalum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Kannundaayathu Ninne || P. Leela, PB Sreenivas || Sreekumaran Thampi || 
|-
| 4 || Marakkaan Kazhiyumo || Kamukara || Sreekumaran Thampi || 
|-
| 5 || Thaippooyakkaavadiyaattam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 6 || Thulliyodum Pullimaane || P Jayachandran || Sreekumaran Thampi || 
|-
| 7 || Varumallo Ravil || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 