Road to the Stars
{{Infobox film
| name = Road to the Stars
| producer   = Pavel Klushantsev|P. Klushantsev
| director   = 
| editor     =  Boris Liapunov   Vasily Solovyov
| music      = S. Shatiryan
| cinematography = 
| studio = Lennauchfilm
| runtime = 50 min.
| country = Soviet Union
| released =  
| language = Russian
}}

Road to the Stars ( ) is a 1957 Soviet film directed by Pavel Klushantsev. It combines elements of science education films and speculative science fiction. The film was groundbreaking for its use of special effects to depict life in space.

== Synopsis ==

The first half of the film is historical and educational in nature, depicting mostly the life and scientific contributions of Konstantin Tsiolkovsky, along with the basic principles of rocket propulsion, ballistics, and Space Flight. It also depicts the contributions of Max Valier and Robert Goddard.

The second half of the film is speculative in nature, with various scenes showing manned space flight (4 years before the flight of Yuri Gagarin), a large space station (in great detail), and the first man on the moon (12 years before the flight of Apollo 11), as well as lunar colonization. Guschev S.   Техника-молодежи №3-1958 с.24-25 

== Cast  ==
* Giorgy Solovyov — Konstantin Tsiolkovsky

== Crew == Boris Liapunov, Vasily Solovyov
* Director — Pavel Klushantsev Mikael Galper
* Composer — S. Shatiryan
* Artist — M. Tsybasov
* Sound Engineer — RP Leviti
* Operators animation — A.V. Lavrentiev and A. M. Romanenko
* Animation artist — V. Shelkov

== Art features ==
The film was far ahead of its time in terms of cinematic  .

== Awards ==
* 2nd prize at CCF I (Moscow) E.V. Kharitonov, A.V. Shcherbak-Zhukov    "On the screen - Miracle: Patriotic fantasy genre and kinoskazki" (1909-2002) : Proceedings of the popular encyclopedia / Motion Picture Arts Research Institute, If (magazine)|if .  - Moscow: B. Sekachev 2003 
* Bronze medal 1958: Б. Kudricha MCF technical and scientific films Belgrade. 

==Legacy==
The film  is believed to have significantly influenced Stanley Kubricks techniques in  , particularly in its accurate depiction of weightlessness and a rotating space station. Encyclopedia Astronautica describes some scenes from 2001 as a "shot-for-shot duplication of Road to the Stars".  Specific comparisons of shots from the two films have been analyzed by the filmmaker Alessandro Cima.   A 1994 article in American Cinematographer says, "When Stanley Kubrick made 2001: a Space Odyssey in 1968, he claimed to have been first to fly actor/astronauts on wires with the camera on the ground, shooting vertically while the actors body covered the wires" but observes that Klushantsev had preceded him in this. 

== References ==

 

== External links ==
* Jan Ivanovich Koltunov:  
*  —full film

 
 
 
 