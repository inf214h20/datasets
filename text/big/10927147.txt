You're Nobody 'til Somebody Kills You
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Youre Nobody til Somebody Kills You
| image          =
| caption        =
| director       = Michael A. Pinckney
| producer       = Spike Lee
| writer         = Michael A. Pinckney
| starring       = James McDaniel, Michael Mosley, Nashawn Kearse, Assiatou Lea, Kevin Carroll, Neko Parkham, Jacinto Taras Riddick
| music          = Kotchy, MAC A MILLION DOLLAR MAN MUSICK 
| cinematography = Ricardo Sarmiento
| editing        =
| distributor    = Locomotive Distribution Inc.
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $1M (estimated) 
}}

Youre Nobody til Somebody Kills You,  is a forthcoming film directed by Michael A. Pinckney, founder and Managing Director of Black Noise Media, a production house based in New York City. 

The film began production in May 2007,  however its release was delayed until 2011 for unknown reasons.  "Youre Nobody Til Somebody Kills You" was slated to premiere at Brooklyns Williamsburg International Film Festival dubbed "WILLiFEST" on September 23, 2011. 
 Michael Mosley ("Law & Order") and Nashawn Kearse (“The Sopranos” “Entourage (TV series)|Entourage.”)  

==Cast==
*James McDaniel – Detective Johnson Michael Mosley – Detective  Francelli
*Nashawn Kearse – “Manchild
*Doug E. Fresh – Rob Ski
*Big Daddy Kane – himself
*Michael K. Williams - A.D.

==Crew==
*Director - Michael A. Pinckney
*Writer - Michael A. Pinckney
*Executive Producer - Spike Lee
*Producers - Neil Carter, Michael A. Pinckney
*Co-Executive Producer: Jonathan Accarrino
*Executive Producers - Gabriel Gornell, Colleen Seldin 
*Casting Director – Donna DeSeta
*Costume Designer - Donna Berwick
*Production Designer - Sarah Frank
*Director of Photography - Ricardo Sarmiento
*Music Supervisor - Charlie Mac
*Original Tracks - MAC A MILLION DOLLAR MAN MUSICK
*Original Tracks - Kotchy  
*Title Track "Youre Nobody Till Somebody Kills You" -Lord Vital produced by Da Archutek for Archutek Communications, LLC

==Official Links==
* 

==References==
 

==External links==
* 

 
 
 
 
 