They Staked Their Lives
 
{{Infobox film
| name           = They Staked Their Lives
| image          = 
| caption        = 
| director       = Alf Sjöberg
| producer       = 
| writer         = Runar Schildt Alf Sjöberg
| starring       = Aino Taube
| music          = 
| cinematography = Harald Berglund
| editing        = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
They Staked Their Lives ( ) is a 1940 Swedish drama film directed by Alf Sjöberg.   

==Cast==
* Aino Taube as Wanda
* Åke Ohberg as John
* Anders Henrikson as Max (as Anders Henriksson)
* Gösta Cederlund as Baker
* Holger Löwenadler as Miller
* Eivor Landström as Eva
* Hampe Faustman as Freedom fighter (as Erik Faustman)
* Bengt Ekerot as Freedom fighter
* Torsten Hillberg as Colonel
* Ernst Brunman as Innkeeper
* Frithiof Bjärne as Captain (as Fritjof Bjärne)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 