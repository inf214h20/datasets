Man of a Thousand Faces
 
 
{{Infobox film
| name           = Man of a Thousand Faces
| image          = Moatf.jpg
| caption        = Theatrical release poster
| director       = Joseph Pevney Robert Arthur
| writer         = Robert Wright Campbell|R. Wright Campbell Ivan Goff Ben Roberts Ralph Wheelwright Robert Evans Marjorie Rambeau Frank Skinner
| cinematography = Russell Metty
| editing        = Ted Kent
| distributor    = Universal-International
| released       =  
| runtime        = 122 min.
| country        = United States
| language       = English, American Sign Language
| budget         =
| gross = $2.4 million (US) 
}}
 Lon Chaney, in which the title role is played by James Cagney.
 Roger Smith, Robert Evans, who soon left acting and eventually became head of Paramount Pictures. 

==Plot==
In the early 1900s, actor Lon Chaney (James Cagney) is working in vaudeville with his wife Cleva (Dorothy Malone). Chaney quits the show and Cleva announces that she is pregnant. Lon is happy and tells Cleva that he has been hired by the famous comedy team Kolb and Dill for an upcoming show.
 Colorado Springs. Lon is reluctant because his parents are both deaf mutes, a fact Lon has never shared. Cleva reacts with disgust and does not want to give birth, fearing that the child will also be a deaf mute and she doesnt want to be the mother of a "dumb thing." 

Months later, the baby Creighton is born and it soon becomes clear that the child is not deaf. Despite this good news, Lons and Clevas marriage continues to erode over the next few years. Soon she takes a job as a singer in a nightclub, dropping young Creighton off to his father backstage at his theater before being driven to work.
 platonic friendship with chorus girl Hazel Hastings (Jane Greer). Hazel is happy to help look after young Creighton. After the child gets sick at the theater, Lon complains to Clevas employer, who reluctantly agrees to terminate her. 

When confronting his wife in her dressing room, Lon discovers that Cleva has been dating Bill, a wealthy patron. Cleva learns shes been fired and reacts by screaming, which causes Bill to enter the dressing room. Bill comforts her and then asks Lon who he is. Lon responds by telling Bill that he is from the collection agency, and that he is here to collect his wife. Bill looks at Cleva with contempt and walks away.

Lon returns to the theater, where he discovers Hazel being accosted in the corridor by a tall, thin man. Lon punches the man in the face and tells him to get up. Hazel explains that he cant and the man lifts up his trousers to the knee, revealing two wooden legs. He is Carl Hastings, her former husband, once rational, but now consumed with bitterness as the result of his accident.

Cleva enters the dressing room to find Lon with his hands on Hazels shoulders. Cleva screams that she will not go back to being a "nurse maid" so that he can play around with a chorus girl. Cleva leaves home and vanishes. Days later, Lon is performing a dance routine in clown makeup at a matinee when a totally deranged Cleva walks on stage and swallows a bottle of acid in front of an audience, permanently damaging her vocal cords.

Cleva is hospitalized, but runs away once more. The scandal essentially destroys Lons career in vaudeville. The state takes custody of young Creighton as they deem his home situation to be unsuitable, causing Lon to react angrily. On the advice of press agent Clarence Locan (Jim Backus), Lon moves to Hollywood to try his luck in the new field of motion pictures.
 The Miracle The Hunchback The Phantom of the Opera.

As his career soars, Lon face personal challenges. Although he marries Hazel and regains custody of Creighton, ex-wife Cleva reappears, seeking to spend time with Creighton (who has been told by Lon that she is dead). Hazel reveals the truth to Creighton, who leaves to stay with his mother, angry with Lon about the deception.
 The Unholy Roger Smith) reconciles with his father and they take a fishing trip at Lons mountain cabin.  After returning from his fishing trip, Lon collapses and is returned home to live out his final days.

On his deathbed, the dying Chaney (now unable to speak due to the cancer) reverts to the sign language of his childhood to express his love for his friends and family and to ask for forgiveness for unspecified transgressions. Lon signals Creighton to bring him his makeup box. He removes a stick of greasepaint and ads a "Jr." to his own name on the box, signaling to his son his desire for him to carry on his lifes work. Creighton leaves with box in hand, ready to start his film career as Lon Chaney, Jr.

==Plot accuracy==
Creative license was used in writing the screenplay, and many incidents were sanitized and fictionalized, including the following:

Creighton Tull Chaney was not born in a hospital as is depicted in the film. He was born at his fathers home in Oklahoma City.   

Lon Chaney had stated in interviews at the time that he did not want Creighton (later Lon Chaney, Jr.) to be an actor  as is clearly depicted in the films conclusion. At the time of his fathers death, Creighton Chaney had been married for two years, attended business college, and worked at an LA water heater company.  When the company failed and financial problems became overwhelming for Creighton, he started to accept film work and was billed under his birth name. It was only in the mid 1930s that he allowed himself (at the insistence of film producers)  to be billed as "Lon Chaney, Jr.", an action he often said he felt ashamed of.  In later life Chaney, Jr. stated that he was proud of the name "Lon Chaney", but not of the name "Lon Chaney, Jr." 

In the film, Lon is depicted as being at home, and surrounded by family and friends when he passes on. In reality, Chaney died in his hospital room after suffering a hemorrhage. 

The depiction of Chaneys makeup for The Phantom of the Opera and The Hunchback of Notre Dame differs significantly from Chaneys original make up for these films. Cagneys face appears partially immobile behind an elaborate full latex mask and other make up. Lon Chaney, Sr. actually took great pride in his ability to distort his appearance using only a minimum of makeup, which still allowed for a great deal of facial expression. For instance, Chaney utilized thin wires in his nose and around his eyes, false teeth and dark paint around his eyes and nostrils, plus other methods. 

Bud Westmores recreations of these original make ups are clearly partial masks which vaguely resemble the originals. Cagneys face in some scenes is fairly immobile, such as the scene where he speaks to Creighton while wearing his Hunchback of Notre Dame make up, and when he speaks to the actress at the conclusion of the unmasking scene in The Phantom of the Opera.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 