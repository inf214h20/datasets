Main Aur Mera Haathi
{{Infobox film
 | name = Main Aur Mera Haathi
 | image = Mainhaathifilm.jpg
 | caption = DVD Cover
 | director = R. Thiagaraj
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Poonam Dhillon Suresh Oberoi Sharat Saxena
 | music = Kalyanji Anandji
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1981
 | runtime = 130 min.
 | country  = India Hindi
 Rs 1.3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1981 Hindi Indian feature directed by R. Thiagaraj, starring Mithun Chakraborty, Poonam Dhillon, Suresh Oberoi and Sharat Saxena.

==Plot==

Main Aur Mera Haathi is an action family drama, where Mithun and Poonam Dhillon playing the lead role, supported by Suresh Oberoi.

==Cast==

*Mithun Chakraborty as Ram / Raj
*Poonam Dhillon as Julie
*Suresh Oberoi as Teja
*Satyen Kappoo
*Sharat Saxena
*Keshto Mukherjee
*Mahendra Sandhu

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Main Aur Mere Haathi"
| Kishore Kumar
|-
| 2
| "Tere Liye"
| Kishore Kumar, Anuradha Paudwal
|-
3
"Hay Kahan Aisa Deewana"
Anuradha - Kishore Kumar
|-
4
"Pyar Ka Pyar Se"
Amit Kumar
|}

==External links==
*  

 
 
 
 