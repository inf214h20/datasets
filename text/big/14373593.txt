The Boys of Paul Street
{{Infobox film
| name           = The Boys of Paul Street
| image          = 
| caption        = 
| director       = Zoltán Fábri
| producer       = Endre Bohem
| writer         = Endre Bohem Zoltán Fábri Ferenc Molnár
| starring       = Mari Törőcsik
| music          = 
| cinematography = György Illés
| editing        = 
| studio         = Groskopf MAFILM Stúdió 1
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 110 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}} youth novel The Paul Street Boys (1906) by the Hungarian writer Ferenc Molnár. It was nominated for the Academy Award for Best Foreign Language Film.    It features English-speaking (American and British) child actors (led by Anthony Kemp as Ernő Nemecsek) accompanied by Hungarian adult ones including Fábris favorite actress Mari Törőcsik as Nemecseks mother. Currently it is acclaimed as the best and most faithful adaptation of Molnárs source novel and a classic film in Hungary.

==Cast==
* Mari Törőcsik as Nemecsek anyja
* Sándor Pécsi as Rácz tanár úr
* László Kozák as Janó
* Anthony Kemp as Nemecsek Ernő
* William Burleigh as Boka
* John Moulder-Brown as Geréb
* Robert Efford as Csónakos
* Mark Colleano as Csele
* Gary OBrien as Weisz
* Martin Beaumont as Kolnay
* Paul Bartleft as Barabás
* Earl Younger as Leszik

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 