Bedside-films
The Bedside-films is a series of eight feature films directed by John Hilbard in 1970-1976, and form part of the wave of erotic films from Denmark.

The eight films are connected by the Danish word "sengekant" (bedside) in the title of each film. They were produced by the film company Palladium, starring Danish actor Ole Søltoft in all except Motorvej på sengekanten.

All the Bedside-films had many pornographic sex scenes, but were nevertheless considered mainstream films. They all had mainstream casts and crews, and were shown in mainstream cinemas and reviewed in national newspapers etc. Ebbe Villadsen: Danish Erotic Film Classics (2005)  The first five, made 1970 - 1973, all featured the actress Birte Tove and were "soft-core". The last three, made 1975 and 1976, after the first of the Zodiac-films|Zodiac-films were released, all included hard-core scenes and shared many actresses with the Zodiac films, such as Anne Bie Warburg, Vivi Rau and Lisbeth Olsen. The actress Annie Birgit Garde features in all the Bedside-films.

Another Danish film company, Happy Film, made a similar series called the Zodiac-films|Zodiac-films, also starring Ole Søltoft. All of these films had hardcore-scenes, but were nonetheless also considered mainstream-productions, with mainstream casts and crews. The first Danish sex comedies were made in the 1960s, but Ole Eges Bordellet (1972) was the first to have hardcore sex-scenes. 

==List of the films==
There have been eight bedside movies in total:

* Mazurka på sengekanten (1970)
* Tandlæge på sengekanten (1971)
* Rektor på sengekanten (1972)
* Motorvej på sengekanten (1972)
* Romantik på sengekanten (1973)
* Der må være en sengekant (1975)
* Hopla på sengekanten (1976)
* Sømænd på sengekanten (1976)

=== Mazurka på sengekanten===
Mazurka på sengekanten (Mazurka on the bedside) was the first of the eight movies and many regard it as the best.
The film takes place in the all-boys private boarding school Krabbesøgaard (Crab Lake Farm). The rector (principal) of the school has been appointed as the new minister of culture.

Because of this, the rector Bosted (Axel Strøbye) needs to find his replacement. The choice falls on the young teacher Mikkelsen (Ole Søltoft), who is popular amongst the students because he wants to convert the boys school into a mixed gender school that also allows girls.
However there is a problem. Mikkelsen is a virgin and school policy dictates that the rector must be a married man. He has thirty days before the new government is officially presented and rector Bosted needs to resign, and in that time he must be engaged, or the position falls to the hated teacher Holst (Paul Hagen) also known as “the doormat”.

The boys, led by the seniors Torben, Vagn, Ole and Michael, that are all already worldly, therefore start on a quest. To get Mikkelsen laid.
With the help of rector Bosted’s neglected wife, both the chairman’s daughters, and a stripper/prostitute that the boys hire, Mikkelsen starts to gain a better understanding of the fairer sex.
But will it be enough to get him a wife in just 30 days, or will “the doormat” end up taking the position?

== External links ==
*  
* 

==See also==
*Zodiac-films|Zodiac-films
*List of mainstream films with unsimulated sex

==Further reading==
*Ebbe Villadsen: Danish Erotic Film Classics (2005)

==References==
 

 
 
 
 