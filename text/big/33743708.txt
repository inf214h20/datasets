Jeevana Mukthi
{{Infobox film
| name           = Jeevana Mukhti
| image          = 
| image_size     =
| caption        =
| director       = T. V. Neelakanthan
| producer       = S. S. Vasan
| writer         = Balijepalli Lakshmikantham (story & dialogues) Samudrala Raghavacharya (lyrics)
| narrator       =
| starring       = P. Suribabu Balijepalli Lakshmikantham Bezawada Rajarathnam Lanka Satyam Kamalakumari 
| music          = Saluri Rajeswara Rao
| cinematography = Sailen Bose
| editing        = 
| studio         = Gemini Studios
| distributor    =
| released       = 1942
| runtime        =
| country        = India Telugu
| budget         =
}}

Jeevana Mukthi ( ) or Jeevanmukthi  Telugu film produced by S. S. Vasan of Gemini Studios. 

==Plot summary== untouchable cobbler, Lord Vishnu appears before him every day, and eats whatever he is given as a prasad offering.  Santha, the daughter of the proud and powerful Rajaguru, learning of this, visits his hut to discover whether it is true.  When she witnesses it for herself, she informs her father of this wonder.  Angry that his daughter has visited the house of an untouchable, he locks her up.  He then informs Jeevudu that the kings mother was performing a cheppula nomu (Chappal vrata) and needs a thousand sandals by next morning.  Jeevudu and his family work all night, but can only make a few sandals.  They fall asleep from exhaustion; when they awaken the next morning, they find the house full of sandals. 
 Sridevi appear in the form of tribals.  They restore their devotees; Rajaguru realizes the error of his ways; and all ends happily.

==Cast==
* P. Suribabu as Jeevudu
* Bezawada Rajarathnam as Seva, wife of Jeevudu
* Master Viswam as Bhavudu, son of Jeevudu
* V. V. Satagopan as Lord Vishnu
* Balijepalli Lakshmikantham as Rajaguru
* Lanka Sathyam as Sishya of Rajaguru
* Kamala Kumari as Santha, daughter of Rajaguru
* Dasari Lakshmaiah Chowdary as King
* Annapurna as Sridevi, consort of Vishnu
* Shanta as Bhoodevi, consort of Vishnu
* Lakshmi Devi as Flower girl
* Sivaramakrishnaiah
* Narasimha Sastry

==Soundtrack==
There are two songs in the film. 
* Aaragimpa Raara Vidu Aaragimpa Raara - P. Suribabu and group
* Veligimpuma Naalo Jyothi Thilakinchaga O Deva - P. Suribabu

==References==
 

 
 
 