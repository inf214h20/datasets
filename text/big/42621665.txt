The Best Things in the World
{{Infobox film
| image          = The Best Things In The World.jpg
| caption        = Theatrical release poster
| director       = Laís Bodanzky
| producer       = Laís Bodanzky Luiz Bolognesi
| writer         = Luiz Bolognesic
| based on       =   Filipe Kartalian
| music          = 
| cinematography = Mauro Pinheiro Jr.
| editing        = Daniel Rezende
| studio         = Gullane Filmes
| distributor    = Warner Bros. Pictures Riofilme
| released       =  
| runtime        = 105 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$6 million 
| gross          = R$2,257,084  
}}
 Filipe Kartalian. 

==Plot==
The film is set in a middle-class school in São Paulo, and tells the one-month period in the life of Hermano, "Mano". Mano and his brother Pedro lead fun-loving lives until they learn their parents are getting a divorce. The anguish of their parents’ separation becomes more difficult when they discover their father is gay. Deeply affected by the changes at home, Mano must also deal with the challenge of being popular at school, having sex for the first time, the discovery of love and a snooping classmate’s destructive gossip blog. The arrival of adulthood brings with it overwhelming difficulties and a major transformation in the way Mano sees the world.

==Cast==
* Francisco Miguez as Mano
* Gabriela Rocha as Carol
* Caio Blat as Artur
* Paulo Vilhena as Marcelo Filipe Kartalian as Pedro
* Gustavo Machado as Gustavo
* Zé Carlos Machado as Horácio
* Maria Eugênia Cortez as Bruna
* Denise Fraga as Camila
* Anna Sophia Gryschek as Valéria

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 