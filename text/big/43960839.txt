Desert Desperadoes
{{Infobox film
| name           = Desert Desperadoes
| image          = Desert_Desperadoes.jpg
| alt            =
| caption        = 
| film name      = 
| director       = Steve Sekely
| producer       = John Nasht Robert Spafford Alan Furlan Robert Hill
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Mario Nascimbene
| cinematography = Massimo Dallamano
| editing        = 
| studio         = Venturini Express Nasht Productions RKO Radio Pictures States Rights Independent Exchanges
| released       =   }}
| runtime        = 81 minutes
| country        = United States Italy
| language       = English
| budget         = 
| gross          =  
}}
 Robert Hill. Co-produced by the Italian company Venturini Express and the American studio Nasht Productions, it was distributed by RKO Radio Pictures through the States Rights Independent Exchanges and released on July 16, 1959.  The film stars Ruth Roman and Akim Tamiroff.

==Cast==
*Ruth Roman as The Woman
*Akim Tamiroff as The Merchant
*Otello Toso as Verrus
*Gianni Musy as Fabius (credited as Gianni Glori)
*Arnoldo Foà as The Chaldean
*Alan Furlan as Rais
*Nino Marchetti as Metullus 

==References==
 

== External links ==
* 

 
 
 
 


 