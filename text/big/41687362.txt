The Oracle of Delphi (film)
{{Infobox film
| name           = LOracle de Delphes
| image          = Oracle Melies.jpg
| caption        = Frame from the film
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Star Film Company
| released       =  
| runtime        = 30 meters/100 feet   
| country        = France
| language       = Silent
| budget         = 
| gross          =
}}

The Oracle of Delphi ( ) is a 1903 French short silent film directed by Georges Méliès. It was released by Mélièss Star Film Company and is numbered 476 in its catalogues.   

==Plot==
In ancient Egypt, two priests deposit an ornate box inside a temple, locking the doors behind them. After their departure, a thief breaks into the temple and steals the box, but he is caught by a mysteriously appearing bearded figure. The bearded man retrieves the box and gives life to the two sphinx statues placed at the doors. The sphinxes appear as living women and attack the thief, whose head turns immediately into a donkeys head. The sphinxes turn back into statues, the bearded man disappears, and the donkey-thief is left sitting astonished on the ground.

==Versions== pirated by negatives of each film he made: one for domestic markets, and one for foreign release.    To produce the two separate negatives, Méliès built a special camera that used two lenses and two reels of film simultaneously. 
 stereo film 3D versions of Méliès films could be made simply by combining the domestic and foreign prints of the film.  Serge Bromberg, the founder of Lobster Films, presented 3D versions of The Oracle of Delphi and another 1903 Méliès film, The Infernal Cauldron, at a January 2010 presentation at the Cinémathèque Française. According to the film critic Kristin Thompson, "the effect of 3D was delightful … the films as synchronized by Lobster looked exactly as if Méliès had designed them for 3D."  Bromberg screened both films again—as well as the 1906 Méliès film The Mysterious Retort, similarly prepared for 3D—at a September 2011 presentation at the Academy of Motion Picture Arts and Sciences. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 