The Gay Deception
{{Infobox film
| name           = The Gay Deception
| image          = Gay-deception-1935.jpg
| image_size     =
| caption        = Film poster
| director       = William Wyler
| producer       = Jesse L. Lasky
| writer         = Stephen Morehouse Avery Don Hartman Patterson McNutt (uncredited contributor to treatment) Samuel Raphaelson (uncredited contributor to dialogue) Arthur Richman (uncredited additional dialogue)
| narrator       =
| starring       = Francis Lederer Frances Dee
| music          =
| cinematography =
| editing        =
| studio         = Fox Film Corporation
| distributor    = 20th Century Fox
| released       =  
| runtime        = 75-77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Gay Deception is a 1935 romantic comedy film starring Francis Lederer and Frances Dee. Writers Stephen Morehouse Avery and Don Hartman were nominated for the Academy Award for Best Story.

==Plot==
Secretary Mirabel Miller (Frances Dee) wins a lottery and decides to live it up in a luxurious New York hotel, where she clashes with a bellboy (Francis Lederer) who is more than he appears to be.

==Cast==
*Francis Lederer as Sandro
*Frances Dee as Mirabel Miller
* Benita Hume as Miss Cordelia Channing
* Alan Mowbray as Lord Clewe
* Lennox Pawle as Consul-General Semanek
*Adele St. Mauer as Lucille (as Adele St. Maur)
* Akim Tamiroff as Spellek
* Luis Alberni as Ernest
* Lionel Stander as Gettel
* Ferdinand Gottschalk as Mr. Squires
* Richard Carle as Mr. Spitzer
*Lenita Lane as Peg DeForrest
*Barbara Fritchie as Joan Dennison Paul Hurst as Bell Captain Robert Greig as Adolph

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 