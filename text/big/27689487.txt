Live Exposure
{{Infobox album 
| Name        = Live Exposure
| Type        = video
| Artist      = Little River Band 100px
| Cover size  = 200
| Released    = 1981 (VHS) 2008 (DVD) The Summit, Houston, Texas
| Genre       = 
| Length      = 75 minutes EMI
| Director    = Derek Burbridge
}}
 The Summit in Houston, Texas on 7 October 1981.

Live Exposure was first released on VHS in 1981, and subsequently released on DVD in 2008

== Track listing == Goble (from Little River Band) Tolhurst (Time Time Exposure)
#"Mistress Of Mine" – Goble (First Under the Wire) Briggs (Diamantina Cocktail)
#"Dont Let The Needle Win" – Briggs (Time Exposure)
#"Reminiscing" – Goble (Sleeper Catcher)
#"Ballerina" – Birtles/Goble (Time Exposure) Cool Change" – Shorrock (First Under the Wire)
#"The Night Owls" – Goble (Time Exposure)
#"Help Is on Its Way" – Shorrock (Diamantina Cocktail)
#"Lonesome Loser" – Briggs ([First Under the Wire)
#"Its Not A Wonder" – Goble (First Under the Wire)
#"Lady" – Goble (Sleeper Catcher)
#"Just Say That You Love Me" – Goble (Time Exposure)

== Personnel ==
 
;Little River Band
*Glenn Shorrock – lead vocal 
*Graeham Goble – harmony vocals, acoustic and electric guitars 
*Beeb Birtles – lead and harmony vocals, acoustic and electric guitars 
*Derek Pellicci – drums 
*Wayne Nelson – bass, lead and harmony vocals 
*Stephen Housden – lead guitar 
*Mal Logan – keyboards

==Reviews==
* 
* 

== External links ==
*  

 

 
 