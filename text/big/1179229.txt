Sudden Impact
 
{{Infobox film
| name           = Sudden Impact
| image          = Sudden Impact.jpg
| caption        = Theatrical poster by Bill Gold
| alt            = A picture of Detective Harry Callaghan against a city skyline. In front of him is glass with a bullet-hole. 
| story          = Earl E. Smith  Charles B. Pierce
| screenplay     = Joseph Stinson
| based on       = Characters created by Harry Julian Fink  R.M. Fink
| starring       = {{Plain list|
* Clint Eastwood
* Sondra Locke
}}
| director       = Clint Eastwood
| producer       = Clint Eastwood
| cinematography = Bruce Surtees
| editing        = Joel Cox
| distributor    = Warner Bros.
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $22 million  
| music          = Lalo Schifrin
| gross          = $67,642,693 (U.S.A)  
}} localization "Coraggio... fatti ammazzare", was also chosen as title for the Italian version of the film.

==Plot==
Artist Jennifer Spencer and her sister are raped by a group of boys, after being betrayed by female friend Rae Parkins. The brutal rape leaves Jennifers sister permanently catatonic. Ten years later, Spencer seeks revenge. She kills one of the rapists (George Wilburn) with two shots—one in the groin and one in the head—from a snubnosed revolver|.38 snubnosed revolver. Spencer then leaves San Francisco because of the subsequent police investigation. Once relocated in the town of San Paulo, Spencer begins restoring its boardwalks historic carousel near the beach where the rapes occurred. 
 Harry Callahan is frustrated when a judge yet again dismisses a case due to unreasonable search and seizure. Later, at his favorite diner, the inspector interrupts a robbery and kills most of the criminals. When the surviving robber takes a hostage, Callahan targets the man with his .44 Magnum and challenges him to "Go ahead, make my day". The criminal surrenders. Callahan later causes powerful crime lord Threlkis to suffer a fatal heart attack at his granddaughters wedding dinner.  

Lieutenant Donnelly and other angry superior officers call Callahan in. They cannot fire or suspend the notorious inspector because, as the police commissioner admits, his "unconventional methods ... get results", albeit with tremendous physical destruction and bad publicity for the department. They instead order him to take a vacation, which Callahan spends target shooting with his AutoMag (pistol)|.44 AutoMag and shotgun-armed partner Horace. But Callahans relaxation is short-lived, as four of Threlkiss hitmen attack him. The inspector dispatches three, and the other narrowly escapes. The suspect from the dismissed case and his friends also attack, throwing two Molotov cocktails into Callahans car. He retrieves one of the undetonated bombs and throws it at the attackers car, causing the men to fatally swerve into the bay. Donnelly immediately sends the inspector to San Paulo to investigate the murder of the man Spencer killed. While the victim is from there, the assignment is also to protect both Callahan and civilians. As Donnelly notes, "people have a nasty habit of getting dead around you."

Upon arriving in sleepy San Paulo, Callahan chases down a would-be robber. But, the reckless hot pursuit draws the anger of the local police. While jogging with his bulldog "Meathead"—a present from Horace—Callahan literally runs into Jennifer Spencer. She is less than thrilled. Upon returning to his motel, Callahan is hunted by the surviving Threlkis hitman. The inspector kills him, after being warned by Meathead. Meanwhile, Spencer kills a second rapist, Kruger, at the beach. Callahan recognizes the modus operandi, but police chief Lester Jannings refuses to work with the famous "big city hotshot" inspector. Callahan learns that the victims and Parkins are friends of Jannings son, Alby. Parkins figures out that the rapists are being targeted and warns two of them (Tyrone and Mick). After fighting Krugers uncooperative brothers-in-law, Eddie and Carl, Callahan meets Spencer again—this time, at an outdoor cafe. Over drinks, he learns that she shares his emphasis on results over methods when seeking justice. But, the inspector adds the caveat "til it breaks the law." He then reveals that he is investigating the San Francisco murder of George Willburn (which rattles Spencer).

Callahan visits Tyrones home and finds him dead, yet another victim of Spencer. Mick stays at Parkins home and both await a likely attack. When the inspector visits them for questioning, Mick lunges for him. After Callahan subdues Mick and takes him to the police station, Spencer arrives and guns down Parkins. Callahan and Spencer meet again and sleep together. But on his way out he notices Spencers car (which he saw earlier at Parkins house).  He goes back to Parkins house and finds her body. Eddie and Carl bail Mick out. Meanwhile, Horace arrives at Callahans motel to celebrate the easing of tensions in San Francisco. Only he meets Mick and company instead. Horace is killed and Mick finds Callahan. The inspector is beaten up and thrown into the ocean.

Meanwhile, Spencer arrives at the Jannings home with intent to kill. She finds Alby Jannings has lapsed into a catatonic state, after his guilty conscience caused him to attempt suicide in a car accident. Chief Jannings admits that to protect his reputation and his only child he "fixed" the crimes. He convinces Spencer to spare Albys life and promises that Mick, whom he does not know is free, will now be punished. Mick and the others, capture Spencer and kill the chief with her .38.

Callahan survives Micks assault. He retrieves his AutoMag from the motel. Micks group brings Spencer to the Santa Cruz Beach Boardwalk for another rape, but she escapes to the carousel. They recapture her, but are startled by the inspectors apparent return from death. After killing the others, Callahan chases Mick, who absconds with Spencer atop the Giant Dipper. The inspector reiterates his famous challenge "make my day"—this time to Mick. When Mick laughs at Callahan, Spencer uses the diversion to break away. Mick is left wide open for Callahan to shoot. He aims and drops the remaining rapist, who plunges through the carousel skylight to a grisly death onto a carousel unicorn. 

The police arrive and find Spencers .38 with Mick; ballistics, Callahan states, will find that "his gun … was used in all the killings." A compassionate Callahan and vindicated Spencer leave the crime scene together.

==Cast== Harry Callahan
* Sondra Locke as Jennifer Spencer     
* Pat Hingle as Chief Lester Jannings
* Bradford Dillman as Captain Briggs Paul Drake as Mick
* Albert Popwell as Horace King
* Audrie J. Neenan as Ray Parkins
* Jack Thibeau as Kruger Michael Currie as Lt. Donnelly
* Michael V. Gazzo as Threlkis
* Mark Keyloun as Officer Bennett
* Kevyn Major Howard as Hawkins
* Bette Ford as Leah
* Nancy Parsons as Mrs. Kruger

==Production==
The screenplay was initially written by Charles B. Pierce and Earl E. Smith for a separate film for Sondra Locke, but was later adapted into a Dirty Harry film by Joseph Stinson. Hughes, p.66  Filming occurred in spring 1983. Hughes, p.69  Many of the films scenes were filmed in San Francisco and Santa Cruz, California. Hughes, p.65  The scene where Harry chases a bank robber in the downtown business district offers a rare glimpse of the area before it was devastated by the Loma Prieta earthquake of October 17, 1989. Footage for the robbery in "Acorn Cafe" was shot at Burger Island (now a McDonalds) at the corner of 3rd Street and Townsend in San Francisco. At this point in his career, Eastwood was receiving a salary that included 60% of all film profits, leaving the other 40% for the studio. Estimates had Eastwood earning $30 million for Sudden Impact. 

==Reception==
===Box office===
The film was a box-office success. In its opening weekend the film took $9,688,561 in 1,530 theaters in the US.    In total in the US, the film made $67,642,693, making it the highest grossing of the five films in the Dirty Harry franchise.  

===Critical response===
It received mixed reviews from critics. Review aggregation website Rotten Tomatoes retrospectively gave the film a score of 59% based on 34 reviews.  
 The Gauntlet. Among other things, the movie never gets a firm hold on its own continuity. Sometimes scenes of simultaneous action appear to take place weeks or maybe months apart."  Roger Ebert was more positive; while noting that the film was "implausible" with "a cardboard villain", he also praised it as "a Dirty Harry movie with only the good parts left in" and "a great audience picture". 

==Legacy== most memorable line in cinema history. United States President Ronald Reagan used the "make my day" line in a March 1985 speech threatening to veto legislation raising taxes.   When campaigning for office as mayor of Carmel-by-the-Sea, California, in 1986, Eastwood used bumper stickers entitled "Go Ahead &mdash; Make Me Mayor". 

==References==
 

==Bibliography==
*  
*  

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 