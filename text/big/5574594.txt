Borat
 
 
 
 
 
 
{{Infobox film
| name                 = Borat
| image                = Borat ver2.jpg
| caption              = Theatrical release poster in faux Cyrillic
| director             = Larry Charles
| producer             = {{Plain list|
* Sacha Baron Cohen
* Jay Roach
}}
| screenplay           = {{Plain list|
* Sacha Baron Cohen
* Anthony Hines
* Peter Baynham
* Dan Mazer
}}
| story                = {{Plain list|
* Sacha Baron Cohen
* Peter Baynham
* Anthony Hines
* Todd Phillips
}}
| based on             =  
| starring             = Sacha Baron Cohen
| music                = Erran Baron Cohen
| cinematography       = {{Plain list|
* Anthony Hardwick
* Luke Geissbühler
}}
| editing              = {{Plain list|
* Peter Teschner
* James Thomas
* Craig Alpert
}}
| production companies = {{Plain list|
* Four By Two
* Everyman Pictures
* Dune Entertainment
* Major Studio Partners
* One America Productions
}}
| distributor          = 20th Century Fox
| released             =  
| runtime              = 84 minutes  
| country              = {{Plain list|
* United Kingdom
* United States 
}}
| language             = {{Plain list|
* English
* Hebrew
* Armenian
}}
| budget               = $18 million   
| gross                = $261.6 million 
}} cameo by Cockney rhyming slang for "Jew". 
 Best Adapted Screenplay at the 79th Academy Awards.
 Arab countries Russian government Region 1 countries).

==Plot==
Kazakh television personality Borat Sagdiyev leaves Kazakhstan for the "Greatest Country in the World", the "U, S and A" to make a documentary at the behest of the Kazakh Ministry of Information. He leaves behind his wife Oksana and other inhabitants of his village including his "43-year-old" mother, "No. 4 prostitute in all of Kazakhstan" sister, "the town rapist", "the town mechanic and abortionist", bringing along his producer Azamat Bagatov and a pet chicken.
 immediately falls the work of Jews. Borat, therefore, takes driving lessons and buys a dilapidated Gaz truck for the journey.
 live television jingoistic American remarks, but then sings a fictional Kazakhstani national anthem to the tune of "The Star-Spangled Banner", receiving a strong negative reaction. Staying at a bed-and-breakfast, Borat and Azamat are stunned to learn their hosts are Jewish. Fearful at the hands of their hosts, the two escape after throwing money at two woodlice, believing they are their Jewish hosts transformed. While Azamat advises a return to New York, Borat attempts to buy a handgun to defend himself against Jews. When told he cannot buy a gun because he is not an American citizen, Borat purchases a bear for protection.
 Confederate heritage items, breaking glass and Dishware|crockery.
 masturbating over a picture of Pamela Anderson in the Baywatch book. Borat becomes enraged and reveals his real motive for travelling to California. Azamat becomes livid at Borats deception, and the situation escalates into a fully nude brawl with homoerotic undertones,  which spills out into the hallway, a crowded elevator, and ultimately into a packed convention ballroom. The two are finally separated by security guards.
 hitchhike to fraternity brothers ticket to United Pentecostal Republican United Mississippi Supreme Court Chief Justice James W. Smith, Jr. are present. Borat learns to forgive Azamat and Pamela. He accompanies church members on a bus to Los Angeles and disembarks to find Azamat dressed as Oliver Hardy (though Borat thinks that he is dressed as Adolf Hitler). The two reconcile and Azamat tells Borat where to find Pamela Anderson.
 Virgin Megastore. traditional marriage sack", Borat pursues her throughout the store in an attempt to abduct her until he is tackled and handcuffed by security guards. Afterwards, Borat marries Luenell, and returns to Kazakhstan with her. The final scene shows the changes that Borats observations in America have brought to his village, including the apparent conversion of the people to Christianity (the Kazakh version of which includes crucifixion and torturing of Jews) and the introduction of computer-based technology, such as iPods, laptop computers and a high-definition, LCD television.

The film plays out with a recapitulation of a mock Kazakhstan national anthem glorifying the countrys potassium resources and its prostitutes as being the "cleanest in the region". The visual melange of Soviet-era photos are mixed with the real flag of Kazakhstan and, incongruously, the final frames show the portrait of Ilham Aliyev, real-life president of Azerbaijan, a country that had not been otherwise mentioned in the film.

==Cast==
  premiere of the film.]] Kazakh journalist, film spin-off. Boston Phoenix.  Luenell as Luenell the prostitute; first seen when Borat calls her to come to the Southern dinner, the climax of his effective destruction of the event.
* Pamela Anderson as herself; she plays a central role in the film as the reason for the journalists cross country journey. She also appears in person at the end of the film, in a botched abduction attempt by Borat for cultural "marriage". 
* When Borat seeks advice from an etiquette coach, he goes on to show nude photos of his allegedly teenaged son.   These photos actually show gay porn star Stonie,     who was chosen because producers were seeking "someone who would look 13 or 14 but was actually of legal age and would do frontal nudity". 

==Production==
 
Except for Borat, Azamat, Luenell, and Pamela Anderson, none of the characters are portrayed by actors.      Most scenes in the film were unscripted,  although the end credits do credit a "Naked Fight Coordinator". In most cases, the films participants were given no warning on what they would be taking part in except for being asked to sign release forms agreeing not to take legal action against the films producers. Engber, Daniel.   Slate.com, 24 October 2006. Retrieved on 2007-03-14. 

Principal photography was already under way in January 2005, when Baron Cohen caused a near riot in what would ultimately be the rodeo scene in the final cut of the film.    An interview with Baron Cohen by Rolling Stone indicated that more than 400 hours of footage had been shot for the film. 

===Location===
The "Kazakhstan" depicted in the film has little or no relationship with the actual country, and the producers explicitly deny attempting to "convey the actual beliefs, practices or behaviour of anyone associated with Kazakhstan" in the all persons fictitious disclaimer|"all persons fictitious" disclaimer. The scenes showing Borats home village were filmed in the village of Glod, Dâmboviţa|Glod, Romania.    The name of Borats neighbour, Nursultan Tuyakbay, is a cross between the names of Kazakh President Nursultan Nazarbayev and opposition politician Zharmakhan Tuyakbay.

===Language=== Polish (or other related languages) for "How are you?" and "thank you", respectively.  While presenting his house, Borat says "tishe" to his house-cow; "tiše/тише" is Russian (similar words exist in other Slavic languages) for "quiet(er)" or "be quiet". 

===Deleted scenes===
The DVD included several   clone that also starred Azamat, Luenell, and Alexandra Paul. A scene in which Borat "started pretending he was being arrested" was also filmed, but was removed under the threat of legal action by prison officials when they learned that the "documentary" was a satire.  In an interview, one of the films writers, Dan Mazer, confirmed that there was a scene filmed but cut in which Borat observed the shooting of actual pornography with actress Brooke Banner. Mazer stated that the scene was deleted so as not to compete with the naked hotel-fight, but hinted it might be included in future DVD releases.  , FantasyMoguls.com. Retrieved on 18 January 2007 

==Release==
 

===Previews===
Borat was previewed at the 2006  , Snyder, Gabriel.  : "Moore takes the floor". Retrieved on 21 December 2006  where it won the Excellence in Filmmaking Award.  , Traverse City Record Eagle, 7 August 2006. Retrieved on 21 December 2006. 

The films official debut was in Toronto on 7 September 2006, at the Ryerson University Theatre during the Toronto International Film Festival. Baron Cohen arrived in character as Borat in a cart pulled by women dressed as peasants. Twenty minutes into the showing, however, the projector broke. Baron Cohen performed an impromptu act to keep the audience amused, but ultimately all attempts to fix the equipment failed.     The film was successfully screened the following night, with Dustin Hoffman in attendance. " ", USA Today, 17 September 2006. Retrieved on 21 December 2006 

In Israel, a proposed poster depicting Borat in a sling bikini was rejected by the films advertising firm in favour of one showing him in his usual suit. 

===Scaled-back U.S. release===
The film opened at No. 1 in the box office, maintaining first place for two weeks straight. The film earned more in the second week ($28,269,900) than in the first ($26,455,463), due to an expansion onto 2,566 screens. 

===Theatrical release===
Borat had its public release on 1 November 2006 in Belgium. By 3 November 2006, it had opened in the United States and Canada, as well as in 14 European countries. Upon its release, it was a massive hit, taking in US$26.4 million in its opening weekend, the highest ever in the United States and Canada for a film released in fewer than 1,000 cinemas  until   in 2008.    However, its opening day (approximately $9.2 million)    was larger than that of the Hannah Montana concert (approximately $8.6 million),    leaving Borat with the record of the highest opening day gross for a film released in fewer than 1,000 cinemas. On its second weekend, Borat surpassed its opening with a total of US$29 million.   

==Reception==

===Critical===
Borat: Cultural Learnings of America for Make Benefit Glorious Nation of Kazakhstan was well received by critics. On Rotten Tomatoes the film holds a rating of 91%, based on 211 reviews, with an average rating of 8/10. The sites critics consensus reads, "Jagshemash! Borat gets high-fives almost all-around for being offensive in the funniest possible way. Part satire, part shockumentary, Borat stars Sacha Baron Cohen as the gleefully sexist, homophobic, and anti-Semitic title character on a cross-country trek to learn more about our strange nation; along the way he dredges up the seamy underbelly of American prejudice and ignorance. Now the cat is out of the bag, what will Cohen do for an encore?".  On Metacritic the film has a score of 89 out of 100, based on 38 critics, indicating "universal acclaim". 

In an article about the changing face of comedy, The Atlantic Monthly said that it "may be the funniest film in a decade".  Michael Medved gave it 3.5 out of 4 stars, calling it "...simultaneously hilarious and cringe-inducing, full of ingenious bits that youll want to describe to your friends and then laugh all over again when you do." 

The Guardian included the film in its list of ten Best films of the noughties (2000–09).   

One negative review came from American critic  , writer Christopher Hitchens offered a counter-argument to suggestions of anti-Americanism in the film. Hitchens suggested instead that the film demonstrated amazing tolerance by the films unknowing subjects, especially citing the reactions of the guests in the Southern dinner scene to Borats behaviour. 

By posting scenes from the film on YouTube, Borat was also exposed to viral communication. This triggered discussions on different national identities (Kazakh, American, Polish, Romanian, Jewish, British) that Baron Cohen had exploited in creating Borat the character. 

===Commercial===
American audiences embraced the film, which played to sold-out crowds at many showings on its opening, despite having been shown on only 837 screens. Borat debuted at No. 1 on its opening weekend, with a total gross of $26.4 million,    beating its competitors  . The films opening weekends cinema average was an estimated $31,511, topping  , yet behind   and Spider-Man (2002 film)|Spider-Man.  It retained the top spot in its second weekend after expanding to 2,566 theatres, extending the box office total to $67.8 million. 

In the United Kingdom, Borat opened at No. 1, with an opening weekend gross of £6,242,344 ($11,935,986),  the 43rd best opening week earnings in the UK as of March 2007.  Since its release, Borat has grossed over $260 million worldwide. 

===Awards and nominations===
Borat received a nomination at the   under the category of  , but lost to Dreamgirls (film)|Dreamgirls.  The Broadcast Film Critics Association named it the Best Comedy Movie of 2006, and the Writers Guild of America, West nominated it for their award for Best Adapted Screenplay.   BFCA.org. Retrieved on 17 March 2007.    WGA.com. Retrieved on 17 March 2007. 

Baron Cohen won a Golden Globe for  .  He received equivalent awards from the  , 2006. Retrieved on 17 March 2007.   . AltFG.com. Retrieved on 17 March 2007.    TFCA Awards 2006. Retrieved on 17 March 2007.   The  , 2006. Retrieved on 17 March 2007.    AltFG.com. Retrieved on 17 March 2007. 
 American Film Time Magazine, Rolling Stone, David Ansen for Newsweek, and Lou Lumenick for the New York Post.      2006 Film Critic Top Ten Lists. Retrieved on 6 January 2007. 
<!-- commenting out tagged sentence
On 3 June 2007, Baron Cohen won the MTV Film Award for best Comedic Performance for Borat.  -->

===Retirement of Borat character===
A  , a gay Austrian fashion reporter. Universal Studios is reported to have produced the film with a budget of $42 million. 

Rupert Murdoch announced in early February 2007 that Baron Cohen had signed on to do another Borat film with Fox.  However, this was contradicted by an interview with Baron Cohen himself in which Baron Cohen stated that Borat was to be discontinued, as he was now too well known to avoid detection as he did in the film and on Da Ali G Show.  A spokesman for Fox later stated that it was too early to begin planning such a film, although they were open to the idea. 

Baron Cohen subsequently announced that he was "killing off" the characters of Borat and Ali G because they were now so famous he could no longer trick people. Even though he decided to retire his trademark characters, on 26 February 2014, he brought the world-famous characters back to life (in Borats case) & out of retirement (in Ali Gs case) for his new FXX series "Ali G: Rezurection," which is a collection of the sketches from all 18 episodes of "Da Ali G Show," including new footage of Baron Cohen in-character as Ali G, who is portrayed as the presenter of the show, in a sense, reviving his characters sketches, and therefore, the characters themselves, in a clip show-esque fashion.   

==Controversies==

===Participants responses===
After the films release, Dharma Arthur, a news producer for WAPT-TV in Jackson, Mississippi, wrote a letter to Newsweek saying that Borats appearance on the station had led to her losing her job: "Because of him, my boss lost faith in my abilities and second-guessed everything I did thereafter … How upsetting that a man who leaves so much harm in his path is lauded as a comedic genius." Although Arthur has said she was fired from the show, she told the AP that she left the station.  She said that she checked a public relations website that Borats producers gave her before booking him. 

In news coverage that aired in January 2005 of the filming of the rodeo scene, Bobby Rowe, producer of the Salem, Virginia, rodeo depicted in the film, provided background on how he had become the victim of a hoax. He said that "months" prior to the appearance, he had been approached by someone from "One America, a California-based film company that was reportedly doing a documentary on a Russian immigrant"; he agreed to permit the "immigrant" to sing the U.S. national anthem after listening to a tape.  After the films release, Rowe said "Some people come up and say, Hey, you made the big time; Ive made the big time, but not in the way I want it."  Cindy Streit, Borats etiquette consultant, has subsequently hired high-profile attorney Gloria Allred, who demanded that the California Attorney General investigate fraud allegedly committed by Baron Cohen and the films producers. 

The Salon.com|Salon Arts & Entertainment site quotes the Behars (a Jewish couple whose guest house Borat and Azamat stay at) as calling the film "outstanding", referring to Baron Cohen as "very lovely and very polite" and a "genius".  The Boston Globe also interviewed the couple, saying they considered the film more anti-Muslim than anti-Semitic and had feared that Baron Cohen and his ensemble might be filming pornography in the house. 

The feminists from Veteran Feminists of America (VFA) felt that they had been duped, having "sensed something odd was going on" before and during the interview with Borat. The Guardian later reported at least one of the women felt that the film was worth going to see at the cinema. 

The New York Post had reported in November 2006 that Pamela Anderson filed for divorce from her husband Kid Rock after he reacted unfavourably to the film during a screening. The Post s article specifically claimed he had said of her role in the film, "Youre nothing but a whore! Youre a slut! How could you do that movie?"  In an interview on The Howard Stern Show, Anderson confirmed that Rock was upset by her appearance in the film, but did not confirm that this was the cause of the separation. 

===Legal action by participants=== lei (about US$1.28 in 2004) each, while others stated they were paid between $70 and $100 each, which did not cover their expenses.   They are asking for $38 million in damages.  One lawsuit was thrown out by U.S. District Judge Loretta Preska in a hearing in early December 2006 on the ground that the allegations in the complaint were too vague. The litigants said they planned to refile. 
 fraternity brothers who appeared in the film, Justin Seay and Christopher Rotunda, sued the producers, claiming Slander and libel|defamation.        The suit by Seay and Rotunda was dismissed in February 2007.  The students also had sought an injunction to prevent the DVD release of the film, which was denied.  Finn, Natalie.  . E! Online 21 February 2007. Retrieved on 2007-03-07.   

Another lawsuit was filed by a South Carolina resident who said he was accosted by Baron Cohen (as Borat) in the bathroom at a restaurant in downtown Columbia, South Carolina|Columbia, with the actor allegedly making comments regarding the individuals genitals, without signing any legal waiver. The lawsuit also sought to have the footage excluded from any DVD releases and removed from Internet video sites. 
 Macedonian Romani Romani singer Esma Redžepova sued the films producers, seeking €800,000 because the film used her song "Chaje Šukarije" without her permission.   Afterwards, Redžepova won a €26,000 compensation, since it turned out that Baron Cohen had got permission from her production house to use the song, which she had not been notified about. 

A lawsuit was launched by Felix Cedeno, who wanted $2.25 million from 20th Century Fox, saying they invaded his privacy and needed permission to use his image. The 31-year-old was riding the subway home to the South Bronx when Baron Cohen let a live hen out of his suitcase, causing chaos in the subway car. 

Baltimore resident Michael Psenicska sought more than $100,000 in damages from Baron Cohen, 20th Century Fox, and other parties. Psenicska—a high school mathematics teacher who also owns a driving school—was reportedly paid $500 in cash to give Baron Cohens bogus Kazakh journalist a driving lesson. In his action—filed in the U.S. District Court in Manhattan—the driving instructor said that he had been told the film was a "documentary about the integration of foreign people into the American way of life", and that if he had known the films true nature, he would have never participated. Psenicska said he was entitled to damages because the defendants used images of him to advertise the film.  The case was dismissed on 9 September 2008. 

Jeffrey Lemerond, who was shown running and yelling, "Get away" as Borat attempted to hug strangers on a New York street, filed a legal case claiming his image was used in the film illegally, and that he suffered "public ridicule, degradation and humiliation" as a result. The case was dismissed. 

Baron Cohen reacted to these suits by noting, "Some of the letters I get are quite unusual, like the one where the lawyer informed me Im about to be sued for $100,000 and at the end says, P.S. Loved the movie. Can you sign a poster for my son Jeremy?  

===Reception in Kazakhstan===
The government of Kazakhstan at first denounced Borat. In 2005, following Borats appearance at the MTV Movie Awards, the countrys Foreign Ministry threatened to sue Sacha Baron Cohen, and Borats ".kz|Kazakh-based" website, www.borat.kz, was taken down.   Kazakhstan also launched a multi-million dollar "Heart of Eurasia" campaign to counter the Borat effect; Baron Cohen replied by denouncing the campaign at an in-character press conference in front of the White House as the propaganda of the "evil nitwits" of Uzbekistan.   Uzbekistan is, throughout the film, referred to by Borat as his nations second leading problem, with the first being the Jews.

In 2006, Gemini Films, the Central Asian distributor of 20th Century Fox, complied with a Kazakh government request to not release the film.  That year, Kazakh ambassador Erlan Idrissov, after viewing the film, called parts of the film funny and wrote that the film had "placed Kazakhstan on the map".    By 2012, Kazakh Foreign Minister Yerzhan Kazykhanov attributed a great rise in tourism to his country&mdash;with visas issued rising ten times&mdash;to the film, saying "I am grateful to Borat for helping attract tourists to Kazakhstan."   
 Amazon UK has also reported significant numbers of orders of Borat on DVD from Kazakhstan. 

In March 2012, the parody national anthem from the film, which acclaims Kazakhstan for its high-quality potassium exports and having the second cleanest prostitutes in the region, was mistakenly played at the Amir of Kuwait International Shooting Grand Prix. The gold medalist, Maria Dmitrienko, stood on the dais while the entire parody was played. The team complained, and the award ceremony was restaged. The incident apparently resulted from the wrong song being downloaded from the Internet.  

===Accusations of racism===
The European Center for Antiziganism Research, which works against negative attitudes toward Romani people, filed a complaint    with German prosecutors on 18 October 2006, based on Borats references to Gypsies in his film. The complaint accuses him of defamation and inciting violence against an ethnic group.  As a consequence, 20th Century Fox declared that it would remove all parts referring to Romani people from trailers shown on German television as well as on the films website. 

Before the release of the film, the Anti-Defamation League (ADL) released a statement expressing concern over Borats characteristic anti-Semitism.  Both Baron Cohen (who is Jewish) and the ADL have stated that the film uses the titular character to expose prejudices felt or tolerated by others,  but the ADL expressed concern that some audiences might remain oblivious to this aspect of the films humor, while "some may even find it reinforcing their bigotry". 

===Censorship in the Arab world=== banned in the entire Arab world except for Lebanon.     Yousuf Abdul Hamid, a film censor for Dubai in the United Arab Emirates, called the film "vile, gross and extremely ridiculous". The censor said that he and his colleagues had walked out on their screening before it had ended, and that only half an hour of the film would be left once all the offensive scenes were removed. 

==Soundtrack==
 
The soundtrack for Borat was released on the iTunes Store on 24 October 2006, and in shops on 31 October 2006. The album included music from the film, five tracks entitled "Dialoguing excerpt from moviefilm", as well as the controversial anti-Semitic song "In My Country There Is Problem" from Da Ali G Show. 
 Gypsy and Balkan artists (mostly Emir Kusturica and Goran Bregovic) and includes music by Erran Baron Cohen, founding member of ZOHAR Sound System and brother of Borat star Sacha Baron Cohen, as well as songs sung by Sacha Baron Cohen himself in character as Borat.

==Home media== Region 2 Region 1 a Virginia TV station about Borats night at the rodeo, complete with an interview with rodeo owner Bobby Rowe.
 Fox in-cover advertising is written in broken English that appears poorly printed, indicating that there are "More movie discs available from US&A" and "Also legal to own in Kazakhstan".
 trailers promises that the depicted films are "coming Kazakhstan in 2028". By April 2007, the DVD had sold over 3.5 million copies, totaling more than $55 million in sales.  While a Blu-ray Disc release date for the U.S. has yet to be announced, it has been released on Blu-ray in Germany, the United Kingdom, France, Australia, Brazil, the Netherlands, Ireland, Finland, Denmark, and Sweden. 

==See also==
 
* Molvanîa
* Elbonia
* Lower Slobbovia

==References==
 

==External links==
 
*  
*  
*  
*  
*   (Archive)

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 