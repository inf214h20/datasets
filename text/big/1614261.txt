The Adventures of Ichabod and Mr. Toad
{{Infobox film
| name = The Adventures of Ichabod and Mr. Toad
| image = ichabodposter.jpg
| caption = Original theatrical poster
| director = Jack Kinney Clyde Geronimi James Algar
| producer = Walt Disney Harry Reeves
| based on =     Pat OMalley Colin Campbell Claude Allister Leslie Denison Edmond Stevens The Rhythmaires
| narrator = Basil Rathbone Bing Crosby
| music = Oliver Wallace
| cinematography =
| editing = John O. Young Walt Disney Productions RKO Radio Pictures, Inc.
| released =  
| runtime = 68 minutes
| country = United States
| language = English
| budget =
}} animated Anthology package film Walt Disney RKO Radio Pictures. The film consists of two segments &ndash; the first is based on the 1908 childrens novel The Wind in the Willows by British author Kenneth Grahame, and the second is based on the 1820 short story "The Legend of Sleepy Hollow," called Ichabod Crane in the film, by American author Washington Irving.
 11th Walt Disney theatrical animated feature and is the last of the studios package film era of the 1940s, following Saludos Amigos, The Three Caballeros, Make Mine Music, Fun and Fancy Free, and Melody Time.

Beginning in 1955, the two portions of the film were separated, and televised as part of the Disneyland (series)|Disneyland television series. They were later marketed and sold separately on home video.

== Segments == framing device. The first segment is introduced and narrated by Basil Rathbone, and the second segment is introduced and narrated by Bing Crosby.

=== The Wind in the Willows === bookkeeper to help Toad keep his estate which is a source of pride in the community.
 water rat) motor car for the first time and becomes entranced by the new machine, taken over by "motor-mania."
 car theft. represents himself and calls his horse Cyril Proudbottom as his first witness. Cyril testifies that the car which Toad was accused of stealing had already been stolen by a gang of weasels. Toad had entered a tavern where the car was parked and offered to buy the car from the weasels. However, since Toad had no money, he instead offered to trade Toad Hall for the car. Toad then calls the bartender Mr. Winky as a witness to the agreement; however, when told by Toad to tell the court what actually happened, Winky falsely testifies that Toad had tried to sell him the stolen car. Toad is found guilty on the spot and sentenced to 20 years in the Tower of London. Toads friends make every effort to appeal his case, but to no avail.

That Christmas Eve, Cyril visits Toad in prison and helps him escape. Meanwhile, MacBadger discovers that Winky is the leader of the weasel gang, and that they have indeed taken over Toad Hall; Winky himself is in possession of the deed. Knowing that the deed bearing Toad and Winkys signature would prove Toads innocence, the four friends sneak into Toad Hall and take the document after a grueling chase around the estate.
 temporarily insane at the time of the transaction. At their New Year celebration, Ratty, Moley, and MacBadger toast their friend Toad whom they believe to be completely reformed. Just then, Toad and Cyril fly past in a Bristol Boxkite, showing that Toad has only discovered the new mania of airplanes.

=== Ichabod Crane === The Sketch Book with other stories, not as a single volume as pictured in the film.)
 Tarrytown that is renowned for its ghostly hauntings, to be the towns new schoolmaster. Despite his odd behavior and awkward appearance, Ichabod soon wins the hearts of the villages women. Brom Bones, the oafish town hero, does his best to bully Ichabod. However, he is very good at ignoring these taunts and continues to interact with the townspeople. Ichabod then falls in love with eighteen-year-old Katrina van Tassel, the beautiful daughter and only child of Baltus van Tassel, who is the richest man in the village. Despite falling in love with her, Ichabod secretly intends to take her familys money for himself. Brom, who is also in love with her, competes with the schoolmaster. Ichabod wins Katrina over at every opportunity, although unbeknownst to him, Katrina, who thinks Brom is too sure of himself, is only using Ichabod to make Brom jealous and very disappointed in him.
 toss salt soldier whose head was lost in battle and every Halloween rises from the grave to seek a new head. Brom claims to have encountered the ghost the previous year and that after fleeing across a nearby old wooden bridge, the ghost disappeared. While Katrina finds Broms song, and Ichabods reaction to it, humorous, Ichabod takes it seriously and becomes scared by it.
 old cemetery, cattails bumping on a log. Relieved, Ichabod begins to laugh with his horse. However, their laughter is cut short by the appearance of the real Headless Horseman riding a black horse (that is, suspiciously, identical to Broms horse). After the ghost gives chase, Ichabod, remembering Broms advice, crosses a covered bridge, which stops the ghosts pursuit. However, the horseman throws his flaming head, revealed to be a jack-o-lantern, at a screaming Ichabod.

The next morning, Ichabods hat is found at the bridge next to a shattered pumpkin, but Ichabod is nowhere to be seen. Sometime later, Brom takes Katrina as his bride. Rumors begin to spread that Ichabod is still alive, married a wealthy widow in a distant county with children who all look like him. However, the people of Sleepy Hollow insist that he was "spirited away" by the Headless Horseman.

== Cast ==
* Bing Crosby - Ichabod Crane, Brom Bones, Narrator (The Legend of Sleepy Hollow)
* Basil Rathbone - Narrator (The Wind in the Willows), Policeman
* Eric Blore - J. Thaddeus Toad Claude Allister - Ratty Colin Campbell - Moley 
* Campbell Grant - Angus MacBadger 
* Jack Mercer - Engine Driver 
* Mel Blanc - Engine Whistle
* Leslie Dennison - Judge, Weasel #1
* Edmond Stevens - Weasel #2
* J. Pat OMalley - Cyril Proudbottom, Mr. Winkie, Policeman, Unseen Paper Boy
* John McLeish - Prosecutor
* Pinto Colvig - Ichabod Crane screaming  (uncredited) 

== Production ==
  Snow White and the Seven Dwarfs, James Bodrero and Campbell Grant pitched to Walt Disney the idea of making a feature film of Kenneth Grahames 1908 childrens book The Wind in the Willows.    Bodrero and Grant felt that Wind in the Willows, with its anthropomorphised animals, could only be produced using animation. Disney was skeptical, however, and felt it would be "corny"  but acquired the rights in June that year. The film was intended to be a single narrative feature film with the title of the same name.

By early 1941, a basic script was complete,  along with a song written by Frank Churchill called "Were Merrily on Our Way". Although it was intended to be a low-budget film (much like Dumbo), Disney hired many animators from the prestigious Bambi (which was nearly complete) and production began in May that year. Within six months, 33 minutes of the film had been animated. Barrier, Michael (1999) Hollywood Cartoons, Oxford University Press, UK  However, the studios ability to produce full-length feature films had been drastically diminished, because World War II had drafted many of their animators into the military and had cut off their foreign release market. Thus, in October 1941, Disney put the production of Wind in the Willows on hold.  "The American Film Institute, catalog of motion pictures, Volume 1, Part 1, Feature films 1941-1950, The Adventures of Ichabod and Mr Toad" 

Then in December 1941, the United States became embroiled in the war after Pearl Harbor was attacked. The US government then asked the Disney studio to produce several propaganda films to help rally support for the war effort. During this time, much of Disneys feature output was made up of so-called "package films". Beginning with Saludos Amigos in 1942, Disney ceased making feature films with a single narrative due to the higher costs of such films, as well as the drain on the studios resources caused by the war.

Walt Disney and his artists felt that the animation of the cartoony anthropomorphized animals in Wind in the Willows was far below the standards of a Disney animated feature. They then decided that Wind in the Willows would be better off being part of a package film. 

Walt Disney started up production again in 1945. Many scenes in Wind in the Willows such as Toad buying several cars before his allowance is cut off, Rat and Mole visiting McBadger in a Sanatorium, Toad making an elaborate escape from his bedroom and Toad tricking a washer woman into helping him escape from prison had not yet been animated.  Therefore, in order to condense the story for the package film, Disney cut these scenes and completed the remaining animation. 

Under the title Three Fabulous Characters they tried to pair it up with Mickey and the Beanstalk and The Gremlins.  However, after The Gremlins failed to materialize, the title was changed to Two Fabulous Characters. Then Mickey and the Beanstalk was cut from Fabulous Characters in favour of pairing it with Bongo under the title Fun and Fancy Free which was eventually released in 1947.

Meanwhile, in December 1946, Disney started production on a new animated feature film, an adaptation of Washington Irvings "The Legend of Sleepy Hollow".   However, the filmmakers found that the running time for The Legend of Sleepy Hollow was not long enough to be a feature film and was more suited to be a package film.

Finally, in 1947, Walt Disney decided to pair The Wind in the Willows with The Legend of Sleepy Hollow under the new working title The Adventures of Ichabod and Mr. Toad.  Well-known celebrities Basil Rathbone and Bing Crosby were cast as narrators in order to provide mass audience appeal.

The Adventures of Ichabod and Mr. Toad was the last of the "package" films, and Disney returned to single-narrative features with 1950s Cinderella (1950 film)|Cinderella, and Disney would continue, despite the package feature, to produce independent shorts on a regular basis until the mid-1950s.

== Reception ==
The film has received positive reviews, garnering a 93% "Fresh" score among critics on Rotten Tomatoes. The Legend of Sleepy Hollow segment has received particular praise for being both effectively scary but still suitable for children and families. The film has gained quite a large fanbase, mostly people who grew up in the 1980s and 1990s when the film was shown on television during the autumn months.

M. Faust of Common Sense Media gave the film five out of five stars, writing, "Two classic stories told in the best Disney style". 

=== Accolades === Golden Globe Awards 
** Best Cinematography Color - Won

== Subsequent usage and home video release == The Reluctant Dragon  due to the fact that both cartoons are based on stories by author Kenneth Grahame. Todays Television Programs. (1955, August 3). Long Island Star-Journal, p. 25.  The Ichabod segment of the film had its television premiere during the following season of TVs Disneyland, on October 26, 1955, under the title The Legend of Sleepy Hollow. Tonight. . .dont miss Channel 7. (1955, October 26). The New York Times, p. 63.  Notably, for this airing of Sleepy Hollow and subsequent reruns, a new 14-minute animated prologue was added, recounting the life of Washington Irving, the storys author. This prologue has never been released on home video.

The Legend of Sleepy Hollow was released on its own to theaters as a 33-minute featurette in September 1963. Shorts Chart. (1963, September 23). BoxOffice, p. 10.  This was the same edit presented on the Disneyland television series, minus the 14-minute prologue and the Walt Disney live-action host segments. Similarly, in 1978, The Wind in the Willows segment of the original film was re-released to theaters under the new title The Madcap Adventures of Mr. Toad to accompany Disneys feature film Hot Lead and Cold Feet. Feature Reviews. (1978, July 31). BoxOffice, p. 77. 

The Legend of Sleepy Hollow had a subsequent television airing, in truncated form, as part of the 1982 TV special Disneys Halloween Treat.

Once it was split into two segments for airing on the Disneyland television series, The Adventures of Ichabod and Mr. Toad was not available for viewing in its original form for many years thereafter, but was instead screened as two individual items.  When first released on home video, the segments retained their names from the Disneyland series (The Legend of Sleepy Hollow and The Wind in the Willows, respectively), having taken their names from the original stories.

Some of the scenes were cut when the segments were split up for home video release. For example:

* The Wind in the Willows
** Part of the introduction was cut because of the new music added.
** The scene where MacBadger confronts the angry townspeople who are suing Toad.
** The newspaper scene regarding Toads disgrace was shortened by removing the newspaper articles of his friends attempts to reopen his case.
* The Legend of Sleepy Hollow
** The only thing that was cut was the introduction in the bookcases.

The Adventures of Ichabod and Mr. Toad received its first complete home video release in the UK in 1991 and in the US in 1992, when it was released by Walt Disney Home Video on laserdisc. A subsequent complete release on VHS followed in 1999 as the last title in the Walt Disney Masterpiece Collection line. In 2000, it appeared on DVD for the first time as part of the Walt Disney Gold Classic Collection line.

The 1963 theatrical version of The Legend of Sleepy Hollow was released on VHS as part of the   series.

"The Adventures of Ichabod and Mr. Toad" was released on Blu-ray, DVD, Digital HD and in a 2-Movie collection with Fun and Fancy Free on August 12, 2014.  It was also released as solely on Blu-ray, DVD and digital copy combo and a stand alone DVD exclusively to Walmart stores.  
 Toon Patrols designs were based on the weasels from the film.

== Merchandising ==
In 2000, the Walt Disney Gold Classic Collection, which is a collection of officially released Disney statue and pin merchandise (not to be confused with the Walt Disney Classics Collection, which was a video series of Disney animated features in the 1980s and early 1990s), released 3,500 limited edition statue sets of the two main Sleepy Hollow characters Ichabod Crane and the Headless Horseman. The figures were originally sold for $695 together as a set.   The pair have since been retired from the collection and its value has risen dramatically   each year.  

On August 29, 2010, Mr. Toad was released as an annual passholder vinylmation. On January 13, 2012, The Headless Horseman was the chaser in the Animation 2 vinylmation set.

== Directing animators == Frank Thomas (Mr. Toad, Rat, Mole, Cyril, Ichabod Crane, Brom Bones, Katrina Van Tassel )
* Ollie Johnston (Mr. Toad, Rat, Mole, Prosecutor, Ichabod Crane, Brom Bones, Katrina Van Tassel, Baltus Van Tassel)
* John Lounsbery (Ichabod Crane)
* Wolfgang Reitherman (The Weasels and Headless Horseman)
* Milt Kahl (MacBadger, Brom Bones)
* Ward Kimball (Mr. Toads escape from prison, Ichabod Crane)
* Eric Larson (Mr. Toad, Ichabod Crane)

== See also ==
* List of Disney theatrical animated features
* Mr. Toads Wild Ride
* Fun and Fancy Free

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 