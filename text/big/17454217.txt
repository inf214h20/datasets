Landscape in the Mist
{{Infobox film
| name           = Landscape in the Mist
| image          = Landscape in the mist.jpg
| caption        =
| writer         = Theo Angelopoulos Tonino Guerra
| starring       = Michalis Zeke Tania Palaiologou Stratos Tzortzoglou
| director       = Theo Angelopoulos
| producer       =
| cinematography =
| distributor    =
| released       =  
| runtime        = 127 minutes
| country        = Greece, Italy, France Greek
| budget         =
}} Greek film directed by Theo Angelopoulos.

==Plot==
The movie portrays the journey of two children in search of their father, whom they believe lives in Germany. On the way they meet many people, including a troupe of actors (a reference to Angelopoulos early movie The Travelling Players), and encounter dangers. Eventually, they cross a river thinking they have reached their hoped-for destination.

==Cast==
* Michalis Zeke as Alexandros
* Tania Palaiologou as Voula
* Stratos Tzortzoglou as Orestis	
* Vassilis Kolovos as truck driver		
* Ilias Logothetis as Seagull	
* Mihalis Giannatos as train station guardian	
* Toula Stathopoulou as woman in police station	
* Gerasimos Skiadaressis as soldier	
* Dimitris Kaberidis as uncle
* Tassos Palatzidis as train conductor

==Accolades== Best European Film 1989  Venice Film Festival Silver Lion 1988
* OCIC Award Venice Film Festival 1989  
* InterFilm Award, Berlin Film Festival 1988  

== References ==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 

 
 