Maruja in Hell
 
{{Infobox film
| name           = Maruja in Hell
| image          = 
| caption        = 
| director       = Francisco José Lombardi
| producer       = 
| writer         = Edgardo Russo José Watanabe
| starring       = Juan Carlos Alarcón
| music          = 
| cinematography = Pili Flores-Guerra
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Peru
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Plot==
A boy named Alejandro is trying to find a job. His friend (an owner of an auto-shop and the ringleader of a street gang) invites him to take part in a robbery of a small recycling enterprise run by an arrogant and domineering middle-aged woman named Carmen.

Carmen runs a business operation that consists of sorting out broken glassware in the backyard of her own house for a nearby glass factory. This work is done for her by a group of miserable, mentally retarded men kept in fear and obedience by a cruel black man who works as an overseer.

According to the plan worked out by the ringleader, the gang is supposed to capture a mentally retarded homeless man from the streets and then try to sell him to Carmen.  The role assigned to Alejandro is to bring the homeless man to Carmen. Alejandro is to notice where Carmen keeps her money so that the whole gang could later come back to the house and rob her. 

The gang captures and ties up a homeless man and Alejandro leads him to Carmens house. When Alejandro knocks on the door, the gate is opened by a beautiful girl named Maruja, the adopted daughter of Carmen. Immediately, Maruja and Alejandro are attracted to each other.  Meanwhile, Carmen insists that she needs "to test" the man to see if he is capable of doing the work. She orders Alejandro to come back a few days later to get paid for bringing the homeless man. However, Alejandro comes back the next day wanting to see Maruja. Maruja opens the door for him and invites him in. They go up to her room and make love.

Marujas room is on the second floor of the house. Alejandro accidentally discovers that he can see Carmens bedroom through a crack in the floor. Peeping through the crack he sees the black overseer having sexual intercourse with Carmen. Suddenly, the overseer takes out a knife and kills Carmen in her bed.  Then he takes out the cardboard box filled with Carmens money and goes out into the yard. Alejandro runs down the stairs and confronts him. A scuffle ensues during which Alejandro kills the overseer. Alejandro asks Maruja to take the money box and hide it.  As he is about to leave the house, the remaining gang members break through the gate and enter the yard. Their leader, upon seeing the corpses, realizes that he might get entangled in a serious crime. Prudently, he orders his gang to leave the place immediately. 

However, two of the gang members, overwhelmed by greed, come back and demand that Alejandro give the money to them. When he refuses, they beat him up until he almost dies. After that they try to rape Maruja. Barely alive, Alejandro crawls toward a cell where mentally retarded workers are kept at night and opens the door for them. The workers run out and attack the bandits, killing one of them by throwing him on a pile of broken glass, while the other gang member manages to escape. After burying the three corpses in the yard of the house the retarded workers one by one leave the house, enjoying their new-found  freedom. Alejandro and Maruja, with the box full of money, also leave the house.

==Cast==
* Juan Carlos Alarcón
* Oswaldo Fernández Manuel Rodríguez Elena Romero as Maruja
* Pablo Serra
* Elvira Travesí
* Julio Vega as Zambo Manuel
* Óscar Vega (actor)|Óscar Vega

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Peruvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 