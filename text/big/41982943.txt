Lego Marvel Super Heroes: Maximum Overload
 
{{Infobox film
| name     = Lego Marvel Super Heroes: Maximum Overload
| director = Greg Richardson
| writer   = Matt Wayne Laura Bailey Grey DeLisle Tom Kenny Dee Bradley Baker Barry Dennen Robin Atkin Downes
| music    = Asher Lenz Stephen Skratt
| editing  = Jeremy Montgomery LEGO
| released = November 5, 2013
| runtime  = 22 minutes
| country  = United States Canada Denmark English
}}
Lego Marvel Super Heroes: Maximum Overload (AKA Marvel Super Heroes: Maximum Overload) is a computer-animated Lego film based on Marvel Comics characters. It premiered online in five parts on November 5, 2013.

==Plot==
  Loki challenges Iron Fist, and many more heroes to face off against their supercharged enemies in an ultimate battle.

==Cast== Laura Bailey Black Widow
* Dee Bradley Baker as Venom (comics)|Venom, Chitauri Loki
* Drake Bell as Spider-Man Wolverine
* Iron Fist
* Grey DeLisle as Pepper Potts Mandarin
* Abomination
* Tom Kenny as Doctor Octopus Hot Dog Vendor
* Tony Matthews as Jogger
* Chi McBride as Nick Fury, Red Skull
* Adrian Pasdar as Iron Man
* Bumper Robinson as Falcon (comics)|Falcon, S.H.I.E.L.D. Sentry
* J. K. Simmons|J.K. Simmons as J. Jonah Jameson
* Roger Craig Smith as Captain America
* Fred Tatasciore as Hulk (comics)|Hulk, S.H.I.E.L.D. Sentry Thor

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 
 
 