Roman Candles (1920 film)
{{Infobox film
| name           = Roman Candles (aka: Yankee Doodle, Jr.)
| image          =
| caption        =
| director       = Jack Pratt
| producer       = Master Pictures
| writer         = W.S. Forstyth(story) Jack Pratt(co-scen.) W.S. Forsyth(co-scen.) Ralph Spence(titles)
| starring       = J. Frank Glendon Edward M. Kimball William Beckley
| editing        =
| distributor    = States Rights; 1920 Burnside; Mar-Apr 1922 (as Yankee Doodle, Jr.) Cinart July 4 1922 (as Yankee Doodle, Jr.)
| released       = September 13, 1920 April-March 1922;rerelease July 1922 rerelease
| runtime        = 7 reels in 1920 5 reels both 1922 rereleases
| country        = USA
| language       = Silent..English titles
}}
Roman Candles is a 1920 silent film drama directed by Jack Pratt and starring J. Frank Glendon and Edward M. Kimball. The film was released in 1920 as a seven reeler on States Rights basis. In 1922 two independent distributors rereleased it with a 5 reel running length. 

It is preserved in the Library of Congress and the BFI Film and Tv Institute, London.   


==Cast==
*J. Frank Glendon - John Arnold, Jr.
*Phalba Morgan - Senorita Zorra Gamorra
*Edward M. Kimball - John Arnold, Sr.
*Hector V. Sarno - The President (*as Victor Sarno)
*Sidney DAlbrook - The Secret Service Chief
*Jack Pratt - Mendoza, The Captain
*Mechtilde Price - ?
*Lola Smith - ?
*Bill Conant - ?
*Jack Waltemeyer - ?
*Teddy - A Dog (*Mack Sennetts Teddy)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 