Watch Out, We're Mad!
{{Infobox film
| name           = Watch Out, Were Mad!  (...altrimenti ci arrabbiamo!) 
| image          = Watch-out-were-mad-(dvd).jpg
| image_size     = 
| caption        = 
| director       = Marcello Fondato
| producer       = Mario Cecchi Gori
| writer         = Marcello Fondato Francesco Scardamaglia
| narrator       = 
| starring       = Terence Hill Bud Spencer Oliver Onions
| cinematography = Arturo Zavattini
| editing        = Sergio Montanari Alfonso Santacana
| distributor    =  1974
| runtime        = 98 minutes
| country        = Italy Spain Italian English English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1974 Italian Italian action comedy starring the comedy team of Terence Hill and Bud Spencer.  

==Plot== Puma dune buggy with yellow top. However, he does not count on "The Kid" (Hill) entering the competition, and he also has a talent for the sport—and an eye on that same dune buggy. During the race, each man battles furiously to the finish line. But in a surprise, they both end up tying for first place, and they are both awarded the dune buggy. They run from their cars and touch the buggy at the same time. Of course, sharing is out of the question, so they soon get to a serious discussion as to how it should be decided as to who gets the buggy. Finally they decide to bet it in a "beer and sausages" duel in a luna park pub, in which "the first one that blow up lose the car and pay the tab" .

The challenge is roughly interrupted by "Boss" (Sharp) henchmen, a building profiteer that wants to demolish the luna park for being able to replace it with a skyscraper. At the order of a henchman to "come out from the car, otherwise i will destroy this jalopy", Ben and Kid disobey trying to fleeing, but instead the buggy is rammed by another car, being destroyed.

Resoluted to reclaim a new buggy (same as the destroyed one), the two come in the Boss restaurant, and here they say "...otherwise well get mad!!".
After the two go away, the boss is disposed to buy a new buggy to compensate them, but is discouraged by the "Doctor" (Pleasance), a German-born Freudian psychologist: he believes that Ben and Kid are two spoiled children that think the boss is their father. To give in to their demand would be a bad psychological fallout. The Doctor exalts Boss wickedness, but he recommend to use it for something unsensed, unuseful.

==Cast==
* Terence Hill as Kid
* Bud Spencer as Ben John Sharp as the Boss
* Donald Pleasence as the Doctor
* Deogratias Huerta as Attila
* Luis Barbero as Jeremias
* Patty Shepard as Liza
* Manuel de Blas as Paganini
* Emilio Laguna as the conductor of the choir

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 
 