The Beales of Grey Gardens
 
{{Infobox Film
| name           = The Beales of Grey Gardens
| image          = Poster of the movie The Beales of Grey Gardens.jpg
| image_size     = 175px
| caption        = 
| director       = 
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2006
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Beales of Grey Gardens is a documentary film by Albert Maysles, David Maysles and Ian Markiewicz, released in 2006.
 documentary Grey Beale estate in East Hampton.

The Beales of Grey Gardens gives greater insight into the simultaneously contentious and loving relationship between the mother and daughter, as well as their relationship with the young caretaker, Jerry, their multitude of cats, and Lois Wright, a friend who briefly appeared in Grey Gardens at the celebration of "Big" Edies birthday.

It includes a dramatic scene of a small fire in the second floor hallway of the mansion, which explains the large hole in the wall shown in the first film.
 East Hampton claimed that she was schizophrenic, which she denied, saying "No Beales are schizophrenic!"
 Republican Party; the renovations done to the mansion following the raid; the title of the original documentary; and the loss of several boyfriends in World War II.
 Jacqueline Bouvier in 1953.

Also revealed is that "Big" Edie liked "Little" Edie to change costumes ten times a day. This explains "Little" Edies constantly changing wardrobe in the original Grey Gardens.

== Music == You Ought to Be in Pictures"
*"Lorraine Lorraine Lorree"
*"V.M.I. March" I Dream Too Much"
*"Spring Will Be a Little Late" Around the World in 80 Days"
*"Should I Be Sweet?"
*"Dont Ever Leave Me (song)|Dont Ever Leave Me"
*"If I Loved You"

== External links ==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 


 