Samraat (film)
 
{{Infobox film
 | name = Samraat
 | image = 
 | caption = DVD Cover
 | director = Mohan Segal
 | producer = Mohan Mohla
 | writer = 
 | dialogue = 
 | starring = Dharmendra Jeetendra Hema Malini Zeenat Aman Amjad Khan
 | music = Laxmikant-Pyarelal
 | lyrics = 
 | associate director = Mike Higgins
 | art director = 
 | choreographer = 
 | released = 
 | runtime = 
 | language = Hindi
 | budget = 
 | preceded_by = 
 | followed_by = 
 }}
 Hindi action thriller starring Dharmendra, Jeetendra, Hema Malini, Zeenat Aman and Amjad Khan. The story is about a sunken ship of gold and the quest to find it by all parties, good and bad.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kal Na Mana Parson Na Mana"
| Lata Mangeshkar
|-
| 2
| "Ankhon Ka Salam Lo"
| Mohammed Rafi, Manna Dey, Lata Mangeshkar
|-
| 3
| "Meri Jaan Tujhe Mere Hathon Marna"
| Asha Bhosle
|-
| 4
| "Meri Patli Kamar Men Hath Dal De"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Panja Chhakka Satta"
| Shailender Singh, Kishore Kumar, Asha Bhosle
|-
| 6
| "Upar Zameen Neeche Aasman"
| Kishore Kumar, Asha Bhosle
|}
==External links==
* 

 
 
 
 

 