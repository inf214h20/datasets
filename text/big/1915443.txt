Love 'em and Weep
{{Infobox film
  | name = Love em and Weep
  | image =Loveemweeptitle card27.jpg
  | caption =
  | director =  Fred Guiol
  | producer = Hal Roach
  | writer = Hal Roach   H.M. Walker (titles) Jimmie Finlayson
  | music = 
  | cinematography =
  | editing =
  | distributor = Pathé Exchange
  | released =  
  | runtime = 20 min.
  | language = Silent film English (Original intertitles)
  | country = United States
  | budget =
  }}
 silent comedy short film starring Mae Busch, Stan Laurel and Oliver Hardy prior to their official billing as the duo Laurel and Hardy. The team appeared in a total of 107 films between 1921 and 1951.
 
== Opening Title ==
Ancient Proverb—Every married man should have his fling—But be careful not to get flung too far.

==Plot== James Finlayson) threatens to expose their past, destroying both his marriage and career. He sends his aide (Stan Laurel) to keep her away from a dinner party he and his wife are hosting that evening.   

==Production==
In Love em and Weep Jimmy Finlayson stars as Titus Tillsbury, a successful businessman who is visited by a blackmailing old flame, played by Mae Busch, who later repeated her role in the remake. The mildly dumb business associate who brings disaster upon his hapless employer is played by Stan Laurel in both films.

Since Laurel and Hardy appear in the film, it is considered an early Laurel and Hardy film despite the fact that Hardys role is a bit part; they barely share any scenes in the film.
 Charlie Hall was to appear with Laurel and Hardy. He played Tillsburys butler, which Finlayson would play himself in the 1931 remake. 

Love em and Weep was filmed in January 1927 and released June 12 of that year by Pathé Exchange.

==Cast==
*Mae Busch as Old Flame
*Stan Laurel as Romaine Ricketts Jimmie Finlayson as Titus Tillsbury
*Oliver Hardy as Judge Chigger
*Charlotte Mineau as Mrs. Aggie Tillsbury
*Vivien Oakland as Mrs. Ricketts
*Ed Brandenburg as Waiter (uncredited) Charlie Hall as Tillsburys butler (uncredited)
*Gale Henry as Gossip (uncredited)
*May Wallace as Mrs. Chigger (uncredited)

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 