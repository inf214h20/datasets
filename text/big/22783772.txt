Prayanam (2009 film)
{{Infobox film
| name           = Prayanam
| image          = Prayanam.jpg
| writer         = Chaitanya Muddana
| starring       = Manoj Manchu Payal Ghosh Brahmanandam Aamir Tameem
| director       = Chandra Sekhar Yeleti
| producer       = Seeta Yeleti
| editing        = Mohan Rama Rao
| cinematography = Sarvesh Murari
| music          = Mahesh Shankar
| country        = India
| Distibutor     = Chaitanyamuddana
| released       =  
| language       = Telugu
| budget         =
}}
 Telugu film starring Manoj Manchu in the lead role; Payal Ghosh is female lead. The film was written and directed by Chandra Sekhar Yeleti and produced by Seeta Yeleti. The movie received positive reviews. The story is said revolve around Manoj and Payal Ghosh in an airport.

==Plot==
This is a story of a boy and girl who meet for the first time in an airport and fall in love before they board their flights. Dhruv is the boy and Harika is the girl. They are distinctive individuals. Dhruv is a kind of boy who loves to take chances in life. Harika is one who likes to make right choices in life. She is doing Masters in Lifestyles Design Academy in Malaysia. Right now she is on her way to India to meet a prospective groom whom her parents have chosen for her. 

Dhruv falls in love at first sight with Harika when he sees her at the airport. But he has only two hours to make her fall in love with him. The rest of the story is about how he succeeds.

==Cast==
* Manoj Manchu as Dhruv
* Payal Ghosh as Harika
* Brahmanandam as Satyanarayana Swamy
* Aamir Tameem as Raman
* Janardhan as Kailash
* Kalpika Ganesh as Moksha

==Filming==

Most of the movie takes place in the dubai airport.

==Critical reception==

Prayanam received positive reviews from critics.

==Soundtrack==
{{Infobox Album
| Name       = Prayanam
| Type       = Soundtrack
| Artist     = Mahesh Shankar
| Released   = 12 May 2009
}}

The soundtrack features three songs (two bit songs) composed by Mahesh Shankar with lyrics by Anantha Sreeram. The songs are available for free download at  .

Eleven-year-old Amruthavarshini was nominated for 57th Filmfare Award (South) in the Best Female Playback category (Telugu) for the song "Meghamaa".

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)
|-
| "YVW"      (5:36)
| Vishal Dadlani, Ranjit, Smita, Shalini Singh, Mynampati Sreeram Chandra
|-
| "Meghamaa"   (2:25)
| Amruthavashini
|-
| "Nuvvu Entha"   (2:20)
| Smita, Mahesh Shankar, Mynampati Sreeram Chandra
|}

 

 
 