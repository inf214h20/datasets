I.Q. (film)
{{Infobox film
| name           = I.Q.
| image          = Iq film poster.jpg
| caption        = Theatrical release poster
| director       = Fred Schepisi
| producer       = Fred Schepisi Carol Baum Scott Rudin Neil A. Machlis
| writer         = Andy Breckman
| starring       = Tim Robbins Meg Ryan Walter Matthau
| music          = Jerry Goldsmith Ian Baker
| editing        = Jill Bilcock
| distributor    = Paramount Pictures
| released       = December 25, 1994
| runtime        = 95 minutes
| country        = United States
| language       = English German
| budget         = $25 million
| gross          = $26,381,221
}}
I.Q. is a 1994 American romantic comedy film directed by Fred Schepisi and starring Tim Robbins, Meg Ryan, and Walter Matthau. The original music score was composed by Jerry Goldsmith. The film centers on a mechanic and a Princeton doctoral candidate who fall in love, thanks to the candidates uncle, Albert Einstein.

==Plot==
An amiable garage mechanic, Ed Walters (Tim Robbins), meets Catherine Boyd (Meg Ryan), a brilliant Princeton University mathematics doctoral candidate, as she comes into the garage, accompanied by her stiff and fussy English fiancé, experimental psychology professor James Moreland (Stephen Fry). There is an immediate connection, but she refuses to acknowledge it. Finding a watch she left at the garage, Ed travels to her address and finds himself face to face with Albert Einstein (Walter Matthau), who is Catherines uncle.   

Albert—portrayed as a fun-loving genius—and his mischievous friends, fellow scientists Nathan Liebknecht (Joseph Maher), Kurt Gödel (Lou Jacobi), and Boris Podolsky (Gene Saks), see Ed as someone who would be better suited for Catherine. The four of them try to help Ed look and sound like a scientist (i.e., a "child prodigy|wunderkind" in physics), while at the same time trying to convince Catherine that life is not all about the mind, but is also about the heart. Einstein sees bringing Ed and Catherine together as his most enduring legacy to his niece, because he realizes that he wont be available to help her for very much longer.

Catherine eventually sees through the ruse, but falls for Ed anyway. A smiling Albert Einstein uses a small telescope to spy happily on the two young moonstruck lovers as they take delight in each others company.

==Cast==
* Tim Robbins as Ed Walters
* Meg Ryan as Catherine Boyd
* Walter Matthau as Albert Einstein
* Lou Jacobi as Kurt Gödel
* Gene Saks as Boris Podolsky
* Joseph Maher as Nathan Liebknecht
* Stephen Fry as James Moreland
* Tony Shalhoub as Bob Rosetti
* Frank Whaley as Frank
* Charles Durning as Louis Bamberger
* Keene Curtis as Dwight D. Eisenhower
* Alice Playten as Gretchen
* Greg Germann as Bill Riley, The Times|Times reporter

==Dramatic alterations==
For dramatic reasons, I.Q. fictionalizes the lives of certain real people. Albert Einstein did not have a niece by the name of Catherine Boyd. Kurt Gödel was famously shy and reclusive,  unlike his fictional counterpart in this film. The movie gives the impression that Einstein and his friends are all around the same age, when in fact, they were between 17 and 30 years younger than Einstein. The real-life Louis Bamberger died in 1944, before the films set period. The characters in the film also listen to Little Richards Tutti Frutti (song)|"Tutti-Fruitti," which was released in November 1955, while Albert Einstein died in April of that year.

==Production==
The director, Fred Schepisi, later said that, while he liked the film, it was not what it could have been:
 "The problem was there were two other producers, there was a studio and there was Tim Robbins and they were all contributing, and Tim Robbins was being difficult because he said in the 90s nobody would like a character who has a woman fall in love with him because of a lie. Thats the whole premise of the film. And its all right for him to know that and believe it, but he should spend the whole time trying to say, "Hey, Im lying to you," and be constantly frustrated. Because of that attitude, he pulled the film this way, he pulled it that way while we were writing and it just felt messy. And nobody ever understood the value of those four scientists, and I like the cast that I had, but the other three scientists apart from Walter Matthau were originally going to be Peter Ustinov, Barry Humphries and John Cleese. I wanted them all the way through, but nobody understood how strong they would be. Nobody understood that with a garage and the scientists and this other guy, if you could just stay within that world, if you kept your two lovers together all the time under pressure and you do lots of silly things - there were a couple of wonderfully silly things when they were trying to prove his theory and they kept blowing things up - it had that whimsy about it that would have kept the lovers together and under tension. If they want subplots, they up the stakes and all this formulaic crap - and thats the problem."  

==Release and reception==
I.Q. opened in theaters on Christmas Day. It grossed $3,131,201 during its opening weekend (12/25-27), ranking #8 in opening-weekend sales for the weekend.  By the time the film closed, it had made $26,381,221 worldwide. 

The film received mixed reviews from critics, as I.Q. holds a 46% rating on Rotten Tomatoes.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 