Beck – Annonsmannen
{{Infobox film
| name           = Beck – Annonsmannen
| image          = Beck 14 - Annonsmannen.jpg
| image_size     =
| alt            = 
| caption        = Swedish DVD-cover
| director       = Daniel Lind Lagerlöf
| producer       = Lars Blomgren Börje Hansson
| writer         = Cilla Börjlind Rolf Börjlind
| narrator       =
| starring       = Peter Haber Mikael Persbrandt Malin Birgerson
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2002
| runtime        = 88 min
| country        = Sweden Swedish
| budget         =
| gross          =
}}
Beck – Annonsmannen is a 2002 film about the Swedish police detective Martin Beck directed by Daniel Lind Lagerlöf.

==Selected cast==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Malin Birgerson as Alice Levander
*Marie Göranzon as Margareta Oberg
*Hanns Zischler as Josef Hillman
*Ingvar Hirdwall as Martin Becks neighbour
*Mårten Klingberg as Nick
*Jimmy Endeley as Robban
*Peter Hüttner as Oljelund
*Rebecka Hemse as Inger (Martin Becks daughter)
*Neil Bourguiba as Wilhelm (Ingers son)
*Andreas Kundler as Jesper Wennquist
*Eva Fritjofsson as Elin (Jespers mother)
*Livia Millhagen as Malin Tavast
*Magnus Krepper as Bengt Tavast
*Angela Kovacs as Doctor
*Sten Elfström as Rune Fjällgård Tommy Andersson as Svanborg

==References==
*{{cite web | title=Beck - Annonsmannen (2002) |
url=http://www.sfi.se/sv/svensk-film/Filmdatabasen/?type=MOVIE&itemid=58015
| publisher=Swedish Film Institute | accessdate=2009-07-14}}

==External links==
* 

 

 
 
 
 
 


 
 