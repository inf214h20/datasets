Tuck Everlasting (2002 film)
 
{{Infobox film
| name = Tuck Everlasting
| image = Tuck Everlasting (2002 film) poster.jpg
| image_size =
| caption = Theatrical release poster 
| director = Jay Russell
| producer = Marc Abraham Jane Startz Thomas Bliss|
| writer = Jeffrey Lieber James V. Hart Natalie Babbitt  (Novel)
| narrator = Elisabeth Shue Jonathan Jackson Scott Bairstow William Hurt  William Ross
| cinematography = James L. Carter Jay Cassidy
| studio = Walt Disney Pictures Scholastic Entertainment Buena Vista Pictures
| released = October 11, 2002 (US) March 20, 2003 (Australia) August 1, 2004 (UK) 
| runtime = 96 minutes
| country = United States
| language = English
| budget = $15 million 
| gross = $19,344,615 
}}
 fantasy Family family romantic of the same title by Natalie Babbitt published in 1975. The Walt Disney Pictures release was directed by Jay Russell.

== Plot ==
The plot revolves around a 15-year-old girl named Winnie Foster, who is from a restrictive upper-class family. One day she runs away into the forest and meets a boy named Jesse Tuck, drinking from a spring. She is then kidnapped by his elder brother, Miles. She soon falls in love with Jesse and later learns that the Tucks cant age or be injured due to drinking water from a magic spring around a hundred years ago and that they kidnapped her in order to hide the secret. They say to her that living forever is more painful than it sounds and believe that giving away the secret of the spring will lead to abomination of Gods laws, and fellow humans suffering similar pain as them.

Meanwhile a suspicious man in a yellow suit befriends the Fosters over time while she is gone. He spies on the Tucks and its revealed that he desires the spring so that he can sell the water to the citizens for profit. When he is trustworthy to the Fosters, he makes a deal where if he agrees to save her from the Tucks and return her to her family, he will get the forest and therefore with it the spring. He goes to the Tucks and reveals his intents to get the forest and therefore the spring. He orders them to tell him where the spring is; when they refuse and try to deny any knowledge about the spring he threatens Winnie with a pistol. He calls their bluff by shooting Jesse and exposing his youth; but in return Jesses mother, Mae, violently beats him with the rear end of a rifle. The hit is so massive that he dies instantly. The constable manages to see the attack and arrest Mae for killing the man in the yellow suit. She and her family is sentenced to be hanged for kidnapping Winnie and murdering the man with the yellow suit. This would expose their immortality.

Winnie helps break the Tucks out of jail and takes their place in their cell. When she is found the next day, the police assume that the Tucks escaped and put her in there.

The Tucks decide to leave the area and Jesse invites Winnie to join them, but Angus warns her that they will be hunted down for as long as they can. She is forced to decide whether to drink from the spring and live forever, or live a mortal existence. She decides that despite the love she and Jesse share, her presence will only endanger them and she chooses to stay behind. After many years, Jesse returns to the tree where the spring used to be, and it is revealed that Winnie chose to never drink from it and lived a long and charitable life.

== Cast ==
* Alexis Bledel as Winnie Foster Jonathan Jackson as Jesse Tuck
* Ben Kingsley as The Man in the Yellow Suit
* William Hurt as Angus Tuck
* Sissy Spacek as Mae Tuck
* Scott Bairstow as Miles Tuck
* Amy Irving as Mrs. Foster
* Victor Garber as Robert Foster
* Julia Hart as Sally Hannaway
* Noami Kline as Beatrice Ruston
* Robert Luis as Night Deputy (as Robert Logan)

== Differences between the movie and the book ==
{| class="wikitable"
|- In the book...|!! In the movie...
|- 
| The books starts in the first week of August. || The movie starts as soon as school is out.
|-
| Winnie is 10. || She is 15.
|-
| The book is set in 1880. || The movie is set in 1914.
|-
| Winnies grave says 1870-1948 (died 78 years). || It says 1899-1999 (died 100 years).
|-
| Winnie runs away because shes tired of being cooped up. || She runs away because shes going to be sent to a boarding school.
|-
| To save Mae, the Tucks remove the bars of a window of the jail, and Winnie switches places with her. || Winnie tells the prison guard that the people who kidnapped her are back to get her. He runs outside with a shotgun to face them. He shoots them, but runs away when he sees they cant die. Meanwhile, Winnie grabs his keys and unlocks Mae and Angus cell.
|-
| Mae is sent to jail to be hanged. || Angus is imprisoned and Mae is to be sent to hanged.
|-
| Mae and Angus visit Winnies grave (in 1950). They return to Treegap riding their old horse-drawn wagon. || Only Jesse comes back (in or around 2002). He returns riding a motorcycle.
|-
| Mae, Jesse, and Miles "kidnap" Winnie when they fear that their secret will get out. They take her to their cabin north of Treegap || Only Miles does.
|- 
| The Tucks run off in a fierce thunderstorm. || They escape in a carriage.
|-
| Jesse gives Winnie a bottle filled with water from the spring and tells her to drink it when she turns 17. || He does not give Winnie a bottle, but she contemplates drinking from the spring after the Tucks leave.
|-  The Tucks house is deep in the woods and a faded red.|| It is a brown log cabin.
|}

== References ==
 

== External links ==
*   
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 