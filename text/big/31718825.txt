Fine Manners
{{Infobox film
| name           = Fine Manners
| image          = Fine Maners lobby card.jpg
| image size     = 
| alt            = 
| caption        = lobby card Richard Rosson Lewis Milestone
| producer       = William LeBaron
| screenplay     = J. Clarkson Miller James A. Creelman Frank Vreiland Eugene OBrien
| music          =  George Webber
| editing        = 
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes 7 reel#Motion picture terminology|reels, 35mm
| country        = United States
| language       = English intertitles
| budget         = 
| gross          =
}} silent comedy film directed initially by Lewis Milestone {{cite news
|url=http://movies.nytimes.com/movie/91455/Fine-Manners/details
|title=Fine Manners (1926)
|work=The New York Times Richard Rosson for Famous Players-Lasky/Paramount Pictures. After an argument with actress Gloria Swanson, director Milestone walked off the project, causing the film to be completed by Rosson, {{cite book
|last=Joseph R. Millichap
|title=Lewis Milestone
|publisher=Twayne Publishers
|date=1981
|series=Twaynes filmmakers series
|pages=15, 31
|isbn=0-8057-9281-3
|url=http://books.google.com/books?ei=PTzHTd2oE4P4sAOLzuTqAQ&ct=result&id=sy8eAAAAMAAJ&dq=%22Lewis+Milestone%22%2C+%22Fine+Manners%22&q=%22Fine+Manners%22}}  who had picked up directorial tricks while working as an assistant director to Allan Dwan. {{cite book
|last=Lawrence J. Quirk
|title=The films of Gloria Swanson
|publisher=Citadel Press
|date=1984
|pages=202
|isbn=0-8065-0874-4
|url=http://books.google.com/books?ei=eYnHTeyXGZPQsAOohpHsAQ&ct=result&id=nn1ZAAAAMAAJ&dq=%22Fine+Manners%22%2C+%22Richard+Rosson%22&q=%22Richard+Rosson%22}}  The success of the film, being Rossons first directorial effort since he co-directed Her Fathers Keeper in 1917 with his brother Arthur Rosson,  won him a long-term contract with Famous Players-Lasky. {{cite news
|url=http://pqasb.pqarchiver.com/latimes/access/362441332.html?dids=362441332:362441332&FMT=ABS&FMTS=ABS:AI&type=historic&date=Sep+28%2C+1926&author=&pub=Los+Angeles+Times&desc=TONY+WILL+BE+IN+IT&pqatl=google
|title=Tony will be in it
|last=Grace Kingsley
|date=September 28, 1926
|work=Los Angeles Times
|accessdate=9 May 2011}}  

==Plot== chorus girl Eugene OBrien), who is posing as a writer while "slumming" in the city. Finding her manner quite refreshing compared to the women he usually meets in his circle, he falls in love with her and confesses his wealth. After she agrees to marriage, he leaves for a six-month tour of South America, and Orchid takes a course in "fine manners" to better prepare herself for Brians world. She becomes too polished, however, and when asked by Brian to marry him upon his return, is happy to become herself again.

==Cast==
* Gloria Swanson as Orchid Murphy  Eugene OBrien as Brian Alden 
* Helen Dunbar as Aunt Agatha 
* Roland Drew as Buddy Murphy
* John Miltern as Courtney Adams 
* Jack La Rue as New Years Eve Celebrant  
* Ivan Lebedeff as Prince 

==Critical reception==
Berkeley Daily Gazette wrote that in her first time in the role of a burlesque chorus girl, Gloria Swanson is "better than ever" and has "added another interesting screen portrayal to her long list of successes." {{cite news
|url=http://news.google.com/newspapers?id=yQ0iAAAAIBAJ&sjid=36YFAAAAIBAJ&pg=5326,5680029&dq=fine-manners+gloria-swanson&hl=en
|title=Gloria Swanson Finds New Field
|date=September 13, 1926
|work=Berkeley Daily Gazette
|publisher=Google News Archive
|accessdate=9 May 2011}}   Miami News called the film "a most laughable comedy" and reported "Critics say this is Glorias triumph". {{cite news
|url=http://news.google.com/newspapers?id=Yz8uAAAAIBAJ&sjid=SNgFAAAAIBAJ&pg=4527,1398378&dq=fine-manners+gloria-swanson&hl=en
|title=Fine Manners
|date=July 3, 1927
|work=Miami News
|publisher=Google News Archive
|pages=23
|accessdate=9 May 2011}}   St. Petersburg Times wote that Fine Manners stands head and shoulders above anythng Gloria has done for the past year," and note that the story was written specifically for her. They wrote that the film "will prove to be the stars most popular vehicle." {{cite news
|url=http://news.google.com/newspapers?id=wh1PAAAAIBAJ&sjid=kkwDAAAAIBAJ&pg=7267,5751776&dq=fine-manners+gloria-swanson&hl=en
|title=Gloria Swanson Stars In Photo Booked At Patio
|date=August 21, 1927
|work=St. Petersburg Times
|publisher=Google News Archive
|pages=10
|accessdate=9 May 2011}} 

Conversely, The New York Times noted  that Fine Maners was reminiscent of George Bernard Shaws play, Pygmalion (play)|Pygmalion, writing "The photoplay has been constructed with meticulous attention to the edicts of the movie school of conventionalities; true characterization, intrigue and subtlety are conspicuously absent. Still, the idea of introducing a chorus girl from a burlesque show and having her try valiantly to grasp the ways of a less demonstrative society, does bring to mind Shaws cockney heroine."  They noted that writers underscore the differences between the societal ranks of the two protagonists by emphasizing Orchids ignorance of social amenities and by her being assigned a common name.  While granting that there are scenes in which the cinematography is clever, they made note that the story itself is not very absorbing. {{cite news
|url=http://movies.nytimes.com/movie/review?res=9D00E2DC173BE233A25753C3A96E9C946795D6CF
|title=review: Fine Manners (1926)
|last=Mordaunt Hall
|date=August 30, 1926
|work=The New York Times
|accessdate=9 May 2011}} 

==Preservation==
The film has survived the decades and is preserved in several archive houses such as George Eastman House, The Library of Congress and the Museum of Modern Art.  



==References==
 

==External links==
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 