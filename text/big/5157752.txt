Annavaram (film)
 
 
 
{{Infobox film
| name = Annavaram
| image = Annavaram poster.jpg
| writer = Perarasu Abburi Ravi Sandhya Asin Thottumkal
| director = Bhimaneni Srinivasa Rao
| producer = Paras Jain N.V. Prasad Dil Raju Supergood films Usha Pictures
| released = 29 December 2006
| runtime = 172 minutes
| editing = Gowtham Raju
| cinematography = Sethu Sriram
| country = India
| language = Telugu
| music = Ramana Gogula
| budget =
| gross =   (3weeks)  
}}
 Tollywood film directed by Bhimaneni Srinivasa Rao (of Suswagatham fame), and starring Pawan Kalyan, Sandhya (actress)|Sandhya, and Asin Thottumkal|Asin. Mega Supergood Films produced the film. The film, released on 29 December 2006, and grossed  230&nbsp;million at the box office in three weeks.  {{cite web|url=http://www.indiaglitz.com/channels/telugu/article/28296.html|title=Annavaram gets record collections
|publisher=indiaglitz |date=6 January 2007|accessdate=2 October 2012}}  This film also rapidly increased the carrier graph of Asin.The soundtrack of the film, released on 14 December 2006, also received a good response. {{Cite web| title=greatandhra.com| work= Pawan-Gogula combo makes Annavaram a prized album |url= http://www.greatandhra.com/movies/news/dec2006/pawan_gogula.php archiveurl = archivedate = 3 January 2007}}  It is a remake of Thirupaachi starring Joseph Vijay.

==Plot==
 
Annavaram (Pawan Kalyan) has a lot of attachment to his sister Varam (Sandhya (actress)|Sandhya). He wants to see his sister trouble free even after marriage and hence wants to get her married to a city dweller who has all the facilities like a mixie, grinder, gas stove and running water. True to his taste, Annavaram marries Varam to a man (Shiva Balaji) who stays in Hyderabad. In order to leave her at her husbands place, Annavaram drops Varam in Hyderabad, India|Hyderabad. Aishwarya (Asin Thottumkal) is the neighbour of Varams husband who is chirpy and moves very close with everyone without any inhibition. During his stay, Annavaram finds that the city is full of goons and his brother-in-law pacifies him not to take everything to head and one should have the adjustment mentality to stay in the city.
 Venu Madhav). After a few months, Varam turns pregnant and Pandus brother Gutkha Pandu threatens Varams husband and takes away the canteen being run by him in a college. Varam is frightened by the attack. Meanwhile, Narasimha gets Visa to fly to Dubai to work there. To see him off at the airport, Annavaram reaches Hyderabad again. Narasimha, Annavaram, Varam and her husband go to Golconda to see the fort, where Narasimha is killed by Ganga. Varam and her husband reach the village to take part in the funeral of Narasimha. Annavaram detains his sister and brother in law in the village, but he leaves the village stating that he got a job. He reaches Hyderabad. He dares Tapas Balu and kills 20 of his men along with a corrupt police officer (Ravi Babu). He even calls up the ACP J.D. Yadav (Nagendra Babu) that he would do the duty of the police as they were handcuffed with political pressures and cant take action against the goons. At this juncture, Annavaram meets his old friend Sairam (Brahmaji) and with his help gets the details of all the goons and decide to eliminate all of them. To bail out from the crisis, Tapas Balu kidnaps ACPs daughter Sanjana. Annavaram attacks Balu and kills him and saves her. Then he targets Gangas brother Pandu and kills him. Next is the turn of Ganga. Like this, he eliminates all the goons and makes Hyderabad a clean city sans any goondas by the time Varam delivers a baby boy. The film ends on a happy note with the union of Annavaram and Aishwarya.

==Cast==
* Pawan Kalyan as ... Annavaram Sandhya as ... Varam
* Asin Thottumkal as ... Aishwarya
* Ashish Vidyarthi as ... Tappas Balu Lal as ... Ganga
* Nagendra Babu as ... ACP Venu Madhav as ... Narasimha
* Bramhaji as ... Sairam
* L.B. Sriram Hema

==Crew==
* Director: Bhimaneni Srinivasa Rao
* Screenplay: Bhimaneni Srinivasa Rao
* Dialogue: Abburi Ravi
* Story: Perarasu
* Producer: N.V. Prasad
* Music: Ramana Gogula
* Background Music: Ramana Gogula
* Editing: Gowtham Raju
* Costume Designer: Renu Desai
* Art Director: Anand Sai Vijayan
* Cinematography: Sethu Sriram Kalyan
* Chandrabose and Masterji

==Music==
{{Infobox album|  
 Name = Annavaram |
 Type = Album |
 Artist = Ramana Gogula |
 Cover =|
 Released =  14 December 2006 (India)|
 Recorded = | Feature film soundtrack |
 Length = |
 Label =  Aditya Music |
 Producer = Ramana Gogula |
 Reviews = |
 Last album = Chinnodu (2006) |
 This album = Annavaram (2006) |
 Next album = Yogi (2007 film)|Yogi (2007) |
}}
Ramana Gogula composed music for the film. The album was launched in stores on 14 December 2006. The film has five songs composed by:
* "Raakshasa Raajyam" - Shankar Mahadevan
* "Lucia" - Ramana Gogula Tippu
* "Neevalle Neevalle" - Kalyani Mano & Ganga

==References==
 

==External links==
*  

 

 
 
 
 
 