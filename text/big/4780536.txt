Thiruvilaiyadal
 
 
 
{{Infobox film
| name     = Thiruvilayadal
| image    = Thiruvilayadal Sivaji.jpg
| caption  = Theatrical release poster
| director       = A. P. Nagarajan
| writer         = A. P. Nagarajan Savitri K. B. Sundarambal
| producer       = A. P. Nagarajan
| cinematography = K. S. Prasad
| editing        = Rajan T. R. Natarajan
| studio         = Sri Vijayalakshmi Pictures
| distributor    = Sri Vijayalakshmi Pictures
| released       = 31 July 1965 
| runtime  = 154 mins Tamil
| country = India
| music =  K. V. Mahadevan
| budget = 
}} Tamil Hindu devotional mythological Savitri in the lead roles with R. Muthuraman, Nagesh, Manorama (Tamil actress)|Manorama, K. B. Sundarambal, T. R. Mahalingam (actor)|T. R. Mahalingam and T. S. Balaiah playing pivotal roles. The films soundtrack and score were composed by K. V. Mahadevan.
 epic stories Lord Shiva appears on Earth in various disguises to test his devotees. Out of the 64 stories, four are depicted in the film. The first being that of the tale of Dharumi (Nagesh). The second being that of Dhatchayini (Sati). The third is where Parvati is born as a fisherwoman and how Shiva retrieves and remarries her by disguising as a fisherman. The last being that of Banabhathirar (T. R. Mahalingam). The soundtrack received positive reception and songs like "Pazham Neeyappa Gnaana Pazham", "Indroru Naal Pothuma", "Isai Thamizh Nee Seidha", "Paattum Naane" became evergreen songs. The length of the film was  .
 Kannada as Shiva Leela Vilasa and was the first Tamil film to be dubbed into Kannada after ten years.

== Plot == Lord Shiva gives a sacred fruit brought by sage Narada to his elder son, Ganesha for outsmarting his younger brother Muruga in a competition for winning the fruit. Angered by his fathers decision, Muruga goes to the hill abode of Palani in the guise of a hermit. Goddess Parvathi comes and narrates four of the divine games of Shiva to calm Muruga down.

The first story tells of the opening of Shivas third eye when he visits Madurai, the capital city of the Pandya Kingdom, ruled at that time by Shenbagapandian. Shenbagapandian wants to find the answer to a question posed by his wife (whether the fragrance from a womans hair is natural or caused by the cosmetics she uses on her hair) and announces a reward of 1000&nbsp;gold coins to anyone who could come up with the answer. A poor poet named Dharumi desperately wants the reward and starts breaking down in the Meenakshi Amman Temple. Shiva, who hears his cries, takes the form of a poet and gives Dharumi a poem containing the answer. Overjoyed, Dharumi takes it to the court and recites the poem, but the ego-filled Tamil poet Nakkeerar claims the poems meaning to be incorrect. After arguing with Nakkeerar about the poems accuracy, Shiva burns him into ashes. Later, he revives Nakkeerar, saying that he only wanted to test his knowledge. Nakkeerar requests the king to give the reward to Dharumi.
 Dhatchayini against the will of her father Daksha|Dhatchan. Dhatchan also performs a Mahayagna without inviting his son-in-law. Dhatchayini asks Shivas permission to go to the ceremony but Shiva refuses to let her go as he feels no good will come out of it. Dhatchayini disobeys him and goes there, only to be insulted by Dhatchan. Dhatchayini curses her father and returns to Shiva to find him angry at her for disobeying him. Dhatchayini asserts that they are one and without her, there is no Shiva. Shiva refuses to agree with her and burns her to ashes trying to prove her wrong and performs his Tandava, which is noticed by the Deva (Hinduism)|Devas, who pacify Shiva. Shiva decides to restore Dhatchayini to life.

The third story describes Parvathi being banished by Shiva when her attention wavers for a moment while listening to Shiva, who was explaining the essence of the Vedas to her. Parvathi, now born as Kayarkanni, is the daughter of a fisherman. When playing with her friends, a strange fisherman (Shiva) approaches and flirts with her, despite her disapproval. The fishermen often face problems due to a giant shark that disrupts their way of life. Shiva asserts that he alone can defeat the shark. After a long battle, Shiva kills the shark (which is actually Nandi (bull)|Nandi) and remarries Parvathi.

The last tale narrated by Parvathi is when Shiva takes the form of a firewood vendor. Hemanatha Bhagavathar, a skilled singer, tries to conquer the Pandya Kingdom when he challenges the kingdoms musicians. The Kings minister advises him to seek the help of Banabathirar, a devotional singer, to challenge Hemanatha Bhagavathar. When all musicians reject the competition, the King orders Banabathirar to compete against Hemanatha Bhagavathar. Knowing that he cannot win, the troubled Banabathirar prays to Shiva. Shiva, in the form of a firewood vendor, shows up outside Hemanatha Bhagavathars house and shatters his arrogance by singing the song, "Paattum Naane". When Hemanatha Bhagavathar comes to know that the vendor was Banabathirars student, he gets embarrassed and leaves the kingdom that very night leaving a note to Banabathirar informing him of his departure. Shiva gives the letter to Banabathirar and reveals his true identity to him. Banabathirar thanks Shiva for his timely help.

After listening to these stories, Murugas rage finally subsides and he reconciles with his family.

== Cast ==
{{quote box Sivaji (Ganesan) walked in and wanted to see the "Dharumi" piece. He did not notice me in the dark sound engineers room. He watched it once and then wanted to see it again – by this time I was sure that my scene, especially the solo lamenting, would be axed. To my astonishment, Sivaji turned and said, Do not remove a single foot from this episode as well as the episode featuring T. S. Balaiah. These will be the highlights of the film. This is my opinion, but as the director, you have the final say. Whatever dubbing additions have to be done, get that fellow (Nagesh), lock him up in the studio and dont let him run away till he completes it to your satisfaction. He has done outstanding work. Such was his generosity to his fellow actors."
| source =&nbsp;– Nagesh, as he was quoted saying in his autobiography. 
| align = right
| width = 35%
}}
 
;Lead actors
* Sivaji Ganesan as Lord Shiva Savitri as Parvathi (called by the names Sakthi, Datchayini and Kayarkanni in the film.)
* K. B. Sundarambal as Avvaiyar

;Male supporting actors
* T. S. Balaiah as Hemanatha Bhagavathar
* R. Muthuraman as Shenbaga Pandiyan
* Nagesh as Dharumi
* O. A. K. Thevar as King Dhatchan
* A. Karunanithi as Ponna/Sovai
* T. R. Mahalingam (actor)|T. R. Mahalingam as Banabhathirar
* K. Sarangkapani
* A. P. Nagarajan as Nakkeeran
* E. R. Sahadevan
* P. D. Sambandam
* T. N. Sivadhanu
* S. V. Sahasranamam as Kayarkannis father

;Female supproting actors
* Devika as the wife of Shenbaga Pandiyan Manorama as Ponni/Kayarkannis friend

<!--==Production==
http://archive.tehelka.com/story_main3.asp?filename=hub052204Mother.asp
http://www.thehindu.com/todays-paper/tp-features/tp-fridayreview/of-performing-arts/article567162.ece
http://www.vikatan.com/new/article.php?module=magazine&aid=626
http://www.maalaimalar.com/2012/05/10180443/thiruvilaiyaal-film-nagesh-act.html
http://www.thehindu.com/todays-paper/tp-features/tp-cinemaplus/an-interesting-nugget/article3875647.ece
K. B. Sundarambal was chosen to play Avvaiyyar, reprising her role from the 1953 film of the same name. -->

== Music ==
Music is composed by K. V. Mahadevan, with lyrics by Kannadasan. 
{{Track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      =
| title1          = Pazham Neeyappa Gnaana Pazham
| extra1          = K. B. Sundarambal
| length1         =
| title2          = Indroru Naal Pothuma
| extra2          = M. Balamuralikrishna
| length2         =
| title3          = Isai Thamizh Nee Seidha
| extra3          = T. R. Mahalingam (actor)|T. R. Mahalingam
| length3         =
| title4          = Paarthal Pasumaram
| extra4          = T.M. Soundararajan
| length4         =
| title5          = Paattum Naane
| extra5          = T.M. Soundararajan
| length5         =
| title6          = Podhigai Malai Uchieley
| extra6          = P. B. Sreenivas, S. Janaki
| length6         =
| title7          = Ondraanavan Uruvil
| extra7          = K. B. Sundarambal
| length7         =
| title8          = Illadha Thondrillai
| extra8          = T. R. Mahalingam (actor)|T. R. Mahalingam
| length8         =
| title9          = Vaasi Vaasi
| extra9          = K.B. Sundarambal
| length9         =
| title10         = Om Namasivaya
| extra10         = Seerkazhi Govindarajan, P. Susheela
| length10        =
}}

==Release==

=== Reception === Ameer told S. R. Ashok Kumar of The Hindu , "Director A. P. Nagarajan’s   Thiruvilaiyadal   is imaginative. It treats a mythological subject in an interesting way. It is one of the best films in the annals of Tamil cinema."  

=== Box office ===
The film ran for 25 weeks in Shanti, a theatre owned by Sivaji Ganesan. 

=== Awards ===
* Thiruvilayadal won the National Film Award for Best Feature Film in Tamil – Certificate of Merit for the Second Best Feature Film in Tamil in 1966.

== Controversies ==
 
In 2012, controversy arose when attempts were made to digitally re-release the film. Justice R. Subbiah of the Madras high court, hearing a suit filed by G. Vijaya of Vijaya Pictures, ordered maintenance of status quo for two weeks in respect of digitisation and release of Thiruvilaiyadal. The matter was again taken up for hearing on 16 August 2012.

In her suit, Vijaya contended that in the year 1975, Sri Vijayalakshmi Pictures, which was in possession of the rights of the film, had transferred the worldwide exclusive negative rights and all other rights such as exploitation and screening in cinema theatres in favour of Movie Film Circuit. In 1976, the latter had transferred all the rights to Vijaya Pictures.

Vijaya Pictures, perhaps buoyed by the performance of Karnan, approached the Gemini Colour Laboratory for digitisation of Thiruvilaiyadal so that it could be re-released. Vijayalakshmi Pictures, however, wrote to the laboratory asking it not to release the film without their prior consent.

In her suit, Vijaya said the worldwide exclusive negative rights for distribution and exhibition would also mean digitisation, as digital format is no different from the original format except for enhancement of viewing quality.

Noting that digitisation did not require any separate licence, Vijaya said the worldwide exclusive negative rights already conferred on her will encompass exploitation and exhibition of the film in digital format. 
 

== References ==
 

== Bibliography ==
*  
*  

== External links ==
*  

 
 
 

 
 
 
 
 
 
 
 
 