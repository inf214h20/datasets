Mythri
 
 
{{Infobox film
| name = Mythri
| image = Mythri Poster.jpg
| alt = 
| caption = Movie Poster
| director = Surya Raju
| producer = Rajesh Kumar
| writer = Surya Raju
| starring = Navdeep Sadha Bramhanandam Uttej "Chitram" Seenu
| music = Vikas
| cinematography = sleva kumar
| editing = vinay
| studio = Hanu Cine Creations
| distributor = 
| released =  
| runtime = 123 minutes
| country = India
| language = Telugu
| budget = 
| gross =
}}
Mythri  is a 2012 Telugu suspense film directed by Surya Raju starring Navdeep and Sadha in the lead roles. Rajesh Kumar produced the movie on Hanu Cine Creations Banner and Vikas provided the music. The movie released on 30 November 2012  The film was later dubbed and released in Tamil as Mythili in August 2014.

==Plot==
Deepus (Navdeep) dream is to make a music album and become famous, he approaches a channel head Murthy (Brahmanandam) who, impressed by his concept, offers to finance the shooting and also suggests his obese secretary as the heroine. Disappointed first, Deepu takes up the offer as he has no choice and chooses a huge old bungalow for the shoot at a secluded spot. As the work begins, Mythri (Sadha) the owner of the property arrives for a short stay and Deepu convinces her to be the heroine of the album and removes Murthys secretary. Meanwhile a death of a unit member and a series of strange incidents take place but the unit leave after the work is wrapped up without any doubts. Deepu stays back with Mythri and unravels the mystery of the haunted house.Towards the end of the story it reveals the mystery behind the deaths and the strange incidents.

==Cast==
* Navdeep as Deepu
* Sadha as Mythri
* Bramhanandam as Murthy
* Uttej
* "Chitram" Seenu
* Suman Setty
* Kallu Chidambaram
* "Satyam" Rajesh
* Bhikshu

==Production==
The movie was launched at Annapurna Studios on May 6, 2012. Karumuri Venkata Nageswara Rao, Nandini Reddy, Satish Reddy, Rajesh Kumar, Surya Raju attended this event.  Since then the movie was shot in a nonstop mode. This marks the return of Sadha to Telugu and Tamil Industry and in this film, she plays the titular role of Mythri, a docile and well mannered young woman during day time. But once the sun goes down, a peculiar change can be observed in Mythri. She turns hysterical, roams around all alone and behaves in mystical ways and the story is all about this change and the mystery behind it.  The movie was Scheduled for a release on November 30, 2012 which is exactly a week after the release of Nagarjunas Dhamarukam.It is available to watch online. Makers have released the original DVD rip. 

==Soundtrack==
{{Infobox album
| Name = Mythri
| Caption = Album cover
| Type = Soundtrack
| Artist = Vikas
| Cover =
| Released =  
| Recorded = 2012 Feature film soundtrack
| Length = 18:18 Telugu
| Label = Hanu Music
| Producer = Vikas
| Last album = 
| This album = 
| Next album = 
}}
The audio was Launched at Hyderabad on 24 September 2012. Vikas provided the music. The audio album has also been released into the market by the producer himself on his own audio label. 
{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 18:18
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Made In India
| note1           = 
| lyrics1         = Sahitya Sagar
| extra1          = Geetha Madhuri Vikas Raghuram Dhanunjay
| length1         = 3:47
| title2          = Jil Jil Jil
| note2           =  
| lyrics2         = Sreshta
| extra2          = Tejaswini
| length2         = 4:05
| title3          = Aa Ra Ra Pedave
| note3           = 
| lyrics3         = Sahitya Sagar
| extra3          = Deepu Sravana Bhargavi
| length3         = 3:15
| title4          = Ningiloni Neeli Megham
| note4           = 
| lyrics4         = Ramajogayya Sastry
| extra4          = Bhargavi Pillai Rolls Ride Megawatt
| length4         = 3:43
| title5          = Dha Dha Dha Dance
| note5           = 
| lyrics5         = Sreshta
| extra5          = Umaneha Vikas Raghuram Dhanunjay
| length5         = 3:27
}}

==References==
 

 
 