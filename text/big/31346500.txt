Main Street (1923 film)
{{infobox film
| name           = Main Street
| image          =
| imagesize      =
| caption        =
| director       = Harry Beaumont
| producer       = Warner Brothers
| writer         = Sinclair Lewis(novel) Alan Hale Louise Fazenda
| cinematography = Edwin B. Du Par Homer Scott
| editor         = Harry Beaumont
| distributor    = Warner Brothers
| released       = April 25, 1923
| runtime        = 90 minutes (9 reels)
| country        = United States
| language       = Silent film(English intertitles)
}} 1923 silent silent film novel by Sinclair Lewis. It was produced and distributed by Warner Brothers and directed by Harry Beaumont. A Broadway play version of the novel was produced in 1921.  This film is lost, as no print exists. 

==Cast==
*Florence Vidor - Carol Milford
*Julien Beaubien -
*Monte Blue - Dr. Will Kennicott
*Harry Myers - Dave Dyer Robert Gordon - Erik Valborg Noah Beery - Adolph Valborg Alan Hale - Miles Bjornstam
*Louise Fazenda - Bea Sorenson
*Anne Schaefer - Mrs. Valborg
*Josephine Crowell - Widow Bogart
*Otis Harlan - Ezra Stowbody
*Gordon Griffith - Cy Bogart
*Lon Poff - Chet Dashaway
*J. P. Lockney - Luke Dawson
*Gilbert Clayton - Sam Clark Jack McDonald - Nat Hicks
*Michael Dark - Guy Pollock
*Estelle Short - Mrs. Dashaway
*Glen Cavender - Harry Haydock
*Katherine Perry - Mrs. Dave Dyer(as Kathryn Perry)
*Aileen Manning - Mrs. Stowbody
*Mrs. Hayward Mack - Mrs. Haydock
*Louis King - Mr. Volstead
*Josephine Kirkwood - Mrs. Sam Clark
*Louise Carver - Mrs. Donovan
*Hal Wilson - Del Snaflin

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 