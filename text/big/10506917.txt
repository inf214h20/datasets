Jánošík (1921 film)
{{Infobox film
| name = Jánošík
| image =Janosik1921.jpg
| writer = Novel: Gustáv Maršall-Petrovský Play: Jiří Mahen Screenplay: Jozef Žák-Marušiak Jaroslav Jerry Siakeľ Daniel Siakeľ
| starring = Theodor Pištěk Mária Fábryová Vladimír Šrámek Jozef Chylo Jaroslav Jerry Siakeľ
| producer = Ján Závodný
| cinematography = Daniel Siakeľ Oldřich Beneš
| distributor = Biografia
| released = 25 Nov. 1921 (Czechoslovakia) 1 Dec. 1921 (U.S.) 3 Jan. 1922 (Slovakia)
| runtime = 68 min
| country = Slovakia United States Czechoslovakia
| language = silent
| budget = $14,500
| gross = 19 mil. crowns (Czechoslovakia)}}
 Slovak black-and-white early American movies in camera work, in the use of parallel narratives, and in sequences inspired by Western (genre)|Westerns. Jánošík placed Slovak filmmaking as the 10th national cinema in the world to produce a Feature film|full-length feature movie. 

== Plot summary == Theodor Pištěk), a young, imposing seminary student, returns to his home village to find that his ailing mother has just died. Count Šándor (Vladimír Šrámek), however, would not release Jánošík’s father (Karel Schleichert) from his weekly obligations for her funeral and has the father caned, which proves fatal for the old man. Jánošík assaults the Count and escapes from the village.

While on the run, Jánošík finds himself fighting on the side of a band of highwaymen in a skirmish with the Count’s cohort commanded by Pišta (Jozef Chylo), discards the frock, joins the band, and takes over the bands leadership. Jánošík’s band parties in the mountains, robs traveling noblemen, and uses disguise to rob the guests at the noblemens County Ball only to redistribute the booty among the farmers.

Jánošík rekindles a love affair with his childhood sweetheart Anička (Mária Fábryová), who is sexually harassed by the Count. The local priest (František Horlivý) helps Jánošík with the cover-up during his visits to the village. However, his frequent calls and yet another scuffle with the Count prove to be his undoing. With the help of a betrayer, the Counts men learn about Jánošíks whereabouts and overpower him and his band during a drinking party in a tavern. Jánošík is hanged.

The central narrative is framed in a story set around the time of the film’s release, in which a hiker (Theodor Pištěk) and friends (Mária Fábryová, Jozef Chylo) pause at a mountain sheepfold where the head shepherd comments on the hiker’s stature similar to the legendary Jánošík’s and narrates for them the film’s storyline.

== Director ==
 Turiec County central Slovakia and baptized Ludvik Jaroslav Siakeľ,  he immigrated to the United States in 1912 at the age of 16 and used Ludwig Jerry as his given names in English. He used the names Jaroslav and Jerry in personal contacts.

== Company ==
 Turiec County central Slovakia. Both had experience with film equipment and processing, and limited experience with filmmaking from working for the Selig Polyscope Company in Chicago (some sources misidentify them as its owners or founders).
 western Slovakia, vicinity of the Siakeľ brothers birthplace in Slovakia,  on two sets constructed at the site, and finished on the sound stage at the A-B Studio in Prague.

== Screenplay ==
 western Slovakia, Slovak by Martin Rázus in 1920.

== Cast ==

{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor  !! Role
|- Theodor Pištěk (1895–1960) || Juraj Jánošík and Hiker (dual role)
|-
| Mária Fábryová (1900–1973) || Anička, Jánošíks lover and Hiker (dual role)
|-
| Vladimír Šrámek || Count Šándor
|-
| Jozef Chylo (?-1956) || Counts Deputy Pišta and Hiker (dual role)
|-
| František Horlivý || Priest, the protector
|-
| L. Hušek || Highwayman Ilčík
|-
| P. Kutný || Highwayman Hrajnoha
|-
| Michal Staník || Highwayman Michalčík
|-
| Karel Schleichert (?-1940) || Jánošíks Father
|- Baron Révay
|- Baroness Révay
|-
| Bronislava Livia (1901-?) || The Révays Daughter
|-
| Jan W. Speerger (1896–1950) || Military Commander
|-
| Saša Dobrovodská || Gypsy Woman
|-
| Ferdinand Fiala (1888–1953) || County Chief of Liptov
|-
| Rudolf Myzet (1888–1964) || Nobleman
|-
| Karel Fiala (1871–1931) || Chief Judge
|-
| Jaroslav Vojta (1888–1970) || 
|-
| Samuel Šťastný || 
|-
| Vlado Ivaška || 
|}

Most of the leading and supporting roles were given to professional or amateur actors. Theodor Pištěk in the dual role of Jánošík and of a hiker in the framing story was one of the most popular actors of the period who starred in nine other films in the same year.  Pištěk believed that he got the role thanks to Jánošíks art director and actor (priest) František Horlivý, who was an amateur actor in Chicago, but used to work in the theater troupe organized by Pištěks father Jan in Prague.  Mária Fábryová in the leading role of Jánošíks lover Anička was an amateur actress from the town of  .

== Release dates ==
 Turiec County, Slovakia, before its theatrical release, which is sometimes misquoted as its release date.  It had premieres in Prague, in Chicago (Cicero, Illinois|Cicero, IL) at the now demolished 1150-seat Atlantic,  and in Žilina at the Grand Bio Universum (later Dom umenia Fatra).

The film was thought lost until 1970. It was restored by Ján Rumanovský with a music soundtrack by Jozef Malovec in 1975. 

The restored version of the silent Jánošík was released on DVD in the PAL format, 4:3 aspect ratio, region-free ("Region 0"), with English, French, German, Hungarian, Polish, Russian, Slovak, and Spanish intertitles by Dikrama/Slovenský filmový ústav  in 2003 as part of a 2-DVD box set with the like-named movies Jánošík I and Jánošík II, both from 1963, and bonus material.

Sources sometimes mention its presumed listing by UNESCO as world cultural heritage, but Jánošík is not included on the lists of Tangible Heritage,  Intangible Heritage,  or World Heritage  maintained by UNESCO.

== Business ==

Jánošíks estimated budget was $14,500 and its total gross in Czechoslovakia during its theatrical run is estimated at close to 19 million Czechoslovak crowns. 

== References ==
 

== External links ==
*  
* Jaroslav Siakeľ (dir., 1921)   (Clip)

 
 
 
 
 
 
 