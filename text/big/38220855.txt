The Gnomes' Great Adventure
{{Infobox film
| name           = The Gnomes Great Adventure 
| caption        = 
| director       = Harvey Weinstein
| producer       = Harvey Weinstein Bob Elliott Ray Goulding
| based on       = The World of David the Gnome
| distributor     = Miramax Films
| released        = 1987
}}

The Gnomes Great Adventure is a 1987 American animated film first released in 1987,   directed by Harvey Weinstein and released by Miramax Films in 1987. The film was Weinsteins second and final directorial attempt since 1987. The film was based on The World of David the Gnome, it follows the life and adventures of the gnomes as they struggle to outwit enemy trolls.  Although the film is relatively unmemorable today, some well known actors did contribute to the film, including Tom Bosley, Christopher Plummer, Bob Elliott (comedian) | Bob Elliott, Ray Goulding and Frank Gorshin. 

==References==
 

==External links==
*  

 
 
 

 