Um Null Uhr schnappt die Falle zu
Um German thriller film directed by Harald Philipp and starring George Nader, Horst Frank and Heinz Weiss.  It was the fourth film in the Jerry Cotton series. 

==Plot==
Jerry Cotton hunts a psychotic murderer who kills some people out of insanity and others for money. Unfortunately he is connected to other assassins of his kind and soon they realise that Jerry Cotton is after them. This makes Jerry himself immediately their favourite target.

==Cast==
* George Nader ...  Jerry Cotton 
* Horst Frank ...  Larry Link 
* Heinz Weiss ...  Phil Decker 
* Dominique Wilms ...  Maureen 
* Allen Pinson ...  Harry 
* Alexander Allerson ...  Husky 
* Monika Grimm ...  Ruth Warren 
* Helga Schlack ...  Helen Culver 
* Ricky Cooper ...  Pal 
* Werner Abrolat ...  Krot

==Bibliography==
* 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 