Aankhen (1968 film)
{{Infobox film
| name = Ankhen
| image = Ankhen 1968 film poster.jpg
| image size =
| caption = Film poster
| director = Ramanand Sagar
| producer = Ramanand Sagar
| writer = Ramanand Sagar
| narrator = Mehmood Kumkum  Nazir Husain Jeevan Sajjan Ravi
| cinematography = G. Singh
| editing = Lachhmandass
| distributor =
| released = 1968
| runtime =
| country = India
| language = Hindi
| budget =
}} Hindi spy Ravi and the lyrics by Sahir Ludhianvi. It was the biggest hit of 1968 in India, attaining blockbuster status. 

Ankhen is considered to be a pioneer in Hindi spy films.  It was initially planned to be shot when Dharmendra had just one film under his belt. But the director Ramanand Sagar had apprehensions of casting him since he was considered raw and was not yet a star; therefore he recruited a huge star of the period, Mala Sinha, opposite him for market considerations. With the success of Phool Aur Paththar, Sagar decided to increase his budget and finalize Dharmendra as the leading man. The film went on to become the biggest hit of the year and Dharmendra was in the top league. His other movie of the year, Shikar (1968 film)|Shikar, went on to be a golden jubilee hit and he became an actor of reckoning.

==Plot==
Shortly after independence India faces terrorists attacks in Assam, resulting in many deaths and casualties. A group of concerned citizens, who are not connected with the government, decide to do something to stop this carnage. While Salim is already at work in Beirut, his cover is blown and he is shot dead. Now Sunil Mehra must travel to Beirut and take over. Once there, he meets a former flame, Meenakshi Mehta, and a female admirer by the name of Zenab. 

The terrorists are headed by a man named Syed, who deputes one of his assistants, Madame, to spy on Sunils dad, Diwan Chand Mehra, by posing as Mehras daughters aunt, forcing her to obey by abducting her son, Babloo, and holding him captive. Soon Syed and his associates, including Doctor X and Captain find out all secrets of Mehra, as a result of which Sunil is trapped and held by Syed. Then Diwans world is shattered when Meenakshi telephonically informs him that Sunil has been killed. The question remains what will happen to Babloo, Diwan, and the rest of the concerned citizens, especially when they have become vulnerable due to Madames presence in their very household.

==Cast==
* Mala Sinha	 ...	Meenakshi Mehta
* Dharmendra	 ...	Sunil Kumkum	 ...	Sunils Sister
* Sujit Kumar	 ...	Nadeem Mehmood	 ...	Mehmood
* Lalita Pawar	 ...	Madam / fake Mousi
* Daisy Irani	 ...	Lily
* Zeb Rehman	 ...	Princess Zehnab
* Madhumati	 ...	Madhu
* Neelam		
* Sujata		
* Nazir Hussain	 ...	Diwan Chand aka Major Saab
* Jeevan	 ...	Doctor
* Amarnath		
* Madan Puri	 ...	Captain Dhumal	 ...	Studio Owner
* Sajjan	 ...	Boss in Beirut
* Master Ratan	 ...	Babloo
* Hiralal		
* Parduman		
* A. A. Khan		
* Jagdish Kanwal
* Narbada Shankar		
* Ram Tipnis		
* Lachhmandass
* M. B. Shetty	 ...	Guard who gets arrested with the Diamond Dealer
* Kailash Advani		
* Ishar Singh		
* Shafi		
* Lalit Kumar		
* Ramlal		
* Mohammed Ali
* Sheru
* Qamar		
* Vishnu		
* Bismillah

==Songs==
* Milti hai zindagi mein mohabbat (Lata)
* Gairon pe karam (Lata)
* De data ke naam tujhko Allah rakhe(Manna Dey and Asha Bhosle)
* Loot Ja (Asha Bhosle, Kamal Barot and Usha Mangeshkar)
* Meri Sunle Araj (Lata)
* Us Mulk Ki Sarhad Ko (Mohd Rafi)

== Awards ==
*  
* Filmfare Best Cinematographer Award: G. Singh (Colour category)  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 