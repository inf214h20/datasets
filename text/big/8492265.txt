Tarzan Finds a Son!
{{Infobox film
| name           = Tarzan Finds a Son!
| image          = Tarzan Finds a Son! (movie poster).jpg
| director       = Richard Thorpe
| producer       = Sam Zimbalist
| writer         = Cyril Hume 
| based on       =  
| starring       = Johnny Weissmuller Maureen OSullivan Johnny Sheffield Henry Wilcoxon
| music          = 
| cinematography = Leonard Smith Frank Sullivan MGM
| released       =  
| country        = United States
| runtime        = 82 minutes
| language       = English budget = $898,000  .  gross = $2,088,000 
}}
 MGM Tarzan series to feature Johnny Weissmuller as the "King of the Apes".

==Plot==
A plane flying to Cape Town, carrying a young couple and their baby, crashes in the jungle. Everyone on the plane dies, except for the baby who is rescued by Cheeta, Tarzans chimpanzee. Tarzan and Jane adopt the child and name him "Boy". Five years later, a search party comes looking for Boy, because he is the heir to a fortune worth millions. Tarzan and Jane claim the child is dead and that Boy is theirs, but Sir Thomas recognizes Boys eyes. The younger Lancings suggest leaving Boy and taking the inheritance; when Sir Thomas objects, they say they will take him back and, as legal guardians, still control of the inheritance. Sir Thomas says hell tell Tarzan, but the rest of the party imprison Sir Thomas in a tent and plan to abduct Boy. Tarzan overhears them plotting; he steals their guns and throws them into a deep lake. Jane arrives the next day and learns what has taken place, and admits that Boy is Greystoke. She persuades Tarzan to retrieve the cache of guns, without which the search party cant survive. Tarzan retrieves them but Jane drops the rope so that Tarzan is trapped. 

Jane, convinced its the right thing to do, goes with Boy and the rest of the Lancings toward civilization but Sir Thomas convinces her that the younger Lancings only want Boy for his money. Sir Thomas tries to sneak away but they shoot him. Thinking Jane is trying to fool them, they ignore her directions and fall into the hands of the Zambeli, known for mutilation of captives. The white people are held in a separate hut while the tribe begins to kill and preserve the native bearers. Jane is wounded while helping Boy to escape through the fence. Boy finds Tarzan and is aided by chimps and elephants to free him. Tarzan reaches the Zambeli village and uses the elephants to drive away the natives. He saves two of the search party, and he and Jane decide to keep Boy with them in the jungle.

==Cast==
* Johnny Weissmuller as Tarzan
* Maureen OSullivan as Jane Parker
* Johnny Sheffield as Boy Ian Hunter as Mr. Austin Lancing
* Henry Stephenson as Sir Thomas Lancing
* Frieda Inescort as Mrs. Lancing
* Henry Wilcoxon as Mr. Sandee Sande
* Laraine Day as Mrs. Richard Lancing
* Morton Lowry as Mr. Richard Lancing Gavin Muir as Pilot

==Production background==
The three-year gap between this and the previous Tarzan film was because MGM had originally let the film rights elapse after Tarzan Escapes. Independent producer Sol Lesser obtained the rights to make five Tarzan movies, but the first of these, Tarzans Revenge, proved to be a flop. The producers believed that audiences were unwilling to accept Glenn Morris in the role made famous by Johnny Weissmuller. (Lesser had been unable to obtain Weissmullers services as he remained under contract at MGM.) MGM realized that Weissmuller had continuing attraction as Tarzan and bought out Lessers interest in the next three films; they restarted their series.

This film was originally to be called Tarzan in Exile. As it featured the jungle couple acquiring an infant, it was changed to Tarzan Finds a Son. The plot device was to appease morality groups, as Tarzan and Jane were married in the films. This was intended as Maureen OSullivans last appearance as Jane in the series, with her character to die as a result of a spear wound. OSullivan was tired of the Jane role, and Boy was brought in as a substitute interest. After audience previews showed very negative reactions to her death, the ending was changed. Johnny Sheffield, who played Boy, claims that Weissmuller handpicked him for the part and taught him to swim.
 Lord Greystoke.

==Reception==
According to MGM records the film earned $1,039,000 in the US and Canada and $1,049,000 elsewhere, resulting in a profit of $528,000. 
==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 