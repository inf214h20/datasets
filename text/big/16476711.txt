The Sculptor (film)
 
The Sculptor is one of the first Australian feature films to be filmed using the new Red One camera,  (Red Digital Cinema Camera Company). The film is described as a Supernatural Thriller. 

The Sculptor is about, "a struggling artist who immerses himself in black magic to further his career, but when he has everything he ever dreamed of, the demons come back to haunt his life, his work and his family." The Sculptor is directed by Australian Christopher Kenworthy, who co-wrote and co-produced with Chantal Bourgault.  Filming took place in Perth, Australia during early 2008. 

Prior to an online release in 2013 the film was rebadged as The Sculptors Ritual. 

== The Technology ==
The Sculptor was shot with the Red One digital camera. Cinematographer Jason Thomas said, "The hype is to be believed. The workflow, resolution, weight, camera features and the rest are unmatched. Forget the whole Film vs Digital debate; this capture system is something again and more."

== The Crew and Cast ==
The main cast is Paul David-Goddard,  Melanie Vallejo, Georgina Andrews, Matt Penny and Gordon Honeycombe.

The production team includes cinematographer Jason Thomas, production designer Emma Fletcher and composer James Ledger, with additional music being provided by the Trembling Blue Stars. 

Jason Thomas has worked on everything from news gathering to 35mm drama, and has won nine awards from the Australian Cinematographers Society, including 3 Gold Awards. Emma Fletcher has worked as art director and production designer on TV series and features, including Hidden Creatures and Lockie Leonard. She won the WA Screen Award for Production Design. James Ledger is composer-in-residence with the West Australian Symphony Orchestra. He won the 3MBS National Composers Award in 2004 and has worked on many film and television projects.

== References ==
 

==External links==
*Trailer http://www.christopherkenworthy.com/fm.html
*Director  
* 

 
 
 

 