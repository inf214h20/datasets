We Were Seven Widows
 
{{Infobox film
| name           = We Were Seven Widows
| image          = Eravamo sette vedove.jpg
| caption        =
| director       = Mario Mattoli
| producer       = Giulio Manenti Steno
| narrator       =
| starring       = Antonio Gandusio
| music          =
| cinematography = Carlo Montuori
| editing        = Fernando Tropea
| distributor    =
| released       =  
| runtime        = 83 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

We Were Seven Widows ( ) is a 1939 Italian comedy film directed by Mario Mattoli and starring Antonio Gandusio.

==Cast==
* Antonio Gandusio - Lavvocato Ruggero Mauri
* Nino Taranto - Orlando, il cameriere di bordo
* Laura Nucci - Vera
* Silvana Jachino - Barbara
* Laura Solari - Anna Calcini
* Greta Gonda - Maria
* Anna Maria Dossena - Ada
* Maria Dominiani - Liliana
* Amelia Chellini - Gioconda Zappi Torriani
* Oscar Andriani - Il marito di Liliana
* Gino Bianchi - Francesco, il marito di Anna
* Adolfo Geri - Il marito di Ada
* Mario Siletti - Popi, il marito geloso di Vera
* Carlo Micheluzzi - Matteo
* Ori Monteverdi - Linfermiera
* Armando Migliari - Il medico

==External links==
* 

 

 
 
 
 
 
 
 
 
 