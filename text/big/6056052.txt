Casino Tycoon 2
 
 
{{Infobox film
| name           = Casino Tycoon 2
| image          = CasinoTycoon2.jpg
| image_size     =
| caption        = Film poster
| film name      = {{Film name
| traditional    = 賭城大亨II之至尊無敵
| simplified     = 赌城大亨II之至尊无敌
| pinyin         = Dǔ Chéng Dà Hēng Èr Zhī Zhì Zūn Wú Dí
| jyutping       = Dou2 Sing4 Daai6 Hang1 Ji6 Zi1 Zi3 Zyun1 Mou4 Dik6 }}
| director       = Wong Jing
| producer       = Wong Jing
| writer         = Wong Jing
| narrator       =
| starring       = Andy Lau Chingmy Yau
| music          = Lowell Lo Lee Yiu-Dung Sherman Chow
| cinematography = Gigo Lee
| editing        = Wong Wing-Ming
| distributor    = Newport Entertainment Ltd.
| released       =  
| runtime        = 114 min
| country        = Hong Kong
| language       = Cantonese
| gross          = HK$10,479,148
}}
 Casino Tycoon.

==Summary==
Andy Lau returns in his role as the Casino Tycoon of Macau, Benny Ho. We join Ho 18 years after the last film as he has established his Casino empire in Macau and is living with his daughter and wheelchair-bound wife. When his daughter brings home a young man eager to make headway in the Casino empire Ho becomes embroiled in a plot to destroy his family.

==Cast==
* Andy Lau
* Chingmy Yau
* Vivian Chan
* Remos Choi
* Lau Siu Ming
* Benz Hui
* Joey Wong (cameo)
* Alex Man (cameo)
* Michelle Reis (cameo)
* Sandra Ng (cameo)
* Calvin Choi (cameo)
* Edmond So (cameo)
* Dennis Chan
* John Ching


 {{Cite web |url=http://www.imdb.com/title/tt0104146/ |title=Casino Tycoon 2 
 |accessdate=30 July 2010 |publisher=imdb.com}} 
   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 