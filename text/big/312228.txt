House Party (film)
{{Infobox film
| name           = House Party
| image          = House Party 1990 Movie Poster.jpg
| image_size     = 195px
| alt            =
| caption        = Theatrical release poster
| director       = Reginald Hudlin Gerald T. Olson Warrington Hudlin
| writer         = Reginald Hudlin Christopher "Play" Martin
| music          = Lenny White Marcus Miller
| cinematography = Peter Deming
| editing        = Earl Watson
| distributor    = New Line Cinema
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $2.5 million 
| gross          = $26,385,627 
| Succeeded by   = House Party 2
}} Kid and Play of hip hop duo Kid John Witherspoon, George Clinton. This was Robin Harris last on-screen performance before his untimely death, shortly after the film was completed.

The film was written and directed by Reginald Hudlin, based on his award-winning Harvard University student film.    The film grossed $26,385,627 in its run at the box office with its widest release being 700 theaters. The film has since become a cult classic.   Upon its initial release, the film garnered critical acclaim.

The lead roles were originally written for DJ Jazzy Jeff & The Fresh Prince.  

==Plot==
  Christopher "Play" George Clinton) scratch and mix a few of his old doo wop records so that he can liven the party with a Rapping|rap, until Stab and the others turn up again. When trying to get away from Stab, he winds up knocking an older man down before attempting to make a run for it. However, Kid and the bullies are caught by the neighborhood police, who humiliate the four teenagers in front of the reunion party attendees before letting them go.
 freestyle battle. Stab and his friends attempt to break up the party, but are arrested a second time by the policemen, who take delight in the prospect of beating them up. Kids father eventually makes his way to the party, demanding to know where Kid is. When he doesnt spot Kid - Kid is upstairs helping Sharane get her coat - Pop vows to wait for the boy at home. Although Kid and Sydney each have an eye for each other, Sharane decides to openly flirt with Kid, much to Sydneys disgust. The three of them soon leave the party, but when Kid tries to make advances on Sharane, she rebuffs him. Kid then walks Sydney back home, and after some argument the two of them finally calm down and make conversation.

Sydney allows Kid to sneak into her house, and the two are about to have sex in Sydneys room when she stops him, wanting to know if she is simply his second choice. Kid admits that Sydney was his first choice all along, but they do not do anything when they see that the only condom  Kid has is too old to be used. When Sydneys parents come home - now revealed as one of the couples at the high school reunion, including the man Kid ran into - Sydney hastily helps Kid sneak out of the house. He manages to get out of yet another scrape with Stab and his brothers, and they all end up in a jail cell, where Kid entertains the rest of the men in the cell by rapping, distracting them long enough for Play, Sharane, Bilal, and Sydney to arrive with enough cash to bail him out. Later on, the five friends say their goodnights. Kid and Sydney share a long passionate kiss goodnight. After Play and Bilal drops him off, Kid sneaks in the house and gets undressed. As he is about to get into bed, he looks up only to find Pop holding a belt. The movie then cuts to the credits where Pop whipping Kid can be heard.

After the credits, two policemen walk outside the police station to be crushed by a roof.

==Cast==
* Christopher "Kid" Reid as Christopher Robinson, Jr. (Kid)
* Robin Harris as Christopher Robinson, Sr. (Pop) Christopher "Play" Martin as Peter Martin (Play)
* Martin Lawrence as Bilal
* "Paul Anthony" George (of Full Force) as Stab
* Lucien "Bowlegged Lou" George, Jr. (of Full Force) as Pee-Wee
* Brian "B-Fine" George (of Full Force) as Zilla Tisha Campbell as Sydney
* Adrienne-Joi Johnson|A.J. Johnson as Sharane
* Gene "Groove" Allen (of Groove B. Chill) as Groove Daryl "Chill" Mitchell (of Groove B. Chill) as Chill
* Lou B. Washington as Otis
* Kelly Jo Minter as LaDonna John Witherspoon as Mr. Strickland
* Bebe Drake as Mrs. Strickland

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on March 9, 1990 by Motown Records. It peaked at 104 on the Billboard 200|Billboard 200 and 20 on the Top R&B/Hip-Hop Albums.

==Sequels==
The film was a popular success, and two   in 1991; and  , which does not feature any of the original cast from the other three films. A fifth installment and direct follow-up to the third film, titled   was filmed in 2012 with Tequan Richmond, Zac Goodman, Tristin Mays, Alex McGregor, Rolonda Watts and Gary Anthony Williams. The film was a direct to DVD release in 2013, and also marked the return of Kid n Play to the series.

==Reception==
The film was well received by film critics.   Roger Ebert of the Chicago Sun-Times gave it three out of four stars and commended its "energy and exuberance".    He called the film "wall-to-wall with exuberant song and dance" and stated, "the musical is a canvas used by the director, Reginald Hudlin, to show us black teenagers with a freshness and originality thats rare in modern movies. The movie has a 96% fresh rating on Rotten Tomatoes with an average rating of 7.4 out of 10. 

American Film Institute recognition:
*AFIs 100 Years... 100 Laughs - Nominated 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 