Njangalude Veettile Athidhikal
{{Infobox film
| name           = Njangalude Veettile Athidhikal
| image          = Njangalude Veettile Athidhikal.jpg
| alt            =
| caption        = Poster
| director       = Sibi Malayil
| producer       = Milan Jaleel
| writer         = K.Gireesh Kumar Narain Lena Lena
| music          = Ratheesh Vega Background score: Bijibal
| cinematography =Sameer Haq
| editing        =Bijith Bala
| studio         = Galaxy Films
| distributor    = Galaxy Films Release (Kerala) Popcorn Entertainments (Asia Pacific)
| released       =  
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Innocent , Lena plays sygnificant roles in the film.   Priyamani has been paired up with Jayaram for the first time.  The background score and soundtrack are composed by Ratheesh Vega. Kalabhavan Shajon debuts as a singer in the film through the song Maaye Maaye.   

The film is produced by Milan Jaleel under his banner Galaxy Films. Popcorn Entertainments Australia distributed the film all over Australia, New Zealand, Singapore and Malaysia. The film released on 30 October 2014, with mixed reviews. 

==Plot==

The films story revolves around Advocate Manoj and Bhavana played by Jayaram and Priyamani, respectively. Their life takes a turn when their only daughter meets a tragic death. The demise of their daughter leads to a gap between them. Eventually, Bhavana will start doubting her husband of having an extramarital affair.The film then talks about some uninvited guests, who come into their life and how they influence the couple and their life.

==Critics review==

Paresh C Palicha of rediff.com rated 2/5 stars and wrote "The film cannot be taken seriously and it turns out to be a miserable disappointment".  Cochintalkies.com rated 2/5 stars and said "Watchable, But do not expect anything. A movie made out of desperation that would the exact sentence to define this movie. Njangalude Veettile Adhithikal with no doubt has a good story but mediocre script and lousy direction has ruined this movie  Sify.com gave "below average" verdict and stated "There is no point in mincing words, Njangalude Veettile Adhithikal tests the patience of the viewer big time. Watch it at your own risk please!".  Indiaglitz.com rated 5/10 stars and wrote "Even after with a  convincing story and decent performances from  the key players, the movie fails to impress, enthuse or amuse at any points of its narratives. The problem with the movie is that the whole theme is handled in a inept matter by K Gireesh kumar, whose script lines fail to bringing any impressions."  Nowrunning.com rated 1.5/5 stars and gave a "poor" verdict and wrote "From the levels of a normal family drama, the movie attempts to become a psychological thriller. Technically the movie doesnt bear the stamp of an experienced director. Njangalude Veettile Athidhikal in all likelihood will struggle in theatres like uninvited guests in a party.".  Unni R Nair of Kerala9.com rated 2/5 stars and said "The film seems just about OK, but fails to impress; it once again proves that it’s the script that makes or mars it all. In this case the script mars it all, despite the story being convincing and the key players delivering rather decent performances. The film, on the whole, fails to impress, excite or entertain." and concluded as "Unimpressive; poor scripting mars it all....Forgettable!!!". 

The Times of India rated 3/5 stars and wrote "As likely as not it deals with a subject related to a mental malady, audience see the ghost of Manichithrathazhu here and there. One also wonders why certain scenes which could have been handled better with situational comedy. Overall the film is worth a watch for the subject that Siby Malayil has tried to deal with and has its heart in the right place.". 

==Music==

Composer Ratheesh Vegas album features three tracks. One of which is sung by actor Shajon and the rest by newbie singers. Though it boasts of no musical novelty, its a playlist friendly album with one melodic fast number and two other soft melodies. Shajon debuts as a singer through singing the track Maaye Maaye penned by Santosh Varma. The song is rhythmic and has a folk feel. Mazhavil Thundu by Sanup is a melody that sets up a hazy mood. However, it reminds you of many other hit melodies here and there. The song Oru Naal is included both in the male and female version by Joju Sebastian and Anitha. Rafeeq Ahameds lyrics for the song are also haunting and romantic. 

{| class="wikitable"
|-
! No!! Song !! Singers !! Lyrics !! Length
|-
| 1 || Maaye Maaye || Kalabhavan Shajon || Santosh Varma || 3.34
|-
| 2 || Mazhavil Thundu || Sanup ||  || 4.27
|-
| 3 || Oru Naal (Male) || Joju Sebastian || Rafeeq Ahamed ||3.48
|-
| 4 || Oru Naal (Female) || Anitha || Rafeeq Ahamed || 2.27
|}

==References==

 

 
 
 