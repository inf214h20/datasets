Resident Alien (film)
 
{{Infobox film
| name = Resident Alien
| image = ResidentAlienMoviePoster.jpg
| caption = Film poster
}}
Resident Alien is a documentary film directed, produced and edited by Jonathan Nossiter, and co-produced by Dean Silvers.

==Plot==
At age 73, writer and melancholy master of the bon mot, Quentin Crisp (1908–1999), became an Englishman in New York. Rossiters camera follows Crisp about the streets of Manhattan, where Crisp seems very much at home, wearing eye shadow, appearing on a makeshift stage, making and repeating wry observations, talking to John Hurt (who played Crisp in the autobiographical TV movie, "The Naked Civil Servant"), and dining with friends. Others who know Crisp comment on him, on his life as an openly gay man with an effeminate manner, and on his place in the history of gays social struggle. The portrait that emerges is  one of wit and of suffering.

==Cast==
* Quentin Crisp as  Himself Sting as Himself - Singer
* John Hurt as Himself - Actor
* Holly Woodlawn as Himself - Performer/Actor
* Fran Lebowitz as Herself - Writer
* Michael Musto as Himself - Gossip Columnist

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 
 