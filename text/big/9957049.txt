The War Between Men and Women
{{Infobox film
| name           = The War Between Men and Women
| image_size     =
| image	         = The War Between Men and Women FilmPoster.jpeg
| caption        =
| director       = Melville Shavelson
| producer       = Danny Arnold
| writer         = Danny Arnold Melville Shavelson James Thurber (writings)
| narrator       = Barbara Harris Jason Robards
| music          = Marvin Hamlisch
| cinematography = Charles F. Wheeler
| editing        = Frank Bracht
| studio         = Cinema Center Films
| distributor    = National General Pictures (1972, original) Paramount Pictures (2014, DVD)
| released       =  
| runtime        = 110 min.
| country        = United States English
| gross          = $4,000,000 (US/ Canada rentals) 
}}
 Barbara Harris, and Jason Robards.  
 1972 by Cinema Center Films.  Like many other films in the Cinema Center catalog, it has long been unavailable on home video, with the exception of a brief release on VHS in 2000.  Nonetheless, it has been shown on television. The film features animated cartoons interspersed in the story based on Thurbers works.
 1969 Thurber-based television series My World and Welcome to It. The screenplay was by Shavelson and by Danny Arnold, who also worked on the 1969 series.  Arnold is best known for his later television series, Barney Miller.  Lisa Gerritsen, who plays Linda Kozlenko in the film, previously co-starred in My World and Welcome to It as Lydia Monroe.

==Plot==

Peter Wilson (Jack Lemmon) is a sarcastic near-sighted cartoonist, author and swinging bachelor living in Manhattan. He detests dogs and children. He is flustered by womens priorities and avoids commitment, much preferring transient physical relationships. At the office of his eye surgeon, Peter meets a leggy, eye-catching brunette named Terri Kozlenko (Barbara Harris). He likes her very much, but discovers later that she is a single mother to three children. 

Nevertheless, they develop a close friendship that grows into romance, when Peter realizes that Terri is the only woman who can tolerate his strong anti-feminist opinions. When she rejects his plan of a sexual relationship conducted exclusively at his bachelor pad (so that he doesnt have to bond with her demanding family), he reluctantly proposes to her. They get married and he moves into her apartment, but her rogue ex-husband Stephen (Jason Robards) appears to spend more time with their children. Stephen and Peter clash at first, but they soon become good drinking friends, much to Terris disapproval.

Peters eyesight gradually worsens and his boss, Howard Mann (Herb Edelman), begins to criticize his work. Peter schedules a risky operation that could cure his problem, and tries to keep it a secret from Terri to avoid worrying her.  Howard gets hysterical and inadvertently ruins Peters alibi of working away from home on a book. Terri tells him that she had known that Peter was going blind when she first met him.

==Cast==
* Jack Lemmon as Peter Edward Wilson Barbara Harris as Theresa Alice Kozlenko
* Jason Robards as Stephen Kozlenko
* Herb Edelman as Howard Mann
* Lisa Gerritsen as Linda Kozlenko
* Moosie Drier as David Kozlenko
* Severn Darden as Dr. Harris
* Lisa Eilbacher as Caroline Kozlenko
* Lucille Meredith as Mrs. Schenker
* Ruth McDevitt as Old Woman
* Lea Marmer as Old Hag
* Joey Faye as Delivery boy
* Alan DeWitt as Man
* John Zaremba as Minister
* Richard Gates as Bernie (as Rick Gates)
* Janya Braunt as Nurse
* Olive Dunbar as Woman at Literary Tea
* Margaret Muse as Woman at Literary Tea
* Bill Hickman as Large Gentleman (as William Hickman)
* Joyce Brothers as Herself (as Dr. Joyce Brothers)
* Danny Arnold as Manhattan Policeman
* Burt Richards as Book Salesman

==Award nomination==
The screenplay by Melville Shavelson and Danny Arnold was nominated for a Writers Guild of America Award for Best Comedy Written Directly for the Screen.

==DVD==
The War Between Men and Women was released to DVD by Paramount Pictures Home Entertainment on January 28th, 2014 as a Region 1 widescreen DVD.

==References==
 
==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 