Parvathi Kalyana
{{Infobox film 
| name           = Parvathi Kalyana
| image          =  
| caption        = 
| director       = B. S. Ranga
| producer       = B. S. Ranga
| story          = 
| writer         = Chi Sadashivaiah (dialogues)
| screenplay     =  Rajkumar Udaykumar M. P. Shankar Raghavendra Rao
| music          = G. K. Venkatesh
| cinematography = B. N. Haridas
| editing        = P. G. Mohan Devendranath Chakrapani
| studio         = Vikram Productions
| distributor    = Vikram Productions
| released       =  
| runtime        = 129 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, directed by B. S. Ranga and produced by B. S. Ranga. The film stars Rajkumar (actor)|Rajkumar, Udaykumar, M. P. Shankar and Raghavendra Rao in lead roles. The film had musical score by G. K. Venkatesh.  

==Cast==
  Rajkumar
*Udaykumar
*M. P. Shankar
*Raghavendra Rao
*Dinesh
*Kuppuraj
*Master Babu
*Pandari Bai
*Chandrakala
*B. Jayashree
*Vijayalalitha
*Baby Prashanth
*Baby Sunanda
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Sadaashiviah || 02.19
|-
| 2 || Vanamaali Vaikuntapathe || PB. Srinivas, Lata Mangeshkar || Chi. Sadaashiviah || 03.08
|}

==References==
 

==External links==
*  
*  
*  

 
 
 

 