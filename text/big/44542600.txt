Hell Bent for Love
 
{{Infobox film
| name           = Hell Bent for Love
| image          = Hell Bent for Love.jpg
| caption        = Film poster
| director       = D. Ross Lederman
| producer       = 
| writer         = Harold Shumate
| starring       = Tim McCoy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
}}

Hell Bent for Love is a 1934 American crime film directed by D. Ross Lederman.   

==Cast==
* Tim McCoy as Police Captain Tim Daley
* Lilian Bond as Millicent Millie Garland
* Bradley Page as Trigger Talano
* Vincent Sherman as Johnny Frank
* Lafe McKee as Dad Daley Harry C. Bradley as Professor
* Wedgwood Nowell as Attorney Kelly Drake
* Eddie Sturgis as Major Dawson Ernie Adams as Henchman Joe Barnard
* Hal Price as Duke Allen
* Gloria Warner as Hannah Brown
* Max Wagner as Ernest Dallas
* Guy Usher as Police Chief OBrien
* Edward LeSaint as Judge (as Ed LeSaint)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 