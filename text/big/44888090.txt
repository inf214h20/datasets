The Road to Love
{{Infobox film
| name           = The Road to Love
| image          = 
| alt            = 
| caption        =
| director       = Scott Sidney
| producer       = Oliver Morosco
| screenplay     = Blanche Dougan Cole Gardner Hunting 
| starring       = Lenore Ulric Colin Chase Lucille Ward Estelle Allen Gayne Whitman Herschel Mayall
| music          = 
| cinematography = James Van Trees
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Scott Sidney and written by Blanche Dougan Cole and Gardner Hunting. The film stars Lenore Ulric, Colin Chase, Lucille Ward, Estelle Allen, Gayne Whitman and Herschel Mayall. The film was released on December 7, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Lenore Ulric as Hafsa
*Colin Chase as Gordon Roberts
*Lucille Ward as Lella Sadiya
*Estelle Allen as Zorah
*Gayne Whitman as Karan 
*Herschel Mayall as Sidi Malik
*Joe Massey as The Old Sheik
*Alfred Hollingsworth as Abdallah

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 