Iceman (2014 film)
{{Infobox film name           = Iceman image          = Iceman3D.jpg caption        = China poster film name      =   director       = Law Wing-cheung producer       = Huang Jianxin   Stephen Shiu   Stephen Shiu Jr.   Donnie Yen writer         = Mark Wu   Lam Fung   Shum Shek-yin starring       = Donnie Yen Wang Baoqiang Huang Shengyi Simon Yam Yu Kang Lam Suet music          =  cinematography =  editing        =  studio         = China 3D Group distributor    =  released       =   runtime        = 104 minutes (Hong Kong)   91 minutes (China) country        = Hong Kong China language       = Cantonese Mandarin    Hindi   English   Japanese budget         = HK$250 million (US$32,244,750)    gross          = US$26,027,923  
}} Hong Kong-Cinema Chinese  3D Martial martial arts action director. 1989 film The Iceman Cometh which was directed by Clarence Fok and starred Yuen Biao, who was earlier reported to join the film.  Donnie Yen hand-picked Jam Hsiao for his unique voice and deep emotions to sing the mandarin theme song.  The Cantonese version is sung by Hong Kong singer and actor Julian Cheung. The film was released in Hong Kong and China on April 25, 2014.   

The sequel is slated for release in 2015.

==Plot==
Iceman revolves around a Ming Dynasty officer, He Ying (Donnie Yen) who was tasked with bringing a mythical time traveling device back to the Ming emperor. He was betrayed, and subsequently frozen. He Ying, Sao(Wang Baoqiang) and Niehu were frozen during a fight and were defrosted in modern day Hong Kong where they continue their battle.

He Ying, Sao (Wang Baoqiang),and Niehu (Yu Kang) are being transported in cryo-stasis pods, when an unforeseen accident sets them free. The three of them escape into the city to fulfill their tasks. Niehu and Sao are dead set in exacting revenge on He Ying, despite not understanding the new world they woke up in. He Ying soon befriends May (Huang Shengyi), who takes advantage of Hes confusion at the modern world by charging him extraordinary amounts of money for rent, food, and so on. Niehu and Sao unknowingly helps 2 Indian mobsters escape the police, and as a result joins the Indian mafia.

All the while, it is slowly revealed that Cheung (Simon Yam) is the one who is looking for the trio, but in particular He Ying. Through various flashbacks, it is revealed that all 4 of them were blood brothers who fought side by side, before He Ying was betrayed and charged with treason. 

After several hilarious encounters in the modern world including meeting Mays mother, fleeing custody of the police (using his master martial arts skills), and the use of technology, He Ying starts to figure out who is ultimately hunting him down.

==Cast==
*Donnie Yen as He Ying    (賀英)
*Wang Baoqiang as Sao (薩獒)
*Huang Shengyi as May (小美)
*Simon Yam as Cheung (元龍)
*Yu Kang as Niehu (聶虎)
*Lam Suet as Tang
*Zhang Shaohua as He Yings mother Benny Chan as Celebrity
*Hu Ming as Eunuch
*Gregory Wong as Officer Szeto
*Jaqueline Chong as Jacqueline
*Mark Wu as Slander
*Ava Yu as Ava
*Alice Chan as Director of retirement home
*Lo Hoi-pang as Pang
*Wong Man-wai as Mays mother
*Chen Huihui as Mimi
*Bao Yumeng as Kid
*Kelly Fu as Female officer
*Fun Lo as Female doctor
*Tin Kai-man as Male doctor
*Jeana Ho as a prostitute speaking Japanese (cameo)

==Production==
Filming for Iceman began on December 19, 2012 in Hong Kong.  Originally produced at a budget of HK$100 million, the production soared up to HK$200 million due to its slow paced filming and to cover the film crew’s insurance.  Because the Hong Kong government did not approve the film to shoot at the Tsing Ma Bridge, an addition of HK$50 million was spent in order to build an imitation set of the Tsing Ma Bridge.   Lead actor and action director Donnie Yen expressed that a seven-minute fight scene took ten days to shoot. Besides Hong Kong, a chunk of the film would also be filmed in Beijing.  For a car chase scene, actor Julian Cheung also loaned his black Lamborghini to the production. 

Filming locations include Hong Kongs defunct Kai Tak Airport  and the Changbai Mountains.  

==Reception==
The Hollywood Reporter writes "Plagued by all manner of production snafus and a ballooning budget, the problems show in the final product. Iceman is a fractured and often baffling martial “epic” that not even popular star Donnie Yen is likely to be able to save."  Screendaily writes the film is "beset by multiple problems, from a patchy incoherent script, to jarring shifts in tone and genre, and sub-par action and effects sequences that even the star presence of Donnie Yen may find hard to reconcile."  Donnie Yens performance won him the Golden Broom Award for Worst Actor. 

==See also==
*Donnie Yen filmography
*The Iceman Cometh (1989 film)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 