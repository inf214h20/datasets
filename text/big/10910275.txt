Towncraft (film)
{{Infobox film

 | name = Towncraft
 | image_size = 
 | caption = 
 | director = Richard Matson
 | producer = Albert Lai Richard Matson
 | writer = 
 | narrator = 
 | starring = 
 | music = 
 | cinematography = 
 | editing = 
 | distributor = Matson Films
 | released = May 18, 2007 (United States)
 | runtime = 104 minutes
 | country = US
 | language = English
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image = TownCraft VideoCover.png
}}

Towncraft is a feature documentary on the independent music scene in Little Rock, Arkansas from 1986 to 2006.  Produced by Matson Films, Towncraft investigates the music that emerges from smaller cities and the importance of these local scenes, as well as their relationship to the larger music world.

Towncraft was released in theaters, on DVD and online on May 22, 2007. Towncraft was the first film to be released simultaneously on these three platforms. 

The film features interviews and performances by many Little Rock bands including Soophie Nun Squad, Ho-Hum, and The American Princes.

==Plot==
In the late 1980s a few kids in the small, sleepy Southern town of Little Rock, discovered punk rock and the DIY ethic that drove it. Unlike other towns, Little Rocks punk scene was composed almost entirely of junior high and High School kids. Over the next decade, they would book their own shows, start record labels, open record stores, play with national acts and formulate a collective set of ideals. In 1992, they released Towncraft (album)|Towncraft, a compilation album and zine that documented their scene.

Towncraft focuses on the roots of the Little Rock scene, how it changed the lives of those involved, the DIY ethos that has shaped the scene for the past 20 years and how the scene continues to thrive outside of the mainstream.

==Soundtrack==
A two-CD 40-band soundtrack was also released with the DVD in a box set, also containing a book. The soundtrack highlights a "Little Rock sound" that developed over two decades.

==Notes==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 


 