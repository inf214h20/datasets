True Blue (2001 film)
{{Infobox film
| name           = True Blue
| image          = True Blue 2001.jpg
| alt            =
| caption        = 
| director       = J.S. Cardone
| producer       = Scott Einbinder Carol Kottenbrook	
| writer         = J.S. Cardone
| starring       = Tom Berenger Lori Heuring Pamela Gidley Tim Jones
| cinematography = Darko Suvak
| editing        = Matthew Ramsey		 
| studio         = Sandstorm Films
| distributor    = 
| released       = 29 November 2001 	
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $1.5 million
| gross          =
}}
True Blue is a 2001 film written and directed by J.S. Cardone.

==Synopsis== New York Chinese Triads and possibly Nikki herself.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Tom Berenger || Rembrandt Macy 
|-
| Lori Heuring || Nikki 
|-
| Pamela Gidley || Beck 
|-
| Barry Newman || Monty 
|-
| Leo Lee || Benny Lee 
|-
| Joshua Peace || Oren Doba
|-
| Alec McClure || Bouton 
|-
| Pedro Miguel Arce || Bounce
|}

==Critical reception==
David Nussair of Reel Film Reviews gave the film 1/2 stars out of four:
 

Mitch Lovell of The Video Vacuum: 
 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 