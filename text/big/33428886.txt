Dilwale Kabhi Na Hare
{{Infobox film
| name           = Dilwale Kabhi Na Hare
| image          = 
| image_size     =
| caption        =
| director       = V.N. Menon
| producer       = Babubhai Thiba
| writer         = Mahendra Dehlvi
| narrator       = Prithvi  Nagma
| music          = Nadeem-Shravan
| cinematography = Mangesh Sawant
| editing        = Padmakar Nirbhavani
| distributor    = Sameer Productions
| released       = 25 September 1992
| runtime        = 150 min.
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Bollywood Prithvi and Nagma in pivotal roles. http://www.apunkachoice.com/titles/dil/dilwale-kabhi-na-hare/mid_13549 

==Cast==
*Rahul Roy...Rahul
*Prithvi (actor)|Prithvi...Vijay
*Nagma...Anjali Oberoi
*Varsha Usgaonkar...Shabnam
*Satish Shah...Gene
*Girja Shankar...D.K. Oberoi 
*Gajendra Chouhan...Shibu
*Rakesh Bedi...Pandit Aladdin Jagat Mama
*Guddi Maruti...Chintin
*Dinesh Anand...Inspector

==Soundtrack==
The Soundtrack Of The Movie Is Composed By The Music Duo Nadeem Shravan.The Song Lyrics Planned By Sameer.The album became a  hit and songs "Hum Pyar Karte Hain", "Tu Meri Hai" and "Dilwale Kabhi Na Hare" were very popular. Most of the songs were sung by Kumar Sanu and  Alka Yagnik along with Shabbir Kumar, Nitin Mukesh & Rajeshwari

{| class="wikitable"
! No !! Title !! Singer(s)
|-
| 1
| "Hum Pyar Karte Hain"
| Kumar Sanu, Alka Yagnik, Nitin Mukesh
|-
| 2
| "Dono Ke Husn Mein"
| Kumar Sanu, Rajeshwari
|-
| 3
| "Tu Meri Hai"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Dilwale Kabhi Na Hare"
| Kumar Sanu, Shabbir Kumar
|-
| 5
| "Ab To Bina Tumhare"
| Kumar Sanu
|-
| 6
| "Khushboo Tumhare Pyar Ki"
| Kumar Sanu, Alka Yagnik
|-
| 7
| "Main Hoon Naarangi"
| Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 
 

 