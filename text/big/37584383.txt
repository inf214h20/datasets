Refuge: Stories of the Selfhelp Home
REFUGE: Stories of the Selfhelp Home is a United States documentary by director Ethan Bensinger. It tells the story of the final generation of Holocaust survivors and refugees through the lens of the Selfhelp Home in Chicago, a little-known community which has provided a home to more than 1,000 Jewish Holocaust survivors and refugees since World War II. 

== Background ==
The film grew out of a project by the director, Ethan Bensinger to film interviews with the last survivors and refugees at the Selfhelp Home.   These interviews are now archived at Selfhelp Home, Spertus Institute of Jewish Studies in Chicago and the Leo Baeck Institute in New York. Out of the 30 survivors he recorded, just a dozen are alive today, most of them in the 90s and older. 

== Content ==
The survivors and refugees describe their memories of Kristallnacht (known as "Night of the Broken Glass"), the coordinated series of attacks by the Nazis against Jewish communities throughout Germany and Austria in 1938.     Others speak of finding refuge in England through the Kindertransport, escaping to the United States and Shanghai, living in hiding in France, and deportation to the Theresienstadt and Auschwitz concentration camps.   The film also explores their postwar lives and how they started new lives in Chicago. 

The film is narrated by historians, including Christopher Browning, Frank Porter Graham Professor of History at the University of North Carolina at Chapel Hill.

== Crew ==
* Director - Ethan Bensinger
* Producer - Beth Sternheimer
* Editor - Ruth Efrati Epstein
* Composer - Steve Zoloto
* Writer - Benjamin Avishai

== Premiere and reception ==
REFUGE premiered in June 2012 at the Illinois Holocaust Museum & Education Center in Skokie, Ill., and has been widely seen both in the Jewish community and at schools, libraries and theaters around the United States.  
REFUGE was chosen as an official selection at the following film festivals:  
* 2012 Sycamore Film Festival – Awarded Best of Fest and Best Documentary
* 2012 Ruby Mountain Film Festival
* 2012 Louisville’s International Festival of Film
* 2012 Crystal Palace International Film Festival (London)
* 2012 Fort Lauderdale International Film Festival
* 2012 East Lansing International Film Festival
* 2012 Weyauwega International Film Festival
* 2012 Alexandria International Film Festival
* 2013 Beloit International Film Festival
* 2013 Geneva Film Festival

== Selfhelp ==
In 1936, Selfhelp was founded in New York City by German-Jewish emigres to help European Jewish refugees fleeing the Nazi regime.   After Kristallnacht, a sister organization opened in Chicago, which provided housing, food, English classes and job placement services to other displaced Jewish émigrés and later, after the war, to Holocaust survivors.     In 1950, Selfhelp opened a residential home, which has given a home to more than 1,000 refugees and survivors. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 