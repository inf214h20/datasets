The Fire Brigade
__NOTOC__
{{Infobox film
| name           = The Fire Brigade
| image          =
| image_size     =
| caption        =
| director       = William Nigh
| story          = Kate Corbaley
| writer         = Robert N. Lee (adaptation) Lotta Woods (titles)
| narrator       = Charles Ray
| music          =
| cinematography = John Arnold
| editing        = Harry L. Decker
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = $249,556 
}}
 silent drama Charles Ray.  The Fire Brigade originally contained sequences shot in two-color Technicolor. A print of the film is preserved in the Metro-Goldwyn-Mayer/United Artists archives. 

The producers of the film contributed 25 per cent of the films receipts toward a college for the instruction of fire-fighting officers. 

==Plot==
Terry ONeil (Charles Ray) is the youngest of a group of Irish-American firefighting brothers. He courts Helen Corwin (May McAvoy), the daughter of a politician whose crooked building contracts resulted in devastating blazes.

==Cast==
* May McAvoy as Helen Corwin  Charles Ray as Terry ONeil 
* Holmes Herbert as James Corwin Tom OBrien as Joe ONeil 
* Eugenie Besserer as Mrs. ONeil 
* Warner Richmond as Jim ONeil 
* Bert Woodruff as Captain ONeil 
* Vivia Ogden as Bridget 
* DeWitt Jennings as Fire Chief Wallace 
* Dan Mason as Peg Leg Murphy 
* Erwin Connelly as Thomas Wainright

==See also==
*List of early color feature films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 