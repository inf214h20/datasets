The Gadfly (1980 film)
 Ovod}}
{{Infobox Film
| name           = Ovod
| image          = 
| image_size     = 
| caption        = 
| director       = Nikolai Mashchenko
| producer       = 
| writer         = Yuli Dunsky Valeri Frid Ethel Lilian Voynich (novel)
| narrator       = 
| starring       = Sergei Bondarchuk Anastasiya Vertinskaya
| music          = 
| cinematography = Sergei Statsenko
| editing        = Aleksandra Goldabenko
| distributor    = Dovzhenko Film Studio, Gosteleradio
| released       = 1980
| runtime        = 210 min.
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Soviet drama film directed by Nikolai Mashchenko based on the novel The Gadfly by Ethel Lilian Voynich. Its screenplay was written by Yuli Dunsky and Valeri Frid.

==Cast==
* Andrei Kharitonov
* Sergei Bondarchuk
* Anastasiya Vertinskaya
* Ada Rogovtseva
* Konstantin Stepankov
* K. Tzanev
* Stefan Dobrev
* Givi Tokhadze
* Kartlos Maradishvili (the role voiced actor Pavel Morozenko)
* Aleksandr Zadneprovsky
* Oleg Chajka
* Mircea Sotsky-Voinicescu
* Aleksei Kolesnik
* Vladimir Talashko

==External links==
* 

 
 
 
 
 
 
 
 


 
 