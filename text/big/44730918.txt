Kulirkaala Megangal
{{Infobox film
| name           = Kulirkaala Megangal
| image          = 
| image_size     =
| caption        =
| director       = C. V. Sridhar
| producer       = K. S. Srinivasan K. S. Sivaraman
| writer         = C. V. Sridhar (dialogues)
| screenplay     = C. V. Sridhar
| story          = C. V. Sridhar Arjun Jeevitha
| music          = Sankar Ganesh
| cinematography = Bhaskara Rao
| editing        = K. R. Ramalingam
| studio         = Vaasan Brothers
| distributor    = Vaasan Brothers
| released       =  
| runtime        = 
| country        = India Tamil
}}
 1986 Cinema Indian Tamil Tamil film, Arjun and Jeevitha in lead roles. The film had musical score by Sankar Ganesh.  

==Cast== Arjun
*Jeevitha
*Vanitha
*Jaishankar
*Thengai Srinivasan Manorama
*Venniradai Moorthy

==Soundtrack==
The music was composed by Shankar Ganesh.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Vaali || 04.27 
|-  Vaali || 03.53 
|-  Vaali || 04.07 
|-  Vaali || 04.39 
|-  Vaali || 04.56 
|}

==References==
 

==External links==
*  
 

 
 
 
 
 
 


 