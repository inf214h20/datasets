Satyam (2003 film)
{{Infobox Film |
  name     = Satyam |
  image          =|
  writer         = Surya Kiran B.V.S.Ravi |
  starring       = Sumanth Genelia DSouza Kota Srinivasa Rao |
  director       = Surya Kiran |
  producer       = Akkineni Nagarjuna |
  distributor    = Annapurna Studios |
  released       = December 19, 2003 |
  cinematography = Sameer Reddy | Nandamuri Hari 144 mins
  language = Telugu | Chakri |
  awards         = |
  budget         = |₹ 3.5 crores
}} Telugu film Surya Kiran. Chakri has won Filmfare Award for Best Male Playback Singer – Telugu. 
==Plot==
Satyam (Sumanth) is an aspiring lyric writer who has a knack for inadvertently getting misunderstood by the two people he cares for the most—his father (Malladi Raghava) and his love interest Ankita (Genelia DSouza). Because he and his father dont get along, Satyam leaves home and starts living with his friends. A sensitive man and a talented writer, Satyam ghost-writes for a popular film lyricist. He wants to prove himself as a lyricist before expressing his love for Ankita. In the meantime, a flamboyant classmate of Ankita proposes to her. The movie then witnesses a turn of events which involves Ankita`s father (Kota Srinivas Rao,) who happens to be a good friend of Satyam. How Satyam overcomes his obstacles and finally succeeds in courting Ankita forms the rest of the story.

==Cast==
*Sumanth as Satyam
*Genelia DSouza as Ankita
*Kota Srinivasa Rao as Shankar
*Bramhanandam as Lingam
*Tanikella Bharani as Chakradhar Sridhar as Prakash Kondavalasa as Simhadri Varsha as Swathi
*Raghava Malladi as Viswanath Chakri as Himself
*Lawrence Raghavendra (cameo in a song)

==Box-office performance==
*The film ran successfully for 50-days in 63 centers, and also completed its 100 day run in over 19 centers.  

== External links ==
*  

 
 
 
 
 

 
 