The Trouble with Spies
{{Infobox film
| name           = The Trouble with Spies
| image          = The Trouble with Spies.jpg
| image_size     = 
| caption        = 
| director       = Burt Kennedy
| writer         = 
| narrator       = 
| starring       = Donald Sutherland
| music          = 
| cinematography = 
| editing        = 
| studio         = HBO Pictures
| distributor    = De Laurentiis Entertainment Group
| released       = December 4, 1987
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Trouble with Spies is a 1987 film directed by Burt Kennedy. A spy spoof comedy, it stars Donald Sutherland and Ned Beatty.  

The film was shot in 1984, but not released until three years later. It includes one of veteran actress Ruth Gordons final performances.

==Plot==
When secret agent George Trent goes missing, spy agency chief Angus sends inept colleague Appleton Porter to the isle of Ibiza to find out why.

Appleton meets a number of guests in Mona Lewiss hotel who were familiar with Trent, but none has a clue what became of him. Appleton himself is totally clueless, nearly being killed a number of times but surviving mainly due to pure dumb luck.

==Cast==
*Donald Sutherland as Appleton Porter
*Ned Beatty as Harry Lewis
*Ruth Gordon as Mrs. Arkwright
*Lucy Gutteridge as Mona
*Michael Hordern as Jason Locke
*Suzanne Danielle as Maria Sola
*Robert Morley as Angus

==References==
 

==External links==
* 

 
 
 
 