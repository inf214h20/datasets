White Mission
{{Infobox film
| name =   White Mission 
| image =
| image_size =
| caption =
| director = Juan de Orduña
| producer = Jesús Rubiera
| writer = Pío Ballesteros   Jaime García Herranz   Antonio Mas Guindal   Jesús Rubiera   Ángel Torres del Álamo   Juan de Orduña
| narrator =
| starring = Manuel Luna   Jorge Mistral   Fernando Rey   Elva de Bethancourt Juan Quintero 
| cinematography = Michel Kelber 
| editing = Mariano Pombo
| studio = Colonial AJE
| distributor = Colonial AJE
| released = 1946
| runtime = 88 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} on location in Spanish Guinea  and in a Spanish studio. The films sets were designed by Sigfrido Burmann and Francisco Canet.

The film portrays a religious mission in the Spanish Empire.

==Cast==
* Ricardo Acero as Padre Mauricio  
* Gabriel Algara as Jiménez  
* Marianela Barandalla as Diana  
* Elva de Bethancourt as Souka  
* Juan Espantaleón as Cesáreo Urgoiti  
* Manuel Luna as Brisco  
* Arturo Marín as Padre Daniel  
* Jorge Mistral as Minoa  
* Nicolás D. Perchicot as Padre Suárez  
* Julio Peña as Padre Javier  
* Fernando Rey as Carlos 
* José Miguel Rupert as Padre de Souka 
* Jesús Tordesillas as Padre Urcola

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008.

== External links ==
* 

 

 
 
 
 
 
 


 