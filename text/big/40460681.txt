Libera Me (2000 film)
{{Infobox film name           = Libera Me image          = File:Libera_Me_(2000)_poster.jpg director       = Yang Yun-ho producer       = Hyun Chung-ryul   Hwang Jeong-wook writer         = Yeo Ji-na starring       = Cha Seung-won   Choi Min-soo music          = Lee Dong-jun cinematography = Seo Jeong-min editing        = Park Soon-deok distributor    =  released       =   runtime        = 107 minutes country        = South Korea language       = Korean budget         =   gross          =  hangul         = 리베라 메 rr             = Libera mae }}
}} action blockbuster film about a mentally-unbalanced arsonist and the firefighters who struggle to stop him.

==Plot==
Five months after his release from prison after serving a twelve-year sentence, arsonist Yeo Hee-soo terrorizes the city of Seoul with a series of deadly blazes. Yeo rigs each fire so that a second, far more lethal conflagration ignites shortly after the firefighters have arrived, causing further casualties. He then turns his attention to those members of the department he feels are interfering with his "mission", which develops into a game of cat and mouse with veteran fireman Jo Sang-woo. 

==Cast==
*Choi Min-soo - Jo Sang-woo
*Cha Seung-won - Yeo Hee-soo
*Yoo Ji-tae - Kim Hyun-tae
*Park Sang-myun - Park Han-mo
*Jung Joon - Lee Jun-seong Kim Gyu-ri - Hyun Min-seong
*Kim Su-ro
*Lee Ho-jae - Kim In-ho
*Park Jae-hoon
*Heo Joon-ho - Lee In-soo
*Jung Ae-ri - Jung Myung-jin
*Park Ji-mi
*Jeong Won-jung

==Production==
The film was shot in Busan, with the support of the city and its local fire department. Instead of using Miniature effect|miniatures, it was filmed in real buildings throughout Busan using a special synthetic oil that allowed the crew to use actual fire. For a key scene involving a gas station, a life-sized set was constructed and detonated at a cost of  . 

==Awards==
2000 Blue Dragon Film Awards 
*Best Visual Effects: Jeong Do-an

2001 Baeksang Arts Awards
*Grand Prize
*Best Film
*Best Actor: Choi Min-soo

2001 Chunsa Film Art Awards
*Best Cinematography: Seo Jeong-min
*Technical Award: Jeong Do-an

2001 Grand Bell Awards
*Best Cinematography: Seo Jeong-min
*Best Editing: Park Soon-deok
*Best Lighting: Shin Joon-ha

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 
 