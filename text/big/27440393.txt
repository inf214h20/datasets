A Little Help
 
{{Infobox film
| name           = A Little Help
| image          = A Little Help film poster.jpg
| director       = Michael J. Weithorn
| producer       =  
| writer         = Michael J. Weithorn
| starring       =  
| studio         = Secret Handshake Entertainment
| distributor    = Freestyle Releasing 
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| gross          = $96,868 
}}
A Little Help is a 2010 American comedy-drama film written and directed by Michael J. Weithorn. It follows the experiences of a dental hygienist following her unfaithful husbands sudden death. It debuted on May 21, 2010 at the Seattle International Film Festival. 

==Cast==
*Jenna Fischer as Laura Pehlke
*Chris ODonnell as Bob Pehlke
*Daniel Yelsky as Dennis Pehlke
*Rob Benedict as Paul Helms Brooke Smith as Kathy Helms
*Kim Coates as Mel Kaminsky
*Leslie Ann Warren as Joan Dunning
*Ron Leibman as Warren Dunning
*Arden Myrin as Ms. Gallagher
*Aida Turturro as Nancy Feldman
*James Rebhorn as Dr. Bronstein
*Joy Suprano as Julie Cantoni
*Sam McMurray as Big Bad Dan
*Dion DiMucci as himself

==References==
 

==External links==
*  
*  

 
 
 
 
 

 