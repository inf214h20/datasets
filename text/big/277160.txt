Entrapment (film)
{{Infobox film
| name           = Entrapment
| image          = Entrapment film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Jon Amiel
| producer       = Sean Connery Michael Hertzberg Rhonda Tollefson
| screenplay     = Ronald Bass William Broyles, Jr.
| story          = Ronald Bass Michael Hertzberg
| starring       = Sean Connery Catherine Zeta-Jones
| music          = Christopher Young
| cinematography = Phil Meheux
| editing        = Terry Rawlings
| studio         = Regency Enterprises
| distributor    = 20th Century Fox
| released       =  
| runtime        = 113 minutes
| country        = United States United Kingdom Germany English
| budget         = $66 million
| gross          = $212,404,396 
}}
Entrapment is a 1999 American caper film directed by Jon Amiel and starring Sean Connery and Catherine Zeta-Jones.

==Plot==
Virginia "Gin" Baker (  and plan the complicated theft at Macs hideout, an isolated castle. Aaron Thibadeaux (Ving Rhames), apparently the only ally that Mac has and trusts, arrives with supplies for the heist. While Mac is busy making final preparations, Gin contacts her boss, Hector Cruz (Will Patton), from a payphone, and informs him of Macs whereabouts. Little does she know that the island is bugged, allowing Mac to eavesdrop on their conversation.  Mac also makes sure to keep Gins romantic advances at bay, unsure if she is a true partner in crime or an ambitious career woman on a mission.

After they have stolen the mask, Mac accuses Gin of planning to sell the mask to a buyer in  . During their set-up, Cruz and his team (with the guidance of the stealthy Thibadeaux) track down Gin and confirm that she is still on mission to bring Mac in. security watching Pudu train station.

Gin arrives at the   and the FBI heads to the next station. Gin jumps trains mid-station and arrives back at Pudu. She tells Mac that she needs him for another job and they both board a RapidKL Light Rail Transit|train.
 

==Cast==
* Sean Connery as Robert "Mac" MacDougal
* Catherine Zeta-Jones as Virginia "Gin" Baker
* Will Patton as Hector Cruz
* Ving Rhames as Aaron Thibadeaux
* Maury Chaykin as Conrad Greene
* Kevin McNally as Haas Terry ONeill as Quinn
* Madhav Sharma as Security Chief
* David Yip as Chief of Police
* Tim Potter as Millennium Man Eric Meyers as Waverly Technician Aaron Swartz as Cruzs Man William Marsh as Computer Technician
* Tony Xu as Banker
* Rolf Saxon as Director

==Filming locations==
 
 Savoy Hotel London, Lloyds of London, Borough Market, London, Duart Castle on the Isle of Mull in Scotland, the Petronas Towers in Kuala Lumpur (with other filming completed at Pinewood Studios), and the Bukit Jalil LRT station However, the signage at this station that was used for the movie was Pudu LRT station instead of Bukit Jalil. {{Cite web
|url=http://www.imdb.com/title/tt0137494/locations
|title=Filming Locations for Entrapment
|work=imdb.com
|publisher=Internet Movie Database
|accessdate=October 21, 2012
}} 

==Critical reception==
 
 Paul Harris, Harry Potter and the Order of the Phoenix.

Other critics such as The New York Times,  New York Magazine,  the Chicago Sun-Times,  Variety (magazine)|Variety,  and Desson Howe/Thomson of the Washington Post  praised the film.
 The Haunting) and Worst Screen Couple (Zeta-Jones and Sean Connery).

The film was screened out of competition at the 1999 Cannes Film Festival.   

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 