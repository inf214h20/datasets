Station West
{{Infobox film
| name           = Station West
| image          = Station west poster small.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Sidney Lanfield
| producer       = Robert Sparks Frank Fenton Winston Miller
| based on       =  
| narrator       =
| starring       = Dick Powell Jane Greer Agnes Moorehead Burl Ives
| music          = Heinz Roemheld
| cinematography = Harry J. Wild
| editing        = Frederic Knudtson
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Luke Short. 

The interest of the film lies in the sharp dialogue between Powell and Greer as much as in its plot. Burl Ives plays a small role and sings the following songs on the soundtrack: "A Stranger in Town," "The Suns Shining Warm," and "A Man Cant Grow Old." 

==Plot==
Two soldiers have been robbed and murdered while guarding a shipment of gold. Into town rides Haven, a military intelligence officer traveling incognito.

A beautiful saloon singer catches Havens eye. After he meets Mrs. Caslon, who owns the gold mine, Haven hears that someone called "Charlie" is the brains behind the scene. He finds out to his surprise that Charlie is the singer.

Charlies lawyer, Bristow, is $6,000 in her debt and therefore might be involved in the gold theft. Haven beats up Charlies saloon bouncer in a fight and is offered a job as transport chief for the gold. Charlies friend, Prince, meanwhile, is growing jealous of her interest in Haven.

While transporting a shipment of gold, the man riding shotgun, Goddard, is killed and Haven knocked cold. When he comes to, he manages to track, catch and kill the robber carrying the gold. He shoos away the dead mans horses and follows them to their home stable, at the sawmill owned by Charlie. Haven pretends to be an ignorant hand working for Charlie, and is charged with transporting the stolen gold in the horses saddlebags back to town to Charlie and Prince.

He hides the gold, and confronts Prince and Charlie. After some to-ing and fro-ing with the gold and an affidavit dictated to Bristow by Haven, Charlie convinces Bristow that he ought to confront Haven. Haven convinces him rather that he is the next target of Prince and Charlie as he knows too much. Bristow, terrified, tries to get away but is shot by Prince. Haven is pinned down, but after persuading the sheriff to arrest him for the crime, Haven escapes, and learns that Charlies men plan to disguise themselves as military officers to steal more of Mrs. Caslons gold.

He foils this plot, then arrives back at the saloon, to arrest Charlie, but also because he is in love with her. Prince sneaks up intending to shoot Haven, but his bullet hits Charlie instead. Haven kills Prince and rushes to Charlies side. She tells Haven she loves him before dying, and he that he loves her.

He rides away as Burl Ives sings that a man cant grow old where theres women and gold.

==Cast==
* Dick Powell as Haven
* Jane Greer as Charlie
* Agnes Moorehead as Mrs. Caslon
* Burl Ives as Hotel Clerk/Balladeer
* Tom Powers as Capt. George Isles
* Gordon Oliver as Prince Steve Brodie as 2nd Lt. Stellman Guinn Williams as Mick Marion
* Raymond Burr as Mark Bristow
* Regis Toomey as Jim Goddard
* Olin Howland as Cook
* John Berkes as Pianist
* Michael Steele as Jerry Dan White as Pete John Kellogg as Ben

==Accolades==
* Writers Guild of America: Best Written American Western - Frank Fenton and Winston Miller; 1949.

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*   at Classic Film Guide
*  

 

 
 
 
 
 
 
 
 
 
 