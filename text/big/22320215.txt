Pasanga
{{Infobox film
| name = Pasanga
| image = Pasanga_DVD.jpg image_size = 
| director = Pandiraj
| producer = M. Sasikumar
| writer = Pandiraj
| starring = Vimal Vega Tamotia Jayaprakash Kishore Sree Ram
| music = James Vasanthan
| cinematography = Premkumar
| editing = Yoga Baskar
| Sound Recordist=Raj shekar..k
| studio = Company Production
| released =  
| runtime = 151 minutes
| country = India
| language = Tamil
| budget = 
| gross =  11 crore 
}} Tamil film Sasikumar and National Film Awards and several other awards at International Film Festivals. 

==Plot==
The story unfolds in a dry barren village. Anbukkarasu (Kishore), the protagonist who wants to excel in his life as a Collector and is an encouraging child, comes across Jeevanandam (Sree Raam), the son of the school teacher Chokkalingam(JayaPrakash), on the first day of school. Jeeva develops instant dislike for Anbu. The good-hearted Anbu tries to befriend Jeeva. However, Jeeva continues to hate Anbu with a passion. Enter Manonmani (Dharini), Jeevas cousin, who develops affinity towards Anbu. This only makes Jeeva detest Anbu further. Not only Anbu excels in academics but also in extracurricular activities, contributes to widening the rift between the two.Anbus parents have diiferent opinions at Life which develops fight between them and also turns out to be a positive for Jeeva to hurt Anbu further.

A fight between Anbu and Jeeva escalates to involve their parents and this results in strife between their respective families as well.Even though as elders,they soothe out and solve the rift between them when Jeevas father tells the importance of forgiving in life as from his experience to Anbus father which makes him go along in a good way with his wife. However, in a twist, Meenakshi Sundaram (Vimal), Anbu’s uncle falls in love with Soppikannu (Vega), the sister of Jeeva. The families get united when they agree to Meenakshi Sundaram’s wedding with Soppikannu, much against the wishes of Jeeva and Anbu whiich continues the rift further.

At the end of their 6th grade,when Jeevas father asks everyone to a write a letter about their past year good/bad happenings which makes Anbu and Jeeva share their feelings about one another. Anbu shares his likings towards Jeeva while Jeeva stands as he was in the beginning.Manonmani makes Jeeva understand the coming of Anbu has changed his life to a good living being.All of a sudden Anbu meets with an accident.Jeeva as first,helps Anbu recover by using the encouraging therapy. He apologises and they both reunite ending with Meenakshi Sundarams marriage with Soppikannu.

==Cast==
* Kishore as Anbukkarasu Vellaichamy
* Sree Raam as Jeeva Nithyanandham Chokkalingam
* Vimal as Meenakshi Sundaram
* Vega Tamotia as Soppikannu Chokkalingam
* Pandian as Kuzhanthaivelu (Pakkada)
* Dharani as Manonmani
* Murugesh  as Kuttimani
* Karthikraja as Gautham Vellaichamy (Bujjima)
* Yoganathan as Akila
* Jayaprakash as Chokkalingam
* Sivakumar as Vellaichamy
* Sujatha as Mrs. Chokkalingam
* Senthikumari as Pothumponnu Vellaichamy

==Accolades==
; Awards
57th National Film Awards Best Child Artist - Kishore & Sree Raam Best Dialogues - Pandiraj Best Feature Film in Tamil

57th Filmfare Awards South Best Supporting Actor - Jayaprakash

4th Vijay Awards Best Supporting Actor - Jayaprakash Best Debut Actor - Vimal Best Find of the Year - Pandiraj Best Crew

; Other awards
* Anandha Vikatan Award for Best Director - Pandiraj
* Anandha Vikatan Award for Best Film
* Chennai International Film Festival for Second Best Feature Film
* International Childrens Film Festival Golden Elephant Award for Best Director - Pandiraj
* Pondicherry Government Sankaradas Swamigal Award For Best Indian Film
* South Scope Cine Awards for Best Film
* World Malayali Councils Essar Award for Best Director - Pandiraj

;International Film Festival
* 19th Golden Rooster And Hundred Flowers Film Festival  (Jiangyin,China)
* Chinese Festival Of India 2010 (China)
* 6th International Children Film Festival 2010 (Bangalore, India)
* 40th International Film Festival Of India 2009 (Goa) 
* 2nd International Children Film Festival Bangladesh
* Pune International Children Film Festival
* Lucknow International Film Festival - Best Film (Nominee) and Best Director (Nominee)
* SILPIX Childrens Film Festival" in America, Chicago 2011
* Norway International Film Festival 2010
* Children’s Film Festival, Singapore - 2011

==Music==
{{Infobox album
| Name = Pasanga
| Type = soundtrack
| Artist = James Vasanthan
| Cover = 
| Released =  
| Recorded = 
| Genre = Film soundtrack
| Length = 19:40
| Label = Think Music
| Producer = 
| Reviews =
| Last album  = Subramaniyapuram (2008)
| This album  = Pasanga (2009)
| Next album  = Naanayam (2009)
}}
Music and soundtracks were composed by Subramaniyapuram fame James Vasanthan. The soundtrack features four songs, the lyrics for which are written by Thamarai, Yugabharathi and James Vasanthan himself.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Lyrics !! Notes
|- 1 || Naandhaan Goppanda || Sathyanarayanan, Larson Cyril || 4:32 || Yugabharathi ||
|- 2 || Oru Vetkam Varudhe ||   ||
|- 3 || Whos that Guy || Benny Dayal || 2:02 || James Vasanthan ||
|- 4 || Anbaale Azhagaagum || M. Balamurali Krishna, Baby K.Sivaangi || 6:06 || Yugabharathi ||
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 