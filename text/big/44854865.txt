Jackson (2015 film)
{{Infobox film
| name           = Jackson
| image          = 
| caption        = 
| director       = D. Sanath Kumar
| producer       = Sundar P. Gowda   Anil 
| writer         = Gokul
| dialogues      = Ravikiran
| starring       = Duniya Vijay Pavana Gowda  Rangayana Raghu
| music          = Arjun Janya
| cinematography = Selvam
| editing        = Suresh S. A.
| studio         = KPS Combines   Duniya Talkies
| distributor    = Bahar Films
| released       =  
| runtime        = 143 min.
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kannada romantic Tamil blockbuster film Idharkuthane Aasaipattai Balakumara (2013) which was directed by Gokul. The film opened on the screens on 15 January 2015. 

==Production==

===Development===
Producers Sundar Gowda and Anil bought the remake of the Tamil film just a week after its release. They approached their friend Duniya Vijay who accepted to play the lead role and help his friends produce the film in his home banner "Duniya Talkies". It was reported that he accepted the role and offered to work in it for free without any remuneration. The film was officially launched on 20 January 2014 in Bangalore co-inciding the birthday of Vijay. 

===Casting===
After finalizing Vijay for the lead role, the producers approached actress Pavana Gowda of Gombegala Love fame to play the female lead reprising the role of Swati Reddy in the original version. Actor Rangayana Raghu was roped in to play a key supporting role.

==Cast==
* Duniya Vijay as Jackson
* Pavana Gowda as Kumudha
* Rangayana Raghu
* Abhi
* Bullet Prakash
* Aishwarya Sindogi
* Nagendra Shah
* Satyajith
* Deepa
* Kote Prabhakar

==Soundtrack==
Music composer Arjun Janya was finally roped in to score for both score and soundtrack after names such as Veer Samarth were doing rounds during the launch of the film. One track composed by Veer Samarth was retained in the final tracklist. The lyrics are written by Yogaraj Bhat and Chethan Kumar.

{{Infobox album  
| Name        = Jackson
| Type        = Soundtrack
| Artist      = Arjun Janya
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Anand Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Avnu Micheal Jacksonu
| lyrics1 	= Yogaraj Bhat
| extra1        = Arjun Janya
| length1       = 
| title2        = Opposite House Kumudha
| lyrics2 	= Chethan Kumar
| extra2        = Arjun Janya
| length2       = 
| title3        = Godu Kelu Godu
| lyrics3 	= Chethan Kumar
| extra3        = Naveen Sajju
| length3       = 
| title4        = Endendigu Naguthiru ( )
| lyrics4 	= Chethan Gandharva
| extra4        = Chethan Kumar
| length4       = 
}}

==References==
 

==External links==


 
 
 
 
 
 
 
 
 
 