Away with Words
 
 
{{Infobox film
| name           = Away With Words
| image          = Away_With_Words.jpg
| image_size     =
| caption        = DVD cover of film from its release in India.
| director       = Christopher Doyle 
| producer       = Hiro Tokimori Noburu Uoya
| writer         = Christopher Doyle Tony Rayns
| starring       = Tadanobu Asano
| music          = 
| cinematography = Christopher Doyle
| editing        = Anne Goursaud
| distributor    = 
| released       = 7 August 1999
| runtime        = 90 minutes
| country        = Hong Kong Japan
| language       = Cantonese English Japanese
| budget         = 
| gross         = 
| preceded_by    = 
| followed_by    = 
}} Chinese title Japanese title auteur trilingual Mavis Xu. flashbacks into Asanos characters childhood in Okinawa. The protagonist suffers from overbearing excesses of his memory, mnemonic associations and synesthesia. The emerging human attachments provide an emotional center and a source of serenity to offset the rampage of the protagonists mind and tame the lavish disarray of urban imagery.
 Borges (presumably Luria for inspiration. Many aspects of Asanos character (memory excess, profound synesthesia, arranging memories visually along roads, wordplay, struggling with an onslaught of associations, comments about restaurant music and its effect on food taste, the waking-for-school scene) are directly borrowed from Alexander Luria|Lurias real life case study The Mind of a Mnemonist.

The film was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

==Cast==
* Tadanobu Asano - Asano
* Georgina Hobson
* Christa Hughes
* Kevin Sherlock - Kevin
* Mavis Xu

==See also==
* Solomon Shereshevskii (Lurias patient whose life description shaped Asanos character in Doyles film)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 
 