Sally (1929 film)
{{Infobox film name = Sally (1929) image = Sally1929.jpg producer  =  director = John Francis Dillon  writer = Waldemar Young  A.P. Younger  based on the Broadway musical by Guy Bolton and P.G. Wodehouse starring = Alexander Gray music = Jerome Kern  Leonid S. Leonardi Irving Berlin Al Dubin Joe Burke art direction = Jack Okey cinematography = Charles Edgar Schoenbaum  (Technicolor) editing = LeRoy Stone distributor =   released =   runtime = 103 minutes language = English country = United States
|}} all talking On with The Desert On with Gold Diggers of Broadway (1929), Paris (1929 film)|Paris (1929), and The Show of Shows (1929). (Song of the West was actually completed by June of 1929 but had its release delayed until March of 1930). Although exhibited in a few select theatres in December of 1929, Sally only went into general release on January 12, 1930.
 Broadway stage hit, Sally (musical)|Sally, produced by Florenz Ziegfeld (which played at The New Amsterdam Theatre, from 12/21/1920 to 4/22/1922),  and retains three of the stage productions Jerome Kern songs ("Look for the Silver Lining", "Sally", and "Wild Rose"), the rest of the music newly written for the film by Al Dubin and Joe Burke. {{Cite book   | last = Bradley
  | first = Edwin M.   | authorlink =   | title = The First Hollywood Musicals: A Critical Filmography of 171 Features, 1927 Through 1932   | publisher = McFarland & Company   | year = 1996   | pages = 87–90   | isbn = }}  Marilyn Miller, who had played the leading part in the Broadway production, was hired by the Warner Brothers at an extravagant sum (reportedly $1000 an hour for a total of $100,000) to star in the filmed version.  The film was nominated for an Academy Award for Best Art Direction by Jack Okey in 1930.    

==Plot==
 
Sally (Marilyn Miller) plays the part of an orphan who had been abandoned as a baby at the Bowling Green telephone exchange. While growing up in an orphanage, she discovered the joy of dancing. In an attempt to save money enough to become a dancer, Sally began working at odd jobs. While working as a waitress, a man named Blair (Alexander Gray) begins coming to her work regularly to see her. They both soon fall for each other. Sally, however, does not know that Blair has been forced into an engagement by his family with a socialite named Marcia (Nora Lane). One day, a theatrical agent shows up at Sallys work (T. Roy Barnes) and gives her a chance to audition for a job. Sally, however, ends up losing her job and the opportunity when she drops a tray of food into Barnes lap. Eventually, Sally gets another job at the Elm Tree Inn, which is managed by Ford Sterling. Blair drops in one day and immediately takes an interest in Sally. He convinces Sterling to have Sally dance for his customers. While she is performing one day, the theatrical agent (T. Roy Barnes) notices her and convinces Sally to impersonate a famous Russian dancer named Noskerova at a party being given by Maude Turner Gordon. At that engagement, she is found to be an imposter and is asked to leave. Before Sally leaves, however, she hears the announcement of Blairs engagement to Marcia. Undaunted, she proceeds with her life and eventually becomes a star on Broadway. Unfortunately she never forgets Blair and becomes terribly depressed until...

==Preservation==
 
While never technically a "lost" film, Sally was unavailable for public viewing for nearly six decades. Warner sold filmography pre-1950 to Associated Artists Productions|AAP/UA, or destroyed nitrate copy this films in end 1940s and 1950s. It was not until around 1990 when the film was once again available for archival and revival screenings. However, the film survives only in black and white except for a 2 -minute color segment from the Wild Rose musical number, which was discovered in the 1990s and inserted into the print currently in circulation. Sepia-toned black-and-white footage is inserted to replace individual frames which are missing in the color fragment.

==Cast==
*Marilyn Miller as Sally
* Alexander Gray as Blair Farrell Joe E. Brown as Grand Duke Connie Constantine
*T. Roy Barnes as Otis Hemingway Hooper
*Pert Kelton as Rosie, Otis girlfriend
*Ford Sterling as Pops Shendorff
*Maude Turner Gordon as Mrs. Ten Brock
*E. J. Ratcliffe as John Farquar Jack Duffy as The Old Roue
*Ethel Stone as Lutie
*Nora Lane as Marcia

==See also==
*List of early color feature films
*List of incomplete or partially lost films

==References==
 

== External links ==
*  
* 

 

 
 
 
 
 
 
 
 
 