Two Weeks in Another Town
{{Infobox film
| name           = Two Weeks in Another Town
| image          = Poster - Two Weeks in Another Town 01.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 1962 Theatrical Poster
| director       = Vincente Minnelli 
| producer       = John Houseman
| writer         = Charles Schnee
| screenplay     = 
| story          = 
| based on       = Two Weeks in Another Town by Irwin Shaw
| narrator       =  George Hamilton   Claire Trevor   Daliah Lavi   Rosanna Schiaffino 
| music          = David Raksin 
| cinematography = Milton R. Krasner
| editing        = Adrienne Fazan   Robert James Kern John Houseman Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 107 minutes
| country        = United States
| language       = English 
| budget         = $3,959,000  . 
| gross          = $2,500,000 
}} George Hamilton, and Rosanna Schiaffino.  

The film depicts the shooting of a romantic costume drama in Rome by a team of decadent Hollywood stars. It contains several references to a previous successful Minnelli movie, The Bad and the Beautiful, also starring Douglas.

The story was seen by some as a reelaboration of the past relationship between actors Tyrone Power and Linda Christian and producer Darryl Zanuck.

==Plot==

Once an established movie star, Jack Andrus has hit rock bottom. An alcoholic, he has been divorced by wife Carlotta, has barely survived a car crash and has spent three years in a sanitarium recovering from a nervous breakdown.

Maurice Kruger, a film director who once was something of a mentor to Andrus, is also a has-been now. However, he has landed a job in Italy, directing a movie that stars a handsome, up-and-coming young actor, Davie Drew.

Andrus is offered a chance to come to Rome and play a role in Krugers new film. He is crestfallen upon arriving when told that the part is no longer available to him. Krugers mean-spirited wife, Clara, doesnt pity him a bit, but Andrus is invited to take a lesser job assisting at Cinecitta Studio with the dubbing of the actors lines.

While working, he socializes with the beautiful Veronica, but she actually is in love with Drew. The actor is having a great deal of difficulty with his part and the movie is already over budget and behind schedule. Krugers stress also is increased by the constant harping of Clara, resulting in a heart attack that sends the director to the hospital.

Andrus is asked to take over the directors chair and complete the film. Glad to do this favor for Kruger, he takes charge and gets the film back on schedule. The actors respond to him so much that Drews representatives tell Andrus the actor will insist on his directing Drews next film.

Proud of what he has done, Andrus goes to Kruger in the hospital, delighted to report the progress hes made, only to be attacked by Clara for trying to undermine Kruger and steal his movie from him. Andrus is shocked when Kruger sides with her.

An all-night descent into an alcohol-fueled rage follows. Carlotta goes along as a drunken Andrus gets behind the wheel of a car and races through the streets of Rome, nearly killing both of them.

At the last minute, Andrus comes to his senses. He vows to return home, continue his sobriety and get his life back on track.

==Production==
George Hamilton was cast as "a troubled, funky James Dean-type actor, for which I couldnt have been less appropriate" as he later admitted. George Hamilton & William Stadiem, Dont Mind If I Do, Simon & Schuster 2008 p 157 
==Reaction==
===Critical===
Bosley Crowther in his New York Times review of August 18, 1962 wrote: "The whole thing is a lot of glib trade patter, ridiculous and unconvincing snarls and a weird professional clash between the actor and director that is like something out of a Hollywood cartoon."
===Box Office===
According to MGM records the film earned $1 million in the US and Canada and $1.5 million elsewhere, leading to an overall loss of $2,969,000.  . 

==Cast==
* Kirk Douglas as Jack Andrus
* Edward G. Robinson as Maurice Kruger
* Cyd Charisse as Carlotta
* Claire Trevor as Clara Kruger
* Daliah Lavi as Veronica George Hamilton as Davie Drew
* Rosanna Schiaffino as Barzelli James Gregory as Tom Byrd

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 