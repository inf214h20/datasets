Zatoichi and the Fugitives
 
{{Infobox film
| name = Zatoichi and the Fugitives
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市果し状
| romaji         = Zatōichi hatashijō
}}
| director = Kimiyoshi Yasuda
| producer = Ikuo Kubodera
| writer = Kinya Naoi
| based on       =  
| starring = Shintaro Katsu Takashi Shimura Yumiko Nogawa
| music = Hajime Kaburagi
| cinematography = Kazuo Miyagawa
| editing        = Kanji Suganuma
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 82 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichi and the Fugitives is the eighteenth episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
 
Zatoichi comes to a village which is the local hub of family farms in a silk growing region. The most prominent villager is Matsugoro who apparently will stop at nothing to bring all under his control. The small village does have a kind doctor, Dr. Junan, who lives with his daughter Oshizu.

Boss Matsugoro appears to be very prosperous. As a minor gangster he has a large retinue of toughs he uses to bully and intimidate others and he owns a silk weaving factory where at least some of his weavers work to service debt and are actually in bondage. He has also acquired some appointed local political position from which he plans to gain more benefits.

Into this situation enters a group of fugitives - criminals who have caused a lot of trouble elsewhere and have a reputation for shocking and unnecessary cruelty. Amongst this group of men is one woman - Oaki. Also amongst these men is Ogano, the disowned son of Dr. Junan from whom hes been estranged since Ogano killed a man 5 years ago in the town of Edo.

The fugitives and Boss Matsugoro reach an arrangement where he will allow them to hide out above his weaving factory and they will do some unnamed strong arm favors for him when asked.

Zatoichi and the fugitives have some minor clashes due to their pugnacity but Ogano prevents these situations from escalating as he knows Zatoichi could easily kill his fellow ruffians if provoked because he witnessed Zatoichi slice a snake in half as it fell through the air from a tree branch when he happened to pass Zatoichi eating his lunch in the woods previously.

Zatoichi and Boss Matsugoro have a final falling out when Zatoichi compels him to allow a sick weaver to return home with her father, release her from compulsory service and burn her bond papers. Matsugoro orders his henchmen to get rid of the troublesome Zatoichi and they wound him severely but he escapes and goes into hiding.

Believing Dr. Junan or his daughter Oshizu to know Zatoichis whereabouts, Matsugoro has his men severely beat the doctor but he refuses to talk. They then take his daughter away and since she wont talk either, Matsugoro orders his men to strip her naked and hang her up. Just as they begin to struggle with her Zatoichi appears to rescue Oshizu and Dr. Junan.

Still weak and severely wounded from his previous fight, Zatoichi nevertheless defeats those holding Oshizu and instructs a redeemed Oaki to lead her to her father back at the factory. The women are discovered by some of Matsugoros men and are about to be questioned when Zatoichi appears and kills all but 1 who runs to warn Matsugoro.

This leads to a large running sword battle inside Matsugoros house where many of his men and finally Matsugoro himself are slain by Zatoichi.

Following the house battle Zatoichi goes outside to rest and is set upon by the fugitives, all of whom he kills until only Ogano is left to face him. Just as their battle is about to begin the women and Dr. Junan arrive to witness Zatoichi swiftly put an end to Oganos life. 

Oshizu cradles her brothers dead body and cries while Zatoichi backs up and then begins to walk away, alone again as the background song says "only the cicadas cry ...".

==Cast==
*Shintaro Katsu as Zatoichi
*Takashi Shimura as Junan
*Kayao Mikimoto as Oshizu
*Kyosuke Machida as Ogano Genpachiro
*Yumiko Nogawa as Oaki
*Hosei Komatsu as Boss Matsugoro
*Shobun Inoue as Kumeji
*Jotaro Semba as MInokichi
*Jutaro Hojo as Genta
*Koichi Mizuhara as Sennosuke 

==Reception==
===Critical response===
J. Doyle Wallis, in a review for  , the helmer of Zatoichis Cane Sword and Daimajin, this entry has a pretty hard and sinister edge. While Ichi always tangles with the bad guys, they are usually typical faceless hordes of just-going-about-their-job gangsters or stone faced, expert swordsmen willing to test their skills for the satisfaction or the money. But, this gang has a vicious streak and a willingness to slaughter the innocent that makes them irredeemable. Likewise, while Ichi has been shown to get hurt or be in trouble, this entry finds him beaten and shot, bloodied and battered more so than usual, or at least until the next to last, original cycle film, Zatoichi In Desperation."   

==References==
 

==External links==
* 
* 
* 
* , review by Steve Kopian for Unseen Films (20 February 2014)
* , by Mark Pollard for Kung Fu Cinema
* , review by D. Trull for Lard Biscuit Enterprises
* , by Thomas Raven for freakengine (April 2012)
* , review by JB for Stuff You Gotta Watch
* , review by Bill Hunt and Todd Doogan for The Digital Bits (20 May 2004)
* , review by Paghat the Ratgirl for Wild Realm Reviews

 

 
 
 
 
 
 
 
 

 