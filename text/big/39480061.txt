Free Samples
 
 

{{Infobox film
| name           = Free Samples
| image          = 
| alt            = 
| caption        = 
| director       = Jay Gammill
| producer       = Joseph McKelheer Eben Kostbar
| writer         = Jim Beggarly
| based on       = 
| starring       = Jess Weixler Halley Feiffer Jesse Eisenberg Jason Ritter Tippi Hedren
| music          = Say Hi
| cinematography = Reed Morano
| editing        = 
| studio         = Film Harvest
| distributor    = Starz Distribution
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Free Samples is a 2012 American independent comedy starring Jess Weixler and Jesse Eisenberg. The film was the directorial debut of Jay Gammill and the writing debut of Jim Beggarly.  

==Plot== Stanford law-school ice cream vendor truck in a parking lot somewhere-in-Southern California|SoCal, and on the day of her college boyfriends birthday, unleashing an unmerciless barrage of hyper neuroticism, abusing her legal practice training on the simple locals and experiencing a rollercoaster of emotions with her unsympathetic so-called friends. Although Jillian claims that shes "stuck in that stupid truck", its only for one day and at the end of it, ends it in a fancy restaurant. 

==Cast==
* Jess Weixler as Jillian
* Jesse Eisenberg as Tex
* Jason Ritter as Wally
* Halley Feiffer as Nancy
* Tippi Hedren as Betty
* Keir ODonnell as Danny
* Jocelin Donahue as Paula
* Whitney Able as Dana

==Release==
The film premiered at Tribeca Film Festival on April 20, 2012, and given a video-on-demand release on May 21, 2013 and theatrical release on May 31, 2013. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 


 