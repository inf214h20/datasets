Ruined Heart! Another Love Story Between a Criminal and a Whore
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Ruined Heart! Another Love Story Between a Criminal and a Whore
| image          =
| caption        = Ruined Heart poster by Dodo Dayao
| director       = Khavn
| producer       = Khavn Fe Virtudazo-Hyde Achinette Villamor
| writer         = Khavn
| starring       = Ian Lomongo Cara Eriguel
| cinematography = Albert Banzon
| editing        = Lawrence S. Ang
| music          = Arvie Bartolome Fando & Lis Angus Lorenzo Steven Chua
| studio         = Kamias Road
| released       =  
| runtime        = 15 minutes
| country        = Philippines
| language       = Tagalog
}} Filipino short film written and directed by independent filmmaker Khavn De La Cruz.    It is the first Philippine entry to be officially selected in the International Competition of the short film section of the Berlin International Film Festival. It competed at the 62nd Berlin International Film Festival.   

The films original title is in  , however, it was given the German title Zerstörtes Herz!.

Ruined Heart! is the classic love story between a gangster and a prostitute, but Khavn "deconstructs this often recounted tale to explore in fleeting images the likelihood of dying too early from the violence so omnipresent in the Philippines today".   

==Production==
Ruined Heart! was filmed in 24 hours, in six different places in Metro Manila. The main location was the filmmakers residence in Kamias Road. Other locations include the walled city, Intramuros; a bar in Marikina; a rooftop near Roxas Boulevard; a slum in Cambridge Street; and a vacant lot in Quezon City.

Khavn, considered the father of digital filmmaking in the Philippines,    is well known for his guerilla tactics: he shoots very fast with a very small crew. He explains that while more demanding on himself and on his crew, this shooting style is both practical and efficient because:  The longer it takes to make a film, the more expensive it is so I keep my shoots short and sweet. It has to do with energy, too. Like improvisational music played on the piano: you have this moment to create something right then and there, you just follow the beat. It’s the idea of “best thought, first thought.” The best take is the first take. All the “mistakes” ultimately become part of the piece, the set design. You allow the world into your films.     

==Awards==
{|class=wikitable
!Year
!Film Festival/Award
!Result
!Category/Recipient
|-
| 2012 || Golden Bear, Berlin International Film Festival || Nominated || Best Short Film
|}

==References==
 

==External links==
*  
*   by Bayani San Diego Jr.,  
*   by VVP.,  

 
 
 
 
 
 
 