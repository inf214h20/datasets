Valhalla (film)
{{Infobox Film
| name           = Valhalla
| image          = Valhalla (film).jpg
| image_size     = 
| caption        =  Peter Madsen Jeffrey J. Varab
| producer       = Anders Mastrup Peter Madsen Henning Kure Niels Søndergaard
| starring       = Dick Kaysø Preben Kristensen
| music          = Ron Goodwin
| cinematography = Niels Grønlykke Jan-Erik Sandberg
| editing        = Lidia Sablone
| distributor    = Metronome
| released       = October 10, 1986 (Denmark) January 2, 1987 (U.S.)
| runtime        = 76 minutes
| country        = Denmark Danish
| DKK40 million  (estimated)
| preceded_by    = 
| followed_by    = 
}}
 Danish animation|animated comic book Scandinavian Folklore|tales Younger Edda Peter Madsen. 
 Danish film of 1986 and proved popular with audiences, however the company failed to regain the cost of production and, as a result, the film became a financial flop at the box office.

==Plot==
The movie takes plot elements told from the three comic albums "Cry Wolf", "The Story of Quark" and "The Journey to Útgarða-Loki". 

Thor, the god of thunder, and Loki, the god of lies, habitually visiting Midgard (Earth), takes refuge for the night at a lonesome farm house, inhabited by a couple of ordinary Viking peasants and their two children, a boy named Tjalvi and his younger sister Röskva. Thor generously offers one of his (immortal) goats which is dragging his chariot, as a feast dinner for all of them, but strongly warns any of the members of the household from breaking the bones. Loki, always treacherous, persuades the boy Tjalvi into doing exactly that, for the sake of the good marrow inside. Thor goes on to recount the Hymiskviða, specifically the part when he fished for the Midgard Serpent, Jörmungandr, to entertain his hosts.  

The next morning, Thor revives his two goat, but is infuriated when he discovers that the animal has become Physical disability|lame, caused by the breaking of the bone the previous night. Thor is craving for revenge, but Loki instead suggests that they take the boy Tjalvi with them to Asgard as a servant, which Thor reluctantly accepts to. Tjalvi on his part is not very courageous on going to Asgard or the prospect of serving the mighty gods either, but his sister Röskva is very adventurous and keen to go, even though she isnt even a prospect (being a girl). Eventually the gods and their new servant is going, leaving the parents and going to Asgard via Bifröst. Well arrived, they soon discover that Röskva has kept hiding in the chariot, and so she is allowed to follow the company and her brother to Thors home Bilskirnir where his wife Sif and their two infant children are awaiting. Here, the two human children are put to hard physical work immediately, having to clean not only the mansion with its 540 rooms (sic) but also nursing the two infants, Þrúðr & Móði and Magni|Móði, and serving the gods. 
 nonverbal Jötunnboy named Quark (disambiguation)|Quark, which almost immediately causes havoc in the thundergods home, and which Loki at first claims followed him home, but finally professes he won, when he lost a bet with Útgarða-Loki and now has to keep him until he behaves properly. Sif is so dispared, when she finds out Quark has to stay at Bilskirnir, that she leaves the home with the two infants in distress, which in turn, makes Thor go crazy and destroy everything around him, until he finally leaves, leaving Loki, Quark, Röskva and Tjalvi behind.

Soon, the children and Quark find they have something in common and befriend each other, while Loki just makes himself comfortably in his new home. He acts as a lazy and cruel master of the house and the children and Quark finally run away to look up the mighty head of the gods  . Since Loki cant or wont bring Quark back, Thor forces him, by escorting him along with Röskva and Tjalvi.

The group travel  , the aging itself. Thor returns to his former self, but he and Loki has to keep Quark. As they leave Útgarða-Lokis castle Quark turns into a chicken. Loki has used his magic to trick everyone that it was Quark who know has to stay with the other jötunn. This saddens both him and Röskva. Thor gives Tjalvi a sword, a token of that he know sees him as a man. Home in Bilskirnir, Röskva walks out to the forest and suddenly Quark appears, having run away. The friends are now reunited.  

==Production==
The project was originally developed by character animators Jeffrey J. Varab and Jakob Stegelmann.  , British Film Institute, accessed 3 December 2012 

They had previously established an animation-school in Copenhagen and trained most of the animators who would eventually work on the film. They managed to raise a small budget for the feature film adaptation before the project was eventually passed to production companies. 

During production, the project ran into severe financial difficulties and was passed on between studios before being made by Swan Film. 
 Peter Madsen, who had drawn the comic books and been the films art director, is credited as director, and Jeffrey J. Varab is credited as co-director. 

At the time of its release, in October 1986  it was Denmarks most expensive film ever made, having cost around 40 million Danish krone|kroner. No Danish film has ever gone as much over budget since.

===Soundtrack===
The soundtrack was composed by Ron Goodwin and was performed by the Copenhagen Collegium Musicum orchestra.  The soundtrack was released in 1987 and re-released in 1998.   The 45 minute soundtrack would be Goodwins last film composition. 

Track Listing
 
 
*01. Opening Title
*02. Thors arrival
*03. Childrens theme
*04. Thors fishing tale
*05. Morning of the magic hammer
*06. Farewell theme
*07. Rainbow bridge
*08. At home with Thor and Sif
*09. Arrival of Loki and Quark
*10. The wrath of Thor
*11. The children in the forest
 
*12. Building the tree-house
*13. Whisteling theme
*14. Giants theme
*15. Eating competition
*16. Drinking competition
*17. Thor lifting the cat
*18. Dancing with Elle
*19. Tjalfe and Thor
*20. Giant party
*21. Farewell to Quark
*22. Tjalfe gets his sword
*22. Finale
 

===Casting===
The Danish release version of the film features the voices of Dick Kaysø, Preben Kristensen, Laura Bro and Marie Ingerslev. The film was translated into other languages including English and German. During the production and animation stages, it was initially animated using an  English language soundtrack featuring the voices of a primarily non-Danish cast. 
 
 
Danish Version
* Dick Kaysø as Thor
* Preben Kristensen as Loki
* Laura Bro as Røskva
* Marie Ingerslev as Tjalfe
* Nis Bank-Mikkelsen as Odin / Útgarða-Loki
* Benny Hansen as Hymir
* Olaf Nielsen as Rolf
* Thomas Eje as Quark
* Claus Ryskjær as Ravnen Hugin
* Kirsten Rolffes as Ravnen Munin
* Jesper Klein as Mimer
* Susse Wold as Sif / Elle
* Allan Corduner as Loke (as Alan Corduner),
* Percy Edwards (Incidental voices)
 
Swedish Version
* Ernst Günther as Thor
* Ernst-Hugo Järegård as Loki
* Kyri Sjöman as Röskva
* Peter Zell as Tjalfe
* Gunnar Öhlund as Odin / Útgarða-Loki / Munin
* Margret Andersson as Sif / Hugin / The Mother
* Thomas Ungewitter as Hymir / The Father
 
English Version
* Stephen Thorne as Thor
* Hans Clarin as Loki
* Suzanne Jones as Roskva
* Alexander Jones as Tjalfe
* Michael Elphick as Útgarða-Loki
* John Hollis as Hymir
* Mark Jones  as Odin
* Dagmar Heller as Sif
* Thomas Eje as Quark
* Geoffrey Matthews (Incidental voices)
* Percy Edwards as (Incidental voices)
 
German Version
* Christopher Lee as Thor
* Radost Bokel as Roskva
* Hans Clarin as Loki
* Manfred Erdmann as Rolf
* Alice Franz as Elda
* Dagmar Heller as Sif
* Monika John as Mutter
* Christian Tramitz as Schildkröte
 

==Box Office and Aftermath==
The movie was released in October 1986.  More than 100 artists had worked hard on the film for 4 years and the total cost of the film was DKK 35 million. (5 million euro). The movie was an instant hit and for several years and reached number 3 on the Danish box office list. In spite of selling more tickets than any other Danish film in 1986, its heavy cost inevitably made it a box-office flop. Swan Film made eight spin-off short films for television featuring Quark, the troll character in both the books and the film, before eventually closing down.

The financial collapse of the Valhalla-production also brought down its sister company, LASER, which had been developing an animated feature film adaptation of Gilgamesh and a laserdisc video game, Pyramid, about a female hero battling various enemies inside an ancient temple structure. However, four animators and one producer prior to completing Valhalla regrouped and formed a new company, A. Film A/S, which to date is Denmarks most successful animation studio, producing frequent artistic triumphs and box-office hits.

==See also==
*List of animated feature-length films

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 