The Enchanting Shadow
 
{{Infobox film
| name           = The Enchanting Shadow
| image          = 
| caption        = 
| director       = Li Han-hsiang
| producer       = Run Run Shaw Duwen Zhou
| writer         = Songling Pu Yueting Wang
| starring       = Betty Loh Ti
| music          = 
| cinematography = Luying He
| editing        = Hsing-lung Chiang
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}} Best Foreign Language Film at the 33rd Academy Awards, but was not accepted as a nominee.  

==Cast==
* Betty Loh Ti as Nie Xiaoqian (as Di Le)
* Ngai Fung
* Li Jen Ho
* Kun Li as Scholars Servant
* Kuo Hua Li
* Chi Lu
* Hsiang Su
* Rhoqing Tang as Lao Lao
* Yueting Wang
* Chih-Ching Yang as Yan Chixia (as Zhiqing Yang)
* Lei Zhao as Ning Caichen

==See also==
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 