Onathumbikkoru Oonjaal
{{Infobox film
| name           = Onathumbikkoru Oonjaal
| image          =
| caption        =
| director       = NP Suresh
| producer       = PKR Pillai
| writer         = Raghunath Paleri
| screenplay     = Raghunath Paleri
| starring       = Bharath Gopi Rameswari Srividya Nedumudi Venu
| music          = A. T. Ummer
| cinematography = Babu Joseph
| editing        = NP Suresh
| studio         = Shirdi Sai Creations
| distributor    = Shirdi Sai Creations
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, directed by NP Suresh and produced by PKR Pillai. The film stars Bharath Gopi, Rameswari, Srividya and Nedumudi Venu in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Bharath Gopi as Augustine
*Rameswari as Gracy
*Srividya
*Nedumudi Venu
*Isaac Thomas
*Vijayan Karote
*Rajarathnam
*Baby Seetha
*Gopalakrishnan
*Kannur Sreelatha
*MG Soman
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Onathumbikkoroonjaalu || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Onnaanam Kunnirangi Vaavaa || S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 