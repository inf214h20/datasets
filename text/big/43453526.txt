Bullet for Hire
 
 
{{Infobox film
| name           = Bullet for Hire
| image          = BulletforHire.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 子彈出租
| simplified     = 子弹出租
| pinyin         = Zǐ Dàn Chū Zū
| jyutping       = Zi2 Daan2 Ceot1 Zou1 }}
| director       = Yuen Chun Man
| producer       = Chan Kin Ting
| writer         = 
| screenplay     = Yuen Chun Man Leung Wai Ting
| story          = 
| based on       =
| starring       = Jacky Cheung Simon Yam Dick Wei Lo Lieh Sheila Chan
| narrator       = 
| music          = Mak Hiu Lun
| cinematography = Johnny Koo
| editing        = Marco Mak
| studio         = Vediocam Film Golden Princess Amusement
| released       =  
| runtime        = 93 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$3,646,104
}}
 Hong Kong action film written and directed by Yuen Chun Man and starring Jacky Cheung, Simon Yam, Dick Wei, Lo Lieh and Sheila Chan.

==Plot== Triad assassins Ngok (Lo Lieh), Hon (Simon Yam) and Shan (Jacky Cheung) are struggling in a battle for money and power in Hong Kong. Ngok and Hon has worked for triad boss Dick (Dick Wei) for over ten years and the two have the two established a profound friendship. Because they were clever, the two were heavily utilized by Dick. However, Ngok is getting old and worried that Dick will harm his only daughter San (Elaine Chow). With the help of the police, Ngok was able to fake his death during an assassination attempt.

Shan, on the other hand, is a newcomer killer. While following Hon, Shan gets to learn about the fickleness of the world. One time after winning HK$100,000 from a bet, he encounters a message girl Lan (Sheila Chan) and develops a romantic relationship with her. At this time, Dick suspects about Ngoks death and sends Hon and Shan to kill him. Dick also kidnaps Lan to blackmail Shan and at this critical moment of deciding the fate of three of them, the choices of life and death are in front in front of them.

==Cast==
*Jacky Cheung as Shan
*Simon Yam as Hon
*Dick Wei as Mr. Dick
*Lo Lieh as Ngok
*Sheila Chan as Lan
*Elaine Lui as Interpol officer
*Elaine Chow as San
*Lam Chung as Bill (cameo)
*Ma Kei as Kwai (cameo)
*Hung San Nam as Ngoks victim in flashback
*Wan Seung Lam as Mr. Dicks thug
*Lee Chun Kit as Mr. Dicks thug
*Paul Wong as Mr. Dicks thug
*Wong Chi Keung as Mr. Dicks thug
*Lung Tin Sang as Boss playing darts in pub
*Johnny Cheung as Bosss thug/Mr. Dicks thug
*Ridley Tsui as Bosss thug
*Johnny Yip
*Tam Chuen Leung
*Danny Chow as one of Kwais men
*Tam Chuen Hing as Superintendent Henry Tso Hon
*Jameson Lam as Restaurant captain
*Ken Yip as Ngai Chai Wans bodyguard
*Tsim Siu Ling
*Hung Yan-yan
*Roger Thomas as Mr. Wilson
*Johnny Koo as Kau
*Huang Kai Sen as Assassin
*Benny Lai as Bosss thug
*Ka See Fung as Bosss thug
*Hon Chun as Bosss thug
*Kwok Nga Cheung

==Theme song==
*Resurrection (復活)
**Composer: Donald Ashley
**Lyricist: Calvin Poon
**Singer: Jacky Cheung

==Reception==

===Critical===
  gave the film a score of 8/10 and writes "Its an erratic, irreverent, almost anything goes flick with comedy thats actually comical and some explosive action sequences that get the squibs bursting." 

===Box office===
The film grossed HK$3,646,104 at the Hong Kong box office during its theatrical run from 5 to 11 September 1991 in Hong Kong.

==See also==
*Jacky Cheung filmography
*Lo Lieh filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 