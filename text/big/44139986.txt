Ithrayum Kaalam
{{Infobox film
| name           = Ithrayum Kaalam
| image          =
| caption        =
| director       = IV Sasi
| producer       = NG John
| writer         = T. Damodaran
| screenplay     = T. Damodaran Madhu Mammootty Jose
| Shyam
| cinematography = Jayanan Vincent
| editing        = K Narayanan
| studio         = Shabana- Dayana
| distributor    = Shabana- Dayana
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Jose in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
  Madhu
*Mammootty
*Shobhana Jose
*Manavalan Joseph
*Ratheesh
*Sankaradi
*Unni
*Captain Raju Shankar
*Prathapachandran
*Sathaar
*Aloor Elsy
*Anjali Naidu
*Balan K Nair
*Bheeman Raghu
*Kundara Johny
*Kannur Sreelatha
*Krishna Kurup
*Kuthiravattam Pappu
*Lalu Alex
*Nanditha Bose
*Rahman
*Sabitha Anand Seema
*Shafeeq
*Sonia (Baby Sonia)
*Soorya
*Surekha
*TG Ravi
*Thodupuzha Vasanthi
 

==Soundtrack== Shyam and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madhumadhuram || Krishnachandran, Lathika || Yusufali Kechery || 
|-
| 2 || Mannaanithu || Krishnachandran, Lathika || Yusufali Kechery || 
|-
| 3 || Sarasa Sringaarame || P Jayachandran, Unni Menon, Jolly Abraham, Lathika || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 