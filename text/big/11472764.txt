The Ninja Mission
The Swedish action film directed by Mats Helge, starring the Polish actors Krzysztof Kolberger and Hanna Pola. All other roles are played by largely unknown Swedish and British actors.

==Story== Soviet Union and taken hostage by the KGB, who have an interest in his latest technological invention. After his daughter Nadia is also kidnapped and brought deep into the heart of Russia, a group of highly trained ninjas, led by American CIA agent Mason, is ordered to get them out.

== Reception ==

Although the film had a very modest release in its native Sweden, The Ninja Mission did well on the foreign market. It was released in 54 countries and achieved cult status in many Asian countries.

==Releases==

As of February 2013, there has not been any legitimate DVD or Bluray|Blu-ray release of the movie, due to objections by Mats Helge. An underground DVD edition has however been made available on Swedish markets by the distributor Horsecreek Entertainment, whose CEO Charles Aperia also served as the movies producer. The DVD edition is uncut and features dubbed English audio with optional Swedish subtitles.

Bo F Munthe stated that Charles Aperia got the idea for the film after he saw a brief draft for a novel Munthe he was working on, entitled Ninja-mördare i svart (Ninja Assassin in Black).

==Filming locations==

Filming took place in Stockholm (Havregatan), Lidköpings city hospital and harbour (with the former acting as the Leningrad institute), as well as the Halden fortress in Norway (acting as the UN building used by the KGB).

== External links ==
* 
* 

 
 
 
 
 
 


 
 