Surf Ninjas
{{Infobox film
| name           = Surf Ninjas
| image          = Surfninjasposter.jpg
| alt            = Three teenage boys stand on a yellow surfboard, riding a tidal wave.  They wear yellow-and-orange bandannas, are armed with sword-like weapons, and are smiling.  Beneath their surfboard is an old, armored man with a shocked look on his face, reaching up with his right hand for help.  Beside the surfboard are the words "Surf Ninjas", followed by a dragon emblem.
| caption        = Theatrical poster
| director       = Neal Israel
| producer       = Evzen Kolar Dan Gordon Neal Israel
| starring       = Ernie Reyes, Jr. Rob Schneider Leslie Nielsen
| music          = David Kitay
| cinematography = Arthur Albert Victor Hammer
| editing        = Tom Walls
| distributor    = New Line Cinema
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $4,916,135
}} martial arts, Dan Gordon.  The film stars Ernie Reyes Jr., Rob Schneider, Nicolas Cowan, and Leslie Nielsen.  Surf Ninjas follows two teenage surfers from Los Angeles who discover that they are crown princes of the Asian kingdom Patusan and reluctantly follow their destinies to dethrone an evil colonel that rules over the kingdom.

Surf Ninjas was filmed in Los Angeles, Hawaii, and Thailand. A video game was also developed and released in conjunction with the film.  Surf Ninjas was released in the United States on August 20, 1993, becoming popular but being received generally unfavorably by critics. The film was released on VHS in December 1993 and re-released on DVD in September 2002.

==Plot==
{|cellspacing="0" style="padding-left: 1.5em;"
|- valign="top"
|
{| cellpadding="0" cellspacing="0"
|-
| Ernie Reyes, Sr.
|&nbsp;... Zatch
|-
| Ernie Reyes, Jr.
|&nbsp;... Johnny
|-
| Nicolas Cowan
|&nbsp;... Adam
|-
| John Karlen
|&nbsp;... Mac
|}
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|
{| cellpadding="0" cellspacing="0"
| Rob Schneider
|&nbsp;... Iggy
|-
| Leslie Nielsen
|&nbsp;... Colonel Chi
|-
| Kelly Hu
|&nbsp;... Ro-May
|-
| Tone Loc
|&nbsp;... Lieutenant Spence
|}
|}
Johnny and Adam are teenage surfers who live in Los Angeles with their father Mac. Two weeks before Johnnys 16th birthday, ninjas attack the teenagers, but they are defeated by Zatch, a mysterious warrior with an eye patch. A follow-up attack results in the kidnapping of the father, though Zatch is able to protect the teenagers and their friend Iggy from the ninjas. Adam discovers that the video game on his Sega Game Gear matches the events happening around him and finds he can control some events through his Sega.  Zatch reveals to Johnny and Adam that they are actually the sons of the king of Patusan, whose land and monarchy was overthrown by the evil Colonel Chi when the boys were very small. It is their destiny to return to Patusan, overthrow Colonel Chi, and free the people. Zatch takes them to the Patusani district in Los Angeles, where Johnny is introduced to a Patusani princess, Ro-May, who has been betrothed to Johnny since they were infants.

Ninjas again attack, but Johnnys abilities as a warrior prince emerge and he defeating several of his foes. Johnny, Adam, Iggy, Zatch, and Ro-May decide to return to Patusan. They are followed by a Los Angeles detective, Lieutenant Spence, who had been investigating the ninja attacks. They reaches Patusan and discover what Colonel Chis rule has meant, including a burned village and a chain gang of political prisoners. The guards spot them and they are forced to fight. Johnny and Adam defeat them and free the villagers are freed from their captivity.

Zatch leads to a hidden cave in which the ancient weapons of the Patusani monarchy are preserved. Zatch arms Johnny and attacks him to prepare him for future challenges. Johnny is beaten repeatedly, but he is finally able to disarm Zatch. They rally the villagers and they travel to the coast opposite from an island that houses the royal city and Colonel Chis dungeon. Unable to go by boat due to an impassable reef, Johnny and Adam tell the Patusanis to make surfboards. They then paddle to the unguarded side of the island.

Landing at the island, Johnny and Zatch lead the attack on the royal city, taking down Chis henchmen.  Johnny and Adams adoptive father Mac is freed, and Johnny confronts Colonel Chi, successfully defeating him (by falling in the water) with the help of Adam and his Game Gear.  With Chis rule undone, peace is restored to Patusan.  Johnny is seated as the heralded warrior prince with Ro-May as his princess and Adam as a prince.  Johnny declares for the monarchy to be dissolved and announces that Patusan would operate as a democracy under the people.

==Production== Sega of Dan Gordon said that he wrote action sequences that would both suit the film and serve as a springboard for the video game.   In the film, one of the lead characters is shown playing the Surf Ninjas video game on a Sega Game Gear.  The video game was released before the films release, and it was considered the first movie-based video game to be developed concurrent with the movie and it was also the first to precede the film itself.   

==Release==
The studio New Line Cinema released Surf Ninjas two weeks earlier than its commercial release date in Evansville, Indiana and Lubbock, Texas as part of a test of regional markets.  The early release marked the first time that a major film was released in Evansville before its national opening without any local ties.  The president of theatrical marketing at New Line, Chris Pula, selected Evansville for its family-oriented audiences.  Pula explained, "Evansville is traditionally a strong family market.  Also, we have a strong relationship with the exhibitors in that area."  The president said that the studio was testing the film in a larger market than usual due to its uncertainty about the films reception, and that the studio would measure its marketing success with ticket receipts. 

Surf Ninjas was widely released in 1,321 theaters in the United States on August&nbsp;20, 1993.  Over its opening weekend, the film grossed  ,  placing 13th in box office rankings ahead of Manhattan Murder Mystery.   Surf Ninjas ultimately grossed   in the United States.     The film was released on VHS on December&nbsp;29, 1993.   It was subsequently released on DVD on September 3, 2002. 

==Critical reaction==

Janet Maslin of The New York Times called most of Surf Ninjas "only mindlessly watchable" and called the film "another of Hollywoods efforts to prove that the American mall mentality is at home in any corner of the globe".  Maslin also found the film to lack in actual surfing content.   Lynn Voedisch of the Chicago Sun-Times described Surf Ninjas as "a marriage of pop icons that simply was fated to be", citing childrens love for ninjas, especially the Teenage Mutant Ninja Turtles, and for the surf culture.  Voedisch considered Rob Schneiders presence as comic relief unfunny, believing that Leslie Nielsen should have received more screen time as the dictator.   Calvin Wilson of The Kansas City Star called the film "a disgrace... even by Hollywood standards", seeing it as a mess of child lead roles, unfunny cameo roles by Schneider and Nielsen, martial arts action, and lame jokes.  Wilson considered the story "stale and uninspired" that involved "people we dont care about doing things we cant believe". 

Stephen Hunter of The Baltimore Sun thought the films lead Ernie Reyes, Jr. was too old and too muscular to be received believably as a 15-year-old.  Hunter otherwise found the Reyes to impress with their fighting skills, though the films martial arts sequences were "bloodless and absurd".  Hunter also criticized the director for depriving the film of personality, with its lack of danger, seriousness, or spontaneity.   Richard Harrington of The Washington Post found the film to be "a harmless summers entertainment" for young people who enjoyed the Teenage Mutant Ninja Turtles#Feature films|Teenage Mutant Ninja Turtles films and 3 Ninjas.  Harrington enjoyed Reyes, Jr. as the protagonist but found Nielsen to be disappointing.   Paul Sherman of the Boston Herald thought that Surf Ninjas was "little more than a succession of dudespeak, surfing, skateboarding, video games, generic rock soundtrack and strained knucklehead humor".  Sherman admired the story arc in which the protagonists learn to accept their destinies, but he thought that "the manufactured thrills along the way get obnoxious".  Sherman thought that the film would only appeal to children under 12 years old, though the films locations in Thailand in the second half added an exotic atmosphere. 

Desmond Ryan of The Philadelphia Inquirer thought that Leslie Nielsen was deceptively portrayed in a major role similar to that of Lieutenant Frank Drebin from the The Naked Gun (film series)|The Naked Gun films, instead having merely "a running and unfunny gag about his malfunctioning answering machine and generally wasted otherwise".  Ryan also found the films dialogue to be "painful" and considered Surf Ninjas to be "beyond airheaded".   Mick LaSalle of the San Francisco Chronicle considered the story of Surf Ninjas to be "harmless and painfully dull".  LaSalle thought that the pacing of the film was too long with only "two smirks over the course of 90 minutes".   Sean Piccoli of The Washington Times thought that the films "dull stretch" was buoyed by the presence of Rob Schneider.  Piccoli compared the martial arts choreography in the film to the "cartoon fantasies that little boys re-enact on neighbors lawns: The good guys, alone and outnumbered by the charging horde, air-punch their way to glory." 

Ron Weiskind of the Pittsburgh Post-Gazette perceived Reyes, Jr. as "a likable presence on screen" and Schneider to be occasionally humorous in his series of gaffes.  Weiskind thought that even with the abundance of martial arts in the film, the scenes were generally too lifeless.  Joe Holleman of the St. Louis Post-Dispatch thought that Surf Ninjas pushed "the right buttons to guarantee adolescent enjoyment".  Holleman acknowledged that the film was "not exactly a milestone in cinematic achievement", but he applauded the acrobatic choreography and the delivery of Schneiders throwaway lines in "the movies funniest moments".   Sean P. Means of The Salt Lake Tribune described the film as a Toys "R" Us version of Indiana Jones and the Temple of Doom, with "the cartoonish martial-arts sequences   their entire existence to the villains stupidity".  Means thought that the film was ultimately "as silly as it is forgettable". 

==References==
 

== Further reading ==
* 
* 

== External links ==
* 
* 

 
 

 
 
   
   
 
 
 
 
 
 
 
 