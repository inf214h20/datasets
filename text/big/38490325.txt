Sampoorna Ramayanam (1958 film)
{{Infobox film
| name           = Sampoorna Ramayanam சம்பூர்ண இராமாயணம்
| image          =
| image_size     =
| caption        =
| director       = K. Somu
| producer       = M. A. Venu
| writer         = A. P. Nagarajan
| screenplay     = A. P. Nagarajan
| based on       =  
| narrator       = Padmini T. K. Bhagavathi P. V. Narasimha Bharathi  Chittor V. Nagaiah S. V. Ranga Rao Garikapati Varalakshmi|G. Varalakshmi
| music          = K. V. Mahadevan
| cinematography = V. K. Gopanna
| editing        = T. Vijayarangam
| studio         = M. A. V. Pictures
| distributor    = M. A. V. Pictures
| released       =   
| runtime        = 204 mins
| country        =   India Tamil
| budget         =
}} Tamil Mythology film directed by K. Somu. It is based on the Valmikis Ramayanam. The film features Sivaji Ganesan and N. T. Rama Rao in lead roles.  Arudra wrote dialogues and lyrics for the Telugu version. 

==Story==
The story is the complete Ramayana from the birth of Lord Rama to his Pattabhisheka after completing his exile.

==Cast== Bharatha
*N. T. Rama Rao as Rama Padmini as Sita
*P. V. Narasimha Bharathi as Lakshmana
*T. K. Bhagavathi as Ravana
*Chittor V. Nagaiah as Dasaratha
*S. D. Subbulakshmi as Kausalya
*Garikapati Varalakshmi|G. Varalakshmi  as Kaikeyi
*V. K. Ramasamy (actor)|V. K. Ramasamy  as 
*M. N. Rajam as Surpanakha
*Sandhya as Mandodari

==Crew==
*Producer: M. A. Venu
*Banner: M. A. V. Pictures
*Director: K. Somu
*Music: K. V. Mahadevan
*Lyrics: A. Maruthakasi
*Based on: Valmikis Ramayanam
*Screenplay: A. P. Nagarajan
*Dialogue: A. P. Nagarajan
*Cinematography: V. K. Gopanna
*Editing: T. Vijayarangam
*Art: Ch. E. Prasad Rao, M. K. Kutty Appu
*Choreography: M. Sampath Kumar, Chinnilal
*Songs Recording: T. S. Rangasamy
*Audiography: C. V. Rangaswamy Iyer

==Soundtrack==
The music composed by K. V. Mahadevan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Annaiyum Pithavumaagi || T. M. Soundararajan || rowspan=9|A. Maruthakasi || 02:31
|-
| 2 || Indru Poi Naalai || C. S. Jayaraman || 02:52
|-
| 3 || Neethi Thavarathu || Sirkazhi Govindarajan || 01:03
|-
| 4 || Pathugaiye Thunaiyagum || T. M. Soundararajan, Sirkazhi Govindarajan || 02:49
|-
| 5 || Sabarikku Raamanum || Sirkazhi Govindarajan || 06:08
|-
| 6 || Sangeetha Sowbagyame || C. S. Jayaraman || 04:24
|-
| 7 || Thennaludaiya || C. S. Jayaraman || 02:31
|-
| 8 || Thavamuni Viswamithiran || C. S. Jayaraman || 07:09
|-
| 9 || Veenai Kodiyudaiya || Tiruchy Loganathan, C. S. Jayaraman || 01:45
|}

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 