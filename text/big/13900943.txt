April Fool's Day (2008 film)
{{Infobox film
| name           = April Fools Day
| image          = Aprilfooldvd.jpg
| caption        =
| director       = The Butcher Brothers
| producer       = Tara L. Craig Frank Mancuso, Jr.
| writer         = The Butcher Brothers Mikey Wigart
| starring       = Taylor Cole Josh Henderson Scout Taylor-Compton Joe Egender Jennifer Siebel
| music          = James Stemple
| cinematography = Michael Maley
| editing        = Raúl Dávalos
| distributor    = Stage 6 Films
| released       = March 25, 2008
| runtime        = 91 minutes
| country        = United States English
| budget         = $5,000,000 (estimated)
}} 1986 film of the same name. It is directed by The Butcher Brothers, also known as Mitchell Altieri and Phil Flores, who also directed the vampire film The Hamiltons. April Fools Day is described by star Scout Taylor-Compton as "Mean Girls crossed with horror",    and was released straight to DVD release on March 25, 2008.  The film received negative reviews from critics and fans.

==Plot==
On April 1, 2007, Desiree Cartier is hosting a party at her mansion for her actress friend Torrance Caldwell. Present for a pre-festivities toast are—Blaine Cartier, Desirees brother, who controls their joint inheritance (to Desirees dislike), U.S. Senate candidate Peter Welling, Peters "Miss Carolina" fiancee Barbie Reynolds, and the quintets less wealthy videographer friend Ryan. With the party in full swing, Desirees social nemesis and Ryans not-so-secret crush Milan Hastings, arrives. Also present is chihuahua-toting society reporter Charles.

As another in her long string of April Fools pranks, Desiree suggests Blaine get Milan tipsy on champagne and seduce her in his bedroom upstairs. Desiree and several of the others hover by the cracked-open bedroom door, prepared to videotape the affair with the camera Desiree lifted from Ryan. However, Milan has a seizure and falls off the balcony to her death. The group goes to court, and Blaine loses control of the familys fortune, which shifts to Desiree, but they are considered innocent and Milans death is considered a fatal prank.

One year later, Desiree, Blaine, Peter, Barbie, Torrance, and Ryan receive anonymous invitations to meet on Milans grave at noon, April 1, 2008, with the cryptic P.S. "I have proof." Just after the entire sextet is back together, a messenger comes to the grave with a box containing a letter and a laptop computer. The letter says one of the six murdered Milan, and if that person does not confess, all of them will be dead by midnight. As a show of intent, the computer has footage of Charles drowning in his pool.

Everyone goes from the cemetery to the pool to confirm this mishap. Subsequently, an increasingly frantic Desiree sees one "suspect" after another "murdered" before her eyes (though the bodies keep disappearing). Barbie is electrocuted in a beauty pageant dressing room, Peters campaign truck runs him down in a parking garage, Ryans throat is slit in his humble apartment, and returning home, Desiree and Blaine discover even their long-time butler Wilford has been butchered in the kitchen.

After a brief separation from her brother, Desiree finds him tied to a chair. Worse yet, gun-wielding Torrance soon has Desiree tied inches away in another chair. After some back and forth, and Torrance fatally shooting Blaine in the chest, a chagrined Desiree finally admits it was she who spiked Milans fatal drink, while allowing brother Blaine to become the "fall guy." At this point Blaine cannot contain a chuckle, and soon the whole crew of 2008 "victims" are surrounding a still-tied Desiree, telling her what a bitch she is, and how they have conspired to prove it to her. Special effects people from Torrances Boogie Nights 2 set have equipped Blaine and the others with "squibs," and other cast members faked Desiree out by dressing as cops and "confirming" Ryans murder as she and her brother were fleeing the scene.

To illustrate her end of the charade involving a revolver shooting blanks, Torrance pulls the trigger once more. Unfortunately for Desiree, this time the cartridge in the chamber is real, and the bullet blows off the top of her head. The next scene involves the same inquest-probate judge from a year earlier absolving Torrance of any guilt for Desirees death, and confirming Blaine as sole heir of the family estate. The final scene shows Blaine driving off in what was Desirees red Mercedes, a slowly building smirk on his face.

==Cast==
*Taylor Cole as Desiree Cartier
*Josh Henderson as Blaine Cartier
*Scout Taylor-Compton as Torrance Caldwell
*Joe Egender as Ryan
*Jennifer Siebel as Barbie Reynolds
*Samuel Child as Peter Welling
*Joseph McKelheer as Charles Lansford
*Frank Aard as Wilford
*Sabrina Ann Aldridge as Milan Hastings

==Production==
April Fools Day was directed by The Butcher Brothers, and the two also re-wrote the script for the remake with Mikey Wigart. Picked up by Stage 6 Films for distribution, it was filmed in North Carolina under producers Tara L. Craig and Frank Mancuso Jr..  Mancuso had also produced the original. Scout Taylor-Compton noted that the filmmakers plan to make April Fools Day an R-rated film. 

The film would retain most of the originals humor; however, Flores said it is "pretty much contemporized, with off-beat humor, different settings -- something that would gel with todays audience." Altieri noted that "Its kind of more a twisting and turning story" with "some really good scares". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 