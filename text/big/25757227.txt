Edge of Eternity (film)
{{Infobox film
| name           = Edge of Eternity
| image          =
| image_size     =
| caption        =
| director       = Don Siegel
| writer         =
| narrator       =
| starring       = Cornel Wilde
| music          = Daniele Amfitheatrof
| cinematography = Burnett Guffey
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 80 min.
| country        = United States Kingman, AZ, Oatman,AZ, Gold Road, Az
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Edge of Eternity is a 1959 CinemaScope film directed by Don Siegel shot on location in the Grand Canyon.

==Plot== Victoria Shaw) speeding recklessly down the road.  The unidentified man is later found dead, hanging bound and gagged in a former mining office in an abandoned gold mine.

The Deputy and Janice Kendon, the speeding woman, team up to solve the murders and a plot to illegally mine gold to sell in Mexico.

==Cast==
*Cornel Wilde as Deputy Les Martin Victoria Shaw as Janice Kendon
*Mickey Shaughnessy as Scotty OBrien
*Edgar Buchanan as Sheriff Edwards
*Rian Garrick as Bob Kendon
*Jack Elam as Bill Ward
*Alexander Lockwood as Jim Kendon
*Dabbs Greer as Gas station attendant
*Tom Fadden as Eli
*Wendell Holmes as Sam Houghton

==Location==
 
The climax of the film, involving a fight on a cable car suspended above the Grand Canyon, was filmed in the aerial tramway to the Bat Cave mine, in the western Grand Canyon of Arizona.

==External links==
* 
* 
 

 
 
 
 
 
 
 
 


 