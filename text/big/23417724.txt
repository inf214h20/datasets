Excuse Me
 
{{Infobox film
| name           = Excuse Me
| image          =
| caption        = Lobby Cardslot
| director       = Alfred J. Goulding
| producer       = Louis B. Mayer
| screenplay     = Rupert Hughes
| based on       =  
| starring       = Conrad Nagel  Norma Shearer Renée Adorée
| music          =
| cinematography = John W. Boyle
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States Silent English intertitles
| budget         =
}} silent comedy film starring Norma Shearer, and Conrad Nagel. It was directed by Alfred J. Goulding, and based on the play of the same name written by best-selling novelist Rupert Hughes. 

Excuse Me is a remake of a 1915 film with the same name. Its status is currently unknown. 

==Plot==
Comedy about a naval officer, Harry Mallory (Conrad Nagel) and his would-be bride Marjorie Newton (Norma Shearer) who spend most of their time running up and down a train looking for a clergyman to marry them.

==Cast==
* Norma Shearer - Marjorie Newton
* Conrad Nagel - Harry Mallory
* Renée Adorée - Francine
* Walter Hiers - Porter John Boles - Lt. Shaw
* Bert Roach - Jimmy Wellington
* William V. Mong - Rev. Dr. Temple
* Edith Yorke - Mrs. Temple
* Gene Cameron - Lt. Hudson
* Fred Kelsey - George Ketchem
* Paul Weigel - Rev. Job Wales
* Mai Wells - Mrs. Job Wales

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 