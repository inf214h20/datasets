Hum Hain Rahi Pyar Ke
 
 

{{Infobox film
| name           = Hum Hai Rahi Pyar Ke
| image          = HumHainRahiPyaarKe.jpg
| caption        = DVD cover
| director       = Mahesh Bhatt
| producer       = Tahir Hussain
| story          = Robin Bhatt
| screenplay     = Aamir Khan Robin Bhatt
| starring       = Juhi Chawla Aamir Khan Sharokh Bharucha Kunal Khemu Baby Ashrafa
| music          = Nadeem-Shravan
| cinematography = Praveen Bhatt
| editing        = Sanjay Sankla
| distributor    = T V Films
| released       = 23 July 1993
| runtime        = 155 min.
| country        = India
| awards         = 
| language       = Hindi
| budget         = 
| gross          = 
}} National Film Award - Special Jury Award. Hum Hain Rahi Pyar Ke also won the Filmfare Best Film Award. It was generally alleged at the time that the movie was ghost-directed by Aamir Khan because Mahesh Bhatt did not show up on the sets several times. It was a success at the box office.

== Synopsis ==
Rahul Malhotra (Aamir Khan) is the caretaker of a garment company that has a pending order of two lakh shirts to Mr Bijlani. Rahul is also the guardian of his deceased sisters mischievous kids: Sunny, Munni, and Vicky. He finds it hard to control the kids, as he is new to this. When the kids cause trouble, Rahul punishes them by locking them in their room. However, the children escape and head for a carnival in town.

Vyjayanti (Juhi Chawla) is the bubbly daughter of a South Indian businessman and music-lover. Her father wants her to marry Natarajan, an Iyer-clan music legend, who is somewhat creepy. Vyjayanti refuses to marry him; as punishment, she is also locked up and escapes. She meets the three kids at the carnival and they become friends. Vyjayanti explains that she has no home so the children invite her to stay with them.

The children go to great lengths to hide Vyjayanti from Rahul. In a row of hilarious sequences, they are always one step ahead of Rahul before he can discover Vyjayanti. Two nights later however, Vyjayanti is revealed. Initially angry, Rahul sees that the children love her so he gives her a job as the childrens governess. Vyjayanti begins to live with Rahul and the kids and slowly falls in love with Rahul.

Seductive, glitzy Maya, Bijlanis daughter, is obsessed with Rahul. She wants to marry Rahul, and Rahul approves, deciding that it would benefit the children. When Vyjayanti and the children find out about Maya and Rahuls upcoming engagement ceremony, Vyjayanti is heartbroken and the kids are upset, as they dislike Maya. On the day of the engagement, Vyjayanti explains to the kids that she loves Rahul and wants to marry him. The kids come up with a plan to stop the engagement. They crash the party with a dramatic act, which successfully postpones the engagement but angers Rahul. Back home, he scolds Vyjayanti and she admits that she loves him, shocking him.

The next morning, Bijlani comes with Maya to offer Rahul a second chance. Rahul defends Vyjayanti against their insults, thereby expressing his own love for her. The mischievous kids chase Bijlani and Maya out of the house with rotten eggs and tomatoes. As revenge, Bijlani and Maya set on auctioning Rahuls house. Rahul asks his workers to work overtime to make up for the shirt orders, which the supportive workers agree to. A successful two lakh shirts are made and loaded on to a truck to be delivered to Bijlani. Bijlani hires some thugs to ensure that the truck doesnt arrive on time. Much to their distaste, Rahul arrives on time with the order, and Bijlani and Maya are arrested.

Vyjayanti is reunited with her father, who disapproves of her marrying anyone outside the Iyer clan. All the factory workers, Rahuls colleagues, and the children ask him to allow Rahul and Vyjayanti to marry. With so much persistence, he accepts and Vyjayanti and Rahul are married in a South Indian ceremony.

== Cast ==
*Aamir Khan as Rahul Malhotra
*Juhi Chawla as Vyjayanti Iyer
*Dalip Tahil as Bijlani
*Navneet Nishan as Maya
*K.D.Chandran as Mr. Iyer
*Veeru Krishnan as Natraj Iyer
*Tiku Talsania as Adv. Homi Wadia
*Mushtaq Khan as Bhagwati Prasad Mishra "Mishraji" Javed Khan as Thief Chhotya
*Sharokh Bharucha as Vicky
*Kunal Khemu as Sunny
*Baby Ashrafa as Munni

== Songs ==
The Soundtrack Of The Movie Is Composed By The Music Duo Nadeem Shravan.The Song Lyrics Planned By Sameer.
{| class="wikitable "
|-
! No !! Title !! Singer(s) !! Length
|-
| 1 || "Ghunghat Ki Aaad Se Dilbar Ka" || Kumar Sanu, Alka Yagnik || 06:17
|-
| 2 || "Mujhse Mohabbat Ka Izhar"  || Kumar Sanu, Alka Yagnik || 05:07
|-
| 3 || "Yunhi Kat Jaayega Safar Saath"|| Kumar Sanu, Alka Yagnik || 07:40
|-
| 4 || "Woh Meri Need Mera Chain Mujhe" || Sadhna Sargam || 05:03
|-
| 5 || "Bambai Se Gayi Poona" || Alka Yagnik  || 04:23
|-
| 6 || "Chikni Soorat Tu Kahan Tha" ||  Kumar Sanu || 04:24	
|-
|}

== Awards ==

*Filmfare Best Film Award
*Filmfare Best Actress Award - Juhi Chawla Sameer
*National National Film Special Jury Award - Mahesh Bhatt National Film Award - Best Female Playback Singer  - Alka Yagnik for "Goonghat Ki Aad Se"

== References ==
 
 

== External links ==
*  

 
 

 
 
 
 
 