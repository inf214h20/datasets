Life as a House
{{Infobox film
| name           = Life as a House
| image          = Life-as-a-house.jpg
| caption        = Original poster
| director       = Irwin Winkler
| producer       = Rob Cowan Irwin Winkler
| writer         = Mark Andrus
| starring       = Kevin Kline Kristin Scott Thomas Hayden Christensen Jena Malone Mary Steenburgen
| music          = Mark Isham
| cinematography = Vilmos Zsigmond
| editing        = Julie Monroe
| distributor    = New Line Cinema
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $27 million   
| gross          = $23,903,791 (worldwide) 
}}
 2001 Cinema American drama film produced and directed by Irwin Winkler. The screenplay by Mark Andrus focuses on a man who is anxious to repair his relationship with his ex-wife and teenaged son after he is diagnosed with terminal cancer. 

==Plot==
George Monroe (Kevin Kline), a fabricator of architectural models, is fired from the job he has held for twenty years when he refuses to fall into step with his co-workers and use the computer technology available to them. In a fit of rage at his bosss refusal to let George keep a few of the models that he had built, he destroys all of the models with a spindle from an architectural drawing, keeping only one for himself. As he exits the building, he collapses on the pavement and is rushed to the hospital, where it is revealed he has cancer of such an advanced stage that the doctor feels any treatment would be futile.
 alienated from his stepfather Peter (Jamey Sheridan), and his mother Robin (Kristin Scott Thomas) finds herself unable to cope. Against his will, Sam must spend the summer with George, who has opted not to reveal his terminal condition, and help him with what will be the final project of his life. Sam is not happy to be forced to stay with George and initially makes it a point not to help him with the houses construction. Sam is also unhappy with the rough facilities, which include a toilet in the common area and an outdoor shower. When George refuses to give Sam any money unless he works for it, Sam toys with becoming a male prostitute, however he is nearly caught and forced to flee from his first encounter. This leads him to steal Georges Vicodin. 

As time passes, George slowly reconnects with Sam. Robin decides to assist as well, and she begins to find herself rediscovering the man she once loved. Also joining in the construction are Alyssa (Jena Malone), Sams classmate who lives in the house next door with her mother Colleen (Mary Steenbergen); local policeman Kurt Walker (Scott Bakula), Georges childhood friend; Sams young half-brothers Adam (Mike Weinberg) and Ryan (Scotty Leavenworth); various neighbors; and eventually Peter, even after separating from Robin when she tells him that her feelings for George have re-awakened. Eventually, George tells Robin of his disease, sending her into shock. That same night, he tells Sam, who reacts with feelings of betrayal and accusing George of being selfish. George responds by saying "No, Sam, I wasnt trying to get you to like me. I was trying to get you to love me," making Sam even angrier at the betrayal and leading him to take refuge at Alyssas house. During that night, George collapses in his garage/shack and is found by Robin the following morning. Complications arise when cantankerous neighbor David Dokos (Sam Robards) tries to halt construction because the buildings height exceeds the allowable limit by six inches. His plans to halt the project are stopped by Sam, who recognises him from his prostitution attempt and subtly blackmails him with that information. 

As a final act of love towards his father, Sam puts Christmas lights all over the unfinished house and shows George the gleaming house from his hospital window. The next morning, Sam returns to finish the house and Robin sits beside George until his death. After a while, Robin goes to the house and tells Sam about his fathers death. Sam, much changed by his re-engagement with his father, inherits the house he finished building, but in the closing scene he decides to give the property to the girl who had been injured in a car crash that Georges father had been the cause of, who was living in a trailer park, rather than occupying it himself or selling it for profit. Over a voice-over, George says his final words to Sam: "I always thought of myself as a house. I was always what I lived in. It didnt need to be big; it didnt even need to be beautiful; it just needed to be mine. I became what I was meant to be. I built myself a life... I built myself a house. Twenty-nine years ago, my father crossed a double line. Changed my life and the life of a little girl forever with that mistake. I cant stop thinking about her. With every crash of every wave, I hear something now. I never listened before. Im on the edge of a cliff, listening. Almost finished. If you were a house, Sam, this is where you would want to be built: on rock, facing the sea. Listening. Listening."

==Cast==
* Kevin Kline as George Monroe
* Kristin Scott Thomas as Robin Kimball
* Hayden Christensen as Sam Monroe
* Jena Malone as Alyssa Beck
* Mary Steenburgen as Colleen Beck
* Jamey Sheridan as Peter Kimball
* Ian Somerhalder as Josh
* Scott Bakula as Kurt Walker
* Sam Robards as David Dokos 
* John Pankow as Bryan Burke 
* Mike Weinberg as Adam Kimball
* Scotty Leavenworth as Ryan Kimball

==Production==
In Character Building: Inside Life as a House, a bonus feature on the DVD release of the film, director Irwin Winkler confesses he never realized the rekindling love between George and Robin was a key aspect of the script until he saw the emotion displayed by Kevin Kline and Kristin Scott Thomas in their scenes together. Winkler encouraged his cast to improvise moments leading into and following their scripted dialogue, many of which were included in the final film.

In From the Ground Up, another DVD bonus feature, production designer Dennis Washington discusses how he was required to construct an entire street of houses leading to Sams house, which was perched on a cliff overlooking the Pacific Ocean in Palos Verdes, California. The new house was built on another site, then dismantled and transported to the film set as each section was needed. Because the film tracked the progress of the dismantling of the old house and the construction of the new one, it had to be shot in sequence. When the film was completed, the house was dismantled, moved, reconstructed, and enlarged to become a library for the Kenter Canyon Elementary School in Brentwood, California|Brentwood. 

The soundtrack includes "What You Wish For" and "Rainy Day" by Guster, "Thats the Way" by Gob (band)|Gob, "Live a Lie" and "Somewhere" by Default (band)|Default, "Sweet Dreams" by Marilyn Manson, "Water" by ohGr, "Rearranged" by Limp Bizkit, "Both Sides Now" by Joni Mitchell, "Gramercy Park" by Deadsy, and "How to Disappear Completely" by Radiohead.

The film premiered at the Toronto International Film Festival and was shown at the Boston Film Festival before going into limited release in the US on October 26, 2001.

==Reception==
Life as a House received mixed reviews from critics, as the film holds a 47% rating on Rotten Tomatoes based on 105 reviews.

===Box office===
The film opened in twenty-nine theaters in the US and grossed $294,056 on its opening weekend. It eventually earned $15,667,270 in the US and $8,236,521 in foreign markets for a total worldwide box office of $23,903,791.    

==Awards and nominations== National Board of Review Award for Breakthrough Performance by an Actor. Kevin Kline was nominated for the Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Leading Role. The film won the Audience Award for Favorite Feature at Aspen Filmfest.

==DVD release==
 , originally cast as Kurt Walker but replaced when he was injured in a motorcycle accident after filming began) with optional commentary; a theatrical press kit; and the original trailer.

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 