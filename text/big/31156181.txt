Achante Bharya
{{Infobox film 
| name           = Achante Bharya
| image          =  Achantebharyafilm.jpg
| image_size     =
| caption        =
| director       = Thikkurissi Sukumaran Nair
| producer       = Thikkurissi Sukumaran Nair
| writer         = KS Gopalakrishnan Thikkurissi Sukumaran Nair (dialogues)
| screenplay     = Thikkurissi Sukumaran Nair Ragini Thikkurissi Sukumaran Nair
| music          = V. Dakshinamoorthy
| cinematography = TN Krishnankutty Nair
| editing        = R Devarajan
| studio         = Asok Productions
| distributor    = Asok Productions
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, Ragini and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by V. Dakshinamoorthy.    It was released in 1971 and based on the Tamil film Chitthi.

==Cast==
 
*Adoor Bhasi as Contractor Karunakaran Nair
*K. P. Ummer as Vijayan Ragini as Thangamma
*Thikkurissi Sukumaran Nair
*Jose Prakash as Rajan
*Sindhu
*Baby Bindu
*Baby Indira
*Baby Shanthi
*Baby Uma
*Baby Vijaya
*Bahadoor as Balan Nair
*Kayyalam
*Master Raju Meena as Adoor Bhasis Mother
*Menon
*Nellikode Bhaskaran as Thikkurissis Son
*Pala Thankam as Raginis Mother
*Vijayasree as Omana
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Thikkurissi Sukumaran Nair and Irayimman Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaariraaro thamarappoomizhi Pootti (Pathos) || S Janaki || Thikkurissi Sukumaran Nair || 
|-
| 2 || Madhuram madhumadhuram || K. J. Yesudas || Thikkurissi Sukumaran Nair || 
|-
| 3 || Omanathinkal Kidavo (Bit) ||  || Irayimman Thampi || 
|-
| 4 || Vaahinee Premavaahinee || K. J. Yesudas, S Janaki || Thikkurissi Sukumaran Nair || 
|-
| 5 || Varumo Nee Varumo || K. J. Yesudas, S Janaki || Thikkurissi Sukumaran Nair || 
|}

==References==
 

==External links==
*  

 
 
 

 