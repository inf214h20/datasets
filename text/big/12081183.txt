Getting Straight
{{Infobox film
| name           = Getting Straight
| image          = Getting straight.jpg
| caption        = Theatrical release poster Richard Rush
| producer       = Richard Rush
| writer         = Robert Kaufman Ken Kolb
| starring       = Elliott Gould Candice Bergen Jeff Corey Harrison Ford
| music          = Ronald Stein
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Maury Winetrobe
| distributor    = Columbia Pictures
| released       =  
| runtime        = 124 mins
| country        = United States English
| budget         =
| gross          = $5.1 million (US/ Canada rentals) 
}} Richard Rush, released by Columbia Pictures.

The story centered upon student politics at a university in the early 1970s, seen through the eyes of non-conformist graduate student Harry Bailey (Elliott Gould).  Also featured in the cast were Candice Bergen as Baileys girlfriend, Jeff Corey as Baileys professor and Harrison Ford as his anti social friend.
 The Strawberry Statement (1970).

==Synopsis==
Harry Bailey, a former student activist and post-graduate, comes back to university to complete an education course to become a teacher. He tries to avoid the increasing student unrest that has surfaced, but finds this difficult as his girlfriend, Jan, is a leader in these protests.

Over time, student demonstrations bring police to the campus to quell the unrest, and the ensuing clashes lead to martial law. Harry is forced to question his values in relation to this. At the height of the rioting he concurs with Jan that "getting straight" is more important than unprotesting acceptance of the educational establishment.

==Critical reception==
Leonard Maltin noted that the film essentially was a "period piece" but that its "central issue of graduate student (Elliott) Gould choosing between academic double-talk and his beliefs remains relevant." On the other hand, Steven Scheuer wrote that the film was reflective of "hippiedom alienation at its shallowest."

Other reviewers, such as Roger Greenspun from The New York Times, were a little more complimentary in tone about the film. While he says that overall the film is "misguided" he lauded Gould for "a brilliant, mercurial performance" and that he "fires" the film "with a fervor and wonderful comic sense of reality."

==Notes==
 

==References==
 
*Greenspun, Roger (1970)   New York Times, May 14, 1970. (accessed 9 July 2007)
*Maltin, Leonard (1991) Leonard Maltins Movie and Video Guide 1992, Signet, New York.
*Scheuer, Steven H. (1990) Movies on TV and Videocassette, Bamtam Books, New York.
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 