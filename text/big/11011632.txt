The Nostril Picker
{{multiple issues|
 
 
}}

	

{{Infobox film
| name = The Nostril Picker
| image = The Nostril Picker.jpg
| caption = 
| director = Mark Nowicki
| producer = Steven Hodge Patrick J. Mathews Mark Nowicki
| writer = Steven Hodge
| narrator = 
| starring = Carl Zschering Edward Tanner Laura Cummings Bob Bingham
| music = Clinton Clark
| cinematography = Patrick J. Mathews Michael Mayne Gary C. Schifflet
| distributor =  1993
| runtime = 76 mins
| country =   English
| budget = 
| preceded_by = 
| followed_by = 
}} 1993 horror slasher genre. The film stars Carl Zschering as Joe Bukowski, a homicidal maniac who uses his special ability to get close to his female targets. Despite the name, the film plot does not directly revolve around a man who picks his nose. Instead, Joe Bukowski learns a chant from a homeless man in return for a small proportion of his alcoholic beverage, and this chant transforms Joe into a form of anybody he pleases. Joe is warned that when the chant is performed excessively it will make him crazy, but chooses to ignore this warning. His main victims in the film are teenage girls attending a high school close to his home. He takes the form of a girl and makes a friendship with these girls but decides to murder, rape and eat them one by one.

== External links ==
*  

 
 
 
 
 


 