Bay of Angels
 
{{Infobox film
| name           = La baie des anges
| image          = BaiedesAngesPoster.jpg
| writer         = Jacques Demy
| starring       = Jeanne Moreau Claude Mann
| director       = Jacques Demy 
| music          = Michel Legrand
| cinematography = Jean Rabier
| editing        = Anne-Marie Cotret
| released       = 1 March 1963
| runtime        = 89 min (France) French
| country        = France
| budget         =
}}
La baie des anges (Bay of Angels in English) is a 1963 French film directed by Jacques Demy.  Starring Jeanne Moreau and Claude Mann, it is Demys second film and deals with the subject of gambling.  

== Plot ==
Jean Fournier (Claude Mann) is a young bank employee who is encouraged by his friend Caron to take an interest in gambling. After winning money in a game of roulette, he decides to vacation in Nice, where he falls in love with Jackie (Jeanne Moreau), divorced and mother to a child she rarely visits. Though Jackie also enjoys Jeans company, she constantly warns him that her passion for gambling will always be greater. 

Jean becomes jealous of not having all of her attention and has mixed feelings about gambling, yet he too is to some extent seduced by this new life style that involves taking risks. Despite Jackies cool façade and alleged control over her choices – she claims she is unattached to the money itself, but rather the thrill of the game, and doesnt mind going from rich to poor in a matter of seconds -, she soon begins to reveal her vulnerability and the emptiness she often feels as result of her addiction.

== Cast ==
* Jeanne Moreau as Jacqueline "Jackie" Demaistre
* Claude Mann as Jean Fournier
* Paul Guers as Caron
* Henri Nassiet as Monsieur Fournier
* Conchita Parodi as the hotel manager
* André Certes as the bank director
* Nicole Chollet as Marthe

==References==
{{reflist|refs=
   

}}

==External links==
* 
* 

 

 
 
 
 
 

 