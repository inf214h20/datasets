Girls About Town (film)
 
{{Infobox film
| name           = Girls About Town
| image          = Girls About Town (film).jpg
| caption        = Film poster
| director       = George Cukor
| producer       = Raymond Griffith
| writer         = Zoë Akins Raymond Griffith Brian Marlow
| starring       = Kay Francis Joel McCrea Lilyan Tashman
| music          = 
| cinematography = Ernest Haller
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States 
| language       = English
| budget         = 
}}
Girls About Town is a 1931 American pre-Code comedy film directed by George Cukor and starring Kay Francis    and Joel McCrea.

==Cast==
*Kay Francis as Wanda Howard
*Joel McCrea as Jim Baker
*Lilyan Tashman as Marie Bailey
*Eugene Pallette as Benjamin Thomas
*Alan Dinehart as Jerry Chase
*Lucile Gleason as Mrs Benjamin Thomas
*Anderson Lawler as Alex Howard
*Lucile Browne as Edna George Barbier as Webster
*Robert McWade as Simms
*Louise Beavers as Hattie
*Judith Wood as Winnie
*Adrienne Ames as Anne
*Frances Bavier as Joy
*Patricia Caron as Billie
*Claire Dodd as Dot

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 