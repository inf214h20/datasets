House of Secrets (1956 film)
{{Infobox film
| name           = House of Secrets
| image          = "House_of_Secrets"_(1956).jpg
| caption        = UK theatrical poster Guy Green
| producer       = Vivian Cox Julian Wintle
| writer         = Robert Buckner Bryan Forbes
| based on       = novel Storm Over Paris by Sterling Noel Michael Craig  Anton Diffring  Gérard Oury 
| cinematography = Harry Waxman
| music          = Hubert Clifford
| editing        = Sidney Hayers
| studio         = Rank Films
| distributor    = Rank Film Distributors
| released       = 23 October 1956 (UK)
| runtime        = 85 minutes
| country        = United Kingdom English
| gross          = 
}} Guy Green, Michael Craig, Anton Diffring, and Gérard Oury.  

==Cast== Michael Craig - Larry Ellis / Steve Chancellor
* Anton Diffring - Anton Lauderbach
* Gérard Oury - Julius Pindar
* Brenda De Banzie - Madame Isabella Ballu
* Geoffrey Keen - Colonel Burleigh, CIA
* David Kossoff - Henryk van de Heide, CIA
* Barbara Bates - Judy Anderson
* Alan Tilvern - Brandelli
* Julia Arnall - Diane Gilbert
* Gordon Tanner - Curtice
* Eugene Deckers - Vidal
* Eric Pohlmann - Gratz
* Carl Jaffe - Walter Dorffman

==Plot==
The film, based on the novel Storm Over Paris by Sterling Noel, follows a man who is sent undercover to infiltrate an international crime organisation.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 