A Pain in the Pullman
{{Infobox Film |
  | name           = A Pain in the Pullman |
  | image          = PainpullmanTITLE.jpg|
  | caption        = | Preston Black |
  | writer         = Preston Black |
  | starring       = Moe Howard Larry Fine Curly Howard Bud Jamison James C. Morton Eddie Laughton Loretta Andrews Phyllis Crane Wilna Hervey |
  | cinematography = Benjamin H. Kline |
  | editing        = William A. Lyon |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 19 46" |
  | country        = United States
  | language       = English
}}

A Pain in the Pullman is the 16th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== train to Southern Pacific train with them, but Joe gets loose, And they have a hard time getting up to the berth bed by making a lot of noise and managing to awaken and annoy all of the trains passengers, including Mr. Paul Pain (James C. Morton) and Mr. Johnson the stage manager and boss (Bud Jamison), the latter of which routinely bonks his head on the upper berth upon awakening. Ultimately, a terrified Joe pulls the trains emergency cord, abruptly stopping the train in the process. The passengers then forcibly remove the Stooges from the train because they were fired for making a lot of noises and bringing their pet monkey onto the train and they land on three cows and hobble away.

==Production notes==
A Pain in the Pullman is the longest Stooge short filmed, running at 19 46";  the shortest is Sappy Bull Fighters, running at 15 12".  Filming was completed between April 29 and May 4, 1936.   
 ), the self-proclaimed "heartthrob of millions".]]

This is the first short in which Moe, Larry, and Curly are actually referred to as "The Three Stooges" in the dialogue.
 steers was reused at the end of A Ducking They Did Go.    The same gag was used in the end of The Ren and Stimpy Show episode "Rubber Nipple Salesmen" (show creator John Kricfalusi was apparently a big fan of the Three Stooges, using a good number of Stooge gags as part of his tenure with Ren and Stimpy; the character of Stimpy is himself based on Larry).
 ZaSu Pitts Richard Lane remade the film in 1947 as Training for Trouble.

The name "Johnson" was shouted a total number of 10 times.

==Shellfish==
Moe Howard had fond memories of filming A Pain in the Pullman. He also recalled his intense dislike for shellfish, and how brother Curly Howard cut the inside of his mouth eating the shells from a Dungeness crab:
{{quote|text=...In one sequence, all three of us wound up in the same upper Berth (sleeping)|berth. Later, we found ourselves a drawing room, not knowing it was assigned to the star of the show (James C. Morton). There was a lovely table set in the room with all kinds of delicacies.

At one point Curly picked up the hard-shelled Dungeness crab. We, of course, were not supposed to know what it was. Larry thought it was a tarantula, Curly figured it to be a turtle, and I concluded that it must be something to eat or it wouldnt be on the table with crackers and sauce.
 tines of his fork. I took the fork from Curly, tossed a napkin on the floor, and asked him to pick it up. When Curly bent over, I hit him on the head with the crab, breaking the shell into a million pieces. Then Curly scooped out some of the meat, tasted it, and made a face. He threw the meat away and proceeded to eat the shell.
 Preston Black, the director, asked me to just lick the claw, but I couldnt. He finally had the prop man duplicate the claw out of sugar and food coloring and had me nibble on it as though I was enjoying it. I was still very wary during the scene. I was afraid they had coated the real shell with sugar and that that awful claw was underneath. I chewed that claw during the scene, but if youll notice, I did it very gingerly.

In the meantime, Curly was still chewing on the shell, which was cutting the inside of his mouth. Finally, our star comes back to his room and kicks us out, and we three climb into our upper berth to go to sleep.   }}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 