Scream (1981 film)
 
{{Infobox film
| name           = Scream
| image          = Scream 1981 vhs.jpg
| imagesize      =
| caption        = VHS cover
| director       = Byron Quisenberry
| producer       = Byron Quisenberry Clara Huff Hal Buchanan
| writer         = Byron Quisenberry Pepper Martin Hank Worden Ethan Wayne Ann Bronston Julie Marine
| music          = Joseph Conlan
| cinematography = Richard Pepin
| editing        = B.W. Kestenberg Shriek Show Code Red
| released       =  
| runtime        = 82 minutes
| country        = United States English
| box office     = $1,083,395 (USA)

}} 1981 American American Slasher slasher film Pepper Martin, Hank Worden, Ethan Wayne, Ann Bronston, and Julie Marine...

== Plot ==
A group of 12 friends on a river camping trip decide to spend the night in an old ghost town, and an unseen killer begins to dispatch them one by one. On the first night at the stroke of 12 midnight, three of the group are killed in rapid succession. Allen is found hung; his friends Rod and John both hacked by a cleaver. In the morning, the nine survivors try to leave, but find their three rubber rafts slashed apart by someone (or something) forcing them to spend another night at the ghost town. During the day, two youths on motor dirt bikes arrives and one of the guides leaves with them to get help from a nearby ranch which is over 30 miles away. 

At nightfall, Bob takes over as de facto leader of the group and has them set up traps to try to trap the killer, but the unseen killer seems to evade them every time leaving no evidence, not even footprints. Soon, the unseen killer strikes again, killing Andy by striking him in the face with an axe and decapitates Bob with a scythe, including one of the dirtbike youths by blowing him through a door and leaving Stan and Lou badly injured. At the stroke of midnight, a mysterious horse-drawn stagecoach arrives in the ghost town, being driven by a mysterious cowboy who introduces himself as Charlie Winters (Woody Strode). Charlie tells the group that he has been hunting the killer for years and also claims that the culprit is the ghost of an old sea captain whom drove people out of town years ago. The rest of the survivors are wary about trusting Charlie, but soon realizes that he may be their only hope of survival.

== Cast == Pepper Martin- Bob
*Hank Worden- John
*Ethan Wayne- Stan
*Ann Bronston- Marion
*Julie Marine- Laura
*Nancy St. Marie- Adriana
*Joseph Alvarado- Rudy
*Alvy Moore- Allen
*Bobby Diamond- Rod
*John Nowak- Jerry
*Joe Allaine- Lou
*Cynthia Faria- Janice
*Bella Bruck- Maggie
*Dee Cooper- Fred
*Bob Macgonigal- Andy
*Gino Difirelli- Len A. Lemont
*Gregg Palmer- Ross
*Woody Strode- Charlie Winters

==Release==
Scream opened theatrically in the USA on January 1, 1981, while being distributed by Cal-Com. The film grossed $1,083,395 by the end of its run.

Scream was then released on home video sometime in the mid-80s by Vestron Video

Media Blasters released a DVD of the film in 2010 under its Shriek Show label. The release included a widescreen transfer, mono sound mix, an audio commentary with director Byron Quisenberry, a TV spot, and a theatrical trailer.
 Code Red The Barn of the Naked Dead. This release did not include the audio commentary with director Byron Quisenberry, the theatrical trailer, or TV spot that was included in Media Blasters release.

== Reception ==
The film was released to negative reception, and is often regarded as one of the worst slasher films of the 1980s due to its slow pacing, insipid script, bad acting, lack of blood or gore, and a confusing killer who is never seen on-screen.

Charles Tatum of efilmcritic.com praised the ghost town set, although also stated that "(Quisenberry) cannot generate any suspense at all." He also bashed the special effects, stating that they "..serve as a subliminal Pavlovian trigger for french fries with extra ketchup
."

Richard Mogg of Retroslashers.net wrote a mixed review of the film, stating that "...it had me roaring on the floor with all the nonsense going on. Sure it fails as a slasher but I’d still give it a passing grade for trying."

Oh-The-Horror.com wrote a generally negative review, stating that: "For what little it has going for it, Scream is just entirely too slow, too dull, and too vague."

DVD Verdict.com wrote a scathing review of the Code Red double-feature release, stating that "Scream is a hunk of lead from the Golden Age of Slashers, a cheap, dull, and bloodless concoction thats a chore to sit through. Its horrendously shot and wretchedly acted..." and criticized the transfer, stating that it "...looks pretty bad, with a good amount of print damage and lousy contrast, though that might be attributed to the source."

== External links ==
* 
* 
* 
* 
* 

 
 