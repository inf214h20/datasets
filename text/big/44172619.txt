Naalkavala
{{Infobox film
| name           = Naalkavala
| image          =
| caption        =
| director       = IV Sasi
| producer       = Babu
| writer         = T. Damodaran
| screenplay     = T. Damodaran Urvashi Thikkurissi Sukumaran Nair Shyam
| cinematography =
| editing        =
| studio         = Thomsun Films
| distributor    = Thomsun Films
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Urvashi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Mammootty
*Shobhana Urvashi
*Thikkurissi Sukumaran Nair
*Ratheesh
*Captain Raju Seema
*Thiruthiyadu Vilasini

==Soundtrack== Shyam and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kinaavu Neyyum || KS Chithra || Yusufali Kechery || 
|-
| 2 || Muthukkudangale   || KS Chithra, Chorus, CO Anto, Krishnachandran || Yusufali Kechery || 
|-
| 3 || Vellinilaavoru || KS Chithra || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 