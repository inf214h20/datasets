The Rage: Carrie 2
{{Infobox film
| name = The Rage: Carrie 2
| image = RageCarrie2.jpg
| caption = Theatrical release poster
| director = Katt Shea
| producer = Patrick J. Palmer Paul Monash
| writer = Rafael Moreu
| based on =  
| starring = Emily Bergl Jason London Dylan Bruno J. Smith-Cameron Amy Irving
| music = Danny B. Harvey
| cinematography = Donald M. Morgan
| editing = Richard Nord
| studio = United Artists  Red Bank Films
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 104 minutes
| country = United States
| language = English
| budget = $21 million   
| gross = $17,762,705 
}} drama Horror horror film novel of the same name by Stephen King and features Carrie Whites half sister Rachel Lang in the title role. Directed by Katt Shea, the film stars Emily Bergl, Jason London, Dylan Bruno, J. Smith-Cameron, and Amy Irving who reprises her role of Sue Snell from the previous film.

The film was released on March 12, 1999 and received generally poor reviews from critics and fans of the original film alike. The film was also a box office bomb grossing merely $17 million against a $21 million production budget.
 2002 made-for-television version of Carrie.    

==Plot==
  telekinetic daughter Rachel Lang (Kayla Campbell) from the devil. Barbara is soon institutionalized for schizophrenia.

Years later Rachel (Emily Bergl), living with foster parents, talks with her best friend Lisa (Mena Suvari), who has lost her virginity to Eric (Zachery Ty Bryan), a football player. The football players have a game where they sleep with girls and receive points revealing Eric never cared for Lisa. After Eric rejects her, Lisa commits suicide by jumping off a building. School counselor Sue Snell (Amy Irving) talks with Rachel about Lisa.

Rachel, who has a part-time job of printing photos, discovers she developed photos of Lisa and Eric. She tells Sue and Sheriff Kelton about Lisa and Eric sleeping together, giving Kelton the photograph. Kelton looks into charging Eric with statutory rape bringing Eric into trouble. Walter, Rachels dog, is struck by a car but Rachel flags down Jesse as he drives past, and after taking Walter to an animal hospital, they go for a coffee. Learning that Rachel gave Kelton the photograph, Eric, Mark (Dylan Bruno), and several other football players go to her trailer late at night, to harass her on the telephone and attempt to break inside but quickly become a victim of her powers. The boys flee when her foster parents arrives.

Sue meets with Rachel. When Sue asks about moving objects with her mind, Rachel screams and a snow globe on Sues desk shatters. Soon afterward, Sue brings Rachel to the original high school, where the disaster took place years before and tells Rachel, that her mother told, that her father was Carries father, which Rachel does not believe. She interrupts Sues story and says her father instead was Bill Kirk, who left when she three months old. Jesse pursues Rachel, angering popular, one-night-stand cheerleader, Tracy. The night the football players attacked, Jesse and Rachel had a date. Jesse convinces Rachel he was unaware of the attack and Rachel agrees to go out with him. Jesse confronts Mark about the attack and the two fight in the locker room.

Mark plots to humiliate Rachel for what she did to Eric after Eric´s and the other´s actions are covered up by the Senior D.A.. Mark apologizes to Jesse and offers his parents cabin so Jesse can spend the night with Rachel. The two shared a romantic evening and Rachel loses her virginity, both unaware that the other guys had set up a hidden video camera to film Jesse and Rachel making love. Rachel goes to a football game to watch Jesse. After the game, one of the football players, Brad Winters and his girlfriend Monica Jones invite Rachel to a party at Marks and she leaves with Monica and her friend Deborah. Jesse is sidetracked by Tracy, who attempts to seduce him.

Rachel is with Jesses friends, when the football players reveal their sex game and claim that Rachel was added to Jesses list, making Rachel believe Jesse never cared for her. They begin to play the videotape that showed Rachel and Jesses lovemaking. As Rachels ability is triggered, she closes the doors of the house and kills most of the party-goers including Deborah. Sue takes Barbara from the mental hospital and goes to Marks house. Sue reaches the door and attempts to peer in just as Rachel flings a fireplace poker at Brads head, which also kills Sue from the other side, leaving Barbara in shock.

Rachel then makes Monicas glasses explode into her eyes. After she is blinded, Monica accidentally shoots Eric in the groin with the harpoon gun, castrating him before they both die. Rachel hears her mother calling for her, which causes her to be distracted. Mark shoots Rachel with the flare gun as she falls into the pool, causing a sensor to extend the cover. Rachel pulls Mark into the pool, and with the cover now fully extended, uses his spear gun to free herself while he drowns. Barbara is at first concerned seeing Rachel as her "little girl." But when she sees Rachels current state, she quickly believes shes possessed and runs away from her. Despite Rachels pleas for Barbara, she runs from the house, leaving her completely heartbroken. Rachel prays for help to die.

Jesse and Tracy finds the house in flames and their friends dead. When Rachel sees Tracy, she kills her by collapsing the ceiling above her. The videotape of them is still playing; when he sees it Jesse tells her he did not know they were taped. Rachel calls him a liar as a notebook hits him, which opens to the score page. Jesse says he loves her, but she does not believe him until she hears him say it on the videotape, realizing he told the truth. When the ceiling collapses over Jesses head, Rachel pushes him out of the way and is pinned. She tells him she loves him and they shared a kiss before his arm catches fire. Jesse tries kissing Rachel again, but she pushes him out of the house before allowing herself to be consumed by the flames, smiling.

One year later, Jesse is at Kings University, sharing his room with Rachels dog Walter which he kept as a memory of Rachel. One night, he has a dream that Rachel enters his room, looking as she did when they made love. In the dream, they reconcile and shares a kiss before she shatters into ash. Jesse awakes and looks at himself in the mirror with distress.

==Cast==
 
* Emily Bergl as Rachel Lang
** Kayla Campbell as Young Rachel
*   with whom Rachel falls in love.
* Dylan Bruno as Mark Bing: a football player who owns the mansion where the football game after-party takes place.
* J. Smith-Cameron as Barbara Lang: Rachels insane mother.
* Zachery Ty Bryan as Eric Stark: a jock who seduces and then humiliates Lisa, resulting in her suicide.
* John Doe as Boyd: Rachels foster father
* Gordon Clapp as Mr. Stark: Erics father
* Rachel Blanchard as Monica Jones: Tracys best friend.
* Charlotte Ayanna as Tracy Campbell: Jesse Ryans ex-girlfriend, a popular cheerleader.
* Justin Urich as Brad Winters: football player and Monicas boyfriend.
*  .
* Elijah Craig as Chuck Potter: football player.
* Eddie Kaye Thomas as Arnold: Rachels friend.
* Clint Jordan as Sheriff Kelton
* Kate Skinner as Emilyn: Rachels foster mother 
* Steven Ford as Coach Walsh
*  .
* Deborah Meschan as Deborah: One of Monicas friends who takes part in setting Rachel up
* Katt Shea as Deputy D.A.
* Rhoda Griffis as Mrs. Porter: A Saleswoman original film. Sissy Spacek turned down an offer to cameo in the film but gave permission to have her likeness used in the film.
 

==Production==

===Development===
Originally titled The Curse, the film was scheduled to start production in 1996 with   quit over creative differences and Katt Shea hurriedly took over the reins with less than a week to prepare to start filming, and two weeks worth of footage to reshoot. "The Rage: Carrie 2" audio commentary. United Artists, 2002. 

===Casting===
Amy Irving reprised the role of Sue Snell, which she originated in the first Carrie, though she was initially wary of taking the role and asked Brian De Palma, director of the original film, for his blessing.  Director Shea was told that she would not be able to use footage of Sissy Spacek from the original Carrie, but she edited several scenes into the film and presented the film to Spacek, who granted permission for her likeness to be used. 

==Release==
The film was theatrically released on March 12, 1999.

===Home Media===
The film was released on VHS and DVD on October 12, 1999.  A Blu-ray version of the film was released on 14 April 2015 in a double feature with the 2002 TV version of Carrie (2002 film)|Carrie. 

==Reception==

===Box Office===
The film was released March 12, 1999 in the United States, opening in second place that weekend.  It grossed a total of $17,762,705 domestically against a $21 million budget making the film a box office bomb. 

===Critical Reception===
Rotten Tomatoes reported the film had a 21% approval rating based on thirty-two reviews and a 33% with six reviews based on top critics. On Metacritic it had a rating of 42 on a scale from 0-100 based on 21 reviews indicating mixed or average reviews. Roger Ebert gave the film 2 out of 4 stars stating "The original Carrie worked because it was a skillful teenage drama grafted onto a horror ending. Also, of course, because De Palma and his star, Sissy Spacek, made the story convincing. The Rage: Carrie 2 is more like a shadow". 

==Soundtrack==
 {{cite video | title = http://www.amazon.com/Carrie-2-Rage-Original-Soundtrack/dp/B00000ID3E/ref=sr_1_3?ie=UTF8&s=music&qid=1212831888&sr=8-3  
}}   Ra
# Ivy
# "Resurrection" – Fear Factory Paradise Lost
# "Low Down" – 10 Watt Mary
# "Looking Down the Barrel" – Five Times Down 	
# "Die with Me" – Type O Negative
# "Keep Sleeping" – 16Volt	
# "Dark Love" – Kate Shrock 	 Sack
# "The Slower I Go" – L.A.X.	
# "Sleep" – Trailer Park Pam
# "Spark Somebody Up" – Budda Mo b

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 