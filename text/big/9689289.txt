El caballo del pueblo
{{Infobox film
| name           = El caballo del pueblo
| image          = Caballodelpuebloposter.jpg
| image_size     =
| caption        =
| director       = Manuel Romero
| producer       =
| writer         = Manuel Romero Luis Bayón Herrera
| narrator       =
| starring       = Pedro Quartucci
| music          = Alberto Soifer
| cinematography = Francisco Múgica
| editing        = Francisco Múgica
| distributor    = Lumiton
| released       = August 15, 1935
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}} 1935  Argentine musical tango film and premiered on August 15, 1935.

The cinematography and editing  was performed by Francisco Múgica.

==Main cast==
*Olinda Bozán as Ruperta
*Irma Córdoba as Esther Peña
*Enrique Serrano as Bebe Viñas
*Pedro Quartucci as Flaco
*Juan Carlos Thorry as Roberto Campos
*Juan Mangiante as Peña
*Juan Porta as Trainer
*Eduardo Lema as Lemos, the jockey
*Vicente Forastieri as Contreras
*N. Fornaresio as Peon
*Lalo Malcolm as a Thug (billed as L. Malcom)
*Nicolás Werenchuk as José Guzmán (billed as N. Werenchuk)
*Mary Parets as Trainers daughter
*Ángel Magaña as Young Man #1 in party (uncredited)
*Pedro Maratea as Young Man #2 in party (uncredited)
*Margarita Padín as Young Woman in show (uncredited)

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 