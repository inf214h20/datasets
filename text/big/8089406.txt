Family Resemblances
 
{{Infobox film
| name           = Family Resemblances
| image          = 
| caption        = 
| director       = Cédric Klapisch
| producer       = Charles Gassot 
| writer         = Cédric Klapisch Agnès Jaoui  Jean-Pierre Bacri
| starring       = Jean-Pierre Darroussin  Catherine Frot
| music          = Philippe Eidel
| cinematography = Benoît Delhomme
| editing        = Francine Sandberg 	
| distributor    = Bac Films
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
}}
 French comedy film. It was directed by Cédric Klapisch, and written by Klapisch, Agnès Jaoui and Jean-Pierre Bacri. The film stars Bacri, Jaoui, Jean-Pierre Darroussin, Catherine Frot, Vladimir Yordanoff and Claire Maurier.
 Best Supporting Best Supporting Actress.

== Plot ==
An average French family ostensibly celebrates a birthday in a restaurant. In one evening and during one meal, family history, tensions, collective and separate grudges, delights, and memories both clash and coalesce. Indeed, poking each others sore spots turns out to be the main order of business. Henri (Bacri) runs a saloon that he inherited from his father called "The Sleepy Dad," and in the near-empty bar, he plays host to several members of the family as they mark the 35th birthday of his sister-in-law, Yolande (Frot). Henris sister, Betty (Jaoui), is 30, single, and not very happy about it; his brother (and Yolandes husband), Philippe (Yordanoff), is an executive in a growing software company; Mother (Maurier) is the siblings strong-willed matriarch; and Henris paralyzed dog is on hand, whom someone describes as "like a rug, but alive." Its not been a good day for most of them: Philippe is anxious that his boss might not have liked the tie he wore on television; Betty is depressed about the sad state of her current relationship; Henri has just learned that his wife is leaving him; and Mother is tossing caustic barbs at everyone left and right. Henris bartender Denis (Darroussin) is the one neutral party on hand, and he provides the voice of reason in the midst of the bickering.

== Awards and nominations ==
*César Awards (France) Best Actor &ndash; Supporting Role (Jean-Pierre Darroussin) Best Actress &ndash; Supporting Role (Catherine Frot) Best Writing (Jean-Pierre Bacri, Agnès Jaoui and Cédric Klapisch)
**Nominated: Best Actress &ndash; Supporting Role (Agnès Jaoui) Best Director (Cédric Klapisch) Best Film

*Gardanne Film Festival (France)
**Won: Audience Award	(Cédric Klapisch)

*Lumières Award (France) Best Director	(Cédric Klapisch) Best Screenplay (Cédric Klapisch, Jean-Pierre Bacri and Agnès Jaoui)

*Montréal World Film Festival|Montréal Film Festival (Canada)
**Won: Public Prize	(Cédric Klapisch) Sleeping Man (Nemuru otoko))

== External links ==
*  

 

 
 
 
 
 
 
 

 