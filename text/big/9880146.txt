Devil in the Flesh (1947 film)
{{Infobox film 
| name = Devil in the Flesh 
| image = eiganotomo-lediableaucorps-nov1952-crop.jpg
| image size = 245px
| caption = Micheline Presle and Gérard Philipe
| director = Claude Autant-Lara  
| producer = Paul Graetz 
| writer = Jean Aurenche and Pierre Bost   Raymond Radiguet  (novel "Le diable au corps")
| narrator = 
| starring = Micheline Presle and Gérard Philipe 
| music = René Cloërec 
| cinematography = Michel Kelber   
| editing = Madeleine Gug 
| studio = Transcontinental Films 
| distributor = 
| released = 1947 (France) 1949 (USA)  1952 (Japan)
| runtime = 125 minutes
| country = France 
| language =French 
| budget = 
| gross = 
}} 1947 French movie directed by Claude Autant-Lara starring Micheline Presle and Gérard Philipe. It is based on a novel by Raymond Radiguet. {{cite web|url=http://en.unifrance.org/movie/3530/devil-in-the-flesh|title=Devil in the Flesh
|publisher=unifrance.org |accessdate=2014-01-25}} 

==Plot==
During World War II a young man seduces an older woman while her husband is a soldier fighting at the front.

==Cast==
*Micheline Presle as Marthe Grangier 
*Gérard Philipe as François Jaubert 
*Denise Grey as Madame Grangier 
*Jean Debucourt as Monsieur Jaubert  Palau as Monsieur Marin 
*Jean Lara as Jacques Lacombe 
*Michel François as Rene 
*Richard Francoeur as the receptionist 
*Max Maxudian as the principal
*Germaine Ledoyen as Madame Jaubert 
*Jeanne Pérez as Madame Marin 
*Jacques Tati as officer

==Accolades== fifth best film of 1949 in film|1949.

==References==
 

== External links ==
 
* 
* 
* 


 

 
 
 
 
 
 
 
 
 


 
 