All Cheerleaders Die (2013 film)
{{Infobox film  name = All Cheerleaders Die image = File:All Cheerleaders Die (2013 film poster).jpg caption =  director = Lucky McKee Chris Sivertson producer = Lucky McKee writer = Lucky McKee Chris Sivertson starring =Caitlin Stasey music = Mads Heldtberg cinematography = Greg Ephraim editing =Ben La Marca, Zach Passero studio = Modernciné distributor = Image Entertainment, Celluloid Dreams released =   runtime = 90 minutes country = United States language = English budget = 
}}
 film of the same name that was also written and directed by McKee and Sivertson, and stars Caitlin Stasey as a cheerleader that must fight against the supernatural.  The movie had its world premiere on September 5, 2013, at the Toronto International Film Festival and had a limited theatrical release in June 2014. 

==Plot==
The film opens with Maddy (Caitlin Stasey) recording footage of her childhood friend Alexis (Felisha Cooper) as she prepares for the final days of school before summer break and for cheerleading practice. She discusses how important it is to remain fit and how dangerous cheerleading can be, as it is easy it is for some of the more advanced cheerleader moves to end with severe or deadly injuries. This proves to be the case when Alexis is thrown into the air and her teammates fail to catch her in time, resulting in her death. 

Once school resumes Maddy decides that she will try out for the cheerleading team and manages to impress the entire team with her acrobatic skills. After being accepted, Maddy notes that Tracy (Brooke Butler) has begun dating Terry (Tom Williamson), a star football player that had been dating Alexis prior to her death. She manages to start getting along with the other cheerleaders, the overly religious and prissy Martha (Reanin Johannink) and her shy sister Hanna (Amanda Grace Cooper), who serves as the cheerleading mascot. This provokes Maddys ex-girlfriend Leena (Sianoa Smit-McPhee), who cant understand why Maddy would want to hang out with the cheerleaders. Unbeknownst to everyone else, Maddy has actually joined the cheerleading squad to take revenge on Terry for as of yet unspecified reasons. 

Maddy begins taking her revenge by convincing Tracy that Terry had cheated on her during the summer and even manages to successfully seduce her at a group gathering of cheerleaders and football players. This greatly hurts Leena (who had been watching the gathering from afar) and angers Terry, who punches Tracy in a fit of anger. The cheerleaders all try to leave, only for Terry to cause an accident that claims the lives of all of the cheerleaders. Horrified at what shes seen, Leena manages to revive all of the dead cheerleaders using Pagan magic and magic stones. The following day the girls are all disoriented and scared, especially in the case of Martha and Hanna, as they have also somehow swapped bodies. Theyre also very hungry, which prompts them to attack one of Leenas neighbors and suck out all of his blood. The girls then go to school, where the football players all watch them with disbelief, as theyd thought them all dead. During the day the cheerleaders pick off the football players one by one, either out of hunger or in the case of Martha, out of anger when she realizes that her sister slept with her boyfriend Manny (Leigh Parker) using her body. 

While the girls were all initially willing to work together, their solidarity unravels due to the days deaths and the discovery that Maddy had joined the squad out of revenge. She tries to explain her reasoning, but none of the others will listen to her- especially not Tracy, as she had genuinely begun to fall in love with Maddy. The only person who will listen is Leena and Maddy tells her that she had been raped by Terry while attempting to film a memorial video for Alexis and that up until that point, she had not wanted revenge. The girls are then picked off one by one by Terry, who has figured out what is going on and manages to defeat them by cutting out the magic stones (which reside in the girls bodies) and swallowing them. Terry manages to corner Maddy and Leena in a graveyard where he tries to force Leena to show him how to use her magic for his benefit, only for him to die after Maddy attacks him and Leenas magic somehow manages to force the stones out of him. Maddy dies again as a result of this but Leena manages to revive her through her own grief, the same thing that caused the original resurrection. The two embrace, only to find that a bloody Alexis is tearing her way out of Terrys corpse (as he had landed on her grave) and screaming Leenas name. The film then cuts to the title card, which reveals that the film is part one in a series and that there will be a sequel.

==Cast==
*Caitlin Stasey as Maddy Killian
*Sianoa Smit-McPhee as Leena Miller
*Brooke Butler as Tracy Bingham
*Amanda Grace Cooper as Hanna Popkin
*Reanin Johannink as Martha Popkin Tom Williamson as Terry Stankus
*Chris Petrovski as George Shank
*Leigh Parker as Manchester Manny Mankiewitz
*Nicholas S. Morrison as Ben
*Jordan Wilson as Vik De Palma
*Felisha Cooper as Alexis Andersen
*Andre Allen Young as Featured extra

==Reception==
Critical reception for All Cheerleaders Die has been mixed and the film holds a rating of 47% "Rotten" on Rotten Tomatoes (based on 34 reviews), with the consensus "All Cheerleaders Die sets out to subvert horror tropes, but ends up falling victim to many of the same trashy cliches its trying to mock".  We Got This Covered praised the film for its originality, and summed the film up by saying "Mindless and contrived at points, no doubt, but All Cheerleaders Die is undeniably a witching, bitching good time worthy of the cliffhanger ending that suggests a future sequel may be in the cards."  Fearnet also gave a positive review, writing: "Whats probably most amusing about All Cheerleaders Die is that it will probably earn a lot of rentals from young male horror fans who smile at the idea of five evil succubi and the promise of some lesbian kissing -- when its actually a very smart and subversive satire about the way women are (very) often objectified in horror films."  In contrast, Reel Film panned the film for being overly bland and not fully utilizing its premise, which the reviewer felt had promise. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 