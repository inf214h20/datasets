The Feature
{{Multiple issues|
 
 
}}
{{Infobox film
| name     = The Feature
| director = Michel Auder Andrew Neel
| writer   = Michel Auder Luke Meyer Andrew Neel
| producer = Ethan Palmer
| starring = Michel Auder
| editing  = Luke Meyer
| studio   = SeeThink Films
| released =  
| country  = United States
}}
The Feature is a collaboration between filmmakers Michel Auder and Andrew Neel. Using a collection of videos that Auder had created over the previous 40 years, in combination with original present-day footage of Auder shot by Neel, the two filmmakers made a feature length film loosely based on the story of Auder’s life.
 Viva Superstar South Pacific.

The Feature is not a biographical film. Although the narrative is linear, footage from various periods in Auder’s life is used throughout the film. The story is augmented with satirical sit-down interviews of Auder in the present day, and biographical information (both fictional and real) is integrated into the hyper-theatrical scenes shot by Neel. 

== External links ==
*  
*  

 
 
 
 
 


 