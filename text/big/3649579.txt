The Peach Thief
 
{{Infobox film
| name           = Крадецът на праскови ( )
| image          =
| producer       = Atanas Papadopulos
| director       = Vulo Radev
| writer         = Emilian Stanev (story) Vulo Radev
| starring       = Nevena Kokanova Rade Marković Mikhail Mikhajlov
| music          = Simeon Pironkov
| cinematography = Todor Stoyanov
| editing         = Ana Manolova-Pipeva
| production_company = Boyana Film
| released       = 1964
| runtime        = 105 min. Bulgarian
}}

The Peach Thief ( ) is a 1964 Bulgarian film, directed by Vulo Radev and based on a story by Emilian Stanev. Set towards the end of World War I, it tells the story of a whirlwind love affair between a Serbian prisoner of war, Ivo Obrenovich (played by Rade Markovic) and a Colonels wife, Elisaveta, played by Nevena Kokanova.

The title refers to an incident in the plot in which Ivo Obrenovich is caught by Elisaveta in her private garden stealing peaches. Her lonely life with her cold husband leads to her reliance on their frequent meetings in the peach garden. They form a mutual deep and affectionate love for one another.

==External links==
* 

 
 
 
 
 