Geliebte Hochstaplerin
{{Infobox film
| name           = Geliebte Hochstaplerin
| image          = 
| image_size     = 
| caption        = 
| director       = Ákos Ráthonyi
| producer       = 
| writer         = Gregor von Rezzori   Jacques Deval (play)
| narrator       = 
| starring       = Nadja Tiller Walter Giller  Elke Sommer
| distributor    = Europa-Filmverleih AG
| released       =  
| runtime        = 88 minutes
| country        = West Germany
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}} German comedy film directed by Ákos Ráthonyi and starring Nadja Tiller, Walter Giller, Elke Sommer and Dietmar Schönherr.  It is based on a play by Jacques Deval. 

==Cast==
* Nadja Tiller – Martine Colombe
* Walter Giller – Robert Bolle
* Elke Sommer – Barbara Shadwell
* Dietmar Schönherr – David Ogden
* Loni Heuser – Mrs Ogden
* Ljuba Welitsch – Celia Shadwell
* Rainer Penkert – Schiffszahlmeister
* Frank Freytag – Mr Stanford

==References==
 

 
 
 
 
 


 
 