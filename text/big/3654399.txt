White Nights (1957 film)
{{Infobox film
| name           = Le notti bianche
| image          = Lenottibianche.jpg
| writer         = Luchino Visconti Fyodor Dostoyevsky (short story)
| starring       = Maria Schell Marcello Mastroianni Jean Marais
| director       = Luchino Visconti
| producer       = Franco Cristaldi
| music          = Nino Rota
| distributor    =
| released       = 6 September 1957
| runtime        = 97 min
| country        = Italy, France
| language       = Italian
| budget         =
}}
 1957 cinema Italian film neorealist Luchino White Nights.

==Plot==
A lonely young man, Mario, meets a lonely young woman, Natalia. Mario (Marcello Mastroianni) is lonely for social reasons; he is a stranger and a newcomer to town. Natalia (Maria Schell) is lonely because she has always lived in isolation, even in the heart of the city. Her loneliness is intensified because she is in love with a man (Jean Marais) who may not ever return to her, but who continues to occupy her heart to the exclusion of any other possible relationship. 

In turning the Dostoevsky story into a film, Visconti eliminated the first-person narration and made Natalia less of an innocent, and at times something of a hysteric and a tease. For his part, Mario rejects obvious offers of romantic attention from other women in the story, holding on to a fruitless obsession.

Mario thanks the young woman for the moment of happiness she has brought him. However, he is left alone at the end of the film, befriending the same stray dog he met at the beginning. He is back at square one, and has put more energy into pursuing the fantasy of an obsession rather than any prospect of real love.

==Cast==
*Maria Schell ...  Natalia
*Marcello Mastroianni ...  Mario
*Jean Marais ...  Tenant
*Marcella Rovena ...  Landlady
*Maria Zanoli ...  Maid (as Maria Zanolli)
*Elena Fancera ...  cashier
*Lanfranco Ceccarelli
*Angelo Galassi
*Renato Terra
*Corrado Pani
*Dirk Sanders ...  Dancer
*Clara Calamai ...  Prostitute

==Remake== Two Lovers, a 2009 movie by James Gray, is loosely based on the 1957 movie.

==References==
 

==External links==
* 
*  
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 

 
 