Hara-Kiri: Death of a Samurai
 
{{Infobox film
| name           = Hara-Kiri: Death of a Samurai
| image          = Hara-Kiri.jpg
| caption        = Film poster
| director       = Takashi Miike
| producer       = Toshiaki Nakazawa Jeremy Thomas
| writer         = Yasuhiko Takiguchi Kikumi Yamagishi
| starring       = Ichikawa Ebizō XI Eita Kōji Yakusho 
| music          = Ryuichi Sakamoto
| cinematography = Nobuyasu Kita
| editing        = Kenji Yamashita
| distributor    = Shochiku (Japan)
| released       =  
| runtime        = 126 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}} 3D drama film directed by Takashi Miike. It was produced by Jeremy Thomas and Toshiaki Nakazawa, who previously teamed with Miike on 2010s 13 Assassins. The film is a 3D remake of Masaki Kobayashis 1962 film Harakiri (1962 film)|Harakiri. 
 Michael Atkinson praised it describing it as "a melodramatic deepening and a grisly doubling-down of Kobayashis great original."  Composer and pop star Ryuichi Sakamoto wrote the original score.

==Cast==
* Ichikawa Ebizō XI as Tsugumo Hanshiro
* Eita as Chijiiwa Motome
* Hikari Mitsushima as Miho
* Naoto Takenaka as Tajiri
* Munetaka Aoki as Omodaka Hikokurō
* Hirofumi Arai as Matsuzaki Hayatonoshō
* Kazuki Namioka as Kawabe Umanosuke
* Yoshihisa Amano as Sasaki Ii Kamon-no-kami Naotaka
* Takashi Sasano as Sōsuke
* Nakamura Baijaku II as Chijiiwa Jinnai
* Kōji Yakusho as Saitō Kageyu

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 