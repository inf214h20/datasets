The Last Step
{{Infobox film
| name           = The Last Step
| image          = 
| alt            =  
| caption        = Theatrical release poster
| director       = Ali Mosaffa
| screenplay     = Ali Mosaffa
| starring       = Leila Hatami Ali Mosaffa Alireza Aghakhani Hamed Behdad

| cinematography = Alireza Barazandeh
| editing        = Fardin Saheb Zamani
| studio         = Road Film Production
| distributor    = Hedayat Film
| released       =  
| runtime        = 88 minutes
| country        = Iran
| language       = Persian

}}

The Last Step (Persian :پله آخر) is Ali Mosaffas second feature film following Portrait of a Lady Far Away (2005) featuring Leila Hatami, Ali Mosaffa, and Alireza Aghakhani. The film is considered one of the few independently produced features within Iranian cinema and gathered worldwide success after its world premiere at the 47th Karlovy Vary International Film Festival in July, 2012.

==Awards==
* Best Adapted Screenplay Ali Mosaffa from the 16th Iranian House of Cinema Film Festival, 2014
* FIPRESCI Prize for Best Film, Karlovy Vary International Film Festival 2012
* Crystal Globe Award for Best Actress Leila Hatami, Karlovy Vary International Film Festival 2012
* Crystal Simorgh Prize for Best Adapted Screenplay Ali Mosaffa, Fajr International Film Festival 2012
* Special Jury Prize at the Kerala International Film Festival 2012
* Award for Best Actor Alireza Aghakhani, Batumi International Art House Film Festival (BIAFF) 2012
* Piuculture Award at the Medfilm International Film Festival 2012

==Reviews==
* FIPRESCI-Karlovy Vary International Film Festival:  
* The Hollywood Reporter:  
* IONCINEMA:  
* American Film Institute Review:  
* SBCC Film Reviews:  

==External links==
*  
*  
*  

 
 
 
 


 