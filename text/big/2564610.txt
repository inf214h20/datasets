The Way Home (2002 film)
{{Infobox film name           = The Way Home  image          = The Way Home film.jpg caption        =  director       = Lee Jeong-hyang  writer         = Lee Jeong-hyang starring       = Kim Eul-boon Yoo Seung-ho  producer       = Hwang Woo-hyun Hwang Jae-woo music          = Kim Dae-heung Kim Yang-hee   cinematography = Yoon Heung-sik  editing        = Kim Jae-beom Kim Sang-beom studio         = Tube Pictures distributor    = CJ Entertainment Tube Entertainment released       =   runtime        = 87 minutes language       = Korean country        = South Korea film name      = {{Film name hangul         =   rr             = Jib Euro mr             = Chip Ŭro hanja          = }} budget         =  gross          =   
}} best picture and Grand_Bell_Awards#Best_screenplay|screenplay.

The Way Home was the second-highest grossing homegrown film in South Korea in 2002. It was released on DVD, with English subtitles, in 2003 by Paramount Pictures|Paramount. 

==Plot==
The story begins on a fine summers morning, when Sang-woo (Yoo Seung-ho) and his mother board a bus to the country. It is soon clear that the unsophisticated rural passengers annoy the seven-year-old urban boy. His mother is taking him to live with his 75-year-old mute, but not deaf, grandmother (Kim Eul-boon) while she looks for a new job after a business venture failed in Seoul. Eventually they reach their destination, a dusty bus stop in the Korean countryside near an unsophisticated village.
  
By now Sang-woo, who has arrived with junk food and toys, has no intention of respecting his mute grandmother especially as her house is primitive with no running water. His mother apologizes for leaving the boy, telling her own mother it will not be for too long before leaving on the next bus. Alone Sang-woo ignores his grandmother, not even wanting to look at her and even calling her a byungshin, or "retard". Next morning, his grandmother starts another day. She goes down the hill to get clean water and washes her clothes at the river. She also tends the melons that she will sell at the market.

One of the Grandmothers neighbors is a hard-working country boy who attempts to become friends with Sang-woo, who declines until the end when he apologizes for making fun of him. The other is a young girl who Sang-Woo falls in love with, but she is more interested in the country boy.

The grandmother, who also cares for her old friends very much, lives a simple and humble life. Eventually, from constant play, Sang-woos Game Boy runs-out of batteries so he asks his grandmother for money for new ones. But she is poor and has none. Selfishly he teases her, and in an intolerant manner throws away her shoes, breaks her (empty) chamber pot, and draws graffiti on her house walls. 

When this fails to get money from his grandmother, Sang-woo steals her ornamental hairpin to trade for batteries. He then goes off to find the shops. When he finally finds the right place he attempts to trade the silver hairpin but instead of getting batteries the shop keeper, who happens to be his grandmothers friend, hits him on the head and sends him home. 
 boiled chicken instead of fried chicken. When Sang-woo wakes up he sees the boiled chicken he gets angry, throwing the food away. Later in the night he finishes the food because he is hungry. The next morning, his grandmother becomes ill and Sang-woo cares for her by wrapping her in additional blankets and serving the remaining chicken.

Despite the hardships faced by the old grandmother who has osteoporosis, the only thing she needs Sang-woo for is to run thread through her needles. She stitches the shoes and shares her earnings with a friend who ends their meeting with the words "Come by again before one of us dies."

Sang-woo remains angry and confused by the unfamiliar environment and repeatedly rejects her attempts to please him. But her unconditional love slowly touches his heart. One day, Sang-woo gets up early and goes with his grandmother to the market where he sees how hard his grandmother persuades passers-by to buy her vegetables. After a long day at the market she takes Sang-woo to a shop and buys him noodles and new shoes. When they are about to board the bus home, Sang-woo asks his grandma to buy him a Choco Pie. 

The grandmother goes to a shop that is run by an elderly friend. The shopkeeper, who now has a bad knee, gives her five or six pies but refuses to take any money, so the grandmother gives the shopkeeper a melon. But when the grandmother returns to the bus with the sweets, Sang-Woo says he wants to ride alone as the girl he likes is also on board. The grandmother tries to get Sang-Woo to take the rest of the produce with him but he refuses. The bus then leaves. Sang-Woo then has to wait for his grandmother to return wondering why it is taking her so long. He then realizes that his grandmother has walked back from town carrying all her produce.  (Note: It was revealed later in the movie that she has money to ride the bus but instead placed it in Sang-woos Game Boy for him to buy batteries.)

Eventually Sang-woo grows to love his grandmother, just as Sang-woos mother writes to tell them she is coming to bring him home.  Worried that she cannot telephone, speak, and, and because she is unable to read or write he attempts to teach her to write two sentences: I Miss You, and, I Am Sick.  When she practices without much success, he tells her to send him a blank letter if she is sick, and he will know it is from her, and that she needs him to come to her side, which he will do right way.  

Sang-woo then does what little he can before he leaves, which is to thread as many needles as he can for his grandmother to use after he has returned to Seoul. His depth of feeling for his grandmother are revealed when the bus leaves and he leaps to the back window to wave his tearful farewells. The film closes with the grandmother continuing to live alone in the thatched-roof house, and, we see many of his favorite postcards in the house:  On the back, he has used his crayons to draw images. Some are of his grandmother smiling, and some are of his grandmother sick.  He writes "I miss you" on some of the cards and "I am sick" on others, so that she can mail them to him and he will know she is alright, and will have mail from her.    

Before the end credits, there is a note of dedication to all grandmothers around the world.

==Cast==
*Kim Eul-boon as Grandmother
*Yoo Seung-ho as Sang-woo
*Dong Hyo-hee as Sang-woos mother
*Min Kyung-hyun as Cheil-e, country boy, neighbor of the grandmother
*Im Eun-kyung as Hae-yeon

==Reception==
The film received generally positive reviews from both Western and Korean critics. The review aggregator Rotten Tomatoes reported that 75% of critics gave the film positive reviews, based on 56 reviews. The film currently holds an average score of 7.5 on IMDb. Steven Rea from the Philadelphia Inquirer gave the film 3.5 stars out of 4 saying that "Jeong-Hyang Lees film is deceptively simple, deeply satisfying."

Many critics praised the style of the movie as well as the acting of the inexperienced Kim Eul-boon who at 78, had not only never acted before, but never even seen a film. The film went on to win the Best Film and Best screenplay awards at the Grand Bell Awards, the Academy Awards|Oscars equivalent in South Korea. It was also nominated for Best Asian Film at the 22nd Hong Kong Film Awards, but lost to My Sassy Girl, another South Korean film.

==Filming locations==
The film was filmed in and around Jeetongma, North Gyeongsang Province, South Korea.

== References ==
 

==External links==
* 
* 

 
 
 
 
 
 

 
 
 
 
 
 