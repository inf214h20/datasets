Rome Express (1950 film)
{{Infobox film
| name =  Rome Express
| image =
| image_size =
| caption =
| director = Christian Stengel
| producer = 
| writer =  
| narrator =
| starring = Hélène Perdrière   Jean Debucourt   Denise Grey   Saturnin Fabre
| music =   Georges Van Parys 
| cinematography = 
| editing = Claude Nicole
| studio = E.T.P.C.   Consortium du Films 
| distributor = Consortium du Film
| released =     18 January 1950 
| runtime = 83 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Rome Express is a 1950 French thriller film directed by Christian Stengel and starring Hélène Perdrière, Jean Debucourt and Denise Grey. 

==Cast==
* Hélène Perdrière as  Hélène
* Jean Debucourt as Lacaze
*  Denise Grey as  Margot
*  Saturnin Fabre as le professeur
*  Arthur Devère as  Jeff Lambick
*  Jacqueline Pierreux as  Nicole
*  Charles Dechamps as Delafosse
*  Robert Pizani as  Cornaglia
*  Jean Tissier as Giovanni
*  Jacqueline Dor as  Denise
*  Roger Caccia as Pigeonnet
*  Mario Podesta as Valentino
*  Nicolas Amato
*  Madeleine Barbulée
*  Roger Caccia
*  Philippe Clay as  un employé de la SNCF
*  René Hell as  Le manager
*  Jacky Blanchot
* Georges Paulais as  Le commissaire
* Julien Maffre

== References ==
 

== Bibliography ==
* Rège, Philippe. Encyclopedia of French Film Directors, Volume 1. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 

 

 