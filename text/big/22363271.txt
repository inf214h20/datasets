Mike's Murder
 

{{Infobox film
| name           = Mikes Murder
| caption        =
| image	         = Mikes Murder FilmPoster.jpeg
| director       = James Bridges
| producer       = James Bridges
| screenplay     = James Bridges
| starring = {{Plainlist|
* Debra Winger
* Mark Keyloun
* Darrell Larson
* Paul Winfield
}} John Barry Joe Jackson
| cinematography = Reynaldo Villalobos
| editing        = Dede Allen
| studio         = The Ladd Company
| distributor    = Warner Bros.
| released       = March 9, 1984
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,059,966
}}

Mikes Murder is a 1984 film, written and directed by James Bridges, and starring Debra Winger, Mark Keyloun and Paul Winfield.

==Synopsis==
In Los Angeles, bank teller Betty Parrish has a one-night stand with a young tennis instructor named Mike, but then has only random contact with him over the course of the next two years.

He is a drug dealer. One day Mike calls to tell her he is being chased for encroaching on another criminals territory. Later, a friend of his calls to say Mike is dead, brutally murdered.

Betty cant let go of him, not without understanding him better, and tries to find out more. It leads to her discovering Mikes hidden side, including a disturbed acquaintance of his named Pete and a record producer named Philip who apparently was involved with Mike in a gay relationship. Bettys own life is placed in peril by the storys end.

==Cast==
* Debra Winger as Betty Parrish
* Mark Keyloun as Mike
* Darrell Larson as Pete
* Brooke Alderson as Patty
* Paul Winfield as Philip
* Robert Crosson as Sam Daniel Shor as Richard
* William Ostrander as Randy

==Production== 
Warner Brothers reportedly was unhappy about the project because of its premise with the drug-fixated underpinnings of the L.A. entertainment world and refused to release it until Bridges made some cuts and changes.  

Bridges wrote the film for Winger having worked with her on Urban Cowboy. Her performance in Mikes Murder led the critic Pauline Kael to describe Winger as "a major reason to go on seeing movies in the 1980s".

== DVD Release ==
Warner Bros. Digital Distribution released Mikes Murder on 4 August 2009, as part of the Warner Archive Collection series.

==References==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 