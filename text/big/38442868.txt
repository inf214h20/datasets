Roy (film)
 
 
{{infobox film
| name = Roy
| image = Roy film poster.jpg
| caption  = Theatrical release poster
| director = Vikramjit Singh
| producer = Bhushan Kumar Divya Khosla Kumar  Freeway Pictures
| writer = Vikramjit Singh
| starring =  
| music = Songs:  
| cinematography = Himman Dhamija
| editing = Dipika Kalra, Rajat Arora
| studio = T-Series 
| distributor = AA Films
| released =  
| country = India
| runtime = 144 minutes
| language = Hindi
| budget =   
| gross  =   
}}
 romantic thriller film, directed by debutant Vikramjit Singh and produced by Bhushan Kumar, Krishan Kumar, Divya Khosla Kumar and Ajay Kapoor under Freeway Pictures.  The movie features Arjun Rampal, Jacqueline Fernandez and Ranbir Kapoor in the lead roles with Shernaz Patel, Rajit Kapur and Shibani Dandekar in supporting roles. Anupam Kher appears in a cameo. The film is based on a mysterious thief and his theft, robbery and love-interest. The movie had its premiere in Dubai on 12 February 2015 and was released worldwide on 13 February 2015. 

==Plot==
Kabir Garewal (Arjun Rampal), a Casanova film maker and screenwriter is making multiple films (GUNS Trilogy) based on a thiefs life and robberies. All his films have been highly successful. In order to shoot the third part of the trilogy, he goes to Malaysia, where he meets a London-based film maker Ayesha (Jacqueline Fernandez). Kabir and Ayesha get friendly and soon fall in love. When Ayesha finds out about Kabirs Casanova attitude, she breaks up with him and returns to London. Dejected, Kabir goes into depression and returns to Mumbai, leaving his film incomplete. After several attempts, Kabir is unable to find the perfect climax for his film. On his assistant Meeras (Shernaz Patel) suggestion, Kabir attends a film festival as part of the jury, where Ayeshas film is being screened. Ayesha thinks Kabir is following her and asks him to stay away from her life. After his fathers (Anupam Kher) death followed by him being sued by his films financiers, Kabir decides to move on and complete his film.
 art thief whom no country seems to be able to get their hands on, including Detective Wadia (Rajit Kapur). On his new assignment, Roy goes to an unknown foreign land to steal an expensive painting only to find that its owner is the beautiful Tia (also played by Jacqueline Fernandez). Tia lives alone in a huge mansion where the painting is kept. Hence, Roy tries to befriend Tia during an art auction and soon impresses her with his charm. Sparks fly between them and both start spending time together, giving Roy entry into the mansion. Looking for the right opportunity, one night Roy runs away with the painting, leaving Tia heartbroken. Roy later regrets, as he realizes he has fallen in love with Tia. He decides to return the painting to her. After a small clash with the paintings new owner, Roy manages to get it back. On receiving the painting, Tia realises that Roy is now a changed man and forgives him.

Meanwhile, Kabirs film Guns 3 is released and is hugely successful. It is then revealed that "Roy" was just an imaginary character in Kabirs film and Roy and Tias story was actually the script of Guns 3. In the end Kabir proposes to Ayesha and they reunite, just like Roy and Tia reunite in Kabirs film. The movie ends with Roy and Tia walking together next to a bridge just like the painting Roy had stolen.

==Cast==
*Arjun Rampal as Kabir Garewal
*Jacqueline Fernandez as Ayesha Aamir / Tia
*Ranbir Kapoor as Roy
*Shernaz Patel as Meera
*Anupam Kher as Mr. Garewal (cameo)
*Rajit Kapur as Detective Wadia
*Shibani Dandekar as Zoya
*Barun Chanda as Roys boss
*Cyrus Brocha as a Talk-show host

==Production==
The film was produced on a budget of  , which included   for production and   for P&A. The film recovered   from satellite rights,   from music rights and   from overseas rights. 

===Promotions and marketing===
The trailer of the film was released on 17 December 2014 and passed over 1 million views on YouTube within 24 hours. The film was promoted at famous reality shows like Bigg Boss and Comedy Nights with Kapil.

==Critical reception==
NDTV rated the film 2 out of 5 stars and called the film boring enough to make almost everyone sleep. Rediff rated the film 0.5 out of 5 stars and called the film "A Slow Torture". Firstpost left the film with no rating also referring to the film as "a slog to sit through with too many unnecessary songs and diversions and not enough meat to either thrill or entertain." Indian Express movie critic gave only .5 stars and questioned "What is Ranbir Kapoor doing in a movie like this?"   Rajeev Masand on ibnlive gave only one star calling it "The film limps lethargically and collapse in a predictable twist ending" 

==Soundtrack==
{{Infobox album
| Name = Roy
| Type = Soundtrack
| Artist =  
 
| Released =  
| Length =   Feature Film Soundtrack
| Label = T-Series
| Last Album = }}
The songs of Roy are composed by Ankit Tiwari, Meet Bros Anjjan and Amaal Mallik, while the lyrics are written by Kumaar, Sandeep Nath and Abhendra Kumar Upadhyay.

Sanjoy Chowdhury composed the film score.

===Track listing===

{{track listing
| extra_column=Singer(s)
| lyrics_credits=yes
| music_credits=yes
| total_length =  
| title1= Sooraj Dooba Hain
| extra1= Arijit Singh, Aditi Singh Sharma
| lyrics1= Kumaar
| music1= Amaal Mallik
| length1=4:23
| title2= Tu Hai Ki Nahi
| extra2= Ankit Tiwari
| lyrics2= Abhendra Kumar Upadhyay
| music2= Ankit Tiwari
| length2= 5:47
| title3= Chittiyaan Kalaiyaan 
| extra3= Meet Bros Anjjan, Kanika Kapoor
| lyrics3= Kumaar
| music3= Meet Bros Anjjan
| length3= 4:19
| title4= Yaara Re KK
| lyrics4= Sandeep Nath
| music4= Ankit Tiwari
| length4= 4:58
| title5= Boond Boond
| extra5= Ankit Tiwari
| lyrics5= Abhendra Kumar Upadhyay
| music5= Ankit Tiwari
| length5= 5:21
| title6= Tu Hai Ki Nahi (Unplugged)
| extra6= Tulsi Kumar
| lyrics6= Abhendra Kumar Upadhyay
| music6= Ankit Tiwari
| length6= 5:21
| title7= Sooraj Dooba Hai (Version 2)
| extra7= Aditi Singh Sharma, Arijit Singh
| lyrics7= Kumaar
| music7= Amaal Mallik
| length7= 4:24}}

===Reception===
The songs of Roy received positive response from critics. Rediff awarded the album with 4 out of 5 stars and called it "a winner all the way".  Koimoi published, "After a really long time, here is an album that has songs which are truly worth listening peacefully on your phones. This album will surely make its way to your playlist and most songs are worth playing on loop." 

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 