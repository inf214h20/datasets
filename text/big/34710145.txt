Les Petites vacances
 
{{Infobox film
| name           = Les Petites Vacances
| director       = Olivier Peyon
| writer         = Olivier Peyon, Cyril Brody & Gladys Marciano
| starring       = Bernadette Lafont, Claude Brasseur
| music          = Jérome Baur
| cinematography = Alexis Kavyrchine
| editing        = Fabrice Rouaud
| distributor    = Pierre Grise Distribution
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

Les Petites Vacances is a 2006 French drama film directed by Olivier Peyon.

==Plot==
Danielle (Bernadette Lafont), a grandmother in her sixties, is planning to take her two grandchildren to their father’s house for the Easter vacation. Since retiring as a schoolteacher, Danielle has regularly taken on this responsibility after her daughter’s divorce. This time, however, the children’s father is not there to welcome them, giving Danielle the opportunity to spend a bit more time with her grandchildren and to take them out for the day.

Jumping at every opportunity presented to them, Danielle soon transforms this day-outing into an impromptu holiday. Only what starts out as a fun adventure gradually turns into an inexorable deconstructing experience, and it soon becomes impossible for Danielle to contemplate taking the kids back…

==Cast==
* Bernadette Lafont as Danièle
* Adèle Csech as Marine
* Claude Brasseur as The Man in The Palace
* Lucas Franchi as Thomas
* Claire Nadeau as Nicole
* Eric Savin as Eric

==External links==
*  

 
 
 
 
 
 


 