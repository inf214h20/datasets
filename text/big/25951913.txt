Ma and Pa Kettle Go to Town
{{Infobox film
| name = Ma and Pa Kettle Go to Town
| image = MaandPaKettleGotoTown.jpg
| caption = Theatrical release poster
| director = Charles Lamont
| producer = Leonard Stern
| starring = Marjorie Main and Percy Kilbride
| music =
| cinematography =
| editing =
| studio = Universal-International
| distributor = Universal-International
| released =  
| runtime = 79 minutes
| country = United States
| awards =
| language = English
| budget =
| gross = $2,175,000 (US rentals) 
}}
Ma and Pa Kettle Go to Town is a 1950 American comedy film directed by Charles Lamont. It is the second installment of Universal Studios|Universal-Internationals Ma and Pa Kettle franchise starring Marjorie Main and Percy Kilbride.

==Plot== contest from the Bubble-Ola Company, and the prize - a free all-paid trip to New York City. Ma tells Pa that they cant go because they have no one to look out for the kids. Meanwhile, famous bank robber Shotgun Munger crashes and drives into the Kettle Farm. Pa comes along and greets him, after a long conversation Munger tells Pa that he can stay and watch over the kids (he was trying to hide from the police). So, Ma and Pa Kettle go to New York City and get in contact with their daughter-in-law Kim and their son Tom.

==Cast==
*Marjorie Main as Ma Kettle
*Percy Kilbride as Pa Kettle Richard Long as Tom Kettle
*Meg Randall as Kim Parker Kettle
*Charles McGraw as Shotgun Mike Munger
*Jim Backus as Joseph Little Joe Rogers Elliott Lewis as Detective Sam Boxer
*Bert Freed as Dutch, Third New York Henchman
*Hal March as Det. Mike Eskow Sherry Jackson, Douglas Kennedy, William Newell, Eugene Persson, J.P. Sloane (Billy Kettle)  (Bill Olan Soule, Willard Waterman, James Westerfield and Chief Yowlachie
 

==Production==
The film was originally called Ma and Pa Kettle Go to New York. Filming started 8 August 1949 in New York. CHASE STORY SOLD TO PROSER, NASSER: Shadow of a Hero May Go Before Cameras Ahead of High Button Shoes
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   30 July 1949: 8.   
==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 