.45 (film)
 {{Infobox Film
| name           = .45 
| image          = 45 (film) poster.jpg 
| image_size     = 
| caption        = Official U.S. Film Poster
| director       = Gary Lennon
| producer       = David Bergstein
| writer         = Gary Lennon
| narrator       = 
| starring       = Milla Jovovich Angus Macfadyen Aisha Tyler Stephen Dorff Sarah Strange
| music          = Timothy Fitzpatrick John Robert Wood
| cinematography = Teodoro Maniaci
| editing        = William M. Anderson Richard Nord
| distributor    = Mobius Entertainment
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 thriller film starring Milla Jovovich, Angus Macfadyen, Aisha Tyler, Stephen Dorff, and Sarah Strange. Gary Lennon, whose last feature was 1995s Drunks, wrote and directed the film. Variety describes the film as a "doublecross pic set in the underworld of Hells Kitchen, Manhattan|Hells Kitchen." .45 was released theatrically in Greece, Taiwan, Singapore, Japan, South Korea, and Mexico.

==Plot== NYPD and ATF on them. They live in a shabby apartment together. However, Vic (Sarah Strange), a lesbian best friend of Kat who also has a crush on her, doesnt approve of them and hates Big Al.

One day, Kat and Big Al are almost arrested when the police raid comes over, but they are released when they realize that Big Al and Kat are both unarmed. Worried about his future in gun business, he meets up with his old partner Reilly (Stephen Dorff) and they briefly return to their car theft business to earn extra money on the side so Al can provide with new guns.

However, Kat has plans on her own; she wants to sell guns on her own to provide since she wants to move out of Queens and into a classier neighborhood out of Queens. One day, she meets up with Jose (Vincent Laresca), a drug dealer and Big Als rival, and sells him a gun. However, Clancy (Tony Munch), a local snitch working for Al, notices them. Kat returns home and pretends to havve been out with Vic, upsetting Al.

Soon enough, Al gets into a brawl at the local bar when the customer refuses to pay more for the gun than intended, during which Kat secretly swaps new plans with Jose. Al, however, gets a tip from Clancy, and drags Kat home, where he forces her to confess and then beats her up brutally after the confession for working business behind her back, then leaves the apartment.

The next day, however, Al is arrested and detained for questioning after he beats up a burglar since the police is suspicious of him. Kat is also questioned, and she is introduced to Liz (Aisha Tyler), a member of an anonymous group that protects women against her violent ex-boyfriends, since they noticed her bruises. However, Kat refuses to snitch on Big Al. However, Big Al beats her up again and then cuts her hair off, after which Kat snaps.

She devises a plan; seducing Vic, Reilly and Liz, she has Liz taking Big Als jacket with lettering on it and shoot Clancy, while Reilly provides her with the gun, and Liz with a place to stay. The next day, Big Al breaks into a church after being drunk and threatens the priest, Father Duffel (Hardie Lineham), and he is detained by the police for Clancys murder.

Soon enough, Kat meets up with Liz and explains to her that she only used her, leaving Vic heartbroken. However, Kat leaves her on good terms, also thanking her for her advice and telling her that she decided to leave New York, having nothing left here. Big Al is sentenced to 15 years for murder, Vic returns to her apartment alone, while Reilly and Vic start a relationship together. The film ends with Kat narrating the whole story, revealing that she moved to a tropical area, before walking away.

==Cast==
*Milla Jovovich as Kat
*Angus Macfadyen as Big Al 
*Stephen Dorff as Reilly 
*Aisha Tyler as Liz
*Sarah Strange as Vic 
*Vincent Laresca as Jose
*Tony Munch as Clancy
*Kay Hawtrey as Marge John Robinson as Cop #1
*Tim Eddis as Cop #2
*Hardie Lineham as Father Duffel
*Dawn Greenhalgh as Fran
*Nola Augustson as Gertie John Gordon as Danny Shawn Campbell as Original Owner
*Robin Brule as Waitress
*Mike McLay as Gerties Husband
*Suresh John as Thief
*Conrad Bergschneider as Sarge
*Chantelle Jewkes as Hooker

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 