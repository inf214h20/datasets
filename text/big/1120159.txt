The Musketeers of Pig Alley
 
{{Infobox film
| name           = The Musketeers of Pig Alley
| image          = The Musketeers of Pig Alley.webm
| writer         = D. W. Griffith Anita Loos Walter Miller
| director       = D. W. Griffith
| producer       =
| distributor    = General Film Company
| released       =   frames per second)
| language       = Silent film English intertitles
| country        = United States 
| music          = Robert Israel
| budget         =
}}
 American short short drama film credited as the first gangster film in history. It is directed by D. W. Griffith and written by Griffith and Anita Loos. It is also credited for its early use of follow focus, a fundamental tool in cinematography. 

The film was released on October 31, 1912 and re-released on November 5, 1915 in the United States. The film was shot in Fort Lee, New Jersey where many other early film studios in Americas first motion picture industry were based at the beginning of the 20th century.    Location shots in New York City reportedly used actual street gang members as extras during the film.

It was also shown in Leeds Film Festival in November 2008, as part of Back to the Electric Palace, with live music by Gabriel Prokofiev, performed in partnership with Opera North.

==Plot==
The film is about a poor married couple living in New York City. The husband works as a musician and must travel often for work. When returning, his wallet is taken by a gangster. His wife goes to a ball where a man tries to drug her, but his attempt is stopped by the same man who robbed the husband. The two criminals become rivals, and a shootout ensues. The husband gets caught in the shootout and recognizes one of the men as the gangster who took his money. The husband sneaks his wallet back and the gangster goes to safety in the couples apartment. Policemen track the gangster down but the wife gives him a false alibi.

==Cast==
* Elmer Booth – Snapper Kid, Musketeers gang leader
* Lillian Gish – The Little Lady
* Clara T. Bracy – The Little Ladys Mother Walter Miller – The Musician
* Alfred Paget – Rival Gang Leader John T. Dillon – Policeman
* Madge Kirby – The Little Ladys Friend/In Alley Harry Carey –  Snappers Sidekick
* Robert Harron – Rival Gang Member/In Alley/At Gangsters Ball
* W. C. Robinson – Rival Gang Member (as Spike Robinson)
* Adolph Lestina – The Bartender/On Street
* Jack Pickford – Boy Gang Member/At Dance Ball
*Antonio Moreno – Young Man at Dance Ball who Leaves

unbilled
*Gertrude Bambrick – Woman at Dance
*Lionel Barrymore – The Musicians Friend
*Kid Broad  Walter Long

==Influence==
The Musketeers of Pig Alley is probably the first ever film about organised crime.

In his book The Movie Stars, film historian Richard Griffith wrote of the scene where Lillian Gish passes another woman on the street (pictured):

     "..Griffiths camera in this scene happened to focus on the unforgettable face of the nameless girl
      in the center of the shot- and a murmurous wave swept audiences at this point in the film whenever 
      it was shown.  No one knows what became of this particular extra, but such raw material, and such 
      camera accidents, became the stuff of stardom later on." 

In fact, the girl is Dorothy Gish, Lillians sister.

In the Cold Case episode Torn (Season 4.21) Lily sees the victim of a 1919 homicide in a homage to the scene of Lillian Gish passing another woman on the street (pictured).

==See also==
* Lionel Barrymore filmography
* Harry Carey filmography
* Lillian Gish filmography
* D. W. Griffith filmography

==References==
 

==External links==
 
* 
*  on YouTube
*  available for free download at  

 
 

 
 
 
 
 
 
 
 
 
 
 