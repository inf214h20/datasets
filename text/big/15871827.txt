Blue City (film)
 
{{Infobox film
| name           = Blue City
| image          = Bluecityposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Michelle Manning Walter Hill Walter Hill novel by Ross Macdonald
| narrator       =
| starring       = {{Plainlist | 
* Judd Nelson
* Ally Sheedy
}}
| music          = Ry Cooder, the Textones
| cinematography = Steven B. Poster
| editing        = Ross Albert
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = $6,947,787
}}
 novel of the same name about a young man who returns to a corrupt small town in Florida to avenge the death of his father. The film was directed by Michelle Manning, and stars Judd Nelson, Ally Sheedy and David Caruso. 

==Plot==
Returning to the small Florida town where he grew up, Billy Turner (Judd Nelson) learns that his father has been killed. With little help from the police, Billy will take matters into his own hands and go up against a ruthless local mob in a desperate search to find the killer.

  

==Cast==
* Judd Nelson as Billy Turner
* Ally Sheedy as Annie Rayford
* David Caruso as Joey Rayford
* Paul Winfield as Chief Luther Reynolds Scott Wilson as Perry Kerch
* Anita Morris as Malvina Kerch-Turner Luis Contreras as Lieutenant Ortiz
* Julie Carmen as Debbie Torres
* Allan Graf as Graf
* Hank Stone as Hank
* Tommy Lister, Jr. as Tiny
* Rex Ryon as Rex
* Felix Nelson as Caretaker
* Willard E. Pugh as Leroy
* Sam Whipple as Jailer
The Textones (Carla Olson, Joe Read, George Callins, Phil Seymour and Tom Jr Morgan) appear in the film performing their song You Can Run as produced by Ry Cooder.

==Production==
The novel was originally published in 1947. WILL THE REAL ROSS MACDONALD PLEASE KEEP WRITING?
Adler, Dick. Los Angeles Times (1923-Current File)   10 Dec 1967: n79.  It was compared to the work of Dashiell Hammett, in particular Red Harvest. Ross Macdonald, his Lew Archer and other secret selves: Ross Macdonald
By JOHN LEONARD. New York Times (1923-Current file)   01 June 1969: BR2. 

Walter Hill wrote the script with Lukas Heller; it was originally intended to star a leading man in his mid-30s but by the mid 1980s a number of popular young male actors had emerged, so the script was rewritten to accommodate one of them. (The lead in the original novel was a man in his early 20s, although a war veteran.) Hill handed over directing duties to Michelle Manning. It was Mannings first film as director although she worked with Sheedy and Nelson on The Breakfast Club as a producer. 

"I dont think Ill become Samantha Peckinpah," said Manning, "but I dont think as a woman that I should have to make a movie with girls in locker rooms putting on make up." 

Filming started in February 1985. FILM CLIPS: YOUTH CALLS THE SHOTS IN BLUE CITY
London, Michael. Los Angeles Times (1923-Current File)   15 Feb 1985: l1.  Preview audiences disliked the movies ending so it was reshot. 

===Props===
The motorcycle used is apparently the same 1978/9 750cc Triumph Bonneville T140E used by Richard Gere in An Officer and a Gentleman (1982) in which David Caruso also appeared. 

==Release and reception==
Highly anticipated at the time by Brat Pack fans, Blue City was considered a disappointment by many and is best remembered today mainly due to its very poor reception by critics upon release.    Judd Nelsons performance was particularly criticised. BRAT WHAPPING
Broeske, Pat H. Los Angeles Times (1923-Current File)   11 May 1986: Q14. 
 

===Awards===
It was nominated for 5 Golden Raspberry Awards in the 7th Golden Raspberry Awards, but won None:
Worst Actor (Nelson) (lost to Prince in Under the Cherry Moon) Madonna in Shanghai Surprise)
Worst Supporting Actor (Scott Wilson) (lost to Jerome Benton in Under the Cherry Moon)
Worst Director (lost to Prince for Under the Cherry Moon)
Worst Picture (lost in a tie to Howard the Duck and Under the Cherry Moon). 

===Box Office===
The film earned $2.7 million in its first weekend and was a box office disappoinment. 

Ally Sheedy later said she "didnt particularly like" the film:
 Michelle Manning and I had been close friends. And she was so excited about the chance to direct that my feeling was it would be really great to work with someone I really liked and help contribute to their first big project. I was very naive, I guess, because I kept hoping it would turn out OK, that somehow all the stuff that was missing would miraculously appear when they edited it all together. I guess thats not the way it works, so I was disappointed. FOR A MOVIE STAR, SHE DOESNT ACT THE PART
Goldstein, Patrick. Los Angeles Times (1923-Current File)   13 May 1986: J1.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 