Her Purchase Price
{{infobox film
| name           = Her Purchase Price
| image          =Her Purchase Price (1919) 1.jpg
| imagesize      =180px
| caption        =Theatrical poster
| director       = Howard C. Hickman
| producer       = B&B Features
| writer         = Maie B. Harvey(story) Harvey F. Thew(scenario)
| starring       = Bessie Barriscale
| mucic          =
| cinematography = Gus Peterson
| editing        =
| distributor    = Film Booking Offices of America|Robertson-Cole
| released       = September 1, 1919
| runtime        = 5 reels
| country        = USA
| language       = Silent(English intertitles)

}} lost  1919 silent film romance distributed by newly formed Film Booking Offices of America|Robertson-Cole. It was directed by Howard C. Hickman and starred Bessie Barriscale.  

==Cast==
*Bessie Barriscale - Sheka
*Alan Roscoe - Sir Derek Anstruther
*Joseph J. Dowling - Hamid-Al
*Kathlyn Williams - Diana Vane
*Stanhope Wheatcroft - George Vincent
*Irene Rich - Marda
*Henry Kolker - Duke of Wryden
*Wedgwood Nowell - ?
*Una Trevelyn - ?

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 