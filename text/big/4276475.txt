The Dark Knight (film)
 
 
 
 
{{Infobox film
| name                 = The Dark Knight
| image                = Dark Knight.jpg
| caption              = Theatrical release poster
| director             = Christopher Nolan
| producer             = {{Plain list|
* Emma Thomas
* Charles Roven
* Christopher Nolan
}}
| screenplay           = {{Plain list|
* Jonathan Nolan
* Christopher Nolan
}}
| story                = {{Plain list|
* Christopher Nolan
* David S. Goyer
}}
| based on             =   
| starring             = {{Plain list|
* Christian Bale
* Michael Caine
* Heath Ledger
* Gary Oldman
* Aaron Eckhart
* Maggie Gyllenhaal
* Morgan Freeman
 
}}
| music                = {{Plain list|
* Hans Zimmer
* James Newton Howard
}}
| cinematography       = Wally Pfister Lee Smith
| production companies = {{Plain list|
* Legendary Pictures
* DC Comics Syncopy
}}
| distributor          = Warner Bros. Pictures
| released             =  
| runtime              = 152 minutes 
| country              = {{Plain list|
* United States   
* United Kingdom 
}}
| language             = English
| budget               = $185 million   
| gross                = $1.005 billion 
}}
 James Gordon The Joker" (Heath Ledger).
 toxic combination of prescription drugs, leading to intense attention from the press and movie-going public. Warner Bros. initially created a viral marketing campaign for The Dark Knight, developing promotional websites and trailers highlighting screenshots of Ledger as the Joker.
 film critics best superhero Best Sound Best Supporting Actor.  The Dark Knight Rises, the final film in the The Dark Knight Trilogy|trilogy, was released on July 20, 2012.

==Plot==

  The Joker and his accomplices rob a mob-owned bank. The accomplices kill each other off one by one in a sequence masterminded by the Joker, who escapes alone with all the money.
 Harvey Dent, in their plan to eradicate the mob. Although Dent is dating Rachel Dawes, Bruce Wayne is impressed with his idealism and offers to throw him a fundraiser. Mob bosses Sal Maroni, Gambol, and The Chechen hold a videoconference with Lau, a Chinese accountant who has hidden their funds and fled to Hong Kong. The Joker interrupts the meeting, warning that Batman is unhindered by jurisdiction. He offers to kill Batman for half their money, but the mob bosses refuse, and Gambol puts a bounty on him. The Joker kills Gambol and takes control of his men. Batman captures Lau and delivers him back to Gotham to testify against the mob.

The Joker announces that people will die each day unless Batman reveals his identity. He then kills Commissioner Gillian B. Loeb and the judge presiding over the mob trials. He also targets Dent at the fundraiser, but Bruce hides Dent. Gordon foils the Jokers assassination attempt on Mayor Garcia, apparently sacrificing himself in the process. Bruce plans to reveal his identity, but Dent instead names himself as Batman to protect the truth. Dent is taken into protective custody and pursued by the Joker across the city; Batman rushes to Dents aid. Gordon, who faked his death, helps apprehend the Joker and is promoted to Commissioner. 

That night, Dent and Rachel disappear. Batman interrogates the Joker and discovers that Dent and Rachel are held in two separate buildings filled with explosives. The Joker reveals their locations, and Batman goes to Rachels, only to realize that the Joker has tricked him into finding Dent moments before both buildings explode, killing Rachel and scarring half of Dents face. The Joker detonates a bomb in the police station and escapes with Lau.

Coleman Reese, an accountant at Wayne Enterprises, deduces Batmans true identity and plans to reveal it. The Joker kills Lau and The Chechen, then threatens to bomb a hospital unless Reese is killed. Gordon and Bruce protect Reese, who changes his mind. The Joker visits Dent in the hospital and convinces him to seek revenge. The Joker then blows up the hospital and escapes with hostages.

Dent starts to go after people responsible for Rachels death, deciding their fates by flipping a coin. He kills Maroni and a cop who had helped kidnap Rachel. It is revealed that the Joker rigged two ferries with explosives to escalate chaos; one ferry is full of citizens, the other full of prison inmates and guards. He then gives the passengers of each ferry the choice to blow the other up before midnight — otherwise, both ferries will explode. The passengers ultimately refuse, however.

Batman asks a reluctant Lucius Fox to use a city-wide tracking prototype device to find the Joker; Fox agrees, but says he will resign immediately afterward. The Joker dresses up hostages as his men, luring Gordons SWAT team to strike them. Batman fights off the SWAT team and the Jokers men, then rescues the hostages. Batman apprehends the Joker, but the Joker gloats that he has won, as Gotham will lose hope once Dents rampage becomes public. The SWAT team arrives to take the Joker into custody.

Dent lures Gordon to the building where Rachel died and holds Gordons family hostage. Batman confronts Dent, who judges the fates of himself, Batman, and Gordons son with three coin flips. He shoots Batman, spares himself, then flips again to determine the boys fate. Batman, who is wearing body armor, tackles Dent off the building, killing him and saving the boy.  Batman then convinces Gordon to frame him for the murders so that Dent will remain a symbol of hope for the city. Gordon destroys the Bat-Signal and launches a manhunt for the Batman.  Alfred Pennyworth burns a letter written by Rachel to Bruce announcing her engagement to Dent, and Fox watches the signal tracker self-destruct.

==Cast==
 , producers Emma Thomas and Charles Roven, actors Monique Gabriela Curnen, Michael Caine, Aaron Eckhart, Maggie Gyllenhaal and Christian Bale.]] Bruce Wayne / Batman:
:A billionaire dedicated to protecting Gotham City from the criminal underworld by night. Bale said he was confident in his choice to return in the role because of the positive response to his portrayal in Batman Begins.    He continued training in the Keysi Fighting Method and performed many of his own stunts,   but did not gain as much muscle as in the previous film because the new Batsuit allowed him to move with greater agility.    Bale described Batmans dilemma as whether "  something that has an end. Can he quit and have an ordinary life? The kind of manic intensity someone has to have to maintain the passion and the anger that they felt as a child, takes an effort after a while, to keep doing that. At some point, you have to exorcise your demons."    He added, "Now you have not just a young man in pain attempting to find some kind of an answer, you have somebody who actually has power, who is burdened by that power, and is having to recognize the difference between attaining that power and holding on to it."    Bale felt Batmans personality had been strongly established in the first film, so it was unlikely his character would be overshadowed by the villains, stating: "I have no problem with competing with someone else. And thats going to make a better movie."   

* Michael Caine as Alfred Pennyworth:
:Bruces trusted butler and confidant. His supply of useful advice to Bruce and his likeness as a father figure has led to him being labeled "Batmans batman (military)|batman".  

* Heath Ledger as the The Joker (The Dark Knight)|Joker: A Clockwork prescription drug overdose, leading to intense press attention and memorial tributes. "It was tremendously emotional, right when he passed, having to go back in and look at him every day  ," Nolan recalled. "But the truth is, I feel very lucky to have something productive to do, to have a performance that he was very, very proud of, and that he had entrusted to me to finish."  All of Ledgers scenes appear as he completed them in the filming; in editing the film, Nolan added no "digital effects" to alter Ledgers actual performance posthumously.    Nolan has dedicated the film in part to Ledgers memory.      
 James Gordon:
:A lieutenant in the   has a great, triangular relationship between Harvey Dent and Gordon and Batman, and thats something we very much drew from."    Oldman added that "Gordon has a great deal of admiration for him at the end, but   is more than ever now the dark knight, the outsider. Im intrigued now to see: If there is a third one, what hes going to do?"  On the possibility of another sequel, he said that "returning to   is not dependent on whether the role was bigger than the one before." 
 Harvey Dent / Two-Face: The Black Thank You for Smoking, and In the Company of Men. Whereas Two-Face is depicted as a crime boss in most characterizations, Nolan chose to portray him as a twisted vigilante to emphasize his role as Batmans counterpart. Eckhart explained, "  is still true to himself. Hes a crime fighter, hes not killing good people. Hes not a bad guy, not purely."   For Dent, Eckhart "kept on thinking about the Kennedy family|Kennedys," particularly Robert F. Kennedy, who was "idealistic, held a grudge and took on the Mob." He had his hair lightened and styled to make him appear more dashing. Nolan told Eckhart to not make Dents Two-Face persona "jokey with slurping sounds or ticks." 

* Maggie Gyllenhaal as Rachel Dawes: Mad Money with Diane Keaton and Queen Latifah.  By March 2007, Gyllenhaal was in "final talks" for the part.  Gyllenhaal has acknowledged her character is a damsel in distress to an extent, but says Nolan sought ways to empower her character, so "Rachels really clear about whats important to her and unwilling to compromise her morals, which made a nice change" from the many conflicted characters whom she has previously portrayed. 

* Morgan Freeman as Lucius Fox: armorer in addition to his corporate duties. 

* Eric Roberts as Sal Maroni:
:A gangster who has taken over Carmine Falcones mob. Bob Hoskins and James Gandolfini auditioned for the role. 
 Chin Han as Lau:
:The accountant who handles the money for the mob.

* Colin McFarlane as Gillian B. Loeb:
:The Police Commissioner of Gotham until his murder at the hands of the Joker. 
 Tom "Tiny" Lister, Jr. as a prison inmate on one of the bomb-rigged ferries. The films supporting villains include Michael Jai White and Ritchie Coster as mob bosses Gambol and The Chechen, respectively. William Fichtner played the Gotham National Bank manager. David Banner originally auditioned for the role of Gambol.    Cillian Murphy returns in a cameo as Scarecrow (DC comics)|Dr. Jonathan Crane / Scarecrow, who is apprehended early on in the film by Batman. 

Musician  . Leahy appears as a guest who defies the Joker when he and his henchmen attack Bruces fundraiser, saying "We are not intimidated by thugs."  Matt Skiba, lead singer of Chicago punk band Alkaline Trio, made a small appearance in the movie. 

==Production==

===Development===
 

Before the release of   as the major influence on his storyline.  According to veteran Batman artist  . #8. Avatar Press. pp. 57 - 63.  While initially uncertain of whether or not he would return to direct the sequel, Nolan did want to reinterpret the Joker on screen.  On July 31, 2006, Warner Bros. officially announced initiation of production for the sequel to Batman Begins titled The Dark Knight;    it is the first live-action Batman film without the word "Batman" in its title, which Bale noted as signaling that "this take on Batman of mine and Chris is very different from any of the others."   

After much research, Nolans brother and co-writer,   influenced a section of the Jokers dialogue in the film, in which he says that anyone can become like him given the right circumstances.    Nolan also cited Heat (1995 film)|Heat as "sort of an inspiration" for his aim "to tell a very large, city story or the story of a city": "If you want to take on Gotham, you want to give Gotham a kind of weight and breadth and depth in there. So you wind up dealing with the political figures, the media figures. Thats part of the whole fabric of how a city is bound together." 
 ending of Batman Begins, noting "things having to get worse before they get better."     While indicating The Dark Knight would continue the themes of Batman Begins, including justice vs. revenge and Bruce Waynes issues with his Thomas Wayne|father,    Nolan emphasized the sequel would also portray Wayne more as a detective, an aspect of his character not fully developed in Batman Begins.  Nolan described the friendly rivalry between Bruce Wayne and Harvey Dent as the "backbone" of the film.  He also chose to compress the overall storyline, allowing Dent to become Two-Face in The Dark Knight, thus giving the film an emotional arc the unsympathetic Joker could not offer.  Nolan acknowledged the title was not only a reference to Batman, but also the fallen "white knight" Harvey Dent.   

===Filming===
While scouting for shooting locations in October 2006, location manager Robin Higgs visited Liverpool, concentrating mainly along the citys waterfront. Other candidates included Yorkshire, Glasgow, and parts of London.  In August 2006, one of the films producers, Charles Roven, stated that its principal photography would begin in March 2007,    but filming was pushed back to April.     For its release in IMAX theaters, Nolan shot four major sequences in that format, including the Jokers opening bank robbery and the car chase midway through the film, which marked the first time that a feature film had been even partially shot in the format.  The cameras used for non-IMAX 35&nbsp;mm scenes were Panavision cameras|Panavisions Panaflex Millennium XL and Platinum. 
 edit some of the IMAX sequences using the original camera negative, which by eliminating generation loss, raised the film resolution of those sequences up to 18 thousand lines.    . 
  (far left) and actor Heath Ledger (in make-up) filming a scene in The Dark Knight with an IMAX camera]]
 Sears Tower, Trump International The Berghoff, Randolph Street Station, and Hotel 71. An old Brachs factory was used as Gotham Hospital. The defunct Van Buren Street post office doubles as Gotham National Bank for the opening bank robbery. Several sequences, including one car chase, were shot on the lower level of Wacker Drive.     The Marina City towers also appear in the background throughout the movie. 

Pinewood Studios, near London, was the primary studio space used for the production. 
While planning a stunt with the Batmobile in a special effects facility near Chertsey, England in September 2007, technician Conway Wickliffe was killed when his car crashed.  The film is dedicated to both Ledger and Wickliffe.  The restaurant scene was filmed at the Criterion Restaurant in Piccadilly Circus, London.  

The following month in London at the defunct Battersea Power Station, a rigged 200-foot fireball was filmed, reportedly for an opening sequence, prompting calls from local residents who feared a terrorist attack on the station.  A similar incident occurred during the filming in Chicago, when an abandoned Brachs candy factory (which was Gotham Hospital in the film) was demolished. 

Filming took place in Hong Kong from November 6 to 11, 2007, at various locations in Central, Hong Kong|Central, including Hong Kongs tallest building at the time, the International Finance Centre, for the scene where Batman captures Lau.         The shoot hired helicopters and C-130 Hercules|C-130 aircraft.  Officials expressed concern over possible noise pollution and traffic.  In response, letters sent to the citys residents promised that the sound level would approximate noise decibels made by buses.  Environmentalists also criticized the filmmakers request to tenants of the waterfront skyscrapers to keep their lights on all night to enhance the cinematography, describing it as a waste of energy.  Cinematographer Wally Pfister found the city officials a "nightmare," and ultimately Nolan had to create Batmans jump from a skyscraper digitally. 

===Design===
 Francis Bacon countercultural popular pop culture Johnny Rotten. prosthetics usually requires. Ledger also said that he felt he was barely wearing any make-up.    

Hemming and Ledgers Joker design has had an impact in popular and political culture in the form of the Barack Obama "Joker" poster, and has since become a meme in its own right. 
 gauntlets have retractable razors which can be fired.  Though the new costume is eight pounds heavier, Bale found it more comfortable and not as hot to wear.  The depiction of Gotham City is less gritty than in Batman Begins. "Ive tried to unclutter the Gotham we created on the last film," said production designer Nathan Crowley. "Gotham is in chaos. We keep blowing up stuff, so we can keep our images clean." 

===Effects===
  Tumbler for Batman Begins, designed six models (built by special effects supervisor Chris Corbould) for use in the films production, because of necessary crash scenes and possible accidents.  Crowley built a prototype in Nolans garage, before six months of safety tests were conducted.  The Batpod is steered by shoulder instead of hand, and the riders arms are protected by sleeve-like shields. The bike has 508-millimeter (20-inch) front and rear tires, and is made to appear as if it is armed with grappling hooks, cannons, and machine guns. The engines are located in the hubs of the wheels, which are set 3 &nbsp;feet (1067&nbsp;mm) apart on either side of the tank. The rider lies belly down on the tank, which can move up and down to dodge any incoming gunfire that Batman may encounter. Stuntman Jean-Pierre Goy doubled for Christian Bale during the riding sequences in The Dark Knight.    The Batpod was highly unstable for riding, and Goy was the only stuntman who could manage to balance the bike, even commenting that he had to "nearly un-learn how to ride a motorcycle" to manage riding the Batpod. Bale did insist on doing shots on the Batpod himself, but was prohibited by the team fearing his safety.   
 Pirates of the Caribbean&nbsp;– something like that, theres something about a very fanciful, very detailed visual effect, that I think is more powerful and less repulsive."   Framestore created 120 computer-generated shots of Two-Faces scarred visage. Nolan felt using make-up would look unrealistic, as it adds to the face, unlike real burn victims. Framestore acknowledged they rearranged the positions of bones, muscles and joints to make the character look more dramatic. For each shot, three 720-pixel HD cameras were set up at different angles on set to fully capture Aaron Eckharts performance. Eckhart wore markers on his face and a prosthetic skullcap, which acted as a lighting reference. A few shots of the skullcap were kept in the film. Framestore also integrated shots of Bale and Eckhart into that of the exploding building where Dent is burned. It was difficult simulating fire on Eckhart because it is inherently unrealistic for only half of something to burn.   

===Music===
  suite for The Damned.    When Ledger died, Zimmer felt like scrapping and composing a new theme, but decided that he could not be sentimental and compromise the "evil   projects."    Howard composed Dents "elegant and beautiful" themes,  which are brass instrument|brass-focused. 

==Marketing==
 
  message "The only sensible way to live in this world is without rules," to send in photographs of these letters, and then featured their photos in a collage.]]

In May 2007, 42 Entertainment began a viral marketing campaign utilizing the films "Why So Serious?" tagline with the launch of a website featuring the fictional political campaign of Harvey Dent, with the caption, "I Believe in Harvey Dent."   This is an updated version of that website.  The site aimed to interest fans by having them try to earn what they wanted to see and, on behalf of Warner Bros., 42 Entertainment also established a "vandalism|vandalized" version of I Believe in Harvey Dent, called "I believe in Harvey Dent too," where e-mails sent by fans slowly removed pixels, revealing the first official image of the Joker; it was ultimately replaced with many "Haha"s and a hidden message that said "see you in December." 

During the 2007 San Diego Comic-Con International, 42 Entertainment launched WhySoSerious.com, sending fans on a scavenger hunt to unlock a teaser trailer and a new photo of the Joker.  On October 31, 2007, the films website morphed into another scavenger hunt with hidden messages, instructing fans to uncover clues at certain locations in major cities throughout the United States, and to take photographs of their discoveries. The clues combined to reveal a new photograph of the Joker and an audio clip of him from the film saying "And tonight, youre gonna break your one rule." Completing the scavenger hunt also led to another website called Rorys Death Kiss    (referencing the false working title of Rorys First Kiss), where fans could submit photographs of themselves costumed as the Joker. Those who sent photos were mailed a copy of a fictional newspaper called The Gotham Times, whose electronic version led to the discovery of numerous other websites.    

  Formula One racing car featuring the Batman insignia, at the 2008 British Grand Prix]] Jordan Goldberg, and Alan Burnett, presents its own distinctive artistic style, paralleling numerous artists collaborating in the same DC Universe. 

  for The Dark Knight, seen in Paris Rue Saint-Honoré in August 2008.]]
After the death of Heath Ledger on January 22, 2008, Warner Bros. adjusted its promotional focus on the Joker,    revising some of its websites dedicated to promoting the film and posting a memorial tribute to Ledger on the films official website  and overlaying a black memorial ribbon on the photo collage in WhySoSerious.com.    On February 29, 2008, I Believe in Harvey Dent was updated to enable fans to send their e-mail addresses and phone numbers.  In March 2008, Harvey Dents fictional campaign informed fans that actual campaign buses nicknamed "Dentmobiles" would tour various cities to promote Dents candidacy for district attorney. 
 The Dark stalked by role play UNO card game, which began commercial distribution in June 2008.   (Source: Warner Bros. Consumer Products.) 
 BitTorrent search engine The Pirate Bay taunted the movie industry over its ability to provide the movie free, replacing its logo with a taunting message. 
 

==Release==
Warner Bros. held the world premiere for The Dark Knight in New York City on July 14, 2008, screening in an IMAX theater with the films composers James Newton Howard and Hans Zimmer playing a part of the film score live.    Leading up to  The Dark Knight s commercial release, the film had drawn "overwhelmingly positive early reviews and buzz on Heath Ledgers turn as the Joker."  The Dark Knight was commercially released on July 16, 2008 in Australia, grossing almost $2.3&nbsp;million in its first day.     

In the United States and Canada, The Dark Knight was distributed to 4,366 theaters, breaking the previous record for the highest number of theaters held by   in 2007. The number of theaters also included 94 IMAX theaters, with the film estimated to be played on 9,200 screens in the United States and Canada.  Online, ticketing services sold enormous numbers of tickets for approximately 3,000 midnight showtimes as well as unusually early showtimes for the films opening day. All IMAX theaters showing The Dark Knight were sold out for the opening weekend. 

===Reception===
 
 
 average score normalized rating in the 0–100 range based on reviews from top mainstream critics, calculated an average score of 82, based on 39 reviews.    CinemaScore polls reported that the average grade cinemagoers gave the film was "A" on an A+ to F scale, and that audiences skewed slightly male and older. 
 universe that has something "raw and elemental" at work within it. In particular, he cites Nolans action choreography in the IMAX-tailored heist sequence as rivaling that of Heat (1995 film)|Heat (1995).  Manohla Dargis of The New York Times wrote "Pitched at the divide between art and industry, poetry and entertainment, it goes darker and deeper than any Hollywood movie of its comic-book kind."    Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Every great hero needs a great villain. And in 2008, Christian Bales Batman found his in Heath Ledgers demented dervish, the Joker."  BBC critic Mark Kermode, in a positive review, said that Ledger is "very, very good" but that Oldmans turn is "the best performance in the film, by a mile". 
 David Denby of The New Yorker said that the story is not coherent enough to properly flesh out the disparities. He said the films mood is one of "constant climax," and that it feels rushed and far too long. Denby criticized scenes which he argued to be meaningless or are cut short just as they become interesting.   (Postdated)  Denby remarks that the central conflict is workable, but that "only half the team can act it," saying that Bales "placid" Bruce Wayne and "dogged but uninteresting" Batman is constantly upstaged by Ledgers "sinister and frightening" performance, which he says is the films one element of success. Denby concludes that Ledger is "mesmerising" in every scene.  The vocalization of Christian Bales Batman (which was partly altered during post-production) was the subject of particular criticism by some commentators, with David Edelstein from NPR describing Bale delivering his performance with "a voice thats deeper and hammier than ever". Alonso Duralde at MSNBC, however, referred to Bales voice in The Dark Knight as an "eerie rasp", as opposed to the voice used in the Batman Begins, which according to Duralde "sounded absurdly deep, like a 10-year-old putting on an ‘adult’ voice to make prank phone calls".  
 Empire s  2008 list of the "500 Greatest Movies of All Time," based upon the weighted votes of 10,000 readers, 150 film directors, and 50 key film critics.  Heath Ledgers interpretation of the Joker was also ranked number three on  Empire s 2008 list of the "100 Greatest Movie Characters of All Time."  In June 2010, the Joker was ranked number five on Entertainment Weekly  "100 Greatest Characters of the Last 20 Years".  Paste (magazine)|Paste magazine named it one of the 50 Best Movies of the Decade (2000–2009), ranking it at number 11.  The Dark Knight was included in American Cinematographers "Best-Shot Film of 1998-2008" list, ranking in the top 10. More than 17,000 people around the world participated in the final vote.  In March 2011, the film was voted by BBC Radio 1 and BBC Radio 1Xtra listeners as their eight favorite film of all time.  In 2012, Total Film ranked The Dark Knight as the sixth most accomplished film of the past 15 years, writing that "Christopher Nolans psycho-operatic crime drama was its decades most exciting blockbuster – and its most challenging."  In 2014, The Dark Knight was ranked the 3rd greatest film ever made on Empire s list of "The 301 Greatest Movies Of All Time" as voted by the magazines readers. 

====Commentary==== War on terror and civil rights to deal with an emergency, certain that he will re-establish those boundaries when the emergency is past."  Klavans article has received criticism on the Internet and in mainstream media outlets, such as in The New Republic s  "The Plank."  Reviewing the film in The Sunday Times, Cosmo Landesman reached the opposite conclusion to Klavan, arguing that The Dark Knight "offers up a lot of moralistic waffle about how we must hug a terrorist&nbsp;– okay, I exaggerate. At its heart, however, is a long and tedious discussion about how individuals and society must never abandon the rule of law in struggling against the forces of lawlessness. In fighting monsters, we must be careful not to become monsters&nbsp;– that sort of thing. The film champions the anti-war coalitions claim that, in having a war on terror, you create the conditions for more terror. We are shown that innocent people died because of Batman&nbsp;– and he falls for it."  Benjamin Kerstein, writing in Azure, says that both Klavan and Landesman "have a point," because "The Dark Knight is a perfect mirror of the society which is watching it: a society so divided on the issues of terror and how to fight it that, for the first time in decades, an American mainstream no longer exists." 

====Themes and analysis==== ethical decisions. By the end, the whole moral foundation of the Batman legend is threatened." 

Other critics have mentioned the theme of the triumph of evil over good. Harvey Dent is seen as Gothams "White Knight" in the beginning of the film but ends up becoming seduced to evil.  The Joker, on the other hand, is seen as the representation of anarchy and chaos. He has no motive, no orders, and no desires but to cause havoc and "watch the world burn." The terrible logic of human error is another theme as well. The ferry scene displays how humans can easily be enticed by iniquity, and how that could lead to potential disaster. 

====Awards====
 

 
 Screen Actors Best Adapted Best Film at the Critics Choice Awards and was named one of the top ten films of 2008 by the American Film Institute.
 Best Sound Best Cinematography, Best Sound Best Visual Best Makeup, Best Film Best Picture winner, Slumdog Millionaire. Although it did not receive a Best Picture nomination, the shows opening song paid homage to The Dark Knight along with the five Best Picture nominees, including host Hugh Jackman riding on a mockup of the Batpod made out of garbage. In spite of the films critical success, the film was noticeably absent from the Best Picture nominee list, prompting controversy and led many to criticize the Academy Awards for "snubbing" the film.   There was speculation that the Academy of Motion Picture Arts and Sciences later changed their number of Best Picture nominees to ten, instead of the traditional five, because of the films omission. In a question-and-answer session that followed the announcement, the Academys then president Sidney Ganis said; "I would not be telling you the truth if I said the words Dark Knight did not come up." 
 Goya Award Best European Film.  It had a nomination in Japan for the 2009 Seiun Awards under the Science Fiction category  with a Japan Academy Prize Award for Best Foreign Film. 

===Box office===
  cinema in Barcelona, Spain]]
The Dark Knight earned $534.9&nbsp;million in North America and $469.7&nbsp;million in other territories for a worldwide total of $1&nbsp;billion. Worldwide, it is the List of highest-grossing films|eighteenth-highest-grossing film, the 2008 in film|highest-grossing film of 2008 and the fourth film in history to gross more than $1 billion. It made $199.7&nbsp;million on its worldwide opening weekend which ranks thirty-fourth on the all-time chart. 

In order to increase the films chances of crossing $1&nbsp;billion in worldwide gross and of winning Oscars, Warner Bros. re-released the film in traditional and IMAX theaters in the United States and other countries on January 23, 2009.   Before the re-release, the films gross remained at $997 million,  but following the re-release, the film crossed the $1-billion-mark in February 2009. 

;North America Star Trek).  It achieved the largest Sunday gross, with $43.6&nbsp;million,  and the largest opening week from Friday to Thursday, with $238.6&nbsp;million (both records surpassed by The Avengers (2012 film)|Marvels The Avengers).   It also achieved the largest cumulative gross through its third and fourth day of release (both records first surpassed by Deathly Hallows – Part 2), and so on until its tenth day of release (records surpassed by Marvels The Avengers).  Moreover, it was the fastest film to reach $100 million (a record first surpassed by New Moon), $150 million and each additional $50 million through $450 million (records surpassed by Marvels The Avengers), and $500 million (a record first surpassed by Avatar (2009 film)|Avatar).  Finally, it achieved the largest second-weekend gross (a record first surpassed by Avatar). 

It has grossed the fourth largest Saturday gross ($51,336,732). On its first Monday, it grossed $24.5 million, which stands as the largest non-holiday Monday gross and the 4th largest Monday gross overall, and on its first Tuesday it grossed another $20.9 million, which stands as the largest non-opening Tuesday gross and the second largest Tuesday gross overall.    Notably, it topped the box office during the second biggest weekend of all time in North America (aggregated total of $253,586,871)  and it was the only 2008 film that remained on top of the box office charts for four consecutive weekends. 

The Dark Knight is the highest-grossing 2008 film, the second-highest-grossing superhero film, the second-highest-grossing film based on comics and the fourth highest-grossing film of all time in North America. Adjusted for ticket-price inflation though, it ranks 28th.  In contrast to Avatar and Titanic, both which grossed more than The Dark Knight in North America and had slow but steady earnings, The Dark Knight broke records in its opening weekend and slowed down significantly after its first few weekends.  

;Markets outside North America
Overseas, The Dark Knight is the highest-grossing 2008 film  and the fourth-highest-grossing superhero film. It premiered in 20 other territories on 4,520 screens, grossing $41.3&nbsp;million in its first weekend.      The film came in second to Hancock (film)|Hancock, which was in its third weekend, screening in 71 territories.  The Dark Knight s biggest territory was Australia, where it grossed $13.7&nbsp;million over the weekend, setting a record for the largest superhero film opening.  It topped the weekend box office outside North America three consecutive times and four in total. Citing cultural sensitivities to some elements in the film, and a reluctance to adhere to pre-release conditions, Warner Bros. declined to release the film in mainland China.  Its highest-grossing market after North America was the UK, Ireland and Malta, where it earned $89.1 million. Also, in Australia, it earned of $39.9 million, still remaining in the all-time top 10 of the country.  The five highest-grossing markets outside North America also include Germany ($29.7 million), France and the Maghreb region ($27.5 million) and South Korea ($25.0 million). 

===Home media===
The film was released on DVD and Blu-ray Disc in North America on December 9, 2008. Releases include a one-disc edition DVD; a two-disc Special Edition DVD; a two-disc edition BD; and a Special Edition BD package featuring a statuette of the Bat-pod.  The BD/iTunes version presents the film in a variable aspect ratio, with the IMAX sequences framed in 1.78:1, while scenes filmed in 35 mm film|35&nbsp;mm are framed in 2.40:1.  The DVD versions feature the entire film framed in a uniform 2.40:1 aspect ratio. Disc 2 of the two-disc Special Edition DVD features the six main IMAX sequences in the original 1.44:1 aspect ratio. Additional IMAX shots throughout the film that are presented in 1.78:1 on the Blu-ray release are not, however, included in the DVDs special features. In addition to the standard DVD releases, some stores released their own exclusive editions of the film.

In the United Kingdom, the film had combined sales of 513,000 units on its first day of release, of which 107,730 (21%) were Blu-ray Discs, the highest number of first-day Blu-ray Discs sold.  In the United States, The Dark Knight set a sales record for most DVDs sold in one day, selling 3 million units on its first day of release – 600,000 of which were Blu-ray Discs. 

The DVD and Blu-ray Disc editions were released in Australia on December 10, 2008. Releases were in the form of a one-disc edition on DVD; a two-disc edition on DVD; a two-disc edition including a Batmask on DVD and BD; a two-disc Batpod statuette Limited BD Edition; a two-disc BD edition; and a four-disc Batman Begins/The Dark Knight pack on DVD and BD. As of December 19, 2008, the DVD release is the top selling film in the Australian DVD Charts  and is expected to break the Australian sales record set by Finding Nemo.  

The movie also sold Blu-ray copies worth 370 million yen (US$4.1&nbsp;million) in Japan, placing it 3rd out of 10 in the top 10 overall Blu-ray category.  

In March 2011, Warner Bros. offered The Dark Knight for rent on Facebook, becoming the first movie ever to be released via digital distribution on a social networking site. Users in the United States are able to use Facebook Credits to view the film. 

==See also==
 
 
*Vigilante film
*List of films featuring surveillance
 

==References==
 

==Further reading==
*  
*  
*  

==External links==
 
 
* 
* 
* 
* 
* 
* 
* 
*  at HD Report
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 