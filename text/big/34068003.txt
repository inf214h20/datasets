Hot Tamale
 
{{Infobox film
| name           = Hot Tamale
| image          = Hot Tamale Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Damian
| producer       =  
| writer         =  
| starring       =  
| music          = Mark Thomas
| cinematography = Frederick Iannone
| editing        = Adam Heller
| studio         = Riviera Films
| distributor    = Motion Picture Corporation of America
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $500,000
}}
 directorial debut.

==Plot==
Harlan Woodruff (Randy Spelling) is a Salsa musician from Wyoming, on a road trip to Los Angeles to realise his dream of making it big as a percussionist. It is revealed that Harlan had a troubled childhood, having found his dead father frozen in a fishing hole. En route to Los Angeles, he runs into Jude, a career criminal on the run from two hit men, Al and Dwayne, who want to retrieve stolen merchandise from him. Desperate, Jude dumps the merchandise into Harlans bag and escapes.

Harlan mean while stays over at his Puerto Rican friend Carlos apartment. He meets Tuesday (Diora Baird), a friend of Carlos there. Carlos leaves town on an assignment and Tuesday and Harlan have the apartment to themselves. Harlan discovery that Carlo is a marijuana cultivator, growing the plants at his house. When trying to smoke the substance, he has a panic attack and lands in the hospital. After making a recovery, Tuesday brings him back to the apartment, where they sleep in. Meanwhile, Al and Dwayne catch up with Jude, who spills his guts that the bag the hit men are looking for is with Harlan. Judes partner Riley (Carmen Electra) is also hot on Harlans trail.

The hitmen and Riley have their own demands and want the stolen merchandise, which turns out to be diamonds. A shoot out ensues and finally the hitmen are nabbed and Harlan walks away from the fire fight unharmed with Tuesday.

==Cast==
*Randy Spelling as Harlan Woodriff, the naive salsa musician from Wyoming.
*Diora Baird as Tuesday Blackwell, the damsel in distress.
*Carmen Electra as Riley, a con woman and thief.
*Jason Priestley as Jude, Rileys parter in crime. Mike Starr as Al

==Reception==
===Critical Response===
The movie was mostly panned by critics, with a few exceptions.  Positive takeaways were performances of Randy Spelling and the debut direction effort of Michael Damian.

==Awards==
The film won the Boston International Film Festival Best Narrative feature Award for Michael Damian and Janeen Damian. It also won the Festival Prize at the Dixie Film Festival. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 