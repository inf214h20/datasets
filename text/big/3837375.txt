World Trade Center (film)
 
{{Infobox film
| name           = World Trade Center
| image          = Worldtrade.jpg
| caption        = Theatrical release poster 
| alt            = A silhouette of the Twin Tower buildings of the World Trade Center
| producer       = Moritz Borman  Debra Hill  Michael Shamberg  Stacey Sher
| director       = Oliver Stone
| writer         = Andrea Berloff
| starring       = Nicolas Cage Maria Bello Michael Peña Maggie Gyllenhaal Stephen Dorff Jon Bernthal Jay Hernandez Michael Shannon Donna Murphy Frank Whaley Jude Ciccolella Danny Nucci Craig Armstrong
| cinematography = Seamus McGarvey  David Brenner  Julie Monroe 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 129 minutes
| country        = United States United Kingdom Germany
| language       = English
| budget         = $65 million
| gross          = $162,970,275
}} disaster drama film directed by Oliver Stone and based on the September 11 attacks at the World Trade Center. It stars Nicolas Cage, Maria Bello, Michael Peña, Maggie Gyllenhaal, Stephen Dorff and Michael Shannon. The film was shot between October 19, 2005 and February 10, 2006 and released on August 9, 2006.

==Plot== Port Authority John McLoughlin has been Metropolitan Transit has also Building 5 and enter the concourse between the towers.
 attack on collapsing onto them and that their only chance of survival is to run into the service elevator shaft. Amoroso trips and does not have time to get up. Rodrigues is unable to get to the shaft in time. McLoughlin, Jimeno and Pezzulo manage to escape the huge amounts of dust and rubble flying down from the South Tower. However, as the rubble continues to crush the elevator shaft, the three are trapped. As the cascade of debris subsides, Pezzulo realizes he can free himself and manages to move nearer to Jimeno who, along with McLoughlin, is pinned under rubble and cannot move. Pezzulo tries but fails to shift the debris covering Jimenos legs and is ordered by McLoughlin not to leave.

As Pezzulo becomes optimistic that they will live, the rumbling begins again as the North Tower starts to collapse. Although Jimeno and McLoughlin are not further harmed, Pezzulo is fatally injured by shifting rubble. After he fires a gun through a gap in the rubble to try to alert rescuers to their position, he dies. Jimeno and McLoughlin spend hours under the rubble, in pain but exchanging stories about their lives and families. McLoughlin is particularly anxious to keep Jimeno from falling asleep and Jimeno also realizes that by straining to grab a metal bar above his body, he can make a noise that rescuers might hear. Two United States Marines, Dave Karnes and Jason Thomas, who are searching for survivors, do hear it and find the men, calling for help to dig them out. Jimeno is rescued first, and then hours later McLoughlin is lifted out of the debris, barely alive and in critical condition. They are then both reunited with their distraught families at the hospital. Two years after the attacks, McLoughlin and Jimeno attend a barbecue with their families: McLoughlins wife Donna, Jimenos wife Allison, daughter Bianca, and their newest addition Olivia.

The epilogue states that John and Will were two of the 20 people pulled out alive and are now retired from active duty.
Dave Karnes re-enlisted in the Marines.

==Cast== John McLoughlin
* Maria Bello as Donna McLoughlin
* Michael Peña as Will Jimeno
* Maggie Gyllenhaal as Allison Jimeno ESU Rescueman Scott Strauss
* Jon Bernthal as Officer Christopher Amoroso
* Jay Hernandez as Officer Dominick Pezzulo
* Michael Shannon as Marine Sgt. Dave Karnes
* Donna Murphy as Judy Jonas
* Frank Whaley as Paramedic Chuck Sereika
* Jude Ciccolella as Inspector Fields
* Danny Nucci as Officer Giraldi
* William Mapother as Marine Sgt. Jason Thomas
* Wass Stevens as Officer Pat McLoughlin
* Armando Riesco as Officer Antonio Rodrigues
* Nicholas Turturro as Officer Colovito
* Ned Eisenberg as Officer Polnicki
* Dara Coleman as Officer Boel
* Nick Damici as Lt. Kassimatis
* Arthur Nascarella as FDNY Chief at Ground Zero
* Patti DArbanville as Donnas Friend
* Viola Davis as Mother in Hospital
* Will Jimeno as Port Authority Police Officer
* Aimee Mullins as Reporter

==Production== Port Authority police officers who are played by Cage and Peña, John McLoughlin and Will Jimeno, and their wives, played by Bello and Gyllenhaal, were involved with the writing of the screenplay and the production of the film. McLoughlin and Jimeno wanted to have a movie made to honor their rescuers and comrades who died on September 11, not for personal gain.
 John McLoughlin and Will Jimeno appear at the end of the film during the barbecue scene.

The real ESU (Emergency Services Unit) police from New York who are depicted in the film—Scott Strauss and Paddy McGee—were on set as technical advisers. In addition, the firemen in the film were played by real FDNY members who served on 9/11. All of them enthusiastically supported the film and its intention to accurately portray the rescue of McLoughlin and Jimeno.

Jeanette Pezzulo, the widow of   resident Jamie Amoroso, whose husband also died during the rescue operation, has also expressed her anger over the film and said she did "not need a movie" to tell her "what a hero" her husband was. 
 

There were initial concerns this film would examine 9/11 conspiracy theories because director Oliver Stone is known for examining similar theories in his films (JFK (film)|JFK in particular), and some 9/11 conspiracy websites are promoting the idea that the film does contain hints of the conspiracy.  Stone and the producers, and the real McLouglin and Jimeno, however, have said the film is a simple dedication to the heroism and sadness of the day with little-to-no political themes.   

The film has been accused of not providing a fair portrayal of the character and motives of rescuer Dave Karnes and paramedic Chuck Sereika. They did not participate in the making of the film and felt their roles of being the first rescuers to reach the trapped men did not receive enough screen time. Sereika began treating and extricating Jimeno a full 20 minutes before officers from the New York City Police Departments Emergency Services Unit arrived.   

==Reception==

===Box office===
On opening weekend it made approximately $18,730,762 in the U.S. and Canada. In total, the film grossed $70,278,893 at the North American box office, and over $162,000,000 worldwide. {{cite web 
| url=http://boxofficemojo.com/movies/?id=wtc.htm 
| title=World Trade Center (2006) 
| work=Boxofficemojo.com 
| publisher = Amazon
| accessdate=October 9, 2011 
}} 
 

===Critical response  ===
The film received generally positive reviews from critics, with a 68% "Fresh" approval rating on Rotten Tomatoes  and a score of 66/100 at Metacritic. 
 NYPD and FDNY were very pleased with it. Former mayor Rudy Giuliani, former Governor George Pataki and then-Fire Commissioner Nicholas Scopetta, as well as representatives from the NY Port Authority, were at the premiere of the film at the Ziegfeld Theatre in Manhattan.

==Media releases== Target stores. Although Paramount initially dropped its support of the Blu-ray format, it came to support Blu-ray again after HD DVDs demise. The film re-appeared on Blu-ray in May 2008.

==See also==
* List of cultural references to the September 11 attacks
* World Trade Center
* September 11 attacks
* Collapse of the World Trade Center
* Will Jimeno
* Dave Karnes John McLoughlin
* Dominick Pezzulo
* Jason Thomas
* United 93 (film)|United 93
* List of firefighting films
 

==Notes==
 

==References==
*  
*  
*  
*  
*  
*  
*  

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 