House of Angels – The Second Summer
{{Infobox Film
| name           = House of Angels – The Second Summer
| image          = Änglagård - andra sommaren.jpg
| image_size     = 
| caption        = Promotional movie poster
| director       = Colin Nutley
| producer       = Colin Nutley
| writer         = Colin Nutley
| narrator       = 
| starring       = Helena Bergström Rikard Wolff Ernst Günther
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 25 December 1994
| runtime        = 138 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} 1994 Swedish drama by Colin Nutley starring Helena Bergström, Rikard Wolff, Ron Dean, and Ernst Günther.  It is the second Swedish drama film about the mansion Änglagård (Angel Farm) located in a small isolated hamlet in Västergötland, Sweden. The film premiered on 25 December 1994. In both the films Änglagård and Änglagård - andra sommaren Sven Wollter (Axel Flogfält) and Viveka Seldahl (Rut Flogfält) acting as a married couple which they also was in real life from 1971 until Seldahl died 2001. They have also acted as married couple in the 2001 Swedish film En sång för Martin.

==Plot==
The film starts one year after Fanny Zander and Zac have returned to the village to find that the mansion Änglagård (film)|Änglagård (Angel Farm) has accidentally burnt to the ground. Fanny and Zac find themselves staying with the brothers Gottfrid and Ivar Pettersson.

The small town mentality intrigues and conflicts continue in the hamlet. The richest man in the village, Axel Flogfält, has promised to rebuild the mansion, but he is also interested in buying the mansion from Fanny. Axels wife, Rut, has a relationship with the village priest, Henning. Her son, Mårten Flogfält, is increasingly interested in Fanny, and tries by all means to seduce her. Mårten still does not know the truth about the identity of Fannys father.

It also emerges that Gottfrid and Ivar have a nearly forgotten brother, Sven, who emigrated to the USA when he was young.  Fanny and Zac convince the brothers that they all shall go and visit their brother.

==Cast ==
*Helena Bergström as Fanny Zander
*Rikard Wolff as Zac
*Ernst Günther as Gottfrid Pettersson
*Tord Peterson as Ivar Pettersson
*Sven Wollter as Axel Flogfält
*Viveka Seldahl as Rut Flogfält
*Ron Dean as Sven Pettersson
*Reine Brynolfsson as Henning Collmer
*Jan Mybrand as Per-Ove Ågren
*Jakob Eklund as Mårten Flogfält
*Ing-Marie Carlsson as Eva Ågren Peter Andersson as Ragnar Zetterberg

==Production==
The film was filmed in Kölingared, Lönnarp and Ulricehamn in Sweden, and in New York City in the USA.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 

 