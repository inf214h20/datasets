Chidambaram (film)
 

{{Infobox film
| name           = Chidambaram
| image          = Chidambaram film.jpg
| image_size     =
| caption        = Promotional poster
| director       = G. Aravindan
| producer       = G. Aravindan
| screenplay     = G. Aravindan
| story          = C. V. Sreeraman
| narrator       = Sreenivasan Mohan Das
| music          = G. Devarajan
| cinematography = Shaji N. Karun
| editing        =
| studio         = Sooryakanthi
| distributor    = Saj Movies
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Sreenivasan and Best Film Best Direction.

==Plot==
 
Shankaran (Bharath Gopi) works as the Office Superintendent in a vast government farm situated in the hilly areas on the border of Kerala and Tamil Nadu. His colleague Jacob (Mohan Das) is the Field Supervisor and a work addict. The two men are poles apart in their characters. Jacob is a down-to-earth and rather commonplace sort of person, whose social pride does not conflict with his easy-going attitude to morality. Shankaran, on the other hand, is a mild-mannered loner. He has an easy, friendly relationship with the workers. Muniyandi (Sreenivasan (actor)|Sreenivasan), a worker in the farm, is a timid god-fearing man. His job is to look after the cattle. One day he confesses to Shankaran that he is going to his village to get married. When he asks Jacob for leave, Jacob brusquely tells him that he must be back immediately after the wedding. But Shankaran invites him to have a drink with him in his room. Hoping that God will not notice this minor aberration. Muniyandi says a quick prayer before taking a few swigs of rum equally quickly. Then he settles down on the floor and starts singing in the praise of the Lord. The embarrassed Shankaran has to help him to his quarters.

Shankaran who is an amateur photographer, goes to attend Muniyandis marriage in a Tamil village. On the way he focuses his camera on the clay horses standing in a row, neglected in the village green. At the wedding he photographs the bride and the groom. Soon after the wedding, Muniyandi brings his wife, Shivkami (Smita Patil) from the brown, barren landscape of Tamil Nadu to the green, undulating meadows of the farm. Shankaran watches them from his office window, as they walk towards their quarters, Shivkami stopping on the way to wonder at the colourful surroundings.

Shivkami reacts at first like a frightened deer to every new sound, every new scene. The massive, well fed cows, the sound of the motorbike, a new face, everything makes her withdraw into herself. It takes time, but slowly and timidly, she starts taking her first sure steps on this new lovely world. She walks around aimlessly when Muniyandi is away at work and wanders into the gardens, touching each flower with supreme wonder. While Muniyandi offers her a timid adoration, Shankaran approaches her with gentle concern. She is no longer afraid of him. She goes to him when an address has to be written on a letter she writes home, and does not shy away from his camera. For Shankaran, she is something gentle and beautiful, like the lush green landscape of the farm.

One day a car stops on the road below Shankarans house. Two younger colleagues bring a couple of minor film stars with them to meet Shankaran. They are on their way to a function where the actors will be their guests. Out of the sprawling lawns, Shankaran and his visitors sit down for a casual drink. On the road below, Jacob appears and is hailed by Shankaran. He comes and joins them, and the conversation turns to the newcomer in the farm, the lovely Shivkami. Jacob teases Shankaran about his interest in her. And suddenly Shankaran is furiously angry and physically assaults Jacob, and they all leave him, embarrassed and upset. In the gathering darkness, Shankaran lies on the grass, pondering on his strange reaction to a casual joke. What does he want from Shivkami?

Meanwhile Jacob has been trying to arrange a job for Shivkami on the farm. Muniyandi is not happy about it at all. But Shivkami herself, now bored with her lonely life, would not mimd working on the farm. Seeing Muniyandi with the cattle, Jacob stops by to tell him that he has found some work for Muniyandis wife. Muniyandi, who is suspicious about Jacobs intentions, politely refuses the job. Jacob, who would not normally associate with what he calls "menials", is angry, and orders Muniyandi to start doing night duty from now on.

At night, in the cattle shed, Muniyandi listens to every sound. A motorbike rushes past, and he is instantly alert. Is that Jacob going to his Shivkami? He comes out of the shed and goes running all way home. His house is quiet and dark. But Muniyandi, in a frantic fear, bangs his fist on the door. And a figure goes past swiftly from the back of the house and disappears into the darkness. It was not Jacob, but Shankaran, the man Muniyandi had trusted. 

Early next morning a crowd gathers outside the cattle shed. Through the half open shutters of the windows placed high on the wall, the workers of the farm peer into the inner gloom, where in a shaft of light, the dark, portly figure of Muniyandi hangs from the wooden beam. His lifeless body sways gently to and fro, the beam making a creaking noise. Shankaran lifts one of the shutters from outside and Muniyandis dead face confronts him with his own shame.

Shankaran runs away. He runs through the forest, pursued by the devil within, till the night gathers, and he falls down exhausted on the cushioned floor of the forest. At night, his two younger colleagues are making ready for bed in their quarters, when there is a frantic knock on the door. It is Shankaran, come back for penance. The men do not understand his state of mental and physical exhaustion. They put him to bed in one of their room, and he sleeps like the dead, escaping the horrors of the day for a few hours.

Guilt changes Shankarans life. For a while he leaves his job and wanders in the city, rootless and alone. He becomes an alcoholic, going listlessly from one liquor den to another. His friends find him a job in a printing press in the city, where he sits dreaming over the proof sheets. But his life has stopped with Muniyandi. The swinging body and the creaking beam have kept him company ever since. The doctor he visits once in a while, never stops talking of religion. "Read the Gita", he says. But the devil within will not be subdued. "Take a holiday", says the doctor. "Go to a religious place, youll find peace of mind".

Shankaran goes wandering again. He goes to Chidambaram temple, built on the spot were Lord Siva was supposed to have been, transformed from the primordial phallus to the Nataraja, the cosmic dancer who liberates the human soul from its earthly shell. Coming out of the inner precincts of the temple, Shankaran stops to wear his shoes and pay the woman who sits looking after them at the entrance. She is a poor creature, huddled in a dark corner. Yet, when she lifts her face, Shankaran sees Shivkami. Old, worn out, with a horrible gash on her face, where Muniyandi had lashed out at her before killing himself.

Shankarans life has come full circle. He has reached his journeys end.

==Cast== Sreenivasan as Muniyaandi
*Bharath Gopi as Shankaran
*Smitha Patil as Shivakaami Innocent as himself
*Nedumudi Venu as himself Murali as Cheriyan
*Dr Mohandas as Jacob
*James

==Soundtrack==
The music was composed by G. Devarajan.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aattilepokum thanni || Seerkazhi Shivachidambaram || ||
|-
| 2 || Anithinudai (Bit) || || Traditional ||
|-
| 3 || Marghazhi (Bit) || || Traditional ||
|-
| 4 || Thalir || P. Madhuri || Traditional ||
|-
| 5 || Thonda randum || P. Madhuri || ||
|-
| 6 || Unnamulai umayaalodum || P. Madhuri || ||
|}

==Production== Aravindan wanted to make a film adaptation of the story written by C. V. Sreeraman. For three years he unsuccessfully tried to get a producer for the project. Then he decided to produce the film himself. The film was made with a tight budget. Filming was primarily held in Mattupetty, near Munnar in Idukki District.   

===Casting===
Unlike earlier films directed by Aravindan, Chidambaram featured a cast consisting of many popular actors. Smitha Patil, who had expressed her desire to work with Aravindan and Gopi joined the project. More actors came forward after learning that Aravindan is producing the film himself. According to Aravindan, "Smita Patil had expressed her desire to act in one of my films much before this. Gopi was also willing to come. When they learned that it is my own production, many artists came forward to help. No one acted in the film expecting any financial returns from me. They did it for me." 

==Themes==

The films two halves deal with two different major themes. The first half primarily explores man-woman relations. Even though Shivakami is loved by Muniyandi, she is drawn towards a more charismatic Shankaran. Shankaran is not too serious about his relation with Shivakami. According to Aravindan, "Shankarans affinity to Shivakami at best is an infatuation. May be that is why he was unprepared and unable to face up to the eventualities."  The second half concentrates on Shankarans response to the events after he runs away from the farm, and it deals with the themes of guilt and redemption. Then there are minor themes such as fading of caste barriers, represented by Shankaran with his relaxed attitude towards people from lower castes, like Muniyandi and Shivakami.

===Significance of the title===
Chidambaram is a temple town located in Tamil Nadu. Shankaran finds Shivakami at Chidambaram in the end of his journey. The legend of Chidambaram temple revolves around a dance contest held between Lord Shiva and Goddess Kali. Kali, the reigning goddess of Thillai forest near Chidambaram witnessed the Ananda Thandava (dance of bliss, as depicted in the famous Nataraja posture) by Shiva and challenged him to a contest. Shiva, wishing to eliminate her arrogance agreed under the condition that whoever wins would become the Lord of Thillai. During the contest, Shiva performed the Urdhva Tandava (one leg pointed straight up), which Kali couldnt perform.  Some legends suggest she was unable to do it out of modesty, and Shiva deliberately performed it with Kalis gender in mind.  However, Kali conceded defeat, felt guilt for her arrogance and became a devotee for Shiva. Shankaran, the name of the protagonist is another name of Shiva. The name Shivakami means either one who loves Shiva or one who is loved by Shiva and is the name of the female deity in the Chidambaram Temple. The uneven relationship between Shiva and Kali is reflected in the relationship between Shankaran and Shivakami as well.

==Reception==
Chidambaram was met with critical acclaim upon its release. Several renowned critics like Iqbal Masud and Ravindran praised the film. Chidambaram was a commercial success too. Aravindan attributed the popularity of the film to a "sustained story line" and the casting of well-known cine artistes. 

==Awards==
 
|- 1985
| G. Aravindan
| National Film Award for Best Feature Film
|  
|-
| G. Aravindan
| Kerala State Film Award for Best Director
|  
|-
| G. Aravindan
| Kerala State Film Award for Best Film 
|  
|-
| Bharath Gopi
| Kerala State Film Award for Best Actor
|  
|}

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 