Linear (film)
{{Infobox film
| name           = Linear
| image          = U2 Linear.jpg
| image_size     = 
| alt            = The opening title screen for the film. It features a black blackground with Linear written in white in the centre. The word is capitalized and underlined, with U2 written below it in a smaller font, also in white.
| caption        = DVD cover
| director       = Anton Corbijn
| producer       = Iain Canning Emile Sherman
| story          = Anton Corbijn Bono
| starring       = Saïd Taghmaoui Danny Lanois
| cinematography = Martin Ruhe
| editing        = James Rose
| studio         = See-Saw Films
| distributor    = Universal Music Group
| released       =  
| runtime        = 57 minutes
| country        = 
| language       = English
| budget         = 
}}
 digital and DVD formats with several editions of the album.

==Development==
The idea behind the film originated from a U2 video shoot in June 2007. During the shoot, Corbijn asked the band to remain still while he filmed them; this created a "photograph on film", in which U2 did not move but the objects around them did.    Impressed, the band believed that the listening experience could be enhanced with visuals, and in May 2008 they commissioned Corbijn to create the film.   The film was created as companion to No Line on the Horizon, and Corbijn has claimed that the film is not a music video, but rather "a new way to listen to a record" and "a new way to use film to connect to music".   

The plot is based on the characters Bono created for the album   as the cop, Lizzie Brocheré as the waitress, Marta Barrio as the lone dancer, and Francisco Javier Malia Vazquez as the barman. 

==Plot==
 
The film opens with a scene of Paris as night falls, before moving on to a journey through the city streets ("Unknown Caller"). At the end of the trip through Paris, a motorcycle cop (Taghmaoui) sits on his police-issued motorcycle, staring at some graffiti on a wall which reads "Fuck the Police" in French language|French.  Kicking his bike over, he pours gasoline on it, sets it alight, and watches it burn ("Breathe"). As dawn breaks, he gets on his own motorcycle and begins his journey through the French countryside before crossing into Spain, aiming to see his girlfriend in Tripoli ("Winter"). Pulling off for a break part-way through the journey, the cop lies on his back and watches a cloud form the image of the African continent before falling asleep ("White as Snow"). Waking up, he resumes his journey across Spain ("No Line on the Horizon").

Upon coming to a town, he pulls off for lunch and enters a small café which, with the exception of the waitress (Brocheré), is devoid of people ("Fez – Being Born"). Bored, the waitress flicks on the television and they watch a U2 music video ("Magnificent"). Resuming his journey, the cop travels through the countryside until making a stop in Cádiz ("Stand Up Comedy"). Walking into a bar, the cop attracts the attention of a woman who begins to dance (Barrio), while the barman (Vazquez) serves him several drinks. Leaving the dancer his keys on the table, the cop goes to leave the bar, but as he does so he looks through a peephole and observes several women with moustaches dancing ("Get on Your Boots"). Walking alone through the streets and with no place to stay overnight, he makes his way down to the beach and falls asleep on the sand ("Moment of Surrender"). Waking up in the morning, he rents a rowboat and begins to paddle his way across the Mediterranean Sea to Tripoli ("Cedars of Lebanon"). 

==Track listing==
The film contains 11 tracks, 10 of which are from No Line on the Horizon. The order of the songs was rearranged from the order of the album. "Winter", a track cut from No Line on the Horizon, is in the film while "Ill Go Crazy If I Dont Go Crazy Tonight" is not.  The films running order is representative of No Line on the Horizon s track list as of May 2008. 

{{tracklist
| total_length = 58:17
| title1 = Unknown Caller
| length1 = 6:19 Breathe
| length2 = 4:37 Winter
| length3 = 6:18 White as Snow
| length4 = 4:47 No Line on the Horizon
| length5 = 4:11
| title6 = Fez&nbsp;– Being Born
| length6 = 5:15 Magnificent
| length7 = 5:24 Stand Up Comedy
| length8 = 3:50
| title9 = Get on Your Boots
| length9 = 3:28
| title10 = Moment of Surrender
| length10 = 7:24 Cedars of Lebanon
| length11 = 4:15 Credits
| length12 = 2:29
}}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 