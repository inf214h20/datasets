Mugabe's Zimbabwe
{{Infobox film
| name     = Mugabes Zimbabwe
| image    = Mugabes_Zimbabwe.jpg
| caption  = Robert Mugabe
| director = Shrenik Rao
| released =  
| runtime  = 60 minutes
| country  = India, Zimbabwe, UK
| language = English
| budget   =
}}
Mugabes Zimbabwe (2010) is a factual film directed by Shrenik Rao.

==Synopsis==
The film narrates the story of Zimbabwe under three decades of Robert Mugabe’s rule as the President of Zimbabwe. The story focuses on how Zimbabwe from its successful independence, under Robert Mugabe’s rule, has collapsed dramatically. 

It features exclusive interviews with the Vice-President of Zimbabwe Ms. Joyce Mujuru, the Governor of Reserve Bank of Zimbabwe - Dr. Gideon Gono, Mr. Joseph Msika, the second Vice-president of Zimbabwe, Ms. Clare Short (British Secretary of State for International Development (1997–2003), Professor Arthur Mutambara (Deputy prime Minister of Zimbabwe (2009-present) and Leader of the Opposition Party Movement for Democratic Change and Archbishop Pius Ncube (Former Archbishop of Bulawayo Zimbabwe), a political refugee.  

==Reception==

Mugabe’s Zimbabwe was taken up for world-wide distribution by TVF International and was featured as one of the ‘Hot Picks of 2011’ by Real Screen at Cannes. The film was critically acclaimed by media across the world.    
 
  

  Pulitzer Prize–winning journalist Barry Bearak of the New York Times said that ‘the film seems a primer to the situation, something of value especially to those not yet familiar with Zimbabwe and its recent 
history’. 
It has been licensed on several media platforms and aired on Television networks across the world.
The film has been translated to 18 languages.   

==References==
 

==External links==
*  
*  
*  
*  
*   (May 2011) Nehanda Radio Zimbabwe.
*  
*  (The Hindu) 
*   (April 2011)
*  
*  
*  
*  
*  
*  
*   (APril 2011) 
*  
*  

 

 
 
 
 
 