Green Street
 
 
{{Infobox film
| name           = Green Street
| image          = Greenstreet.jpg
| caption        = DVD cover
| director       = Lexi Alexander
| producer       = Donald Zuckerman Deborah Del Prete
| screenplay     = Lexi Alexander Dougie Brimson Josh Shelov
| story          = Lexi Alexander Dougie Brimson
| narrator       = Sam Gibson
| starring       = Elijah Wood Charlie Hunnam Claire Forlani Leo Gregory
| music          = Christopher Franke
| cinematography = Alexander Buono
| editing        = Paul Trejo
| studio         = OddLot Entertainment
| distributor    = Universal Pictures (UK) Freestyle Releasing (US)
| released       =  
| runtime        = 109 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $346,830
| gross          = $3,154,346 (Worldwide)
}} West Ham football firm (the Green Street Elite)  run by his brother-in-laws younger brother and is morally transformed by their commitment to each other.

The story was developed by Lexi Alexander, based on her own experience in her brothers firm. Unwilling to shoot the film with German speaking actors, Lexi decided to adapt the heart of the story into the world of English hooliganism. While researching the subject on British internet forums, she came across a self-described hooligan who urged her to contact author Dougie Brimson. Brimson later admitted that he had been the hooligan who had initially made contact and had used a false identity to sound out Alexander and establish both her identity and her credibility.

Brimson wrote the initial script but Alexander later recruited another writer, Josh Shelov, to assist with structure and plot, while Brimson offered technical advice. However, little of his rewrite made the final cut on account of Shelovs lack of understanding of both soccer and the hooligan culture.

Throughout the film, the Green Street Elite, loosely based on West Hams   was released on various dates throughout the world from March 2009 to July 2010 while and the second called   was released in the UK on 21 October 2013

==Plot== West Ham Birmingham City, football match, because of the xenophobic nature of his friends. He is persuaded because Steve will only give the money Pete needs to Matt. After defeating Matt in a fight, Pete decides to take Matt to the football match, thinking he might learn a thing or two.
 Upton Park Birmingham fans, Chelsea Grin, but he is rescued by some GSE members, who are on their way to a larger fight. Though grossly outnumbered, the GSE manage to hold their ground until reinforcements chase off the Birmingham firm. Matt does well in his first true fight and is inducted into the GSE. After a row with Steve, Matt moves in with Pete, and the two exchange stories.
 Manchester United. Matt was not meant to come but ends up sneaking onto the train. Whilst on the train they are warned that 40 Manchester United firm members are waiting for them at the station. Bovver hits the emergency stop button which allows the GSE to get off at an earlier stop (Macclesfield). Having failed to find a taxi, they persuade a van driver to take them into Manchester. Matt sits in the front of the van with the driver; the rest of the GSE are in the rear. As the van approaches the Manchester United fans, Matt tells them that they are moving equipment for a Hugh Grant film, so the fans let them through. When past them, he stops the van, opens up the back, and the GSE charge out to attack the United firm members. They win the fight and run away singing "Theres your famous GSE!"
 Geoff Bell), with whom Bovver makes negotiations after getting jealous of Matt. After one of the members of the GSE see Matt meeting his father, a renowned journalist for The Times, for lunch, they assume Matt is a "journo" as well. Bovver informs Pete of this, and, when Steve finds out, he goes to the Abbey to warn Matt. Matt finds out that Steve used to be "The Major" of the GSE but quit following a match against Millwall F.C.|Millwall, to which Tommy Hatcher brought along his 12-year-old son. The boy was killed in the ensuing fight by members of the GSE, causing Tommy Hatcher to "lose the plot," blaming Steve and the GSE for his sons death. After witnessing this tragedy, Steve left football hooliganism for good.
 petrol bomb the bar. Upon arriving, Tommy Hatcher confronts Steve. Steves attempt to convince Tommy Hatcher that he is no longer involved in the GSE only further reminds Hatcher of his son, and he stabs Steve in the neck with a broken bottle, telling him that if he dies tonight then they are both even. Bovver, who had been knocked out by one of Tommy Hatchers men upon arriving at the Abbey, comes round just in time to help Steve, who is badly injured. At the hospital, Pete blasts Bovver for his betrayal. Shannon decides to head back to the United States to ensure the safety of her family.

In the aftermath, the two firms meet near the Millennium Dome for a bloody and all-out brawl. Matt and Bovver show up to fight for the GSE, but during the fight, Matts sister, Shannon, turns up with her infant son to look for Matt and are proceeded to be attacked by Hatchers right-hand man. Matt and Bovver come to their rescue. Pete notices that Tommy Hatcher is approaching the car, and distracts Tommy by taunting him to "finish him off." When Tommy Hatcher declares to have finished with him, Pete then retorts that Tommy Hatcher was to blame for his sons death, having failed to protect him, shouting "he was your son!". Tommy Hatcher, driven to insanity, attacks and beats Pete to death with his fists, all the while shouting out a variation of the words to the chant Only a poor little Hammer, using it as an analogy for Petes condition. As both sides draw a line at manslaughter, the fight completely halts at this point, and a weeping Tommy is eventually dragged off Pete by members of his firm. Everyone on both sides gathers around Petes dead body in shock, with Bovver sobbing at his side.

Matt returns to the United States and confronts Jeremy Van Holden in a restaurant toilet, where Jeremy is snorting cocaine. Jeremy arrogantly tells Matt to leave during a brief discussion in which he admits to his identity as the cocaine stashs true owner. Matt then pulls out a tape recorder and plays back what Jeremy just said, saying that it is his "ticket back to Harvard." Jeremy lunges at him to try to get the tape, but Matt casually reverses the attack and raises his fist as if to punch Jeremy. He does not do so, instead walking out with a smile as Jeremy collapses to the floor, defeated. The film ends with Matt walking down the street outside the restaurant singing "Im Forever Blowing Bubbles."

==Cast==

*Elijah Wood as Matthew Matt Buckner, nicknamed "The Yankee". A 20 year old American, studying journalism at Harvard University. His mother has died, and his sister lives in London with her English husband. He does not see his dad very often, because he is away working most of the time.

*Charlie Hunnam as Peter Pete Dunham. He teaches history and PE in a local primary school, and also runs West Hams firm, the Green Street Elite (GSE). He is Steves younger brother.

*Leo Gregory as Bovver, a member of the GSE, and Petes right-hand man.

*Claire Forlani as Shannon Dunham (née Buckner). Matts older sister, married to Steve, mother of Ben.

*Marc Warren as Steven Steve Dunham. He ran the GSE in the early 90s and was known as the Major, but he resigned and now lives with his wife Shannon and his son Ben.

*Ross McCall as Dave Bjorno, a Royal Air Force Officer and a member of the GSE.

*Rafe Spall as Swill, a member of the GSE.

*Kieran Bew as Ike, a member of the GSE
 Geoff Bell as Tommy Hatcher, leader of Millwalls firm, the NTO.

*James Allison and Oliver Allison as Ben Dunham, son of Shannon and Steve.

*Terence Jay as Jeremy Van Holden, a cocaine addict and dealer, son of a senator, currently studying at Harvard University.

*Joel Beckett as Terry, landlord of the Bridgett Abbey pub on Walsh Road, and a former member of the GSE. He was Steves right-hand man when Steve led the GSE.

==Cultural context==
The name of the firm in film, the Green Street Elite, refers to   (ICF). 

==Critical reception==
The film received mixed reviews upon release. It scored 46% on film website Rotten Tomatoes,  and 55% on the website Metacritic.  Roger Ebert gave the film a very favourable review,  while the BBC described it as "calamitous".  E! Online reviewed it as "saddled with a predictable storyline and such feckless dialogue that you cant help but view the whole thing as an exercise in stupidity".  Lead star Charlie Hunnams attempted Cockney accent was derided by many critics as being the worst in movie history. 

==Trivia==
Unbeknownst to the director or the producers, writer Dougie Brimson inserted numerous references to his Royal Air Force background into the script as well as a number of private in-jokes. One of these is the use of the term, GSE which actually refers to Brimsons trade whilst serving in the military (Ground Support Equipment). Green Street Elite was developed to fit those initials.

Despite numerous references to differences between the film and the book, Green Street was not based on a book of any kind. The planned novelisation was shelved following creative and contractual disputes between Brimson and Alexander.

==Awards==
Green Street won several awards including Best Feature at the LA Femme Film Festival, Best of the Fest at the Malibu Film Festival, and the Special Jury Award at the SXSW Film Festival.
 Up for Grabs and Opie Gets Laid. 

==Sequels==
  was released Direct-to-video|straight-to-DVD in March 2009.

The film does not star most of the main cast of the first film, but rather focuses on Ross McCall, who played Dave in the first film. The plot has Dave, who was caught from the fight at the end of the first film, in a prison where he must fight to survive. 
  was released in the UK on 21 October 2013.

Starring Scott Adkins from The Expendables 2. Danny Harvey (Adkins) has spent all of his life fighting - in the playground, on the football pitch, and then heading up the West Ham firm the Green Street Elite (GSE). After having turned his back from violence fourteen years prior, Danny is thrust back into the GSE. Younger brother Joey, played by Billy Cook, is killed in an organised fight against a rival firm and Danny is desperate to seek revenge for his brother’s death. Danny returns to  the GSE and his past, the only way he knows to find out who killed his younger brother.

==See also==
 
*The Football Factory (film)
*The Firm (1988 film)
*Inter City Firm

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 