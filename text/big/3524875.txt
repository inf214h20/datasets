Taste the Blood of Dracula
 
{{Infobox film name = Taste the Blood of Dracula image = Taste the blood of dracula.jpg caption = Promotional poster  director = Peter Sasdy producer = Aida Young writer = Anthony Hinds Bram Stoker  (character)  starring = Christopher Lee Geoffrey Keen Gwen Watford music = James Bernard cinematography = Arthur Grant editing = Chris Barnes studio = Hammer Films distributor = Warner-Pathé Distributors|Warner-Pathé (UK) Warner Bros. (USA) released =   runtime = 91 min (cut, USA) 95 min (uncut, UK) country = United Kingdom language = English
}}
Taste the Blood of Dracula is a British horror film produced by Hammer Film Productions and released in 1970 in film|1970. It stars Christopher Lee as Count Dracula, and was directed by Peter Sasdy based upon a script by Anthony Hinds. The film was released as a double bill alongside fellow Hammer production Crescendo (film)|Crescendo.

==Storyline==
===Prologue===
A businessman named Weller is travelling through Eastern Europe when he is thrown from his carriage during a struggle and knocked unconscious. After coming to he discovers it is night time. After wandering some way, he hears a deathly scream. Terrified, Weller runs and falls into a grassy slope. Looking up, he sees a caped figure screaming in agony with a large crucifix impaling him from the back. Weller watches in amazement and fear as the figure dies and disintegrates from blood to reddish dust. Examining the remains, Weller finds a ring, a cape and a brooch with dried blood on it. Dusting away the dried blood, Weller is petrified by the name on the brooch; Dracula.

===Plot===
Three English gentlemen - Hargood, Paxton and Secker - have formed a circle ostensibly devoted to charitable work but in reality they indulge themselves in brothels. One night they are intrigued by a young man who bursts into the brothel and is immediately tended to after snapping his fingers, despite the brothelkeepers objections. The gentlemen are informed that he is Courtley, who was disinherited for celebrating a Black Mass.

Hoping for more intense pleasures, Hargood meets Courtley outside the brothel. The younger man takes the three to the Cafe Royal and promises them experiences they will never forget but insists that they go to see Weller and purchase from him Draculas ring, cloak and dried blood. Having done so, the three meet with Courtley at an abandoned Church for a ceremony during which he puts the dried blood into goblets and mixes it with drops of his own blood, telling the men to drink. They refuse, so he drinks the blood himself, screams and falls to the ground. As he grabs their legs, they kick and beat him, not stopping until Courtley dies, at which they flee. While the three return to their respective homes and return to their lives, Courtleys body, left in the abandoned church, transforms into Dracula, who vows that those who have destroyed his servant will be destroyed.

Dracula begins his revenge with Hargood, who has begun to drink heavily and also treats his daughter Alice harshly, furious that she continues to see Paul, Paxtons son. Dracula takes control of Alices mind via hypnosis and as her drunken father chases after her, she picks up a shovel and kills him. The next day, Hargood is found dead and Alice is missing. The police inspector in charge of the case refuses to investigate Alices disappearance, citing a lack of time and resources.

At her fathers funeral, Alice hovers behind bushes and attracts the attention of Pauls sister Lucy, telling her to meet her that night. They enter the abandoned church where Alice introduces her to a dark figure. Lucy assumes him to be Alices lover but she is greeted by Dracula, who turns her into a vampire.

With Hargood dead and Alice and Lucy missing, Paxton fears that Courtley is exacting revenge and, together with Secker, visits the abandoned church to check for Courtleys corpse. The body is missing but they discover Lucy asleep in a coffin with marks on her throat. Secker realizes she is a vampire and tries to stake her, but Paxton shoots him in the arm, forcing him to flee. While Secker stumbles his way home, Paxton weeps over his daughters body. When he finally develops the courage to stake Lucy himself, she awakens, and Dracula appears. Alice pins Paxton down and Lucy drives a wooden stake through his chest.

That night, Seckers son Jeremy sees Lucy, his lover, at his window and comes down to see her. She sinks her fangs into his throat, enslaving him while Dracula watches. The vampire Jeremy then stabs his father on Lucys orders. On the way back to the church, Lucy begs for Draculas approval but instead he drains her dry and leaves her destroyed. Back at the church, he prepares to bite Alice but a cock crows and he returns to his coffin.

Seckers body causes Jeremys arrest. The police inspector assumes that he hated his father and stabbed him in a rage. Paul disagrees but the inspector refuses to listen. He hands Paul a letter - "the ramblings of a lunatic" he calls it - in which Secker instructs Paul on how to fight the vampires.

Following Seckers instructions, Paul makes his way to the abandoned church. He finds Lucys exsanguinated body en route, floating in a lake. At the church he bars the door with a large cross and clears the altar of Black Mass instruments, replacing them with the proper materials. He calls for Alice, who appears together with Dracula. Paul confronts Dracula with a cross but Alice, still entranced, disarms him. She seeks Draculas approval but he dismisses her. He tries to leave but is prevented by the cross barring the door. His retreat is also barred by a cross which an angry and disappointed Alice threw to the floor. Dracula climbs the balcony and throws objects at Paul and Alice, before backing into a stained glass window depicting a cross. He breaks the glass but suddenly sees the changed surroundings and hears the Lords Prayer recited in Latin. Dazzled and overwhelmed by the power of the newly re-sanctified church, Dracula falls to the altar, and dissolves back into bloody dust. With the vampire destroyed, Paul and Alice leave.

==Cast==
*Christopher Lee as Count Dracula
*Geoffrey Keen as William Hargood
*Gwen Watford as Martha Hargood
*Linda Hayden as Alice Hargood
*Peter Sallis as Samuel Paxton
*Anthony Corlan as Paul Paxton
*Isla Blair as Lucy Paxton John Carson as Jonathan Secker Martin Jarvis as Jeremy Secker
*Ralph Bates as Lord Courtley
*Roy Kinnear as Weller
*Michael Ripper as Inspector Cobb
*Russell Hunter as Felix  Shirley Jaffe as Betty - Hargoods Maid 
*Keith Marsh as Father
*Madeline Smith as Dolly
*Reginald Barratt as Vicar

==Production notes==
*Taste the Blood of Dracula was originally written without Dracula appearing at all.    With Christopher Lees increasing reluctance to reprise the role, Hammer intended to replace Lee and Dracula in the franchise with the Lord Courtley character played by Ralph Bates, who would rise as a vampire after his death and seek revenge on Hargood, Paxton, and Secker. Hammers American distributor refused to release the film without Dracula appearing, prompting Hammer to convince Lee to return, with Dracula replacing the resurrected Courtley.   
*Vincent Price was originally cast to play one of the dissipated British gentlemen, but when the budget for the film was cut, the studio could no longer afford Price and he was released from his contract. 
*The scenes of the gentlemens visit to the local brothel were heavily edited on the films original release. They are fully re-instated on the DVD release.
*In its original United States release, it was rated GP (General audience, Parental guidance suggested—the forerunner to todays PG), but when it was re-released to DVD it was rated R for sexual content/nudity, and brief violence.
*An alternate version of the scene where Lucy bites Jeremy was filmed, with the young man actually becoming a vampire.  This scene was not used, possibly to avoid complicating the plot further with the introduction of another vampire.

==Critical reception==
The Hammer Story: The Authorised History of Hammer Films called the film "the finest genuine Dracula sequel in the entire   series."  It currently holds a positive 64% score on Rotten Tomatoes.

==DVD release==
On 6 November 2007 the movie was released in a film pack along with Dracula (1958 film)|Dracula, Dracula Has Risen from the Grave, and Dracula A.D. 1972. 

==See also==
*Vampire film

==References==
 

;Sources

* |pages=192}}

==External links==
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 