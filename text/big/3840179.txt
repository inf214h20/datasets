Viva Zapatero!
{{Infobox film
| name           = Viva Zapatero!
| image          = Viva_Zapatero_(2005).jpg
| caption        =
| director       = Sabina Guzzanti
| producer       = Andrea Occhipinti, Sabina Guzzanti
| writer         = Sabina Guzzanti
| starring       = Sabina Guzzanti
| music          =  Riccardo Giagni
| cinematography = Paolo Santolini
| editing        = Clelio Benevento
| released       =  
| country        = Italy
| runtime        = 80 min Italian
}}
Viva Zapatero! is a 2005 documentary by Sabina Guzzanti telling her side of the story regarding the conflict with Silvio Berlusconi over a late-night TV political satire show broadcast on RAI-3.

The  show, RAIot (a play on the name of the Italian state public TV: RAI, and the English word riot), lampooned prime minister Berlusconi. Since it wasnt considered a satirical show, but a political one, it was cancelled after the first episode.

==Origin==
Viva Zapatero! is titled after the famous "Viva Zapata!" and it also refers to the Spanish prime minister, José Luis Rodríguez Zapatero, who, immediately after gaining power in Spain, ensured that the head of the state-run public broadcaster would no longer be a political appointee  (as opposed to Italy).

==Story==
The TV broadcasting of the satirical program RAIot was censored in November 2003 after the comedienne, Sabina Guzzanti, made outspoken criticism of the Berlusconi media empire. Mediaset, one of Berlusconis companies, sued the Italian state broadcasting company RAI because of the Guzzanti show asking for 20 million Euro for "damages" and from November 2003 she was forced to appear only in theatres around Italy.

After the show was cancelled, Guzzanti tried fruitlessly to get it reinstated, in spite of the fact that a judge dismissed the case that initially resulted in the termination of the show. Her struggle however, did result in this film. The documentary openly reveals censorship laws that Italian Prime Minister Silvio Berlusconi is presently imposing on the countrys freedom of speech. It is seen by some as Italys "Fahrenheit 9/11." 

In August 2005, it was the sleeper hit of the Venice International Film Festival, receiving a 15-minute standing ovation after its premiere screening. When it opened in Italian theaters, over 200,000 people went to see it in the first week. Its success resulted in an invitation to the 2006 Sundance Film Festival.

==Awards==
Official Selection - Sundance Film Festival 2006
 Official Selection - Tribeca Film Festival 2006

==See also==
*Censorship
*Fahrenheit 9/11 
*Freedom of the press 
*Freedom of speech
*Satire

==External links==
* 
*  
* 
* 
* 
* 
*  written by Boyd van Hoeij
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 