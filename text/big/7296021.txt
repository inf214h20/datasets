Farewell to Harry
{{Infobox Film |
  name     = Farewell to Harry|
  image = |
  caption        = |
    writer         = Garrett Bennett, Neil Weinberger, Steve Edmiston, Ann Wilkinson |
  starring       = Joe Flanigan, William Hall, Jr., Lysette Anthony|
  director       = Garrett Bennett |
  producer       = William Steding |
  distributor    = Questar Inc. PorchLight Entertainment |
  released   = 2002 |
  runtime        = 1 hour 38 min. |
  country        = United States |
  language = English |
  music          = |
  awards         = |
    budget         = |
}}
Farewell to Harry is a 2002 small town drama about a writer (Joe Flanigan) who forms an unexpected friendship with a local legend (William Hall, Jr.)

Tagline: A writer finds his story in a friends final chapter.

==Plot summary==

Nick Sennet (Joe Flanigan) is a writer who returns to his Pacific Northwest hometown to write a novel. While in town, he meets Harry (William Hall, Jr.),who, according to legend, is dead. As their friendship grows, Nick learns that Harry owns a run-down hat factory, where he spends his days drinking whiskey. When Nick becomes a projectionist at a local theatre, he decides that he is going to help Harry save himself before it is too late.
While this is happening Nick meets Harrys old girlfriend, Louie Sinclair (Lysette Anthony)
They decide to try to renovate the factory, and to try to save Harry. When it seems that all will fail, they stumble upon a hidden cellar filled with vintage hats, which allows their dreams to be fulfilled.

==Cast==
*Joe Flanigan ....  Nick Sennet
*Brent David Fraser .... Mickey
*William Hall, Jr. ....  Harry
*Lysette Anthony ....  Louie Sinclair
*Carl Ballantine ....  Hickey John Gilbert ....  Belov

==Awards==

Farewell to Harry won the following awards:  Houston International Film Festival:
*Silver Remy Award, Best First Feature Film
Independent Spirit Awards:
*Semi-Finalist – Someone to Watch First Glance Philadelphia Film Festival:
*Runner-Up, Best Feature
Seattle International Film Festival:
*Special Jury Award – Shooting in Seattle – Best Film Phoenix International Film Festival:
*Official Selection
Spokane International Film Festival:
*Official Selection

==External links==
* 
* 

 
 