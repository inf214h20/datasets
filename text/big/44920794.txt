Outpost: Rise of the Spetsnaz
{{Infobox film
| name           = Outpost: Rise of the Spetsnaz
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Kieran Parker
| producer       =  
| writer         = Rae Brunton
| screenplay     = 
| story          = 
| based on       =  
| starring       = Bryan Larkin Iván Kamarás Michael McKell
| narrator       =  
| music          = Al Hardiman Patrick Jonsson
| cinematography = Carlos De Carvalho
| editing        = Naysun Alae-Carew
| production companies = 
| distributor    =  
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  
}}
Outpost: Rise of the Spetsnaz, also known as Outpost 3, is a 2013 British sci-fi horror film and the third entry in the Outpost film series. Unlike its predecessors  , Rise of the Spetsnaz was not directed by Steve Barker and was instead directed by Kieran Parker, who had served as a producer on both of the prior films.   The film had its world premiere on 27 June 2013	 at the Edinburgh International Film Festival. Rise of the Spetsnaz serves as a prequel to the series and is set during World War II and expands upon the creation of the invincible supernatural soldiers.   

==Synopsis==
Set during World War II, the film follows a group of soldiers that find themselves faced not only with zombie soldiers but also with the threat of themselves becoming part of the zombie corps. 

==Cast==
*Bryan Larkin as Dolokhov
*Iván Kamarás as Fyodor
*Michael McKell as Strasser
*Velibor Topic as Arkadi
*Laurence Possa as Osakin
*Ben Lambert as Rogers
*Alec Utgoff as Kostya
*Vince Docherty as Klotz
*Gareth Morrison as Potrovsky
*Leo Horsfield as The Surgeon
*Vivien Taylor as The Nurse

==Production==
While creating the film Parker chose to focus on the medical aspect of the zombie soldiers, whereas the previous two films took a more supernatural and mechanical approach.  Exterior filming took place in Ripon, Yorkshire while interiors were shot on a stage near Glasgow, Scotland, and filming took place over a 28 day period.   

==Reception== The List and Fangoria both gave more positive reviews for Rise of the Spetsnaz,  and Fangoria wrote that "Despite its flaws, RISE OF THE SPETSNAZ is still a fun watch, offering enough for action and horror fans alike. It’s bloody and brutal while sleek and respectful of its place within the franchise, and sure to please fans of the first two films as well as the unfamiliar." 

==References==
 

==External links==
*  

 
 
 
 
 