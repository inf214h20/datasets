Faithless (2000 film)
 
{{Infobox film
| name           = Faithless
| image          = Faithless (2000 film).jpg
| caption        = Swedish DVD cover
| director       = Liv Ullmann
| producer       = Maria Curman Kaj Larsen Johan Mardell
| writer         = Ingmar Bergman
| starring       = Lena Endre Erland Josephson Krister Henriksson
| music          =
| cinematography = Jörgen Persson
| editing        = Sylvia Ingemarsson
| distributor    = AB Svensk Filmindustri
| released       = 13 May 2000
| runtime        = 154 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
}}
Faithless ( ) is a Swedish film directed by Liv Ullman from a script by Ingmar Bergman. The story is loosely based on experiences of adultery from Bergmans own life. It was entered into the 2000 Cannes Film Festival.   

==Plot==
An aging director named Bergman conjures in his imagination the central character, Marianne. Over a period of days, on his secluded Fårö refuge in the Baltic Sea, he interviews her to compose the story of her life-changing affair. She had been happily married to Markus, an orchestra conductor, with a young daughter Isabelle. She suddenly falls in love with their best friend David. They have an extended interlude in Paris and then must deal with the ramifications.

The style of the film is very cerebral, abstract, and ambiguous. The main potential audience is viewers familiar with the previous decades of work of Bergman and Ullmann, who may appreciate this meditative capstone to their careers and lives together.

==Cast==
* Lena Endre – Marianne
* Erland Josephson – Bergman
* Krister Henriksson – David
* Thomas Hanzon – Markus
* Michelle Gylemo – Isabelle
* Juni Dahr – Margareta
* Philip Zandén – Martin Goldman
* Thérèse Brunnander – Petra Holst
* Marie Richardson – Anna Berg
* Stina Ekblad – Eva
* Johan Rabaeus – Johan
* Jan-Olof Strandberg – Axel
* Björn Granath – Gustav
* Gertrud Stenung – Martha

==References==
 

==External links==
* 
* 
 
 

 
 
 
 
 
 