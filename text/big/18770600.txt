The Forty-First (1956 film)
{{Infobox film
| name = The Forty-First
| image =
| image_size =
| caption =
| director = Grigori Chukhrai
| producer =G. Lukin
| screenplay = Grigori Koltunov
| based on =  
| narrator =
| starring = Izolda Izvitskaya Oleg Strizhenov
| music = Nikolai Kryukov
| cinematography = Sergey Urusevsky
| editing =Lidia Lysenkova
| distributor =
| studio = Mosfilm
| released =   (Soviet Union|USSR)
| runtime = 88 minutes
| country = Soviet Union
| language = Russian
| budget =
}}
 Soviet film Boris Lavrenyev. White Army.

==Plot== camels are stolen, their commander decides to send his captive on a boat to their headquarters in Kazalinsk via the Aral Sea. The vessel capsizes in a sudden storm, and only Maria and Otrok remain alive, stranded on an isolated island. The Red soldier treats the White officer when he catches a fever and is slowly charmed by his manners, while he is overcome with gratitude and begins to call her Man Friday with affection. When she demands to know what he means, he tells her the story about Robinson Crusoe. The two fall in love and seem to forget about the war. 

When a boat approaches their isle, they first think these are fishermen and run toward them. Otrok recognizes them as White soldiers and intends to join them. Maria shoots him in the back, killing him. As she realizes he is dead, she runs into the sea and embraces his corpse.

==Cast==
*Izolda Izvitskaya as Maria Maryutka Filatovna
*Oleg Strizhenov as Lieutenant Vadim Nikolaevich Govorukha-Otrok
*Nikolai Kryuchkov as Commissar Arsentiy Yevsyukov
*Assanbek Umuraliyev as Umankul 
*Nikolai Dupak as Chupilko
*Pyotr Lyubeshkin as Guzhov
*Georgi Shapovalov as Terentyev 
*Danil Netrebin as Semyanin 
*Anatoli Kokorin as Yegorov
*Muratbek Ryskulov as caravan master
*T. Sardarbekova as Aul girl
*Kirey Zharkimbayev as Timerkul, Aul elder
*Vadim Zakharchenko as Lieutenant Kuchkovskiy
*S. Solonitsky as White colonel
*Alexander Grechany as Prokopych
*Nikolai Khryashchikov as the Yesaul

==Production==
The film was based on the story of the same name by Boris Lavrenyov, which had already been filmed as a   and Mikhail Romm. When the screenplay was discussed in the directorate on 19 April 1955 and several of those present raised the issue, Romm said: "very well! Let every girl fall in love with the enemy and then kill him." After that, it was approved for filming. 

Principal photography commenced in spring 1956 and ended in the summer. It was conducted in the Turkmen SSR, in the vicinity of Krasnovodsk and on the Caspian Seas Cheleken Peninsula; the latter served as the location for the island scenes. 

==Reception== Special Jury Prize and was nominated for the Palme dOr. 

==See also==
*The Forty-First (1927 film)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 