The Woods Are Full of Cuckoos
{{Infobox Hollywood cartoon| image =
| cartoon_name = The Woods Are Full Of Cuckoos
| series = Merrie Melodies
| supervision = Frank Tashlin
| animator = Robert bentley
| producer = Leon Schlesigner Warner Bros. Pictures
| director = Frank Tashlin
| story_artist = Melvin Millar Robert Bentley
| musician = Carl W. Stalling
| producer = Leon Schlesinger
| voice actor = Mel Blanc
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures The Vitaphone Corporation
| release_date = December 4, 1937
| color_process = Technicolor
| runtime = 7 min
| movie_language = English
}}
The Woods Are Full Of Cuckoos is a Merrie Melodie cartoon directed by Frank Tashlin, and released in December 1937.  It is a parody/send-up of several different radio programs of the era, particularly the then-popular "community sing" programs. Author and critic Alexander Woollcott is parodied as Owl Kott in the cartoon, a parody that Tashlin would revisit in Have You Got Any Castles? (1938). This is the very first cartoon to use the orange-yellow color scheme for the Merrie Melodies opening rings and also with a new logo for it.

==Plot== Owl Kott" (satirizing Woolcotts "Town Crier" radio program) giving an introduction to the festivities. This is followed by a Ben Bernie caricature called "Ben Birdie", feuding with "Walter Finchell". The same spoof was used in the cartoon The Coo-Coo Nut Grove (1936). Walter Winchell had a well-publicized feud with Bernie at the time, which, like Jack Bennys "feud" with Fred Allen, was faked for publicity purposes - Bernie and Winchell were actually good friends.

Next is "Milton Squirrel" (Milton Berle, emcee of Gillette Community Sing) introducing "Wendell Howl" (Wendell Hall) and an audience trying to figure out which page to go to in their songbooks, which results in Wendell getting pelted by the audiences songbooks. Then, "Billy Goat and "Ernie Bear" (Billy Jones and Ernie Hare) and everyone else sings a song which the words are:

:The Woods are full of cuckoos,
:Cuckoos, cuckoos,
:The Woods are full of cuckoos
:and my heart is full of love.

During the song, a fox (a caricature of Fred Allen) called "Mr. Allen" is told that hes singing "Swanee River" instead of the actual song. Then the song is sung by "Eddie Gander" (Eddie Cantor), "Sophie Turkey" (Sophie Tucker), "W.C. Fieldmouse" (W.C. Fields|Fields), "Dick Fowl" (Dick Powell), "Fats Swallow" (Fats Waller|Waller), "Deanna Terrapin" (Deanna Durbin), "Irvin S. Frog" (Irvin S. Cobb), "Fred McFurry" (Fred MacMurray), "Bing Crowsby" (Bing Crosby|Crosby), "Al Goatson" (Al Jolson|Jolson), "Ruby Squealer" (Ruby Keeler, Jolsons wife at the time), and "Lanny Hoss" (Lanny Ross). Then "Grace Moose" (Grace Moore) and "Lily Swans" (Lily Pons) sing notes, each note higher than the other. Comedienne and jazz singer Martha Raye (caricitured here as a mule named "Moutha Bray") makes an appearance in a scatting jazz take.  Then, more spoofs are made, including movie critic and gossip columnist "Louella Possums" (Louella Parsons), Raven McQuandry (Haven McQuarrie, emcee of Do You Want To Be An Actor?), Joe Penguin (Joe Penner), Tizzie Fish ("Tizzie Lish", a character on Al Pearces radio show), Jack Bunny (Jack Benny|Benny), Mary Livingstone, and Andy Devine (a regular on Bennys radio program). Finally Owl Kott finishes the cartoon by bidding the audience goodnight, and saying "All is well, all is well..."

==Availability==
This cartoon is found on The Looney Tunes Golden Collection: Volume 3.

 
{{Succession box |
before= Porkys Hero Agency (LT) |
title= List of Merrie Melodies Cartoons |
years= 1937 |
after= September in The Rain|}}
 

 
 
 
 
 
 