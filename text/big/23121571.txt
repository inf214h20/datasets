The Big Night Bathe
 
{{Infobox Film
| name           = The Big Night Bathe
| image          = 
| image_size     = 
| caption        = 
| director       = Binka Zhelyazkova
| producer       = Ivan Petkov
| writer         = Hristo Ganev
| narrator       = 
| starring       = Yanina Kasheva
| music          = 
| cinematography = Plamen Vagenshtain
| editing        = Madlena Dyakova
| distributor    = 
| released       = 1 December 1980
| runtime        = 144 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Big Night Bathe ( , Transliteration|translit.&nbsp;Golyamoto noshtno kapane) is a 1980 Bulgarian drama film directed by Binka Zhelyazkova. It was screened in the Un Certain Regard section at the 1981 Cannes Film Festival.   

==Cast==
* Yanina Kasheva - Ninel
* Malgorzata Braunek - Zana
* Tanya Shahova - Lora
* Lyuben Chatalov - Stoyan
* Ilia Karaivanov - Ivan
* Nikolai Sotirov - Sava
* Juozas Budraitis - Vili
* Ivan Kondov
* Ventzislav Bozhinov
* Krasimir Mashev
* Nelli Topalova
* Kiril Bozev
* Lyubomir Mladenov
* Ramiz Tatarov
* Margarita Kumbalieva
* Lyubka Ilieva

==References==
 

==External links==
* 

 
 
 
 
 
 
 