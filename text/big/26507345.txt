Father of Invention
{{Infobox film
| name = Father of Invention
| image = Father of Invention.jpg
| caption = Theatrical release poster
| director = Trent Cooper
| producer = Kevin Spacey Ken Barbet Dana Brunetti Kia Jam Jonathan D. Krane Jason Sciavicco
| writer = Trent Cooper Jonathan D. Krane Heather Graham Craig Robinson and Virginia Madsen
| music = Nick Urata
| cinematography = Steve Tedlin
| editing = Heather Pearsons
| studio = Trigger Street Productions Anchor Bay Films K Jam Media
| released =  
| runtime = 120 minues
| country = United States
| language = English
| budget =
| gross =
}}
Father of Invention is a 2010 American comedy-drama film directed by Trent Cooper and starring Kevin Spacey, Camilla Belle and Johnny Knoxville.

== Synopsis ==
Robert Axle, a New Orleans-based eccentric inventor turned ego maniacal infomercial guru, loses it all when one of his inventions maims thousands of customers. After eight years in maximum security prison, Axle is ready to redeem his name and rebuild his billion dollar empire. But first he must convince his estranged 22-year-old daughter to let him live with her and her quirky, over protective roommates.

== Cast ==
* Kevin Spacey as Robert Axle
* Virginia Madsen as Lorraine
* John Stamos as Steven "Steve" Leslie Thurmond
* Johnny Knoxville as Troy Coangelo
* Camilla Belle as Claire Elizabeth (formerly Axle but dropped the last name after her father went to prison.) Heather Graham as Phoebe
* Michael Rosenbaum as Eddie
* Anna Anissimova as Donna Craig Robinson as Jerry
* Rhoda Griffis as Penny Camp Jack McGee
* Danny Comden as Matt James
* Marc Macaulay as Grocery Store Clerk
* Audra Marie as Businessmans Date
* Mary Elizabeth Cobb as Sheila

== References ==
*  

== External links ==
*  
*  

 
 
 
 
 
 
 


 