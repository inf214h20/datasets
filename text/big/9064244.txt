Scorpio (film)
{{Infobox film
| name           = Scorpio
| image          = Scorpio-poster.jpg
| image size     = 
| alt            = 
| caption        = Original film poster by Robert McGinnis
| director       = Michael Winner
| producer       = Walter Mirisch
| writer         = David W. Rintels Gerald Wilson
| narrator       = 
| starring       = Burt Lancaster Alain Delon Paul Scofield John Colicos Gayle Hunnicutt J.D. Cannon
| music          = Jerry Fielding
| cinematography = 
| editing        = 
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 114 min.
| country        = United States
| language       = 
| budget         = 
| gross          = $1,400,000 (US/ Canada rentals)  1,052,001 admissions (France) 
| preceded by    = 
| followed by    = 
}}
Scorpio is a 1973 spy film directed by Michael Winner. It stars Burt Lancaster, Alain Delon and Paul Scofield.

==Plot==
It is about a Central Intelligence Agency (CIA) agent named Cross (Burt Lancaster), a successful but retiring assassin, who is training "Scorpio" to replace him.  Cross is teaching him as much about protecting himself from his patrons and never trusting anyone as how to get away clean.

The CIA tells "Scorpio", Jean Laurier (Alain Delon), to kill Cross for suspected treason and collaboration with the Russians.  Scorpio is threatened with jail on a false narcotics charge if he doesnt cooperate.

In a failed break-in of Crosss home, CIA agents shoot and kill his wife Sarah (Joanne Linville), causing Cross to go on the run and seek protection from the Russians, who want the secrets he knows and maybe revenge for past acts.  Cross is very successful at evading capture, and even kills the CIA director who was responsible for his wifes death.  However, the CIA gains evidence that Cross is working with Scorpios girlfriend.

Enraged by this betrayal, Scorpio corners and kills his girlfriend and Cross, but not before Cross imparts his last words of wisdom.  Moments later, Scorpio is assassinated, as Cross said he would be when "They" were done with him.  The viewer is left to speculate on who is behind Scorpios death.

==Cast==
* Burt Lancaster as Cross
* Alain Delon as Scorpio
* Paul Scofield as Zharkov
* John Colicos as McLeod
* Gayle Hunnicutt as Susan
* J.D. Cannon as Filchock
* Joanne Linville as Sarah
* Vladek Sheybal as Zemetkin
* James Sikking as Harris
* William Smithers as Mitchell
* Celeste Yarnall as Helen Thomas

==Critical Reception== Time Out magazine, Geoff Andrew took a negative view of the film: "Winner directs with typically crass abandon, wasting a solid performance from Lancaster". "Scorpio", in Time Out Film Guide 2011,
Time Out, London, 2010. ISBN 1846702089 (p. 936). 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 