Dunked in the Deep
{{Infobox Film |
  | name           = Dunked in the Deep |
  | image          = Dunkedindeepposter49.jpg |
  | caption        =  |
  | director       = Jules White | Felix Adler|
  | starring       = Moe Howard Larry Fine Shemp Howard Gene Roth|
  | cinematography = Vincent J. Farrar | 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       = November 3, 1949 (United States|U.S.) |
  | runtime        = 16 46" |
  | country        = United States
  | language       = English
}}
Dunked in the Deep is the 119th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are tricked into becoming stowaways by their neighbor Mr. Borscht (Gene Roth), a spy for a fictitious USSR-like country. Stranded on a freighter on the high seas, and sustained by eating salami, they discover that their friend has concealed stolen microfilm in watermelons. After a wild chase, the boys overtake Borscht and recover the microfilm.
  (far left) looks on]]

==Production notes== reworked in 1956 as Commotion on the Ocean using ample stock footage.   

The voice heard on the radio broadcast is Moe; Shemp Howard accidentally cut his hand on the lock when he rushes to the door in an effort to open it. 

Hiding microfilm in watermelons is an allusion to an actual event from the previous year. In 1948,   documents, which Chambers had concealed in a hollowed-out pumpkin on his Maryland farm. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 