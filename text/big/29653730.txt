Squeeze a Flower
{{Infobox film
| name           = Squeeze a Flower
| image          =
| image size     =
| alt            =
| caption        =
| director       = Marc Daniels
| producer       = George Willoughby 
| writer         = Charles Isaacs
| narrator       =
| starring       = Walter Chiari Jack Albertson Rowena Wallace
| music          = Tommy Leonetti
| cinematography = Brain West
| editing        = Stanley Moore Group W Films NLT Productions
| distributor    = British Empire Films
| released       = 13 February 1970
| runtime        = 102 Minutes
| country        = Australia
| language       = English
| budget         = $750,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 246 
| gross          =
}} Australian comedy film directed by Marc Daniels and starring Walter Chiari.

==Plot==
Brother George is the only monk at the Italian Monastery who knows the secret recipe of the popular liqueur that is the sole source of income for the monastery. When he feels their sole distributor, a local wine merchant, isnt giving the Monastery a fair price, he leaves and moves to Australia. There he works at a vineyard picking grapes and starts making the Liquor in his spare time.

George then comes to the attention of the Winery owner Alfredo Brazzi and the two agree to a partnership to make the liqueur. Alfredo is unaware George is a monk and that he sends 50 Percent of the money back to his Italian monastery.

Alfredo and his son-in-law Tim constantly try to steal the secret recipe. They recruit June for their skulduggery, but she falls in love with George, also unaware of his religious calling. Finally, the Italian wine merchant travels to Sydney, willing to make a deal with Brother George. The merchant ends up paying double the price he had previously rejected. 

==Cast==
* Walter Chiari – Brother George
* Jack Albertson – Alfredo Brazzi
* Rowena Wallace – June Phillips Dave Allen – Tim OMahoney
* Kirrily Nolan – Maria OMahoney
* Alec Kellaway – the Abbot
*Michael Laurence – Brother James
*Alan Tobin – Brother Peter
*Charles McCallum – Brother Sebastian
*Harry Lawrence – Vequis
*Roger Ward – bosun
*Alex Mozart – truck driver
*Sandy Harbutt – grape pickers
*Amanda Irving – grape picker
*Jeff Ashby – Bert Andrews
*Penny Sugg -stewardess
*Sue Lloyd – receptionist
*Barry Crocker – waiter
*Lea Denfield – flower seller
*Pat Sullivan – laboratory assistant
*Bobby Limb
*Dawn Lake

==Production==
The film was meant to be the first in a proposed series of ten films made jointly by NLT Productions and Group W. NLT Productions was a television production company in Sydney and was supported by Motion Picture Investments, a company associated with various Australian businessmen including Sir Reginald Ansett.  Group W was a division of the American Westinghouse Broadcasting Company. 

Leading cast and crew were imported: the director, writer and producer were all American and the lead actors were from overseas: Italian Walter Chiari, American Jack Albertson and Irish David Allen. It was Allens first major film role. 

Filming began in mid February 1969and only took a month. Shooting took place in the studio of Ajax Films in Sydney and on location in St Patricks College, Manly, and Mount Pleasant vineyard in the Hunter Valley.  

Walter Chiari had previously made Theyre a Weird Mob (film)|Theyre a Weird Mob (1966) in Australia. He married his girlfriend during the shoot. 

==Release==
The films premiere in Sydney was attended by Australian Prime Minister John Gorton but the movie was not well received, commercially or critically. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 