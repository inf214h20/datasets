The Wall (2012 drama film)
 
{{Infobox film
| name           = The Wall
| image          = Film poster for The Wall (2012 film).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Julian Pölsler
| producer       = {{Plainlist|
* Wasiliki Bleser
* Rainer Kölmel
* Antonin Svoboda
* Bruno Wagner
}}
| writer         = Julian Pölsler
| based on       =  
| starring       = Martina Gedeck
| music          = 
| cinematography = {{Plainlist|
* Markus Fraunholz    
* Martin Gschlacht    
* Bernhard Keller    
* Helmut Pirnat    
* Hans Selikovsky    
* Richard Wagner  
}}
| editing        = {{Plainlist|
* Thomas Kohler    
* Bettina Mazakarini  
}}
| studio         = {{Plainlist|
* Coop99 Filmproduktion 
* Starhaus Filmproduktion 
}}
| distributor    = {{Plainlist|
* The Match Factory  
* Music Box Films  
* StudioCanal  
* Thim Film  
}}
| released       =   |df=yes}}
| runtime        = 108 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = $1,156,765   
}}
 Die Wand Austrian Alps. Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated.

==Plot==
A woman (Martina Gedeck) travels with her two friends, Hugo and Luise, and their loyal dog Lynx to their isolated hunting lodge in the Austrian Alps. Soon after they arrive, Luise insists that Hugo accompany her to a pub in a nearby village, leaving behind their dog and the woman. The next morning, realizing her friends never returned as planned, the woman sets off on foot toward the village, followed by the dog. As she walks along the road, her progress is stopped abruptly by a mysterious invisible wall. After several unsuccessful attempts to continue past the invisible barrier, the woman turns back to the lodge with the dog. Along the way she approaches a farmhouse, but again is prevented by the invisible wall from making contact with the two owners who appear frozen in time. 

Back at the lodge, the woman grows increasingly depressed over her predicament. After exploring the area again with binoculars, she concludes that the couple at the farmhouse, as well as all the people in the village, must be dead. During her walk, the woman encounters a cow that she takes with her. Knowing the animal is both a blessing and a burden, she feeds and milks her, and names her Bella. Sometime later, she gets into Hugos car and drives down the road toward the village and tries to drive through the obstruction—but the car crashes into the invisible wall.

Fighting off despair, the woman decides that she can survive the summer with the cow and her endless supply of wood. While she plants potatoes and looks for food, she keeps track of the passing time by crossing off the days on a calendar—a remnant of the civilized life she still retains. She takes in a stray cat. While she welcomes the newcomer, the dog remains her "only friend in a world of troubles and loneliness"—always happy to see her.  To survive, she is forced to engage in the "bloody business of hunting" animals for food.

One day the woman hikes to another lodge in a high mountain pasture. Comforted by the warm summer sun, the beautiful mountains, and the gentle sounds of birds, the woman is transformed by the experience. Soon the cat gives birth and the woman names the newborn white cat Pearl. Later that summer, the woman with great effort harvests the hay in the meadow. In the fall, the white cat Pearl dies in a wind storm. On 5 November, the woman starts to write her "report" on the back of old calendars and stationery.

The woman and her animals face an icy winter. On 11 January, the cow gives birth to a calf. In the coming weeks, the woman grows tired and dreams of succumbing to the deadening snow. On 25 May, after spending a year at the lodge in the narrow valley, the woman and her animals leave in a procession and climb to the high mountain pasture where they spend the summer comforted by the warm sun and star-filled nights. For the first time in her life, the woman experiences a feeling of calm "as if a big hand stopped the clock" in her head.  During that second summer, a transformation begins to take place in her, as if her "newer self" was "being absorbed into a greater whole".

In October the woman returns from the pasture and begins writing her report again. Winter quickly follows, and soon the spring. One morning she spots a white crow, seemingly ostracized by her black brethren. Gradually the woman becomes disengaged from her past. In June, she and the animals return to the pasture, but this year she does not feel the same rapture as before. One day, while returning with Lynx after a walk, the woman sees a strange man killing Bellas bull calf with an ax. As Lynx runs to stop him, the man kills the loyal dog. The woman retrieves her rifle and shoots the stranger—killing him. After rolling the corpse over a high cliff, the woman buries her dog Lynx in a deep hole. The next morning, she and Bella the cow leave the pasture and return to the narrow valley. 

That October she harvests potatoes and fruit and soon the winter arrives again. On 25 February, having run out of paper to write on, the woman ends her report.

==Cast==
* Martina Gedeck as Die Frau
* Ulrike Beimpold as Luise
* Karlheinz Hackl as Hugo
* Julia Gschnitzer as Keuschlerin
* Hans-Michael Rehberg as Keuschler
* Wolfgang Maria Bauer as Der Mann

==Release==
The Wall premiered at the 62nd Berlin International Film Festival on 12 February 2012.   It was released 5 October 2012, in Austria and grossed $74,356.  The worldwide gross was $1,156,765. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 74% of 39 surveyed critics gave the film a positive review; the average rating was 6.5/10.   Metacritic rated it 67/100 based on 18 reviews.   Boyd van Hoeij of Variety (magazine)|Variety wrote that the constant voiceover turns the film into "a well-illustrated audiobook" in which "images and voiceover never quite fuse into a single whole".   Neil Young of The Hollywood Reporter called the cinematography "stunningly beautiful" but said that the characterization of the woman was lacking in back story, and the climaxs "tonal shift is unsatisfyingly awkward".   Gary Goldstein of the Los Angeles Times called it "a remarkably involving film" that is "far more transfixing than it may sound".   Neil Genzlinger of The New York Times called it "a one-woman study of physical and mental survival" with a convincing performance by Gedeck. 

===Awards and nominations===
{| class="wikitable"
|- Award
!style="width:250px;"|Category Nominee
!style="width:50px;"|Result
|-
| Bambi Awards, 2012
| Best Actress
| Martina Gedeck
|  
|- Berlin International Film Festival, 2012
| Prize of the Ecumenical Jury, Panorama
| Julian Pölsler 
|  
|- Bombay International Film Fesival, 2012
| Golden Gateway
| Julian Pölsler
|  
|- Deutscher Filmpreis|German Film Awards, 2013
| Best Sound
| Christian Bischoff, Johannes Konecny, Uwe Haussig
|  
|-
| Best Performance by an Actress in a Leading Role
| Martina Gedeck
|  
|-
| Outstanding Feature Film
| Wasiliki Bleser, Rainer Kölmel, Antonin Svoboda, Bruno Wagner 
|  
|- German Film Critics Association Awards, 2013
| Best Actress
| Martina Gedeck
|  
|- Romy Gala, Austria, 2013
| Best Direction
| Julian Pölsler
|  
|-
| Favorite Actress
| Martina Gedeck
|  
|- Vienna International Film Festival|Viennale, 2013
| Best Film
| Wasiliki Bleser, Rainer Kölmel, Antonin Svoboda, Bruno Wagner 
|  
|-
| Best Director
| Julian Pölsler
|  
|-
| Best Actress
| Martina Gedeck
|  
|-
| Best Screenplay
| Julian Pölsler
|  
|-
| Best Art Direction
| Petra Heim, Enid Löser, Renate Schmaderer, Laura Biemann 
|  
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Austrian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 