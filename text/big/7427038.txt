The Naked Monster
 
{{Infobox film
| name           = The Naked Monster
| image          = Naked_monster_key_art_4_wiki.jpg
| image size     =
| border         =
| alt            =
| caption        = DVD cover
| director       = Ted Newsom Wayne Berwick
| producer       = Ted Newsom
| writer         = Ted Newsom
| narrator       = George Fenneman
| starring       = Kenneth Tobey Brinke Stevens R.G. Wilson John Goodwin Cathy Cahn Forrest J Ackerman John Agar Michelle Bauer Bob Burns Jeanne Carmen Robert Clarke Robert O. Cornthwaite George Fenneman Robert Shayne  Paul Marco  Linnea Quigley  Les Tremayne
| music          = Albert Glasser Ronald Stein
| cinematography = Lazslo Wong Howe Mark Wolf
| editing        = Steve Bradley Ted Newsom Jeffrey W. Scaduto
| studio         = Heidelberg Films
| distributor    = Anthem Pictures Imageworks Entertainment International
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Naked Monster is a 2005 American ultra low-budget science fiction and horror comedy fan-film written by Ted Newsom and directed by Newsom and Wayne Berwick as an homage to and spoof of the "giant monster-on-the-loose" films of the 1950s. {{cite web
| url=http://www.dvdtalk.com/dvdsavant/s2088nake.html
| title=DVD Savant review: The Naked Monster
| publisher=DVD Talk
| date=August 16, 2006
| accessdate=August 5, 2011
| author=Glenn Erickson}}  {{cite news
| url=http://www.washingtonpost.com/wp-dyn/content/article/2006/08/07/AR2006080700562.html
| title=It Came From the DVD Bin
|work=Washington Post
| date=August 15, 2006
| accessdate=August 5, 2011
| author=Mike Keefe-Feldman}}  {{cite web
| url=http://www.dvdtalk.com/reviews/23396/naked-monster-the/
| title=review: The Naked Monster
| work=DVD Talk
| date=August 1, 2006
| accessdate=August 5, 2011
| author=Stuart Galbraith IV}}  {{cite web
| url=http://www.dreadcentral.com/reviews/naked-monster-the-dvd
| title=DVD review: The Naked Monster
| publisher=Dread Central
| date=December 18, 2007
| accessdate=August 5, 2011
| author=Melissa Bostaph}}  The final project took 21 years to make, and was actor Kenneth Tobeys last film. 

==Background==
The project originated in 1984, after director Ted Newsom was challenged with a bet to produce a movie for $2,500.    In response he created "Attack of the B-Movie Monster" {{cite web
| url=http://www.ovguide.com/the-naked-monster-9202a8c04000641f80000000011d1d35#
| title=The Naked Monster Video OV Guide
| accessdate=August 5, 2011}}  which had a limited VHS release under that name in 1985. {{cite news
| url=http://movies.nytimes.com/movie/225213/Attack-of-the-B-Movie-Monster/details
| title=Attack of the B-Movie Monster (1985)
| work=InBaseline
| agency=The New York Times
| accessdate=August 5, 2011}}  {{cite web
| url=http://movies.amctv.com/movie/225213/Attack-of-the-B-Movie-Monster/overview
| title=Attack of the B-Movie Monster overview AMC
 | accessdate=August 5, 2011}}  {{cite web
| url=http://www.sfsite.com/gary/agar01.htm
| title=John Agar Gary Westfahls Bio-Encyclopedia of Science Fiction Film
| accessdate=August 6, 2011
| author=Gary Westfahl}}   
In revisiting the project, the filmmakers added scenes from old monster films to make a new version for release on DVD. {{cite web
| url=http://www.mjsimpson.co.uk/reviews/nakedmonster.html
| title=review: The Naked Monster
| publisher=M. J. Simpson
| accessdate=August 5, 2011
}}   According to Newsom, he "hauled out the old scripts, took gags and lines, and did a 25-page script, which condensed things to manageable size. That version of the project was designed as a half-hour short which could be shot in about four weekends (plus the time for effects). On that basis, I asked Wayne Berwick to direct "Attack of the B-Movie Monster", since I was producing and had drawn the storyboards for both the live action and effects shots." {{cite web
|url=http://www.mjsimpson.co.uk/interviews/tednewsoma.html
|title=Ted Newsom Interview:Part 1
|publisher=M. J. Simpson Super 8 film during 18 days of shooting in the summer of 1984, although for some days they would only shoot for two hours then quit.   Over the following months the animation effects were added and the version was finished by the end of the year.  The film was first screened at the now defunct EZTV in West Hollywood. 
 John Harmon, Rank Telecine apparatus and made a deal with Chuck Adleman of Anthem Pictures to use their editing system to cut the 100 minutes down by 16 minutes.   In the cutting process several effects shots were lost, such as the whole Titanic and submarine sequences, but Newsom claimed these needlessly slowed down the film. 
 War of the Worlds (1953). 

==Synopsis== John Goodwin). They discover that a giant green monster is at large, the Creaturesaurus Erectus, ("He wrecked us?  He nearly KILLED us!")  They turn to experienced monster fighter Colonel Patrick Hendry (Kenneth Tobey), who has been locked in a government asylum for decades.  With the help of other monster experts, lots of stock footage and incredibly bad special effects, humanity fights back against the "Thing from Another Time Zone".

==Cast==
 
* Kenneth Tobey as Col. Patrick Hendry (from The Thing from Another World (1951))
* Brinke Stevens as Dr. Nikki Carlton
* R.G. Wilson as Sheriff Lance Boiler John Goodwin as Agent Jeff T. Stewart
* Cathy Cahn as Connie Lingus
* Forrest J Ackerman as Flustered Man
* John Agar as himself (as Clete Ferguson, from Revenge of the Creature) *
* Michelle Bauer as Second Mom
* Bob Burns III as Admiral Burns
* Jeanne Carmen as Mrs. Lipschitz
* Robert Clarke as Major Allison (from Beyond the Time Barrier (1960))
* Robert O. Cornthwaite as Dr. Carrington (from The Thing from Another World (1951))
* George Fenneman as Narrator
* Robert Shayne as Professor Bradshaw (from Indestructible Man (1956)) John Harmon as Mr. Lipschitz
* Paul Marco as Kelton the Cop (from Plan 9 from Outer Space (1959) and other films)
* Lori Nelson as herself (as Helen Dobson, from Revenge of the Creature) *
* Linnea Quigley as Deaf Girl
* Daniel Roebuck as Captain Company
* Ann Robinson as Dr. Sylvia Van Buren (from The War of the Worlds (1953))
* Gloria Talbott as Nikkis Mom 
* Les Tremayne as General Mann (from The War of the Worlds (1953))
* Ted Newsom as Hospital Orderly
* Jimmy Williams as Sgt. Paisley
* Brady Hicks as Lt. Nyby
* Robert Weaver as Foreman Gorman
* Tim Murphy as Tank Commander
* Tim Sullivan as Dr. Howard
* Richard Nathan as Dr. Howard
* Jason Sechrest as Cat Scan Tech Bill Warren as Panicked Man
* Stuart Galbraith IV as Not So Panicked Man
* Danny Berwick as Baby
* Tina Cheri as Honey
* Milos as Guy with Honey
* Mike Reiss as Oil Can Guzzler
* Debbie Watson as Screaming Babe
* Lonn Friend as Crushed Man
* Del Howison as Screaming Townsfolk
 

*Certain character names and plot elements were created with regard to the "2LiveCrew/Pretty Woman" decision on copyright exemption in parody.

==Critical reception==
DVD Talk reviewer Glenn Erickson noted that "the generation of baby boomers who grew up watching Creature Feature movies on television has become a substantial fan base and the audience for all manner of nostalgic movie fare, most of it terrible". He explained that with such topics now being the subject of films with major budgets, the only low-budget genre filmmaking being produced is at the direct-to-video level, and expanded that "finding anything worthwhile in either camp has become an exceptional event". He noted that being assembled by genre enthusiasts, The Naked Monster has an "amusing point of view", and that while it is "Nobodys idea of quality moviemaking,   is good fun for the undemanding monster fan" which "follows the tradition of John Landis Schlock (film)|Schlock", but "accelerated with non-stop jokes in the manner of Airplane!". He clarified that while the jokes are not all funny, "most of the nonsense makes one smile and the occasional mismatched cutaway or individual gag is especially funny", making special note that the films humor is "fine-tuned to the movie memories of the films intended audience". 

Dread Central wrote that "There is nothing better than a great bad movie", and the films cast of B-Movie horror and sci-fi celebrities sweetened the deal in creating a film "that epitomizes everything that is so scrumptious about all of your favorite B-Movie entrees". They also wrote that the film acts as a "compilation/montage/homage..." "...of nearly every early monster or sci-fi film ever made". After viewing it, the reviewer wrote that the film began "beyond cheesy and damn near annoying at first, but once I finally understood what the filmmakers were attempting to do I just sat back, set my brain on coast, and enjoyed the ride". He enjoyed that the acting was "intentionally atrocious", the monster "unbelievably ludicrous", the plotline and story "full-blown stupidity at its finest", and the use of nudity "relentlessly gratuitous", writing that "you cant help but take pleasure in watching the brilliantly calculated cinematic train wreck." 
 Bill Warren", Robert Cornthwaite, George Fenneman, Robert Shayne and Gloria Talbott died before the film was completed and that Paul Marco died within a year of the films release, and remarked that the film "stands as a warm tribute to each and every one of them". 

Stuart Galbraith IV of DVD Talk had a small role in the film, writing "The project began around 1983–84, and was still shooting as late as 1998 or 99, when this reviewer was enthusiastically recruited for a bit part."  In reviewing the completed project in 2006, he shared instances where science fiction and horror films of the 50s and 60s had been the target for satire and parody by "neophyte directors", and wrote "Rarely are these misbegotten projects made by filmmakers who actually like or understand the nature of the films theyre sending up, and rarer still are they actually funny, striking that delicate balance between an affection for the genre with a recognition of its sometimes silly cliches and successfully translating this into humor."  He noted that The Naked Monster was successful in striking that balance, but that it was "overlong and will appeal mainly to an audience weaned on the myriad films it references", and that the films humor "wisely varies" from sight gags and bizarre non sequiturs to plays on words, in-jokes, and unashamed bawdiness, with its dialogue "played with straight faces", and the film dialogue being "often funny precisely because its only very slightly askew".  He concluded by writing that the film "would probably play a lot better cut even tighter, say trimmed of another 15 minutes or so," as "The style of humor tends to tax the viewer after an hour, and the climax drags on much longer than it should." 

Washington Post noted director Ted Newsoms success in his wish to create a 50s-style B-movie for 2005m using footage from many earlier films in creating his homage to the genre. 

==DVD extras==
The DVD includes an audio commentary with directors Wayne Berwick and Ted Newsom with Newsom discussing everything from Kenneth Tobey|Tobeys grumpy dissatisfaction with the project to his own realization that years after they had originally shot their scenes, some actors from the cast are no longer speaking to him. Also included is a gallery of film stills, six minutes of deleted scenes, a brief video documentary, and a 16-minute interview with Kenneth Tobey with Dr. Franklin Ruehl.   

==Recognition==

===Awards & nominations===
* 2005, Shockerfest nomination, Best Actress Science Fiction for Brinke Stevens 
* 2005, Shckerfest nomination, Best Actor Science Fiction for Kenneth Tobey   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 