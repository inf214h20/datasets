Kick In (1931 film)
{{infobox film
| name           = Kick In
| image          =
| imagesize      =
| caption        = Richard Wallace
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Willard Mack (play) Bartlett Cormack (screenplay)
| cinematography = Victor Milner
| editing        =
| distributor   = Paramount Pictures
| released       = May 24, 1931
| runtime        = 75 minutes
| country        = United States
| language       = English
}} Richard Wallace and starred the legendary Clara Bow in her last film for Paramount.
 1922 version released by Paramount. The 1922 film, lost for over 80 years, was discovered to have been in the Gosfilmofond archive in Moscow and returned to the U.S. in 2010.

The 1931 version of Kick In is currently controlled by Universal Studios, who own or control all Paramount films made between 1929 and 1949. The 1931 Kick In has (as of 2011) never been broadcast on television.  

==Cast==
*Clara Bow - Molly Hewes
*Regis Toomey - Chick Hewes
*Wynne Gibson - Myrtle Sylvester
*Juliette Compton - Piccadilly Bessie
*Leslie Fenton - Charlie James Murray - Benny LaMarr
*Donald Crisp - Police Commissioner Harvey Paul Hurst - Detective Whip Fogarty
*Wade Boteler - Detective Jack Davis

uncredited
*Edward LeSaint - Purnell, Chicks Boss
*Donald MacKenzie - ?
*J. Carrol Naish - Sam
*Ben Taggart - Detective Johnson
*Phil Tead - Burke, Reporter

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 