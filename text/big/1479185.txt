Fun and Fancy Free
 
{{Infobox film
| name = Fun and Fancy Free
| image = funfanposter.jpg Bill Roberts William Morgan    
| caption = Original theatrical release poster
| producer = Walt Disney Harry Reeves Ted Sears Sinclair Lewis    
| starring = Cliff Edwards Edgar Bergen Luana Patten Walt Disney Clarence Nash Pinto Colvig Billy Gilbert Anita Gordon Dinah Shore Sterling Holloway Dennis Day Paul Smith Walt Disney Productions RKO Radio Pictures, Inc.
| released =  
| runtime = 73 minutes
| country = United States
| language = English
| budget =
}}

Fun and Fancy Free is a 1947 animated feature produced by Walt Disney and released by RKO Radio Pictures on September 27, 1947. It was one of the "package films" (feature-length compilations of shorter segments) the studio produced in the 1940s. It is the 9th animated feature in the Walt Disney Animated Classics series, and the fourth package film by Disney.
 Jimmy MacDonald.

==Film segments== Mickey and the Beanstalk. Jiminy Cricket first appears inside a large plant in a large house, exploring, and singing "Im a Happy-go-Lucky Fellow" until he happens to stumble upon a doll, a teddy bear, a record player with some records, and sets up to play the story of Bongo.

===Little Bear Bongo|Bongo===
This segment is based on the tale "Little Bear Bongo" by Sinclair Lewis, following a circus bear named Bongo who wishes to live freely in the wild. Bongo is raised in captivity and is praised for his performances, but is poorly treated off stage. As such, while traveling by train his natural instincts urge him to break free. Once he escapes and enters a forest, a day passes before his idealistic assessment of his new living situation is shattered and he is faced with some hard conditions. The next morning however, he meets a female bear named Lulubelle. The two fall in love, but he immediately faces a romantic rival in the brutish, enormously-shaped Lumpjaw. Bongo fails to interpret Lulubelle slapping him as a sign of affection and when she accidentally slaps Lumpjaw, he claims her for himself, forcing all other bears into a celebration for the "happy" new couple. Bongo comes to understand the meaning of slapping one another among wild bears and returns to challenge Lumpjaw. He manages to outwit Lumpjaw for much of their fight until the two fall into a river and go over a waterfall. While Lumpjaw is swept away, Bongos hat saves him from falling and he can finally claim Lulubelle as his mate.

Bongo is narrated by Dinah Shore.
 Mickey and the Beanstalk===
This segment is an adaptation of Jack and the Beanstalk with Mickey Mouse, Donald Duck, and Goofy as peasants who discover temperamental Willie the Giants castle in the sky through the use of some magic beans.

Mickey and the Beanstalk was narrated by Edgar Bergen in live-action sequences, who, with the help of his ventriloquist dummies Charlie McCarthy and Mortimer Snerd, told the tale to child actress Luana Patten at her birthday party.

A third version of Mickey and the Beanstalk was on the Disney television show "The Mouse Factory", which aired from 1972 to 1974. This version starred Shari Lewis and Lamb Chop.
 insane by hunger, broke the fourth wall and attempted to kill their cow with an axe, Mickey decides to trade the cow for money to buy food. Goofy and Donald are excited about eating until Mickey comes back and reveals that he traded their beloved bovine for magic beans. Thinking that Mickey was tricked, Donald furiously throws the beans and they fall through a hole in the floor. However, it developes the beans are truly magical as later that night, a beanstalk sprouts and it carries their house upward as it grows. Climbing the gigantic beanstalk they enter a magical kingdom of enormous scope, and entering the castle, Mickey, Donald and Goofy help themselves to a sumptuous feast. This roused the ire of Willie the Giant, who is able to transform himself into anything. When spotted by Willie, Mickey spys a fly-swatter and asked Willie to demonstrate his powers by turning into a fly. Willie initially suggested turning into a pink bunny, but agreed to Mickeys request, he turns into a pink bunny anyway, seeing Mickey, Donald, and Goofy with the fly-swatter. Angry, Willie captures Mickey, Donald, and Goofy and lock them in a box to keep them from pulling any more tricks. Mickey escapes. Mickey must find the key and rescue them, and does so with the help of the singing golden harp. Once free, the hapless heroes return the golden harp to her rightful place and Happy Valley is restored to its former glory, killing the giant by chopping down the beanstalk (the end is never  shown, and a happy ending is declared).

The cartoon ends with Willie the Giant (having survived the fall) stomping through Hollywood looking for Mickey Mouse. Before the scene closes, Willie notices The Brown Derby restaurant and picks up the building looking for Mickey and since the restaurant looks like a hat, places it on his head, and stomps off with the HOLLYWOOD lights blinking in the background.

==Voice cast==
* Edgar Bergen – himself, Charlie McCarthy and Mortimer Snerd
* Luana Patten – herself
* Dinah Shore – singer, narrator of Bongo
* Cliff Edwards – Jiminy Cricket Jimmy MacDonald – Mickey Mouse
* Clarence Nash – Donald Duck
* Pinto Colvig – Goofy
* Billy Gilbert – Willie the Giant
* Anita Gordon – singing harp
* Candy Candido - Lumpjaw the black bear (growls) (uncredited)

==Production==
During the 1940s, Mickey and the Beanstalk and Bongo were originally going to be developed as two separate feature films.

In the late 1930s, Mickeys popularity fell behind Donald Duck, Goofy, Pluto and Max Fleischers Popeye. To boost his popularity, Walt Disney and his artists created cartoons such as The Brave Little Tailor and The Sorcerers Apprentice, which later were included in the feature film Fantasia (1940 film)|Fantasia. In early 1940 during production on Fantasia, animators Bill Cottrell and T. Hee pitched the idea of a feature film based on Jack and the Beanstalk starring Mickey Mouse as Jack, with Donald Duck and Goofy as supporting characters. When they pitched it to Walt, he "burst out laughing with tears rolling down his cheeks with joy", as Cottrell and Hee later recalled. Walt enjoyed it so much he invited other employees to listen to their tale. However he said as much as he enjoyed it, the film would never go into production because as Walt claimed, they "murdered   characters". Gabler, Neal (2006)"Walt Disney: The Triumph of American Imagination", Alfred A. Knopf Inc, New York City  However, Cottrell and Hee were able to talk Walt into giving it the greenlight and story development as The Legend of Happy Valley, which commenced on May 2, 1940. "The story behind Fun and Fancy Free", Disney VHS, 1997 
 Honest John and Gideon from Pinocchio (1940 film)|Pinocchio who  con him into trading his cow for the "magic beans".  Another version had a scene where Mickey gave the cow to the Queen (played by Minnie Mouse) as a gift, and in return she gave him the magic beans. However, both scenes were cut when the story was trimmed for Fun and Fancy Free and the film does not explain how Mickey got the beans.

Shortly after the rough animation on Dumbo was complete in May 1941, The Legend of Happy Valley went into production, using many of the same cast, although RKO doubted it would be a success. Barrier, Michel (1999) Hollywood Cartoons Oxford University Press, United Kingdom  Since it was a simple, low-budget film, in six months fifty minutes was animated on "Happy Valley". Then on October 27, 1941, due to the Disney animators strike and World War II which had cut off Disneys foreign release market caused serious debts so Disney put The Legend of Happy Valley on hold. 

Meanwhile, production was starting on Bongo, a film based on the short story written by Sinclair Lewis for Cosmopolitan (magazine)|Cosmopolitan magazine in 1930. It was suggested that Bongo could be a prequel to Dumbo and some of the cast from the 1941 film would appear as supporting characters,  however the idea never fully materialized. In earlier drafts Bongo had a chimpanzee as a friend and partner in his circus act. He was first called "Beverly" then "Chimpy", but the character was ultimately dropped when condensing the story.  Bongo and Chimpy also encountered two mischievous bear cubs who were dropped.  Originally, the designs for the characters were more realistic, but when paired for Fun and Fancy Free the designs were simplified and drawn more cartoony.  The script was nearly completed by December 8, 1941, the day after the attack on Pearl Harbor. 
 Alice in Peter Pan, package film. He did this during the war on Saludos Amigos and The Three Caballeros and continued after the war until he had enough money to make a single narrative feature again.

Walt felt that since the animation of Bongo and The Legend of Happy Valley (renamed Mickey and the Beanstalk) was not sophisticated enough to be a Disney animated feature film, the artists decided to include the story in a package film.  At first Walt wanted Mickey and the Beanstalk paired with Wind in the Willows (which was in production around this time), under the new title Two Fabulous Characters. However Mickey and the Beanstalk was cut from Two Fabulous Characters and paired with Bongo instead. Two Fabulous Characters eventually added The Legend of Sleepy Hollow and was re-titled The Adventures of Ichabod and Mr Toad. 
 Jimmy MacDonalds first turn at doing Mickeys voice, as he dubbed in additional lines due to the fact that Disney recorded most of Mickeys dialogue in the spring and summer of 1941. Disney however, reprised the role for the introduction to the original 1955-1959 run of The Mickey Mouse Club. 

Celebrities Edgar Bergen and Dinah Shore introduced the segments in order to appeal to a mass audience. Jiminy Cricket from Pinocchio (1940 film)|Pinocchio sings "Im a Happy-go-Lucky Fellow", a song written for and cut from Pinocchio (1940 film)|Pinocchio before its release. 

==Directing animators==
* Ward Kimball (Jiminy Cricket, Donald Duck, Lumpjaw)
* Les Clark (Singing Harp, Lulubelle)
* John Lounsbery (Willie the Giant) Fred Moore (Mickey)
* Wolfgang Reitherman (Goofy)
* Art Babbitt (Bongo)

==Release and reception==
The film was released on September 27, 1947 and enjoyed fairly decent reception. The Disney package films of the late 1940s helped finance the 1950 movie Cinderella (1950 film)|Cinderella, and subsequent others, such as Alice in Wonderland and Peter Pan.

==TV broadcasts and home video releases==

===Availability=== anthology TV series in the 1950s and 1960s. Mickey and the Beanstalk, aired on a 1963 episode with new introductory segments, and Ludwig Von Drakes narration (voiced by Paul Frees) replacing Edgar Bergen (and the sassy comments of his ventriloquist dummy Charlie McCarthy). Another version of Beanstalk replaced Bergen with narration by Sterling Holloway, as a stand-alone short in such venues as the 1980s TV show, Good Morning, Mickey!. This short was one of many featured in Donald Ducks 50th Birthday.

In 1982, Fun and Fancy Free was released in its entirety on VHS. It was re-released on VHS in 1997 and 2000. It was released on   (Volume 1).
 Walt Disney Mini Classics line. In this case, Bongo is similar to the one that aired on the anthology series, in a 1955 episode, which uses Jiminy Crickets narration and singing replacing Dinah Shores. Similarly, the Ludwig Von Drake version of Mickey and the Beanstalk was released in the Mini Classics. This version was then re-released in 1993, as part of List of Walt Disney and Buena Vista video releases#Disneys Favorite Stories|Disneys Favorite Stories collection. "Fun and Fancy Free" was released in a 2-Movie collection Blu-ray with The Adventures of Ichabod and Mr. Toad on August 12. 

===Alternate versions===
The video and TV releases of Mickey and the Beanstalk have different edits in many parts:

* In the TV version the opening scene where "Happy Valley" is shown being developed as a vision is absent.
* In some VHS releases,  the scream of the harp is deleted when the giant kidnaps her.
* In the TV version, the scene where Mickey, Donald and Goofy are walking through the giants footsteps is deleted.
* In some VHS releases, the dragonfly scene was shortened to the part where the fish eats it. The reason is unknown, but probably due to references to World War II.
* In some VHS releases, the clip of Goofy diving into the gelatin to retrieve his hat, was shortened to him diving into the walnut bowl.
* The TV version had  music added to some parts .
* In the theatrical version, when Willie awakens to chase Mickey and the others, Luana is heard saying, "Oh!". This was edited out of the TV version, but some VHS releases left it in.

==See also==
* List of animated feature films
* List of package films
* The Adventures of Ichabod and Mr Toad

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 