RV (film)
{{Infobox film
| name           = RV
| image          = Rv-movieposter.jpg
| caption        = Theatrical release poster
| director       = Barry Sonnenfeld
| producer       = Lucy Fisher Douglas Wick
| writer         = Geoff Rodkey Joanna "JoJo" Levesque Josh Hutcherson
| music          = James Newton Howard Fred Murphy
| editing        = Kevin Tent
| studio         = Relativity Media Red Wagon Entertainment Intermedia Films IMF
| distributor    = Columbia Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $50 million    
| gross          = $87.5 million {{cite web|title=R.V.
|url=http://www.the-numbers.com/movies/2006/RV.php}}  
}}
 road comedy Joanna "JoJo" Levesque, Erika-Shaye Gair, Josh Hutcherson, Chloe Sonnenfeld, Hunter Parrish, Alex Ferris, Will Arnett, Brendan Fletcher, Matthew Gray Gubler, Barry Sonnenfeld, Richard Ian Cox, Rob LaBelle, Brian Markinson and Ty Olsson. It was released on April 28, 2006, in North America. The film was released on DVD and Blu-ray Disc on August 15, 2006.

==Plot== weightlifter who Hip hop. RV from the dodgy dealer Irv (Barry Sonnenfeld) and tells his family they are traveling to the Rocky Mountains|Rockies.

On their trip, Bob and his family encounter many mishaps. These include him damaging the parking brake, crashing into and running over objects such as shopping carts, flushing out a trio of raccoons with a stink bomb, and fixing a clogged sewage system. Along the way, they meet another traveling family, the Gornickes, consisting of Travis (Jeff Daniels), Mary Jo (Kristin Chenoweth), and their children, Earl (Hunter Parrish), Billy (Alex Ferris), and Moon (Chloe Sonnenfeld). Earl develops a romantic interest in Cassie and Carl starts to like Moon, but thinking that the Gornickes are too strange for them, Bob and Jamie decide to ditch them; when the Gornickes reappear at another stop, the Munros believe they are stalking them.

Meanwhile, to disguise his business trip, Bob tries to e-mail a proposal outline from his laptop, working in restrooms; eventually, a hitchhiker steals it, leaving him with only a BlackBerry PDA, which he does manage to use to compose and wirelessly send his proposal to his company. The Gornickes then recover his stolen laptop after picking up the same hitchhiker, and pursue to return it to him. 

Eventually the Munros begin to enjoy their vacation. In order to attend the merger meeting, though, Bob distracts his family by faking illness and sends them on a hike. The meeting with Alpine Soda is a success, but Bob is invited to talk to the whole company again the next day. Rushing back to his family in the RV, he takes a treacherous 4 wheel drive trail, and gets the huge vehicle stuck atop a jutting boulder in the middle of it. He eventually manages to dislodge it from there by getting on the front and rocking it until it eventually wobbles and tips forward enough to slide down from atop the boulder. Now riding on the front while it is traveling at a frenzied pace, he barely manages to return to his family in time, succeeding in fooling them, but while he is attempting a similar ruse the next day, the parking brake fails again and the RV rolls into a lake. He lets it slip about the real intentions of the vacation, and his family is upset that he would use them like that. Still needing to get to the meeting, he retrieves one of his familys bicycles from the lake and pedals off. Jamie, Cassie, and Carl are then picked up by the Gornickes, and soon realize how well they get along, when Bob appears again, climbing atop the moving bus. Bob apologizes to his family, and they in turn apologize to him for their selfishness and reveal that they love him more than the lifestyle his job gives them. Bob is just about to blow off the meeting when it turns out that hes right outside the headquarters.
 epiphany and Route 66" (RV Style).

==Cast==
* Robin Williams as Bobby "Bob" Munro
* Jeff Daniels as Travis Gornicke
* Cheryl Hines as Jamie Munro
* Kristin Chenoweth as Mary Jo Gornicke Joanna "JoJo" Levesque as Cassie Munro
** Erika-Shaye Gair as 5-year-old Cassie Munro 
* Josh Hutcherson as Carl Munro
* Chloe Sonnenfeld as Moon Gornicke
* Hunter Parrish as Earl Gornicke
* Alex Ferris as Billy "Bill" Gornicke
* Will Arnett as Todd Mallory
* Brendan Fletcher as Howie
* Matthew Gray Gubler as Joseph "Joe" Joe
* Barry Sonnenfeld as Irv
* Richard Ian Cox as Laird (as Richard Cox)
* Rob LaBelle as Larry Moiphine
* Brian Markinson as Garry Moiphine 
* Ty Olsson as Diablo Pass Officer

==Production==
The film began principal photography in the Vancouver area and southern Alberta on May 25, 2005 and finished filming in December 2005.

==Soundtrack==
The score was written by   (keyboards), Russ Kunkel (drums), Ray Herndon (guitar), Viktor Krauss (bass), and Buck Reid (pedal steel). Alvin Chea, vocalist from Take 6, provided solo vocals. Additional music was provided by Stuart Michael Thomas and Blake Neely.

Several songs were featured prominently in the film including: "GTO", "Route 66", "Cherry Bomb", and "Stand By Your Man".

==Reception==
RV received mostly negative reviews from critics. On Rotten Tomatoes, the film holds a rating of 23%, based on 121 reviews, with an average rating of 4.2/10. The sites consensus reads, "An unoriginal and only occasionally funny family road-trip movie, RV is a mediocre effort that not even the charisma of Robin Williams can save."  On Metacritic, the film has a score of 33 out of 100, based on 28 critics, indicating "generally unfavorable reviews". 

Justin Chang of Variety (magazine)|Variety said "RV works up an ingratiating sweetness that partially compensates for its blunt predictability and meager laughs."  Roger Ebert, writing for the Chicago Sun-Times, said "There is nothing I much disliked but little to really recommend." 
 Worst Supporting Actress. 

{| class="wikitable"
|-
!Award
!Category
!Nominee
!Result
|- Golden Raspberry Award Worst Excuse for Family Entertainment
| 
|- Golden Raspberry Worst Supporting Actress Kristin Chenoweth
| 
|- Young Artist Award Best Performance in a Feature Film - Leading Young Actor Josh Hutcherson
| 
|}

==Box office==
RV grossed $71.7 million in America and $15.8 million in other territories for a total gross of $87.5 million, against its $50 million budget.

In its opening weekend, the film finished number one at the box office with $16.4 million. 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 