Coal Miner's Daughter (film)
 
{{Infobox film
| name           = Coal Miners Daughter
| image          = Coal_miners_daughter.jpg
| image_size     = 215px
| caption        = Theatrical release poster
| director       = Michael Apted
| producer       = Bernard Schwartz Tom Rickman
| based on       =  
| starring       = Sissy Spacek Tommy Lee Jones Beverly DAngelo Levon Helm
| music          = Owen Bradley
| cinematography = Ralf D. Bode Arthur Schmidt Universal Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget          = $15 million  
| gross          = $67,182,787
}}
Coal Miners Daughter is a 1980 biographical film which tells the story of country music legendary singer Loretta Lynn. It stars Sissy Spacek as Loretta, a role that earned her the Academy Award for Best Actress. Tommy Lee Jones as Lorettas husband Mooney Lynn, Beverly DAngelo and Levon Helm also star. The film was directed by Michael Apted. 

Levon Helm (drummer for the rock group The Band) made his screen debut as Lorettas father, Ted Webb. Ernest Tubb, Roy Acuff, and Minnie Pearl all make cameo appearances as themselves. 

The film was adapted from Loretta Lynns 1976 autobiography written with George Vecsey. At the time of the films release, Loretta was 48 years old.

==Synopsis==
  Oliver Vanetta (Doolittle) "Mooney" Lynn (Tommy Lee Jones) when she is 15 years old and he was 22 years old. A mother of four by the time she is 19 (and a grandmother by age 29), Loretta begins singing the occasional songs at local honky-tonks on weekends as well as making the occasional radio appearance.

At the age of 25, Norm Burley—the owner of Zero Records, a small Canadian record label—hears her sing during one of her early Northern Washington radio appearances. Burley gives the couple the money needed to travel to Los Angeles to cut a demo tape from which her first single, "Honky Tonk Girl", would be made. After returning home from the sessions, Mooney suggests that they go on a promotional tour to push the record. He takes his own publicity photo, and spends many late nights writing letters to show promoters and to radio disc jockeys all over the South. After Loretta receives an emergency phone call from her mother telling her that her father had died, she and Mooney hit the road with records, photos, and their children. The two embark on an extensive promotional tour of radio stations across the South.

En route, and unbeknownst to the pair, Lorettas first single, "Honky Tonk Girl", hits the charts based on radio and jukebox plays, and earns her a spot on the Grand Ole Opry. After seventeen straight weekly performances on the Opry, she is invited to sing at the Ernest Tubb Record Shop Midnite Jamboree after her performance that night. Country superstar Patsy Cline, one of Lorettas idols, who had recently been hospitalized from a near-fatal car wreck, prompting Loretta to dedicate Patsys newest hit "I Fall to Pieces" to the singer herself as a musical get-well card. Cline listens to the broadcast that night from her hospital room and sends her husband Charlie Dick down to Tubbs record shop to fetch Loretta so the two can meet. A long and close friendship with Patsy Cline follows, and ends only by the tragic death of her idol in a plane crash on March 5, 1963.

Extensive touring, keeping up her image, overwork, and a great deal of stress (from trying to keep her marriage and family together) cause her a nervous breakdown. However, after a year off at her ranch, in Hurricane Mills, Tennessee, Loretta goes back on the road in fine form and becomes the First Lady of Country Music.
 Coal Miners Daughter", her signature song, to a sold-out audience.

==Cast==
* Sissy Spacek as Loretta Lynn Doolittle Lynn
* Beverly DAngelo as Patsy Cline
* Levon Helm as Ted Webb
* Phyllis Boyens as Clary Webb
* Bob Hannah as Charlie Dick
* William Sanderson as Lee Dollarhide
* Ernest Tubb as himself
* Roy Acuff as himself
* Minnie Pearl as herself

==Production== DVD audio commentary for the Collectors Edition of the film. Initially, Spacek was reluctant to participate, and asked to do her own singing in the film in hopes of scaring off the studio from pursuing her for the role. At the time that Lynn prematurely announced on The Tonight Show Starring Johnny Carson that "Sissy Spacek is going to play me," the actress was torn between friends who advised her to do Lynns film and those who advised her to choose instead a Nicolas Roeg project due to start filming at the same time. Talking it over with her mother-in-law that evening, Spacek was advised to pray for a sign, which she did. She and her husband subsequently went for a drive in his mothers car, where the radio was tuned to a classical music station that changed formats at sunset every evening. As the couple pulled out of the parking garage, the title line of the song "Coal Miners Daughter" sallied forth from the radio. Sissy Spacek and Michael Apted. Feature commentary track, Coal Miners Daughter 25th Anniversary/Collectors Edition, 2005. 

==Release==

===Critical reception===
Upon its release in 1980, the film received unanimous critical praise. It currently has a perfect "Fresh" score of 100% on Rotten Tomatoes with 14 "Fresh" reviews and no "Rotten" ones.  Variety (magazine)|Variety called it "a thoughtful, endearing film charting the life of singer Loretta Lynn from the depths of poverty in rural Kentucky to her eventual rise to the title of queen of country music."  Roger Ebert stated that the film "has been made with great taste and style; its more intelligent and observant than movie biographies of singing stars used to be." 

===Awards=== Best Art Best Cinematography, Best Film Best Picture, Best Sound Roger Heman Best Writing, Screenplay Based on Material from Another Medium.    

For her performance, Spacek won an Academy Award, as well as "Best Actress" awards from the Golden Globes, the National Board of Review, the Los Angeles Film Critics Association, the New York Film Critics Circle, and the Kansas City Film Critics Circle. Her co-star Beverly DAngelo, who played Lorettas mentor, Patsy Cline, also chose to do her own singing rather than lip-synching; she was nominated for a Golden Globe, as was Tommy Lee Jones.

===Home media===
* This film was released on LaserDisc on two separate releases. The first release was in May 1980, and the extended play version was released in July 1981. These releases were both made by MCA DiscoVision.
* The film was released in the VHS format in the 1980s by MCA Home Video and on March 1, 1992 by MCA/Universal Home Video.
* On September 13, 2005, Universal released a 25th Anniversary Edition on DVD, in widescreen (1.85:1) format and featuring the music tracks remixed to 5.1 Dolby Digital stereo, leaving the dialogue and effects tracks as they were on the original mono soundtrack from 1980.
* That same DVD was included in a 4-pack DVD set that also included Smokey and the Bandit, The Best Little Whorehouse in Texas, Fried Green Tomatoes.
* On January 7, 2014, Universal released the film on Blu-ray.

==Soundtrack==
{{Infobox album
| Name        = Coal Miners Daughter: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = Coal Miners Daughter.png
| Released    = March 7, 1980
| Recorded    = 1980   Bradleys Barn    (Mt. Juliet, Tennessee)     Country
| MCA Nashville
| Producer    = Owen Bradley
}}
{{Album ratings
| rev1= AllMusic 
| rev1Score =     
}} MCA Nashville label.    It included music by Beverly DAngelo, Levon Helm, and Sissy Spacek except for the "End Credits Medley" and material by other artists which were not under contract to MCA. RIAA on cassette tape,  and Compact Disc|CD.   Levon Helms Blue Moon of Kentucky was released as a single on 7" vinyl, both as a double-A-side and also with a non-album track also sung by Helm, Allen Toussaints Working in the Coal Mine, on the B-side. 

{{tracklist
| writing_credits = yes
| extra_column    = Performer The Titanic
| writer1         = A.P. Carter, Sara Carter, Maybelle Carter
| extra1          = Sissy Spacek
| length1         = 2:29
| title2          = Blue Moon of Kentucky
| writer2         = Bill Monroe
| extra2          = Levon Helm
| length2         = 2:51
| title3          = There He Goes Eddie Miller, Durwood Haddock, W.S. Stevenson
| extra3          = Sissy Spacek
| length3         = 2:11
| title4          = Im a Honky Tonk Girl
| writer4         = Loretta Lynn
| extra4          = Sissy Spacek
| length4         = 2:22
| title5          = Amazing Grace
| writer5         = John Newton
| extra5          = Funeral Guests
| length5         = 2:08
| title6          = Walkin After Midnight
| writer6         = Donn Hecht, Alan Block
| extra6          = Beverly DAngelo
| length6         = 2:21 Crazy
| writer7         = Willie Nelson|H.W. Nelson
| extra7          = Beverly DAngelo
| length7         = 2:45
| title8          = I Fall to Pieces
| writer8         = Hank Cochran, Harlan Howard
| extra8          = Sissy Spacek
| length8         = 2:48 Sweet Dreams
| writer9         = Don Gibson
| extra9          = Beverly DAngelo
| length9         = 2:37
| title10         = Back in Babys Arms Bob Montgomery
| extra10         = Sissy Spacek, Beverly DAngelo
| length10        = 2:10
| title11         = Ones on the Way
| writer11        = Shel Silverstein
| extra11         = Sissy Spacek
| length11        = 2:42 You Aint Woman Enough To Take My Man
| writer12        = Lynn
| extra12         = Sissy Spacek
| length12        = 2:18
| title13         = Youre Lookin at Country
| writer13        = Lynn
| extra13         = Sissy Spacek
| length13        = 2:26 Coal Miners Daughter
| writer14        = Lynn
| extra14         = Sissy Spacek
| length14        = 3:04
}}

;Charts and certifications
 
 

;Weekly charts
{| class="wikitable sortable"
! Chart (1980)
! Peak position
|-
| Canada Country Albums (RPM (magazine)|RPM)   
| align="center"| 1
|-
| Canada Top Albums (RPM (magazine)|RPM)
| align="center"| 23
|-
| US Top Country Albums (Billboard (magazine)|Billboard)
| align="center"| 2
|-
| US Billboard 200|Billboard 200
| align="center"| 40
|-
|}
 

;Certifications
{| class="wikitable"
|- Country
!scope="col"|List Certification
|- Recording Industry United States
| Gold 
 
|}

 
 
 
 
 

==Adaptation==
On May 10, 2012, at the Grand Ole Opry, Lynn announced that Zooey Deschanel will play her in a Broadway musical adaptation. 

One episode of The Simpsons, entitled "Colonel Homer", is partly based on this film. The episode also stars Beverly DAngelo as cocktail waitress Lurleen Lumpkin, who happens to have a beautiful country singing voice.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 