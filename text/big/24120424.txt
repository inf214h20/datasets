Tenali Ramakrishna (film)
 

{{Infobox film
| name           = Tenali Ramakrishna
| image          =Tenali Ramakrishna.jpg
| image_size     =
| caption        =
| director       = B. S. Ranga
| producer       = B. S. Ranga
| writer         = Samudrala Raghavacharya (Telugu) Kannadasan (Tamil)
| story          = C. K. Venkataramaiah
| screenplay     = C. K. Venkataramaiah  
| based on       =   Jamuna Chittor Mikkilineni Mukkamala Sandhya Rajanala Rajanala Vangara Vangara Surabhi Balasaraswati Laxmikantam Ramakoti
| music          = Viswanathan Ramamoorthy
| cinematography = B. S. Ranga
| editing        = P. G. Mohan
| studio         = Vikram Productions
| distributor    = Vikram Productions
| released       = 12 January 1956 3 February 1956 (Tamil)
| runtime        =
| country        = India Telugu Tamil Tamil
| budget         =
}}

Tenali Ramakrishna  is a 1956 Indian bilingual, historical, biographical drama film primarily shot in Telugu cinema|Telugu, and directed by B. S. Ranga. This film was simultaneously made in Tamil and is named as Tenali Raman ( ). N. T. Rama Rao appeared as Srikrishna Devaraya in both films.  Tenali Ramakrishna was played by A. Nageswara Rao in Telugu version while Sivaji Ganesan portrayed the role in Tamil. The plot is based on the play written by C. K. Venkataramaiah.
 All India Certificate of Merit for Best Feature Film at 4th National Film Awards.

==Plot==
The Deccan Sultans of Berar, Ahmednagar, Bidar, Bijapur and Golconda who the splinters from the erstwhile Bahmani Sultnate now unite with the common purpose to defeat of Krishnadeva Raya and the conquest of the prosperous Vijayanagaram. They send their stooge Kanakaraj to assassinate Krishnadeva Raya, but Kanakaraj fails in his mission and is put to death.

Then they planned courtesan Krishnasini. Krishnasini enters Vijayanagaram, and with her acclaimed dancing skills, manages to elicit the notice of the King, a great connoisseur of arts and beauty. She then plays her cards cleverly and besotted by her intelligent repartees and smoldering sensuousness, the susceptible King is soon a puppet in her hands.

Orders are given that anyone who enters their private chamber would be beheaded and the King spends with Krishnasini’s for months. Reports reach the ministers that the Sultans are planning to take advantage of the King’s inaccessibility and launch a combined attack on Vijayanagaram. Worried at the state of affairs, Tenali Raman braves the prohibitory order and enters Krishnasini’s abode dressed as a woman, but all his appeals to the King seem to fall on deaf ears.

Meanwhile Queen Tirumalamba falls seriously sick and the King finally comes out of his daze. Once the King is at his wife’s bedside, Tenali Ramakrishna manages to gain entry into Krishnasini’s house again, this time under the guise of an omniscient saint who assures her that he would bring the King back to her. He catches her red-handed with her gang of spies, and signals to the hidden soldiers to surround her. Realized that the game is up, Krishnasini prefers a dignified death. Shocked to see her stab herself, Timmarusu remonstrates with her that she has acted in haste, for the King would have certainly forgiven her.

==Cast==
*N. T. Rama Rao as Sri Krishna Deva Raya
*A. Nageswara Rao as Tenali Ramakrishna (Telugu) Tenali Raman (Tamil)
*Chittor V. Nagaiah as Timmarusu Jamuna as Kamala
*P. Bhanumathi as Krishnasani (Telugu) Rangasani (Tamil)
*Sandhya as Maharani Tirumaladevi
*M. N. Nambiar as Rajaguru
*Surabhi Balasaraswathi as Radha
*Mukkamala Krishna Murthy as Tatacharyulu
*Mikkilineni Radhakrishna Murthy as Kanakaraju
*Master Venkateswar as Son of Ramakrishna

==Crew==
* Producer: B. S. Ranga
* Production Company: Vikram Productions
* Director: B. S. Ranga
* Music: Viswanathan Ramamoorthy
* Dialogues: Samudrala Raghavacharya (Telugu) / Kannadasan (Tamil)
* Lyrics (Tamil): Kannadasan, M. K. Athmanathan &  Tamizhmannan
* Lyrics (Telugu): Samudrala Raghavacharya & Jayadeva
* Story: C. K. Venkataramaiah
* Art Direction: Ganga & Vali
* Editing: P. G. Mohan
* Choreography: P. S. Gopalakrishnan & A. K. Chopra
* Cinematography: B. S. Ranga
* Audiography: T. S. Rangasamy
* Stunt: None
* Dance: None

==Soundtrack==
The music was composed by Viswanathan-Ramamoorthy.

===Telugu Songs===
* "Jagamula Dayanele Janani Sadashivuni Manoharini" by P. Leela (Background title song)
* "Aakatayi Pilla Mooka Andala Chilaka" (Singer: Ramakoti; Cast: Ramakoti)
* "Chandana Charchita Neelakalebara" by  , Cast: Sandhya) Ghantasala (Lyrics: Samudrala, Cast: Akkineni)
* "Gandu Pilli Menu Marachi Banda Nidura Poyera" by Ghantasala & Chittor V. Nagaiah (Cast: Akkineni and Nagaiah)
* "Ichchakalu Naaku Neeku Inka Elara" by P. Leela (Cast: Balasaraswati)
* "Jhan Jhan Kankanamulu" by Raavu Balasaraswathi|R. Balasaraswathi Devi
* "Kannulu Ninde Kannela Vinna Mannanaleera Raja" by P. Bhanumathi
* "Teerani Naa Korikale Teerenu Ee Roju" by P. Bhanumathi

===Tamil Songs=== Playback singers are T. M. Soundararajan, Ghantasala (singer)|Ghantasala, V. N. Sundharam, P. Leela, P. Suseela, Raavu Balasaraswathi|R. Balasaraswathi Devi & A. P. Komala.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ulagellaam Unatharulaal Malarum || P. Leela || M. K. Athamanathan|| 
|- 
| 2 || Naattu Jananga Adaiyelam || Karikkol Raju || Kannadasan ||
|-  Geetha Govindam ||
|-  Ghantasala || Tamaizhmannan ||
|- 
| 5 || Ulagellaam Unatharulaal Malarum (pathos)  || P. Leela || M. K. Athamanathan ||
|- 
| 6 || Chittu Pole Mullai Mottuppole || A. P. Komala ||  Kannadasan||
|- 
| 7 || Aadum Kalaiyellam Paruva Mangaiyar Azhagu Koorum || P. Leela || Kannadasan || 
|- 
| 8 || Thennavan Thaai Nattu Singaarame || P. Suseela || Kannadasan || 
|- 
| 9 || Thangam Pogum Meni Undhan Sondham Ini || Raavu Balasaraswathi|R. Balasaraswathi Devi || Kannadasan || 
|- 
| 10 || Putrile Pambirukkum.... Kottaiyile Oru Kaalatthile || T. M. Soundararajan & Chittor V. Nagaiah || Kannadasan || 
|- 
| 11 || Kangalil Adidum Penmaiyin Naadagam || P. Bhanumathi || || 
|- 
| 12 || Kannamirandum Minnidum Annam || P. Bhanumathi || Kannadasan || 
|- 
| 13 || Pirandha Naal Mannan Pirandha Naal || P. Bhanumathi || Kannadasan || 
|- 
| 14 || Vinnulagil Minni Varum Thaaragaiye Po Po || P. Bhanumathi || Kannadasan || 
|- 
| 15 || Adari Padarndha || V. N. Sundharam || Kannadasan || 
|- 
| 16 || Ponnalla Porul || V. N. Sundharam || Kannadasan ||  
|- 
| 17 || Kannaa Pinnaa Mannaa || V. N. Sundharam || Kannadasan ||  
|- 
| 18 || Vindhiyam Vadakkaaga || V. N. Sundharam || Kannadasan ||  
|- 
| 19 || Chandhiran Pole || V. N. Sundharam || Kannadasan ||  
|- 
| 20 || Drru Drru Ena Madugal || V. N. Sundharam || Kannadasan ||  
|- 
| 21 || Thaadhi Thoodho Theedhu || V. N. Sundharam || Kannadasan ||  
|}

==Awards== National Film Awards    1956 - All India Certificate of Merit for Best Feature Film (Telugu Version)
** 1956 - National Film Award for Best Feature Film in Telugu

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 