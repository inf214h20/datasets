Ezhavathu Manithan
{{Infobox film
| name           = Ezhavathu Manithan
| image          = EzhavathuManithan.jpg
| director       = K. Hariharan (director)|K. Hariharan
| cinematography = Dharmma DFTech
| writer         = Arunmozhi
| starring       = Raghuvaran Rathna
| producer       = Palai Shunmugham
| distributor    =
| released       = 1982
| runtime        = 125 mins Tamil
| music          = L. Vaidyanathan
}}
 Tamil film directed by K. Hariharan (director)|K. Hariharan. The films score and soundtrack are composed by L. Vaidyanathan. Released in the birth centenary year of Mahakavi Subramania Bharati, all 10 songs in the film are from the works of this Tamil poet. The film met with widespread critical acclaim upon release, winning the National Film Award for Best Feature Film in Tamil and two Tamil Nadu State Film Awards. It was also nominated for the Golden St. George prize at the Moscow International Film Festival.

==Plot==
An engineering college graduate decides to go and work in an industrial factory, however once there starts working against the exploitation and mistreatment of the workers there. His actions eventually lead to a strike by the workers, and the situation looks likely to deteriorate further until a lawyer turns up to reveal hidden agenda of the factorys owners.

==Soundtrack==

Track List 
{| class="wikitable"
|-
! Title !! Artist
|-
| Aaduvome Pazhzhu Paaduvoome || Deepan Chakravarthy, Mathangi, Susheela, Chandilyan
|-
| Achchamillai Achchamillai  || S.P.Balasubrahmanyam
|-
| Endha Neramum || KJ Yesudas
|-
| Kakai Sirakinile || KJ Yesudas  
|-
| Manadil Urudhi Vendum || B Neeraja
|-
| Nallathor Veenaiseithe || Rajkumar Bharathi
|-
| Nenjil Uramum inri || Rajkumar Bharathi
|-
| Odi Velaiyadu Papa || KJ Yesudas, Saibaba
|-
| Senthamizh Naadennum || Susheela
|-
| Veenaiyadi Neeyenakku || KJ Yesudas, B Neeraja  
|-
| Veenaiyadi Neeyenakku  || B Neeraja
|}

==Awards==
The film has been nominated for the following awards since its release:

1983 Moscow International Film Festival (Russia)
* Nominated - Golden Prize - K. Hariharan

The film has won the following awards since its release:
 National Film Awards (India) 
* Won - Silver Lotus Award - Best Regional Film (Tamil) - Ezhavathu Manithan - K. Hariharan.

1983 Tamil Nadu State Film Awards Best Film (Third prize) Best Story Writer

==Trivia==
*Actor Raghuvaran debuted in this film.

==External links==
* 

==References==
 

 
 
 
 
 
 
 
 