Mature Woman: Wife-Hunting
{{Infobox film
| name = Mature Woman: Wife-Hunting
| image = Mature Woman Wife-Hunting.jpg
| image_size = 
| caption = Theatrical poster for Mature Woman: Wife-Hunting (2006)
| director = Yutaka Ikejima   
| producer = 
| writer = Kyōko Godai
| starring = Yuki Mikami
| music = Kazumi Ōba
| cinematography = Shōji Shimizu
| editing = Shōji Sakai
| distributor = Cement Match Shintōhō
| released = June 9, 2006
| runtime = 64 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2006 Japanese pink film directed by Yutaka Ikejima and scripted by Ikejimas wife, Kyōko Godai. It is an erotic suspense-thriller in the style of Alfred Hitchcock|Hitchcock.  At the Pink Grand Prix ceremony it was awarded as the Sixth Best Film of the year. 

==Cast==
* Yuki Mikami (三上夕希): Kanako Shindō
* Itsuka Harusaki (春咲いつか): Kaori Honjō
* Mayuko Sasaki (佐々木麻由子): Yui Takemiya
* Yuria Hidaka (日高ゆりあ): Mio Saeki
* Yasushi Takemoto (竹本泰志): 原田裕作
* Kazu Itsuki (樹かず): Tatsuya Moroboshi
* Yutaka Ikejima (池島ゆたか): Inspector Takasugi
* Guriko Yamanote (山ノ手ぐり子): Manami Nishina
* Kikujirō Honda: Shinichirō Sakisaka

== Availability ==
 
Mature Woman: Wife-Hunting was released theatrically on June 9, 2006.  It was released as Ryoshu on home video in 2005. 

==Bibliography==
*  
*  
*  

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 
 


 
 