The Weight of Chains 2
{{Infobox film name = The Weight of Chains 2 image = The Weight of Chains 2 poster.jpg caption = director = Boris Malagurski
| producer       = Boris Malagurski
| screenplay     =  starring = Noam Chomsky Carla Del Ponte Mlađan Dinkić Vuk Jeremić Diana Johnstone Ivo Josipović Slavko Kulić Miroslav Lazanski Igor Mandić Michael Parenti Oliver Stone R. James Woolsey, Jr.|R. James Woolsey music =  editing = 
| studio         = Malagurski Cinema distributor =  released =   runtime = 124 min country = Canada language = English, Serbian
| budget         = $42,005
}}
The Weight of Chains 2 is a 2014    Directed and produced by Boris Malagurski, the film was released on November 20, 2014 at the Serbian Film Festival at Montecasino in Johannesburg, South Africa.  It is the sequel to 2010s The Weight of Chains. 

== Synopsis == neoliberal economics in the Balkans.     The sequel continues where the The Weight of Chains ended, covering the period from 2000 to the present. 
 former Yugoslavia, from politics, economics, military, culture and education to the media. "Through stories of stolen and sold off companies, corrupt politicians, fictional tribunals, destructive foreign investors and various economic-military alliances, the new film deconstructs modern myths about everything weve been told will bring us a better life", Malagurski told Tanjug.  In a discussion, the filmmaker said "institutions, like the International Monetary Fund, act like hitmen" whose policies "destroyed the economies of many countries". 

In "The Weight of Chains 2", the filmmaker talks about how a shock-economy based on the Washington Consensus led many countries, to the edge of existence. Malagurski provides, what he describes as, evidence of this in Serbia, but also examples in Argentina and Iceland which confronted what Malagurski describes as the "neoliberal dictate" and, as a result, are better off today. http://m.vesti-online.com/Dijaspora/drzava/Nemacka/Vesti/459852/Buntom-protiv-lanaca 

== Production ==
The film was produced with support from the Office for Kosovo and Metohija,  the Serbian Ministry of Youth and Sport, the Belgrade Secretariat for Culture, Radio Television of Serbia,   Subotica.com  as well as individual donors worldwide. 

== Screenings ==
Following the World Premiere in Johannesburg, "The Weight of Chains 2" had its European Premiere on November 29, 2014 at the Swedish Film Institute in Stockholm, Sweden, as a part of BANEFF - the Balkan New Film Festival.   In an interview, Malagurski said that the film was shown at the Monterrey Institute of Technology and Higher Education  and the film was also screened at the Museo Nacional de las Culturas  in Mexico City, Mexico. Subsequent screenings took place in Vancouver  and Toronto  in Canada, Innsbruck  in Austria and Stuttgart  and Berlin  in Germany. The Balkan premiere took place in Banja Luka,   while the Serbian premiere was in Subotica.  The film opened in cinemas in Belgrade,  Vienna,  Novi Sad,  Linz,  Niš,  Kraljevo  and was shown in Čačak,  Kozarska Dubica,  Teslić,  Gračanica, Kosovo|Gračanica  and Sombor.  The film is also due to be shown as part of the 2015 BANEFF in Oslo, Norway. 

== Critical Response ==
Serbian film critic Dubravka Lakić wrote a review of the film in Politika following the Belgrade premiere of "The Weight of Chains 2", in which she wrote that "its not necessary for you to agree with Malagurskis ideology to notice that he indisputably made a full-blooded and, for our cinematic terms, unsurpassed documentary". She described the film as "exciting and dynamic", adding that, in regards to both content and form, it is a "complex film", which could have been "even better if it had been more concise". Lakić points out that, by "confronting claims made by the interviewees", the message of the film is clearly presented - "resistance to neoliberalism is no longer a matter of ideology, but of common sense". 

== Interviewees ==
The interviewees in the film include:    Politika    
* Noam Chomsky – American linguist, philosopher, cognitive scientist, logician, political commentator and activist.
* Carla Del Ponte – former Chief Prosecutor of two United Nations international criminal law tribunals. Governor of the National Bank of Serbia from 2000 to 2003
* Vuk Jeremić – Serbian politician, former President of the United Nations General Assembly. Western foreign policy.
* Ivo Josipović – Croatian politician, President of Croatia from 2010 to 2015.
* Slavko Kulić – Croatian scientist and economist concerned with the sociology of international relations. commentator for the Belgrade daily Politika.
* Igor Mandić – Croatian writer, literary critic, columnist and essayist.
* Michael Parenti – American political scientist, historian, and cultural critic.
* Oliver Stone – American film director, screenwriter and producer,
* R. James Woolsey, Jr.|R. James Woolsey – national security and energy specialist and former Director of Central Intelligence who headed the Central Intelligence Agency from 1993 until 1995.

Two of the interviewees were featured only in the film trailer: 
* Branimir Brstina - Serbian actor.
* Michael Ruppert – American author, investigative journalist, political activist and peak oil awareness advocate.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 