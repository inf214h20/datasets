Aag (1948 film)
{{Infobox film
| name           = Aag
| image          = Aag_poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Raj Kapoor
| producer       = Raj Kapoor
| writer         = Inder Raj Anand
| narrator       = 
| starring       = Raj Kapoor Nargis Premnath
| music          = Ram Ganguly
| cinematography = 
| editing        = 
| distributor    = R.K. Films
| released       = 6 August 1948
| runtime        = 130 mins
| country        = India
| language       = Hindi/ Urdu
}}

Aag ( ;  ) is a 1948 Bollywood film which is produced, directed by and stars Raj Kapoor. The film marked the debut of Raj Kapoor as producer and director and was the first film produced by his R.K. Banner. Nargis, Premnath, Nigar Sultana and Kamini Kaushal also starred in supporting roles. Raj Kapoors youngest brother Shashi Kapoor appeared as a child artist in this film playing the younger version of his character (Kewal). This was the first film in which Raj Kapoor and Nargis appeared together.

==Plot==
Kewal (Raj Kapoor) reluctantly accepts his father (Kamal Kapoor)s demands to continue the family tradition by studying law and become a successful lawyer just like him.  However due to a lack of interest in becoming a lawyer and more interest in opening up his own theatre company, he fails his law exams and is thrown out of the house by his father. Luckily he finds a patron of the arts Rajan, (Premnath) who is the owner of a theater  company, that has closed down. A childhood romance with a girl named Nimmi haunts his fantasies, and Kewal searches for her in other women, even renaming them after his former sweetheart. With a theater, a play and a feminine image in his mind, he discovers a woman made homeless by Partition (Nargis) and the play of his dreams can at last be written and performed.

==Cast==
*Raj Kapoor .... Kewal Khanna
*Nargis ... Nimmi
*Kamini Kaushal ...  Miss Nirmala
*Nigar Sultana ...  Nirmala
*Kamal Kapoor ... Kewal Khannas father
*Premnath ...  Rajan
*Shashi Kapoor .... Young Kewal Khanna

== Music ==
The music for this film was composed and conducted by Ram Ganguly. For his next film Barsaat (1949 film)|Barsaat, Raj Kapoor employed Shankar-Jaikishan. This duo went on to produce memorable melodies for the RK banner.

== Box Office ==

This film was not a major success at box office. As a director Raj Kapoor got his first major hit next year(1949) in the form of Barsaat (1949 film).

== External links ==
*  

 

 
 
 
 
 
 

 