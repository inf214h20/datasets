En Uyir Kannamma
{{Infobox film
| name           = En Uyir Kannamma
| image          = En_Uyir_Kannamma.jpg
| image_size     =
| caption        =
| director       = Sivachandran
| producer       = S. K. Shankaralinkam
| writer         = 
| screenplay     =  Radha Lakshmi Lakshmi S. S. Chandran
| music          = Ilayaraja
| cinematography = 
| editing        = 
| studio         =  S.K.S.Film Creations
| distributor    =  S.K.S.Film Creations
| released       = 1988
| country        = India Tamil
}}
 1988 Cinema Indian Tamil Tamil film, Lakshmi and S. S. Chandran in lead roles. The film had musical score by Ilayaraja.  

==Cast==
*Prabhu Radha
*Lakshmi Lakshmi
*Sivachandran
*S. S. Chandran
*Santhana Bharathi

==Soundtrack==
The music was composed by Ilaiyaraaja. {{cite web|url=http://play.raaga.com/tamil/album/En-Uyir-Kannamma-T0002685|title=En Uyir Kannamma Songs|accessdate=2014-09-18|publisher=raaga.co
m}}  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Chithra || Vaali || 01.23 
|-  Vaali || 04.23 
|-  Vaali || 03.48 
|-  Chithra || Vaali || 04.35 
|-  Chithra || Vaali || 04.30 
|-  Vaali || 04.42 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 