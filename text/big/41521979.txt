Hello Yaar Pesurathu
{{Infobox film
| name = Hello Yaar Pesurathu
| image = HelloYaarPesurathu.jpg
| caption = LP Vinyl Records Cover
| director = Ramarajan
| writer = Suresh Jeevitha
| producer = Shanmugharajan
| music = Illayaraja Gangai Amaran
| editor =
| released = 1985
| runtime = Tamil
| budget =
}}
 1985 Cinema Indian feature directed by Suresh and Jeevitha in lead roles.    This is one of the rare movies which had music given by Maestro Illayaraja and his brother Gangai Amaran together.

==Cast==
 Suresh
*Jeevitha

==Soundtrack ==
{{Infobox album Name     = Hello Yaar Pesurathu Type     = film Cover    = Released =  Music    = Illayaraja Gangai Amaran Genre  Feature film soundtrack Length   = 21:26 Label    = Echo
}}

The music composed by Ilaiyaraaja and Gangai Amaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Naal Nalla Naal || S. Janaki|| Vairamuthu
|-
| 2 || Hello Aasai Deepam || S. Janaki, Deepan Chakravarthy || Kuruvikkarambai Shanmugham
|-
| 3 || Vaanilae Oru Thenila || S. Janaki, Malaysia Vasudevan || Kavingar Muthulingam
|-
| 4 || Pachchai Paasi || Malaysia Vasudevan, S. P. Sailaja || Gangai Amaran
|}

==References==
 

==External links==

 
 
 
 
 


 