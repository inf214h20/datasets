A Warrior's Heart
{{Infobox film
| name = A Warriors Heart
| image = A-warriors-heart-film.jpg
| caption = Theatrical release poster
| director = Michael F. Sears Martin Dugard
| based on = 
| starring = Kellan Lutz   Adam Beach   Ashley Greene   Gabrielle Anwar
| producer =  Marc Spizzirri, Steven Istock, Ed Richardson
| music = Alec Puro
| cinematography = Thomas L. Callaway
| editing = Ellen Goldwasser
| distributor = Camelot Entertainment
| released =  
| runtime = 95 min
| country = United States
| language = English
| budget = $2.500.000 
| gross = 
}} Martin Dugard.

==Synopsis==
Star Lacrosse player Conor Sullivan (Kellan Lutz) is not excited about moving to an unknown town and being the new kid at high school. He has a new love interest Brooklyn (Ashley Greene), but he struggles to find a meaning to his life.

Conors Marine father is redeployed into Iraq where he dies in combat leaving Conor in shock and denial as he starts acting out in self-destructive ways. This greatly worries his mother Claire (Gabrielle Anwar). There is also a violent on-field clash with a long-time nemesis (Chord Overstreet) and a vandalism incident that lands him in a jail cell and finally gets him kicked off the team. To regain his obvious passion for the sport, he goes for arduous training in a wilderness Lacrosse camp. The camp is under the tutelage of his dead fathers old combat buddy, Sgt. Major Duke Wayne (Adam Beach), who opens Conors eyes to the true meaning of maturity, sportsmanship and manhood.

==Cast==
;Main
*Kellan Lutz as Conor Sullivan
*Adam Beach as Sgt. Major Duke Wayne
*Ashley Greene as Brooklyn
*Gabrielle Anwar as Claire Sullivan
;Others
*Chord Overstreet as Dupree
*William Mapother as David Milligan Aaron Hill as Joe Bryant Chris Potter as Seamus Sullivan
*Jay Hayden as JP Jones
*Ridge Canipe as Keegan Sullivan
*Daniel Booko as Powell
*JT Alexander as Sierra Lacrosse Player #29
*Alex Rose Wiesel as Girls Lacrosse Player #12
*Bryan Lillis as Riggins
*Hymnson Chan as Brierfield Player
*Lauren Minite as Charlie
*Jim Pacitti as Coach Jarvis
*Basilina Butler as Parent
*Diego Acuna as West Coast Referee
*Randall May as East Coast Referee

==Screenings==
The film was presented at the 2011 Cannes Film Festival and released in the United States on December 2, 2011. Much of the filming was done at Mayfield Senior School in Pasadena, CA

==Critical reception==
The film was met with mostly negative reviews from mainstream critics. Review aggregator Rotten Tomatoes gave it a "rotten" score of 18% based on reviews from 11 critics,  but received a 54% for the audience rating.

The film has a Netflix rating of 3.9/5.

==References==
 

==External links==
* 
* 
* 

 
 
 
 