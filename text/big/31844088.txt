Sammohanam
{{Infobox film
| name           = Sammohanam
| image          = Sammohanam.jpg
| caption        =
| alt            =
| director       = C. P. Padmakumar
| producer       = C.P. Padmakumar
| writer         = Balakrishnan Mangad Murali  Archana
| music          =  Illayaraja
| cinematography = Radhakrishnan M J
| editing        = K.R. Bose
| studio         = Cinevalley Motion Pictures
| distributor    =
| released       =  
| runtime        =1 hour 44 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Archana in the lead roles. The film received the Best of the Fest Award at the Edinburgh International Film Festival in 1995.  The film is based Rithubhedangal, a story written by Balakrishnan Mangad.  Illayaraja done this film without charging any fees as he was impressed with its storyline. 

==Plot==
Sammohanam tells the story of a mysteriously seductive girl, Pennu (Archana (actress)|Archana) in a quiet village to which she has temporarily relocated, in search of her missing grandfather, Karuvan Valyachan. She creates a strange chasm among male folks, breaking marriages and old friendships. Chandu  (Murali (Malayalam actor)|Murali), a wealthy farmer who owns paddy fields was the first to fall for her charms and even breaks up with his wife Jaanu (Bindu Panicker) and family. Later when Pennu started wooing Chindan (Balakrishnan), a jaggery mill owner and a close friend of Chandu, he cannot stand it and attacked Chindan with a sickle, chopping off his earlobe. Then comes Ummini (Nedumudi Venu), a travelling seller with his pair of horses. He is an old alley of Pennus grandfather. Soon he too got enchanted and fell for Pennus charm. Ambu (Sarath), a helper of Chandu also has a hidden passion for Pennu, even though he is much younger to Pennu. Chandu sees Ummini and Pennu together and becomes angry and fought Ummini with his sickle. Ambu tries to intervene and gets accidentally knifed to death by Chandu. Chandu out of guilt and grief commits suicide by jumping in to a waterfall. Finally Pennu leaves the village alone, after torching her house, having fractured the village community with her sexuality.

==Cast== Murali as Chandu Archana as Pennu
* Nedumudi Venu as Ummini
* Sarath as Ambu
* Radhakrishnan as Chindan
* K. P. A. C. Lalitha as Chiruthamma
* Bindu Panicker as Jaanu (wife of Chandu)
* Kukku Parameswaran as Kunjani (sister of Chindan)

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 