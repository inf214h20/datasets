Campus Man
{{Infobox film
| name           = Campus Man
| image          = Campus Man poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ron Casden  Jon Landau
| screenplay     = Geoffrey Baere Matt Dorff 
| story          = Matt Dorff Alex Horvat
| starring       = John Dye Steven Lyon Kim Delaney Kathleen Wilhoite Miles OKeeffe Morgan Fairchild
| music          = James Newton Howard
| cinematography = Francis Kenny
| editing        = Steve Polivka 
| studio         = RKO Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $989,528 
}}

Campus Man is a 1987 American comedy film directed by Ron Casden and written by Geoffrey Baere and Matt Dorff. The film stars John Dye, Steven Lyon, Kim Delaney, Kathleen Wilhoite, Miles OKeeffe and Morgan Fairchild.  The film was released on April 10, 1987, by Paramount Pictures.

==Plot==
 

== Cast ==
 
*John Dye as Todd Barrett
*Steven Lyon as Brett Wilson 
*Kim Delaney as Dayna Thomas
*Kathleen Wilhoite as Molly Gibson
*Miles OKeeffe as Cactus Jack
*Morgan Fairchild as Katherine Van Buren
*John Welsh as Professor Jarman
*Josef Rainer as Charles McCormick
*Richard Alexander as Mr. Bowersox
*Steve Archer as Coach Waters
*Eden Brandy as Party Girl
*Marty Miller as Party Animal
*Gayn Erickson as Student #1
*Zibby Miles as Student #2
*Isabelle Bailey as Student #3
*Paul Mancuso as Student #4
*Jason Scott as Checker
*Bob G. Anthony as Loan Officer	
*Deborah Dee as Graffittis Girl 
*Lorin Young as Muffy
*Cheli Ann Chew as Buffy 
*Mark Curtis as Newscaster
*Bill Stull	as Newscaster 
*Tracy Tanen as Apartment Girl
*Ivan E. Schwarz as Purvis
*James Sanich as Uniformed Cop
*Danny Sullivan as Financial Manager
*Stuart Grant as Mr. Jackson
*Janet Osgood as Announcer
*Tiny Wells as Thug #1
*Henry Tank as Thug #2
*Linda Williams as TV Reporter
*Val Ross as Bookstore Manager
*Holly Davis  as Girl in Bookstore
*Julie Lamm as Girl in Stands
*Larry Swanson as Nerd #1
*Michael Byun as Nerd #2
*Michael Spiller as Undercover Cop
*Ellen Ruffalo as Fashion Editor
*Charley Gilleran as Telephone Repairman
*Kristen Danielson as Usherette
*Scott P. Anthony as Diver
*Duwan Erickson as Diver
*Randy Mastey as Diver
*Ron Piemonte as Diver
*Bill Travis as Diver
*Jim Sullivan as Diver
*Steve Voelker as Calendar Man
*Christopher Ambrose as Calendar Man
*James A. Quistorff as Calendar Man
*D. Sidney Potter as Calendar Man
*Robert Donahue as Calendar Man
*Ricky Austin Hill as Calendar Man
*David T. Moran as Calendar Man
*John Jaqua as Calendar Man
*Brad Huestis as Calendar Man
*Guy R. Vick as Calendar Man
*Mark P. Bastin as Calendar Man 
 

==Reception==
The film grossed $319,218 in its opening weekend. 	

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 