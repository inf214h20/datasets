Penn & Teller's Cruel Tricks for Dear Friends
 
{{Infobox film
| name           = Penn & Tellers Cruel Tricks for Dear Friends
| image          = CruelTricksForDearFriends.jpg
| caption        = Video cover
| director       = Art Wolff
| producer       = Timothy Marx Cindy Valk
| writer         = Penn & Teller Eddie Gorodetsky
| editor         = Bill Daris Harvey Kopel
| cinematography = Robert Leacock
| starring       = Penn & Teller Lydia Lunch Alan Hunter Marc Garland
| music          = Penn & Teller Timothy Marx The Residents
| studio         = The Mofo Video Corp. Lorimar Home Video
| released       = July 10, 1987
| runtime        = 59 minutes
| country        = United States English
}}
 Lorimar Home Video, the tape features seven different swindles or tricks that the home viewer can use to fool their friends. The tape was a companion piece to their best-selling book of the same name, released two years later. All of the tricks involve using a portion of the videotape.

==Tape Format== opening logo:

Penn & Teller assume no responsibility for the misuse of these materials and will not be held liable to anyone who loses money from the illegal misuse of this tape.

After, Penn & Teller introduce themselves, making a huge point about how there are roughly 138 suckers to every American who owns a VCR (and can thus buy and use the tape). The only music in the tape is some incidental background music provided by Penn & Teller themselves (Penn on bass guitar, Teller on Electronic keyboard|keyboard). They also filmed the tape almost exclusively in the apartment of co-writer Eddie Gorodetsky.  This was supposedly to help reduce the cost of the tape, which, as Penn points out, retailed at $3.95.

==The Scams==
===The Card Force (no name given)=== card force. Nothing Sacred.  After a TV bumper, a quick segment called "Newsbreak" comes on.  The female newsreader reads a news piece about a new government scandal.  After reading a few lines, the newsreader is given a breaking headline.  She pauses, smiles brightly, holds up a jumbo-sized three of clubs and says "Is this your card?"  The magician is then supposed to take the money, and laugh at his victim.

===Super Kleener===
This trick does not involve cards, but instead involves the user to have paper towels and a bottle of glass cleaner.  The tape also included a sticker which reads "Static Free for TV Screens" to place on the bottle.  The Magician is supposed to start to watch another clip from Nothing Sacred when the Magician is supposed to act like they see a speck of dirt on the screen.  The Magician is then supposed to spray some of the cleaner on a paper towel and after a special moment happens in the movie the magician starts rubbing the screen with the paper towel.  The clip has been digitally altered to look like the image is being torn and distorted like, as Penn puts it, "a watercolor being rubbed with a wet sponge."  The victim is supposed to be shocked into thinking that an over-the-counter cleanser can actually damage an image on a TV screen.  This is not to be done for money, but just for laughs. This trick was demonstrated on an episode of Late Night with David Letterman

===Demon from Hell===
Said to be Penns favorite clip, this trick makes fun of the then-popular claim by Fundamentalist Christians that many popular albums, movies, and TV shows contained backwards messages (referred to as backmasking).  Penn says "this is obviously hysterical grandstanding.  Use your head!  If backwards masking really worked, Ozzy Osbourne would be the Prime Minister of England !"  The magician is then asked to claim that he has found a backwards masking technique in a show hosted by an unnamed Televangelist.  The victim is asked to put some money on it.  The magician pauses the tape, and then is supposed to pretend to play the film backwards.  In actuality the Magician just presses the "play" button .  The footage starts to play in reverse.  The preacher starts to say all manner of Satanic messages such as "Satan is our Lord" and "The screams of torture are the music of my dance". This trick was done to seemingly poke fun at televangelists like Jim Bakker and Jimmy Swaggart.

===Vidi-Kopy===
In "Vidi-Kopy", Penn & Teller claim they have created an experimental tape that can turn a VCR into a digital copier.  After a short description, the Magician starts the tape with what appears to be a computer screen placed on it.  The Magician prepares a "random piece of paper."  A female voice says "Do not touch screen during Vidi-Kopy operation."  After a few seconds of digital flashing, a copy of the image drawn on the paper appears on the screen.  After a fake "save" function, the Magician is then asked to take a dollar bill out of his pocket, and hold it against the screen and an image of the bill appears on the screen, then the magician presses a fake button on the screen which makes the image magnify on the screen.  The Magician then has two options, either turn off the tape and then attempt to sell the fake Vidi-Kopy tape to the victim, or keep the tape rolling for the punch-line.  The Magician is supposed to downplay the warnings about touching the screen and place his bare hand on the screen and press the copy button.  The image flashes and a copy of his hand is supposed to appear on the screen, which flashes violently, the female voice starts saying "System Failure."  The Magician is supposed to scream and act like his hand has been horribly burned.

===Diamond and the Dope=== VJ Alan Alan Hunter.  Then you tell your friend that he will be the Magician for an interactive card trick.  Penn will ask the person who is in on it to pull out a card and examine it and place it back into the deck.  It doesnt matter what card you pick, you say it was the four of diamonds.  The victim is then asked to place the deck on top of the TV.  Teller then "reaches" up and pulls down a similarly gimmicked card.  Penn says "Is this your card, the FIVE of diamonds?"  The person says no, then Teller (who has been wearing a red and white striped tie) moves the hole onto a white stripe, this making the red diamond vanish, and thus correctly finding the chosen card.  The victim will be amazed that the trick has gone off so well.  The person who is in on it is supposed to say that he thinks he knows how the trick was done and asks for the card...he pulls out the gimmicked card which has now magically appeared in the deck.  Penn says that there are three separate betting points, but does not say what they are.

===Beginners Luck?===
This trick is supposed to be used on either people who are too cheap to buy their own copy of the tape, or too sophisticated to fall for the more outrageous scams.  The bit will revolve around a fake magic tutorial jokingly titled "Cool Tricks for Dear Friends".  Penn & Teller will show the victim and the magician how to do a simple trick involving three cards.  The idea is to take three playing cards, fan them out and then flipping over the middle card so the fan shows up front, back, front.  Penn will then give some bogus instructions to a trick that involves flipping the middle card over in order to make all three backs show in a row.  The victim will have trouble with the trick, while the Magician will nail it on the first go.  This is because the Magician has made a gaffed double-backed card by using a glue stick to stick two cards together, so only the backs are showing.  Penn says to milk this trick, you are supposed to act patronizing and condescending to your victim, (in the video, Penn & Teller claim that they have taught the trick to handicapped children and that they have learned it straight-away.)

===Wedge of Greed===
The last trick on the tape is said to be Tellers favorite.  The trick is a  ) is created that is said to "borderline on eerie."  To prove this, The magician asks both people to stand back to back, with the woman staring at the TV, which is playing yet another clip of Nothing Sacred.  The magician pulls out a randomly picked book and asks the victim to pick a page at random, and then he asks the victim to tell him how many lines down to go on the page and how many word across in order to pick a random word.  This word is written down on a card and handed to the victim, who is goaded into betting his money that his girlfriend CANT guess the word by concentrating.  This drives a wedge between the victim and his lover...who magically knows that the word is "bundle."  This is because the magician doesnt use the word picked out of the book, he writes down the word "bundle" and hands it to the victim.  While this is going on, the woman is watching the TV, when over the clip, Teller walks in holding up large cue cards which say that the word is "bundle."

After the last trick footage is shown, the tape ends with Penn and Teller at the beach, thanking the viewer and saying goodbye.

==Quotes==
Penn: "There are 5,380,000,000 people in this world.  4.71% of these live in the United States.  Of those living in the United States, 60.3% own televisions.  And of those owning televisions, only 27% own video recorders.  That makes 38,705,057 of US, and 5,341,294,943 of THEM.  Thats 137.999 suckers for each one of us. *cash register sfx*  Sucker-wise, its an embarrassment of riches.  If you follow our instructions closely, and have the money to fund your betting, youre gonna make money off us.  Even if you paid as much as the full, list, retail price of $3.95 for this tape, youre gonna increase your money by an order of magnitude in less time than it takes to say knowing falsification of advertising claims resulting in irretrievable financial loss by the aggrieved party."

Penn: (referring to "Vidi-Kopy") "Youre smart enough to see the implications...theres money to be made here.  Demonstrate Vidi-Kopy to the vegetable who walks like a man, and then sell it to him.  The price? Well, let me put it this way, If Lt. Doofus is the buyer, and a Penn & Teller fan is the seller, its pretty safe to say its a sellers market."

Penn: (referring to "Wedge of Greed") "This is our last scam, and Teller has asked me to make a couple of announcements. First of all, hed like you to know that this one is his favorite, and second of all, DONT EVER CALL WHAT HE DOES MIME!" (Teller slams his fist into his palm.)

==External links==
* 

 

 
 
 