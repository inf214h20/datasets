Postal (film)
{{Infobox film
| name           = Postal
| image          = Postalposter07.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Uwe Boll
| producer       = Vince Desiderio Dan Clarke Shawn Williamson
| writer         = Uwe Boll Bryan C. Knight
| based on       =   Larry Thomas David Huddleston Seymour Cassel
| music          = Jessica de Rooij
| cinematography = Mathias Neumann
| editing        = Julian Clarke
| studio         = Running With Scissors, Inc. Brightlight Pictures Pitchblack Pictures
| distributor    = Freestyle Releasing Vivendi Entertainment
| released       =  
| runtime        = 114 minutes   102 minutes   100 minutes
| country        = United States Canada Germany
| language       = English
| budget         = $15 million   
| gross          = $146,741 (Worldwide) http://www.imdb.com/title/tt0486640/business 
}} action comedy Larry Thomas, David Huddleston, and Seymour Cassel.

Like the majority of Bolls previous films, Postal is a film adaptation of a video game, in this case, Postal (video game)|Postal, though this film draws more heavily from that video games sequel, Postal 2. 

==Plot== Postal Dude, Uncle Dave, a slovenly con artist turned doomsday cult leader who owes the US government over a million dollars in back-taxes. With the help of Uncle Daves right hand man Richie and an army of big-breasted, scantily clad cult members, the Dude devises a plan to hijack a shipment of 2,000 Krotchy Dolls, a rare, sought-after plush toy resembling a giant scrotum. Uncle Dave plans to sell them online, where their prices have reached as high as $4,000 a doll.
 September 11, under the watchful eye of bin Ladens best friend George W. Bush, are after the same shipment, but for entirely different reasons. Hoping to outdo the catastrophe of 9/11, they plan to instill the dolls with Avian influenza and distribute them to unsuspecting American children. The two groups meet at the shipments destination, Nazi-themed amusement park Little Germany. A fight between Postal creator Vince Desi and Postal director and park owner Uwe Boll (which ends with Boll being shot in the genitals, confessing "I hate video games"), sparks a massive shootout between the cult, the terrorists and the police, resulting in the deaths of dozens of innocent children. The Dude and the cult are able to get away with both the shipment and the parks opening day guest, Verne Troyer, pursued by Al-Qaeda, the police and a mob of angry citizens.

Upon returning to their compound, which has been overtaken by the terrorists, the Dude, Uncle Dave and the rest covertly sneak into the compounds underground bunker, where Richie reveals that he must now fulfill the prophecy foretold in Uncle Daves fictional Bible: to bring about the extinction of the human race. As per Uncle Daves Bible, the event initiating the apocalypse is the rape of a "tiny entertainer" by a thousand monkeys. After Verne Troyer is quickly thrown into a pit of chimpanzees, Richie shoots and kills Uncle Dave, then imprisons the Dude. The Dude manages to escape the compound with a plethora of weapons, deciding to wage a one-man war against al-Qaeda, his uncles murderer, his cheating wife, the police and the many people who want him dead. On the way to his trailer (where he plans to blow up his spouse), he meets up with an attractive young barista, Faith, who joins forces with him after an explosive gunfight followed later by the Dudes heartfelt but futile monologue about war. The two of them then proceed to kill all the terrorists, all the bloodthirsty townspeople, the remains of the now-mad cult, his wife, and her multiple lovers. In the midst of the shootout, bin Laden escapes to a payphone, where he calls Bush for help. Bush sends a helicopter to save him and plans for the two to rendezvous.
 nuclear missiles towards America, which are scheduled to hit in under two minutes.
 I think this is the beginning of a beautiful friendship". At that moment, all of the nuclear missiles hit, and the country is destroyed.

==Cast==
* Zack Ward as The Postal Dude
* Dave Foley as Uncle Dave
* Chris Coppola as Richard
* Jackie Tohn as Faith
* J.K. Simmons as Candidate Welles
* Verne Troyer as Himself Larry Thomas as Osama bin Laden
* David Huddleston as Peter
* Seymour Cassel as Paul
* Ralf Möller as Officer John Chris Spencer as Officer Greg
* Michael Paré as Panhandler
* Erick Avari as Habib
* Lindsay Hollister as Recorder
* Brent Mendenhall as George W. Bush
* Rick Hoffman as Mr. Blither
* Michael Benyaer as Mohammed
* Uwe Boll as Himself
* Vince Desiderio as Krotchy/Himself
* Carrie Genzel as Reporter Gayle
* Mike Dopud as Security guard #2
* Richard Ian Cox as Coffee customer
* Julia Sandberg Hansson as Mitzi

==Production== adapted into a film. Uwe Bolls Commentary, Postal DVD  Intrigued by the games premise and blatant political incorrectness, Boll contacted Running With Scissors, Inc. president Vince Desiderio, who sold him the rights under the condition that he would be involved with the script and the production. Supposedly, Desiderio and Postal 2 director Steve Wik pitched a much grittier, darker version of the Postal story, but Boll rejected it, fully intent on turning it into a comedy in order to use the film as a platform for political satire as well as "revenge" against the people who have protested his movies.  Boll ended up writing the script with assistant director Bryan C. Knight, who had worked on all of Bolls previous video game adaptations. In an interview for Nathan Rabins book about movie flops, Dave Foley said that Boll did want to make a serious statement about how a cult of heroism had surrounded people who were murdered in the 9/11 attacks, and that he and Boll agreed that being the victim of terrorism makes people victims, not heroes. Foley added that he tried to talk Boll out of including the notorious 9/11 sequence that opens the film where two Al Qaeda hijackers plan to call off their attack when Osama Bin Laden informs them that they will not receive anywhere near 72 virgins for their services, only to have passengers break into the cockpit and accidentally fly the plane into the World Trade Center because the film would have no chance of appearing on many (or any) screens in the U.S. if it stayed in, which is what really happened.
 Cloverdale and Vancouver, British Columbia, Canada. 

==Release and box office performance==

===Worldwide release===
The 114-minute directors cut of Postal premiered at Montreals Fantasia Festival on July 21, 2007. The film made its way along several more United States and European film festivals until finally receiving a limited release in Germany on October 18. It opened at #27 in the German box office, taking in $79,353 from 48 screens and banked $142,761 in its entire run. In Italy it ended its box office run after two weeks with $3,980. As of August 31, 2008, it has grossed a total of $146,741 worldwide.      

===American release===
Despite Boll’s announcement that Postal would be given a wide release on October 12, 2007,  it was delayed until May 23, 2008. Additionally, on May 16, theater distributors pulled out of their deal for a wide release of 1,500 screens to a limited release of only four screens.   Said Boll of the change:

 

On May 20, the screen count increased to 12 screens. By the time of the films release, it had grown to 21. 

Postal opened one day after Indiana Jones and the Kingdom of the Crystal Skull, which led to video promotions from Boll, jokingly claiming that his film would "destroy" the other film at the box office.  A number of Internet promos were made featuring Verne Troyer dressed as Indiana Jones, proclaiming Postals superiority.  

===Home media===
 
Postal received its North American DVD release on August 26, 2008 in both 102-minute unrated and 100-minute rated versions, as well as a 102-minute unrated Blu-ray release. Both versions feature the films trailer, a promotional spot featuring Verne Troyers Indiana Jones, a featurette detailing the filming of the Little Germany scene, footage of Bolls infamous Uwe Boll#Critic boxing matches (Raging Boll)|"Raging Boll" boxing matches, and an audio commentary by Boll. Some editions come with the full version of Postal 2 (Share the Pain edition) on a bonus CD.  

A 114-minute directors cut was released on September 26, 2008 in Germany. It was planned for a North American release on Blu-ray for November 25, 2008, and on DVD for January 6, 2009, but both of these releases were delayed indefinitely. 

==Critical reception and awards==
Reception from professional critics were very negative. The film holds an 7% approval rating at Rotten Tomatoes, based on 41 reviews (3 positive, 38 negative). {{cite web url = title = accessdate = publisher = Rotten Tomatoes }} 

Positive reviews have stemmed from such notable websites as G4 (TV channel)|G4,  UGO Networks|UGO,  Film Threat,  JoBlo.com,  and MTV.  Additionally, some previous detractors of Boll’s work have recanted their opinions of the director after seeing Postal. 
 Worst Director (Boll). The film ended up winning Worst Director. Despite critical condemnation, Postal won two awards at the Hoboken International Film Festival: Best Director and the festivals top prize, Best of Festival. 

==Sequel==
Boll stated shortly after the films production that he would most likely make a Postal 2, even if it went direct-to-video. 
In a 2012 interview, Vince Desi commented that they "are in talks at present regarding another movie".   

On August 28, 2013 Boll announced he was funding production of Postal 2 through Kickstarter, however the project was canceled on October 5 the same year. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 