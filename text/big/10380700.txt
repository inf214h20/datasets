Trick 'r Treat
 
{{Infobox film
| name           = Trick r Treat
| image          = Trick r treat.jpg
| alt            = 
| caption        = Promotional poster
| director       = Michael Dougherty
| producer       = Bryan Singer
| writer         = Michael Dougherty
| based on       =
| starring       = {{plainlist|
* Dylan Baker
* Rochelle Aytes
* Anna Paquin Brian Cox
}}
| music          = Douglas Pipes
| cinematography = Glen MacPherson
| editing        = Robert Ivison
| studio         = {{plainlist|
* Legendary Pictures
* Bad Hat Harry Productions
}}
| distributor    = Warner Premiere
| released       =  
| runtime        = 82 minutes
| country        = {{plainlist|
* United States
* Canada
}}
| language       = English
| budget         = $12 million   
| gross          = $6.8 million   (domestic video sales) 
}} Brian Cox and Anna Paquin, and centers on four Halloween-related horror stories. One common element that ties the stories together is the presence of Sam (Trick r Treat)|Sam, a mysterious pint-sized Trick-or-treating|trick-or-treater wearing shabby orange pajamas with a burlap sack over his head, that makes an appearance in all the stories whenever someone breaks Halloween traditions.

Despite being delayed for two years and having a very limited theatrical release, the film received much critical acclaim and has since garnered a strong cult following.  In October 2013, the filmmakers announced that a sequel, Trick r Treat 2, is in the works.

== Plot ==
===Opening===

Emma (Leslie Bibb) and her holiday-obsessed husband, Henry (Tahmoh Penikett), have set up numerous ghost-scarecrows for Halloween in their yard, although she is mostly uninterested in Halloween. After returning home from a Halloween party, Emma tries to blow out a jack-o-lantern, but Henry tells her not to because it is against tradition to extinguish a jack-o-lantern on Halloween; she blows it out anyway. While Henry is inside waiting for Emma to take down the decorations, she is murdered by an unknown assailant with a large blade-shaped pumpkin lollipop in front of the kids who were trick and treating, who ran away in horror. Later, Henry goes outside and notices limbs hanging out of wires. Just then, one of the scarecrows lights up. He approaches it and takes down the cover, only to see Emmas body stuck onto the stick and wrapped in decorative lights, with her limbs chopped off and her mouth stuffed with a large lollipop, and screams in horror.

===The Principal===
 Brett Kelly) Brian Cox), and his dog. After finishing the burial, Wilkins sees Kreeg screaming from his window, begging for help; Wilkins disregards this and Kreeg is knocked down by an unknown assailant. Later, Wilkins arrives in his basement, where he helps his son Billy carve a "jack-o-lantern" in their basement, which is revealed to be Charlies severed head. The sequence ends with Billy saying "But dont forget to help me with the eyes."

===The School Bus Massacre Revisited===

Five kids, Macy (Britt McKillip), Schrader, Sara, Chip and Rhonda, a Savant syndrome|savant, journey to the local rock quarry where Macy tells them the local urban legend of "The Halloween School Bus Massacre". Thirty years ago, in 1977, there was a group of mentally retarded kids. Their parents were embarrassed by their conditions and saw them to be a burden to them, and as such, they were sent off to a school in another town so they would not face the townspeoples mocking. One day, on Halloween, they paid the bus driver to kill the kids so they would be freed of their burden. He drove the bus to the quarry, chained the kids to the seats, and while he was checking them, one kid managed to get free and he went to the steering wheel, wanting to drive back home. However, he set the gear in forward instead of reverse and drove into the lake, drowning them all. However, the bus driver managed to get out.

The group plays a prank on Rhonda in which they pretend to be the undead children and chase Rhonda until she trips and knocks herself out. Macy kicks a jack-o-lantern into the lake, whereafter the school bus children suddenly rise from the water. The undead children chase the group, and manage to grab hold of the chains which Sara is wearing as part of her costume, dragging her away to be killed. Meanwhile, Rhonda has locked herself in the elevator leading out of the quarry. Despite the others begging her to open it, she ignores them and rides it up herself as the School Bus children corner them as a revenge for the prank. As Rhonda steps out of the elevator, the other kids are heard being dismembered and eaten alive.

===Surprise Party===

Laurie (Anna Paquin), a self-conscious 22-year-old virgin, is getting ready for a party with her older sister Danielle (Lauren Lee Smith) and their two friends Maria (Rochelle Aytes) and Janet (Moneca Delain). Annoyed by their talk about boys, she stays behind and plans to join them later. The other girls find local men to bring along with them. Later, alone on her way to the party, Laurie is attacked by a vampire dressed in black. Meanwhile, Danielle, Maria, and Janet party at a bonfire with the men they brought, with Danielle worried about Laurie. The vampires body suddenly drops from a tree onto the party and Laurie appears. The "vampire" is revealed to be Principal Wilkins in disguise wearing fake fangs. The girls at the party, along with Laurie, suddenly transform into werewolves, removing their skin. It is revealed that Lauries "virginity" actually signifies that she has never killed anyone before. The girls then proceed to devour their dates along with Wilkins.

===Sam===

Kreeg, a Halloween-hating man, lives alone with his dog, Spite, as his only companion. Kreeg shows his disgust for Halloween by scaring away trick-or-treaters. While at his house, Kreeg is attacked by Sam (Quinn Lord), a small human being dressed in an orange jump suit and with a smiley face on a sack he is wearing over his head. During the struggle, Kreeg rips off the sack over Sams head to reveal his head to be a cross between a jack-o-lantern and a human skull. Kreeg runs to his window calling for Mr. Wilkins to help him, but is tackled by Sam. Kreeg gains the upper hand when he manages to fire his shotgun at Sam, seemingly killing it, in the process shooting off one of Sams hands. As Kreeg dials 9-1-1, Sam re-attaches his hand and rises. Sam then attacks him again, stabbing at him with a large pumpkin lollipop out of which he has taken a bite, rendering it sharp. Sam finds that the lollipop has lodged itself into a piece of chocolate from which Kreeg had earlier taken a bite. Sam takes the lollipop wedged into the chocolate and walks away, revealing that Sam was trying to punish Kreeg for scaring away the kids and wanted candy for compensation. Meanwhile, pictures burning slowly in a nearby fireplace show a class photo of the masked children from the "School Bus Massacre" and the bus driver, who is revealed to be Kreeg.

===Conclusion===

Bandaged and bruised from his encounter with Sam, Kreeg answers his door to give candy to trick-or-treaters. While on his porch, he spots Sam walk over to Emma and Henrys house just after she blows out the jack-o-lantern, revealing himself as Emmas killer in order to punish her for not following Halloweens rules. Rhonda walks across the street in a trance and is almost hit by the laughing, human-form werewolf girls in their car, on their way back from the surprise party. Billy is sitting on his porch handing out candy, unaware that his father is now dead. Kreeg then walks back inside when there is another knock at the door. He opens the door to find the kids from the School Bus Massacre standing there with their bags outstretched, saying, "Trick r Treat", and, suddenly recognizing them, stares in shock. A series of comic strip panels shows Kreeg being murdered and dismembered by them.

==Cast==
 
* Dylan Baker as Steven Wilkins
* Rochelle Aytes as Maria
* Anna Paquin as Laurie Brian Cox as Mr. Kreeg Sam / Peeping Tommy
* Lauren Lee Smith as Danielle
* Tahmoh Penikett as Henry
* Moneca Delain as Janet Brett Kelly as Charlie
* Britt McKillip as Macy
* Isabelle Deluce as Sara
* Jean-Luc Bilodeau as Schrader
* Leslie Bibb as Emma Patrick Gilmore as Bud the Cameraman
* C. Ernst Harth as Giant Baby
* Christine Willes as Mrs. Henderson
* Samm Todd as Rhonda
 

==Production ==

===Seasons Greetings===
Seasons Greetings is an animated short created by Trick r Treat writer and director Michael Dougherty in 1996 and was the precursor of the film.  The movie featured Sam as a little boy dressed in orange footy pajamas with his burlap sack head covering, as he is being stalked by a stranger on Halloween night. The short was released as a DVD extra on the original release for Trick r Treat and was aired on FEARnet in October 2013 as part of a 24-hour Trick r Treat marathon on Halloween. 

===Trick r Treat===
Trick r Treat was filmed on location in Vancouver, British Columbia. Originally slated for an October 5, 2007 release, it was announced in September 2007 that the film had been pushed back. 

==Release==

===Theatrical screenings=== New York After Dark The Bloor.

===Home media=== Warner Bros. direct to DVD and Blu-ray on October 6, 2009 in North America, October 26 in the UK, and October 28 in Australia.

==Merchandise==
* Sideshow Collectibles created a 15" vinyl figure based on the films scarecrow-like character Sam. NECA created a 5½" scale figure of Sam that has been released as part of NECAs "Cult Classics" line of movie figures; the figure includes a stand, pumpkins, "candybar," lollipop, sack, and interchangeable, uncovered head. 
* Palace Press and Insight Editions published a 108-page coffee table book entitled Trick r Treat: Tales of Mayhem, Mystery & Mischief. It documents the making of the film, and includes storyboards, concept art, cast and crew biographies, and behind-the-scenes photographs.

===Comic books===
  Wildstorm Comics had planned to release a four-issue adaptation of Trick r Treat written by Marc Andreyko and illustrated by Fiona Staples, with covers by Michael Dougherty, Breehn Burns and Ragnar.  The series was originally going to be released weekly in October 2007, ending on Halloween, but the series was pushed back due to the films backlisting. The four comics were instead released as a graphic novel adaptation in October 2009.  Legendary Comics set the second Trick r Treat comic book for an October 2015 release date,  and features Arts of Artist Fiona Staples.  The comic will be released alongside the graphic novel adaptation of Doughertys Krampus (film)|Krampus. 

==Reception==

===Critical reaction===
Despite only a handful of public screenings, the film has received critical acclaim. Based on 20 reviews collected by Rotten Tomatoes, the film has an overall "Fresh" approval rating from critics of 85%, with an average score of 7.4/10 and a critical consensus that the "deftly crafted tribute to Halloween legends, Trick r Treat hits all the genre marks with gusto and old fashioned suspense."  Dread Central gave it 5 out of 5 stars, stating, "Trick r Treat ranks alongside John Carpenters Halloween (1978 film)|Halloween as traditional October viewing and I cant imagine a single horror fan that wont fall head over heels in love with it."  The film earned 10 out of 10 from Ryan Rotten of ShockTilYouDrop.com.  IGN called it a "very well-crafted Halloween horror tribute is a scary blast," rating it a score of 8 out of 10.  Bloody Disgusting ranked the film ninth in their list of the Top 20 Horror Films of the Decade, calling it "so good that its lack of a theatrical release borders on the criminal." 

===Awards===
* 2008 – Audience Choice Award, Screamfest Horror Film Festival 
* 2009 – Silver Audience Award, Toronto After Dark Film Festival 

==Sequel==
Michael Dougherty announced in October 2009 that he is planning a sequel,  but later stated that there were "no active development nor an attempt at a pitch."  He went on to say that "  more fans continue to support and spread the movie, the more likely it is that Sam will rise from the pumpkin patch once more."  Dougherty helped create a short promotional Easter trailer for FEARnets Trick r Treat 24-hour marathon for Halloween 2011; the trailer showed a familys Easter celebration turning into one of horror, with Sam watching the chaos outside whilst wearing rabbit ears.  In October 2013, Dougherty and Legendary Pictures officially announced a sequel, titled Trick r Treat 2. Dougherty said he plans to "shake it up a little bit" with the sequel. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 