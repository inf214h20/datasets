Macabre (2009 film)
{{Infobox film
| name           = Macabre
| image          = Film-rumahdara-id.jpg
| caption        = Promotional Poster
| director       = Mo Brothers
| producer       = Delon Tio
| writer         = Mo Brothers
| starring       =  
| music          =  
| cinematography = Roni Arnold
| editing        = Herman Kumala Panca
| distributor    =  
| released       =  
| runtime        = 95 minutes
| country        = Indonesia
| language       = Indonesian
| budget         =
| gross          =
}}
Macabre, international title for Rumah Dara (Indonesian title), in Russia titled Dara (rus. Дара) and in Singapore Darah, is a 2009 Indonesian horror-slasher film. The film tells about a group of people travelling by car to the airport, when along the road, they meet a beautiful woman named Maya who says she has been robbed and needs a ride home. The film is based on the short film Dara (short film)|Dara.

Before the film was screened in Indonesia, it was screened at several festivals in 2009. Rumah Dara then received a wide release in Indonesia on January 22, 2010. The movie has been planned for distribution in North America and Europe by Overlook Entertainment. {{cite web
 | last = Noh | first = Jeah | year = 2009
 | url = http://www.screendaily.com/festivals/cannes/cannes-news/overlook-turns-macabre-for-europe-and-us/5001201.article
 | title = Overlook turns Macabre for Europe and US
 | publisher = ScreenDaily | language = }}
 

According to the official Twitter page for Rumah Dara, the film is banned in Malaysia because of excessive violence. The movie is the first Indonesian film to be banned there. {{cite web
 | year = 2010
 | url = http://www.esqmagazine.com/hiburan/2010/02/05/1385/film-rumah-dara-dicekal-di-malaysia.html
 | title = Film "Rumah Dara" Dicekal di Malaysia
 | publisher = ESQmagazine | language = Indonesian}}
 

The story unfolds as the group of travelers attempts to escape from a house which is owned by a mysterious lady named Dara and her family. Later it is revealed that the family are killers and cannibals attempting to gain immortality. The group is attacked with several types of weapons.

==Plot==
 

Adjie and Astrid, a married couple, are getting ready to travel to Sydney for a new job. They are accompanied by their friends, Alam, Eko, and Jimmy to the airport. On the road, they meet up with Adjies sister, Ladya, and convince her to go with them. However, Ladya, who still blames Adjie for their parents death, declines. After some accidents at Ladyas workplace and Astrids coaxing, she changes her mind and decides to go with them. After they drive away from Ladyas workplace, they see a confused-looking, beautiful woman named Maya on the road. She tells them she has been robbed and is unable to get home. Pitying her, they give Maya a ride home.

The group travels by car to what seems to be the middle of nowhere, where Mayas house is located. There, they meet Mayas family. Mayas mother, Dara, has a very young and lovely face with an eerie expression and mysterious body language. Adam, Mayas second brother greets them and is very gentlemanly. There is also Arman, Daras eldest son, who is grumpy and never speaks. After introductions, Dara secretly drugs Astrids drink. After much insistence, they agree to stay for dinner. However, Adjie and Astrid, who is pregnant, go to the guestroom upstairs so Astrid could rest. Unfortunately, the remainder of their friends, who are having dinner, get drugged and knocked out. Arman takes them to a cellar, with Alam still at the dinner table. Alam is seduced by Maya, but rejects her. Out of anger, she slashes him, and when he tries to retaliate, Adam breaks his arm.

Adjie and Astrid saw this and try to get upstairs, but Adam catches Adjie and breaks his leg. Astrid runs upstairs and locks herself in the guest room. Dara tells her that the drug she was given will cause her to go into labor. Then Astrid finds a small window, climbs up and sees a car arriving at the house. Astrid calls for help, but it turns out that the woman from the car is Daras friend. She is given several coolers of "meat", revealing the motive for the slaughter to come. They are a family of cannibals, who belong to a secret society attempting to gain immortality from it. The family regularly searches for victims, providing human flesh for the society. Astrids water breaks, and she delivers her own baby by herself.

Astrid gets out of the room with her newborn son and embraces her husband. Meanwhile, Dara sneaks into the room and takes the baby. Dara tells the couple that they could escape by themselves. However, if they try to take the infant back, they will all die that night. Astrid follows Dara to the next room, while Adjie goes to another room to try to find help.

Down in the cellar, Arman has slaughtered Alam with a chainsaw and now chooses Ladya to be his next victim. However, she knocks him out and escapes. She breaks out of the cellar and sets Eko and Jimmy free. After their escape, the trio re-enter the house, in an attempt to save the married couple. However, Maya, who is armed with a crossbow, sees them and shoots at Eko, hitting his left ear. They run to the forest, with Adam now chasing them. Ladya gets away and goes back to the house. Jimmys neck is broken by Adam, and he dies. Eko manages to walk through the forest and find a road, where he is spotted by the light of a car.

Back in the house, Adjie enters a room filled with baby cadavers and sees one baby that was being preserved. He also finds his wife, stabbed in the neck with a huge hair pin. After finding Astrid dead, Adjie strangles Dara out of revenge. Dara stabs him and strangles him into unconsciousness. Ladya enters, but Arman grabs her and brings her upstairs to rape her.

Dara and Maya go to front door; Adam is there already. A police patrol has found Eko and takes him into their car. The four police officers separate; two go upstairs, while the others remain on the first floor. One of the officers finds a video containing the clip that was seen at the start of this film. Shocked, he accidentally drops a small collection of photographs, depicting Adam, Arman, and Maya, but strangely dated as 1912. There is also a photo of Dara, dated 1889. Upstairs, Arman is stabbed in the eye while raping Ladya. She escapes while Arman screams loudly, tipping off the police. The police take a defensive posture, but Adam turns off the lights, and the family of cannibals attack the police in the dark. However, the leader of the police gets away, to retrieve his shotgun, and fires it at Maya, wounding her. Dara, who has just killed the other officers, runs to the front door and takes the shotgun away from the police leader and kills him. Dara finds the injured Maya and breaks her neck to end her daughters suffering.

Arman comes out of the room with the wooden stick jammed in his eye. He slashes the throat of one of the police officers who had gone upstairs to investigate earlier. They die together. Ladya runs to the room where Adjie lies unconscious. She finds Astrids body, after which they are attacked by Adam. While Adam fights with Adjie, Eko enters and jabs Adam with a sword. Adam, who is quite durable, removes the sword while Adjie pours a liquid over his head. Seeing this, Ladya takes her lighter and tosses it to Adam. Adam goes up in flames and runs to another room. Eko goes downstairs alone and is slashed by Dara with a chainsaw. Back upstairs, Ladya and Adjie attack Adam once more. Ladya, who has taken the sword, slices Adams throat. Together with Adjie, they decapitate Adam.

Ladya and Adjie take the baby to another part of the house, stained by blood. Ladya takes revenge on Dara, the last remaining villain. She goes to the dining room, where she finds Ekos body and is attacked by Dara and her chainsaw. Ladya with the sword and Dara with the chainsaw, fight each other. Ladya escapes to the other side of dining table and runs out of the room. But Dara slashes Ladyas foot, causing blood to squirt out. Dara gets ready to attack Ladya again, but is knocked in the head by Adjie and Ladya gets away. Dara is still able to grab the chainsaw, slashing Adjies shoulder. Adjie finds a gun next to him and throws it to Ladya. Ladya takes the gun and shoots Dara, then strangles her with her necklace. Dara lies on the floor and is kicked by Ladya. Ladya reunites with Adjie and apologizes to him. Adjie forgives her and dies after he asks Ladya to care for her nephew (his newborn son).

Morning has come as Ladya gets away from the house with her nephew in her arms. She puts her nephew in a box in the backseat of the police car. She starts the car, but before she can drive away, Dara reappears and punches out the cars window, grabbing Ladya. Ladya drives the car to the forest road, opens the car door, puts the car in reverse, and smashes Dara against a big tree. Ladya looks at Daras fallen body and smiles, knowing that she is safe after the previous nights bloody "game". She says to her nephew, "Were going home." As she drives away, we see Daras bloody hand moving, slowly moving.

==Cast==
* Julie Estelle – Ladya
* Ario Bayu – Adjie
* Sigi Wimala – Astrid
* Shareefa Daanish – Dara
* Imelda Therinne – Maya
* Arifin Putra – Adam
* Daniel Mananta – Jimmy Mike Lucock – Alam
* Dendy Subangil – Eko
* Ruli Lubis – Arman
* Aming - Mecia

==Awards==
Leading actress Shareefa Daanish won the 2009 Best Actress award at the Puchon International Fantastic Film Festival (PiFan 2009) for her role. 

==Different versions==
There are a few differences between the Indonesian theatrical version and the Singapore theatrical version & DVD. The Indonesian version has been edited, with the decapitation and falling-head scene being deleted due to the graphic nature. In the Singapore theatrical version, which does contain those scenes edited from the Indonesian version, any scenes showing blades making contact with flesh have been removed, with some unimportant scenes added in their stead.

==Critical reception==
The film was well received in Indonesia, though some criticized it as being derivative of The Texas Chain Saw Massacre (1974) and other Hollywood slasher films.

==References==
 

==External links==
* 
* 
*  at Shock Till You Drop

 
 
 
 
 
 
 
 
 
 
 
 