The Melody Man
{{infobox_film
| title          =  The Melody Man
| image          =
| imagesize      =
| caption        =
| director       = Roy William Neill
| producer       = Harry Cohn
| writer         = Herbert Fields(play:The Melody Man) Howard J. Green
| starring       = John St. Polis Alice Day William Collier, Jr.
| music          =
| cinematography = Ted Tetzlaff
| editing        = Leonard Wheeler
| distributor    = Columbia Pictures
| released       = January 15, 1930
| runtime        = 68
| country        = USA
| language       = English

}}
The Melody Man is a 1930 Pre-Code Hollywood|Pre-code sound motion picture produced and distributed by Columbia Pictures. It was directed by Roy William Neill and starred  John St. Polis,  Alice Day and William Collier, Jr.. The story is based on a Broadway play by Herbert Fields. 

Preserved in the Library of Congress. 


==Cast==
*John St. Polis - Von Kemper
*Alice Day - Elsa
*William Collier, Jr. - Al Tyler (*billed as Buster Collier)
*Johnnie Walker - Joe Yates
*Mildred Harris - Martha
*Albert Conti - Prince Friedrich
*Anton Vaverka - Franz Josef
 
uncredited
*Tenen Holtz - Gustav
*Lee Kohlmar - Adolph
*Nicholai Konovaloff - Bachman (*as Major Nichols)
*Bertram Marburgh - Van Bader

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 