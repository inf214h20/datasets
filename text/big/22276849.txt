Come Play with Me (1968 film)
{{Infobox film
| name           = Come Play with Me
| image	         = Come Play with Me FilmPoster.jpeg
| caption        = A poster with the films Italian title: Grazie, zia
| director       = Salvatore Samperi
| producer       = 
| writer         = Salvatore Samperi Sergio Bazzini Pier Luigi Murgia
| starring       = Lisa Gastoni
| music          = 
| cinematography = Aldo Scavarda
| editing        = Silvano Agosti
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Come Play with Me ( ) is a 1968 Italian drama film directed by Salvatore Samperi. It was listed to compete at the 1968 Cannes Film Festival,    but the festival was cancelled due to the events of May 1968 in France.

==Plot==
Alvise (Lou Castel) is a young man who thinks he is paralyzed. He receives treatment for his psychological problem. His father leaves Alvise  for a few days in the care of Aunt Lea (Lisa Gastoni), who is Alvises mothers sister. Alvise becomes infatuated with his Aunt and Lea also reciprocates gradually.

Lea breaks off her relationship with Stefano (Gabriele Ferzetti) to be with her nephew. Alvise promises to make love to Lea if she plays with him and beats him at any of the games. Lea and Alvise play a number of games leading to the ultimate game of euthanasia.

==Cast==
* Lisa Gastoni - Aunt Lea
* Lou Castel - Alvise
* Gabriele Ferzetti - Stefano
* Luisa De Santis - Nicoletta - the singer
* Massimo Sarchielli - Massimo
* Nicoletta Rizzi - The Secretary of Alvises father
* Anita Dreyer - Barbara

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 