Stoned (film)
{{Infobox Film
| name           = Stoned
| image          = Stonedmp.jpg
| image_size     = 
| caption        = Promotional movie poster
| director       = Stephen Woolley
| producer       =  Neal Purvis Robert Wade
| narrator       = 
| starring       = Leo Gregory Paddy Considine David Morrissey Ben Whishaw Tuva Novotny Amelia Warner Monet Mazur  
| music          = David Arnold John Mathieson
| editing        = Sam Sneade
| distributor    = Screen Media Films
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2005 film about Brian Jones, one of the founding members of The Rolling Stones. The film is a cinematic work of historical fiction, taking as its premise the idea that Jones was murdered by Frank Thorogood, a builder who had been hired to renovate and improve Joness house Cotchford Farm in East Sussex. The film also paints a picture of Joness use of alcohol and drugs, and his relationships with Anita Pallenberg and Anna Wohlin. 

The film was directed by Stephen Woolley, and written by Neal Purvis and Robert Wade. Leo Gregory played the role of Brian Jones and Paddy Considine of Frank Thorogood.

This film grossed $38,922 in limited theatrical release in the United States,  but has performed strongly in the home video market in the US. 

==Cast==
* Leo Gregory ... Brian Jones
* Paddy Considine ... Frank Thorogood
* Sam Sharwood ... Sam
* David Morrissey ... Tom Keylock
* Ben Wishaw ... Keith Richards
* Luke de Woolfson ... Mick Jagger
* Tuva Novotny ... Anna Wohlin
* Amelia Warner ... Janet
* Monet Mazur ... Anita Pallenberg

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 