Take Me to Hollywood
{{Infobox film
| name =   Take Me to Hollywood
| image =
| image_size =
| caption =
| director = Edgar Neville 
| producer = 
| writer =  Edgar Neville 
| narrator =
| starring = 
| music = Luis Patiño  
| cinematography = Agustín Macasoli  
| editing = 
| studio = Star Film
| distributor = 
| released = 20 June 1931 
| runtime = 
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
Take Me to Hollywood (Spanish:Yo quiero que me lleven a Hollywood) is a 1931 Spanish comedy film directed by Edgar Neville. It was one of the earliest Spanish sound films. 

==Cast==
* Emilia Barrado    
* Julia Bilbao    
* Pedro Chicote    
* Federico García Sanchíz  
* Perlita Greco    
* Enrique Herreros   
* José Martín     Antonio Robles    
* Ángeles Somavilla 
* Manuel Vico

== References ==
 
 
==Bibliography==
* Labanyi, Jo & Pavlović, Tatjana. A Companion to Spanish Cinema. John Wiley & Sons, 2012.

== External links ==
* 

 

 
 
 
 
 
 

 