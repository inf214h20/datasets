South Sea Rose
{{infobox_film
| name           = South Sea Rose
| image          =
| imagesize      =
| caption        =
| director       = Allan Dwan
| producer       = Allan Dwan
| writer         = Sonya Levien Elliott Lester (dialogue)
| based on       =  
| starring       = Lenore Ulric
| music          = Peter Brunelli (uncredited) Arthur Kay (uncredited) Glen Knight (uncredited)
| cinematography = Harold Rosson
| editing        = Harold Schuster
| distributor    = Fox Film Corporation
| released       = December 8, 1929 August 3, 1931 (Finland)
| runtime        = 69 minutes (7 reels)
| country        = United States
| language       = English
}}

South Sea Rose is a 1929 American comedy-drama filmdistributed by the Fox Film Corporation and produced and directed by Allan Dwan. This picture was Dwans second collaboration with star Lenore Ulric, their first being Frozen Justice. Much of the cast and crew on Frozen Justice returned for this film.  

South Sea Rose is based the 1928 Broadway stage play La Gringa by Tom Cushing which starred then unknown theatre player Claudette Colbert.  Like Frozen Justice, this film is now presumed lost film|lost. 

==Cast==
*Lenore Ulric - Rosalie Durnay
*Charles Bickford - Captain Briggs
*Kenneth MacKenna - Doctor Tom Winston
*J. Farrell MacDonald - Hackett Elizabeth Patterson - Sarah
*Tom Patricola - Willie Gump
*Ilka Chase - The Maid
*George MacFarlane - The Tavern Keeper Ben Hall - The Cabin Boy
*Daphne Pollard - Mrs. Nott
*Roscoe Ates - The Ships Cook Charlotte Walker - The Mother Superior
*Emile Chautard - Rosalies Uncle

==References==
 

==External links==
* 
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 