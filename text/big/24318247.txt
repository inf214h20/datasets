The Love of Jeanne Ney
 
{{Infobox film
| name           = The Love of Jeanne Ney
| image          =
| caption        =
| director       = Georg Wilhelm Pabst
| producer       =
| writer         = Ilja Ehrenburg Rudolf Leonhardt Ladislaus Vajda
| starring       = Édith Jéhanne
| cinematography = Robert Lach Fritz Arno Wagner
| editing        = Georg Wilhelm Pabst Marc Sorkin
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = Weimar Republic Silent German intertitles
| budget         =
}}
 silent German drama film directed by Georg Wilhelm Pabst.   

==Plot==
Jeanne (Edith Jehanne) is the daughter of Alfred Ney, a French diplomat and political observer. The family is based in Russia during the post-revolutionary civil war. Her father is set up by the scheming Khalibiev, who sells him a list of Bolshevik agents that includes Jeannes lover, Andreas Labov (Uno Henning). The information is leaked by Alfreds Chinese servant, though Khalibiev isnt implicated. With the revolutionary army about to storm the city, Andreas is forced to execute Jeannes father. She is horrified, but urges Andreas to run for his life. He warns her that it is she who must run, as the Red Army will soon occupy the town. She escapes with the help of a homely soldier, whos become smitten with her.

Jeanne flees to Paris, followed by Khalibiev and Andreas. She takes a job as a secretary under her uncle Raymond (Adolph Edgar Licho), a private detective. Khalibiev sets about seducing Raymonds blind daughter, Gabrielle (Brigitte Helm), in order to rob her and run away with a flapper he meets at a bar. The latter girl balks and warns Raymond, who has meanwhile been searching for a stolen diamond with a $50,000 reward. The diamond turns out to have been swallowed by a shiny-object-loving parrot.

Raymond, who has become hopelessly obsessed with Jeanne, tries to force himself on her and loses his grip on reality. That night Khalibiev sneaks in, strangles him, and steals the money. He frames Andreas by letting the blind Gabrielle grab his coat while he flees the scenes of the crime (he stole the coat from Andreas) and dropping a wallet with Andreass photo. Andreas is caught delivering money for the communist party in France, which makes him look all the more suspicious.

Jeanne thinks to use Khalibiev as an alibi, as he saw her leaving the building with Andreas, without realizing he is the murderer. They travel by train with the apparent intention of clearing Andreas, but Khalibiev makes sexual advances to her. When she screams he attempts to silence her with his handkerchief, forgetting he has wrapped the stolen diamond in it. She realizes he is the murderer. He is arrested, and Andreas is freed. They leave together.

==Cast==
* Édith Jéhanne as Jeanne Ney
* Uno Henning as Andreas Labov
* Fritz Rasp as Khalibiev
* Brigitte Helm as Gabrielle
* Adolf E. Licho as Raymond Ney
* Eugen Jensen as Andre Ney
* Hans Jaray as Poitras
* Sig Arno as Gaston (as Siegfried Arno)
* Hertha von Walther as Margot
* Vladimir Sokoloff as Zacharkiewicz
* Jack Trevor
* Mammey Terja-Basa
* Josefine Dora
* Heinrich Gotho
* Margarete Kupfer (as Küpfer)
* Robert Scholz

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 