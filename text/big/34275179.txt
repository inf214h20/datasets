Potta Potti
{{Infobox film
| name = Potta Potti 50/50
| image =
| image size =
| caption =
| director = Yuvaraj Dhayalan
| producer = V. Muraliraman
| writer = Yuvaraj Dhayalan
| starring = Sadagoppan Ramesh Harini
| music = Aruldev
| cinematography = Gopi Amarnath
| editing = Raja Mohammed
| studio = AVR Talkies Flicker Studios
| distributor =
| released =  
| runtime =
| country = India
| language = Tamil
| budget =1,80,00,000
| gross =2,20,00,000
| preceded by =
| followed by =
}}
 Indian Tamil Tamil Sports-Comedy film written and directed by newcomer Yuvaraj Dhayalan, featuring cricketer Sadagoppan Ramesh in the starring role alongside several newcomers.  The film, initially titled Pattai Patti,   released on 5 August 2011 to generally positive reviews.

==Plot==

Two men, Kodaivannan and Kolaivannan, of a remote village decide to marry a rich girl. So village rivalries told to both
kodaivannan and Kolaivannan, those who won a match, that person eligible to marry that girl. The ‘good’ hearted Kodaivannan’s
team kidnaps the real cricketer, Sadagoppan Ramesh, while he was on his way to Thekkady. The Kolaivannan
hires his coach from a greedy big shot from Chennai who eyes the big land of this village for his business.

Now, the teams have one more reason to fight against each other, the ‘bad’ Kolaivannan winning the wealth for his Chennai man, and the ‘good’ Kodaivannan winning the game to save their village. In the climax, the ‘good’ Kodaivannan wins the match and saves their village.

In between, the coach Sadagoppan Ramesh falls in love with that girl.

==Cast==
* Sadagoppan Ramesh as himself
* Harini as Ranjitham
* R. Sivam as Kodaivaanan
* Umar as Kolaivaanan
* Mayilsamy as Harichandra
* Avathar Ganesh as Avathaaram

==Soundtrack==

{{tracklist
| collapsed       = 
| all_music       = Arul Dev
| all_lyrics      = Jayamurasu and Kevin Shadrach
| extra_column    = Artist(s)
| title1          = Iduvarai Iduvarai
| extra1          = Hariharan (singer)|Hariharan, Mahathi, Aruldev
| length1         = 4:12
| title2          = Ekkuthappa Ekkuthappa Sathyan
| length2         = 5:02
| title3          = Padai Nadungum
| extra3          = Pavan
| length3         = 3:32
| title4          = Adangaathaa Vegam Karthik
| length4         = 4:18
| title5          = Never Gonna Change
| extra5          = Vijay Narain
| length5         = 2:24
}}

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

==Reception==
The film received overall positive critical response, with most reviewers noting its resemblance to the award-winning Hindi film   cited: "Potta Potti doesnt have the excitement of a movie based on sports but it doesnt disappoint as a fun-filled movie. Watch it for the innocence with which the villagers approach the game and some hilarious moments..."  Malathai Rangarajan from The Hindu wrote: "If keeping the viewer in splits is the aim, Potta Potti achieves it with ease", adding that it "may have arrived late, but the delay hardly affects the entertainment it provides".  Rohit Ramachandran of Nowrunning.com called it a "A simple, sincere delight that makes no bones about its inspirations. It works well, once."  

==References==
 

 
 
 
 
 
 