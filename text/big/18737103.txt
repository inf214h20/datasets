On War (film)
{{Infobox film
| name           = On War
| image          = Affiche-On War (film) .jpg 
| caption        = Theatrical release poster
| director       = Bertrand Bonello 
| producer       = Bertrand Bonello Kristina Larsen 
| writer         = Bertrand Bonello
| starring       = Mathieu Amalric    Asia Argento 
| music          = Bertrand Bonello
| cinematography = Josée Deshaies
| editing        = Fabrice Rouaud
| distributor    = Ad Vitam Distribution
| runtime        = 130 minutes
| country        = France
| language       = French
| released       =  
| budget         =
| gross          =
}}
On War ( ) is a 2008 French comedy-drama film directed by Bertrand Bonello and starring Mathieu Amalric. The cast also includes Laurent Lucas, Guillaume Depardieu, Asia Argento, Michel Piccoli and Léa Seydoux. The title loosely refers to the treatise On War, by Carl von Clausewitz.

== Plot ==
Bertrand (Mathieu Amalric), a film director, is conducting research for his latest film, and asks a funeral director if he can stay back at his funeral parlour after the close of business. Bertrand cannot resist getting into a coffin, and accidentally knocks the lid down, locking himself in the coffin.
 David Cronenbergs film, Existenz|eXistenZ. He returns to the funeral parlour in the evening, where a strange man breaks into the funeral parlour with him. After asking Bertrand to describe his experience the previous night, the man eventually takes Bertrand to a mansion, headquarters to what appears to be a cult. Although the cult has militaristic aspects, it also seems to be devoted to pleasure.

Bertrand is gradually drawn deeper and deeper into the cult.

== Cast ==
* Mathieu Amalric as Bertrand
* Asia Argento as Uma
* Guillaume Depardieu as Charles
* Clotilde Hesme as Louise
* Laurent Delbecque as Pierre
* Léa Seydoux as Marie
* Michel Piccoli as Le grand Hou
* Aurore Clément as La mère de Bertrand
* Elina Löwensohn as Rachel
* Laurent Lucas as Christophe 
* Marcelo Novais Teles as Frédéric

== External links ==
*  

 

 
 
 
 
 
 
 