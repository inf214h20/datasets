Should a Woman Divorce?
{{Infobox film 
 | name = Should a Woman Divorce?
 | image = Should A Woman Divorce 1914 still.jpg
 | caption = Scene from Should a Woman Divorce?
 | director = Edwin McKim
 | writer = Ivan Abramson
 | starring = Lea Leland and Leonid Samoloff
 | producer = Ivan Film Productions
 | distributor = 
 | budget = 
 | released = 1914
 | country = United States English intertitles
 | runtime = 
 | language = Silent 
  | }}
 1914 silent film written by Ivan Abramson and directed by Edwin McKim, and starring Lea Leland and Leonid Samoloff.

==Plot==
Grace Roberts (played by Lea Leland), marries rancher Edward Smith, who is revealed to be a crappy vice-ridden spouse.  They have a daughter, Vivian.  Dr. Franklin (Leonid Samoloff) whisks Grace away from this unhappy life, and they move to New York under aliases, pretending to be married (since surely Smith would not agree to a divorce).  Grace and Franklin have a son, Walter (Milton S. Gould).  Vivian gets sick, however, and Grace and Franklin return to save her.   Somehow this reunion, as Smith had assumed Grace to be dead, causes the death of Franklin.   This plot device frees Grace to return to her fathers farm with both children. Connelly, Robert B.  , p. 253 (1998) 

==Cast==
*Leonid Samoloff as Dr. Franklin
*Lea Leland as Grace Roberts
*Anna Lehr	
*Mabel Wright 
*Ordean Stark as Vivian	
*Robert Taber 	
*Frederic Roberts
*Milton S. Gould as Walter (child actor, grandson of Abramson, later became a prominent attorney)

==Background and reception==
The film was not favorably reviewed by critics, as Robert B. Connellys 1998 volume on silent films succinctly describes it as a "stinker."     has made up, using what license he needed, such as total disregard of American divorce laws, to make his picture entertaining."  Judson also noted the pictures "distinct Semitic atmosphere" despite its supposed farming area setting, which is an astute observation as Jewish writer/producer Ivan Abramson had emigrated from Russia and this was one of his first films.  Actor Leonid Samoloff was an accomplished    (23 April 1915).  , Berkeley Daily Gazette (review appears to be adapted from the Moving Picture World review)   , The Lyceum Magazine (March 1917, p. 57) 

The film was directed by   website, Retrieved October 18, 2011 

Abramsons grandson Milton S. Gould was cast in the role of Graces son Walter. Pizzitola, Louis.  , p. 133 (2002) 

==References==
 

==External links==
* 
*  at American Film Institute

 
 
 
 