1732 Høtten
{{Infobox film
| name           = 1732 Høtten
| image          = 
| image size     =
| caption        = 
| director       = Karin Julsrud
| producer       = 
| writer         = Kjetil Indregard
| narrator       =
| starring       = Reidar Sørensen   Stig Henrik Hoff   Lailia Goody
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 26 December 1998 
| runtime        = 100 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian Thriller thriller film directed by Karin Julsrud, starring Reidar Sørensen Stig Henrik Hoff and Lailia Goody. In the small village of Høtten, six months after the rape and murder of a 13-year-old girl, one of the suspects is found dead. Nicholas Ramm (Reidar Sørensen) has to investigate the crime. The film was Julsruds directorial début. {{cite news|url=http://www.dagbladet.no/nyheter/1998/12/29/151265.html|title=Død på landet
|last=Eirik W.|first=Alver|date=29 December 1998|work=Dagbladet|language=Norwegian|accessdate=7 December 2010}} 

==Reception== dice throw of one and called it a "speculative, purposeless and ice-cold miss".    Eirik W. Alver of Dagbladet was slightly more satisfied with the film, giving it three points. Though he was not bored by the movie, he found the characters and the environment caricatured. 

==References==
 

==External links==
*  
*  

 
 
 
 


 
 