Taiga (film)
{{Infobox film
| name           = Taiga
| image          = 
| image size     = 
| alt          =
| caption        = 
| director       = Ulrike Ottinger {{cite book
|last=Nusser
|first=Tanja mamaya nanga iyan
jessalyn kayle leyes
|title=Von und zu anderen Ufern: Ulrike Ottingers filmische Reiseerzählungen
|url=http://books.google.com/books?id=hLah-CPnudUC&pg=PA231
|accessdate=15 October 2010
|date=1 January 2002
|publisher=Böhlau Verlag Köln Weimar
|language=German
|isbn=978-3-412-17501-6
|page=231}} 
| producer       = 
| writer         = Ulrike Ottinger
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Ulrike Ottinger
| editing        = 
| studio         = 
| distributor    = 
| released       =   {{cite book
|last=Blunden
|first=Jane
|title=Mongolia 2nd
|url=http://books.google.com/books?id=u1Y-51tTJ78C&pg=PA215
|accessdate=15 October 2010
|date=21 August 2008
|publisher=Bradt Travel Guides
|isbn=978-1-84162-178-4
|page=215}} 
| runtime        = 501 min.
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Taiga (1992) is an eight-hour documentary directed and photographed by Ulrike Ottinger.

It focuses on the life and rituals of nomadic peoples in Northern Mongolia, {{cite book
|last=Ebert
|first=Roger
|authorlink=Roger Ebert
|title=Roger Eberts Movie Yearbook 2010
|url=http://books.google.com/books?id=-1aM7D_ymdAC&pg=PA289
|accessdate=15 October 2010
|date=20 October 2009
|publisher=Andrews McMeel Publishing
|isbn=978-0-7407-8536-8
|page=289}}  specifically the Darkhad nomads and the Sojon Urinjanghai. {{cite book
|author=Deutsche Gesellschaft für Asienkunde
|title=Asien
|url=http://books.google.com/books?id=ZxBuAAAAMAAJ
|accessdate=15 October 2010
|year=1993
|publisher=Deutsche Gesellschaft für Asienkunde
|language=German
|page=116}} 

== See also ==
*List of longest films by running time

==References==
 

==Further reading==
*{{cite book
|last=Ottinger
|first=Ulrike
|authorlink=Ulrike Ottinger
|title=Taiga: eine Reise ins nördliche Land der Mongolen
|url=http://books.google.com/books?id=28PvGAAACAAJ
|accessdate=15 October 2010
|year=1993
|publisher=Nishen
|language=German
|isbn=978-3-88940-078-9}}

==External links==
*  
*  

 
 
 
 
 
 
 
 


 