Calvento Files
 

{{Infobox television
  | show_name = Calvento Files
  | image =
  | caption = Documentary drama
  | camera =
  | picture_format = 480i SDTV
  | runtime =  1 hour and 30 minutes
  | creator = ABS-CBN Broadcasting Corp.
  | developer = ABS-CBN
  | starring = Tony Calvento
  | country = Philippines
  | network = ABS-CBN
  | first_aired =    
  | last_aired =    
  | num_episodes = n/a
  | followed_by = Katapat Mayor Fred Lim
  | website =
http://web.archive.org/web/19970616220839/http://www.abs-cbn.com/entertainment/tvshows/dramas/index.html
}}
Calvento Files is a now-defunct crime and investigative show which aired on ABS-CBN from 1995 to 1998. It was hosted by seasoned broadcast journalist Tony Calvento.

==The host==
This is the foremost question. Why? Because the credibility and confidence of the viewing public and every storys authenticity will depend much on him as the direct source of information.

Our awareness of his name can perhaps be attributed to his then-popular column in Peoples Journal. Hotline : By Tony Calvento. The wide following of Tony Calventos column gives us a built-in advantage in terms of media mileage esp. considering that it has a large target audience, C, D, and E.

Tony Calvento has won numerous awards. He is a Presidential awardee for his contribution in crime solution. A Golden Scroll Awardee for Journalism and numerous awards from law-enforcement agencies like the Philippine National Police and the National Bureau of Investigation.

In 1999, Calvento Files won the top award as best docu-drama category in the 1999 New Year Film Festival for the episode "Dying young but not in vain".

==The show==
There is no definite structure or formula for the show that we can really pinpoint since it really aims to present a not too predictable treatment and predictability is precisely what we want to avoid. Writers are actually encouraged to have a more free-flowing attitude towards every story given to them. However, the fear that it might fail to establish its own look and identity that will set it apart from other shows is not really a problem. Its being a docu-drama is enough to stand as its unique selling point.

Moreover, despite the fact that Tony Calvento will never seen on screen as this will endanger his life, a voice-over treatment will be used instead. This serves as his direct involvement in the story which will definitely be always present in every episode. Reminiscent of the show Charlies Angels, the identity of the angels boss was withheld which added mystery, curiosity, and interest for the show.

The show being so popular Tony Calvento then decided to come out on screen and face whatever danger it entails in the performance of his duty.

On the other hand, creation of the storyline by the writer will not depend solely on the articles written by Mr. Calvento but actual interviews with the people involved will also bear weight to ensure well-developed characterization. More importantly, each episode will be meticulously handled by top caliber directors.

For our pilot episode, a very rich story on the Oroquieta massacre was unanimously chosen by the creative group.

==The System/Process of Selecting a Story==
All available articles given by Mr. Calvento are read and from there the group chooses the interesting ones. For the pilot episode, for example, the Oroquieta Massacre was deemed as the most controversial one and as such was chosen for the pilot episode.

Initial research is done by the researcher with Mr. Calventos article as the take off point. Materials are read by the writers and after that, advises the researcher on what other information he needs. On the other hand, the segment producer sets an interview with the people concerned and is provided with questions coming from the writer which he has made based on the materials he read.

After all the interviews have been made, the writer then previews them and then makes a sequence treatment. Said treatment is submitted to the headwriter and the creative consultant. They brainstorm on the treatment, of course, with the supervision of the executive producer, and then give life to the story by coming up with a final sequence treatment.

==Movie version==
{{Infobox film
| name= Calvento Files: The Movie
| image=
| caption =
| writer= Ricky Lee Tony Calvento
| starring= Claudine Barretto Rio Locsin Diether Ocampo Lito Pimentel
| director=Michael de Mesa Laurenti Dyogi
| producer = Trina N. Dayrit Malou N. Santos Charo Santos-Concio
| music= Nonong Buencamino
| cinematography = Ricardo Jacinto Joe Tutanes
| studio= Star Cinema
| distributor= Star Cinema
| released=  
| runtime=129 minutes
| country = Philippines| English 
| movie_series=
| awards=|
}}
* The Star Cinema made the movie version directed by Laurenti Dyogi and Michael De Mesa. It stars Diether Ocampo and Claudine Barretto in the "Balintuwad" (Upside Down) episode directed by Laurenti Dyogi, while John Estrada, Sharmaine Arnaiz and Cris Villanueva starred in "Nay, May Moomoo" (Mother, Theres a Ghost) episode directed by Michael De Mesa.

 
 
 
 
 
 
 