Rabbit's Moon
 
{{Infobox film
| name           = Rabbits Moon
| image          = RabbitsMoon.jpg
| image_size     =
| caption        =
| director       = Kenneth Anger
| producer       =
| writer         =
| narrator       =
| starring       = Claude Revenant André Soubeyran Nadine Valence
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1972 in film|1972, 1979
| runtime        = Short (2 different versions): 16 min. (1972)/7 min. (1979)
| country        = United States English
| budget         =
}}
Rabbits Moon is an avant-garde short film by American filmmaker Kenneth Anger. Filmed in 1950, Rabbits Moon was not completed (nor did it see release) until 1972 in film|1972. Anger re-released the film in 1979, sped up and with a different soundtrack.
 Chinese and Aztec mythology), and his futile attempts to jump up and catch it. Subsequently, another clown (Harlequin) appears and teases Pierrot, showing him Columbina, with whom he appears to fall in love.

==Production==

The sets were borrowed from French filmmaker Jean-Pierre Melville. 

==Music==
 Bye Bye Baby" by Mary Wells, "I Only Have Eyes For You" by The Flamingos and "Tears On My Pillow" by The El Dorados. The 1979 version features only a loop of A Raincoats "It Came In The Night" as its soundtrack.

==Legacy==
 electronic duo Rabbit in the Moon as the inspiration for their name.

==See also==
* Commedia dellarte

==References==
 

==External links==
* 

 
 
 
 
 

 

 