Adajya
 
{{Infobox film
| name           = Adajya
| image          = Adajya.jpg
| image size     =
| caption        = Screenshot
| director       = Santwana Bardoloi
| producer       = Santwana Bardoloi
| writer         = Santwana Bardoloi  Indira Goswami (novel)
| narrator       =
| starring       = Tom Alter Trisha Saikia
| music          =
| cinematography = Mrinalkanti Das
| editing        = A. Sreekar Prasad
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = India Assamese
}}
Adajya is a 1996 Assamese language film directed by Santwana Bardoloi based on a novel by Indira Goswami.

==Plot==
The film, based on Indira Goswamis Dontal Haatir Uiye Khuwa Haoda, is set in 1940s Assam. Three widows struggle to lead dignified lives despite the extreme restrictions mandated by law and custom. The arrival of a young American scholar, a poisonous snakebite, and the theft of ancestral jewelry combine to bring the situation of the young and beautiful widow Giribala to a painful crisis.

==Cast==
*Tom Alter ...  Mark Sahib
*Trisha Saikia ...  Giribala
*Bishnu Kharghoria
*Triveni Bora
*Bhagirothi

==Awards==
*National award- Best Regional Film
*Jurys special award

==External links==
*  
*http://www.3continents.com/3continents/fiche_films.jsp?filmid=126

 
 
 
 
 


 
 