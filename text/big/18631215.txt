What Waits Below
{{Infobox film
| name     = What Waits Below
| image    = Secrets of the Phantom Caverns movie poster.jpg
| caption  = Theatrical release poster using films original title
| director = Don Sharp
| producer = Robert D. Bailey Sandy Howard Don Levin Mel Pearl Jeffrey M. Sneller
| writer   = Ken Barnett Christy Marx Robert Vincent ONeill Richard Johnson Anne Heywood
| music    = Denny Jaeger Michel Rubini
| cinematography = Virgil L. Harper
| editing  = John R. Bowey 
| distributor =  1985
| runtime  = 88 minutes
| country  = United Kingdom
| language = English
| budget   =
}}

What Waits Below is a science-fiction adventure film (initially released under the title Secrets of the Phantom Caverns) released in 1985. Directed by Don Sharp, produced by the Adams Apple Film Company, the film runs for 88 minutes and starred Robert Powell, Timothy Bottoms, and Lisa Blount.  The tagline for the video release of the film was "Underground, no-one can hear you die".

== Plot summary ==
The US military is running a test for a special type of radio transmitter, to be used to communicate with submarines, in a deep system of underground caves in Central America. When the signal from one of the transmitters suddenly disappears, a team of soldiers led by Major Elbert Stevens (Bottoms) and cave specialists led by Rupert Wolf Wolfsen (Powell) including scientist Leslie Peterson (Blount) are sent in to find out what happened. 

Exploring deep underground, they stumble upon a tribe of albino cave-dwellers who have apparently been isolated from the rest of the world for thousands of years. The cave-dwellers are hurt by radio frequencies and are able to see in infra-red frequencies, tracking the explorers by their body heat.

==Production== Cathedral Caverns location sent at least 17 people to the hospital.   Production wrapped in late October 1983. 

==Release==
The film was released on VHS in the United States in November 1985.   

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 