Scoutman
{{Infobox film
| name = Scoutman
| image = 
| image_size =
| caption = 
| director = Masato Ishioka
| producer = 
| writer = Masato Ishioka
| narrator = 
| starring = Miku Matsumoto Hideo Nakaizumi
| music = Kōji Endō
| cinematography = 
| editing = 
| studio = Gold View Company Ltd
| distributor = Argo Pictures Gold View Company Ltd
| released = 
| runtime = 114 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}

  is a 2000 Japanese film written and directed by Masato Ishioka.

==Plot==
Twenty-year-old Atsushi and his seventeen-year-old girlfriend Mari arrive in Tokyo from the suburbs with plans to marry despite their parents objections but they are soon pulled into the citys dark underside. Atsushi meets Miki, a fading adult video (AV) actress, and gets introduced by the veteran Yoshiya to the occupation of "scoutman", recruiters who ply the streets trying to convince young girls to join the AV industry. Meanwhile, Mari, looking for work, runs into Kana who initiates her into the business of selling tickets to swingers parties. The young couple continue to fall deeper into the seductions of Tokyos sex industry and away from each other.

==Cast== Miku Matsumoto as Mari
* Hideo Nakaizumi as Atsushi
* Yuka Fujimoto as Kana
* Akihito Yoshiie as Yoshiya
* Yuri Komuro as Miki
* Shirō Shimomoto as Sugishita
* Kei Morikawa as Director

==Production==
Director Masato Ishioka learned his trade working as an assistant director in the adult video (AV) industry under Tadashi Yoyogi at Athena Eizou for several years.     Other veterans of the adult industry in the film include former AV star Yuri Komuro, playing the part of Miki, as well as actresses Fuka Sakurai ( ),  and Yuna Miyazawa ( )  and AV director Kei Morikawa in small roles. 

==Release== Nakano Musashino Region 2 NTSC All Regions version (as Scout Man) was also produced. 

==Reception==

===Critics===
The film was generally well received by critics, Mark Schilling at the Japan Times Online gives the movie four stars and praises its realistic portrayal of Japans sex industry  while Jasper Sharp calls it "perhaps the most honest and realistic films about the sex industry from the past few years." 

===Awards===
The film won director Ishioka the Directors Guild of Japan New Directors Award for 2001  and the Directors Week Special Jury Award at the 2001 Fantasporto. 

==References==
 

==External links==
*  
*  

 
 