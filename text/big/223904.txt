Mary, Queen of Scots (1971 film)
 
{{Infobox film
| name           = Mary, Queen of Scots 
| image          = Mary queen of scotsposter.jpg theatrical poster
| director       = Charles Jarrott
| producer       = Hal B. Wallis John Hale
| narrator       = 
| starring       = Vanessa Redgrave Glenda Jackson Timothy Dalton Nigel Davenport Patrick McGoohan Trevor Howard Ian Holm John Barry
| cinematography = Christopher Challis
| editing        = Richard Marden
| distributor    = Universal Pictures
| released       = December 1971
| runtime        = 128 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} John Hale and directed by Charles Jarrott. Leading an all-star cast are Vanessa Redgrave as the titular character and Glenda Jackson as Elizabeth I. Jackson had previously played the part of Elizabeth in the BBC TV drama Elizabeth R, screened in February and March 1971, the first episode of which was also written by Hale.
 John Hale and the film directed by Charles Jarrott. Like the play by Friedrich Schiller and the opera by Gaetano Donizetti, it takes considerable liberties with history in order to achieve increased dramatic effect, in particular two fictitious face-to-face encounters between the two Queens (who never met in real life). The film received a less than enthusiastic review from the New York Times, but was nominated for several awards.

==Plot== James Stewart, Lord Morays (Patrick McGoohan) ambitions for rule. He suggests that Mary enjoy herself in Scotland, and pass the time with dancing and feasting. Moray wants to rule Scotland while the lovely but inexperienced Mary becomes a figurehead.
 Robert Dudley Daniel Massey), Lord Darnley (Timothy Dalton), from a powerful Catholic family.  Tempted by the handsome Darnley, Mary impulsively chooses him for marriage. Lord Moray, a Protestant, opposes the marriage, but Mary ignores him. She even exiles Moray to strengthen her own authority. Elizabeth is satisfied that reckless, passionate Marys romantic misadventures will keep her busy in Scotland and give shrewd, practical Elizabeth less to worry about.

Soon after the wedding, spoiled brat Darnley throws a childish temper tantrum, complaining that he has no real power and is merely Marys King Consort. A disillusioned Mary soon banishes Darnley from her bed and frequently consults with the gentle, soft-spoken Italian courtier David Riccio (Ian Holm). Darnley had previously had him as a lover and accuses him of fathering Marys expected child.
 Lord Bothwell (Nigel Davenport). He has been an ally of Mary since her arrival in Scotland.  After he defeats the plotters, Mary forces a truce among their leader Moray, Darnley and Bothwell. Mary gives birth to a son, James I of England|James, who is expected to succeed both Mary and the unmarried, childless Elizabeth.

The peace is short-lived. The weak, selfish Darnley still wants power, though by now he is hideously scarred and already dying of syphilis (the pox). Mary pities him, but finds herself falling in love with the rough but loyal Bothwell. With Morays help, they arrange for Darnley to be killed in a gunpowder explosion at his manor; Darnley escapes before the blast but is stabbed to death. Bothwell marries Mary, and their few brief nights together are blissful. But Moray rejoins the Scottish lords and leads a rebellion against them. He forces Mary to Abdication|abdicate, and she and her husband are driven into exile, Mary to England and Bothwell to Denmark.  Marys young son James is to be crowned King of Scotland (although Moray will effectively rule for years) and raised as a Protestant.
 Sir William Cecil (Trevor Howard), is anxious to get rid of Mary, but Elizabeth fears to set a precedent by putting an anointed monarch to death. She also fears that Marys death might spark a rebellion by her Catholic subjects and cause problems with powerful France and Spain. As a result Mary is doomed to an open-ended captivity. Over time the once proud Queen of Scots succumbs to an empty routine, plotting half-heartedly to escape but growing increasingly comfortable in her luxurious seclusion. She occupies herself with a lazy daily schedule of cards, embroidery and gossip, talking vaguely of escape yet sleeping later and later each morning. Yet while the helpless imprisoned queen has lost all will to harm her enemies, they continue to plot her final destruction.
 Walsingham (Richard Richard Warner), Cecil finds evidence of Marys involvement in the conspiracy to assassinate Elizabeth known as the Babington Plot. Finally Elizabeth confronts Mary, who regains her royal pride and behaves defiantly at their secret meeting. Although Elizabeth offers her mercy if she begs for forgiveness, Mary will not beg for mercy in public.  She endures the trial, conviction and execution. She knows her son James will ultimately succeed to the English throne.

==Historical liberties==
For dramatic effect, the film presents two meetings between the queens, although they never met in life. Moreover, the film depicts Mary as enjoying a late-morning cup of hot chocolate in bed (and even requesting it when she is a prisoner) when this was not a popular drink in the British Isles until well into the 18th century.

==Cast==
* Vanessa Redgrave as Mary, Queen of Scots. Queen Elizabeth I of England
* Patrick McGoohan as Marys half-brother James Stewart, 1st Earl of Moray
* Timothy Dalton as Marys second husband Henry Stuart, Lord Darnley
* Nigel Davenport as Marys third husband, James Hepburn, 4th Earl of Bothwell William Cecil Daniel Massey as Elizabeths lover, Robert Dudley, Earl of Leicester
* Ian Holm as Marys advisor, David Rizzio
* Andrew Keir as Ruthven Robert James as Scottish religious reformer John Knox King Francis II of France
* Katherine Kath as Marys first mother-in-law, Catherine de Medici
* Frances White as Marys companion, Mary Fleming Duke of Guise Cardinal of Lorraine Richard Warner as Elizabeths spy master Francis Walsingham Earl of Morton Brian Coburn Earl of Huntly
* Maria Aitken as Lady Bothwell
* Jeremy Bulloch as Andrew

==Production notes==
The film was shot in France (Château de Chenonceau), Scotland, and England. The song in the opening sequence, "Vivre et Mourir," is sung by Redgrave.  The lyrics are taken from a sonnet written by Mary, Queen of Scots. 

==Reception==
Vincent Canby had little good to say about the film in the New York Times of 4 February 1972, describing it as "a loveless, passionless costume drama".  He wrote, "Unfortunately there is no excitement whatsoever in what Charles Jarrott, the director, and John Hale, the author of the original screenplay, have put together...Mary, Queen of Scots intends, I assume, to illuminate history...yet all its really doing is touching bases, like a dull, dutiful student...Because both Miss Redgrave and Miss Jackson possess identifiable intelligence,   is not as difficult to sit through as some bad movies I can think of. Its just solemn, well-groomed and dumb." 

Roger Ebert gave the movie three stars and lauded the interpretation of Redgrave and Jackson, noting however the "soap opera" approach to the script. 

==Awards and nominations== Best Actress Best Art Peter Howitt), Best Costume Best Music, Best Sound Bob Jones, John Aldred).      

The film received several Golden Globe nominations, including Best Motion Picture - Drama, Best Motion Picture Actress - Drama (Glenda Jackson), Best Motion Picture Actress - Drama (Vanessa Redgrave), Best Original Score (John Barry), and Best Screenplay (John Hale).

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 