Hit-The-Trail Holliday
 
{{Infobox film
| name           = Hit-The-Trail Holliday
| image          =File:Hit the Trail Holliday poster.jpg
| caption        =Film poster
| director       = Marshall Neilan
| producer       = Adolph Zukor Jesse Lasky George M. Cohan John Emerson Anita Loos
| based on       =  
| starring       = George M. Cohan
| music          =
| cinematography = Walter Stradling
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = US
| language       = Silent English inter-titles
}}
Hit-The-Trail Holliday is a lost  1918 silent film comedy directed by Marshall Neilan and starring George M. Cohan in filmization based on his 1915 Broadway play, Hit-the-Trail-Holiday(spelling of the play differs from the film).  Cohan produced the film in conjunction with Famous Players-Lasky.  

==Cast==
*George M. Cohan - Billy Holliday
*Marguerite Clayton - Edith Jason
*Robert Broderick - Otto Wurst Pat OMalley - Kent B. Wurst
*Russell Bassett - Burr Jason
*Richard Barthelmess - Bobby Jason
*William Walcott - Reverend Holden
*Estar Banks - ?

==See also==
*The House That Shadows Built (1931 promotional film by Paramount); a possibility that the unnamed Cohan clip is from Hit-The-Trail Holliday.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 