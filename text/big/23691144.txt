Bells of San Angelo
{{Infobox film
| name           = Bells of San Angelo
| image_size     =
| image	=	Bells of San Angelo FilmPoster.jpeg
| caption        =
| director       = William Witney
| producer       = Edward J. White (associate producer)
| writer         = Paul Gangelin (story) Sloan Nibley (screenplay)
| narrator       =
| starring       = Roy Rogers
| music          = Charles Maxwell
| cinematography = Jack A. Marta
| editing        = Lester Orlebeck
| distributor    =
| released       = 15 April 1947
| runtime        = 78 minutes (original version)   54 minutes (edited version)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 American film Western film mixes half a dozen songs with mystery, international smuggling of silver, violence, a pack of dogs and comedy relief with one character packing a "sixteen shooter".

==Plot==
Roy is a "Border Inspector" ever on the alert for smuggling silver between Mexico and the United States.  Roys Mexican friends have told him that one of their own has important information about a silver mine on the American side of the border, but their contact is shot and killed by the mine guards.  Before shooting him they plant a piece of ore containing a high level of silver on his body.

The American mine owners say they play rough as there are no towns or law enforcement anywhere near them and their mine is just across the border making it a tempting target for robbers who plan to rob the mine then escape to Mexico.

Other events are happening in the area, such as the arrival of Western mystery author Lee Madison who Roy and his friends feel ridicules the West and those who live in it. The gang doesnt know that Lee is really a woman, so when Lee becomes aware of their dislike for her writing, she hides under an alias.

Lionel Bates arrives from England saying Scotland Yard is on the hunt for an English national named George Wallingford Lancaster.  Roy notices the news greatly alarms the dog loving sheriff Cookie Bullfincher.

==Cast==
*Roy Rogers as Roy Rogers Trigger as Trigger, Roys Horse
*Dale Evans as Lee Madison
*Andy Devine as Cookie Bullfincher John McGuire as Rex Gridley
*Olaf Hytten as Lionel Bates
*Dave Sharpe as Henchman Ulrich
*Fritz Leiber as Padre
*Hank Patterson as Deaf Bus Passenger
*Fred Snowflake Toones as The Cook
*Eddie Acuff as Bus Driver
*Bob Nolan as Bob
*Sons of the Pioneers as Musicians
*Pat Brady as Pat 
*Keefe Brasselle as Ignacio 
*Dale Van Sickel as Henchman Mike 
*Luana Walters as Lodge Clerk

==Soundtrack== John Elliott as Jack Elliott) Tim Spencer)
* "I Like to Get Up Early in the Morning" (Written by John Elliott as Jack Elliott)
* "A Cowboys Dream of Heaven" (Written by John Elliott as Jack Elliott)
* "I Love the West" (Written by John Elliott as Jack Elliott) - Dale Evans
* Pat Brady and Sons of the Pioneers - "Hot Lead" (Written by Tim Spencer)

==External links==
* 
* 

 

 
 
 
 
 
 
 


 