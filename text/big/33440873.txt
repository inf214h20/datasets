Nova Zembla (film)
__NOTOC__
 
{{Infobox film
| name           = Nova Zembla
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Reinout Oerlemans
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Robert de Hoog   Derek de Lint   Victor Reinier   Doutzen Kroes   Jan Decleir 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Netherlands
| language       = Dutch
| budget         = 
| gross          = 
}}

Nova Zembla is a 2011 Dutch historical drama film directed by Reinout Oerlemans. It is the first Dutch  feature film in 3D film|3D. 

The film describes the last journey of Willem Barentsz and Jacob van Heemskerk through 1596-1597 when they and their crew tried to discover the North East passage to the Indies. However, due to the sea ice, they are stranded on the island of Novaya Zemlya and have to spend the winter there in Het Behouden Huys (The Safe Home). The story is told through the eyes of Gerrit de Veer, and is loosely based on a diary he published in 1598 after his safe return.  Gerrit is portrayed as having a relationship with the daughter of the astronomer, cartographer and reverend Petrus Plancius, who pioneered the concept of the North East passage to reach the Indies. The Novaya Zemlya effect, first described by De Veer, is shown in the film, albeit in a non-historical fashion.

==Cast==
* Robert de Hoog as Gerrit de Veer
* Derek de Lint as Willem Barentsz
* Victor Reinier as Jacob van Heemskerk
* Jan Decleir as Petrus Plancius
* Doutzen Kroes as Catharina Plancius

== Filming locations ==
The film was filmed on location in Iceland, Belgium, Canada and the Netherlands. The scenes in Amsterdam were filmed in Brugge.

== Soundtrack ==
The Main Theme of the movie has been remixed by Dutch DJ and producer Armin van Buuren

== See also ==
* Boat Trip 3D (2008), the first Dutch digital 3D short film 

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 