Teenage Mother (film)
{{Infobox film
| name = Teenage mother
| image =teenagemother1967.jpg
| caption = Exploitation Drama Poster
| director = Jerry Gross
| producer = Jerry Gross
| writers =  Nicholas Demetroles   Jerry Gross
| starring = Arlene sue Farber   Julia Ange   Howard Lee May
| released =  January 1967
| music = Steve Karmen
| cinematographer = Richard E. Brooks
| distributor= Cinematic Industries, Inc.
| runtime = 78 minutes
| country = United States English
}}
Teenage Mother (a.k.a. The Hygiene Story) is a 1967 American exploitation film directed by Jerry Gross and starring Arlene Sue Farber.  The film is about teenage pregnancy and hygiene. It was billed as "The film that dares to explain what most parents cant."    The film also features a graphic actualization of birth.

==Reception==
An author likened the film to a grindhouse edition of the 2007 film, Juno (film)|Juno.  

In a brief interview on the DVD extras of the 2007 documentary film Heckler (film)|Heckler Comedian Fred Willard, who made his film debut with a small role in Teenage Mother, reports that the audience at one screening of the film booed when his character interrupted an attempted sexual assault of the female lead character.

==See also==
* I Drink Your Blood
* Cinemation Industries

==References==
 

 
 
 
 
 

 