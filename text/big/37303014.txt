God Loves Caviar
 
{{Infobox film
| name           = God Loves Caviar
| image          = 
| caption        = 
| director       = Yannis Smaragdis
| producer       = Eleni Smaragdi
| writer         = Yannis Smaragdis
| starring       = Sebastian Koch
| music          = 
| cinematography = Aris Stavrou
| editing        = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Greece
| language       = English, Greek
| budget         = 
}}

God Loves Caviar ( , Transliteration|translit.&nbsp;O Theós agapáei to chaviári) is a 2012 Greek drama film directed by Yannis Smaragdis.    

==Plot==
The film based on the true story of Ioannis Varvakis, a Greek caviar merchant and benefactor from Psara and former pirate.  He born in Psara and from childhood dealt with the sea navigation, a sector with great tradition in his island. At the age of 17 he built his own ship, which later he offered to the Russians during Orlov Revolt. After the destruction of his ship, he turned to Saint Petersburg and asked an audience from Catherine the Great. So, he was given compensation and an authorization for free fishing in Caspian Sea. Thanks to his skills in navigation, he dominated in sea and soon he became rich. When he discovered the caviar, he captured the idea for trading of this product. From trade of caviar he became millionaire and afterwards he gave part of his fortune for important works that improved the life of Russian and Greeks in the Black Sea coasts. At his last years, he became member of Filiki Eteria that contributed to overthrow the Ottoman rule from Greece. He died in 1825 in Zante, during the Greek War of Independence. After his death he donated his fortune in Ioannis Varvakis Foundation that offered important benefactions in Greece. The script follows the whole life of Varvakis, but started the narration from the last moments of Varvakis in Zante.  

==Cast==
* Sebastian Koch as Ioannis Varvakis
* Evgeniy Stychkin as Ivan
* Juan Diego Botto as Lefentarios
* Olga Sutulova as Helena
* John Cleese as McCormick
* Catherine Deneuve as Empress Catherine II of Russia
* Akis Sakellariou as Kimon
* Alexandra Sakelaropoulou as Varvakis Mother
* Fotini Baxevani
* Lakis Lazopoulos as Fisherman of God
* Pavlos Kontoyannidis
* Alexandros Mylonas as Temporary Prime Minister
* Yannis Vouros as Businessman A

==Reception==
The film was one of the official selections that debuted in 2012 Toronto Film Festival.  In 2013 the film was the highest grossing film in Greece. 

==References==
 

==External links==
*  

 
 
 
 
 