The Serpent (1916 film)
 
{{Infobox film
| name           = The Serpent
| image          = 
| caption        = 
| director       = Raoul Walsh William Fox Raoul A. Walsh (scenario)
| based on       =  
| starring       = Theda Bara James A. Marcus
| music          = 
| cinematography = Georges Benoît
| editing        = 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 silent drama Raoul A. Chimney Rock, North Carolina, and at the Fox Studio in Fort Lee, New Jersey.  It is now considered lost film|lost. 

==Plot==
As described in a film magazine review,  after Vania Lazar is betrayed and debauched by Grand Duke Valanoff, she leaves Russia with no thought except to prey upon the sex that has made her what she is. Then comes the World War I|war, and she sees wounded Russians being taken to the hospital. In one room, she finds Prince Valanoff, the son of her betrayer, and with her wiles she wins his love and then his name. When the Grand Duke comes to visit, his son the Prince is absent. Not recognizing the new Vania, the Grand Duke responds to her lure, and the son discovers his own father as the betrayer of his happiness.

==Cast==
* Theda Bara as Vania Lazar
* James A. Marcus as Ivan Lazar
* Lillian Hathaway as Martsa Lazar
* Charles Craig as Grand Duke Valanoff
* Carl Harbaugh as Prince Valanoff
* George Walsh as Andrey Sobi
* Nan Carter as Ema Lachno
* Marcel Morhange as Gregoire
* Bernard Nedell

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 