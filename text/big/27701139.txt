The Psychopath
{{Infobox film
| name           = The Psychopath
| image          = The Psychopath german poster.jpg
| image_size     = 
| caption        = German poster
| director       = Freddie Francis producer = Max Rosenberg Milton Subotsky
| writer         = Robert Bloch
| narrator       = 
| starring       = Patrick Wymark
| music          = Elisabeth Lutyens
| cinematography = 
| editing        = 
| studio         = Amicus Productions
| distributor    = 
| released       = May 1966
| runtime        = 82 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Psychopath is a 1966 film directed by Freddie Francis and written by Robert Bloch. It stars Patrick Wymark and Margaret Johnston.  
==Plot==
A police inspector investigates a string of murders where the victims have dolls attached to their bodies. The trail soon leads to one Mrs. Von Sturm, who knows a set of dark secrets that may hold the key to the murders.

==Cast==
* Patrick Wymark as Inspector Holloway
* Margaret Johnston as Mrs. Von Sturm
* John Standing as Mark Von Sturm
* Alexander Knox as Frank Saville
* Judy Huxtable as Louise Saville
* Don Borisenko as Donald Loftis
* Thorley Walters as Martin Roth
* Robert Crewdson as Victor Ledoux
* Colin Gordon as Dr. Glyn Tim Barrett as Morgan John Harvey as Reinhardt Klermer Harold Lang as Briggs
==Production==
The film was originally known as Shizo. Shooting started September 1965. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 40-42 

The Psychopath was an attempt to capitalize on the success of Hammer Films recent series of psychological thrillers, including Taste of Fear.   accessed 23 February 2014 

==Reception==
The film was very popular in Europe, particularly Italy. 

==References==
 

==External links==
* 
*  at TCMDB

 
 
 

 
 
 
 
 
 
 
 

 