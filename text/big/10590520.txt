Twins Mission
 
 
{{Infobox Film
| name           = Twins Mission
| image          = Twins-Mission-poster.jpg
| image_size     = 
| caption        = Hong Kong film poster
| director       = Kong Tao-Hoi
| producer       = Tsui Siu-Ming Amy Li
| writer         = Fong-Sai Keung Tsui Siu-Ming
| narrator       =
| starring       = Charlene Choi Gillian Chung Sammo Hung
| music          =
| cinematography = Ko Chiu-Lam
| editing        = Wong Wing-Ming Emperor Motion Picture Group
| released       =  
| runtime        = 99 min
| country        = Hong Kong
| language       = Cantonese
| budget         = IIA
}}
 martial arts-Hong action choreographer Wu Jing spoof of Twins and leaves the audience with a cliffhanger ending.

==Plot==

An evil gang of twins hold up a train to steal a magical Tibetan artifact, the Heavens Bead from 3 dalai lamas. This artifact has healing powers and is a highly desirable item. A battle ensues and the artifact is knocked from the hands of a parachuting villain into the bag of an unsuspecting passerby.

The passerby, oblivious, gets into his van and heads to Hong Kong. This is where the other set of twins come in; they are aided by the twin Laus and Uncle Luck as they try to get the Heavens Bead back.

A secret mission takes place, in which the good twins infiltrate a high rise building in order to recover the item. Although they initially succeed, the artifact is then passed to the wrong twin. Happy, Lilians sister, who is suffering from cancer is also kidnapped and held prisoner in the high-rise building.

The good twins eventually recover the Heavens Bead and lose it again while rescuing Happy. The villains get away with the Heavens Bead.

==Cast==

*Gillian Chung as Pearl
*Charlene Choi as Jade
*Sammo Hung as Uncle Luck Wu Jing as Lau Hay and Lau San
*Yuen Wah as Chang Chung and Chang Yung
*Jess Zhang as Lilian Li Steven Cheung as Fred
*Qiu Lier as Happy
*Sek Sau as Professor Mok Sam Lee as Officer Lam
*Andy Liang as Wan Pau
*Cody Liang as Wan Lung
*Mona as Mona
*Ammar as Ahao
*Lisa as Lisa
*Long Fei as Kitten
*Long Ze as Puppy
*Albert Leung as Iron Head
*Herbert Leung as Bronze Head
*Lam Ho as Marco
*Lam Kit as Polo
*Liang Yueyun as Miao Kam Fung
*Bobby Yip as Miao Yam Fung Chen Hao

==External links==
*  

 
 
 
 
 
 


 
 