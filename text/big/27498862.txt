Cactus (1986 film)
 
 
{{Infobox film
| name           = Cactus
| image          = Cactus 1986.jpg
| caption        = Film poster
| director       = Paul Cox
| producer       = Paul Cox
| writer         = Paul Cox Bob Ellis
| starring       = Isabelle Huppert
| music          = 
| cinematography = Yuri Sokol
| editing        = Tim Lewis
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Australia
| language       = English
| budget         = A$1.5 million 
| gross = A$73,751 (Australia) 
}}

Cactus is a 1986 Australian drama film directed by Paul Cox and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert as Colo Robert Menzies as Robert
* Norman Kaye as Tom
* Monica Maughan as Bea
* Banula Marika as Banduk
* Sheila Florance as Martha
* Peter Aanensen as George
* Julia Blake as Club Speaker
* Jean-Pierre Mignon as Jean-Francois Ray Marshall as Kevin
* Maurie Fields as Maurie Sean Scully as Doctor
* Dawn Klingberg as Pedestrian
* Kyra Cox as Sister
* Tony Llewellyn-Jones as Father

==Production==
The film was inspired by Paul Coxs memory of his mother temporarily going blind when he was a child. He worked on the story for a number of years, then decided he wanted to work with Isabelle Huppert and wrote the script with her in mind. Bob Ellis worked on the script but fought with Cox over the fact it was about a Frenchwoman staying in Australia, and the two men ended their collaboration. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p95-96 

Actors Equity objected to Hupperts importation but this was overruled. 

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 
*  at Australian Screen Online

 
 

 
 
 
 
 
 

 
 