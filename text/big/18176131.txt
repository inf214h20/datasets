Texas Night Train
 
{{Infobox film
| name           = Texas Night Train
| image          = TexasNightTrain.jpg
| caption        = Poster art
| director       = Shanti Guy
| producer       = Shanti Guy Kathryn Guy Dan Huber
| writer         = Shanti Guy
| starring       = Chuck Huber Lydia Mackay Lloyd W.L. Barnes Jr.
| music          = Time Starks/Byron Richard Hood Dan Phillips
| release        = 
| cinematography = Shanti Guy
| editing        = Shanti Guy
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| distributor    = Dope Scope Productions
}}
Texas Night Train is a 2001 independent horror film by Fort Worth, Texas-based artist and first-time filmmaker Shanti Guy.

==Plot==
 voodoo witch Mae (Lydia Mackay). One night at a bar, she drugs his drink and he passes out.  When he awakes the next morning, Jake finds one of his kidneys has been cut from his body.  He is also handcuffed to a railroad track.  Freeing himself, he hops on a passing freight train and encounters a mysterious hobo (Lloyd W.L. Barnes, Jr.) who shares the secret of Mae’s activities.  Jake goes into seclusion for years, then returns to seek his revenge on Mae.   

==Production and reception==
Texas Night Train was produced on a US$7,200 budget.  According to the filmmaker, it was inspired by the rockabilly song "Moppin’ the Floor with My Baby’s Head." 

The film had its theatrical premiere in New York City on September 21, 2001, and reviews were overwhelmingly negative. Rob Nelson, writing in the Village Voice, called it "repugnant" and "a slow train to nowhere."  Lawrence Van Gelder, reviewing the film for The New York Times, said the production "amounts to about 10 minutes of storytelling in 75 minutes of repetitive film, ill-defined characters, wretched acting and droning voice-over maunderings."  Rob Firsching’s review at the Amazing World of Cult Movies web site stated: "Every copy should be destroyed and Shanti Guy should be legally prohibited from ever picking up a camera again." 

Despite the negative critical reaction, filmmaker Shanti Guy told the media that audiences were more receptive. As he stated in an interview: “There are three standard reactions that I got. Number one: we are proud of you whatever you do. Number two: that was some much cooler than I thought it would be. And number three: so, what was it about? I plan to try the film in some festivals and see if it gets picked up there. If nothing happens with that, I will distribute it myself in the hopes that it will become an underground rockabilly cult film.” 

To date, Texas Night Train has not been released on DVD.

==References==
 

==External links==
*  

 
 

 
 
 