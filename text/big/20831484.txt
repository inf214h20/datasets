Buddies (1983 film)
 
 
{{Infobox Film name           = Buddies  image          =  caption        =  producer       = John Dingwall  director       = Arch Nicholson writer         = John Dingwall  starring  Harold Hopkins music  Chris Neal cinematography = David Eggby editing        = Martyn Down distributor    =   released       = 1983 running time   = 97 minutes country        = Australia language       = English  budget         = A$1.9 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p88-89  gross = A$81,777
}} AFI Award for the script.  The film was not a great success in 1983, as no Australian distributor wanted to release it, but Dingwall took it around the country cinemas himself, where it was well received. 

==Plot==
Young miners Mike and Johnny (Colin Friels and Harold Hopkins) work in the gem fields of central Queensland around Emerald, Queensland|Emerald. Conflict arises when their pick-and-shovel operation is threatened by a large scale bulldozer operator.

==Principal cast==
*Colin Friels as Mike
*Kris McQuade as Stella Harold Hopkins as Johnny
*Simon Chilvers as Alfred
*Norman Kaye as George
*Lisa Peers as Jennifer
*Bruce Spence as Ted
*Andrew Sharp as Peter
*Dinah Shearing as Merle

==Production==
John Dingwall wrote the script and decided to produce it himself. He raised the money with the help of Rex Pilbeam, a former mayor of Rockhampton. Most of the money was raised in Queensland, including investment from the Queensland Film Corporation. Shooting took place on location in Emerald, Queensland and lasted six weeks. 

==Box Office==
According to Dingwall, the film tested extremely well with audiences but there was a difficulty in marketing it.  Buddies grossed $81,777 at the box office in Australia,  which is equivalent to $215,891
in 2009 dollars.

==Awards and nominations== Chris Neal) categories. John Dingwall won the Best Original Screenplay AFI Award for the script. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
*Buddies at the  
*  at Oz Movies
 
 
 
 
 
 
 


 
 