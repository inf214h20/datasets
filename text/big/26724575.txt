Something for the Birds
{{Infobox film
| name           = Something for the Birds
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wise
| writer         = 
| narrator       = 
| starring       = Victor Mature
| music          = 
| cinematography = Joseph LaShelle
| editing        = 
| distributor    =  
| released       = October 1952
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Something for the Birds is a 1952 film directed by Robert Wise. It stars Victor Mature and Patricia Neal.  

==Cast==
* Victor Mature as Steve Bennett
* Patricia Neal as Anne Richards
* Edmund Gwenn as Admiral Johnnie Adams
* Larry Keating as Roy Patterson
* Gladys Hurlbut as Della Rice

==References==
 

==External links==
*  

 

 
 