Crime and Punishment (1970 film)
{{Infobox film name = Crime and Punishment image = Crime and Punishment (1970 film).jpg image_size = caption = Theatrical release poster director = Lev Kulidzhanov producer =  writer = Lev Kulidzhanov Nikolai Figurovsky Fyodor Dostoyevsky (novel) narrator =  starring = Georgi Taratorkin Innokenti Smoktunovsky music = Mikhail Ziv cinematography = Vyacheslav Shumsky editing =  distributor =  studio = Gorky Film Studio released = 21 September 1970 runtime = 221 minutes country = Soviet Union language = Russian budget = 
}} Soviet film eponymous novel by Fyodor Dostoevsky.

==Cast==
* Georgi Taratorkin - Rodion Romanovich Raskolnikov
* Innokenty Smoktunovsky - Porfiry Petrovitch
* Tatyana Bedova - Sonya Marmeladova
* Victoria Fyodorova | Viktoriya Fyodorova - Avdotya Romanovna
* Yefim Kopelyan - Svidrigailov
* Yevgeni Lebedev - Marmeladov
* Maya Bulgakova - Yekaterina Ivanovna
* Irina Gosheva - Pulkheriya Aleksandrovna
* Vladimir Basov - Luzhin
* Aleksandr Pavlov - Razumikhin
* Vladimir Belokurov		
* Inna Makarova - Nastasya
* Sergei Nikonenko - Nikolai		
* Valeri Nosik - Zametov
* Dzidra Ritenberga - Luiza Ivanovna
* Ivan Ryzhov - Tit Vasilievich
* Yuri Sarantsev - Lieutenant Powder
* Lyubov Sokolova - Yelizaveta

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 