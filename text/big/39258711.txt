I Do (2012 American film)
{{Infobox film
| name           = I Do
| image          = http://ia.media-imdb.com/images/M/MV5BMTAxNzg2MTM5NjdeQTJeQWpwZ15BbWU3MDM4NjQ1MTg@._V1_SY317_CR2,0,214,317_AL_.jpg
| alt            = 
| caption        = 
| director       = Glenn Gaylord
| producer       = David W. Ross
| writer         = David W. Ross
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = July 18, 2012
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American drama film, released in 2012. Directed by Glenn Gaylord and written and produced by David W. Ross, the film stars Ross as Jack Edwards, a gay artist from England living and working in New York City. Following the death of his brother Peter (Grant Bowler), he enters into a green card marriage with his lesbian best friend Ali (Jamie-Lynn Sigler) so that he can stay in the country to help his widowed sister-in-law Mya (Alicia Witt), but is then forced to confront the unequal status of same-sex marriage as he meets and falls in love with Mano (Maurice Compte).

The film premiered at Los Angeles Outfest on July 18, 2012,  and was screened at several LGBT and mainstream film festivals in late 2012 and early 2013. It had a general theatrical release on May 31, 2013. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 