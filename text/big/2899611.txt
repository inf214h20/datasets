A Conversation with Norman
 
 
{{Infobox film
| name           = A Conversation with Norman
| image          = 
| caption        = 
| director       = Jonathan M. Parisen
| producer       = C.M. Murphy Jonathan M. Parisen
| writer         = Jonathan M. Parisen
| screenplay     = 
| story          = 
| based on       =  
| starring       = Christopher Englese Grace Orosz Tom Loggins Michael Sean Regan
| music          = 
| cinematography = 
| editing        = Jonathan M. Parisen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 homage to Alfred Hitchcocks Psycho (1960 film)|Psycho.

It took six years on and off for the filmmaker to get the film made due to problems with sets and casting.  It premiered at Anthology Film Archives in New York City on June 13, 2005 just three days before the forty-fifth anniversary of the New York premiere of Psycho.
Parisen announced on premiere night that the film would play that night and never be shown again. Parisen promised that the film would never make it to VHS or DVD and would never be shown on television. He said he was doing this to make the premiere night more of an event for the viewing audience. To date Parisen has kept that promise.

==Cast==
The film stars Christopher Englese as Norman, Grace Orosz as Marion, radio talk show host Bob Marrone as Doctor Richmond and Tom Loggins as Sam.

==External links==
* 

 
 
 


 