Phone (film)
{{Infobox film  name           = Phone image          = Phone film poster.jpg caption        = Theatrical poster director       = Ahn Byeong-ki producer       = Ahn Byeong-ki writer         = Ahn Byeong-ki   Lee Yu-jin starring  Kim Yoo-mi music          = Lee Sang-ho  cinematography = Mun Yong-sik  editing        = Park Soon-duk distributor    =  released       =   runtime        = 103 minutes country        = South Korea language       = Korean budget         =  gross          =   
| film name      = {{Film name hangul         =   rr             = Pon mr             = Pon}}
}} 2002 South Kim Yoo-mi.  The film is a complex and disturbing love story that involves possession and ghosts.

==Plot==
After writing a series of articles about a pedophilia scandal, journalist Ji-won receives threatening calls on her cellular and subsequently changes her number. Her close friend Ho-jung and her husband Chang-hoon invite Ji-won to move to their empty house in Bang Bae that is newly furnished. When Ho-jungs daughter Young-ju answers an anonymous phone call on Ji-wons new cell phone, the girl screams and begins to show a disturbing attraction for her father and jealous rejection towards her mother. Meanwhile Ji-won receives more mysterious phone calls and sees a teenager playing Beethovens "Moonlight Sonata" on the piano. After investigating her phone number, Ji-won discovers that the original owner of the number, Jin-hee, had vanished and that the two subsequent owners of the number had died mysteriously in unusual circumstances. Her further investigation about Jin-hee reveals that the teenage schoolgirl was obsessively in love with a man who had broken up with her.

Soon, Ji-won finds out Jin-hee was in a relationship with Chang-hoon. Ho-jung found out about the affair and confronted Jin-hee. During their confrontation, Jin-hee mocked Ho-jungs infertility problem and told Ho-jung that she was pregnant with Chang-hoons baby. In a struggle between the two women, Ho-jung accidentally pushed Jin-hee down the stairs and killed her. Ji-won also discovers that Jin-hees dead body was hidden inside one of the walls at the house she is staying at. However, before Ji-won can go out for help, Ho-Jung arrives and knocks her out, preparing to kill her. It is also revealed that Ho-jungs husband, Chang-hoon, commits suicide.

Ho-jung plans to burn the body of Jin-hee and the unconscious Ji-won with gasoline. However, Jin-hees spirit awakens and takes revenge on Ho-jung, thus saving Ji-won. The film ends with Ji-won dropping the cursed cell phone into the ocean, which rings after entering the water.

==Cast==
* Ha Ji-won...  Ji-won, a young journalist Kim Yoo-mi... Ho-jung
* Choi Woo-jae ... Chang-hoon
* Choi Ji-yeon ... Jin-hee
* Eun Seo-woo ... Young-ju
* Choi Jung-yoon ... Min Ja-young

==Remake== American remake in 2009. 

==See also==
*K-Horror

==References==
 

==External links==
* 
* 
* 
*   

 

 
 
 
 
 
 

 
 