Less Than Zero (film)
{{Infobox film
| name           = Less Than Zero
| image          = Less than zero 1987 poster.jpg
| image_size     = 200px
| alt            = 
| caption        = Theatrical release poster
| director       = Marek Kanievska
| producer       = Jon Avnet Jordan Kerner Marvin Worth
| screenplay     = Harley Peyton
| based on       =  
| starring       = Andrew McCarthy Jami Gertz Robert Downey, Jr. James Spader
| music          = Thomas Newman
| cinematography = Edward Lachman
| editing        = Peter E. Berger Michael Tronick
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English Spanish
| budget         = 
| gross          = $12,396,383 
}} novel of drug addict. The film presents a look at the culture of wealthy, decadent youth in Los Angeles.

Less Than Zero received mixed reviews among critics. Ellis hated the film initially but his view of it later softened. He insists that the film bears no resemblance to his novel and felt that it was miscast with the exceptions of Downey and James Spader.

==Plot==
Clay Easton (Andrew McCarthy) is a straitlaced college freshman on the east coast, who returns home to Los Angeles, California, for Christmas to find things very different from the way he left them. His high school girlfriend, Blair (Jami Gertz), has become addicted to drugs and has been having sex with his high school best friend, Julian Wells (Robert Downey, Jr.). Julian, whose life has gone downhill after his startup record company falls apart, has become a drug addict. Hes also been cut off by his family for stealing to support his habit and reduced to homelessness. Julian is also being hassled by his dealer, Rip (James Spader) an old classmate, for a debt of $50,000 that he owes to him.
 prostitute to work off the debt. After suffering through a night of being sick from not being able to score drugs and hiding from Rip, Julian decides to quit and begs his father (Nicholas Pryor) to help him. He then tells Rip the next day his plans for sobriety, which Rip does not believe and lures Julian back into doing drugs and hooking. Clay finds Julian and rescues him; after a violent confrontation with Rip and his henchman, Clay, Julian and Blair all escape and begin the long drive to the desert so Julian can attempt to achieve sobriety once and for all. However, the damage has already been done; the next morning Julian dies from heart failure in the car.

After Julians funeral, Clay and Blair are sitting on a cemetery bench reminiscing about him. Clay then tells Blair he is going back east and wants her to go with him, to which she agrees. We see the snapshot of the three of them at graduation—the last time all three of them were ever happy together.

==Cast==
* Andrew McCarthy as Clay Easton
* Jami Gertz as Blair
* Robert Downey, Jr. as Julian Wells
* James Spader as Rip
* Nicholas Pryor as Benjamin Wells
* Tony Bill as Bradford Easton
* Donna Mitchell as Elaine Easton Michael Bowen as Hop Sarah Buxton as Markie
* Jayne Modean as Cindy
* Lisanne Falk as Patti
* Michael Greene as Robert Wells
* Brad Pitt (uncredited) as Partygoer / Preppie Guy at Fight
* Christopher Maleki (uncredited) as Guy at Party
* Jackie Swanson as Party Girl (uncredited)

==Production==
Ellis book was originally optioned by producer Marvin Worth for $7,500 before its publication in June 1985 with the understanding that 20th Century Fox would finance it.   

The purchase was sponsored by Scott Rudin and Larry Mark, Vice Presidents of Production. The book went on to become a best seller but the producers had to create a coherent story and change Clay, the central character, because they felt that he was too passive.  They also eliminated his bisexuality and casual drug use. Worth hired Pulitzer Prize-winning playwright Michael Cristofer to write the screenplay. He stuck close to the tone of the novel and had Clay take some drugs but did not make him bisexual. The studio felt that Cristofers script was too harsh for a commercial film. 

Fox then assigned the film to producer Jon Avnet who had made Risky Business. He felt that Cristofers script was "so depressing and degrading."  Avnet instead wanted to transform "a very extreme situation" into "a sentimental story about warmth, caring and tenderness in an atmosphere hostile to those kinds of emotions".    Studio executives and Avnet argued over the amount of decadence depicted in the film that would not alienate audiences. Larry Gordon, President of Fox, and who had approved the purchase of the book, was replaced by Alan Horn who was then replaced by Leonard Goldberg. Goldberg found the material distasteful but Barry Diller, the Chairman of Fox, wanted to make the film. 
 Another Country. The studio wanted to appeal to actor Andrew McCarthys teenage girl fans without alienating an older audience. 

Cinematographer Edward Lachman remembers that originally the film was a lot "edgier" and that the studio took it away from Kanievska.    He also recalled a scene he shot with the music group Red Hot Chili Peppers: "The Red Hot Chili Peppers were in that film and the studio became very conservative and they said, Oh the band, theyre sweaty and they dont have their shirts on. They destroyed an incredible Steadicam shot, all because they had to cut around them being bare-chested". 

At an early test screening, the studio recruited an audience between the ages of 15 and 24; they hated Robert Downey Jr.s character.  As a result, new scenes were shot to make his and Jami Gertzs character more repentant. For example, a high school graduation scene was shot to lighten the mood by showing the three main characters as good friends during better times. 

==Reception==

===Box office=== Hello Again Baby Boom s fifth weekend. It went on to gross $12,396,383 in North America. 

===Critical response===
   The Rules of Attraction, which he gave a BOMB rating to.

Review aggregation website Rotten Tomatoes gave it a score of 54% based on a weighted average of 24 reviews.   Flixster   
In the New York Times, Janet Maslin wrote, "Mr. Downey gives a performance that is desperately moving, with the kind of emotion that comes as a real surprise in these surroundings."  
Rita Kempley, in her review for the Washington Post, called the film, "noodle-headed and faint-hearted, a shallow swipe at a serious problem, with a happily-ever-after ending yet."   Antonioni making a high-school public-service movie and youll have an inkling of the movies high-toned silliness." 
In the Chicago Sun-Times, Roger Ebert gave Less Than Zero a four-star review, noting that the "movie knows cocaine inside out and paints a portrait of drug addiction that is all the more harrowing because it takes place in the Beverly Hills fast lane...The movies three central performances are flawless...  acting here is so real, so subtle and so observant that its scary...The whole movie looks brilliantly superficial, and so Downeys predicament is all the more poignant: He is surrounded by all of this, he is in it and of it, and yet he cannot have it".   
    New York David Denby wrote, "In many ways, Less than Zero is a cynical, manipulative job. Yet, the movie has something great in it, something that could legitimately move teenagers (or anyone else): Robert Downey Jr. as the disintegrating Julian, a performance in which beautiful exuberance gives way horrifying to a sudden, startled sadness".   

Upon its initial release, Ellis hated the film. While promoting the book Lunar Park he said he has gotten very "sentimental" about it    and has "really warmed up to it now. Ive accepted it".    
He admits that the film bears no resemblance to his novel but that it captured, "a certain youth culture during that decade that no other movie caught", and felt that it was miscast with the exceptions of Downey and Spader.  Furthermore he has said, "I think that movie is gorgeous, and the performances that I thought were shaky seem much better now. Like, Jami Gertz seems much better to me now than she did 20 years ago. It’s something I can watch".    
The film was voted as the 22nd best film set in Los Angeles in the last 25 years by a group of Los Angeles Times writers and editors with two criteria: "The movie had to communicate some inherent truth about the L.A. experience, and only one film per director was allowed on the list". 

==Sequel==
On April 14, 2009, MTV News announced that Ellis had nearly finished Imperial Bedrooms, his seventh novel and the sequel to Less Than Zero. Ellis has revealed that the films main characters are all still alive in the present day, and has already begun looking ahead to the possibility of a film adaptation. Ellis feels that interpreting it as a sequel to the 1987 Less Than Zero adaptation "would be a great idea" and hopes to be able to reunite Spader, McCarthy, and Gertz should Fox option the sequel.    

==Soundtrack==
 
A soundtrack containing a variety of music types was released on November 6, 1987 by Def Jam Recordings. It peaked at 31 on the Billboard 200|Billboard 200 and 22 on the Top R&B/Hip-Hop Albums and was certified gold on February 8, 1988.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 