The Crush (1993 film)
{{Infobox film name           = The Crush image          = Crushposter93.jpg caption  Promotional film poster director       = Alan Shapiro producer       = James G. Robinson Gary Barber writer         = Alan Shapiro starring  Jennifer Rubin Kurtwood Smith Alicia Silverstone music          = Graeme Revell cinematography = Bruce Surtees editing        = Ian Crafford distributor    = Morgan Creek Productions Warner Bros. Pictures released       = April 2, 1993 runtime        = 89 minutes country        = United States language       = English budget         = $6 million gross          = $13,609,396 (United&nbsp;States)
}}
The Crush is a 1993 American psychological thriller film written and directed by Alan Shapiro, which stars Cary Elwes and Alicia Silverstone in her feature film debut. It was filmed on location from 24 September 1992 - 20 November 1992  in Vancouver, British Columbia.

In editing the film for broadcast TV, the character of Darians name was changed to Adrian after a lawsuit against Shapiro by the real-life Darian Forrester.  The VHS and laserdisc versions of the film still use the original name, but DVD releases and later cable TV airings also change the name to Adrian. The plot of The Crush was based on an actual incident involving the neighbor of writer Shapiro. 

==Plot==
Arriving in Seattle, Washington, writer Nick Eliot (Cary Elwes) secures a job at Pique magazine and lodging in a guest house belonging to Cliff and Liv Forrester (Kurtwood Smith and Gwynyth Walsh). The handsome Nick soon makes the acquaintance of the Forresters 14-year-old daughter Adrian (Alicia Silverstone), a precocious girl who develops an intense attraction to him. She secretly helps Nick by sneaking into his room and rewriting one of his Pique stories, which subsequently wins a rave from his editor/boss, Michael (Matthew Walker). At a party thrown by the Forresters, Nick agrees to accompany the lonely girl on a nighttime drive to a romantic spot, where she kisses him.

This intensifies Adrians crush on Nick, but he quickly wises up and attempts to put her off, having begun a budding romance with coworker Amy (Jennifer Rubin). Adrian continues to boldly pursue him, even going so far as to undress in his view while he is hiding in her closet. Nick, however, continues to rebuff her advances, and Adrians actions become destructive. She defaces a car hes restored and erases his computer discs, yet hes unable to convince Cliff and Liv of whats going on. Cheyenne (Amber Benson), a friend of Adrian who tries to warn Nick about her, meets with an "accident" at the riding school they attend together. After Adrian spies on Amy in bed with Nick, the girl locks Amy in her darkroom and empties a wasps nest into the vents.

Amy survives, and Nick, now convinced that Adrian is big trouble, attempts to find new lodging. However, Adrian manages to sabotage his efforts. She accuses him of sexually assaulting her with "evidence" obtained from a used condom from Nicks trash, leading to his arrest. After Michael bails him out (and fires him), Nick is met once again by Cheyenne. She informs Nick that she knows he did not do anything to Adrian, and that Adrian had behaved like this before to a previous crush, a camp counselor named Rick who "accidentally" died by eating something poisonous. Cheyenne also informs Nick of a diary Adrian kept that can acquit him.

When Cheyenne leaves, Nick goes looking for her when he hears strange noises from the Forresters house. He discovers Cheyenne tied up in the attic and is confronted by Adrian and then Cliff, who attacks him. Adrian, still infatuated, attacks her father, leaving Nick free to subdue her with one punch. Acquitted, Nick goes to live with Amy while Adrian, confined to a psychiatric ward, wishes to express her remorse for what she has done to Nick. Her doctor comments that she is making good progress, unaware she is developing a crush on him. The final scene is her going back into her psychiatric hospital room. She takes a look at her new doctors wedding photo, and shares an evil glare at the camera.

==Reception==

The film has a 21% rating from critics on rottentomatoes.com and 77% of audiences have expressed an interest to view the film.  As of 28 June 2014, Warner Bros. have failed to release the film on any DVD or Blu-ray Disc format in the United Kingdom, having only been issued on rental and sell-through VHS in 1995. 

==Cast==
*Cary Elwes as Nicholas "Nick" Eliot
*Alicia Silverstone as Adrian Forrester Jennifer Rubin as Amy Maddik
*Kurtwood Smith as Cliff Forrester
*Gwynyth Walsh as Liv Forrester
*Amber Benson as Cheyenne Matthew Walker as Michael

==Awards and nominations==
{| class="wikitable"
! Association
! Category
! Person
! Result
|- MTV Movie Award Best Breakthrough Performance
| Alicia Silverstone
|  
|- Best Villain
| Alicia Silverstone
|  
|- Most Desirable Female
| Alicia Silverstone
|  
|- Young Artist Award
|  
| Alicia Silverstone
|  
|}

==References==
 

==External links==
*   film collective resource
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 