Mortadelo and Filemon. Mission: Save the Planet
{{Infobox film
| name           = Mortadelo and Filemon. Mission: Save the Planet
| film name      =  
| image          =
| image_size     =
| caption        =
| director       = Miguel Bardem
| producer       = Antena 3 Films
| writer         = Carlos Martín Juan V. Pozuelo Miguel Bardem
| starring       = Edu Soto - Mortadelo Pepe Viyuela - Filemón
| cinematography =
| editing        = Iván Aledo
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = Spain
| language       = Spanish
}} Spanish comedy film based on the popular Spanish comic book series Mortadelo y Filemón by Francisco Ibáñez Talavera starring Eduardo Soto and Pepe Viyuela in the lead roles.  It is the second live action movie of the characters, the first being La gran aventura de Mortadelo y Filemón.

==Plot==

Coinciding with the worst drought ever, much of the worlds water is polluted. The evil Botijola plans against the meeting of the worlds top agents, intending to eliminate them all. His intention is to replace all water in the world with the awful beverage that carries his name, that contains no water in its formula. To accomplish this, he plans to abduct the scatterbrained professor Bacterio who has invented a machine that converts water into energy and moves it elsewhere, to get him to work for him. He does this by using quick change artist Todoquisque disguised as Filemón. Nevertheless, there is one thing that Botijola did not count on. Bacterio has thwarted him by hiding the pieces of his invention in different historic moments, utilizing a time machine he built.

Mortadelo and Filemón have fallen out and are working minor jobs in the city, which they manage to mess up. The pair are reunited to solve the case. They use Bacterios time machine to go to ancient Rome, the Coliseum and after Filemon beats a giant gladiator, they pick up the first part, which is then stolen by Chulin who works for Botijola. They then go back to the Spanish Inquisition|Inquisition, meet Torquemados and set prisoners free before getting the second piece, also stolen by Chulin. They then go to Botijolas desert HQ to try and stop him, with mixed success till Ofelia steps in and cuts off gravity with another Bacterio invention. A large area of water has been dried up but thanks to Mortadelo, it is returned to the city instead of where it comes from and floods the city so much that many buildings are submerged.

==Cast==

* Eduardo Soto as Mortadelo
* Pepe Viyuela as Filemón
* Mariano Venancio as Superintendent Vincente (their boss at T.I.A.)
* Janfri Topera as Professor Bacterio
* Berta Ojea as Ofelia (Vincentes secretary) Carlos Santos as Botijola
* Alex ODogherty as Todoquisque
* Toás Pozz as Chulin
* Andrés Gasch as Matraca
* Carmen Ruiz as Mortadelos sister

==Other==
The film is 98 minutes long and is out on DVD, with Spanish language and Spanish subtitles. As in the comics, Soto towers over Viyuela and is often seen bent over talking to him. They are helped by a CGI dog named Bush, who has human traits.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 