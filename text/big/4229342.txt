The Work and the Story
 
{{Infobox film
| name = The Work and the Story
| image = WorkAndTheStory.jpg
| director = Nathan Smith Jones
| producer = Miriam Smith
| writer = Nathan Smith Jones,  Dan Merkley
| starring = Nathan Smith Jones,  Jen Hoskins,  Eric Artell,  Dan Merkley,  Richard Dutcher
| distributor = Off-Hollywood Distribution
| released = 2003
| runtime = 77 min.
| language = English
| music = Big Idea Music
| awards =
| budget = $103,000
| tagline        = Large Egos. Big Dreams. Small Budgets.
}} The Work and the Glory

== Plot ==

The film takes place just after the fictional disappearance of Richard Dutcher, famous for beginning the current Mormon movie phase with his work Gods Army (motion picture)|Gods Army.  After Dutchers disappearance the film follows the journeys of three Mormon filmmakers who are eager to take his place.  However, one of these filmmakers doesnt really want to see Dutcher found.

== Production ==

Richard Dutcher donated the 16mm film stock to make the movie. This is the same stock (three years-old) that Gods Army (motion picture)|Gods Army was going to be shot on, had Dutcher not found the financing to shoot on 35mm.

== External links ==
*http://www.theworkandthestory.com
*http://www.ldsfilm.com/WaS/WorkAndStory.html
*  

 
 
 
 