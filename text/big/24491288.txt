The Way of a Girl
{{Infobox film
|name= The Way of a Girl
|image=
|image_size=
|caption=
|director= Robert G. Vignola
|producer= Louis B. Mayer Irving Thalberg
|writer= Albert S. Le Vino (screenplay) Katharine Newlin Burt (story) Matt Moore William Russell
|cinematography= John Arnold
|editing=
|distributor= Metro-Goldwyn-Mayer
|released=  
|runtime= 60 minutes
|country= United States Silent with English intertitles
|budget=
}} silent era drama film|motion Matt Moore, William Russell.
 Directed by Robert G. Vignola, the screenplay was written by Albert S. Le Vino based on a story by Katharine Newlin Burt.

The female lead, Eleanor Boardman, stars in one of the 11 movies she did for MGM in the first two years of the life of the studio. 

==Syopsis==
Rosamond (played by Boardman) goes down the highway and city streets in a mile-a-minute auto. She survives arrests and smash-ups only to be captured by escaped convicts.

==Cast==
*Eleanor Boardman as Rosamond Matt Moore as George William Russell as Brand
*Matthew Betz as Matt
*Charles K. French as the Police Judge

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 