The Journey (2014 Malaysian film)
 

{{Infobox film
| name           = The Journey
| image          = The_Journey_Theatrical_Movie_Poster(2014_Malaysia_Movie).jpg
| caption        = Theatrical release poster
| alt            =
| director       = Chiu Keng Guan
| producer       = Choo Chi Han
| screenplay     = 
| story          = Ryon Lee
| starring       = {{Plainlist|
*Ben Andrew Pfeifer
*Frankie Lee Sai Peng
*Joanne Yew Hong Im  }}
| music          = 
| cinematography = Eric Yeong
| editing        = 
| studio         = Wohoo Studios
| distributor    = Astro Shaw
| released       =  
| runtime        = 110 mins
| country        = Malaysia
| language       = Chinese / English
| budget         = RM3 million  
| gross          = RM17.16 million  
}}
 Malaysian Chinese film directed by Chiu Keng Guan.   The film is known for being the highest grossing Malaysian film as of July 2014. 

==Plot==
The Journey tells the story of Uncle Chuan (Frankie Lee), a conservative father with a rigid set of rules. When his daughter, Bee (Joanne Yew) returns home after spending most of her formative years in England with a fiance, Benji (Ben Pfeiffer) in-tow, Uncle Chuan refuses to give his blessing. With cultural differences and language barrier that could potentially damage the union between Benji and his loved one, he feels that something must be done quick.
Uncle Chuan reluctantly allows them to marry but on one condition — the wedding ceremony has to be in the traditional way. Despite their lack of understanding towards one another, Benji and Uncle Chuan embark on a nationwide journey to hand-deliver wedding invitations to the latter’s childhood friends. Throughout the journey, the two learn valuable lessons about accepting each other’s differences.

==Box office==
The film grossed RM2.2 million at launch, easily setting the record for best opening weekend by a Malaysian Chinese production. It went on to gross RM3.7 million after 6 days of release.  By the 11th day, it had grossed a total of RM6.6 million.  The film was a critical success and with good word of mouth, it officially became the highest grossing local Malaysian Chinese movie production on the 12 February 2014 with a gross of RM7.6 million, surpassing the previous record which was held by Ah Beng: Three Wishes (RM7.56 million).  As of Feb 16th, the film had grossed RM9.8 million, making it the first local Malaysian Chinese movie production set to break the RM10 million barrier. Local cinema operators added additional screens to meet the high demand which resulted in sold out screenings across the Klang Valley, Penang and Johor.  On Feb 20th, the film was only RM0.2 million behind Ombak Rindu, grossing a total of RM10.70 million, the first local Chinese production to hit the RM10 million milestone. Owing to the tremendous success of the film in Malaysia, the production company contemplated an earlier release of the film in Singapore.   On the fourth weekend ending Feb 23rd 2014, The Journey officially became the highest grossing local production ever in Malaysias box office, having amassed a gross of RM12.92 million, beating the previous record holder, KL Gangsters RM11.74 million.   As of March 17, the film had raked in a total of RM17.16 million. 

==Filming locations==
* Cameron Highlands, Pahang
* Chew Jetty, Penang
* Pinang Tunggal Railway Bridge, Penang (dismantled)
* Aur Island, Johor
* Kuala Lumpur
* Baling, Kedah
* Ipoh, Perak.
* Sabah.

==Home media==
The Journey was released by Multimedia Entertainment in DVD and VCD on March 20, 2014. Alternatively, audiences can view the film on Astro First, Channel 480 with an additional bonus feature of documentary, namely Chasing for Dream. The film was also released in Singapore theaters on the same day. 

==References==
 

 
 