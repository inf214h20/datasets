Gang Busters (serial)
{{Infobox Film
| name           = Gang Busters
| image          = 
| image_size     = 
| caption        =  Ray Taylor Jacques Jaccard (asst.)
| producer       = Ford Beebe Al Martin Victor McLeod George H. Plympton
| narrator       =  Robert Armstrong
| music          = 
| cinematography = John W. Boyle William A. Sickner Charles Maynard
| distributor    = Universal Pictures
| released       = 1942
| runtime        = 13 chapters (251 min)
| country        =   United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial movie serial based on the radio series Gang Busters. 

==Plot== Robert Armstrong), Richard Davies).

==Cast==
* Kent Taylor as Detective Lieutenant Bill Bannister
* Irene Hervey as Vicki Logan
* Ralph Morgan as Professor Mortis Robert Armstrong as Detective Tim Nolan Richard Davies as Happy Haskins, photographer
* Joseph Crehan as Police Chief Martin OBrien George Watts as Mayor Hansen
* Ralf Harolde as Halliger, one of Professor Mortis henchmen
* John Gallaudet as Wilkinson, one of Professor Mortis henchmen
* William Haade as Mike Taboni, new member of Mortiss gang

==Production== Ray Taylor, veteran director responsible for many hit serials, and Noel M. Smith, former silent-screen director who specialized in fast action (Smith directed many of Larry Semons action-filled comedies of the 1920s). 

This is mostly a crime film but contains a small science fiction element.  The villains, The League of Murdered Men, are all dead criminals killed and revived by Professor Mortis using his own mysterious poison. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 194
 | chapter = 8. The Detectives "Gangbusters!" 
 }} 

==Critical reception==
The film was very successful in its original release, and was re-released in 1949. Authors Jim Harmon and Donald F. Glut describe Gang Busters as a "well made and interesting serial." 

Cline writes that the serial is one of Universals best {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 89
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 }}  and that Professor Mortis is one of the best characters ever created for a serial. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 108
 | chapter = 7. Masters of Menace (The Villains)
 }} 

==Chapter titles==
# The League of Murdered Men
# The Death Plunge
# Murder Blockade
# Hangmans Noose
# Man Undercover
# Under Crumbling Walls
# The Water Trap
# Murder by Proxy
# Gang Bait
# Mob Vengeance
# Wanted at Headquarters
# The Long Chance
# Law and Order
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 232
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1942
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial 
| before=Don Winslow of the Navy (1942)
| years=Gang Busters (1942)
| after=Junior G-Men of the Air (1942)}}
 

 

 
 
 
 
 
 
 
 
 
 