Khwaabb
 
 
{{Infobox film
| name           = Khwaabb
| image          = Khwaabb poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Zaid Ali Khan
| producer       = Moraad Ali Khan
| writer         = Moraad Ali Khan
| starring       = Navdip Singh Simer Motiani
| music          = Sandeep Chowta Sajjad Ali Chandwani
| cinematography = Aharon Rothschild
| editing        = Sejal Painter
| studio         = Bullseye Productions
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} romance and drama film directed by Zaid Ali Khan and produced by Moraad Ali Khan under the banner of Bullseye Productions. It features actors Navdip Singh and Simar Motiani in the lead roles. Music of the film has been composed by Sandeep Chowta and Sajjad Ali Chandwani. The film is released on 9 May 2014.   

The story revolves around the lives of two athletes: their struggles, their dreams and their aspirations. 

The official trailer of Khwaabb was unveiled on 23 August 2013 at Delhi. Upon being released on the net on the following day, the trailer got 1,22,000 views in 10 days only.   

Khwaabb was initially supposed to be released on October 2012.  However, it was later postponed to 9 May 2014.

== Cast ==
* Navdip Singh as Sanjay Kumar
* Simer Motiani as Kiran Missra
* Bajrangbali Singh as Ram Prasad Lakshman
* Rishi Miglani as Sameer
* Apeksha Verma as Archie
* Nafisa Ali as Herself
*Jeetendr.Gupta as Rihipal
* Dhirendra Gupta as Nathalal
* Vaibhav Bhisht as Immamudim
* Abbas Ali Khan as Sanjays father 
* Manoj Bakshi as Baldev Singh

== Music ==
{{Infobox album 
| Name       = Khwaabb
| Type       = soundtrack
| Artist     =  
| Cover      = 
| Alt        = 
| Released   =     
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Times Music
| Producer   = 
| Chronology =  Sandeep Chowta
| Last album = Saheb, Biwi Aur Gangster Returns (2013)
| This album = Khwaabb (2014)
| Next album = 
| Misc       = {{Extra chronology 
 | Artist    = Sajjad Ali Chandwani
 | Type      = Soundtrack 
 | Last album = Challo Driver (2012)
 | This album = Khwaabb (2014)
 | Next album = 
 }}
}}

The Music Launch was done by Salman Khan on 28 March2014 where he stated his interest in Producing a Sports based Movie.     Music of Khwaabb has been composed by Sandeep Chowta and Sajjad Ali Chandwani.  The soundtrack album consists of three tracks.   

=== Track listing ===
{{Track listing
| headline        = Khwaabb
| extra_column    = Singer(s)
| total_length    = 19:15
| writing_credits = no
| lyrics_credits  = no
| music_credits   = yes
| title1          = Khwaabb
| music1          = Sandeep Chowta
| extra1          = Sonu Nigam
| length1         = 4:18
| title2          = TV Ko Dekh
| music2          = Sajjad Ali Chandwani
| extra2          = Kailash Kher
| length2         = 3:57
| title3          = Shamein
| music3          = Sajjad Ali Chandwani
| extra3          = Shreya Ghoshal, Rahat Nusrat Fateh Ali Khan 
| length3         = 6:11
| title4          = The Final Lap
| music4          = Amal Malik
| extra4          = Instrumental
| length4         = 3:18
| title5          = The Love Unspoken
| music5          = Amal Malik
| extra5          = Instrumental
| length5         = 1:31
| title6          = Kirans Discovery
| music6          = Amal Malik
| extra6          = Instrumental
| length6         = 2:39
}}

== References ==
 

== External links ==
 