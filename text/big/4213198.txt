Wildcats (film)
{{Infobox film
| name = Wildcats
| image = Wildcats moviePoster.jpg
| caption = Theatrical release poster Michael Ritchie
| writer = Ezra Sacks
| producer = Anthea Sylbert
| starring = {{Plainlist|
* Goldie Hawn
* James Keach
* Swoosie Kurtz
}}
| music = James Newton Howard
| cinematography = Donald E. Thorin
| editing = Richard A. Harris
| distributor = Warner Bros.
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| budget =
| gross = $26,285,544 (United States) 
}} sports comedy Michael Ritchie and is also the film debut of Wesley Snipes and Woody Harrelson.

==Plot==
Molly McGrath is the daughter of a famed football coach who is dying to head her own team. When her wish is finally granted, Molly leaves her job coaching girls track at an affluent high school (Prescott High School) to take over a football team at an inner-city high school (Central High School)--the kind of place where guard dogs are needed to patrol the campus. At first the new coach’s idealism and optimism are suffocated with racial and gender prejudice, but eventually her overriding spirit begins to whip her unruly team into shape. At the same time, she must also struggle to win a battle for the custody of her two young daughters. The real test for Molly comes when her Central High team faces Prescott in the city championship.

==Cast==
* Goldie Hawn as Molly McGrath
* Swoosie Kurtz as Verna McGrath
* Jan Hooks as Stephanie Needham
* LL Cool J as Rapper
* Woody Harrelson as Krushinski
* Wesley Snipes as Trumaine

==Filming==
The film used Lane Technical College Prep High School football stadium for some of their shots.

==Reception==
The film was met with a mostly negative response from critics.  

===Box office===
The film debuted at No.4. 

==See also==
* White savior narrative in film

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 