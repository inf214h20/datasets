Howard the Duck (film)
 
{{Infobox film
| name           = Howard the Duck
| image          = Howard the Duck (1986).jpg
| alt            = The words "More adventure than humanly possible" and a giant egg with a beak holding a cigar.
| caption        = Theatrical release poster
| director       = Willard Huyck
| producer       = {{Plainlist|
* Ian Bryce
* Gloria Katz
* Robert Latham Brown
* George Lucas}}
| writer         = {{Plainlist|
* Willard Huyck
* Gloria Katz}}
| based on       =  
| starring       = {{Plainlist|
* Lea Thompson
* Tim Robbins
* Jeffrey Jones
* David Paymer
* Paul Guilfoyle
* Chip Zien}}
| music          = {{Plainlist| John Barry
* Songs:
* Thomas Dolby}}
| cinematography = Richard H. Kline
| editing        = {{Plainlist|
* Michael Chandler
* Sidney Wolinsky}}
| studio         = {{Plainlist|
* Lucasfilm
* Marvel Studios}} Universal Pictures
| released       =  
| runtime        = 111 minutes  
| country        = United States
| language       = English
| budget         = $37 million 
| gross          = $38 million   
}} science fiction animated film Marvel comic Captain America serial of 1944.
 surrealist comic worst films ever made. Contemporary critics saw the decision to shoot the film in live action instead of as an animated film and the appearance of Howard as primary obstacles to its success, while more recent commentators tend to focus on the films writing. Despite the criticism, it has gained a cult following among fans of the comic book series.

==Plot== anthropomorphic ducks. As he is reading the latest issue of Playboy|Playduck magazine, his armchair begins to quake violently and propels him out of his apartment building and into outer space; Howard eventually lands on Earth, in Cleveland, Ohio. Upon arriving, Howard encounters a woman being attacked by thugs. He defeats them using a unique style of martial arts. After the thugs flee, the woman introduces herself as Beverly Switzler (Lea Thompson), and decides to take Howard to her apartment and let him spend the night. The following day, Beverly takes Howard to Phil Blumbertt (Tim Robbins), a scientist whom Beverly hopes can help Howard return to his world. After Phil is revealed to be only a janitor, Howard resigns himself to life on Earth and rejects Beverlys aid. He soon applies for a job as a janitor at a local romance spa. Because of unfair treatment by his boss, Howard resigns and rejoins Beverly, who plays in a band called Cherry Bomb. At the club at which Cherry Bomb is performing, Howard comes across their manager (Richard Edson), and confronts him when he insults the band. A fight breaks out, in which Howard is victorious.

Howard rejoins Beverly backstage after the bands performance and accompanies her back to her apartment, where Beverly persuades him to be the bands new manager. The two begin to flirt, but soon after that they are interrupted by Blumburtt and two of his colleagues, who reveal that a dimensional-jumping device they were inventing was aimed at Howards planet and transported him to Earth when it was activated. They theorize that Howard can be sent back to his world through a reversal of this same process. Upon their arrival at the laboratory, the device malfunctions when it is activated, arousing the possibility of something else being transported to Earth. At this point, Dr. Walter Jenning (Jeffrey Jones) is possessed by a life form from another alternate dimension. When they visit a diner, the creature introduces himself as a "Dark Overlord of the Universe" and demonstrates his developing mental powers by destroying table utensils and condiments. A fight ensues when a group of truckers in the diner begin to insult Howard. Howard is captured and is almost killed by the diner chef, but the Dark Overlord destroys the diner and escapes with Beverly.

Howard locates Phil, who is arrested for his involvement in the diner fight. After they escape, they discover an Ultralight aircraft, which they use to search for the Dark Overlord and Beverly. At the laboratory, the Dark Overlord ties Beverly down to a metal bed and plans to transfer another one of its kind into her body with the dimension machine. Howard and Phil arrive and apparently destroy the Dark Overlord with an experimental "neutron disintegrator" laser; however, it has only been forced out of Jennings body. The Dark Overlord reveals its true form at this point. Howard fires the neutron disintegrator at the hideous beast, obliterating it, and destroys the dimension machine, preventing more creatures from arriving on Earth, but also ruining Howards only chance of returning to his planet. Howard then becomes Beverlys manager and hires Phil as an employee on her tour.

==Cast==
 
* Lea Thompson as Beverly Switzler
* Tim Robbins as Phil Blumburtt
* Jeffrey Jones as Dr. Walter Jenning
* David Paymer as Larry
* Paul Guilfoyle as Lieutenant Welker
* Chip Zien (voice) as Howard the Duck
* Liz Sagal as Ronette
* Dominique Davalos as Cal Holly Robinson as K.C.
* Tommy Swerdlow as Ginger Moss
* Richard Edson as Ritchie
* Miles Chapin as Carter
* Paul Comi as Dr. Chapin
* Richard McGonagle as First cop
* Virginia Capers as Cora Mae
* Miguel Sandoval as Bar entertainment supervisor William Hall as Officer Hanson
* Thomas Dolby as Rock club bartender
* Richard Kiley (voice) as The Cosmos
* Debbie Lee Carrington (voice) as Additional ducks
* Brian Steele (voice) as Dark Overlord of the Universe

;Actors Portraying the Ducks
* Ed Gale
* Tim Rose 
* Steve Sleap
* Peter Baird
* Mary Wells
* Lisa Sturz
* Jordan Prentice
 

==Production==
  in order to focus on producing films, including Howard the Duck.]]
George Lucas attended film school with Willard Huyck and Gloria Katz, who later co-wrote American Graffiti with Lucas. After the films production concluded, Lucas told Huyck and Katz about the comic book Howard the Duck, primarily written by Steve Gerber, describing the series as being "very funny" and praising its elements of film noir and absurdism.    In 1984, Lucas relinquished his presidency of Lucasfilm to focus on producing films.  According to the documentary A Look Back at Howard the Duck, Huyck, Katz and Lucas began to seriously consider adapting Howard the Duck as a film, and met with Gerber to discuss the project.  Steve Gerbers account differs slightly; he recalls that at the time he was approached to discuss the film, Lucas was not yet involved with the project.   

The film was optioned by Universal Studios. According to Marvin Antonowsky, "Sidney   lobbied very hard for Howard the Duck", because the studio had passed on previous projects that Lucas was involved in, which had been very successful.  Sheinberg denied any involvement in Howard the Duck, claiming that he never read the screenplay.    Huyck and Katz strongly felt that the film should be animated. Because Universal needed a film for a summer release, Lucas suggested that the film could be produced in live action, with special effects created by Industrial Light & Magic. 
 dwarf actors, he was able to hire a number of extras to work on these sequences. 

The Ultralight sequence was difficult to shoot, requiring intense coordination and actors Tim Robbins and Ed Gale to actually fly the plane.  The location scout was stumped for a location for the Ultralight sequence; after she described what she was looking for, a telephone repairman working in her office in San Francisco suggested Petaluma for the scene. Because of the limited shooting time, a third unit was hired to speed up the filming process.  The climax was shot in a naval installation in San Francisco, where conditions were cold throughout the shoot.  The film cost an estimated $36 million to produce.   
 The Spectre so that he could watch the final day of shooting. 

===Development===
Huyck and Katz began to develop ideas for the film. Early on in the production, it was decided that the personality of the character would be changed from that of the comics, in which Howard was rude and obnoxious, in order to make the character nicer.    Gerber read over the script and offered his comments and suggestions. In addition, Hyuck and Katz met with Gerber to discuss a horror sequence that they were having difficulty with. 

During the screenwriting process, a stronger emphasis was placed on special effects, rather than satire and story.  Overall, the tone of the film is in diametric opposition to the comics. Whereas Katz declared that "Its a film about a duck from outer space... Its not supposed to be an existential experience... Were supposed to have fun with this concept, but for some reason reviewers werent able to get over that problem."   Gerber declared that the comic book series was an existental joke, stating "This is no joke! There it is. The cosmic giggle. The funniest gag in the universe. That lifes most serious moments and most incredibly dumb moments are often distinguishable only by a momentary point of view. Anyone who doesnt believe this probably cannot enjoy reading Howard the Duck."  However, after shooting was finished, Gerber stated that he felt the film was faithful to both the spirit of the comic book and the characters of Howard and Beverly. 

An early proposed storyline involved the character being transported to Hawaii. Huyck states that this storyline was considered because "we thought it would be sort of fun to shoot there". According to Katz, they did not want to explain how Howard arrived on Earth initially, but later rewrote the screenplay so that the film would begin on Howards home world.  Huyck and Katz wanted to incorporate both lighter, humorous elements and darker, suspenseful elements. Katz states that some readers were confused by the sexual elements of the screenplay, as they were unsure as to whether the film was intended for adults or children. Huyck and Katz wrote the ending leaving the story open for a sequel, which was never produced. 

===Adaptation=== live actors and to use special effects for Howard.

The script significantly altered the personality of the title character, played the story straight instead of as a satire, removed the surrealist elements, and added supernatural elements that could highlight special effects work done by Lucas Industrial Light & Magic.
 Marvel Super Special #41  and in a three-issue limited series. 

===Special effects===
Lucasfilm built animatronic suits, costumes and puppets for the film. Because of the limited preparation time, varied "ducks" created for the film would explode or lose feathers, and multiple ducks were built with the wrong proportions. On the first day of shooting, the crew realized the poor quality of the effects when they found that the inside of the puppets neck was visible when its mouth opened. Huyck repeatedly reshot scenes involving Howard as the animatronics were improved. Because multiple puppeteers were in charge of controlling different parts of the animatronic body, Huyck was unable to coordinate the shoot properly. In the opening sequence, Howards chair is propelled out of his apartment by wires, which were later digitally erased by computer, an effect that was uncommon in 1986. The effect of the feathers on Howards head becoming erect during the love sequence took months to prepare. 

The voice of Howard, Chip Zien, was not cast until after shooting completed. Because Ed Gales voice was difficult to hear when he wore his suit, Huyck ordered Gale to perform his scenes without speaking any of the required dialogue, which was later synchronized during the editing process.   Lead puppeteer Tim Rose was given a microphone attached to a small speaker, which would allow Rose to speak the dialogue in order to help the actors respond to Howards dialogue.  While wearing his suit, Gale could only see through Howards mouth, and had to sense his location without proper eyesight. Gale often had to walk backwards before beginning rehearsals.  In between takes, a hair dryer was stuffed in Howards bill in order to keep Gale cool.  Gale taped two of his fingers together in order to wear the three-fingered hands created for the Howard costume.    A total of six actors gave physical performances as Howard.   

Gerber was impressed by the appearance of Howard, and commented, "It was very bizarre to meet it and ... realize not just that I created it - that would have been bizarre enough... you know, it was sort of like meeting a child I didnt know I had ..." 

Makeup artists Tom Burman and Bari Dreiband-Burman and actor Jeffrey Jones discussed the appearance of the Dark Overlord character with Huyck and Katz, and developed the characters progressing looks. When Katzs daughter visited the set during the shoot, she was terrified by Jones appearance in makeup. The diner sequence combines practical effects, including squibs and air cannons, with visual effects created by ILM.  Sound designer Ben Burtt created the voice of the Dark Overlord by altering Jeffrey Jones voice as his character transformed.  Stop motion effects during the climax were designed by Phil Tippett, who began with a clay model before upgrading to more sophisticated pieces. 

===Casting=== Madonna and Cyndi Lauper." During the shoot, Thompson complained that the filmmakers chose to shoot Howards closeup before hers. Thompson also states that she regrets not wearing a wig, as her hairstyle took two hours a day to prepare.  Jeffrey Jones was cast because of his performance in Amadeus (film)|Amadeus. Although Tim Robbins had not appeared in many films, Huyck and Katz were confident that he was right for the part. 

In order to play the physical role of Howard, Huyck and Katz held casting calls with dwarf actors, eventually casting a child actor and hiring Ed Gale, who had been rejected because he was too tall for the role, to perform stunts and portray the role during evening shoots.  The child actor found the shooting conditions to be too difficult to handle,  and the films editors were unable to match day and evening sequences because of the difference in the two portrayals.  Because Gale also served as an understudy, he took over the role.  

After the film was completed, Huyck and Katz auditioned John Cusack and Martin Short for the voice of Howard, eventually casting Chip Zien, because they felt his gravelly voice worked well for the part.    Because Howards voice was not cast until the film had begun editing, synchronization was extremely difficult. 

===Music=== John Barry. George Clinton.  Gale was choreographed to dance and play guitar as Howard. Dolby built a special guitar for Gale to rehearse and film with. 

==Reception==
===Critical response===
  for "Worst New Star".  The appearance of Howard was generally seen as being unconvincing.      ]]
 Worst Supporting Worst Director Worst Original Worst Screenplay, Worst New Worst Picture, tied with Under the Cherry Moon.  The movie also won a Stinkers Bad Movie Awards for Worst Picture. 

===Box office===
The film was considered a Box office bomb, grossing $16,295,774 in the United States and $21,667,000 worldwide for a total of $37,962,774, just under $1 million above the production budget.  When the film was screened for Universal, Katz said that the studios executives left without commenting on the film.  Screenings for test audiences were met with mixed response.  Rumors suggested that Universal production heads Frank Price and Sidney Sheinberg engaged in a fistfight after arguing over who was to blame for green-lighting the film. Both executives denied the rumors.   News reports speculated that one or both would be fired by MCA chairman Lew Wasserman.  Price soon left the studio, and was succeeded by Tom Pollack. The September 17, 1986 issue of Variety (magazine)|Variety attributed Prices departure to the failure of the film, even though he had not approved the films production.  Following the films failure, Huyck and Katz left for Hawaii and refused to read reviews of the film. 

In 2014, the LA Times listed the film as one of the costliest box office flops of all time. 

==Legacy== Academy Award Mystic River. Ed Wood, The Devils Advocate. Zien found fame on Broadway, starring in the original cast of Stephen Sondheims Into the Woods.
 Chucky performances, the antagonist in the Childs Play (film series)|Childs Play horror film series.  After the films release, Huyck and Katz chose to work on more dramatic projects in order to separate themselves from Howard the Duck.  Katz said Lucas continued to support the film after its failure, because he felt it would later be seen in a better light than it had been at the time of its release.  Huyck said he later encountered fans and supporters of the film who felt that it had been unfairly treated by critics. 

In June 2012, the   re-release to celebrate its 25th anniversary. He eventually gets Joe Quesada to try and appeal to, and bribe, George Lucas into supporting the re-release. http://www.youtube.com/watch?v=71h3Mo3Lbro 

==See also==
 
* List of films considered the worst
* Golden Raspberry Award for Worst Original Song

==References==
 

==External links==
 
*   at  
*  
*  
*  

   
{{Succession box Razzie Award for Worst Picture
(tied with Under the Cherry Moon)
|years=7th Golden Raspberry Awards
|before= 
|after=Leonard Part 6
}}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 