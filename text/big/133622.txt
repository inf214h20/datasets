Howards End (film)
 
 
 
{{Infobox film
| name           = Howards End
| image          = Howards end poster.jpg
| caption        = Theatrical release poster James Ivory
| screenplay     = Ruth Prawer Jhabvala
| based on       =  
| starring       = Anthony Hopkins  Vanessa Redgrave  Helena Bonham Carter  Emma Thompson
| producer       = Ismail Merchant Richard Robbins Percy Grainger (opening and end title)
| cinematography = Tony Pierce-Roberts
| editing        = Andrew Marcus
| studio         = Merchant Ivory Productions
| distributor    = Mayfair (UK) Sony Pictures Classics
| released       =  
| runtime        = 140 minutes
| country        = United Kingdom
| language       = English
| budget         = $8 million
| gross          = $25,966,555
}} romantic drama novel of A Room James Ivory and produced by Ismail Merchant.
 Best Picture Best Director James Ivory. Best Art Direction (Luciana Arrighi and Ian Whittaker). Ruth Prawer Jhabvala earned her second Academy Award for Best Adapted Screenplay, while Emma Thompson won the 1992 Academy Award for Best Actress.

==Plot==
The story takes place in  ; and the Basts, a young couple down on their luck, who may be traced to the lower middle class.  (Forster is clear that the novel is "not concerned with the very poor".) The film asks the question "Who will inherit England?" and answers it through the ownership of the house, Howards End, as it passes from person to person.

At the start of the film, the younger sister, Helen Schlegel (Helena Bonham Carter), rashly becomes engaged to the younger Wilcox son, Paul.  The next day they realise their mistake and break it off by mutual consent, but Helen had already written to her older sister, who tells the family. Aunt Juley (Prunella Scales) subsequently arrives at Howards End and acquaints the Wilcoxes with the fact of the engagement, unaware that it had in the meantime been broken off.  Later, when the Wilcox family takes a house in the vicinity of the Schlegels in London, the older sister, Margaret Schlegel (Emma Thompson), feels compelled to visit because of the social embarrassment of the previous year.  

She resumes the friendship of Pauls mother, Ruth Wilcox (Vanessa Redgrave), whom she had briefly met before, while the two of them were travelling abroad with their families. Ruth is descended from English yeoman stock and it is through her family that the Wilcoxes have come to own Howards End, a house she loves dearly. It stands symbolically above class distinction (in both the film and the novel), representing rural England, the rich tapestry of its manifold traditions, and the deeply rooted cultural heritage associated with them.  

Over the course of the next few months, the two women become very good friends, and Ruth eventually regards Margaret as a kindred spirit.  Hearing that the lease on the Schlegels London house is due to expire, and knowing she is soon to die, Ruth bequeaths Howards End to Margaret in a handwritten will.  This causes great consternation to the Wilcoxes, who refuse to believe that Ruth was in her "right mind" or could possibly have intended her home to go to a relative stranger.  The Wilcoxes burn the piece of paper on which Ruths bequest is written, and decide to keep her will a secret.  Henry Wilcox (Anthony Hopkins) is aware, as Margaret is not, that he has prevented the Schlegels from finding a home at Howards End. Therefore he offers to help Margaret look for a new place to live.  As a result, they get to know each other quite well and are very much impressed. Henry proposes marriage. Margaret accepts.

Some time before this the Schlegels had befriended a young, poor, yet highly intellectual clerk, Leonard Bast (Samuel West).  Both sisters find him remarkable, infused with a spirit of "romantic ambition" as Margaret puts it. Wishing to improve his lot, they pass along advice from Henry to the effect that Leonard must leave his post, because the insurance company he works for is supposedly heading for a crash.  Leonard acts on this advice in good faith, but finds himself in a far worse position; indeed he is unable to find any employment.

The two plot lines converge unexpectedly at the scene of the beautiful wedding party of Evie Wilcox (Henrys daughter with Ruth).  Helen has found the Basts destitute, on the verge of starvation, and brings them from London to Shropshire and storms into the garden party.  Jacky Bast (Nicola Duffett) overeats and gets drunk; Margaret approaches her with Henry trying to resolve the situation.  Jacky recognises Henry immediately and it transpires that many years previously, he had had an illicit affair with her.  Humiliated and suspicious, Henry breaks off the engagement. Nevertheless he and Margaret make their peace with each other the same evening, and she forgives his moral transgression, valiantly determining: "this is not going to trouble us".  Margaret then writes to Helen, insisting, in accordance with Henrys wishes, that she take the Basts away, so as to avoid further anguish and embarrassment.

Helen in turn feels betrayed and consequently, the Schlegel sisters drift apart.  Hurt and upset, Helen has a brief affair with Leonard Bast, for whom she has a profound appreciation.  She falls pregnant and decides to leave the country, telling no one of her condition.  Before going away however she offers Leonard Bast (who knows nothing of her being with child) some very substantial financial assistance, which he refuses absolutely, returning her cheque. Several months following these events Aunt Juleys illness prompts Helen to travel back to England. She must reclaim her possessions, and asks if she may stay one night at Howards End (where they are kept) for sentimental reasons, as she has at present no other home.  This of course cannot be done without Henrys permission; but as soon as he learns from Margaret that Helen, still unmarried, is pregnant, he indignantly rules that she cannot stay at his house, and that the man responsible for her condition must be found out and punished for dishonouring her.

Margaret is dismayed by the ruthlessness of Henrys conduct and perceives his attitude as insensitive and unjust. She remonstrates with him bitterly about the different standards of sexual propriety applied to men and women, and declares her intention to leave him.  At this juncture Leonard and the elder Wilcox son, Charles (James Wilby), make their separate ways to Howards End, where the final tragedy unfolds: Charles attacks Leonard with a sword, inadvertently killing him. This is discovered by the police and Charles is arrested.  Henrys pride is shaken; his feelings of heartbreak and remorse surface at last, and he and Margaret are reunited, becoming truly close, even closer than they were before the crisis which has plunged a sword (literally and figuratively) through the very heart of their family, had occurred.

Ultimately, Ruth Wilcoxs wish is fulfilled: Henry leaves Howards End to Margaret in his last will and testament. Helen is happily reconciled with Margaret, who regards Helens son as the rightful heir.  Henry is aware of his wifes intent to leave the house to her nephew upon her own death and fully approves. In both the film and the novel, the final ownership of Howards End is emblematic of new class relations in Britain. It is the wealth of the new industrialists (the Wilcoxes), married to the politically reforming vision of liberalism (the Schlegels,) which will make amends and reward the children of the underprivileged (the Basts); whereupon Howards End is revealed as an instrument of poetic justice and redemption.

==Cast==
* Vanessa Redgrave ... Ruth Wilcox
* Helena Bonham Carter ... Helen Schlegel Joseph Bennett ... Paul Wilcox
* Emma Thompson ... Margaret Schlegel
* Prunella Scales ... Aunt Juley
* Adrian Ross Magenty ... Tibby Schlegel
* Jo Kendall ... Annie
* Anthony Hopkins ... Henry Wilcox
* James Wilby ... Charles Wilcox
* Jemma Redgrave ... Evie Wilcox
* Ian Latimer ... Stationmaster
* Samuel West ... Leonard Bast
* Simon Callow ... Music and Meaning Lecturer (cameo)
* Mary Nash ... Pianist
* Siegbert Prawer ... Man Asking a Question
* Susie Lindeman ... Dolly Wilcox
* Nicola Duffett ... Jacky Bast Mark Tandy, Andrew St. Clair, Anne Lambton, Emma Godfrey, Duncan Brown, Iain Kelly ... Luncheon Guests
* Atalanta White... Maid at Howards End
* Gerald Paris ... Porphyrion Supervisor
* Allie Byrne, Sally Geoghegan, Paula Stockbridge, Bridget Duvall, Lucy Freeman, Harriet Stewart, Tina Leslie ... Blue-stockings
* Mark Payton ... Percy Cahill
* David Delaney ... Simpsons Carver
* Mary McWilliams ... Wilcox Baby
* Barbara Hicks ... Miss Avery
* Rodney Rymell ... Chauffeur
* Luke Parry ... Tom, the Farmers Boy
* Antony Gilding ... Bank Supervisor
* Peter Cellier ... Colonel Fussell Crispin Bonham Carter ... Albert Fussell
* Patricia Lawrence, Margery Mason ... Wedding Guests
* Jim Bowden ... Martlett
* Alan James ... Porphyrion Chief Clerk
* Jocelyn Cobb ... Telegraph Operator
* Peter Darling ... Doctor
* Terence Sach ... Deliveryman
* Brian Lipson ... Police Inspector
* Barr Heckstall-Smith ... Helens Child

==Production==

===Casting=== Three Sisters in 1990, a production in which the third sister was played by Vanessas sister Lynn Redgrave. Samuel West is the son of Prunella Scales.

===Music===
* "Bridal Lullaby" by Percy Grainger  Courtesy of Bardie Edition
* "Mock Morris" by Percy Grainger  Courtesy of Schott & Co., Ltd.
* "La Dance" (1906) by André Derain  (c) 1992 Artist Rights Socitey, New York/ADAGP Courtesy of the Fridart Foundation. 5th Symphony by Ludwig van Beethoven (uncredited)
 Richard Robbins, Martin Jones.

===Filming locations===
Part of the film  was shot at the  , Oxfordshire, and the Wilcoxess house is nearby.  Some scenes were also shot at Brampton Bryan in Herefordshire. 

==Reception==
The film received massive critical acclaim. On 5 June 2005, Roger Ebert included it on his list of "Great Movies". 

As of September 2013, the film-critics aggregate site Rotten Tomatoes listed 92% positive reviews based on 36 reviews (33 "fresh", 3 "rotten"). 

According to the website Box Office Mojo the Total Gross of the film stands at $25,966,555. 

==Awards and nominations==
65th Academy Awards (1992) Best Actress – Emma Thompson Best Adapted Screenplay – Ruth Prawer Jhabvala Best Art Direction – Luciana Arrighi and Ian Whittaker Best Picture – Ismail Merchant Directing – James Ivory Best Supporting Actress – Vanessa Redgrave Best Cinematography – Tony Pierce-Roberts Costume Design John Bright Original Music Richard Robbins
 45th British British Academy Film (BAFTA) Awards (1992) Best Film – Ismail Merchant Best Actress in a Leading Role – Emma Thompson Best Direction James Ivory Best Adapted Screenplay – Ruth Prawer Jhabvala Best Actress in a Supporting Role – Helena Bonham Carter Best Actor in a Supporting Role – Samuel West Best Cinematography – Tony Pierce-Roberts Best Production Design – Luciana Arrighi Best Costume John Bright Best Editing – Andrew Marcus Best Makeup and Hair – Christine Beveridge
 50th Golden Globe Awards (1992) Best Actress in a Motion Picture – Drama – Emma Thompson Best Director James Ivory Best Motion Picture – Drama – Ismail Merchant Best Screenplay – Ruth Prawer Jhabvala

1992 Directors Guild of America Awards James Ivory

1992 Writers Guild of America Awards
* Nominated: Best Screenplay Based on Material Previously Produced or Published — Ruth Prawer Jhabvala

1992 New York Film Critics Circle Awards
* Won: Best Actress – Emma Thompson
* Nominated: Best Film — Ismail Merchant James Ivory
 Los Angeles Film Critics Association Awards
* Won: Best Actress – Emma Thompson
 National Society of Film Critics Awards
*Won: Best Actress – Emma Thompson
* Nominated: Best Supporting Actress — Vanessa Redgrave
 1992 National Board of Review Awards
* Won: Best Film – Ismail Merchant James Ivory
* Won: Best Actress – Emma Thompson
 Cannes Film Festival James Ivory   
* Nominated: Palme dor

==References==
 

==External links==
*  
*  
*  

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 