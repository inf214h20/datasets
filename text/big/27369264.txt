The Legend Is Born: Ip Man
 
 
{{Infobox film
| name           = The Legend Is Born – Ip Man
| image          = The Legend is Born – Ip Man poster.jpg
| caption        = Original Hong Kong film poster
| film name = {{Film name| traditional    = 葉問前傳
| simplified     = 叶问前传
| pinyin         = Yè Wèn Qiánzhuàn
| jyutping       = Jip6 Man6 Cin4-zyun6}}
| producer       = Sin Kwok-lam
| director       = Herman Yau
| writer         = Erica Lee
| starring       =  
| music          = Chun Hung Mak
| editing        = 
| cinematography = Kwong-Hung Chan
| studio         = Mei Ah Entertainment
| distributor    = Cathay-Keris Films Universe Laser & Video Co. Ltd.
| released       =  
| runtime        = 
| country        = Hong Kong
| language       =  
| budget         = 
| gross          = 
}}
 Hong Kong grandmaster Yip Ip Man, Ip Man or Ip Man 2, The Legend is Born features several actors who appeared in Yips films, including Sammo Hung, Louis Fan, and Chen Zhihui. The film also features a special appearance by Ip Chun, the son of Ip Man.

==Plot==
As a child, Ip Man learns Wing Chun from Chan Wah-shun together with Ip Tin-chi (Ip Mans adopted brother) and Lee Mei-wai. After Chans death from an illness, Ip Man continues to learn Wing Chun from his senior, Ng Chung-sok, before eventually leaving Foshan to study in Hong Kong.

In Hong Kong, after a field hockey match, Ip Man and his schoolmates are racially insulted by a Westerner. Ip Man takes offense to the language. The Westerner then slurs them in Chinese. Ip Man challenges the Westerner to a fight and defeats him. Ip Mans notoriety and popularity soar after the incident. While getting medicine to treat the Westerner whom he now refers to as a friend, he meets master Leung Bik, who is actually the son of Leung Jan, Chan Wah-shuns teacher. Ip Man learns a different, improved style of Wing Chun from Leung and his prowess in martial arts improves tremendously. Meanwhile in Foshan, Ip Tin-chi rose to become a prominent businessman under another Wing Chun martial arts association.

Ip Man returns to Foshan years later and reunites with his peers. Ng Chung-sok sees that Ip Man has mastered a new style of Wing Chun from Leung Bik, which differs from Chan Wah-shuns orthodox style. Ip Man also later falls in love with Cheung Wing-shing, the daughter of the vice-mayor of Foshan. When Lee Mei-wai realises that Ip Man loves Cheung, she accepts Ip Tin-chis love for her. On their wedding night, her godfather is murdered and Ip Man is arrested as a prime suspect after being witnessed struggling with him. Cheung later lied that Ip Man was with her the entire evening to have him released from jail. Lee discovers a letter to Ip Tin-chi, revealing his involvement in the murder of her godfather. She was saved by Ip Tin-chi when she attempted suicide and both of them leave for Foshan but are stopped by the Japanese. Lee is captured and Ip Tin-chi is forced to kill Ng Chung-sok because Lee showed the letter to him.

At the martial arts association, Ng Chung-sok was defeated by Ip Tin-chi and the Japanese. Ip Man arrives in time to save Ng Chung-sok from being killed and then subsequently defeats the Japanese and Ip Tin-chi. Ip Tin-chi then reveals that he is actually a Japanese named Tanaka Eiketsu, and was sent to China to infiltrate and work as an undercover agent. He then performs seppuku to end his life. Ip Man rushes off to the pier to rescue Lee. He defeats the Japanese and rescues Lee. At the pier, they discover that the Japanese have been smuggling Japanese children to China, possibly as future undercover agents, similar to Ip Tin-chi.

Ng Chung-sok is seen narrating the story of the night to new apprentices at the martial arts association. Ip Mans son, Ip Chun, is seen among the new apprentices. Ip Man arrives back at the association and is shown married to Cheung.

==Cast== Ip Man oath brother Huang Yi as Cheung Wing-shing, Ip Mans love interest
* Rose Chan Ka-Wun as Lee Mei-wai, Ip Mans junior. 
* Xu Jiao as the younger Lee.
* Ip Chun as Leung Bik, Ip Mans Wing Chun terms#Family Lineage Titles or Terms|sisuk
* Sammo Hung as Chan Wah-shun, Ip Mans sifu / Austin Yuan
* Hins Cheung as Cho, Ip Mans love rival
* Yuen Biao as Ng Chung-sok, Ip Mans senior / Vincent Yuan
* Lam Suet as Cheung Ho-tin, the vice-mayor of Foshan and father of Cheung Wing-shing
* Sire Ma as Cheung Wing-wah, Cheung Wing-shings younger sister
* Bernice Liu as Kitano Yumi
* Chen Zhihui as Ip Mans father
* Kenny Kwan
* Andy Taylor as foreign challenger at the field hockey match

==Release==
The film was released in Hong Kong on 24 June 2010. 

==Home media==

===VCD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| 26 August 2010
|| Hong Kong
|| N/A
|| Universal Studio
|| NTSC
|| Cantonese, Mandarin 
|| English, Traditional Chinese
|| 2VCDs
|| 
|}

===DVD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| 26 August 2010
|| Hong Kong
|| N/A
|| Universal Studio
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| 6.1, Dolby Digital EX(TM) / THX Surround EX(TM), DTS Extended Surround(TM) / DTS-ES(TM)
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|- 
|- style="text-align:center;"
|| 13 October 2010
|| Taiwan
|| N/A
|| 
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Dolby Digital 2.0, Dolby Digital 5.1, Dolby Digital EX(TM) / THX Surround EX(TM)
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|-
|- style="text-align:center;"
|| 20 December 2010
|| United Kingdom
|| N/A
|| Metrodome Group (UK)
|| PAL
|| 2
|| Cantonese, Mandarin
|| 
|| English
|| 
|| 
|-
|- style="text-align:center;"
|| 13 December 2011
|| United States
|| N/A
|| Funimation
|| NTSC
|| 1
|| Cantonese, English
|| 
|| English
|| 
|| 
|}

===Blu-ray Disc===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| 26 August 2010
|| Hong Kong
|| N/A
|| Universal Studio
|| 
|| All
|| Cantonese, Mandarin
|| DTS-HD Master Audio 7.1,Dolby Digital 5.1 EX, Dolby Digital 5.1 EX
|| Traditional Chinese, English 
|| 
|| 
|-
|- style="text-align:center;"
|| 13 October 2010
|| Taiwan
|| N/A
|| TBA
|| 
|| All
|| Cantonese, Mandarin
|| 7.1, Dolby Digital 2.0, DTS-HD Master Audio, Dolby Digital 5.1, Dolby Digital EX(TM) / THX Surround EX(TM)
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|-
|- style="text-align:center;"
|| 28 January 2011
|| Germany
|| N/A
|| Splendid Entertainment
|| 
|| B
|| Cantonese, German (Dubbed)
|| DTS-HD HR 5.1
|| German, Dutch  
|| Released under the title "IP Man Zero"
|| 
|-
|- style="text-align:center;"
|| 13 December 2011
|| United States
|| N/A
|| Funimation
|| NTSC (DVD)
|| A
|| Cantonese, English (Dubbed)
|| TBA
|| English 
|| Blu-ray + DVD combo
|| 
|-
|- style="text-align:center;"
|| 13 December 2011
|| Canada
|| N/A
|| Vivendi Visual Entertainment 
|| NTSC (DVD)
|| A
|| Cantonese, English (Dubbed)
|| TBA
|| English 
|| Blu-ray + DVD combo
|| 
|}

==See also==
* Ip Man (film)|Ip Man (film)
* Ip Man 2
* The Grandmaster (film)|The Grandmaster (film)
*  
* Ip Man (TV series)|Ip Man (TV series)

==References==
 

==External links==
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 