Just Rambling Along
 
{{Infobox film
| name           = Just Rambling Along
| image          = 
| caption        = 
| director       = Hal Roach
| producer       = Hal Roach
| writer         = 
| starring       = Stan Laurel
| music          = 
| cinematography = Robert Doran
| editing        = Thomas J. Crizer
| distributor    = 
| released       =  
| runtime        = 9 minutes
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 short silent silent comedy film featuring Stan Laurel.    The film is Laurels earliest surviving work and the first project he did with film producer Hal Roach, who later put out a large portion of the Laurel and Hardy films.

==Plot==
The story is of a poor young sap who cant seem to get a break. Hes thrown out of a diner, and then finds a wallet, which is immediately snatched away from him by a little boy.  Stan tries to fight the boy for it, but the boys father, a police officer, stops him. Stan gives up and walks away.

Next the Pretty Young Lady (Clarine Seymour) woos him and a park bench full of men back into the diner.  When the hostess sees Stan, she kicks him out on his rear end.  Out on the street, he finds the little boy playing with the wallet.  He quickly snatches it away and goes back into the diner.  When the hostess tries to throw him out a third time, he shows her that he has money to pay for a meal.  Before going to the serving line, he pauses to flirt with the pretty young lady, who promptly throws a drink in his face.

He then goes up to the serving line where the chef gives him a taste of everything he has to offer that day.  Stan stuffs his face, but shakes his head and tells the chef that he doesnt want any of it.  He only wants a cup of coffee.  When the chef has his back turned, Stan stuffs his pockets and boater full of food.

He goes back to the pretty young ladys table, sits down, and tries to flirt with her once again.  While hes eating, she switches their tickets, and gets up to leave.  He follows her to the cashier and realizes hes left with her bill, which he cannot pay.  He tries to sneak out of the diner, but hes caught and thrown out on his rear for the third and final time.  The film ends with the cop roughing him up as the young boy looks on.

==Cast==
* Stan Laurel as Nervy Young Man
* Clarine Seymour as Pretty Lady
* Noah Young as Policeman
* James Parrott as Cook
* Bud Jamison as Chef
* Bunny Bixby
* Mary Burns Harry Clifton
* Helen Fletcher
* Max Hamburger
* Wallace Howe
* Bert Jefferson
* Alma Maxim
* Belle Mitchell
* Herb Morris
* Marie Mosquini
* William Petterson
* Hazel Powell
* Lillian Rothchild
* Adu Sanders
* Emmy Wallace
* Dorothea Wolbert

==See also==
* List of American films of 1918
* Stan Laurel filmography

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 