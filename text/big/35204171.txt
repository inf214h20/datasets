Kidnapped (1948 film)
 
{{Infobox film
| name           = Kidnapped
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = William Beaudine
| producer       = Lindsley Parsons
| writer         = Robert Louis Stevenson
| based on       = 
| screenplay     = Scott Darling
| narrator       = 
| starring       = Roddy McDowall Sue England Dan OHerlihy
| music          = Edward J. Kay Dave Torbett
| cinematography = William A. Sickner
| editing        = Ace Herman
| studio         = Lindsley Parsons Picture Corporation
| distributor    = Monogram Pictures
| released       =  
| runtime        = 81 min.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama directed by William Beaudine, starring Roddy McDowall, Sue England and Dan OHerlihy. The former child star Roddy McDowall plays David Balfour in the story about a young man cheated out of his birthright by his wicked, covetous uncle Ebenezer (Houseley Stevenson).

==Plot summary==
 

==Cast==
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Roddy McDowall || David Balfour   
|-
| Sue England || Aileen Fairlie 
|-
| Dan OHerlihy || Alan Breck 
|-
| Roland Winters || Capt. Hoseason 
|-
| Jeff Corey || Shuan  
|-
| Houseley Stevenson || Ebenezer  
|-
| Erskine Sanford || Rankeillor 
|-
| Alex Frazer || Hugh Fairlie  
|-
| Winifriede McDowall || Innkeepers Wife  
|-	
| Robert J. Anderson || Ransome 
|-
| Janet Murdoch || Janet Clouston 
|-
| Olaf Hytten || The Red Fox 
|-
| Erville Alderson ||  Mungo 
|- Mary Gordon || Scottish Woman 
|-
| Hugh OBrian || Sailor
|-
| Eric Wilton || Rankeillors Secretary
|-
| Jimmie Dodd || Scotsman Sailor
|}

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 