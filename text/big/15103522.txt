Nine Days in One Year
{{Infobox film name = Nine Days in One Year image = Nine Days in One Year.jpg image_size =  caption =  director = Mikhail Romm producer = Cinematography Ministry of the USSR writer = Daniil Khrabrovitsky Mikhail Romm narrator = Zinovi Gerdt starring = Aleksey Batalov Innokenty Smoktunovsky Yevgeniy Yevstigneyev music = Dzhon Ter-Tatevosyan cinematography = German Lavrov editing = Yeva Ladyzhenskaya distributor = Mosfilm Artkino Pictures (1964, USA, subtitled) RUSCICO (2004, worldwide, DVD) released = March 5,  1962 runtime = 111 min. country = Soviet Union language = Russian budget =  gross = 
}} Soviet black-and-white drama film directed by Mikhail Romm about nuclear particle physics, Soviet scientists (physicists) and their relationship.

The film won the Crystal Globe Award in 1962.

==Plot summary== dose of radiation. Gusev is irradiated also. Doctors warn that any more radiation will kill him. At that time his friend Ilya and Lyolya who loved Dmitri developed a romantic relationship. The enamoured couple prepares for a wedding and look for a possibility to inform Dmitri about it. When they meet, Dmitri already suspects this and coldly received Lyolya and Ilya. Lyolya is caught up in self-contradictions, and as she tries to establish his true feelings for her she learns about the terrible diagnosis. Realising that she still loves Dmitri, Lyolya cancels her wedding to Kulikov and weds Gusev.
 bone marrow transplantation.

==Reception==
* 

==Cast==
* Aleksey Batalov as Dmitri Gusev
* Innokenty Smoktunovsky as Ilya Kulikov
* Tatyana Lavrova as Lyolya
* Nikolai Plotnikov as professor Sintsov
* Sergei Blinnikov
* Yevgeniy Yevstigneyev as Nikolai Ivanovich
* Mikhail Kozakov
* Valentin Nikulin
* Pavel Shpringfeld
* Aleksandr Pelevin
* Yevgeni Teterin as professor Pokrovsky (surgeon) Nikolai Sergeyev as Gusevs Father
* Ada Vojtsik
* Valentina Belyayeva as doctor
* Lyusyena Ovchinnikova

Off-screen voice by Zinovi Gerdt (narrator).

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 