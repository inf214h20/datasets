Mexico (film)
{{Infobox Hollywood cartoon|
| cartoon_name = Mexico
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline Pinto Colvig
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = November 17, 1930
| color_process = Black and white
| runtime = 5:42 English
| preceded_by = The Navy
| followed_by = Africa (film)|Africa 
}}

Mexico is a 1930 short animated film by Walter Lantz Productions, and stars Oswald the Lucky Rabbit. As of 2012, the cartoon only exist as a silent print.

==Plot==
Oswald and a big bear are competing each other in a cock fight. While the bears rooster is larger and stronger, Oswalds is more agile. After his cock gets knocked down by a hard impact, Oswald feeds it with some special seeds. The seeds benefitted the rabbits chicken significantly until the bears was taken down for good.

Following his victory at the cock fighting event, Oswald is being paraded on a limousine. On the way, he spots a female raccoon whom he finds very attractive. Oswald jumps out of the car and comes to her. After a brief conversation, the rabbit and the raccoon became friends and started dancing around.

Back at the cock fighting arena, the bear was depressed by his defeat and doesnt know what to do next. He tries to get his rooster back on its feet, only to see it fall again. The disgruntled bear then walks out and decides to take his frustration on Oswald.

The film scene returns to Oswald who is still dancing around with the raccoon. Just then, the bear rushes in and confronts him. The bear tries to attack with a kick but Oswald cleverly catches the assailants leg. The rabbit then takes off the bears boot and gives a tickle on the foot, therefore incapacitating the bear who laughs uncontrollably. The bear attempts another assault, only to be tickled off by Oswald in the abdomen. In no time, Oswalds rooster comes in and tickles the bear out of the picture. Oswald then resumes his date with the raccoon.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 