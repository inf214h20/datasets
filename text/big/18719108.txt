Sundo
{{Infobox Film 
|  name               = Sundo
|  image              = Sundo.jpg
|  caption            = Theatrical poster
|  writer             = Aloy Adlawan 
|  starring           = Robin Padilla Katrina Halili Rhian Ramos Hero Angeles Mark Bautista and Sunshine Dizon
|  director           = Topel Lee 
|  producer           = Jose Mari Abacan Topel Lee
|  music              = Carmina Robles Cuya
|  cinematography     = J.A. Tadena
|  editing            = Marya Ignacio 
|  distributor        = GMA Films
|  released           = March 18, 2009
|  runtime            = 161 minutes Filipino Tagalog Tagalog English English
|  preceded_by        = 
|  followed_by        = 
|  budget             = Philippine peso|Php34,500,000
|  gross              = Philippine peso|Php100,079,566
|  country            = Philippines
}} thriller film director Topel Ouija screenwriter Aloy Adlawan. The film stars Robin Padilla, Katrina Halili, Rhian Ramos, Hero Angeles, Mark Bautista and Sunshine Dizon.  The film was released on March 18, 2009. 

== Plot ==
The film opens with Vanessa (Iza Calzado) who was killed when she was visited by her long dead husband as her companion. The film then cuts to Romano (Robin Padilla), a retired military operative who goes into seclusion after being seriously wounded in battle and almost died, discovers that he has the uncanny ability of seeing ghosts around people who will soon encounter sudden, disastrous deaths. Worried of his brother, Romano’s blind sister Isabel (Rhian Ramos) persuades him to return to Manila with the help of Louella (Sunshine Dizon), Romano’s childhood friend who harbors affection for him.

An aspiring actress, Kristina (Katrina Halili), a widow, Lumen (Glydel Mercado), and her nephew, Eric (Hero Angeles) hitch a ride with Romano, Isabel and Louella along with Louella’s driver, Baste (Mark Bautista). On their way back to the city, the group manages to avoid a fatal accident on the road and their respective “sundo” or ghost companion by a twist of fate. After surviving, the group took their supper in a local eatery somewhere in Baguio. Lumen, who was eager to pee, went into the bathroom where she died as a rail spike pins her head. Soon, Eric died in a series of odd reactions similar to Final Destination happened as he was electrocuted.

The group realized that there is a way to cheat death as Baste and Romano nearly survives a large explosion in a public market killing a large populace. Then the group starts to die in gruesome ways and Kristina, Baste and Isabel died in mysterious ways. Romano was confronted by death for the last time in the rooftop and he fell in the tower towards the pool. He rose up the pool and saw Louella grieving not because of Romano but how she want to avenge her fathers death (in the film due to family matters). Romano turns around and saw his dead body lying on the pool. He was the ghost companion of Louella, the last one to die.

== Principal cast ==
* Robin Padilla as Romano 
* Sunshine Dizon as Louella
* Rhian Ramos as Isabel 
* Katrina Halili as Kristina
* Hero Angeles as Eric 
* Mark Bautista as Baste 
* Glydel Mercado as Lumen
* Jacklyn Jose as Mercedes
* Iza Calzado as Vanessa

== Sundo 2 Cast ==

NO DETAILS YET...

== Reception == Ouija and Shake, Rattle & Roll 8|Yaya.  However, the film is criticized for being "too violent" for a PG-13 movie by critic Veronica R. Samio, whilst praising the film for its fast pace and screenwriter Aloy Adlawans "shocking" twist ending. 

The final gross of the movie is P100,079,566 according to box office mojo.

==Media Release==
The series was released onto DVD-format and VCD-format by GMA Records. The DVD contained the full-length trailer of the movie. The DVD/VCD was released in 2009.

== See also ==
*GMA Films
*Ouija (2007 film)

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 