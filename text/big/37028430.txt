Runaway Daughters (1956 film)
{{Infobox film
| name           = Runaway Daughters
| image          =
| caption        =
| director       = Edward L. Cahn Alex Gordon
| writer         = Lou Rusoff
| based on       =
| starring       = Marla English John Litel Anna Sten
| music          =
| cinematography =
| editing        =
| studio         = American International Pictures
| distributor    =
| released       =  
| runtime        = 91 mins
| country        = United States
| language       = English
| budget         = $90,000 Samuel Z Arkoff & Richard Turbo, Flying Through Hollywood By the Seat of My Pants, Birch Lane Press, 1992 p 50  
| gross          = 
}} remade in 1994. The film was released by American International Pictures as a double feature with Shake, Rattle & Rock! (1956 film)|Shake, Rattle and Rock.

==Production==
The script was allegedly based on an incident that writer Lou Rusoff came across when he worked as a social worker. 

Anna Sten came out of retirement to make the movie. Tom Conway had a stroke during filming and was replaced by John Litel. Gary A. Smith, American International Pictures: The Golden Years, Bear Manor Media 2014 p 40  The film was shot in nine days, only running two and a half hours into overtime. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p65-68 

Male lead Steve Terrell was signed to a long term contract by Arkoff for 15 films. 

==References==
 

==External links==
*  at TCMDB
*  at IMDB

 

 
 
 

 