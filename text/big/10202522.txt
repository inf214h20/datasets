Jungle Queen (serial)
 
{{Infobox film
| name           = Jungle Queen
| image_size     =
| image	=	Jungle Queen FilmPoster.jpeg
| caption        = Ray Taylor
| producer       =
| writer         =
| narrator       =
| starring       = Edward Norris Eddie Quillan Douglass Dumbrille Lois Collier Ruth Roman Tala Birell Clarence Muse
| music          =
| cinematography =
| editing        =
| distributor    = Universal Pictures
| released       = 1945
| runtime        = 13 chapters (219 min)
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 Universal Serial movie serial.

Material from this serial was re-edited into the feature film Jungle Safari (1956).

==Plot== British Middle Africa as a first step in conquering Africa.  Attempting to place their own sympathiser in charge of the local tribe, they face resistance from Pamela Courtney searching for her father, a pair of American volunteers and the mysterious Jungle Queel Lothel, who appears out of nowhere in her nightgown to give advice and instructions to the tribe.

==Cast==
*Edward Norris as Bob Elliot
*Eddie Quillan as Chuck Kelly Nazi villain
*Lois Collier as Pamela Courtney
*Ruth Roman as Lothel, Jungle Queen
*Tala Birell as Dr. Elise Bork
*Clarence Muse as Kyba
*Cy Kendall as Tambosa Tim
*Clinton Rosemond as Godac
*Lumsden Hare as Mr X
*Lester Matthews as Commissioner Braham Chatterton
*Napoleon Simpson as Maati
*Budd Buster as Jungle Jack
*Emmett Smith as Noma
*Jim Basquette as Orbon

==Critical reception==
Cline writes that "although well produced, it often became bogged down with complicated plot twists, psychological debates and confusion as to who was on whose side, and what was really being accomplished."  The Jungle Queen herself is never adequately explained. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 35
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# Invitation to Danger
# Jungle Sacrifice
# The Flaming Mountain
# Wildcat Stampede
# The Burning Jungle
# Danger Ship
# Trip-wire Murder
# The Mortar Bomb
# Death Watch
# Execution Chamber
# The Trail of Doom
# Dragged Under
# The Secret of the Sword!
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 239
 | chapter = Filmography
 }} 

==Production==
The serial reuses aircraft footage from Five Came Back (1939) as well as volcano and crocodile attack footage from East of Borneo (1931).

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{Succession box Universal Serial Serial
| before=Mystery of the River Boat (1944)
| years=Jungle Queen (1945) The Master Key (1945)}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 