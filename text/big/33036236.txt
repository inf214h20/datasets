Sacred and Profane Love (film)
{{infobox film
| name           = Sacred and Profane Love
| image          =Sacred and Profane Love (1921) - Nagel & Ferguson.jpg
| imagesize      =
| caption        =Film still with Nagel and Ferguson
| director       = William Desmond Taylor
| producer       = Adolph Zukor Jesse Lasky
| writer         =   (writer/scenario)
| starring       = Elsie Ferguson Conrad Nagel
| music          =
| cinematography = Ted D. McCord James Van Trees
| editing        =
| distributor    = Paramount Pictures
| released       = May 22, 1921
| runtime        = 5 to 6 reels
| country        = United States
| language       = Silent film (English intertitles)
}}
 1921 silent silent film produced by Famous Players-Lasky and distributed by Paramount Pictures. This film was directed by William Desmond Taylor and starred Elsie Ferguson with Conrad Nagel. It is based on a book The Book of Carlotta by Arnold Bennett and was turned into a 1920 Broadway play which also starred Elsie Ferguson. Writer/director Julia Crawford Ivers adapted the book and play to the screen while her son James Van Trees served as one of the films cinematographers. All known copies of this film are lost film|lost.  

==Plot==
As described in a film publication summary,    Carlotta Peel (Ferguson), brought up by a maiden aunt with maiden ideas, secretly attends a concert by Emilie Diaz (Nagel). After the concert she meets the pianist and later succumbs to the strains of "Samson and Delilah" played by Emilie. Carlotta spends the night with Emilie and returns home the next morning to find her aunt dead. She does not see Emilie again, and after several years she is a well known novelist who is loved by her publisher, Frank Ispenlove (Holding). The publishers wife Mary (Greenwood) commits suicide because of her husbands affair with Carlotta. Frank then kills himself. After some time and at a different locality, Carlotta finds Emilie, a victim of absinthe. She nurses him back to health and his musical gift is restored. She is now happy with her first love.

==Cast==
*Elsie Ferguson - Carlotta Peel
*Conrad Nagel - Emilie Diaz, pianist
*Thomas Holding - Frank Ispenlove
*Helen Dunbar - Constance Peel
*Winifred Greenwood - Mary Ispenlove
*Raymond Blathwayt - Lord Francis Alcar
*Clarissa Selwynne - Mrs. Sardis
*Howard Gaye - Albert Vicary
*Forrest Stanley - Samson
*Jane Keckley - Rebecca

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 