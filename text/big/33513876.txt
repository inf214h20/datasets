On the Sunny Side (1961 film)
{{Infobox film
| name           = Auf der Sonnenseite
| image          =
| image size     =
| caption        =
| director       = Ralf Kirsten
| producer       = Alexander Lösche
| writer         = Heinz Kahlau, Gisela Steineckert, Ralf Kirsten
| narrator       = Manfred Krug
| starring       = Manfred Krug
| music          = Andre Asriel, Manfred Krug and the Jazz Optimists from Berlin
| cinematography = Hans Heinrich
| editing        = Christel Röhl
| studio         = DEFA
| distributor    = PROGRESS-Film Verleih
| released       = 5 January 1962
| runtime        = 101 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

On the Sunny Side (  musical comedy film, directed by Ralf Kirsten and starring Manfred Krug. It was released in List of East German films|1962.

==Plot==
Martin Hoff, a steel smelter and an amateur actor and Jazz singer, is sent to a drama school by his factorys committee. Due to his troublesome conduct, he is expelled. Hoff meets a young woman called Ottilie who is unimpressed by him; he bets with his friends that he shall manage to charm her. Hoff begins to work in the construction site where she serves as a group manager, although she has troubles enforcing her will on her male subordinates. He only has success with her after director Jens Krüger guides him to become a diligent laborer. In addition to his work, he continues to perform in amateur plays. When theater manager Pabst sees him acting, he invites him to join his cast. Hoff and Ottilie decide to marry.

==Cast==
* Manfred Krug: Martin Hoff
* Marita Böhme: Ottilie Zinn Heinz Schubert: Felix Schnepf
* Rolf Herricht: Hoffs friend
* Peter Sturm: Pabst
* Fred Mahr: Jens Krüger
* Gert Andreae: Matze Wind
* Günter Naumann: driver
* Werner Lierck: director
* Willi Neuenhahn: viewer
* Rolf Römer: Hoffs friend
* Heinz Lyschik: Hoffs friend

==Production==
During the end of the 1950s, the DEFA Studios management noted the publics demand for entertainment and responded by producing a series of light comedies, relatively free from ideological messages. According to Heiko R. Blum, On the Sunny Side "distinguished itself by its unmitigated portrayal of real life." 

The films script was largely inspired by leading star Manfred Krugs biography: he worked in a steel factory before turning to an acting career. Sabine Hake. German National Cinema. Routledge (2002). ISBN 978-0-415-08901-2. Page 130.  Krugs Jazz band and his singing career were also a central theme in the plot.  

==Reception==
According to DEFA historian Dagmar Schittly, On the Sunny Side was the most popular East German film of the early 1960, and was very successful with the audience.  Director Ralf Kirsten, Krug, writers Heinz Kahlau and Gisela Steineckert and cinematographer Hans Heinrich were awarded the Heinrich Greif Prize 1st class in collective for their work on the picture at March 1962.  Steineckert also received the Art Prize of the Free German Trade Union Federation. 
 star system, the film turned Krug into East Germanys only "genuine screen idol".  On the Sunny Side was the actors breakthrough picture, and he became famous after its release.  Ina-Lyn Reif wrote that the states artistic establishment perceived Krug as "capturing the younger generations mood" after the release of On the Sunny Side. 
 jeans in a neutral manner; Krug and other workers wore them. At the time, they were considered a bourgeois form of clothing and were banned in schools. 

==References==
 

==External links==
*  on the IMDb.

 
 
 
 
 