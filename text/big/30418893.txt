Tantric Tourists
{{Multiple issues|
 
 
 
}}

 
 

Tantric Tourists is an independent British feature documentary film directed by Alexander Snelling and produced though Slack Alice Films by Kirsty Allison and Alexander Snelling. The film was shot entirely in India over a period of two weeks and is essentially a road movie, featuring various forms of transport, mainly bus, but also train, plane, boat, elephant, bicycle and rickshaw.

==Synopsis==
A group of American Tourists go to India in search of enlightenment. The main character in the film is group leader Laurie Handlers, a larger-than-life guru from New York City who practices and teaches tantra and leads a small group of travelers on their first trip to India. The film is a classic mythical journey with the characters learning from their travels.

Although superficially the film may appear to be a mockumentary, all participants are real people and nothing was staged in the filming.  As such the film can be termed a fly-on-the-wall film and has been compared to This Is Spinal Tap for its outrageous and hilarious characters.

==Distribution==
The film is being distributed by the Independent Film Company and is being theatrically released in the UK on 14 February 2010.

==Music==
The film features a rich soundtrack of Indian and Indian-inspired music from:
*Banco de Gaia
*Asha Bhosle
*Lata Mangeshkar
*Mohammed Rafi
*Hariprasad Chaurasia
*Sirus Severin

==Awards==
The film has won several awards most notably:
* Best UK First Feature at Londons East End Film Festival 
* Best First Time Director (Nevada Film Festival 
* Golden Palm (Mexico International Film Festival)
* Rising Star Award (Canada International Film Festival) 

==Tantra==
The use of the word tantra in the title of the film relates to the ancient Hindu art  and not the modern Western interpretation  that tends to associate tantra uniquely with sex.

== References ==
 
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 
 