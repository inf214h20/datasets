Ira Madiyama
{{Infobox film
| name = Ira Madiyama (August Sun)
| image =
| caption =
| director = Prasanna Vithanage
| starring = Peter D Almeida Nimmi Harasgama Namal Jayasinghe Mohamed Rahfiulla 
| released =  
| runtime = 108 minutes
| country = Sri Lanka Sinhala & Tamil
}}

Ira Madiyama (August Sun) is a bi-lingual feature film directed by award-winning filmmaker Prasanna Vithanage. 

The film premiered at the Montreal World Film Festival. 

==Synopsis==
Ira Madiyama (August Sun) set in Sri Lanka during the mid-1990s and tells three simultaneous stories against the backdrop of the countrys savage civil war (1983 – 2009).

Chamari (Nimmi Harasgama) is searching for her husband, a Sinhalese Sri Lankan Air Force pilot shot down in flight, whom she believes has been taken prisoner by the Tamil Tigers. Desperate to know the truth, she enlists a sympathetic journalist and sets out on a journey to track him down. 

Meanwhile, eleven-year-old Tamil Muslim Arfath (Mohamed Rahfiulla) is struggling to keep his companion and friend, a dog, while the family together with the entire village is forced to evacuate by a rebel army.

The third narrative follows Duminda (Namal Jayasinghe), a young soldier who walks into a brothel to find his sister among the working girls.

The main action of the film takes place in Sri Lanka’s northern territories, parts of which are controlled by the Tamil rebels who have created a de facto separate state. 

These stories are about people who are struggling to hold on to their hopes and dreams while being swept up by the torrents of war. The film is about their quest for life. 

==Cast==
{| class="wikitable"
|- 
! Actor !! Role 
|- Peter D Almeida || Saman
|-  Nimmi Harasgama|| Chamari
|- Namal Jayasinghe|| Duminda
|- Mohamed Rahfiulla || Arfath
|- Shoaib Mansoor || Hassan
|-
|}

==Music==
The original music for Ira Madiyama was composed by Lakshman Joseph De Saram.

==Trivia==
* Ira Madiyama (August Sun) was a bi-lingual film in Sinhala & Tamil.

* The father and son characters of Hassan and Arfath, were real-life refugees who had fled their hometown of Mannar under similar circumstances.

* Although, set against the backdrop of war and ethnic conflict the film focused on the social fabrics of the nation. There were no warfare scenes in the film.

* The events in Ira Madiyama (August Sun) take place on the day Sri Lanka became world champions when they beat Australia in the 1996 Cricket World Cup final in Lahore, Pakistan. It is also the period immediately following the breakdown of the first attempt at peace talks between the Sri Lankan government and the Liberation Tigers of Tamil Eelam (LTTE). So it was a time of great tension and uncertainty mixed with elation at the cricketing success. 

==Theatrical Release==

===Sri Lanka===

Ira Madiyama (August Sun) saw its domestic release across Sri Lanka on 10 February 2005. It opened across 16 centres islandwide. It ran for 55 days to good critical and commercial response. 

===Singapore===

Ira Madiyama (August Sun) and Akasa Kusum (Flowers of the Sky) are set for limited release from 18 June 2010, at Sinema Old School  a 136-seater high definition cinema screening local and award-winning films in Singapore. 

==International Film Festivals==

===Awards===

* Grand Prix - Special Mention, Fribourg International Film Festival, Switzerland

* FIPRESCI - NETPAC Award, Singapore International Film Festival, Singapore

* Best Film - Silver Award Lady Harimaguada De Plata, Las Palmas International Film Festival, Canary Islands

* Best Actress - Nimmi Harasgama, Las Palmas International Film Festival, Canary Islands

* Grand Jury Prize, Makati Cinemanila International Film Festival, Philippines

===Official Selections===

* Montreal World Film Festival, Canada (World Premiere)

* Quebec International Film Festival, Canada

* International Film Festival of India, India

* Chennai International Film Festival, India

* Kolkata Film Festival, India

* Pusan International Film Festival, Korea

* London Film Festival, United Kingdom

* Bite the Mango, Bradford, United Kingdom

* Amiens International Film Festival, France

* Deauville Asian Film Festival, France

* Fukuoka International Film Festival, Japan

* Cinequest International Film Festival, USA

* Istanbul International Film Festival, Turkey

* Durbun International Film Festival, South Africa

* Hong Kong Asian Film Festival, Hong Kong SAR

* Taiwan Film Festival, Chinese Taipei

* Film Fest Hamburg, Germany

* Dhaka International Film Festival, Bangladesh

* Films from the South Festival, Oslo, Norway

==References==
 

==External links==
*  
*  

 

 
 
 