Gretchen (film)
{{Infobox film
| name           = Gretchen
| image          =
| caption        =
| director       = Steve Collins
| producer       = Rajen Savjani
| starring       = 
| music          = 
| cinematography = PJ Raval
| editing        = Mike Shen
| studio         = Film Science Van Hoy/Knudsen Productions
| distributor    = Film1 Watchmaker Films
| released       = 
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Gretchen is a 2006 film directed by Steve Collins, starring Courtney Davis as the title character. The film was Collins first full-length feature and was based on his short film Gretchen & the Night Danger.

==Plot==
Gretchen Finkle (Courtney Davis) is a 17-year-old high school student with a romantic obsession over Ricky (John Merriman). Her mother (Becky Ann Baker) becomes so concerned about Gretchens crush that she sends her daughter to an in-patient emotional therapy clinic.

==Cast==
* Courtney Davis as Gretchen Finkle
* John Merriman as Ricky Marichino
* Macon Blair as Nick Rangoon 
* Becky Ann Baker as Lori Finkle
* Stephen Root as Herb
* Peyton Hayslip as Miss Jennings

==Production==

===Development===
Gretchen is a full-length adaptation of Steve Collins 2004 short film Gretchen & the Night Danger, which won the Jury Award for Competition Shorts at South By Southwest.  

==Reception==

===Critical response===
Sky Hirschkron, writing for Stylus Magazine, stated, "Collins is perceptive to Gretchen’s status as a loser; the problem is that he offers nothing to counterbalance it, resulting in a perspective inadvertently as cruel as her tormentors".  Film Threat s Don Lewis commented, "Gretchen is a really dry, funny and sad film   The bond between Gretchen and her mother is a sweet touch and without it, one might think Collins was cinematically abusing Gretchen just as everyone else does throughout the film." 

===Accolades===
 
{| class="wikitable" style="width:95%;"
|+List of awards and nominations
! | Award
! | Category
! | Nominee
! | Result
|- Los Angeles Film Festival   Best Dramatic Feature Steve Collins
| 
|-
|}

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 