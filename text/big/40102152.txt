Entertainment (film)
 

 

{{Infobox film
| name = Entertainment
| image = Its Entertainment.jpg
| alt = Entertainment
| director = Sajid-Farhad
| producer = Ramesh S. Taurani Jayantilal Gada
| writer = Sajid-Farhad  (dialogue) 
| story = K. Subhash
| screenplay = Sajid-Farhad
| starring = 
| music = Sachin - Jigar
| cinematography = Manoj Soni
| editing = Steven Bernard
| studio = Tips Industries Limited
| distributor = Pen Movies
| released =  
| runtime = 140 minutes
| country = India
| language = Hindi
| budget =
| gross =   
}}

Entertainment is a 2014 Bollywood comedy film directed by duo Sajid-Farhad in their directorial debut. Starring Akshay Kumar and Tamannaah Bhatia in the lead role, the film is produced by Ramesh S. Taurani of Tips Industries Limited. Entertainment was released on 8 August 2014 to positive reviews. The movie was declared a semi hit.

== Plot == Tamannaah Bhatia) is shooting for her television series. After her shoot lets out, they go for a walk around the park, observing other couples. At the end of their walk, Akhil proposes to Saakshi. Akhil and Saakshi go to her fathers (Mithun Chakraborty) house, where they are told that until Akhil becomes rich, he cant marry Saakshi.
Akhil goes to his film-obsessed friend, Jugnus (Krushna Abhishek)  shop. He tells him that he is going to visit his father at the hospital because he has chest pain. However, his father(Darshan Jariwala), is actually acting and is dancing around with the nurse, only staying in the hospital because it has service like a 5-Star hotel. Akhil arrives just in time to hear and see this, and goes to beat up his father, when he reveals that Akhil is adopted and his real father wasnt ready for a child so his mother left, but was killed in a train crash. Luckily Akhil survived, and was sent to his adoptive father. Then Akhil beats him up. He goes back to his house, and opens the chest that he never had before, which contains love letters from his real father, to his mother, and a locket with his father and mothers pictures. He discovers that his father is Pannalal Johri (Dalip Tahil) who is a billionaire in Bangkok. Right as he finds this, on the television comes news that Pannalal Johri is dead and his 3000 crores will go to anyone who can prove that they are related to Pannalal Johri.
After arriving at Johri mansion, his fathers house at Bangkok, he discovers that a dog named Entertainment is the owner of Johris property. They try to kill Entertainment, by having Akhil become Entertainments caretaker, but fail. Saakshi and her father surprise Akhil at the mansion, and discover that Entertainment is the heir, causing the father to declare that Saakshi will never marry Akhil. At this time 2 brothers, Karan (Prakash Raj) and Arjun (Sonu Sood), the 2nd cousins of Johri, escape jail. 
Karan and Arjun want to kill Akhil, because they figure that the dog will die in a couple of years anyway, so while Akhil is attempting to kill Entertainment, by drowning him under a thinly iced lake. Karan and Arjun hit the ground at the same time as Akhil causing him to nearly fall in, but Entertainment saves the former, and falls into the lake instead. After a lot of efforts Akhil fails to save entertainment but suddenly entertainment comes out of the water and they develop feelings for each other. Akhil realizes his mistake and learns that Entertainment is good hearted and good dog so he becomes his friend and leaves the mansion. Karan and Arjun, citing that their lawyer knows the judge very well, inappropriately acquire the property from the Entertainment- the dog.

Akhil learns this news and returns. He vows that hell get the property back for Entertainment. Akhils strategy is to divide (the two brothers) and conquer. He manages to get a job as a servant at the Mansion, now owned by Karan and Arjun. Akhil attempts to split the two brothers, first by introducing Saakshi who seduces the two brothers separately. However, the scheme fails as background music rejoices the two brothers. Akhil again attempts trying to split two brothers, this time by introducing a false impression of ghost of Entertainment-the dog. The two brothers quarrel and accuse each other of illegally disguising as Entertainment and they tricked the judges. Akhils friends tape this conversation slyly, however, the CD drops accidentally and Karan and Arjun realize that all this was planned by Akhil.

Entertainment runs with the CD. Karan, Arjun and their gang chase the dog, Akhil, and his friends. Akhil manages to beat them all. Later, Karan shoots Akhil, but Entertainment takes the bullet by jumping in front of Akhil. Akhil gets angry and beats up Karan black and blue.

Entertainment is taken to the hospital but unable to get saved, Akhil in a fit of anger he punches him but at this process he had saved him. Karan and Arjun come to apologize and are forgiven. Saakshis father gets Akhil and Saakshi married while Entertainment also marries a female dog, this starts a party song "Veerey Di Wedding". This film ends when it shows that Akhil and Saakshi lives a life with Entertainment and female dog and finally at the end it shows the social message Love your pets and they will entertain you all life

== Cast ==
* Junior as Entertainment
* Akshay Kumar as Akhil Pannalal Johri/Akhil Lokhande
* Tamannaah Bhatia as Saakshi
* Krushna Abhishek as Jugnu
* Johnny Lever as Habibullah Shiekh
* Prakash Raj as Karan
* Sonu Sood as Arjun
* Mithun Chakraborty as Saakshis Father
* Ritesh Deshmukh as Jagtap Capsules teleshopping host (cameo)  . 1 August 2014. 
* Shreyas Talpade as cricketer (cameo) 
* Remo DSouza as choreographer 
* Dalip Tahil as Pannalal Johri  (cameo)
* Vrajesh Hirjee as Wannabe groom (cameo)
* Darshan Jariwala as Akhils adoptive father (cameo)
* Hiten Tejwani as the show actor (cameo)  
* Damian Mavis as Thug

== Production ==

 
{{Quote box
| quote="This film is full of love and laughter. Apart from working with Tamannaah, the best bit is that for the first time in my career, Ill be working with a dog! Ive romanced women; Ive played with babies; Ive done every stunt under the sun. Now, its time for me to take a cue from a canine. This film promises fun and it has got what everyone loves - dogs, dogs and more dogs." 
| source=Akshay Kumar on Entertainment. 
| width=35%}}
In February 2013, Taurani confirmed that the film will feature Kumar in the lead role.  Bhatias involvement with the project was confirmed the following month.  In April Taurani confirmed that Sonu Sood and Prakash Raj will be playing the antagonists and that Johnny Lever was also part of the project.  Sood had earlier acted alongside Kumar in the comedy drama Singh Is Kinng.  The film was tentatively titled Entertainment.  In an interview Sajid-Farhad said that Kumar had motivated them to start their directing career.  They said that they had prepared the script for their directorial venture a long time ago.  Shooting started on 3 June 2013 in Mumbai where the mahurat shot was filmed.  Television actor Hiten Tejwani also did a special appearance as a TV presenter.  An audition was conducted in Bangkok for the role of the dog–Entertainment. A Golden Retriever named Wonder Dog was chosen from a total of 50 dogs to play the role.     In June 2013 the producers, directors and Kumar discussed about shooting locations. Kumar expresses his wish to shoot the film in Bangkok, where he had worked as a waiter and trained in martial arts. The finalised locations included Baanpradhana Bungalow in Ongkuruk, Asiatique Mall, Ancient City, Bangkok University and Ongkuruk Railway Station. 

The film was scheduled to premiere on 28 March 2014.   On 1 July shooting started in Bangkok for a 3-months-long schedule where 80% of the film was shot,  remaining 10% of the film was shot in Mumbai with one song in Goa during late 2013. Shooting was wrapped up in April 2014.   A few songs and an item number were shot in Mumbai.  The Censor Board of Film Certification in India had objected to Abdullah, the name of Johnny Levers character in the film. The boards objection was based on the fact that the name of the character is sacred and it was mispronounced by several other characters throughout the film and this would have hurt the religious sentiments of people. The board had asked Sajid Farhad to change the characters name. They complied with the boards instructions and changed the name to Habibullah. The changes were made just a week before the release of the film. The board had also objected to use of the word HIV during a joke in the film and subsequently it was muted. Another of boards objection was the use of trident by Kumars character in the film. The shot was removed. 

All the costumes used in the filming were donated to the youth organization in Defense of Animals. It auctioned the clothes and the money collected in this way was used for the benefit of stray animals. Kumar also bought one clothing item. The lead actress Tammanah Bhatia stated that she would donate all the costumes used by her in her future film projects to such initiatives.  Earlier titled Its Entertainment, the teaser trailer of the movie unveiled on 14 May  whilst the theatrical trailer was released on 19 May.  Politician and animal rights activist Maneka Gandhi was the chief guest at the trailer launch ceremony.  The producers initially wanted the film to be titled Entertainment but Amole Gupte had already registered for his upcoming film but in July 2014 Gupte donated it to the Sajid-Farhad as a friendly gesture.  Kumar decided that the credit for the dog Junior should appear before his.  Some fraudsters released a poster of the film and invited people to attend the music launch at Birla Matoshree. Producer Taurani warned people about the fraud posters.  The films satellite right were sold to Zee TV; though the producers did not disclose the amount, media speculated that the deal was worth  . 

== Soundtrack ==

{{Infobox album  
| Name = Entertainment
| Type = Soundtrack
| Artist = Sachin - Jigar
| Cover =
| Alt =
| Released =  
| Recorded =
| Genre =
| Length =  
| Label = Tips Industries Limited
| Producer = Tips Industries Limited
| Last album = Humpty Sharma Ki Dulhania (2014)
| This album = Entertainment (2014)
| Next album = Bey Yaar (2014)
}}

The soundtrack is composed by Sachin - Jigar. It will be the first time that Sachin - Jigar have worked with Akshay Kumar. The song "Teri Mahima Aprampar" was taken from the Telugu film D for Dopidi. Kumar also sang one of the tracks of the film. The YouTube link of the making of the song was uploaded on Twitter.  "Johnny Johnny" was released first. Based on a nursery rhyme, the song featured Kumar lip-synching to a female voice. The directors said about the song "In a small subtle way, we are showing our support for women empowerment".  Sunitra Pacheco of The Indian Express said that it had "all the ingredients to become a new party anthem".  In a review for Rediff.com Joginder Tuteja said that the films music "is entertaining, providing good fun at a frantic pace."     He called "Veerey Di Wedding" "an infectious number" and praised Mikas voice. About the song "Johnny Johnny", he said "It has addictive beats that get you hooked at the first listening" and praised the lyrics and singers. He added that the song "will be played for many months, till the next New Year party at least." 

{{Track listing
|headline=Track listing
|lyrics_credits=yes
|music_credits=yes
|title1=Johnny Johnny
|music1= Jigar Saraiya, Priya Panchaal & Madhav Krishnan 
|lyrics1= Mayur Puri
|length1=3:38
|title2=Tera Naam Doon
|music2= Atif Aslam & Shalmali Kholgade 
|lyrics2= Priya Panchaal 
|length2= 4:44
|title3=Veerey Di Wedding
|music3= Mika Singh
|lyrics3= Mayur Puri
|length3= 3:34
|title4=Teri Mahima Aprampar
|music4= Udit Narayan & Anushka Manchanda
|lyrics4= Ashish Pandit
|length4= 3:45
|title5=Nahi Woh Saamne
|music5= Atif Aslam
|lyrics5= Priya Panchaal
|length5=1:48
|title6=Veerey Di Wedding(Remix)
|music6= Mika Singh
|lyrics6= Mayur Puri
|length6=3:37
|title7=Johnny Johnny (Remix) 
|music7=  Mika Singh
|lyrics7= Mayur Puri
|length7=3:38
|title8=Maange Maange 
|music8=  Akshay Kumar
|lyrics8= —

|length8=2:57
}}

== Critical reception ==

Entertainment received positive reviews from critics. Meena Iyer of The Times of India praised the films humour, Kumars acting and his bonding with the dog, especially in comic scenes. She stated that though the dog was not a good actor yet "his eyes can melt your heart." She said that the directors could have kept the film "tight".  Mihir Fadnavis of Firstpost called it a "sheer genius" and said that it was Kumars funniest film since Hera Pheri. He wrote that the film satirised all of the Sajid-Farhad films and took "potshots at everything that is wrong with desi cinema and television." He also appreciated the scene transitions and called it "technically great."  Hindustan Times  Sweta Kaushal criticised the screenplay, Kumar and Prakash Rajs acting. She said that Bhatia "  have much to do" and Chakraborty did not have much screenspace. However, she praised Abhisheks comedy and said that it was the "  bright spark in the film." She concluded her review by saying that the film was neither a "great piece of art" nor entertaining.  Prior to the films release, Hindustan Times published an article titled Four reasons why you shouldnt waste your time on Entertainment and criticed the films story, songs and dialogues.  Shubha Shetty-Saha of Mid Day wrote "Entertainment pleasantly surprises with its delightful absurdity and sure gets you roaring with laughter at several points." She called it a child friendly film and appreciated the dialogues and the performance given by the actors. However, Saha stated that the pace of the films second half was slowed down by the excessive melodrama.  Writing for The Indian Express, Shubhra Gupta called the film "dull and loud". She criticised the films story and dialogues.     Rohit Khilnani of India Today appreciated Kumar, Bhatia and Levers performance and wrote that the rest of the cast was wasted. He criticised the plot and called it "senseless and full of loopholes." 
{{quote box source = align = width =25%}}
Sukanya Verma of Rediff.com wrote "Entertainment is unapologetically filmi in its trappings, treatment, thought." She further wrote that the film was "mostly a garrulous, occasionally comical farce that intermittently serves as reminder that in the search of “entertainment, entertainment, entertainment” one can always rely on the delightfully loony Johnny Lever."  Gayatri Sankar of Zee News called Kumars acting one of the best comic performances he had given so far. She also appreciated the films screenplay, cinematography, dialogues and the actors portrayal of their respective characters. She wrote that a few of the songs could have been removed and the directors had succeeded in giving the message of treating "animals with respect and love". {{cite news |last=Sankar|first=Gayatri| url=http://zeenews.india.com/entertainment/bollywood/entertainment-review-simple-lighthearted-comedy-total-paisa-vasool_159736.html | title=‘Entertainment’ review: Simple lighthearted comedy, total paisa vasool
| publisher=Zee News | date=8 August 2014 | accessdate=8 August 2014}}   Writing for CNN-IBN, Rajeev Masand termed the films humour repetitive and lazy. He criticised the films story but appreciated Levers performance by calling him "the single saving grace in this overcooked, misguided comedy".  In his review for Bollywood Hungama, critic Taran Adarsh praised the humorous writing and the comedic acting talents of the actors. He called the film "a joy ride that lives up to its title." {{cite news | last=Adarsh|first= Taran| authorlink= Taran Adarsh
| url=http://www.bollywoodhungama.com/moviemicro/criticreview/id/600589 | title=Entertainment (2014) : Hindi movie critic review by Taran Adarsh| publisher=Bollywood Hungama | date=7 August 2014 | accessdate=7 August 2014}}   However, Daily News and Analysis called it a commercial failure and Tamannah Bhatias third disaster in a row. 

== Box office ==

The film released on 8 August and collected   on the same day.  However, the performance at multiplexes in metropolitan areas was low.  On the third day of release it collected about  .    During its first weekend it managed to gross   and Indian film website Bollywood Hungama declared it Akshay Kumars 7th highest opening weekend grosser.  Entertainment recorded the ninth highest opening weekend collection of 2014.  In its first 5 days of screening it collected around  .  In its first week the film grossed   at the Indian Box office. 

It grossed   on its second Friday, which was a national holiday. The film received tough competition from Ajay Devgan starrer Singham Returns which released on the same day. Entertainment was declared a semi-hit film and trade analysts expected that it would be able to gross   during its total run.  In its second weekend it added approx.   more to its total collection.  Till second week the films total collection had reached approx.  .  However, it performed poorly during the third weekend and collected around  .  The films producer Ramesh Taurani expected the film to do better business but was happy with the overall performance. 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 