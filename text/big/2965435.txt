The Inkwell
{{Infobox film
| name           = The Inkwell
| image          = InwellFilmPoster.jpg
| caption        = Theatrical release poster
| writer         = Trey Ellis Paris Qualles
| starring       = Larenz Tate Joe Morton Suzzanne Douglass Glynn Turman Vanessa Bell Calloway Morris Chestnut 
| director       = Matty Rich 
| producer       = Irving Azoff Matthew Baer Jon J. Jashni Guy Riedel
| studio         = Touchstone Pictures  Buena Vista Pictures
| released       = April 22, 1994
| runtime        = 110 minutes 
| country        = United States 
| language       = English 
| budget         = $8,000,000 (approximately)
| gross          = $8,880,705
}}
 romantic comedy/drama drama film, directed by Matty Rich.    The film stars Larenz Tate, Joe Morton, Suzzanne Douglass, Glynn Turman, and Vanessa Bell Calloway. 

==Plot==
Set in the summer of 1976, the film follows the adventures of Drew Tate (Larenz Tate), a shy 16-year-old from upstate New York, when he and his family spend two weeks with affluent relatives on Marthas Vineyard. Drews parents, Kenny (Joe Morton) and Brenda (Suzzanne Douglass), worry that their son is emotionally disturbed. His favorite companion is a doll, in which he names Iago (after the character in the Shakespeare classic Othello), with which he engages in animated conversations. They also fear that a fire he accidentally set in the family garage foreshadows a future as an arsonist. 
 black society conservatives whose Republican dignitaries such as Barry Goldwater and Ronald Reagan (who they keep saying will become President someday).  Kenny, a former Black Panther, and Spencer argue furiously about racial issues.
 Fourth of Bicentennial fireworks end up symbolizing not just United States|Americas 200th birthday but Drew finally losing his virginity with Heather.

==Cast==
For the 20th anniversary of the film, the cast reunited with writer/filmmaker Lathleen Ade-Brown for   where Larenz Tate spoke about the casting process. He told the magazine "Matty Rich was holding auditions in LA. Jada   was already cast in the role   and I remember her calling me, saying, ‘You got to do this movie!’ In fact, she was saying, ‘Listen, let’s meet up and rehearse because they are going to want me to read with you, so let’s rehearse, so you totally land it!’ I told her, ‘I’m going to rip that role! No need to rehearse, you just keep up with me and we just play off each other.’ She says. ‘I got you, let’s do it!’ I go in the audition and we really just lit up the room, then I had to audition solo. They didn’t know what to expect considering I just did Menace II Society playing O-Dawg, a completely street person. So that impressed them and they offered me the part."

:*Larenz Tate as Drew Tate
:*Joe Morton as Kenny Tate
:*Suzzanne Douglass as Brenda Tate
:*Glynn Turman as Spencer Phillips
:*Vanessa Bell Calloway as Francis Phillips
:*Adrienne-Joi Johnson as Heather Lee
:*Morris Chestnut as Harold Lee
:*Jada Pinkett as Lauren Kelly
:*Duane Martin as Junior Phillips
:*Mary Alice as Evelyn
:*Phyllis Yvonne Stickney as Dr. Wade
:*Markus Redmond as Darryl
:*Perry Moore as Moe
:*Akia Victor as Charlene Jade

==In popular culture==
In the 2000 episode " " of the series Third Watch, the paramedic character List of Third Watch characters#Monte Doc Parker|Doc refers to summer vacations spent at "the Inkwell on the island", without describing precisely what he means.

==References==
 

==External links==
* 
*  including the Inkwell Beach

 
 
 
 
 
 
 
 
 
 
 
 