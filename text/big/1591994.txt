Last Summer
 
{{Infobox film
| name           = Last Summer
| image          = Last Summer - poster - 1969.jpg
| caption        = Theatrical Poster
| director       = Frank Perry
| producer       = 
| writer         = Eleanor Perry
| based on    =  
| narrator       =  Richard Thomas
| music          = John Simon Collin Walcott
| studio         = Alsid Productions
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| gross          = $3 million (US/ Canada rentals) 
}}
 Fire Island Richard Thomas. Kansas City Film Critics Circle Award.

==Synopsis==
While spending the summer on Fire Island, Peter (Richard Thomas) and Dan (Bruce Davison), two adolescent boys from upper-middle-class families, meet Sandy (Barbara Hershey), a young girl who has found a wounded seagull on the beach. After the boys remove a fishhook from the birds throat, the three youngsters become fast friends and spend all their time together, swimming, boating, smoking marijuana and cautiously experimenting with their awakening sexual impulses during visits to a movie house on the mainland.
 Puerto Rican Anibal.

To Rhodas embarrassment, Sandy, Dan, and Peter get the man drunk and abandon him to three local bullies. Although Rhoda rebukes Peter for his behavior, she succeeds only in alienating him, and he goes off with Sandy and Dan for a picnic in the woods. Dans plan of proving his manhood to Sandy is ruined when Rhoda tags along.

Irritated by Rhodas intrusion into their clique, Sandy removes her bikini top and dares Rhoda to do the same. Disgusted, Rhoda tries to leave, but Sandy goads the boys into holding her back. The frightened girl appeals to Peter for help, but he joins Sandy in pinning Rhoda to the ground while she is savagely raped by Dan. Following the assault, the three leave; Sandy and Dan return to the beach while Peter hesitates on a sand dune near Rhoda.

==Production notes== Richard Thomas in a scene from Last Summer filmed on Fire Island.]]
* Eleanor Perrys screenplay was based on the novel by Evan Hunter, published in hardcover by Doubleday (1968) and paperback by Signet (1969). 
* The character of Dan was originally called David in the book but was changed for the film. 
* A subplot involving a wounded seagull affected Barbara Hershey sufficiently for her to change her surname to Seagull for a couple of years. 
* Sondra Locke was offered the role of Sandy, but turned it down. 
* Ralph Waite played father to Richard Thomas in this movie, then went on to play his father again in The Waltons.
* The film takes place almost entirely on Fire Island, a long sandbar off Long Island with the Atlantic Ocean on one side, the Great South Bay on the other, and upper-class summer homes built on its beaches and dunes.  
* Evan Hunter, author of the source novel, wrote a sequel novel, which he titled Come Winter, in 1973. Hunter was working  on the screenplay of Come Winter; however, it was never filmed.
* Last Summer was one of a handful of high-profile X-rated movies that were released in 1969 along with the Oscar-winning best picture Midnight Cowboy and Haskell Wexlers docudrama Medium Cool.
* Given an X rating when it was first submitted to the MPAA due to the scene that depicted Rhodas rape, the original X-rated version of Last Summer never saw the light of day, as the producers did not want a scandalous rating to detract from the movie itself. It was then edited down to an MPAA R-rating for its theatrical release, but it still has nudity and strong language. When the film is transmitted occasionally on cable, a further-censored PG-rated version is presented which cuts out all nudity and heavily edits the scene of Rhodas rape. The R-rated version is the one seen in the VHS videotape release. The film is not available on DVD, nor is the original X-rated version.
* All original 35mm prints of the film were lost for years. In 2001 a 16mm print was located at the National Film and Sound Archive of Australia after a two-year search and was brought to Los Angeles. Apparently it was the only surviving film version of the movie.

==Soundtrack==
The film had a soundtrack LP (Warner Bros.-Seven Arts WS 1791) of the score composed by John Simon and Collin Walcott. Heard on the soundtrack: John Simon (piano), Collin Walcott (sitar, tamboura), Aunt Marys Transcendental Slip and Lurch Band (rock band), Cyrus Faryar (voice), Buddy Bruno (voice), Ray Draper (tuba, voice), Electric Meatball (rock band), Henry Diltz (banjo, voice), Bad Kharma Dan and the Bicycle Brothers (motorcycle gang). Rick Danko, Levon Helm and Richard Manuel of The Band played on the soundtrack as well, but were uncredited due to having had a contract on another record label.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 