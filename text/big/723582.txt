The Story of Robin Hood and His Merrie Men
{{Infobox film
| name = The Story of Robin Hood and His Merrie Men
| image = Story of robin hoodsxf.jpg
|caption=Theatrical release poster
| writer = Lawrence Edward Watkin
| starring = Richard Todd Joan Rice Peter Finch
| director =  Ken Annakin
| producer = Perce Pearce Guy Green Walt Disney Productions RKO Radio Pictures, Inc.    RKO Radio Pictures, Inc.
| released =   |ref2= }}
| runtime = 84 minutes
| language = English
| music = Clifton Parker 
| budget = 
| gross = $2.1 million (US rentals) 
}} Disney version Treasure Island (1950).

==Cast==
* Richard Todd as Robin Hood
* Joan Rice as Maid Marian
* Peter Finch as the Sheriff of Nottingham James Hayter Hammer film A Challenge for Robin Hood.
* James Robertson Justice as Little John
* Martita Hunt as Queen Eleanor of Aquitaine Prince John
* Elton Hayes as Alan-a-Dale
* Anthony Eustrel as the Archbishop of Canterbury King Richard I
* Anthony Forwood as Will Scarlet Bill Owen as Will Stutely
* Louise Hampton as Tyb, aged nurse of Maid Marian
* Richard Graydon as Merrie Man

==Production== Treasure Island (1950).  These and several other Disney films were made using British funds frozen during World War II. The Story of Robin Hood and His Merrie Men was filmed in 3-strip Technicolor.

==Release==
The world premiere was in London on March 13, 1952; the New York opening was on June 26, 1952. In a wake of this a promotional film entitled The Riddle of Robin Hood was produced. 

The film was one of the most popular in Britain in 1952. 

==Home releases==
A Laserdisc was released in 1992, a VHS tape was released in 1994 (the Walt Disneys Studio Film Collection) and a limited Disney Movie Club DVD was released in July 2006.  All releases are 1.33:1 fullscreen in monaural (as shot).

A Disneyland Records LP of four songs from the soundtrack with narration by Dallas McKennon was released in 1963.

==References==
 

==External links==
*   
*  
*   at Turner Classic Movies
*  
*   at Ultimate Disney

 
 

 
 
 
 
 
 


 