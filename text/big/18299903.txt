Rock On!!
 
 
 

{{Infobox film
| name = Rock on!!
| image = Rock on poster.jpg
| image_size =
| caption = Theatrical release poster
| director = Abhishek Kapoor
| producer = Farhan Akhtar Ritesh Sidhwani
| writer  = Abhishek Kapoor 
| starring = Arjun Rampal Shahana Goswami Farhan Akhtar Prachi Desai Purab Kohli Koel Purie Luke Kenny Nicolette Bird
| music = Shankar-Ehsaan-Loy
| cinematography = Jason West
| editing = Deepa Bhatia Excel Entertainment
| distributor = Reliance Big Pictures
| released = 29 August 2008
| runtime = 145 minutes
| country = India
| language = Hindi
| budget = 
| gross =   380&nbsp;million 
| preceded_by =
| followed_by =
}} rock musical drama film written and directed by Abhishek Kapoor, produced by Farhan Akhtar, and with music by Shankar-Ehsaan-Loy. The film marks the successful Bollywood debut of Farhan Akhtar and Prachi Desai.
 Rolling Stone (India).  Its screenplay has also been added to the Academy Film Archive. 

==Plot== rock rock band Magik, in 1998. Joseph (Joe) Mascarhenas (Arjun Rampal) is the lead guitarist who feels necessity to prove his worth as a musician. Aditya Shroff (Farhan Akhtar) is the lead singer who rebelled against his well to-do family to play music. Rob Nancy (Luke Kenny) who plays keyboards and Kedar Zaveri a.k.a. KD/"Killer Drummer" (Purab Kohli) comprise the remainder of the band. After a competition is announced by Channel V, they decide to enter, as the winner will be offered an album and one music video.

After they win the competition, the band has to go through some sacrifices while signing the contract. Joe feels the most slighted when his song, which he wrote for his girlfriend, Debbie (Shahana Goswami), and which was the only slow track in the entire album, is excluded from the track list to make space for a random remix song. Debbie is also rejected as the bands stylist without even being called for any demonstration. Later, when the cameramen focus only on Aditya during the filming of the music video, Joe becomes furious at Aditya before leaving with Debbie. Aditya, who failed to notice that anything was amiss, is shocked and abandons music and his girlfriend Tania (Nicolette Bird). "Magik" thus disbands and its members become estranged.

Ten years later, Aditya is a high-powered executive of an investment banking firm. His wife Sakshi (Prachi Desai), hoping to relieve his habitual resentment, decides to gift him a gold-chain wrist watch for his birthday. K.D., who now works for his fathers jewellery business, overhears Sakshi talking about Aditya at the jewellery shop, and introduces himself. Sakshi later conveys the meeting to Aditya who denies knowing K.D. When Sakshi finds a box filled with Magiks photos, she invites K.D. to the birthday party, telling him she wants to reunite Aditya with the band.

K.D. meets with Rob, who now makes a living composing jingles for advertisements. Together, they invite Joe, who lives largely unemployed, with an eight-year-old son. The family is supported by Debbie, whom he has married and who holds a grudge against the band. Joe feels he should reconcile with his old friend but Debbie sees this as his weakness. K.D. and Rob thus attend the party without Joe, and Aditya is shocked to see them. He later scolds Sakshi for trying to dig up his past, which he claims to have left behind. Sakshi is hurt, and she leaves him after informing him about her pregnancy. Sakshis friend Devika (Koel Purie) persuades Aditya to face his past and meet with his ex-bandmates. K.D., Joe and Rob meet up and visit the place where they used to practice as a band. Aditya also turns up and reconciles with Joe and the rest. They start practicing regularly at Adityas house. Learning about this, Sakshi, too, returns to Aditya.

Channel V announces another contest, and at Robs insistence, the band enters. Debbie arranges a guitar-playing job for Joe on a cruise-line, expected to set sail on the same day as the contest. It is later discovered that Rob is dying of a brain tumour and his last wish is to perform with Magik. The contest is aired on the radio, and while Joe is on his way to the airport, he hears Magik perform the song he wrote for Debbie 10 years ago, and dedicate it to him. This prompts him to join the concert, where he and Aditya sing duet in another song. The epilogue reveals that Rob died two months after that performance. Sakshi gives birth to a baby boy, whom they name "Rob" as a memorial to their friend. Devika is dating K.D., who starts a record company with Joe. Debbie quits her job in the fishing business and becomes a successful stylist. The band members and their families meet every weekend.

==Cast==
* Arjun Rampal as Joseph Mascarenhas (Joe)
* Farhan Akhtar as Aditya Shroff
* Purab Kohli as Kedar Zaveri/KD
* Luke Kenny as Rob Nancy
* Prachi Desai as Sakshi Shroff
* Shahana Goswami as Debbie Mascarenhas
* Koel Puri as Devika
* Dalip Tahil as Bajaj
* Reema Kagti
* Suraj Jagan
* Monica Dogra as herself (Shaair)
* Nicolette Bird as Tanya
* Anu Malik as himself
* Jameel Khan
* Jonathan Horovitz as Travel Writer

==Response==

===Box office===
Despite critical acclaim, the film only grossed   400&nbsp;million domestically; Box Office India labeled it as an "average grosser".    Overseas it was moderately well received.

===Critical reception===
Rock On was well received by a number of critics.   of the   called Rock On "refreshing" and "surprisingly quiet and thoughtful." 

Other critics gave the film mixed reviews. Rajeev Masand of CNN-IBN gave the film three out of five stars and states: "With Rock On, director Abhishek Kapoor promises a true-blue band film, but ultimately delivers a masala Hindi film that just happens to be about a band   Yet, Rock On is rescued by some marvelous moments that stay with you until the end." 

==Awards==
{| class="wikitable" style="width:99%;"   
|+ Awards and nominations 
|-
! Distributor
! Category
! Recipient
! Result
|- National Film Awards  Best Feature Film in Hindi 
|  Abhishek Kapoor Farhan Akhtar Ritesh Sidhwani 
| rowspan="9"     Hindustan Times. 16 March 2010.  
|- Best Supporting Actor 
|  Arjun Rampal 
|- 54th Filmfare Awards  Best Supporting Actor 
|  Arjun Rampal 
|- Best Male Debut 
|  Farhan Akhtar 
|- Best Actress (Critics Choice) 
|  Shahana Goswami 
|- Best Story 
|  Abhishek Kapoor 
|- Best Cinematogpher 
|  Jason West 
|- Best Sound Design 
|  Vinod Subramaniyam 
|- Special Performance Award 
|  Purab Kohli 
|- Best Film 
|  Farhan Akhtar Ritesh Sidwani 
| rowspan="10"  
|- Best Director 
|  Abhishek Kapoor 
|- Best Supporting Actress 
|  Shahana Goswami 
|- Best Music Director 
|  Shankar-Ehsaan-Loy 
|- Best Male Playback Singer 
|  Farhan Akhtar for Socha Hai 
|- Best Lyricist 
|  Javed Akhtar for Socha Hai 
|- Best Female Debut 
|  Prachi Desai 
|- Best Screenplay 
|  Abhishek Kapoor Pubali Chaudhary 
|- Best Scene of the Year 
|  Rock On! 
|- 15th Star Screen Awards  Most Promising Newcomer – Female 
|  Prachi Desai 
|- Best Supporting Actor 
|  Arjun Rampal 
| rowspan="9"  
|- Best Supporting Actress 
|  Shahana Goswami 
|- Most Promising Newcomer - Male 
|  Farhan Akhtar 
|- Best Editing 
|  Deepa Bhatia 
|-
|  Best Cinematography 
|  Jason West 
|-
|  Best Art Direction 
|  Shashank Tere  
|- 10th International Indian Film Academy Awards  Best Supporting Actor 
|  Arjun Rampal 
|- Star Debut of the Year - Male 
|  Farhan Akhtar 
|- Best Cinematography 
|  Jason West 
|- Best Picture  Excel Entertainment Reliance Big Pictures 
| rowspan="17"  
|- Best Director 
|  Abhishek Kapoor 
|- Best Supporting Actress 
|  Shahana Goswami 
|- Best Music Director 
|  Shankar-Ehsaan-Loy 
|- Best Lyrics 
|  Javed Akhtar for Socha Hai 
|- Best Male Playback Singer 
|  Farhan Akhtar for Socha Hai 
|- 4th Apsara Awards  Best Film 
|  Ritesh Sidhwani Farhan Akhtar 
|- Best Director 
|  Abhishek Kapoor 
|- Best Actor in a Supporting Role 
|  Purab Kohli 
|- Best Actress in a Supporting Role 
|  Shahana Goswami 
|-
|  Best Story 
|  Abhishek Kapoor 
|-
|  Best Dialogue 
|  Farhan Akhtar 
|- Best Music Director 
|  Shankar-Ehsaan-Loy 
|- Best Female Playback Singer 
|  Dominique Cerejo for Ye Tumhari Meri Baatien 
|- Best Music Director 
|  Shankar-Ehsaan-Loy 
|-
|  Best Re-Recording 
|  Anup Dev 
|-
|  Best Editing 
|  Deepa Bhatia 
|-
|  Best Sound Recording 
|  Baylon Fonseca 
| rowspan="3"  
|- 11th Anandalok Awards 
|  Best Actress (Hindi) 
|  Prachi Desai 
|-
|  Sabse Favourite Kaun 
|  Favourite Nayi Heroine 
|  Prachi Desai 
|}

==Soundtrack==
 

==Sequel==
After the success of Rock On, a sequel was announced but has not yet begun production.   As of early 2010, director Abhishek Kapoor had no concrete plans for the sequel. However, he has confirmed that he plans to make a sequel to Rock On. Arjun Rampal and Farhan Akhtar are returning for the sequel since they have been keen on starting the sequel since 2008. The film is yet to begin production. As of February 2013, the writers confirm Prachi Desai and Shahana Goswami to be the part of sequel too. Alia Bhatt would also be introduced. The film will be based on male-bonding. The music will be composed by Shankar-Ehsaan-Loy, with lyrics by Javed Akhtar.Shraddha Kapoor will be part of the sequel, She will be new face in the movie.

==References==
 

==External links==
*  
*  - Excel Entertainment
*  - Excel Entertainment

 
 
 

 
 
 
 
 
 
 
 