Charley's Aunt (1934 film)
{{Infobox film
| name = Charleys Aunt 
| image = 
| image_size =
| caption =
| director = Robert A. Stemmle
| producer = 
| writer = Brandon Thomas (play)   Robert A. Stemmle
| narrator =
| starring = Fritz Rasp   Paul Kemp   Max Gülstorff
| music = Harald Böhmelt 
| cinematography = Carl Drews   
| editing = 
| studio =  Minverva Film
| distributor = 
| released =  17 August 1934 
| runtime = 91 minutes
| country = Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Charleys Aunt (German:Charleys Tante) is a 1934 German comedy film directed by Robert A. Stemmle and starring  Fritz Rasp, Paul Kemp and Max Gülstorff.  It is based on the British writer Brandon Thomass 1892 play Charleys Aunt. The films sets were designed by art director Franz Schroedter.

==Cast==
* Fritz Rasp as Lord Babberley   Paul Kemp as Fancourt Babbs Babberley  
* Max Gülstorff as Sir Francis Chesney  
* Albert Lieven as Jack Chesney 
* Erik Ode as Charley Wykeham  
* Paul Henckels as Stephan Spettik  
* Jessie Vihrog as Amy  
* Carola Höhn as Kitty  
* Ida Wüst as Donna Lucia dAlvadorez  
* Vilma Bekendorf as Ela Delahey 
* Fita Benkhoff as Mary Fin 
* Fritz Odemar as Bressett  
* Ernst Legal as Nelson  
* Ernst Nessler as Aristophan  
* Charlotte Berlow as Miss Bedford  
* Henry Pauly as Smith 
* Rudolf Platte as Shipmaker

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 

 