Man Ki Aankhen
{{Infobox film
| name           = Man Ki Aankhen
| image          = Man Ki Aankhen.jpg
| caption        =
| director       = Raghunath Jhalani
| producer       = I. A. Nadiadwala
| writer         = Bhakri (Dialogue), Jwalamukhi (Screenplay)
| starring       = Dharmendra  Waheeda Rehman
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        =
| distributor    = 
| released       = 1970
| runtime        = approx 180 min 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }} 1970 Bollywood film directed Raghunath Jhalani. 

==Plot==
Rajesh Agarwal marries with Geeta, daughter of his school teacher Master Dinanath. Rajesh marries with Geeta without telling his mother as he was afraid that his dowry seeking mother would oppose the marriage. Rajesh comes back home with Geeta in hope that his mothers anger will subside sooner or later.

==Cast==
*Dharmendra - as Rajesh Agarwal.
*Waheeda Rehman - as Guddi.
*Sujit Kumar - as Naresh Agarwal.
*Faryal - as Vandana N. Agarwal.
*Manmohan Krishna - as Master Dinanath.
*Leela Chitnis - as Mrs Dinanath.
*Mohan Sherry - as Mr Keshav.
*Brahm Bhardwaj - as Raj Bahadur.
*Lalita Pawar - as Mother of Rajesh Agarwal.

==Songs==
The music of the movie was composed by Laxmikant Pyarelal. The lyrics were penned by Sahir Ludhianvi.
* Dil Kahe Ruk Ja - Mohammad Rafi
* Chala Bhi Aa Aaja Rasiya - Lata Mangeshkar, Mohammad Rafi  

==References==
 

== External links ==
*  

 
 
 
 


 