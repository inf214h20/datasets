The Department (film)
{{Infobox film
| name               = The Department
| image              = The Department movie poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Remi Vaughn-Richards
| producer           = Uduak Oguamanam   Chinaza Onuzo
| writer         = Chinaza Onuzo
| starring       =  
| music          = 
| cinematography = Ayoola Ireyomi
| editing        = Niyi Akinmolayan
| studio         = Inkblot Productions   Closer Pictures
| distributor   = FilmOne Distributions
| released       =  
| runtime        = 104 minutes
| country        = Nigeria
| language       = English
| budget         = 
| gross          =
}}
 romantic Crime crime action film, starring Majid Michel, OC Ukeje, Desmond Elliot, Osas Ighodaro, Jide Kosoko, Seun Akindele, Somkele Iyamah and Funky Mallam.   The film which is the first feature film from Inkblot Productions and Closer Pictures,     is directed by Remi Vaughn-Richards, produced by Uduak Oguamanam and Chinaza Onuzo. 

The film tells the story of a secret department in a business conglomerate, as its gang members blackmail top executives into selling their companies to the leader of the conglomerate (Jide Kosoko). Two lovers (Majid Michel and Osas Ighodaro) however opt out of the organization, but the group wants her back for one last job. She accepts against her husbands will, who consequently decides to sabotage the department in order to save their marriage.

==Cast==
*Osas Ighodaro as Tolu Okoye
*Majid Michel as Nnamdi
*Desmond Elliot as Effiong
*OC Ukeje as Segun
*Jide Kosoko as Chief
*Udoka Oyeka as James Okolo
*Kenneth Okolie as
*Somkele Iyamah as
*Seun Akindele as
*Saheed Funky Mallam as

==Release==
A theatrical trailer for The Department was released on 29 December 2014.   The film had a press screening on 20 January 2015 at Filmhouse Cinemas in Surulere, Lagos; it was reportedly well received by the critics at the event.     It premiered on 25 January 2015,  and was theatrically released on 30 January 2015.  

==Reception==
Folasewa Olatunde praised the performances in the film, but talked down on some unrealistic parts of the film. She concludes that the film is "intriguing and suspense filled". 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 