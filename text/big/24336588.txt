Rope Cosmetology
{{Infobox film
| name = Rope Cosmetology
| image = Rope Cosmetology.jpg
| image_size = 
| caption = Theatrical poster for Rope Cosmetology (1978)
| director = Shōgorō Nishimura 
| producer = Yoshiki Yūki
| writer = Oniroku Dan (original story) Akio Ido (screenplay)
| narrator = 
| starring = Naomi Tani Aoi Nakajima Katsu Yamada
| music = 
| cinematography = Yonezō Maeda
| editing = Toyoharu Nishimura
| distributor = Nikkatsu
| released = December 2, 1978
| runtime = 67 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1978 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Shōgorō Nishimura and starring Naomi Tani.

==Synopsis==
Kanako is a married woman who receives an invitation from Tomoe, a lesbian lover from her past, to attend a showing of artwork by Isaku, Tomoes husband. Isakus paintings have an S&M theme which surprises Kanako. Tomoe persuades Kanako to begin going with her to a class in which she is learning how to behave like a proper female dog. Kanakos husband, Eiichirō, is suspicious about her activities. After hiring a private detective and learning about her perverse hobbies, Eiichirō throws Kanako into a landfill. The film ends with Eiichirō apparently taking his dog for a walk. The camera pans down to show that, despite his apparent objections to her lessons, Eiichirō is in fact walking Kanako, who is on all fours on a dog leash. 

==Cast==
* Naomi Tani: Kanako Ishino 
* Aoi Nakajima: Tomoe Takita
* Katsu Yamada: Eiichirō Ishino
* Akira Takahashi: Isaku Takita
* Tayori Hinatsu: Takitas assistant

==Background and critical appraisal==
Lead actress Naomi Tani and director Shōgorō Nishimura had worked together previously in the successful Lady Black Rose (1978).  Jasper Sharp uses Rope Cosmetology to help explain Tanis popularity in the pink film genre. Despite her image as a proud, matronly, high-class woman, she was willing, if not eager, to submit to extremes of degradation to make her films memorable. Sharp notes that the highlight of the film, in this respect, is when Tani is covered in butter and humped by a German Shepherd Dog.  After delving into the extreme and perverse side of Roman Porno with Rope Cosmetology, director Nishimura returned to his usual, more standard fare with his next film, Forbidden Ordeal (1979). 

Allmovie writes of the film, "Well-directed and convincingly played, the film manages to walk the fine line between the nauseating misogyny of Japans appalling turtle girl films and the outright silliness of American and British pony girl efforts, presenting a reasonable introduction to the oft-misunderstood world of erotic discipline." 

==Availability==
Rope Cosmetology was released theatrically in Japan on December 2, 1978.  It was released to home video in VHS format on April 27, 1990, and re-released on September 10, 1993.   It was released on DVD in Japan on March 21, 2007, as part of Geneon Universal Entertainment|Geneons seventh wave of Nikkatsu Roman porno series. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 