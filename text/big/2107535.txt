Present Indicative
{{Infobox film
| name           = Present Indicative
| image          = 
| caption        = 
| director       = Péter Bacsó
| producer       = 
| writer         = Péter Zimre Péter Bacsó
| starring       = András Kovács
| music          = 
| cinematography = János Zsombolyai
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}
 Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

==Cast==
* József Borsóhalmi as Somogyi bácsi
* Irén Bódis as Mózesné, Irén
* Ágnes Dávid as Zsófika
* Gabriella Koszta as Ica
* András Kovács as Kalocsa
* Tibor Liska as Kulcsár
* Lehel Ohidy as Takács, párttitkár (as Óhidy Lehel)
* Klára Pápai as Mózes anyja
* Ádám Rajhona as Kárász
* Ágoston Simon as Mózes Imre (as Simon Ágoston)
* László Sugár as Nagy Ferenc Szabó as Gyula bácsi
* Lajos Szabó as Görbe

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 