The Long Voyage Home
{{Infobox film
| name           = The Long Voyage Home
| image          = Original movie poster for the film The Long Voyage Home.jpg
| caption        = Theatrical release poster
| director       = John Ford
| producer       = Walter Wanger
| screenplay     = Dudley Nichols
| based on       =  
| starring       = {{plainlist|
* John Wayne Thomas Mitchell Ian Hunter
}}
| music          = Richard Hageman
| cinematography = Gregg Toland
| editing        = Sherman Todd
| studio         = Argosy Pictures
| distributor    = United Artists
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $682,495 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p440 
| gross          = $580,129 
}} Thomas Mitchell, Ian Hunter, Wilfrid Lawson, John Qualen, Mildred Natwick, and Ward Bond, among others.

The film was adapted by Dudley Nichols from the plays The Moon of the Caribbees, In the Zone, Bound East for Cardiff, and The Long Voyage Home by Eugene ONeill. The original plays by Eugene ONeill were written around the time of World War I and were among his earliest plays.  Ford set the story for the motion picture, however, during World War II. 

The picture tells the story of the crew aboard a freighter during the early days of World War II.

==Plot== British cargo ship named the SS Glencairn, during World War II, on the long voyage home from the West Indies to Baltimore and then to England. The ship carries a cargo of high-explosives.

On liberty, after a night of drinking in bars in the West Indies, the crew returns to the tramp steamer and set sail for Baltimore.
 Irishman Driscoll Thomas Mitchell), Swedish ex-farmer Ian Hunter), and others.

After the ship picks up a load of dynamite in Baltimore, the rough seas they encounter become nerve-racking to the crew.  When the anchor breaks loose, Yank (Ward Bond) is injured in the effort to secure it.  With no doctor on board, nothing can be done for his injury, and he dies.
 German spy alcoholic who has run away from his family. When they near port, a German plane attacks the ship, killing Smitty in a burst of machine gun fire. The rest of the crew members decide not to sign on for another voyage on the Glencairn and go ashore, determined to help Ole return to his family in Sweden, whom he has not seen in ten years. 
 shanghaied aboard another ship, the Amindra. Driscoll and the rest of the crew rescue him from the ship, but Driscoll is accidentally left behind in the confusion. As the crew straggles back to the Glencairn the next morning to sign on for another voyage, they learn that the Amindra was sunk by German torpedoes, killing all on board.

==Cast==
* John Wayne as Ole Olsen Thomas Mitchell as Aloysius "Drisk" Driscoll Ian Hunter as Smitty Smith, an alias of Thomas Fenwick
* Barry Fitzgerald as Cocky Wilfrid Lawson as Captain
* John Qualen as Axel Swanson
* Mildred Natwick as Freda
* Ward Bond as Yank
* Arthur Shields as Donkeyman
* Joe Sawyer as Davis
* J.M. Kerrigan as Nick, Limehouse Crimp
* Rafaela Ottiano as Bella, a Tropical Woman
* Carmen Morales as Principal Spanish Girl
* Jack Pennick as Johnny Bergman
* Bob Perry as Paddy

==Production==
Independent film producer  , Grant Wood, George Biddle, James Chapin, Ernest Fiene, Robert Philipp, Luis Quintanilla, Raphael Soyer and Georges Schreiber. Eleven original paintings emerged from this inaugural effort.  These toured the country in the museum circuit of the day beginning with a display in the Associated American Artists Galleries on Fifth Avenue, New York.  

==Release==
The film did poorly in its theatrical release, losing $224,336. 

It was released on DVD in 2006 by Warner Bros. Home Video but is now out-of-print. The Criterion Collection now has the home video rights to release it. It has not yet been released on DVD or Blu Ray by The Criterion Collection.

==Reception==
Critic Bosley Crowther, film critic for The New York Times, liked the screenplay, the message of the film, and John Fords direction, and wrote, "John Ford has truly fashioned a modern Odyssey—a stark and tough-fibered motion picture which tells with lean economy the never-ending story of mans wanderings over the waters of the world in search of peace for his soul...it is harsh and relentless and only briefly compassionate in its revelation of mans pathetic shortcomings. But it is one of the most honest pictures ever placed upon the screen; it gives a penetrating glimpse into the hearts of little men and, because it shows that out of human weakness there proceeds some nobility, it is far more gratifying than the fanciest hero-worshiping fare." 

The staff at Variety (magazine)|Variety magazine wrote, "Combining dramatic content of four Eugene ONeill one-act plays, John Ford pilots adventures of a tramp steamer from the West Indies to an American port, and then across the Atlantic with cargo of high explosives. Picture is typically Fordian, his direction accentuating characterizations and adventures of the voyage." 

The review aggregator Rotten Tomatoes reports that 100 percent of critics gave the film a positive review based on eight reviews. 

===Awards===
====Wins====
* New York Film Critics Circle Awards: NYFCC Award; Best Director, John Ford; 1940.

====Nominations====
* Academy Awards:   
** Best Black-and-White Cinematography, Gregg Toland
** Best Special Effects, R. T. Layton (photographic), Ray Binger (photographic) and Thomas T. Moulton (sound)
** Best Film Editing, Sherman Todd
** Best Original Score, Richard Hageman
** Best Picture, John Ford
** Best Screenplay Writing, Dudley Nichols

==References==
 

==External links==
*  
*  
*  
*  
*   essay by Chris Fujiwara at FIPRESCI
*   by Ned Scott

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 