At the Beginning of Glorious Days
 
{{Infobox film
|name=At the Beginning of Glorious Days
|image= 
|image_size= 
|alt= 
|caption=  Sergey Gerasimov
|producer= 
|screenplay=Sergey Gerasimov Yuri Kavtaradze
|starring=Dmitri Zolotukhin Tamara Makarova Natalya Bondarchuk Nikiolai Yeryemenko Mikhail Nozhkin Boris Khmelnitsky Lyubov Germanova Ivan Lapikov Lyubov Polekhina Marina Levtova 
|music=Vladimir Martynov
|cinematography=Sergey Filippov, Horst Hardt
|editing= 
|studio=Gorky Film Studio
|distributor=Russian Cinema Council
|released= 
|runtime=2:14 minutes
|country=Soviet Union
|language=Russian
|budget=}} Peter I, Aleksey Tolstoy. Sergey Gerasimov.   The movie is considered to be a classic of Russian historical cinema.

==Synopsis==
In late 17th-century Russia, Czar Peter the Great orders an attack on Turkey, which refuses to pay taxes to the Russian government. The Russian military is equipped with outdated technology, and they suffer their first defeat. After the defeat the czar orders the building of a fleet, and sends many educated men to study in Germany, France, and Holland. Russian victory over Turkey will not only force the Turks to pay taxes, but also make the Sea of Azov accessible to the Russians.

==Cast==
*Dmitri Zolotukhin — Peter the Great
*Tamara Makarova — Natalya Naryshkina Czarevna Sophia
*Nikiolai Yeryemenko — Aleksandr Danilovich Menshikov
*Mikhail Nozhkin — Knyaz Boris Alexeyevich Galitzine
*Boris Khmelnitsky — Kuzma Chermnyi
*Lyubov Germanova — Eudoxia Lopukhina
*Ivan Lapikov — Zhemov
*Lyubov Polekhina — Sanka Brovkina
*Marina Levtova — Olga Bulnosova
*Yekaterina Vasillyeva — Antonida Bulnosova
*Pyotr Glebov — Gypsy
*Marina Golub — Verka
*Muza Krepkogorskaya — Sparrow caregiver Yuri Moroz — Alyosha Brovkin
*Peter Reusse — Franz Lefort
*Eduard Bocharov — Merchant Ivan Brovkin
*Anatoli Barantsev — Nikita Zotov
*Roman Filippov — Fyodor Romodanovsky
*Vladimir Kashpur — Ovdokim
*Aleksandr Borisovich Belyavskiy — Lev Naryshkin
*Nikolai Grinko — Starets Nektari

==References==
 

==External links==
*  Russian Cinema Council catalogue
 