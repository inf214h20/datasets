The Patriot (1998 film)
{{Infobox film
| name = The Patriot
| image = The Patriot.jpg
| caption = Region 1 DVD cover
| director = Dean Semler
| writer = M. Sussman John Kingswell
| based on = The Last Canadian by William C. Heine
| starring = Steven Seagal Gailard Sartain L.Q. Jones Camilla Belle
| producer = Howard Baldwin Patrick Choi Nile Niami Howard L. Baldwin Steven Seagal Julius R. Nasso
| music = Stephen Edwards
| cinematography = Stephen F. Windon
| editing = Derek Brechin
| distributor = Buena Vista Home Entertainment  (USA) 
| budget = $25,000,000 
| released =  
| runtime = 90 minutes
| country = United States
| language = English
}}

This article is about the Steven Seagal film.  For the Roland Emmerich film about the American Revolutionary War, starring Mel Gibson, see The Patriot (2000 film)
 1998 action American patriot who has to foil the plot of a militia to release a deadly virus. It is Seagals first direct-to-video film.

==Synopsis== herbal medicine and is also a weapons and self-defense expert, is called to a hospital when people start dying from an unknown but very deadly disease. He determines that the cause is a highly dangerous airborne virus and calls in a Biological Response team, who seal off the town while doctors start treating sufferers with a vaccine. Several have already died.
 rebel Militia militia leader, Floyd Chisholm (Gailard Sartain), who has given himself up after a long siege and has been arrested on weapons charges. In court, having ingested the virus himself (believing that he also possesses the vaccine) he spits at the judge, and starts the rapid spread of the disease.

Floyds militia followers, who have been allowed to go free, attack and capture the hospital, including Wesley and his daughter Holly (Camilla Belle), with much loss of life, and rescue him. But too late, they realize that the vaccine they possess does not actually work.

Working at gunpoint, Wesley takes a sample of Hollys blood; it shows that Holly has been infected, but somehow her body is fighting it off. Wesley and Holly contrive to escape and travel to a farm where Hollys grandfather lives. Wesley takes a blood sample from his friend Dr. Ann White Cloud (Whitney Yellow Robe), and realizes that her body is also fighting off the infection.

Wesley and Ann gain access to a secret underground laboratory where Wesley used to work, where they hope to come up with a cure. Wesley finds out why Ann and Holly are not being affected by the virus: they have been drinking tea made with a specific wild herb that is known to Native American healers.

Back at the hospital, Wesley and Holly are captured by the militia, but he manages to kill Floyd and disable the other soldiers. As soon as the biological protection team learn of the cure, they go out and pick all the flowers they can find and drop them by helicopter over the town, telling the people to boil them and drink the liquid.

==Cast==
* Steven Seagal  ... Dr. Wesley McClaren
* Gailard Sartain  ... Floyd Chisholm
* L.Q. Jones  ... Frank Silas Weir Mitchell  ... Pogue
* Camilla Belle  ... Holly McClaren
* Dan Beene  ... Richard Bach
* Damon Collazo  ... Lt. Johnson
* Whitney Yellow Robe  ... Dr. Ann White Cloud
* Brad Leland  ... Big Bob
* Molly McClure  ... Molly
* Philip Winchester  ... Young Miltiaman
* Douglas Sebern  ... Judge Tomkins
* Ross Loney  ... Clem
* Bernard OConnor  ... Dr. Tom Hergot
* Leonard Mountain Chief  ... Grandpa
* R.J. Burns  ... Navy Captain
* Robert Harvey  ... Col. Harvey
* Ron Andrews  ... Radioman
* Jeff Tillotson  ... Pvt. Benson
* Don Peterson  ... T.S. Soldier #1
* Cory Brown  ... T.S. Soldier #2
* Tom Vanek  ... Roadblock Sentry
* Scott Wetsel  ... FBI Agent
* Dillinger Steele  ... Marshal
* Gene E. Carlstrom  ... Old Rancher
* Ayako Fujitani  ... McClarens Assistant (as Ayako Seagal)
* Kelcie Beene  ... Hollys Friend #1
* Callie Strozzi  ... Hollys Friend #2
* Stephen Jensen ... ATF Agent #3

==Production== Ennis and Virginia City, Montana, and for three days on the campus of Montana State University. Filming was briefly halted to remove snow from the ground during shooting in Virginia City, to maintain continuity.  
 William C. Heines novel The Last Canadian, it shares virtually no similarities with the novel except the idea of a deadly virus. No character names, events, or even locations appear in both the book and the film.  

The film is Seagals only effort to date in which he co-stars with his daughter, actress Ayako Fujitani, and is notable for an extremely low amount of action scenes compared to Seagals other work.

==Reception==

The film has received generally negative reviews, with critics frequently singling out the extremely low number of action sequences. David Nusair of Reel Film Reviews called it "dull" and criticized its lack of action (he claims, "Out of a 90 minute movie, theres maybe 10 minutes of actual Seagal hand-to-hand combat") and unwelcome political messages.  Seagalogy author Vern argues that "in many ways The Patriot is an admirable effort," noting "The production values and acting are better than some of Seagals subsequent movies," but going on to point out that due to its "ridiculously low action quotient it is a least-favorite of many Seagal fans."  As of 2 October 2014, the film has a 22% "rotten" critics rating on Rotten Tomatoes (based on 9 reviews).   

==References==
 

==External links==
*  

 
 
 
 
 
 
 