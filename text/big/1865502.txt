Wrongfully Accused
{{Infobox film
| name           = Wrongfully Accused
| image          = Wrongfully Accused.jpg
| caption        = Theatrical release poster
| director       = Pat Proft
| producer       = Pat Proft James G. Robinson Bernd Eichinger
| writer         = Pat Proft Kelly Le Michael York
| music          = Bill Conti
| cinematography = Glen MacPherson
| studio         = Morgan Creek Productions Constantin Film
| editing        = James R. Symons Warner Bros. Pictures   Pathé  
| released       =  
| runtime        = 87 minutes
| country        = Germany United States
| language       = English
| budget         = 
| gross          = $9,623,329
}} parody of The Fugitive.

==Plot== Michael York), Kelly Le Brock) and his possible mistress Cass Lake (Melinda McGraw).

  in Carbonite" prop from The Empire Strikes Back and Return of the Jedi]]
The next evening he finds a note from Lauren in his car which summons him to the Goodhue residence. When he goes to the Goodhue mansion, he bumps into Sean Laughrea (Aaron Pearl), who has just killed Goodhue (together with an unknown accomplice). A violent fight follows, during which Harrison discovers that Sean is missing an eye, an arm, and a leg, and he overhears the preparations for an operation with the codename "Highlander" before he is knocked out. When he wakes up, Harrison finds himself arrested and convicted for the murder of Goodhue. Desperate to prove his innocence, Harrison escapes from his prison transport following an accident. Lieutenant Fergus Falls (Richard Crenna) arrives on the scene, takes charge, barks out orders and vows to do whatever it takes to capture the fugitive.

Harrison returns to the Goodhue mansion where he encounters Cass, who is trying to retrieve something from behind a portrait. She tells him she knows he is innocent and believes Lauren is the killer, but refuses to say anything to the police because Lauren is her sister. She provides him with a place to hide and helps him shake his pursuers, but Harrisons opportunities to rest are short and fleeting: Falls seems to find him wherever he goes, and Cass behaves suspiciously, increasing Harrisons doubts of whom to trust.

Harrison gradually begins to piece together the puzzle; he remembers that Sean was present at the party as the bartender and was given a great amount of money by Cass. He also finds that Cass is strangely interested in Sir Robert McKintyre, the secretary-general of the United Nations. Eventually, after investigating Seans disabilities in a limb replacement clinic, he discovers that Cass, Lauren and Sean are planning an assassination attempt on McKintyre. He manages to follow the group but is caught. Cass shoots Harrison but actually fakes his death, both because she has fallen in love with him and because she wants to stop the assassination, since she has found out that McKintyre is really her father. Goodhue has been murdered by Sean and Lauren because he had come to suspect that his wife was actually a terrorist and had only used him to further her goals.

At a Scottish festival, Harrison and Cass just barely manage to save McKintyres life. They are cornered by Lauren, Sean and accomplices, but Fergus Falls and a SWAT team arrive just in the nick of time, arresting the terrorists. Falls officially tells Harrison that he was "wrongfully accused", clearing his name and acquitting him. In the last scene, Harrison and Cass are riding on the bow of a cruise ship (spoofing Titanic (1997 film)|Titanic) and end up bumping their heads on a low bridge.

==Cast==
* Leslie Nielsen as Ryan Harrison
* Richard Crenna as Lieutenant Fergus Falls Kelly Le Brock as Lauren Goodhue
* Melinda McGraw as Cass Lake Michael York as Hibbing Goodhue
* Sandra Bernhard as Dr. Fridley
* Aaron Pearl as Sean Laughrea Leslie Jones as Sergeant Tina Bailey Ben Ratner as Sergeant Orono
* Gerard Plunkett as Sir Robert McKintyre Duncan Fraser as Sergeant MacDonald

==Reception==

===Box office===
The film opened on August 21, 1998 in 2,062 cinemas. On its opening weekend, it grossed USD $3,504,630 or approximately $1,700 per theatre. Wrongfully Accused s overall gross was $9,623,329. 

===Critical response===
Wrongfully Accused got generally negative reviews. It received a 22% "rotten" rating on the Rotten Tomatoes web site based on 32 reviews and a 3.9/10 rating. 

James Berardinelli of ReelViews gave the film only one out of four stars and wrote: "Wrongfully Accused is a mind-numbingly awful motion picture that has a better chance of making a viewer physically ill than of provoking a genuine laugh. Someone should sit down with Leslie Nielsen and suggest that, for his own good and the good of all those who go to his movies, he should retire. ... Nielsen may be a nice guy, but his increasingly feeble attempts at "comic" performances are becoming so painful to watch that Im starting to dislike the man. ... Wrongfully Accused has nothing worth laughing at. With as many lame one-liners, visual gags, and puns as this script lobs at the audience, it comes as an absolute shock how humorless the results are. ... Its incomprehensible to think that anyone would find this movie funny—except the quote whores, that is." 

Anita Gates of New York Times wrote: "No one can accuse Wrongfully Accused of a shortage of jokes. ... Unfortunately, most of the jokes just arent very funny. ... Maybe Pat Proft   works best as part of a team. Maybe he needs one of his old collaborators -- ideally David Zucker or Jim Abrahams -- to turn his concepts into punch lines. Without them, Wrongfully Accused   feels like a tangle of funny ideas all dressed up with nowhere to go. ... Throughout the film, Richard Crenna   seems right on the verge of making his Tommy Lee Jones imitation truly great, but he always just misses, maybe because he has only his attitude to work with. ... Even a cameo by the generally fabulous Sandra Bernhard falls flat. Things go better for Lamb Chop, the saccharine little sock puppet whose creator, Shari Lewis, died earlier this month. Lamb Chop has a cameo as an audience member in the opening concert scene and steals the show."  

Lisa Schwarzbaum of Entertainment Weekly wrote: "Opening so soon after Mafia! tried our patience for the whole Airplane! and Naked Gun parody thing, Wrongfully Accused has a lot going against it. Its sloppy, tired, obvious, and overdone. ... But strewn throughout this shameless, old-fart comedy circus are so many giddy, good-natured, much-needed stink bombs aimed at middlebrow and lowbrow pop culture that a few are bound to hit the mark: The worst excesses of Baywatch, JFK, Anaconda, Charlies Angels, Mission: Impossible, Braveheart, Titanic, and Field of Dreams are appropriately honored. So too are North by Northwest, Hong Kong action movies, and Mentos commercials. Some of the most pointed commentary is also the most throwaway."  

==Examples of parody==
* Many of the character names are based on names of actual places in Minnesota:
** Hibbing Goodhue — combination of Hibbing, Minnesota and Goodhue County, Minnesota lake and city in Cass County, Minnesota
** Lt. Fergus Falls — Fergus Falls, Minnesota
** Sgt. Orono — Orono, Minnesota
** Dr. Fridley — Fridley, Minnesota Twin Cities-area Columbia Heights, Excelsior Bay, Stillwater Prison, etc.
* A TV anchorwoman in the film is called Ruth Kimble. Kimble is the protagonists surname in The Fugitive.
* The police in the bus scene parodies a pre-flight safety demonstration. WCCO news is an actual TV station and news program in the Twin Cities, the logo used was WCCOs logo for quite some time. Lord of the Dance.
* When losing his bow during the concert, Ryan professionally takes up another from a sheath on his back after the fashion of famous rock drummers. Nick Mason does exactly that during the famous Live at Pompeii concert with Pink Floyd. Jack Ryan, Patriot Games Clear and Present Danger, and Harrison Fords own name. The Fugitive same as the tagline.
* In the  , Nielsens character Frank Drebin mentions how painful it is to land on a bicycle without a seat. In Wrongfully Accused, Nielsens character does just that after escaping from a bus/train collision.
* During the Cornfield Scene, Harrison is driving a car with hydraulics and the same steering wheel Cheech Marin uses in Up in Smoke.
* The "Machine Gun Jig" scene was a take off of the highly successful show Riverdance.
* The usher gesturing viewers of Ryans concert to their seats with a lightsaber is presumably poking fun at the Star Wars series of films.
* In one scene, he talks his way out of suspicion by referencing objects in view in a direct parody of The Usual Suspects.
* The entire film is based around a plot called Hylander; the Hylander is the mascot of the high school that Pat Proft graduated from, Columbia Heights High School.
* One scene presenting Ryan dressed in kilt parodies the William Wallace character from the film Braveheart. 1984 Apple Macintosh advertisement.

==Subjects parodied==
There are numerous films and media parodied in Wrongfully Accused, including:

  The Fugitive Patriot Games Clear and Present Danger
* On the Waterfront
* Star Wars
* The Godfather
* Casablanca
* Falling Down
* Ben-Hur (1959 film)|Ben-Hur
* Dirty Harry
* Superman (1978 film)|Superman
* Braveheart
* Baywatch
* North by Northwest
* Die Hard
*  
* Titanic (1997 film)|Titanic
* Charlies Angels
* Fatal Attraction
* The Seven Year Itch The Maltese Falcon
* Back to the Future
* Up in Smoke
*  
* Field of Dreams
* The Usual Suspects
* ER (TV series)|ER
* Anaconda (film)|Anaconda

 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 