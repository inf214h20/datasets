Here Comes Mr. Jordan
{{Infobox film
| name           = Here Comes Mr. Jordan
| image          = 72HERE20COMESMRJORDANP.jpg
| image_size     =
| director       = Alexander Hall
| producer       = Everett Riskin
| based on       =  
| screenplay     = Sidney Buchman Seton I. Miller Robert Montgomery Evelyn Keyes Claude Rains Rita Johnson Edward Everett Horton
| music          = Friedrich Hollaender Joseph Walker
| editing        = Viola Lawrence
| Studio    = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 94 min.
| country        = United States
| language       = English
}}
 Robert Montgomery, Claude Rains and Evelyn Keyes. Here Comes Mr. Jordan was adapted by Sidney Buchman and Seton I. Miller from the play Heaven Can Wait by Harry Segall and was directed by Alexander Hall.
 Down to Hollywood features, I Married Angel on Angels in the Outfield (1951), but it all began with Here Comes Mr. Jordan. Stafford, Jeff.   Turner Classic Movies. Retrieved: April 9, 2015. 

==Plot== Jordan (Claude take over a newly dead corpse. Mr. Jordan explains that a body is just something that is worn, like an overcoat; inside, Joe will still be himself. Joe insists that it be someone in good physical shape, because he wants to continue his boxing career. Joe keeps saying the body they find "has to be in the pink".
 John Emery). Joe is reluctant to take over a life so unlike his previous one, but when he sees the murderous pair mockingly berating Miss Logan (Evelyn Keyes), the daughter of a financier who was sold worthless bonds by Farnsworths bank, he changes his mind and agrees to take over Farnsworths body. 

As Farnsworth, Joe repays all the investors, including Miss Logans father. He sends for Corkle and convinces him that he is Joe (by playing his saxophone just as badly as he did in his previous incarnation). With Farnsworths money to smooth the way, Corkle trains him and arranges a bout with the current heavyweight champion, but Mr. Jordan returns to warn Joe that, while he is destined to be the champion, it cannot happen that way. Joe has just enough time to tell Miss Logan, with whom hes fallen in love, that if a stranger (especially if he is a boxer) approaches her, to give him a chance. Then he is shot by his secretary. The body is hidden, and Joe returns to a ghostly existence.

Accompanied by Mr. Jordan, Joe finds that his replacement in the prizefight with the champ is a clean-cut, honest fighter named Murdoch, whom Joe knows and respects. Finding that he has forgotten his lucky saxophone, Joe runs back to the Farnsworth mansion to find that everyone believes Farnsworth has "disappeared." Corkle has hired a private investigator to find him. Corkle explains about Joe, Mr. Jordan and the body-switching, but of course the police detective (Donald MacBride) thinks he is a nut. Joe manages to mentally nudge Corkle into turning on the radio to the fight and hears that Murdoch has collapsed without even being touched. Mr. Jordan reveals that the boxer was shot by gamblers because he refused to throw the fight. Joe takes over Murdochs body and wins the title. Back at the mansion, Corkle hears one of the radio announcers mention a saxophone hanging by the ringside and realizes Joe has assumed Murdochs body.

Corkle races down to the dressing room. There, Joe passes along information from Mr. Jordan that Farnsworths body is in a refrigerator in the basement of the mansion. Corkle tells the detective, who promptly has Mrs. Farnsworth and the secretary arrested. As Murdoch, Joe fires his old, crooked manager and hires Corkle. Mr. Jordan reveals to Joe that this is his destiny; he can be Murdoch and live his life.
 past life, Mr. Jordan hangs around for a bit longer until Miss Logan arrives. She wanted to see Corkle, but runs into Murdoch instead. The pair feel they have met before. The two go off together, while Mr. Jordan smiles and says "So long, champ."

==Cast==
  Robert Montgomery as Joe Pendleton
* Evelyn Keyes as Bette Logan
* Claude Rains as Mr. Jordan
* Rita Johnson as Julia Farnsworth
* Edward Everett Horton as Messenger 7013
* James Gleason as Max "Pop" Corkle John Emery as Tony Abbott
* Donald MacBride as Insp. Williams
* Don Costello as Lefty
* Halliwell Hobbes as Sisk
* Benny Rubin as "Bugsy" (the handler)
* Lloyd Bridges as Mr. Sloan, the co-pilot
* Eddie Bruce as Reporter
* Joe Conti as William Forrest John Ince as Bill Collector
* Selmar Jackson as Board Member
* Bert Young as Taxi Driver
* Warren Ashe as Charlie
* Ken Christy as Plainclothesman
* Chester Conklin as Newsboy
* Joseph Crehan as Doctor
* Mary Currier as Secretary
* Edmund Elton as Elderly man
* Tom Hanlon as Announcer
* Bobby Larson as "Chips"
 

==Production==
  New York stage, until Columbia purchased the rights as a vehicle for Cary Grant. While it was still in preproduction, Montgomery was borrowed from Metro-Goldwyn-Mayer to star in the film.   afi.com. Retrieved: April 10, 2015.  

Principal photography began on April 21, 1941, and ran until June 5, 1941. Location shooting took place at Providencia Ranch, California, and on Universal City sound stages. 

==Reception==
When Here Comes Mr. Jordan was reviewed by film critic Theodore Strauss for The New York Times, he noted, "... Columbia has assembled its brightest people for a delightful and totally disarming joke at heavens expense." He further described the film as, "... gay, witty, tender and not a little wise. It is also one of the choicest comic fantasies of the year." 
 Hollywood moviemaking at its best, with first-rate cast and performances." 

==Awards and honors== Best Actor Best Director, Best Actor Joseph Walker Best Cinematography, Black-and-White.
 UCLA UCLA Film and Television Archive with the cooperation of Columbia Pictures and the Library of Congress. 

==Remakes==
On January 26, 1942, Claude Rains, Evelyn Keyes and James Gleason reprised their roles in a   remake of Here Comes Mr. Jordan. 

==References==
Notes
 
Citations
 
Bibliography
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
*  
*  
*  
*  
*   on Lux Radio Theater: January 26, 1942

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 