Tucker & Dale vs. Evil
 
{{Infobox film
| name = Tucker & Dale vs. Evil
| image = Tucker-and-dale-vs-evil.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Eli Craig
| producer = Morgan Jurgenson Albert Klychak Rosanne Milliken Deepak Nayar
| screenplay = Eli Craig Morgan Jurgenson
| story = Eli Craig
| starring = Tyler Labine Alan Tudyk Katrina Bowden Jesse Moss 
| music = Michael Shields Andrew Kaiser
| cinematography = David Geddes
| editing = Bridget Durnford
| studio = Eden Rock Media Looby Lou Reliance Motion Picture Company Urban Island
| distributor = Magnet Releasing
| released =  
| runtime = 89 minutes 
| country = Canada 
| language = English
| budget = 
| gross = $4.7 million   
}}

Tucker & Dale vs. Evil is a 2010 Canadian comedy horror film written and directed by Eli Craig. The film stars Alan Tudyk, Tyler Labine and Katrina Bowden.

==Plot==
Allison, Chad, Chloe, Chuck, Jason, Naomi, Todd, Mitch and Mike are going camping in   and appearance, he only scares her and her friends.

Tucker and Dale arrive at their decrepit cabin and begin repairing it. Nearby in the woods, Chad tells a story about the "Memorial Day Massacre", a hillbilly attack which took place 20 years ago. The college kids go skinny-dipping where Tucker and Dale are fishing, and Allison, startled, hits her head. Tucker and Dale save her, but her friends think she was kidnapped. When Allison wakes up in Tucker and Dales cabin the next day, she is initially scared but befriends the two hillbillies. The other college kids arrive at the cabin to save Allison from her "psychopathic captors", and Chuck runs away to get the police. While Dale and Allison are inside the cabin, Tucker angers some bees and frantically waves around his chainsaw, which the college kids misinterpret as hostility. They scatter through the woods, and Mitch accidentally impales himself on a broken tree. After finding Mitchs body, Chad persuades the others that they are in a battle for survival.

Alisons friends follow Tucker and Dale back to their cabin and see Allison helping out with construction of an outhouse, but they assume she is digging her own grave. The college kids attack, but Todd and Mike end up accidentally killing themselves, and Allison is accidentally knocked unconscious again by Dales shovel. The other kids assume the hillbillies killed them when they see Tucker trying to save Mike from the wood chipper. Tucker and Dale think the college kids are suicidal and that contacting the police will make them murder suspects. Chuck arrives back with a sheriff, who expresses doubt over Tucker and Dales suicide pact theory. The sheriff goes inside the cabin and accidentally kills himself, and Chuck accidentally kills himself with the sheriffs gun. Chad reappears and attempts to shoot Tucker and Dale but only manages to capture Tucker, whom he ties upside down to a tree. Chad then tortures Tucker and cuts Tuckers fingers. He then sends a message to Dale to come and get Tucker. 

Dale leaves to rescue Tucker while Chad and Naomi return to the cabin to save Allison. When Allison tries to explain the situation, they accuse her of having Stockholm syndrome. Tucker and Dale return, and Allison attempts to lead a calm discussion. Chad says his grandmother told him that his father was killed in the Memorial Day Massacre, and his mother was the lone survivor. Jason and Chloe break in to save everyone, and a fire breaks out. Tucker, Dale, and Allison escape; Naomi, Chloe and Jason die, and Chad, insane and scarred, vows revenge. After a car crash, an injured Tucker tells Dale that Chad has taken Allison to an old sawmill, where he forcibly kisses her. Dale rescues Allison, and the two barricade themselves inside an upstairs office, where they discover news clippings that reveal Chads father to be the killer and rapist, not one of the victims. Chad becomes enraged, and Dale stops his attack by throwing a box of chamomile tea at Chad, which triggers a severe asthma attack. Chad convulses and falls out the window, apparently to his death.

The police and a news crew arrive late at the cabin and broadcast a news report stating that the deaths appear to be the result of a suicide pact and a deranged killer. Tucker watches the report on the news while convalescing in the hospital. Dale enters, and they discuss Tuckers recovery. Tucker asks Dale whether he managed to invite Allison on a date and is happy to hear the two of them are going ten-pin bowling|bowling. Later that night at the bowling alley, the two profess their feelings for each other and kiss.

==Cast==
* Tyler Labine as Dale Dobson
* Alan Tudyk as Tucker McGee
* Katrina Bowden as Allison
* Jesse Moss as Chad
* Chelan Simmons as Chloe
* Philip Granger as Sheriff
* Brandon Jay McLaren as Jason
* Christie Laing as Naomi
* Travis Nelson as Chuck
* Alex Arsenault as Todd
* Adam Beauchesne as Mitch
* Joseph Allan Sutherland as Mike
* Karen Reigh as Cheryl
* Tye Evans as Chads Dad

The films director, Eli Craig, and his wife Sasha have brief roles as a cameraman and reporter respectively.

==Production== trailer was released. 

==Reception==
===Box office=== SXSW Film Festival.  The movie was distributed by Magnolia and received a limited theatrical release in the US on 30 September 2011.  On its opening weekend, the film grossed $52,843 from 30 theaters. Domestic gross currently stands at $223,838, with foreign gross adding $4,525,678, bringing the worldwide gross to $4,749,516. 

===Critical response===
Tucker and Dale vs. Evil received positive reviews from critics and has a "certified fresh" score of 84% on Rotten Tomatoes based on 96 reviews with an average rating of 6.8 out of 10. The critical consensus states "Like the best horror/comedies, Tucker & Dale vs. Evil mines its central crazy joke for some incredible scares, laughs, and&mdash;believe it or not&mdash;heart".  The film also has a score of 65 out of 100 on Metacritic based on 23 critics indicating "generally favorable reviews". 

Todd Gilchrist of Shock Till You Drop wrote, "Eli Craigs feature debut celebrates genre conventions while turning the traditional view of horror-movie heroes and villains upside down."  Roger Ebert also gave the film a positive review, writing, "Students of the Little Movie Glossary may find it funny how carefully "Tucker and Dale" works its way through upended cliches".   Noel Murray of The A.V. Club rated it C+ and called it "surprisingly clever" but "too slick and too cute".   Dennis Harvey of Variety (magazine)|Variety wrote that the film "offers good-natured, confidently executed splatstick whose frequent hilarity suffers only from peaking too early." 

==Accolades==
{| class="wikitable sortable"
|-
! Award
! Category
! Nominee
! Result
|- Leo Award 
| Best Cinematography in a Feature Length Drama
| David Geddes
|  
|-
| Best Feature Length Drama
| Rosanne Milliken and Crawford Hawkins
|  
|-
| Best Overall Sound in a Feature Length Drama
| Paul A. Sharpe, Graeme Hughes and Iain Pattison
|  
|-
| Best Sound Editing
| James Fonnyadt
|  
|-
| Best Sound Editing in a Feature Length Drama
| Dario DiSanto, Brian Campbell, James Fonnyadt, Jay Cheetham, Kirby Jinnah and Kris Casavant
|  
|-
| Best Stunt Coordination in a Feature Length Drama
| Jodi Stecyk
|  
|-
| Best Supporting Performance by a Male in a Feature Length Drama
| Jesse Moss
|  
|-
| Sitges Film Festival
| Best Film  Eli Craig
|  
|-
| SXSW Film Festival
| Audience Award 
|  
|-
| Fantasia Festival
| Jury Prize; Best First Feature 
|  
|-
| Ampia Awards
| AMPIA Award; Best Feature Film
|  
|- Fangoria Chainsaw Award Best Actor
| Tyler Labine
|  
|-
| Best Supporting Actress
| Katrina Bowden
|  
|- Best Limited-Release/Direct-to-Video Film
|  
|-
| Best Screenplay
| Eli Craig, Morgan Jurgenson
|  
|-
|}

==Sequel==
At HorrorHound Weekend 2014 cast members Tyler Labine and Alan Tudyk confirmed that a sequel is in development.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 