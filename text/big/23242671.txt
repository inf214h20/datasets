Lady Jayne: Killer
{{Infobox film
| name           = Lady Jane: Killer Betrayal
| image          = Betrayal-2003.JPG
| caption        = DVD cover
| director       = Mark L. Lester
| producer       = Mark L. Lester Dana Dubovsky
| writer         = Jeffrey Goldenberg C. Courtney Joyner
| narrator       =
| starring       = Erika Eleniak Julie du Page Adam Baldwin James Remar Louis Mandylor
| music          = Richard McHugh
| cinematography = Joao Fernandes
| editing        = Donn Aron Daniel Macena
| distributor    = American World Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         =
}}
 thriller film released in 2003. The film stars Erika Eleniak, Julie du Page, Adam Baldwin, James Remar and Louis Mandylor.

==Film synopsis==
Jayne Ferré (du Page) is a professional assassin that works for the Mafia. When one of her hits goes wrong, she ends up with a suitcase full of a million dollars that belong to mob boss Frank Bianci (Louis Mandylor). Knowing that he has put a price on her head, Ferré decides to leave Los Angeles and ends up hitching a ride with Kerry, a teenager running from a drug dealer, and his mother, Emily (Eleniak).

After agreeing to cover their expenses, Jayne, Emily, and Kerry head for Texas. During the ride, their car breaks down which forces them to spend a night in a motel. On the night, Kerry discovers Jaynes money and decides to return to LA to pay his debt.

When Jayne finds out, she kidnaps Emily and they go to LA after Kerry to recover her money. Emily manages to escape, and stumbles upon Alex Tyler, an FBI agent who agrees to protect her and to take her to her son.

==Cast==
* Erika Eleniak as Emily Shaw
* Julie du Page as Jayne Ferré
* Adam Baldwin as Det. Mark Winston
* James Remar as Alex Tyler
* Louis Mandylor as Frank Bianci
* Jeremy Lelliott as Kerry

==Release==
The film DVD premiered on 2003 in Italy, Netherlands, Finland, Spain, United States, and Germany.

==External links==
* 
*   at Rotten Tomatoes

 

 
 
 
 


 