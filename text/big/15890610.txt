The Song of Sparrows
{{Infobox film
| name           = The Song of Sparrows
| image          = The Song of Sparrows, 2008 film.jpg  
| image_size     =
| caption        = Film poster
| director       = Majid Majidi
| producer       = Majid Majidi
| writer         = Majid Majidi Mehran Kashani
| narrator       =
| starring       = Reza Naji
| music          = Hossein Alizadeh
| cinematography =
| editing        =
| distributor    =
| released       = February 2008
| runtime        =
| country        = Iran Azeri
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 2008 Cinema Iranian movie directed by Majid Majidi.  It tells the story of Karim, a man who works at an ostrich farm until he is fired because one of the ostriches escaped. He finds a new job in Tehran, but he faces new problems in his personal life.   
This film opened to critical acclaim.

==Plot==
Karim works at an ostrich farm outside of Tehran, Iran. He leads a simple and contented life in his small house with his wife Narges, and three children, whom he loves and tries to make happy.

One day when he is in the farm, he is told to return home early as his elder daughter - Haniyeh - has lost her hearing aid. When he reaches home he finds that his son Hussein and neighborhood children are searching for the hearing aid in their underground water cistern, which, because of blockages holds little but sludgy mud now. Karim scolds his son and others for coming there, but joins them in searching for the hearing aid. During the search his son  Hussein and his friends reveal their idea of clearing the sludge and raising fish in the hope of becoming millionaires. Karim rejects the idea and discourages them. They eventually find the hearing aid, but Karim discovers that its not working properly. He approaches the hospital and learns that he has to wait up to four months to get the aid repaired for free, otherwise he has to go to Tehran city to replace it immediately. As his daughters exam is approaching, he is worried about getting the hearing aid promptly.

Shortly after, when they are moving new ostriches into the farm, one of the ostriches escapes, Karim is blamed for the loss and is fired from the farm. Soon after this, he travels to the city in order to repair Haniyehs, hearing aid which he knows will cost 350,000 tomans. He finds himself mistaken for a motorcycle taxi driver and thus begins his new profession: ferrying people and goods through heavy traffic. However, the people and goods he is dealing with every day start to change Karims generous and honest nature, much to the distress of his wife and daughters. Every day he brings discarded items from the city to his home and becomes more greedy and begins to forget about his daughter’s hearing aid. It is up to those closest to him to restore the values that he once cherished. One day when he tries to arrange the mounting pile of junk which he has brought home, the pile collapses around him breaking his leg and causing other injuries which prevent him from working. During his recovery, his son starts working instead, partly to help feed the family, but the son and his friends also manage to buy the fish which they previously talked about. While delivering some plants to a local farm along with his uncle, the barrel which contains the fish starts to leak. As the boys carry it to refill and fix it, the barrel bursts and all the fish spill out on the ground. Instead of just watching them die, his son Hussein releases the fish in the nearby water. Karim watches this and feels proud of his son. While Karim is getting healthier he gets a message from an ex-colleague that the ostrich which had run away earlier had since returned. Karim goes to the farm and watches the ostrich with tears in his eyes.

==Awards and nominations==
* Winner, Best Performance by an Actor (Reza Naji) - Asia Pacific Screen Awards 2008
* Selected as Irans representative for Best Foreign Language Film in the 81st Academy Awards. 
* 2008:  : Reza Naji   

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 