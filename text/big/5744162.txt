Funny Games (2007 film)
 
 
{{Infobox film
| name = Funny Games
| image = Mpafunnygamesposterb.jpg
| caption = Theatrical release poster
| director = Michael Haneke
| producer = Hamish McAlpine Christian Baute Chris Coen Andro Steinborn Naomi Watts
| writer = Michael Haneke
| starring = Naomi Watts Tim Roth Michael Pitt Brady Corbet Devon Gearhart
| cinematography = Darius Khondji
| editing = Monika Willi
| studio = Celluloid Dreams Tartan Films Film4 Productions
| distributor = Warner Independent Pictures
| released =    
| runtime = 111 minutes  
| country = United States France United Kingdom Germany Italy
| language = English
| budget = $15 million
| gross = $8 million
}} psychological Thriller thriller film Funny Games. Naomi Watts, Tim Roth, Michael Pitt, and Brady Corbet star in the main roles. The film is a Shot-for-shot#Film to film|shot-for-shot remake of the 1997 film, albeit in English and set in the United States with different actors.    Exterior scenes were filmed on Long Island.  The film is an international co-production of the United States, United Kingdom, France, Germany, and Italy.   

Haneke has stated that the film is a reflection and criticism of violence used in media. 

==Plot==
Members of a loving family—George and Ann Farber, their son Georgie, and their dog—arrive at their lake house. Their next-door neighbour, Fred, is seen with two young men, Peter and Paul, who seem to be friends or relatives. They find Fred reacting somewhat awkwardly. Fred and Paul come over to help put the boat into the lake. Lucky, the dog, keeps barking at Paul, but George ignores it. After Fred and Paul leave, George and Georgie stay outside by the lake, tending to their boat. Georgie asks his father why Fred was behaving so strangely.

While Ann is in the kitchen cooking, Peter comes by to borrow some eggs. Ann gives him the eggs but Peter drops them. Feeling a little annoyed, Ann gives him another four eggs and Peter takes off. Soon afterwards she hears Lucky barking and Peter and Paul show up together. They seem friendly, and they admire a golf club belonging to George. Paul asks her to try out one of the clubs outside and she approves. In the boat, George and Georgie hear Lucky is barking hysterically when suddenly the barking stops.

Peter and Paul request more eggs, because the last ones also ended up broken, Ann becomes frustrated, but when George tries to force the men to leave, Peter breaks Georges leg with the golf club. The family is then taken hostage.

Ann tries to call for help on a cell phone, but finds it unusable because Peter had dropped it in the sink. Paul then guides Ann on a hunt to find the familys dog, which he has killed with Georges golf club. When neighbors visit, Ann passes the two men off as friends.

The family is forced to participate in a number of  . Paul frequently ridicules Peters weight and lack of intelligence. He describes a number of contradicting stories of Peters past, although no definitive explanation is ever presented as to the mens origins or motives.

Georgie tries to escape. He attempts to climb a locked gate but changes his mind and goes to the neighbors empty house. There he discovers that they have been killed. Georgie attempts to shoot Paul with a shotgun, but the gun fails to go off. Paul returns him to the living room, along with the shotgun.

The men play a new game, saying whoever gets counted out will be shot. Georgie panics and makes a run for his life which results in Peter shooting and killing him. Paul is a little annoyed that Peter didnt follow the rules of their game to the letter.

George and Ann weep for their loss. They eventually resolve to survive. Ann is able to flee the house while George, with a broken leg, desperately tries to make a call on the malfunctioning phone. Ann struggles to find help, only to be re-captured by Peter and Paul, who return her to the house. After stabbing George, they attempt to force Ann to make a choice for her husband, between a painful, prolonged death with the knife or a quick death with the shotgun.

Ann seizes the shotgun on the table in front of her and kills Peter. An enraged Paul grabs the shotgun and starts looking for the television remote. Upon finding it, he literally rewinds the last occurrences back to a moment before Ann grabs the shotgun, thereby breaking the fourth wall. On the "do over," Paul snatches the shotgun away and admonishes her, saying she isnt allowed to break the rules.

Peter and Paul then kill George and they take Ann, bound and gagged, out onto the familys boat. Around eight oclock in the morning, they nonchalantly throw her into the water to drown, thus winning their bet. They dock at the house of the neighbors who had previously visited the family. They request some eggs, thereby restarting their cycle of murder.

==Cast==
{| class="wikitable"
|-
! Character !! 1997 Austrian version !! 2007 American version
|-
| Anna || Susanne Lothar || Naomi Watts
|-
| George Sr. || Ulrich Mühe || Tim Roth
|-
| George Jr. || Stefan Clapczynski || Devon Gearhart
|-
| Paul || Arno Frisch || Michael Pitt
|-
| Peter || Frank Giering || Brady Corbet
|-
| Fred || Christoph Bantzer || Boyd Gaines
|- Siobhan Fallon
|-
| Robert || Wolfgang Glück || Robert LuPone
|}

For 2007s American remake, the character of Gerda was renamed "Betsy", the first family to fall victim to Paul and Peter were given the surname "Farber" and the 2nd family were given the surname "Thompson".

==Development==
 . Retrieved on 12 October 2013. 

After the 2007 film used the same house including props and tones, Robert Koehler of Cineaste (magazine)|Cineaste wrote that this "proves for certain that — whether he uses the great cinematographer Jurgen Jurges (for the 1997 version) or the great Darius Khondji (for the new film) — Haneke is fundamentally his own cinematographer exercising considerable control over the entire look of his films." 

==Release== British premiere at the London Film Festival on 20 October 2007.       Its United States premiere was at the 2008 Sundance Film Festival on 19 January 2008. It began a limited release in the United States and Canada on 14 March 2008, distributed by Warner Independent.      A wider release to more theaters came on 8 April 2008. The film was shown at the Istanbul Film Festival in April 2008. It did not receive a wide theatrical release in the United States before coming out on DVD. Funny Games was a box office failure, grossing a little more than half of its $15 million budget. The Guardian|Guardian writer Geoffrey Macnab included Funny Games s lack of success among the reasons for the closure of Tartan Films, which co-produced the film and released it in the United Kingdom.    In Germany, the film was released under the title "Funny Games U.S.". 

===Home media===
The DVD was released on 10 June 2008, in the US. The DVD does not contain any extra material but instead it includes both widescreen and full screen editions on one disc. In the UK, the DVD and Blu-ray Disc|Blu-ray were released on 28 July with the extra material being the original theatrical trailer, Q&A with producers Hamish McAlpine and Chris Coen, interviews with the cast, viral video clips and film notes.

==Themes== breaks the fourth wall throughout the film and addresses the camera in various ways. As he directs Ann to look for her dead dog, he turns, winks, and smirks at the camera. When he asks the family to bet on their survival, he turns to the camera and asks the audience whether they will bet as well. At the end of the film, when requesting eggs from the next family, he looks into the camera and smirks again. Only Paul breaks the fourth wall in the film, while Peter makes references to the formulaic suspense rules of traditional cinema throughout the film.

Paul also frequently states his intentions to follow the standards of movie plot development. When he asks the audience to bet, he guesses that the audience wants the family to win. After the killers vanish in the third act, Paul later explains that he had to give the victims a last chance to escape or else it would not be dramatic. Toward the end of the movie, he postpones killing the rest of the family because the movie has not yet reached feature length. Throughout the film, Paul shows awareness of the audiences expectations.
 one protagonist red herring. At the end of the film, Paul again smirks triumphantly at the audience. As a self-aware character, he is able to go against the viewers wishes and make himself the winner of the film.

After killing Ann, Peter and Paul argue about the line between reality and fiction. Paul believes that a fiction that is observed is just as real as anything else, but Peter dismisses this idea. Unlike Paul, Peter never shows an awareness that he is in a film.

Haneke states that the entire film was not intended to be a horror film. He says he wanted to make a message about violence in the media by making an incredibly violent, but otherwise pointless movie. He had written a short essay revealing how he felt on the issue, called "Violence + Media." The essay is included as a chapter  in the book A Companion to Michael Haneke. 

==Critical reception==
The film received mixed reviews from critics. The review aggregator Rotten Tomatoes reported that 52% of critics gave the film positive reviews, based on 139 reviews.  Metacritic reported the film had an average score of 44 out of 100, based on 33 reviews. 

Todd Gilchrist from  , whose Hostel (2005 film)|Hostel movies have brought nothing but scorn from responsible critics."  The Chicago Sun-Times review of 14 March 2008 gave the film a mere half-star out of a possible four.

The Times of London ranked it #25 on its 100 Worst Films of 2008 list, calling it "art-house torture porn."   ( ) 

==Soundtrack== Naked City Torture Garden.

==See also==
* List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 