Don Quixote (unfinished film)
{{Infobox film
| name           = Don Quixote
| image          =
| image_size     =
| caption        =
| director       = Orson Welles
| producer       = Orson Welles Oscar Dancigers
| writer         = Orson Welles (based on the novel by Miguel de Cervantes)
| narrator       = Orson Welles
| starring       = Francisco Reiguera Akim Tamiroff Patty McCormack Orson Welles
| music          =
| release        =
| cinematography = Jack Draper (principal cinematographer) Jose Garcia Galisteo Juan Manuel de Lachica Edmond Richard Ricardo Navarrete Manuel Mateos Giorgio Tonti Gary Graver
| editing        = Orson Welles Mauro Bonanni Maurizio Lucidi Renzo Lucidi Peter Parasheles Ira Wohl Alberto Valenzuela
| released       =
| runtime        = Unfinished
| country        =
| language       = English (dubbed)
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| distributor    =
}}

{{Infobox film
| name           = Don Quijote de Orson Welles
| image          =
| image_size     =
| caption        = DVD  box art
| director       = Orson Welles Jesus Franco Javier Mina
| producer       = Patxi Irigoyen
| writer         = Orson Welles Jesus Franco
| narrator       = Orson Welles Jose Mediavilla
| starring       = Francisco Reiguera Akim Tamiroff Orson Welles
| music          = Daniel J. White
| release        =
| cinematography = John S. Carroll
| editing        = Jesus Franco Stanley Kotis Josa Maria Almirall Fatima Micalczik
| released       = May 16, 1992 (Cannes Film Festival)
| runtime        = 110 minutes
| country        = Spain, Italy, USA
| language       = English (dubbed) Spanish (dubbed) Italian (dubbed)
| budget         =
| gross          = ESP 807,150 (Spain)
| preceded_by    =
| followed_by    =
| distributor    = El Silencio
}}

Don Quixote or Don Quixote de Orson Welles is an unfinished film project produced, written and directed by Orson Welles. Principal photography was between 1957 and 1969; while test footage was filmed as early as 1955, second-unit photography was done as late as 1972, and Welles was working on the film on and off until his death in 1985.

==Cast==
*Francisco Reiguera...Don Quixote
*Akim Tamiroff...Sancho Panza
*Patty McCormack...Dulcie
*Orson Welles...Himself/Narrator

==Television project - Don Quixote Passes By==
Don Quixote was initially conceived in 1955 as a 30-minute film for  , "What interests me is the idea of these dated old virtues. And why they still seem to speak to us when, by all logic, theyre so hopelessly irrelevant. Thats why Ive been obsessed for so long with Don Quixote...  cant ever be contemporary - thats really the idea. He never was. But hes alive somehow, and hes riding through Spain even now...The anachronism of Don Quixotes knightly armor in what was Cervantes own modern time doesnt show up very sharply now. Ive simply translated the anachronism. My film demonstrates that he and Sancho Panza are eternal." 
 Black Magic, The Trial). Cowie, Peter. "The Cinema of Orson Welles." 1973, A.S. Barnes & Co.  It was the first time Welles had filmed in colour since the ill-fated production of Its All True (film)|Its All True in 1942. However, representatives from CBS viewed unedited film and were unhappy with Welles concept, cancelling the project.    The original colour test shots with Auer were subsequently lost, and are no longer believed to exist.

==Change to a feature film==
Welles decided to push ahead by expanding the production into a feature film, made in black and white.  Singer/actor Frank Sinatra (an old friend who Welles had first befriended when the two were newcomers to Hollywood in the early 1940s) invested US$25,000 in the new film, with Welles providing additional self-funding derived from his work as an actor. 

==Production==
On 29 June 1957, after having been removed from his own film Touch of Evil, Welles headed to Mexico City to begin work on the feature-length version of Don Quixote.  The part of Don Quixote had been offered to Charlton Heston, who had just finished filming Touch of Evil with Welles, and Heston was keen on playing the role, but was only available for two weeks, which Welles feared would be insufficient. Spanish actor Francisco Reiguera was cast as Don Quixote and Akim Tamiroff remained as Sancho Panza.  Welles also brought in child actress Patty McCormack to play Dulcie, an American girl visiting Mexico City as the citys central framing device. During her visit, Dulcie would encounter Welles (playing himself) in a hotel lobby, on the hotel patio and in a horse-drawn carriage, and he would tell her the story of Don Quixote. She would then meet Don Quixote and Sancho Panza in the present day, and would later tell Welles of her adventures with them. 
 Texcoco and Río Frio, Mexico|Río Frio.
 The Vikings King of Five Kings and Rhinoceros (play)|Rhinoceros When money was available, he switched the location shooting to Spain. As time went by, McCormack matured out of childhood, forcing Welles to drop her character from the film.  In later years, he stated that he wished to re-film her scenes, plus some new ones, with his daughter Beatrice Welles (who had a small part in his Chimes at Midnight). However, he never did so, and by the late 1960s Beatrice also grew out of childhood.
 The Trial.  Welles continued to show Don Quixote and Sancho Panza in the present day, where they react with bafflement at such inventions as motor scooters, airplanes,  automobiles, radio, television, cinema screens and missiles.    Welles never filmed a literal version of the famous scene in which Quixote duels with windmills, he instead made a modern-day version of it in which Quixote walks into a cinema. Sancho Panza and Patty McCormacks character are sat in the audience, watching the screen in silent amazement. A battle scene plays onscreen, and Quixote mistakes this for the real thing, trying to do battle with the screen and tearing it to pieces with his sword.

The production became so prolonged that Reiguera, who was seriously ailing by the end of the 1960s, asked Welles to finish shooting his scenes before his health gave out. Welles was able to complete the scenes involving Reiguera prior to the actor’s death in 1969.  However, as Welles shot most of the footage silently, he seldom filmed the original actors dialogue. He intended to dub the voices himself (as he did on many of his films, including Macbeth, Othello, The Trial and The Deep), combining his narration with his voicing all the characters, but only ever did so for some limited portions of the film.
 Ed Wood (1994).

==Changing concept, and unfinished work==
Although principal photography ended after Reiguera’s passing, Welles never brought forth a completed version of the film.  As the years passed, he insisted that he was keen to complete the film, but it is clear that the concept changed several times. Welles stressed that unlike some of his other films, he was under no deadlines and regarded the film as "My own personal project, to be completed in my own time, as one might with a novel", since he was not contracted to any studio and had privately financed the picture himself. Filming The Trial (1981) Welles made these comments in an interview captured in this film 

At one point in the 1960s, Welles planned to end his version by having Don Quixote and Sancho Panza surviving an atomic cataclysm, but the sequence was never shot.  As Welles deemed that principal photography was complete by 1969, it is likely that by this stage he had changed his conception of the ending.
 the Holy Week procession and some inserts of windmills for the film - although this footage has since been lost. Joseph McBride, What Ever Happened to Orson Welles? A portrait of an independent career (University Press of Kentucky, Lexington, Kentucky, 2006) p.238  

By the early 1980s, he was looking to complete the picture as an "essay film" in the style of his F for Fake and Filming Othello, using the footage of Don Quixote and Sancho Panza to compare the values of Cervantes Spain, Francisco Franco|Francos Spain (when the film was set), and modern-day Spain post-Franco. Welles himself explained, "I keep changing my approach, the subject takes hold of me and I grow dissatisfied with the old footage. I once had a finished version where the Don and Sancho go to the Moon, but then   went to the Moon, which ruined it, so I scrapped ten reels  . Now I am going to make it a film essay about the pollution of old Spain. But its personal to me."  However, he never filmed any of the footage necessary for this later variation.

One possible explanation for the films lack of completion was offered by Welles comments to his friend and colleague Dominique Antoine - he told her that he could only complete Don Quixote if he one day decided not to return to Spain, since every fresh visit gave him a new perspective, with new concepts for the film.  At the time of his death, he was still discussing doing more filming for Don Quixote, and had produced over 1,000 pages of script for the project. 

The endless delay in completing the project spurred the filmmaker to consider calling the project When Are You Going to Finish Don Quixote?, referring to the question he was tired of hearing. (It is unclear whether or not Welles was joking about this.)    Up until his death in 1985, Welles was still publicly talking about bringing the unfinished work to completion. 
 Cannes Film Festival.     The footage consisted of 45 minutes of scenes and outtakes from the film, assembled by the archivists from the Cinémathèque Française and supervised by the director Costa-Gavras. 

==Don Quijote de Orson Welles by Jesus Franco== Croatian actress who was Welles mistress and collaborator in his later years, and Suzanne Cloutier, the Canadian actress who played Desdemona in Welles film version of Othello (1952 film)|Othello.  In his will, Welles left Kodar the rights to all his unfinished film projects (including Don Quixote) and she was keen to see it completed. She spent the late 1980s touring Europe in a camper van with her Don Quixote  footage, and approached several notable directors to complete the project. All of them declined for various reasons - except Franco. Franco seemed a logical choice, as he had worked as Welles Second Unit Director on his Chimes at Midnight.
 Italian film editor Mauro Bonanni (who had worked on the film in Rome in 1969), who was engaged in a legal dispute with Kodar over the rights to the film. He refused to allow its incorporation into the Irigoyen-Franco project, although he would later permit some scenes to be shown on Italian television.  As a consequence of this litigation between Kodar and Bonanni, Kodar insisted that none of the footage with Patty McCormck should be used.
 Joseph McBride a documentary he had made on Spain in the 1960s. Welles had not intended to appear in the film himself, other than in its framing scenes as the narrator, and yet the Irigoyen/Franco film features several scenes with Quixote and Sancho Panza on Spanish streets, with Welles apparently looking on. Additionally, Franco inserts a windmill scene into the film, even though Welles had not filmed one or ever intended to film one - the scene relies on footage of Quixote charging across plains, interspersed with windmill images (which were not filmed by Welles), zooms and jump cuts.
 The Magnificent The Stranger, The Lady from Shanghai, Macbeth (1948 film)|Macbeth, Mr. Arkadin and Touch of Evil), so he divided up all the reels of film for Don Quixote and deliberately mislabelled many of them, telling Mauro Bonanni, "If someone finds them, they mustnt understand the sequence, because only I know that." 

The Irigoyen and Franco work premiered at the 1992 Cannes Film Festival as Don Quixote de Orson Welles, with English- and Spanish-language versions produced.  Initial reaction was predominantly negative, and this version was never theatrically released in the U.S. In September 2008, a U.S. DVD edition was released as Orson Welles Don Quixote by Image Entertainment.  The footage of Don Quixote in the cinema that is in Bonannis possession has turned up on YouTube. 

Spanish film critic Juan Cobos saw a rough cut of Welles unfinished footage (which he praised very highly), and stated that the 1992 edit by Franco bore little resemblance to it. Similarly, Jonathan Rosenbaum describes the 45 minutes of footage assembled in 1986 as being vastly superior to the Franco edit.

===Cast of Don Quijote de Orson Welles===
*Francisco Reiguera...Don Quixote
*Jose Mediavilla...Voice of Don Quixote/Narrator (selected scenes)
*Akim Tamiroff...Sancho Panza
*Jan Carlos Ordonez...Voice of Sancho Panza (selected scenes)
*Orson Welles...Himself/Narrator/Voice of Don Quixote/Voice of Don Quixote (selected scenes)

==Surviving footage== Munich Film Museum, but in the course of making Don Quijote de Orson Welles she had earlier sold much of the footage to the Filmoteca Española in Madrid, whose holdings include around 40 minutes edited and dubbed by Welles. Welles own editing workprint is held by the Cinémathèque Française in Paris. Additional footage is held by Mauro Bonnani in Italy, and in at least one other private collection.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 