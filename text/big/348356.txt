Bringing Out the Dead
 
{{Infobox film
| name           = Bringing Out the Dead
| image          = Bringing_out_the_dead.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| screenplay     = Paul Schrader
| based on       =  
| producer       = Barbara De Fina Scott Rudin
| starring       = Nicolas Cage Patricia Arquette John Goodman Ving Rhames Tom Sizemore Marc Anthony
| music          = Elmer Bernstein Robert Richardson
| editing        = Thelma Schoonmaker
| studio         = Touchstone Pictures Buena Vista International
| released       = October 22, 1999
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $55 million
| gross          = $16.7 million 
}} Joe Connelly. flop at the box office but received very positive reviews from critics. It was also the last North American title to be released on Laserdisc. {{cite web
|url=http://laserdiscplanet.com/museum2.html
|title=LaserDisc Museum
|accessdate=2012-11-27
|publisher=LASERDISC PLANET
}} 

== Plot ==
Set in Manhattan in the early 1990s, Frank Pierce (Cage) is a burned-out paramedic who works the graveyard shift in a two-man ambulance team with various different partners. Usually exhausted and depressed, he has not saved any patients in months and begins to see the ghosts of those lost, especially a homeless adolescent girl named Rose whose face appears on the bodies of others. Frank and his first partner Larry (Goodman) respond to a call by the family of a man named Mr. Burke who has entered cardiac arrest. Frank befriends Mr. Burkes distraught daughter Mary (Arquette), a former junkie. Frank discovers Mary was childhood friends with Noel (Anthony), a brain-damaged drug addict and delinquent who is frequently sent to the hospital.
 New York and caused cardiac arrest calls to sky-rocket, roll out from the victims sleeve which implies it was a shooting by a rival drug gang. While in the back of the ambulance with Frank and Noel the victim goes into denial and repents his drug dealing ways but dies before they can reach the hospital.
 Puerto Rican man whose girlfriend is giving birth to twins despite his claims they are both Virginity|virgins, calling it a miracle. Frank rushes one baby to the hospital but it later dies. In a moment of desperation Frank starts drinking and Marcus soon joins in, crashing the ambulance into a parked car.
 resuscitates Burke instead.

The next shift Frank is paired with his third partner Tom Wolls (Sizemore), an enthusiastic man with violent tendencies. At this point Frank is slowly beginning to lose his mind - while tending to a suicidal junkie Frank manages to scare the patient away. The pair are then called to Cys drug den where another shooting has occurred, and find Cy impaled on a railing, having attempted to jump to safety. Frank holds on to Cy as the other emergency services cut the railing but Cy and Frank are nearly flung off the edge before being pulled back up. Cy then thanks Frank for saving his life - the first patient Frank has saved in months. Afterwards Frank agrees to help Tom beat up Noel, but Frank is distracted and Noel flees into an area beneath the houses. Tom and Frank chase after Noel but Frank starts to hallucinate again, snapping out of it just as he comes upon Tom beating Noel with his Baseball bat|bat. During his second visit to Mr. Burke, the voice again pleas to let him die, and this time Frank removes Burkes breathing apparatus causing him to enter cardiac arrest, ending his life. Frank then heads to Marys apartment to inform her, and she seems to accept her fathers death. Frank is invited in, falling asleep at Marys side.

== Cast ==
* Nicolas Cage as Frank Pierce
* Patricia Arquette as Mary Burke
* John Goodman as Larry
* Ving Rhames as Marcus
* Tom Sizemore as Tom Wolls
* Marc Anthony as Noel
* Cliff Curtis as Cy Coates
* Mary Beth Hurt as Nurse Constance
* Aida Turturro as Nurse Crupp
* Phyllis Somerville as Mrs. Burke
* Queen Latifah as Dispatcher Love (Voice Only)
* Martin Scorsese as Dispatcher (Voice Only)

== Sound-track ==

=== Track listing ===
# "T.B. Sheets" - Van Morrison	 Janie Jones" - The Clash	
# "You Cant Put Your Arms Around a Memory" - Johnny Thunders	
# "Whats the Frequency, Kenneth?" - R.E.M.	
# "Im So Bored with the USA" - The Clash	
# "Red Red Wine" - UB40	 Nowhere to Run" - Martha Reeves and the Vandellas	
# "Too Many Fish in the Sea" - The Marvelettes Japanese Sandman)" - The Cellos
# "Rivers of Babylon" - The Melodians
# "Combination of the Two" - Big Brother & The Holding Company Bell Boy" - The Who

== Production == Snake Eyes (1998) with Gary Sinise. 
The opening song on the movie is "T.B. Sheets", a lengthy blues-influenced song, about a young girl who lies dying in a hospital bed, surrounded by the heavy smell of death and disease. It was written by Van Morrison and included on his 1967 album, Blowin Your Mind!. The song was originally to be used in Taxi Driver.

The director, Martin Scorsese, and Queen Latifah provided the voice of the ambulance dispatchers.

== Reception ==

=== Critical response ===
The film was well received by critics and holds a 71% "Certified Fresh" rating on Rotten Tomatoes, based on 105 reviews. The sites consensus reads "Stunning and compelling, Scorsese and Cage succeed at satisfying the audience."  
Roger Ebert gave it a perfect four-star rating, writing, "To look at Bringing Out the Dead--to look, indeed, at almost any Scorsese film--is to be reminded that film can touch us urgently and deeply". 

=== Box office ===
Bringing Out the Dead debuted at #4 in 1,936 theatres with a week-end gross of only $6,193,052. Produced at a budget of $55 million but generating a revenue of just $16.7 million, the film was a box office bomb.

== Notes ==
*  

== References ==
 

== External links ==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 