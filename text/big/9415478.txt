Hell's Wind Staff
 
 
{{Infobox film
| name = Hells Wind Staff
| image = HellsWindstaffDVDCover.jpg
| image_size = 
| caption = DVD cover
| director = Lu Chin-ku Tony Wong
| producer = Tony Wong Hwang Yen-kwang
| writer = Huang Yu-liang Tony Wong Thomas Tang
| story = Tony Wong
| starring = Hwang Jang-lee Meng Yuen-man Mang Hoi  Kwan Yung-moon
| music = Frankie Chan
| cinematography = Ma Goon-wa
| editing = Poon Hung-yiu
| distributor = Yuk Long Movies
| released =  
| runtime = 85 min.
| country = Hong Kong
| language = Cantonese
| budget = 
}}
Hells Wind Staff (released in the United Kingdom as Hellz Windstaff) is a 1979 Hong Kong martial arts film directed by Lu Chin-ku, and also written, produced, storied and directed by Tony Wong, starring Hwang Jang-lee, Meng Yuen-man, Mang Hoi and Kwan Yung-moon.

==Plot==
Two young kung fu experts are terrorized by an evil warlord whose weapon is known as the Hells Wind Staff. With the aid of an old rival of the warlord, they train in the Dragon Hands and the Rowing Oar to face off against the deadly Hells Wind Staff.

==Cast==
*Hwang Jang-lee – Lu Shan-tu
*Meng Yuen-man – Tiger Wang
*Mang Hoi – Stone Dragon
*Kwan Yung-moon – Shek
*Jason Pai Piao – Ching Wan-li
*Yip Fei-yang – Tiao Erh
*Lau Hok-nin – Master Wang Fu-hu
*Baan Yun-sang – Snake Fighter
*Wong Mei – Master Liu Chia-wen
*Hsu Hsia – Cousin Hsu
*Cheung Hei – Older Fisherman #1
*Chui Fat – Beats Up Unwilling Fishermen
*Gam Tin-chue – Older Fisherman #2
*Ho Kei-chong
*Kei Ho-chiu
*Lee Chu-hwa – Muscles
*Lin Ke-ming
*Lo Wai – Hsu’s Student
*Tai San – Recruits Fishermen
*Wang Han-chen – Uncle Fu
*Yeung Sai-gwan

==Action choreographers==
*Hsu Hsia
*Corey Yuen
*Chin Yuet-sang
*Yuen Shun-yee

==DVD release== Region 2 in the United Kingdom on 24 February 2003, it was distributed by Eastern Heroes.
In the US it was released on DVD by Xenon Video (2003) in 2.35:1 ratio, original language (Cantonese with chinese and english subtitles burned in), the ending where you see the bad guy get shoved into a tree stump and his legs yanked apart like a wishbone, is truncated. The World Video release (2001) is full screen, english dubbed and it features the full ending.

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 