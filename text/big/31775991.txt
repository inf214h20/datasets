Wives and Lovers (film)
{{Infobox film
| name           = Wives and Lovers
| image          = 
| image_size     = 
| caption        =  John Rich
| writer         = Edward Anhalt
| based on       =  
| narrator       = 
| starring       = Janet Leigh	
| music          = Lyn Murray
| cinematography = Lucien Ballard
| editing        = Warren Low
| studio         = 
| distributor    = 
| released       = November 4, 1963
| runtime        = 104 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} John Rich. It stars Janet Leigh and Van Johnson.  It was nominated for an Academy Award in 1964 for costume design. 

==Plot==
Husband and wife Bill and Bertie Austin and their daughter live in a low-rent apartment. Hes a struggling writer, at least until agent Lucinda Ford breaks the news that shes sold his book to a publisher, including the rights to turn it into a Broadway play.

A new house in Connecticut is the first way to celebrate. But during the long hours Bill is away working on the play, Bertie befriends hard-drinking neighbor Fran Cabrell and her boyfriend Wylie, who plant seeds of suspicion in Berties mind that Bill and his beautiful agent might be more than just business partners.

Bertie jealously retaliates by flirting with Gar Aldrich, an actor who will be in her husbands play. Bill goes to Connecticut for a heart-to-heart talk, finds Gar there and punches him. But when the plays a success, Bill and Bertie decide to give married life one more try.

==Cast==
*Janet Leigh as Bertie Austin
*Van Johnson as Bill Austin
*Shelley Winters as Fran Cabrell
*Martha Hyer as Lucinda Ford
*Ray Walston as Wylie Dreberg
*Jeremy Slate as Gar Aldrich

==References==
 

==External links==
* 

 

 
 
 
 