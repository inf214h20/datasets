Crossbow (short film)
{{Infobox film
| name = Crossbow
| image = crossbowposter.jpg
| director = David Michôd
| producer = Angie Fielder Polly Staniford 
| writer = David Michôd
| starring = Cy Standen Joel Edgerton Lisa Chappell Mirrah Foulkes
| music  = Sam Petty
| cinematography = Greig Fraser
| editing = Luke Doolan
| studio = Aquarius Films
| released =  
| runtime = 15 min
| country = Australia English
}}
Crossbow is a 2007 Australian coming-of-age film|coming-of-age drama film written and directed by David Michôd.   The film features Cy Standen, Joel Edgerton, Lisa Chappell and Mirrah Foulkes and had its world premiere in competition at the Venice Film Festival on 9 March 2007.  After that the film compete at number of film festivals and earned good reviews.   

== Plot ==
The film focus on a kid and his relationship with his mum and dad. His problems and struggle and the neighbor who watched the whole thing unravel.

==Cast==
*Cy Standen as The kid
*Joel Edgerton as The dad
*Lisa Chappell as The mum
*Mirrah Foulkes as The Cop
*David Michôd as Narrator

==Filming==
Filming took place at Macquarie Fields, Sydney, New South Wales, Australia. 

==Reception==

===Critical response===
The film received mainly positive reviews. Jason Sondhi of short of the week gave film the positive review and praised Michôds direction and said "I find the elegaic streak that Crossbow mines to be rare and wonderful in film, and kudos go to Michôd, because it is difficult to pull off. Indeed the film is strikingly reminiscent to one of the best films of this vein, Sofia Coppola’s work, The Virgin Suicides, a movie structured very similarly, with its title that undermines suspense, and its observant narration that wrestles with the exquisite sadness of seemingly senseless tragedy, and how it relates to sexuality and nostalgia."   Another review for the film also praised Michôd by saying "Michôd seems very close to hitting all the right notes in Crossbow and he seems extremely comfortable behind a camera." 

===Accolades===
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3"| 2007
| Melbourne International Film Festival
| Film Victoria Erwin Rado Award for Best Australian Short Film
| 
|   
|- Australian Film Institute Awards (AFI)
| Best Screenplay in a Short Film
| David Michôd
|   
|-
| Fitz Best Short film Awards
| Best Film
| 
|   
|-
| rowspan="3"| 2008
| rowspan="2"| Flickerfest film festival
| Best Director
| David Michôd
|      
|-
| Best Sound Design
| Sam Petty
|   
|-
| St Kilda Short Film Festival Best Cinematography
| Greig Fraser
|    
|}
 

==See also==
* Cinema of Australia

== References ==
 

== External links ==
*  
* 
* http://www.aquariusfilms.com.au/index.php/about
* http://www.aquariusfilms.com.au/index.php/download_file/-/view/16

 

 
 
 
 
 
 
 
 
 
 
 
 