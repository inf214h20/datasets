Bach's Fight for Freedom
{{Infobox film
| name           = Bachs Fight for Freedom
| image          =
| caption        =
| director       = Stuart Gillard David Devine, Richard Mozer David Devine Writer Raymond Storey
| narrator       =
| starring       = Kyle Labine Eric Peterson
| music          =
| cinematography = David Perrault
| editing        = Michael Pacek
| studio         = Devine Entertainment
| distributor    =
| released       =  
| runtime        = 53 min.
| country        = Canada Czech Republic English
| budget         =
| gross          =
}}	
 David Devine and Richard Mozer for HBO Original Films of New York and directed by Stuart Gillard.
 Ian D. Clark, Rosemary Dunsmore, Kevin Jubinville, and Eric Peterson.

The chapel organist, Johann Sebastian Bach, is enraged.  It is bad enough his boss, Duke Wilhelm, stifles his creativity.  Now the bumbling fool has given him a servant he suspects is a spy!  But soon, the temperamental composer recognizes a kindred soul in his new 10-year-old assistant, Frederick.  For both know too well what it is like not to be able to follow your dream.  Set in 1717, Bachs 32nd year, Bachs Fight For Freedom passionately argues that the only master you can serve faithfully is your own heart.
Filmed in Ceske Krumlov in Bohenia in the south of the Czech Republic and with a soundtrack score featuring the Brandenburg Concertos produced by David Devine for Sony Music of New York using ancient instruments with the Slovak Philharmonic conducted by Ondrej Leonard.

Bach has been broadcast in over 100 countries in many languages and is used in numerous Canadian and American elementary and middle school music classes.

==External links==
* 
* 

 

 
 
 
 
 