Basement (2010 film)
{{Infobox film
| name           = Basement
| image          = Basement-2010-film.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Asham Kamboj
| producer       = Ish Jalal Asham Kamboj Terry Stone
| writer         = Ewen Glass
| narrator       = 
| starring       = Danny Dyer Jimi Mistry Emily Beecham
| music          = 
| cinematography = 
| editing        = Peter Davies
| studio         = Gateway Films
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 77 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Basement is a 2010 British horror film, starring Danny Dyer and  written by Ewen Glass about six friends who are lured into a basement for a sinister experiment.

==Plot==
After returning from an anti-war demonstration, Gary (Danny Dyer), Sarah (Kierston Wareing), Saffron (Lois Winstone), Pru (Emily Beecham) and Derek (Jimi Mistry) stop in the country. Derek and Saffron discover a metal hatch in the middle of the forest and they decide to explore inside. Gary, Pru and Sarah search for their missing friends and follow them into the hatch and find themselves locked in when it closes behind them.

==Production and release== straight to DVD and Blu-ray on August 23, 2010.

==Reception==
Critical reception for The Basement has been negative, with BeyondHollywood.com writing that the film could have been "enjoyable trash, though sadly its mind numbing, soul destroying lack of energy or originality drags it back down into the mire."  HorrorNews.net called the film "poorly written", saying that "in general it was just not a very good movie".  The Sun reported that the movie was so ill received at its premiere that "People were even walking out moments after the film started as it was so bad."  Dread Central also reviewed the film, saying it was "an entertainment black hole. There isn’t a single line, performance, camera movement, idea, scene, shot or piece of editing that warrants any kind of praise or attention." 

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 


 