The Web (film)
{{Infobox film
| name           = The Web
| image          = The Web 1947 movie poster.jpg
| alt            = 
| caption        = Theatrical release poster Michael Gordon Jerry Bresler
| screenplay     = William Bowers Bertram Millhauser
| story          = Harry Kurnitz
| starring       = Ella Raines Edmond OBrien
| music          = Hans J. Salter
| cinematography = Irving Glassberg
| editing        = Russell F. Schoengarth
| distributor    = Universal Studios
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} thriller film Michael Gordon and starring Ella Raines, Edmond OBrien, William Bendix and Vincent Price. 

==Plot==
Leopold Kroner (Fritz Leiber, Sr.), formerly of Colby Enterprises, is released after five years in prison for embezzlement. Andrew Colby (Price), claiming that Kroner has threatened him, hires lawyer Bob Regan (Edmond OBrien) as a personal bodyguard. That evening, Regan hears a gunshot from Colbys study and finds Kroner there, apparently trying to kill Colby. Regan kills Kroner when he turns around, pointing the gun at him.

Regan believes Colbys explanation that Kroner had become delusional and threatening, until Regans police buddy Damico (William Bendix) lets on that hes suspicious that Regan murdered Kroner. Kroners daughter Martha Kroner (Maria Palmer) shows up at Regans apartment and tries, but fails, to murder him. She reveals that Colby had invited Kroner to the house that night and Kroner was of sound mind. Regan investigates further, getting information about Kroners embezzlement case from a reporter and Colbys secretary, Noel (Ella Raines). Regan has a friend impersonate one of Colbys associates on the phone to deceive him into providing information about the embezzlement, unknowing that this associate is already long dead.
 John Abbott) with a weapon having Regans fingerprints. The two of them are framed for theft and murder, but Lt. Damico tricks Colby into thinking Charles is still alive. Since Charles would reveal all of Colbys actions, that night Colby tries to sneak down and strangle Charles, only to be caught red-handed.

==Cast==
* Ella Raines as Noel Faraday
* Edmond OBrien as Bob Regan
* William Bendix as Lt. Damico
* Vincent Price as Andrew Colby
* Maria Palmer as Martha Kroner John Abbott as Charles Murdock
* Fritz Leiber (Sr.) as Leopold Kroner
* Howland Chamberlain as James Timothy Nolan
* Tito Vuolo as Emilio Canepa
* Wilton Graff as District Attorney
* Robin Raymond as Newspaper Librarian

==Reception==

===Critical response===
When the film was released The New York Times film critic gave the film a negative review, writing, "Ella Raines and Edmond OBrien, as the lawyer, play their stock roles with competence, and William Bendix plays the lieutenant with a suggestion of, shall we say, retarded intellectual attainment. But hes not nearly as dumb as he makes out and, on the other hand. The Web is not nearly as good as it might have been." 
 Pillow Talk/Texas Across the River/Boston Blackie Goes Hollywood) keeps things breezy and topped off with zesty mustard. Writers Bertram Millhauser and William Bowers keep the story by Harry Kurnitz free of any dull momenta. The pic has the following things going for it: William Bendix is superb playing a smart cop against type, a sassy Ella Raines smoothly swinging her hips is good for the eyes and ears, a slimy Vincent Price as the sinister villain makes your blood boil in an entertaining way, and a rarely seen as slim Edmund OBrien is oafishly prancing around as the good guy lawyer of the people and makes for a likable hero." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 