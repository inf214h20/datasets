Wu Dang (film)
 
 
{{Infobox film
| name = Wu Dang
| image = Wu Dang (film).jpg
| alt = 
| caption = Film poster
| film name = {{Film name| traditional = 大武當之天地密碼
| simplified = 大武当之天地密码
| pinyin = Dà Wǔdāng Zhī Tiāndì Mìmǎ}}
| director = Patrick Leung
| producer = Chan Khan David Wang
| writer = Chan Khan
| starring = Vincent Zhao Yang Mi Louis Fan Dennis To Xu Jiao
| music = Lincoln Lo
| cinematography = Cheung Tung-leung Cheung Ka-fai
| studio = Mei Ah Great Wall Media Mei Ah Film Production 
| distributor = Hua Tianxia Film Distribution Company   Mei Ah Entertainment  
| released =  
| runtime = 
| country = China Hong Kong   
| language = Mandarin
| budget = 
| gross =
}}
Wu Dang is a 2012 Chinese-Hong Kong martial arts fantasy film directed by Patrick Leung, starring Vincent Zhao, Yang Mi, Louis Fan, Dennis To and Xu Jiao. It was first released in mainland China on 6 July 2012.

==Plot== Republican era in China. Tang Yunlong, an archaeology professor, returns from the United States to China and he brings along his daughter, Tang Ning, with him. He meets Paul Chen, an antique seller who deals in stolen artefacts from all over China. A fight breaks out when Tang seizes a treasure map from Chen. Tang defeats Chens men and escapes with the map. The Tangs then make their way to the Wudang Mountains to attend a martial arts contest. Elsewhere, on an aeroplane, a girl called Tianxin fights with other martial artists on board and steals an invitation card to attend the event at Wudang.

At Wudang, it is revealed that Tang Yunlong and Tianxin are there for the same objective — to hunt for seven treasures hidden all over the Wudang Mountains. Tianxin wants only one of the seven, a sword, which she insists belonged to her family. On the other hand, Tang Yunlong is searching for a magic pill, which can cure his daughter of a rare medical illness that killed his wife. While hunting for the treasures, Tang Yunlong and Tianxin have several encounters and confrontations with the Wudang guardians keeping watch over the treasures, as well as with Paul Chen and his men, who are also there for the treasures.

==Cast==
* Vincent Zhao as Tang Yunlong
* Yang Mi as Tianxin
* Louis Fan as Shui Heyi
* Dennis To as Taoist Bailong
* Xu Jiao as Tang Ning
* Paw Hee-ching as Shui Heyis mother
* Henry Fong as Taoist Xie
* Tam Chun-yin as Paul Chen
* Wang Xiao as mute Taoist
* Wang Yuying as Tianxue
* Xiao Xiangfei as Taoist priest
* Liu Jiayu as Taoist nun
* Zhou Yang as housekeeper
* Tian Ruihui as Tong Yan

==Production==
Wu Dang was directed by Hong Kong film director Patrick Leung, who previously directed other films such as La Brassiere (2001) and The Twins Effect II (2004). The action scenes were choreographed by Hong Kong action director Corey Yuen, who also worked on The Twins Effect II with Leung. 

The costumes in the film were designed by Emi Wada, who won the Academy Award for Best Costume Design in 1985. 

Wu Dang marked the first time Yang Mi played a "fighter girl" in a martial arts film. 

Louis Fan fractured his toe while performing a difficult stunt in one scene, but, despite his injury, he insisted on continuing until the shooting ended. 

==Release==
Vincent Zhao and Yang Mi attended a press event in Beijing on 30 May 2012 to promote the film.  Wu Dang was released in mainland China on 6 July 2012 and in Hong Kong on 17 July 2012.

==Reception==
Yangtze River Daily (长江日报) reporter Wan Xuming (万旭明) wrote, "Based on the immense response at the auditions held in Hengdian World Studios, the audience probably regard this film as one that showcases the grandeur of the Wudang Mountains." 

On 16 July 2012, NetEase stated that the plot of Wu Dang not only closely resembles that of Tiandao Mima (天道密码), a novel by Sima Changxiao (司马长啸) published in August 2011, but its Chinese title (Da Wudang Zhi Tiandi Mima) is also very similar to that of the novel. This led to suspicions of plagiarism. 

==References==
 

==External links==
*  
*  
*     on moviemtime.com
*     on Sina.com

 
 
 
 
 
 
 
 
 