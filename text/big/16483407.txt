Crac
 CRAC}}
{{Infobox Film
| name = Crac
| image =
| image_size =
| caption =
| director = Frédéric Back
| producer = Frédéric Back
| writer = Frédéric Back
| narrator =
| starring =
| music = Normand Roger
| cinematography =
| editing = Jacques Leroux
| studio = Société Radio-Canada
| distributor =EastWest Entertainment (2005) (USA, DVD)
| released = 1981
| runtime = 15 minutes
| country =
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
}} 1981 Animation|animated short film produced, written and directed by Frédéric Back. The story follows the experiences of a rocking chair, from its creation from a tree through its time as a member of a Canadian farming family.

Crac won the 1981 Academy Award for Best Animated Short Film. {{cite web
 | title = 1981 (54th): SHORT FILM (Animated)
 | work = Academy Awards Database
 | publisher = Academy of Motion Picture Arts and Sciences
 | url = http://awardsdatabase.oscars.org/ampas_awards/DisplayMain.jsp?curTime=1206192847806
 | accessdate = 2008-03-22}} 

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 