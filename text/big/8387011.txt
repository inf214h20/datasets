As You Desire Me (film)
{{Infobox film
| name           = As You Desire Me
| image          = As you desire me.jpeg
| caption        = Original film poster
| director       = George Fitzmaurice
| producer       = George Fitzmaurice Irving Thalberg
| writer         = Gene Markey Luigi Pirandello (play)
| starring       = Greta Garbo  Melvyn Douglas   Erich von Stroheim   Owen Moore  Hedda Hopper
| music          =
| cinematography = William H. Daniels
| editing        = George Hively
| distributor    = Metro-Goldwyn-Mayer
| released       = May 28, 1932
| runtime        = 70 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}

As You Desire Me is a 1932 film adaptation of the play by Luigi Pirandello released by Metro-Goldwyn-Mayer. It was produced and directed by George Fitzmaurice with Irving Thalberg as co-producer. The adaptation was by Gene Markey, the cinematography by
William H. Daniels, the art direction by Cedric Gibbons and the costume design by Adrian (costume designer)|Adrian.

The film stars Greta Garbo and Melvyn Douglas with Erich von Stroheim, Owen Moore and Hedda Hopper. Its running time is less than 71 minutes, making it the shortest of all Garbos Hollywood films.

This is also the only film in which Garbo appears as a blonde. It made $1,362,000. 

==Plot==
Budapest bar entertainer Zara (Greta Garbo) is a discontented alcoholic who is pursued by many men but lives with novelist Carl Salter (Erich von Stroheim). A strange man called Tony (Owen Moore) shows up on Salters estate claiming that Zara is actually Maria, the wife of his close friend Bruno. Maria, Tony claims, had her memory destroyed during a World War I invasion 10 years ago. Zara doesnt remember but leaves with Tony to Salters dismay. Bruno, now an officer in the Italian Army, tries to coax Marias memory back on his large estate. No one is really sure if Zara is Maria, and when Salter shows up with a mental case from Trieste that he claims is the real Maria, everyone on Brunos estate is desperately searching for the truth.

The narrative structure of As You Desire Me was used by Marcelle Maurette for the play Anastasia, which was translated into English by Guy Bolton, then later fashioned into a 1956 film adaptation, penned by Bolton and Arthur Laurents.  The dual plot of amnesia victim who may or may not be someone, and the income to be gained by proving the identity were the key plot devices used.

==Cast (in credits order)==
*Greta Garbo as Zara / Maria
*Melvyn Douglas as Count Bruno Varelli
*Erich Von Stroheim as Carl Salter
*Owen Moore as Tony Boffie
*Hedda Hopper as Ines Montari
*Rafaela Ottiano as Lena
*Warburton Gamble as Baron
*Albert Conti as Captain
*William Ricciardi as Pietro
*Roland Varno as Albert

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 