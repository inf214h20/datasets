The Eagle (1959 film)
{{Infobox film
| name           = Orzeł
| image          = 
| caption        = 
| director       = Leonard Buczkowski
| producer       = 
| writer         = Leonard Buczkowski, Janusz Meissner
| starring       = 
| music          = Waldemar Kazanecki
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

The Eagle ( ) is a 1959 Polish war film directed by Leonard Buczkowski. It is based on the true story of the Polish World War II submarine  . Since the real Orzeł was sunk in the war, to assure authenticity her role was played by her sister ship, the  . The film was entered into the 1st Moscow International Film Festival.   
Basically the story is freely based on the so-called Tallinn-episode of the Polish submarine Orzel, which in Sep. 1939 entered the harbour of then still neutral Estonia to leave her commanding officer with symptoms of illness. The Estonian authorities tried to intern the sub under the pressure from the Nazi-Germany, but the Orzel escaped.
==Cast==
* Aleksander Sewruk as Commander Henryk Kłoczkowski 
* Wieńczysław Gliński as Captain Jan Grabiński
* Jan Machulski as Lieutenant Pilecki
* Roland Głowacki as Lieutenant Roland
* Bronisław Pawlik as Mate Rokosz

== See also ==
* Cinema of Poland
* List of Polish language films
* Submarine films

==References==
 

==External links==
*  

 
 
 
 
 
 
 