Dreams for Sale: Lehigh Acres and the Florida Foreclosure Crisis
 

{{Infobox film
| name           = Dreams for Sale: Lehigh Acres and the Florida Foreclosure Crisis
| image          = DreamsforSaleAD.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Official movie poster for Dreams for Sale
| director       = Raymond Schillinger
| producer       = Raymond Schillinger (producer) Thomas Keenan (co-producer) Luke Kosar (co-producer)
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Raymond Schillinger
| editing        = Raymond Schillinger
| studio         = 
| distributor    = 
| released       =  
| runtime        = 34 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Dreams for Sale: Lehigh Acres and the Florida Foreclosure Crisis is a 2010 American documentary film directed by Raymond A. Schillinger.  The film tells the story of Lehigh Acres, a community located in Lee County, Florida that has been identified as one of the "slums of the future" by mainstream press.  It features interviews with residents of Lehigh Acres and surrounding areas, who describe their experiences relating to the history of Lehigh Acres. 

==Production==
The production team for Dreams for Sale consisted of Raymond A. Schillinger (director/editor), Luke Kosar (assistant producer), and Thomas Keenan (assistant producer). Production for Dreams for Sale began in March 2009, and principal shooting was complete by June 2009. Editing took place over the year following shooting. The film was released online in late November 2010 in three parts on YouTube. The online cut of the film is 34 minutes in total length. 

==References==
 

==External links==
*  
* http://consumerist.com/2011/01/dreams-for-sale-lehigh-acres.html

 
 
 
 
 
 
 
 
 
 

 