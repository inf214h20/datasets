Faces of Death II
{{Infobox film
| name           = Faces of Death II
| image          = Faces of Death II.jpg
| caption        =
| director       = John Alan Schwartz
| producer       = William B. James
| writer         = John Alan Schwartz
| starring       =
| music          = Gene Kauer
| cinematography = Peter B. Good
| editing        = James Roy Henri Ivon Simone
| distributor    = Gorgon Video
| released       =  
| runtime        = 85 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}
Faces of Death II is the first sequel to the 1978 mondo film Faces of Death. Like its predecessor, the film was written and directed by John Alan Schwartz (as "Alan Black" and "Conan Le Cilaire" respectively). Schwartz puts in another cameo appearance, this time as the wounded criminal in front of the drug store. Dr. Francis B. Gröss (portrayed by Michael Carr) again narrates the proceedings.
 Kenny Powers to jump a rocket-powered car over the Saint Lawrence River in Canada and land over one mile away in New York. Also featured nearly in its entirety is the 1980 boxing match between Johnny Owen and Lupe Pintor, with Owen being knocked out and later dying from the injuries sustained in the match.

==External links==
*  
*  

 

 
 
 
 
 
 
 

 
 