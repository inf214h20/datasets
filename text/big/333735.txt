Pirates (2005 film)
{{Infobox film 
| name           = Pirates
| image          = Pirates 2005 film.jpg
| caption        = DVD cover Joone
| Joone
| starring       = {{Plainlist|
* Jesse Jane
* Carmen Luvana Janine
* Teagan Presley Devon
* Jenaveve Jolie
* Evan Stone
}}
| released       =  
| cinematography = Joone
| editing        = Joey Pulgadas
| runtime        = 129 minutes English
| budget         = $10,000,000
}}

Pirates (also known as Pirates XXX) is a  .

The producer stated in an interview that Pirates was the most expensive pornographic film made to date,  with a reported budget of up to $10 million United States dollar|U.S. dollars   and featuring unusually high production values for a pornographic film.

==Plot== first officer Jules believes in him. When they save a young woman named Isabella from drowning, she tells them that her husbands ship has been destroyed by the feared Captain Victor Stagnetti and his crew of cutthroat pirates.

Reynolds and his crew go hunting for Stagnetti, who tries to find a map that leads to a powerful secret on an island somewhere in the Caribbean Sea. Stagnetti finds the secret "staff" unlocked by Isabellas husband Manuel.

After the crew escapes the spawn of darkness summoned by Stagnetti, their ship engages Stagnettis in battle, ending Stagnettis reign as a pirate.

==Cast==
*Jesse Jane as Jules
*Carmen Luvana as Isabella
*Janine Lindemulder as Serena Devon as Madelyn
*Teagan Presley as Christina
*Evan Stone as Captain Edward Reynolds Tommy Gunn as Captain Eric Victor Stagnetti
*Austyn Moore as Angelina
*Jenaveve Jolie as the Pirate Dancer
*Steven St. Croix as Marco
*Kris Slater as Manuel Venezuela
*Nhan as Wu Cho

==Production==
The production was shot using high definition digital video cameras and featured over 300 special effects shots.  It also included an original music score, later released on a separate soundtrack CD, and was mastered in Dolby Digital 5.1 surround sound. Several scenes were shot on board the Bounty II, a replica of HMS Bounty, in St. Petersburg, Florida.  The owners of the ship were not aware of the true nature of the film as they were advised that the film being made was a "Disney-type pirate film for families",  a mistaken notion that would continue in video stores after its release. 

The film was initially released as a three-disc DVD set (the movie on a standard video DVD, the movie again in a high definition (720P) Windows Media format,  and a special features disc) priced as high as $70. An edited R-rated version of the film was released on DVD on July 11, 2006.  Digital Playground also released an Original Motion Picture Soundtrack CD, a rarity among adult video releases.

The film was also released on Blu-ray Disc (containing only xxx version) and HD DVD as one of the first High-definition video|high-definition adult releases on an optical disc format.

== Sequel == Las Vegas, Nevada. 
 Belladonna along with Jesse Jane, Evan Stone and many other performers from the first movie, and that it would be released in September 2008. 

The second movie was released on September 26, 2008, under the title  .

==Reviews==
RogReviews called it "the most talked about adult movie of the year".   

The film set a record by winning 11 AVN Awards  (see section below).

The New York Times described the film as "a relatively high-budget story of a group of ragtag sailors who go searching for a crew of evil pirates who have a plan for world domination. Also, many of the characters in the movie have sex with one another." 

==Awards==
AVN Awards:
* Best Video Feature
* Best DVD
* Best Director – Video (Joone (director)|Joone)
* Best Actress – Video (Janine Lindemulder)
* Best High-Definition Production
* Best All-Girl Sex Scene – Video (Janine Lindemulder & Jesse Jane)
* Best Special Effects
* Best Actor – Video (Evan Stone)
* Best Music Tommy Gunn)
* Best On-Line Marketing Campaign

XRCO Awards:
* Best Release (2006)   
* Epic (2006)   

Adult DVD Empire Awards:
* 2005 Editors Choice
* 2005 Best Overall DVD
* 2005 Best DVD Menu Design
* 2005 Best DVDpi Video Quality
* 2005 Best DVD Audio Quality
* 2005 Viewers Choice

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 