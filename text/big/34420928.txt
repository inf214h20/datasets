Mannin Maindhan
{{Infobox film
| name = Mannin Maindhan
| image = Mannin Maindhan DVD cover.svg
| caption = DVD cover
| director = Rama Narayanan
| producer = Rama Narayanan
| story =
| screenplay = Karunanidhi
| starring =  
| music = Bharathwaj
| cinematography = N. K. Viswanathan
| editing = Raj Keerthy
| studio = Azhagar Films
| distributor =
| released =  
| country = India
| runtime = 145 min
| language = Tamil
}}
 2005 Cinema Indian Tamil Tamil film, Suhain lead roles. Sathyaraj plays an extended cameo role. The film, a remake of successful Telugu film Yagnam (2004 film)|Yagnam.  The film was produced by Rama Narayanan, had musical score by Bharathwaj and was released on 4 March 2005 to negative reviews.     

==Plot==
Bhairavamurthy (Manoj K. Jayan) and Gajapathy (Ponnambalam (actor)|Ponnambalam) are two landlords in a village who fight with each other. Bhairavamurthys wife and his car driver get killed by Gajapathys henchmen.

Kadhir (Sibiraj), the car drivers son, decided to take revenge on Gajapathy one day and grew him up in Bhairavamurthys house.

Amutha (Suhasini (Telugu actress)|Suha), Bhairavamurthys daughter, returns to the village from her college and she falls in love with Kadhir. Bhairavamurthy decides, to join his hands with Gajapathy, to kill his faithful henchman.

The couple elopes from the house and runs around village. Pratap (Sathyaraj), an honest cop, saves them. The henchmen, who worked with the landlords, lay down their arms and support the couple. Bhairavamurthy and Gajapathy decide to kill both Kadhir, but they kill each other.

==Cast==
* Sibiraj as Kadhir Suha as Amudha
* Manoj K. Jayan as Bhairavamurthy Ponnambalam as Gajapathy
* Sathyaraj as Pratap (extended cameo)
* Vadivelu as Quarter Vadivelu
* Suman Setty as Dheena
* Bala Singh as Joseph
* Indhu as Josephs wife
* Kumari Muthu as a police constable Thyagu as a police constable
* Dharmavarapu Subramanyam as Raghavan (guest appearance)

== Soundtrack ==
{{Infobox album |  
 Name = Mannin Maindhan |
 Type = soundtrack |
 Artist = Bharathwaj |
 Cover = |
 Released = 2005 |
 Recorded = 2005 | Feature film soundtrack |
 Length = 22:03 |
 Label = |
 Producer = Bharathwaj |
 Reviews = |
 Last album = |
 This album = |
 Next album = |
}}

The film score and the soundtrack were composed by film composer Bharathwaj. The soundtrack, released in 2005, features 5 tracks with lyrics written by Karunanidhi, Vaali (poet)|Vaali, Na. Muthukumar, Vairamuthu and Snehan.  Indiaglitz.com said : "Bharadwajs music is very flat with none of the song sounding impressive". 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || "Andravin Ponnu" || Mano (singer)|Mano, Reshmi || 4:04
|- 2 || "Idhu Ladies Hostel" || Jay Laxmi || 4:41
|- 3 || "Kannin Manipola" || K. S. Chitra, Master Sathiya || 3:41
|- 4 || "Kilakku Killakku" || Muralidharan || 5:15
|- 5 || Tippu || 4:22
|}

==Reception==
Indiaglitz.com stating that the film was "more dusty than being earthy". 

==References==
 

 

 
 
 
 
 
 