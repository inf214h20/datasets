Kaddu Beykat
{{Infobox film
| name           = Kaddu Beykat
| image          =
| image_size     =
| caption        =
| director       = Safi Faye
| producer       =
| writer         = Safi Faye {{cite web
| title = Production Credits
| publisher = Allmovie
| url = http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:145308~T3
| accessdate =2008-05-10 }} 
| narrator       =
| starring       = Assane Faye Maguette Gueye
| music          =
| cinematography =
| editing        = André Davanture 
| distributor    =
| released       = June 1976 (Berlin) October 20, 1976 (France)
| runtime        = 90&nbsp;minutes
| country        = Senegal French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1975 Senegalese film directed by Safi Faye. It was the first feature film made by a Sub-Saharan African woman to be commercially distributed and brought international recognition for its director. Ukadike, p.30  Spaas, p.185  Centred on a romance, it chronicles the daily lives of people in a rural Senegalese village.

==Plot== groundnuts and as a result, Ngor cannot afford the bride price for Columba. He goes to Senegals capital city, Dakar, to try to earn more money and is exploited there. He returns to the villagers and shares his experiences of the city with the other men. The story, which shows the daily lives of the villagers, is told in the form of a letter to a friend from a villager, voiced by Faye. Russell, p. 59 

==Cast==
* Assane Faye as Ngor
* Maguette Gueye as Columba

==Background== French Ministry documentary and colonial farming practices and government policies which have encouraged single-crop farming of cash crops for export, in some cases leading villages further into poverty.   The film is dedicated to Fayes grandfather who features in the film, and who died 11 days after filming ended. 

==Distribution and reception==
Kaddu Beykat played at the 1976 Berlin International Film Festival where it won the FIPRESCI Prize Petrolle, p.177  and the OCIC Award. It also won the Georges Sadoul Prize and an award at the Panafrican Film and Television Festival of Ouagadougou.  {{cite web
| title = Safi Faye
| publisher = jiffynotes.com
| year = 1998
| url = http://www.jiffynotes.com/a_study_guides/book_notes/ewb_05/ewb_05_02108.html
| accessdate =2008-05-10 }}  It was released in France on October 20, 1976. It was initially banned in Senegal. {{cite web
| title = Africa Beyond
| publisher = BBC
| year = 2007
| url = http://www.bbc.co.uk/africabeyond/events/200703/
| accessdate =2008-05-10 }} 

==References==

===Notes===
 

===Sources===
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
*  
*  

 
 
 
 
 
 
 
 