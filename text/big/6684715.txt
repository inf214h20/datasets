Pudhiya Geethai
{{Infobox film
| name = Pudhiya Geethai
| image = Puthiya Geethai.jpg
| director = K. P. Jagannath|K.P. Jagan
| writer = K. P. Jagannath|K.P. Jagan Vijay Meera Jasmine Ameesha Patel Kalabhavan Mani Karunas
| producer = Viswaas Sundar
| music = Yuvan Shankar Raja   (Songs)    Karthik Raja   (Background Score)  
| cinematography = Ramesh Krishna Rithuu Ramesh
| editing = V. T. Vijayan
| studio = Viswaas
| distributor = 
| released =  
| runtime = 
| language = Tamil
| country = India
}} Indian Tamil Tamil film written and directed by newcomer K. P. Jagannath|K.P. Jagan, starring Vijay (actor)|Vijay, Meera Jasmine and Ameesha Patel in lead roles. Notably, this film is Patels first and remains her only Tamil film till date. The film, which has music scored by brothers Yuvan Shankar Raja and Karthik Raja, released on 8 May 2003 to mixed reviews but was a hit at box office

The film tells the tale of a man who has positive attitude in his life and a never-give-up attitude. The story starts as if the hero at his birth and an astrologer tells his wife that the man will die at the age of 27. The story tells that whether will the hero die at 27 or not.

==Plot==
Sarathy (Vijay (actor)|Vijay) shines in every fact of life and be a role model to every youngster was predicted by an astrologer who also predicted  that his life will end when he is 27. Without knowing this, Sarathy enjoys his life. Suji (Meera Jasmine) is his close friend while Jo (Amisha Patel), a photojournalist, adores Sarathy for his attitude and tricks Suji by saying she loves him and plays with her emotions. The incidents that follow with the accord to the astrologers prediction forms the rest of the story.The power of positive thinking and the power of spoken word has been dramatically portrayed by Sarathys character.

==Cast== Vijay as Sarathy
* Meera Jasmine as Suji
* Amisha Patel as Jo
* Kalabhavan Mani as Reddiyar
* Karunas as Ganesh
* Nassar as Chinnaya / Sarathys Father
* Kasinadhuni Viswanath|K. Viswanath as astrologer
* Sarath Babu as Annamalai
* Ilavarasu as Shekar Sanjeev as Lawrence
* Kalairani as Thaiyalnayagi / Sarathys Mother
* Yogini
* Madhumitha
* Srilekha
* Sadhana

==Production== Vijay in English for her to read.  Before release, the films title was changed to Pudhiya Geethai.

==Soundtrack==
{{Infobox album |  
| Name = Pudhiya Geethai
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Puthiya geethai cd cover.jpg
| Released = 29 March 2003
| Recorded = 2002 Feature film soundtrack
| Length = 
| Label = Five Star Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Aadanthe Ado Type (2003)
| This album = Pudhiya Geethai (2003)
| Next album = Thennavan (2003)
}}

The songs were composed by Yuvan Shankar Raja, while his elder brother Karthik Raja composed the films background score. The audio CD containing songs released on 29 March 2003 and features 6 tracks, the lyrics were penned by Vaali (poet)|Kavignar Vaali, Pa. Vijay, Yugabharathi and Vijay Sagar. "Puthiya Geethai" remains Yuvan Shankars first and only collaboration with Vijay.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricist!! Notes
|- 1 || "Naan Odum Ilaignan" ||   ||
|- 2 || "Mercury Poove" || Nideesh Gopalan,   ||
|- 3 || "Vasiyakaara - I" || Devan, Chitra Sivaraman  || 3:52 || Yugabharathi ||
|- 4 || "Manase" ||   ||
|- 5 || "Vasiyakaara - II" ||   ||
|- 6 || "Annamalai" || Tippu (singer)|Tippu, Devan || 4:52 || Vijay Sagar ||
|}

==Release==
The satellite rights of the film were secured by Sun TV. The film was given a "U" certificate by the Indian Censor Board.

==Reception==
The film earned mostly positive reviews. The critic from The Hindu stated that "the end is predictable, but the conviction with which the climax drives home the message makes it interesting."  Another reviewer stated the film had "nothing new", describing it as "real melodramatic soap-opera that moves hearts of viewers and at the same time gives a message for the audience." 

The film was one more failure at box office for Vijay.  

==References==
 

==External links==
*  

 

 
 
 