Libeled Lady
{{Infobox film 
| name           = Libeled Lady 
| image          = Poster - Libeled Lady 01.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| producer       = Lawrence Weingarten  Jack Conway 
| writer         = Maurine Dallas Watkins Howard Emmett Rogers George Oppenheimer
| starring       =  Jean Harlow William Powell Myrna Loy Spencer Tracy Walter Connolly 
| music          = William Axt
| cinematography = Norbert Brodine
| editing        = Frederick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $603,000  . 
| gross          = $2,723,000 
}}
 Jack Conway.
 remade in 1946 as Easy to Wed with Esther Williams, Van Johnson, and Lucille Ball.

==Plot==
Wealthy Connie Allenbury (Myrna Loy) is falsely accused of breaking up a marriage and sues the New York Evening Star newspaper for $5,000,000 for libel. Warren Haggerty (Spencer Tracy), the managing editor, turns in desperation to former reporter and suave ladies man Bill Chandler (William Powell) for help. His scheme is to maneuver Connie into being alone with him when his wife shows up, so the suit will have to be dropped. Chandler is not married, so Warren volunteers his long-suffering fiancée, Gladys Benton (Jean Harlow), over her loud protests.

Bill arranges to return to America from England on the same ocean liner as Connie and her father J. B. (Walter Connolly). He pays some men to pose as reporters and harass Connie at the dock, so that he can "rescue" her and become acquainted. On the voyage, Connie initially treats him with contempt, assuming that he is just the latest in a long line of fortune hunters after her money, but Bill gradually overcomes her suspicions.

Complications arise when Connie and Bill actually fall in love. They get married, but Gladys decides that she prefers Bill to a marriage-averse newspaperman and interrupts their honeymoon to reclaim her husband. Bill reveals that he found out that Gladys Mexican divorce wasnt valid, but then Gladys tops him. She got a second divorce, and she and Bill are actually man and wife. Fortunately, Connie and Bill manage to show Gladys that she really loves Warren.

==Cast==

*  Jean Harlow as Gladys Benton
*  William Powell as Bill Chandler
*  Myrna Loy as Connie Allenbury
*  Spencer Tracy as Warren Haggerty
*  Walter Connolly as James B. Allenbury
*Charley Grapewin as Hollis Bane, Haggertys boss
*Cora Witherspoon as Mrs. Burns-Norvell, a talkative acquaintance of the Allenburys
*E. E. Clive as Evans, a fishing instructor
*Bunny Beatty as Babs Burns-Norvell, Mrs. Burns-Norvells daughter
*Otto Yamaoka as Ching
*Charles Trowbridge as Graham
*Spencer Charters as the Magistrate
*George Chandler as the Bellhop Billy Benedict as Johnny
*Gwen Lee as the switchboard operator

;Cast notes
*Hattie McDaniel makes a brief appearance as a maid, a role she played with great frequency.

==Production==
 
The film went into production in mid-July 1936 and wrapped on September 1.  Location shooting took place in Sonora, California.  Lionel Barrymore was originally cast as Mr. Allenbury,  and Rosalind Russell was originally considered to play Connie Allenbury.

Harlow and Powell were an off-screen couple, and Harlow wanted to play Connie Allenbury, so that her character and Powells would wind up together.  MGM insisted, however, that the film be another William Powell-Myrna Loy vehicle, as they originally intended. Harlow had already signed on to do the film but had to settle for the role of Gladys Benton. Nevertheless, as Gladys, top-billed Harlow got to play a wedding scene with Powell. During filming, Harlow changed her legal name from her birth name of Harlean Carpenter McGrew Bern Rosson to Jean Harlow. Frank Miller    She would make only two more films before dying at the age of 26 in 1937.

It has been rumored that Loy and Tracy had an affair during the shooting of the film. 

==Reception==
The film was released on 9 October 1936,  and earned $2.7 million at the box office  &mdash; $1,601,000 earned in the US and Canada and $1,122,000 overseas, resulting in a profit of $1,189,000.  

It received an Academy Award nomination as Best Picture in 1937, but lost to The Great Ziegfeld, which also starred William Powell and Myrna Loy.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 

 

 
 
 
 
 
 
 
 