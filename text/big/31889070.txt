A Railway Collision
 
 
{{Infobox film
| name           = A Railway Collision
| image          = A Railway Collision (1900) - yt.webm
| image_size     = 
| caption        = 
| director       = Walter R. Booth
| producer       = Robert W. Paul
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 22 secs
| country        = United Kingdom Silent
| budget         =
}}
 short silent silent drama film, directed by Walter R. Booth and produced by Robert W. Paul. The film depicts a stretch of single-track railway running through mountainous terrain on an embankment above a lake and a yacht. A goods train passes a signal, stops and begins reversing back up the track. A fast express train comes out of a tunnel in front of the goods train, on the same track. The two trains collide head-on and plunge down an embankment.    It was one of a number of sensationalist "trick films" made at Pauls Animatograph Works, his studio in Muswell Hill in north London,  and represents one of only a very small number of surviving films by Paul. 

==Production and premise==
Frederick A. Talbot, writing in 1912, records that "the scene of the accident was a field, in which the scenery was erected with considerable care, and a long length of model railway track was laid down, while the trains were good toy models."    According to Michael Brooke of BFI Screenonline, it "is one of the earliest examples of this technique in practice". Brooke notes that "unlike some of his other films of the period, Booth does not attempt to enhance the effect by intercutting obviously full-scale material, though his successors would undoubtedly have added a shot inside a carriage full of screaming passengers."   

Despite the contrived nature of the scenario and the basic nature of the model work, viewers appear to have found its depiction convincing. Talbot comments that "many people marvelled at Pauls good fortune in being the first on the scene to photograph such a disaster. They were convinced that it was genuine." He calls the film "forty of the most thrilling seconds it is possible to conceive" and praises it for rendering a disaster that was "perfect in its swiftness and wreckage; and the cinematograph film images being less sharp and decisive than those obtained by a hand camera, the illusion was conveyed very convincingly." 

==Reception==
The film was a commercial success and was widely pirated in the United States.  It was only   long, as it was designed to be played with a Kinetoscope, a type of early motion picture exhibition device designed to enable one individual at a time to view a film through a peephole viewer window. However, A Railway Collision proved so popular that it was adopted for the cinematograph, allowing larger audiences to view it.  It had a lasting influence, attracting numerous imitators,  and the technique of using model trains to represent real ones was used in many subsequent British films. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 