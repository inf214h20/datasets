The Babysitter
 
 
{{Infobox film
| name           = The Babysitter
| image          = Babysitterposter.jpg
| caption        = Promotional poster
| director       = Guy Ferland
| producer       = Kevin J. Messick Steve Perry
| writer         = Guy Ferland
| based on       =  
| starring       = Alicia Silverstone Jeremy London Nicky Katt J. T. Walsh
| music          = Loek Dikker
| cinematography = Rick Bota
| editing        = Jim Prior Victoria T. Thompson
| studio         = Spelling Films International
| distributor    = Spelling Entertainment Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}} thriller film directed by Guy Ferland and starring Alicia Silverstone based on the eponymous short story by Robert Coover in his collection Pricksongs and Descants (1969). The film was released direct-to-video in October 1995.

==Plot==
Jennifer (Alicia Silverstone) is a beautiful teenager who is hired to babysit the children of Harry Tucker (J.T. Walsh) and his wife, Dolly Tucker (Lee Garlington), while they attend a party hosted by their friends, Bill Holsten (George Segal) and his wife, Bernice Holsten (Lois Chiles). Harry often fantasizes about Jennifer, while Dolly misinterprets Bills compliments as a sign of attraction and fantasizes about him. Meanwhile, Jennifers ex-boyfriend Jack (Jeremy London), whom she broke up with after he began pressuring her for sex, runs into his estranged troublemaking friend Mark (Nicky Katt), Bill and Bernices son, who once had a fling for Jennifer and still harbors feelings for her. Throughout the night, Harry, Jack and Mark have increasingly racy fantasies about Jennifer.

Jack calls Jennifer and asks to visit her at the Tuckers residence, but she refuses. Mark later steals beer from Bills party, where they run into Harry, who becomes fixated on the notion Jack might go to his house to have sex with Jennifer. Jack and Mark get increasingly drunk and show up uninvited to see Jennifer, but she refuses to let them in. They then spend the rest of the night stalking around the house and spying on her through the window. Meanwhile, Harry gets drunk and falls asleep in his car, where he has a nightmare of Jennifer and Jack having sex, which drives him to rush home and confront them. In his absence, Dolly makes a pass at Bill, who rejects her, but agrees to keep her secret and offers to drive her home.

At the Tuckers residence, Jack and Mark force their way in while Jennifer is taking a bath and, after a tense argument, Mark knocks Jack unconscious and attempts to rape Jennifer, who runs out of the house. Mark pursues her and ends up being fatally run over by Harry, who is arrested for drunk driving just as Bill and Dolly arrive and hear about the accident. Before being escorted home, Jennifer confronts Jack, who is being questioned by the police, and asks him, "What were you thinking?" before leaving an ashamed and guilt-stricken Jack behind.

==Cast==
* Alicia Silverstone as Jennifer
* Jeremy London as Jack
* Nicky Katt as Mark
* J. T. Walsh as Harry Tucker
* Lee Garlington as Dolly Tucker
* Lois Chiles as Bernice Holsten
* George Segal as Bill Holsten
* Ryan Slater as Jimmy
* Brittany English Stephens as Bitsy
* Tuesday Knight as Waitress

==Reception==
The Babysitter received negative reviews; as of February 2013, it holds a 29% "rotten" rating on review aggregate website Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 