Sukiyaki Western Django
{{Infobox film
| name           = Sukiyaki Western Django
| image          = SUKIYAKI WESTERN DJANGOposter-200.jpg
| image_size     = 215px
| alt            = 
| caption        = Japanese release poster
| director       = Takashi Miike
| producer       = Masato Ôsaki
| writer         = Masaru Nakamura Takashi Miike
| starring       = Hideaki Ito Kōichi Satō Yusuke Iseya Masanobu Ando Masato Sakai Yoji Tanaka Renji Ishibashi Sansei Shiomi Takaaki Ishibashi Shun Oguri Quentin Tarantino Yutaka Matsushige Yoshino Kimura Teruyuki Kagawa Kaori Momoi
| music          = Kôji Endô
| cinematography = Toyomichi Kurita
| editing        = Taiji Shimamura
| studio         = Sedic International Geneon Universal Entertainment Sony Pictures Entertainment Dentsu TV Asahi Shogakukan A-Team Nagoya Broadcasting Network Tokyu Recreation
| distributor    = Sony Pictures Entertainment
| released       =  
| runtime        = 121 minutes
| country        = Japan
| language       = English
| budget         = $3.8 million
| gross          = $2,725,258
}} Japanese Western Japanese dish sukiyaki, as well as Sergio Corbuccis spaghetti western film Django (1966 film)|Django. It also takes inspiration from the "Man with No Name" stock character variously used in the spaghetti western genre but most notably in the Dollars Trilogy|Dollars trilogy by Sergio Leone (initially inspired by Akira Kurosawas jidaigeki film Yojimbo (film)|Yojimbo). The film stars Hideaki Ito, Kōichi Satō, Yusuke Iseya, Masanobu Ando, Masato Sakai, Yoji Tanaka, Renji Ishibashi, Sansei Shiomi, Takaaki Ishibashi, Shun Oguri, Quentin Tarantino, Yutaka Matsushige, Yoshino Kimura, Teruyuki Kagawa and Kaori Momoi.
 Genji and Heike gangs face off in a town named "Yuta" in "Nevata", when a nameless gunman comes into town to help a prostitute get revenge on the warring gangs. The film contains numerous references both to the historical Genpei War and to Wars of the Roses, as well as the films Yojimbo and Django.

The original version of Sukiyaki Western Django had a running time of 121 minutes (2 hours and 1 minute) when it first premiered on 5 September 2007 at the Venice Film Festival and was released on 15 September 2007 in Japan. This was the version shown in Japanese cinemas and received mixed reviews from critics. For the North American premiere at the New York Asian Film Festival on 1 July 2008, Miike had the film edited down to 98 minutes (1 hour and 38 minutes). This was the version released outside of Japan.

==Plot==
A lone gunman travels to the town of Yuta, which is run by the warring clans of the white-colored Genji and red-colored Heike. After ignoring requests from both clans to join them, he is given shelter by a woman named Ruriko, who takes care of her mute grandson Heihachi. Ruriko tells the gunman that many years ago, the town prospered in gold mining until both clans fought over the gold and drove away the population. The Heike-aligned sheriff tells the gunman that in the midst of the chaos, a Heike man named Akira married a Genji woman named Shizuka and lived peacefully with their son Heihachi, until Heike leader Kiyomori killed Akira in cold blood, rendering Heihachi mute from the trauma. Seeking protection for her son, Shizuka became a prostitute for the Genji. Since then, Heihachi has been tending to a trio of red and white roses, waiting for the day they bloom.

Later that day, the gunman wins a challenge from the Genji henchman Yoichi to have Shizuka for the night. Before he proceeds with her, he is told by Genji leader Yoshitsune that he is reminiscent of the legendary female gunslinger Bloody Benten. Later, Shizuka warns the gunman that Yoshitsune sent some men to retrieve a new weapon for Yoichi to use on him. He tells Shizuka to take her son and leave town tomorrow. The next morning, following a tip-off from Shizuka, the sheriff informs Kiyomori of the Genjis plans. The Heikes ambush the wagon, with Kiyomori acquiring a Gatling gun stored inside a coffin. Meanwhile, as the Genjis race toward the wagon raid, Ruriko, Shizuka and Heihachi are fleeing from town when Shizuka runs back to save the roses. She is mortally shot through the heart by Yoichi. The gunman attempts to intervene, but is forced to drop his guns before being tortured by the Genji thug. Rurikos servant Toshio suddenly appears and throws a gun at her before she shoots and kills Yoichi and his henchmen, revealing herself to be Bloody Benten. In retaliation for the wagon raid, the Genjis destroy the Heikes fortress.
 under his left sleeve.

After burying their loved ones, the gunman takes a fistful of gold from the treasure chest, telling Heihachi that the rest is his. As he rides off through the snow, Heihachi looks at the roses and slowly utters, "Love". The ending text reveals that a few years later, Heihachi travels to Italy and becomes the gunslinger known as "Django". No one knows if the roses have bloomed.

==Cast==
*Hideaki Ito as the Gunman Kiyomori
*Yusuke Yoshitsune  Yoichi  Benkei
*Shun Oguri as Akira Shigemori
*Yoshino Shizuka
*Teruyuki Kagawa as Sheriff Hoanka
*Kaori Momoi as Ruriko
*Yutaka Matsushige as Toshio
*Renji Ishibashi as Mura Munemori
*Toshiyuki Nishida as Piripero 
*Quentin Tarantino as Piringo
*Ruka Uchida as Heihachi

==Soundtrack==
The films soundtrack composed by Kôji Endô was released on CD on September 5, 2007. It features  , a Japanese remake of the original Django theme song by veteran enka singer Saburō Kitajima. 

==Western critical reception==
When Sukiyaki Western Django premiered as part of the "Midnight Madness" program at the Toronto International Film Festival, it received mixed reviews.  Cam Lindsay of Exclaim! magazine wrote admiringly:
 "The fast-paced action is well staged on a set that borrows from both western and samurai traditions; Miike mixes both good old gunplay (a Gatling gun that’s housed in the original film’s iconic coffin) and martial arts swordplay, which intermingle cohesively until the last fight. Perhaps the most remarkable aspect of Miike’s western is his decision to use a Japanese cast to speak English. Supported by English subtitles, it’s a peculiar choice that at first feels like a novelty, only to fade into the film’s absorbing environment.

Sukiyaki Western Django feels very much like a genuine western, and with it Miike demonstrates his mastery of working a genre film until it becomes a creation of his very own."  

On the other end of the spectrum, Will Sloan of Inside Toronto wrote:
 "This is a crazy, fast-paced spectacle of a movie, with some stunning action scenes and gorgeously colourful production design. The problem is, it’s an empty spectacle. Miike pays homage to the spaghetti westerns of Sergio Leone and Sergio Corbucci, but forgets that those directors genuinely loved the kitschy pop culture they emulated instead of regarding it with smug superiority. Kill Bill was a comic book, yes, but Tarantino allowed his actors room to create characters the audience could care about, while Miike, by having his cast speak awkward English, is perversely trying to keep their characters two-dimensional and keep the audience distant...Ultimately, Sukiyaki Western Django is an exhausting experience. This is not a film you become involved in – it isn’t funny or engaging. Rather, it’s one that you’re supposed to watch with a cool, hip sense of ironic detachment, sitting in the audience and saying to yourself, “Aren’t I cool for laughing at this?” How could anyone enjoy such a self-conscious time at the movies?"  

The film is currently rated at 56% on Rotten Tomatoes, based on 52 reviews with the consensus as "Inventive and off-kilter, the newest feast from J-Horror director Takashi Miike is super-sensory, self-referential and somewhat excessive." 

==Manga==
On June 8, 2007, a manga adaptation by Kotobuki Shiriagari began serialization on Shogakukans Big Comic Superior.

==See also==
* The Tale of the Heike

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 