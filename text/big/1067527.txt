Infernal Affairs II
 
 
 
{{Infobox film
| name           = Infernal Affairs II
| image          = Infernal Affairs II.jpg
| caption        = Theatrical release poster
| director       = Andrew Lau Alan Mak
| film name =  II
 | simplified     =  II}}
| producer       = Andrew Lau
| writer         = Felix Chong Alan Mak Anthony Wong Eric Tsang Carina Lau Shawn Yue Edison Chen Francis Ng Hu Jun Chapman To
| music          = Chan Kwong-wing
| cinematography = Andrew Lau Ng Man Ching Danny Pang Curran Pang Media Asia Raintree Pictures Eastern Dragon Film Co. Basic Pictures
| distributor    = Hong Kong:    (DVD)  Dragon Dynasty  (DVD) 
| released       =  
| runtime        = 119 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = Hong Kong dollar|HK$24,919,376
}}
 II|s= II|j=Mou4 Gaan3 Dou6 Ji6|p=Wú Jiān Dào Er}} 2003 Cinema Hong Kong thriller film directed by Andrew Lau and Alan Mak. 
 Anthony Wong, Tony Leung, who played the central roles in the original, appear in the film, as they are replaced by the younger versions played by Chen and Yue, respectively. The events of the film take place from 1991 to 1997.

==Plot==
 Anthony Wong) mole within triad boss, Ngai Kwun. Lau is later greeted by Hons wife, Mary (Carina Lau), who casually ascertains whether he has any reservations about his mission for Hon. While giving him cash, Mary advises Lau to maintain a low profile. She also confesses that she was the person who ordered the hit on Ngai Kwun, admitting that Hon has no knowledge of this transgression and urges Lau to remain silent. Mary wants Hon to replace Ngai Kwun as the triad boss.

Meanwhile, instructors at the police academy discover that Chan Wing-yan (Shawn Yue), a promising but troubled cadet, is the half-brother to Ngai Kwuns heir, Ngai Wing-hau (Francis Ng); he is subsequently discharged from the force. Chan is later approached by Wong, who asks him why he wants to be a cop; Chan replies, "I want to be a good guy." Wong subsequently makes Chan into an undercover agent for the police, sending him to prison to get close to one of Hons henchmen, "Crazy" Keung (Chapman To). Meanwhile, Ngai Wing-hau takes his late fathers place as triad boss; he is the only Ngai child directly involved in the family business. With Ngai dead, four other triad bosses, known as the "Big Four", dismiss Ngai and debate on whether to pay their tithe to his family. However, Ngai blackmails with his knowledge of their mutual betrayals. Hon acts as an agent provocateur for Ngai in this affair.
 Thai cocaine racket. Meanwhile, Hon leaks information about criminal dealings to Lau, who is able to apprehend many local gangsters and earn a promotion in rank.

During Ngais next drug deal, a  .

In 1997, Lau is picked as one of the officers to preside over the ceremony signifying Transfer of sovereignty over Hong Kong|Britains handover of Hong Kong to China. Ngai attempts to enter politics, but his support disintegrates after Hon betrays him to the police. Wong brings Hon back to Hong Kong under witness protection, but Ngai manages to kidnap his family in retribution. However, during the confrontation between Ngai and Hon, it is revealed that Hons Thai associates are also holding Ngais family hostage in Hawaii and that the woman being held hostage is a decoy. Wong arrives with a task force and guns down Ngai, who dies in Chans arms. Moments before succumbing to his wound, Ngai discovers the wire in Chans jacket and realizes that his half-brother is an undercover cop.

Hons tactics against Ngai lead to a falling out between him and Wong; shortly after their final meeting, Ngais entire family is murdered. The pieces are set in place for the first film: Hon goes down the dark path of replacing Ngai as the main triad boss, becoming Wongs new foe; Lau is a police inspector and Hons mole; Chan is forced to remain undercover, returning to join Hons triad. As the handover ceremony takes place, Hon sheds tears over the loss of Mary before hosting a party. Back at police headquarters, Lau handles a case involving a young woman, coincidentally also called Mary, who becomes his wife in Infernal Affairs.

==Cast== Anthony Wong as Wong Chi-shing (黃志誠; Wong Chi Shing), a police inspector who aims to take down the Ngai family triad.
* Eric Tsang as Hon Sam (韓琛; Hon Sum), a member of Ngais triad.
* Carina Lau as Mary, Hon Sams wife.
* Shawn Yue as Chan Wing-yan (陳永仁; Chan Wing Yan), Ngai Wing-haus half-brother and an undercover cop.
* Edison Chen as Lau Kin-ming (劉健明; Lau Kin Ming), Hon Sams mole in the police force.
* Francis Ng as Ngai Wing-hau (倪永孝; Ni Wing Hao), the boss of the Ngai family triad.
* Hu Jun as Luk Kai-cheung (陸啟昌; Luk kai Chung), a police superintendent who is Wongs close friend and partner.
* Joe Cheung as Ngai Kwun (倪坤; Ni Kun), Ngai Wing-haus father who was assassinated by Lau.
* Henry Fong as Gandhi (甘地; Gandi), one of the Big Four.
* Peter Ngor as Negro (黑鬼; Black Ghost), one of the Big Four.
* Arthur Wong as Kwok-wah (國華; Kwok Wa), one of the Big Four.
* Teddy Chan as Man-ching (文拯; Man cheng), one of the Big Four.
* Chiu Chung-yu as Mary, the girl Lau meets in the police station at the end of the film.
* Phorjeat Keanpetch as Sunny, the Thai drug lord who tried to kill Hon Sam.
* Ye Shipin as socialite at 1997 party
* Tay Ping Hui as Ngai Wing-haus lawyer
* Roy Cheung as Law Kai-yin (羅繼賢; Law kai Yin), an undercover cop in Ngais triad.
* Liu Kai-chi as Uncle John (三叔; Uncle Third), Ngai Kwuns younger brother.
* Chapman To as "Crazy" Keung (傻強; Silly Keung), Chans friend and fellow triad member.
* Hui Kam-fung as principal of police cadet school
* Alexander Chan as Ngai Wing-yee (倪永義; Ni Wing Yi), Ngai Wing-haus younger brother.
* Andrew Lin as Ngai Wing-chung (倪永忠; Ni Wing Chung), Ngai Wing-haus elder brother.
* Kara Hui as Ngai Wing-haus elder sister Wan Chi-keung as Superintendent Leung (梁Sir; Leung Sir), Wongs superior.
* Chan Charoenwichai as Paul, the Thai drug dealer who became friends with Hon Sam.

==Music== Wong Ka-keung, Yip Sai-wing, and performed by the band Beyond (band)|Beyond.

==Reception==
The film was highly anticipated prior to its release due to the success achieved by Infernal Affairs. However, the general response to the film was mixed. 

==Box office==
The film grossed HK$24,919,376 {{cite web 
| url = http://hkmdb.com/db/movies/view.mhtml?id=10370&display_set=eng
| title = Infernal Affairs II (2003) 
| accessdate = 2009-07-05
| publisher = HKMDB.com}}  — big by 2003 Hong Kong standards, but only about half of the originals earnings.

==Awards== 2003 Hong Kong Film Awards, it could not match its predecessors success. The film won only one award, Best Original Film Song, for the song "長空" (performed by Cantopop band Beyond (band)|Beyond).  The film won the Best Film award at the Hong Kong Film Critics Society Awards.

23rd Hong Kong Film Awards
*Won: Best Original Film Song (Wong Ka Keung, Yip Sai Wing, Beyond)
*Nominated: Best Film (Andrew Lau)
*Nominated: Best Director (Andrew Lau, Mak Siu-fai)
*Nominated: Best Screenplay (Mak Siu-fai, Chong Man-keung)
*Nominated: Best Actor (Francis Ng)
*Nominated: Best Actress (Carina Lau)
*Nominated: Best Supporting Actor (Chapman To)
*Nominated: Best Supporting Actor (Liu Kai-chi)
*Nominated: Best Cinematography (Andrew Lau, Ng Man-ching)
*Nominated: Best Editing (Curran Pang, Danny Pang)
*Nominated: Best Original Film Score (Chan Kwong-wing)
*Nominated: Best Sound Effects (Kinson Tsang)

10th Hong Kong Film Critics Society Awards
*Won: Best Film

==See also==
 
* Infernal Affairs
* Infernal Affairs III
* List of films set in Hong Kong
* List of Hong Kong films
* List of Dragon Dynasty releases

==References==
 

==External links==
*   (dead)
*  
*  
*  
*  

   
{{succession box
| title = Hong Kong Film Critics Society Awards for Best Film
| years = 2003
| before= Chinese Odyssey 2002
| after = McDull, Prince de la Bun
}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 