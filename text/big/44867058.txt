Food Chains
{{Infobox film
| name           = Food Chains
| image          = 
| caption        = 
| director       = Sanjay Rawal Hamilton Fish Sanjay Rawal
| exec. producers = Eva Longoria Eric Schlosser Lekha Singh Abigail Disney Mayra Hernandez David Damian Figueroa Bob Leary Alfonso Montiel Roberto Gonzalez Barrera Alisa Swidler
| writer         = Erin Barnett Sanjay Rawal
| starring       = Eve Ensler Barry Estabrook Dolores Huerta Robert F. Kennedy Jr. Kerry Kennedy Eva Longoria Eric Schlosser Forest Whitaker
| music          = Gil Talmi Macklemore
| cinematography = Forest Woodward
| editing        = Erin Barnett
| studio         = 
| distributor    = Screen Media
| released       = 2014
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Food Chains is a 2014 American documentary film about agricultural labor in the United States.

==Summary==
In  , March 31, 2014  However, their working conditions are shown to be less than favorable.  As a result, they form the  , a Florida-based food wholesaler, to pay them one penny more per pound of tomato.  Meanwhile, the documentary also shows farmworkers in the vineyards of the Napa Valley. Tara Duggan,  , San Francisco Chronicle, November 25, 2014 

==Production== Democratic fundraiser Eva Longoria, Fast Food Nation author Eric Schlosser, and heiress Abigail Disney, among others.  

It was presented at the Berlin Film Festival, the Tribeca Film Festival and the Napa Valley Film Festival.   Shortly after,  Screen Media purchased the distribution rights for North America.  A Spanish version, narrated by actor Demián Bichir was released. 

==Critical reception==
In a review for   called the film rousing and emphatic and empathetic.. Jeannette Catsoulis,  , The New York Times, November 20, 2014  

==References==
 

==External link==
* 
* 

 
 
 
 
 