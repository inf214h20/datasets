The Suspicious Death of a Minor
{{Infobox film
| name = The Suspicious Death of a Minor
| image = The Suspicious Death of a Minor.jpg
| caption =
| director = Sergio Martino
| writer = Ernesto Gastaldi Sergio Martino
| starring = Claudio Cassinelli Mel Ferrer Lia Tanzi
| music = Luciano Michelini
| cinematography = Giancarlo Ferrando
| editing = Raimondo Crociani
| producer = Luciano Martino
| released =  
}}
 1975 Cinema Italian giallo film directed by Sergio Martino.       

== Plot summary ==
Police detective Paolo Germi (Claudio Cassinelli) and the mysterious Marisa meet each other at a dance hall. Germi is unsuspecting of the secret Marisa is carrying with her: adverse conditions forced her into prostitution. As Germi finds the young girl brutally murdered, he decides to go after her killers. During his investigation, he enters a world of intrigue and obfuscation that leave an endless trail of blood.

== Cast ==

*Claudio Cassinelli: Paolo Germi 
*Mel Ferrer: Police superintendent
*Lia Tanzi: Carmela 
*Gianfranco Barra: Teti 
*Patrizia Castaldi: Marisa 
*Adolfo Caruso: Giannino 
*Jenny Tamburi: Gloria 
*Massimo Girotti: Gaudenzio Pesce 
*Barbara Magnolfi: Floriana 
*Franca Scagnetti: Mother of Giannino

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 