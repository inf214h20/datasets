Orphans of Apollo
{{Infobox film
| name           = Orphans of Apollo
| image          = Orphans of Apollo.jpg
| caption        = 
| director       = Becky Neiman & Michael Potter
| producer       = Michael Potter
| starring       = Rick Tumlinson
| music          = 
| cinematography = 
| editing        = Todd Jones
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 space advocates Walter Anderson.   

==Background==
MirCorp founder Walt Anderson’s highest-profile venture was an audacious plan to take over the Russian space station Mir—which was being abandoned by the Russian space agency - and turn it into a commercial outpost in orbit. That effort ultimately failed. Some argue MirCorp was done in by a US government that wanted to squelch any potential competition to the ISS. The film’s title refers to those people who came of age during the early years of the Space Age and expected to see progress continue at the rate seen in those heady early days, only to be disappointed—orphaned—by events of the last few decades. If they were going to have the kind of bold future they once envisioned, they would have to build it themselves. And, with MirCorp, that’s what they tried to do.    In honor of the 50th anniversary of the Smithsonian Institution, an exclusive screening of the film” Orphans of Apollo” was shown in the National Air and Space Museum’s Lockheed Martin IMAX Theater.

==Synopsis==
“Orphans of Apollo” is a documentary film based on a group of entrepreneurs heading MirCorp, who negotiate a business deal with the Russian government to lease the Mir space station for commercial space station. The film covers the historical period from the time President Nixon’s decision to end the NASA Apollo Moon program to post-Soviet Russia. More than solely a high-tech space story, the film includes themes of international negotiation, the power of entrepreneur vision, failed effort, and of course political power play. Walt Anderson, Richard Branson, Tom Clancy, Jeffrey Manber, Rick Tumlinson and others. The documentary has sufficient action to move it along quickly.

==Michael Potter==
Michael Potter is currently a Senior Fellow at the International Institute of Space Commerce. Formerly, Potter worked as an international telecommunications analyst at the Center for Strategic and International Studies (CSIS) in Washington D.C.    Michael Potter was one of the founders of Esprit Telecom, a successful European telecommunications company, where he was president until leaving to establish Paradigm Ventures a high-technology venture capital firm.    Potter also was a founding member of the European Competitive Telecommunications Association. The ECTA pushed for deregulation of telecommunications markets across Europe as a result of commercial practices by former monopolies to limit activities by resellers to access networks and obtain low pricing for the new operators.
 TED forum (Technology, Entertainment & Design), an organization with strong interest in bringing creative solutions to help solve problems in the developing world. TED has partnered with the Clinton Global Initiative current effort to improve health care in rural Rwanda.   

==Accolades==
Winner of the Space Frontier Foundation’s Vision of the Future 2008

==See also==
 
*   a documentary about newspace venture
*  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 