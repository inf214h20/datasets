Best Guy
 
{{Infobox film
| name           = Best Guy
| image          = 
| caption        =
| director       = Toru Murakawa
| producer       = Yasuhiro Hasegawa Akio Yamaguchi Yosuke Mizuno Yoshihiro Kojima Michio Tohohara
| writer         = Makoto Takada   Toru Murakawa
| starring       = Yūji Oda Naomi Zaizen
| music          = Minoru Yamazaki
| cinematography = Yoshitaka Sakamoto
| editing        = Masaaki Kawashima
| studio         = Toei
| released       =  
| runtime        = 115 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 Japanese action film directed by Toru Murakawa, and produced by Toei Company in association with Mitsui & Co. and Tohokushinsha Film. The screenplay was written by Murakawa with Makoto Takada. The film stars Yūji Oda, Naomi Zaizen, Masato Furuoya, Masato Nagamori, and Toshio Kurosawa.    The title refers to the highest rank of the Japan Air Self-Defense Force|JASDFs F-15J training program.

The films aerial scenes were produced in cooperation with the JASDF, using the Mitsubishi F-15J - the countrys variant of the McDonnell Douglas F-15 Eagle. Seen by movie critics as a copy of Top Gun, Best Guy was a box-office failure in Japan.

==Cast==
* Yūji Oda
* Naomi Zaizen
* Masato Furuoya
* Masato Nagamori
* Toshio Kurosawa
* Mikihisa Azuma
* Chiharu Iwamoto
* Akiko Kana
* Chiyoko Shimakura
* Akiji Kobayashi
* Masahiro Sudou
* Takaaki Enoki
* Naoto Takenaka

==Soundtrack== BMG Victor on October 21, 1990. Jeacocke herself makes an appearance in the beginning of the film.

==Marketing== Hasegawa issued a "Best Guy" edition of their 1/48 scale F-15J Eagle model kit to promote the film. 

==See also==
* Top Gun

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 