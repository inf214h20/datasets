The Awful Truth (1929 film)
 
{{Infobox film name           = The Awful Truth  caption        = Ina Claire and Henry Daniell image          = The Awful Truth 1929.jpg director       = Marshall Neilan producer       = Maurice Revnes writer         = Horace Jackson  screenplay  Arthur Richman based on       =   starring       = Ina Claire   Henry Daniell music          = cinematography = David Abel    editing        = Frank E. Hull studio         = Pathé Exchange distributor    = Pathé Exchange released       =   country        = United States  language       = English runtime        = 68 mins.
}}
 Arthur Richman, based on a play by Richman. Ina Claire starred in the original stage version on Broadway in 1922. The film is now considered Lost film|lost. 

==Cast==
*Ina Claire as Lucy Warriner
*Henry Daniell as Norman Warriner
*Theodore von Eltz as Edgar Trent Paul Harvey as Dan Leeson
*Blanche Friderici as Mrs. Leeson
*Judith Vosselli as Josephine Trent John Roche as Jimmy Kempster
*Ernest Hilliard

==Other versions== 1925 silent 1937 with Irene Dunne and Cary Grant. The play was remade unsuccessfully in color, as the musical Lets Do It Again (1953 film)|Lets Do It Again (1953) starring Jane Wyman and Ray Milland.  

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 