The Longest Most Meaningless Movie in the World
 
{{Infobox film
| name           = The Longest Most Meaningless Movie in the World
| image          =
| image size     =
| caption        =
| director       = Anthony Scott
| producer       = Anthony Scott
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1968
| runtime        = 2880 mins
| country        =   United Kingdom English
| budget         =
| preceded by    =
| followed by    =
}} underground movie UK that runs to 48 hours long. No actual footage was shot for the project, which instead consists entirely of outtakes, commercials, strips of undeveloped film, Academy leader, and other filmic castoff material, creating a seemingly endless stream of newsreel and stock footage. 

It was the longest film ever made at the time of its release,  but has since been superseded by many other films.

== See also ==
*List of longest films by running time

==External links==
* 

 
 
 
 
 
 
 
 


 