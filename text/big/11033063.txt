Zizek!
{{Infobox film
| name           = Zizek!
| image          = Žižek!.jpg
| image_size     = 
| caption        = DVD cover
| director       = Astra Taylor
| producer       = Lawrence Konner
| writer         = 
| narrator       = 
| starring       = Slavoj Žižek
| music          = Jeremy Barnes
| cinematography = Jesse Epstein Martina Radwan
| editing        = Laura Hanna
| studio         = 
| distributor    = Zeitgeist Films
| released       = November 17, 2005
| runtime        = 71 minutes
| country        = United States Canada
| language       = English
| budget         = 
| gross          = $40,331 (Worldwide)   
}}
Zizek!, sometimes written as Žižek!, is a 2005 American/Canadian documentary film directed by Astra Taylor. Its subject is philosopher and psychoanalyst Slavoj Žižek, a prolific author and former candidate for the Presidency of Slovenia.

Zizek! premiered at the 2005 Toronto International Film Festival on September 11, 2005 and opened in one theatre in New York City on November 17. It eventually grossed $20,177 in the US and $20,154 in foreign markets for a total worldwide box office of $40,331. 

==Critical reception==
A.O. Scott of the New York Times observed, "Ms. Taylor, clearly thrilled by her proximity to her hero, seems incapable of the analytical distance that would provide insight into either his ideas or the cultural phenomenon he represents. On the basis of this film, it is hard to know whether Mr. Zizeks superstar status is merited, or to say what his cult says about the state of contemporary thought. Zizek! is entertaining without being especially illuminating." 

Eddie Cockrell of Variety (magazine)|Variety called the film a "verbose profile" of Žižek containing "a lot of esoteric, eccentric theories, and little context within his globetrotting life." 

Sean Axmaker of the Seattle Post-Intelligencer graded the film B and said it "attempts to put Zizeks philosophy into practical, accessible terms. Accessible, of course, being a relative term - key concepts are dropped through the film in snippets and sound bites that are gone before youve had a chance to really chew them over. You may not grasp his ideological philosophy, but youll have a good time making the attempt." 

Peter Bradshaw of The Guardian rated the film three out of five stars, adding he thought it "doesnt quite have the interest and focus of Sophie Fiennes recent Zizek documentary, The Perverts Guide to Cinema." 

==See also==
*The Reality of the Virtual (2004)
*The Perverts Guide to Cinema (2006)
*The Perverts Guide to Ideology (2012)
*Liebe Dein Symptom wie Dich selbst! (1996)
*Examined Life (2008)
*Marx Reloaded (2011)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 