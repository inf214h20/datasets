Ambikapathy (1937 film)
 
{{Infobox film
| name           = Ambikapathy
| image          = Ambikapathy 1937 poster.jpg
| caption        = Theatrical poster
| director       = Ellis R. Dungan
| writer         = Screenplay : T. R. S. Mani Dialogue : Elangovan (Thanigachalam)
| starring       = M. K. Thyagaraja Bhagavathar M. R. Santhanalakshmi Serugulathur Sama T. S. Balaiya N. S. Krishnan T. A. Madhuram P. G. Venkatesan
| producer       = Salem Shankar Films
| distributor    = 
| music          = Songs : Papanasam Sivan Background Score : K. C. Dey
| cinematography = Krishnagopal Bal Brigae Dhrona
| editing        = Ellis R. Dungan
| released       = 11 December 1937
| runtime        = 210 min. Tamil
}}
 Tamil film directed by American film director Ellis R. Dungan.             It starred M. K. Thyagaraja Bhagavathar, M. R. Santhanalakshmi, Serugulathur Sama, T. S. Balaiya, N. S. Krishnan, T. A. Madhuram and P. G. Venkatesan. Ambikapathy is regarded as one of the greatest hits of pre-independence Tamil cinema. Ambikapathy, along with Chintamani (1937 film)|Chintamani were the greatest hits of 1937      and made critics regard him as the "first superstar of Tamil cinema".        This was the first Tamil film to name a music director in its credits.   

==Production== Kambar was played by Serugalathur Sama whose appearance was based on that of Rabindranath Tagore. T. S. Balaiya was cast as the villain Rudrasenan. The comic relief was provided by the husband and wife comedy team of N. S. Krishnan - T. A. Madhuram. The film was shot in East India Studio in Calcutta. Background score was composed by K. C. Dey, while Papanasam Sivan composed music and wrote lyrics for the Songs. The completed film was 19,000 fet in length (runtime : 210 minutes).     
Initially, the producer M. S. Thottana Chettiar, wanted Y. V. Rao to direct the film, but buoyed by the success ofChintamani, Rao demanded a huge amount of money as payment which the producers could not afford. Instead they hired the up-and-coming American film director Eungan.     Ellis R. Dungan, on hearing the story, immediately agreed to direct the film.  Adevadasi had earlier been chosen to form the lead pair with M.K. Thyagaraja Bhagavathar in the movie.    But she refused when she discovered that M. K. Thyagaraja Bhagavathar was not a Brahmin and she had to be teamed up with another Brahmin actor in a minor role.  M. R. Santhanalakshmi, a popular stage actress was cast as Amaravathi.

==Plot== Kambar (Serugulathur Kulothunga Chola, Amaravati (played by M. R. Santhanalakshmi). The king objects to their love and insists on testing Ambikapathys literary mettle before judging his worth. The test given to Ambikapathi is that he should write and sing a hundred poems in the field of Puram (dealing with war and politics). The poems should not have any reference to the field of Aram (dealing of love and romance). Ambikapathi begins the test in the Kings court with a Kadavul Vaazhthu(invocation to God). Amaravathi who is keeping the count, mistakes the invocation as a poem and counts it as poem number one. When he has sung only ninety nine Puram poems, she thinks he has completed the task and signals him that hundred poems have been sung. Declaring victory, Ambikapathy sings of his love for her and thus fails the test. He is executed by the king. 

==Cast and crew==
 
* M. K. Thyagaraja Bhagavathar ... Ambikapathy
* M. R. Santhanalakshmi ... Amaravati Kambar
* Kulothunga Cholan
* T. S. Balaiya ... Rudrasenan
* N. S. Krishnan
* T. A. Madhuram
* P. G. Venkatesan
* P. R. Mangalam
* S. S. Raja Mani
* Ellis R. Dungan - Director and Editor
* T. R. S. Mani - Screenplay
* Elangovan - Dialogue
* K. C. Dey - Background Music
* Papanasam Sivan - Songs (music and lyrics)
* Krishna Gopal - Cinematographer   

==Reception==
The film was released on 11 December 1937 and was a big box office success. It ran for 52 weeks. Dungans love scenes, Bhagavathars singing and Elangovans dialogue made the film a talked after success. After Chintamani, this was the second hit film for Bhagavathar in 1937 and made him the "first superstar of Tamil cinema".     

==Soundtrack==
Partial list of Songs from Ambikapathi:
* Maane naan unnai adaya
* Chandira sooriyar pom gathi marinum
* Maasila nilavae nam kadhalai magizhvadhodu
* Ulaginil inbam veru undo

==See also==
* M. K. Thyagaraja Bhagavathar
* Ellis R. Dungan
* Kollywood
* N. S. Krishnan
* Tamil cinema
*List of historical drama films of Asia

==References==
 

==External links==
*  

 

 
 
 
 
 
 