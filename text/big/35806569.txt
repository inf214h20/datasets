Where the Lark Sings (film)
{{Infobox film
| name           = Where the Lark Sings
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Lamac
| producer       = 
| writer         = Franz Lehár (operetta)   Géza von Cziffra
| narrator       = 
| starring       = Mártha Eggerth Alfred Neugebauer Hans Söhnker   Lucie Englisch
| music          = Franz Lehár (operetta)
| editing        = Viktor Bánky
| cinematography = Werner Brandes   István Eiben   Hans Imbert
| studio         = Film A.G Berna
| distributor    = Lux Film (Austria)   Tobis (worldwide)
| released       = 30 October 1936
| runtime        = 97 minutes
| country        = Germany   Hungary   Switzerland
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} musical comedy Where the Lark Sings by Franz Lehár. The film was a German language co-production (filmmaking)|co-production between Hungary, Germany and Switzerland

==Cast==
* Mártha Eggerth - Baronesse Margit von Bardy 
* Alfred Neugebauer - Baron von Bardy - ihr Vater 
* Hans Söhnker - Hans Berend 
* Lucie Englisch - Anna - Margrits Zofe 
* Fritz Imhoff - Török - Faktotum bei Bardy 
* Rudolf Carl - Pista - Knecht bei Bardy 
* Oskar Pouché - Zakos, Mühlenbesitzer 
* Robert Valberg - Rechtsanwalt Dr. Kolbe 
* Gisa Wurm - Emma Kolbe - seine Frau 
* Tibor Halmay - Willi Kolbe - deren Sohn 
* Maria Matzner - Piri, Magd 
* Rita Tanagra - Else - Margits Freundin 
* Leo Resnicek - Autobus-Chauffeur 
* Karl Hauser - Gyuri - Kellner 
* Joe Furtner - Dr. Dudas 
* Anna Kallina - Mutter von Hans Berend

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 