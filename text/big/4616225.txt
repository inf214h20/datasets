Monrak Transistor
{{Infobox film
| name           = Monrak Transistor
| image          = Monraktransistor.jpg
| caption        = Cover of the Thailand DVD release.
| director       = Pen-Ek Ratanaruang
| producer       = Charoen Iamphungporn Duangkamol Limcharoen Nonzee Nimibutr
| writer         = Pen-Ek Ratanaruang Wat Wanlayangkoon  (novel) 
| starring       = Supakorn Kitsuwon,  Siriyakorn Pukkavesh
| music          = Amornbhong Methakunavudh  Chartchai Pongprapapan  Suraphol Sombatcharoen  (songs) 
| cinematography = Chankit Chamnivikaipong
| editing        = Patamanadda Yukol
| distributor    = Five Star Production
| released       = 28 December 2001  (Thailand) 
| runtime        = 129 min.
| country        = Thailand
| language       = Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2001 Cinema Thai film musical and crime film|crime, it is the story of a young man named Pan and his odyssey after he goes AWOL from the army and tries to make it as a luk thung singing star.

==Plot==
The story begins in a jail, where a prisoner is being interrogated. The action is taking place in the background, behind bars and is blurred. The focus is on a bottle of laxative. Seems the prisoner has stolen a necklace and swallowed it. Soon, the necklace is passed. And its not even real gold.

The old jailer picks up the story, saying the prisoner is a boy named Pan from his home village.

Pan is a simple country boy. In the words of the jailer, he thinks about entertainment too much and is not respectful enough of his elders. In other words, hes not too bright.
 flashes back to a village fair, where hes up on stage singing his heart out, with his lyrics being composed on the spot and directed toward Sadao, a pretty village girl whos dancing in the crowd.

A local rich kid pulls up in his truck and asks Sadao to dance. Then, when the rich kid goes to the drinks stand, Pan hands his microphone over to another performer and moves to dance with Sadao. The rich kid returns, and Pan bumps into him, spilling the drinks. The rich guy, with his thuggish friends in tow, orders Pan to clean up the mess. Pan does so by spitting on the guys shoes.

A fight breaks out, but the music keeps going, with a guitarist picking up the beat and screaming a punk song as the fight intensifies.

Pan and Sadao retreat to Sadaos home, where Pan breaks into another song, expressing his love. But before long, Sadaos irascible father shows up with a shotgun, causing Pan to jump into the river to escape the shotgun blasts.

Pan is not easily deterred. Via his sister, he sends Sadao a pretty blue blouse, accompanied by a love note. He then shows up one day to dig a pond for Sadaos father, explaining that the man Sadaos father had originally hired was sick. He insists on calling the man Dad.

"Stop calling me Dad. When did I fuck your mother," the old man cruelly admonishes Pan.
 cultural association of the foot being the basest part of the body, gravely offends him. Pan is back in the doghouse with Sadaos father.

Yet the two become married. For a present, Pan presents Sadao with a new transistor radio. They have a baby on the way and they enjoy being together.

"The movie could end here," the narrator chimes in, "and youd be heading for exits with a happy ending. But there is more to this sad tale."

Pans run of bad luck starts when he draws the wrong number in the draft lottery and must enter the Royal Thai Army|army. He heads off to basic training before his wife gives birth to their child. He promises to write her a letter every day.

A musical interlude depicts Pan and the other soldiers singing the mournful song "Mai Leum" ("Dont Forget) as they crawl on their backs in the mud under barbed wire, and during their haircuts.

One day Pan sees a poster for a singing contest and at the urging of his army buddies, he enters. He nervously gets up on stage and says he wants to sing "The Sad Soldier". The band doesnt know the tune, so Pan sings it a cappella. Though he wows the crowd, he faints onstage when the song is complete. Along with a local girl, Dao, Pan wins the contest and without giving thought to the Absent without leave|consequences, hes on a bus headed for Bangkok, where he hopes to become a big singing star.

He ends up locked inside the music companys office, where he spends the night. The next day, he meets his new boss, a sleazy producer named Suwat, who insists Pan call him "Daddy". He lectures Pan about all the hard work hell need to do before making it as a star.

 
So Pan pitches in around the office, mopping floors and running errands. Months go by. He mops the floor while the other singer who won the contest, Dao, receives training as a singer. Pan keeps mopping. Soon, 27 months have gone by. Hes still mopping floors.

Meanwhile, Sadao is left alone to raise the couples child. She has not heard a word from Pan and is looking careworn. The radio she was given as a wedding present is starting to wear out.

Pan sleeps in a storage closet, a room he shares with an old man named Yen, who reveals that he, too, wanted to be a singing star, but its the young women who usually get all the breaks first, he tells Pan.

So Pan keeps mopping floors, washing cars and running errands. He also becomes close with Dao, whom he assists one night after she becomes ill.
 gold lame tuxedo and pushed onstage.

What he doesnt know is that out in the crowd is Sadao and her father. They have finally tracked down Pan and have come to visit him. Shes brought him bottles of rainwater from the village, figuring the water in the city is dirty and unfit to drink. Pan and Sadao enjoy a brief reunion after the show, but Pan is quickly whisked away by Suwat, to Suwats home, which is decorated with animal skins.
 porn tape - its a film of the girl singer, Dao. Suwat tells Pan to strip and has him pose for photos. Suwat becomes bolder and bolder, and eventually sexually assaults Pan. Pan reacts in surprise and confusion, pushing Suwat off of him. Suwat lands on a glass table and is killed.

Pan runs out into the street. He sees a policeman. Now, not only is he AWOL from the army, hes also a murderer. He then spots a truck loaded down with other men, so he hops aboard, hoping to hop back off when the truck stops. But the truck doesnt stop until its taken Pan to a remote sugar cane plantation, where hes set to work cutting cane in torturous conditions.
 travelling salesman, film dubber, improvising lines to tell her how beautiful she is.

Back on the sugar-cane plantation, the workers, tired of their diet of vegetables and rice, are restive. Pan has made friends with one of the workers, Siew, but Pan is also well liked by the tough boss, Yot. One night at a card game, Yot finds that Siew has won all his money. A fight breaks out. There is running through the jungle. Dead bodies are uncovered. The horror! Pan and Siew keep running, and eventually wind up in the city.

Starving and their clothes ragged, they happen upon a luxury hotel where they see beggers, street cleaners and motorcycle taxi drivers - poor people - being ushered in, Pan and Siew walk in and start helping themselves to the buffet, shoving food into their pockets. Its a charity ball where the elite are dressing up as the poor, and Pan and Siew win the prize for most authentic costume. But when all the food in their pockets is discovered, they are kicked out of the hotel.

Desperate for money, Pan and Siew hatch another plan. Siew snatches a womans necklace, and, as she chases him, he passes it Pan, who is then chased by the police. Eventually Pan is caught, and this brings the story back to where it started in the jail.

Pan ends up serving two years in prison, where he and the other inmates work on the prison farm, fertilizing crops with their own feces and urine. While dipping a bucket into the sewage well, Pan falls in, and is covered in the brown substance.

On his release, Pan waits on the street for a ride. A truck pulls up. It is Siew, who is wearing a track suit, lots of jewelry and is carrying a cellular phone. With his hair dyed blond, he calls himself Peter and announces he is now a drug dealer, and has made quick money. And, to add more indignity to the situation, hes married a former singing star and porn actress - Dao.

Finally, Pan returns to Sadao. She looks more careworn than ever. In addition to a little boy, theres an infant in a crib. "Whos kid is that?" Pan asks when he sees the younger baby. "His father was a dog," she explains. "They are all dogs." Pan looks around. A photo of Sadaos father is on the wall. Hes died. The transistor radio lies in a corner, broken and covered with dust. The pretty blue blouse is faded and stained and crumpled on the floor in another corner.

Theres a final musical reprise of "Mai Leum", with all the characters in the film putting in an appearance to sing the chorus. Sadao reluctantly accepts Pan back into her life, and breaks down, weeping profusely as the couple embraces.

==Cast==
*Supakorn Kitsuwon as Pan
*Siriyakorn Pukkavesh as Sadao
*Black Phomtong as Yot
*Somlek Sakdikul as Suwat
*Porntip Papanai as Dao
*Ampon Rattanawong as Siew
*Prasit Wongrakthai as Sadaws father
*Chartchai Hamnuansak as the old prison guard
*Ackarat Nitipol as Kiattisak
*Sawang Rodnuch as Yen
*Ornnapa Krissadee as the charity ball announcer

==Festivals and awards==
The film was List of Thailands official entries to the Academy Awards|Thailands official entry for the Academy Award for Best Foreign Language Film in 2002. It was the first Thai film selected for the Directors Fortnight at the Cannes Film Festival, where it screened in 2002.    Awards include:  
* 2002 Asia-Pacific Film Festival
**Best actor, Supakorn Kitsuwon
**Best Sound
*2002 Seattle International Film Festival
**Asian Trade Winds Award
*2002 Thailand National Film Awards
**Best actress, Siriyakorn Pukkavesh
**Best picture, Cinemasia
**Best screenplay, Pen-Ek Ratanaruang
*2002 Vienna International Film Festival
**Reader Jury of the Standard
*2009 International Film Festival of Kerala

==Trivia==
*The film is dedicated to luk thung singer Suraphol Sombatcharoen (1930–1968), who wrote the song "Mai Leum" ("Dont Forget"). western screened 2000 Cinema Thai film by Wisit Sasanatieng and co-starring Monrak Transistor star Supakorn Kitsuwon.
*At the time "Monrak Transistor" was made, Thai author Prabda Yoon was dating lead actress Siriyakorn Pukkavesh. While visiting the set, Prabda became friends with director Pen-Ek Ratanaruang, and the pair went on to collaborate on two films, Last Life in the Universe and Invisible Waves.

==References==
* Stephens, Chuck (May 30, 2003)  . The Guardian.
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 