Aval Appadithan
{{Infobox film name = image = caption = director = producer = writer = screenplay = starring = music = studio = cinematography = editing = released = runtime = country = language = Tamil language|Tamil}} Indian Tamil Tamil drama film co-written and directed by C. Rudhraiya, on his directorial debut. The film was produced by Ragamanjari in association with the M.G.R. Government Film and Television Training Institute. It stars Kamal Haasan, Rajinikanth, and Sripriya, while Ilaiyaraaja composed the films music. The plot revolves around Manju (Sripriya) and the difficulties she faces in her life, due to her romantic relationships, resulting in her developing an aggressive and cynical nature towards men.

Released on 30 October 1978, the films final length was  . Although the film received positive critical reception, it was not a box office success at the time of its release. However, after directors P. Bharathiraja and Mrinal Sen saw the film and wrote positively about it, the film began to develop an audience. The film was noted for its stylish filmmaking, screenplay, and dialogue, a large portion of it being in English.
 Best Film Best Cinematographer Special Award for the Best Actress of 1978. In 2013, CNN-IBN included the film in its list, "The 100 greatest Indian films of all time".

== Plot ==
Manju ( ), who has come to Chennai from Coimbatore to make a documentary on women. Sensitive and sincere, he believes his job has a purpose and is both shocked and amused at the cynical attitudes of Manju and Thyagu.
 Christian priests son, used her to satisfy his needs and lust, then called her "sister" in front of her parents. These incidents have led to her present attitude towards men. Arun later shares these conversations with Thyagu, who warns Arun to steer clear of such women.

Inevitably, Arun falls for Manju. However Manju incurs Thyagus wrath when he overhears her reprimanding her office staff for commenting on her character. When Thyagu also comments about her, she resigns from her job. When he learns of this, Arun requests Thyagu to re-employ her. Thyagu simply laughs and says that she is already back, after which Manju seems to have a change of heart and starts courting Thyagu. Arun is devastated to see that she has turned out to be just the sort of woman that Thyagu earlier said she was   opportunistic, money-minded, and fickle. When he asks her about her contradicting stands in life, she responds by saying that is the way she is and will be.

The truth finally emerges that Manju was merely baiting Thyagu to teach him a lesson. When Thyagu starts believing that Manju has fallen for him, he attempts to take advantage of her at a party banquet, but she rebukes and slaps him, after which Thyagu runs away in fright. However, this revelation comes too late for her, as Arun, who is disillusioned with her behaviour, has already married a small town girl (Saritha). When Manju tells her aunt about the attempt to humiliate Thyagu and its ramifications, her aunt tells Manju that she deserved it for leaving behind a golden opportunity to start a new life. In a final discussion in Thyagus car, Manju asks Aruns wife, "What do you think of womens liberation?". "what do you think of womens liberation", Aruns wife replies, "I dont know". Manju replies with a cynical, "that is why you are happy". The film ends with Manju standing on the road as the car carrying Thyagu and the married couple pulls away from her. A voice-over says, "She died today. She will be reborn tomorrow. She will die again. She will be reborn again. Thats how she is".

== Cast ==
* Kamal Haasan as Arun
* Rajinikanth as Thyagu
* Sripriya as Manju
* Sivachandran as Mano
* Saritha as Aruns wife

== Production ==
=== Development === iconoclastic in nature.    Rudhraiya, Haasan, and Ananthu wanted to experiment with their ideas in Tamil. This was Rudhraiyas first film as director;  quite radical in his approach, he wanted to change the conventions of Tamil cinema at that time.    K. Rajeshwar was writing a script dealing with womens liberation at that time, and it was decided that his script would be used for the film; the result was Aval Appadithan.     The initial script written by Rajeshwar consisted of two pages.      

Aval Appadithan was the debut film for both Nallusamy and M. N. Gnanashekaran, who jointly handled the films cinematography.  Vanna Nilavan co-wrote the screenplay with Somasundareshwar and Rudhraiya.  The film was co-produced by Ragamanjari,  in association with the students of the M.G.R. Government Film and Television Training Institute.  Sripriya, who played Manju, was initially unsure about acting in the film due to her busy schedule at that time, and only agreed to do it on Haasans insistence.  According to Rajeshwar, the characterisation of Manju was inspired from a woman he met and who had similar radical beliefs.  Rajinikanth was cast solely on Haasan’s recommendation; he stated, "if Kamal had said, “Don’t cast Rajini”, nobody would have taken me".  

=== Filming ===
{{quote box quote = "  and   used to discuss the scenes to be shot for   at least two or three days in advance. As for the dialogues, he used to tell me about the scene in detail. He would not be easily satisfied. He would ask for rewriting the lines, if he was not happy with what was written." source = — Writer Vannanilavan, on the filming of Aval Appadithan  align = right width = 30%
}} film negative was used to make the film, and the team incurred a cost of   20,000 for exterior shooting equipment.  The scenes where Arun interviewed women for his documentary were real scenes, improvised with women they would meet at colleges and bus stops, and shot using the live-recording method.  The film uses a sharp contrast of black and white colours to lend a surreal atmosphere to it,  and no makeup was used for the lead actors. 

Filming proceeded smoothly as almost all of the dialogues were ready by the time team went for filming the scenes. The camera angles were pre-planned as well.  Haasan shot the film in his spare time, as he was involved in over 20 other films as an actor during the production of Aval Appadithan. Before a shot, Haasan discussed the scene with Ananthu and Rudhraiya on how Godard would have done it. The film was shot in two-hour sessions over a period of four-five months.  The opening scene where Haasan looks into the camera and says "Konjam left-la ukaarunga" (English: Sit a little to the left, please.) was meant as a sign to the audience to support gender equality. 

== Themes and influences ==
Aval Appadithans central theme is on women and their plight in society, as exemplified by Manju and her relationships. Born to a timid father and a mother with loose moral values, she is also subsequently affected by two people she becomes romantically involved with. One, her college mate, left her to marry someone else for the sake of a job; and the second is Mano, the son of a Christian priest, who used her to satisfy his lust and then trivialised their relationship by calling her "sister" in front of her parents. These relationships result in her becoming wary of men and developing an aggressive nature towards them.  Conversations related to matters like the status of women in contemporary (1978) times and the nature of humankind are frequently seen in the film. 

Sociologist Selvaraj Velayutham says in his book, Tamil Cinema: The Cultural Politics of Indias Other Film Industry, that "The woman’s characterisation is, of course, brought out entirely verbally by her  . According to her, she has become this way because of a wayward mother.   The film constantly resorts to existing myths about women and relationships: that a wayward mother destroys her children; that a woman who speaks the truth is always alone; that men are scared of her; that the woman who is different is confused, not sure of herself and is only seeking love from a man but does not know it herself. The only plus point of the film is that it does not expose the body of women in the way it is customary to do.   The visuals constantly play upon the fact that she is pitted against the world. All this could have been avoided if only she had a proper mother!" 

Artist Jeeva (artist)|V. Jeevananand compared Aval Appadithan to other films whose central theme was women, such as Charulata (1964), Aval Oru Thodar Kathai (1974), and Panchagni (1986), while also labelling them as "classics that put the spotlight on women."    Ashish Rajadhyaksha and Paul Willemen, in their book Encyclopedia of Indian Cinema, say the film was also inspired by the 1972 film, Dhakam, which starred R. Muthuraman and Pandari Bai.  The film is an exception on stereotypes of women, as shown by paralleling an independent woman, Manju, and a pious traditional woman: Manju gets into problems while Aruns wife is happy. The last lines of the film where Manju asks "what do you think of womens liberation",  Aruns wife answers, "I dont know",  to which Manju says "that is why you are happy", send the message that one will inevitably get into trouble if one exhibits assertive behaviour. 

Kamal Haasans character, Arun, is an early version of a metrosexual male   sensitive and sincere. Rajinikanths character, Thyagu, is the exact opposite of Arun   money-minded, arrogant, and a womaniser. This is evident when Thyagu says to Arun: "Women should be enjoyed, not analysed."  According to film critic Naman Ramachandran, Thyagu was, by far, Rajinikanths most entertaining character up to that point in his career; his character was a self-confessed chauvinist who believed that men and women can never be equal, and that women are merely objects to be used for mens pleasure. When Arun calls Thyagu "a prejudiced ass", Thyagu responds by saying, "I am a male ass," with the dialogue being in English. His opinion of Sripriyas character, Manju, is seen when he says (also in English), "She is a self pitying sex-starved bitch!" 

== Music ==
{{Infobox album Name = Aval Appadithan Longtype =  Type = Soundtrack Artist = Ilaiyaraaja Cover = AvalAppadithan1.jpg Border = yes Alt =  Caption = Original Album Cover Art Released =  Recorded =  Genre = Feature film soundtrack Length = 10:43 Language = Tamil
|Label = EMI Records Producer = Ilaiyaraaja
}}

The music for the film was composed by Ilaiyaraaja, and the soundtrack album was released through EMI Records.    At that time, Ilaiyaraaja was one of the busiest persons in the Tamil film industry, but he took a reduced fee for his services, at the insistence of Rudhraiya and Haasan, due to the films budget.  

After the recording session of "Ninaivo Oru Paravai" from the film, Sigappu Rojakkal (1978), Ilaiyaraaja asked Haasan to record "Panneer Pushpangale" that same afternoon. During the recording session, Ilaiyaraaja suggested that Haasan tone down the opening notes; when Haasan sang perfectly as per his suggestion, Ilaiyaraaja accepted Haasans next rendition of the song.  The song "Uravugal Thodarkathai" was reused by Ilaiyaraaja in the film Megha (2014 film)|Megha (2014).  Ilaiyaraaja wanted Vannanilavan to write the lyrics for the song, since Vannanilavan had difficulties in writing the lyrics, he opted out. He was subsequently replaced with Gangai Amaran. 

The soundtrack received positive reviews from critics. G. Dhananjayan said in his book The Best of Tamil Cinema, "Ilaiyaraajas brooding background score added to the sombre nature of the movie. Two songs, "Uravugal Thodarkathai" sung by K. J. Yesudas and "Panneer Pushpangale" sung by Kamal Haasan remain popular even today."  B. Kolappan of The Hindu wrote, "If the song "Uravugal Thodarkathai" poignantly captures the vulnerable moments in the life of a woman, "Panneer Pushpangale" and "Vaazhkai Odam Chella" in Aval Appadithan are known for their melody and philosophical touch." 

{{Track listing headline = extra_column = lyrics_credits = title1 = lyrics1 = extra1 = length1 = 4:13}}

{{Track listing headline = extra_column = lyrics_credits = total_length = title1 = lyrics1 = extra1 = extra2 = length2 = 3:21}}

== Release == Safire Theatre Group finally agreed to screen the film as a one-print, one theatre release.  

=== Critical response === Rediff said, "It was what we would call parallel cinema these days." 

Writing for The Hindu, Baradwaj Rangan said, "Aval Appadithan was different. The shadowy black-and-white cinematography was different. The dialogues, which were more about revealing character than advancing plot, were different. The frank handling of sex and profanity (she is a self-pitying, sex-starved bitch!) was different. The documentary-like detours were different. The painfully sensitive, feminist hero was different. Rudraiah was different."  Another critic from The Hindu, B. Kolappan, called the performances of the lead cast "excellent".  Mrinal Sen remarked, "The film was far ahead of its times."  Critics also appreciated the live-recording method of shooting the sequences where Haasans character, Arun, interviewed women for his documentary. 

=== Box office === cult following. guerilla attack on the industry by insiders like me. It slipped through their fingers, so to speak. With all the attention that films get these days, I doubt we can get away with such a film any more." 

=== Accolades === Best Film Best Cinematographer, Special Award for Best Actress of the year. 

== Legacy ==
{{quote box quote = source = align = width = 30%}}
Aval Appadithan is one of only two films ever directed by Rudhraiya; the other was Gramathu Athiyayam (1980).  Aval Appadithan was noted for its stylish filmmaking, screenplay and dialogue, a large portion of it being in English. The dialogues were sharp and were considered almost vulgar.  It also broke the style of filmmaking followed up until that time.  It was the first film made by a graduate from the M.G.R. Government Film and Television Training Institute,   paving the way for Indian film technology students to achieve success in the film industry.   Sripriya included it in her list of favourite films she had worked in. 

In May 2007, K. Balamurugan of Rediff included Aval Appadithan in his list of "Rajnis Tamil Top 10".    In July 2007, S. R. Ashok Kumar of The Hindu asked eight Tamil film directors to list their all-time favourite Tamil films; two of them&nbsp;– Balu Mahendra and Ameer (director)|Ameer&nbsp;– named Aval Appadithan.  Thiagarajan Kumararaja  named Aval Appadithan as an inspiration for his film Aaranya Kaandam (2011).  Haasan described the film as an "unconventional way of film making."  In April 2013, CNN-IBN included the film in its list, "The 100 greatest Indian films of all time".  In June 2013, A. Muthusamy of Honey Bee Music enhanced the songs from their original version on the films soundtrack album to 5.1 surround sound.  In July 2013, Sruti Harihara Subramanian, founder and trustee of The Cinema Resource Centre (TCRC), told Janani Sampath of The New Indian Express that many people assumed the film was directed by K. Balachander, not by Rudhraiya. Sruti also has an album of promotional stills and photographs of the films production. 

In November 2013, The New Indian Express included the film in its list, "Kamal Haasans most underrated films".  In February 2014, CNN-IBN included Aval Appadithan in the list titled, "12 Indian films that would make great books".  In Kathai Thiraikathai Vasanam Iyakkam (2014), the heros writing team discusses the theme of Aval Appadithan in order to get ideas for their films story, until they realise that the film was a failure at the time of its release.  In January 2015, K. Rajeshwar said, "I was told that if Aval Appadithan were made today, it would be a blockbuster. I don’t agree, for it’s still taboo for a woman to talk about her sexual encounters. The profile of the audience should change." 

== References ==
 

== Bibliography ==
*  
*  
*  
*  

== External links ==
*  

 

 
 
 
 
 
 
 
 