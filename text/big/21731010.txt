Mery per sempre
Mery per sempre ("Forever Mary"), is an Italian language dramatic film directed by Marco Risi and released in 1989. It stars Michele Placido, Claudio Amendola, Alessandro Di Sanzo, Francesco Benigno, Salvatore Termini and Tony Sperandeo.

== Synopsis ==
Mery per sempre is set in Malaspina, a juvenile detention centre in Palermo, Sicily, during the 1980s. The protagonist, Marco Terzi (Michele Placido), is a teacher who has transferred to Palermo from Milan. He obtains a position as teacher inside the institution, and has great difficulty in establishing a rapport with the young, hostile students; in particular, Natale Sperandeo (Francesco Benigno), the leader of the group, who is inside for murder, and Pietro Giancone (Claudio Amendola). Terzis situation is further complicated when Mario Libassi (Alessandro Di Sanzo), a transvestite prostitute, nicknamed Mery, falls in love with him. With the passing of time, however, Terzi slowly gains the respect and admiration of his students, even from the most hardened of the young men.

Mery per sempre was followed in 1990 with the sequel Ragazzi fuori, featuring most of the same cast and characters, with the exception of Placido and Amendola. The film Mery per sempre was the inspiration behind a song with the same name by the popular Israeli singer Ivri Lider. 

== References ==
 
*  

 
 
 
 
 
 
 


 