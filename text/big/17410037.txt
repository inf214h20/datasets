The Bloody Exorcism of Coffin Joe
{{Infobox film
| name           = The Bloody Exorcism of Coffin Joe
| image          = The Bloody Exorcism of Coffin Joe.jpg
| caption        = Theatrical release poster
| director       = José Mojica Marins
| producer       = Anibal Massaini Neto
| writer         = José Mojica Marins Adriano Stuart Rubens Francisco Luchetti
| starring       = José Mojica Marins
| music          = Geraldo José
| cinematography = Antonio Meliande
| editing        = Carlos Coimbra
| studio         = Cinedistri
| distributor    = Cinedistri
| released       = December 23, 1974
| runtime        = 100 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
 Coffin Joe as a major character, although it is not considered part of the "Coffin Joe trilogy" (At Midnight Ill Take Your Soul, This Night I Will Possess Your Corpse, and Embodiment of Evil).      

Filmed in Marins trademark b movie style, including the use of inconsistent visual and sound editing, a bizarre audio track, and extremely low budget effects.

== Plot ==
Playing himself, and having filmed the final scene of his most recent movie, Jose Mojica Marins gives an interview discussing his plans for his next film. After an interviewer asks Marins about the true existence of Coffin Joe, Marins replies “Coffin Joe does not exist”. A camera light then explodes. He states he will leave for vacation at the country home of his friend Alvaro to write the script for his next film, which he plans on calling “The Demon Exorcist”.

He arrives at Alvaros. He is met there by Mr. Julio, father of Alvaro, tending his flowers. Marins is taken to the house and greeted by Alvaro, his wife Lucia, and their three daughters: Betinha, Luciana, and Vilma. Vilma is soon to be married to her fiancee Carlos.

Soon odd things occur around the house and property such as winds blowing and horses being frightened, and that night Mr. Júlio frightens everyone when he begins tearing off his shirt and declaring in a frenzied voice that has come to collect a debt. Marins investigates the house that night and finds a locked hallway, but is attacked by flying books and the lights turn off. Betinha sees tarantulas and a snake in the Christmas tree. During these events the scene cuts to a strange woman who carries a white cat and is surrounded by occult figurines and symbols, and has a framed portrait of Coffin Joe behind her on the wall.

Lucia admits at Marins that Vilma is promised to a local witch in the marriage to Eugenio, who is the son of Satan. Vilma is actually the daughter of the witch, but Lucia obtained her as a newborn due to her husbands infertility and faked the pregnancy with the promise to raise Vilma and return her as an adult for the marriage to Eugenio. The recent occurrences in the house are the result of the witchs anger at Vilmas engagement to Carlos. 

Carlos is injured in a mysterious car accident and Lucia faints fearing it to be the action of the witch. The scene then shows the witch biting the head off a rooster as a sacrifice. Marins then returns to his room and hears Vilma moaning in the next room. He finds Vilma wandering naked and she strikes him on the head.
 rants and pronouncements typical of Coffin Joe, such as “may the blood of those who dont deserve to live burst out of their bodies!”, and “may lightning burn the scum!”.
 exorcise the family members of their influence from Coffin Joe.

Marins and Coffin Joe struggle. After Marins is knocked to the ground, a transparent image of Coffin Joe is seen leaving Marins body. The witch and Eugenio both die as results of Marins impromptu Christian pronouncements.

Marins wakes in his room clutching the crucifix. He goes to check on the family members and sees Mr. Julio setting the table for Christmas dinner, Vilma and Carlos in a loving embrace, and Betinha and Luciana opening presents. Marins silently takes his leave as they sing Christmas carols.

The film ends with a shot of Betinhas face with a serious expression. The camera zooms in to the reflection in her eye which shows Coffin Joe who is laughing amid the sounds of tortured screams.

== Cast ==
* Agenor Alves
* Alcione Mazzeo as Luciana
* Ariane Arantes as Wilma
* Geórgia Gomide as Lúcia
* Jofre Soares as Júlio
* José Mojica Marins as Himself/Coffin Joe
* Luiz Karlo
* Marcelo Picchi as Carlos
* Marisol Marins as Betinha
* Rubens Francisco
* Walter Stuart as Álvaro
* Wanda Kosmo as Malvina
==Release==
 

==Reception==
 
==References==
 

== External links ==
*    
*  
*   on  

 

 
 
 
 
 
 
 