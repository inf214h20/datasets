The Power of Right
{{Infobox film
| name           = The Power of Right
| image          =
| caption        =
| director       = Floyd Martin Thornton 
| producer       = 
| writer         = Reuben Gillmer James Knight   Evelyn Boucher   Frank Petley   Leslie Reardon
| music          = 
| cinematography = 
| editing        = 
| studio         = Harma Photoplays
| distributor    = Harma Photoplays
| released       = March 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent war James Knight, Evelyn Boucher and Frank Petley. The film had strong similarities to The Warrior Strain also featuring the Prince of Wales and directed by Thornton. 

==Cast== James Knight as Gerald Stafford
* Evelyn Boucher as Elsie Vigor
* Frank Petley as Danvers
* Leslie Reardon as Leslie Stafford
* Sydney Grant
* Clifford Pembroke
* John Gliddon
* Adeline Hayden Coffin Prince of Wales as Himself
* Marjorie Villis

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.

 

==External links==
* 

 
 
 
 
 
 
 
 


 
 