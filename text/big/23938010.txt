Baby Blues (2008 film)
{{Infobox Film
| name = Baby Blues
| image = BABY BLUES.jpg
| director = Lars Jacobson Amardeep Kaleka
| writer = Lars Jacobson
| producer = Zack Canepari Amardeep Kaleka
| starring = Colleen Porch Ridge Canipe Joel Bryant Kali Majors Holden Thomas Maynard
| distributor = Sweat Shop Films
| released =  
| runtime = 85 minutes
| country = United States
| language = English
}}
Baby Blues (also known as Cradle Will Fall) is a 2008 American horror film co-directed by Lars Jacobson and Amar Kaleka, based on the 2001 killings of five children by their mother Andrea Yates.  The entire film was filmed in Savannah, Georgia USA by the company Neverending Light Productions.

==Plot==
Based on Andrea Yates and her family, the five children being drowned in the family bathroom by their mother.
 psychotic break down due to postpartum depression and after her husband, a truck driver, hits the road again after only being home a day. It’s all too much for her and she snaps, begins to break things in the middle of dinner. She then calmly walks away with the baby to the upstairs bedroom. 

Jimmy (Ridge Canipe), the eldest son, trying to keep his younger brother and sister calm, starts cleaning up, but soon he decides to check on his mother and discovers the horrible truth. His baby brother is dead, and she’s preparing to take care of the rest of her kids. Now hope for the familys survival rests on the shoulder of Jimmy, the eldest son and surrogate man of the house. After his mother tries to drown his sister and he knocks her out, Jimmy gets his brother onto a bike and tells him to ride for help while trying to get his sister to safety. 
 slasher style. Jimmy manages to survive up until his father arrives back after hearing him on a radio transmission. The film then cuts to a hospital and Jimmy is set to come home. His father then tells Jimmy that his mother is also coming home, much to Jimmys surprise. We are then shown the mother, stood pregnant and singing rock-a-bye baby.

==Cast==
*Colleen Porch as Mom
*Ridge Canipe as James "Jimmy" Williams Jr.
*Joel Bryant as James Williams, Sr.
*Kali Majors as Cathy Williams
*Holden Thomas Maynard as Sammy Williams

==Release==
The film was released on August 5, 2008 in the USA and was subject to mediocre reviews, often criticising the mothers dialogue but praising the acting.  On August 9, 2008 "Baby Blues" was released straight to DVD under the name "Cradle Will Fall" in the U.K.

==References==
 

==External links==
* 

 
 
 
 
 
 
 