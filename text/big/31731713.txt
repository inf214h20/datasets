Ginza Cosmetics
{{Infobox film
| name           = Ginza Cosmetics
| image          = Ginza Cosmetics.jpg
| image_size     =
| caption        = DVD cover
| director       = Mikio Naruse
| producer       = Motohiko Ito
| writer         = Tomoichirô Inoue (novel) Matsuo Kishi (screenplay)
| narrator       =
| starring       = Kinuyo Tanaka
| music          = Seiichi Suzuki
| cinematography = Akira Mimura
| editing        = Hidetoshi Kasama
| distributor    = Shintoho
| released       =  
| runtime        = 87 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}

  is a 1951 black-and-white Japanese film directed by Mikio Naruse. It follows the life of a quiet geisha, single mother of a young boy, in the lively Tokyo quarter of Ginza. The film is based on a novel by Tomoichiro Igami and also on screenwriter Matsuo Kishi and director Mikio Naruses personal knowledge of Tokyos Ginza district.

==Plot==
Yukiko Tsuji (Kinuyo Tanaka) is a gentle, affectionate take on a middle aged bar hostess, struggling to bring up a child alone and facing financial, sexual and end of career issues, as well as the implied disapproval of society.

==Cast==
* Kinuyo Tanaka as Yukiko Tsuji
* Ranko Hanai as Shizue Sayama
* Kyōko Kagawa as Kyōko
* Eijirō Yanagi as Seikichi Kineya
* Eijirō Tōno as Hyōbei Sugano
* Yoshihiro Nishikubo as Haruo

==External links==
* Uhlich, Keith. " ." Slant Magazine, 6 February 2006.
*  at Japan Movie Database
*   Mubi

 

 
 
 
 
 
 
 


 