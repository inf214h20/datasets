King Corn (film)
 
{{Infobox film
| name = King Corn
 | image = King corn.jpg
 | caption  = King Corn theatrical poster
 | director = Aaron Woolf
 | writer   = Ian Cheney Curtis Ellis Jeffrey K. Miller Aaron Woolf
 | producer = Aaron Woolf
 | starring = Ian Cheney Curtis Ellis Michael Pollan Stephen Macko Earl Butz
 | distributor = Balcony Releasing
 | released =  
 | country = United States
 | language = English budget      = 
 }}
King Corn is a documentary film released in October 2007 that follows college friends Ian Cheney and Curtis Ellis (directed by Aaron Woolf) as they move from Boston to Greene, Iowa to grow and farm an acre of maize|corn. In the process, Cheney and Ellis examine the role that the increasing production of corn has had for American society, spotlighting the role of government subsidies in encouraging the huge amount of corn grown.

The film shows how industrialization in corn has all but eliminated the image of the family farm, which is being replaced by larger industrial farms. Cheney and Ellis suggest that this trend reflects a larger industrialization of the North American food system. As was outlined in the film, decisions relating to what crops are grown and how they are grown are based on government manipulated economic considerations rather than their true economic, environmental, or social ramifications. This is demonstrated in the film by the production of high fructose corn syrup, an ingredient found in many cheap food products, such as fast food.

==Critical reception==
King Corn received generally positive reviews. The film earned a review score of 70 from the review aggregate site Metacritic (based on 15 reviews). Rotten Tomatoes awarded the film a score of 95% (based on 20 reviews). 

==See also==
 
* 
*Deconstructing Dinner
*The Jungle
*Taste the Waste A Place at the Table
 

==References==
 

==External links==
* 
*  PBS
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 

 