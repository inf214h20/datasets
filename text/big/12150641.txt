We Are from Jazz
{{Infobox Film
| name           = We Are from Jazz
| image          = We Are from Jazz.jpg
| caption        = A film poster
| director       = Karen Shahnazarov
| producer       = Konstantin Forostenko
| writer         = Aleksandr Borodyansky Karen Shahnazarov
| narrator       = 
| starring       = Igor Sklyar Alexander Pankratov-Cherniy
| music          = Anatoli Kroll (conductor) Mark Minkov (composer) Ella Zelentsova (sound)
| cinematography = Vladimir Shevtsik
| editing        = Lidiya Milioti
| studio   = Mosfilm
| released       = 1984
| runtime        = 89 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Soviet comedy musical film by Karen Shahnazarov.

== Plot ==
A student is expelled from music school because he loves jazz, and jazz at that time (film represents 1920s) is a kind of music that represents USA capitalism. He hires two street musicians to form a band, and goes from one city to another trying to gain fame.

== Cast ==
* Igor Sklyar as Konstantin
* Alexander Pankratov-Chorny as Stefan
* Nikolai Averyushkin as Georgi
* Pyotr Shcherbakov as Ivan
* Yelena Tsyplakova as Katie
* Yevgeniy Yevstigneyev as Papa
* Leonid Kuravlyov
* Borislav Brondukov as fake of Captain Kolbasyev
* Larisa Dolina as Clementine Fernandez
* Yuri Vasilyev
* Nikolai Kochegarov
* Pyotr Merkuryev
* Yuri Gusev
* Irina Mazur

* Vladimir Aleksandrov
* K. Bakhchiyev
* Vitaly Bobrov - the real Captain Kolbasyev  
* K. Chakhiryan
* Boris Gitin
* O. Kazanchev
* Grigori Malikov
* Vyacheslav Patsal
* Vladimir Pitsek
* Aleksandr Pyatkov
* R. Sadykov
* Oleg Savosin
* Yekaterina Semyonova
* I. Shubin
* P. Skladchikov
* L. Yankov

==External links==
* 
* 
*   online at official Mosfilm site (with English subtitles)

==References==

 

 
 
 
 
 
 
 


 
 