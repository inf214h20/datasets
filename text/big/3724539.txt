Les Visiteurs du Soir
{{Infobox film
| name           = Les Visiteurs du Soir
| image          = Visiteursdusoir.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Marcel Carné
| producer       = André Paulve
| writer         = Pierre Laroche Jacques Prévert
| narrator       = 
| starring       = Arletty Jules Berry Marie Déa Marcel Herrand Fernand Ledoux Alain Cuny
| music          = Maurice Thiriet
| cinematography = Roger Hubert
| editing        = Henri Rust
| distributor    = 
| released       = 1942
| runtime        = 118 min.
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1942 film Les Enfants Nazi occupation.

The film stars Arletty, Marie Déa, Jules Berry, Fernand Ledoux, Gabriel Gabrio, Alain Cuny and, in a minor role, Simone Signoret.

==Plot==
In May 1485 two of the devils envoys, Gilles (Alain Cuny) and Dominique (Arletty), arrive at the castle of Baron Hugues (Fernand Ledoux) on the night of a celebration for his daughters engagement. The Barons daughter, Anne (Marie Déa), is set to marry Renaud (Marcel Herrand), a warlord who prefers talking about battle more than reciting love poems. Disguised as traveling minstrels, Gilles and Dominique enter the castle and use their powers of enticement to ruin the upcoming nuptials. Gilles seduces the innocent Anne, while both the Baron and Renaud become bewitched with Dominique. But, when Gilles accidentally falls in love with Anne, the Devil (Jules Berry) arrives to ensure that any true happiness is destroyed. When Gilles and Anne are caught together in her room, Gilles is thrown into the dungeon, and Anne and Renauds engagement is called off.

When the Baron and Renaud realize that they are both in love with Dominique, they duel to the death and Renaud is killed. Following the Devils orders, Dominique leaves the castle and entices the Baron to follow her in suit. Intrigued by Annes unusual purity and faith in love, the Devil decides he wants Anne for himself. Making a deal with the Devil, Anne agrees to be with him in return for the Devil releasing Gilles from chains. Once Gilles is free, the Devil strips Gilles of his memory and Gilles walks off leaving Anne with the Devil. But, once Gilles is gone, Anne reveals that she lied and that she could never love the Devil. Returning to the fountain where she and Gilles first pronounced their love, Anne and Gilles reunite and through the power of love, Gilles recovers his memory. Finding the two once again in love, the Devil changes them both into statues, but finds that, even underneath stone, their hearts continue to beat.

==Production==
The film was shot in Nice, in Vichy France, and due to the war, Carné faced a number of difficulties in making the film. Due to the increased censorship during the war, Carné wanted to make a historical and fantastical film that would have little difficulty with the censors.

==Reception==
The film premiered at Paris’s Madeleine Cinema on 4 December 1942 and was one of the biggest film events during the war. It was called "the grandest film of the Occupation."  One of the reasons that the film was such a huge success was due to murmuring before the film was released that the film was an allegory for the current situation. Many people saw the character of the Devil as representing Hitler and the continued beating hearts of the lovers as representing France living under German rule, but not giving up hope. Carné maintained until his death that the film was not an intentional allegory for the war and that any relationship was purely unconscious. 

==References==
* 

===Citations===
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 