Comic King
 
{{Infobox film
| name        = Comic King
| image       = Comickingmovie.jpg
| caption        = Comic King movie poster
| imdb_rating    =
| director       = O Sing-Pui
| producer    = Philip Kwok
| writers        = Lau Ding Kin
| starring       = Julian Cheung Ruby Lin Nicholas Tse Eason Chan Hacken Lee
| runtime        = 92 min.
| country        = Hong Kong
| language       = Cantonese
| Box office     = HK $4,014,014.00
}} 2000 Hong comedy film directed by O Sing-Pui. It was shown in cinemas from 19 January 2001 to 21 February 2001.

==Synopsis==
A romantic triangle turns fantasy into reality in this comedy from Hong Kong. Fung Yip (Julian Cheung) and Mo Wan (Eason Chan) are a pair of young cartoonists who have been hired by a comic book publisher to help write and draw a new superhero comic. Both Fung Yip and Mo Wan become seriously infatuated with Mandy (Ruby Lin), a beautiful girl who works in the office, and as the friends become rivals for her affections, their characters become increasingly contentious; in time, the superheroes theyve created come to life and begin settling their differences using their fighting skills. Nicholas Tse also appears in a dual role as the two rival comic book heroes.

Comic King pokes fun at several of the trademarks of the comic industry, including the marketing and packaging of the comics themselves. Those familiar with the "industry" will enjoy the in-joke references.

==Cast==
*Julian Cheung as Yip Fung
*Ruby Lin as Mandy
*Nicholas Tse as Ting Fung
*Eason Chan as Mo Wan
*Hacken Lee as Young Lo
*Liu Wai-hung Spencer Lam as Newsstand vendor
*Jerry Lamb as Comic book villain Tats Lau Yi-Dat as Comic book hero Helena Law Lan as Queen of Mahjong

==External links==
*  
*  

 
 
 
 


 
 