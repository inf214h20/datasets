Jackie (2012 film)
{{Infobox film
| name = Jackie
| image = Jackie (2012 film).jpg
| border =
| alt =
| caption = Film poster
| director = Antoinette Beumer
| producer = Hans de Weers Ronald van Wechem
| writer = Marnie Blok Karin van Holst Pellekaan
| based on =  
| starring = Carice van Houten Jelka van Houten Holly Hunter
| music = Melcher Meirmans Merlijn Snitker Chrisnanne Wiegel 	
| cinematography = Danny Elsen
| editing = Marc Bechtold
| studio =
| distributor = Starway Film Distribution (Belgium) Benelux Film Distributors (Netherlands)
| released =  
| runtime = 100 minutes
| country = Netherlands
| language = Dutch and English
| budget =
| gross =
}}

Jackie is a 2012 Dutch comedy-drama film directed by Antoinette Beumer, from an idea by Marnie Block and Karen van Holst Pellekaan. The leading roles are played by Carice van Houten, her real-life sister Jelka van Houten and Academy Award winner Holly Hunter.  

==Plot==
Sofie (Carice van Houten) and Daan (Jelka van Houten) are twin sisters who were raised by two fathers (Paul Case and Jaap Spijkers). When they get a phone call from America that their biological mother (Holly Hunter) is in a hospital with a complicated leg fracture awaiting transfer to a rehabilitation center, the two end up in an adventure where everything they believed in is called into question. This results in an unforgettable journey to New Mexico with the strange and inappropriate Jackie  where the lives of the two sisters will change forever.

==Cast==
*Carice van Houten - Sophie
*Jelka van Houten - Daan, Sophies twin sister
*Holly Hunter - Jackie, Sophie & Daans biological mother
*Jaap Spijkers - Harm, Sophie & Daans father
*Paul Hoes - Marcel, Sophie & Daans father
*Jeroen Spitzenberger - Joost, Daans husband
*Hajo Bruins - Robert, Sophies boss
*Elise Schaap - Rosa, one of Sophies co-workers

==References==
 

==External links==
*  

 
 
 
 
 


 