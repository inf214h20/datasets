Dogfight (film)
{{Infobox film
| name = Dogfight
| image =
| image_size = 
| alt = 
| caption = 
| director = Nancy Savoca Richard Guay Peter Newman
| writer = Bob Comfort
| starring = River Phoenix Lili Taylor
| music = Sarah Class Mason Daring
| cinematography = Bobby Bukowski
| editing = John Tintori
| studio = 
| distributor = Warner Bros. Pictures
| released =  
| runtime = 89 minutes
| country = United States
| language = English
| budget = 
| gross = $397,631
}} San Francisco, California, during the Vietnam War (1963 &ndash; 1966). It stars River Phoenix and Lili Taylor, and was directed by Nancy Savoca. 

The film explores the love between an 18-year-old US Marine Corps|Marine, Corporal Eddie Birdlace, on his way to Vietnam, and a young woman, Rose Fenny. Both lovers are portrayed as innocent and inexperienced: Birdlace is angry and inept, Fenny is idealistic yet unsophisticated.

==Plot== Marine buddies have arrived in San Francisco for twenty-four hours, before shipping off to Vietnam, and are planning on attending a "dogfight" (a party where Marines compete to bring the ugliest date, unbeknownst to the girls they bring) later that evening. They separate into the city to attempt to find dates. After a few women reject his advances, Birdlace ducks into a coffee shop, where he encounters Rose, a waitress, on her break, practicing her guitar. She is not particularly "ugly", but rather plain, shy and awkward. Birdlace attempts to charm her, complimenting her on her guitar playing, and inviting her to a party. She is suspicious of his motives, but decides to accept his invitation.

While walking to the bar where the party is to be held, Birdlace begins to have second thoughts about playing such a cruel trick on Rose after realizing shes not ugly enough to compete, and attempts to talk her out of going in. However they encounter one of Birdlaces buddies and his "date" in front of the bar, and so he has no choice but to proceed with Rose into the dogfight. Birdlace proceeds to get drunk, presumably feeling guilty. Shortly after, Rose convinces Birdlace to dance with her, though at first he resists because he knows thats where the dates get judged. The alcohol and dancing eventually make Rose feel dizzy, and she rushes off and ends up getting sick in the bathroom. Rose does not win the dogfight; Marcie, the date of Birdlaces friend Berzin, is the winner. In the ladies room, it is revealed that Marcie is actually a prostitute whom Berzin has hired (which is a violation of the rules of the dogfight) and clues Rose in to the true nature of the party. Rose is devastated, tears into Birdlace, and then storms off. Birdlace immediately regrets having treated Rose so cruelly, and chases after her. He convinces her to let him buy her dinner, in an attempt to make it up to her.

After dinner, the two walk to a club where Rose hopes to perform soon, and then to an arcade. Birdlace is surprised to find himself enjoying spending time with Rose, so much so that he forgets that he was to have met up with his three buddies at a tattoo parlor where they were to get matching tattoos to solidify their friendship. Rose tells Birdlace about her dream to become a folk singer, and he reveals to her that he will be shipping off to Okinawa the following day, and from there on to "a little country called Vietnam," he hopes. She offers to write to him, and asks if he will write back. Birdlace walks Rose home, and they share an awkward moment on her doorstep, before she hesitantly invites him in. They attempt to talk, but end up engaged in a self-conscious yet endearing sexual encounter.

As he is leaving at dawn, Rose gives him her address and asks him to write. Birdlace meets up with his buddies, where they board their bus. Birdlace makes up a story that he did not show up because he spent the night with the beautiful wife of an officer. Berzin later shares with Birdlace that he saw him with Rose; Birdlace counters that he is aware that Berzins "date", Marcie, was actually a prostitute. They agree to keep one anothers secrets, as Birdlace tears up Roses address and throws it out the window of the bus.

Rose is then shown with her mother, weeping and watching coverage of President Kennedys assassination on TV. The film then cuts to 1966, where Birdlace and his three friends are shown in Vietnam. They are playing cards and trying to pass time, when they are suddenly mortared. The scene descends to chaos.

Birdlace is then shown getting off of a Greyhound bus in San Francisco. Discharged from the Marines, he is walking with a limp (presumably from his injuries from the explosion), and it is suggested that his three friends were all killed. He is taken by how much things have changed in the three years since he was last there, with hippies and flower children everywhere. He walks to the neighborhood where Roses coffee shop is, and goes to a bar across the street to have a drink. The bartender tells him that Roses mother has turned the coffee shop over to Rose. He then makes his way across the street and into the coffee shop. Rose, not having heard from him in three years, is surprised to see him, and can only say "hi". She walks over to him, and they fall into an ambiguous embrace, as the film ends.

==Cast==
* River Phoenix as Eddie Birdlace
* Lili Taylor as Rose
* Elizabeth Daily|E.G. Daily as Marcie
* Richard Panebianco as Berzin Anthony Clark as Oakie, one of Birdlaces Marine friends
* Mitchell Whitfield as Benjamin, one of Birdlaces Marine friends
* Holly Near as Roses mother
* Brendan Fraser as a sailor who gets into a fight with the Marines

==Reception==
The film was widely praised by critics.     Dustin Putman said, "A virtually unknown gem, and is one of the sweetest, most touching romances of the decade." 

==Soundtrack==
The films soundtrack featured a number of prominent 1960s artists, including John Fahey, Bob Dylan, Woody Guthrie, Joan Baez, Pete Seeger and Malvina Reynolds.

==Musical adaptation==
 
 Benj Pasek & Justin Paul and a book by Peter Duchan, was directed by Joe Mantello and choreographed by Christopher Gattelli. It starred Lindsay Mendez as Rose and Derek Klena as Eddie. The cast also included Nick Blaemire, Annaleigh Ashford, Steven Booth, Becca Ayers, Adam Halpin, Dierdre Friel, F. Michael Haynie, James Moye and Josh Segarra. David Zinn designed sets and costumes and Paul Gallo designed the lights. The production opened on July 16, 2012, after previews from June 27, and concluded its limited run on August 19. The show received rave reviews for its young writers and for leading lady Lindsay Mendezs tour de force performance.   

Dogfight earned two 2013  : Outstanding New Off-Broadway Musical, Outstanding New Score (Broadway or Off-Broadway), Outstanding Book of a Musical (Broadway or Off-Broadway), Outstanding Actress in a Musical (Lindsay Mendez), and Outstanding Lighting Design (Paul Gallo).  The show was also nominated for two 2013 Drama League Awards for Outstanding Production of a Broadway or Off-Broadway Musical and Distinguished Performance (Lindsay Mendez),  as well as the 2013 Drama Desk Award for Outstanding Actress in a Musical (Lindsay Mendez). 

The original cast recording was released on April 30, 2013. 

In August 2014, the musical had its European premiere at the Southwark Playhouse in London, directed by Matt Ryan.

== References ==
 

==External links==
*  
*  
*  at Allmovie
*   at the Music Theatre International website

 

 
 
 
 
 
 
 
 
 
 
 