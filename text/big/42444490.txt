Mududida Tavare Aralithu
{{Infobox film name           = Mududida Tavare Aralithu image          = caption        =  director       = K. V. Jayaram producer       = Kalaniketan writer         = Chandralekha starring  Lakshmi  music          = M. Ranga Rao cinematography = S. Ramachandra editing        = P. Bhaktavatsalam studio         = Durga Combines released       =   runtime        = 141 minutes country        = India language       = Kannada budget         = 
}} Kannada romantic Lakshmi in the lead roles. 

The film was a musical blockbuster with the songs composed by M. Ranga Rao considered evergreen hits.

==Cast==
* Ananth Nag Lakshmi
* K. S. Ashwath
* Shobha
* Mukhyamantri Chandru Leelavathi
* Mysore Lokesh
* Advani Lakshmi Devi
* Uma Shivakumar
* Musuri Krishnamurthy
* Dingri Nagaraj

==Soundtrack==
The music was composed by M. Ranga Rao.  All the songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Munjane Moodida Haage
| extra1 = S. P. Balasubramanyam
| lyrics1 = Doddarangegowda
| length1 = 
| title2 = Aralide Aralide 
| extra2 = S. Janaki
| lyrics2 = Anamika
| length2 = 
| title3 = Vivaha Baalige 
| extra3 = S. P. Balasubramanyam
| lyrics3 = Doddarangegowda
| length3 = 
| title4 = Milana Kaanadu
| extra4 = S. P. Balasubramanyam
| lyrics4 = Doddarangegowda
| length4 = 
| title5 = Uriyuthide Hoobana
| extra5 = S. Janaki
| lyrics5 = Doddarangegowda
| length5 =
}}

==References==
 

==External sources==
*  
*  

 
 
 
 
 
 
 


 