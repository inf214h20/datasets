The Bat (1926 film)
 
 
{{Infobox film
| name           = The Bat
| image          = TheBat1926.jpg
| caption        = Theatrical poster
| director       = Roland West
| producer       = Joseph M. Schenck Roland West
| based on       =  
| writer         = Roland West Julien Josephson George Marion, Jr. (intertitles) Robert McKim
| cinematography = Arthur Edeson
| distributor    = United Artists
| released       =  
| runtime        = 86 minutes
| country        = United States Silent English intertitles
}} Broadway hit The Bat by Mary Roberts Rinehart and Avery Hopwood, directed by Roland West and starring Jack Pickford and Louise Fazenda.

==Cast==
*George Beranger - Gideon Bell (billed as Andre de Beranger)
*Charles Herzinger - Courleigh Fleming
*Emily Fitzroy - Miss Cornelia Van Gorder
*Louise Fazenda - Lizzie Allen
*Arthur Housman - Richard Fleming (billed as Arthur Houseman) Robert McKim - Dr. Wells
*Jack Pickford - Brooks Bailey
*Jewel Carmen - Miss Dale Ogden
*Sojin Kamiyama - Billy, the Butler
*Tullio Carminati - Detective Moletti
*Eddie Gribbon - Detective Anderson
*Lee Shumway - The Unknown

==Plot==
Writer Cornelia Van Gorder (Fitzroy) rents a house for the summer. In the house, people search for hidden loot while a caped killer (nicknamed "The Bat") murders them one by one.

==Remakes==
Film remakes:
*Director Roland West remade his film four years later as The Bat Whispers (1930), with Chester Morris and Una Merkel, and also released by United Artists.  The Bat, Allied Artists.

TV adaptations include:
*Broadway Television Theatre (1953) 60-minute TV series (WOR-TV, syndicated), with Estelle Winwood, Alice Pearce, and Jay Joslyn Margaret Hamilton, and Jason Robards.
*Der Spinnenmörder (1978) 88-minute German TV movie

==Connection to Bob Kanes "Batman"==
Comic-book creator Bob Kane said in his 1989 autobiography Batman and Me that the villain of the 1930 film The Bat Whispers was an inspiration for his character Batman. 

==References==
 

==External links==
 
*  
*  
*  
*   ( )
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 