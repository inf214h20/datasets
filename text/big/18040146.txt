A Boy Called Hate
{{Infobox film
| name           = A Boy Called Hate
| image          = ABoyCalledHate1995Cover.jpg
| image size     = 
| alt            = 
| caption        = German DVD Cover
| director       = Mitch Marcus
| producer       = Steven Haft
| writer         = Mitch Marcus
| screenplay     = 
| story          =
| based on       =  
| narrator       =  James Caan Missy Crider Elliott Gould Pray for Rain
| cinematography = Paul Holahan
| editing        = Michael Ruscio
| studio         = 
| distributor    = Aquarius TV, Canes Home Video, Dove
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} James Caan, Missy Crider, Adam Beach and Elliott Gould.  It was the first film directed by Mitch Marcus, who also wrote the screenplay.

==Plot== James Caan), who is bilking a former employer in a workers compensation fraud scheme.  One evening while taking a motorcycle ride, Hate witnesses what appears to be an attempted rape.  He shoots the would-be attacker and takes off with Cindy, the young girl being assaulted (Missy Crider).  It turns out that the rapist is an assistant district attorney (Elliott Gould), who survives the shooting and falsely reports that he was the victim of a robbery.  Hate and Cindy leave Los Angeles, but their situation deteriorates when Hate fatally shoots a motorcycle officer whom he mistakenly believes has come to arrest him. 

==Critical reaction==
A Boy Called Hate had a brief theatrical release in June 1996 and received mixed reviews.   Peter Stack, reviewing the film for The San Francisco Chronicle, suggested that "Two aspects of A Boy Called Hate are definitely worth the trip to the Lumiere, where the film opens today: James Caans look-alike son, Scott, plays the lead role, and writer-director Mitch Marcus succeeds in capturing the grim essence of Los Angeles arid outskirts as a tacky wasteland."   Ella Taylor, reviewing the film for the LA Weekly wrote that "Mitch Marcus first feature is living proof that the most fatigued of plots (young lovers-on-the-run-plus-gun) can be recharged by a director who understands the difference between exploration and exploitation."

==References==
 

==External links==
* 
*  

 
 