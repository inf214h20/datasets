Eight Below
{{Infobox film
| name           = Eight Below
| image          = Eight Below poster.jpg
| caption        = Theatrical release poster Frank Marshall
| producer       = Patrick Crowley David Hoberman
| screenplay     = David DiGillio
| based on       =   Tatsuo Nogami Susumu Saji
| starring       =  Paul Walker Bruce Greenwood Moon Bloodgood Jason Biggs
| music          = Mark Isham Don Burgess Christopher Rouse
| studio         = Walt Disney Pictures Spyglass Entertainment Mandeville Films The Kennedy/Marshall Company|Kennedy/Marshall Buena Vista Pictures
| released       =  
| runtime        = 120 minutes
| country        = United States 
| filming location = British Colombia, Canada
| language       = English
| budget         = $40 million
| gross          = $120,455,994
}} adventure film Frank Marshall and written by David DiGilio. It stars Paul Walker, Bruce Greenwood, Moon Bloodgood, and Jason Biggs. It was released theatrically on February 17, 2006, by Walt Disney Pictures in the United States. The film is set in Antarctica, but was filmed in Svalbard, Norway, Greenland, and British Columbia, Canada.

==Plot== meteorite from the planet Mercury (planet)|Mercury. Shepard does so, ignoring his own intuition, which tells him it is too late in the season (January) to complete such a treacherous route. Worried about the snowmobiles breaking through the thinning ice or falling into a crevasse, Shepard tells Harrison and McClaren that the only way to get to Mount Melbourne is by dog sled.

Shepard and McClaren make it to Mount Melbourne, but immediately are called back to base camp due to an approaching heavy storm. McClaren begs for more time, and Shepard gives him half a day. McClaren finds what he was looking for and the two head back to the sled.

Shepard pauses to patch up one of the dogs (Old Jack) whose paw is bleeding. McClaren, while walking around to get a better radio connection with base, slides down an embankment when a soft ledge gives way. His landing at the bottom cracks the thin ice and McClaren ends up breaking through. Shepard is able to get his lead dog Maya to take a rope to McClaren and the dog team pulls him from the water.
 whiteout conditions, Shepard and McClaren have to rely on the dogs stamina and keen sense of direction to get them back to base. The injured people are immediately evacuated, along with all other personnel, due to the storm, which is expected to intensify. With too much weight in the plane to carry both people and dogs, the human team medically evacuates Shepard and McClaren with a plan to return later for the dogs. The dogs are temporarily left behind, but the storm is worse than expected. Because of the harsh weather conditions and a shortage of supplies at the McMurdo Station, it soon becomes apparent that no rescue will be attempted until the next spring.

Back at home, Shepard is guilt-ridden about leaving his dogs and stops working as an Antarctic-conditions guide. Five months later, and after a heart-to-heart session with an older, veteran guide, Shepard decides to throw his all into rescuing the dogs. Before leaving for the trip, Shepard patches things up with McClaren and shares his intention to rescue the dogs. McClaren learns Shepard does not have enough money to pay for the trip but tells him that he cannot help him. Soon afterwards, McClaren sees a drawing of the dog team made by his young son, with the title: "My Hero is... THE DOGS WHO SAVED MY DADDY." McClaren realizes the magnitude of his ingratitude and uses the remaining balance of his grant money to finance a rescue mission.

===The Dogs Story=== southern lights, one of the dogs, Dewey, falls off a steep slope and is fatally injured. The dogs show their affection for their teammate, but after Dewey succumbs to his injuries and dies, the dogs eventually have to move on. The youngest dog, Max, stays with Dewey a little longer and is separated from the rest.
 leopard seal that fiercely guards its meal. The other dogs also find their way to the dead Orca and Max gets the leopard seals attention so the rest can eat. But the leopard seal quickly sees whats happening, bites Maya on the leg and nearly kills her. Max and the other dogs fiercely chase him off. Maya manages to survive, but her leg is seriously injured.

The rest of the pack continue to hunt for food. Maya refuses food brought for her, and gives pack leadership to Max, who has shown bravery, leadership, and compassion.

The dogs manage to find their way close to the base camp, where Shepard and his rescue team are arriving. Shepard and the five dogs have a happy reunion. When Shepard gets ready to go in the car to leave, Max refuses to go. Max leads Jerry to the injured Maya, and he is able to rescue her as well. The team then leaves the base to return home with the six dogs. The final scene shows a grave for Old Jack and Dewey.

==Cast==
* Paul Walker as Jerry Shepard
* Bruce Greenwood as Dr. Davis McClaren
* Moon Bloodgood as Katie
* Jason Biggs as Charlie Cooper
* Gerard Plunkett as Dr. Andy Harrison
* August Schellenberg as Mindo
* Wendy Crewson as Eve McClaren
* Belinda Metz as Rosemary Paris
* Connor Christopher Levins as Eric McClaren
* Duncan Fraser as Captain Lovett
* Dan Ziskie as Navy Commander
* Michael David Simms as Armin Butler
* Daniel Bacon as Bureaucrat #2
* Laara Sadiq as Bureaucrat #3
* Malcolm Stewart as Charles Buffett

==Background==
The 1958 ill-fated Japanese expedition to Antarctica inspired the 1983 hit film Antarctica (1983 film)|Antarctica, of which Eight Below is a remake.   Eight Below adapts the events of the 1958 incident, moved forward to 1993.     In the 1958 event, fifteen Sakhalin Husky sled dogs were abandoned when the expedition team was unable to return to the base. When the team returned a year later, two dogs were still alive. Another seven were still chained up and dead, five were unaccounted for, and one died just outside of Showa Station.

===Sled dogs=== Siberian Huskies Snow Dogs.   The animal filming was supervised by the American Humane Association, and the film carries the standard "No animals were harmed..." disclaimer, despite an on-set incident in which a trainer used significant force to break up an animal fight. 

==Release==

===Critical reception===
The film received generally positive reviews from critics.   reacted favorably ("the dog actors will melt your heart"), but pointed out, as did other reviewers, that "Antarctica buffs" will be critical of errors, such as portraying midwinter events occurring in "balmy, blazing daylight at a time Antarctica is locked in round-the-clock darkness and temperatures of 140 degrees below." 

The review aggregator Rotten Tomatoes reported that 72% of critics gave the film a positive review, based on 146 reviews." The sites consensus says, "Featuring a stellar cast of marooned mutts, who deftly display emotion, tenderness, loyalty and resolve, Eight Below is a heartwarming and exhilarating adventure film." 

===Box office===
According to Box Office Mojo, the film opened at #1 on February 17, 2006, with a total weekend gross of $20,188,176 in 3,066 theaters, averaging to about $6,584 per theater. The film closed on June 1, 2006 with a total worldwide gross of $120,453,565 ($81,612,565 domestic and $38,841,000 overseas). 

==Awards==
Wins
* ) 2007.

Nominations
*Satellite Awards: Satellite Award, Best Youth DVD, 2006.

==Home media==
 
The film was released on separate format widescreen and full screen editions on DVD on June 26, 2006. It was also released on PlayStation Portable (an original widescreen format) on June 26, 2006. The film was released on high definition Blu-ray for an original widescreen presentation on September 19, 2006.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 