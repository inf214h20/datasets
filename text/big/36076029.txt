Carmen Miranda: Bananas is My Business
{{Infobox film
| name           = Carmen Miranda: Bananas Is My Business
| image          = Gangs all here trailer.jpg
| caption        = Documentary tells the life and career of Carmen Miranda, cultural icon of Brazil. 
| alt            =
| director       = Helena Solberg
| producer       = {{Plainlist |
* Helena Solberg
* David Meyer
}}
| writer         = Helena Solberg
| starring       = {{Plainlist |
* Cynthia Adler
* Eric Barreto
* Mario Cunha
* Alice Faye
* Aurora Miranda
* Carmen Miranda
* Leticia Monte
* Rita Moreno
* Cesar Romero
* Synval Silva
* Helena Solberg  
}}
| distributor    = {{Plainlist |
* Radiante Filmes  
* Cinema International Corporation  
* PBS and Fox Lorber Home Video   }}
| released       =    
| runtime        = 91 minutes
| country        = {{Plainlist |
* United States
* Brazil
* United Kingdom  }}
| language       = English Portuguese
}} Broadway in New York City, then in the film industry after she signed with 20th Century Fox in Los Angeles, and her later years in life, before her death and her return to Brazil. Helena Solberg uses two different film styles, biography and directorial reverie, in which Solberg uses actor Erick Barretos to “resurrect Carmen Miranda in several fantasy sequences.  Helena Solbergs attitudes shift throughout the documentary from awe-struck child to empathetic and forgiving Brazilian woman, which she uses to represent the contradictory subplots of Carmen Mirandas life. 
Alongside the fantasy like resurrection of Miranda, Solberg accompanies her documentary with multiple interviews with Carmen Mirandas friends and family, like her sister, her first boyfriend, the guitarist Laurindo Almeida, samba composer Synval Silva, Cesar Romero, and Alice Faye.  

==Brief synopsis==
Born in 1909, Carmen Miranda was already famous in   or being Hollywoods version of one—with all the notoriety and fortune it would bring. Today Miranda is a cult figure, known mainly for her exuberant renditions of such songs as "South American Way" and "The Lady in the Tutti-Frutti Hat," performed in garish costumes topped with fruit-filled turbans.

===The career start===
Carmen Miranda, an almost ghostly character in the imagination of Portuguese, Brazilian, and American audiences, comes back to life in the first scene of the documentary as a dream narrated by   Images from her memorial service in Rio de Janeiro follow, showing the grief of her Brazilian fans as she says goodbye to what she considered her homeland. Born in the small Portuguese village of Varzea da Ovelha e Aliviada on February 9, 1909, Carmen was appropriated by the people of her village as a symbol of success. Making use of interviews with her younger sister Aurora Miranda, the documentary tales the migration story of Carmen, from Portugal to Brazil, were they arrived in November 1909. Carmen Miranda, daughter of a modest barber, Jose Maria Pinto da Cunha, lived in Rio de Janeiro. There, while working at a hat store, she was first discovered as a singing talent, growing up in Rio de Janeiro, as a working class adolescent, she noticed the strong influence of Samba music as a powerful cultural aspect of life in Rio’s slums. 

Embracing that current as a way of expressing herself as an artist, Carmen rose through the radio ranks, while “In those days, a girl that sang on the radio was frowned upon… In a world dominated by man, she was able to navigate through those struggles.”As a local artist, she kept a close relationship with composer Synval Silva and Laurindo Almeida until she left for the United States. This opportunity came in 1939, when she performed along her band of Brazilian musicians. Carmen Miranda embarked on The Normandy for New York after being signed by Lee Shubert, who included her in the cast for Broadway play The Streets of Paris. This was the episode that transformed the life of who was later to be known as The Brazilian Bombshell. Once in New York, Carmen Miranda showed how her extravagant looks, and beautiful voice spoke for her, despite of the fact that her American audience could not understand a word she was speaking.


===Career in the United States and the fight for her identity===
 . ]]
As she became more popular, and stories about her success were heard in Brazil through the media, Brazilians were skeptical of Carmen Mirandas success in New York. Carmen Miranda found herself fighting tirelessly to prove her identity as a Brazilian,  but also to keep the attention from her American audience. Her appropriation of the style would win her many enemies within Brazil, as she represented a sector of Brazilian culture, the Afro-Brazilian, who represented not only the racialized other according to Brazil’s white elite, but where also a threat to national identity.  The press and the elite constantly attacked her image, many who “looked at her as an embarrassment and an affront to their cultural heritage”.  Helena Solberg also suggests that Miranda’s image was exploited and used by the United States Government during World War II as part of its Good Neighbor Policy, towards Latin America, whose natural resources were vital and needed to fight the war.  Even though this would bring credibility to her image, Carmen, in on of her many identities, would eventually lead her to even larger criticism. When Carmen became a blond for her movies in Hollywood, when the World War II was over, the audience back in Brazil bashed her with critics once again, this time saying that she was too ‘Americanized’ However, her American audience seemed to be captivated by the exotic and colorful style of the singer/actress. She was sensually silly, a comical icon of fertility, and friendliness that threatened no one. Time after time, Carmen Miranda was consumed by sadness, since she knew that her beloved exemplifies Carmen Miranda as a commodity to be consumed by U.S. audiences, while her value to her people in Brazil declined as she was considered “Americanized.” Solberg includes interviews with Rita Moreno, who offers her critique of Hollywood’s stereotyping of Latin American women as loud, sassy, and over-sexualized.

===Death and the aftermath===
  (1947).]]
The final stage of the movie chronicles the events leading up to Carmen Miranda’s death. In the documentary, Helena Solberg uses interviews with Carmen Mirandas closest friends and workers, such as her housekeeper, to show that its troubled marriage to filmmaker David Sebastian, and her exhausting work schedule led to a deep depression. With doctors orders, Carmen Miranda took a leave from work and traveled back to Brazil to rest for several weeks. Upon her return to Los Angeles, Carmen Miranda appeared on NBC television series The Jimmy Durante Show where on August 4, 1955, Carmen Miranda suffered a mild heart attack after completing a dance number with Jimmy Durante. Carmen Miranda fell to her knees, upon when Jimmy Durante told the band to “stop the music” as he helped Carmen Miranda up to her feet as she laughed, “Im all out of breath!” Carmen Miranda would finish off the show only to later suffer a second heart attack in her Beverly Hills home that led to her death.

The documentary shows the service that was held in Los Angeles and also shows the return of her body to Rio de Janeiro in accordance with Carmen Mirandas last wishes.  Upon her arrival, the Brazilian government declared a period of national mourning, as more than 60,000 people attended her ceremony at the Rio town hall and where more than half a million Brazilians escorted her body to her final resting place. 

== Cast ==
*Alice Faye — Herself
*Aloísio de Oliveira — Himself
*Aurora Miranda — Herself
*Carmen Miranda — Herself (archive footage)
*Caribé da Rocha — Herself
*Cássio Barsante — Herself
*Cesar Romero —  Himself
*Cynthia Adler — Hedda Hopper
*Eric Barreto — Carmen Miranda (Fantasy Sequences)
*Estela Romero — Herself
*Helena Solberg — Herself (Narrator)
*Ivan Jack — Himself
*Jeanne Allan — Herself
*Jorge Guinle — Himself
*Laurindo Almeida — Himself
*Letícia Monte — Carmen Miranda (teenager)
*Mario Cunha — Himself
*Raul Smandek — Himself
*Rita Moreno — Herself
*Synval Silva — Himself
*Ted Allan — Himself

== Development ==
=== Production ===
{{Quote box
| quote = "I believe that the vision in relation the figure Carmens is changing, apeople are seeing more clearly the impact it she caused in both the culture of Brazil, as in the United States. His legacy is being more valued, in the 40s, there was an elite that did not want see the countrys image associated with Carmen and sang songs. But she remains a strong icon, and your image never grew old and follows arousing the interest of different generations"
| source = Helena Solberg about Carmen Miranda. 
| width =20em
| align =
}}
David Mayer foi parte da gênese do filme e trabalhou junto a Helena Solberg até o fim da edição do documentário. O diretor de fotografia, o polonês Tomasz Magierski, foi constante nas filmagens, enquanto o resto da equipe mudava dependendo do país em que o documentário ia sendo filmado. O filme foi financiado pela  , Public Broadcasting Service, Channel 4 (Reino Unido), Rádio e Televisão de Portugal e RioFilme.

The film was produced by David Mayer and  .

The film had a high cost for a documentary, around US $ 550,000., due the image rights paid to major studios for the use of Carmen Miranda movies. Through searching, the producers found new images until then unreleased, including home movies of the singer herself, which added a new cost by the use rights. 

=== Release === Brasilia Film PBS in POV series.  

The docudrama received several awards and was well received by critics at festivals in Chicago, Locarno, Toronto, Melbourne, Yamagata, Yamagata|Yamagata, and London, and closed the year in Havana.  

=== Awards ===
{|class="wikitable" border="1" cellspacing="1" cellpadding="3"
! Year
! Festival
! Category
! Country
! Result 
|- 1994 || Festival de Brasília  || Best Documentary ||   ||  
|-
|| Jurys Special Award ||   ||  
|-
|| Critics Award ||   ||  
|- 1995 || Havana Film Festival || Best Documentary ||   ||  
|-
|| Chicago International Film Festival  || Best Documentary ||   ||  
|-
|| Yamagata International Documentary Film Festival  || Best Documentary ||   ||  
|- 1996  || International Film Festival of Uruguay || Best Documentary ||   ||  
|-
|| Encontro Internacional de Cinema de Portugal || Best Film - Popular Jury ||   ||  
|-
|}

=== Soundtrack ===
{| class="wikitable"
|- Song  Performance
!  style="text-align:center; width:300px;"|Note(s)
|-
|"Adeus Batucada"
| style="text-align:center;"| Synval Silva
|
*(during the closing credits) 
|-
|"Aquarela do Brasil"
|style="text-align:center;"| Carmen Miranda
| The Gangs All Here (1943).
|-
|"Ao Voltar da Batucada"
|style="text-align:center;"| Synval Silva
|
|-
|"A Week-End in Havana"
| style="text-align:center;"| Carmen Miranda
|
*Performance of the film Week-End in Havana (1941).
|-
|"Boneca de Pixe"
| style="text-align:center;"| Carmen Miranda
| style="text-align:center;"| 
|-
|"Cai, Cai"
| style="text-align:center;"| Carmen Miranda
|  
*Performance of the film That Night in Rio (1941).
|-
|"Chica Chica Boom Chic"
| style="text-align:center;"| Carmen Miranda
|
*Performance of the film That Night in Rio (1941).
|-
|"Camisa Listada"
| style="text-align:center;"| Carmen Miranda
|
|-
|"Cantores de Rádio"
| style="text-align:center;"| Aurora Miranda Carmen Miranda
|
*Performance of the film Hello, Hello, Carnival! (1936).
|-
|"Coração"
| style="text-align:center;"| Synval Silva
|
|-
|"Disseram que Voltei Americanizada"
|style="text-align:center;"| Erick Barreto
| 
*(as Carmen Miranda)
|-
|"Diz que tem"
| style="text-align:center;"| Carmen Miranda
|
|-
|"I Like You Very Much"
| style="text-align:center;"| Carmen Miranda Erick Barreto	
|
*Performance of the short film Sing With The Stars
*(as Carmen Miranda)
|-
|"I Make My Money With Bananas"
| style="text-align:center;"| Erick Barreto
|
*(as Carmen Miranda)
|-
|"K-K-K-Katy"
| style="text-align:center;"| Carmen Miranda
|
*Performance of the short film Sing With The Stars
|- O que é que a baiana tem?"
| style="text-align:center;"| Carmen Miranda
|
*Performance of the film Banana da Terra (1939).
|-
|"O Samba e o Tango"
|style="text-align:center;"| Carmen Miranda
|
|-
|"Prá Você Gostar de Mim" (Taí)
|style="text-align:center;"| Carmen Miranda
|
|-
|"Primavera no Rio"
|style="text-align:center;"| Carmen Miranda
|
|-
|"South American Way"
|style="text-align:center;"| Carmen Miranda
|
*Performance of the film Down Argentine Way (1940).
|- Street of Dreams"
|style="text-align:center;"| The Ink Spots
|
|-
|"Tico-Tico no Fubá"
|style="text-align:center;"| Carmen Miranda
|
*Performance of the short film Sing With The Stars
|-
|"The Lady In The Tutti Frutti Hat"
|style="text-align:center;"| Carmen Miranda
| The Gangs All Here (1943).
|-
|"The Soul Of Carmen Miranda"
|style="text-align:center;"| John Cale
|
*(during the beginning of the credits) 
|-
|}

=== Festivals ===
{|class="wikitable" border="1" cellspacing="1" cellpadding="3"
! Festival 
! Country
! Year
|- Melbourne International Film Festival ||   || 1995
|- Locarno International Film Festival ||   || 1995
|- Toronto International Film Festival ||   || 1995
|- Yamagata International Documentary Film Festival ||   || 1995
|- BFI London Film Festival ||   || 1995
|- International Film Festival Rotterdam ||   || 1996
|-
|Göteborg International Film Festival ||   || 1996
|- Santa Barbara International Film Festival ||   || 1996
|-
|  ||   || 1996
|-
|  ||   || 1996
|- Hong Kong International Film Festival ||   || 1996
|- Mostra Internacional de Films de Dones de Barcelona ||   || 1996
|- Jerusalem Film Festival ||   || 1996
|- Galway Film Fleadh ||   || 1996
|- New Zealand Wellington Film Festival ||   || 1996
|- The London Latin American Film Festival ||   || 1996
|- Helsinki Film Festival ||   || 1996
|- Films from the South ||   || 1996
|- Festival de Cine Realizado por Mujeres ||   || 1996
|- International Womens Film Festival Dortmund ||   || 1999
|- Flying Broom International Womens Film Festival ||   || 2000
|-
| Its All True – International Documentary Film Festival ||   || 2014
|-
|}

== Repercussion ==
The film was extremely well received by American criticism. In Brazil, was acquired by networks Canal Brasil and TV Cultura, and the cable channels GNT and Curta!. In Latin America, by the Discovery Channel and Film&Arts. In the United States aired nationally by PBS network, and in France by Canal Plus.

=== Critical reception ===
==== In the United States ====
The film received generally positive reviews from critics. Dave Kehr of the Daily News, commented in his review "Carmen Miranda remains one of the most immediately recognizable images in movie history (...) an explosion of fantasy, energy and playful eroticism" and that the documentary directed by Helena Solberg " is a complex, personal and moving study of a great entertainer who became a victim of the cultural moment she embodied." 

Film critic of The New York Times, Stephen Holden "the films clips of Miranda from the Hollywood years are like jolts of electricity, for she exudes an incandescent vitality along with a percussive vocal bravura. It is sad that Hollywood, after crowning her with bananas, couldnt think of anything else to do with her except to turn that image into a joke."  

In his analysis for The Village Voice, Amy Taubin wrote that the film was "fabulous and gorgeous." Godfrey Cheshire in Variety (magazine) |Variety says "Bananas Is My Business provides a fascinating account of mega star."  

In his review for The Austin Chronicle, Marjorie Baumgarten recalled that "Bananas Is My Business is a documentary that wants to find the person behind the stereotypical icon of Hollywood." 

The New York Magazine wrote that "Bananas is my Business reveals a much more fascinating figure" more criticized the documentary, which in its conception "is flawed in several respects." 

Ken Ringle of The Washington Post called the documentary of "provocative, affectionate and intelligent." The Time Out magazine wrote that "The most interesting material here relates to Mirandas role as a national symbol for Brazilians, and as the embodiment of Roosevelts Good Neighbor policy during WWII. The exuberant personality comes through loud and clear, but once she made it in Hollywood, Carmen never varied her act, and its all too obvious why she was crushed under the weight of all those bananas." 

Tim Purtell for Entertainment Weekly says "Bananas is my Business traces the Brazilian Bombshells journey from hat maker to South American recording star to Hollywood, where she was typecast as a Latin bimbo. Piquant interviews with Mirandas sister and various musicians and composers mingle with showbiz dish and, in a playful touch, dramatic reenactments by a man in Miranda drag. Hes good, but the show belongs to Miranda herself — in clips, home movies, and rare archival footage — performing with a delirious, inimitable vitality." {{cite web |url=http://www.ew.com/article/1996/06/07/carmen-miranda-bananas-my-business|title=Carmen Miranda: Bananas Is My Business
|author=Tim Purtell|date=|work=|publisher=Entertainment Weekly|accessdate=April 24, 2015}} 

Jonathan Rosenbaum of the Chicago Reader, noted that the film "is highly personal and informative (...) about a woman who became an exaggerated bomb." 

David Hiltbrand magazine People (magazine) |Peoples wrote "this documentary is also an unusually personal essay about Mirandas enduring impact on Pan-American culture and on the imagination of Brazilian filmmaker Helena Solberg. Weaving together news, interviews with intimates, vibrant Technicolor footage from Mirandas films and chimerical dream sequences featuring Carmen impersonator Erik Barreto, this film creates a portrait with both depth and flair." 

Roger Hurlburt for Sun Sentinel, wrote that "Before adopting the fruit-laden turbans that became her trademark, Miranda was a radio singer. Beautiful, charismatic and optimistic, she once said that all she needed to be happy was a bowl of soup and the freedom to sing (...), she also found herself hovering between two cultures. In Brazil, the press accused her of selling out to Hollywood. At the same time, studio heads refused to allow Miranda to stray from the self-mocking image that made her a top moneymaker (...) Bananas Is My Business examines with sensitivity and integrity the myth that was Carmen Miranda." 

In its review, the TV Guide magazine says "director Helena Solberg unearths fascinating material about Mirandas early life and importance within Brazilian pop culture (she was jeered as a sell-out during what was supposed to be a triumphant return to her native country). Belying her image as a goofy Latin bomshell, Miranda emerges as a remarkably canny manipulator of her own star persona and career." 

==== In Brazil ====
In Brazil, Sonia Nolasco wrote for O Estado de S. Paulo that "there are so many surprises and revelations in the documentary "Banana Is My Business" that the viewer gets vexed of ignoring the greatness of our biggest export product." Paul Francis for O Globo said "the film Solberg and Meyer is the first news about the Brazilian cinema that interests me since Barravento."

Luiz Zanin Oricchio wrote for O Estado de S. Paulo that "The film is not a bunch of facts and images. Has an axis, a direction, a thesis without being cold as theorem."

Inácio Araújo, film critic for the Folha de São Paulo "Bananas is my Business, is a beautiful documentary in which Helena Solberg outlines your profile and the trajetora it would take Carmen Miranda international fame, and premature death (...) in the reconstitution sector, there is something to question: any attempt to imitate Carmen results inferior to it (especially dance). The documental part compensate it is failure." 

The journalist Arnaldo Jabor wrote for the Folha de São Paulo "a precious movie (...) Helena Solberg and David Meyer, in a research work and lyricism were beyond mere documentary and redesigned not only the rise and fall of Carmen Miranda but also a portrait of our fragility. You need to watch Bananas Is My Business to see who we are." 

Nelson Motta in his review for O Estado de S. Paulo wrote that "you laugh, you cry, (...) you falls in love madly for this woman, with enormous eyes, and mouth full of sex, joy and music."

Diogo Mainardi for Veja (magazine) |Veja "Bananas Is My Business should be adopted as the national motto, coined in all currencies. It is our contribution to humanity, bananas, grimaces and smiles." 

Zuenir Ventura for the Jornal do Brasil "a definitive and extraordinary film about Carmen Miranda."

==References==
 

== External links ==
*  
*  
*  
*   at the Bright Lights Film Journal
*  
*  
*  
*   at TV Guide
*   at Yahoo Movies
*   on MSN Movies

 
 
 
 
 
 
 
 
 
 