The Shanghai Story
{{Infobox film
| name = The Shanghai Story
| image =The_Shanghai_Story.jpg
| image_size =
| caption =
| director = Frank Lloyd
| producer = Frank Lloyd Steve Fisher
| narrator =
| starring =  Ruth Roman   Edmond OBrien   Richard Jaeckel   Barry Kelley
| music = R. Dale Butts 
| cinematography = Jack A. Marta 
| editing = Tony Martinelli     
| studio =  Republic Pictures
| distributor = Republic Pictures
| released = September 1, 1954
| runtime = 90 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Shanghai Story is a 1954 American thriller film directed by Frank Lloyd and starring Ruth Roman, Edmond OBrien and Richard Jaeckel. It was based on a novel by Lester Yard. 

==Main cast==
*  Ruth Roman as Rita King  
* Edmond OBrien as Dr. Dan Maynard  
* Richard Jaeckel as Knuckles Greer  
* Barry Kelley as Ricki Dolmine  
* Whit Bissell as Paul Grant  
* Basil Ruysdael as Rev. Hollingsworth   Marvin Miller as Colonel Zorek  
* Yvette Duguay as Mrs. Leah De Verno 
* Paul Picerni as Mr. Emilio De Verno  
* Isabel Randolph as Mrs. Merryweather  
* Philip Ahn as Major Ling Wu 
* Frances Rafferty as Mrs. Warren  
* Frank Ferguson as Mr. Haljerson  
* James Griffith as Carl Hoyt   John Alvin as Mr. Warren  
* Frank Puglia as Mr. Chen  
* Victor Sen Yung as Sun Lee  
* Janine Perreau as Penny Warren  
* Richard Loo as Junior Officer

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 