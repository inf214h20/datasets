What Happened Was
{{Infobox film
| name           = What Happened Was
| image          = What Happened Was.jpg
| image size     = 160px
| caption        = Theatrical release poster
| director       = Tom Noonan
| producer       = Scott Macaulay, Robin OHara
| writer         = Tom Noonan
| narrator       =
| starring       = Karen Sillas Tom Noonan
| music          =
| cinematography =
| editing        =
| distributor    = Samuel Goldwyn
| released       = 26 January 1994 (premiere at Sundance Film Festival|Sundance) 9 September 1994 (USA)
| runtime        = 91 minutes
| country        =   English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1994 independent film written, directed by and starring Tom Noonan. It is an adaptation of Noonans original play of the same name.

==Plot==
It depicts two people, played by Karen Sillas and Tom Noonan, on a first date; their conversation gradually reveals their lonely lives and hidden personalities.

==Reception==
What Happened Was... has an overall approval rating of 90% on Rotten Tomatoes. 

On the Siskel & Ebert show, Gene Siskel gave the film a thumbs up, stating that "For what is really just one long night of conversation, the stakes and the tension couldnt be any higher if these were two characters having a more conventional action scene."  Roger Ebert, however, gave the film a thumbs down, calling it "Contrived" and stating that "There is a lot less here than meets the eye."

==References==
 

==External links==
* 
* 

 
 
{{Succession box
| title= 
| years=1994
| before=Ruby in Paradise tied with Public Access
| after=The Brothers McMullen}}
 

 
 
 
 
 
 
 
 


 
 