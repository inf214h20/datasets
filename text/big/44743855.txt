Uthama Raasa
{{Infobox film
| name           = Uthama Raasa
| image          =
| caption        = Raj Kapoor
| producer       = K. Balu Raj Kapoor (dialogues) Raj Kapoor Raj Kapoor Prabhu Kushboo Raja
| music          = Ilayaraja
| cinematography = Velu Prabhakaran
| editing        = Ganesh-Kumar
| studio         = K. B. Films
| distributor    = K. B. Films
| released       =  
| runtime        = 141 minutes
| country        = India Tamil
}}
 1993 Cinema Indian Tamil Tamil film, Raj Kapoor Raja in lead roles. The film had musical score by Ilayaraja.  

==Cast==
  Prabhu
* Kushboo
* Radha Ravi Raja
* Goundamani
* Senthil Manorama
* Thyagu
* Charle Vaishnavi
* S. N. Lakshmi
* Sethu Vinayagam
* Senthamarai
* Venkatesh
* Boobathi Raja
* Manikkaraj
* Durga
* Shanthi
* Chandrika
* Bindhu
* Pandiyan
 

==Soundtrack==
The music was composed by Ilaiyaraaja. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Vaali || 04.51
|- Vaali || 05.09
|- Vaali || 05.54
|- Mano || Vaali || 05.04
|- Janaki || Vaali || 05.06
|- Janaki || Vaali || 05.09
|}

==References==
 

==External links==
*  

 
 
 
 
 


 