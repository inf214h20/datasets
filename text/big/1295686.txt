Santa's Slay
 
{{Infobox film
| name           = Santas Slay
| image          = Santaposterbigla3.jpg
| caption        =
| writer         = David Steiman Douglas Smith Dave Thomas|
| director       = David Steiman
| producer       = Sammy Lee Brett Ratner Doug Steeden
| music          = Henning Lohner
| cinematography = Matthew F. Leonetti
| editing        = Steven Polivka Julia Wong
| studio         = VIP Medienfonds 1
| distributor    = Media 8 Entertainment
| released       =  
| runtime        = 78 minutes
| country        = Canada 
                   United States
| language       = English
| gross          =
}}

Santas Slay is a 2005 Christmas black comedy horror film that stars former professional wrestler Bill Goldberg as Santa Claus. The movie was written and directed by David Steiman, a former assistant to Brett Ratner.

It was shot in Edmonton and Wetaskiwin, Alberta.  The film was released to home media by Lionsgate Home Entertainment.
While filming the final zamboni scene in Bruderheim, Alberta one of their film trailers caught fire.
==Plot summary== James Caan, Fran Drescher, Chris Kattan, and Rebecca Gayheart.
 Dave Thomas), a crooked minister who manages to survive the massacre.  Later, Santa murders the local Jewish deli owner Mr. Green (Saul Rubinek) using his own Menorah (Hanukkah)|menorah.
 Douglas Smith) virgin birth produced by Satan (just as Jesus was the result of a virgin birth produced by God- meaning that Santa is somewhat of an Antichrist).  Christmas was "The Day of Slaying" for Santa until, in 1005 AD, an angel defeated Santa in a curling match and sentenced Santa to deliver presents on Christmas for 1000 years.  This means that Santa is free to kill again in 2005.

Upon arriving at the scene of Mr. Greens murder, Nicholas is taken to the police station for questioning.  He is bailed out by his girlfriend Mary "Mac" Mackenzie (Emilie de Ravin), just before Santa arrives and kills all of the officers.  Santa pursues them in a police car, but they are able to escape (thanks to a shotgun, left in Macs truck by her gun-crazed father).  They flee to Mr. Yulesons bunker, with Santa still in pursuit.  Nicholas and Mac manage to escape, care of Grandpas snowmobile, but Grandpa is run over by Santas "hell-deer" and killed.
 Zamboni but are saved by Grandpa, who is actually the angel that originally defeated Santa.  With Christmas ended and his powers gone, Santa flees in his sleigh but his "hell-deer" are shot down by Macs father with a bazooka.  Pastor Timmons is found dead in a Santa suit and is presumed to be the killer, while in fact the real Santa Claus is boarding a flight from Winnipeg to the North Pole.

After the credits, Santa is looking over his naughty list, when he looks into the camera and says "Bill Goldberg|Whos Next?"

==Cast==
*Bill Goldberg as Santa Douglas Smith as Nicolas Yuleson
*Emilie de Ravin as Mary Mac Mackenzie
*Robert Culp as Grandpa Dave Thomas as Pastor Timmons
*Saul Rubinek as Mr. Green
*Rebecca Gayheart as Gwen Mason
*Chris Kattan as Jason Mason
*Fran Drescher as Virginia Mason
*Alicia Lorén as Beth Mason
*Annie Sorell as Taylor Mason (as Annie M. Sorell)

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 