The House Where Evil Dwells
 
{{Infobox film
| name           = The House Where Evil Dwells
| image          = The House Where Evil Dwells Poster.jpg
| caption        = Theatricial release poster Kevin Connor
| producer       = Martin B. Cohen
| writer         = Novel: James Hardiman Screenplay: Robert Suhosky Susan George Doug McClure Amy Barrett Mako Hattori Tsuiyuki Sasaki Toshiya Maruyama
| music          = Ken Thorne
| cinematography = Jacques Haitkin
| editing        = Barry Peters
| studio         = Cohen Commercial Credit Holdings Toei Company
| distributor    = MGM/UA Entertainment Company
| released       = May 14, 1982 (USA)
| runtime        = 88 min
| country        = United States|USA/Japan
| language       = English
| budget         = 
| gross          = 
| followed_by    = 
}}
 Susan George Kevin Connor and produced by Martin B. Cohen. It was based on a novel by James Hardiman and turned into a screenplay by Robert Suhosky.

==Plot==
In 1840, in the rural and wooded hillside region of Kushiata near Kyoto, Japan, a samurai, named Shigero, comes home to find his wife, Otami, in bed with another man, named Masanori. In a violent scene, Shigero kills them both and then himself. Flash-forward to the present day, an American family of three, whom includes writer Ted Fletcher, his wife Laura, and their 12-year-old daughter, Amy, moves into this since-abandoned house and starts to experience incidents of haunting and possession. The three murdered people still haunt the house and subject each of the Fletcher family to various harassment and mischief which gets more frequent and serious with each passing day. 

A Zen monk approaches Ted and tells him the story about the murders and urges him to leave the house. At the same time, Laura slowly becomes consumed by the evil presence of the three ghosts and begins an affair with Alex Curtis, a diplomat friend of Teds whom introduced them to the house. The evil presence within the haunted house, including the ghosts briefly possessing each member of the family to do odd things, reveals that the ghosts are plotting to re-enact the mass murder-suicide so their souls could be free from the confines of the house. 
 giant spider crabs which attack Amy one evening and it leads her to falling from a tree when she tries to escape and is forced to be sent back to America. 

At the climax, Ted calls the Zen monk who exorcises the ghosts from the house and tells them to leave by the morning or the ghosts will return. When Laura tells Ted about her infidelity with Alex, he predictably takes it very badly and attacks her in which Alex arrives leading to the ghosts returning to the house where they possess all three of them and finally re-enact the gory confrontation from the opening leading to the deaths of Alex, Laura, and Ted, just as the ghosts planned. The end result has the three ghosts leaving the house for the afterlife... and implying that the souls of Ted, Laura and Alex now haunt the cursed house in their place.

==Cast==
*Edward Albert as Ted Fletcher  Susan George as Laura Fletcher
*Doug McClure as Alex Curtis
*Amy Barrett as Amy Fletcher
*Mako Hattori as Otami
*Tsuiyuki Sasaki as Shigero (as Toshiyuki Sasaki)
*Toshiya Maruyama as Masanori
*Tsuyako Olajima as Majyo Witch (as Tsuyako Okajima)
*Henry Mittwer as Zen Monk
*Mayumi Umeda as Noriko, the babysitter
*Shuren Sakurai as Noh Mask Maker
*Hiroko Takano as Wakako
*Shôji Ohara as Assistant Mask Maker (as Shoji Ohara)
*Jirô Shirai as Tadashi (as Jiro Shirai)
*Kazuo Yoshida as Editor
*Kunihiko Shinjo as Assistant Editor
*Gentaro Mori as Yoshio
*Tomoko Shimizu as Aiko
*Misao Arai as Hayashi
*Chiyoko Hardiman as Mama-San
*Hideo Shimedo as Policeman (as Hideo Shimado)

==Critical reception==
 
Vincent Canby of The New York Times wrote, "The House Where Evil Dwells... should satisfy all but the most insatiable appetites for haunted house|haunted-house movies..." 

TV Guide said, "The film has more nudity than chills, but it does have some quirky humor, especially in the exorcism scene." 
 Amityville "Homage Kevin Connors best efforts, or in spite of them. In truth, though, its not all that bad. The acting doesnt particularly suck, and while the ideas dont seem to spring fully formed to the screen, those ideas are at least good, and relatively unique." 
==Home Media Release==
 
==See also==
*List of ghost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 