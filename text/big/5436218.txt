The Cape Canaveral Monsters

{{Infobox film
| name = The Cape Canaveral Monsters
| image =
| caption =
| director = Phil Tucker
| executive producer = Lionel Dichter
| producer = Richard Greer
| writer = Phil Tucker
| narrator =
| starring = Jason Johnson, Katherine Victor
| music = Gene Kauer
| cinematography = W. Merle Connell
| editing = Richard Greer
| distributor =
| released =  
| runtime = 68 Minutes
| country = United States English
| budget =
| gross =
}}

The Cape Canaveral Monsters is a 1960 science-fiction b-movie written and directed by Phil Tucker.

==Plot==
After a couple is killed in an auto accident, their bodies are inhabited by extraterrestrial beings. Taking refuge in an underground cave, the aliens attempt to sabotage the U.S. space program. 

==Production==
The film was shot in Griffith Park Los Angeles and in Bronson caves. 
Linda Connell, playing Sally, was the daughter of director of photography W. Merle Connell who shot the film. This was her only film role.  

The films production company was CCM Productions.  

==Reception==
The film holds a low 3.5/10 on the Internet Movie Database from 248 votes.  


==References==
 

 
 
 
 
 


 