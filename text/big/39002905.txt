It's Easier for a Camel...
 
{{Infobox film
| name           = Its Easier for a Camel...
| image          = Its Easier for a Camel.jpg
| caption        = Film poster
| director       = Valeria Bruni Tedeschi
| producer       = Paulo Branco Maurizio Antonini  Mimmo Calopresti
| writer         = Valeria Bruni Tedeschi Noémie Lvovsky Agnès de Sacy 
| starring       = Valeria Bruni Tedeschi
| music          = 
| cinematography = Jeanne Lapoirie
| editing        = Anne Weil 
| distributor    = Le Petit Bureau
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
}}

Its Easier for a Camel... ( ) is a 2003 French comedy film written, directed by and starring Valeria Bruni Tedeschi. It was entered into the 25th Moscow International Film Festival.    It won the Louis Delluc Prize for Best First Film in 2003.

==Cast==
* Valeria Bruni Tedeschi as Federica
* Chiara Mastroianni as Bianca
* Jean-Hugues Anglade as Pierre
* Denis Podalydès as Philippe
* Marisa Bruni Tedeschi as Mother (as Marysa Borini)
* Roberto Herlitzka as Father
* Lambert Wilson as Aurelio
* Pascal Bongard as Priest
* Nicolas Briançon as Director
* Yvan Attal as Man in Park
* Emmanuelle Devos as Philippes Wife

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 