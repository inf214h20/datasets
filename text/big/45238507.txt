The Cradle (film)
{{Infobox film
| name           = The Cradle
| image          = The Cradle (1922) - 3.jpg
| alt            = 
| caption        = Still with Walter McGrail and Ethel Clayton Paul Powell
| producer       = Jesse L. Lasky
| screenplay     = Olga Printzlau Charles Meredith Mary Jane Irving Anna Lehr Walter McGrail Adele Farrington
| music          = 
| cinematography = Harold Rosson
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Drama drama film Paul Powell Charles Meredith, Mary Jane Irving, Anna Lehr, Walter McGrail, and Adele Farrington. The film was released on March 4, 1922, by Paramount Pictures.        

The film is preserved in the Library of Congress collections.  

==Plot==
 

== Cast ==
*Ethel Clayton as Margaret Harvey Charles Meredith as Dr. Robeert Harveey
*Mary Jane Irving as Doris Harvey
*Anna Lehr as Lola Forbes
*Walter McGrail as Courtney Webster
*Adele Farrington as Mrs. Mason 

== References ==
 

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 