Zigano
{{Infobox film
| name           = Zigano
| image          = 
| image_size     = 
| caption        = 
| director       = Gérard Bourgeois   Harry Piel 
| producer       = 
| writer         = Henrik Galeen   
| narrator       = 
| starring       = Harry Piel   Denise Legeay   Dary Holm   Olga Limburg
| music          = 
| editing        =
| cinematography = Georg Muschner    Gotthardt Wolf Gaumont   Hape-Film 
| distributor    = Bayern-Film
| released       = 27 July 1925
| runtime        = 
| country        = France   Germany 
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent crime film directed by Gérard Bourgeois and Harry Piel and starring Piel, Denise Legeay and Dary Holm. It premiered in Berlin on 27 July 1925. 

==Cast==
* Harry Piel as Benito 
* Denise Legeay as Fiametta 
* Dary Holm as Beatrice 
* Olga Limburg as Teresa 
* Fritz Greiner as Statthalter Ganossa 
* José Davert as Matteo 
* Albert Paulig as Der alte Polizeipräfekt 
* Karl Etlinger as Prälat 
* Raimondo Van Riel as Herzog Ludowico 
* Conte Apoloni as Zigano 
* Henrik Galeen

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 