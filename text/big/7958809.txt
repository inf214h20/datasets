Good to Go (film)
 

{{Infobox Film
| name           = Good to Go
| image          = goodtogo.jpg
| image_size     =
| caption        = 
| director       = Blaine Novak
| producer       = Island Pictures
| writer         = Blaine Novak
| narrator       = 
| starring       = Art Garfunkel Robert DoQui Harris Yulin
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = August 1, 1986
| runtime        = 87 min.
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1986 film directed by Blaine Novak, starring Art Garfunkel as a Washington, D.C. journalist who struggles to clear his name after being framed for rape and murder.

==Soundtrack==
The films soundtrack is rooted in the regional genre of go-go music, including a title song by Trouble Funk. To this day, Good to Go enjoys a cult following among go-go fans.

==External links==
* 
*  

 
 
 
 


 