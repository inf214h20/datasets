Behind the Scenes (1914 film)
{{infobox film
| name           = Behind the Scenes
| image          = Behind the Scenes 1914.jpg
| imagesize      =
| caption        = from left: Russell Bassett, James Kirkwood, and Mary Pickford in Behind the Scenes James Kirkwood
| producer       = Adolph Zukor Daniel Frohman
| based on       =  
| starring       = Mary Pickford James Kirkwood Lowell Sherman
| cinematography = Emmett A. Williams
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 5 reels
| country        = USA Silent (English intertitles)
}} Margaret Mayo, James Kirkwood directed and co-starred.

==Cast==
*Mary Pickford - Dolly Lane James Kirkwood - Steve Hunter
*Lowell Sherman - Teddy Harrington
*Ida Waterman - Mrs. Harrington
*Russell Bassett - Joe Canby

==Preservation status==
A print is in the collection at George Eastman House.  

==References==
 

==External links==
* 
* 
* 
*{{cite journal
 | last = Mason
 | first = Edith Huntington
 | title = Behind the Scenes Margaret Mayo
 | journal = Photoplay
 | volume = VII
 | issue = 1
 | pages = 113ff
 | publisher = Cloud Publishing Co.
 | location = Chicago, Ill
 | date = December 1914
 | url = https://archive.org/stream/PhotoplayMagazineDec.1914/Photoplay1214#page/n109/mode/2up
 | accessdate = 25 December 2013}}

 
 
 
 
 
 
 


 