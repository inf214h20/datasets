Time to Leave
{{Infobox film
| name = Time to Leave
| image = Time to leavemp.jpg
| caption = Poster for the film
| director = François Ozon
| producer = Oliver Delbosc Marc Missonnier
| writer   = François Ozon
| starring = Melvil Poupaud Christian Sengewald Valeria Bruni-Tedeschi Jeanne Moreau
| music    = Valentyn Sylvestrov
| cinematography = Jeanne Lapoirie
| editing  = Monica Coleman 
| distributor = Mars Distribution 
| released =  	
| runtime  = 81 minutes 
| country = France
| language = French
| budget   = 
}}
Time to Leave ( ) is a French film directed by François Ozon, released in 2005 in film|2005. It was screened in the Un Certain Regard section at the 2005 Cannes Film Festival.   

==Plot== terminally ill and has only three months to live. He rejects the treatment for his metastasized tumor that might offer him a slim (less than 5%) chance of survival.

Romain exhibits both selfish and recklessness behavior. He realizes that his good looks give him a certain amount of leeway and he tests the forbearance of the people who care for him. He chases away his lover Sasha and delights in antagonizing his sister. The only person in whom he confides about his illness is his grandmother Laura.

==Cast==
* Melvil Poupaud as Romain
* Jeanne Moreau as Laura
* Valeria Bruni Tedeschi as Jany
* Daniel Duval as Le père
* Marie Rivière as La mère
* Christian Sengewald as Sasha

==Awards==
2005 Valladolid International Film Festival: 
*Silver Spike - François Ozon
*Best Actor - Melvil Poupaud

==Critical reception==
The film received generally positive reviews from critics. The review aggregator Rotten Tomatoes reported that 76% of critics gave the film positive reviews, based on 49 reviews.  Metacritic reported the film had an average score of 67 out of 100, based on 21 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 