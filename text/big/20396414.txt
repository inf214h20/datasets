Life Is Hot in Cracktown
{{Infobox film
| name           = Life Is Hot in Cracktown
| image          = Life is hot in cracktown.jpg
| caption        = Promotional film poster
| director       = Buddy Giovinazzo
| producer       = William Fisch Larry Rattner
| writer         = Buddy Giovinazzo Mark Webber Edoardo Ballerini Ariel Winter
| music          = 
| cinematography =  	
| editing        =	 	
| distributor    = Lightning Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}
Life Is Hot in Cracktown is a 2009 crime drama film based on Buddy Giovinazzos eponymous 1993 collection of short stories. Giovinazzo directed and wrote the film.

==Plot==
The film intertwines several unsettling stories of people in a Manhattan neighborhood ravaged by crack cocaine. Four Dwell in "Cracktown": www.aceshowbiz.com/news/view/00007378.html  Six Immigrating to "Cracktown": www.aceshowbiz.com/news/view/00007693.html 
 torturing a very sick pensioner, and then steps up to doing a murder-for-hire at the behest of a local drug dealer, unaware of the enormous risks.
 bodega at night.  It hardly matters that he is working so hard because the babys non-stop crying will not allow him to sleep.

Another young couple, Marybeth and Benny, are both drug addicts, and Marybeth is actually a pre-op transexual named Mickey.  Marybeth makes a bit of money as a street prostitute, and Benny is into very low-paying burglaries.  Their principal source of drugs is a well-off transexual named Ridley, who is looking to follow in Marybeths footsteps.

Young Willy and younger Susie are the much-neglected children of an addict named Mommy, who makes them sleep on the floor of their one room apartment. Mommys current boyfriend, a hot-tempered addict named Chaz, makes the children beg on the street for his drug money.  Willys one bright spot is neighbor child Melody, whose mother pimps her out every night.  When Chaz and Mommy leave the children behind while they embark on a drug-fueled quest to get more drug money from a relative of Chaz, and Melody is picked up by the police. Willy, meanwhile, is sent on a wild goose chase by Betty, an aging prostitute who enjoys tormenting her neighbors.

==Cast==
*Evan Ross as Romeo
*Stephanie Lugo as Debbie, Romeos girlfriend
*Shannyn Sossamon as Concetta a woman faced with the challenges of raising her sick child, wife of Manny  
*Victor Rasuk as her husband Manny, who supports Concetta and his baby by working the front desk of a drug-infested tenement building by day and an often-robbed bodega at night 
*Lara Flynn Boyle as a faded and cruel prostitute named Betty McBain 
*Thomas Ian Nicholas  as Chad Wesley a rookie cop 
*Vondie Curtis-Hall as  street-toughened veteran cop 
*Kerry Washington as Marybeth/Mickey, a transsexual prostitute 
*Desmond Harrington as Benny, MaryBeths "husband"  Mark Webber as Ridley/Gabrielle, a transgender prostitute and friend of MaryBeth 
*Brandon Routh as Sizemore, a homeless drug addict who sometimes talks to Willy  
*RZA as a drug dealer named Sammy, who does business with Romeo 
*Carly Pope as Stacy
*Tony Plana as an alcoholic man 
*Ridge Canipe as Willy, the older brother of Susie abusive boyfriend
*Illeana Douglas  as Mommy, the heavily addicted mother of Willy and Susie
*Mayte Garcia as Murietta
*Ariel Winter as Susie
*Omar Regan as Cremont
*Jeremy West as Mr. Rutherford, a crippled pensioner robbed by Romeo

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 