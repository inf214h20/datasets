Daybreak (1933 film)
{{Infobox film name           = Daybreak image          =  caption        =  starring       = Li Lili Gao Zhanfei Ye Juanjuan Yuan Congmei director  Sun Yu writer         = Sun Yu cinematography = Zhou Ke producer       =  studio         = Lianhua Film Company distributor    = United States (DVD): Cinema Epoch runtime        = 96 min released  1933
|country China
|language Chinese intertitles budget         =
}}
 1933 Cinema Chinese silent Sun Yu for the Lianhua Film Company. It follows a young girl from a rural fishing village, Ling Ling  (played by Li Lili) as she moves to the glittering city of Shanghai. There she is first raped and then forced into prostitution before eventually becoming a martyr for the coming revolution.

The film stars Li Lili, one of the biggest silent film stars of the period.

== Cast ==
* Li Lili - Ling Ling, the films heroine, a young fishing village girl who experiences the darker side of Shanghai. 
* Gao Zhanfei - Ling Lings revolutionary lover
* Ye Juanjuan
* Yuan Congmei

== Plot == the Bund. Eventually she finds a job working at a factory. Things turn dark, however, when Ling Ling is raped by her employers son. She is then sold into prostitution.

Ironically, her role as prostitute allows her to move into higher social circles serving as a high-classed call girl. In this role, Ling Ling begins to come into some money, which she hopes to use to help others including her former factory friends and those less fortunate.
 sentenced to death. In the films conclusion, Ling Ling tells her firing squad to fire only when she smiles her best smile.

== DVD release == region free English Subtitle subtitles and also includes Shen Xilings film, Crossroads (1937 film)|Crossroads.

== External links ==
*  
*  
*  
*   from the Chinese Movie Database

 
 
 
 
 
 
 
 
 

 
 