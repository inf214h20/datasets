Rites of Spring (film)
{{Infobox film
| name           = Rites of Spring
| image          = Rites of Spring film.jpg
| alt            =
| caption        =
| director       = Padraig Reynolds
| producer       = {{plainlist|
* Wes Benton
* John Norris
* E. Thompson
}}
| screenplay     = Padraig Reynolds
| starring       = {{plainlist|
* A. J. Bowen
* Anessa Ramsey
* Sonny Marinelli
* Katherine Randolph
* James Bartz
* Shanna Forrestall
* Hannah Bryan
* Skylar Burke
* Marco St. John
}}
| music          = Holly Amber Church
| cinematography = Carl Herse
| editing        = Aaron Peak
| studio         = {{plainlist|
* Red Planet Entertainment
* White Rock Lake Productions
* Vigilante Entertainment
}}
| distributor    = IFC Films
| released       =   }}
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Rites of Spring is a 2011 American horror film written and directed by Padraig Reynolds.  It stars A. J. Bowen, Sonny Marinelli, and Katherine Randolph as kidnappers who encounter an unrelated kidnapping victim (Anessa Ramsey) who is fleeing a monster.

== Plot ==
Rachel Adams and her friend Alyssa Miller work for Ryan Hayden.  Rachel is responsible for losing an important client, but she allows Ben Geringer to take the fall.  Feeling guilty, she goes out to drink with Alyssa and resolves to come clean.  Before she can, she and Alyssa are kidnapped by a man known only as the Stranger.  The Stranger takes them to his barn, where he strings them up and demands to know if theyre clean.  Confused and scared, the women do not know how to answer him; this only intensifies when he takes a blood sample from both women and gives it to a strange creature kept in a locked hole.  The Stranger strips Alyssa naked, gives her a sponge bath, puts a goat mask on her head, and takes her away.  Rachel frees herself, but she is too late to save Alyssa, who has been decapitated.  Rachel assaults the Stranger, who warns her not to let the creature free, and flees the barn in a panic.  Freed, the creature pursues her.

Meanwhile, Ben Geringer and his wife Amy have fallen on hard times, as Hayden has fired him.  Ben recruits his wife, his brother Tommy, and his acquaintance Paul Nolan to ransom Haydens daughter.  Ben distrusts Paul, but Paul has an inside person that they need.  The kidnapping goes off without any problems, but Paul deviates from the plan: he murders Haydens wife and takes nanny Jessica hostage.  When Tommy goes to collect the ransom money from Hayden, Hayden in turn takes Tommy hostage.  Hayden forces Tommy to take him to the others and attempts to use Tommy as leverage to regain his daughter.  However, when Hayden frees Jessica, she shoots him dead and reveals herself as Pauls accomplice.  Paul and Jessica announce that they are taking all the money; Tommy protests, and Paul kills him.  Before Paul can finish off Ben and Amy, Rachel bursts in and begs for help.  At the same time, Haydens daughter escapes through a window, never to be seen again.

The creature enters and kills Jessica.  Paul takes Rachel hostage and attempts to leave with the money; however, the creature kills him and stalks Rachel.  Amy suggests that she and Ben just leave, but Ben refuses to abandon Rachel.  Amy reluctantly joins him, and they discover the Strangers house.  When they find his mementos and a wall covered in news clippings of missing women, they attempt to leave, but the Stranger captures them both and prepares them for the creature.  Amy is taken away, but before the Stranger can prepare Ben for sacrifice, Rachel rescues him.  Together, Ben and Rachel search for Amy, only to find her moments before the creature decapitates her.  Ben tosses Rachel his car keys and demands that she flee.  Rachel sets a trap for the creature, but she accidentally kills Ben instead.  Chased through the town by the creature, Rachel finally makes a last stand in her car: she slices the creature with its own scythe and leaves it for dead.

== Cast ==
* A. J. Bowen as Ben Geringer
* Anessa Ramsey as Rachel Adams
* Sonny Marinelli as Paul Nolan
* Katherine Randolph as Amy
* James Bartz as Ryan Hayden
* Shanna Forrestall as Gillian Hayden
* Andrew Breland as Tommy Geringer 
* Hannah Bryan as Alyssa Miller 
* Sarah Pachelli as Jessica
* Skylar Burke as Kelly Hayden
* Marco St. John as the Stranger
* Jeff Nations as Karmanor/Worm Face

== Production == The Signal, and Bowen was excited about working with her again in a non-antagonistic role.   The sets were all practical, and one set was a former Governors mansion. 

== Release ==
The film premiered at Screamfest in Hollywood, California on October 22, 2011.   IFC Midnight released it on video-on-demand and gave it a limited theatrical run on July 27, 2012. 

== Reception == Time Out Chicago rated it 1/5 stars and called it "a hodgepodge of genre clichés that proves as exciting as a bloody nose."   David Guzman of Film Journal International said that the lack of originality was offset by the fresh premise and suspense.   Paul Doro of Shock Till You Drop criticized the lack of back story for the creature and scenes that "appear to be a homeless man chasing idiots around an empty building." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 