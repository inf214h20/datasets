Love Undercover
 
 
{{Infobox film
| name           = Love Undercover
| image          = 
| alt            = 
| caption        = 
| film name      = 新紮師妹 Joe Ma
| producer       = Ivy Kong Joe Ma Sunny Chan Wing Sun
| screenplay     = 
| story          = 
| based on       =  
| starring       = Miriam Yeung Daniel Wu
| narrator       = 
| music          = Lincoln Lo Kin
| cinematography = Cheung Man Po Cheung Ka-fai
| studio         = 
| distributor    = 
| released       =  
| runtime        = 103 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 11.8 M. HK$
| date           = 7 March 2002
}} Hong Kong film directed by Joe Ma Wai-Ho.

The film was followed by two sequels, also directed by Joe Ma:   (2003) and Love Undercover 3 (2006).

==Cast and roles==
* Miriam Yeung - Fong Lai Kuen
* Daniel Wu - Hoi
* Benz Hui - Officer Chung
* Wyman Wong - Roger
* Sammy Leung - Over
* Cha Siu-Yan - Madame Cha
* Chow Chung - Au Yiu San
* Matt Chow	
* Joe Lee Yiu-ming - Chuen
* Lee Siu-kei - FBI officer
* Alan Mak
* Ng Chi Hung - FBI officer
* Wong Ho-Yin - Hung Chow
* Wong Yat Tung	
* Wilson Yip

==External links==
*  
*  

 

 
 
 
 
 

 
 