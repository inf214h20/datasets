Afghantsi
 
{{Infobox film
| name           = Afghantsi
| image          = 
| caption        = 
| director       = Peter Kosminsky
| producer       = Peter Kosminsky
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Afghantsi is a 1988 documentary film directed by Peter Kosminsky   for Yorkshire Television.

It is based on numerous interviews with Soviet soldiers and officers filmed in Kabul at the end of the Soviet war in Afghanistan. 
 Airborne Division, including an outpost on a mountain side overlooking Tajbeg Palace, the headquarters of the 40th Army.

The soldiers in the film speak about the hardships of service in Afghanistan, combat experience and the loss of close friends. 

Another part of the film is devoted to interviews with veterans of the war that have already returned home, and parents of those who died.

==See also==
*Afgan (movie)

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 