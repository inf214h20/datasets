The Blind Woman of Sorrento (1916 film)
{{Infobox film
| name           = The Blind Woman of Sorrento 
| image          =
| caption        =
| director       = Gustavo Serena 
| producer       = 
| writer         = Francesco Mastriani (novel) 
| starring       = Alfredo De Antoni   Olga Benetti   Carlo Benetti   Gustavo Serena
| music          = 
| cinematography = 
| editing        = 
| studio         = Caesar Film
| distributor    = Caesar Film
| released       = May 1916
| runtime        = 
| country        = Italy
| language       = Silent   Italian intertitles
| budget         =
}} silent drama novel of the same title by Francesco Mastriani. Subsequent adaptations were made in 1934, 1952 and 1963. 
 
==Cast==
*Alfredo De Antoni 
* Carlo Benetti 
* Olga Benetti 
* Gustavo Serena
*Domenico Cini 
* Bianca Cipriani 
* Lea Giunchi
* Giorgio Gizzi

== References ==
 

== Bibliography ==
* Poppi, Roberto. I registi: dal 1930 ai giorni nostri.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 