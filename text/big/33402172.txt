Top Floor, Left Wing
{{Infobox film
| name           = Top Floor, Left Wing
| image          = 
| alt            =  
| caption        = 
| director       = Angelo Cianci
| producer       = 
| writer         = Angelo Cianci 
| starring       = Hippolyte Girardot, Fellag, and Aymen Saidi
| music          = Jørgen Lauritsen 
| cinematography = Laurent Brunet 
| editing        = Raphaele Urtin 
| studio         = 
| distributor    = Memento Films Dist.
| released       =  
| runtime        = 125 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Top Floor, Left Wing (Dernier étage, gauche, gauche) is a 2010 French comedy film.  It won the Panorama FIPRESCI Award at the Berlin International Film Festival in 2011. 

==Plot==
Three men in a banlieue are mistaken for terrorists and get targeted by a French SWAT team.

==Cast==
* Hippolyte Girardot as François Echeveria
* Mohamed Fellag as Mohand Atelhadj
* Aymen Saïdi as Salem (Akli) Atelhadj
* Lyes Salem as Hamza Barrida
* Thierry Godard as Commandant Villard
* Michel Vuillermoz as the prefect Judith Henry as Anna Echeveria
* Julie-Anne Roth as Lieutenant Saroyan

==References==
 

==External links==
* 

 
 


 