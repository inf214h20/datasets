Sarafina! (film)
 
 
 
 

{{Infobox film
| name           = Sarafina!
| image          = Sarafina poster.jpg
| image_size     =
| caption        = Theatrical release poster
| alt            = Two black women stand in front of the continent of Africa. The sun is rising with several figures dancing in the background as a flock of doves fly away.
| director       = Darrell Roodt
| producer       = William Nicholson
| narrator       =
| starring       = Leleti Khumalo Whoopi Goldberg Miriam Makeba John Kani
| music          = Mbongeni Ngema Stanley Myers Hugh Masekela 
| cinematography = Mark Vicente
| editing        = David Heitner
| studio         = Hollywood Pictures Miramax Films BBC Buena Vista Pictures (US) Warner Bros. (UK) 1992
| runtime        = 117 minutes United States South Africa
| language       = English
| budget         =
| gross          = $7,306,242
| preceded_by    =
| followed_by    =
}}

Sarafina! is a 1992 South African film starring Leleti Khumalo, Whoopi Goldberg, Miriam Makeba, John Kani and Tertius Meintjies.

==Plot==
The plot centres on students involved in the Soweto Riots, in opposition to the implementation of Afrikaans as the language of instruction in schools. white household in apartheid South Africa, and inspires her peers to rise up in protest, especially after her inspirational teacher, Mary Masombuka (Whoopi Goldberg) is imprisoned.

==Production==

===Filming===
The film was shot on location in Soweto and Johannesburg, South Africa. Darrell Roodt directed, with the script by Mbongeni Ngema and William Nicholson. Leleti Khumalo reprised her role as Sarafina, with Whoopi Goldberg as Mary Masombuka and Miriam Makeba as Angelina.
Companies involved included the   scenes of violence.
For Whoopi Goldberg, this was a project she was determined to be a part of, and convinced the executives at Disney that if they agreed to make this film, she would agree to reprise her role as Dolores Van Cartier in  , which Disney was very keen to make since the original had brought in many millions worldwide. 

==Reception==

===Accolades===
The film was screened out of competition at the 1992 Cannes Film Festival.   

===Release===
The film was released on 18 September 1992. Sarafina! was re-released in South Africa on 16 June 2006 to commemorate the 30th anniversary of the Soweto uprising in Soweto. The re-mastered director’s cut is not very different from the original, except for the inclusion of one scene that was cut from the original, between Leleti Khumalo (Sarafina) and Miriam Makeba (Sarafinas mother), which includes a musical number "Thank You Mama".

===Box office===
Sarafina! was unsuccessful in North American box office receipts, grossing $7,306,242. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 