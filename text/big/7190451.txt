Where Love Has Gone (film)
{{Infobox film
| name           = Where Love Has Gone
| image          = Where Love Has Gone 67.jpeg
| caption        = Original film poster
| director       = Edward Dmytryk
| producer       = Joseph E. Levine
| writer         = John Michael Hayes
| based on       =  
| starring       = Susan Hayward Bette Davis Mike Connors Joey Heatherton Jane Greer  DeForest Kelley George Macready
| music          = Walter Scharf
| cinematography = Joseph MacDonald
| editing        = Frank Bracht
| distributor    = Paramount Pictures
| released       =  
| runtime        = 111 mins.
| country        = United States
| language       = English
| budget         =
| gross          = est. $3,600,000 (US/ Canada) 
}}
 the novel of the same name by Harold Robbins. The music score was by Walter Scharf, the cinematography by Joseph MacDonald and the costume design by Edith Head.
 Anne Seymour and George Macready.

==Plot==
The film begins with headlines stating that 15-year-old Danielle Miller (Joey Heatherton) has murdered a man, Rick Lazich, who was the latest lover of her mother Valerie Hayden (Susan Hayward). Danis father, Luke Miller (Mike Connors) describes the events that led to the tragedy.
 Army Air Forces hero Miller is in San Francisco for a parade in his honor, and meets Valerie Hayden at an art show where one of her works is being exhibited. He is invited to dinner by Valerie mother, Mrs. Gerald Hayden (Bette Davis), who offers him a job and dowry as an enticement for him to marry Valerie. He storms from the house but is followed by Valerie who says she is unable to go against her mothers wishes but that she admires him for having refused her. A relationship develops and the two marry, although a former suitor, Sam Corwin (DeForest Kelley) predicts that the marriage will fail.

As time passes, Luke Miller becomes a successful architect and refuses another offer of employment from his mother-in-law, however the influential and vindictive Mrs. Hayden uses her contacts in the banking industry to ensure that Miller is refused loans to help him build his business. He relents and accepts a position in Mrs. Haydens company. Their daughter, Dani, is born but the relationship of the couple begins to deteriorate with Miller declining into alcoholism, and Valerie indulging in a promiscuous lifestyle. The marriage ends when Miller actually finds her having sex with another man and Mrs. Hayden insists she divorce him. Years pass and Dani eventually becomes her mothers rival for the same man.  
 Anne Seymour) can persuade Dani to open up about her feelings. When Mrs. Hayden petitions for custody of Dani and she still refuses to reveal herself, Valerie reveals that Dani was trying to kill her, and that Rick was only killed when he tried to defend Valerie.   Valerie returns home and commits suicide, and after her death Luke Miller tries to help Dani rebuild her life. 

==Cast==
* Susan Hayward as Valerie Hayden Miller
* Bette Davis as Mrs. Gerald Hayden
* Mike Connors as Maj. Luke Miller (as Michael Connors)
* Joey Heatherton as Danielle Valerie Miller
* Jane Greer as Marian Spicer
* DeForest Kelley as Sam Corwin
* George Macready as Gordon Harris Anne Seymour as Dr. Sally Jennings
* Willis Bouchey as Judge Murphy
* Walter Reedv as George Babson
* Ann Doran as Mrs. Geraghty
* Bartlett Robinson as Mr. John Coleman
* Whit Bissell as Prof. Bell Anthony Caruso as Rafael

==Critic response==
Although Robbins and the studio refused to acknowledge a connection, some publications such as Newsweek noted the similarities between the movie and the real-life case of Cheryl Crane, the daughter of actress Lana Turner, who in 1958 stabbed and killed her mothers boyfriend, Johnny Stompanato, claiming that she was defending Turner from attack. Newsweek wrote that the case seemed to have influenced the "foolish story" and described it as "a typical Harold Robbins pastiche of newspaper clippings liberally shellacked with sentiment and glued with sex".   

The Saturday Review criticized the script saying that it "somehow manages to make every dramatic line (particularly when uttered by Susan Hayward) sound like a caption to a cartoon in The New Yorker. 

==Nominations==
The theme song "Where Love Has Gone" by Jimmy Van Heusen and Sammy Cahn was nominated for both an Academy Award and Golden Globe in the "Best Song" category.  Jack Jones sang the theme song on his album of the same name.

==Notes and references==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 