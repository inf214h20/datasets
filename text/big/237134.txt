Paris, Texas (film)
 
{{Infobox film
| name           = Paris, Texas
| image          = Paris texas moviep.jpg
| image_size     =
| alt            = 
| caption        = Theatrical release poster
| director       = Wim Wenders
| producer       = Anatole Dauman Don Guest
| writer         = L. M. Kit Carson Sam Shepard
| starring = {{plainlist|
* Harry Dean Stanton
* Nastassja Kinski
* Dean Stockwell
* Aurore Clément
* Hunter Carson
}}
| music          = Ry Cooder
| cinematography = Robby Müller
| editing        = Peter Przygodda
| studio         = Road Movies Filmproduktion Channel Four Films Argos Films Westdeutscher Rundfunk (WDR)
| distributor    = Tobis   Argos Films   Palace Pictures   Axiom Films   20th Century Fox  
| released       =  
| runtime        = 150 minutes 
| country        = West Germany France United Kingdom United States
| language       = English
| budget         = £1,162,000
| gross          = $2,181,987
}}
Paris, Texas is a 1984 drama film directed by Wim Wenders and starring Harry Dean Stanton, Dean Stockwell, Nastassja Kinski, and Hunter Carson. The screenplay was written by L.M. Kit Carson and playwright Sam Shepard, and the distinctive musical score was composed by Ry Cooder. The cinematography was by Robby Müller. The film was a Co-production (filmmaking)|co-production between companies in France and West Germany, and was filmed in the United States.

The plot focuses on an amnesiac (Stanton) who, after mysteriously wandering out of the desert, attempts to revive his relationship with his brother (Stockwell) and seven-year-old son (Carson), and to track down his former wife who abandoned their family (Kinski).
 FIPRESCI Prize and the Prize of the Ecumenical Jury. 
   The film has been released on DVD and Blu-ray by the Criterion Collection.

==Plot==
Travis Henderson (Harry Dean Stanton) is walking alone across a vast South Texas desert landscape. Looking for water, he enters a saloon and collapses. He is treated by a doctor, but does not speak or respond to questions. The doctor finds a phone number on Travis, calls the Los Angeles number, and reaches his brother, Walt Henderson (Dean Stockwell), who agrees to pick him up. When Walt arrives in Texas, he discovers that Travis is gone. When he finds him wandering alone, Walt tells his silent brother that he will take him back to Los Angeles. They stop at a motel, but Travis wanders off again. Walt finds him, and the two drive to a diner, where Walt begins to question the still silent Travis more forcefully about his disappearance. Walt and his wife, Anne (Aurore Clément), have not heard from Travis in four years. After Travis abandoned his son Hunter (Hunter Carson), Walt and Anne took care of him for four years. Travis is visibly moved by the mention of his son, and tears flow from his eyes.

Travis finally breaks his silence when he is looking at a map and remarks "Paris, Texas|Paris," talking about how hed like to go there, although Walt mistakenly thinks that he is talking about Paris, France, telling him that it is a little out of the way. When Travis refuses to fly, Walt rents a car, and the brothers begin a two-day road trip back to Los Angeles. The next day, as the two brothers continue their journey, Travis shows Walt a weathered photograph of a vacant lot. He explains that he purchased the property in Paris, Texas—a town he believes is the place where he was conceived, based on the stories told by their mother.

When they arrive in Los Angeles, Travis meets Anne and the son whom he abandoned four years earlier. Hunter is uncomfortable around this stranger who is his father. Walt shows some old home movies, hoping to evoke good memories and help break the ice between the father and son. The movies show Travis with his wife, Jane (Nastassja Kinski), and their young son, sharing a day at the beach.

In the coming days, the relationship between Travis and his son slowly grows, and a bond of trust between the two starts to develop. Anne tells Travis that although she has not heard from Hunters mother in a year, Jane still deposits money into a bank account for her son on the same day each month. She reveals the name of the bank in Houston, Texas, where the deposits are made. Travis becomes determined to find his lost wife, and when he tells his son that he plans to travel to Houston to find his mother, the boy says he will accompany him.

Travis and Hunter leave for Texas without telling Walt and Anne. During their journey, Travis and Hunter grow closer, with Hunter sharing things he learned in school, and Travis sharing his memories. When they arrive in Houston on the expected day of deposit, Hunter spots his mother leaving the bank. They follow her to a parking lot of a striptease club. Telling Hunter to wait in the car, Travis enters the club, containing rooms where customers sit behind one-way mirrors and tell the strippers what they want to see via telephone. The women cannot see the customers. Travis is shocked, but ends up in a room opposite Jane. After several minutes of awkward silence, Travis walks out, returns to the car, and drives to a bar, where he begins to drink.

The next day, Travis drops Hunter off at the Meridien Hotel in downtown Houston, Texas|Houston, and heads back to the striptease club. Travis enters a room with Jane on the other side of the one-way mirror. He picks up the phone, turns his chair away from her, and tells her a story of a man and a young girl who fell in love, married, and had a child—probably before they were ready. At first, Jane is confused by the story, but she soon understands who is on the other side of the glass telling the true story of their relationship. Travis describes how this couples love turned from being joyful to stifling, explains how the drunken man suffocated the young girl with his jealousy and control, and tells how he came to loathe himself and why he disappeared to a place "without language or streets"—never wanting to see anyone again.

When Travis prepares to leave, Jane urges him to stay. She tells how hard it was to leave him—that for years she thought of him often. Travis finally faces the glass, turns a lamp on his face so Jane can see him, and tells her where she can find Hunter, asking her to go there and reunite with her son. Jane agrees and Travis leaves the room. Later that night, Jane enters the hotel room where Hunter is waiting, and the mother and child embrace each other. Travis leaves Houston behind him, driving alone.

==Cast==
* Harry Dean Stanton as Travis Henderson
* Sam Berry as Gas Station Attendant
* Bernhard Wicki as Doctor Ulmer
* Dean Stockwell as Walt Henderson
* Aurore Clément as Anne Henderson
* Claresie Mobley as Car Rental Clerk
* Hunter Carson as Hunter Henderson Viva as Woman on TV
* Socorro Valdez as Carmelita
* Edward Fayton as Hunters Friend
* Justin Hogg as Hunter (age 3)
* Nastassja Kinski as Jane Henderson
* Tom Farrell as Screaming Man
* John Lurie as Slater
* Jeni Vici as Stretch
* Mydolls as Rehearsing Band

==Title== Marathon in the Trans-Pecos|Trans-Pecos region of west Texas; and Nordheim, Texas|Nordheim, southeast of San Antonio. Instead, Paris is referred to as the location of a vacant lot owned by Travis that is seen in a photograph. His obsession with the town appears to be based on the notion that his parents indicated to him that he was probably conceived there. The photograph shows a desert landscape, although in reality Paris lies on the edge of the forests in Northeast Texas and the flat to gently-rolling humid farmland of the north-central part of that state, far from any desert.

==Style==
Paris, Texas is notable for its images of the Texas landscape and climate. The first shot is a birds eye-view of the desert, a bleak, dry, alien landscape. Shots follow of old advertisement billboards, placards, graffiti, rusty iron carcasses, old railway lines, neon signs, motels, seemingly never-ending roads, and Los Angeles, finally culminating in some famous scenes shot outside a drive-through bank in Downtown Houston. The films production design was by Kate Altman. The cinematography is typical of Robby Müllers work, a long-time collaborator of Wim Wenders.

The film is accompanied by a slide-guitar score by Ry Cooder, based on Blind Willie Johnsons "Dark Was the Night, Cold Was the Ground".

==Reception== FIPRESCI Prize, and the Prize of the Ecumenical Jury.  2006 as part of the Sundance Collection category.   
 BAFTA Awards Best Director Best Film and other categories.

Paris, Texas currently holds an 100% "fresh" rating on Rotten Tomatoes, based on 27 reviews.  Critic Roger Ebert gave the film four stars, particularly praising the performance of Hunter Carson. Summarizing his review of the film, Ebert wrote "Paris, Texas is a movie with the kind of passion and willingness to experiment that was more common fifteen years ago than it is now. It has more links with films like Five Easy Pieces and Easy Rider and Midnight Cowboy, than with the slick arcade games that are the box-office winners of the 1980s. It is true, deep, and brilliant." 
Newsweek referred to the film as "a story of the United States, a grim portrait of a land where people like Travis and Jane cannot put down roots, a story of a sprawling, powerful, richly endowed land where people can get desperately lost." 
{{cite web
| url        = http://www.wim-wenders.com/movies/movies_spec/paristexas/paris_texas.htm
| title      = Paris, Texas Official Site
| publisher  = Wim-wenders.com
| date       =
| accessdate = 2010-01-24
}}  The New York Times   Vincent Canby gave the film a mixed review, writing, "The film is wonderful and funny and full of real emotion as it details the means by which Travis and the boy become reconciled. Then it goes flying out the car window when father and son decide to take off for Texas in search of Jane." 

==In popular culture==
* Irish rock group U2 cite Paris, Texas as an inspiration for their album The Joshua Tree. 
{{cite web
| last       = Kelly
| first      = Nick
| url        = http://www.independent.ie/entertainment/music/from-the-lone-star-state-to-outer-space-1719711.html
| title      = From the Lone Star State to outer space
| work       = The Independent
| date       = 2009-04-25
| accessdate = 2010-01-24
}}  Travis and Texas both took their names from this film. 
{{cite web
| last       = Levine
| first      = Nick
| url        = http://www.digitalspy.co.uk/music/interviews/a208089/sharleen-spiteri.html
| title      = Sharleen Spiteri
| work       = Digital Spy
| date       = 2010-03-12
| accessdate = 2010-03-12
}}  
{{cite web
| last       = Graham
| first      = Polly
| url        = http://www.dailymail.co.uk/home/moslive/article-483100/Paper-Scissors-Rock-The-return-Travis.html
| title      = Paper, Scissors, Rock: The return of Travis
| work       = Daily Mail
| date       = 2007-09-21
| accessdate = 2010-03-12
}} 
* Late musicians Kurt Cobain and Elliott Smith claimed this was their favorite movie of all time. 
* Defunct instrumental rock band The Six Parts Seven used samples from the film at the beginning of the song "From California to Houston, on Lightspeed". The songs title is also an homage to the film.
* Jane Hendersons line "Yep, I know that feeling" is sampled on Primal Screams 1991 album Screamadelica, at the end of the song "Im Comin Down"; it is also repeated in the song "Space Angel Station" on the 1994 Drum Club album Drums Are Dangerous.
* Dialogue from the film is sampled during the song "O.O.B.E." on the album Live 93 by The Orb.
* Dialogue "Do you think he still loves her?  How would I know that Hunter? I think he does." is sampled during the song "She Stands Up" on M83 (album)|M83 by the band M83 (band)|M83.
* Scottish band Not for Pussies cite Paris, Texas as inspiration for their song "Redemption". 
* Travis Touchdown from No More Heroes is named after the main character.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 