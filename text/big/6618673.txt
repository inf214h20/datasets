Employee of the Month (2006 film)
{{Infobox film
| name           = Employee of the Month
| image          = Eotm-poster.jpg
| caption        = 
| director       = Greg Coolidge Robert L. Joe Simpson Brian Volk-Weiss
| screenplay     = Don Calame Chris Conroy Greg Coolidge
| story          = Don Calame Chris Conroy
| starring       = Dane Cook Jessica Simpson Dax Shepard
| music          = John Swihart
| cinematography = Anthony B. Richmond
| editing        = Tom Lewis
| studio         = Tapestry Films
| distributor    = Lionsgate
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $38.4 million 
}}
Employee of the Month is a 2006 American comedy film directed by Greg Coolidge, written by Don Calame, Chris Conroy, and Coolidge, and starring Dane Cook, Jessica Simpson and Dax Shepard. The main plot revolves around two shop employees (portrayed by Cook and Shepard) who compete for the affection of their newest co-worker. The film was shot primarily at a Costco in Albuquerque, New Mexico. 

==Plot== Super Club" as a box-boy. Living with his grandmother, he spends his free time with his co-workers, Lon Neilson (Andy Dick), Iqbal Raji (Brian George), and Russell Porpis-Gunders (Harland Williams). Despite his "slacker" like ways he is actually fairly kind-hearted, popular and supportive. His coworker Vince Downey (Dax Shepard) earns the Employee of the Month title for the 17th time in a row. Vince is egotistical and rude towards his co-workers, mainly Jorge Mecico (Efren Ramirez), whom he berates constantly. When new cashier Amy Renfro (Jessica Simpson) joins the staff, Zack and Vince fall for her and begin to compete for her affection. Although Amy had a dinner with Vince, she is repulsed by his attempt to put moves on her.  However, Vince does not realize Amys disgust of him, thinking that they had a good kiss, and continues pursuing her.  When Zack is told that Amy slept with the "Employee of the Month" at her last job, he decides to aim for the title.  He picks up his act and becomes a harder worker, giving Vince competition for the title. He also goes on a date with Amy, which takes place entirely in Super Club, blindsiding Vince who is determined to give Zack a tough time.

Within a few days, with Vince still racking up the daily star, Zack starts to realize that getting "Employee of the Month" is not as easy as he thought. After Iqbal gives him more encouragement, he finally finds his groove and to the horror of Vince, gets the next days star. A war of attrition begins, as Vince tries everything he can think of to derail Zacks string of stars, even breaking into his house to reset the clocks and cause him to be late. But Zack barely arrives on time, making Vinces attempt to sabotage the former unsuccessful. Zack agrees to take Iqbals shift on the day of a championship slow-pitch game against rival chain Maxi-Mart. However, he leaves to play in the game and Iqbal is fired. Frustrated at Zacks new attitude, his friends tell him he is turning into Vince and feel his attempt at getting the title is a result of trying to have sex with Amy. Amy overhears the conversation and is disgusted at Zack for his true intentions. Amy reveals to Zack that her last boyfriend, who happened to be Employee of the Month, was conniving, conceited, and impolite which was why she requested a transfer because she could not stand being with him.

At the end of the month, there is a tie between Zack and Vince. On the day of the tie-breaking competition, Zack decides to quit, and gets Iqbal his job back and tells him he took responsibility for what happened, making a heartfelt apology to Lon, Iqbal, and Russell. Zack tells them he plans to win the competition, not for recognition or to make an impression, but for pride. When the store manager is about to announce Zacks resignation, Zack, Lon, Iqbal, and Russell show up claiming Zack never filed the resignation papers. It is revealed Russell bribed the human resources manager with a broken Butterfinger. Zack also attempts to reconcile his relationship with Amy, even if it would not be romantic, he would settle for friendship by apologizing to her. Despite Vinces protests, the competition, for the fastest checkout, is held. The Employee of the Month Award is to be granted to the person who finishes the task in lesser time.

Vince manages to beat Zack by a matter of seconds, but during the award ceremony, Semi (Marcello Thedford), the security guard, brings a surveillance video of the competition which shows Vince throwing items behind his back and onto the belt without scanning them. Vince denies the allegations of shoplifting|under-ringing. The stores assistant manager, who had just completed an audit of the tills used in the competition, proved the surveillance video was right about Vince giving customers free things and has been doing this for some time, costing the store thousands of dollars. When security tape footage is shown and his register receipt totals proved he cost the store money, Vince is fired and required to wear a police tracking device in lieu of jail time. Zack ends up winning the competition having the second most points as well as the love of Amy. Six weeks following Vinces termination from Super Club, he is on probation and working at a rival store Maxi-Mart. Jorge has finally learned to assert himself and gives Vince the same poor treatment that he was once given by Vince, though is nonetheless willing to give Vince a ride to the bus stop, knowing that this would put Vince outside the range of his probation leg-tracker.

==Cast==
* Dane Cook as Zack Bradley, a box boy at Super Club and the main protagonist.
* Jessica Simpson as Amy Renfro, Zack and Vince love interest. A new cashier.
* Dax Shepard as Vince Downey, Super Clubs head cashier and the main antagonist.
* Andy Dick as Lon Neilson, Zacks co-worker, the myopic eyeglasses stand attendant.
* Brian George as Iqbal Raji, Zacks Indian co-worker, from the electronics department.
* Harland Williams as Russell Porpis-Gunders, Zacks good friend and co-worker.
* Efren Ramirez as Jorge Mecico, a box boy and secondary antagonist. Vinces wingman.
* Sean Whalen as Dirk Dittman, the assistant manager of Super Club.
* Marcello Thedford as Semi, Super Clubs scatterbrained security officer.
* Tim Bagley as Glen Garry, the manager of Super Club.
* Danny Woodburn as Glen Ross, Glen Garrys older brother who suffers from Dwarfism.

==Box office== Open Season. The film was made on a $12 million budget, and has earned a little over $28 million in the United States.

In the United Kingdom the film debuted at #4 opening with over £1,000,000 in returns. The film has grossed $34 million worldwide.

Cook posted the following on his   in regard to the films box office success:
 

==Promotion==
The film received heavy promotion from not only TV stations such as MTV, which aired sneak peeks of the film every night after the "10 Spot", but a variety of websites including sites such as Perez Hilton, Myspace, and YouTube promoted the film.
 The Today Show, Jimmy Kimmel Live!, and MTVs Total Request Live, where she was joined by Dane Cook to promote the film release.

Dane Cook appeared as the host of the 2006 season opener of Saturday Night Live to promote the film, though no mention of the film was made during his opening monologue or the subsequent sketches. 

Dane and Jessica hosted the 2006 Teen Choice Awards in order to promote the film.

==Reception==
The film received mostly negative reviews. Rotten Tomatoes has the film as having received 71 "rotten" reviews and only 18 "fresh" reviews, with a 20% ranking overall.  The consensus on that site reads: "Employee of the Month features mediocre performances, few laughs, and a lack of satiric bite."

Robert Koehler of Variety (magazine)|Variety saw some positives in the film saying "Camaraderie in cast is a key to pics enjoyment, delivered by the charismatic Cook, alongside his buddies, played by Brian George, Andy Dick (for once, not manic) and Harland Williams, with Bagley, Ramirez and Woodburn in hilarious standout turns".  Jessica Simpson earned a Razzie Award nomination for Worst Actress for her performance in the film, but lost out to Sharon Stone for Basic Instinct 2.

The film went into syndication in 2008, and has since garnered many more positive reviews. In 2011, TV Guide listed it as "one of Dane Cooks best big screen works." The consensus between both viewers and critics is that the film "was not worth the price of admission, but definitely worth watching for the first time at home."

==DVD==
The DVD was released on January 16, 2007 and opened at #2 at the sales chart, selling 603,711 units which translates to $10.2m. As per the latest figures, 1,315,439 DVD units have been sold, bringing in $20,942,856 in revenue, exceeding its budget. This does not include Blu-ray sales or DVD rentals. 
 improv scenes by actors Andy Dick and Harland Williams, an alternate opening with Eva Longoria showing Vince and Zacks first day on the job watching a video tape for new employees, an "At Work with Lon" feature showing Dick in character attempting to help to customers at Super Club, plus trailers for other Lionsgate films.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 