Lahu Ke Do Rang (1979 film)
{{Infobox film



| name           = Lahu Ke Do Rang
| image          = Lahu-ke-do-rang-1979.jpg
| image_size     = 
| caption        = DVD cover
| director       = Mahesh Bhatt
| producer       = Seeroo Daryanani   Bhagwan S.C. 
| writer         = Suraj Sanim
| narrator       =  Helen
| music          = Bappi Lahiri
| lyrics         = Faruk Kaiser
| cinematography = Pravin Bhatt
| editing        = Waman Bhosle Gurudutt Shirali
| distributor    = 
| released       =  
| runtime        = 
| country        = India 
| language       = Hindi 
| budget         = 
| gross          =  3,10,00,000   
}} 1979 Hindi movie directed by Mahesh Bhatt. The film stars Vinod Khanna, Helen Jairag Richardson|Helen, Danny Denzongpa, Shabana Azmi, Ranjeet, and Prema Narayan. The music and lyrics for the film is by Bappi Lahiri and Faruk Kaiser respectively.

The story, screenplay and dialogues of the film are written by Suraj Sanim. The film was shot in Hong Kong and India. The film was rated "hit" at the Box Office. 

==Plot==
Shamsher Singh (played by Vinod Khanna) is part of the Indian army lead by Subhas Chandra Bose to fight British out of India. With British on his back to catch him in Hong Kong, he is rescued and helped by Suzy (played by Helen (actress)|Helen). Falling in love with him, Suzy becomes pregnant with Shamshers baby. But she lets Shamsher go back to India as he is on a greater mission asking him to return to take her and their baby along with him to India. Back in India Shamsher is already married to Ladjo (played by Indrani Mukherjee). The two of them have a son named Raj. 

Shamshers friend Shankar (played by Ranjeet) betrays them all with help of Mac (played by Mac Mohan) and kills Shamsher. But Mac also betrays Shankar and hides the looted gold somewhere without telling it to Shankar.

Years later Shamshers son Raj Singh (also played by Vinod Khanna) joins police force and becomes Inspector. He is now interested in finding the murderers of his father. Mac who finishes his jail time is released and is hunted by Shankar for the looted gold. Shankar has now changed his name into Devi Dayal. Mac reveals to Devi Dayal that the gold is buried in a car that at bottom of a deep lake. To fetch the gold they hire an excellent diver Suraj (played by Danny Denzongpa). Suraj is the illegitimate son of Shamsheer and Suzy and is angry about how her mother Suzy was betrayed by Shamsher and how he never returned to get them. 

On his quest to solve a murder case, Raj goes to Darjeeling where he meets Roma (played by Shabana Azmi) and fall in love with her. Suraj is also in Darjeeling on his new job called in by Devi Dayal. Suraj too falls in love with Roma but is very hesitant to communicate. Romas tells Raj of how her mother had been hooked to drugs by Devi Dayal. Series of incidents reveal all the secrets and Raj and Suraj unite to take revenge of their fathers death.

==Cast==
*Vinod Khanna as  Inspector Raj Singh/Shamsher Singh
*Shabana Azmi as  Roma
*Danny Denzongpa as  Suraj Helen as  Suzy
*Ranjeet as  Shankar Lathuria/Devi Dayal
*Indrani Mukherjee as  Ladjo Singh
*Goga Kapoor as  Goga
*Mac Mohan as Mac

==Music==
Composed by Bappi Lahiri, the songs are penned by Faruk Kaiser.
{{Track listing
| extra_column = Singer(s)
| title1 = Chahiye Thoda Pyaar| extra1 = Kishore Kumar | length1 = 05:16
| title2 = Muskurata Hua Mera Yaar | extra2 = Kishore Kumar | length2 = 06:41
| title3 = Masti Mein Jo Nikli | extra3 = Kishore Kumar, Sulakshana Pandit | length3 = 04:23
| title4 = Zid Na Karo | extra4 = Lata Mangeshkar | length4 = 04:16
| title5 = Maathe Ki Bindiya-I| extra5 = Mohammad Rafi, Anuradha Paudwal | length5 = 06:00
| title6 = Humse Tum Mile | extra6 = Chandrani Mukherjee, Danny Denzongpa | length6 = 04:16
| title7 = Zid Na Karo | extra7 = Yesudas | length7 = 04:17
| title8 = Maathe Ki Bindiya-II | extra8 = | length8 = 
}}

==Awards==
The film won two awards at the 27th Filmfare Awards held in 1980. Helen received her first Filmfare Award after three nominations in Best Supporting Actress category earlier. Helen
*Filmfare Best Art Direction for Madhukar S. Shinde

==References==
 

== External links ==
*  

 

 
 
 