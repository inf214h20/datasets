La criada bien criada
{{Infobox film
| name           = La criada bien criada
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Fernando Cortés
| producer       = Fernando Cortés Fernando de Fuentes, Jr.
| writer         = Fernando Cortés
| screenplay     = 
| story          = 
| based on       = La criada bien criada
| narrator       = 
| starring       = María Victoria
| music          = Rafael de la Paz
| cinematography = Xavier Cruz
| editing        = Sergio Soto
| studio         = Diana Films Teleprogramas Acapulco
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
La criada bien criada ("The Well-Bred Maid") is a 1972 Mexican comedy film based on the television series of the same name. It was directed by Fernando Cortés and stars María Victoria.   

==Plot==
Inocencia has left her pueblo to find a job at Mexico City. In Mexico City, Inocencia meets two men, a deliveryman and a postman, who fight for her love. The postman finds a babysitting job for Inocencia. Inocencia has to take care of Bebito, who is more "grown up" then she expected.

==Cast==
*María Victoria as Inocencia
*Chabelo as Bebito
*Guillermo Rivas as the delivery man
*Alejandro Suárez as the postman
*Jorge Lavat
*Arturo Castro

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 