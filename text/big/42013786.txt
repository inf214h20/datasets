Paul Blart: Mall Cop 2
{{Infobox film
| name           = Paul Blart: Mall Cop 2
| image          = Paul Blart - Mall Cop 2 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andy Fickman
| producer       = {{Plainlist|
* Todd Garner
* Kevin James
* Adam Sandler}}
| writer         = {{Plainlist|
* Nick Bakay
* Kevin James}}
| starring       = {{Plainlist|
* Kevin James
* Neal McDonough
* Daniella Alonso
* David Henrie
* Raini Rodriguez
* Loni Love}}
| music          = Rupert Gregson-Williams
| cinematography = Dean Semler
| editing        = Scott Hill 
| studio         = Happy Madison Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 94 minutes 
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $75.7 million   
}}
Paul Blart: Mall Cop 2 is a 2015 American  . Kevin James stars in the film as Paul Blart, a mall cop. The other cast includes Neal McDonough, Daniella Alonso, David Henrie, Raini Rodriguez, and Vic Dibitetto. The film was released on April 17, 2015, by Columbia Pictures.
 film tax credit. On its release, the film was universally panned by critics but has grossed over $75 million worldwide.

==Plot==
Set after the events of the first film, Paul Blart (Kevin James) gets a notice of divorce since his wife, Amy was immediately disillusioned with marriage. To make things worse, his mother Margaret (Shirley Knight) is hit by a milk truck. However, Paul takes comfort in his job as a mall security guard at his mall, the West Orange Pavilion Mall. He is seen accompanying a little boy back to his mother. The mother thanks Paul and tells her son to hug him. The boy instead slaps Paul and runs off again. Pauls daughter Maya (Raini Rodriguez) just got her acceptance letter to University of California, Los Angeles|UCLA. Paul gets a letter inviting him to a security officers convention in Las Vegas. Thinking this could be something big for him, he announces this to Maya and invites her to join him. Not wanting to spoil his joy, she decides not to tell him about her acceptance just yet.

The two head off to Vegas and get accommodated at the Wynn Hotel. Paul meets the general manager, a pretty young woman named Divina Martinez (Daniella Alonso), though he believes she is making a pass at him, even though she is already seeing the hotels head of security, Eduardo (Eduardo Verástegui). Maya falls in love with the hotels valet, Lane (David Henrie). A fellow security guard from the Mall of America, Donna Ericone (Loni Love), knows of Pauls saving of his mall and drops a hint that he may be the key note speaker that night for the other guards. In the hotel, a criminal named Vincent (Neal McDonough) is plotting with his team to steal priceless works of art from the hotel, replace them with replicas, then sell the real ones at auction. They disguise themselves as employees and plan out their heist.

Paul learns that another security guard, Nick Manero (Nicholas Turturro) is giving the speech that night, crushing Pauls spirit. He also becomes too overprotective of Maya, spying on her as she talks to Lane. She sends him off after telling him that hes embarrassing her. Maya later goes out to the pool bar with Lane after telling her dad she was going back to their room. When he doesnt find her there, he panics and calls security to find Maya until she calls him from her phone. The guards leave with annoyance, and Eduardo mocks Pauls profession. Maya finds her father at the cafe, and he says he found out about UCLA from the letter in Mayas bathrobe. He wants her to stay close to home at a junior college, but Maya becomes angry and declares that shes going to UCLA.

Paul hangs out with Donna and three other guards - Saul Gundermutt (Gary Valentine) from the Philadelphia Farmers Market, Khan Mubi (Shelly Desai) from Walmart in Kings Landing, and Gino Chizzeti (Vic Dibitetto) from the Staten Island Industrial Complex - around the hotel to check out all the cool non-lethal equipment on display. Later, Paul finds Manero drunk as he tries to hit on a woman at the bar. While Paul attempts to diffuse the tension, Manero passes out, and Paul is then given the chance to be the speaker. He calls Maya to ask her to come by, but she is at a party with Lane.

As Paul prepares to give his speech, he decides to get some fresh air, only to come across a wild bird and get into a fight with it. He then goes up to give the speech, while Vincent and his cohorts begin to pull off their heist. Maya enters their suite and is subsequently taken hostage. Lane goes looking for her and is abducted as well. Paul gives a rousing speech that moves all the other security guards, as well as Divina, who finds herself inexplicably attracted to Paul with each passing moment. Maya attempts to contact Paul, only for him to ignore the calls during the speech. After it ends, Paul calls Maya and learns that Vincent has her. Paul runs to her, only for his hypoglycemia to act up and make him pass out. He gets back on his feet by catching drops of melted ice cream from a little girls cone.

Paul springs into action and takes down a couple of Vincents thugs by sheer dumb luck. He learns what theyre trying to steal and plans to stop them while also rescuing Maya and Lane. He gathers all the non-lethal equipment and takes out most of the thugs. Meanwhile, Maya escapes the room shes in using the copper wire in a snow globe that Lane gave her to unlock the door. They have to go back to the room since the thugs are too close. Maya and Lane overhear one of them offering Vincent an oatmeal raisin cookie, but he rejects it due to being violently allergic.

After taking out one thug carrying a Van Gogh painting, Paul contacts Vincent and tells him theyll trade the painting for Maya. Vincent drags Maya out after hearing her tell Lane about Pauls fear of being alone after being divorced, losing his mother, and now having his daughter go away to college. Vincent uses this against Paul, but it doesnt faze him as he got Donna, Saul, Khan, and Gino to join forces with him. They square off against all of Vincents henchmen as he takes Maya and Lane to the helipad to get away. The security guards clumsily but successfully take out the villains.

Paul gets to the rooftop to see Vincent has taken Maya and Lane to the roof of another building. Eduardo goes up to find Paul. Paul thinks he is in on the scheme, but Eduardo is just mad that Paul has attracted Divinas attention. They settle their differences and, with the help of the other guards, including a now sober Manero, Paul uses a zipline to get to the other building and stop Vincent. He uses a gun with some sticky material to take out the remaining henchmen, but he accidentally gets his own feet stuck. Vincent is about to shoot Paul when Maya finds oatmeal shampoo and rubs it all over Vincents face, causing him to go blind and to break out in hives. Paul jumps out of his shoes and headbutts Vincent, who is instantly killed.

The rest of the villains are arrested and the artwork is returned safely. Paul makes peace with Eduardo and talks to Divina, who admits her attraction to Paul, but he feels that she must stay with Eduardo. She agrees, though Paul immediately realizes he just made a huge mistake. Later, the security guards honor Paul with a medal of valor. Finally, Paul decides that he must let Maya go and supports her choice to go to UCLA. Months later, Paul drops Maya off at the UCLA campus with her new friends. Paul walks back to his car when he spots a policewoman riding a horse downtown. He tries to coolly walk over to her, but she starts to write him a ticket for jaywalking in a business district. He mentions that hes an officer, and the woman instead decides to give him her number. Paul then proudly slaps the horse on the rear, causing it to kick him against his car. The policewoman asks Paul if hes okay. He says hes never been better.

==Cast==
 
* Kevin James as Paul Blart 
* Neal McDonough as Vincent 
* Daniella Alonso as Divina Martinez 
* David Henrie as Lane 
* Raini Rodriguez as Maya Blart 
* Loni Love as Donna Ericone 
* D. B. Woodside  as Robinson
* Eduardo Verástegui as Eduardo 
* Nicholas Turturro  as Nick Manero
* Gary Valentine  as Saul Gundermutt
* Geovanni Gopradi as Ramos 
* Vic Dibitetto as Gino Chizetti 
* Ana Gasteyer  as Mrs. Gundermutt 
* Shelly Desai as Khan Mubi
* Shirley Knight as Margaret Blart, Pauls mother and Mayas paternal grandmother
 

==Production==
 
===Development=== Happy Madisons Adam Sandler.    The cast includes David Henrie,    Raini Rodriguez, Eduardo Verástegui, Nicholas Turturro, Gary Valentine,    Neal McDonough,    Daniella Alonso,    and D. B. Woodside, starring alongside with James.   

On March 14, 2014, the Nevada Film Office announced that Sony Pictures had been awarded the first certificate of eligibility for a tax credit, for the filming of Paul Blart: Mall Cop 2.  Nevada Film Office Director, Eric Preiss, indicated that the production would get $4.3 million in tax credits based on the proposal in their application.  On April 2, 2014, Columbia Pictures announced that the film would be released on April 17, 2015.   

===Filming===
In an October 2012 interview, James said that he liked the idea of filming the sequel at the Mall of America.  Principal photography commenced on April 21, 2014, at Wynn Las Vegas, and ended on June 26, 2014.    It is the first time that Steve Wynn has allowed a commercial film to be shot at this property. 

==Release==
Paul Blart: Mall Cop 2 was released by Columbia Pictures in the United States on April 17, 2015. 

===Box office===
 , Paul Blart: Mall Cop 2 has grossed $51.2 million in North America and $24.2 million in other territories for a worldwide total of $75.4 million, against a budget of $30 million. 

In its opening weekend, the film grossed $23.8 million, finishing second at the box office behind Furious 7 ($29.2 million). 

===Critical reception===
Paul Blart: Mall Cop 2 has been universally panned by critics. On  , the film has a score of 13 out of 100, based on 16 critics, indicating "overwhelming dislike". 

Sara Stewart of the   gave the film a negative review, saying "James tries hard, very hard, to inject the proceedings with slapstick humor, propelling his large body through endless physical contortions in a fruitless effort for laughs."  Justin Chang of Variety (magazine)|Variety gave the film a negative review, saying "Kevin James keeps falling on his face and colliding with heavy objects, this time in Vegas, in this tacky, numbingly inane sequel." 

Christy Lemire of  .  . To this small sample of the ever-expanding list of wretched movie sequels, add Paul Blart: Mall Cop 2, a gobsmackingly witless excuse for entertainment."  Andy Webster of The New York Times gave the film a negative review, saying "You wont find much offensive in Kevin Jamess slick, innocuous vehicle Paul Blart: Mall Cop 2. You wont find much prompting an emotional reaction in general, so familiar are the jokes and situations." 

In CinemaScore polls conducted during the opening weekend, cinema audiences gave Paul Blart: Mall Cop 2 an average grade of "B-" on an A+ to F scale. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 