Vaamanan
{{Infobox Film
| name = Vaamanan
| image = Vamanan.jpg
| director = I. Mueenuddin Ahmed|I. Ahmed
| producer = R. Ravindran
| writer = Ahmed Jai Rahman Rahman Priya Santhanam Sampath Urvashi Delhi Ganesh
| music = Yuvan Shankar Raja
| cinematography = Arvind Krishna
| editing = S. Surajkavee
| studio = Dream Valley Corporation
| distributor = 
| released =  
| runtime = 
| country = India
| language = Tamil
| budget = 10 crores
| gross  = 23 crores}}
 Tamil action Jai and Enemy of the State (1998).

==Plot==
Anand (Jai (actor)|Jai) is a young carefree boy from Salem who comes to Chennai to stay along with his friend Chandru (Santhanam (actor)|Santhanam) to pursue his dream to become an actor. Anand uses the help from his friend Chandru, a television journalist, and his newly found girlfriend, Divya (Priya Anand). Meanwhile, minister Viduthalai, who was touted to become the next chief minister, is killed by minister Anbu Chezhiyan (Sampath Raj), which accidentally gets recorded on tape of an Ad film director Vinoth. He befriends a famous actress Pooja (Lakshmi Rai). They both see the video and inform the joint commissioner Kailasam, who is also a hand-in-glove in the murder. Kailasam sends some goons to get the tape and Vinoth evades. After a long chase, they get him killed in a train, where Anand and Divya were travelling. Anand, having witnessed the murder places the tape into Divyas bag, which she misses in the train. Anand develops a friendship with her after this. In an urge to become an actor he always observes the character of people with the strange attitude and one such person is John Vijay. Anand joins him and John promises to make him an actor. He would take Anand to a strangers home, break the house and do some stuffs as if they were thief. One such house was that of Pooja, where they go and get some information regarding her. On that eve, Anand befriends Pooja, when he saves her when she was laid in the pool of blood, after being attacked by the goons. She helps him in becoming an actor and asks some photos of his to get him a chance. When Anand goes to the Poojas home, he finds her dead and Anand is put to blame (as he was the one present). Anand goes underground, finally mistaken by Divya and her mother. Atlast, it is known that John has merely used him and it was he (John) who committed this cold-blooded murder, as told by minister. Joint Commissioner Kailasam is after Anand and the tape and finally, Anand reminds of the tape accidentally put in to the handbag of Divya. The tape goes to the right hands, to that of Gopi (Poojas media friend) who gets it and publicizes. In the end, all the baddies are killed and Anand has become a real life hero instead of acting hero.

==Cast== Jai as Anand
* Priya Anand as Divya Rahman as John Vijay
* Lakshmi Rai as Pooja Santhanam as Chandru
* Sampath Raj as Anbu Chezhiyan Urvashi as Divyas mother
* Thalaivasal Vijay as Kailasam Rohini as Rohini
* Delhi Ganesh as Viduthalai
* Shanmugaraj as Sivan
* R. Ravendran as Mukhil Gopinath as Gopi

==Soundtrack==
{{Infobox album |  
| Name = Vaamanan
| Type = soundtrack 
| Artist = Yuvan Shankar Raja
| Cover =
| Released =  9 April 2009  (India) 
| Recorded = NYSA & PDMS Studio  (Mumbai)  Kalasa Studio  (Chennai)  Feature film soundtrack 
| Length = 22:23
| Label = Sony Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Sarvam (2009)
| This album  = Vaamanan (2009) 
| Next album  = Muthirai (2009)
}}

The soundtrack album of Vaamanan, composed by director Ahmeds friend Yuvan Shankar Raja, was released on 9 April 2009 at Sathyam Cinemas in Chennai.  The album features 5 songs with lyrics written by lyricist Na. Muthukumar. The romance songs "Aedho Saigirai" and "Oru Devathai" were amongst the years most popular songs.  

{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 24:47
| title1          = Aedho Saigirai
| extra1          = Javed Ali, Sowmya Raoh
| length1         = 4:50
| title2          = Lucky Star
| extra2          = Blaaze, Suvi Suresh, Mohd. Aslam
| length2         = 4:14
| title3          = Money Money
| extra3          = DJ Earl, Preethi
| length3         = 4:28
| title4          = Oru Devathai
| extra4          = Roop Kumar Rathod
| length4         = 4:56
| title5          = Enge Povadhu
| extra5          = Vijay Yesudas
| length5         = 3:55
}}

==Production==

===Development=== Jeeva in the lead role. The film was to be directed by newcomer Ahmed with music scored by Yuvan Shankar Raja and cinematography by Arvind Krishna.   However, no more news regarding the film came out until it was announced that the same director will direct the film, which was then renamed to Vaamanan with actor Jai (actor)|Jai in the lead and again with the support of Yuvan Shankar Raja and Arvind Krishna as the music composer and cinematographer respectively.

===Filming=== Saalakkudi and Hyderabad, Andhra Pradesh|Hyderabad. Other important scenes and the songs were filmed in foreign countries also like in  Thailand (Bangkok), United States (New Jersey), South Africa, Malaysia and Singapore.  
 South Indian Film. The film carries a special thanks in the title card to senior actor, Ajith Kumar, who assisted in helping fix a problem encountered with the helicopter. 

==Controversy== Jai ran into trouble for making controversial comments about the potential box office fare of his future films. The actor, who was filming for Vaamanan, Aval Peyar Thamizharasi, Adhe Neram Adhe Idam and Arjunan Kadhali at the time, revealed that only Vaamanan would do well and the rest would become financial failures. The producer of this film also revealed he wanted to take action against Jai for “making such irresponsible and damaging statements about his own films.”  Initially, the council had asked him to complete his pending assignments before he could start work on Venkat Prabhus Goa (film)|Goa, but, the producer of the film, Soundarya Rajinikanth, intervened and bailed Jai out of the ban. 

==Reviews==
Oneindia: "The first 30 minutes of the film looks very peppy and interesting and grips you to your seat, before slipping out into a run of the mill usual Kollywood style thriller".  Hindu wrote: "Despite its share of improbabilities, if Vaamanan (U/A) manages to impact the viewer to a certain extent it is mainly because of the raciness in the last lap".  Rediff wrote: "the script lets off at the intermission, and made it tauter, racier, and with at least a sprinkling of logic".  Sify wrote: "What could have been an edge-of-the-seat crime thriller falls flat in the second half due to lack of a proper script and far too many compromises made by the director". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 