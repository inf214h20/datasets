The Adventures of Dollie
 
{{Infobox film
| name           = The Adventures of Dollie
| image          = The adventures of Dollie.webm
| caption        = 
| director       = D. W. Griffith G. W. Bitzer
| producer       = 
| writer         = Stanner E.V. Taylor
| starring       = Arthur V. Johnson
| cinematography = Arthur Marvin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 12 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 debut film gypsy peddler, ends up trapped in a barrel as it floats downriver toward a waterfall.

==Plot==
On a beautiful summer day a father and mother take their daughter Dollie on an outing to the river. The mother refuses to buy a gypsys wares. The gypsy tries to rob the mother, but the father drives him off. The gypsy returns to the camp and devises a plan. They return and kidnaps Dollie while her parents are distracted. A rescue crew is organized, but the gypsy takes Dollie to his camp. They gag Dollie and hide her in a barrel before the rescue party gets to the camp. Once they leave the gypsies and escapes in their wagon. As the wagon crosses the river, the barrel falls into the water. Still sealed in the barrel, Dollie is swept downstream in dangerous currents. A boy who is fishing in the river finds the barrel, and Dollie is reunited safely with her parents.

==Cast==
* Arthur V. Johnson as Father
* Linda Arvidson as Mother
* Gladys Egan as Dollie
* Charles Inslee as Gypsy
* Madeline West as Gypsys wife
* Mrs. George Gebhardt

==See also==
* List of American films of 1908
* 1908 in film
* D. W. Griffith filmography

==References==
 

==External links==
* 
*   on YouTube
* 

 

 
 
 
 
 
 
 
 
 
 
 
 