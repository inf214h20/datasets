The Toy Wife
{{Infobox film
| name           = The Toy Wife
| image          = The Toy Wife poster.jpg
| caption        = Theatrical poster
| writer         = Zoë Akins
| starring       = Luise Rainer Melvyn Douglas
| director       = Richard Thorpe
| producer       = Merian C. Cooper Edward Ward
| cinematography = Oliver T. Marsh
| editing        = Elmo Veron
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $485,000  .  gross = $855,000 
}} 1938 drama film directed by Richard Thorpe. The period film was produced by Merian C. Cooper, written by Zoë Akins and had Luise Rainer and Melvyn Douglas in the leading roles.

==Plot==
Set during the American Civil War, The Toy Wife tells the story of Frou-Frou, a 16-year-old Flirting|coquette. She has been in France to attend a prestigious school, but is now returning to her family plantation in Louisiana. Craving to go to New Orleans, she fakes a toothache to visit a dentist there. She is chaperoned by Madame Vallaire, but soon ditches her to attend a ball. There, she meets Vaillares son Andre, a wastral whom she is immediately attracted to.

After returning home, Frou-Frou and her older sister Louise befriend Georges Sartoris, a family friend who received a knife wound after prosecuting a white man for killing a black slave. Louise is in love with him, but encourages her sister to marry him after finding out Georges is more interested in Frou-Frou. As suggested, Frou-Frou and Georges marry. Five years later, their four-year-old son celebrates his birthday. Georges is worried his wife is after all those years still youthful and flirtatious.

Fearing she would be unable to give up her life style to become attached to the household, Georges asks Louise to teach her sister how to be a wife. Things dont work out as planned and eventually it is Louise who is doing all the chores. Meanwhile, Frou-Frou becomes reacquainted with Andre while rehearsing a new play she will star in. At home, she realises her sister is taking over her life, winning over the heart of both Georges and her son. Outraged, she confronts Louise and soon elopes with Andre.

Six months later, Frou-Frous father Victor is informed by Madame Vallaire that his daughter and Andre are currently living in New York City. Distraught, Victor collapses and dies the same day. Meanwhile, Frou-Frou and Andre are living in poverty due to Andres gambling debts. Her fathers will leaves her with half of his plantation, but she forfeits her share to her son Georgie.  When she and Andre return to New Orleans, jealous ex-husband Georges challenges Andre to a duel. Everybody suspects Andre will win, but he is shot and killed.  It is hinted that Andre had purposely chosen to be the loser in the duel, because he chose pistols as the weapons, rather than his actual preference of swords. 

Time goes by and Frou-Frou is now a poor woman, dying of pneumonia. One evening, while praying in church, she is noticed by Louise. She makes Georges realize that Frou-Frou become the woman she was for him, explaining that was what he really wanted. Touched, he visits Frou-Frou and finally allows her to see her son again. He takes her back home and is told there by Frou-Frou he should marry Louise. Soon after, Frou-Frou dies.

==Cast==
*Luise Rainer as Gilberte Frou Frou Brigard
*Melvyn Douglas as George Sartoris Robert Young as Andre Vallaire
*Barbara ONeil as Louise Brigard
*H. B. Warner as Victor Brigard
*Alma Kruger as Madame Vallaire
*Libby Taylor as Suzanne
*Theresa Harris as Pickaninny Pick
*Walter Kingsford as Judge Rondell
*Clinton Rosemond as Pompey
*Clarence Muse as Brutus
*Leonard Penn as Gaston Vincent

==Production== 1938 in movie adaption was already in works. With Warner Bros. distributing Jezebel (film)|Jezebel as well, Metro-Goldwyn-Mayer knew they couldnt be left behind. A Civil War-themed film was rushed into production, eventually released as The Toy Wife. A Rose for Mrs. Miniver by Michael Troyan p. 78 

The film starred  s only, including The Toy Wife.

There was a schedule conflict during the production, which forced director Richard Thorpe to withdraw from the direction of The Shopworn Angel (1938), a drama film starring Margaret Sullavan and James Stewart.  There was a lack of a big budget. By casting Rainer, the studio couldnt afford a famous male lead and assigned Melvyn Douglas instead.  Greer Garson was at one point considered for the role of Louise, but Rainer and Thorpe objected to the idea. 

==Reception==
Although the studio thought the film would be a hit, it became a flop.  According to MGM records the film earned $557,000 in the US and Canada and $298,000 elsewhere resulting in a loss of $29,000. 

The Toy Wife was dismissed as a tearjerker and the plot was heavily criticized.  Rainer received a lot of negative criticism, with The New York Times calling her portrayal "wound too tightly for anybodys comfort". The critic also noted she was being "too feminine".  Neither she nor the studio was happy with the results.

Rainer told her casting in this movie was a failed punishment, because she insisted she loved working with Douglas. Rainer was known for hating Hollywood and all the insiders, but described Douglas as a loving person, who was one of the few caring about more than acting.  Nevertheless, Rainer ended her contract with the studio the same year and retired from the film industry.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 