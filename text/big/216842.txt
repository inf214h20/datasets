Day of the Fight
{{Infobox film
| name           = Day of the Fight
| image          = Day of the Fight title.jpg
| image_size     = 215px
| caption        = title card
| director       = Stanley Kubrick
| producer       = Stanley Kubrick Jay Bonafield (uncredited)
| writer         = Robert Rein (narration) Stanley Kubrick
| narrator       = Douglas Edwards
| starring       = Walter Cartier Vincent Cartier
| music          = Gerald Fried
| cinematography = Stanley Kubrick Alexander Singer
| editing        = Julian Bergman Stanley Kubrick (uncredited)
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 16 minutes
| country        = United States
| language       = English
| budget         = US$3900
| gross          =
}}
Day of the Fight is a 1951 American short subject documentary film shot in black-and-white and also the first picture directed by Stanley Kubrick. Kubrick financed the film himself, and it is based on an earlier photo feature he had done as a photographer for Look (American magazine)|Look magazine in 1949.  

 

==Story== boxer Walter Cartier during the height of his career, on the day of a fight with black middleweight Bobby James, which took place on April 17, 1950.

The film opens with a short section on boxings history, and then follows Cartier through his day, as he prepares for the 10 P.M. bout that night.  He eats breakfast in his West 12th Street apartment in Greenwich Village, then goes to early mass and eats lunch at his favorite restaurant.  At 4 P.M., he starts preparations for the fight.  By 8 P.M., he is waiting in his dressing room at Laurel Gardens in Newark, New Jersey for the fight to begin.

We then see the fight itself, where he comes out victorious in a short match.   Jeff Stafford   

==Cast==
*Douglas Edwards as Narrator (voice only)
*Walter Cartier as Himself (uncredited)
*Vincent Cartier as Himself -  Walters twin brother (uncredited)
*Nat Fleischer as Himself - boxing historian (uncredited)
*Bobby James as Himself - Walters opponent (uncredited) 
*Stanley Kubrick as Himself - man at ringside with camera (uncredited) 
*Alexander Singer as Himself -man at ringside with camera (uncredited) 
*Judy Singer as Herself - female fan in crowd (uncredited)

===Cast notes===
*The year after the fight chronicled in Day of the Fight took place, Walter Cartier made boxing history by knocking out Joe Rindone in the first forty-seven seconds of a match (16 October 1951). Cartier had played some bit parts in movies before he appeared in Day of the Fight, and afterwards continued to appear occasionally in movies up until 1971, but he was most successful playing mild-mannered Private Claude Dillingham on the sitcom The Phil Silvers Show for the 1955-1956 season. 
 the Bronx), The Killing, and went on to have a long career as a director of hour-long TV dramas. 

*Douglas Edwards, who narrated Day of the Fight was a veteran radio and television newscaster.  At this time, he was the anchor for the first daily television news program, on CBS, which would later be called Douglas Edwards with the News, and then The CBS Evening News.  Edwards was replaced by Walter Cronkite in 1962, but remained a noted voice on CBS Radio news programs until he retired in 1988. 
 
 

==Production==
Kubrick and Alexander Singer used daylight-loading Eyemo cameras that take 100-foot spools of 35mm black-and-white film to shoot the fight, with Kubrick shooting hand-held (often from below) and Singers camera on a tripod.  The 100-foot reels required constant reloading, and when the knock-out punch which ended the bout came, Kubrick didnt catch it because he was reloading.  Fortunately, Singer did. 

Day of the Fight is the first credit on composer Gerald Frieds resume.  Fried, a childhood friend of Stanley Kubrick, went on to score or conduct (or both) over 100 films.   In 1977, he shared an Emmy Award with Quincy Jones for the music for the TV mini-series Roots (TV miniseries)|Roots, and was nominated for an Academy Award in 1976 for Birds Do It, Bees Do It. 

Although the original planned buyer of the picture went out of business, Kubrick was able to sell Day of the Fight to RKO Pictures for $4,000, making a small benefit of $100 above the $3,900 cost of making the film. Joseph Gelmis  , excerpted from The Film Director as Superstar New York: Doubleday, 1970. 

According to Jeremy Bernstein (http://www.brainpickings.org/index.php/2013/11/27/jeremy-bernstein-stanley-kubrick-interview/?utm_source=buffer&utm_campaign=Buffer&utm_content=buffer8c995&utm_medium=twitter) the film lost $100 as documented in this 11/27/1965 voice recording of interview with Stanley Kubrick:  (https://soundcloud.com/brainpicker/a-rare-interview-with-stanley).

Day of the Fight was released as part of RKO-Pathés "This Is America" series and premiered on 26 April 1951 at New Yorks Paramount Theater, on the same program as the film My Forbidden Past.  Frank Sinatra headlined the live stage show that day. 

==Notes==
 

==External links==
*  
*  
*  
*   at  
*   at  
*  
* Watch  

 

 
 
 
 
 
 
 
 
 
 
 
 
 