Wandering Papas
{{Infobox film
| name           = Wandering Papas
| image  	 = Wandering Papas FilmPoster.jpeg
| caption        = Film poster
| director       = Stan Laurel
| producer       = Hal Roach
| writer         = Stan Laurel H.M. Walker Hal Yates
| starring       = Oliver Hardy
| music          = 
| cinematography = Glen Carrier Len Powers Frank Young
| editing        = Richard C. Currier
| distributor    = 
| released       =  
| runtime        = 8 minutes
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 Clyde Cook, featuring Oliver Hardy, and directed by Stan Laurel.   

==Cast== Clyde Cook - The camp cook
* Oliver Hardy - The foreman (as Babe Hardy)
* Sue ONeill - Susie, the hermits daughter
* Tyler Brooke - Onion, a bridge engineer
* Adolph Milar - The hermit (as Adolph Millar)

==See also==
* List of American films of 1926
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 