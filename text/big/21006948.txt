Wheat (film)
{{Infobox film name = Wheat image = Wheat poster.jpg alt =  caption = Film poster director = He Ping producer = He Ping Yu Dong Zhao Guoliang writer = He Ping starring = Fan Bingbing Wang Jue Du Jiayi Wang Xueqi Wang Zhiwen music = Liu Xing cinematography = Zhao Xiaoshi editing =  studio =  distributor =  released =   runtime = 108 minutes country = China language = Mandarin budget =  gross = 
}}
Wheat ( ) is a 2009 Chinese historical drama film directed by He Ping, starring Fan Bingbing, Wang Jue, Du Jiayi, Wang Xueqi and Wang Zhiwen. The film tells the story of women left behind when their husbands went to war.

The film was produced for US$6 million and was funded by He Pings own Beijing Classic Culture, along with Polybona Films, and the state-backed Xian Film Studio.   

==Cast==
*Fan Bingbing as Li
*Huang Jue as Xia
*Du Jiayi as Zhe
*Wang Xueqi as Lord Ju Cong
*Wang Zhiwen as Chong
*Wang Ji
*Li Ge
*Sun Guitian
*Wang Jiajia as Yan

==Release==
Filmed in 2008, Wheat premiered on June 13, 2009, as the opening film to the 2009 Shanghai International Film Festival. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 