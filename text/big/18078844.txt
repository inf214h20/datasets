Woos Whoopee
Woos Pat Sullivan.

== Summary ==
The short opens with a long shot of a large, urban building swaying in time to the loud strains of   is in her right hand and her frequent looks at the time and her angry expressions make it clear as to her intended use of the implement.

The next scene is back at the Whoopee Club. Felix and a female rabbit are shown dancing solo in what appears to be a dance contest. The couple stops and each pulls out a bottle of beer from within their selves and drink them in unison to the cheers of the onlooking animals.

A clock tower showing a time of 3:00 raises its own dome. A hammer at the end of a mechanical arm extends out toward the left and strikes the hour on the dome; the moon has a face a la the man in the moon which visibly reacts to the sounding of the clock tower. Things are starting to quiet down in The Whoopee Club, but Felix continues to revel with party favors, using one to retrieve a bottle of beer from an unsuspecting rabbit patron. He downs the bottle, laughing the entire time and pirouettes into the camera, momentarily obscuring the scene until he pirouettes away, still laughing.

Mrs. Felix is now shown waiting and pacing by the grandfather clock (which shows a time of 2:30) in much the same style as her husband. An inebriated Felix waves a goodbye through the front door of the club to the patrons inside, falling off the front step as he turns to leave. He tips the entire top of his head as if it were a hat and staggers toward the camera and out of frame to the left.

Felixs hallucinations now begin in earnest as he staggers down the street with the surrounding buildings swaying in time to his steps. He staggers toward a street lamp and hugs the base for support. The lamp seems to come to life, kicking Felix away and then dusting itself off with a whisk broom. The lamp flings the broom in Felixs face and walks away. Felix tries to remove the broom, but the bristles remain in his face which he strokes as if it were a natural beard. He removes his tail, turns it into a walking stick and staggers after the street lamp in the manner of an arthritic old man. The lamp is seen sashaying down the street as if it were a woman with Felix in slow pursuit. The broom falls from Felixs face as he in turn falls to his knees, begging the street lamp to stop. Felix once again tips his ears in a salute; the street lamp reacts as if it were a shy young girl. Felix attempts to steady himself at the base of the lamp, but it shifts slightly causing him to fall. The lamp reacts with a funny little dance which infuriates Felix. He lunges for the street lamp which now turns into a sort of smoke-blowing dragon. Felix turns and runs toward the camera while the dragons open mouth momentarily obscures the scene as it begins its pursuit.
 atomizer and "extinguishes" the dragon, which disappears. The tip of Felixs tail was left slightly smoking, so he removes the tip and begins to smoke it as if it were a cigar.

The hallucinations continue as we see a charging elephant, lion and gorilla begin their pursuit. As Felix turns to run, a second lion appears, blocking his path. Felixs sudden lunge away from the lion flings him head first into a distant tree. More hallucinations appear in the form of a second elephant which morphs into a winged ape.
 bugle which sprouts legs and a horselike tail and takes off after the fleeing Felix.

Felixs pursuits end as he enters the front door of his home. He peeks through the keyhole and sees a bizarre, round-bodied creature flapping its wings and blinking its eyes. Felix laughs off this latest hallucination, removes his tail and inserts it into an umbrella stand. As he laughs and staggers, he suddenly remembers his wife, still pacing before the clock which now reads 6:00. Felix removes his shoes at the foot of the stairs before tiptoeing up. The stairs move as if they were piano keys as Felix falls to the foot of the stairs. Another hallucination begins; Felix finds himself back outside as a chicken laughs at his plight. As he begins to beat up the chicken, we see that in reality he is standing at the foot of his wifes bed punching out a feather pillow. An alarm clock on the windowsill shows a time of 3:00, but when Mrs. Felix kicks Felix off the bed, he lands in front of a cuckoo clock displaying a time of 6:00. As the cuckoo clock chimes six, Felix draws a large pistol from his pajamas and shoots the cuckoo. The effect of the explosion fades the cartoon to black, ending the story.

== External links and references ==
*  
*  

 
 
 
 
 
 