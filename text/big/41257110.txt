Il tenente dei carabinieri
{{Infobox film
 | name = Il tenente dei carabinieri
 | image = Il tenente dei carabinieri.jpg
 | caption =
 | director =  Maurizio Ponzi
 | writer =  Leo Benvenuti Piero De Bernardi Maurizio Ponzi
 | starring =  Nino Manfredi Enrico Montesano Massimo Boldi
 | music =  Bruno Zambrini
 | cinematography =   Carlo Cerchio 
 | editing =  	Antonio Siciliano 
 | producer =  Mario Cecchi Gori & Vittorio Cecchi Gori
 | released = 1986
 | country = Italy
 | language = Italian
 | runtime = 110 min
 }}
Il tenente dei carabinieri (The lieutenant of Carabinieri) is a 1986 Italian Crime film|crime-comedy film directed by Maurizio Ponzi.   

==Plot==
The Lieutenant of Police Duilio Cordelli often quarrels with his Commander Colonel Vinci. When the two must investigate two murders suspect, Duilio discovers that a gang of thieves plan to cover a major bank heist in Rome with a traffic of counterfeit notes.

== Cast ==
* Enrico Montesano: Lt. Duilio Cordelli 
* Nino Manfredi: Colonel Vinci
* Massimo Boldi: Brigadiere Nautico Lodifei
* Marisa Laurito: wife of Cordelli
* Mattia Sbragia : Domenico Vasaturo
* Nuccia Fumo : Mother of Cordelli
* Bruno Corazzari: Augusto Lorenzini

==References==
 

==External links==
* 

 
 
 
 
 


 
 