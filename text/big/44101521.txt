Ee Thanalil Ithiri Nerum
{{Infobox film 
| name           = Ee Thanalil Ithiri Nerum
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = Hameed KT Kunjumon John Paul John Paul
| starring       = Mammootty Shobhana Thilakan Adoor Bhasi Shyam
| cinematography = Jayanan Vincent
| editing        = G Murali
| studio         = Rachana Pictures
| distributor    = Rachana Pictures
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by PG Vishwambharan and produced by Hameed and KT Kunjumon. The film stars Mammootty, Shobhana, Thilakan and Adoor Bhasi in lead roles. The film had musical score by Shyam (composer)|Shyam.    

==Cast==
*Mammootty
*Shobhana
*Thilakan
*Adoor Bhasi Rohini
*Baby Chaithanya Rahman
*Sumithra Sumithra

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa Ramya Sreerangame || S Janaki || Poovachal Khader || 
|-
| 2 || D.I.S.C.O (Swarnathaamara Kiliye) || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|-
| 3 || Maanam Mannil || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 4 || Mummy Mummy || S Janaki || Poovachal Khader || 
|-
| 5 || Poovaninju Maanasam || K. J. Yesudas, S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 