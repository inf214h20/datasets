22 Female Kottayam
{{Infobox film
| name           =  22 Female Kottayam
| image          = Female22.jpg
| alt            =  
| caption        = Promotional poster
| director       =Aashiq Abu
| producer       = O. G. Sunil
| writer         = Abhilash S Kumar Syam Pushkaran
| starring       = Rima Kallingal Fahadh Faasil
| music          = Bijibal Rex Vijayan  (Film score|BGM) 
| cinematography = Shyju Khalid
| editing        = Vivek Harshan
| studio         = Film Brewery
| distributor    = PJ Entertainments Europe
| released       =   
| runtime        = 122 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Indian thriller drama film directed by Aashiq Abu and starring Rima Kallingal and Fahadh Faasil in the lead roles.  The story concerns the travails of 22-year-old nurse Tessa (Rima Kallingal) from Kottayam who was raped and who takes revenge on her tormentors. Set and filmed in Bangalore, it released on April 13, 2012, and received strongly positive reviews from critics. It was also well received at the box-office.  Rima Kallingal won the Kerala State Film Award for Best Actress for her performances in 22 Female Kottayam and Nidra (2012 film)|Nidra. 

==Plot==
Tessa (Rima Kallingal) is a nursing student in Bangalore with plans of traveling to Canada for a career. She meets Cyril (Fahadh Faasil) from the travel consultancy agency working towards setting up her visa. They soon fall in love and start living together. Tessa loves him with all her heart and takes their relationship and living together seriously.

One day while at a pub, a guy misbehaves with Tessa and Cyril beats him up badly. The guy tries to take revenge on Cyril and searches for him. Cyril goes into hiding with the help of his boss Hegde (Prathap Pothen). Hegde arrives at Cyrils home to inform Tessa about the situation. Then he asks her plainly "Can I have sex with you?" A shocked Tessa is then brutally attacked and raped. When Cyril finds out what happened, he becomes violent and wants to kill Hegde. Tessa calms him down saying that she does not want to make the incident worse than it is; she instead wants to get to Canada at the earliest. Once Tessa recovers from her injuries, Hegde visits her again to ask for forgiveness. He comes while Cyril is not around and rapes her for a second time. Tessa decides not to travel abroad and plans to murder Hegde.

Cyril discusses the situation with his boss who suggests killing Tessa and appoints Cyril to do it. Cyril traps her by putting some drugs in her bag. The police arrest Tessa and she is imprisoned. While Tessa calls out for help, she finds Cyril simply walking away from her, which is when she realises Cyril set her up. Cyril relocates to Cochin and runs a modeling agency.

While in prison Tessa meets Zubeida (Rashmi Sathish) who is sentenced for murder. Through Zubeidas criminal world connections Tessa realizes that Cyril, a pimp, was cheating her along with the support of his boss Hegde. Zubeida and Tessa bond well with each other, and Zubeida gives her the strength and courage needed for striking back at Cyril and Hegde.

When the court sets her free, Tessa with the help of DK (Sathaar) kills Hegde by poisoning him with a cobra. Next, she arrives in Cochin in search of Cyril while pretending to be a model. Later one night they meet in his studio but Cyril recognizes her and becomes angry. He beats her and verbally abuses her not because he knows she has a plan of revenge but thinks that she became a slut who does any adjustment to flourish her career. But his frustration dissolves as he wants to enjoy her company. He reminds her she is a mere woman.
 penectomises him. When he regains consciousness she tells him that she has removed his male organ through a medical surgery. While Cyril finds himself in intense pain and bound to his bed, she taunts him to make him realize his faults and the gross wrongs he committed to her. But he doesnt yield and doesnt admit his life as a pimp is a fault. 

Another twist happens when Tessa tells Cyril that her appearance as a model is a disguise. She knows that she is still somewhere in his heart. She reminds him that she has only lost someone who cheated on her, whereas he lost someone who genuinely loved him. Now Cyril is stunned that he is not even able to face Tessa. Cyril recollects that her love was true and his love was overshadowed by his male supremacy concept and greed for wealth. He understands and admits his mistakes. Then Tessa leaves him but not before inviting him to settle the score with her, if any remains there. Cyril corrects his concept of her as she is the woman and accepts her challenge and tells her that he will confront her when he is ready, knowing that he has to settle the score with her in terms of true love.

Tessa leaves for Canada, dismantles her cell phone, and cuts further contact with DK.

==Cast==
*Rima Kallingal as Tessa K. Abraham
*Fahadh Faasil  as Cyril C. Mathew
* Prathap Pothan as Hegde
* Riya Saira as Tissa K. Abraham
* T. G. Ravi as Ravi
* Sathaar as DK
* Rashmi Sathish as Zubeida
* Srinda Ashab as Jincy
* Vijay Babu as Benny

==Music==
{{Infobox album
| Name = 22 Female Kottayam
| Artist = Rex Vijayan
| Type = Soundtrack
| caption = 
| Cover = 
| Released = 
| Recorded =
| Genre = Film soundtrack
| Length = 
| Language = Malayalam
| Label = Manorama Music
|| Producer = Rex Vijayan
| Last album = Chaappa Kurishu (2011)
| This album = 22 Female Kottayam
| Next album = English (film)|English (2013)
}} Rex Vijayan. 
{{tracklist
| headline = 
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = 
| title1 = Chillaane
| lyrics1 =  
| extra1 = Tony,Neha Nair
| length1 = 
| title2 = Melle Kollum
| lyrics2 = 
| extra2 = Job Kurian,Neha Nair
| length2 = 
| title3 = Neeyo
| lyrics3 = 
| extra3 = Bijibal,Neha Nair
| length3 = 
}}

==Production==
Ashik says that the concept of the film was there in his mind since many years. "This concept was there in my mind when I was working on my maiden venture,  ) along with Shyam Pushkaran of Salt N Pepper fame. Kumar had worked with Ashik in Daddy Cool and Salt N Pepper as an assistant director. Kumar says the story is loosely based on real-life stories about young women who move out of Kerala to Hyderabad, India|Hyderabad, Orissa and Bangalore in search of nursing jobs to eventually go abroad.  The production controller of 22 Female Kottayam is Shibu G Suseelan.

22FK started production in December 2011 and was mostly filmed from Bangalore.  Shyju Khalid wielded the camera. He says that the film, which was shot on a multi-camera, was a big challenge for him. "It is a thriller-cum-romance movie. In the first half, there is romance and suddenly a rape scene, so I had to add a lot of grays to give that effect. And, the climax was the most challenging part, as it’s very dramatic and I had to add the proper effect to retain the mood. Here, the subject was a little dark so I could experiment, but in normal movies you can’t do much of an experiment. Also, you have to keep in mind the taste of Malayalis," says Khalid. 

===Inspirations=== If Tomorrow Comes by Sidney Sheldon.

# The jail scenes (including the character of her cell-mate)
# The planned killings of all those people who cheated her
# Revenge of her lover in the climax

The basic idea is based on  . 
  

22FK was also reportedly inspired from the 1978 film I Spit on Your Grave, which was remade in I Spit on Your Grave (2010 film)|2010.

==Critical reception==
22 Female Kottayam received positive critical response upon its release.
 One India commented that "22 Female Kottayam is like a fresh whiff of breeze" and concluded the review by adding, "Cheers for director Ashik for experimenting with such a novel and path breaking plot."  Rediff also rated the film 3 out of 5 star praising the direction of Ashik by saying "Ashik adds a distinct style to his direction with 22 Female Kottayam."  Sify rated it as good and said that "22 Female Kottayam may be far from perfect, but it is a movie in the right direction. Such efforts should be well appreciated, as it is a step that leads to better cinema."  NowRunning rated the film 3/5 and said "Big hugs, Mr. Abu for gifting us with this fighter female, 22 years old, from Kottayam. And an extra big hug for not parading her as a virgin."  Metro Matinee rated film as "Excellent" and said "Ashiks 22FK is a refreshing theme with superb execution." 

==Awards==
; Asiavision Movie Awards (2012)  . The Gulf Today. 23 October 2012. Retrieved 11 November 2012. 
* Best Actress - Rima Kallingal
* Performer of the Year - Fahadh Faasil

; Filmfare Awards South (60th Filmfare Awards South|2013)  Best Actor - Fahadh Faasil Best Actress - Rima Kallingal Best Film - 22 Female Kottayam (Nominated) Best Director - Aashiq Abu (Nominated) Best Supporting Actress - Rashmi Satheesh (Nominated)

; Mohan Raghavan Awards (2012) 
* Best Director - Aashiq Abu

; South Indian International Movie Awards (2nd South Indian International Movie Awards|2013) Best Actor in a Negative Role - Prathap K. Pothan Best Actress (Critics) - Rima Kallingal Best Debutant Producer - O. G. Sunil Best Director - Aashiq Abu

== Remakes == Malini 22 Vijayawada, with Nithya Menen and Krish J. Sathaar playing the lead roles.

==References==
 

==External links==
*  
*  
* 22 Female Kottayam at  
 
 
 
 
 
 
 
 
 
 
 
 