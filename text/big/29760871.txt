And Once Again
{{Infobox film
| name           = And Once Again
| image          = Andonceagain.jpg
| caption        = 
| director       = Amol Palekar
| producer       = 
| writer         = 
| screenplay     = 
| story          = Sandhya Gokhale
| based on       = 
| starring       = Rajat Kapoor Antara Mali Rituparna Sengupta
| music          = Anjan Biswas Debjyoti Mishra
| cinematography = Asim Bose
| editing        = Abhijeet Deshpande
| studio         = Shangrila Kreations Parth Productions 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi English  . Bollywood Hungama. Retrieved November 2, 2014. 
| budget         = 
| gross          = 
}}
And Once Again is a 2010 Indian family drama film directed by Amol Palekar. The film premiered in India on 20 August 2010.

==Plot==
Rishi (Rajat Kapoor) is coming to terms with the loss of his wife and son while being posted in a foreign country. Trying his level best to let go of the past and move on, he takes help of a therapist whose daughter Manu (Rituparna Sengupta) he marries at a later stage. During a visit to Sikkim, Rishi stumbles upon a female monk who reminds him of his first wife Savitri (Antara Mali). There is an uncanny resemblance between the two. It not only baffles him but also sends his life spiralling out of control. After sharing his experience with Manu and telling her about the similarity, he decides to investigate the situation. What follows is an unpredictable journey where he revisits his past, confronts the present and questions his future. 

==References==
 

==External links==
  at Bollywood Hungama

 
 
 
 
 
 
 


 
 