Private Hell 36
{{Infobox film
| name           = Private Hell 36
| image          = Private hell 36.JPG
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Don Siegel
| producer       = Collier Young
| screenplay     = {{plainlist|
* Collier Young
* Ida Lupino
}}
| starring       = {{plainlist|
* Ida Lupino
* Steve Cochran
* Howard Duff
}}
| music          = Leith Stevens
| cinematography = Burnett Guffey
| editing        = Stanford Tischler
| studio         = The Filmakers
| distributor    = Filmakers Releasing Organization
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
}}
Private Hell 36 is a 1954 black-and-white film noir, directed by Don Siegel.  It features Ida Lupino, Steve Cochran, Howard Duff, among others. 

The picture was one of the last feature-length efforts by Filmakers, a company created by producer Collier Young and his star and then-wife Ida Lupino.

==Plot==
L.A. police detectives Cal Bruner (Steve Cochran) and Jack Farnham (Howard Duff) get in over their heads when they decide to split up thousands of dollars they found on a recently killed counterfeiter.  To make matters worse, they are assigned by their police captain to look for the missing cash.

Things get even worse when one cop gets romantically involved with Lili Marlowe (Ida Lupino), a money-hungry nightclub singer. Farnham decides to turn honest and hand the money over to his superiors, but the other cop decides to take it all.

==Cast==
* Ida Lupino as Lilli Marlowe
* Steve Cochran as Cal Bruner
* Howard Duff as Jack Farnham
* Dean Jagger as Capt. Michaels
* Dorothy Malone as Francey Farnham
* Bridget Duff as Bridget Farnham
* Jerry Hausner as Hausner, Nightclub Owner
* Dabbs Greer as Sam Marvin, bartender
* Chris OBrien as Coroner
* Kenneth Patterson as Detective Lieutenant Lubin (as Ken Patterson)
* George Dockstader as Fugitive
* Jimmy Hawkins as Delivery Boy

==Background==
The racetrack scenes in the film were shot at Hollywood Park Racetrack in Inglewood, California.

==Reception==
Film critic Bosley Crowther wrote a tepid review, "A critic might note that attention is sharply divided between the main theme and the incidental character that Miss Lupino plays. This is somewhat understandable, since Miss Lupino happens to be one of the partners in Filmakers and a coauthor of the script. But lets not worry about it. No deplorable damage is done. Theres not very much here to damage. Just an average melodrama about cops." 

==References==
 

==External links==
*  
*  
*  
*  
*   at Film Noir of the Week by Megan Abbott

 

 
 
 
 
 
 
 
 
 
 