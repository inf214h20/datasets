Registe
 
{{Infobox film
| name           = Registe
| image          = Maria de Medeiros (Elvira Notari) in REGISTE.jpg
| alt            = 
| director       = Diana DellErba
| producer       = Louis Nero
| writer         = Diana DellErba
| starring       =  
| music          = Giulio Castagnoli
| cinematography = Diana DellErba
| editing        = Diana DellErba
| studio         = LAltrofilm
| distributor    = LAltrofilm
| released       =  
| runtime        = 76 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} Italian independent film written and directed by Diana DellErba.

==Plot==
Registe, talking on a blade is an Italian documentary about the Italian Cinema signed by women and about the pioneer of the Silent Cinema Elvira Notari (1875-1946) plays by Maria De Medeiros. The directors interviewed are the most important Italian women directors: Lina Wertmüller, Cecilia Mangini, Francesca Archibugi, Francesca Comencini, Wilma Labate, Cinzia Th Torrini, Roberta Torre, Antonietta De Lillo, Giada Colagrande, Donatella Maiorca, Ilaria Borrelli and other.

==Cast==
 
*  
* Lina Wertmüller: herself
* Cecilia Mangini: herself
* Francesca Archibugi: herself
* Francesca Comencini: herself
* Cinzia Th Torrini: herself
* Roberta Torre: herself
* Giada Colagrande: herself
* Donatella Maiorca: herself
* Gian Luigi Rondi: himself
* Anselma DellOlio: herself
* Silvana Silvestri: herself
* Eliana Lo Castro Napoli: herself
* Eugenio Allegri: Vincenzo Caccavone
* Gian Maria Villani: Gennariello
* Marco Sabatino: Nicola Notari
* Rebecca Volpe: Goddess Lilith
* Ilaria Borrelli: herself
* Maria Sole Tognazzi: herself
* Antonietta De Lillo: herself
* Anne Riitta Ciccone: herself
* Alina Marazzi: herself
* Donatella Baglivo: herself
* Elisa Mereghetti: herself
* Anna Negri: herself
* Nina Di Majo: herself
* Paola Randi: herself
* Susanna Nicchiarelli: herself
* Stefania Bonatelli: herself

 

==References==
 

==External links==
*  
*  

 
 
 

 