Akenfield
 
Akenfield is a film made by   (1969). It can claim a degree of cult status - and has been described as a work of rural realism, unusual in relation to East Anglia. Blythe himself has a cameo role as the vicar and all other parts are played by real-life villagers who improvised their own dialogue. There are no professional actors in the piece. The directors father Reg Hall, a station master born in Bury St Edmunds, appears briefly as the village policeman walking down a lane with a bicycle. Blythes book is the distillation of interviews with local people, and his technique is somewhat echoed in the pioneering verbatim theatre style developed in London Road at the National Theatre in 2011. Akenfield the film is a work of fiction, based on an 18-page story synopsis by Blythe. Most of the filming was done at weekends, when the cast was available, and shooting took almost a year - following the changing seasons in the process.

The music was intended to be written by Benjamin Britten, himself a Suffolk man, but he suffered a heart attack and was unable to work. Instead, Hall chose Michael Tippett, also from East Anglia, and a friend and colleague - they had worked together at Londons Royal Opera House. Tippetts Fantasia Concertante on a Theme of Corelli plays a major role in the emotional timbre of the film.   

==Background== The Countryman, Summer 1973, p113-121. 

Akenfield is a made-up placename based partly upon Akenham (a small village just north of Ipswich, the county town of Suffolk, England|Suffolk) and probably partly on Charsfield, a village just outside the small town of Wickham Market, Suffolk, about ten miles north-east of Akenham. The film of Akenfield was made on location in the villages just west of Wickham Market, notably Hoo, Suffolk|Hoo, Debach, Charsfield, Monewden, Dallinghoo, Letheringham and Pettistree. The actors in the film were non-professional, drawn from the local population, and therefore speak with authentic accents and play their parts in a manner unaffected by the habit of stage or screen performance. After making the film, most returned to usual rural occupations. 
 John Moores Brensham or Elmbury. The film is a remarkable translation of this scholarly view into a portrait of a rural community told through the eyes of one of its members. In seeing through his eyes, we also see through the eyes of his ancestors.

Blythe had spent the winter of 1966-7 listening to three generations of his Suffolk neighbours in the villages of Charsfield and Debach, recording their views on education, class, welfare, religion, farming and also death. Published in 1969, the book painted a picture of country living at a time of change - its stories told in the voices of the farmers and villagers themselves. Such was its power that Akenfield was translated into more than 20 languages, including Swedish. It became required reading in American and Canadian high schools and universities. In 1999 Penguin re-published it as a Twentieth-Century Classic, which helped bring it to a new audience of readers.

In discussions prior to filming Blythe and Hall talked about Robert Bressons films of French rural life and Man of Aran.  One of the major challenges of the filming was to recreate the sense of a rural economy based around horses; Blythe considered one of the best scenes the evocation of a harvest around 1911, complete with "Suffolk waggons, the biggest in England, and heroic punches to draw them". 

The film lasts 98 minutes.

== Plot == Newmarket for a job, took a wife in the village and lived in a tied cottage on the farmers estate for the rest of his life. His son, Toms father, was killed in the Second World War, and Tom has grown up hearing all sorts of stories from his grandfather. Everyone around him says what a good old boy his grandfather was, and remembers the old days, but all Tom can hear is the words of his grandfather ringing in his ears, and now in 1974 he is making his own plans to get away, with or without his girlfriend. The cycle goes round and round, and all the time the customs and the landscape are so colourful and beautiful, with prominent and striking use of music by Michael Tippett (Fantasia Concertante on a Theme of Corelli), but with the skull-like menace of poverty, entrapment and war grinning through the veil of rural beauty. Will Tom be defeated by the land and the hard work, just as his grandfather was?  Shand plays all three generations, grandfather, father and son.

== Literary environment == reaper finding he has just killed the sitting bird.]]

* For East Anglian folklore, perhaps in such scenes as Hollering largesse, there is an allusion to the work of John Glyde Jnr in The New Suffolk Garland.
* Past and present, and the experiences of successive generations, merge in the way suggested by T. S. Eliot in East Coker (poem)|East Coker, in an eternal recurrence through cameos and flashbacks.
* A courtship scene in which the future bride steals the clothes of a young man while he is swimming in the river, and is then chased by him naked across the fields, is borrowed from H.E. Bates Uncle Silas story The Revelation (My Uncle Silas, 1939).
* A scene in which the grandfather as a young man is reaping, and weeps when he accidentally crushes a birds egg, is derived from a Thomas Bewick miniature in his History of British Birds. This is a homage to the oral historian George Ewart Evans of Blaxhall, a village near to Charsfield, who used the Bewick image on the title page of his first Blaxhall study Ask the Fellows Who Cut The Hay (Faber and Faber, London 1956).

==References==
 

==External links==
*  
*  
*  
*  
 

 
 
 
 
 
 
 