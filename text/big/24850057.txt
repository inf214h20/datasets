Dog's Dialogue
{{Infobox film
| name           = Dogs Dialogue
| image          = 
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       = Hubert Niogret
| writer         = Raúl Ruiz Nicole Muchnik
| starring       = Eva Simonet
| music          = 
| cinematography = Denis Lenoir
| editing        = Valeria Sarmiento
| distributor    = 
| released       =  
| runtime        = 22 minutes
| country        = France
| language       = French
| budget         = 
}}

Dogs Dialogue ( ) is a 1977 French short crime film directed by Raúl Ruiz (director)|Raúl Ruiz.     The story, told entirely in still images, revolves around a young girl who is told her mother is not her real mother.  The girl leaves her small town, grows into a beautiful woman, and starts searching for love and fulfillment in undesirable places.  The story is narrated off screen, and the stills are intercut with video footage of a city landscape and dogs barking.

==Cast==
* Eva Simonet as Henri
* Robert Darmel (voice)
* Silke Humel as Monique
* Frank Lesne
* Marie Christine Poisot
* Hugo Santiago
* Genevieve Such
* Laurence Such
* Michel Such
* Pierre Olivier Such
* Yves Wecker

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 