Spy Kids 2: The Island of Lost Dreams
{{Infobox film
| name           = Spy Kids 2: The Island of Lost Dreams
| image          = SpyKids2.jpg
| caption        = Theatrical release poster
| director       = Robert Rodriguez
| producer       = {{Plainlist|
* Elizabeth Avellan
* Robert Rodriguez }}
| writer         = Robert Rodriguez
| starring       = {{Plainlist|
* Antonio Banderas
* Carla Gugino
* Alexa Vega
* Daryl Sabara
* Mike Judge
* Ricardo Montalbán
* Holland Taylor
* Christopher McDonald
* Danny Trejo
* Cheech Marin
* Steve Buscemi
* Taylor Momsen
* Matt OLeary
* Emily Osment }}
| music          = {{Plainlist|
* John Debney
* Robert Rodriguez }}
| cinematography = Robert Rodriguez
| editing        = Robert Rodriguez
| studio         = {{Plainlist|
* Dimension Films
* Troublemaker Studios }}
| distributor    = Buena Vista Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $38 million 
| gross          = $119.7 million 
}}
 family adventure adventure film written and directed by Robert Rodriguez. It is the second installment in the Spy Kids (franchise)|Spy Kids film series, which began with 2001s Spy Kids.

Upon release, Spy Kids 2 received positive reviews from critics  and became a commercial success by grossing over $119 million worldwide. 

==Plot==
 
The OSS now has a full child spy section, of which 13-year-old Carmen Cortez and 10-year-old Juni Cortez have become agents. They face particularly difficult competition with Gary and Gerti Giggles (Matt OLeary and Emily Osment), the children of double-dealing agent Donnagon Giggles (Mike Judge), whom Carmen and Juni helped to rescue in the previous film. It is shown that Carmen defends Gary and has a crush on him.
 President was reading from, is named the director of the OSS. Juni is fired after being framed by Gary, who was actually to blame, into losing the "Transmooker", a highly coveted device which can shut off all electronic devices. In his new position as director, Donnagon can carry on with his plan to steal the Transmooker, so he can rule the world.

After Carmen manages to hack into the database and reinstates Junis level as an agent, she and Juni use some hints from Alexander Minion (Tony Shalhoub) and follow the trail to a mysterious island near Madagascar, which is home to Romero (Steve Buscemi), a lunatic scientist. Romero has been attempting to create genetic engineering|genetically-miniaturized animals, so he can make a profit by selling the animals to kids in "miniature zoos". He had an experiment go wrong after accidentally pouring growth concoction onto the mutated set of animals, as a result, he is unwilling to leave the safety of his lab, out of fear of being eaten. When Carmen is captured by a Spork, a breed of flying pigs, she meets Gerti, who reveals to her that Gary is actually evil. Carmen changes her feelings for Gary after he tries to kill Juni, and she sides with her brother. Meanwhile, Romero finds out that his creatures are much friendlier than he thought. After a number of action sequences, such as fighting pirate skeletons (that came alive after Juni stole a necklace from them) and being captured by Sporks, the spy kids, along with the help of their family (who came to the island when they heard their kids were missing), Romero and his creatures, and Gerti Giggles, destroy the Transmooker and defeat Donnagon and Gary. Afterwards, Gregorio and Donnagon pathetically fight each other.  

Donnagon is fired by the President and his daughter, Gary is temporarily disavowed, and Gregorio is appointed director of the OSS by Alexandra on her fathers behalf, while Juni resigns due to the impersonal treatment of agents by the OSS. As the Cortez family leaves the island, Romero gives Juni a miniature spider-monkey as a gift, and all the islands inhabitants bid farewell to the Cortez family. During the credits, Machete has Carmen sing as an undercover pop star in a concert. Carmen says she cant sing so Machete gives her a mic which auto-tunes her voice and a belt that helps her dance. He also gives Juni a guitar that plays itself. After the performance, Machete informs Carmen that he did not put the batteries in. Also Dinky Winks (Bill Paxton) paddles to Romeros island to strike up a business deal. A Slizzard, a lizard-like creature with a snake-like neck, puts one of its scales on his hand.

==Cast==
* Alexa Vega as Carmen Cortez
* Daryl Sabara as Juni Cortez 
* Antonio Banderas as Gregorio Cortez
* Carla Gugino as Ingrid Cortez
* Steve Buscemi as Romero
* Mike Judge as Donnagon Giggles
* Cheech Marin as Felix Gumm
* Ricardo Montalbán as Valentin Avellan
* Holland Taylor as Helga Avellan Isador "Machete" Cortez
* Matt OLeary as Gary Giggles
* Emily Osment as Gerti Giggles
* Christopher McDonald as the President of the United States
* Alan Cumming as Fegan Floop
* Tony Shalhoub as Alexander Minion
* Taylor Momsen as Alexandra
* Bill Paxton as Dinky Winks

==Production==
===Filming sites===
* Arenal Lake, Costa Rica
* Austin, Texas|Austin, Texas, USA
* Big Bend National Park, Texas, USA Manuel Antonio, Costa Rica
* San Antonio, Texas, USA
* Six Flags Over Texas; Arlington, Texas|Arlington, Texas, USA

===Special effects===
Despite the fact that this film uses over twice the amount of special effects shots than the first film, Robert Rodriguez did not ask the producers for a larger budget; he said  "...I told the studio I dont want more money. I just want to be more creative".    Rodriguez picked some visual effects companies who were eager and less established, as well as starting up his own Troublemaker Studios, and reemploying Hybride, who had worked with him on the first film.  Gregor Punchatz, the films lead animator, employed a certain technique to make the movements of the computer generated creatures resemble the stop-motion work of filmmaker Ray Harryhausen,  who has a cameo in the film. {{cite web |url=http://www.sfgate.com/cgi-bin/article.cgi?f=/chronicle/archive/2004/05/16/RVG3F6HJ801.DTL
 |title=The man who made the monsters move |accessdate=2008-09-13 |last=Soloman |first=Charles |coauthors= |date=2004-05-16 |work= |publisher=SFGate: Home of the San Francisco Chronicle }}  The scene with the army of live skeletons was shot on a real rock formation, with the two young actors on safety wires,  and the computer generated skeletons added later to over three dozen shots.   

==Reception==
Upon release, Spy Kids 2: The Island of Lost Dreams received mostly positive reviews from critics. It currently scores a 74% "Certified Fresh" approval rating on the review aggregate website Rotten Tomatoes with an average rating of 6.6/10. It also has a 66/100 rating on Metacritic. The sites critical consensus reads: "Though the concept is no longer fresh, Spy Kids 2 is still an agreeable and energetic romp."   

  series for kids.  Kenneth Turan of the New York Times  gave it 4 out of 5 stars said, "The movie is a gaudy, noisy thrill ride -- hyperactive, slightly out of control and full of kinetic, mischievous charm."  Lisa Schwarzbaum of Entertainment Weekly wrote, "The antics are a tad more frantic, and the gizmos work overtime, as if ... Robert Rodriguez felt the hot breath of el diablo on his neck. On the other hand, the inventiveness is still superior and the network of   and family is extended."  Michael Wilmington of Metromix Chicago, noting how Rodriguez borrows many elements from television and earlier films, stated that, "Rodriguez recycles and refurbishes all these old movie bits with the opportunistic energy of a man looting his old attic toy chest -- but he also puts some personal feeling into the movie. This is a film about families staying together, children asserting themselves and even, to some degree, Latino power". 

The film grossed  $85,846,429 domestically and  $33,876,929 overseas for a worldwide total of  $119,723,358.   

==Home video release==
The film was released on VHS and DVD in the United States on February 18, 2003. The film is also available to download on iTunes. A Blu-ray Disc|Blu-ray re-release was scheduled for August 2, 2011 to coincide with the fourth film.

==Soundtrack==
{{Infobox album  
| Name       = Music from the Dimension Motion Picture Spy Kids 2: The Island of Lost Dreams
| Type       = Soundtrack
| Artist     = Robert Rodriguez and John Debney
| Released   = August 6, 2002 pop
| Length     = 44:04
| Label      = Milan Records
| Chronology = Robert Rodriguez film soundtrack Spy Kids (2001)
| This album = Spy Kids 2: The Island of Lost Dreams (2002)
| Next album =   (2003)
}}

{{Album reviews rev1 = Allmusic rev1score =     rev2 = Filmtracks rev2score =    
}}
 pop and indie rock, and includes songs performed by Alan Cumming and Alexa Vega. Unusually, the orchestral score for the film was recorded in the auditorium of a local high school in Austin, Georgetown High School. 

===Track listing===
All tracks composed by John Debney and Robert Rodriguez, and performed by the Texas Philharmonic Orchestra.

#"The Juggler"
#"Spy Ballet"
#"Magna Men"
#"Treehouse"
#"R.A.L.P.H."
#"Who, What, When, Where, and Why?" (performed by Alan Cumming)
#"Escape from DragonSpy"
#"SpyParents"
#"Island of Lost Dreams"
#"Donnagons Big Office"/"The Giggles"
#"Mysterious Volcano Island"
#"Romeros Zoo Too"
#"Mothership"/"SpyGrandparents"
#"Magna Racers"
#"Aztec Treasure Room"
#"Skeletons"
#"Creature Battle"
#"Romeros Creatures"/"SpyBeach"
#"SpyDad vs. SpyDad"/"Romeros Gift"
#"Isle of Dreams" (performed by Alexa Vega)
 soundtrack album Bachianas Brasileiras No. 5" for soprano and eight cellos by Heitor Villa-Lobos.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 