The Young Kieslowski
{{Infobox film
| name = The Young Kieslowski
| image = 
| alt = 
| caption = 
| director = Kerem Sanga
| producer =  
* Danny Leiner
* Seth Caplan
* David Hunter
* Ross Putman
* Ben Ross (executive producer)
* Chris Colbert (executive producer
| writer = Kerem Sanga
| starring =  
* Ryan Malgarini
* Haley Lu Richardson
* Joshua Malina
* Melora Walters
* James Le Gros
 
| narrator = 
| music = John Swihart
| cinematography = Rick Diaz
| editing = Ryan Brown
| distributor = Mance Media
| released =  
* July 24, 2015
* (theatrical release)
* June 14, 2014
* (Los Angeles Film Festival)
 
| runtime = 96 minutes
| country = United States
| language = English
| budget = 
}}

The Young Kieslowski is a 2014 romantic comedy, written and directed by Kerem Sanga, and loosely inspired by the story of his own parents. 

The film premiered at the 2014 Los Angeles Film Festival, where it won the Audience Award for Best Narrative Feature. 

It has been picked up for distribution by  , with a planned theatrical and digital release on July 24, 2015. 

==Plot==
The Young Kieslowski is a relationship comedy about Brian Kieslowski and Leslie Mallard, both undergraduates at Caltech, and both socially awkward virgins.  At a party, they meet and hook-up.  Afterwards, Leslie discovers that shes pregnant with twins, and decides she wants to keep them.  Brian, in his first relationship and out of his depths, does what he believes is the right thing.  He pretends to support her in her decision, while secretly hoping that shell change her mind.  The two of them then embark on a California road trip to break the big news to both of their uniquely dysfunctional families.

==Cast==
* Ryan Malgarini — Brian Kieslowski
* Haley Lu Richardson — Leslie Mallard
* Joshua Malina — Robert Kieslowski
* Melora Walters — Barbara Kieslowski
* James Le Gros — Walter Mallard
* Osric Chau — Hanyeoul Cho
* Jessica Lu — Hi Jing
* John Redlinger — Charles
* Sam Aotaki — Judo Girl
* Billy Scafuri — James
* Patrick Rutnam — Ravi

==References==
 

==External links==
*  
*  
*  

 
 
 
 