It Happened to Jane
 
{{Infobox film
| name           = It Happened to Jane
| image          = It Happened to Jane poster.jpg
| image_size     =
| caption        = Movie poster
| director       = Richard Quine
| producer       = Martin Melcher Richard Quine
| writer         = Norman Katkov Max Wilk
| starring       = Doris Day Jack Lemmon Ernie Kovacs
| music          = George Duning
| cinematography = Charles Lawton Jr. Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States English
| gross          = $1.7 million (est. US/ Canada rentals) 
}}

It Happened to Jane is a 1959 romantic comedy film starring Doris Day,  Jack Lemmon, and Ernie Kovacs directed by Richard Quine and written by Norman Katkov and Max Wilk.

The film was co-produced by Quine and Days husband at the time, Martin Melcher.

The film was re-released in 1961, with the title Twinkle and Shine. 

==Plot==
In May 1959, in the town of Cape Anne, Maine, a foul-up by the Eastern & Portland Railroad (E&P) results in the death of 300 lobsters shipped by Jane Osgood (Day), an attractive, widowed businesswoman with two children. She gets her lawyer and friend, George Denham (Lemmon), to go after the E&P to pay damages after her customer, the Marshalltown Country Club, refuses all future orders.

In the E&P office in New York City, railroad executive Harry Foster Malone (Kovacs) learns about the Osgood lawsuit. Due to the budget cuts Malone had instated, there had been no station agent at Marshalltown to receive Janes lobsters. Malone sends employees Crawford Sloan (Walter Greaza) and Wilbur Peterson (Philip Coolidge) to Cape Anne to deal with the situation. The two attorneys offer Jane $700 in compensation, but Jane turns it down because the loss to her business reputation is more than that.

Jane wins in court, but E&P appeals the case to the state Supreme Court in Augusta, Maine. George files a writ of execution to force payment and take possession of the train, Old 97, in lieu of payment.
 Steve Forrest) siding on which the train is sitting.  In a charming scene, Jane and George are shown singing an original song, "Be Prepared", to a pack of local cub scouts at a forested picnic.   

Jane travels to New York to appear on American Broadcasting Company|ABC, NBC, and CBS, including the show Ive Got a Secret. Fearful of bad publicity, Malone finally gives in and cancels the rent, but gives Jane the train. Meanwhile, George becomes increasingly jealous when he learns that Larry in New York is attracted to Jane and has proposed marriage to her.  Jane receives telegrams of support from the American public, and the Marshalltown club, which had earlier reneged, now promises to continue business with her.  

Back in Cape Anne, during a packed town meeting, Jane learns that Malone has ordered all his trains to bypass the town and has also given Jane 48 hours to remove Old 97 from the track. With service ended, local merchants will find it difficult to get their merchandise. Jane runs away and George, in an impassioned speech, scolds the townspeople for turning against her.

Realizing that Old 97 is just the way to deliver the lobsters, Jane and George persuade everybody to fill up the trains tender with coal from their homes. George recruits his uncle Otis, a retired E&P engineer, to engineer the train.

Old 97 sets off with Jane, her children, and George (who shovels coal to the engine), to deliver lobsters on board to customers in several distant towns. Malone does everything possible to delay them, even as several of his office staff resign, seeing him as a villain. Jane becomes upset at the roundabout route Malone is forcing them to take. Eventually, the coal runs out, stopping Old 97 and blocking traffic.

Just then, Malone arrives by helicopter, after hearing that the train is stalled.  Jane scolds him for his underhanded actions. Malone finally agrees to Janes demands. Jane and George tell him to come along so he cannot cause any more trouble. He finally shows his good side by helping shovel coal. Larry and a photographer are in Marshall Town when the train arrives. George kisses Jane in front of Larry, and she agrees to marry George and remain in Cape Anne.
 first selectman, a badly needed fire engine pulls into town, a present from Malone.

==Cast==
 
* Doris Day as Jane Osgood
* Jack Lemmon as George Denham
* Ernie Kovacs as Harry Foster Malone Steve Forrest as Larry Hall
* Teddy Rooney as Billy Osgood
* Gina Gillespie as Betty Osgood Russ Brown as Uncle Otis Denham
* Walter Greaza as Crawford Sloan
* Parker Fennelly as Homer Bean
* Mary Wickes as Matilda Runyon
* Dick Crockett as Clarence Runyon
* Philip Coolidge as Wilbur Peterson
 
* Max Showalter as Selwyn Harris
* John Cecil Holm as Aaron Caldwell
* Napoleon Whiting as Eugene The Waiter
* Gene Rayburn as himself, a reporter from WTIC-TV
* Dave Garroway as The Left Hand Host
* Robert Paige as The Big Payoff Host
* Garry Moore as Ive Got A Secret Host
* Bill Cullen as Panelist
* Jayne Meadows as Panelist Henry Morgan as Panelist
* Betsy Palmer as Panelist
*  (Michael Frazier) as the baby 
 

==Production==
Old 97 is based on the J-Class 2-8-2 steam locomotives that used to run on the New Haven Railroad. Old 97 was a common nickname for steam locomotives that had the number 97 on them (see, for example, the popular song “The Wreck of the Old 97”).

The movie was mostly filmed in Chester, Connecticut. The final scene filmed at downtown Chester Connecticut was about 1 mile from the train station used in the movie. The locomotive in the final scene was a wooden prop built near where the final scene was filmed. People from all over Connecticut were invited to be extras in the movie.

==Songs==
*"It Happened To Jane", performed by Doris Day
*"Be Prepared", performed by Day
*"Ive Been Workin On The Railroad"

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 