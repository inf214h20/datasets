Hombre (film)
{{Infobox film
| name           = Hombre
| image          = Hombre (film).jpg
| caption        = Theatrical release poster
| director       = Martin Ritt
| producer       = Irving Ravetch Martin Ritt
| writer         = Irving Ravetch  
| starring       = Paul Newman Fredric March Richard Boone David Rose
| cinematography = James Wong Howe
| editing        = Frank Bracht
| distributor    = 20th Century Fox
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $5,860,000 
| gross          = $12,000,000 
}} revisionist western novel of the same name by Elmore Leonard and starring  Paul Newman, Fredric March, Richard Boone, Martin Balsam, and Diane Cilento.

Newmans amount of dialogue in the film is minimal and much of the role is conveyed through mannerism and action. This was the sixth and final time Ritt directed Newman; they had previously worked together on The Long Hot Summer, Paris Blues, Hemingways Adventures of a Young Man, Hud (film)|Hud and The Outrage.

==Plot==
In late 19th-century Arizona, an Apache-raised white man, John Russell, faces prejudice in the white world after he returns for his inheritance (a gold watch and a boarding house) upon his fathers death.  Deciding to sell the house in order to buy a herd of horses—which does not endear him to the boarders who live there or to the caretaker, Jessie—Russell ends up riding a stagecoach with Jessie and unhappily married boarders Doris and Billy Lee Blake leaving town.

Three others ride with them: Indian agent Professor Alexander Favor, his aristocratic wife Audra and the crude Cicero Grimes.  Upon discovering that John Russell is an Indian, Professor Favor requests that Russell ride up top with driver Henry Mendez.

The stagecoach is robbed by a gang led by Grimes, who knew that Dr. Favor had been carrying money that he stole from the very Apaches whom Russell grew up with.  Grimes rides off, taking Mrs. Favor as a hostage.

Russell manages to shoot two of the outlaws—one of whom is Jessies lover, sheriff-gone-bad Frank—who have the stolen money in their saddle bags. He insists that Dr. Favor give the recovered money back to him. The bigots he rode with now appeal to Russell to lead them to safety.

Russells instincts to protect the group clash with their naive and "civilized" attitudes to save the Favors, especially when Grimes and his remaining gang offer to trade Mrs. Favor for the money. Their pity for Mrs. Favors life eventually outweighs the knowledge that Grimes is using her to bait a trap. Russell gives the money to Billy Lee, asking him to take it back to the Indians from whom it was stolen.  Russell descends from the groups hideout with saddle bags that he pretends are full of the money, while Billy Lee stays in the hideout and aims a rifle at one of the outlaws. Russell cuts Mrs Favor loose and she slowly makes her way up to the group, but by the time Russell throws the saddle bags to Grimes Mrs Favor has collapsed at a point where she is obscuring Billy Lees target. In the ensuing firefight, although Russell is able to kill Grimes, Billy Lee is unable to prevent an outlaw shooting Russell dead.

==Cast==
* Paul Newman as John Russell
* Fredric March as Dr. Alex Favor
* Richard Boone as Cicero Grimes
* Diane Cilento as Jessie Cameron Mitchell as Frank Braden
* Barbara Rush as Audra Favor
* Peter Lazer as Billy Lee Blake
* Margaret Blye as Doris Blake
* Martin Balsam as Henry Mendez
* Skip Ward as Steve Early
* Frank Silvera as Mexican bandit
* David Canary as Lamar Dean
* Val Avery as Delgado
* Larry Ward as Soldier

==Background== Native Americans in a different and more truthful way than what had previously been seen in westerns. The film shows the need for Indian and non-Indian to cooperate with each other for mutual benefit. The subplot focuses on the hypocrisy and duality of respectable citizens. 

==Production==
The movie was filmed on location in the Coronado National Forest in Arizona, at the Helvetia Mine in Pima County, Arizona, at Old Tucson, Arizona, and at the Bell Ranch in Santa Susana, California    Stage station scenes filmed  at Jean Dry Lake Las Vegas Nevada

==Reception==
The film earned $6.5 million in rentals in North America, making it one of the biggest hits of the year. 

===Critical reaction=== The Professionals was the best-directed film out of Hollywood, and this year it looks as if the honors may rest with Martin Ritt and Hombre." Ebert gave the film a rating of three and a half out of four possible stars in his review.   

Hombre has a 100% rating on the film review aggregator Rotten Tomatoes. 

==References==
 

==External links==
*  
*   film review at the New York Times by Bosley Crowther

 
 

 
 
 
 
 
 
 
 
 
 