Tales from the Golden Age
 
{{Infobox film
| name           = Tales from the Golden Age
| image          = Tales_from_the_golden_age_poster.jpg
| caption        = US film poster
| director       = Hanno Höfer Cristian Mungiu Constantin Popescu Ioana Uricaru Razvan Marculescu
| producer       = Cristian Mungiu Oleg Mutu
| writer         = Cristian Mungiu
| starring       = Tania Popa
| music          = Hanno Höfer Jimi Laco (Nightlosers)
| cinematography = Liviu Marghidan Oleg Mutu Alex Sterian
| editing        = Dana Bunescu Ioana Uricaru
| distributor    = Wild Bunch
| released       =  
| runtime        = 138 minutes
| country        = Romania
| language       = Romanian
| budget         =
}}
 omnibus film. It was screened as part of the Un Certain Regard section at the 2009 Cannes Film Festival.    

The film is composed of five whimsical yet blackly comic short stories, each one set in the late communist period in Romania and based on urban myths from the time, reflecting the perspective of ordinary people. The title of the film refers to the alleged "Golden Age" (by communist propaganda) of the last 15 years of Nicolae Ceaușescus regime.  Each episode concludes with "legend tells us ... ", each item being legend because, of course, no such thing could have happened during the "Golden Age".

==Episodes==
The Legend of the Official Visit (Romanian: Legenda Activistului în Inspectie)
* Alexandru Potocean - The Secretary
* Teodor Corban - The Mayor
* Emanuel Parvu - The Party Inspector
Local Party officials scurry into action when a motorcade of VIPs and foreign dignitaries promises to pass through the village. A government official arrives and attention is paid to the smallest detail, yet the people are let down when a phone call reveals the motorcade is no longer coming. Everyone is disconsolate and the worse for drink, the official orders everyone to ride together on the childrens carousel, but as the mayor loses conscience, they realize that there is no one around to switch the machine off and let them down. Legend tells that they were all still trapped there when the motorcade did after all pass through.

The Legend of the Party Photographer (Romanian: Legenda Fotografului Oficial)
* Avram Birau - The Photographer
* Paul Dunca - The Photographers Assistant
* Viorel Comanici - The Party Secretary Giscard dEstaing doctor the photograph to make Ceaușescu appear taller than dEstaing, and whether he should be wearing a hat. Pressure is mounting as the deadline to get the paper out to the workers on time approaches.  A hat is duly provided for Ceaușescu in the photograph, but no one notices until too late that he was already carrying one, leaving the image of him comically carrying one hat while wearing another.  Police are sent out to retrieve all the copies that had been sent out.  One copy on a train is left uncollected, its owner laughs at the picture.  Legend tells us that this was the only time that the paper, Scînteia, did not reach the workers.

The Legend of the Zealous Activist (Romanian: Legenda Politrucului Zelos)
* Calin Chirila - The Party Activist
* Romeo Tudor - The Shepherd
The audience at a large county level Communist Party meeting is told by an outside official that their county, home to four notables that he names, has an unacceptably low literacy rate in its villages and towns.  He urges the young activists to spare no effort to remedy this disgrace.  One such activist is next seen being driven out to a remote village until the washed out road forces him to complete the journey on foot over difficult territory.  The first person he meets is a shepherd.  He wastes no time establishing himself as a know-it-all, informing the shepherd that his cooking method is dangerous.  He heads into town to meet the mayor, who offers various excuses, such as lack of electricity and the demands of farm work, for the poor attendance at the local school and other problems.  The activist will accept no excuse insisting that all illiterate locals old and young must come in for class.  Only a few children attend the first session when the activist insists that the mayor round up the adults.  Some come, others, including the shepherd, send food items instead.  A boy who has learned to read starts reading labels and warning signs to the shepherd, who ignores the information doing what he pleases as always.  The activist, fed up with the shepherds resistance goes out in the rain to get the shepherd to class.  The activist stops to work on his shoe leaning against a power line tower.  The shepherd, fed up with the activists meddling, uses the pretext that the activist is being electrocuted by the tower to strike him with his crook.  Next we see the activist, arm in sling, sitting on a cart loaded with food stuffs and chickens, while the mayor reads an appreciative farewell praising the activist and feigning sorrow at his early departure.  On the way out the activist offers his hand to the shepherd who does not accept the gesture but hands him a bag of cheese to say goodbye and good riddance.  Legend tells us that the next year the village reported a literacy rate of 99%.

The Legend of the Greedy Policeman (Romanian: Legenda Milițianului Lacom)
* Ion Sapdaru - Policeman Alexa
* Virginia Mirea - Policemans Wife
* Gabriel Spahiu - Neighbor
A policeman is promised a pig by his brother, but when it is delivered, is found to still be alive. Uncertain how to slaughter a pig, and unwilling to share the pig with their neighbours, the family manage to gas it with butane in their apartments kitchen. Believing the gas to have completely dissipated, they try to burn the hairs from its corpse with a blow lamp, but instead succeed in blowing up their apartment.  Legend tells us that the family used what remained of the animal for the seasonal celebrations.

The Legend of the Air Sellers (Romanian: Legenda Vânzătorilor de Aer)
* Diana Cavallioti - Crina
* Radu Iacoban - Bughi
Crina meets small-time  . She joins him on one of his trips, but persuades him that collecting multiple air samples will be faster. They discuss how many bottles in would take to buy a car, if not a Dacia then the inferior Lastun.  Their scheme unravels when they become overambitious and try to scam an entire apartment block out of its bottles.  Legend tells us that with the end of Communism Romanians bought Dacia cars with empty bottles.

The Legend of the Chicken Driver (Romanian: Legenda Şoferului de Găini)
* Vlad Ivanov - Grigore
* Tania Popa - Camelia
* Liliana Mocanu - Marusia Easter coming up, it would be more profitable to sell them on to the general public instead. Their scheme collapses when he is found out and jailed for embezzlement. He is eventually permitted a visitor to his prison, though it proves to be not the woman he sought, but his angry wife.  Legend tells us that many Romanians were forced to steal to survive during the 80s.

==See also==
* Romanian New Wave
* Cinema of Romania

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 