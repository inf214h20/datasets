Terminal Velocity (film)
{{Infobox film
| name           = Terminal Velocity
| image          = TerminalVelocity.jpg
| caption        = Film poster
| director       = Deran Sarafian
| writer         = David Twohy
| starring       = Charlie Sheen Nastassja Kinski James Gandolfini Christopher McDonald
| music          = Joel McNeely Oliver Wood
| editing        = Frank J. Urioste
| producer       = David Twohy Ted Field Robert W. Cort
| studio         = Hollywood Pictures Interscope Communications PolyGram Filmed Entertainment Nomura Babcock & Brown Buena Vista Pictures
| released       =  
| runtime        = 102 min.
| country        = United States
| language       = English
| budget         = $50 million
| gross          = $16,478,900 (USA) 
}} 1994 action skydiver and  Nastassja Kinski as a tough KGB spy who team up  to stop renegade spys who are trying to steal gold. It was written by David Twohy and directed by Deran Sarafian. The film co-stars  James Gandolfini and Christopher McDonald.
 US $500,000. The musical score was composed by Joel McNeely.

==Synopsis==
A Boeing 747 lands in the middle of a desert. A young Russian woman is tortured by getting dunked in the aquarium of her apartment until she drowns and is left by her two assailants in the shower stall. Skydiving instructor Richard Ditch Brodie (Charlie Sheen) takes on a new client, Chris Morrow (Nastassja Kinski), who on her first jump does not open her parachute and apparently dies.

Brodie discovers that Morrow faked her death and that she is really a Russian spy trying to recover a shipment of gold. Brodie uses all of his skydiving skills to outwit the villains and to stay alive.

== Cast ==
* Charlie Sheen – Richard "Ditch" Brodie
* Nastassja Kinski – Chris Morrow/Krista Moldova
* James Gandolfini – Ben Pinkwater
* Christopher McDonald – Kerr
* Melvin Van Peebles – Noble
* Cathryn de Prume – Karen
* Hans Howes - Sam

== Production == bluescreen and reverse zoom made it seem as if it were in free-fall.

Portions of the film were shot in Palm Springs, California. {{Cite book
 | last = Niemann
 | first = Greg
 | title = Palm Springs Legends: creation of a desert oasis
 | publisher = Sunbelt Publications
 | year = 2006
 | location = San Diego, CA
 | pages = 286
 | url =  http://books.google.com/books?id=RwXQGTuL1M0C&pg=PA169&lpg=PA169&dq=palm+springs+film+locations&source=bl&ots=N8AgHRYZbg&sig=RMIwy_g3fEV3KoK1aa7P4VxuJWM&hl=en#v=onepage&q=palm%20springs%20film%20locations&f=false
 | doi =
 | id =
 | isbn = 978-0-932653-74-1
 | mr =
 | jfm =
|oclc=61211290}}  ( )   Other filming locations were Alabama Hills (Lone Pine, California); a windfarm near Tehachapi, California; Douglas, Arizona; Flagstaff, Arizona; Little Colorado River Canyon, Arizona; Moscow, Russia; Phoenix, Arizona; San Bernardino, California and Tucson, Arizona. 

== Reception ==
The film was panned by critics, and currently has a 17% positive scale on Rotten Tomatoes based on 23 reviews.  

The movie debuted at No. 2 at the box office behind Timecop.  It eventually made $16.4 million in ticket sales, making it a box office flop compared to its $50 million budget.

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 