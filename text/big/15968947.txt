Broken Angel (film)
{{Infobox film
| name = Broken Angel
| image = Image-Brokenangel_poster.jpg
| caption = Teaser poster
| director = Aclan Bates
| producer = Leslie Bates-Büyüktürkoğlu Kevin Totti Corchiani Unicvisions
| writer = Leslie Bates-Büyüktürkoğlu Tulay Pirlant
| starring = Nehir Erdoğan Nilüfer Açıkalın Patrick Muldoon Devon Odessa Fay Masterson Jay Karnes Ayşe Nil Şamlıoğlu Zachary Charles Yuri Bradac
| music = Kemal Günüç (Turkish version) Edwin Wendler (international version)
| cinematography = Neil Lisk
| editing = Aclan Bates Radu Ion Bayard Stryker Chris Ward
| distributor = Warner Bros.
| released =  
| country = United States Turkey English Turkish Turkish
| budget =
| gross =
}}
 Turkish girl America in the 1980s and fell prey to the image of fame and fortune.

==Plot==

A young Turkish girl comes to America in search of the life she saw in the movies and on TV. Broken Angel shows that life for a foreigner is not always the Hollywood life, especially when you do not speak the language. As she is slowly swallowed by the realities, only one boy, a deaf artist, can reach her and save her life.
 Turks in the U.S. and vice versa. Broken Angel   in Istanbul and Izmir.

==Cast==
{| class="wikitable"
|- bgcolor="#efefef"
Directed by Aclan Bates Buyukturkoglu
! Actor !! Role
|- Nehir Erdoğan|| Ebru
|- Ajla Hodzic|| Asli
|-
|Nilüfer Açıkalın|| Filiz
|- Patrick Muldoon|| Kevin
|- Devon Odessa||  Elizabeth
|- Fay Masterson|| Mimi
|- Jay Karnes|| Michael Levy
|- Clyde Kusatsu|| Dondi
|- Colin Fickes|| Hank
|-
|Ayşe Nil Şamlıoğlu||
|- Maree Cheatham||  Mrs. Kraus
|- Zachary Charles||  Rusty
|- Yuri Bradac|| Grigori
|-
|}

==Production==
The picture was filmed in Los Angeles in the summer of 2007. - 

==Music==
Kemal Günüç (Turkish version) 
Edwin Wendler (international version) 
Victor Kaply - Music Supervisor (international version)

==Bridge to Turkiye Fund==
Broken Angel LLC, the company behind the film project Broken Angel, announced that they have designated Bridge to Turkiye Fund as their favorite charity.   This means that 10% of all investment in the film, as well as a portion of the profits, will be used to benefit the needy in Turkey through the fund. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 