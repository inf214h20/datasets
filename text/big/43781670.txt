A Dog of Flanders (1935 film)
{{Infobox film
| name           = A Dog of Flanders
| image          = 
| alt            =
| caption        =  Jimmy Anderson (assistant)
| producer       = William Sistrom
| screenplay     = Ainsworth Morgan
| writer         = Dorothy Yost (adaptation)
| based on       =  
| starring       = Frankie Thomas O.P. Heggie
| music          = Alberto Colombo (uncredited)
| cinematography = J. Roy Hunt
| editing        = George Crone RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 novel of the same name by Ouida. The film stars Frankie Thomas, appearing in only his second film (the first being Wednesdays Child (film)|Wednesdays Child).

==Cast==
* Frankie Thomas as Nello Daas
* Harry Beresford as Sacristan
* O.P. Heggie as Grandfather Jehan Daas
* DeWitt Jennings as Carl Cogez Lightning as Leopold, "Leo" the dog
* Sarah Padden as Frau Keller
* Helen Parrish as Maria Cogez
* Richard Quine as Pieter Vanderkloot
* Frank Reicher as Herr Vanderkloot
* Addison Richards as Herr Herden
* Christian Rub as Hans
* Ann Shoemaker as Frau Ilse Cogez
* Nella Walker as Frau Vanderkloot
* Reginald Barlow as Official with court order (uncredited)
* Elsa Janssenas The Cogez Maid (uncredited)
* Marcia Mae Jones as Little girl at party (uncredited)
* Henry Kolker as Monsieur LaTour, Art critic (uncredited) Clarence Wilson as Antoine, Official with court order (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 