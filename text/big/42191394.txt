Boys of Abu Ghraib
{{Infobox film
| name           = Boys of Abu Ghraib
| image          = Boys_of_Abu_Ghraib_Poster.jpg
| caption        = Theatrical release poster
| director       = Luke Moran
| producer       = Luke Moran  Cru Ennis
| screenplay     = Luke Moran
| inspired by    = The events of Abu Ghraib torture and prisoner abuse John Heard 
| music          = Dan Marocco
| cinematography = Peter Holland
| editing        = Jeff Fullmer  Luca Disica
| studio         = Rebel One Pictures
| distributor    = Vertical Entertainment
| released       =  
| runtime        = 102 minutes
| country        = United States 
| language       = English
}}
 war film events that Abu Ghraib John Heard. Acclaimed filmmakers Edward Zwick and Marshall Herskovitz served as executive producers on the film, which was produced by Luke Moran and Cru Ennis.

The film was released in US theaters on March 28, 2014. It has a runtime of 102 minutes and is rated R.

== Plot ==
 
The film starts in 2003, the day before 22-year-old Jack Farmer (Luke Moran) ships out for Iraq, having joined the Army Reserves in hopes of being part of something bigger than himself.

He and dozens of other young men, whose motives range from patriotism to the promise of adventure and sheer youthful restlessness, arrive at Abu Ghraib, 20 miles from Baghdad and formerly used by Saddam Hussein to imprison, torture and murder dissidents. CO Capt. Hayes (Scott Patterson) greets them with a rousing speech about standing on the front lines of the war for freedom, but it doesnt take long for the most gung-ho among them to realize theyve been shipped to the armpit of the universe. There are no generators (meaning no electricity), the "barracks" are former prison cells, complete with bloodstained floors, boarded up windows and scrawled evidence of prisoners counting down the days to oblivion, and they arent on the frontlines of anything. A situation exacerbated by the fact that theyre completely cut off from anything that might keep them connected to their civilian lives. There are no phones, movies or TV, no online connectivity and no mail. About the only things to do with their abundant free time is lift weights and indulge in macho horseplay.

Jack requests MP duty just for a change of scenery, undaunted by the cautionary tale of an earlier volunteer who had the same idea and wound up shooting himself in the foot to escape. With no training, briefing or idea what to expect, Jack is transferred to Hard Site, a cellblock that supposedly houses hard-core terrorists. His new boss, Sgt. Tanner (Sean Astin), matter of factly tells him that Hard Sites guiding principle is "no compassion": their job is to "soften up" prisoners for military interrogators by making them as miserable as possible without leaving visible evidence of maltreatment. Their weapons are deprivation, humiliation and isolation. "Are you sure were supposed to be doing this?" Jack asks, as he takes in the bare cells and their shivering, demoralized occupants. "Fuckin-a right were supposed to be doing this," Tanner replies, adding that its also kind of fun: witness the detainee hes nicknamed "Chewie" and trained to howl like a wookie.

Jack is not a boat-rocker by nature, but the whole business seems so wrong it is not long before he breaks rule one—no talking to the detainees except to order them around—and develops a friendship with Ghazi Hammoud (Omid Abtahi), accused of having engineered a deadly bombing. His quiet insistence that hes guilty of nothing more than being Western educated and unwilling to implicate other innocent men to buy his own freedom hastens the erosion of Jacks confidence in the institution he serves.

What follows is the decay of a boy into a hardened man capable of anything. And a shocking ending.

== Cast ==
* Luke Moran as Jack Farmer
* Sean Astin as Staff Sergeant Tanner
* Omid Abtahi as Ghazi Hammoud
* Sara Paxton as Peyton John Heard as Sam Farmer Michael Welch as Eugene "Pits" Fowler
* Elijah Kelley as Babatunde "Tunde" Ogundule John Robinson as Ryan Fox
* Scott Patterson as Captain Hayes
* Cru Ennis as Shaw
* Jerry Hernandez as Rodeo

== Production ==
Shooting occurred in Santa Fe, New Mexico at the Penitentiary of New Mexico, location of the New Mexico State Penitentiary riot.

== Reception ==
Some critics criticized the film for not depicting the exact events of the scandal, but fictionalizing a story based on these events, while other appreciated the story and its one-step-at-a-time explanation of how an event like this actually came to pass. Audience reviews were more positive, most particularly from veterans for its lack of a political agenda. The film won the Audience Award at the Gasparilla Film Festival and the War on Screen International Film Festival, the only two film festivals it screened at.
In general this movie was recognized by much of the critics as manufactured by the neoconservative movement as a way to justify torture and to advocate for the members of the military to participated in torture during the Iraq conflict.

==References==
 

==External links==
*  
*  
*  

 
 