Detention of the Dead
{{Infobox film
| name           = Detention of the Dead
| image          =
| alt            =
| caption        =
| director       = Alex Craig Mann Brooke P. Anderson Michael Manasseri
| writer         = Rob Rinow Alex Craig Mann
| based on       = Detention of the Dead a play by Rob Rinow Jayson Blair Max Adler Joseph Porter
| music          = Cody Westheimer
| cinematography = Noah Rosenthal
| editing        =
| studio         = Gala Films
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          =
}}
Detention of the Dead is a 2012 zombie comedy-horror film written and directed by Alex Craig Mann {{cite web
| url=http://www.webcitation.org/608rYGgzb
| title=Serving time in "DETENTION OF THE DEAD"
| publisher=Fangoria
| date=May 31, 2011
| accessdate=July 13, 2011
| author=Allan Dart}}  based upon the Rob Rinow stage play {{cite news
| url=http://www.webcitation.org/608sFi92d
| title=Detention of the Dead Variety
| date=November 8, 2009
| accessdate=July 13, 2011
| author=Terry Morgan}}  of the same name. {{cite news
| url=http://www.dailytribune.com/articles/2011/06/01/news/doc4de5a3e10b065783254065.txt?viewmode=fullstory
| title=Detention of the Dead
| publisher=Oakland Tribunal
| date=June 1, 2011
| accessdate=July 13, 2011
| author=Mayuri Munot}}   Filming began in Spring 2011. It had a small theatrical release in L.A. on June 28 (2013), and went to DVD on July 23.

==Plot== Jayson Blair]]), Max Adler), another football player/jock who also bullies Eddie and is good friends with Brad; Ash (Justin Chon), a happy-go-lucky stoner; and Eddies best friend Willow (Alexa Nikolas), a goth who shares with him a love for zombie movies. During detention, Ash attempts to sell drugs to Mark (Joseph Porter), a quiet student in detention. Ash quickly realizes Mark is unwell and alerts Mrs. Rumblethorp (Michele Messmer). However Mark becomes aggressive, biting Mrs. Rumblethorp and forcing the group to evacuate the detention room.

In the school corridors the group discover a wide-scale zombie outbreak with all the other students now undead. They decide to barricade themselves in the school library; believing no one would ever go there. Mrs. Rumblethorp eventually dies and reanimates, causing the group to restrain her as Ash decapitates her with a paper cutter. Some time later, the group decide to search the library for available information on zombies. While searching the book stacks, the librarian attacks and bites Jimmy, before Brad kills the zombie. Eddie and Willow argue that they must kill Jimmy before he becomes a zombie, but Brad remains confident that his friend can survive his bite, quickly revealed to be denial from previously being bitten by Mrs. Rumblethorp on the finger himself. However, Jimmy accepts his fate and jumps out a window where he is eaten by a group of zombies. 

As the group panic at their situation, Ash convinces the group to smoke weed to calm their nerves. While doing so, the group bond, learning more about each other, challenging their original high-school stereotypes, which they list openly: the nerd (Eddie), the social outcast and goth (Willow), the popular, self-involved cheerleader (Janet), the jock (Brad) and the stoner (Ash). The group are attacked by the zombies and narrowly manage to remain safe within the library. Realizing the barricades will not hold, Brad, Willow and Ash decide to escape through the vents while Eddie and Janet remain in the library. While in the vents, the group discover zombified rats who eat Ash, before the vent breaks, leaving Willow and Brad stranded in the halls of the school. They arm themselves with Ashs severed legs and begin to fight through the hordes of zombies back to the library. Meanwhile, Janet convinces Eddie to take her virginity, but the pair are interrupted by Willow and Brad.

Inside the library, Willow is hurt when she realizes Eddies intentions with Janet. Quickly the barricades of the door begin to weaken, before Brad eventually succumbs to his wounds and attacks Janet, who manages to knock him out of a nearby window. Eddie formulates a plan to escape, using a gun he had obtained in order to commit suicide after not getting into Harvard University. The survivors fight through the school corridors and make it to the school gym. While climbing a ladder up to the roof, a zombie bites Janet on the leg, but all three make it onto the roof. Realizing she is going to die, Janet pleads with Willow to shoot her. However the group are shocked to find the zombies can climb ladders, and all the bullets are used to fight back the oncoming zombies. The zombified Brad attacks the group, and Janet sacrifices herself by jumping off the roof with Brad. Eddie and Willow lose hope of survival, shortly before military units descend. The zombies are killed, and Eddie and Willow leave the school. As the pair share their first kiss, a zombified Janet tackles Eddie and the military open fire on her.

==Cast==
* Jacob Zachar as Eddie
* Alexa Nikolas as Willow
* Christa B. Allen as Janet Jayson Blair as Brad
* Justin Chon as Ash Max Adler as Jimmy
* Joseph Porter as Mark
* Joey Paul Gowdy as Zombified Student
* Michele Messmer as Mrs. Rumblethorp

==References==
 

==External links==
*  
*  

 
 
 
 
 