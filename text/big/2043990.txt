Spy Hard
{{Infobox film
| name           = Spy Hard
| image          = Spyhardposter.jpg
| caption        = Theatrical release poster
| director       = Rick Friedberg
| producer       = Rick Friedberg Doug Draizin Jeffrey Konvitz Dick Chudnow Jason Friedberg Aaron Seltzer
| starring       = Leslie Nielsen Nicollette Sheridan Andy Griffith Charles Durning
| music          = Bill Conti
| cinematography = John R. Leonetti
| editing        = Eric Sears
| studio         = Hollywood Pictures Buena Vista Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $18 million
| gross          = $26.9 million
}} spy comedy film parody starring Leslie Nielsen and Nicollette Sheridan, parodying James Bond films and other action films.  The introduction to the movie is sung by comedy artist "Weird Al" Yankovic. It is the first film to be written by Jason Friedberg and Aaron Seltzer. The film title is pun of Die Hard.

==Plot==
 
Secret agent WD-40 Dick Steele (Leslie Nielsen) has his work cut out for him. Along with the mysterious and lovely Veronique Ukrinsky, Agent Pi|3.14, he must rescue the kidnapped Barbara Dahl and stop the evil genius, a General named Rancor (Andy Griffith), from seizing control of the entire world.

Rancor was wounded in an earlier encounter and no longer has arms. However, he can "arm" himself by attaching robotic limbs with various weapons attached. Steele is talked out of retirement by an old friend, agent Steven Bishop (Robert Guillaume), and given his new assignment by The Director (Charles Durning), who also is testing out a variety of elaborate disguises. At headquarters, Steele encounters an old agency nemesis, Norm Coleman (Barry Bostwick), and flirts with the Directors adoring secretary, referred to as Miss Cheevus (Marcia Gay Harden).
 home alone, who is very good at fending off intruders. Steele resists the temptations of a dangerous woman (Talisa Soto) he finds waiting for him in bed. But he does work very closely with Agent 3.14 (Nicollette Sheridan), whose father, Professor Ukrinsky (Elya Baskin), is also being held captive by Rancor.

Everything comes to an explosive conclusion at the Generals remote fortress, where Steele rescues both Barbara Dahl (Stephanie Romanov) and Miss Cheevus and launches a literally disarmed Rancor into outer space, saving mankind.

==Cast==
* Leslie Nielsen as Dick Steele, WD-40
* Nicollette Sheridan as Veronique Yukrinsky, Agent 3.14
* Charles Durning as The Director
* Marcia Gay Harden as Miss Cheevus
* Barry Bostwick as Norm Coleman
* John Ales as Kabul
* Andy Griffith as General Rancor
* Elya Baskin as Professor Yukrinsky
* Mason Gamble as McLuckey
* Carlos Lauchu as Slice
* Stephanie Romanov as Victoria and Barbara Dahl
* Dr. Joyce Brothers as Steeles Tag Team Member
* Ray Charles as Bus Driver
* Hulk Hogan as Dicks tag-team partner Roger Clinton as Agent Clinton
* Robert Culp as Businessman
* Fabio Lanzoni as Himself
* Robert Guillaume as Steven Bishop
* Pat Morita as Brian the Waiter
* Talisa Soto as Desiree More
* Mr. T as a helicopter pilot
* Alex Trebek as Agency Tape Recorder (voice)
* Taylor Negron as Painter
* Curtis Armstrong as Pastry chef
* Tina Arning as Dancer #1
* William Barillaro as Blind Driver
* Michael Berryman as the man with the oxygen mask
* Downtown Julie Brown as Cigarette Girl
* Stephen Burrows as Agent Burrows
* Keith Campbell as Thug #2
* Carl Ciarfalio as Thug #1
* Brad Grunberg as Postal Worker
* Wayne Cotter as Male Dancer Rick Cramer as Heimlich, Rancor Terrorist
* Eddie Deezen as Rancor guard that gets spit on
* Joey Dente as Goombah, Dead Wise Guy
* Paul Eliopoulos as Agent #1
* Andrew Christian English as Paratrooper
* Johnny G as Agent #2
* Michael Lee Gogin (Brad Garrett, voice) as Short Rancor guard
* Bruce Gray as The President John Kassir as Rancor guard at intercom
* Sally Stevens as Vocal conductor/singer
* Thuy Trang as a Hawaiian-dressed waitress
* "Weird Al" Yankovic as Himself in title sequence
* Larry Walsh as Musician
* Rawle D. Lewis  as Boatman #1

==Title sequence==
 , although, for legal reasons, all credits and titles had to be taken out, excluding that of the film and of Yankovic himself.

==Reception==

===Critical reaction===
Reviews of Spy Hard were very negative. The film received a "rotten" rating of 8% on Rotten Tomatoes based on 36 reviews and a 3.4/10 rating. 

James Berardinelli of ReelViews wrote: "Director Rick Friedberg   has crafted a dreadfully unfunny comedy that takes Naked Gun-like sketches and rehashes them without a whit of style or energy. ... For movie-after-movie, Leslie Nielsen has milked this same personality, and its starting to wear very thin. As affable as the actor is, theres just nothing left in this caricature. However, while Spy Hard might have worked better with, say, Roger Moore in the title role (his 007 was a parody towards the end, anyway), Nielsens performance is only a small part of a massively-flawed production. Hard is the operative word here, because, even at just eighty-one minutes, this movie is unbelievably difficult to sit through." 

Stephen Holden of New York Times wrote: "Spy Hard is never funnier than during its opening credit sequence in which "Weird Al" Yankovic bellows his parody of the brassy theme song from Goldfinger, while obese cartoon silhouettes swim across the screen. ... Instead of building sustained comic set pieces, it takes a machine-gun approach to humor. Without looking at where its aiming, it opens fire and sprays comic bullets in all directions, trusting that a few will hit the bulls-eye. A few do, but many more dont. ... Around the halfway point, Spy Hard begins to run out of ideas and becomes a series of crude, rambunctious parodies of other films. ... When Spy Hard abruptly ends after only 81 minutes, you sense that it has used up every last round of available ammunition. It was simply exhausted and couldnt move another inch." 

Mick LaSalle of San Francisco Chronicle wrote: "Its done in the style of the Zucker-Abrahams-Zucker Naked Gun series, but although the style is there, the jokes arent. Spy Hard relies on silly slapstick, takeoffs of recent films and the shock effect of celebrity cameos. But all that exertion doesnt add up to more than a handful of laughs. ... The story is too weak to work even as a clothesline for gags. Spy Hard eschews a coherent story and instead just strings together movie takeoffs. ... Nielsen, with his expert deadpan and sense of comic timing, creates the illusion of humor -- for about 15 minutes. Thanks to him, what could have been an unbearable experience becomes merely empty. Still, he cant work miracles, and nothing short of a miracle could have made Spy Hard worth seeing." 

===Box office===
The film opened at #3 with $10,448,420 behind   s opening weekend and Twister (1996 film)|Twister s third. It eventually grossed $26,960,191 at the box office. 

==See also==
* Spy Hard (song)|"Spy Hard" (song)

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 