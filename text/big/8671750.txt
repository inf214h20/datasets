Wild Rebels
{{Infobox Film
| name = Wild Rebels
| image = Wildrebels.jpg
| writer = William Grefe John Vella Willie Pastrano Jeff Gillen
| director = William Grefe
| movie_music = Al Jacobs
| released = 1967
| runtime = 90 min.
| language = English
}}

Wild Rebels is a 1967 film directed by William Grefe. It was featured as an episode of the Comedy Central movie-mocking television series Mystery Science Theater 3000, and was released on the Mystery Science Theater 3000 video releases#DVD|Collection, Volume 9 box set. The tag line for the movie was "They live for kicks... love for kicks... kill for kicks".

The film starred Steve Alaimo as Rod Tillman, a stock car driver that goes undercover as the wheel man for a motorcycle gang.

== Plot ==
 stock car bikers ( Pastrano, Vella, and Gillen) that call themselves "Satans Angels".  They realize Rod can drive the getaway car for their robberies and offer him a place in their gang.  Despite making out with the lady in the group, Linda (Byers), Rod opts out. On his way back to the town of Citrusville, he gets recruited by Lieutenant Dorn (Walter Philbin) to go undercover amongst the bikers, who have thus far been able to outsmart the police due to their mobility on motorcycles.  After the police fix a race to make it appear that Rod is in desperate need of money, the bikers agree to let him join the gang.  He proves himself as a driver during a hold up at a gun shop where the owner is shot.  Back at the hideout, Linda reveals that she doesnt commit these crimes for financial gain, but for the thrill of the action.  During their next crime, a bank robbery, Rod signals to a passing officer while the gang is inside.  A shooting spree ensues and the gang realizes Rod is a mole (espionage)|mole, forcing him to drive away at gunpoint.  Several policemen are killed as they chase the gang to an old lighthouse.  Banjo and Fats are killed and Rod tries to make a break by running up the spiral staircase of the lighthouse.  Jeeter follows after him and corners him with a shotgun.  A shot is heard, and its revealed that Linda, having a change of heart about her life of crime, has saved the life of the wounded hero.  Linda goes off to a life in prison and Rod walks into the sunset with Lieutenant Dorn.

== External links ==
*  

 
 
 
 
 



 