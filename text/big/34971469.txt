Call Me Kuchu
{{Infobox film
| name           = Call Me Kuchu
| image          = Call Me Kuchu poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Malika Zouhali-Worrall Katherine Fairfax Wright
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = $3,476 
}}
Call Me Kuchu is a 2012 American documentary film directed by  , March 3, 2012.  focusing in part on the 2011 murder of LGBT activist David Kato. 

The film jointly received the 2014 GLAAD Media Award for Outstanding Documentary alongside Bridegroom (film)|Bridegroom.

==Title== Swahili origin, is a way to refer to homosexuals in Uganda; Sylvia Tamale documented the usage of the word by homosexual Ugandans as a catch-all self-description. 

==Summary==
In Kampala, two men are having a ninth year anniversary party together with friends. However, it is a very quiet event and everyone is dressing casually to avoid attracting attention. Meanwhile, we see footage of pastors and politicians describing homosexuality as a Western and sinful activity.

Outside his home, David Kato (1964-2011) recounts how he found out about "gay life" when he was living in South Africa ten years earlier. He picked up a gay escort and had sex for the first time, at the age of twenty-eight. He then decided to return to his home country of Uganda and spread gay rights there. At the headquarters of Sexual Minorities Uganda, the LGBT non-profit organization he runs, he explains he is the first openly gay man in Uganda. He adds his job is to track all instances of homophobia in Uganda. We then see a man from Mbale explaing he was arrested and humiliated by police officers... We are then introduced to Naome Ruzindana, a lesbian activist with two children. In 2004, she founded the Coalition of African Lesbians.
 Long Jones, who tells us about another article in Rolling Stone, suggesting they have a list of 100 homosexuals are spreading AIDS in Uganda. At a trial over the articles, Gilles Muhame does not present a defense and the ruling is adjourned; Pastor Solomon Male is present.
 Galatians 3:28, and wants to build a safe center for them.

Meanwhile, we learn that politician David Bahati has proposed the Uganda Anti-Homosexuality Bill, widely condemned by the international community. However, residents of Jinja, Uganda protest in favour of the bill, organized by Pastor Martin Ssempa, falsely accusing homosexuals of "raping children." We then hear from Dr Sylvia Tamale who explains that in March 2009, the Family Life Network invited evangelical leaders from the United States to warn Ugandans about an alleged homosexual threat from overseas. Similarly, another pastor called HM Nyanzi says homosexuality is against the word of God because it is against reproduction. Meanwhile, Rolling Stone published part two of pictures and Gilles boasts about it as an accomplishment. David appeals to the local bureau of the Office of the United Nations High Commissioner for Human Rights. Bishop Senyonjo claims that he is dismissed from the Church of Uganda for his support of homosexuals, though the church claims that it was because of his participation in the consecration of a man to be a bishop of a church with which the Church of Uganda is not in communion.  In another twist, there is a second trial, and the judge rules that the newspaper is not allowed to publish pictures, names and addresses of homosexuals. David and his friends have a party–a fashion show where they crossdress to celebrate their victory.

David is killed. Anti-gay protesters disrupt the funeral. Still, his friends have a party in his honour after the funeral. In New York City, LGBT activists honour his death too, condemning pastors Lou Engle and Scott Lively for allegedly promoting homophobia in Uganda. Meanwhile, Gilles of Rolling Stone is unrepentant and does not take the blame for his death. Davids friends fear the worst for their lives. Still, due to international pressure, the anti-gay law is not passed.

==Critical reception==
The film premiered at the  , February 17, 2012. 

==See also==
* God Loves Uganda

==Distribution==
Distribution rights for the film were acquired by Cinedigm Entertainment Group in October 2012, with plans for a theatrical release in early 2013, followed by on-demand, premium digital, DVD and V releases. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 