Nishkarsha
 
{{Infobox film name           = Nishkarsha image          = Nishkarsha_Movie_Poster.jpg image_size     =  caption        =  director       = Sunil Kumar Desai producer       = Doddagowda C Patil,  G M Jayadevappa writer         = L R Ranganath Rao narrator       =  starring  Vishnuvardhan Anant Nag B. C. Patil|B.C. Patil music          = Gunasingh cinematography = P Rajan editing        = Janardhan R distributor    =  released       = 1993 runtime        = 146 min country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}
Nishkarsha ( ) is a 1993 Kannada action/heist film by Sunil Kumar Desai starring Vishnuvardhan (actor)|Vishnuvardhan, Anant Nag, B. C. Patil. The story revolves around a bank robbery where terrorists become trapped with the hostages. It also explores the many travails of the police commissioner and ATS commando to free the hostages and reign in the terrorists. Nishkarsha was hailed as a one-of-a-kind movie in Kannada cinema with its realistic plot elements and single location used for most parts.

==Plot==

The story opens with a kidnapping of an architect (Avinash) at night. He is whisked away to a secluded location where he is tortured by unknown assailants for information regarding the structural details of a bank which he helped build. When the torture reaches excruciating levels, the architect gives in and shares the confidential details of the security alarms and safety devices in the bank. It is shown that the leader of the group called Dev (B. C. Patil) is a terrorist and will stop at nothing to get the banks money to further his groups operations. It is also shown that he commands a strong team of at least 20 deadly terrorists.

The following morning, the scene opens at Bangalores Manipal center, the place of the bank that Dev and his associates have planned to rob. It is a normal day, the staff slowly trickle in, and business resumes. Meanwhile, a van that enters into the building, unloads a bunch of carton boxes marked to be delivered to the bank. The boxes are then transferred slowly to the bank premises. Unknown to the security guards, the boxes contain automatic weapons and deadly explosives, which the terrorists plan to use to take control of the bank.
 ATS immediately.

ATS Commando Ajay (Vishnuvardhan) is apprised of the situation. He quickly assembles a team and meets with the police commissioner. Commando Ajay takes charge of the operation and learns that the terrorist Dev is the brother of another terrorist who he himself had killed a few years ago. Dev, too, learns that Ajay has been enlisted to supervise this operation and demands that he be taken off the force immediately. When the he doesnt oblige, Dev kills the bank manager by throwing him out of the building. The commissioner is left with no choice but to listen to the terrorist.

Soon enough, the police commissioner hatches a plot to bring back Ajay. He succeeds, and Ajay once again takes charge. Meanwhile, the hostages inside grow restless. Fearing that they will all die, they hatch their own plan and attempt a coup. It goes horribly wrong and a few of the hostages get killed. Gundanna (Ramesh Bhat) a lift mechanic, with whom Ajay is in constant touch, acts as a spy and aids Ajay in performing reconnaissance of the terrorists and their activities.

In the end, Ajay infiltrates the terrorists stronghold with help of his trusted commando (Prakash Raj) and Gundanna. Although the commando valiantly gives his life and Gundanna is seriously injured, Ajay frees the hostages, kills Dev, and saves the day.

==Cast==
* Vishnuvardhan as ATS Commando Ajay
* Ananth Nag as Police Commissioner
* B. C. Patil as Dev
* Suman Nagarkar as Bank Employee
* Ramesh Bhat as Gundanna
* Prakash Raj as ATS Commando
* Avinash as Bank Architect

==External links==
*  
*  

 
 
 
 