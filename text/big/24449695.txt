Dr. Christian Meets the Women
{{Infobox film
| name           = Dr. Christian Meets the Women
| image_size     =
| image	=	Dr. Christian Meets the Women FilmPoster.jpeg
| caption        =
| director       = William C. McGann
| producer       = Monroe Shaff (associate producer) William Stephens (producer)
| writer         = Marion Orth
| narrator       =
| starring       = See below
| music          = Monroe Friedman
| cinematography = John Alton
| editing        =
| distributor    =
| released       = July 5, 1940 
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Dr. Christian Meets the Women is a 1940 American film directed by William C. McGann, one of the series of six Dr. Christian films featuring Jean Hersholt.

==Plot Summary==

Professor Kenneth Parker, a God-fearing physical culturist, arrives to work in the serene little town of River’s End. He claims to be a specialist and top authority on health matters. The town physician, Dr. Paul Christian, reacts to Parker’s promises to the women in town of dramatic weight loss, if they followed his advice. The head of the town women’s club, Mrs. Browning, is charmed by the questionable professor. Parker and invites him to her home and to have a lecture when the club is meeting. He is welcome to use the club as his forum for his teachings.

The professor starts teaching the women about strict diet being the best road to self-satisfaction. Dr. Christian, on the other hand, begins to warn the women about the dangers with wholesale diets, claiming that all diets should be tailored to fit the individual and advising the women not to listen to the professor.

The professor’s teachings result in the disruption of the town womens eating routines. They also disrupt the peace and quiet in the Browning family life, causing Mrs. Browning and her husband to argue about the professor’s teachings and intrusions on the town life. The Browning’s daughter, Kitty, has taken an interest in the professor’s assistant, Bill Ferris, and started an extreme diet to seem more pleasing to him. Kitty soon collapses from starvation. Dr. Christian claims the professor is a fraud and a charlatan. The town doesn’t listen to his warnings.

Kitty’s condition gets worse and Dr. Christian, exhausted from an abnormal workload because of the professor’s teachings, manages to visit her. While examining her he discovers that the professor has given the girl, and the other women, benzedrine. Dr. Christian finally discloses the professor and his cultist teachings as a public hazard.

The heresies of the professor come to Bill’s knowledge, and he finally realizes that his boss is a fraud. He quits and runs to Kitty’s bedside. With the help of a blood transfusion he manages to save Kitty’s life. The town women are taken off their unhealthy diets and life in town returns to normal, after the professor is driven off, disclosed as a quack. Later, Bill and Kitty are married. 

== Cast ==
*Jean Hersholt as Dr. Paul Christian
*Dorothy Lovett as Judy Price
*Edgar Kennedy as George Browning
*Rod La Rocque as Prof. Kenneth Parker
*Frank Albertson as Bill Ferris
*Lynn Merrick as Kitty Browning
*Maude Eburne as Mrs. Hastings
*Veda Ann Borg as Carol Compton
*Lelah Tyler as Martha Browning William Gould as Dr. Webster
*Phyllis Kennedy as Annie, the maid
*Bertha Priestley as Alice Mason
*Diedra Vale as Gladys Mason
*Heinie Conklin as Ed the plumber

== External links ==
* 
* 

==References==
 

 

 
 
 
 
 
 
 