The Mystery Man (film)
{{Infobox film
| name           = The Mystery Man
| image          = 
| image_size     = 
| caption        = 
| director       = Ray McCarey
| producer       = Trem Carr (supervising producer) Paul Malvern (associate producer) George Yohalem (supervising producer)
| writer         = Tate Finn (story) William A. Johnston (adaptation) John W. Krafft (screenplay) and Rollo Lloyd (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Harry Neumann
| editing        = Carl Pierson
| studio         = 
| distributor    = 
| released       = 1935
| runtime        = 65 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Mystery Man is a 1935 American film directed by Ray McCarey.

== Plot summary ==
A newspaper man, Larry Doyle and a young woman, Anne Olgivie, meet by chance in a coffeeshop. She hasnt got the money to pay, and he pays for her without being seen. In the telegraphic office, where she wants to send a wire to her mother asking to send her some money as she is broke, she is not permitted to send it, but he followed her and read the thrown away message. On the street waiting the light for the pedestrians to switch, it seems as if she throws herself under a car, but he make it look as if she embraces him wildly.
From that point on they know each other how broke they are and he asks her to confide in him.
They take a suite in a Hotel, pretending to be on a honeymoon and trie to rise some money. Some pals send him 50 dollars, that calm down the Hotel Manager at first.
Meantime Doyle tries also to contact the newspaper in St. Louis as he thinks to make a big story about the Eel, a gangster, who puzzles the Police. But there are difficulties in identifying Doyle because his chief  in Chicago, mad at him, said hes not Doyle. 
As he ran out of money again, Doyle pawns a police revolver he was given after he helped the police solve a case. He gets 25 Dollars.
Short after Doyle and Anne are involved in the shooting between the Eel and others,and Doyle sits in the car of the Eal being ordered to drive away the money and to meet the next at five thirty.
Meantime the police has identified the gun used in the shooting,and although Doyle had explained to the Police and the District Attorney how the shooting had taken place he is suspected of having committed the crime.
Finallly Larry and Anne go to the Pawnshop, where they pawned the revolver, and after a fight with the proprietor, the Eel comes in. Larry pretends to be the shopkeeper, but the Eel knows him and pointing a gun he threatens Larry. Fortunaltely Anne had a gun and is able to shoot just before the Eal kills Doyle.
The case is solved, but all the newspapers write of Mrs. Doyle, and that worries Larry. But the final scene is not worried at all, as a intense kiss answers to the problem.

== Cast == Robert Armstrong as Larry Doyle
*Maxine Doyle as Anne Ogilvie
*Henry Kolker as Ellwyn A. "Jo-Jo" Jonas
*LeRoy Mason as The Eel, Gangster James Burke as Managing Editor Marvin
*Guy Usher as District Attorney Johnson
*James P. Burtis as Whalen
*Monte Collins as Dunn, Reporter
*Sam Lufkin as Weeks
*Otto Fries as Nate, Pawnbroker
*Norman Houston as T. Fulton Whistler
*Dell Henderson as Mr. Clark, Hotel Manager
*Lee Shumway as Plainclothes Man
*Sam Flint as Jerome Roberts, Publisher

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 