Captain Thunderbolt (film)
 
 
{{Infobox film
| name           = Captain Thunderbolt
| image          = 
| image_size     = 
| caption        =  Cecil Holmes John Wiltshire
| writer         = Creswick Jenkinson
| narrator       =  Grant Taylor   Charles Tingwell
| music          = Sydney John Kay
| cinematography = Ross Wood
| editing        = Margaret Cardin
| studio         = Associated TV
| distributor    = Ray Films
| released       = 1953 (overseas) June 1955 (Australia)
| runtime        = 69 min. (53 min. TV version)
| country        = Australia
| language       = English
| budget         = ₤15,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 216. 
| gross          = ₤30,000 
}} Cecil Holmes about the bushranger Captain Thunderbolt. It was one of the few all-Australian films of the 1950s. 

==Synopsis==
Fred Ward is imprisoned for horse stealing. He escapes from Cockatoo Island and under the name of Captain Thunderbolt becomes a bushranger, working with his friend and fellow escapee Alan Blake.

Thunderbolt is tracked by the evil Sergeant Mannix who undertakes gunfights with the bushranger at a dance, then at a rocky outcrop, only to discover that he has killed Alan Blake instead. Mannix passes off Blakes body as Thunderbolt, enabling the bushranger to escape. The legend grows that Thunderbolt did not die.

==Cast== Grant Taylor as Captain Thunderbolt
*Charles Tingwell as Alan Blake
*Rosemary Miller as Joan
*Harp McGuire as Sgt Mannix John Fegan as Dalton
*Jean Blue as Mrs Ward
*John Fernside as Colonel
*Loretta Boutmy as Maggie
*Ronald Whelan as Hogstone
*Charles Tasman as Colonial Secretary
*Harvey Adams as parliamentarian
*Patricia Hill as Belle
*John Brunskill as Judge

==Production== Sir Benjamin Fuller.  
 David Drummond.

British censorship requirements meant that the real-life romantic relationship between Thunderbolt and his aboriginal girlfriend Mary, who helped him escape from Cockatoo Island, was not featured in the film when released in Britain. 

Captain Thunderbolt was allowed to live at the end of the film because the producers hoped to spin it off into a TV series.  This did not happen.

==Release==
The film did not receive a wide release in Australia - it did not play in Melbourne cinemas until late 1955, and Sydney until 1956. However it sold well overseas, including to American television. 

The only copy of the film in possession of the Australian National Film and Sound Archive is a 53 minute TV edition. The archive is looking for a copy of the full 69 minute version. 

==References==
 

==External links==
*  in the Internet Movie Database
* 
*  at National Film and Sound Archive
*  at Oz Movies
*  Specific website for the search for this film.

 
 
 
 