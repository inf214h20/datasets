Little Miss Broadway
{{Infobox film
| name           = Little Miss Broadway
| image          = LittleMissBroadway1.jpg
| caption        = Film poster
| director       = Irving Cummings
| producer       = Darryl F. Zanuck
| writer         = Harry Tugend Jack Yellen
| starring       = Shirley Temple Edna May Oliver George Murphy Phyllis Brooks
| music          = Harold Spina
| cinematography = Arthur C. Miller
| editing        =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}}
 1938 American musical drama film directed by Irving Cummings.  The screenplay was written by Harry Tugend and Jack Yellen.  The film stars Shirley Temple in a story about a theatrical boarding house and its occupants, and was originally titled Little Lady of Broadway.  In 2009, the film was available on DVD and videocassette.

==Plot==
Betsy Brown is released from an orphanage into the care of Pop Shea, her parents friend who runs a boarding house for theatrical performers.  Sarah Wendling, the curmudgeon owner and next-door neighbor of the building, detests "show people" and their noise, and demands Pop pay the $2,500 back rent he owes or move out immediately. Her nephew Roger is in love with Pops daughter Barbara and files suit against Sarah in order to gain control of the building and his inheritance, with which he plans to stage a show starring the hotel residents. Sarah questions the soundness of Rogers investment in the show, and Betsy convinces the judge to see the production before he decides the case.  With the assistance of her friends, the little girl presents a lavish musical revue in the courtroom that so impresses one of the observers he offers the troupe $2,500 a week to star in his International Follies. Having had a change of heart, Sarah insists the show is worth $5,000 and convinces the impresario to double his offer. Roger and Barbara then announce their intent to wed and adopt Betsy.

==Cast==
* Shirley Temple as Betsy Brown, an orphan Edward Ellis as Pop Shea, Betsys parents friend
* Edna May Oliver as Sarah Wendling
* Donald Meek as Willoughby Wendling, her brother
* George Murphy as Roger, Sarahs nephew, Betsys adoptive father
* Phyllis Brooks as Barbara, Pops daughter, Betsys adoptive mother
* Jimmy Durante as Jimmy Clayton, a bandleader
* Jane Darwell as Miss Hutchins
* Patricia Wilder as Flossie George Barbier as Fiske
* Barbra Bell Cross as Carol, an orphan
* El Brendel as Ole, an animal trainer
* George Brasno as George
* Olive Brasno as Olive

==Production==

Murphy, who was not satisfied with the dance routine in "We Should Be Together," insisted that movies closing dance number be reworked. Despite Temples mothers concerns, Temple was on board with it. The dance number proved so popular with the cast and crew that Murphy and Temple gave an encore performance after the cameras stopped rolling. 

==Music==
Six songs were written by Harold Spina (music) and Walter Bullock (lyrics). All were performed by Temple.
*"Little Miss Broadway"
*"Be Optimistic"
*"How Can I Thank You?"
*"We Should Be Together"
*"If All the World Were Paper"
*"Swing Me an Old Fashioned Song"

Other songs appearing in the movie include: 
* "When You Were Sweet Sixteen"

==Release==

===Critical reception===
The New York Times wrote, "The devastating Mistress Temple is slightly less devastating than usual   it cant be old age, but it does look like weariness   although she performs with her customary gaiety and dimpled charm, there is no mistaking the effort every dimple cost her."  

TV Guide called it "a delightful Shirley Temple vehicle in which she again does what she does best &ndash; portray a singing, dancing, pouting orphan girl."  

===Home media===
In 2009, the film was available on videocassette and DVD.  Some editions had special features and theatrical trailers.

==See also==
* List of films in the public domain
* Shirley Temple filmography

==References==
 

 

 
 
 
 
 
 
 
 