Mad Max: Fury Road
 
 
{{Infobox film
| name           = Mad Max: Fury Road
| image          = Max_Mad_Fury_Road_Newest_Poster.jpg
| caption        = Theatrical release poster
| alt            = Theatrical release poster George Miller
| producer       = {{Plain list | Doug Mitchell
* George Miller
* P. J. Voeten
}}
| writer         = {{Plain list |
* George Miller
* Brendan McCarthy
* Nico Lathouris
}}
| starring       = {{Plain list |
* Tom Hardy
* Charlize Theron
* Nicholas Hoult
* Hugh Keays-Byrne
* Rosie Huntington-Whiteley
* Riley Keough
* Zoë Kravitz Abbey Lee
* Courtney Eaton
}}
| music          = Junkie XL
| cinematography = John Seale
| editing        = Margaret Sixel
| studio         = {{Plain list |
* Kennedy Miller Mitchell
* Village Roadshow Pictures
}} Warner Bros. Pictures
| released       =  
| runtime        = 120 minutes     
| country        = {{Plain list |
* Australia
* United States
}}
| language       = English
| budget         = $150 million 
| gross          = 
}} George Miller, and the fourth film of Millers Mad Max (franchise)|Mad Max franchise. The first film of the franchise in 30 years, Fury Road stars Tom Hardy as Max Rockatansky|"Mad" Max Rockatansky, making it also the first Mad Max film not to feature Mel Gibson in the title role. It also stars Charlize Theron.
 68th Cannes Film Festival in an out of competition screening, followed by a wide theatrical release on 15 May 2015.

==Premise==
Several years after a series of catastrophic worldwide calamities, Max (Tom Hardy), a former highway patrolman, meets Furiosa (Charlize Theron), a woman attempting to cross an immense desert.  With her are five former female captives of the tyrannical Immortan Joe (Hugh Keays-Byrne) and his bloodthirsty gang. Their only hope of reaching safety is Max and his expansive knowledge of the deserts many dangers. When Max is captured by Joe, his only chance of escape depends on Furiosa and her colleagues, each of whom is considered a precious object vital to the continued survival of the human race. 

==Cast==
 
* Tom Hardy as Max Rockatansky|"Mad" Max Rockatansky
* Charlize Theron as Imperator Furiosa
* Nicholas Hoult as Nux
* Hugh Keays-Byrne as Immortan Joe
* Rosie Huntington-Whiteley as The Splendid Angharad
* Riley Keough as Capable
* Zoë Kravitz as Toast the Knowing Abbey Lee as The Dag
* Courtney Eaton as Cheedo the Fragile Nathan Jones as Rictus Erectus
* Josh Helman as Slit
* Megan Gale as Valkyrie John Howard as The People Eater Richard Carter as the Bullet Farmer iOTA as Coma-Doof Warrior
* Angus Sampson as the Organic Mechanic
* Jennifer Hagan as Miss Giddy
* Melissa Jaffer
* Gillian Jones
* Joy Smithers
 

==Production==
===Development=== George Miller announced in 2003 that a script had been written for a fourth film, and that pre-production was in the early stages.  Although the project was given the green light for a US$100 million budget to begin filming in Australia in May 2003, Mad Max 4 entered hiatus due to security concerns related to trying to film in Namibia because the United States and many other countries had tightened travel and shipping restrictions.    With the outbreak of the Iraq War, Mad Max 4 was abandoned as it was considered a potentially politically sensitive film. Although Mel Gibson had been cast to return as Max, he lost interest after production was cancelled. 

  announced in 2003 that a script had been written for a fourth film, and that pre-production was in the early stages.]] 3D animated feature film was in pre-production and would be taking much of the plot from Fury Road,    although Mel Gibson would not be in the film and Miller was looking for a "different route", a "renaissance" of the franchise.  Miller cited the film Akira (film)|Akira as an inspiration for what he wanted to do with the franchise. George Miller was also developing an action-adventure tie-in video game based on the fourth film, along with God of War II video game designer Cory Barlog. Both projects were expected to take two to two-and-a-half years, according to Miller, with a release date of either 2011 or 2012. The Fury Road film was going to be produced at Dr.D Studios, a digital art studios founded in 2008 by George Miller and Doug Mitchell. 

On 18 May 2009, it was reported that location scouting was underway for Mad Max 4.    After exploring the possibility of an animated 3D film, George Miller decided instead to shoot a 3D live action film, and at this point plans to make the animated film were immediately dropped, and by May 2009, location scouting for the Mad Max sequel had begun.  An Australian press said in May 2009: "MAD   Max is revving up for a long-awaited return to the big screen, almost 25 years after Tina Turner ran Mel Gibson out of Bartertown. Director George Miller is gearing up to shoot the fourth film in the ground-breaking Aussie road warrior franchise, industry sources say. Scouting for locations is under way for the movie, which many thought would never get off the ground. It could go into production later this year." 

In October 2009, Miller announced that that filming of Fury Road would commence at Broken Hill, New South Wales in early 2011, ending years of speculation.  This announcement attracted widespread media attention in Australia, with speculation at that time on whether Mel Gibson would return as Max.  That same month, British actor Tom Hardy was in negotiations to take the lead role of Max, while it was also announced that Charlize Theron would also play a major role in the film.  In June 2010, Hardy announced on Friday Night with Jonathan Ross that he would play the title role in a new version of Mad Max.  In July 2010, Miller announced plans to shoot two Mad Max films back-to-back, entitled Mad Max: Fury Road and Mad Max: Furiosa.  In November 2011, filming was moved from Broken Hill to Namibia, after unexpected heavy rains turned the desert there into a lush landscape of wildflowers, inappropriate for the look of the movie.  

In a July 2014 interview at San Diego Comic-Con International|Comic-Con, Miller said he designed the film in storyboard form before writing the screenplay, working with five storyboard artists. It came out as about 3,500 panels, almost the same number of shots as in the finished film. He wanted the film to be almost a continuous chase, with relatively little dialogue, and to have the visuals come first.   Paraphrasing Alfred Hitchcock|Hitchcock, Miller said that he wanted the film to be understood in Japan without the use of subtitles. 

===Filming===
Principal photography began in July 2012 in Namibia.  In October 2012, The Hollywood Reporter reported that Warner Bros. sent an executive to keep the production on track.  The filming wrapped on 17 December 2012.  In February 2013, a leaked draft from the Namibian Coast Conservation and Management group accused the producers of damaging parts of the Namib Desert, endangering a number of plant and animal species.   However, the Namibia Film Commission said it had "no reservations" after visiting the set during production. It disputed claims reported in the media, calling the accusations "unjust rhetoric". 

Cinematographer John Seale, who came out of retirement to shoot Fury Road,  outfitted his camera crew with six Arri Alexas, as well as a number of Canon EOS 5Ds that were used as crash cams for the action sequences. 

In September 2013, it was announced that the film would undergo reshoots in November 2013.  In July 2014, director George Miller described the film as "a very simple allegory, almost a western on wheels".    Miller claimed that 90% of the effects were practical effect|practical. 

===Music===
 
The  .  Prior to Junkie XLs involvement,  ,  Miller met with the composer in Sydney. "I got very inspired and started writing pieces of music for scenes," said Junkie XL. "The initial main themes were written in the four weeks after that first meeting and those themes never changed."  A soundtrack album is scheduled to be released by WaterTower Music on 12 May 2015. 

==Release== 68th Cannes Film Festival in an out of competition screening,  and will be released in theaters on 15 May 2015. 
 Vertigo will release three comic book prequels, one per month, that detail the backstory for a character in the film. A hardcover collection of art inspired by the film will be released 6 May 2015. 

==Sequels== back to back, but Warner Bros. has since made no confirmation that there will be any more films in the franchise.  In March 2015, during an interview with Esquire (magazine)|Esquire magazine, Hardy revealed that he was attached to star in three more Mad Max films following Mad Max: Fury Road. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 