Tripoli (film)
{{Infobox film
| name           = Tripoli
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       = Will Price 
| producer       = William H. Pine William C. Thomas
| screenplay     = Winston Miller
| story          = Will Price Winston Miller John Payne Maureen OHara Howard Da Silva Phillip Reed Grant Withers Lowell Gilmore Connie Gilchrist
| music          = Lucien Cailliet
| cinematography = James Wong Howe
| editing        = Howard A. Smith
| studio         = Pine-Thomas Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Payne, Maureen OHara, Howard Da Silva, Phillip Reed, Grant Withers, Lowell Gilmore and Connie Gilchrist. The film was released on November 9, 1950, by Paramount Pictures.  

==Plot==
 

== Cast == John Payne as Lt. OBannion
*Maureen OHara as Countess DArneau
*Howard Da Silva as Capt. Demetrios
*Phillip Reed as Hamet Karamanly
*Grant Withers as Sgt. Derek
*Lowell Gilmore as Lt. Tripp
*Connie Gilchrist as Henriette
*Alan Napier as Khalil
*Herbert Heyes as Gen. Eaton
*Alberto Morin as Il Taiib
*Emil Hanna as Interpreter
*Grandon Rhodes as Cmmdre. Barron Frank Fenton as Capt. Adams
*Rosa Turich as Seewauk
*Ray Hyke as Crawford Walter Reed as Wade
*Paul Livermore as Evans
*Gregg Barton as Huggins
*Don Summers as Langley
*Jack Pennick as Busch
*Ewing Mitchell as Elroy

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 