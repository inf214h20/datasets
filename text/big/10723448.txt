The Dawns Here Are Quiet
{{infobox film
| name        = The Dawns Here Are Quiet  (А зори здесь тихие)
| image       = A zori zdes tikhie poster.jpg
| caption     =
| director    = Stanislav Rostotsky Boris Vasilyev
| producer    = Andrei Martynov   Lyudmila Zajtseva
| music       =
| studio      = Gorky Film Studio
| distributor =
| released    =  
| runtime     = 188 minutes
| country     = Soviet Union
| language    = Russian
}}
 Boris Vasilyevs Oscar in Best Foreign Language Film category.

==Plot== Karelia (near Finland) in 1942 during World War II and was filmed near Ruskeala. Senior Sergeant Vaskov is stationed with a group of young female anti-aircraft gunners in a railway station far from the front line. Vaskov is not used to these gunners active, playful personalities and therefore clashes with them over daily issues. But Vaskov, being the only man in the village, has to accommodate them in many cases.
 Nagant and deactivated hand-grenade, attacks the cabin where the Germans are resting. The Germans are totally surprised and are either killed or captured. Meanwhile, reinforcements find Vaskov before he passes out.

Twenty years after the war ends, Vaskov visits the place again with the adopted son of one of the women.

==Cast==
* Yelena Drapeko as Lisa Britschkina
* Yekaterina Markova as Galya Chetvertak (as Ye. Markova)
* Olga Ostroumova as Zhenya Komelkowa
* Irina Shevchuk as Rita Osyanina
* Irina Dolganova as Sonia Gurvich (as I. Dolganova) Andrei Martynov as Senior Sergeant Vaskov
* Lyudmila Zajtseva as Sergeant Kiryanowa
* Alla Meshcheryakova as Maria Nikiforovna

==Awards==
The film was nominated for an Academy Award for Best Foreign Language Film in 1972.   

==DVD release==
The 2004 Ruscico release includes a documentary, "Womens War". Interviewed are actresses Irina Shevchuk, Yelena Drapeko, and Yekaterina Markova.

This film had been re-made in Tamil as Peranmai, starring Jayam Ravi

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Soviet submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 