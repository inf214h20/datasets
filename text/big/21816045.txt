Man with the Gun
{{Infobox film
| name           = Man with the Gun
| image          = ManWithTheGun1955Poster.jpg
| caption        = Film poster Richard Wilson
| producer       = Samuel Goldwyn, Jr.
| writer         = N. B. Stone, Jr. Richard Wilson
| screenplay     = 
| story          = 
| based on       =  
| starring       = Robert Mitchum Jan Sterling Henry Hull Barbara Lawrence Leo Gordon Claude Akins
| music          = Alex North
| cinematography = Lee Garmes
| editing        = Gene Milford
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.8 million (US)  
}}

Man with the Gun is a 1955 Western film starring Robert Mitchum. The film was released in the United Kingdom as The Trouble Shooter and is also sometimes entitled Deadly Peacemaker. The supporting cast includes Jan Sterling, Henry Hull, Barbara Lawrence, Leo Gordon, and Claude Akins. The black-and-white film, which opens with Leo Gordons character shooting a little boys dog in front of the child, was photographed in standard Academy ratio, written by N. B. Stone Jr and Richard Wilson, and directed by Richard Wilson.

==Plot==
Clint Tollinger arrives in town, and is recognized as the "man in grey", a gun for hire who had a reputation for cleaning up other towns. After 14 killings in a year are followed by a night shooting and the burning of a house under construction, and the town marshall tells the town council hes not sure what can be done, the council hires Tollinger as their new "town tamer". The marshall deputizes Tollinger, then tells him he is on his own. Tollinger begins by warning two known gunmen to leave town. He then begins to disarm everyone in the town. Then in an unseen confrontation he kills the two gunmen hed previously warned to leave town.

A group of gunmen shoots up a poster warning of the gun ordnance at the edge of town. As they ride in, they yell that they are looking for Tollinger. Forewarned by the shots, he gets the drop on them from the loft of the town stable with a shotgun. He orders them to drop their guns and they do so. One trades small talk with him while reaching for a derringer in his hat, but Tollinger shoots him.

At a social event, one of the town women advises Tollinger that his job wont be finished until the dance hall girls also leave town. She criticizes them for dancing and carrying on, unaware that Tollinger is an old friend to the madam who manages them. But not all is well in their relationship. When Tollinger warns Nelly Bain that her girls also have a curfew, she replies that she knows what happens in a "Tollinger-tamed town". She tells him she and the girls will be moving on further west, and asks him for suggestions. She asks for the name of any town where he wont be. But theres a hidden history between the two of them, and everything isnt what it seems.

Two gunmen from a large cattle spread ride into town to tell Tollinger that theyve detained Jeff Castle for trying to build a house on disputed land. They appear to have set a trap, inviting Tollinger to the ranch to pick him up, where they will have the advantage. Tollinger arrests the two for carrying guns in town. Some of the towns citizens suddenly have second thoughts, worrying that others from the ranch will come in and shoot up the town. But the arrest proves a good tactical move; the other ranch hands deliver Castle into town, trading him for the two prisoners.

Nelly Bain tells Tollinger something shes kept from him since he arrived in town, that their daughter had died, and Tollinger takes it badly. His patience and calm demeanor suddenly gone, he resolves to clean up the town the quick and dirty way; he burns the saloon, which is owned by Dade Holman, the cattleman who had sent the gunmen.

Holman sets up a trap for Tollinger, and rides into town in a surrey to see that it works. Tollinger kills his assailant but is shot by Holman.  Castle shoots Holman, and at the end Nelly and Tollinger (who apparently survives) kiss and are reconciled.

==Cast==
* Robert Mitchum as Clint Tollinger
* Jan Sterling as Nelly Bain
* Karen Sharpe as Stella Atkins
* Henry Hull as Marshal Lee Sims
* Emile Meyer as Saul Atkins
* John Lupton as Jeff Castle
* Barbara Lawrence as Ann Wakefield
* Ted de Corsia as Frenchy Lescaux
* Leo Gordon as Ed Pinchot
* James Westerfield as Mr. Zender (drummer)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 