The Curse of the Mummy's Tomb
 
 
 
 
{{Infobox film
| name = The Curse of the Mummys Tomb
| image = Cursemummystomb.jpg
| caption = Film poster
| director = Michael Carreras
| producer = Michael Carreras
| writer = Henry Younger (pen name of Michael Carreras) Ronald Howard Fred Clark Jeanne Roland
| music = Carlo Martelli Franz Reizenstein (uncredited)
| cinematography = Otto Heller, British Society of Cinematographers|B.S.C.
| editing =
| studio = Hammer Film Productions Columbia
| released = 18 October 1964
| runtime = 78 minutes
| country = United Kingdom
| language = English
| budget =
}} Ronald Howard, Fred Clark and introducing Jeanne Roland.  Made by Hammer Film Productions, it was released in the UK on 18 October 1964 and, in the US, by Columbia Pictures on 31 December 1964.

==Plot==
" ) and Sir Giles Dalrymple (Jack Gwillim) as well as French Professor Eugene Dubois (unbilled Bernard Rebel, who died three weeks before the films UK premiere).  Assisting in the expedition is Professor Dubois daughter, and Brays fiancée, Annette (Jeanne Roland), herself an Egyptology expert.  All the artifacts are brought back to London by the projects backer, American showman Alexander King (Fred Clark), who plans to recoup his investment by staging luridly sensational public exhibits of the Egyptian treasures.  Soon after arrival, however, the mummy revives and starts to kill various members of the expedition, while it becomes evident that sinister Adam Beauchamp (Beecham) (Terence Morgan), a wealthy arts patron whom members of the expedition meet on the ship returning to England, harbors a crucial revelation of the mummys past and future.

==Cast==
*Terence Morgan: Adam Beauchamp, a secretive key character Ronald Howard: John Bray, the Egyptologist
*Fred Clark: Alexander King, the American promoter
*Jeanne Roland: Annette Dubois, daughter of Professor Dubois (the Egyptologist who is killed in the opening scene) and John Brays fiancée
*George Pastell: Hashmi Bey, representative of Egypts colonial government and worshipful supplicant of the Mummy
*Jack Gwillim: Sir Giles Dalrymple, another Egyptologist John Paul: Inspector Mackenzie
*Dickie Owen: the Mummy
*Jill Mai Meredith: Jenny, Beauchamps maid
*Michael Ripper: Achmed, Egyptian servant Harold Goodwin: Fred, one of Alexander Kings workmen Jimmy Gardner: Freds mate
*Vernon Smythe: Jessop, Beauchamps butler
*Marianne Stone: Hashmi Beys landlady Ray Austin: Stuntman: Assassin on-board ship

==Production== Bernard Robinson, most of the cast and crew were not Hammer veterans. Female lead Jeanne Roland, in her screen debut, receives "and introducing" credit, but her voice is dubbed with one that emphasises a prominently thick French accent. 
 original "mummy" film.

Because union rules in Britain decreed that one person could not be credited as the writer, producer and director of a film, writer/producer/director Michael Careras adopted the name "Henry Younger" for his screenplay—a deliberate analogy to the name "John Elder", which was Hammer producer Anthony Hinds writing pseudonym.

== Critical reception == Allmovie critic Cavett Binyon called the film a "rather dull mummy muddle".  In the 2000s, moods towards this film and its Hammer stablemates have changed. Some critics have seen the Hammer Mummys franchise as more enjoyable than the "Americanized" pictures.

The Curse of the Mummy currently holds an average two and a half star rating (5.3/10) on IMDb.

==Novelization== John Burke as part of his 1966 book The Hammer Horror Film Omnibus.

==Home video release==
In North America, the film was released on 14 October 2008 along with three other Hammer horror films on the 2-DVD set Icons of Horror Collection: Hammer Films (ASIN: B001B9ZVVC) by Sony Pictures Home Entertainment.

== References ==
 

==External links==
* 
*  at Moviephone
* 
*  at Rotten Tomatoes
*  at Turner Classic Movies

 

 
 
 
 
 
 
 
 
 