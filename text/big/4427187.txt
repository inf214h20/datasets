Vacation in Reno
{{Infobox film
| name           = Vacation in Reno
| image          =
| caption        =
| director       = Leslie Goodwins
| producer       = Leslie Goodwins Charles Kerr Arthur Ross
| narrator       =
| starring       =
| music          = Paul Sawtell
| cinematography = George E. Diskant
| editing        = Les Millbrook
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States English
| budget         =
| gross          =
}} 1946 film directed by Leslie Goodwins starring Jack Haley, Anne Jeffreys, Iris Adrian, Wally Brown, Alan Carney, and Morgan Conway.

==Plot==
Jack Carroll (portrayed by Jack Haley) and his wife (Anne Jeffreys) have an argument about their friends, but when he makes a crack that his mother-in-law is a "fat porpoise," they fight and she leaves him.
 Reno and checks into the Bar Nothing Ranch. Later, the robbers come to Reno and check in. They bury a suitcase of money where Jack plans to find it with his metal detector. Jack finds the money, but is taken by another lady.

This is the beginning of his troubles, where he encounters different men: a sheriff, a sailor, and a gun moll who convinces to the police that she is Mrs. Carroll. Jacks wife arrives and Jack is unable to adequately explain things to her before she gets a divorce.

== Cast ==

* Jack Haley as Jack Carroll 
* Anne Jeffreys as Eleanor 
* Wally Brown as Eddie Roberts 
* Iris Adrian as Bunny Wells 
* Morgan Conway as Joe 
* Alan Carney as Angel 
* Myrna Dell as Mrs. Dumont 
* Matt McHugh as William Dumont 
* Claire Carleton as Sally Beaver 
* Jason Robards Sr. as Sheriff 
* Matt Willis as Hank the Deputy

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 