Three Many Weddings
{{Infobox film
| name           = Three Many Weddings
| film name      = 3 bodas de más
| image          =
| caption        = 
| director       = Javier Ruiz Caldera
| producer       = 
| writer         = Pablo Alén Breixo Corral Laura Sánchez Berto Romero Rossy de Palma
| music          = Javier Rodero
| cinematography = Arnau Valls Colomer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Best Comedy award in the first edition of Premios Feroz.

==Plot==
Ruth (Inma Cuesta) is a marine biologist who, in less than a month, is invited to the weddings of three of her ex-boyfriends. Single and unable to refuse, she manages to convince her new intern, Dani (Martiño Rivas), to go with her. During the weddings, plenty of outlandish events will take place and eventually lead Ruth to decide who she wants with her in the future. 

==Cast==
*Inma Cuesta as Ruth
*Martiño Rivas as Dani
*Quim Gutiérrez as Jonás
*Berto Romero as Pedro
*Paco León as Mikel Laura Sánchez as Álex
*María Botto as Ruths boss
*Silvia Abril as Lucía
*Joaquín Reyes (actor)|Joaquín Reyes and Rossy de Palma as Ruths parents
*Bárbara Santa-Cruz as Catalina

==Awards and nominations==
{| class="wikitable"
! Awards !! Category !! Nominated !! Result
|- Goya Awards  Best Main Actress Inma Cuesta
| 
|- Best Original Screenplay Pablo Alén & Breixo Corral
| 
|- Best New Actor Berto Romero
| 
|- Best Production Marta Sánchez de Miguel
| 
|- Best Editing Alberto de Toro
| 
|- Best Costume Design Cristina Rodríguez
| 
|- Best Make-Up and Hairstyles Eli Adánez & Sergio Pérez
| 
|- Premios Feroz  Best Comedy
| 
|- Best Screenplay Pablo Alén & Breixo Corral
| 
|- Best Main Actress Inma Cuesta
| 
|- Best Supporting Actress Rossy de Palma
| 
|-
|Bárbara Santa-Cruz
| 
|- Best Trailer
| 
|- Best Film Poster
| 
|- Neox Fan Awards Best Spanish film 
| 
|- Best Spanish film actor  Quim Gutiérrez
| 
|- Best Spanish film actress  Inma Cuesta
| 
|- Best kiss  Quim Gutiérrez & Inma Cuesta
| 
|}

==References==
 

==External links==
* 

 
 
 