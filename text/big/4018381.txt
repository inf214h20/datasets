Duo (film)
 

Duo: The True Story of a Gifted Child with Down Syndrome is a 1996 independent film produced and directed by Alexandre Ginnsz, starring his 12-year-old brother Stephane Ginnsz. It is notable for featuring the first lead actor with Down syndrome in film history.

==Awards==
* WINNER OF THE MARTIN SCORSESE BEST FILM AWARD (1996)
* WINNER OF THE WARNER BROS. PICTURES BEST FILM AWARD (1996)
* WINNER OF THE WASSERMAN AWARD FOR BEST CINEMATOGRAPHY (1996)
* FINALIST AT THE CHICAGO INTERNATIONAL FILM FESTIVAL (1996)
* OFFICIAL ENTRY AT THE ACADEMY AWARDS (STUDENT) (1996)
* NOMINATED FOR THE T.A.S.H. MEDIA AWARD (1996)
"...for best promoting the inclusion of people with severe disabilities in all aspects of community life and reaching a national audience."

... and following the release of Duo on DVD in 2005:
* Featured Guest at the National Down Syndrome Congress Convention (2005)
* Featured Guest at the New York Sprouts International Film Festival (2005)  

== References ==
 

==External links==
* 
* 
* 
* 

 
 
 


 