Amores perros
 
 
{{Infobox film
| name            = Amores perros
| image           = Amores perros poster.jpg
| alt             = 
| caption         = US release poster
| director        = Alejandro González Iñárritu
| producer        = Alejandro González Iñárritu
| writer          = Guillermo Arriaga
| starring        = Emilio Echevarría Gael García Bernal Goya Toledo Álvaro Guerrero Vanessa Bauche Jorge Salinas Adriana Barraza
| music           = Gustavo Santaolalla
| cinematography  = Rodrigo Prieto
| editing         = Alejandro González Iñárritu Luis Carballar Fernando Pérez Unda
| studio          = Zeta Entertainment Alta Vista Films
| distributor     = Nu Vision
| released        =  
| runtime         = 153 minutes  
| country         = Mexico
| language        = Spanish
| budget          = $2.4 million 
| gross           = $20.9 million 
}} drama Thriller thriller film Pulp Fiction",  containing three distinct stories which are connected by a car accident in Mexico City. 

Each of the three tales is also a reflection on the cruelty of humans towards both animals and other humans, showing how humans may live dark or even hideous lives. But the films theme is loyalty, as symbolized by the dog, "mans best friend". Dogs are important to the main characters in each of the three stories, and in each story various forms of human loyalty or disloyalty are shown: disloyalty to a brother by trying to seduce the brothers wife, disloyalty to a wife by keeping a mistress with subsequent disloyalty to the mistress when she is injured and loses her beauty, loss of loyalty to youthful idealism and rediscovered loyalty to a daughter as a hit-man falls from and then attempts to regain grace.
 Bersuit Vergarabat. Amores perros was nominated for the Academy Award for Best Foreign Language Film in 2000 and won the Ariel Award for Best Picture from the Mexican Academy of Film.

==Plot==
The film is constructed from three distinct stories linked by a car accident that brings the characters briefly together.

;Octavio y Susana
The first segment stars Gael García Bernal and Vanessa Bauche as the title characters. Susana is Octavios sister-in-law; however, Octavio is in love with her and doesnt like the way his brother, Ramiro, treats her. Octavio tries to persuade her to run away with him to get out from under Ramiros abuse. Needing to make money so that he and Susana can escape and start a life of their own, Octavio becomes involved in the business of dog fighting. Octavio makes enough money to run away with Susana, but Susana takes the money and leaves with her husband. Octavio continues his dog fighting business until a rival owner shoots his dog Cofi. Octavio stabs the rival owner and finds himself in a car chase with his lifelong friend, Jorge, and the wounded dog. A collision follows; Jorge dies and Octavio is badly injured.

;Daniel y Valeria Spanish supermodel Valeria played by Goya Toledo. Valerias leg is severely broken in the accident with Octavios car and she may be unable to continue working as a model. Valeria is using a wheelchair while she recuperates in the apartment she shares with Daniel. Her dog Richie disappears under the floorboards one day and stays there for days. The missing dog triggers serious tension for the couple, causing numerous fights which leads to doubts about their relationship on both sides. Valeria re-injures her leg trying to help the dog, resulting in severe internal bleeding and eventually gangrene. Her doctor is forced to amputate the leg, removing any chance she might have had at returning to her modeling career. Once her leg is gone, Valeria realizes that her life is most likely ruined, since her sense of purpose, modeling, has been taken from her. Later, we see Daniel calling his estranged wife to hear her voice and then hanging up without speaking, suggesting either that he regrets his disloyalty to her, that he realizes that his attraction to Valeria was based only on her now lost beauty, or perhaps both.

;El Chivo y Maru
The final segment stars Emilio Echevarría and Lourdes Echevarría. The story concerns a former private school teacher who had become involved in guerrilla movements that landed him in prison for 20 years. He appears in the film as a bedraggled vagrant pushing a junk cart accompanied by several mongrel dogs for whom he cares. Though he appears to live in perpetual squalor in an abandoned warehouse, he is in fact a professional hitman, El Chivo (The Goat). At times throughout his story, Chivo tries to make contact with his daughter, Maru, whom he abandoned when she was a two-year-old child when he began his guerrilla involvement. Marus mother told Meru that her father had died, instead of telling Maru the truth about her fathers abandonment and prison sentence.

As the story progress, El Chivo is hired by a businessman to kill his partner. El Chivo is about to make the kill when the films central car crash interrupts him. During the chaos at the crash scene, El Chivo steals Octavios money and takes his wounded dog Cofi to his home to nurture it. While El Chivo is away from the warehouse one day, Cofi, due to his proclivity for dog fighting, kills all of the other dogs in El Chivos house. El Chivo is intensely upset and prepares to kill Cofi, but El Chivo forgives Cofi, figuring that Cofi doesnt know any better, and seeing Cofis violence as a reflection of his own life as a hitman.

Meanwhile, Ramiro and an accomplice are attempting to rob a bank when Ramiro is shot and killed by a Plainclothes law enforcement|plain-clothes policeman. Octavio, seriously injured from the car accident, sees Susana for the first time since she and Ramiro fled with his money. Despite having been wronged, Octavio tries again to get Susana to run away with him, but she becomes angry with the fact that Octavio is willing to run away with her after she has just lost somebody she loves.

Still grieving for his beloved dogs, El Chivo captures his intended victim, and, after learning that the victim is the clients half-brother, also captures his client. After shaving his beard and grooming his hair (leading to a drastic change in his appearance) he leaves both men alive and chained to the separate walls with a pistol within reach between them, their fate left undetermined. El Chivo then breaks into his daughters house while she is away and leaves her a large bundle of money along with a message on her answering machine explaining what happened to him and why the family was split. Just before El Chivo was to tell his daughter Maru that he loves her, the answering machine stops recording. El Chivo then goes to an autoshop, where he sells the clients SUV. The mechanic asks him the dogs name. El Chivo calls him "Negro" (Spanish for "Black"). After El Chivo receives the money for the car, Chivo and Negro walk away and disappear once more.

==Themes==
===Inequality===
The three overlapping stories all take place in Mexico City, but because of class division, there is severe segregation of economic classes with El Chivo squatting on the outskirts of town, Octavio living in a working-class settlement/neighborhood, and Valeria living in a luxury high-rise apartment.  If not for the car accident, these three characters would never interact. The upperclass is victimized in Amores perros even when they are the ones perpetuating crime, for instance, El Chivo is hired to kill a man’s business partner and eventually decides to leave both men to fight it out themselves. Although Ramiro works at a grocery store, he also participates in the underground economy by committing robberies. Octavio and El Chivo participate in the underground Mexican economy as well, in order to secure untaxed income and bring stability to their lives. 

===Violence===
Amores perros contains domestic violence, gun violence, and animal cruelty. 

===Dogfighting===
Dogfighting is banned in most Latin American countries and exists as an element of the underground economy that comes to exist in working class societies. Although violent, dogfighting provides an opportunity for Octavio to make money. This is true to life in the sense that participating in the underground economy gives people in the lower class the ability to make money and experience mobility. González Iñárritu was heavily criticized for his inclusion of dogfighting in the film but has said himself that although it is horrible, dogfighting is one of the harsh realities of Mexico City. 

==Production==
Produced by Zeta Film and AltaVista Films, production began on 12 April 1999.
 commentary track, by the director and the screenplay writer. A controversial aspect of the film is the dog fighting sequences. González Iñárritu explains that no dogs were harmed during the making of Amores perros. In the scenes where dogs are apparently attacking each other, they were actually playing. Their muzzles were covered with fine fishing line, so that they were unable to bite another dog. In the shots where dogs are apparently dead or dying, they were sedated (under supervision of the Mexican SPCA). The grittiness of the scenes is amplified by quick cuts and sound effects. Another, unusual, aspect of the production of Amores perros, was the danger to the cast and film crew while filming in the poor parts of Mexico City. The director and some of the crew were actually robbed by street gangs.

==Awards==
The film was met with very positive reviews from critics and received many nominations and awards. Review aggregate Rotten Tomatoes reports that 92% of critics have given the film a positive review based on 111 reviews, with an average score of 7.8/10, making the film a "Certified Fresh" on the websites rating system.
* The film won the BAFTA Award for Best Film Not in the English Language and was nominated for the Academy Award for Best Foreign Language Film.
* The film won the top award at the 2001 edition of Fantasporto.
* The film won the Prize of the Critics Week at the 2000 Cannes Film Festival. Grand Prix of the Belgian Syndicate of Cinema Critics.
* Amores perros also appeared on Empire (magazine)|Empires 2008 list of the 500 greatest movies of all time where it ranked at number 492. 

==See also==
* Hyperlink cinema - the film style of using multiple inter-connected story lines. Aayutha Ezhuthu - Hindi/Tamil films with similar narrative structure.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 