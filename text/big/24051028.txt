No Time for Sergeants (1958 film)
{{Infobox film
| name           = No Time for Sergeants
| image          = No time for seargents - movie poster.png
| image_size     = 225px
| caption        = 1958 movie poster
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy Alex Segal
| writer         = Mac Hyman (novel) Ira Levin (play)
| screenplay     = John Lee Mahin
| narrator       = Andy Griffith Nick Adams
| music          = Ray Heindorf
| cinematography = Harold Rosson
| editing        = William H. Ziegler
| distributor    = Warner Bros. Pictures
| released       = May 29, 1958 (US)
| runtime        = 119 minutes
| country        = United States English
| budget         =
| gross          = $7.2 million (est. US/ Canada rentals) 
}}
 Broadway cast. Nick Adams the original novel.

== Plot == Georgia who Macon and Atlanta. Pa Stockdale says he has been to those cities many years before and he was ridiculed.

Wrongfully shackled by McKinney, Stockdale joins a group of new United States Air Force draftees being transported to basic training. They include the obnoxious bully Irving S. Blanchard, who having undergone ROTC training, volunteers to be in charge. (Stockdale hears that Irving had ROTC and thinks its a disease.)

They report to boot camp, where Stockdale and his equally dim, but smarter friend, Ben Whitledge, begin the struggle to become Airmen.

Stockdale is incredibly strong and can drink any man under the table. He proceeds to make life miserable for the man in charge, Master Sergeant Orville C. King, who likes his barracks to be quiet and calm. King wears Air Crewmember Wings and states, "he is 45 years old, has eighteen years in the military service and sixteen years as a sergeant." In exasperation, the sergeant places the country bumpkin on full-time latrine duty. Stockdale believes his new position of "P.L.O." (Permanent Latrine Orderly) to be a promotion.
 War Between the States, it was the infantry that did the fighting," which is understandable, airplanes not having been invented yet.

King tells Will the Captain desires to see the barracks and latrine spotless. A company inspection takes a surprising turn when Stockdales immaculately clean latrine is what impresses Kings company commander most. King gets into hot water, however, when Stockdale opens his big mouth and reveals that the sergeant kept him on bathroom duty on a permanent basis while also neglecting to have the recruit complete all the required military exams and paper work. The company commander orders King to have Stockdale through his training in one week, or else he will not only be demoted, but be made "Permanent Latrine Orderly" himself.

Rushing him through testing, King bribes Stockdale by promising to give him his wristwatch, if he can pass. Stockdale flummoxes and frustrates the various officer and men, he takes a manual dexterity test conducted by Corporal John C. Brown (Don Knotts), a psychiatric test by Maj. Royal B. Demming (James Millhollin), and an eye exam, amazingly managing to get by after driving them all crazy. Stockdale gets the wristwatch as a reward.

Ben comes to attention when a WAF Captain (Jean Willes) speaks, Will is speechless. Later, she is escorted into the Enlisted Mens Mess Hall by the Colonel. After being informed of military etiquette as to female officers by Ben, when Blanchard and King ask Will what he sees he says, "a Captain."
 
Private Blanchard tells M/Sgt. King to get three passes to go to a local bar, "The Purple Grotto" where King and Blanchard try to get Stockdale drunk in order to make him look bad and King look good. He admits he has never drunk...store bought liquor. King and Blanchard are inebriated, but Will is still sober. Stockdale says the only alcohol he previously had was what his father made with corn, grain and kerosene. Blanchard buys Lighter Fluid from a cigarette girl. Mixing gin and bourbon with kerosene ... Will drinks it and says "its familiar." Then a drunken Army infantryman walks by and a barroom brawl begins. Stockdale leaves behind a fighting Blanchard and King; as he walks past the Air Police, he tells them that upstairs is the bar.

The colonel and captain later inspect the latrine and barracks. Will has mechanically rigged all the toilet seats to open simultaneously in a "salute". While Blanchard is arrested and detained by the Air Police, M/Sgt. King is found filthy in a torn uniform, later in the latrine, and is summarily reduced to private rank, while Whitledge was blamed for Kings appearance and is also placed in disgraced status. As King goes back to his office dejected, he admits to Stockdale that he and Blanchard had been trying to trick Stockdale to get him out of the way so that King would not look bad, but their effort backfired. Will gives the watch back to King. King also admits he had grown to like Stockdale and became his reluctant and inadvertent mentor to success in the Air Force. Will, Ben and Pvt. King are sent to Gunnery School. Will is at the bottom of the class and Ben is next to the bottom. King graduates as the top man of the class. He is assigned to General Eugene Bushs staff and is given back his M/Sgt. rank.

The story ends with Stockdale and Whitledge (who now has disdain for Stockdale for ruining his image in front of the captain back in the barracks) flying to Denver in an obsolete B-25 medium bomber. Stockdales assignment is tail gunner on the bomber. After putting the plane on Autopilot, the lazy pilots fall asleep, and the airplane soon becomes lost at night over what the navigator thinks is the Gulf of Mexico. They really are flying over the Atomic Bomb Test Site at Yucca Flats, Nevada, during an A-bomb test called "Operation Prometheus". The radio operator of the plane was left behind at the base, so Stockdale and Whitledge must radio to obtain their real position; however, the radio is inoperatable. Stockdale remembers that back home in Georgia, his father would spit into the radio and smack it to make it work. Stockdale repeats the method, and the radio works. Military radiomen on the ground, confused by Stockdales folksy, clownish speech, have the commander of the test Maj. Gen. Vernon Pollard, U.S. Army Infantry frantically rouse Maj. Gen. Eugene Bush, U.S. Air Force from his sleep to confirm that Stockdale is not a prankster, and to give Stockdale emergency flying instructions. Bush reassures Stockdale that he is real and not a foreign agent as King tells Bush to remind Stockdale of the watch. Meanwhile, a fire breaks out in the aft of the plane. The Air Force and Army are put on full alert. Stockdale and Whitledge bail out of the plane just before it crashes, and are declared dead by the Staff Aide M/Sgt. King. The officers survive and are to be decorated. During an Air Medal ceremony honoring the officers and Stockdale and Whitledge as fallen heroes, they reappear, and the Air Force has to cover up that the pair are alive to avoid an international public humiliation.

Stockdale suggests both he and Ben (who finally forgives Stockdale for his unintentionally innocent flaws, as he thought he and Stockdale were to be executed for being deserters, inadvertently faking their deaths and for perpetrating a public fraud at the ceremony) be transferred to become infantrymen. Will and Ben are taken at night to a secluded area. An agreement is reached by two former West Point classmates, General Pollard, USA and General Bush, USAF, who also heartily approve of Stockdales last request—to have M/Sgt. King transferred with them to the infantry. General Pollard has to cut an Air Medal from his uniform. He sits as General Bush pins the Air Medal on Ben Whitledge. As a radio station signs off, to The Star-Spangled Banner, all the men snap to attention, including a reluctant General Pollard.

== Cast ==
  
* Andy Griffith as Pvt. Will Stockdale
* Myron McCormick as M/Sgt. Orville C. King Nick Adams as Pvt. Benjamin B. Whitledge
* Murray Hamilton as Irving S. Blanchard Howard Smith as Maj. Gen. Eugene Bush, U.S. Air Force
* Will Hutchins as Lt. George Bridges  B-25 (pilot)
* Sydney Smith as Maj. Gen. Vernon Pollard, U.S. Army
* James Millhollin as Maj. Royal B. Demming (psychiatrist)
 
* Don Knotts as Cpl. John C. Brown (dexterity tester)
* Jean Willes as WAF Captain
* Bartlett Robinson as Captain
* Henry McCann as Lt. Cover
* Dub Taylor as McKinney (draft board man) William Fawcett as Pa Stockdale
* Raymond Bailey as Base Colonel
* Jamie Farr as Lt. Gardella B-25 (co-pilot)
 

Cast notes
*As of 2014, Hutchins and Farr are the last surviving cast members.

== Reception ==
The film was a major hit, and was largely responsible for launching the popular careers of Griffith and Knotts. 

==References==
Notes
 

== External links ==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 