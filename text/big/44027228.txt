Yakshagaanam
{{Infobox film
| name           = Yakshagaanam
| image          =
| caption        =
| director       = Sheela
| producer       = Mathi Oli Shanmukham
| writer         = Medhavi S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Madhu Sheela Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = M. S. Viswanathan
| cinematography = KB Dayalan Melli Irani
| editing        = Ravi
| studio         = Apsara Combines
| distributor    = Apsara Combines
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed by Sheela and produced by Mathi Oli Shanmukham. The film stars Madhu (actor)|Madhu, Sheela, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by M. S. Viswanathan.    The film was remade in Tamil as Aayiram Jenmangal and in Telugu as Devude Gelichadu.

==Cast==
  Madhu
*Sheela
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Manavalan Joseph
*Adoor Pankajam
*Jayakumari
*K. P. Ummer Sadhana
*T. K. Balachandran
*T. P. Madhavan
*Ushanandini
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arupathinaalu Kalakal || LR Eeswari || Vayalar ||
|-
| 2 || Nisheedhini Nisheedhini || S Janaki || Vayalar ||
|-
| 3 || Pokaam Namukku || S Janaki || Vayalar ||
|-
| 4 || Thenkinnam Poonkinnam || K. J. Yesudas, P Susheela || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 
 


 