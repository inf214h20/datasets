Don't Cry, Nanking
{{Infobox film
| name = Dont Cry, Nanking
| image =
| caption =
| director = Wu Ziniu
| producer = Wang Ying-Hsiang
| writer =
| starring = Chin Han Rene Liu Cho Yuet
| music = Tan Dun
| cinematography =
| editing = LS Group
| distributor =
| released =  
| runtime = 110 minutes
| language = Mandarin
| budget =
| film name = {{Film name| jianti =南京１９３７
| fanti =南京１９３７
| pinyin =Nánjīng yī jiǔ sān qī}}
}} Chinese film about the 1937 Nanking Massacre committed by the Imperial Japanese Army in the former capital city Nanjing, China.

==Plot==
The story focuses on a family, a Chinese doctor, his pregnant Japanese wife and their two children, who escaped the Battle of Shanghai hoping to seek refuge in the capital where the doctor was born.
 International Committee for Nanking Safety Zone.

Among historical characters such as John Rabe and Minnie Vautrin (whose names have been curiously changed to John Robbins and Whitney Craft in the English translation), the film also features an out-of-context excerpt of the infamous Contest to kill 100 people using a sword between Toshiaki Mukai and Tsuyochi Noda.

There are some painful scenes such as the execution, by machine gun, of thousands of Chinese prisoners of war.
 The Rape of Nanking and Herbert Bixs Hirohito and the Making of Modern Japan, the movie shows General Iwane Matsui giving the order to "kill all the captives" and omits any reference to Prince Asaka.

==Cast== Chin Han - Shing Yin
*Rene Liu - Shu Qin
*Cho Yuet - Lui Oi
*Ulrich Ottenburger - John Rabe John Magee
*Rebecca Peyrelon - missionary Miss Hua

== Reception ==
Upon its release in 1995, the movie grossed HK$2,102,915 in Hong Kong.  The film was not released in Japan until December 1997, nearly two years after its completion. 

== See also ==
*Nanking (2007 film)|Nanking (film)
* 
*Japanese war crimes Tokyo Trial (film)

== References ==
 

==External links==
* 
*  from the Chinese Movie Database
*  on director Wu Ziniu

 
 
 
 
 
 
 
 
 


 