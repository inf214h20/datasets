The Party Goes On
{{Infobox film
| name =   The Party Goes On
| image =
| image_size =
| caption =
| director = Enrique Gómez
| producer =  Juan Martín   Andrés Rodríguez Villa   Enrique Gómez 
| narrator =
| starring = Antonio Casal   María Isbert   Juan Vázquez
| music = Jesús García Leoz  
| cinematography = Sebastián Perera  
| editing = Antonio Gimeno 
| studio = Sagitario Films
| distributor = 
| released = 31 December 1948   
| runtime = 110 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
The Party Goes On (Spanish:La fiesta sigue) is a 1948 Spanish drama film directed by Enrique Gómez and starring Antonio Casal, María Isbert and Juan Vázquez. It is set in the world of bullfighting 

==Cast==
*   Fernando Aguirre as Empresario  
* Rafael Albaicín as Rafael 
* Gabriel Algara 
* Margarita Andrey as Genoveva 
* Manuel Arbó 
* Mariano Asquerino as Marqués 
* Rafael Bardem     
* Antonio Casal as Pacorro  
* Carlos Casaravilla as M. Chambom  
* Juan de Landa as Padre prior  
* Marianela de Montijo as Gitanilla  
* Roberto Font as Felipe  
* Luís Gómez as Torero  
* Casimiro Hurtado as Alguacil  
* José Isbert as M. Lapin  
* María Isbert as Campesina  
* Tony Leblanc as Joaquín  
* Juana Mansó as Madre  
* Leonor María as Soledad  
* Arturo Marín as Alcalde  
* José Prada as Mr. Peel  
* Joaquín Roa as Pregonero  
* Alberto Romea as Mr. Wines  
* Rafael Romero Marchent as Torero  
* José María Seoane as Cristóbal  
* Juan Vázquez as Fraile  

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008. 

== External links ==
* 

 
 
 
 
 
 
 

 