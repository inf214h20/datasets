The Last Stop (film)
{{Infobox film
| name           = The Last Stop
| image          = The Last Stop Film Poster.jpg
| caption        = 
| director       = Marcio Cury
| producer       = Elizabeth Curi
| writer         = Di Moretti
| starring       = Mounir Maasri   Klarah Lobato   Elisa Lucinda
| music          = Patrick De Jongh
| cinematography = Krishna Schmidt
| editing        = Dirceu Lustosa
| studio         = Asacine Produções
| distributor    = Polifilmes (Brazil) Day Two Pictures (Lebanon)
| released       =  
| runtime        = 114 minutes
| country        = Brazil Lebanon
| language       = Portuguese Arabic
| budget         = BRL|R$ 3,400,000
}} 45th Festival 36th São 35th Cairo 16th Cine Las Americas International Film Festival in Austin, Texas. 

The film follows the story of a Lebanese boy that is forced to leave his homeland, involved in a war that erupted in the Middle East, and migrate with the family to Brazil. 

==Plot==
The Lebanese teenager Tarik leaves his hometown in search of a better life in Brazil. On the journey by ship, he befriended other young Arabs and Syrians, but when they reached the country, each went to a different way. After 50 years, Tarik, with the help of his daughter, resolves to find the friends of the trip. 

==Cast==
 
*Mounir Maasri as Tarik
*Elisa Lucinda as Ciça
*Klarah Lobato as Samia
*João Antônio as Karim
*Edgard Navarro as Joseph
*Narciza Leão as Mouna
*Iberê Cavalcanti as Mohamad
*Adriano Siri as Hassan
*José Charbel as Hanna
*Chico Santanna as Ali
*Sérgio Fidalgo as Mustafa
*Roula Hamadeh as Mother (Najla)
*Claude Khalil as Postman
*Estephan Ghassan as Father
*Adriano Barroso as Ribeirinho Porto de Belém
*Ghassan Estefan as Father
*Sergio Fidalgo as Mustafa
 

==Production==
The screenwriter Di Moretti interviewed ten Lebanese families as a source of research for the film. With three of them he did more in-depth interviews, in order to learn more about the Lebanese culture.   

According to Moretti, there were two versions of the screenplay: one to be entirely filmed in Brazil and another with scenes to be shot in Lebanese territory. The second version was used in the film. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 