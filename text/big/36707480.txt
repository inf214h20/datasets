El Agente 00-P2
 

{{Infobox film
| name           = El Agente 00-P2
| image          = 
| caption        = Theatrical release poster
| director       = Andrés Couturier
| producer       = Fernando de Fuentes   Jose C. Garcia de Letona   Federico Unda
| screenplay     = Martinez Vara Adolfo   Andrés Couturier   Alberto Rodriguez
| story          = Jose C. Garcia de Letona
| starring       = Jaime Camil Dulce María Silvia Pinal
| music          = Alejandro de Icaza
| cinematography = 
| editing        = 
| studio         = Ánima Estudios Fidecine
| distributor    = Videociné
| released       =  
| runtime        = 93 minutes 
| country        = Mexico
| language       = English
| budget         = $1.8 million   
| gross          = $1,518,150   
}}
El Agente 00-P2 (also known as Agent Macaw: Shaken & Stirred) is a 2009   movies.

==Synopsis==
Tambo Macaw (Jaime Camil), an overweight macaw who works as a janitor at the Central Intelligentus Animalus (CIA) dreams of becoming a secret agent. The opportunity arises when he is assigned, by mistake, the most important mission in the agencys history; stop the wicked plans of Mamá Osa (Silvia Pinal) and her evil organization. Tambo is helped by Gino Tutifrutti (Mario Castañeda Partido), an old turtle thats in charge of the “advanced” technology lab in the agency and is one of Tambos few friends. This odd couple journey into the coolest and craziest adventure ever, which will lead them to faraway places where theyll face weird and dangerous foes. Our heroes will have to use their limited resources – in Tambos case, his limited intelligence – to save the world from an icy extinction.

==Cast==
*Jaime Camil as Tambo Macaw, a macaw
*Dulce María as Molly Cocatu, female macaw
*Silvia Pinal as Mamá Osa, a polar bear
**Ana Paula Fogarty as Niña Mamá Osa
*Mario Castañeda as Gino Tutifrutti, a cameleon
*Luis Alfonso Mendoza as Jacinto Tortugo, a turtle
*Rogelio Guerra as Jefe Lipo, a hippo
*José F. Lavat as Yunque, an ox

==Box office== MXN ($347,138 USD) on its opening weekend and grossed a total of $19,444,465 MXN ($1,518,150 USD) during its theatrical run. 

==Soundtrack==
This film features two hit singles, One Way or Another, performed by Blondie (band)|Blondie, and Summer Fun, performed by The Barracudas.

==See also==
* 
*James Bond in film
*Ánima Estudios

==References==
 

==External links==
* 
* 

 

 
 