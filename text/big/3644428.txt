Good Scouts
 
{{Infobox Hollywood cartoon cartoon name=Good Scouts
|series=Donald Duck
|image=Good Scouts.jpg image size=
|alt=
|caption=Donald and his nephews arrive at their camp site Jack King
|producer=Walt Disney story artist=Carl Barks Harry Reeves voice actor=Clarence Nash
|animator=Paul Allen Al Eugster Frank Follmer Jack Hannah Ed Love Walt Disney Productions RKO Radio Pictures release date=  (USA) color process=Technicolor
|runtime=8 minutes
|country=United States
|language=English preceded by=Donalds Nephews followed by=Donalds Golf Game
}}
  (standing with pointer) and Harry Reeves (clowning) present the storyboard for Good Scouts in 1937]] Walt Disney RKO Radio Jack King and features Clarence Nash as Donald and the three nephews. 
 Ferdinand the Bull. Also nominated that year from Disney were Brave Little Tailor and Mother Goose Goes Hollywood, setting the record for most nominations in the category for one studio. Good Scouts was the first Academy Award nomination for the Donald Duck series.

==Plot== petrified tree and pitch a tent with bad knots causing the nephews to laugh.

Frustrated at the nephews lack of gratitude for his efforts, Donald decides to make them sorry by pretending to have been injured, pouring ketchup over himself. The dutiful nephews spring into action and quickly bandage Donald from head to toe. Donald is then unable to see and wanders aimlessly, eventually falling into a honey jar.

A large grizzly bear soon arrives having been attracted by the smell of food. Trying to escape the bear, Donald runs off a cliff and falls onto "Old Reliable Geyser" (a reference to Old Faithful) and gets his rear end stuck in the opening of the geyser. The water shoots Donald into the air, bringing him closer to the bear who is still above at the cliffs edge.

The nephews try to save Donald by plugging the geyser. They finally roll a large boulder over it, but the geyser is only stopped momentarily. Donald is seen later that night, still running from the bear on top of the boulder rotating under their feet, perfectly balanced on top of the continuous stream of water from the geyser. The nephews, having exhausted their means of rescuing their uncle, bed down for the night in their tent, wishing him "Good night", "Unca", "Donald".

==Character development== Donald Duck comics, and marked a significant change in their behavior. In their earlier appearances, the triplets were mischievous and caused a lot of trouble for Donald. But in Good Scouts the boys seem to have matured a great deal. They are shown as being resourceful, using teamwork, and generally trying to help Donald out of trouble. Although the boys are seen as mischievous in several later appearance, it was this more helpful characterization which would become more characteristic of them.
 Sea Scouts (1939) and Home Defense (1943).

==Releases==
*1938 &ndash; theatrical release
*1956 &ndash;  " (TV) Walt Disneys Wonderful World of Color, #8.11: "Kids is Kids" (TV)
*1965 &ndash; Donald Duck Goes West (theatrical)
*1981 &ndash; "Kids is Kids Starring Donald Duck" (VHS)
*c. 1983 &ndash; Good Morning, Mickey!, episode #69 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #61 (TV) The Ink and Paint Club, episode #1.20: "Huey, Dewey and Louie" (TV)
*2004 &ndash; " " (DVD)
*2005 &ndash; " " (DVD)
*2011 &ndash; iTunes (digital download)

==See also==
*Good Scout Award
*Junior Woodchucks

==References==
 

==External links==
* 
* 
*  at The Encyclopedia of Animated Disney Shorts
 
 
 
 
 
 