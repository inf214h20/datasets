Après la vie
 
 
{{Infobox film
| name           = Après la vie 
| image          =
| caption        = 
| director       = Lucas Belvaux
| producer       = Diana Elbaum Patrick Sobelman
| writer         = Lucas Belvaux
| starring       = Gilbert Melki Dominique Blanc Ornella Muti
| music          = Riccardo del Fra
| cinematography = Pierre Milon
| editing        = Danielle Anezin
| distributor    = Diaphana Films
| released       =  
| runtime        = 124 minutes
| country        = France Belgium
| language       = French
}} thriller and Un couple épatant, a comedy.

Belvaux referred in the DVD commentary that main idea behind Trilogy is that the main characters in a particular story are the secondary characters of others, in such sense the three films happen at the same time and share a series of common scenes and plot points, complementing each other, but also have their own perspective and style. The audience is left with piecing the films together, which Belvaux avoided, since editing the three films into one single narrative would have resulted in a very long film with no style of its own.

==Cast==
{| class="wikitable"
|-
! style="background-color:tan;" width="150" | Actor
! style="background-color:silver;" width="200" | Role
|-
| Dominique Blanc || Agnès Manise, High school teacher, drug addict
|-
| Gilbert Melki || Pascal Manise, Police Inspector
|-
| Ornella Muti || Cécile Coste, High school teacher, friend of Agnès
|-
| François Morel (actor)|François Morel || Alain Coste
|-
| Catherine Frot || Jeanne Rivet, High school teacher and friend of Cécile
|-
| Lucas Belvaux || Bruno Le Roux, posing as Pierre
|-
| Bernard Mazzinghi || Dr. Georges Colinet, surgeon
|-
| Valérie Mairesse || Claire, Alain Costes personal assistant|PA
|-
| Olivier Darimont || Francis Rivet, Jeannes husband
|-
| Patrick Descamps || Jacquillat, a drug lord
|-
| Alexis Tomassian || Banane, a drug dealer
|-
| Yves Claessens || Freddy, a bar owner
|-
| Pierre Gérard || Olivier
|-
| Christine Henkart || Madame Guiot
|-
| Jean-Henri Roger || neighbour
|-
| Raphaële Godin || Louise Coste
|-
| Patrick Depeyrrat || Patrick
|-
| Vincent Colombe || Rémy Coste
|}

==Plot==
  bar where dealer of Jacquillat, a drug lord. A phone call interrupts the conversation but it is very brief. Manise gives the drugs to his wife Agnès Manise, who has been addicted for 20 years but has never had to buy her own fixes because her husband provides her with them. Agnès tells Manise that her friend Cécile Coste has invited them to a party he does not want to go but concedes when she agrees to buy him a suit (clothing)|suit. In the meantime the police is looking for Bruno Le Roux, a former leftist revolutionary who has escaped from prison, this means that they have to tail Jeanne Rivet, a colleague of Agnès in the same high school where they work as teachers. In their search for Le Roux they also go to the house of Mme. Guiot, the mother of Jean-Jean, a former associate of Le Rouxs, she tells them that he has gone to look for a job out of the city.
  fainted at the party, he takes her home and shoots her up himself with the last fix he has, she apologizes and dozes off.

The next day Manise takes part in a school debate regarding drugs, he plays the tough cop who is only concerned for the law, during the conversation, Dr. Colinet, a friend of the Costes, tells the class about alternatives for drug users such as substitute drugs. After the debate, Cécile asks Manise to tail her husband Alain Coste, because he has been hiding something from her. He asks for a photo and inquires if he was ever involved in politics before they married or at the university, she denies it. Manise investigates Alain but seems more interested in Cécile, he checks the files on the Peoples Army and he concludes that Alain is clean other than having a few speeding tickets. He cuts him off the photo and keeps the half with Cécile on it. Manise is called, there has been a fire where the firemen have found lots of guns, in the ensuing investigation they conclude that it was Le Roux, he has left a bunch of evidence behind him and a neighbour gives a full description.

The next day Pascal follows Alain who meets with a younger woman, but when he describes her to Cécile, she recognizes her own daughter, he must start all over again. In the meantime Agnès has been increasingly ill due to   and all men are called in.
 strike to PA does not believe him. Agnès confronts Pascal and tells her that she cannot face the pain anymore but Pascal sets his foot down and asks her to resist for 5 days without any drugs, she agrees and throws him out of the apartment. When walking out he runs into Cécile, she has come to ask him to stop his investigations on Alain, he tells her that he only wanted to compare the PAs hair to a sample that he found in Alains Jaguar Cars|Jaguar. Cécile gives Manise a ride to the Police Station.

In the meantime Agnès has gone out looking for a fix, she tries to buy dope from Banane, a petty drug dealer, she is so desperate that she even offers sex in exchange for a score but he refuses. She attacks Banane but he gets the upper hand until he is stopped by a man who points a gun at him, the stranger allows Agnès to take all the drugs she wants and she goes into the street but finds that the Police have blockaded the entire neighbourhood, she throws all she has and escapes back to Bananes selling post. The man is furious to see her back but remains calm when she tells him about the Police. They stay and she figures out that the Police is looking for him, she offers her apartment to hide if he brings her in, her husband is a cop and no one shall look for him there. Back at the Police Station, Manise talks with Francis Rivet, husband of Jeanne, who is willing to make the necessary statements to get her out of jail, during this conversation it turns out that it was Francis who betrayed the Peoples Army to the Police and if Jeanne has been able to live her life is because the government has nothing on her.

Pascal lets Jeanne go, he asks why none of Agnès friends shall talk to him, for the first time Jeanne answers and lets him know that it is because he is a cop. As she leaves, he catches up with her and offers to drive her home because he spotted a silver car that has been following him around. Manise confronts Jacquillat about his spies and demands that they stop following him and his witnesses. Jacquillat agrees, calls off his henchmen and inquires about Agnès, Manise is very distressed and offers some of his information in exchange for a fix but Jacquillat refuses and he reveals that Le Roux has been with his wife when they assaulted one of his dealers, they drive to his place. Meanwhile, Agnès is overdozing and is saved by the stranger that helped her to get the only fix she managed to save for herself. Manise walks in unnoticed on a conversation between the stranger (Le Roux) and his wife where he promises to get her the drugs she needs, Manise hides as Le Roux takes Agnès to her bed. Manise tells Jacquillat that Le Roux was not there and even offers to pay for the morphine but Jacquillat refuses and leaves.

 
 faculty is celebration for Jeannes liberation. Agnès asks Cécile to borrow her car and tells her that she met someone and wants to use her chalet for a romantic rendez-vous. Cécile is bewildered and reluctantly gives her keys to her car and the chalet. Agnès makes copies of the keys and goes to the parking lot where she meets with "Pierre", they drive to the chalet where they have breakfast. She asks "Pierre" if he got her some dope but he says that so far he has nothing, she becomes angry and leaves but he stops her and offers an explanation of his actions, she refuses to know and tells him that word are just words, actions are what count. She drives back but stops in the middle of the road and ponders on how low she has sunk.

Back at the police station, Dr. Colinet reports Alains disappearance. On a hunch, Manise parks in front of Claires (Alains  s Céciles car with the copies that she made and goes to the chalet looking for Le Roux but he is not there. She leaves a note explaining that he should avoid his known hide outs. She then drives around the city and frustrates the stake outs. Olivier, a friend of Pascals, tells her to avoid going to a public square where there has been a shoot out, most probably linked with Le Roux. She goes there and sees Manise investigating the many deaths of innocent by-standers but sees that Le Roux is still alive.

Manise drives to Cécile Costes place and tells her that Alain is at Claires, they drive there but she has found nothing, he spots a man spying on them and he runs to him, he presses his gun against his head and tells him that the next time he will shoot, Cécile asks him to let him go, the man is Claires boyfriend. Back in his car he confesses to Cécile that Agnès dealer has been blackmailing him, she understands and asks him to drive her to her chalet. She walks in but comes back out after a very shot while, before they get in his car he tells her that he loves her and she slaps him in the face. He leaves. Manise goes to the hospital and asks Dr. Colinet for morphine but the doctor refuses and demands that he leaves despite his physical threats. Back in his place he talks with Agnès and she tells him that he does not love her anymore because he cannot stop her pain. Someone calls at the door, he opens, it is Jeanne, she has come to tell him about Le Rouxs whereabouts. Manise goes to Jacquillats and tells him where to find Le Roux, as the henchmen leave, he calls on his radio to the police and lets them know where to find him too. Manise has received a bunch of drugs from Jacquillat, which he takes to Agnès, seeing the drugs, she concludes what became of Le Roux and calls Manise a bastard. She is about to inject herself with the drugs but stops at the last minute and tosses all her drugs.
 terrace overlooking the city, he pulls his gun out and seems that he will commit suicide, Agnés hugs him and tells him not to leave her alone. He presses the gun to the back of her head, she tells him that she still needs him, he lowers his gun and drops it.

==External links==
* 

 
 

 
 
 
 
 
 
 