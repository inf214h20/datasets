Sandy (novel)
{{infobox book | 
| name          = Sandy
| title_orig    = 
| translator    = 
| image         =  
| caption = Frontispiece illustration by W.L. Jacobs
| author        = Alice Hegan Rice
| illustrator   = W.L. Jacobs
| cover_artist  = 
| country       = 
| language      = English
| genre         = Novel
| publisher     = The Century Company
| pub_date      = April 1905
| english_pub_date =
| media_type    = Print (hardcover)
| pages         = 304
| preceded_by   = 
| followed_by   = 
}}

Sandy is the third book written by  )  It was originally published in serial form in   (December 1904) (pp. 192-207)  (May 13, 1905).  ,   (Vol. 30, No. 25, p. 943) 

The novel is based on the boyhood stories of S. S. McClure, publisher of McClures magazine, and his upbringing in Ireland and early struggles in the United States. Lyon, Peter  , p. 255 (1967) 

==Plot==

A contemporary synopsis of the novels plot describes it as follows:

This is the story of a young Irish boy named Sandy Kilday, who at the age of sixteen, being without home or relatives, decides to try his luck in the new country across the sea.  Accordingly he slips aboard one of the big ocean liners as a stowaway, but is discovered before the voyage is half over and in spite of his entreaties is told he must be returned by the next steamer.  Sandy, however, who has a winning way and sunny smile, arouses the interest of the ships doctor, who pays his passage and gives him some money with which to start his new life.  On the voyage Sandy has made friends with a lad in steerage named Ricks Wilson, who earns his living by peddling, and he decides to join him in this career.  Sandy has also been deeply impressed by the face of a lovely young girl who is one of the cabin passengers and when he discovers that she is Miss Ruth Nelson of Kentucky he decides to make that state his destination.  He and Ricks remain companions for sometime although Sandys strong sense of honor causes disagreements as to the methods of their dealings.  Sandy finally becomes disgusted with this life and after catching a glimpse of Ruth at a circus, where he is dispensing his wares in a humorous manner, he decides to abandon it altogether.

He parts from Ricks and falling ill by the roadside is picked up by a colored woman named Aunt Melvy, who is in the employ of Judge Hollis.  The latter takes Sandy to his home and his wife nurses him through a long fever and then, as they are childless, they adopt him into their household.  The Judge gives Sandy a good education, sends him to college and he becomes a successful lawyer.  All this time his love for Ruth has been unswerving though she has not responded to his advances.  Judge Hollis is shot by an unknown assailant and Sandy, who discovers the assailant to be Ruths dissipated brother Carter, refuses to give evidence against him.  Sandy is kept in jail until freed by Ruths intervention, Carter having confessed his crime to his sister before his death.  The Judge recovers from his wound and Sandy and Ruth are happily married to the satisfaction of all concerned. Warner, Charles Dudley, ed.  , p. 593-94 (1910 edition) 

==Adaptions==
A silent film adaptation was released in 1918, directed by George Melford, and starring Jack Pickford as Sandy Kilday, Louise Huff as Ruth Nelson, and James Neill as Judge Hollis. (August 19, 1918).  , Calgary Daily Herald 

==References==
 

==External links==
* 
*  full scan via Google Books
*   (1918)

 
 
 
 
 
 
 