A Sister to Assist 'Er (1927 film)
{{Infobox film
| name           = A Sister to Assist Er
| image          = 
| alt            =  
| caption        = 
| director       = George Dewhurst
| producer       = Maurice Elvey Gareth Gundrey   Victor Saville
| writer         = George Dewhurst John le Breton
| based on       =  
| starring       = Mary Brough Polly Emery Humberston Wright A. Bromley Davenport
| music          = 
| cinematography = Percy Strong
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Distributors
| released       =  
| runtime        = 6,000 feet 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 silent comedy A Sister to Assist Er by John le Breton.

==Cast==
* Mary Brough as Mrs. May 
* Polly Emery as Mrs. Mull 
* Humberston Wright as Mr. Mull 
* A. Bromley Davenport as Jim Harris 
* Alf Goddard as Sailor  Jack Harris as Alf

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 