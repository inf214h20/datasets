Frankie and Johnny (1991 film)
{{Infobox film
| name           = Frankie and Johnny
| image          = Frankie and Johnny poster.jpg
| caption        = Theatrical release poster
| director       = Garry Marshall
| producer       = Garry Marshall
| writer         = Terrence McNally
| starring       = Al Pacino Michelle Pfeiffer Héctor Elizondo Nathan Lane Kate Nelligan
| music          = Marvin Hamlisch
| cinematography = Dante Spinotti
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| gross          = $22,773,535 (United&nbsp;States) 
}}

Frankie and Johnny is a 1991 American drama film directed by Garry Marshall, and starring Al Pacino and Michelle Pfeiffer in their first film together since Scarface (1983 film)|Scarface (1983). Héctor Elizondo, Nathan Lane and Kate Nelligan appeared in supporting roles. The original score was composed by Marvin Hamlisch.

The screenplay for Frankie and Johnny was adapted by Terrence McNally from his own off-Broadway play Frankie and Johnny in the Clair de Lune (1987), which featured Kenneth Welsh and Kathy Bates. The most notable alteration in the film was the addition of several supporting characters and various locations; in the original play, only the two eponymous characters appeared onstage, and the entire drama took place in one apartment. 
 American popular Frankie and Johnny, first published in 1904, which tells the story of a woman who finds her man making love to another woman and shoots him dead. 
 Frankie and Johnny (1966) starring Elvis Presley and Donna Douglas, takes its name from the song but is in no other way related to this film.

==Plot==
 

Johnny (Pacino) is a middle-aged man, just released from prison whos looking for a job. Hes hired as a short-order cook in a local diner where he meets Frankie, a pretty waitress who is trying to move on with her life after getting out of an abusive relationship. Her only friends seem to be her friendly gay neighbor Tim (Lane) and her fellow waitresses at the diner. Johnny attempts to win Frankies heart but quickly realizes it will be quite a challenge in this true-to-life romantic dramedy.

==Cast==
*Al Pacino as Johnny
*Michelle Pfeiffer as Frankie
*Héctor Elizondo as Nick
*Nathan Lane as Tim
*Kate Nelligan as Cora
*Jane Morris as Nedda
*Greg Lewis as Tino
*Al Fann as Luther
*Ele Keats as Artemis
*Fernando López as Jorge

==Production==
Kathy Bates, the actress who originated the role of Frankie in the 1987 Off-Broadway play, campaigned for the film role but was passed over in favor of Michelle Pfeiffer.  She later said of the eventual casting: "I thought it was wonderful to see a love story about people over forty, ordinary people who were trying to connect... I dont think we will see it with this movie." 

  was filming in a nearby studio, and Garry Marshall arranged for the actors William Shatner (James T. Kirk) and Leonard Nimoy (Spock) to appear fully costumed, out of camera shot, behind a door in one scene in order to elicit genuine surprise from Al Pacino when he opened it.   

==Reception==
Frankie and Johnny currently holds a score of 78% on Rotten Tomatoes based on 18 reviews, indicating generally positive reviews. 

  in the  , who has directed from a screenplay by Mr. McNally that amounts to a complete revision, Frankie and Johnny has been reshaped into foolproof schmaltz. "Foolproof" is the operative word... But somehow Mr. Marshall, Mr. McNally and their superb leading actors are able to retain the intimacy of their material. They also retain the storys fundamental wariness about romance, even when everything about Ms. Pfeiffer and Mr. Pacino has the audience wondering why they dont simply fall into each others arms."    Rita Kempley in the   summed it up thus: "Pacino wears a vest and bandanna and moons through the part. Pfeiffer plays dowdy. Marshall directs as if Marty (film)|Marty had never happened." 

Much attention was paid to the controversial casting choices of   as Marty."  Variety (magazine)|Variety asserted that no one would "believe that Pfeiffer hasnt had a date since Ronald Reagan was president, and no matter how hard she tries to look plain, there is no disguising that she just gets more beautiful all the time."   

However, some critics commended Pfeiffer for her performance, notably Rolling Stone, who called it "a triumph. She is among that rarefied group of actresses (Anjelica Huston, Meryl Streep) whose work keeps taking us by surprise. Her powerfully subtle acting can tickle the funny bone or pierce the heart with equally uncanny skill."  The New York Times wrote that "Ms. Pfeiffers extraordinary beauty makes her fine-tuned, deeply persuasive performance as the tough and fearful Frankie that much more surprising." 
 Dick Tracy), shows a real flair for comic delicacy."  The New York Times wrote that "Mr. Pacino has not been this uncomplicatedly appealing since his Dog Day Afternoon days, and he makes Johnnys endless enterprise in wooing Frankie a delight. His scenes alone with Ms. Pfeiffer have a precision and honesty that keep the films maudlin aspects at bay."  Variety, however, described him as "a warm, slobbering dog who cant leave people alone, Pacinos Johnny comes on real strong, and his pronounced neediness is too much at times." 

Kate Nelligan was singled out for her supporting turn; the New York Times wrote that "Kate Nelligan, nearly unrecognizable, is outstandingly enjoyable as the gum-chewing, man-crazy one."  Rolling Stone thought that "seeing this Royal Shakespeare Company actress cut loose with this bold and brassy performance is one of the films zippiest treats." 

==Awards and nominations== Fried Green Tomatoes (1991), for its "fair, accurate and inclusive representations of the lesbian, gay, bisexual and transgender (LGBT) community and the issues that affect their lives."   

Michelle Pfeiffer was nominated for the Golden Globe Award for Best Actress - Motion Picture Musical or Comedy, but lost to Bette Midler in For The Boys (1991). 

Kate Nelligan won the BAFTA Award for Best Actress in a Supporting Role and the National Board of Review Award for Best Supporting Actress. 

==References==
 

==External links==
 
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 