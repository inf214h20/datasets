The White Bus
{{Infobox film
| name           = The White Bus
| image          = 
| caption        = 
| director       = Lindsay Anderson
| producer       = Oscar Lewenstein (executive producer), Lindsay Anderson (producer) Michael Deeley (associate producer)
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio = Woodfall Film Productions
| distributor    = 
| released       = 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The White Bus is a 1967 short film by British director Lindsay Anderson. The screenplay was jointly adapted  with Shelagh Delaney from a short story in her collection Sweetly Sings the Donkey (1963). 

==Outline== John Sharp) happen to also be taking the trip.
 Albert Square Town Hall Central Library. The model estate of high-rise flats was shot on the Kersal Flats estate, while the factory sequences were shot in Trafford Park, including the Metropolitan-Vickers works. It also featured scenes on Cheetham Hill Road and inside Cheetham College (now demolished).
 Stephen Moore, is a (young) bowler-hatted man who pesters the heroine with nonsense  just after her arrival.

==History and production==
It was originally commissioned by producer Oscar Lewenstein, then a director of Woodfall, as one third of a portmanteau feature entitled Red White and Zero, with the other sections supplied by Andersons Free Cinema collaborators Tony Richardson and Karel Reisz  from the other short stories by Shelagh Delaney.

The "first real days shooting" was on 19 October 1965, and took about a month to complete. 

The two other planned sections of the film developed into what became Richardsons Red and Blue and Peter Brooks Ride of the Valkyrie (1967), Reisz having dropped out, both of which are unrelated to Delaneys work. Of these, only The White Bus received a theatrical release in the UK. 

== Notes ==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 