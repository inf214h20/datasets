Little Miss Marker (1980 film)
{{Infobox Film
  | name           = Little Miss Marker
  | image          = 1980-little-miss-markerPOSTER.jpg
  | caption        =  
  | director       = Walter Bernstein 
  | writer         = Walter Bernstein Damon Runyon| Kenneth McMillan|
  | music = Henry Mancini|
  | cinematography = Philip H. Lathrop|
  | editing        = 
  | producer       = Jennings Lang Walter Matthau|
  | distributor    = Universal Studios 
  | released       = March 21, 1980
  | runtime        = 103 mins. 
  | country        = United States
  | language       = English
}}
 1980 American film of the same name starring Shirley Temple and Adolphe Menjou.

==Plot==
Sorrowful Jones (Matthau) is a gloomy, cantankerous bookie circa 1934, who is confronted by Carter, a gambler who cannot pay a $10 debt. He ultimately gives his 6-year-old daughter (Stimson) to Sorrowfuls gangster-run gambling operation as a "marker" (collateral) for a bet. When he loses his bet and commits suicide, the gangsters are left with the "Kid" on their hands. Sorrowfuls nervous assistant, Regret (Newhart), is concerned about the legalities of this, particularly the kidnapping statutes.

In the interim, a crime boss named Blackie (Curtis) coerces his longtime rival Sorrowful into financing a new gambling joint. It is opened in the stately home of Blackies girlfriend, widowed Amanda Worthington (Andrews), who needs money to buy back her family property. Amanda is also counting on a racehorse of hers called Sir Galahad to ride to her rescue. While the Kids personal needs inconvenience Sorrowful, a father-daughter relationship develops between them and they become inseparable. Amanda also takes a liking to the Kid, and reluctantly, the icy Sorrowful, who eventually comes to love her as well—much to Blackies chagrin.

==Cast==
* Walter Matthau as Sorrowful Jones
* Julie Andrews as Amanda Worthington
* Tony Curtis as Blackie
* Bob Newhart as Regret
* Sara Stimson as the Kid
* Brian Dennehy as Herbie Kenneth McMillan as Brannigan
* Lee Grant as the Judge Andrew Rubin as Carter, the Kids father
* Ralph Manza

==Award Nominations==
In 1981, Sara Stimson was nominated for the female Young Artist Award in the category of Best Major Motion Picture - Family Entertainment. Stimson lost to Diane Lane for her performance in Touched by Love.  Little Miss Marker would become Stimsons only acting credit. 

==Notes==
An earlier remake of Little Miss Marker — entitled 40 Pounds of Trouble — also featured Tony Curtis in a modified Sorrowful Jones role. It failed dismally at the box office.

==References==
 

==External links ==
*  

 
 
 
 
 
 
 
 


 