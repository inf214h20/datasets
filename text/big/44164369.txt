Onninu Purake Mattonnu
{{Infobox film
| name           = Onninu Purake Mattonnu
| image          =
| caption        =
| director       = Thulasidas
| producer       = Sundarlal Nahatha
| writer         = Thulasidas
| screenplay     = Thulasidas Rohini
| music          = Kannur Rajan
| cinematography = P Kumar
| editing        = Hariharaputhran
| studio         = Melody Creations
| distributor    = Melody Creations
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Rohini in lead roles. The film had musical score by Kannur Rajan.   

==Cast==
*Thilakan
*Nedumudi Venu
*Ratheesh Rohini
*Devan Devan
*MG Soman
*Mala Aravindan
*Santhakumari Shari

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hridaya Swaram || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|-
| 2 || Irul Moodum || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 