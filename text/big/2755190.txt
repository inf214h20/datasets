Berlin Express
{{Infobox film
| name           = Berlin Express
| image          = BerlinExpress.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Jacques Tourneur
| producer       = Bert Granet
| screenplay     = Harold Medford
| story          = Curt Siodmak
| starring       = Merle Oberon Robert Ryan Charles Korvin
| music          = Frederick Hollander
| cinematography = Lucien Ballard
| editing        = Sherman Todd
| distributor    = RKO Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
|}}
Berlin Express is a 1948 American drama film directed by Jacques Tourneur and starring Robert Ryan, Merle Oberon and Paul Lukas.

Thrown together by chance, a group of people search a city for a kidnapped peace activist. Set in Allied-occupied Germany, it was shot on location in post-World War II Frankfurt|Frankfurt-am-Main (with exterior and interior shots of the IG Farben Building and its paternoster elevators ) and Berlin. During the opening credits, a full-screen notice reads, "Actual scenes in Frankfurt and Berlin were photographed by authorisation of the United States Army of Occupation, the British Army of Occupation, the Soviet Army of Occupation."

==Plot==
Dr. Bernhardt tries to become better acquainted with the passengers of Allied nationalities who have boarded a train, they all rebuff his overtures because he is a German. When he retires to his compartment, he is killed by a bomb. While the others are questioned at the next stop, Frankfurt, they learn that the dead man was actually one of the doctors bodyguards. Bernhardt had been posing as another passenger, and Lucienne is his secretary.

Bernhardts enemies are not foiled for long. He is kidnapped from the busy train station in broad daylight after he greets Walther, an old, trusted friend. The U.S. Army quickly institutes a search of the city, but when Lucienne begs her fellow travelers to help look for Bernhardt (as they know what he looks like), they at first all decline. One by one, however, they change their minds.

Lucienne suggests they go see Walther, unaware that he has betrayed Bernhardt in return for his missing wifes location. When they get there, they discover only Walthers body. He hanged himself after the kidnappers revealed his wife has been dead all along.

The group then splits up to cover the city, with Lindley accompanying Lucienne to various illegal nightclubs. At the last one, Lindley notices a woman smoking an unusually long cigarette, just like the ones Bernhardt likes. He picks up a discarded butt and shows Lucienne that it has a "B" monogram on it. When the woman turns out to be an entertainer, pretending to know the answers of questions posed by the customers, Lindley asks her where Bernhardt is. Her clown assistant impedes Lindley, allowing her to get away. When Lindley and Lucienne question Sergeant Barnes, the American soldier who was sitting with the woman beforehand, he reluctantly agrees to lead them to where she lives.

It is a trap, however. When they get to an abandoned brewery, Barnes turns out to be working with the kidnappers. Now all three are prisoners. Fortunately, an undercover agent had knocked out the clown and taken his place, accompanying the others to the hideout. He is shot when the real clown shows up, but manages to get back to the nightclub and inform the authorities where Bernhardt is being held. American soldiers break in just as Bernhardt and Lucienne are about to be shot, and free the three unharmed. Kessler, the ringleader, is killed by Perrot, who turns out to be Bernhardts would-be assassin.

The passengers reboard the train. Perrot suggests that each of them take a turn guarding Bernhardt in his compartment, with him going first. Afterward, Lindley pieces together various lies Perrot had told and recalls that he knew that the bomb was made from a grenade, but the others dismiss his suspicions. Luckily, he sees Perrot strangling Bernhardt in the reflection from a passing train and saves the doctors life. Perrot is shot dead as he tries to flee.

==Cast==
* Merle Oberon as Lucienne. Oberon was married to the film’s cinematographer, Lucien Ballard, at the time.
* Robert Ryan as Robert Lindley
* Charles Korvin as Perrot
* Paul Lukas as Dr. Bernhardt
* Robert Coote as Sterling
* Reinhold Schünzel as Walther
* Roman Toporow as Lt. Maxim
* Peter von Zerneck as Hans Schmidt
* Otto Waldis as Kessler
* Fritz Kortner as Franzen
* Michael Harvey as Sgt. Barnes Tom Keene as Major
* Charles McGraw as USFET Col. Johns
* Marle Hayden as Maja the Mind Reader

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave the film a positive review, and wrote, "Most striking feature of this production is its extraordinary background of war-ravaged Germany. With a documentary eye, this film etches a powerfully grim picture of life amidst the shambles. It makes awesome and exciting cinema...Ryan establishes himself as a first-rate actor in this film, demonstrating conclusively that his brilliant performance in Crossfire (film)|Crossfire was no one-shot affair." Variety, however, did criticize the screenplay for "its failure to break away from the formula of anti-Nazi films." 

The New York Times had a similar response, stating the films photography of the post-war landscape creates a "realistic, awesome and impressive vista." After luke-warm praise for the films plot, the reviewer continues, "...it is the panoramic and close views of life amid the new architecture of Frankfort and Berlin — early Twentieth Century modern warfare architecture — which gives the adventure the authentic impact of a documentary." 

The scenes of the interior of the RTO (Rail Transportation Office) Army Duty Train to Berlin in this film look essentially the same as the train looked in the mid-1970s, which reinforces the evaluation of the New York Times review that Berlin Express had "the authentic impact of a documentary." 

===Awards===
Nominations
* Writers Guild of America, USA: WGA Award for the Screen, Best Written American Drama, Harold Medford; 1949.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 