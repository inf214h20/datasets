Iron Man (1931 film)
 
{{Infobox film
| name           = Iron Man
| image          = Ironman1931.jpg
| caption        = Theatrical release poster
| director       = Tod Browning
| screenplay     = Francis Edward Faragoh
| producer       = E. M. Asher Tod Browning Carl Laemmle Jr.
| based on       =   Robert Armstrong Jean Harlow
| music          = Bernie Grossman
| cinematography = Percy Hilburn
| editing        = Milton Carruth
| distributor    = Universal Studios
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Robert Armstrong and Jean Harlow.   
 the film Jeff Chandler, Evelyn Keyes and Rock Hudson, directed by Joseph Pevney.

==Plot==
After lightweight prizefighter Kid Mason (Ayres) loses his opening fight, golddigging wife Rose (Harlow) leaves him for Hollywood. Without her around, Mason trains seriously and starts winning. Naturally, Rose returns and worms her way back into his life, despite the misgivings of manager George Regan (Armstrong). Eventually, she cons Mason into dumping Regan and replacing him with her secret lover Lewis (Miljan), even though he has almost no experience in the fight game. To make matters worse, Masons high living and neglect of his training threatens his latest title defense.

==Cast==
* Lew Ayres as Kid Mason Robert Armstrong as George Regan
* Jean Harlow as Rose Mason
* John Miljan as Paul H. Lewis Edward Dillon as Jeff
* Mike Donlin as McNeil
* Morrie Cohan as Rattler OKeefe
* Mary Doran as Showgirl
* Mildred Van Dorn as Gladys DeVere
* Ned Sparks as Riley
* Sammy Blum as Mandel

==References==
===Notes===
 

===Bibliography===
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 