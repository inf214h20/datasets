Les Bricoleurs
{{Infobox film
| name           = Les bricoleurs
| image          = 
| image_size     = 
| caption        = 
| director       = Jean Girault
| producer       = 
| writer         = Jean Girault   Jacques Vilfrid
| narrator       = 
| starring       =  Francis Blance Darry Crowl  Elke Sommer
| distributor    = Félix Films
| released       = 1 January 1963
| runtime        = 113 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French thriller film directed by Jean Girault and starring Francis Blanche, Darry Cowl, Elke Sommer and Jacqueline Maillan.  Two estate agents discover a body in a house they are about to sell to a customer.  It was released as Who Stole the Body? in the United States.

== Cast ==

*Francis Blanche : Edouard 
*Darry Cowl : Félix
*Jacqueline Maillan : Langlaise 
*Clément Harari : Le professeur Hippolyte
*Elke Sommer : Brigitte 
*Daniel Ceccaldi : La Banque Hubert
*Claudine Coster : Ingrid  Mario David : Georges, lami de Monica
*Valérie Lagrange : Monica 
*Rolande Ségur : Princess Elisabeth
*Bernard Dhéran : inspector de lauto-école 
*Daniel Emilfork  : Igor, le domestique du professeur Hippolyte
*Serge Marquand : Le chasseur du professeur 
*Paul Mercey  : Le curé 
*André Badin : Ludovic, le chauffeur de lAnglaise 
*Yves Barsacq  : Le maître dhôtel de La Banque
*Roger Carel : Le comte de La Bigle 
*Philippe Castelli : Le facteur 
*Marcel Pérès : Le garde-chasse 
*Jacques Seiler  : Le majordome du comte
*Jean Tissier : Le professeur Gédéon Depois-Demesure
*Gisèle Sandre : La dernière victime
*France Anglade : Une jeune femme passant son permis
*Christian Méry : Un homme passant son permis
*Ski Hy-Lee : Le voleur de la banque

==References==
 

 
 
 
 
 
 


 
 