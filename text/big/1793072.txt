Dil Se..
 
 
 
{{Infobox film
| name           = Dil Se
| image          = Dil Se DVD cover.jpg
| image_size     =
| caption        = Poster
| director       = Mani Ratnam
| producer       = Bharat Shah Mani Ratnam Ram Gopal Varma  Shekhar Kapur G. Srinivasan Sujatha
| screenplay     =  Mani Ratnam
| story          =  Mani Ratnam
| starring       = Shahrukh Khan Manisha Koirala Preity Zinta Sanjai Mishra
| music          = A. R. Rahman
| cinematography = Santosh Sivan
| editing        = Suresh Urs
| studio         = Madras Talkies Varma Corporation
| distributor    = Madras Talkies Eros International
| released       = 21 August 1998
| runtime        = 158 mins
| country        = India Hindi
| budget         =     
| gross          =      (All India Gross)
| preceded_by    =
| followed_by    =
| website        =
}} Netpac Award at the 1999 Berlin International Film Festival. {{cite web |url= http://www.cinescene.com/names/maniratnam.html
|title= FROM THE HEART - The Films of Mani Ratnam
|accessdate= 2011-04-04 |author= Pat Padua |publisher= cinescene.com}} 

Noted for its non linear screenplay, Dil Se was shot in Himachal, Kashmir, Assam, Delhi, Kerala, and other parts of India and Bhutan over a period of 55 days. The highly stylized film won awards for cinematography, audiography, choreography, and music, among others. The film was a success overseas earning $975,000 (USA) and £537,930 in the UK,  becoming the first Indian film to enter the top 10 in the United Kingdom box office charts. National Film Awards, and six Filmfare Awards. {{cite journal
 | last = Aftab
 | first = Kaleem
 |date=October 2002
 | title = Brown: the new black! Bollywood in Britain
 | journal = Critical Quarterly
 | volume = 44
 | issue = 3
 | pages = 88–98
 | publisher = Blackwell Synergy
 | doi = 10.1111/1467-8705.00435 NRI audience in the commercial fate of Bollywood produce.
 }}  {{cite web
|url= http://www.bfi.org.uk/sightandsound/review/5
|title= Dil Se..
|accessdate= 2008-02-16
|author= Cary Rajinder Sawhney
|year= 2006
|publisher= British Film Institute
}} 

==Plot==
Amar Kant Varma (Shahrukh Khan) is a Program Executive for All India Radio. He is dispatched from New Delhi to cover festivities in the northeastern parts of India, and Kashmir.  Baraks Valley Express at Haflong station. The train is running 8hrs late, and he comes across a beautiful woman (Manisha Koirala) and is instantly attracted to the stranger. He tries to talk to her at the railway station, but she does not seem to respond, however when he asks if he can get anything for her she says she would like a cup of tea. As Amar goes to get her tea, her train arrives and she boards with three men.

Later, Amar spots the same woman in Silchar. He attempts to talk to her but she says she cannot recall meeting him before. As part of his news reporting assignment, Amar interviews many citizens of Barak Valley, and an extremist leader, who claim that the reason behind human rights violations, and poverty in the region is due to Indian Government, and that the terrorists do not wish to enter into any dialogue with the government, and further justify their Insurgency attacks and resistance in the region.

A few weeks later, Amar describes his encounter with the woman live at his program recording session over the Radio. Later, he again spots her at a post office (It is later revealed that she is corresponding to her terrorist group in New Delhi, and are planning a suicide attack). At this juncture, she tells him to leave her alone. However he follows her to the house and tells her that he is in love with her, but she resists and tells Amar that she is married. Amar feels embarrassed and wishes to apologise to her, however she arrives with two men (who Amar believes are her husbands) who take Amar away and beat him unconscious.

During the beating, Amar learns that the men are presumably her brothers and that, the woman had lied about her being married. This motivates him to pursue the woman – he then goes to the post office where he initially spotted her and bribes the PCO owner into giving him her contact details. He reaches her home, and learns from the locals, that she is a Kashmiri, and is visiting Ladakh. Amar follows the woman, and spots the woman at a local Festival, Amar claims that he is here as part of his assignment, reporting on the festivals. While shooting at the Festival, a suicide bomber is chased to death by the military. (It is later revealed that the suicide bomber, is part of the terrorist group, and that the woman is associated with him). At this juncture, the woman boards the bus and uses Amar to her advantage, purporting in front of the military personnel that her name is Meghna and that she is with her husband, the radio reporter.

The bus takes off. After reaching a terrain, the bus breaks down and the passengers are stranded to walk to the nearby village. Amar angrily confronts Meghna for having him beaten up with her family – Amar eventually forces himself onto Meghna, causing her to have an anxiety attack. (It is later revealed that she actually suffers from Rape trauma syndrome). The two end up travelling together and recuperate. However, Next day, Amar wakes up only to find Meghna having left. (It is later revealed that Meghna is part of a Kashmir Liberation extremist group which plans a suicide attack in New Delhi at the upcoming Republic Day celebration).

Amar travels back to his home in Delhi. He learns to his surprise that his family has found in Preeti Nair (Preity Zinta) a potential bride for him. Amar agrees to marry Preeti because he does not hope to meet Meghna again.  Connaught Place, however by the time local cops interfere, the man kills himself with cyanide. (It is later revealed that the man is also associated with Meghnas terrorist group). 
 CBI inquiry operation). Based on eyewitness claims of the Connaught Place incident, Amar is now a prime suspect of the CBI (Piyush Mishra). At this juncture Amar follows Meghna and questions her motives, and she reveals to Amar, that, as a child, she had been a rape victim in the Kunan Poshpora incident and that her soul seeks liberation through her suicide attack on the Indian army and the President of India during the Republic Day. Now the CBI convinces the Army general of India to grant permission to conduct security checks of all the Army convoys and tankers participating in the parade.

Amar is again assaulted by Meghnas brother (Aditya Srivastava) and the terrorists and as Amar fights back the terrorists receive a call from Meghna on their mobile. Amar grabs the mobile and pleads Meghna to stop all this and marry him. Meghna reveals that it is too late, and presumes Amar is being killed. But Amar returns home, only to find out from Preeti that Amars mom is also being questioned and that Meghnas location is at Sunder Nagar. The CBI also misconstrue that Amar is part of the terrorist group and the investigators arrest Amar at his residence.

Amar claims to the CBI that he is not in cahoots with the terrorists, but in love with Meghna and that he has interviewed one of the extremist leaders and wants to prevent them from perpetrating the attack. The CBI rejects Amars claims and sedates him for further interrogation. The next day Meghna is ready for the suicide attack. Amar escapes from the CBI and tries to hold Meghna back. Amar expresses his love and desire to be with her, embraces her and pleads her to live with him. Meghna now realises how deeply she is in love with Amar. As they embrace the bomb explodes, and they both die.

==Cast==
* Shahrukh Khan as Amarkant "Amar" Varma
* Manisha Koirala as Meghna
* Preity Zinta as Preeti Nair
* Raghuvir Yadav as Shukla
* Zohra Sehgal as Grandmother
* Aditya Srivastava as Meghnas brother
* Arundathi Nag as AIR Station Chief
* Sabyasachi Chakrabarty Group member
* Piyush Mishra as CBI investigation officer
* Mita Vasisht as Mita
* Malaika Arora in the item number "Chaiyya Chaiyya"
* Sanjai Mishra as Group Member
* Gajraj Rao as CBI investigation officer
* Priya Parulekar as Young Meghna
* Tigmanshu Dhulia cameo

==Production==
South Indian actress Simran Bagga was Mani Rathnams first choice for the role that Preity Zinta eventually accepted. 

==Themes==
Dil Se is said to be a journey through the 7 shades of love that are defined in ancient Arabic literature. Those shades are defined as attraction, infatuation, love, reverence, worship, obsession, and death. The character played by Shahrukh Khan passes through each shade during the course of the film. 

The film is a dramatisation of the attraction between a character from the heart of India and another from a peripheral state and a representation of opposites in the eyes of the law and society.    Dil Se is described as a film "structured through deferment and unfulfilled teasing promises."
    Rediff.com said about the film, "The entire feel of the film is appropriately poetic, with a few romantic exchanges standing out quite memorably. Tigmanshu Dhulia has handled the films dialogues adroitly. Amid moonlit desert dunes, there is a particularly stirring conversation between the leading pair. Amar reveals his love for Meghnas eyes -- because he cant see the world hidden behind them, and his hate for the same, stunning eyes -- because he cant see the world hidden behind them."   

Elleke Boehmer and Stephen Morton in their book Terror and the postcolonial (2009) believe that the songs and their exotic locations in the film were very important in masking the impossible reconciliation between a terrorist and an uptight government agent by evoking pure fantasy.  They argue that this is a phenomenon called the "liminal space of dreaming" in that the terrorist woman cannot fulfill her sexual desire so the songs fill the void of this desire by "their sumptuousness and exotic locales" in the Ladakh region. 

==Release and reception== Netpac Award National Film Awards, and six Filmfare Awards. West London. 

Deepa Deosthalee wrote a positive review to the film, calling it "A picture perfect ode to love" and praising the direction, writing and performances. 

==Awards==
The film has won the following awards:

1999 Berlin International Film Festival (Germany) Netpac Award - Special Mention - Mani Ratnam
 National Film Awards (India) Cinematography - Santosh Sivan Best Audiography - H. Sridhar

1999 Filmfare Awards (India) Best Female Debut - Preity Zinta Best Music Director - A. R. Rahman Best Lyricist Gulzar for "Chaiyya Chaiyya" Best Male Playback - Sukhwinder Singh for "Chaiyya Chaiyya" Best Cinematographer - Santosh Sivan Best Choreography - Farah Khan for "Chaiyya Chaiyya"

1999 Star Screen Awards (India) Best Male Playback - Sukhwinder Singh for "Chaiyya Chaiyya"

==Soundtrack==
{{Infobox album
| Name = Dil Se
| Type = Soundtrack
| Artist = A. R. Rahman
| Cover = Dilsecover.jpg
| Background = Gainsboro
| Released = 1998
| Recorded = Panchathan Record Inn
| Genre    = World Music
| Length   =
| Label    = Venus
| Producer = A.R. Rahman
| Last album = Jeans (soundtrack)|Jeans (1998)
| This album = Dil Se (1998)
| Next album = Earth (1998 film)|Earth (1998)
}}

{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =   
| rev2 = All Music
| rev2Score =   
}}

The soundtrack features 6 songs composed by  .  This soundtrack is described as a landmark album in Indian music, with each and every song becoming colossal hits.

The soundtrack was recorded in several other languages. The Tamil version of the track "Chaiyya Chaiyya", entitled "Thaiyya Thaiyya", was sung by Palghat Sriram, although Sukhwinder Singh, who sang the Hindi version was credited as the singer.  Malayalam lyrics for the song "Jiya Jale" were penned by Gireesh Puthenchery while the Punjabi part of "Thayya Thayya" was penned by Tejpaul Kour. 
 Pulse played bass on title song Dil Se Re. 

The background score was also very much appreciated and said to have contributed largely to the film.

===Hindi (Dil Se)===
{{tracklist
| all_music = A. R. Rahman Gulzar
| extra_column = Singer(s)
| title1 = Chaiyya Chaiyya
| extra1 = Sukhwinder Singh & Sapna Awasthi
| length1 = 6:54
| title2 = Jiya Jale
| extra2 = Lata Mangeshkar, M. G. Sreekumar & Chorus
| length2 = 5:07
| title3 = Dil Se Re Anupama & Febi Mani
| length3 = 6:44
| title4 = E Ajnabi
| extra4 = Udit Narayan & Mahalakshmi Iyer
| length4 = 5:48
| title5 = Thayya Thayya (Remix)
| extra5 = Sukhwinder Singh
| length5 = 4:35
| title6 = Satrangi Re
| extra6 = Sonu Nigam & Kavita Krishnamurthy
| length6 = 7:25
}}

===Tamil (Uyire)===
{{tracklist
| all_lyrics = Vairamuthu
| all_music = A. R. Rahman
| extra_column = Singer(s)
| title1 = Thaiyya Thaiyya
| extra1 = Sukhwinder Singh & Malgudi Subha
| length1 = 6:55
| title2 = Nenjinile Nenjinile
| extra2 = S. Janaki, M. G. Sreekumar & Chorus
| length2 = 5:09
| title3 = Sandhosha Kanneere
| extra3 = A. R. Rahman, Sowmya Raoh, Dominique Cerejo & Kavita Paudwal
| length3 = 6:42
| title4 = Poongkaatrilae
| extra4 = Unni Menon & Swarnalatha
| length4 = 5:45
| title5 = Thayya Thayya (Remix)
| extra5 = Hariharan
| length5 = 4:19
| title6 = En Uyire Srinivas & Sujatha Mohan
| length6 = 7:26
}}

===Telugu (Premato)===
{{tracklist Sitarama Sastry
| all_music = A. R. Rahman
| extra_column = Singer(s)
| title1 = Thaiyya Thaiyya
| extra1 = Sukhwinder Singh & Malgudi Subha
| length1 = 6:52
| title2 = Innaalilaa Ledule
| extra2 = K. S. Chithra, M. G. Sreekumar & Chorus
| length2 = 5:06
| title3 = Ninnele
| extra3 = A.R. Rahman, Anuradha Sriram, Anupama & Febi Mani
| length3 = 6:37
| title4 = O Priyatama Mano & Swarnalatha
| length4 = 7:25
| title5 = Chaiyya Chaiyya (Remix)
| extra5 = Sukhwinder Singh
| length5 = 4:17
| title6 = Ooristhu Ooguthu
| extra6 = Srinivas & Sujatha Mohan
| length6 = 5:42
}}

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 