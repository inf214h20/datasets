Penny of Top Hill Trail
 
{{Infobox film
| name           = Penny of Top Hill Trail
| image          =
| alt            =
| caption        =
| director       = Arthur Berthelet
| producer       = Andrew J. Callaghan 
| writer         = 
| screenplay     = {{Plain list|*Finis Fox
*Beatrice Van }}
| story          =
| based on       =  
| starring       = Bessie Love
| cinematography = Sam Landers   
| editing        =
| studio         = Andrew J. Callaghan Productions
| distributor    = Federated Film Exchanges of America, Inc. 
| released       =   reels   
| country        = Silent (English intertitles)
| budget         = $80,000 
| gross          =
}} silent 1921 Western comedy-drama  film based on the 1919 novel by Belle Kanaris Maniates.   It was directed by Arthur Berthelet  and stars Bessie Love. 

==Plot==
When Penny (Love) goes to a ranch, she is mistaken for a thief. She encounters the ranch foreman (Oakman), who tries to reform her. When another girl is revealed to be the real thief, Pennys reputation is cleared, and she reveals her true identity: a film actress on vacation. She and the foreman realize their love for each other, and Penny decides to stay on the ranch with him. 

==Cast==
* Bessie Love as Penny   
* Wheeler Oakman as Kurt Walters Raymond Cannon as Jo Gary
* Harry De Vere as Louis Kingdon
* Lizette Thorne as Mrs. Kingdon
* Gloria Holt as Betty Kingdon
* George Stone as Francis Kingdon
* Herbert Hertier as Hebier

==Production==
Exteriors were filmed in Tucson, Arizona. 

==Release==
Upon its release, some theaters showed the film with The Hope Diamond Mystery and a Ham and Budd comedy. 

==Reception==
Overall, the film received positive reviews  and was successful a the box office.  The wardrobe, atypical for Western films,  and Loves frequent hairstyle changes distracted some viewers from the plot. 

==References==
 

==External links==
* 
* 
* 
* 


 
 
 
 
 