Light Drops
{{Infobox film
| name           = Light Drops
| image          = Lightdrops.gif
| image size     = 55 px
| border         = 
| alt            = 
| caption        = 
| director       = Fernando Vendrell
| producers       = Ana Costa and Fernando Costa
| writer         = Ian Swann
| screenplay     = Fernando Vendrell
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Nuno Canavarro and Luís Represas
| cinematography = Mário Masini
| editing        = José Nascimento
| studio         = Cinemate
| distributor    = Marfilmes, Lusomundo (Mozambique) and AMC (Portugal) 2002 
| runtime        = 100 mins
| country        = Portugal/Mozambique
| language       = Portuguese
| budget         = 
| gross          = 
}}

Light Drops (2002), original title O Gotejar da Luz, is a Portuguese/Mozambican film by Fernando Vendrell. 

Vendrells film is set in the colonial Mozambique of the 1950s and tells the story of teenage protagonist Rui Pedros growing awareness of the injustices of colonial Mozambique.

The story is set in the settlement of Bué Maria on the Pungwe River, in the central Mozambican province of Sofala.

==Distribution history==

The film premiered on 8 February in the Xénon cinema in Maputo and on 15 February in the São Jorge and Quarteto cinemas of Lisbon.

Vendrells film is based on the short story “O Lento Gotejar da Luz” by  , a journalist and writer who grew up in Mozambique but left the country for political reasons

==Synopsis==

Rui Pedro is a fifty something man who returns to the ruins of the cotton plantation where he grew up in colonial Mozambique. In the almost silent opening to the film, Rui Pedro drives from one of the big cities of Mozambique, perhaps Beira, Mozambique|Beira, out to Bué Maria, where he spent his childhood and adolescence. There he finds the ruins of a house, which we later realize is the house in which he and his family once lived. Rui Pedro pitches camp by the river, where he encounters a young shepherd boy. Though the boy cannot speak Portuguese, Rui Pedro shares food and a cigarette with him. This meeting between the older white mana the young black boy foreshadows the relationship between Rui Pedro and Jacopo, the ferryman from his childhood.  His visit recalls memories of his adolescence, and in particular a set of incidents that took place when he was fourteen and home from school for the summer break from school, dramatic incidence which would mark his life. 

The son of Portuguese colonists living amongst a small community of whites at a remote outpost in the bus, Rui Pedro has grown up shuttling between two ways of life, the European mores of his family and the African traditions of the workers his father manages. His life has wavered between the two banks of the nearby Pungue river, between the culture of the white settlers and that of the black natives, between the boss and the slaves, between violence and peace, between love and passion. 

After the Second World War, at a time when the other European powers were reluctantly making ready to decolonize, Portugal stepped up its involvement in its African territories. In Light Drops, as the colonial cotton company Rui Pedros father works for forces through a bumper cotton harvest, much to the detriment of the local population. This exploitation at a general level is echoed in the relationship between Rui Pedros cousin and his familys maid Ana, who is betrothed to Guinda, a local mechanic and froe. The violent end to their relationship foreshadows the violence to come in Mozambique.

==Main characters==
===Rui Pedro===

Played by Luís Sarmento (Rui Pedro at 50) and Filipe Carvalho (Rui Pedro at 14), the protagonst finds his adolescent questions about identity, society, morality and sexuality played out against the ideology of a repressive colonial society.

===Jacopo===

Played by Amaral Matos, Jacopo is Rui Pedros black father, who teaches him to respect nature and African culture but also gives the young boy his first experience of mercilessness. Jacopo is a ferryman, who mediates between the two banks of the river professionally and symbolically for Rui Pedro.

===Ana===

Played by Alexandra Antunes, Ana is Rui Pedros familys maid, practically a sister to Rui Pedro and his mothers godchild. She is an assimilada, caught between European values she has been taught to adopt and African traditions which her community still expects her to obey. Her torn loyalties come to the fore in her relationship with Guinda and her attraction for Rui Pedros cousin, X.

===Guinda===
 
Played by Alberto Magassela, Guinda is a local mechanic. He aspires to become an assimilado, yet is careful to follow traditional protocol in his engagement to Ana.

===Carlos===

Played by  Marco dAlmeida, Carlos is Rui Pedros older cousin. He is studying in South Africa, and has returned to Mozambique for his summer holidays with stiff racist attitudes which contrast with the relative racial tolerance of his family. On the cusp of adulthood, he is caught between the attitudes he has learnt in South Africa and his attraction for Ana.

===Dona Alice===

Played by Teresa Madruga, Dona Alice is Rui Pedros mother. She fusses over her son, treats Ana almost like a surrogate daughter and expresses objections to colonialism (without, for all that, fundamentally challenging any of its suppositions.

===César===

Played by António Fonseca, Rui Pedros father is an ambiguous figure, caught between care and callousness, his awareness that colonialism is drawing to a close and his desire to make a success of his plantation, as per the orders of his colonial superiors.

=== Andrade e Castro===

Played by Carlos Gomes, Castro is a colonial administrator who visits the plantation.

===Isaura===

Played by Carla Bolito, another goddaughter of Rui Pedros mother, Isaura is trapped in a loveless marriage with Barroso

===Barroso===

Played by Vitor Norte, Barroso is a trader from rural Portugal. Brutish towards his wife, he aims to make the most amount of profit possible in his store, even if this means cheating his African customers.

===Fombe===

Played by Marenguele Mawhayi 

Other actors involved include Ana Magaia and Alfredo Ernesto

==Festival appearances==

* 30ª Mostra Internacional de Cinema de S. Paulo (2001)
*   (2002)
*   (Nantes, França - 2002)
*   (2002)
*   (Greece - 2002)
*   (Amiens - 2002)
* Commomwealth Film Festival, Manchester 6 - 25 Jun  2003
*   de São Paulo (2004)
*   (2006)

==External links==
*   at the Berlin Film Festival.
*   in Berlin.
*    in the newspaper O Público (in Portuguese)
*   in the newspaper Diário Digital (in Portuguese)
*   in the newspaper Correio da Manhã (in Portuguese)
*   in the newspaper Diário de Notícias (in Portuguese)
*   on IMDb (in English)

 
 