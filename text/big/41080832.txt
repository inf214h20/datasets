Lo más sublime
{{Infobox film
| name = Lo más sublime
| image =
| border =
| alt =
| caption =
| director = Enric Ponsà
| producer = Llorenç Arché Codina Josep Mª Casals Tarragó
| writer = Enric Ponsà
| screenplay = Enric Ponsà
| story =
| based on =
| starring = Antonio B. de Vila, Mercedes Domènech, Rosita Ponsa, Lina Vivarelli, Antonio Granell, Valeriano Cortés, Jaime Reixach, Julio Payá, Eduard de C. Tarragó, Enrique Filló and girl Nuri
| cinematography = Antonio Burgos
| editing =
| studio = Produccions E.L.A.
| distributor =
| released =  
| runtime = 60 minutes, 1614 meters
| country = Catalonia Spain Spanish intertitles
| budget =
| gross =
}}
 Catalan unreleased drama was directed by Enrique Ponsá and protagonized by a group of young entrepreneurs who rolled the film in four months during the weekends in different places of Costa Brava, mainly Blanes.
  
     

== Plot==
In San Cebrian, a coastal village, Rosa with her daughter Nuri and her godson Sardinilla, hoped for the return of her fisherman husband Juan, but they found him dead after storm. Friends of the family, Antonio and Rosita, with Sardinilla decide to look for a treasure with the help of a map that kept Rosita. His enemies, Tomas and Andres, try to take it by deception. Andres, plus interest in Rosita, the beloved of Antonio, is a victim of Tomás greeds, but when he is wounded, Antonio and Sardinilla return him to the good track.

== Cast==
{| class="wikitable sortable"
|- Character !!Actor !! Pseudonym
|-
| Antonio     || Antonio B. De Vila || Antonio Burgos
|-
| Rosa || Mercedes Doménech ||
|-
| Ana Maria|| Rosita Ponsá||
|-
| Clara || Lina Vivarelli ||
|-
| Andrés, the traitor|| Antonio Granell ||
|-
| Carlos, the Sardinilla|| Valeriano Cortés ||
|-
| Juan || Jaime Reixach || Jaume Ponsá Doménech
|-
| Gaspar || Julio Paya ||
|-
| Ibo, the schooners captain|| Eduard de C. Tarragó || Josep Maria Casals Tarragó
|-
| Lucas || Enrique Filló ||
|-
| young girl Nuri || Nuria Burgos ||
|}

== About the film ==
The film was made by ELA Productions, a newly company and unique film.   LLorenç Arché Codina was one of the promoters as a film producer|producer, with Enrique Ponsa as a film director|director, Antonio Burgos as a decorator  and Josep Maria Casals Tarragó as an executive producer and actor. 

 


It was filmed in the twenties when the landscape of the Costa Brava became interested for the filmmakers. Along with Lo más sublime, were also filmed at the same time: Lilian (1921), Entre Marinos (1920), El místic (1926), Baixant de la font del gat (1927), etc.  The exteriors were shot in Blanes and the coast between Blanes, Lloret de Mar and Tossa de Mar. The beginning of the film gives an overview of San Cebrian taken from the big rock Sa Palomera in Blanes, considered the beginning of the Costa Brava. 

On 20 September 1927 it was made a projection for the criticism  that advised to cut intertitles, delete some others and eliminate some scenes of the film before presenting it to the public.   With these changes it was foreseen a great success in Spain and good aspirations abroad.  Lo más sublime was released on Teatre Novedades (Barcelona) on July 1928. The price of entry was 0.75 chair and general 0.40 cents of peseta. 
In 1992, a copy of the film, preserved in the original nitrocellulose support by childrens executive producer, Albert & Josep-Maria Casals Martorell, was recovered  by the film archive of Catalonia (Filmoteca de Catalunya) and transferred to a support that could be designed with safety guarantees. In January 1994 Lo más sublime was revived  at  Cine Alcazar in Barcelona, with piano accompaniment by Joan Pineda, coinciding with the opening of the publicity material between 1925 and 1936 exhibition about the Catalan film distributor Mundial Film. 

 
 Advertising on Popular Film, July 21, 1927


 ELA Productions (Barcelona)


 

== See also ==
 
* List of Costa Brava films
 

== References ==
 

== Bibliography ==
There are two original research about the film in the archives of Blanes (Barcelona) city:
*  
*  

== External links ==
 
*   Film archive deposit of Catalonia
*  
*  

 
 
 
 
 
 