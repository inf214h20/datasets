Pengejar Angin
 
 
{{Infobox film
| name           = Pengejar Angin
| image          = Pengejar angin.jpg
| caption        = Film Poster
| director       = Hanung Bramantyo
| producer       = Dhoni Ramadhan
| writer         = Ben Sihombing
| starring       = {{plainlist|
*Qausar Harta Yudana
*Mathias Muchus
*Lukman Sardi
*Girogino Abraham
}}
| music          =
| cinematography = Faozan Rizal
| editing        = Wawan I Wibowo
| distributor    = {{plainlist|
*Putaar Production
*Government of South Sumatra
}}
| released       =  
| runtime        = 101 minutes
| country        = Indonesia
| language       = Indonesian
| budget         =
| gross          =
}} 2011 SEA Games. Funded in part by the government of South Sumatra, where the Games were held, the film raised criticism for its use as an advertisement.

==Plot==
In a small village in Lahat Regency|Lahat, South Sumatra, an 18-year-old boy named Dapunta (Qausar Harta Yudana) is almost ready to graduate from senior high school; he is known as the Wind Chaser locally because of his running capabilities. He and his mother Dakunta (Wanda Hamidah) want him to go to university, but his father&nbsp;– the leader of a gang of bandits&nbsp;– refuses to allow it. Dapunta decides to go to university no matter what.
 crush Nyimas (Siti Helda) and teacher Damar (Lukman Sardi) help him, motivating him to practice his running so that he can use athletics to enter the university of his choice. However, his classmate Yusuf (Giorgino Abraham)&nbsp;– who has similar running abilities&nbsp;– is also training; he sabotages some of Dapuntas practice sessions. Meanwhile, the headmaster attempts to limit Dapuntas training out of spite for his father.

A month later, Damars friend Ferdy (Agus Kuncoro), a trainer, comes to Lahat looking for talent to participate in the upcoming Southeast Asian Games (SEA Games). He takes on Dapunta and further trains him. After Dapunta is able to represent his country at the SEA Games in Palembang, his father relents and allows Dapunta to follow his dreams.

==Production== extras were cast on-location. 
 2011 SEA Games with the national capital Jakarta. Although Bramantyo, who generally received funding from the production house behind his films,  considered the funding a blessing, he noted that several changes had to be made to the plot, including adding an emphasis on the provincial governments work towards the SEA Games (such as the Gelora Sriwijaya Stadium) and education and health-care programs. The Governor of South Sumatra, Alex Noerdin, also appeared in several scenes. The films producer, Ramdhoni Ramadhan, described Pengejar Angin as a pilot project for "raising public awareness" of government programs in the province. 

==Release and reception==
Pengejar Angin premiered at Gandaria City in South Jakarta on 31 October 2012,  with a wide release on 3 November.  It did poorly in mall-based cinemas in major cities, but was warmly received in smaller cities.  In June 2012, Noerdin announced that the government had set aside a portion of its yearly budget for further films promoting the region. 

The film received criticism over its government funding and was called "politically biased" and "glaringly commercial" for its promotion of the SEA Games.  Bramantyo, commenting after the premiere, noted that he knew he would be suspected of selling out.  Lisa Siregar, writing for The Jakarta Globe, wrote that the film "discuss  government health and education services with the conviction of a soap commercial".  Panditio Rayendra, writing in the entertainment magazine Tabloid Bintang, found the poster reminiscent of an action or horror movie but the film surprisingly optimistic; he considered Muchus to have stolen the show with his acting. 

==Awards==
Pengejar Angin was nominated for three Citra Awards at the 2011 Indonesian Film Festival, winning one;  with ? (film)|? and Tendangan dari Langit (A Kick from the Sky), it was one of three films by Bramantyo in competition at the festival.  At the 2012 Bandung Film Festival the film received nine nominations, winning two. 

{| class="wikitable" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="3" | Indonesian Film Festival
| rowspan="3" | 2011
| Best Supporting Actor
| Mathias Muchus
|  
|-
| Best Cinematography
| Faozan Rizal
|  
|-
| Best Editing
| Wawan I Wibowo
|  
|-
! scope="row" rowspan="2" | Bandung Film Festival
| rowspan="2" | 2012
| Best Actor
| Qausar Harta Yudana
|  
|-
| Best Screenwriter
| Ben Sihombing
|  
|}

==References==
;Footnotes
 

;Bibliography
 
* {{cite news
  | last1 = Fikri
  | first1 = Ahmad
  | date=13 May 2012
  | title =Baru Main Film, Qausar Sukses Jadi Pemeran Terbaik
  | trans_title=First Film, Qausar Becomes Best Leading Actor
  |work=Tempo
  | url = http://www.tempo.co/read/news/2012/05/13/111403586/Baru-Main-Film-Qausar-Sukses-Jadi-Pemeran-Terbaik
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jM5FZ80
  |archivedate=27 June 2012
  }}
* {{cite news
  | last1 = Herman
  | first1 = Ami
  | date=25 June 2012
  | title =Didukung Alex Noerdin, Garap Perfilman Sumsel
  | trans_title=With Alex Nordins Support, Improving South Sumatran Film
  | work=Suara Karya
  | url = http://www.suarakarya-online.com/news.html?id=306160
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jNwuuL9
  |archivedate=27 June 2012
  }}
* {{cite news
 | work=The Jakarta Post
 | title = A vibrant year for the film industry
 | trans_title = 
 | language = 
 | url =http://www.thejakartapost.com/news/2011/12/18/a-vibrant-year-film-industry.html
 | last=Kurniasari
 | first=Triwik
 | date =18 December 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dAu5NwZ
 | ref = 
}}
* {{cite news
  | last1 = Mustholih
  | date=22 October 2011
  | title =Film Pertama dan Terakhir Wanda Hamidah
  | trans_title=Wanda Hamidahs First and Last Film
  |work=Tempo
  | url = http://www.tempo.co/read/news/2011/10/22/125362718/Film-Pertama-dan-Terakhir-Wanda-Hamidah
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jMvC7Su
  |archivedate=27 June 2012
  }}
* {{cite news
  | last1 = Mustholih
  | date=31 October 2011
  | title =Mathias Muchus Berotot dan Berwajah Seram
  | trans_title=Mathias Muchus: Muscles and a Scary Face
  |work=Tempo
  | url =http://www.tempo.co/read/news/2011/10/31/125364145/Mathias-Muchus-Berotot-dan-Berwajah-Seram
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jNMVEQh
  |archivedate=27 June 2012
  }}
*{{cite web
 |title=Penghargaan Pengejar Angin
 |trans_title=Awards for Pengejar Angin
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p013-11-434180_pengejar-angin/award#.T-rVBlLE_Mw
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=27 June 2012
 |archiveurl=http://www.webcitation.org/68jMV91Jm
 |archivedate=27 June 2012
 |ref= 
}}
* {{cite news
  | last1 = Rayendra
  | first1 = Panditio
  | date=8 November 2011
  | title =Pengejar Angin (Dapunta), Ketika Penerus Bajing Loncat Memilih Kuliah
  | trans_title=The Wind Chaser (Dapunta), when a Bandits Heir Chooses University
  |work=Tabloid Bintang
  | url = http://www.tabloidbintang.com/film-tv-musik/ulasan/17616-pengejar-angin-dapunta-ketika-penerus-bajing-loncat-memilih-kuliah.html
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jP1jYiP
  |archivedate=27 June 2012
  }}
* {{cite news
  | last1 = Sari
  | first1 = Dianing
  | date=23 April 2012
  | title =Pengejar Angin, Alasan Hanung Gerilya Bioskop Daerah
  | trans_title=Pengejar Angin, the Reason Hanung is Going for Smaller Theatres
  |work=Tempo
  | url = http://www.tempo.co/read/news/2012/04/23/219399048/Pengejar-Angin-Alasan-Hanung-Gerilya-Bioskop-Daerah
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jLlPiJa
  |archivedate=27 June 2012
  }}
* {{cite news
  | last1 = Seno
  | first1 = Budi
  | date=7 November 2011
  | title =Siap untuk Dicurigai
  | trans_title=Ready for Suspicion
  |work=Suara Karya
  | url = http://www.suarakarya-online.com/news.html?id=290511
  | accessdate =27 June 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/68jNwuuL9
  |archivedate=27 June 2012
  }}
* {{cite news
  | last1 = Siregar
  | first1 = Lisa
  | date=2 November 2011
  | title = A Film With a (Paid-for) Message
  |work=Jakarta Globe
  | url = http://www.thejakartaglobe.com/lifeandtimes/a-film-with-a-paid-for-message/475811
  | accessdate =28 November 2011
  | ref =  
  |archiveurl=http://www.webcitation.org/63WcTI7uo
  |archivedate=28 November 2011
  }}
 

==External links==
* 
 

 
 
 
 