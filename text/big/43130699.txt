Bindaas (2014 film)
{{Infobox film
| name =  Bindaas
| image = File:Bindaas 2014 Movie Theatrical Poster 2.jpg
| caption = Bindaas Original New Poster
| director = Rajiv Kumar Biswas
| story = Koratala Siva and N.K. Salil
| screenplay = N.K. Salil
| producer = Shrikanta Mohta  Shree Venkatesh Films Dev  Srabanti Chatterjee  Sayantika Banerjee
| cinematography = Kumud Verma
| editing = Raviranjan Maitra
| music = Savvy Gupta  Arindom Chatterjee  Habib Wahid
| distributor = Shree Venkatesh Films Jalsha Movies Production
| released =  
| runtime = 160 minutes
| country = India Bengali
}}
 action romance romantic Cinema Bengali film Chiranjeet Chakraborty. It is a Remake of Telugu film Mirchi (film)|Mirchi.

==Plot==
Kajol (Sayantika Banerjee) became friends with  Abhimanyu aka Abhi (Dev (actor)|Dev) after the later saves the former from some goons in Italy. But one day she asks Abhi to leave her and go as she cannot stand the separation from him if their relationship develops any further. He then goes back to India and changes the mind of Kajols brother (Surajit Sen) and when they give a vacation he goes with him to his village. There, where everyone are conservative in nature, he changes their nature and makes them more lovable.  Abhi abides by the philosophy of peace and love. He feels that one can conquer the world with love, not by war.There, he tries to wield his charm and win people over. Gradually, Kajols family begins to appreciate Abhis honesty. They decide to marry Abhi with Kajol. But Kajols uncle, Rudra, challenges Abhi to kill his rivals son, and only then would he allow Abhi to marry Kajol. Here unfolds another story. The rivals son is none other than Abhi himself, who, over a bitter incident in the family had left the village and also his childhood sweetheart Anjali.

==Cast==
  Dev as Abhimanyu aka Abhi
* Srabanti Chatterjee as Anjali
* Sayantika Banerjee as Kajol Chiranjeet Chakraborty as Dibakar Sen, Abhis father
* Kharaj Mukherjee as Krishnochandra Singha
* Indrani Sen
* Sudip Mukherjee as Abhis uncle
* Amitabh Bhattacharjee
* Raja as Rudra, Kajols elder brother
* Surajit Sen as Indra 
* Supriyo Dutta
* Sumit Ganguly as Sukhdeb
* Tomal Roy Chowdhury
 

==Making==
The shooting of the film started on 25 November 2013. It was produced by Shree Venkatesh Films and Jalsha Movies Production. The main script writer and director was Rajiv Kumar Biswas.

== Soundtrack ==
{{Infobox album  
| Name       = Bindaas
| Type       = Soundtrack
| Artist     = Savvy, Arindom and Habib Wahid
| Cover      =
| Alt        = 
| Released   =  
| Recorded   = 2014 Feature film soundtrack
| Length     =  
| Label      = Shree Venkatesh Films, Jalsha Movies Production and V Music
| Producer   = Shrikanto Mohta   Jalsha Movies Production
| Chronology = Savvy Gupta
| Last album = Ami Shudhu Cheyechi Tomay  (2014)
| This album = Bindaas  (2014)
| Next album = Yoddha - The Warrior  (2014)
| Misc       = {{Extra chronology 
 | Artist     = Arindom Chatterjee
 | Type       = Soundtrack
 | Last album = Bangla Naache Bhangra  (2013)
 | This album = Bindaas  (2014)
 | Next album = Borbaad  (2014)
 }}
| Misc       = {{Extra chronology 
 | Artist     = Habib Wahid
 | Type       = Soundtrack
 | Last album = 
 | This album = Bindaas  (2014)
 | Next album = 
 }}
}}
The soundtrack of the film was scored by Savvy Gupta, Arindom Chatterjee, Habib Wahid and Devi Sri Prasad.
{| border="5" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; backgrond: #f1f5fc; border: 1px #abd5f5 solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor=" #d0e5f5" align="center"
! Track !! Song !! Singer(s) !! Duration (min:sec) !! Music !! Lyricist
|-
| 1 || "Party Shoes" || Shadaab Hashmi and Neha Kakkar || 3:45 || Savvy Gupta || Riddhi/Savvy
|-
| 2 || "Tomake Chere Ami" ||   || S.A Olike
|-
| 3 || "Bhalobeshe Kono Bhool Korini Aami" || Arindom Chatterjee and Shalmali Kholgade || 4.33 || Arindom Chatterjee || Prosen
|-
| 4 || "Ajke Ei Khushir Dine" ||   || Priyo Chattopadhay
|-
| 5 || "Remix Qawwali" ||   || Savvy
|-
| 6 || "Bindaas Mashup" || Savvy Gupta || 4.00 || Savvy Gupta, Arindom Chatterjee and Habib Wahid || DJ Mix
|}

==References==
* http://www.nowrunning.com/movie/15842/bengali/bindaas/4764/review.htm
* http://indianexpress.com/article/entertainment/screen/bindaas-bengali-a-bold-film/
* http://in.bookmyshow.com/movies/bindaas-bengali/ET00023204
* http://timesofindia.indiatimes.com/entertainment/bengali/movie-reviews/Bindaas/movie-review/39033763.cms

 
 
 
 
 