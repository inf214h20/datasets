The Mad Parade
{{Infobox film
| name           = The Mad Parade
| image          =
| caption        =
| director       = William Beaudine
| producer       = William Beaudine
| writer         = Gertrude Orr Doris Malloy
| starring       = Evelyn Brent
| music          =
| cinematography = 
| editing        = Richard Cahoon
| distributor    =
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} canteen workers toiling in a chateau near the frontlines in France during World War I. It was directed by William Beaudine and starred Evelyn Brent.    

==Cast==
* Evelyn Brent as Monica Dale
* Irene Rich as Mrs. Schuyler
* Louise Fazenda as Fanny Smithers
* Lilyan Tashman as Lil Wheeler
* Marceline Day as Dorothy Quinlan
* Fritzi Ridgeway as Prudence Graham
* June Clyde as Janice Lee
* Elizabeth Keating as Bluebell Jones
* Helen Keating as Rosemary Jones

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 


 