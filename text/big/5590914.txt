Rawhead Rex (film)
 
 
 
 
 
{{Infobox film| name =Rawhead Rex
  | image = Rawheadrexposter.jpg
  | caption = Theatrical release poster.
  | director = George Pavlou	
  | producer = Kevin Attew Don Hawkins
  | writer = Clive Barker
  | starring = David Dukes Kelly Piper Niall Tóibín Cora Venus Lunny Ronan Wilmot Donal McCann
  | music = Colin Towns John Metcalfe
  | editing = Andy Horvitch 
  | distributor = Empire Pictures 1987
  | runtime = 89 minutes English
  | country  = United Kingdom Ireland
  | budget   = Unknown
  | box office   = Unknown
  }}
 Irish horror film directed by George Pavlou and written by Clive Barker. The film is about a monstrous pagan gods bloody rampage through the Irish countryside, and based on the short story by Clive Barker that originally appeared in vol. 3 of his Books of Blood series. Barker had previously worked on Underworld (1985 film)|Transmutations (also known as Underworld). 

==Plot==
Howard Hallenbeck (David Dukes) travels to Ireland to research items of religious significance. He goes to a rural church to photograph some graves. Meanwhile, three farmers are attempting to remove an ominous stone column from a field. Two of the farmers head home. A thunderstorm appears out of nowhere, and smoke pours from the ground. Lightning strikes the column. The monster Rawhead Rex rises from the dirt.

Howard meets Declan OBrien (Ronan Wilmot), who directs him to Reverend Coot. The curious OBrien approaches the altar and places his hand on it. Images flash before his eyes. This experience apparently destroys OBriens sanity. Afterwards, Howard inquires about the churchs parish records. Coot says he can arrange to have Howard look at them.

Later, a man arrives at the home of locals Dennis and Jenny. He discovers a clearly traumatized Jenny. Police arrive. Rawhead drags Denniss dead body through the forest and comes upon a trailer park. A teenager named Andy Johnson is trying to make out with his girlfriend. The two teens head into the woods. Soon after, Howard sees Rawhead on top of a distant hill with Andys head in his hand.

Afterwards, Howard speaks again with Coot, who tells him the churchs parish records have been stolen. Declan OBrien destroys his camera. He takes his family on the road again.

On the road, Howards daughter needs to go to the bathroom, so Howard pulls over and lets her go by a tree. Hearing her suddenly scream, Howard and his wife rush to her; Howards son stays in the van, alone. Rawhead kills Howards son and takes the body into the woods. Infuriated by the polices unsuccessful efforts to track down Rawhead, Howard returns to the church. He discovers that there is a weapon shown in the stained glass window that can be used to defeat the monster. After Howard leaves, Coot curiously touches the altar but resists the temptations and images it shows him.

Rawhead arrives at the church to baptize OBrien by urinating on him. A bewildered Coot goes outside to investigate the noise and sees Rawhead. Horrified, Coot flees inside the church and into the basement while Rawhead destroys everything inside. Coot finds the missing parish records, showing what appears to be some kind of blueprint of Rawhead himself. The insane OBrien catches Coot and forces him upstairs to be sacrificed to Rawhead. The police arrive at the church and prepare to open fire on Rawhead, but they hesitate because he is carrying Coot. The brainwashed inspector dumps gasoline around the police cars and ignites it just as they begin to shoot at Rawhead, killing all the police, including himself.

Howard leaves his wife and daughter and goes back to the church where a dying Coot tells him that Rawhead is afraid of something in the altar. Howard goes inside where OBrian is burning books and is overpowered by Howard. Howard, by using a candle stick, opens the altar and gets to the weapon. OBrian retreats to Rawhead to tell him, leaving Rawhead displeased. Howard tries to use the weapon, but has no effect. In anger, Rawhead kills OBrian by tearing out his throat, with Howards wife watching in terror. As Rawhead tries to kill Howard, his wife picks up the weapon, it activates, stopping Rawhead from killing Howard. A ray of light comes out of the weapon and hits Rawhead, hurting him. Howard realizes that it has to be a woman for it to work. Then the form of a woman appears from the stone and shoots electric rays through the stones and into Rawheads body, knocking him to the ground. After a few more blasts, Rawhead is drained and weakened to the point where he has no hair, has aged, ill and dying. Finally he falls through the ground with Howards wife dropping the weapon in with him. Rawhead is smashed under giant stones and finished. Both Howard and his wife cry in light of it being over.

In the end, the boy from the trailer park places flowers on Andy Johnsons grave. As he walks away, Rawhead emerges from the ground and roars. The film then goes to its end credits.

==Cast==
*David Dukes as Howard Hallenbeck
*Kelly Piper as Elaine Hallenbeck
*Ronan Wilmott as Declan OBrien
*Niall Toibin as Reverend Coot
*Niall OBrien as Detective Inspector Isaac Gissing
*Hugh OConor as Robbie Hallenbeck
*Cora Lunny as Minty Hallenbeck
*Heinrich von Schellendorf as Rawhead Rex
*Donal McCann as Tom Garron

==Release==
The film was given a limited release theatrically in the United States by Empire Pictures in 1987. It was released on VHS by Vestron Video the same year. 

The film was released on DVD in the United States by Artisan Entertainment in 1999.  This version is currently out of print.   It was released on DVD in the United Kingdom by Prism in 2003. 

==Reception==
 

==Remake==
 
In a 2010 interview with ACIN, Barker discussed a possible remake of the film. He talked of a "Tortured Souls" adaptation, as well as a remake of Rawhead Rex. Barker was not happy with the result of the original, mainly the creatures look in the film (he describes Rawhead as a 9&nbsp;ft tall phallus with teeth). While its not confirmed that Barker will remake the film, mainly due to his role in the Hellraiser reboot, he still has expressed interest in directing a remake.

== References ==
 

==External links==
* 
* http://www.moviesonline.ca/movienews_1269.html
* http://bloody-disgusting.com/news/2588/clive-barker-speaks-rawhead-rex-being-remade/
* http://ghoulsonfilm.net/?p=530

 

 
 
 
 
 
 
 
 
 