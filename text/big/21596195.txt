If the Wind Frightens You
{{Infobox Film
| name           = If the Wind Frightens You
| image          = 
| image size     = 
| caption        = 
| director       = Emile Degelin
| producer       = Hervé Thys
| writer         = Emile Degelin Jacqueline Harpman
| narrator       = 
| starring       = Elisabeth Dulac
| music          = 
| cinematography = Frédéric Geilfus
| editing        = Emile Degelin
| distributor    = 
| released       = 1960
| runtime        = 83 minutes
| country        = Belgium
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

If the Wind Frightens You ( ) is a 1960 Belgian drama film directed by Emile Degelin. It was entered into the 1960 Cannes Film Festival.     

==Cast==
* Elisabeth Dulac - Claude
* Guy Lesire - Pierre
* Henri Billen - Playboy
* Antinea
* André Dandois
* Gaston Desmedt
* Jacqueline Harpan
* Jacqueline Harpman
* Bobette Jouret - Elizabeth
* Anne-Marie La Fère - Bernadette Paul Roland - Nozem

==References==
 

==External links==
* 

 
 
 
 
 
 
 