Koyil Kaalai
{{Infobox film
| name           = Koyil Kaalai
| image          =
| image_size     =
| caption        =
| director       = Gangai Amaran
| producer       = Gangai Amaran Ilayaraja
| writer         = R. Selvaraj Gangai Amaran (dialogues)
| screenplay     = Gangai Amaran Kanaka Sujatha Sujatha Veerapandiyan
| music          = Ilayaraja
| cinematography = A. Sabapathy
| editing        = B. Lenin V. T. Vijayan
| studio         =  Ammas Pictures
| distributor    =  Ammas Pictures
| released       =  
| country        = India Tamil
}}
 1993 Cinema Indian Tamil Tamil film, Sujatha and Veera Pandiyan in lead roles. The film had musical score by Ilayaraja. 

==Cast==
 
*Vijayakanth Kanaka
*Sujatha Sujatha
*Veera Pandiyan
*Yuvarani
*Mahima
*Valarmathi
*Shanmugasundaram
*Goundamani
*Senthil
*Vadivelu
*Periya Karuppu Thevar
 

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Thavamirunthu || || Gangai Amaran || 04.19 
|-  Janaki || Gangai Amaran || 05.01 
|-  Mano || Gangai Amaran || 01.33 
|- 
| 4 || Thaayundu Thanthai || Ilaiyaraaja || Ilaiyaraaja || 05.00 
|-  Janaki || Gangai Amaran || 00.48 
|-  Janaki || Gangai Amaran || 04.56 
|-  Janaki || Gangai Amaran || 04.59 
|- 
| 8 || Thaayundu Thantai || Gangai Amaran || Ilaiyaraaja || 05.00 
|-  Janaki || Gangai Amaran || 05.00 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 