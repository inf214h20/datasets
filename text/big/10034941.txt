The Next Race: The Remote Viewings
The 2007 independent science fiction film written, directed, edited and produced by Stewart St. John. It is the first chapter in the "Creation Wars" saga, followed by Dark Metropolis (2010). Two more sequels are planned.

==Synopsis==
The film is set in an unspecified time on Earth when the descendants of a genetically enhanced race created by mankind have control of the planet. Known as the "ghen" - a derivative of the word "generic" which humans labeled them as—the genetically superior race rule from their underground utopia, Hollow Earth.  Baron Crecilius Pryme, a charismatic political from Ume City, sets out to convince his fellow ghen that what remains of the human race must be eradicated because he believes they are forming a crusade to attack the ghen.  Crecilius shrewdly moves his brother, Aiden Pryme, into his deadly group of private soldiers known as "The Ministers", who hunt and abuse humans above ground.

Meanwhile, a human woman known as "The Channeler", is communicating with the "Kalendoah", a group of non-physical beings of infinite intelligence who promise there is hope and a coming savior.  

Below ground, Crecilius has heard rumblings of a woman offering hope to mankind, and who has the ability to cure the sick and revive the dead, and sends his Ministers to find her.  But Crecilius finds he has even more to worry about when his mother, Hannalin Pryme, takes a stand against his genocide of the human race, and reveals a long-buried secret that will forever change the destinies of the Pryme family.

==Official release==
In 2006, St. John took his $20 million version of The Next Race and wrote this prequel story which he shot on a shoe-string budget, setting the stage for the bigger budgeted features to follow. St. John edited the film himself, including the movie trailer. In February 2007, St. John was invited by the "New York Comic Con" to premiere the trailer, which received applause and interest from the assembled crowd. The film was officially selected to screen at the 2007 Hollywood DV/HD Festival on March 31.

On April 1, 2007, The Next Race was nominated for five awards at the Hollywood DV/HD Festival and won three, including Best Production Design, Best Makeup, and Best Science Fiction Feature Film.

==External links==
 

 
 
 
 
 
 
 
 

 