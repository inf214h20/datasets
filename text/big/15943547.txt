Pankh
{{Infobox film
| name           = Pankh
| image          = Pankh poster.JPG
| caption        =
| director       = Sudipto Chattopadhyaya Sanjay Gupta
| writer         = Sudipto Chattopadhyaya,Varun Gautam
| starring       = Bipasha Basu Maradona Rebello 
Lilette Dubey Mahesh Manjrekar Ronit Roy Sanjeeda Sheikh Amit Purohit
| music          = Raju Singh
| cinematography = Somak Mukherjee
| editing        = Sanjib Datta, Bunty Nagi
| distributor    = Eros Entertainment
| released       =  
| runtime        = 90 minutes
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Pankh is a 2010 Bollywood film written and directed by Sudipto Chattopadhyaya and starring Bipasha Basu, Lilette Dubey, Mahesh Manjrekar, Ronit Roy, Sanjeeda Sheikh and newcomers Maradona Rebello and Amit Purohit.    The film was released on 2 April 2010. The film is reportedly based on the case of child actor Ahsaas Channa, whos apparently a girl passed off as a boy in movies like Kabhi Alvida Naa Kehna and Vaastu Shastra. 

==Plot==
The film is the story of the repercussions that have happened to a boy - a child artist who once played girl roles in films as Baby Kusum - once he becomes an adult and wants to be launched as a hero.  Baby Kusum is a child star, a cute little girl. In reality, Baby Kusum is a boy, masquerading as a girl. He was named Jerry, and then renamed Master Jai for the movies. He and his mother Mary (Lilette) share a turbulent relationship. The boy’s family is poor and they want to make it big. The pressure of parents and ridicule make it a very sad situation for him. He becomes a drug-abuser who has never been to school and spends all his time at home. Eventually he creates an imaginary character he is in love with named Nandini   who taunts him, fights with him, and questions his every move. Jerry is made to face the camera again as a young man, which leads to the final catastrophe. The film highlights the common practice in the Indian film industry of casting children in role contrary to their natural genders.   

==Cast==
* Bipasha Basu — Nandini
*  Sanjeeda Sheikh — Kusum
*    Lilette Dubey — Mary
* Mahesh Manjrekar — producer
* Ronit Roy — director
* Maradona Rebello — Jerry Dacunha
* Amit Purohit — Salim

==Production==

===Development=== Sanjay Gupta who after a 20 minute narration signed him for the film. According to an interview with Times of India Chattopadhyaya drew inspiration from a documentary titled Children Of The Silver Screen by Dilip Ghosh in which child artistes are interviewed as adults and expressed their trauma. The protagonist Jerry, always imagines Bipasha to be a diva and director, Sudipto Chattopadhyay, who has styled the actress’ look in the movie, says that this idea was executed by Rocky S and Gavin Miguel. Rocky wanted Bips to look like two divas - Sophia Loren and Liza Minnelli from ‘Cabaret’. Bipasha reportedly cut her hair for a song, as the wig didn’t fit her. 

===Casting===
  Sanjay Gupta says "I gave Mahesh Manjrekar pivotal roles in my ‘Kaante (2002 film)|Kaante’ and ‘Musafir (2004 film)|Musafir’. I think he’s a really good actor. And if one has to choose between Mahesh Manjrekar the actor and the director I’d go for the former"   Sanjeeda Sheikh of Nach Baliye 3 fame was chosen for a character who is less than 17 years old. 

===Promotion===
A 1-minute  promo shot was released for the movie on January 2008. It shows the essence of the film, a little boy being readied for the film and when he emerges hes a child actress Baby Kusum. It then shows the boy grown up as a young man - a dropout-drug-addict. As he struggles to discover his true self, he is shown rebelling against his mother and getting violent with whoever tries to get close to him. Bipasha appears in the last few seconds of the promo and is shown as the alter ego of Maradona. 

==Music==
{{Infobox album
| Name = Pankh
| Type = soundtrack
| Artist = Raju Singh
| Cover =
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length =
| Lyrics =
| Label = Sanjay Gupta
| Last album  = Raaz – The Mystery Continues (2008)
| This album  = Pankh (2009)
| Next album  = Karma, Confessions and Holi (2009)
}}
Newcomer Raju Singh directs the music for the movie. Singer Sunidhi Chauhan has reportedly sang the song "Jiya Jala Na" where Bipasha seduces Maradona. 

==References==
 

==External links==
 
*  

 
 
 
 
 