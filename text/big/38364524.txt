Courage of the West
{{Infobox film
| name           = Courage of the West
| image          = Courage of the West poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Joseph H. Lewis
| producer       = 
| writer         = Norton S. Parker
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Bob Baker, J. Farrell MacDonald, Lois January, Fuzzy Knight
| music          = Fleming Allen
| cinematography = Virgil Miller
| editing        = Charles Craft
| studio         = Universal Studios
| distributor    = 
| released       =  
| runtime        = 56 minutes 
| country        = USA
| language       = English
| budget         = 
| gross          = 
}} Bob Baker made his debut as a singing cowboy.

==Production==

The film was the first that Joseph H. Lewis directed. 
75 non-union cowboys were hired in Sonora to work on the movie. Lewis was told that the Screen Actors Guild did not have jurisdiction at this distance from Los Angeles, although its members would have to be paid the standard rates agreed with the Guild. 
Filming was completed in seven days. 
This movie, Bakers first, was thought to be his best. The others suffered from predictable plots and poor scripts. 
Lois January, the love interest in the film, said, "Bob Baker was too pretty! He was nice, but didnt get friendly.  He didnt want me to sing a song in his picture.  That business is full of jealousy...". 

==Synopsis==

The film is set during the American Civil War. It begins with a scene in which President Abraham Lincoln establishes the "Free Rangers" to protect gold shipments from the west.
The film then tells the  story of a ranger, played by J. Farrell MacDonald, who adopts the son of a convicted outlaw.  The son is played by Bob Baker. He grows up and becomes the head of the Rangers.  He finds himself in pursuit of a gang of gold robbers, not knowing that their leader is his natural father.  After various twists and turns, the father is shot and the hero marries the girl with whom he has fallen in love, played by Lois January. 
During breaks in the action, Baker sings Resting Beside the Campfire, Ride Along Free Rangers, Song of the Trail, and Ill Build a Ranch House on the Range.
Fleming Allen wrote all these songs. 

==Reception==
 New York Times review said, "Nothing of cult director Joseph H. Lewis much-vaunted flair is on display in this average musical Western". 

==Notes and references==
Citations
 
Sources
 
*  |url=http://www.tcm.com/tcmdb/title/71619/Courage-of-the-West/
 |title=Courage of the West - Brief Synopsis |work=Turner Classic Movies |publisher=Time Warner
 |accessdate=2013-01-29}}
*  |url=http://www.imdb.com/title/tt0028746/
 |title=Courage of the West (1937)|accessdate=2013-01-29 |work=IMDB|}}
*{{cite book |ref=harv
 |last=Driscoll|first=Jim|title=Reflections of a B- Movie Junkie: A Tribute To, and Homage Of, the B-Movie Genre Films of the Saturday Matinees, of Primarily the 40s and 50s
 |url=http://books.google.com/books?id=iqQ5mmu9DfYC&pg=PA42|accessdate=2013-01-28 |chapter=Warblin Bob
 |date=2008-12-30|publisher=Xlibris Corporation|isbn=978-1-4628-3820-2}}
*{{cite book |ref=harv
 |last1=Fitzgerald|first1=Michael G.|last2=Magers|first2=Boyd
 |title=Ladies of the Western: Interviews With 25 Actresses from the Silent Era to the Television Westerns of the 1950s and 1960s
 |url=http://books.google.com/books?id=z-3Yo-60tWEC&pg=PA106|accessdate=2013-01-28
 |date=2009-10-30|publisher=McFarland|isbn=978-0-7864-3938-6}}
*{{cite book |ref=harv
 |last=Sarris|first=Andrew|title=The American Cinema: Directors and Directions, 1929-1968
 |url=http://books.google.com/books?id=q0YFBbFBL8YC&pg=PA132|accessdate=2013-01-29
 |year=1996|publisher=Da Capo Press|isbn=978-0-306-80728-2|page=132}}
*{{cite book|ref=harv
 |last=Segrave|first=Kerry|title=Film Actors Organize: Union Formation Efforts in America, 1912-1937
 |url=http://books.google.com/books?id=6vj7SFsj6CIC&pg=PA165|accessdate=2013-01-29
 |date=2009-01-16|publisher=McFarland|isbn=978-0-7864-4276-8}}
*{{cite journal |ref=harv |url=http://movies.nytimes.com/movie/87969/Courage-of-the-West/overview
 |last=Wollstein|first=Hans J. |year=2010|title=Courage of the West (1937)|accessdate=2013-01-29}}
 

 

 
 
 