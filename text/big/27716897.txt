Tom and Jerry Meet Sherlock Holmes
 
{{Infobox film
| name           = Tom and Jerry Meet Sherlock Holmes
| image          = Tom and Jerry Meet Sherlock Holmes.jpg
| alt            =  
| caption        = 
| director       = Spike Brandt Jeff Siergey
| producer       = Tony Cervone Sam Register Spike Brandt Bobbie Page Alan Burnett Todd Popp
| writer         = Earl Kress
| based on       = Tom and Jerry by William Hanna & Joseph Barbera Sherlock Holmes by Arthur Conan Doyle Michael York Greg Ellis Jess Harnell Richard McGonagle Kath Soucie Tom Kenny
| music          = Michael Tavera
| cinematography = 
| editing        = Robert Birchard
| studio         = Turner Entertainment Warner Bros. Animation
| distributor    = Warner Premiere
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English Spanish French
| budget         = 
| gross          = 
}}
 animated Direct-to-video|direct-to-video film starring Tom and Jerry produced by Warner Bros. Animation. It is the first Tom and Jerry direct-to-video film to be produced without any of its original creators, William Hanna and Joseph Barbera. In the city of London, a crook is stealing jewels around the Scotland Yard, and Red, a beautiful singer, is being framed for this crime. Tom and Jerry, as assistants, must help Sherlock Holmes look for clues and track down the real thief to solve this case.

Since its release, it has also aired on Cartoon Network as part of their regular rotation of Tom and Jerry cartoons.

==Plot==
In London, an unseen criminal begins masterminding his "perfect plot on paper" which starts with a robbery taking place and three cats stealing a pink diamond. Three constables spot them and give chase, but the cats escape to the rooftops and glide off into the night, giving the stolen diamond to a mysterious horseman. The next day Dr. Watson rushes to 221B in Baker Street and informs his colleague Sherlock Holmes of the robbery. Holmes calls Jerry Mouse to bring him a copy of the Times. Jerry heads out to buy it, bumping into Tom Cat on the way, who has something for Holmes. Jerry sends a pile of bricks on Tom and Tom gives chase. Jerry returns to Holmes flat and hands him the paper or what was left of it after the chase. Reading a letter Tom had given to them, for the night, Holmes and Watson decide to go to the Bruce Nigel Theatre and see a lady named Red (Tex Avery)|Red. Holmes is told that she is being blackmailed and Holmes suggests the real perpetrator might be - Professor Moriarty. Holmes deduces that the Star of Punjab, a diamond that is sensitive to the light of a solar eclipse which was to happen the following day, is to be stolen by the mastermind of the blackmail.

At the Punjab Embassy, Spike and Tyke are assigned to guard the Star of Punjab. Spike begins to teach Tyke how to be a good dog guard, but the three cats from the previous night steal the diamond while an unfocused Spike is not looking. The three cats then leave a small button and retreat. They climb out through a hole in Reds home that leads to the tunnel and escape before Holmes, Tom and Jerry arrive. Upon arriving, Jerry tricks Tom into stepping into a broken board and the trio check the tunnel. Finding sawdust, they retreat once hearing Tyke sounding the alarm. Holmes and Watson leave to find the shop from where the button came from, while Tom and Jerry are left to take Red to Holmes flat.

The police believe Red is behind the crimes and offer a reward for the one who finds her. Butch and Droopy move in to take the case and race to see who gets it. Butch then bumps into Tom, Jerry and Red. They begin a tiring chase and Tom drops a lantern on Butchs head. They run to the church and they are let in by Jerrys brother Nibbles (Tom and Jerry)|Tuffy. Tuffy and Red escape but Tom and Jerry hide in the organ. Droopy and Butch then play a song on it and send Tom and Jerry flying.
 The Twisted Lip. Red distracts the cats while guiding Tom, Jerry and Tuffy to the cats, the former getting punched in the face on the way by a customer (for bringing his drink to a different customer). Although the cats escape in time and kidnap Jerry, Tom follows them and rescues Jerry, except for the diamond. They are sent flying again, Jerry lands to safety, but Tom lands on another pitchfork. The trio follow the cats to a graveyard and find the diamond. However, the mysterious horseman from previously gets there before them but Jerry secretly steals the key. Red decides to go to her friends place, who is a professor.
 Crown Jewels. By using the eclipses light, Moriarty will utilize the diamonds as a heat-ray that will fire and ricochet off several mirrors and slice a hole into the Tower of London.

Moriarty then makes his way to the Tower of London with a captive Red. Spike and Tyke are left to guard the Crown Jewels after the incident at the Embassy, but the sliced wall crushes Spike and Moriartys cats steal the Jewels. However, Tom knocks out one cat and tricks him into firing the ray on the cage setting them free. They use the cats winged backpack to fly to the Tower but Moriarty escapes with the Jewels. Tom then crashes into the carriage and they free Red. Jerry frees the horse and spots Holmes and Watson making their way to Moriarty (who earlier had been deduced by Holmes to steal the Jewels). Butch fails to catch the cats and Droopy sends them into the Tower, where they are beaten up by Spike.

Moriarty activates his horseless carriage and he and Holmes fight atop the carriage. Tom accidentally breaks the brake and the carriage steers off an unfinished Tower Bridge. Red, Tom, Jerry and Tuffy make it back to the bridge but believe Holmes to be dead. However, Watson arrives and finds Holmes clinging to an edge with the Jewels. Moriarty and the three cats are arrested but Droopy gets the reward instead of Butch.

At the church, Red marries a wolf and they start howling and run off. While Holmes and Watson watch them run off, Jerry then secretly sets Toms tail on fire and Tom gives chase to Jerry. It is then revealed that Droopy had done the wedding, and Droopy smiles at the screen.

==Cast== Michael York as Sherlock Holmes Professor James Moriarty
* John Rhys-Davies as Doctor Watson
* Grey DeLisle as Red, parody to Irene Adler
* Jeff Bergman as Butch the Bulldog, Droopy
* Phil LaMarr as Spike and Tyke (characters)|Spike, Policeman Greg Ellis as Tin, Sergeant
* Jess Harnell as Pam, Brett Jeremy
* Richard McGonagle as Ali, First Policeman Tuffy

==Trivia==
* This is the first  , and it also marks the first animated appearances of Tom and Jerry since Tom and Jerry Tales.
* Michael Tavera provided the films score, which has been continued from this point forward and it still remains to this day.
* It is the first direct-to-video Tom and Jerry film to not have the involvement of either William Hanna or Joseph Barbera after their deaths.

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 