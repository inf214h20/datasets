Chittagong (film)
 
 
 

{{Infobox film
| name           = Chittagong
| image          = Chittagongposter.jpg
| caption        = Theatrical release poster
| director       = Bedabrata Pain 
| producer       = Bedabrata Pain
| starring       = Manoj Bajpai Vega Tamotia Jaideep Ahlawat Alexx ONell
| writer         = Bedabrata Pain Shonali Bose
| music          = Shankar-Ehsaan-Loy
| cinematography = Eric Zimmerman
| editing        = Aldo Velasco
| released       =    
| country        = India
| language       = {{Plainlist|
* Hindi
* Bengali
* English}} budget         =   gross          =   (3rd week domestic nett)      
}} historical war Chittagong Uprising.   The film features music by trio Shankar Ehsaan Loy and sound by Resul Pookutty.  The world premier of film was on 10 April 2012.  Chittagong released on 12 October 2012 and nett grossed Rs 3.1&nbsp;million at the  Indian box office. This movie won the 60th National Film Award for the Best Debut Film of a Director.

==Plot==
The story is set against the backdrop of a little-known saga in 1930s British Colonial India (now Bangladesh) where a group of schoolboys and young women, led by a schoolteacher Masterda Surya Sen (Manoj Bajpai), dared to take on the Empire. Chittagong is the story of a diffident 14-year-old boy, Jhunku (Delzad Hiwale). Swept up into this seemingly impossible mission, the reluctant teenager battles with self-doubts to achieve an improbable triumph.

Jhunku, now a 23-year-old youth, is being chased by the Bengal police. He hides himself in a bunker with his childhood friend Aparna (Apu/Opu). While hiding, he begins to reflect on his past hopes and dreams. The story goes to a flashback to narrate the events that happened 10 years ago.

In 1930, Surya Sen and his followers are protesting the death of the revolutionary Jatin Das. Jhunku is not allowed to join them by his lawyer father who wants him to study in England. Apart from fearing his father, Jhunku is also torn between his admiration and respect for the magistrate, Wilkinson and his wife, who show great likings for him and his fascination for the charismatic figure of Masterda, who is followed and revered by most of his friends. Jhunku, due to his faith in Wilkinson who is personally against torturing revolutionaries, has great belief in British justice and believes that by getting an English education he might better equip himself to free his country. This causes arguments between him and his friends, especially Aparna. Wilkinson is opposed by the police inspector Major Johnson who defies his orders and arrests the protesters, and badly tortures Masterda.

In protest, some students hit the strict police officer Maj. Johnson (Alexx ONell) by spilling oil under his motorbike, making him fall. Enraged, Johnson makes random enquiries about the culprits but are unsuccessful. However, Wilkinson manages to confirm the truth out of Jhunku, and an enraged Johnson then shoots Sukhen (Shaheb Bhattacharya), one of the boys involved. This incident makes Jhunku an outcast amongst his friends, and he becomes determined to avenge the death of Sukhen. Soon afterwards, Johnson is made the DIG of police although Wilkinson had requested his transfer. This incident further shocks Jhunku. All his faith on his tutor Sir Wilkinson is lost, and he joins Masterdas army.

Masteraa and his fellow comrades Nirmal Sen (Nawazuddin Siddiqui), Loknath Bal (Rajkummar Rao), Ambika Chakraborthy and Ananta Ghosh (Jaideep Ahlawat) train the 50-odd students and plan to capture the town of Chittagong on 18 April 1930 by disconnecting all modes of communication. As per plan, the armoury of the police is captured by a group of revolutionaries led by Ganesh Ghosh and revolutionaries, led by Lokenath Baul takes over the Auxiliary Force armoury. Unfortunately the machine guns are not located. The revolutionaries also dislocate telephone and telegraph communications and disrupt the movement of the trains. After the successful raids, all the revolutionary groups gathered outside the police armoury where Surya Sen takes a military salute, hoisting the national flag and proclaims a provisional revolutionary government. The whole town is overjoyed at the success of Indian Republican Army and Chittagong is officially conquered by Surya Sen and his boys for one day. However, reinforcements from Calcutta soon set out to capture the rebels. Knowing of the army attack, the revolutionaries leave town before dawn and march towards the Jalalabad hill ranges, looking for a safe place. But the British Armys first wave is demolished by Masterdas teenagers. This enrages the British and they bring in machine guns. Harish, the younger brother of Lokenath and a friend of Jhunku, dies.

Soon after revolutionaries disperse in smaller groups, to nearby villages. Ahsanullah Khan from CID comes to Chittagong and cracks down on the revolutionaries. Jhunku is arrested and is severely beaten by Johnson and Ahsanullah during interrogation; he refuses to betray his leaders and comrades. He is sentenced to Kaala-Paani for life imprisonment. In due course, many revolutionaries and police officials are killed in the gun fights including noted revolutionary Nirmal Sen and police CID chief Ahsanullah. However, Pritilata Waddedar successfully attacks the European Club and assassinates DIG Johnson; being gravely wounded she commits suicide by swallowing cyanide.
 Tebagha Uprising of 1945, which marked the end of British Raj in India. The film ends with the memorable song "Ishaan."

==Cast==
* Manoj Bajpai - Masterda Surya Sen    Barry John - Magistrate Wilkinson
* Helen Jones - Emma Wilkinson
* Delzad Hiwale - Jhunku (young Subodh Roy)
* Vijay Verma - Older Jhunku
* Dibyendu Bhattacharya - Ambika Chakrobarty
* Vishal Vijay - Ganesh Ghosh
* Vega Tamotia - Pritilata Waddedar  
* Nawazuddin Siddiqui   - Nirmal Sen
* Rajkummar Rao - Lokenath Bal
* Jaideep Ahlawat - Ananta Singh
* Alexx ONell - DIG Charles Johnson
* Anurag Arora - Ahsanullah
* Sauraseni Maitra - Aparna (young)
* Chaiti Ghosh  - Aparna
* Paritosh Sand - Nilesh Roy
* Tanaji Dasgupta - Rajat Sen
* Arindoi Bagchi - Bhavani Lal
* Shaheb Bhattacharya - Sukhendu
* Tim Grandage - Governor General
* Devina Seth - Kalpana Dutt

==Controversy== Chittagong uprising.  Bedabrata Pain, though, said it was delayed due to the distributors suggesting to postpone it despite him preferring it to release before Khelein Hum Jee Jaan Sey. He also said that Abhishek and his mom Jaya had loved the script when he went to offer the role to Abhishek. 

==Soundtrack==
{{Infobox album|
| Name = Chittagong
| Type = Soundtrack
| Artist = Shankar Ehsaan Loy
| Cover = Chittagong_Album_Cover.png
| Border = yes
| Alt = 
| Caption = Original CD Cover
| Released =
| Recorded = 2011 Feature film soundtrack
| Length =  Hindi
| Label = Yellow And Red Music
| Producer = 
| Reviews =  
| Last album = Don 2 (2011)
| This album = Chittagong (2012)
| Next album = Delhi Safari (2012)
}}
{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_music       = Shankar-Ehsaan-Loy
| all_lyrics      = Prasoon Joshi
| title1          = Bolo Na
| extra1          = Shankar Mahadevan
| length1         = 4:57
| title2          = Bechayan Sapne
| extra2          = Abhijeet Sawant, Gulraj Singh, Mahalakshmi Iyer, Sameer Khan, Shankar Mahadevan
| length2         = 4:56
| title3          = Jeeney Ki Wajah
| extra3          = Aarti Sinha, Mani Mahadevan, Kshitij Wagh, Pooja Gopalan, Raman Mahadevan, Shreekumar Vakkiyil
| length3         = 3:42
| title4          = Ishan
| extra4          = Aarti Sinha, Bedabrata Pain, Kshitij Wagh, Mani Mahadevan, Pooja Gopalan, Raman Mahadevan, Shreekumar Vakkiyil
| length4         = 3:52
| title5          = The Battle
| extra5          = 
| length5         = 2:07
| title6          =  Inspiration
| extra6          =
| length6         = 1:04
| title7          = Masterda
| extra7          =
| length7         = 0:58
| title8          = Chittagong
| extra8          = 
| length8         = 2:04
}}

===Reception===
The album was met with critical acclaim. Kartik of Milliblog praised the album, calling it a "classy opening to Shankar Ehsaan Loy’s 2012 account". {{cite web|url=http://itwofs.com/milliblog/2012/09/27/chittagong-music-review-hindi-shankar-ehsaan-loy/
|accessdate=29 September 2012|title=  Chittagong (Music review), Hindi – Shankar Ehsaan Loy by Milliblog!|date=29 September 2012|quote=Classy opening to Shankar Ehsaan Loy’s 2012 account! }}  Music Aloud remarked that the soundtrack was the best of SEL in a long time. {{cite web|url=http://www.musicaloud.com/2012/09/28/chittagong-music-review-bollywood-soundtrack/
|accessdate=29 September 2012|title=Chittagong - Music Review (Bollywood Soundtrack): Music Aloud|date=29 September 2012|quote=Chittagong is the best SEL has sounded in a long time, hope the soundtrack gets the exposure it deserves. Music review of Chittagong}}  DunkDaft quoted about the music as A sheer delightful soundtrack for the period film.   

Khalid Mohamed praised the score as "evocative",  while Shabna Ansari of NDTV deemed it "soulful". {{cite web|url=http://www.dnaindia.com/entertainment/review_the-after-hrs-review-chittagong-is-compelling_1751733
|accessdate=13 October 2012|title=The After Hrs review: Chittagong is compelling - Entertainment - DNA|date=13 October 2012|quote=The film is seen through the eyes of the youngest soldier in the Indian freedom struggle – Subodh Roy aka Jhunku (Delzad Hiwale).}} 

==Promotion== Cinemax Versova (Mumbai)|Versova, a western Mumbai suburb. Several Bollywood biggies including Amitabh Bachchan, Shahrukh Khan, Resul Pookutty and Anil Kapoor turned up to support the filmmaker. 

== Critical reception ==
{| class="wikitable infobox plain" style="float:right; width:20em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|- DNA India
| 
|-
|The Times of India
| 
|- Bollywood Hungama
| 
|- Rediff
| 
|}

The film was released worldwide on 12 October 2012. It garnered mostly positive reviews from critics.

Srijna Mitra Das of   gave 3.5 stars.  Aniruddha Guha of DNA gave it 3.5 stars. "Chittagong is the kind of film that will leave you with a heavy heart, and moved. Give it a shot." said DNA.  MOZVO, social movie rating site, gave Chittagong 3.5 out of 5 making it a Recommended movie.  Shubra Gupta of the Indian Express gave 3.5 stars.  Jim Luce of Huffington Post wrote "The film Chittagong is a brilliant, poignant action-drama, made more so by the fact that it is true." 

==Accolades==
{| class="wikitable"   
|+ List of positive awards and nominations
|-
! style="width:25%;" scope="col"| Distributor
! style="width:15%;" scope="col"| Date aired
! style="width:25%;" scope="col"| Category
! style="width:25%;" scope="col"| Recipient
! style="width:8%;" scope="col"| Result
! style="width:2%;" scope="col"| Reference
|-
|| Colors Screen Awards
|| 19 January 2013
| Best Production Design
| Samir Chanda, Amit Roy, Pradeep Jha  
| rowspan="1"  
| style="text-align:center;"| 
|}

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 