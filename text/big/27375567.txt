The Princess of Nebraska
{{Infobox film
| name           = The Princess of Nebraska
| image          = 
| caption        = 
| director       = Wayne Wang
| writer         =  Li Ling
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Li Ling and Brian Danforth.  It was adapted from a story by Yiyun Li.

==Plot==
A pregnant San Francisco Chinese teenage immigrant named Sasha tells of life in America.

==Cast== Li Ling as Sasha
*Brian Danforth as Boshen
*Minghua Tan as May
*Zhi Hao Li as Driver
*Hiep Thi Le as Mother at mall

==Reception==
A.O. Scott of the New York Times gave a positive review saying "Moments of obviousness are offset by a feeling of gritty lyricism in Wayne Wangs "Princess of Nebraska," a beautifully shot but awkwardly acted movie." 

==References==
 

==External links==
*  

 

 
 
 
 