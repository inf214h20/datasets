Celine: Through the Eyes of the World
{{Infobox film
| name           = Celine: Through the Eyes of the World
| image          = TTEOTW.jpg
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Stéphane Laporte
| producer       = Julie Snyder
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Celine Dion
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =  The Hot Ticket
| released       =  
| runtime        = 120 minutes
| country        = Canada
| language       = {{flat list|
*English
*French
}}
| budget         = 
| gross          = 
}}

Celine: Through the Eyes of the World is a Documentary film|documentary–concert film chronicling the life of Canadian singer, Celine Dion during her 2008–2009 Taking Chances World Tour. It premiered in Miami on 16 February 2010 and was released by The Hot Ticket in theaters in North America on 17 February 2010. It was also released in Australia and the United Kingdom. The film received mixed reviews from critics, with most of them indicating it is mainly for the fans. Celine: Through the Eyes of the World grossed $1,027,341 in Canada alone and became the number-one domestic movie there.
 Platinum in Gold in the United States and Australia.

==Background and release==
During the Taking Chances World Tour, Dion played concerts in five continents, 25 countries and 93 cities, selling more than three million tickets.    Running from February 2008 through February 2009, featuring both Anglophone and Francophone setlists, the tour broke attendance and box office records at venues around the world.  In December 2009, Sony Pictures Entertainment announced that their division, The Hot Ticket will bring a special motion picture entitled Celine: Through the Eyes of the World to theaters.     

Intercutting concert performances of Dions biggest hits with behind-the-scenes footage, the documentary depicts the singers life during the Taking Chances World Tour, giving fans the unique opportunity to follow Dion everywhere, on stage, backstage, and enjoying free time with her family.  It was filmed by Jean Lamoureux and directed by Stéphane Laporte.  According to Dion herself, "This was an amazing world tour, but theres a lot more than just concert footage on this film. I let the cameras follow me everywhere. There are a lot of ups and downs, and its very personal... and its definitely the most intimate journey that Ive ever shared with my fans." 
 Odeon and Cineworld cinemas across the United Kingdom in late February 2010. 

==Concert footage==
The following concert performances were included in the documentary. While some showed a full performance, most of the songs were edited and cut short.
 I Drove All Night"
*"Love Can Move Mountains"
*"Ive Got the Music in Me"
*"Im Alive (Celine Dion song)|Im Alive"
*"We Will Rock You"
*"Watashi Wa Totemo Shiawase Ne"
*"A World to Believe In  (duet with Yuna Ito) 
*"To Love You More" Taking Chances"
*"Its All Coming Back to Me Now#Céline Dion|Its All Coming Back to Me Now" Eyes on Me" The Show Must Go On" The Power of Love" Shadow of Love" All by Myself"
*"Alone (Heart song)#Céline Dion version|Alone"
*"Dans un autre monde"
*"Un garçon pas comme les autres (Ziggy)"
*"Pour que tu maimes encore"
*"Jirai où tu iras"  (duet with Marc Langis)  A Song For You" The Prayer"  (duet with Andrea Bocelli via pre-recorded video) 
*"Im Your Angel"  (duet with Barnev Valsaint) 
*"Sil suffisait daimer (song)|Sil suffisait daimer" My Love"
*"Because You Loved Me"  (duet with Charice Pempengco|Charice)  Soul medley including "Its a Mans Mans Mans World"
*"River Deep – Mountain High"
*"My Heart Will Go On"

==Critical reception==
{{Album ratings MC = 52/100  rev1 = AllMovie rev1score =      rev2 = Boston Herald rev2score = A-  rev3 = Boxoffice (magazine)|Boxoffice rev3score =    rev4 = The Gazette rev4score =    rev5 = The Globe and Mail rev5score =    rev6 = Los Angeles Times rev6score =    rev7 = Quad-City Times rev7score =    rev8 = Toronto Star rev8score =    rev9 = Toronto Sun rev9score =    rev10 = USA Today rev10score =   
}}
Review aggregation website Metacritic, which assigns a weighted mean rating out of 100 to reviews from mainstream critics, gave the film an average score of 52 based on 4 reviews, which indicates "mixed or average reviews".    Elysa Gardner of USA Today gave the movie 3 stars out of 4. She stated that "Celine Dion may be responsible for some of the smoothest, most syrupy pop recordings in recent decades, but in concert, she is a pit bull."     Lauren Carter from Boston Herald gave the film A-. According to her "the film succeeds because it lets us in on Celine the person as much as Celine the performer."    

Richard Ouzounian of Toronto Star gave the film 3.5 out of 4 stars and said that "its more than just a collection of the Quebec singers greatest hits performed in concert, although theres enough high-energy, nicely shot musical numbers to gladden the heart of any Dionista." Also "what makes this a real surprise and something that should strike a chord with those who arent necessarily Dion fans, is that it also takes you inside: inside the universe of a superstar tour as it careens around the world, as well as inside the mind, heart and – in one unforgettable scene – even the larynx of Dion." In the end, "if you like Dion, the singer, then you will love Celine: Through the Eyes of the World. But even if you dont, you cant help but be impressed by this fascinating portrait of a superstar trying very hard to keep her feet on earth while her career keeps soaring ever upward."    
 The Gazette said that "fans die and go to pop heaven with Stéphane Laportes sympathetic and even candid documentary about our very own superstars world tour in 2008-09. Non-fans discover her real appeal in Céline: Through the Eyes of the World." He gave the film 3 out of 5 stars.     

Linda Cook of Quad-City Times gave the film 3 out of 4 stars and said that she "was pleasantly surprised when she realized the movie had her hooked about 15 minutes into the film". She also said that "its mesmerizing to see audiences all over the world singing along in French and/or English, and to see the moment when she was presented with the Legion of Honour." She "enjoyed watching as Celine and her family became tourists in the countries she visited, whether she was taking in the sounds and sights of Africa or Australia. Above all, its fascinating to watch her music unite the world as audiences from Dubai and Amsterdam sing familiar lyrics."    

Sara Schieron from Boxoffice (magazine)|Boxoffice stated that "in sum, what you see is a woman and a family that handle celebrity with a unique grace." She also said that the "box office will, no doubt, confirm demand for the movie" and gave it 3 stars out of 5.    According to The Globe and Mail "you have to admit Dion is the James Brown of her generation - the hardest working woman in showbusiness".    Los Angeles Times wrote that "rather than capturing a restless star pushing at her own boundaries, Through the Eyes of the World finds Dion sitting pretty, on top of the world and happy to stay there".   

==Box office==
In Canada, after just two days Celine: Through the Eyes of the World was shown in 81 theatres and entered the domestic chart at number two, earning $240,942. The next week it became number-one domestic movie. It was shown in 89 theatres and earned $476,412. In the third week, it stayed at number one, grossing $309,987 and was shown in 54 theatres. After just three weeks and eight days of screening (17, 18, 20, 21, 22, 25, 27 and 28 February 2010), the movie grossed $1,027,341 in Canada.  In Australia, Celine: Through the Eyes of the World earned $213,583 from only 45 screens, with $4,746 per-theater average and became the seventeenth most popular film between 18 and 24 February 2010. 

==Home media==
{{Infobox Album
| Name        = Celine: Through the Eyes of the World
| Type        = video
| Artist      = Celine Dion
| Cover       =
| Cover size  = 
| Released    =  
| Recorded    = 2008–2009 Pop
| Length      = 173:00 Columbia
| Director    = Stéphane Laporte
| Producer    = Julie Snyder
| Reviews     = 
| Chronology  = 
| Last album     =  
| This album     =  
| Next album     =  
}}

Celine: Through the Eyes of the World was released on   (Tournée mondiale Taking Chances: le spectacle in Francophone countries), which was issued on the same date. 
 Fade Away," The Show Must Go On." 

===Commercial performance===
In Canada, Dions new DVD releases debuted at the top three on the   (2007).    entered the chart at number two with 31,000 units sold, followed by   at number three with sales of 8,000 copies.   This was only the second time since 2004, after   award for selling 200,000 copies, Tournée mondiale Taking Chances: le spectacle with   for selling 40,000 copies.    This made Celine: Through the Eyes of the World, one of the fastest selling double-Diamond certification by a Canadian artist. 

In the United States, Dion entered the   and   certified Taking Chances World Tour: The Concert   for shipment of 50,000 units.       On the Billboards 2010 Year-End Top Music Video Sales chart, Taking Chances World Tour: The Concert was placed at numer ten and Celine: Through the Eyes of the World at number nineteen.    According to Nielsen SoundScan, Taking Chances World Tour: The Concert has sold 76,000 copies in 2010 and Celine: Through the Eyes of the World ended the year with sales of 50,000 units in the United States. 
 Platinum in Gold in Australia in May 2010.

===Charts and certifications===
 
 

==== Weekly charts ====
{|class="wikitable sortable"
|-
!DVD chart (2010)
!Peak position
|- Argentinian Music DVDs (Argentine Chamber of Phonograms and Videograms Producers|CAPIF)  2
|- Australian Music DVDs (ARIA Charts|ARIA)  2
|- Austrian Music DVDs (Ö3 Austria Top 40|Ö3 Austria)  8
|- Belgian Music DVDs (Ultratop Flanders)  1
|- Belgian Music DVDs (Ultratop Wallonia)  1
|- Czech Republic Music DVDs (International Federation of the Phonographic Industry|ČNS IFPI)  5
|- Canadian Music DVDs (Nielsen SoundScan)  1
|- Danish Music DVDs (Tracklisten|Hitlisten)  3
|- Dutch Music DVDs (MegaCharts)  2
|- Finnish Music Suomen virallinen lista)  7
|- French Music DVDs (Syndicat National de lÉdition Phonographique|SNEP)  2
|- German Music Media Control)  6
|- Hungarian Music DVDs (Association of Hungarian Record Companies|MAHASZ)  3
|- Irish Music DVDs (Irish Recorded Music Association|IRMA)  5
|- Italian Music DVDs (Federazione Industria Musicale Italiana|FIMI)  3
|- New Zealand Music DVDs (Recorded Music NZ)  2
|- Portuguese Music DVDs (Associação Fonográfica Portuguesa|AFP)  8
|- South African Music DVDs (Recording Industry of South Africa|RISA)    2
|- Spanish Music DVDs (Productores de Música de España|PROMUSICAE)  17
|- Swedish Music DVDs (Sverigetopplistan)  2
|- Swiss Music Schweizer Hitparade)  4
|- UK Music DVDs (Official Charts Company|OCC)  1
|- US Top Music Video Sales (Billboard (magazine)|Billboard)  2
|-
|}

{|class="wikitable sortable"
|-
!Album chart (2010)
!Peak position
|-
 
|-
 
|- South Korean International Albums (Gaon Albums Chart|GAON)  93
|-
|}
 

====Certifications====
 
 
 
 
 
 

==== Year-end charts ====
{| class="wikitable sortable"
|-
!Chart (2010)
!Position
|- Belgian Music DVDs (Ultratop Flanders)  11
|- Belgian Music DVDs (Ultratop Wallonia)  9
|- Dutch Music DVDs (MegaCharts)  32
|- French Music DVDs (SNEP)  13
|- US Top Music Video Sales (Billboard)  19
|-
|}
 

===Release history===
{|class="wikitable"
! Region
! Date
! Label
! Format
! Catalog
|-
|rowspan="1"| Australia  
|rowspan="1"| 29 April 2010 Columbia
|rowspan="5"| DVD, Blu-ray Disc|Blu-ray
|rowspan="1"| {{flat list|
*88697583039  (DVD) 
*88697583069  (Blu-ray) 
}}
|-
|rowspan="1"| France  
|rowspan="1"| 3 May 2010
|rowspan="2"| {{flat list|
*88697650829  (DVD) 
*88697583069  (Blu-ray) 
}}
|-
|rowspan="1"| Canada    
|rowspan="1"| 4 May 2010
|-
|rowspan="1"| United Kingdom  
|rowspan="1"| 10 May 2010
|rowspan="2"| {{flat list|
*88697583039  (DVD) 
*88697583069  (Blu-ray) 
}} 
|-
|rowspan="1"| United States  
|rowspan="1"| 11 May 2010
|-
|rowspan="1"| United Kingdom 
|rowspan="2"| 17 May 2010
|rowspan="4"| 2DVD
|rowspan="1"| 88697673689
|-
|rowspan="1"| France 
|rowspan="2"| 88697673699
|-
|rowspan="1"| Canada 
|rowspan="2"| 25 May 2010
|-
|rowspan="1"| United States 
|rowspan="1"| 88697673689
|-
|rowspan="1"| Japan 
|rowspan="1"| 4 August 2010 SMEJ
|rowspan="1"| DVD
|rowspan="1"| SIBP-163
|-
|}

==See also==
* 
*Taking Chances World Tour

==External links==
* 
* 
* 
* 
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 