Losin' It
{{Infobox film
| name           = Losin It
| image          = LosinIt.jpg
| caption        = Theatrical release poster
| director       = Curtis Hanson
| producer       = Bryan Gindoff Hannah Hempstead Joel B. Michaels
| writer         = Bill L. Norton
| starring = {{plainlist|
* Tom Cruise
* Jackie Earle Haley John Stockwell
* Shelley Long
}}
| music          = Kenneth Wannberg
| cinematography = Gilbert Taylor
| editing        = Richard Halsey Embassy Pictures MGM (2001, DVD)
| released       =  
| runtime        = 100 minutes
| country        = Canada United States
| language       = English
| budget         = $7,000,000  
}} John Stockwell. The film is directed by Curtis Hanson.  It was filmed largely in Calexico, California.

== Plot ==
Four teenagers from late 1950s  ity, while Wendell came along with them to buy fireworks. They end up picking up a woman named Kathy, who goes with them because she wants a fast divorce from her husband, and they get into a series of misadventures south of the border.

== Cast ==
* Tom Cruise as Woody
* Shelley Long as Kathy
* Jackie Earle Haley as Dave John Stockwell as Spider
* John P. Navin, Jr. as Wendell
* Henry Darrow as Sheriff
* Hector Elias as Chuey
* Mario Marcelino as Pablo
* John Valby as Johnny Hotrocks
* Rick Rossovich as Marine
* Kale Browne as Larry

== Reception ==
=== Critical response ===
The film has received mostly negative reviews from critics, with a 22%  score on Rotten Tomatoes based on nine reviews. 

== Home media ==
 
MGM released Losin It as a Region 1 DVD on February 6, 2001.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 