Hysterical Psycho
{{Infobox film
| name           = Hysterical Psycho
| image          =
| alt            =
| caption        =
| director       = Dan Fogler
| producer       = Thomas Michael Sullivan
| writer         = Dan Fogler
| starring       = {{plainlist|
* Randy Baruh
* Noah Bean
* Kelly Hutchinson
* Charissa Camorro
* Nicholas DeCegli
* Kate Gersten
}}
| music          = Michelangelo Sosnowitz
| cinematography = Mario Ducoudray
| editing        = Jacob Gentry
| studio         = Studio 13
| distributor    = Indican Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Hysterical Psycho is a 2009 American horror comedy film written and directed by Dan Fogler.  It stars Randy Baruh, Noah Bean, Kelly Hutchinson, Charissa Camorro, Nicholas DeCegli, and Kate Gersten as a group of friends who encounter "lunar radiation" that causes people to go insane.

== Plot ==
A theater troupe from New York City visit a secluded area known as Moon Lake.  Unknown to them, the cabin in which they stay is located on a Native American burial ground among other sites.  In addition, it is the location of a lunar meteorite that has bathed the area in radiation that causes people to go violently insane.

== Cast ==
* Randy Baruh as Lenny
* Noah Bean as Chuck
* Kelly Hutchinson as Cindy
* Charissa Camorro as Ally
* Nicholas DeCegli as the giant
* Kate Gersten as Sara
* Lennon Parham as deaf mute sister
* Sarah Saltzberg as Jo
*  Ariel Shafir as Steve
* Thomas Michael Sullivan as Christopher

Gilbert Gottfried and writer-director Dan Fogler appear in cameos.

== Release ==
Hysterical Psycho premiered at the Tribeca Film Festival.  Indican released it on DVD in the US on February 18, 2014. 

== Reception ==
John Anderson of Variety (magazine)|Variety wrote that it was probably more fun to make than it is to watch, as horror films have already parodied themselves at length.  Bloody Disgusting rated it 4.5/5 stars and wrote, "Hysterical Psycho is by far one of the most absurd films I’ve ever admitted to loving."   Mark L. Miller of Aint It Cool News compared it to Student Bodies and wrote, "There are moments that will definitely make you laugh, but in between those guffaws, I found myself mostly impressed by the intensity and creativity of the scares."   Matt Donato of We Got This Covered rated it 2.5/5 stars and wrote, "Its a shame that Hysterical Psycho never really comes together, because there are some hilariously horrifying moments of spoofy slasher goodness to be found here." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 