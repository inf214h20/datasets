Blackmailed (1951 film)
{{Infobox film
| name           = Blackmailed
| alt            =  
| image	=	Blackmailed FilmPoster.jpeg
| caption        = 
| director       = Marc Allégret
| producer       = Harold Huth Norman Spencer
| writer         = Hugh Mills Elizabeth Myers Roger Vadim
| starring       = Mai Zetterling Dirk Bogarde Fay Compton Robert Flemyng
| music          = John Wooldridge
| cinematography = George Stretton
| editing        = John Shirley
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Blackmailed is a 1951 British drama film directed by Marc Allégret and starring Mai Zetterling, Dirk Bogarde, Fay Compton and Robert Flemyng.  It was adapted from a novel by Elizabeth Myers and was also released as Mrs. Christopher.

==Cast==
* Mai Zetterling - Carol Edwards
* Dirk Bogarde - Stephen Mundy
* Fay Compton  - Mrs Christopher
* Robert Flemyng - Doctor Freeman
* Michael Gough - Maurice Edwards
* James Robertson Justice - Mr Sine
* Joan Rice - Alma
* Harold Huth - Hugh Sainsbury
* Wilfrid Hyde-White - Lord Dearsley
* Nora Gordon - Housekeeper
* Cyril Chamberlain - Police Constable
* Charles Saynor - Police Constable
* Derrick Penley - Patrick
* Peter Owen - Chief Printer
* Dennis Brian - Sub-Editor
* Arthur Hambling - Inspector Canin

==References==
 

==External links==
* 
 
  
 
 
 
 
 
 
 
 

 