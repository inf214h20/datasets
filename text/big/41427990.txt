The Forger (2014 film)
{{Infobox film
| name           = The Forger
| image          = The Forger poster.jpg
| caption        = Theatrical release poster Philip Martin
| producer       = 
| writer         = Richard DOvidio
| starring       = John Travolta Christopher Plummer Abigail Spencer  Jennifer Ehle Tye Sheridan Victor Gojcaj
| music          = 
| cinematography = John Bailey Peter Boyle
| studio         = 
| distributor    = Saban Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $11 million 
| gross          = 
}}
 Philip Martin and starring John Travolta. It started filming in October 2013.   It was released to theaters on April 24, 2015. 

== Premise ==
A second-generation petty thief arranges to get out of prison to spend time with his ailing son by taking on a job with his father to pay back the syndicate that arranged his release. 

==Cast==
*John Travolta as Raymond J. Cutert
*Christopher Plummer as Joseph Cutter
*Abigail Spencer as Agent Paisley
*Jennifer Ehle as Kim 
*Tye Sheridan as Will Cutter
*Victor Gojcaj as Dimitri

==Release and reception==
===Distribution===
The Forger premiered at the Toronto International Film Festival on September 12, 2014.  Two days before the premiere, Saban Films acquired the U.S. distribution rights for over $2 million. 

===Critical response===
The Forger received mostly negative reviews. On  , the film has a score of 32 out of 100, indicating "generally unfavorable reviews", based on 16 reviews from critics. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 