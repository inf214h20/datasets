Soulboy (film)
 
{{Infobox film
| name           = Soulboy
| image          = Soulboy.jpg
| border         = yes
| caption        = 
| director       = Shimmy Marcus
| producer       = Christine Aldersen Natasha Carlish Peter Rudge
| writer         = Jeff Williams Bruce Jones
| music          = Len Arran
| cinematography =  
| distributor    = Soda Pictures Moviehouse Entertainment
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Soulboy (previously given the working title Souled Out   ) is a 2010 British film directed by Shimmy Marcus about 17-year-old Joe McCain (Martin Compston) coming of age in 1974 amidst the northern soul scene. The film was shot in Stoke on Trent.

==Plot==
  
The film is set in Stoke-on-Trent in 1974. Joe McCain, 17 and restless, is bored with the flatline tedium of a life that seems like its going nowhere, spending his Saturday nights in a dead pub called The Purple Onion and trying to rob the local fish and chip shop. However he then sees a beautiful woman in the street, and acting on impulse follows her into a record shop called Dee Dees Discs, where he finds out that one of her main interests is soul music and dancing at weekends at the Wigan Casino - the home of Northern Soul. He decides to go with his friend Russ on the coach that Saturday night, and starts to devote himself to learning how to fit in with the soul scene and become a Soul Boy - but there are complications on the way...

==Cast==
*Martin Compston as Joe McCain
*Felicity Jones as Mandy Hodgson
*Alfie Allen as Russ Mountjoy
*Nichola Burley as Jane Rogers
*Craig Parkinson as Alan
*Brian McCardie as Fish Shop Bobby
*Jo Hartley as Monica
*Pat Shortt as Brendan
*Huey Morgan as Dee Dee Bruce Jones as Wigan Casino bouncer

==Soundtrack== Duffy and The Dap-Kings worked on the soundtrack to the film.    
Two classic Jimmy Radcliffe recordings appear on the soundtrack release, the storming dancer "Breakaway" by The Steve Karmen Big Band featuring Jimmy Radcliffe and one of the 3 Before 8 alltime classics "Long After Tonight Is All Over".

==References==
 

==External links==
*  
*  

 
 
 
 
 