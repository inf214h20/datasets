Hineini
{{Infobox Film
| name           = Hineini: Coming Out in a Jewish High School
| image =  
| image_size     = 
| caption        = 
| director       = Irena Fayngold
| producer 	 =   and Irena Fayngold
| cinematography = Bob Nesson
| editing        = Michael Traub
| distributor    = Keshet
| released       = 2005/DVD in 2008
| runtime        = 60 mins for feature film, plus 30 mins extras
| country        = USA
| language       = English
}}

Hineini: Coming Out in a Jewish High School (2005) is a documentary film by director Irena Fayngold.

==Plot==
Hineini (Hebrew for Here I am) chronicles the story of Izens attempt to establish a gay-straight alliance at a Jewish high school in the Boston.

The student, Shulamit Izen, enters 9th grade at The New Jewish High School (now Gann Academy) in Waltham, Massachusetts openly identifying as a lesbian. Set in an interview format, Hineini documents Izen, her family, teachers, and other students who both support her campaign and those who oppose it. 

Among themes examined in the documentary is the Jewish community wrestling with the issues of pluralism and diversity.

==Impact==
The film was listed in the 2006 issue of  , a resource guide of the "50 most innovative Jewish organizations and projects"  in the United States. 

Hineini, along with Trembling Before G-d and Encounter Point, are featured in the  .

==Reactions==
"An engaging portrait of a pioneering teen activist...a terrific teaching tool. New Jews take on religious pluralism is never less than fascinating as students and staffers hold forth, pro and con. Docu renders ancient concerns as vibrantly contemporary." - Variety 

"  examines how one community balanced its members’ profound and conflicting needs…Fayngold captures   tension…  the emotional stakes." -The Boston Globe  

"…it’s hard to resist the inspiring story of Shulamit Izen, a lesbian student who started a Gay-Straight Alliance at her private, religious New Jewish High School." - San Francisco Chronicle 

"Director Irena Fayngold retraced Izens journey, which took place in 2001, for her film...When she met Izen, Fayngold realized this wasnt the story of one girl. It was the story of a community, she said." 

"Poignant…Fayngold’s film reveals both the pathos and humor involved in the soul-searching that Izen and Gann Academy experience." -Jewish Telegraph Agency 

"In the end it is perhaps   Lehmann who articulates the film’s theme most precisely and most eloquently by noting that the core of our tradition is to bring together those conflicting opinions not in an attempt to somehow resolve them or create harmony, but to actually live in the tension of those differences." 

==See also== List of GLBT-related films

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 