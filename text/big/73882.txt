The Gold Rush
 
{{Infobox film
| name = The Gold Rush
| image = The Gold Rush.jpg 
| caption = Theatrical release poster 
| director = Charlie Chaplin
| producer = Charlie Chaplin
| writer = Charlie Chaplin Tom Murray
| music = (1942 re-release){{plain list|
*Charlie Chaplin
*Carli Elinor
*Max Terr
*James L. Fields
}}
| cinematography = Roland Totheroh
| editing = Charlie Chaplin
| distributor = United Artists
| released =  
| runtime = 95 minutes
| country = United States
| language = Silent film English intertitles
| budget = $923,000
| gross  = $2.5 million (US/Canada)   accessed 19 April 2014  $4 million (worldwide) {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 | page = 57
 }} 
}}
  silent comedy Little Tramp Tom Murray, Henry Bergman, and Malcolm Waite. Chaplin declared several times that this was the film for which he most wanted to be remembered.  Though it was a silent film, it received  Academy Award nominations for Best Music and Best Sound Recording upon its re-release in 1942.

==Plot==
 
The Lone Prospector (played by Chaplin), a valiant weakling, seeks fame and fortune among the sturdy men who marched across Chilkoot Pass during the Klondike Gold Rush. The Lone Prospectors inoffensive patience and his ill-chosen garb make him the target for the buffoonery of his comrades and the victim of the merciless rigors of the frozen North. After he is caught in a blizzard, the icy clutches of the storm have almost claimed him when he stumbles into the cabin of Black Larsen (played by Murray), a renegade. Larsen is thrusting him out the door, back into the arms of death, when Fate, which preserves the destinies of simple children, appears in the person of Big Jim McKay (played by Swain). Jim subdues the renegade, and he and the Lone Prospector occupy the cabin while their unwilling host is thrust forth to obtain food. Starvation almost claims the two until a bear intrudes and is killed to supply their larder.

The storm abates, and the two depart for the nearest town. Jim heads for his hidden mine, the richest in Alaska. Jim finds Larsen in possession of his property, and in the battle that ensues, Larsen fells Jim with a blow from a shovel. Larsen flees from the scene and is swept to his death in an avalanche. Jim recovers consciousness, but he has lost his memory from the blow.

The Lone Prospector arrives in one of the boom towns of the gold trail. He becomes the principal amusement of the village, a victim of  practical jokers, and the target of gibes and hilarity from the dance hall habitués. His attention becomes centered on Georgia (played by Hale), queen of the dance hall entertainers; he becomes enamored with the girl at first sight. In his timid and pathetic way, he adores Georgia at a distance and braves the gibes of the dance hall roughs to feast his lovelorn eyes. Every indignity is heaped upon him until as a last cruel jest, Jack Cameron (played by Waite), the Beau Brummel of the camp, hands him an endearing note from Georgia. Believing it written for him, the unhappy lover starts feverishly searching the dance hall for the girl, when Jim, his memory partially restored, enters.

Jims only thought is to find the location of the cabin in order to locate his lost mine. He recognizes the Lone Prospector and seizes him, shouting to lead the way to the cabin so that they can both be millionaires. But the lovelorn Prospector catches sight of Georgia on the balcony; breaking away, he darts up to embrace her and declare his love, to the astonishment of the girl as well as the crowd. Unceremoniously dragged from the hall by Jim, the Lone Prospector shouts to Georgia that he soon will return to claim her, as a millionaire. He and Jim return to the cabin, better-provisioned than before. Overnight, another blizzard blows the cabin all the way to Jims claim and beyond&nbsp;— half over a cliff. In the morning, Jim and the Lone Prospector awake to a teeter-totter experience lasting many tense minutes, before the Lone Prospector is pulled from the cabin by Jim as it falls into a chasm.

One year later, Jim and his partner, the Lone Prospector, are returning to the United States wealthy. Yet the heart-yearnings of the lover will not be stilled. Georgia has disappeared, and his search for her has been futile. The fame of the partners strike has spread, and newspapermen board the liner for interviews. The Lone Prospector consents to don his old clothes for a photograph. Tripping in the companionway, he falls down the stairs into the arms of Georgia, on her way back to the United States as a steerage passenger. The reporters sense a romance and ask who the girl is. The Lone Prospector whispers to Georgia, who nods assent. Arm in arm, they pose for pictures while the reporters enthusiastically exclaim, "What a great story this will make!"

==Cast==
* Charlie Chaplin (as The Tramp) as The Lone Prospector
* Georgia Hale as Georgia
* Mack Swain as Big Jim McKay Tom Murray as Black Larsen
* Malcolm Waite as Jack Cameron
* Henry Bergman as Hank Curtis

==Production==
Lita Grey, whom Chaplin married in mid-1924, was originally cast as the leading lady, but was replaced by Georgia Hale. Although photographs of Grey exist in the role, documentaries such as Unknown Chaplin and Chaplin Today: The Gold Rush do not contain any film footage of her. Discussing the making of the film in the documentary series Unknown Chaplin, Hale revealed that she had idolized Chaplin since childhood, and that the final scene of the original version, in which the two kiss, reflected the state of their relationship by that time; Chaplins marriage to Lita Grey had collapsed during production of the film. Hale discusses her relationship with Chaplin in her memoir Charlie Chaplin: Intimate Close-Ups. 

Chaplin attempted to film many of the scenes on location near Truckee, California, in early 1924. He abandoned most of this footage, which included the Lone Prospector being chased through snow by Big Jim, instead of just around the hut as in the final film, retaining only the films opening scene. The final film was shot on the back lot and stages at Chaplins Hollywood studio, where elaborate Klondike sets were constructed.

==Box office==
The Gold Rush was a huge success in the US and worldwide. It is the fifth-highest-grossing silent film in cinema history, taking in more than $4,250,001 at the box office in 1926, and the highest-grossing silent comedy.  Chaplin proclaimed at the time of its release that this was the film for which he wanted to be remembered.   

It earned United Artists $1 million and Chaplin himself a profit of $2 million. 

==Critical reception==
 
The original 1925 release of The Gold Rush was generally praised by critics. Mordaunt Hall wrote in The New York Times:

 

Variety (magazine)|Variety also published a rave review, saying that it was "the greatest and most elaborate comedy ever filmed, and will stand for years as the biggest hit in its field, just as The Birth of a Nation still withstands the many competitors in the dramatic class." 

The New Yorker published a mixed review, believing that the dramatic elements of the film did not work well alongside Chaplins familiar slapstick: 

Nevertheless, The New Yorker included The Gold Rush in its year-end list of the ten best films of 1925. 
 Brussels World The Battleship Potemkin. In 1992, The Gold Rush was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

Chaplin biographer Jeffrey Vance considers The Gold Rush to be Chaplins greatest work of the silent-film era. Vance writes, "The Gold Rush is arguably his greatest and most ambitious silent film; it was the longest and most expensive comedy produced up to that time.  The film contains many of Chaplin’s most celebrated comedy sequences, including the boiling and eating of his shoe, the dance of the rolls, and the teetering cabin.  However, the greatness of The Gold Rush does not rest solely on its comedy sequences but on the fact that they are integrated so fully into a character-driven narrative. Chaplin had no reservations about the finished product.  Indeed, in the contemporary publicity for the film, he is quoted, This is the picture that I want to be remembered by." 

===American Film Institute recognition===
*1998: AFIs 100 Years... 100 Movies #74
*2000: AFIs 100 Years... 100 Laughs #25
*2007: AFIs 100 Years... 100 Movies (10th Anniversary Edition) #58

==1942 re-release==
In 1942, Chaplin released a new version of The Gold Rush, modifying the original silent 1925 film by adding a recorded musical score, adding a narration which he recorded himself, and tightening the editing, which reduced the films running time by several minutes.    The film was also shortened by being run at "sound speed", i.e. 24 frames per second; like most silent movies, it was originally shot and exhibited at a slower speed. As noted above, Chaplin also changed some plot points. Besides removing the kiss at the end, another change eliminated a subplot in which the Lone Prospector is tricked into believing Georgia is in love with him by Georgias paramour, Jack.

The new music score by Max Terr and the sound recording by James L. Fields were nominated for Academy Awards in 15th Academy Awards|1943.   

The Gold Rush was the first of Chaplins classic silent films that he converted to a sound version in this fashion.  As revealed in the 2003 DVD release, the reissue of The Gold Rush also served to preserve most of the footage from the original film, as even the restored DVD print of the 1925 original shows noticeable degradation of image and missing frames, artifacts not in evidence in the 1942 version.

==Copyright and home video== public domain in the US, as Chaplin did not renew its copyright registration in the 28th year after publication in accordance with American law at the time.   As such, the film was once widely available on home video in the US. In the years since, Chaplins estate has blocked the unauthorized releases of The Gold Rush in the United States by arguing that under URAA/GATT, the film remains under copyright in Great Britain. 

MK2 Editions and Warner Home Video (Turner Entertainment Co.) currently hold DVD distribution rights. In 2012, both the reconstruction of the 1925 silent version and the 1942 narrated reissue version were released in both Blu-ray and DVD by the Criterion Collection. This set included a new audio commentary track by Chaplin biographer and scholar Jeffrey Vance. 

==In popular culture== Bande à Grampa Simpson The Muppets.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 