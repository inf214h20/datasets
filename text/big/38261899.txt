Via Mala (1945 film)
{{Infobox film
| name           = Via Mala
| image          =
| image_size     =
| caption        =
| director       = Josef von Báky 
| producer       = 
| writer         =  John Knittel (novel)   Thea von Harbou
| narrator       =
| starring       = Karin Hardt  Carl Wery   Viktor Staal   Hilde Körber
| music          = Georg Haentzschel
| cinematography = Carl Hoffmann Wolfgang Becker
| studio         = Universum Film AG
| distributor    =  Universum Film AG
| released       = 7 April 1945
| runtime        = 108 minutes
| country        = Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Via Mala unconditional surrender of Germany.  The film is visually German expressionism|expressionist, something comparatively rare during the Nazi era.

==Synopsis==
In a rural village, the tyrannical Jonas Lauretz intimidates his family, mistress and neighbours. After he disappears one night, it is widely believed that his eldest daughter, Silvelie, has murdered him. A new investigating judge arrives in the village, he falls in love with Silvelie. He becomes torn between his love for her and his duty to investigate the potential crime. Eventually it emerges that it was not Silvelie who murdered Jonas Lauretz but the village innkeeper Bündner. He is forgiven by everyone because they all shared his desire to murder him. OBrien p.233 

==Production== Minister of on location in Mayrhofen in Tyrol (state)|Tyrol. A variety of further delays meant that the finished film was not ready until March 1944. This was still deemed unsatisfactory, and several scenes were re-shot. The ending moved away from that of the novel, where all family members except Silvelie were revealed to have taken part in the murder.  It was finally submitted to the censor in January 1945. 

==Release== escapist films were preferred. It was finally agreed that the film could go on general release only outside Germany. 

The film premièred in Mayrhofen on 7 April 1945. It was not released in Germany until 16 January 1948, when it had its première in East Berlin. 

==Cast==
* Karin Hardt as Silvelie 
* Carl Wery as Jonas Lauretz 
* Viktor Staal as Andreas von Richenau 
* Hilde Körber as Hanna 
* Hildegard Grethe as Frau Lauretz 
* Albert Florath as Der Amtmann
* Ferdinand Asper as Gast bei Bündner 
* Karl Hellmer as Jöry 
* Malte Jäger as Nikolaus 
* Carl Kuhlmann as Lukas Bündner 
* Ludwig Linkmann as Der Amtsdiener 
* Renate Mannhardt as Kuni 
* Heinz Günther Puhlmann as Angestellter von Bündner 
* F.W. Schröder-Schrom as Gast bei Bündner 
* Franz Lichtenauer as Gast bei Bündner  Klaus Pohl as Gast bei Bündner 
* Georg Vogelsang as Lechner-Bauer 
* Walter Werner as Dorfarzt

==References==
 

==Bibliography==
* OBrien, Mary-Elizabeth. Nazi Cinema as Enchantment. The Politics of Entertainment in the Third Reich. Camden House, 2006.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 