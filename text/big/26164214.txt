The Girl in Black Stockings
{{Infobox Film
| name           = The Girl in Black Stockings
| image          = Thegirlinblackstockings.jpg
| caption        = 
| director       = Howard W. Koch
| producer       = Aubrey Schenck
| writer         = Richard H. Landau Peter Godfrey
| starring       = Lex Barker Anne Bancroft Mamie Van Doren
| music          = Les Baxter
| cinematography = William Margulies
| editing        = John F. Schreyer
| distributor    = United Artists
| released       = September 24, 1957 (United States)
| runtime        = 73 minutes
| country        = United States English
}} 1957 B-movie mystery film starring Lex Barker, Anne Bancroft and Mamie Van Doren.

==Plot==

A lodge in Kanab, Utah is where Los Angeles lawyer David Hewson goes for a peaceful vacation. He quickly is attracted to Beth Dixon, a switchboard operator and a former personal assistant to lodge owner Edmund Parry.

The murder of playgirl Marsha Morgan, her throat cut, disrupts the peace and quiet. Sheriff Holmes begins the investigation, starting with the wheelchair-bound Parry, who admits to hating the dead woman, and Parrys possessive sister Julia, who helps him run the lodge. It turns out David once dated Morgan as well.

A new guest, Joseph Felton, checks in. The sheriffs suspects also include guests Norman Grant, a drunken actor, and his ambitious girlfriend, Harriet Ames. A missing kitchen knife believed to be the murder weapon is found by Indian Joe, who works at the lodge.

Beth eavesdrops on a phone call Felton makes from his room. Felton is later found killed by a gunshot, and it turns out he was a private detective. David becomes more and more convinced that the Parrys are behind all this. Ames is seen kissing Edmund Parry, which doesnt please Edmunds sister or Grant.

To his shock, David arrives as Beth holds a knife to Julia Parrys bloody throat, claiming to have stabbed her in self-defense. It turns out, however, that Edmund had hired the investigator Felton to follow the psychologically disturbed Beth, who is responsible for all the murders.

==Cast==

*Lex Barker as David Hewson
*Anne Bancroft as Beth Dixon
*Mamie Van Doren as Harriet Ames
*Ron Randell as Edmund Parry
*Marie Windsor as Julia Parry
*John Dehner as Sheriff Jess Holmes John Holland as Norman Grant
*Diana Van der Vlis as Louise Miles
*Richard Cutting as Dr. John Aitkin
*Larry Chance as Indian Joe
*Gene ODonnell as Joseph Felton
*Gerald Frank as Frankie Pierce
*Karl MacDonald as Deputy Fred
*Norman Leavitt as Amos
*Stuart Whitman as Prentiss
*Dan Blocker as bartender

==Trivia==

This film was mainly shown at drive-in theatres where it was a hit with audiences, particularly Mamie Van Doren fans.  

The motel used for the movie was really called the Parry Lodge and was situated in Kanab, Utah. In the early 1930s, the Parry brothers opened the lodge to allow Hollywood crew members who were filming in the area, a place to stay. In time, many famous movie stars have stayed at this motel.  

The movie was filmed in 1956, but released almost a year later in September 1957.

==References==
 

==External links==
*  

 
 
 