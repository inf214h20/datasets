Nella città l'inferno
{{Infobox film
| name           = Nella città linferno
| image          =Nella città linferno.jpg
| image size     =
| caption        =
| director       = Renato Castellani
| producer       =
| writer         = Isa Mari ("Roma, Via delle Mantellate," novel) Renato Castellani (adaptation)
| narrator       =
| starring       = Anna Magnani Giulietta Masina
| music          =Roman Vlad
| cinematography = 	Leonida Barboni
| editing        =
| distributor    =
| released       = 1959
| runtime        = 106 min.
| country        = Italy Italian
| budget         =
| preceded by    =
| followed by    =
}}
Nella città linferno (Behind Closed Shutters) is a 1959 Italian film directed by Renato Castellani.

==Plot==
Egle (Anna Magnani) is an inmate of a female jail. Lina (Giulietta Masina) is a nervous newcomer who is much more temperate, with some naiveness even. Egle influences Lina and she convinces her so, successfully, Lina extorts the man who had incriminated her (Antonio Zampi detto Adone, by Alberto Sordi). Lina is released out of the prison thus. However, still in jail, Egle meets Lina again as she has been detained by exerting prostitution, with a marginal demeanor which is due to Egles previous influence on her.

== Cast ==
*Anna Magnani: Egle
*Giulietta Masina: Lina
*Myriam Bru: Vittorina 
*Cristina Gajoni: Marietta Mugnari
*Renato Salvatori: Piero
*Alberto Sordi: Antonio Zampi, detto Adone
*Angela Portaluri: Laura
*Milly Monti: Sister Giuseppina
*Maria Virginia Benati: Vera 
*Marcella Rovena: Miss Luisa
*Gina Rovere: Delia 
*Miranda Campa:  Ida Maroni
*Saro Urzì: Maresciallo 
*Sergio Fantoni: Judge
*Umberto Spadaro: Director of the prison

== External links ==
* 

 
 
 
 
 
 
 
 
 

 