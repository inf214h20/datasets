Bond Street (film)
{{Infobox film
| name           = Bond Street
| image          = "Bond_Street"_(film).jpg
| image_size     = 
| caption        =  Gordon Parry
| producer       = Anatole de Grunwald	
| writer         = Terrence Rattigan  Rodney Ackland Anatole de Grunwald	
| narrator       = 
| starring       = Jean Kent
| music          = Benjamin Frankel
| cinematography = Otto Heller Bryan Langley
| editing        = Gerald Turney-Smith
| studio         = De Grunwald Productions for  Associated British Picture Corporation	
| distributor    = Associated British-Pathé (UK)
| released       = 25 May 1948 (London) (UK)
| runtime        = 109 minutes
| country        = United Kingdom
| language       = English 
| budget         = 
| gross = £155,312 (UK) 
| preceded_by    = 
| followed_by    = 
|
}} portmanteau drama Gordon Parry and based on a story by Terence Rattigan. It stars Jean Kent, Roland Young, Kathleen Harrison and Derek Farr.  The film depicts a brides dress, veil, pearls and flowers purchased in Londons Bond Street - and the secret story behind each item. 

==Cast==
* Jean Kent as Ricki Merritt 
* Roland Young as George Chester-Barrett 
* Kathleen Harrison as Ethel Brawn 
* Derek Farr as Joe Marsh 
* Hazel Court as Julia Chester-Barrett  Ronald Howard as Steve Winter 
* Paula Valenska as Elsa 
* Patricia Plunkett as Mary Phillips 
* Robert Flemyng as Frank Moody 
* Adrianne Allen as Mrs. Taverner 
* Kenneth Griffith as Len Phillips 
* Joan Dowling as Norma 
* Charles Goldner as Waiter 
* James McKechnie as Inspector Yarrow 
* Leslie Dwyer as Barman
* Aubrey Mallalieu as Parkins
* Darcy Conyers as Bank Clerk

==Critical reception==
*Britmovie called the film an "entertaining portmanteau comedy-drama charting the events occurring during a typical 24-hour period on London’s thoroughfare Bond Street. Linking the four stories together is the impending wedding of society girl Hazel Court and Robert Flemyng. Producer Anatole de Grunwald and co-writer Terence Rattigan would later revisit the formula for Anthony Asquith’s The V.I.P.s (1963) and The Yellow Rolls-Royce (1964)."  
*The New York Times called the film "an entertainment grab bag, which, in this case, means that some of the parts are better than the whole...But this spectators favorite Bond Stree interlude is the final chapter, concerning a bouquet and an old flame who turns up at an inopportune time to claim the groom as her own. Roland Young is vastly amusing as the droll father of the prospective bride...Bond Street is fresh enough to have a certain amount of novelty appeal which helps to compensate for the inconsistencies of its dramatic construction. It may not be in a class with Quartet (1948 film)| Quartet, a handy point of reference, but the new film can stand on its own merits with any audience that is willing to accept half a loaf."  

==References==
 

 
 

 
 
 
 
 
 
 
 
 


 