Eddie Murphy Raw
 
 
{{Infobox film
| name = Eddie Murphy Raw
| image = Eddiemurphyrawposter.jpg
| caption = Theatrical release poster Robert Townsend
| producer = Robert D. Wachs Keenen Ivory Wayans
| writer = Eddie Murphy   Eddie Murphy Keenen Ivory Wayans  
| starring = Eddie Murphy
| cinematography = Ernest Dickerson
| editing = Lisa Day
| studio = Eddie Murphy Productions
| distributor = Paramount Pictures
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| gross = $50,504,655 
}}
 Robert Townsend and starring Eddie Murphy. It was Murphys second feature stand-up film, following Eddie Murphy Delirious. However, unlike Delirious, Raw received a wide theatrical release. The 93-minute show was filmed in New York Citys Felt Forum, a venue in the Madison Square Garden complex.

==Humor==
After the initial sketch and into the live show itself Murphy begins by saying how various people he referenced responded to their mention in his previous stand-up show, Eddie Murphy Delirious|Delirious, specifically Mr. T and Michael Jackson.  Murphy also talks about a phone call he received from Bill Cosby in which he said that his son, Ennis, heard his profanity and requested that Murphy not say the word "fuck" on stage again, and began chastising him for his offensive material. Murphy then went on to say that he called Richard Pryor and told him about what Cosby said.  Pryor responded angrily, calling Cosby a "Jell-O Puddin eatin motherfucka", and that if Murphys fans think it was funny, then Cosby has no business telling him how to do his act. Murphy also talks about how Pryor was an inspiration to Murphy. He talks about how he was inspired by Pryor, whom he called "raw". Murphy then went into one of his old jokes, a bit about a flushing failure, while imitating Pryors voice. He then goes on to talk about how people who dont speak English only picking up the curse words in his act, and repeating them back to him.

His next bit is about STDs, which Murphy then segues into jokes about relationships; notably of the practice that developed during the 1980s of wives divorcing their husbands and taking "HALF!", as well as about the faults of both men and women and how the opposite sex exploit those weaknesses, basing it on the message in the song "What Have You Done for Me Lately". He jokes that he intends to go deep into Africa to find a "bush bitch" who has no concept of Western culture in order to get a wife who (he hopes) will not divorce him. He also makes passing references to Japanese women who are supposedly very obedient to their husbands.

Murphy delivers a well-received segment on Italian-Americans, on their stereotypical behavior, especially how they behave after seeing a Rocky movie. He then talks about white peoples apparent inability to dance. He later talks about his experience in a nightclub where an Italian-American started a fight with Murphy, causing a large brawl, the end result of which saw Murphy getting sued for millions of dollars by all involved and even a few who were not involved.

Murphy also talks about his rude mother making him "the biggest homemade onion-and-green-pepper hamburger that was shaped like a meatball on Wonder Bread", and an exaggerated account on how poor his family was, eventually going into a routine impersonating his father drunk, which follows on from the nightclub brawl segment.

==Records== highest "fuck social norms toward profanity relax. 

==Cast (opening segment)==
*Eddie Murphy - Himself
*Tatyana Ali - Eddies sister (sketch)
*Deon Richmond - Little Eddie (sketch)
*Billie Allen - Eddies aunt (sketch)
*James Brown III - Thanksgiving guest
*Edye Byrde - Mrs. Butts (sketch)
*Michelle Davison - Thanksgiving guest
*Clebert Ford - Uncle Lester (sketch)
*Birdie M. Hale - Aunt Rose (sketch)
*J. D. Hall - Party guest (sketch)
*Tiger Haynes - Card player #3
*Barbara Iley - Thanksgiving guest Leonard Jackson - Uncle Gus (sketch)
*Samuel L. Jackson - Eddies Uncle
*John Lafayette - Thanksgiving guest
*Davenia McFadden - Eddies Aunt (sketch)
*Gwen McGee - Eddies mother (sketch)
*Lex Monson - Card player #4
*Warren Morris - Poetry reader
*Basil Wallace - Eddies Father (sketch) Damien Wayans - Child running in the house
*Ellis E. Williams - Eddies uncle (sketch) Carol Woods - Eddies Aunt
*Kim Wayans - Interviewed fan (uncredited)

==Reception==

The film was met with critical acclaim, with many critics praising Eddie Murphys stand-up routine. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 