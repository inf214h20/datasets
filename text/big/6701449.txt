Padayappa
 
 
 
{{Infobox film
| name = Padayappa
| image = Padayappa.jpg
| border = no
| caption = Theatrical release poster
| director = K. S. Ravikumar
| writer = K. S. Ravikumar
| starring = Sivaji Ganesan Rajinikanth Ramya Krishnan Soundarya
| producer = K. Sathya Narayana M. V. Krishna Rao H. Vittal Prasad P. L. Thenappan
| music = A. R. Rahman
| studio = Arunachala Cine Creations
| distributor =
| cinematography = S. Murthy Prasad
| editing = K. Thanigachalam
| released =  
| runtime = 192 minutes
| country = India
| language = Tamil
| budget = 
| gross = 
}} Tamil drama drama film background score were composed by A. R. Rahman.

The plot revolves around Padayappa (Rajinikanth), a mechanical engineer whose father (Sivaji Ganesan) gives up his property to his foster brother (Manivannan), and then dies of shock soon after. Neelambari (Ramya Krishnan) initially loves Padayappa, but plans to humiliate him after his family humiliates her father (Radha Ravi). The rest of the plot deals with Padayappa overcoming all the obstacles placed by Neelambari.
 Tamil New prints and Filmfare Award Best Actress Telugu under the title Narasimha. The dubbed version was also commercially successful, and had a theatrical run of 50 days at 49 theatres.

== Plot ==
Padayappa is a mechanical engineer who returns to his village from Chennai to attend his sisters wedding. His sister is engaged to Suryaprakash, the son of their maternal uncle. During his stay, he comes across Vasundhara, and falls in love with her. However, shyness and fear of Neelambari, her landlady, prevents Vasundhara from initially expressing her feelings. In addition, Neelambari is Suryaprakashs spoiled sister, who is madly in love with Padayappa. 

Unexpectedly, Padayappas fathers foster brother demands a share in the family property. Padayappas father, the chieftain of the village, follows the customs of his forefathers and refuses to divide the property and instead gives the entire property to his foster brother. This forces Padayappas family to leave their home. Unable to bear this shock, Padayappas father dies. Suryaprakash then cancels his wedding with Padayappas sister, decides and marries the daughter of Padayappas fathers foster brother, who now owns the property of Padayappas father. 

Meanwhile, Padayappa discovers that a hill on his property is solid granite, which allows him to start a granite business, from which he becomes rich. He uses the money to help the poor in his village, and provide them jobs. As his business flourishes, his family is able to once again settle down. Padayappa assumes his fathers position as village chieftain, and his sister gets married to one of the engineers who work in his company. When Neelambari learns about Padayappas love for Vasundhara, she becomes jealous of her, and her parents beg Padayappas widowed mother to allow Neelambari to marry him as padayappa is the richest than before now. However, to everyones surprise, Padayappas mother embarrasses Suryaprakash in front of the entire village when she agrees to a marriage proposal made by Vasundharas mother, her brothers servant. This was in retaliation for Suryaprakashs humiliation of her after Padayappas father death. Unable to bear the humiliation, Neelambaris father commits suicide. When Neelambari tries to kill Vasundhara by letting a bull loose on her, Padayappa saves her, after which the two marry. Subsequent to the wedding, Neelambari locks herself in a room in Suryaprakashs house, thinking only about Padayappa for 18 years. In between, Padayappa finds his fathers foster brother in trouble and was pulled out by a financier, from whom he had borrowed money on interest keeping the family house, and helps his fathers foster brother, who is suffering financially. As a result, Padayappas fathers foster brother becomes indebted to him and seeks Padayappas pardon for his misdeeds. Padayappa forgives him.

Neelambari plans her revenge on Padayappa, now a father of two daughters. Suryaprakash also has a son, Chandraprakash alias Chandru, who studies at the same college as Padayappas elder daughter, Anitha. Chandru is advised by Neelambari to make Anitha fall in love with him. At the same time, Padayappa plans to get Anitha married to his sisters son. Neelambari, having made Chandru pretend to fall in love with Anitha, plans to humiliate Padayappa by making Anitha say that she does not wish to marry her parents choice for her, and that she is in love with someone else. At the marriage ceremony, after Anitha does what Neelambari told her to do, Padayappa then makes an oath to unite Anitha with her lover by the next Muhurta day, or commit suicide. Padayappa discovers that Chandru really did fall in love with Anitha, even though he was only initially pretending to do so, on Neelambaris orders. When Padayappa takes Chandru and Anitha to the temple to get married, Neelambari and Suryaprakash give chase to stop them. During the chase, Suryaprakash is killed in a car accident.

Armed with a gun, Neelambari reaches the temple where Chandru and Anitha are married, and in anger tries to kill Padayappa. However, Padayappa saves her life when he prevents a bull from attacking her, while at the same time dodging the bullets she is firing at him. Rather than live with the humiliation of knowing she was unsuccessful in avenging her fathers death, as well as having her life saved by her enemy, Neelambari commits suicide, promising to take revenge on Padayappa in her next birth. Padayappa prays for her soul to be at peace and eventually attain salvation.

== Cast ==
 
 
* Sivaji Ganesan as Padayappas father
* Rajinikanth as Padayappa
* Ramya Krishnan as Neelambari
* Soundarya as Vasundhara Lakshmi as Padayappas mother Sithara as Padayappas sister
* Radha Ravi as Neelambaris father
* Nassar as Suryaprakash
* Manivannan as Padayappas fathers foster brother Senthil as Azhagesan Abbas as Chandraprakash aka Chandru Preetha as Anitha Anu Mohan as Chinnarasu
* Ramesh Khanna as Padayappas friend
* Vadivukkarasi as Vasundharas mother
* Sathyapriya as Neelambaris mother Mansoor Ali Khan as Krishnasamy Mudaliar
* Prakash Raj in a special appearance as Police Officer Subramaniam
* Raj Bahadur as a politician named Raj Bahadur
* Mohan V. Raman as the lawyer who does the formalities for dividing the property
* K. S. Ravikumar in a special appearance in the song "Kikku Yerudhey"
* Kanal Kannan in a cameo appearance
 

== Production ==

=== Development === FEFSI strike delayed the project. After the strike ended, Ravikumar still had to complete the film he was then working on, Natpukkaga (1998), before he could begin Padayappa. After completing Natpukkaga in June 1998, Padayappa was further delayed when Ravikumar agreed to quickly remake Natpukkaga in Telugu as Sneham Kosam (1999).   
 Aarupadayappa  Lord Murugan six abodes. 

=== Cast and crew ===
{{quote box
| quote = "Todays audience expects novelty from film makers and stars. The success of   in Padayappa is an example of this. Action and reaction are the key factors for an artiste’s success.”
| source =&nbsp;– Ramya Krishnan, in an interview with The Hindu in July 1999. 
| align = right
| width = 30%
}}
The film was produced by Rajinikanths personal assistants, K. Sathya Narayana, M. V. Krishna Rao, and H. Vittal Prasad under their production banner, Arunachala Cine Creations,  along with P. L. Thenappan as co-producer.  Lalitha Mani was the choreographer for the song sequences.  Jyothi Krishna, son of producer A. M. Rathnam, was involved in the development of the films script. 
 Simran and Meena were screened for Shalini was touted to play the role of Padayappas sister,    but the role eventually went to Sithara (actress)|Sithara. 

The look of the older Padayappa — bearded, with sunglasses — is based on Rajinikanths look as the character Manik Baashha in Baashha (1995);  Ganesh Nadar of Rediff commented, "Give   a cheroot and thats Padayappa".  Sivaji Ganesan was cast as the protagonists father; Padayappa was the last film he worked on before his death in 2001,  although Pooparika Varugirom (1999) was his final release.  His characters appearance, with a mutton-chop moustache, is based on a similar role he played in Thevar Magan (1992). 

=== Filming ===
Padayappa launched production at the Ragavendra Kalyana Mandapam on 1 October 1998.  The films climax scene was one of the first to be shot,  and was filmed in one take using two cameras. Around 2,000 extras were used for the scene.  The car that was used in the scene which introduced Neelambari in the film belonged to Ravikumar. Ravikumar used the newly purchased car in the film at Rajinikanths insistence.  Filming also took place in Mysore.  The Vadapalani|Vadapalani-based shop, D. V. Nehru wigs, supplied the wigs which were sported by Ganesan in the film. 

"Kikku Yerudhey" was the last song sequence that was shot. For the sequence, Rajinikanth required Ravikumar to sport an outfit similar to Rajinikanths, and enact a small part in the song. Rajinikanth also selected the part of the song where Ravikumar would make his appearance. After reluctantly agreeing to do the part, a scene was filmed featuring Rajinikanth and Ravikumar. Rajinikanth said he felt the shot did not look right, and re-takes for Ravikumars sequence were done. After the re-takes were completed, Rajinikanth admitted that the first sequence was fine. When Ravikumar asked the cameraman why he had not told him earlier, the cameraman replied by saying that Rajinikanth wanted Ravikumar to do seven takes, in order to teach him a lesson for all the takes that Ravikumar had required of Rajinikanth. 

== Music ==
 
 background score were composed by A. R. Rahman, with lyrics by Vairamuthu. The soundtrack was released through Star Music.  Strips of herbal rejuvenator capsules were sold along with the films music cassettes.  Before the films release, Rahman asked Ravikumar if the soundtrack could be released in August 1999. Ravikumar informed Rahman that he had already discussed a release date with the press, and that Rahman would be blamed for any delay. To make the deadline, Rahman did a live re-recording of both the soundtrack and score to finish them on time. 
 Srinivas as a leading singer in the film industry. 

Singer Charulatha Mani, in her column for The Hindu, "A Ragas journey", called "Minsara Kanna" a "mind-blowing piece".  G. Dhananjayan, in his book, The Best of Tamil Cinema, says the songs are "mass  ", also citing that the songs contributed to the films success.  Srikanth Srinivasa of the Deccan Herald wrote, "The music by  , to Vairamuthu’s lyrics, sounds good while the movie is on, though whether without the presence of   they would have, is another thing."    S. Shiva Kumar of The Times of India was more critical of the soundtrack, and called it "lacklustre".   

== Release == Tamil New Years day.  This was the first Tamil film to be released worldwide without the involvement of distributors.  It was also the first Tamil film to be released worldwide with 210 prints,  and 700,000 audio cassettes.  The films rights in Japan were sold for US$50,000, which was the highest an Indian film fetched for commercial release in 1999.   Co-producer Thenappan registered the film posters as a Class 34 trademark in 1998, to be used for trademarking such items as beedis, cigarettes, cheroots and tobacco, making it the first instance of brand extension in the Tamil film industry.  The pre-release business of the films overseas rights amounted to   30 million. 

=== Critical response ===
Ananda Vikatan, in its original review of the film dated 25 April 1999, wrote, "Original stamp of Rajni style can be seen in the film several times ... Ramya Krishnan has matched Rajni and created a royal path separately ... The film is exclusively made for Rajnis fans ...," and gave the film 41 marks out of 100.  The Deccan Herald gave the film a positive verdict, claiming that the "positive energy generated by this film is simply astounding", and labelling Rajinikanths role as "terrific". 

Ganesh Nadar of Rediff also gave a positive review, praising Ramya Krishnans performance in the film, and said that she "does a fantastic job", concluding, "... if you are a Rajni fan, this film is vintage stuff."   
P. C. Balasubramanianram and N. Ramakrishnan, in their book, Grand Brand Rajini, said, "Padayappa, in one way, stands testimony to Rajinis life itself."  However, S. Shiva Kumar of The Times of India was critical of the films allusions to the actors political career, stating that the film was "more style than substance". 

=== Box office === Business Today says that the film was estimated to have earned   440 million worldwide, with a total of   380 million at the domestic box office, and   60 million overseas. 
 Telugu under the title Narasimha.  The dubbed version was also a commercially successful venture, and had a theatrical run of 50 days in 49 theatres. 

=== Dropped sequel === intervals for the film. Ravikumar was wary of this idea, and went to actor Kamal Haasan for advice, since the initial final cut of Haasans Nayakan (1987 film)|Nayakan (1987) was also 22 reels. Using his prior experience on trimming Nayakan, Haasan was able to cut Padayappa down to 16 reels. When Kumudam s reporter Kannan asked Rajinikanth to release the scenes which had been cut as Padayappa s sequel, Rajinikanth immediately spoke to Ravikumar about the possibility, but was informed that those reels had been destroyed. 

== Accolades == Best Actress Filmfare Awards. The film also won five Tamil Nadu State Film Awards.  

{| class="wikitable" class="wikitable sortable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Ceremony
! Category
! Nominee(s)
! Outcome
|- Filmfare Awards South 46th Filmfare Awards South  Best Actress – Tamil
| Ramya Krishnan
|  
|- Tamil Nadu State Film Awards Tamil Nadu State film Awards - 1968    Best Film (first prize)
| K. S. Ravikumar
|  
|- Best Actor
| Rajinikanth
|  
|- Best Actress (Special Prize)
| Ramya Krishnan
|  
|- Best Male Playback
| Srinivas
|  
|- Best Make-up Artist
| Sundaramoorthy
|  
|}

== Legacy ==
 
With the success of Padayappa, Ramya Krishnan, who up to that point in time had only performed glamorous roles,  showed her versatility as an actress.    The character Neelambari reappears in   (2009), when Krish Malhotra, the novels protagonist, travels to Nungambakkam by auto rickshaw, the auto rickshaw driver stops to worship a poster of Padayappa. 
 2015 film directed by Shaji Kailas. The film stars R. K. (actor)|R. K. and Poonam Kaur in the lead roles.  Scenes from the film were parodied in various other films, notable of which are En Purushan Kuzhandhai Maadhiri (2001),  Annai Kaligambal (2003),  Sivaji (film)|Sivaji (2007).  Vel (film)|Vel (2007),  Siva Manasula Sakthi (2009),  Malai Malai (2009),  Vanakkam Chennai (2013)  and All in All Azhagu Raja (2013).  Padayappa was also parodied in the STAR Vijay comedy series Lollu Sabha, in an episode appropriately named Vadayappa. 
 Minion characters Despicable Me franchise are dressed as Rajinikanth.  The digital art was hand drawn on a digital pad by Gautham Raj.  One of the posters depicted a minion sitting on a swing and dressed like Rajinikanths character in Padayappa, reminiscent of the scene where Padayappa went to Neelambaris house but is not offered a seat even though Neelambari sits. Padayappa then uses his dupatta to pull down a swing from the ceiling, on which he sits.    This swing scene was included by Behindwoods in its list of "Top 20 Mass Scenes".  Behindwoods, in its list named "Tamil Superstars tryst with facial hair", called Padayappas moustache "Regal". 

== References ==
 

== Sources ==
 
*  
*  
*  
* 
*  
*  }}
*  
*  
 

==External links==
*  

 
 
 

 
 
 
 
 
 