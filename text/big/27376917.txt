Cherry Bomb (film)
 
{{Infobox film
| name           = Cherry Bomb
| image          =
| alt            = 
| caption        =
| director       = Kyle Day
| producer       =  
| writer         =  
| starring       =  
| music          = Jason Latimer
| cinematography = Andrew Michael Barrera
| editing        =  
| studio         = Strike Anywhere Productions
| distributor    = Well Go USA
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Cherry Bomb is an independent action revenge film starring Julin Jean and adult film star Nick Manning, in his first leading role in a mainstream film.

==Plot==
Set in 1984 in a small Texas town, Cherry Bomb follows the story of Cherry, an exotic dancer who is sexually assaulted by a group of men in the club where she works.  Upon waking in the hospital, Cherry soon comes to find that all of the men have escaped justice, seemingly due to the help of corrupt local law enforcement. Against all odds, Cherry teams up with her estranged brother and vows revenge against the men who left her emotionally and physically broken. But things dont always go as planned, and Cherry finds herself up against the police, a vicious hired gun, and a barrage of unexpected consequences as she strives for vengeance.

==Production==
The idea for Cherry Bomb began in late 2008 as a collaboration between writer Garrett Hargrove and director Kyle Day. Intended for independent production, the film underwent an extensive pre-production stage which involved many changes involving cast, crew, and union status, which subsequently caused several delays.
 Austin and surrounding areas.

On April 30, 2010, the first official trailer was released on the Cherry Bomb website via YouTube and garnered overwhelmingly positive responses, largely due to social networking via the Cherry Bomb Facebook page  and industry blogs such as Geek Tyrant,  Dread Central,  and Trailer Addict.   Shot in March 2010, the film was released in the United States on 11 June 2011. 

==Cast==
* Julin Jean as Cherry
* Nick Manning as Ian Benedict
* John Gabriel Rodriguez as Brandon
* Allen Hackley as Bull
* Jeremy James Douglas Norton as Adam Berry
* Denise Williamson as Sapphire
* Charlotte Biggs as Punk Rock Polly
* Tony Bottorff as Barry Ford
* Lizabeth Waters as Marilyn Lewis
* Alan Brady as Rick Lewis
* Giovanni Antonello as Officer Tran
* D.J. Morrison	as Detective Whitlock
* Sabrina Jones	as Emerald
* Charissa Jarrett as Nurse Jenny
* Joann Fields as Linda Randall
* Nicole Hutchenson as Sandi
* Amanda Arnold	asPatricia
* LeMarc Johnson as Wally
* Alexandra Hutchenson as Alyssa
* Aaron Alexander as Ed Randall
* Grayce Benesh	as Rhoxxie
* Mysteria Black as Luscious
* Salvador Perez as Mike the Bouncer
* Andrew Henczak as Fred Jacobs
* Alexa Hanse as Rita
* Craig Welborn	as Cage
* Bryan Modzelewski as Strip Club DJ
* Conor Nobles as Doug Baggins
* Caitlyn Hutson as Ginger
* Debbie Day as Secretary
* Chad Sitzmann	as Dr. Rocky Von Stromboli III
* Dave Buckman as Pete Finn
* Trey Huguley as Isaac
* Marcelo Beduschi as Andre the Bouncer

== Awards ==
* Portland Underground Film Festival - Official Selection  
* Splatterfest - Official Selection  

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 