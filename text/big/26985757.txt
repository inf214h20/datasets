Chatroom (film)
 
 
 
{{Infobox film
| name           = Chatroom
| image          = Chatroom.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Hideo Nakata
| producer       = {{Plainlist|
* Laura Hastings-Smith
* Alison Owen}}
| writer         = Enda Walsh
| starring       = {{Plainlist| Aaron Johnson
* Hannah Murray
* Imogen Poots Matthew Beard
* Daniel Kaluuya}}
| music          = Kenji Kawai
| cinematography = Benoît Delhomme
| editing        = Masahiro Hirakubo
| studio         = Ruby Films
| distributor    = Pathé
| released       =  
| runtime        = 97 minutes  
| country        = United Kingdom
| language       = English
}} drama Thriller thriller film directed by Hideo Nakata  about five teenagers who meet on the internet and encourage each others bad behaviour. The film is based on the play Chatroom by Enda Walsh. 

==Plot== Aaron Johnson) Matthew Beard), another kid; Eva (Imogen Poots), a model; Emily (Hannah Murray), a goody two-shoes; and Mo (Daniel Kaluuya), a normal kid. There is no real subject matter in "Chelsea Teens!" which instead focuses on the lives of each teen as they talk. Even though they only really communicate through text, the film depicts them in an old hotel-like room and actually having contact. 
 pedophile because he is attracted to his friends prepubescent sister, Keisha (Rebecca McLintock). William sees it to himself to help them in a crude manner. He Photoshops embarrassing pictures of Evas co-worker and posts them online. He convinces Jim to flush down his anti-depressants to make himself feel more relaxed and to reveal his face behind the depressants, his true identity. He tells Emily to do some dirty work, teaming up with Eva. They come up with ways in which Emily could be more violent and make it look like somebody is harassing her family, which makes her parents try to protect her more. He tells Mo to tell his friend Si the truth but this backfires when Si calls him a pervert and attacks him.

William becomes darker and more menacing and even begins to watch people commit suicide. He then takes it upon himself to make Jim commit suicide. His plans are halted though when his computer and phone are taken away from him by his father, who when looking through Williams computer, finds one of the suicide videos. William gets his backup computer and phone and goes after Jim, who meets up with him at London Zoo. Mo and the others find out about Williams intended actions and go to stop him, meeting up in person and trying to follow William and Jim around London.

Jim makes it to the zoo first but decides to not do it. He tries to leave but William goes right after him. William catches Jim, but he refuses to shoot himself and throws the gun to the floor. When William gets it and comes back, Eva punches him and the rest of the crew comes, followed by the police. William tries to escape but is only able to climb up some crates. He then jumps in front of the speeding train behind the crates and is killed. The teens leave without talking to each other; Williams account is shut down and the credits roll.

==Cast== Aaron Johnson as William Collins
* Hannah Murray as Emily
* Imogen Poots as Eva Matthew Beard as Jim
* Daniel Kaluuya as Mo
* Megan Dodds as Grace Collins
* Michelle Fairley as Rosie
* Nicholas Gleaves as Paul Collins
* Jacob Anderson as Si
* Richard Madden as Ripley Collins
* Dorothy Atkinson as Emilys mum
* Rebecca McLintock as Keisha

==Production==
Principal photography took place in early 2010 at Shepperton Studios in Shepperton, Surrey,  with some outdoor scenes shot in Camden and Primrose Hill. The film is based on a screenplay by Enda Walsh, who wrote the stage play of the same name. 

==Release==
The film premiered in the Un Certain Regard section at the 2010 Cannes Film Festival.    The theatrical release was in late 2010.  It premiered in France in late summer 2010.  In September 2010, the film acquired a British distributor. Revolver Entertainment also planned a special online marketing campaign for the film. 

===Critical reception===
Chatroom received largely negative reviews. It currently holds a 9% rating on review aggregator website Rotten Tomatoes based on 22 reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 