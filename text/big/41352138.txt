Gory Gory Hallelujah
{{Infobox film
| image          =
| alt            =
| caption        =
| director       = Sue Corcoran
| producer       = Leslie Rugaber
| screenplay     = Angie Louise
| starring       = Tim Gouran Angie Louise Jeff Gilbert Todd Licea Keith Winsted Jason Collins Joseph Franklin
| music          = Bruce Munroe
| cinematography = A.K. Rosencrans
| editing        = Sue Corcoran Angie Louise
| studio         = Von Piglet Sisters
| distributor    = Indican Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $12,604   
}}
Gory Gory Hallelujah is a 2003 American comedy horror musical film directed by Sue Corcoran, written by Angie Louise, and starring Tim Gouran, Angie Louise, Jeff Gilbert, Todd Licea, Keith Winsted, Jason Collins, and Joseph Franklin.  On a road trip, a group of actors confronts Elvis impersonators, religious extremists, and zombies.

== Plot ==
After they each fail their audition for Jesus in a play, a Jew, a militant African American, a feminist, and a bisexual hippie go on a road trip.  On the way, they confront hostile Elvis impersonators and end up in a fight.  Fleeing the law, they end up in an intolerant religious community that holds the secret of the apocalypse; a botched magical spell reveals it to be a zombie apocalypse.  Zombies kill everyone but Jessie, who lives among them and decides to study their culture.

== Cast ==
* Tim Gouran as Sky
* Angie Louise as Jessie
* Jeff Gilbert as Rahim
* Todd Licea as Joshua
* Keith Winsted as Preacher John
* Jason Collins as Ralph Peed
* Joseph Franklin as Mo Jack

== Release ==
Gory Gory Hallelujah had a limited release in January 2005 and made $12,604.  It was released on DVD October 30, 2006. 

== Reception ==
Dennis Harvey of Variety (magazine)|Variety called the film "heavy-handed and devoid of wit".   Ashley Cooper of Film Threat rated the film 3/5 stars and called it a refreshing B movie that doesnt take itself seriously.   Bill Gibron of DVD Talk rated the film 4.5/5 stars and wrote, "Though its smartly realized narrative kind of falls apart toward the end, and its breakneck pacing means that much of the subtleties get lost in the chaos, Gory Gory Hallelujah is still one exciting, engaging film."   Peter Dendle called it a lackluster independent film with "a few painful song-and-dance numbers". 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 