Un giorno speciale
{{Infobox film
| name           = Un giorno speciale
| image          = Un-giorno-speciale.jpg
| caption        = Film poster
| director       = Francesca Comencini
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = Luca Bigazzi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
Un giorno speciale (also known as A Special Day) is a 2012 Italian drama film directed by Francesca Comencini. The film was selected to compete for the Golden Lion at the 69th Venice International Film Festival.   

==Cast==
* Filippo Scicchitano as Marco
* Giulia Valentini as Gina
* Roberto Infascelli as Autista deposito
* Antonio Zavatteri as On. Balestra (as Antonio Giancarlo Zavatteri)
* Daniela Del Priore as Marta
* Rocco Miglionicco as Rocco

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 