Voyage with Jacob
{{Infobox film
| name           = Voyage with Jacob
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Pál Gábor
| producer       = 
| writer         = István Császár Pál Gábor
| narrator       = 
| starring       = Péter Huszti
| music          = 
| cinematography = János Kende
| editing        = Éva Kármentő
| studio         = 
| distributor    = 
| released       = 26 October 1972
| runtime        = 88 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Hungarian film directed by Pál Gábor. It was released in 1972.

==Cast==
* Péter Huszti - Fényes István
* Ion Bog - Jakab
* Éva Szabó - Kata
* Iván Szendrő - Lépes Feri
* Györgyi Andai - Emese
* Erika Bodnár - Eszter
* Ildikó Bánsági - Ildikó
* Marianna Moór
* Gabriella Borbás
* Kati Lázár
* Ágnes Hegedűs - Lépes anyja
* Eszter Vörös
* Árpád Gyenge - Tanár
* János Körmendi - Főnök
* Ferenc Paláncz - Lajos, a csapos

==External links==
*  

 
 
 
 
 
 