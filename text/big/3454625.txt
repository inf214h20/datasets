The Sleeping Dictionary
{{Infobox film
| name           = The Sleeping Dictionary
| image          = The_sleeping_dictionary_poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Guy Jenkin
| producer       = {{Plainlist|
* Simon Bosanquet
* Jimmy Mulville
* Denise ODonoghue
}}
| writer         = Guy Jenkin
| starring       = {{Plainlist|
* Hugh Dancy
* Jessica Alba
* Brenda Blethyn
* Emily Mortimer
* Bob Hoskins
}}
| music          = Simon Boswell
| cinematography = Martin Fuhrer
| editing        = Lesley Walker
| studio         = Fine Line Features
| distributor    = Fine Line Features
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic drama film written and directed by Guy Jenkin and starring Hugh Dancy, Jessica Alba, Brenda Blethyn, Emily Mortimer, and Bob Hoskins. The film is about a young Englishman who is sent to Sarawak in the 1930s to become part of the British colonial government. There he encounters some unorthodox local traditions, and finds himself faced with tough decisions of the heart involving a beautiful young local woman who becomes the object of his affections.    The Sleeping Dictionary was filmed on location in Sarawak, Malaysia.   

==Plot== Iban society. There he meets his boss Henry Bullard (Bob Hoskins) and his wife Aggie Bullard (Brenda Blethyn). John tries to civilize them, building schools and providing education for the Iban people. He is met with unfamiliar local customs. Selima (Jessica Alba) becomes his "sleeping dictionary", who sleeps with him and teaches him the language and the habits of the locals.

John is sent up river where a sickness is affecting the Yakata tribe. He and Selima travel inland. John witnesses a nearby mining operation run by Europeans. He notices that the Yakata have rice – which has been given to them by the miners – and he guesses correctly that the miners have poisoned the rice in order to get rid of the Yakata. Knowing that they will exact vengeance, John tells the Yakata what has happened. The Yakata wipe out the miners.

Despite their intents, the two find themselves falling into a forbidden love. John is eager to marry Selima despite the longhouse not allowing it. When John tells Henry about his plans to marry her, they lock Selima up. Selima then agrees to marry in the longhouse and they part ways. Bullard threatens to send him to trial for the death of the European miners. He makes a deal with John. John has to give up Selima, and go to Britain for a years vacation and to meet the Bullards daughter Cecilia. Another local British official, Neville Shipperly (Noah Taylor), a boorish drunk and a man who despises the locals, is jealous of John because he had planned to win Cecilia as his own.

A year later, John is seen marrying Cecilia. He still struggles to get over his past with his sleeping dictionary. With Cecilia, he decides the best thing to do is go back to Sarawak to continue his work there. Returning to Sarawak, Cecilia notices Johns desire for Selima with his constant distance from her. Cecilia demands to know more about Selima and John replies by saying that she is married to Belansai and that the couple have a baby together.

While at the lake collecting rocks for research, John sees Selima with a baby. He believes the child to be his and asks Famous to arrange a meeting with the pair. Soon back at the house, Selima walks in, unaware that John is there. John begs to see his son and soon Selima walks away not before John can stop them. Here, John meets his son Manda for the first time. When Belansai hears news that John is spending time with his wife, he sneaks in to try to kill John but only manages to hurt him with a razor. The next morning, Henry reveals to John his past about his own sleeping dictionary, which resulted in the birth of another child: Selima. When Belansai is caught for trying to kill an officer, he is sentenced to be hanged. Selima is not happy that Belansai will be killed as hes been a good father to Manda. Not wanting to kill Belansai, a friend of his, John goes through with announcing Belansais hanging as he had no other option. Later that night, Selima tries to break Belansai out, not knowing John is already there. When she walks over to the jail cell, she sees John breaking Belansai out and handing him a gun. As Belansai escapes, John asks Selima to meet him at the dock so they can escape on the boat. Selima tells him he wont come as theyll catch him. John turns to Selima and says "Then Ill tell them Id rather have you than a country... or a language... or a history". They embrace as the rain is pouring behind them.

The next day, since the people of the Longhouse have turned on Selima, she is forced to become the sleeping dictionary for Neville. Later Cecilia announces she is pregnant, shocking John. That night, Selima bashes Neville on the head, knocking him out, because he has attempted to attack her and force her. She grabs the baby and runs from the house, heading for the docks. Although John still has plans to be with Selima and their son, he begins writing a note but stops as Cecilia catches him. The couple then talk about Johns love for Selima and how Cecilia wants John to be happy. Aggie is not happy that Cecilia and Henry have allowed both John and Selima to run away together because she never left Henrys sight, fearing hed go with his sleeping dictionary. She encourages Neville to go after them.

With the help of Famous and the Yakata, John searches for Selima as shes left believing that John didnt come to the place of arrangement. They reunite as Neville comes through with a gun. He tells them to cuff themselves around the bamboos and tells them of his plans to kill John, Selima and their baby. Theyre then rescued by the Yakata, who kill Neville.

At the end, they decide to live together and migrate with the Yakata.

==Cast==
* Hugh Dancy as John Truscott
* Jessica Alba as Selima
* Brenda Blethyn as Aggie Bullard
* Emily Mortimer as Cecilia Bullard
* Bob Hoskins as Henry Bullard
* Christopher Ling Lee Ian as Jasmine
* Junix Inocian as Famous
* Michael Jessing Langgi as Melaka
* Mano Maniam as Policeman
* K.K. Moggie as Tipong
* Cicilia Anak Richard as Jester Woman
* Malcolm Rogers as Vicar
* Eugene Salleh as Belansai
* Noah Taylor as Neville Shipperly
* Kate Helen White as Mandar
* Prang as Famous Monkey

==Production==

===Screenplay===
Guy Jenkins, the writer and director, mentioned that he created the screenplay after a back-packing trip he made to Borneo in the early 1980s and became enamoured of the concept of ngayap which was the Iban way of courtship practised in the early 1920s and 30s. He combined the story with the romanticised idea of young Britons being posted to jungle outposts and being "thrown in the deep end" when they had to learn the local language in express time. And so a human "sleeping dictionary" was allocated to each of them.

===Filming locations===
The Sleeping Dictionary was filmed on location in Sarawak, Malaysia, within an hours drive from the state capital of Kuching.  The secondary location of the longhouse was specially constructed at great expense (RM125,000) at Batang Ai, about 15 minutes drive on the secondary road to the Hilton Batang Ai Resort, where the film cast and crew were based for 10 days. Kuching locations included the Matang Recreation Park; Buntal fishing village and many smaller villages and country homesteads. The local crew and cast numbered up to 600 on certain days, for a few crowd scenes.

===Production companies===
The local Malaysian film production company for this feature was Southeast Asia Film Locations Services, whose Sri Lankan partner Chandran Rutnam had brought Steven Spielberg to Colombo to shoot Indiana Jones and the Temple of Doom. His Malaysian partner Edgar Ong was involved in the first major Hollywood feature made in Sarawak, Farewell to the King, in 1987. He later produced Sacred Planet for Disney/IMAX which was narrated by Robert Redford.

==Historical inaccuracies==
This film was set when the third British White Rajah; Charles Vyner Brooke, was on the throne in Sarawak. Sarawak was a British Protectorate at this time, not a British colony as stated in the film.  Sarawak became a British Colony after the Second World War when the Third Rajah abdicated.

==Release==
A few countries received a theatrical release of the movie, including Thailand, Indonesia, Canada, Japan and parts of Europe and the Americas. Elsewhere it went straight to DVD release, where it was awarded the first-time ever DVD Award (for direct-to-DVD films only) for Best Actor (Bob Hoskins). Hoskins was not the first choice, as both Michael Caine and then Tom Wilkinson had turned down the role.
 Dark Angel TV series, created and directed by James Cameron.

Another cast member and Oscar nominee was Brenda Blethyn. Emily Mortimer and Hugh Dancy went on to larger roles in more prominent movies.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 