Perón: Apuntes para una biografía
{{Infobox film
| name           = Perón: Apuntes para una biografía
| image          = Apuntes para una biografía.jpg
| alt            = 
| caption        = 
| director       = Jorge Coscia
| producer       = 
| writer         = Jorge Coscia
| music          = José Luis Castiñeira
| cinematography = 
| editing        = Celeste Maidana, Liliana Nadal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          =
}}
Perón: Apuntes para una biografía ( ) is a 2010 Argentine documentary film about Juan Domingo Perón. It features a number of Argentine historians like Norberto Galasso, and it is focuses mainly in Perón early life and his intervention in the Revolution of 43. His government is described in a brief summany at the end, and it doesnt reference his life after the Revolución Libertadora military coup that ousted him from power. It is written and directed by the Argentine secretary of culture, Jorge Coscia.  It is the eight biography film produced by Caras y Caretas. 

The film includes old photos, video archives, recordings at key sites of his early life and interviews with historians.

==See also==
* Early life of Juan Perón

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 