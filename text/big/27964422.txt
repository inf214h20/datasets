Red Hair (film)
{{Infobox film
| name           = Red Hair
| image          = Red Hair theatrical poster.jpg
| alt            = 
| caption        = 1928 theatrical poster
| director       = Clarence G. Badger
| producer       = Adolph Zukor Jesse L. Lasky B. P. Schulberg
| writer         = Agnes Brand Leahy
| screenplay     =  
| based on       =   William Austin
| music          = 
| cinematography = Alfred Gilks
| editing        = Doris Drought Paramount Famous Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = 
| gross          = 
}}
Red Hair is a 1928 silent film starring Clara Bow and Lane Chandler, directed by Clarence G. Badger, based on a novel by Elinor Glyn, and released by Paramount Pictures.
 production stills. 

==Plot==
A free-spirited young girl has three middle-aged admirers, each of whom sees her from a completely different perspective. Unknown to her, they also happen to be the guardians of a wealthy young man to whom she is attracted.

==Cast==
* Clara Bow as Bubbles McCoy
* Lane Chandler as Robert Lennon William Austin as Dr. Eustace Gill
* Jacqueline Gadsden as Minnie Luther
* Lawrence Grant as Judge Rufus Lennon Claude King as Thomas L. Burke William Irving as Demmy

==See also==
*List of lost films
*List of early color feature films

==References==
 

==External links==
 
*  
*   at SilentEra.com
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 


 