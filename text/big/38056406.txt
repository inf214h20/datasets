Janur Kuning
{{Infobox film
| name      = Janur Kuning
| image     = Janur-kuning.jpg
| image size   = 
| border     = 
| alt      = Video cover, showing four main characters
| caption    = VCD cover
| director    = Alam Surawidjaja
| producer    = Abbas Wiranatakusuma
| writer     = {{plain list|
*Syafnizal Durab
*Arto Hady
}}
| starring    = {{plain list|
*Kaharuddin Syah
*Deddy Sutomo
*Dicky Zukarnaen
}}
| music     =Sudharnoto
| cinematography = Kasdullah
| editing    = Soemardjono
| studio     = Metro 77
| distributor  = 
| released    =  
| runtime    = 127 minutes
| country    = Indonesia
| language    = Indonesian
| budget     = Indonesian rupiah|Rp.&nbsp;375 million
| gross     = 
}} Indonesian revolutionaries six-hour assault on Yogyakarta, under Suharto, in a show of force against the Dutch army. At the time the most expensive domestic production ever, the films title is meant to symbolise the Indonesian peoples struggle. A critical success, Janur Kuning received a nomination and two special awards at the 1980 Indonesian Film Festival. It was screened annually on 1 March between 1980 and 1998, but has since been criticised as an attempt to manipulate history and create a cult with President Suharto in the centre.

==Plot== invades the republican capital at Yogyakarta. Commander in Chief of the Army Sudirman (Deddy Sutomo) escapes, while the citys sultan Hamengkubuwana&nbsp;IX must stay there with his people and the national leadership. Sudirman, after escaping from the Dutch who were following him, goes on a guerrilla campaign.

The following year, Lieutenant Colonel Suharto (Kaharuddin Syah) devises a daring plan, a show of force in the capital. Early in the morning on 1&nbsp;March 1949 he takes his soldiers&nbsp;– all in full uniform&nbsp;– and retakes the city, overwhelming the Dutch. After six hours Suharto and his men retreat, as planned. Several months later the Dutch recognise Indonesias independence.

==Production==
  has been described as having mystic powers.]]
Janur Kuning was directed by Alam Surawidjaja and produced by Abbas Wiranatakusuma of the Jakarta-based production house Metro 77. The script was written by Syafnizal Durab in collaboration with Arto Hady, while cinematography was completed by Kasdullah. In post-production Soemardjono handled editing and Sudharnoto and Suparman Sidik handled music and other sound effects. Kaharuddin Syah starred as Suharto, while Deddy Sutomo played Sudirman; other cast members included Amak Baldjun and Dicky Zulkarnaen. 

Janur Kuning was the most expensive Indonesian film up to that point, with a budget of Indonesian rupiah|Rp.&nbsp;375&nbsp;million. The cost led production to be held up for over a month when the allocated funds were unavailable.  It was the second domestic production to deal with the 1&nbsp;March general assault, after Usmar Ismails Enam Djam di Djogdja (Six Hours in Jogja; 1951).  The films title, which refers to immature coconut leaves, is meant to symbolise the Indonesian peoples struggle against the Dutch colonists.  In the film, fighters wear the leaves on their sleeves to show their loyalty. 
 New Order disapproved of it.  Sudirman, sickly from tuberculosis during his guerrilla campaign, is depicted as having mystic powers: the military historian Katherine McGregor notes such an effect in one scene, where Sudirman whispers a prayer and causes the rain to fall, distracting the his Dutch pursuers and allowing him to escape. 

==Release and reception==
Janur Kuning was released in 1980. At that years Indonesian Film Festival, Amak Baldjun was nominated for a Citra Award for Best Supporting Actor for his role in the film. Two special awards were also given at the ceremony, for Producer Supporting the Peoples Struggle (  and Hopeful Actor ( ).  It has remained well received. In an overview of Indonesian patriotic films for The Jakarta Globe, Awis Mranani wrote that Janur Kuning was "one to watch". 

After its release, Janur Kuning was broadcast annually on TVRI on 1&nbsp;March.  In September 1998, four months after the fall of Suharto, Information Minister Yunus Yosfiah stated that the film was an attempt to manipulate history and create a cult with Suharto in the centre. Two other films, Serangan Fajar (Dawn Attack; 1981) and Pengkhianatan G30S/PKI (Treachery of G30S/PKI; 1984), were also affected by the decree.   Serangan Fajar portrayed Suharto as a great hero of the revolution, especially the 1&nbsp;March General Assault,   while Pengkhianatan G30S/PKI emphasised the former presidents role in destroying the 30&nbsp;September Movement coup in 1965.  The accuracy of Janur Kuning had been questioned as early as its release. 

A 35&nbsp;mm copy is stored at Sinematek Indonesia in Jakarta. 

==Footnotes==
 
==Works cited==
 
*{{cite news
 |title=Ingat Janur Kuning
 |trans_title=Remembers Janur Kuning
 |language=Indonesian
 |url=http://suaramerdeka.com/v1/index.php/read/cetak/2009/03/02/54055/Ingat-Janur-Kuning
 |work=Suara Merdeka
 |location=Semarang
 |last= Armitrianto
 |first=Adhitia
 |date=2 March 2009
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGqbAgj2
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite book
 |url=http://books.google.co.id/books?id=9Vk9dgytvlkC
 |title=Warisan (daripada) Soeharto
 |trans_title=Legacy (from) Soeharto
 |language=Indonesian
 |isbn=978-979-709-364-8
 |year=2008
 |first=Bagus
 |last=Dharmawan
 |publisher=Kompas
 |location=Jakarta
 |ref=harv
}}
*{{cite web
 |title=Enam Djam di Djogdja
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-e017-51-507852_enam-djam-di-djogdja
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGpZf2bS
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite web
 |title=Janur Kuning
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-j011-79-029264_janur-kuning
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGkBXdX4
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite web
 |title=Kredit Janur Kuning
 |trans_title=Credits for Janur Kuning
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-j011-79-029264_janur-kuning/credit
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGoNS7bX
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=dVxi2oXZqjkC
 |title=History in Uniform: Military Ideology and the Construction of Indonesias Past
 |isbn=978-9971-69-360-2
 |last1=McGregor
 |first1=Katharine E
 |year=2007
 |ref=harv
 |location=Honolulu
 |publisher=University of Honolulu Press
}}
*{{cite news
 |title=Indonesias Fights for Freedom on Screen
 |url=http://www.thejakartaglobe.com/blogs/indonesias-fights-for-freedom-on-screen/459706
 |work=The Jakarta Globe
 |last=Mranani
 |first=Awis
 |date=17 August 2011
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGrCJc5w
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite web
 |title=Penghargaan Janur Kuning
 |trans_title=Awards for Janur Kuning
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-j011-79-029264_janur-kuning/award
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGomuE0u
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite web
 |title=Pengkhianatan G-30-S PKI
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p022-82-358646_pengkhianatan-g-30-s-pki
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=25 December 2012
 |archiveurl=http://www.webcitation.org/6DADDwQUq
 |archivedate=25 December 2012
 |ref= 
}}
*{{cite news
 |title=Janur Kuning, Simbol Perlawanan
 |trans_title=Yellow Coconut Leaves, Symbols of Resistance
 |language=Indonesian
 |url=http://regional.kompas.com/read/2010/12/15/18550770/Janur.Kuning.Simbol.Perlawanan
 |work=Kompas
 |last=Prihtiyani
 |first=Eny
 |location=Jakarta
 |date=15 December 2010
 |accessdate=29 December 2012
 |archiveurl=http://www.webcitation.org/6DGqx6A8a
 |archivedate=29 December 2012
 |ref= 
}}
*{{cite news
 |url=http://www.tempo.co/read/news/2012/09/29/078432686/3-Pemeran-Sentral-di-Film-Pengkhianatan-G-30-SPKI
 |archiveurl=http://www.webcitation.org/6DAlJx5hE
 |archivedate=25 December 2012
 |accessdate=25 December 2012
 |date=30 September 2012
 |work=Tempo
 |author1=Rini K
 |author2=Evan
 |title=Tokoh di Balik Penghentian Pemutaran Film G30S
 |trans_title=Persons Behind the Ceasing of Screenings of the G30S Film
 |language=Indonesian
 |ref= 
}}
*{{cite book
 |title=Media, Culture and Politics in Indonesia
 |last1=Sen
 |first1=Krishna
 |authorlink=
 |first2=David T.
 |last2= Hill
 |year=2006
 |publisher=Equinox Publishing
 |location=Jakarta
 |isbn= 978-979-3780-42-9
 |url=http://books.google.com/?id=xMhWm38KQcsC
 |ref=harv
}}
*{{cite web
 |title=Serangan Fajar
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-s013-81-597985_serangan-fajar
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=25 December 2012
 |archiveurl=http://www.webcitation.org/6DAEPhebo
 |archivedate=25 December 2012
 |ref= 
}}
 

 
 
 