Hatsukoi Jigokuhen
{{Infobox film
| name           = Hatsukoi Jigokuhen (Nanami, The Inferno of First Love)
| image          = Hani Nanami.jpg
| caption        = Poster to the film, Nanami, The Inferno of First Love
| director       = Susumu Hani
| producer       = Tomoji Fujii Satoshi Fuji
| writer         = Susumu Hani Shūji Terayama
| starring       = Haruo Asanu Kuniko Ishii
| music          = Tōru Takemitsu Akio Yashiro
| cinematography = Yuji Okumura
| editing        = Susumu Hani
| distributor    = Art Theatre Guild
| released       = May 25, 1968
| runtime        = 108 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1968 film directed by Susumu Hani and co-scripted by him with Shūji Terayama.  It is one of Hanis best known works. In the West, it is known as Nanami, The Inferno of First Love or as Nanami, First Love.  The movie focuses on the pain of emerging from adolescence. The film was nominated for the Golden Bear award at the 18th Berlin International Film Festival in 1968.  Many film scholars consider this work to be one of Hanis major achievements, while others judge the film to be commercial and exploitive. 

==Cast==
* Haruo Asanu - Algebra
* Kazuko Fukuda - Mrs. Otagaki, Shuns stepmother
* Kuniko Ishii - Nanami
* Ichirô Kimura - Psychiatrist
* Kazuo Kimura - Doctor
* Kôji Mitsui - Mr. Otagaki, Shuns stepfather
* Misako Miyato - Mother
* Kimiko Nakamura - Ankokujis wife
* Akio Takahashi - Shun
* Minoru Yuasa - Ankokuji

== References ==
 

==Sources==
*  
*  

==External links==
* 
* 
*  
*  

 

 
 
 
 
 


 