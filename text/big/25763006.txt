Something Beneath
{{Infobox film
| name                  = Something Beneath
| image                 = something beneath dvd.jpg
| caption               = DVD cover
| director              = David Winning
| producer              = Phyllis Laing
| writer                = Mark Mullin Ethlie Ann Vare David Winning Natalie Brown Brendan Beiser
| cinematography        = Brenton Spencer
| music                 = Michael Richard Plowman
| editing               = Mark Sanders
| studio                = RHI Entertainment Buffalo Gal Pictures Hallmark Entertainment Paquin Films
| distributor           = Time Warner Cable 
| released              =  
| runtime               = 90 minutes
| country               = Canada
| language              = English
}} Natalie Brown and Brendan Beiser.  It is the 4th film in the Maneater Series.

==Plot==
 
When attendees at an ecological conference begin to report strange happenings, an event coordinator learns the gruesome truth: a slime-like organism is causing humans to behave in a terrifying manner.  Together with a brave priest she must race against time to evacuate the guests before more fall victim to the horror that lurks underneath them.

==Cast==
* Kevin Sorbo as Father Douglas Middleton Natalie Brown as Khali Spence
* Peter MacNeill as Deadmarsh
* Brendan Beiser as Dr. Connolly
* Gordon Tanner	as Symes
* Tom Keenan as Hank
* Blake Taylor as Reggie
* Brittany Scobie as Mikaela Strovsky
* Frank Adamson	as Lowell Kent
* Paige Bannister as Aimee
* Brett Donahue as Tony
* Rob McLaughlin as Eugene Herman
* Gene Pyrz as Jim Bailey
* Tracey Nepinak as Khalis Grandmother
* Aimee Cadorath as Young Khali
* Kevin Aichele	as Dutch
* Mike Bell as Construction Foreman
* Brandon Doty as Backpacking Hippie
* Lindsay Embroyle as Paramedic
* David Stuart Evans as Clerk
* Craig Matthews as Hard Hat
* Kyle Nobess as Sheik
* Thanya Romero	as Manuela
* Tanakh as Ajax
* Karl Thordarson as Mr. Briggs
* Julia Van de Spiegle as Woman in the mirror
* David Winning as Beast in the Woods / Angry Arab guest / TV announcer

==Production==
The movie was filmed in Winnipeg, Manitoba in November 2006.  In a 2007 FearNet interview, Sorbo described it as “sort of a flip back to the ‘50s creature films. ”

==Release==
The film premiered on 26 April 2007 as part of the Houston Film Festival and was than chapter of the Rhode Island International Film Festival on 11 August 2007. 

== Critical reception ==
 

==Home media==
It was released on DVD by Genius Entertainment on September 9, 2008. 

Something Beneath was released on DVD in Germany February 25, 2010, under the title “Endstation: Angriff aus dem Untergrund”. ”

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 