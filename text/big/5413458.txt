Havana (film)
{{Infobox film
| name           = Havana
| image          = Havana_imp.jpg
| alt            =
| caption        =
| director       = Sydney Pollack Richard Roth
| writer         = Judith Rascoe David Rayfiel
| starring       = Robert Redford Lena Olin Raúl Juliá Alan Arkin
| music          = Dave Grusin
| cinematography = Owen Roizman
| editing        = Fredric Steinkamp William Steinkamp
| studio         =
| distributor    = Universal Pictures
| released       =  
| runtime        = 140 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $9,243,140
}}
Havana is a 1990 drama film starring Robert Redford, Lena Olin and Raúl Juliá, directed by Sydney Pollack with music by Dave Grusin. The films plot concerns Jack Weil (Redford), an American professional gambler who decides to visit Havana, Cuba to gamble. En route to Havana, he meets Roberta Duran (Olin), the wife of a revolutionary, Arturo (Juliá). Shortly after their arrival, Arturo is taken away by the secret police, and Roberta is captured and tortured. Jack frees her, but she continues to support the revolution.

==Plot==
The film is set on the eve of the Cuban Revolutions victory on January 1, 1959.

On Christmas Eve, 1958 aboard the boat from Miami to Havana, Roberta Duran enlists the aid of Jack Weil (Robert Redford) in smuggling in Signal Corps (United States Army)|U.S. Army Signal Corps radios destined for Cuban Revolutionaries in the hills. Weil agrees only because he is romantically interested in Ms. Duran. When they rendezvous for the "payoff", Roberta reveals she is married, dashing Weils hopes.

Weil meets up with a Cuban journalist acquaintance (Tony Plana) and during a night on the town they run into Roberta and her husband, Dr. Arturo Duran. Dr. Duran (Raúl Juliá) is a Revolutionary leader. When Roberta points Weil out to him, Dr. Duran invites Weil to join them for dinner, and asks Weil for further aid to the cause. Weil turns him down, even after Duran outlines the desperate situation confronting the Cuban majority.

The next morning, after a night of debauchery for Weil, but a night of arrests of Revolutionaries by the police, Weil reads a newspaper account of Dr. Durans arrest and death. In shock, he continues with the planned poker game, at which he meets the head of the secret police. He learns that Roberta was also arrested and held; she was also tortured. Weil uses the debt one of the other players (a lieutenant) owes him to obtain Robertas release. In shock from her husbands death and her own experience in jail, she agrees to let him shelter her in his apartment, but that afternoon she disappears.

Realizing that he is in love with Roberta, and encouraged by an old gambling friend, Weil drives into Cubas interior to find her at Dr. Durans old estate. He persuades her to return with him to Havana and to leave Cuba with him. When she asks, he explains that a lump on his arm contains a diamond he had sewn into his arm in his youth, as insurance that no matter what happens in life, one always has that diamond.
 CIA agent Marion Chigwell (Daniel Davis) whose acquaintance he had made. He obtains the information by threatening to blow the agents cover of gourmet magazine writer, then uses it also to make a deal with him regarding Dr. Duran.
 
Pretending to work for the CIA, Weil goes to see Dr. Duran, who is held by the chief of the secret police (SIM).  He tells the chief that Washington, D.C. has new plans for Duran and wants him released, with a payoff of $50,000.  He "orders" the chief to have Duran cleaned up and dressed (Duran had been tortured and was in extremely bad shape) and taken to his house. Weil goes to a doctor, then a jeweler, to sell the diamond to raise the cash for Dr. Durans release.

Back at his apartment, he informs Roberta, who had decided to make a life with him, that her husband is still alive.  In shock, she leaves on her own to find her husband.  Meanwhile, Weil had blown the big game with high rollers he had been angling for since the day he arrived in Havana.  The casino manager Joe Volpi (Alan Arkin) forgives him, knowing he had made rescuing Roberta his priority.

That night, New Years Eve, 1959, the insurrection is won by the Revolutionaries.  The upperclass, the government and the secret police all leave their lavish New Years Eve parties to make a mad dash to the ports and airport to leave the country.  The people pour into the streets, celebrating the victory by trashing the casinos and dancing.  Weil and Joe agree it is a new day and time for them to go.
 The Cuisine of Indochina." Not long after, Roberta shows up to wish him farewell.  She discovers, by seeing the bandage on his arm, what it had cost him to save her husband for her.  They hug goodbye.  She remains with the Revolution, and he has been changed by it.

Four years later in 1963, Jack drives down to the Florida Keys and gazes across the sea toward Havana, hoping to see a boat that might bring Roberta on board. He knows the ferry is no longer running. However, he does this every year. He hopes to someday see Roberta again. He also realizes that the changes in Cuba were being echoed in the changes of the 1960s happening in United States|America. Its a new decade.

==Cast== World War Pearl Harbor when it was bombed. Jack comes to Havana to gamble but finds himself getting caught up in the Cuban Revolution.
* Lena Olin as Bobby Durán: Swedish by birth, Bobby moved to California to become an actress then went to Mexico when her first husband got black listed. When she came to Havana, she soon married Arturo Duran.
* Alan Arkin as Joe Volpí: Manager of a popular Havana casino, Joe works for infamous Jewish-American mobster Meyer Lansky. Joe is an old friend of Jacks.
* Raúl Juliá (Uncredited) as Arturo Durán: A member of an old, wealthy family, Arturo is a figurehead in the Revolution but must keep his activities to a minimum in Havana.
* Tomás Milián as Menocal: A colonel in the secret police, Menocal answers to Batista and tries to keep Havana under control through abduction and murder. Menocal is against the Revolution because he believes no matter who is in charge someone will always suffer.
*  , Marion is frequently seen around Havana trying food at restaurants.
* Tony Plana as Julio Ramos: A friend of Jacks and a reporter, Julio is sympathetic to the Revolution.
* Betsy Brantley as Diane: An American tourist who meets Jack at a bar.
* Lise Cutter as Patty: Dianes friend.
* Richard Farnsworth as the Professor: An old gambler who gives Jack relationship advice.
* Mark Rydell as Meyer Lansky: The infamous Jewish mobster, he is head of Mafia operations in Havana, owning many of Havanas casinos.
* Vasek Simek as Willy
* Fred Asparagus as Baby Hernández
* Richard Portnow as Mike MacClaney
* Dion Anderson as Roy Forbes
* Carmine Caridi as Captain Potts

==Reception==
The reviews were generally negative. The film has a 24% rating on Rotten Tomatoes based on 25 reviews. On a budget of $40 million, Havana made only $9 million domestically, making it a box office bomb.
 Golden Globe, Grammy nominations. It was one of Dave Grusins most acclaimed scores.

==Production==
 
 
* Filming began on November 22, 1989 and completed on April 28, 1990.
* Raúl Juliá chose to remain uncredited because the films producers would not give him above-the-title credit alongside Robert Redford and Lena Olin. 
* Tony Plana and Tomás Milián are Cuban-American actors who lived in Cuba during the 1950s.  Milián commented that the film recreated 1958 Havana in great detail during the Batista regimes last days.  Many of the extras were exiled Cubans who had moved to the Dominican Republic.  According to Sydney Pollack, "The atmosphere became quite emotional... They remembered the old days in Havana.  Our set took them back 30 years."
* Sydney Pollack hoped to film in Havana. However, U.S. law would not allow the producers to spend any U.S. dollars in Cuba, U.S. citizens could not legally enter Cuba, and relations between the U.S. and Cuba in 1989 were not conducive to filming an American motion picture in Havana.
* It was decided to make the entire film in the Dominican Republic.  The vegetation was the same, and Santo Domingo offered certain architectural similarities, though not a wide boulevard like Havanas famous Prado (Paseo de Marti).  The end scene was filmed in Key West, Florida.
* The films main set, called "The Big Set", was a quarter-mile long street surrounded by facades representing casinos, restaurants and hotels.  Interior scenes were shot in replicated casino floors, room suites and cafes.   The Prado  was replicated by the producers at a former air base in the Dominican Republic.  To replicate the Prado, a team of about 300 tradesmen was used over 80 neon signs which needed to be made in the U.S. and shipped to the Dominican Republic. It took 20 weeks to construct "The Big Set".
* Costume designer Bernie Pollock had to outfit 2,000 extras with costumes, and needed 8000–10,000 costumes for frequent changes during different scenes of the film.  Besides 1950s period clothing, there were large numbers of hats, accessories, jewellery, gloves, along with 1950s Cuban military uniforms.  The wardrobe items were brought in from both Los Angeles and England.
* About one hundred 1950s vintage American automobiles, buses and trucks appear in the film.
* The version of Rum and Coca-Cola by The Andrews Sisters is a re-recorded version from 1961 from their DOT Records album Greatest Hits (recorded two years after the films setting).
==See also==
* Havana (soundtrack)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 