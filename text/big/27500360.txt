Alive and Kicking (film)
 
{{Infobox film
| name           = Alive and Kicking
| caption        = 
| image	=	Alive and Kicking FilmPoster.jpeg
| director       = Cyril Frankel
| producer       = Victor Skutezky
| writer         = Denis Cannan William Dinnie William Murum
| screenplay     = 
| story          = 
| based on       =  
| starring       = Sybil Thorndike Kathleen Harrison Estelle Winwood Stanley Holloway
| music          = Philip Green
| cinematography = Gilbert Taylor
| editing        = Bernard Gribble
| studio         = Diador Seven Arts (US)
| released       =     
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Alive and Kicking is a 1959 British comedy film directed by Cyril Frankel and starring Sybil Thorndike, Kathleen Harrison, Estelle Winwood and Stanley Holloway.  Its plot follows three women who grow dissatisfied with their lives in a retirement home and decide to search for fresh enjoyment and adventure. They eventually end up running a successful sweater business in Ireland.

==Cast==
* Sybil Thorndike - Dora
* Kathleen Harrison - Rosie
* Estelle Winwood - Mabel
* Stanley Holloway - MacDonagh
* Liam Redmond - Old man
* Marjorie Rhodes - Old woman
* Richard Harris - Lover
* Olive McFarland - Lover
* John Salew - Solicitor
* Eric Pohlmann - Captain
* Colin Gordon - Bird watcher
* Joyce Carey - Matron
* Anita Sharp-Bolster - Postmistress
* Paul Farrell - Postman
* Patrick McAlinney - Policeman
* Liz Fraser - Minor role
* Brendan ODowda - Singer/Minor Role

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 