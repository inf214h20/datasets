Presumed Guilty (film)
 
{{Infobox film
| name           = Presumed Guilty
| image          = 
| caption        =  Roberto Hernández Geoffrey Smith
| producer       = Layda Negrete, Roberto Hernández, Martha Sosa, Yissel Ibarra
| writer         = 
| starring       = Antonio Zúñiga Eva Gutiérrez Rafaél Ramirez Heredia
| music          = 
| cinematography = 
| editing        = Felipe Gomez Roberto Hernández 
| studio         = Lawyers with Camera; Instituto Mexicano de Cinematografía CONACULTA; Fondo para la producción cinematográfica (FOPROCINE)
| distributor    = Cinépolis
| released       =  
| runtime        = 87 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} exonerate a wrongly convicted man by making a documentary. The film was released theatrically at about the same time the Oscar nominated films such as Black Swan and The Kings Speech were being shown on cinema screens in Mexico. It surpassed both of those films at the Box Office. The film was televised by Televisa on Channel 2 in the fall of 2011.

==Plot==
Two lawyers struggle to free a man, Antonio Zúñiga, who has been wrongly convicted by the Mexican judicial system. Zúñiga was arrested on charges of murder and convicted largely on the testimony of one man. Zúñiga was told by authorities “You did it and that’s it”. Zúñiga was given the sentence of 20 years in prison for a crime that was impossible for him to have committed. Three witnesses explained that he was at his place of work during the time of which he was accused of murder. However, the man was a close relative of the victim who had no firm evidence against Zúñiga, while the accused produced several witnesses able to place him far from the scene of the crime at the time of the murder. Despite this, Zúñiga was found guilty, and when lawyers Roberto Hernández and Layda Negrete learned about his case, they agreed to help him. Hernández and Negrete  cautiously advised Zúñiga, knowing that many case like his had failed before him and they were fearful of providing Zúñiga and his family with too much hope. Before leaving for graduate school in Berkeley, CA, Hernández and Negrete  advised the family to go public with this case- they felt it was their best shot at pressuring the Mexican judicial system to admit their error and free Zúñiga. After it was revealed that the lawyer appointed to represent Zúñiga did not have a valid license to practice law, authorities grudgingly agreed to a new trial, but with the same judge, Héctor Palomares Medina, presiding. This judge showed little interest in evidence that Zúñiga was falsely convicted. Battling an arrogant judge, uncooperative witnesses and a legal system riddled with corruption, Hernández and Negrete found that it was easy to prove Zúñigas innocence, but hard to get the authorities to acknowledge this fact. “The conviction was eventually overturned in 2008 after the filmmakers persuaded the judges hearing the appeal to watch the videos. Zúñiga was immediately released"   Presumed Guilty was a selection at the 2009 Toronto International Film Festival.

Unfortunately, situations like these are more common than one would hope after viewing “Presumed Guilty.” One woman, Rosa Juilia Leyva Martinez, told the Washington Office on Latin America that she was traveling within Mexico to buy seeds with some people in her town. Unbeknownst to her, she carried one of their bags through the airport that contained heroin. Once arrested, she reported that she was tortured, raped, and forced to sign a confession which sentences her to 22 years in prison. She was released after 11 years. “According to the Mexican Ministry of the Interiors 2012 National Survey of the Criminal Justice System, only 6% of the Mexicans surveyed had confidence in the justice system. Responders said the main problems were that criminals are not held accountable, the system is corrupt, the judicial process slow, and the service from public servants is poor.”   Some other startling facts that the documentary display for the viewers are the following: 
• 95% of verdicts are convictions 
• 92% of those convictions are not based on physical evidence 
• 78% of inmates are fed by their own families 
• 93% of inmates are never shown arrest warrants 
• 93% of defendants never see a judge

The story of Presumed Guilty was made into an episode of the TV series "P.O.V.#2010|P.O.V." that aired on 27 July 2010. It subsequently was nominated to three Emmy awards for "Best Research" "Best Documentary" and "Outstanding Investigative Journalism."

==Banning the Film==
In February 2011, Presumed Guilty was released to Mexican audiences and one month later, a judge moved to have the film banned. This action caused the movie to gain unprecedented popularity almost overnight, presumably because this seemed as though the judicial system had a secret to hide that this film revealed. The distributor said they would continue to show the movie until the order had been formally submitted. Presumed Guilty quickly became a box-office hit. This film also took to the streets in the form of pirated DVDs. One vendor said he sold 70 copies on a single Saturday, continuing that he had only sold that many of other box-office hits over a full week. 

==Life for Zúñiga==
At the end of the film, Zúñiga is shown embracing his wife and child in a joyful reunion. But what the film doesn’t show is his life subsequent to being released from prison. Zúñiga now lives a life in fear, scared that someone will take revenge for the film. He is careful to have record of when he leaves his home via receipts and security cameras, even though leaving home isn’t something he does very often anymore. He cannot simply have his same life back- he must always be wary of the potential dangers that this film has caused. He hopes to now finish and receive his high school degree and to support this family. 

==Cast==
* José Antonio Zúñiga Rodríguez (Antonio Zúñiga, the wrongly convicted man) as himself
* Eva Gutiérrez (wife of Antonio Zúñiga) as herself
* Rafaél Ramirez Heredia (Zuñigas defense lawyer) as himself
* Roberto Hernández (filmmaker/lawyer) as himself
* Layda Negrete (filmmaker/lawyer) as herself
* Hector Palomares Medina (the judge) as himself
* Maricela Guzman (the prosecutor) as herself
* Victor Daniel Reyes (witness for the prosecution) as himself
* Jose Manuel Ortega Saavedra (Detective) as himself

==Production== Public Policy at the Goldman School of Public Policy at the University of California, Berkeley. They are married and have two daughters.

==Accolades==
Presumed Guilty has received numerous awards and honors, including the following:
*Emmy 2010-Outstanding Investigative Journalism
*One World Media (London, 2010) - Best Feature Documentary
*Documenta Madrid (2010) - Best Documentary Award and Audience Award 
*East End Film Festival (London, 2010) - Best Feature Documentary
*Los Angeles Film Festival (2010) - Audience Award and Best International Feature
*Human Rights Watch Film Festival (New York, 2010) - Closing Night Film 
*Human Rights Watch Film Festival] (Toronto, 2010) - Closing Night Film 
*San Francisco International Film Festival (2010) - Golden Gate Best Bay Area Documentary
*Guadalajara International Film Festival (2010) - Best Documentary Copenhagen International Documentary Film Festival (2009) - Amnesty International Award
*Morelia International Film Festival] (2010) - Best Documentary  Maysles Brothers Documentary Award
*News & Documentary Emmy Awards (2011) - Emmy for Outstanding Investigative Journalism – Long Form  

==Mexican exhibition and banning==
Cinépolis announced that Presumed Guilty would premiere in its movie theaters in Mexico on February 18, 2011.

On March 2, 2011, a Mexican Federal Judge named Blanca Lobo banished the film from theaters, and ordered that the exhibition and distribution of the documentary be halted, on the basis that the main witness for the prosecution had filed an recurso de amparo|amparo against RTC, the government agency that rates films in Mexico. RTC replied that it lacked authority to censor films, but the judge insisted the film should be banned.

Judge Lobos order was revoked on March 9, 2011 by a higher court based on Article 6 of the Mexican Constitution which protects the right to free expression. By then, the movie had been widely distributed by street vendors as well as on YouTube. Litigation concerning Judge Lobos order continues to this day.

The filmmakers are currently facing numerous lawsuits in Mexican courts, none of which have been resolved definitely. The case concerning Judge Lobos order has been sent to Supreme Court of Mexico|Mexicos Supreme Court, which will soon decide whether it accepts to hear it.

==See also==
* Law of Mexico
* Presumption of innocence
* Law enforcement in Mexico Mexico City law enforcement Crime in Mexico -- corruption
* Federal District Police—the police of Mexico City
* Attorney General of Mexico

==References==
 

==External links==
* http://www.economist.com/node/18483267
*    
*    
*   Wall Street Journal article
*   -- Los Angeles Times article
*   -- San Francisco Chronicle article
*   --   article
*   -- NPR article
*   on the film and P.O.V. television episode
*  —article in The William and Flora Hewlett Foundation Newsletter
*  , by Cathy Cockrell (1 December 2008)
*  , by Cathy Cockrell (1 December 2008)  
*  , by Andrew Cohen (7/26/2010)
*   by Rachel Swan (7/28/2010)
*   (7/15/2010)

 
 
 
 
 
 
 
 
 
 

 