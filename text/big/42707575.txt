Tananam Tananam
{{Infobox film name           = Tananam Tananam image          = image_size     = caption        = director       = Kavitha Lankesh producer       = N. M. Srinivas writer  Kalki
|narrator       = starring       = Shaam Ramya Rakshita Girish Karnad music          = K. Kalyan cinematography = A. C. Mahendran editing        = M. N. Swamy studio         = Sri Thulaja Bhavani Creations released       =   runtime        = 139 minutes country        = India language       = Kannada budget         =
}}
 Kannada romantic Tamil short story written by Kalki Krishnamurthy|Kalki.
 Filmfare Awards in Best Actress and Best Lyricist categories. Noted violinist L. Subramaniam was roped in to play violin bits in the film.

==Production==
Director Kavitha Lankesh held a press conference meet at Bangalore to announce her next venture titled Tananam Tananam in early February 2005. She announced the commencement of shooting date from 15 February 2005. She put all the pre-release rumors to rest saying that both the leading ladies have equal footage in the film and that the film itself is a triangular love story set against the backdrop of classical music and professional theatre. She also admitted that the story was inspired from a Tamil short story penned by Kalki Krishnamurthy|Kalki. The classical music notes that comes in the film scenes have been tuned by acclaimed violinist L. Subramaniam. 

==Cast==
* Shaam as Shankar
* Rakshita as Bhavani
* Ramya as Vanaja
* Girish Karnad as Shastry
* Bharathi Vishnuvardhan as Shastrys Wife
* Avinash Sharan
* Harish Raj
* Mandeep Roy Baby Amulya

==Soundtrack==
The music of the film was composed and lyrics written by K. Kalyan. The title track won him the Filmfare Award for Best Lyricist - Kannada for the year 2006.

{{Infobox album  
| Name        = Tananam Tananam
| Type        = Soundtrack
| Artist      = K. Kalyan
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Ashwini Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Kande Kande Govindana
| lyrics1 	= K. Kalyan
| extra1        = K. S. Chithra, Ajay Warrior
| length1       = 
| title2        = Cheluvantha Rajakumari
| lyrics2 	= K. Kalyan
| extra2        = Kunal Ganjawala, K. S. Chithra
| length2       = 
| title3        = Chapparisu
| lyrics3       = K. Kalyan
| extra3 	= Malgudi Subha, Hemanth Kumar
| length3       = 
| title4        = Koogalatheya Dooradalli
| extra4        = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics4 	= K. Kalyan
| length4       = 
| title5        = Kalitha Hudugi
| extra5        = Gururaj Hosakote
| lyrics5       = K. Kalyan
| length5       = 
| title6        = Preethi Endarenu
| extra6        = Kunal Ganjawala
| lyrics6       = K. Kalyan
| length6       =
| title7        = Tananam Tananam
| extra7        = K. S. Chithra
| lyrics7       = K. Kalyan
| length7       =
}}

==Awards==
* Filmfare Awards South Best Actress - Ramya Best Lyricist - K. Kalyan

==References==
 

==External source==
*  
*  

 
 
 
 
 
 
 

 