Watchmen (film)
 
 
{{Infobox film
| name                 = Watchmen
| image                = Watchmen film poster.jpg
| alt                  = A rainy city. Six people stand there, all but one - a masked man in hat and trench coat - staring at the viewer: a muscular and glowing blue man, a blonde man in a spandex armor, a man in an armor with a cape and wearing a helmet resembling an owl, a woman in a yellow and black latex suit, and a mustached man in a leather vest who smokes a cigar and holds a gun. Text at the top of the image includes "From the visionary director of 300". Text at the bottom of the poster reveals the title, production credits, and release date.
| caption              = Theatrical release poster
| director             = Zack Snyder
| producer             = {{Plain list| Lawrence Gordon
* Lloyd Levin
* Deborah Snyder
}}
| screenplay           = {{Plain list|
* David Hayter
* Alex Tse
}}
| based on             =  
| starring             = {{Plain list|
* Malin Åkerman
* Billy Crudup
* Matthew Goode
* Carla Gugino
* Jackie Earle Haley
* Jeffrey Dean Morgan Patrick Wilson
}}
| music                = Tyler Bates
| cinematography       = Larry Fong
| editing              = William Hoy
| production companies = {{Plain list|
* Legendary Pictures
* DC Comics
* Lawrence Gordon Productions
}}
| distributor          = {{Plain list|
* Warner Bros. Pictures  
* Paramount Pictures  
}}
| released             =   
| runtime              = 162 minutes 
| country              = United States
| language             = English
| budget               = $130 million 
| gross                = $185.3 million   
}}
 Patrick Wilson. adaptation of same name moral limitations are challenged by the complex nature of the circumstances.
 Lawrence Gordon green screens and opted for real sets instead.

Following its world premiere at Odeon Leicester Square on February 23, 2009,  the film was released in both conventional and IMAX theaters on March 6, 2009, grossing $55 million on the opening weekend, and grossed over $185 million at the worldwide box office.

A DVD based on elements of the Watchmen universe was released, including an animated adaptation of the comic Tales of the Black Freighter within the story, starring Gerard Butler, and the fictional biography    Under the Hood, detailing the older generation of superheroes from the films back-story. A directors cut with 24 minutes of additional footage was released in July 2009. The "Ultimate Cut" edition incorporated the Tales of the Black Freighter content into the narrative as it was in the original graphic novel, lengthening the runtime to 215 minutes, and was released on November 3, 2009.

==Plot==
  the Comedian Rorschach continues to operate outside the law.
 Daniel Dreiberg Laurie Jupiter Adrian Veidt (Ozymandias), who dismisses it.
 invade Afghanistan. Later, Rorschachs theory appears to be justified when Veidt narrowly avoids an assassination attempt, and Rorschach finds himself framed for the murder of a former villain named Moloch (comics)|Moloch. Meanwhile, Jupiter, after breaking up with Manhattan, goes to stay with Dreiberg, and the two former superheroes eventually become lovers, and decide to come out of retirement. After helping Rorschach break out of prison, Jupiter is confronted by Manhattan. He takes her to Mars and, after she asks him to save the world, he explains that he is no longer interested in humanity. As he probes her memories, he discovers she is the product of a voluntary affair between her mother Sally (the original Silk Spectre) and Blake, who had previously tried to rape her. His interest in humanity is renewed by this improbable sequence of events, and Manhattan resolves to return to Earth with Jupiter.
 Antarctic retreat. Veidt confirms he is the mastermind behind Blakes murder, Manhattans exile, Rorschachs framing, and his own assassination attempt, which he used to avert suspicion. He explains that his plan is to unify the United States and the Soviet Union by destroying the worlds main cities with exploding energy reactors that Manhattan unknowingly helped him create. Rorschach and Dreiberg attempt to stop him, but Veidt subdues them both, and reveals that his plan has already been set into motion: the reactors have been detonated, and the energy signatures are recognized as Manhattans.
 New York teleport to his Antarctic base just after he has beaten Rorschach and Dreiberg, causing Veidt to retreat and attempt to kill Manhattan. Unsuccessful, he shows them a televised news report in which Nixon states that the United States and Soviets have allied against their new "common enemy," Dr. Manhattan. The heroes eventually realize that revealing the truth would only disrupt the peace. Rorschach forces Manhattan to vaporize him, to keep him from revealing the truth. Manhattan shares a final kiss with Jupiter and departs permanently for another galaxy while an enraged Dreiberg assaults Veidt, who nevertheless defends his actions. Dreiberg and Jupiter leave Veidt to think about his choices.

Jupiter and Dreiberg return to New York and plan to continue fighting crime. Jupiter reveals to her mother that she has learned that Blake is her real father, and the two reconcile. The film ends with an editor of the New Frontiersman telling a young employee that he may print whatever he likes from a collection of crank mailings. Among these lies Rorschachs journal, which explains everything from beginning to end, implying that the truth of Veidts plan will be revealed.

==Cast and characters==
 
  John McLaughlin, Jackie Kennedy, Andy Warhol, Truman Capote, Elvis Presley, Mao Zedong, Larry King, David Bowie, Mick Jagger, Lee Iacocca, and the Village People.   Snyder said he wanted younger actors because of the many flashback scenes, and it was easier to age actors with make-up rather than cast two actors in the same role.    Snyders son cameos as a young Rorschach,  while the director himself appears as an American soldier in Vietnam.  Actor Thomas Jane was invited by Snyder, but declined to work in the film due to being too busy. 

*   and Kate Winslet were both approached by the studio for the role. the 2008 space western film The Day the Earth Stood Still.
*   (a big fan of the character) for the part, but said that Goode was "big and tall and lean," which aided in bringing "this beautiful ageless, Aryan superman" feel to the character.  Goode interpreted Veidts back-story to portray him with a German accent in private and an American one in public; Goode explained Veidt gave up his familys wealth and traveled the world, becoming a self-made man because he was ashamed of his parents Nazi past, which in turn highlighted the themes of the American Dream and the characters duality.   Snyder said Goode "fit the bill.... We were having a hard time casting  , because we needed someone handsome, beautiful and sophisticated, and thats a tough combo."   
*   meets Alberto Vargas." black belt in Kenpō, but described Rorschachs attack patterns as sloppier and more aggressive due to the characters boxing background.    Rorschach appears several times in the movie without his mask before he is apprehended, carrying a placard sign proclaiming, "The End is Nigh," but not until he is unmasked by the police is it made apparent that the sign bearer is Rorschach. Edward Morgan Blake / The Comedian: A superhero who is commissioned by the U.S. government.  When reading the comic for the part, Morgan stopped when he saw his character was killed off three pages in. When telling his agent he did not want the part, he was told to continue reading it and find out how important his character was.  Morgan found the role a challenge, explaining, "For some reason, in reading the novel, you dont hate this guy even though he does things that are unmentionable.   My job is to kind of make that translate, so as a viewer you end up not making excuses to like him, but you dont hate him like you should for doing the things that he does."     Of his casting, Snyder said, "Its hard to find a mans man in Hollywood. It just is. And Jeffrey came in and was grumpy and cool and grizzled, and I was, like, OK, Jeffrey is perfect!" 
*  , which also co-starred Haley. Wilson put on 25&nbsp;lbs. to play the overweight Dreiberg.  He compared Dreiberg to a soldier who returns from war unable to fit into society.  Both Joaquin Phoenix and John Cusack (another fan of the novel) were involved in early projects of the film.

;Supporting cast Moloch
* Stephen McHattie as Hollis Mason/Nite Owl President Richard Nixon

==Production==
  Lawrence Gordon turnaround in 1991,    and the project was moved to Warner Bros., where Terry Gilliam was attached to direct and Charles McKeown to rewrite it. Due to lack of funding — Gilliam and Silver were only able to raise $25 million for the film (a quarter of the necessary budget) because their previous films had gone overbudget  — and Gilliams belief that the comic would have been unfilmable, Gilliam eventually left Watchmen, and Warner Bros. dropped the project. 

 
In October 2001, Gordon partnered with Lloyd Levin and Universal Studios, hiring David Hayter to write and direct.    Hayter and the producers left Universal due to creative differences,  and Gordon and Levin expressed interest in setting up Watchmen at Revolution Studios. The project did not hold together at Revolution Studios and subsequently fell apart.    In July 2004, it was announced Paramount Pictures would produce Watchmen, and they attached Darren Aronofsky to direct Hayters script. Producers Gordon and Levin remained attached, collaborating with Aronofskys producing partner, Eric Watson.  Paul Greengrass replaced Aronofsky when he left to focus on The Fountain.  Ultimately, Paramount placed Watchmen in turnaround. 

In October 2005, Gordon and Levin met with Warner Bros. to develop the film there again.  Impressed with Zack Snyders work on 300 (film)|300, Warner Bros. approached him to direct an adaptation of Watchmen.    Screenwriter Alex Tse drew from his favorite elements of Hayters script,  but also returned it to the original Cold War setting of the Watchmen comic. Similar to his approach to 300, Snyder used the comic book as a storyboard.    Following negotiations, Paramount, which had already spent $7 million in their failed project, earned the rights for international distribution of Watchmen and 25% of the films ownership.   
 Batman & green screens.    Filming started on September 17, 2007,  and ended on February 19, 2008,  on an estimated $120 million budget.    To handle the 1,100 shots featuring visual effects, a quarter of them being computer-generated imagery,  ten different effects companies were involved with Watchmen.   While 20th Century Fox filed a lawsuit to block the films release, the studios eventually settled, and Fox received an upfront payment and a percentage of the worldwide gross from the film and all sequels and spin-offs in return. 

Dave Gibbons became an adviser on Snyders film, but Moore has refused to have his name attached to any film adaptations of his work.    Moore has stated he has no interest in seeing Snyders adaptation; he told Entertainment Weekly in 2008, "There are things that we did with Watchmen that could only work in a comic, and were indeed designed to show off things that other media cant."   While Moore believes that David Hayters screenplay was "as close as I could imagine anyone getting to Watchmen," he asserted he did not intend to see the film if it were made.   

==Music==
  The Times Requiem appears at the end of the film. "Desolation Row" was covered by My Chemical Romance especially for the film, and the song plays in the end credits.

==Release==

===Marketing===
 . Warner Bros. took this low-key approach to avoid rushing the game on such a tight schedule, as most games adapted from films are panned by critics and consumers.  The game is set in the 1970s, and is written by   mobile game featuring Nite Owl and The Comedian fighting enemies in their respective settings of New York City and Vietnam.  On March 6, 2009, a game for the Apple Inc. iPhone and iPod Touch platform was released, titled Watchmen: Justice is Coming. Though highly anticipated, this mobile title suffered from serious game play and network issues which have yet to be resolved. 

As a promotion for the film, Warner Bros. Entertainment released  , a series of narrated animations of the original comic book. The first chapter was released for purchase in the summer of 2008 on digital video stores, such as iTunes Store and Amazon Video on Demand.    DC Direct released action figures based on the film in January 2009.  Director Zack Snyder also set up a YouTube contest petitioning Watchmen fans to create faux commercials of products made by the fictional Veidt Enterprises.  The producers also released two short video pieces online, which were intended to be viral videos designed as fictional backstory pieces, with one being a 1970 newscast marking the 10th anniversary of the public appearance of Dr. Manhattan. The other was a short propaganda film promoting the Keene Act of 1977, which made it illegal to be a superhero without government support. An official viral marketing web site, The New Frontiersman, is named after the tabloid magazine featured in the graphic novel, and contains teasers styled as declassified documents.  After the trailer to the film premiered in July 2008, DC Comics president Paul Levitz said that the company had had to print more than 900,000 copies of Watchmen trade collection to meet the additional demand for the book that the advertising campaign had generated, with the total annual print run expected to be over one million copies.  DC Comics reissued Watchmen #1 for the original cover price of $1.50 on December 10, 2008; no other issues are to be reprinted. 

===Home media=== Tales of the Black Freighter, a fictional comic within the Watchmen limited series, was adapted as a direct-to-video animated feature from Warner Premiere and Warner Bros. Animation, and released on March 24, 2009.    It was originally included in the Watchmen script,    but was changed from live-action footage to animation because of the $20 million it would have cost to film it in the stylized manner of 300 (film)|300 that Snyder wanted;  this animated version, originally intended to be included in the final cut,    was then cut because the film was already approaching a three-hour running time.  Gerard Butler, who starred in 300, voices the Captain in the animated feature, having been promised a role in the live-action film that never materialized.  Jared Harris voices his deceased friend Ridley, whom the Captain hallucinates is talking to him. Snyder had Butler and Harris record their parts together.  International rights to Black Freighter are held by Paramount. 

The Tales of the Black Freighter DVD also includes Under the Hood, a fictional  , was released in digital video stores and DVD on March 3. It included an exclusive scene from the movie but as of press time (prior to the discs release) the scene had yet to be added. 

The film was released on   for PlayStation 3. 

Watchmen debuted at the top of the rental, DVD and Blu-ray charts.  First week sales of the DVD stood at 1,232,725 copies, generating $24,597,425 in sales revenue. As of November 1, 2009 the DVD has sold a total of 2,510,321 copies and $46,766,383 in revenue.   

===Directors cut===
A directors cut of the film running at 186 minutes held a limited release in Los Angeles, Dallas, Minneapolis, and New York City. The directors cut was released on DVD in the United States, along with the theatrical version. The directors cut was also released on Blu-ray, but not the theatrical cut. The theatrical version was released on DVD and Blu-ray in the European area, later on, the directors cut was also released, exclusively on the Blu-ray disc|Blu-ray format.

===Ultimate cut===
In November 2009, a four-disc set was released as the "Ultimate Cut."  This version included the directors cut of the film re-edited to contain Tales of the Black Freighter into the story as it is featured in the graphic novel, bringing the run time of the film to 215 minutes. The set also included two additional hours of bonus features including Under the Hood and The Complete Motion Comic. Originally released only on DVD, the set later became available on Blu-ray.

==Reception==

===Box office=== Star Trek, The Dark Knight.  

Following its first week at the box office, Watchmen saw a significant drop in attendance. By the end of its second weekend, the film brought in $17,817,301, finishing second on that weekends box office. The 67.7% overall decrease is one of the highest for a major comic book film.    Losing two-thirds of its audience from its opening weekend, the film finished second for the weekend of March 13–15, 2009.   The film continued to drop about 60% in almost every subsequent weekend, leaving the top ten in its fifth weekend, and the top twenty in its seventh.  Watchmen crossed the $100 million mark on March 26, its twenty-first day at the box office,  and finished its theatrical run in the United States on May 28, having grossed $107,509,799 in 84 days. The film had grossed one-fifth of its ultimate gross on its opening day, and more than half of that total by the end of its opening weekend. 
 The Hangover, Batman & Robin),  and the thirty-first highest-grossing film of 2009. 

Watchmen earned $26.6 million in 45 territories overseas; of these, Britain and France had the highest box office with an estimated $4.6 million and $2.5 million, respectively.  Watchmen also took in approximately $2.3 million in Russia, $2.3 million in Australia, $1.6 million in Italy, and $1.4 million in Korea.  The film collected $77,743,688 in foreign box office, bringing its worldwide total to $185,253,487. 

===Critical reaction===

The movie has received a polarized reaction from both audiences and critics. Some critics gave it overwhelmingly positive reviews for the dark and unique style on the superhero genre, the cast and the visual effects; while others derided it for the same reasons, as well as the R-rating (for "strong graphic violence, sexuality, nudity, and language"), the running time, and the much-publicized fidelity to the graphic novel.
 average score normalized rating out of 100 to reviews from mainstream critics, the film has received an average score of 56, based on 39 reviews, indicating "mixed or average reviews."    CinemaScore polls reported that the average grade cinemagoers gave the film was B on an A+ to F scale, and that the primary audience was older men.  
 Time Out Sydney gave the film 4/6 in his review of February 25, praising the films inventiveness but concluding, "While Watchmen is still as rich, daring, and intelligent an action film as theres ever been, it also proves Moore absolutely right  . As a comic book, Watchmen is an extraordinary thing. As a movie, its just another movie, awash with sound and fury." 

The negative reviews generally disliked the films   wrote, "Watching Watchmen is the spiritual equivalent of being whacked on the skull for 163 minutes. The reverence is inert, the violence noxious, the mythology murky, the tone grandiose, the texture glutinous."    Donald Clarke of   and The Hollywood Reporter were even less taken with the film. Varietys Justin Chang commented that, "The movie is ultimately undone by its own reverence; theres simply no room for these characters and stories to breathe of their own accord, and even the most fastidiously replicated scenes can feel glib and truncated,"    and Kirk Honeycutt of The Hollywood Reporter writing, "The real disappointment is that the film does not transport an audience to another world, as 300 did. Nor does the third-rate Raymond Chandler|Chandler-esque narration by Rorschach help...Looks like we have the first real flop of 2009."   
 Fight Club, Watchmen would continue to be a talking point among those who liked or disliked the film. Boucher felt in spite of his own mixed feelings about the finished film, he was "oddly proud" that the director had made a faithful adaptation that was "nothing less than the boldest popcorn movie ever made. Snyder somehow managed to get a major studio to make a movie with no stars, no name superheroes and a hard R-rating, thanks to all those broken bones, that oddly off-putting Owl Ship sex scene and, of course, the unforgettable glowing blue penis." 

===Awards=== 2009 VES Awards, seven awards at the 36th Saturn Awards, and 13 awards at the 2009 Scream Awards. The film was also pre-nominated for the Academy Award for Best Visual Effects, although it did not make the final shortlist.

{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! colspan=4 style="background:#B0C4DE;" | Awards
|- align="center"
! style="background: #CCCCCC;" | Award
! style="background: #CCCCCC;" | Category
! style="background: #CCCCCC;" | Recipient(s)
! style="background: #CCCCCC;" | Outcome
|- 36th Saturn Awards
|- Saturn Award Best Fantasy Film
|
| 
|- Saturn Award Best Director Zack Snyder
| 
|- Saturn Award Best Supporting Actress Malin Åkerman
| 
|- Saturn Award Best Writing Alex Tse and David Hayter
| 
|- Saturn Award Best Costume Michael Wilkinson
| 
|- Best Production Design
|
| 
|- Saturn Award Best Special Edition DVD Release
|Watchmen: The Ultimate Cut
| 
|- 2009 Scream Awards
|- Best Fantasy Film
|
| 
|- Best Supporting Actress Carla Gugino
| 
|- Breakout Performance-Female Malin Åkerman
| 
|- Best Esemble
|
| 
|- Best F/X
|
| 
|- Scream Song of the Year Desolation Row" by My Chemical Romance
| 
|- Best Superhero Jackie Earle Haley
| 
|- Billy Crudup
| 
|- Malin Åkerman
| 
|- Most Memorable Mutilation Arms Cut off by Rotary Saw
| 
|- Fight Scene of the Year Ozymandias v. The Comedian
| 
|- Holy Sh!t! Scene of the Year The Destruction of Manhattan
| 
|- Best Comic Book Movie
|
| 
|- Visual Effects 2009 VES Awards Outstanding Animated Character in a Live Action Feature Motion Picture Doctor Manhattan
| 
|}

==References==
 

;Further reading
 
*  
*  
*  
 

==External links==
 
 
*  
*  
*  
*  

*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 