Roosevelt, New Jersey: Visions of Utopia
 
 Roosevelt, New Jersey: Visions of Utopia is a 1983 Documentary film|documentary. Richard Kroehling {{cite web
 | title = Roosevelt, New Jersey  : visions of Utopia
 | publisher =University of Vermont 
 | url =http://voyager.uvm.edu/bibs/bid725426.html
 | accessdate=July 2008}}  served as director and producer. The film explores the story of an independent, farming community of Jews, which was developed on socialist principles and the belief that rural fresh air would benefit the working class population of New York City. Its a modern-day utopia that almost came true for one Jewish village. During the Great Depression, President Franklin Delano Roosevelt (FDR) supported an order to transfer New York’s downstate garment workers to the Jersey Homesteads.
 fifty states of America: a community developed by the domineering leader of the United States, President Roosevelt. Its purpose was to bring serenity and a farm type lifestyle to a select group of the urban industrial workforce. The program was met with huge demand by impoverished factory workers looking for an improved way of life. Workers arrived in full force to take on the opportunity to fill out application forms for the project. Those selected worked together to build a community from scratch. Fields filled with mud were quickly transformed into pristine living spaces, with previously-unsettled lands now hosting avant-garde architecture. This particular environment allowed community, creativity and entrepreneurship to flourish.

Even though Jersey Homesteads was inhabited by hard-working people, the residents also knew how to enjoy themselves. Each evening there was a celebration that involved music, delectable meals and at times a bonfire. The settlement’s environment was welcoming and family-centered. One dweller teased that mothers could send their children into the street, because they were certain neighbors would happily provide them with a filling dinner. “There was such freedom about friendship,” a woman resident boasted, “you could walk into people’s houses and just sit down and enjoy an evening; we loved it—life here was just wonderful.” In the face of mounting anti-Semitism abroad, Jersey Homesteads represented hope for Jews everywhere.

Despite a start full of promise, the experiment ultimately failed when the federal government withdrew funding in 1939 in response to mounting tension in the community. “There were so many different points of view and that to us was humorous and at the same time delightful,” one original Homesteader says, but that once joyful dissidence among community members became worrisome as time moved forward, becoming “fantastic quarrels.”

“I think we learned that Utopias don’t seem to work out because people are not that unselfish,” one man laments. Ultimately, there were too many residents who could not sufficiently contribute to the settlement’s monetary needs and the price of living became too expensive. The Jersey Homesteads experiment may have been a failure, but it was a historic endeavor of America’s Jewish community. The testimonies of the people who lived through it suggest that this type of living situation is possible and can be rewarding. If allowed more time, maybe the residents could have resolved their differences. At the end of the day, Roosevelt, New Jersey wasn’t utopia, but for a brief moment in history it came close.

==Notes==
 

==References==
*{{cite web
 | title = Movie Reviews for Roosevelt, New Jersey: Visions of Utopia 1983 Information and Film Reviews
 | publisher = DVD Movie Reviews 
 | url =http://www.movierevie.ws/movies/101099/Roosevelt-New-Jersey-Visions-of-Utopia.html
 | accessdate=July 2008}}

==External links==
* 
* 

 
 
 
 
 
 
 
 