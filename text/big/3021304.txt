The Little Colonel
{{Infobox film
| name           = The Little Colonel
| image          = littlecolonelVHS.jpg
| caption        = VHS cover David Butler
| producer       = Buddy G. DeSylva
| writer         = Screenplay:   John Lodge Bill Robinson Hattie McDaniel
| music          = Cyril J. Mockridge
| cinematography = Arthur C. Miller
| editing        =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
 David Butler. The screenplay by William M. Conselman was adapted from the childrens novel of the same name by Annie Fellows Johnston, originally published in 1895. It focuses on the reconciliation of an estranged father and daughter in the years following the American Civil War.   The film stars Shirley Temple, Lionel Barrymore, Evelyn Venable, John Lodge, Bill Robinson, and Hattie McDaniel.

The Little Colonel was the first of four cinematic pairings between Temple and Robinson, and features the duos famous staircase dance. The film was well received, and, in 2009, was available on videocassette and DVD in both black-and-white and computer-colorized versions.

==Plot==
Shortly after the American Civil War, southern belle Elizabeth Lloyd (Evelyn Venable) marries a northerner, Jack Sherman (John Lodge). Her father Colonel Lloyd (Lionel Barrymore) disowns her in anger and retaliation. Elizabeth and Jack move west where they become parents of a girl they name Lloyd Sherman.

Six years later, Lloyd Sherman is made an honorary colonel in the Army.  Elizabeth returns to the south with little Lloyd and settles in a cottage near Colonel Lloyd’s mansion while her husband Jack remains in the west prospecting for gold.   When Colonel Lloyd discovers his daughter living in the neighborhood, he treats her with disdain. Little Lloyd learns of her parents’ past from housekeeper Mom Beck (Hattie McDaniel), and, when she meets her grandfather for the first time, throws mud at him. The two eventually become contentious friends.

Elizabeth’s husband returns from the west with a fever.  He has lost everything in his prospecting venture, but the family is saved from complete ruin when the Union Pacific Railroad requests right of way across Jack’s western property.   Jacks former prospecting partners have heard of the Railroad’s offer and try to swindle Jack.  They resort to holding the Sherman couple hostage until the deed to their valuable property is located.

Little Lloyd runs through dark woods for her grandfather but he refuses to help.  He changes his mind when little Lloyd says she never wants to see him again.   They arrive at the cottage just in time to save Elizabeth and Jack.   The film ends with a brief Technicolor sequence featuring a pink party for little Lloyd, her friends, and her reconciled family.

==Cast==
* Shirley Temple as Lloyd Sherman, the daughter of Elizabeth and Jack Sherman, and granddaughter to Colonel Lloyd
* Lionel Barrymore as Colonel Lloyd
* Evelyn Venable as Elizabeth Lloyd Sherman, Jack Sherman’s wife, little Lloyd’s mother, and Colonel Lloyd’s daughter John Lodge as Jack Sherman, Elizabeth’s husband and little Lloyd’s father
* Bill Robinson as Walker, Colonel Lloyd’s butler 
* Hattie McDaniel as Becky Mom Beck Porter, Elizabeth’s housekeeper
* Avonnie Jackson as May Lily, little Lloyd’s friend
* Nyanza Potts as Henry Clay, little Lloyd’s friend

==Production==

The Little Colonel is best known for the famous staircase dance between Robinson and Temple. It was the first interracial dance pairing in Hollywood history and was so controversial that it was cut out in the south. The idea was actually first proposed by Fox head Winfield Sheehan after a discussion with D. W. Griffith. Sheehan set his sights on Robinson, but unsure of his ability as an actor, arranged for a contract that was void if Robinson failed the dramatic test. Robinson passed the test and was brought in to both star with Temple and to teach her tap dancing.  They quickly hit it off, as Temple recounted years later:

 Robinson walked a step ahead of us, but when he noticed me hurrying to catch up, he shortened his stride to accommodate mine. I kept reaching up for his hand, but he hadnt looked down and seemed unaware. Fannie called his attention to what I was doing, so he stopped short, bent low over me, his eyes wide and rows of brilliant teeth showing in a wide smile. When he took my hand in his, it felt large and cool. For a few moments, we continued walking silence. "Can I call you Uncle Billy?" I asked. "Why sure you can," he replied... "But then I get to call you darlin." It was a deal. From then on, whenever we walked together it was hand in hand, and I was always his "darlin."  

During the filming of the movie, Temple drew the ire of veteran actor Lionel Barrymore when she prompted him for one of his lines after he forgot it, causing him to storm off in a fit of anger. Temple was sent off to apologize to Barrymore but instead of directly apologizing, told him she thought he was the best actor in the world and asked for his autograph, defusing the situation and bringing Barrymore onto the set. 

This film made brief usage of early Technicolor film, which required heavy usage of red-hued makeup for the actors. It would be the only time that Temple would wear makeup on the set of her Fox films. 

==Release==

===Critical responses===
Andre Sennwald in his New York Times review of March 22, 1935 thought the film " ll adrip with magnolia whimsy and vast, unashamed portions of synthetic Dixie atmosphere". He further wrote that the film was "so ruthless in its exploitation of Miss Temples great talent for infant charm that it seldom succeeds in being properly lively and gay".  He finished his review noting the audience applauded for a full eleven seconds after the final fade-out, and that the film "ought to bring out the best in every one who sees it." 

===Home media===
In 2009, the film was available on videocassette and DVD in both the original black-and-white version and a computer-colorized version of the original.  Some versions included theatrical trailers and other special features.

==See also==
* Shirley Temple filmography
* Lionel Barrymore filmography

==References==
;Footnotes
 

;Works cited
 
*  
 

;Bibliography
*    In her essay "Cuteness and Commodity Aesthetics: Tom Thumb and Shirley Temple", Lori Merish examines the cult of cuteness in America.

==External links==
*  
*  
*   (includes illustrations for The Little Colonel books by Johnstons friend and neighbor, photographer Kate Matthews)
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 