Rope Hell
{{Infobox film
| name = Rope Hell
| image = Rope Hell.jpg
| image_size = 
| caption = Theatrical poster for Rope Hell (1978)
| director = Kōyū Ohara 
| producer = Yoshiki Yūki
| writer = Kyōhei Konno
| narrator = 
| starring = Naomi Tani
| music = Hajime Kaburagi
| cinematography = Hidenobu Nimura
| editing = Atsushi Nabeshima
| distributor = Nikkatsu
| released = June 24, 1978
| runtime = 69 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1978 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Kōyū Ohara and starring Naomi Tani.

==Synopsis==
Akiko is the heir to a yakuza clan. Hitoshi, who has been kicked out of the rival Hono Clan after attempting to seduce Akiko, kidnaps her at the behest of Hanamura. Hanamura has formed a new gang and intends to use Akiko as a hostage to take over her clans territory. During the torture and abuse sessions which follow, Akiko comes to enjoy the treatment and forsakes her gangland empire.      

==Cast==
* Naomi Tani: Akiko 
* Nami Aoki: Machiko
* Hirokazu Inoue: Saiji
* Hitoshi Takagi: Hitoshi Hanamura
* Kenji Fuji: Gorō

==Background==
Rope Hell was based on   (1977), and had teamed her with her on-screen tormentor in Rope Hell, Hirokazu Inoue in Fairy in a Cage (also 1977).  Both of these films had also based on Dans writings. 

==Critical appraisal==
Allmovie judges Rope Hell to be an inferior film compared to Fairy in a Cage.  In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers also write that Rope Hell is not up to the quality of Ohara and Tanis previous work together, but comment positively on Oharas visuals.  The Weissers judge Oniroku Dans story to be more objectionable than some of his others, with the message that a woman will chose submission and reject self-expression if given the choice, to be clearer than in his other scripts. 

==Availability==
Rope Hell was released theatrically in Japan on June 24, 1978.  It was released to home video in VHS format in Japan on February 6, 1998. 

==Bibliography==

===English===
*  
*   
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 


 
 