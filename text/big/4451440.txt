Whatever It Takes (2000 film)
 
{{Infobox film
| name        = Whatever It Takes
| image       = Whatever it takes.jpg
| caption     = Promotional poster
| director    = David Raynr
| writer      = Mark Schwahn
| starring    = Shane West Marla Sokoloff Jodi Lyn OKeefe James Franco Aaron Paul Julia Sweeney Colin Hanks Kip Pardue Bill Brown Victoria Dee Paul Schiff Mark Schwahn
| studio      = Phoenix Pictures
| distributor = Columbia Pictures
| released    =  
| language    = English
| country     = United States
| runtime     = 94 minutes
| budget      = $32 million
| gross       = $9,902,115
}} America on Cyrano de Bergerac.

==Plot== Cyrano de Bergerac; Ryan composes soulful e-mails for Chris, and Chris advises Ryan to treat Ashley like dirt, which seems to be the only way to get her attention.
At first, neither finds it easy to change their ways; Chris comes on too strong, and Ryan is too nervous to be a jerk. But as they start to succeed, Ryan begins to see Maggie in a new light and wonders if hes pursuing the right girl. He realizes Ashley is not meant for him, and tries to convince Maggie about Chriss affection for her. Maggie is reluctant to take him "back" at first, but then realizes Ryan has a change of heart.

==Cast==
*Shane West as Ryan Woodman
*James Franco as Chris Campbell
*Jodi Lyn OKeefe as Ashley Grant
*Marla Sokoloff as Maggie Carter
*Julia Sweeney as Kate Woodman
*Aaron Paul as Floyd
*Colin Hanks as Cosmo
*Kip Pardue as Harris
*Manu Intiraymi as Dunleavy
*Richard Schiff as P.E Teacher
*Scott Vickaryous as Stu
*Nick Cannon as Chess Club Kid

==Reception==
Whatever It Takes was panned by critics and currently holds a 17% rating on Rotten Tomatoes based on 66 reviews.

===Box office===
The film entered the North American box office at #6, making approximately US$4.1 million in its opening weekend.

==DVD== UK in 2001.

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 