The Boogie Man Will Get You
{{Infobox film
| name           = The Boogie Man Will Get You
| image          = Boogiemanwillgetyou.jpg
| image_size     =
| caption        =
| director       = Lew Landers
| producer       = Colbert Clark
| writer         = Hal Fimberg Robert B. Hunt Edwin Blum Paul Gangelin
| narrator       =
| starring       = Boris Karloff Peter Lorre
| music          =
| cinematography = Henry Freulich
| editing        = Richard Fantl
| distributor    = Columbia Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States English
| budget         =
}} Broadway production Arsenic and Old Lace. As he had done several times previously, Karloff played the part of a "mad scientist", Professor Billings, who is using the basement of his inn to conduct experiments into using electricity to create a race of superhumans. The inn is bought by a new owner, who is initially unaware of the work Billings is conducting.  Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 282-283 

==Plot==
Faced with mortgage debts, Professor Nathaniel Billings ( s to help the war effort. Laydens ex-husband Bill (Larry Parks) is against the sale, but is too late to stop it, and decides to stay on at the inn for a few days.

One night at dinner, the residents hear the sounds of a ghost. Bill suspects that this is part of a plan to scare the new owner away. While investigating, Bill discovers in the basement the dead body of travelling salesman Johnson (Eddie Laughton), an experiment subject who died shortly after the sale. He reports this discovery to the local sheriff Dr Arthur Lorentz (Peter Lorre). After making inquiries, Lorentz realises the potential for profit and decides to work with Billings on a subsequent experiment. Their initial plan is to use Bill as a test subject, but this proves unsuccessful, so they turn their attention to Maxie, a visiting powder puff salesman (Maxie Rosenbloom).

Before the experiments can begin, one of the inns guests is murdered. Billings and Lorentz see the primary suspect as another guest, J. Gilbert Brampton (Don Beddoe), but the police officers who set out to investigate are intercepted on the way. Maxie scares away an intruder known as "Jo-Jo" (Frank Puglia), who is intending to steal Billingss equipment. Billings and Lorentz decide to begin their experiment on Maxie so that they can use him to stop "Jo-Jo" from blowing up a nearby munitions plant. Meanwhile, Brampton informs Winnie and Bill that he is visiting as a representative of the Historical Society of America, who are interested in buying the inn.

When the police officers eventually arrive, they arrest the housekeeper and Ebenezer for the murders. The dead bodies come back to life, having apparently been in a state of suspended animation. The police officers decide to send the rest of the houses inhabitants to the Idlewild Sanatorium, a local psychiatric institution. Pitts, Columbia Pictures, p. 24.  Buehrer, Boris Karloff, pp. 157-158. 

==Cast==
* Boris Karloff as  Prof. Nathaniel Billings 
* Peter Lorre as  Dr. Lorentz  Powder Puff Salesman
* Larry Parks as Bill Layden  Miss Jeff Donnell as Winnie Slade (as Miss Jeff Donnell) 
* Maude Eburne as Amelia Jones 
* Don Beddoe as J. Gilbert Brampton 
* George McKay as Ebenezer 
* Frank Puglia as Silvio Bacigalupi, the Italian Escaped-POW 
* Eddie Laughton as Mr. Johnson 
* Frank Sully as Police Officer Starrett  James Morton as Trooper Frank Quincy Frank Mitchell as Jo-Jo, the Human Bomb

==Production== Broadway play Arsenic and The Man With Nine Lives (1940) and The Devil Commands (1941).  
 
The screenplay was written by Edwin Blum in four weeks, Youngkin, The Lost One, p. 206.  based on a story by Hal Fimberg and Robert B. Hunt, adapted by Paul Gangelin.  Although the film is a comedy-horror, Blum described the studios aims as creating a "goose pimple" film; Blum explained that he "was and still   incapable of writing a straight horror film".  Before filming began, Jack Fier had been named as the producer, but Colbert Clark took over from him.    
 Three Little Pigs and the cat from Pinocchio (1940 film)|Pinocchio. The studio consequently abandoned this publicity idea.  

==Release and reception==
The Boogie Man Will Get You was released in October 1942  to a mixed critical reception. A Variety (magazine)|Variety reviewer commented positively on the film, describing it as a "screwball comedy" offering "hearty laughs". Pitts, Columbia Pictures, p. 25.  According to a reviewer in Kinematograph Weekly, however, the madness of the characters was "a little too studio to promote spontaneous thrills or laughter". Writing in the New York Daily News, Kate Cameron stated that "frightening people in theatres takes more ingenuity and adroitness than the authors of this screenplay put into it". 

==Notes==
;Citations
 
;Bibliography
* 
* 
* 
* 
* 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 