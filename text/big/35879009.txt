The Eleven Schill Officers (1932 film)
{{Infobox film
| name           = The Eleven Schill Officers 
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Meinert
| producer       = 
| writer         = 
| narrator       = 
| starring       = Friedrich Kayßler   Hertha Thiele  Heinz Klingenberg   Hans Brausewetter
| music          = Karl M. May
| editing        = 
| cinematography =  
| studio         = Märkische Film
| distributor    = Märkische Film
| released       = 20 August 1932
| runtime        = 101 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German historical silent film of the same name which had also been directed by Meinert. The film depicts the failed 1809 uprising of Prussian soldiers led by Ferdinand von Schill against the occupying French. It focuses in particular on eleven of Schills officers who were executed by the French at Wesel.  The film was a Prussian film, part of a wider trend of German historical films made during the Weimar Era and set in the Napoleonic Era. 

==Main cast==
* Friedrich Kayßler - Freiherr von Trachtenberg 
* Hertha Thiele - Maria von Trachtenberg 
* Heinz Klingenberg - Fritz von Trachtenberg 
* Hans Brausewetter - Karl Keffenbrink 
* Veit Harlan - Klaus Gabain 
* Camilla Spira - Magd Anna 
* Ernst Stahl-Nachbaur - Hauptmann Busch 
* Theodor Loos - Offizier Hans Küffer 
* Carl de Vogt - Ferdinand von Schill
* Eugen Rex   
* Ferdinand Hart   
* Wera Liessem    Paul Günther   
* Erna Morena
* Elsa Wagner   
* Bernhard Goetzke   
* Hugo Flink   
* Ernst Rückert   
* Ludwig Trautmann   
* Clemens Hasse   
* Hans Halden

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Mustafa, Sam A. The Long Ride of Major Von Schill: A Journey Through German History and Memory. Rowman & Littlefield, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 