Prathyekam Sradhikkukka
{{Infobox film
| name           = Prathyyekom Shradikkuka
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = Renji Mathew
| writer         = Renji Mathew Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Mukesh Rohini Rohini Shalini|Baby Shalini
| music          = Raveendran
| cinematography = Anandakkuttan
| editing        = G Murali
| studio         = Bijis Films
| distributor    = Bijis Films
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Rohini and Baby Shalini in lead roles. The film had musical score by Raveendran.   

==Cast==
*Mammootty as  Suresh Mukesh as Raju Baby Shalini as Mini
*Lalu Alex as SI Jayadevan
*Mala Aravindan as Detective Veerabhadran Priya as Sophia Sulakshana as Nirmala
*Captain Raju as Shukkoor Innocent as Pothachan
*Bob Christo as Christopher
*Jalaja as Shobha
*Kanakalatha
*Babitha as Babitha
*James

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Omanakkaiyil || KS Chithra || Balu Kiriyath || 
|-
| 2 || Udayam prabhachoriyum || K. J. Yesudas || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 