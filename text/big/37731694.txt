The Wind Rises
 
{{Infobox film
| name           = The Wind Rises
| image          = Kaze Tachinu poster.jpg
| alt            = 
| caption        = Japanese theatrical poster
| film name      = {{Infobox name module
| kanji          = 風立ちぬ
| romaji         = Kaze Tachinu
}}
| director       = Hayao Miyazaki Toshio Suzuki
| writer         = Hayao Miyazaki
| based on       =  
| starring       = {{Plainlist|
* Hideaki Anno
* Miori Takimoto Hidetoshi Nishijima
* Masahiko Nishimura
* Steve Alpert
* Morio Kazama
* Keiko Takeshita
* Mirai Shida
* Jun Kunimura
* Shinobu Otake
* Nomura Mansai}}
| music          = Joe Hisaishi
| cinematography = Atsushi Okui
| editing        = Takeshi Seyama
| studio         = Studio Ghibli
| distributor    = Toho
| released       =   
| runtime        = 126 minutes  
| country        = Japan
| language       = {{Plainlist|
* Japanese
* English
* German
* Italian}}
| budget         = United States dollar|$30 million 
| gross          = $134,965,832   
}} animated List historical drama film written and directed by Hayao Miyazaki and animated by Studio Ghibli. It was released by Toho on July 20, 2013 in Japan, and by Touchstone Pictures in North America on February 21, 2014.    
 biopic of Jiro Horikoshi (1903–1982), designer of the Mitsubishi A5M fighter aircraft and its successor, the Mitsubishi A6M Zero, used by the Empire of Japan during World War II. The film is adapted from Miyazakis manga of the same name, which was in turn loosely based on the 1937 short story The Wind Has Risen by Tatsuo Hori.  It was the final film directed by Miyazaki before his retirement in September 2013.  

The Wind Rises was the highest-grossing Japanese film in Japan in 2013 and received critical acclaim. It won and was nominated for several awards, including nominations for the Academy Award for Best Animated Feature, the Golden Globe Award for Best Foreign Language Film, and the Japan Academy Prize for Animation of the Year.

==Plot==
 
In 1918, the young Jiro Horikoshi longs to become a pilot, but knows his nearsightedness prevents it.  One day, he reads about the famous Italian aircraft designer Giovanni Battista Caproni, and dreams about him that night. Caproni tells him that building planes is better than flying them.

Five years later, Jiro is traveling by train to study aeronautical engineering at Tokyo Imperial University,    and meets a young girl, Nahoko, traveling with her maid. When the Great Kanto Earthquake of 1923 hits, Nahokos maid breaks her leg and Jiro carries her to Nahokos family. He leaves without giving his name.

In 1927, Jiro graduates with his close friend Kiro Honjo (who later designs the Mitsubishi G3M), and both begin work at aircraft manufacturer Mitsubishi assigned to design a fighter plane, the Falcon, for the Imperial Army.    During tests in 1928, however, the Falcon breaks apart in mid-air and the Army rejects the aircraft.  Dejected over the seeming backwardness of Japanese technology, Jiro and Honjo are sent to Germany in 1929 to carry out technical research and obtain a production license for a Junkers G.38 aircraft.   Jiro sees Hugo Junkers, argues with German guards and encounters some Nazi thugs.  He dreams again of Caproni, who tells him that the world is better for the beauty of planes, even if humankind might put them to ugly purposes.
 secret police. 

Wanted in connection with Castorp, Jiro hides at his supervisors home while he works on a new navy project. Following a lung hemorrhage, Nahoko recuperates in an alpine sanatorium, but cannot bear being apart from Jiro, and returns to marry him. Jiros sister Kayo, a doctor, warns Jiro that his marriage to Nahoko will end badly as tuberculosis is incurable. Though Nahokos health deteriorates, she and Jiro enjoy their time together.

Jiro leaves for the test flight of his new prototype aircraft, the Mitsubishi A5M. Sensing that she will soon die, Nahoko secretly returns to the sanatorium and leaves letters for Jiro, her family, and friends. At the test site, Jiro is distracted from his success by a gust of wind, sensing Nahokos death.
 Zeros fly past and their pilots salute Jiro. "Not a single one returned," Jiro sadly remarks. Caproni comforts him, saying Jiros dream of building beautiful aircraft was nonetheless realized, though they are ultimately "beautiful, cursed dreams waiting for the sky to swallow them up."  Nahoko appears, exhorting her husband to live his life to the fullest. In tears, Jiro thanks her, and Caproni states that she was beautiful like the wind. He proceeds to invite Jiro to his house for wine, and the credits start.

==Voice cast==
 
{| class="wikitable"
|-
! Character !! Japanese  !! English 
|-
| Jiro Horikoshi
| Hideaki Anno 
| Joseph Gordon-Levitt 
|-
| Nahoko Satomi
| Miori Takimoto
| Emily Blunt
|-
| Kiro Honjo Hidetoshi Nishijima
| John Krasinski
|-
| Kurokawa
| Masahiko Nishimura
| Martin Short
|-
| Castorp Stephen Alpert
| Werner Herzog
|-
| Satomi
| Morio Kazama
| William H. Macy
|-
| Jiros mother
| Keiko Takeshita
| Edie Mirman
|-
| Kayo Horikoshi
| Mirai Shida
| Mae Whitman
|-
| Hattori
| Jun Kunimura
| Mandy Patinkin
|-
| Mrs. Kurokawa
| Shinobu Otake
| Jennifer Grey
|-
| Giovanni Battista Caproni
| Nomura Mansai
| Stanley Tucci
|-
| Kinu
|
| Mae Whitman
|-
| Sone
|
| Elijah Wood
|-
| Mitsubishi employee
|
| Ronan Farrow
|-
| Young Jiro
|
| Zach Callison
|-
| Young Kayo
|
| Eva Bella
|-
| Young Nahoko
|
| Madeleine Rose Yen
|-
| Katayama
|
| Darren Criss
|-
| Flight Engineer
|
| David Cowgill
|-
|}

==Production==
 
The Wind Rises is directed by Hayao Miyazaki, whose previous films include My Neighbor Totoro, Princess Mononoke, and Spirited Away.    It was the first film that Miyazaki solely directed since Ponyo in 2008. 

Miyazaki began to conceive a story to illustrate the life of Jiro Horikoshi in 2008.    He published the story as a manga series in the monthly magazine  Model Graphix from April 2009 to January 2010, with the title borrowed from Tatsuo Hori’s novel The Wind Has Risen ( ).    The story in the manga follows the historical account of Horikoshis aircraft development up to 1935 (the year of the Mitsubishi A5M maiden flight),    and intertwines with fictional encounters with Caproni and Nahoko Satomi ( ).  The scenes with Nahoko in the manga were adopted from the novel The Wind Has Risen,  in which Tatsuo Hori wrote about his life experience with his fiancée, Ayako Yano ( ), before she died from tuberculosis. The name Nahoko Satomi was borrowed from the female protagonist of another novel by Tatsuo Hori, Naoko ( ).  The character of Hans Castorp is borrowed from Thomas Manns novel The Magic Mountain.   
 Toshio Suzuki proposed to adopt the manga The Wind Has Risen instead. At first Miyazaki rejected the proposal because he created the manga as a hobby and considered its subjects not suitable for children, the traditional audience of the feature animations from Studio Ghibli. However Miyazaki changed his objection after a staff member suggested that "children should be allowed to be exposed to subjects they are not familiar with".   

Miyazaki was inspired to make the film after reading this quote from Horikoshi: "All I wanted to do was to make something beautiful".  The Wind Rises: Hayao Miyazakis new film stirs controversy | work=Los Angeles Times | date=15 August 2013 | accessdate=16 August 2013 | author=Keegan, Rebecca}} 

===Music===
The Film score|films score was composed and conducted by Joe Hisaishi, and performed by the Yomiuri Nippon Symphony Orchestra.

The film also includes singer-songwriter Yumi Matsutoyas 1973 song  . Matsutoya has collaborated with Studio Ghibli before in the production for Kikis Delivery Service, which features her songs   and  . Producer Suzuki recommended "Hikōki-gumo" to Miyazaki in December 2012, feeling the lyrics resembled the story of The Wind Rises. 

The Wind Rises soundtrack was released in Japan on July 17, 2013 by Tokuma Japan Communications.   

{{Tracklist
| collapsed = yes
| headline=Tracks 
| title1  =    A Journey (A Dream of Flight)
| length1=2:55
| title2  =    A Shooting Star     | length2=1:37
| title3  =    Caproni (An Aeronautical Designers Dream)
| length3=1:45
| title4  =    A Journey (A Decision)
| length4=1:12
| title5  =    Nahoko (The Encounter)
| length5=0:48
| title6  =    The Refuge
| length6=1:20
| title7  =    The Lifesaver
| length7=0:46
| title8  =    Caproni (A Phantom Giant Aircraft)
| length8=1:42
| title9  =    A Heart Aflutter
| length9=0:39
| title10  =    A Journey (Jiros Sister)
| length10=1:33
| title11  =    A Journey (First Day at Work)
| length11=1:28
| title12  =    The Falcon Project
| length12=1:34
| title13  =    The Falcon
| length13=1:22
| title14  =    Junkers
| length14=1:28
| title15  =    A Journey (Italian Winds)
| length15=1:44
| title16  =    A Journey (Caproni Retires)
| length16=1:19
| title17  =    A Journey (An Encounter at Karuizawa)
| length17=1:44
| title18  =    Nahoko (Her Destiny)
| length18=0:45
| title19  =    Nahoko (A Rainbow)
| length19=1:09
| title20  =    Castorp (The Magic Mountain)
| length20=1:09
| title21  =    The Wind
| length21=0:52
| title22  =    Paper Airplane
| length22=2:37
| title23  =    Nahoko (The Proposal)
| length23=1:10
| title24  =    Prototype 8
| length24=0:58
| title25  =    Castorp (A Separation)
| length25=1:48
| title26  =    Nahoko (I Miss You)
| length26=3:05
| title27  =    Nahoko (An Unexpected Meeting)
| length27=3:04
| title28  =    A Journey (The Wedding)
| length28=1:56
| title29  =    Naoko (Together)
| length29=1:04
| title30  =    A Journey (A Parting)
| length30=1:18
| title31  =    A Journey (A Kingdom of Dreams)
| length31=3:35
| title32  =   
| length32=3:22
}}

Das gibts nur einmal (English: It only happens once) is the German song Hans Castorp sings while playing the piano at Hotel Kusakaru in the film. Jiro Horikoshi and Nahokos father later join the singing. This song is composed by Werner Richard Heymann for the German movie Der Kongreß tanzt.

==Release== The Tale of the Princess Kaguya, another Ghibli film by Isao Takahata, in Japan in mid-2013.    This would have been the first time that the works of the two directors were released together since the release of the films My Neighbor Totoro and Grave of the Fireflies in 1988.  However, Kaguya-hime was delayed until November 23, 2013 and   The Wind Rises was released on July 20, 2013. 

The film played in competition at the 70th Venice International Film Festival.   It had its official North American premiere at the 2013 Toronto International Film Festival,  although a sneak preview of the film was presented earlier at the 2013 Telluride Film Festival (the film screened outside the official program). 
 select cities, with wide release on February 28.  The film was released in the United Kingdom on May 9, 2014 with distribution by StudioCanal. 

===Home media=== Touchstone Home Entertainment released The Wind Rises on Blu-ray Disc and DVD on November 18, 2014. The Wind Rises release includes supplement features with storyboards, the original Japanese trailers and TV spots, a "Behind the Microphone" featurette with members of the English voice cast and a video from when the film was announced to be completed. The audio format for both English and Japanese language are in mono (DTS-HD MA 1.0). 

==Reception==

===Box office===
The film grossed Japanese yen|¥11.6 billion (United States dollar|US$113 million)  at the Japanese box office, becoming the highest grossing film in Japan in 2013. 

===Critical response=== weighted average score, rated the film an 83/100 based on 41 reviews, citing "universal acclaim". 

Film critic David Ehrlich rated the film 9.7/10 and called the film, "Perhaps the greatest animated film ever made". Ehrlich further writes, "While initially jarring, Miyazakis unapologetic deviations from fact help The Wind Rises to transcend the linearity of its expected structure, the film eventually revealing itself to be less of a biopic than it is a devastatingly honest lament for the corruption of beauty, and how invariably pathetic the human response to that loss must be. Miyazaki’s films are often preoccupied with absence, the value of things left behind and how the ghosts of beautiful things are traced onto our memories like the shadows of objects outlined by a nuclear flash. The Wind Rises looks back as only a culminating work can." 

 , Matthew Penney wrote "What Miyazaki offers is a layered look at how Horikoshis passion for flight was captured by capital and militarism", and "(the film) is one of Miyazakis most ambitious and thought-provoking visions as well as one of his most beautifully realized visual projects".   

===Controversy=== Liberal Democratic Party to change the Constitution of Japan, which irritated nationalists.   Liberals were unhappy that a warplane engineer was the films protagonist  and questioned why Miyazaki would make a flattering film about a man who "built killing machines"; others alleged that some who built the planes were Korean and Chinese forced laborers. 

In an interview with the Asahi Shimbun, Miyazaki said he had "very complex feelings" about World War II since, as a pacifist, he felt militarist Japan had acted out of "foolish arrogance". However, Miyazaki also said that the Zero plane "represented one of the few things we Japanese could be proud of&nbsp;–   were a truly formidable presence, and so were the pilots who flew them". 

===Accolades=== Japan Academy Prize in the category of Best Music Score.    It was also selected as "Audience Favorite – Animation" at Mill Valley Film Festival.

{| class="wikitable collapsible collapsed" style="width:100%;"
|-
!List of awards and nominations for The Wind Rises
|-
|style="padding:0; border:none;"|
{|class="wikitable" style="border:none; margin:0; width:100%;"
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Awards  March 2, 2014 Best Animated Feature Toshio Suzuki
|  
|-
| Alliance of Women Film Journalists 
| 19 December 2013
| Best Animated Feature
| Hayao Miyazaki
|  
|-
| rowspan="3"| Annie Awards   
| rowspan="3"| 1 February 2014 Best Animated Feature
| The Wind Rises Studio Ghibli, Touchstone Pictures
|  
|-
| Character Animation in a Feature Production
| Kitaro Kosaka
|  
|-
| Writing in an Animated Feature Production
| Hayao Miyazaki
|  
|-
| Asia Pacific Screen Awards
| 12 December 2013 Best Animated Feature Film
| Toshio Suzuki, Japan
|  
|- Boston Online Film Critics Association 7 December 2013 Best Animated Film
| 
|  
|- Boston Society of Film Critics
| 8 December 2013
| Best Animated Film
|
| 
|- Chicago Film Critics Association
|rowspan=2| 16 December 2013
| Best Foreign&nbsp;— Language Film
|
| 
|-
| Best Animated Feature
|
| 
|-
| 19th Critics Choice Awards|Critics Choice Movie Award   
| 16 January 2014
| Best Animated Feature
|
| 
|-
|Dallas-Fort Worth Film Critics
| 16 December 2013
| Best Foreign Language Film
|
| 
|-
| Denver Film Critics Society   2013 Denver January 13, 2014
| Best Animated Feature
|
| 
|- Florida Film Critics Circle
| 18 December 2013
| Best Animated Feature
|
| 
|-
| Georgia Film Critics Association 
| 10 January 2014
| Best Animated Feature
|
|  
|- 71st Golden Golden Globe Awards  
| 12 January 2014 Best Foreign Language Film
|
| 
|-
| Houston Film Critics Society  15 December 2013 Best Animated Feature
|
|  
|-
| IGNs Best of 2013 Awards 
| 10 January 2014
| Best Animated Movie
|
|  
|-
| Indiana Film Critics Association 
| 19 December 2013
| Best Animated Feature
|
|  
|-
| International Cinephile Society 
| 23 February 2014
| Best Animated Film
| 
|  
|-
| Iowa Film Critics 
| 10 January 2014 Best Animated Feature
|
| 
|- Japan Academy Japan Academy Prize  
|rowspan=2| 7 March 2014 Animation of the Year
|
| 
|-
| Best Music Score
| Joe Hisaishi
|  
|- Las Vegas Film Critics Society December 18, 2013
| Best Animated Feature
|
| 
|- Los Angeles Film Critics Association Los Angeles December 8, 2013
| Best Animation
|
| 
|-
| Mill Valley Film Festival
| 13 October 2013
| Audience Favorite&nbsp;– Animation Hayao Miyazaki
|  
|- National Board of Review
| 7 January 2014 National Board Best Animated Film
|
| 
|-
| New York Film Critics Circle December 3, 2013 Best Animated Film
|
| 
|- New York Film Critics Online
| 8 December 2013
| Best Animated Feature
|
| 
|- New York Film Festival
| 13 October 2013
| Grand Marnier Fellowship Award for Best Film
|
| 
|- Online Film Critics Society 16 December 2013
| Best Picture
|
| 
|-
| Best Director
| Hayao Miyazaki
| 
|-
| Best Animated Feature
|
| 
|-
| Best Film Not in the English Language
|
| 
|-
| Best Adapted Screenplay
| Hayao Miyazaki
| 
|- Phoenix Film Critics Society
| 17 December 2013
| Best Animated Film
|
| 
|- San Diego Film Critics Society
| 17 December 2013
| Best Animated Film
| 
| 

|-
| San Francisco Film Critics Circle   
| 15 December 2013
| Best Animated Feature
|
|  
|-
| San Sebastián International Film Festival
| 20 September 2013
| Audience Award
| 
| 
|-
|St. Louis Gateway Film Critics Association
| 16 December 2013
| Best Animated Feature
|
| 
|- Satellite Awards
| 23 February 2014
| Best Motion Picture, Animated or Mixed Media
|
| 
|-
| Southeastern Film Critics Association  16 December 2013 Best Animated Feature
|
| 
|-
| Toronto Film Critics Association  
| 17 December 2013
| Best Animated Feature
|
|  
|-
| Toronto International Film Festival September 15, 2013
| Peoples Choice Award for Best Drama Feature Film
|
| 
|-
| Utah Film Critics Association 
| 20 December 2013
| Best Animated Feature
|
| 
|-
| Venice Film Festival September 7, 2013
| Golden Lion
|
| 
|- Washington D.C. Area Film Critics Association Washington D.C. December 9, 2013
| Best Animated Feature
|
| 
|-
|rowspan=2| Women Film Critics Circle
|rowspan=2| 16 December 2013
| Best Animated Feature
|
| 
|-
| Best Family Film
|
| 
|}
|}

==See also==
 
* Porco Rosso, a 1992 Ghibli animated film which contains a number of similar thematic elements. The Cockpit, a similar 1993 anime OVA focusing on World War II Axis allegiances, also featuring an emphasis on the warplanes.
* Grave of the Fireflies, another Ghibli anime film from 1988 covering the Japanese perspective on World War II and its effects on civilians. The Aviator, a 2004 live action film about the life and times of Howard Hughes, a rich American plane designer and also accomplished test pilot.

==References==
 

==External links==
 
* {{Official websites
| http://thewindrisesmovie.tumblr.com/ | North America
| http://www.wildbunch.biz/films/the_wind_rises/ | Europe
| http://kazetachinu.jp | Japan
}}
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 