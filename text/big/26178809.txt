Elopement (film)
{{Infobox film
| name           = Elopement 
| image          = 
| image_size     = 
| caption        = 
| director       = Henry Koster
| producer     = Fred Kohlmar
| writer         = Bess Taffel
| narrator       = 
| starring       = Clifton Webb Anne Francis Alfred Newman (uncredited)
| cinematography = Joseph LaShelle
| editing        = William B. Murphy
| distributor    = Twentieth Century Fox
| released       = November 23, 1951
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2 million (US rentals) 
}}
Elopement is a 1951 comedy film directed by Henry Koster.

==Plot==
Jacqueline "Jake" Osborne is sent to college to follow in the footsteps of her successful father Howard, only to fall in love with a professor there and impulsively decide to elope.

Howard and his wife are furious, but when they confront young professor Matt Reagans parents, they find them equally irate. The parents hit the road together hoping to head off the kids marriage plans, while Jake and Matt begin to bicker and wonder if maybe things are going a little too fast.
 
==Cast==
* Clifton Webb as Howard Osborne
* Anne Francis as Jake Osborne
* Charles Bickford as Tom Reagan
* William Lundigan as Matt Reagan
* Reginald Gardiner as Roger

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 