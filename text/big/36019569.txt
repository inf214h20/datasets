Snow White and the Huntsman
 
{{Infobox film name           = Snow White and the Huntsman image          = Snow White and the Huntsman Poster.jpg caption        = Theatrical release poster genre          = Adventure Action Fantasy director       = Rupert Sanders producer       =   screenplay     =   story          = Evan Daugherty based on       = Snow White by the Brothers Grimm starring       = {{Plainlist|
* Kristen Stewart
* Charlize Theron
* Chris Hemsworth
* Sam Claflin
* Sam Spruell
}} music          = James Newton Howard cinematography = {{Plainlist|
* Greig Fraser 
* Gavin Free }} editing        =   studio         = Roth Films distributor  Universal Pictures released       =   runtime        = 127 minutes 
132 minutes (extended version)  country        = United States language       = English budget         = $170 million  gross          = $396,592,829   
}} German fairy Eric the Huntsman that she will bring back his dead wife if he captures Snow White. After the Huntsman shifts his loyalty to Snow White, Ravenna disguises herself and tempts Snow White into eating a poisoned apple. The film is directed by Rupert Sanders, his first feature film, and written by Evan Daugherty, Martin Solibakke, John Lee Hancock, and Hossein Amini.
 Best Visual Best Costume Design at the 85th Academy Awards. It was a success at the box office, earning $396.5 M. Although critics praised the production design and Therons performance, the performances of Hemsworth, Claflin, and Stewart received mixed  reviews, and Daugherty, Hancock and Aminis screenplay was criticized. Rotten Tomatoes consensus was "while it offers an appropriately dark take on the fairy tale that inspired it, Snow White and the Huntsman is undone by uneven acting, problematic pacing, and a confused script." 

==Plot==
While admiring a rose blooming in the winter, Queen Eleanor of the Kingdom of Tabor pricks her finger on one of its thorns. Three drops of blood fall onto the snow-covered ground, and she wishes for a daughter as white as the snow, with lips as red as the blood, hair as black as a ravens wings and a heart as strong and defiant as the rose. The Queen gives birth to Snow White, but then falls ill and dies. After her death, Snow Whites father rescues Ravenna from an invading Dark Army of demonic glass soldiers, becomes enchanted with her beauty, and marries her.

Queen (Snow White)|Ravenna, who is in fact a powerful sorceress and the Dark Armys master, kills Magnus on their wedding night and usurps control of the kingdom. Snow Whites childhood friend William and his father Duke Hammond escape the castle but are unable to rescue her, and she is captured by Ravennas brother Finn, and locked away in the north tower of the castle for many years.
 Magic Mirror Eric the Huntsman, a widower and drunkard, to capture Snow White, promising to bring his wife back to life in exchange. The Huntsman tracks down Snow White, but when Finn reveals that Ravenna does not actually have the power to do what she promised, the Huntsman fights him and his men while Snow White runs away. When the Huntsman catches up with her, she promises him gold if he will escort her to Duke Hammonds castle. Meanwhile, Finn gathers another band of men to find her, and Duke Hammond and his son William learn that she is alive. William leaves the castle on his own to find her, joining Finns band as a archery|bowman.
 eight dwarves namely Beith, Muir, Quert, Coll, Duir, Gort, Nion, and Gus. The blind Muir perceives that Snow White is the daughter of the former king, and the only person who can defeat Ravenna and end her reign.

As they travel through a fairy sanctuary, the group is attacked by Finn and his men. Eric battles Finn and kills him, and William reveals himself and helps defeat Finns men. However, Gus is killed when he sacrifices himself to take an arrow meant for Snow White. William joins the group which continues the journey to Hammonds castle.

Halfway to Duke Hammonds castle, Ravenna disguises herself as William and tempts Snow White into eating a poisoned apple, but is forced to flee when the Huntsman and William discover her. William kisses Snow White, whom he believes to be dead. She is taken to Hammonds castle. As she lies in repose, the Huntsman professes his regret for not saving Snow White, who reminds him of his wife, and kisses her, breaking the spell. She awakens and walks into the courtyard, and rallies the Dukes army to mount a siege against Ravenna.

The dwarves infiltrate the castle through the sewers and open the gates, allowing the Dukes army inside. Snow White confronts Ravenna, but is overpowered. Ravenna is about to kill Snow White and consume her heart, but Snow White uses a move the Huntsman taught her and kills Ravenna, and Duke Hammonds army is victorious. The kingdom once again enjoys peace and harmony as Snow White is crowned Queen.

==Cast==
* Kristen Stewart as Snow White      
** Raffey Cassidy as young Snow White
* Chris Hemsworth as Huntsman (Snow White)|Eric, the Huntsman Queen Ravenna, Snow Whites stepmother Ravenna
* Sam Claflin as William, son of Duke Hammond
** Xavier Atkins as young William
* Lily Cole as Greta, a young girl who befriends Snow White
* Sam Spruell as Finn, Ravennas brother and enforcer
** Elliot Reeve as young Finn
* Vincent Regan as Duke Hammond, Williams father
* Noah Huntley as King Magnus, Snow Whites father
* Liberty Ross as Queen Eleanor, Snow Whites mother Magic Mirror
* Rachael Stirling as Anna
* Hattie Gotobed as Lily
* Greg Hicks as Black Knight General
* Peter Ferdinando as Black Knight
* Anastasia Hille as Ravennas mother

===The Dwarves===
The Dwarves were played by actors of average height who had their faces digitally transmuted onto small bodies. This caused a protest from the Little People of America. 

* Ian McShane as Beith, the leader of the Dwarves.
* Bob Hoskins as Muir, the blind elder Dwarf, who possesses the powers of premonition. This was Hoskinss final role before his retirement from acting (and his death) due to Parkinsons disease. Johnny Harris as Quert, Muirs son.
* Toby Jones as Coll, Duirs brother.
* Eddie Marsan as Duir, Colls brother.
* Ray Winstone as Gort, an ill-tempered Dwarf.
* Nick Frost as Nion,  Beiths right-hand man.
* Brian Gleeson as Gus, the youngest of the Dwarves, who develops a bond with Snow White.

==Development==

===Casting===
  2012 in Anaheim, California on March 2012.]]
 tweets from co-producer Palak Patel that confirmed that Stewart was offered the role.  The tweets also stated that an official confirmation would be coming out later that week, but it would be several weeks before her casting was officially confirmed by the studio. 

At first, Winona Ryder was considered to play Ravenna, before the role went to Charlize Theron.  Tom Hardy was first offered the role of Eric, the Huntsman, but turned down the offer. The role was then offered to Michael Fassbender,  and then Johnny Depp, but both declined as well.  Viggo Mortensen was in negotiations with Universal for the part, but ultimately turned down the role, too.  Hugh Jackman was briefly offered the role, but also declined.    In 2011, Thor (film)|Thor star Chris Hemsworth was eventually cast in the role of the Huntsman. 

===Production===
 
 medieval battles. 
 Breath of Life" exclusively for the film, which was reportedly inspired by Therons character Queen Ravenna.  

==Release==
The film had its premiere on May 14, 2012, at the Empire, Leicester Square, in London. 

===Home media===
The film was released on DVD and Blu-ray Disc|Blu-ray in Region 1 on September 11, 2012.  The film was released on the same formats in Region 2 on October 1, 2012. 

==Reception==

===Box office===
  in June 2012.]]

Snow White and the Huntsman earned $155,136,755 in North America, along with $241,260,448 in other territories, for a worldwide total of $396,397,203.   

In North America, the film earned $1,383,000 from midnight showings.  For its opening day, the film topped the box office with $20,468,525.  It debuted in first place at the box office during its opening weekend with $56,217,700.  It is the seventeenth highest-grossing 2012 film. 

Outside North America, Snow White and the Huntsman had an opening of $39.3&nbsp;million, ranking second overall for the weekend behind Men in Black 3; however, it ranked number 1 in 30 countries. 

===Critical reaction===
  in June 2012.]]

Snow White and the Huntsman received mixed reviews; the film has a 48% "rotten" rating on Rotten Tomatoes, and 55% "mixed" rating among its "top critics"  based on 209 reviews with a consensus reading, "While it offers an appropriately dark take on the fairy tale that inspired it, Snow White & the Huntsman is undone by uneven acting, problematic pacing, and a confused script."  CinemaScore polls conducted revealed the average grade that filmgoers gave the film was a "B" on an A+ to F scale.  The movie received a 57 score from Metacritic, indicating average reviews.
 New York fascist of Minneapolis Star Tribune gave the film 4/4 stars. 

  gave the film two out of four stars and said, "Only Bob Hoskins as the blind seer Muir comes close to making us care. We can almost glean Snow Whites heroic possibilities through his clouded eyes. As much as wed like to, we certainly cant from Stewarts efforts." 

Scott Foundas states that "Stewart’s Snow White... pouts her lips, bats her bedroom eyes, and scarcely seems to have more on her mind than who might take her to the senior prom—let alone the destiny of an entire kingdom."  Richard Roeper gave the movie a B+, calling it "Vastly superior to Mirror, Mirror", and praising Charlize Theron and Kristen Stewarts performances.

==Accolades==
{| class="wikitable sortable"
|+ List of awards and nominations
|-
! Year
! Recipient
! Award
! Category
! Result
|-
| 2012
| Charlize Theron
| Teen Choice Award
| Choice Movie Hissy Fit
|  
|-
| 2012
| Kristen Stewart
| Teen Choice Award
| Choice Summer Movie Star: Female
|  
|-
| 2012
| Chris Hemsworth
| Teen Choice Award The Avengers) 
|  
|-
| 2012
| Sam Claflin
| Teen Choice Award
| Choice Movie Breakout
|  
|-
| 2012
| Charlize Theron
| Teen Choice Award
| Choice Movie Villain
|  
|-
| 2012
| Charlize Theron
| Teen Choice Award
| Choice Summer Movie Star: Female  (also for Prometheus (2012 film)|Prometheus) 
|    (she lost to Kristen Stewart) 
|-
| 2012
| Chris Hemsworth
| GQ Award
| GQ Men Of The Year Award for International Breakthrough
|  
|-
| 2012
| Colleen Atwood
| Gucci Award
| Best Costume Design
|  
|-
| 2012
| Florence and the Machine
| World Soundtrack Awards Best Original Song Written Directly for a Film
|  
|-
| 2012
| Chris Munro and Craig Henighan
| Satellite Award
| Best Sound (Editing & Mixing)
|  
|-
| 2012
| Wild Card and Universal Pictures
| Golden Trailer Award
| Best Action  (for "Forever") 
|  
|-
| 2012
| Universal Pictures
| Golden Trailer Award
| Best Summer Blockbuster 2012 TV Spot  (for "Ravenna") 
|  
|-
| 2012
| Universal Pictures
| Golden Trailer Award
| Best Motion/Title Graphics  (for "Domestic Trailer 2") 
|  
|-
| 2012
| Universal Pictures and Wild Card
| Golden Trailer Award
| Best Summer Blockbuster 2012 TV Spot  (for "Bound") 
|  
|-
| 2012
| Universal Pictures and Aspect Ratio
| Golden Trailer Award
| Best Summer Blockbuster 2012 TV Spot  (for "Kingdom") 
|  
|-
| 2012
| Universal Pictures and Wild Card
| Golden Trailer Award
| Best in Show  ( for "Forever") 
|  
|-
| 2012
| Cedric Nicolas-Troyan, Philip Brennan, Neil Corbould and Michael Dawson
| St. Louis Gateway Film Critics Association
| Best Visual Effects
|  
|-
| 2012
| Greig Fraser 
| San Diego Film Critics Society Awards
| Special Award
|  
|-
| 2012
| Film 
| Peoples Choice Awards
| Favorite Film
|  
|-
| 2013
| Chris Hemsworth
| Peoples Choice Awards The Avengers) 
|  
|-
| 2013
| Charlize Theron 
| Peoples Choice Awards
| Favorite Dramatic Movie Actress  (also for Prometheus (2012 film)|Prometheus) 
|  
|-
| 2013
| Kristen Stewart & Chris Hemsworth 
| Peoples Choice Awards
| Favorite On-Screen Chemistry
|  
|-
| 2013
| Kristen Stewart 
| Peoples Choice Awards
| Favorite Face of Heroism
|  
|-
| 2013
| Colleen Atwood  Academy Awards
| Best Costume Design
|  
|-
| 2013
| Cedric Nicolas-Troyan, Philip Brennan, Neil Corbould and Michael Dawson
| Academy Awards
| Best Visual Effects
|  
|-
| 2013
| Kristen Stewart Golden Raspberry Awards
| Worst Actress  (also for  ) 
|  
|-
| 2013
| Film Saturn Awards
| Best Fantasy Film
|  
|-
| 2013
| Charlize Theron
| Saturn Awards
| Best Supporting Actress
|  
|-
| 2013
| Colleen Atwood
| Saturn Awards
| Best Costume
|  
|-
| 2013
| Cedric Nicolas-Troyan, Philip Brennan, Neil Corbould and Michael Dawson
| Saturn Awards
| Best Special Effects
|  
|- Kristen Stewart||2013 Best Hero|| 
|}

==Prequel==
  Gavin OConnor and Andres Muschietti are on the shortlist to direct the sequel.  On June 26, 2014, Deadline reported that Darabont is in talks to direct the sequel.  Metro-Goldwyn-Mayer and Legendary Pictures will co-finance the film with Universal. On July 31, 2014, it was announced that there would not be a sequel, but a prequel, and it will be titled as The Huntsman, which would not star Stewart as Snow White, and will be released on April 22, 2016.  In January 2015, Darabont left the project as director,  but the third leading role was set with Emily Blunt. It was announced from Schmoes Know that Cedric Nicolas-Troyan will take over as the new director for the film.   Nick Frost will return as Dwarf Nion and Jessica Chastain will star.  On March 18th, 2015, Rob Brydon, Alexandra Roach and Sheridan Smith were added to the cast as dwarves.  

==See also==
  Mirror Mirror, another 2012 film based on the tale of Snow White starring Lily Collins as Snow White and Julia Roberts as the Evil Queen Clementianna, Snow Whites evil stepmother.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 