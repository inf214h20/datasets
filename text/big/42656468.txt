Lekar Hum Deewana Dil
 
 
 
{{Infobox film
| image = Lekar Hum Deewana Dil poster.JPG
| caption = Official poster
| name = Lekar Hum Deewana Dil
| director = Arif Ali
| writer = Arif Ali
| starring = Armaan Jain Deeksha Seth
| producer = Dinesh Vijan Saif Ali Khan
| music = A. R. Rahman
| studio = Eros International Maddock Films Illuminati Films
| cinematography = Laxman Utekar
| editing = Shan Mohammed
| distributor=Eros International
| runtime = 140 minutes 
| released =  
| budget =    
| country = India
| language = Hindi
| gross=   
}} romantic Bollywood Aahista Aahista, Imtiaz Ali. The film has music scored by A. R. Rahman. The film was released on 4 July 2014.  The film received mixed to negative critical reception upon release and was a failure at Indian Box Office.

==Cast==
*Armaan Jain as Dinesh "Dino" Nigam 
*Deeksha Seth as Karishma Shetty
*Kumud Mishra as Pradeep Nigam
*Anita Kulkarni as Meena Nigam
*Sudeep Sahir as Dev Nigam
*Rahul Shetty as Dhananjay Shetty
*Rinku Karmarkar as Mridula Shetty
*Neha Mahajan as Seema
*Akhil Iyer as Mahesh
*Rohini Hattangadi as Judge
*Gautami Kapoor as Marriage Counselor
*Varun Badola as Chacha
*Nishikant Dixit as Film Producer
*Nikita Dutta
*Prabuddha Dayama
*Darius Shroff as Dinos Lawyer
*Jaywant Wadkar as Karishmas Lawyer
*Shravan Mehta
*Zuber K. Khan

==Production==

===Development===
In 2013, reports emerged that director Imtiaz Alis brother Arif Ali was about to make a directorial debut love story. By October 2013, Saif Ali Khan through his official press release confirmed,  "For starters, I am co-producing a film with Armaan Jain. It is being directed by Imtiaz Alis brother Arif Ali." {{cite web|url=http://indiatoday.intoday.in/story/imtiaz-alis-brother-arif-to-direct-kareenas-nephew-armaans-debut-film/1/248355.html|title=Imtiaz Alis brother Arif to direct Kareenas nephew Armaans debut film
|work=India Today|date=31 January 2013}}     Later on inclusion of A. R. Rahman for the films music, producer Dinesh Vijan quoted, "We are excited about our collaboration with A R Rahman too. He has created a young and new sound, which is unlike anything we have heard before."    The film was untitled for over six months prior to official announcements though there were several rounds of discussions amongst the team, suggesting title as Dekho Magar Pyaar Se    Later it was director Karan Johar who came up with title Lekar Hum Deewana Dil, apparently, the chart-busting track of the same name from Nasir Hussains 1973 film Yaadon Ki Baaraat.  In an interview with Press Trust of India the lead actor quoted, "I gave the audition for Lekar Hum Deewana Dil without telling any one in the family. When Arif sir selected me for the film, I called up didi (Kareena Kapoor) and broke the news to her that I am the next actor of their production."    On their roles, Armaan stated, "My character Dino is someone who loves thrill, adventure full of energy and full of life. He is someone who does things without really thinking about it. He does thing and then things about it or probably doesnt even think. So I think the crust of the character is that at some point in time we have always done things without thinking and often it can be a problem, you can make lot of mistakes. So just that carefree attitude, that energy is something I use to have when I was 17 so playing this character just took me back to when I was 17 years. So yeah its just a lively character."  Further, Deeksha noted that the film is realistic. Her character had less or no make up with simple hairstyles and costumes. She had kept her dialogue delivery normal without any filmy touches. A notable aspect, she added that the film warrants that the girl is not garish while the boy doesnt start showing off his abs.  In an interview with The Times of India, actress Nikita Dutta who plays a supporting role in the film said she perceives her character as a sweet and smart girl. She is paired opposite Sudeep Sahir in the film.    

===Filming===
The film was shot with majority of filming in Mumbai. Officially, the shooting began in May 2013.  By September 2013, an estimate forty percent of the filming was completed in Mumbai itself. The next schedule of filming began in the same city. Certain filming took place in Chhattisgarh (wherein one of the longest scenes was shot), Ooty and Chennai.  Filming was completed by February 2014.  Choreographer Rahul Dev Shetty marks his Bollywood debut through this film. Dubbing works of the film were carried out at B. R. Studios in Mumbai. 

===Marketing===
The first looks of the lead actors were revealed on 7 May 2014.  The theatrical trailer was released on 8 May 2014.  It was listed in 2014 May months week top Youtube video in India.  The video teaser of "Khalifa" sung by A. R. Rahman was released on 13 May 2014 and the teaser of song "Maaloom" was released on 19 May 2014.  The first dialogue promo was released on 24 June 2014.  In June 2014, the film was promoted by the lead actors, appearing at television shows- . On this the female lead of the film quoted, "Actually even in the movie my character Karishma adopts a who is a very integral part in the movie and wherever Dino and Karishma go, the dog is with them. So because we felt so strongly towards it and we just felt that this is something we should spread and people should do it because we have a lot of strays in our country."   Yahoo Retrieved. 2 July 2014 

==Music==
  JW Marriott in Mumbai. The concert had A. R. Rahman playing the piano to the vocal performance of singers on their respective songs of the soundtrack.  The event was presided by Imtiaz Ali, Karan Johar, Amit Sadh, Anaita Shroff Adajania, Kapoor family—notably Karisma Kapoor, Kareena Kapoor Rishi Kapoor, Neetu Singh, Randhir Kapoor, Babita apart from the main crew of the film. 

The soundtrack album received mixed to positive critical reception upon release.  

==Critical response==

===India===

Critic Blessy Chettiar for   gave the film 1 star out of 5 and reviewed the main aspects of the film as, "Armaan Jain is too young to face the camera and entertain the audience on the big screen and to hit the right chord, he should wait till he is offered the perfect script that suits his age. The female lead, Deeskha has a good screen presence but has little scope since the writing doesnt give her much potential to show any talent. A. R. Rahmans music is not as melodious and below average just like the film. Director Arif Ali fails to entertain through this stale formula with appalling direction."  Anindita Dev for Zee News wrote, "Young Armaan seems to have made a bad choice for a debut and has a lot to learn before the audience takes him seriously. Arif Alis debut is nothing short of a huge letdown. The movie is not worth wasting money on as neither the story-line not the characters are entertaining.  At Koimoi, critic Manohar Basu gave the film 1 out of 5 stars and felt, "Lekar Hum Deewana Dil is a bad film which is quite a blemish on romantic comedies. Neither romantic nor funny from any angle, it is an awfully boring film."  Shubhra Gupta for The Indian Express gave a score of 0.5 (out of 5) and noted, "Deeksha has little more promise than Armaan Jain".  Anuj Kumar for The Hindu called the film "a dreary escapist fare—Strictly for the cool and the confused ones."  At the India-West critic R. M. Vijayakar gave a headline, "This Silly Tale Is a Waste of Young Talent." He criticised the technical aspects and added, "We feel sorry for these two talents who needed better starts, and sincerely hope that they will be able to live this movie down."  Subha Shetty for Mid Day wrote, "As you go through this 140 plus minutes of love saga, which gets increasingly irritating, you stop caring if they are together or not, because going by the storyline, it seemed like they didnt care either. Jain makes a strictly decent debut, he definitely needs a lot more grooming while Deeksha seems to have the potential though this was not the platform to judge her talent. 

Nishi Tiwari of Rediff called the film A bit of mess and summarised, "Lekar Hum Deewana Dil would have done much better for itself had it tried to find its original voice, instead of pandering to a vague idea of what sells when it comes to love stories." She gave the film 2.5 stars out of 5.  Madhureeta Mukherjee for The Economic Times assigned 2.5 stars out of 5, "The chemistry between the two is affable, but the daily bickering that is cutesy at start, becomes repetitive. Also, the story is far from unique.  On the contrary, Taran Adarsh for Bollywood Hungama felt, "On the whole, LEKAR HUM DEEWANA DIL has several wonderful moments and genuine sparks that stay with you. The film should appeal to its target audience – the youth." 

===Overseas===
Naheed Israr for the newspaper Dawn (newspaper)|Dawn felt, "Lekar Hum Deewana Dil is purely for entertainment – so dont expect more from it. The story may be traditional, but it doesnt let you get bored."  Mumtaz Khusro for News on Sunday noted that Lekar Hum Deewana Dil wasnt a patch on either of the excellent movies like Saathiya (film)|Saathiya or Jaane Tu... Ya Jaane Na and thus, she called the film a bit of embarrassing and rated it 1.5 on 5.  Anupriya Kumar for Reuters noted, "It is an insipid disaster. Go for "Lekar Hum Deewana Dil" if clichéd plots, bad acting and occasional bursts of actual fun do the trick for you. Director Arif Alis Lekar Hum Deewana Dil will try your patience from the word go."  Sneha May Francis of Emirates 24/7 summarised, "While the new star kid (Armaan Jain) on the block is sincere, his romance appears like a bizarre mix of old Bollywood love stories. At two hours and twenty minutes, Arifs predictable love story is a drag, and definitely not the finest." 

==Box office==
The film released on 1300 screens in India.  The film collected   gross on the opening day and   gross after the first three days. 

==References==
 

==External links==
*  

 
 
 
 
 