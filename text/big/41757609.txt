Forget Me Not (1936 film)
{{Infobox film
| name =  Forget Me Not 
| image = "Forget_Me_Not"_(1936).jpg
| image_size =
| caption = German poster
| director = Zoltan Korda
| producer = Alberto Giacalone   Alexander Korda  Hugh Gray   Ernst Marischka   Arthur Wimperis  
| narrator =
| starring = Beniamino Gigli   Joan Gardner   Ivan Brandt   Jeanne Stuart
| music = Mischa Spoliansky   Muir Mathieson
| cinematography = Hans Schneeberger
| editing = Henry Cornelius  
| studio = London Film Productions
| distributor = United Artists
| released = 21 December 1936
| runtime = 73 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} film of London Films.  The film was not generally well received by critics, although they praised Giglis singing. 

==Cast==
* Beniamino Gigli as Enzo Curti 
* Joan Gardner as Helen Carleton  
* Ivan Brandt as Hugh Anderson  
* Jeanne Stuart as Olga Desmond 
* Richard Gofe as Benvenuto Curti  
* Hugh Wakefield as Mr. Jackson   Charles Carson as George Arnold  
* Allan Jeayes as London theatre manager  
* Hay Petrie as New York theatre manager

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 


 