Black List (1995 film)
{{Infobox film
| name           = Black List
| image          = 
| caption        = 
| director       = Jean-Marc Vallée
| producer       = Marcel Giroux
| writer         = Sylvain Guy Michel Côté Geneviève Brouillette Sylvie Bourque
| music          = Serge Arcuri Luc Aubry
| cinematography = Pierre Gill
| editing        = Jean-Marc Vallée
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = 
}} Michel Côté, Geneviève Brouillette, Sylvie Bourque, André Champagne and Aubert Pallascio.

==Plot==
Following the trial of a judge who was found with Gabrielle Angers (Geneviève Brouillette), a prostitute, she gives a list of her clients to a new judge, Jacques Savard (Michel Côté), which contains the names of some very influential judges and politicians. Then, dead bodies and death threats erupt. Jacques is the trial judge and his own life seems to be in danger

==Reception==
Black List was the highest-grossing film in Quebec in 1995.  It received nine nominations at the 16th Genie Awards, which were held on January 14, 1996. 

==Accolades==
{| class="wikitable" style="font-size: 95%;"
|+
! Award
! Category
! Recipients and nominees
! Outcome
|-
| rowspan="9"| 16th Genie Awards Best Motion Picture
| Marcel Giroux
|  
|- Best Performance by an Actor in a Supporting Role
| Aubert Pallascio
|  
|- Best Achievement in Direction
| Jean-Marc Vallée
|  
|- Best Achievement in Screenwriting
| Sylvain Guy
|  
|- Best Achievement in Cinematography
| Pierre Gill
|  
|- Best Achievement in Editing
| Jean-Marc Vallée
|  
|- Best Achievement in Music – Original Score
| Luc Aubry and Serge Arcuri
|  
|- Best Achievement in Sound Editing
| Diane Boucher, François Dupire, Martin Pinsonnault, Louis Dupire, and Alice Wright 
|  
|- Best Achievement in Overall Sound
| Gavin Fernandes, Luc Boudrias, Daniel Masse, and Michel Descombes 
|  
|-
|}

==Notes==
 

==External links==
*  

 

 
 
 
 


 