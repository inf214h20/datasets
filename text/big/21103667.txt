The Rainbow Thief
{{Infobox film
| name = The Rainbow Thief
| image = The Rainbow Thief VideoCover.jpeg
| caption =
| director = Alejandro Jodorowsky
| producer =
| writer = Berta Domínguez D.
| narrator =
| starring = Peter OToole Omar Sharif Christopher Lee
| music = Jean Musy
| cinematography = Ronnie Taylor
| editing = Mauro Bonanni
| distributor = Rink Anstalt
| released =  
| runtime = 87 minutes
| country = England English
| budget =
| gross =
}}
 Lawrence of Arabia co-stars Peter OToole and Omar Sharif in a fable of friendship. Christopher Lee also plays a brief role.

== Synopsis ==

Rudolf Von Tannen is an eccentric millionaire who cares for no one but his dalmatians. One night he welcomes his guests - all of them related to him, expecting to cash in his fortune once he passes away - to a dinner party. The dogs are fed caviar and the people are given bones to eat. This sends them away in anger. Then Rudolfs predilect brothel service arrives, the Rainbow Girls, big-breasted women dressed with the colors of the rainbow. After dancing and partying with them, Rudolf has a heart attack that leaves him comatose.

The relatives gather to argue over the will, but since Rudolf is alive but in a coma, nothing can be done. The relatives suspect that Rudolf will leave all of his fortune to his equally eccentric nephew, Meleagre. Meleagre arrives in time to overhear the back-talk, and walks away unnoticed with his dog Chronos.

Five years later, Meleagre and Dima (a petty thief) live together in the sewerline. Chronos has died. Together they wait for Rudolfs demise and the subsequent inheritance. Dima has set to stealing in order to make a living for the two of them, and takes advantage of carnivals and traveling circuses in order to do so. He has frequent run-ins against a bartender (played by English rock musician Ian Dury), whom he owes large amounts of money, as well as several low-life individuals (a midget, a giant, phony blind beggars) and Ambrosia, a large woman whose love he exploits for money.

One night, as he escapes one of his many persecutors, he reads about Rudolfs demise, and sets out to spend his savings in a dinner with Ambrosia. However, upon close inspection of the newspaper, he finds out that Rudolf has left his entire fortune to the Rainbow Girls (as long as they take care of his dogs). Upset, Dima confronts Meleagre, feeling betrayed by him, although Meleagre argues that the fortune he once promised was not money or gold, but paradise and eternity. Outraged, Dima forsakes him and decides to leave him and the sewers for good by taking a ship to Singapore. Feeling guilty for leaving Meleagre behind, he jumps off the train and hurries back to the sewers, where his friend awaits death with his dogs corpse.

The couple set to find a way out of the flooding sewerlines, but to no good. They eventually reach a ladder leading upwards. Dima manages to climb up to safety. Meleagre happily accepts his fate and hurls himself into a strong current that sweeps him away. Dima climbs up and sits catatonic in the middle of the street for hours, shocked.

In the very end, as Dima walks by the docks, he spots a very much alive Chronos swimming in the water. The dog and the thief reunite and walk happily away by the pier, under a rainbow.

==Production notes== British film. Filming was carried out in Gdańsk, Poland. He was frequently threatened by the producers not to change anything in the script, effectively restraining further artistic involvement on his part. Jodorowsky has since disowned the movie.

It was released in cinemas in London (May 1990), Italy (Il Ladro dellarcobaleno, 1990), France (Le voleur darc-en-ciel, Paris, 1994) and, after, Spain (El ladrón del Arco iris, Cine Doré, Madrid, 2011); but it was never released in American cinemas. 
This movie, along with his previous Tusk (1980 film)|Tusk in 1980, mark his most impersonal work, set far apart from his earlier work. It was discussed along with his other films in the documentary La Constellation Jodorowsky (1994).

==External links==
*  

 

 
 
 
 
 
 
 
 