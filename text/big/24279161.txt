Verdict (film)
 
{{Infobox Film
| name           = Verdict
| image          = Verdict-poster.jpg
| image_size     = 
| caption        = French film poster
| border         = yes
| director       = André Cayatte
| producer       = Carlo Ponti
| writer         = Paul Andréota   André Cayatte
| narrator       = 
| starring       = Sophia Loren   Jean Gabin   Julien Bertheau
| distributor    = Warner-Columbia Film
| released       = 11 September 1974
| runtime        = 95 minutes
| country        = France   Italy
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Italian drama film directed by André Cayatte and starring Sophia Loren, Jean Gabin, Julien Bertheau, Muriel Catalá and Michel Albertini.  A French judge comes under intense personal pressure to acquit a man who is accused of murdering his lover.  It was also released under the title Jury of One.

==Cast==
* Sophia Loren - Teresa Leoni
* Jean Gabin - Leguen, the Judge
* Julien Bertheau - Verlac, the Advocate General
* Muriel Catalá - Anne Chartier
* Michel Albertini - André Leoni
* Gisèle Casadesus - Nicole Leguen
* Henri Garcin - Maître Lannelongue
* Daniel Lecourtois - Le procureur général
* Mario Pilar - Joseph Sauveur

==References==
 

 
 
 
 
 
 
 
 
 


 
 