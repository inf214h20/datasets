The Damned (1963 film)
 
 
{{Infobox film
| name           = The Damned
| image          = The_Damned_1963_movie.jpg
| caption        = Children of Ice And Darkness... They Are the Lurking Unseen Evil You Dare Not Face Alone !
| director       = Joseph Losey
| producer       = Michael Carreras Anthony Hinds Anthony Nelson Keys Evan Jones (adaptation) H.L. Lawrence (novel)
| starring       = Macdonald Carey Shirley Anne Field Oliver Reed Alexander Knox Viveca Lindfors James Bernard Arthur Grant
| editing        = Reginald Mills
| studio         = Hammer Film Productions
| distributor    = Columbia Pictures
| released       = 1963
| runtime        = 96 minutes (cut on release to 87 in UK and 77 in USA)
| budget         = £170,000 
| title          = 
| publisher      = 
| accessdate     =  UK
| English
}} British science Hammer Film production directed by Joseph Losey and based on H.L. Lawrences novel The Children of Light.

==Plot== divorcing  Weymouth he meets Joan, a 20-year-old girl, who lures him into a mugging staged by her biker Teddy Boy brother King and his gang. Wells is beaten up and robbed.

Later Joan approaches Wells on his small boat. While he is prepared to forgive and forget, she implies that he asked for it after trying to pick her up. At that moment King and his gang appear. After they threaten and taunt Wells, he sets off on his boat.  As he pulls away he calls on Joan to join him which she does, defying her over-protective brother.

As they float off of the coast Joan tells Wells of the abuse she suffered at the hands of her brother whenever she gets close to other men, noting a previous time when he locked her in a cupboard. Though Wells urges her to escape with him, she decides to return to shore. Wells heads back, unaware that he is being watched by a member of Kings gang.

At night Wells and Joan return to a quiet part of the mainland and make love in a cliff top house surrounded by curious sculptures. Caught up and surrounded by the gang, the couple escape into a nearby military base where guards and dogs are soon in pursuit of them all.

The house was rented by sculptor Freya Neilson whose lover, Bernard, is a scientist who runs the base. He will not discuss his work, warning her that he "might be condemning her to death".

Wells and Joan make their way down the cliff face. King goes after them. There they discover a small network of caves and bunkers occupied by a group of nine young boys and girls, all aged 11 and born in the same week. Although well-dressed, cared-for and educated, it is clear that they do not know much about the outside world. The childrens skin is also cold to the touch.
 radiation suits. spaceship and are to populate a distant planet.

The children have a small hideout in a connecting cave where they keep pictures and mementos of people they think are their parents. They believe that it is a safe and secret place but Bernard and his associates are well aware of it. Bernard, who is in his own way genuinely fond of the children, has allowed them this degree of privacy in spite of objections by the head of security, Major Holland.

Time passes and Wells, Joan and King feel increasingly unwell but Wells and Joan have promised to help the children escape and pressure King into helping them. The children keep them fed by smuggling food that they have learned to make themselves past the surveillance cameras into the hideout.

Since security has been unable to find the intruders, Bernard tells the children via the TV monitor that he knows their secret and asks that they give up the grown-ups that they are hiding there. He reminds them of a rabbit they once acquired as a pet but which later died and warns them that the same might happen to their would-be rescuers. The children rebel and destroy the cameras.

Men in radiation suits then enter the premises but are overpowered by Wells and King. Using a Geiger counter, Wells discovers that the children are radioactive. Nevertheless he agrees to break them out. They escape the bunker and face the world, the sun and flowers for the first time.  Before they can take it all in, they are rounded up and, kicking and screaming, are taken back to the bunker by other men in radiation suits. Freya Neilson, the sculptor, witnesses the events.

King drives off in a sports car, accompanied by Henry, one of the children. Weakened by the radioactivity, King orders Henry out of the car saying that he is "poison". Henry is then grabbed by pursuers and is forcibly taken back to the bunker in a Sikorsky H-19|helicopter.

Chased by another helicopter, King crashes his car into a river when confronted with a roadblock at the end of a bridge. Joan and Wells are allowed to leave on his boat but the sickness catches up with them and the boat goes round and round in the sea monitored by a helicopter, which will destroy the boat once its occupants are dead.
 nuclear war. Their bodies will resist the nuclear fallout and the human race will continue. Now that Neilson knows his secret, Bernard asks her to join him. She refuses and, true to his word, he kills her.

The film ends with swimmers and beach-goers enjoying the sea and sand, oblivious to the children who are desperately and vainly calling for help from their cliff side prison. These are the damned.

==Production and Release== director Joseph blacklisted by Hollywood. The film was produced by Hammer Films|Hammer, which had enjoyed great success with such science fiction/horror films as The Quatermass Xperiment and The Curse of Frankenstein.
 Evan Jones two weeks prior to filming. Bruce G. Hallenbeck, British Cult Cinema: Hammer Fantasy and Sci-Fi, Hemlock Books 2011 p115 

Losey originally wanted Neilson the sculptor to be killed by one of the helicopters, but the studio insisted that Bernard kill her personally. The studio also wished to tone down the incestuous references between King and Joan.  
 Peter Nicholls   A complete print was released in arthouse cinemas in 2007. On 15 January 2010, it was released on DVD as part of the Icons of Suspense Collection from Hammer Films. 

The movie went over budget by ₤25,000. Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 67 
 Phil Hardy (editor).  The Aurum Film Encyclopedia:  Science Fiction, Aurum Press, 1984.  Reprinted as The Overlook Film Encyclopedia:  Science Fiction, Overlook Press, 1995, ISBN 0-87951-626-7 

==Cast==
*Macdonald Carey .... Simon Wells
*Shirley Anne Field .... Joan
*Viveca Lindfors .... Freya Neilson
*Alexander Knox .... Bernard
*Oliver Reed .... King
*Walter Gotell .... Major Holland
*James Villiers .... Captain Gregory
*Tom Kempinski  ....  Ted 
*Kenneth Cope  ....  Sid  
*Brian Oulton  ....  Mr. Dingle  
*Barbara Everest  ....  Miss Lamont   James Maxwell  ....  Mr. Talbot 
*Nicholas Clay .... Richard

===The children===
*Kit Williams .... Henry
*Rachel Clay .... Victoria
*Caroline Sheldon .... Elizabeth
*David Palmer .... George
*John Thompson .... Charles
*Christopher Witty .... William
*Rebecca Dignam .... Anne
*Siobhan Taylor .... Mary

==References==
 

== External links ==
*  
*   by Dave Kehr, a review in The New York Times, 02-04-2010.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 