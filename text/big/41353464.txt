Kidnapped for Christ
{{Infobox film
| name           = Kidnapped for Christ
| image          = Kidnapped for Christ.jpg
| alt            = 
| caption        = 
| director       = Kate S. Logan
| producer       = {{plainlist|
* Yada Zamora
* Paul Levin
* Kate S. Logan}}
| writer         = {{plainlist|
* Yada Zamora}}
| starring       = 
| music          = Joseph DeBeasi
| cinematography = {{plainlist|
*Peter Borrud
*Stash Slionski}}
| editing        = {{plainlist|
* H. Dwight Raymond III
* Sean Yates}}
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Mike Manning are the executive producers.   

The film premiered at the Slamdance Film Festival in Park City, Utah, in January 2014.  

==Background==
Escuela Caribe, also known as Caribe Vista, was a boarding school for "troubled" teens near the mountain community of Jarabacoa in the Dominican Republic owned by Marion, Indiana-based New Horizons Youth Ministries, an evangelical organization originally headquartered in Grand Rapids, MI.  The school was originally located in Port-au-Prince, Haiti, founded by Pastor Gordon  Blossom in 1971 and known as Caribe Vista Youth Safari. On April 27, 1974 the Haitian Government asked for immediate deportation of 43 Caribe Vista Youth Safari staff and students. US Ambassador to Haiti Heyward Isham cited the ministries failure to maintain visas that had expired for over two years as well as other "dubious" activities as the reason for the unusual immediate deportation. The ministry moved temporarily to La Vega, Dominican Republic. Eventually the school would change names to Caribe Vista/ Escuela Caribe, settling in Jarabacoa on a remote 30-acre fenced campus that typically housed around 45 students at one time.  Blossom developed the schools program, calling it  "Culture Shock Therapy"  or "Christian Milieu Therapy". According to former students of Escuela Caribe, they were subjected to a range of abuses including intense forced labor and repetitive exercise, physical beatings (called "swats"), extreme isolation, and various forms of emotional abuse.  

==Plot==
The documentary details the experiences of several teenagers who were forcibly removed from their homes and sent to Escuela Caribe at their parents behest. The film focuses on the plight of a Colorado high school student, David, sent to the school by his parents after he told them he was gay. The film also documents the experiences of two girls: Beth, who was sent to the school because of a "debilitating anxiety disorder", and Tai, who was sent for behavioral problems resulting from childhood trauma.  

==Development== Jesus Land, who was subjected to the schools abuse in the 1980s.   Manning became involved in the project when one of the subjects mentioned it to him.  In turn, Manning brought it to the attention of Bass and DeSanto. 

Logan made use of a Kickstarter campaign and two Indiegogo campaigns to raise funds.   The Kickstarter campaign, which was to raise funding to complete editing, exceeded its $25,000 goal and raised $34,075 by January 1, 2014.   Logan said that the campaigns were a full-time job, but they gave her a built-in audience that she would not otherwise have had. 

==Release==
A test screening of the film was given at the Sacramento International Gay and Lesbian Film Festival in October 2013. 

It premiered at the Slamdance Film Festival at the Treasure Mountain Inn in Park City, Utah, on January 17, 2014. The film was one of only eight documentaries that were chosen from 5,000 films submitted for the festival.   It won "Audience Award Best Feature Documentary".
 Showtime began showing the film, including on its sister channels Showtime 2 and Showcase and via their on-demand service. 

==See also==
* Conversion therapy
* Latter Days
* List of lesbian, gay, bisexual or transgender-related films of 2014

==References==
{{Reflist|2|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   
}}

==External links==
* 
* 

 
 
 
 
 
 
 
 