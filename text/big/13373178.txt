Climb Dance
{{Infobox film
| name           = Climb Dance
| image          =
| caption        =
| director       = Jean Louis Mourey
| producer       =
| writer         =
| starring       = Ari Vatanen
| music          =
| released       = 1989
| runtime        = 5 min. Automobiles Peugeot
| language       =
| budget         =
| gross          =
}}
 Finnish Rallying|rally driver Ari Vatanen setting a record time in a highly modified four-wheel drive, all-wheel steering Peugeot 405 Turbo 16 GR at the 1988 Pikes Peak International Hillclimb in Colorado, USA. The film was produced by Peugeot and directed by Jean Louis Mourey. The record time set was 10:47.77.  

In 2013 Peugeot commissioned remastered HD version of film in celebration of their contest in Pikes Peak International Hill Climb 2013 edition with rally ace Sébastien Loeb. 
 Easter egg in V-Rally 2.

==Awards==
* Grand Prix Du Film in Festival De Chamonix, 1990
* Silver Screen at US Industrial Film & Video Festival Chicago, 1990
* Prix Special Du Jury at the Festival International Du Film DAventure Val D´Isere, 1990
* Golden Award at the International Film Festival Houston, 1990

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 