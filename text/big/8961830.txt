Fair Game (1986 film)
 
{{Infobox film
| name           = Fair Game
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Mario Andreacchio
| producer       = Harley Manners Ron Saunders
| writer         = Rob George
| narrator       =  Peter Ford Don Barker Carmel Young Adrian Shirley Wayne Anthony
| music          = Ashley Irwin
| cinematography = Andrew Lesnie
| editing        = Andrew Prowse	
| studio         = Southern Films International
| distributor    = 
| released       =  
| runtime        = 90 min.
| country        = Australia
| language       = English
| budget         = A$1.26 million 
| gross          = AU$13,902 (Australia)
| preceded by    = 
| followed by    = 
}}
Fair Game is a 1986 Australian action thriller film directed by Mario Andreacchio from a screenplay by Rob George. Quentin Tarantino enthuses about the movie in the documentary Not Quite Hollywood.

==Plot==
A young woman (Cassandra Delaney) who runs a wildlife sanctuary in the Outback is menaced by three kangaroo hunters who have entered the sanctuary looking for new game.

==Production==
The movie was shot in South Australia with the assistance of the Australian Film Commission. Director Mario Andreacchio later said:
 Fair Game came out of a situation where we were wanting to make a movie that was a B-grade video suspense thriller. I wanted to treat it like comic book violence - it was always like a comic book study of violence. What amazed me and the thing I found quite disappointing was that it started to become a cult film in some parts of the world and people were taking it seriously. And that, for me, became a real turning point. I thought, if people are taking this seriously, then I dont think I can make this sort of material.  

==Box Office==
Fair Game grossed $13,902 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 
 