Beyond Outrage
 
{{Infobox film
| name           = Beyond Outrage
| image          = Beyond Outrage US Poster.jpg
| caption        = US Theatrical release poster
| director       = Takeshi Kitano Masayuki Mori Takio Yoshida
| writer         = Takeshi Kitano
| starring       = Beat Takeshi Toshiyuki Nishida Tomokazu Miura Keiichi Suzuki
| cinematography = Katsumi Yanagishima
| editing        = Takeshi Kitano Yoshinori Ota
| studio         = Bandai Visual Office Kitano TV Tokyo
| distributor    = Magnet Releasing
| released       =  
| runtime        = 112 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = US$16,211,978  
}}

  is a 2012 Japanese yakuza film directed by Takeshi Kitano, starring Kitano (aka "Beat Takeshi"), Toshiyuki Nishida, and Tomokazu Miura. It is a sequel to Kitanos 2010 film, Outrage (2010 film)|Outrage.

==Plot summary==
The film begins with two anti-corruption detectives observing the discovery of dead bodies in a car being recovered from the bottom of a harbor. The detectives suspect that the drowned bodies are related to the recent gang war and power struggle at the huge Sanno-kai crime syndicate, which covers a large portion of eastern Japan. The new Grand Yakuza leader of Sanno-kai, Kato (Tomokazu Miura), must now reform his standing with the powerful rival Hanabishi-kai of western Japan. Otomo (Beat Takeshi), a former Yakuza, is serving time in a maximum security prison after falling out of favor with Sekiuchi, the former Grand Yakuza before Kato murdered him and took control.

When it is finally ascertained by detectives that one of the dead bodies recovered from the harbor in the opening scene was actually the body of a high government official, the departmental Chief of Police strongly suspects that Kato has become too strong and influential, believing he is so protected by his corrupt political ties that he can completely avoid police interference, even after ordering the execution of a government official.  The Chief appoints Special Detective Kataoka - who has previously infiltrated the Sanno-kai syndicate by posing as a corrupt cop - to see if he can ignite further dissent between the already aggressive Sanno-kai Yakuza leaders. Kataoka decides to shorten Otomos prison sentence and have him released, theorizing that his returning presence might stir-up bad memories of old bitter clan rivalries which prevailed before Otomo was imprisoned. This would potentially cause old cracks to resurface in this new and growing version of the old Sanno-kai.

When Kataoka uses his influence to shorten Otomos imprisonment, Kato allows his lieutenants to order his assassination. Otomo, with careful calculation and help from the rival Hanabishi-kai, initiates a ruthless and bloody rampage through the ranks of the Sanno-kai in order to exact his version of justice for having been previously betrayed and forced to serve his prison sentence. His careful calculation pays off as his ruthless campaign to dismantle the new Sanno-kai leadership, including Kato, is efficiently carried out. The decimated Sanno-kai ends up being absorbed into the Hanabishi-kai under their Grand Yakuza leader, who has virtually unified the entire underground of all of Japan into a massive, single and centralized organization.

In the closing scene, Otomo goes to a funeral home where some of his dead Yakuza brothers are being interred. He is surprised in the parking lot by Kataoka, who tells him that all the Yakuza leaders are inside and that he should not enter without a gun. When Otomo replies that he is unarmed, Kataoka - still acting the corrupt cop - offers his own gun. By now, however, it has fully dawned upon Otomo that Kataoka has been playing the two rival crime syndicates against each other in order to weaken and destroy the Sanno-kai. Upon receiving the gun, Otomo empties it into Kataoka at point-blank range, killing him.

==Cast==
 

* Takeshi Kitano (Beat Takeshi) as Otomo
* Tomokazu Miura as Kato
* Ryo Kase as Ishihara
* Fumiyo Kohinata as Detective Kataoka
* Yutaka Matsushige as Detective Shigeta
* Toshiyuki Nishida as Nishino
* Sansei Shiomi as Nakata
* Shigeru Kōyama as Fuse
* Katsunori Takahashi as Jo
* Akira Nakao as Tomita
* Tetsushi Tanaka as Funaki
* Ken Mitsuishi as Gomi
* Tatsuo Nadaka as Shiroyama
* Shun Sugata as Okamoto
* Hideo Nakano as Kimura
* Hirofumi Arai as Ono
* Kenta Kiritani as Shima
* Tokio Kaneda as Korean fixer
* Hakuryu as Korean fixers bodyguard
 

==Release==
Beyond Outrage was screened in competition at the 69th Venice International Film Festival. 

==Soundtrack== Keiichi Suzuki, the same Japanese composer he had used for the original Outrage film, for the complete sequel soundtrack, and previously Kitano had collaborated with him for the complete soundtrack to his Zatōichi (2003 film)|Zatoichi film. This complete soundtrack for Beyond Outrage was their third film collaboration.

==Reception==
Gabe Toro of IndieWire gave Beyond Outrage an "A-" rating.  Justin Chang of Variety (magazine)|Variety described the film as "a slow-motion deathtrap in which the wall-to-wall chatter feels like a joyless, too-leisurely distraction from the inevitable bloodletting". Meanwhile, he commented that Otomo (Beat Takeshi) is "the most memorable figure here, a demon of death shown to brook no nonsense in the films blunt, perfect final scene".  Lee Marshall of Screen International said, "Out-and-out shouting matches between supposedly composed clan members are another forte of Outrage Beyond – a film that always has humour bubbling just underneath its hard-boiled surface". 

Kinema Junpo placed Beyond Outrage at number 3 in their "10 Best Japanese Films of 2012",  while it was ranked at number 36 on the Film Comments "50 Best Undistributed Films of 2012". 

==Sequel==
In September 2012, Takeshi Kitano said that the producers wanted him to make the third Outrage film.  As reported by Macnab, the making of a third Outrage film would complete the first film trilogy for Takeshi Kitano. As of 30 June 2013, Box Office Mojo reported a total revenue for Outrage approaching USD ten million with USD 8,383,891 in the total worldwide lifetime box office.  As of 28 July 2013, Beyond Outrage had receipts more than twice as high, at USD 16,995,152.

==References==
 

==External links==
*    
*  

 

 
 
 
 