Dragonheart
 
{{Infobox film
| name           = Dragonheart
| image          = Dragonheart ver1.jpg
| caption        = Theatrical release poster
| director       = Rob Cohen
| producer       = Raffaella De Laurentiis
| screenplay     = Charles Edward Pogue
| story          = Charles Edward Pogue Patrick Read Johnson
| starring       = Dennis Quaid Sean Connery David Thewlis Pete Postlethwaite Dina Meyer Jason Isaacs Brian Thompson Julie Christie
| music          = Randy Edelman
| cinematography = David Eggby
| editor         = Peter Amundson
| distributor    = Universal Pictures
| released       =    
| runtime        = 103 minutes
| country        = United States Slovakia United Kingdom
| language       = English
| budget         = $57 million
| gross          = $115,267,375   
}}
Dragonheart is a 1996 American   in 2000. A direct-to-video prequel,  , was released in 2015.

==Plot== Saxon prince, Roman castle. Bowen believes that the dragons heart has twisted Einon, and swears vengeance on all dragons.
 the constellation of stars. Unbeknownst to Bowen, Draco is the dragon who shared his heart with Einon, and through this connection, any pain inflicted upon one is also felt by the other.

Meanwhile, Kara, also now an adult, (Dina Meyer) seeks revenge on Einon for murdering her father and is imprisoned. Einon recognizes her as the one responsible for his near-death and attempts to seduce her. Aislinn, disgusted by what her son has become, helps her to escape. Kara tries to rally the villagers against Einon, but they instead offer her as a sacrifice to Draco, who takes her to his lair. Einon arrives to recapture her and fights Bowen, declaring that he never believed in the knights code of honor. Draco intervenes and Einon flees. Kara asks Bowen to help overthrow Einon, but the disillusioned knight refuses.
 con is Knights of namesake constellation, which is a heaven for dragons who prove their worth. He fears that his failure will cost him his soul, and agrees to help Kara and Gilbert against Einon. After experiencing a vision of King Arthur (voiced by John Gielgud) that reminds him of his knightly code, Bowen agrees to help as well.

With Bowen and Draco on their side, the villagers are organized into a formidable fighting force. Aislinn presents Einon with a group of dragon-slayers, secretly knowing that killing Draco will cause Einon to die as well. The villagers are on the verge of victory against Einons cavalry when Gilbert strikes Einon in the heart with an arrow (He states "Thou. Shalt. Not. Kill!", quoting from Exodus 20:13). Draco feels the pain also, falls from the sky, and is captured. Einon realizes that he is effectively immortal as long as Draco remains alive, and determines to keep the dragon imprisoned. Aislinn attempts to kill Draco during the night, but Einon stops her, then he kills her.

The rebels invade Einons castle, and Draco begs Bowen to kill him as it is the only way to end Einons reign. Einon charges at Bowen with a dagger, but Bowen reluctantly throws an axe into Dracos exposed heart. Draco and Einon both die, and Dracos body dissipates as his soul becomes a new star in the constellation. Bowen and Kara go on to lead the kingdom into an era of justice and brotherhood.

==Production==
===Development=== The Skin Butch Cassidy pitch the idea to screenplay writer Charles Edward Pogue, and he agreed to work on the film. De Laurentiis originally intended for John Badham, Rob Cohens then-partner, to work on Dragonheart. According to Cohen, Badham "didnt respond" to the material, so Johnson was then asked to direct the film.   

To be able to stay within the budget that Universal Studios was willing to shell out with Johnson directing, the developers approached Jim Hensons Creature Shop to create the dragon through traditional means. The dragon model was done within eight weeks time, and the crew then went to Shepperton Studios in England to begin shooting the film, starting with the campfire scene. The crew faced difficulties in keeping within the budget. 
 Jurassic Park. 

===Casting===
The principal cast members of Dragonheart were:
* Dennis Quaid as Bowen, a knight who becomes a dragon-slayer and then allies with Draco. Director Rob Cohen was impressed with Quaid, telling producer Raffaella De Laurentiis "  is a knight of the old code." Cohen called Quaid "obviously intelligent and fun to work with", and said that he "really   he   Bowen." Quaid underwent rigorous training for the role, mostly practicing swordfighting. Quaid and Cohen both wanted Bowens sword technique to have an "Eastern flavor", so Quaid trained with Japanese sword master Kiyoshi Yamasaki. 
* Sean Connery as the voice of Draco, the last remaining dragon. Cohen felt it was "very important that   personality be derived from the actor who was going to play the voice", and said that Connery was the only actor he had in mind for the role. He described Connerys voice as "unique" and "instantly recognizable", but said that it was "what   stood for in life as an actor and as a man that most related to what I wanted for Draco." Voice recording for Draco was done in three sessions. To help animate Dracos facial expressions, Cohen and the ILM animators took close-up shots of Connery from his previous films, categorized the clips according to what emotion was being expressed, and put them in separate tapes for easy reference. 
* David Thewlis as Einon, the tyrannical king who shares part of Dracos heart. Cohen cast Thewlis based on his performance in Naked (1993 film)|Naked, stating "what makes a villain scary is the brain, not the brawn."  The young Einon in the films opening scenes was played by Lee Oakes. In the Name of the Father, feeling that "anyone who was assured in a dramatic role could take Brother Gilbert and make it real and charmingly funny." 
* Jason Isaacs as Lord Felton, Einons second in command. He hires Bowen to slay a dragon running rampant around his village, but refuses to pay after learning more of Bowen.
* Julie Christie as Queen Aislinn, Einons mother. Cohen found Christie through David Thewlis casting agent. 
* Dina Meyer as Kara, a peasant girl who seeks revenge on Einon for killing her father. Meyer was the second actress Cohen interviewed for the role. Cohen stated that he needed an actress who was "strong and someone who could, in the end, handle herself with these double viking axes and look believable." 
* Brian Thompson as Brok, Einons knight who served alongside Einons father when he was king. Terry ONeill as Redbeard.
* John Gielgud as the uncredited voice of King Arthur, who speaks to Bowen during his visit to Avalon.

===Dragon design and animation===
Phil Tippett, a visual effects producer specializing in creature design and character animation, and Industrial Light & Magic (ILM) were hired to work on animating Draco for Dragonheart. Tippett states that the responsibilities that his studio had for Dragonheart differed from what they did for Jurassic Park in that they were mostly responsible for the actual look and design of the dragon as well as the storyboards, Blocking (stage)|blocking, and the timing of action sequences. 
 Chinese guardian lion, which Cohen describes as having "a lion-like elegance, a fierceness", and that it is "ultimately a proud...visually powerful creature". He also drew ideas from nature, such as the boa constrictors jaw structure and the musculature of horses. Tippett also took into consideration how the scenes with Draco in it would be framed, the size difference between Draco and the human actors, and what he would actually be doing throughout the film. 

Tippett and his crew created a five-foot model of Draco for lighting reference, and an articulated model that could be used for as a reference for Dracos poses. Because Draco would have more screen time than the CGI dinosaurs in Jurassic Park (23 minutes for Draco, as opposed to six and a half for the dinosaurs), visual effects producer Julie Weaver and her team did a screen test for Dragonheart six to eight months before actual storyboarding, using a "stretched out" version of the Tyrannosaurus Rex from Jurassic Park. 

The film is also notably the first to use ILMs Caricature software. 

===Filming===
Actual filming began in July 1994 in Slovakia. During sequences with Draco and Bowen in them, visual effects supervisor Scott Squires and his teams used what they called a "monster stick"—a pole with a bar and two red circles at the top—as an indicator for where Dracos eyes would be for Quaids reference. They also set up speakers through which Cohen would read Dracos lines for Quaid, which Quaid said "helped   out a lot." 
 microlight as reference, and then edited the footage to "put Draco over the top of that and remove any traces of the aircraft." 

Although Draco is fully rendered in CGI, full-sized models of some of Dracos body parts were used for some of the scenes. One of them was Dracos foot, which was used to pin Bowen to the ground, and the other was Dracos jaw during the scene where Bowen gets trapped inside it. While the foot was a non-moving prop, the jaw had moving parts and was operated by a puppeteer. 

According to Cohen, they spent an additional thirteen months working on the film after making the final cut of the film. He was in Rome to shoot Daylight (film)|Daylight on-location during this period, and had to review animation sequences with ILM and give them his comments and instructions through a satellite hookup. 

===Music and sound=== Seven Years in Tibet, and clip montages at the Academy Awards, making it a well known film score.  MCA Records released the films soundtrack album on May 28, 1996, which contains 15 music tracks.

==Release==
===Box office===
 
 
The film was released in the US and Canada on May 31, 1996, and earned $15,027,150 during its opening weekend. 

===Home video===
Dragonheart was released on   on March 27, 2012.

==Reception==
Based on reviews from 30 critics compiled retrospectively, Rotten Tomatoes gives the film a score of 50% with an average rating of 5.7 out of 10.  Critics praised the premise, visual effects and character development but panned the script as confusing and clichéd.

Roger Ebert gave the film 3 stars out of 4, saying "While no reasonable person over the age of 12 would presumably be able to take it seriously, it nevertheless has a lighthearted joy, a cheerfulness, an insouciance, that recalls the days when movies were content to be fun. Add that to the impressive technical achievement that went into creating the dragon, and you have something to acknowledge here. It isnt great cinema, but Im glad I saw it."   The New York Daily News described the film as "a movie for people young enough to keep dragons in the menageries of their imaginations", and went on to say that "the dragon is the most believable part of the whole movie."  Ken Tucker of Entertainment Weekly gave the movie a positive review, but criticized the fact that Sean Connery provided the voice for Draco, saying that "If only Sean Connery didnt have such a wonderfully distinctive voice, Draco might live and breathe as his own creature." 

===Awards and nominations===
{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result
|- 69th Academy Academy Awards Academy Award Best Effects, Visual Effects Scott Squires, Phil Tippett, James Straus, and Kit West 
|bgcolor="#ffdddd"| Nominated
|- 23rd Saturn Saturn Awards Best Fantasy Film Universal Pictures Won
|- Best Costumes Thomas Casterline and Anna B. Sheppard
|bgcolor="#ffdddd"| Nominated
|- Best Music Randy Edelman
|bgcolor="#ffdddd"| Nominated
|- Best Special Effects Scott Squires, Phil Tippett, James Straus, and Kit West
|bgcolor="#ffdddd"| Nominated
|- Hollywood Film Festival Hollywood Digital Award Scott Squires Won
|- Satellite Awards Satellite Award Outstanding Visual Effects Scott Squires
|bgcolor="#ffdddd"| Nominated
|- Sitges Film Festival Best Film Rob Cohen
|bgcolor="#ffdddd"| Nominated
|- Online Film & Television Association    Best Voice-Over Performance Sean Connery Won
|- Best Sci-Fi/Fantasy/Horror Picture Raffaella De Laurentiis 
|bgcolor="#ffdddd"| Nominated
|- Best Visual Effects Scott Squires, Phil Tippett, James Straus, Kit West 
|bgcolor="#ffdddd"| Nominated
|}

==Other media== Acclaim ported PC version of the game, which received similar criticism.  There was also an original Game Boy game based on the film.

==Prequel and sequel==
A direct-to-video sequel to the film called   was released in 2000 and a prequel called   was released in 2015. According to Matthew Feitshans, screenwriter of Dragonheart 3, a Dragonheart 4 is currently in the works. 

==Legacy== following and cult classic.   The character of Draco also gained popularity, often being ranked as one of the most memorable dragons ever portrayed on film, with fans noting him as ILMs best work on the heels of Jurassic Park and praising Sean Connerys vocal performance.  In 2006, Draco was ranked no. 6 on a top 10 list of movie dragons by Karl Heitmueller for MTV Movie News.  In 2013, WatchMojo.com ranked Draco no. 8 on their list of "Top 10 Dragons  from Movies and TV". 

On various days throughout the year in Toronto, a fully restored "20th anniversary edition" of Dragonheart with never-before-seen footage, enhanced visual effects, and a digitally remastered soundtrack is screened at the AMC Yonge & Dundas 24 theatre. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 