Man with the Steel Whip
{{Infobox film
| name           = Man with the Steel Whip
| director       = Franklin Adreon
| image	=	Man with the Steel Whip FilmPoster.jpeg
| producer       = Franklin Adreon
| writer         = Ronald Davidson Dick Simmons Barbara Bestar Dale Van Sickel Mauritz Hugo Lane Bradford
| cinematography = Bud Thackery
| distributor    = Republic Pictures
| released       =   19 July 1954 {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 138–139
 | chapter =
 }} 
| runtime         = 12 chapters (167 minutes)  English
| country         =  
| budget          = $172,794 (negative cost: $174,718) 
| awards          =
}}
 Republic Movie serial.  It uses considerable stock footage from previous Republic serials "Zorros Black Whip", "The Painted Stallion" and "Daredevils of the West".  As with any serial from the 50s, it is not considered to be one of the best examples of the format.  It uses the standard plot of an illicit "land grab" coupled with a new spin on the Zorro story (as Republic wished to use old stock footage from their Zorro serials but had since lost the licence for that property).

==Plot==
In a version of a standard western plot, the saloon owner Barnett wants the land under the local Indian Reservation for its gold deposits.  In order to drive off the people living on the reservation, he forms a gang to attack the local ranchers and frame the Indians.  Rancher Jerry Randall opposes him using the legendary masked identity of "El Latigo", friend to the Indians.

==Cast== Richard Simmons as Jerry Randall, a rancher, and El Latigo, legendary masked hero
*Barbara Bestar as Nancy Cooper, a school teacher
*Dale Van Sickel as Crane, a henchman
*Mauritz Hugo as Barnett, saloon owner and villain attempting a traditional Western land grab
*Lane Bradford as Tosco
*Pat Hogan as the Indian Chief
*Roy Barcroft as the Sheriff

==Production==
Man with the Steel Whip was budgeted at $172,794 although the final negative cost was $174,718 (a $1,924, or 1.1%, overspend).  It was the most expensive Republic serial of 1954. 

It was filmed between 2 March and 22 March 1954 under the working title Man with a Whip.   The serials production number was 1938. 

Man with the Steel Whip used stock footage from all of the previous Zorro serials produced by Republic Pictures.  The result of this is that the costume and body shape of the hero, El Latigo, keeps changing between scenes, even becoming female in scenes taken from Zorros Black Whip (1944). {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | chapter = 12. The Westerns "Who Was That Masked Man!"
 | page = 300
 }}  {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 5. Shazam and Good-by
 | page = 141
 }} 

The serial contains many mistakes, such as Nancy (Barbara Bester) occasionally referring to the character Jerry Randall as "Dick" (Richard Simmons real name), which were either not noticed or not considered worth correcting before release. 

===Stunts=== Tom Steele as Jerry Randall/Henchman Tom/Henchman Gage (doubling Dick Simmons)
*Babe De Freest as El Latigo (doubling Dick Simmons via stock footage)
*Guy Teague as Price
*Chuck Hayward as Barn Henchman
*Robert Buzz Henry as Orco
*Walt La Rue as a townsman
*Eddie Parker (via stock footage)
*Bill Yrigoyen (via stock footage)
*Joe Yrigoyen (via stock footage)

===Special effects===
Special effects by the Lydecker brothers.

==Release==
===Theatrical===
Man with the Steel Whips official release date is 19 July 1954, although this is actually the date the sixth chapter was made available to film exchanges. 
 The Phantom Rider, re-titled as Ghost Riders of the West, instead of a new serial.  The next new serial, Panther Girl of the Kongo, followed in 1955. 

==Chapter titles==
# The Spirit Rider (20min)
# Savage Fury (13min 20s)
# Mask of El Latiago (13min 20s)
# The Murder Cave (13min 20s)
# The Stone Guillotine (13min 20s)
# Flame and Battle (13min 20s)
# Double Ambush (13min 20s)
# The Blazing Barrier (13min 20s)
# The Silent Informer (13min 20s)
# Window of Death (13min 20s) - a clipshow|re-cap chapter
# The Fatal Masquerade (13min 20s)
# Redskin Raiders (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 256
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{Succession box Republic Serial Serial
| before=Trader Tom of the China Seas (1953 in film|1953)
| years=Man with the Steel Whip (1954 in film|1954)
| after=Panther Girl of the Kongo (1955 in film|1955)}}
 

 

 
 
 
 
 
 
 
 