Aasoo Bane Angaarey
{{multiple issues|
 
 
}}

 
{{Infobox film
| name           = Aasoo Bane Angaarey
| image          = Aasoo Bane Angaarey.jpg
| alt            =  
| caption        = Poster
| director       = Mehul Kumar
| producer       = 
| writer         = 
| based on       =  
| starring       = 
| music          = Rajesh Roshan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 3.11.1993
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1993 Indian drama film starring Bindu (actress)|Bindu, Jeetendra and Madhuri Dixit in lead roles and directed by Mehul Kumar.

==Plot==
After the death of his first wife, Mr. Verma remarries Durgadevi (Bindu (actress)|Bindu), so that she can look after his son, Ravi (Jeetendra). Subsequently, Durga also gets pregnant and gives birth to a son Kiran. After few years as Mr. Verma passes away, Durga decides to enter into politics with the help of Sewakram (Prem Chopra). Ravi becomes the managing director of his firm and falls in love with a typist Usha (Madhuri Dixit). Durgadevi first disagrees with the marriage but finally agrees. After the marriage, Durga stands for election and wins to become the states Chief Minister. Meanwhile Usha becomes pregnant. Ravi, for his business related work, has to travel abroad. Kiran tells his mother that he has had an affair with Usha before she married Ravi. Shocked with this Durgadevi decides to deal with this situation. Usha falls prey to Durgadevi, Kiran and Sewakram.

==Cast== Bindu as Mrs. Durgadevi Verma
* Prem Chopra as Sewakram
* Jeetendra  as Ravi Verma
* Madhuri Dixit  as Usha / Madhu (Double Role)
* Aruna Irani as Radha Verma / Mrs. Shyamsunder
* Anupam Kher as Shyamsunder (Radhas husband) Helen
* Ashok Kumar
* Johnny Lever
* Guddi Maruti
* Suresh Oberoi
* Archana Puran Singh as Chanda

==Music==
The music for the film is composed by Rajesh Roshan.
* "Tuzhe Dekh Ke" - Singer: Sadhana Sargam
* "Teri Rashi Ke"

== External links ==
*  

 

 
 
 