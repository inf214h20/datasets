Sirithu Vazha Vendum
{{Infobox film
| name           = SIRITHU VAZHA VENDUM    
| image          = Sirithu Vazha Vendum.jpg
| image_size     =
| caption        =
| director       = S. S. Balan
| producer       = Manian  Vidwan Ve. Lakshmanan
| writer         = R. K. Shanmugam
| screenplay     = S. S. Balan
| story          = Salim-Javed
| narrator       = Latha M. N. Nambiar Thengai Srinivasan
| music          = M. S. Viswanathan
| cinematography = A. Shammugham
| editing        = M. Umanath
| studio         = Udayam Productions
| distributor    = Udhayam Productions
| released       = 30 November 1974   
| runtime        = 146 mins
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Indian Tamil Tamil  directed by S. S. Balan, starring M. G. Ramachandran in the lead role and Latha (actress)|Latha, M. N. Nambiar among others.

==Plot==

As a child, Ramu (MGR) escaped from killing during the massacre of his parents.

He grows up, haunted by the memory of this horrible night, drawing indefatigably a white horse, resulting from the curb chain bracelet of the mysterious killer.

He finds the murderer some years later on his policemans way with the help of Usthad Abdul Rahman (MGR), (an ex-owner of cabaret), and settle the score with him.

==Cast==
*M. G. Ramachandran as Inspector Ramu (Ramu payya) and Rahman Bai (alias Usthad Abdul Rahman) Latha as Mala, the knife-grinder
*M. N. Nambiar as Nakanraj
*R. S. Manohar as Othai Kannu
*Thengai Srinivasan as "Kedhy" Pakiri
*L. Kanchana
*V. S. Raghavan as De Selva
*V. Gopalakrishnan as The superior of Ramu
*Isari Velan as a bad guy
*V. R. Thilakam as Janaki, Ramu s mother
*S.V.Ramdass as Dayalu
*Vijayalakshmi
*Nalini
*Tirutchi Chelardhar Rajan as Marimuthu, Ramu s father
*Veeraghavan as a police officer, Ramu s foster father

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

It is the remake of Amitabh Bachchan and Jaya Bachchan starrer Zanjeer (1973 film)|Zanjeer (1973) Hindi film.

The film was blockbuster and ran more than 100 days. 

It is the 125th movie of MGR.

It is the only time, when the son of S. S. Vasan, boss of The Gemini Circuit, shows a MGR, in the skin of a violent, fashionable cop in the 70s and in the time-honored expression : " In the unorthodox, fast and muscular methods".

SIRITHU VAZHA VENDUM  has opted for a style that does not differ from  , also in 1973.

In this alarming adventure, MGR puts on both principal roles, policeman Ramu and boss Usthad Abdul Rahman.

There is a great fight between both MGR, where falls at the end an olympic bowl incandescent in pieces.

MGR is dressed one more time magnificently by costume designer M. A. Muthu.

The big actor M. N. Nambiar plays a particularly abominable miserable.

It is a strong and alarming role at the same time.

The title of the movie was inspired by a song of the soundtrack of the success of the previous year, ULAGUM SUTRUM VALIBAN, another MGR Film.

"Konja Neram Ennai..." was intended at first for the movie INAINTHA KAIKAL (with MGR) which does not live finally in the daytime.

==Soundtrack==
The music composed by M. S. Viswanathan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali || 05:35
|- Pulamaipithan || 03:52
|-
| 3 || Ondre Solvaan || T. M. Soundararajan || 05:25
|- Vaali || 03:06
|-
| 5 || Ulagam Ennum || T. M. Soundararajan, P. Susheela, Sheik Muhammed || Pulamaipithan || 05:19
|}

==References==
 

==External links==
 

 
 
 
 


 