Sorry Bhai!
{{Infobox Film
| name           = Sorry Bhai!
| image          = Sorrybhai.jpg
| image_size     = 
| caption        = Movie Poster for Sorry Bhai!.
| director       = Onir
| producer       = Vatsal Seth Sanjay Suri Onir Nikhil Advani Mukesh Talreja
| writer         = Ashwini Malik
| starring       = Sharman Joshi Shabana Azmi Boman Irani Sanjay Suri Chitrangada Singh
| music          = Gaurav Dayal   Vivek Philip
| cinematography = Sachin Kumar
| editing        = Irene Dhar Malik
| distributor    = Puja Films Anticlock Films Mumbai Mantra
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
}}

Sorry Bhai  is a Bollywood film starring Shabana Azmi, Boman Irani, Sanjay Suri, Sharman Joshi and Chitrangada Singh. It was directed by Onir and released on 28 November 2008.

==Plot==

Siddharth Mathur (Sharman Joshi), a shy young scientist, travels to Mauritius for his elder brother Harshs (Sanjay Suri)  wedding. Accompanying him is his mother Gayatri (Shabana Azmi), a reluctant traveller since she is angry at Harsh for deciding to get married without consulting them. Also travelling is Siddharths cheery father Navin (Boman Irani), whose sole entertainment is pulling Gayatris leg. Harsh, pre-occupied with work, can spend little time with his family and it is left to his fiancée Aaliyah (Chitrangada Singh) to show them around Mauritius before the wedding. However, Mas anger at Harsh ensures that she takes an instant dislike for Aaliyah, and it is Aaliyah and Siddharth who end up spending loads of time together. This, added to the fact that Aaliyah feels neglected by the career-obsessed Harsh, leads to them being irresistibly drawn to each other. A horrified Siddharth battles this attraction desperately, but Aaliyah has fallen madly in love and pursues him with single-minded determination. When Siddharth’s defences start crumbling and Ma starts getting suspicious, all hell breaks loose in the Mathur family.

==Cast==

* Sharman Joshi as Siddharth Mathur
* Shabana Azmi as Gayatri Devi  (Mother)
* Boman Irani as Navin (Father)
* Sanjay Suri as Harsh Mathur
* Chitrangada Singh as Aaliyah
* Gaurav Nanda as Student

==External links==
*  

 
 
 
 
 


 