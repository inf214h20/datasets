Grey Gardens
 
 
{{Infobox film
| name           = Grey Gardens
| image          = Grey Gardens (1975 film) poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Albert Maysles David Maysles Ellen Hovde Muffie Meyer
| producer       = Albert Maysles David Maysles Associate Producer - Susan Froemke Edith "Big Edith "Little Edie" Bouvier Beale
| cinematography = Albert Maysles David Maysles
| editing        = Ellen Hovde Muffie Meyer Susan Froemke
| studio         = Portrait Films
| distributor    = Portrait Films
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
}}
 reclusive socialites, Grey Gardens, East Hampton, New York. The film was screened at the 1976 Cannes Film Festival but was not entered into the main competition.   
 Vanity Fair Gimme Shelter and Salesman (film)|Salesman) and Muffie Meyer. 

In 2010 the film was selected by the Library of Congress for preservation in the United States National Film Registry as being "culturally, historically, or aesthetically significant". In a 2014 Sight and Sound poll, film critics voted Grey Gardens the joint ninth best documentary film of all time.   

==Cast== Edith "Big Edie" Ewing Bouvier Beale as Herself Edith "Little Edie" Bouvier Beale as Herself
* Brooks Hyers as Himself - Gardener
* Norman Vincent Peale as Himself (voice)
* Jack Helmuth as Himself - Birthday Guest (uncredited)
* Albert Maysles as Himself (uncredited)
* David Maysles as Himself (uncredited)
* Jerry Torre as Himself - Handyman (uncredited)
* Lois Wright as Herself - Birthday Guest (uncredited)

==Background== Grey Gardens estate, for decades with limited funds in increasing squalor and isolation. 

The house was designed in 1897 by Joseph Greenleaf Thorpe and purchased in 1923 by "Big Edie" and her husband Phelan Beale. After Phelan left his wife, "Big Edie" and "Little Edie" lived there for more than 50 years. The house was called Grey Gardens because of the color of the dunes, the cement garden walls, and the sea mist. 
 New York Suffolk County Health Department. With the Beale women facing eviction and the razing of their house, in the summer of 1972 Jacqueline Onassis and her sister Lee Radziwill provided the necessary funds to stabilize and repair the dilapidated house so that it would meet village codes.

Albert and David Maysles became interested in their story and received permission to film a documentary about the women, which was released in 1976 to wide critical acclaim. Their direct cinema technique left the women to tell their own stories.

==Soundtrack== Tea for Two" (music by Vincent Youmans and lyrics by Irving Caesar)
* Edith Bouvier Beale – "We Belong Together" from "Music in the Air" (lyrics and book by Oscar Hammerstein II and music by Jerome Kern) 
* Edith Bouvier Beale – "You and the Night and the Music" (music by Arthur Schwartz and lyrics by Howard Dietz) Night and Day" (written by Cole Porter)
* Edith Little Edie Bouvier Beale – "People Will Say Were in Love" (music by Richard Rodgers and Oscar Hammerstein II)
* Edith Little Edie Bouvier Beale – "Lili Marleen"

==Aftermath== Town & Country, after their purchase, Quinn and Bradlee completely restored the house and grounds.

Jerry Torre, the handyman shown in the documentary, was sought by the filmmakers for years afterward, and was found by chance driving a New York City taxicab.  He is now a sculptor at The Art Students League of New York and a documentary is being made about his life by Jason Hay and Steve Pelizza of Aggregate Pictures.  

Lois Wright, one of the two birthday party guests in the film, has hosted a public television show in East Hampton since the 1980s. She wrote a book about her experiences at the house with the Beales. 
 limited theatrical release.

==Adaptations==

===Musical theatre===
 
The documentary, and the womens story, were adapted as a full-length musical, Grey Gardens, with book by Doug Wright, music by Scott Frankel and lyrics by Michael Korie. Starring Christine Ebersole and Mary Louise Wilson, the show premiered at Playwrights Horizons in New York City in February 2006.  The musical re-opened on Broadway in November 2006 at the Walter Kerr Theatre, and was included in more than 25 "Best of 2006" lists in newspapers and magazines.   The production won a Tony Award for Best Costume Design, and Ebersole and Wilson each won Tony Awards for their performances.  The Broadway production closed on July 29, 2007.  It was the first musical on Broadway ever to be adapted from a documentary. 

===Television film===
 
Grey Gardens, an HBO film, stars Jessica Lange and Drew Barrymore as the Edies, with Jeanne Tripplehorn as Jacqueline Kennedy, and Daniel Baldwin as Julius Krug. Directed and co-written (with Patricia Rozema) by filmmaker Michael Sucsy, filming began on October 22, 2007, in Toronto.  It flashes back and forth between Little Edies life as a young woman and the actual filming/premiere of the 1975 documentary. First aired on HBO on April 18, 2009, the film won six Primetime Emmys  and two Golden Globes. 

==References in other works==
 
Grey Gardens has been referenced on television, in songs and even in the fashion world.  In 1999, fashion photographer Steven Meisel shot an editorial, with the same name, featuring Amber Valletta, for Vogue Italia.  Rufus Wainwrights song "Grey Gardens" appears on his 2001 album Poses (album)|Poses.  Canadian rock band Stars (Canadian band)|Stars song "The Woods" from their album Heart (Stars album)|Heart contains samples of dialogue from the film.  

On   more than once: in season 4, contestant   participated in a Match Game-style challenge impersonating Little Edie. 

On The L Word, season 2, episode 4 "Lynch Pin" (2005), character Mark Wayland refers Grey Gardens as one of his favorite documentaries and character Jenny Schecter also talks highly about it. 

<!-- ANY OF THESE THAT ARE MERE PASSING REFERENCES SHOULD BE DELETED. RETAIN ONLY ITEMS WHERE THERE IS A SUBSTANTIAL or ongoing reference to Grey Gardens; and in each case, a reference must be added that mentions what was said or done in the show concerning Grey Gardens.
*  
* Castle (TV series)|Castle – season 3, episode 23: "Pretty Dead"
* Gilmore Girls
**season 3, episode 9: "A Deep-Fried Korean Thanksgiving"
**season 4, episode 7: "The Festival of Living Art"
* The L Word – season 2, episode 4: "Lynch Pin"
* Rugrats – season 2, episode 15: "The Case of the Missing Rugrat"
* House (TV series)|House – season 7, episode 12: "You Must Remember This"
* 90210 (TV series)|90210 – season 3, episode 19: "Nerdy Little Secrets" New Girl – season 2, episode 8: "Parents"
* One Life to Live – Dorian dreams of her future life with Blair, 2010 Bored to Death, season 2, episode 6: "The Case of the Grievous Clerical Error!" (2010)
 -->

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  ". Criterion Collection essay by Hilton Als
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 