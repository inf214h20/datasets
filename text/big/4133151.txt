Sleepaway Camp II: Unhappy Campers
 
{{Infobox film
| name           = Sleepaway Camp II
| image          = Sleepaway Camp 2.jpg
| writer         = Fritz Gordon Robert Hiltzik (characters)
| starring       = Pamela Springsteen Renée Estevez Susan Marie Snyder Valerie Hartman Tony Higgins Brian Patrick Clarke Walter Gotell
| music          = James Oliverio
| director       = Michael A. Simpson
| runtime        = 80 minutes
| language       = English
| country        = United States
| budget         = $465,000
| released       =  
}}

Sleepaway Camp II: Unhappy Campers also known as Nightmare Vacation II is a 1988 horror-comedy film and a sequel to the movie Sleepaway Camp, written by Fritz Gordon and directed by Michael A. Simpson. Unlike Sleepaway Camp, Sleepaway Camp II pokes fun at various horror films. It stars Pamela Springsteen, Renée Estevez, and Tony Higgins.

==Plot==
T.C., the head counselor at Camp Rolling Hills, is at a campfire with the male campers Sean, Rob, Anthony, Judd, Charlie and Emilio. Also there is Phoebe, who has sneaked away from her cabin to be with the boys. As Phoebe tells the story about the killings of the previous film at Camp Arawak, her head counselor Angela appears and forces her to go back to the cabin. After the pair get into an argument, Phoebe becomes lost, only to be attacked by Angela who hits her over the head with a log before cutting her tongue out.
 cremated sister. Angela then pours gasoline over Brooke and burns her alive. Meanwhile, Molly and Sean start a relationship, despite Allys attempts to seduce Sean.

That night, the boys throw a panty raid in the girls cabin until Angela comes in, throwing them out. Later, the girls raid the boys cabin. T.C allows the fun to go on, but Angela appears and witnesses Mare showing off her breasts. Mare decides she wants to leave the camp, so gets Angela to drive her. While traveling, Angela gives Mare one last chance to apologize, however when she fails to do so Angela murders her with an electric drill. The following day, Angela discovers Charlie and Emilio looking at naked pictures of various campers they had taken in secret. That night, the girls go camping, giving Anthony and Judd a chance to scare Angela, dressed as Freddy Krueger and Jason Voorhees. The plan backfires however, when Angela dressed as Leatherface slashes Anthonys throat with a clawed glove before murdering Judd with a chainsaw. After finding out Ally has walked away to have sex with Rob, Angela plans to murder the pair but her plan fails.

The next day, Angela sets up a trap for Ally, leaving her a fake note from Sean. Ally goes to meet Sean, but instead meets Angela, who stabs Ally in the back before forcing her down the outhouse toilet hole, drowning her in feces and leeches. That night, Demi reveals to Angela that she phoned the families of the girls who had been sent home by Angela, but they denied the girls being sent home. Realizing she could be caught, Angela strangles Demi to death with a guitar string, before stabbing Lea to death when she finds Demis body. The next day, Uncle John and T.C fire Angela for not telling Uncle John about sending the many campers home.

Feeling sad for an upset Angela, Molly and Sean go into the woods to cheer her up. However the pair discover the bodies of the other campers in a cabin, before Angela ties them up. Back at camp, Rob tells T.C the whereabouts of Molly and Sean, prompting T.C to go off to fetch them. He enters the cabin, only for Angela to throw battery acid on his face, killing him. Sean realizes Angela is Peter Baker, the murderer from the previous movie. Angela reveals that after two years of electroshock therapy and a sex change, she was released for having good records from doctors. She then proceeds to decapitate Sean. Angela leaves the cabin, allowing Molly to free herself and upon Angela entering, knock her out and escape. Angela chases after Molly through the woods, who finally falls off a rock and is presumed dead.

Later that night, camp counselor Diane discovers the dead bodies of Charlie, Emilio, Uncle John and Rob, before she is stabbed to death by Angela. Meanwhile, Molly regains consciousness and begins to make her way out of the forest. Angela hitchhikes with a truck driver, who quickly annoys her, making Angela stab her to death. As Molly makes it out of the woods, she finds the truck, but is horrified to find that Angela is the driver. The movie ends with Molly screaming and presumably killed by Angela.

==Cast==
* Pamela Springsteen as Angela Johnson / Angela Baker
* Renée Estevez as Molly Nagle
* Tony Higgins as Sean (credited as Anthony Higgins)
*   as Ally Burgess
* Brian Patrick Clarke as T.C.
* Walter Gotell as Uncle John
* Susan Marie Snyder as Mare
* Terry Hobbs as Rob Darrinco
* Kendall Bean as Demi
* Julie Murphy as Lea
*   as Brooke Shote
* Amy Fields as Jodi Shote
* Benji Wilhoite as Anthony
* Walter Franks III as Judd
* Justin Nowell as Charlie
*   as Phoebe
* Jason Ehrlich as Emilio
* Carol Martin Vines as Diane
*   as Girl Sent Home
* Jill Jane Clements as Woman in Truck

==Production==
A YMCA youth camp in Waco, Georgia was used for the fictional Camp Rolling Hills.  Filming began and wrapped in 1987.

==Releases==
Like Sleepaway Camp, Sleepaway Camp II was released theatrically on a limited release| limited basis.  It was released in theaters on August 26, 1988 before being released on VHS in the United States by Nelson Entertainment two months later. 

The film has been released twice on DVD in the United States by Anchor Bay Entertainment.  The first release was in 2002 with a single DVD edition,  as well as in the Sleepaway Camp Survival Kit.   Both these releases are currently out of print.

Anchor Bay Entertainment also released this movie on DVD in the United Kingdom on the 31st May 2004,  whilst Futuristic Entertainment released it on VHS in the nineties under the alternate title "Nightmare Vacation 2";  also release on DVD via 23rd Century Home Entertainment.

  for the first time on Blu-ray disc on June 9th, 2015. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 