Pythons 2
 
{{Infobox television film name           =Pythons 2 caption        =
| image	=	Python II FilmPoster.jpeg director       =Lee McConnell  producer       =Jeffrey Breach  Phillip Roth  writer         =Jeff Rank  starring       =William Zabka  Dana Ashbrook   Alex Jolig  Simmone Jade Mackinnon  music          =Rich McHugh  cinematography =Azusa Ohno  editing        =David Flores studio         =UFO/Unified Film Organization and Python Productions network  Sci Fi Channel released       =  runtime        =90 minutes country        =United States language       =English
}} Sci Fi Sci Fi Billy Zabka, reprising his role as Greg Larson from the first film, Dana Ashbrook and Simmone Jade Mackinnon. Directed by Lee McConnell, it was produced by  Jeffery Beach and Phillip Roth for UFO/Unified Film Organization and Python Productions. This film was dedicated to Janine Clark, who died some time in 2001.

==Plot==
One of a pair of giant bioengineered pythons with acidic venom and armor-like skin, created by a Russian and American joint military operation led by U.S. Army Colonel Robert Evans Jefferson, Jr. (Marcus Aurelius), escapes into Russias Ural Mountains. Jefferson and his team go to retrieve it, and find disaster in a clandestine Russian military base where the creature slaughters soldiers and scientists alike.

American Dwight Stoddard (Dana Ashbrook) and his Russian wife Nalia (Simmone Jade Mackinnon) run a shipping business in Russia. Greg Larson (Billy Zabka, reprising his role from Python) hires them to move some mysterious container, which is holding another larger python, and they reach the isolated and deserted Russian military base. Larsons primary concern is retrieving the snakes DNA, and he begins to kill his own men in the attempt. After being discovered about his true intentions by Dwight and Nalia, Larson engages Dwight in a fist fight. During the fight, Larson gets suffocated by the first snake and dies while begging for help. The snake is killed when an explosive is hurled into its mouth by Dwight, while the other snake chases Dwight and Nalia through a land mine field and gets itself blown up by the explosives. Dwight and Nalia survive, and are rescued by Russian soldiers.

== Cast ==
Source: 
* William Zabka as Greg Larson 
* Dana Ashbrook as Dwight Stoddard	  	 
*Alex Jolig as Matthew Coe	  	 
*Simmone Jade Mackinnon as Nalia	  	
* Marcus Aurelius as Col. Robert Evans Jefferson, Jr. 	  	 
*Mihail Miltchev as Hewitt	  	 
*Vladimir Kolev as Vladi	  	 
*Kiril Efremov as Boyce	  	 
*Raicho Vasilev as Dirc	  	 
*Vadko Dimitrov as McKuen	  	 
*Anthony Nichols as Kerupkot  	 
*Velizar Binev as Aziz	  	 
*Tyron Pinkham as Pilot	  	 
*Sgt. Robert Sands as Co-pilot	  	 
*Maxim Gentchev as Old soldier	  	 
*Hristo Shopov as Doctor	  	
*Ivailo Geraskov as Col. Zubov	  	 
*Ivan Burney as Russian soldier 1
*Georgi Ivanov as Russian soldier 2	  	 
*Ivan Panez as Scientist 1
*Stanislav Dimitrov as Scientist 2	  	 
*Robert Zachar as Father	  	
*Bojka Velkova  as Mother	  	
*Kiril Hristov as Spencer

==Production== digital effects supervisor was Florentino Calzada. 

==Reception==
The DVD & Video Guide 2005 describes the movie as beginning "on a boring note and goes downhill from there".  Doug Pratt states that Zabkas performance appears as if "he had sat through too many Emillio Esteves films"   Doublas Pratt  and calls the cinematography of the DVD transfer "grainy".  The Videohounds Golden Movie Retriever 2005 gave the film its lowest rating on a 5 point scale. 

==See also==
*List of killer snake films

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 