Bolo Na Tumi Amar
 
{{Infobox film
| name = Bolo Na Tumi Amar
| image = bnta.jpg
| caption = Shakib Khan & Anika Kabir Shokh|Shokh, two leading stars of the film.
| director = M B Manik
| producer = 
| writer = Shokh Nirob Thoma mirza
| music = Ali Akram Shovo
| cinematography = Azzdozamman Mizno
| editing = Zinnath Hossian
| distributor =
| released =  
| runtime = 150 minutes
| country = Bangladesh Bengali
| budget = ৳20 millions.
| gross = ৳30 millions.
}}
Bolo Na Tumi Amar ( ;  ) (earlier titled Tomake Chara Bachbo Naa), is a Bengali language comedy-action film set in Bangladesh. It was directed by M B Manik and stars Shakib Khan, Anika Kabir Shokh, Nirob And Thoma Mirza.  The film was a box-office success in Bangladesh.

==Cast==
* Shakib Khan
* Anika Kabir Shokh
* Nirob
* Thoma Mirza
* Probir mitro
* Misa

==Music==
{{Infobox album
| Name = Bolo Na Tumi Amar
| Type = soundtrack
| Artist = Ali Akram Shovo
| Cover =
| Background =
| Released =  
| Genre = Feature film soundtrack
| Length =
| Label =
| Producer = 
|}}
The music to Bolo Na Tumi Amar was directed by Ali Akram Shovo.

{{Track listing
| extra_column = Singer(s)
| title1 = Ei Mone Baro Mash | extra1 = S I Tutol and Mila 
| title2 = Tomar Chokhe Dekhe | extra2 = Monir Khan and Shabina Ismin 
| title3 = Buke Ache Koto Batha Joma | extra3 = S I Tutol 
| title4 = Angoly Deho Theke | extra4 = Polash 
| title5 = Naughty Boy | extra5 = Asif and Doli 
}}

==References==
 

 
 
 
 
 
 
 
 
 


 
 
 