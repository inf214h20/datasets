Voodoo Man
{{Infobox film
| name           = Voodoo Man
| image          = Voodoo Man.jpg
| image_size     =
| caption        =
| director       = William Beaudine Barney A. Sarecky (associate producer)
| writer         = Robert Charles (story and screenplay)
| narrator       =
| starring       = Bela Lugosi John Carradine George Zucco
| music          =
| cinematography = Marcel Le Picard
| editing        = Carl Pierson
| distributor    = Monogram Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 American horror film directed by William Beaudine and starring Bela Lugosi, John Carradine and George Zucco.


== Plot ==
Nicholas (George Zucco) runs a filling station in the sticks. In reality, he is helping Dr. Richard Marlowe (Bela Lugosi) capture comely young ladies, so he transfer their life essences to his long-dead wife. Also assisting is Toby (John Carradine), who lovingly shepherds the leftover zombie girls and pounds on bongos during voodoo ceremonies. The hero is a Hollywood screenwriter who, at the end of the picture, turns the experience into a script titled "Voodoo Man." When his producer asks who should star in it, the hero suggests ... Bela Lugosi.

== Cast ==
*Bela Lugosi as Dr.Richard Marlowe
*John Carradine as Toby
*George Zucco as Nicholas
*Wanda McKay as Betty Benton
*Louise Currie as Stella Saunders
*Tod Andrews as Ralph Dawson
*Ellen Hall as Mrs. Evelyn Marlowe Terry Walker as Alice
*Mary Currier as Mrs. Benton
*Claire James as Zombie Henry Hall as Sheriff Dan White as Deputy Elmer
*Pat McKee as Grego
*Mici Goty as Marie, the housekeeper

== Production==
The film was shot in seven days beginning on October 16 1943 and was the last of Lugosis Monogram features with Return of the Ape man (1944). The film title was originally "Tiger Man" by author Andrew Colvin but was later changed as Voodoo Man and Colvin got no screen credit.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 