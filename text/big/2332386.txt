My Voyage to Italy
{{Infobox film
| name           = My Voyage to Italy
| image          =My Voyage to Italy DVD.jpg
| caption        =DVD cover
| writer         = Kent Jones Raffaele Donato Suso Cecchi dAmico Martin Scorsese
| starring       = Martin Scorsese (host)
| director       = Martin Scorsese
| producer       = Giorgio Armani
| distributor    = Miramax Films
| released       = September 11, 1999
| runtime        = 246 minutes
| country        = United States, Italy
| language       = English/Italian/French/German
| budget         =
}}
 documentary by acclaimed Italian-American director Martin Scorsese. The film is a voyage through Italian cinema history, marking influential films for Scorsese and particularly covering the Italian neorealism period.

The films of Roberto Rossellini make up for half the films discussed in the entire documentary, dealing with his seminal influence on Italian cinema and cinema history. Other directors mentioned include Vittorio de Sica, Luchino Visconti, Federico Fellini, Michelangelo Antonioni.

It was released in 1999 at a length of four hours. Two years later, it was screened out of competition at the 2001 Cannes Film Festival.   

==Films discussed==
*Rome, Open City (Roma città aperta) (1945), directed by Roberto Rossellini
*Paisà (1946), directed by Roberto Rossellini
*1860 (film)|1860 (1934), directed by Alessandro Blasetti
*Fabiola (1947), directed by Alessandro Blasetti
*The Iron Crown (La corona di ferro) (1941), directed by Alessandro Blasetti
*Cabiria (1914), directed by Giovanni Pastrone
*La terra trema (1948), directed by Luchino Visconti
*Bicycle Thieves (1948), directed by Vittorio De Sica
*Fantasia sottomarina (1940), directed by Roberto Rossellini
*Viaggio in Italia (Journey to Italy) (1954), directed by Roberto Rossellini  La Prise de pouvoir par Louis XIV (1966), directed by Roberto Rossellini 
*Germany Year Zero (1947), directed by Roberto Rossellini
*The Miracle (Il miracolo) segment (1948) of LAmore (film)|LAmore, directed by Roberto Rossellini
*Stromboli (film)|Stromboli (1950), directed by Roberto Rossellini
*The Flowers of St. Francis (Francesco, giullare di Dio) (1950), directed by Roberto Rossellini
*Europa 51 (1952), directed by Roberto Rossellini
*Gli uomini, che mascalzoni! (1932), directed by Mario Camerini with Vittorio De Sica as Bruno
*Il signor Max (1937), directed by Mario Camerini with Vittorio De Sica as Gianni/Max Varaldo
*Shoeshine (film)|Shoeshine (Sciuscià) (1946), directed by Vittorio De Sica
*Umberto D (1952), directed by Vittorio De Sica, directed by Vittorio De Sica The Roof (Il tetto) (1956), directed by Vittorio De Sica
*Two Women (La ciociara) (1961), directed by Vittorio De Sica The Garden of the Finzi-Continis (Il giardino dei Finzi-Contini) (1970), directed by Vittorio De Sica
*The Gold of Naples (Loro di Napoli) (1954), directed by Vittorio De Sica
*Senso (film)|Senso (1954), directed by Luchino Visconti Les Bas-fonds (The Lower Depths) (1936), directed by Jean Renoir
*Ossessione (1943), directed by Luchino Visconti
*Giorni di Gloria (Days of Glory) (1945), directed by Giuseppe De Santis, Mario Serandrei, Marcello Pagliero and Luchino Visconti
*Bellissima (film)|Bellissima (1951), directed by Luchino Visconti, with Alessandro Blasetti, a film director, appears as himself.
*I vitelloni (1953), directed by Federico Fellini
*La Strada (1955), directed by Federico Fellini
*Nights of Cabiria ( Le notti di Cabiria) (1957), directed by Federico Fellini
*La Dolce Vita (1960), directed by Federico Fellini
*8½ (1963), directed by Federico Fellini
*Divorzio allitaliana (Divorce, Italian Style) (1961), directed by Pietro Germi
*Lavventura (1960), directed by Michelangelo Antonioni
*La Notte (The Night) (1961), directed by Michelangelo Antonioni
*Leclisse (1962), directed by Michelangelo Antonioni

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 