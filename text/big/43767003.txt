Finding Neighbors
{{Infobox film
| name           = Finding Neighbors
| image          = 
| alt            = 
| caption        = 
| director       = Ron Judkins
| producer       = Judy Korin, Jennifer Day Young
| writer         = Ron Judkins
| starring       = Michael OKeefe 
Catherine Dent Blake Bashoff Julie Mond Sean Patrick Thomas
| music          = Ronit Kirchman
| cinematography = Tari Segal
| editing        = Travis Hatfield
| animator         = Una Lorenzen
| illustrator    = Barry Brunner
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 independent film directed by Ron Judkins. This adult comedic drama is the story of a formerly acclaimed graphic novelist who goes looking for a true connection outside of his marriage – and just over his fence. Six months late on a book delivery, Sam (Michael OKeefe) has succumbed to a full-on midlife and creative crisis. Sherrie (Julie Mond), the provocative girl-next-door, offers him an easy and willing distraction. But it is Sam’s budding friendship with another neighbor (Blake Bashoff), a gay man, that ultimately helps him reconnect with his creativity. Sam, however, reveals nothing of this to his wife (Catherine Dent), and as he begins to change, she becomes increasingly suspicious about what’s going on in the neighborhood. 

==Cast==

* Michael OKeefe as Sam
* Catherine Dent as Mary
* Blake Bashoff as Jeff
* Julie Mond as Sherrie
* Sean Patrick Thomas as Paul
* Mike Genovese as Mike

==Release==
Finding Neighbors world premiered at the 2013 Austin Film Festival, and subsequently screened within such festivals as St. Louis International Film Festival, Florida Film Festival, Newport Beach International Film Festival, Omaha Film Festival, and Boston LGBT Film Festival. 

==Production Notes== Best Sound (Jurassic Park, Saving Private Ryan) and has been nominated for another three in the same category.   ""Finding Neighbors"" is his first film since 1999s The Hi-Line which premiered at Sundance Film Festival, and won the Austin Film Festival Audience Award.   The film was shot primarily in Atwater Village in Los Angeles, CA. 

==References==
 

==External links==
*  
*  

 
 
 


 