Twice-Told Tales (film)
{{Infobox film
| name           = Twice-Told Tales
| director       = Sidney Salkow
| image          = Twicetoldtales.jpg
| writer         = Robert E. Kent
| based on       = novel by Nathaniel Hawthorne
| starring       = Vincent Price
| producer       = Robert E. Kent Edward Small (executive)
| music          = Richard LaSalle
| cinematography = Ellis W. Carter
| editing        = Grant Whytock
| studio         = Admiral Pictures / Robert E. Kent Productions
| distributor    = United Artists
| released       =  
| runtime        = 120 minutes
| language       = English
}}
Twice-Told Tales (1963 in film|1963) is an American horror film directed by Sidney Salkow and starring Vincent Price. 

==Production background==
The film is based on two of Nathaniel Hawthornes stories, "Dr. Heideggers Experiment" (1837) and "Rappaccinis Daughter" (1844), and the novel The House of the Seven Gables (1851), which had previously been adapted in 1940 also starring Price.   Only "Dr. Heideggers Experiment" was actually published in Hawthornes Twice-Told Tales, which supplied the films title.

==Plot==
Each of the three sequences is introduced by Vincent Price (in a voice-over). Price also stars in all three narratives. 

==="Dr. Heideggers Experiment"=== Sebastian Cabot) and Alex (Price), meet to celebrate Heideggers 79th birthday. They discover that Heideggers fiancée from 38 years before, Sylvia, is perfectly preserved in her coffin. Heidegger believes that the water dripping into the coffin has the power to preserve. He tries it on a withered rose and it comes back into full bloom.  

Carl and Alex drink it and become young again. Carl injects the liquid into Sylvia and she comes back to life.  Sylvia reveals that she and Alex were secretly lovers. Carl attacks Alex, but Alex kills him in the struggle. The effects of the water wear off. Sylvia is reduced to a desiccated skeleton, Carls body returns to its original age. Alex returns to the crypt to find more of the water, but it no longer flows.

==="Rappaccinis Daughter"===
In Padua, Giacomo Rappaccini (Price) keeps his daughter Beatrice in a garden. A university student next door, Giovanni, sees her and falls in love. One of Giovannis professors says that he used to teach with Rappaccini.  Many years ago, Rappaccini abruptly quit academia and became a recluse after his wife ran away with a lover. Rappaccini has treated Beatrice with an exotic plant extract that makes her touch deadly; he does this to keep her safe from unwanted suitors, but it makes her a prisoner in her own home. 

When Giacomo sees the attraction between Giovanni and Beatrice, he surreptitiously treats Giovanni with the extract so they can be together. Giovanni is aghast, and obtains an experimental antidote from his professor. He consumes the antidote in front of Beatrice, but it kills him. Beatrice drinks it also, killing herself. Giacomo grabs the exotic plant with both hands and its touch kills him.

==="House of the Seven Gables"===
Gerald Pyncheon (Price) returns to his family house after an absence of 17 years, bringing with him his wife Alice. His sister Hannah, who had been living in the house, tells Alice about the curse put upon Pyncheon men by Matthew Moll, who used to own the house but lost it in a shady deal to the Pyncheon family. Jonathan Moll, a descendant of Matthew, arrives, but he refuses Geralds offer to give him the house in exchange for the location of a vault where valuable property deeds are stored. Alice becomes haunted by the curse on the house, which eventually leads her to the cellar. 

Gerald finds her there and discovers the map to the vault. He kills Hannah to keep her share of the inheritance. Gerald traps Alice in the basement grave of Mathew Moll, then goes to the study to find the vault. He opens it, and a skeletal hand inside the vault kills him. Jonathan arrives and takes Alice out of the house, just as it shakes and collapses. 

==Cast==
*Vincent Price as  Alex Medbourne / Giacomo Rappaccini / Gerald Pyncheon  Sebastian Cabot as  Dr. Carl Heidigger 
*Brett Halsey as Giovanni Guasconti 
*Beverly Garland as  Alice Pyncheon 
*Richard Denning as Jonathan Maulle 
*Mari Blanchard as Sylvia Ward 
*Abraham Sofaer as  Prof. Pietro Baglioni 
*Jacqueline deWit as  Hannah Pyncheon, Geralds Sister 
*Joyce Taylor as Beatrice Rappaccini 
*Edith Evanson as  Lisabetta, the landlady 
*Floyd Simmons as  Ghost of Mathew Maulle 
*Gene Roth as  Cabman

==Production==
Filming started on Halloween 1962. Champion to Direct Daisy, Carnival: Husband Will Star Ginger; No Man an Island Review
Scheuer, Philip K. Los Angeles Times (1923-Current File)   01 Nov 1962: C9.  

==See also==
* List of American films of 1963

==References==
 

==External links==
*  
*  at TCMDB
 

 
 
 
 
 
 
 
 
 
 
 
 