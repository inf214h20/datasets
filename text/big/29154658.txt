Judgement of God
{{Infobox film
| name           = Le Jugement de Dieu
| image          = 
| caption        = 
| director       = Raymond Bernard
| producer       = B.U.P.
| writer         = Bernard Zimmer Pierre Montazel
| starring       = Andrée Debar Louis de Funès
| music          = Joseph Kosma
| cinematography = 
| editing        = 
| distributor    = 
| released       = 20 August 1952 (France)
| runtime        = 98 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Drama drama film from 1952, directed by Raymond Bernard, written by Jean Montazel, and starring by Andrée Debar and Louis de Funès. The scenario was written on the basis of German legend from the 15th century. German title - "Agnes Bernauer". 

== Cast ==
*  
*  
*  
* Gabrielle Dorziat : Josepha, Prince Alberts aunt
* Jean Barrère : Count Törring
* Olivier Hussenot : Mr Bernauer, the father of Agnès and Marie (the barber)
* Louis Seigner : the burgomaster (mayor)
* André Wasley : the captain
* Jacques Dynam : a soldier
* Max Dalban : a butcher
* Jean Clarieux : the leader of the outlaws
* Marcel Raine : the minister
* Louis de Funès : the burgomasters emissary
* Georges Douking : Enrique (the monk)

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 


 