Les Corsaires du Bois de Boulogne
{{Infobox film
| name           = Les Corsaires du Bois de Boulogne
| image          = 
| caption        = 
| director       = Norbert Carbonnaux
| producer       = Pécéfilms
| writer         = Norbert Carbonnaux Robert Dhéry
| starring       = Raymond Bussières Louis de Funès
| music          = Norbert Glanzberg
| cinematography = 
| editing        = 
| distributor    = Sofradis
| released       = 31 May 1954 (France)
| runtime        = 77 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1954, directed and written by Norbert Carbonnaux, starring Raymond Bussières and Louis de Funès. 

== Cast ==
* Raymond Bussières : Hector Colomb, street singer
* Annette Poivre : Adèle, singer of streets
* Christian Duvaleix : Cyprien, street singer
* Denise Grey : Mrs Grossac, wife of industrialist
* Véra Norman : Caroline Grossac, the girl
* Jean Ozenne : Marcel Grossac, the industrialist
* Sophie Sel : a servant of "Grossac"
* Jess Hahn : the American marine Mario David : the athlete
* Jacques Ary : gendarme
* Christian Brocard : the news vendor  (uncredited)
* Laure Paillette :  the patroness of café (uncredited)
* Georges Lautner : amateur radio (uncredited)
* Louis de Funès : the commissar (uncredited)
* Olga Sminsky (uncredited)
* Monique Dutot (uncredited)
* Antonio Longard (uncredited)

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 