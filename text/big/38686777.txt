Dear Mr. Watterson
{{Infobox film
| name           = Dear Mr. Watterson
| image          = Dear Mr. Watterson.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = The poster for Dear Mr. Watterson
| director       = Joel Allen Schroeder
| producer       =Chris Browne Matt McUsic
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Seth Green Berkeley Breathed Stephan Pastis
| music          = Mike Boggs
| cinematography = Andrew Waruszewski
| editing        = Joel Allen Schroeder
| studio         = 
| distributor    = 
| released       = November 15, 2013
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Dear Mr. Watterson is a 2013 American documentary film directed by Joel Allen Schroeder, produced by Christopher Browne and Matt McUsic, with Andrew P. Waruszewski as the cinematographer.  The film follows the career of Bill Watterson, the author of the comic strip Calvin and Hobbes, and the influence of both the author and the comic strip on the world.

Watterson ended the strip on December 31, 1995, and since then remained away from the public scene. Joel Schroeder says that the film is not a quest to find Watterson or invade his privacy:
{{Quotation| 
Dear Mr. Watterson is a film that will look to the readers and fans of Calvin & Hobbes to tell the story of the strip and its creator. As we explore the art and impact of Bill Watterson through this unique perspective, the undying appreciation and love of Calvin & Hobbes and the man behind it will be evident in the anecdotes, stories, and memories shared by readers of the strip and friends and colleagues of Mr. Watterson. —Joel Allen Schroeder}}

==History==
The origin of Dear Mr. Watterson came from Schroeder wanting to understand the cultural impact of Bill Watterson’s decade-long comic strip, so he began as a series of fan interviews in December 2007. 
 Keith Knight, Jenny Robb, Tony Cochran, Andrew Farago (Cartoon Art Museum), Joe Wos (Toonseum), Jean Schulz, Jan Eliot, Bill Amend, and more. And in addition also launched a second Kickstarter campaign  in order to fund the finishing of the project. The campaign was successfully funded July 14, 2012.

On November 21, 2012, Schroeder reported to the Kickstarter backers that they had completed the Martini Shot, which is the last take you shoot while in production on a film. In late December 2012, the crew shipped nearly 200 posters  as a show of appreciation to backers of their kickstarter all over the world. Since then they have been updating their Kickstarter with notifications on their submissions of the film to film festivals all over the US, and on March 1, 2013 they announced that Dear Mr. Watterson had been accepted into the 37th Annual Cleveland International Film Festival. 

==Release==
Dear Mr. Watterson had its premiere at the   and a video on demand release on November 15, 2013.    

At the October 6th 2013 Buffalo International Film Festival screening of the film, childrens author, Buffalo NY native and Calvin and Hobbes fan, Keith White Jr. was in attendance as a special guest.  He signed books before the theater doors opened and stayed to watch the film and greet the crowd before the screening began.

==Influences and impact==
 
The film began with Schroeder interviewing fans of the strip to better understand the cultural impact it had. The strip adhered to topics like morality and animal activism, combined with an imagination and vantage point of a young child, hyper-aware of stereotypes and subjective cultural perspectives. Watterson reported in an interview that he has no regrets of ending the strip when he did, and that he could have gone on repeating himself for another 10–20 years and the fans that were grieving the strip would be wishing it dead for its tediousness.

In an interview with NPR′s Weekend Edition, Schroeder explained that Watterson′s final cartoon exemplified the strip′s enduring appeal.    Said Schroeder, describing the panel: “It′s a fresh layer of snow and Calvin and Hobbes are out with the toboggan, and Calvin looks to Hobbes and says, ‘It’s a magical world, old buddy ... let′s go exploring.’ And those last words are just, I think, a challenge to all of us to make sure that we have that curiosity. And words, I think words to live by.” 

==References==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 