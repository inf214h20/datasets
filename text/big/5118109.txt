Vivacious Lady
{{Infobox film
| name           = Vivacious Lady
| image          = Vivacious_Lady_FR.jpeg
| caption        = Original film poster
| director       = George Stevens
| producer       = George Stevens
| writer         = I. A. R. Wylie (story) P. J. Wolfson Ernest Pagano
| starring       = Ginger Rogers James Stewart Frances Mercer Beulah Bondi Franklin Pangborn Charles Coburn Hattie McDaniel
| music          = Roy Webb
| cinematography = Robert De Grasse
| editing        = Henry Berman
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget = $703,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross = $1,206,000 
}}

Vivacious Lady is a 1938 American black-and-white romantic comedy film starring Ginger Rogers and James Stewart, produced and directed by George Stevens, and released by RKO Radio Pictures. The screenplay was written by P.J. Wolfson and Ernest Pagano and adapted from a short story by I. A. R. Wylie. The music score was by Roy Webb and the cinematography by Robert De Grasse.

The film features supporting performances by Frances Mercer, Beulah Bondi, Franklin Pangborn, and Charles Coburn, as well as an uncredited appearance by Hattie McDaniel.

==Plot==
 
Vivacious Lady is a story of love at first sight between a young botany professor named Peter Morgan Jr. (James Stewart|Stewart) and a nightclub singer named Francey (Ginger Rogers|Rogers). The film also has comedic elements, including repeatedly frustrated attempts by the newlywed couple to find a moment alone with each other.

The story begins when Peter is sent to Manhattan to retrieve his playboy cousin Keith (James Ellison (actor)|Ellison) and immediately falls in love with Francey. After a whirlwind one-day courtship, Peter and Francey get married, and they and Keith return to the Morgan familys home, where Peter teaches at the university run by his father Peter Morgan Sr. (Charles Coburn|Coburn).  Mr. Morgan is known for being a proud, overbearing man, so Peter is afraid to tell him about the marriage. When they arrive, Mr. Morgan and Peters high-society fiancée Helen (Mercer) initially take Francey for another of Keiths girlfriends. While Peter decides how to approach his father with the news, Francey stays at a women-only hotel, and Peter and Keith introduce her as a new botany student.  

Peter mentions Francey to his father twice, but on both occasions, Mr. Morgan interrupts and ignores his son, and when Peter becomes insistent, his apparently ailing mother (Beulah Bondi|Bondi) has a flare-up of her heart condition, making any further conversation impossible. For his third attempt, Peter decides to announce the marriage to his parents at the universitys student-faculty prom.  Keith brings Francey to the prom as his own guest, and Francey, still posing as a student, develops a friendly rapport with Mrs. Morgan, but gets into a nasty brawl with Helen in which Francey accidentally punches Peters father.

Peter says nothing at the prom, but blurts the news to his father just as Mr. Morgan is about to give an important speech, resulting in another argument and another flare-up of Mrs. Morgans heart condition. This prevents Mrs. Morgan from learning who Francey is, but she accidentally finds out from Francey herself during a conversation in Franceys apartment. Mrs. Morgan accepts the news happily, and admits to Francey that she pretends to have heart trouble any time her husband gets into an argument, but Mr. Morgan demands that Francey leave Peter, threatening to fire him if she doesnt.  Francey agrees to leave, but the incident releases thirty years of marital frustration in Mrs. Morgan, who also decides to leave her husband.

Francey tells Peter she will leave him unless he can change his fathers mind before her train departs. Peters solution is to threaten the family with disgrace by getting drunk and otherwise misbehaving until his father relents, even if it costs him his job. Peter passes out before he can reach the train, which departs with both Francey and Mrs. Morgan aboard, but Mr. Morgan, having finally yielded to the combined pressure of his son and wife, stops the train by driving ahead of it with Peter and parking the car on the track. Both marriages are saved, and Peter and Francey finally have their honeymoon on the train.

==Cast==
* Ginger Rogers as Francey
* James Stewart as Prof. Peter Morgan Jr. James Ellison as Keith Morgan
* Beulah Bondi as Mrs. Martha Morgan
* Charles Coburn as Peter Morgan, Sr.
* Frances Mercer as Helen
* Phyllis Kennedy as Jenny
* Franklin Pangborn as Apartment Manager
* Grady Sutton as Culpepper, Teaching Assistant
* Jack Carson as Charlie, Waiter Captain
* Alec Craig as Joseph, Chauffeur
* Willie Best as Train Porter
* Hattie McDaniel as a Maid

==Production==
Vivacious Lady marked one of James Stewarts earliest starring roles. Ginger Rogers recommended Stewart as her leading man in this film. Although neither actor collaborated on any prior work, the two were dating at the time. 

After four days of shooting in April 1937, Stewart became ill, but then left to star in Of Human Hearts. RKO considered replacing Stewart, but shelved the production until December 1937. Actors Donald Crisp and Fay Bainter, who were cast in the original production, were replaced by Charles Coburn and Beulah Bondi.

==Reception==
The film made a profit of $75,000. 

In the early 1960s Steve McQueen announced that he wanted to appear in a remake but this did not eventuate. 

==Awards and nominations==
Vivacious Lady was nominated for two Academy Awards, for Best Cinematography and Best Sound, Recording (John Aalberg).    George Stevens won a Special Recommendation Award at the 1938 Venice Film Festival.

==Adaptations to Other Media== Robert Walker and Lurene Tuttle,    the December 3, 1945 Screen Guild Theater with James Stewart and Janet Blair and on the August 14, 1946 episode of Academy Award Theater with Lana Turner.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 