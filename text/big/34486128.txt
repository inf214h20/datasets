Parisian Love
{{infobox film
| name           = Parisian Love
| image          =
| imagesize      =
| caption        =
| director       = Louis J. Gasnier
| producer       = B. P. Schulberg
| writer         = F. Oakley Crawford (story) Lois Hutchinson (adaptation)
| starring       = Clara Bow
| music          =
| cinematography = Allen G. Siegler
| editing        =
| distributor    = Preferred Pictures Corporation Al Lichtman Independent Sales Corporation (States Rights)
| released       = August 1, 1925
| runtime        = 70 minutes; 7 reels (6,324 feet)
| country        = United States
| language       = Silent English intertitles

}}
Parisian Love is a black and white 1925 silent film starring Clara Bow. The film was produced by B.P. Schulberg Productions. A copy of this film still survives.  

==Cast==
*Clara Bow - Marie Donald Keith - Armand
*Lillian Leighton - Frouchard
*J. Gordon Russell - DAvril
*Hazel Keener - Margot
*Lou Tellegen - Pierre Marcel
*Jean De Briac - Knifer
*Otto Matieson - Apache Leader
*Alyce Mills - Jean DArcy

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 