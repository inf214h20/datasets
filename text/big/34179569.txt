Can This Be Dixie?
 
{{Infobox film
| name            = Can This Be Dixie?
| image           =
| caption         = George Marshall
| producer        = Sol M. Wurtzel George Marshall Lamar Trotti
| narrator        =
| starring        = Jane Withers
| music           =
| cinematography  =
| editing         =
| distributor     =
| released        =  
| runtime         = 70 minutes
| country         = United States
| language        = English
}}

Can This Be Dixie? is a 1936 American film featuring child star Jane Withers.

Withers plays Peg Gurgle, who, with her uncle Robert E. Lee Gurgle, runs a traveling musical patent medicine show through the deep south.  When they encounter a plantation owner named Colonel Robert E. Lee Peachtree, their luck picks up when the Colonel buys a bottle of their elixir for each one of his plantation field hands.  When the sheriff impounds their wagon, the Gurgles stay on with the Colonel and helps defend his mansion against Yankees and bankers.

In 1937 and 1938 Withers became one of the top 10 box-office stars in the United States, despite her status as Foxs second-tier child star (behind Shirley Temple).  On a shooting schedule that allowed 21 to 24 days per picture, she acquired the nickname "One-Take Withers",  and produced four or five films a year.

The level of comedy can be assessed by the names of the characters, the names of the musical numbers ("Pick, Pick, Pickaninny," "Uncle Toms Cabin is a Cabaret Now"), and the fact that Withers appeared in blackface.  Some even more racially offensive material was challenged by co-star Hattie McDaniel   and successfully removed from the picture.

== Cast ==
 
* Jane Withers as Peg Gurgle
* Slim Summerville as Robert E. Lee Gurgle Helen Wood as Virginia Peachtree Thomas Beck as Ulysses S. Sherman
* Sara Haden as Miss Beauregard Peachtree
* Claude Gillingwater as Col. Robert Peachtree Donald Cook as Longstreet Butler James Burke as Sheriff N.B.F. Rider
* Jed Prouty as Ed Grant
* Hattie McDaniel as Lizzie
* Troy Brown Sr. as Jeff Davis Brunch 
* Robert Warwick Gen. Beauregard Peachtree
* Billy Bletcher as John P. Smith Peachtree William Worthington as George Washington Peachtree
* Otis Harlan as Thoma Jefferson Peachtree
 

== References ==

 

== External links ==

*  
*  

 
 
 
 