The Seventh Floor (1967 film)
 
{{Infobox film
| name           = The Seventh Floor
| image          = The Seventh Floor.jpg
| caption        = Film poster
| director       = Ugo Tognazzi
| producer       = Henryk Chrosicki Alfonso Sansone Ugo Tognazzi
| writer         = Ugo Tognazzi Rafael Azcona
| starring       = Ugo Tognazzi
| music          = Teo Usuelli
| cinematography = Enzo Serafin
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Seventh Floor ( ) is a 1967 Italian comedy film directed by and starring Ugo Tognazzi. It was entered into the 17th Berlin International Film Festival.   

==Cast==
* Ugo Tognazzi as Giuseppe Inzerna
* Tina Louise as Dr. Immer Mehr
* Olga Villi as Anita, Inzerna wife
* Franca Bettoia as Giovanna, Inzernas lover
* Alicia Brandet as Gloria, Inzerna daughter
* Gildo Tognazzi as Gerolamo, Inzernas father
* Alessandro Quasimodo
* Gigi Ballista as Dr. Claretta Riccardo Garrone as Barbiere
* Marco Ferreri as Dr. Salamoia

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 