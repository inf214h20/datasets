Keli Kottu
{{Infobox film
| name           = Keli Kottu
| image          =
| caption        =
| director       = TS Mohan
| producer       = TS Mohan
| writer         =
| screenplay     = Devan Shari Shari
| music          = Rajamani
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Devan and Shari in lead roles. The film had musical score by Rajamani.   

==Cast== Devan
*Shari Shari

==Soundtrack==
The music was composed by Rajamani and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kuyil Paadum || KS Chithra, MG Sreekumar || Mankombu Gopalakrishnan || 
|-
| 2 || Veendum || KS Chithra || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 