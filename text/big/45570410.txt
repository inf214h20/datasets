Redemption (1917 film)
{{Infobox film
| name           = Redemption
| image          = Redemption.jpg
| caption        =
| director       = Julius Steiger Joseph A. Golden
| producer       = Triumph Film Corporation Julius Steiger
| writer         = John Stanton(scenario) Julius Steiger(titles)
| starring       = Evelyn Nesbit Russell Thaw
| music          =
| cinematography = John Urie
| editing        =
| distributor    = Triumph Film Corporation
| released       = June 2, 1917
| runtime        = 60 minutes; 6 reels
| country        = USA
| language       = Silent..English titles
}} lost  1917 silent film drama starring Evelyn Nesbit. Yet another film which is not too far removed from Nesbits own scandalized public life. Nesbits young son Russell costars with her. 

==Cast==
*Evelyn Nesbit - Alice Loring (*as Evelyn Nesbit Thaw)
*Russell Thaw - Harry Loring
*Charles Wellesley - Stephen Brooks (*as Charles Wellsley)
*Mary Hall - Brookss wife
*William Clark - Robert, Their Son (*as William Clarke)
*Joyce Fair - Grace, Their Daughter
*Edward Lynch - Thomas Loring
*George Clarke - Harry (15 years later)
*Marie Reichardt - Mrs. Collins

==References==
 

==External links==
* 
* 

 
 
 
 
 


 