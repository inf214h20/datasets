Happy Days (1929 film)
 
{{Infobox film
| name = Happy Days
| image = Happydays1929.jpg
| image_size =
| caption =
| director = Benjamin Stoloff William Fox
| writer = Sidney Lanfield Edwin J. Burke
| starring = Charles E. Evans Marjorie White Richard Keene Stuart Erwin
| music = Harry Stoddard
| cinematography = Lucien N. Andriot John Schmitz J.O. Taylor
| editing = Clyde Carruth
| distributor = Fox Film Corporation
| released =  
| runtime = 80 minutes English
| country = United States
| budget =
}} Old Ironsides with two sequences in a widescreen process called "Magnascope". In 1928, MGM released Trail of 98 in a widescreen process called "Fanthom Screen". 
 George Jessel, Ann Pennington, Victor McLaglen, Dixie Lee, Edmund Lowe, and Frank Richardson. It also featured the first appearance of Betty Grable on film, aged 12, as a chorus girl, and Sir Harry Lauders nephew, Harry Lauder II, a conductor for Fox, who was drafted into the chorus.

==Plot==
Originally titled New Orleans Frolic, the story centers around Margie (played by Marjorie White), a singer on a showboat who goes to make her fortune in New York City, despite being in love with the boat owners grandson. Although successful in the city, when she hears that the showboat is in financial trouble she calls all the boats former stars to perform in a show to rescue it.

==Premiere== Roxy Theater Grandeur screen Carthay Circle Theatre in Los Angeles, from February 28, 1930.

At a screening at the Roxy Theater, film critic Mordaunt Hall praised the cinematography, which was noted to be enhanced by the wider format. However, he regarded the film itself as "... not one that gives as full a conception of the possibilities as future films of this type will probably do." 
 Fox Film William Fox losing his business, which was eventually merged in 1935 with Twentieth Century Pictures to form 20th Century Fox. No widescreen print of Happy Days is known to survive.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 