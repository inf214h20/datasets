Svengali (1954 film)
{{Infobox film
 | name = Svengali
 | image = "Svengali"_(1954).jpg
 | image_size = 
 | caption = 
 | director = Noel Langley
 | producer =  George Minter
 | screenplay = Noel Langley
| based on       = the novel Trilby (novel)|Trilby by George Du Maurier
 | starring =  Hildegard Knef  Donald Wolfit  Terence Morgan
 | music = William Alwyn
 | cinematography = Wilkie Cooper
 | editing =  John Pomeroy
| studio = George Minter Productions
 | distributor = Renown Pictures Corporation  (UK)
 | released = December 1954	(UK)
 | runtime = 82 min
 | country = United Kingdom
 | language = English
 | budget = 
}} British drama film directed by Noel Langley and starring Hildegard Knef, Donald Wolfit and Terence Morgan.  A svengali hypnotises an artists model into becoming a great opera singer, but she struggles to escape from his powers. It was based on the novel Trilby (novel)|Trilby by George Du Maurier. Donald Wolfit was a last minute replacement for actor Robert Newton, who left three weeks into filming, and can still be seen in some long shots.        

Amongst the end credits is the acknowledgment: "The producer expresses his grateful appreciation for the magnificent singing voice of Madame Elizabeth Schwarzkopf."  

==Cast==
* Hildegard Knef ...  Trilby OFarrall
* Donald Wolfit ...  Svengali
* Terence Morgan ...  Billy Bagot
* Derek Bond ...  The Laird Paul Rogers ...  Taffy
* David Kossoff ...  Gecko
* Hubert Gregg ...  Durian Noel Purcell ...  Patrick OFerrall
* Alfie Bass ...  Carrell
* Harry Secombe ...  Barizel
* Peter Illing ...  Police Inspector
* Joan Haythorne ...  Mrs. Bagot
* Hugh Cross ...  Dubose
* David Oxley ...  Dodor Richard Pearson ...  Lambert Michael Craig ...  Zouzou
* Arnold Bell ... Tout 
* Martin Boddey ... Doctor  Cyril Smith ... 1st Stage Manager 
* Marne Maitland ... 2nd Stage Manager 
* Elisabeth Schwarzkopf ...  Trilby OFarrall (singing voice)

==Critical reception==
Under the heading, "Sixth Filming of Novel Fails to Hypnotize," The New York Times described the film as "a stylized curio that seems out of place in the atomic age...as old-fashioned as side whiskers and Bustle|bustles" ; {{cite web|url=http://www.nytimes.com/movie/review?res=9900E2D91338E23BBC4E51DFBF66838E649EDE|title=Movie Review - 1931 John Moulin Rouge (1952), photographed in Technicolor by Oswald Morris. Svengali was made on a fraction of that films budget, though does look handsome for what it is."  

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 