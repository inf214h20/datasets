Bachpan (1945 film)
{{Infobox film
| name           = Bachpan
| image          = Bachpan 1945.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Basant Pictures
| writer         = 
| screenplay     = Homi Wadia
| story          = 
| based on       = 
| narrator       = 
| starring       = Mazhar Khan Baby Madhuri Boman Shroff Nandrekar
| music          = S. N. Tripathi
| cinematography = 
| editing        = 
| studio         = Basant Pictures
| distributor    = 
| released       = 1945
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1945 Hindi drama film directed by Homi Wadia.    It was produced by Homi Wadia’s Basant Films and had music by S. N. Tripathi.  The film starred Mazhar Khan, Chandraprabha, Shashi Kapoor Sr., Gulab, Dalpat, Baby Shakuntala, Dixit, Baby Madhuri. 

==Cast==
* Mazhar Khan
* Baby Madhuri
* Chandraprabha
* Nandrekar
* Dixit
* Shakuntala
* Gulab
* Dalpat
* Shashi Kapoor

==Music==
Film’s music was composed by S. N. Tripathi, with lyrics written by I. C. Kapoor. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Bagiyo Me Phool Khile Hai 
|-
| 2
| Billi Ki Tarah Woh Dabe Paanv Chali Re
|-
| 3
| Bole Re Panchi Bole 
|-
| 4
| Choti Si Kahani 
|-
| 5
| Ye Than Hai Mere Samne 
|-
| 6
| Hindustan Hamara 
|-
| 7
| Gokul Ki Ek Naar Chhabili 
|-
| 8
| Main To Girdhar Aage Nachungi 
|-
| 9
| Rakhi Ka Din Aaya 
|-
| 10
| Matware Manwa Le Chal 
|}

==References==
 

==External links==
* 

 


 


 
 
 
 
 