Pegeen (film)
{{Infobox film
| name           = Pegeen
| image          = Pegeen (1920) - Love & Spere.jpg
| alt            =
| caption        = Still with Bessie Love and Charles Spere David Smith
| producer       =
| writer         =
| screenplay     = 
| story          = 
| based on       =  
| starring       = Bessie Love
| cinematography = Charles R. Seeling 
| editing        =
| studio         = Vitagraph 
| distributor    =
| released       =   reels 
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}} David Smith.  It stars Bessie Love in the title role. Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
Recently widowed Danny ONeil (Stanley) has the belief that his wife will return to him by way of fire, and sets fire to buildings around town in hope that she will return to him. For her safety, his daughter Pegeen (Love) is sent to live with neighbor. When her father is to be arrested, Pegeens friend Ezra (McGuire) helps hide her father, who dies shortly thereafter.    

==Cast==
*Bessie Love as Pegeen ONeill  
*Edward Burns as John Archibald
*Ruth Fuller Gordon as Nora Moran
*Charles Spere as Jimmie
*Juan DeLa Cruz as Meredith
*Major McGuire as Ezra
*George Stanley as Dan ONeill
*Anne Schaefer as Ellen

==Reception==
Reviews for the film were mixed.   Quotes from negative reviews:
* 
* 
*   Its "worst criticism" is that "it is not a thriller, nor a spectacle. Neither is it a heavy digest of a weighty social or economic problem. It is just a simple story of every day people, told in simple, direct continuity, intelligently and coherently."   

Scenes involving a hanging and a shoot-out were recommended for removal when showing the film to family audiences. 

==References==
 

==External links==
* 
* 
*  at AFI
*  at BFI
* 


 
 
 
 
 