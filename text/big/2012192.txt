Umberto D.
{{Infobox film
| name           = Umberto D.
| image          = UmbertoD.jpg
| caption        = 
| director       = Vittorio De Sica De Sica-Giuseppe Amato
| writer         = Cesare Zavattini (story and screenplay)
| starring       = Carlo Battisti Maria-Pia Casilio Lina Gennari Ileana Simova Elena Rea Memmo Carotenuto
| music          = Alessandro Cicognini
| cinematography = G.R. Aldo|G. R. Aldo
| editing        = Eraldo Da Roma Criterion (Region 1 DVD)
| released       = January 20, 1952 (Italy) November 7, 1955 (USA)
| runtime        = 89 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Italian Italian neorealist film directed by Vittorio De Sica. Most of the actors were non-professional, including Carlo Battisti, who plays the title role of Umberto Domenico Ferrari, a poor old man in Rome desperately trying to keep his room. His landlady (Lina Gennari) is evicting him, and his only true friends, the housemaid (Maria-Pia Casilio) and his dog Flike (called Flag in some subtitled versions of the film) are of no help.

According to Robert Osborne of Turner Classic Movies, this was De Sicas favorite of all his films. The movie was included in "Time Magazines All-Time 100 Movies" in 2005.  The films sets were designed by Virgilio Marchi.

==Plot==
Police disperse an organized street demonstration of elderly men demanding a raise in their meager pensions. One of the marchers is Umberto D. Ferrari, a retired government worker.

He returns to his room and finds his landlady has rented it out for an hour to an amorous couple. She threatens to evict Ferrari at the end of the month if he cannot pay the overdue rent: fifteen thousand lire. Umberto sells a watch and some books, but only raises a third of the amount. The landlady refuses to accept partial payment.

Meanwhile, the sympathetic maid confides in Umberto that she has her own problems. She is three months pregnant, but is unsure which of two soldiers is the father, the tall one from Naples or the short one from Florence.

Feeling ill, Umberto gets himself admitted to a hospital; it turns out to be tonsillitis, and he is discharged after a few days. When he returns to the apartment, he finds workmen renovating the entire place. The landlady is getting married. Umbertos room has a gaping hole in the wall; the maid tells him it is to become part of an enlarged living room.

The maid was taking care of the dog, but a door was left open and Flike ran away. Umberto rushes to the city pound and is relieved to find his dog.

With a working friend turning a deaf ear to his veiled plea for a loan, and unable to bring himself to beg from strangers on the street, Umberto contemplates suicide, but knows he must first see that Flike is taken care of. He packs and leaves the apartment. His final parting advice to the maid is to get rid of her boyfriend from Florence.

Umberto attempts to find a place for Flike, first with a couple who board dogs, then a little girl he knows, but the latters nanny makes her give the dog back. Flike goes to play with some children, and Umberto slips away, gambling that one of them will adopt him.  Despite Umbertos attempt to abandon Flike, the dog finds him hiding under a footbridge. Finally in desperation, Umberto takes the dog in his arms and walks on to a railway track as a speeding train approaches. Flike becomes frightened, wriggles free and flees.  Umberto runs after him, and it seems as if Flike is leading him away from his intended death. He coaxes Flike back and starts playing with him in the park reaffirming his love for his little dog and for life.  But he is still homeless and destitute, and ominous music plays in the background as the film ends.

==Reception==
Ingmar Bergman cited Umberto D. as his favorite film. 

===Awards and nominations===
* Vittorio De Sica nominated for the Grand Prix - 1952 Cannes Film Festival   
* 1955 New York Film Critics Circle Award for Best Foreign Film
* Cesare Zavattini nominated for the 1957 Academy Award for Best Writing, Motion Picture Story

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 