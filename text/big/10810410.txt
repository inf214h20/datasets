90 Miles
 
{{Infobox film
| name           = 90 Miles
| image          = 
| alt            =  
| caption        = 
| director       = Juan Carlos Zaldívar
| producer       = 
| writer         = Juan Carlos Zaldívar|
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 143 minutes (directors cut) and 53 minutes (POV version)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

90 Miles is a 2001  ). 90 Miles recounts the strange twist of fate that took Juan Carlos Zaldívar across one of the worlds most treacherous stretches of water. It is a journey of a family in search for healing and understanding.  Probing and thoughtful, Zaldívar uncovers the emotional distance opened in thousands of families by the 90 miles between the U.S. and Cuba.

==Synopsis==
In 1980, Zaldívar was a 13-year-old who had grown up as a loyalist of the Cuban Revolution jeering in the streets at the thousands of "Marielitos" leaving the island by boat for the United States. However, within weeks, he had become a Marielito himself, headed with the rest of his family for a new life in Miami. Now a U.S.-based filmmaker, Zaldívar recounts the strange twist of fate that took him across one of the worlds most treacherous stretches of water in 90 Miles.

As related by Zaldívar in the intensely personal and evocative film, arrival in South Florida is only the beginning of the familys struggle to comprehend the full meaning of their passage into exile. What follows is an intimate and uneasy accounting of the historical forces that have split the Cuban national family in two, and which shape the passage of values from one generation to the next.

90 Miles was filmed over eight years, as the filmmaker returned to Cuba for the first time in 1998 to visit his hometown of Holguín, and again in 1999. Using news clips, family photos and home movies, the film creates a portrait of recent Cuban history, as dramatized by one familys aspirations and disappointments. Zaldívars is a tale rich in crossed borders, cultural re-assimilation and cross-cultural ferment. In the early 1980s, during the Mariel boatlift, Zaldivar was a highly promising student. Having grown in a socialist country, Zaldivar was happy to join in the regimes efforts to publicly humiliate some of the thousands of Cubans who were leaving in the boatlift, labeling them gusanos (worms).

Unbeknownst to him, a change was in the horizon for Zaldivar and his family. One of his uncles who had fled to the U.S. in the 1960s offered to arrange the familys boatlift to Florida — on the condition that all or none of the family go. The family was reluctant to interrupt the lives of their children — Zaldívar and his two sisters — if the siblings were not willing. So the decision fell, for all practical purposes, on the young ones.

Faced with the sudden possibility of leaving the country, Zaldívars family revealed to him, for the first time, their ongoing disillusionment with the Cuban Revolution. Out of this difference grew a cruel dilemma for the child. In the end, unable to deny his family the opportunity to start anew in Florida and deciding to place blind trust in his parents, Zaldívar agreed to go.

In the United States, though homesick and nostalgic for his homeland and surprised both by what he liked and disliked about North American life, Zaldívar resumed learning and growing with the headstrong adaptability so often demonstrated by youth. He continued his media studies, moved to New York, became a filmmaker and came out as a gay man. Similarly, his two sisters made happy lives, marrying and having children.

Interestingly, it is the older generation, which had most wanted to come to Florida, that experienced the greater problems. Zaldívars father, especially, grew depressed and remote from his son after arriving. The fathers dream of building his own home in Cuba, derailed by the Revolution, seemed to lose its power in the United States, where many Cuban men find themselves working jobs well below their professional level. Feeling betrayed by the Cuban Revolution and defeated by the American Dream, Zaldívars father withdrew into himself. Where Zaldívars revolutionary zeal in Cuba had created a divide of silence between father and son, in the U.S., the fathers sense of failure and futility only widened the gap.

90 Miles is the account of Zaldívars quest to piece together the twists — and consequences — of his familys journey into exile. Above all, it is a search for understanding and healing between father and son, by uncovering the emotional distance opened up by just 90 miles of water between Cuba and the U.S. mainland.

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 