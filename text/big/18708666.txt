Heera Aur Pathar (1964 film)
{{Infobox film
| name           = Heera aur pathar
| image          = Heera aur pathar.jpg
| image_size     =
| caption        = Waheed Murad and Zeba in Heera aur pathar 
| director       = Pervaiz Malik
| producer       = Waheed Murad
| writer         =
| narrator       =
| starring       = Waheed Murad Zeba Nirala Ibrahim Nafees Kamal Irani
| music          = Sohail Rana
| cinematography =
| editing        = M. Aqeel Khan
| distributor    = Film Arts
| released       = 12 November 1964
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}
Heera aur pathar (Urdu: ہیرا اور پتھر) is a 1964 Pakistani Urdu black-and-white film. It is the first film of Waheed Murad as a lead actor. The film was released at Karachis Naz Cinema, on November 12, 1964, and completed 56 weeks on cinemas with 25 weeks on main cinemas in Karachi. Waheed Murad introduced Pervaiz Malik for the first time in the Pakistani cinema world. Moreover, he had also introduced film editor, M. Aqeel Khan, who had also received the Nigar award for this movie. 

==Cast and film crew==
Casting includes Waheed Murad, Zeba, Nirala, Ibrahim Nafees and Kamal Irani. Waheed Murad played the role of a gadha gari wala (Donkey cart owner).  This film is based on his innocent love with Zeba. At that time, the film attracted many Indian cinema-goers to see this film. Waheed Murad won Nigar award as the best actor of 1964. Heera aur pather proved to be the luckiest film for all film cast and the production crew, as it gave a breakthrough to the careers of Waheed Murad, Pervaiz Malik, Sohail Rana, M. Aqeel Khan and Zeba.

==Music==
The music of the film was composed by  , Najma Niazi, Khursheed Begum, Saleem Shahzad, Mala (Pakistani singer)|Mala, Wasim Farooqui and Talat Siddiqui.

===Songography===
*Mujhay tum se mohabbat hai... by Ahmed Rushdi and Najma Niazi
*Gori simti jaye sharam se... by Ahmed Rushdi
*Aaj mujhe kya hua... by Najma Niazi
*Rahey himmat jawan apni... by  Khursheed Nurali (Sheerazi) and Saleem Shahzad Mala
*Mujhay ek larki se pyar ho gaya... by Saleem Shahzad and Talat Siddiqui
*Aao manaen picnic... by Waseem Farooqui and Khursheed Begum

==Awards==
Heera aur pathar won two Nigar awards for the year 1964. Waheed Murad won award in the best actor category, and M. Aqeel Khan won the award in the best film editor category.

==References==
 

 
 
 
 
 


 