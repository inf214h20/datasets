Totò and Carolina
 
{{Infobox film
| name           = Totò and Carolina
| image          = 
| caption        = 
| director       = Mario Monicelli
| producer       = Alfredo De Laurentiis
| writer         = Ennio Flaiano Mario Monicelli Age & Scarpelli Rodolfo Sonego
| starring       = Totò
| music          = 
| cinematography = Domenico Scala
| editing        = Adriana Novelli
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Totò and Carolina ( ) is a 1955 Italian comedy film directed by Mario Monicelli and starring Totò. The film was banned when it was first released, as it made fun of a policeman.   

==Plot==
During a police raid at Villa Borghese, the agent Antonio Caccavallo, a widower with dependent child and parent, stops along with other women of life even Carolina. In fact, the girl is just ran away from home because she was pregnant. The poor Caccavallo so by the Commissioner is obliged to give back to the country of origin, and give it to a relative. But the task of arranging Carolina will prove to be more complicated than expected, partly because of the reluctance of the girl who does not want to set foot where she had fled. Nevertheless Carolina somehow manages to confide in and bond with the policeman, which includes professional obligations, and despite a few troubles do not go (try to escape, and even suicide) does not grudge. In the end, lying to his superiors about the success of the mission, it will load Caccavallo welcoming her to his house, where for a long time lacked a female presence.

In the final scene, after Caccavallo told the Commissioner: "You will find a fool who gets angry," follows the demand Carolina who was waiting in the street, who asked, "Where are we going?". Caccavallo replied: "A house of a fool", which contains a part of his being compassionate and its lack of affection, and other bankruptcy as guardian of legality.

==Cast==
* Totò as Antonio Caccavallo
* Anna-Maria Ferrero as Carolina De Vico
* Arnoldo Foà as Commissary
* Maurizio Arena as Mario, the thief
* Tina Pica as Lady at hospital
* Gianni Cavalieri as Man from Venice
* Rosita Pisano as Mrs. Barozzoli
* Fanny Landini as Prostitute
* Nino Vingelli as Brigadiere
* Enzo Garinei as Dott. Rinaldi
* Guido Agostinelli as Caccavallos father
* Giovanni Grasso as Deputy Commissary

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 