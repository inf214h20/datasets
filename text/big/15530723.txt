Krishnarjuna
{{Infobox film
| name           = Krishnarjuna
| image          = krishnarjuna_poster.jpg
| caption        =
| writer         = Madhukuri Raja  
| story          = P.Vasu
| screenplay     = P.Vasu
| producer       = Mohan Babu
| director       = P.Vasu Nagarjuna Akkineni Manchu Vishnu Mohan Babu Mamta Mohandas
| music          = M. M. Keeravani Om Prakash
| editing        = Gautham Raju
| studio         = Sree Lakshmi Prasanna Pictures
| released       =  
| runtime        = 154 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Nagarjuna Akkineni in a guest role and Mohan Babu in cameo appearance and music composed by M. M. Keeravani. The film recorded as flop at box-office. 

==Plot== Nagarjuna Akkineni) comes to Arjuns assistance. Arjun gains some powers from his companionship with the Lord and overcomes the evils that threaten his peace and family life. He also succeeds in reopening the doors of the temple. Arjun then dies by jumping of a tall building because of his faith in Lord Krishna who revives him a minute after his death. The film ends on a happy note.

==Cast==
{{columns-list|3| Nagarjuna Akkineni as Lord Krishna / Bangaram
* Manchu Vishnu as Arjun
* Mamta Mohandas as Sathya
* Mohan Babu as Baba
* Nassar as Pedababu
* Dev Gill Nepolian
* P. Vasu
* Brahmanandam Sunil  Venu Madhav 
* M. S. Narayana
* Tanikella Bharani 
* Giri Babu  Mallikarjuna Rao 
* Raghu Babu 
* Lakshmipathi 
* Gundu Hanumantha Rao 
* Srinivasa Reddy 
* Satyam Rajesh
* Ananth
* Kadambari Kiran 
* Uttej 
* Suthi Velu Prema  Manorama 
* Bhuvaneswari 
* Surekha Vani 
* Apoorva 
* Poornima 
* Srilalita 
* Gayatri 
* Master Akshay  
* Baby Greeshma
}}

==Soundtrack==
{{Infobox album
| Name        = Krishnarjuna
| Tagline     = 
| Type        = film
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 2007
| Recorded    = 
| Genre       = Soundtrack
| Length      = 34:01
| Label       = 24 Frames Music
| Producer    = 
| Reviews     =
| Last album  = Pandurangadu   (2008)
| This album  = Krishnarjuna   (2008)
| Next album  = Gunde Jhallumandi   (2008)
}}
The music was composed by M. M. Keeravani. Music released on 24 Frames Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 34:01
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = A A Aa E Ee
| lyrics1 = Ramajogayya Sastry 
| extra1  = Shankar Mahadevan
| length1 = 4:23

| title2  = Aaja Mehabooba
| lyrics2 = Sahithi
| extra2  = Achu Rajamani|Achu, Geetha Madhuri
| length2 = 4:10

| title3  = Buggalerrabada
| lyrics3 = Sahithi
| extra3  = M. M. Keeravani, Mamta Mohandas
| length3 = 4:45

| title4  = Yamaranjumeedha	
| lyrics4 = Gurukiran Sunitha
| length4 = 4:12

| title5  = Thruvata Baba
| lyrics5 = Jonnavithhula Ramalingeswara Rao    Tippu
| length5 = 4:18

| title6  = Pedda Marrikemo 
| lyrics6 = Ramajogayya Sastry 
| extra6  = Mano (singer)|Mano, Madhu Balakrishnan
| length6 = 4:32

| title7  = Yedi Manchi 
| lyrics7 = Ramajogayya Sastry 
| extra7  = Madhu Balakrishnan
| length7 = 4:30

| title8  = We R Coming 
| lyrics8 = Ramajogayya Sastry 
| extra8  = Pranavi,Bhargavi Pillai,Noel
| length8 = 3:11
}}

==References==
 

==External links==
*  

 
 
 
 


 
this film was copied from english movie ///"bruce almighty"/// and some scenes were included and some are excluded