Ebar Shabor
{{Infobox film
| name           = Ebar Shabor
| image          = Ebar Shabor poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Arindam Sil
| producer       = 
| writer         = Shirshendu Mukhopadhyay
| screenplay     = Padmanabha Dasgupta  Arindam Sil
| based on       = Rwin by Shirshendu Mukhopadhyay Rahul Banerjee
| music          = Bickram Ghosh 
| editing        = Sujay Dutta Ray
| studio         = Reliance Entertainment Mundus Services
| distributor    = Reliance Entertainment
| released       =   cinematography  = Shirsha Ray
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali Thriller Thriller film based on the Detective story  Rwin by Shirshendu Mukhopadhyay.    The film is directed by Arindam Sil, and produced by Reliance Entertainment and Mundus Services.  This is the second directorial venture of the director after Aborto 
The film is based on the investigation of the murder of Mitali Ghosh (Swastika Mukherjee).What follows is a revelation of certain shameful truths that are prevalent in the life of a typical high society person.  The film released on 2 January 2015. 

==Plot==
A police detective Shabor Dasgupta (Saswata Chatterjee) is entrusted with the daunting task of solving the mystery surrounding the murder of Mitali Ghosh (Swastika Mukherjee), a woman with a messy past, who was killed on the night she had thrown a party for friends and family. Rahul Banerjee), who has relationships with several women, including a schools physical education teacher, Julekha Sharma (June Malia), and another girl, Ksanika (Debolina Dutta).
As Shabor probes deeper, he learns many disturbing secrets about the Ghosh family, including the fact that Mitali had once eloped with para boy Pantu Haldar (Ritwick Chakraborty). She had married and left him within six months, ruining his future in the process. Another character, Deol, also comes into the picture. The detective now has to deal with the complex relationship problems that run deep in the family and the mystery gets more and more complicated.

==Cast==
*Saswata Chatterjee as Shabor Dasgupta
*Abir Chatterjee as Mithu Mitra
*Ritwick Chakraborty as Pantu Haldar
*Swastika Mukherjee as Mitali Ghosh
*Payel Sarkar as Joyeeta Ghosh
*June Malia as Rita/Julekha
*Debolina Dutta as Kshanika Rahul Banerjee as Samiran Bagchi
*Subhrajit Dutta
*Dipankar De as Arun Ghosh
*Santu Mukherjee
*Rajat Ganguly as Varun Ghosh

==Reception==
Upon the release Ebar Shabor received positive response from Critics. The Times of India gave it 4 star out of 5, and said: "Ebar Shabor is the directors baby — right from the animated opening credits, the spot-on camera work by DoP Shirsha Ray, the unnervingly real chase sequences, the subtle interplay of sadness and humour to the rush of emotions in the last 10 minutes. Its a film thats as real as life itself.
Watch it, watch it again. Youll love it every time." 
The movie has been running to packed houses over the weekend. All multiplex shows were sold out while at Nandan, Ebar Shabor had an 80% occupancy. Weekday shows have had 70-75% attendance across multiplexes. 

==Sequel==
After the films success, makers are planning a sequel to this film. 

==See also==
*Aborto

==References==
 

==External links==
*  
*  on YouTube

 
 
 
 
 