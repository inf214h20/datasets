Chromophobia (film)
{{Infobox film
| name= Chromophobia
| image= Chromophobiaposter.jpg
| caption = Theatrical release poster
| writer=Martha Fiennes
| starring=Penélope Cruz Ralph Fiennes Kristin Scott Thomas
| director=Martha Fiennes
| music=Magnus Fiennes
| distributor=Quinta Communications
| released= 
| runtime= 136 minutes
| country=United Kingdom
| language=English
| movie_series=
| awards=
| producer=Tarak Ben Ammar Ron Rotholz
| budget= $10,000,000 (estimated)
}}

Chromophobia is an ensemble film which debuted at the 2005 Cannes Film Festival in France.    The films crew was made up of a brother-sister trio - Martha Fiennes wrote and directed the film, Ralph Fiennes starred in it, and Magnus Fiennes composed the score. The film was shot entirely in Great Britain and the Isle of Man.

==Plot==

Chromophobia takes a look at several people and relationships. Some of these people are very wealthy and have become so obsessed with their wealth and material objects they are very detached from more important things in life. The things they choose to reject in their pursuit for the finer things in life include friendship, their children, love and integrity. At one point or another in the movie, each character begins to realize and then face their problems and in a sense their lives come crashing down. The characters then begin to see if they can get their lives back and carry on with a new look.

==Cast==
*Penélope Cruz as Gloria
*Ralph Fiennes as Stephen Tulloch
*Kristin Scott Thomas as Iona Aylesbury
*Rhys Ifans as Colin
*Ian Holm as Edward Aylesbury
*Damian Lewis as Marcus Aylesbury
*Ben Chaplin as Trent

==References==
 

== External links ==
*  

 
 
 
 
 
 

 