Blue Denim
{{Infobox film
| name           = Blue Denim Blue Jeans (UK)
| image          = BlueDenim1.jpg
| image_size     =
| caption        = Movie poster for Blue Denim Philip Dunne
| producer       = Charles Brackett
| based on       =   Philip Dunne Edith R. Sommer
| narrator       =
| starring       = Carol Lynley Brandon deWilde
| music          = Bernard Herrmann
| cinematography = Leo Tover
| editing        = William H. Reynolds
| distributor    = 20th Century-Fox
| released       =  
| runtime        = 89 min.
| country        = United States English
| budget         = $980,000 
| gross          = $2.5 million (est. US/ Canada rentals) 
}}
 Broadway play All Fall Midnight Cowboy Playhouse Theatre.
 Marsha Hunt and Roberta Shore appear as supporting characters.

Dealing with the issues of teenage pregnancy and (then-illegal) abortion, both versions were not without controversy. The play and the film each had different endings, and the word abortion was excised from the plays script when it was adapted into the films screenplay. 

Blue Denim has never been released on home video media.

==Plot==
The story is set in Dearborn, Michigan, during the 1950s, and revolves around sixteen-year-old Arthur Bartley (Brandon deWilde) and his schoolmates, fifteen-year-old Janet Willard (Carol Lynley) and Ernie (Warren Berlinger). While widowers-daughter Janet laughs at Arthur and Ernies forays into smoking, drinking, and playing cards, shes always been interested in Arthur, and as Arthurs parents try to shelter him from negative things in life (like the euthanasia of the family dog, done while hes at school), he turns to Janet for comfort.

The relationship between Janet and Arthur results in her becoming pregnant. Unable to ask their parents (who misinterpret their pleas as "ordinary" teenage curiosity about sex and adulthood) for help, they turn to Ernie, whod boasted earlier about "helping a sailor who got his girl in trouble" by directing him to an abortionist &ndash; only to discover Ernie made it all up, based on secondhand stories. The three seek together to arrange an abortion and raise the funds, only to be discovered by their parents at the last moment. In the meantime, Arthur and Janet find out how much they do not yet know about life &ndash; and how much they truly care about each other.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Carol Lynley || Janet Willard
|-
| Brandon deWilde || Arthur Bartley
|-
| Macdonald Carey || Maj. Malcolm Bartley, Ret.
|- Marsha Hunt || Jessie Bartley
|-
| Warren Berlinger || Ernie
|-
| Buck Class || Axel Sorenson
|-
| Nina Shipman || Lillian Bartley
|- Vaughn Taylor || Professor Willard
|-
| Roberta Shore || Cherie
|- Mary Young || Aunt Bidda
|-
| Malcolm Atterbury || Marriage License Clerk (uncredited)
|-
| William Schallert|| George - Bank Vice President (uncredited)
|}

==Differences between stage and film versions==
In the original stage version, Janet does have her pregnancy aborted, and she and Arthur talk it over later as they settle their feelings for each other. When the play was adapted for Hollywood, however, strict production codes forbade anything but the condemnation of abortion, so the storyline was changed. Arthur and Janet instead go off together, to get married and stay with Janets aunt in another city until the baby is born.

==Critical and public reception== sociological impact, Fundamentalist preachers.

==Home media==
 
  soundtrack cover for Blue Denim.]]

===Video===
20th Century Fox has never released Blue Denim on VHS or DVD media. It plays occasionally on cable TV and video on demand. 

===Soundtrack=== The Man Who Knew Too Much (1956), Vertigo (film)|Vertigo (1958), North by Northwest (1959) and Psycho (1960 film)|Psycho (1960) for Alfred Hitchcock.

Herrmanns Blue Denim  score was released on CD on November 15, 2001. It is on the Film Score Monthly (FSM) label, as FSM0415, along with Elmer Bernsteins score for The View from Pompeys Head (1955). It has been described by FSM as a "Baby Vertigo" type of score, reminiscent of Herrmanns anguished romantic writing for Hitchcock.  

==In other media==
*In Truman Capotes book In Cold Blood (1966), Bobby Rupp, Nancy Clutters beau, says, "We talked for a while, and made a date to go to the movies Sunday night - a picture all the girls were looking forward to, Blue Denim." Less Than Zero (when Clay and Blair are in bed in the loft).

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 