The Boxing Kangaroo
 

{{Infobox film
| name           = The Boxing Kangaroo
| image          = TheBoxingKangaroo.jpg
| image_size     =
| caption        = Screenshot from the film
| director       = Birt Acres
| producer       =  
| writer         =
| starring       =
| cinematography = Birt Acres
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom Silent
| budget         =
| gross          =
}}
 British Short short black-and-white silent documentary film, produced and directed by Birt Acres for exhibition on Robert W. Pauls peep show Kinetoscopes, featuring a young boy boxing with a kangaroo. The film was considered lost until footage from an 1896 Fairground Programme, originally shown in a portable booth at Hull Fair by Midlands photographer George Williams, donated to the National Fairground Archive was identified as being from this film. 

It was one of at least four boxing-themed films Acres produced in 1896, the others being Boxing Match; or, Glove Contest, A Boxing Match in Two Rounds by Sgt. Instructor F.Barrett and Sgt. Pope and A Prize Fight by Jem Mace and Burke.     The following year, German filmmaker Max Skladanowsky made a similar film depicting a man boxing with a kangaroo, entitled Das boxende Känguruh.   

==References==
 

== External links ==
* 
*   at Silent Era

 
 
 
 
 
 

 
 