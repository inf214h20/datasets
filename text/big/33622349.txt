The Mad Songs of Fernanda Hussein
{{Infobox film
| name           = The Mad Songs of Fernanda Hussein
| image          = 
| alt            = 
| caption        = 
| director       = John Gianvito
| producer       = John Gianvito
| writer         = John Gianvito
| starring       = Bonnie Chavez Sherri Goen Thia Gonzalez
| music          = Johannes Ammon Jakov Jakoulov Igor Tkachenko
| cinematography = Ulli Bonnekamp
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 168 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Mad Songs of Fernanda Hussein is a 2001 film. In Greece the film is known as "Ta tragoudia trelas tis Fernanda Hussein".

==Cast==
*Bonnie Chavez - Police Dispatcher
*Sherri Goen
*Thia Gonzalez	
*Cliff Gravel - The Veteran
*Carlos Moreno Jr. - Mike
*Robert Perrea	
*Elizabeth Pilar	
*Dustin Scott
*Carlos Stevens

==Awards==
At the 2001 Buenos Aires International Festival of Independent Cinema, the John Giavito won a Special Award for his work on the film. The film was also nominated for Best Film. At the 2001 Thessaloniki Film Festival, the film was nominated for a Golden Alexander.

==Reception==
The film has a critic rating of 40% on Rotten Tomatoes.  

==References==
* 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 