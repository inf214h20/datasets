Midnight Manhunt
{{Infobox Film
| name           = Midnight Manhunt
| image          = Midnight Manhunt.jpg
| caption        = DVD cover
| director       = William C. Thomas
| producer       = Maxwell Shane David Lang
| starring       = Alexander Laszlo
| cinematography = Fred Jackman Jr.
| editing        = Henry Adams
| distributor    = Paramount Pictures (theatrical) Metropolis Productions Inc.
| released       =   July 24, 1945
| runtime        = 64 min
| country        = USA
| awards         = English
| budget         =
| preceded_by    =
| followed_by    = 1945 crime David Lang. The film premiered on July 24, 1945 and is in the public domain.
 Ann Savage, Leo Gorcey and George Zucco.

==Plot summary== Ann Savage) is first on the scene, but she is soon in competition with her boyfriend, fellow reporter Pete Willis (William Gargan). The killer traps Sue in the wax museum when he returns there looking for the body. Leo Gorcey plays the caretaker of the wax museum.

==Cast==
*William Gargan ...  Pete Willis Ann Savage ...  Sue Gallagher
*Leo Gorcey ...  Clutch Tracy
*George Zucco ...  Jelke Paul Hurst ...  Murphy
*Don Beddoe ...  Det. Lt. Max Hurley
*Charles Halton...  Henry Miggs
*George E. Stone ...  Joe Wells

==See also==
* List of films in the public domain

==External links==
*  
*  
*  

 
 
 
 
 
 


 