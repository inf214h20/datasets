The Widow from Chicago
{{Infobox film
| name           = The Widow from Chicago
| image          = The_Widow_From_Chicago_1930_Poster.jpg
| caption        = Theatrical poster
| director       = Edward F. Cline
| producer       =
| writer         = Earl Baldwin Neil Hamilton Frank McHugh
| music          = Leo F. Forbstein Erno Rapee
| cinematography = Sol Polito
| editing        = Edward Schroeder
| distributor    =  
| released       =  
| runtime        = 64 minutes
| country        = United States English
| budget         =
}} Neil Hamilton and Frank McHugh. Planned as a full-scale musical, the songs were cut from the film before release due to the publics aversion for musicals.

==Synopsis==
Two detectives, played by John Elliot and Harold Goodwin, board a train in pursuit of a gangster, who is played by Neil Hamilton. Hamilton is travelling to New York to work for Edward G. Robinson, a notorious gangster who owns a nightclub. Hamilton jumps off the train near a bridge crossing and since no trace of him can be found the police believe him to be dead. Goodwin assumes Hamiltons identity and joins Robinsons gang but is quickly discovered to be an imposter and shot. Determined to find out who killed her brother, Alice White poses as Hamiltons widow and attempts to get a job at Robinsons nightclub. Hamilton eventually shows up and White is almost exposed as an imposter. Hamilton, however, is eventually persuaded by White and he promises not to tell Robinson the truth. White and Hamilton fall in love. During a hold-up, White protects Hamilton by shooting at a cop in the back. Hamilton begins to think of reforming due to Whites influence. White eventually gets Robinson to confess that he shot her brother by pretending to be interested in him. She leaves the phone off the hook while the police listen in on his confession. When the police show up, Robinson realizes what White has done and uses White as a shield against the police but Hamilton manages to save White and Robinson is forced to surrender to the police.

==Preservation==
A 62 minute version of the film survives and has been broadcast on television and cable. Due to the publics backlash against musicals late in 1930, all of the musical numbers were cut from the film to make it more marketable. These cuts accounts for the short length of the film. The film was advertised as a gangster picture, a genre which had become very popular with the public. The complete musical film was released intact in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this full version still exists.

==Cast==
*Alice White as Polly
*Edward G. Robinson as Dominic Neil Hamilton as Swifty Dorgan
*Frank McHugh as Slug
*Lee Shumway as Johnston
*Brooks Benedict as Mullins
*E.H. Calvert as Davis
*Betty Francisco as Helen Harold Goodwin as Jimmy

==External links==
* 

 

 
 
 
 
 
 
 
 