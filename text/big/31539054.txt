Snowtown (film)
 
{{Infobox film
| name           = Snowtown
| image          = Snowtown (film).jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Justin Kurzel
| producer       = Anna McLeish Sarah Shaw
| screenplay     = Shaun Grant
| story          = Shaun Grant Justin Kurzel
| based on       = Snowtown murders
| starring       = Daniel Henshall Lucas Pittaway Louise Harris
| music          = Jed Kurzel
| cinematography = Adam Arkapaw
| editing        = Veronika Jenet Warp Films Madman Films
| released       =  
| runtime        = 120 minutes  
| country        = Australia
| language       = English
| budget         = 
| gross          = $8,452 
}}
Snowtown, also known as The Snowtown Murders, is a 2011 Australian film based on the true story of the  Snowtown murders directed by Justin Kurzel and written by Shaun Grant.      

==Plot== Jamie lives Elizabeth Harvey, paedophiles and homosexuals, deals with the boyfriend by continually harassing him until he leaves town. John begins to assume the role of Jamies father figure.

Jamie finds himself slowly drawn into Johns homophobic and violent tendencies, unable to escape because of his charismatic and intimidating dominance. On one occasion, he has Jamie shoot his dog. John meanwhile influences the rest of the neighborhood with his extremely homophobic views, and separates Barry from his younger boyfriend Robert Wagner (serial killer)|Robert. Only Troy seems to dislike John. Barry soon disappears, leaving behind only a tape saying that he is going to Queensland.

Shortly afterward, Jamie visits his drug-addicted best friend Gavin with John, who takes a dislike to Gavin. Later one night John and Robert take Jamie into his garden shed and show him the bodies of Barry and Gavin. Distressed, Jamie lashes out at John but is powerless to resist, and remains under his influence. When John learns that Jamie has been abused by Troy, he and Robert torture Troy. Jamie later kills the brutalized Troy in an act of mercy. Now desensitized, Jamie assists John in carrying out several murders. John and his team store the bodies in the vault of an abandoned bank in the town of Snowtown, South Australia|Snowtown.

Jamie is persuaded by John to lure his half-brother Dave to the bank building, ostensibly to look at a computer for sale. Jamie drives with him to the town, vaguely conscious of what he is doing, and leads Dave into the building, where he is met by John and Robert. Unaware of what is going on, Dave watches Jamie shut the door of the bank.

Against a black screen, captions reveal that South Australian Police discovered the remains of eight people stored in barrels in the bank vault of Snowtown on 20 May 1999, and the following day John Bunting and Robert Wagner were arrested.

==Cast==
* Daniel Henshall as John Bunting
* Lucas Pittaway  as James Vlassakis
* Aaron Viergever as Robert Wagner
* David Walker as Mark Haydon
* Louise Harris as Elizabeth Harvey
* Keiran Schwerdt as Thomas Trevilyan
* Bob Adriaens as Gavin
* Frank Cwiertniak as Jeffrey
* Matthew Howard as Nicholas
* Marcus Howard as Alex
* Anthony Groves as Troy
* Richard Green as Barry
* Beau Gosling as David

==Production==
Screen Australia announced in March 2010 that it would be funding the film    and Film Victoria provided $245,000.  The film was produced by Warp Films Australia,  a collaboration between Warp Films and distributor Madman Entertainment.

Peter Campbell of Warp Films Australia had to get the remaining suppression orders lifted so the film could be premiered.  

Snowtown is Justin Kurzels first feature length film as director.  His short film Bluetongue was shown at the 2005 Cannes Film Festival. 
 Davoren Park. Kurzel himself grew up in the area and felt that using locals would move the film from being a one dimensional horror show to a tragic human story showing what happens when people are disadvantaged. Davoren Park is considered one of the most violent and dysfunctional suburbs in Australia and a place where emergency vehicles fear to go without a police escort. According to Kurzel, far from the "wow, Im going to be a movie star" attitude that he had expected, he had some difficulty convincing them to take part.  
 Adelaide metropolitan area.

==Release== premiered at the 2011 Adelaide Film Festival and won the festivals "Audience Award", and was selected as one of seven films from around the world that were shown at International Critics Week competitions that ran in parallel with the 2011 Cannes Film Festival.     At Cannes, the film was awarded with a Special Mention. 

The film was released in the United Kingdom by Revolver Entertainment and IFC Midnight acquired the North America distribution rights. 

==Reception==
Snowtown received positive reviews. Review aggregator Rotten Tomatoes lists an 83% approval rating based on 65 reviews with an average rating of 7.5 out of 10 with the consensus "Its a bleak and brutal endurance test, but for viewers with the strength and patience to make it to the end, Snowtown will prove an uncommonly powerful viewing experience." 
 Jaime Rosaless Vengeance Is Mine demystified the killers macabre criminal career in their various ways; what Snowtown does is create a social-realist horror story showing the killer as parodic paterfamilias." 
 SBS awarded the film three-and-a-half stars out of five, commenting that director Kurzel "sidesteps the gore – mostly – to focus instead on the circumstances that enabled the atrocities to occur...It’s a gripping, discomforting watch." 
 Richard Wilkins, gave the film a rating of zero stars, stating "This is as close to a snuff movie as I ever want to see… I dont care if its rooted in truth or not, its appalling. Ive seen it so you dont have to."  This review was criticised by culture zine Pedestrian TV  and was dismissed by Kurzel  and actress Louise Harris.  

===Awards and nominations===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Australasian Performing Rights Association Award Best Music Jed Kurzel
| 
|- Australian Directors Guild Award Best Director Justin Kurzel
| 
|- AACTA Awards  (1st AACTA Awards|1st)  AACTA Award Best Film Anna McLeish
| 
|- Sarah Shaw
| 
|- AACTA Award Best Direction Justin Kurzel
| 
|- AACTA Award Best Adapted Screenplay Shaun Grant
| 
|- AACTA Award Best Actor Daniel Henshall
| 
|- AACTA Award Best Supporting Actress Louise Harris
| 
|- AACTA Award Best Cinematography Adam Arkapaw
| 
|- AACTA Award Best Editing Veronika Jenet
| 
|- AACTA Award Best Original Music Score Jed Kurzel
| 
|- AACTA Award Best Sound Frank Lipson
| 
|- Andrew McGrath
| 
|- Des Kenneally
| 
|- Michael Carden
| 
|- John Simpson
| 
|- Erin McKimm
| 
|- AFI Members Choice Award Anna McLeish
| 
|- Sarah Shaw
| 
|- Film Critics Circle of Australia Awards Best Film Anna McLeish
| 
|- Sarah Shaw
| 
|- Best Director Justin Kurzel
| 
|- Best Screenplay Shaun Grant
| 
|- Best Actor Daniel Henshall
| 
|- Best Supporting Actress Louise Harris
| 
|- Best Cinematography Adam Arkapaw
| 
|- Best Editor Veronika Jenet
| 
|- Best Music Score Jed Kurzel
| 
|- Inside Film Awards Best Director Justin Kurzel
| 
|- Best Cinematography Adam Arkapaw
| 
|- Best Editing Veronika Jenet
| 
|- Best Production Design Fiona Crombie
| 
|- Best Sound Frank Lipson
| 
|- Andrew McGrath
| 
|- Des Kenneally
| 
|- Michael Carden
| 
|- John Simpson
| 
|- Erin McKimm
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Warp Films

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 