Makale Mappu Tharu
{{Infobox film
| name           = Makale Mappu Tharu
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = EK Thyagarajan
| writer         =
| screenplay     =
| starring       = Prem Nazir Adoor Bhasi Ratheesh Sathyakala
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Sree Murugalaya Films
| distributor    = Sree Murugalaya Films
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by EK Thyagarajan. The film stars Prem Nazir, Adoor Bhasi, Ratheesh and Sathyakala in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Prem Nazir
*Adoor Bhasi
*Ratheesh
*Sathyakala
*TG Ravi

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Roopam Madhuritharoopam || Krishnachandran, Lathika || Poovachal Khader || 
|-
| 2 || Vannaalum chengannoore || P. Madhuri || Poovachal Khader || 
|-
| 3 || Vidhiyo kadamkathayo || KP Brahmanandan || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 