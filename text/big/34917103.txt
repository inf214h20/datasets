Cousin Kate (1921 film)
{{infobox film
| name           = Cousin Kate
| image          = Cousin_Kate_1921_newspaperad.jpg
| imagesize      = 190px
| caption        = Newspaper advertisement
| director       = Lucille McVey (aka Mr. & Mrs. Sidney Drew|Mrs. Sidney Drew) Albert E. Smith
| writer         = Lillian Case Russell (adaptation)
| based on       =  
| starring       = Alice Joyce Gilbert Emery
| music          =
| cinematography = Joseph Shelderfer
| editing        =
| distributor    = Vitagraph Company of America
| released       =  
| runtime        = 50 minutes
| country        = United States Silent (English intertitles)
}} lost  Sidney Drew. Alice Joyce stars in this film version.  

==Cast==
*Alice Joyce - Kate Curtis
*Gilbert Emery - Heath Desmond
*Beth Martin - Amy Spencer
*Inez Shannon - Mrs. Spencer
*Leslie Austin - Reverend James Bartlett
*Freddie Verdi - Bobby
*Frances Miller - Jane (* billed Frances Miller Grant)
*Henry Hallam - Bishop

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 


 