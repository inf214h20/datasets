The Steel Trap
{{Infobox film
| name           = The Steel Trap
| image          = The Steel Trap film.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew L. Stone
| producer       = Bert E. Friedlob
| screenplay     = Andrew L. Stone
| starring       = Joseph Cotten Teresa Wright
| music          = Dimitri Tiomkin
| cinematography = Ernest Laszlo Otto Ludwig
| studio         = Thor Productions
| distributor    = 20th Century-Fox
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross = $1 million (US rentals) 
}}
The Steel Trap is a 1952 thriller film noir written and directed by Andrew L. Stone, and starring Joseph Cotten and Teresa Wright. 

==Plot==
Joseph Cotten plays Jim Osborne, a bank officer in Los Angeles who is tempted to steal from his own bank and flee the country before he can be caught.

His fantasy of doing this becomes a real plan when he learns that Brazil does not allow extradition; it is also an attractive destination and is close enough to reach by traveling over a weekend. If he steals the money at close of business on a Friday, he need only reach Brazil by the Sunday night to be immune to capture before the theft is even discovered.

But the season when the bank opens on Saturdays is about to begin, so he must take action the same week or else wait for months.

He tells his wife Laurie (Teresa Wright) that the bank is sending him to Rio de Janeiro on business and he wants her and their daughter to travel with him. It is a great opportunity for his career, he says, and he has been given it in preference to the officer who would normally be sent, so she should not talk to anyone at the bank about it.

Laurie is delighted with the news but insists their daughter stay at home with Lauries mother. Jim decides he can send for her after Laurie knows they are staying in Rio, and goes ahead with the crime.
 illegally exporting gold, and the money is revealed.
 Unreported large standby and the flight is already full.  They will not be able to reach Rio on Sunday.

Now fearing arrest, Jim checks into a hotel using a false name. Laurie overhears this, realizes the truth, and confronts him. When he admits the theft, she is shattered and will have no part in it; she leaves him and flies back to Los Angeles.

Within hours, Jim realizes that his wife and daughter are far more important to him than his dreams of wealth. But fortunately it is still possible to save the situation. Laurie was too upset to tell anyone why she had suddenly returned, and Jim has used his own money for their extravagant travel expenses, so the banks money is intact. After phoning his wife, Jim flies to Los Angeles himself, reaches the bank as it is preparing to open on Monday, and just manages to replace the money before it is missed.

==Cast==
* Joseph Cotten as Jim Osborne
* Teresa Wright as Laurie Osborne
* Jonathan Hale as Tom Bowers
* Walter Sande as Customs Inspector
* Eddie Marr as Ken Woodley
* Carleton Young as Briggs, airline clerk

==Background==
This was the second time that Cotten and Wright starred in a film together, following Hitchcocks Shadow of a Doubt (1943), where she played his niece.

Cotten also starred in A Blueprint for Murder (1953), with Jean Peters and Gary Merrill, a thriller noir directed by Andrew L. Stone as well.

==Reception==

===Critical response===
When the film was released, film critic Bosley Crowther, praised the film, writing, The Steel Trap which came to Loews State yesterday, is a straight exercise in the build-up of cold, agonizing suspense ... As a purely contrived generation of runaway anxiety, this little melodrama amounts to a skillful and no-lost-motion job ... Indeed, its an entertaining picture." 

Variety (magazine)|Variety magazine said of the film, "Andrew Stone’s direction of his own story emphasizes suspense that is leavened with welcome chuckles of relief in telling the improbable but entertaining events." 

==References==
 

==External links==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*   essay by Robert Cashill at Cineaste (magazine)|Cineaste
*  

 
 
 
 
 
 
 
 
 
 
 