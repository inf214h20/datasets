What's Cookin' Doc?
{{Infobox Hollywood cartoon
| cartoon_name   = Whats Cookin Doc?
| series         = Merrie Melodies (Bugs Bunny)
| image          = Whats Cookin Doc Lobby Card.PNG
| caption        = Lobby card
| director       = Robert Clampett   Friz Freleng|I. Freleng  (clip from Hiawathas Rabbit Hunt) 
| producer       = Leon Schlesinger
| story_artist   = Michael Sasanoff Bob McKimson   Rod Scribner (uncredited)
| voice_actor    = Mel Blanc  Robert C. Bruce (uncredited)
| musician       = Carl W. Stalling studio  Leon Schlesinger Productions
| distributor    = Warner Bros. Pictures, Inc.
| release_date   = January 8, 1944 (USA)
| color_process  = Technicolor
| runtime        = 8:08 (one reel)
| movie_language = English
}} 1944 Warner cartoon in the Merrie Melodies series, directed by Bob Clampett and starring Bugs Bunny. The title is a variant on Bugs catch-phrase "Whats up Doc?". It also hints at one of the scenes in the picture.

== Synopsis == emcee starting to introduce the Oscar show.

  won the Oscar instead of him]]At this point the film switches to animation, with the shadow of a now-animated emcee (and now voiced by Mel Blanc) continuing to introduce the Oscar, and Bugs (also Mel Blancs voice, as usual) assuring the viewer that "its in da bag; Im a cinch to win". Bugs is stunned when the award goes instead to James Cagney (who had actually won in the previous years ceremony, for Warners Yankee Doodle Dandy). Shock turns to anger as Bugs declares the results to be "Sabotage|sa-bo-TAH-gee" and demands a recount.
 stag reel" (with the title card depicting a grinning stag) starts to roll, and the startled Bugs quickly stops it and switches to the right film.
 A Star Is Born). The emcee asks the audience (in an affected nasal voice), "Shall we give it to him, folks?" and they yell, "Yeah, lets give it to him!" whereupon they shower Bugs with fruits and vegetables (enabling him to briefly do a Carmen Miranda impression)... and an ersatz Oscar labeled "booby prize", which is actually a gold-plated rabbit statue. Bugs is so pleased at winning it, he remarks, "Ill even take youse to bed wit me every night!" The statue suddenly comes alive, asks in a voice like that of radio character, Bert Gordon, "Do you mean it?", smooches the startled bunny, and takes on an effeminate, hip-swiveling pose. The screen fades out, Clampetts famous vocalized "Bay-woop!" is heard, and the "Thats all, Folks!" card appears.

==Analysis==
The subtext of the short is the self-consciousness of Warner Bros. Cartoons about their lack of success at the Academy Awards. The studio had yet to win an Academy Award for Best Animated Short Film. Crafton (1998), p. 116  The clips from Hiawathas Rabbit Hunt (1941) allude to this subtext. It was a former nominee for the award and had lost to Lend a Paw (1941). Crafton (1998), p. 116 
 A Star Chinese Theatre, Trocadero and Cocoanut Grove. Crafton (1998), p. 116 

The premise of the film is that   and his romantic acting by changing into Charles Boyer and romancing a carrot. Crafton (1998), p. 116 
 Snow White and the Seven Dwarfs (1937). Crafton (1998), p. 116  The implication is that awards are not won by the most talented and deserving, but those capable of lobbying. Crafton (1998), p. 116 

The short includes a subtle reference to World War II. There is a newspaper headline announcing the Academy Awards. A sub-headline on the same page reads "Jap   Cruiser Blown Up". This is a reference to the contemporary Imperial Japanese Navy. Shull, Wilt (2004), p. 177 

==Edited versions== Bugs Bunny cartoons to be pulled from Cartoon Networks "June Bugs" 2001 special by order of AOL Time Warner, due to comic stereotyping resulting from the inclusion of the Hiawathas Rabbit Hunt clip. However, this cartoons stereotypes are light compared to the more controversial animated pieces that never made it to air—such as Friz Frelengs Bugs Bunny Nips the Nips and Tex Averys All This and Rabbit Stew—and has aired on "The Bugs and Daffy Show", The Looney Tunes Show and on The Bob Clampett Show. 

==Availability==
The short occurs in its entirety in the documentary   Part 1, which is available as a special feature on Discs 1 and 2 of the  , although it has not been refurbished or released independently in that series. It also appears as a bonus short on Warner Bros. Home Entertainment Academy Awards Animation Collection disc 3.

==Sources==
*  
*  

==References==
 

===Footnotes===
 

==See also==

* Looney Tunes and Merrie Melodies filmography (1940–1949)
* List of Bugs Bunny cartoons
* Mickeys Gala Premiere
* Mickeys Polo Team
* Mother Goose Goes To Hollywood
* The Autograph Hound
* Hollywood Steps Out
* Hollywood Daffy
* Slick Hare

 
{{succession box |
before= Little Red Riding Rabbit | Bugs Bunny Cartoons |
years=1944 |
after= Bugs Bunny and the Three Bears}}
 

 
 
 
 
 
 
 
 