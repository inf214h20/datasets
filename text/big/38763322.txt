Torchy Gets Her Man
 
{{Infobox film
| name           = Torchy Gets Her Man
| image          = 
| alt            = 
| caption        = 
| director       = William Beaudine
| producer       = Jack L. Warner Hal B. Wallis
| writer         = 
| based on       = 
| screenplay     = Albert DeMond Frederick Nebel
| narrator       =  Tom Kennedy Howard Jackson
| cinematography = Warren Lynch Arthur L. Todd
| editing        = Harold McLernon
| studio         = 
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 63 min
| country        = United States English
| budget         = 
| gross          = 
}}
 1938 Drama drama directed by William Beaudine, starring Glenda Farrell and Barton MacLane. This was the sixth of nine in a series of Torchy Blane appeared in the Warner Brothers and First National films.

==Plot summary==
 
In this entry in the "Torchy Blane" series, the young reporter tries to expose a ring of counterfeiters led by a man pretending to be a G-man. 

==Cast==
* Glenda Farrell as Torchy Blane  
* Barton MacLane as Detective Steve McBride   Tom Kennedy as Gahagan  
* Willard Robertson as Charles Gilbert  
* Thomas E. Jackson as Henchman Gloomy (as Tommy Jackson)  
* George Guhl as Desk Sergeant Graves  
* Frank Reicher as The Professor - Henchman  
* John Ridgely as Henchman Bugs  
* Joe Cunningham as Maxie - Star Editor  
* Herbert Rawlinson as Tom Brennan

==References==
 

== External links ==
*  
*  
*  

 

 
 