Nerds 2.0.1
{{Infobox television film
| name           =   Nerds 2.0.1: A Brief History of the Internet
| image          =  
| caption        = 
| genre          = Documentary 
| creator        =
| director       = Stephen Segaller
| producer       = Robert X. Cringely, John Gau, and Stephen Segaller
| writer         = Robert X. Cringely, John Gau, and Stephen Segaller
| screenplay     =
| based on       = 
| narrator       = Robert X. Cringely
| starring       = 
| music          = Michael Bard
| cinematography = Greg Bond, Wendy Revak, Brett Wood
| editing        = Bruce Barrow
| studio         = John Gau Productions for Oregon Public Broadcasting
| budget         = 
| country        = United States
| language       = English
| distributor    = PBS
| network        = Channel 4/PBS
| released       = September 19-October 3, 1998 (Channel 4)
| first_aired    = November 25, 1998 (PBS)
| last_aired     = 
| runtime        = 150 minutes 
| website        = http://www.pbs.org/opb/nerds2.0.1
}}

Nerds 2.0.1: A Brief History of the Internet - a.k.a. Glory of the Geeks - is a 1998 American PBS television documentary that explores the development of the Arpanet, the Internet, and the World Wide Web in from 1969 to 1998. It was created during the Dot-com bubble|dot-com boom of the late 1990s. The documentary was hosted and co-written by Robert X. Cringely (Mark Stephens), and is the sequel to  the 1996 documentary, Triumph of the Nerds. It was first broadcast  as Glory of the Geeks in three weekly episodes between September 19 and October 3, 1998 on Channel 4 in the United Kingdom, and  as Nerds 2.0.1 on consecutive days between November 25, 1998 by PBS in the United States.

==Episodes==
As broadcast by Channel 4/PBS:

*Networking the Nerds (September 19, 1998/November 25, 1998)
*Serving the Suits (September 26, 1998/November 25, 1998)
*Wiring the World (October 3, 1998/November 25, 1998)

==Partial cast==
 
* Marc Andreessen
* Steve Ballmer
* Paul Baran
* Andy Bechtolsheim
* Tim Berners-Lee
* Len Bosack
* Stewart Brand
* Vannevar Bush
* Steve Case
* Vint Cerf
* Steve Crocker
* Will Crowther
* John Doerr
* Larry Ellison
* Doug Engelbart
* Bill Gates
* James Gosling
* Steve Jobs
* Bill Joy
* Bob Kahn
* Vinod Khosla
* Len Kleinrock
* Sandy Lerner
* J.C.R. Licklider
* Drew Major
* John McAfee
* Scott McNealy
* Bob Metcalfe
* Ted Nelson
* Ray Noorda
* Severo Ornstein
* Jonathan Postel
* Howard Rheingold Larry Roberts
* Eric Schmidt
* Jon Shirley
* Ivan Sutherland Bob Taylor
* Larry Tesler
* Ray Tomlinson
* Don Valentine
 

==Funding==
* Viewers Like You
* Alfred P. Sloan Foundation

==Further reading==
* Schillinger, Liesel. " ." Wired (magazine), December 1998.
* Sisario, Ben. " ." New York Times, November 22, 1998.

== External links ==
*  
*  (1998)
*  (1998)
*  (1998)

 
 
 
 
 

 