Funkytown (film)
{{Infobox film
| name           = Funkytown
| image          = Funkytown.jpg
| caption        = 
| director       = Daniel Roby
| producer       = Simon Trottier André Rouleau
| writer         = Steve Galluccio
| screenplay     = 
| story          = 
| based on       =  
| starring       = Patrick Huard Justin Chatwin Sarah Mutch Paul Doucet Raymond Bouchard François Létourneau Romina DUgo 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Caramel Films
| released       =  
| runtime        = 132 minutes
| country        = Canada
| language       = English French CAD $7.2 million
| gross          = 
}}
Funkytown is a 2011 Canadian drama film directed by  , March 4, 2011. 
 1980 independence referendum had fractured and polarized the city, and Montreal had also begun to experience a decade of economic decline. By then, it had ceased to be the largest city in Canada, and had ceased as well to be Canadas financial and industrial centre.   Lederman, Lew. "Some Effects on Investment of the Election in Quebec of the Parti Québécois". Canadian Insights and Perspectives (Association of Universities and Colleges of Canada) April 18, 2002. 
  Stanley Street. It now houses the premium strip club "Chez Parée" and the dance club "La Boom". Kelly, Brendan "Discos stayin alive: Steve Galluccios latest feature, Funkytown, puts Montreal back into The Limelight to celebrate the citys dance-crazy heyday of the 1970s" The Gazette (Montreal) 15 June 2009. pg. A.22 

==Cast==
Raymond Bouchard stars as Gilles Lefebvre, a record producer and impresario who runs the club with his son Daniel (François Létourneau). Paul Doucet stars as Jonathan Aaronson, a flamboyant gay radio, television and fashion personality and trendsetter (a fictionalised version of Douglas Coco Leopold). Patrick Huard stars as Bastien Lavallée, an influential radio and television personality whose dance music shows play a key role in promoting the citys disco scene. Lavallée is a fictionalized version of real-life Montreal radio and television personality Alain Montpetit. 

The cast also includes Geneviève Brouillette (as Mimi, a former Gogo (Quebec music)|Gogo singer now down on her luck), Justin Chatwin (as Tino, a young Italian waiter and disco dancer), Romina DUgo (as Tinos girlfriend), Sophie Cadieux (as Helene, Daniel Lefebvres secretary), and Sarah Mutch (as Adriana, a model who wants to become a disco singer).

==Language== English and Canadian French|French. For French audiences the English dialogue is subtitling|subtitled, while for English audiences the French dialogue is subtitled.
 La Presse accusing it of being essentially an English film with only token dialogue in French, rather than a truly bilingual film. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 