Hallelujah the Hills (film)
Nota Bene: Hallelujah the Hills (the film) has nothing whatsoever to do with a rock group calling themselves Hallelujah The Hills.  The Movie was made in 1963 and the rock group appeared on the scene in 2005.

{{Infobox film
| name = Hallelujah the Hills
| image =  
| image_size =
| alt = 
| caption = 
| director = Adolfas Mekas
| producer = David C. Stone
| writer = Adolfas Mekas
| starring = Peter H. Beard Martin Greenbaum Sheila Finn Peggy Steffans Jerome Raphael Blanche Dee Jerome Hill Taylor Mead Emsh 
 
| music = Meyer Kupferman
| cinematography = Ed Emshwiller
| editing = Adolfas Mekas
| studio = Vermont Productions
| distributor = 
| released = 1963 
| title= Hallelujah the Hills
| runtime = 82 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
 
Hallelujah the Hills (1963) was written, directed and edited by Adolfas Mekas.  The picture was his first feature film.

==Chicago Tribune                        The MOVIES==

“Hallelujah the Hills, the funniest comedy you’ve never seen.......”

“An obscure farce from 1963 comes to the Siskel Film Center for a rare screening on February 6, 2014....” by Nina Metz 
See entire review at AdolfasMekas.com
 

==Plot==
"Two young men, Jack and Leo, are both courting the same girl.  For seven long years they persist, but she finally gives herself to the horrible Gideon.  In a sense, just as this is the pretext for the film, so the courtships of Vera is a pretext for Jack and Leo to camp out together in the Vermont woods near her home, and to indulge themselves in the wildest of horseplay and high jinks.  The film has a Giffithian flavor, a lyrical naivete, which is extremely touching.  At the same time it is full of sophisticated film parodies - Rashomon, the New Wave, Douglas Fairbanks, Ma and Pa Kettle.  In short, this is one of the most completely American films ever made, in its combination of anarchistic wackiness with a nostalgic sense of the lost frontier and (maybe theyre both the same) the magic of youth." -Richard Roud, in the program notes for The First New York Film Festival at Lincoln Center NY, screening September 14, 1963.

In 1963 after screenings in the Cannes Festival Critics’ section, the Montreal Film Festival and the Locarno festival where it won the Silver Sail, HALLELUJAH THE HILLS, Adolfas Mekas’ first feature film made its USA debut at the The First New York Film Festival at Lincoln Center on September 14, 1963, at a 6:30pm screening.  It received rave reviews and went on to a 15-week engagement at the Fifth Avenue Cinema in New York, and movie theatres around the country.  Currently, it is available in 35mm from Anthology Film Archives and the  
Museum of Modern Art, where it is also available in 16mm.

For further availability go to: www.HallelujahEditions.com

==Photos==
 
 
 
 
 
 
 


==Publicity==

In Books

* "Hallelujah les Collines" Fiche Filmographique No. 229 Paris (Special Issue)

* "Hallelujah les Collines" lAvant-Scene No. 64. Novembre 1966 Paris. (Special Issue ‚ screenplay in French)

In Periodicals

* "Girl Tree" (Talk of the Town) The New Yorker, 8 December 1962, New York

* "Hallelujah the Hills" by Harriet R. Polt, Film Quarterly Fall, 1963, Vol. XVII No. 1, Los Angeles

* "Hallelujah the Hills" by Gene Moskowitz, Variety, 15 May 1963, New York

* "Hallelujah the Hills" by Robert Benayoun, La Cinematographie Francaise, 18 mai 1963, Paris

* "Cannes" by Richard Roud, The Guardian, 30 May 1963, London

* "Hallelujah the Hills" by Marcel Martin, Cinema 63 No. 77 Juin 1963, Paris

* "A Funny and Lyrical American Film" by Richard Roud, Manchester Guardian Weekly, 5 June 1963, Paris

* "Hallelujah the Hills La Comica Finale" by Ugo Casiraghi, La Fiera del Cinema, July 1963, Roma

* "Hallelujah: lavant-garde loufoque" by Robert Benayoun, France Observateur, 11/7/63, Paris

* "Hallelujah the Hills" Festival, 23 luglio 1963, Locarno

* "Pomeriggio per il Nuovo Mondo" by Giuseppe Curonici Giornale del Popolo, 24/7/63, Lugano

* "Hallelujah the Hills, Una Spassosa Galoppata nel Regno dell Assurdo," Corriere del Ticino, 25 luglio 1963, Lugano.

* "Poesie et Burlesque" by Francois Rochat, Gazette de Lausanne, 27/28 juillet 1963

* "Hallelujah the Hills" by Michael F. Feiner, The Montreal Star, 5 August 63, Montreal

* "Ein Rueckblick" Neue Z√ºrcher Zeitung Fernausgabe No. 216, Blatt 8., 8/8/63, Zurich

* "Experimentation and Entertainment" by Carol Brightman, Long Island Press, October 1963

* "Adolfo Meko Aleliuja kalnams" by R. Vastokas, Teviskes Ziburiai, 10 October 1963

* "Hallelujah les Collines," Combat, 26/27 Oct 1963, Paris

* "Hallelujah: Les Dingues au Cinema" by Guy Allombert Arts, 30 October 1963, Paris

* "Hallelujah the Hills" Film Culture, Winter 1963/64, New York

* "Une Improvisation loufoque et tendre" by Armand Monjo LHumanite, 11/11/63, Paris

* "Du Burlesque en Libert√©" by Pierre Marcabru Arts, 13 November 1963, Paris

* "Hallelujah les Collines" by Claude Mauriac Le Figaro Littoraire, 14 November 1963, Paris

* "Hallelujah les Collines" by Jeander, Liberation, 14 Nov 1963, Paris

* "Hallelujah les Collines" by Pierre Mazars, Le Figaro, 15 Nov 1963, Paris

* "Jack et Leo" by Claude Tarare, Paris Express, 21 November 1963, Paris

* "Hallelujah the Hills" by Jean dYvoire, Telerama No. 723, 24 November 1963, Paris

* "Hallelujah the Hills" by Andrew Sarris, Village Voice, Dec 1963, New York

* "Meku filmo premjera" Vienybe, 6 Dec 1963, New York

* "Hallelujah the Hills" Daily Telegraph, 12 December 1963, London

* "Where the Hell Are We?" Time, Vol. 82 No. 24, 13 December 1963, New York

* "Lietuviu filmininku laimejimas" Draugas, 14 December 1963, Chicago

* "Hallelujah the Hills directed by Adolfas Mekas" Yale Daily News, 16 December 1963, New Haven

* "Hallelujah the Hills" New York Journal American, 17 December 1963

* "Hallelujah the Hills" The Times, 19 December 1963, London

* "Foofs, Spoofs are Far Out and Big" Life, 20 December 1963, New York

* "Hallelujah the Hills" by Brendan Gill, The New Yorker, 21 December 1963

* "The Art Film" by Penelope Gilliatt The Observer, 22 December 1963, London

* "Hallelujah the Hills" by Ronald Gold Motion Picture Daily, 27 December 1963, New York

* "Une divine idiotie" by Michel Aubriant, Paris-Presse lIntransigeant, 11/13/63, Paris

* "Hallelujah the Hills at 5th Avenue Cinema" by Archer Winsten, NY Post, 12/17/63

* "Hallelujah the Hills Will Take Some Deciphering" Morning Telegram, 12/18/63, New York

* "Adolfas Mekas; Mon film est un chant damiti" La Cinematographic Francaise, 5/20/63, Paris

* "Apie broliu Meku filma Hallelujah the Hills" Ns. Laisve, 12/20/63, New York

* "Situation II Du Cinema Americain" by Jean-Luc Godard, Cahiers Du Cinema. No. 150, 151. Dec 1963- Jan 1964  Paris

* "Hallelujah the Hills" by Paul Breslow Vogue, 15 February 1964, New York

* "Hallelujah! Its Great Fun!" by Richard Christiansen Chicago Daily News, 26 February 1964

* "Screwball Stuff" by Hollis Alpert Saturday Review, 29 February 1964, New York

* "The American Film," by Gudrun Howarth The Seventh Art, Spring 1964

* "Wild, Weird, Wacky Film at Cinema" by John G. Houser, Los Angeles Herald-Examiner, 6 March 1964, Los Angeles

* "Nutty But Good Fun," Boston Traveler, 12 March 1964

* "Hallelujah is Hilarious" by Cynthia Van Hazinga, College News, 19 March 1964, Wellesley

* "Zany New Comedy Spoofs Its Artistic Peers" by Anne Burnett, Northeastern News, 20 March 1964

* "Hallelujah the Hills" by McLaren Harris, Boston Herald, 27 March 1964

* "Hallelujah the Hills" by Robin Beat, Time & Tide. Vol. XI. No. 227, April 1964, London

* "Hallelujah, har filmar USA:s fribytare" by Lasse Bergstrom Expressen, 6 April 1964, Stockholm

* "Vermont Nonsense" San Francisco Chronicle, 22 April 1964

* "Hallelujah the Hills, Acclaimed as Wildest, Weirdest, Wooziest" by Charles Richardson, The Commuter, 23 April 64, Cleveland

* "Hallelujah the Hills Hilarious, Crazy Film" by Kevin Thomas, Los Angeles Times, 3/5/64

* "Hallelujah Gets Good Audience" by James Meade San Diego Union, 12 May 1964

* "Scotia Girl Scores in Art Theatre Film" by Robert Day Times-Union, 20 June 1964, Albany NY

* "Hallelujah the Hills " by Dwight Macdonald Esquire, Vol.LXII No. 1, July 1964

* "Hallelujah the Hills" by E.P. Filmkritik, August 1964, Munchen

* "Halleluja, broder!" Chaplin, Sept. 64, No. 48, Stockholm

* "Movie Lacks a Plot, but its Lots of Fun" by William Leonard, Chicago Tribune, 3/10/64

* "Hallelujah the Hills! All Fun, No Substance" by Peggy Doyle, Boston Record American, 3/12/64

* "Peter Beard Leads Hallelujah Chorus" by Eli Flam, The Patriot Ledger, 3/13/64, Boston

* "Satire on Movies Full of Vitality by James Powers, The Hollywood Reporter, 2/21/64, Los Angeles.

* "A New Wave in Vermont" by Stanley Eichelbaum, San Francisco Examiner, 4/23/64

* "Hill Hidden in Big Fog, Critic Says" by W. Ward Marsh, The Plain Dealer, 4/24/64, Cleveland

* "Broken Images of Hallelujah the Hills" by Godfrey John, Christian Science Monitor, 3/25/64

* "Sound and Fury, Signifying Nothing" by Lois Palken, Tufts Weekly, 3/27/64, Medford MA

* "Hallelujah die Hugel" by Peter M. Ladiges, Filmkritik 1966

* "Films" by Andrew Sarris The Village Voice, 21 April 1966, New York

* "Hallelujah les Collines" by Alain Strambi La Marseillaise, 1 August 1966, Marseille

* "Zany Movie Filmed in South Derry" by Judson Hall, The Brattleboro Daily Reformer, 1/18/68

*  Anthology Film Archives "The Films of Adolfas Mekas" Time Out New York, 20/26 Oct 2011

*  Alternatives and Revivals "Hallelujah The Hills" by Jay Hoberman, Village Voice, 26 Oct/Nov 2011

* "Hallelujah The Hills" by R.B., The New Yorker, 31 October 2011

* "Remembering the Masters" Spotlight, 16th IFFKerala, 9 December 2011

* "New Yawk New Wave" at Film Forum by David Fear TimeOut New York, January 2013 http://www.timeout.com/newyork/film/new-yawk-new-wave-at-film-forum

* "The Joy and Exuberance of Adolfas Mekas: Hallelujah the Hills" by Aaron Cutler The L Magazine, 23 January 2013, NY http://theLmagazine.com/newyork/the-joy-and-exuberance-of-adolfas-mekas-hallelujah-the-hills/Content?oid=2291682

*  Now Playing "Hallelujah the Hills" by R.B., The New Yorker, 28 January 2013

* "Hallelujah the Hills, the Funniest Comedy Youve Never Seen" by Nina Metz, Chicago Closeup, 6 February 2014, The Chicago Tribune

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 