Broadway Danny Rose
{{Infobox film
| name           = Broadway Danny Rose
| image          = Broadway_danny_rose.jpg
| caption        =
| director       = Woody Allen
| producer       = Robert Greenhut
| writer         = Woody Allen
| narrator       =
| starring = {{Plainlist|
* Woody Allen
* Mia Farrow
* Nick Apollo Forte
}}
| music          =
| cinematography = Gordon Willis
| editing        = Susan E. Morse
| distributor    = Orion Pictures Corporation
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = $10,600,497
}}
Broadway Danny Rose is a 1984 American black-and-white comedy film written, directed by and starring Woody Allen. It was screened out of competition at the 1984 Cannes Film Festival.   

== Plot ==
A hapless talent manager named Danny Rose, by helping a client, gets dragged into a love triangle involving the mob. His story is told in flashback, an anecdote shared amongst a group of comedians over lunch at New Yorks Carnegie Deli.

Roses one-man talent agency represents countless untalented entertainers, including washed-up lounge singer Lou Canova (Nick Apollo Forte), whose career is on the rebound.
 Waldorf Astoria, where he will perform in front of Milton Berle, who could potentially hire him for even bigger things.

At the singers insistence, Danny Rose acts as a "Beard (companion)|beard," masquerading as Tinas boyfriend to divert attention from the affair. Tinas ex-boyfriend is extremely jealous, and believing Tinas relationship with Danny to be real, he orders a hit on Danny, who finds himself in danger of losing both his client and his life.

Danny and Tina narrowly escape, as Danny at gunpoint says Tinas real boyfriend is one of Dannys clients who Danny believes is on a cruise for a few weeks. Danny and Tina escape and show up at the Waldorf to find Lou drunk and unprepared to perform. Danny sobers Lou with a unique concoction that he has come up with over the years; Lou sobers up, and gives a command performance. With a new prestigious talent manager in attendance at the performance, Lou, in front of Tina (and with her encouragement), fires Danny and hires the new manager. Danny, feeling cheated, goes to the Carnegie Deli where he hears that the client he ratted on to save himself was beaten up by the hit men and is now in the hospital. Danny goes to the hospital to console his client and pays his hospital bills.

Lou, who has left his wife and kids to marry Tina, becomes a success. Tina, feeling guilty for not sticking up for Danny, is depressed and they eventually split up. It is now Thanksgiving and Danny is hosting a party with all of his clients there. Tina shows up to the door and apologizes, asking Danny to remember his uncle Sidneys motto, "acceptance, forgiveness, and love." At first Danny turns Tina away, but later catches up with her and they appear to make up. During this closing shot, the voiceover of the group of comedians talking about the story is heard. They praise Danny, and say that he was eventually awarded Broadways highest honor: a sandwich at Broadways best-known deli was named after him.

==Cast==
* Woody Allen - Danny Rose
* Mia Farrow - Tina Vitale
* Nick Apollo Forte - Lou Canova
* Sandy Baron - Himself
* Corbett Monica - Himself
* Jackie Gayle - Himself
* Morty Gunty - Himself
* Will Jordan - Himself Howard Storm - Himself
* Gloria Parker - Impresario of the Musical Glasses Jack Rollins - Himself
* Milton Berle - Himself
* Howard Cosell - Himself
* Joe Franklin - Himself
* Gerald Schoenfeld - Sid Bacharach
* Craig Vandenburgh - Ray Webb
* Herb Reynolds - Barney Dunn
* Edwin Bordo - Johnny Rispoli
* Gina DeAngelis - Johnnys mother
* Paul Greco - Vito Rispoli
* Frank Renzulli - Joe Rispoli

==Casting==
Steve Rossi claimed he tested for the role but Allen reneged when he did not want the film to be known as an Allen and Rossi film. 

Allen initially offered the part of the has-been singer Lou Canova to Sylvester Stallone.

==Reception==

===Box office===
Broadway Danny Rose opened on January 27, 1984 on 109 North American screens, grossing $953,794 ($8,750 per screen) in its opening weekend. When it expanded to 613 theatres on February 17, its results were less impressive - $2,083,455 on the weekend ($3,398 per screen). Its total domestic gross was $10,600,497, off an $8 million budget. 

===Critical response===
Broadway Danny Rose received a very positive reception from critics.  It holds a 100% positive "Fresh" rating from critics on Rotten Tomatoes with the consensus, "Woody Allens hard-working, uphill-climbing Broadway talent agent is rendered memorably with equal parts absurdity and affection." 
 Time Out praised the combination of style and substance, stating "The jokes are firmly embedded in plot and characterisation, and the film, shot by Gordon Willis in harsh black-and-white, looks terrific; but what makes it work so well is the unsentimental warmth pervading every frame." 

In October 2013, the film was voted by the Guardian (newspaper)|Guardian readers as the fifth best film directed by Woody Allen. 

==Release==

===Home media=== Twilight Time April 8, 2014. 

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 