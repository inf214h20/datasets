Love and Co
{{Infobox film
| name           = Love and Co
| image          = Amor & Cia.png
| image_size     = 230px
| caption        = Theatrical release poster
| director       = Helvécio Ratton
| producer       = Simone Magalhães Matos
| writer         = Carlos Alberto Ratton
| based on       =  
| starring       = Marco Nanini Patricia Pillar Alexandre Borges
| music          = Tavinho Moura
| cinematography = José Tadeu Ribeiro
| editing        = Diana Vasconcellos
| studio         = Quimera Filmes Rosa Filmes
| distributor    = RioFilme, Severiano Ribeiro   Rosa Filmes  
| released       =  
| runtime        = 100 minutes
| country        = Brazil Portugal
| language       = Portuguese
| budget         = R$3 million 
| gross          = R$237,310 
}}
Love and Co   ( ) is a 1998 Brazilian-Portuguese comedy-drama film directed by Helvécio Ratton. Based on Eça de Queirozs novel Alves & Cia, it stars Marco Nanini, Patricia Pillar and Alexandre Borges. Shot in São João Del Rei, Minas Gerais and set in the 19th century, it follows Alves (Nanini) as he finds his wife Ludovina (Pillar) with Machado (Borges), and challenges him for a gun duel.   

==Cast==
*Marco Nanini as Alves
*Patricia Pillar as Ludovina
*Alexandre Borges as Machado
*Rogério Cardoso as Neto
*Cláudio Mamberti as Carvalho
*Ary França as Medeiros
*Maria Sílvia as Margarida
*Nelson Dantas as Asprígio
*Rui Resende as Abílio
*Sônia Siqueira as Joana

==Reception==
Love and Co grossed R$237,310 and was watched by 47,179 people in the 24 Brazilian theaters in which it was released.    It was nominated for the 1st Grande Prêmio Cinema Brasil for Best Film and Best Actor (Marcos Nanini), but lost in both categories.   It was awarded as the Best Iberoamerican Film at the 14th Mar del Plata Film Festival.  At the 31st Festival de Brasília, it won the Best Film, Best Actress (Patricia Pillar), and shared the Best Art Direction (Clóvis Bueno and Vera Hamburger) with Kenoma.  Marco Nanini won Best Actor while Tavinho Moura won Best Music at the 3rd Brazilian Film Festival of Miami. 

==References==
 

==External links==
* 
*  at Cinemateca Brasileira
*  at Rosa Filmes

 

 
 
 
 
 
 
 
 
 
 