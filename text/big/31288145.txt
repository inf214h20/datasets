Ghost Train (1927 film)
{{Infobox film
| name           = The Ghost Train
| image          =
| caption        =
| director       = Géza von Bolváry Hermann Fellner   Arnold Pressburger   Josef Somlo
| writer         = Arnold Ridley (play)   Adolf Lantz   Benno Vigny
| starring       = Guy Newall Ilse Bois Louis Ralph   Hilde Jennings
| music          = Willy Schmidt-Gentner
| cinematography = Otto Kanturek
| editing        = 
| studio         = Gainsborough Pictures   Phoebus-Film
| distributor    = Woolf & Freedman Film Service
| released       = September 1927 (UK)   29 October 1927 (Germany)
| runtime        = 6,500 feet  
| country        = Germany   United Kingdom 
| awards         = German intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British comedy comedy crime The Ghost UFA and was shot at the latters studio in Berlin.

==Cast==
* Guy Newall - Teddy Deakin
* Ilse Bois - Miss Bourne
* Louis Ralph - Saul Hodgkin
* Hilde Jennings - Peggy Murdock
* John Manners - Charles Murdock
* Sinaida Korolenko - Elsie Winthrop
* Ernő Verebes - Richard Winthrop
* Hertha von Walther - Julia Price

==References==
 

==Bibliography==
* Bergfelder, Tim & Cargnelli, Christian. Destination London: German-speaking emigrés and British cinema, 1925-1950. Berghahn Books, 2008.
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 