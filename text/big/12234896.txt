Anokhi Raat
{{Infobox film
| name           = Anokhi Raat
| image          = anokhiraat.jpg
| caption        =  Asit Sen
| producer       = L.B. Lachman
| writer         = Hrishikesh Mukherjee Anand Kumar Hrishikesh Mukherjee Sanjeev Kumar Zaheeda Hussain Parikshit Sahni
| music          = Salil Choudhary Roshan
| cinematography = Kamal Bose
| editing        = Tarun Dutta
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}} Asit Sen. Sanjeev Kumar, Zaheeda Hussain, Aruna Irani, Keshto Mukherjee and Parikshat Sahni. It has music by Salil Choudhary and Roshan. The film has some outstanding songs that include "Oh re taal mile", "Mahalon ka raja mila", "Mile na phool to", and "Dulhan se tumhara milan hoga". 

==Plot==
The events depicted in the film take place on a single night and results in several characters sharing their life stories. It is a fascinating depiction of some of the challenges faced by the poor and by women in Indian society, some of which continue to this day. 

== Cast == Sanjeev Kumar as Baldev Singh
* Zaheeda Hussain as Rama / Gopa
* Tarun Bose as Madan Lal
* Aruna Irani as Mrs. Prema Rai
* Parikshit Sahni as Painter Anwar Hussain as Ram Das

== Sound Track ==
01. Oh Re Taal Mile Nadi Ke Jal Mein"      Singer- Mukesh      Music Director- Roshan      Lyricist- Indeevar      Actors- Zaheeda, Sanjeev Kumar

02. Mile Na Phul To Kaanton Se Dosti Kar Li"      Mohammed Rafi     Roshan     Indeevar     Sanjeev Kumar, Zaheeda

03. Mahalon Kaa Raajaa Milaa"      Lata Mangeshkar     Roshan     Indeevar     Zaheeda, Tarun Bose, Sanjeev Kumar

04. Dulhan Se Tumhara Milan Hoga"      Mukesh     Roshan     Indeevar     Zaheeda, Sanjeev Kumar

05. Meri Beri Ke Ber Mat Todo"      Asha Bhosle     Roshan     Indeevar


==Awards==
*Filmfare Best Art Direction (black & white)- Ajit Bannerjee
*Filmfare Best Cinematographer Award (black & white) -  Kamal Bose
*Filmfare Best Screenplay Award- Hrishikesh Mukherjee 
*Filmfare Best Dialogue Award-  Anand Kumar 

==References==
 

==External links==

{{External media
|video1=  
}}
*  

 
 
 
 


 