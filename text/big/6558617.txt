Dreams That Money Can Buy
{{Infobox Film
| name           = Dreams That Money Can Buy
| image          = Dreams_that_money_can_buy.jpg
| image_size     = 
| caption        = DVD cover Hans Richter Hans Richter Hans Richter  David Vern
| narrator       = 
| starring       = Jack Bittner Libby Holman Josh White
| music          = Louis Applebaum ("Narcissus") Paul Bowles ("Desire" and "Ballet") John Cage ("Discs") David Diamond ("Circus") Darius Milhaud ("Ruth, Roses and Revolvers") Josh White ("The Girl With the Prefabricated Heart")
| cinematography = Werner Brandes  Arnold S. Eagle Peter Glushanok MeyerRosenblum  Herman Shulman Victor Vicas
| editing        = 
| distributor    = 
| released       = September 1947
| runtime        = 99 mins
| country        =   English
| budget         = $15,000
| preceded_by    = 
| followed_by    = 
}} experimental feature surrealist artist Hans Richter.

The film was produced by Kenneth Macpherson and Peggy Guggenheim. 

Collaborators included Max Ernst, Marcel Duchamp, Man Ray, Alexander Calder, Darius Milhaud and Fernand Léger. The film won the Award for the Best Original Contribution to the Progress of Cinematography at the 1947 Venice Film Festival.

==Plot== neurotic clients. surreal dream sequences in the diegesis is in fact the creation of a contemporary avant-garde and/or surrealist artist, as follows:

:Desire Max Ernst (Director/Writer) 

:The Girl with the Prefabricated Heart Fernand Léger (Director/Writer) 
Song Lyrics John Latouche Sung by Libby Holman and Josh White, accompanied by Norma Cazanjian and Doris Okerson

:Ruth, Roses and Revolvers  Man Ray (Director/Writer) 
Music By Darius Milhaud

:Discs Marcel Duchamp (Writer) 
Music By John Cage

:Circus Alexander Calder (Writer) 
Music By David Diamond

:Ballet Alexander Calder  (Director/Writer) 
Music By Paul Bowles
 Hans Richter (Director/Writer) 
Music By Louis Applebaum
Dialogue by Richard Holback and Hans Richter

Joes waiting room is full within minutes of his first day of operation, "the first installment on the  2 billion clients" according to the male narrator in voiceover, whose voice is the only one we hear in the non-dream sequences.

Case number one is Mr and Mrs A. Mr A is a "methodical, exact" bank clerk. His wife "complains   has a mind like a double entry column; no virtues, no vices". She wants a dream for him "with practical values to widen his horizons, heighten ambitions, maybe a raise in salary". Joe asks Mrs A to leave the room during Mr As consultation. Mr A reveals that within his ledger he has a collection of art images cut from magazines, including  drawings of a woman reclining in bed; another on an old mans lap; another being shot by an animal-headed man; a filmic image of red liquid passing through water, and another of a melting wax figure of a woman.

Joe "finds a dream" for Mr A based on these interests. In the dream ("Desire") leaves fall to the ground beside a red curtain. A woman in white reclines in a red-curtained four-poster bed. A small golden ball rises and falls from her mouth as she breathes. She swallows the ball, smiles and falls asleep. Jail bars appear by her bed, and a voyeuristic man watches from behind them as the woman dreams of nightingales with calves hooves. It appears the man is part of her dream, and telephones her to ask in voiceover for details.  She tells him in voiceover "they talked about love and pleasure". The telephone by her bedside falls to the floor, breaks open and exudes a misty smoke, which envelops her bed. Two young men tumble over each other in what seems like a war scene of shipwrecked sailors who are pulled from underneath the bed by a man dressed as a formal authority figure (Max Ernst).  The voyeur man watching breaks through the jail bars and  enters the bedroom, raising the woman from her bed, and they embrace.  They exit the room, and awkwardly tumble down through a basement corridor wreathed in steam. The authority figure impassively watches them. The voyeur plays dice in the leaves of a dark brick-lined corridor,  The narrating womans voice says "who wants to come with me under my warm white gown," repeaing the last three words several times and closes with the woman tossing the small golden ball into the air and catching it.  

Case number two ("The Girl With the Prefabricated Heart") begins as Joe accepts payment from Mr. A and ends his session, as a young woman wearing a suit, glasses, and a beret enters the room carrying a briefcase, and Mrs. A returns and glares.  The As leave.  The young woman tries to "sign Joe up" for various causes.  In voiceover, she tries to convince Joe while we hear a mans voice telling Joe to resist her.  She flirts, cries, almost leaves, comes back.  The music features a repeated wolf whistle over a light jazz tune.  Joe signs.  He removes her glasses and they almost kiss.  Smoke fills the screen, and clears with a closeup on a dismembered mannequin.  The music is a light tune sung by Holman with the refrain of "untouched by human hands."  It segues into a sequence in which wigged and costumed mannequins seem to dance and pose.  Holman and White sing.  Whirling wheel graphics punctuate the scenes as jewellery is offered to "a healthy girl", Julie, as the male singer/mannequin woos her.  She cries "this is ridiculous! Sisters, come to my aid!" and "so from his ardent arms she fled." The male mannequin is beheaded as Julie rides on an exercise cycle dressed in a bridal gown.  "for theres no man alive who could ever survive a girl with a prefabricated heart."  We transition back to Joes offce as the young woman leaves, then suddenly returns and kisses him, then leaves again.

As she leaves, Mrs. A returns with a garbled, speeded up voiceover expressing her stream of consciousness.   Case number three begins. "Youll find out nothing here - this man is obviously a fake of some kind."  But she wants a treatment...Joe shows the lady a photo of a young smiling couple.  The young woman in the photo reminds Mrs A of herself when she was a carefree girl.  Mrs. A realizes she wants to break the shell around her and has a dream of the same young couple with the man reading a declaration from a book titled “Ruth, Roses and Revolvers.”  The young man is apparently trying to woo the woman “those who have insured themselves against all risks are bound to lose all.”  The couple joins others at a film screening  were the group watches a man on the screen who goes through various poses and gestures.  The audience imitates the man in the film by repeating his gestures.  After the screening  the couple sees the book leaning against a tree.  One woman jokes “all we need now is a revolver.”  The man turns the book around.  On the back cover is a photo of Man Ray, the author,  and we see superimposed a procession of wounded soldiers.

Mrs. A pays in cash and leaves with the photo.  Outside the room there is a chaotic riot and an impotent policeman doing nothing but holding a pose.  In the ensuing commotion a gangster (case number four) has made his way into Joe’s office.  He wants a dream that might help him win the horse races.  The dream consists of spinning disc illusions (by Marcel Duchamp) and a prism distorted version of “Nude Descending a Staircase.”  The gangster is unimpressed and robs Joe at gunpoint.  A policeman enters and asks the gangster for a gun license.  The gangster produces it and the cop lets him go.  Joe is knocked unconscious by the gangster who flees the room.  

Case number five and six - A blind old man and a little girl enter the empty office.  The girl plays with a ball that becomes a collection of Alexander Calder’s mobiles.  A sombre mask watches the playful movements of the mobiles.  Joe regains consciousness and re-enters his office to find the two new inhabitants.  The blind man actually wants to sell a dream rather than buy one.  He makes circus figures out of wire and the figures come to life and perform.  Joe buys the dream.

Case number seven: The next customer doesn’t answer the desk buzzer.  Joe goes to the door and sees that the next customer is himself, standing frozen, surrounded by blocks of ice.  Joe finds a blue poker chip that the little girl left on the floor and he enters his own autobiographical dream.  He is playing poker with his friends around a table being watched over by a classical bust of a bearded man (Morpheus (mythology)|Morpheus?).  As Joe reaches for a liquor glass, it explodes.  In the spilt fluid on the table, he sees his own reflection.  Joe’s skin suddenly turns blue, causing his friends to reject him (“would you want to sit at a table with a blue man?”).  They leave Joe alone in the room where his furniture becomes alive and closes in on him.  He finds a blue cord and follows it (“the blue thread of hope will lead me out of the labyrinth”).  The cord leads outdoors where he is accosted by the public who block his progress (“the right of being in everybody’s way is the right of everybody.”)  The people turn into ladders leading up in four directions.  Joe chooses a ladder and starts to climb.  He remembers the jubilance at the end of the second world war and the prospect of peace and optimism.  Streamers fall from the sky.  Suddenly Joe realizes the rungs are disappearing from his ladder.  He desperately grabs a window sill and pulls himself inside a room.  Dangling colorful circles surround a woman reclining in a hammock.  She offers Joe a drink, a bowl of cherries and a knife.  He kisses the woman and makes a move to slice open her throat but instead cuts the blue cord he’s been following.  The cord bleeds red.  Joe exits the room and re-enters the poker room.  The bearded bust bursts into flames.  He again sees his friends around the poker table but now they’ve become flaming bird cages.  He takes the bearded bust and attempts to escape out his window - hanging by a rope.  The woman he abandoned uses the knife to cut his escape rope.  Joe and the statue fall but become balls of colored ink in water that rains down upon the broken classical bust on the pavement below that appears to be staring upward into the colored patterns.

==Cast==
===Credited Cast===
* Jack Bittner ...  Joe/Narcissus
* Libby Holman
* Josh White
* Norman Cazanjian
* Doris Okerson John La Touche (as John Latouche) ...  The Gangster

* Louis Applebaum ...  Musical Direction
* Ethel Beseda ...  Mrs. A.
* Samuel Cohen ...  Mr. A
* Max Ernst ...  Le President
* Jo Fontaine-Maison ...  The girl
* Bernard Friend ...  Policeman
* Bernard Graves ...  The male voice
* Dorothy Griffith ...
* Evelyn Hausman ...
* Anthony Laterie ...  The blind man
* Julien Levy ...  The man
* Jo Mitchell ...
* Ray Pippitt ...
* Miriam Raeburn ...
* Arthur Seymour ...  The man
* Ruth Sobotka ...  The girl
* Valerie Tite ...  The girl who wants to sign him up
(Cast list source: imdb  

==References==
 

==External links==
*  
*  
* Dreams That Money Can Buy catalog  
* New York Times review of its theatrical run in 1948  

 
 
 
 
 
 
 