Perché uccidi ancora
{{Infobox film
| name           =Perché uccidi ancora
| image          =Perché uccidi ancora.jpg
| image_size     =
| caption        =
| director       =José Antonio de la Loma
| producer       =
| writer         =Gabriel Vincent Davis,  Edoardo Mulargia
| narrator       =
| starring       = Anthony Steffen
| music          = Felice Di Stefano
| cinematography = Hans Burmann	 Vitaliano Natalucci	 	
| editor       = Enzo Alabiso	 
| distributor    =
| released       = 1965
| runtime        = 88 minutes (Italian version)
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian western film adventure directed by José Antonio de la Loma.  

==Plot==
Steven MacDougall quits the army when he receives the message that his father has been gunned down by a member of the Lopez clan. He hurries to protect his kin but the Lopez clan knows that hes coming. Only scarcely he escapes their trap. When hes eventually joined his family, their farm is attacked, yet Steven fights of his enemies another time. Now the fiendish Lopez clan hires a professional assassin from outside.

==Cast==
*Anthony Steffen	... 	Steve McDougall
*Ida Galli	... 	Judy McDougall (as Evelyn Stewart)
*Aldo Berti	... 	Gringo
*Gemma Cuervo	... 	Pilar Gómez (as Jennifer Crowe)
*José Calvo	... 	López (as Josepe Calvo)
*Hugo Blanco	... 	Manuel Lopez
*José Torres	... 	Lopez Henchman
*Franco Latini	... 	Oliveras (as Frank Campbell)
*Ignazio Leone
*Lino Desmond	... 	Gringos brother
*Willi Colombini
*Stelio Candelli		(as Stanley Kent)
*Giovanni Ivan Scratuglia	... 	Lopez Henchman (as Giovanni Ivan)

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 