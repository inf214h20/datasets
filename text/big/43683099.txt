The Game Is Over
{{Infobox film
| title           = The Game Is Over
| image           = 
| caption         = 
| director        = Roger Vadim producer = Mario Sarago Roger Vadim Jean Cau  Roger Vadim Bernard Frechtman
| based on        =  
| starring        = Jane Fonda Michel Piccoli Peter McEnery
| cinematography: Claude Renoir editor = Victoria Mercanton music = Jean Bouchéty, Jean-Pierre Bourtayre
| studio        = Les Films Marceau (Paris) Cocinor - Comptoir cinématographique du Nord Mega Film (Rome) distributor = Royal Films International Columbia (US)
| country         = France Italy
| genre           = Drama
| runtime         = 98 minutes
| released        = 1966 (France) 1967 (USA) gross = 2,558,254 admissions (France) 
}} Italian French language film directed by Roger Vadim and starring Jane Fonda, Michel Piccoli and Peter McEnery. The film is a modern-day adaptation of the 1871-72 novel La Curée by Émile Zola.   

==Synopsis==
In Paris, Maxime Saccard visits his wealthy industrialist father Alexandre and his beautiful young Canadian wife, Renée. Alexandre fathered Maxime years ago in a prior marriage and Maxime has come to stay with them after studying in England.

Renée tells Maxime that she married Alexandre when she was pregnant following an unhappy love affair; the child was stillborn and the passion between the two has faded.

Renée and Maximé begin an affair and fall in love with each other. Renée, who came from a wealthy family, asks Alexandre for a divorce. He agrees, on the condition that she leaves the fortune she brought to their marriage invested in his business. 

Renée accepts this and goes to Switzerland for a divorce. But while she is away, Alexandre confronts his son with two alternatives: he can either run off with the now penniless Renée or become engaged to Anne Sernet, the daughter of a wealthy banker whose support Alexandre wants for his business. Maxime agrees on the second course of action.

Renée returns from Switzerland to find Alexandre holding a ball celebrating Maximes engagement to Anne. Renée throws herself into the pool to kill herself - but then changes her mind and dripping wet enters the party. Alexandre escorts her to the gymnasium, where she sits and stares into an empty future.

== Cast ==
* Jane Fonda: Renée Saccard
* Michel Piccoli: Alexandre Saccard
* Peter McEnery: Maxime Saccard
* Tina Aumont: Anne Sernet  Jacques Monod: M. Sernet
* Howard Vernon: Lawyer
* Douglas Read: Hotel manager
* Ham-Chau Luong: M. Chou 
* Germaine Montero: Guest 
* Joé Davray: The gardener (uncredited)
* Hélène Dieudonné : The maid (uncredited)
* Van Doude: Guest (uncredited)
* Simone Valère: Mme. Sernet (uncredited)
* Dominique Zardi: Guest (uncredited)
==Production==
The movie was one of a series Vadim made based on a classic text. He described the book as "about high society in Paris with a rather serious background, since it likens dogs turning on a deer in a hunt to people." Vadim Is Frank On, Off Screen
Scheuer, Philip K. Los Angeles Times (1923-Current File)   20 July 1965: C8.   

"I am making no attempt to give the wide sociological picture that Zola did," added Vadim. "I am not a naturalist or a moralist. The Zola characters were hardly everyday. There was something fantastic about them, though they have their counterparts today, as I hope to show." And Vadim Created Jane Fonda
By THOMAS QUINN CURTISS. New York Times (1923-Current file)   16 Jan 1966: X15.   

Vadim and Jane Fonda married immediately prior to making the film. Producer Weds Jane Fonda In Ceremony in Las Vegas
New York Times (1923-Current file)   15 Aug 1965: 70.  

It was only the second film performance of Tina Aumont. Hollywood Family--2nd Generation
Los Angeles Times (1923-Current File)   15 Mar 1966: c10.  

The movie was shot in both English and French versions. Fonda acted in French and English. "I said it was hard enough to shoot anything once," she said. "But doing it twice, I found, seemed perfectly natural. 

"I love working in French," she added. "I feel a certain kind of freedom. The way you feel when you learn to speak a foreign language and find you can say things you wouldnt dare say in English." Heres What Happened to Baby Jane
By GERALD JONAS. New York Times (1923-Current file)   22 Jan 1967: 91.  

Peter McEnery did not speak French so when shooting the French version he had to learn his lines phonetically and a French actor was then dubbed in for him. Its the doing she likes: Suggestion of reality
By Kimmis Hendrick. The Christian Science Monitor (1908-Current file)   17 Feb 1967: 4.  

Fonda appears in some nude scenes which were explicit for the time. She said later about shooting these:
 You have to be relaxed, free. Pornography begins when things become self-conscious. But the set was cleared and closed, and I knew Vadim would protect me in the cutting room. Months later we discovered that a photographer had hidden in the rafters and taken pictures which he sold to Playboy. It rocked me, it really did. Its a simple matter of breaking and entering, and invasion of privacy.  

==Reception==
The movie was a solid box office success in France, where it received mostly good reviews. When it screened at the Venice Film Festival, however, critical reception was hostile. A Ball in Venice Competes With Films for Excitement
New York Times (1923-Current file)   09 Sep 1966: 41.  

When the movie was screened commercially in Italy later, all copies were seized on the grounds of obscenity. The Italian producer and 23 cinema owners were charged.  Jane Fonda Film Seized in Italy
New York Times (1923-Current file)   22 Nov 1966: 33.   Producer Indicted in Italy For Obscene Vadim Film
New York Times (1923-Current file)   20 Dec 1966: 58.   Vadim and Fonda were not charged. Roger Vadim, Jane Fonda Face Obscenity Charge
Chicago Tribune (1963-Current file)   26 Apr 1967: b5.  
===US Release===
The Game Is Over received mostly negative reviews in the US. Critics praised the cinematography by Claude Renoir, but called the films story and dialogue trite. New York Times reviewer Bosley Crowther wrote that the film "has absolutely nothing in it but fancy clothes and decor",  while critic Roger Ebert called it "a tedious and ridiculous film of great physical beauty".    The Washington Post called it "this deliciously false and phony picture." Janes Back; So Is Sandra
By Richard L. Coe. The Washington Post, Times Herald (1959-1973)   08 Apr 1967: B7.  

However the Los Angeles Times called it Vadims "best film since Les Liasions Dangereuses and the finest of Miss Fondas career... Rarely has Vadims style been so expressive." Game Is Over and Vadim a Winner
Thomas, Kevin. Los Angeles Times (1923-Current File)   20 Jan 1967: c6.  

==References==
 

== External links ==
*  
*  at TCMDB
 
 
 
 
 
 
 