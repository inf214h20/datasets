Trip to Jewish Cuba
 
{{Infobox film
| name           = Trip to Jewish Cuba
| image          = Triptojewishcubahomeimage-1-.jpg
| image_size     = 200px
| caption        =
| director       = Bonnie Burt
| producer       = Bonnie Burt
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1996
| runtime        = 15 minutes
| country        = United States
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Trip to Jewish Cuba (1996) is an American short documentary film, directed by Bonnie Burt, that follows an United States|American-Jewish aid mission as it travels to Cuba to observe and help the islands Jewish community following a relaxing of the governments policies restricting religion. 

==Summary==
Many Jews immigrated to Cuba in the early 20th century, following social disruption in Europe. By 1924, some 24,000 Jewish immigrants worked in the garment industry in Cuba. In the 1930s and 1940s around the Second World War, Cuba was among the first countries in the Americas to take in European Jews as refugees. Jewish Cuban community had numerous Yiddish newspapers were distributed, and anti-Semitism did not appear during the war years. Following the Communist revolution led by Fidel Castro, more than 90% of Cuban Jewry left the island together with other Cubans of the middle and upper classes.

The revolution frowned on religion in general, making life for practicing believers challenging. By 1989, the Jewish community was reduced to fewer than 800 practicing members.

Trip To Jewish Cuba explores American reactions to the Jewish Cubans they are ostensibly helping. Seeing the Cubans’ struggles makes their accomplishments seem that much more admirable, and causes the Americans to rethink their own sense of commitment. “The feeling inside the synagogue!” an American woman exclaims, noting, “These are people who are proud to be Jewish.” Unlike American Jews who can afford to take their faith for granted, Cuban Jews have to make a strong effort just to keep their tradition alive. Their enthusiasm to learn and their pride in their accomplishments inspires their American visitors.

==Reception==
June Safran, Executive Director, The Cuba-America Jewish Mission:
"Seeing this video is a great way to learn about the Jewish communities of Cuba through the eyes of American visitors of all ages. Especially helpful for those contemplating a trip."

==External links==
*   * 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 