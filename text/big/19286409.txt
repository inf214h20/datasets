Aasai Mugam
{{Infobox film
| name = Aasai Mugam
| image =Aasai Mugam.jpg
| director = P. Pullaiah
| writer = Aarur Das Thuraiyoor K. Murthy
| screenplay = Aarur Das Thuraiyoor K. Murthy
| story = T. N. Balu
| starring = M. G. Ramachandran B. Saroja Devi M. N. Nambiar Nagesh
| producer = P. L. Mohan Ram
| music = S. M. Subbaiah Naidu
| cinematography = P. L. Roy W. R. Subba Rao T. M. Sundar Babu
| editing = C. P. Jambulingam P. K. Krishnan S. R. Das K. R. Krishnan
| studio = Mohan Productions
| distributor = Mohan Productions
| released =  
| runtime = 145 mins
| country = India Tamil
| budget =
}}

Aasai Mugam is a Tamil language film starring M. G. Ramachandran. The film was released in 1965.

==Cast==
* M. G. Ramachandran
* B. Saroja Devi
* M. N. Nambiar
* Nagesh
* K. D. Santhanam
* S. V. Ramadoss
* Geethanjali
* Lakshmi Prabha
* C. K. Saraswathy

==Songs==
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Naaloru Medai
| extra1       = T. M. Soundararajan Vaali
| length1      = 3.23
| title2       = Neeya Illai Naana
| extra2       = T. M. Soundararajan, P. Susheela Vaali
| length2      = 3.23
| title3       = Etthanai Periya
| extra3       = T. M. Soundararajan Vaali
| length3      = 3.42
| title4       = Yeththanai Yeththanai
| extra4       = T. M. Soundararajan Vaali
| length4      = 
| title5       = 
| extra5       =
| lyrics5      = 
| length5      = 
| title6       = 
| extra6       =
| lyrics6      = 
| length6      = 
}}

==References==
 

==External links==
* 

 
 
 
 


 