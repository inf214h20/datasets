My Big Love
{{Infobox Film
| name           = My Big Love
| image          = My Big Love Poster.jpg
| director       = Jade Castro
| producer       = Tess V. Fuentes Kriz Gazmen Charo Santos-Concio Malou N. Santos
| writer         = Henry Quitain Theresa de Guzman Michiko Yamamoto Theodore Boborol	
| starring       = Toni Gonzaga Sam Milby Kristine Hermosa
| music          = 
| cinematography = Ike Avellana
| editing        = Mikael Pestaño
| distributor    = ABS-CBN Film Productions Star Cinema
| released       = 27 February 2008
| runtime        = 110 min
| country        = Philippines
| awards         = 
| language       = Filipino / Tagalog, English
| budget         = 
| gross          = PHP 70 Million
| preceded_by    = 
| followed_by    = 
}}

My Big Love is a 2008 romantic comedy film from Star Cinema starring Sam Milby, Kristine Hermosa and Toni Gonzaga.

==Overview==
Sam Milby stars as Macky, a nice, courteous, well-bred educated culinary chef. He has all the qualities of an ideal man—except that he is overweight. Penchant for food is his constant mindset. Cooking his element, and eating, his pastime!

Now meet the two women that affect his life: Nina (Kristine Hermosa), is the young socialite-columnist who is Macky’s forever crush. To Nina, Macky becomes the secret admirer, showering her with food, flowers, gifts. On the other side of the fence is Aira (Toni Gonzaga) the ever-vibrant fitness instructor. To Aira Macky becomes a knight-in-distress, she motivates him to lose the weight he wants to lose in order to win Nina over.

What happens next is a journey of finding ones self, of discovering the best in each of us, and the unveiling of true love that goes beyond the looks—and weight! 

==Plot==
Pastry chef Macky (Sam Milby) has struggled with his weight for most of his life. It’s not easy being fat, especially when you’re in love with a young socialite columnist named Niña (Kristine Hermosa). He has been her constant secret admirer, surprising Niña every time with cakes and flowers. Everything is going well until Niña meets Macky, who gets the shock of her life when she sees Macky in his 300-pound glory.

Then comes Aira (Toni Gonzaga), the ever-vibrant fitness instructor who knows how to motivate her clients well. Being the breadwinner of the family, she has to double her effort to meet their daily needs. After a number of failed attempts, Macky finally agrees to be her client. In the process of losing weight, the two gained love and affection for each other.

Aira then agrees to accept an offer to work abroad, separating the two. Two years pass and the Macky who was once laughed at is now one of the most sought-after bachelors in town. Unfortunately, his heart is now owned by her ultimate fantasy - Niña. But an untimely encounter with Aira reminds Macky of his happy old days. Macky now has to choose who really is his big love – is it the woman he dreamt of all his life or is it the woman who loved him unconditionally?

==Cast and characters==

===Main cast===
*Toni Gonzaga as Aira
*Sam Milby as Macky
*Kristine Hermosa as Niña

===Supporting cast===
*Ricardo Cepeda as Bert Bugoy Cariño as Johnny Capistrano
*Sandy Andolong as Mama Bing
*Dick Israel as Chef Sen
*Lito Pimentel as Oca
*Malou de Guzman as Zeny
*Janus Del Prado as Lito
*Marissa Sanchez as Honeylet
*Michelle Madrigal as Lyn
*Jaymee Joaquin as Gela
*Kian Kazemi as Pilo
*Aldred Gatchalian as A-Cap
*Fred Payawan as Rene

==References==
 

==External links==
*  

 
 
 
 
 