Liliomfi
{{Infobox film
| name           = Liliomfi
| image          = 
| caption        = 
| director       = Károly Makk
| producer       = 
| writer         = Ede Szigligeti Dezső Mészöly
| starring       = Iván Darvas
| music          = 
| cinematography = István Pásztor
| editing        = Sándor Boronkay
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Liliomfi is a 1954 Hungarian comedy film directed by Károly Makk. It was entered into the 1955 Cannes Film Festival.   

==Cast==
* Iván Darvas - Liliomfi
* Marianne Krencsey - Mariska
* Margit Dajka - Camilla
* Samu Balázs - Szilvay professzor
* Éva Ruttkai - Erzsi
* Imre Soós - Gyuri
* Sándor Pécsi - Szellemfi
* Sándor Tompa - Kányai
* Vera Szemere - Zengőbércziné
* Dezső Garas - Ifjú Schnaps
* Gábor Rajnay -Pejachevich gróf

==References==
 

==External links==
* 

 
 
 
 
 


 
 