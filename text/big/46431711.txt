Tricoche and Cacolet
{{Infobox film
| name = Tricoche and Cacolet
| image =
| image_size =
| caption =
| director = Pierre Colombier
| producer =  Bernard Natan   Emile Natan   Joseph Spigler
| writer =  Ludovic Halevy (play)   Henri Meilhac (play)   René Pujol
| narrator =
| starring = Fernandel   Frédéric Duvallès   Ginette Leclerc
| music = Casimir Oberfeld   
| cinematography = Armand Thirard   
| editing =     Jean Pouzet 
| studio = Les Films Modernes 
| distributor = Les Distributeurs Associés| released =7 September 1938 
| runtime = 107 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Jacques Colombier. detective agency. They are separately hired by a husband and wife to spy on each other with their respective lovers, while a Turkish Prince has fallen in love with both the wife and her husbands mistress. Eventually everything is resolved to everyones satisfaction.

==Cast==
*  Fernandel as Tricoche 
* Frédéric Duvallès as Cacolet
* Ginette Leclerc as Fanny de Saint-Origan dite Fanny Bombance 
* Elvire Popesco as Bernardine Van der Pouf 
* Saturnin Fabre as Monsieur Van der Pouf 
* Monique Montey as Georgette 
* Palmyre Levasseur as La bonne de Tricoche 
* Madeleine Suffel as Virginie - la bonne du bureau de placement 
* Jean Weber as Le duc Émile 
* Sylvio De Pedrelli as Oscar Pacha 
* Jean Gobet as Breloque - le secrétaire 
* Gaston Orbal as Fil-de-fer 
* Rivers Cadet as Naphtaline 
* Alexandre Mihalesco as Le régisseur 
* Albert Malbert as Lacteur 
* Simone Chobillon 
* Manuel Gary 
* Franck Maurice 
* Fernand Trignol

== References ==
 

== Bibliography ==
* Jean-Louis Ginibre, John Lithgow & Barbara Cady. Ladies Or Gentlemen: A Pictorial History of Male Cross-dressing in the Movies. Filipacchi Publishing, 2005. 

== External links ==
*  

 

 
 
 
 
 
 

 