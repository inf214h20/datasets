Oka Laila Kosam
{{Infobox film
| name = Oka Laila Kosam
| image = Oka Laila Kosam.jpg
| caption = Film poster
| writer = Vijay Kumar Konda
| starring = Naga Chaitanya Pooja Hegde
| director = Vijay Kumar Konda
| cinematography = I. Andrew
| producer = Akkineni Nagarjuna
| editing = Praveen Pudi
| studio = Annapurna Studios
| country = India
| released =  
| runtime = 150 minutes
| language = Telugu
| music = Anoop Rubens
| awards =
| budget =     
| gross =   
}}
 Telugu Romance Nagarjuna on his home banner Annapurna Studios, the film features Naga Chaitanya and Pooja Hegde in the lead roles. Anoop Rubens composed music for this film while I. Andrew and Praveen Pudi handled the cinematography and editing departments respectively.  The films talkie part was wrapped up on May 20, 2014.  The film released worldwide on October 17, 2014. 

== Plot == ISB who completes his studies and comes back to India after a world tour. One fine day, he spots Nandana and falls head over heels for her. However Nandana develops hatred for Karthik because of few circumstances. When Karthik proposes to her, she rejects him point blank. Karthiks maternal uncle Pandu returns from Dubai and because of his hatred towards Karthik, he plans to take revenge by letting Karthiks parents arrange a bride for marriage against his wishes. The bride accidentally turns out to be Nandana and Karthik accepts to marry her. She is not interested but she cant oppose her father because of her love towards her father. When she tries to reject the marriage, both of their parents complete the procedures and they are pronounced engaged.

Karthik keeps writing a book titled Oka Laila Kosam which is a collection of his experiences with Nandana. Meanwhile Karthik meets his friend at a pub and comes to know that she is being blackmailed by a goon regarding their private moments. Karthik fights with the goons and his friends and solves the issues. Nandana is unable to get convinced that Karthiks love is sincere and believes that he is a staunch flirt. The book Oka Laila Kosam gets published and Karthik is paid an amount of  25,000. As a part of his attempts to impress her, Karthik gets a star named as Nandana by acquiring a star certificate from International Star Registry with the money he earned. Though initially happy, she still is unable to believe him and tears the certificate into pieces. But she is unable to reject Karthik as she cant let her father disappointed by doing so. That night a drunken Karthik is confronted by the same goon and a fight happen in which the goon is attacked brutally.

On the day of marriage, at the venue, police arrive and arrest Karthik for murdering the goon and thus the marriage is halted much to the delight of Nandana. Before leaving Karthik asks her whether she is happy now at least and leaves with a smile. This puzzles Nandana and it is known later that the case and arrest were fake and were planned by Karthik himself with the help of a rich business man whose daughters successful love affair had Karthik as the peace maker. Nandana meanwhile receives the book Oka Laila Kosam from Shahrukh who credits its writer as the owner of the pigeons she has. She reads the book which is her story and learns Karthik sincerity from the words in the book. She meets Karthik and proposes to him before their respective families and they both unite.

== Cast ==
*Naga Chaitanya as Karthik
*Pooja Hegde as Nandana Ali as Pandu Suman as Karthiks father
*Sayaji Shinde as Nandanas father
*Madhunandan as Karthiks friend Rohini as Nandanas mother
*Sudha as Karthiks mother

== Production ==

=== Development ===
 Vijay and costumes were designed by Neeraja Kona.  In a statement to the Indo-Asian News Service|IANS, Vijay Kumar said that the film would have a healthy humor suitable for family viewing as he felt that not everybody were comfortable with the homosexual jokes in his debut film. 

=== Casting === Ali was selected to play the character of Naga Chaitanyas uncle in the film which was said to be a hilarious role. 

=== Filming ===
The principal photography commenced on December 12, 2013 in Hyderabad and the first schedule was wrapped up nearly on December 26, 2013.  The second schedule began in January 2014 and some crucial scenes were shot in that schedule at Hyderabad.  The second schedule was wrapped up on January 11, 2014.  After a gap due to Akkineni Nageswara Raos death, Naga Chaitanya joined the films shoot in February 2014.  In mid-February 2014, it was reported that the film is in its third and last major schedule and majority of the talkie part was said to be wrapped up with patchwork and songs remained to be shot.  The shooting continued at Annapurna Studios where some crucial scenes on the lead were shot.  At the same time, it was announced that the talkie part of the film would be wrapped up by April 2014 and after shooting the songs in abroad, the films shoot will be wrapped up thus making it a summer release.   

On March 18, Vijay declared that 60% of the films shoot has been wrapped.  In the end of March, a fight sequence was shot on Naga Chaitanya which was choreographed by Ram-Lakshman.  The shoot then continued in locales of Hyderabad in mid-April 2014 where scenes on principal cast were shot.  It was expected then that the principal photography would end in span of a month.  On May 20, 2014 Pooja Hegde informed via Twitter that the films talkie part has been wrapped up and only the songs are left to be shot.  In the first week of June, it was reported that 3 songs will be shot at Switzerland from June 12 to June 25, 2014.  Because of the songs shoot, Chaitanya could not attend the promotional activities of Autonagar Surya prior to the films release.  The filming continued in the second week of July 2014 at Interlaken at Switzerland where a song was shot on the lead pair. Part of the songs shoot included the lead pair paragliding.  On August 2, it was announced that the films shoot is in the final stages.  A week later, the film entered post production phase.   

=== Soundtrack ===
{{Infobox album
| Name           = Oka Laila Kosam
| Longtype       = To Oka Laila Kosam
| Type           = soundtrack
| Artist         = Anoop Rubens
| Cover          = Oka Laila Kosam Audio.jpg
| Released       = August 23, 2014
| Recorded       = 2014 Feature film soundtrack
| Length         = 23:00 Telugu
| Label          = Aditya Music
| Producer       = Anoop Rubens
| Last album     = Manam (soundtrack)|Manam (2014)
| This album     = Oka Laila Kosam (2014)
| Next album     = temper(jr.n.t.r)
| Misc           = {{Singles
| Name           = Oka Laila Kosam
| Type           = soundtrack
| Single 1       = Freedom
| Single 1 date  = August 15, 2014 
| Single 2       = O Meri Janejana
| Single 2 date  = August 17, 2014 
| Single 3       = Oka Laila Kosam
| Single 3 date  = August 17, 2014 
}}
}}

Anoop Rubens composed the films soundtrack and the background score marking his second collaboration with Vijay Kumar Konda. In the second week of August 2014, Vijay revealed that the soundtrack would feature a remixed version of the song Oka Laila Kosam from the film Ramudu Kaadu Krishnudu after which this film is titled.  It was reported in mid-August 2014 that the films soundtrack is scheduled for a release on August 15, 2014 with one of the songs being launched at Hyderabad and two songs being launched at PVP Mall in Vijayawada.  

Later, the single track Freedom sung by   and between 7:00 PM TO 8:00 PM in Red FM 93.5 by Naga Chaitanya, Pooja Hegde and Vijay Kumar Konda while it was known later that the films soundtrack would be marketed by Aditya Music.    The songs O Meri Janejana sung by Javed Ali and written by Sri Mani, Oka Laila Kosam sung by S. P. B. Charan & Divya and written by Dasari Narayana Rao were released at PVP Mall in Vijayawada on happened on August 17, 2014.        

It was reported later that the complete soundtrack featuring 5 songs would be launched on August 22, 2014.  However, it was launched the next day by Aditya Musics streaming section in YouTube.   Reviewing the soundtrack, The Times of India wrote "On the whole, the album looks good with a blend of melodies and fast paced numbers".  The platinum disc function was held on September 20, 2014. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 23:99
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Freedom
| lyrics1         = Vanamali
| extra1          = Anup Rubens, Alfans Jhosuf 
| length1         = 04:25
| title2          = O Cheli Nuvve Na Cheli
| lyrics2         = Phani Chandra
| extra2          = Adnan Sami, Ramya Behara
| length2         = 04:16
| title3          = O Meri Jane Jana
| lyrics3         = Sri Mani
| extra3          = Javed Ali
| length3         = 04:22
| title4          = Oka Laila Kosam
| lyrics4         = Dasari Narayana Rao
| extra4          = S. P. B. Charan, Divya
| length4         = 04:27
| title5          = Teluse Nuvvu Ravani
| lyrics5         = Anantha Sreeram
| extra5          = Ankit Tiwari
| length5         = 03:57
| title6          = Rubens Club Mix (Mash up)
| lyrics6         = -
| extra6          = Anup Rubens
| length6         = 02:47
}}

== Release ==
In mid-February 2014, the makers planned for a summer 2014 release.  On August 2, 2014 the makers said that they are planning to release the film on September 5, 2014.    The films overseas theatrical screening & distribution rights were acquired by Blue Sky Cinemas.  The release date was moved to August 29, 2014 because the post production earlier than expected.  Nagarjuna later postponed the film indefinitely because of other major releases happening at that time. 

The films release date was postponed to October end or early November 2014.  The release date was finalized as October 17, 2014.  There were no changes in the plan in view of the Uttarandhra districts being affected by Cyclone Hudhud. Naga Chaitanya said "I feel sad on seeing the pictures and hearing the news. Once normalcy is restored, we plan to re-release the film in those districts. For now only a few theaters in the affected regions will screen the film." 

=== Marketing ===
On August 2, 2014 the first look poster was launched which featured the lead pair.   The films trailer was released on August 12, 2014.   The trailer registered positive response from the viewers and received 0.1 million views in YouTube in one day of its release.  Naga Chaitanya, Pooja Hegde, Vijay Kumar Konda and Anoop Rubens did a flash mob and entertained the huge crowd who attended the launch of two songs of the soundtrack at PVP Mall in Vijayawada on August 17, 2014 as a part of the films promotion.  However, the event ended on an ugly note as people were seen pushing each other while hooting and cheering for Chaitanya who tried to get closer to him and the situation soon got out of hand as a mini stampede ensued in the mall. The incident forced the police to resort to lathi charge to bring the crowd under control. 
 Ali on October 11, 2014.  As part of the promotional campaign, an event named Vandha Lailala Kosam Naga Chaitanya (Naga Chaitanya for 100 Lailas) was held at Annapurna 7 acres Studios on October 13, 2014. 100 girls and 100 boys all over the state have attended this event for whom special trailers of the film were screened. Anoop Rubens gave a live performance. Pooja Hegdes birthday celebrations were also held on the same stage. 

=== Reception === Oneindia Entertainment wrote "Vijay Kumar Kondas story and screenplay should really be appreciated for portraying the casual story magically, onscreen. Oka Laila Kosam is a good romantic entertainer. The movie includes all commercial elements and will be a hit movie in Naga Chaitanyas career again" and rated it 3 out of 5.  

IndiaGlitz wrote "Barring a few well-crafted scenes and nice visuals, the film lacks a strong story to keep the story going. Once watchable for the taking" and rated it 2.75 out of 5.  Indo-Asian News Service|IANS wrote "At a running time of 150 minutes, you really wish you had the power to stop the film whenever you want. Alas, that option is out of the question .Oka Laila Kosam, which is partly refreshing, is messed up mostly as it suffers from cliched storytelling."  Rajitha S. of The New Indian Express wrote "The film starts on a boring note, but it gets racy until the end of first half. The second half has too many elements which makes it clear that director Vijay Kumar Konda wanted to attribute too many characteristics to Naga Chaitanya. Chaitanya though has done justice to the role, as a character Karthik is slightly overdone". 

=== Home Media ===
The film had its Television premiere on November 23, 2014 on   and Yevadu. 

== Box Office ==
Oka Laila Kosam opened to 65%-75% occupancy in both single screens and multiplexes.  The film collected   on its first day at the global box office.   The collections improved because of strong word of mouth and the film collected   in AP/Nizam region and   nett in the rest of the world by the end of three days taking its global first weekend total to   thus recovering 35% of the films budget.  The film netted approximately   in its first week alone globally and was declared a Box office hit.   

== References ==
 

==External links==
*  

 
 