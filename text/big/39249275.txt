A Most Wanted Man (film)
 
{{Infobox film
| name           = A Most Wanted Man
| image          = A Most Wanted Man Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Anton Corbijn
| producer       = {{Plainlist|
* Stephen Cornwell
* Gail Egan
* Malte Grunert
* Simon Cornwell
* Andrea Calderwood
}}
| screenplay     = Andrew Bovell
| based on       =  
| starring       = {{Plainlist|
* Philip Seymour Hoffman
* Rachel McAdams
* Willem Dafoe
* Robin Wright
* Grigoriy Dobrygin
* Homayoun Ershadi
* Nina Hoss
* Daniel Brühl
|}}
| music          = Herbert Grönemeyer
| cinematography = Benoît Delhomme
| editing        = Claire Simpson
| studio         =  {{Plainlist|
* Demarest Films
* Potboiler Productions
* The Ink Factory
* Film4 Productions
}}
| distributor    = Entertainment One
| released       =  
| runtime        = 119 minutes 
| country        = United Kingdom
| language       = English
| budget         = $15 million 
| gross          = $31.6 million   
}}
 espionage thriller the same 40th Deauville American Film Festival.  It is the last of Hoffmans films released in his lifetime.

==Plot==
Issa Karpov (Grigoriy Dobrygin), a refugee from Chechnya, enters Hamburg, Germany, illegally. Günther Bachmann (Philip Seymour Hoffman), a German espionage agent, leads a team that seeks to develop intelligence from the local Muslim community. The team learns of Karpovs presence from CCTV footage and confirms from Russian intelligence that he is considered to be a potentially dangerous terrorist. Bachmanns team also tracks the activities of a local Muslim philanthropist, Dr. Abdullah (Homayoun Ershadi), who is believed to be funneling funds to terrorist activities, though the team is unable to prove this. German security official Mohr (Rainer Bock) and American diplomatic attaché Sullivan (Robin Wright) both take interest in the two cases.
 laundered money turn Brue and Richter who convince Karpov to donate the funds to Abdullahs organization in the hope that Abdullah will reroute some of the funds to a shipping company acting as a front for al-Qaeda. Bachmann plans to capture Abdullah and turn him as well in order to ensnare those higher up in the terrorist organization. The plan is approved by the interior minister, and Abdullah does indeed route funds to the shipping company, but as Bachmann prepares to take Abdullah into custody, he is ambushed by forces reporting to Mohr and Sullivan who capture Abdullah and Karpov. Bachmann walks away, defeated.

==Cast==
* Philip Seymour Hoffman as Günther Bachmann
* Rachel McAdams as Annabel Richter
* Willem Dafoe as Tommy Brue
* Robin Wright as Martha Sullivan
* Grigoriy Dobrygin as Issa Karpov
* Derya Alabora as Leyla
* Daniel Brühl as Max
* Nina Hoss as Irna Frey
* Herbert Grönemeyer as Michael Axelrod
* Martin Wuttke as Erhardt
* Kostja Ullmann as Rasheed
* Homayoun Ershadi as Dr. Faisal Abdullah
* Mehdi Dehbi as Jamal Abdullah
* Vicky Krieps as Niki
* Rainer Bock as Dieter Mohr
* Charlotte Schwab

==Production== Lionsgate acquired the US distribution rights to the film. 

== Filming ==
Principal photography took place in Hamburg, Germany in September 2012. 

== Marketing ==
On 11 April 2014, the first trailer for the film was released.  A new trailer for the UK was revealed on 30 June. 

== Release ==
On 25 July 2014, the film received a limited release in the United States, beginning with 361 theatres and later expanding wider. It has earned US$31,554,855 worldwide. 

==Reception==
A Most Wanted Man received positive reviews and has a "Certified Fresh" score of 88% on  , the film has a score of 73 out of 100, based on 42 critics, indicating "generally favorable reviews". Richard Roeper called the film one of the best spy thrillers in recent years, and called it the seventh best film of 2014. 

Many critics praised Hoffmans performance, which was his last leading role before his death in February 2014.  Critic Kenneth Turan of the Los Angeles Times called it a "crackerjack thriller" and praised the performance of the entire cast but Hoffman in particular. He wrote that A Most Wanted Man is "a fitting film for him to leave on, not only because it is so expertly done but because his role was so challenging." 

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 