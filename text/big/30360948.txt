Chillerama
{{Infobox film
| name           = Chillerama
| image          = Chillerama_poster.JPG
| alt            = 
| caption        = Poster Tim Sullivan Adam Green Joe Lynch
| producer       = Jason Richard Miller Andrew Mysko Cory Neal
| writer         = Adam Rifkin Tim Sullivan Adam Green Joe Lynch
| starring       = see Cast sections Crew section
| editing        = see Crew section
| cinematography = see Crew section
| studio         = ArieScope Pictures Dino Dela Rocha
| distributor    = Image Entertainment
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Chillerama is a 2011 horror comedy anthology film consisting of four stories (or segments) that take place at a drive-in theater playing monster movies. Each segment is a homage to a different genre and style. 
 Tim Sullivan The Twilight Adam Green Joe Lynch. Tying each segment of the anthology together is a framing story: a worker for the theater, in a drunken state, digs up his deceased wifes body and attempts oral sex on it, only for her to turn into a zombie and bite his genitals, causing him to slowly turn into a zombie between segments as he is working.

Filming took place in late 2010 and was release at Fantasy Filmfest on August 22, 2011.  On September 29, 2011 it was released to video on demand and on DVD and Blu-ray Disc|Blu-ray on November 29, 2011.

==Origin== KISS frontman Gene Simmons,  but reality television was beginning to dominate American airwaves, so the project was shelved. 
 Adam Green Joe Lynch at Rainbow Bar and Grill,   and the idea resurfaced.  Soon the quartet decided to make Chillerama as an independently produced film, with Greens studio, Ariescope Pictures, serving as the headquarters of operations.  Due to estimated budgetary constraints, Werewolf of Alcatraz was dropped and replaced with Wadzilla;  I Was a Teenage Vampire was changed to Teenage Werebear at Lynchs behest;  Zombie Drive-In became Zom-B-Movie; and a fifth short called Deathication was added to the drive-in sequence to fake out viewers.

=="Wadzilla"==

===Premise===

"Wadzilla" is a spoof on 1950s monster movies and is about "a guy that goes to get his sperm count raised, and it creates one big sperm that attacks New York City". 

===Cast===

*Adam Rifkin as Miles Munson
*Sarah Mutch as Louise
*Owen Benjamin as Larry
*Ray Wise as Dr. Weems
*Eric Roberts as General Bukkake
*Miles Dougal as Hobo
*Lin Shaye as Baglady

===Production===

It was directed and written by Adam Rifkin, who also stars in the film.   Special effects was done by The Chiodo Brothers. 

=="I Was a Teenage Werebear"==

===Premise===
 The Twilight Saga.  It is set in 1962 and is about a "closeted kid who meets these other closeted kids, who when aroused turn into leather daddy werebears". 

===Cast===
 Sean Paul Lockhart as Ricky
*Anton Troy as Talon
*Gabby West as Peggy Lou
*Adam Robitel as Butch
*Lin Shaye as Nurse Maleva
*Ron Jeremy  as Playbear
*Tim Sullivan as Coach Tuffman
* Thomas C. Colby-Dog as the Head Wearbear

===Production===
 Tim Sullivan. bear is slang for "big hairy burly men" in the gay community.   It includes five original rock n roll songs performed by the actors. 
 Sean Paul Lockhart (Brent Corrigan) was cast as the lead role.   On November 5, 2010, a scene was filmed on the beach of Sycamore Cove near Malibu, California|Malibu.  Robert Pendergraft provided the practical effects. 

===Soundtrack===

{{Infobox album |  
| Name        = Chillerama Presents: Tim Sullivans I Was a Teenage Werebear 
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Caption     = 
| Alt         = 
| Released    = 2011
| Recorded    = 
| Genre       = Soundtrack
| Length      = 43:21
| Label       = BSX Records
| Producer    = 
| Last album  = 
| This album  =
| Next album  = 
}}

{{Track listing
| collapsed       =
| headline        = Tim Sullivan & Patrick Copeland except where otherwise noted
| total_length    = 43:21
| title1          = Chillerama
| length1         = 3:53
| note1           = written and performed by Psycho Charger
| title2          = Dont Look Away 
| length2         = 2:12 Sean Paul Lockhart
| title3          = Purge 
| length3         = 1:48 Sean Paul Lockhart
| title4          = Love Bit Me on the Ass
| length4         = 1:44 Sean Paul Lockhart
| title5          = Do the Werebear (And Let the Werebear Do You)
| length5         = 2:57
| note4           = performed by Anton Troy, Tom Colby & Chris Staviski 
| title6          = Room for All
| length6         = 1:57 Sean Paul Lockhart
| title7          = Where Were You When I Was 17?
| length7         = 2:50
| note7           = written and performed by Bobby Vinton
| title8          = Undercover Lover
| length8         = 3:30
| note8           = performed by Briana Nadeau; written by Briana Nadeau & Adam Williams
| title9          = Sexy Ways
| length9         = 3:12
| note9           = written and performed by Robert Vinton
| title10         = Im Gonna Make Him Mine
| length10        = 2:49
| note10           = performed by Briana Nadeau; written by Briana Nadeau & Ryan Jennings
| title11         = I Was a Teenage Werebear Instrumental Suite
| length11        = 11:36
| title12         = Chillerama Drive In – Freak Out Remix
| length12        = 4:19
| note12          = Written and performed by Psycho Charger
}}

===eBay Controversy===
 Tim Sullivan CD soundtrack Sean Paul Lockhart clad in a pair of red briefs.  After a few of the auctions had ended, eBay suddenly pulled down the listing and canceled all finalized orders of the album and 8x10.  Initially Sullivan thought it was a mistake, but when he spoke to people in eBay’s safety & trust department, he was told that the auction included a photo of a man with "engorged genitalia"  which they deemed "sexually and morally offensive."    The exact same photo graced the cover of the May 2011 issue of Odyssey magazine which has sold on eBay without incident,  and thus led Sullivan to believe he was being discriminated against because his auctions accompanying text emphasized the gay content of the production.   Sullivan was initially told that he could re-list the auction, but only in the adults-only section of the site that features pornographic material.  After two failed appeals to reinstate the auction in the regular site, eBay told him that he couldnt relist it at all, even without the offending photo.   Rather than wasting more time, energy and risk further jeopardizing his eBay seller feedback rating, Sullivan decided to give up and just sell the album and 8x10 directly from his Facebook page, instructing buyers to send him an email with the header: "Fuck yes, I wanna be sexually and morally offended!" 

===Stand-Alone DVD===

On September 20, 2012, writer/director Tim Sullivan began selling a stand-alone DVD of his segment, I Was a Teenage Werebear through his Facebook page. 
  Titled Uncut & Hairy Ultimate Edition, this version is limited to 2500 copies and includes an extended version of the short with 5.1 audio and color correction, as well as assortment of extras which were not included on the Chillerama DVD.

===Stage Musical===

Soon after Teenage Werebear was released, Tim Sullivan was approached by Sean Abley,  a producer and filmmaker who had previously adapted cult films such as Santa Claus Conquers the Martians and Reefer Madness to the stage, with the proposition of turning the short film into a full-length stage musical.  With Abley set to direct, Sullivan began writing additional songs and retained several cast and crew members, including Sean Paul Lockhart, Anton Troy, Tom Colby and producer Adam Rifkin for a production which was originally announced to hit the stage in September 2012,   though it didnt come to fruition by the projected date.

=="The Diary of Anne Frankenstein"==

===Premise===

"The Diary of Anne Frankenstein" is a black and white film about Hitler "trying to create the perfect killing machine to win the war". 

===Cast===
 Joel David Moore as Adolf Hitler 
*Kristina Klebe as Eva Braun 
*Kane Hodder as Meshugannah  Jim Ward as Anne Franks Father

===Production===
 Adam Green came into Chillerama with the title of this short already established, but he was encouraged to make it his own.  Greens first decision, given the delicate nature of the subject matter, was that the story had to be over-the-top and would have to "make a clown out of Adolf Hitler|Hitler"  to ensure no one would deem it offensive.  His second decision was casting Joel Moore in that role.  "If Joel Moore is Hitler," Green said, "there is absolutely no way you can take this seriously for a second." 

Green then decided to hire a cast of German-speaking actors and, to make Hitler look like even more of a fool, have Moore speak gibberish throughout the film.  Greens idea was to have Moore sound slightly convincing as the film began to give audiences who dont comprehend German the idea that he was actually speaking the language, but as the film progresses "his German gets worse and worse,"  ultimately devolving into total gibberish and random words and phrases, such as "Oshkosh Bgosh" and "Boba Fett."   Actress Kristina Klebe and her mother translated Greens script into German,  and the cast rehearsed it two ways: first in English to get the rhythms down, and then in German as it would be shot.  For a few key moments, Moore was taught to speak German phrases, albeit not the ones that are subtitled on the screen.  For example, during his song "I Dont Want to Rule the World," instead of the titular line Moore sings, "Ich habe würmer in meinem schwanz," which means, "I have worms in my penis." 

=="Zom-B-Movie"==

===Premise===
 Joe Lynch. 

===Cast===

*Richard Riehle as Cecil Kaufman
*Corey Jones as Tobe
*Kaili Thorne as Mayna
*Brendan McCreary as Ryan Miller
*Ward Roberts as Miller

===Soundtrack===

{{Infobox album |  
| Name        = Chillerama Presents Zom-B-Movie 
| Type        = Soundtrack
| Artist      = Bear McCreary, Young Beautiful in a Hurry, Joshua Silverstein
| Cover       =
| Caption     = 
| Alt         = 
| Released    = 2011
| Recorded    = 
| Genre       = Soundtrack
| Length      = 60:48
| Label       = BSX Records
| Producer    = 
| Last album  = 
| This album  =
| Next album  = 
}}

{{Track listing
| collapsed       =
| headline        =
| all_writing     = Bear McCreary except where otherwise noted
| total_length    = 60:48
| title1          = I Dont Want to Die a Virgin 
| length1         = 4:34
| note1             = performed by Young Beautiful in a Hurry; written by Brendan McCreary
| title2          = Chillerama Main Title / Floyds Bean Bag 
| length2         = 3:06
| title3          = Ryan to the Rescue 
| length3         = 2:33
| title4          = Fernando Phagabeefy
| length4         = 3:07
| title5          = Deathication
| length5         = 3:07
| title6          = The Kiss
| length6         = 0:57
| title7          = Cecil and Orson
| length7         = 1:48
| title8          = Tainted Popcorn
| length8         = 2:09
| title9          = The Final Reel
| length9         = 2:38
| title10         = Rosebud Motherfucker
| length10        = 4:00
| title11         = One Hell of a Show
| length11        = 1:41
| title12         = Fugue in Z Minor
| length12        = 2:58
| title13         = Rosemary’s Picnic Table
| length13        = 3:05
| title14         = Seducing Ryan 
| length14        = 0:34
| title15         = Zom-B-Movie Suite 
| length15        = 4:48
| title16         = Deathication (Movement Number Two) 
| length16        = 19:06
| note16           = Lyrics by Raya Yarbrough; featuring Joshua Silverstein, vocals
}}

==Crew==
{| class="wikitable" width=99% border="1"
!align="center" | Segment
!align="center" | Cinematographer
!align="center" | Editor
!align="center" | Composer
|-
!"Wadzilla"
|rowspan="4" align="center" |  Will Barratt 
|rowspan="2" align="center" |  Ed Mark 
|rowspan="2" align="center" | Andy Garfield
|-
!"The Diary of Anne Frankenstein"
|-
! "I Was a Teenage Werebear"
|align="center"  | Gavin Heffernan
|align="center" | Patrick Copeland
|-
! "Zom-B-Movie"
|align="center"  | Matt Brulotte
|align="center" | Bear McCreary
|-
|}

==Release and reception==
Chillerama was distributed by Image Entertainment.  The segment "The Diary of Anne Frankenstein" was shown at the 2010 London FrightFest Film Festival to a positive response from the audience.  It was shown at Fantasy Filmfest in Germany on August 22, 2011.  The film was released in various major cities in the United States from September 15, 2011 to October 29, 2011.  It was released on video on demand on September 29, 2011  and on DVD and Blu-ray Disc|Blu-ray on November 29, 2011. 
 Darker magazine that this movie has a great value for the horror genre (though he mentioned that Chillerama could be adequately appreciated only by die-hard horror fans). 

Two soundtrack albums have been released for the film. Chillerama Presents Zom-B-Movie was released on November 16, 2011 through BSX Records.   This album includes Bear McCrearys complete score, as well as I Dont Want to Die a Virgin by star Brendan McCrearys band Young Beautiful in a Hurry.  Seven songs featured in Zom-B-Movie were omitted from the album.

Chillerama Presents: Tim Sullivans I Was a Teenage Werebear was released on February 14, 2012 through BSX Records.   This album includes all of the songs from the segment, as well as Room For All (Everybodys Gay), a production number which was cut out of the version of the film included in Chillerama, and two versions of the titular Chillerama song by Psycho Charger.

Currently Andy Garfields music for Wadzilla and The Diary of Anne Frankenstein is not commercially available.

Writer/director Tim Sullivan began selling a limited number of DVD-Rs of the "Uncut and Hairy" version of I Was a Teenage Werebear on September 20, 2012. 

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

 Drive-In Jivin: Adam Rifkin and Tim Sullivan on Chillerama, Phantom of the Movies Videoscope,, Vol. 20, No. 83 Summer 2012 pp. 48-49 

   

   

}}

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 