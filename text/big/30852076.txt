Susannah of the Mounties (film)
{{Infobox film
| name           = Susannah of the Mounties
| image          = Susannah_of_the_Mounties_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Walter Lang
* William A. Seiter
}}
| producer       = Kenneth Macgowan
| screenplay     = {{Plainlist|
* Robert Ellis
* Helen Logan
}}
| story          = {{Plainlist|
* Fidel LaBarba
* Walter Ferris
}}
| based on       =  
| starring       = {{Plainlist|
* Shirley Temple
* Randolph Scott
* Margaret Lockwood
}}
| music          = 
| cinematography = Arthur C. Miller
| editing        = Robert Bischoff
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Susannah of the Mounties is a 1939 American drama film directed by Walter Lang and William A. Seiter and starring Shirley Temple, Randolph Scott, and Margaret Lockwood.    Based on the 1936 novel Susannah of the Mounties by Muriel Denison, the film is about an orphaned survivor of an Indian attack in the Canadian West who is taken in by a Mountie and his girlfriend. Following additional Indian attacks, the Mountie is saved from the stake by the young girls intervention with the Indian chief.

==Plot==
As the Canadian Pacific Railway makes its way through the western frontier of Canada in the early 1880s, railroad workers and settlers come under frequent attack by Indians who resent the white mans encroachment on their land. One such attack on a wagon train leaves only one survivor of the Indian massacre, a young girl named Susannah Sheldon (Shirley Temple) who is found by a mounted patrol in the command of Inspector Angus "Monty" Montague. The young girl is taken back to the post where she is adopted by Monty (Randolph Scott) and his friend, Pat OHannegan (J. Farrell MacDonald). The two men do their best to help the girl overcome her terrible ordeal.

Sometime later, Vicky Standing (Margaret Lockwood), the daughter of the Superintendent (Moroni Olsen), arrives from Toronto to visit her father. Monty is immediately enchanted by the beautiful woman. The blossoming romance sparks a rivalry in Susannah and Harlan Chambers (Lester Matthews), the head of the railroad camp. The Indian attacks resume when a band of renegades steals horses from the railroad camp. One of the friendly Indians, Chief Big Eagle (Maurice Moscovitch), promises to track down the renegades and delivery them to the camp. As a show of good faith, the Chief leaves his son Little Chief (Martin Good Rider) at the post.

Little Chief is soon teaching Susannah the way of the Indians. While the two are out riding one day, they come upon one of the renegades, Wolf Pelt (Victor Jory), attempting to sell his stolen horses to Chambers. The two argue and Chambers threatens the Indians with extinction. Wolf Pelt returns to his tribe and uses Chambers threats to demand that the tribe go to war against the white man. That night, Wolf Pelt raids the post to retrieve Little Chief and kidnaps Monty. Soon after, Big Eagle sends a message demanding that the railroad abandon the area or they will kill Monty. 

Susannah decides to search for Monty, and as she approaches the Indian camp she is taken prisoner by the Indians. As the tribe prepares to burn Monty at the stake, Susannah escapes from her teepee and makes an appeal to Big Chief, accusing Wolf Pelt of inciting Chambers by stealing his horses. Wolf Pelt denies the charges. To determine who is telling the truth, Big Chief uses the stick of truth that will point to the liar. When the stick drops towards Wolf Pelt, Big Chief frees Monty and offers him and Susannah his pipe of peace.

==Cast==
* Shirley Temple as Susannah Sheldon
* Randolph Scott as Inspector Angus "Monty" Montague
* Margaret Lockwood as Vicky Standing
* Martin Good Rider as Little Chief
* J. Farrell MacDonald as Pat OHannegan
* Maurice Moscovitch as Chief Big Eagle
* Moroni Olsen as Supt. Andrew Standing
* Victor Jory as Wolf Pelt
* Lester Matthews as Harlan Chambers
* Leyland Hodgson as Randall
* Herbert Evans as Doctor
* Jack Luden as Williams
* Charles Irwin as Sergeant MacGregor John Sutton as Corporal Piggott   

==Production==
In the movie there was a contingent of 12 full blooded Blackfoot Indians led by Chief Albert Mad Plume, who were brought in largely as extras. Another member of the Blackfoot tribe, Martin Goodrider, played the role of Little Chief. Temple and Goodrider struck up an instant friendship (something unusual with Temple as she was normally forbidden from mingling with her child costars). As an act of good will, Temple swore in all members of the Blackfoot tribe as members of the Shirley Temple Police Force while Temple was made an honorary member of the Blackfoot tribe and given the name Bright Shining Star. 

==Soundtrack==
* "In the Gloaming" (Annie Fortescue Harrison, Meta Orred) sung a cappella by soldiers in Supt. Standings home
* "Ill Teach You to Waltz" (M.F. Carrey) played on harmonica by J. Farrell MacDonald and sung by Shirley Temple
* "Down Went McGinty" (Joseph Flynn) sung a cappella by J. Farrell MacDonald   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 