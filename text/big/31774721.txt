Pleins feux sur Stanislas
{{Infobox film
| name           = Pleins feux sur Stanislas
| image          = Pleins feux sur Stanislas.jpg
| image size     = 200
| caption        =
| director       = Jean-Charles Dudrumet
| producer       = Caro, CICC
| writer         = Michel Cousin Jean-Charles Dudrumet
| starring       = Jean Marais
| music          = Georges Delerue
| cinematography =  Pierre Gueguen
| editing        = Armand Psenny Jeannine Verneau
| distributor    =
| released       = 23 September 1965 (France)
| runtime        = 93 minutes
| country        = France, West Germany
| awards         =
| language       = French
| budget         =
}}
 comedy thriller film from 1965. It was directed by Jean-Charles Dudrumet, written by Michel Cousin and Jean-Charles Dudrumet, starring Jean Marais.  The film was known under the title Killer Spy (USA),   (Portugal),   (West Germany).   

It was a sequel of Lhonorable Stanislas, agent secret from 1963.

== Cast ==
* Jean Marais: Stanislas Dubois, the secret agent, writer
* Nadja Tiller: Bénédicte Rameau, literature critic
* André Luguet: the colonel of Sailly, the leader
* Bernadette Lafont: Rosine Lenoble, the fiancée of Vladimir
* Rudolf Forster: Rameau, the father of Bénédicte (under the name of "Rudolph Forster")
* Nicole Maurey: Claire, the chairwoman of the association
* Yvonne Clech: the hotel keeper
* Marcelle Arnold: Morin, secretary of Stanislas Jacques Morel: the tax inspector of Stanislas
* Bernard Lajarrige: Paul, the butler of Stanislas
* Edward Meeks: James, the English spy
* Billy Kearns: American spy
* Clément Harari: Soviet spy
* Henri Tisot: Agent 07 at the telephone
* Pierre Tchernia: the TV presenter
* Max Montavon: the barman of the dining car
* Edmond Tamiz: Nikita / Vladimir, the murdered brothers
* Charles Régnier: the man with the cat (uncredited)

== References ==
 

== External links ==
*  
*   at Films de France
 

 
 
 
 
 
 
 
 
 

 