Prime Time (2015 film)
 
 
 
 
{{multiple issues|
 
 
 
}}
{{Infobox film
| name            = Prime Time 
| film name       = 
| image           = Official Poster.jpg
| caption         = Official Poster
| director        = Promod Kashyap 
| producer        = Himanshu Kesari Patil 
| writer          = Shubhrojyoti Guha 
| starring        = Sulekha Talwalkar  Milind Shintre  Kritika Deo 
| music           = Niranjan Pedgaonkar
| release        =      
| language        = Marathi
| country         = India
| Music label     = Zee Music Marathi 
}}

Prime Time is an upcoming Marathi movie which tells the story of Vaishali Apte, her small Maharashtrian family and her bizarre obsession with the world of television.  Her obsession leads the family through a roller coaster of adventures where ultimately they realize the true value of their relationships. Prime Time is a lighthearted comedy. The movie slated to release on 29 May,2015.     

==Cast==
* Sulekha Talwalkar as Vaishali Shantaram  Apte
* Milind Shintre as Shantaram Apte
* Kritika Deo as Kavya Shantaram  Apte
* Kishore Pradhan as Purshottam Apte
* Nisha Parulekar as Manju Khandekar
* Swayam Jadhav as Raghu Shantaram Apte
* Anurag Worlikar as Niranjan aka Batuk
* Gayatri Deshmukh as Kamini Shetty
* Jayant Gadekar as Popatrao

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 