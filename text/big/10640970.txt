Viduthalai
{{Infobox film|
| name = Viduthalai
| image = Viduthalai.jpg
| caption = Official poster
| director = K. Vijayan
| writer = Vishnuvardhan R.N.Sudarshan Madhavi Anuratha Shalini
| producer = Suresh Balaje Chandrabose
| editor =
| released = 8 july 1986
| runtime = Tamil
| budget =
}}
 Vishnuvardhan and Madhavi (actress)|Madhavi.
 Qurbani

==Plot==
The combination of Super Star Rajini and Kannada Superstar Dr. Vishnuvardhan came together for this movie. Raja (Rajinikanth|RajiniKanth) is a thief- expert in breaking open treasuries. In one such robbery, he is being watched by a jolly but shrewd police inspector Rajasingam (Sivaji Ganesan). Radha (Madhavi) is a gorgeous disco club dancer & singer. Both Raja and Radha are in love with each other. But Raja has not disclosed to Radha that he is a thief. An evil brother-sister duo- seek revenge against crime boss Sudarshan who cheated Jwaala and siphoned her money. Therefore they hire Raja to break open Sudarshans treasury and steal back the money. Raja is paid a token amount to do the job. But suddenly Inspector Rajasingam arrests Raja and the court sentences Raja to 2 years imprisonment. Radha is devastated after she realizes Raja was a thief. Meanwhile, Amar (Vishnuvardhan) is an ace crime member in Sudarshans gang who revolts against Sudarshan. He is a widower with a daughter is studying in a boarding school. However before quitting Sudarshans gang, Amar has committed a crime wearing a mask and Inspector Rajasingam is investigating that case. Once Amar saves Radha from a gang of rowdy bikers. They meet regularly as Radha likes Amars daughter. Soon Amar begins to love Radha who does not reciprocate because she still loves Raja. Amar realizes Radha is not interested and does not proceed. Raja completes his jail sentence. While returning, he meets the Vijaykumar, who again reminds him of the deal to rob Sudarshan. During the conversation, Amar incidentally reaches the site and a fist fight ensues between Amar and the Vijaykumar. While fleeing, Vijaykumar swears revenge against Amar. Thus Raja and Amar meet for the first time. Raja takes Amar to introduce to Radha but both Radha and Amar pretend as if they do not know each other since they dont want Raja to unnecessarily suspect them.
Later Vijaykumars goons kidnap Amars daughter and beat Amar who is hospitalized. In return for Amar and his daughters safety, Raja agrees to do Vijaykumars job. He nurses Amar back to normalcy and soon they turn thick friends. Amar promises Raja he will support him in this one last robbery. They plan to shift to London after the robbery with the money. They concoct a scheme whereby Amar would steal gold bars and jewellery from a safe, then phone the police, let Raja take over, get arrested, get a prison term for about 12 to 18 months. After his release, he will join Amar in the U.K. Things dont go according to plan as Raja gets arrested for killing Sudarshan while Amar and Radha reach London with the money. Raja construes that Amar deliberately framed him so that he can get Raja out of the way, keep all the money (as well as Radha) for himself. Raja escapes from jail and reaches London to apprehend Amar. After a brief tussle between the two, Raja realizes the truth and that Amar did not frame him. Vijaykumar and his goons reach London to take revenge against Raja and Amar. In the climax of the movie, Amar sacrifices his life to save Raja from getting killed by Vijaykumar.

==Cast==
* Sivaji Ganesan as Inspector Rajasingam
* Rajinikanth as Raja Vishnuvardhan as Amar Madhavi as Radha Vijayakumar as Vikram

==Production==
Balaji who saw Hindi film Qurbani decided to remake it in Tamil. He wanted Rajini to play Feroz Khans character and Sivaji to play Amjad Khans character. Sivaji agreed to play the role. The dialogues for the film was written by Aaroor Das. 

==Release==
Cinemalead wrote: "Viduthalai was another big let down despite big star cast   the soul and required punch from the original Qurbani was missing".  This film being a high budget film but the film became failure at box office.

==Soundtrack==
There are 5 songs in the movie composed by Chandrabose. Lyrics are by vaali and pulamaipithan.
*Naatukulle - SPB, SP Shailaja
*Thangamani - SPB, Vani Jayaram
*Neelakuyilgal Rendu - SPB


==References==
 
==External links==
 
 
 
 
 
 