No. 4 Street of Our Lady
 
No. 4 Street of Our Lady is a 2009 documentary film about the story of Franciszka Halamajowa, a Polish-Catholic woman who risked her life to save her Jewish neighbors.

==Synopsis==
On the eve of World War II, roughly 6,000 Jews lived in Sokal, a small town on the Bug River located in a region known as Eastern Galicia (Eastern Europe)|Galicia. She hid two families in the hayloft of her pigsty for close to two years, and another family in a hole dug under her kitchen floor. Toward the end of the war, she also sheltered a German soldier who had defected from the army.

==Release and reception==
Directed by Barbara Bird, Judy Maltz and Richie Sherman, the film has been screened at dozens of film festivals around the world and broadcast on television in the United States,   Israel  and Poland.  Among other accolades, It was the recipient of a Cine Golden Eagle Award,  a Silver Telly Award   the grand prize for Best Documentary Film at the Rhode Island International Film Festival in 2009   and Best Documentary at the Detroit Jewish Film Festival in 2010. Writing in The Jewish Week, film critic George Robinson called it a "Holocaust documentary with a subtle but very real difference, its structure combining the present with memory in a way that most such films cannot." 

The film is now being distributed by Olive Tree Pictures, a non-profit that promotes what it describes as "life-transforming" films. 

==Production==
Many of the details of this rescue story were contained in a diary kept by one of the survivors, the late Moshe Maltz, whose granddaughter produced the film. The diary was translated into English and self-published in 1993.   Shot largely on location in Sokal, the film also incorporates testimonies from other survivors, the descendants of the rescuer and local townspeople. Following the release of the film, the Anti-Defamation League post-humously honored Halamajowa with its "Courage to Care" award.   Both Halamajowa and her daughter, Helena, were recognized in 1984 as Righteous Among the Nations by the Israeli Holocaust memorial institution, Yad Vashem — the highest honor bestowed on those who saved Jews during the Holocaust. 

==Controversy==
In March 2013, a Canadian money manager, J.L. Witterick, (also known as Jenny Witterick and Jennifer Witterick) published a book, "My Mothers Secret,"  which she said was inspired by the film.  The book was originally self-published by iUniverse, but after entering the Globe and Mail bestseller list, it was picked up by the international Penguin group.     In February 2014, the filmmakers filed a suit in the Canadian Federal Court against her and her publisher, Penguin Canada, for copyright and moral rights infringement.  The suit lists 30 instances of copying or near copying from the film in the book. The case has yet to be heard in court.  

== References ==
 

==External links==
*  
*  

 
 