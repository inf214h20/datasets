In the Shadow of the Wind
{{Infobox film
| name           = In the Shadow of the Wind
| image          = In the Shadow of the Wind.jpg
| caption        = Film poster
| director       = Yves Simoneau
| producer       = Justine Héroux
| writer         = Marcel Beaulieu Sheldon Chad Anne Hébert Yves Simoneau
| starring       = Steve Banner
| music          = 
| cinematography = Alain Dostie
| editing        = Joële Van Effenterre
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Canada
| language       = French
| budget         = 
}}

In the Shadow of the Wind ( ) is a 1987 Canadian drama film directed by Yves Simoneau. It was entered into the 37th Berlin International Film Festival.   

==Cast==
* Steve Banner as Stevens Brown
* Charlotte Valandrey as Olivia Atkins
* Laure Marsac as Nora Atkins
* Angèle Coutu as Maureen
* Paul Hébert as Thimothe Brown
* Marie Tifo as Irene Jones
* Bernard-Pierre Donnadieu as Priest
* Lothaire Bluteau as Perceval Brown
* Roland Chenail
* Guy Thauvette as Patrick Atkins
* Pierre Powers as Sydney Atkins
* Henri Chassé as Bob
* Denise Gagnon
* Jocelyn Bérubé
* Jean-Louis Millette as Vieux Stevens

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 