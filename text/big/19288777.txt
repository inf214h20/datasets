Sleeping Luck
{{Infobox Film |
  name                =  Sleeping Luck|
  image               =  |
  director            = Ángeles González Sinde |
  writer              = Ángeles González Sinde  Belén Gopegui|
  starring            = Adriana Ozores Velilla Valbuena Pilar Castro Ana Wagener Alfonso Vallejo|
  producer            = Gerardo Herrero  Javier López Blanco   Mariela Besuievsky|
  release date        = 2003 |
  runtime             = 105 minutes |
  language            = Spanish|
  budget              =  |
  music               = |
  country             = Spain |
  production_company  = 
|}}
 Sleeping Luck (in Spanish language|Spanish, La suerte dormida) is a 2003 movie directed by Ángeles González Sinde. The film is based on real facts.

==Synopsis==
Ángela (Adriana Ozores), a lawyer who has recently lost her family, accepts an indemnity case against a construction company for the death of one of its workers.

== Prizes ==
=== Goyas 2004 ===
{| border="1" cellpadding="4" cellspacing="0"
|- bgcolor="#CCCCCC"
! Category|| Person ||Result
|-
|Best New Director||Ángeles González Sinde||Winner
|- Best Actress Adriana Ozores||Nominée
|-
|}
*Premio Turia al mejor trabajo novel (2003)

== External links ==
*  

 
 
 
 
 


 