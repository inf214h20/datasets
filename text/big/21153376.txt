Madagascar 3: Europe's Most Wanted
{{Infobox film
| name        = Madagascar 3: Europes Most Wanted
| image       = Madagascar3-Poster.jpg
| alt         =
| caption     = Theatrical release poster Tom McGrath 
| producer    = Mireille Soria Mark Swift
| screenplay  = Eric Darnell Noah Baumbach
| based on    =  
| starring    = Ben Stiller Chris Rock David Schwimmer Jada Pinkett Smith Sacha Baron Cohen Cedric the Entertainer Andy Richter Tom McGrath Jessica Chastain Bryan Cranston Martin Short Frances McDormand
| music       = Hans Zimmer	
| editing     = Nick Fletcher
| studio      = DreamWorks Animation Pacific Data Images
| distributor = Paramount Pictures
| released    =  
| runtime     = 93 minutes
| country     = United States
| language    = English
| budget      = $145 million 
| gross       = $746.9 million 
}}
 Tom McGrath and Conrad Vernon. Its world premiere was at the 2012 Cannes Film Festival on May 18, 2012.

Alex, Marty, Melman and Gloria are still struggling to get home to New York. This time, their journey takes them to Europe where they are relentlessly pursued by the murderous Monaco-based French Animal Control officer Captain Chantel Dubois (Frances McDormand). As a means of getting passage to North America, the zoo animals join a circus as they become close friends, including Gia (Jessica Chastain), Vitaly (Bryan Cranston) and Stefano (Martin Short). Together, they spectacularly revitalize the business and along the way find themselves reconsidering where their true home really is.

The film was released on June 8, 2012, to critical and commercial success; it is the best-reviewed film in the series, with a 79% "Certified Fresh" approval rating on the review aggregate site Rotten Tomatoes.  It is also the highest-grossing Madagascar film with a worldwide gross of over $746 million.  A spin-off sequel titled Penguins of Madagascar was released on November 26, 2014. A sequel, Madagascar 4, was announced for 2018, but it was removed from its schedule due to the studios restructuring. 

==Plot==
 
  Alex the African lion Marty the Melman the Gloria the King Julien, Maurice and the penguins Mason and Phil, and fly back to New York City, which they agree to do.

At Monte Carlo, the penguins and chimpanzees have been saving up their daily winnings from the casino to fly back to Africa and bring their friends home. Alex and his gang see them and believe that they had ditched them to stay there and their cover is blown. Animal control, captained by the overzealous Chantel Dubois is called in to deal with the animals. A high-speed chase around the streets of Monaco ensues between the relentless Dubois and the animals in a truck driven by the penguins. They barely escape Dubois, who vows to capture them and add Alexs head to her collection of stuffed and mounted animals.
 Stefano the Gia the Vitaly the Siberian tiger|tiger. The animals soon learn from Stefano that they are performing in Rome and London, where they plan to impress a promoter to get them on their first American tour. Before the zoo animals claim is discredited, the penguins suddenly appear with a deal to purchase the circus themselves, resulting in the pleased departure of all the humans. Julien also starts falling in love with a tricycle-riding bear named List of Madagascar (franchise) characters#Sonya|Sonya. Meanwhile, the circus animals perform their act at the Colosseum in Rome, but the show proves to be a disaster, much to the zoo animals horror. The angered audience demands refunds, right to going to the point of chasing the circus to the departing train.

En route to London, Stefano soon reveals to Alex that Vitaly was the biggest star of them all. But his attempt at an impossible jump through a flaming pinkie ring ended in disaster when he burned his fur, (which he had coated in extra virgin olive oil to slip through the narrow opening which was flammable ),Vitaly lost everything most importantly his passion.
Due to Vitaly being their inspiration they lost passion as well. The train makes a stop in the Alps, where an inspired Alex convinces the performers to rework their act to become the worlds first animal-only circus. Heartened by Alexs vision, the zoo and the circus animals develop sophisticated acts together and become closer friends in the process, especially Alex and Gia, who find themselves falling in love.

Meanwhile, Dubois is arrested in Rome after causing problems with the local police officers while chasing the animals, but escapes and discovers that Alex was the missing lion from the zoo in New York. Once free, Dubois recruits her injured men and they head toward the Alps, forcing the animals to proceed to London despite incomplete rehearsals. Alex finds Vitaly preparing to leave and convinces him to stay by reminding him of how he enjoys performing the impossible. He suggests that he uses hair conditioner as a safer lubricant to perform his flaming ring jump as well as fix his damaged fur. As a result, Vitalys stunt is performed perfectly and the show is a spectacular success. After the impressed promoter arranges for an American tour, Dubois shows up with a paper showing that Alex was missing. Though the penguins are able to foil Dubois plan, Alex is forced to confess that the four of them are just zoo animals trying to get home, disappointing the others who feel used and lied to by the four of them. The zoo animals and circus go their separate ways but arrive in New York City at the same time. Realizing how much their adventures have changed them, the zoo animals find that their true home was with the circus. Before they can go back, however, Dubois tranquilizes and captures them, before being discovered by the zoo staff, who believe she is responsible for returning the missing animals. Julien and the penguins manage to get to the circus and convince the circus animals to rescue their friends.
 first film).

==Voice cast==
{{multiple image
| direction = horizontal
| image1    = Ben Stiller Cannes 2012.jpg
| width1    = 100
| image2    = Jessica Chastain Cannes 2, 2012.jpg
| width2    = 124
| footer            = Ben Stiller and Jessica Chastain at the 2012 Cannes Film Festival, where the film had its worldwide premiere.
}}
 
* Ben Stiller as List of Madagascar (franchise) characters#Alex|Alex, a lion and Gias love interest. zebra and Alexs best friend.
* David Schwimmer as List of Madagascar (franchise) characters#Melman|Melman, a reticulated giraffe|giraffe, another of Alexs friends and Glorias love interest.
* Jada Pinkett Smith as List of Madagascar (franchise) characters#Gloria|Gloria, a hippopotamus, another of Alexs friends and Melmans love interest. King Julien XIII, a ring-tailed lemur.
* Cedric the Entertainer as List of Madagascar (franchise) characters#Maurice|Maurice, an aye-aye.
* Andy Richter as List of Madagascar (franchise) characters#Mort|Mort, a mouse lemur. Tom McGrath Skipper and First Policeman. Captain Chantal DuBois, the leader of the Animal Control.
* Jessica Chastain as List of Madagascar (franchise) characters#Gia|Gia, an Italian jaguar and Alexs love interest. Vitaly a Russian Siberian tiger. Stefano an Italian sea lion. Chris Miller as List of Madagascar (franchise) characters#Kowalski|Kowalski, one of Skippers right-hand men. Rico
* Private
* Sonya the bear Andalusian Triplets (Esmeralda, Esperanza and Ernestina) Mason and Second Policeman
* Vinnie Jones as Freddie the dog Steve Jones as Jonesy the dog
* Nick Fletcher as Frankie the dog
* Eric Darnell as Comandante, Zoo Official and Zoo Announcer Daniel OConnor as Casino Security and Mayor of New York City Danny Jacobs Circus Master

==Production==
DreamWorks Animations CEO  . Katzenberg stated, "There is at least one more chapter. We ultimately want to see the characters make it back to New York."  At the Television Critics Association press tour in January 2009, Katzenberg was asked if there would be a third film in the series. He replied, "Yes, we are making a Madagascar 3 now, and it will be out in the summer of 2012." 

A significant amount of the animation and visual effects for the film had been done at DreamWorks Dedicated Unit, an India-based unit at Technicolor SA|Technicolor. 

==Release==
Madagascar 3: Europes Most Wanted premiered at the Cannes Film Festival on May 18, 2012.    The American release followed on June 8, 2012.    The film was also converted to the IMAX format and shown in specific European territories, including Russia, Ukraine, and Poland. 

===Home media=== UltraViolet System and the Blu-ray and Blu-ray 3D comes with a rainbow wig. 

==Reception==

===Critical reception===
Madagascar 3: Europes Most Wanted received generally positive reviews from critics. Based on 129 reviews, the film holds a "Certified Fresh" rating of 79% on review aggregator Rotten Tomatoes, with an average rating of 6.8/10. The sites critical consensus reads "Dazzlingly colorful and frenetic, Madagascar 3 is silly enough for young kids, but boasts enough surprising smarts to engage parents along the way."    This marks the best general review consensus of the film series that has showed improving critical favor with the original film having a score of 55%,  and the sequel scoring 64%.  On Metacritic, it holds a score of 60 out of 100, based on 26 reviews, indicating "mixed or average reviews." 
 Newark Star-Ledger calls the movie "fun and fast family entertainment.   the animals jazzy circus performance, done in black-light colors and set to a Katy Perry song — may be one of the trippiest scenes in a mainstream kiddie movie since Dumbo saw those pink elephants." 

===Box office=== 11th highest-grossing animated film and the 52nd highest-grossing film. The film took 66 and 94 days of release, respectively, to out-gross its two predecessors. It surpassed Kung Fu Panda 2 to become Dreamworks highest-grossing non-Shrek film and the first non-Shrek film to reach over $700 million.

In North America, the film made $20.7 million on its opening day, which was higher than the opening day grosses of the original film ($13.9 million) and its sequel ($17.6 million).  For its opening weekend, the film ranked at the #1 spot, beating Prometheus (2012 film)|Prometheus, with $60.3 million, which was higher than the opening of the original Madagascar ($47.2 million), but was behind the opening weekend of Escape 2 Africa ($63.1 million).  It remained at the #1 spot for two consecutive weekends.  In North America, it is the highest-grossing film in the series,  the sixth highest-grossing DreamWorks Animation film,  the second highest-grossing 2012 animated film,  and the tenth highest-grossing film of 2012. 

Outside North America, Madagascar 3 out-grossed  )  and became the highest-grossing animated film (surpassed by  )  and the third highest-grossing film ever (at the time), earning $49.4 million.  It also set an opening weekend record for any film in Argentina with $3.80 million  (first surpassed by Ice Age: Continental Drift)  and it set opening weekend records for animated films in Brazil, Venezuela, Trinidad,  and the United Arab Emirates. 

===Accolades===
{| class="wikitable"
|-
! Award!! Category !! Nominated !! Result
|- ASCAP Award 
| Top Box Office Films
| Hans Zimmer
|  
|- Teen Choice Awards 
| Movie Voice
| Chris Rock
| rowspan=13  
|-
| Summer Movie: Comedy/Music
| Madagascar 3
|- Annie Awards  
| Animated Effects in an Animated Production
| Jihyun Yoon
|-
| Character Design in an Animated Feature Production
| Craig Kellman
|-
| Production Design in an Animated Feature Production
| Kendal Cronkhite-Shaindlin, Shannon Jeffries, Lindsey Olivares, Kenard Pak
|-
| Storyboarding in an Animated Feature Production
| Rob Koo
|- Satellite Awards Satellite Award  || Motion Picture, Animated or Mixed Media  ||Madagascar 3
|- Best Original Song
| "Love Always Comes as a Surprise" - Peter Asher & Dave Stewart
|-
| Critics Choice Movie Awards  Best Animated Feature
| rowspan=2|Madagascar 3
|-
| rowspan="3" | 2013 Kids Choice Awards|Kids Choice Awards 
| Favorite Animated Movie
|-
| rowspan=2| Favorite Voice from an Animated Movie
| Ben Stiller
|-
| Chris Rock
|-
|}

==Soundtrack==
{{Infobox album
| Name       = Madagascar 3: Europes Most Wanted
| Type       = Soundtrack
| Artist     = Hans Zimmer 
| Cover      = 
| Released   = June 5, 2012
| Recorded   = 2012 Score
| Length     = 40:25 Interscope
| Producer   = Hans Zimmer
| Chronology = Hans Zimmer film scores
| Last album =   (2011)
| This album = Madagascar 3: Europes Most Wanted (2012) The Dark Knight Rises (2012)
}}
Madagascar 3: Europes Most Wanted is the soundtrack of the film scored by Hans Zimmer and was released on June 5, 2012. 

{{tracklist
| extra_column = Performer
| music_credits = yes
| total_length = 40:25
| title1 = New York City Surprise
| music1 = Hans Zimmer
| length1 = 3:05
| title2 = Gonna Make You Sweat (Everybody Dance Now) Danny Jacobs
| length2 = 2:15 Wannabe
| extra3 = Danny Jacobs
| length3 = 2:37
| title4 = Game On
| music4 = Hans Zimmer
| length4 = 3:12
| title5 = Hot in Herre
| extra5 = Danny Jacobs
| length5 = 2:27
| title6 = We No Speak Americano
| music6 = Yolanda Be Cool & DCUP
| extra6 = Yolanda Be Cool & DCUP
| length6 = 4:29
| title7 = Light the Hoop on Fire!
| music7 = Hans Zimmer
| length7 = 3:10
| title8 = Fur Power!
| music8 = Hans Zimmer
| length8 = 2:18 Non Je Ne Regrette Rien
| extra9 = Frances McDormand
| length9 = 1:13
| title10 = Love Always Comes as a Surprise
| music10 = Peter Asher
| extra10 = Peter Asher
| length10 = 3:21
| title11 = Rescue Stefano
| music11 = Hans Zimmer
| length11 = 5:51 Firework
| music12 = Katy Perry
| extra12 = Katy Perry
| length12 = 3:46
| title13 = Afro Circus/I Like to Move It
| extra13 = Chris Rock & Danny Jacobs
| length13 = 2:41
}}
Note:

* In some variations of the soundtrack, "Cool Jerk" is featured in replacement of "We No Speak Americano".
* "Sexy and I Know It" by LMFAO was only used in the theatrical trailer, and not included on the soundtrack. Journey and album of the same name by Enya were also used, but are not included on the soundtrack.
* "Land of Hope and Glory" by Edward Elgar appears in the track Fur Power. Julius Fučík.

==Video games==
A video game based on the film, Madagascar 3: The Video Game, was released on June 5, 2012. The game allows gamers to play as Alex, Marty, Melman, and Gloria in their attempt to escape Captain Chantel DuBois and return home to New York City.  It was released to Wii, Nintendo 3DS, Nintendo DS, Xbox 360, and PlayStation 3. Published by D3 Publisher, the Wii, Xbox 360 and PlayStation 3 versions were developed by Monkey Bar Games, and the 3DS and DS versions by Torus Games.  The Xbox 360 version received negative reviews from critics with Metacritic giving it a 45 out of 100. 

A mobile video game, Madagascar: Join the Circus!, was released on June 4, 2012, for iPhone and iPad. The game allows players to build a circus and play mini-games.  

==Comic book==
A comic book based on the film and titled Madagascar Digest Prequel: Long Live the King! was released on June 12, 2012, by Ape Entertainment.  

==Sequel and spin-off==
In June 2014, it was announced that Madagascar 4 would be released on May 18, 2018.  In January 2015, the film was removed from the release schedule following corporate restructuring and DreamWorks Animations new policy to release two films a year.   

A spin-off feature film titled  .   

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 