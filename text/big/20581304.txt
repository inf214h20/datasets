Maalik (1972 film)
{{Infobox film
| name           = Maalik
| image          = Maalik72.jpg 
| caption        = 
| director       = A. Bhimsingh
| producer       = 
| writer         = 
| starring       = Rajesh Khanna   Sharmila Tagore 
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 1972 Bollywood drama film directed by A. Bhimsingh. The film stars Rajesh Khanna and Sharmila Tagore . Ashok Kumar played a special appearance.

==Cast==
*Sharmila Tagore ...  Savitri 
*Rajesh Khanna ...  Raju 
*Ashok Kumar ...  Ganesh Duttji Guru 
*Deven Verma ...  Ram Murthy Pandey (as Devan Varma) 
*Sonia Sahni ...  Narangi 
*Bipin Gupta ...  Dharamdevta 
*Shivraj ...  Pandeyji 
*Baby Pinky ...  Lord Krishna

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Khana Milega Pina Milega"
| Kishore Kumar
|-
| 2
| "Aa Man Se Man Ki Dor"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 3
| "Aarti Utaro Radheshyam Ki"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 4
| "Duniya Ki Har Maa Apne Bete Ko"
| Lata Mangeshkar
|-
| 5
| "Kanhaiya Tujhe Aana Padega"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 6
| "O Malik Mere Aaye Hai Sahare Tere"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 7
| "Radhe Krishna Bol Mukh Se"
| Mahendra Kapoor
|-
| 8
| "Kanhaiya Tujhe Aana Padega"
| Mahendra Kapoor
|}

==External links==
*  

 

 
 
 
 
 


 
 