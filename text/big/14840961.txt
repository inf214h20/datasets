Ek Alag Mausam
{{Infobox film
| name           = Ek Alag Mausam 
| image          = Ek_Alag_Mausam.jpg
| caption        =
| director       = K.P.Sasi
| producer       =  
| writer         = Mahesh Dattani
| starring       = Nandita Das Anupam Kher Renuka Shahane  
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 3 January 2003
| runtime        = 120 min 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}
Ek Alag Mausam (  language movie directed by K.P.Sasi and starring Nandita Das, Anupam Kher, Renuka Shahane, Rajit Kapur, Arundathi Nag, Sreelatha and Sally Whittaker.

==Story==
 love story about a couple and the denial of basic rights to HIV positive people. It is based on a script by playwright Mahesh Dattani. Jeroninio Almeida, the fund-raising director of ActionAid asserted that a serious subject can be dealt with in an entertaining way, without trivialising the issue. ActionAid spent Rs 50 lakh for the making of the movie. {{cite web |url= http://ww.smashits.com/ek-alag-mausam-based-on-aids/bollywood-gossip-2794.html|title=Ek Alag Mausam based on AIDS
 |last1=|first1=|last2= |first2=|work=Smashits.com |date=|accessdate=17 November 2012}} 

==References==
 

== External links ==
*  
*  

 
 
 

 