Black Widow (2010 film)
 
{{Infobox film
| name           = Black Widow
| image          = 
| caption        = 
| director       = Mark Roemmich
| producer       = Mark Roemmich
| writer         = Mark Roemmich
| starring       = Janine Turner Jack Scalia Jennifer ODell Christopher McDonald Joanna Pacula Krista Allen Rachel Hunter
| music          = Alan Williams
| cinematography = David Drzeweichi Don Zimmerman
| studio         = United Artists
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Entertainment Company   United International Pictures  
| released       =  
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = $4 million.
| gross          = $34 million.
}}
Black Widow is an 2010 thriller movie directed by Mark Roemmich and starring Janine Turner, Jack Scalia and Jennifer ODell. 

==Plot== 
Sean McMurphy, a wealthy entrepreneur, meets a very beautiful woman and a passionate love affair ignites but soon it unfolds into a dark suspense/thriller where everyone around him is seduced into her dark web of lies, torture and deceit. 

==Reception== 
The movie received mixed to positive reception from critics. Roger Ebert gave the movie 3/4 stars. 

==External links== 
*  

 
 
 


 