Indriyam
{{Infobox film
| name           = Indriyam
| image          = 
| caption        = Poster Lena Vikram Vikram   Vani Viswanath
| director       = George Kithu
| writer         = 
| producer       = 
| cinematography = Prathapan
| editing        = K. B. Harikrishnan
| dialogues      = 
| music          = Berny-Ignatius
| distributor    = 
| studio         = 
| released       = 8 September 2000
| runtime        = 129 minutes Malayalam
| country        = India
}}
 Malayalam horror Vikram and Vani Viswanath appearing amongst others in the cast.  It was dubbed and released in Tamil as Manthira Kottai in December 2000, shortly after the success of Sethu, in which Vikram had featured.

==Plot==
A college group of anthropology students go to the remote forest are of Muthuvan Mala, under the oversight of Prof. Shankaranarayanan (Raghavan), in order to study the tribal life which had existed there. A student, Sunny (Nishanth Sagar), unknowingly unleashes the spirit of Neeli (Vani Viswanath), who is a ghost seeking revenge against the Thripangod royal family, one of whom (Devan (actor)|Devan) had killed her and her lover (Vikram (actor)|Vikram. Consequently Neeli starts killing off the students one by one and the police call for sorcerer Vadakkedath Namboothiri (Prathapachandran) to be summoned.

== Cast ==
* Nishanth Sagar as Sunny
* Boban Alamudan as Hari Varma Lena as Sreedevi
* Yadu Krishnan as Vijay
* Sharath Das as Anoop Raghavan as Shankaranarayanan
* Ravi Vallathol Vikram as Udhaya
* Vani Viswanath as Neeli Devan as Rajaraja Varma Thirumanas
* Aranmula Ponnamma as Muthassi
* Geetha Nair as Hari Varmas mother
* Prathapachandran as Vadakkedath Namboothiri
* Nandhu
* Kochu Preman
* Cochin Haneefa Azeez

==Release==
After release, a film critic noted that "It has a creditable screenplay which develops the vendetta plot convincingly."  In December 2000, the film was dubbed and released in Tamil as Manthira Kottai to make most of Vikrams new found fame following the success of Sethu (1999). 

== References ==
 

==External links==
* 

 
 
 
 

 