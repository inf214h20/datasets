Pervirella
{{Infobox film
|  image          = Pervirella.jpg
|  writer         = Alex Chandon (script), Josh Collins (story) Jason Slater Nico Rilla (additional material) Emily Booth Eileen Daly Sexton Ming
|  director       = Alex Chandon Josh Collins (Co-Director)
|  producer       = Josh Collins
|  distributor    = Eclectic DVD Distribution Screen Edge
|  music          = François Evans
|  cinematography = Cobra Media Jon Ford
|  editing        = Alex Chandon
| studio          = Exotic Entertainment Productions
|  released       = 
|  runtime        = 92 minutes English
|  tagline        = 
|  awards         = 
|  budget         = 
}}
 Emily Booth Jonathan Ross and Mark Lamarr. 

Pervirella harbors a sex demon within her. 

==Plot==
In the land of Condon, the deranged Queen Victoria seals the country in behind a huge wall and establishes a "Monarchy of Terror." The intellectuals and sexually liberated are persecuted and murdered. Some of them form an underground movement, the Cult of Perv, led by the Demon Nanny who dies giving birth to Pervirella, who grows at an amazing rate into a beautiful young woman. Whenever her magic necklace is removed, Pervirella becomes a raging nymphomaniac and - hunted by every interested group in Condon - teams up with special agent Amicus Reilly. 

==Cast== Emily Bouffante as Pervirella
* Ron Drand as Professor Rumphole Pump
* The Shend as Monty
* Sexton Ming as Queen Victoria
* Tara Hamilton	as Ingrid Thorne
* Anthony Waghorne as Sexton Ming
* Eileen Daly as Cu-Rare
* Max Décharné as The Curator
* Lenny Fowler as Hoffman
* Helen Darling as Effete
* Hannah Walker	as Esquesa
* Mark Lamarr as Irch Bishop Jonathan Ross as Bish Archop
* David Warbeck as Amicus Reilly
* Benedict Martin as Erasmus Chan Roger Robinson as Admiral Titeship
* Rebecca Eden as Demon Nanny
* Dianne Hickman as Nurse Hagg
* Anna McMellin as Baby Pervirella
* Simon Wolfe Howard as Doctor Connery
* Jason Slater as Advisor Rectum
* Ian Damage Ballard as Executioner
* Pat Prince of Wales as Sexy Older Lady
* Josh Collins as Guard / Abo Extra / Monster Gash
* Matt Brain as Duke Nehru / Soggy
* Babzotica as Lady of the Barrel / Witch Doctor
* Alex Chandon as Savage Dynamite Robert Black as Duke Nukem 3D
* Marie Bennet as Duchess of Muffy
* Davo M. Davenport as Eunuch
* Su Goulding as Wench
* Yvan Serrano-Fontova as Japanese Executioner / musician ( Healer Selecta/Dustaphonics)
* Austin Vince as Cheeky Chinaman
* Damian Stephens as Ninja
* Alberto Fox Miyashima as Ninja
* Mike Butch Body Hurst as Killer Robot
* Sheer Khan as Flying Carpet
* John Grover as Abo Bone
* Wayne Northern as Abo Doo
* Matt Russel as Abo Stick Mark White as Abo Rafia / Scarred Perv William Loveday as Spiritual Guru
* Dirty Jacqui Burd as Cheerleader
* Dirty Terri Burd as Cheerleader
* Tequila Sunrise as Tequila Sunrise
* Demented Ant as Ant demented
* Howard J. Ford as Statue of Liberty
* Jon Ford as Peter Fonda
* Claire French-Blake as Aqua-Manda
* Anne-Marie Foss as Lovely Nubile Peter Godwin as Macho John Lee as Hairy
* Stephen Allen as Laughlin
* Steve Fettes as Erect
* Bubsi as Baldi
* James Linguard as Smelly
* Dario Ilari as Horny
* Darren Hepolitte as Spiky
* Brian Belle-Fortune as Cuddly
* Simon Booth as Cute
* Debbie Malynn as Cuter
* Helena Spostolides as Cutest
* Carlo Roberto as Cheeky
* Graham Russel as Corny
* Cathi Unsworth as Beautiful Pervette
* Bruce Brand as Beach Band

==Production==
The Underground UK punk rockers Billy Childish, Sexton Ming, Simon Wolfe Howard and Bruce Brand had cameos.    Alex Chandon and Josh Collins cast over 140 actors for his film project, include Caroline Munroe, but she left the project after short time. 

==Release==
The film was originally released as VHS Tape on 27 October 1998 in the UK.    The DVD contains comprehensive biographies, the story behind the film and its music, photos and more and was released on 5 May 2002 over Eclectic DVD Distribution. 

==Soundtrack== Soundtrack was released as RocknRoll-Victorian-Erotic-Horror-Soundtrack, composed by François Evans, it is include Tracks from Frat Shack Shakedown and Perve Parlor. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 