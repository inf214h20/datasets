Glory Glory (2000 film)
 
 
{{Infobox film
| name           = Glory Glory
| director       = Paul Matthews
| writer         = Paul Matthews
| producer       = Elizabeth Matthews, Paul Matthews
| starring       = Chantell Stander Paul Johansson Amanda Donohoe Steven Bauer
| music          = Mark Thomas
| cinematography = Vincent G. Cox
| editing        = Peter Davies
| country        =   RSA English
| budget         =
| released       = 2000
| preceded by    =
| followed by    =
}}
Glory Glory is a 2000 western film.    The film was also marketed under the title Hooded Angels. 

==Synopsis==
It is the dawn of the Wild West in Silver Creek. A gang of beautiful but deadly women have unleashed violence on the society that failed to protect them, robbing banks and destroying everything in their path. A posse is sent after these masked raiders, not knowing whom they are chasing nor the fate that will await them - for bringing these female vigilantes to justice will unleash a surprising past and a new future for all.

==Cast==
*Amanda Donohoe as Widow
*Steven Bauer as Jack
*Paul Johansson as Wes
*Gary Busey as Sheriff
*Chantell Stander as Hannah
*Juliana Venter as Ellie
*David Dukas as Billy
*Gideon Emery as Sil
*Jenna Dover as Jane
*Julie Hartley as April
*Candice Argall as Becky
*Jennifer Steyn as Christa
*Anna Katerina as Marie
*Michelle Bradshaw as Sherrie

==References==
 

==External links==
* 

 
 
 
 
 


 
 