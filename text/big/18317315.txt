Kal Kissne Dekha
{{Infobox film
| name          | image          = KalKissneDekha2009.jpg
| alt            =  
| caption        = Movie poster of Kal Kissne Dekha
| director       = Vivek Sharma
| producer       =| writer         = Vivek Sharma, Shyam Goel & Sudhanshu Dube
| starring       = Jackky Bhagnani Vaishali Desai Rishi Kapoor Nushrat Bharucha
| music          = Sajid-Wajid
| cinematography = 
| editing        = 
| studio         = 
| distributor    = BIG Cinemas
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = $24,625 (US) 
}}
Kal Kissne Dekha ( ,   Sci-fi film|sci-fi romance film directed by Vivek Sharma, who earlier directed Bhootnath. The film introduces debutantes Jackky Bhagnani and Vaishali Desai.    It has been reported to have similarities with the 2007 Hollywood film Next (2007 film)|Next, starring Nicolas Cage.    Kal Kissne Dekha released on 12 June 2009 and was an box office flop.

==Plot==
Nihal Singh (Jackky Bhagnani) is a teen from Chandigarh who loves to build complicated gadgets to learn what it is behind science. He has a brilliant mind and dreams of studying at an elite science institute. When he is accepted into his dream college in Mumbai, he discovers college is a whole new world, new people and new challenges. At school Nihal is attracted to Meesha (Vaishali Desai), a proud, rude brat. She cannot accept that Nihal is managing to charm everyone his way including Professor Siddarth Verma (Rishi Kapoor), the professor of the college.

Nihal starts to get visions of Meesha being in danger. He saves her life and his secret is revealed that he can see the future. After this incident, love blossoms between Nihal and Meesha. Meanwhile, media exposure results in attention from anti-social elements, including an explosion at a mall. Nihal sees the explosion before it takes place and manages to rescue most people. It turns out one of Nihals friend is behind the attack. Despite seeing the bomber, he keeps quiet and doesnt tell the police department. He strengthens up and decides to fight the attacker himself.

==Production==
The film commenced shooting on 18 June 2008 in the University of the Witwatersrand in Johannesburg, South Africa.  Durban, Cape Town, Stellenbosch (a town near Cape Town) and the University of Stellenbosch were the other locations. The prominent oaks in Stellenbosch can be clearly seen in many of the scenes. One of the dance numbers was shot in the universitys library. 

In March 2009, it was announced that Azharuddin Mohammed Ismail and Rubina Ali, two of the child stars of Slumdog Millionaire (2008), were cast in Kal Kissne Dekha.  

==Cast==
* Jackky Bhagnani as Nihaal Singh
* Vaishali Desai as Nisha Kapoor
* Archana Puran Singh as Bebe (Nihaals mother)
* Rishi Kapoor as Professor Sidharth Verma
* Ritesh Deshmukh as Kali Charan
* Rahul Dev as Marshal
* Vrajesh Hirjee as Police Inspector
* Farida Jalal as Maria
* Sanjay Dutt as DJ (special appearance)
* Juhi Chawla (special appearance)
* Nushrat Bharucha as Ria
* Akshay Kapoor as Kabir

==Music==
{{Infobox album |  
| Name = Kal Kissne Dekha
| Type = Soundtrack
| Artist = Sajid-Wajid
| Cover = KalKissneDekha2009AlbumCover.jpg
| Released =  17 March 2009 (India) |
| Recorded = Feature film soundtrack
| Length = 42:33 Sameer
| Label = Big Music
| Producer = 
| Reviews =
| Chronology = 
| Last album = 
| This album = 
| Next album = 
}}

Music is composed by Sajid-Wajid with lyrics provided by Sameer (lyricist)|Sameer. The music has got good reviews.

{{tracklist
| headline        = Tracklist     
| music_credits   = no
| extra_column    = Artist(s)
| title1          = Alam Guzarne Ko
| extra1          = Sonu Nigam, Suzanne DMello
| length1         = 4:40
| title2          = Kal Kissne Dekha
| extra2          = Shaan (singer)|Shaan, Shreya Ghoshal, Bob
| length2         = 4:58
| title3          = Kal Kissne Dekha (Club Mix)
| extra3          = Shaan (singer)|Shaan, Shreya Ghoshal, Bob
| length3         = 4:59
| title4          = Soniye Billori
| extra4          = Sonu Nigam, Suzanne DMello
| length4         = 5:15
| title5          = Jashn Hai  Wajid
| length5         = 4:43
| title6          = Bin Tere Mar Jawan
| extra6          = Shreya Ghoshal
| length6         = 1:26 Tere Bina Lagta Nahin Mera Jiya Wajid
| length7         = 4:41
| title8          = Soniye Billori (Club Mix)
| extra8          = Sonu Nigam, Suzanne DMello
| length8         = 5:29
| title9          = Kal Kissne Dekha (Romantic Version) Wajid
| length9         = 1:46
| title10         = Aasmaan Jhuk Gaya
| extra10         = Shaan (singer)|Shaan, Shreya Ghoshal
| length10        = 4:33
}}

==References==
 

==External links==
*  
*  
*  

 
 
 