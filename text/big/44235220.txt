Five Star Hospital
{{Infobox film
| name           = Five Star Hospital
| image          =
| caption        =
| director       = Thaha
| producer       =
| writer         =
| screenplay     =
| starring       = Vishnu Sukumari Jagathy Sreekumar Thilakan
| music          = Bombay Ravi
| cinematography =
| editing        =
| studio         = Screen Art Productions
| distributor    = Screen Art Productions
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, directed by Thaha. The film stars Vishnu, Sukumari, Jagathy Sreekumar and Thilakan in lead roles. The film had musical score by Bombay Ravi.   

==Cast==
 
*Vishnu
*Sukumari
*Jagathy Sreekumar
*Thilakan Kalpana
*Devan Devan
*Geetha Geetha
*Jagadish Kaveri
*Mala Aravindan
*N. F. Varghese
 

==Soundtrack==
The music was composed by Bombay Ravi and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anaadi Gayakan || KG Markose || Yusufali Kechery || 
|-
| 2 || Chiricheppu || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Ithra Madhurikkumo || K. J. Yesudas || Yusufali Kechery || 
|-
| 4 || Maamava || K. J. Yesudas || Yusufali Kechery || 
|-
| 5 || Maranno Nee Nilaavil || K. J. Yesudas || Yusufali Kechery || 
|-
| 6 || Maranno Nee Nilaavil   || KS Chithra || Yusufali Kechery || 
|-
| 7 || Vaathil Thurakkoo   || K. J. Yesudas || Yusufali Kechery || 
|-
| 8 || Vaathil Thurakku    || KS Chithra || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 