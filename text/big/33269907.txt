Kon Khon
{{Infobox film
| name           = Kon Khon
| image          = 
| caption        = 
| director       = Sarunyu Wongkrachang
| producer       = 
| writer         = Sarunyu Wongkrachang
| screenplay     = 
| story          = 
| based on       =  
| starring       = Apinya Rungpitakmana,   Nantarat Chaowarat   Nirut Sirichanya   Sorapong Chatree
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sahamongkol Film International
| released       =  
| runtime        = 90 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| gross          = 
}}

Kon Khon ( ) is a 2011 Thai film by Sarunyu Wongkrachang. It concerns a love triangle involving three rival Khon dancers The film was selected as the Thai entry for the Best Foreign Language Film at the 84th Academy Awards,        but it did not make the final shortlist.   

==Plot==
An orphan, Chart, raised by his master after his parents death and trained be to be a Khon dancer, becomes involved with the dance teacher Rambai.

==Cast==
*Apinya Rungpitakmana ... Chart
*Gongtun Pongpatna ... Tue
*Nantarat Chaowarat... Ram
*Pimolrat Pisalayabuth ... Rambai
*Nirut Sirichanya
*Sorapong Chatree

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Thai submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 
 


 
 