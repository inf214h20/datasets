Rowdy Ramu
{{Infobox film
| name = Rowdy Ramu
| image =
| caption =
| director = M. Krishnan Nair (director)|M. Krishnan Nair
| producer = M Mani
| writer = Sunitha Cheri Viswanath (dialogues)
| screenplay = Cheri Viswanath Madhu Sharada Sharada Jayabharathi Jose Prakash Shyam
| cinematography =
| editing = G Venkittaraman
| studio = Sunitha Productions
| distributor = Sunitha Productions
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by M Mani. The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, Jayabharathi and Jose Prakash in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
   Madhu  Sharada 
*Jayabharathi 
*Jose Prakash 
*Manavalan Joseph  Raghavan 
*Adoor Bhavani 
*Anandavally 
*Aranmula Ponnamma 
*Aryad Gopalakrishnan
*Baby Sreekala
*Balan K Nair 
*KPAC Sunny 
*Poojappura Ravi  Sadhana 
*Veeran 
 

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Gaaname Prema Gaaname || K. J. Yesudas, Vani Jairam || Bichu Thirumala || 
|- 
| 2 || Manjin Thereri || S Janaki, Vani Jairam || Bichu Thirumala || 
|- 
| 3 || Naladamayanthi Kadhayile || K. J. Yesudas || Bichu Thirumala || 
|- 
| 4 || Neram Poy || K. J. Yesudas, Chorus || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 


 