The Gang's All Here (1939 film)
The directed by produced by Associated British Picture Corporation. Subsequently, the film was released in 1943 with the film title of The Amazing Mr. Forrest. 

==Synopsis==
The dapper musical comedy favorite, Jack Buchanan is practically the whole show in The Gangs All Here. Buchanan plays John Forrest, a top investigator for the Stamford Insurance Company. Retiring from the firm, Forrest intends to devote the rest of his life to writing detective fiction, but this plan goes out the window when his former employers are robbed of $1,000,000 in jewels belonging to foreign potentate Prince Homouska (Walter Rilla). With the help of his befuddled butler Treadwell (Edward Everett Horton), Forrest follows the trail of clues to American gangster boss Alberni (Jack La Rue), capturing his quarry with a variety of slapstick subterfuges. Released in the US by PRC Pictures, The Gangs All Here remains one of Jack Buchanans best-loved vehicles. 

==Cast==
* Jack Buchanan as John Forrest
* Googie Withers as Alice Forrest
* Edward Everett Horton as Treadwell
* Syd Walker as Younce
* Otto Kruger as Mike Chadwick
* Jack La Rue as Alberni
* Walter Rilla as Prince Homouska David Burns as Beretti Charles Carson as Charles Cartwright
* Leslie Perrins as Harper
* Ronald Shiner as Spider Ferris

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 