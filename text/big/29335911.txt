La Traversée de Paris (film)
{{Infobox film
| name           = La Traversée de Paris
| image          = 
| caption        = 
| director       = Claude Autant-Lara
| producer       = Franco-London Film (Paris), Continentale Produzione (Rome)
| writer         = Marcel Aymé Jean Aurenche
| starring       = Jean Gabin Bourvil Louis de Funès
| music          = René Cloërec
| cinematography = Jacques Natteau
| editing        = Madeleine Gug
| distributor    = S.N.A Gaumont
| released       = 26 October 1956 (France)
| runtime        = 80 minutes
| country        = France/Italy
| language       = French, German
| budget         = 
}}
 French Comedy comedy Drama drama film from 1956, directed by Claude Autant-Lara, written by Marcel Aymé, starring Jean Gabin, Bourvil and Louis de Funès. The film is known under the titles: "Four Bags Full" (USA), "Pig Across Paris" (UK), "The Trip Across Paris" (International English title). 

== Cast ==
*Jean Gabin : Grandgil, the painter
*Bourvil : Marcel Martin, taxi driver in unemployment
*Louis de Funès : Jambier, the grocer
*Jeannette Batti : Mariette Martin, wife of Marcel
*Jacques Marin : the boss of the restaurant
*Robert Arnoux : Marchandot, the butcher pork butcher
*Georgette Anys : Lucienne Couronne, the coffee maker
*Jean Dunot : Alfred Couronne, the proprietor
*Monette Dinay : Mrs Jambier, lépicière
*René Hell : the father Jambier
* Myno Burney : Angèle Marchandot, la bouchère, charcutière
* Harald Wolff : German commander (uncredited)
* Bernard Lajarrige : a policeman
* Anouk Ferjac : la jeune fille lors de lalerte (uncredited)
* Hubert Noël : le gigolo arrêté (uncredited)
* Béatrice Arnac : prostitute (uncredited)
* Jean/Hans Verner : le motard allemand
* Laurence Badie : la serveuse du restaurant
* Claude Vernier : le secrétaire allemand de la Kommandantur
* Hugues Wanner : le père de Dédé

==Plot==
Action takes place in Paris during WWII in 1942. Unemployed Taxi-Driver, Marcel Martin makes his living delivering parcels on the black market. One day, he must carry by foot, to the other side of the capital, four suitcases containing pork meat. He goes to the basement of a grocer named Jambier and plays the accordion while the animal is butchered.

Martin then goes with his wife Mariette to the restaurant where he must find his accomplice. He learns that he has been arrested by the police. A stranger then enters the restaurant and on a misunderstanding, Martin invited him to share his meal and to work with him replacing his former accomplice.

This decision is quickly turning out as calamitous as this new character, named Grandgil, isnt very compliant. He firsts ask for a drastic increase in salary terrorizing the unfortunate grocer Jambier. Then he almost damages the bar, where the two accomplices are hiding from the police, and calls the patrons "poor coward". 

Grandgil then almost knocks-off a policeman in Martins neighborhood. And later when escaping a German patrol, they end up taking refuge in the apartment of Grandgil, where Martin is stunned to discover that Grandgil is in fact a famous painter of some renown who has agreed to follow along mainly for his own entertainment.

Nevertheless continuing their path, they finally arrive at the delivery but find the door locked. Then they make such a racket that a German patrol arrives. In the Kommandantur1 where they are taken, a German officer recognizes the famous painter Grandgil ; and he is about to release both of them when an announcement of the assassination of a German colonel changes the situation allowing him only to save in extremis Grandgil, while Martin is sent to Germany for Compulsory Work Service (STO).

The years pass. War is over, and we find Grandgil on a dock station in Lyon followed by a carrier bag. Then on top of the window of the car, suddenly Grandgil recognizes  Martin, as always carrying other peoples suitcases.

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 
 
 
 

 