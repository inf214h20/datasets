Pagal (film)
{{Infobox film
| name           = Pagal
| image          = Pagal_(1940).jpg
| image_size     =
| caption        = Film poster
| director       = A. R. Kardar Ranjit Movietone
| writer         = A. R. Kardar
| narrator       =
| starring       = Prithviraj Kapoor Madhuri Noor Mohammed Charlie Sitara Devi
| music          =
| cinematography = Krishna Gopal
| editing        = Khemchand Prakash
| distributor    =
| released       = 5 July 1940
| runtime        =
| country        = British Raj Hindi
| budget         =
| gross          =
| website        =
}}
 1940 Indian Bollywood film. It was the fourth highest grossing Indian film of 1940.  The psycho-social melodrama was directed by A. R. Kardar for Ranjit Movietone.    The story and dialogues were also by Kardar. Khemchand Prakash composed the music with lyrics written by D. N. Madhok.    The film had Prithviraj Kapoor playing a doctor with psychological problems, working in a "lunatic asylum".    The rest of the cast included Madhuri, Sitara Devi, Noor Mohammed Charlie, Khatoon, Trilok Kapoor and Sunalini Devi.   

Th film involves a doctor who loves a girl but marries her sister due to an unfortunate coincidence. His treatment of his wife and the girl he loves, and his own eventual deterioration into insanity forms the basis of story. Pithviraj Kapoors was appreciated by the critics and the audience, making the film a big commercial success.   

==References==
 

==External links==
*  

 

 
 
 
 


 