Merton of the Movies
{{Infobox film
| name           = Merton of the Movies
| image          =Merton OTM.jpg
| caption        = 1924 movie poster
| director       = James Cruze
| producer       = Adolph Zukor Jesse Lasky Walter Woods (scenario)
| based on       = novel Merton of the Movies by Harry Leon Wilson play by George S. Kaufman and Marc Connelly Glenn Hunter Viola Dana
| music          = Karl Brown
| editing        =
| distributor    = Paramount Pictures
| released       = November 3, 1924
| runtime        = 80 minutes; 8 reels(7,655 feet)
| country        = USA
| language       = Silent..English
}} Walter Woods. Glenn Hunter and Viola Dana.

==Plot==
Merton is an aspiring movie actor. He is a terrible actor but when the movie executives see how funny his overacting is, they cast him in a comedy, but tell him that hes acting in a drama.

==Cast== Glenn Hunter – Merton Gill
*Charles Sellon – Pete Gashwiler
*Sadie Gordon – Mrs. Gaswiler
*Gale Henry – Tessie Kearns
*Luke Cosgrove – Lowell Hardy
*Viola Dana – Sally Montague or Flips
*DeWitt Jennings – Jeff Baird
*Elliott Rothe – Harold Parmalee Charles Ogle – Mr. Montague
*Ethel Wales – Mrs. Montague
*Frank Jonasson – Henshaw
*Eleanor Lawson – Mrs. Patterson

==See also==
*List of lost films
*Hollywood (1923 film)|Hollywood (1923), a film about movies also directed by James Cruze and also a lost film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 