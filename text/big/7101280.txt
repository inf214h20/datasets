Sweeney Todd: The Demon Barber of Fleet Street (1936 film)
 
 
{{Infobox film
| name           = Sweeney Todd: The Demon Barber of Fleet Street
| image          = The Demon Barber Of Fleet Street.jpg
| image_size     = 250px
| caption        = UK release poster George King
| producer       = George King
| writer         = Frederick Hayward H.F. Maltby George Dibdin-Pitt
| starring       = Tod Slaughter Stella Rho John Singer Eve Lister Bruce Seton
| music          = Eric Ansell
| cinematography = Jack Parker 
| editing        = John Seabourne, Sr.
| distributor    = George King Productions
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom
| language       = English
}}
 drama horror George King. The film features actor Tod Slaughter in one of his most famous roles as the barber Sweeney Todd.

== Plot ==
 
Sweeney Todd (Tod Slaughter) is a barber with a shop near the docks of London.

Using his charm and tonsorial skills, Todd lures wealthy, respectable customers into his barber-shop at Fleet Street, where he settles them into a mechanical barbers chair which dumps them head-first down into the basement, ready to have their throats cut with a straight-edge, razor-sharp blade—if the fall does not kill them first.

Mrs. Lovett|Mrs. Lovatt (Stella Rho), a lady who makes meat-pies next-door, disposes of the bodies for a share of the stolen money.
 the daughter (Eve Lister) of a local aristocrat, but her father (D. J. Williams) refuses him. When her lover, Mark (Bruce Seton), returns from an ocean voyage, Todd tries to get him out of the way.

The fall, however, does not kill Mark, and Mrs. Lovatt hides him, allowing him to escape. When he recovers, he returns in disguise to expose the barber.

Realizing that he has been exposed, Todd sets fire to his shop and is ironically killed by his own hand, setting his trap floor into motion at the end of the film.

== Cast ==
*Tod Slaughter as Sweeney Todd
*Stella Rho as Mrs. Lovett|Mrs. Lovatt
*John Singer as Tobias Ragg Johanna Oakley
*Bruce Seton as Mark Ingerstreet
*D. J. Williams (actor)|D. J. Williams as Stephen Oakley
*Davina Craig as Nan
*Jerry Verno as Pearley
*Graham Soutten (credited as Ben Souten) as Beadle
*Billy Holland as Mr. Parsons
*Norman Pierce as Mr. Findlay
*Aubrey Mallalieu as Trader Paterson Henry B. Longhurst as a man on the quayside who talks to Sweeney Todd   Ben Williams as Captain Stephenson  

== External links ==
*  
*  
* Watch   at  

 
 

 
 
 
 
 
 
 
 
 
 