Ultraman (1979 film)
{{Infobox Film
| name = Ultraman
| image = Ultraman (1979 film).jpg
| image_size = 
| caption = Theatrical poster
| director = Akio Jissoji 
| producer = Noboru Tsuburaya
| writer = Mamoru Sasaki
| narrator = 
| starring = Susumu Kurobe Akiji Kobayashi
| music = Kunio Miyauchi
| cinematography = Yasumichi Fukuzawa Masaharu Utsumi
| editing = 
| distributor = Tsuburaya Productions / Toho
| released = March 17, 1979
| runtime = 102 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1979 Japanese tokusatsu kaiju film directed by Akio Jissoji. It is a compilation film made up of scenes from Jissojis episodes of the original Ultraman TV series.

== Cast ==
*Akiji Kobayashi
*Susumu Kurobe
*Sandayū Dokumamushi
*Masanari Nihei
*Hiroko Sakurai

==Monsters==
*Gavadon
*Telesdon
*The Under The Ground Human Jamila
*Skydon
*Seabozu
*Alien Baltan Red King Gomora

==Availability==
Ultraman (1979) was released on DVD in Japan on June 13, 2006. 

Ultraman (1979) was released on LaserDisc in Japan on July 22, 1993.

==External links==
*  
*  
*  

==Notes==
 

 
 

 
 
 
 
 
 
 
 


 