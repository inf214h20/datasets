The Hands
The Italian film directed by Alejandro Doria. Doria and Juan Bautista Stagnaro wrote the screenplay. The film won one Goya Award.

==Movie Cast==
*Graciela Borges as Perla
*Jorge Marrale as Padre Mario
*Duilio Marzio as Monseñor Alessandri
*Belén Blanco as Silvia
*Carlos Portaluppi as Padre Giacomino
*Carlos Weber as Monseñor Arizaga
*Jean Pierre Reguerraz as Spagnuolo

==Movie Awards and nominations==
Argentine Film Critics Association Awards
*Won: Best Costume Design
*Nominated: Best Actor &ndash; Leading Role (Jorge Marrale)
*Nominated: Best Actor &ndash; Supporting Role (Duilio Marzio)
*Nominated: Best Actress &ndash; Leading Role (Graciela Borges)
*Nominated: Best Art Direction (Margarita Jusid)
*Nominated: Best Director (Alejandro Doria)
*Nominated: Best Film
*Nominated: Best New Actor (Esteban Perez)
*Nominated: Best Screenplay &ndash; Original (Alejandro Doria and Juan Bautista Stagnaro)
 Goya Awards (Spain) Best Spanish Language Foreign Film

==External links==
* 

www.myspace.com/lasmanos1

 
 
 
 
 


 