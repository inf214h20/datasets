Les Saignantes
{{Infobox Film
| name           = Les Saignantes
| image          = Les Saignantes.jpg
| caption        = Les Saignantes promotional poster.
| director       = Jean-Pierre Bekolo
| producer       = Jean-Pierre Bekolo Andre Bennett Lisa Crosato Jim Fink Michelle Gue Pascale Obolo Adrienne Silvey
| writer         = Jean-Pierre Bekolo
| starring       = Dorylia Calmel Adèle Ado
| music          = Joelle Esso Adam Zanders
| cinematography = Robert Humphreys
| editing        = Jean-Pierre Bekolo
| runtime        = 92 mins. France  Cameroon
| French
}} erotic thriller thriller with a strong political sensibility. Two sexy young women win the favors of the corrupt political elite, but when one of these leaders dies in the middle of a sexual act, the friends are left with a corpse to get rid of. Bekolo eviscerates the ruling elite but with the canny use of inter-titles also leaves the audience with something to ponder. The film won the Silver Stallion (second best African film) at Fespaco 2007 and the Best actress awards with the special mention of the jury.

==Cast==
* Adèle Ado as Majolie.
* Dorylia Calmel as Pet
* Emile Abossolo MBo as Minister of State
* Josephine Ndagnou as Natou
* Essindi Mindja as Essomba
* Alain Dzukam as Rokko
* Veronique Mendouga as Dr. Amanga
* Bekate Meyong as Mamba
* Thierry Mintamack as Tony

==Critics==
*http://www.austinchronicle.com/gyrobase/Calendar/Film?Film=oid%3A341348
*http://www.grioo.com/info8719.html
*http://www.africultures.com/php/index.php?nav=article&no=3943
*http://www.africine.org/?menu=art&no=8155
*http://www.africansuccess.org/visuFiche.php?id=102&lang=fr

==Awards==
* Adèle Ado won Best Actress for her work in Les Saignantes at the 2007 Ouagadougou Panafrican Film and Television Festival.
* Jean-Pierre Bekolo won the "Silver Etalon de Yennega" for his work on the film at the 2007 Ouagadougou Panafrican Film and Television Festival.

==External links==

#  

 
 
 
 
 
 


 
 