Arang (film)
{{Infobox film
| name           = Arang
| image          = Arang film poster.jpg
| caption        = Theatrical poster
| director       = Ahn Sang-hoon
| writer         = Ahn Sang-hoon Shin Yun-kyung
| producer       = Lee Min-ho   Lee Gyeong-heon   Park Jae-su   Hwang Mun-su
| starring       = Song Yun-ah   Lee Dong-wook
| cinematography = Chung Kwang-suk
| editing        = Ko Im-pyo
| music          =  	Jeong Dong-in
| distributor    = CJ Entertainment
| released       =  
| runtime        = 97 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
| film name      = {{Film name
| hangul         = 아랑
| rr             = Arang
| mr             = Arang
| hanja          =  }}
}} 2006 South Korean horror film.

==Synopsis==
A veteran detective So-young and her rookie partner Hyun-ki come across an incendiary homicide case. They discover that the present case is related to the mysterious death of a girl ten years prior. So-young becomes plagued with nightmares in which the girl appears, and the killings continue.

The girl is actually Min-jeong, Hyun-kis first love. Those who died are killed by Hyun-ki by giving them a cigarette that is inlaid with a gas which causes a simulated heart attack. Those who have the cigarette raped Min-jeong 10 years ago in an abandoned salt house. Hyun-ki was forced to tape the incident as he was emotionally wounded when he saw Min-jeong and her lover making out in the salt house at graduation day. Hurt, he agrees to tape the incident when the group who plans to rape Min-jeong persuaded him. Min-jeongs lover, who came by to save her, was killed by one of the group members. Min-jeong was said to have gone mad and disappeared after that incident, but in fact she was pregnant and buried under a mound of salt after one of the police officers who handled the rape incident tricked her into the salt house and locked her inside, burying her along with her unborn child. 

Hyun-ki threatens the police officer to go to the salt house and dig out Min-jeong. So-young catches upon both of them and also threatens Hyun-ki to put down his gun. Hyun-ki pleads with So-young to kill him, but So-young refuses, saying that she will let him live in regret and make him watch his children suffer after him, whispering in his ear when hes dying and says that his life is no better than a stray dog. Hyun-ki states that he cant bear it, and afraid to live that kind of life, ends his life by committing suicide. 

The water that rains down on that day washes away the salt that buried Min-jeong, and slowly, Min-jeongs body is revealed along with her dead baby between her legs. Her corpse is not rotten, thanks to the salt that preserved her body. The forensic doctor states that when a pregnant woman dies, gas formed inside the body. The gas pushes the baby out even after the mother is dead.

So-young was once violated by an unknown man with a scar on his right hand when she was young. She stated that the reason she became a police officer was to find that man and kill him. After the incident, the man who once violated her, now a father and a successful businessman, mysteriously dies in a hotel room. It is said that the night before the man died, So-young dreamed about the girl wearing a white dress, smiling and laughing together in the salt house. She concluded that the dream she had signified that Min-jeong had helped her take her revenge by killing the man, and after that So-young writes a novel and gets it published. 

At the end of the film, theres a myth said to relate to the movie. The Legend of Arang 400 years ago states that theres a village full of new magistrates that are killed mysteriously. That is why no new magistrate dares to go to the village to be appointed. However, theres a new magistrate willing to go to the village. He found out that the spirit of Arang is full of hatred and revenge because she was raped and killed. After that, he helped the spirit to catch the culprit who raped and killed her, sending the culprit to justice. He found Arangs corpse and buried her. It was said that because of her hatred, her corpse did not rot when it is found after so many years. 

==Cast==
* Song Yun-ah... So-young
* Lee Dong-wook... Hyun-ki
* Lee Jong-su... Dong-min
* Kim Hae-in... Min-jeong
* Jung Won-joong... Kim Ban-jeong
* Lee Seung-cheol... Jo So-jang
* Choo So-young... Su-bin
* Jeon Jun-hong... Jeong-ho
* Joo Sang-wook... Jae-hyeon
* Lee Seung-ju... Ji-cheol
* Kim Ok-bin

==Availability== Region 1 DVD on May 8, 2007. 

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 