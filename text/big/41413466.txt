Android Cop
{{Infobox film
| name           = Android Cop
| image          = Android Cop.jpg
| caption        = DVD cover Mark Atkins
| producer       = {{Plainlist| 
* David Michael Latt
* David Rimawi
* Paul Bales
}}
| screenplay     =  
| story          = 
| starring       = {{Plainlist| 
* Michael Jai White
* Charles S. Dutton 
* Randy Wayne 
* Kadeem Hardison
}}
| music          = Chris Ridenhour
| cinematography = 
| editing        =  
| studio         = The Asylum
| distributor    = The Asylum
| released       =   }}
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 science fiction action film produced by The Asylum and directed by Mark Atkins. The film stars Michael Jai White, Charles S. Dutton, Randy Wayne and Kadeem Hardison.  It is a mockbuster of RoboCop (2014 film)|RoboCop.  

== Plot ==
In the year 2045, a Los Angeles Police Department detective and his new partner - a state of the art crimefighting robot cop - are tasked with recovering a runaway Telepresence android containing the consciousness of the Mayors daughter, who remains unaware that she is a human mind in a machine body.  Their investigation takes the unlikely partners into the heart of the Zone - a vast, irradiated section of California created by earthquakes years earlier - where they recover the Mayors daughter but are betrayed by corrupt fellow officers, forcing the trio to fight their way out of the Zone through mutant ghettos, gang territories, and even the LAPD itself.

== Cast ==
* Michael Jai White as Officer Hammond
* Kadeem Hardison as Sgt. Jones
* Randy Wayne as Andy (The Android Cop)
* Charles S. Dutton as Mayor Jacobs
* Larissa Vereza as Helen Jacobs
* Jay Gillespie as Reynolds
* Deena Trudy as Officer Jackson
* Delpaneaux Wills as Officer Williams
* Duane Avery as Newald Mason

== Release ==
The film was released direct-to-video and video-on-demand on February 4, 2014 in the United States. In the tradition of The Asylums catalog, Android Cop is a mockbuster of the 2014 Metro-Goldwyn-Mayer|MGM/Columbia Pictures remake of RoboCop (2014 film)|RoboCop. 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 