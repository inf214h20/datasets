Lady of the Tropics
 
{{Infobox film
| name           = Lady of the Tropics
| image          = Lady of the Tropics - Film Poster.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster Jack Conway 
| producer       = Sam Zimbalist
| writer         = Ben Hecht
| narrator       = Robert Taylor Hedy Lamarr Joseph Schildkraut Gloria Franklin
| music          = Franz Waxman
| editing        = Elmo Veron
| cinematography = George J. Folsey Norbert Brodine
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $913,000  . 
| gross          = $1,533,000 
}} Jack Conway Robert Taylor, mixed race background he meets in Saigon. The film was nominated for the Academy Award for Best Cinematography.

==Main cast==
  Robert Taylor as Bill Carey 
* Hedy Lamarr as Manon DeVargnes 
* Joseph Schildkraut as Pierre Delaroch 
* Gloria Franklin as Nina 
* Ernest Cossart as Father Antoine 
* Mary Zimbalist as Dolly Harrison 
* Charles Trowbridge as Alfred Z. Harrison 
* Frederick Worlock as Colonel Demassey 
* Paul Porcasi as Lamartine 
* Marguerita Padula as Madame Kya 
* Cecil Cunningham as Countess Berichi 
* Natalie Moorhead as Mrs. Hazlitt

==Reception==
According to MGM records the film made $1,042,000 in the US and Canada and $491,000 elsewhere, resulting in a profit of $99,000. 

==Bibliography==
* Barton, Ruth. Hedy Lamarr: The Most Beautiful Woman in Film. University Press of Kentucky, 2010.
* Marchetti, Gina. Romance and the "Yellow Peril": Race, Sex, and Discursive Strategies in Hollywood Fiction. University of California Press, 1993.

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 