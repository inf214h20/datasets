Elektra (2005 film)
{{Infobox film name		= Elektra image		= Elektra teaser.jpg caption        = Theatrical Release Poster writer         = {{Plainlist|
*Zak Penn
*Stuart Zicherman
*Raven Metzner}} based on       =   starring	= {{Plainlist| 
*Jennifer Garner Goran Visnjic
*Will Yun Lee
*Cary-Hiroyuki Tagawa
*Terence Stamp}} director	= Rob Bowman producer	= {{Plainlist|
*Avi Arad
*Gary Foster
*Arnon Milchan}} studio		= {{Plainlist| Marvel Enterprises
*Regency Enterprises New Regency Productions
*Horseshoe Bay Productions}} distributor	= 20th Century Fox released	=   runtime	= 96 minutes   99 minutes   country	= {{Plainlist|
*United States
*Canada}} language	= {{Plainlist|
*English
*Japanese}} budget		= $43 million gross		= $56.7 million  . Box Office Mojo. Accessed August 30, 2008.  music		= Christophe Beck cinematography = Bill Roe editing        = Kevin Stitt
}} directed by Rob Bowman. Elektra Natchios assassin whose weapon of choice is a pair of Sai (weapon)|sai.
 Frank Miller Fantastic Four, Sky High. 

==Plot==
  Elektra Natchios Stick (Terence Stamp). She is brought to his training compound to learn Kimagure, an ancient martial arts discipline that provides its practitioners with precognition as well as the ability to resurrect the dead. Elektra is soon expelled because of her inability to let go of her rage. She leaves and uses her training to become a contract killer.
 Goran Visnjic). The Hand, a crime syndicate of ninja mercenaries.
 Typhoid (Natassia Stone (Bob Tattoo (Chris Ackerman). Elektra flees with Mark and Abby through a secret underground exit to the orchard, while McCabe sacrifices himself to allow them to escape.
 Chaste ninjas arrive, forcing Kirigi, Typhoid, and Tattoo to retreat. Stick manages to save Elektra from death and takes them under his protection.
 astrally projects herself to a meeting with Kirigi and challenges him to a fight; the winner claiming Abby for their own purpose. Elektra returns to her childhood home to face Kirigi, and finally remembers he was her mothers killer. Elektra is defeated by Kirigi, but Abby arrives and engages him long enough for Elektra to recuperate. Elektra and Abby then escape and hide in a hedge maze but are separated when Abby is captured by snakes dispatched by Tattoo. Elektra finds Tattoo and kills him, saving Abby in the process. Elektra engages Kirigi a second time and manages to kill him. Typhoid poisons Abby, the same way she did to Elektra earlier, killing her in the process. Elektra kills Typhoid and successfully resurrects Abby, overcoming her rage. When Mark comes to take Abby, he and Elektra kiss and go their separate ways. Just as Elektra leaves the grounds of her childhood home for the final time, she meets Stick and the two exchange words to each other.  Elektra departs, knowing Abby and Mark will be safe.

==Cast==
  Elektra Natchios Stick
*Goran Goran Visnjic as Mark Miller
*Kirsten Prout as Abby Miller
*Will Yun Lee as Kirigi
*Cary-Hiroyuki Tagawa as Roshi
*Colin Cunningham as McCabe
*Hiro Kanagawa as Meizumi Typhoid
*Bob Stone
*Chris Tattoo
*Edison T. Ribeiro as Kinkou
*Laura Ward as Young Elektra
*Jana Mitsoula as Mrs. Natchios
*Kurt Max Runte as Nikolas Natchios
*Jason Isaacs as DeMarco 
 

Ben Affleck reprised his role as Matt Murdock in a cameo, but was cut from the final film. The scene was included on the DVD as a deleted scene. 

==Reception==

===Box office===
Elektra opened on January 14, 2005 in the United States in 3,204 theatres. In its opening weekend it ranked 5th, taking $12,804,793. Must Have Content   Howard the Duck. The film had a worldwide total of $56,681,566. Although both movie adaptations of "Howard, the Duck" and Elektra did fail to meet their expected revenues at the box office, both did manage to make their respective gross budgets back, and make a reasonable small profit, with Elektra adding around an estimated $12,000,000, whereas Howard, the Duck added a little over $1,000,000 to meet its production budget In fact, the only movie adaptation based on a Marvel Comic Book character that was released in both American and worldwide cinemas that not only failed at the box office to meet its expected profit gross, but also failed to meet its estimated production value budget was the "Punisher: War Zone," in 2008. 

===Critical reception===
Elektra received generally negative reviews from film critics. Based on 150 reviews collected by Rotten Tomatoes Elektra earned a 10% "rotten" rating.  Also, Metacritic gave it a metascore of 34 out of 100, indicating "generally negative reviews". 

==Home media==
The DVD of Elektra was released on April 5, 2005. It featured several deleted scenes, including one featuring a cameo of Ben Affleck who starred as the title character in Daredevil (film)|Daredevil, which preceded this film.

===Unrated cut===
An extended and slightly refined two-disc unrated edition directors cut DVD was released in October 2005, featuring a cut detailed for a home video. However, unlike the Daredevil directors cut which added about 30 minutes of material not in the original theatrical release, this directors cut added only about 3 minutes of footage. It was also criticized for poor video transfer.  . Accessed 2008-08-30 

A Blu-ray Disc of Elektra was released on October 19, 2009 for the United Kingdom (and France) only.
The U.S. version was released on May 4, 2010. It contains only the unrated cut of the film.

==Soundtrack==
{{Infobox album 
| Name        = Elektra: The Album
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = January 11, 2005
| Recorded    = 
| Genre       =Hard rock
| Length      =
| Label       = Wind-up Records|Wind-up
| Producer    = Various producers
| Chronology  = Marvel Comics film series soundtrack Music from and Inspired by Spider-Man 2 (2004)
| This album  = Elektra: The Album (2005)
| Next album  =   (2005)
}}
{{Album ratings rev1 = AllMusic rev1score =   
}}
Elektra: The Album was released in 2005 by Wind-up Records for the film Elektra starring Jennifer Garner.

As with many Wind-up Records|Wind-up soundtracks, almost none of the songs featured on the album were actually used in the movie. "Sooner or Later" is played briefly in one scene and a remix not included on this album of "Hollow" is also played. The end credits feature "Wonder", "Photograph" and "Thousand Mile Wish (Elektra Mix)": but other than this, none of the songs on the album were used in the actual motion picture.

===Track listing===
{{tracklist
| extra_column    = Artist
| title1          = Never There (She Stabs) Strata
| length1         = 3:44
| title2          = Hey Kids Jet
| length2         = 2:58
| title3          = Everyone is Wrong
| extra3          = The Donnas
| length3         = 3:28
| title4          = Sooner or Later 
| extra4          = Switchfoot
| length4         = 4:09
| title5          = Thousand Mile Wish (Elektra Mix)
| extra5          = Finger Eleven
| length5         = 4:00
| title6          = Wonder
| extra6          = Megan McCauley
| length6         = 3:53
| title7          = Your Own Disaster
| extra7          = Taking Back Sunday
| length7         = 5:42
| title8          = Breathe No More
| extra8          = Evanescence
| length8         = 3:48
| title9          = Photograph
| extra9          = 12 Stones
| length9         = 3:58
| title10         = Save Me
| extra10         = Alter Bridge
| length10        = 3:27
| title11         = Beautiful The Dreaming
| length11        = 3:03
| title12         = Hollow
| extra12         = Submersed
| length12        = 4:04
| title13         = Angels With Even Filthier Souls
| extra13         = Hawthorne Heights
| length13        = 2:55
| title14         = 5 Years
| extra14         = The Twenty Twos
| length14        = 3:52
| title15         = In The Light
| extra15         = Full Blown Rose
| length15        = 4:13
}}

==Video game== publishers felt it would not be popular enough. However, there is a game based on the movie that was released for mobile game|mobile. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 