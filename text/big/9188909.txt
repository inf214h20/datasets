Ayyamna al-Holwa
{{Infobox film
| name           = Ayyamna al-Holwa  أيامنا الحلوة 
| image          = Ayyamna-al-Helwa.jpg
| image_size     =
| caption        = Main cast of Ayyamna al-Holwa. From left to right: Ali (Abdel Halim Hafez), Ramzy (Ahmed Ramzy), Hoda (Faten Hamama, and Ahmed (Omar Sharif)
| director       = Helmy Halim
| producer       = Arab Film (الفلم العربي)
| writer         = Ali El Zorkani Helmy Halim
| starring       = Faten Hamama Omar Sharif Abdel Halim Hafez Ahmed Ramzy Zeinat Sedki
| distributor    = 
| released       = 1955
| runtime        = 
| country        = Egypt Arabic
| budget         = 
}}
 1955 Egyptian Egyptian Cinema centennial, this film was selected one of the best 150 Egyptian film productions.

== Plot ==
Faten Hamama plays Hoda, a poor woman who leaves an orphanage to live with three young men (Ahmed, Ali, and Ramzi) in a room on a building rooftop. The three of these men fall in love with her, but she prefers Ahmed, who is played by Omar Sharif, and the others accept that and stay loyal to their friendship. When, one day, Hoda gets sick, the three men urgently work hard to gather enough money to pay for her surgery. The film is not conclusive about what happens to Hoda, but she is supposed to live with her sickness for the rest of her life and can hardly work or get married. What the film shows is the love and fraternity that is created in her friends through her sickness.

== Main cast ==
*Faten Hamama as Hoda
*Omar Sharif as Ahmed
*Abdel Halim Hafez as Ali
*Ahmed Ramzy as Ramzy
*Zahrat El-Ola as Zanouba
*Zeinat Sedki as Bekhaterha

== References ==
* 
* 
* 

== External links ==
*  

 
 
 
 
 
 
 


 
 