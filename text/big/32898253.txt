The Exchange (film)
 
{{Infobox film
| name           = The Exchange
| image          = The Exchange (film).jpg
| caption        = Film poster
| director       = Eran Kolirin
| producer       = Eran Kolirin Elon Ratzkovsky Karl Baumgarten
| writer         = Eran Kolirin
| starring       = Dov Navon Rotem Keinan Sharon Tal Shirili Deshe
| music          = 
| cinematography = Shai Goldman
| editing        = Arik Leibovitch
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Israel Germany
| language       = Hebrew
| budget         = 
}}
The Exchange ( , Transliteration|translit.&nbsp;Hahithalfut) is an 2012 Israeli drama film directed by Eran Kolirin.  The film was screened in competition at the 68th Venice International Film Festival in September 2011.   

==Plot==
Oded (Rotem Keinan) is an assistant lecturer at the Tel Aviv University and married to Tami (Sharon Tal), a young architect looking for a job.  Oded becomes friends with a neighbour, Yoav (Dov Navon), and they become obsessed with breaking pre-ordained frames and examining their lives objectively.

==Cast==
 
* Dov Navon as Yoav
* Rotem Keinan as Oded
* Sharon Tal as Tami
* Shirili Deshe as Yael

==References==
 

==External links==
*  

 
 
 
 
 
 
 