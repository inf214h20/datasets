The Kiss (1988 film)
 
{{Infobox Film
| name =
| image =
| image_size =
| caption =
| director = Pen Densham John Watson
| writer = Stephen Volk (story and screenplay) Tom Ropelewski
| starring = Joanna Pacula Meredith Salenger Nicholas Kilbertus Mimi Kuzyk Jan Rubes Shawn Levy
| music = J. Peter Robinson
| cinematography = Francois Protat
| editing = Stan Cole
| studio =
| distributor = TriStar Pictures
| released = October 14, 1988
| runtime = 101 minutes
| country = United States Canada
| language = English
| budget = $2.5 million
| gross = $1,869,148
}}

The Kiss is a horror/thriller film released in 1988 and set in the United States and Africa.  The Kiss is a late occult-horror-voodoo style film starring Joanna Pacula, Meredith Salenger and Nicholas Kilbertus.

A short prologue set in "Belgian Congo, 1963" establishes two of the main characters, Hilary and Felice Dunbar, and also the films curse, as well as a cursed totem resembles a seemingly-angry leech-like serpentine figure with a long tongue. Flash forward to the late 1980s, Albany, New York, where Hilary (Talya Rubin) lives with her husband Jack Halloran (Nicholas Kilbertus) and teenage daughter Amy (Meredith Salenger). Their suburban stability is shattered when Hilary receives an unexpected phone call from her estranged sister Felice (Joanna Pacuła), now a globe-travelling model. The two arrange to meet, yet Hilary dies in a gruesome car accident soon after inviting Felice to visit her family in Albany. Felice shows up anyway five months later, swiftly seduces Jack, kills a few interlopers, and makes quick enemies with her niece Amy.

==Cast==
*Joanna Pacula as Felice Dunbar
*Meredith Salenger as Amy Bridget Halloran
*Nicholas Kilbertus as Jack Halloran
*Mimi Kuzyk as Brenda Carson
*Jan Rubes as Gordon Tobin
*Shawn Levy as Terry OConnell
*Sabrina Boudot as Heather
*Pamela Collyer as Hilary Edna Dunbar Halloran

==External links==
* 

 
 
 
 
 
 
 
 
 


 