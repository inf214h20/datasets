Arima Nambi
{{Infobox film
| name           = Arima Nambi
| image          = Arima nambi.jpg
| caption        = Theatrical poster
| director       = Anand Shankar
| producer       = S. Thanu
| writer         = Anand Shankar
| starring       = Vikram Prabhu Priya Anand J. D. Chakravarthy
| music          = Sivamani
| cinematography = R. D. Rajasekhar
| editing        = Bhuvan Srinivasan
| studio         = V Creations
| distributor    = Vendhar Movies
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 2014 Tamil Indian Tamil suspense action thriller film directed by Anand Shankar.  Vikram Prabhu, Priya Anand, and J. D. Chakravarthy in pivotal roles. Drummer Sivamani made his debut as a music director in the film,  which was shot by R. D. Rajasekhar while the editing was done by Bhuvan Srinivasan. The shoot of Arima Nambi started on 3 June 2013 in Chennai.  It released on 4 July 2014 to mostly positive reviews. 

==Plot==
Arjun Krishna (Vikram Prabhu), a technician in a Chennai-based BMW showroom, meets Anamika Raghunath (Priya Anand), a college student, while out with his friends at Hard Rock Cafe. They immediately fall in love with each other and go on a date the following night. Following the date, Anamika invites Arjun to her apartment for a bottle of vodka. Things go well until Anamika gets kidnapped by two men. Arjun, who was in the bathroom when Anamika was kidnapped, immediately lodges a complaint at the Besant Nagar police station. Sub-Inspector Arumugam (M. S. Baskar), who is assigned the case, then goes to Anamikas apartment along with Arjun, but the apartment is now cleaned up and there seems to be no sign that a kidnapping has taken place, while the apartments watchman insists that Anamika had not returned to her apartment for three days. Also, the CCTV camera installed at the apartment does not show any men arriving and taking away Anamika, prompting Arumugam to suspect that Arjun has lodged a false complaint as he is drunk. However, when Arumugam calls up Anamikas father Raghunath Kumar (Shankar Sundaram), the chairman of the TV news channel Channel 24, he merely replies that he doesnt know anything about the kidnapping and that Anamika is holidaying in Goa, at which Arumugam suspects something amiss, realising that Arjun had not lied after all. Arumugam and Arjun rush to Raghunaths mansion, where they see him being threatened by the two men who had kidnapped Anamika as well as the police inspector of Besant Nagar to give them a SD card which has some critical information. Raghunath calls up his editor and asks him to give the SD card to the three men, after which he is killed by them. Arjun and Arumugam pursue the three men. While Arjun manages to kill the police inspector, the other two men escape, killing Arumugam in the process.
 Union Minister Prime Ministerial candidate Rishi Dev (J. D. Chakravarthy). He kills the two men and informs the situation to rishi dev, who decides to rush to Chennai within the next three hours.

Having got the SD card as well as its password, Arjun inserts the memory card into his mobile phone and sees that it has only one file in it; a video. The video shows the murder of model-turned-actress Megha Sharma (Lekha Washington) at the hands of Rishi Dev two days ago and how he had fabricated the incident to make it look like the actress died due to a gas leak at her mansion. Arjun and Anamika, horrified on seeing this and also on the extent to which Rishi Devs henchmen went to keep the video from being seen, decide to expose Rishi Dev by uploading the video on YouTube. This backfires when Arjuns friend Raj (Arjunan (actor)|Arjunan), brings Rishi Devs henchmen to the hotel room where Arjun and Anamika are staying, having been bribed by them to do so. They manage to subdue Raj as well as Rishi Devs henchmen and go on the run.
 the Forum Vijaya Mall (where they are hiding) at 3 pm as there is going to be a "sensational event" there. This move backfires completely on Arjun and Anamika, as Anamika accidentally breaks the SD card, destroying the only piece of evidence which is against Rishi Dev. The police take advantage of the media assembled at the mall and claim on live television that Arjun and Anamika are murderers, announcing a   5 lakh reward for those who capture them.

Arjun then hatches another plan to expose Rishi Dev. He contacts Rishi Dev through the police commissioner, asking him to meet him later that night at his private airfield alone. At the airfield, he tries to force Rishi Dev to reveal the truth about Megha Sharmas death, using his mobile phone to record the meeting. But before he can get out the truth, the police arrive, following which Rishi Dev destroys the mobile phone and beats him up, all the while revealing that he killed Megha Sharma, but he cannot do anything about it because he is going to be arrested for the attempted murder of a Union Minister. Unfortunately for Rishi Dev, Arjun had organized their entire meeting, as well as Rishi Devs revelation, using a button camera, with the video having been shared using 3G connectivity to Anamika, who then ensured that the video was broadcast live by all leading TV news channels, thus exposing Rishi Dev, who is soon arrested.
 proposes marriage to Anamika, to which she agrees.

==Cast==
*Vikram Prabhu as Arjun Krishna
*Priya Anand as Anamika Raghunath
*J. D. Chakravarthy as Rishi Dev Arjunan as Raj
*Yog Japee as Arulraj
*M. S. Baskar as S. I. Arumugam
*Shankar Sundaram as Raghunath
*Aruldoss as henchman
*Sathish as Rishi Devas P. A.
*Lekha Washington as Megha Sharma (guest appearance).

==Release==
The satellite rights of the film were sold to Jaya TV. The film released on 4 July 2014 in 1200 screens. It had its premiere for celebrities and the technical crew at Sathyam cinemas on 3 July 2014.  The film subsequently became the leading film at the box office on the weekend of its release, taking a good opening.   It continued its successful run by remaining in first place for a second consecutive week despite new releases and subsequently went on to record profits at the box office. 

===Critical reception===
The film released to mostly positive reviews. http://www.ibtimes.co.in/arima-nambi-movie-review-roundup-decent-thriller-603704  
Baradwaj Rangan wrote, "Arima Nambi may not be great art, but at least for a while, it’s a supremely well-engineered machine. Anand Shankar thumbs his nose at (...) conventional wisdom. His is a Hollywood sensibility, and his film needs it because it’s a conspiracy thriller and the only way to make this kind of movie is to make it the Hollywood way". He went on to call the first half "fantastic" while the second half was "less riveting".  The Times of India gave it 3 stars out of 5 and wrote, "The film loses some of its verve in the second half, which feels a little stretched and all over the place. And, yet, there is enough cleverness in the writing and assuredness in the execution, especially for a debut film, to make Arima Nambi stand apart from your usual action thrillers".  S. Saraswathi, writing for Rediff.com said, "A skilfully written plot, deft direction and excellent camera work make director Anand Shankars Arima Nambi a highly engaging, action-packed thriller", and gave it 3 stars out of 5. 

Sify wrote, "Arima Nambi has very racy first half but loses steam in the second half. In fact the first half an hour is taut and racy, but wafer thin storyline, predictable scenes, songs and a long drawn out climax acts as speed breakers", calling it "a decent attempt by a group of youngsters to make a fairly decent thriller".  Deccan Chronicle, giving the film 2.5 out of 5 stars, wrote, "Anand Shankar has chosen one (...) intelligent plot with Vikram Prabhu helming the affair, which is gripping in parts".  Bangalore Mirror called the film "a well-crafted, nail-biting thriller, barring a few romantic scenes in the beginning".  Silverscreen described the plot as "familiar", but said the "difference lies in the packaging – stylish, riveting and clever".  Indiaglitz said the movie was "an action thriller that speeds up, hits the bumper and then shoots off again".  Oneindia.in gave it 2.5 stars and called it "A stylish thriller, which can been watched once". 

==Soundtrack==
{{Infobox album 
| Name = Arima Nambi
| Longtype = 
| Type = Soundtrack
| Artist = Drums Sivamani
| Border = yes
| Alt =
| Caption = 
| Released = 13 April 2014
| Recorded = 2013-14 Feature film soundtrack Tamil
| Label = Tiger Audios
| Length = 22:10
| Producer = Drums Sivamani
| Last album = 
| This album = Arima Nambi (2014)
| Next album = Kanithan (2014)
}}
The films soundtrack was composed by Drums Sivamani, for whom the film marks his composing debut. The album features five tracks and was released on 13 April 2014 at Sathyam Cinemas, Chennai.  The album became the first venture for producer Kalaipuli S. Dhanus newly formed music label, Tiger Studios. 

{{Track listing
| headline       = Track listing 
| extra_column   = Singer(s)
| music_credits  = no
| lyrics_credits = yes
| all_lyrics     = 
| total_length   = 22:10

| title1 = Yaaro Yaar Aval
| extra1 = Runa Rizvi, Shabir
| lyrics1 = Madhan Karky
| length1 = 5:23
| title2 = Idhayam En Idhayam
| extra2 = Javed Ali
| lyrics2 = Na. Muthukumar
| length2 = 5:20
| title3 = Naanum Unnil Paadhi
| extra3 = Rita, Alma
| lyrics3 = Pulamai Pithan
| length3 = 5:10
| title4 = Neeye Neeye
| extra4 = Shreya Ghoshal
| lyrics4 = Arivumathi
| length4 = 4:22
| title5 = Arima Nambi - The Theme
| extra5 = Instrumental
| length5 = 2:05
}}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 