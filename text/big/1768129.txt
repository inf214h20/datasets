Fear and Trembling (film)
{{Infobox film
| name           =Stupeur et tremblements
| image          =
| director       =Alain Corneau
| producer       =
| writer         =Amélie Nothomb Alain Corneau
| starring       =Sylvie Testud Kaori Tsuji Taro Suwa Bison Katayama
| movie_music    =
| distributor    =
| released       =2003
| runtime        =107 min
| country        = France, Japan Japanese
| budget         = 
| music          = 
| awards         = 
}} French film novel of the same name by Amélie Nothomb.

== Synopsis ==
Amélie, a young Belgian woman (played by Sylvie Testud), having spent her childhood in Japan, decides to return to live there and tries to integrate into Japanese society.  She is determined to be a "real Japanese" before her year contract runs out, though it is precisely this determination that is incompatible with Japanese humility.  Though she is hired for a choice position as a translator at an import/export firm, her inability to understand Japanese cultural norms results in increasingly humiliating demotions.  Though Amelie secretly adulates her immediate supervisor, Ms Mori (Kaori Tsuji), the latter takes sadistic pleasure in belittling Amelie.  Mori finally manages to break Amelies will by making her the bathroom attendant, and is delighted when Amelie tells her that she will not renew her contract.  Amelie realizes that she is finally a real Japanese when she enters the company presidents office "with fear and trembling," which was possible only because her determination had been broken by Moris systematic humiliation.

The title, "Fear and Trembling", is said in the film to be the way Japanese must behave when addressing the Emperor. For Westerners, it calls to mind a line from Philippians 2:12, "continue to work out your salvation with fear and trembling", which could also describe Amélies attitude during her year at Yumimoto.

==Awards and nominations==
*César Awards (France) Best Actress &ndash; Leading Role (Sylvie Testud) Best Writing (Alain Corneau)
 Karlovy Vary Film Festival (Czech Republic)  Facing Windows (La finestra di fronte))
**Won: Special Mention (Alain Corneau)
**Nominated: Crystal Globe (Alain Corneau)

==External links==
* 
 
 
 
 
 
 
 
 

 