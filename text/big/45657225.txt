Shoes (1916 film)
{{Infobox film
| name           = Shoes
| image          = Shoes 1916 film.jpg
| caption        =
| director       = Lois Weber
| producer       = Bluebird Photoplays
| writer         = Lois Weber (scenario)
| based on       = story, Shoes, by Stella Wynne Herron from a novel by Jane Addams
| starring       = Mary MacLaren
| cinematography = Stephen S. Norton King Gray Al Siegler
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       = June 26, 1916
| runtime        = 60 minutes
| country        = USA
| language       = Silent..English
}}
Shoes is a 1916 silent film drama directed by Lois Weber and starring Mary MacLaren. It was distributed by the Universal Film Manufacturing Company and produced by a subsidiary called Bluebird Photoplays.  

The film was held and restored by the EYE Institute Nederlands between 2008-2011. 

==Cast==
*Mary MacLaren - Eva Meyer
*Harry Griffith - Her Father
*Mattie Witting - Her Mother (*as Mrs. A.E. Witting)
*Jessie Arnold - Lil
*William V. Mong - Cabaret Charlie

uncredited
*Lina Basquette - ?Unknown role
*Violet Schram - ?Unknown role

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 