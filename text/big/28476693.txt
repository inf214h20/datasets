Ayyanar (film)
{{Infobox film
| name           = Ayyanar
| image          = 
| caption        = 
| director       = Rajamithran
| producer       = P. L. Thenappan
| writer         = Rajamithran Aadhi Meera Rajashree Santhanam Santhanam Anupama Kumar Jayaprakash       
| music          = Thaman
| cinematography = Sri Sriram
| editing        = M.Kasivishwanathan
| studio         = Sri Rajalakshmi Films
| distributor    = 
| released       = 10 December 2010
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Aadhi and Santhanam and Anupama Kumar playing supporting roles. The film was released on 10 December 2010 to negative reviews and was an Average at the Box office, it was dubbed in Telugu as "Vasthad".  One month after its release, it was telecasted on Vijay TV during Pongal. 

==Cast== Aadhi as Prabha/Ayyanar
* Meera Nandan as Anitha
* Vishnupriyan as Saravanan Santhanam
* Arpit Ranka as Ramani
* Rajashree
* Mahadevan
* Jayaprakash
* Anupama Kumar Soori

==Plot==
Prabha (Aadhi) is the elder son of the family and he does nothing but spending all his time with his friends. He is chided by his father for that. Being a volleyball player comes to his rescue as he finds a part-time job as a volleyball coach in a womens college.

His younger brother works in a television channel and Prabha is often compared to him by the family, which incurs his wrath. The siblings cross swords with each other. One day the younger brother is killed and the blame falls on Prabha.

He is on the run and soon joins a goonda working as a henchman to an influential politician (Mahadevan). Prabha goes on a killing spree killing one by one in the gang. Finally a flashback reveals that they were the reason for his brothers death.

==Production==
Aadhi suffered a minor injury on his knee, while a fight scene was being picturised on the terrace of an under-construction apartment building, near the Gemini Flyover in Chennai.

The fight was to be choreographed in the night effect as per the script. Fight master Super Subbarayan choreographed this stunt featuring the hero (Aadhi) fighting the three villains of the film Ravi Kale, Pithamagan Mahadevan and Arpit. 

==Soundtrack==
{{Infobox album|  
| Name = Ayyanar
| Type = Soundtrack
| Artist = Thaman
| Cover = 
| Released = 14 June 2010
| Recorded = 2009-2010 Feature film soundtrack
| Length = 
| Label = Sri Rajalakshmi Films
| Producer = Thaman
| Last album = 
| This album = 
| Next album = 
}}

Film score and the soundtrack are composed by Thaman. "Aathadi Aathadi" received a high response from audience.
{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 
| title1          = Kuthu Kuthu
| extra1          = Aalaap Raju, Bruce Lee, Vel Murugan
| length1         = 
| title2          = Paniye Paniye
| extra2          = Ranjith (singer)|Ranjith, Priyadharshini
| length2         = 
| title3          = Pachai Kili
| extra3          = Rahul Nambiar
| length3         = 
| title4          = Aathadi Aathadi
| extra4          = Naveen
| length4         = 
| title5          = Theme
| extra5          = Thaman
| length5         = 
}}

==Reviews==
The film received mixed reviews.  Behindwoods wrote:"Ayyanar stands in the passable league that can gratify the audiences, who are least, bothered about logics".  Sify wrote:"On the whole, Ayyanar is a mass film that is found wanting in tempo and packaging". 

==References==
 

==External links==
*  

 
 
 
 