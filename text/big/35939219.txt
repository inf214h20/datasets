The Robber Bride (film)
{{Infobox film
| name           = The Robber Bride
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Oskar Messter
| writer         =  Robert Wiene   
| narrator       = 
| starring       = Henny Porten   Friedrich Feher   Artur Menzel
| music          =  Giuseppe Becce   
| editing        = 
| cinematography = Karl Freund
| studio         = Messter Film
| distributor    = Hansa Film
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent comedy film directed by Robert Wiene and starring Henny Porten, Friedrich Feher and Artur Menzel. A young woman with romantic ideas rejects the arranged marriage her parents want for her, dreaming instead of marrying a bandit. 

==Cast==
* Henny Porten
* Friedrich Feher
* Artur Menzel
* Karl Elzer

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 