Pellichesukundam
{{Infobox film
| name           = Pellichesukundam
| image          = Pellichesukundamposter.jpg
| image_size     =
| caption        =
| director       = Muthyala Subbaiah
| producer       = C Venkat Raju G Siva Raju
| writer         = Posani Krishna Murali  (dialogues) 
| story          = Bhupathi Raja
| narrator       = Venkatesh Soundarya Laila
| Koti
| cinematography = K.Ravindra Babu
| editing        = Gautham Raju
| studio         = Geetha Chitra International
| distributor    =
| released       =  
| runtime        = 2:20:48
| country        = India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Telugu Cinema film directed Laila played the lead roles and music composed by Saluri Koteswara Rao|Koti.The film was Super Hit at the box office. 

==Plot== Venkatesh is Laila shows up. She is in love with him, and their parents want them to get married. The rest of the story is whom will Venkatesh marry and how. 

==Cast==
{{columns-list|3| Venkatesh as Anand
* Soundarya as Shanti Laila as Laila
* Mohan Raj  Devan 
* Satya Prakash 
* Balap Singh 
* Raman Panja 
* Brahmanandam  Sudhakar 
* Tanikella Bharani  Venu Madhav 
* Narra Venkateswara Rao 
* Posani Krishna Murali 
* Subhalekha Sudhakar 
* Subbaraya Sharma 
* Suthi Velu 
* Kallu Chidambaram 
* Kishore Rathi  
* Chandra Mouli 
* Gadiraju Subba Rao 
* Echuri Annapurna  Sumitra 
* Rajitha
* Ragini
* Aswini 
* Annuja 
* Radha Prashanthi 
* Lata 
* Sabita
* Nallini  Master Mahendra 
* Baby Soumya
}}

==Soundtrack==
{{Infobox album
| Name        = Pellichesukundam
| Tagline     =
| Type        = film Koti
| Cover       =
| Released    = 1997
| Recorded    =
| Genre       = Soundtrack
| Length      = 28:25
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Priya O Priya   (1997)
| This album  = Pellichesukundam   (1997)
| Next album  = Evandi Pelli Chesukondi   (1997)
}}

Music composed by Saluri Koteswara Rao|Koti. All songs are hit tracks. Music released on SUPREME Music Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:25
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = O Laila Laila
| lyrics1 = Bhuvanachandra  SP Balu, Chitra 
| length1 = 4:37

| title2  = Kokila Kokila
| lyrics2 = Sai Sri Harsha 
| extra2  = SP Balu,Chitra
| length2 = 5:03

| title3  = Nuvvemi Chesavu Sirivennela Seetarama Sastry
| extra3  = K. J. Yesudas
| length3 = 4:48

| title4  = Enno Enno
| lyrics4 = Sirivennela Seetarama Sastry
| extra4  = SP Balu,Chitra
| length4 = 4:20

| title5  = Manasuna Manasai Chandrabose
| extra5  =  SP Balu,Chitra
| length5 = 5:15

| title6  = Ghuma Ghumalade
| lyrics6 = Chandrabose
| extra6  =  SP Balu,Chitra
| length6 = 4:16
}}

==Remakes==
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f21f21f21; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Year !! Film !! Language !! Cast !! Notes
|-
| 1998 || En Uyir Nee Thaane || Tamil || Prabhu (actor)|Prabhu, Devayani (actress)|Devayani, Maheswari ||
|-
| 1999 ||  Sudhu Ekbar Bolo|| Bengali || Prosenjit Chatterjee, Rituparna Sengupta, Mouli Ganguly || 
|-
| 2001 || Hamara Dil Aapke Paas Hai || Hindi || Anil Kapoor, Aishwarya Rai, Sonali Bendre ||
|-
| 2001 || Maduve Aagona Baa || Kannada || Shivrajkumar, Laya (actress)|Laya, Shilpa Shetty ||
|-
|}

==Audio==
 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- align="center"
! style="background:#B0C4DE;" | No
! style="background:#B0C4DE;" | Song title
! style="background:#B0C4DE;" | Singers
|- Chitra
|-
|2|| "Ghuma Ghuma Laade" ||S. P. Balasubrahmanyam, Chitra
|-
|3|| "Kokila Kokila" || S. P. Balasubrahmanyam, Chitra
|-
|4|| "Manasuna Manasai" || S. P. Balasubrahmanyam, Chitra
|-
|5|| "Nuvvemi Chesavu" || Yesudas 
|-
|6|| "Oh Laila Laila" ||S. P. Balasubrahmanyam, Chitra
|}

== References ==
 

==External links==
*  

 

 
 
 
 

 