Enga Ooru Rasathi
{{Infobox film
| name           = Enga Ooru Rasathi
| image          =
| caption        =
| director       = N. S. Rajendran
| producer       = N. S. Rajendran
| writer         =
| screenplay     = N. S. Rajendran
| story          = P. Kalaimani Sudhakar Raadhika Radhika Goundamani S. R. Vijaya
| music          = Gangai Amaran
| cinematography = G. Thyagu
| editing        = M. Vellaichamy M. Kesavan
| studio         = Ravi Combines
| distributor    = Ravi Combines
| released       =  
| runtime        = 105min
| country        = India Tamil
}}
 1980 Cinema Indian Tamil Tamil film, directed and produced by N. S. Rajendran. The film stars Betha Sudhakar|Sudhakar, Raadhika Sarathkumar|Radhika, Goundamani and S. R. Vijaya in lead roles. The film had musical score by Gangai Amaran.  

==Cast==
  Sudhakar
* Radhika
* Goundamani
* Gandhimathi in Guest appearance
* S. R. Vijaya
* Latha
* C. K. Saraswathi
* Ambika
* Sethuraja
* Prabhakar
* G. Krishnamurthy
* Selvaraj
* C. R. S. Murthy
* Thirumurthy
 

==Soundtrack==
The music was composed by Gangai Amaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ponmaanai Thedi || Malaysia Vasudevan, S. P. Sailaja || Kannadasan || 04.18
|}

==References==
 

==External links==
*  

 
 
 
 


 