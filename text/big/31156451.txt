Swapnam
{{Infobox film 
| name           = Swapnam
| image          =
| caption        =
| director       = Babu Nanthankodu
| producer       = Sivan
| writer         = P Kesavadev Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Madhu Nanditha Sudheer Jose Prakash
| music          = Salil Chowdhary Ashok Kumar
| editing        = Ramesh
| studio         =
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, Sudheer and Jose Prakash in lead roles. The film had musical score by Salil Chowdhary.   

==Cast==
  Madhu as Viswanathan
*Nanditha Bose as Gauri Sudheer as Bindu
*Jose Prakash
*Adoor Bhavani
*Aranmula Ponnamma as Viswanathans Mother
*Aryad Gopalakrishnan
*Balan K Nair
*Kottarakkara Sreedharan Nair
*Radhamani
*Rani Chandra
 

==Soundtrack==
The music was composed by Salil Chowdhary and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maane Maane || K. J. Yesudas || ONV Kurup || 
|-
| 2 || Mazhavilkodi(Kaanakkuyile) || S Janaki || ONV Kurup || 
|-
| 3 || Nee Varu Kaavyadevathe || K. J. Yesudas || ONV Kurup || 
|-
| 4 || Shaarike En Shaarike || S Janaki || ONV Kurup || 
|-
| 5 || Sourayoodhathil || Vani Jairam || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 