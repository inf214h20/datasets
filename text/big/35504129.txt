The End of the Golden Weather
 
 
The End of the Golden Weather is a play by Bruce Mason about a boys loss of innocence in Great Depression|Depression-era New Zealand. It was written for solo performance by the author but can be performed by an ensemble and was made into an award-winning feature film directed by Ian Mune in 1991.  It was workshopped in 1959 and first performed for the public in 1960. The script was published in 1962 and again in 1970 after Mason had performed it more than 500 times. Mason, Bruce The End of the Golden Weather A voyage into a New Zealand childhood, New Zealand University Press and Price Milburn, Wellington, 1962, 95pp, SBN 7055 0010 1  In 1963 he performed it at the Edinburgh Festival.
 North Shore in the 1930s, the main character is a nameless boy of about 12 (called Geoff in the film), based on the young Mason and a man from his childhood.

The play is in four parts:
:Sunday at Te Parenga
:The night of the riots
:Christmas at Te Parenga
:The made man
 broom flowers bach (shack) at the end of summer.

Mason took the plays title from that of a novel the narrator in Thomas Wolfes The Web and the Rock had wanted to write (  p 12). It has become a cliche in New Zealand for the end of summer.

== The End of the Golden Weather (1991 film) ==
The 1991 film was directed and co-produced by Ian Mune, who also wrote the screenplay with the cooperation of Bruce Mason, although the film was finally made after Mason’s death. The film, 104 minutes long, was made with a budget of $NZ3 million, and shot on Te Muri Beach, Takapuna beach and Takapuna Grammar School. Helen Martin says that the film keeps to the spirit of the play, although Mune chose to leave out the Depression aspects (e.g. the 1932 Queen Street riots) and concentrate on the Geoff and Firpo story. 

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 