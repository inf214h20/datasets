Quest for Zhu
{{infobox film
| name           = Quest for Zhu
| image          = 
| image_size     = 
| caption        = 
| director       = Bob Douchette
| producer       = Laura Kurzu Bob Douchette Ashley Hornsby Meghan Hornsby Sean Derek
| based on       = Zhu Zhu Pets Ian Corlett Jillian Michaels Jan Rabson Kathleen Barr
| music          = Michael Tavera
| cinematography = 
| editing        = Richard Finn Cepia LLC The Dream Garden Company Prana Animation Studios Universal Studios Home Entertainment
| released       =  
| runtime        = 73 minutes
| country        = United States   Canada
| language       = English
}}
 adventure family Jillian Michaels, Jan Rabson, and Kathleen Barr. The film was released on DVD September 27, 2011, then aired on Nickelodeon on December 4, 2011.

==Synopsis== Ian Corlett). On their journey, they must do whatever it takes to overcome obstacles, find Zhu Fu (Jan Rabson), and defeat Mazhula (Kathleen Barr).

==Cast==
*Mariah Wilkerson as Katie, Pipsqueaks owner
*Shannon Chan-Kent as Pipsqueak, one of the films main protagonists  Ian Corlett as Mr. Squiggles, Stinker, and Zhuquasha
**Mr. Squiggles: one of the films main protagonists
**Stinker: the films secondary antagonist
**Zhuquasha: a large arctic mountain beast
*Sean Campbell as Chunk, one of the films protagonists
*Erin Mathews as Num Nums and Surfer
**Num Nums: one of the films main protagonists
**Surfer: a supporting Zhu character Jillian Michaels cheer leader
*Jan Rabson as Zhu Fu and Mangawanga
**Zhu Fu: the ruler of Zhu overthroned by Mazhula tribal Zhu character 
*Kathleen Barr as Mazhula, the films main antagonist

==Marketing==

===Soundtrack===
 
The official "Quest for Zhu" soundtrack was released on November 21, 2011. 

===Video game===
Coinciding with the films release also on September 27, a Nintendo DS video game adaptation of the film was released in stores.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 

 