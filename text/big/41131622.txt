Ganesh Talkies
{{Infobox film
| name           = Ganesh Talkies
| director       = Anjan Dutt   Times of India 
| producer       = Reliance Entertainment 
| writer         = Anjan Dutt Rajesh Sharma Raima Sen Chandan Roy Sanyal Pallavi Chatterjee 
| music          = Neel Dutt 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 21 June 2013 (India)   IMDB 
| runtime        = 2:11:38
| country        = India Bengali
| budget         =  INR 10,000,000 (estimated) 
| gross          = 
}}

Ganesh Talkies is a 2013 Cinema of Bengal | Bengali film written and directed by Anjan Dutt. The movie is produced by Reliance Entertainment.  It portraits two families of polar opposite Bengali and Marwari communities.

==Cast==
* Biswajit Chakraborty Rajesh Sharma
* Raima Sen
* Chandan Roy Sanyal
* Ekavali Khanna
* Pallavi Chatterjee
* Koneenica Banerjee
* Taranga Sarkar
* Rita Koiral

==Plot Summary==
 
A family drama based on two families belonging to the polar opposite Bengali and Marwari communities. The film centers on childhood friendship, a probable wedding and above of all the triumph of young love, Pashupati Ghosh the patriarch of the Bengali family lives gregariously but still in denial of his waning wealth. Whereas his neighbour Praveen Agarwal although has amassed a great deal of wealth over the course of years, lives a miserly life. Differences of opinions, scheming family members, shifting cultural dynamics and a secret love that has blossomed despite of all odds forms the crux of this film, the love between Saban and Arjun is one where Chana plays the messenger delivering clandestine notes from one lover to another. Drama unfolds when both the families come to know about Saban, Arjun, and their plan to elope. Just when Saban is to be married to a U.S. based computer engineer Niky. Underlying tensions erupt and bubble to the surface, each family blames the other. Amidst this entire ruckus, Saban is locked inside her room and attempts suicide. She is rushed to the hospital where they refuse to let Arjun visit her. On the other hand, Pushupati is constantly threatened by the local goon Krishnagopal Das to vacate his property. Navin the brother of Praveen Agarwal who has had his eyes set on the property for a long time is carrying out this plan. Arjun intervenes and is mortally injured, he is rushed to the I.C.U. where immediate blood in needed. Pashupati willingly steps in to donate blood and their blood groups miraculously match each other. In the process, Pashupati saves Arjuns life. The two families eventually unite and it seems that a wedding is in store after all!

== Music ==
The music of this movie is composed by Neel Dutt.

Track List:
{{tracklist
| all_music       = 
| all_lyrics      = 
| extra_column    = Artist(s)

| title1          = Jhal Legechey
| extra1          = Rupankar Bagchi, Ujjaini
| length1         = 03:53

| title2          = Shedin Dujoney
| extra2          = Rupankar Bagchi
| length2         = 04:25

| title3          = Ja Hobar Ta Hobei Hobe
| extra3          = Anjan Dutt
| length3         = 03:46

| title4          = Yeh Zindagi
| extra4         = Arko Mukherjee, Neel Dutt
| length4         = 03:34

| title5          = Yeh Zindagi (Remix)
| extra5          = Arko Mukherjee, Neel Dutt
| length5         = 03:38
}}

==References==
 

== External links ==
# IMDB:  
# Ganesh Talkies - Press conference:  

 

 
 