Mother Küsters' Trip to Heaven
{{Infobox film
| name           = Mother Küsters Trip to Heaven
| image          = Mother Küsters Trip to Heaven.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Rainer Werner Fassbinder
| producer       = 
| writer         = Rainer Werner Fassbinder, Kurt Raab, Heinrich Zille
| narrator       = 
| starring       = Brigitte Mira Ingrid Caven Karlheinz Böhm, Margit Carstensen  
 Armin Meier  Irm Hermann
| music          = Peer Raben
| cinematography = Michael Ballhaus
| editing        = Thea Eymesz
| studio         = 
| distributor    = Tango Film
| released       = 2 January 1976 Braad Thomsen, Fassbinder,  p. 332 
| runtime        = 103 minutes 
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Mother Küsters Trip to Heaven ( ) is a 1975 German film written and directed by Rainer Werner Fassbinder. It stars Brigitte Mira, Ingrid Caven, Karlheinz Böhm and Margit Carstensen. The film was shot over 20 days between February and March 1975 in Frankfurt am Main. 

==Synopsis== layoff announcements.

A group of reporters take advantage of the grieving Mother Küsters to sensationalize the deaths. Finding no solace from her son Ernst (Meier), daughter-in-law Helene (Irm Hermann), who promptly go on holiday, or daughter (Ingrid Caven|Caven), Küsters turns to Karl and Marianne Thälmann (Karlheinz Böhm|Böhm and Margit Carstensen|Carstensen), two members of what turns out to be the German Communist Party (DKP). They introduce themselves at Hermanns funeral, and invite her to their home, which Marianne had inherited.

The Communists see Küsterss husband as a revolutionary and a misguided victim of capitalism, but she is initially unpersuaded; her husband saw communists as troublemakers. Her daughter Corinna advises her mother to have nothing to do with them, and points out the differing conditions enjoyed by the authorities and the people in the East Germany|East. An article on the tragedy by Niemeyer (Gottfried John), a photojournalist who had earlier shown a particular interest in the family, appears in a magazine. Emma finds the article objectionable, but her daughter, who has embarked on an affair with Niemeyer, defends him on earning a living grounds. At the factory, Emma Küsters finds that the company pension scheme will not apply in her case; the Works council|workers council and the company board are at one on the issue. Her daughter leaves, and Ernst and Helene, newly returned from holiday, announce they are to set up home on their own. Helene, expecting a child, does not get along with her sister-in-law.

Emma Küsters now joins the Communist Party, having found Karls newspaper article more sympathetic, but after Küsters speaks at her first DKP political gathering she meets a young male paper seller who claims to really have her interests at heart in clearing her husbands name. He gives her his contact details. She quickly grows impatient with the communists passive tactics; they have to campaign in the forthcoming elections Karl explains. She connects with a small group of anarchism|anarchists, who, though even smaller in number than the communists, claim to have more spirit.

There are two endings to the film:
*The anarchists leading member, Horst Knab, demands to see Niemeyer at the magazines offices but the secretary (Lilo Pempeit, Fassbinders mother) says he is unavailable. The editor suggests Küsters send in a letter which he might publish. The anarchists take the staff hostage at gunpoint, including Niemeyer, now living with Corinna, who arrives unaware how far the situation has developed. The anarchists demand the release of all political prisoners in West Germany, a Mercedes 600 to take them to the airport and a Boeing 707 to take them out of the country. Küsters looks horrified at the anarchists demands but is killed in a subsequent clash with the police. Knab kills Linke (the editor) but Knab is also shot. (The last stages of the narrative are detailed with captions.)
*In another ending (primarily used for the American edition), Niemeyer is at the magazine offices but as the staff start to leave at the end of their working day, the two anarchists grow bored with the sit-down strike and leave with Küsters remaining. After being told by her daughter Corinna, clearly now living with Niemeyer who has phoned her, that she is making herself ridiculous, Emma Küsters meets the friendly janitor of the newspapers offices, whose wife is dead. The widow and widower leave to have dinner together, apparently beginning a romantic relationship. 
 media in The Lost German Communist armchair activism".

== Cast ==
*Brigitte Mira – Emma Küsters
*Ingrid Caven – Corinna Corinne,  aka Corinna Küsters
*Armin Meier – Ernst Küsters
*Irm Hermann – Helene Küsters
*Karlheinz Böhm – Karl Tillmann 
*Margit Carstensen – Frau Marianne Tillmann
*Gottfried John – Niemeyer
*Matthias Fuchs – Horst Knab

==Notes==
 

==Bibliography==
* Braad Thomsen, Christian,  Fassbinder: Life and Work of a Provocative Genius, University of Minnesota Press, 2004, ISBN 0-8166-4364-4

==External links==
* 
* 

 

 
 
 
 
 
 