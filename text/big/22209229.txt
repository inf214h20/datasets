Searching for Sonny
{{Infobox film
| name = Searching for Sonny
| caption =
| image = Searching for Sonny FilmPoster.jpeg
| director       =  Andrew Disney
| producer       = Greg Beauchamp Red Sanders
| writer         =  Andrew Disney
| screenplay     = 
| story          = 
| based on       =   Michael Hogan Nick Kocher Brad Sham Clarke Peters Jared Knight Michael Kagan  Richard Folmer 	
| narrator       = 
| music          = Alice Wood
| cinematography = Jeffrey Waldron
| editing        = Sam Parnell
| studio         = Red Productions
| distributor    = FilmBuff
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Searching for Sonny is a 2011 independent film and the feature film debut of writer/director Andrew Disney.    The film released on October 22, 2011 and stars Jason Dohring as one of several people involved in a murder mystery.  

==Plot==
Elliott (Jason Dohring), Calvin (Nick Kocher), Gary (Brian McElhaney) and Eden (Minka Kelly), find themselves chief suspects in a murder mystery at their ten-year high school reunion. Ironically, the events surrounding the disappearance of their friend Sonny (Masi Oka), is reminiscent of a high school play they once performed, coincidentally written by Sonny himself. Deception leads to scandal and the truth surfaces as the friends learn that shady businessmen and school officials are involved in a complex scheme of money and murder.

==Cast==
* Minka Kelly as Eden Mercer
* Jason Dohring as Elliot Knight
* Masi Oka as Sonny Bosco Michael Hogan as Principal Faden Nick Kocher as Calvin Knight Brian McElhaney as Gary Noble
* Clarke Peters as The Narrator
* Mackinlee Waddell as Maggie Phillips
* Brandon Thornton as Zack Hayes
* Kristin Sutton as Cynthia Schubert

==Reception==
Critical reception for Searching for Sonny has been mostly positive and The Hollywood Reporter summed the film up as "Quirky, comic film noir proves yet again that high school reunions never turn out well."   Much of the films praise centered around the films acting and directing,   and the Oklaholma Gazette commented that Disneys work helped to make the film look "like several million dollars were poured into the thing".    

===Awards===
*Best Screenplay at the Phoenix Film Festival (2012, won)    
*Best Ensemble at the Phoenix Film Festival (2012, won) 
*Best Narrative Feature at the Festivus Film Festival (2012, won) 
*Best Feature Narrative at the Boomtown Film and Music Festival (2012, won)  
*Best In Show at the Boomtown Film and Music Festival (2012, won)  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 