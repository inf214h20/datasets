Skinwalker Ranch (film)
{{Infobox film
| name           = Skinwalker Ranch
| image          = SkinwalkerRanchFilmPoster.jpg
| alt            = 
| caption        = 
| director       = Devin McGinn
| producer       = 
| writer         = Adam Ohler
| starring       = Taylor Bateman, Steve Berg, Michael Black
| music          = 
| cinematography = Michael Black
| editing        = 
| studio         = DeepStudios
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} UK as Skinwalkers) is a 2013 sci-fi horror film that was directed by Devin McGinn. The movie had a video on demand and limited theatrical release on October 30, 2013 and stars Taylor Bateman, Steve Berg, and Michael Black.  It is loosely based upon folklore surrounding the titular Utah|Utah-based Skinwalker Ranch, which is rumored to be the site of several UFO sightings.  

==Synopsis== Native American man, who tells the crew that their lives are in danger.

==Cast==
*Taylor Bateman as Rebecca
*Steve Berg as Sam
*Michael Black as Britton Sloan
*Erin Cahill as Lisa
*Carol Call as Lissie Miller
*Kyle Davis as Ray Reed
*Mike Flynn as Interviewee
*Jon Gries as Hoyt
*Michael Horse as Ahote
*Nash Lucas as Cody
*Devin McGinn as Cameron Murphy
*Matthew Rocheleau as Matt
*Anne Sward as Interviewee
*Tobijah Tyler as Interviewee
*Clint Vanderlinden as Interviewee

==Reception==
Critical reception for Skinwalker Ranch was mixed to negative.  Common criticism centered around the plot,  which Shock Till You Drop considered to be due to the over familiarity of films of this nature and of movies purported to be based on true events.  Libertas Film Magazine and the Salt Lake Tribune both gave positive reviews for Skinwalker Ranch,  with the Tribune praising the movies visual-effects work.  Dan Callahan of RogerEbert.com gave a mixed review, remarking that "There is no point in "Skinwalker Ranch" when it seems as if this could actually be found footage, but it does garner a few scares and even a few honest laughs in its rather short running time. If you’re looking for a neat little Halloween movie, you could do worse." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 