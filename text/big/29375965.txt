Mon pote le gitan
{{Infobox film
| name           = Mon pote le gitan
| image          = 
| caption        = 
| director       = François Gir Guy Lionel
| producer       =  Floralies Films, C.I.C.C, Licorne Films (France)
| writer         = Alain Blancel Guy Lionel Michel Duran
| starring       = Jean Richard Louis de Funès
| music          = Marc Héral
| cinematography = 
| editing        = 
| distributor    = C.C.F.C.
| released       = 2 December 1959 (France)
| runtime        = 87 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1959, directed by François Gir, written by Alain Blancel, starring Louis de Funès. In Italy the film is known under the title: "Il dottor zivago".  the scenario was written on the basis of "Les Pittuitis" of Michel Duran.

== Cast ==
* Jean Richard : M Pittuiti, the Gypsy
* Louis de Funès : M. Védrines, editor
* Grégory Chmara : Le "Pépé"
* Michel Subor : Bruno Pittuiti, the son of the Gypsy
* Guy Bertil : Théo Védrines, the son of the editor
* Lila Kedrova : La "Choute"
* Brigitte Auber : Odette, la bonne
* Simone Paris : Mrs Védrines
* Anne Doat : Gisèle Védrines, daughter
* Joseph Reinhardt : the musician
* Thérésa Dmitri : Zita Pittuiti, daughter
* Jacqueline Caurat : the reporter
* Luce Fabiole
* Jacques Verrières
* Odile "Maguy" Poisson
* Robert Destain
* François Bonnefois
* Eliane Martel
* Monique Dagand

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 


 