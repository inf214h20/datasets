MacGruber (film)
 
{{Infobox film
| name = MacGruber
| image = Macgruber poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Jorma Taccone
| producer = Lorne Michaels John Goldwyn Hugh Jackman John Solomon Jorma Taccone
| starring = Will Forte Kristen Wiig Ryan Phillippe Powers Boothe Maya Rudolph Val Kilmer 
| music = Matthew Compton
| cinematography = Brandon Trost
| editing = Jamie Gross Rogue Pictures Goldwyn
| Universal Pictures
| released =   
| runtime = 99 minutes  
| country = United States
| language = English
| budget = $10 million   
| gross = $9,322,895 
}} action comedy sketch of the same name, itself a parody of action-adventure television series MacGyver. Jorma Taccone of the comedy trio The Lonely Island directed the film, which stars Will Forte in the title role; Kristen Wiig as his love interest/partner, Vicki St. Elmo; Ryan Phillippe as Dixon Piper, a young lieutenant who becomes part of MacGrubers team; Maya Rudolph as MacGrubers dead wife, Casey; and Val Kilmer as the villain, Dieter von Cunth.

The film, released on May 21, 2010 after being pushed from its original April 23 date, was a box office bomb, grossing $9.3 million worldwide on a $10 million budget. It divided critics, who either commended or loathed the films use of toilet humor. Despite this, it has since been labeled a cult classic.

==Plot== Green Beret, Navy SEAL Army Ranger MacGruber in Ecuador. The two men find MacGruber meditating in a chapel, and try to convince him to return to the United States in an effort to retrieve the warhead. MacGruber refuses; later that night, MacGruber explodes into a fit of rage after a flashback where Cunth killed his fiancée, Casey Fitzpatrick, at their wedding; he then accepts the Colonels offer.

After arriving at The Pentagon and having a heated conversation with Faith and Piper, MacGruber decides he will form his own team to pursue Cunth, declining the offer to build a team around Piper. MacGruber successfully recruits all but his long-time friend Vicki St. Elmo and Brick Hughes - MacGruber initially recruits Hughes, but upon discovering Brick is Homosexuality|gay, crosses his name off his list. MacGruber and his team meet Faith and Piper on a tarmac. Upon being asked where his team is, MacGruber responds that they are in the van along with his homemade C-4 (explosive)|C-4 explosives. The van promptly explodes, killing the team. MacGruber is distraught over the loss of the team and is promptly removed from the Cunth case. In a one-on-one conversation with Piper, MacGruber convinces him to form a new team. Vicki also arrives, completing MacGrubers team.
 Las Vegas. MacGruber gets on stage and announces who he is, his intentions, and where he will be the next day. The team sets up a sting operation with Vicki portraying MacGruber. Hoss Bender, one of Cunths henchmen, attacks the van MacGruber and Piper are in. MacGruber tells Piper to pass him an Incredi-Mop, which he uses to turn the ignition key and hit the gas pedal, running down Bender. With Vicki disguised as Bender and Piper disguised as MacGruber, the team breaks into a warehouse to stop von Cunth from getting the passwords to operate the rocket. MacGruber distracts the guards by walking around naked with a piece of celery clenched between his buttocks. Piper manages to kill most of the men inside, but is unable to stop the transfer of the pass codes. MacGruber and the team go to a charity event Cunth is holding. After a heated conversation, Cunths guards throw MacGruber out.

After the fiasco, MacGruber returns to the Pentagon where Faith reprimands him. MacGruber and Piper relax and drink after being taken off the case. Soldiers attack but MacGruber uses Piper as a human shield to survive; Vicki and MacGruber escape. Piper survives due to the fact that he was wearing a bulletproof vest, but leaves, disgusted that MacGruber used him as protection. Vicki and MacGruber return to Vickis house where the two have noisy sex. MacGruber goes to his wifes grave in shame, feeling that he has betrayed her memory, but her ghost gives her blessing to allow MacGruber to pursue Vicki. MacGruber then has sex with the ghost of his wife on her tombstone.

Upon returning to Vickis house, MacGruber discovers that Cunth kidnapped her, and realizes what Cunths plan is: to bomb the State of the Union address. Cunth calls MacGruber to gloat, but MacGruber traces the call. MacGruber meets up with Piper to save Vicki. The two men make their way into Cunths compound, in large part due to MacGrubers propensity for ripping throats. Soldiers capture MacGruber and Piper and bring them to Vicki and the missile. The group manages to overpower Cunth and his men and MacGruber handcuffs Cunth to a handrail. MacGruber removes the nuclear component and guidance system before his team escapes as the missile explodes.

Six months later, MacGruber and Vicki are getting married. Also present at the wedding as ghosts are his dead team members Vernon Freedom, Tug Phelps, Tut Beemer, and Tanker Lutz. Out of the corner of his eye, MacGruber spots a disfigured Cunth, thought dead, with an Rocket-propelled grenade|RPG. MacGruber saves Vicki, and battles Cunth before throwing him off a cliff behind the altar, shooting him with a machine gun and launching a grenade as he falls, incinerating the corpse, and finally urinating on it at the foot of the cliff.
 After the credits, MacGruber sits in a tree, playing a saxophone.

==Cast==
 
* Will Forte as MacGruber
* Kristen Wiig as Vicki Gloria St. Elmo
* Ryan Phillippe as Lt. Dixon Piper
* Powers Boothe as Col. Jim Faith
* Val Kilmer as Dieter Von Cunth
* Maya Rudolph as Casey Janine Fitzpatrick
* Chris Jericho as Frank Korver
* Montel Vontavious Porter as Vernon Freedom
* The Great Khali as Tug Phelps The Big Show as Brick Hughes
* Mark Henry as Tut Beemer Kane as Tanker Lutz
* Andy Mackenzie as Hoss Bender
* Derek Mears as Large henchman
* Rhys Coiro as Yerik Novikov
* Jasper Cole as Zeke Pleshette
* Kevin Skousen as Senator Garver
* Brandon Trost as Bricks boyfriend
* Jason Trost as Smoking guard
* Amare Stoudemire as himself
* Chris Kittinger as Man in tux
* DJ Nu-Mark (uncredited) as Club DJ
 

==Production==
{{Quote box quote  = What you see with this movie is exactly what we wanted to do. It’s the three of us having a bunch of fun writing it, then having fun making it with a bunch of our friends—old friends and new friends. I think that fun comes across when you watch it. It’s rare that you get that kind of creative freedom. source = Will Forte to The A.V. Club, 2010    quoted = 1 width  = 25% align  = right
}}
MacGruber is based on a recurring sketch on Saturday Night Live (SNL), a parody of the television series MacGyver. It was created by writer Jorma Taccone, who pitched the idea to cast member/writer Will Forte over a period of several weeks.  Forte was initially reluctant to commit to the sketch, deeming it too dumb, but accepted after persuasion from Taccone.  The first sketch aired in January 2007, and led to multiple more segments in the following years. In 2009, the sketches were spun off into a series of commercials sponsored by Pepsi premiering during Super Bowl XLIII that featured the actor behind MacGyver, Richard Dean Anderson, as MacGrubers father. The advertisements led the character and sketches to receive a wider level of popularity.   

Following the success of the advertisements, creator Lorne Michaels approached Forte, Taccone, and writer John Solomon with the idea to produce a MacGruber film, and they were at first skeptical. They began pitching ideas for a potential feature-length adaption, deciding first and foremost it would not be the sketch repeated for its entire runtime.    The films central conceit was to produce a real action film, with MacGruber as comic relief.    It was inspired by their love of 1980s–90s action films, such as Lethal Weapon, Rambo (film series)|Rambo, and Die Hard. To this end, it is considerably more deadpan in its parody, with most characters outside MacGruber designed "as serious as possible."  The most difficult part of the writing process was to portray MacGruber—an "insanely flawed   narcissistic" individual—as a likable lead character.  
 production process left the trio deprived of sleep. This led to sequences deemed "crazier," such as a scene involving celery.  There was concern the film might receive an NC-17 rating from the Motion Picture Association of America, which could lead to certain commercial disappointment.    The script was written in five weeks by Taccone and Forte,    with a first draft coming in at 177 pages and lacking a third act. It was subsequently re-written under budgetary concerns, which mostly involved deleting scenes containing special effects.  Forte praised the level of creative freedom afforded to the filmmakers, noting that even the more outrageous jokes were allowed to remain in the final film.  

Following a short six week period of preparation,  the film was shot between August 10 and September 13, 2009 in Albuquerque, New Mexico|Albuquerque, New Mexico.  The film shoot employed a crew composed of local workers as a part of a tax credit.    The quick film schedule of 28 days led to the filmmakers adjusting scenes to complete them on time, as the presence of automatic weapons on set would slow down the process. Keeping in line with their sources for parody, the filmmakers opted for cinematography emulating the style of blockbuster action films. This involved most prominently using smoke machines for interior areas, as they noticed similar scenes in Lethal Weapon inexplicably contain prominent smoke.   
 WWE wrestlers The Big MVP and The Great Khali and actor Derek Mears, were later confirmed.  

Although the film had a release date of May 21, 2010, the film was originally scheduled for an April 23 release. 

===Legal disputes=== film version of the TV series. In 2010, his lawyer sent several cease-and-desist letters and met with litigators to determine a course of action. No suit was brought.   

==Marketing== red band green band trailer was released. 
 hosted WWE Raw from the Izod Center in East Rutherford, New Jersey in character to promote the film. 

Phillippe guest starred on Saturday Night Live on April 17, 2010 and made reference to the films shooting in his opening monologue. 

==Reception==
===Critical response===
The film received mixed reviews, citing the crude humor and vulgarity. Review aggregation website Rotten Tomatoes gives the film a score of 47% based on 123 reviews, with an average score of 5.1/10. The sites consensus concludes the film "too often mistakes shock value for real humor, but MacGruber is better than many SNL films – and better than it probably should be."  
Metacritic, another review aggregator, assigned the film a weighted average score of 43% based on 21 reviews from mainstream critics. 

John DeFore of The Hollywood Reporter felt it "utterly disposable but diverting."    Peter Travers of Rolling Stone praised the "unabashed affection" of Taccones directorial style while commending Fortes performance as "contagious."    The Boston Globe  Ty Burr deemed it "a lot better than it should be," while criticizing its "smugness" and abundance of toilet humor. {{citenews|url=http://www.boston.com/ae/movies/articles/2010/05/21/macgruber_displays_flickering_wit_when_it_goofs_on_reagan_era/|title=‘MacGruber’ displays flickering wit when it goofs on Reagan era
|work=The Boston Globe|author=Ty Burr|date=May 21, 2010|accessdate=April 13, 2015}}  In contrast, Andrew Schenker of Slate Magazine felt that "MacGruber  at its best when its most vulgar, when its foul-mouthed and essentially insane hero is free to indulge in his signature bits of raunchy whimsy."   

A.O. Scott of   was similarly negative, commenting, "Only the merest hint of amusement is to be found in this uninspired latest effusion from the conveyor belt that is Saturday Night Live."    Entertainment Weekly  Lisa Schwarzbaum called it a "naughty throwaway in all senses of the word throwaway-90 minutes of talented performers doing and saying dumb, crude stuff in pursuit of an elusive laugh."   
 IGN UK gave it 3 out of 5 stars saying "When the film is funny, its very funny."  
Jon Peters of KillerFilm gave it 3 out of 5 stars saying "Its consistently funny and it didnt need gray tape to do it. Its funny in the old Airplane! humor, mixed with a little Mel Brooks, type of way   But none of this would work, if it wasnt for Will Fortes brilliant blend of witless charm and dumb ass heroics." 
 Hot Rod "may be just as poorly received, but their rhythms are unpredictable and exciting, shocked to life by moments of anti-comedy and wacky deconstruction. Hardcore comedy devotees pick up on them like a dog whistle."    Matt Singer, in a retrospective review for The Dissolve, deemed it a "cult favorite in the making," remarking, "MacGruber stands out by defying every rational commercial impulse.   Whatever your personal opinion of it, it’s hard to dispute that Taccone’s direction, Forte’s performance, a wildly unpredictable script, and a general go-for-broke attitude all make MacGruber unique."   

===Box office===
The film grossed $1,569,025 on its opening day,  and $4,043,945 for its opening weekend.  The film earned a total of $8,460,995 by the end of its third weekend, still short of the $10 million production cost. 

In July 2010, Parade (magazine)|Parade listed the film #2 on its list of "Biggest Box Office Flops of 2010."   

MacGruber realized a third-week drop of showings of 94%, from 2,546 to 177 theaters.    As a result of the flop, the film was removed from theaters after the third week.

==Sequel==
After rumors of a sequel were dismissed by Forte in 2012, two years later, in August 2014, interest was renewed and director Taccone has revealed that an outline is complete. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 