Ahista Ahista (2006 film)
{{Infobox film
| name           = Ahista Ahista ...
| image          = AhistaAhista2006_MoviePoster.jpg
| caption        = Theatrical release poster
| director       = Shivam Nair
| producer       = Anjum Rizvi Imtiaz Ali Imtiaz Ali
| starring       = Abhay Deol Soha Ali Khan Shayan Munshi
| music          = Himesh Reshammiya Abhishek Ray
| cinematography = Prakash Kutty
| editing        = Pravin Angre
| distributor    = Anjum Rizvi Film Company P R Films
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
}} White Nights, on which an earlier Raj Kapoor film Chhalia was also based.

==Plot==
This story is woven around aimless youth Ankush Ramdev (Abhay Deol) who scrapes a living by acting as a witness in Delhis marriage registrar bureau and a girl Megha (Soha Ali Khan) who has run away from her home in Nainital, to get married to her love Dheeraj (Shayan Munshi).

Fate intervenes and Dheeraj does not show up at the Marriage Registrars Office. Dejected, Megha is forced to rely on Ankushs generosity. Ankush helps Megha by getting her a job at a local old age home so that she has the security of a roof over her head. As time passes Megha begins to realize her potential as an educated girl who had earlier dedicated her life to her boyfriend and that relationship, thinking that there was nothing more to her. She realizes that she was wrong, and finally there comes a day when she tells Ankush that she is glad Dheeraj stood her up at the Marriage Bureau. Else, she would have never known who she really was.

At the same time Ankush realizes that the meager amount he earns each day is not enough for him. He still has to repay the loan he had taken for Meghas sake. So he starts working as a banks representative who opens bank accounts for a commission. Slowly he starts increasing the number of hours working at his new job and reduces those spent at the Marriage Registrars Office.

A day comes when he is able to repay the loan he had taken for Megha. On learning of this Megha reprimands him for doing so much for a girl who was a stranger for him. But now Ankush is earning 5 times more than he was before. He has a much more respectable job and a future. Ankush believes he has changed because of Megha and her faith in him. Life progresses well for Ankush. Soon he gets an offer to join the same bank as an Area Supervisor, at a handsome salary. Ankush hesitates, as fluent English is a pre-requisite.

Megha inspires Ankush giving him the confidence to realize that all he needed was an opportunity in life to succeed. A relationship develops between Megha and Ankush. Megha decides to leave her past with Dheeraj behind and embrace the future with Ankush by her side. 

Everything seems to be falling in place for Ankush when Dheeraj returns. He is persistent in his search for Megha and Ankushs new found happiness crumbles. 

Ankush tries to force Dheeraj to return to Nainital. He tries all his wits and energy to do this, but does not succeed. He even shows Dheeraj the death certificate of Megha, so that he can return, but Dheeraj has complete faith in his love and Megha.

One day Ankushs friend gives Dheeraj the whereabouts of Megha. This is on the same day Megha and Ankush have planned to get married. On seeing Dheeraj in the mirror, she gets angry at him and asks him to leave. Dheeraj agrees, but he also insists her to listen to what had happened to him when he was coming to Delhi from Nainital. He was preyed in a plot of RDX in the train. He somehow escapes from the plot. Megha realizes that what Dheeraj did was not intentional and was just a situation in which Dheeraj was trapped.

Megha talks to Ankush and tells him that she cannot live without Dheeraj and finally Ankush realizes that he is just a friend to her. In the end Ankush becomes the witness of the wedding of Dheeraj and Megha but asks for his regular fees, severing all connection to Megha and Dheeraj love story.

==Cast==

* Abhay Deol as Ankush Ramdev
* Soha Ali Khan as Megha
* Shayan Munshi as Dheeraj
* Shakeel Khan as Ankush Friend
* Kamini Khanna as Ankushs Friends Ammi
* Sohrab Ardeshir as Father
* Ashwin Chitale 
* Murad Ali as Chawl Owner
* Brijendra Kala as PCO Owner
* D. Santosh (Actor)|D. Santosh as Pipney (Ankushs Room Mate)

==External links==
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 