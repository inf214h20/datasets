Blue Collar Comedy Tour: The Movie
{{Infobox film | name = Blue Collar Comedy Tour: The Movie
  | image = Blue Collar 1.jpg
  | caption = Blue Collar Comedy Tour: The Movie DVD
  | director = C.B. Harding
  | producer =
  | writer =
  | starring = Jeff Foxworthy Bill Engvall Larry the Cable Guy Ron White David Alan Grier
  | music =
  | cinematography =
  | editing =
  | distributor = Warner Bros.
  | country = United States
  | released = March 28, 2003
  | runtime = 106 minutes
  | language = English
  | budget = $5 million
  |  gross = $604,856
  }}

Blue Collar Comedy Tour: The Movie is a   (2006).  The film has been praised for its jokes and irreverent humor and is considered to be the best film the group has done.

== Plot ==
The film features live stand-up performances filmed at Phoenixs Dodge Theater in July 2002 as well as behind-the-scenes sequences highlighting the individual comedians.

==Television Broadcast==

When aired on Comedy Central, in addition to editing of the stand-up material for time and content, Heidi Klums appearance is completely cut out.

== Filming Locations ==

*Dodge Theater in Phoenix, Arizona
*Scottsdale, Arizona
*Scottsdale Fashion Square -7014 East Camelback Road (Mall Scenes)
*Phoenix, Arizona PARK N SWAP
*Mesa, Arizona FIESTA MALL

== Soundtrack ==
# "Dont Ask Me No Questions", Chris Cagle
# "Act Naturally", Buck Owens
# "Sharp Dressed Man", ZZ Top
# "Boogie Chillen", John Lee Hooker
# "Venom Wearing Denim", Junior Brown

==Reception==
The film has received near-universal critical acclaim, praising the irreverent jokes and humor. On Rotten Tomatoes, the film has a 92% positive score.

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 


 