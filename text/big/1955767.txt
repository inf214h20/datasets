Hatari!
{{Infobox film name = Hatari! image = Hatari.jpg caption = Frank McCarthy director = Howard Hawks producer = Howard Hawks Paul Helmick screenplay = Leigh Brackett story = Harry Kurnitz starring = John Wayne Elsa Martinelli Hardy Krüger Red Buttons music = Henry Mancini cinematography = Russell Harlan editing = Stuart Gilmore distributor = Paramount Pictures released =   runtime = 157 min. country =   language = English
|gross = $12,923,077   The Numbers. Retrieved June 13, 2013. 
}}
Hatari! ( ,  , a dormant volcano.

Hatari! was shot in Technicolor and filmed on location in northern Tanganyika (in what is now Tanzania).

==Plot==
Hatari! is the story of a group of adventurers in East Africa, engaged in the exciting and lucrative but dangerous business of catching wild animals for delivery to zoos around the world. As "Momella Game Ltd.", they operate from a compound near the town of Arusha. The head of the group is Sean Mercer (John Wayne); the others are safari veteran Little Wolf a/k/a "Indian" (Bruce Cabot), drivers "Pockets" (Red Buttons), and Kurt (Hardy Krüger), roper Luis (a Mexican former bullfighting|bullfighter; Valentin de Vargas), and Brandy (Michele Girardon), a young woman whose late father was a member of the group; she grew up there.

Their method (shown in several action sequences) is to chase the selected animal across the plains with a truck, driven by Pockets, a former Brooklyn taxi driver. Sean stands in the bed of the truck with a rope noose on a long pole, and snags the animal by its head. (For smaller animals, Sean rides in a seat mounted on the trucks left front fender.) A smaller, faster "herding car" swings outside, driving the animal back toward the "catching truck". Kurt, a German auto racing driver, drives the herding car. Once the animal is snagged, Luis, an expert roper and lassoer, catches its legs and secures it. The animal is then moved into a travel crate, carried on a third truck, driven by Brandy. The captured animals are held in pens in the compound, tended by native workers, until they are shipped out at the end of the hunting season.

Sometimes the group ranges far outside the compound for several days, accompanied by additional  trucks with camp equipment.
 type AB- blood. However, the Frenchman has that type. He provides the transfusion, but only after making Kurt ask him.

The group returns to the compound after celebrating Indians survival. There, Sean, and then Pockets, are very surprised to find a strange young woman sleeping in Seans bed. The next morning, she introduces herself as Anna Maria DAlessandro ("Just call me Dallas"; Elsa Martinelli), a photojournalist sent by the Basel Zoo to record the capture of the many animals they have ordered. 

Sean is annoyed, but under the contract with the zoo they must accommodate her, and Dallas quickly makes friends with the others, especially Pockets. Dallas rides along on the groups catching runs, snapping pictures.

She is immediately attracted to Sean, and (she thinks) he to her, but he treats her brusquely. Pockets explains: a few years earlier, Sean was engaged to a woman who came to the compound, and then abruptly left him. Ever since, he distrusts women - especially those he is attracted to. If he wasnt attracted to Dallas, he wouldnt be rude to her.

The young Frenchman comes to the compound, and after proving himself a crack shot, is hired to replace Indian. His name is Charles Maurey, but Sean dubs him "Chips". His job is to ride with Kurt in the herding car, carrying a rifle in case of animal attack.

Indian returns, and urges Sean to forego catching any rhinos this season. A "nice Belgian kid" was killed in an earlier rhino chase, as was Brandys father, and now Indian was nearly killed. He suggests there is a jinx. Sean agrees only to postpone rhino to the end of the season.

Dallas makes some progress with Sean, but friction between them continues, especially after Dallas adopts first one, then two, and finally three orphan elephant calves. This leads to her being adopted by the local Warusha tribe as Mama Tembo ("Mother of Elephants").

Chips and Kurt flirt with Brandy; as Sean notices, "Shes all growed up." But she falls for Pockets instead. Additional chases are shown, with the group capturing a zebra, a giraffe, a gazelle, a African buffalo|buffalo, and a wildebeest. They also trap a leopard in a baited cage. When the herding car is mired during a river crossing, Chips shoots a crocodile that is threatening Kurt.

Pockets spends several days privately tinkering in the compound workshop. He invents a method of flinging a net over a tree full of monkeys, which the zoos want. The group catches over 500 monkeys.

With all other orders filled, the group catches a rhino without serious incident, and Indian agrees that the jinx is broken. The group goes to Arusha to celebrate the end of the season, but Dallas declines. She is frustrated, because though she has had some intimate moments with Sean, he has never quite declared his feelings. When Sean urges her to join the groups excursion, she lashes out at him and bursts into tears, leaving him baffled.

The next morning, shes gone, leaving a farewell letter with Pockets. Sean and the group rush to Arusha to catch her, which results in the baby elephants chasing Dallas all over town.

In the final scene, Dallas is again in Seans bed when he enters the room, and they reprise the dialogue from their first meeting. As before, Pockets also comes in drunk and again asks Sean "What is she doing in your bed?" But this time, Sean announces "We got married today!"

==Cast==
*John Wayne as Sean Mercer
*Hardy Krüger as Kurt Müller
*Elsa Martinelli as Anna Maria "Dallas" DAlessandro
*Red Buttons as "Pockets"
*Gérard Blain as Charles "Chips" Maurey
*Bruce Cabot as Little Wolf ("The Indian")
*Michèle Girardon as Brandy de la Court
*Valentin de Vargas as Luis Francisco Garcia Lopez
*Eduard Franz as Dr. Sanderson
*Queenie Leonard as Nurse (scenes deleted)

==Production==
Hatari! has a very loose script and, like many other major works of Hawks, is principally structured on the relationships among the characters, though it is "bookended" by the initial violent (and nearly fatal) encounter with a rhinoceros and the end-of-season determination to make such a capture to fulfill the teams quota. Much of the film revolves around scenes of chasing animals in jeeps and trucks across the African plains. The animals pursued are also all live, wild, and untrained, a procedure banned today over concerns of exhausting and killing the targeted animals. The script was written by Hawks favorite writer, Leigh Brackett, after the group returned from Africa with the catching scenes.

At the beginning of the production, all Hawks knew was that he wanted a movie about people who catch animals in Africa for zoos, a dangerous profession with exciting scenes the likes of which had never been seen on-screen before.  Hawks increased his knowledge on animal catching from the work of the famous South African animal conservationist, Dr. Ian Player. In 1952, South Africa was eliminating all large wild animals to protect livestock, and only 300 white rhinos survived. Player then invented his famed rhino catching technique to relocate and save the white rhinos. Players project was called "Operation Rhino" and it was recorded in the renowned documentary film Operation Rhino. Hawks studied this film closely and incorporated aspects of it into his film. McIntyre, Thomas. "Fifty Years of HATARI! – The Story of Most Expensive Safari In the World." Sports Afield, May/June 2012, pg 70  

Michèle Girardon (Brandy) spoke no English when cast in the role; she taught herself English while on the set, according to a July 1961 LIFE magazine profile of the actress.   

Government licensed animal catcher Willy de Beer was hired by Hawks to be the close by technical advisor, and his assistants became their staff of experts in regards to catching the animals. 

Hawks was inspired by the famous animal photographer Ylla, so he had script writer Brackett add the character of Dallas. Hawks said, "We took that part of the story from a real character, a German girl. She was the best animal photographer in the world."     

Filming in Africa was dangerous for the production team and actors. According to director Howard Hawks, all the animal captures in the picture were actually performed by the actors; no stuntmen or animal handlers were substituted on-screen. The rhino really did escape, and the actors really did have to recapture it - and Hawks included the sequence for its realism. Much of the action sequence audio had to be re-dubbed due to John Waynes cursing while wrestling with the animals. However, a stand-in, "Rusty" Walkley (real name: Mildred Lucy Walkley), was used for some scenes involving Elsa Martinelli. 

Hawks said Wayne admitted being scared during some of the action scenes, and "had the feeling with every swerve that the car was going to overturn as he hung on for dear life, out in the open with only a seat belt for support, motor roaring, body jarring every which-way, animals kicking dirt and rocks and the thunder of hundreds of hooves increasing the din in his ears." Wayne felt it was unpredictable with the terrains hidden holes and obstacles which could have been disastrous. 

When Hawks interviewed de Vargas, he told him it would be very dangerous and showed him a documentary. De Vargas had no double and like the rest of the cast played in the animal catching shots.  One evening Buttons and Wayne were playing cards outside and a leopard came out of the bush towards them. When Buttons mentioned the approaching leopard, Wayne said, "See what he wants."  De Vargas said technical adviser Willy de Beer was mauled by a loose baby leopard that sprang on him from a tree, "He came back with his arm covered in bandages and throat completely wrapped, but he just shrugged it off."  

As the animals frequently refused to make noise "on cue" (in particular, the baby elephants refused to trumpet inside populated areas), local Arusha game experts and zoo collectors were hired to do "animal voice impersonations".

Hawks stated in interviews that he had originally planned to star both Clark Gable and Wayne in the film until Gables death finally ruled that out.

Hatari! introduced the memorable Henry Mancini tune "Baby Elephant Walk".    Another memorable musical moment is a duet of Stephen Fosters "Old Folks at Home" (Swanee River) with Dallas playing the piano, and Pockets playing the harmonica.

==Reception== theatrical rentals. 8th highest grossing film of 1962.

==See also==
*John Wayne filmography

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
*  in the  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 