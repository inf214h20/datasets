Tex Granger
{{Infobox Film
| name           = Tex Granger
| image          = Poster of the movie Tex Granger, Midnight Rider of the Plains .jpg
| image_size     = 
| caption        = Poster of chapter 10 (Midnight Ambush)
| director       = Derwin Abrahams
| producer       = Sam Katzman
| writer         = Lewis Clay Royal K. Cole Harry L. Fraser Arthur Hoerl George H. Plympton
| narrator       =  Peggy Stewart Jack Ingram I. Stanford Jolley
| music          = Mischa Bakaleinikoff
| cinematography = Ira H. Morgan Earl Turner
| distributor    = Columbia Pictures 1948
| runtime        = 15 chapters (270 min)
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Columbia Serial movie serial The Last Frontier (1926 in film|1926),  which was itself based on the novel of the same name by Courtney Ryley Cooper.  Tex Granger was the 36th of the 57 serials released by Columbia.

==Plot==
When Tex Granger rides into Three Buttes, Helen Kent persuades him to buy the local newspaper.  However, loan shark Rance Carson appoints the bandit Blaze Talbot as town marshal to act as his enforcer and soon the town is in chaos.  With fighting between rival gangs, Tex dons a mask to become The Midnight Rider and bring the criminals to justice.

==Cast==
*Robert Kellard as Tex Granger, new owner of the town newspaper and secretly also The Midnight Rider of the Plains Peggy Stewart as Helen Kent
*Robert Buzz Henry as Timmy Perkins, a young boy 
*Smith Ballew as Blaze Talbot, bandit appointed as marshal by Carson Jack Ingram as Reno, bandit working with Blaze Talbot
*I. Stanford Jolley as Rance Carson, the villainous loan shark Terry Frost as Luke Adams, one of Carsons henchmen
*Jim Diehl as Conroy, one of Carsons henchmen 
*Britt Wood as Sandy White 
*Tiny Brauer as Morgan, one of Carsons henchmen
*Duke, the dog

==Chapter titles==
# Tex Finds Trouble
# Rider of Mystery Mesa
# Dead or Alive
# Dangerous Trails
# Renegade Pass
# A Crooked Deal
# The Rider Unmasked
# Mystery of the Silver Ghost
# The Rider Trapped
# Midnight Ambush
# Renegade Roundup
# Carsons Last Draw
# Blaze Takes Over
# Riding Wild
# The Rider Meets Blaze
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 247
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
*  
*  featuring Tex Granger

 
{{succession box  Columbia Serial Serial  Brick Bradford (1947)
| years=Tex Granger (1948)
| after=Superman (serial)|Superman (1948)}}
 

 

 
 
 
 
 
 
 