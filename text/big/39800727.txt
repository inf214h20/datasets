Figures of the Night
{{Infobox film
| name           = Figures of the Night
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald 
| producer       = Richard Oswald
| writer         = Karl Hans Strobl   Richard Oswald
| narrator       = 
| starring       = Paul Wegener   Reinhold Schünzel   Erna Morena   Conrad Veidt
| music          = 
| editing        =
| cinematography = Carl Hoffmann
| studio         = Richard-Oswald-Produktion
| distributor    = Decla-Bioscop
| released       = 6 January 1920
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| 
}} German silent silent horror film directed by Richard Oswald and starring Paul Wegener, Reinhold Schünzel and Erna Morena.  The films art direction was by Hans Dreier. It was filmed at the Babelsberg Studio in Berlin

==Cast==
* Paul Wegener as Thomas Bezug 
* Reinhold Schünzel as Sekretär 
* Erna Morena as Elisabeth, Bezugs Tochter 
* Erik Charell as Arnold, Bezugs Sohn, Affe 
* Conrad Veidt as Clown 
* Anita Berber as Tänzerin 
* Paul Bildt as Erfinder 
* Theodor Loos   
* Willi Allen

==References==
 

==Bibliography==
* Hutchings, Peter. The A to Z of Horror Cinema. Scarecrow Press, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 