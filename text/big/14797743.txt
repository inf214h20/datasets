Gaalipata
{{Infobox film
| name           = Gaalipata
| image          = Galipata.jpg
| caption        = Theatrical release poster
| director       = Yogaraj Bhat
| producer       = Suryaprakash Rao A. M. Rathnam
| writer         = Yogaraj Bhat
| screenplay     = Yogaraj Bhat Ganesh Daisy Neethu Bhavana Rao
| music          = V. Harikrishna Rathnavelu
| editing        = Suresh Urs
| studio         = SPR Entertainers (India) Pvt. Ltd.
| released       = 18 January 2008
| country        = India
| language       = Kannada
| budget         =  
| boxoffice      =  
}}
 Neethu and Bhavana Rao. The music for the film was composed by V. Harikrishna.

==Plot==
Three friends, Ganesh (Ganesh), Diganth (Diganth) and Kitty (Rajesh Krishnan) come to Mugilupete. Ganesh falls in love with Sowmya (Daisy Bopanna), a widow, staying with her in-laws Kodandaram (Anant Nag) and Padma (Padmaja Rao) while Diganth and Kitty fall for Kodandarams daughters Radha (Neethoo) and Pavani (Bhavana Rao) respectively.

While Diganth-Radha and Kitty-Pavani get the green signal for marriage, Sowmya is hesitant to accept Ganesh as her life partner. The director shows how Ganesh wins her heart in a unique way.

==Cast== Ganesh as Ganesh
* Rajesh Krishnan as Kitty
* Diganth as Diganth/Doodh Peda
* Ananth Nag as Kodandaram
* Daisy Bopanna as Souwmya
* Bhavana Rao as Pavani Neethu as Radha
* Padmaja Rao as Padma
* Dayal Padmanabhan as Dracula
* Rangayana Raghu
* Sudha Belawadi
* Rajaram

==Shooting locations==
Most part of the outdoor scenery of "Gaalipata" was shot in "Kodachadri" Kodachadri shivamoga district & "coorg" mandalpatti Yogaraj Bhat chose  and represented it as mugilpate where the three friends arrive.

==Box office==
Gaalipata had a huge publicity campaign. After a strong opening, the film was shown for 100 days in its main theater. It went for a 175-day run in PVR cinemas in Bengaluru. 

==Awards== Ganesh won Best Actor Award (Kannada) at the 56th Filmfare Awards South, held in Hyderabad, India|Hyderabad. In total, the movie won three awards including Best Music (V. Harikrishna) and Best Lyricist (Jayanth Kaikini for "Minchaagi Neenu Baralu"). 

==Soundtrack==
V. Harikrishnas songs for the film, including "Akaasha Ishte Yaakideyo", "Minchaagi Neenu Baralu" and "Na Dheem Dheem Tana" became hits and received significant air-time on television and radio channels.

{| class="wikitable sortable"
|-
!Song !! Singer !!Lyrics
|- Aakasha Ishteyakideyo || Tippu (singer)|Tippu, Kunal Ganjawala || Jayanth Kaikini
|- Minchaagi Neenu || Sonu Nigam ||  Jayanth Kaikini
|- Nadhimdhim Tanananaa || K. S. Chithra || Yograj Bhat
|- Kavite || Vijay Prakash|| Hrudayashiva
|- Jeeva Kaleva || Sonu Nigam || Yogaraj Bhat
|- Aaha Bedurubombe || Udit Narayan, Anuradha Sriram || Jayanth Kaikini, Dinesh
|}

==Home media==
The movie was released on DVD with 5.1 channel surround sound and English subtitles and VCD.

==References==
 

==External links==
*  

 

 
 
 
 