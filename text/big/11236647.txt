Jaane Tu... Ya Jaane Na
 
 
{{Infobox film
| name                = Jaane Tu... Ya Jaane Na
| image               = Jaanetuyajaanena.JPG
| caption             = Theatrical release poster
| director            = Abbas Tyrewala
| producer            = Mansoor Khan Aamir Khan
| story               = Abbas Tyrewala Farhan Akhtar Imran Khan   Genelia DSouza  Prateik Babbar
| music               = A. R. Rahman
| cinematography      = Manoj Lobo
| editing             = Shan Mohammed
| distributor         = Aamir Khan Productions Excel Entertainment
| released            = 4 July 2008
| runtime             = 155 min.
| country             = India Hindi
| genre               = Romantic comedy
| budget              =  
| gross               =   
}}
 Imran Khan and Genelia DSouza in pivotal roles. Produced by Mansoor Khan, Aamir Khan, it marks the directional debut of Abbas Tyrewala, the debut of Imran Khan (Aamir Khans nephew) and Prateik Babbar as actors, and the re-appearance of DSouza in Hindi cinema. Released on 4 July 2008, the film received positive reviews, and went on to become a Super Hit at the box office. The music is by A. R. Rahman.

==Plot==
The story begins at an airport, when Jignesh a.k.a. Jiggy(Nirav Mehta), Ravindran a.k.a. Rotlu(Karan Makhija), Sandhya a.k.a. Bombs(Alishka Varde) and Shaleen(Sugandha Garg) offer to tell Mala(Renuka Kunzru), Jiggys friend & crush, the tale of Jai and Aditi, a love story with joys and sorrows, happiness and heartbreaks, laughter, songs and fights, and...a climax at the airport. Mala is initially uninterested in the story, and hates loves stories in general, but still agrees to listen as the flight carrying the friends they have come to receive is late.

Meet Jai Singh Rathore, a.k.a. Rats(Imran Khan), the most non-violent Rajput ever. Meet Aditi Mahant(Genelia DSouza), a.k.a. ‘Meow‘, a highly aggressive, impulsive girl. She abuses. She scratches (hence the name Meow).But still Jai and Aditi are college-going best buddies. Rotlu, Jiggy, Bombs and Shaleen are also college-mates of Jai & Aditi, and a part of their group. Rotlu loves Aditi and Bombs loves Jai. However, Jai and Aditi are always so busy with each other that they overlook this.
Jai and Aditi are perfect for each other. Their friends know this. Their parents know this. Everybody but the two themselves know this. Aditi dreams of a virile, macho husband, while Jai wants a sweet, romantic girl. Since the two of them dont believe that they are in love, after finishing their college they decide to hunt for a perfect life partner for each other.
Jai falls in love with Meghna(Manjari Phadnis) at first sight in a club where two psychos were misbehaving with her. He saves her, not by fighting but by tricking the two psychos, who always ride horses. Meghna always plays the game of imagination, “Whats this?” When Meghna and Jai get close to each other, Rotlu, Jiggy, Bombs and Shaleen are happy for Jai but nobody notices that Aditi was missing Jais company. Aditi notices changes in Jais behaviour.

Meghna informs Jai that her parents squabble together but cannot stay without each other. She says this is really "cute." Jai visits Meghnas parents. Her father is an alcoholic. Her parents always argue with each other and Jai feels uncomfortable with them.
When Jai goes to meet Meghnas parents, he misses Aditis surprise birthday party. She is hurt by this and acts rudely to Jai when he goes to wish her later that evening.

Aditi gets engaged to her fathers friends son, Sushant Modi(Ayaz Khan).She sees in him the macho man she always wanted, however, Sushant is actually a spoilt playboy.  Aditis brother Amit(Prateik Babbar) tries to make her see Sushants reality. But Aditi thinks that Jai loves Meghna and continues her relationship with Sushant. At Jiggys birthday bash, Aditi introduces her fiancé. Then everyone decides to dance. Aditi is disturbed because she is jealous seeing Jai with Meghna, and Jai is upset that Aditi is engaged and disturbed. Aditi becomes emotional seeing Jai with Meghna But However in the middle of the dance, Sushant thinks Aditi is crying because she likes him and kisses her. Jai sees this and stops dancing abruptly. While taking Meghna home, she tries to cheer him up, but it just results him in becoming more angry. He shouts at Meghna for acting childishly.

Jai realises that Meghna is a girl who runs from the reality and hides in rather stupid refuges. Meghna breaks down, and lets Jai know how she became this way, and why. Meghna shares her dark childhood story of her father(Rajat Kapoor) having an extra-marital affair and her mother, Sheela(Kitu Gidwani) leaving the residence, but later only returning for Meghnas sake. Meghna is hurt when she sees her parents in pain solely on her account. But since theres no other way round, she decides to close her eyes and see things the way she wants, in a happy manner. Therefore, according to her, her parents fight in a very cute manner. All is happy in the way she sees things, and does not want anyone to remove this veil before her eyes until she is ready for it. The next day, to cheer up things, she kids Jai by saying that Aditi is the right girl for him and he should marry her instead. Jai is taken aback when Meghna laughs and apologises, saying that she was only joking, and did not intend to shock Jai. But Jai reveals that there is more truth to the joke than Meghna thinks, and says that he is actually in love with Aditi. Meghna is shattered and leaves with a goodbye kiss. Aditi reveals her feelings for Jai under duress from Sushant, who slaps her on her face, leaving a bruise. Aditi breaks up with him and decides to go to the USA to study film-making at New York Universitys Tisch School of the Arts. Jai spots the bruise on Aditis face and contrary to his usual non-violent nature, he thrashes Sushant for daring to lay a hand on her.

Inspector Waghmare( ), as he had challenged Jais mother that Jai will fulfill the conditions some day.
He jumps over the security booths and runs, seeking for Aditi, while the Airport security chases him, supposing him to be a terrorist who ran away from security checks. He finds Aditi and sings the song – "Jaane tu ya jaane na" for her, which is the same song that Jai had said in the beginning hed sing for the love of his life. Aditi, delighted, hugs him and cancels her US trip. Airport security cop Inspector Prakash, on finding that Jai is not a terrorist, lets him go, but with a warning.

Rotlu, Jiggy, Bombs and Shaleen conclude the story and welcome Jai and Aditi, just returning from their honeymoon, at the airport. Mala gets extremely delighted to see them and introduces herself as Jiggy’s girlfriend and they leave the airport together.

==Cast==
 Imran Khan as Jai Singh Rathore (Rats)
* Genelia DSouza as Aditi Mahant (Meow)
* Nirav Mehta as Jignesh (Jiggy)
* Alishka Varde as Sandhya (Bombs)
* Renuka Kunzru as Mala
* Prateik Babbar as Amit Mahant,Aditis brother
* Ratna Pathak as  Savitri Rathore, Jais mother
* Manjari Phadnis as Meghna
* Anuradha Patel as Aditis mother
* Jayant Kripalani as Aditis Father
* Sugandha Garg as Shaleen
* Karan Makhija as Ravindran (Rotlu)
* Ayaz Khan as Sushant Modi, Aditis Fiance
* Naseeruddin Shah as Jai Singh Rathores father
* Sohail Khan as Vinay Singh Rathore Arbaaz Khan as Kuber Singh Rathore
* Paresh Rawal as Inspector P K Waghmare
* Murli Sharma as Inspector Prakash
* Kitu Gidwani as Sheela (Meghnas mother)
* Rajat Kapoor as Meghnas father
* Shakun Batra as Nilesh
* Padam Bhola as Vivek

== Release ==

The film was released on 4 July 2008. The premiere of the film was held at the PVR Cinemas in Goregaon, Mumbai where many Bollywood stars were present. The film was released with 473 prints worldwide, which includes 282 analogue prints, 81 digital screens and 110 prints overseas.  In its second week, the number of shows at multiplexes were increased while 100 more centres were opened.   

==Reception==

===Critical response===

The movie got positive responses from the critics. Rediff.com gave it a 4 out of 5, "a rock-solid ensemble cast that is mouthwateringly perfect". Movie Talkies has given it 4 stars, "the coolest, warmest, hippest and funniest musical romantic comedy in years has arrived, and a new Khan is born."  Glamsham gave it another 4 out of 5, "another fruitful outing for Aamir Khan. This man certainly knows what he is doing." Planet Bollywood gave it 4 stars, "a fresh breezy entertainer which undoubtedly is the best the year has seen so far." AOL India reviewer Noyon Jyoti Parasara gave it 3.5 out of 5 and said, "Jaane Tu... weaves in a lot of emotions right from friendship, love to insecurity. Every character is shown to have a definite backing. Which means a very good job by the writer."  Taran Adarsh of IndiaFM gave it 3.5 stars, "a breezy entertainer which will be loved by its target audience – the youth".

===Box office===

Jaane Tu... Ya Jaane Na was an early success, with 80% or more in morning shows and 100% by the noon shows across India. Monday usually witnesses a drop in business for films in India, but Jaane Tu... Ya Jaane Na had strong showings on Monday (75%), Tuesday (70%) and Wednesday (60%+).    The first week all-India gross was   with an average of    per print.  For the second week, the number of shows were increased and it started the week grossing almost 100% at several screens.  The film grossed   in its second week, taking its total to  . It grossed approximately   in full run, thus being declared a superhit.

== Music ==
{{Infobox album |  
 Name = Jaane Tu... Ya Jaane Na
| Type = Soundtrack
| Artist = A. R. Rahman
| Cover = 
| Released =  20 May 2008 (CD release)
| Recorded = Panchathan Record Inn A.M. Studios Feature film soundtrack
| Length = 34:32
| Label = T-Series
| Producer = A.R. Rahman
| Last album =    (2008)
| This album = Jaane Tu... Ya Jaane Na (2008)
| Next album = Sakkarakatti   (2008)|
}}
{{Album ratings
| rev1 = Rediff
| rev1Score =   
| rev2 = Planet Bollywood
| rev2Score =   
| rev3 = Bollywood Hungama
| rev3Score =   
}}

The music of Jaane Tu... Ya Jaane Na, composed by A. R. Rahman and lyrics by director Abbas Tyrewala, was released in India on 20 May 2008 with producer Aamir Khan presenting the first audio CD to veteran actor Shammi Kapoor.  The soundtrack held the number one spot on the music charts for several consecutive weeks.  The soundtrack was well received by the audiences and the critics, {{cite news| first = Taran
| last = Adarsh|url=http://www.bollywoodhungama.com/movies/review/12917/index.html|title=Jaane Tu Ya Jaane Na movie review|publisher=Bollywoodhungama.com|date=4 July 2008}}   with "Pappu Cant Dance" becoming one of the major hits of 2008. 
Rahman was awarded with the Filmfare Best Music Director Award and the Star Screen Award for Best Music Director for this soundtrack. The track "Kabhi Kabhi Aditi" was nominated for Best Playback Singer (Rashid Ali) and Best Lyrics (Tyrewala) at the Filmfare awards.
{{Track listing
| extra_column    = Artist(s)
| title1   = Kabhi Kabhi Aditi Rashid Ali
| length1  = 3:41
| title2   = Pappu Cant Dance
| extra2   = Anupama (singer)|Anupama, Benny Dayal, Blaaze, Tanvi Shah|Tanvi, Darshana KT|Darshana, Satish Subrahmaniam, Mohammed Aslam
| length2  = 4:27
| title3   = Jaane Tu Mera Kya Hai (Aditi)
| extra3   = Runa Rizvi
| length3  = 3:41
| title4   = Nazrein Milana Nazrein Churana
| extra4    = Benny Dayal, Satish Subrahmaniam, Naresh Iyer, Darshana, Swetha Mohan|Swetha, Tanvi, Sayonora Philip|Sayonara,  Bhargavi, Anupama
| length4  = 3:57
| title5   = Tu Bole Main Boloon
| extra5    = A. R. Rahman
| length5  = 4:36
| title6   = Kahin Toh
| extra6    = Rashid Ali, Vasundhara Das
| length6  = 5:05
| title7   = Jaane Tu Meri Kya Hai (Jai)
| extra7    = Sukhwinder Singh
| length7  = 5:44
| title8   = Pappu Cant Dance (Remix)
| note8    = Remixed by Krishna Chetan
| extra8    = Anupama, Benny Dayal, Blaaze, Tanvi, Darshana, Satish Subrahmaniam, Mohammed Aslam
| length8  = 4:27
}}

== Awards ==

===Star Screen Awards=== Most Promising Imran Khan Best Music Director – A. R. Rahman

===Filmfare Awards=== Best Male Imran Khan Best Music Director Award – A. R. Rahman Special Performance Award – Prateik Babbar Best Choreography Award – Longinus Fernandes

===4th Apsara Film & Television Producers Guild Awards===
*Best Male Debut – Imran Khan (actor)

=== Star Sabse Favourite kaun Awards === Naya Hero Imran Khan

===Stardust Awards=== Breakthrough Performance Award (Male) – Prateik Babbar Breakthrough Performance Award (Female) – Manjari Phadnis
* New Musical Sensation (Male) – Benny Dayal for (Pappu Cant Dance)

== References ==
 

==External links==
* 

 
 
 
 