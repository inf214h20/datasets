Music in My Heart
{{Infobox film 
 | name = Music in My Heart
 | image          =
 | image_size     =
 | caption        =
 | director = Joseph Santley
 | producer = Irving Starr
 | writer = James Edward Grant
 | starring = {{Plainlist| Tony Martin
*Rita Hayworth
*Edith Fellows
*Alan Mowbray
}}
 | music = {{Plainlist| Chet Forrest Bob Wright
*Ary Barroso Charles Henderson (vocal arranger)
*Morris Stoloff (musical director)
}}
 | cinematography = John Stumar Otto Meyer
 | distributor = Columbia Pictures
 | released =  
 | runtime = 70 minutes
 | language = English
 | country = United States
 | budget = 
}}
 Tony Martin and Rita Hayworth. Hayworths first musical for the studio, the film was recognized with an Academy Award nomination for the song, "Its a Blue World", performed by Martin and Andre Kostelanetz and His Orchestra.

==Production==
Production on Music in My Heart (alternate title Passport to Happiness) began in October 1939. The film was released January 10, 1940. 

==Cast==
Credits for Music in My Heart are listed in the AFI Catalog of Feature Films.    Tony Martin … Robert Gregory 
* Rita Hayworth … Patricia OMalley 
* Edith Fellows … Mary
* Alan Mowbray … Charles Gardner 
* Eric Blore … Griggs
* George Tobias … Sascha 
* Joseph Crehan … Mark C. Gilman 
* George Humbert … Luigi
* Joey Ray … Miller
* Don Brodie … Taxi Driver
* Julieta Novis … Leading Lady
* Eddie Kane … Blake
* Phil Tead … Marshall
* Marten Lamont … Barrett 
* Andre Kostelanetz and His Orchestra

==Soundtrack==
Chet Forrest and Bob Wrights original songs for Music in My Heart include "Oh What a Lovely Dream", "Punchinello", "Ive Got Music in My Heart", "Its a Blue World" (a hit record for Tony Martin), "No Other Love" and "Hearts in the Sky". The film also features Ary Barrosos samba, "No Tabuleiro da Baiana", performed by Andre Kostelanetz and His Orchestra.    

==Accolades== Chet Forrest Bob Wright, Best Original Song at the 13th Academy Awards.    The song is performed in the film by Tony Martin and Andre Kostelanetz and His Orchestra.

==Reception==
Film historian Clive Hirschhorn describes Music in My Heart as "a lightweight Irving Starr production" with "serviceable words and music" and "unremarkable direction".  

Biographer Barbara Leaming characterized the film as one of the "dreadful mistakes" Columbia Pictures made with Rita Hayworth as the studio tried to figure out how to use her to advantage. Music in My Heart was one of five pictures Hayworth appeared in that year, none of which caught on with the public. Leaming, Barbara, If This Was Happiness: A Biography of Rita Hayworth. New York: Viking, 1989 ISBN 0-670-81978-6  

Reviewing the 2004 DVD release, Turner Classic Movies called Music in My Heart "a fun, charming, and unpretentious little musical which illustrates very well what an ordinary Hollywood entertainment of 1940 was like. … In the end, its Martins voice and Hayworths overall presence which makes this a nice little winner, though Eric Blore, Alan Mowbray and George Tobias provide solid support as always." 

==Home media==
* 2004: Columbia TriStar Home Entertainment, DVD. ISBN 9781404955332 

==References==
 

== External links ==
*  
* 
*  
*   by Tony Martin (Decca Records 2932B, 1939) at YouTube

 
 
 
 
 
 
 
 
 
 

 