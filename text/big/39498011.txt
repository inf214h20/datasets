Atom the Amazing Zombie Killer
{{Infobox film
| name           = Atom the Amazing Zombie Killer
| image          = AtomtheAmazingZombieKiller.jpg
| alt            = 
| caption        = 
| director       = Zack Beins Richard Taylor
| producer       = Zack Beins Richard Taylor Tim Johnson Robert Sausaman
| writer         = Zack Beins Richard Taylor Tim Johnson
| starring       = Mark Shonsey Lindy Starr Zachary Byron Helm Lloyd Kaufman
| music          = 
| cinematography = 
| editing        = 
| studio         = Bizjack Flemco
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 independent Horror horror comedy feature film directed by Zack Beins and Richard Taylor, and written by Beins, Taylor and Tim Johnson.

==Plot synopsis== bowler and hallucinate that everyone around him are the flesh-eating Zombie (fictional)|undead. Believing the zombie apocalypse has arrived, Atom must defend himself from the "zombies" in the most violent and unpredictable manner possible.
 campy aesthetic and graphic violence and nudity common of the companys films, as well as including many overt references to Troma movies and a cameo appearance from Troma president Lloyd Kaufman.      

==Production== trailer to generate interest, and hosted several benefit concerts for the movie with local bands. 

The film was shot entirely in the cities of Denver and Arvada, Colorado|Arvada, Colorado over the course of four years.     According to interviews with Taylor and Beins, the production was troubled by numerous issues, exacerbated by the extremely low budget of the film; notably, large portions of the film had to be re-shot after one of the main actors quit and had to be recast, while problems in finding affordable filming locations resulted in shooting at five different bowling alleys.    Atom was shot in high-definition video; as neither Beins or Taylor owned an HD camera, the two tracked down a friend who did and offered him an acting role in exchange for use of their camera. 
 internet personality Return to the Class of Nuke Em High).   Zachory Byon Helm, who was cast as the films primary antagonist Dario, is the president of the  , and several of his hearses were used in the production.

==Release and media== Las Vegas.     In 2013, the film was screened at the Denver Comic Con, followed by a Los Angeles premiere at the California Institute of Abnormalarts.  The same year, Atom was again screened in Denver with Midget Zombie Takeover by The Worst Movie Ever! director Glenn Berggoetz.
 clamshell box. 

In 2013 at Comic-Con International Taylor and Beins sat on the panel for Troma Entertainment and were asked by a fan if the film will be released on something more current then VHS. Taylor asks Lloyd Kaufman if Troma would release a DVD of the film. Kaufman replies "Will see how it does on VHS."   
 Orange County Badd Bunny Breakout. In 2013, Taylor and Beins directed a music video for the song in La Habra, California, featuring the Chicken Heads and numerous cameos from local musicians, including Dukey Flyswatter of Haunted Garage. The video premiered on September 3, 2013. 

A digital soundtrack album for the film, featuring both the original score done by Tim Johnson and songs from punk, rock and metal bands, was released on the online music store Bandcamp on May 10, 2012.  In 2012, another music video was produced and made for the song "Rock & Bowl" which features all the producers of Atom under the persona band Bolonium. 

On May 28th 2014 it was announced that the film will be released on DVD officially by Whacked Movies early 2015.

==References==
 

==External links==
* 
* 
*  at  

 
 
 
 
 
 
 
 
 