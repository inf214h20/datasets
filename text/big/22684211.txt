The Yellow Handkerchief (1977 film)
 
{{Infobox film
| name = The Yellow Handkerchief
| image = The Yellow Handkerchief.jpg
| caption = Theatrical poster to The Yellow Handkerchief (1977)
| director = Yoji Yamada  
| producer = 
| writer = Yoshitaka Asama & Yoji Yamada
| starring = Ken Takakura
| music = Masaru Sato
| cinematography = 
| editing = 
| distributor = Shochiku
| released =  
| runtime = 108 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}} Japan Academy Prize.  The film was inspired by a column series written by American journalist Pete Hamill for the New York Post in 1971. 

==Cast==
* Ken Takakura: Yusaku Shima
* Chieko Baisho: Mitsue Shima
* Tetsuya Takeda: Kinya Hanada
* Kaori Momoi: Akemi Ogawa
* Hachirō Tako
* Hisao Dazai: Ryokan Manager
* Mari Okamoto: Ramen Shop Girl
* Kiyoshi Atsumi: Watanabe Kachō

==Adaptations==
*Yathra|Yathra(1985) The Yellow Handkerchief(2008)

==Bibliography==
*  
*  
*  
*  
*  

==References==
 

==External links==
*  

 
{{Navboxes  title = Awards  list =
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 


 