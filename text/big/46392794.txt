Emperor's Ball
{{Infobox film
| name = Emperors Ball
| image = Kaiseball screenshot.JPG
| image_size =
| caption =
| director = Franz Antel
| producer = Franz Hoffmann
| writer = Jutta Bornemann   Karl Leiter
| narrator =
| starring = Sonja Ziemann   Rudolf Prack   Hannelore Bollmann Hans Lang   Heinz Musil   Lotar Olias 
| cinematography = Hans Heinz Theyer    
| editing =  Arnfried Heyne 
| studio =  Neusser-Film 
| distributor = Gloria Film
| released =  10 September 1953
| runtime = 95 minutes
| country = Austria   German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Emperors Ball (German:Kaiserball) is a 1956 Austrian drama film directed by Franz Antel and starring Sonja Ziemann, Rudolf Prack and Hannelore Bollmann. The film is part of a cycle of films set during the old Austro-Hungarian Empire.  It was shot in agfacolor with sets designed by Otto Pischinger.

==Cast==
* Sonja Ziemann as Franzi  
* Rudolf Prack as Reichsgraf Georg von Hohenegg  
* Hannelore Bollmann as Prinzessin Christine  
* Maria Andergast as Fürstin zu Schenckenberg  
* Jane Tilden as Gräfin Reichenbach  
* Ilse Peternell as Direktrice  
* Bully Buhlan as Graf Baranyi  
* Hans Olden as Erzherzog Benedikt   Rolf Olsen as Jean Müller  
* Paul Löwinger as Portier Bichler  
* Thomas Hörbiger as Willi  
* C.W. Fernbach as Offizier  
* Raoul Retzer as Kriminalkommissär   Hans Moser as Portier Rienössl

== References ==
 

== Bibliography ==
* Jennifer M. Kapczynski & Michael D. Richardson. A New History of German Cinema. Boydell & Brewer, 2014.

== External links ==
*  

 
 
 
 
 
 
 
 

 