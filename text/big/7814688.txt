Thunderbolt!
{{Infobox film
| name = Thunderbolt!
| image = ThunderboltDVD.jpg
| caption =
| director = Capt John Sturges and Lt Col William Wyler
| writer = Lester Koenig
| starring = Col James Stewart Lloyd Bridges
| music = Cpl Gail Kubik
| producer = Carl Krueger Productions U.S. Army Air Corps
| distributor = Monogram Pictures Corporation
| released = 26 July 1947 US
| runtime = 44 minutes US Italy France English
| color = Technicolor
}} 1947 film Axis supply Gustav Line Anzio beachhead.
 RAF mission, and indeed belongs to all people who desire freedom.
 Gustav line, and the mission to cut off the supply lines by destroying bridges and roads in northern Italy.

Next the film follows the airmen through the tense moments before the flight, and the long journey to the mainland while flying in formation. The pilots are shown finding their target, a bridge, and successfully taking it out; then they go on independent "strafing" activities, finding trains, lighthouses, anything that could be used by the enemy and destroying it.

When the pilots return, the film shows how the airmen try to relax in the makeshift community in Corsica; but it also takes a melancholy look into how some of them are getting along emotionally, thinking of what else they could be doing with "the best years of their lives".

Thunderbolt! ends when the Allies set free Rome in May 1944. The narrators note that it is the "evening" of the mission in Corsica, but not the end of the war. At the end the words "THE END" appear... behind a superimposed red question mark.

== See also ==
*List of Allied propaganda films of World War II

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 