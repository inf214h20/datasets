The Only Son (1914 film)
{{Infobox film
| name           = The Only Son
| image          = Theonlyson 1914 - newspaper photo.jpg
| caption        = Publicity photo published in a contemporary newspaper. William C. de Mille Thomas N. Heffron
| producer       =
| writer         =
| based on       =  
| starring       = James Blackwell
| cinematography =
| editing        =
| distributor    = Famous Players-Lasky
| released       =  
| runtime        = 50 minutes
| country        = United States 
| language       = Silent with English intertitles
}}
 silent drama film directed by Oscar Apfel and Cecil B. DeMille. The film is based on the play of the same name by Winchell Smith and stars James Blackwell.   

==Cast==
* James Blackwell as Thomas Brainerd, Sr.
* A. MacMillan as Henry Thompson
* Thomas W. Ross as Thomas Brainerd, Jr.
* Jane Darwell as Mrs. Brainerd
* Merta Carpenter as Gertrude Brainerd
* Arthur Collins as Jim Tompkins
* J.P. Wild as Charles Lester
* Fred Starr as Collins
* Milton Brown

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 