The Sawdust Paradise
{{Infobox film
| name           = The Sawdust Paradise
| image          = 
| alt            = 
| caption        = 
| director       = Luther Reed
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Julian Johnson Louise Long George Manker Watters 
| starring       = Esther Ralston Reed Howes Hobart Bosworth Tom Maguire George B. French Alan Roscoe Mary Alden
| music          = Gerard Carbonara
| cinematography = Harold Rosson
| editing        = Otho Lovering
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Luther Reed and written by Julian Johnson, Louise Long and George Manker Watters. The film stars Esther Ralston, Reed Howes, Hobart Bosworth, Tom Maguire, George B. French, Alan Roscoe and Mary Alden. The film was released on September 1, 1928, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9401E2DF113AE03ABC4F51DFBE668383639EDE|title=Movie Review -
  Oh Kay - THE SCREEN; The Victorious Evangelist. A British Picture. Other Photoplays. - NYTimes.com|work=nytimes.com|accessdate=12 February 2015}}  

==Plot==
 

== Cast ==
*Esther Ralston as Hallie
*Reed Howes as Butch
*Hobart Bosworth as Isaiah
*Tom Maguire as Danny
*George B. French as Tanner 
*Alan Roscoe as Ward
*Mary Alden as Mother
*J. W. Johnston as District Attorney 
*Frank Brownlee as Sheriff
*Helen Hunt as Organist 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 