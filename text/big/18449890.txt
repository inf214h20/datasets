Quest for the Mighty Sword
{{Infobox film
| name           = Ator 4: Quest for the Mighty Sword
| image          = Ator 4 poster.jpg
| caption        = UK poster
| director       = Joe DAmato
| producer       = Carlo Maria Cordio
| writer         = Joe DAmato Donald OBrien
| editing        = Kathleen Stratton
| released       =  
| runtime        = 94 mins.
| country        = Italy/United States
| language       = English
}}
Quest for the Mighty Sword (aka Ator 4, and Troll 3) is the fourth and final film in the   which was the only film in the series not directed by DAmato. 

Joe DAmato was reportedly displeased with Brescias approach to his character, and so re-took control of the franchise in 1988. In 1990, DAmato released the final Ator film, Quest for the Mighty Sword, which featured the adventures of the Son of Ator. It was released in Europe and the United States under a variety of titles including: Ator III: The Hobgoblin (indicating DAmatos personal disregard for the previous film), Ator 4: Quest for the Mighty Sword, and Hobgoblin. Here, Eric Allan Kramer plays the son of Ator (who has been killed at the start of this film); this is the only film in the series not to feature Miles OKeeffe in the role.

"Fans" of DAmato often note that the "hobgoblin" from this film is a reused goblin suit from Troll 2.
In some communities, The film is also known as Troll 3 despite another film also known as Troll 3 having the same name and co-directed by DAmato.

==Synopsis==
When Ator is killed, many years later his son looks for his sword.

==Cast==
*Eric Allan Kramer as the Son of Ator

==Rating==
The film was rated PG-13 in the US.

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 


 