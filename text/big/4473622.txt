Images (film)
{{Infobox film
| name           = Images
| image          = Original movie poster for the film Images.jpg
| caption        = original film poster
| director       = Robert Altman
| producer       = Tommy Thompson
| writer         = Robert Altman  Susannah York  (book, In Search of Unicorns) 
| starring       = Susannah York René Auberjonois Marcel Bozzuffi Cathryn Harrison
| music          = John Williams Stomu Yamashta Hemdale Film Group Ltd.
| distributor    = Columbia Pictures
| released       =  
| runtime        = 101 min.
| language       = English
| budget         = $807,000
}}
Images is a 1972 British-American psychological thriller film directed by Robert Altman and starring Susannah York. The picture follows an unstable childrens literature|childrens author who finds herself engulfed in apparitions and hallucinations while staying at her remote vacation home.

==Plot==
Wealthy housewife and childrens author Cathryn (Susannah York) receives a series of disturbing and eerie phone calls in her home in London one dreary night. The female voice on the other end suggests mockingly to her that her husband Hugh (René Auberjonois) is having an affair.

Cathryns husband comes home, finding her in complete disarray. Hugh attempts to comfort her, but then he is gone, and she sees a different man who is behaving as if he were her husband. She screams in horror and backs away, only to see her vision of the figure revert to her husband. 

Hugh attributes her outburst to stress and her budding pregnancy. He decides to take a vacation to the countryside at an isolated cottage. But as she dwells there, Cathryn delves into darker delusions as the stranger returns, and she finds it difficult to determine what is reality and what is in her mind.

==Cast==
* Susannah York as Cathryn Rene Auberjonois as Hugh
* Marcel Bozzuffi as Rene
* Hugh Millais as Marcel
* Cathryn Harrison as Susannah

==Awards==
* 1972 New York Film Critics Circle
** Nominated - Best Actress (Susannah York)
* 1972 Cannes Film Festival Best Actress (Susannah York)   
* Nominated - Golden Palm (Robert Altman)
* 44th Academy Awards Best Music, Original Dramatic Score (John Williams)
* 26th British Academy Film Awards Best Cinematography (Vilmos Zsigmond)
* 30th Golden Globe Awards Best English-Language Foreign Film
* Writers Guild of America Awards 1972 Best Drama Written Directly for the Screen (Robert Altman)

==See also==
*Twist ending
*Schizophrenia
*Surrealism

==References==
 

==External links==
*  
*  
*   by Roger Ebert
*   by Howard Thompson

 

 
 
 
 
 
 
 
 
 
 