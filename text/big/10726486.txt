Junoon (1978 film)
 
 
{{Infobox film
| name           = Junoon
| image          = Junoon_film.jpg
| image_size     =
| caption        = DVD cover
| director       = Shyam Benegal
| producer       = Shashi Kapoor
| writer         = Shyam Benegal (screenplay) Satyadev Dubey (dialogues) Ismat Chugtai (dialogues) Ruskin Bond (novella, A  Flight of Pigeons)
| narrator       = Amrish Puri
| starring       = Shashi Kapoor Shabana Azmi Jennifer Kendal Naseeruddin Shah
| music          = Vanraj Bhatia
| cinematography = Govind Nihalani
| editing        = Bhanudas Divakar
| distributor    =
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Hindi English
| budget         =
}}
Junoon ( : The Obsession) is a critically acclaimed and award-winning 1978 Indian Hindi language film produced by Shashi Kapoor and directed by Shyam Benegal. The film is based on Ruskin Bonds fictional novella, A Flight of Pigeons, set around the Indian Rebellion of 1857. The film also had award-winning soundtrack, composed by Vanraj Bhatia, and cinematography by Govind Nihalani.
 Kunal Kapoor, and Sanjana Kapoor,

==Plot==

The story is set around the Indian Mutiny of 1857. Javed Khan (Shashi Kapoor) is a feckless feudal chieftain with a Muslim Pathan heritage, whose world revolves around breeding carrier pigeons. His younger brother-in-law, Sarfaraz Khan (Naseeruddin Shah) is politically awakened and actively plots the fight against the British. Freedom fighters attack the local British administrators while they are in Sunday Worship at Church, massacring them all. Miriam Labadoor, Jennifer Kendal) manages to escape with her daughter, Ruth (Nafisa Ali) and mother. The three women seek refuge with the wealthy Hindu family of Lala Ramjimal (Kulbhushan Kharbanda). Lala is torn between his loyalty for India and his privileged position under the British. However, matters are taken out of his hand by Javed Khan who barges into Lalas house and forcibly takes Ruth and her family to his own house. This leads to jealousy on part of his wife, Firdaus (Shabana Azmi) and anger on part of his brother, who ultimately gives in to the Pathan tradition of offering hospitality and sanctuary even to uninvited guests. http://en.wikipedia.org/wiki/Nanawatai. Various situations ensue due to cultural misunderstandings in the domestic routine of the Muslim household with its new English guests. Javed falls in love with Ruth, and wants to marry her but is opposed bitterly by her mother.  Noticing intense feelings of Javed for her daughter Ruth, Miriam Labadoor (mother) cleverly makes an agreement with Javed that she would only give her daughter’s hand to Javed if British were defeated.  At first instance, Javed is hesitant but accepts the offer when again Miriam asks him if he has misgivings in his war against the British.  There are simmerings of a love affair under the watchful suspicious eyes of Firdaus.

Meanwhile the Rebellion runs into problems and the British are defeating the ill-organized Indian forces. In a stormy scene, Sarfaraz destroys Javeds pigeon coops and sets his pets free after he finds out that Indian forces have lost the Battle for Delhi. There is a delayed recognition by Javed of his subjugated identity, colonised by the British. Sarfaraz dies in a battle against the British. The Labadoors return to the protection of the re-deployed British contingent, smuggled by Firdaus, who only wants to save her marriage. Javed finds out that the Labadoors have sought sanctuary in the church and rushes there to meet Ruth one last time.  Surprisingly, Ruth comes out and expresses her feelings for Javed against her mother’s will.  However, Javed honourably keeps his word and the promise he had made with Miriam Labadoor and leaves the church without Ruth. The movie ends here with the voiceover that Javed was martyred fighting the British while Ruth and her mother return to England. Ruth dies fifty five years later, unwed.

===Cast===
* Shashi Kapoor as Javed Khan
* Nafisa Ali as Ruth Labadoor
* Jennifer Kendal as Miriam Labadoor (Ruths Mom)
* Naseeruddin Shah as Sarfaraz Khan, Javeds brother-in-law
* Shabana Azmi as Firdaus, Javeds wife
* Ismat Chughtai as Miriams Mom
* Kulbhushan Kharbanda as Lala Ramjimal
* Sanjana Kapoor Kunal Kapoor
* Karan Kapoor
* Benjamin Gilani as Rashid Khan
* Sushma Seth as Javeds aunt
* Tom Alter as Ruths father
* Amrish Puri as the Narrator
* Geoffrey Kendal
* Deepti Naval as Rashids wife
* Pearl Padamsee as bitter woman

==Awards== National Film Awards awards=== Best Feature Film in Hindi – Shashi Kapoor Best Cinematography – Govind Nihalani Best Audiography – Hitendra Ghosh

===1980 Filmfare Awards awards=== Best Film – Shashi Kapoor Best Director- Shyam Benegal Best Dialogues – Pandit Satyadev Dubey Best Editing – Bhanudas Divakar Best Cinematography – Govind Nihalani Best Sound Recording – Hitendra Ghosh

===1980 Filmfare Awards nominations=== Best Supporting Actor – Naseeruddin Shah Best Supporting Actress – Jennifer Kendal

===Recognition=== International Film Festival, New Delhi, 1979.
* Official Indian entry at the XIth Moscow International Film Festival.
* Featured at the Montreal World Film Festival 1979, the Cairo International Film Festival 1979, the Sydney Film Festival 1980 and the Melbourne International Film Festival 1980.

==Soundtrack==
{{Infobox album
| Name        = Junoon
| Type        = soundtrack
| Artist      = Vanraj Bhatia
| Cover       = Junoon_soundtrack.jpg
| Background  = gainsboro
| Released    = 1978
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = HMV
| Producer    = Kersy Lord
| Reviews     =
}}
 Sant Kabir.

# "Khusro rain piya ki jaagi pee ke sang" – Jamil Ahmad
# "Ishq ne todi sar pe qayamat" – Mohammad Rafi
# "Come live with me and be my love" – Jennifer Kendal
# "Ghir aayi kari ghata matwali sawan ki aayi bahaar re" – Asha Bhosle, Varsha Bhosle

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 