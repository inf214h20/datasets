Hanste Khelte
{{Infobox film
| name           = Hanste Khelte
| image          = HansteKheltey94.png
| image_size     =
| caption        = CD Cover
| director       = Bharat Rangachary
| producer       = Firoz Nadiadwala
| writer         = 
| narrator       =
| starring       = Rahul Roy   Nandini Singh   Ishrat Ali
| music          = Jatin Lalit
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Bollywood film directed by Bharat Rangachary and produced by Firoz Nadiadwala. It stars Rahul Roy, Nandini Singh (who made her debut with this film), Asrani, Rakesh Bedi, Anant Mahadevan  and Ishrat Ali in pivotal roles. Amitabh Bachchan gives his voice as a God in this film. {{cite web|title=Hanste Khelte CD-Compact Disc|url=http://www.ebay.com/itm/Hanste-Khelte-Music-Jatin-Lalit-Soundtrack-Melody-Made-in-UK/151454419013?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D27538%26meid%3D0f1f9befbe6541eb9a4ad7b9e075d25f%26pid%3D100005%26prg%3D11353%26rk%3D3%26rkt%3D6%26sd%3D151402151112&rt=nc
|publisher=ebay|accessdate=2014-11-30|}} 

==Cast==
* Rahul Roy... Rahul Chopra
* Nandini Singh...Pooja Verma
* Asrani...Namah
* Lisa Ray... Rekha (Special Appearance)
* Rakesh Bedi...Om
* Anant Mahadevan...Shivah
* Satyajeet... Amit
* Aparajita... Poojas maidservant
* Ishrat Ali...Devil

==Soundtrack==
{{Infobox album |  
 Name = Hanste Khelte |
 Type = Soundtrack |
 Artist = Jatin Lalit |
 Cover = |
 Released = 1994 |
 Recorded = | Feature film soundtrack |
 Length = |
 Label = |
 Producer = Jatin Lalit | 
 Reviews = |
}}
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "O Meri Rekha" Abhijeet
|-
| 2
| "O Yaara O Yaara"
| Kumar Sanu
|-
| 3
| "Diwana Dil Kahe"
| Suresh Wadkar, Sadhana Sargam
|-
| 4
| "Are Jaa Mujhe Na Sikha"
| Abhijeet Bhattacharya, Vijayta Pandit
|-
|}

==References==
 

==External links==
 

 
 
 
 


 