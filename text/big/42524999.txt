Itha Oru Snehagatha
{{Infobox film
| name           = Itha Oru Snehagatha
| image          = 
| caption        = Poster Vikram  Laila
| director       = Captain Raju
| writer         = 
| producer       = Varghese Kurian
| cinematography = Anandakuttan
| editing        = 
| dialogues      =  Deva
| distributor    = 
| studio         = Appus International
| released       =  
| runtime        = 135 minutes Malayalam
| country        = India
}}
 Malayalam romance Vikram and Laila in leading roles. It was dubbed and released in Tamil as Thrill in May 2002, shortly after the success of Dhill, in which Vikram and Laila had featured.

==Plot==
Church Father Daniel (Captain Raju) is appointed as the junior priest of the town church and subsequently tries to unite the warring population of Hindus and Christians in the town. The Christians are led by Kariyachan (K. P. A. C. Sunny) and Mathayichan (Rajan P. Dev), while the Hindus from the town are led by Raman Nair (Kollam Thulasi). Father Daniel forms a music troupe called Rhythm Orchestra  along with the cross-religion lovers Roy (Vikram (actor)|Vikram) and Hema (Laila Mehdin|Laila), trying to unite them. Meanwhile, Father Daniel faces stiff opposition from various quarters.

== Cast ==
* Captain Raju as Father Daniel Vikram as Roy Laila as Hema Manthra
* K. P. A. C. Sunny as Kariyachan
* Rajan P. Dev as Mathayichan
* Kollam Thulasi as Raman Nair
* Nedumudi Venu
* Thilakan
* N. F. Varghese
* Mamukkoya
* Jagannatha Varma
* J. V. Somayajulu
* Kaviyoor Ponnamma

==Release==
In 2002 following the success of the Vikram-Laila starrer Dhill (2001), Golden Cine Creations chose to dub the film into Tamil and release it to make most of Vikrams rising popularity.  When enquired about his opinion on his old films being dubbed and released, Vikram revealed he had no problem as such films had given him work during the struggling phase of his career. A song with lyrics featuring the titles of Vikrams other films was prepared for the Tamil version. 

== Soundtrack ==
{{Infobox album
| Name        = Itha Oru Snehagatha
| Lontype     = 
| Type        = Soundtrack Deva
| Deva
| Feature film soundtrack Deva
| Last album  = 
| This album  = 
| Next album  = 
}}

The soundtrack album is composed by Deva (music director)|Deva. Lyrics were penned by Kadhal Mathi. 

Tracklist (Tamil version)
{{track listing
| extra column    = Singer(s)
| lyrics_credits  = 
| total_length    = 

| title1       = Enathu Nenjil
| extra1       = Unnikrishnan, Ganga
| lyrics1      = 
| length1      = 

| title2       = Nenjangal
| extra2       = Unnikrishnan
| lyrics2      = 
| length2      = 

| title3       = Oonjal Pola Deva
| lyrics3      = 
| length3      = 

| title4       = Vidu Vidu
| extra4       = A. R. Reihana, Anuradha Sriram
| lyrics4      = 
| length4      = 

| title5       = Dhilluku Jodiya
| extra5       = Mohammed Beer
| lyrics5      = 
| length5      =
}}

== References ==
 

==External links==
* 

 
 
 