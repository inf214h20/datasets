Malombra (1942 film)
{{Infobox film
| name =  Malombra 
| image =Malombra (1942 film).jpg
| image_size =
| caption =
| director = Mario Soldati
| producer =  
| writer =  Antonio Fogazzaro (novel)    Mario Bonfantini   Renato Castellani   Ettore Maria Margadonna   Mario Soldati 
| narrator =
| starring = Isa Miranda   Andrea Checchi   Irasema Dilián   Gualtiero Tumiati Giuseppe Rosati 
| cinematography = Massimo Terzano
| editing = Giovanni Paolucci   Gisa Radicchi Levi    
| studio = Lux Film 
| distributor = Lux Film 
| released = 17 December 1942 
| runtime = 135 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} film of the same title. It was made at Cinecittà with sets designed by Gino Brosio.
 gothic melodrama, set in a castle on the edge of Lake Como during the Nineteenth century.

==Cast==
* Isa Miranda as Marina di Malombra 
* Andrea Checchi as Corrado Silla 
* Irasema Dilián as Edith Steinegge
* Gualtiero Tumiati as Il conte Cesare dOrmengo  
* Nino Crisman as Nepo Salvador  
* Enzo Biliotti as Il commendator Napoleone Vezza 
* Ada Dondini as Fosca Salvador  
* Giacinto Molteni as Andrea Steinegge  
* Corrado Racca as Padre Tosi  
* Luigi Pavese as Il professore Binda  
* Nando Tamberlani as Don Innocenzo  
* Doretta Sestan as Fanny  
* Paolo Bonecchi as Il dottor Pitour  
* Elvira Bonecchi as Giovanna, la governante  
* Giovanni Barrella as Lispettore della cartiera  
* Giacomo Moschini as Giorgio Mirovitch, il notaio  
* Anna Huala as La governante di Fosca

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 