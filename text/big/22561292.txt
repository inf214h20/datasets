Black Bridge
 
{{Infobox film
| name           = Black Bridge
| image          = File:Black_Bridge_84_Year_of_the_Banger.jpg
| caption        =  Kevin Doherty
| producer       = Kevin Doherty
| writer         = Kevin Doherty
| starring       = 
| music          = Goathorn Betrayer
| cinematography = Kevin Doherty Brett Howe
| editing        = 
| studio         = 
| distributor    = Winnipeg Film Group 
| released       =  
| runtime        = 108 minutes
| country        = Canada
| language       = English
| budget         = $6000 CDN
| gross          = 
}} Kevin Doherty. Brighton Rock, Trained Bears and the American group Geronimo!.

==Plot==
A close-knit group of headbanging friends all have an equal passion for Heavy Metal music, partying and experimenting with the occult and supernatural.  When the mutilated body of twelve-year-old Mikey Gay (brother of the Hellrats’ feared, trouble-making gang-leader Vinny) surfaces in Sammy’s “satanic enshrined” bedroom, all six become prime suspects in the murder.

==Cast==
*Adam Smoluk as Adrian Downing
*Raimey Gallant as Kathy Osbourne
*Jason Malloy as Clive DuBrow
*Mike Silver as Eddie Elliot
*Jennifer Pudavick as Tracey Roth
*James Clayton as Brian Gomer Young (as Clayton Champagne)
*Orlando Carriera as Sammy Rhoades
*Natasha Reske-Naurocki as Lisa
*David Stuart Evans as Mr. Simmons
*Spencer Maybee as Vinny Gay
*Mike Cunningham as Bruce Pug Pugnowski
*Zenon Hudyma as Blackie
*Kierin Kocourek as Mikey Gay
*Kelly Wolfman as Patricia Butler
*Alan MacKenzie as Neil Ward

== Reception ==
Canuxploitation was generally positive in their review of Black Bridge, writing "Though a few false acting notes are hit, Black Bridge still manages to weave several naturally unfolding stories into a decisively emotional narrative, even working in some very funny flashes of dark humor. A worthy effort deserving of a goat-horn salute or two."  The Winnipeg Free Press was mixed in their review, stating "I cant really give Black Bridge a big thumbs-up, but I can throw it a few devil-horns for the crazed fervour with which it descends into high school headbanging culture, circa 1984." 

== References ==
 

==External links==
*  
*  
*   at We Love Metal
*   at 1000 Bullets

 
 
 
 
 
 