The 4th Floor (2003 film)
{{Infobox film name     = Planta 4a image    = Planta 4a.jpg caption  = Theatrical release poster director = Antonio Mercero starring = Juan José Ballesta Alejandro Zafra Gorka Moreno Luis Ángel Priego Monti Castiñeiras country  = Spain language = Spanish
}}

Planta 4ª (  film directed by Antonio Mercero and starring Juan José Ballesta,  Alejandro Zafra, Gorka Moreno, Luis Ángel Priego, and Monti Castiñeiras. It was nominated for Best Picture in the 2004 Goya Awards.

==Plot==
A group of seriously ill boys live on the fourth floor of a hospital. The floor is their home and the other patients their family. Miguel Ángel (Ballesta) is the leader of the group, recently arrived Jorge (Zafra) is fearfully awaiting his test results and Dani (Moreno) is experiencing his first love affair.

==Cast==
*Juan José Ballesta as Miguel Ángel
*Alejandro Zafra as Jorge
*Gorka Moreno as Dani
*Luis Ángel Priego
*Monti Castiñeiras

==Awards and nominations==
===Won===
Montréal World Film Festival|Montréal Film Festival 
*Best Director (Antonio Mercero)

===Nominated=== Goya Awards Best Film (lost to Take My Eyes)

Spanish Actors Union
*Best Performance in a Minor Role &ndash; Male (Miguel Foronda)

==External links==
*  

 
 
 
 
 
 

 
 