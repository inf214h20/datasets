Häxan
 
 
 
{{Infobox film
| name           = Häxan
| image          = Haxan sv poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Swedish release poster
| director       = Benjamin Christensen
| screenplay         = Benjamin Christensen
| starring       = Benjamin Christensen Clara Pontoppidan Oscar Stribolt Astrid Holm Maren Pedersen
| cinematography = Johan Ankerstjerne
| editing        = Edia Hansen
| studio         = Svensk Filmindustri
| distributor    = Skandias Filmbyrå     Linked 2015-04-01 
| released       =  
| runtime        = 122 minutes    19 minutes   
| country        = Sweden Denmark Swedish intertitles SEK 2 million
}} documentary but contains dramatised sequences that are comparable to horror films.
 medieval scenes Swedish kronor.  Although it won acclaim in Denmark and Sweden, the film was banned in the United States and heavily censored in other countries for what were considered at that time graphic depictions of torture, nudity in film|nudity, and sexual perversion. Tybjerg, Casper, Et lille lands vagabonder 1920–1929, 100 Års Dansk Film, Rosinante Forslag, 2001, p. 68. ISBN 87-621-0157-9. 

==Plot==

===Part 1===
A scholarly dissertation on the appearances of demons and witches in primitive and medieval culture, a number of photographs of statuary, paintings, and woodcuts are used as demonstrative pieces. In addition, several large scale models are employed to demonstrate medieval concepts of the structure of the solar system and the commonly accepted depiction of Hell.

===Part 2===
A series of vignettes theatrically demonstrating medieval superstition and beliefs concerning witchcraft, including Satan (played by Christensen himself) tempting a sleeping woman away from her husbands bed and terrorizing a group of monks. Also shown is a woman purchasing a love potion from a supposed witch, and a sequence showing a supposed witch dreaming of flying through the air and attending a witches gathering.

===Part 3===
A long narrative broken up into several parts; set in the Middle Ages, it concerns an old woman accused of witchcraft by a dying mans family. The narrative is used to demonstrate the treatment of suspected witches by the religious authorities of the time. The old woman, after being tortured, admits to heavy involvement in witchcraft, including detailed descriptions of a Witches Sabbath, even going so far as to "name" other supposed witches, including two of the women in the dying mans household. Eventually, the dying mans wife is arrested as a witch when one of the clergymen accuses her of bewitching him.

===Part 4=== somnambulist and a kleptomaniac, the implication being that these behaviors would have been thought of as demonically-influenced in medieval times whereas modern times recognizes them as psychological ailments. There is heavy irony, however, in the observation that the "temperate shower of the clinic" i.e. the treatment of "hysterical women" in a modern institution, has replaced medieval solutions such as burning at the stake.

==Cast ==
Source: 
 
* Benjamin Christensen as the Devil
* Ella la Cour as Sorceress Karna
* Emmy Schønfeld as Karnas Assistant
* Kate Fabian as the Old Maid
* Oscar Stribolt as the Fat Monk
* Wilhelmine Henriksen as Apelone
* Astrid Holm as Anna
* Elisabeth Christensen as Annas Mother
* Karen Winther as Annas Sister
* Maren Pedersen as the Witch
* Johannes Andersen as Pater Henrik, Witch Judge
* Elith Pio as Johannes, Witch Judge
* Aage Hertel as Witch Judge
* Ib Schønberg as Witch Judge
* Holst Jørgensen as Peter Titta (in Denmark called Ole Kighul)
* Clara Pontoppidan as Sister Cecilia, Nun
* Elsa Vermehren as Flagellating Nun
* Alice OFredericks as Nun
* Gerda Madsen as Nun
* Karina Bell as Nun
* Tora Teje as the Hysteric Woman
* Poul Reumert as the Jeweller
* H.C. Nilsen as the Jewellers Assistant Albrecht Schmidt as the Psychiatrist
* Knud Rassow as the Anatomist
* Ellen Rassow as the Maid
* Frederik Christensen as a Citizen
* Henry Seemann as a Citizen
 

==Production==
After finding a copy of the Malleus Maleficarum in a Berlin bookshop, Christensen spent two years—from 1919 to 1921—studying manuals, illustrations and treatises on witches and witch-hunting.  He included a lengthy bibliography in the original playbill at the films premiere. He intended to create an entirely new film rather than an adaptation of literary fiction, which was the case for films of that day. "In   I am against these adaptations... I seek to find the way forward to original films." 

Christensen obtained funding from the large Swedish production company Svensk Filmindustri, preferring it over the local Danish film studios, so that he could maintain complete artistic freedom.  He used the money to buy and refurbish the Astra film studio in Hellerup, Denmark. Filming then ran from February through October 1921. Christensen and cinematographer Johan Ankerstjerne filmed only at night or in a closed set to maintain the films dark hue.  Post-production required another year before the film premiered in late 1922. Total cost for Svensk Film, including refurbishing the Astra Film Studio, reached between 1.5 and 2 million kronor, making Häxan the most expensive Scandinavian silent film in history. 

==Release==
The film premiered simultaneously in four Swedish cities—Stockholm, Helsingborg, Malmö and Gothenburg—on 18 September 1922,  something very unusual in Sweden at the time. The release length was 2506 metres (which equals 122 minutes at 18 Frame rate|fps, which was the intended frame rate, and 91 minutes at todays 24 fps). It received its Danish premiere in Copenhagen on 7 November 1922. 

==Re-releases==
The film was re-released in 1941 in Denmark with an extended introduction by Christensen. The intertitles were also changed in this version. 

In 1968, an abbreviated version of the film (77 minutes as opposed to the originals 104 minutes) was released, entitled Witchcraft Through the Ages. This version featured an eclectic jazz score by Daniel Humair (played by a quintet including Jean-Luc Ponty on violin and Daniel Humair on percussion) and dramatic narration by William S. Burroughs. 

On October 16, 2001, Häxan was released on DVD by The Criterion Collection. This release features a restored print of the original version of the film, as well as the 1968 Witchcraft Through the Ages version. Also featured are extensive production notes, a re-recorded musical score, commentary by Danish film scholar Casper Tybjerg, a gallery featuring the images used in the films first section, and the introduction Christensen recorded for the 1941 re-release. 

==Soundtracks==
 
The film has had several different soundtracks through the years. When it premiered in Sweden, it was accompanied by a score compiled from preexisting compositions. Details of the selection, which met with the directors enthusiastic approval, have been lost. It is probable, but not certain, that it was the same music as for the Copenhagen premiere two months later, which is known. In Copenhagen it was played by a 50-piece orchestra, and the selection, combining pieces by Franz Schubert|Schubert, Christoph Willibald Gluck|Gluck, and Ludwig van Beethoven|Beethoven, has been restored and recorded with a smaller ensemble by arranger/conductor Gillian Anderson for the 2001 Criterion Collection DVD edition. 
 Danish composer conductor Launy original score, but this has never been verified.
 Geoff Smith, to be performed on the hammered dulcimer. Smith performed the soundtrack throughout the UK in 2007.  The other was by the British group Bronnt Industries Kapital, and was also performed throughout the UK and Europe in 2007. A DVD of the film featuring both soundtracks was released by Tartan Films on September 24, 2007.

A third soundtrack to be recorded and released in 2007 was a score commissioned from silent film composer   by the Swedish Film Institute. This soundtrack is featured on the DVD release of the most recent restoration of the film, made by the Swedish Film Institute.
 The Häxan Score was released on CD in 2006.

The French experimental music group Art Zoyd also created a soundtrack that was released on CD in 1997. This work was commissioned by the City of Copenhagen, the European Union designated Cultural Capital of Europe 1996. Art Zoyd members Gérard Hourbette and Thierry Zaboïtzeff composed the soundtrack.

The American film scoring ensemble The Rats & People Motion Picture Orchestra premiered its new score for the film at the 2010 St. Louis International Film Festival.

A new soundtrack from the Dublin Film Ensemble was commissioned by the Horrorthon Festival in 2014 for presentation in the Irish Film Institute.

==Reception==
 

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*   at Danish Film Institute (in Danish)
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 