Polariffic
 
{{Infobox film
| image= 
| caption=  Rob Shaw
| producer= Marissa Weisman
| writer= Allan Neuwirth
| narrator= 
| starring= 
| music= Charles-Henri Avelange
| cinematography = 
| editing= Brent Heise
| studio= Bent Image Lab
| distributor= 
| released= 
| runtime=5  minutes
| country=United States
| language=English
| budget= 
| gross= 
}}
 Rob Shaw.

==Plot==
Taking place in the North Pole, "Polariffic" is a story about a foursome of Arctic friends that takes a journey from fear to friendship. Snowby a gentle polar bear, Jaz a be-bopping penguin, Flitter a light-on-her-feet artic fox and Cupcake a thoughtful baby seal are the best of friends, until one day they encounter Charlie, a young Yeti who tries to befriend them. At first terrified, the four friends find themselves face-to-face with the Yeti and must muster the courage to trust him in order to save themselves. In the end the gang learns appearances arent always what they seem.

==Accolades==
{| class="wikitable plainrowheaders"
|+  List of Awards and Nominations 
|-
! scope="col" style="width:4%;"| Year
! scope="col" style="width:25%;"| Award
! scope="col" style="width:33%;"| Category
! scope="col" style="width:33%;"| Recipients and nominees
! scope="col" style="width:5%;"| Results
|-
! scope="row" rowspan="4" style="text-align:center;"| 2014 42nd Annual Annie Awards  Annie Award Best Animated Special Production
|Polariffic
| 
|-
|}

==References==
 



 