Cándida
:For the general given name, see Candida (given name).
{{Infobox film
| name           =  Cándida
| image          =Candida 1939.jpg
| image_size     =
| caption        =
| director       = Luis Bayon Herrera
| producer       = Luis Bayon Herrera
| writer         = Luis Bayon Herrera
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1939
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}} Argentine musical film drama directed by Luis Bayon Herrera. The tango film premiered in Buenos Aires and starred Juan Carlos Thorry.

==Cast==
* Niní Marshall ...  Cándida 
* Augusto Codecá ...  Jesús 
* Juan Carlos Thorry ...  Dr. Adolfo Sánchez 
* Tulia Ciámpoli ...  Esther 
* César Fiaschi ...  Dr. Luis Giménez 
* Adolfo Stray ...  Jacobo 
* Nélida Bilbao ...  La dactilógrafa 
* Lita Fernand ...  Julia 
* Chiche Gicovatte ...  Julia 
* Cielito ...  Pepito 
* S. Tortorelli ...  Pepito 
* L.S. Pereyra ...  Augustito 
* Pedro González ...  El médico 
* Armando Durán ...  Agenciero 
* A. Porzio ...  El patotero

==External links==
* 
 
 
 
 
 
 
 
 
 


 
 