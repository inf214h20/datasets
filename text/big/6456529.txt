Bobbili Puli
{{Infobox film
| name           = Bobbili Puli
| image          = Bobbili Puli.jpg
| image_size     = 
| caption        = 
| director       = Dasari Narayana Rao
| producer       = Vadde Ramesh
| writer         = Dasari Narayana Rao
| narrator       = 
| starring       = N.T. Rama Rao Sridevi jayachitra  Kongara Jaggaiah Kaikala Satyanarayana Rao Gopal Rao Murali Mohan Jayachitra Prabhakar Reddy Allu Ramalingaiah Prasad Babu
| music          = J. V. Raghavulu
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1982
| runtime        = 
| country        = India Telugu
| budget         = 
}} Telugu action film|action-drama film starring  N.T. Rama Rao (NTR) and Sridevi in the lead roles. Kongara Jaggaiah, Kaikala Satyanarayana and Rao Gopal Rao played supporting roles.

In the film, NTR played the role is a Military Officer who later on turns into a flamboyant character in order to remove anti-social elements present in the society. Sridevi played the role of a lawyer and was NTRs love interest in the film. This film is produced by vadde ramesh. written and directed by Dasari Narayana Rao. The film celebrated silver jubilee. The story and the power of NTRs character in the film motivated him to take the political path. The film was remade in Hindi as Zakhmi Sher with Jeetendra.

==Cast==
* Nandamuri Taraka Rama Rao ...  Major Chakradhar
* Sridevi
* Kongara Jaggaiah ...  Gopinath
* Kaikala Satyanarayana ...  Sanyasi
* Rao Gopal Rao ...  Bhanoji Rao
* Jayachitra
* Murali Mohan
* M. Prabhakar Reddy
* Prasad Babu
* Allu Ramalingaiah

==Crew==
*Direction, dialogues, screenplay, story : Dasari Narayana Rao
*Production : Vadde Ramesh
*Original Music : J. V. Raghavulu
*Playback singers : S. Janaki, S. P. Balasubramaniam P. Susheela
*Choreographer : P. A. Saleem

==Box-office==
*The film had a 175-day run. 

==References==
 

==External links==
*  

 
 
 
 
 

 