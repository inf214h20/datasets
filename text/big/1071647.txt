Something Wild (1986 film)
{{Infobox film
| name = Something Wild
| image = SomethingWildPoster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Jonathan Demme
| producer = Jonathan Demme Kenneth Utt
| writer = E. Max Frye
| starring = Jeff Daniels Melanie Griffith Ray Liotta Laurie Anderson Steve Jones
| cinematography = Tak Fujimoto Craig McKay
| distributor = Orion Pictures  (Sony Pictures Entertainment) 
| released =  
| runtime = 113 minutes
| country = United States
| language = English
| budget = 
| gross = $8,362,969 
}} action comedy film directed by Jonathan Demme and starring Melanie Griffith, Jeff Daniels and Ray Liotta. It was screened out of competition at the 1987 Cannes Film Festival.     This film has some elements of a road movie, and it has acquired a certain cult status.

==Plot==
In New York City, Charlie Driggs (Jeff Daniels) is a seemingly conventional banker whose wife has left him. In a café, an adorable brunette (Melanie Griffith) who calls herself Lulu spots him leaving without paying. After a teasing confrontation, the two leave in a car that, Lulu says, she acquired from a divorce.  They  embark on a bizarre adventure, including crashing and abandoning the car, stealing from a liquor store and leaving a diner without paying. Believing Charlie to be married, Lulu discloses her real name is Audrey, and takes him to visit her mother, Peaches. Audrey now adopts a different persona, becoming a demure platinum blonde. Coming to accept Audreys free-wheeling lifestyle, Charlie realises he is falling in love with her.

The relationship takes a dark turn when her violent ex-convict husband, Ray Sinclair (Ray Liotta), shows up at a high school reunion; Ray wants her back. Dumping his girlfriend Irene (Margaret Colin), Ray takes Audrey and Charlie on a short-lived crime spree. The trio end up in a motel room where Audrey learns Charlie is no longer happily married and, under duress, reluctantly realizes she has to stay with Ray.

Ray tells Charlie to leave, warning him to keep away, but Charlie secretly tails them when they leave the motel. Charlie devises a plan to extract Audrey from Rays grasp, and having done so, he takes Audrey to his home. Ray unexpectedly shows up and begins beating up Charlie, but in a scuffle Charlie accidentally stabs Ray, who dies. Audrey is taken away for questioning. Charlie later comes looking for Audrey at her apartment, but she has moved. Near the café where they first met, Audrey (in her third incarnation) appears with a station wagon. She invites Charlie into her car and back into her life.

==Cast==
* Jeff Daniels as Charles Driggs
* Melanie Griffith as Audrey Hankel, aka Lulu
* Ray Liotta as Ray Sinclair Charles Napier as Irate chef
* John Sayles as Motorcycle cop
* Tracey Walter as The Country Squire
* Gary Goetzman as Guido Paonessa
* Robert Ridgely as Richard Graves
* Buzz Kilman as TV Newscaster
* Adelle Lutz as Rose John Waters as Used car salesman
* Margaret Colin as Irene
* Jack Gilpin as Larry Dillman
* Su Tissue as Peggy Dillman
* Sister Carol as Dottie
* The Feelies as The Willies

==Reception==
Something Wild gained positive acclaim from critics. The film currently holds an 88% rating on Rotten Tomatoes based on 32 reviews, with the consensus reading: "Boasting loads of quirky charm, a pair of likable leads, and confident direction from Jonathan Demme, Something Wild navigates its unpredictable tonal twists with room to spare."

==Home media==
Something Wild was released on DVD on June 5, 2001. The film was presented in its original 1.85:1 widescreen aspect ratio. The only special feature was the original theatrical trailer. That edition is currently out of print.
 Criterion Collection DVD and Blu-ray Disc on May 10, 2011. The Blu-ray has a new, restored high-definition digital transfer, supervised by director of photography Tak Fujimoto and approved by director Jonathan Demme. It also features new video interviews with Demme and writer E. Max Frye, the original theatrical trailer, and a special booklet featuring an essay by film critic David Thompson. 

The films soundtrack was released as a CD, featuring only 10 of the 49 tracks in the title credits.  All the school reunion songs performed by The Feelies, including "Fame" and "Im a Believer", were omitted, and The Troggs "Wild Thing", which gave the film its title and which was sung in the convertible scene, was also left out.

==Accolades==
;1987 Golden Globe Awards Best Actor&nbsp; – Motion Picture Musical or Comedy: Jeff Daniels Best Actress&nbsp;– Motion Picture Musical or Comedy: Melanie Griffith Best Supporting Actor&nbsp;– Motion Picture: Ray Liotta

;1987 Edgar Awards Best Motion Picture Screenplay: E. Max Frye

==See also==
* List of American films of 1986
* Manic Pixie Dream Girl

==References==
 

==External links==
*  
*  
*  
*  
*  by David Thompson

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 