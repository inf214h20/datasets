Merci la vie
 
{{Infobox film
| name           = Merci la vie
| image          =
| caption        =
| director       = Bertrand Blier
| producer       = Jean-Louis Livi
| writer         = Bertrand Blier Catherine Jacob
| music          =
| cinematography = Philippe Rousselot
| editing        = Claudine Merlin
| distributor    = Canal+
| released       =  
| runtime        = 117 minutes
| country        = France
| language       = French
| budget         =
| gross          = $7,077,050 
}}

Merci la vie is a 1991 French film written and directed by Bertrand Blier. It won the César Award for Best Actor in a Supporting Role, and was nominated for Best Film, Best Actress, Best Supporting Actress, Best Director, Best Writing and Best Editing.

== Plot ==
Naive schoolgirl Camille Pelleveau meets the slightly older and more experienced Joëlle, a promiscuous woman who has just been thrown out of a car by her abusive boyfriend. Camille follows Joëlle as they go on a rampage where she discovers sex as they pick up men. Joëlle also shows Camille the darker side of life, as they start by crashing the mens cars and then decide to take on the whole town. However, medical researcher Dr. Marc Antoine Worms has invented a sexually transmitted disease and used Joëlle as a guinea pig by infecting her with it, so that he could become famous as the discoverer of its cure. Camille eventually learns about AIDS and fears she may have contracted the disease.

The story involves flashbacks, and in one sequence we learn that Camilles parents are feuding. Illogically, she tries to persuade them to reunite long enough for her conception to take place. The surreal plot and series of stylized scenes is in keeping with postmodern cinema, which challenges the notion of original creative thought.

== Cast ==
*Charlotte Gainsbourg as Camille Pelleveau
*Anouk Grinberg as Joëlle
*Michel Blanc as Raymond Pelleveau (Young Father)
*Jean Carmet as Raymond Pelleveau (Old Father)
*Annie Girardot as Evangéline Pelleveau (Old Mother)
*Jean-Louis Trintignant as SS Officer Catherine Jacob as Evangéline Pelleveau (Young Mother)
*Gérard Depardieu as Doctor Marc Antoine Worms
*Thierry Frémont as François
*François Perrot as Maurice, the Film Director
*Yves Rénier as Robert, the guard
*Didier Bénureau as 2nd Film Director
*Anouk Ferjac as Mother in clinic
*Philippe Clévenot as Producer
*Jacques Boudet as Craven, the Depositer

== References ==
 

== External links ==
*  
*  

 

 
 
 
 


 