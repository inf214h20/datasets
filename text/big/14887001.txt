The Legend of the Boy and the Eagle
 
{{Infobox film
| name           = The Legend of the Boy and the Eagle
| image          = The Legend of the Boy and the Eagle.jpg
| caption        =
| director       = Jack Couffer
| producer       = Hamilton Luske Joseph Strick
| writer         = Jack Couffer
| narrator       =
| starring       = Stanford Lomakema
| music          =
| cinematography = Ed Durden Jack Couffer
| editing        = Verna Fields Lloyd L. Richardson
| distributor    = Buena Vista Distribution Company
| released       =  
| runtime        = 48 min.
| country        = United States
| language       = English
| budget         =
}} Walt Disney film. A Hopi Indian boy is banished from his village after he defies tribal law and frees a sacred, sacrificial eagle. After surviving in the wilderness he returns to his village where he is again rejected. Fleeing the boy climbs a cliff and jumps off but before he reaches the ground turns into an eagle.

== Cast ==
* Stanford Lomakema as The Eagle boy
* Frank Dekova as Narrator (voice)

== External links ==
*  

 
 
 
 
 
 
 
 
 


 