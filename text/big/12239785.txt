Zhizn i priklyucheniya chetyrekh druzei 1/2
{{Infobox Film
| name =  Zhizn i priklyucheniya chetyrekh druzei 1/2
| originalname = Жизнь и приключения четырех друзей 1/2 
| director = Oleg Yeryshev
| writer = Yusef Printsev
| starring = Katya Kishmereshkina   Petr Shelokhonov   Mikhail Svetin   Yefim Kamenetsky   Lev Lemke
| music = Gennadi Gladkov Igor Tsvetkov
| cinematography = Roman Chernyak
| editing = G. Saidakovskaya
| distributor = -Russia- Ochakovo-Films - non-Russia - Enio Film 1980
| runtime = 63 min.
| country = Soviet Union Russian
}}
Zhizn i priklyucheniya chetyrekh druzei 1/2 ( ) (   , released later in 1981. Both films were re-edited in 1994, in St. Petersburg, Russia, and released on VHS and DVD, becoming a popular family entertainment for all ages.

== Synopsis ==
Three dogs and one cat are naturally suspicious of each other. At first the dogs and cat are playing various tricks with each other, and their thoughts are translated to the viewers by actor Lev Lemke. Eventually the four pets become good friends and have adventures together. They follow their owner, a forest ranger (played by Petr Shelokhonov), on various trips. With the help of a girl (played by Katya Kishmereshkina) three dogs and cat also help other people and have lots of fun together.

==Main cast==
* Katya Kishmereshkina
* Petr Shelokhonov
* Mikhail Svetin
* Yefim Kamenetsky
* Lev Lemke - as Translator of animals thoughts.
* Vladimir Bobin
* Valeri Bychenkov
* Valeri Doronin
* Andrei Khilko
* Nikita Yeryshev

==Animal actors==
* Cat Svetofor - played by an independent cat Vaska (no owner mentioned)
* Dog Bubrik - played by terriere Chingiz (dog owner - E. Sokolova)
* Dog Fram  - played by shepherd Yanko (dog owner - L. Ostretsova)
* Dog Toshka - as herself (dog owner - S. Pavlova)

==Crew==
* Director: Oleg Yeryshev
* Production director: Yu. Goncharov
* Writers:    Yusef Printsev, Oleg Yeryshev
* Cinematographers: Roman Chernyak, V. Ivanets
* Composers: Gennadi Gladkov, Igor Tsvetkov
* Art director: Konstantin Dmitrakov
* Editing: G. Saidakovskaya, L. Burtsova
* Second unit director: A. Ratnikov
* Sound: Iosif Minkov
* Music editor: A. Aristov
* Light: A. Zakharov, L. Yudina
* Animal trainers: L. Ostretsova, S. Pavlova, E. Sokolova.
* Production managers: E. Grigorenko, O. Krakhovskaya, E. Snyatovskaya

==Production Company==
* Gosteleradio
* Lentelefilm
* Ochakovo-Films

==Distributors==
* Gosteleradio (all formats, from 1980–1991) in the Soviet Union.
* Ochakovo-Films (all formats, after 1992) in Russia.
* Enio Film (after 1994, VHS video) (non-Russia, non-USA).

== Production ==
* Filming dates 1979 - 1980
* Filming locations: Lentelefilm studios, Leningrad, Leningrad oblast, Russia.
* Five additional animal trainers took part in filming.
* Three dogs and one cat were selected for filming after auditioning hundreds of dogs and cats with their owners.
* Dogs and cat "improvised" a lot before the camera, so many takes were made for each scene before getting good results for congruent editing.
* Dogs and cat made some unexpected moves during the filming, so the writer, Yusef Printsev, had to create additional lines for human actors and for translator of the animals thoughts (Lev Lemke).

==Release== Central Television, as well as in theatres in Leningrad, Moscow, and across Russia, as a prequel to Zhizn i priklyucheniya chetyrekh druzei 2. Both films also ran in theatres across the former Soviet Union and in Eastern Europe. In 1994 the film was re-edited in St. Petersburg, Russia for video release. That same year the film was released on VHS and DVD.

* The film and its sequel were also released in Germany in the 2000s (decade) under the title "Die Abenteuer der vier Strolche (1/2)" and the sequel "Die Abenteuer der vier Strolche (3/4)."

* This film is the first in a series of 8 episodes made between 1980 and 1994. Each episode has a different plot and variations in the cast of actors, but with the same three dogs and cat in the first 4 episodes. The first 4 episodes are paired (1/2 and 3/4) and edited as two movies for video release, now available on VHS and DVD in Europe and in the countries of the former USSR. It remains a popular family movie for all ages.

== Interesting facts ==
* Three dogs, named Bubrik, Fram and Toshka, and cat, named Svetofor, became really friendly with each other by the end of episodes 1/2.
* Dogs and cat also befriended actors and were cooperative with actors and director in the course of filming. Owners of dogs and cat also eventually became extras in the film.

== External links ==
 
*   (English)
* Zhizn i priklyucheniya chetyrekh druzei (Die Abenteuer der vier Strolche)   (German)
* Cast and synopsis for "Жизнь и приключения четырех друзей" (1980)   (Russian)
* Cast, crew, plot and other data for "ЖИЗНЬ И ПРИКЛЮЧЕНИЯ ЧЕТЫРЕХ ДРУЗЕЙ"   (Russian)
* Cast and crew for "Zhizn i priklyucheniya chetyrekh druzei"/Жизнь и приключения четырех друзей from Lentelefilm studio   (Russian)
* Zhizn i priklyucheniya chetyrekh druzei/Жизнь и приключения четырех друзей on Film.ru   (Russian)

 
 
 
 
 
 
 
 
 
 