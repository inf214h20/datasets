The Premature Burial (film)
 
{{Infobox film
| name           = The Premature Burial
| image          =
| caption        =
| director       = Roger Corman
| producer       = Roger Corman Samuel Z. Arkoff Gene Corman (Exec Prod)
| writer         = Short story:   Ray Russell
| narrator       = Heather Angel Richard Ney
| music          = Ronald Stein Les Baxter
| cinematography = Floyd Crosby
| editing        = Ronald Sinclair
| distributor    = American International Pictures
| released       =  
| runtime        = 81 min.
| country        = United States
| language       = English
| budget         =
| gross          = $1 million  172,329 admissions (France) 
}} Heather Angel 1844 short story of the same name by Edgar Allan Poe. It was the third in the series of eight Poe-themed pictures, known informally as the American International Pictures#List of Corman-Poe films|"Poe Cycle", directed by Corman for American International.

==Plot==
Set in the early dark Victorian-era 1830s or 1840s (also similar to Charles Dickens fiction of rain-soaked London streets), it follows Guy Carrell, who is obsessed with the fear of death. He is most obsessed with the fear of being buried alive. Though his fiancee Emily says he has nothing to be afraid of, he still thinks he will be buried alive (a common fear and in reality an occasional occurrence). So deluded, he seeks help from a few people, including his sister, but he still is haunted by the fear of death and the sense that someone close wants him dead.

==Cast==
* Ray Milland as Guy Carrell  Heather Angel as Kate Carrell, Guys sister 
* Hazel Court as Emily Gault, Guys wife
* Alan Napier as Dr. Gideon Gault
* Richard Ney as Miles Archer 
* John Dierkes as Sweeney 
* Dick Miller as Mole 
* Clive Halliday as Judson 
* Brendan Dillon as Clergyman

==Production== American International Pictures (AIP) starring the famous and preeminent horror and suspense star of the 1950s and 60s, Vincent Price.

He decided to make his own Poe film with financing through Pathe Lab. He wanted to use Price, but AIP had him under exclusive contract, so he cast instead Ray Milland. On the first day of shooting James Nicholson and Sam Arkoff of AIP turned up, announcing Corman was working for them - they had threatened Pathe with the loss of their business if they did not bring the movie back to AIP. Roger Corman & Jim Jerome, "How I Made a Hundred Movies in Hollywood and Never lost a Dime", Muller, 1990, page 83-84 

Francis Ford Coppola worked on the movie as dialogue director. Coppola Breaks the Age Barrier
Madsen, Axel. Los Angeles Times (1923-Current File)   02 Jan 1966: m6. 

==Reception== The Lost Weekend" movie), even if he fails to capture the manic intensity that Price brought to the other Poe films that he played or starred in. Cormans deft direction, employing a rich palette of colors and superb widescreen compositions, is on a par with the series finest installments." 

==Awards and nominations==
The film won a 1962 "Golden Laurel" - "Sleeper of the Year" Award.

==See also==
* Edgar Allan Poe in television and film

==References==
 

== External links ==
*  
*Roger Corman on   at Trailers from Hell
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 