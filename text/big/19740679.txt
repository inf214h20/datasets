Defying Gravity (2008 film)
Defying Gravity is a 2008 independent feature film produced by Balancing Act Pictures.  It was accepted to 11 film festivals and won 4 awards. It is distributed by Indie-Pictures.

== Plot == mute runaway girl, takes refuge in the Jewish cemetery in which her mother is laid to rest. She is watched over by Jorge, the cemetery caretaker, an illegal immigrant.
 VW bus, he becomes fascinated by the gothic cemetery waif and attempts to befriend her.

A sequence of events results in Cass getting lost. Jorge and Shore flee the cemetery in a stolen hearse to find Cassandra before her abusive stepfather does. All paths lead to Yermo, California - the site of a roadside diner and a feisty but compassionate transvestite waitress named Lola.


== Cast ==
*Alexandra Mathews as Cassandra
*Willam Belli as Lola
*Macauley Gray as Shore
*Mario Martinez as Jorge
*Tony Franchitto (credited as Anthony Franchitto) as Lubitch
*Andrew Torres as Menendez
*Bea Bernstein as Psychic Lady
*Sara Corder as Destry
*Michael Francesco as Sebastian
*Ellen Gedert as Social Worker
*Andrew Hawley as Mercurio
*Ariel James as Zoe
*Mike McNamee as Penlorion
*Liniana Montenegro as Ofelia
*Carlos Luis Orrala as Arnulfo
*Joey Rivas as Pedro
*Tara Saunders as Edwina
*Jane Shepherd as Miss Grace

==Production==
*Director: Michael Keller
*Writer: Lisa Savy James 
*Producer: Lisa Savy James
*Cinematography: Justin Talley
*Editor: Brian Voelkerding

==Film Festivals and Awards==
 
*Digital Video and Hi Def Festival 2008 - Best Story/Writing
*Fallbrook Film Festival 2008 - Directors Choice
*Action on Film International Film Festival 2008 2008 - Best LGBT Project
*Paso Robles Digital Film Festival 2008 - Festival Choice Award
*Wildwood By The Sea Film Festival 2008 - Best Narrative Feature
*Riverside International Film Festival 2008 - Official Selection
*Feel Good Film Festival 2008 - Official Selection
*Other Venice Film Festival 2008 - Official Selection
*La Femme Film Festival 2008 - Official Selection
*Red Rock Film Festival 2008 - Official Selection
*Beverly Hills Digital and Hi-Def Festival 2008 - Official Selection



== References ==
* , Dan Bennett, North County Times; 23 April 2008
* , Peter Surowski, The Temecula Valley News; 20 June 2008
*Defying the Odds: How a Middle-Aged Math Teacher Became an Indie Producer on Defying Gravity, Indie Slate Magazine; March/April 2008
* 
* 

==External links==
* 

 
 