Vanishing Point (1997 film)
 

{{Infobox film
| name           = Vanishing Point
| image          = Vanishing Point (1997 film).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Charles Robert Carner
| producer       = 
| writer         = Malcolm Hart (story) Guillermo Cabrera Infante (earlier screenplay)  Charles Robert Carner (teleplay)
| narrator       = 
| starring       = 
| music          = James Verboort
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} cult film Fox television network.   

The film stars Viggo Mortensen,  Jason Priestley, Peta Wilson, Christine Elise and Keith David and the same model 1970 Dodge Challenger R/T as in the original, and was directed by Charles Robert Carner.

== Synopsis == 426 Hemi powered 1970 Dodge Challenger R/T to Salt Lake City, Utah. On the way, hes informed that his wifes already-difficult pregnancy has taken a turn for the worse and continues to head back home to Idaho, refusing to stop for police when flagged down for speeding.
 libertarian DJ with a Gadsden flag in his studio, who is constantly giving homilies on things such as income taxes and government oppression. The Voice, intrigued by Kowalkskis run across country, sets out to find the truth about Kowalski. As he does so, he discovers the truth of Kowalskis "drug run" and that he is really rushing home to be with his wife and her now dangerous pregnancy.

Along the way, Kowalski runs into the desert where he gets lost, blows a tire, and spends the night in an Indian reservation. He finds his way back onto the road where he continues on his way to Idaho, tricking the FBI into going the opposite direction of his intended path. However, as the day rides on he falls asleep and drives into the salt flats. A woman on a motorcycle finds him just after he wakes up and informs him that he has damaged his oil pan. He follows her to her hideout where he meets her boyfriend who is at first suspicious that he is from the government. He is hiding from the Internal Revenue Service|IRS. After the girl and Kowalski convince him he is one of them, he offers to ride 30 miles to try and locate a replacement oil pan. He arrives back after successfully finding one but also warns Kowalski that the roads and intersections are teeming with police and agents. After they help Kowalski fit the pan they offer to help him with a plan to run a roadblock.

Kowalski successfully gets through the roadblock by having flashing police lights mounted on the roof and rushing at the roadblock; in the dimming light the police think its another police car and hurriedly let him through. However an officer fires a shot at the retreating Challenger and blows out the rear screen.  Afterwards Kowalski shuts off his headlights and vanishes into the woods, using night vision goggles he bought from the guy with the scanner. The police helicopter has infrared detectors but Kowalski evades being seen by hiding the car under a large piece of tin. He wakes up from a nightmare about his wife at 7:19 a.m., finds a phone booth and calls the hospital, speaking to the same doctor as previously.

Kowalski then drives down the road to where the movie began. Upon seeing the roadblock, he stops where we first see him in the beginning. A flashback reveals that his wife died at 7:19 a.m. after her kidneys failed. He then drives his car into the roadblock at full speed. An epilogue by "The Voice" reveals that although the authorities claim he died in the crash, the body was never found and that some witnesses claim he bailed out just before the crash and escaped authorities with the help of sympathetic onlookers.  A scenario is given where his former Mexican friend finds his tags and shows he is now living in the wilderness with his newborn daughter.

== Cast ==
* Viggo Mortensen as Jimmy Kowalski
* Christine Elise as Raphinia Kowalski
* Steve Railsback as Sergeant Preston
* Rodney A. Grant
* Peter Murnik as Gilmore James MacDonald as James G. MacDonald
* Paul Benjamin
* Geno Silva as Mike Mas
* John Doe as Sammy
* Peta Wilson as Motorcycle girl
* Keith David as Warren Taft
* Jason Priestley as The Voice

== Contrast with original film ==
The bare bones of the plot of this remake are the same as the original but the number of major characters has grown.  Kowalski and the radio DJs characters have been changed significantly and the plot is changed as a result. The DJ does not have the supernatural power to speak with Kowalski through the radio but instead seeks the truth about Kowalskis sketchy past. The Kowalski in the original actually drove for the pure love of speed borne out of a personal quest for freedom and to escape the ghosts of his past experiences. The remake however, gives Kowalski a more clear and socially accepted background and reason to drive fast. The Kowalski of the 1971 movie was an archetypal antihero, set against the backdrop of the fringe-dwelling people of the 1960s and early 1970s.  The 1997 TV remake replaces the lead characters ambivalent image with a simpler and more palatable, wholesome lead character and motivations, in particular eliminating all references to drug use, rebellion or sexuality, all of which were hallmarks of the 1971 film.

== See also ==
*Vanishing Point (1971 film)|Vanishing Point (1971 film) 
*Death Proof
*Grindhouse (film)|Grindhouse

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 