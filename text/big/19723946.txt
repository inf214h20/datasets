Fear(s) of the Dark
{{Infobox film
| name           = Fear(s) of the Dark
| image          = Fearsofthedark.jpg
| caption        =  Charles Burns Marie Caillou Pierre di Sciullo Lorenzo Mattotti Richard McGuire
| producer       = Valérie Schermann Christophe Jankovic
| writer         = Jerry Kramsky Michel Pirus Romain Slocombe Blutch Charles Burns Pierre di Sciullo
| narrator       = Nicole Garcia
| starring       = Aure Atika Guillaume Depardieu Louisa Pili François Creton Christian Hecq Arthur H
| music          = 
| cinematography = 
| editing        = 
| distributor    = Diaphana Films
| released       =  
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}} horror anthology film on the subject of fear  written and directed by several notable comic book creators and graphic designers.  It premiered at the 2007 Roma Film Festival and released in France in February 2008. 

==Plot==
Throughout the film, the disembodied voice of a woman is heard; she express her fears through a monologue, making confessions from trivial anxiety, grotesque nightmares, to crippling sadness.
# The first story is a traditional animation written and directed by Blutch. It focuses on a sinister old man and his four vicious dogs, whom he restrains on leashes, as they trek through the countryside; one by one, the man releases each dog on a victim. Charles Burns. immense interest in insects. In his adolescence, he captures a mysterious human-shaped beetle; it escapes, and yet the boy seems haunted by it. Come maturity, the boy becomes infatuated with a promiscuous woman; he invites her to his house one night, whereupon she seems to develop an obsession with him after waking up with a deep gash on her arm the next morning.
# The third story is a two-dimensional anime animation written by Romain Slocombe and directed by Marie Caillou. It focuses on a meek girl living in rural Japan who is receiving clinical treatment for nightmares, her doctor being a scientist who sedates her to experience these nightmares again. In her nightmares, she is bullied by her sadistic classmates, and is also haunted by the ghost of a samurai.
# The fourth story is a traditional animation directed by Lorenzo Mattotti. It focuses on a young boy living in rural France. Having lost his uncle in a poaching trip, the boy confides in his friend, a mysterious orphan, who claims that the uncle could have been "mauled" by a beast from the sky. The friend disappears later on, and a professional ranger is hired to hunt down the alleged beast. A massive crocodile from the swamps is killed and displayed in the church, but the boy is haunted by shadows of his orphan friend.
# The fifth story is directed by Richard McGuire. The intervals are two-dimensional computer animations written and directed by Pierre di Sciullo. The story focuses on a burly mustached man finding refuge against a blizzard in a pitch-black abandoned house. As he settles in, paranoia and dark forces begin to creep in around the mans fear. He uncovers a photo album of a woman, possibly the mistress of the house, who seemed to have a tendency to "cut" people out of her life.

==References==
 

==External links==
*   at Prima Linéa Productions  
*    
*   at Metrodome Releasing (United Kingdom)
*   at IFC Films (United States)
*   at Madman Entertainment (Australia)
*   at Cinefantastique
*  
*  
*  
*  
*  
*   at Horror Movies

 
 
 
 
 
 
 
 
 
 
 
 
 
 