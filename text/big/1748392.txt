How I Won the War
 
 
{{multiple issues|
 
 
}}

{{Infobox film
  | name     = How I Won the War
  | image    = How I Won the War DVD cover.jpg
  | caption  = DVD cover for How I Won the War (John Lennon on cover)
  | director = Richard Lester		
  | producer = Richard Lester Patrick Ryan Charles Wood
  | starring = Michael Crawford John Lennon Roy Kinnear Jack MacGowran Michael Hordern Lee Montague Karl Michael Vogler
  | music    = Ken Thorne David Watkin
  | editing  = John Victor-Smith
  | studio   = Petersham Pictures
  | distributor = United Artists
  | released = 18 October 1967  (UK)    23 October 1967  (US) 
  | runtime  = 109 minutes
  | country = United Kingdom
  | language = English
  | budget   =
}} Patrick Ryan. Allied landings bridge on the Rhine at Remagen in 1945. The film was not critically well received.

==Principal character and plot==
The main character, Lieutenant Goodbody, is an inept, idealistic, naïve, and almost relentlessly jingoistic wartime-commissioned (not regular) officer. One of the main subversive themes in the film is the platoons repeated attempts or temptations to kill or otherwise rid themselves of their complete liability of a commander. In fact, with dead-weight heavy irony, while Lieutenant Goodbodys ineptitude and attempts at derring-do lead to the gradual demise of his entire unit, Goodbody survives, together with one of his charges who finishes the film confined to psychiatric care, and the units persistent deserter. In a heavy macabre device, each deceased soldier is replaced by a plastic model toy soldier, played by an actor in World War II uniform whose face is obscured by netting, underscoring Goodbodys lack of adult connection with his duties.

==Cast==
* Michael Crawford as Lieutenant Earnest Goodbody
* John Lennon as Gripweed
* Roy Kinnear as Clapper
* Lee Montague as Sergeant Transom
* Jack MacGowran as Juniper
* Michael Hordern as Grapple
* Jack Hedley as Melancholy Musketeer
* Karl Michael Vogler as Odlebog
* Ronald Lacey as Spool
* James Cossins as Drogue
* Ewan Hooper as Dooley
* Alexander Knox as American General
* Robert Hardy as British General
* Sheila Hancock as Mrs. Clappers Friend

==Production==
Filming took place at the Bergen-Hohne Training Area, in Verden an der Aller, in Achim and in Spain in Almería Province in the autumn of 1966.  Lennon, taking a break from the Beatles after nearly four years of constant touring, was asked by Lester to play Musketeer Gripweed.  To prepare for the role, Lennon had his hair trimmed down, contrasting sharply with his mop-top image.  During filming, he started wearing round "granny-like" glasses, which soon became his sartorial trademark.  A photo of Lennon in character as Gripweed found its way into many print publications, including the front page of the first issue of Rolling Stone, released in November 1967.

During his stay in Almería, Lennon had rented a villa called Santa Isabel, whose wrought-iron gates and surrounding lush vegetation bore a resemblance to Strawberry Field, a Salvation Army garden near Lennons childhood home; it was this observation that inspired Lennon to write "Strawberry Fields Forever" while filming.

From 28–29 December 1966, Lennon recorded all dubbing (filmmaking)|post-synchronisation work for his character in the film at Twickenham Film Studios in London, England.

The films release was delayed by six months as Richard Lester went on to work on Petulia (1968), shortly after completing How I Won the War.

==Narrative and themes== Charles Wood, borrowed themes and dialogue from his surreal and bitterly dark (and banned) anti-war play Dingo. In particular the character of the spectral clown Juniper is closely modelled on the Camp Comic from the play, who likewise uses a blackly comic style to ridicule the fatuous glorification of war. Goodbody narrates the film retrospectively, more or less, while in conversation with his German officer captor, Odlebog, at the Rhine bridgehead in 1945. From their duologue emerges another key source of subversion – the two officers are in fact united in their class attitudes and officer-status contempt for (and ignorance of) their men. While they admit that the question of the massacre of Jews might divide them, they equally admit that it is not of prime concern to either of them. Goodbodys jingoistic patriotism finally relents when he accepts his German counterparts accusation of being, in principle, a Fascist. They then resolve to settle their disagreements on a commercial basis (Odlebog proposes selling Goodbody the last intact bridge over the Rhine; in the novel the bridge is identified as that at Remagen) which could be construed as a satire on unethical business practices and capitalism. This sequence also appears in the novel. Fascism amongst the British is previously mentioned when Gripweed (Lennons character) is revealed to be a former follower of Oswald Mosley and the British Union of Fascists, though Colonel Grapple (played by Michael Hordern) sees nothing for Gripweed to be embarrassed about, stressing that "Fascism is something you grow out of". One monologue in the film concerns Musketeer Junipers lament &ndash; while impersonating a high-ranked officer &ndash; about how officer material is drawn from the working and lower class, and not (as it used to be) from the feudal aristocracy.

==The Regiment== Patrick Ryan chose not to identify a real Army unit. The officers chase wine and glory, the soldiers chase sex and evade the enemy. The model is a regular infantry regiment forced, in wartime, to accept temporarily commissioned officers like Goodbody into its number, as well as returning reservists called back into service. In both world wars this has provided a huge bone of contention for regular regiments, where the exclusive esprit de corps is highly valued and safeguarded. The name Musketeers recalls the Royal Fusiliers, but the later mention of the "Brigade of Musketeers" recalls the Brigade of Guards. In the film, the regiment is presented as a cavalry regiment (armoured with tanks or light armour, such as the half-tracks) that has been adapted to "an independent role as infantry". The platoon of the novel has become a troop, a Cavalry designation. None of these features come from the novel, such as the use of half-tracks and Transoms appointment as "Corporal of Musket", which suggests the cavalry title Corporal of Horse. These aspects are most likely due to the screenwriter Charles Wood being a former regular army cavalryman.

==Comparison with the novel==
The novel – more subtle than the film though perhaps even more subversive – uses none of the absurdist/surrealist devices associated with the film and differs greatly in style and content. The novel represents a far more conservative, structured (though still comic) war memoir, told by a sarcastically naïve and puerile Lieutenant Goodbody in the first person. It follows an authentic chronology of the war consistent with one of the long-serving regular infantry units – for example of the 4th Infantry Division – such as the 2nd Royal Fusiliers, including (unlike the film) the campaigns in Italy and Greece. Rather than surrealism the novel offers some quite chillingly vivid accounts of Tunis and Cassino. Patrick Ryan served as an infantry and then a reconnaissance officer in the war. Throughout, the authors bitterness at the pointlessness of war, and the battle of class interests in the hierarchy, are common to the film, as are most of the characters (though the novel predictably includes many more than the film).

==Comparison with Candide== Leibnizian optimism of Candides Pangloss). Another frequently reappearing feature is Musketeer Clappers endless series of hopeless personal problems, invariably involving his wifes infidelities. Only the second of these recurring scenes is found in the novel, and in this case, unlike Candide, the optimism always comes from the innocent Goodbody (Candide), never Clapper.

==References==
 

==External links==
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 