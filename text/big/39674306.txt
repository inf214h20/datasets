Lady in the Iron Mask
{{Infobox film
| name           = Lady in the Iron Mask
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ralph Murphy
| producer       = Walter Wanger Eugene Frenke
| writer         = 
| screenplay     = Jack Pollexfen Aubrey Wisberg
| story          = Alexandre Dumas (novel)
| based on       = The Vicomte de Bragelonne
| narrator       = 
| starring       = Louis Hayward Patricia Medina Alan Hale, Jr.
| music          = Dimitri Tiomkin
| cinematography = 
| editing        = Ernest Laszlo
| studio         = Twentieth Century Fox
| distributor    = Twentieth Century Fox 
| released       = 1952
| runtime        = 78 mins
| country        = USA
| language       = English 
| budget         = 
| gross          = 
}} Steve Brodie Athos in this Three Musketeers adventure film, a reworking of Douglas Fairbanks 1929 screen epic The Iron Mask, an adaptation of the last section of the novel The Vicomte de Bragelonne by Alexandre Dumas, père, which is itself based on the French legend of The Man in the Iron Mask.
 The Man in the Iron Mask while Alan Hale, Sr. portrayed Porthos, the same part subsequently played by his lookalike son Alan Hale, Jr. in Lady in the Iron Mask thirteen years later.

==Cast==
Louis Hayward	 ...	
DArtagnan 
Patricia Medina	 ...	
Princess Anne / Princess Louise 
Alan Hale, Jr.	 ...	
Porthos 
Judd Holdren	 ...	
Aramis  Steve Brodie	 ...	 Athos  John Sutton	 ...	
Duke de Valdac 
Hal Gerard	 ...	
Philip of Spain 
Lester Matthews	 ...	
Prime Minister Rochard 
John Dehner	 ...	
Count de Fourrier

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 

 