Claude Duval (film)
 
 
{{Infobox film
| name           = Claude Duval 
| image          =
| caption        = George A. Cooper
| producer       = 
| writer         = Mary Bennett   Louis Stevens Hugh Miller  A.B. Imeson
| music          =  Henry Harris
| editing        =  Gaumont British 
| distributor    = Gaumont British Distributors
| released       = April 1924
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent adventure George A. Hugh Miller.  It is based on the historical story of Claude Duval.

==Plot==
In the seventeenth century a young Frenchman arrives in Britain and becomes mixed up in intrigue and ends up as a highwaymen.

==Cast==
* Nigel Barrie - Claude Duval 
* Fay Compton - Duchess Frances  Hugh Miller - Lord Lionel Malyn 
* A.B. Imeson - Lord Chesterton 
* Dorinea Shirley - Moll Crisp  James Knight - Captain Craddock  James Lindsay - Duke of Brentleigh 
* Betty Faire - Lady Anne 
* Charles Ashton - Tom Crisp 
* Tom Coventry - Mr Crisp 
* Stella St. Audrie - Mrs Crisp

==References==
 

 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 