Heat (2006 film)
{{Infobox film name = Heat image = Zhara poster.jpg caption = Russian release poster director =   producer = Fyodor Bondarchuk Alexander Rodnyansky Dmitry Rudovsky writer = Rezo Gigineishvili narrator = starring = Timur Yunusov Olga Bolbukh Mikhail Porechenkov music =   cinematography =   editing = Igor Litoninsky distributor = Channel STS released = December 28, 2006 runtime = 95 min. country = Russia language = Russian budget =      gross = US$15,710,000  (Russia)  
}} Russian teen teen romantic romantic comedy loosely based on the Walking the Streets of Moscow,    directed by   and produced by Fyodor Bondarchuk. Heat along with Wolfhound (2006 film)|Wolfhound became one of the most expensive Russian films in 2006.  Besides, its budget was three times less as compared to the advertising campaign.   

==Plot==
After three years a conscript sailor, Aleksey (Aleksey Chadov), comes back from the Black Sea Fleet to Moscow for a meeting with his waiting girlfriend, Masha ( ), and old classmates. Unfortunately, he finds out that his girlfriend is already married and has a child, then decides to spend the remaining time in a restaurant with his friends—an Business oligarch|oligarchs son, Konstantin (Konstantin Kryukov), a beginner actor, Artur (Artur Smolyaninov), and a Hip hop music|hip-hop artist, Timati. When their lunch is coming to the end, it appears that no one has money to pay the bill, except Konstantins dollars which do not accept for payment. The rapper is decided to exchange currency at the nearest money changer, that will cause troubles.
 nazi skinhead gang. Saving his own life, the hero hides in the Konstantins flat, where an Ostap Bender-like  swindler, Dani ( ), in waiting him. Meanwhile, his friends cannot wait anymore, so Artur is sent off. But before he could change the money, he is allured by the film director (Fyodor Bondarchuk) and prepares for shooting, but misses the actor-bus and leaves with gastarbeiters to demolish the Rossiya Hotel. Eventually, Konstantin hands over his last dollars to Aleksey, but he also failed after falling in love with a young traffic victim, Nastya ( ). After all that, despaired Konstantin makes a fuss in the restaurant, as a result he gets jailed by the police. Luckily for him, the mates could not abandon their friend and rescue him from captivity.

== Production==
Gigineishvili and Bondarchuk used the same young cast and crew from The 9th Company for their film, set in Moscow during a hot boiling summer, which caused as the title Heat instead of the draft entitled "City Tales" ("Сказки города").  Its filming took place in the shortest time period: "the script was written in ten days; pre-production took no more than two weeks; and after four months of shooting, the movie was done."  

==Reception==
 
Despite the fiscal success, Heat took in $15 million in the CIS, and about $1 million in Ukraine,  the film, mainly, received disapproving responses of critics for the commercial direction. According to the e-poll of Moskovskij Komsomolets, Heat was recognized as the worst Russian film of 2006. 

* 
* 
* 
* 
* 
* 
* 
* 
* 

===Awards and nominations===
{| class="wikitable"
|- style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- MTV Russia Movie Awards Best Kiss Aleksey Chadov and Agnija Ditkovskytė
| 
|- Best Comedic Performance Deni Dadaev
| 
|- Best Movie
|
| 
|- Best Female Performance Agnija Ditkovskytė
| 
|- Best Breakthrough Performance
|  and Agnija Ditkovskytė
| 
|-
|}

==Censorship in Ukraine== Ministry of Culture of Ukraine and the Security Service of Ukraine, forbidden to display 69 Russian films and TV series with Mikhail Porechenkov, including the film Heat. 

==Soundtrack==
{{Infobox album Name = ЖАRА: Soundtrack Type = Soundtrack Longtype = Artist = various artists Cover = Zhara soundtrack.jpg Released = February, 2007 Recorded = Genre = Pop music|Pop, R&B Length = 62:27 Label  = CD Land Producer =
}}
{{Album ratings
| rev1      = NEWSmusic
| rev1Score = (3/10)   
| rev2      = 
| rev2Score = 
}}
The original soundtrack was released in February, 2007 and included a song of  , several tracks from Timatis new album Black Star (Timati album)|Black Star and other musicians. 

===Track listing===
#"Когда ты плачешь" - TOKiO 4:15
#"Летняя Москва" - Karina Koks 3:42
#"Город ночных фонарей" - Timati &   3:21
#"Жара" - Timati, F. Bondarchuk, NASTY 4:21 Basta 3:55
#"Жара 77" - Centr 4:03
#"Детка" - Timati 4:24
#"Всё между нами" -   4:14 DJ Smash 2:26
#"Groovin" - VIP77 4:11
#"Black star" - Timati 4:57
#"Лето"	- Lomonosov (band) 4:17
#"Москва" - VIP77 3:33
#"Жара"	- DJ Smash 3:10
#"Where you gonna be" - VIP77 4:15
#"Happy New Year" - Siberia (band) 3:23

==References==
;Footnotes
 
;Notes
 

==External links==
*   
* 

 
 
 
 
 