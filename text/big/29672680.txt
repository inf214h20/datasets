Hécate
 
{{Infobox film
| name           = Hécate
| image          = 
| caption        = 
| director       = Daniel Schmid
| producer       = 
| writer         = Paul Morand Pascal Jardin Daniel Schmid
| starring       = Bernard Giraudeau
| music          = Carlos dAlessio
| cinematography = Renato Berta
| editing        = Nicole Lubtchansky
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France Switzerland
| language       = French
| budget         = 
}}

Hécate is a 1982 French-Swiss drama film directed by Daniel Schmid. It was entered into the 33rd Berlin International Film Festival.   

==Cast==
* Bernard Giraudeau as Julien Rochelle
* Lauren Hutton as Clothilde de Watteville
* Jean Bouise as Vaudable, consul de France
* Jean-Pierre Kalfon as Massard
* Gérard Desarthe as Le colonel de Watteville
* Juliette Brac as Miss Henry
* Patrick Thursfield as LAnglais
* Suzanne Thau as La tenancière du bordel
* Raja Reinking as La fille du bar
* Mustapha Tsouli as Ibrahim
* Teco Celio as Le capitaine Berta
* René Marc as Le ministre
* Ernst Stiefel as Le capitaine russe

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 