Architecture 101
{{Infobox film
| name           = Architecture 101
| image          = Architecture 101 film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Geonchukhakgaeron
| mr             = Kŏnch‘uk‘ak kaeron}}
| director       = Lee Yong-joo
| producer       = Shim Jae-myung Lee Eun
| writer         = Lee Yong-joo Suzy
| music          = Lee Ji-soo
| cinematography = Jo Sang-yoon
| editing        = Kim Sang-beom Kim Jae-beom
| studio         = Myung Films
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =         
}}
Architecture 101 ( ; lit. Introduction to Architecture) is a 2012 South Korean romance film written and directed by Lee Yong-joo. The film tells the story of two students who meet in an introductory architecture class and fall in love. Fifteen years later, the girl tracks down her first love to seek his help in building her dream house. 

== Plot ==
Seoul, the present day. Out of the blue, architect Lee Seung-min (Uhm Tae-woong) is approached by Yang Seo-yeon (Han Ga-in), whom he knew at college some 17 years ago, to design a new house for her on the site of her 30-year-old family home on Jeju island. Seung-min reluctantly agrees but cant come up with a design that pleases her. In the end, they decide to renovate and expand the existing house, and he and Seo-yeon spend a considerable amount of time together down in Jeju, to the growing annoyance of his fiancee Eun-chae (Ko Joon-hee), with whom he is soon to be married and move to the US. As Seo-yeon cares for her dying father (Lee Seung-ho) and Seung-min learns more about what became of Seo-yeon in the intervening years, he recalls their initial meeting at college in the early 1990s.

Seung-min and Seo-yeon (Lee Je-hoon and Bae Suzy|Suzy) had lived in the same neighborhood (Jeongneung-dong, Seoul) and attended the same architecture class. He remembers her liking rich student Jae-wook (Yoo Yeon-seok), his inability to declare his attraction to her, and the times being coached by his best friend, Nab-ddeuk (Jo Jung-suk) in how to get girls. Hoping to confess his feelings to her at the perfect timing, Seung-min asks Seo-yeon to meet him at the abandoned house they frequent, on the first day that it snows that coming winter. But one night he catches Jae-wook and a drunk Seo-yeon entering her house together. Fearing the worst, he ends his friendship with Seo-yeon due to his pain of believing she had chosen Jae-wook. The first day of snow arrives, and Seo-yeon is left waiting in the abandoned house alone. Heartbroken, she leaves behind her portable CD player with a CD of her favorite artist.

Seo-yeon in the present day receives that very same CD player and CD from Seung-min, meaning that he actually went to the house later and remembered their promise. But despite the bitter-sweetness of their first love, in the end, Seung-min still chooses his fiancee Eun-chae and flies with her to America, while Seo-yeon sits in the house he built for her, listening to the CD.   . Korean Film Biz Zone. May 30, 2012.   

== Cast ==
* Uhm Tae-woong as Lee Seung-min in the present
* Han Ga-in as Yang Seo-yeon in the present    
* Lee Je-hoon as Lee Seung-min in the past  Suzy as Yang Seo-yeon in the past     
* Yoo Yeon-seok as Jae-wook, Seo-yeons sunbae and Seung-mins rival 
* Jo Jung-suk as Nab-ddeuk, Seung-mins best friend
* Ko Joon-hee as Eun-chae, Seung-mins fiancee
* Kim Dong-joo as Seung-mins mother
* Lee Seung-ho as Seo-yeons father
* Kim Eui-sung as Professor Kang 
* Park Soo-young as Architect Koo
* Park Jin-woo as the taxi driver

== Production == Jeju Island. 

It was chosen as the closing film at the 15th Shanghai International Film Festival.    
 Typhoon Bolaven, regarded as the most powerful storm to strike the Korean Peninsula in nearly a decade, severely damaged Seo-yeons house that was constructed especially for the movie.  The house was rebuilt and renovated,  designed by architect Gu Seung-hoe (executive consultant of construction for the film), with the interior design by Woo Seung-mie (the films art director). It opened in March 2013 as a cafe, called Cafe Seo-yeons House. 

==Music==
The film reignited 1990s throwback fever among Koreans and made the fashion, music and celebrities of the period cool once again. Songs from the 90s, including duo  . May 3, 2012. 

And the movie’s impact on the present-day market was felt.   temporarily made a page on its official website that lists EPs from the 1990s alongside contemporary artists who are known to emulate the sentiments of the 1990s. 

==Release==
Architecture 101 was released in South Korean theaters on March 22, 2012. It was subsequently released in Hong Kong on October 22, 2012.   

===Home video===
On September 19, 2012, a two-disc limited edition DVD was released containing extra footage shot and edited by the director Lee Yong-joo, audio commentary by actors Uhm Tae-woong, Lee Je-hoon and Bae Suzy|Suzy,  interviews and trailers, and a book containing pictures and production images from the set. 

Among the deleted scenes are a flashback scene of Seung-min and Nab-ddeuk walking side-by-side; adult Seung-min taking a drunk Seo-yeon to her hotel room; and a longer, deeper kiss between Lee Je-hoon and Bae Suzy|Suzy. The scenes ended up on the cutting room floor because the director felt they did not fit the movies tone. 

==Reception==
The film captured viewers’ attention and earned critical plaudits with its restrained style and well drawn characters. 

It held the No. 1 spot at the box office for three weeks after its release, attracting over 1 million viewers in only eight days, passing 2 million in seventeen days, and reaching 3 million on April 18.      Male moviegoers are the backbone of the movie’s sales, an unusual path to success for a romantic drama.  Critics have said that Architecture 101 especially resonates with the nostalgia men feel for their first loves. 
 over 4.1 million admissions, a new box office record for Korean melodramas.   

== Awards and nominations ==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
! Ref.
|- 2012
| 48th Baeksang Arts Awards
| Best New Actor (Film)
| Lee Je-hoon
|  
| 
|-
| Best New Actress (Film) Suzy
|  
| 
|-
| Most Popular Actor (Film)
| Lee Je-hoon
|  
|rowspan=2|
|-
| Most Popular Actress (Film) Suzy
|  
|-
| rowspan="2"| 6th Mnet 20s Choice Awards
| 20s Male Movie Star
| Lee Je-hoon
|  
|rowspan=2| 
|-
| 20s Female Movie Star Suzy
|  
|-
|  5th Style Icon Awards
| First Love Fantasies Suzy
|  
|
|-
| rowspan="8"| 21st Buil Film Awards
| Best Film
| Architecture 101
|  
|rowspan=8|  
|-
| Best Supporting Actor
| Jo Jung-suk
|  
|-
| Best New Director
| Lee Yong-joo
|  
|-
| Best New Actor
| Jo Jung-suk
|  
|-
| Best New Actress Suzy
|  
|-
| Best Screenplay
| Lee Yong-joo
|  
|-
| Best Cinematography
| Jo Sang-yoon
|  
|-
| Best Music
| Lee Ji-soo
|  
|-
| rowspan="4"| 49th Grand Bell Awards
| Best Director
| Lee Yong-joo
|  
| rowspan=4|
|-
| Best Supporting Actor
| Jo Jung-suk
|  
|-
| Best New Actor
| Jo Jung-suk
|  
|-
| Best New Actress Suzy
|  
|-
|  32nd Korean Association of Film Critics Awards
| Best Music
| Lee Ji-soo
|  
| 
|- 33rd Blue Dragon Film Awards
| Best New Actor
| Jo Jung-suk
|  
|    
|-
| Best New Actress Suzy
|  
| rowspan=6|  
|-
| Best Screenplay
| Lee Yong-joo
|  
|-
| Best Cinematography
| Jo Sang-yoon
|  
|-
| Best Art Direction
| Woo Seung-mi
|  
|-
| Best Lighting
| Park Se-moon
|  
|-
| Best Music
| Lee Ji-soo
|  
|-
| Popularity Award Suzy
|  
|  
|-
|  20th Korean Culture and Entertainment Awards
| Best New Actor
| Jo Jung-suk
|  
|   
|- 2013
|  4th KOFRA Film Awards (Korea Film Reporters Association)
| Best New Actor
| Jo Jung-suk
|  
|  
|}

== References ==
 

== External links ==
*    
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 