Torrente, el brazo tonto de la ley
 
{{Infobox film
| name           = Torrente, el brazo tonto de la ley
| image          = Torrente 1 dvd cover.jpg
| writer         = Santiago Segura
| starring       = Santiago Segura Javier Bardem Javier Cámara Tony Leblanc Neus Asensi
| director       = Santiago Segura
| producer       = Andrés Vicente Gómez
| cinematography = Carles Gusi
| distributor    =
| released       =  
| editing        = Fidel Collados
| runtime        = 97 min. PST
| gross          = 1.500.000.000 PST
| language       = Spanish
}}
 dark comedy written, directed and starred by Santiago Segura. Also stars Javier Bardem, Javier Cámara, Neus Asensi and Tony Leblanc.

== Plot==
José Luis Torrente is a lazy, rude, drunkard, sexist, racist, far right|right-wing Madrid cop who lives in a decrepit apartment in a slum neighbourhood with his wheelchair-bound father, whose disability checks are Torrentes only real income.
 nymphomaniac niece of the family, Amparo. In order to get close to her, he befriends her nerdy weapon enthusiast cousin, Rafi, by taking him to target practice and on his nightly patrol rounds through the neighbourhood. During their patrols, Torrente begins to suspect that criminal activity is occurring in the new local Chinese restaurant. His suspicions are confirmed when his father accidentally overdoses after eating a stolen food roll which was filled with packets of heroin. Torrente decides to crack the drug ring in order to regain his former status within the Police Force.

Simultaneously, Torrente successfully attempts to seduce Amparo, who has sex with him after his fathers overdose. Amparos aunt, Reme, misreads her relationship with Torrente and believes that they are engaged.

Torrente and Rafi sneak into the restaurant at night and witness El Francés, the underboss of the drug trafficking outfit run by a mobster named Mendoza, torturing and executing a delivery boy named Wang, who had lost a shipment of the heroin (which in reality was unwittingly taken by Torrentes father) and they overhear that the outfit will soon be receiving a major drug shipment from a mobster known as Farelli. The pair accidentally make their presence known and flee the restaurant on Rafis fish delivery van while being chased by armed delivery boys.

Torrente enlists the help of Rafis equally nerdy friends: Malaguita, a martial artist, Bombilla, an electronics expert, and Toneti, a James Bond aficionado. The crew picks up Torrentes father from the hospital (while drunk) and then prepare a reconnaissance mission to discover the location of the drug deal. Toneti goes to the Chinese restaurant while wearing a wire but quickly blows his cover and winds up revealing Torrentes name to El Francés before trying to escape through a window and falling to his death.

El Francés and some of his goons raid Torrentes apartment but are attacked by Torrentes father, who wields a taser and some pliers, before the father suffers a heart attack and plummets down a flight of stairs. Nonetheless, they kidnap Amparo when she arrived to the apartment looking for Torrente.

After discovering his fathers death and Amparos kidnapping, Torrente becomes despondent but soon after Lio-Chii, Wangs girlfriend and a waitress at the Chinese restaurant who had once waited on a drunken Torrente, arrives and reveals the location of the drug deal, claiming she wants revenge for her boyfriends death.
 oral service to Mendozas men in a back room). Rafi gets cornered by Mendoza but hes rescued when Lio-Chii shoots him in the back.

In the aftermath of the shootout, Rafi and Malaguita get congratulated by police commissioner Cayetano for helping in bringing down one of the most vicious local drug rings and Rafi begins a relationship with Lio-Chii. Torrente gets taken away on an ambulance for his wounds. Cayetano sweeps the scene and discovers that the money is gone. In the ambulance speeding away, Torrente bribes the ambulance drivers (played by the comedy duo Faemino y Cansado) and flees to Torremolinos with the 50 million pesetas that he swiped while no one was watching.

== Cast ==
*Santiago Segura as José Luis Torrente
*Javier Bardem as Sultán
*Javier Cámara as Rafi
*Neus Asensi as Amparito
*Chus Lampreave as Reme
*Tony Leblanc as Torrente Padre
*Julio Sanjuán as Malaguita
*Jaime Barnatán as Toneti
*Darío Paso as Bombilla
*Carlos Perea as Carlitos
*Manuel Manquiña as El Francés
*Espartaco Santoni as Mendoza
*Rosa Zhidán as Lio-Chii Antonio de la Torre as Rodrigo
*Javier Jurdao as Israel
*Carlos Bardem as Cayetano
*César Vea as Borja
* Jake Nong as Wang

== Production Location==
*Leganés
*Madrid
*Móstoles
*Navacerrada
*San Sebastián de los Reyes

== Released Dates==
*Spain – 13 March 1998
*Portugal – February 1999
*France – 21 April 1999
*Hungary – 22 April 1999
*Argentina – 3 June 1999
*Iceland – 19 November 1999

== External links ==
* 
* 
 

 
 
 
 
 