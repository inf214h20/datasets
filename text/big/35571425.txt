Antiviral (film)
 
{{Infobox film
| name           = Antiviral
| image          = Antiviral (film).jpg
| caption        = Film poster
| director       = Brandon Cronenberg
| producer       = Niv Fichman
| writer         = Brandon Cronenberg
| starring       = Caleb Landry Jones
| music          = E.C. Woodley
| cinematography = Karim Hussain
| editing        = Matthew Hannam
| distributor    = Alliance Films
| released       =  
| runtime        = 110 minutes
| country        = Canada
| language       = English
| budget         = C$3.3 million 
| gross          = $61,808 
}}
Antiviral is a 2012 Canadian horror film directed by Brandon Cronenberg. The film competed in the Un Certain Regard section at the 2012 Cannes Film Festival.   Cronenberg re-edited the film after the festival to make it tighter, trimming nearly six minutes out of the film. The revised film was first shown at the 2012 Toronto International Film Festival,  and was a co-winner, alongside Jason Buxtons Blackbird (2012 film)|Blackbird, of the festivals Best Canadian First Feature Film award. 

==Plot== copy protection placed on the virus by the clinic; once injected in a client, the pathogen is rendered incommunicable. He then passes the pathogens to Arvid, who works at Astral Bodies, a celebrity meat market where meat is grown from the cells of celebrities for consumption.

After Derek is caught smuggling and arrested, Lucas asks Syd to take his place and harvest a pathogen from Hannah, who has recently fallen ill. Once he has taken a blood sample from Hannah, Syd quickly injects himself with some of her blood. He experiences the first symptoms, fever and disorientation, rapidly, and he leaves work early. His attempts to remove the virus copy protection are unsuccessful, and the console is destroyed during the process. When Syd awakens from severe delirium the next day, he discovers Hannah has died from the unknown disease, and all products harvested from her have skyrocketed in popularity. Desperate to fix his console, he approaches Arvid, who sets up a meeting with Levine, the leader of the piracy group. Levine offers to fix the console in exchange for samples of Syds blood, but Syd refuses. Levine subdues him and forcibly takes samples from Syd, as the pathogen which killed Hannah is now hotly in demand on the black market, and lethal pathogens are not legal to distribute.

The next day, a severely ill Syd is approached by two men, who drive him to an undisclosed location where Hannah is  still alive. Syd learns from Hannahs physician Dr. Abendroth that the virus infecting them both has been intentionally engineered with a security measure to prevent analysis, which explains why Syds console was destroyed. Hannahs death was fabricated to protect her, but she is shown to be in the extreme stages of the illness, extremely weak and bleeding from her mouth. Dr. Abendroth reveals to Syd that the virus is a modified version of an illness that Hannah has had before, and that he himself has an infatuation with her, having had samples of her skin grafted to his own arm. He further suggests that since Syd unnecessarily injected himself with Hannahs blood he too, is "just another fan." As Syds condition worsens, he returns to the Lucas Clinic, where he traces the original strain of the virus to Derek Lessing, who sold it to a rival company, Vole & Tesser. Dr Abendroth discovers that Vole & Tesser patented the modified strain, though their motives remain unclear.

Before Syd can proceed further, he is abducted by Levine and incarcerated in a room where his deterioration and death will be broadcast on reality television to sate Hannahs fans, who couldnt witness her death. As Syd begins to show the final symptoms, he escapes by spearing Levine in the mouth with a syringe and holding a nurse hostage with his infected blood. After Syd realizes that Vole & Tesser infected Hannah to harvest their own patented pathogen from her, Syd contacts Tesser and negotiates a deal. The film closes with a virtual reality version of Hannah that advertises her "Afterlife," exclusively from Vole & Tesser. Syd is shown working for Vole & Tesser, where Hannahs cells have been replicated to form a distorted cell garden from Astral Bodies celebrity meat technology. Viruses injected into her system are sold. Syd, infatuated, cuts her arm and drinks the flowing blood; Hannahs deformed face and body are revealed as he does so.

==Cast==
* Caleb Landry Jones as Syd March
* Sarah Gadon as Hannah Geist
* Malcolm McDowell as Dr. Abendroth Douglas Smith as Edward Porris
* Joe Pingue as Arvid
* Nicholas Campbell as Dorian
* James Cade as Levine
* Lara Jean Chorostecki as Michelle
* Lisa Berry as Receptionist
* Salvatore Antonio as Topp
* Elitsa Bako as Vera
* Mark Caven as Lucc

==Development==
Cronenberg has stated that the genesis of the film was a viral infection he once had. More precisely, the "central idea came to him in a fever dream during a bout of illness," wrote journalist Jill Lawless.  Cronenberg said, "I was delirious and was obsessing over the physicality of illness, the fact that there was something in my body and in my cells that had come from someone elses body, and I started to think there was a weird intimacy to that connection. And afterwards I tried to think of a character who would see a disease that way and I thought: a celebrity-obsessed fan. Celebrity culture is completely bodily obsessed - who has the most cellulite, who has fungus feet? Celebrity culture completely fetishizes the body and so I thought the film should also fetishize the body - in a very grotesque way." 

It was further shaped when he saw an interview Sarah Michelle Gellar did on Jimmy Kimmel Live!; what struck him was when "she said she was sick and if she sneezed shed infect the whole audience, and everyone just started cheering." 

==Filming==
The film was shot in Hamilton, Ontario and in Toronto. 

==Reception==
Antiviral has received mixed reviews, and holds a score of 55 out of 100 on review aggregator Metacritic, based on 18 reviews.  Rotten Tomatoes reports that 65% of critics gave the film a positive review with an average score of 5.9/10, based on 82 reviews.   Writing for Empire (film magazine)|Empire, Kim Newman called it "a smart, subversive but rather cold debut".  Variety (magazine)|Varietys Justin Chang stated that Brandon Cronenberg "has his own distinct flair for the grotesque", but that "the film suffers from basic pacing issues" and that "Antiviral never builds the sort of character investment or narrative momentum that would allow its visceral horrors to seriously disturb."  Stephen Garrett of The New York Observer described the film as "stomach-churning" and added that "the production, though impeccably polished and featuring an abundance of blood, spit, mucous, and pus, misses the mark with underdeveloped characters and a schematic plot. But boy, do those gory ideas and notions linger in the mind."  Peter Bradshaw of The Guardian was more negative, stating that "Brandon Cronenbergs movie is made with some technical skill and focus, but it is agonisingly self-regarding and tiresome." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 