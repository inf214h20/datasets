The Last Stand (1984 film)
 
{{Infobox film
| name           = Last Stand
| image          =
| caption        =
| director       = Tony Stevens
| producer       = John McLean
| writer         = Jim Barnes Don Walker (electronic organ|organ, piano) Phil Small (bass guitar|bass), Steve Prestwich (Drum kit|drums)
| music          = Cold Chisel
| cinematography = John Whitteron
| editing        = Tony Stevens
| distributor    = Warner Music Group
| released       = July 1984 (cinema), October 2005 (DVD)
| runtime        =
| country        = Australia
| language       = English
| budget         =
| gross          =
}} 
 rock band Cold Chisel. It was filmed on 13 and 15 December 1983 and released to cinemas in July 1984. A DVD version featuring extra footage was issued in October 2005.

==Track listing==
#"Standing On The Outside" Cheap Wine"
#"Rising Sun"
#"Janelle" Khe Sanh"
#"Twentieth Century"
#"You Got Nothing I Want"
#"Tomorrow"
#"Star Hotel"
#"Choirgirl (song)|Choirgirl"
#"Bow River"
#"Flame Trees" Saturday Night" Wild Thing"
#"Goodbye (Astrid Goodbye)"
#"Dont Let Go (Jesse Stone song)|Dont Let Go"

Bonus tracks from 2005 edition:

#"Only One"
#"River Deep Mountain High" Only Make Believe"
#Soundcheck: "Build This Love"

 

 
 
 
 
 


 