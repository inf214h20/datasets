A Chrysanthemum Bursts in Cincoesquinas
{{Infobox film
| name           = Un crisantemo estalla en cinco esquinas
| image          = Crisantemoestalla.jpg
| caption        = Theatrical release Poster
| director       = Daniel Burman
| producer       = Diego Dubcovsky
| writer         = Daniel Burman
| starring       = José Luis Alfonzo Pastora Vega Martin Kalwill
| music          = Antonio Tarrago Ros
| cinematography = Esteban Sapir
| editing        = Verónica Chen
| distributor    = BD Cine
| released       =   
| runtime        = 83 minutes
| country        = Argentina Brazil France Spain Spanish
| budget         =
}} Spanish film, written and directed by Daniel Burman, his first feature film. The picture was produced by Diego Dubcovsky.  It features José Luis Alfonzo, Pastora Vega, Martin Kalwill, among others.

Film critic Anthony Kaufman, writing for indieWIRE, an online community of independent filmmakers and aficionados, said Burmans A Chrysanthemum Burst in Cincoesquinas (1998) has been cited as the beginning of the "New Argentine Cinema" wave. 

==Plot==
The story takes place in South America at the turn of the 20th Century. As a child, Erasmo was left with a nurse by his parents, who had to escape a waging civil war.  Erasmo is now a grown man.  He has lost his parents, and now his foster mother is brutally murdered. He seeks to avenge her death, and the culprit is the landowner and head of state, El Zancudo. Erasmo befriends a poor Jew named Saul, who is prepared to help him in his undertaking. Along the way, Erasmo finds allies, adversaries, love, and then Magdalena.

==Cast==
* José Luis Alfonzo as Erasmo
* Pastora Vega as La Gallega
* Martin Kalwill as Saul
* Valentina Bassi as Magdalena
* Millie Stegman as La Boletera
* Walter Reyno as El Zancudo
* Roly Serrano as Cachao
* Ricardo Merkin as Doctor
* Aldo Romero as Lucio
* María Luisa Argüello as Elsa
* Sandra Ceballos as Mother
* Guadalupe Farías Gómez as Albina
* Antonio Tarragó Ross as Chamamecero

==Distribution==
The film was first presented at the Berlin International Film Festival on February 11, 1998. It opened in Argentina on May 7, 1998. It screened at the Muestra de Cine Argentino en Medellín, Colombia.

==Awards==
Wins
* Sochi International Film Festival,   Prize, Daniel Burman.

==References==
 

==External links==
*  
*  
*   at the cinenacional.com  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 