The Hole (2009 film)
{{Infobox film
| name           = The Hole
| image          = Holeposter2010.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joe Dante
| producer       = Claudio Fäh Michel Litvak Vicki Sotheran David Lancaster
| writer         = Mark L. Smith
| starring       = Chris Massoglia Haley Bennett Nathan Gamble Teri Polo Bruce Dern
| music          = Javier Navarrete
| cinematography = Theo van de Sande
| editing        = Marshall Harvey
| studio         = Bold Films  BenderSpink The Hole
| distributor    = Big Air Studios
| released       =  
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $10,362,785 
}} fantasy Horror thriller film directed by Joe Dante  and stars Teri Polo, Chris Massoglia, and Haley Bennett. 

==Plot==
 
Susan and her sons Dane and Lucas Thompson move from Brooklyn to Bensonville, in the countryside. Dane is upset with the constant changes of address and the family has lived in many cities. Lucas and Dane befriend their next door neighbor, the gorgeous Julie and the brothers find a bottomless hole in the basement of their house locked with several padlocks. They take the locks off and soon they are haunted by their darkest fears. Further, they believe that the hole might be a gateway to hell.

==Cast==
* Chris Massoglia as Dane Thompson 
* Haley Bennett as Julie Campbell 
* Nathan Gamble as Lucas Thompson 
* Teri Polo as Susan Thompson  
* Bruce Dern as Creepy Carl 
* Quinn Lord as Annie Smith 
* John DeSantis as Monster Dad 
* Merritt Patterson as Young Girl 
* Jessica Just as Pool Girl 
* Chelsea Ricketts as Whitney
* Chord Overstreet as Adam
* Dick Miller (uncredited Cameo appearance|cameo) as Pizza Delivery Guy

==Production==
The film began shooting in 3-D on December 5, 2008, in Vancouver, Canada. 

==Reception==
The film garnered positive reviews from critics. Film review aggregator Rotten Tomatoes reports that 80% of the critics gave the film a positive review, based on a sample of 34 reviews, with a rating average of 6.4 out of 10. 

==Release==
The film premiered on September 12, 2009, at 2009 Toronto International Film Festival  and had its theatrical release in summer 2010.  It screened at the Cannes Film Festival 2010.  The film had its United States debut screening at the Castro Theatre in San Francisco on October 7, 2011. 

On September 28, 2012, The Hole opened in theatres in Los Angeles and Atlanta. A release in St. Louis is scheduled for November. The film was released on DVD and Blu-ray Disc|Blu-ray on October 2, 2012. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 