Paid (2006 film)
{{Infobox film
| name           = Paid
| image          = paidposter1.jpg 
| director       = Laurence Lamers
| producer       = Silvester Slavenburg
| writer         = Laurence Lamers
| starring       = Anne Charrier Murilo Benício Tom Conti Guy Marchand Corbin Bernsen Beppe Clerici Fendi van Brederode Tygo Gernandt Hajo Bruins
| music          = Jaques Morelenbaum 
| cinematography = Tom Erisman
| editing        = Martyn Gould
| distributor    = Vivendi Entertainment Lightyear Entertainment
| sales agent    = Non Stop Sales
| premiere       = September 25, 2006
| runtime        = 91 min.
| country        = Netherlands Portuguese
| budget         = $1,500,000
}} 2006 English language feature film directed by Laurence Lamers. It was filmed in Netherlands between 2004 and 2005 with Anne Charrier, Murilo Benício, Tom Conti, Guy Marchand, Corbin Bernsen, Marie-France Pisier, Beppe Clerici and Tygo Gernandt.

== Synopsis ==
Paid is a film noir in the tradition of the French gangster movie. It is a story about underworld characters that have come to regret the choices they have made and now secretly long for a different and more meaningful life. It is also a love story between Paula Gireaux, a 28-year-old Parisian call girl working in Amsterdam and a hit man, Michel Angelo (30) whose hearts are touched when a six-year-old Bolivian boy abruptly enters their lives. The boy changes their fate. He gives them reason to reach for their dreams and to start a whole new life together.
But, at the moment Paula and Michel decide to escape the underworld, Paula realizes that her hands are tied to a paid deal she has made with a powerful English narcotics baron, Rudi Dancer (55). The enticing offer Paula could not refuse, turns into the demon that stands in the way of her future with Michel. Eventually, Michel and Paula find their way to freedom but not without paying a high price.

== Production == Dutch film industry as it was made without public funds, privately financed and it combined a mix of nationalities, which made it an international orientated film. The actors came from France, United States|USA, United Kingdom|Britain, Italy, Brazil, and The Netherlands. If we look to minor extra roles, also Peru, Colombia and Venezuela.
 Swedish company called NonStop Sales.

The period between production and completing the film in post production took a while, because of availabilities of shooting. The production was planned for 25 days. After a week they had to replace the child and had to re-shoot 4 days within the 25 days. During editing there were re-shoots. One day in Brazil, Rio de Janeiro and two days in Amsterdam. The total editing period was 38 days. The final mix had to end till the end of 2005.

The production costs were 1.5 million Euros.

== Premiere ==
The film had its world premiere on 25 September 2006 in the Tuschinksi Theatre in Amsterdam, The Netherlands.

== Music ==
The music was composed by Jaques Morelenbaum, one of Brazils most respected musicians. He arranged the music in the studio of Léo Gandelman in Rio de Janeiro, in the neighborhood of Ipanema.

His wife Paula Morelenbaum sang the end title music "Bésame Mucho".

== Cast ==
* Anne Charrier - Paula 
* Murilo Benício - Michel
* Tom Conti - Rudi 
* Guy Marchand - Giuseppe 
* Fendi van Brederode - Luis 
* Corbin Bernsen - William Montague 
* Tygo Gernandt - Bennie
* Hajo Bruins - Andre
* Beppe Clerici - Max 
* Helmert Woudenberg - Cor 
* Manouk van der Meulen - Anna 
* Marie-France Pisier - Gislaine 
* Ana Lúcia Torre - maid

== Crew ==
*Director & Screenwriter: Laurence Lamers
*D.O.P: Tom Erisman
*Editor: Martyn Gould
*Composer: Jaques Morelenbaum
*Art-Director: Dimitri Merkoulov
*Producer: Silvester Slavenburg
*Line-Producer: Martin Lagestee
*Executive Producers: Franco Sama, Jordan Yale Levine
*Production Company: Slavenburg Films

== Distribution ==

Paramount Home Video (DVD Netherlands 2007)
Waterwood Films (Theatrical Netherlands 2007)
Vivendi Universal (DVD USA/Canada 2009)
Lightyear Entertainment (all other media USA/Canada 2009)

== Festivals ==
The film was shown at the following festivals:
*The Dutch Festival (The Netherlands)
*Mostra de São Paulo (Brazil)
*Filmfest von Braunschwick (Germany)
*Dereel Festival of Melbourne (Australia)
*The New Orleans Film Festival (USA)
*The Exground Film Festival of Wiesbaden (Germany)

==External links==
* 

 
 
 
 