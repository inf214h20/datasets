Eating Too Fast
{{Infobox film
| name           = Eating Too Fast
| image          = 
| alt            =  
| caption        = 
| director       = Andy Warhol
| producer       = Andy Warhol
| writer         = 
| screenplay     = 
| story          = 
| starring       = Gregory Battcock
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1966  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Blow Job (1964). Battcock had previously appeared in Warhols films Batman Dracula (1964) and Horse (1965). 

==Production background==
The British Film Institute catalogue says that the first half of the film is a still shot, showing Battcock eating an apple and taking a phone call, while apparently receiving fellatio. The second half of the film has more camera movement.  Battcocks diaries say the film was made in Battcocks Greenwich Village apartment with Warhol and Lou Reed present for the filming.

WarholStars.com gives far more details saying: "In the first reel, you see a close-up of art critic Gregory Battcock as he is receiving a blow-job. Like the silent version, the person who is performing oral sex on him is out of the frame. Battcock looks somewhat bored during the proceedings. He drinks wine and some water. Towards the end of the first reel, he gets a phone call from a friend named Bob who has just returned from the trip. While they are chatting, the camera pans down to reveal the back of the head of someone who is going down on Battcock while he chats on the phone. Battcock tells the guy to come over and see him in about 33 minutes (the length of reel two). At the end of the reel, Battcock tells the guy going down on him that the callers mother finally died. The guy responds with something along the line of Oh, really. During reel two, Warhol moves the camera --tilting up and down to the guy on the floor whose face is never revealed. There are also some occasional zooms in and out of Battcocks face. At one point, Battcock starts to eat an apple and begins choking. The guy on the floor tells him You shouldnt eat so fast. The film ends without the sexual act being completed (at least from what I can tell...)" 

==References==
 

==See also==
*Andy Warhol filmography

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 