The Jazz Age (film)
:For the term which refers to the 1920s, see Jazz Age.
 
{{Infobox film
| name           = The Jazz Age (1929)
| image          =
| image_size     =
| caption        =
| director       = Leslie Shores
| producer       =
| writer         = Paul Gangelin (script) Randolph Bartlett (intertitles)
| narrator       =
| starring       = Douglas Fairbanks Jr. Marceline Day Henry B. Walthall Joel McCrea
| music          = Josiah Zuro
| cinematography = Ted Pahle
| editing        = Ann McKnight
| studio         = Film Booking Offices of America
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 64 minutes (7 reels; 6246 ft.)
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
The Jazz Age (1929) is a film starring Douglas Fairbanks Jr., Marceline Day, and Joel McCrea in his first leading role. The film, directed by Leslie Shores and written by Randolph Bartlett, was released by RKO Radio Pictures soon after RKO was created from Film Booking Offices of America (FBO), Radio Corporation of America|RCA, and the Keith-Albee-Orpheum theater chain. 

==Plot==
Steve Maxwell (Fairbanks) and Sue Randall (Day), during an escapade, wreck one of her fathers streetcars. Randall uses this incident to stop the elder Maxwell (Walthall) from opposing his illegal contract with the city. When Steve tells all to the city council, Mr. Randall (Ratcliffe) threatens Steve with arrest, Sue admits her culpability, and announces her intentions of marrying Steve. 

==Cast==
*Douglas Fairbanks, Jr.	...  Steve Maxwell
*Marceline Day ...  Sue Randall
*Henry B. Walthall	...	Mr. Maxwell (billed as H. B. Walthall)
*Myrtle Stedman	...	Mrs. Maxwell
*Gertrude Messinger	...	Marjorie
*Joel McCrea	...	Todd Sayles
*William Bechtel	...	Mr. Sayles
*E. J. Ratcliffe	...	Mr. Randall
*Ione Holmes	...	Ellen McBride
*Edgar Dearing	...	Motor Cop

==Production background== silent and part-talkie version, so the film could be shown in theaters equipped for sound, and for those not equipped for sound. The part-talkie version was recorded in RCA Photophone. 
 NBC News Project 20, narrated by Fred Allen also titled The Jazz Age (1956), and a 15-episode TV series of the same name on the BBC (1968). Both the IMDB and TCM websites, for the 1929 film, show the 1956 film as available on DVD for purchase. No info is given about the availability of the 1929 title.

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 