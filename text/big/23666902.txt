Figaro qua, Figaro là
 
{{Infobox film
| name           =Figaro qua, Figaro là
| image          =Figaro_qua,_Figaro_là.jpg
| image_size     =
| caption        = 
| director       =Carlo Ludovico Bragaglia
| producer       = Lux Film (Dino De Laurentiis & Carlo Ponti)
| writer         = Agenore Incrocci, Furio Scarpelli, Marcello Marchesi, Vittorio Metz (from Gioachino Rossini)
| narrator       = 
| starring       = Totò, Isa Barzizza
| music          =  Pippo Barzizza (from Gioachino Rossinis The Barber of Seville)
| cinematography =   Mario Albertelli 
| editor       =  Renato Cinquini
| distributor    = Lux Film
| released       =  
| runtime        = 84 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Figaro qua, Figaro là is a 1950 Italian comedy film directed by Carlo Ludovico Bragaglia.

==Plot==
The protagonist of the film, set in 1700, is Figaro, the barber of Seville, which is likely to be arrested because although there is a ban opens his shop on Sundays. Figaro is a friend of a nobleman who fell in love with Rosina, her friend and daughter of the governor. But the father of Rosina does not agree to their marriage. One day Rosina, by her maid Dove manages to tell the Count that one night staying at the inn "of four bulls." Then the Count and Figaro with a friend of hers to the inn before they get Rosina and his court. Their plan is to replace the host, pose as their owners of the inn and abduct Rosina. But unfortunately not all is according to plan. In fact, Pedro, dangerous bandit, he learns that Rosina and his court must stay at the inn that night and his men raided the inn: they dress all by hosts and make prisoners Figaro, the Count and his friend. Then finally comes Rosina remains very disappointed when he sees that his beloved is not there. Figaro, however, has an idea: he writes a note and puts it in OCA that will bring to the table, where it says that the man in the white hat is Pedro. Unfortunately at that time the white hats got Figaro and Pedro, so the soldiers Figaro stop believing that it is Pedro and the plan fails. Figaro is sentenced to death by firing squad, but in the end manages to escape helped by count. Eventually, after many vicissitudes, Count riece to marry Rosina and, after marriage, Figaro goes to live with them.

==Cast==
*Totò	as	Figaro
*Isa Barzizza	as 	Rosina
*Gianni Agus	as	Count of Almaviva
*Renato Rascel	as	Don Alonzo
*Guglielmo Barnabò	as 	Don Bartolo 
*Jole Fierro	as	Colomba 
*Luigi Pavese	as	Pedro 
*Franca Marzi	as 	Consuelo
* Pietro Tordi as  Fiorello
* Ugo Sasso as  Hurtado 
* Mario Siletti as  the president of the court 
* Mario Castellani as the actor 
* Giulio Calì as the barber help

== External links ==
*  

 

 
 
 
 
 
 
 

 