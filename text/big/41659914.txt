Raja Natwarlal
 
 
{{Infobox film
| name           = Raja Natwarlal
| image          = Raja Natwarlal.jpg
| director       = Kunal Deshmukh
| producer       = Siddharth Roy Kapur
| writer         = Sanjay Masoom  (dialogue) 
| screenplay     = Narender Kumar
| story          = Parveez Shaikh
| starring       = Emraan Hashmi Humaima Malick Paresh Rawal Kay Kay Menon
| cinematography = Raaj A. Chakravarti
| editing        = Anand Subaya
| music          = Songs: Yuvan Shankar Raja Background Score: Sandeep Shirodkar
| studio         = UTV Motion Pictures
| released       =  
| runtime        =140 minutes
| country        = India Hindi
| budget         = 
| gross          =    
}}
 Hindi crime crime thriller film directed by Kunal Deshmukh and produced by Siddharth Roy Kapur under UTV Motion Pictures. The film features Emraan Hashmi in the title role, alongside Humaima Malick, Paresh Rawal, Kay Kay Menon and Deepak Tijori in a special appearance.  The film was released on 29 August 2014 and received mixed reviews from critics.

==Plot==
Raja (Emraan Hashmi) is a small-time conman who cons people for a living along with his partner in crime Raghav (Deepak Tijori). He is in love with a bar girl Ziya (Humaima Malick). Feeling sorry for her, Raja decides he wants to go for a big catch so that he can marry his lady love and she wont have to work in a bar any more. Thats when the trouble starts. Along with Raghav, Raja makes a grand plan to swindle a huge amount of money from two baddies. The plan is successful and Raja decides to celebrate.

This is when Varda Yadav (Kay Kay Menon), a billionaire based in Cape Town, enters. The money actually belonged to him and upon realising it has been stolen by two street-cons Vardha orders them to be killed. Vardas men find Raghav and kill him. Raja witnesses this and decides to avenge the loss.

He seeks the help of a seasoned crook Yogi (Paresh Rawal), whom Raghav used to talk about before his murder. After a lot of persuasion, Yogi agrees to help Raja have his payback. Yogi trains him, and eventually they form a team of crooks, and set out to Cape Town to finish off Varda.

==Cast==
* Emraan Hashmi as Raja
* Humaima Malick as Ziya
* Paresh Rawal as Yogi
* Kay Kay Menon as Varda Yadav
* Deepak Tijori as Raghav 
* Prachi Shah as Raghavs wife
* Ashiesh Roy as Kaashi
* Namit Shah as Vicky

==Production==
In May 2013, it was first reported that Kunal Deshmukhs next project would be produced by UTV Motion Pictures.  Deshmukh selected Emraan Hashmi for the lead role, collaborating with him for the fourth time.    The film was titled Shaatir and Hashmi was revealed to play the character of a shaatir conman in the film.  In April 2014, the films title was changed to Raja Natwarlal inspired by the 1979 Amitabh Bachan film Mr. Natwarlal. 

In June, it was reported that Abhishek Bachchan would play another lead role besides Hashmi.    But he opted out and next month Paresh Rawal was signed for the role.  Jacqueline Fernandez and Priyanka Chopra were approached to play the lead female role, but both actresses could not accommodate the dates.  In November 2013, Pakistani model and actor Humaima Malick was cast as the female lead.  Deepak Tijori was signed for a role in the film in December.  Kay Kay Menon was signed for an important role and Deshmukh stated that his character named Vardha Yadav had been modelled on the Sahara India Pariwar chief Subrata Roy.  Pritam Chakraborty, who had composed the music for all of Deshmukhs films, was supposed to work on this films music, too,  but was replaced by Yuvan Shankar Raja.     Deshmukh later stated that Pritam wanted to take a break and that it was Pritam himself who referred Yuvan Shankar Raja to him.     

Deshmukh had planned to start the first schedule of the film early 2014 but since Hashmis other film Badtameez Dil had been postponed, the start of the films shoot was advanced to October 2013.  Principal photography commenced on 20 October 2013. Hashmi took special dance training for the film under choreographer, Raju Khan. 

==Soundtrack==
{{Infobox album 
| Name = Raja Natwarlal
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Border = yes
| Cover =
| Alt =
| Caption =
| Released = 30 July 2014
| Recorded = 2013–14 Feature film soundtrack
| Language = Hindi
| Label = UTV Music
| Length = 24:35
| Producer = Yuvan Shankar Raja
| Last album = Anjaan (soundtrack)|Anjaan (2013)
| This album = Raja Natwarlal (2014) Govindudu Andarivadele (2014)
}} Radio City.  The reprise "Tere Hoke Rehenge" was the first song to be released, although it was the last song to be recorded. 

Joginder Tuteja from Rediff.com gave the album 3 out of 5 stars and wrote, "Raja Natwarlals music grows on you slowly", further adding, "Since debutant Yuvan Shankar Raja brings in a new sound with this album, he deserves a few hearings before one pronounces judgment".  Bollywood Hungama gave 2.5 stars out of 5 and wrote, "Yuvan Shankar Raja has absorbed the ethos of the contemporary Hindi film song with far more accuracy, acumen and sharpness than any colleague down South with the exception of that redoubtable genius, M.M. Kreem. Yuvan Shankar Rajas tunes are quite trendy, and their appeal makes the score sound fresh despite generic similarities to todays hit music. His production is immaculate and Irshad Kamils lyrics as usual vary from the pithy to the occasionally needlessly Sufi". 

{{tracklist
| headline        = Raja Natwaralal (Original Motion Picture Soundtrack) 
| extra_column    = Artist(s)
| total_length    = 24:35
| title1          = Tere Hoke Rehengay
| extra1          = Arijit Singh
| length1         = 4:03
| title2          = Dukki Tikki
| extra2          = Mika Singh
| length2         = 4:59
| title3          = Tere Hoke Rehengay (Reprise)
| extra3          = Shweta Pandit
| length3         = 4:29
| title4          = Kabhi Ruhani Kabhi Rumani
| extra4          = Benny Dayal
| length4         = 4:00
| title5          = Namak Paare
| extra5          = Mamta Sharma, Anupam Amod
| length5         = 3:45
| title6          = Flip Your Collar Back
| extra6          = Benny Dayal
| length6         = 3:59
}}

==Critical reception==
The film received mixed reviews upon release. 

The Times of India gave 3.5 stars out of 5 and wrote, "With its twisting story, good-looking frames and zingy acting, Raja Natwarlal keeps you entertained".  Rachel Saltz from The New York Times wrote, "Mr. Deshmukhs setup can be overly fussy – some of the con machinations seem needlessly complicated and hard to follow, or maybe not quite worth following – but his payoff works. And his cast, too, hits the right notes and finds an easy rhythm".  Bollywood Hungama gave 3 stars out of 5 and wrote, "Kunal Deshmukh does a reasonably good job in Raja Natwarlal. There are a few repeated moments from his earlier films, but the plot of the film overshadows that. He manages to get the audiences glued to their seats throughout the film, a few loopholes notwithstanding". 

Anupama Chopra from Hindustan Times gave a rating of 2.5 and wrote, "If you can get past the foolishness of the plot, director Kunal Deshmukhs film is mildly engaging".  Raja Sen from Rediff gave the same rating, while writing, "Raja Natwarlal has some smarts but tragically lacks the skill or the sleight-of-hand".  Kusumita Das from Deccan Chronicle gave 2.5 stars as well and wrote, "Theres a gripping premise of a revenge plan, little bit of comedy, some interesting dialogues and convincing performances from the chief cast. But in a bid to broaden the suspense and make the twists more unpredictable, the story gets entwined in all the yarns it manages to spin for over two hours".  Shubha Shetty-Saha from mid-day gave 2.5 stars out of 5, too, and wrote, "This film had huge potential but sadly one walks out of the theatre unimpressed by Raja Natwarlals antics and this film".  Rahul Desai from Mumbai Mirror wrote, "This film is far from perfect, but makes for an acceptable non-Italian version of The Indian Job-by entertaining solely within the confines of a revenge drama".  Sify wrote, "The film, with an impressive intermission point, is pacy and full of twists in the second half...Where the film falters is in insinuating incongruous melodrama and unnecessary characters". 

Shubhra Gupta from   reported, "With its many twists, the film actually has enough potential to stay gripping, but fails woefully at it as it gets a hackneyed treatment".  Faheem Ruhani from India Today, gave the film one star and said, "Unfortunately for Emraan, there is very little in this film that is enthralling. In fact, this story...is nothing but plain boring and exhausting to sit through".  Mihir Fadnavis from firstpost.com wrote, "Raja Natwarlal is a con film about con men, and it succeeds wonderfully in conning you into coming to see it. Raja Natwarlal is a Stupid Mans Oceans 11".  Sanjukta Sharma from livemint wrote, "You probably wont remember this film after you have watched it. There is no inventiveness in plot, treatment and characterization—unlike the best con movies, not for a moment do any of the characters seem to be duping each other, and their wiles or avarice hold no surprises. Raja Natwarlal is unimaginative pulp; it begins and ends on a lukewarm note". 

==Box office==
The films approximate opening day gross was   to  ,  while   were reportedly collected on Saturday; the overall weekend gross was  .  At the end of the first week, the film had collected   and the overall lifetime collection were    according to Bollywood Hungama. 

==References==
 

==External links==
*  

 
 
 
 