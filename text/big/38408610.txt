Kirikou and the Men and Women
 
{{Infobox film  
| name           = Kirikou and the Men and Women
| image          = 
| caption        = Original French film poster
| director       = Michel Ocelot
| producer       = Didier Brunner, Jacques Bled, Ivan Rouvreure
| writer         = Michel Ocelot Bénédicte Galup Susie Morgenstern Cendrine Maubourguet
| starring french= Romann Berrux Awa Sène Sarr 
| released       =  
| studio         = Les Armateurs, Mac Guff Ligne, France 3 Cinéma, Studio O
| distributor    = Studio Canal, Cinéart
| runtime        = 88 minutes
| music          = Thibault Agyeman
| editing        = 
| country        = France
| language       = French
| budget         = 6,900,000 Euro|€
}}
Kirikou and the Men and Women ( ) is a 2012 CGI animation feature film written and directed by Michel Ocelot. The film was originally released on October 3, 2012. 

It followes the successful Kirikou et la Sorcière and Kirikou et les bêtes sauvages, released in 2005, and adapted into a stage Musical theatre|musical, Kirikou et Karaba, first performed in 2007. 

==Accolades==
{| class="wikitable"
|-
! Year !! Award Show !! Award !! Category !! Result
|-
| 2013 || Cannes || || César du meilleur film danimation || Nominated
|-
|}

==Notes==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 