Grabben i graven bredvid
{{Infobox film
| name           = Grabben i graven bredvid
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kjell Sundvall
| producer       = Börje Hansson Charlotta Denward
| screenplay     = Sara Heldt
| based on       =  
| narrator       = 
| starring       = Elisabet Carlsson Michael Nyqvist
| music          = 
| cinematography = Philip Øgaard
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 94 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Grabben i graven bredvid is a 2002 Swedish film directed by Kjell Sundvall.

==Cast==
* Elisabet Carlsson as Desirée Wallin
* Michael Nyqvist as Benny Söderström
* Annika Olsson as Märta
* Anna Azcarate as Lilian
* Rolf Degerlund as Bengt-Göran
* Anita Heikkilä as Violet

==External links==
*  
*  

 
 

 