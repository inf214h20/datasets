A Journey of Samyak Buddha
 

{{Infobox film
| name                 = A Journey of Samyak Buddha
| director             = Praveen Damle
| producer             = Praveen Damle
| starring             = Gita Agrawal  Jyoti Bhagat  Ravi Patil Gautam Degre  Abhishek Urade  Mrilan Sharma  Abhay Sathe
| music                = Praveen Damle 
| screenplay           = Praveen Damle
| cinematography       = Bhushan Prasad
| Editor               = Bhushan Prasad
| distributor          = Manoj Nandwana ,Jai Viratra Entertainment 
| released             =  
| country              = India
| language             = Hindi
}}

A Journey of Samyak Buddha ( ’s miraculous birth, marriage, and his path towards enlightenment. The biographical film is based on Dr. Babasaheb Ambedkar’s book, The Buddha &amp; His Righteousness. 

==Synopsis==

Gautam Buddha’s biographical movie showcases different facets of his enlightened life. 

==Cast==
The actors in the film included:

* Gita Agrawa
* Jyoti Bhagat
* Ravi Patil
* Gautam Degre
* Abhishek Urade
* Mrilan Sharma
* Abhay Sathe]]
* Gangadhar Patil
* Jeevan Chore
* Jaya Kamble
* Ashok Sonnktte
* Pradeep
* Rashtrapal Wasekar
* Kutrmare
* Shankar Machani
* Sunil Dhale
* Harsha Kamble
* Dipankar Salim Sheikh
* Hrithik Roshan
* Sneha Kamble
* Savita Jhamrz

==Artist list ==
* Abhishek Urade as the Buddha
* Ravi Patil as father
* Geeta Agrawal as Prajapati
* Jyoti Bagat as mother
* Mrinal Pendharkar as Yashodhara
* Gautam Dhengare as Bhikshoo Anand

==Production==

The film was directed and produced by Pravin Damle and it is being distributed by Manoj Nandwana’s Jai Viratra Entertainment Limited.  The movie is set release to all over India on 26 July 2013.

==See also==
*Depictions of Gautama Buddha in film

==References==
 

== External links ==
*  
* http://www.bollywoodhungama.com/moviemicro/cast/id/503896

 

 
 
 
 
 
 
 
 
 


 