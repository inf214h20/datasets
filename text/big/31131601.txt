Under New Management
Under New Management is a 1946 British comedy film directed by John E. Blakeley and starring Nat Jackley, Norman Evans and Dan Young.  A chimney sweep inherits a hotel and calls on a number of ex-army friends to staff it. It was one of a number of films at the time dealing with the contemporary issue of demobilisation following the end of the Second World War.  It is also known by the alternative title of Honeymoon Hotel.

==Main cast==
* Nat Jackley - Nat
* Norman Evans - Joe Evans
* Dan Young - Dan
* Betty Jumel - Betty
* Nicolette Roeg - Brenda Evans
* Tony Dalton - Tony
* Marianne Lincoln - Marianne
* Bunty Meadows - Bunty
* Aubrey Mallalieu - John Marshall
* G.H. Mulcaster - William Barclay
* John Rorke - Father Flannery
* Hay Petrie - Bridegroom
* Joss Ambler - Hotel Manager
* David Keir - Colonel
* Percival Mackey Orchestra - Themselves

==References==
 

==Bibliography==
* Mundy, John. The British musical film. Manchester University Press, 2007.

==External links==
* 

 
 
 
 
 
 
 


 
 