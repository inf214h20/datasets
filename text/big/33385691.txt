Anneliese: The Exorcist Tapes
{{Infobox film
| name           = Anneliese: The Exorcist Tapes
| image          = Anneliese_exorcist_tapes_image.jpg
| caption        = DVD cover 
| director       = Jude Gerard Prest 
| producer       = 
| writer         =
| starring       = Gerold Wunstel Kai Cofer Christopher Karl Johnson Nikki Muller
| music          = 
| cinematography = Prema Ball 
| editing        = Prema Ball 
| distributor    = The Asylum
| released       =  
| runtime        = 91 minutes 
| country        = United States
| language       = English German
| budget         = $100,000 
}}
Anneliese: The Exorcist Tapes or Paranormal Entity 3: The Exorcist Tapes is a 2011 documentary-style horror film directed by Jude Gerard Prest. It is based on the real life exorcism of Anneliese Michel, a young woman thought to have been demonic possession|possessed. The film was released direct-to-video on March 1, 2011. It is also a mockbuster of Paranormal Activity 3 and one of several films based on the exorcism of Anneliese Michel, including The Exorcism of Emily Rose and Requiem (2006 film)|Requiem.

==Cast==
* Gerold Wunstel ... Pastor Ernest Alt
* Kai Cofer ... Dr. Frederick Gruber
* Christopher Karl Johnson ... Father Renz
* Nikki Muller ... Anneliese Michel
* Yaz Canli ... Sandy
* Robert Shampain ... Dr. Kenneth Landers
* Korey Simeone ... Steve Parker
* Annette Remter ... Anna Michel
* David Reynolds ... Josef Michel

==Release==
The film premiered on DVD on the March 1, 2011 in the United States, which includes the film itself and the actual video footage. In Australia, the film received a DVD release on September 21, 2011.   In the UK, Anchor Bay Entertainment will release the film on DVD under the name Paranormal Entity 3: The Exorcist Tapes, on October 17, 2011.  

In Germany, it was released on Blu-ray under the name Der Exorzismus der Anneliese M. on July 28, 2011. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 