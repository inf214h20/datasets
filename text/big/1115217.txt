Beat Girl
{{Infobox film
| name           = Beat Girl
| image          = BeatGirl.jpg
| caption        =
| director       = Edmond T. Gréville
| producer       = George Willoughby
| writer         = Dail Ambler
| narrator       = David Farrar Noëlle Adam Christopher Lee Gillian Hills Adam Faith John Barry Trevor Peacock (lyrics)
| cinematography = Walter Lassally
| editing        = Gordon Pilkington
| distributor    = Renown Pictures (UK) Victoria Films (USA)
| released       = 1960
| runtime        = 85 min.
| country        = UK English
| budget         =
| preceded by    =
| followed by    =
}} British film about late-fifties youth-rebellion. The film was later released in the United States under the title Wild for Kicks. 

The title character of Beat Girl was played by starlet  )  had already come out. The film also features Christopher Lee and Nigel Green as strip joint operators, and Oliver Reed in a small role as one of the "beat" youth.
 
The original music was composer   album to be released on an LP album|LP, and reached number 11 on the British album charts, paving the way for later films to release soundtrack albums.    

==Plot==
 David Farrar), a wealthy and prominent architect, returns home to London bringing his new wife, beautiful 24-year-old Nichole (Noelle Adam), whom he has just married in Paris. Paul is anxious to introduce Nichole to his teenage daughter Jennifer (Gillian Hills), but Jennifer appears less than happy about her fathers remarriage and coldly rejects all of Nicholes friendly overtures all evening. After Paul and Nichole go to bed, Jennifer sneaks out to the local beatnik club for an evening of rock music and dancing with her friends, including Dave (Adam Faith), a youth from a working-class background who plays guitar and writes songs; Tony (Peter McEnery) , a generals son whose mother was killed in the Blitz and who has a drinking problem, although beatniks frown on alcohol; and Dodo (Shirley Anne Field), Tonys well-bred girlfriend. Dave and Jennifer are attracted to each other.

The next day Nichole plans to meet Jennifer at her art college so they can have lunch together. At lunchtime, Nichole arrives at Jennifers school but is told that Jennifer has left and gone to the beatnik coffee club in Soho. Nichole goes to the club and confronts Jennifer in front of her friends, who are impressed by Nicholes youth, good looks and knowledge of modern jazz. The fact that her friends and especially Dave seem to like Nichole upsets Jennifer. Nichole leaves, reminding Jennifer to be home for her fathers important business dinner that night. As Nichole leaves, she passes Greta (Delphi Lawrence), the star performer at the strip club across the street.  Greta recognizes Nichole and greets her by name, but Nichole ignores her, to Gretas annoyance. Jennifer and her friends see this encounter and wonder how Greta and Nichole might know each other. Jennifer suspects that Nichole was also a stripper before meeting her father.

That night at Pauls business dinner, Jennifer tries to embarrass Nichole in front of the guests by bringing up the encounter with Greta, making sure to emphasize that Greta is a stripper. After the guests leave, Paul questions Nichole, who says that she knew Greta in Paris and that they were in ballet together but Greta pursued a different way of life and Nichole lost track of her. Paul accepts her explanation, but Jennifer goes to the strip club to ask Greta directly. Greta at first claims she made a mistake and doesnt really know Nichole, but under pressure from her boyfriend, strip club manager Kenny King (Christopher Lee), she reveals that she and Nichole worked together as strippers and occasional prostitutes in Paris. Jennifer, encouraged by Kenny, becomes enamored with the idea of becoming a stripper herself. Jennifer is caught by Paul and Nichole coming home from the strip club at 3 am and an angry confrontation results. Jennifer taunts Nichole by telling her she has spoken with Greta and that Nichole had better stay out of Jennifers life from now on or else Jennifer will tell what she knows about Nichole. Nichole visits the strip club to tell Kenny and Greta to stay away from her stepdaughter, but Kenny says that Jennifer will be welcome at the club any time and that if Nichole interferes he will tell Paul about her past. 

Jennifer and her friends have a wild night including dancing at a candlelit cavern, a dangerous auto race, and a game of "chicken" on railroad tracks where the last person to leave the rails before the train arrives (Jennifer) wins. Throughout the evening, Jennifer and Dave dare each other to increasingly dangerous behaviors as a way of flirting. Jennifer invites everyone to continue the party at her house, as her father is out of town and Nichole presumably wont interfere for fear Jennifer will reveal her past. Jennifer accepts a dare to "strip like a Frenchie" and begins a strip tease to music, but when she gets down to her underwear Nichole bursts from her bedroom and stops her. Then Paul suddenly arrives home and breaks up the party, throwing all of the beatniks out of his house including Dave. Jennifer angrily tells her father about Nicholes activities in Paris. Nichole, crying, admits it is true and explains she only did it because she was broke and hungry. Paul and Nichole profess their love for each other and reconcile.
 Teddy Boys who vandalize Daves jalopy and smash his guitar. Paul and Nichole arrive on the scene searching for Jennifer just as the police drag her, in hysterics, out of the strip club. The police release Jennifer to Paul and Nichole and the family, arms around each other, heads for home, as Dave throws his broken guitar in a trash bin and proclaims, "Funny, only squares know where to go."

==Cast list== David Farrar as Paul Linden
*Noëlle Adam as Nichole
*Gillian Hills as Jennifer Linden
*Adam Faith as Dave
*Peter McEnery as Tony
*Shirley Anne Field as Dodo
*Christopher Lee as Kenny King
*Delphi Lawrence as Greta
*Nigel Green as Simon
*Margot Bryant as Martha
*Oliver Reed as Plaid Shirt (Beat youth)
*Michael Kayne as Duffle Coat (Beat youth)
*Claire Gordon as Honey
*Robert Raglan as F.O. official
*Nade Beall as Officials Wife
*Norman Mitchell as Club doorman
*Pascaline as strip dancer with scarf
*Diane DOrsay (Moyle) as strip dancer in white négligée

==Production notes== British Board of Film Censors in March 1959, whose reviewer termed it "machine-made dirt" and "the worst script I have read for some years." The project was then renamed "Beat Girl" and nudity was reduced; however, censors still objected to scenes of strip tease, juvenile delinquency, and teenagers playing "chicken" by lying on railroad tracks before an oncoming train. Ultimately, the film received an History_of_British_film_certificates|"X" certificate, causing its release to be delayed as its release was queued behind a glut of other X-rated films.   When finally released it performed reasonably well at the box office in UK, despite receiving bad reviews. 

The movie was filmed at MGM-British Studios at Borehamwood, Hertfordshire, UK, with exteriors filmed in Soho and Chislehurst Caves (then located in Kent). 

After Adam Faith was cast, John Barry was asked to compose the film soundtrack as he had already been collaborating with Faith as an arranger.  Barry was subsequently hired to score Faiths next films Never Let Go and Mix Me a Person, leading to Barrys successful career as a composer and arranger of film music. In addition to the Beat Girl soundtrack LP reaching number 11 in the album charts, the song "Made You," composed by John Barry and Trevor Peacock and performed in the film by Faith, achieved minor hit status before being banned by the BBC for suggestive lyrics.  
 Crazy Horse Saloon in Paris. 

Some versions of the released film have cut the original striptease sequences (which included topless nudity), some exposition scenes set in the strip club, the "chicken" game scene, and/or some opening exposition scenes with David Farrar and Noëlle Adam on a train and then with Gillian Hills at the "Linden" home.

==Further reading==
*Caine, Andrew. Interpreting Rock Movies: The Pop Film and Its Critics in Britain. Manchester Univ. Press, 2004. ISBN 0-7190-6538-0.
*Glynn, Stephen. The British Pop Music Film: The Beatles and Beyond. Palgrave Macmillan, 2013. ISBN 978-0-230-39222-9.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 