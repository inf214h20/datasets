Booker's Place: A Mississippi Story
 
{{Infobox film
| name = Bookers Place: A Mississippi Story
| image = Bookers Place, A Mississippi Story.jpeg
| caption = Theatrical release poster
| director = Raymond De Felitta
| producer = David Zellerford Yvette Johnson (co-producer) Steven C. Beer (executive producer) Lynn Roer (executive producer)
| writer = 
| starring = 
| music = 
| cinematography = 
| editing = George Gross
| studio = Eyepatch Productions
| distributor = 
| released =     
| runtime = 91 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
Bookers Place: A Mississippi Story is a 2012  ,  a short NBC television documentary about racism in the American South. During his interview with producer Frank De Felitta, he spoke openly about racism, and his treatment as a waiter in an all-white restaurant. The broadcast of his remarks had catastrophic consequences for Wright.     

Bookers Place: A Mississippi Story was directed by the son of Frank De Felitta, Oscar-nominated, independent filmmaker Raymond De Felitta,   and co-produced by one of Booker Wright’s four grandchildren, Yvette Johnson.    It includes interviews with those who lived in the community. They discuss life at the time, and the restaurant Wright owned, which catered to African-American customers. 
 
The documentary premiered at the Tribeca Film Festival on April 25, 2012. 

==Background==
===Mississippi: A Self Portrait===
The original documentary, the subject of Bookers Place: A Mississippi Story, was produced by Frank De Felitta in 1965. De Felitta worked for NBC as a documentary filmmaker. He was given his own unit, and so generally had the freedom to select his own topics. He was inspired to make Mississippi: A Self Portrait after reading a New York Times Sunday Magazine article by Hodding Carter about injustice experienced by African Americans in Mississippi. He approached NBC, and the project was approved.  He traveled to Mississippi to interview local residents. There, in Greenwood, Mississippi|Greenwood, he was introduced to Booker Wright, a waiter at Lusco’s, a Racial segregation|whites-only restaurant. Wright sang the menu, a gimmick at the restaurant. This was also because there were no menus, a measure used to discourage African Americans from patronizing the restaurant.  Wright spoke openly about his treatment by customers, and life in a racist society. The documentary appeared on NBC television. 

===Consequences of the broadcast===
Following the broadcast of the NBC documentary, Wright quit his job at the all-white restaurant after being shunned by customers. He was severely Pistol-whipping|pistol-whipped by a policeman, and his own restaurant, Booker’s Place was Small incendiary device|firebombed.  Wright was eventually murdered by an African American customer.

==References==
 

==External links==
*   at the Tribeca Film Festival website
*   on Democracy Now!
* 
*  
*  

 

 
 
 
 
 
 