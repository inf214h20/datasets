Surya Vamsha
{{Infobox film
| name = Suryavamsha
| image =
| director = S. Narayan
| writer = Vikraman Vishnuvardhan Isha Vijayalakshmi
| producer = Anitha Kumaraswamy
| music = V. Manohar
| cinematographer = M. Rajendra
| studio = Chennambika Films
| editor = P. R. Soundar Raju
| distributor =
| released =  
| runtime =
| country = India
| language = Kannada
| budget =
}}
 Kannada film Vishnuvardhan in Lakshmi in the lead roles. The films score and soundtrack are composed by V. Manohar, however a couple of retained original tunes were composed by music director S. A. Rajkumar. 
 Venkatesh and Meena as lead actors and in Hindi as Sooryavansham (1999) with Amitabh Bachchan and Soundarya in leading roles.

==Cast== Vishnuvardhan
* Isha Koppikar Lakshmi
* Ramesh Bhat Vijayalakshmi
* Doddanna
* Mukhyamantri Chandru
* S. Narayan in a special appearance

==Soundtrack==
All music are scored by V. Manohar and lyrics by S. Narayan and Prof. Doddarange Gowda. The track Sevanthiye Sevanthiye composed by S. A. Rajkumar is however retained from the original film. 

{|class="wikitable"
! Sl No. !! Song Title !! Singer(s)
|-
| 1 || "Sevanthiye Sevanthiye" || S. P. Balasubramanyam
|-
| 2 || "O Meghagala Baagilali" || Rajesh Krishnan, K. S. Chitra
|-
| 3 || "Onde Ondu Question" ||
|-
| 4 || "Pancharangi" || S. P. Balasubramanyam
|-
| 5 || "Belli Chukki Baaninalli" || K. S. Chitra
|-
| 6 || "Sevanthiye Sevanthiye" || K. S. Chitra
|-
|}

==Remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Suryavamsam (1998) (Telugu cinema|Telugu) || Sooryavansham (1999) (Hindi cinema|Hindi) || Surya Vamsha (2000) (Kannada cinema|Kannada)
|- Dr Vishnuvardhan
|-
| Devayani || Meena Durairaj || Soundarya || Isha Koppikar
|}

==References==
 

==External links==
 

 
 
 
 
 
 


 