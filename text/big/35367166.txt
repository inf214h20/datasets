Late Blossom
{{Infobox film name           = Late Blossom image          = File:Late_Blossom_poster.jpg
| film name =     rr             = Keudaereul Saranghamnida   mr             = Kŭtae rŭl Saranghapnida}} director       = Choo Chang-min producer       = Kim Sung-jin   Lee Gun-seon   Yoon Ki-chan   Nam Hyun  writer         = Choo Chang-min Lee Man-hee Kim Sang-su Kim Yong-deok based on       =   starring       = Lee Soon-jae   Yoon So-jung   Song Jae-ho   Kim Soo-mi music          = Kang Min-guk cinematography = Choi Yun-man editing        = Choo Chang-min Go Ah-mo distributor    = Next Entertainment World released       =   runtime        = 118 minutes country        = South Korea language       = Korean budget         =  gross          = 
}}
Late Blossom ( ; lit. "I Love You") is a 2011 South Korean film written and directed by Choo Chang-min about the love story of two elderly couples.    
 indie slowly gained positive word-of-mouth and critical praise, and eventually became a box office success with over 1,645,505 ticket sales,  as well as a cultural darling among industry peers. 
 comic I online in 2007 and later published in three volumes. In 2008, it was turned into a play and drew audiences of more than 120,000 by 2010. 

==Plot==
The movie revolves around four senior citizens living in a hillside village. Kim Man-seok is a cranky milkman with a short fuse and a foul mouth. He wakes the village early each morning with his noisy, battered motorcycle. He meets Song Ee-peun, who scavenges for scrap paper while roaming around the town at daybreak. As they meet again and again, they slowly develop feelings for each other.

Ms. Song parks her handcart at a junkyard and sees Jang Kun-bong, the caretaker of the parking lot next to the scrap yard. One day, Kun-bong wakes up late and forgets to lock his door and asks Ms. Song to fasten it for him. Meanwhile, Jang’s Alzheimers-afflicted wife Soon-yi wanders around the town, ending up on the back of Man-seok’s motorbike.   

==Cast==
*Lee Soon-jae - Kim Man-seok
*Yoon So-jung - Song Ee-peun
*Song Jae-ho - Jang Kun-bong
*Kim Soo-mi - Jo Soon-yi
*Oh Dal-su - Dal-su
*Song Ji-hyo - Kim Yeon-ah
*Kim Hyung-jun - Jung Min-chae
*Lee Sang-hoon - Duk-bae
*Lee Moon-sik - Scratch
*Kil Hae-yeon - Kun-bongs daughter-in-law 1
*Jo Jae-yoon - Kun-bongs brother-in-law
*Kwon Bum-taek - Man-seoks wifes doctor
*Lee Chae-eun - young Ee-peun 
*Kang Hyun-joong - young Sang-tae 
*Lee Joon-hyuk - photographer
*Ra Mi-ran - nurse
*Jeon Bae-su - public officer of small neighborhood office
*Lee Moon-su - Duk-baes father
*Kim Hyang-gi - street light kid

==Awards==
*2011    
*2011 China    

==Box office==
Initially difficult to finance due to ageism, the film was shot with a   (US$ 900,000) budget, and then marketed with another   (a small amount compared to most Korean mainstream films). The sleeper hit eventually recouped four times its cost in just a few weeks, attracting 1,645,505 admissions and grossing more than   domestically.

==Spin-off==
A same-titled 16-episode television series spin-off (media)|spin-off starring Kim Hyung-jun aired on SBS Plus from April to June 2012. Of the movie cast members, only Lee Soon-jae reprised his role.       

==References==
 

== External links ==
*    
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 