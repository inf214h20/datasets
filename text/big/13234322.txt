Kill or Cure (1923 film)
 
{{Infobox film
| name           = Kill or Cure
| image          = 
| image size     = 
| caption        = 
| director       = Scott Pembroke
| producer       = Hal Roach
| writer         = 
| narrator       = 
| starring       = Stan Laurel
| music          = 
| cinematography = Frank Young
| editing        = 
| distributor    = 
| released       =  
| runtime        = 11 minutes
| country        = United States  English intertitles
| budget         = 
}}

Kill or Cure is a 1923 American film featuring Stan Laurel. Prints of the film survive.    It was directed by Scott Pembroke.

==Cast==
* Stan Laurel - Door to door salesman
* Katherine Grant - Maid with bird cage
* Noah Young - Car owner Eddie Baker - Sheriff
* Mark Jones - Speedy Sam
* Helen Gilmore - Aggressive non-customer
* George Rowe - Deaf man
* Sammy Brooks - Short non-customer

==See also==
* Stan Laurel filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 