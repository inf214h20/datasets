September Affair
{{Infobox film
| name = September Affair
| image = Poster of the movie September Affair.jpg
| caption =
| director = William Dieterle
| producer = Hal B. Wallis
| writer = Fritz Rotter Robert Thoeren Andrew Solt Robert Arthur
| music = Victor Young   Kurt Weill  Sergei Rachmaninoff Charles B. Lang   Victor Milner
| editing = Warren Low
| distributor = Paramount Pictures
| released =  
| runtime = 104 minutes English Italian Italian
| country = United States
| budget =
| gross = $1,425,000 (US rentals) 
}} 1950 film, directed by William Dieterle, starring Joan Fontaine, Joseph Cotten and Jessica Tandy.  It was produced by Hal B. Wallis.

==Plot==
Marianne "Manina" Stuart (Joan Fontaine), a prominent concert pianist, meets David Lawrence (Joseph Cotten), a businessman, on a flight from Rome to New York. Their plane is diverted to Naples for engine repairs, and they decide to kill time by doing some sight-seeing.

At lunch, a recording of the Kurt Weill/Maxwell Anderson song September Song, sung by Walter Huston, is playing. Manina is single, and David is unhappily married with a son in his late teens. They talk too long and miss their flight, and decide to stay on for a few days, getting to know each other.  They quickly fall in love.
 Robert Arthur).
 Piano Concerto No. 2 in New York, and she keeps up her practice during the secret affair.  She also has contact with piano teacher Maria Salvatini (Françoise Rosay), who agrees not to reveal Manina is very much alive, but continues to tutor her.

David transfers a large sum of money to Maria Salvatini by issuing a check dated prior to the flight.  They use the money as a nest egg for their life in Florence.  Catherine and her son travel to Florence after hearing of this transfer to try and find out any more on Davids fate from the woman he gave the money to. David Jr recognizes Maninas face from the list of persumed dead and puts two and two together that his father is alive. After this Davids wife writes him a note and then leaves. Knowing their secret is out, Manina goes on to perform the Rachmaninoff concerto as originally planned in New York. In the end, Manina realizes she cant stay with David, that they tried to hide from the past but it caught up with them, and after her concert leaves, bidding David goodbye at the airport.

==Music==
The primary music score was written by Victor Young.  

September Song, music by Kurt Weill, lyrics by Maxwell Anderson, is heard, originally in the recording by Walter Huston.   Later, Johnny Wilson (Jimmy Lydon), a sailor, sings it live.  Huston’s recording had been made in 1938, but the film gave it a new lease of life and it made it to the top of the 1950 hit parade.   
 Piano Concerto No. 2 are heard a number of times throughout the film.   The pianist in the Rachmaninoff was Leonard Pennario.  

The voice of Enrico Caruso is also heard in an off-screen recording. 

==Other==
The costume design was by Edith Head.   The film was shot on location in Naples, Capri, Florence and other places in Italy. 

==Cast==
* Joan Fontaine - Manina Stuart
* Joseph Cotten -  David Lawrence
* Françoise Rosay - Maria Salvatini
* Jessica Tandy -  Catherine Lawrence Robert Arthur - David Lawrence Jr
* Jimmy Lydon - Johnny Wilson
* Fortunio Bonanova - Grazzi
* Grazia Narciso - Bianca
* Anna Demetrio - Rosita
* Lou Steele - Vittorio Portini
* Frank Yaconelli -  Mr. Peppino
* Hal B. Wallis makes an uncredited cameo appearance as a tourist in a souvenir shop.

==References==
 

== External links ==
*  
* 
* 

 

 
 
 
 
 
 
 
 