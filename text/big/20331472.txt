A Home at the End of the World (film)
{{Infobox film
 | name = A Home at the End of the World
 | image = A Home at the End of the World film.jpg
 | caption = Original film poster Michael Mayer
 | writer = Michael Cunningham Based on his novel Robin Wright Penn Dallas Roberts Sissy Spacek John Wells
 | music = Duncan Sheik
 | cinematography = Enrique Chediak
 | editing = Andrew Marcus Lee Percy
 | distributor = Warner Independent Pictures
 | budget = $6.5 million
 | gross = $1,548,955 (worldwide)
 | released = July 23, 2004
 | runtime = 96 minutes
 | country = United States English
}}
 Michael Mayer. novel of the same title.

==Plot synopsis== suburban Cleveland East Village apartment with bohemian Clare. Bobby moves in, and the three create their own household.

Although Jonathan is openly gay and highly promiscuity|promiscuous, he is deeply committed to Clare and the two have tentatively planned to have a baby. Clare seduces and starts a relationship with Bobby, and she eventually becomes pregnant by him. Their romance occasionally is disrupted by sparks of jealousy between the two men until Jonathan, tired of being the third wheel, disappears without warning. He re-enters their lives when his father Ned dies and Bobby and Clare travel to Phoenix, Arizona for the services. The three take Neds car back east with them, and they impulsively decide to buy a house near Woodstock, New York, where Bobby and Jonathan open and operate a cafe while Clare raises her daughter.

Jonathan discovers what appears to be a Kaposis sarcoma lesion on his groin and, although Bobby tries to convince him its simply a bruise, others soon appear. Clare begins to feel left out, seeing the close relationship Jonathan and Bobby share. One day, she takes the baby for what ostensibly is a brief visit to her mother in Philadelphia, but Bobby and Jonathan accurately suspect she has no intention of returning and Bobby decides to care for Jonathan during his last days. On a cold winter day some months later, Bobby and Jonathan scatter Neds ashes in the field behind their home, and Jonathan (who now visibly appears to be ill) lets Bobby know he would like his own ashes scattered in the same place, following his now inevitable early death from AIDS.

==Cast==
*Colin Farrell ..... Bobby Morrow (1982)
*Dallas Roberts ..... Jonathan Glover (1982)
*Robin Wright Penn ..... Clare
*Sissy Spacek ..... Alice Glover
*Matt Frewer ..... Ned Glover
*Erik Scott Smith ..... Bobby Morrow (1974)
*Harris Allan ..... Jonathan Glover (1974)
*Asia Vieira .... Emily

==Production== Schomberg and Toronto in Ontario, Canada.
 New York Los Angeles Gay and Lesbian Film Festival before going into limited release in the US. It grossed $64,728 on five screens on its opening weekend. It eventually earned $1,029,872 in the US and $519,083 in foreign markets for a total worldwide box office of $1,548,955. 

==Critical reception==
A.O. Scott of The New York Times observed, "As a novelist Mr. Cunningham can carry elusive, complex emotions on the current of his lovely, intelligent prose. A screenwriter, though, is more tightly bound to conventions of chronology and perspective, and in parceling his story into discrete scenes, Mr. Cunningham has turned a delicate novel into a bland and clumsy film . . . so thoroughly decent in its intentions and so tactful in its methods that people are likely to persuade themselves that its better than it is, which is not very good . . . The actors do what they can to import some of the texture of life into a project that is overly preoccupied with the idea of life, but the mannered self-consciousness of the script and the direction keeps flattening them into types." 

Roger Ebert of the Chicago Sun-Times said, "The movie exists outside our expectations for such stories. Nothing about it is conventional. The three-member household is puzzling not only to us, but to its members. We expect conflict, resolution, an ending happy or sad, but what we get is mostly life, muddling through . . . Colin Farrell is astonishing in the movie, not least because the character is such a departure from everything he has done before." 

Mick LaSalle of the San Francisco Chronicle stated, "What we have here . . . is a movie about a friendship and about the changing nature of families. We also have a movie about what it was like to be a child in the late 1960s, a teenager in the mid-1970s and a young adult in the early 1980s. In these aspects, the film is sensitive, sociologically accurate and emotionally true. But the picture is also the story of one character in particular, Bobby, and when it comes to Bobby, A Home at the End of the World is sappy and bogus." He added, "Farrell is not the first actor anyone would cast as an innocent, and he seems to know that and is keen on making good. His speech is tentative but true. His eyes are darting but soulful. The effort is there, but its a performance you end up rooting for rather than enjoying, because theres no way to just relax and watch." 

Peter Travers of Rolling Stone awarded the film three out of four stars, calling it "funny and heartfelt" and "a small treasure." He added, "Farrells astutely judged portrayal . . . is a career highlight" and "Stage director Michael Mayer (Side Man) makes a striking debut in film." 

David Rooney of Variety (magazine)|Variety called the film "emotionally rich drama" "driven by soulful performances." He added, "Strong word of mouth could help elevate this touching film beyond its core audience of gay men and admirers of the book." 

==Awards and nominations== National Board GLAAD Media Gotham Award Irish Film Award for Best Actor.

==See also==
 
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 