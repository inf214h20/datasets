Dushman Duniya Ka
 
{{Infobox film
| name           = Dushman Duniya Ka दुशमन दुनिया का  
| image          = Duushman.jpg
| caption        = Theatrical release poster Mehmood
| producer       = Ashok Mishra
| writer         = Aziz Quaisi
| starring       = Jeetendra Sumalatha Laila Mehdin Shahrukh Khan Salman Khan
| music          = Anu Malik
| music release  = 
| country        = India
| cinematography = Shankar Naidu
| editing        = Heera
| distributor    = 
| released       = 20 September 1996
| runtime        = 162 mins Hindi
| budget         = 
| gross          = 
}} Mehmood and produced by Ashok Mishra. The film stars Laila Mehdin and newcomer Manzoor Ali in the lead roles alongside Jeetendra and Sumalatha. Shahrukh Khan and Salman Khan have special appearances in the film. It has music by then struggler Himesh Reshammiya, who debuted in this film.

==Plot summary==
The movie focuses on Mahesh (Jeetendra), an honest and diligent young man, who has no knowledge of his background as he was raised in an orphanage. One day, out with his friend Badru (Shahrukh Khan), he meets Reshma (Sumalatha), who is also an orphan. Badru advices Mahesh to make a move on her, though as he approaches her, the two fall in love and Badru decides to get them married. They then live a harmonious life, and soon are proud parents of a healthy young boy who they name Lucky (Manzoor Ali). Lucky grows up to be as honest and diligent as his dad, and his future appears to be very bright. When Lucky sees his father smoking, he starts to as well, but when his father stops him, he explains that an old man around his school sells him cigarettes. When Badru and Mahesh approach the old man, he tries to run. Badru chases after him, and is then run over by a truck. Badru tragically dies, leaving Mahesh, Reshma and Lucky on their own. 

Years later, when Lucky grows up, he falls in love with Lata (Laila Mehdin), and both plan to marry soon with the blessings of their respective parents. Then Maheshs world is turned upside down when he finds drugs in Luckys pocket. He speaks to Lucky about this, and Lucky promises to give up drugs forever, and does live up to his promise. While driving home one day, Mahesh witnesses a drug peddler selling drugs to a group of youths which also included Lucky. He notifies the police, and they arrest the youths, but the peddler manages to escape. All the other youth tested positive for drugs, except for Lucky. When he returns home, his parents refuse to believe him, this angers him and he and his father come close to fisticuffs. 

Mahesh asks Lucky to leave, and he does so. Even Lata has left him and dumped him by now. Lucky regresses to drugs and his friends, until he runs out of money. Now he must either steal or even consider killing someone for money, - and the only ones he could think of taking money from are his parents. What will be Luckys fate?

== Cast ==
* Jeetendra as Mahesh Thakur
* Manzoor Ali as Lucky 
* Sumalatha as Reshma
* Laila Mehdin as Lata
* Johnny Lever as Head Master
* Farida Jalal as the Manager of womens shelter
* Mehmood Ali as Bakrewale baba
* Shahrukh Khan as Badru (Special Appearance)
* Salman Khan as Salman (Special Appearance)

==Production==
The film marked the debut of Mehmood Alis son Manzoor Ali who did not act in any film after this. Actors Shahrukh Khan and Salman Khan both have special appearances in the film, although they were seen in every trailer and promo of the film. They are both also shown together in all the wallpapers of the film. Shahrukh only has a 15 minute role in the beginning of the film and Salman has a five minute role halfway through the film.

== External links ==
*  

 
 
 