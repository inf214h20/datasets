Black Angel
 
{{Infobox film
| name           = Black Angel
| image          = Black Angel Poster.jpg
| image_size     = 150px
| alt            = 
| caption        = Theatrical release poster
| director       = Roy William Neill 
| producer       = Tom McKnight Roy William Neill
| screenplay     = Roy Chanslor
| based on       =  
| starring       = Dan Duryea June Vincent Peter Lorre Broderick Crawford
| music          = Frank Skinner
| cinematography = Paul Ivano
| editing        = Saul A. Goodkind
| distributor    = Universal Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Black Angel by Cornell Woolrich. The film was director Roy William Neills last film. 

==Plot==
A falsely convicted mans wife, Catherine (June Vincent), and an alcoholic composer and pianist, Martin (Dan Duryea), team up in an attempt to clear her husband of the murder of a blonde singer, who had been Martins wife. Their investigation leads them to face-to-face confrontations with a determined policeman (Broderick Crawford) and a shifty nightclub owner (Peter Lorre), who Catherine and Martin suspect may be the real killer.

==Cast==
* Dan Duryea as Martin Blair
* June Vincent as Catherine Bennett
* Peter Lorre as Marko
* Broderick Crawford as Captain Flood
* Constance Dowling as Mavis Marlowe
* Wallace Ford as Joe
* Junius Matthews as Dr. Courtney

==Reception==
Dark City: The Film Noir, by Spencer Selby, calls Black Angel: 
"Important, stylish B-noir, featuring Dan Duryea as the ironic central character".  

Writer Cornell Woolrich reportedly hated this adaption of his story which, aside from the conclusion, differed greatly from his book.  

==References==

===Notes===
 

===Additional references===
*  
*  

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 