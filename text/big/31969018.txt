Damadamm!
 
 
{{Infobox film
| name           = Damadamm
| image          = Damadamm.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Swapna Waghmare Joshi
| producer       = Himesh Reshammiya
| writer         = Subrat Sinha
| starring       = Himesh Reshammiya Sonal Sehgal Purbi Joshi Sachin Gupta
| cinematography = 
| editing        = 
| studio         = 
| distributor    = HR Musik
| released       =  
| runtime        = 122 min
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}
Damadamm! is a 2011 Bollywood romantic comedy film directed by Swapna Waghmare Joshi. The film stars Himesh Reshammiya opposite Sonal Sehgal and Purbi Joshi in lead roles. Himesh Reshammiya plays a script writer who is fed up of his girlfriend who always suspects him, though after she goes on holiday he openly parties and flirts with other girls. The real chaos starts once his girlfriend returns. The film released on 27 October 2011, received positive reviews from critics .

==Plot==
Damadamm is about the life of Sameer (Himesh Reshammiya), a writer who writes scripts for Indian films. He works with his girlfriend, Shikha (Purbi Joshi) at the industry. Shikha is totally over-possessive over Sameer, and suspects him all the time. Due to a speciallity, Sameer cant dump her either. He has no idea how to get rid of Shikhas suspicion, until one day her family invites her over for a relatives wedding. She leaves for a few weeks, and until then, Sameer gets his total freedom. He drinks; parties and even flirts with random girls. One day, enters a new girl into his office, Sanjana (Sonal Sehgal) and its love at first-sight for both of them. The two are then assigned to work together on a film, but Sameer has to be careful, as Sanjana is his boss younger sister. Whilst working together, Sameer and Sanjana start to fall in love. 

During this, Sameer even starts to ignore Shikhas phone calls. Getting fed up, Shikha arrives back home, when she sees how close Sameer and Sanjana have gotten within 15 days she gets jealous, causing an argument with Sameer and the two break up. She then starts friendship with Sameer, after his boss arranges for Sameer and Sanjanas wedding. But before the wedding Sameer eventually realises he truly loved Shikha and cant live without her, so he breaks the marriage with Sanjana. Therefore, his boss sacks him, whereas Sameer replies that he would rather pick his girlfriend Shikha then his work. Sameer manages to convince Shikha to give him another chance.

Later the boss rehires both Sameer & Shikha and even throws them a party. (The film ends with celebrations and the song "Umrao Jaan").

==Cast==
* Himesh Reshammiya as Sameer "Sam" / Babu
* Purbi Joshi as Shikha
* Sonal Sehgal as Sanjana
* Rajesh Khattar as Sameers boss / Sanjanas brother
* Lily Patel as Lily Aunty

==Reception==
===Reviews===
Taran Adarsh of Bollywood Hungama says "On the whole, DAMADAMM isnt bad, but it isnt great either. Though it has a hit score to its credit and some endearing moments, it will have to rely on a strong word of mouth to withstand a mighty opponent  ."  Mayank Shekhar of Hindustan Times added, "This film actually has a darn good script" 

Dainik Bhaskar reviews it as "The story is simple, predictable with pinch of humour and emotions. This love saga is something anyone would have watched millions of times, however, whats noteworthy is the placement of the sequences which are successful in letting the story be a simple one and not complicating it further". 

===Box office===
The film had taken a slow but steady start and the first week wrapped up at Rs 40&nbsp;million nett, domestically. First-day collections were around Rs 5&nbsp;million. Despite a strong contender in Ra.One, Damadamm! had a strong place at the ticket counter.

The five-day collections were around Rs 26.5&nbsp;million, and after Day 6, they were close to Rs 31.5&nbsp;million. Territory-wise, Mumbai and Delhi-UP had drawn the best response. The film held steady in its second week as well. 

Print and advertising costs, amounting to Rs 21&nbsp;million had already been recovered from music rights, Indian video and overseas rights, which were collectively sold at Rs 23.1&nbsp;million (Music: Rs 17.1&nbsp;million, Indian video: Rs 3&nbsp;million and overseas rights: Rs 3&nbsp;million). Also, the cost of production, pegged at Rs 30&nbsp;million, which was recovered through satellite rights. 

Film collected Rs.72.5&nbsp;million at the box office in 3rd week.

==Soundtrack==
{{Infobox Album |  
 Name = Damadamm!
| Type = Soundtrack
| Artist = Himesh Reshammiya
| Released = 7 September 2011
| Genre = Film soundtrack
| Length = 45:30
| Label = T-Series & HR Musik
| Producer = Himesh Reshammiya
| Last album = Bodyguard (2011 Hindi film)|Bodyguard  (2011)
| This album = Damadamm! (2011)
| Next album = Dangerous Ishhq  (2012) 
}}
 Sachin Gupta Sachin Gupta  and Shabbir Ahmed.Music of Damadamm! received positive response from both public and critics,songs like Umraojaan and Madhushala became Huge hits,Damadamm! is one of the best album of 2011 as stated by Bollywood Hungama 

===Tracklist===
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration 
|-
| Damadamm
| Himesh Reshammiya, Vinit Singh, Alam Gir Khan, Palak Muchhal, Shabab Sabri, Sabina Shaikh, Rubina Shaikh & Punnu Brar
| 5:54
|-
| Umrao Jaan
| Himesh Reshammiya & Purbi Joshi
| 4:12
|- 
| Aaja Ve
| Himesh Reshammiya
| 2:46
|-
| Madhushala
| Himesh Reshammiya & Aditi Singh Sharma
| 4:50
|-
| Yun Toh Mera Dil
| Himesh Reshammiya & Sadhana Sargam
| 2:58
|-
| Hum Tum
| Himesh Reshammiya & Vaishali Made
| 5:49
|-
| Tere Bina
| Himesh Reshammiya
| 5:57
|-
| I Need My Space
| Himesh Reshammiya
| 5:04
|-
| Mango
| Himesh Reshammiya & Aditi Singh Sharma
| 3:40
|-
| Bhool Jaaun Sachin Gupta
| 4:09
|}

==Home Media==
It premiered worldwide on Colors (TV channel)|Colors, just a week before its DVD release on 24 Dec 2011.

==References==
 

== External links ==
*  
*    on Sulekha

 
 
 