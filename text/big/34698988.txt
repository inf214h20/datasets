Nobody Else But You
{{Infobox film
| name     = Nobody Else But You
| image    = 
| director = Gérald Hustache-Mathieu
| producer = 
| writer = 
| Script = 
| starring = Jean-Paul Rouve   Sophie Quinton
| cinematography = Pierre Cottereau
| editing = Valérie Deseine
| music = Stéphane Lopez
| country = France
| language = French
| runtime = 102&nbsp;minutes
| released =  
}}
Nobody Else But You ( ) is a 2011 French thriller≈ film directed by Gérald Hustache-Mathieu.   

==Plot==
During his holidays on the countryside, David Rousseau, an author of crime novels, investigates the death of a beautiful young woman named Candice. He discovers that Candice had modeled her life and career after Marilyn Monroe. Rousseau refuses to believe she has committed suicide. Teaming up with a local policeman, he seeks to prove that Candice was murdered.

== Cast ==
* Jean-Paul Rouve - David Rousseau
* Sophie Quinton - Candice
* Guillaume Gouix - Gendarme Bruno Leloup
* Olivier Rabourdin - Colbert, police chief
* Joséphine de Meaux - Cathy
* Arsinée Khanjian - Juliette Geminy
* Clara Ponsot - the receptionist

== References ==
 

== External links ==
* 
* 

 
 
 


 