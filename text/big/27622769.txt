One Brief Summer
{{Infobox film
| name           = One Brief Summer
| image          = 
| image_size     = 
| caption        =  John Mackenzie
| producer       = Guido Coen
| writer         = 
| narrator       = 
| starring       = Felicity Gibson Clifford Evans Jennifer Hilary
| music          = Roger Webb
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = January 1970
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} John Mackenzie. It stars Felicity Gibson and Clifford Evans.  It was made at Twickenham Studios.

==Cast==
* Felicity Gibson as Susan Long
* Clifford Evans as Mark Stevens
* Jennifer Hilary as Jennifer
* Peter Egan as Bill Denton
* Jan Holden as Elizabeth
* Fanny Carby as Mrs. Shaw  
* Richard Vernon as Hayward  
* Helen Lindsay as Mrs. Hayward  
* Basil Moss as John Robertson  
* David Leland as Peter  
* Brian Wilde as Lambert
* Lockwood West as Ebert
* Carolyn Seymour as Marks Secretary

==References==
 

==External links==
* 

 

 
 
 
 
 

 