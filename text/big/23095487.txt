Pascali's Island (film)
{{Infobox film
| name           = Pascalis Island
| image          = Pascalis Island film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = James Dearden
| producer       = Tania Blunden   Paul Raphael   Mirella Sklavounou
| writer         = James Dearden
| based on       =  
| starring       = Ben Kingsley Charles Dance  Helen Mirren 
| music          = Loek Dikker
| cinematography = Roger Deakins
| editing        = Edward Marnier
| studio         = Channel Four Films    Dearfilm
| distributor    = Avenue Pictures Productions 
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} 1988 cinema British drama novel by Barry Unsworth. It was written and directed by James Dearden. It stars Ben Kingsley, Charles Dance and Helen Mirren. It was entered into the 1988 Cannes Film Festival.   
 Greek island of Symi and in Rhodes in the late summer of 1987.

==Plot==
In 1908 at Nisi, a small Greek Island under Ottoman rule, Turkish officials, Greek rebels, German emissaries and other foreign mercenaries mingle as they all try to keep the upper hand in that remote part of the crumbling Ottoman Empire. Basil Pascali, a half-British half-Cypriot man self considers a local feature in the island. Since his arrival twenty years before, he spies for the Sultan sending detailed reports about suspicious activities. Nobody reads or even acknowledges his observations, his payment has never been increased but still arrives regularly, so he continues his work as an informant with unfailing eagerness.

Pascali’s suspicions are aroused with the arrival of Anthony Bowles, a British archeologist, whose purpose in visiting the island is unclear. Basil quickly befriends Bowles at the hotel’s lounge bar and offers the archeologist his services as translator. Pascali introduces Bowles to his close friend, Lydia Neuman, a bohemian Austrian painter resident in the island. While Lydia and Anthony become smitten with each other, Pascali slips in to Bowles hotel room to investigate.
 
In Bowles suitcase, Pascali finds a fake antique, a small statues head, which makes him suspect that the archeologist may be a fraud. Needing help arranging a deal to lease some land from the local Pasha, Bowles hires Pascali as a translator. At Bowles insistence, the agreement is sealed officially with a contract. Suspecting Bowles intentions, Pascali warns him that the Pasha is not a man to be crossed. On their part, the Turkish authorities tell Pascali that he will be held responsible if Bowles fails to make the full payment.

Spying Bowles, Pascali finds the archeologist romancing Lydia; swimming naked with her in a remote cove. Pacali has been secretly in love with Lydia and envies the well tanned British archeologist. Turned on by the experience, Pascali relives his sexual frustration at a Turkish bath. Unexpectedly, Bowles wants to change the terms of his contract. He found some small archeological object of great significant, he claims, so he wants the right to excavate to be included in a new lease. Once again Pascali severs as translator and intermediary with the Pasha, who seeing the objects, a gold collar and the antique statues head, refuses to grant the excavation rights. The Turkish Pasha wants to buy the lease back; Bowles asks for a much larger sum that he originally paid. Pascali tells Bowles that he does not need to keep the pretense with him. Pascali knows that the small statues head is a forgery and that Bowles intention from the start was to swindle the Turkish authorities enticing them to buy the lease back at a higher amount. Pascali asks for part of Bowles earning in exchange for his silence forcing Bowles to concede.

The ploy becomes more complicated when, by chance, Bowles makes a real important archeological discovery: a large bronze statue of a boy from Greek times that is in pristine condition.

==Cast==
* Ben Kingsley - Basil Pascali
* Charles Dance - Anthony Bowles
* Helen Mirren - Lydia Neuman
* Kevork Malikyan - Mardosian
* George Murcell - Herr Gesing
* Nadim Sawalha - Pasha
* Stefan Gryff - Izzet Effendi
* Vernon Dobtcheff - Pariente Sheila Allen - Mrs. Marchant
* T. P. McKenna - Dr. Hogan
* Danielle Allen - Mrs. Hogan
* Nick Burnell - Chaudan
* Giorgos Oikonomou - Greek Rebel
* Alistair Campbell - Captain
* Ali Abatsis - Boy in Bath
* Brook Williams - Turkish Officer
* Joshua Losey - Turkish Soldier (as Josh Losey)
* Nick Karagiannis - Boy in Church

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 