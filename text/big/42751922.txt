True Love Story (film)
{{Infobox film
| name           = True Love Story
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Gitanjali Rao
| producer       = 
| writer         = Gitanjali Rao
| screenplay     = 
| story          = Gitanjali Rao
| based on       =  
| narrator       = 
| starring       = 
| music          = Tapatam - EarthSync/Laya Project
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 19 minutes
| country        = India
| language       = Silent
| budget         = 
| gross          = 
}}

True Love Story is a 2014 Indian animation short film written and directed by Gitanjali Rao. The silent film is a coming-of-age romance film set in streets of Mumbai, where a flower-seller falls in love with a bar dancer, that too in Bollywood-fantasy style.  

In February 2014, the film won the Golden Conch Best Animation Film Award at the 2014 Mumbai International Film Festival (MIFF).   At the 2014 Cannes Film Festival the film was one of 10 selected short films at 2014 Cannes Film Festival#Semaine de la Critique .28International Critics.27 Week.29|Critics Week.    

Her debut animation short, Printed Rainbow (2006) had won the Kodak Short Film Award, Small Golden Rail and the Young Critics Award  at Critics Week section at Cannes in 2006. The film has also won the Golden Conch for Best Animation Film in the 2006 Mumbai International Film Festival.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 