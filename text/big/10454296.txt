Heroes (2008 film)
{{Infobox Film |
| name           = Heroes
| image          = Heroes Hindi Film.jpg
| caption        = Theatrical release poster
| director       = Samir Karnik
| producer       = Bharat Shah Vikas Kapoor Samir Karnik
| story          = Aseem Arora Samir Karnik
| screenplay     = Aseem Arora Samir Karnik
| starring       = Mithun Chakraborty  Sunny Deol  Salman Khan  Bobby Deol  Preity Zinta  Sohail Khan   Vatsal Sheth
Dino Morea  Amrita Arora
| cinematography = Binod Pradhan Gopal Shah
| editing        = Sanjay Sankla
| distributor    = Future Picture Company Pvt.Ltd Mega Bollywood
| music          = Sajid-Wajid Monty Sharma
| released       = October 24, 2008
| runtime        = 
| language       = Hindi
| country        = India
| awards         = 
| budget         = Rs. 20 crores   
}}

Heroes is a 2008 Hindi film directed by Samir Karnik and starring Mithun Chakraborty, Sunny Deol, Salman Khan, Bobby Deol, Preity Zinta, Sohail Khan, Vatsal Sheth and Dino Morea. The film is written by Aseem Arora. Although initially set to be released on 6 June 2008,    it was pushed to 24 October 2008, the opening weekend of the holiday Diwali. On 22 November 2008 Heroes script was asked to be part of the Academy of Motion Picture Arts and Sciences.

==Plot==
Two film academy students, Ali (Vatsal Sheth) and Sameer (Sohail Khan), must make a movie in order to graduate. They choose to create a documentary illustrating reasons not to join the Indian Armed Forces, and go on a motorcycle road trip bearing three letters they have been given to deliver &mdash; each from a slain soldier to his family.

On their first stop, in Atari, Amritsar, they meet the widow, Kuljeet Kaur (Preity Zinta), and the son, Jassi, of a Sikh soldier, Balkar Singh (Salman Khan), who was Killed in action (KIA) three years earlier. The students find that the entire village is very proud of the heroic officer and his sacrifice for the country. When they are flying kites, their kite is cut and it falls beyond a certain fence, in another field. Jassi tells them that the area beyond the fence is Pakistan, and the fence is the Border.

The students second stop, Himachal, finds them meeting the wheelchair-bound former Air Force pilot Vikram Shergill (Sunny Deol), whose Army officer brother, Dhananjay Shergill (Bobby Deol), had also been (KIA). There they see that Vikram is very proud of his brothers sacrifice for his country. He shows them how he has come to terms with his own grief.

The third letter is to be delivered to a certain Mr. Naqvi (Mithun Chakraborty) and his wife Mrs. Naqvi (Prateeksha Lonkar). In between, their bike runs out of petrol, and they hitch a ride on a military convoy heading to a nearby base. They witness coffins of martyrs in their truck, and the driver quotes an inspiring poem. At the base, they talk to the Regiment commander and find another letter by Lt. Sahil Naqvi (Dino Morea) which they request to deliver it themselves. They see that Mrs. Naqvi is busy in a tea party, hardly paying any attention. Sameer accuses her of not loving her son, and says that Sahil was a coward. Mrs. Naqvi plays them a tape which Sahil had recorded in war, and he saved a juniors life. She tells him that she and her husband have been affected, and the Tea Parties serve as a distraction for them. They leave, but return the next day, and slowly bring the couples life back to normal.
 America as the trip has changed their outlook. They try to join the Army but fail. Hence, they start a school to share their experience. Some years later, Sameer and Ali are waking around their School campus. A man in olive green uniform (Salman Khan) approaches them. This is revealed to be Jassi, who will soon graduate from the IMA. The movie ends with the statement, "You dont have to be a soldier to love your country".

== Production ==
Originally titled Mera Bharat Mahaan (My India is Great), the film was rechristened  Heroes, to avoid sounding jingoistic. 

Filming began on 15 June 2007. Salman and Preity are paired opposite each other, in their fifth film together.    Shooting took place in Ladakh,    Chandigarh, Punjab and Delhi.  Some scenes were shot in Pangong Tso featuring Salman Khan, Sohail Khan and Vatsal Sheth. 

==Cast==

{| class="wikitable"
|-
! Actor/Actress
! Role
|- Mithun Chakraborty|| Dr. Naqvi
|- Sunny Deol|| Vikram Shergill
|- Bobby Deol|| Dhananjay Shergill
|- Salman Khan|| Balkar Singh/ Jassvinder Singh
|- Preity Zinta|| Kuljeet Kaur
|- Sohail Khan|| Sameer (Saandh)
|- Vatsal Sheth|| Ali (Nawab Sahib)
|- Dino Morea|| Sahil Naqvi
|- Amrita Arora|| Piya (Saandhs girlfriend)
|- Riya Sen|| Shivani (Alis girlfriend)
|- Mohnish Behl|| Akash Sarin
|- Prateeksha Lonkar||Mrs. Naqvi
|- Tinnu Anand|| Film Institutes Director
|- Vivek Shauq|| Professor 
|}

==Crew==
* Director: Samir Karnik
* Script and dialog: Samir Karnik, Aseem Arora
* Story: Aseem Arora
* Producers: Bharat Shah, Vikas Kapoor, Samir Karnik
* Directors of photography: Binod Pradhan, Gopal Shah
* Stunts: Peter Hein
* Music: Monty Sharma (credited as Monty), Sajid-Wajid
* Lyrics: Jalees Sherwani, Rahul B. Seth
* Editor: Sanjay Sankla
* Production designer: Jitendra Kawa
* Costumes: Shamayel Khan
* Choreography: Piyush Panchal

==Release==

===Critical Reception===
Heroes received generally positive reviews from critics. Taran Adarsh of Bollywood Hungama gave the movie 3.5 stars out of 5.

  movies gave the movie 4 out 5.

The films writer Aseem Arora was nominated for a Filmfare Award for Best Story.

===Box office===
Ths film grossed $326,425 in its opening week. It was an above-average performer at box office. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 