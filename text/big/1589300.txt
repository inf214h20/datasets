Very Bad Things
{{Infobox film
| name          = Very Bad Things
| image         = Very Bad Things.jpg
| caption       = Theatrical release poster
| director      = Peter Berg
| producer      = Cindy Cowan Diane Nabatoff Michael Schiffer
| title=Full cast and crew for Very Bad Things (1998)
| url=http://www.imdb.com/title/tt0124198/fullcredits
| work=IMDb
| accessdate     = 2009-06-06
| screenplay     = Peter Berg
| story          =
| based on       =  
| narrator       = Daniel Stern Jeremy Piven Christian Slater with Leland Orser and Jeanne Tripplehorn
| music          = Stewart Copeland
| cinematography = David Hennings
| editing        = Dan Lebental
| studio         = Initial Entertainment Group Interscope Communications
| distributor    = PolyGram Filmed Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = $9.9 million
}} Daniel Stern, Jeremy Piven, Christian Slater, Leland Orser and Jeanne Tripplehorn.

==Plot== 
Kyle Fisher (Jon Favreau) and his best man Charles Moore (Leland Orser) wait uncomfortably for Fishers wedding to his "Bridezilla" fiancée Laura (Cameron Diaz), reminiscing about recent events over the past few days.

Going back a few days, Fisher organizes a crazy bachelor party in a Las Vegas hotel with his best friends: Moore, Robert Boyd (Christian Slater), and brothers Adam and Michael Berkow (Daniel Stern and Jeremy Piven), spending the night with drinks, drugs and a stripper/prostitute, Tina (Kobe Tai). Things go awry when Michael gets drunk and pays Tina extra money for sex in the bathroom, then accidentally slams her against a wall and puts a towel hook through her head, killing her instantly. As the group argues over what to do -- Adam insists that they call the police, Boyd demands that they bury Tinas body in the desert -- a security guard comes to investigate the noise and sees Tinas corpse in the bathroom. In desperation, Boyd stabs the guard to death and the group is forced to dismember the bodies to take them to the desert for burial.

Over the next few days, Adam remains riddled with guilt over his role in the cover-up, particularly when the guards disappearance appears in the paper. Meanwhile, Boyd becomes obsessed with gruesome death. At the rehearsal dinner, Adam cracks under the pressure and fights Michael in the parking lot. Though the group breaks up the fight and convinces Michael to leave, he tries to ram his jeep into Adams beloved minivan. Adam desperately runs in front of his minivan to stop him and is crushed in the inevitable collision. In the hospital, Adam whispers something to his wife Lois (Jeanne Tripplehorn) before succumbing to his wounds. Michael, wracked with guilt, becomes slightly insane over his brothers death.

Lois calls the remaining men and demands answers about what happened in Las Vegas. Fisher desperately makes up a story about Adam sleeping with a prostitute, and not for the first time. But Boyd, suspecting that Lois does not believe them and will call the police, invades her home that night; they clash violently and he kills her. He then calls Fisher and Moore to bring Michael to the house, where he quietly shoots him dead before rejoining Fisher and Moore in the car. He concocts an alibi about a Michael/Lois/Adam love triangle to answer any interrogation by police. Fisher and Laura are awarded custody of Adam and Lois sons, but theyre conned out of most of Adams life-insurance policy. Fisher breaks down and confesses the story to Laura privately, but she shrugs most of it off and insists that the wedding will proceed as planned. 

On the wedding day, Boyd confronts Fisher and demands Adams life-insurance money; Fisher refuses and a fight ensues. Intervening, Laura viciously beats Boyd with a hatstand, but during the wedding, Fisher and Moore realize that best-man Boyd has the wedding rings. Moore retrieves them and Boyd succumbs to his injuries while Laura and Fisher are married. Once the newlyweds have a private moment, Laura demands that Fisher bury Boyds body in the desert, then leave no witnesses by killing Moore and Adams dog. Fisher and Moore head out to bury Boyd with the bodies of Michael, Tina, and the guard, but Fisher cannot go through with killing Moore or the dog. Overcome reminiscing about happier times with his friends, Fisher loses focus and crashes into a passing car. The film ends with Fisher, Moore, and the dog left disabled and paralyzed, with Laura running out of the house and onto the road screaming as she knows that shell be forced to look after them and Adams sons for the rest of her life.

==Cast==
* Cameron Diaz as Laura Garrety-Fisher
* Jon Favreau as Kyle Fisher Daniel Stern as Adam Berkow, the first member of the group to die after accidentally being run over by his own brother.
* Jeremy Piven as Michael Berkow, the second member of the group to die after getting shot dead by Boyd.
* Christian Slater as Robert Boyd, the third member of the group to die after being fatally killed by Laura.
* Leland Orser as Charles Moore, the most quiet of the group. Like Adam, he becomes wracked with fear over repercussions for his role in the two killings. Though he does not die, he ends up paralysed from the neck down as a result of a car crash with Kyle.
* Jeanne Tripplehorn as Lois Berkow, another victim after being killed by Boyd when she begins to question the groups actions in Las Vegas.
* Joey Zimmerman as Adam Berkow Jr.
* Kobe Tai as Tina, a stripper whose accidental death sets in motion the events of the entire film.

==Reception==
The film scored a 44% on Rotten Tomatoes, with the consensus, "Mean-spirited and empty."   Roger Ebert wrote that Very Bad Things is "not a bad movie, just a reprehensible one."  Some critics appreciated the cold-blooded approach, however. Mitland McDonagh of TV Guide said, "In a world filled with crude movie sitcoms, Bergs bitter, worst-possible-case scenario really does stand alone." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 