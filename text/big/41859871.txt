The Priest (film)
{{Infobox film
| name     = The Priest
| image    = The Priest.jpg
| director = Vladimir Khotinenko
| producer = 
| Script   = 
| starring = Sergei Makovetsky
| cinematography = 
| music    = 
| country  = Russia
| language = Russian
| runtime  = 130 minutes
| released =   
}}

The Priest ( ) is a 2009 Russian drama film directed by Vladimir Khotinenko. 

== Plot == 

The film begins in June 1941 in the backwater village of Tikhoe in Latvia. Priest Alexander carries out the duties of his ministry, helped by his wife, Alevtina. Two days later the Nazi invaders enter the village. The Nazi invaders are keen to reopen the Orthodox churches closed by the Soviet power. Alexander is offered a mission to Pskov oblast. An orthodox church building, confiscated and turned into a hall for film showings and the like, is restored to its former use, the church bell rescued from the lake etc. However life under the Nazis is ambiguous and the priest must walk a tightrope (metaphorically) between faithful Christian service and loyalty to his country and people. A poignant scene is the Easter service, celebrated along with POWs surrounded by German guards. Alexander and Alevtina also harbour Jewish orphans. Alevtina falls ill from contact with the POWs and puts the children first by losing herself in a snowstorm lest she infect the orphans. The plot concludes with the Soviet authorities back in power and the priest imprisoned by the NKVD. The epilogue shows the priest decades later, visited by the orphans he saved many years before.

== Cast ==
* Sergei Makovetsky - Otets Aleksandr Ionin 
* Nina Usatova - Matushka Alevtina
* Liza Arzamasova - Eva 
* Kirill Pletnyov - Aleksandr Lugotintsev

== References ==
 

== External links ==
* 

 
 
 
 
 
 


 