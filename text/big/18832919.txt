Do Not Go Gentle
Do directed by Emlyn Williams. It was the United Kingdoms submission to the 74th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

The film was produced by Alun Ffred Jones and stars Stewart Jones, Gwenno Hodgkins, Arwel Gruffydd, Marged Esli, Caroline Berry and Romolo Bruni.

The Welsh title translates literally as "Age Pledge". The English title is a reference to the Dylan Thomas poem "Do not go gentle into that good night".

==See also==
 
*Cinema of the United Kingdom
*List of submissions to the 74th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 
*  at britishcouncil.org

 
 
 


 