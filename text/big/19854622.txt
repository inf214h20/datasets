Nandu (film)
{{Infobox film
| name = Nandu
| image = 
| caption = Official DVD Box Cover
| director = J. Mahendran
| producer =S. Dakshnamoorthy
| writer =J. Mahendran
| narrator = Ashwini Kumarimuthu Rajasekhar Rajendra Archana S.N. Parvathi Sivagami Vanitha
| music = Ilaiyaraaja   
| music = Ilayaraja Ashok Kumar
| editing =A. Paul Duraisingh
| studio = Rangaraj Creations
| distributor = Rangaraj Creations
| released = 17 April 1981
| runtime =110 mins
| country = India Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}} Tamil feature film directed by J. Mahendran. The films score and soundtrack are composed by Ilayaraja. 

==Plot==

Ram Kumar Sharma is a sickly person living in Lucknow in a large family. He suffers from asthma and does not like the behavior of his autocratic father but is deeply attached to his loving mother. Unable to get along with his father who compels him to marry a girl of his choice, the protagonist leaves for Chennai. He finds a job there and meets Seetha (Ashwini), whom he eventually marries. Once settled, he goes around looking for a house with the help of house broker - Kumari Muthu. Incidentally, he finds Seetha residing as a tenant in a portion under the very same house. He likes the portion and agrees to start living there.

The lives of the girls who live in portions of the large house are brightened by the appearance of the engineer from Lucknow. Of them, two of them try to impress Sharma. One of them is the house owners daughter, while the other is Seetha. Eventually, he ends up marrying Seetha.

Nandu - The significance of the title "Nandu" is not known until the climax of the story. Nandu means crab in Tamil. The protagonist plays an asthma patient in the movie.

== Soundtrack ==
Lyrics are written by Gangai Amaran (Tamil) and P. B. Srinivas (Hindi) and scored by Ilayaraja.

* Paaduthamma Kaatril Alaikal - Title Song
* Alli thantha Bhoomi - Malaysia Vasudevan
* Kaise Kahoon Kuch Kahna Sakoon - Bhupendra, S. Janaki|S.Janaki
* Manjal Veyil Maalai Itta Poove - Uma Ramanan
* Hum Hai Akele - S. Janaki|S.Janaki

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 