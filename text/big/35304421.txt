Jobs (film)
 
 
{{infobox film
| name           = Jobs
| image          = Jobs (film).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joshua Michael Stern
| producer       = Mark Hulme
| writer         = Matt Whiteley
| starring       =  Ashton Kutcher Dermot Mulroney Josh Gad Lukas Haas J. K. Simmons Lesley Ann Warren Ron Eldard Ahna OReilly John Getz James Woods Matthew Modine 
| music          = John Debney
| cinematography = Russell Carpenter (U.S.) Aseem Bajaj (India) 
| editing        = Robert Komatsu
| studio         = Endgame Entertainment Five Star Feature Films   Venture Forth
| distributor    = Open Road Films (United States) Entertainment One (Canada)
| released       =     
| runtime        = 122 minutes 
| country        = United States
| language       = English
| budget         = $12 million   
| gross          = $35.9 million 
}} Apple Computers (now Apple Inc.) co-founder Steve Wozniak.

Jobs was chosen to close the 2013 Sundance Film Festival.      

== Plot ==
The film opens in 2001 with a slightly older Steve Jobs (Ashton Kutcher) introducing the iPod at an Apple Town Hall meeting.
 Baba Ram Dass. Influenced by this book and his experiences with History of lysergic acid diethylamide|LSD, Jobs and Kottke spend time in Hippie trail|India.

Two years later, Jobs is back in Los Altos, California living at home with his adoptive parents Paul (John Getz) and Clara (Lesley Ann Warren). He is working for Atari and develops a partnership with his friend Steve Wozniak (Josh Gad) after he sees that Wozniak has built a personal computer (the Apple I). They name their new company Apple Computer, though there already is a company called Apple Records that is owned by The Beatles (Wozniak then teases Jobs that this is symbolic of his preference for Bob Dylan). Wozniak gives a demonstration of the Apple I at the Homebrew Computer Club, where Jobs receives a contract  with  Paul Terrell (Brad William Henke). Jobs asks his mechanic/carpenter father Paul  for permission to use the family garage (set up as a carpentry/tool center) for his new company. His father agrees and Jobs then adds Kottke, Bill Fernandez (Victor Rasuk), Bill Atkinson (Nelson Franklin), Chris Espinosa (Eddie Hassell), and later Rod Holt (Ron Eldard) to the Apple team to build Apple I computers.  Terrell is disappointed by what they produce which forces Jobs to seek capital elsewhere. After many failed attempts by Jobs to gain venture capital, Mike Markkula (Dermot Mulroney) invests in the company which allows them to move forward.
 The Lisa Macintosh Group where he works with Bill Atkinson, Burrell Smith (Lenny Jacobson), Chris Espinosa, and Andy Hertzfeld (Elden Henson). He also forces the original team leader of the Macintosh group, Jef Raskin, out of it. Though the Macintosh is introduced with a great deal of fanfare in 1984, Jobs is forced out of the company by Sculley in 1985.
 dialogue for the Think Different commercial in 1997. Before the credits, there are original photos of all the main characters paired with clips from the film of the actor playing the part, plus a dedication to Steve Jobs.

==Cast==

===Apple===
* Ashton Kutcher as Steve Jobs
* Josh Gad as Steve Wozniak
* Lukas Haas as Daniel Kottke
* Victor Rasuk as Bill Fernandez
*Eddie Hassell as Chris Espinosa
* Ron Eldard as Rod Holt
* Nelson Franklin as Bill Atkinson
* Elden Henson as Andy Hertzfeld
* Lenny Jacobson as Burrell Smith
* Giles Matthey as Jonathan Ive
* Dermot Mulroney as Mike Markkula
* Matthew Modine as John Sculley
* J. K. Simmons as Arthur Rock
* Kevin Dunn as Gil Amelio

===Family===
* John Getz as Paul Jobs
* Lesley Ann Warren as Clara Jobs
* Abby Brammell as Laurene Powell Jobs
* Annika Bertea as Lisa Brennan-Jobs (adult) Lisa Brennan (child)
* Ahna OReilly as Chrisann Brennan

===Other===
* James Woods as Jack Dudman Al Alcorn
* Brad William Henke as Paul Terrell
* Robert Pine as Edgar S. Woolard, Jr.
* Amanda Crew as Julie

==Production==

===Development===
 .]]
Screenwriter Matt Whiteley began work on the screenplay around the time Steve Jobs took medical leave from Apple to battle pancreatic cancer.  Director Joshua Michael Stern stated in an interview that all material for the screenplay was collected via research and interviews:

 Mark Hulme, our producer, had an expert team of researchers to comb through all public records and interviews that had anything to do with Steve Jobs. Mark, the screenwriter and the research team, also took it upon themselves to interview quite a large pool of people who either worked at Apple or worked with Steve to make sure we portrayed as accurate a portrait and telling of the events possible within the constraints of the films length.  

===Production===
Production began in June 2012 at Jobs childhood home in Los Altos, California, with the help of Jobs stepmother, Marilyn Jobs (who still lives there).  It was also observed by his sister Patty.  UCLA was used as the backdrop for Jobs time at Reed College.  The majority of the film was shot in the Los Angeles region.      

====India==== 1974 trek Jama Masjid, Safdarjung Tomb and Humayun’s Tomb."     Aseem Bajaj (Bandit Queen, Chameli (film)|Chameli, and Khoya Khoya Chand) served as cinematographer for the scenes shot in India. Bajaj notes that they "shot guerrilla style in the crazy and mad by-lanes of Chandni Chowk in Old Delhi. We shot near the Red Fort and the famous Jama Masjid for two full days with multiple cameras spread across everywhere. Ashton stood frozen with the chaos staring right in his face which helped us capture what Steve Jobs must have felt on his visit to India."    

==Release and reception==

===Box office===
The Business Insider described the films release as a box office bomb. It earned $6.7 million in its first weekend and placed seventh overall. 

It had a worldwide gross of $35,931,410. 

===Critical reception=== weighted average of 4.9/10. The sites consensus reads, "An ambitious but skin-deep portrait of an influential, complex figure, Jobs often has the feel of an over-sentimentalized made-for-TV biopic."    Review aggregator Metacritic gave the film a score of 44 out of 100, based on 35 critics, indicating "mixed or average reviews". 

Bogdan Fedeles of   gave the film four out of five stars, describing it as an "almost ironically styled old-fashioned biopic" and stating that "this sharp look at the late Steve Jobs and the technological and cultural changes he brought about is entertaining and smart, with a great, career 2.0 performance from Ashton Kutcher."  Doris Toumarkine of Film Journal International describes the film as a "terrific dramatization of late Apple founder Steve Jobs rise from inspired, art-inclined hippie to corporate mogul and King of Cool delivers Mac-nificently and should gratify everyone from die-hard geeks to gawkers curious about entrepreneurial success." 

 , argues that "the film is beautifully shot and Kutchers portrayal of Jobs, while not spot-on, is pretty darned good. He certainly has the look down and the walk. But Ashton Kutcher also produced this film and hes definitely a better actor than producer. There are a lot of historical inaccuracies that just dont have to be there.   The great failing of this film is the same failing as with   years (which occupy less than 60 seconds of this 122 minute film) that turned Jobs from a brat into a leader, but they dont bother to cover that."  Mick LaSalle of the San Francisco Chronicle states that "at its best, its a good picture, and at its worst, its almost good."  Peter Travers of Rolling Stone suggests that "Kutcher nails the genius and narcissism. Its a quietly dazzling performance" but also notes that "Jobs is a one-man show that needed to go for broke and doesnt. My guess is that Jobs would give it a swat."  Contributor for Roger Ebert.com, Susan Wloszczyna, gave the movie 2/4 stars, saying that, "Rather than attempting a deeper plunge behind the whys and wherefores of the elite business-model gospel according to Apple Inc. guru Steve Jobs and – more importantly – what it says about our culture, the filmmakers follow the easy rise-fall-rise-again blueprint familiar to anyone who has seen an episode of VH1s Behind the Music."    She further discusses how Kutchers performance and the overall movie failed to portray Jobs in iconic manner that current pop culture suggests even after Jobs passing. In a movie review for The New York Times, writer Manohla Dargis writes that Jobs was "inevitably unsatisfying"    and a result of a poor performance of the filmmakers rather than the actors themselves.

===Wozniak, Kottke, Fernandez, Espinosa, and Hertzfeld=== The Sony people got in contact with me too and in the end I went with them. You cant do both   and be paid."  At around the same time, he responded to the first promotional clip for the film on Gizmodo by stating that the "personalities are very wrong, although mine is closer... our relationship was so different than what was portrayed."   
 forthcoming Steve Jobs film.) When asked why he didnt at least correct the inaccuracies he saw, Wozniak said, I have a very busy life, and it came at a very busy time in my life."   

In an interview with Slashdot, Daniel Kottke states that he consulted on early versions of the screenplay and notes that "Ashtons very good. I have no complaints with him at all, no complaints with his portrayal of Jobs. The complaint that people would rightly have about the film is that it portrays Woz as not having the same vision as Steve Jobs, which is really unfair." He also said that the early versions of the screenplay "were painful. Really painful. I forwarded the first draft to Mike Markkula because they wanted his feedback, and Mike took such a bad reaction to it, he wouldnt have anything more to do with the project. By the time it got to the fourth draft, it was okay. It wasnt making me cringe."  Kottke also outlines various areas that were both accurate and inaccurate in the film. Bill Fernandez was part of the same interview but states that he didnt see the film because "the whole thing is a work of fiction, and I dont want to be upset by all the things that the screenwriter has invented and dont represent the truth." Kottke responded that he didnt think of the film as fiction because "I was involved early on in the film, and they really, sincerely tried to make it as accurate as they could."   

Chris Espinosa stated on Twitter, "FYI My position at Apple precludes my commenting on the #JobsMovie with the press or public. But I can say that I enjoyed watching the film." 

The TV show John Wants Answers took Steve Wozniak, Daniel Kottke, and Andy Hertzfeld through the film scene by scene and discussed how the events actually occurred.   

== Original soundtrack ==
A number of classic rock, classical music, and contemporary works appeared in the film.  The commercial film soundtrack focuses on an original score by John Debney and includes some but not all of the classical and classic rock works. 

{{Track listing
| extra_column    = Singers
| title1          = Peace Train
| note1           = 1971
| extra1          = Cat Stevens 
| length1         =  
| title2          =  
| note2           = 18th century
| extra2          = Johann Sebastian Bach
| length2         = 
| title3          = House of the Rising Sun
| note3           = 1966
| extra3          = The Brymers      
| length3         = 
| title4          = Silver Ghost
| note4           = 1970
| extra4          = Parish Hall 
| length4         = Fantasie Impromptu in C-sharp minor, Op. posth. 66
| note5           = 1834
| extra5          = Frédéric Chopin 
| length5         = 
| title6          = Boots of Spanish Leather
| note6           = 1964
| extra6          = Bob Dylan 
| length6         =  Scarborough Fair
| extra7          = Dylan McDonald & Cassidy Cooper / Produced by Mason Cooper & Jerry Deaton 
| length7         =  
| title8          = There Were Times
| note8           = 2013
| extra8          = Freddy Monday
| length8         =
| title9          = Sacrifice
| note9           = 1960s
| extra9          = The Brymers  
| length9         = 
| title10         = Lifes Been Good
| note10          = 1978
| extra10         = Joe Walsh
| length10        =  Roll with the Changes
| note11           = 1978
| extra11         = REO Speedwagon 
| length11        = 
| title12         = Shine on Me
| extra12         = Matthew Cheadle
| length12        = 
| title13         = Walk on the Ocean
| note13          = 1992
| extra13         = Toad the Wet Sprocket
| length13        = 
| title14          = You Can Do (Whatever)
| note14           = 2013
| extra14          = Yusuf Islam 
| length14         =  
}}

==See also==
  Steve Jobs, an upcoming Universal Studios film that will be directed by Danny Boyle with a screenplay by Aaron Sorkin. Jobs will be portrayed by Michael Fassbender.
*   (2012) is a documentary that includes the entire 70-minute interview Jobs gave for Triumph of the Nerds in 1995.
* Pirates of Silicon Valley (1999), drama film about the rise of Apple Computer (Steve Jobs and Steve Wozniak) and Microsoft (Bill Gates, Steve Ballmer, Paul Allen) from 1971 to 1997. Jobs is portrayed by Noah Wyle.
* Triumph of the Nerds (1996), a television documentary about the rivalry between, and personalities of, Apple and Microsoft, written and hosted by Robert X. Cringely.

==Further reading==
*Gruman, Galen. " ." InfoWorld, August 19, 2013.

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 