Bartok the Magnificent
 

{{Infobox film
| name           = Bartok the Magnificent
| image          = Bartok the Magnificent.jpg
| image size     =
| caption        = DVD cover
| director       = Don Bluth Gary Goldman
| producer       = Don Bluth Gary Goldman Hank Azaria
| writer         = Jay Lacopo
| narrator       =
| starring       = Hank Azaria Kelsey Grammer Tim Curry Phillip Van Dyke Andrea Martin Catherine OHara Jennifer Tilly Diedrich Bader
| music          = Stephen Flaherty
| cinematography =
| editing        = Bob Bender Fiona Trayler
| studio         = Fox Animation Studios
| distributor    = 20th Century Fox Home Entertainment
| released       = November 16, 1999
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Bartok the Magnificent, directed by Don Bluth and Gary Goldman, is a 1999 direct-to-video Spin-off (media)|spin-off to the 1997 film Anastasia (1997 film)|Anastasia, which features Hank Azaria as the voice of Bartok, Kelsey Grammer as the voice of Zozi, and Jennifer Tilly as Piloff. It is a "family adventure...animated comedy" film, according to Fort Oglethorpe Press. http://news.google.com/newspapers?id=n5JOAAAAIBAJ&sjid=UEcDAAAAIBAJ&pg=3428,5431935&dq=bartok-the-magnificent&hl=en 

Although the film was released after Anastasia (1997 film)|Anastasia, it is unclear if the events in the film take place before or after those of its predecessor. While many of Don Bluths films have received sequels and spin-offs, this is the only spin-off Don Bluth has directed. Variety writer John Laydon explained how the films were connected: "The literally batty sidekick who swiped scenes throughout 1997s Anastasia is rewarded with his very own animated adventure in Bartok the Magnificent, a lightly diverting direct-to-video opus". He also argued that the film was a prequel "since the setting is pre-revolutionary Russia"   

==Production==
A spin-off film was devised as "Hollywood audiences went batty over the impish Bartok in Foxs 1997 animated musical Anastasia.    Chris Meledandri, then-president of 20th Century Fox Animation said "Once we thought about a lot of ideas, our favorite idea was the one you see". 

== Plot ==
Russia is being terrorized by an evil witch known as Baba Yaga (Andrea Martin), and the only one who is not afraid of her is Bartok the Magnificent (Hank Azaria). Bartok, an albino bat, has just arrived in Moscow and is impressing everyone with his performances, including Prince Ivan (Phillip Van Dyke). However, Ivans advisor, Ludmilla (Catherine OHara), finds Bartok annoying and naive, and she tries to make a Cossack stop his performance. After Bartoks show, a bear suddenly attacks. Bartok saves everyone by stunning the bear, knocking it over, and trapping it in a wagon.

Delighted with Bartoks bravery, everyone around him rewards him with gold, including Prince Ivan, who gives him a royal red ring, much to the displeasure of Ludmilla, who reminds him that the ring is only for members of the Romanov family, not commoners. She asks that he take the ring back, but Ivan disagrees, saying it is time for a change. Ludmilla, seeing that she cannot dissuade him, reluctantly allows it and they leave.

Ludmilla is still upset that Ivan has given a ring to a commoner, especially a street performer. Ivan retorts that that was his intention, and his friend Captain Vol (Diedrich Bader) agrees that Bartok was funny. Ludmilla, on the other hand, believes that Ivan needs to respect his duty to the crown, which incites Ivan, who is tired of listening to her, to say that he will do as he pleases and it is she who must respect the crown.

Meanwhile, Bartok is counting the money he received when the bear wakes up and scares him. It turns out Bartoks amazing rescue was just another act - the bear is Zozi (Kelsey Grammer), Bartoks business partner. Zozi is apprehensive about Ivans ring and agrees with Ludmilla, that the ring should be returned. Bartok stubbornly refuses to give it back since it was a gift.

Back in Moscow, Ivan is kidnapped by Baba Yaga, which leads to an immediate investigation. Ludmilla finds one of Baba Yagas iron teeth, and she informs the people what has transpired. When she asks for someone brave enough to rescue Prince Ivan, two children (Kelly Marie Berger and Zachary B. Charles) nominate Bartok.

Bartok and Zozi are on their way to St. Petersburg when Zozi spots the Cossacks coming after them. The pair become worried because they assume that Ludmilla wants Ivans ring returned. Bartok tries to conceal his identity, but he is brought before the people, who explain that Ivan has been taken by Baba Yaga, and that they are relying on him to rescue their prince. Bartok reluctantly accepts, and he and Zozi head to the Iron Forest to confront Baba Yaga and save Prince Ivan.

They find Baba Yagas hut, but must answer a riddle given by a giant skull ( ) is frozen to a boulder, Oblie (French Stewart), a giant blacksmith surrounded by an aura of fire, must be tricked into letting his crown be stolen, and the magic feather must be obtained without flight, using only the boulder Piloff was stuck to and Oblies crown.

He gathers the objects demanded, but Baba Yaga still needs something from Bartok himself. He offers everything he can think of, but Baba Yaga rejects everything and bursts out laughing. Bartok, outraged, begins to yell, and he upsets Baba Yaga by accusing her of lying and cheating, and claiming that everyone hates her. After he apologizes to her, he starts crying and Baba gets the most important ingredient: tears which are from Bartoks heart. She makes a magic potion from the items she had Bartok collect and reveals that she never took Prince Ivan and that the potion she made was meant for Bartok himself. Baba Yaga explains that when Bartok drinks the potion, whatever he is in his heart will show ten times in his exterior.

Bartok and Zozi return to town and lead Ludmilla and Vol up to the top of the tower where Ivan is imprisoned. However, when they arrive, Ludmilla locks Bartok and Vol up with Ivan and reveals she had Vol kidnap the prince (telling him to "get him out of the way" as in kill him, while Vol misunderstood and locked him up, supposedly for his own safety) while she framed Baba Yaga as part of her scheme to forcibly seize the Russian throne. She steals Bartoks magic potion and leaves Bartok, Ivan, and Vol trapped in a well tower filling up with water.

She drinks it, believing her beauty will become tenfold, singing "The Real Ludmilla Comes Out" as she descends the tower. Unbeknownst to her, the potion causes her to steadily transform into an enormous dragon as it is what she is on the inside. Upon this discovery, the incensed woman goes on a rampage through Moscow, setting many buildings alight with her newly acquired fire breath ability. Zozi then comes to the rescue, saving Bartok, Vol, and the Prince. Bartok battles Ludmilla and tricks her into climbing the tower. When she gets to the top, the tower starts to become unstable and causes the top of the tower to fall, flooding the streets and dousing the flames. As the townspeople gather around Ludmillas dead body, Zozi reveals that Bartok is a true hero, not only because he stopped Ludmilla, but because he had showed Baba Yaga compassion.

Bartok returns Ivans ring and Baba Yaga appears, writing "Bartok, The Magnificent" in the sky. Bartok gives Baba Yaga a goodbye hug as she and Piloff depart, counting on seeing Bartok again.

==Cast==
* Hank Azaria as Bartok
* Kelsey Grammer as Zozi
* Catherine OHara as Ludmilla
* Andrea Martin as Baba Yaga
* Tim Curry as The Skull, the entrance to Baba Yagas hut.
* Jennifer Tilly as Piloff, Baba Yagas pet
* French Stewart as Oble
* Phillip Van Dyke as Prince Ivan
* Diedrich Bader as Vol, Ivans friend and the Captain of the Guard.
* Glenn Shadix as Townspeople
* Danny Mann as Head Cossack

==Music== Oscar nominations, in which Bartok made his first appearance along with his master, Rasputin.

=== Song listing ===
{| class="wikitable"
|-
! Song number !! Title !! Performers !! Length
|-
| 1|| Baba Yaga || Ensemble vocals || 1:00
|-
| 2|| Bartok the Magnificent|| Bartok and village people|| 2:40
|-
| 3|| A Possible Hero|| Zozi and Bartok ||  1:40
|-
| 4|| Someones In My House || Baba Yaga and magical objects|| 1:55
|-
| 5|| The Real Ludmilla || Ludmilla and prisoners|| 2:05
|}

==Marketing==
In late 1999, pancake purveyor IHOP Corp. started selling 2 versions of Bartok, as part of a promotion for the direct-to-video film "Bartok The Magnificent". The company planned "to sell about 500,000 of the six-inch-high toys - Bartok Puppet and Turban Bartok - for $2.99 with any food purchase". It was "also offering $2 mail-in rebate coupons for the $20 video...and free activity books for children". 

==Release==
Bartok the Magnificent was first released on VHS and DVD by 20th Century Fox Home Entertainment on November 16, 1999,  and was later re-released in 2005 as part of a 2-disc set alongside Anastasia entitled Family Fun Edition. {{cite
web|url=http://web.archive.org/web/20051231053851/http://www.dvdtown.com/announcement/anastasiafamilyfuneditionondvd/2738/ |title=Anastasia: Family Fun Edition on DVD - DVD Town |publisher=Web.archive.org |date=2005-12-31 |accessdate=2013-10-18}}  Bartok the Magnificent was also included as a special feature on the March 2011 release of Anastasia on Blu-ray.

The "tape and DVD conclude with sing-along segments that reprise the...original tunes by Stephen Flaherty...and Lynn Ahrons"  - Bartok the Magnificent, A Possible Hero and Someones in My House.    Other DVD extras include also include Bartok and Anastasia trailers, and a Maze Game "that features three...mazes that you control with your remote control". 

==Visual and audio==
The aspect ratio is 1.33:1 - Full Frame. The DVD release has the original aspect ratio, and it is not anamorphic. As the source is video and not film, and because there is no widescreen aspect ratio available, the quality is at the same level of the original film. Digitally Obsessed says "The colors are nicely rendered, with a minimum of bleeding" but when viewed on "a 115 foot projection screen through a progressive scan player...the image was fairly grainy and uneven".  The film has English and French audio. Digitally Obsessed says "The DS2.0 mix is more than adequate for this fun little bat romp   lack of directionality in the mix. The dialogue is clear and center speaker weighted". It concluded by saying "This is a great DVD for kids, because besides just watching the movie they can enjoy the three sing-alongs or try to find Prince Ivan in the mazes. Bartok teaches moral values in a way that kids can understand"  According to LoveFilm, the film has been dubbed into: English, German, French, Spanish, Italian, Swedish, and Dutch. It has subtitles in:	Dutch, French, German, Italian, Spanish, and Swedish.    Fort Worth Star-Telegram implied this was one of the rare direct-to-video films that is great quality, saying "the made-for-tape bin can yield an undiscovered bargain   Bartok the Magnificent".  Lexington Herald-Leader said "to my surprise...the movie overall   quite good." 

==Critical reception==
In a review written on January 1, 2000, Dan Jardine of Apollo Guide gave the film a score of 71/100.    On December 8, 2004, Michael Dequina of TheMovieReport.com wrote a review in which he scored it 1.5/4, and wrote "This uninspired, but mercifully short, adventure will hold some amusement for little kids but bore everyone else".  FamilyVideo said the film "is marked by imaginative scenery, catchy songs, comic characters   own funny and neurotic commentary".  Hartford Courant described the film as "enjoyable".  Indianapolis Star said "Bartok is quite good for video-only release".  DigitallyObsessed gave the film a Style grade of B+, Substance rating of A, Image Transfer rating of C, Audio Transfer rating of B, and Extras rating of B+ - averaging out to a B+ rating of the film as a whole. It said "Stephen Flahertys score is very nice".  On LoveFilm, the film has a rating of 3/5 stars based on 222 member ratings. 

In a 1999 review, John Laydon of Variety explained: "Tykes will likely be charmed by the brisk pacing, vibrant (albeit stereotypical) characters and engaging storyline, while parents may be especially grateful for a cartoon with much better production values than Pokemon". He noted "even very small children will notice early on that Ludmilla...a duplicitous regent, is the real villain of the piece". He said co-directors Bluth and Goldman "do a respectable job of establishing what promises to be a new direct-to-video franchise", adding that "though certainly not as lavish as its bigscreen predecessor  , the sequel is attractive and involving, with Tim Curry and Jennifer Tilly  well cast as supporting-character voices". He said Azaria has "amusing brio", while Grammer "is the real scene-stealer this time". He described the songs as "pleasant but unremarkable". 

Also in 1999, Fort Oglethorpe Press described the film as "spectacular", "frolicking", and "fun-filled", adding that it is "loaded with breathtaking, feature-quality animation", and "spectacular music", and "enchanting new songs".   

The Trades questioned the films existence, saying "I am unsure what reason this spinoff was made, but regardless, it was a well done one". It added that "the same team directed and produced the second movie, and unlike many direct to video movies, it is animated as well as the first and uses a healthy portion of CGI, something many movies of that nature tend to lack. Backgrounds have the same detail as the original movie, making this a definite worthwhile watch". 

The Dallas Morning News notes "Bartok the Magnificent does even more disservice to Russian history than Anastasia did". 

==References==
 

==External links==
*  
*  
*   at the New York Times

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 