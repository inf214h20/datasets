Powder Blue (film)
{{Infobox film
| name           = Powder Blue
| image          = Powder blue.jpg
| caption        = Promotional film poster
| director       = Timothy Linh Bui Bobby Schwartz Tracee Stanley-Newell
| screenplay     = Timothy Linh Bui
| story          = Timothy Linh Bui Stephane Gauger
| starring       = Jessica Biel Forest Whitaker Patrick Swayze Ray Liotta Eddie Redmayne Kris Kristofferson 
| music          = Didier Rachou
| cinematography = Jonathan Sela
| editing        = Leo Trombetta Jamie Selkirk
| studio         = New Line Cinema Paramount Pictures Focus Features
| distributor    = Speakeasy Releasing
| released       =   }}
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
}}
Powder Blue is a 2008 drama film with an ensemble cast featuring several interconnected story arcs. It was written and directed by Timothy Linh Bui, and features Patrick Swayzes last film role. The film saw only limited theatrical release in the USA and was ultimately released principally on DVD in May 2009.  The film was subsequently released in Kazakhstan and Russia and on US cable television premium movie channels in late 2009.

==Synopsis==
Several Los Angeles residents meet on Christmas Eve through chance, tragedy, loss and divine intervention.
 suicidal ex-priest. Alejandro Romero plays a transvestite prostitute who shares an unexpected emotional bond with the priest. 

==Cast==
The cast includes the following:   from the Internet Movie Database 
* Jessica Biel as Rose-Johnny
* Forest Whitaker as Charlie
* Patrick Swayze as Velvet Larry
* Ray Liotta as Jack Doheny
* Lisa Kudrow as Sally
* Riki Lindhome as Nicole
* Sanaa Lathan as Diana
* Eddie Redmayne as Qwerty Doolittle
* Kris Kristofferson as Randall
* Alejandro Romero as Lexus
* Chandler Canterbury as Billy
* Jeffery A. Baker as Slim

==Reception==
According to Variety (magazine)|Variety magazine, "the heartstring-pulling contrivances of the film, set during Christmastime, go way over the top...Biel often overacts even more than her role requires."   The magazine calls director Buis "trumpeting of the power of love in the city of lonely hearts ... both ear-splittingly loud and tone-deaf at the same time" with  "Jonathan Selas color palette of nightmarish reds and blues and blinding whites, simply enforc  the pics borderline hysteria."

Rotten Tomatoes collected only eight reviews for Powder Blue with an average rating of 3.7 out of 10. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 