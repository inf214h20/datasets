The Code of Marcia Gray
{{infobox film
| name           = The Code of Marcia Gray
| image          = The Code of Marcia Gray - 1916 - newspaperad.jpg
| caption        = Newspaper advertisement.
| director       = Frank Lloyd
| producer       = Oliver Morosco 
| writer         = Elliot Clawson Frank Lloyd
| starring       = Constance Collier
| music          =
| cinematography = James Van Trees
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}} silent romantic crime drama produced by Oliver Morosco, distributed through Paramount Pictures and directed by Frank Lloyd. 

==Cast==
*Constance Collier as Marcia Gray
*Harry De Vere as Harry Gray
*Forrest Stanley as lawyer
*Frank A. Bonn as James Romaine
*Howard Davies as Ed Crane
*Helen Jerome Eddy as Cranes Daughter
*Herbert Standing as Banker Agnew

==Production background==
The film is based on a true story concerning the collapse of the Knickerbocker Bank in New York. The film starred Constance Collier in her second film role. Collier made this film during her trip to the United States with Herbert Beerbohm Tree. 

==Preservation status==
The film survives in the Library of Congress.  

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 