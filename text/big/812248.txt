All's Well, Ends Well
 
 
 
{{Infobox film
| name           = Alls Well, Ends Well
| image          = Alls Well, Ends Well poster.jpg
| caption        =
| director       = Clifton Ko Raymond Wong
| writer         = Vincent Kok Choi Ting-Ting Roman Cheung Eric Yeung
| narrator       =
| starring       = Stephen Chow Maggie Cheung Leslie Cheung
| music          = Violet Lam
| cinematography = Lee Kin-Keung
| editing        = Kam Ma Mandarin Films Ltd.
| released       = 25 January 1992
| runtime        = 100 min
| country        = Hong Kong Cantonese
| budget         =
| gross          =
}}
 Raymond Wong, Sandra Ng, and Teresa Mo.
 Lunar New cult classic by most Hong Kong audiences.

The film was followed by six sequels:
*Alls Well, Ends Well Too or Hua tian xi shi (1993)
*Alls Well, Ends Well 1997 or 97 Jia You Xi Shi (1997)
*Alls Well, Ends Well 2009 (2009)
*Alls Well, Ends Well 2010 (2010)
*Alls Well, Ends Well 2011 or Jui Keung Hei Si (2011)
*Alls Well, Ends Well 2012 (2012)

==Cast and roles==
* Stephen Chow – Foon (a womanizer DJ who enjoys flirting while on broadcast)
* Maggie Cheung – Holli-yuk (an art film-lover who dreams of finding her dream guy)
* Leslie Cheung – So (Foons effeminate elder brother who is a floral arranger)
* Teresa Mo – Mo-Seung (Sos second cousin and nemesis turned lover) Raymond Wong – Moon (Foons eldest brother who is unfaithful)
* Sandra Ng – Tai Sou Big Wife (Wife of Moon who was divorced and then remarried)
* Sheila Chan – Playing herself, Moons mistress who was once a beauty pageant winner
* Lee Heung-kam – Grandmother (the brothers mother)
* Kwan Hoi-san – Grandfather (the brothers father)
* Clifton Ko – Annoyed karaoke patron
* Vincent Kok – Japanese businessman
* Loletta Lee – Foons girl
* Wong Jim
* Wong Kwong-leung – Brother Kwong
* Andrew Yuen – Kai Ming
* Cheri Ho
* Chow Chi-Fai

 
 

==Plot details==

"Alls Well, Ends Well" is a comic romance of 3 hapless brothers, who eventually learn through their amorous exploits and misadventures that love is only won through gradual nurturing, and quickly lost through the quick, dishonest, selfish ways which they have always taken for granted.

Moon is eldest brother and head of the family.  The film begins with the celebration of his 7th anniversary wedding with Tai Sou, which celebration he deserts, preferring instead the company of his mistress (Sheila Chan).  He turns up at home with her later, forcing his wife (whom he calls hag) to leave the house in dismay.  Although she is very devoted to her husband and tolerates him for all his misbehaviors, her tolerance stops short of accepting his mistress openly in the family home.

So (Leslie Cheung) is an effeminate floral arranger and lecturer at an art school, who is good at cooking and enjoys womens hobbies.  His second cousin Mo-Seung comes to his house on that same evening, and entirely devours an elaborate gourmet banquet which So had intended as a gift for Tai-Sou on her otherwise disastrous wedding anniversary.  From that day on, So and Mo-Seung are constantly at loggerheads over trivial issues, insulting each other with vulgar metaphors during a mahjong game session, apparently irreconcilable.

Foon is a local radio DJ who flirts shamelessly while on air and is well-known among his legion of female fans for his impressive kissing technique. Holli-yuk (Maggie Cheung) calls him on air one day and arranges a date with him. She is an avid Hollywood movie lover who enjoys re-enacting particular love-scenes from movies. She is convinced that Foon shares her romantic outlook and they soon become lovers.  Foon, however, is a notorious playboy not eager to settle down.  Predictably, Holli-yuk catches him in an act of infidelity.  After a freak accident leaves Foon suffering from a mildly debilitating mental illness, Holli-yuk offers to become a nurse for him.  Taking advantage of her role as his nurturer, she gleefully devises methods to punish him for his callous behaviour.

The brothers dilemmas are later resolved in similarly titled sequel films.  Each story concludes on a cheeky and high-spirited note.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 