Nuvvostanante Nenoddantana
{{Infobox film
| name = Nuvvostanante Nenoddantana
| image = nuvvosthanante_nenoddantana.jpg
| director = Prabhu Deva
| writer = Paruchuri Brothers  (dialogues) 
| screenplay = M. S. Raju
| story    = Veeru Potla
| starring = Siddharth Narayan Trisha Krishnan Prakash Raj
| producer = M.S. Raju
| distributor = Sumanth Arts Venu Indian ISC
| music = Devi Sri Prasad
| editing = K.V. Krishna Reddy
| released = 14 January 2005
| runtime = 161 minutes
| language = Telugu
| country = India
| budget = 6cr
| box office = 15crs 
}}
 Sidharth and Trisha Krishnan. It marks the directorial debut of Prabhu Deva.  Film is on the same plot of 1989  Maine Pyar Kiya .The soundtrack was composed by Devi Sri Prasad. The film garnered eight Filmfare Awards South and has turned out to be a blockbuster.  The film was remade in Tamil as "Unakkum Enakkum" in 2006. The film was remade in Oriya as "Suna Chadhei Mo Rupa Chadhei" in 2009 Starring Anubhab Mohanty And Barsha Priyadarshani. The film was remade in Hindi as Ramaiya Vastavaiya which was released on 19 July 2013. 

==Plot==
Santosh (Siddarth Narayan) is a playful young NRI (non-resident Indian) from London, who returns to India to attend his cousin Lalitas wedding. There, he meets one of Lalitas friends, Siri (Trisha Krishnan) who comes from the countryside to attend the marriage. Though they initially clash, after a series of misadventures, they fall in love. Santosh, however, is supposed to marry the spoiled daughter of his uncles friend.

When Siri goes back home, Santosh follows her to win her over. But Siris over-protecting brother Krishna (Srihari) is less than happy. He promises Santosh to give his blessings for the wedding if Santosh manages to produce more harvest than he does. Santosh accepts the challenge and starts learning farming. However, the son of a friend of Krishna is secretly in love with Siri, and his father asks Krishna for Siris hand in marriage with his son. Krishna doesnt consent. With brooding jealousy, the wannabe-fiancé kidnaps Siri after Santosh wins the farming challenge. Santosh chases the kidnapers and kills the wannabe-fiancé. Siris brother Krishna takes the blame for the murder and goes to jail. The couple gets married when Krishna gets released from jail. The movie ends with rejoining of all family members.

==Cast== Siddharth ... Santosh
* Trisha Krishnan ... Siri
* Srihari ... Sivarama Krishna (Siris brother) Geetha ... Janaki (Santoshs mother)
* Prakash Raj ... Prakash (Santoshs dad)
* Veda Sastry...Lalitha (Siris best friend and Santoshs cousin)
* Paruchuri Gopala Krishna ... Station Master
* Tanikella Bharani ... Kantepudi Srinivasa Rao
* Dharmavarapu Subramanyam ... Udayagiri Subbarao Sunil ... Banda
* Santhoshi ... Gowri
* Nanditha ... Dolly
* Narsing Yadav  Chandra Mohan 
* Jaya Prakash Reddy
* Narra Venkateswara Rao ... Muddu Krishnaiyya
* Abhishek 
* Raghu Babu
* Pavala Shyamala
* Prabhu Deva ... Special appearance
* M.S. Raju ... Special appearance

==Crew==
* Director: Prabhu Deva
* Story: Veeru Potla
* Dialogue: Paruchuri Brothers
* Producer: M.S. Raju
* Music: Devi Sri Prasad
* Editor: K.V. Krishna Reddy
* Cinematographer: Venu Isc Sirivennela
* Vijayan
* Second assistant director: Paturi Venkata Sai Babu

==Music==
The film has seven songs composed by Devi Sri Prasad. Karthik & Sumangali Tippu
* "Ghal Ghal Ghal Ghal" — S.P. Balu|S.P. Balasubramaniam
* "Padam Kadalanantunda" — Sagar
* "Paripoke Pitta" — Mallikarjun & Sagar
* "Chandrullo Unde Kundellu" — Shankar Mahadevan
* "Adire Adire" — Jassie Gift & Kalpana

==Character map and remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
| Nuvvostanante Nenoddantana (2002) (Telugu cinema|Telugu) || Something Something... Unakkum Enakkum  (2003) (Tamil cinema|Tamil) || Neenello Naanalle (2005) (Cinema of Karnataka|Kannada) || Ramaiya Vastavaiya (2014) (Bollywood|Hindi)   || I Love You (2007 Bengali film) ||   Suna Chadhei Mo Rupa Chadhei (Oriya)   ||Nissash Amar Tumi (Bangladesh) 
|-
| Srihari || Prabhu Ganesan || Vishnuvardhan (actor) || Sonu Sood || Tapas Paul || Siddhanta Mahapatra || Misha Sawodagor 
|- Payal Sarkar || Barsha Priyadarshini|| Apu Biswas  
|- Siddharth || Dev  Anubhav Mohanty|| Shakib Khan
|}

==Release==
Nuvvostanante Nenoddantana was released with 90 prints on 14 January 2005; more prints were added later to meet the public demand. 

==Box office performance==
It was biggest blockbuster of year 2005 in tollywood . Ran for house full shows for many days at major centres. It collected a gross close to 1cr at many centres such as shanthi 70mm ,bramarambha 70mm , prasadz ,shalini etc 
The film ran for 50 days in 79 centres  and 100 days in 35 centres, becoming a huge blockbuster. 
It collected a share of 15crs at the box office.

==Awards==
;Filmfare Awards South Best Film - M.S. Raju Best Actor - Siddharth Narayan Best Actress - Trisha Krishnan Best Music Director - Devi Sri Prasad Best Supporting Actor - Srihari Best Choreography - Prabhu Deva Best Male Playback - Shankar Mahadevan Best Lyricist Sirivennela

;Nandi Awards Best Home-Viewing Feature Film - M.S. Raju Best Actress - Trisha Krishnan Best Supporting Actor - Srihari Best Comedian - Santoshini Best Art Director - Vivek

;Santosham Film Awards Best Music Director - Devi Sri Prasad

==Remakes== Tamil with the title Something Something... Unakkum Enakkum. The lead casts are Jayam Ravi and Trisha Krishnan in Tamil language|Tamil. Oriya as Suna Chadhei Mo Rupa Chadhei starring Anubhav, Barsha and Sidharth. I Love You (India) and Nissash Amar Tumi (Bangladesh) respectively.
* It was remade  into Hindi as Ramaiya Vastavaiya (2013), directed by Prabhu Deva.

==References==
 

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2005
| before=Varsham (2004 film)|Varsham
| after=Bommarillu}}
 

 
 

 
 
 
 
 
 
 
 