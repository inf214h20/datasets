Amchem Noxib
 
{{Infobox film
| name           = Amchem Noxib
| image          = Amchem Noxib.jpg
| image size     = 200px
| alt            =  monochrome Konkani film Amchem Noxib.
| director       = A.Salam
| producer       = Frank Fernand
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Alfred Rose, Philomena Braz, Lucian Dias, Leena Vaz, Master Vaz
| music          = Frank Fernand
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Konkani
| budget         = 
| gross          =
}} Konkani film released in 1963.
 National Award winning Nirmon. Amchem Noxib, which was a hit, was a trendsetter for the fledgling Konkani cinema.  

==Cast and crew==
* Director: A. Salam
* Producer: Frank Fernand
* Music: Frank Fernand Alfred Rose, Philomena Braz, Lucian Dias, Leena Vaz, Master Vaz  

==Music==
Amchem Noxib has some of Konkani cinemas most memorable and popular songs.  The producer Fernand was himself accomplished in the field of music.

{| class="wikitable" border="1"
|-
! Song
! Singer
! Picturised on
! Composer
! Lyricist
! Video
|-
| Don Kallzam (Sontos Bhogta)
| Molly
| Rita Lobo
| ?
| C. Alvares
|  
|-
| Molbailo Dou
| Molly
| C. Alvares, Rita Lobo
| ?
| C. Alvares
|  
|-
| Godacho Pav
| Anthony Mendes
| Anthony Mendes, Antonette Mendes
| ?
| M. Boyer
|  
|-
| I Lost My Heart To you
| Molly
| C. Alvares, Rita Lobo
| Alfred Rose
| Alfred Rose
|  
|-
| Io Moga
| Martha, Molly, Star of Arossim
| C. Alvares, Rita Lobo, M. Boyer
| ?
| Remmie Colaco
|  
|-
| Mando Goencho (Ratchea Ranant)
| Janet, Juliet, Molly
| Rita Lobo
| ?
| Remmie Colaco
|  
|-
| Bencdaita Pai (Tum Nasloi)
| Anthony Mendes, Antonette Mendes
| Anthony Mendes, Antonette Mendes
| ?
| C. Alvares
|  
|}

==References==
 

==External links==
*  

 
 
 
 


 