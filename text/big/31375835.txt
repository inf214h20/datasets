L.A. Bounty
{{Infobox film
| name           = L.A. Bounty
| alt            = 
| image          = L.A. Bounty FilmPoster.jpeg
| caption        = 
| director       = Worth Keeter
| producer       = Sybil Danning Michael W. Leighton
| writer         = Sybil Danning Michael W. Leighton
| starring       = Sybil Danning Wings Hauser Blackie Dammett 
| music          = Howard Leese John Sterling
| cinematography = Gary Graver
| editing        = Stewart Schill
| studio         = Adventuress Productions International
| distributor    = MGM
| released       =  
| runtime        = 85 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

L.A. Bounty is a 1989 drama film directed by Worth Keeter, starring Sybil Danning, Wings Hauser and Blackie Dammett. Ruger (Sybil Danning), an ex-cop-turned-bounty hunter goes after a crazed killer.

==Cast==
* Sybil Danning as Ruger
* Wings Hauser as Cavanaugh
* Blackie Dammett as James Maxwell
* Henry Darrow as Lt. Chandler Frank Doubleday as Rand
* Robert Hanley as Mike Rhodes
* Lenore Kasdorf as Kelly Rhodes
* Bob Minor as Martin
* Robert Quarry as Jimmy
* Van Quattro as Michaels
* Branscombe Richmond as Willis
* J. Christopher Sullivan as Mayor Burrows
* Maxine Wasa as Cavanaughs Girl

==Home Video==
L.A Bounty was originally released on VHS in 1989 by IVE Entertainment, and re=released in 1992 by Avid Home Entertainment
In 2001, MGM Home Entertainment released it as an exclusive to Aamazon.com There is yet to be a DVD release

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 


 