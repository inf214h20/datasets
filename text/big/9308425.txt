Two Solutions for One Problem
{{Infobox film
| name = Two Solutions for One Problem
| director =Abbas Kiarostami
| writer =Abbas Kiarostami
| starring =
| producer =
| released =Iran 1975 Persian
| runtime =4 min. 25 sec.
| distributor =
}} 1975 Iranian short film directed by Abbas Kiarostami.

==Film details==
During breaktime, Dara and Nader have a fierce argument about a torn exercise book that the former has given back to the latter. There are two possible outcomes, which the film shows one after the other. One is that Dara wants to get his own back, and the two boys start a violent fight; the other is that they work together to mend the exercise book with a little glue.

==See also==
*List of Iranian films

==External links==
* 
* 

 

 
 
 
 
 


 
 