The Magnificent Adventurer
{{Infobox film
 | name = The Magnificent Adventurer
 | image = The Magnificent Adventurer.jpg
 | image_size = 
 | director = Riccardo Freda
 | writer = Filippo Sanjust  
 | story =  
 | starring =  Brett Halsey
 | music =Francesco De Masi
 | cinematography = Raffaele Masciocchi
 | editing = 
 | producer = 
 | distributor =
 | released = 1963 Italian 
 }} 1963 Italian-French-Spanish adventure film directed by Riccardo Freda. It is loosely based on real life events of Benvenuto Cellini.      

== Cast ==

* Brett Halsey: Benvenuto Cellini
* Françoise Fabian: Lucrezia
* Claudia Mori: Piera
* Bernard Blier: Papa Clemente VII
* Rossella Como: Angela
* Félix Dafauce: conte Frangipani
* José Nieto (actor)|José Nieto: Connestabile di Borbone
* Jacinto San Emeterio: Francisco I
* Elio Pandolfi: attore
* Andrea Bosic: Michelangelo
* Diego Michelotti: Carlo V
* Umberto DOrsi: granduca di Toscana
* Carla Calò: zia di Angela
* Sandro Dori: zio di Angela
* Giampiero Littera: Francesco
* Dany París: moglie di Francesco

== Reception ==
Richard Roud referred to the film as "a constant pleasure to the eye" and "a glittering riot of delicate colour shadings in both sets and costumes".  

==References==
 

==External links==
* 
 
    
 
  
  
   
  
  
  
   
  
 
 
 
 
 


 
 