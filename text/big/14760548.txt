Trio (film)
 
 
{{Infobox film
| name           = Trio
| image          = Poster_for_"Trio"_(1950).jpg
| caption        = Australian daybill
| director       = Ken Annakin Harold French
| producer       = Antony Darnborough
| writer         = W. Somerset Maugham (stories and screenplay) Noel Langley R. C. Sherriff James Hayter Kathleen Harrison Nigel Patrick Wilfred Hyde-White Jean Simmons Michael Rennie Roland Culver
| music          =
| cinematography = Geoffrey Unsworth Reginald H. Wyer
| editing        = Alfred Roome
| studio         = Gainsborough Pictures Rank Organisation
| distributor    = General Film Distributors (UK) Paramount Pictures (US)
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Trio (also known as W. Somerset Maughams Trio) is a 1950 British anthology film based on three short stories by   directed "The Verger" and "Mr. Know-All", while Harold French was responsible for "Sanatorium".

Trio is the second of a film trilogy, all consisting of adaptations of Maughams stories, preceded by the 1948 Quartet (1948 film)|Quartet and followed by the 1951 Encore (1951 film)|Encore.

The film was nominated for the Academy Award for Best Sound, Recording (Cyril Crowhurst).   

==Plot==
===The Verger=== James Hayter), is illiterate. When Foreman refuses to learn to read, the vicar feels he has no choice but to fire him.

On the way back to his lodgings, Foreman notices that there is not a tobacconist shop in the area. Needing work, he decides to open one. He also takes the opportunity to propose to his landlady, Emma (Kathleen Harrison). Their fledgling business is very successful, and Foreman soon sets up another shop, run by his stepdaughter and her husband. Over the next decade, Foreman starts up more and more shops, becoming a wealthy man in the process and depositing his profits at the bank.

The bank manager (Felix Aylmer) recommends that he invest his sizeable savings to get a better return on his money, causing Foreman to reveal that he has not been able to because he could not read the necessary papers. The stunned manager exclaims (rhetorically) what would you be if you could read?; Foreman replies that he would be the verger of St. Peters Church.

===Mr. Know-All===
Reserved Mr. Gray (Wilfred Hyde-White) finds himself forced to share a cabin on an ocean liner with the loud, opinionated, supremely self-confident gem dealer Max Kelada (Nigel Patrick). Kelada soon dominates all the onboard social gatherings, much to the annoyance of his fellow passengers, who take to calling him "Mr. Know-All" behind his back because of his insistence that he is an expert on all subjects.

One night, he remarks on the fine quality of the pearl necklace worn by the pretty Mrs. Ramsay (Anne Crawford), who has rejoined her husband (Naunton Wayne) after a two-year separation caused by his work. Mr. Ramsay bets him that the pearls are fake; Kelada swiftly accepts the wager, despite Mrs. Ramsays attempt to call it off. While examining the pearls, Kelada observes that the woman is very uneasy. He then admits that he was wrong and pays Mr. Ramsay.

Afterwards, back in their cabin, Gray and Kelada are surprised when a banknote is slipped under their door. Gray gets Kelada to tell the truth: the pearls are real and very costly. Kelada adds that he would not have left such an attractive wife alone for that long. Gray begins to warm to his cabinmate.

===Sanatorium===
Writer Mr. Ashenden (Roland Culver) is sent to a sanatorium for his health. While there, he becomes acquainted with the lives and dramas of the residents. Another newcomer is the scandalous Major George Templeton (Michael Rennie), who admires lovely Evie Bishop (Jean Simmons). Evie has spent years in one sanatorium after another. Ashenden also observes the ongoing feud between longtime patients Mr. Campbell (John Laurie) and Mr. McLeod (Finlay Currie), who delight in making each others lives miserable. Finally, Mr. Chester (Raymond Huntley) resents the visits of his loving wife (Betty Ann Davies) because he envies her robust good health.

Tragedy strikes when McLeod dies, depriving Campbell of his enjoyment of life. Meanwhile, George and Evie fall in love; however, doctors warn them that George will hasten his death if they marry and try to enjoy a normal life. Despite the warning, the lovers decide that happiness, no matter how brief, is worth the price and leave the sanatorium. Their example eases Mr. Chesters bitterness with his own fate and strengthens his love for his wife.

==Cast==
===The Verger=== James Hayter as Albert Foreman
*Kathleen Harrison as Emma Foreman (née Brown)
*Michael Hordern as the vicar
*Felix Aylmer as the bank manager
*Lana Morris as Gladys, Emmas daughter
*Glyn Houston as Ted, Gladys husband

===Mr. Know-All===
*Nigel Patrick as Max Kelada
*Wilfred Hyde-White as Mr. Gray
*Anne Crawford as Mrs. Ramsay
*Naunton Wayne as Mr. Ramsay
*Clive Morton as the ships captain
*Bill Travers as Fellowes (credited as Bill Linden-Travers)

===Sanatorium===
*Michael Rennie as Major George Templeton
*Jean Simmons as Evie Bishop
*Roland Culver as Mr. Ashenden
*André Morell as Dr. Lennox
*John Laurie as Mr. Campbell
*Finlay Currie as Mr. McLeod
*Raymond Huntley as Mr. Chester
*Betty Ann Davies as Mrs. Chester

==Critical reception==
*In The New York Times Bosley Crowther noted, "another delightful screen potpourri, made from short stories of W. Somerset Maugham...Wonderfully rich...Shot through with keen, ironic humor" 
* TV Guide noted, "a small and highly enjoyable film."  

== References ==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 