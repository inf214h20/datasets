Halleluja for Django
{{Infobox film
| name = Halleluja for Django
| image = Halleluja for Django.jpg
| caption =
| director = Maurizio Lucidi
| writer =
| music = Luis Enríquez Bacalov
| cinematography = 
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 1967 Cinema Italian Spaghetti Western film directed by Maurizio Lucidi.

It was referred as a film with a great story and good tension but weak in its giallo part.   

== Cast == George Hilton: Billy Rhum Cooney 
* Jack Betts: David Phaylard  Walter Barnes: Jarrett/Clay Thomas 
* Sonia Romanoff: Mara
* Erika Blanc: Jenny
* Mario Brega: Andreas/Yanaro
* Jeff Cameron: Mark

== Plot ==
A man dressed as a monk accompanied by a woman arrives to Middletown with a statue of St Absalom on a wagon. Jarret and his gang set fire to a hay wagon in the street and during the confusion they rob the bank, and place the gold coins inside the statue, which was parked by the window of the bank office. Then they ride off and disperse to avoid the pursuing posse.
 
The monk and his companion proceed with the statue to the insignificant town of Poorlands. Later the gang reconvenes there. When the sheriff tries to telegraph his suspicions he is shot, and Jarret takes control of the town and puts a guard with the telegraph operator to make sure that all incoming messages are acknowledged.

The man in monk clothes, who is called ”the priest” (Il Santo) objects that killings were not part of the agreement. Jarret’s plan, however, is to await an Indian guide who knows the way through the desert, and then kill everybody and burn the town to get rid of witnesses. However the guide does not arrive – because he was killed earlier in a fight with the Priest who had tried to pay him not to come. The Priest secretly continues a liaison with Jarret’s woman Mara (the woman on the wagon) and plans to take the gold. He thus supports Jarret against gang members who want it to be divided at once.

Billy Rhum, the brother of the sheriff, is locked up in jail but slips out, first to bury the body of his brother, and next to save his friend Mark, who has been caught trying to go for help. When the furious Jarret has all the citizens dragged out into the street and starts shooting people the telegraph operator attempts to send an alarm and is shot. ”The priest” (who no longer dress as one) has become aware of Billy’s outings and offers him money to kill Jarret. When the gang check the statue they find stones instead of gold.

Now the posse approaches to investigate why the town telegraph has ceased acknowledging. All men are locked in and women, children and gang members appear as attending the funeral of the telegraph operator. Suddenly Billy Rhum appears as the sheriff, and suggests that the men of the town – that is Jarret and the gang – join the posse. When they have left, he offers the Priest to leave with Mara, but the two start a shoot-out over the money. They are joined first by Jarret and then by his men, who have turned back covered by the dust of the posse. Billy offers part of the gold to Jarret or the Priest if they kill the other. The rest of the gang perish in the fight and after Priest has saved Billy they join forces, while Mara and Billy’s girl friend Jenny fight over a gun in the saloon. Jarred barricades himself in the sheriff office and finds the gold hidden in boxes with dynamite. He starts throwing dynamite at his foes, but the whole office explodes and sends the gold raining over the citizens.

==References==
 

==External links==
* 

 
 
 
 
 
 
 