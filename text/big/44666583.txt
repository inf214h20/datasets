Along the Mediterranean
{{infobox film name = Along the Mediterranean image =  caption =  director = Sidney Olcott producer = Kalem Company writer =  starring =  distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 349 ft
| country = United States language = Silent film (English intertitles) 
}}
Along the Mediterranean is a 1912 American silent documentary produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott.

==Production notes==
The documentary was shot in December 1911, in Algiers, in Italy, In Genoa, in Naples.

==References==
* The Moving Picture World, Vol 12, p 202 and 358.
* New York Dramatic Mirror, 8 mai 1912, p 29.
* Supplement to the Bioscope, June 27, 1912, p XVIII.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 

 