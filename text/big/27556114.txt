Hold That Line
 

{{Infobox film
| name           = Hold That Line
| image          =
| image_size     =
| caption        =
| director       = William Beaudine Jerry Thomas Tim Ryan Charles R. Marion
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Hold That Line is a 1952 comedy film starring The Bowery Boys. The film was released on March 23, 1952 by Monogram Pictures and is the twenty-fifth film in the series.

==Plot==
The members of the local universitys trust make a wager that anyone can make it through college if just given the chance.  Because of this, they enlist the boys to attend the university to prove the theory.  While the boys do not become academic scholars, Sach invents a "vitamin" drink that makes him invincible.  They all join the football team and Sach becomes the star player, leading them to the big championship game.  A local gambler sees an opportunity to make some money by kidnapping Sach and preventing him from playing.  However, Slip and the rest of the gang rescue Sach and return him to the game.  Unfortunately he does not have any more "vitamins", so Slip plans a ruse on the playing field that distracts the other team and allows him to score the winning touchdown.  In the end, Sach cannot reproduce his "vitamin" formula, but he does produce a new concoction that allows him to fly!

==Production==
This is the first appearance of Gil Stratton, Jr. as a member of the gang, having taken over for William Benedict. Stratton was unhappy with idea of being a Bowery Boy, and as a result he  tried to keep himself as inconspicuous in the films as possible; he often gave his dialogue to Leo Gorcey or Huntz Hall. 

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck (Credited as David Condon)
*Bennie Bartlett as Butch (Credited as David Bartlett)
*Gil Stratton, Jr. as Junior

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski & Morris Dumbrowski
*John Bromfield as Biff Wallace
*Taylor Holmes as Dean Forrester
*Veda Ann Borg as Candy Callin

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Crazy Over Horses 1951
| after=Here Come the Marines 1952}}
 

 
 

 
 
 
 
 
 