A Pizza Tweety Pie
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = A Pizza Tweety-Pie
| series = Merrie Melodies (Sylvester (Looney Tunes)|Sylvester/Tweety/Granny (Looney Tunes)|Granny)
| image          = 
| caption        = 
| director       = Friz Freleng
| producer       = 
| writer         = Warren Foster
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mel Blanc Daws Butler (uncredited) June Foray (uncredited)
| music          = Milt Franklyn
| cinematography = 
| editing        = Treg Brown
| studio         = 
| distributor    = 
| released       =  
| runtime        = 6 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Sylvester and Granny (Looney Tunes)|Granny, with Mel Blanc providing the voices of   Sylvester (speaking in an Italian Accent (dialect)|accent) and Tweety, and June Foray (uncredited) providing Grannys voice.

== Synopsis == barber shops down there (because of the many red and white striped barber poles).

As Tweety is singing “Santa Lucia” and strumming his mandolin in his cage, Sylvester spies him from his balcony across the canal. In haste, he runs out of the hotel with an open sandwich roll, and falls into the water. He climbs out and finds a canoe and starts rowing, but forgets to loosen it from the rope. After he cuts the rope, he sinks with the canoe.

Sylvester then starts paddling in a rubber raft, but Tweety takes a slingshot and punctures it. The raft floats back to the dock with Sylvester as the air leaks out, and Sylvester removes the deflated raft from his hind quarters in disgust.

Next, Sylvester tries to swing across the canal with a rope Tarzan-style, but lands in the water into the gaping mouth of a hungry shark. Sylvester wrestles his way out and swims hurriedly away.

Then, using an electric fan and a balloon tied to his waist, Sylvester attempts to float his way across through the air, but he floats too high. Tweety, again using the slingshot, shoots Sylvester down from the sky. Sylvester dons a bathing cap as he is descending, but misses the water, landing on the sidewalk, next to Tweety and Granny’s hotel as it turns out. He runs into the hotel and takes the elevator up to the floor of Granny and Tweetys room, but Granny and Tweety are leaving, so Sylvester goes back down the elevator, which takes him...into the water!
 lowla bridgeada”!

Finally, as Sylvester is dining on a plate of spaghetti, he again hears Tweety singing “Santa Lucia” (he is out of his cage this time), and proceeds to hurl a strand of spaghetti like a lasso to catch Tweety. Nearly strangled, Tweety screams to Granny for help. Granny clutches Sylvesters noose of pasta and substitutes a mallet in Tweety’s place. As Sylvester sucks the spaghetti into his mouth, he gets clobbered squarely in the head with the mallet, causing birds to appear uttering Tweety’s trademark line: “I tawt I taw a puddytat!” (Ironically in this cartoon, Tweety never uses this line himself.)

== External links ==
* 

 
 
 
 
 
 