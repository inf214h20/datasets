Perils of the Yukon
 
{{Infobox film
| name           = Perils of the Yukon
| image	         = Perils of the Yukon FilmPoster.jpeg
| caption        = Film poster
| director       = Jay Marchant J. P. McGowan Perry N. Vekroff
| producer       =  George Morgan George H. Plympton William Desmond Laura La Plante
| cinematography = 
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 15 episodes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 Northern film serial directed by Jay Marchant, J. P. McGowan and Perry N. Vekroff. This serial is presumably lost film|lost. 

==Cast== William Desmond as Jack Merrill Sr. / Jack Merrill Jr.
* Laura La Plante as Olga
* Fred R. Stanton as Ivan Petroff (as Fred Stanton) Joseph McDermott as Hogan (as Joe McDermott) George A. Williams as Scott McPherson
* Mack V. Wright as Lew Scully
* Fred Kohler as Captain Whipple
* Neola May as Neewah (as Princess Neela)
* Chief Harris as Numa
* Joseph W. Girard (as Joseph Girard)
* Ruth Royce
* Clark Comstock

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 