Campane a martello
 
{{Infobox film
| name           = Campane a martello
| image          = Campane a martello.jpg
| caption        = Film poster
| director       = Luigi Zampa
| producer       = Carlo Ponti
| writer         = Paolo Heusch Piero Tellini
| starring       = Gina Lollobrigida
| music          = Nino Rota
| cinematography = Carlo Montuori
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Campane a martello is a 1949 Italian drama film directed by Luigi Zampa and starring Gina Lollobrigida.   

==Cast==
* Gina Lollobrigida - Agostina
* Yvonne Sanson - Australia
* Eduardo De Filippo - Don Andrea
* Carlo Giustini - Marco
* Carlo Romano - Gendarme
* Clelia Matania - Bianca
* Agostino Salvietti - Mayor
* Ernesto Almirante - Landowner
* Luigi Saltamerenda - Butcher
* Salvatore Arcidiacono - The Pharmacist
* Ada Colangeli - Francesca Carlo Pisacane - Filippo the altar boy
* Francesco Santoro - Franco
* Vittoria Febbi - Connie
* Pasquale Misiano - Chauffeur

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 