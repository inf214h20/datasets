A Man for All Seasons (1966 film)
 
 
 
{{Infobox film
| name = A Man for All Seasons
| image = A Man for All Seasons (1966 movie poster).gif
| alt = 
| caption = Original film poster by Howard Terpning
| director = Fred Zinnemann
| producer = Fred Zinnemann
| writer = Robert Bolt
| starring = {{Plainlist| 
* Paul Scofield
* Wendy Hiller
* Leo McKern
* Orson Welles Robert Shaw
* Susannah York}}
| music = Georges Delerue
| cinematography = Ted Moore
| editing = Ralph Kemplen
| studio = Highland Films
| distributor = Columbia Pictures
| released =   
| runtime = 120 minutes  
| country = United Kingdom
| language = English
| budget = $2 million
| gross = $28.4 million 
}} biographical drama the same Best Picture, Best Director, Best Actor.
 top 100 Vatican listed greatest movies of all time. 

==Title==
The title reflects playwright Bolts portrayal of More as the ultimate man of conscience and as remaining true to his principles and religion under all circumstances and at all times. Bolt borrowed the title from Robert Whittington, a contemporary of More, who in 1520 wrote of him:
 

==Premise==
Sir Thomas More was the 16th-century Lord Chancellor of England who refused to sign a letter asking Pope Clement VII to annul King Henry VIII of Englands marriage to Catherine of Aragon and resigned rather than take an Oath of Supremacy declaring Henry VIII Supreme Head of the Church of England. Both the play and the film portray More as a tragic hero, motivated by his devout Roman Catholic faith and envied by rivals, such as Thomas Cromwell. He is also deeply loved by his family and respected by the common people. The films story is set between 1529 and 1535, at the high point of the reign of Henry VIII of England.

==Plot==
Cardinal Wolsey, the Lord Chancellor of England, summons Sir Thomas More to Hampton Court. King Henry VIII of England wishes to divorce his wife and marry Anne Boleyn. Saying that England needs a male heir to prevent another civil war, Wolsey chastises More for being the only member of the Privy Council to argue against him. When More states that the Pope will never grant a divorce, he is scandalised by Wolseys suggestion that they apply "pressure" on Church lands to force the issue. To Wolseys fury, More responds, "No, Your Grace. I am not going to help you."
 Richard Rich, a young acquaintance from Cambridge waiting by the dock for his return. An ambitious young man, who is drawn to the allure of political power, Rich pleads with More for a position at Court. More, citing the many corruptions there, advises Richard to become a teacher instead.
 Meg with William Roper, who announces his desire to marry her. The devoutly Catholic More states that he respects Roper but that his answer will remain, "No," as long as he is a Lutheranism|Lutheran.

Soon after, Wolsey dies, banished from Court for failing to extort a divorce from the Vatican. King Henry appoints More as Lord Chancellor of England.

Soon after, the King makes an "impromptu" visit to the More estate to inquire about his divorce. Sir Thomas, not wishing to admit that his conscience forbids him to use unethical means to get the results the King demands, remains unmoved as Henry alternates between threats, tantrums, and promises of unbounded Royal favour. More finally refers to Catherine of Aragon as "the Queen," and the King screams that those who call her that are liars and traitors. Enraged, King Henry returns to his barge and orders the oarsmen to cast off. At the embankment, Rich is asked by Thomas Cromwell, whether he has information that could damage Mores reputation. In exchange, Cromwell promises him a position at Court.

Roper, learning of Mores quarrel with the King, reveals that his religious opinions have altered considerably. He declares that by attacking the Roman Catholic Church, the King has become "the Devils minister." A horrified More admonishes him to be more guarded as Rich arrives, pleading again for a position at Court. When More again refuses, Rich denounces Mores steward as a spy for Cromwell. An unmoved More responds, "Of course, thats one of my servants."

Humiliated, Rich joins Cromwell in attempting to bring down More. Meanwhile, the King tires of Papal refusals and and has Parliament declares him "Supreme Head of the Church in England." He demands that both the bishops and Parliament renounce all allegiance to the Pope. More quietly resigns as Lord Chancellor rather than accept the new order. As he does so, his close friend, Thomas Howard, 3rd Duke of Norfolk, attempts to draw his opinions out as part of a friendly chat with no witnesses present. More, however, knows that the time for speaking openly of such matters is over.

Later, Cromwell repeats this conversation to the Duke and suggests that if More attends the Kings wedding to Anne Boleyn, he will have no further problems. When More declines to appear, he is summoned again to Wolseys office at Hampton Court, now occupied by Cromwell. More is interrogated on his opinions but refuses to answer, citing it as his right under English Law. Infuriated, Cromwell declares that the King views him as a traitor, but allows him to return home.
 new oath about the marriage is being circulated and that all must take it on pain of high treason. Initially, More says he would be willing to take the oath, provided it refers only to the Kings marriage to Anne Boleyn. Upon learning that it also names the King as Supreme Head of the Church, More refuses to take it and is imprisoned in the Tower of London.

In spite of the bullying tactics of Cromwell, the subtle manipulation of Archbishop Thomas Cranmer and the pleadings of both the Duke of Norfolk and his family, More remains steadfast in his refusal to take the Oath. He also refuses to explain his objections, knowing that he cannot be convicted if he refuses to explain. A request for new books to read backfires, resulting in confiscation of the books he has, and Richard Rich removes them from Mores cell. 
 perjured testimony of Rich, who has been made Attorney General for Wales as a reward.

Now having nothing left to lose, More denounces the Kings actions as illegal. As grounds, he cites the Biblical basis for the authority of the Papacy over Christendom. He further declares that the Churchs immunity to State interference is guaranteed both in Magna Carta and in the Kings own Coronation Oath. As the audience screams in protest, More is condemned to death by beheading. Before his execution on Tower Hill, More pardons the executioner, and says, "I die  His Majestys good servant, but Gods first."

A narrator (actually Colin Blakely who played the part of Mores servant Matthew), intones the epilogue:
 Archbishop was Richard Rich Chancellor of England and died in his bed."

==Cast==
 
  Sir Thomas More
* Wendy Hiller as Alice More
* Leo McKern as Thomas Cromwell Cardinal Wolsey Robert Shaw Henry VIII
  Margaret More The Duke of Norfolk Richard Rich William Roper (the Younger)
* Colin Blakely as Matthew
 
* Cyril Luckham as Archbishop Thomas Cranmer
* Vanessa Redgrave as Anne Boleyn
* Jack Gwillim as Chief Justice
* Michael Latimer as Norfolks Aide
* Thomas Heathcote as Boatman
 
* Yootha Joyce as Averil Machin Anthony Nicholls as Kings Representative John Nettleton as Jailer
* Eira Heath as Matthews Wife
* Molly Urquhart as Maid
 
* Paul Hardwick as Courtier
* Philip Brack as Captain of the Guard
* Martin Boddey as Governor of Tower
* Eric Mason as Executioner Matt Zimmerman as Messenger
 

==Adaptation==
Robert Bolt adapted the screenplay himself. The running commentary of The Common Man was deleted and the character was divided into the roles of the Thames boatman, Mores steward, an innkeeper, the jailer from the Tower, the jury foreman and the executioner. The subplot involving the imperial ambassador, Eustace Chapuys, was also excised. A few minor scenes were added to the play, for instance Wolseys death, Mores investiture as Chancellor, and the Kings wedding to Anne Boleyn, to cover narrative gaps left by the exclusion of the Common Man.
 the Duke of Norfolk was the judge both historically and in the plays depiction of the trial, the character of the Chief Justice (Jack Gwillim) was created for the film. Norfolk is still present, but plays little role in the proceedings.

==Production==
The producers initially feared that Scofield was not a big enough name to draw in audiences, so the producers approached Richard Burton, who turned down the part. Laurence Olivier was also considered, but director Zinnemann demanded that Scofield be cast. He played More both in Londons West End and on Broadway; the latter appearance led to a Tony Award.

Alec Guinness was the studios first choice to play Cardinal Wolsey, and Peter OToole was the first choice to play Henry VIII. Richard Harris was also considered. Bolt wanted film director John Huston to play Norfolk, but he refused. Vanessa Redgrave was originally to have played Margaret, but she had a theatre commitment. She agreed to a cameo as Anne Boleyn on the condition that she not be billed in the part or mentioned in the previews.

To keep the budget at under $2 million, the actors all took salary cuts. Only Scofield, York, and Welles were paid salaries exceeding £10,000. For playing Rich, his first major film role, John Hurt was paid £3,000. Vanessa Redgrave appeared simply for the fun of it and refused to accept any money.
 1988 remake.

==Reception== fifth highest grossing film of 1966.
 top 100 British films.

===Accolades=== Best Actor Best Adapted Best Cinematography, Best Costume Best Director, Best Picture. Best Supporting Best Supporting Actress for Hiller. The film also helped launch the career of the then-unknown Hurt. The films win for Best Picture Oscar defeated another heavy nominee Whos Afraid of Virginia Woolf? (film)|Whos Afraid of Virginia Woolf? which earned 13 nominations, almost twice the winning pictures total nominations alone. For Zinnemann, he won a second time as Best Director and earned a second Best Picture award after his winning both awards for the first time for From Here to Eternity.
 Best Film Best British Best Photography Best Production Best Actor (Scofield). The film was also entered into the 5th Moscow International Film Festival where Scofield won the award for Best Actor.   

==See also== A Man for All Seasons&nbsp;– television film (1988)
* Anne Boleyn in popular culture
* Cultural depictions of Henry VIII of England
* Trial movies

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 