Perfumed Ball
{{Infobox film
| name           = Perfumed Ball
| image          = Perfumed Ball.jpg
| caption        = Theatrical release poster
| director       = Lírio Ferreira Paulo Caldas
| producer       = Aniceto Ferreira Beto Monteiro
| writer         = Hilton Lacerda Lírio Ferreira Paulo Caldas
| starring       = Duda Mamberti Luiz Carlos Vasconcelos Aramis Trindade Chico Diaz
| music          = Paulo Rafael
| cinematography = Paulo Jacinto dos Reis
| editing        = Vânia Debs
| studio         = Saci Filmes
| distributor    = RioFilme 
| released       =  
| runtime        = 93 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$326,879 
}} Benjamin Abrahão befriended Lampião, the leader of Cangaço, and filmed his crimes—a feat Brazilian army could not do.  It used footages took in 1936 for Abrahãos 1959 film Lampião, o Rei do Cangaço.    

==Cast==
*Duda Mamberti as Benjamin Abrahão
*Luiz Carlos Vasconcelos as Lampião
*Aramis Trindade as lieutenant Lindalvo Rosas
*Chico Diaz as colonel Zé de Zito
*Jofre Soares as Padre Cícero
*Cláudio Mamberti as colonel João Libório
*Germano Haiut as Ademar Albuquerque Maria Bonita

==Reception==
It entered the 1996 Festival de Brasília, where it won the Best Film, Best Newcomer Director, Best Art Direction (Adão Pinheiro), Best Supporting Actor (Aramis Trindade) awards, and shared the Critics Choice Award with Um Céu de Estrelas.  It was released on Brazilian theatres on July 26, 1997.  It was shown at the 1997 Cannes Film Festival, at the 1997 Toronto International Film Festival, at the 1998 Chicago Latino Film Festival,  and won the Coral Prize for Bestil Film Poster at the 19th Havana Film Festival.  and is considered an important mark in Pernambucos cinema industry, being one of the Pernambucos first films to be acclaimed by international critics. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 