Carny (2009 film)
{{Infobox television film
| name = Carny
| image = Carny-2009-DVD.jpg
| caption = US DVD cover.
| director = Sheldon Wilson
| producer = 
| writer = Douglas G. Davis
| starring = Lou Diamond Phillips Alan C. Peterson
| music = 
| cinematography = Danny Nowak
| editing =  Horror
| RHI Entertainment Muse Entertainment
| network = Syfy
| released =  
| runtime = 88 minutes
| country = Canada
| language = English
}} Canadian television television horror film by Syfy and the 21st film in the Maneater Series. The film was directed by Sheldon Wilson and stars Lou Diamond Phillips. 

== Plot ==
The legendary Jersey Devil appears in quick flash glimpses in the very beginning of the film as it growls. After Cap (Alan C. Peterson) kills the man who sold him the Jersey Devil, Cap and his assistant attempted to bring the Jersey Devil into the carnival of the truck. Cap shoots him with a tranquilizer dart to calm the Jersey Devil down. However, as Caps assistant heaves on the Jersey Devil in chains, it slashes at his eye, causing it to bleed. Cap eventually shoots the Jersey Devil with more tranquilizer darts which finally knocks out the Jersey Devil.

When the sheriff Atlas (Lou Diamond Phillips) comes to investigate Caps carnival under the local pastors requests, Cap takes him into the tent where the Jersey Devil is being kept for the opening show. The Jersey Devil growls at Atlas from inside his cage and Atlas, shocked at the Jersey Devils hellish appearance, suggests that the Jersey Devil is not safe. During the opening show, the audience is shocked at the sight of the Jersey Devil. All except for the pastors son Taylor (Matt Murray) and his friend who throws peanuts at the Jersey Devil from the audience. This angers the creature and the Jersey Devil eventually escapes from his cage and begins terrorizing the carnival. He throws a man inside the cage that he had been locked in and escapes the carnival right before flying into the Ferris wheel and injuring himself.

Later the following night, the Jersey Devil finds Taylor and his friend hiding in an old barn in the forest when he hears a dog barking at him. The Jersey Devil goes after the dog as Taylor and his friend escape the barn. However, the Jersey Devil eventually finds Taylor hiding under a bridge as his friend hides in an old car. The Jersey Devil lands on the bridge and then looks under it to find Taylor, who he kills and dismembers. Later when Taylors friend, who survived the incident, and his mother are driving down a road in the forest, the Jersey Devil attacks him through the car window and flies off with him. As the mother is gazing at the skies trying to find him, the Jersey Devil drops his dead body on the car right before killing the mother.

Cap and his assistant later try to recapture the Jersey Devil by using tranquilizer darts. They stab Atlas deputy to attract the creature with the smell of his blood. However, as the Jersey Devil approaches, the deputy shoots at the beast right before dying, which causes the Jersey Devil to retreat. The Jersey Devil then sets his sights on Caps carnival once more and kills several of his carnies. Cap later tries once again to recapture the beast by stabbing his own assistant to death to attract the Jersey Devil with his blood. The Jersey Devil soon arrives and sneaks up from behind Cap. Cap then shoots the Jersey Devil with his new tranquilizer darts that "have enough drugs in them to put an elephant to sleep for a week", which knocks out the Jersey Devil once more.

When Cap is arrested for murder after Atlas discovers part of a tranquilizer dart in his deputys back, the Jersey Devil awakens from the tranquilizer darts and arrives at the police station where he kills Cap. The pastor, who wants revenge on the devil and the carnival for the death of his son, stabs the Jersey Devil in his shoulders with two tranquilizer darts. This eventually knocks out the Jersey Devil. However, when the pastor and his followers arrive at the carnival, the Jersey Devil awakens right before the pastors eyes. The Jersey Devil growls at the pastor and then kills him.

When Atlas arrives at the now burning carnival, he attempts to rescue one of Caps carnies, Samara (Simone-Élise Girard), and the Jersey Devil arrives to kill them as he lands on one of the RVs. The Jersey Devil chases Samara to the Ferris wheel where the "Gentle Giant" attempts to stop him. However, the Jersey Devil attacks and kills him. Atlas eventually rams the beast into the Ferris wheel with a car in attempts to kill him. As the Jersey Devil is rammed into the Ferris wheel by Atlas, he growls at him from the front car window until the Ferris wheel eventually collapses on both of them. This finally kills both the Jersey Devil and Atlas. Before the Jersey Devil dies, its hand is seen collapsing.

== Cast ==
*Lou Diamond Phillips as Atlas
*Simone-Élise Girard as Samara
*Alan C. Peterson as Cap
*Vlasta Vrána as Pastor Owen
*Matt Murray as Taylor
*Rick Genest as Carny

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 