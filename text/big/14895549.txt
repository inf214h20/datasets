Dark Horse (1992 film)
{{Infobox Film
| name           = Dark Horse
| image          = DarkHorse92.jpg
| image_size     =
| caption        = 
| director       = David Hemmings
| producer       = Allan Glaser
| writer         = Janet McClean Donovan Leitch  
| music          = Roger Bellon
| cinematography = Steve Yaconelli
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 
| country        =  
| language       = English
| budget         = 
}} American drama film directed by David Hemmings. The screenplay by Janet Maclean was adapted from an original story by Tab Hunter.

==Plot==
The plot focuses on new-girl-in-town Allison Mills, a teenager who recently lost her mother. When she hangs out with the wrong crowd, she gets into trouble and is sentenced to community service at a local stable. There she comes to love spending time with the animals until an automobile accident cripples her and her favorite horse Jet. The wheelchair-bound girl learns to overcome her handicap through the indomitable spirit of the Working animal#Riding animals or mounts|steed, who overcomes the odds and runs again.

==Principal cast==
*Ed Begley, Jr. ..... Jack Mills
*Mimi Rogers ..... Dr. Susan Hadley
*Ari Meyers ..... Allison Mills Donovan Leitch ..... J.B. Hadley
*Samantha Eggar ..... Mrs. Curtis
*Natasha Gregson Wagner ..... Martha
*Tab Hunter ..... Perkins
*Tisha Sterling ..... Officer Ross

==Production notes== life partner Allan Glaser. 
   double in The Black Stallion. After breaking his leg, the animal spent a full year recuperating in a sling. Although he never walked properly again, when set loose he could run with no problem.  

Director Hemmings insisted on shooting in his home base of Sun Valley, Idaho, where he was surrounded by an entourage offering strong support. He drank heavily during filming, and often was barely functional at the end of the day. Producer Allan Glaser tolerated his erratic and boorish behavior only because the dailies were so good. 

The film was shown at the 1992 Cannes Film Festival and received positive reviews. It was released theatrically in July 1992. 

==References==
 

==External links==
*  

 
 
 