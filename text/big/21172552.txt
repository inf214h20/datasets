Zouzou (film)
{{Infobox film
| name           = Zouzou
| image          = 1934 Zouzou.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = Marc Allégret
| producer       = Arys Nissotti
| writer         = Carlo Rim
| narrator       =
| starring       = Jean Gabin Josephine Baker
| music          = Vincent Scotto Georges Van Parys Alain Romans
| cinematography =
| editing        = Denise Batcheff
| distributor    = Corona Films
| released       =  
| runtime        = 85 min.
| country        = France French
| budget         =
| gross          =
}}

Zouzou is a French film by Marc Allégret released in 1934.   

==Plot==
As children, Zouzou and Jean are paired in a traveling circus as twins: shes dark, hes light. After theyve grown, he treats her as if she were his sister, but shes in love with him. In Paris, hes a music hall electrician, shes a laundress who delivers clean underwear to the hall. She introduces him to Claire, her friend at work, and the couple fall in love. Jean conspires to get the shows star out of town and for the theater manager to see the high-spirited Zouzou perform. When Jeans accused of murder and Zouzou needs money to mount his defense, she pleads to go on stage. Her talents may save the show, but can anything save her dream of life with Jean?

==Cast==
# Jean Gabin: Jean, an orphan
# Josephine Baker: Zouzou, the orphan mulatto
# Pierre Larquey: Father Mélé, fairground
# Claire Gerard: Ms Valley, the laundress
# Yvette Lebon: Clare Valley, the daughter
# Illa Meery: Miss Barbara, the star of music hall
# Madeleine Guitty: Josette, clothing
# Marcel Vallée: Mr Horn, director of music-hall
# Teddy Michaud: Julot
# Pierre Palau: Mr. de Saint-Lévy, the protector
# Serge Grave: Jean, Child
# Floyd Smith: The ballet master of the journal
# Viviane Romance: The girl attablée
# Philippe Richard: The Police Commissioner
# Robert Seller: The Sponsor
# Roger Blin: A witness to the murder
# Georges Van Parys: Pianist
# Eugène Stuber: A man at the ball
# Louis Wins: The conductor of the journal
# Irene Ascou: Zouzou, child
# Geo Forster
# Lucien Walter
# Emile Carrara
# Adrienne Trenkel
# Andrée Wendler

==References==
 

==External links==
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 


 
 