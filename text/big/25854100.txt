Usher (2004 film)
Usher is a 2004 film written and directed by Roger Leatherwood.  It is loosely adapted from the short story "The Fall of the House of Usher" by Edgar Allan Poe. It tells the story of a young hitman who falls on hard times and gets a job in a movie theatre. The theatre seems to have a personality of its own and the hitman, named Ash, finds himself losing his personality as he falls into a day-to-day work routine.

The feature film premiered at the 2004 Telluride Indie Film Festival (now defunct) and won Best Feature. Additionally, Thomas Alexander  won the award for "Best Actor in a Feature" from Wicked Pixels 2004 "Cinema Edge" awards and was reviewed by Film Threat magazine (online) and Shock Cinema. 

Since November 2009 the film has been being posted in short non-sequential segments, one a week, by ArtOverLife Studios. 

Roger Leatherwood directed the short animated film "Lieing Man"  in 2008.

==References==
 

==External links==
*  

 

 
 