Puss in Boots (1999 film)
 
 
{{Infobox film
| name           = Puss in Boots
| image          = PussInBoots1999.jpg
| image_size     =
| caption        = Puss In Boots, DVD Cover
| director       = Phil Nibbelink
| producer       = Margit Friesacher Eric Parkinson
| writer         = Jacob Grimm Jacob Grimm (story) Phil Nibbelink
| narrator       = Michael York Vivian Schilling
| music          =
| cinematography =
| editing        = Phil Nibbelink
| studio         = Phil Nibbelink Productions Allumination Plaza Entertainment Non-USA Showcase International
| released       =  
| runtime        = 75 minutes
| country        = US
| language       = English
| budget         =
}} Puss in Boots.
 Michael York, and Vivian Schilling.

==Plot==
 anthropomorphic cat. The cat, wanting to help his owner out of poverty, decides to use his wit to turn Gunther into a prince. In his plan, the cat tries to help Gunther win the heart of the Princess. However, an evil shape shifting ogre also has his eyes on marrying the girl. After she is captured, Gunther and his clever cat go after the Princess to rescue her before it is too late. 

==Differences from the original story==

In the film, the ogres shape changing abilities and other magical powers are granted by a necklace, which he also must wear to survive during daylight. Also differing from the original tale, is that the ogre wants to marry the Princess. He offers the king unlimited gold, jewels and diamonds for his marrying the kings daughter, but the king refuses. When Puss in Boots tricks the ogre into morphing into a mouse, the ogre escapes him and realizes the deception, then morphs into a large monster and attempts to kill the cat. But his necklace is stolen, and the princess, king, millers son and cat are able to prevent him from catching the necklace by dawn.

In the film, some mice are on the side of the millers son following an initial confrontation with Puss in Boots.

==Cast==
 Michael York – Puss in Boots
*Judge Reinhold – Gunther
*Dan Haggerty – The King
*Vivian Schilling – Princess
*Kevin Dorsey – Ogre 
*Charles von Bernuth – Zeek

==See also==
*List of animated feature-length films

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 