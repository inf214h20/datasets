May 18 (film)
{{Infobox film
| name           = May 18
| image          = May 18 (film).jpg
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =    
 | hanja          =  한  
 | rr             = Hwaryeohan hyuga
 | mr             = Hwaryŏhan hyuga}} Kim Ji-hoon
| producer       = Yoo In-taek   Park Chang-hyeon   Lee Su-nam
| writer         = Na Hyeon
| starring       = Kim Sang-kyung Ahn Sung-ki Lee Yo-won Lee Joon-gi
| music          = Kim Seong-hyeon
| cinematography = Lee Du-nam
| editing        = Wang Sang-ik   Ham Sung-won
| distributor    = CJ Entertainment
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}}
May 18 ( ; lit. "Splendid Holiday") is a South Korean film released in 2007 in film|2007.  

==Synopsis== massacre at Gwangju on May 18, 1980. It occurred when General Chun Doo-hwan tried to eliminate any rebels by using military force.

Min-woo leads a relatively peaceful life with his younger brother Jin-woo—until the day the soldiers go on the rampage against the citizens. The citizens form a militia determined to protect their loved ones, and Min-woo finds himself in the middle of it all.

==Timeline of the Gwangju Massacre==

===May 17, 1980===
 airborne paratroopers blocking the university responded with violent means. After the incident, students moved into the downtown area, where they were joined by the citizens of the city. The growing crowd was met by the use of force, including gunfire, that caused some fatalities.

===May 20, 1980===

As the news of the fatalities spread, on May 20, protesters burned down the Munhwa Broadcasting Corporation local station which, under effective government control, portrayed the protests as hooligans led by Communist agents. By May 21, some 300,000 people had joined the protest against the Generals power; weapons depots and police stations were looted of their weapons and the civil militias, known as the Citizen Army, beat back the armed forces, killing several soldiers.

With all routes and communications leading in and out of the city blocked by armed forces, a civilian body was formed to maintain order and conduct negotiations with the government. Although order was well maintained, a number of negotiations to resolve the situation failed to achieve any results.

===May 27, 1980===

On May 27, airborne and army troops from five divisions were inserted and defeated the civil militias in the downtown area in only 90 minutes.

==Cast==
* Kim Sang-kyung as Kang Min-woo
* Ahn Sung-ki as Park Heung-soo
* Lee Yo-won as Park Shin-ae 
* Lee Joon-gi as Kang Jin-woo
* Park Chul-min as In-bong
* Park Won-sang as Yong-dae
* Song Jae-ho as Priest Kim
* Na Moon-hee as Naju-daek
* Son Byung-ho as Teacher Jung
* Baek Bong-ki as Won-ki
* Jung In-gi as Jin-chul
* Lee Eol as Lieutenant Colonel Bae
* Choi Jae-hwan as Byung-jo
* Yoo Hyung-kwan as Byung-jos father
* Im Hyun-sung as Sang-pil
* Park Yong-soo as General Jung
* Kwon Tae-won as General Choi
* Uhm Hyo-sup as Captain Kim
* Kim Cheol-ki as Corporal Yoo

==Awards and nominations==
;2007 Blue Dragon Film Awards 
* Nomination – Best Film
* Nomination – Best Actor – Kim Sang-kyung
* Nomination – Best Actress – Lee Yo-won Kim Ji-hoon
* Nomination – Best Supporting Actor – Park Chul-min
* Nomination – Best Screenplay – Na Hyeon
* Nomination – Best Cinematography – Lee Du-man
* Nomination – Best Art Direction – Park Il-hyun

;2007 Korea Movie Star Awards
*Best Film Kim Ji-hoon
*Best Supporting Actress – Na Moon-hee
*Best Tears Award – Kim Sang-kyung

;2007 Korean Film Awards
* Nomination – Best Supporting Actor – Park Chul-min
* Nomination – Best Art Direction – Park Il-hyun
* Nomination – Best Visual Effects – Kim Jong-su
* Nomination – Best Sound – Jang Gwang-su

;2008 Baeksang Arts Awards
* Nomination – Best Film

;2008 Grand Bell Awards
* Nomination – Best Supporting Actor – Park Chul-min

==References==
 

==External links==
*    
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 