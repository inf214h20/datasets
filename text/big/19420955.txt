Bugambilia
{{Infobox film
  | name = Bugambilia
  | image = BugambiliaPoster.jpg
  | caption = 
  | director = Emilio Fernández
  | producer = Felipe Subervielle
  | writer = Emilio Fernández Mauricio Magdaleno
  | starring = Dolores del Río Pedro Armendáriz Julio Villarreal
  | music = Raúl Lavista
  | cinematography = Gabriel Figueroa
  | editing = Gloria Schoemann
  | distributor = Films Mundiales
  | released =
  | runtime = 105 minutes
  | country = Mexico
  | language = Spanish
  | budget=
    }} 1945 directed by Emilio Fernández and starring Dolores del Río and Pedro Armendáriz.

== Plot ==
In the Mexican city of Guanajuato, in the 1800s, the young and beautiful Amalia de los Robles (Dolores del Río) wakes up the passion of all the men of the region. This provokes the fury of her widowed father, Don Fernando (Julio Villarreal) who sees in her the face of his dead wife. But Amalia  falls in love with the smart Ricardo (Pedro Armendáriz). Tragic circumstances, however, prevent them from being united.

== Cast ==
* Dolores del Río .... Amalia de los Robles
* Pedro Armendáriz .... Ricardo Rojas
* Julio Villarreal .... Don Fernando de los Robles
* Alberto Galán .... Luis Felipe
* Stella Inda .... Zarca
* Arturo Soto Rangel .... Cura
* Concha Sanza .... Nana Nicanora
* Roberto Cañedo .... Alberto, el poeta
* Jose Elías Moreno .... Socio de Ricardo
* Maruja Griffel .... Matilde, la chismosa

== Curiosities ==
The plot of the movie was inspired by the Rodolfo Usiglis poem The Bugambilia, that the writer dedicated to Dolores del Rio.  The movie was considered the Mexican version of the American film Wuthering Heights (directed by William Wyler).  The film paused the important collaboration of Dolores del Río with Emilio Fernández that had lasted 3 years.

== External links ==
* 

 

 
 
 
 
 

 