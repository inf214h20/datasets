Hello Darling
 
{{Infobox film
| name           = Hello Darling
| image          = hellodarling1.jpg
| alt            =  
| caption        = Official Movie Poster
| director       = Manoj Tiwari
| producer       = Ashok Ghai
| writer         = Pankaj Trivedi Sachin Shah
| starring       = Gul Panag Isha Koppikar Celina Jaitley Sunny Deol Javed Jaffrey Divya Dutta Chunky Pandey Seema Biswas Asawari Joshi Mukesh Tiwari Vrajesh Hirjee
| music          = Songs:  
| cinematography = Ravi Yadav
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Hello Darling is a Bollywood comedy film produced by Ashok Ghai and directed by Manoj Tiwari, starring Gul Panag, Isha Koppikar and Celina Jaitley in the lead roles. The film was released on 27 August 2010 under the Mukta Arts Films banner.   Sunny Deol makes a special appearance in the movie. The film is a remake of the Tamil film, Magalir Mattum (1994) produced by Kamal Hassan.

==Plot==
Candy (Celina Jaitley) and Mansi (Gul Panag) work in a fashion designer firm. Satvari Chaudary (Isha Koppikar) joins their firm for a job. Candy and Mansi inform Satvari of their flirtatious boss, Hardhik Vasu (Javed Jaaferi) whose desire is to sleep with hot women. Everyone in the firm knows that Mansi always prepares coffee for Hardhik. One day, Mansi, who is fed up with Hardhik stealing her work, accidentally mixes Rat Kill in his coffee. As Hardhik is about to sip his coffee, he falls off his chair. (But it creates the impression that he died from the poison).

Fearful of getting caught, the three girls (Candy, Mansi and Satvari) head to the hospital to make things right. Due to a misunderstanding, the three girls think hes dead. (Coincidentally, another patient died, which they think is Hardhik)  Mansi and Satvari hide his "body" in the back of their taxi but soon they realize its the wrong body. The next day at work, the three girls find out Hardhik is still alive, who also recorded the girls taking out a dead body from hospital. Hardhik offers the girls to avoid jail on one condition - they must accompany Hardhik to Kandala for 7 days. The day before leaving, the girls contact Hardhik under the pretext of sleeping with him. After a bit of song and dance, they make him unconscious by dousing a cloth with methylated spirits and placing it on his face.

Meanwhile, Hardhiks wife (Divya Dutta) finds out he had an affair with Candy. To reform him, she calls Poolan Bhai (Seema Biswas), who runs a place to reform men of their flirtatious deeds. Due to a misunderstanding, they think her husband is Rocky (Chunkey Pandey), who is Candys girlfriend. Hardhik manages to escape from the house (the girls chained him that night he was unconscious). The girls then contemplate whether to contact Nikhi Bajaj (Sunny Deol), the manager of the firm. He reaches there and disapproves Hardhiks contact for renewal and sends him to Bangladesh. Nikhil appoints Mansi as vice-president and persists her to get married to her fiance, Ashish (Anil Mange). Nikhil himself ends up falling in love with Satvari whereas Candy is reunited with Rocky. The film ends in Bangladesh where Hardhik realizes his boss (Ronit Roy) is gay

==Cast==
*Celina Jaitley as Candy DSouza, Rockys girlfriend, the innovative one the three girls, she had an affair with Hardhik
*Chunkey Pandey as Rocky, Boyfriend of Candy who always boasts a rockstar persona
*Isha Koppikar as Satvari Chaudary. The dim-witted one of the three main girls
*Gul Panag as Mansi Joshi, the smart one of the three girls
*Jaaved Jaffrey as Hardhik Vasu, the flirtatious boss who eyes out Candy, Mansi ans Satvari
*Divya Dutta as Hardhiks wife
*Mukesh Tiwari as Inspector Eagle, hired by Hardhik to solve the case of "Hardhiks dead body"
*Sanjay Mishra as "Dead" Patient
*Vrajesh Hirjee as Thief
*Sunny Deol as Nikhil Bajaj (special appearance)
*Anil Mange as Ashish (Mansis fiance)
*Seema Biswas as Poolan Bhai, a very strict and aggressive woman who runs a reformatory for husbands
*Vivek Shauq as Ajay (cameo appearance)
*Ronit Roy as gay boss in Bangladesh, Hardhiks new boss

==Soundtrack==
{{Infobox album
| Name        = Hello Darling
| Type        = soundtrack
| Artist      =
| Cover       =
| Released    = 27 July 2010
| Recorded    =
| Producer    = Feature film soundtrack
| Length      =
| Label       =
}}
The soundtrack was launched on 27 July 2010.

===Track list===
{{Track listing
| extra_column = Singer(s)
| title1 = Working Girls Priyadarshini
| title2 = Aa Jaane Ja
| extra2 = Javed Jaffrey, Akriti Kakkar, Antara Mitra
| title3 = Mai Leke Aaya Band Baja Richa Sharma, Ritu Pathak, Rana Mazumder
| title4 = Dil Toh Saala
| extra4 = Sunidhi Chauhan
| title5 = Attrah Baras Ki
| extra5 = Suzanne DMello
| title6 = Mai Leke Aaya Band Baja
| note6  = Remix
| extra6 = Richa Sharma, Ritu Pathak, Rana Mazumder - (DJ A-Myth)
}}

==See also==
* Bollywood films of 2010

==References==
 

==External links==
*  

 
 
 