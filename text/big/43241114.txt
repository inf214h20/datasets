Wild Canaries
{{Infobox film
| name           = Wild Canaries
| image          = Wild Canaries film.png
| caption        =
| director       = Lawrence Michael Levine
| producer       = Lawrence Michael Levine
| writer         = Lawrence Michael Levine
| starring       = {{Plainlist|
*Sophia Takal
*Lawrence Michael Levine 
*Alia Shawkat
*Jason Ritter
*Annie Parisse}}
| music          = Michael Montes
| cinematography = Mark Schwartzbard
| editing        = Sofi Marshall
| studio          = Little Teeth Pictures
| distributor   = Sundance Selects
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Wild Canaries is an American comedy film directed, produced and written by Lawrence Michael Levine. It stars Sophia Takal, Alia Shawkat, Jason Ritter and Annie Parisse. The film premiered at the South by Southwest Film Festival on  March 8, 2014. 

The film was released in select theaters and on demand beginning on February 25, 2015 courtesy of Sundance Selects. 

== Synopsis ==
Barri (Sophia Takal) and Noah (Lawrence Michael Levine) are a couple who suspect the mysterious death of her neighbor Sylvia (Marylouise Burke). With their friend Jean (Alia Shawkat) they investigate the crime and discover secrets in the apartment. Anthony (Kevin Corrigan) becomes the prime suspect.

== Cast ==
*Sophia Takal as Barri
*Lawrence Michael Levine as Noah
*Alia Shawkat as Jean
*Jason Ritter as Damiel
*Annie Parisse as Eleanor
*Kevin Corrigan as Anthony
*Marylouise Burke as Sylvia
*Lindsay Burdge as Annabell
*Eleonore Hendricks as Molly
*Jennifer Kim as Lori
*Kent Osborne as Calvin
*Bob Byington as Disgruntled Filmmaker
*Helen Merino as Real Estate Broker

==Release==
The film premiered at the South by South West Film Festival on March 8, 2014. on December 23, 2014 it was announced Sundance Selects had acquired distribution rights for the film with a planned 2015 release.  The film was released in select theaters and on demand beginning on February 25, 2015. 

==References==

 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 