César and Rosalie
{{Infobox_Film |
  name     = César and Rosalie |
  image          = César et Rosalie.jpg|
image_size=|
  writer         = Jean-Loup Dabadie  Claude Néron  Claude Sautet|
  starring       = Yves Montand Romy Schneider Sami Frey |
  director       = Claude Sautet |
  producer       = Michelle de Broca|
cinematography= Jean Boffety|
music= |
  distributor       =Cinema 5  |
  released   = 27 October 1972 |
  runtime        = 107 minutes |
  country = France |
  language =  French |
  budget         = |
}} French romance film starring Yves Montand and Romy Schneider, directed by Claude Sautet.

==Cast==
* Yves Montand - Cesar
* Romy Schneider - Rosalie
* Sami Frey - David
* Bernard Le Coq - Michel
*   - Lucie Artigues
* Henri-Jacques Huet - Marcel
* Isabelle Huppert - Marite
* Gisela Hahn - Carla
* Betty Beckers - Madeleine
* Hervé Sand - Georges
* Jacques Dhéry - Henri Harrieu
* Pippo Merisi - Albert
* Carlo Nell - Jérôme
* Dimitri Petricenko - Simon

==See also==
* Isabelle Huppert filmography

== External links ==
*  

 

 
 
 
 
 
 
 