Angela (1978 film)
{{Infobox film
| name           = Angela
| image          = 
| image_size     = 
| caption        = 
| director       = Boris Sagal
| writer         = Charles E. Israel
| narrator       = 
| starring       = Sophia Loren Steve Railsback John Vernon John Huston
| music          = Henry Mancini
| cinematography = Marc Champion
| editing        = Yves Langlois
| studio         = 
| distributor    = Warner Bros.
| released       = April 19, 1978
| runtime        = 91 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Angela is a 1978 film directed by Boris Sagal. It stars Sophia Loren and Steve Railsback.  

==Cast==
*Sophia Loren as Angela Kincaid
*Steve Railsback as Jean Lebrecque
*John Huston as Hogan
*John Vernon as Ben Kincaid
*Michelle Rossignol as Coco

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 