Top of the Heap (film)
 
{{Infobox film
| name           = Top of the Heap
| image size     = 
| image	=	Top of the Heap FilmPoster.jpeg
| caption        = 
| director       = Christopher St. John
| producer       = Christopher St. John
| writer         = Christopher St. John
| narrator       = 
| starring       = Christopher St. John
| music          = 
| cinematography = Richard A. Kelley
| editing        = Michael Pozen
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
}}

Top of the Heap is a 1972 American drama film directed by and starring Christopher St. John. It was entered into the 22nd Berlin International Film Festival.   

==Cast==
* Christopher St. John - George Lattimer Paula Kelly - Black Chick
* Florence St. Peter - Viola Lattimer
* Leonard Kuras - Bobby Gelman John Alderson - Captain Walsh
* Patrick McVey - Tim Cassidy
* Allen Garfield - Taxi Driver
* Ingeborg Sørensen - Nurse Swenson (as Ingeborg Sorensen)
* Ron Douglas - Hip Passenger John McMurtry - Dope Dealer
* Damu King - Pot Peddler
* Ji-Tu Cumbuka - Pot Peddler Brian Cutler - Rookie Policeman Jerry Jones - Club Owner Willie Harris - Bouncer

==References==
 

==External links==
* 

 
 
 
 
 
 


 