Monsieur Lazhar
 
 
{{Infobox film
| name           = Monsieur Lazhar
| image          = Monsieur lazhar.jpg
| caption        = Film poster
| director       = Philippe Falardeau
| producer       = Luc Déry Kim McCraw
| screenplay     = Philippe Falardeau
| story          = Évelyne de la Chenelière
| starring       = Mohamed Fellag Sophie Nélisse Émilien Néron Danielle Proulx Brigitte Poupart Jules Philip
| music          = Martin Léon, Sherya Ghoshal
| cinematography = Ronald Plante
| editing        = Stéphane Lafleur
| studio         = Microscope Productions Les Films Seville Pictures
| distributor    = Music Box Films Christal Films Arsenal Filmverleih Agora Films UGC International Seville Pictures A Contracorriente Films Thim Film Europafilm Distribution Company
| released       =  
| runtime        = 94 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = $6.6 million   
}}
 Oscar for Best Foreign Language Film at the 84th Academy Awards. The film was released theatrically in the United States on 13 April 2012 by distributor Music Box Films.

==Plot== hangs herself. cultural gap evident from the very first day of class and despite his difficulty adapting to the school systems constraints. As the children try to move on from their former teachers suicide, nobody at the school is aware of Bashirs painful past, or his precarious status as a refugee. His wife, who was a teacher and writer, died along with the couples daughter and son in an arson attack. The murderers were angered by her last book, in which she pointed a finger at those responsible for the countrys reconciliation, which had led to the liberation of many perpetrators of huge crimes. The film goes on to explore Bashirs relationships with the students and faculty, and how the students come to grips with their former teachers suicide. One student, Alice, writes an assignment on the death of their teacher, revealing the deep pain and confusion felt by each of the students.
 teaching qualification; previously, he had run a restaurant. He is then fired from the school. He asks the principal to be able to teach one more day, convincing her by noting that the old teacher never got to say goodbye to her students. On his last day, Bashir has his students correct a fable he wrote which is a metaphor of his tragic past life in Algeria and the loss of his family in a fire. Before he leaves, one of his students, Alice (whom he professed to be his favourite to her mother) gives him a tearful hug goodbye.

==Cast== Mohamed Saïd Fellag as Bachir Lazhar
* Sophie Nélisse as Alice LÉcuyer
* Émilien Néron as Simon
* Danielle Proulx as Mrs. Vaillancourt
* Brigitte Poupart as Claire Lajoie
* Jules Philip as Gaston
* Daniel Gadouas as Mr. Gilbert Danis
* Louis Champagne as Concierge
* Seddik Benslimane as Abdelmalek
* Marie-Ève Beauregard as Marie-Frédérique
* André Robitaille as Commissionner
* Francine Ruel as Mrs. Dumas
* Sophie Sanscartier as Audrée
* Évelyne de la Chenelière as Alices mother
* Vincent Millard as Victor
* Louis-David Leblanc as Boris
* Nicole-Sylvie Lagrande as Psychologist
* Gabriel Verdier as Jordan
* Marie Charlebois as Prosecutor
* Marianne Soucy-Lord as Shanel
* Stéphane Demers as Marie-Frédériques father
* Nathalie Costa as Marie-Frédériques mother
* Héléna Laliberté as Martine Lachance

==Production==
There were 28 days of shooting, about an average time for a film to be shot in Quebec. The principal filming occurred in the summer so the production could use the school and so the educations of the child actors were not disrupted. Four of the shooting days occurred in the winter. 

== Reception ==

=== Box office ===
Monsieur Lazhar grossed $2,009,517 in North America and $4,572,398 in other countries, for a worldwide total of $6,581,915. 

=== Critical response ===
The film received a very high critical acclaim. At Rotten Tomatoes, the film holds a rating of 97%, based on 110 reviews and an average rating of 8.1/10.    It also has a score of 83 on Metacritic based on 31 reviews.    Critic Ann Hornaday of The Washington Post called the film one of the ten best of 2012. 

=== Awards ===
The film was selected as the Canadian entry for the  , 17 February 2012.  It is also the second consecutive year that a Canadian film in French was nominated in this category, following the nomination of Denis Villeneuves Incendies at the 83rd Academy Awards.
 Jutra Film Awards, winning seven out of nine categories for which it was nominated, a finalist in the most categories at the Jutra awards.  It also won the City of Toronto Award as the Best Canadian Feature Film at the 2011 Toronto International Film Festival.      
 Best Picture, Best Actor, Best Supporting Best Adapted Achievement in Achievement in Best Actress Best Young Best Young Actress in an International Feature Film respectively.   

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 