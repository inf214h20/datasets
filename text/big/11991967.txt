Ek: The Power of One
 
 

 
{{Infobox film
| name           = Ek: The Power of One
| image          = Ekthepowerofone poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sangeeth Sivan
| producer       = Jaswant Khera
| screenplay     = Trivikram Srinivas
| story          = Trivikram Srinivas
| starring       = Bobby Deol Shriya Saran Nana Patekar
| music          = Songs:  
| cinematography = T Ramji
| editing        = Chirag Jain
| studio         = 
| distributor    = K Sera Sera Dharam Films
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = Indian rupee|Rs. 60&nbsp;million   
}} Telugu film Athadu. Athadu was a super hit movie while EK flopped and received negative reviews.

==Plot==
Nandu (Bobby Deol) an orphan turned assassin somehow gets wrongly accused of a politicians murder and is on the run. On his escapade on a train he meets Puran (Akshay Kapoor), who is homebound after fourteen years, they get chatting and Nandu finds all about his joint family and the wedding of Purans sister that he is going to attend.

However the police catch up with Nandu on the train and shoot at him but Puran gets hit accidentally and dies on the spot. Nandu goes to Purans family home in his village to return his grandfathers watch and also to break the news of his grandsons death to him. However it turns out that Purans family mistakes Nandu for Puran and makes him a part of the celebration at home. He thinks that it is his responsibility to fulfill the deeds the Puran had to fulfill.

Preet (Shriya Saran), who is the daughter of Purans grandfathers friend is in awe of Nandu and falls in love with him. CBI Inspector Rane (Nana Patekar), is given the responsibility of tracking down the killer and is after Nandu.

At the end, Nandu realizes that his friend Shekar (Pradeep Kharab) is behind everything. He has a tape of his innocence and gives it to Inspector Rane. He then goes back to Purans family and lives with them.

==Cast==

* Nana Patekar as CBI Inspector NandKumar Rane 
* Bobby Deol as Nandkumar Nandu Sharma / Fake Puran Singh
* Shriya Saran as Preet
* Jackie Shroff as Savte (Special Appearance)
* Kulbhushan Kharbanda as Kripal Singh
* Zarina Wahab as Gayatri bua
* Akshay Kapoor as Puran Singh
* Gurpreet Ghuggi as Guru
* Rana Ranbir as Dev
* Sachin Khedekar as Anna Mhatre
* Pradeep Kharab as Shekhar
* Raghuveer Yadav as Joshi
* Preeti Bhutani as Nandni
* Jaspal Bhatti as Amrik Singh Nasa
* Upasna Singh as Manjit bua
* Sanjay Mishra as Advocate Parminder Singh
* Ritu Vij as Balbir bua
* Anant Jog as Police Inspector 
* Chunky Pandey as Balli
* Rana Jung Bahadur as Chaudhary
* Shishir Sharma as CBI Chief
* Simran Deepali as Channo

==Soundtrack==

The songs of the film were composed by Pritam and the song Bang Bang was written by Mayur Puri.

==Reception==
As of April 2009, the film has grossed Indian rupee|Rs. 130&nbsp;million. 

==References==
 

 
 
 
 
 