The Shamrock Handicap
 
{{Infobox film
| name           = The Shamrock Handicap
| image          =
| caption        =
| director       = John Ford
| producer       = John Ford John Stone Elizabeth Pickett
| starring       = Janet Gaynor Leslie Fenton
| music          =
| cinematography = George Schneiderman
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 66 minutes (22.9 frame rate| frame/s)
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

The Shamrock Handicap is a 1926 American romance film directed by John Ford. A print of the film still exists in the Museum of Modern Art film archive.   
 Don Juan possibly because his character in that film vanishes.

==Cast==
* Janet Gaynor as Lady Sheila OHara
* Leslie Fenton as Neil Ross
* Willard Louis as Orville Finch
* J. Farrell MacDonald as Cornelius Emmet Sarsfield Con OShea
* Claire McDowell as Molly OShea
* Louis Payne as Sir Miles OHara
* George Harris as Jockey Bennie Ginsburg (as Georgie Harris)
* Andy Clark as Chesty Morgan
* Ely Reynolds as Virus Cakes
* Thomas Delmar as Michaels (uncredited)
* Bill Elliott as Well-Wishing Villager (uncredited)
* Brandon Hurst as The Procurer of Taxes (uncredited)
* Eric Mayne as Doctor (uncredited)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 
 