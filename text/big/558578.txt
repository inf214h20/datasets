Merry Christmas, Mr. Lawrence
 
{{Infobox film
| name           = Merry Christmas, Mr. Lawrence
| image          = Merry Christmas Mr Lawrence poster Japanese.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original Japanese poster
| director       = Nagisa Oshima
| producer       = Jeremy Thomas
| writer         = Nagisa Oshima Paul Mayersberg 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring = {{Plainlist|
* David Bowie
* Tom Conti
* Ryuichi Sakamoto Takeshi
* Jack Thompson
}}
| music          = Ryuichi Sakamoto
| cinematography = Toichiro Narushima
| editing        = Tomoyo Oshima
| studio         = Recorded Picture Company
| distributor    = Universal Pictures (US) Palace Pictures (UK) Shochiku (Japan)
| released       =   
| runtime        = 123 minutes
| country        = United Kingdom Japan New Zealand
| language       = Japanese English
| budget         = 
| gross          = 
}} Jack Thompson.

It was written by Oshima and Paul Mayersberg and based on Sir Laurens van der Posts experiences as a Japanese prisoner of war during World War II as depicted in his works The Seed and the Sower (1963) and The Night of the New Moon (1970). Sakamoto also wrote the score and the vocal theme "Forbidden Colours", featuring David Sylvian, which was a hit single in many territories.

The film was entered into the 1983 Cannes Film Festival in competition for the Palme dOr.    Sakamotos score also won the film a BAFTA Award for Best Film Music.   

==Plot==
The film deals with the relationships among four men in a Japanese prisoner of war camp during the Second World War &mdash; Major Jack Celliers, a rebellious New Zealander with a guilty secret from his youth; Captain Yonoi, the young camp commandant; Lieutenant Colonel John Lawrence (Conti), a British officer who has lived in Japan and speaks Japanese fluently; and Sergeant Hara who is seemingly brutal and yet humane in some ways and with whom Lawrence develops a peculiar friendship.
 Allied officer and prisoner representative) with Celliers as the spokesman for the prisoners.
 batman suspects the mental hold that Celliers has on Yonoi so he tries to kill Celliers but fails in the attempt. Celliers manages to escape his cell and rescues Lawrence, only to be thwarted by Yonoi unexpectedly. Yonoi challenges Celliers to single combat saying "If you defeat me, you will be free" but Celliers refuses, thrusting his prior assailants bayonet into the sand. Yonois batman then commits seppuku in atonement after urging Yonoi to kill Celliers before Celliers can destroy Yonoi.

It is Christmas Eve and Sgt Hara is drinking heavily and orders both Celliers and Lawrence to be brought to him. Hara then advises them that he is playing "Santa Claus" and is ordering their release because a prisoner confesses having the radio. He then calls out in English for the first time "Merry Christmas Lawrence".
 rank and walks decisively in Yonois direction, between him and the man about to be executed and ends up resolutely kissing him on the cheek with a straight face. This is an unbearable offence to Yonois bushido honor code; he reaches out for his katana against Celliers, only to collapse under the conflicting feelings of vindicating himself from the offence suffered in front of his troops and his own feelings for Celliers. Celliers is then attacked and beaten up by the Japanese soldiers.

Captain Yonoi himself is then redeployed and his successor who declares that "he is not as sentimental as Captain Yonoi" immediately has Celliers buried in the ground up to his neck as a means of punishment and then left to die. Captain Yonoi goes to Celliers when there is no one around and cuts a lock of hair. He then pays his respects and leaves, and Celliers dies shortly afterwards.

In 1946, four years later, Lawrence visits Sergeant Hara, who has now been imprisoned by the Allied forces. Hara has learned to speak English while in captivity and reveals that he is going to be executed the next day for war crimes, stating that he is not afraid to die, but doesnt understand how his actions were any different from those of any other soldier. Lawrence tells him that Yonoi had given him a lock of Celliers hair and told him to take it to his village in Japan, where he should place it in a shrine. Hara reminisces about Celliers and Yonoi. It is revealed that Yonoi himself was killed just before the war ended. Hara reminisces about that Christmas Eve and both are very much amused. The two bid each other farewell for the last time and just before Lawrence leaves, Hara calls out again, "Merry Christmas! Merry Christmas, Mr. Lawrence".

==Cast==
* David Bowie - Maj. Jack Strafer Celliers
* Tom Conti - Lt. Col. John Lawrence
* Ryuichi Sakamoto - Capt. Yonoi
* Takeshi Kitano - Sgt. Gengo Hara Jack Thompson - Group Capt. Hicksley
* Johnny Okura - Kanemoto
* Alistair Browning - De Jong
* James Malcolm - Celliers brother
* Chris Broun - Celliers aged 12
* Yuya Uchida - Commandant of military prison
* Ryunosuke Kaneda - Colonel Fujimura, President of the Court
* Takashi Naitô - Lt. Iwata
* Tamio Ishikura - Prosecutor
* Rokko Toura - Interpreter
* Kan Mikami - Lt. Ito

==Production== The Elephant Man on Broadway theatre|Broadway.  He commented that Bowie had "an inner spirit that is indestructible." While shooting the movie, Bowie was amazed that Oshima had a two- to three-acre camp built on the remote Polynesian island of Rarotonga, but most of the camp was never shot on film. He said Oshima "only shot little bits at the corners. I kind of thought it was a waste, but when I saw the movie, it was just so potent - you could feel the camp there, quite definitely."    Bowie noted how Oshima would give an incredible amount of direction to his Japanese actors ("down to the minutest detail"), but when directing him or fellow Westerner Tom Conti, he would say "Please do whatever it is you people do."    Bowie thought his performance in the movie was "the most credible performance" hed done in a film up to that point in his career. 
 rushes and shipped the film off the island with no safety prints. "It was all going out of the camera and down to the post office and being wrapped up in brown paper and sent off to Japan," Bowie stated. Oshimas editor in Japan cut the movie into a rough print within four days of Oshima returning to Japan. 

==Soundtrack==
{{Infobox album  
| Name        = Merry Christmas, Mr. Lawrence 
| Type        = soundtrack
| Artist      = Ryuichi Sakamoto
| Cover       = 
| Length      =
| Recorded    =
| Released    = 1983 (Virgin Records) October 11, 1994 (Milan Records)
| Genre       = Ambient music|Ambient, Classical music|classical, film score|film, synthpop,  downtempo,  Electronic music|electronic, Folk music|folk, World music|world,    gamelan 
| Label       = Milan Records, Virgin Records
}}

All compositions are by Ryuichi Sakamoto except "23rd Psalm", which is Traditional music|traditional. Lyrics are written and sung by David Sylvian on "Forbidden Colours". Sakamoto won the 1983 BAFTA Award for Best Film Music for the films soundtrack. 

Track listing: Merry Christmas Mr. Lawrence"
# "Batavia"
# "Germination"
# "A Hearty Breakfast"
# "Before the War"
# "The Seed and the Sower"
# "A Brief Encounter"
# "Ride, Ride, Ride (Celliers Brothers Song)"
# "The Fight"
# "Father Christmas"
# "Dismissed"
# "Assembly"
# "Beyond Reason"
# "Sowing the Seed"
# "23rd Psalm"
# "Last Regrets"
# "Ride, Ride, Ride (Reprise)"
# "The Seed"
# "Forbidden Colours"

==Reception==
The film has a 79% Critic Rating on Rotten Tomatoes, and an 82% Audience Rating.   It has a rating of 7.3 on IMDB.com. 

New York Times critic Janet Maslin wrote a favorable review, saying that David Bowie plays a born leader in Nagisa Oshimas Merry Christmas Mr. Lawrence, and he plays him like a born film star. Mr. Bowies screen presence here is mercurial and arresting, and he seems to arrive at this effortlessly, though he manages to do something slyly different in every scene. The demands of his role may sometimes be improbable and elaborate, but Mr. Bowie fills them in a remarkably plain and direct way. Little else in the film is so unaffected or clear. 
On the films Japanese actors, she writes that "the two main Japanese characters who have brought him to this understanding are Sergeant Hara (Takeshi), a brutal figure who taunts Lawrence while also admiring him, and Captain Yonoi (Ryuichi Sakamoto), the handsome young camp commander, who has a fierce belief in the samurai code. Both of these actors perform at an obvious disadvantage, since their English is awkward and the motives of their characters are imperfectly revealed. However, they are able to convey the complex affinity that exists between captors and prisoners, a point that is made most touchingly in a brief postwar coda." 

==References==
 

==External links==
*  
*  
*  
*   at NZ On Screen

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 