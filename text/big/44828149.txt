Romance Complicated
{{Infobox film
| name           = Romance Complicated
| image          = Romance Complicated Official Poster.jpg 
| caption        = Official Poster
| director       = Dhwani Gautam
| producer       = Nishit K. Jain   Rajiv Sharma   Kirti Premraaj Jain   
| writer         = Dhwani Gautam
| starring       = Malhar Pandya   Divya Misra   Dharmesh Vyas   Shekhar Shukla   Darshan Jariwala   Kshitisha Soni   Umang Acharya   Elvira Stoian   Dhwani Gautam 
| music          = Jatin-Pratik & Darshan Raval
| cinematography = Prashant Gohel
| editing      = HarkiratSingh Lal
| studio = Luminescence Films   (In Association with)   Dhwani Gautam Films
| released       =  
| runtime        = 
| country        = India
| language       = Gujarati
}}

Romance Complicated is a 2015 Romantic Gujarati Comedy film, directed by Dhwani Gautam   and produced by Nishit Jain & Rajiv Sharma. The Film stars Malhar Pandya   and Divya Misra   make their debut as leads in this Gujarati film. The film is expected to release on 3 July 2015.  

==Cast==
* Malhar Pandya as Dev Patel
* Divya Misra as Maahi Patel
* Dharmesh Vyas as Dhiraj Patel 
* Shekhar Shukla as Karsan Patel 
* Darshan Jariwala as Tiku Mamaji 
* Kshitisha Soni as Aashima 
* Umang Acharya as Aaditya 
* Nirmit Vaishnav as Sandeep Patel.
* Riddhi Raval Parikh as Aarti Patel
* Shweta Dhasmana as Shweta Patel
* Harish Daghiya as Paresh
* Maulik Nayak as Pappu 
* Aishwaria Dussane as Anushka Joshi 
* Nilofer Khalid as Preeti Patel 
* Elvira Stoian as Elvira
* Dhwani Gautam as Aarsh Malhotra 
* Avani Modi as Pooja Sharma

== Production ==

=== Development === Shreya Goshal, Javed ali, Rashid Ali sing the title track of the movie.

=== Filming ===
Shooting of the film started in post September 2014. The shooting is said to set in some parts of Ahmedabad and Gandhinagar in India, and New York and Florida in United States. 

=== Casting ===
Debutants Malhar Pandya and Divya Misra have signed this movie playing the role of Dev Patel and Maahi Patel. Dharmesh Vyas, Darshan Jariwala and Shekhar Shukla are in a supporting role. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 