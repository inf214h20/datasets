Snow Trail
{{Infobox film
| name           = Snow Trail
| image          = Snow Trail poster.jpg
| caption        = Japanese movie poster
| director       = Senkichi Taniguchi
| producer       = Tomoyuki Tanaka
| writer         = Akira Kurosawa 
Senkichi Taniguchi (uncredited)
| starring       =
| music          = Akira Ifukube
| studio         = Toho
| cinematography =
| editing        =
| distributor    = 1947 
| runtime        = 89 min.
| country        = Japan
| awards         = Japanese
| budget         =
| preceded_by    =
| followed_by    =
|
}} Japanese film directed by Senkichi Taniguchi  from Akira Kurosawas screenplay. It was the first film role for Toshirō Mifune, later to become one of Japans most famous actors. Mifune and the other main actor in the film Takashi Shimura, later became long-term collaborators of film director Akira Kurosawa.
 

== Cast ==
* Toshiro Mifune as Eijima
* Takashi Shimura as Nojiro
* Yoshio Kosugi as Takasugi
* Akitake Kono as Honda
* Setsuko Wakayama as Haruko
* Kokuten Kōdō as Harukos Grandfather 

==Plot==
The story follows the fate of three bank robbers (Mifune, Takashi Shimura, and Yoshio Kosugi) on the run from the police, who hide out high up in the snowy Japanese mountains in a remote lodge inhabited by an old man, his granddaughter and an intrepid mountaineer (Akitake Kono) trapped there by a recent blizzard.  They don’t know that the men are criminals, and a tense standoff starts to unfold.

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 