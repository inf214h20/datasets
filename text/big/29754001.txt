Run Chrissie Run!
{{Infobox film
| name           = Run Chrissie Run!
| image          = 
| caption        = Original poster
| director       = Chris Langman
| producer       = Jock Blair Harley Manners Ron Saunders
| writer         = Graham Hartley Based on the novel by Keith Leopold
| starring       = Carmen Duncan Red Symons Michael Aitkens Shane Briant
| music          = Robert Kretschmer Ernie Clark
| editing        = Andrew Prouse
| studio         = South Australian Film Corporation
| distributor    = Australian Video (Australia) EuroVideo (Germany)
| released       = 1986
| runtime        = 95 minutes
| country        = Australia
| language       = English
| budget         = A$1,646,000 "Production Survey", Cinema Papers, August 1984 p261 
}} 

Run Chrissie Run! is a 1986 Australian film, directed by Chris Langman. Graham Hartley adapted the script from the novel When We Ran by Keith Leopold. The film was released in the USA under the title Moving Targets.

The film is not connected in any way with the unproduced radio play Run, Chrissie, Run by Fay Weldon. 

==Plot== IRA hitmen Annie Jones) and the proceeds of an old bank robbery in Germany. The two IRA hitmen, accompanied by an angry biker, track Eve and Chrissie to the Barossa Valley. Riley arrives on the scene in time for an explosive finale.

==Cast==
*Carmen Duncan ... Eve
*Red Symons ... Pitt
*Michael Aitkens ... Riley
*Shane Briant ... Terrier
*Nicholas Eadie ... Toe Annie Jones ... Chrissie
*David Clencie ... Paul
*Peter Stratford ... Meyerdahl
*Sarah De Teliga ... Sue
*Simone Buchanan ... Cathy
*Joanna Moore ... Cricket coach
==Release==
The film was made in 1984 but not screened on Australian TV until 1988. 
==See also==
 
* Australian films of 1986
* Cinema of Australia
* List of films shot in Adelaide
* List of Australian films
* South Australian Film Corporation

==References==
 

==External links==
*  
*  - South Australian Film Corporation

 
 
 
 
 
 
 
 