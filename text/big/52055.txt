Mars Needs Women
{{Infobox film
| name           = Mars Needs Women 
| caption        = DVD cover
| image	         = Mars Needs Women FilmPoster.jpeg
| director       = Larry Buchanan
| producer       = Larry Buchanan
| writer         = Larry Buchanan
| starring       = Tommy Kirk Yvonne Craig Patrick Cranshaw
| narrator       = Larry Buchanan
| music          = Ronald Stein
| cinematography = Robert Jessup
| editing        = Larry Buchanan
| studio         = Azalea Pictures
| distributor    = American International Television
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $20,000 (estimate) 
}}
 American science schlock artist/auteur Larry Buchanan,  and stars Tommy Kirk, Yvonne Craig, and Byron Lord.  The film was syndicated directly to television without a theatrical release by American International Pictures.
 
==Plot==
In 1966 a U. S. military station in Houston, the United States Decoding Service (U.S.D.S.), NASA Wing, has intercepted a message. After decoding, the message contains only the cryptic statement: "Mars ... Needs ... Women". 

Martians have developed a genetic deficiency that now produces only male children. A mission to Earth is launched, consisting of five Martian males, led by Dop (Tommy Kirk). The team intends to recruit Earth women to come to Mars to mate and produce female offspring, saving their civilization from extinction; the team finally selects their prospective candidates. Using their sophisticated transponder, Dop attempts to make contact with the military, who have now tracked their arrival on Earth.

The U. S. military eventually views the Martians as invaders, so the team takes on the guise of Earth men, acquiring human clothes, money, maps, and transportation. They quickly set their sights on four women: a Homecoming Queen, a stewardess, a stripper, and, most especially, a Pulitzer Prize-winning scientist, Dr. Bolen (Yvonne Craig), an expert in "space genetics". Resorting to hypnosis, the women are captured, but Dop quickly becomes enamored with Dr. Bolen; soon he is ready to sabotage their mission to Earth for her. After the military discover their hideout, the Martians are forced to return home without their female captives. 

Mars still needs women.

==Cast==
Main roles and screen credits:   Turner Classic Movies. Retrieved: June 22, 2012. 
  Seattle Sun reporter)
* Yvonne Craig (as Dr. Marjorie Bolen)
* Warren Hammack (as Martian Doctor/Fellow #2)
* Tony Huston (as Martian Fellow #3, billed as Anthony Huston)
* Larry Tanner (as Martian Fellow #4)
* Cal Duggan (as Martian Fellow #5)
* Pat Delaney (as artist abductee)
* Sherry Roberts (as Brenda Knowlan, abductee)
* Donna Lindberg (as Stewardess, abductee)
* "Bubbles" Cash (as Stripper, abductee)  
* Byron Lord (as Col. Bob Page, U.S.D.S.)
* Roger Ready (as Stimmons)
* Barnett Shaw (as Man at military conference)
* Neil Fletcher (as Secretary of Defense)
* Chet Davis (as network news reporter)
 
==Production==
 "  , Dop, the Martian leader, wearing a poor-fitting skin divers wet suit, materializes in the Space Center, attempting to explain his mission on Earth.]]
 John Ashley, who had just made The Eye Creatures (1965) for Buchanan, says he was meant to play the lead role but got busy on another project and Tommy Kirk then stepped in. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p238 

Kirk called the film "undoubtedly one of the stupidest motion pictures ever made. How I got talked into it, I dont know."  

Kirk had previously played a Martian seeking Earth women in AIPs   for his scene in a planetarium (at Dallas Fair Park) as he explains that his world is dying.  Dietrich, Christopher.   dvddrive-in.com. Retrieved: June 22, 2012. 

The other notable lead, Yvonne Craig, had starred in several films,  and numerous television roles, including her portrayal of Batgirl in the CBS cult comedy superhero TV series Batman.

Over a short, two-week shooting schedule, Buchanan shot Mars Needs Women in his hometown of Dallas, pretending it to be Houston. Faced with his usual meager budget, he resorted to using available spaces, including office buildings, to serve as NASA headquarters. Typical shoddy "B" movie production values are evident throughout the film. Southland Life Insurance Building is clearly visible as the Martians drive among the humans. Other prominent Dallas landmarks, including Southern Methodist University, are also featured. Footage from a SMU Homecoming game is used, and the Homecoming Queen is one of the Martians "recruits". Footage was also shot at Dallas Fair Park (Planetarium, Lagoon, and Science Building). Additional footage was shot at Dallas Love Field, where a man is shown reading the Houston Chronicle, and at the Gypsy Room on Harry Hines Blvd. The scene at the NASA "Space Center" was filmed in Richardson, TX, north of Dallas, in the Antenna Building, which belonged to Collins Radio, a NASA contractor.
 Boeing NB-52B mother ship General Dynamics F-111 fighter-bomber in particular). Due to poor lighting, parts of the film were made by undercranking the camera and having the actors move more slowly, sometimes shooting at 18 or 12 frames per second instead of the usual 24. Actors also stretched out scenes with long sequences with no dialogue, either walking or doing menial tasks. One lengthy scene involves the camera focused on a loudspeaker. 

==Reception==
Although originally intended for theatrical release, Mars Needs Women was distributed directly to television by American International. The film subsequently met a receptive late night viewing audience, becoming a perfect example of the cult film, where all the unintended hilarity comes from the films overly serious approach, "cheesy" production values, and ludicrous plot.  One of the films more notable fans was Frank Zappa, who referenced the film in the title of an instrumental composition, "Manx Needs Women," which was regularly performed by Zappa in concert in 1976; recordings of the songs performances appear on Zappas albums Zappa In New York and Philly 76.
 Pump Up the Volume" by the group MARRS ("Mars ... needs ... women ...!") is taken from the films original preview trailer. 

Heavy metal musician, Rob Zombie, released a song of the same name on his 2010 album, Hellbilly Deluxe 2.

==Proposed sequel==
In the early 1990s Buchanan announced a sequel was in development at Universal Pictures with John Avnet and Jordan Kerner. It was intended to be "a sophisticated romantic comedy based on the ideas first set forth in the original." Goodsell, Greg. "The Weird and Wacky World of Larry Buchanan". Filmfax, No. 38, April/May 1993, p. 60.  The film was never made.

==References==
Citations
 
Notes
 
Bibliography
 
* Ray, Fred Olen. The New Poverty Row: Independent Filmmakers as Distributors. Jefferson, North Carolina: McFarland Publishers, 1991. ISBN 978-0-8995-0628-9.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 