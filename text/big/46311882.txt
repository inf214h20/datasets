Farebi Shahzada
 
 
{{Infobox film
| name           = Farebi Shahzada 
| image          = 
| image size     =
| caption        = 
| director       = A. R. Kardar
| producer       = United Players Corporation
| writer         = 
| narrator       =
| starring       = Gul Hamid Gulzar Hiralal M. Ismail
| music          = 
| cinematography = K. V. Machve
| editing        =
| studio         = Playart Phototone/United Players Corporation
| distributor    =
| released       =  
| runtime        = 
| country        = British India
| language       = Silent film
| budget         =
| preceded by    =
| followed by    =
}} 1931 Indian cinemas action silent film directed by A. R. Kardar.       The film was also known as Gudaria Sultan or The Shepherd King and was the fourth of seven films Kardar produced under Kardars United Players Corporation, Lahore.    

The film starred Gul Hamid, Gulzar and M. Ismail with Hiralal.    The other actors in the cast included Hassan Din, Ahmed Din, Haider Shah and Fazal Shah.    Farebi Shahzada, like Kardars earlier films was also released at The Deepak cinema in Bhati Gate area of Lahore.   

==Cast==
* Gulzar
* M. Ismail
* Hiralal
* Hassan Din
* Haider Shah
* Fazal Shah
* Ahmed Din
 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 