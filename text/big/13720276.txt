Go Naked in the World
{{Infobox film
| name           = Go Naked in the World
| image          = Go naked in the world.jpg
| image_size     = 
| caption        = Theatrical release poster by Reynold Brown
| director       = Ranald MacDougall Charles Walters (uncredited)
| producer       = Aaron Rosenberg
| writer         = Ranald MacDougall
| based on       =  
| narrator       = 
| starring       = Gina Lollobrigida Anthony Franciosa Ernest Borgnine
| music          = Adolph Deutsch
| cinematography = Milton R. Krasner
| editing        = John McSweeney Jr.
| studio         = Arcola Pictures
| distributor    = Metro-Goldwyn-Mayer
| released       = March 10, 1961
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Go Naked in the World is a 1961 American drama Metrocolor film in CinemaScope written and directed by Ranald MacDougall and co-directed by an uncredited Charles Walters, produced by Aaron Rosenberg, and starring  Gina Lollobrigida,  Anthony Franciosa, and Ernest Borgnine. it is based on a 1959 novel of the same name by Tom  T. Chamales.

==Plot==
The son of a wealthy Greek immigrant, Nick Stratton (Anthony Franciosa) is attempting to find his own way in the world after returning home to San Francisco following a stint in the Army. His father Pete (Ernest Borgnine) is a self-made millionaire and important in the Greek immigrant community. Pete loves his son but he tries to buy his love and dominate his life like he does his employees and business associates. Nick struggles to assert his own identity but the family pressure and his love for his Father are very strong. Pete wants Nick to marry a nice Greek girl, the daughter of a business associate, but Nick falls in love with Guilietta Cameron (Gina Lollobrigida). He takes her as his date to his parents wedding anniversary full of members of the Greek immigrant community. Pete reveals to him in no uncertain terms that Guilietta is a prostitution|prostitute, whose services Pete and many of his friends, who are present at the party, have purchased. 

Nick tries to disengage from Guilietta but they are madly in love. Everywhere they go they keep meeting former clients of Guiliettas. Guilietta does everything she can to drive him away, hurting him and making him feel like just another "John" who now bores her, but she really doesnt want this. She wants him to prove his love for her. She is devastated when she succeeds in driving him away. She sees him in a bar after she has gone off with a former client and starts flirting outrageously and physically with every man she can find. Nick gets into a fight dragging men off her and is arrested.

==Additional information==
(The basic story of a well-born young man falling in love with a prostitute, only to have his father intervene in the relationship by appealing to the young woman to leave him, has been told many times, most famously as the film Camille (1936 film)|Camille, and the opera La Traviata.)

==Cast==
* Gina Lollobrigida - Guilietta Cameron
* Anthony Franciosa - Nick Stratton
* Ernest Borgnine - Pete Stratton
* Luana Patten - Yvonne Stratton
* Will Kuluva - Argus Dlavolos
* Philip Ober - Josh Kebner John Kellogg - Cobby, Hotel Detective
* Nancy R. Pollock - Mary Stratton
* Tracey Roberts - Diana

==Reception==
Upon its release, Bosley Crowther gave the film a scathing review: "Here is a film that its producer — not the censor, not anybody else; just its producer — should have taken out and burned." 

===Box Office===
According to MGM records the film recorded a loss of $1,462,000.   }}. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 