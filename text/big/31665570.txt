Bill Cracks Down
{{Infobox film
| name           = Bill Cracks Down
| image          = File:Bill Cracks Down lobby card.jpg
| image_size     = 
| caption        = Lobby card
| director       = William Nigh
| producer       = William Berke (associate producer)
| writer         = Morgan Cox (story) and Owen Francis (story) Dorrell McGowan (screenplay) and Stuart E. McGowan (screenplay)
| narrator       = 
| starring       = See below
| music          =  William Nobles Edward Mann
| distributor    = Republic Pictures
| released       = 1937
| runtime        = 61 minutes 53 minutes (American edited version)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bill Cracks Down is a 1937 American film directed by William Nigh. The film is also known as Men of Steel in the UK.

== Cast ==
*Grant Withers as "Tons" Walker
*Beatrice Roberts as Susan Bailey
*Ranny Weeks as Bill Reardon Jr., aka Bill Hall
*Judith Allen as Elaine Witworth William Newell as Eddie "Porky" Plunkett
*Pierre Watkin as William "Bill" Reardon Sr. Roger Williams as Steve, Mill Foreman
*Georgia Caine as Mrs. Witworth
*Greta Meyer as Hilda
*Edgar Norton as Jarvis, the Butler
*Harry Depp as Smalley, the Lawyer
*Eugene King as Zimich
*Landers Stevens as Dr. Colcord
*Eddie Rochester Anderson as Chauffeur

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 