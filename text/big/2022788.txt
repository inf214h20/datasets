Bionicle 3: Web of Shadows
{{Multiple issues
| 
 
 
}}

{{Infobox film
| name= Bionicle 3: Web of Shadows
| image= Fus.jpg
| image size = 230px
| screenplay= Brett Matthews
| story= Bob Thompson Henry Gilroy Greg Farshtey
| director=  Terry Shakespeare, David Molina
| producer= Sue Shakespeare, Bob Thompson LEGO
| distributor= Miramax Films Buena Vista Home Entertainment
| released= 11 October 2005
| runtime = 
| language = English
| music= Nathan Furst
| editing= Billy Jones
| awards= 
| budget=
}}
Bionicle 3: Web of Shadows is the  .

The film tells the tale of the heroic Toa Metru returning to their island city home of Metru Nui to rescue its populace the Matoran, all of which remain in a deep coma. But upon arrival, the heroes discover their once beautiful island home has been destroyed and overrun by webs created by spider-like beasts called Visorak, who capture the Toa and poison them, mutating their form into a half-Toa, half-beast state; Toa Hordika. With the aid of the Rahaga, they begin a quest to find the legendary Keetongu in order to find a way to change back into their original Toa forms to rescue the Matoran, who were being held by the monstrous Visorak hordes and their king Sidorak and viceroy Roodaka.

==Plot==
The film begins with the sight of the crystal prison of the Makuta. Suddenly, a mysterious voice calls his name and a talon scratches the prison, causing a small single shard to fall off and land in the Amaja circle from the previous film. Vakama, as a List of Bionicle characters#Turaga|Turaga, begins again the tale of the Toa Metru (from where it left off in the previous film), who sealed the evil Makuta and vowed to save the sleeping Matoran. But the task would not be easy, as the Makuta had called out to his legions, a horde of poisoners led by a ruthless king and a malevolent queen, and the noble Toa must now face a web of shadows.
 Matoran trapped Visorak attack the Toa with their spinners, paralyzing them. Later, a Keelerak heads to the Colosseum to report to the hordes king, Sidorak. The king of the horde orders the Toa killed, but the Vortixx Roodaka, the viceroy and future queen of the hordes, persuades Sidorak to allow the Hordika venom to take effect before killing the Toa and bring her their bodies. As the Toa hang above in their Pupa#Cocoon|cocoons, the Toa are arguing with Matau blaming Vakama for leading them into a trap. As they hang, they are transformed into Toa Hordika, causing them to fall from their high altitude. Luckily, they are saved by the six beings who later introduce themselves as Rahaga, led by Norik.
 Rahi named Keetongu, whom no one has seen for millennia, nor do some believe his existence. Vakama, angry for being blamed and thinking that his friends dont understand how difficult it is to be a leader, storms off, determining to get the better of the other Toa by trying save the Matoran alone. However, he is captured by the Visorak, waking up in the Colosseum observation tower with Roodaka waiting for him to wake up. She gave him a proposal; if he leads the hordes, he can rule Metru Nui. Overpowered by his Hordika instinct, Vakama accepted her offer. He captures five of the six Rahaga, except Norik, and destroys much of the Great Temple, prompting the other Toa Hordika and Norik to quickly looking for Keetongus whereabouts by following the inscription translated before the attack: "Follow the falling tears to Ko-Metru until they reach the sky."

The five Toa Hordika and Norik follow the stream of tears to Ko-Metru, where they end up in a cave and meet Keetongu. Meanwhile, Vakama is made master of the Visorak hordes and prepares to capture the other Toa. The Toa and Norik request Keetongus help. He refuses, telling them that he could not start a battle on their behalf, but he could aid those loyal to the three virtues (unity, duty, and destiny), doing so being Keetongus sworn duty. When Keetongu also refuses Mataus request for the Toa to be changed back, Norik explains that in order to rescue Vakama and the Matoran, they must learn to live and fight as Hordika, not be rid of their monstrous forms. However, the Toas devotion to Vakama has touched the Rahi, so he agrees to join them in their fight.

Later, at the Coliseum, the final battle for Metru Nui begins. While the Toa distract the Visorak, Matau, who believed he was responsible for Vakamas turning, confronts him while Keetongu fights Sidorak and Roodaka. Matau tries to reason with Vakama as Keetongu is struck down by Roodaka, though the blast does not kill him, and Roodaka leaves Sidorak to be killed by the Rahi. Matau reminds Vakama about his duty as a Toa and his destiny to rescue the Matoran, returning Vakama to his senses.

Norik frees his fellow Rahaga and joins the Toa, but they are defeated by Roodakas Kahgarak and she demands them to give her their elemental powers. The five Toa fire their Rhotuka spinners, but have little effect on her. At this time, Vakama confronts her ready to fire his spinner, and Roodaka warns him all the assembled Visorak will destroy him and his friends if shes struck down. Vakama then orders all the Visorak to leave, telling them they were free of Roodakas power, backing his claim with the power Sidorak had given him as commander of the hordes. Now without any aid, Roodaka simply stands and allows Vakama to fire his shot, but Norik realises too late what would happen when it strikes the red heartstone in her breastplate. As Roodakas body falls, a crimson hand encloses her and teleports her away, leaving behind the stone. Vakama realizes that the stone was Makutas, carved from the same protodermis that the Toa sealed him with; as Norik regretfully explains, in destroying the stone by giving the last bit of elemental energy, Vakama had broken that seal and set the Makuta free, but Vakama is confident that they can stop him again.

The Toa then approach Keetongu, wishing him to change them back to their original forms. Keetongu was at first reluctant as he believed they were better off as Hordika, having gained control over their bestial sides. But Vakama persuaded him that it was their destiny to be Toa to guide the Matoran. Relenting to their request, Keetongu changes them back into Toa Metru. The scene changes, showing the Toa readying a fleet of airships which will take them to their new home of Mata Nui, named in honor of the Great Spirit. As they neared the Great Barrier, they notice the Makuta has indeed been freed, but Vakama is sure that Toa will always be there to fight back against him. At the movies end, Turaga Vakama finishes his story to Takanuva, Jaller and Hahli, telling them that they must find their own destiny.

==Production==
Nathan Furst composed the music for the movie, and director David Molina said that watching the movie with the music was an "amazing experience", making the story "suddenly alive and emotional".  David Molina and Terry Shakespeare also worked as Art Directors on the film, Terry also worked as Visual Effects Supervisor.

==Reception==
The computer-generated effects were praised by some critics, stating that they could be appreciated even though the film was geared toward teenagers and young people.  The DVD release was noted for its good quality audio and video but meager extras. 

Bionicle was nominated for the Golden Reel Award for Best Sound Editing in Direct to Video by the Motion Pictures Sound Editors,  and the Annie Award for Best Home Entertainment Production. 

==Cast and characters==
 
*Alessandro Juliani as Toa Vakama http://www.imdb.com/title/tt0471588/fullcredits#cast 
*Brian Drummond as Toa Matau & Toa Onewa
*Tabitha St. Germain as Toa Nokama Paul Dobson as Toa Whenua & Sidorak
*Trevor Devall as Toa Nuju & Rahaga Iruini
*French Tickner as Rahaga Norik
*Kathleen Barr as Roodaka & Rahaga Gaaki
*Scott McNeil as Keetongu & Rahaga Bomonga
*Christopher Gaze as Turaga Vakama (narrator) 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 