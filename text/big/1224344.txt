Topper (film)
{{Infobox film
| name           = Topper
| image          = Topper Lobby Card.jpg
| image_size     = 225px
| caption        = Lobby card
| director       = Norman Z. McLeod
| producer       = Hal Roach
| based_on       = Thorne Smith (novel) Eric Hatch Eddie Moran
| starring       = Constance Bennett Cary Grant Roland Young  Billie Burke
| music          = Marvin Hatly
| cinematography = Norbert Brodine
| editing        = William H. Terhune
| released       =  
| distributor    = Metro-Goldwyn-Mayer
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $500,000 (proposed)   on TCM.com 
| gross          =
}}
Topper (1937 in film|1937) is an American comedy film starring Constance Bennett and Cary Grant which tells the story of a stuffy, stuck-in-his-ways man, Cosmo Topper (Roland Young) who is haunted by the ghosts of a fun-loving married couple.
 Eric Hatch, Jack Jevne and Eddie Moran from the novel by Thorne Smith. The movie was directed by Norman Z. McLeod, produced by Hal Roach, and distributed by Metro-Goldwyn-Mayer. The supporting cast includes Billie Burke and Eugene Pallette. Topper was a huge hit with film audiences in the summer of 1937; since Cary Grant had a percentage deal on the film, he made quite a bit of money on the films success.
 television series,  which premiered in 1953 and ran for two seasons, starring Leo G. Carroll, Robert Sterling and Anne Jeffreys. In 1973, a television pilot for a proposed new series Topper Returns (1973)  was produced, starring Roddy McDowall, Stefanie Powers and John Fink. A TV movie remake, Topper (1979)  was also produced starring Kate Jackson, Jack Warden and Andrew Stevens. Nearly Departed, a short-lived American TV series of the 1980s starring Eric Idle of Monty Python fame, was based on the same premise.
 colorized version, produced by Hal Roach Studios and Colorization Inc. 

==Plot==
George (Cary Grant) and Marion (Constance Bennett) Kerby are as rich as they are irresponsible. When George wrecks their classy sports car, they wake up from the accident as ghosts. Realizing they aren’t in heaven or hell because they’ve never been responsible enough to do good deeds or bad ones, they decide that freeing their old friend Cosmo Topper (Roland Young) from his regimented lifestyle will be their ticket into heaven.

Topper, a wealthy bank president, is trapped in a boring job. Worse still, Clara (Billie Burke), his social-climbing wife, seems to care only about nagging him and presenting  a respectable façade. On a whim, after George and Marion die, Topper buys George’s flashy sports car. Soon he meets the ghosts of his dead friends, and immediately they begin to liven up his dull life with drinking and dancing, flirting and fun.

The escapades lead quickly to Cosmo’s arrest, and the ensuing scandal alienates his wife Clara, however some of the people Clara would like to socialize with now because of Toppers scandal become interested in her and Topper. Cosmo moves out into a hotel with Marion who claims she is no longer married since she is dead. Clara fears she has lost Cosmo forever. The Toppers loyal butler suggests that she lighten up a bit; she decides he’s right and dons the lingerie and other attire of “a forward woman.” After Cosmo has a near-death experience and nearly joins George and Marion in the afterlife, Cosmo and Clara are happily reunited, and George and Marion, their good deed done, gladly depart for heaven.

==Cast==
*Constance Bennett as Marion Kerby
*Cary Grant as George Kerby
*Roland Young as Cosmo Topper
*Billie Burke as Mrs. Clara Topper
*Alan Mowbray as Wilkins, the butler/valet/house servant
*Eugene Pallette as Casey Arthur Lake as Elevator Boy, and later as a bell boy
*Hedda Hopper as Mrs. Grace Stuyvesant
*Virginia Sale as Miss Johnson
*Ward Bond (uncredited) as a cab driver in one scene
  
;Cast notes
*Songwriter and pianist     It was Carmichaels screen debut.  As the couple leave the bar, George (Grant) says, "(Good)night Hoagy!", and Carmichael replies "So long, see ya next time."

==Production==
After a long career producing comedy shorts, producer   

For Grants opposite number, Roach was interested in Jean Harlow, and as Topper W. C. Fields, but Harlow was too ill, and Fields turned down the offer. When Roach reached out to Constance Bennett, she was impressed enough with the property that she agreed to be paid less than her usual $40,000 fee. 

Topper was shot at Hal Roach Studios in    and location shooting took place at the entrance to the Bullocks department store on Wilshire Boulevard &ndash; as the entrance to the "Seabreeze Hotel"  &ndash; and at a location on San Rafael Avenue in Pasadena, California. 

==Reception==
Topper was a box-office hit, and gave a boost to the careers of all the lead actors, in particular Cary Grant, who moved from this film into a series of classic     Constance Bennett &ndash; who has previously been known as more of a "clothes-horse" than an actress &ndash; received very good notices, and Roach reunited her with director McLeod and screenwriters Jevne and Moran &ndash; was well as Billie Burke and Alan Mowbray &ndash; for 1938s Merrily We Live. 

==Awards and honors== Best Actor Best Sound, Recording for Elmer A. Raguse.   

American Film Institute Lists
*AFIs 100 Years... 100 Laughs - #60
*AFIs 10 Top 10 - Nominated Fantasy Film

==See also==
*List of ghost films

==References==
 

==External links==
* 
* 
*  at Turner Classic Movies
* 
* 
*Watch   at the Internet Archive
*Hemmings Daily blog:  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 