Samadhi (1950 film)
{{Infobox film
| name           = Samadhi
| image          = Samadhi50.jpg
| image_size     = 
| caption        = 
| director       = Ramesh Saigal
| producer       = 
| writer         = 
| narrator       = 
| starring       = Ashok Kumar, Nalini Jaywant
| music          = C. Ramchandra
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1950
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1950 Hindi cinema film directed by Ramesh Saigal. A box-office success, the film was the highest earning film of 1950, earning an approximate gross of Rs. 1,35,00,000 and a net gross of Rs. 75,00,000. 
 Samadhi (1972) directed by Prakash Mehra and starring Dharmendra, Asha Parekh, and Jaya Bhaduri.

==Cast==
*Ashok Kumar ...  Shekhar 
*Nalini Jaywant ...  Lilly DSouza 
*Kuldip Kaur ...  Dolly DSouza   Shyam ...  Suresh 
*Mubarak ...  Boss 
*S.L. Puri   
*Badri Prasad ...  Ram Prasad 
*Shashi Kapoor...   Pratap
*N. Kabir   
*Collins ...  Netaji

==References==
 

==External links==
*  

 
 
 


 