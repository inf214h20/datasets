L'Afrique en morceaux
{{Infobox film
| name           = LAfrique en morceaux 
| image          = 
| caption        = 
| director       = Jihan El-Tahri
| producer       = Arte France Canal + Capa
| writer         = 
| starring       = 
| distributor    = 
| released       = 2000
| runtime        = 104 minutes
| country        = France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Jihan El Tahri Peter Chappell
| cinematography = Peter Chappell
| editing        = Véronique Leroy
| music          = 
}}

LAfrique en morceaux  is a 2000 documentary film directed by Jihan El-Tahri.    

== Synopsis ==
April, 1994. Genocide in Rwanda. 800,000 dead. A catastrophe that upset the balance in the entire region. The Great Lakes region of Africa ended the year with a bloodbath. This documentary shows the intrigues, the dramatic effects, the treasons, the vengeances that prevailed over those years and whose only goal was to maintain or increase each faction’s area of influence. In just ten years, the population saw all their hopes vanish: The dream of an Africa in control of its own destiny, alimentary self-sufficiency, the end of interethnic conflicts.    

== References ==
 
 

 
 
 
 
 
 


 