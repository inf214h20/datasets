Young Aphrodites
{{Infobox film
 | name           = Young Aphrodites
 | caption        = 
 | image	=	Young Aphrodites FilmPoster.jpeg
| director       = Nikos Koundouros
 | producer       = Minos films
 | writer         = Kostas Sfikas Vassilis Vassilikos
| starring        = Kleopatra Rota Eleni Prokopiou Takis Emmanouil Vaggelis Ioannidis Zannino Anestis Vlachos Kostas Papakonstantinou Vasilis Kailas
 | music          = Giannis Markopoulos
 | cinematography = 
 | editing        = 
 | distributor    = 
 | released       = 1963
 | runtime        = 
 | awards         = 
 | budget         = 
 | country        = Greece
 | language       = Greek
 | cine_gr_id     = 703267}}
 1963 directed by Nikos Koundouros based on a script of Vassilis Vassilikos.

==Plot==
In 200 BC, a nomadic group of shepherds, in search of new pastures, leave the mountains to settle close to a fishing village. The women of the village hide and the only ones to venture out are Arta, the fishermans wife, and a twelve-year-old girl, Chloe.

Skymnos, a young shepherd, approaches Chloe who walks semi-naked around the rocks and the beach. Among the two children begins a tantalising game, which does not clearly end up with the couple having intercourse; as a sign of his affection, Skymnos catches a pelican for Chloe and mounts it on a gantry. On the other hand, although Arta initially rejects Tsakalos, she finally succumbs and the couple meets in a cave where Skymnos and Chloe can watch through a crack in the rock.

When the shepherds decide to leave, Skymnos refuses to follow them. Lykas, a mute teenage shepherd, finds Chloe and rapes her. At first Chloe struggles, but then apparently gives in to the sensations her first sexual experience is exposing her to. When Skymnos witnesses this scene, he unties the dead pelican, throws its corpse into the sea and allows himself to be swept away.

==Production notes==
Many of the actors were actually sheepherders. The film is based on the fable of Daphnis and Chloe.

==Awards== Berlin Film Festival) - Nikos Koundouros   
* International Federation of Film Critics (FIPRESCI) Prize

==References==
 

== External links ==
*  

 
 
 
 
 
 