The Best of Everything (film)
{{Infobox film
| name           = The Best of Everything
| image          = VM SY400 SX600 .jpg
| image_size     =
| caption        = VHS cover
| director       = Jean Negulesco
| producer       = Jerry Wald
| screenplay     = Edith Sommer Mann Rubin
| based on       =   Robert Evans Stephen Boyd Brian Aherne Martha Hyer Alfred Newman
| cinematography = William C. Mellor Robert Simpson
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 121 mins.
| country        = United States
| language       = English
| budget         = $1,965,000 
| gross          =
| preceded_by    =
| followed_by    =
}} Robert Evans, and Joan Crawford. The movie relates the professional careers and private lives of three women who share a small apartment in New York City and work together in a paperback publishing firm.
 novel of Alfred Newman wrote the musical score, the last under his longtime contract as Foxs musical director.  The film has been released on VHS and DVD.

==Plot==
Caroline Bender (Lange) is an ambitious young secretary in a publishing firm who, when jilted, finds consolation in the arms of editor Mike Rice (Boyd). Gregg Adams (Parker) is a typist and an aspiring actress romantically involved with stage director David Savage (Jourdan). When the director dumps her, she is devastated, falls from a fire escape while lurking outside his apartment, and dies.

April Morrison (Baker) winds up pregnant, and jumps from a car when her unborn infants father, Dexter Key (Evans), refuses to marry her and urges an abortion.

All three women are under the supervision of editor Amanda Farrow (Crawford), an exacting professional and a frustrated woman who marries, leaves the firm, and returns when she finds the simple life of home and marriage not to her liking.

==Cast==
*Hope Lange as Caroline Bender
*Stephen Boyd as Mike Rice
*Suzy Parker as Gregg Adams
*Martha Hyer as Barbara Lamont
*Diane Baker as April Morrison
*Brian Aherne as Fred Shalimar Robert Evans as Dexter Key
*Brett Halsey as Eddie Harris
*Donald Harron as Sidney Carter
*Louis Jourdan as David Savage
*Joan Crawford as Amanda Farrow

==Production== Robert Evans, Bob Wagner."  In further early casting considerations, Wald mentioned Joanne Woodward, Audrey Hepburn, Lauren Bacall and Margaret Truman.  In November 1958, Rona Jaffe officially sold the rights to her book for $100,000.  She did not want any part in preparing the screenplay, her intent was appearing in the film, saying: "I want to appear in the movie in a walk-on part. I would just appear briefly as one of the offices pool of stenographers." 

Martin Ritt was initially set to direct, but he was replaced by Jean Negulesco in January 1959, because reportedly, Ritt was upset with the casting of Suzy Parker. Ritt dismissed this rumor, saying the script wasnt his "cup of tea".  When she learned that Wald was sick, Parker agreed to do the film, reporting for work in January 1959.  She was persuaded to take the role in the summer of 1958, but a broken arm prohibited her from taking the role.  On playing a neurotic actress, Parker commented: "I know the type extremely well." 

During casting, several actors were considered, cast and replaced. In August 1958, Diane Varsi and Lee Remick were, along with Suzy Parker, attached to star in the film, but they both withdrew.  Remick was forced to leave production in early 1959 due to physical problems. In September 1958, Julia Meade signed on for the film, planning to make her screen debut.  She eventually did not appear in the film. In January 1959, the unknown actress Diane Hartman was cast as Barbara, but she was later replaced by Martha Hyer.  Jack Warden agreed to a co-starring role in March 1959, but he did not appear.  Less than a month later, Jean Peters was planning to make her comeback in this film.  Had Peters not been replaced, it would have been her fifth film under direction of Negulesco. Another actress cast in March 1959 without appearing in the definite film is June Blair, who was set to play one of the starring roles. 

Joan Crawford was cast in May 1959, ten days before shooting began.  Around the time, Crawford was almost broke.  She commented on her role: "Im on the screen only seven minutes. But I liked the part, and I want to do other movies and TV films if I can find what I want." Crawford had recently been elected to the Board of Directors of Pepsi-Cola and planned to spend a greater part of her time promoting the soft drink giant. 

==Music== Alfred Newman, with orchestrations by Earle Hagen and Herbert Spencer.  Additional development of Newmans themes were done by Cyril Mockridge for two scenes, and the songs "Again" and "Kiss Them for Me," both by Lionel Newman; and "Somethings Gotta Give (song)|Somethings Gotta Give" by Johnny Mercer are used as source music. 
{{cite journal
| title      = The Best of Everything
| others    = Alfred Newman
| date      = 2002
| url       =
| last      = Kendall
| first     = Lukas
| author2 =
| pages     =5, 10
| type    = CD insert notes
| journal = Film Score Monthly
| volume=4 |issue=11
| location  = Culver City, California, U.S.A.
| language  =
}} 

The title song for the film was composed by Newman, with lyrics by Sammy Cahn, and performed by Johnny Mathis. Producer Jerry Wald first showed interest in Mathis for the title song in August 1958. 

The music, as recorded for the motion picture, was released on cd in 2002, on Film Score Monthly records.

==Reception== Howard Thompson described the film as a "handsome but curiously unstimulating drama" and noted, "the casting is dandy" with kudos to Lange. Commenting on Joan Crawford, the critic described her performance as "suave trouping". Thompson pointed out that, "...for all its knowing air and chic appointments, the picture lumbers onto the plane of soap opera, under Mr. Negulescos reverential guidance." 

Paul V. Beckley ended his review in the New York Herald Tribune with: "...Miss Crawford comes near making the rest of the picture look like a distraction." 

Crawfords peripheral role in the film generated much criticism.  The men she was involved with romantically never appeared on the screen, and several of her scenes&nbsp;— including, according to co-star Diane Baker, a superbly acted drunk episode&nbsp;— were cut, reportedly due to the films length. 
 daytime soap ABC in 1970.

==Oscar nominations==
 Best Original Best Costumes-Color (Adele Palmer).    

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 