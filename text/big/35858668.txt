Invasion Quartet
{{Infobox film
| name = Invasion Quartet 
| image = Iqpos.jpg 
| caption = Original film poster by Ronald Searle
| producer = Ronald Kinnoch
| director = Jay Lewis
| writer = Jack Trevor Story John Briley Norman Collins (story)
| narrator = 
| starring = Bill Travers Spike Milligan
| music = Ron Goodwin
| cinematography = Geoffrey Faithfull Gerald Moss
| editing = Ernest Walter
| studio = 
| distributor = Metro Goldwyn Mayer
| released = September 1961
| runtime = 91 minutes
| country = United Kingdom
| language = English
| budget = 
}}
 The Guns of Navarone.  It was directed by Jay Lewis and starred Bill Travers and Spike Milligan.

==Plot==
Two wounded officers, one English and one French are deemed unfit and surplus to requirements.  They leave their hospital and together with an explosives expert suffering from mental illness and a Colonel thought too old to serve in the Army make their way to France to destroy a long range German artillery piece.
 Peter King and Pte Leslie Cuthbertson.

==Cast==
* Bill Travers as Freddie Oppenheimer
* Spike Milligan as Godfrey Pringle 
* Grégoire Aslan as Debrie 
* John Le Mesurier as Colonel
* Thorley Walters as Cummings 
* Maurice Denham as Dr. Barker
* Thelma Ruby as Matron 
* Millicent Martin as Kay 
* Cyril Luckham as Col. Harbottle 
* William Mervyn as Naval Officer
* Peter Swanwick as Gun Commander

==Reception==
According to MGM records, the film made a loss of $119,000.  . 

==See also==
*Two Men Went to War

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 