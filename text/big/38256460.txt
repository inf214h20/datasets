The Boxing Girls of Kabul
 
The Boxing Girls of Kabul is a 2012 Canadian documentary film directed by Ariel Nasr which follows young women boxers and their coach, Sabir Sharifi, at Afghanistan’s female boxing academy, as these athletes face harassment and threats in their efforts to represent their country in international competition and attempt to qualify for the 2012 Olympic Games.      

Training takes place at Ghazi Stadium, Afghanistan’s national stadium, which had previously been the site of executions by the Taliban.   

The 52-minute documentary was produced by Annette Clarke for the National Film Board of Canada.  Julia Kent composed music for the film. 

==Festivals and awards==
The Boxing Girls of Kabul received the award for Best Documentary at the Viewfinders International Film Festival for Youth in Halifax (April 17 to 21 2012), the Inspirit Foundation Pluralism Prize at Hot Docs (April 26 to May 6, 2012) as well as an Honourable Mention for the Colin Low Award at the DOXA Documentary Film Festival in Vancouver (May 4 to 13 2012). 

In January 2013, the film was nominated for best short documentary at the 1st Canadian Screen Awards.   

==Distribution== DLA for Latin America, France Televisions, DBS in Israel, the Korean Broadcasting System and Japans NHK. 

==See also==
*Buzkashi Boys, a 2012 short drama produced by Nasr, nominated for the Academy Award for Best Live Action Short Film.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 
 