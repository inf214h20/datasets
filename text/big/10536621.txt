Place des Cordeliers à Lyon
{{Infobox film
| name           = Place des Cordeliers à Lyon 
| image          = Cinématographe Lumière.jpg
| image_size     = 
| caption        = The poster advertising the Lumière brothers cinematographe
| director       = Louis Lumière
| producer       = Louis Lumière
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Louis Lumière
| editing        = 
| distributor    = 
| released       = 1895 April 15, 2005 (re-release)
| runtime        = 40 seconds
| country        = France Silent
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1895 France|French short black-and-white silent documentary Louis Lumière.

The film formed part of the first commercial presentation of the Lumière Cinématographe on 28 December 1895 at the Salon Indien, Grand Café, 14 Boulevard des Capuchins, Paris. 

==Production== 35 mm aspect ratio of 1.33:1. 

==Plot==
The film has no plot as such but is instead a stationary camera positioned on the Place des Cordeliers in Lyon. The camera observes the traffic passing along the street, including people walking and a number of horses pulling carriages.

==Current status==
The existing footage of this film was edited into The Lumière Brothers First Films published in 1996. 

==References==
 

==External links==
*  (requires quicktime)
*  
*   on YouTube

 

 
 
 
 
 
 
 
 
 

 
 