Toorpu Velle Railu
{{Infobox film
| name           = Toorpu Velle Railu
| image          =
| image_size     =
| caption        = Bapu
| producer       =
| writer         = Mullapudi Venkata Ramana
| story          = R. Selvaraj
| narrator       = Mohan Jyothi Jyothi
| music          = S. P. Balasubramanyam
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1979
| runtime        =
| country        = India Telugu
| budget         =
}}
 Telugu film Bapu and Mohan and Jyoti. The film was a remake of Tamil film Kizhake Pogum Rail.

==Cast== Mohan
*Jyothi Jyothi

==Soundtrack==
The Music for the film was composed by the Playback singer S. P. Balasubramanyam and the lyrics were penned by Aarudhra & Jaladi Raja Rao
* Chuttu Chengavi Cheera (Lyrics: Aarudhra; Singer: S. P. Balasubramanyam)
* Emitidi Emitidi Edo Teliyanidi (Lyrics: Aarudhra; Singer: P. Susheela)
* Kanne Maa Chinnari (Singer: P. Susheela)
* Ko Ante Koyilamma Koko (Lyrics: Jaladi Raja Rao; Singer: S. P. Balasubramanyam)
* Neetibotlu Neetibotlu Paatala Kanneetibotlu (Lyrics: Aarudhra; Singer: S. P. Balasubramanyam)
* Sandepoddu Andalunna Chinnadee (Lyrics: Aarudhra; Singer: S. P. Balasubramanyam)
* Vasthade Naa Raaju (Singer: S. P. Sailaja)
* Veguchukka Podichindi (Lyrics:Aarudhra; Singer: S. P. Balasubramanyam)

==External links==
*  

 
 
 
 
 


 