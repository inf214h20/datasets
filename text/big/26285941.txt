Home to Danger
 
 
{{Infobox film
| name           = Home to Danger
| image          = "Home_to_Danger"_(1951).jpg
| image_size     = 
| caption        = 
| director       = Terence Fisher
| producer       = Lance Comfort
| writer         = Ian Stuart Black   Francis Edge   John Temple-Smith
| narrator       = 
| starring       = Guy Rolfe   Rona Anderson   Francis Lister   Stanley Baker
| music          = Malcolm Arnold
| cinematography = Reginald H. Wyer
| editing        = Francis Edge
| studio         = New World Pictures
| distributor    = Eros Films
| released       = August 1951
| runtime        = 66 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Home to Danger is a 1951 British crime film directed by Terence Fisher starring Guy Rolfe, Rona Anderson and Stanley Baker.  It was made at the Riverside Studios in Hammersmith as a supporting feature. 

==Synopsis==
A young woman returns to Britain following the death of her estranged, wealthy father who it is believed committed suicide. It is expected that the bulk of the estate will pass to his business partner. However, when the will is read out she is given most of the money as a gesture of reconciliation by her father. She clings to her belief that he did not kill himself and investigates the circumstances of his death. Before long, plots are being hatched to kill her.

==Cast==
*Guy Rolfe as Robert
*Rona Anderson as Barbara
*Francis Lister as Wainright
*Alan Wheatley as Hughes
*Bruce Belfrage as Solicitor Peter Jones as Lips Leonard
*Stanley Baker as Willie Dougan
*Dennis Harkin as Jimmy-The-One

==Critical reception==
Radio Times called the film a "standard whodunnit" ;   while Britmovie thought it a "tense murder-mystery b-movie."   

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2011.

==External links==
* 

 

 
 
 
 
 
 
 
 

 