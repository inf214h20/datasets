Farishtay
{{Infobox film
| name           = Farishtay
| image          = Farishtay 1991 film poster.jpg
| caption        = Film poster
| director       = Anil Sharma
| producer       = Sattee Shourie
| writer         = Shadb Kumar Anil Sharma Swapna Jayaprada
| music          = Bappi Lahiri
| cinematography = Anil Dhanda
| editing        = A.R. Rajendran
| distributor    = S.G.S Films
| released       =  
| runtime        = 197 minutes
| country        = India
| awards         =
| language       = Hindi
| budget         =  
| gross          = 
}}

Farishtay is a Bollywood film is directed by Anil Sharma.

==Plot==
Gayetri lives with two close male friends, who she considers her brothers by the names of Veeru (Dharmendra) and Dheeru (Vinod Khanna), who are small-time thieves, and con men, and are known to the local police. When Gayatri meets and falls in love with Police Inspector Arjun Thanghe (Rajinikanth), the duo are delighted and arrange her marriage with great pomp and ceremony. Arjun is then assigned duties to a distant village, which is facing oppression at the hands of Raja Jaichand (Sadashiv Amrapurkar), who refuses to accept the Indian Government nor even acknowledge it, and rules the region like a dictator. Arjun attempts to set things right, but is killed in the process, and Gayetri loses her mind. Gayatri does find her way to contact her brothers, and is horrified to find them in the employ of the person who has killed her husband, none other than Mr. Jaichand

== Cast  ==
{|class="wikitable"
|-
!Actor
!Character in the movie
|-
| Dharmendra
| Virendra Kumar Veeru
|-
| Vinod Khanna
| Dhirendra Kumar Dheeru
|-
| Sridevi
| Rasbhari
|-
| Rajinikanth
| Police Inspector Arjun Tange
|-
| Jaya Pradha
| Sheela
|- Swapna
| Gayatri
|-
| Sadashiv Amrapurkar
| Raja Jaichand
|-
| Kulbhushan Kharbanda
| Police Commissioner
|-
| Rajendra Nath
| Mohan Lal
|-
| A.K. Hangal
| Abdul
|-
| Beena Banerjee
| Mother of slain child
|-
| Guddi Maruti
| Pandits Wife
|-
| Bob Christo
| Terrorist Bob
|-
| Manik Irani
| Chedhi Ram
|-
| Viju Khote
| Guest Role
|-
| Jankidas
| Guest Role
|-
| Dev Kumar
| Guest Role
|-
| Yunus Parvez
| Guest Role
|-
| Tom Alter
| Guest Role
|-
| Narendra Nath
| Guest Role
|-
| Dinesh Hingoo
| Sheelas Father
|-
| Shammi
| Mausi
|-
| Anand Balraj
| Andy
|-
| Huma Khan
| Angoori
|}

==External links==
*  

 
 


 