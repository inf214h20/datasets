Dreaming Lips (1932 film)
{{Infobox film
| name           = Dreaming Lips 
| image          = 
| image_size     = 
| caption        = 
| director       = Paul Czinner 
| producer       = Marcel Hellman
| writer         = Henri Bernstein (play)   Carl Mayer   Paul Czinner
| narrator       = 
| starring       = Elisabeth Bergner   Rudolf Forster   Anton Edthofer   Margarethe Hruby
| music          =  Erich Schmidt
| cinematography = Jules Kruger   René Ribault
| studio         = Pathé-Natan   Matador-Film  
| distributor    = Bavaria Film
| released       = 13 September 1932
| runtime        = 95 minutes
| country        = France   Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German drama film directed by Paul Czinner  and starring Elisabeth Bergner, Rudolf Forster and Anton Edthofer. The film is based on the play Mélo by Henri Bernstein. As was common at the time, the film was a co-production (filmmaking)|co-production with a separate French-language version Mélo (film)|Mélo made.
 remade the was released in 1953, starring Maria Schell.

==Cast==
* Elisabeth Bergner as Gaby 
* Rudolf Forster as Michael Marsden 
* Anton Edthofer as Peter 
* Margarethe Hruby as Christine 
* Jaro Fürth as Arzt  Peter Krogeras Kind 
* Karl Hannemann as Impresario
* Ernst Stahl-Nachbaur as Polizist 
* Werner Pledath   
* Gustav Püttjer   
* Willi Schur

==References==
 

==Bibliography==
* Hake, Sabine. German National Cinema. Routledge, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 