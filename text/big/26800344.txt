Crimes of the Past
{{multiple issues|
 
 
}}

{{Infobox film
| name = Crimes of the Past
| image = Crimes of the Past.jpg
| caption = 
| director = Garrett Bennett
| producer = Lenville ODonnell Victor Kepler Steve Edministon
| writer = Steve Edministon
| starring = David Rasche Elisabeth Röhm Chad Lindberg Eric Roberts
| music = Kyle Porter
| cinematography = Julio Riberyo
| editing = Bryan Gunnar Cole Liza McDonald
| distributor = MarVista Entertainment
| released =  
| runtime =
| country = United States
| language = English
| budget = 
| gross =  
 |publisher= 
 |accessdate= 
}}
Crimes of the Past, also known as The Spy and the Sparrow, is a 2009 thriller film starring David Rasche and Eric Roberts.

==Plot==
Retired CIA Agent Thomas Sparrow (David Rasche) retires and returns home to Seattle to try to reconnect with his long-lost daughter (Elisabeth Röhm), but a former colleague (Eric Roberts) complicates things by coming back into Sparrows life.

==Cast==
* David Rasche as Thomas Sparrow
* Elisabeth Röhm as Josephine Sparrow
* Eric Roberts as Robert Byrne
* Chad Lindberg as Kidd Bangs
* John Aylward as Clay Covington
* Cynthia Geary as Agent Cotton

==Production== Washington from  February 16 - March 14, 2007, and opened up to mixed to reviews. Stephen Farber of the Hollywood Reporter said "While it lacks major marquee names and doesnt quite hit a home run, it is an engrossing, superbly acted movie that will please audiences who manage to catch it.

==External links==
*  
*  
*   from The Hollywood Reporter

 
 
 
 
 

 