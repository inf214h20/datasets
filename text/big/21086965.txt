Never Say Goodbye (1946 film)
{{Infobox Film
| name           = Never Say Goodbye
| image          = Neversaygoodbye1946.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = James V. Kern William Jacobs
| writer         = I.A.L. Diamond James V. Kern based on = adaptation by Lewis R. Foster story by Ben Barzman Norma Barzman
| starring       = Errol Flynn Eleanor Parker
| music          = Friedrich Hollaender
| cinematography = Arthur Edeson
| editing        = Folmar Blangsted
| distributor    = Warner Bros.
| released       = November 9, 1946
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 1,180,998 admissions (France)  £116,821 (England)  
| preceded_by    = 
| followed_by    = 
}}
 1946 romantic comedy film about a divorcing couple and the daughter who works to bring them back together. It was Errol Flynns first purely comedic role since Fours a Crowd (1938), although Footsteps in the Dark (1941) had been a screwball comedy.

==Plot summary==
Divorced New York couple Phil and Ellen Gayley each buy a winter coat for their seven-year-old daughter Phillippa, known as "Flip". Flip has spent the last six months with her father but is about to move in with her mother.

Phil asks Ellen to dinner to attempt a reconciliation. While there, model Nancy Graham sees Phil and assumes he is there to see her. Phil tries to juggle both women but Ellen busts him and leaves.

On Christmas Eve, Phil dresses up as Senta Claus in order to sneak into Ellens apartment and see his daughter. Ellen assumes he is her divorce lawyer, Rex De Vallon, who earlier agreed to pay Santa. When the real Rex arrives, Phil locks him in the bathroom and a fight ensues. Ellen then insists Phil stay away from Flip for the next six months.

Phil manages to persuade Ellen and Flip go away together to a rural cabin in Connecticut that is owned by his friend, Jack Gordon. However Jack turns up with his girlfriend Nancy, ruining the trip.

Meanwhile, Flip has been writing letters to Fenwick Lonkowski, a Marine, pretending to be older than she is, and sent him a picture of Ellen instead of one of herself. Fenwick arrives to have lunch with Flip and assumes Ellen is her; Ellen decides to flirt with him in order to get revenge on Phil.

Eventually Phil tells Fenwick that Flip wrote the letters. When Fenwick learns how much Flip wants her parents to reunite, he decides to help her. Fenwick takes Flip to Luigis, and she refuses to return unless her parents make up. Ellen finally agrees to take Phil back, and Fenwick consoles himself with Luigis hat check girl. 

==Cast==
* Errol Flynn as Phil Gayley
* Eleanor Parker as Ellen Gayley
* Lucile Watson as Mrs. Hamilton
* S. Z. Sakall as Luigi
* Forrest Tucker as Corporal Fenwick Lonkowski Donald Woods as Rex DeVallon
* Peggy Knudsen as Nancy Graham
*Tom DAndrea as Jack Gordon
* Hattie McDaniel as Cozy
*Patti Brady as Phillippa "Flip" Gayley

==Production==
===Development===
The film was originally known as Dont Ever Leave Me.It was an original story by Norma and Ben Barzman and was purchased by Warner Bros in June 1944 as a vehicle for Claire Foley, who had appeared in the play Janie (play)|Janie which had just been acquired by Warners for filming. Jesse L. Lasky was assigned to produce. Of Local Origin
New York Times (1923-Current file)   06 June 1944: 14.   Looking at Hollywood
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   15 June 1944: 20.  

Then in September it was announced William Jacobs would produce instead. Of Local Origin
New York Times (1923-Current file)   13 Sep 1944: 17.   

The project remained in development until June 1945, when it was in an article announced Errol Flynn would star. Flynn had been set to star in two action films, The Adventures of Don Juan and The Frontiersman, but both had been postponed. (Don Juan was shot some years later; The Frontiersman - postponed because "of the wartime travel problem, many location sequences being necessary for the story" 19th Century Mystery Yarn To Be Filmed: Opening Tonight
The Christian Science Monitor (1908-Current file)   18 June 1945: 5.   - was never made.) The article mentioned that the plot of Dont Ever Leave Me was about a young girl who sends a photo of her widowed mother to a servicemen, which was also the plot of another film going to be made at Columbia around this time, Dear Mr Private. SCREEN NEWS: WARNERS TO STAR FLYNN IN DONT EVER LEAVE ME OF LOCAL ORIGIN
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   14 June 1945: 23.  James Kern was attached to direct. Looking at Hollywood
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   23 June 1945: 13.  
===Shooting===
Eleanor Parker was allocated the female lead opposite Flynn. Newcomer Patti Brady was given the role of their daughter. SCREEN NEWS: RKO Acquires Rights to Molnar Play, Lawyer Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   10 July 1945: 8.  

In July 1945 the title was changed to Never Say Goodbye. Screen News
The Christian Science Monitor (1908-Current file)   16 July 1945: 4.  

Forrest Tucker was borrowed from Columbia to play his role. FOX TO STAR BOYER IN GAMBLING FILM: Any Number Can Play to Be Based on Novel by Heth-- John L. Held Over Here
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   07 Aug 1945: 17.   He later signed a long term contract with Warners. Christopher Blake Wyman Probability
Schallert, Edwin. Los Angeles Times (1923-Current File)   19 Feb 1947: A3.  

Filming took place in August 1945.

For the scene in which Phil puts on a "tough guy" front to intimidate Fenwick, Humphrey Bogart (who went uncredited) overdubbed Flynns dialogue.  Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 145 
==Reception==
The film was a box office disappointment.  
===Critical===
The Los Angeles Times criticised the lack of originality in the comic set pieces ("director James V. Kern has had to borrow just about every situation in the book just to keep going" but said "Flynn goes through the motions with more good nature than you might expect" and that Parker was "lovely, unaffected". Flynn Takes Kidding With Good Grace
Scheuer, Philip K. Los Angeles Times (1923-Current File)   11 Dec 1946: 11.  

The New York Times said that "considering the interference provided him by the script, he   is handling the novel assignment in a moderately entertaining style... it is a silly little fable... Mr. Flynns unaccustomed performance is not likely to win him a palm as Hollywoods most accomplished farceur, but it does have amusing points—especially when he endeavors to pose as a tough guy with Humphrey Bogarts voice, and Eleanor Parker is remarkably attractive and encouraging as his obviously reluctant ex-wife. S. Z. Sakall, too, is amusing as a friendly restaurateur, but deliver us, please, from Patti Brady, a lisping youngster who plays the tottling child." 
==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 

 