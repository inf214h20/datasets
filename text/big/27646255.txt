Secret Reunion
 
{{Infobox film
| name           = Secret Reunion
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Uihyeongje
| mr             = Ŭihyŏngje}}
| image          = SecretReunion2010Poster.jpg
| caption        = South Korean Poster
| director       = Jang Hoon
| producer       = Yoo Jeong-hoon Song Myung-cheol Jang Won-seok
| screenplay     = Jang Min-seok Kim Joo-ho Choi Kwan-young Jang Hoon
| story          = Kim Ki-duk (uncredited)
| starring       =  
| music          = Noh Hyung-woo
| cinematography = Lee Mo-gae
| editing        = Nam Na-yeong
| studio         =  
| distributor    = Showbox
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| gross          =   
}} spy thriller film directed by Jang Hoon, and one of the highest grossing Korean films of 2010.
 National Intelligence Service (NIS) who falls from grace after failing to stop the assassination of a North Korean dissident. The sleeper cell responsible includes relative newcomer Ji-won played by Kang Dong-won, who gets marked as a traitor when the mission goes south. Six years later both men are working different jobs when they bump into each other and quickly form a bond.
 Rough Cut two years prior.    

== Plot ==
Ji-won is one of numerous North Korean undercover spies living in South Korea as ordinary citizens, until hes called to fulfill his mission: the assassination of Kim Jong-ils second cousin who wrote a book that the North Korean government deems as a great betrayal to the Fatherland. However, one of his fellow spies, Tae-soon, betrays his orders by switching his allegiance to South Korea, and Ji-won mistakenly becomes the target for both countries.
 National Intelligence Service (NIS) who tries to foil the hit. Despite having insider information, he doesnt notify his agency superiors, and only involves his team in the mission. Unable to prevent the assassination, and with the loss of his fellow agents in a gun fight, he becomes the sole scapegoat for the agency and gets Termination of employment|discharged. 

Six years later, Lee is now a private investigator and bounty hunter who locates runaway mail order brides, while Ji-won works in construction. Due to a coincidence, Lee gets saved from a mob by Ji-won; they both recognize each other instantly but keep it to themselves. With an ulterior motive for their previous unfinished business, Han-gyoo talks Ji-won into working for him and sharing living quarters - as Ji-won accepts Han-gyoos offer in order to spy on him, thinking that Han-gyoo is still an agent. While confronting Tae-soon for betraying their home country, Ji-won learns his true reasons behind it. He also learns that Lee was actually fired from NIS for not reporting the information to his superiors that couldve prevented the loss of both civilian and agent lives. As a result, he must make ends meet as a private investigator in returning runaway foreign brides to their husbands and shutting down a ring of corrupt businessmen that are ripping them off. During that time, a former associate of Han-gyoo reaches out for his help.

While secretly spying on Ji-won when hes out, Han-gyoo discovers Shadow, along with three other important people. He and Kyeong-nam confront Pastor Lee about his involvement. Pastor Lee reveals Ji-wons real name, Jo In-joon and that he has family back in North Korea. He hasnt seen his wife and daughter for seven years. When the North Korean government mistakenly labeled him a traitor, Ji-won is forced to hide away in South Korea. He was desperate and came to Pastor Lee for help in getting his family out of North Korea for a better life. It was then Han-gyoo finally realized what the apartment payment was meant for, his family. Before Pastor Lee could help Ji-won get his family out, the nuclear incident occurred and naturally the borders tightened. The next day, Ji-won comes across Tae-soons body and feels guilty for attacking him earlier. An agent from NIS recognizes him and orders Ji-won to turn himself in. 

When Lee discovers that the watch was fitted with a GPS system, he desperately follows Ji-won and tries to warn him to get rid of the watch. In the car, Shadow gives him one last chance to prove himself to their home country. The North Korean professor has betrayed them by revealing the secrets to NIS, thus they must get rid of him. Once killing him, Han-gyoo arrives and takes off Ji-wons watch, which gets him stabbed. It was then he learns that Shadow was the real traitor in his killing spree and realized that he was the one who kept him from being with his family. Betrayed, Ji-won attempts to fight him of, but is shot and Lee kills Shadow. NIS thanks him for his help, but Han-gyoo doesnt return as an agent and remains a PI. Encouraged by Ji-wons letter to visit his own family in England, he does and is surprised to see Ji-won and his family on the same flight.

== Cast ==
* Song Kang-ho as Lee Han-gyoo, a NIS agent   
* Kang Dong-won as Song Ji-won, a spy from North Korea
* Jeon Gook-hwan as Shadow
* Park Hyuk-kwon as Ko Kyeong-nam
* Yoon Hee-seok as Son Tae-soon
* Choi Jung-woo as NIS department head
* Ko Chang-seok as Vietnamese gang boss

== Reception == The Man from Nowhere. This puts it in the List of highest-grossing films in South Korea|all-time box office records of South Korea.

== References ==
 

== External links ==
*    
*   at Naver  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 