Escape from Planet Earth
 
 
{{Infobox film
| name = Escape from Planet Earth
| image = Escape from Planet Earth poster.jpg
| border = yes
| alt = US Advance Poster
| caption = Theatrical release poster
| director = Cal Brunker
| producer = {{plainlist|
* Donna Gigliotti
* Catherine Winder
* Luke Carroll
* Tony Leech
}}
| screenplay = {{plainlist|
* Bob Barlen
* Cal Brunker
}}
| story = {{plainlist|
* Tony Leech
* Cory Edwards
}}
| starring = {{plainlist|
* Rob Corddry
* Brendan Fraser
* Sarah Jessica Parker
* William Shatner
* Jessica Alba Craig Robinson
* George Lopez
* Jane Lynch
* Sofía Vergara
}}
| music = Aaron Zigman 
| cinematography = Matthew A. Ward
| editing = {{plainlist|
* Matthew Landon
* Scott Winlaw
}}
| studio = {{plainlist|
* Blue Yonder Films
* GRF Productions
* Jon Shestack Productions
* Kaleidoscope TWC
* Protocol Pictures
* Rainmaker Entertainment
}}
| distributor = {{plainlist|
* Alliance Films  (Canada) 
* The Weinstein Company  (United States) 
}}
| released =  
| runtime = 89 minutes   
| country = Canada United States
| language = English
| budget = $40 million 
| gross = $74.6 million 
}} computer animated Craig Robinson, George Lopez, Jane Lynch and Sofía Vergara. The film was released on February 15, 2013.  This was the first Rainmaker Entertainment film released in theaters. It was also Jessica Albas voice debut in an animated feature.

==Plot==
  anchorwoman Gabby Babblebrook (Sofía Vergara) while at BASA his brother Gary (Rob Corddry) works. Garys son Kip (Jonathan Morgan Heit) is a big fan of his uncle Scorch. Soon, Gary and Scorch receive a message from Lena Thackleman (Jessica Alba), the head of BASA, that Scorch will be sent to the "Dark Planet" (Earth) due to an SOS call. Scorch decides to go on the mission to the Dark Planet, but Gary strongly insists that he doesnt go because he is not serious. Also, no alien has ever returned from the dark planet. After further arguing, Gary finally says that he wont be helping Scorch and quits BASA before Scorch himself fires him. Gary then goes home to his wife Kira (Sarah Jessica Parker) and Kip only to find out that Scorch has already gone on the mission to the Dark Planet, while Kip is watching it on live TV in excitement.

Scorch arrives on Earth and lands in the desert and finds a 7-Eleven convenience store but mistakes an airdancer for a dying being. Scorch is then tranquilized and captured by General Shanker Saunderson (William Shatner), the malevolent general of the US Army, and is taken to "Area 51" where aliens from other planets are held. After finding that this has happened, Kip wants to go rescue Scorch but Gary discourages him and he himself doesnt want to go. Kip is angry and goes to his room. Gary goes to Kips room to apologize and admit that he is sorry about Scorch. When Gary flips Kips blanket open, instead of seeing Kip, he sees his dog and that Kips window is open. Knowing that Kip is going to try and save Scorch, he rushes to BASA with Kira wearing his rocket boots. They arrive to find that Kip is about to take off in a ship.

Gary manages to cancel the launch sequence, but he re-activates the sequence so Gary himself can rescue Scorch. He soon arrives on the Dark Planet. As soon as he arrives his ship immediately activates a self-destruct sequence, but Gary manages to get out. He then arrives at the same store that Scorch arrived at earlier. Gary goes inside there, but is spotted by two men inside named Hawk (Steve Zahn) and Hammer (Chris Parnell). Both Gary and the two men get freaked out by each other and try to hide from each other. After the two men realize that Gary is not a hostile alien, they offer him a Slurpee. Gary takes it but drinks it too fast and gets brain freeze. Afterwards, Shankers men break into the store and capture Gary, taking him to Area 51.
 Craig Robinson), Io (Jane Lynch), and Thurman (George Lopez), who tell Gary that various human technology has been invented by them for Shanker to rip off and sell to the world so he will release them from Area 51. With these technologies made by the aliens, Shanker had made deals with companies like Apple Inc., Facebook, and Google to distribute the technologies to them. Gary reunites with his brother, but is again annoyed by his conceited behavior. After a food fight in the cafeteria, the aliens make their way to the peace shield. Meanwhile, Lena captures Kira, who stayed at BASA to try to contact Gary in concern for his safety. Lena then reveals her plan to give a lifetime supply of blubonium to Shanker.

After Shanker reveals the blutonium, Gary unintentionally provokes Scorch into stealing it after stating its dangerous power and when being chased, Scorch destroys the blutonium, causing Shanker to freeze him. He orders Gary to fix the blutonium and reveals that hes going to destroy all of the alien planets with a laser ray using the blutonium. Shanker says that all aliens are hostile just because a grey alien spaceship killed his dad in 1947. Gary fixes the ray with help from his new friends, but Shanker goes back on his promise to release him and instead freezes him like his brother. The other aliens discover Shankers true intentions when he tries to destroy Baab with the laser ray and mutiny, knocking out Shankers henchmen. However, it is revealed that Gary did not put the machine together fully and it malfunctions, destroying itself before it can destroy Baab. With Gary and Scorch released from their icy prisons by the machine alongside the other frozen aliens, the brothers, Doc, Thurman, and Io escape Area 51 and eventually find Scorchs ship in a trailer park. With help from Hawk and Hammer (who live together in the park), Gary and his friends get into the spaceship and take off after helping to narrow its location down with a tornado scare.

Meanwhile, back on Baab, Kip frees his mother, who stops and subdues Lena after the latter took off with the blutonium shipment (and in the midst of the battle, learns Shanker was using her). US Air Force jets chases Garys saucer, but Kip guides his father through and manages to destroy the jets by making the spaceship pull up suddenly over a waterfall while the jets ram into it. However, Shanker (wearing Scorchs robotic suit that he wears on his adventures) uses a tractor beam to stop the ship, and freeze it in midair. Gary and Scorch jumps on and manages to get the suit off from Shanker which causes him to fall to his death. Suddenly, Scorch and Gary begin to plummet to their dooms but they and Shanker are rescued by the captured grey aliens (the ones who accidentally killed Shankers dad) who have their own plans to deal with Shanker.

Scorch, Gary, Doc, Thurman and Io return to Planet Baab where Gary is reunited with his family. Scorch is greeted as a hero, but gives the credit to his brother which the citizens of Baab celebrate. Scorch then embarks on his toughest mission yet: marrying Gabby Babblebrook. Hawk and Hammer are also present at the wedding as the airdancer is also patched up.

==Cast==
* Rob Corddry as Gary Supernova, Scorchs older brother and the head of mission control at BASA.
* Brendan Fraser as Scorch Supernova, Garys younger brother who is an arrogant but benevolent space pilot.
* Sarah Jessica Parker as Kira Supernova, Garys wife and Kips mother.
* William Shatner as General Shanker Saunderson, the villainous head of Area 51 whose father was accidentally killed by a grey alien spaceship.
** Joshua Rush as young Shanker
* Jessica Alba as Lena Thackleman, a BASAs no-nonsense chief whos later revealed to be an ally of Shanker. Craig Robinson radio show on his planet that got so famous that he invented the social networking service.
* George Lopez as Thurman, a 3-eyed slug-like alien with 4 arms who Gary befriends and becomes his cellmate at Area 51. He was a professor on his planet where he had invented touchscreen technology.
* Jane Lynch as Io, a giant cyclops-like alien with anger management issues who Gary befriends. She worked as a librarian on her planet until she got so mad at looking up stuff for her kind that she invented the web search engine. anchorwoman on Baab, Scorch fiancée and later wife.
* Jonathan Morgan Heit as Kip Supernova, Gary and Kiras adventurous son and Scorchs nephew.
* Ricky Gervais as James Bing, a sarcastic computer Artificial intelligence|AI.
* Steve Zahn as Hawk, a human working at 7-Eleven who Gary befriends.
* Chris Parnell as Hammer, a human working at 7-Eleven who Gary befriends.
* Paul Sheer as Cameraman
* Jason Simpson as Barry
* Kaitlin Olson as 3D Movie Girl
* Bob Bergen as 3D Movie Girl
* Daran Norris as Orientation Film Host Jim Ward Grey Alien #1
* Cooper Barnes as Grey Alien #2, Grey Alien #3
* Joe Sanfelipo as BASA Guard #1, a guard of BASA.
* Scott Beehner as BASA Guard #2, a guard of BASA Michael Dobson as Shanker Saundersons Dad, the father of Shanker who was accidentally killed by a grey alien spaceship.
* Tim Dadabo as Larry Longeyes
* Adrian Petriw as Snark Beast

==Development==
The film was in development at The Weinstein Company at least since 2007.  The film was first announced in a press release from The Weinstein Company, which announced that the film was in full production and also announced most of the cast.   

The films director is Cal Brunker, who previously worked as a storyboard designer on  . The film was originally set for release on February 14, 2013, but was pushed back to February 15, 2013, due to conflicting schedules. 

===Lawsuit===
Writer-director Tony Leech and film producer Brian Inerfeld sued The Weinstein Company, claiming they signed a deal whereby they were to receive at least 20 percent of Escapes adjusted gross profit, which they estimated would be worth close to $50 million in back end participation alone.    But the film languished in development, and the plaintiffs claimed that the Weinsteins repeatedly unlocked the script, forcing rewrites at least 17 times, which they say "eviscerated" the movies budget by keeping 200-plus animators on payroll.  With the film pushing its budget, the Weinsteins went outside for fresh capital. 

The Weinstein Company entered into a Funding and Security Agreement with JTM whereby the financiers agreed to provide new money and, in return, get 25 percent of the films gross receipts and 100 percent of all foreign gross receipts.  Leech and Inerfeld were upset, alleging that the agreement had mortgaged their own financial upside and said the Weinsteins advised them that if they wanted their past due money, they would have to agree to this arrangement.  Instead, Leech and Inerfeld went on the legal attack against TWC even claiming that they were paid $500,000 in hush money to keep the dispute quiet on the verge of the Weinsteins The Kings Speech Oscar victory in 2011.  As for JTM, the plaintiffs demanded a declaratory judgment that their contractual rights to share in the profits were superior to JTMs security interest in profits from the film. 

On February 15, 2013, the same day the film was released, in a document filed in the New York Supreme Court, lawyers for both sides filed a motion of discontinuance in the case, effectively ending it. No details of the settlement were made available but because the motion was filed “with prejudice” both sides would be paying their own legal costs. 

==Music==

===Soundtrack===
{{Infobox album  
| Name        = Escape from Planet Earth: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = February 19, 2013
| Recorded    = 2012
| Genre       = Film soundtrack
| Length      = 51:06
| Label       = Sony Classical
| Producer    =
| Reviews     =
}}

Escape from Planet Earth: Original Motion Picture Soundtrack is the soundtrack of the film which was released on February 19, 2013. 

====Track listing====
{{Track listing|collapsed=yes|
| music_credits   = yes
| total_length    = 51:06 Shooting Star
| music1          = Owl City
| length1         = 4:06
| title2          = Give Me Your Hand (Best Song Ever)
| music2          = The Ready Set
| length2         = 3:47
| title3          = Bom Bom
| music3          = Sam and the Womp
| length3         = 2:54
| title4          = Watch Your Back
| music4          = Zeazy Z
| length4         = 2:21
| title5          = Dollaz (Gotta Get It) (Bad Ass Remix)
| music5          = The Fresh Force Four
| length5         = 3:06
| title6          = Shine Supernova
| music6          = Cody Simpson
| length6         = 3:12
| title7          = What Matters Most
| music7          = Delta Rae
| length7         = 2:51
| title8          = George Valentin
| music8          = Brussels Philharmonic Orchestra
| length8         = 5:36
| title9          = Escape from Planet Earth Overture
| music9         = Aaron Zigman
| length9         = 4:57
| title10         = Tornado / Shanker Battles the Aliens
| music10         = Aaron Zigman
| length10        = 6:37
| title11         = Escape from Planet Earth Variation
| music11         = Aaron Zigman
| length11        = 3:53
| title12         = Shanker Targets Planet Baab / Gary and Aliens Escape
| music12         = Aaron Zigman
| length12        = 5:09
| title13         = Lets Go Home
| music13         = Aaron Zigman
| length13        = 2:37
}}

===Score===
{{Infobox album
| Name       = Escape from Planet Earth: Original Score By Aaron Zigman
| Type       = film
| Artist     = Aaron Zigman
| Cover      = 
| Released   = February 8, 2013
| Recorded   = 2012 Score
| Length     =  1:14:17
| Label      = Sony Classical
| Producer   = 
| Chronology = Aaron Zigman film scores
| Last album = Step Up Revolution (2012)
| This album = Escape from Planet Earth (2013)
| Next album = 
}}
Escape from Planet Earth: Original Score By Aaron Zigman is the soundtrack of the film scored by Aaron Zigman which was released on February 8, 2013. 

====Track listing====
*All songs written and composed by Aaron Zigman. 
{{tracklist|collapsed=yes|
| total_length = 1:14:17
| title1 = Escape from Planet Earth Overture
| length1 = 4:58
| title2 = Family Theme / Gary & Kira Save Kip
| length2 = 4:06
| title3 = The Peace Shield
| length3 = 2:52
| title4 = Tornado / Shanker Battles the Aliens
| length4 = 6:37
| title5 = Evil Lenas Theme / Dark Planet Press Conference
| length5 = 1:18
| title6 = Kira & Evil Lena / Gary Goes to Save Scorch
| length6 = 3:58
| title7 = Kira & Kip Caught / Evil Lena
| length7 = 1:30
| title8 = Scorch – Family Theme
| length8 = 2:33
| title9 = Step Away from the Bluebonium
| length9 = 3:07
| title10 = The Gnalarch Mission
| length10 = 2:26
| title11 = Scorch Returns to Planet Baab
| length11 = 1:57
| title12 = Scorch Me Baby
| length12 = 2:49
| title13 = Shanker Targets Planet Baab / Gary and Aliens Escape
| length13 = 5:09
| title14 = Fire Up the Ship
| length14 = 1:17
| title15 = Dark Planet Info
| length15 = 1:48
| title16 = Gary Captured / Area 51
| length16 = 4:06
| title17 = General & Evil Lena / Kip Saves Kira
| length17 = 4:16
| title18 = Scorch Goes to the Dark Planet
| length18 = 4:49
| title19 = Freezing Gun Fight
| length19 = 3:05
| title20 = Aliens Save the Day
| length20 = 1:59 
| title21 = Garys Cell
| length21 = 2:13
| title22 = Lets Go Home
| length22 = 2:36
| title23 = Main Title
| length23 = 0:53
| title24 = Escape from Planet Earth Variation
| length24 = 
}}

==Release==

===Critical response=== normalized rating out of 100 top reviews from mainstream critics, calculated a score of 35 based on 11 reviews.  Personal reactions to the film were mixed, with both positive and negative reviews.
 Newark Star-Ledger Time Out gave the film two out of five stars, saying, "The late Douglas Adams summed up Earth as "mostly harmless," a description that also applies to this eminently tolerable animated time-filler." 

Alonso Duralde of The Wrap gave the film a negative review, saying, "Its a bowl of warm water into which no one has bothered to place a bouillon cube. The kids in the theater with me never mustered a single laugh or gasp of excitement. Its plenty o nuttin."  Peter Howell of the Toronto Star gave the film two and a half stars out of four, saying, "No matter whether you call Escape from Planet Earth sincere homage or cynical thievery, it goes down well in its brisk 89 minutes."  Gregg Katzman of IGN gave the film a 4.5 out of 10, saying, "Escape From Planet Earth looks fantastic and is sporting some commendable voice acting, but these qualities cant overcome a stale script and significant lack of laughs. Unless you have a young kid that wants to see it, I just cant recommend this one at all."  Sheri Linden of the Los Angeles Times gave the film three out of five stars, saying, "It never discovers new worlds, but "Escape From Planet Earth is, in its genial way, escape enough."  Tom Russo of The Boston Globe gave the film two stars out of four, saying, "If "Escape" figures prominently into your February staycation plans, you won’t feel like you’ve thrown your money away, but the kids won’t still be buzzing about it when they get back to school, either."  Roger Moore of The Seattle Times gave the film two out of four stars, saying, "The animation is what sells Escape from Planet Earth, with rich, textured surfaces – check out the fishnet webbing on Scorch’s spacesuit, the paint worn off the hardware and the perfectly rendered 7-Eleven, where even the Slurpee (product placement in a cartoon?) shimmers like the real thing. But it’s not worth paying 3D prices".  Joe Leydon of Variety (magazine)|Variety gave the film a positive review, saying, "A lightweight, warp-speed, brightly colored trifle that should delight small children and sporadically amuse their parents." 

===Box office=== Safe Haven.  In its second weekend, the film went up to number three grossing an additional $10,682,037.  In its third weekend, the film dropped to number six grossing $6,619,827.  In its fourth weekend, the film dropped to number nine grossing $3,218,923. 

===Home media===
Escape from Planet Earth was released on DVD, Blu-ray and Blu-ray 3D on June 4, 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 