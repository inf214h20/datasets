Universal Soldier: Regeneration
{{Infobox film 
| name           = Universal Soldier: Regeneration
| image          = Universal Soldier Regeneration - Dolph Lundgren Jean-Claude Van Damme - DVD Case.jpg
| caption        = Poster
| director       = John Hyams
| producer       = Craig Baumgarten Mark Damon Moshe Diamant
| writer         = Victor Ostrovsky
| Based on       = Characters created by Richard Rothstein Christopher Leitch and Dean Devlin Mike Pyle Corey Johnson Kerry Shale Aki Avni 
| music          = Kris Hill Michael Krassner
| cinematography = Peter Hyams
| editing        = Jason Gallagher John Hyams
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $9 million 
| gross          = $844,447  
}} 
Universal Soldier: Regeneration (also known in some countries as Universal Soldier: A New Beginning) is a 2009 action film being the third theatrically released film in the  .
 MMA fighter Mike Pyle directly to video in the United States and other parts of the world. The film has received average to mixed reviews, but it has since developed a notable cult following.

== Plot ==
A group of terrorists led by Commander Topov (Zahary Baharov) kidnap the Ukrainian prime ministers son and daughter and hold them hostage, demanding the release of their imprisoned comrades within 72 hours. In addition, they have taken over the crippled Chernobyl Nuclear Power Plant and threaten to detonate it if their demands are not met. It is revealed that among the ranks of the terrorists is an experimental Next-Generation UniSol (NGU) (Andrei Arlovski), who was smuggled in by rogue scientist Dr. Colin (Kerry Shale). U.S. forces join up with the Ukrainian army at the plant, but quickly retreat when the NGU slaughters most of them effortlessly.  Dr. Porter (Garry Cooper), Dr. Colins former colleague on the Universal Soldier program, revives four UniSols to take down the NGU, but they are systematically eliminated.

Former UniSol  ) - Deverauxs nemesis - who quickly kills Commander Topov. However, Dr. Colin never considered Scotts mental instability, and he is killed by his own creation. Scott then reactivates the bomb before heading out to hunt the children.
 Mike Pyle) is sent in to infiltrate the plant and rescue the prime ministers children. He is successful in locating them and leads them toward safety. On their way out, they encounter the NGU. The children flee as Burke tries in vain to hold off the NGU, who stabs him to death after a brutal fight.

With 30 minutes remaining on the bombs timer, a re-conditioned Deveraux is geared up and sent to the plant, where he kills every terrorist he encounters. He searches the buildings and finds the children cornered by Scott.  Scott, who has distorted memories of Deveraux, is about to kill the children when Deveraux attacks and a grueling fight ensues. In the end, Deveraux impales Scott on the forehead with a lead pipe and fires a shotgun through it, blowing his brains out.

As Deveraux escorts the children to safety, they are attacked by the NGU. Deveraux and the NGU take the fight to the site of the bomb, with less than two minutes remaining. During the melee, Deveraux removes the detonator and jams it in the back of the NGUs uniform as they both jump out of the reactor chamber. NGU pulls the detonator off his back as it explodes, taking him with it. U.S. soldiers quickly arrive on the scene and tend to the children as Deveraux leaves. Burkes body is placed in a black bag and taken away, as well as recovered pieces of the NGU.

In Langley, Virginia, Burkes body is shown stored in a cryogenic chamber as a new UniSol, along with multiple clones made of him.

== Cast ==
* Jean-Claude Van Damme as Luc Deveraux
* Dolph Lundgren as Andrew Scott
* Andrei Arlovski as NGU Mike Pyle as Capt. Kevin Burke
* Garry Cooper as Dr. Porter  Corey Johnson as Coby
* Kerry Shale as Dr. Colin 
* Aki Avni as General Boris
* Emily Joyce as Dr. Sandra Fleming
* Yonko Dimitrov as Dimitri
* Violeta Markovska as Ivana
* John Laskowski as Captain
* Zahari Baharov as Commander Vasily Topov
* Kristopher Van Varenberg as Miles Jon Foo as UniSol 2

== Production ==
In a phone interview between director John Hyams and the Van Damme fanbase, the director commented:

 I am hoping that we are taking a film that was made a long time ago and we are now trying to present these characters in a contemporary context, and that means stylistically contemporary and to use my own taste, something that feels like it belongs in this era of film making. I also think that that film is a bit of a nostalgia piece, not only to late 80s but also early 90s. 
  The Thing, Carpenter would always have these great synth scores. Another piece we used was the group Tangerine Dream did a great score for the movie Sorcerer (film)|Sorcerer and taking a note from films like Blade Runner, movies influenced me and my idea of science fiction and action film making and take all the work I did in documentaries and the work I did within MMA and taking all those elements and putting them together to create a style of the film that I think is very different to the first two films, but I think fans of the first two will appreciate and maybe people who haven’t seen them will appreciate it.
 
I hope that we have breathed some life into the franchise. Some of the powers that be have certainly talked about doing another one already, whether that happens or not involves a lot of elements to come together. With what’s happening with the storylines, we could certainly go in a number of directions.    

== Release ==

On October 1, 2009, a surprise screening of the film took place at the Fantastic Fest in Austin, Texas, U.S.A.   

The films international theatrical release dates are as follows: in Israel on January 7, 2010, followed by the Philippines on January 8, followed by both Bahrain and the United Arab Emirates on January 27, both Malaysia and Singapore on January 28. A month later, it was released in Lebanon on March 25, 2010, Jordan on March 31, 2010 and Japan on June 26, 2010.

The film was released directly on DVD and Blu-ray Disc|Blu-ray on February 2 in the United States, February 9 in Brazil, April 5 in the United Kingdom, and May 4 in France and Germany.

== Box office ==
The film has been mostly released directly to DVD / Blu-ray in the US and Europe, as well and the following figures do not include theatrical box office reports from important territories such as Israel, Japan or South Korea. As of April 7, 2010, the film has grossed $844,447 in United Arab Emirates, Singapore, Italy, Lebanon, Malaysia and the Philippines.    

== Reception ==
Since its release, the film has received better than average reviews for a straight-to-DVD franchise sequel. Dread Central gave it 3 out of 5 knives, saying "there is almost nothing but solid b-level action until the credits roll." 

== Sequel ==
It was announced in May 2010 that Van Damme and Lundgren would return for a fourth official installment.   will be the first in the series to be filmed in 3-D film|3-D. John Hyams also returned as director. 

== References ==
 

== External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 