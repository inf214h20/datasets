The One She Loved
{{Infobox film
| name           = The One She Loved
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = George Hennessy
| starring       = Henry B. Walthall
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by  D. W. Griffith. The film, by the Biograph Company, was shot in Fort Lee, New Jersey when many early film studios in Americas first motion picture industry were based there at the beginning of the 20th century.    

==Cast==
* Henry B. Walthall as The Husband
* Mary Pickford as The Wife
* Lionel Barrymore as The Neighbor
* Kate Bruce as The Neighbors Wife
* Gertrude Bambrick as The Stenographer
* Madge Kirby as The Nurse Harry Carey as The Neighbors Friend
* Lillian Gish
* Eldean Stuart as The Baby

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 