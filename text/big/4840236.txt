Symptomology of a Rock Band: The Case of Crash Test Dummies
 
{{Infobox film
| name           = Symptomology of a Rock Band: The Case of Crash Test Dummies
| image          = CTDSymptomology.jpg
| caption        = VHS cover
| music          = Crash Test Dummies
| cinematography =
| editing        =
| distributor    = Arista Records|Arista/BMG
| released       =  
| runtime        = 45 min
| country        = Canada
| awards         = English
| budget         =
}}
Symptomology of a Rock Band: The Case of Crash Test Dummies (1994, Arista Records|Arista/BMG) is a 45-minute video directed by Kris Lefcoe, about the band Crash Test Dummies, done in the style of a medically oriented rockumentary. It contains the bands music videos interspersed with footage of the band and commentary by various pseudo-professionals.

The video is currently out of print.

==Video Listing==
{| class="wikitable"
|- bgcolor="#FFFFFF"
! width="200" rowspan="1"| Video
! width="150" rowspan="1"| Director
! width="75" rowspan="1"| Date
|-
|align="left"|1. "Supermans Song" Dale Heslip 1991
|-
|align="left"|2. "Afternoons & Coffeespoons" Tim Hamilton 1994
|-
|align="left"|3. "Mmm Mmm Mmm Mmm" Dale Heslip 1993
|- God Shuffled His Feet" Tim Hamilton 1994
|-
|align="left"|5. "Swimming In Your Ocean" Tim Hamilton 1994
|-
|- bgcolor="#FFFFFF"
|}

 

 
 
 
 
 
 
 
 
 
 


 