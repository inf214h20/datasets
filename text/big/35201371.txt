Kudiyirundha Koyil
{{Infobox film
| name           = KUDIYIRUNDHA KOYIL
| image          = Kudiyirundha Koyil.jpg
| image_size     =
| caption        =
| director       = K. Shankar
| producer       = T. S. Raja Sunderasan
| writer         = Swornam
| narrator       = Rajashree Pandari Bai M. N. Nambiar Nagesh V. K. Ramasamy (actor)|V. K. Ramasamy
| music          = M. S. Viswanathan
| cinematography = V. Ramamoorthy
| editing        = K. Shankar K. Narayanan
| studio         = Saravana Screens
| distributor    = Saravana Screens
| released       = 5 March 1968 
| runtime        = 166 mins
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Indian Tamil Tamil film directed by K. Shankar, starring M. G. Ramachandran, J. Jayalalitha, Rajasree, M. N. Nambiar, Nagesh (actor)|Nagesh, L. Vijayalakshmi.

==Plot==

Twins Sekhar (Master Sekhar) and Anand (Master Sekhar)  along with Mangalam (Pandari Bai), their mother witness their father (S.V.Ramdass) being killed by escaped prisoner Nagappan (M. N. Nambiar), and are grief-stricken. This prompts the family to leave to Chennai in order to pursue a new life. But when Sekhar gets down from the train to fetch some water, it leaves without him. Nagappan arrives and kidnaps Sekhar, who is raised as a criminal, unaware that the same man killed his father.

Years later, the now-grown up Sekhar (MGR) who calls himself "Babu", is an established criminal and is the most wanted man in the city. On the other hand, Anand (MGR) is a club-dancer and neither are aware of each others existence. During a police encounter, Babu is fatally wounded and seeks shelter in Mangalam s house, although he does not recognise her as his mother. He develops a soft corner for her, but when Nagappan (become Boobadhi, so-called respectable one owner of cabaret and employer of Babu) finds out about this, he tries to eradicate the kind-self out of Babu.

During another police encounter, Babu is again fatally wounded but becomes insane this time, also becoming amnesiac. D.I.G. Mogan (Major Sundarrajan), the local inspector then comes across Anand, and after seeing the striking resemblance between him and Babu, advices him to act as Babu in order to get all the secrets of the gang and have them arrested. Anand agrees, but later realises Babu is his brother. Anands girlfriend Jaya (Jayalalithaa) sees him having lot of money in a brief-case, and the police chase him. Unaware of the truth, she thinks he has turned into a criminal, and refuses to speak to him. Anands mother also comes to know of this, and becomes heartbroken. However, Mangalam and Jaya soon reconcile with Anand after learning of the truth, and they both also find out that Babu is his brother.

Babus girlfriend Asha (Rajasree|Rajashree) later finds out that Anand is impersonating Babu, but he surrenders to her and explains about Babus medical condition, subsequently revealing himself as Babus brother. Asha forgives him and the duo subsequently team up to defeat Nagappan and his men. Babu later escapes from the prison to kill Anand when learning about him, but is cornered by Jaya who tells him that Anand is his brother. Babu refuses to believe this, and kidnaps Jaya. He is later stopped by Mangalam, who makes him realise that he is her son and Anand is his brother, Babu finally realises it. Remembering that Nagappan killed his father, he teams up with Anand to defeat Nagappan, who is later arrested. Subsequently, Babu returns to being "Sekhar", and reunites with his family.

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as Anand and Babu alias Sekhar
|-
| Jayalalithaa || as Jaya, Anand s lover
|-
| Rajasree || as Asha, Babu s lover
|-
| L. Vijayalakshimi (Guest-star) || as The dancer, the partner of MGR-Anand in the song : "Aadaludan Paadalai..." 
|-
| Pandari Bai || as Mangalam, Anand and Babu/Sekhars mother
|-
| M. N. Nambiar || as Boobadhy, (Hotel Sorgam s owner) alias Nagappan
|-
| Major Sundarrajan || as The police officer, D.I.G. Mogan
|-
| V. K. Ramasamy (actor)|V. K. Ramasamy (Guest-star) || as Rav Bagavadhor Singaram, Jayas father
|-
| S. V. Ramadoss (Guest-star)  || as Ramnadhan, Anand and Babu/Sekhars father
|-
| Nellore Kandha Rav (Guest-star)  || as
|- Nagesh (Guest-star) || as Jayas elde brother and Rav Bagavadhor Singaram s son
|-
| S.M.Thirupadhiswamy || as The committed suicide receiver, after the song : "Aadaludan Paadalai..."
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==
 China Town.

The film was adapted from the 1962 Hindi film China Town, which starred Shammi Kapoor. 

The big Asian star of action movie, Jackie Chan interpreted in the same vein an entitled movie TWIN DRAGONS (Twin Dragons) of Tsui Hark and Ringo Lam, in 1992.

We can find the same weft in other taken(brought) out movie, previous year, in 1991 in DOUBLE IMPACT (Double Impact) of Sheldon Lettich, with another big star, Jean-Claude Van Damme.

But the one who inspired them was an immense comic, an American star Bob Hope, in MY FAVORITE SPY (My Favorite Spy) of 1951, a comedy realized by Norman Z. McLeod.

KUDIYIRUNDHA KOYIL has almost was called SANGAMAM.

In the credits of opening, the name of MGR appears only at the end, with the mentions of "Puraitchy Nadhigar - Makkal Thilagum" MGR in a double role. 

It was the case of another movie of MGR, PANATHOTHAM of 1963, another production of Saravana Films ((later, G.N.Velumani changed her in Saravana Screen), with the same director K. Shankar, the name of MGR joins that at the end of credits of opening.

The first name after the title of the movie (in this case, (KUDIYIRUNDHA KOYIL)  is the one of the big actress Jayalalithaa.
 Bangra dance sequence in the song "Aadaludan Padalai...", which became an instant hit among audiences. 
MGR had to practice for one month to dance in the song. 

" Tiger " Nellore Kantha Rao (native of Andhra-Pradesh) makes it a noticed appearance. Here, he comes to attack the MGR-Anand. What gives rise to a beautiful fight. He is stopped clear(net) by a blow of bottle at the top of the skull (with hair) by beautiful Rajasree-Asha. Physically, he reminds us another actor, Harold Sakata, the most famous villain Oddjob in the James Bond movie Goldfinger (film)|Goldfinger. We can see him in other big movie of MGR, the romantic comedy of the A.V.M. Productions, ANBE VAA of 1966. 

The " Puraitchy Thalaivar " MGR received rightly the award of "Better Actor Tamil" of year 1969 for this movie.

The film ran 100 days in Chennai and other major cities. 

==Soundtrack==
{{tracklist
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Neeyethan enakku
| extra1       = T. M. Soundararajan, P. Susheela Vaali
| length1      = 3.35
| title2       = Aadaludan Paadalai
| extra2       = T. M. Soundararajan, P. Susheela Vaali
| length2      = 6.07
| title3       = Ennai Theriyuma
| extra3       = T. M. Soundararajan Vaali
| length3      = 3.38
| title4       = Kunguma pottin Mangalam
| extra4       = T. M. Soundararajan, P.Susheela
| lyrics4      = Roshanara Begum
| length4      = 3.42
| title5       = Naan yar nee yar
| extra5       = T. M. Soundararajan
| lyrics5      = Pulamaipithan
| length5      = 3.19
| title6       = Thuluvadho ilamai
| extra6       = T. M. Soundararajan, L. R. Eswari Vaali
| length6      = 3.37
| title7       = Un vizhiyum en vaalum
| extra7       = T. M. Soundararajan, L. R. Eswari Vaali
| length7      = 3.17
| title8       = Aaduvathu udalukku
| extra8       = L. R. Eswari
| lyrics8      = Kannadasan
| length8      = 	
}}

==References==
 

==External links==
*  

 
 

 
 
 
 
 