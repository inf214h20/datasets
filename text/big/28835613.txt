Dagim
{{Infobox film
| name           = Dagim
| image          = Dagim.jpg
| alt            =  
| caption        = 
| director       = Joaquin Pedro Valdes
| producer       = Cinema One Originals
| writer         = 
| screenplay     = 
| story          = 
| starring       = Marc Abaya Martin del Rosario Rita Iringan Samuel Quintana Bembol Roco
| music          = 
| cinematography = Takeyuki Onishi
| editing        = Carlo Manatad
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = 
}}
Dagim is a Philippine horror film directed by Joaquin Pedro Valdes, starring Marc Abaya, Martin del Rosario, Rita Iringan and Samuel Quintana. The film is one of seven finalists in the Cinema One Originals for 2010. 

==Plot==
In the early nineties, Jun (Martin del Rosario) and Diego (Samuel Quintana) are brothers whose father, Elias, has disappeared. Juns best friend, Teban (Chase Vega), tries to convince the brothers that his policeman father, Tolome (Bembol Roco), hasnt given up trying to find Elias. With local authorities proving little help and faced with the very real possibility that their father may not be coming back, Jun and Diego take it upon themselves to find him.

The boys quest takes them to the foreboding mountain town of Hukayan where they meet the beautiful Lila (Rita Iringan), whose fragile beauty convinces the boys to escort her home, lest she fall prey to the brutal military patrols that roam the area. Lila lives in a village deep in the forest of Panimdim, part of a tribe led by her brother, the charismatic Pido (Marc Abaya).

Pido is a gracious host, introducing the brothers to the ways of the tribe, where members live a simple existence of brotherhood and equality, far from the corruption and greed that rule “civilization”. Jun is enamored by the possibility of living in Panimdim, little suspecting that the tribesmen’s pacifistic exteriors belie a terrible secret.  By the time Jun realizes that nothing is as it seems, it may already be too late, as Pido is already preparing to initiate the newest member of the tribe: Diego.

==Cast==
* Marc Abaya as Pido
* Martin del Rosario as Jun
* Rita Iringan as Lila
* Sam Quintana as Diego
* Daniel Fernando as Elias
* Francis Franzuela as Inggo
* Bembol Roco as Tolome

==Awards==
* Martin del Rosario won 2 times as "Breakthrough Performance by an Actor" in the 8th Golden Screen Awards & "New Movie Actor of the Year" in the 27th PMPC Star Awards for Movies.

==References==
 

==External links==
*  

 
 
 
 
 


 
 