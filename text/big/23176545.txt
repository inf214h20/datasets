More Than a Secretary
{{Infobox film
|image=More Than a Secretary poster.jpg
|caption=Swedish poster
|director=Alfred E. Green
|writer=Dale Van Every Lynn Starling
|story=Ethel Hill Aben Kandel based on= 
|producer=Everett Riskin (associate producer)  
|music=Dimitri Tiomkin (uncredited)  
|cinematography=Henry Freulich   
|editing=Al Clark    
|country=U.S.
|language=English
|studio=Columbia Pictures
|distributor=Columbia Pictures 
|runtime=77 min.
|released=December 24, 1936
}}

More Than a Secretary (1936) is an American romantic comedy film made by Columbia Pictures, directed by Alfred E. Green, and written by Dale Van Every and Lynn Starling. The story was adapted by Ethel Hill and Aben Kandel, based on the magazine story "Safari in Manhattan" by Matt Taylor.  It tells the story of a health magazine secretary who is in love with her boss.

==Plot==
Ms. Baldwin (Jean Arthur) and Ms. Davis (Ruth Donnelly) are owners and instructors of the Supreme Secretarial School.  Ms. Baldwin is fighting Spring Fever and daydreams while teaching her class. They take pride in turning out well trained secretaries. They are having problems teaching a secretarial student, named Maizie (Dorothea Kent) who cannot spell, take dictation or type. When the instructors ask her what  she is doing at the school, she replies with, “I‘m here for the same reason that every other smart girl’s here - to, uh, get a chance to meet nice men.” 

Ms. Davis tells her, “This doesn’t happen to be a matrimonial agency.”  Next, a former student drops by with a prospective student and informs the owners that she is getting married to a Junior Vice President (due to her job as a secretary).  This leads Ms. Baldwin to wonder if these young ladies are onto something.

Meanwhile, a client, Mr. Gilbert  (George Brent) who is the editor of Body and Brain magazine continues to fire secretarial graduates from the Supreme Secretarial School.  He calls the school and over the phone, Mr. Gilbert complains to Ms. Baldwin about inept secretaries. Ms. Baldwin covers the phone receiver and repeats what he is saying to Ms. Davis.  Ms. Baldwin says to Ms. Davis, “He wants to know what’s wrong with the modern woman?” Ms. Davis replies, “.. the modern man.”

Ms. Baldwin decides to go to Mr. Gilbert’s office to see what he expects of a secretary.  When they meet, he inadvertently thinks she is a secretary rather than the owner of the school and tells her to report to work in the morning.  She is immediately smitten with Mr. Gilbert and decides to work as his secretary.

==Cast==
* Jean Arthur as  Carol Baldwin
* George Brent as  Fred Gilbert
* Lionel Stander as  Ernest
* Ruth Donnelly as  Helen Davis Reginald Denny as  Bill Houston
* Dorothea Kent as  Maizie West
* Charles Halton as  Mr. Crosby
* Geraldine Hall as  Enid

==Home media==
The film is available on DVD as part of the Jean Arthur Comedy Collection, released by Sony Pictures Home Entertainment. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 