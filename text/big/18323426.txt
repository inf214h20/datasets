Motherland Hotel
{{Infobox film
| name           = Motherland Hotel
| image          = AnayurtOteli.jpg
| image size     =
| caption        = Film poster
| director       = Ömer Kavur
| producer       = Ömer Kavur Cengiz Ergun
| writer         = Screenplay:  Ömer Kavur Novel: Yusuf Atılgan
| narrator       =
| starring       = Macit Koper Şahika Tekand Serra Yılmaz Orhan Çağman
| music          = Atilla Özdemiroğlu
| cinematography = Orhan Oğuz
| editing        = Odak Film Alfa Film
| distributor    =
| released       = 1986
| runtime        = 101 minutes
| country        = Turkey
| language       = Turkish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Turkish film directed by Ömer Kavur. It is an adaptation of the novel of the same name by Yusuf Atılgan.

==Plot==
Zebercet owns a hotel in a small provincial town. He manages to keep it up with the help of one maid, a little girl who lives with him. One evening, one of the clients leaves the hotel, promising to return in a week. Haunted by the memory of the beautiful unknown, it leaves little to be gained by a little melancholy. Overwhelmed by his impulses, he refuses to take any clients, and closes the hotel.

==Cast==
*Macit Koper - Zebercet
*Şahika Tekand - The lady
*Serra Yılmaz - Ortalıkçı
*Orhan Çağman - Emekli Subay

==Awards==
*1987 Antalya Golden Orange Film Festival: Best  Director, Second Best Film
*1987 Istanbul Film Festival: Best Turkish Film
*Venice Film Festival: International Film Writers Federation Award

==External links==
* 

 
 
 
 
 
 


 
 