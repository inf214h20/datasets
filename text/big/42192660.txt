Oru Modhal Oru Kadhal
{{Infobox film
| name           = Oru Modhal Oru Kadhal
| director       = T.G. Keerthi Kumar
| producer       = {{Plain list| RR 
* S.A. Jawahar Basha
}}
| writer         = T.G. Keerthi Kumar
| starring       = {{Plain list|
* Vivek Rajagopal
* Megha Burman
* Pyramid Natarajan 
* Swaminathan
* Meera Krishnan
}}
| runtime        = 115 minutes
| music          = K.R. Kawin
| cinematography = Yugga H.
| editing        = Sathyaraj
| released       =  
| country        = India
| language       = Tamil
| budget         = 1.6 Crores
}} RR under the banner of Kandan Gearup Entertainment.This complete family entertainer film has Vivek Rajgopal who played the role of Karthik and Megha Burman, the lead heroine is a Mumbai based model who has already done ad films for many leading brands.
It is loosely based on the love story of director and his wife.

==Plot==
Oru modhal oru kadhal is a story of a guy (Vivek Rajagopal), who falls in love with a girl, and their trouble-filled journey from Chennai to Bangalore to Delhi giving troubles to his friends and family, portrayed hilariously.

==Cast==
* Vivek Rajgopal as Karthik
* Megha Burman as Anusha
* Pyramid Natarajan as Karthiks Father
* Meera Krishnan as Karthiks Mother
* Swaminathan as Shrinivasan
* Mamta Prasad as Anushas Aunt

==Soundtrack== Hindi song "Punjabi Paartha", sung by Shankar Mahadevan and Sunidhi Chauhan, with lyrics by Aditi K.K.
{{Infobox album 
| Name = Oru Modhal Oru Kadhal
| Type = Soundtrack
| Artist = K.R. Kawin
| Cover = 
| Released = January 29,2014
| Recorded = Producer =  Feature film soundtrack
| Length =
| Label = divo
}}

{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! Track# !! Song !! Singer(s) !! Duration
|-
| 1 || "Punjabiya Partha" || Shankar Mahadevan, Sunidhi Chauhan || 4:36
|-
| 2 || "Kalangathe Namba" || Velmurugan || 4:41
|-
| 3 || "Crystal Vanilla" || Vijay Prakash, Sharky  || 5:27
|-
| 4 || "Appana Police Copu" || Tippu, Chinna Ponnu  || 5:25
|-
| 5 || "Kadhal Devathai" || KR. Kawin || 3:32
|-
| 6 || "Punjabiya Partha (Club Mix)" || Shankar Mahadevan, Sunidhi Chauhan || 4:29
|-
| 7 || "OMOK Theme" || KR. Kawin || 0:52
|}
{{Album ratings
| rev1 = Raaga.com
| rev1Score =  
}}

==Reception==
===Critical Response===
The film got mixed reviews upon release. Manish Purohit of Sandhira.com gave 4 out of 5 stars saying "romance and comedy assured a lot of fun and will take you through a beautiful love journey".  Mohan K, of Galatta.com praised the movie calling it a casual romantic comedy and adding "some comedy sequences tickle the funny bone and keeps the viewers laughing".  Times of India gave 2/5 criticizing the script but adding "at the end you find yourself smiling on the simple and sweet journey of South to North".  Vivek Rajagopals performance was praised.
Raisa Nasreen of Book my Show gave 1/10 highly criticizing the script in all aspects. 

==References==
 

==External links==
*  
* http://www.nowrunning.com/movie/13300/tamil/oru-modhal-oru-kadhal/preview.htm
* http://play.raaga.com/tamil/album/Oru-Modhal-Oru-Kadhal-T0003999
* http://www.indiaglitz.com/channels/tamil/moviegallery/17695.html
* http://www.thehindu.com/features/cinema/cinema-reviews/audio-beat-oru-mothal-oru-kadhal-a-leaf-out-of-life/article5693272.ece
* http://www.raaga.com/channels/tamil/moviedetail.asp?mid=T0003999
* http://behindwoods.com/tamil-movies/oru-modhal-oru-kadhal/oru-modhal-oru-kadhal-trailer.html
* http://www.sify.com/movies/oru-modhal-oru-kadhal-audio-trailer-launch-imagegallery-kollywood-ob3rs0cjgce.html
* http://www.mixss.in/2014/01/oru-modhal-oru-kadhal-tamil-movie-songs.html
* http://eiway.com/2014/03/17/movie-review-oru-modhal-oru-kadhal
* http://www.iluvcinema.in/tamil/oru-modhal-oru-kadhal-movie-review/

 