The Night We Got the Bird
 
 
{{Infobox Film
| name           = The Night We Got the Bird
| image          = "The_Night_We_Got_the_Bird"_(1961).jpg
| image_size     = 
| caption        = 
| director       = Darcy Conyers
| producer       = Darcy Conyers   Brian Rix
| writer         = Darcy Conyers   Brian Rix   Tony Hilton 
| music          = Tommy Watt
| cinematography = S.D Onions
| editing        = Thelma Connell
| narrator       = 
| starring       = Brian Rix  Dora Bryan   Ronald Shiner   Liz Fraser British Lion
| released       =  
| runtime        = UK 82 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

The Night We Got the Bird is a 1961 British comedy film and a follow up to the 1959 film The Night We Dropped a Clanger, it was directed by Darcy Conyers and starring Brian Rix, Dora Bryan, Ronald Shiner and Irene Handl.  It is based on Basil Thomas play The Love Birds,  and was the last film Ronald Shiner made. 

==Plot==
When unscrupulous Brighton antiques dealer Cecil Gibson (Ronald Shiner) dies, his widow Julie (Dora Bryan) remarries, and she and new husband Bertie (Brian Rix) go off on honeymoon. But they are chased by a gangster because of a fake antique bed that the late Cecil pawned off for quick cash. In addition, it appears Cecil the dealer has reincarnated as a mouthy South American parrot, whose aim is to make married life difficult for his wife and Bertie.   

==Cast==
* Brian Rix - Bertie Skidmore
* Dora Bryan - Julie Skidmore
* Ronald Shiner - Cecil Gibson
* Leo Franklyn - Victor
* Liz Fraser - Fay
* Irene Handl - Ma
* Terry Scott - P. C. Lovejoy
* Reginald Beckwith - Chippendale Charlie
* John Le Mesurier - Court Clerk

==Critical reception==
*TV Guide wrote, "sophomoric British comedy...The script finds lots of excuses for people to lose their pants and make vulgar, inane sexual jokes that wouldnt amuse a 10-year-old."  
*Allmovie wrote, "several hilarious slapstick scenes involving chases or sexual encounters, as well as the more reserved wit found in caricatures like an inept magistrate, are all hallmarks of a typically British sense of humor here (shared by many non-Brits)."   Carry On films. Never mind, there are some priceless cameo performances from the supporting cast, including Robertson Hare as a dithering doctor, John le Mesurier as a long-suffering court clerk, Kynaston Reeves, hilarious as a deaf magistrate, and Terry Scott as a constable."  

== External links==
* 

==References==
 

 
 
 
 
 
 


 
 