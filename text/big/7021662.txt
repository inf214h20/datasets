Ringmaster (film)
{{Infobox Film
| name           = Ringmaster
| image          = Ringmaster-Poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Neil Abramson
| producer       = Gary W. Goldstein
| writer         = Jon Bernstein
| narrator       = 
| starring       = Jerry Springer Jaime Pressly William McNamara Molly Hagan John Capodice Wendy Raquel Robinson Ashley Holbrook Tangie Ambrose Nicki Micheaux Krista Tesreau Dawn Maxey Maximilliana with Michael Jai White and Michael Dudikoff
| music          = Kennard Ramsey
| cinematography = Russ Lyster	
| editing        = Suzanne Hines
| studio         = Motion Picture Corporation of America The Kushner-Locke Company
| distributor    = Artisan Entertainment
| released       = November 25, 1998
| runtime        = 90 min
| country        =   English
| budget         = $20 million
| gross          = $9,257,103
| preceded_by    = 
| followed_by    = 
}} 1998 United American comedy film starring Jerry Springer playing (essentially) himself as Jerry Farrelly, host of a show similar to his own, in this case called simply Jerry.   

==Plot== urban African black woman the show itself, detailing the difficulty Jerry faces in trying to come to terms with his rather dubious claim to fame, and the staffs utter amazement at the bizarre stories they must deal with.

A minor sub-plot involves a producer on the show who mistakenly picks up one of the guests, a self-proclaimed "crossdresser|man-by-day-woman-by-night."

==Cast==
*Jerry Springer as Jerry Farrelly
*Jaime Pressly as Angel Zorzak
*William McNamara as Troy Davenport
*Molly Hagan as Connie Zorzak
*John Capodice as Mel Riley
*Wendy Raquel Robinson as Starletta
*Ashley Holbrook as Willie
*Tangie Ambrose as Vonda Simmons
*Nicki Micheaux as Leshawnette
*Michael Jai White as Demond
*Krista Tesreau as Catherine Winicott
*Dawn Maxey as Natalie
*Maximilliana as Charlie/Claire
*Michael Dudikoff as Rusty

==Reception==

The film had a generally negative reception, with  ).

==Soundtrack==
 
A soundtrack containing hip hop music was released on March 23, 1999 by Lil Joe Records. It peaked at #80 on the Top R&B/Hip-Hop Albums.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 