Wild in the Country
 
{{Infobox film
| name           = Wild in the Country
| image          = WildintheCountryMoviePoster.jpg
| border         = yes
| caption        = Theatrical release poster Philip Dunne
| producer       = Jerry Wald
| screenplay     = Clifford Odets
| based on       =  
| starring       = {{Plainlist|
* Elvis Presley
* Hope Lange
* Tuesday Weld
* Millie Perkins
}}
| music          = Kenyon Hopkins
| cinematography = William C. Mellor
| editing        = Dorothy Spencer
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $2,975,000 
| gross          = 
}} Philip Dunne and starring Elvis Presley, Hope Lange, Tuesday Weld, and Millie Perkins. Based on the 1958 novel The Lost Country by J. R. Salamanca, the film is about a troubled young man from a dysfunctional family who pursues a literary career. The screenplay was written by playwright Clifford Odets.

==Plot==
The movie starts off with Glenn Tyler (Elvis Presley) getting into a fight with, and badly injuring, his drunken brother. A court releases him on probation into the care of his uncle in a small town, appointing Irene Sperry (Hope Lange) to give him psychological counselling. Marked as a trouble-maker, he is falsely suspected of various misdemeanors including an affair with Irene. Eventually shown to be innocent, he leaves to go to college and become a writer.

==Cast==
* Elvis Presley as Glenn Tyler
* Hope Lange as Irene Sperry
* Tuesday Weld as Mrs. Noreen Martin
* Millie Perkins as Betty Lee Parsons
* Rafer Johnson as Davis John Ireland as Phil Macy
* Gary Lockwood as Cliff Macy
* William Mims as Uncle Rolfe Braxton
* Raymond Greenleaf as Dr. Underwood 
* Christina Crawford as Monica George  
* Robin Raymond as Flossie
* Pat Buttram as Mr. Longstreet, the Mechanic (uncredited)
* Jason Robards Sr. as Judge Tom Parker (uncredited)
* Red West as Hank Tyler (uncredited)

==Background==
Wild in the Country was filmed on location in Napa Valley and in Hollywood Studios, although it is set in the Shenandoah Valley. The cast and crew created a public sensation in Napa for over two months of filming. The motel where many of the cast stayed, Casa Beliveau (since torn down), was so mobbed that Elvis had to be moved to the St. Helena home that was being used in the film as Irene Sperrys house, where Glenn Tyler went for counseling. Now a top-rated inn in Napa Valley and known as The Ink House, the room where Presley stayed for over two months can still be rented.
 Calistoga and St. Helena. Calistogas downtown main street was used as the hometown of Glenn Tylers uncle and his cousin. Other filming locations in Napa Valley include the Silverado Trail between Calistoga and St. Helena, the Cameo Cinema (then The Roxy), an old movie theater still in operation in downtown St. Helena where the dance hall scenes with Elvis and Tuesday Weld were filmed, and the hills and farmland behind what is now Whitehall Lane Winery just north of the town of Rutherford.

The Ink House was used as the house and backyard where a drunken Glenn Tyler tries to hose down Irene Sperry through the porch window, and the nearby 1885 barn is where Irene Sperry drives her DeSoto in to attempt suicide when she is so distraught over her suspected romance with Glenn and the scandal it has caused. In one scene, Betty Lee slaps Glenn. Millie Perkins suffered a broken arm while doing the scene, and before the film was released, the scene ended up being cut out of the movie.

This was Elvis last dramatic lead role until Charro! as his next film, Blue Hawaii, was his first big budget musical and was a box office sensation.  All his subsequent movies were largely formula musicals which were quite lucrative but never gave him the chance to develop his potential as a serious actor that was very apparent in Wild in the Country.

In the original script and rough cut of the film, Hope Langes character Irene Sperry succeeds in her suicide attempt.  However, preview audiences reacted negatively to it and the scene was redone in which Irene survives and sees Glenn off to college.

Presley began an off-screen romance with Hollywood "bad girl" Tuesday Weld but the relationship was short-lived after Colonel Tom Parker warned Presley against his involvement, fearful it would harm his image.  Elvis and Hope Lange also were quite taken with each other, but her separation from her husband did not result in a divorce until the next summer making her unavailable for a serious relationship.

Other notable members in the cast included Jason Robards, Sr., Christina Crawford (daughter of Joan Crawford), Pat Buttram and the legendary Rudd Weatherwax who trained the animals used in the movie.

==Soundtrack== producer Urban Lonely Man" and "Forget Me Never" left out of the film.
 Wild in the Country," was released on the very next single, catalogue 47-7880b on May 2, 1961, as the B-side of the No.5 hit "I Feel So Bad."  Both B-sides made the Billboard Hot 100|Billboard Hot 100 independently of their A-sides, "Lonely Man" peaking at No.32 and "Wild in the Country" at No.26.

The songs "In My Way" and "Forget Me Never" would be included on the 1965 anniversary compilation album Elvis for Everyone, while "I Slipped, I Stumbled, I Fell" appeared on the 1961 album Something for Everybody.

The soundtrack was re-released on the Follow that Dream collectors label with unreleased outtakes of all the songs.

===Track listing=== George Weiss, Hugo Peretti, Luigi Creatore) Fred Wise)
# "In My Way" (Ben Weisman, Fred Wise)
# "Husky Dusky Day" (a cappella duet with Hope Lange) (unknown recording date and location)

===Personnel===
* Elvis Presley – vocals, guitar
* The Jordanaires – background vocals
* Scotty Moore – electric guitars 
* Tiny Timbrell – acoustic guitars
* Jimmie Haskell – accordion
* Dudley Brooks – piano
* Meyer Rubin – double bass drums

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 