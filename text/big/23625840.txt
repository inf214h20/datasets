Eleventh Hour (animated short)
{{Infobox Hollywood cartoon|
| cartoon_name = Eleventh Hour Superman
| image = Eleventh_Hour_title.png
| caption = Title card from Eleventh Hour. Dan Gordon Carl Meyer Bill Turner
| animator = Willard Bowsky   William Henning
| voice_actor = Bud Collyer   Joan Alexander   Julian Noa   Jack Mercer
| musician = Sammy Timberg Dan Gordon   Seymour Kneitel   Izzy Sparber
| studio = Famous Studios
| distributor = Paramount Pictures
| release_date = November 20, 1942 (USA)
| color_process = Technicolor
| runtime = 8 min. (one reel)
| preceded_by = Showdown (1942 film)|Showdown (1942)
| followed_by = Destruction, Inc. (1942)
| movie_language = English
}}
Eleventh Hour (1942) is the twelfth of seventeen animated Technicolor short films based upon the DC Comics character Superman. Produced by Famous Studios, the cartoon was originally released to theaters by Paramount Pictures on November 20, 1942.

==Plot==
While Clark Kent and Lois Lane are prisoners in wartime Japan, Superman becomes a saboteur.

In the Japanese City of Yokohama the Eleventh Hour strikes and a ship is turned over. Superman escapes searchlights while sirens go off and goes through a window, putting a barred grille back in place. Lois asks if Clark is awake, to which he asks who could sleep through a racket like this. Lois says the racket has been happening every night since they have been interned. Clark says it may be sabotage, which Lois also hopes. She wonders if Superman is responsible. A guard tells them to stop talking.

A Japanese Official says the sabotage must stop at once. As the Eleventh Hour strikes, Clark looks at his watch and leaves the window, returning as Superman. He leaves the room by removing the grille and drags a ship over into the sea. Sabotage happens every night at the Eleventh Hour, and the Official again says the sabotage must stop. Lois sees Superman as he leaps between buildings. She says outside Clarks room that it is Superman, she just saw him, and the Japanese have a swell chance of catching him. However a guard covers her mouth from behind and drags her out. Notices are put up saying Warning! Superman One more act of Sabotage and the American Girl Reporter will be executed at once.

Superman sends another ship into the sea, but is buried under steel girders. Lois is taken out for execution with her hands tied. As Superman digs himself out she walks against the wall and is blindfolded. Superman sees the notice and is fired on, but leaps away. He shields Lois just as the bullets are fired, and leaps away with her. On a ship landing in America Lois is interviewed. She is asked by a reporter if Clark got away, but says he is still over there but Superman promised to look after him. As the Eleventh Hour strikes in Japan there is another explosion.

==Voice cast==
*Bud Collyer as Superman/Clark Kent (uncredited)
*Joan Alexander as Lois Lane (uncredited)
*Jack Mercer as Japanese Guard, Japanese Official, Reporter (uncredited)
*Jackson Beck as Narrator (uncredited)

==References==
 

==External links==
*  
*   at the Internet Archive
*  
*   on YouTube

 

 
 
 
 
 
 
 
 
 