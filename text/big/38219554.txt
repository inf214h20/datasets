The Second Mate (1928 film)
 
 
{{Infobox film
| name           = The Second Mate 
| image          =
| caption        =
| director       = J. Steven Edwards 
| producer       = H.B. Parkinson
| writer         = R.W. Rees (novel)   J. Steven Edwards David Dunbar  Cecil Barry   Lorna Duveen   Eric Hales
| music          = 
| cinematography = 
| editing        = 
| studio         = H.B. Parkinson Films
| distributor    = Pioneer Film Distributors 
| released       = 1928
| runtime        = 44 minutes 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent adventure David Dunbar, Cecil Barry and Lorna Duveen. It was made at Isleworth Studios as a quota quickie. A ship is rescued by the Captains daughter.

==Cast==
*  David Dunbar (actor)| David Dunbar as Jack Arkwright  
* Cecil Barry as Captain Bywater  
* Lorna Duveen as Ivy Bywater  
* Eric Hales as Captain Petrie  

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B film. British Film Institute, 2007.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 