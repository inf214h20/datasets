Viva Riva!
{{Infobox film
| name           = Viva Riva!
| image          = VivaRiva!2010Poster.jpg
| alt            = 
| caption        = US Film poster
| director       = Djo Tunda Wa Munga
| producer       =   
| writer         =  
| starring       =  
| music          = 
| cinematography = 
| editing        =  
| studio         = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Democratic Republic of the Congo French
| budget         = 
| gross          = 
}}
 Congolese Crime crime Thriller thriller film written & directed by Djo Tunda Wa Munga and starring Patsha Bay, Manie Malone, Fabrice Kwizera, Hoji Fortuna, Marlene Longage, Alex Herabo & Diplôme Amekindra. The film received 12 nominations and won 6 awards at the 7th Africa Movie Academy Awards, including the awards for Best Picture, Best Director, Best Cinematography & Best Production Design, a feat that made it the highest winning film in the history of the AMAAs till date.           Viva Riva! also won at the 2011 MTV Movie Awards for Best African Movie 

== Plot ==
The film features a gang war ignited by a fuel crisis in Democratic Republic of the Congo|Congo. 

==Cast==
*Patsha Bay - Riva
*Manie Malone - Nora
*Hoji Fortuna - Cesar
*Fabrice Kwizera - Jason
*Marlene Longage - the Commandant
*Alex Herabo
*Diplôme Amekindra

==Reception==
Reviews has been very positive, It currently holds an 85% fresh rating based on 55 reviews on Rotten Tomatoes with a general description that "Vibrant and violent, Viva Riva is a stylish, fast-paced crime drama.".  It has a 65% rating on Metacritic based on 16 reviews. 

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 