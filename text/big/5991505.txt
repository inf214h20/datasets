The Shadow (1994 film)
 
{{Infobox film
| name           = The Shadow
| image          = Shadowpost.jpg
| caption        = Theatrical poster
| director       = Russell Mulcahy
| producer       = Willi Baer Martin Bregman Michael Scott Bregman
| screenplay     = David Koepp
| based on       =  
| starring       = Alec Baldwin John Lone Penelope Ann Miller Ian McKellen Peter Boyle Jonathan Winters Tim Curry
| music          = Jerry Goldsmith
| basedon        = Characters Created by Walter B. Gibson 
| cinematography = Stephen H. Burum
| editing        = Peter Honess Beth Jochem Besterveld
| studio         = Bregman/Baer Productions, Inc.
| distributor    = Universal Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $48,063,435
}} the eponymous comic book character created by Walter B. Gibson in 1931. 

== Plot ==
The story begins in  -Ko (Mandarin Chinese for "Dark Eagle").  He is abducted from his palace by servants of the Tulku (voiced by Barry Dennen), a holy man who exhibits otherworldly powers and knows Cranstons identity. He informs Cranston that he is to become a force for good. Cranston objects but is silenced by the Kīla (Buddhism)|Phurba (voiced by Frank Welker), a mystical living triple-edged knife that assaults Cranston, wounding him. Cranston is unable to refuse and remains under the tutelage of the Tulku for seven years. He learns to "cloud mens minds," a form of mystical, psychic hypnosis that allows him to influence others thoughts and bend their perceptions so he cannot be seen, except for his shadow (since light itself cannot be deceived), hence his new alias.

Cranston returns to New York and resumes his previous life. No one is aware of his past in the East; he is seen as a shallow and opulent playboy. He operates as The Shadow, a vigilante who terrorizes the underworld. Citizens who are saved by The Shadow are recruited to be his agents, providing him with informants and specialists. The existence of The Shadow is regarded by the public as nothing more than an urban legend. But The Shadows secret is endangered when Cranston meets Margo Lane (Penelope Ann Miller), an eccentric socialite who is also a natural telepath. He is intrigued, but unable to continue seeing her as he cannot keep his thoughts from her.

Cranston is challenged by Shiwan Khan (John Lone), another student of the Tulku who possesses even sharper powers, but had successfully resisted redemption and hence had stayed evil. Khan is the last descendant of Genghis Khan and plans to fulfill his ancestors goal of world domination. He offers Cranston an alliance, sensing that bloodlust and a thirst for power still exist in his heart, but Cranston refuses. Cranston acquires a rare coin from Khan and learns that it is made of a metal called "bronzium" (an impure form of uranium) that theoretically can generate an explosion large enough to destroy a city. This suspicion is confirmed when he learns that Margos father Reinhardt (Ian McKellen), an atomic scientist working for the War Department, has vanished.
 hypnotizes Margo Lane and sends her to assassinate Cranston, hoping that Cranston will be forced to kill her, thus reawakening his darker side. Instead, Cranston breaks Khans hold on her, but she is now aware of his secret identity. Cranston prepares to rescue Reinhardt but is thwarted by several of Khans henchmen. The Shadow suffers another setback when he confronts Reinhardts former assistant, Farley Claymore (Tim Curry), who has joined Khans forces. Claymore traps The Shadow in a submersion tank, but Cranston escapes drowning by mentally summoning Margo. The Shadow learns of Khans hideout: the luxurious Hotel Monolith, a building in the middle of the city that Shiwan Khan, mastering the tulkus greatest secret, has rendered invisible; it appears to everyone else as an empty lot, but The Shadow sees it even through Khans mental clouding. Knowing that Khan has Reinhardt hostage and the completed atomic bomb in his possession, he infiltrates the hotel for a final showdown.

The Shadow fights his way through the hotel, killing Claymore and Khans warriors. He faces Khan but is subdued by the Phurba, sustaining multiple injuries until he realizes that only a peaceful mind can truly control the Phurba. Overcoming Khans command of the knife, he launches the Phurba into Khans torso. The injury breaks Khans concentration, freeing Reinhardt from his hypnotic state and rendering the hotel visible to everyone. The Shadow pursues Khan into the bowels of the building while Margo and Reinhardt disable the atomic bomb—and almost fail due to Reinhardts color blindness. The Shadow defeats Khan by psychically hurling a glass shard into his skull.

Khan awakens in a padded cell, confused as to how he got there. He discovers that his powers no longer work. He learns that the doctors saved his life by removing the part of his brain that harbored his psychic abilities. He demands to be set free, but is ignored along with the rants of the other inmates. Unknown to him, the doctor is an agent of The Shadow who has ensured that Khan is no longer a threat.

Now safe from Khan, Cranston gives in to his love for Margo, but duty calls soon after, and he promises to find her later that night. Margo asks how he will know where to find her, and Cranston reassures her, "Ill Know."

==Cast==
* Alec Baldwin as Lamont Cranston / The Shadow 
* John Lone as Shiwan Khan
* Penelope Ann Miller as Margo Lane
* Peter Boyle as Moses "Moe" Shrevnitz
* Ian McKellen as Dr. Reinhardt Lane
* Tim Curry as Farley Claymore 
* Jonathan Winters as Wainwright Barth
* Sab Shimono as Dr. Roy Tam
* Andre Gregory as Burbank
* James Hong as Li Peng
* Joseph Maher as Isaac Newboldt
* Max Wright as Berger
* Ethan Phillips as Nelson
* Frank Welker as the voice of Phurba
* Barry Dennen as the voice of Tulku (uncredited)

== Production ==
{{multiple image
 | direction = vertical
 | width     = 150
 | footer    = Orson Welles was the voice of The Shadow in the original radio show, upon which the 1994 film version was partially based, from September 1937 to October 1938.
 | image1    = Orson Welles 1937.jpg
 | alt1      = Publicity photograph of Orson Welles, dated 1937.
 | caption1  =
 | image2    = WellesShadow.jpg
 | alt2      = Promotional photograph of Orson Welles dressed as The Shadow, dated 1937 or 1938.
 | caption2  =
}}
Producer Martin Bregman bought the rights to The Shadow in 1982. Screenwriter David Koepp had listened to The Shadow radio show as a child when CBS radio re-ran it on Sunday nights. He was hired in 1990 to write a new draft and was able to find the right tone that the studio liked. {{cite news
 | last = Schwager
 | first = Jeff
 | coauthors =
 | title = Out of the Shadows
 | work = Moviemaker
 | pages =
 | language =
 | publisher =
 | date = August 13, 1994
 | url = http://www.moviemaker.com/magazine/editorial.php?id=213
 | accessdate =2007-04-16 }}  Bregman remembers, “Some of them were light, some of them were darker, and others were supposedly funnier – which they weren’t. It just didn’t work”. {{cite news
 | last = Peterson
 | first = Don E
 | coauthors =
 | title = The Shadow Takes Shape
 | work = Sci-Fi Entertainment
 | pages =
 | language =
 | publisher =
 | date = August 1994
 | url =
 | accessdate = }}  Koepps script relied predominantly on the pulp novels while taking the overall tone from the radio show with the actual plot originated by Koepp himself in consultation with Bregman.

In an attempt to differentiate the film from other superhero films of the time, Koepp focused on “the copy line, ‘Who knows what evil lurks in the hearts of men?’ and wondered how he knew what evil lurks in the hearts of men. And I decided that perhaps it was because he was uncomfortably familiar with the evil in his own heart”.  For Koepp, the film then became “a story of guilt and atonement”.  He picked Shiwan Khan as the film’s villain because “he was bold and he knew what he was doing – he wanted to conquer the world. That was very simple, maybe a little ambitious, but he knew exactly what he wanted.”  He had always been a fan of Alec Baldwin and wrote the script with him in mind: "He has the eyes and the voice; he had so much of what I pictured Cranston being".  Koepp also sat in on rehearsals and incorporated a lot of the actor’s humor into the script. 
 Universal backlot Hall of FX in this film, but it’s not a FX film. It’s a character/story-driven film. The FX are part of the story.” {{cite news
 | last = Murray
 | first = Will
 | coauthors =
 | title = Master of Death
 | work =
 | pages =
 | language =
 | publisher = Starlog
 | date = August 1994
 | url = http://www.shadowsanctum.net/collector/collector_images/Starlog_pg1.jpg
 | accessdate = 2007-04-16 }} 

==Soundtrack==
The Arista Records label released a soundtrack album in 1994. The soundtrack featured selections from Jerry Goldsmiths score and other songs from the film. 

Track Listing:
#The Shadow Knows... 1994 (Dialogue, performed by Alec Baldwin)   Original Sin (Theme from The Shadow) (Written by Jim Steinman, performed by Taylor Dayne)  
#The Poppy Fields (Main Title)  
#Some Kind of Mystery (Written by Diane Warren, performed by Sinoa)  
#The Sanctum  
#Who Are You?  
#Chest Pains  
#The Knife  
#The Hotel  
#The Tank  
#Frontal Lobotomy  
#Original Sin (Theme from The Shadow) Film Mix (Written by Jim Steinman, performed by Taylor Dayne)  
#The Shadow Radio Show 1937: Who Knows What Evil Lurks in the Hearts of Men? (Dialogue, performed by Orson Welles)  

Significantly, Camille Saint-Saënss 1872 composition Le Rouet dOmphale ("Omphales Spinning Wheel") is not used in the films score.

In 2012, Intrada released a 2-CD package that features the world premiere of the entire soundtrack composed by Jerry Goldsmith.

==Reception==

===Box office=== The Mask (later on), and it was a financial disappointment.      The film started off strongly, debuting at No. 2, but failed to sustain any momentum,   and grossed $32 million domestically, with a worldwide total of $48 million  against a budget of $40 million. The planned franchise never materialized.

===Critical===
The film received mostly mixed to negative reviews, with a 36% rating on review aggregate Rotten Tomatoes, with the consensus as "Visually impressive, but ultimately forgettable." The more detailed summary described the film as having "impressive" visuals and a story that does not "strike a memorable chord."  Entertainment Weekly placed the film on its "21 Worst Comic-Book Movies Ever" list.  However, on Siskel and Ebert, noted critic Roger Ebert gave the film a positive review.
 The Rocketeer The Phantom.

==Other media==
 
A Shadow video game for the Super Nintendo was developed to tie in with the 1994 film, but, after the low box office gross, was never released, though ROMS are available.
 Midway (under the Bally Technologies|Bally label) released a Shadow themed pinball machine in 1994. Brian Eddy of Attack From Mars and Medieval Madness fame designed the game. It was his first pinball game design, and it was moderately successful. Dan Forden composed original music for the game.

==Novelization==
James Luceno wrote the novelization which went deeper into the events of the film and included many nods to the radio show and the original pulp magazines, most significantly alluding to the fact that The Shadows true identity was Kent Allard and that Lamont Cranston was just another identity he assumed.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 