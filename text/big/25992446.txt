Prasthanam
{{Infobox film name           = Prasthanam image          = prasthanam.jpg director       = Deva Katta writer         = Deva Katta producer       = Ravi Vallabhaneni
VijayKrishna L starring       =   cinematography = Shamdat
| editing        = Dharmendra Kakarala music          = Mahesh Shankar released       =   runtime        = 152 minutes country        = India language       = Telugu budget         =   3.5 crores
}}
Prasthanam ( ;  ) is a 2010 Indian Telugu-language drama film written and directed by Deva Katta. The film was produced by Ravi Vallabhaneni under VRC Media & Entertainment banner. It was dubbed into Tamil as Padhavi. The film was selected for screening in the Indian Panorama section at International Film Festival of India, Goa.  

==Plot== Sai Kumar) takes over the reins of rural political power from a dying man, Dasaradha Rama Naidu (Balayya). On his death bed, Dasaradha Rama Naidu requests Loknath to marry his widowed daughter-in-law whose husband was murdered by rival political group. Loknath complies. His stepson is Mitra (Sharwanand) and his own son is Chinna (Sundeep Kishan). While Mitra is a devoted and loving son to Loknath, his step daughter has always hated him for marrying their mother and severs ties with them. Loknath wants to see Mitra as his heir because Mitra is a thinking man whereas Chinna is hot-blooded guy with bad life style. Chinna develops loathing towards Mitra because he receives lesser attention than Mitra from his father and is constantly discouraged by his father from entering politics. Mitras sisters sister-in-law is the love interest of Mitra. Chinna gets upset on learning that Mitra is the new youth president of the party. That night, he gets high on drugs and sodomizes Bashas daughter. Mitra wants to punish Chinna for the act he has done. Chinna tries to get Mitra killed in a movie theater, but Mitra escapes. Chinna then kills Mitras sister and her husband.

Now there is twist in tale, Loknath sacrifices his political goodwill in protecting his own son. By doing so he and Mitra are forced to confront each other and Loknath gets Mitra beaten up when Mitra chases after Chinna to avenge his sister. Mitra leaves the house and moves to his old home in the village and then kills Chinna. Loknath orders both Mitra and Basha to be killed. Basha saves Mitra from the goons and tells him a terrible secret from Loknaths past. Loknath had actually killed Mitras biological father, who was merely injured in the fight with the political rivals.

Mitra confronts Loknath with this secret and tells him that no amount of reading puranas (ancient religious texts) will absolve him of his heinous deed. Loknath then says " Go beyond the puranas; there are no villains and heroes in this world,there are only ordinary people doing small mistakes to fulfill their goals". Loknath then tries to kill Basha, when Mitra snatches the gun from him. Loknath tells Mitra to kill him; Mitra falls down on Loknaths feet and says that Loknath is the only father he has ever known and even if his biological father had lived, Mitra would not loved him as much as Loknath. With this, Mitra and Basha leave.

The film ends with Loknath shooting himself.

==Themes==

The movie, on one hand, deals with politics at grass root levels that occur in villages of India, corrupt political system and the nexus between businessmen and politicians. On the other hand, the movie deals with themes like greed, ambition and devotion which are driving forces of the lead characters in this movie. A movie-buff argues that the central theme revolves around the quote from the Book of Exodus, Old Testament, "The sins of the fathers shall be visited upon the children.   

==Cast==
* Sharwanand - Mitra
* Sundeep Kishan - Chinna Sai Kumar - Loknath
* Ruby Parihar
* Vennela Kishore - Goli
* Jaya Prakash Reddy - Bangaru Naidu
* Pavithra Lokesh - Mother
* Rashmi Gautam - Nadia
* Surekha Vani - Sister Jeeva - Mangampeta
* Mannava Balayya

==Soundtrack==
{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes
| title1 = Payanamae | extra1 = VV.Prasanna, Niranjanaiah Katta | length1 = 3:06 | lyrics1 = Deva Katta
| title2 = Bedaro | extra2 = Deepu | length2 = 3:51 | lyrics2 = Chaitanya Prasad
| title3 = Evado Vaadu | extra3 = Narayana | length3 = 4:47 | lyrics3 = Deva Katta
| title4 = Murali Lola | extra4 = Kalyani,  
| title5 = Naayudochaadoe | extra5 = VV.Prasanna | length5 = 3:34 | lyrics5 = Vanamali
| title6 = Innalluga | extra6 = Sahithi, Mahesh Shankar | length6 = 4:50 | lyrics6 = Vanamali
| title7 = Nee Rendallo | extra7 = Karthik (singer)|Karthik, S.Sowmya | length7 = 3:48 | lyrics7 = Vanamali
}}

==Reception== Simha and Darling (2010 film)|Darling, the movie did extremely well for its budget purely based on word of mouth publicity. The remake rights itself in several languages earned more than 70 lakhs for the producer. The film director confessed that it was  a mistake to release the movie just a week ahead two biggies in the lakshmi talk show. The movie garnered super positive response from critics upon its release. The critics mainly praised the performance of sai kumar and the convincing plot though some criticized the director for inapt song inclusion in crucial portions of the movie. The director also revealed that he never wanted to shoot a couple of songs and even took out another song from the final cut. He had to concede the inclusion of songs due certain external forces. The movie editor Dharmendra Kakarala was into fame after his debut with this film.The movie won several awards including the state government nandi awards, besides the film was selected for screening in the Indian Panorama section at International Film Festival of India, Goa.
It won two filmfare awards - best film critics choice and best supporting for Sai Kumar apart from many more regional awards.

==Awards==
;Nandi Awards
* 2010 - Nandi Award for Best Feature Film (Bronze) - Prasthanam Produced by Ravi Vallabhaneni & Directed by Deva Katta
* 2010 - Nandi Award for Best Supporting Actor - Sai Kumar
;Filmfare Awards South Filmfare Award for Best Supporting Actor (Telugu) - Sai Kumar Filmfare South Critics Award for Best film - Deva Katta

==See also==
* Vennela

* Jilla

==References==
 

==External links==
* http://filmyweb.blogspot.com/2009/09/prasthanam-choreographed-by-sagar-das.html
* http://www.indiaglitz.com/channels/telugu/musicreview/11249.html
* http://cinestills.andhracafe.com/v/Prasthanam+Posters/
* http://cinefolks.com/telugu/News/news/Prasthanam+in+theatres+by+Feb+second+week/
* http://www.idlebrain.com/news/2000march20/devakatta-prasthanam.html
*  
* To watch http://sunmoonmovies.blogspot.com/2011/07/prasthanam.html

 
 
 
 
 