W la foca
{{Infobox film
| name = W la foca
| image = W la foca.jpg
| caption =
| director = Nando Cicero
| writer =
| music = Detto Mariano
| cinematography = Giorgio Di Battista
| editing =
| producer =
| released =  
| country = Italy
| language = Italian
}} 1982 commedia sexy allitaliana directed by Nando Cicero.   

==Plot== Roman physician Dr. Filippo Patacchiola (Bombolo). Patacchiola lives with his nymphomaniac wife (Dagmar Lassander), raunchy daughter Marisa (Michela Miti), senile father (Riccardo Billi), mentally retarded son Paolo (Fabio Grossi), and African maid Domenica (Anna Fall) who Patacchiola wants to use as a "manmaker" for his son. The inevitable cycle of misunderstandings, couple exchange, and sexual seductions gets even more complicated when Andreas lover Michele (Carlo Marini) arrives in Rome to see Andrea.

==Cast==
*Lory Del Santo: Andrea
*Michela Miti: Marisa Patacchiola	
*Riccardo Billi: the Grandfather
*Bombolo: Dr. Filippo Patacchiola
*Dagmar Lassander: Signora Patacchiola
*Fabio Grossi: Paolo Patacchiola
*Anna Fall: Domenica
*Carlo Marini: Michele
*Victor Cavallo: painter	
*Franco Bracardi: tramp 
*Enzo Andronico: exhibitionist	 
*Moana Pozzi: girl on the train

==Screening==
In 2004 it was restored and shown as part of the retrospective "Storia Segreta del Cinema Italiano: Italian Kings of the Bs" at the 61st Venice International Film Festival. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 