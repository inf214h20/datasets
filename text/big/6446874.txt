Lunacy (film)
{{Infobox film 
| name           = Lunacy
| image          = Lunacyposter.jpg
| caption        = Poster for Lunacy
| director       = Jan Švankmajer
| producer       = Jaromír Kallista
| writers        = Jan Švankmajer
| starring       = Pavel Liška Jan Tříska Anna Geislerová Martin Huba Jaroslav Dušek Pavel Nový
| music          = Ivo Špalj
| cinematography = Juraj Galvánek
| editing        = Marie Zemanová
| distributor    = Warner Bros. (Czech Republic) Zeitgeist Films (USA)
| released       =  
| runtime        = 118 minutes
| country        = Czech Republic Slovakia
| language       = Czech
| budget         =
| gross          = $133,982 (INT)   
}}

Lunacy ( ) is a 2005 Czech film by Jan Švankmajer. The film is loosely based on two short stories, "The System of Doctor Tarr and Professor Fether" and "The Premature Burial", by Edgar Allan Poe. It is also partly inspired by the works of the Marquis de Sade. The film was shot between October 2004 and April 2005, on location in the village of Peruc close to Prague, and in Švankmajers studio in the village of Knovíz. 

==Plot summary==
Jean Berlot (Liska) is a deeply troubled man who has been haunted by violent hallucinations since the death of his mother, who was committed to a mental institution when she passed on. While arranging his mothers funeral, Jean meets a fellow inmate who claims to be the Marquis de Sade (Triska), and lives as if hes in Early modern France|18th-century France rather than the Czech Republic in 2005. Jean strikes up an alliance with De Sade, though they can hardly be called friends, and after becoming an unwilling accomplice to De Sades debauchery, Jean joins him at a hospital run by Dr. Murlloppe (Dusek), who offers "Purgative Therapy" for people who arent mad but could be in the future. Jean falls for a beautiful nurse named Charlota (Geislerova), who claims shes being held at the hospital against her will; in time, Jean hatches a plan to liberate her and the inmates, though he learns the truth is even more disturbing than hes been led to believe.

== Box office ==
The film grossed $48,324 in the US and $85,658 from foreign markets for a grand total of $133,982. 

== References ==
 

==External links==
*  at Metacritic
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 


 