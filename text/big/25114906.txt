Manna from Heaven (film)
{{Infobox film
| name           = Manna from Heaven
| image          = Manna from Heaven movie.gif
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Gabrielle Burton Maria Burton
| producer       = Gabrielle Burton Charity Burton Ursula Burton
| writer         = Gabrielle B. Burton
| narrator       =
| starring       = Shirley Jones Cloris Leachman Seymour Cassel
| music          = Timothy Jones James T. Sale
| cinematography = Edward Slattery Robert Tate Mattie Valentine
| studio         = Five Sisters Productions
| distributor    =
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $505,675 (USA)
}} 2002 film written by Gabrielle B. Burton and co-directed by her daughters Gabrielle C. Burton and Maria Burton. The film won awards at four film festivals.   It was actor Jerry Orbachs final film before his death from prostate cancer in 2004 and Shelley Duvalls final film before her retirement from acting in 2002.

==Plot==
Manna From Heaven is a comedic fable about what happens when you get a gift from God (a financial windfall), but many years later you find out it was a just a loan and its due immediately. Once upon a time, many years ago, a neighborhood in Buffalo, NY is mysteriously showered with 20 dollar bills. Theresa, a young girl who everyone thinks is a saint, doesnt have much trouble convincing her loose-knit "family" that the money is a gift from Heaven. Years later, Theresa, who has become a nun, has an epiphany that it is time to pay the money back, so she calls the eccentric group together to repay the "loan." The problem is, nobody wants to give back the money, nobody has the money, they dont know to whom it belongs, and most of them cant stand each other. Along the way, the characters learn about family, romance, reconciliation and redemption, and by working together they begin to realize their full potential.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Shirley Jones|| Bunny
|-
| Cloris Leachman || Helen
|-
| Louise Fletcher|| Mother Superior
|-
| Seymour Cassel || Stanley
|-
| Frank Gorshin || Ed
|-
| Austin Pendleton || Two-Digit Doyle
|-
| Shelley Duvall || Detective Dubrinski
|- Waltz Contest Announcer
|}

==Critical reception==
Dave Kehr of The New York Times liked the film overall:
 

==References==
 

== External links ==
*  
*  
 

 
 
 
 
 
 
 
 


 