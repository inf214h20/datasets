The Scamp
  1957 British drama film directed by Wolf Rilla and starring Richard Attenborough, Colin Petersen, and Dorothy Alison.

==Synopsis==
Tod Dawson, is a motherless boy, who is mistreated and beaten by his violent father. He eventually finds happiness with kindly foster parents - the Leigh family.

==Cast==
* Richard Attenborough as Stephen Leigh  
* Dorothy Alison as Barbara Leigh  
* Colin Petersen as Tod Dawson  
* Terence Morgan as Mike Dawson  
* Jill Adams as Julie Dawson  
* Maureen Delany as Mrs. Perryman  
* Margaretta Scott as Mrs. Blundell  
* David Franks as Eddie  
* Geoffrey Keen as Headmaster  
* Charles Lloyd-Pack as Beamish  
* June Cunningham as Annette  
* Sam Kydd as Shopkeeper
* Victor Brooks as Inspector Birch

==External links==
* 

 
 
 
 
 
 
 


 