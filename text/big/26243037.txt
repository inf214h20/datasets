The Most Dangerous Man in America
{{Infobox film
| name           = The Most Dangerous Man in America: Daniel Ellsberg and the Pentagon Papers
| image          = The Most Dangerous Man in America.jpg
| caption        =
| director       =  
| producer       =  
| writer         =  
| narrator       =
| starring       = Daniel Ellsberg
| music          = Blake Leyh
| cinematography =  
| editing        =  
| studio         =
| distributor    = First Run Features
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} involvement in Vietnam.   
 PBS series POV (TV series)|POV in 2010, for which it earned a Peabody Award.  

==Awards and nominations==
===Nominated=== Academy Award Best Documentary Feature

===Won===
International Documentary Film Festival Amsterdam
*Special Jury Award
Palm Springs International Film Festival
*Audience Award Best Documentary
National Board of Review, USA
*Freedom of Expression Award
Mill Valley Film Festival, USA
*Audience Award Best Documentary
San Luis Obispo International Film Festival, USA
*Best In Fest
Boulder International Film Festival, USA
*Best Feature Documentary
Its All True Film Festival, Brazil
*Audience Award Best Documentary
Fresno Film Festival, USA
*Audience Award Best Documentary
Sydney Film Festival, Australia
*Best Documentary
Mendocino Film Festival, USA
*Audience Choice Award Co-Winner Docaviv Film Festival, Israel
*Special Jury Mention
Traverse City Film Festival, USA
*Audience Award Best Documentary
American Historical Association, USA
*John OConnor Film Award
History Makers Award, USA
*Best History Production

==References==
 

==External links==
*  
*  
*   at Kovno Communications 
*  
*  
 

 
 
 
 
 
 
 
 
 
 