Our Lady of the Assassins (film)
 

 La virgen de los sicarios}}
{{Infobox film
| name           = Our Lady of the Assassins  (La virgen de los sicarios)
| image          = Lavirgendelossicarios.png
| image_size     =
| caption        = Theatrical release poster
| director       = Barbet Schroeder
| writer         = Fernando Vallejo
| starring       = Germán Jaramillo Anderson Ballesteros Juan David Restrepo
| music          = Jorge Arriagada
| cinematography = Rodrigo Lalinde
| editing        = Elsa Vásquez
| released       = 2000
| runtime        = 98 mins
| country        = Colombia Spain France
| language       = Spanish
}}
 novel of the same title by Fernando Vallejo.
 

== Synopsis ==

Fernando (Germán Jaramillo) meets Alexis (Anderson Ballesteros), a handsome gay youth, at a party of one of his old friends and immediately falls for him. The two begin a relationship which, apart from the sex, consists mainly in Fernando telling Alexis how pastoral the city was when he left, while Alexis explains to Fernando the ins and outs of everyday robbery, violence, and shootings. Even though Fernando has come home to die, his sarcastic worldview is mellowed somewhat by his relationship with Alexis.

He soon discovers that Alexis is a gang member and hitman (or Sicarii#Modern Comparisons|sicario) himself, and that members of other gangs are after him. After several assassination attempts fail because of Alexis skillful handling of his Beretta, he is finally killed by two boys on a motorcycle. Fernando is partly responsible for this, as Alexis weapon has been lost before the murder due to Fernandos suicidal impulses.

Fernando visits Alexis mother and gives her some money, and then walks through the streets aimlessly when he encounters Wilmar (Juan David Restrepo), who bears a striking resemblance to Alexis, not only in his looks but in his entire manner.

He invites Wilmar for lunch and the two begin an affair, rekindling the kind of relationship he had with Alexis. Wilmar is also a killer, but it is a shocking revelation  to Fernando when he finds out that Wilmar is the one who shot Alexis. He vows to kill Wilmar, but then learns it was Alexis who started the violence by killing Wilmars brother, calling for vengeance on him by Wilmar.

When Wilmar goes to say goodbye to his mother before he and Fernando leave the country together, he is killed as well. Seeing that the vicious cycle of atrocities in Medellín denies happiness, Fernando presumably commits suicide, if the last scene is taken to hint at that.

==Cast==
*Germán Jaramillo as Fernando
*Anderson Ballesteros as Alexis
*Juan David Restrepo as Wilmar
*Manuel Busquets as Alfonso

== Film production ==

The film was shot with early high-definition video cameras (Sony HDW-700) in the year 2000. The digital video gives the movie a cinéma vérité look and was one of the first uses of HD video for a feature film.

== See also ==
* Movies depicting Colombia
* List of Colombian films

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 