Garo: Soukoku no Maryu
{{Infobox film
| name           = Garo: Soukoku no Maryu
| image          = Soukoku no Maryu.jpg
| film name = {{Film name| kanji          = 牙狼〈GARO〉～蒼哭ノ魔竜～
| romaji         = Garo ~Sōkoku no Maryū~}}
| alt            = 
| caption        = 
| director       = Keita Amemiya
| producer       =  
| writer         = Keita Amemiya
| starring       =  
| music          = Shunji Inoue
| cinematography = Akira Nishioka
| editing        = Tomoki Nagasaka
| studio         = Tohokushinsha Film
| distributor    = Tohokushinsha Film
| released       =  
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a Japanese film that released on February 23, 2013, and serves as an epilogue to  .  , and Keiko Matsuzaka. The catchphrase for the movie is  .

==Plot==
After the events of  , Kouga Saezima appears before Gajari to honor the pact he forged in order to stop Sigma Fudō from killing all the Makai Knights. Kouga is given the task to travel to the   to retrieve a part of Gajari: the  . However, upon his arrival into the  , Kouga finds himself without his coat and Zaruba no where to be seen before seeing the Garoken turned into the   before running off. Confronting a group of tin knights called   escorting a picture frame, Kouga obtains a sword when they are attacked by a formless monster, a  . Once the two surviving knights run off, Kouga sees a blue skinned girl emerging from the picture frame. The girl introduces herself as Meru and that she is like every resident in the Promised Land: A living object. Meru proceeds to explain that she was captured to be taken to   as an offering to Queen Judam. Despite being warned of the danger, Kouga is guided to the   by Meru to find out where to find the Fang of Sorrow.

Once there, Kouga encounters a fading scarecrow-like entity that transforms into a human-like form when he unintentionally named him "Kakashi". After Kakashi takes his leave, learning that he is in the   graveyard of those who lost their names and forms, Kouga and Meru are forced to run from the Nanashi reacting to the event. Once at the shrine, Kouga finds that its   is Zaruba who has no memory of Kouga and assumes a large form to reflect Kougas imagination to have the human fight him for what he wants to know. Eventually, Zaruba remembers Kouga and revert to original form. Zaruba proceeds to reveal that Kougas arrival has caused a time dilation and that he has served as the guardian for centuries before being reunited. Before anything else can be settled, Kouga calls out the mysterious gnome-like object that has been following them. Introducing himself as Kiria from the  , he pleas Kouga for his help. Learning about the Maryu, a dark dragon that creates nothingness in its path and would emerge once the moon is completely black, Kouga and the others come to the suspected conclusion that the monsters fang is also the Fang of Sorrow. However, Kouge feels that finding his items should be his first priority before defeating the Maryu.

Once at the Land of Bliss, finding himself dancing with Meru, Kouga encounters Kakashi who provokes him into a duel before the harpy-like   appears. Revealed to be Kougas coat, the harpy flies off with Kouga in pursuit while Kakashi follows after him. However, the three are captured by the Green Castle with Kouga meeting Judam and voicing his opinions on her heartless behavior for what she considers ugly and lustful greed for beautiful things. By then, thanks to Kakashis trusted canine companion Kuromaru steering their Halloween-ish   house into the Green Castle, Kouga frees his coat and Kakashi so they can escape. After learning he needs the Garoken for his coat to recognize him, Kouga learns of Kakashis plans to merge with the Maryu to become human. However, Judam took the Maryu scale needed for the fusion. But upon learning Kouga intends to kill the Maryu, Kakashi decides to finish their duel when Kiria arrives. Unfortunately, Kouga and Kiria find the Land of Bliss in ruins with the former helping the latter remember his best friend Raudo after he died. The two then find another survivor named Daruki, who reveals that Judam took Meru as an offering to the Maryu.

At the  , before dangling her above the vortex the Maryu is to emerge from, Judam reveals to a humanized Meru her intention to exact revenge on humans even if she must sacrifice her only beauty by merging with the Maryu to do so. However, as Kakashi makes an attempt to reclaim the scale in a battle between his house and Judams castle, Kiria saves Meru while Kouga sees the Garoken and reclaims the weapon. Seeing a ghost of Taiga appearing before him while regaining his coat, Kouga arrives to save Kakashi after his house is destroyed before making his way to the Green Castle to face Judam. Seeing her memory wiping against the human, Judam battles Kouga as she reveals that she will merge with the Maryu to enter the human world and destroy humanity for discarding her into the Promised Land. Kouga attempts to reason with Judam, but she refuses to listen as she uses the Maryus scale transform into a small dragon. Despite its size, the Maryu quickly creates a giant mechanical construct around itself.

Donning Garo, Kouga chases after the Maryu as it attempts to transverse into the human world with his friends helping out. However, once in the space between dimensions, the Maryu is about to kill Garo with its   when Kakashi arrives the Makai Knight the energies within his heart. Riding a version of Goten, Blue Dragon Garo destroys the Maryus construct to run Judam through. As Judam accepts Kougas apology for humanitys discarding nature, the Maryuu dissolves with only a fang from its construct body remaining. Back in the Promised Land, Kouga finds Kakashi dying from his self sacrifices and learns that he was a training dummy Kouga sparred with as a child. After Kouga tells him that he was one of many who made him, Kakashi fades away with Kuromaru crying over his friends departure. Once he arrives with Meru, revealed to be a pen in the human world, Kiria fades once his owner finds him alongside Kuromaru, who is a stuffed animal.

Destroying the Maryus fang to Meru and Zarubas shock, Kouga explains that he himself is the actual Fang of Sorrow. Meru is confused and Kouga explains to her that Gajari was a product of the Makai Knights imagination and that the whole quest was for Kouga to understand the symbiosis of the dreamers and the dream. Gajari appears and congratulates Kouga for reaching that conclusion before transforming into a portal to send Kouga back to his world. Before he leaves, Kouga tells Meru that he knew the entire she was Rekkas dragonfish Kaoru as she returns to her true form to accompany Kouga. While visiting the remnants of Kakashis true form, learning of a competition among the Makai Knights in his absence where the winner can visit one who is deceased, Kouga challenges the winner Rei to a duel with his father helping his opponent in spirit. Some time after, Kaoru at the park in the aftermath of her books successful launch when she is reunited with Kouga.

==Cast==
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* ,  :  

==Theme song==
*  
**Lyrics & Composition: Hironobu Kageyama
**Arrangement: Yoshichika Kuriyama, Shiho Terada
**Artist: JAM Project

==References==
 

==External links==
*   

 

 
 
 
 
 
 