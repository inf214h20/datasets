Aanivaer
{{Infobox film
| name           = Aanivaer
| image          = Aanivaer_Movie_Poster.jpg
| caption        =
| director       = John Mahendran
| producer       = C. Prabhakaran
| writer         = John Mahendran
| choreography   =
| starring       = Nandha (actor)|Nandha, Madhumitha, Neelima Rani
| music          = Satheesh
| cinematography = Komagan
| editing        =
| distributor    = Thamizh Thiraikkann
| released       = September 23, 2006
| runtime        =
| country        = United Kingdom Canada
| language       =
| budget         =
| gross          =
| preceded by    =
}}
 Tamil independent Indian Tamil film director John Mahendran released in the western Tamil diasporal regions on September 23, 2006.

==Plot==
Dr. Nanda (Nandha (actor)|Nandha) works at a small makeshift hospital that serves as the only option for the wounded and the dying from the war. He refuses to leave his motherland for greener pastures and sets out to serve his people. Journalist Sandhya (Madhumitha) comes from India for a cover story on the ethnic strife. But the witnessing of every possible cruelty inflicted on the people is too much to take. Having personally experienced the pitiful plight of the Tamils in Sri Lanka, she goes back to South India with a heavy heart. The love lorn journalist from South India returns to Vanni for the second time with a view to meeting the Doctor cum social worker whom she had met and loved on her first visit. Her initial enquiries to find out the whereabouts of the Doctor friend prove difficult. But she continues.

==Cast== Nandha
* Madhumitha
* Neelima Rani
* Mullai Yesudasan

==Production== Civil War Sri Lankan Tamil population in Canada, the United Kingdom, and Australia.  The film also released in India the following year.   

==References==
 

==External links==
*  

 

 
 
 
 
 


 