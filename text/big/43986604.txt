Both (film)
{{Infobox film
| name           = Both
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Lisset Barcellos
| producer       = Celeste Carrasco Dana Castro
| writer         = Rafael Dumett
| starring       = Jackie Parker Mike Martinez
| music          = 
| cinematography = Amanda Micheli Andrew Piccone
| editing        = Lisset Barcellos
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = USA
| language       = English, some Spanish
| budget         = $150,000 (estimated)
| gross          = 
}}
Both is a 2005 US-Canadian-Peruvian drama film directed by Lisset Barcellos. Starring Jackie Parker, Mike Martinez, Nicole Wilder, Fabrizio Aguilar, Ximena Ameri and Pable Barcellos, the film tells the story of a stunt double with an intersex variation, who discovers her past.

The film was welcomed by members of the intersex community. The world premiere took place at  , May 19, 2005. 

==Inception==
An independent film, the story behind Both is based in part on the experience of the filmmaker herself.  Talking with the Hartford Courant, Barcellos commented, "I made my film so I dont have to talk about my own experiences". 

==Plot==
Rebeca Duarte (Jackie Parker) is a beautiful and bisexual stunt double, who regularly puts her life at risk in the course of her job. Duarte feels disconnected from her own body, but doesnt understand why that is. Out of the blue, she receives a photo album from a member of her family in Peru. In the photographs she finds her parents and her brother, who passed away, but she is missing. A telephone call to her mother doesnt explain the story behind the photograph album or her non-appearance. Duarte sets out to discover the mystery.

==Reception==

SF Gate described this debut film by Lisset Barcellos as "Raw, nervy and groundbreaking", "a brilliant debut" mildly hampered by low production values.  The East Bay Express described the movie as an "overlooked gem" and "impressive debut" 

The film was well received by intersex community organizations. 
The  . 

==Availability==
The full movie is available online via Vimeo. 

==See also==
* Intersex
* Hermaphrodite

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 