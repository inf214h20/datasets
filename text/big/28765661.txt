Alligator II: The Mutation
{{Infobox Film
| name           = Alligator II: The Mutation
| image          = alligator_II_the_mutation.jpg
| caption        = DVD cover Jon Hess
| producer       = Brandon Chase Cary Glieberman
| writer         = Curt Allen Woody Brown Harlan Arnold Nicolas Cowan Brock Peters
| music          = Jack K. Tillar
| cinematography = Joseph Mangine
| editing        = Christopher Ellis Marshall Harvey
| released       = December 18, 1991
| runtime        = 92 mins
| country        = United States English
| budget         = $3,000,000 (estimated)
| gross          = 
| followed_by    =
}}

Alligator II: The Mutation is a 1991 American direct-to-video sequel of Alligator (film)|Alligator (1980).

==Plot==
Deep in the sewers beneath the city of Regent Park, another baby alligator feeds on the experimental animals discarded by future Chemicals Corporation. Nourished by toxic growth hormones and other mutating chemicals, the gator grows immense in size and develops voracious appetite and goes out on a killing spree. Nobody believes the sightings until a large number of people are killed, and the police eventually embark on a search-and-destroy mission to stop the alligators murderous rampage. They track it down to a lake and a police helicopter attempts to blow the alligator up, but to no avail. Afterwards, two officers enter a marsh area where the alligator had escaped to. One of the officers uses a rocket launcher and ends the alligators life.

==Cast==
*Joseph Bologna as David Hodges
*Dee Wallace as Christine Hodges Richard Lynch as Hawk Hawkins Woody Brown as Rich Harmon
*Holly Gagnier as Sheri Anderson
*Bill Daily as Mayor Anderson
*Steve Railsback as Vincent Vinnie Brown
*Brock Peters as Chief Speed
*Trevor Eyster (credit as Tim Eyster) as J.J. Hodges
*Voyo Goric as Carmen

==Reception==
Upon its release on video, it met negative reception with critics comparing it to the first/original. The film currently holds a rating of 12% on review aggregator Rotten Tomatoes.  It has a rating of 3.2/10 on Internet Movie Database|IMDb.

==References==
 

==External links==
*  
*  

 
 
 
 
 