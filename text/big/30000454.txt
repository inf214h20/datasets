We Moderns
{{infobox film
| name           = We Moderns
| image          = We Moderns.JPG
| caption        = John Francis Dillon John McCormick
| based on       =  
| writer         = June Mathis (scenario)
| starring       = Colleen Moore
| music          =
| cinematography = Ted D. McCord 
| editing        = Edwin Robbins
| studio          = John McCormick Productions
| distributor    = First National Pictures
| released       =  
| runtime        = 7 reel#Motion picture terminology|reels; between 6,609 and 6,656 feet
| country        = United States Silent English intertitles
}}
 silent comedy John Francis John McCormick (1893-1961), was released through First National Pictures. It was based on the play and novel by Israel Zangwill. The play ran for 22 performances in 1924 at the Gaiety Theatre (New York), produced and directed by Harrison Grey Fiske and starring Helen Hayes and Isabel Irving. 

Portions of the film were shot on location in and around London while Colleen was in the city during her European tour, which was undertaken to promote her films So Big and Sally. Those portions were directed by her husband and depicted a scavenger hunt, so that the production could visit many famous and recognizable London locations . A car was purchased specifically for the film and was shipped back to the states so that the film could be completed at the studio. Colleen had to acquire a special permit to drive. The film was intended to be an English look at the flapper, and the story shared many of the same elements of Colleens star-making Flaming Youth. As in Flaming Youth, Colleens brother Cleeve had a part in this film as well. He played the brother of Colleens character.

==Cast==
*Colleen Moore - Mary Sundale
*Jack Mulhall - John Ashler Carl Miller - Oscar Pleat
*Claude Gillingwater - Sir Robert Sundale
*Clarissa Selwynne - Lady Kitty Sundale
*Cleve Morison - Dick Sundale
*Marcelle Corday - Theodosia
*Blanche Payson - Johanna Tom McGuire - Beamish
*Dorothy Seastrom - Dolly Wimple
*Louis Payne - Sir William Wimple

==Preservation status==
We Moderns is currently considered a lost film.  

==See also==
*List of lost films

==References==
 

==Bibliography==
*Jeff Codori, Colleen Moore; A Biography of the Silent Film Star,   ISBN 978-0-7864-4969-9 (print) ISBN 978-0-7864-8899-5 (eBook)

==External links==
*  
*  
*  

 


 
 
 
 
 
 
 
 
 
 