A Sangre Fría
{{Infobox film name     =   A sangre fría
|  image          = Sangrefriaposter.jpg
|  caption        =
|  director       = Daniel Tinayre
|  producer       = Juan J. Guthmann
|  writer         = Luis Saslavsky
|  starring       = Tito Alonso, Lopez Lagar Amelia Bence
|  music          = Helena Cortesina
|  cinematography = Alberto Etchebehere
|  editing        = Jorge Gárate
|  distributor    =
|  released       = 1947
|  runtime        = 95 minutes
|  country        = Argentina Spanish
|  budget         =
}} Argentine murder thriller film directed by Daniel Tinayre and written by Luis Saslavsky.   

==Plot==
  portrays a vulnerable ageing aunt]]

Amelia Bence portrays a wealthy but weak and vulnerable old lady, mistreated by evil nephew Lopez Lagar. Enlisting the aid of Bences evil unfaithful serving girl, Lagar poisons his elderly aunt, hoping to secure her substantial fortune without being accused of murder. But the villain and his confederate are foiled in their plans in the nick of time.

==Cast==
*Tito Alonso
*Amelia Bence
*Ricardo Castro Ríos
*Helena Cortesina ....  Linda Moreno
*Floren Delbene
*Carmen Giménez ....  Mujer en hotel
*Antonia Herrero
*Ángel Laborde ....  Hombre en estación de servicio
*Marcelo Lavalle ....  Transpunte
*Carmen Llambí ....  Monja
*Mercedes Llambí ....  Enfermera
*Pedro López Lagar
*Domingo Mania ....  Sr. Dupont
*José Maurer ....  Doctor
*Luis Otero ....  Comisario Valdez
*Juan Pecci ....  Farmacéutico
*Ilde Pirovano
*Elvira Quiroga
*Domingo Sapelli
*Nicolás Taricano ....  José

==Release==
 ) (left) attempts to murder her to inherit her fortune in this 1947 thriller from Argentina]]

The film was released  on 5 September 1962.
 Argentine Academy of Cinematography Arts and Sciences gave the Best Actress award to Amelia Bence and the Best individual production award to Luis Saslavsky for this film. 

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 


 
 