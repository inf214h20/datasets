The Long Days of Summer
{{Infobox film
| name           = The Long Days of Summer
| image          = Fourth of July - Days of Summer DVD cover-2.jpg
| image_size     = 240px
| caption        = DVD cover art
| director       = Dan Curtis
| producer       = Dan Curtis Lee Hutson Joseph Stern
| writer         = Hindi Brooks Lee Hutson
| narrator       = Charles Aidman Dean Jones Louanne Donald David Baron Michael McGuire
| music          = Walter Scharf
| cinematography = Charles Correll
| editing        = Bernard Gribble ABC
| released       =  
| runtime        = 81 min.
| country        = United States English
| budget         = $750,000 Dean Jones and Dan Curtis DVD Commentary, Released 2005 
| gross          =
}}
 ABC television Dean Jones, Donald Moffat, Ronnie Scribner and Louanne Sirota|Louanne.

==Plot== David Baron). German challenger, Dean Jones) also begins to experience antisemitism when he takes in a Jewish boarder from Germany (Donald Moffat) who has come to the United States to warn of the increasing menace overseas in the form of Adolf Hitler. As Danny and his family are faced with their own pressures from within their small-town community, they must each make difficult decisions about standing up for what is right in the face of discrimination and intimidation.

==Cast==
 Dean Jones	        ... 	Ed Cooper
*Joan Hackett	... 	Millie Cooper
*Ronnie Scribner	... 	Daniel Cooper Louanne ... Sarah Cooper
*Donald Moffat	... 	Josef Kaplan
*Andrew Duggan	... 	Sam Wiggins David Baron	... 	Freddy Landauer Jr. Michael McGuire	... 	Lieutenant OHare
*Lee de Broux	... 	Fred Landauer Sr.
*Baruch Lumet	... 	Rabbi
*Gloria Calomee	... 	Clementine
*Leigh French	... 	Frances Haley
*John Karlen	... 	Duane Haley Tiger Williams ... Charlie Wilson Adam Gunn	        ... 	Howie Martin Brian Andrews	... 	Marty Albert
*Dan Appel	        ... 	Dave Zimmer (as Danny Appel)
*Dave Shelley	... 	Tom Wade
*Joseph G. Medalis	... 	Coach Dowd (as Joe Medalis) Stephen Roberts	... 	Franklin D. Roosevelt
*Richard Reicheg	... 	Bill Elliott
*Paula Brook	... 	June Riley
*Steve Wayne	... 	Butler
*Charles Aidman	... 	Narrator (uncredited)

==Production== pilot for Curtis made ABC network instead.     
   Dean Jones Louanne was cast to take over the role of Sarah for the sequel, becoming her very first role before rising to stardom in such films as Oh, God! Book II and A Night in the Life of Jimmy Reardon.   

As with the first film, the decision was made to shoot "Bridgeport, Connecticut" in California.  Curtis returned to the same neighborhood in Echo Park, Los Angeles, and shot many of the outdoor scenes for the sequel in the same locations that were used in the original film. 

==Awards==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
! Ref.
|-
| 1980 Young Artist Youth In Film Award  (now known as the Young Artist Award) 
| Best TV Special for Family Entertainment
| align=center|–
|  
| align=center|   
|}

==See also==
When Every Day Was the Fourth of July

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 