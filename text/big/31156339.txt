Brahmachari (1972 film)
{{Infobox film 
| name           = Brahmachari
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = SST Lakshmanan SST Subramaniyam Thiruppathi Chettiyar
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Sharada Adoor Bhasi Jose Prakash
| music          = V. Dakshinamoorthy
| cinematography = JG Vijayam
| editing        = K Sankunni
| studio         = Evershine
| distributor    = Evershine
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by SST Lakshmanan, SST Subramaniyam and Thiruppathi Chettiyar. The film stars Prem Nazir, Sharada (actress)|Sharada, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by V. Dakshinamoorthy.    The film was a remake of the 1967 Tamil film Penne Nee Vazhga and Hindi film Ek Nari Ek Brahmachari.

==Cast==
 
*Prem Nazir Sharada
*Adoor Bhasi
*Jose Prakash
*Mohan Sharma
*Sankaradi
*T. R. Omana Baby
*T. S. Muthaiah
*Yamuna
*Ambili
*Bahadoor
*Girija
*John Varghese
*Justin
*Oolan Ramu
*Rani Chandra Sujatha
*Treesa
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chithrashilaapaalikal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Innalathe Vennilaavin || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Karayoo Nee Karayoo || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Njan Njan Njanenna || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Pathinezhu Thikayaatha || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 