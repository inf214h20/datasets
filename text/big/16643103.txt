Head Case (film)
{{Infobox film
| name           = Head Case
| director       = Anthony Spadaccini
| image	=	Head Case FilmPoster.jpeg
| producer       = Anthony Spadaccini Benjamin P. Ablao, Jr.
| writer         = Anthony Spadaccini
| starring       = Paul McCloskey Barbara Lessin Brinke Stevens Bruce De Santis Emily Spiegel Michael J. Panichelli, Jr.
| music          = Gerard Satamian
| distributor    = Fleet Street Films B.P.A. Productions Group, Inc. Brain Damage Films
| released       = Newark Film Festival  
| runtime        = 101 minutes
| country        = United States
| language       = English 
| budget         = $5,000
}}
Head Case is a 2007 pseudo-documentary film (cinéma vérité) horror film written and directed by Anthony Spadaccini.

The film is presented as a collection of home movies from serial killers Wayne and Andrea Montgomery, middle-class suburban residents of Claymont, Delaware who film their sadistic crimes.

==Cast==
* Paul McCloskey as Wayne Montgomery
* Barbara Lessin as Andrea Montgomery
* Brinke Stevens as Julie
* Bruce De Santis as Todd Montgomery
* Emily Spiegel as Monica Montgomery
* Michael J. Panichelli, Jr. as Detective John Haynes

==External links==
*  

 
 
 
 
 
 


 