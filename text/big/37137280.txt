Floating City
 Floating city}}
 
{{Infobox film
| name           = Floating City
| image          = Floating City poster.jpg
| border         = 
| alt            = 
| caption        = China poster
| director       = Yim Ho 
| producer       = Carl Change Yim Ho
| writer         = Marco Pong Yim Ho
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Charlie Young Josie Ho
| music          = 
| cinematography = Ardy Tam
| editing        = Stanley Tam
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = China Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
Floating City ( ) is a 2012 Chinese-Hong Kong drama film directed by Yim Ho. 

==Plot==
The story of a self-made man in a rapidly changing Hong Kong at the end of British Colonial rule.

==Reception==
Exclaim! writer Robert Bell said "...the subject matter of Floating City doesnt come as a surprise, mirroring the life of a self-made man with the rapid change of Hong Kong from small fishing village to booming metropolis over latter half of the 20th Century. But what does surprise is just how amateurish and sloppy it all is, which is of substantial concern with such an epic and ambitious project." 

DVD Talk reviewer Thomas Spurlin said "As a whole, the impression I took away from Floating City is that of an overwrought and disjointed meditation on the value of life and the injustice of misfortune, a melodrama that confuses its soapbox-worthy communication of social issues and generational shifts in China for an enriching tale of cohabiting with them." 

PopMatters journalist J.C. Maçek III wrote "First and foremost, Floating City is an incredibly beautiful movie. Director Yim Ho uses his camera lens to drink in the scenery of Hong Kong over a decades-long story that is as beautiful as the scenery itself." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 
 