Romance of the Underworld
{{infobox film
| name           = Romance of the Underworld
| image          =
| caption        =
| director       = Irving Cummings William Fox
| writer         = Paul Armstrong(play:A Romance of the Underworld) Douglas Z. Doty(story) Sidney Lanfield(story) Douglas Z. Doty(screenplay) Garrett Graham(intertitles)
| starring       = Mary Astor
| music          =
| cinematography = Conrad Wells
| editing        = Frank Hull
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 68 minutes; 7 reels(6,162 feet)
| country        = United States
| language       = Silent film(English intertitles)
}} James Kirkwood with Catherine Calvert in Astors part. The 1928 film is extant at the Museum of Modern Art.  

==Cast==
*Mary Astor - Judith Andrews
*Ben Bard - Derby Dan Manning Robert Elliott - Edwin Burke John Boles - Stephen Ransome
*Oscar Apfel - Champagne Joe
*Helen Lynch - Blonodie Nell
*William H. Tooker - Asa Jenks

Uncredited roles:

* William Benge - Bartender
* Maurice Black - Maitre D  
* Sherry Hall - Pianist
* John Kelly  - window cleaner 
* Broderick OFarrell - Attorney
* Dick Rush - Police assistant
* Phillips Smalley - John T. Woodring
* Lottie Williams - Nanny
* Florence Wix - Cafe supervisor

==References==
 

==External links==
* 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 


 
 