Mooz-lum
{{Infobox Film
| name           = Mooz-lum
| caption        = 
| image	=	Mooz-lum FilmPoster.jpeg
| director       = Qasim "Q" Basir 
| producer       = Peace Film
| writer         = Qasim "Q" Basir
| starring       = Danny Glover Nia Long Evan Ross Roger Guenveur Smith 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Rising Pictures (Australia)
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $1,549,141 (USA)
| gross          = $330,048 (USA)
}} African American their aftermath. The film was initially promoted primarily through social media, before opening for its limited theatrical release on February 11, 2011.    

==Plot==
Amid a strict Muslim rearing and a social life he has never had, Tariq Mahdi (Evan Ross) enters college confused.

New peers, family and mentors help him find his place, but the 9/11 attacks force him to face his past and make the biggest decisions of his life.

==Filming==
The movie was filmed in Southeastern Michigan. Although the college attended by Tariq is never explicitly identified, most of the college scenes were filmed on location on the campuses of the University of Michigan and Eastern Michigan University. The mosque scene was filmed at the Islamic Center of America in Dearborn, Michigan.

==Reception==
In 2004, aspiring director Qasim "Q" Basir happened to cross paths with controversial director, Spike Lee, in his Detroit hometown. Basir asked him for guidance on how to succeed in the industry.  Lee simply replied, "Write a good script, man."

Basir, now 30, followed Lees simple advice, which led him to eventually write, and direct his first full-feature film, Mooz-lum, starring Nia Long, Danny Glover, Evan Ross and Roger Guenveur Smith. Partnering with AMC and Eventful, the film was released nationally in February 2011.

Mooz-lum is based on the trials of Tariq Mahdi (Ross), a Muslim-American boy who struggles to find his identity while enduring the forces of his strict father, other Muslims, and non-Muslims who are pulling him in different directions.

"I said yes immediately and wanted to make sure I represented Muslim women properly," Deeqa said. "A piece like this sheds light on an area of life that we may not really know and have never seen portrayed on the screen, and that is what really attracted me to the part."

According to the films Web site, Mooz-lum embraces four words: "Forgiveness, Tolerance, Hope and Identity." Basir said, "I hope this film is well-received and it opens minds, because right now, there is an immense amount of ignorance surrounding Islam and Muslim people and I just want to put a human face on it."

===Critical response===
As of January 14, 2015, Mooz-lum has received an overall rating of 78% from all critics (7 fresh and 2 rotten) at Rotten Tomatoes. 

===Awards and honors===
*14th Annual Urbanworld Film Festival 2010 - Best Narrative Feature
*Chicago International Film Festival 2010 - Official Selection
*34th Cairo International Film Festival - Official Selection

==See also==
*List of cultural references to the September 11 attacks

==Notes==
 

==Further reading==
*Basir, Qasim. " ." Huffington Post, August 19, 2010.
*" " - National Public Radio, September 20, 2010.
*" " - Contending Modernities, February 17, 2011.

== External links ==
*  
*  
* https://www.facebook.com/Moozlumthemovie

 
 
 
 