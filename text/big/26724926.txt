Broadway to Cheyenne
{{Infobox film
| name           = Broadway to Cheyenne
| image_size     =
| image	=	Broadway to Cheyenne FilmPoster.jpeg
| caption        =
| director       = Harry L. Fraser
| producer       = Trem Carr (producer)
| writer         = Wellyn Totman (story and adaptation) & Harry L. Fraser (story and adaptation)
| narrator       =
| starring       = See below
| music          =
| cinematography = Archie Stout
| editing        = Carl Pierson
| distributor    =
| studio         = Monogram Pictures
| released       = 1932
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Western film Western with the gangster film and vigilante film.

==Plot== detective "Breezy" Kildare is attempting to arrest B.H. "Butch" Owens, the leader of a gang of criminals who attempted to bribe him.  He is wounded in a shootout between Owens gang and another gang in a Broadway night club.  

His police chief allows him to recuperate and cool down in his thirst for justice back in his home of Wyoming where his father is a cattleman.  Once arriving back home he soon discovers the gangsters who attempted to bribe and kill him are lying low there and diversifying by starting a Cattlemans Benevolent Association that is actually a protection racket protecting the cattlemen from such perils as having their cattle machine gunned.  

When his father is shot in a drive-by shooting, Breezy leads the cattlemen against the well armed gangsters who no longer have the power of a bribed administration or high powered legal protection, but now have to face six gun justice and lynch law.

==Cast==
*Rex Bell as "Breezy" Kildare
*Marceline Day as Ruth Carter
*Matthew Betz as Joe Carter Robert Ellis as B.H. "Butch" Owens
*George Gabby Hayes as Walrus
*Huntley Gordon as New York City Dist. Atty. Brent 
*Roy DArcy as Jess Harvey
*Gwen Lee as Mrs. Myrna Wallace
*Harry Semels as Louie Walsh
*Al Bridge as Al (Owens henchman)
*Rae Daggett as Rae Walsh John Elliott as Martin Kildare
*Gordon De Main as Rancher
*Earl Dwire as Cattleman

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 