The War of the Worlds: Next Century
 
{{Infobox Film
| name           = The War of the Worlds: Next Century
| image          = Wojna swiatow 1981.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Piotr Szulkin
| producer       = 
| writer         = Piotr Szulkin
| narrator       = 
| starring       = 
| music          = Józef Skrzek
| cinematography = Zygmunt Samosiuk
| editing        = Elżbieta Kurkowska
| studio         = 
| distributor    = 
| released       = 20 February 1983
| runtime        = 92 minutes
| country        = Poland
| language       = Polish
| budget         = 
| gross          = 
}}
 Polish film by Piotr Szulkin which is inspired by the classical novel of H. G. Wells, The War of the Worlds. It had its premiere on 20 February 1983.

==Plot==
The film starts from the position close to the literary inspiration suggested in the title, but rather from developing it in the same manner as the novel it is used as a witty commentary on the political situation of Poland in the period of the Polish Peoples Republic.
 Martians resembles a police state in which a huge role is played by television, which is used as a propaganda tool.

The main character of the film, Iron Idem (Roman Wilhelmi) is a news presenter who  has a popular TV program, Iron Idems Independent News. However, the news that is presented on his program is carefully chosen by Idems boss (Mariusz Dmochowski) who later orders the kidnapping of Idems wife (Krystyna Janda). Iron Idem is forced to collaborate with the state apparatus, which is controlled by blood thirsty Martians, and encourages people to give blood.

After being thrown out of his flat, Idem has a chance to observe stupefied citizens who fall victim to the repression of the state apparatus. Finally, the main protagonist rebels and criticizes society during a TV Super Show which is a concert organized as a farewell to the Martians.

On the day after the Martians departure the Earth’s mass media change their perception of the whole situation and the visit from Mars is viewed as an aggressive invasion and Iron Idem is shown as the main collaborator. He is sentenced to death and killed but only on the television screen. In reality he leaves the television studio and steps into the outside world which is covered by mist.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 