The People Next Door (1970 film)
{{Infobox film
| name           = The People Next Door
| image          = Peoplenextdoor.jpg
| image_size     = 
| caption        = Eli Wallach and Deborah Winters star in David Greenes The People Next Door. David Greene
| writer         = 
| narrator       = 
| starring       = Eli Wallach
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Avco Embassy Pictures
| released       = August 26, 1970
| runtime        = 93 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} David Greene. It stars Eli Wallach and Julie Harris. 

==Cast==
*Eli Wallach as Arthur Mason
*Julie Harris as Gerrie Mason
*Deborah Winters as Maxie Mason
*Stephen McHattie as Artie Mason
*Hal Holbrook as David Hoffman
*Cloris Leachman as Tina Hoffman
*Rue McClanahan as Della
*Nehemiah Persoff as Dr. Salazar

==Plot summary==

A married couple, Arthur and Gerrie Mason (Eli Wallach and Julie Harris) struggle with the realities of their imperfect marriage as they fight to save and rehabilitate their teenage daughter (Deborah Winters) from having been led into a life of drug addiction and ultimate committal to a mental ward. A son, Artie, is played by Stephen McHattie.

==See also==

*List of films featuring hallucinogens

==References==
 

==External links==
* 

 

 
 
 
 

 