The Deadly Camp
 
 
{{Infobox film
| name           = The Deadly Camp
| image          = TheDeadlyCamp.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Wide Sight Entertainment
| film name      = Shan gou 1999
| director       = Bowie Lau
| producer       = Lee Siu-kei
| writer         = Bowie Lau   Kenneth Hau Wai Lai
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = Tommy Wai
| cinematography = Andy Kwong
| editing        = Marco Mak
| studio         = Wong Jings Workshop Ltd.
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}

The Deadly Camp (Shan gou 1999) is a 1999 horror film written and directed by Bowie Lau, and co-written by Kenneth Hau Wai Lai.

== Plot ==
 prophylactic salesman (in actually, he and his associates are smugglers looking for Hwa). Boars henchmen, Pervert and Blowie, wander off to urinate, and accidentally pee on and set fire to a deformed boy they encounter. Pervert and Blowie run off, and the boy returns home, where he is tended to by his father, the bandaged man.

In the afternoon of the next day, Pervert is captured by the boy and his father, who dismember him with a chainsaw. Blowie tells Boar that he thinks something has happened to Pervert, but is ignored. While Pervert goes to hang out with the couples, Boar and his wife Mi Mi are captured by the bandaged man, who takes them to his home. In attempt to barter for his freedom, Boar tries to teach the mans son how to "screw" Mi Mi, but she enrages the man into killing her when she knocks the boy away. Boar escapes the house, but triggers a tripwire on his way out, and is impaled by punji sticks.

While going to wash dishes, Linda and Be Be find Boars body, and the latter is captured by the bandaged man. After Linda tells the others what happened, Soldier goes off to find Be Be, eventually stumbling onto the killers lair, where he is killed with his own knife. The next to die is Blowie, who goes off alone to find a cell phone he and Pervert had earlier lost. The killer gives chase to the other campers, but loses them, so he returns home, and decapitates Be Be when she rejects his sons advances.
 snare which swings him into tent spikes embedded in a tree. As the trio celebrate their victory, the killers previously unseen wife appears, and slices Lindas neck open with her husbands dropped chainsaw. The wife chases Ken to a cliff, which he knocks her off of.

With every member of the deranged family apparently dead, Ken and Winnie go back to their campsite, just as their boat arrives. After Ken and Winnie board the boat, the wifes hand shoots out of the water, and grabs the side of it.

== Cast ==
 Anthony Wong as Boar
* Keung-Kuen Lai as Ken
* Winnie Leung as Winnie
* Sammuel Leung as Blowie
* Tsz Sin Lam as Professor
* Yeung Fan as Mi Mi
* Chat Pui-Wan as Be Be
* Nam Wing Chan as Pervert
* Andy Tsang as Retarded Guy
* Ling Ling Chui as Linda
* Tam Kon-Chung as Hwa
* Koo-Ling Chung as Soldier
* Mak Siu-Wah as Retarded Guys Father
* Lin Koon-Hung as Retarded Guys Mother

== Reception ==

Beyond Hollywood wrote that The Deadly Camp was an enjoyable film with interesting villains, amusing characters and a plot that, while generic and clichéd, was refreshingly lacking in the pretension found in most post-Scream (1996 film)|Scream slasher films.  A score of three out of five was given by Hysteria Lives!, which said that while the film may have had a clichéd plot, it was still "a lot of dumb fun" with a formidable antagonist, nice cinematography, fast and frenetic action, and some genuinely tense moments. 

Monster Hunter criticized the film for its weak story, lack of characterization, disappointing bloodshed and bad actors, writing that "Other than the fact that this is a product of Hong Kong, The Deadly Camp is a fairly typical slasher movie, and while not the worst one youve ever seen, its nowhere near what youd call good."   So Good Reviews also responded negatively to the film, deadpanning that it had "crappy actors performing crappy characters under crappy direction" and extremely poor gore effects.  Another critical review was given by Soiled Sinema, which said The Deadly Camp "doesnt demonstrate aptness in any category" and suffered from annoying characters and a nonsensical plot. 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 