Behind the Mask (1932 film)
 
{{Infobox film
| name           = Behind the Mask
| image          = Behind the Mask.png
| caption        = Film poster John Francis Dillon
| producer       = Harry Cohn
| writer         = Dorothy Howell Jo Swerling Jack Holt Constance Cummings
| music          = 
| cinematography = Ted Tetzlaff
| editing        = Otis Garrett
| distributor    = Columbia Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 John Francis Jack Holt and featuring Boris Karloff.

==Cast== Jack Holt - Jack Hart
* Constance Cummings - Julie Arnold
* Boris Karloff - Jim Henderson Claude King - Arnold
* Bertha Mann - Edwards
* Edward Van Sloan - Dr. August Steiner / Dr. Alec Munsell / Mr. X
* Willard Robertson - Capt. E.J. Hawkes
* Thomas E. Jackson - Burke

==Plot==
A federal agent (Holt) goes undercover to infiltrate a drug smuggling operation headed by a mysterious Mr. X (Van Sloan), a criminal mastermind whose identity is unknown even to his henchmen. Mr. X is also running a bogus hospital where victims are killed on the operating table, and their coffins stuffed with narcotics. The drug-filled coffins are then buried in a cemetery.

==See also==
* List of American films of 1932
* Boris Karloff filmography

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 