Suburban Secrets (film)
{{Infobox film
| name           = Suburban Secrets
| image          = Secrets2004.jpg
| alt            =  
| caption        = Cover to two-disc Special Edition DVD Joe Sarno
| producer       = Michael Raso
| writer         = Joe Sarno
| starring       = Isadora Edison Tina Tyler Kay Kirtland John Samuel Jordan Chelsea Mundae
| music          = Pink Delicates
| cinematography = M.A. Morales
| editing        = Brian McNulty
| studio         = 
| distributor    = EI Independent Cinema
| released       =   
| runtime        = 83 minutes (TV version) 153 minutes (directors cut)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 softcore pornographic film, written and directed by Joseph W. Sarno.  The film stars Isadora Edison, Tina Tyler, Kay Kirtland, and Chelsea Mundae.  Seduction Cinema regular A.J. Khan also appears.

==Plot overview== nude model, returns to her small-town hometown after learning her ex-boyfriend has begun a relationship with her aunt Cynthia.

==Synopsis==
During a photo shoot, nude model Laura (Isadora Edison) receives a phone call from her sister Winnifred (Chelsea Mundae).  Laura learns that her ex-boyfriend, high-powered attorney Nelson Nyland (John Samuel Jordan), is dating her aunt Cynthia (Tina Tyler); not only that, they are making plans to move in together.  While she tells Winnie thats shes pleased, Laura is emotionally hurt.

After hanging up, Laura asks her photographer, Jennine, for a few weeks off.  Jennine agrees, commenting that she has some good shots for her editor.  Intercut with a montage of Laura and Jannine having sex, Jannine comments that she has "an awful schoolgirl crush" on Laura.  Laura is initially put off, but then passionately kisses Jannine.

Meanwhile, in Meadow Springs, Nelson visits his law partner, and sister, Judith.  Judith invites Nelson out to a romantic dinner, but Nelson explains that he is expected by Cynthia.

At Cynthias cabin, Nelson arrives to find Cynthia seductively rubbing herself.  Making himself comfortable, Nelson approaches her from behind and cups her breasts.  After several minutes of foreplay, they retire to the bedroom.

At a cafe, Winnie asks her co-worker Louise (A.J. Khan) why she pushed Winnie so hard into telling Laura about Cynthia and Nelson.  Louise admits that she is a potential novelist, and that she is writing an expose about the towns sexual goings-on.  The scene then intercuts with both Nelson and Cynthia, and Laura and Jannine having sex.

==Production==
 Misty Mundae and Julian Wells. {{cite video
  | people = 
  | title = Inside Suburban Secrets featurette
  | medium = Special Edition DVD
  | publisher = EI Independent Cinema
  | location = 
  }}   Mundae initially agreed, but in time backed out, wanting to get out of the porn industry.   Wells left the project soon afterwards. 

To replace his stars, producer Michael Raso turned to redhead Isadora Edison, who had made an impression on him in the film SpiderBabe.   For the role of Cynthia, he wanted an older woman and turned to porn veteran Tina Tyler.  Tyler had worked with Joe Sarno before,    and enjoyed the script.  Since the film dealt with incest, the first question Tyler asked was "are you sure the content is legal?"   Sarno countered that as long as the actors werent related, they should be fine. 

As with most, if not all, of Joe Sarnos films, the sex scenes were written around the plot, instead of the plot written around the sex scenes. 

===Directors cut===
The film was released on home video in a "Hot TV Cut," which was the film as it aired on cable, and a "directors cut" with about an hour of new footage.

==References==
 

== External links ==
*  

 
 
 
 
 