Karuvelam Pookkal
{{Infobox film name               = Karuvelam Pookkal கருவேலம் பூக்கள் image              =Karuvelam_Pookkal.jpeg image_size   =190px director           = Poomani writer             = Poomani starring  Nasser Raadhika Radhika Thalaivasal Vijay Charle, producer           = National Film Development Corporation of India distributor        = music              = Ilaiyaraaja cinematography     = Thangar Bachan editing            = B. Lenin and V. T. Vijayan released           = 1996 runtime            = 130 Minutes country    = India, Tamil Nadu language  Tamil
|}}

Karuvelam Pookkal ( n Tamil film, directed by Poomani. It was jointly produced by National Film Development Corporation of India, and Doordarshan a government agency. It stars Nassar|Nasser, Raadhika Sarathkumar|Radhika, Thalaivasal Vijay and Charle among others. The film is in Tamil Nadu Village drama film. The films Music is composed by Ilaiyaraaja. 

==Plot== director Poomani, NFDC and Doordarshan.

Karisalkulam, a small village, is inhabited mainly by Farmers. When the arid land offers them nothing but penury, enter the match factory owners from nearby villages. They lure the credulous, illiterate villagers into sending their daughters to work at the factories from dawn to much after dusk. The initial euphoria of a steady income givesway to despair, as is clearly brought out through the story of Nallamuthu (Nassar|Nasser), his wife Vadivu (Raadhika Sarathkumar|Radhika) and their three children. Mariyappan (Charle) is the Match factory agent in charge of finding child labourers for the factory. While the children slog in the factories the men of the household become lazy appendages who spend their time getting drunk. Nasser, as the cynical father, who does not wish to get his daughter married because he does not want to forgo the income, has rendered a splendid performance in Karuvelam Pookal. Mention must be made of the scenes in which he cries aloud at his tragedies and when his impotent anger is turned towards the agents of the match factories. Radhikas is another sterling portrayal. In the role of a responsible mother caught between her husbands apathy and avarice and her childrens untold suffering, she indeed lives the character. Sonia is the eldest daughter, Dhanalakshmi. Caught in the quagmire of drudgery and in the strangulating web of poverty, she seeks a way out, but sadly the respite is too short lived. A convincing essay by the young Actress. The visuals of the opening sequences transport you to the days of P. Bharathiraja|Bharatirajas movie 16 Vayadhinilae. The beautifully captured rustic ambience is another creditable show by Thangar Bachan who has wielded the camera. The dialect, the innocent humour, the openness and the gullibility - Poomani presents them all with absolute vividness.
The factories need only young girls and the boys therefore escape the hardship. After stressing on this point throughout, it is strange to see the boy accompany his sister to work, in the end. The fragrance of Karuvelam Pookkal is bound to last in the minds of the discerning audience. A film that makes you think. 

==Cast== Nasser as Nallamuthu. Radhika as Vadivu.
* Charle as Mariappan.
* Thalaivasal Vijay as Teacher. Shakthi as Thangavelu. Soniya as Dhanalakshmi.

==Lyrics==

The lyrics for the song Kaalayila Kann Muzhichaen (Translation: In the morning, I woke up) are particularly touching and the soulful melody adds to the effect. Ilaiyarajas music in a bucolic setting is an exotic combination, as Karuvelam Pookkal clearly proves.

==Awards==
* Tamil Nadu State Film Award Special Prize in 1996 

==International Film festivals ==
The movie was screened in the following international film festivals:

* Zanzibar International Film Festival in 1998. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 