The Artist's Dilemma
{{Infobox film
| name           =  The Artists Dilemma
| image          =  The Artists Dilemma (1901) - yt.webm
| image size     = 
| caption        = 
| director       = Edwin S. Porter
| producer       = Edison Studios
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = December 14, 1901
| runtime        = 120 seconds
| country        = United States English intertitles
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
The Artists Dilemma is a 1901 silent, fantasy film directed by Edwin S. Porter for Edison Studios. It was filmed in New York City, New York, USA.  It is short film running two minutes.  It features an artist asleep in his studio and his dream of a clock opening and a beautiful woman coming out of it.

==See also==
* List of American films of 1901
*Edwin S. Porter
*Edison Studios

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 