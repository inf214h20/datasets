Waking Up in Reno
{{Infobox film
| name           = Waking Up in Reno
| image          = Waking up in Reno DVD.jpg
| caption        = DVD cover
| director       = Jordan Brady
| producer       = Ben Myron Robert Salerno Dwight Yoakam
| writer         = Brent Briscoe Mark Fauser
| narrator       = 
| starring       = Natasha Richardson Billy Bob Thornton Patrick Swayze Charlize Theron 
| music          = Marty Stuart
| cinematography = William A. Fraker
| editing        = Lisa Zeno Churgin
| distributor    = Miramax Films
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| gross          = $267,109 (US)   
}}  comedy drama Little Rock Reno to see a monster truck rally.

==Plot==
Lonnie Earl Dodd is a Little Rock car dealer who stars in his own cheesy television commercials. He and his wife Darlene are best friends with Roy and Candy Kirkendall, who are trying to start a family. When the two couples decide to drive cross-country to see a monster truck rally, Lonnie Earl pulls a new SUV off his lot and the four set off. En route, they stop at an Amarillo, Texas restaurant where Lonnie Earl is determined to win a free dinner by consuming a 72-ounce steak and all the trimmings within an hour. Darlene longs to see the Grand Canyon, but Lonnie Earl insists they stick to their schedule and refuses to fulfill her dream. It becomes increasingly clear Darlene is living timidly in her husbands shadow, kowtowing to his demands and accepting his verbal and emotional abuse without complaint.
 fertility test he took before leaving home. Roy is sterile, and therefore clearly not the father of Candys child.

Darlene notices an uneasy glance between Lonnie Earl and Candy and realizes the two have been having an affair. Devastated, she treats herself to a complete and very expensive makeover and goes to see Tony Orlando perform, determined not to let her insensitive husband rob her of this dream as well. Meanwhile, Roy is in the hotel lounge trying to make headway with Brenda, who unbeknownst to him is a high-class hooker. Eventually the two couples return to their suite, where they engage in loud arguments and Bare-knuckle boxing|fisticuffs. The following day they discover Darlene has found the ultimate way to avenge her husbands boorish treatment of her - she has donated the SUV he intended to sell when they returned home to be destroyed by an enormous, fire-breathing Robosaurus during the monster truck rally.  

In an epilogue we learn Roy and Candy are the parents of three children, the results of the fertility test having been incorrect. Lonnie Earl and a confident Darlene are equal partners in his business, and she has become the star of the still-cheesey ads he continues to make.

==Cast==
* Natasha Richardson ..... Darlene Dodd
* Billy Bob Thornton ..... Lonnie Earl Dodd
* Patrick Swayze ..... Roy Kirkendall
* Charlize Theron ..... Candy Kirkendall
* Holmes Osborne ..... Doc Tuley
* Penélope Cruz ..... Brenda

==Critical reception==
Roger Ebert of the Chicago Sun-Times rated the film 1½ stars, calling it "another one of those road comedies where Southern roots are supposed to make boring people seem colorful". He continued, "Well, they could be, if they had anything really at risk. But the film is way too gentle to back them into a corner. Theyre nice people whose problems are all solved with sitcom dialogue, and the profoundly traditional screenplay makes sure that love and family triumph in the end." Despite finding the characters to be "pleasant", and feeling that "in some grudging way we are happy that theyre happy," he ultimately declared that "nothing in Waking Up in Reno ever inspired me to think of its inhabitants as anything more than markers in a screenplay."  

Kevin Thomas of the Los Angeles Times observed, "The one thing that can be said of Waking Up in Reno is that its rigorously consistent. Every note rings false, for writers Brent Briscoe and Mark Fauser have overlooked no stereotypes or clichés of small-town blue-collar speech, behavior or tastes. Because they have not drawn from life but from a zillion other contemporary middle Americana movies and TV shows, their characters are so many times removed from reality that it is hard to blame director Jordan Brady for relentlessly condescending to their characters and plot. (This picture is way too heavy-handed to pass for satire.)"  

Sean Axmaker of the Seattle Post-Intelligencer noted "the tepid script is neither satire nor farce, and the soap-opera twists are far too tame to spark the material. With the low-gear direction by Jordan Brady, you might think he has some heavy hauling to do, but the teary confessions and screechy screaming bouts are all sound and no fury . . . This half-baked production sat on Miramaxs shelf for a couple of years. Its no more done now than then, merely more stale."  

Peter Travers of Rolling Stone said the film "offers big, fat, dumb laughs that may make you hate yourself for giving in. Ah, what the hell. The whole cast, directed by Jordan Brady with no restraint, is slumming . . . Thornton plays this low-ball farce with deceptive, masterful ease. Appreciate it."  

Todd McCarthy of Variety (magazine)|Variety called the film "a hillbilly romantic comedy in which the hillbillies show up but the romance and comedy never do" and "a real what-were-they-thinking effort." He added, "Given the complete lack of urgency and inspiration in the material,   filmmakers have tried to give their work a semblance of life by all manner of desperate means - animated maps, jumpy editing, jokey narration and slumber-arresting musical cues, to little avail."  

==Box office==
The film opened in 197 theaters in the United States and Canada on October 25, 2002. On its opening weekend it grossed $108,930, an average of $552 per screen, and ranked #45 at the box office. It was pulled from release after four weeks. 

==Home media==
Miramax Home Entertainment released the Region 1 DVD on April 8, 2003. The film is in anamorphic widescreen format with audio tracks in English and French. Bonus features include commentary by director Jordan Brady and screenwriters Brent Briscoe and Mark Fauser, deleted scenes with optional commentary, and The Making of Waking Up in Reno.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 