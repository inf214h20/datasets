Srigala Item
 
{{Infobox film
| name           = Srigala Item
| image          = Srigala Item poster.jpg
| image_size     = 170
| border         = 
| alt            = A film poster
| caption        = Poster
| director       = Tan Tjoei Hock
| producer       =The Teng Chun
| screenplay     = Tan Tjoei Hock
| narrator       = 
| starring       ={{plain list|
*Hadidjah
*Moh Mochtar
*Tan Tjeng Bok
}}
| music          = Mas Sardi
| cinematography = The Teng Gan
| editing        = The TS
| studio         = Java Industrial Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Srigala Item (Indonesian for Black Wolf, also advertised with the Dutch title De Zwarte Wolf) is a 1941 film from the Dutch East Indies that was directed by Tan Tjoei Hock and produced by The Teng Chun for Action Film. Starring Hadidjah, Mohamad Mochtar, and Tan Tjeng Bok, the films plot&nbsp;– inspired by Zorro&nbsp;– follows a young man who became a masked vigilante to take revenge against his conniving uncle. Srigala Item was a commercial success, which Misbach Yusa Biran credits to the plots use for escapism. A copy of the black-and-white film, which featured kroncong music, is stored at Sinematek Indonesia.

==Plot==
Through violence, Djoekri (Tan Tjeng Bok) is able to gain control of his brother Mardjoekis (Bissoe) wealth and plantation, Soemberwaras. The latter disappears, leaving behind his adult son Mochtar (Mohamad Mochtar). At the plantation, the young man is treated as a servant and often beaten by Djoekri and his right-hand man, Hasan. Djoekris son Joesoef (Mohamad Sani), however, leads a life of plenty.

Soon Djoekris activities are targeted by a masked man known as the "Black Wolf" ("Srigala Item"), who also foils Joesoefs attempts to woo Soehaemi (Hadidjah), whom Mochtar loves. Djoekri tires of the Black Wolfs interference and takes him on in a battle. Though Djoekri almost wins, ultimately the Black Wolf emerges victorious. It is later revealed that Mardjoeki remains alive and Mochtar was the Black Wolf. 

==Production== The Mark of Zorro (1940) .   credit a stage version of the bandit.}} as with the American bandit, the Black Wolf used a whip and wore all black.  The black-and-white film was shot by The Teng Chuns brother Teng Gan, with Hajopan Bajo Angin as artistic designer. 

The film starred Hadidjah and Mohamad Mochtar, who had first acted together in JIFs Alang-Alang (film)|Alang-Alang (Grass; 1939) and were promoted as a celebrity couple in competition with Raden Mochtar and Roekiah of Tans Film.  It also featured Bissoe, Tan Tjeng Bok, and Mohamad Sani.  Bissoe had made his feature film debut with JIFs Oh Iboe in 1938,  while Sani had made his debut for JIF in 1940.  Tan Tjeng Bok, a former stage star with Dardanella (theatre company)|Dardanella, made his feature film debut through Srigala Item. 

Music for Srigala Item was done in the kroncong (traditional music with Portuguese influences) style, popular at the time, by Mas Sardi.  Hadidjah is credited as singing two songs, "Ja. Ja. Ja. Ja." ("Yeah. Yeah. Yeah. Yeah.") and "Termenoeng-Menoeng" ("Pensive");  Tan Tjeng Bok is also recorded as performing a song.  

==Release and reception==
  films released by JIF and its subsidiaries that year.  It was advertised, sometimes under the Dutch name of De Zwarte Wolf, as sensational, full of action, and mysterious"   and marketed for all ages.  The film was released in Batavia (modern day Jakarta), the capital of the Indies,  and by July 1941 it had reached Surabaya, in Eastern Java,  and Singapore, then part of British Malaya. 

An anonymous review of Srigala Item in the Surabaya-based daily Soerabaijasch Handelsblad was positive, predicting the film would be a commercial success.  This prediction was fulfilled, as Srigala Item reached extensive audiences&nbsp;– mostly lower-class native Indonesians|natives.  The Indonesian film historian Misbach Yusa Biran credits this success to escapism.  He writes that the films theme of an oppressed, financially destitute youth who is able to control his destiny by becoming a masked vigilante, allowed viewers to be able to see themselves as the Black Wolf, and thus take revenge against those who had wronged them. 
 Japanese occupation of the Indies began in early 1942, leading to the closing of all but one film studio. The cast of Srigala Item remained with JIF until the company closed, as did Tan.  According to JB Kristantos Katalog Film Indonesia (Catalogue of Indonesian Films), a copy of Srigala Item is stored at Sinematek Indonesia in Jakarta.  The reels have been damaged by acid, leading to part of the film being excised. 

==Explanatory notes==
 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
* {{cite book
  | title = Snapshots of Indonesian Film History and Non-theatrical Films in Indonesia
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  |url=http://books.google.co.id/books?id=yeFkAAAAMAAJ&q=%22Srigala+Item%22+-wikipedia+-blogspot+-download+-facebook&dq=%22Srigala+Item%22+-wikipedia+-blogspot+-download+-facebook&hl=en&sa=X&ei=2IKoUfujLaOEiAKMp4DgCA&redir_esc=y
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = c. 1986
  | oclc =  	20550138
  | ref = harv
  }}
*{{cite book
 |url=http://books.google.ca/books?id=sl92GYNaKJIC
 |title=A to Z about Indonesian Film
 |language=Indonesian
 |isbn=978-979-752-367-1
 |last=Imanjaya
 |first=Ekky
 |ref=harv
 |publisher=Mizan
 |location=Bandung
 |year=2006
}}
* {{cite web
  | title = Kredit Srigala Item
  |trans_title=Credits for Srigala Item
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s011-41-356648_srigala-item/credit
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 31 May 2013
  | archiveurl = http://www.webcitation.org/6H1WOl6Lx
  | archivedate = 31 May 2013
  | ref =  
  }}
* {{cite web
  | title = M. Sani
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b99b254a965e_m-sani/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 31 May 2013
  | archiveurl = http://www.webcitation.org/6H1WyErPS
  | archivedate = 31 May 2013
  | ref =  
  }}
*{{cite book
 |url=http://books.google.co.id/books?id=o95kAAAAMAAJ&q=%22Srigala+Item%22+-wikipedia+-blogspot+-download+-facebook&dq=%22Srigala+Item%22+-wikipedia+-blogspot+-download+-facebook&hl=en&sa=X&ei=2IKoUfujLaOEiAKMp4DgCA&redir_esc=y
 |title=Shadows on the Silver Screen: A Social History of Indonesian Film
 |last1=Said
 |first=Salim
 |location=Jakarta
 |publisher=Lontar Foundation
 |last2=McGlynn
 |first2=John H.
 |authorlink2= John H. McGlynn
 |last3=Boileau
 |first3=Janet P.
 |ref=harv
 |year=1991
 |isbn=978-979-8083-04-4
}}
* {{cite news
  | title =Sampoerna-theater "Srigala Item"
  | language = Dutch
  | url = http://kranten.kb.nl/view/article/id/ddd%3A011121701%3Ampeg21%3Ap010%3Aa0145
  | work = Soerabaijasch Handelsblad
  | publisher = Kolff & Co
  | location = Surabaya
  | date=23 July 1941
  | page=8
  | accessdate = 31 May 2013
  | ref =  
  }}
*{{cite news
 |last=Sembiring
 |first=Dalih
 |title=Saving Indonesian Cinema Treasures
 |work=The Jakarta Globe
 |url=http://www.thejakartaglobe.com/home/saving-indonesian-cinema-treasures/326208
 |archiveurl=http://www.webcitation.org/68hoRH0H4
 |date=26 August 2009
 |accessdate=26 June 2012
 |archivedate=26 June 2012
 |ref= 
}}
* {{cite web
  | title = Srigala Item
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s011-41-356648_srigala-item
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 25 July 2012
  | archiveurl = http://www.webcitation.org/69SPzzsGE
  | archivedate = 25 July 2012
  | ref =  
  }}
* {{cite web
  | title = Tan Tjoei Hock   Filmografi
  | trans_title = Tan Tjoei Hock   Filmography
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b99b0c50ac6d_tan-tjoei-hock/filmography
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 23 September 2012
  | archiveurl = http://www.webcitation.org/6AtFf8vrn
  | archivedate = 23 September 2012
  | ref =  
  }}
* {{cite news
  | title = (untitled)
  | language = Dutch
  | url = http://kranten.kb.nl/view/article/id/ddd%3A011121701%3Ampeg21%3Ap010%3Aa0145
  | work = Soerabaijasch Handelsblad
  | publisher = Kolff & Co
  | location = Surabaya
  | date=23 July 1941
  | page=10
  | accessdate = 31 May 2013
  | ref =  
  }}
*{{cite news
 |title=(untitled)
 |date=11 July 1941
 |page=6
 |location=Singapore
 |publisher=
 |work=The Straits Times
 |url=http://newspapers.nl.sg/Digitised/Article/straitstimes19410711-1.2.31.1.aspx
 |accessdate=31 May 2013
 |ref= 
}}
 

 
 
 
 

 
 
 