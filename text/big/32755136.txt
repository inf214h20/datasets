A Year of the Quiet Sun
 

{{Infobox film
  | name = A Year of the Quiet Sun 
  | image = 
  | caption =
  | director = Krzysztof Zanussi	
  | producer = Film Polski
  | writer = Krzysztof Zanussi Scott Wilson
  | music = Wojciech Kilar
  | cinematography = Sławomir Idziak
  | editing =Marek Denys
  | distributor = Film Polski  SPI International Poland (TV KinoPolska "Masterpieces of Polish Cinema")
  | released = September, 1984 (premiere at Venice Film Festival|VFF) 25 February 1985 (Poland)
  | country = Poland Germany United States
  | runtime = 110 minutes
  | language = Polish English
  | budget = 
  }} Polish film American soldier.
 Best Foreign Language Film. At the Venice Film Festival, the film was awarded the Golden Lion and Pasinetti Awards.

==Cast==
* Maja Komorowska as Emilia Scott Wilson as Norman
* Hanna Skarżanka as Emilias Mother
* Ewa Dałkowska as Stella
* Vadim Glowna as Herman Daniel Webb as David
* Zbigniew Zapasiewicz as Szary
* Zofia Rysiówna as Interpreter
* Janusz Gajos as Moonlighter
* Jerzy Stuhr as Adzio
* Gustaw Lutkiewicz as Bakery Owner
* Marek Kondrat as Malutki
* Jerzy Nowak as English Doctor

== See also ==
*Cinema of Poland
*List of Polish language films

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 