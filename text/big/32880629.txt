Assigned to his Wife
 
 
{{Infobox film
| name           = Assigned to his Wife
| image          = 
| image_size     = 
| caption        =  John Gavin John Gavin
| writer         = Agnes Gavin
| narrator       =  John Gavin Agnes Gavin
| music          = 
| cinematography = A.J. Moulton 
| editing        = 
| studio = John F. Gavin Productions
| distributor    = John Gavin
| released       = 22 September 1911 
| runtime        = over 4,000 feet  
| country        = Australia
| language       = Silent film
| budget         = £500 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 26. 
}}
 John Gavin. It is a Convicts in Australia|convict-era "military romantic melodrama".   

==Plot==
The film is set in the early 1840s in England and Van Diemens Land and concerns Jack Throsbie (John Gavin), an English soldier who is falsely accused of a crime and sentenced to Australia, where he befriends an aboriginal boy.

The chapter headings were:
#Colonel McGregors quarters at Aldershot. Capt. Danvers, the Colonels secretary, proposes marriage to Miss Bess Wilmot and is refused. Bess confesses her love for Jack Throsbie to whom she is secretly married.
#Husband and wife.
#A dastardly reminder. Falsely accused. Court martial held. Condemned to death. A brothers remorse.
#Transported to Van Diemens Land. 
#Eight months later. Captain Danvers arrives in Hobart as relieving officer In charge of the settlement. Mrs. Throsbie follow her unhappy husband to Van Diemens Land. Jack is assigned to his wife.
#A wolf in sheeps clothing. Jack Throsbies home. Dennies happiness.
#Captain Danvers shows his true colours. 
#A cry for help. Jack Throsbie defends his wife against Captain Danvers. In the Grip of the Law. A Wifes Despair. 
#The deserted hut. Yacka (A. Delaware) and the troopers.
#To the Bush. The troopers Discomfiture. The biter bitten. The bush camp. Yacka falls into the hands of the troopers. #Jack Throsbie to the rescue. A dying mans confession. The sensational swim across the river. The escape of the black boy Yacka. The Governor of the settlement (H. Benson) brings Tess news of Jack Throsbies innocence. 
#Down the rapids.
#Sensational dive by "the black boy" Yacka.
#Bess Throsbies home. Jacks brings news of Jack Throsbies capture. Husband and wife meet again. Yacka is pardoned.
#Death of Captain Danvers.
#Good-bye Van Diemens Land. England Once more. Honour to whom honour is due. Under the old flag. Yacka the black boy in England.
#Happiness at last. 

The main situations in the film were advertised as being:
*about to be shot for treachery; 
*the fight with the guards; 
*the blackboys wonderful escape; 
*swimming the river on horseback; 
*the fight on the river; 
*the flogging. 

==Cast== John Gavin as Jack Throsbie
*Agnes Gavin as Bess Wilmot
*H Harding as Captain Denvers
*Carr Austin as Colonel McGregor
*W Power as Harry Wilmot
*J Harris as Sandy McDougall
*H Benson as Governor of the Settlement
*F Henderson as Trooper McGuire
*A. Delaware as Yacko, the Black Boy
*Miss Daphne as Bertha McGregor

==Production==
The film was shot on location in bush near Sydney and at Gavins improvised studio in Waverly.   A highlight was a dive of 250 feet by Yacka off a cliff into a river. 

==Reception==
The film was reportedly successful at the box office. 

It was meant to be followed by another from Gavin The White Hope but it does not appear this was made. 

==References==
 

==External links==
* 

 

 
 
 
 
 