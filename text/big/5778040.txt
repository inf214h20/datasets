Om Shanti Om
 
 
 
{{Infobox film
| name           = Om Shanti Om
| image          = Omshantiom.jpg
| caption        = Theatrical release poster
| director       = Farah Khan
| producer       = Shah Rukh Khan   Gauri Khan
| writer         = Mayur Puri (Dialogue)
| screenplay     = Farah Khan  Mushtaq Sheikh
| story          = Farah Khan
| starring       = Shah Rukh Khan Arjun Rampal Kirron Kher Deepika Padukone Shreyas Talpade
| music          = Songs:  
| country        = India
| cinematography = V. Manikandan
| editing        = Shirish Kunder
| studio         = Red Chillies Entertainment
| distributor    = Red Chillies Entertainment
| released       = 9 November 2007
| runtime        = 170 minutes
| language       = Hindi
| budget         =     
| gross          =      
}}
 Indian film industry of both these eras.

The film was released in 2,000 prints worldwide making it the largest Indian cinematic release at the time.  Om Shanti Om was released on 9 November 2007 to mostly positive reviews from critics and record-breaking box office collections. It grossed   worldwide and thus became the highest-grossing Hindi film of all time at the time of its release.       

==Plot==
  Shah Rukh junior artist in the 1970s Hindi film industry, in love with film star Shanti Priya (Deepika Padukone). One evening, Om views the premiere of Shantis film Dream Girl, posing as Manoj Kumar. Later that night, Om becomes drunk and describes his vision of himself as a famous, wealthy film star winning the Filmfare Award for Best Actor to his friend Pappu (Shreyas Talpade) and some local children. At the shooting of a later film, Om rescues Shanti from a fire scene where of the fire has escaped control, and they become friends. One day, after following Shanti, he overhears her and producer Mukesh Mehras (Arjun Rampal) conversation, that Mukesh and Shanti have married in secret and Shanti is pregnant and expects a traditional wedding. Mukesh pretends to be happy with the news, and invites Shanti at the set of their soon-to-be-made film Om Shanti Om. Mukesh promises to cancel the film, reveal their marriage to the public, and have a grand wedding on the set; but instead traps Shanti on the set and sets it on fire to prevent a financial loss and to protect his career. Shanti tearfully begs Mukesh to release her but he leaves anyway.

Om, who happens to be at the same location that night, attempts to rescue Shanti, but is attacked by Mukeshs guards. After the guards leave, he again attempts to rescue Shanti and enters the set. He finds Shanti but he is thrown from the building by an explosion And Shanti dies.

Outside, Om is hit by a car owned by Rajesh Kapoor (Javed Sheikh), a well-known actor taking his wife Lovely to the hospital to give birth. At the hospital Om remembers his moments with Shanti as he dies due to this injuries while Lovely gives birth to a son, also named Om, who in adulthood becomes a popular movie star and a lookalike of Om, nicknamed O.K., and lives the luxurious life imagined by Om Prakash, but experiences pyrophobia and subconsciously inherits Om Prakashs memories. In acceptance of an award, O.K. unexpectedly delivers Om Prakashs drunken speech to the assembly. Pappu hears this on television. At a celebration of his award, O.K. is introduced by his father to Mukesh Mehra, and remembers the events that happened in his past life thereupon completely recovers Om Prakashs memories. He later reunites with Oms mother Bela (Kirron Kher) and Pappu and conspires to avenge Shantis death by making Mukesh confess his crime. 
 doppelganger of Shanti. Throughout the films shooting, O.K. and his friends arrange incidents to remind Mukesh of his crime. During the music launch of the film, O.K. taunts Mukesh by revealing, through a song, the extent to which he knows the story of Shantis death. But when Mukesh runs after Sandy thinking she is a ghost of Shanti, Sandy accidentally cuts her arm and bleeds. Seeing this, Mukesh confirms that she is not Shantis ghost. But when he is about to confront O.K, he is suddenly hit by the chandelier.

After Mukesh wakes up, O.K. confronts Mukesh but is shocked when Mukesh reveals that he knows who Sandy is. During the quarrel, Sandy shows up. O.K tries to send Sandy back but when Sandy reveals that Mukesh, after the fire ceased to burn, found that Shanti survived the fire, but he buried her alive below the sets chandelier, O.K is confused about how Sandy knows this. Mukesh attempts to shoot the Sandy shocked by the extent to which she knows about the murder, but Mukesh and O.K. fight, which results in yet another fire, just when O.K is about to kill Mukesh, Sandy stops him and the chandelier falls on Mukesh, killing him instantly. Pappu and Sandy join O.K. O.K. is shocked that he saw the same person and realizes it is Shantis ghost. The ghost smiles warmly towards Om and tearfully bids goodbye to him. The film ends with Shantis ghost leaving the scene and disappears into the light.

==Cast==
* Shah Rukh Khan as Om Prakash Makhija / Om Kapoor
* Deepika Padukone as Shanti Priya / Sandhya
* Arjun Rampal as Mukesh Mehra
* Kirron Kher as Bela Makhija
* Shreyas Talpade as Pappu Master
* Javed Sheikh as Rajesh Kapoor
* Asawari Joshi as Lovely Kapoor
* Yuvika Chaudhary as Dolly Bindu Desai as Kamini
* Satish Shah as Film Director
* Shawar Ali as Shawar Ali Khan
* Nitesh Pandey as Anwar

Special appearances during the song "Deewangi Deewangi" (in order of appearance)
 
* Rani Mukerji
* Zayed Khan
* Vidya Balan
* Jeetendra
* Tusshar Kapoor
* Priyanka Chopra
* Shilpa Shetty
* Dharmendra
* Shabana Azmi
* Urmila Matondkar
 
* Karisma Kapoor Arbaaz Khan
* Malaika Arora Khan
* Dino Morea
* Amrita Arora
* Juhi Chawla
* Aftab Shivdasani Tabu
* Govinda
* Mithun Chakraborty
 
* Kajol
* Preity Zinta
* Bobby Deol
* Rekha
* Ritesh Deshmukh
* Salman Khan
* Saif Ali Khan
* Sanjay Dutt
* Sunil Shetty
* Lara Dutta

 
Other Cameo appearances throughout the film (in alphabetical order)
 
* Abhishek Bachchan as himself
* Akshay Kumar as himself
* Ameesha Patel as Om Kapoors heroine at Filmfare Awards
* Amitabh Bachchan as himself
* Arshad Warsi as himself
* Bappi Lahiri as himself
* Bipasha Basu as herself
* Chunky Pandey as himself
* Dhananjay Singh as himself
* Dia Mirza as Om Kapoors heroine at Filmfare Awards.
* Farah Khan in the opening of the film.
* Feroz Khan as himself
* Gauri Khan as herself
 
* Hrithik Roshan as himself
* Karan Johar as himself
* Soumya Seth as Audience
* Koena Mitra as herself
* Mayur Puri as the director of Apahij Pyar
* Vishal Dadlani as the director of Mohabbat Man, a superhero film
* V. Manikandan as the director of Mind It, a parody of action films
* Virat Kohli as Ranbeer, villain of Apahij Pyar
* Priya Patil as Natasha, heroine of Apahij Pyar
* Rakesh Roshan as himself
* Rishi Kapoor as himself
* Shabana Azmi as herself
* Subhash Ghai as himself
* Yash Chopra as himself
 

==Production==
The film is based on reincarnation. It is inspired by Rishi Kapoors 1980 Hindi film Karz (film)|Karz and borrows many elements from it, including the title which is named after one of that films songs. In the beginning of Om Shanti Om, Rishi Kapoor can be seen dancing to the song of the same name. The second half of the film is inspired also by Bimal Roys Madhumati, which was the first Hindi film that revolved around the concept of reincarnation. Shooting of the film began in January 2007 at various locations in India. The film was released on 9 November 2007.

Om Shanti Om created a record of sorts by going in for an unheard of 2000 prints (worldwide) release. This was the highest number of prints (including digital) for any Indian movie at the time of its release.    Om Shanti Om set another record for registered pre-advance booking of 18,000 tickets in a chain of theatres in Delhi a few days before the advance booking was to start.   
 Eros International for an amount between Rs. 720–750 million.   Baba Films, production and distribution company, had offered a record Rs. 110 million for the rights to the Mumbai Circuit, easily surpassing the highest amount ever paid for the territory.   

A book, titled The Making of Om Shanti Om written by Mushtaq Sheikh, was released after the release of the film. The book gives an insight into the production and happenings behind the camera of the film.  

Deepika Padukones voice has been dubbed by sound artiste Mona Ghosh Shetty for this film. 
==Soundtrack==
{{Infobox album   Name       =  Om Shanti Om Type       = Soundtrack Artist     = Vishal-Shekhar Cover      = Omshantiomalbum.jpg Caption    = Album cover Released   =  18 September 2007 (India) Recorded   =  Genre  Feature film soundtrack Length     = 56:07 Label      =   T-Series Producer   = Shahrukh Khan & Gauri Khan Reviews    =  Last album = Cash (2007 film)|Cash  (2007) This album = Om Shanti Om (2007) Next album = Tashan (film)|Tashan (2008)
}}

{{Album ratings rev1      = Bollywood Hungama rev1Score =    rev2      = Rediff.com rev2Score =   
}}

The films songs were composed by the musical duo Vishal-Shekhar with lyrics by Javed Akhtar, whilst the background score was composed by Sandeep Chowta. Initially, A. R. Rahman was signed in to compose original songs and background score for the film. But the maestro opted out of this movie, as T-Series did not agree to share the copyrights of music with him and the lyricist.    The soundtrack for the film released on 18 September 2007.

{{Track listing
| extra_column = Singer(s)
| total_length = 61:23
| title1   = Ajab Si KK
| length1  = 4:03
| title2   = Dard-e-Disco
| extra2   = Sukhwinder Singh, Caralisa Monteiro, Nisha, Marianne
| length2  = 4:31
| title3   = Deewangi Deewangi
| extra3   = Shaan (singer)|Shaan, Udit Narayan, Shreya Ghoshal, Sunidhi Chauhan, Rahul Saxena
| length3  = 5:54
| title4   = Main Agar Kahoon
| extra4   = Sonu Nigam, Shreya Ghoshal
| length4  = 5:10
| title5   = Jag Soona Soona Lage Richa Sharma
| length5  = 5:31
| title6   = Dhoom Taana
| extra6   = Abhijeet Bhattacharya, Shreya Ghoshal
| length6  = 6:15
| title7   = Dastaan-E-Om Shanti Om
| extra7   = Shaan
| length7  = 7:08
| title8   = Dard-e-Disco (Remix)
| extra8   = Sukhwinder Singh, Caralisa Monteiro, Nisha, Marianne
| length8  = 4:38
| title9   = Deewangi Deewangi (Rainbow Mix)
| extra9   = Shaan, Udit Narayan, Shreya Ghoshal, Sunidhi Chauhan, Rahul Saxena
| length9  = 4:48
| title10  = Om Shanti Om (Medley Mix)
| extra10  = Sukhwinder Singh, Caralisa Monteiro, Nisha, Marianne, Shaan, Udit Narayan, Shreya Ghoshal, Sunidhi Chauhan, Rahul Saxena, Abhijeet Bhattacharya
| length10 = 6:06
| title11  = Dastaan-E-Om Shanti Om (Dark Mix) Shaan
| length11 = 6:21
| title12  = Om Shanti Om (Instrumental)
| extra12  = 
| length12 = 0:58
}}

==Box office==

===India===
Om Shanti Om opened across 878 cinemas in 2000 prints worldwide   Om Shanti Om nett grossed(after deducting entertainment tax)   in India.  

===Overseas===
The film collected US$2.78 million in the United Kingdom, US$3.6 million in North America and US$3.7 million collectively from the rest of the world, which resulted in total overseas collections of $10,080,000, the 4th largest of all time as of 2010. 

As a result of these collections, a worldwide gross of    was accumulated. 

==Reception and impact==

Taran Adarsh from Bollywood hungama gave the movie a 4 out 5 star rating stating "This Diwali, have a blast! (...) At the box-office, the film will set new records in days to come and has the power to emerge one of the biggest hits of SRKs career."  Hindustan Timess Khalid Mohamed gave the film 4/5 saying that the film is "A must for masala movie lovers.."  Tajpal Rathore of BBC gave it 4 out of 5 stars and stated, "Both a homage to and parody of Hindi Films, this cinematic feast delivered straight from the heart of the film industry will have you glued to your seats till the end."  Nikhat Kazmi of The Times of India gave 3.5 of 5 stars stating the film is a total paisa vasool and a true tribute to Karz (film)|Karz (1980).  Raja Sen of Rediff.com gave 3 and half stars and stated, "Om Shanti Om is an exultant, heady, joyous film reveling in Hindi Films, and as at most parties where the bubbly flows free, there is much silly giggling and tremendous immaturity. Youd do well to breathe in the filmi fumes, lift your own collar-tips upwards, and leave sense out of the equation. More cameos are written in than dialogues, so sit back and play spot-the-celeb. Or watch the Khan have a blast on screen."   Mark Medley of National Post gave 3 stars and stated, "The film is a mess for all the right reasons; elements of comedy, drama, romance, action and the supernatural are packed in. But really, the plot is just a vehicle to get from one song-and-dance number to the next."  AOL Indias Noyon Jyoti Parasara gave the movie 3 out of 5 stars stating, "The movie consists of all the elements that are essentially called the navratnas of Indian cinema&nbsp;– from joy to grief to romance to revenge. And she mixes these well to cook up a potboiler, which is sure to be a run away hit."  Rajeev Mansad of CNN-IBN give Om Shanti Om 3 out of 5 stars, stated "A special mention must be made for the films excellent dialogue which so cleverly incorporates Hindi Movess oldest clichés into these characters everyday parlance."  Sudish Kamnath of The Hindu stated, "Om Shanti Om is a light-hearted tribute to Hindi cinema the way we know it and love it, in spite of its flaws, improvisation and implausibility.  That apart, the movie is a hell of a party, a bits-and-pieces blockbuster strung together with a series of laughs, songs and dances. SRK shows us why hes the rock star of our times."  SearchIndia.com gave the movie the thumbs-down stating that "A dispiritingly commonplace theme of reincarnation packaged in a disjointed, tracing-paper-thin plot with ho-hum performances by the lead actors renders a mediocre movie that only addled fans of Hindi Film superstar Shahrukh Khan would love." 

Review aggregator Rotten Tomatoes gave Om Shanti Om a rating of 83%, based upon 6 reviews (5 fresh and 1 rotten). 

Nina Davuluris talent for Miss America 2014 was a Hindi Film fusion dance performed to  Dhoom Taana. It was the first time Hindi Film ever appeared on the Miss America stage and Davuluri is the first Indian American to win the competition.               

===Manoj Kumars complaints===
Om Shanti Om landed into controversy with actor Manoj Kumar, for showing his body double in a bad taste. Offended by the sequence, Kumar planned to sue the makers of Om Shanti Om. {{cite news
| title =Shahrukh Khan apologises to Manoj Kumar
| publisher =CNN-IBN
| date= 16 November 2007
| url = http://www.ibnlive.com/news/shah-rukh-khan-apologises-to-manoj-kumar/52439-8.html | accessdate =25 November 2007 }}  Kumar added, "Are the Mumbai police so stupid that they cant recognise Manoj Kumar and lathicharge him in the 70s when he was a star?”. {{cite news
| last =Deshpande
| first =Swati
| title =Hurt Manoj Kumar wants to sue SRK
| publisher =The Times of India
| date= 16 November 2007
| url =http://timesofindia.indiatimes.com/Om_Shanti_Om_Vs_Saawariya/Hurt_Manoj_Kumar_wants_to_sue_SRK/articleshow/2544784.cms| accessdate =25 November 2007  }}  Kumar also alleged that Shahrukh Khan is communal.  Later, in a press conference, Shahrukh Khan and director Farah Khan accepted their mistake and apologised for the matter. {{cite news
| title =SRK apologises to Manoj Kumar
| publisher =The Times of India
| date= 17 November 2007 Ram in everyone and ignore the Ravana." {{cite news
| last =Sharma
| first =Purnima
| title =Some shanti for Manoj
| publisher =The Times of India
| date= 23 November 2007
| url =http://articles.timesofindia.indiatimes.com/2007-11-23/news-interviews/27957668_1_manoj-kumar-farah-khan-shah-rukh-khan| accessdate =25 November 2007  }} 
 Sony Entertainment Television, to edit the Manoj Kumar look-alike scenes before showing the film on the channel on 10 August 2008. It also ordered that the film could not be shown in any media—TV, DVD or Internet—without the scene being deleted.  

===Alleged plagiarism===
On 7 August 2008, before its television release, scriptwriter Ajay Monga, moved the Bombay High Court alleging that the basic storyline of the film was lifted from a film script he had emailed to Shah Rukh Khan in 2006. According to the petition, "Monga, along with one more writer Hemant Hegde, had registered the script with the Cine Writers Association (CWA) in September 2005. In January 2008, Cine Writers Association (CWA) rejected Mongas appeal at a special Executive Committee meeting. Thereafter, he approached the court to stay the films screening on television. Though, on 6 August the court rejected Mongas plea for seeking a stay on the television telecast, it directed all the respondents including Shahrukh Khan, Farah Khan, Red Chillies Entertainment, Gauri Khan (director Red Chillies) and films co-writer Mushtaq Sheikh, to file their say by the next hearing on 29 September 2008.       

In November 2008, the Film Writers association sent a communication to Red Chillies and Ajay Monga that it had found similarities in Om Shanti Om and Mongas script. The similarities were more than mere coincidences according to Sooni Taraporewala who chaired a special committee that has investigated the case on behalf of the Film Writers association. 

Another allegation of plagiarism came from Rinki Bhattacharya, daughter of late Bimal Roy, who directed Madhumati. She threatened legal action against Red Chillies Entertainment and the producer-director of Om Shanti Om, as she felt that the films second half was similar to Madhumati, also a rebirth saga.  

==Awards==

===National Film Awards===
;Won
2008 – National Film Award for Best Art Direction&nbsp;– Sabu Cyril 

===Filmfare Awards===
;Won Best Female Debut&nbsp;– Deepika Padukone
*Best Special Effects&nbsp;– Red Chillies VFX
;Nominated  Best Actor&nbsp;– Shah Rukh Khan
*Best Actress&nbsp;– Deepika Padukone
*Best Director&nbsp;– Farah Khan
*Best Film&nbsp;– Red Chillies Entertainment
*Best Supporting Actor&nbsp;– Shreyas Talpade
*Best Music&nbsp;– Vishal-Shekhar
*Best Lyrics&nbsp;– Javed Akhtar for Main Agar Kahoon
*Best Lyrics&nbsp;– Vishal Dadlani for Aankhon Mein Teri
*Best male Playback Singer&nbsp;– K.K. for Aankhon Mein Teri
*Best male Playback Singer&nbsp;– Sonu Nigam for Main Agar Kahoon
*Best Dialogue&nbsp;– Mayur Puri

===Star Screen Awards===
;Won
* Best Newcomer Female&nbsp;– Deepika Padukone
* Best Choreographer&nbsp;– Farah Khan Jodi No. 1&nbsp;– Shahrukh Khan and Deepika Padukone
* Best Special Effects&nbsp;– Red Chillies VFX

;Nominated 
* Best Film
* Best Actor in a Negative Role&nbsp;– Arjun Rampal
* Best Director&nbsp;– Farah Khan
* Best Playback Singer Male&nbsp;– K.K. for Aankhon Mein Teri
* Best Background Music&nbsp;– Vishal-Shekhar
* Best Art Direction&nbsp;– Sabu Cyril

===Stardust Awards===
;Won Dream Director Award&nbsp;– Farah Khan
*Best Actor in a Negative role&nbsp;– Arjun Rampal
*Breakthrough performance Male&nbsp;– Shreyas Talpade

===Zee Cine Awards===
;Won
* Best Actor in a Negative Role&nbsp;– Arjun Rampal
* Best Female Debut&nbsp;– Deepika Padukone
* Best Choreography&nbsp;– Farah Khan
* Best Costumes&nbsp;– Manish Malhotra, Karan Johar and Sanjiv Mulchandani
* Best Visual Effects&nbsp;– Red Chillies VFX

;Nominated 
* Best Film
* Best Actor Male&nbsp;– Shah Rukh Khan
* Best Actor Female&nbsp;– Deepika Padukone
* Best Director&nbsp;– Farah Khan
* Best Actor in a Supporting Role Male&nbsp;– Shreyas Talpade
* Most Popular Track of the Year&nbsp;– Dard-E-Disco
* Best Playback Singer Male&nbsp;– KK for Aankhon Mein Teri
* Best Playback Singer Male&nbsp;– Sonu Nigam for Main Agar Kahoon
* Best Lyrics&nbsp;– Main Agar Kahoon
* Best Music&nbsp;– Vishal-Shekhar

===IIFA Awards===
;Won
* Best Art Direction&nbsp;– Sabu Cyril
* Best Makeup&nbsp;– Bharat-Dorris, Ravi Indulkar and Namrata Soni
* Best Debutant Female&nbsp;– Deepika Padukone
* Best Lyricist&nbsp;– Javed Akhtar
* Best Costume Design&nbsp;– Manish Malhotra, Karan Johar and Sanjiv Mulchandani
* Best Special Effects&nbsp;– Red Chillies VFX

;Nominated 
* Best Picture
* Performance in a Leading Role Female&nbsp;– Deepika Padukone
* Performance in a Negative Role&nbsp;– Arjun Rampal
* Playback Singer Male&nbsp;– K.K. for Aankhon Mein Teri Om

===Asian Film Awards===
;Won
* Best Composer&nbsp;– Vishal Dadlani, Shekhar Ravjiani

;Nominated
* Best Actress&nbsp;– Deepika Padukone

===Asia Pacific Screen Awards===
;Nominated Best Feature Film

===Annual Central European Hindi Film Award===
;Won 
* Best Film
* Best Director&nbsp;– Farah Khan
* Best Breakthrough Role (Female)&nbsp;– Deepika Padukone
* Best Costume Design&nbsp;– Manish Malhotra, Karan Johar and Sanjiv Mulchandani
* Best Special Effects&nbsp;– Red Chillies VFX
* Best Art Direction&nbsp;– Sabu Cyril
* Best Music&nbsp;– Vishal-Shekhar
* Best Playback Singer Male&nbsp;– Sonu Nigam for Main Agar Kahoon

===Planet-Bollywood Peoples Choice Awards===
;Won 
* Best Song&nbsp;– Ajab Si 
* Best Music Director&nbsp;– Vishal Shekhar 
* Best Choreographer&nbsp;– Farah Khan 
* Best Female Debut&nbsp;– Deepika Padukone 
* Best Male Singer&nbsp;– KK - Ajab Si 
* Best Female Singer&nbsp;– Shreya Ghosal
* Best Editing&nbsp;– Shirish Kunder



==References==
 

==External links==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 