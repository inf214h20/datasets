Live Today, Die Tomorrow!
{{Infobox film
| name           = Live Today, Die Tomorrow!
| image          = 
| caption        = 
| director       = Kaneto Shindo
| producer       = 
| writer         = Shozo Matsuda Kaneto Shindo
| starring       = Daijiro Harada
| music          = 
| cinematography = Kiyomi Kuroda
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

Live Today, Die Tomorrow! ( ) is a 1970 Japanese drama film based on the true story of Norio Nagayama. It was directed by Kaneto Shindo based on his own screenplay, and starred Daijiro Harada and Nobuko Otowa.

==Plot==
Michio Yamada (Daijiro Harada), a recent school graduate, is sent to Tokyo to work as a fruit-packer in a department store as part of a government programme. He takes a gun from a house on an American base and uses it to kill several people. 

The scene changes to Yamadas childhood. Yamada is born the child of a reprobate and a weak-willed woman (Nobuko Otowa). As a boy Yamada, experiences poverty and the rape of his sister at first hand.

==Cast==
* Daijiro Harada as Michio Yamada
* Nobuko Otowa as Take Yamada
* Keiko Torii as Sakie Hayashi
* Kiwako Taichi as Friend
* Kei Sato as Detective
* Daigo Kusano as Hanjiro Yamada

==Reception==
The film won the Golden Prize at the 7th Moscow International Film Festival in 1971.   

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 