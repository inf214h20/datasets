The Past-Master (film)
{{Infobox film
| name = Баш Майсторът ( )
| image = KirilGospodinov11.jpg
| director = Petar B. Vasilev
| writer = Atanas Mandadzhiev
| photography = Krum Krumov Yuriy Yakovlev
| music = Atanas Kosev
| studio = Bulgarian National Television
| released = 1970
| country = Bulgaria
| runtime = 65 min. Bulgarian
}}
The Past-Master (  / Bash Maystorat) is a Bulgarian satiric TV comedy film released in 1970, directed by Petar B. Vasilev. 
The movie turned the actor Kiril Gospodinov and his character Rangel Lelin - the past-master into one of the Trade Marks of the Bulgarian film art. The Past-Master became so popular that producers were made to consider sequels which eventually happened a decade later.

There are four more films released as sequels:
*Bash Maystorat na Ekskurziya / The Past-Master on Excursion (1980)
*Bash Maystorat Fermer / The Past-Master-Farmer (1981)
*Bash Maystorat na More / The Past-Master at the Seaside (1982)
*Bash Maystorat Nachalnik / The Past-Master - Boss (1983)

==Plot==
A research worker Robespier Galabov (Yuriy Yakovlev (Bulgarian actor)|Yakovlev) lives with his family in a small communal flat with shared kitchen and dreams about a self-contained home. Galabov meet Rangel Lelin (Kiril Gospodinov|Gospodinov),  the well-known amidst the localities as  the past-master, when he realized that the new municipal apartment they apply for wont be ready in the next decade. The Past-Master promises to build the private house in a month. After starting the construction, Rangel Lelin constantly blackmail Galabov for more money through treat of "putting the hat".

Finally the house was built but the inauguration become gloomy. It turns out that the chimney of the fireplace doesnt work properly.
After days of  luckless attempts to solve the problem the past-masters brother come into sight. Knowing the Lelins tricks he find a hat built in the chimney.

==Production==
 

Production company:
* Bulgarian National Television

Director:
*Petar B. Vasilev devoted his works to the comedy and more accurately to satirical one. He created sequence of films, with the main character The Past-Master, which popularity goes beyond the scope of their contemporaneity.

Writer:
*Atanas Mandadzhiev being an author of novels and novelettes he had a go at screenplays. The Past-Master work, a bitting comedy,  gained the sympathies of the public. Moreover, the main character turned into a byword for definite stratum that lives on the system defects.

Director of Photography
*Krum Krumov worked with some of the most interesting Bulgarian directors. Always subordinating the dramaturgy he succeeded in creating of his individual study of style.

Filmed: 1970
 
The film was released on DVD in 2000s.

==Cast==
* Kiril Gospodinov ...as Rangel Lelin the past-master  Yuri Yakovlev ...as Robespier Galabov the research worker 
* Valentina Borisova ...as Galabovs wife
* Veselina Nikolaeva ...as Lelins wife Todor Todorov
* Vladimir Davchev

==Response==
The film was subsumed among the 50 golden Bulgarian films in the book by the journalist Pencho Kovachev. The book was published in 2008 by "Zahariy Stoyanov" publishing house.

==References==
*Pencho Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008
* 

==External links==
*  

 
 
 
 