Kinaram
{{multiple issues|
 
 
 
}}

{{Infobox film|
| name     = Kinnaram
| image          =
| director       = Sathyan Anthikkad
| producer       = 
| writer         = P. Balakrishnan
| starring       = Sukumaran Poornima Jayaram Nedumudi Venu Jagathi Sreekumar
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| country        = India
| language       = Malayalam
}}

Kinnaram is a Malayalam comedy film released in 1983. Sukumaran, Nedumudi Venu, Jagathi Sreekumar, Poornima Jayaram, Sukumari and Sankaradi appeared in main roles. Directed by Sathyan Anthikkad, this film is story of two bachelor friends who happened to fell in love with a same girl. Mammootty appeared in just one scene in this film.

==Plot==
Unni (Nedumudi Venu) and Sethu (Sukumaran) are two bachelor friends staying together in Chennai. Though irritating at sometimes, with his music talents, the house owner Varma (Jagathi Sreekumar), a music director is always ready to financially support them at times. One day, Unni gets a letter from his father, asking him to receive Radha (Poornima Jayaram), daughter of his friend  who will be arriving at Chennai to join at a company for a job. At railway station, both Unni and Sethu fall for her and both try all methods to woo her. Radha is accommodated at the house of Mary (Sukumari), a colleague of theirs, who is also a sister-like figure for them. As she approached late after the joining date, Radha is denied the job. Sethu introduces her to V.N Nair (Sankaradi), his manager, also a womanizer and gets her a job at his company. This creates a friction in the friendship between Unni and Sethu. Both, but fails to express their feelings to her, instead fight each other bitterly over issues, leading even to physically attacking each other. One day Radha introduces them to  Balachandran (Mammootty), whom she addresses as her fiancee and asks them both to attend her wedding without fail. Realizing their foolishness, Sethu and Unni patches up and reaches back home happily. But at home, Sethu gets a letter from his father informing the arrival of daughter of one of his friend in Chennai. As both rushes to railway station to pick her up, the cards starts rolling.

==Songs Written by Jagathy Sreekumar==

Jagathi Sreekumar who played the role of Varmaji, a small time music composer at chennai, has penned various songs, which he sings in many scenes of the movie.
 Pistah Suma Kira- This song has been remixed as a promotional song for the 2013 movie, Neram.
 

2. Otta Pathrathil, Vaa Kuruvi and Aruthe

 

 
 
 
 


 