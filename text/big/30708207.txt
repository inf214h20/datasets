Santiago Files
{{Infobox film
| name           = Santiago Files
| image          = OfficialPoster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Cetywa Powell
| producer       = 
| writer         = Cetywa Powell
| narrator       = Malcolm McDowell
| starring       = Ariel Dorfman Isabel Allende Judd Kessler
| music          = Paul Foss
| cinematography = Ammon Ehrisman Tim Laurel
| editing        = Drew Lahat
| studio         = 
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Santiago Files, directed by   narrates the film. The film premiered in September 2011.

The documentary is in the permanent collection of the American Documentary Film Festivals archives (AmDocs), housed at the Rancho Mirage Library in Palm Springs.

==Film Festivals, Screenings, Availability==
* American Documentary Film Festivals Archives (AmDocs), Rancho Mirage Public Library, Palm Springs (June 2013)
*    
* Santa Cruz Film Festival (May 2012)] 
* Landmark Theatres Nuart (Oct, 2011)
*International Crime And Punishment Film Festival, Istanbul, Turkey (Sep 23-30, 2011) 

==Subject matter==

CIA documents on Salvador Allende were released to the public in 2000. Santiago Files uses CIA documents, taped conversations with Richard Nixon discussing Allende, interview clips with former Director of the CIA, Richard Helms, interview clips with Henry Kissinger, and interview clips with Salvador Allende to tell its story.

"The most memorable conclusion I can recall from the film  , was that Allende had been a leader – and whenever a leader arises in Latin America, the US finds a way to get rid of him. We played a role, but the Chileans made this happen. We invested $10 million in supporting opposition groups and media. Even in that time, it wasnt very much. The fact is that after the decisions he made in the first year, time was not on his side. Unless he could control his own coalition – which he couldnt – the downward cycle was just going to continue." --Judd L. Kessler (Regional Legal Advisor at the U.S. Embassy, 1970–1973)

==Contributors==
Ariel Dorfman 
Chilean Author and Playwright. Salvador Allendes cultural advisor (1970–1973)

Isabel Allende 
Chilean Author. Salvador Allendes niece

Judd L. Kessler 
Regional Legal Advisor at the US Embassy in Chile (1970–1973)

==References==
* NBC News in Turkey releases festival line-up of films  

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 