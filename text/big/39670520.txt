Un Pecado Por Mes
{{Infobox film
| name           = Un pecado por mes
| image          =
| image_size     =
| caption        =
| director       = Mario C. Lugones
| producer         = Francisco Oyarzábal
| writer         = Julio Porter
| narrator       = 
| starring       = Susana Canales Norma Giménez Hugo Pimentel Miguel Gómez Bao
| music          = George Andreani
| cinematography = Alfredo Traverso
| editor         = Jacobo Spoliansky
| studio         = Lumiton
| distributor    = 
| released       = May 18, 1949
| runtime        = 78 minutes
| country        = Argentina Spanish
| budget         =
}}
 Argentine comedy film directed by Mario C. Lugones and written by Julio Porter. It premiered on May 18, 1949. 

==Plot==
Paloma and Marcel simulate toward each other that they are rich as they develop a whirlwind romance. She works as a secretary at a prominent auction house. He is a full-time student whose poor parents struggle to pay his tuition. To impress her, Marcel claims to be from a family of wealthy ranchers. To impress him, Paloma claims to be the daughter of Belisario Quintana, her wealthy employer who, apart from being her boss, is also her familys landlord. As the two get closer, their deceptions begin to unravel.

==Cast==
*  Pedro Vargas 
*  Susana Canales 
*  Norma Giménez
*  Hugo Pimentel
*  Miguel Gómez Bao Diego Martínez
*  Ramón J. Garay
*  Herminia Mas
*  José Nájera (actor)|José Nájera
*  Pola Neuman
*  Ricardo de Rosas
*  Hedy Crilla
*  Enrique de Pedro
*  Tato Bores

==Release==
Released theatrically in Argentina in 1949, the film was released on DVD in 2009. 

==Recognition==

 

==References==
 

==External links==
*   at Allrovi
*   at the Internet Movie Database

 
 
 
 


 