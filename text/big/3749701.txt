The Source (2002 film)
{{Infobox Film 
| name        = The Surge/Source 
| image       = The Source (2002 movie poster).jpg 
| caption     = The Surge/Source
| writer      = Roger Kristian Jones,  S. Lee Taylor
| starring    = Mathew Scollon, Melissa Renée Martin, Edward DeRuiter, Alice Frank 
| director    = S. Lee Taylor 
| producer    = 
| distributor = The Asylum 
| released    = 2002 
| runtime     = 97 min  
| language    = English 
| music       = Mark David William Tabanou 
| awards      = 
| budget      = 
}}

The Source is a 2002 Film|movie, directed by S. Lee Taylor.  The story concerns four teenagers who obtain superhuman powers allowing them to control others.

It released under the name The Source (The Secret Craft) in the United Kingdom|UK.  In the United States|U.S., it was released on DVD as The Surge.

==Plot==

The film opens by following a moody  ; Ashley, Zacks sister; and Phoebe, a flower child.  They go to a forest and find a glowing rock. They gain powers from just stepping into its presence, and they use these powers to intimidate and humiliate people who have made fun of them over the years. 
*Zack gains telepathy.
*Phoebe gains telekinesis/psychokinesis.
*Ashley gains speech-induced psychic suggestion. ability to heal or hurt others/himself using his mind.

However, her power goes to Ashleys head; and she begins to take over the school, using mind control. She attacks her brother Zack and tries to kill Phoebe and Reese. They force her to heal Zack, but she forces Phoebe to levitate off the building and drop to the ground. Reese (because of his ability to heal or hurt) takes his own hearing away, when Ashley tries to control him; and he breaks the piece of the rock she had around her neck. He heals Phoebe; but, when they return to destroy the glowing rock, it has disappeared. Ashley is committed to a mental institution; but a former teacher brings her more of the glowing rock. The movie ends with her eyes turning blue, indicating that her powers have returned.

==Main cast==
*Mathew Scollon ....  Reese Hauser 
*Melissa Reneé Martin ....  Ashley Bainbridge 
*Edward DeRuiter ....  Zack Bainbridge 
*Alice Frank ....  Phoebe Lewis 
*Johnny Venocur ....  Jerry Hauser 
*Ronald Rezac ....  Principal McKinley 
*Roger Kristian Jones ....  Moss Man 
*Steven Glinn ....  Raimy 
*Mark Wood ....  Mr. Jessup 
*David Castro ....  Lane 
*Paul Taviani ....  Mr. Bartlett 
*Anna DeCardi ....  Miss Dunn 
*David Anders ....  Booji (as David Holt) 
*Cory Travalena ....  Bane 
*Aaron Deakins ....  Pugg

==External links==
* 

 
 
 