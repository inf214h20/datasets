Manjadikuru
{{Infobox film
| name = Manjadikuru
| image = Manjadikuru_film.jpg
| image_size = 300px
| caption = Film official poster
| director = Anjali Menon
| producer = Vinod Menon Anjali Menon
| screenplay = Anjali Menon Dialogues: Paliyath Aparna Menon Anjali Menon
| narrator       = Prithviraj Sukumaran Rahman Urvashi Urvashi
| editing = B. Lenin
| cinematography = Pietro Zuercher
| studio = Little Films 
| distributor = August Cinema 
| music = Ramesh Narayan Francois Gamaury (BGM) Kavalam Narayana Panikkar (lyrics)
| released =    (Kerala) 
| runtime = 137 Minutes
| language = Malayalam
| country =  India
}}

Manjadikuru ( ,  ) is a  . A shorter video version of the film was premiered at the 2008 International Film Festival of Kerala, and won the FIPRESCI Award for best Malayalam film and Hassankutty award for Best Debutant Indian director.     In 2009, it swept the awards at South Asian International Film Festival (SAIFF) at New York, winning five Grand Jury Awards - Best Film, Best Director, Best Screenplay, Best Cinematographer and Best Emerging Talent.     cinemaofmalayalam.net.  

==Plot==
Manjadikuru is a story of homecoming from late 1970s, when 10-year-old Vicky arrives at his grandparents home in rural Kerala to attend his grandfathers funeral. The disjointed family gathers together for the sixteen-day-long funeral period. During this period, Vicky discovers more about himself, his family and culture than he had expected to. The journey is narrated through the memories of an adult Vicky who returns to the same house to recount the experience. After some years the boy returns as a young man (Prithviraj Sukumaran|Prithviraj), to visit old grounds, and finds the Lucky red seeds scattered away. 

==Cast==

* Special appearance :- Prithviraj as elder Vicky Padmapriya as elder Roja

* The kids:-
::Sidharth as Vicky
::Vyjayanthi as Roja
::Rejosh as Kannan
::Arathi Sasikumar as Manikutty

* The family:-
::Thilakan as Muthashan/Appukuttan Nair
::Kaviyoor Ponnamma as Muthashi/Devayani Murali as Sanyasi Maaman / Murali
::Jagathi Sreekumar as Unni
::Bindu Panicker as Ammu Urvashi as Sujatha
::Sagar Shiyaz as Hari Rahman as Raghu
::Sreedevika as Latha
::Praveena as Sheela
::Harishanth as Ravi
::Sindhu Menon as Sudhamani
::Thrissur Chandran as Vasudevan Nair
::Firoz Mohan as Balu
::Julia George as Sindhu
::Shrevya Kannan as Baby Seetha

* Other casts:-
::Poojappura Ravi as Lawyer
::Kannan Pattambi as Marimuthu
::Venu Machad as Vishnu/Shop owner
::Sreekumar as Police Inspector

==Release==
The film released in theatres in May 2012, after winning awards in film festivals. For the commercial release, songs and additional scenes are added to the film to make it more appealing to people.   

==Soundtrack==
{{Infobox album 
| Name        = Manjadikuru
| Type        = soundtrack
| Artist      = Ramesh Narayan 150px
| Background  = 
| Recorded    =
| Released    = 2012
| Genre       =
| Length      =  
| Label       = Universal Music India
| Producer    = Little Films
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
|}} Pandit Ramesh Narayan.

{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || Ariya Vazhikalil || K. J. Yesudas|Dr. K. J. Yesudas
|- Chorus (Madhusree Narayan)
|-
| 3 || Manchaadipenne Vaadi || Shweta Mohan, Chorus (Arjun, Abhilash)
|-
| 4 || Chaadi Chaadi || Vijay Yesudas, Chorus (Arjun, Abhilash)
|-
| 5 || Manne Nampi || Kavalam Narayana Panikkar, Anil, Sathish
|- Prithviraj
|-
| 7 || Keralam Daivathinte Swantham || Prithviraj
|}

==Awards== Kerala State Film Awards 2012 Best Screenplay - Anjali Menon Best Child Artist - Vyjayanthi Best Sound Editor - M. R. Rajakrishnan 13th International Film Festival of Kerala 2008
* Hassankutty Award for Best Debut Director - Anjali Menon  
* FIPRESCI Prize for Best Malayalam Film in Malayalam Cinema Today section   
;6th South Asian International Film Festival 2009
* Grand Jury award for Best Film
* Grand Jury award for Best Director - Anjali Menon.
* Grand Jury award for Best Screenplay - Anjali Menon.
* Grand Jury award for Best Cinematography - Pietro Zuercher.
* Grand Jury award for Best Emerging Talent - Vyjayanthi.   
;Other awards
* Best Debut Direction by a Woman at New Jersey ISACF, New York, US
* Certificate of Appreciation, SCHLINGEL International Film Festival, Germany
* Official selection to the 2010 La Rochelle International Film Festival, France.   
* Official selection to Cinekid Festival, Amsterdam
* Official selection to Festival Internacional de Cine Para la Infancia Y La Juventud, Madrid, Spain
* Official selection to Indian Panorama, International Film Festival of India 

==Critical Reception==
Aswin J Kumar from The Times of India gave the movie 3.5 stars saying that "Manjadikkuru is a beautifully shot film that can be loved for its sheer earnestness and sense of belonging.".  Chris Fujiwara from International Federation of Film Critics|Federación Internacional de la Prensa Cinematográfica said that "It is a lesson in the ways of society but a metaphorical lesson for the audience, the kind of lesson that (as one imagines, or remembers, while watching Lucky Red Seeds) the cinema was invented to teach.  Ben Umstead from Twitch Film said that "Manjadikuru is undoubtedly one of the most beautiful films about childhood Ive seen. ". 

Veeyen from Nowrunning gave the movie 3 stars saying that "Anjaly Menons Manjadikkuru is an ode to the inherent innocence that once existed in us. Appealing equally to viewers of all ages, the flight of fancy that she offers to this incredible world that once was, is certainly not to be missed! ".   Metromatinee said "The film has been kept simple, highlights relationships and family values, and has to be seen with the whole family. Verdict: Small But Beautiful & Nostalgic ...!"  Parvathy Anoop from Deccan Herald stated that "Anjali Menon leaves no stone unturned as she explores elements of hypocrisy, love, sympathy and sorrow inside a large Nair family." 

Paresh C Palicha from Rediff.com gave the verdict of 3 stars saying that "Manjadikuru is enchanting. In this film, Anjali Menon succeeds in giving us a charming view of the adult world seen through the eyes of a child."  Sify gave the movie a verdict of Good stating that "Manjadikuru comes as a whiff of fresh air. Enjoy this fabulous debut of a new talent called Anjali Menon straightaway!".  Indiaglitz gave the movie a rating of 6.5 in 10 stating that "Manjadikkuru is a right addition to the list of well made films that came out this year. Just have a go into this scent of fresh air." 

==VCD/DVD Release==
The VCDs and DVDs were released by Satyam Audios on 17 June 2013.

==References==
 

==External links==
*  
* 
*  
* 

 
 
 
 