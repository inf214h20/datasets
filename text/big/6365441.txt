The Phantom Creeps
{{Infobox film
| name           = The Phantom Creeps
| image          = Phantomcreeps.jpg
| image_size     =
| caption        =
| director       = Ford Beebe Saul A. Goodkind
| producer       = Henry MacRae (associate producer)
| writer         = Willis Cooper (original story) George Plympton Basil Dickey Mildred Barish (screenplay)
| narrator       = Dorothy Arnold Robert Kent
| music          = Charles Previn
| cinematography = Jerry Ash William Sickner
| editing        = Irving Birnbaum Joseph Gluck Alvin Todd
| distributor    = Universal Pictures
| released       =  
| runtime        = 12 chapters (265 min)
| country        = United States English
| budget         =
}} 1939 Serial serial about a mad scientist who attempts to rule the world by creating various elaborate inventions. In a dramatic fashion, foreign agents and G-Man (slang)|G-Men try to seize the inventions for themselves.
 Dorothy Arnold Robert Kent.

It was adapted in DCs Movie Comics #6, cover date September–October 1939, the final issue of that title. {{cite news
  | last =Kohl
  | first =Leonard J
  | title =The Sinister Serials of Bela Lugosi
  | work =Filmfax magazine
  | pages =44
  | language =
  | publisher =
  | date =May–June 1996
  | url =
  | accessdate =  }} 

The first three episodes of The Phantom Creeps were lampooned during the second season of the TV show Mystery Science Theater 3000.

==Plot== Ed Wolff), robot spiders that can destroy life or paralyse it and he also has a deadly meteorite fragment from which he extracts an element which can induce suspended animation in an entire army. Foreign spies, operating under the guise of a foreign language school, are trying to buy or mostly steal the meteorite element, while his former partner, Dr. Fred Mallory, miffed that Zorka will not turn his inventions over to the U.S. Government, blows the whistle on him to Captain Bob West of the Military Intelligence Department. Tired of answering the door and saying no to the spies and the government, Zorka moves his lab. When his beloved wife is killed, Zorka, puttering around for his own amusement up to this point, is crushed and swears eternal vengeance against anyone trying to use his creations to make himself world dictator. And would have if not for his assistant Monk, an escaped convict virtually enslaved by Zorka, who is cowardly, treacherous and totally incompetent, and whose accidental or deliberate interference with Zorkas efforts repeatedly frustrates his masters own plans...

==Cast==
*Béla Lugosi as Dr. Alex Zorka. Lugosi received top billing for this, his final serial appearance. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 349–350
 | chapter = 14. The Villains "All Bad, All Mad"
 }}  Robert Kent as Capt. Bob West, G-Man (slang)|G-Man Dorothy Arnold as Jean Drew, reporter
*Edwin Stanley as Dr. Fred Mallory, Dr. Zorkas former partner
*Regis Toomey as Lt. Jim Daley, G-Man
*Jack C. Smith as Monk, Dr. Zorkas assistant
*Edward Van Sloan as Jarvis, foreign spy chief
*Dora Clement as Ann Zorka
*Anthony Averill as Rankin, a foreign spy
*Hugh Huntley as Perkins, Dr. Mallorys lab assistant Ed Wolff as The Robot

==Production== The Invisible Ray (look closely and youll see Boris Karloff), including scenes of Dr Zorka finding the meteorite in Africa.  As with several Universal serials, some of the stock music came from the Frankenstein (1931 film)|Frankenstein films.  The Phantom Creeps   car chase was itself used as stock footage in later serials. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 3. At This Theater Next Week
 | page = 95
 }}   Newsreel shots of the Hindenburg disaster were used as part of Dr Zorkas final spree of destruction after his robot, which is supposed to destroy the human race, is stopped due to the sabotage by Monk after being unleashed. 

Universal tried to improve serials by eliminating the written foreword at the start of each chapter.  This led to The Phantom Creeps being the first serial in which the studio used vertically scrolling text as the foreword. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 5. Shazam and Good-by
 | page = 138
 }} 

==Influence==
The innovation of the scrolling text version of the synopsis at the beginning of each chapter was used for the Star Wars films as the "Star Wars opening crawl".

The Rob Zombie song "Meet the Creeper" is based on this movie.  Zombie has used robots and props based on the design of The Robot in several music videos and live shows. The character Murray The Robot in Zombies animated movie The Haunted World of El Superbeasto is also based on The Robot.

==Chapter titles==
# The Menacing Power
# Death Stalks the Highways
# Crashing Towers
# Invisible Terror
# Thundering Rails
# The Iron Monster
# The Menacing Mist
# Trapped in the Flames
# Speeding Doom
# Phantom Footprints
# The Blast
# To Destroy the World
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 225
 }} 

==See also==
* List of film serials by year
* List of film serials by studio
* List of films in the public domain

==References==
 

==External links==
* 
*  (1949 TV film edited from serial)
*  (1949 TV film edited from serial)
* 
* 

 
{{Succession box Universal Serial Serial
| The Oregon Trail (1939 in film|1939)
| years=The Phantom Creeps (1939 in film|1939) The Green Hornet (1940 in film|1940)}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 