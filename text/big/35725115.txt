Christmas Tango
 
{{Infobox film
| name           = Christmas Tango
| image          = 
| caption        = 
| director       = Nikos Koutelidakis
| producer       = Yannis Exintaris
| writer         = Yannis Xanthoulis
| starring       = Yannis Bezos
| music          = 
| cinematography = Yannis Drakoularakos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}

Christmas Tango ( , Transliteration|translit.&nbsp;To tango ton Hristougennon) is a 2011 Greek drama film directed by Nikos Koutelidakis.    

==Plot==
The Christmas Tango is based on the novel by Yannis Xanthoulis.

An unexpected meeting between the sixty-five-year-old Lazaros Lazarou and a young man, on Christmas daybrings back hidden memories from 1970 when a sensual tango at a Christmas celebration at an army camp in Evros was the focus for the intersection of four lives: an introverted soldier; a harsh lieutenant; a strict and very conservative colonel; and Zoi Loggou (Vicky Papadopoulou), the colonels wife.

Zoi Loggous life is sufffocating until she discovers a secret admirer in the barracks who forces one of his soldiers to teach him how to dance the tango for the Christmas party so that he can get close to Vicky and reveal his love for her.

Coming back to the present and Zoi today is aged about sixty-five about 65 and also suffering from Alzheimer’s disease.

==Cast==
* Yannis Bezos as Manolis Loggos
* Antinoos Albanis as Lazaros Lazarou
* Yannis Stankoglou as Stefanos Karamanidis
* Vicky Papadopoulou as Zoi Loggou
* Eleni Kokkidou as Paraskevi
* Vassilis Risvas as Notis Voskopoulos
* Giannis Papagiannis as Anastasiou
* Foivos Kontogiannis as Balaskas
* Giorgos Papageorgiou as Doctor
* Vangelis Romnios as Bakas
* Thanos Hronis as Kallergis

==Accolades==
{| class="wikitable"
|+ List of awards and nominations 
! Award !! Category !! Recipients and nominees !! Result
|- Hellenic Film 2012 Hellenic Best Music||Yannis Aiolou|| 
|- Best Production Yorgos Georgiou || 
|- Best Costumes Yorgos Georgiou || 
|}

==References==
 

==External links==
*  

 
 
 
 
 

 
 