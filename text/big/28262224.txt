Charlie Chan in City in Darkness
{{Infobox Film
| name           = Charlie Chan in City in Darkness
| image_size     = 
| image	=	Charlie Chan in City in Darkness FilmPoster.jpeg
| caption        = 
| director       = Herbert I. Leeds
| producer       =  Robert Ellis Helen Logan
| narrator       = 
| starring       = Sidney Toler Lynn Bari Richard Clarke
| music          = 
| cinematography = 
| editing        = 
| studio         = Twentieth Century-Fox
| distributor    = Twentieth Century-Fox
| released       = November 15, 1939
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Charlie Chan in City in Darkness, also titled City in Darkness, is a 1939 mystery film starring Sidney Toler in his fourth performance as detective Charlie Chan.

==Plot==
Chan investigates a murder in Paris connected to the growing signs of impending war.

==Cast==
*Sidney Toler as Charlie Chan
*Lynn Bari as Marie Dubon
*Richard Clarke as Tony Madero (as Richard Clark)
*Harold Huber as Police Inspector Marcel Spivak
*Pedro de Cordoba as Antoine
*Dorothy Tree as Charlotte Ronnell
*C. Henry Gordon as Prefect of Police J. Romaine
*Douglass Dumbrille as B. Petroff (as Douglas Dumbrille)
*Noel Madison as Belescu
*Leo G. Carroll as Louis Santelle
*Lon Chaney, Jr. as Pierre
*Louis Mercier as Gentleman Max George Davis as Alex
*Barbara Leonard as Lola
*Adrienne DAmbricourt as Hotel Proprietress
*Frederick Vogeding as Captain (as Fredrik Vogeding)

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 