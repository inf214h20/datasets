7 Women
{{Infobox film
| name           = 7 Women
| image          = 7 Women 1966 poster.jpg
| image_size     = 220px
| caption        = 1966 theatrical poster by Reynold Brown
| director       = John Ford
| producer       = Bernard Smith John Ford
| writer         = Janet Green John McCormick
| based on       =  
| narrator       =
| starring       = Anne Bancroft Margaret Leighton Flora Robson Sue Lyon
| music          = Elmer Bernstein
| cinematography = Joseph LaShelle
| editing        = Otho Lovering
| distributor    = MGM
| released       =  
| runtime        = 87 min.
| country        = United States
| language       = English Mandarin
| budget         = $2.3 million Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 103-105 
| preceded_by    =
| followed_by    =
}}
 film drama John McCormick, based on the short story "Chinese Finale" by Norah Lofts. The music score was by Elmer Bernstein and the cinematography by Joseph LaShelle. This was the last feature film directed by Ford, ending a career that spanned approximately fifty years.

The film starred Anne Bancroft, Sue Lyon, Margaret Leighton, Flora Robson, Mildred Dunnock, Betty Field, Anna Lee, with Eddie Albert, Mike Mazurki and Woody Strode.

==Plot==
In rural Republic of China (1912-1949)|China, in 1935, all but one of the white residents of a remote Christian missionary post are women. Miss Agatha Andrews (Margaret Leighton) is the head of the mission, assisted by Miss Argent (Mildred Dunnock). Charles Pether (Eddie Albert) is a mission teacher; his peevish, middle-aged wife Florrie (Betty Field) is pregnant for the first time. Emma Clark (Sue Lyon) is the only young staff member.

Everyone is elated to learn that a much-needed doctor is arriving. However, they are all shocked to discover that Dr. D. R. Cartwright (Anne Bancroft) is a woman who smokes, wears pants and disdains religion. She and Andrews are soon at odds. Clark, who has led a very sheltered life, is fascinated by the newcomer, much to Andrews great dismay.

After she has settled in, Cartwright urges Andrews to provide money to send Florrie Pether to a modern facility, as she is too old to give birth safely in their primitive surroundings. Andrews refuses.

Meanwhile, there are rumors of atrocities committed by the bandit Tunga Khan (Mike Mazurki). Andrews is certain that the mission will be safe, as they are American citizens. She remains unconcerned even when the nearby Chinese garrison flees. After a nearby, even poorer British mission is sacked by Tunga Khan, Andrews reluctantly accepts survivors Miss Binns (Flora Robson), Mrs. Russell (Anna Lee) and Miss Ling (Jane Chang), but only for a short time, as she is unwilling to harbor those of any other denomination for long.

One night, Charles sees a fire on the horizon and hears gunfire, so he and Kim drive out to investigate. When the car returns and its horn is honked, someone opens the gate. Bandits on horseback charge in firing their guns and quickly take over the mission. Before he is shot in the back, Kim tells the women Charles was murdered when he tried to rescue a woman being assaulted by Tunga Khans men. The seven women are herded into a shed.

With Florrie in labor, Cartwright asks for her desperately needed medical bag. Tunga Khan offers to exchange it for her submission to him. The doctor agrees, and a baby boy survives his birth. After Cartwright goes to fulfill her end of the bargain, Andrews vilifies her, calling her the "whore of Babylon", among other things. The others, however, understand the sacrifice the doctor has made and why.

For entertainment, the bandits enjoy watching a wrestling match. When a lean warrior (Woody Strode) steps into the ring to face the winner of a bout, Tunga Khan insists on accepting the challenge himself. Tunga Khan breaks the mans neck, ending a possible threat to his leadership.

Cartwright manages to convince Tunga Khan to let the other women go. Before Miss Argent leaves, she sees the doctor hide a bottle that she had earlier called poison. She urges Cartwright not to do what she is planning, but to no avail. With the others safely away, Cartwright secretly poisons two drinks. After Tunga Khan drinks one, he immediately keels over dead. She utters, "So long ya bastard!" Then, after a moments hesitation, Cartwright drinks from the second cup.

==Cast==
* Anne Bancroft  as Dr. D. R. Cartwright
* Sue Lyon  as Emma Clark, Mission Staff
* Margaret Leighton  as Agatha Andrews, Head of Mission
* Flora Robson  as Miss Binns, Head of British Mission
* Mildred Dunnock  as Jane Argent, Andrews Assistant
* Betty Field  as Mrs. Florrie Pether, Charles pregnant wife
* Anna Lee  as Mrs. Russell, Mission Staff
* Eddie Albert  as Charles Pether, Mission Teacher
* Mike Mazurki  as Tunga Khan, Bandit Leader
* Woody Strode  as Lean Warrior
* Jane Chang  as Miss Ling, Mission Staff
* Hans William Lee  as Kim, Mission Staff
* H. W. Gim  as Coolie
* Irene Tsu  as Chinese Girl

==Acclaim==
Fred Camper, Richard Combs and Simon Galiero all rated it among the top ten greatest movies of all time. The film also appeared in several other lists. These include:
* Most Misappreciated American Films of All Time (1977, Andrew Sarris)
* Most Misappreciated American Films of All Time (1977, Pascal Bonitzer)
* Most Misappreciated American Films of All Time (1977, Serge Daney)
* Most Important American Films (1977, Enno Patalas)
* Most Important American Films (1977, Luc Moullet)
* Genre Favorites: Adventure (1993)
* Alternative Choices to Sight and Sounds 360 Films Classics List (1998)
* 100 Essential Films (2003–Present, Slant Magazine)
* Favorite Films (1975, Syndicat Francais de la Critique de Cinema)

Cahiers du cinéma voted it the 6th best film of 1966  and Andrew Sarris rated it the third-best of 1966 (only being beaten by Blow-up and Gertrud (film)|Gertrud). 

The film was voted by They Shoot Pictures, Dont They? as the 680th greatest film of all time, in a poll of 1,825 film critics, scholars, cinephiles, etc. and as well in a culmination of over 900 greatest film lists of all kinds, that were already existing.

==Production==
The original story Chinese Finale was presented as an episode of Alcoa Theatre in March 1960 with Hilda Plowright as Miss Andrews and Jan Sterling as Dr. Mary Cartwright. 

John Ford considered both Katharine Hepburn and Jennifer Jones for the role of Dr. Cartwright, and Rosalind Russell lobbied for the part, but eventually Patricia Neal was cast. Ford began the film on 8 February 1965 on the MGM backlot, but after three days of filming, Neal had a stroke. Anne Bancroft took over the role of Dr. Cartwright but Ford was unhappy with Bancroft and called her "the mistress of monotone".   Ford originally considered Carol Lynley for the role played in the film by MGM contract star Sue Lyon, whom the studio insisted on.  Shooting finished on April 12, only six days behind schedule. 

Ford chose Otho Lovering to edit the film; they had first worked together on Stagecoach (1939 film)|Stagecoach (1939). Lovering edited most of Fords feature films in the 1960s.

The film was not released until 1966.

==Analysis==
As Ford was a devout Catholic, the film shows the difference between the claim of being moral and the act of morals; the stark contrast between compassion and sacrifice to the austere holier-than-thou philosophy. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 