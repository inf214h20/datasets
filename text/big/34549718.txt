Welcome to New York (2012 film)
{{Infobox film
| name           = Welcome to New York
| image          = Welcome to New York The Movie.jpg
| caption        = 
| director       = Steven Tylor OConnor
| producer       = Steven Tylor OConnor Sean David
| writer         = Steven Tylor OConnor
| story          = Sean David
| starring       = Sherry Vine  Sean Paul Lockhart  Nicholas Page / Lauren Ordair  Ashleigh Murray  Megan Kane  Matthew Watson
| featuring          = Casper Andreas   Victor Cruz  Alex Ringler   Steven Tylor OConnor   Shacottha   Dasha Kittredge   Seanny Flynn   Ashley Layfield
| music          = Eric Williams   Brandon Hilton   Bryan Fenkart   Chris Salvatore 
| cinematography = Nick Massey
| editing        = Sam Dakil
| studio         = Gold Street Collective
| released       =  
| runtime        = 30 minutes
| country        = United States
| language       = English
| budget         = $10,000 (United States dollar|USD) 
}} comedy short film directed and written by Steven Tylor OConnor and based on story by Sean David. It stars Sherry Vine, Sean Paul Lockhart, Nicholas Page/ Lauren Ordair, Ashleigh Murray, Megan Kane, Matthew Watson with Casper Andreas, Victor Cruz and Alex Ringler. Welcome to New York is about unique first experiences in New York City. The film received generally favorable reviews from film critics.

==Plot==
The story of five young peoples unique first experiences in New York City.

==Cast==
* Sherry Vine as Dr. Kitty Rosenblatt
* Sean Paul Lockhart as Jake
* Casper Andreas as Christopher
* Matthew Watson as Ryan
* Ashleigh Murray as Simone
* Megan Kanev as Cassie
* Nicholas Page/ Lauren Ordair as Mick/ Lauren Ordair
* Victor Cruz as Jose
* Alex Ringler as Alex or "Cupcake"
* Shacottha as Crazy Person
* Steven Tylor OConnor as Scotty
* Sean David as Waiter
* Seanny Flynn as Stacey 	
* Dasha Kittredge as RoxAnn
* Ashley Layfield as Becky

==Production== Kansas City, Tivoli theatre as part of the "Kansas City LGBT Film Festival".

==Soundtrack==
*"City by the Sound" — Eric Williams
*"Set Fire to The Night" — Brandon Hilton
*"Empty Handed" — Bryan Fenkart
*"OK" — Bryan Fenkart
*"Dirty Love" — Chris Salvatore
*"I Want Your Sex" — Chris Salvatore

==Reception==
The film received generally positive reviews from film critics. Amos Lassen wrote "All of us love surprises and “Welcome to New York” is filled with them”. “Heading the cast is Sherry Vine as Dr. Kitty Rosenblatt, a very unorthodox psychiatrist who has quite a successful practice (and who is absolutely hysterical in her role)”. He add: “Steven Tylor O’Connor directed and he should be very proud of this clever little film. This is the second film of his that I have seen and I can say that he is one of the new directors to watch. If “Welcome to New York” is an omen of what we can expect from him then we are very lucky”. 

Scott’s Movie said, "Welcome to New York is a perfectly charming film”. “One of the funniest moments in the flick is the look on Nick Page’s face when Vine expresses befuddlement over the notion of working as a drag queen”. “Ms. Vine is very funny, and in a way that doesn’t rely on the inherent humor of a man dressing as a woman. For some reason, she reminds me a lot of the actor Sylvia Miles”. “The strongest  scenario involving a very appealing Megan Kane and Matthew Watson and an awkward first date with Sean Paul Lockhart and Alex Ringler that could have been drawn from a Seinfeld episode. Lockhart is particularly endearing as an apparent young innocent”. “Perhaps the best compliment I can pay O’Connor’s movie is that it ended too soon. It could easily be expanded to feature length. While it is clearly low-budget, it does not look cheap. And it has a nifty soundtrack that includes tracks by Eric Williams, Brandon Hilton, Bryan Fenkart and Chris Salvatore”. 

===Awards===
* Audience Favorite at the 2012 Louisville LGBT Film Festival for "Narrative Short".

==References==
 

==External links==
*  
*  
*   at Vimeo

 
 
 
 
 
 
 