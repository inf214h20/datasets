The Cat and the Fiddle (film)
{{Infobox film
| name           = The Cat and the Fiddle
| image          = The Cat and the Fiddle.jpg
| image_size     =
| caption        = Original sheet music
| director       = William K. Howard Sam Wood (uncredited) 
| producer       = Bernard H. Hyman Bella Spewack (screenplay)
| starring       = Ramon Novarro Jeanette MacDonald
| music          =
| cinematography = Charles G. Clarke Ray Rennahan Harold Rosson
| editing        = Frank Hull
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes
| country        = United States English
| budget         = $843,000 Turk, Edward Baron "Hollywood Diva: A Biography of Jeanette MacDonald" (University of California Press, 1998) 
| gross          = $455,000 (Domestic earnings)  $644,000 (Foreign earnings) 
}} romantic musical Broadway musical of the same name by Jerome Kern and Otto A. Harbach, about a romance between a struggling composer and an American singer. The film stars Ramon Novarro and Jeanette MacDonald in her MGM debut.

The final reel was filmed in the then newly perfected Technicolor|three-strip Technicolor process, previously used only in Walt Disney cartoons such as Flowers and Trees (1932).

==Cast==
*Ramon Novarro as Victor Florescu 
*Jeanette MacDonald as Shirley Sheridan 
*Frank Morgan as Jules Daudet  Charles Butterworth as Charles 
*Jean Hersholt as Professor Bertier 
*Vivienne Segal as Odette Brieux  Frank Conroy as The Theatre Owner 
*Henry Armetta as the Taxi Driver 
*Adrienne DAmbricourt as Concierge 
*Joseph Cawthorn as Rodolphe Rudy Brieux

==External links==
* 
* 

==Notes==
 
 
 

 
 
 
 
 
 
 
 
 
   
 
 


 
 