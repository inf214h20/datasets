Brain Drain (film)
{{Infobox film
| name           = Brain Drain
| image          = 
| alt            = 
| caption        = 
| director       = Fernando González Molina
| producer       = Daniel Écija Mercedes Gamero
| writer         = Curro Velázquez Álex Pina
| starring       = Mario Casas Amaia Salamanca Alberto Amarilla Gorka Lasaosa Pablo Penedo Canco Rodríguez Blanca Suárez Simon Cohen
| music          = Manel Santiesteban
| cinematography = Sergio Delgado
| editing        = Irene Blecua A3 Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = Spain
| language       = Spanish English
| budget         = 
| gross          = 
}} Spanish romantic-comedy A3 Filmss production.

The production started in 14 July 2008 in   (Brain Drain) in 2011.

== Plot ==
The film begins with an 18&nbsp;years old Emilio (Mario Casas) who tells his amorous misadventures with Natalia (Amaia Salamanca) due to all his physical problems (Dental braces|orthodoncy, orthosis, etc.) When he is 18 he finally gets rid of all his dental appareils and has the chance to confess to Natalia his love. But when she gets a student grant to attend Oxford University (England), all his friends try to help him by going to Oxford too with fake grants.
 English and their disabilities and quirks (one of the Emilios friends is Blindness|blind, other is paraplegic and the last one is a drug dealer). Nevertheless and despite their impediments, they make their best to help him to get closer to Natalia with disastrous results.

== Cast ==
*Mario Casas as Emilio Carbajosa Benito
*Amaia Salamanca as Natalia
*Alberto Amarilla as José Manuel "Chuli" Sánchez Expósito
*Gorka Lasaosa as Rafael "Ruedas" Garrido Calvo
*Pablo Penedo as Felipe "Corneto" Roldán Salas
*Canco Rodríguez as Raimundo "Cabra" Vargas Montoya
*Blanca Suárez as "Angelical voice"/Speaker
*Sarah Mühlhause as Claudia Simon Cohen as Edward Chamberlain Carlos Santos as Potro Reg Wilson as Professor
*Asunción Balaguer as Emilios grandmother
*Joan Dalmau as Emilio grandfather
*Álex Angulo as Cecilio Garrido
*Loles León as Rita Calvo
*David Fernández Ortiz as Loren Sánchez
*José Luis Gil as Manuel Roldán
*Mariano Peña as Julián Roldán Fernando Guillén as Natalias grandfather
*Antonio Resines as Natalias father
*Óscar Casas as Young Emilio Carbajosa Benito (11&nbsp;years old)
*Oliver Vigil as Young Emilio Carbajosa Benito (8&nbsp;years old)
*Andrea Diaz as Young Natalia

== Reception == State of Play. 

Meanwhile, in the international box office: in United States, the film got $1,614,121 in its first weekend at cinemas. 

The total income amounts to almost 7 mill€. becoming the biggest blockbuster Spanish film in 2009.

The film itself however, was panned by critics.  

== References ==
 

== External links ==
* 
* 

 
 
 
 