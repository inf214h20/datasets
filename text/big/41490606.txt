Spectres (film)
{{Infobox film
| name           = Spectres
| image          = Spectres.jpg
| director       = Phil Leirness
| producer       = Robert Ballo Bud Robertson
| writer         = Bud Robertson
| starring       = Marina Sirtis Dean Haglund Tucker Smallwood
| music          = John Boegehold
| cinematography = Robert Ballo
| editing        = Edward Bishop
| studio         = Shadowland
| distributor    = Lifetime Movie Network Xenon Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Spectres is a 2004 supernatural drama film directed by Phil Leirness and starring by Marina Sirtis, Dean Haglund and Tucker Smallwood.

==Plot==
Kelly is a 16 year old suicide survivor, her mother Laura Lee is at a loss when it comes to what should be done next so she takes Kelly away for the summer to a holiday home. But strange things start happening and Kelly starts acting like a different person. Kellys therapist, Dr. Halsey, seeks the assistance of a psychic.

==Cast==
* Marina Sirtis as Laura Lee
* Dean Haglund as Dr. Halsey
* Tucker Smallwood as Will Franklin
* Lauren Birkell as Kelly Webber
* Alexis Cruz as Sean
* Chris Hardwick as Sam Phillips
* Loanne Bishop as Suzanne
* Linda Park as Renee Hansen
* David Hedison as William
* Alexander Agate as C.J. Hansen
* Lillian Lehman as Fran Mullins
* Joe Smith as Mark
* Neil Dickson as Wally

==External links==
*  

 
 
 
 
 
 
 
 


 