Chakram (2005 film)
{{Infobox film
| name           = Chakram
| image          = Chakram poster.jpg
| writer         = Krishna Vamsi
| starring       = Prabhas Raju Uppalapati Asin Thottumkal Charmme Kaur Prakash Raj
| director       = Krishna Vamsi
| producer       = Venkatraju Sivaraju Om Prakash 
| distributor    =
| released       = March 25, 2005
| runtime        =
| language       = Telugu Chakri
| awards         =
| budget         =
}}
 Telugu film which released on March 25, 2005 and was written by Krishna Vamsi. Prabhas played the lead role while Asin Thottumkal, Charmme Kaur and Prakash Raj played supporting roles. It was also dubbed into Hindi under the same title by Manish Shah of Goldmines Telefilms.

== Plot ==
(Prabhas) is a foreign returnee and has a dream of building a hospital at Pulivendula. He is in love with his colleague, Lakshmi (Asin Thottumkal) who studied with him abroad. But leaves her half-way through after he realizes that he was struck with cancer. Not wanting to tell her, he moves to Hyderabad and stays in place called ‘Sahara Colony’. There, he tries to solve people problems in the colony. Seeing all this, another girl who is also named Lakshmi (Charmy Kaur|Charmy) falls in love with Chakram. The rest is a heart-rending saga of the cancer-struck patients love station.

==Cast==
*Prabhas as Chakram
*Asin as Lakshmi
*Charmy Kaur as Lakshmi
*Prakash Raj as Chkrams father
*Tanikella Bharani Rajya Lakshmi
*Radha Kumari
*Narayana Rao
*Kalpana
*B. Padmanabham as himself Mallikarjuna Rao
*Amanchi Venkata Subrahmanyam|A.V.S.
*M. S. Narayana
*Raghu Babu
*Shiva Reddy
*Srinivas Reddy Venu Madhav as auto driver
*Delhi Rajeswari
*Bhuvaneswari
*Vajja Venkata Giridhar
*Brahmanandam as railway T.C. (Cameo)
*Ahuti Prasad

==Awards==
Nandi Award for Best Director - Krishna Vamsi

== References ==
 

== External links ==
*  

 

 
 
 
 


 