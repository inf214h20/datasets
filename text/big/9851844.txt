88 Antop Hill
{{Infobox Film  name           = 88 Antop Hill image          = 88anatophill.jpg writer         = Kushan Nandy Kiran Shroff dialogues      = Ashish Deo producer       = Kiran Shroff director       = Kushan Nandy starring  Jasmine
|music          = Rajesh Roy released       = 27 June 2003 runtime        = 122 minutes language       = Hindi
}}

88 Antop Hill is a 2003 mystery thriller Hindi film. The story, screenplay and direction are by Kushan Nandy; dialogues by Ashish Deo. The film is a murder mystery and is loosely based on James Hadley Chases novel Tiger By The Tail.

== Plot overview ==

The film opens with the brutal murder of Neeraj Shah (Sachin Dubey). His body is dumped in the trunk of a car. This crime isnt committed on impulse: Whoever did it had good reason.

We then enter the life of a bank executive Pratyush Shelar (Atul Kulkarni). He gets home late on his anniversary because he had to console an old college classmate about her marital woes. His own marital woes are worsened when his wife Antara (Suchitra Pillai) kicks up a big fight and leaves their home with their daughter Saanjh.

Pratyush is upset about this. His colleague Aslam Durrani (Harsh Khurana) picks up on this and suggests an exotic dancer Teesta (Shweta Menon). Pratyush is too upset to be interested. But Aslam is determined; later that night he calls Pratyush and pretends as though some thugs are about to kill him. He asks Pratyush to come immediately to a certain location.

That location is 88 Antop Hill.

Pratyush arrives. 88 Antop Hill is a halfway house mainly occupied by exotic dancers and escorts. Pratyush walks into the flat Aslam had mentioned; it is Teestas lair. She attempts to seduce him but he refuses her advances. Upon her request, he takes her to the Moksha club and buys her a drink. (He doesnt drink.) She remarks that the bartender Sol (Rahul Panday) knows all her secrets. While leaving the bar, their car is attacked by an apparently drunk man, who seems to know Teesta. Teesta urges Pratyush to leave fast. When he brings her back home, he realizes that his car keys are missing. He waits as she looks for them.

When Teesta gets into her bedroom, she is mysteriously murdered. She stumbles into Pratyushs arms, fatally stabbed, and collapses. Scared and confused, Pratyush leaves immediately. One of the neighbors, a weird and on-and-off senile old fellow called Murli Mansukhani (Shauket Baig) sees him leave.

When the cops begin to investigate, they find that Teesta had a roommate Sonali (Jasmine DSouza|Jasmine) and that Sonali is engaged to a prominent businessman, K.K. Menon (Sanjay Singh). The investigating officer, and the hero of this movie, Inspector Arvind Khanvilkar (Rahul Dev), starts his work. The prime suspect is an unwitting Pratyush because he was the last person to see Teesta alive.

A somewhat convoluted plot evolves. Sonali has several dark secrets and a good-for-nothing brother Prashant (Subrotto Dutta). Teesta was somehow connected to Sonalis dark past. Menon knows this and wanted to wipe out everything, including Prashant. He has Prashant kidnapped, and later he tells Sol to bump off Prashant. There is a three-way fight among them. Pratyush arrives upon the scene and witnesses two more murders. A dying Prashant advises Pratyush to go to Sonali at the earliest. In the meantime, the weird old man of 88 Antop Hill, Murli, recalls that he had seen Pratyush and follows him to blackmail him. When Pratyush asks him to get lost, he goes to Sonalis house and tries to blackmail her. Aslam Durrani visits Pratyush and, seeing him, thinks he has probably committed the crime, saying that Pratyush would better leave the city and that he wouldnt want anything to do with the latter anymore.

Pratyush contacts the cops but a dumbwitted cop sternly asks him to surrender, as the police have found the murder weapon and a blood stained shirt, with Pratyushs diary stating 88 Antop Hill on the fateful date at his house. Pratyush panics and goes to Sonali to convey Prashants message to her. She does not listen to what he says. Here Menon and the good inspector show up as well. 

At this point the crux arises &mdash; the inspector says that he has found out that Sonali is actually "Sonali Shah", and she was married already to the deceased Neeraj Shah; it was Sonali who killed him because he wouldnt give her a divorce to marry Menon. He also says that Neerajs house was ransacked by Sonali to find the marriage certificate.

At this point, Arvind goes to the fridge and finds Murli Mansukhani lying stabbed in it! The truth is then out. It is known that Sonali murdered Neeraj and hid his corpse in the fridge and murdered Murli because he opened the fridge out of curiosity and found Neerajs corpse. Arvind also reveals that Sonali killed Teesta. Sonali says it was because Teesta knew about her marriage with Neeraj, and used to blackmail her, saying she will make it known to Menon. It was Sonali who had followed Pratyush and Teesata on the fateful night back to 88 Antop Hill, secretly climbed into Teestas bedroom and slit her throat. She further followed Pratyush and leaves the knife in his house to implicate him. It is then revealed that Menon has murdered Prashant, at which Sonali is shocked.

At this point, Menon makes a last effort to save himself and Sonali. He tries to bribe Arvind with 10 million, saying that he must arrest poor Pratyush on charges of all the murders. To this, Arvind agrees and shoots Pratyush in the shoulder. But the wounded Pratyush releases gas from the cylinder nearby and threatens to light his cigarette lighter. While he grabs Arvinds phone and tries to call police, Menon attacks him and stabs him on the sofa, and Arvind quickly turns out the gas. But the wounded Pratyush pulls out Menons revolver, kicks him off and shoots him through a cushion.

At this point, a dumbfounded Sonali picks up a pistol and threatens to kill Pratyush. Arvind tells her not to, but she pulls the trigger &mdash; only to find that the gun is empty.

Arvind then apologizes to Pratyush, saying he had to wound the latter or else Menon wouldve killed Pratyush. Sonali is then arrested on charges of multiple homicide and attempt to kill Pratyush.

The film ends with the inspector relieving Pratyush of all charges in the case and requesting him to take care of a pet dog. Pratyushs wife and child return.

==Cast==

*Rahul Dev ... Inspector Arvind Khanvilkar
*Atul Kulkarni ... Pratyush Shelar
*Shweta Menon ... Teesta Jasmine ... Sonali
*Shauket Baig ... Murli Mansukhani

== External links ==
*  

 
 
 
 