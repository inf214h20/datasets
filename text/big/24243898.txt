Fifteen Minutes That Shook the World
 
  Champions League The miracle Rafa Benitez and has cameos by Steven Gerrard, Jamie Carragher and Dietmar Hamann. The film has been available on DVD since late 2009.

The film begins with an TV journalist investigating the events of the Champions League final. He eventually tracks down CCTV footage of the Liverpool dressing room at half time.

The film had its world premiere on November 16, 2009 at the Odeon Cinema in Liverpool with a party held at Cafe Sports Express.

The film was made to raise money for Jamie Carraghers "23 Foundation" which is a Merseyside based childrens charity, and the cast were all volunteers.

==Cast==
 Andrew Schofield as a journalist Rafa Benítez
*Jamie Carragher as Himself - Liverpool F.C. Player
*Steven Gerrard as Himself - Liverpool F.C. Player
*Dietmar Hamann as Himself - Liverpool F.C. Player
*Philly Carragher as Himself
*Lindzi Germain as Gwladys
*Sean McKee as Bitter Blue
*Marc J. Morrison as Ratboy (as ...Marc Morrison)
*Sonny Spofforth as Ronaldo
*David Gisbourne as Vladimír Šmicer Michael Robinson as Milan Baroš
*Daniel Sanderson as Igor Bišćan
*Phil Connolly as Harry Kewell
*Tom Doolan as John Arne Riise
*Shawn "Spykatcha" John as Djibril Cissé

 

 
 
 
 
 
 

 