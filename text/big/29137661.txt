El revólver sangriento
{{Infobox film
| name           = El revólver sangriento
| image          = Elrevolversangriento.PNG
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = Miguel M. Delgado
| producer       = 
| writer         = 
| screenplay     = Alfredo Salazar
| story          = 
| based on       = 
| narrator       =  Luis Aguilar Lola Beltrán Flor Silvestre
| music          = Antonio Díaz Conde
| cinematography = José Ortiz Ramos
| editing        = Jorge Bustos
| studio         = Cinematográfica Calderón
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} Luis Aguilar, Flor Silvestre, Emilio Fernández, Manuel Capetillo, Antonio Aguilar, and Irma Dorantes, as credited in the films theatrical posters. The lead actors were credited in an unusual "rigorous appearance on the screen" style, where the films main characters are not ordered by importance, but by on-screen appearance. Written for the screen by Alfredo Salazar, the film was a production of Cinematográfica Calderón and follows the account of a silver-plated revolver which has a deadly curse and falls on the hands of different men.

==Plot== Luis Aguilar) bar called "La Patrona", whose bartender is Carmen (Lola Beltrán) who is infatuated with Juan. She serves him something to drink, and they converse, as a next scene focuses on a beautiful woman named Rosa (Flor Silvestre) who sings the song "Cariño bonito" as she waters her geraniums and watches her birds. At the end of her singing, her aunt (Emma Roldán) arrives at the house and tells her of Juans return to the town. She replies that he came back for her to take her with him, and to prepare her clothes. When Rosa arrives at the bar, Juan responds to her coldly, and even depreciates her.

==Cast== Luis Aguilar as Juan Chavéz, outlaw who dies in a duel for the love of Rosa.
*Lola Beltrán as Carmen, bartender girl who is in love with Juan. Flor Silvestre as Rosa, Juans love-interest and former girlfriend.
*René Cardona as Sheriff
*Emilio Fernández as Félix Gómez, as jailed bandit who escapes with the revolver.
*Manuel Capetillo as Rogelio Cruz, horse wrangler who steals Félixs money and revolver.
*Cuco Sánchez as Cuco
*Antonio Aguilar as Ramón, ex-bandit who seeks a peaceful life as a cattle rancher.
*Irma Dorantes as Ramóns wife, and mother of his child.
*David Silva as "El Manso"
*Arturo Martínez as "El Chacal"
*David Reynoso as "El Toro" Díaz
*Enrique Lucero as Pedro
*Emma Roldán as Rosas aunt
*Jorge Mondragón as the Doctor
*Jose Chávez as the Hotel owner
*Lupe Mejía as the Barmaid
*Julián de Meriche as don León
*Alfonso Arnold as Sheriff Ruiz
*Jorge Russek as "El Toro"s bandit
*Quintín Bulnes as "Chimuelo"
*Leon Barroso as Manuel
*Emilio Garibay as Vigilant 
*Eleazar García as the stagecoach driver
*María Elena Velasco as Pedros wife, and the mother of his three children.

==Soundtrack==
*"Cuando no se de ti", written by Enrique Sarabia and performed by Irma Dorantes.
*"Cariño bonito", written by Cuco Sánchez and performed by Flor Silvestre.
*"Fallaste corazón", written and performed by Cuco Sánchez.
*"El hombre alegre", written by Cuco Sánchez and performed by Manuel Capetillo.
*"Nube bajas", written by Tomás Méndez and performed by Lola Beltrán.
*"Cuando dos almas", written by Fructuoso G. Reyes and performed by Antonio Aguilar.
*"El pecador", written by Alex F. Roth and performed by Juan Mendoza.

==International releases==
The film was released in Italy in 1966,    under the title Duello a Santo Cruz ( ).  Some of the Italian promotional movie posters for the film only credit James Fields, the sound supervisor, Jorge Russek, as "Georgia Russek", David Silva as "David Rayen", and Quintín Bulnes as "Quentin Bulness" as the head cast.

==Home media releases==
Laguna Films has released the film individually in VHS once, and also along other four Antonio Aguilar feature films in DVD in the "Latigo Norteño" double-sided 2-disc pack.

==References==
 

==External links==
* 
*  at the Cinematográfica Calderón|Cinematográfica Calderón Film Library

 
 
 
 
 
 
 