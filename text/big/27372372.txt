Limitless
 
{{Infobox film name = Limitless image = Limitless Poster.jpg border = yes alt =  caption = Theatrical release poster director = Neil Burger producer = Leslie Dixon Ryan Kavanaugh Scott Kroopf screenplay = Leslie Dixon based on =   starring = Bradley Cooper Abbie Cornish Robert De Niro music = Paul Leonard-Morgan cinematography = Jo Willems editing = Tracy Adams Naomi Geraghty studio = Rogue Many Rivers Boy of the Year Intermedia Film distributor = Relativity Media released =   runtime = 105 minutes country = United States language = English budget = $27 million    gross = $161,849,455    
}} thriller film directed by Neil Burger and starring Bradley Cooper, Abbie Cornish, and Robert De Niro. It is based on the novel The Dark Fields by Alan Glynn.

==Plot==
 

Eddie Morra (Bradley Cooper), an author suffering from writers block, living in New York, is stressed by an approaching deadline. His girlfriend Lindy (Abbie Cornish), frustrated with his lack of progress and financial dependence, breaks up with him. Later, Eddie meets Vernon Gant (Johnny Whitworth), the estranged brother of Eddies ex-wife, Melissa (Anna Friel). Vernon, involved with a pharmaceutical company, gives Eddie a nootropic drug, NZT-48. After taking the pill, Eddie finds himself able to learn much faster and recall memories from his distant past, with the only apparent side effect being a change in the color of Eddies irises while on the drug – his eyes becoming an intense shade of electric blue. He uses this ability to finish ninety pages of his book. The next day, the effects having worn off, he seeks out Vernon in an attempt to get more. While Eddie is running an errand, Vernon is killed. Eddie returns, calls the police and then discovers Vernons NZT stash just before they arrive. Using the new miracle drug, Eddie quickly completes his book.

Testing his new ability on the stock market, Eddie makes large returns on small investments. Realizing he requires more capital, he borrows one hundred thousand dollars from a Russian loan shark, Gennady (Andrew Howard), and successfully makes a return of two million dollars. He increases his NZT dosage and rekindles his relationship with Lindy.

Eddies success leads to a meeting with a business tycoon, Carl Van Loon (Robert De Niro), who wants Eddie to advise him on a merger with Hank Atwood (Richard Bekins). After work, Eddie starts experiencing blackouts involving large quantities of missing time. He finds himself at a nightclub, a hotel party, in a hotel room with a blonde woman (Caroline Winberg), and in a subway station where he easily subdues several men using techniques seen in old kung fu movies, boxing matches, and documentaries. When this particular marathon blackout finally ends, he finds himself on the Brooklyn Bridge, from which he slowly limps home. Later, Eddie sees a news report detailing the murder of the woman with whom he had slept, but he is unable to remember whether or not he was the killer.

Eddie meets with Melissa and discovers that she too had been on NZT. When she attempted to stop taking it, she had experienced a severe mental rebound effect, as well as a limp like Eddie, while others had died after stopping. On his way home, Eddie is attacked by Gennady, who takes Eddies last NZT pill. Eddie visits Lindy and asks her to retrieve his backup stash, which he had hidden in her apartment. On her way back, she is followed by a man (Tomas Arana) whod been stalking Eddie. He corners Lindy in a park, and Eddie tells her to take an NZT pill. The pill enables her to escape and she returns the stash to Eddie.

Eddie experiments with the drug and learns to control his dosage, sleep schedule and food intake to prevent side effects. He continues to earn money on the stock exchange and hires bodyguards to protect him from Gennady, who threatens him in an attempt to obtain more NZT. He buys an armored penthouse and hires a laboratory to reverse engineer NZT. For his part in the merger, Eddie is promised forty million dollars, and he hires an attorney (Ned Eisenberg) to help keep the police from investigating the deaths of both Vernon and the woman.

On the day of the merger, Atwoods wife informs Van Loon that he has fallen into a coma. Eddie recognizes Atwoods driver as his stalker. While Eddie participates in a lineup, his attorney steals Eddies whole supply of NZT from his jacket. Soon afterwards, Eddie discovers that his pills are gone and begins to enter Drug withdrawal|withdrawal.  He also learns that his bodyguards have been killed. But the severe effects of withdrawal cause him to hurry home when Van Loon questions Eddie about his knowledge relating to Atwoods coma. Gennady breaks into his apartment, demanding more NZT. He reveals that to increase the effects potency and duration he has been dissolving it in water and injecting it. Eddie stabs Gennady and licks up some of his pooling blood for the NZT it now contains. His increased mental acuity restored, Eddie kills Gennadys henchmen and escapes. He meets with his stalker, surmising that Atwood employed the man to locate more NZT. The two join forces and recover Eddies stash from his attorney (who did not pass the NZT to his client).

A year later, Eddie has retained his wealth, his book has been released, and he is running for the United States Senate. Van Loon visits him and reveals that he has absorbed the company that produced NZT and shut down Eddies laboratory. He offers a steady supply of the drug in return for power when Eddie inevitably becomes President of the United States|president. Eddie implies that he has had multiple laboratories working on NZT for the purposes of reverse engineering it, as well as being able to eliminate all of the negative side-effects. He states that he has found a way to wean himself completely off of the drug without losing any of his enhanced abilities. He turns down Van Loon and sends him on his way. He meets Lindy at a Chinese restaurant for lunch, where his Chinese language skills with the waiter invite skepticism from Lindy as to whether he is actually off of the drug.

==Cast==
* Bradley Cooper as Edward "Eddie" Morra
* Robert De Niro as Carlos "Carl" Van Loon
* Abbie Cornish as Lindy
* Anna Friel as Melissa Gant
* Johnny Whitworth as Vernon Gant
* Richard Bekins as Henry "Hank" Atwood
* Robert John Burke as Donald "Don" Pierce
* Tomas Arana as the Man in a tan coat
* T.V. Carpio as Valerie
* Patricia Kalember as Mrs. Atwood
* Andrew Howard as Gennady
* Ned Eisenberg as Morris Brandt

==Production==
Limitless is based on the novel The Dark Fields by Alan Glynn. The film is directed by Neil Burger and is based on a screenplay by Leslie Dixon, who had acquired rights to the source material. Dixon wrote the adapted screenplay for less than her normal cost in exchange for being made one of the films producers.    She and fellow producer Scott Kroopf approached Burger to direct the film, at the time titled The Dark Fields. For Burger, who had written and directed his previous three films, the collaboration was his first foray solely as director.    With Universal Pictures developing the project, Shia LaBeouf was announced in April 2008 to be cast as the films star. 
 Rogue Pictures. By November 2009, actor Bradley Cooper replaced LaBeouf in the starring role.   Robert De Niro was cast opposite Cooper by March 2010, and The Dark Fields began filming in Philadelphia the following May.  Filming also took place in New York City.  For a car chase scene filmed in Puerto Vallarta, filmmakers sought a luxury car. Italian carmaker Maserati provided two Maserati GranTurismo coupes free in "a guerrilla-style approach" to product placement.  By December 2010, The Dark Fields was re-titled Limitless. 

==Release==
Limitless had its world premiere in New York City on  , 2011.  It was released in   in the United States and Canada on  , 2011.    It grossed a   on its opening weekend to rank first at the box office, beating other openers  .   Limitless was released in the United Kingdom on  , 2011. 

Before the films release, Box Office Mojo called Limitless a "wild card", highlighting its "clearly articulated" premise and the pairing of Cooper and De Niro, but questioned a successful opening. The film opened at number one in its first week in the US. The film did well at the box office, earning some $79 million in the U.S. and Canada as well as some $157 million worldwide against its $27 million budget. 

==Critical reception==
Limitless received generally positive reviews from film critics.  , which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 59 based on 37 reviews. 
 20 percent of its brain. Still, thats more than a lot of movies do."  
 Russian gangsters and Wall Street crooks. Honeycutt reserved praise for Cooper, Abbie Cornish, and Anna Friel. He also commended cinematographer Jo Willems camerawork and Patrizia von Brandensteins production design in the films array of locales. 

Variety (magazine)|Variety s Robert Koehler called Limitless a "propulsive, unexpectedly funny thriller". Koehler wrote, "What makes the film so entertaining is its willingness to go far out, with transgressive touches and mind-bending images that take zoom and fish-eye shots to a new technical level, as the pill enables Eddie to experience astonishing new degrees of clarity, perception and energy." He said of Coopers performance, "Going from grungy to ultra-suave with a corresponding shift in attitude, Cooper shows off his range in a film he dominates from start to finish. The result is classic Hollywood star magnetism, engaging auds   physically and vocally, as his narration proves to be a crucial element of the pics humor." The critic also positively compared Willems cinematography to the style in Déjà Vu (2006 film)|Déjà Vu (2006) and commended the tempo set by the films editors Naomi Geraghty and Tracy Adams and by composer Paul Leonard-Morgan. 
 Best Science 2012 Saturn Awards, but lost to Rise of the Planet of the Apes.  

==TV series spin-off==
Bradley Cooper announced in October 2013 that he, Leslie Dixon and Scott Kroopf will be executive producers of a television series based on Limitless. 

On November 3, 2014, it was announced that CBS is be financing a pilot episode for the Limitless TV series. The pilot will continue where the movie left off. Cooper will be an executive producer but will not appear in the show. It was revealed that the main character will be called Brian Sinclair. 

The Limitless pilot will be directed by Marc Webb, replacing Burger who had to pull out due to a scheduling conflict with the Showtime drama pilot Billions.  Burger is still an executive producer, alongside Alex Kurtzman, Roberto Orci and Heather Kadin. It will be based off a script by Elementary (TV series)|Elementary executive producer Craig Sweeny.     The Limitless pilot will air on 1 June 2015 on CBS. 

==See also==
* Intellectual giftedness

==References==
 

==External links==
*  
*  
*  
*  
*   at MSNBC
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 