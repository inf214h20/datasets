American Reunion
 
 
 
{{Infobox film
| name             = American Reunion
| image            = American Reunion Film Poster.jpg American Pie movie poster
| border           = yes
| alt              = Group picture of the cast. Alyson Hannigan has a baby bottle in hand instead of a flute. The pie has only a small slice left, indicating this is the final film in the series.
| director         = Jon Hurwitz Hayden Schlossberg Chris Moore Craig Perry Adam Herz Warren Zide
| writer           = Jon Hurwitz Hayden Schlossberg
| based on         =  Chris Klein Thomas Ian Nicholas Eddie Kaye Thomas Seann William Scott Tara Reid Natasha Lyonne Mena Suvari Eugene Levy
| music            = Lyle Workman
| cinematography   = Daryn Okada   
| editing          = Jeff Betancourt 
| studio           = Relativity Media Zide/Perry Productions
| distributor      = Universal Pictures
| released         =  
| runtime          = 113 minutes   
| country          = United States
| language         = English
| budget           = $50 million 
| gross            = $235 million 
}} ensemble comedy film written and directed by Jon Hurwitz and Hayden Schlossberg. It is the fourth installment in the American Pie (film series)#Theatrical films|American Pie theatrical series and eighth installment in the American Pie (film series)|American Pie franchise overall.

==Plot== Chris Klein), Kevin Myers (Thomas Ian Nicholas), Paul Finch (Eddie Kaye Thomas), and Steve Stifler (Seann William Scott) are well-established in their lives and careers. Jim is still married to Michelle (Alyson Hannigan) and now has a two-year-old son, Evan. Since the birth of their son, Jim and Michelles sex life has deteriorated. Oz is an NFL sportscaster living in Los Angeles with his supermodel girlfriend Mia (Katrina Bowden). Kevin is married to Ellie and works from home as an architect. Finch tells his friends that he has been traveling the world, and still searching for his one true love. Stifler works as a temp at an investment firm, where he is also the victim of cruel verbal abuse by his arrogant employer.

Former classmate John (John Cho), one half of the MILF duo, organizes a Class of 1999 high school reunion in East Great Falls. Jim and Michelle return to Jims old home, where his father Noah (Eugene Levy) is now a widower. Jim encounters his neighbor Kara (Ali Cobrin), whom he used to babysit and who is soon to turn 18. Jim meets Oz, Kevin, and Finch at a bar, where they meet Selena Vega (Dania Ramirez), Michelles best friend from band. Stifler happens by, and joins them for weekend activities.
 defecating in their beer cooler and destroying their jet skis. That night, most of the group go to the falls and find a high school party celebrating Karas birthday. Finch and Selena reconnect, and they fall in love. Kara becomes intoxicated; Jim drives her home, and she tries to seduce him. They are discovered by John, who simply mistakes Kara for Michelle. Oz, Finch, and Stifler help Jim return Kara to her parents home, but A.J. spots them. Kevin wakes up hungover next to Vicky and assumes they had sex.
 strike out. A fight ensues, which is disrupted by police.  But the police arrest Finch for stealing a motorcycle, which Stifler finds amusing.  Fed up with Stiflers rudeness, the guys tell him off, and the others admit that they did not want him to ruin things like he always does. Hurt, Stifler ends the party.
 Staples and Chris Owen) hooks up with Loni. Stifler is asked to be a party planner for a wedding for his former lacrosse teammates.  He also meets Finchs mother Rachel (Rebecca De Mornay) who intrigues Stifler by telling him she wishes her son were more into sports. After Rachel makes a brazen sexual pass at Stifler using lacrosse equipment-related innuendos, Rachel and Stifler proceed to have sex on the lacrosse field, while Rachel loudly and passionately agrees with Stiflers proclamations about his manliness. John is reunited with his estranged buddy, Justin (Justin Isfeld) (MILF Guy #1) and they watch Stifler having sex with Finchs mom while chanting "MILF (slang)|MILF,! MILF! MILF!".

The next morning, Jim and Kara apologize to each other for their behavior. Oz plans to stay in town with Heather, Finch plans a trip with Selena to Europe, and Stifler drops subtle hints about sleeping with Rachel. Jim proposes a pact for them to reunite once a year. They all agree and make a toast as the franchises theme song ("Laid (song)|Laid") plays. As the credits roll, Stifler says, "I fucked Finchs mom!" in a half-whisper, leaving Finch mortified. In a mid-credits scene, Noah Levenstein receives sexual gratification from Stiflers Mom at a movie theater.

==Cast==
  in Sydney 2012.]]
 
 
* Jason Biggs as Jim Levenstein
* Alyson Hannigan as Michelle Levenstein Chris Klein as Chris "Oz" Ostreicher
* Thomas Ian Nicholas as Kevin Myers
* Eddie Kaye Thomas as Paul Finch
* Seann William Scott as Steve Stifler
* Tara Reid as Vicky Lathum
* Natasha Lyonne as Jessica
* Mena Suvari as Heather Gardner
* Eugene Levy as Noah Levenstein, Jims Dad
* Jennifer Coolidge as Stiflers Mom
* Dania Ramirez as Selena
* Ali Cobrin as Kara
* John Cho as MILF Guy #2
* Katrina Bowden as Mia
* Jay Harrington as Dr. Ron
* Chuck Hittinger as AJ
* Shannon Elizabeth as Nadia Chris Owen as Sherman
 

==Production==

===Development===
It was reported  in   that Universal Pictures was planning to produce a third theatrically released sequel to the first film.  In  , the film entered pre-production, with Jon Hurwitz and Hayden Schlossberg signing on to write and direct with plans of reuniting the whole cast of the primary series. 

===Casting===
In  , it was announced  that Jason Biggs, Seann William Scott and Eugene Levy had signed on to reprise their roles.  Biggs and Scott were granted executive producer credits and also helped convince the other previous cast members to return. {{cite news|url= http://www.usatoday.com/life/movies/news/story/2011-10-11/american-pie-reunion-sneak-peek/50736642/1 |title=All the Pie ingredients are there in American Reunion Chris Klein, and Mena Suvari signed on.    The following month, Thomas Ian Nicholas, Tara Reid, Eddie Kaye Thomas,  Shannon Elizabeth,    and Jennifer Coolidge  signed on. In June and  , John Cho  and Natasha Lyonne  were the last returning cast to sign on.

On  , a casting call went out for the character "Kara", a role that involved "upper frontal nudity".  Ali Cobrin was cast in the role. National Football League wide receiver Chad Ochocinco has a cameo along with other wide receiver Terrell Owens.    

Jason Biggs and Seann William Scott each received a reported $5 million plus a percentage of the profits for their performances. Alyson Hannigan and Eugene Levy were said to have been paid $3 million each, with the rest of the cast receiving payments within the $500,000 to $700,000 range, except Tara Reid who was paid $250,000. 

===Filming=== metro Atlanta, Monroe and Newton High Covington from   to  . Scenes were filmed at the schools gym for a reunion prom set, football field, commons area and hallways; which included 200 extras. Under the deal the production company paid $10,000 to the Newton County School System for using the school. 
 Cumming to film at Mary Alice Park on Lake Lanier and included about 100 extra (actor)|extras.    Moore said the beach at the lake looks similar to a Lake Michigan setting, which is the state in which the film is set. The production company paid $23,000 to have full access to the property for a week.  Suvari finished filming her scenes on  . 

==Release==

===Box office=== The Hunger Games. {{cite news |url= http://www.hollywoodreporter.com/news/hunger-games-american-reunion-titanic-3d-box-office-309421|title=Hunger Games Sinks American Reunion, Titanic 3D|date=April 8, 2012|work=The Hollywood Reporter
|accessdate=April 19, 2012|first=Pamela|last=McClintock}} 
On its second week of release it dropped to number 5 at the box office with a weekend total of $10,473,810. 

The film earned $56,758,835 in North America and $177,978,063 internationally, for a worldwide total of $234,736,898. 

===Home video===
The DVD and Blu-ray discs were released on July 10, 2012 in North America.  The film was also released in a box set called American Pie Quadrilogy on August 22, 2012 in Australia.  The rated R version was available on iTunes a few days ahead of time as an "Early Digital Release".  It was released on the September 10, 2012 in the United Kingdom. 

==Reception==
American Wedding received mixed reviews from critics. Rotten Tomatoes gives the film a rating of 43%, based on 170 reviews, with an average rating of 5.2/10. The sites critical consensus reads, "Itll provide sweetly nostalgic comfort food for fans of the franchise, but American Reunion fails to do anything truly new or interesting -- or even very funny -- with the characters."   On Metacritic, the film has a rating of 49 out of 100, based on 34 critics, indicating "mixed or average reviews". 

According to Roger Ebert, who gave the film three out of four stars: 
 The charm of "American Pie" was the relative youth and naivete of the characters. It was all happening for the first time, and they had the single-minded obsession with sex typical of many teenagers. "American Reunion" has a sense of deja vu, but it still delivers a lot of nice laughs. Most of them for me came thanks to Stifler. . . If you liked the earlier films, I suppose you gotta see this one. Otherwise, I dunno. 

The Village Voice concludes its review with the following: 
 After some strained "Remember the time . . ." callbacks to 13-year-old gags, American Reunion gets comfortable and funny, as Hurwitz and Schlossberg hit familiar marks from unexpected angles, while the ensemble interplay is "routine" in the best sense of the word. Taken altogether, the Pie movies offer a cohesive worldview, showing each of lifes stages as the setting for fresh-yet-familiar catastrophes, relieved by a belief in sex, however ridiculous it might look, as a restorative force. The recipe is so durable and the sustained character work so second-skin by now, one can imagine the Pie films keeping with the dramatis personae through middle age and into the problems of geriatric love, a raunch-comic version of Britains documentary Up series. American Midlife Crisis? American Retirement? American Funeral? Lets go! 

Peter Travers of Rolling Stone gave American Reunion a positive review of two and half stars out of four saying, "American Reunion reminds us what we liked about the original: the way the movie sweetened its raunch to build a rooting interest in these characters." 

===Accolades===
 
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient(s) !! Result
|-
| rowspan="3"| 2012 Teen Choice Awards   
| Choice Movie: Comedy
|
|  
|-
| Choice Movie Actor: Comedy
| Jason Biggs
|  
|-
| Choice Movie Actress: Comedy
| Alyson Hannigan
|  
|}

==Soundtrack==
{{Infobox album
| Name        = American Reunion: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various artists
| Cover       =
| Released    =  
| Recorded    = 
| Genre       = 
| Length      = 55:32
| Label       = Universal Republic
| Producer    = 
| Last album  = American Wedding: Music from the Motion Picture (2003)
| This album  = American Reunion: Music from the Motion Picture (2012)
| Next album  = 
}}

{{Album ratings
| rev1 = Allmusic
| rev1score =    
}}

{{tracklist
| extra_column    =  Performed by
| writing_credits = yes Last Night
| note1         = 
| writer1       = 
| extra1        = Good Charlotte
| length1       = 3:40
| title2        = You Make Me Feel... Sabi
| writer2       = 
| extra2        = Cobra Starship
| length2       = 3:35
| title3        = Here Comes the Hotstepper
| note3         = 
| writer3       = 
| extra3        = Stooshe
| length3       = 3:36
| title4        = Wannamama
| note4         = 
| writer4       = 
| extra4        = Pop Levi
| length4       = 3:28 My First Kiss
| note5         = featuring Ke$ha
| writer5       = 
| extra5        = 3OH!3
| length5       = 3:13
| title6        = Im a Man
| note6         = 
| writer6       = 
| extra6        = The Blue Van
| length6       = 3:49
| title7        = Bring It On Home
| note7         = 
| writer7       =  Kopek
| length7       = 3:08 Rump Shaker
| note8         = featuring Teddy Riley
| writer8       = 
| extra8        = Wreckx-N-Effect
| length8       = 3:57 Wannabe
| note9         = radio edit
| writer9       = 
| extra9        = Spice Girls
| length9       = 2:53
| title10        = Ill Make Love to You
| note10         = 
| writer10       = 
| extra10        = Boyz II Men
| length10       = 4:02
| title11        = This Is How We Do It
| note11         = 
| writer11       = 
| extra11        = Montell Jordan
| length11       = 3:59
| title12        = The Good Life
| note12         = 
| writer12       = 
| extra12        = Hassahn Phenomenon
| length12       = 3:21
| title13        = My Generation
| note13         = 
| writer13       =  Thomas Nicholas Band
| length13       = 2:28
| title14        = Class of 99
| note14         = 
| writer14       = 
| extra14        = Lyle Workman
| length14       = 5:49 Na Na Na
| note15         = 
| writer15       = 
| extra15        = My Chemical Romance
| length15       = 4:13
| title16        = American Reunion
| note16         = 
| writer16       = 
| extra16        = Lyle Workman
| length16       = 3:26 Laid
| note17         = 
| writer17       =  James
| length17       = 2:37
}}
The two score tracks were recorded by a 60-70 piece orchestra 
conducted by John Ashton Thomas who also orchestrated Workmans score 
for the orchestral sessions.
The orchestra was recorded at the Eastwood Scoring Stage at the 
Warner Bros. lot by Shawn Murphy who also mixed the score.

==Sequel== American Pie 5 was announced on August 4, 2012, with Jon Hurwitz and Hayden Schlossberg returning as directors and screenwriters.   

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 