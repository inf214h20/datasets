The Trachtenburg Family Slideshow Players: Off & On Broadway
{{Infobox Film
| name = The Trachtenburg Family Slideshow Players: Off & On Broadway
| image = 
| starring = Jason Trachtenburg, Tina Piña Trachtenburg, Rachel Trachtenburg
| producer = Richard Drutman
| director = Richard Drutman
| music = 
| cinematography = 
| distributor = Sarathan Records, Trirode Pictures and Rumur Inc.
| released = August 1, 2006 United States
| language = English
| budget = 
| gross = }}
 documentary featuring the Trachtenburg Family Slideshow Players, a self-described indie rock-vaudeville conceptual art rock pop music collective band. The documentary was directed and produced in 2005 by Richard Drutman, the same person who directed the family’s music video for the song “Mountain Trip to Japan, 1959”. It was released on August 1, 2006. The concert performances at Lambs Theater as well as the performances in the bonus features introducing the family was shot on July 29, 2005 at Lamb’s Theatre in New York City.

The DVD is a collage of live musical performances of the Trachtenburgs at Lamb’s Theatre in NYC, old performances/interviews of the family as well as interviews of other such celebrities such as Nellie McKay and Regina Spektor.

== Key Events == John Waters, a filmmaker. The name of the venue is called “A John Waters Christmas”. Alongside the Trachtenburgs, is Kimya Dawson, member of the Moldy Peaches. (Dawson is not shown on the DVD). Waters is seen after the show talking to Jason and Rachel outside before he walks away.
*Jason Trachtenburg’s performance of “Beautiful Dandelion” with Corn Mo (This performance is not shown at Lamb’s). President Bush and Dick Cheney parade. Rachel is leading the march on a drum.
*Jason, Tina, Rachel and Rachel’s friend Micha Carlson-Schmitt Jolly (a close friend of Rachel’s from Seattle joining the family on their European tour) walking through NYC, ducking into a coffee shop, shopping at a flea market and then visiting Reverend Jen and her dog, Reverend Jen, Jr. at her Troll Museum.
*Jason traveling to an organic supermarket at night to buy apples the night before the family’s European tour.
*The Trachtenburgs and Micha at Burrito Loca, a Mexican restaurant in Seattle, eating nachos. New York rock band, The WoWz.
*Reverend Jen’s tour of her Troll Museum, showing off her Rachel Trachtenburg troll.
*Jason attempting to lift an abandoned sewing machine for Tina, but failing.
*Jason’s manic list of things to do the day before the European tour (packing clothes, making Tina’s Good Morning Bread, doing laundry, buying ingredients for Tina and Rachel’s dinner and buying apples, which he succeeds in). East Village (Jason buys Stereo 8|8-track tapes, Tina picks out a vintage shirt for Rachel, Rachel and Micha browsing items, the family buying snow cones, Tina buys a pair go-go boots and Jason buys a belt (to his amazement, the belt costs only a dollar).
*Micha’s interview with Steve Smith, Rachel’s former drum teacher and Beth Jacuelwicz, the band’s former manager. Both are talking about their stories of working with Rachel.
*Jason’s message to fans about the dangers of cell phone use at the end of the documentary.
*In the DVD bootleg skit featured in the bonus material, the Trachtenburgs are shown walking in Times Square when they suddenly stop to buy a bootleg DVD of the documentary from a peddler for only $5. When the family takes the DVD home to watch, the DVD is nothing more than a poorly made clip of one the family’s performances. However, the movie cuts off before the performance actually starts.

== Appearances ==
*Jay Ruttenberg, music writer for Time Out New York Magazine
*David Cross, comedian
*Andrew Katz, lead singer of band The New York Howl; recording artist
*Heather Mansfield, member of band The Brunettes; recording artist
*Nellie McKay, recording artist
*Regina Spektor, recording artist
*Eugene Mirman, comedian
*Corn Mo, recording artist
*Rachel Piña, mother of Tina Piña Trachtenburg
*Steve Smith, Rachel Trachtenburg’s former drum teacher
*Beth Jaculewicz, former manager of the TFSP
*Reverend Jen, actress, painter, poet, director
*Langhorne Slim, recording artist
*Micha Carlson-Schmitt Jolly, friend of Rachel Trachtenburg and interviewer John Waters, filmmaker
*Simon Beins, Sam Grossman, and Johnny Dydo, members of The WoWz; recording artists The Presidents of the United States of America; recording artists
*Checker Phil of   (featured only in bonus features)

== Songs Performed In Documentary ==
*"Theme from Trachtenburg Family Slideshow Players"
*"Mountain Trip To Japan, 1959"
*"Look At Me"
*"Beautiful Dandelion"  (Jason Trachtenburg on guitar and Corn Mo on accordion; not performed at Lamb’s) 
*"Wendy’s, Sambo’s and Long John Silver’s"
*"Don’t You Know What I Mean?"
*"World’s Best Friend"  (with The WoWz) 
*"Rachel Trachtenburg" by Touching You (also known as Christopher X. Brodeur; song also known as "May I Have Your Hand Rachel Trachtenburg" by Liquid Tapedeck, Brodeur’s band)
*"Together As a System We Are Unbeatable"
*"Middle America"
*"I’m Singing a Song"  (End credits; Jason on guitar and vocals, Rachel on tambourine and Tina on vocals) 

== Bonus Features ==
The bonus features include two music videos, Jason Trachtenburg’s live solo performances on interviews with Checker Phil of Checkerboard Kids, more live performances of the family at Lamb’s Theatre and a skit of the Trachtenburgs buying and watching a bootleg video of On & Off Broadway.

Music Videos
*"Mountain Trip to Japan, 1959"
* "Eggs"

Jason Trachtenburg Performances
*"Slideshowers in Paradise" (Original TFSP song)
*"Untitled (When You See the Slides, Sing It)" (Original TFSP song)
*"East Village Rocker with a Missed Connection"

Trachtenburg Family Slideshow Players Performances at Lamb’s
*"OPNAD Contribution Study Committee Report, June 1977"
*"What Will The Corporation Do?"
*"Let’s Not Have the Same Weight In 1978 – Let’s Have More"
*"Why Did We Decide To Take This Decision To You?"
*"Super D" – Skit and slide presentation featuring Jason Trachtenburg

Other
*DVD Bootleg Times Square skit

==Main Personnel==
*Jason Trachtenburg: performer, interviewee
*Tina Piña Trachtenburg: performer, interviewee, costume designer, opening montage artist
*Rachel Trachtenburg: performer, interviewee
*Richard Drutman: director, producer, live soundtrack producer
*Jonathan Kochmer: Executive producer
*Bev Chin: A&R/Associate Producer
*Suki Hawley: Editor
*Jeffery Hartshorne: Director of Photography
*Mike King: DVD artwork artist
*Released by: Sarathan Records, Trirode Pictures and Rumur Inc.

==External links==
* 
* 
* 
* 

 

 
 
 