Layla and Majnun (1937 film)
{{Infobox film
| name           = Layla and Majnun
| image          = Layla and Majnun (1937 film).jpg
| image_size     = 
| caption        = 
| director       = Abdolhossein Sepenta
| producer       = 
| writer         = Abdolhossein Sepenta
| narrator       = 
| starring       = Haji Abed Manouchehr Arian Abed Basravi
| music          = Ali-Naqi Vaziri
| cinematography = Probat Das
| editing        = 
| distributor    = East India Films
| released       =  
| runtime        = 145 minutes
| country        = Iran
| language       = Persian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1937 Iranian romance film produced in 1937 by Abdolhossein Sepanta by the East India Film Studios.

It was based on a full-scale dramatic poem by Nizami and was Sepantas fifth produced film that had sound. The complete script for Laila and Majnun is available as are Sepanta’s other manuscripts.  The script contain detailed information regarding exterior and interior scenes, dialogues, and actors’ movements, settings, costumes, lighting, sound effects and camera movements. The explanatory notes on editing and film processing are offered and scene descriptions are mostly accompanied by carefully worked-out drawings. This film was produced in conjunction with the Iranian diaspora community of Calcutta, which included notables Abed Basravi of the Basravi Masjid, which was a cultural centre for the community at the British Indian capital.

Sepanta says about the failure of this final film:"In September 1936, I arrived in Bushehr with a print of Laila and Majnun. Due to bu-reaucratic complications, the film print could not be immediately released, and we had to leave for Tehran without it. Government officials’ attitude was inexplicably hostile from the beginning and I almost was sorry that I returned home. The authorities did not value cinema as an art form or even as a means of mass communication, and I soon realized that I had to forget about my dream of establishing a film studio. I even had difficulty getting permission to screen my film, and in the end the machinations of the movie theater owners forced us to turn over the film to them almost for nothing."

==External links==
* 

 

 
 
 
 
 
 


 
 