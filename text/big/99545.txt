This Is Spinal Tap
 
 
{{Infobox film
| name           = This Is Spinal Tap
| image          = Thisisspinaltapposter.jpg
| caption        = Theatrical release poster
| director       = Rob Reiner
| producer       = Karen Murphy
| writer         = {{Plain list|
* Christopher Guest
* Michael McKean
* Harry Shearer
* Rob Reiner
}}
| starring       = {{Plain list|
* Christopher Guest
* Michael McKean
* Harry Shearer
* Rob Reiner
* June Chadwick
* Tony Hendra
* Bruno Kirby
}}
| music          = {{Plain list|
* Christopher Guest
* Michael McKean
* Harry Shearer
* Rob Reiner
}}
| cinematography = Peter Smokler
| editing        = {{Plain list| Robert Leighton
* Kent Beyda
* Kim Secrist
}}
| distributor    = Embassy Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $4.7 million  }} 
}}
 heavy metal Spinal Tap. satirizes the heavy metal hagiographic tendencies of rock documentaries of the time.

Reiner and the three main actors are credited as the writers of the movie because they ad libbed much of the dialogue. Several dozen hours of footage were filmed before Reiner edited it to the released movie.
 cameo appearances in the movie.

In 2002, This Is Spinal Tap was deemed "culturally, historically, or aesthetically significant" by the Library of Congress and was selected for preservation by the United States National Film Registry. 

==Plot== Spinal Tap" to promote their new album Smell the Glove, but interspersed with one-on-one interviews with the members of the group and footage of the group from previous periods in their career.
 
 an existing heavy metal go to eleven; when Di Bergi asks, "Why dont you just make ten louder and make ten be the top number and make that a little louder?" Tufnel can only reply, "These go to eleven." Tufnel later plays a somber quasi-classical music composition on piano for Di Bergi, claiming it to be a "Mach piece" (a hybrid between Mozart and Bach), before revealing the composition to be entitled "Lick My Love Pump".

As the tour starts, concert appearances are repeatedly canceled due to low ticket sales. Tensions continue to increase when several major retailers refuse to sell Smell the Glove because of its sexist cover art and there is growing resentment shown towards the groups manager Ian Faith (Tony Hendra). Tufnel becomes even more perturbed when St. Hubbins girlfriend Jeanine (June Chadwick) — a manipulative yoga and astrology devotee — joins the group on tour, begins to participate in band meetings, and attempts to influence their costumes and stage presentation.  The bands distributor, Polymer Records, opts to release Smell the Glove with an entirely black cover without consulting the band. The album fails to draw crowds to autograph sessions with the band.
  double prime symbol instead of single prime. The resulting prop, seen for the first time by the group during a show, is only 18&nbsp;inches high (instead of the intended 18 feet), making the group a laughingstock on stage. The group accuses Faith of mismanagement, and when St. Hubbins suggests Jeanine should co-manage the group, Faith quits in disgust.

The tour continues, rescheduled into smaller and smaller venues. Tufnel becomes marginalized by Jeanine and St. Hubbins. At their next gig (at a United States Air Force base near Tacoma, Washington) Tufnel is upset by an equipment malfunction and leaves the group in the middle of a show. In their next gig, in an amphitheater at an amusement park (second-billed behind a puppet show), they find that Nigels absence severely limits their repertoire. They are forced to improvise a fusion-esque, experimental song entitled "Jazz Odyssey", which is poorly received.
 wildly popular in Japan, in fact its at number 5 in the charts there. He then tells David that Faith would like to arrange a new tour in that country. David is initially cool to the idea, but later, when on stage, David beckons Nigel on to join them and he grabs his guitar and plays with them onstage. David then gets caught up in the moment of the tours final performance and not only allows Nigel to return, but rehires Faith back as manager as well. Despite losing their drummer Mick Shrimpton (Ric Parnell|R.J. Parnell) as he inexplicably explodes onstage, the film ends with Spinal Tap playing a series of sold-out arena shows for enthusiastic fans on their Japanese tour.

==Cast==
 
* Michael McKean as David St. Hubbins
* Christopher Guest as Nigel Tufnel (Tuffy)
* Harry Shearer as Derek Smalls
* Rob Reiner as Marty Di Bergi
* Tony Hendra as Ian Faith
* Ric Parnell|R.J. Parnell, (drummer for Atomic Rooster), as Mick Shrimpton
* David Kaff as Viv Savage
* June Chadwick as Jeanine Pettibone
* Bruno Kirby as limo driver Tommy Pischedda
* Ed Begley, Jr. as John "Stumpy" Pepys
* Danny Kortchmar as Ronnie Pudding
* Fran Drescher as Bobbi Flekman
* Patrick Macnee as Sir Denis Eton-Hogg Julie Payne as mime waitress
* Dana Carvey as mime waiter
* Sandy Helberg as Angelo DiMentibelio
* Zane Buzby as Rolling Stone reporter
* Billy Crystal as Morty the Mime
 
* Paul Benedict as Tucker "Smitty" Brown
* Howard Hesseman as Terry Ladd (Duke Fames Manager)
* Paul Shortino as Duke Fame
* Lara Cody as Duke Fames groupie
* Andrew J. Lederer as student promoter
* Russ Kunkel as doomed drummer Eric "Stumpy Joe" Childs
* Victory Tischler-Blue as Cindy
* Joyce Hyser as Belinda
* Gloria Gifford as the airport security officer with the security wand
* Paul Shaffer as incompetent promoter Artie Fufkin (Polymer Records) Archie Hahn as the room service guy Charles Levin as Disc n Dat manager
* Anjelica Huston as Polly Deutsch
* Donald Kendrick as Background Vocalist
* Fred Willard as Air Force Lt. Bob Hookstratten
* Wonderful Smith as Backstage Maintenance Worker
* Cherie Currie as the lead singer of The Dose. The band and her character were completely removed from the movie and appear in deleted scenes. David Guerra as the roadie (who lifts Tufnel back to his feet)
* Ralph Lee Moss as Angus, the Road Monster
* Z. Robert Carvan as Mr. Goldberg, the Voice of Rock                                                               
 

==Reception==
This Is Spinal Tap was only a modest success upon its initial release. However, the film found greater success, and a cult following, after it was released on video.

Since its release, This Is Spinal Tap has received universal acclaim from critics  and is widely regarded as one of the best films of 1984.     Roger Ebert of the Chicago Sun-Times gave the film 4 stars out of 4 and wrote "This Is Spinal Tap is one of the funniest, most intelligent, most original films of the year. The satire has a deft, wicked touch. Spinal Tap is not that much worse than, not that much different from, some successful rock bands."  Ebert later placed the film on his ten best list of 1984.  The film currently holds a 95% "Certified Fresh" rating on the review aggregate website Rotten Tomatoes.  In 2002, This Is Spinal Tap was deemed "culturally, historically, or aesthetically significant" by the Library of Congress and was selected for preservation in the United States National Film Registry. 

Critics praised the film not only for its satire of the rollercoaster lifestyles of rock stars but also for its take on the non-fiction film genre. David Ansen from Newsweek called the film "a satire of the documentary form itself, complete with perfectly faded clips from old TV shows of the band in its mod and flower-child incarnations". {{cite book
  | last = Muir
  | first = John
  | authorlink =
  | coauthors =
  | title = Best in Show: The Films of Christopher Guest and Company
  | publisher = Applause Theatre & Cinema Books
  | year = 2004
  | location =
  | page = 31
  | url =
  | doi =
  | isbn =  }} 

Even with cameos from Billy Crystal and Patrick Macnee, Spinal Tap still managed to trick many of its moviegoers into believing the band existed. Reiner admits "when Spinal Tap initially came out, everybody thought it was a real band... the reason it did go over everybodys head was that it was very close to home". {{cite book
  | last = Yabroff
  | first = Jennie
  | authorlink =
  | coauthors =
  | title = The Real Spinal Tap
  | publisher = Newsweek
  | year = 2009
  | location =
  | pages =
  | url =
  | doi =
  | isbn =  }} 
 
The movie cut close to home for some musicians. Jimmy Page, Robert Plant, Jerry Cantrell, Dee Snider and Ozzy Osbourne all reported that, like Spinal Tap, they had become lost in confusing arena backstage hallways trying to make their way to the stage.   Paul Du Noyer, "Who the hell does Jimmy Page think he is?", Q (magazine)|Q magazine, August 1988, p. 7.  When Dokkens George Lynch saw the movie he is said to have exclaimed, "Thats us! Howd they make a movie about us?" {{cite book
  | last = Konow
  | first = David
  | authorlink =
  | coauthors =
  | title = Bang Your Head
  | publisher = Three Rivers Press
  | year = 2002
  | location =
  | pages = 216–217
  | url =
  | doi =
  | isbn =  0-609-80732-3}}  The Misfits saying, "When I first saw Spinal Tap, I was like, Hey, this is my old band." {{cite book
  | last = Blush
  | first = Steven
  | authorlink =
  | coauthors =
  | title = American Hardcore
  | publisher = Feral House
  | year = 2001
  | location =
  | page = 207
  | url =
  | doi =
  | isbn =  0-922915-71-7}} 
 title location, as seen by an old childhood friend. When Pete mentions an incident where his drummer complained that "the caviar in their dressing room was the wrong viscosity - for throwing," the friend notes "This is Spinal Tap is obviously a true story."
 black album". Shortly after the tour started, Metallicas James Hetfield suffered third degree burns on his arms after he stood too close to a pyrotechnic device. Earlier in that tour, backstage at the Freddie Mercury Tribute Show, Metallica met with Spinal Tap and discussed how their "black album" was an homage to Spinal Taps Smell the Glove. This was captured on the Metallica DVD A Year and a Half in the Life of Metallica.
 Nirvana explains declining the offer to be a part of the movie Singles (1992 film)|Singles. Kurt Cobain goes on to say, "Theres never really been a good documentary on rock and roll bands." Dave Grohl then cuts in saying, "Except for Spinal Tap,   was the only rock movie worth watching." Which Kurt agreed with, as well as mentioning Dont Look Back, by D.A. Pennebaker.
 Steven   saw it he didnt see any humor in it." When the movie was released, Aerosmiths most recent album, Rock in a Hard Place, depicted Stonehenge prominently on the cover.
 George Lynch debut LP Uriah Heep when principal photography was about to begin, and told them how they had been booked to play an air force base. They subsequently used the story in the film.

U2 guitarist The Edge said in the documentary It Might Get Loud that when he first saw Spinal Tap "I didnt laugh, I wept," because it summed up what a brainless swamp big-label rock music had become. 

Canadian heavy metal band Anvil (band)|Anvil, whose drummer is named Robb Reiner, have been called "the real Spinal Tap" based on the misadventures depicted in their documentary Anvil! The Story of Anvil. 

In the Pearl Jam documentary Pearl Jam Twenty, the band members jokingly refer to the fact that while the core lineup of the group has remained unchanged (singer Eddie Vedder, guitarists Mike McCready and Stone Gossard, bassist Jeff Ament), the band has had five drummers. The band members describe this as very Spinal Tap of us. In the documentary a mock silent film called The Drummer Story is shown explaining what happened to their previous drummers. In the silent film, one of them is almost eaten by a sea monster, only to be rescued by Eddie Vedder, playing a lifeguard.
 Time Out London named it the best comedy film of all time. 

American Film Institute recognition 100 Years... 100 Laughs #29  100 Years... 100 Songs (Official Ballot) for the song "Big Bottom".  100 Years... These go to eleven". 

==Home video release==
This Is Spinal Tap has been released twice on DVD.

The first release was a 1998   for "Hell Hole". Sales of this edition were discontinued after only two years and the DVD has become a valuable collectors item. Much of this material had appeared on a 1994 CD-ROM by The Voyager Company that included the entire film in QuickTime format.
 The Joe Franklin Show. The special features were produced by Automat Pictures. However, this version of the film was missing the subtitles that appear throughout the film (for example, introducing band members, other personnel, and location names) and did not include the commentaries from the Criterion edition. The MGM DVD is missing the subtitles burned into the film; they have been replaced with player generated subtitles.

A 25th Anniversary Edition Blu-ray Disc release was released on July 28, 2009. It includes all bonus features from the MGM DVD plus an interview with Nigel about Stonehenge and Warmer Than Hell from the bands Live Earth performance. It does not include the commentaries from the Criterion Collection DVD, even though MGM had stated that they would be included in the earliest press release for the Blu-ray version (most likely due to legal issues), and does not feature a "create your own avatars" element teased in publicity. However, this version does restore the subtitles that introduce band members/locales/events/etc. that were missing from MGMs DVD. The alternative, Region B, UK edition of this version additionally features a new hour-long documentary featuring famous fans, the "Bitch School" promo, the EPK for the "Back From The Dead" album, an interview with the late Reg Presley discussing the influence of the Troggs tapes on the film, and the first hour (ending with an abrupt edit) of The Return Of Spinal Tap. It does however lose the Di Bergi short and the Joe Franklin clip.
 11 out of 10, though this is a joke in reference to the memorable scene in the film. 

==Appearances in other media==
Harry Shearer, who played Derek Smalls, went on to become one of the main voice artists on The Simpsons, providing voices for Principal Skinner, Mr. Burns, Waylon Smithers, Ned Flanders and many others. Spinal Taps members voiced cartoon versions of themselves in "The Otto Show", first playing on a concert attended by Bart and Milhouse which escalates into a riot after the bands early leave, then having their tour bus run off the road by Otto in the school bus.
 turned up to eleven instead of ten, a popular idiom that was popularized by the film. However, it is still only possible to choose a rating from one to ten.
 James Charles "Jim" Marshall, founder of the famous amplifier company whose equipment is featured in the scene.

==Related works==
* Christmas With The Devil, 1984 followup single
* Inside Spinal Tap (1985), a rare companion book by Peter Occhiogrosso. In 1992 this was revised and expanded exclusively for the UK market.
* Break Like the Wind (1992) album
* "The Otto Show", a 1992 episode of The Simpsons
* A two-hour, made-for-TV sequel, The Return of Spinal Tap, was broadcast and released on video in 1992 to promote Break Like the Wind. It consisted mostly of footage from an actual Spinal Tap concert at the Royal Albert Hall.
* This Is Spinal Tap: The Official Companion (ISBN 0-7475-4218-X) was published in 2000 to coincide. It featured a "Tapistory", full transcript of the film (including out-takes), a discography, lyrics and an A–Z of the band. This book largely recycles material from the Peter Occhiogrosso book and Criterion DVD commentaries. Back from the Dead, 2009 album and DVD
* Unplugged and Unwigged, 2009 live DVD of Guest, McKean, and Shearer performing songs from their various works

==See also==
 
*A Mighty Wind|A Mighty Wind (film) (2003)
*Bad News (band)
*Best in Show (film)|Best in Show (film) (2000)
*For Your Consideration (film)|For Your Consideration (film) (2006)
*Spinal Tap discography
*Up to eleven
*Waiting for Guffman|Waiting for Guffman (film) (1996)
*All You Need Is Cash (1978)
*Fear of a Black Hat (1994)
 

== Notes and references ==
 

==External links==
 
*  
*  
*  
*  
*   by Peter Occhiogrosso

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 