Barber's Tales
{{Infobox film name           =Barbers Tales image          = File:Angkwentongbarbero.jpg caption        = director       =Jun Lana producer      = writer         = screenplay     =Jun Lana cinematography   =  editing          =  music            =  starring       = Eugene Domingo Eddie Garcia Iza Calzado studio = APT Entertainment Octobertrain Films distributor =  released   =  runtime        = 120 minutes country        = Philippines language  Filipino  Tagalog English English
|budget           = gross            =
}} Jun Robles Lana. The film stars Eugene Domingo as Marilou, a widow who is forced to take her late husband’s job as community barber during the end of Marcos era. The film is the follow up to Lana’s film Bwakaw and second of a trilogy focused on the small town life in the Philippines.   The film had its world premiere and competed at the 2013 Tokyo International Film Festival, where it won the Best Actress Award for Eugene Domingos performance.

The film will have its mainstream release in the Philippines on August 13 of 2014.  

==Synopsis==
Barbers Tales is set in the rural town in the Philippines during the end of Marcos dictatorship, and tells the story of newly widowed Marilou (Eugene Domingo) who inherits the towns only barbershop from her husband- a business that has been passed down by generations of men in her husbands family. With no other means of support, she musters the courage to run the barbershop. But as to be expected, she fails to attract any customers. But a touching act of kindness she extended to Rosa, a prostitute who works in the town brothel, leads to an unexpected opportunity. Rosa, who now considers Marilou a friend, urges her prostitute friends to pressure their male clientele into patronizing Marilous barbershop. The men have no choice but to grudgingly oblige out of fear that Rosa will expose their infidelity to their wives.

==Cast==
*Eugene Domingo as Marilou
*Daniel Fernando as Jose
*Eddie Garcia as Father Arturo
*Sue Prado as Rosa
*Shamaine Buencamino as Tess
*Nicco Manalo as Edmond
*Gladys Reyes as Susan
*Noni Buencamino as Mayor Alfredo Bartolome
*Iza Calzado as Cecilia
*Nora Aunor as guerilla leader (cameo appearance)

==Awards and nominations==
*2013 Tokyo International Film Festival 
** Best Actress (Eugene Domingo)
** Nominated–Tokyo Sakura Grand Prix 2014 Asian Film Awards
** Nominated–Best Actress (Eugene Domingo)
*2014 Udine Far East Film Festival
** 3rd Place - Audience Award

==References==
 

==External links==
*  

 
 
 
 