Anita (2009 film)
{{Infobox film name = Anita, la vida puede cambiar en un instante (Anita, life can change in an instant) director = Marcos Carnevale assistantdirector = Bruno Hernández artistic direction = Adriana Siemenson producer = Marco Carnevale Jorge Gundín music = Lito Vitale sound = José Luis Díaz Gonzalo Matijas cinematography = Guillermo Zappino editing = Pablo Barbieri Carrera costumes = Mónica Toschi effects = Juan José Elias Swan Glecer Alejandro Valente starring = Norma Aleandro Luis Luque Leonor Manso Peto Menahem Alejandra Manzo Marcela Guerty Mercedes Scapola Mateo Cho Mei Lan Chen Huang Hui Shion country = Argentina released = 2009 genre = Drama runtime = 104 minutes language = Spanish
}}
 Argentine film released in 2009.

==Synopsis==
Anita (Alejandra Manzo) is a young girl with Downs Syndrome and lives with her mother (Norma Aleandro) in the Once neighborhood of Balvanera in Buenos Aires. Her life is changed forever in 1994 after the  AMIA bombing. Anita doesnt understand what has happened; she only remembers that her mother left to handle some paperwork, the ground began to shake and she fell off the ladder she was not supposed to climb. Frightened by the noise, she runs from the area of the bombing, totally disoriented. In her odyssey she encounters several people that provide temporary assistance as she searches for her mother.

== The People Anita Encounters ==

=== Felix ===
Foul-mouthed, constant problems with his ex-wife and unkempt, he takes her in after encountering her at a public phone, feeding her and giving her a bed.  He lets her stay for one night and then kicks her out along with a warm sweater to wear.  Another time, he leaves her on the bus to fend for herself. He always thinks of the possibility of going to the police with her.

=== Chinese family ===
Anita gets hungry and she chooses a store with a severe owner and her docile mother.  She is rejected when she has no money and rudely served when she does have money.  The mother convinced her daughter to let Anita into their family life, where she eats and asks for more.  She quietly witnesses the family arguments.  On the last day she is with them, the son sees her dancing to music while he is in charge of the store and he turns it up and dances with her.  This leads to a store robbery, Anita being threatened, as well.  She manages to get out and runs away.

=== Shady Men/Nurse ===
Anita falls asleep with a fever on a sofa men have to move for an unknown crime and she is taken to one of the mens sister, a nurse, who injects Anita with a cure for fever.  She tells her brother to eventually do something about Anita because she works and cannot take care of her.  When she allows Anita to stay alone, Anita misinterprets her instructions and stays on the couch no matter what, which includes bodily functions.  Anita ruins her couch with urine.

==Cast==
*Alejandra Manzo - Anita.
*Norma Aleandro - Dora (Anitas mother).
*Luis Luque - Félix.
*Leonor Manso - Nori (a nurse).
*Peto Menahem - Ariel (Anitas brother).
*Mercedes Scápola - Nati (Ariels girlfriend).
*Marcela Guerty - wife of Félix.
*Mei Lan Chen 
*Huang Hui Shion
*Mateo Cho
*Agus Weimer
*Sofi Spengler
*Meli Fuchs
*Jesi Fuchs 
*Carla Ramos
*Marti Alva

=== Language ===
This film is spoken in Spanish with English subtitles.  Chinese is also spoken, but no subtitles accompany.
 
 

 