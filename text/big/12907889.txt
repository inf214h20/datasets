Aurat (1953 film)
{{Infobox film
| name           = Aurat
| image          = aurat 1953.jpg
| image_size     = 
| caption        = 
| director       = Bhagwan Das Varma
| producer       = Munshiram Varma
| writer         = Bhagwan Das Varma (screenplay)  Agha Jani Kashmiri, Balkishen Mauj, Ratilal Thakar (story)
| narrator       = 
| starring       = Hiralal Premnath Bina Rai
| music          = Shankar Jaikishan
| cinematography = Jamshed Irani
| editing        = Pandurang S. Khochikar
| distributor    = 
| released       = 1953
| runtime        =
| country        = India
| language       = Hindi
| budget         = 
}} 1953 Hindi Samson and Delilah.  Premnath and Bina Rai fell in love during filming and later got married.  The film was not successful. 

==Plot==
Adil (Premnath) is a dashing, brave young man, full of hope and mindful of the rights of all people, especially the downtrodden. It is this last attribute that ends up putting him in the bad books of the Emperor (Ulhas). The Emperor orders his arrest. But before the soldiers can arrest Adil, he single-handedly rescues the Emperors bethrothed from a lion. This earns him the gratitude of beautiful Queen-to-be, Juhi (Purnima). The Emperor, pleased with Adil, gives him an important assignment in his army, and promises that Adils fellow-villagers will not face oppression, taxes or forced labour from his army. Ruhi (Bina Rai) loves Adil, but Adil is unaware of this. Juhi is impressed by Adil and this impression turns to love, and she tells Adil about this. Adil does not think it appropriate for a bethrothed to be his lover and wife, and spurns her. Angered Juhi starts to plot against Adil, by getting the emperor to collect taxes and forced labour from Adils village. This angers Adil, who goes to confront the emperor and the queen-to-be. Angered, the Emperor dismisses Adil, and orders his arrest. Adil flees to the hills. Numerous attempts by the emperors armies to apprehend Adil are in vain. Finally, Juhi tells the emperor that she will bring in Adil without spilling a drop of blood. Will Ruhi succeed where so many have failed?

box office the film was flop but bina rai got married

==Cast==
* Bina Rai	 ...	Juhi
* Prem Nath	 ...	Adil (as Premnath) Purnima	 ...	Ruhi Ulhas        ...	The King (as Ullhas)
* Hiralal	 ...	Military Commander
* Roopmala	 ...	Yasmin

==Soundtrack==
* Nainon Se Nain Huwe Char
* Ulfat Ka Saaz ChhedSuna Suna
o
* Suna Suna Hai Jahan
* Dard-E-Ulfat Chhupaon Kahan
* Aankhon Aankhon Mein
* Yeh Duniya Banayi Hai Kis Be-Reham Ne

== External links ==
*  

 
 
 
 

 