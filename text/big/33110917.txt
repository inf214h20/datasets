The Woman in the Case (1916 Australian film)
 
{{Infobox film
  | name     = The Woman in the Case
  | image    = 
  | caption  =  George Willoughby
  | producer = George Willoughby
  | writer   = 
  | based on = play by Clyde Fitch
  | starring = Jean Robertson
  | music    = 
  | cinematography = 
  | editing  = 
| studio = Willoughbys Photo-plays
  | distributor = Eureka Films
  | released = 2 May 1916 (preview)    3 July 1916 
  | runtime  = 6,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 play of the same name by Clyde Fitch.

It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 64 

==Plot==
Julian Rolfe has an affair with Clare Foster as a young man, but then settles down to marriage with Margaret. Clare tries to blackmail Julian but Margaret destroys the letters. Clare murders Julians ward, Phillip, and tries to frame Julian for it. Julian is sentenced to death but Margaret manages to get Clare to confess.

==Cast==
*Jean Robertson as Margaret Rolfe
*Loris Bingham as Clare Foster
*Fred Knowles as Julian Rolfe
*Herbert J Bentley as Phillip Long
*Winter Hall
*David Edelsten
*Austin Milroy

==Production==
George Willoughby had toured with the play though Australia in 1911 and 1912 to great success.   Over 300 people were involved in making the movie. 
 AIF and was wounded in France in May 1917, losing an arm. However he managed to resume his career. 

Two other films were made from the same play, in 1916 and 1922 (as The Law and the Woman).

==Release==
The movie was trade screened in May 1916. 

Willoughby later revived the play in 1927. 

It was announced that the Willoughby Company were then to make The Pearl of the Pacific based on a story by Randolph Bedford, but this film appears to have never been made. 

==See also==
*The Woman in the Case (1916 American film)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 