Nalla Neram
{{Infobox film
| name           = NALLA NERAM
| image          = Nalla Neram.jpg
| image_size     =
| caption        =
| director       = M. A. Thirumurugam
| producer       = Sandow M. M. A. Chinnappa Thevar
| writer         = R. K. Shanmugam (dialogues)
| story          = Sandow M. M. A. Chinnappa Thevar
| narrator       =
| starring       = M. G. Ramachandran K. R. Vijaya Major Sundarrajan Nagesh S. A. Ashokan Thengai Srinivasan
| music          = K. V. Mahadevan
| cinematography = T. M. Sundar Babu V. Selvaraj
| editing        = M. A. Thirumurugam M. G. Balu Rao
| studio         = Devar Films
| distributor    = Devar Films
| released       = 10 March 1972 
| runtime        = 162 minutes
| country        = India Tamil
| budget         = 
| website        =
}}
 Indian Tamil Tamil  directed by M. A. Thirumugam, starring M. G. Ramachandran in the lead role and K. R. Vijaya, Nagesh among others. 

==Plot==

NALLA NERAM is about Raju (MGR)s choice between love his wife (K. R. Vijaya) and friendship (his loyal and devoted pet elephants, ).

This is the story about the friendship and loyalty of animals with man.

Raju owns elephants and earns a livelihood by making them perform tricks.

He falls in love with Vijaya and they get married. But soon, a problem arises.

Vijaya hates elephants because of a personal tragedy in her life.

She also feels that Raju spends too much time with his elephant friends.

Will the elephants succeed in winning over her heart or will Raju be forced to make a choice between his wife and his animal friends?

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
|-
| M. G. Ramachandran || as Raju (alias Raja Kumar), a business man
|-
| S. A. Ashokan || as Dharmalingham, Vijaya s father
|-
| Major Sundarrajan || as Velu, the trainer of animals 
|-
| Thengai Srinivasan || as Raju s the accountant
|-
| S. V. Ramadasss || as Raju s father
|-
| V. Gopalakrishnan || as Paramasivan, the ex-famous acrobat (The human torch)
|- Justin || as The hired man of Velu
|-
| "Sandow" M.M.A.Chinnappa Devar || as Ranga, a fairground entertainer and Velu s henchman  
|-
| Nagesh || as Murugan, a fairground entertainer and Raju s friend  
|-
| K. R. Vijaya || as Viji (alias Vijaya), Raju s lover and after wife
|-
| Kumari Satchou || as Valli, Murugan s lover
|-
| Radhiga || as
|-
| "Kovalai" Kamatchi || as 
|-
| Master Raju Kumar || as Raju, child
|-
| 4 magnificent elephants (Not mentioned) || as Ramu (is the friend and the favorite of Raju), Ganga, Meena and Somu
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

The movie is a remake of super silver jubilee hit 1971 Hindi film Haathi Mere Saathi by Rajesh Khanna with excellent songs.

About 5 years passed when MGR finds his partner of VIVASAYEE (another Devar Films of 1967), the big actress K.R.Vijaya.

There is 16 "Devar Films" with MGR.

The winning combination(overall) was always :

MGR (Main actor, hero)/M.A.Thirumugham (Director)/K.V.Mahadevan (Composer)/"Sandow" M.M.A.Chinnappa Devar (Producer, in the movie, sometimes, the henchman)

NALLA NERAM was the last collaboration (in highlight), MGR-Chinnappa Devar and the only one in color movie in the list.

==Soundtrack==
The music composed by K. V. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Agattumda Thambi || T. M. Soundararajan || Avinashi Mani || 03:05
|-
| 2 || Nee Thottal || T. M. Soundararajan, P. Susheela || Kannadasan || 03:22
|-
| 3 || Odi Odi Uzhaikkum || T. M. Soundararajan || Pulamaipithan || 03:11
|-
| 4 || Tick Tick || T. M. Soundararajan, P. Susheela || Kannadasan || 03:12
|-
| 5 || Dance Music || Sexy Tamil Cabaret Song Instrumental || || 02:19
|}

==References==
 

==External links==
*  

 
 
 
 
 


 