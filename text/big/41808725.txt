The Assisi Underground (film)
{{Infobox film
| name           = The Assisi Underground
| image          = Assisi Underground.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Alexander Ramati
| producer       = {{Plainlist |
*Yoram Globus
*Menahem Golan
}}
| writer         = Alexander Ramati
| screenplay     = Alexander Ramati
| story          = 
| based on       =   
| narrator       = 
| starring       = {{Plainlist |
* Ben Cross
* James Mason
* Irene Papas
* Maximilian Schell
* Karlheinz Hackl
}}
| music          = Dov Seltzer
| cinematography = Giuseppe Rotunno
| editing        = Michael J. Duthie
| studio         = 
| distributor    = 
| released       =  
| runtime        = 115 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} novel of the same name by Alexander Ramati.

==Plot==
In 1943 Franciscan priest Rufino Niccacci is charged by the bishop of Assisi Giuseppe Placido Nicolini with rescuing Italian Jews from the Nazis.

==Cast==
* Ben Cross as Rufino Niccacci  
* James Mason as Giuseppe Placido Nicolini  
* Irene Papas as Mother Giuseppina 
* Maximilian Schell  as Colonel Valentin Müller
* Karlheinz Hackl as  Capitain von Velden       
* Geoffrey Copleston as Chief of Police Bertolucci   
* Riccardo Cucciolla  as Luigi Brizi   
* Angelo Infanti as Giorgio Kropf 
* Delia Boccardo as  Countess Cristina 
* Paolo Malco as Paolo Josza 
* Roberto Bisacco as Professor Rieti
* Edmund Purdom as Cardinal Della Costa
* Venantino Venantini as Pietro
* Maurice Poli as Vito 
* Giancarlo Prete as Col. Gay 
* Alessandra Mussolini as  Sister Beata
* Riccardo Salvino as Otto Maionica
* Greta Vayan as Rita Maionica

==Critical response==
The film received a poor critical response.  
  

==References==
 

==External links==
*  

 
 
 
 
 
 


 