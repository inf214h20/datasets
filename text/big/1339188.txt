Popeye (1980 film)
 
{{Infobox film
| name           = Popeye
| image          = Popeyemovieposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Altman Robert Evans
| screenplay     = Jules Feiffer
| story          = Robert Altman
| based on       =  
| starring       = Robin Williams Shelley Duvall Paul L. Smith Paul Dooley Ray Walston
| music          = Harry Nilsson
| cinematography = Giuseppe Rotunno John W. Holmes David A. Simmons Walt Disney The Hearst Corporation Buena Vista Distribution Co., Inc.  
| released       =  
| runtime        = 114 minutes 
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $60 million   
}} American Musical musical comedy film, directed by Robert Altman and is a live-action film adaptation of E. C. Segars Thimble Theatre aka Popeye comic strip. It stars Robin Williams     as Popeye the Sailor Man and Shelley Duvall as Olive Oyl.

The film premiered on December 6, 1980 in Los Angeles, California, to mixed reviews and disappointing box office. The film has since been released on DVD as well as digital download and Netflix instant streaming. Harry Nilssons soundtrack received mostly positive reviews.

==Plot==
Popeye, a sailor, arrives at the small coastal town of Sweethaven ("Sweethaven—An Anthem") while searching for his long-lost father. He is immediately feared by the townsfolk simply because he is a stranger ("Blow Me Down"), and is accosted by a greedy taxman. He rents a room at the Oyl familys boarding house where the Oyls daughter, Olive Oyl|Olive, is preparing for her engagement party. Her hand is promised to Captain Bluto, a powerful, perpetually angry bully who runs the town in the name of the mysterious Commodore. In the morning, Popeye visits the local diner for breakfast ("Everything Is Food") and demonstrates his strength as he brawls with a gang of provocative ruffians who give him and the other customers a hard time.

On the night of the engagement party, Bluto and the townsfolk arrive at the Oyls home. Olive, however, sneaks out of the house, after discovering that the only attribute she can report for her bullying fiance is size ("Hes Large"). She encounters Popeye, who failed to fit in with the townsfolk at the party. The two eventually come across an abandoned baby in a basket. Popeye adopts the child, naming him SweePea, and the two return to the Oyls home. Bluto, however, has grown increasingly furious with Olives absence. He realizes that she means to break off the engagement. He eventually flies into a rage and destroys the house ("Im Mean"). When he sees Popeye and Olive with SweePea, Bluto beats Popeye into submission and declares heavy taxation for the Oyls.

The taxman repossesses the remains of the Oyls home and all their possessions. The Oyls son, Castor Oyl|Castor, decides to compete against the local heavyweight boxer, Oxblood Oxheart, in the hopes of winning a hefty prize for his family. However, Castor is no match for Oxheart and is savagely beaten and knocked out of the ring. Popeye takes the ring in Castors place and defeats Oxheart, putting on a show for the townsfolk and finally earning their respect. Back at home, Popeye and Olive sing SweePea to sleep ("Sailin ").
 Wimpy overhears and asks to take SweePea out for a walk, though he actually takes him to the "horse races" (actually a form of carnival game as depicted in the movie) and wins two games. Popeye, however, is outraged, and vents his frustrations to the racing parlors customers ("I Yam What I Yam"). Fearing further exploitation of his child, Popeye moves out of the Oyls home and onto the docks; when the taxman harasses him, Popeye pushes him into the water, prompting a celebration by the townspeople. In the chaos, Wimpy, who has been intimidated by Bluto, kidnaps SweePea for him. That night, Olive remarks to herself about her budding relationship with Popeye ("He Needs Me"), while Popeye writes a message in a bottle for SweePea ("SweePeas Lullaby").

Wimpy sees Bluto taking SweePea into the Commodores ship; he and Olive inform Popeye. Inside, Bluto presents the boy to the curmudgeonly Commodore, promising that he is worth a fortune; however, the Commodore refuses to listen, reminding Bluto that his buried treasure is all the fortune he needs. His patience with the Commodore exhausted, Bluto ties him up and takes SweePea himself ("Its Not Easy Being Me"). Popeye storms the ship and meets the Commodore, realizing that he is his father, Poopdeck Pappy. However, Pappy initially denies that Popeye is his son; to prove it, Pappy tries to feed Popeye spinach, which he claims is his familys source of great strength. However, Popeye hates spinach and refuses to eat it. Bluto kidnaps Olive as well and sets sail to find Pappys treasure. Popeye, Pappy, and the Oyl family board Pappys ship to give pursuit. Bluto sails to Scab Island, a desolate island in the middle of the ocean, while Pappy argues with his son and rants about children ("Kids").
 turns yellow and he swims away as Popeye celebrates his victory and his new-found appreciation of spinach ("Popeye the Sailor Man").

==Cast==
 
* Robin Williams as Popeye
* Shelley Duvall as Olive Oyl
* Paul L. Smith as Bluto
* Paul Dooley as J. Wellington Wimpy
* Richard Libertini as George W. Geezil
* Ray Walston as Poopdeck Pappy
* Donald Moffat as The Taxman
* MacIntyre Dixon as Cole Oyl
* Roberta Maxwell as Nana Oyl
* Donovan Scott as Castor Oyl
* Allan F. Nicholls as Rough House
* Wesley Ivan Hurt as SweePea Ham Gravy
* Robert Fortier as Bill Barnacle
* David McCharen as Harry Hotcash
* Sharon Kinney as Cherry
* Peter Bray as Oxblood Oxheart
* Linda Hunt as Mrs. Oxheart
* Geoff Hoyle as Scoop
* Wayne Robson as Chizzelflint
* Larry Pisoni as Chico
* Carlo Pellegrini as Swifty
* Klaus Voormann as Von Schnitzel
* Dennis Franz as Spike Carlos Brown as Slug
* Jack Mercer as Popeye the Sailor (Animated prologue only)
 

==Musical numbers==
# "Sweethaven—An Anthem" - Chorus
# "Blow Me Down" - Popeye
# "Everything Is Food" - Chorus
# "Hes Large" - Olive and Chorus
# "Im Mean" - Bluto and Chorus
# "Sailin " - Popeye and Olive
# "I Yam What I Yam" - Popeye
# "He Needs Me" - Olive
# "SweePeas Lullaby" - Popeye
# "Its Not Easy Being Me" - Pappy and Bluto
# "Kids" - Pappy
# "Im Popeye the Sailor Man" - Popeye, Olive, and Chorus

==Production==
 ]]
 Columbia and Robert Evans found out that Paramount had lost the bidding for Annie, he held an executive meeting in which he asked about comic strip characters that they had the rights to, that could also be used in order to create a movie musical, and one attendee said "Popeye".

At that time, although King Features Syndicate retained the television rights to Popeye and related characters, (Hanna-Barbera was producing the series The All-New Popeye Hour at the time under license from King Features), Paramount still held all theatrical rights to the Popeye character, due to the studio releasing cartoons produced by Fleischer Studios and Famous Studios, respectively, that lasted from 1932-57.

Evans commissioned Jules Feiffer to write a script. In 1977, he said he wanted Dustin Hoffman to play Popeye opposite Lily Tomlin as Olive Oyl, with John Schlesinger directing. At the Movies: Producer Sets Hoffmans Sail For Popeye
Flatley, Guy. New York Times (1923-Current file)   October 14, 1977: 58. 

The film was shot in Malta. The film set that was built still exists, and it is now a tourist attraction known as Popeye Village.

==Release==
  premiered at the Graumans Chinese Theatre|Manns Chinese Theater in Los Angeles on December 6, 1980, two days before what would have been E.C. Segars 86th birthday.  

===Box office===
The film grossed US$6,000,000 on its opening weekend in the U.S., and made US$32,000,000 after 32 days.      The film earned United States dollar|$49,823,037    at the United States box office — more than double the films budget — and a worldwide total of US$60,000,000.   Although the films gross was decent, it was nowhere near the blockbuster that Paramount and Disney had expected, and was thus written off as a flop. 

===Critical reception===
The film received overall mixed reviews: some favorable, from critics such as   cartoons instead; youll be much better off."    Rotten Tomatoes gave the film a 57% "Rotten" rating with the critical consensus stating   "Altmans take on the iconic cartoon is messy and wildly uneven, but its robust humor and manic charm are hard to resist."

==Soundtrack==
{{Infobox album|  
| Name        = Popeye (Soundtrack)
| Type        = Soundtrack
| Artist      = Harry Nilsson
| Cover       =
| Released    = 1981
| Recorded    = 1980
| Genre       = Pop Music
| Length      =
| Label       = Boardwalk
| Producer    = Bruce Robb Flash Harry   (1980)
| This album  = Popeye (soundtrack)   (1981)
| Next album  = –
}} Flash Harry to write the score for the film. He wrote all the original songs and co-produced the music with producer Bruce Robb at Cherokee Studios. The soundtrack was unusual in that the actors sang some of the songs "live". For that reason, the studio album did not quite match the tracks heard in the film. Van Dyke Parks is credited as music arranger.

In the U.S. trailer for the film which contained the song "I Yam What I Yam", the version heard of the song was from the soundtrack album, not the film.
 the original Max Fleischer cartoon.

# "I Yam What I Yam" – (2:16)
# "He Needs Me" – (3:33)
# "Swee Peas Lullaby" – (2:06)
# "Din We" – (3:06)
# "Sweethaven—An Anthem" – (2:56)
# "Blow Me Down" – (4:07)
# "Sailin" – (2:48)
# "Its Not Easy Being Me" – (2:20)
# "Hes Large" – (4:19)
# "Im Mean" – (2:33)
# "Kids" – (4:23)
# "Im Popeye the Sailor Man" – (1:19)

* The song "Everything Is Food" was not included on the album, while the song "Din We" (which was cut from the film) was.
* The song "Sweethaven—An Anthem" is the only song heard twice in the film.

==See also==
* List of American films of 1980

==References==
 

;Further reading
*  

==External links==
 
 
*  
*  
*  
*  
*  
*     at Allmusic
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 