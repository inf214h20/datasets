Mega Shark Versus Giant Octopus
{{Infobox film
| name           = Mega Shark Versus Giant Octopus
| image          = megasharkvsgiantoctopus.jpg
| caption        = DVD cover
| director       = Jack Perez (as Ace Hannah)
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Jack Perez (as Ace Hannah)
| narrator       =  Deborah Gibson Lorenzo Lamas Mark Hengst Sean Lawlor Jay Beyers Stephen Blackehart
| music          = Chris Ridenhour
| cinematography = Alexander Yellen
| editing        = Marq Morrison
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 Deborah Gibson and actor Lorenzo Lamas. Though it was met with a negative reaction from critics for its outlandish plot, it is arguably the most popular film by The Asylum to date. The film is about the hunt for two prehistoric sea monsters causing mayhem and carnage at sea.  This film is also notable as one of the very few American films to feature an Asian American male as a romantic lead as Vic Chaos character Dr. Seiji Shimada, serves as a love interest for Gibsons character, Emma MacNeil.

== Plot == Deborah Gibson) sonar transmitters into the water, causing a pod of whales to go out of control and start ramming a nearby glacier. In the chaos, the helicopter crashes into the glacier, and the combined damage breaks the glacier open, thawing two hibernating, prehistoric creatures. MacNeil narrowly avoids destruction as, unknown to her, a giant shark and octopus are freed. Some time later, a drilling platform off the coast of Japan is attacked by the octopus, which has tentacles large enough to wrap around the entire structure. After returning to Point Dume, California, MacNeil investigates the corpse of a beached whale covered with many bloody wounds. Her employer Dick Richie (Mark Hengst) believes them to be from a tanker propeller, but MacNeil insists they appear to be from a creature. Later, she extracts what appears to be a shark’s tooth from one of the wounds. Elsewhere, the huge shark leaps tens of thousands of feet into the air from the ocean and attacks a commercial aircraft, forcing it to crash into the water.
 naval destroyer"  engages the megalodon, but is destroyed after its guns fail to destroy the shark. MacNeil, Sanders, and Shimada are arrested by a team of soldiers and taken to government official Allan Baxter (Lorenzo Lamas), a rude and racist man who demands their help in destroying the creatures. The four agree to help, in exchange for the government trying to capture the creatures for study rather than destroy them.

While working at a naval laboratory to develop a method for luring the creatures, MacNeil and Shimada become attracted to each other and have sex in a utility closet. The incident makes them realize they can attract the creatures using chemicals. MacNeil and Sanders agree to place a trap for the shark in San Francisco Bay, while Shimada returns to Tokyo to attract the octopus. MacNeil and Sanders barely escape the shark after placing the trap with a mini-submarine. The plan fails, however, when the shark destroys another destroyer sent by Baxter. The shark then resurfaces and bites off a large portion of the Golden Gate Bridge, killing many civilians on the bridge. Later, Shimada contacts the Americans and says the Japanese trap, having the same disaster, only succeeded in angering the octopus, which has escaped despite multiple artillery and missile hits. Baxter suggests using nuclear weapons against the creatures, which MacNeil, Sanders, and Shimada strongly oppose due to the risk of marine devastation, coastal damage and human casualties. As an alternative, MacNeil suggests using the same pheromone traps to create a “Thrilla in Manila” by drawing the two creatures together. She believes that because the two creatures were frozen in ice locked in combat, they must be natural rivals and their aggressiveness towards one another will cause them to fight to the death if theyre lured together. MacNeil, Sanders, and Baxter are assigned to a submarine to find the shark and lure it to the North Pacific Ocean.

After a short search, the submarine brings both the shark and the octopus to an ice trench off the Alaskan coast, where MacNeil first encountered the creatures. Sanders himself ends up piloting the sub after the original pilot loses his nerve and pulls a gun on the captain (Dean Kreyling) before he is overtaken. The octopus and shark begin to fight, Giant Octopus, wraps around Mega Shark, and tries to suffocate him. Mega Shark bites the tentacles to get free. They separate. Mega Shark attacks the submarine. dragging it in its mouth. MacNeil, Sanders, and Baxter man a mini-submarine and detach just as the shark bites the larger submarine in half, killing the rest of the crew. The shark nearly destroys the mini-submarine, but they are saved when a Japanese sub manned by Shimada fires torpedoes at it. The octopus entangles the Japanese submarine and nearly destroys it, but the sub is released after the octopus is attacked by the shark. The two creatures engage in a fierce battle, at the end of which, the octopus strangles the shark after the shark dismembers some of its tentacles, causing it to bleed to death. The two sink, dead, still locked from their battle.  McNeil and the others, after watching the showdown between the two monsters, discover Shimada and his sub survived the octopus attack. The film ends with MacNeil, Sanders, and Shimada deciding to visit the North Sea after receiving infrared images of mysterious organic life there.

==Cast== Deborah Gibson as Emma MacNeil
*Lorenzo Lamas as Allan Baxter
*Vic Chao as Dr. Seiji Shimada
*Mark Hengst as Dick Richie
*Sean Lawlor as Lamar Sanders
*Dean Kreyling as U.S. Sub Captain
*Stephen Blackehart as U.S. Sub Sonar Chief
*Larry Wang Parrish as Japanese Typhoon Captain
*Douglas N. Hachiya as Japanese Sonar Tech
*Jay Beyers as Pilot Officer
*Stefanie Gernhauser as Sub Cmdr. Francoise Riley
*Jonathan Nation as Vince
*Russ Kingston as Admiral Scott
*Cooper Harris as U.S. Destroyer Sonar Tech
*Dustin Harnish as U.S. Sub Helmsman
*Colin Broussard as Radioman

==Production==
Efforts to convert the movie into 3D were scrapped when the studio failed to acquire adequate funding for 3-D film. 
 Long Beach, CA, and at Laurel Canyon Stages in Arleta, Los Angeles, California|Arleta, CA. Other locations included the Long Beach pier and Leo Carillo State Beach in Malibu, California|Malibu, CA.

==Release and reception==
The theatrical trailer released in mid-May 2009 became a viral hit, scoring over a million hits on MTV.com and another million more on YouTube upon launch, prompting brisk pre-orders of the DVD. 

The film was met with mostly negative reviews, with a 15 percent rating on Rotten Tomatoes, and the consensus stating "With shoddy FX, acting and directing. This isnt so bad its good. Its just so bad its terrible."  Peter Whittle of The Sunday Times gave the film one out of five stars and considered it "Unwatchable, almost unreviewable, this stupid monster movie makes the Béla Lugosi swan song Plan 9 from Outer Space look like a masterpiece."   Philip French of The Observer said in his review that "The risible special effects and the clumsy acting recall not Roger Corman productions but the ineptitude of Ed Wood, though the result is far less endearing."  Kim Newman of Empire (magazine)|Empire magazine gave it two out of five stars, calling it "Daft, plain daft. With a few daft but spectacular stunts."  Scott Mendelson of The Huffington Post also gave the film two out of five stars, saying that "the actors are all appropriately terrible and the story is completely absurd." 

On the other hand, Bill Gibron of PopMatters gave the film an 8 out of 10, saying that "Schlock (film)|Schlock may be an acquired taste, like caviar, foie gras, and Arbys, but its hard to see how anyone wouldnt enjoy this extremely tacky dish." He also praised Gibsons performance in the film.    Despite criticizing the overall film quality, Stephen of The Three Rs gave the film a 7 out of 10, calling the plane attack scene "the epitome of monster awesome." 

==Sequels==
 

Following the buzz generated by the release of the films trailer, Gibson hinted on an interview that a sequel may be in the works. "Because God knows what else is in the ocean", she said. "I thought maybe a Seahorse? Maybe a 500-foot (150 m) Lobster would be good. But yes, there is the potential for a sequel, and I would absolutely love it." 

In mid-2010, The Asylum updated their catalog with Mega Shark Versus Crocosaurus, which was released on December 21, 2010. The cast and director of Mega Shark Versus Giant Octopus did not return in the sequel; instead, the film was directed by Christopher Douglas-Olen Ray and featured a new cast led by Jaleel White, Gary Stretch and Robert Picardo. 

The Asylum released the third chapter in the series titled Mega Shark Versus Mecha Shark on January 28, 2014, with Gibson reprising her role as Emma MacNeil.   The fourth Mega Shark was in February 2015 confirmed Mega Shark vs. Kolossus,  had an premiere date on 7 July 2015. 

==See also==
*List of killer shark films

==References==
 

==External links==
*  at The Asylum
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 