Lilac Time (1928 film)
{{Infobox film
| name           = Lilac Time
| image          = Lilac Time theatrical poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = {{Plainlist|
* George Fitzmaurice
* Frank Lloyd (uncredited)
}} John McCormick
| writer         = {{Plainlist|
* Carey Wilson
* George Marion Jr. (intertitles)
* Jane Murfin (play)
* Jane Cowl (play)
* Willis Goldbeck (adaptation)
* Adela Rogers St. Johns (adaptation)
}}
| based on       =  
| starring       = {{Plainlist|
* Colleen Moore
* Gary Cooper
}}
| music          = {{Plainlist|
* Cecil Copping
* Nathaniel Shilkret
}}
| cinematography = Sidney Hickox
| editing        = Alexander Hall
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 100 minutes (11 reels)
| country        = United States
| language       = English intertitles Vitaphone  (with music score and sound effects) 
| budget         = 
| gross          = 
}}
Lilac Time is a 1928 American silent romantic war film directed by George Fitzmaurice and starring Colleen Moore and Gary Cooper. Based on a novel by Guy Fowler, the film is about young American aviators fighting for Britain during World War I who are billeted in a field next to a farmhouse in France. The daughter who lives on the farm meets one of the new aviators who is attracted to her. As the flyers head off on a mission, the young aviator promises to return to her. 
 John McCormick (Moores husband), and distributed by First National Pictures. The film is based on a 1917 Broadway play written by Jane Murfin and actress Jane Cowl, who adapted the story from a book by Guy Fowler.  This film was released with a Vitaphone score and music effects, featuring the song "Jeannine, I Dream of Lilac Time," but there was no spoken dialogue. The film premiered in New York City on August 3, 1928, and was release in the United States on October 18, 1928.

==Production background==
The film was shot on sets at First Nationals Burbank studio and on location in El Torro, California, where a working airstrip, full-sized French Village and farm were built. In addition a portable machine shop serviced the eight aircraft used in the production. Looking for realism, many extras cast as soldiers in the film had been actual World War I soldiers, in the ranks they portrayed. The chief stunt pilot, Dick Grace, had only finished doing stunt work on the Paramount film Wings (1927 film)|Wings almost two months earlier. Grace sustained a severe neck injury in a stunt crash while making Wings but recovered in time for Lilac Time. 

The film offers several phases, beginning with slapstick comedy elements, becoming an intense romantic film, then segueing into a spectacular aerial showdown followed by a duel in the sky between Coopers character and the Red Baron before returning to romantic complications.
 Flaming Youth (1923). 

Among those in the cast were Colleen Moores brother Cleve (under the name Cleve Moore) and Jack Stone, her cousin. Eugenie Besserer had played "Mrs. Goode," a mother figure in Colleens earlier film Little Orphan Annie, the first film to bring Colleen a measure of fame. 

A restored 35mm print of the film was screened at the Museum of Modern Art in New York City in September 2014.

==Cast==
* Colleen Moore as Jeannine Berthelot
* Gary Cooper as Captain Philip Blythe
* Burr McIntosh as General Blyth George Cooper as Mechanics helper
* Cleve Moore as Captain Russell
* Kathryn McGuire as Lady Iris Rankin
* Eugenie Besserer as Madame Berthelot
* Emile Chautard as The Mayor
* Jack Stone as The Kid Edward Dillon as Mike the Mechanic
* Dick Grace as Aviator
* Stuart Knox as Aviator
* Harlan Hilton as Aviator
* Richard Jarvis as Aviator
* Jack Ponder as Aviator
* Dan Dowling as Aviator
* Eddie Clayton as The Enemy Ace Arthur Lake as The Unlucky One
* Philo McCullough as German Officer
* Nelson McDowell as French Drummer
* J. Gunnis Davis Paul Hurst
* Harold Lockwood - (Harold Lockwood Jr.; son of the late silent movie star)

==References==
 

==Footnotes==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,   Print ISBN 978-0-7864-4969-9 / EBook ISBN 978-0-7864-8899-5

==External links==
* 
* 
* 
*  at Virtual History

 

 
 
 
 
 
 
 
 